<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix �¾����� | ���ౡ�������� | �ౡͽ��ɽ</title>
<?
require_once("about_comedix.php");
require_once("show_select_values.ini");

$fname = $PHP_SELF;

// ���å����Υ����å�
$session = qualify_session($session, $fname);
if ($session == "0") {
	js_login_exit();
}

// ���¤Υ����å�
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	js_login_exit();
}

// ���Ų���Ͽ���¤����
$section_admin_auth = check_authority($session, 28, $fname);

// ������Ͽ���¤����
$ward_admin_auth = check_authority($session, 21, $fname);

// �ǡ����١�������³
$con = connect2db($fname);

// ɽ�����֤�����
if ($year == "") {$year = date("Y");}
if ($month == "") {$month = date("m");}
if ($day == "") {$day = date("d");}
if ($term == "") {$term = "1w";}
$start_date = "$year$month$day";
switch ($term) {
case "1w":
	$end_date = date("Ymd", strtotime("+6 days", mktime(0, 0, 0, $month, $day, $year)));
	break;
case "2w":
	$end_date = date("Ymd", strtotime("+13 days", mktime(0, 0, 0, $month, $day, $year)));
	break;
case "3w":
	$end_date = date("Ymd", strtotime("+20 days", mktime(0, 0, 0, $month, $day, $year)));
	break;
case "1m":
	$end_date = date("Ymd", strtotime("+1 month -1 day", mktime(0, 0, 0, $month, $day, $year)));
	break;
}

// ���ŵ��إޥ��������
$sql = "select mst_cd, mst_name from institemmst";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	js_error_exit();
}
$insts = array();
while ($row = pg_fetch_array($sel)) {
	$insts[$row["mst_cd"]] = $row["mst_name"];
}

// �ౡ��ޥ��������
$out_pos_master = get_out_pos_master($con, $fname);

// ���԰��������
if ($mode == "") $mode = "r";  // �ǥե���Ȥ�ͽ���r�ˡ����ӡ�o��
if ($mode == "r") {
	$sql = "select i.inpt_out_res_dt as out_dt, i.inpt_out_res_tm as out_tm, w.ward_name, r.ptrm_name, i.inpt_short_stay, i.inpt_lt_kj_nm, i.inpt_ft_kj_nm, p.ptif_birth, p.ptif_sex, d.dr_nm, i.inpt_back_in_dt, i.inpt_back_in_tm, i.inpt_out_pos_rireki, i.inpt_out_pos_cd, i.inpt_out_inst_cd, i.out_res_updated as updated, i.ptif_id, i.inpt_in_dt, i.inpt_in_tm, (select max(n.note) from bed_outnote n where n.ptif_id = i.ptif_id and n.type = 'p' and n.mode = 'r') as note from inptmst i ";
	$cond = q("where i.inpt_out_res_dt between '%s' and '%s' order by out_dt, out_tm", $start_date, $end_date);
} else {
	$sql = "select i.inpt_out_dt as out_dt, i.inpt_out_tm as out_tm, w.ward_name, r.ptrm_name, i.inpt_short_stay, i.inpt_lt_kj_nm, i.inpt_ft_kj_nm, p.ptif_birth, p.ptif_sex, d.dr_nm, i.inpt_back_in_dt, i.inpt_back_in_tm, i.inpt_out_pos_rireki, i.inpt_out_pos_cd, i.inpt_out_inst_cd, i.out_updated as updated, i.ptif_id, i.inpt_in_dt, i.inpt_in_tm, (select max(n.note) from bed_outnote n where n.ptif_id = i.ptif_id and n.type = 'p' and n.mode = 'o' and n.in_dttm = cast((i.inpt_in_dt || i.inpt_in_tm) as varchar)) as note from inpthist i ";
	$cond = q("where i.inpt_out_dt between '%s' and '%s' order by out_dt, out_tm", $start_date, $end_date);
}
$sql .= "inner join ptifmst p on p.ptif_id = i.ptif_id inner join wdmst w on w.bldg_cd = i.bldg_cd and w.ward_cd = i.ward_cd inner join ptrmmst r on r.bldg_cd = i.bldg_cd and r.ward_cd = i.ward_cd and r.ptrm_room_no = i.ptrm_room_no left join drmst d on d.sect_id = i.inpt_sect_id and d.dr_id = i.dr_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	js_error_exit();
}
$patients = array();
$notes = array();
while ($row = pg_fetch_array($sel)) {
	$in_dttm = $row["inpt_in_dt"] . $row["inpt_in_tm"];

	$patients[$row["out_dt"]][] = array(
		"ward_name" => $row["ward_name"],
		"ptrm_name" => $row["ptrm_name"],
		"short_stay" => format_checked($row["inpt_short_stay"]),
		"kanji_name" => $row["inpt_lt_kj_nm"] . " " . $row["inpt_ft_kj_nm"],
		"birth" => format_date($row["ptif_birth"]),
		"age" => format_age($row["ptif_birth"]),
		"sex" => format_sex($row["ptif_sex"]),
		"dr_nm" => $row["dr_nm"],
		"operators" => get_operators($con, $mode, $row["ptif_id"], $row["inpt_in_dt"], $row["inpt_in_tm"], $fname),
		"back_in_dttm" => format_datetime($row["inpt_back_in_dt"], $row["inpt_back_in_tm"]),
		"out_inst" => format_out_inst($out_pos_master, $row["inpt_out_pos_rireki"], $row["inpt_out_pos_cd"], $insts, $row["inpt_out_inst_cd"]),
		"out_tm" => format_time($row["out_tm"]),
		"updated" => $row["updated"],
		"ptif_id" => $row["ptif_id"],
		"in_dttm" => $in_dttm
	);

	if ($mode == "r") {
		$notes["p"][$row["ptif_id"]] = $row["note"];
	} else {
		$notes["p"][$row["ptif_id"]][$in_dttm] = $row["note"];
	}
}

// ���ͥꥹ�Ȥ����������ʬ��
$sql = "select * from bed_outnote";
$cond = q("where type = 'd' and mode = '%s' and date between '%s' and '%s'", $mode, $start_date, $end_date);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	js_error_exit();
}
while ($row = pg_fetch_array($sel)) {
	$notes["d"][$row["date"]] = $row["note"];
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function exportExcel() {
	document.excelform.submit();
}

function changeDate() {
	location.href = 'bed_menu_schedule_out.php?session=' + document.mainform.session.value + '&mode=' + document.mainform.mode.value + '&year=' + document.dateform.year.value + '&month=' + document.dateform.month.value + '&day=' + document.dateform.day.value + '&term=' + document.dateform.term.value;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? ehu($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="�¾�����"></a></td>
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? ehu($session); ?>"><b>�¾�����</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j16"><a href="entity_menu.php?session=<? ehu($session); ?>&entity=1"><b>�������̤�</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j16"><a href="building_list.php?session=<? ehu($session); ?>"><b>�������̤�</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#5279a5"><a href="bed_menu.php?session=<? ehu($session); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12" color="#ffffff"><b>�¾������ȥå�</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? ehu($session); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����Ȳ�</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? ehu($session); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���ౡ��Ͽ</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? ehu($session); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�٥åȥᥤ��</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? ehu($session); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�¾�����Ψ����</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? ehu($session); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�߱���������</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? ehu($session); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���ԥǡ������</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- ��˥塼 -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr height="22">
<td width="17%" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu.php?session=<? ehu($session); ?>">���Τ餻</a></font></td>
<td width="17%" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_status.php?session=<? ehu($session); ?>">�����������ɽ</a></font></td>
<td width="17%" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���ౡ��������</font></td>
<td width="17%" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_inpatient_search.php?session=<? ehu($session); ?>">�������Ը���</a></font></td>
<td width="17%" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_vacant_search.php?session=<? ehu($session); ?>">��������</a></font></td>
<td width="17%" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_occupation.php?session=<? ehu($session); ?>">�¾����Ѿ���</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- ���ౡ�������� -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22" bgcolor="#bdd1e7">
<td width="45"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_schedule.php?session=<? ehu($session); ?>">����</a></font></td>
<td width="90"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_schedule_in.php?session=<? ehu($session); ?>">����ͽ��ɽ</a></font></td>
<td width="90"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><b>�ౡͽ��ɽ</b></font></td>
<td width="150"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_schedule_report.php?session=<? ehu($session); ?>">�����̶����������</a></font></td>
<td width="110"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_schedule_move.php?session=<? ehu($session); ?>">�����ư����</a></font></td>
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_schedule_change.php?session=<? ehu($session); ?>">ž���԰���</a></font></td>
</tr>
</table>
</td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="4" alt=""><br>

<form name="dateform">
<table border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
ǯ�����ʼ��ˡ�<select name="year">
<? show_select_years_span(1960, date("Y") + 1, $year, false); ?>
</select>/<select name="month">
<? show_select_months($month); ?>
</select>/<select name="day">
<? show_select_days($day); ?>
</select>
<select name="term">
<option value="1w"<? if ($term == "1w") {eh(" selected");} ?>>1����
<option value="2w"<? if ($term == "2w") {eh(" selected");} ?>>2����
<option value="3w"<? if ($term == "3w") {eh(" selected");} ?>>3����
<option value="1m"<? if ($term == "1m") {eh(" selected");} ?>>1����
</select>
<input type="button" value="�ѹ�" onclick="changeDate();" style="margin-right:30px;">
<span style="margin-right:10px;">
<? if ($mode == "o") { ?>
<a href="bed_menu_schedule_out.php?session=<? ehu($session); ?>&mode=r&year=<? ehu($year); ?>&month=<? ehu($month); ?>&day=<? ehu($day); ?>&term=<? ehu($term); ?>">
<? } else { ?>
<b>
<? } ?>
ͽ��
<? if ($mode == "o") { ?>
</a>
<? } else { ?>
</b>
<? } ?>
</span>
<span style="margin-right:30px;">
<? if ($mode == "r") { ?>
<a href="bed_menu_schedule_out.php?session=<? ehu($session); ?>&mode=o&year=<? ehu($year); ?>&month=<? ehu($month); ?>&day=<? ehu($day); ?>&term=<? ehu($term); ?>">
<? } else { ?>
<b>
<? } ?>
����
<? if ($mode != "result") { ?>
</a>
<? } else { ?>
</b>
<? } ?>
</span>
<input type="button" value="Excel����" onclick="exportExcel();">
</font></td>
</tr>
</table>
</form>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>

<form name="mainform" action="bed_menu_schedule_out_update.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block">
<tr height="22" bgcolor="#f6f9ff">
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">NEW</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�����ֹ�</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">û��</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����̾</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��ǯ����</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ǯ��</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�缣��</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ô��</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ౡ�����̾</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ౡ����</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�������ͽ��</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
</tr>
<?
$new_start = date("YmdHis", strtotime("-1 day"));

$date = $start_date;
while ($date <= $end_date) {
	$tmp_year = substr($date, 0, 4);
	$tmp_month = substr($date, 4, 2);
	$tmp_day = substr($date, 6, 2);
	$timestamp = mktime(0, 0, 0, $tmp_month, $tmp_day, $tmp_year);
	$rowspan = count($patients[$date]);
	if ($rowspan == 0) $rowspan = 1;
?>
<tr height="22">
<td nowrap align="center" rowspan="<? eh($rowspan); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh(format_date($date)); ?></font></td>
<td nowrap align="center" rowspan="<? eh($rowspan); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh(get_weekday($timestamp)); ?></font></td>
<?
	if (isset($patients[$date][0])) {
?>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? show_label_if_new($new_start, $patients[$date][0]["updated"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["ward_name"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["ptrm_name"]); ?></font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["short_stay"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["kanji_name"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["birth"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["age"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["sex"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["dr_nm"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? show_operators($patients[$date][0]["operators"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["out_inst"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["out_tm"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["back_in_dttm"]); ?></font></td>
<? if ($mode == "r") { ?>
<td nowrap>
<input type="button" value="����" onclick="window.open('bed_menu_schedule_out_detail.php?session=<? ehu($session); ?>&mode=<? ehu($mode); ?>&target=p|<? ehu($patients[$date][0]["ptif_id"]); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');">
<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($notes["p"][$patients[$date][0]["ptif_id"]]); ?></font>
</td>
<? } else { ?>
<td nowrap>
<input type="button" value="����" onclick="window.open('bed_menu_schedule_out_detail.php?session=<? ehu($session); ?>&mode=<? ehu($mode); ?>&target=p|<? ehu($patients[$date][0]["ptif_id"]); ?>|<? ehu($patients[$date][0]["in_dttm"]); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');">
<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($notes["p"][$patients[$date][0]["ptif_id"]][$patients[$date][0]["in_dttm"]]); ?></font>
</td>
<? } ?>
<?
	} else {
		for ($j = 1; $j <= 13; $j++) {
?>
<td></td>
<?
		}
?>
<td nowrap>
<input type="button" value="����" onclick="window.open('bed_menu_schedule_out_detail.php?session=<? ehu($session); ?>&mode=<? ehu($mode); ?>&target=d|<? ehu($date); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');">
<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($notes["d"][$date]); ?></font>
</td>
<?
	}
?>
</tr>
<?
	for ($j = 1; $j < $rowspan; $j++) {
?>
<tr height="22">
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? show_label_if_new($new_start, $patients[$date][$j]["updated"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["ward_name"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["ptrm_name"]); ?></font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["short_stay"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["kanji_name"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["birth"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["age"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["sex"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["dr_nm"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? show_operators($patients[$date][$j]["operator"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["out_inst"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["out_tm"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["back_in_dttm"]); ?></font></td>
<? if ($mode == "r") { ?>
<td nowrap>
<input type="button" value="����" onclick="window.open('bed_menu_schedule_out_detail.php?session=<? ehu($session); ?>&mode=<? ehu($mode); ?>&target=p|<? ehu($patients[$date][$j]["ptif_id"]); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');">
<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($notes["p"][$patients[$date][$j]["ptif_id"]]); ?></font></td>
<? } else { ?>
<td nowrap>
<input type="button" value="����" onclick="window.open('bed_menu_schedule_out_detail.php?session=<? ehu($session); ?>&mode=<? ehu($mode); ?>&target=p|<? ehu($patients[$date][$j]["ptif_id"]); ?>|<? ehu($patients[$date][$j]["in_dttm"]); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');">
<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($notes["p"][$patients[$date][$j]["ptif_id"]][$patients[$date][$j]["in_dttm"]]); ?></font>
</td>
<? } ?>
</tr>
<?
	}
	$date = date("Ymd", strtotime("+1 day", $timestamp));
}
?>
</table>
<input type="hidden" name="session" value="<? eh($session); ?>">
<input type="hidden" name="mode" value="<? eh($mode); ?>">
<input type="hidden" name="year" value="<? eh($year); ?>">
<input type="hidden" name="month" value="<? eh($month); ?>">
<input type="hidden" name="day" value="<? eh($day); ?>">
<input type="hidden" name="term" value="<? eh($term); ?>">
</form>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td align="left" style="padding-left:10px;"><a href="javascript:void(0);" onclick="window.scrollBy(99999, 0);">��</a></td>
<td align="right" style="padding-right:10px;"><a href="javascript:void(0);" onclick="window.scrollBy(-99999, 0);">��</a></td>
<tr>
</table>

<form name="excelform" action="bed_menu_schedule_out_excel.php" method="get" target="excelframe">
<input type="hidden" name="session" value="<? eh($session); ?>">
<input type="hidden" name="mode" value="<? eh($mode); ?>">
<input type="hidden" name="year" value="<? eh($year); ?>">
<input type="hidden" name="month" value="<? eh($month); ?>">
<input type="hidden" name="day" value="<? eh($day); ?>">
<input type="hidden" name="term" value="<? eh($term); ?>">
</form>
<iframe name="excelframe" width="0" height="0" frameborder="0"></iframe>

</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function get_out_pos_master($con, $fname) {
	$sql = "select rireki_index, item_cd, item_name from tmplitemrireki";
	$cond = "where mst_cd = 'A307' and disp_flg";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		js_error_exit();
	}

	$master = array();
	while ($row = pg_fetch_array($sel)) {
		$master[$row["rireki_index"]][$row["item_cd"]] = $row["item_name"];
	}
	return $master;
}

function get_weekday($timestamp) {
	switch (date("w", $timestamp)) {
	case "0":
		return "��";
	case "1":
		return "��";
	case "2":
		return "��";
	case "3":
		return "��";
	case "4":
		return "��";
	case "5":
		return "��";
	case "6":
		return "��";
	}
}

function show_label_if_new($new_start, $updated) {
	if ($updated >= $new_start) {
?>
<img src="img/icon/new.gif" alt="NEW" width="24" height="10" style="vertical-align:middle;">
<?
	}
}

function format_checked($checked) {
	return ($checked == "t") ? "��" : "";
}

function format_date($date) {
	return preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $date);
}

function format_age($birth) {
	if ($birth == "") return "";

	$birth_yr = substr($birth, 0, 4);
	$birth_md = substr($birth, 4, 8);
	$age = date("Y") - $birth_yr;
	if ($birth_md > date("md")) {
		$age--;
	}
	return "{$age}��";
}

function format_sex($sex) {
	switch ($sex) {
	case "0":
		return "����";
	case "1":
		return "����";
	case "2":
		return "����";
	default:
		return "";
	}
}

function get_operators($con, $mode, $ptif_id, $in_dt, $in_tm, $fname) {
	if ($mode == "r") {
		$sql = "select e.emp_lt_nm from inptop";
		$cond = q("where i.ptif_id = '%s'", $ptif_id);
	} else {
		$sql = "select e.emp_lt_nm from inptophist";
		$cond = q("where i.ptif_id = '%s' and i.inpt_in_dt = '%s' and i.inpt_in_tm = '%s'", $ptif_id, $in_dt, $in_tm);
	}
	$sql .= " i inner join empmst e on e.emp_id = i.emp_id";
	$cond .= " and exists (select * from jobmst j where j.job_id = e.emp_job and j.bed_op_flg) order by i.order_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		js_error_exit();
	}

	$operators = array();
	while ($row = pg_fetch_array($sel)) {
		$operators[] = $row["emp_lt_nm"];
	}
	return $operators;
}

function show_operators($operators) {
	for ($i = 0, $j = count($operators); $i < $j; $i++) {
		if ($i > 0) {
?>
<br>
<?
		}
		eh($operators[$i]);
	}
}

function format_intro_inst($pre_div, $inst_master, $intro_inst_cd) {
	if ($inst_master[$intro_inst_cd] != "") return $inst_master[$intro_inst_cd];

	switch ($pre_div) {
	case "1":
		return "����";
	case "2":
		return "���ŵ���¾";
	default:
		return "";
	}
}

function format_datetime($date, $time) {
	$date = format_date($date);
	$time = format_time($time);
	return trim("{$date} {$time}");
}

function format_out_inst($out_pos_master, $out_pos_rireki, $out_pos_cd, $insts, $out_inst_cd) {
	if (isset($out_pos_master[$out_pos_rireki][$out_pos_cd])) {
		$out_inst = $out_pos_master[$out_pos_rireki][$out_pos_cd];
	} else {
		$out_inst = "";
	}

	if (isset($insts[$out_inst_cd])) {
		if ($out_inst != "") {
			$out_inst .= "��";
		}
		$out_inst .= $insts[$out_inst_cd];
	}

	return $out_inst;
}

function format_time($time) {
	return preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $time);
}
