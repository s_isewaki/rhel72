<?php

ob_start();

require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("show_select_values.ini");
require("referer_common.ini");
require("get_values.ini");
require("./conf/sql.inf");
require("show_class_name.ini");

//require_once("jnl_application_common.ini");
//require("jnl_application_imprint_common.ini");

$fname = $PHP_SELF;

// セッションのチェック

$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,31,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, 82, $fname);

$con = connect2db($fname);

// 職員ID取得
$emp_id=get_emp_id($con,$session,$fname);


if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
	$encoding = 'sjis';
}
else {
	$encoding = mb_http_output();   // Firefox
}

$stamp = time();

$date = date('YmdHis', $stamp);


//$fileName = "委員会・WG名簿_".$date.".xls";
$fileName = "CommitteeMemberList_".$date.".xls";
if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
	$encoding = 'sjis';
} else {
	$encoding = mb_http_output();   // Firefox
}
$fileName = mb_convert_encoding(
		$fileName,
		$encoding,
		mb_internal_encoding());

$fileName = mb_convert_encoding($fileName, $encoding, mb_internal_encoding());
ob_clean();
header("Content-Type: application/vnd.ms-excel");
header(sprintf('Content-Disposition: attachment; filename=%s', $fileName));
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');

$download_data = setData($fname,$con,$r_level,$search_rsp_user_name,$ch_end_date,$search_user_name,$class_id,$atrb_id,$search_working_name,$search_project_name);

echo mb_convert_encoding(
		nl2br($download_data),
		'sjis',
		mb_internal_encoding()
		);

ob_end_flush();

pg_close($con); 






/**
 * /エクセル出力用データセット
 *
 * @param mixed $con This is a description
 * @param mixed $r_level This is a description
 * @param mixed $search_rsp_user_name This is a description
 * @param mixed $ch_end_date This is a description
 * @param mixed $search_user_name This is a description
 * @param mixed $class_id This is a description
 * @param mixed $atrb_id This is a description
 * @param mixed $search_working_name This is a description
 * @param mixed $search_project_name This is a description
 * @return mixed This is the return value description
 *
 */
function setData
($fname,$con,$r_level="",$search_rsp_user_name="",$ch_end_date="",$search_user_name="",$class_id="",$atrb_id="",$search_working_name="",$search_project_name="")
{
	
	$arr_class_name = get_class_name_array($con, $fname);
	
	//エクセル出力用変数
	$dispSetdata = "";	
	
	//出力用構造体
	$disp_data = array();
	$disp_cnt = -1;
	$color_number = 1;
	
	if($r_level == "5")
	{
		//責任者の検索の場合は別ＳＱＬにしました
		
		//検索条件にメンバー名が指定されている場合
		$query_name = " and   (rspemp.emp_lt_nm || rspemp.emp_ft_nm like '%".$search_rsp_user_name."%' or 
				rspemp.emp_kn_lt_nm || rspemp.emp_kn_ft_nm like '%".$search_rsp_user_name."%') ";
		
		if ($ch_end_date == "1") 
		{
			$pjt_end_date = " and (project.pjt_real_end_date = '' or project.pjt_real_end_date is null) ";
		}
		else
		{
			$pjt_end_date = " ";
		}
		
		// 委員会,ワーキング情報を取得
		$sql = "select pjt_id as id, pjt_name as community, 
				CASE WHEN pjt_parent_id is null THEN  0 ELSE  1 END as working_flg, a.wg_sort_id,
				class_nm as struc1, atrb_nm as struc2, pjt_response as res_id, rsp_lt_nm, rsp_ft_nm, pp.com_name
				from
				(select project.pjt_id, project.pjt_name, project.pjt_delete_flag, project.pjt_parent_id, project.wg_sort_id, 
				project.class_id , project.atrb_id, project.pjt_response,  project.pjt_reg_emp_id,
				rspemp.emp_lt_nm as rsp_lt_nm, rspemp.emp_ft_nm as rsp_ft_nm
				from project 
				inner join empmst rspemp on project.pjt_response = rspemp.emp_id 
				where pjt_delete_flag = 'f' "
			.$pjt_end_date.$query_name."  order by project.pjt_sort_id ,project.wg_sort_id ,project.class_id ,project.atrb_id ) a
				left join classmst on a.class_id = classmst.class_id
				left join atrbmst on a.atrb_id = atrbmst.atrb_id
				left join (select pjt_id as wid ,pjt_name as com_name, pjt_sort_id as sid from project) pp on a.pjt_parent_id = pp.wid
				";
		$cond = " where pjt_delete_flag = 'f' order by pp.sid,a.wg_sort_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == "0") {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		
		while ($row = pg_fetch_array($sel)) 
		{
			//委員会のメンバー情報を取得し、表示用配列に代入する
			
			$disp_cnt++;
			
			$disp_data[$disp_cnt]["struc1"] = $row["struc1"];		//組織１
			$disp_data[$disp_cnt]["struc2"] = $row["struc2"];		//組織２
			$disp_data[$disp_cnt]["rsp_name"] = $row["rsp_lt_nm"].$row["rsp_ft_nm"];		//責任者
			
			if($row["working_flg"] == "0")
			{
				//委員会名
				$disp_data[$disp_cnt]["community"] = $row["community"];	//委員会名
				$disp_data[$disp_cnt]["working"] = "";	//ワーキング名
			}
			else
			{
				//ワーキング名
				$disp_data[$disp_cnt]["community"] = $row["com_name"];//ワーキングが所属する委員会名
				$disp_data[$disp_cnt]["working"] = $row["community"];//ワーキング名
			}
			
			if($color_number % 2 ==0)
			{
				$disp_data[$disp_cnt]["bgcolor"] = "FFFCC8"; //背景色
			}
			else
			{
				$disp_data[$disp_cnt]["bgcolor"] = "FFD1A4"; //背景色
			}
			$color_number++;
		}
	}
	else
	{
		
		////////////////////////////////////////
		////////条件検索に設定するSQLを作成する
		////////////////////////////////////////
		
		if($ch_user_name == "1")
		{
			//検索条件にメンバー名が指定されている場合
			$query_name = " and   (empmst.emp_lt_nm || empmst.emp_ft_nm like '%".$search_user_name."%' or 
					empmst.emp_kn_lt_nm || empmst.emp_kn_ft_nm like '%".$search_user_name."%') ";
		}
		else
		{
			//検索条件にメンバー名が指定されていない場合
			$query_name = " ";
		}
		
		if ($ch_end_date == "1") 
		{
			$pjt_end_date = " and (project.pjt_real_end_date = '' or project.pjt_real_end_date is null) ";
		}
		else
		{
			$pjt_end_date = " ";
		}
		
		switch ($r_level) 
		{
			case "1"://条件指定なし
				$set_class1 = " ";
				$set_class2 = " ";
				$set_community = " ";
				$set_working = " ";
				break;
			
			case "2"://組織指定
				if($class_id =="")//第一階層が指定されていない場合はSQLにis nullを記述
					{$set_class1 = "and project.class_id is null ";}
				else
					{$set_class1 = "and project.class_id='".$class_id."' ";}
				
				if($atrb_id =="")//第二階層が指定されていない場合はSQLにis nullを記述
					{$set_class2 = "and project.atrb_id is null ";	}
				else
					{$set_class2 = "and project.atrb_id='".$atrb_id."' ";}
				
				$set_community = " ";
				$set_working = " ";
				
				break;
			
			case "3":
				if($class_id =="")//第一階層が指定されていない場合はSQLにis nullを記述
					{$set_class1 = "and project.class_id is null ";}
				else
					{$set_class1 = "and project.class_id='".$class_id."' ";}
				
				if($atrb_id =="")//第二階層が指定されていない場合はSQLにis nullを記述
					{$set_class2 = "and project.atrb_id is null ";	}
				else
					{$set_class2 = "and project.atrb_id='".$atrb_id."' ";}
				
				$set_community = "and project.pjt_name like '%".$search_project_name."%' ";
				$set_working = " ";
				
				break;
			
			case "4":
				if($class_id =="")//第一階層が指定されていない場合はSQLにis nullを記述
					{$set_class1 = "and project.class_id is null ";}
				else
					{$set_class1 = "and project.class_id='".$class_id."' ";}
				
				if($atrb_id =="")//第二階層が指定されていない場合はSQLにis nullを記述
					{$set_class2 = "and project.atrb_id is null ";}
				else
					{$set_class2 = "and project.atrb_id='".$atrb_id."' ";}
				
				$set_community = " ";
				$set_working = "and project.pjt_name like '%".$search_working_name."%' ";
				
				break;
			
			default:
				$set_class1 = " ";
				$set_class2 = " ";
				$set_community = " ";
				$set_working = " ";
				
				break;
		}
		
		////////////////////////////////////////
		////////作成したSQLを実行する
		////////////////////////////////////////
		
		// 委員会情報を取得
		
		$sql = "select a.pjt_id as id, pjt_name as community, pjt_parent_id as parent_id ,
				classmst.class_nm as struc1, atrbmst.atrb_nm as struc2, pjt_response as res_id, rsp_lt_nm, rsp_ft_nm 
				from
				(select project.pjt_id, project.pjt_name, project.pjt_parent_id, project.pjt_delete_flag, 
				project.class_id , project.atrb_id, project.pjt_response, 
				rspemp.emp_lt_nm as rsp_lt_nm, rspemp.emp_ft_nm as rsp_ft_nm
				from project 
				inner join empmst rspemp on project.pjt_response = rspemp.emp_id 
				where pjt_parent_id is null and pjt_delete_flag = 'f' "
			.$pjt_end_date.$set_class1.$set_class2.$set_community."  order by project.pjt_sort_id, project.class_id ,project.atrb_id ) a
				left join classmst on a.class_id = classmst.class_id
				left join atrbmst on a.atrb_id = atrbmst.atrb_id 
				";
				
		$cond = " where pjt_parent_id is null and pjt_delete_flag = 'f' order by struc1 desc, struc2 desc";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == "0") {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		
		$www = pg_num_rows($sel);
		
		while ($row = pg_fetch_array($sel)) 
		{

			//委員会の議事録承認者を取得
			$sql = "select pjt_id, empmst.emp_lt_nm as ap_lt_nm, empmst.emp_ft_nm as ap_ft_nm 
					from prcdaprv_default 
					left join empmst on prcdaprv_default.prcdaprv_emp_id = empmst.emp_id
					where prcdaprv_default.pjt_id = ".$row["id"]."";
			$cond = "";
			
			$sel_ap = select_from_table($con, $sql, $cond, $fname);
			if ($sel_ap == "0") {
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			
			// メンバー情報を取得〜配列に格納(委員会)
			$sql = "select a.emp_id, a.emp_lt_nm, a.emp_ft_nm, a.member_kind , 
					case a.member_kind when '1' then '事務局' else 'メンバー' end 
					from (select promember.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, promember.member_kind
					from promember inner join empmst on promember.emp_id = empmst.emp_id 
					".$query_name."			
					where promember.pjt_id = '".$row["id"]."' ) a ";
			$cond = "";
			
			$sel2 = select_from_table($con, $sql, $cond, $fname);
			if ($sel2 == "0") {
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			
			if($r_level < 4)//もし検索条件にＷＧ名が指定されていたら委員会情報は表示しない
			{
				if(pg_num_rows($sel2) == 0)//メンバーが一人も存在しなかった場合は委員会情報のみを表示する
				{
					
					if($ch_user_name != "1")//メンバーが存在していなくて、検索条件でメンバー名を指定していたら表示しない
					{
						
						$disp_cnt++;
						
						$disp_data[$disp_cnt]["comid"] = $row["id"];		//委員会ＩＤ
						$disp_data[$disp_cnt]["struc1"] = $row["struc1"];		//組織１
						$disp_data[$disp_cnt]["struc2"] = $row["struc2"];		//組織２
						$disp_data[$disp_cnt]["community"] = $row["community"];	//委員会名
						$disp_data[$disp_cnt]["working"] = "";	//ワーキング名
						$disp_data[$disp_cnt]["rsp_name"] = $row["rsp_lt_nm"].$row["rsp_ft_nm"];		//責任者
						
						if($color_number % 2 ==0)
						{
							$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor1; //背景色
						}
						else
						{
							$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor2; //背景色
						}
						
						$ap_cnt = 0;
						while ($row_sel_ap = pg_fetch_array($sel_ap)) 
						{
							//承認者エリアに担当者名を設定
							
							if($ap_cnt == 0)
							{
								$disp_data[$disp_cnt]["approver"] = $row_sel_ap["ap_lt_nm"].$row_sel_ap["ap_ft_nm"];	//承認者
							}
							else
							{
								$disp_data[$disp_cnt]["approver"] = $disp_data[$disp_cnt]["approver"].",".$row_sel_ap["ap_lt_nm"].$row_sel_ap["ap_ft_nm"];	//承認者
							}
							
							$ap_cnt++;
						}
						
						$color_number++;
					}
				}
				else
				{
					//委員会のメンバー情報を取得し、表示用配列に代入する
					
					$disp_cnt++;
					
					$disp_data[$disp_cnt]["comid"] = $row["id"];		//委員会ＩＤ
					$disp_data[$disp_cnt]["struc1"] = $row["struc1"];		//組織１
					$disp_data[$disp_cnt]["struc2"] = $row["struc2"];		//組織２
					$disp_data[$disp_cnt]["community"] = $row["community"];	//委員会名
					$disp_data[$disp_cnt]["working"] = "";	//ワーキング名
					$disp_data[$disp_cnt]["rsp_name"] = $row["rsp_lt_nm"].$row["rsp_ft_nm"];		//責任者
					
					//事務局メンバー数
					$secretariat_cnt = 0;
					
					//一般メンバー数
					$gen_member_cnt = 0;
					
					while ($row2 = pg_fetch_array($sel2)) 
					{
						//委員会のメンバー情報を取得し、表示用配列に代入する
						
						if($row2["case"] == "事務局")
						{
							//事務局エリアに担当者名を設定
							
							if($secretariat_cnt == 0)
							{
								$disp_data[$disp_cnt]["sec_mem_name"] = $row2["emp_lt_nm"].$row2["emp_ft_nm"];		//担当者
							}
							else
							{
								$disp_data[$disp_cnt]["sec_mem_name"] = $disp_data[$disp_cnt]["sec_mem_name"].",".$row2["emp_lt_nm"].$row2["emp_ft_nm"];		//担当者
							}
							
							$secretariat_cnt++;
						}
						else
						{
							//メンバーエリアに担当者名を設定
							
							if($gen_member_cnt == 0)
							{
								$disp_data[$disp_cnt]["gen_mem_name"] = $row2["emp_lt_nm"].$row2["emp_ft_nm"];		//担当者
							}
							else
							{
								$disp_data[$disp_cnt]["gen_mem_name"] = $disp_data[$disp_cnt]["gen_mem_name"].",".$row2["emp_lt_nm"].$row2["emp_ft_nm"];		//担当者
							}
							
							$gen_member_cnt++;
						}
					}
					
					$ap_cnt = 0;
					while ($row_sel_ap = pg_fetch_array($sel_ap)) 
					{
						//承認者エリアに担当者名を設定
						
						if($ap_cnt == 0)
						{
							$disp_data[$disp_cnt]["approver"] = $row_sel_ap["ap_lt_nm"].$row_sel_ap["ap_ft_nm"];	//承認者
						}
						else
						{
							$disp_data[$disp_cnt]["approver"] = $disp_data[$disp_cnt]["approver"].",".$row_sel_ap["ap_lt_nm"].$row_sel_ap["ap_ft_nm"];	//承認者
						}
						
						$ap_cnt++;
					}
					
					if($color_number % 2 ==0)
					{
						$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor1; //背景色
					}
					else
					{
						$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor2; //背景色
					}
				}
				
				if(pg_num_rows($sel4) > 0)
				{
					$color_number++;
				}
			}
				
			// 委員会に紐づいたWG情報を取得
			$sql = "select pjt_id as id, pjt_name as community, pjt_parent_id as parent_id ,
					classmst.class_nm as struc1, atrbmst.atrb_nm as struc2, pjt_response as res_id, rsp_lt_nm, rsp_ft_nm 
					from
					(select project.pjt_id, project.pjt_name, project.pjt_parent_id, project.pjt_delete_flag,  
					project.class_id , project.atrb_id, project.pjt_response,
					rspemp.emp_lt_nm as rsp_lt_nm, rspemp.emp_ft_nm as rsp_ft_nm, project.wg_sort_id
					from project 
					inner join empmst rspemp on project.pjt_response = rspemp.emp_id 
					where pjt_parent_id = '".$row["id"]."' and pjt_delete_flag = 'f' "
				.$pjt_end_date.$set_working." order by project.wg_sort_id, project.class_id ,project.atrb_id) a
					left join classmst on a.class_id = classmst.class_id
					left join atrbmst on a.atrb_id = atrbmst.atrb_id
					";
			$cond = " where pjt_delete_flag = 'f'";
			$sel3 = select_from_table($con, $sql, $cond, $fname);
			if ($sel3 == "0") {
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			
			while ($row3 = pg_fetch_array($sel3)) 
			{

				//ワーキングの議事録承認者を取得
				$sql = "select pjt_id, empmst.emp_lt_nm as ap_lt_nm, empmst.emp_ft_nm as ap_ft_nm 
						from prcdaprv_default 
						left join empmst on prcdaprv_default.prcdaprv_emp_id = empmst.emp_id
						where prcdaprv_default.pjt_id = ".$row3["id"]."";
				$cond = "order by prcdaprv_order, prcdaprv_no";
				
				$sel_ap = select_from_table($con, $sql, $cond, $fname);
				if ($sel_ap == "0") {
					pg_close($con);
					echo("<script type='text/javascript' src='./js/showpage.js'></script>");
					echo("<script language='javascript'>showErrorPage(window);</script>");
					exit;
				}

				//WK情報を取得し、表示用配列に代入する
				
				// メンバー情報を取得〜配列に格納(ＷＧ)
				
				$sql = "select a.emp_id, a.emp_lt_nm, a.emp_ft_nm, a.member_kind , 
						case a.member_kind when '1' then '事務局' else 'メンバー' end 
						from (select promember.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, promember.member_kind
						from promember inner join empmst on promember.emp_id = empmst.emp_id 
						".$query_name."			
						where promember.pjt_id = '".$row3["id"]."' ) a ";
				$cond = "";
				
				$sel4 = select_from_table($con, $sql, $cond, $fname);
				if ($sel4 == "0") {
					pg_close($con);
					echo("<script type='text/javascript' src='./js/showpage.js'></script>");
					echo("<script language='javascript'>showErrorPage(window);</script>");
					exit;
				}
				
				if(pg_num_rows($sel4) == 0)//メンバーが一人も存在しなかった場合は委員会情報のみを表示する
				{
					
					if($ch_user_name != "1")//メンバーが存在していなくて、検索条件でメンバー名を指定していたら表示しない
					{
						
						$disp_cnt++;
						
						$disp_data[$disp_cnt]["comid"] = $row["id"];		//委員会ＩＤ
						$disp_data[$disp_cnt]["wgid"] = $row3["id"];		//ワーキングＩＤ
						$disp_data[$disp_cnt]["struc1"] = $row["struc1"];		//組織１
						$disp_data[$disp_cnt]["struc2"] = $row["struc2"];		//組織２
						$disp_data[$disp_cnt]["community"] =  $row["community"];	//委員会名
						$disp_data[$disp_cnt]["working"] = $row3["community"];	//ワーキング名
						$disp_data[$disp_cnt]["rsp_name"] = $row3["rsp_lt_nm"].$row3["rsp_ft_nm"];		//責任者
						
						if($color_number % 2 ==0)
						{
							$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor1; //背景色
						}
						else
						{
							$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor2; //背景色
						}
						
						$ap_cnt = 0;
						while ($row_sel_ap = pg_fetch_array($sel_ap)) 
						{
							//承認者エリアに担当者名を設定
							
							if($ap_cnt == 0)
							{
								$disp_data[$disp_cnt]["approver"] = $row_sel_ap["ap_lt_nm"].$row_sel_ap["ap_ft_nm"];	//承認者
							}
							else
							{
								$disp_data[$disp_cnt]["approver"] = $disp_data[$disp_cnt]["approver"].",".$row_sel_ap["ap_lt_nm"].$row_sel_ap["ap_ft_nm"];	//承認者
							}
							
							$ap_cnt++;
						}
						$color_number++;
					}
				}
				else
				{
					
					//委員会のメンバー情報を取得し、表示用配列に代入する
					
					$disp_cnt++;
					
					$disp_data[$disp_cnt]["comid"] = $row["id"];		//委員会ＩＤ
					$disp_data[$disp_cnt]["wgid"] = $row3["id"];		//ワーキングＩＤ
					$disp_data[$disp_cnt]["struc1"] = $row["struc1"];		//組織１
					$disp_data[$disp_cnt]["struc2"] = $row["struc2"];		//組織２
					$disp_data[$disp_cnt]["community"] =  $row["community"];	//委員会名
					$disp_data[$disp_cnt]["working"] = $row3["community"];	//ワーキング名
					$disp_data[$disp_cnt]["rsp_name"] = $row3["rsp_lt_nm"].$row3["rsp_ft_nm"];		//責任者

					//事務局メンバー数
					$secretariat_cnt = 0;
					
					//一般メンバー数
					$gen_member_cnt = 0;

					while ($row4 = pg_fetch_array($sel4)) 
					{
						
						//委員会のメンバー情報を取得し、表示用配列に代入する
						
						if($row4["case"] == "事務局")
						{
							//事務局エリアに担当者名を設定
							
							if($secretariat_cnt == 0)
							{
								$disp_data[$disp_cnt]["sec_mem_name"] = $row4["emp_lt_nm"].$row4["emp_ft_nm"];		//担当者
							}
							else
							{
								$disp_data[$disp_cnt]["sec_mem_name"] = $disp_data[$disp_cnt]["sec_mem_name"].",".$row4["emp_lt_nm"].$row4["emp_ft_nm"];		//担当者
							}
							$secretariat_cnt++;
						}
						else
						{
							//メンバーエリアに担当者名を設定
							
							if($gen_member_cnt == 0)
							{
								$disp_data[$disp_cnt]["gen_mem_name"] = $row4["emp_lt_nm"].$row4["emp_ft_nm"];		//担当者
							}
							else
							{
								$disp_data[$disp_cnt]["gen_mem_name"] = $disp_data[$disp_cnt]["gen_mem_name"].",".$row4["emp_lt_nm"].$row4["emp_ft_nm"];		//担当者
							}
							$gen_member_cnt++;
						}
					}

					$ap_cnt = 0;
					while ($row_sel_ap = pg_fetch_array($sel_ap)) 
					{
						//承認者エリアに担当者名を設定
						
						if($ap_cnt == 0)
						{
							$disp_data[$disp_cnt]["approver"] = $row_sel_ap["ap_lt_nm"].$row_sel_ap["ap_ft_nm"];	//承認者
						}
						else
						{
							$disp_data[$disp_cnt]["approver"] = $disp_data[$disp_cnt]["approver"].",".$row_sel_ap["ap_lt_nm"].$row_sel_ap["ap_ft_nm"];	//承認者
						}
						
						$ap_cnt++;
					}
					
					if($color_number % 2 ==0)
					{
						$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor1; //背景色
					}
					else
					{
						$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor2; //背景色
					}
					
				}
				
				if(pg_num_rows($sel4) > 0)
				{
					$color_number++;
				}
			}
		}
	}
	
	$data = "";
	// metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
	$data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";
	
	$data .= "<table border=1 >";

//ヘッダ行表示
	$data .= "<tr align=\"center\">";
	$data .= "<td bgcolor=\"#defafa\" ><font size=\"4\">組織１</font></td>";
	$data .= "<td bgcolor=\"#defafa\" ><font size=\"4\">組織２</font></td>";
	$data .= "<td bgcolor=\"#defafa\" ><font size=\"4\">委員会</font></td>";
	$data .= "<td bgcolor=\"#defafa\" ><font size=\"4\">ワーキング</font></td>";	
	$data .= "<td bgcolor=\"#defafa\" ><font size=\"4\">責任者</font></td>";
	
	if($r_level != "5")//責任者検索の場合は以下の表示は必要なし 
	{
		$data .= "<td bgcolor=\"#defafa\" ><font size=\"4\">事務局</font></td>";
		$data .= "<td bgcolor=\"#defafa\" ><font size=\"4\">メンバー</font></td>";
		$data .= "<td bgcolor=\"#defafa\" ><font size=\"4\">議事録承認者</font></td>";
	}
	
	$data .= "</tr>";
	
	
//実データ表示	
	for ($i = 0; $i <= $disp_cnt; $i++) 
	{
		$data .= "<tr align=\"center\">";
		
		$data .= "<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$disp_data[$i]["struc1"]."</font></td>";
		$data .= "<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$disp_data[$i]["struc2"]."</font></td>";
		$data .= "<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$disp_data[$i]["community"]."</font></td>";
		$data .= "<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$disp_data[$i]["working"]."</font></td>";
		$data .= "<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$disp_data[$i]["rsp_name"]."</font></td>";

		if($r_level != "5")//責任者検索の場合は以下の表示は必要なし 
		{
			$data .= "<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$disp_data[$i]["sec_mem_name"]."</font></td>";
			$data .= "<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$disp_data[$i]["gen_mem_name"]."</font></td>";
			$data .= "<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$disp_data[$i]["approver"]."</font></td>";
		}

		$data .= "</tr>";
	}

	$data .= "</table>";

	return $data;	
	

	
}

?>
