<?
require_once("cl_common_log_class.inc");
require_once("Cmx.php");
require_once("MDB2.php");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//##########################################################################
// データベース接続オブジェクト取得
//##########################################################################
$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
}
$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

require_once("cl/model/workflow/empmst_model.php");
$emp_model = new empmst_model($mdb2,$emp_id);
$log->debug("職員マスタ用DBA取得",__FILE__,__LINE__);

$arr_emp = $emp_model->select_login_emp($session);
$emp_id  = $arr_emp["emp_id"];
$lt_nm  = $arr_emp["emp_lt_nm"];
$ft_nm  = $arr_emp["emp_ft_nm"];

require_once("cl/model/search/cl_training_progress_manager_model.php");
$progress_manager_model = new cl_training_progress_manager_model($mdb2,$emp_id);
$log->debug("研修受講進捗用DBA取得",__FILE__,__LINE__);

require_once("cl/model/master/cl_mst_nurse_education_committee_model.php");
$education_committee_model = new cl_mst_nurse_education_committee_model($mdb2,$emp_id);
$log->debug("看護教育委員会DBA取得",__FILE__,__LINE__);

require_once("cl/model/master/cl_mst_setting_council_model.php");
$council_model = new cl_mst_setting_council_model($mdb2,$emp_id);
$log->debug("審議会DBA取得",__FILE__,__LINE__);

require_once("cl/model/master/cl_mst_nurse_manager_st_model.php");
$manager_st_model = new cl_mst_nurse_manager_st_model($mdb2,$emp_id);
$log->debug("所属長DBA取得",__FILE__,__LINE__);

require_once("cl/model/master/cl_mst_supervisor_st_model.php");
$supervisor_st_model = new cl_mst_supervisor_st_model($mdb2,$emp_id);
$log->debug("看護部長DBA取得",__FILE__,__LINE__);

if ($_POST['regist_flg'] == 'true') {

	// トランザクション開始
	$mdb2->beginTransaction();
	$log->debug("トランザクション開始",__FILE__,__LINE__);

	define("DELETE_FLAG_PREFIX","delete_flag");

	$arr_field_name = array_keys($_POST);
	foreach($arr_field_name as $field_name){
		$pos = strpos($field_name,DELETE_FLAG_PREFIX);
		if ($pos === 0) {
			$arr_training_id = substr($field_name,strlen(DELETE_FLAG_PREFIX));
			if ($_POST[$field_name] != "") {
				$training_model->logical_delete($_POST[$field_name]);
			}
		}
	}

	// コミット
	$mdb2->commit();
	$log->debug("コミット",__FILE__,__LINE__);

}

$select_flg = true;

// 看護教育委員会チェック
$education_committee = $education_committee_model->check_education_committee($emp_id);

// ログインユーザーが看護教育委員会でない場合
if (count($education_committee) == 0){
	// 審議会チェック
	$council = $council_model->check_council($emp_id);

	// ログインユーザーが看護教育委員会・審議会で無い場合
	if (count($council) == 0){
		// 所属長(役職)チェック
		$supervisor = $supervisor_st_model->check_supervisor($emp_id);

		if (count($supervisor) > 0){
			// 所属長(部署)取得
			$supervisor_class = $progress_manager_model->get_supervisor_class();
		}

		// 看護部長(役職)チェック
		$nurse_manager = $manager_st_model->check_nurse_manager($emp_id);

		if (count($nurse_manager) > 0){
			// 看護部長(部署)取得
			$nurse_manager_class = $progress_manager_model->get_nurse_manager_class();
		}

		if (count($supervisor) == 0 && count($nurse_manager) == 0){
			$select_flg = false;
		} else {
			if(count($supervisor) == 0){
				$class_division = $nurse_manager_class['class_division'];
			} else if(count($nurse_manager) == 0){
				$class_division = $supervisor_class['class_division'];
			} else {
				if ($nurse_manager_class['class_division'] > $supervisor_class['class_division']){
					$class_division = $supervisor_class['class_division'];
				} else {
					$class_division = $nurse_manager_class['class_division'];
				}
			}
			$login_emp_id = $emp_id;
		}

	}
}

if ($select_flg) {
	if (
		$_POST['date_apply_from_y'] == ''
		|| $_POST['date_apply_from_m'] == ''
		|| $_POST['date_apply_from_d'] == ''
		|| $_POST['date_apply_from_y'] == '-'
		|| $_POST['date_apply_from_m'] == '-'
		|| $_POST['date_apply_from_d'] == '-'
	) {
		$apply_date_from = '';
	} else {
		$apply_date_from = $_POST['date_apply_from_y'].$_POST['date_apply_from_m'].$_POST['date_apply_from_d'];
	}

	if (
		$_POST['date_apply_to_y'] == ''
		|| $_POST['date_apply_to_m'] == ''
		|| $_POST['date_apply_to_d'] == ''
		|| $_POST['date_apply_to_y'] == '-'
		|| $_POST['date_apply_to_m'] == '-'
		|| $_POST['date_apply_to_d'] == '-'
	) {
		$apply_date_to = '';
	} else {
		$apply_date_to = $_POST['date_apply_to_y'].$_POST['date_apply_to_m'].$_POST['date_apply_to_d'];
	}

	if (
		$_POST['date_students_from_y'] == ''
		|| $_POST['date_students_from_m'] == ''
		|| $_POST['date_students_from_d'] == ''
		|| $_POST['date_students_from_y'] == '-'
		|| $_POST['date_students_from_m'] == '-'
		|| $_POST['date_students_from_d'] == '-'
	) {
		$students_date_from = '';
	} else {
		$students_date_from = $_POST['date_students_from_y'].$_POST['date_students_from_m'].$_POST['date_students_from_d'];
	}

	if (
		$_POST['date_students_to_y'] == ''
		|| $_POST['date_students_to_m'] == ''
		|| $_POST['date_students_to_d'] == ''
		|| $_POST['date_students_to_y'] == '-'
		|| $_POST['date_students_to_m'] == '-'
		|| $_POST['date_students_to_d'] == '-'
	) {
		$students_date_to = '';
	} else {
		$students_date_to = $_POST['date_students_to_y'].$_POST['date_students_to_m'].$_POST['date_students_to_d'];
	}

	if (
		$_POST['date_eval_plan_from_y'] == ''
		|| $_POST['date_eval_plan_from_m'] == ''
		|| $_POST['date_eval_plan_from_d'] == ''
		|| $_POST['date_eval_plan_from_y'] == '-'
		|| $_POST['date_eval_plan_from_m'] == '-'
		|| $_POST['date_eval_plan_from_d'] == '-'
	) {
		$eval_plan_date_from = '';
	} else {
		$eval_plan_date_from = $_POST['date_eval_plan_from_y'].$_POST['date_eval_plan_from_m'].$_POST['date_eval_plan_from_d'];
	}

	if (
		$_POST['date_eval_plan_to_y'] == ''
		|| $_POST['date_eval_plan_to_m'] == ''
		|| $_POST['date_eval_plan_to_d'] == ''
		|| $_POST['date_eval_plan_to_y'] == '-'
		|| $_POST['date_eval_plan_to_m'] == '-'
		|| $_POST['date_eval_plan_to_d'] == '-'
	) {
		$eval_plan_date_to = '';
	} else {
		$eval_plan_date_to = $_POST['date_eval_plan_to_y'].$_POST['date_eval_plan_to_m'].$_POST['date_eval_plan_to_d'];
	}

	if (
		$_POST['date_report_from_y'] == ''
		|| $_POST['date_report_from_m'] == ''
		|| $_POST['date_report_from_d'] == ''
		|| $_POST['date_report_from_y'] == '-'
		|| $_POST['date_report_from_m'] == '-'
		|| $_POST['date_report_from_d'] == '-'
	) {
		$report_date_from = '';
	} else {
		$report_date_from = $_POST['date_report_from_y'].$_POST['date_report_from_m'].$_POST['date_report_from_d'];
	}

	if (
		$_POST['date_report_to_y'] == ''
		|| $_POST['date_report_to_m'] == ''
		|| $_POST['date_report_to_d'] == ''
		|| $_POST['date_report_to_y'] == '-'
		|| $_POST['date_report_to_m'] == '-'
		|| $_POST['date_report_to_d'] == '-'
	) {
		$report_date_to = '';
	} else {
		$report_date_to = $_POST['date_report_to_y'].$_POST['date_report_to_m'].$_POST['date_report_to_d'];
	}

	if (
		$_POST['date_eval_from_y'] == ''
		|| $_POST['date_eval_from_m'] == ''
		|| $_POST['date_eval_from_d'] == ''
		|| $_POST['date_eval_from_y'] == '-'
		|| $_POST['date_eval_from_m'] == '-'
		|| $_POST['date_eval_from_d'] == '-'
	) {
		$eval_date_from = '';
	} else {
		$eval_date_from = $_POST['date_eval_from_y'].$_POST['date_eval_from_m'].$_POST['date_eval_from_d'];
	}

	if (
		$_POST['date_eval_to_y'] == ''
		|| $_POST['date_eval_to_m'] == ''
		|| $_POST['date_eval_to_d'] == ''
		|| $_POST['date_eval_to_y'] == '-'
		|| $_POST['date_eval_to_m'] == '-'
		|| $_POST['date_eval_to_d'] == '-'
	) {
		$eval_date_to = '';
	} else {
		$eval_date_to = $_POST['date_eval_to_y'].$_POST['date_eval_to_m'].$_POST['date_eval_to_d'];
	}

	$param = array(
		'now_level'				=> $_POST['now_level']
		,'emp_nm'				=> $_POST['emp_nm']
		,'class_id'				=> $_POST['search_emp_class']
		,'atrb_id'				=> $_POST['search_emp_attribute']
		,'dept_id'				=> $_POST['search_emp_dept']
		,'room_id'				=> $_POST['search_emp_room']
		,'training_id'			=> $_POST['training_id']
		,'training_name'		=> $_POST['training_name']
		,'teacher_name'			=> $_POST['teacher_name']
		,'apply_date_from'		=> $apply_date_from
		,'apply_date_to'		=> $apply_date_to
		,'students_date_from'	=> $students_date_from
		,'students_date_to'		=> $students_date_to
		,'eval_plan_date_from'	=> $eval_plan_date_from
		,'eval_plan_date_to'	=> $eval_plan_date_to
		,'report_date_from'		=> $report_date_from
		,'report_date_to'		=> $report_date_to
		,'eval_date_from'		=> $eval_date_from
		,'eval_date_to'			=> $eval_date_to
		,'login_emp_id'			=> $login_emp_id
		,'class_division'		=> $class_division
		,'training_div'			=> $training_div
	);

	// 一覧取得
	if($_POST['user_csv_flg'] == 'true') {
		$data = $progress_manager_model->get_user_csv_list($param);
	} else {
		$data = $progress_manager_model->get_training_list($param);
	}
}

for ($i = 0; $i < count($data); $i++){

	// 氏名
	$data[$i]['emp_full_nm']  = $data[$i]['emp_lt_nm'];
	$data[$i]['emp_full_nm'] .= '　';
	$data[$i]['emp_full_nm'] .= $data[$i]['emp_ft_nm'];

	// 所属
	$data[$i]['affiliation']  = $data[$i]['class_nm'];
	$data[$i]['affiliation'] .= ' > ';
	$data[$i]['affiliation'] .= $data[$i]['atrb_nm'];
	$data[$i]['affiliation'] .= ' > ';
	$data[$i]['affiliation'] .= $data[$i]['dept_nm'];
	if ($data[$i]['room_nm'] != '') {
		$data[$i]['affiliation'] .= ' > ';
		$data[$i]['affiliation'] .= $data[$i]['room_nm'];
	}

	// レベル
	switch($data[$i]['now_level']){
		case 1:
			$data[$i]['now_level'] = 'I';
			break;
		case 2:
			$data[$i]['now_level'] = 'II';
			break;
		case 3:
			$data[$i]['now_level'] = 'III';
			break;
		case 4:
			$data[$i]['now_level'] = 'IV';
			break;
		case 5:
			$data[$i]['now_level'] = 'V';
			break;
	}

	// 講師名
	$data[$i]['teacher_name'] = '';
	if ($data[$i]['teacher1_lt_nm'] != ''){
		if ($data[$i]['teacher_name'] != ''){
			$data[$i]['teacher_name'] .= ',';
		}
		$data[$i]['teacher_name'] .= $data[$i]['teacher1_lt_nm'];
		$data[$i]['teacher_name'] .= '　';
		$data[$i]['teacher_name'] .= $data[$i]['teacher1_ft_nm'];
	}
	if ($data[$i]['teacher2_lt_nm'] != ''){
		if ($data[$i]['teacher_name'] != ''){
			$data[$i]['teacher_name'] .= ',';
		}
		$data[$i]['teacher_name'] .= $data[$i]['teacher2_lt_nm'];
		$data[$i]['teacher_name'] .= '　';
		$data[$i]['teacher_name'] .= $data[$i]['teacher2_ft_nm'];
	}
	if ($data[$i]['teacher3_lt_nm'] != ''){
		if ($data[$i]['teacher_name'] != ''){
			$data[$i]['teacher_name'] .= ',';
		}
		$data[$i]['teacher_name'] .= $data[$i]['teacher3_lt_nm'];
		$data[$i]['teacher_name'] .= '　';
		$data[$i]['teacher_name'] .= $data[$i]['teacher3_ft_nm'];
	}
	if ($data[$i]['teacher4_lt_nm'] != ''){
		if ($data[$i]['teacher_name'] != ''){
			$data[$i]['teacher_name'] .= ',';
		}
		$data[$i]['teacher_name'] .= $data[$i]['teacher4_lt_nm'];
		$data[$i]['teacher_name'] .= '　';
		$data[$i]['teacher_name'] .= $data[$i]['teacher4_ft_nm'];
	}
	if ($data[$i]['teacher5_lt_nm'] != ''){
		if ($data[$i]['teacher_name'] != ''){
			$data[$i]['teacher_name'] .= ',';
		}
		$data[$i]['teacher_name'] .= $data[$i]['teacher5_lt_nm'];
		$data[$i]['teacher_name'] .= '　';
		$data[$i]['teacher_name'] .= $data[$i]['teacher5_ft_nm'];
	}

	// 申請日
	if ($data[$i]['apply_date'] != ''){
		$apply_date_year = substr($data[$i]['apply_date'],0,4);
		$apply_date_month = substr($data[$i]['apply_date'],5,2);
		$apply_date_day = substr($data[$i]['apply_date'],8,2);
		$data[$i]['apply_date']  = $apply_date_year;
		$data[$i]['apply_date'] .= '/';
		$data[$i]['apply_date'] .= $apply_date_month;
		$data[$i]['apply_date'] .= '/';
		$data[$i]['apply_date'] .= $apply_date_day;
	}

	// 受講日
	if ($data[$i]['plan_date'] != ''){
		$plan_date_year = substr($data[$i]['plan_date'],0,4);
		$plan_date_month = substr($data[$i]['plan_date'],5,2);
		$plan_date_day = substr($data[$i]['plan_date'],8,2);
		$data[$i]['plan_date'] = $plan_date_year;
		$data[$i]['plan_date'] .= '/';
		$data[$i]['plan_date'] .= $plan_date_month;
		$data[$i]['plan_date'] .= '/';
		$data[$i]['plan_date'] .= $plan_date_day;
	} else {
		$data[$i]['plan_date'] = '';
		if ($data[$i]['from_date'] != ''){
			$from_date_year = substr($data[$i]['from_date'],0,4);
			$from_date_month = substr($data[$i]['from_date'],5,2);
			$from_date_day = substr($data[$i]['from_date'],8,2);
			$data[$i]['plan_date'] .= $from_date_year;
			$data[$i]['plan_date'] .= '/';
			$data[$i]['plan_date'] .= $from_date_month;
			$data[$i]['plan_date'] .= '/';
			$data[$i]['plan_date'] .= $from_date_day;
		}
		if ($data[$i]['to_date'] != ''){
			$to_date_year = substr($data[$i]['to_date'],0,4);
			$to_date_month = substr($data[$i]['to_date'],5,2);
			$to_date_day = substr($data[$i]['to_date'],8,2);
			$data[$i]['plan_date'] .= '・';
			$data[$i]['plan_date'] .= $to_date_year;
			$data[$i]['plan_date'] .= '/';
			$data[$i]['plan_date'] .= $to_date_month;
			$data[$i]['plan_date'] .= '/';
			$data[$i]['plan_date'] .= $to_date_day;
		}
	}

	// 報告日
	if ($data[$i]['report_date'] != ''){
		$report_date_year = substr($data[$i]['report_date'],0,4);
		$report_date_month = substr($data[$i]['report_date'],5,2);
		$report_date_day = substr($data[$i]['report_date'],8,2);
		$data[$i]['report_date']  = $report_date_year;
		$data[$i]['report_date'] .= '/';
		$data[$i]['report_date'] .= $report_date_month;
		$data[$i]['report_date'] .= '/';
		$data[$i]['report_date'] .= $report_date_day;
	}

}

// DB切断
$mdb2->disconnect();
$log->debug("DB切断",__FILE__,__LINE__);

//----------------------------------------------

//----------------------------------------------
	// 一覧取得
	if($_POST['user_csv_flg'] == 'true') {
		// 項目配列作成
		$array_csv_header = array();
		$array_csv_header[] = "職員ID";
		$array_csv_header[] = "職員氏名";
		$array_csv_header[] = "所属";
		$array_csv_header[] = "現在のラダーレベル";
		$array_csv_header[] = "経験年数";				// 看護師開始日
		$array_csv_header[] = "当院での経験年数";		// 当院就業開始日
		$array_csv_header[] = "研修名";
		$array_csv_header[] = "研修で学びたいこと";
	} else {
		// 項目配列作成
		$array_csv_header = array();
		$array_csv_header[] = "職員ID";
		$array_csv_header[] = "職員氏名";
		$array_csv_header[] = "所属";
		$array_csv_header[] = "現在のラダーレベル";
		$array_csv_header[] = "研修番号";
		$array_csv_header[] = "研修名";
		$array_csv_header[] = "講師名";
		$array_csv_header[] = "申請日";
		$array_csv_header[] = "受講日";
		$array_csv_header[] = "報告日";
	}
		// ヘッダー行
		$log->debug('■file_name   :'.$file_name	,__FILE__,__LINE__);


		// 文字コード変換
		$csv_header = array();
		foreach($array_csv_header as $tmp_csv_header){
			$csv_header[] = format_column_for_csv($tmp_csv_header);
		}

		// 配列から文字列化
		$csv_header = implode(",", $csv_header);

		$log->debug('■csv_header   :'.$csv_header	,__FILE__,__LINE__);

		// CSV中身作成
		$csv_body = "";
		foreach ($data as $Arow){
			$csv_body .= "\r\n";

			if($_POST['user_csv_flg'] == 'true') {
				$csv_body .= format_column_for_csv($Arow["emp_personal_id"]);			// 職員ID
				$csv_body .= ",". format_column_for_csv($Arow["emp_full_nm"]);			// 職員氏名
				$csv_body .= ",". format_column_for_csv($Arow["affiliation"]);			// 所属
				$csv_body .= ",". format_column_for_csv($Arow["now_level"]);			// 現在のラダーレベル
				$csv_body .= ",". format_column_for_csv($Arow["year_cnt"]);				//"経験年数";
				$csv_body .= ",". format_column_for_csv($Arow["year_cnt"]);				//"当院での経験年数";
				$csv_body .= ",". format_column_for_csv($Arow["training_name"]);		//"研修名";
				$csv_body .= ",". format_column_for_csv($Arow["learn_memo"]);			//"研修で学びたいこと";
			} else {
				$csv_body .= format_column_for_csv($Arow["emp_personal_id"]);			// 職員ID
				$csv_body .= ",". format_column_for_csv($Arow["emp_full_nm"]);			// 職員氏名
				$csv_body .= ",". format_column_for_csv($Arow["affiliation"]);			// 所属
				$csv_body .= ",". format_column_for_csv($Arow["now_level"]);			// 現在のラダーレベル
				$csv_body .= ",". format_column_for_csv($Arow["training_id"]);			//"研修番号";
				$csv_body .= ",". format_column_for_csv($Arow["training_name"]);		//"研修名";
				$csv_body .= ",". format_column_for_csv($Arow["teacher_name"]);			// 講師名
				$csv_body .= ",". format_column_for_csv($Arow["apply_date"]);			// 申請日
				$csv_body .= ",". format_column_for_csv($Arow["plan_date"]);			// 受講日
				$csv_body .= ",". format_column_for_csv($Arow["report_date"]);			// 報告日
			}

		}

		$csv = $csv_header . $csv_body;

		if($_POST['user_csv_flg'] == 'true') {
			$file_name = "progress_manager_user_list.csv";
		} else {
			$file_name = "progress_manager_training_list.csv";
		}

		$log->debug('■file_name   :'.$file_name	,__FILE__,__LINE__);

		// CSVを出力
		header("Content-Type: application/octet-stream");
		header("Content-Disposition: attachment; filename=$file_name");
		header("Content-Length: " . strlen($csv) );
		echo($csv);

		$log->debug('■csv   :'.$csv	,__FILE__,__LINE__);


//----------------------------------------------

	// 文字コード変換
	function format_column_for_csv($value, $from_encoding = "EUC-JP") {
		$buf = str_replace("\r", "", $value);
		$buf = str_replace("\n", "", $buf);
		if (strpos($buf, ",") !== false)  {
			$buf = '"' . $buf . '"';
		}
		return mb_convert_encoding($buf, "Shift_JIS", $from_encoding);
	}


$log->info(basename(__FILE__)." END");
$log->shutdown();

