<?
ob_start();
require_once("about_session.php");
require_once("about_authority.php");
ob_end_clean();

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 81, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$delete_flg = ($select_box == "DUST") ? "t" : "f";

$arr_condition = array("category"      => $category,
                       "workflow"      => $workflow,
                       "apply_title"   => $apply_title,
                       "apply_emp_nm"  => $apply_emp_nm,
                       "date_y1"       => $date_y1,
                       "date_m1"       => $date_m1,
                       "date_d1"       => $date_d1,
                       "date_y2"       => $date_y2,
                       "date_m2"       => $date_m2,
                       "date_d2"       => $date_d2,
                       "apply_stat"    => $apply_stat,
                       "class"         => $class,
                       "attribute"     => $attribute,
                       "dept"          => $dept,
                       "room"          => $room,
                       "apply_content" => $apply_content);

// データベースに接続
$con = connect2db($fname);

$apply_list = get_apply_list_for_csv($con, $delete_flg, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition, $mode, $apply_ids, $fname, $session);

// 全件出力の場合、申請書データから申請書IDリストを作成
if ($mode == "2") {
	$apply_ids = get_apply_ids_for_csv($apply_list);
}

// 添付ファイル情報を取得
$apply_files = get_apply_files_for_csv($con, $apply_ids, $fname, $session);

// 承認者情報を取得
$approves = get_approves_for_csv($con, $apply_ids, $fname, $session);

// データベース接続を閉じる
pg_close($con);

$csv_header = format_csv_header($apply_list[0], $approves[$apply_list[0]["apply_id"]]);
$csv_body   = format_csv_body($apply_list, $apply_files, $approves);
$csv        = $csv_header . "\r\n" . $csv_body;

$file_name = "workflow.csv";
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);

// 申請書一覧を取得
function get_apply_list_for_csv($con, $delete_flg, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition, $mode, $apply_ids, $fname, $session) {
	$category        = $arr_condition["category"];
	$workflow        = $arr_condition["workflow"];
	$apply_title     = $arr_condition["apply_title"];
	$apply_emp_nm    = $arr_condition["apply_emp_nm"];
	$apply_yyyy_from = $arr_condition["date_y1"];
	$apply_mm_from   = $arr_condition["date_m1"];
	$apply_dd_from   = $arr_condition["date_d1"];
	$apply_yyyy_to   = $arr_condition["date_y2"];
	$apply_mm_to     = $arr_condition["date_m2"];
	$apply_dd_to     = $arr_condition["date_d2"];
	$apply_stat      = $arr_condition["apply_stat"];
	$class           = $arr_condition["class"];
	$attribute       = $arr_condition["attribute"];
	$dept            = $arr_condition["dept"];
	$room            = $arr_condition["room"];
	$apply_content   = $arr_condition["apply_content"];

	$sql  = "select ";
	$sql .= "a.*, ";
	$sql .= "b.short_wkfw_name, ";
	$sql .= "b.wkfw_title, ";
	$sql .= "b.approve_label, ";
	$sql .= "c.emp_lt_nm, ";
	$sql .= "c.emp_ft_nm, ";
	$sql .= "d.class_nm, ";
	$sql .= "e.atrb_nm, ";
	$sql .= "f.dept_nm, ";
	$sql .= "g.room_nm ";
	$sql .= "from jnl_apply a ";
	$sql .= "left join jnl_wkfwmst b on a.wkfw_id = b.wkfw_id ";
	$sql .= "left join empmst c on a.emp_id = c.emp_id ";
	$sql .= "left join classmst d on a.emp_class = d.class_id ";
	$sql .= "left join atrbmst e on a.emp_attribute = e.atrb_id ";
	$sql .= "left join deptmst f on a.emp_dept = f.dept_id ";
	$sql .= "left join classroom g on a.emp_room = g.room_id ";

	// 申請書指定の場合
	if ($mode == "1") {
		$cond = "where a.apply_id in (" . join(",", $apply_ids) . ") ";

	// 全件出力の場合
	} else {
		$cond = "where (not a.draft_flg) and a.delete_flg = '$delete_flg' ";

		if ($selected_cate != "") {
			$cond .= "and b.wkfw_type = $selected_cate ";
		}

		if ($selected_folder != "") {
			$cond .= "and b.wkfw_folder_id = $selected_folder ";
		}

		if ($selected_wkfw_id != "") {
			$cond .= "and b.wkfw_id = $selected_wkfw_id ";
		}

		// カテゴリ
		if ($category != "" && $category != "-") {
			$cond .= "and b.wkfw_type = $category ";
		}

		// 申請書名
		if ($workflow != "" && $workflow != "-") {
			$cond .= "and b.wkfw_id = $workflow ";
		}

		// 表題
		if ($apply_title != "") {
			$apply_title = pg_escape_string($apply_title);
			$cond .= "and a.apply_title like '%$apply_title%' ";
		}

		// 申請者名
		if ($apply_emp_nm != "") {
			$apply_emp_nm = pg_escape_string($apply_emp_nm);
			$cond .= "and ";
			$cond .= "(c.emp_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_kn_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_kn_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or (c.emp_lt_nm || c.emp_ft_nm) like '%$apply_emp_nm%' ";
			$cond .= "or (c.emp_kn_lt_nm || c.emp_kn_ft_nm) like '%$apply_emp_nm%') ";
		}

		// 申請状況
		if ($apply_stat != "" && $apply_stat != "-") {
			$cond .= "and a.apply_stat = '$apply_stat' ";
		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) <= '$date_str' ";
		}

		// 部署
		if ($class != "") {
			$cond .= "and a.emp_class = $class ";
		}
		if ($attribute != "") {
			$cond .= "and a.emp_attribute = $attribute ";
		}
		if ($dept != "") {
			$cond .= "and a.emp_dept = $dept ";
		}
		if ($room != "") {
			$cond .= "and a.emp_room = $room ";
		}

		// 文字列検索
		if ($apply_content != "") {
			$search_keys = split(" ", $apply_content);
			for ($i = 0, $j = count($search_keys); $i < $j; $i++) {
				$tmp_cond[] = "a.apply_content like '%{$search_keys[$i]}%'";
			}
			$cond .= "and (" . join(" or ", $tmp_cond) . ")";
		}
	}

	$cond .= "order by a.apply_date desc, a.apply_no desc ";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	return pg_fetch_all($sel);
}

function get_apply_ids_for_csv($apply_list) {
	$apply_ids = array();
	foreach ($apply_list as $tmp_apply) {
		$apply_ids[] = $tmp_apply["apply_id"];
	}
	return $apply_ids;
}

function get_apply_files_for_csv($con, $apply_ids, $fname, $session) {
	$sql  = "select apply_id, applyfile_name from jnl_applyfile";
	$cond = "where apply_id in (" . join(",", $apply_ids) . ") order by apply_id, applyfile_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$apply_files = array();
	while ($row = pg_fetch_array($sel)) {
		$apply_files[$row["apply_id"]][] = $row["applyfile_name"];
	}
	return $apply_files;
}

function get_approves_for_csv($con, $apply_ids, $fname, $session) {
	$sql  = "select a.*, b.st_nm, c.emp_lt_nm, c.emp_ft_nm from jnl_applyapv a left join stmst b on b.st_id = a.emp_st left join empmst c on c.emp_id = a.emp_id";
	$cond = "where a.apply_id in (" . join(",", $apply_ids) . ") and (not a.delete_flg) order by a.apv_order, a.apv_sub_order";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$approves = array();
	while ($row = pg_fetch_array($sel, NULL, PGSQL_ASSOC)) {
		$approves[$row["apply_id"]][] = $row;
	}
	return $approves;
}

function format_csv_header($apply, $approves) {
	$csv = "申請番号,申請書名,表題,申請者,申請日,申請状況,所属,";

	if ($apply["wkfw_content_type"] == "1" || strpos($apply["apply_content"], "<?xml") === false) {
		$csv .= "テキスト入力";
	} else {
		$csv .= format_template_header_for_csv($apply["apply_content"]);
	}

	$csv .= ",添付ファイル,承認者コメント";

	foreach ($approves as $tmp_approve) {
		$tmp_no = $tmp_approve["apv_order"];
		if ($tmp_approve["apv_sub_order"] != "") {
			$tmp_no .= "-" . $tmp_approve["apv_sub_order"];
		}
		$csv .= ",承認者{$tmp_no}役職,承認者{$tmp_no}氏名,承認者{$tmp_no}承認状況";
	}

	return mb_convert_encoding($csv, "Shift_JIS", "EUC-JP");
}

function format_template_header_for_csv($apply_content) {
	if (!$dom = domxml_open_mem($apply_content)) {
		return "テンプレート";
	}

	$root_nodes = $dom->get_elements_by_tagname('apply');
	if (count($root_nodes) == 0) {
		return "テンプレート";
	}
	$root_node = $root_nodes[0];

	$tags = array();
	$child_nodes = $root_node->child_nodes();
	foreach ($child_nodes as $child_node) {
		if ($child_node->node_type() == XML_ELEMENT_NODE) {
			$disp_attribute = $child_node->get_attribute_node("disp");
			if ($disp_attribute) {
				$tags[] = mb_convert_encoding($disp_attribute->value(), "EUC-JP", "UTF-8");
			} else {
				$tags[] = mb_convert_encoding($child_node->tagname(), "EUC-JP", "UTF-8");
			}
		}
	}

	return join(",", $tags);
}

function format_csv_body($apply_list, $apply_files, $approves) {
	$csv = "";
	foreach ($apply_list as $tmp_apply) {
		$tmp_apply_no       = format_column_for_csv(format_apply_no_for_csv($tmp_apply["short_wkfw_name"], $tmp_apply["apply_date"], $tmp_apply["apply_no"]));
		$tmp_wkfw_title     = format_column_for_csv($tmp_apply["wkfw_title"]);
		$tmp_apply_title    = format_column_for_csv($tmp_apply["apply_title"]);
		$tmp_apply_emp_name = format_column_for_csv(format_emp_name_for_csv($tmp_apply["emp_lt_nm"], $tmp_apply["emp_ft_nm"]));
		$tmp_apply_date     = format_column_for_csv(format_date_for_csv($tmp_apply["apply_date"]));
		$tmp_apply_status   = format_column_for_csv(format_apply_status_for_csv($tmp_apply["apply_stat"], $tmp_apply["approve_label"], $approves[$tmp_apply["apply_id"]]));
		$tmp_apply_emp_dept = format_column_for_csv(format_emp_dept_for_csv($tmp_apply["class_nm"], $tmp_apply["atrb_nm"], $tmp_apply["dept_nm"], $tmp_apply["room_nm"]));
		$tmp_apply_contents = format_apply_contents_for_csv($tmp_apply["wkfw_content_type"], $tmp_apply["apply_content"]);
		$tmp_apply_file     = format_column_for_csv(format_apply_file_for_csv($apply_files[$tmp_apply["apply_id"]]));
		$tmp_comment        = format_column_for_csv(format_comment_for_csv($approves[$tmp_apply["apply_id"]]));

		$csv .= "{$tmp_apply_no},{$tmp_wkfw_title},{$tmp_apply_title},{$tmp_apply_emp_name},{$tmp_apply_date},{$tmp_apply_status},{$tmp_apply_emp_dept},{$tmp_apply_contents},{$tmp_apply_file},{$tmp_comment}";

		foreach ($approves[$tmp_apply["apply_id"]] as $tmp_approve) {
			$tmp_approve_st_name  = format_column_for_csv($tmp_approve["st_nm"]);
			$tmp_approve_emp_name = format_column_for_csv(format_emp_name_for_csv($tmp_approve["emp_lt_nm"], $tmp_approve["emp_ft_nm"]));
			$tmp_approve_status   = format_column_for_csv(format_approve_status_for_csv($tmp_approve["apv_stat"], $tmp_approve["approve_label"]));

			$csv .= ",{$tmp_approve_st_name},{$tmp_approve_emp_name},{$tmp_approve_status}";
		}

		$csv .= "\r\n";
	}
	return $csv;
}

function format_apply_no_for_csv($short_wkfw_name, $apply_date, $apply_no) {
	$year          = substr($apply_date, 0, 4);
	$month_and_day = substr($apply_date, 4, 4);
	if ($month_and_day >= "0101" && $month_and_day <= "0331") {
		$year = $year - 1;
	}
	return $short_wkfw_name . "-" . $year . sprintf("%04d", $apply_no);
}

function format_emp_name_for_csv($lt_nm, $ft_nm) {
	return $lt_nm . " " . $ft_nm;
}

function format_date_for_csv($ymd) {
	return preg_replace("/^(\d{4})(\d{2})(\d{2}).*$/", "$1/$2/$3", $ymd);
}

function format_apply_status_for_csv($apply_stat, $approve_label, $approves) {
	$status_prefix = ($approve_label != "2") ? "承認" : "確認";

	switch ($apply_stat) {
	case "0":
		foreach ($approves as $tmp_approve) {
			if ($tmp_approve["apv_stat"] != 0) {
				return "一部" . $status_prefix;
			}
		}
		return "未" . $status_prefix;

	case "1":
		return $status_prefix . "確定";

	case "2":
		return "否認";

	case "3":
		return "差戻し";
	}
}

function format_emp_dept_for_csv($class_nm, $atrb_nm, $dept_nm, $room_nm) {
	if ($class_nm == "" || $atrb_nm == "" || $dept_nm == "") {
		return "";
	}

	$emp_dept = "$class_nm > $atrb_nm > $dept_nm";
	if ($room_nm != "") {
		$emp_dept .= " > $room_nm";
	}

	return $emp_dept;
}

function format_apply_contents_for_csv($wkfw_content_type, $apply_content) {
	if ($wkfw_content_type == "1" || strpos($apply_content, "<?xml") === false) {
		return format_column_for_csv($apply_content);
	}

	if (!$dom = domxml_open_mem($apply_content)) {
		return "";
	}

	$root_nodes = $dom->get_elements_by_tagname('apply');
	if (count($root_nodes) == 0) {
		return "";
	}
	$root_node = $root_nodes[0];

	$contents = array();
	$child_nodes = $root_node->child_nodes();
	foreach ($child_nodes as $child_node) {
		if ($child_node->node_type() != XML_ELEMENT_NODE) {
			continue;
		}

		$grandchild_nodes = $child_node->child_nodes();
		if (count($grandchild_nodes) == 0) {
			$contents[] = "";
		} else {
			$contents[] = format_column_for_csv($grandchild_nodes[0]->node_value(), "UTF-8");
		}
	}

	return join(",", $contents);
}

function format_apply_file_for_csv($apply_files) {
	return join("、", $apply_files);
}

function format_comment_for_csv($approves) {
	$comment = "";
	foreach ($approves as $tmp_approve) {
		if ($tmp_approve["apv_comment"] == "") {
			continue;
		}
		$tmp_emp_name = format_emp_name_for_csv($tmp_approve["emp_lt_nm"], $tmp_approve["emp_ft_nm"]);
		$comment .= "【{$tmp_emp_name}さんのコメント】{$tmp_approve["apv_comment"]}";
	}
	return $comment;
}

function format_approve_status_for_csv($apv_stat, $approve_label) {
	$status_prefix = ($approve_label != "2") ? "承認" : "確認";

	switch ($apv_stat) {
	case "0":
		return "未" . $status_prefix;

	case "1":
		return $status_prefix;

	case "2":
		return "否認";

	case "3":
		return "差戻し";
	}
}

function format_column_for_csv($value, $from_encoding = "EUC-JP") {
	$buf = str_replace("\r", "", $value);
	$buf = str_replace("\n", "", $buf);
	if (strpos($buf, ",") !== false)  {
		$buf = '"' . $buf . '"';
	}
	return mb_convert_encoding($buf, "Shift_JIS", $from_encoding);
}
?>
