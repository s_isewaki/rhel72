<?

// 合議者詳細画面
// 承認詳細画面を流用したため機能が多数残っています

require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_application_apply_history.ini");
require_once("show_application_council_detail.ini");
require_once("show_select_values.ini");
require_once("application_imprint_common.ini");
require_once("yui_calendar_util.ini");
require_once("get_values.ini");
require_once("application_workflow_common_class.php");
require_once("get_values_for_template.ini");
require_once("library_common.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,7,$fname);
if($wkfw=="0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$obj = new application_workflow_common_class($con, $fname);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript" src="js/domready.js"></script>
<?
// 履歴フラグ true:履歴なので登録ボタン等を無効化する

$history_flg = false;
if($target_apply_id == "") {
    $target_apply_id = $apply_id;
} else {
    // $target_apply_idが異なる場合は再申請された以前のデータ
    if ($target_apply_id != $apply_id) {
        $history_flg = true;
    }
}

// 申請・ワークフロー情報取得
$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($target_apply_id);
$wkfw_content      = $arr_apply_wkfwmst[0]["wkfw_content"];
$wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
$apply_content     = $arr_apply_wkfwmst[0]["apply_content"];
$wkfw_id           = $arr_apply_wkfwmst[0]["wkfw_id"];
$wkfw_history_no   = $arr_apply_wkfwmst[0]["wkfw_history_no"];
$approve_label     = ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認";

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

$imprint_flg = get_imprint_flg($con, $emp_id, $fname);

$imgsrc = ($approve_label == "確認") ? "img/confirmed.gif" : "img/approved.gif";
if ($imprint_flg == "t") {
    $imgsrc = "application_imprint_image.php?session=".$session."&emp_id=".$emp_id."&apv_flg=1&imprint_flg=".$imprint_flg."&t=".date('YmdHis') . "&approve_label=" . urlencode($approve_label);
}

$dirname = "workflow/imprint/";
$img_file = ($approve_label == "確認") ? "img/confirmed.gif" : "img/approved.gif";
if ($imprint_flg == "t") {
    $img_file1 = "$dirname$emp_id.gif";
    $img_file2 = "$dirname$emp_id.jpg";
    // 当該職員の画像が登録済みか確認
    if (is_file($img_file1)) {
        $img_file = $img_file1;
    } else if (is_file($img_file2)) {
        $img_file = $img_file2;
    }
}

// 画像サイズ取得
list($w_frame1, $h_frame1) = get_imprint_imagesize($img_file);
list($w_frame2, $h_frame2) = get_imprint_imagesize("img/approve_ng.gif");
list($w_frame3, $h_frame3) = get_imprint_imagesize("img/returned.gif");


$sel_apply_wkfwmst = search_apply_wkfwmst($con, $fname, $target_apply_id);
$wkfw_content = pg_fetch_result($sel_apply_wkfwmst, 0, "wkfw_content");
$wkfw_content_type = pg_fetch_result($sel_apply_wkfwmst, 0, "wkfw_content_type");
$apply_content = pg_fetch_result($sel_apply_wkfwmst, 0, "apply_content");


// 本文形式タイプのデフォルトを「テキスト」とする
if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

// 形式をテキストからテンプレートに変更した場合の古いデータ対応
// wkfw_content_typeは2:テンプレートだが、登録データがXMLでない場合、1:テキストとして処理する
if ($wkfw_content_type == "2") {
    if (strpos($apply_content, "<?xml") === false) {
        $wkfw_content_type = "1";
    }
}

if($wkfw_history_no != "")
{
    $arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
    $wkfw_content = $arr_wkfw_template_history["wkfw_content"];
}

$num = 0;
if ($wkfw_content_type == "2") {
    $pos = 0;
    while (1) {
        $pos = strpos($wkfw_content, 'show_cal', $pos);
        if ($pos === false) {
            break;
        } else {
            $num++;
        }
        $pos++;
    }
}



// 異動チェック（不要なので0固定にしておく）
$changes_flg = 0;

// 外部ファイルを読み込む（$num==0でも読み込んでください）
write_yui_calendar_use_file_read_0_12_2();
?>
<title>CoMedix 決裁・申請 | <? echo($approve_label); ?>詳細</title>
<script type="text/javascript" src="js/yui_0.12.2/build/dragdrop/dragdrop-min.js"></script>
<?
// カレンダー作成、関数出力
write_yui_calendar_script2($num);
?>

<script type="text/javascript">
var spacer_img = new Image(60, 60);
spacer_img.src = 'img/spacer.gif';

var approved_img = new Image(<? echo($w_frame1); ?>, <? echo($h_frame1); ?>);
approved_img.src = '<? echo($imgsrc); ?>';

var approve_ng_img = new Image(<? echo($w_frame2); ?>, <? echo($h_frame2); ?>);
approve_ng_img.src = 'img/approve_ng.gif';

var returned_img = new Image(<? echo($w_frame3); ?>, <? echo($h_frame3); ?>);
returned_img.src = 'img/returned.gif';

var apply1_pos;
var apply2_pos;
var apply3_pos;
function initPage() {
    var dd1 = new YAHOO.util.DD("apply1", "applyset");
    var dd2 = new YAHOO.util.DD("apply2", "applyset");
    var dd3 = new YAHOO.util.DD("apply3", "applyset");
    var ddtarget1 = new YAHOO.util.DDTarget("target1", "applyset");

    dd1.startPos = YAHOO.util.Dom.getXY("apply1");
    apply1_pos = dd1.startPos;
    dd1.onDragDrop = function (e, id) {
        if (id == "target1") {
            var flg=this.getDragEl().id.substring(5,6);
            set_target(flg);
        }
    }
    dd1.endDrag = function (x, y) {
        var el = this.getDragEl();
        YAHOO.util.Dom.setXY(el, this.startPos);
        el.style.backgroundColor = '';
        el.style.zIndex = 0;
    }

    dd2.startPos = YAHOO.util.Dom.getXY("apply2");
    apply2_pos = dd2.startPos;
    dd2.startDrag = function (x, y) {
        var el = this.getDragEl();
        el.style.zIndex = 999;
    }
    dd2.onDragDrop = function (e, id) {
        if (id == "target1") {
            var flg=this.getDragEl().id.substring(5,6);
            set_target(flg);
        }
    }
    dd2.endDrag = function (x, y) {
        var el = this.getDragEl();
        YAHOO.util.Dom.setXY(el, this.startPos);
        el.style.backgroundColor = '';
        el.style.zIndex = 0;
    }

    dd3.startPos = YAHOO.util.Dom.getXY("apply3");
    apply3_pos = dd3.startPos;
    dd3.startDrag = function (x, y) {
        var el = this.getDragEl();
        el.style.zIndex = 999;
    }
    dd3.onDragDrop = function (e, id) {
        if (id == "target1") {
            var flg=this.getDragEl().id.substring(5,6);
            set_target(flg);
        }
    }
    dd3.endDrag = function (x, y) {
        var el = this.getDragEl();
        YAHOO.util.Dom.setXY(el, this.startPos);
        el.style.backgroundColor = '';
        el.style.zIndex = 0;
    }
}

function set_target(flg) {

// drag元を消す
    document.getElementById("apply"+flg).style.display = "none";
// 白い背景を表示
    document.getElementById("empty"+flg).style.display = "";

// ターゲットに画像を表示
    var imgsrc = "";
    if (flg == 1) {
        imgsrc = "<?=$imgsrc?>";
        document.getElementById("img1").width = <?=$w_frame1?>;
        document.getElementById("img1").height = <?=$h_frame1?>;
    } else if (flg == 2) {
        imgsrc = "img/approve_ng.gif";
        document.getElementById("img1").width = <?=$w_frame2?>;
        document.getElementById("img1").height = <?=$h_frame2?>;
    } else if (flg == 3) {
        imgsrc = "img/returned.gif";
        document.getElementById("img1").width = <?=$w_frame3?>;
        document.getElementById("img1").height = <?=$h_frame3?>;
    }
    document.getElementById("img1").src = imgsrc;
// 背景を白に
    document.getElementById("target1").style.backgroundColor = "#ffffff";
    document.getElementById("message1").style.backgroundColor = "#f6f9ff";
// プルダウンを設定
    document.apply.approve.value = flg;

    document.apply.drag_flg.value = "t";

}

function reset_imprint() {

    document.getElementById("apply1").style.display = "";
    document.getElementById("empty1").style.display = "none";
    YAHOO.util.Dom.setXY(document.getElementById("apply1"), apply1_pos);
    document.getElementById("apply2").style.display = "";
    document.getElementById("empty2").style.display = "none";
    YAHOO.util.Dom.setXY(document.getElementById("apply2"), apply2_pos);
    document.getElementById("apply3").style.display = "";
    document.getElementById("empty3").style.display = "none";
    YAHOO.util.Dom.setXY(document.getElementById("apply3"), apply3_pos);
    document.getElementById("target1").style.backgroundColor = "#ffcccc";
    document.getElementById("img1").src = "img/spacer.gif";
    document.getElementById("message1").style.backgroundColor = "#ffcccc";

    document.apply.drag_flg.value = "f";

}

function history_select(apply_id, target_apply_id) {

    document.apply.apply_id.value = apply_id;
    document.apply.target_apply_id.value = target_apply_id;
    document.apply.action="application_approve_detail.php?session=<?=$session?>&apv_order_fix=<?=$apv_order_fix?>&apv_sub_order_fix=<?=$apv_sub_order_fix?>&send_apved_order_fix=<?=$send_apved_order_fix?>";
    document.apply.submit();
}

var loadedContent;
function storeLoadedContent() {
	loadedContent = serializeContent();
}

function serializeContent() {
	var contents = [];

	var elements = document.apply.elements;
	for (var i = 0, j = elements.length; i < j; i++) {
		var e = elements[i];
		if (e.name == '' || e.type == 'hidden' || e.name == 'apv_comment' || e.name == 'approve') continue;
		var s = Form.Element.serialize(e);
		if (s) contents.push(s);
	}

	return contents.sort().join('&');
}

function approve_regist() {
//    if (serializeContent() != loadedContent) {
//        if (!document.getElementById('update_button').disabled) {
//            alert('申請内容を編集した場合は、先に申請内容更新ボタンを押してください。');
//        } else {
//            alert('申請内容は変更できません。');
//        }
//        return;
//    }

    if (!confirm('更新します。よろしいですか？')) return;

    new Ajax.Request(
        'text_api.php',
        {
            method: 'post',
            postBody: $H({'type':'1', 'apply_id':'<? echo($target_apply_id); ?>'}).toQueryString(),
            onComplete: function (req) {
                var loadedContentMD5 = document.apply.apply_content_md5.value;
                var currentContentMD5 = req.responseText;
                if (loadedContentMD5 != currentContentMD5) {
                    alert('別のユーザが申請内容を更新したため、処理を中断します。画面を開き直してください。');
                    return;
                }

                document.apply.action="application_council_detail_regist.php?session=<?=$session?>";
                document.apply.update_action.value = 'f';
                document.apply.submit();
            }
        }
    );
}

function approve_update() {
    if (window.InputCheck && !InputCheck()) return;

//    if (serializeContent() == loadedContent) {
//        alert('申請内容が変更されていないため、更新不要です。');
//        return;
//    }

    if (!confirm('更新します。よろしいですか？')) return;

    new Ajax.Request(
        'text_api.php',
        {
            method: 'post',
            postBody: $H({'type':'1', 'apply_id':'<? echo($target_apply_id); ?>'}).toQueryString(),
            onComplete: function (req) {
                var loadedContentMD5 = document.apply.apply_content_md5.value;
                var currentContentMD5 = req.responseText;
                if (loadedContentMD5 != currentContentMD5) {
                    alert('別のユーザが申請内容を更新したため、処理を中断します。画面を開き直してください。');
                    return;
                }
                document.apply.action="application_council_detail_regist.php?session=<?=$session?>";
                document.apply.update_action.value = 't';
                document.apply.location_href.value = location.href;
                document.apply.submit();
            }
        }
    );
}

// テキストエリア行数の自動拡張

var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    initialize: function(field)
    {
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);

    },

    resizeNeeded: function(event)
    {
	;
/*
        var objs = document.getElementsByTagName('textarea');
        for (var i = 0, j = objs.length; i < j; i++) {
            var t = objs[i];
            if (t.scrollHeight > t.clientHeight) {
                t.style.height = (t.scrollHeight + 5) + 'px';
            }
        }

        // 履歴表示table高さ調整
        flg = resize_history_tbl();
        if(flg)
        {
            // 承認画像のY軸座標位置の再設定
            var apply1 = YAHOO.util.Dom.getXY("apply1");
            var empty1 = YAHOO.util.Dom.getXY("empty1");

            y_position = '';
            if(apply1 != false)
            {
                y_position = apply1[1];
            }
            else
            {
                y_position = empty1[1];
            }

            apply1_pos[1] = y_position;
            apply2_pos[1] = y_position;
            apply3_pos[1] = y_position;
        }
*/
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
    var objs = document.getElementsByTagName('textarea');
    for (var i = 0, j = objs.length; i < j; i++) {
        var t = objs[i];
        if (t.scrollHeight > t.clientHeight) {
            t.style.height = (t.scrollHeight + 5) + 'px';
        }
    }
}

// 履歴表示tableの高さ調節
/*
function resize_history_tbl()
{

    var flg = false;
    heigh1 = document.getElementById('history_tbl').style.height;
    document.getElementById('history_tbl').style.height = document.getElementById('dtl_tbl').offsetHeight;
    heigh2 = document.getElementById('history_tbl').style.height;

    if(heigh1 != heigh2)
    {
        flg = true;
    }
    return flg

}
*/
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}

p {margin:0;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initcal();
<?



$apply_stat = $arr_apply_wkfwmst[0]["apply_stat"];

// 承認画像イメージ表示・非表示
$approve_flg = false;
$arr_applyapv = $obj->get_applyapv($apply_id);
foreach($arr_applyapv as $applyapv)
{
    $apv_stat       = $applyapv["apv_stat"];
    $apv_order      = $applyapv["apv_order"];
    $apv_sub_order  = $applyapv["apv_sub_order"];

    if(($apv_order == $apv_order_fix && $apv_sub_order == $apv_sub_order_fix) && $apv_stat == 0)
    {
        $approve_flg = true;
        break;
    }
}


$approve_flg = false;

$sel_apply_wkfwmst = search_apply_wkfwmst($con, $fname, $target_apply_id);
$apply_stat = pg_fetch_result($sel_apply_wkfwmst, 0, "apply_stat");


// 承認者情報取得
$sel_applyapv = search_applyapv($con, $fname, $target_apply_id);
$approve_num = pg_numrows($sel_applyapv);    // 承認者数

for ($i=0; $i<$approve_num; $i++) {
    $apv_stat = pg_fetch_result($sel_applyapv, $i, "apv_stat");
    $apv_emp_id = pg_fetch_result($sel_applyapv, $i, "emp_id");

    if(($apv_order == $apv_order_fix && $apv_sub_order == $apv_sub_order_fix) && $apv_stat == 0)
    {
        $approve_flg = true;
        break;
    }
}

?>
 if (window.OnloadSub) { OnloadSub(); };resizeAllTextArea();initPage();if (window.refreshApproveOrders) {refreshApproveOrders();} storeLoadedContent();">
<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="target_apply_id" value="<? echo($target_apply_id); ?>">
<input type="hidden" name="update_action" value="">
<input type="hidden" name="apply_content_md5" value="<? echo(md5($apply_content)); ?>">
<input type="hidden" name="location_href" value="">
<input type="hidden" name="emp_id" value="<? echo $emp_id; ?>">

<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing">&nbsp</td><!--TOP BAR-->
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top" width="75%">
<?
$mode = "";
show_application_council_detail($con, $session, $fname, $target_apply_id, $mode, $apv_comment, $approve, $drag_flg, $changes_flg, $history_flg, $apv_order_fix, $apv_sub_order_fix, $send_apved_order_fix); ?>
</td>
</center>
</form>

<form name="council_print_form" action="application_council_detail_print.php" method="post" target="counci_detail_print_window">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="fname" value="<?=$fname?>">
<input type="hidden" name="target_apply_id" value="<?=$target_apply_id?>">

<input type="hidden" name="back" value="t">
<input type="hidden" name="mode" value="approve_print">

<input type="hidden" name="history_flg" value="">
<input type="hidden" name="apv_order_fix" value="">
<input type="hidden" name="apv_sub_order_fix" value="">
<input type="hidden" name="send_apved_order_fix" value="">

<input type="hidden" name="approve" value="">
<input type="hidden" name="drag_flg" value="">

<input type="hidden" name="apv_print_comment" value="">

</form>
</body>
</html>
<?
// 申請・ワークフロー情報取得
function search_apply_wkfwmst($con, $fname, $apply_id) {

    $sql = "select a.apply_stat, a.apply_date, a.apply_content, a.apply_title, a.re_apply_id, a.emp_class as apply_emp_class, a.emp_attribute as apply_emp_attribute, a.emp_dept as apply_emp_dept, a.emp_room as apply_emp_room, b.wkfw_id, b.wkfw_title, b.wkfw_start_date, b.wkfw_end_date, b.wkfw_appr, b.wkfw_content, b.wkfw_content_type, c.wkfw_nm, d.emp_lt_nm, d.emp_ft_nm, d.emp_class as emp_cls, d.emp_attribute as emp_atr, d.emp_dept as emp_dpt, d.emp_room as emp_mst_room ";
    $sql .= "from apply a ";
    $sql .= "inner join wkfwmst b on a.wkfw_id = b.wkfw_id ";
    $sql .= "inner join wkfwcatemst c on b.wkfw_type = c.wkfw_type ";
    $sql .= "inner join empmst d on a.emp_id = d.emp_id";
    $cond = "where a.apply_id = $apply_id";

    $sel=select_from_table($con,$sql,$cond,$fname);
    if($sel==0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    return $sel;
}

// 承認者情報取得
function search_applyapv($con, $fname, $apply_id) {

    $sql = "select a.apply_id, a.wkfw_id, a.apv_order, a.emp_id, a.apv_stat, a.apv_date, a.st_div, a.deci_flg, a.apv_comment, a.emp_class as apply_emp_class, a.emp_attribute as apply_emp_attribute, a.emp_dept as apply_emp_dept, a.emp_room as apply_emp_room, a.emp_st as apply_emp_st, b.emp_lt_nm, b.emp_ft_nm, b.emp_class as emp_cls, b.emp_attribute as emp_atr, b.emp_dept as emp_dpt, b.emp_room as emp_mst_room, b.emp_st as emp_stat, c.st_nm as apply_st_nm, d.st_nm as empmst_st_nm ";
    $sql .= "from ";
    $sql .= "applyapv a ";
    $sql .= "inner join empmst b on a.emp_id = b.emp_id ";
    $sql .= "left outer join stmst c on a.emp_st = c.st_id ";
    $sql .= "inner join stmst d on b.emp_st = d.st_id ";

    $cond = "where a.apply_id = $apply_id order by a.apv_order asc";

    $sel=select_from_table($con,$sql,$cond,$fname);
    if($sel==0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    return $sel;
}

pg_close($con);
