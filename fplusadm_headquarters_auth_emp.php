<?
require_once("about_comedix.php");
require_once("fplusadm_common.ini");
require_once("show_fplusadm_lib_list.ini");
require_once("mygroup_common.php");
require_once("label_by_profile_type.ini");
require_once("fplus_select_box.ini");


//====================================
//画面名
//====================================
$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$checkauth = check_authority($session, 79, $fname);
$con = @connect2db($fname);
if(!$session || !$checkauth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}

//======================================
// 組織階層情報取得
//======================================
$arr_org_level = array();
$level_cnt = 0;
$sql = "select class_nm, atrb_nm, dept_nm, room_nm, class_cnt from classname ";
$cond = "";
$sel = select_from_table($con,$sql,$cond,$fname);
if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$arr_org_level[0] = pg_result($sel,0,"class_nm");
$arr_org_level[1] = pg_result($sel,0,"atrb_nm");
$arr_org_level[2] = pg_result($sel,0,"dept_nm");
$arr_org_level[3] = pg_result($sel,0,"room_nm");
$level_cnt = pg_result($sel,0,"class_cnt");

// ログインユーザの職員IDを取得
$sql = "select emp_id from session where session_id = '$session'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// マイグループと職員名リスト取得
list($mygroups, $employees) = get_mygroups_employees($con, $fname, $emp_id);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

//権限区分
$auth_part = @$_REQUEST["auth_part"];

$insert_mode = @$_REQUEST["insert_mode"];

$disp_area_ids = @$_REQUEST["disp_area_ids_1"];


$sel_emp_ids = @$_REQUEST["sel_emp_ids"];

//======================================
// 登録ボタン押下時処理
//======================================
if($insert_mode == "1"){
	$ary_target_emp = preg_split("/,/", $disp_area_ids);

	$upd_date = date("Y-m-d H:i:s");

	// 医療事故情報ID採番
	$sql = "SELECT max(headquarters_auth_id) FROM fplus_headquarters_auth_emp";
	$cond = "";
	$headquarters_auth_id_sel = select_from_table($con,$sql,$cond,$fname);
	$headquarters_auth_id = pg_result($headquarters_auth_id_sel,0,"max");
	if($headquarters_auth_id == ""){
		$headquarters_auth_id = 1;
	}else{
		$headquarters_auth_id = $headquarters_auth_id + 1;
	}


	// トランザクション開始
	pg_query($con, "begin");

	if(ary_sel_emp_ids != ""){
		$ary_sel_emp_ids = preg_split("/,/", $sel_emp_ids);

		//削除処理(論理削除)
		$sql=	"update fplus_headquarters_auth_emp set";
		$set =	array("del_flg");
		$cond = "where emp_id in ( ";
		for($i = 0;$i < count($ary_sel_emp_ids);$i++){
			if($i != 0){
				$cond .= ",";
			}
			$cond .= " '{$ary_sel_emp_ids[$i]}'";
		}
		$cond .= " ) AND auth_part = '$auth_part'";

		$setvalue = array("t");
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	
	$sql = "";
	for($i=0;$i<count($ary_target_emp);$i++){
		if($i!=0){
			$headquarters_auth_id += 1;
		}
		$sql .= " INSERT INTO fplus_headquarters_auth_emp (headquarters_auth_id, auth_part, emp_id, update_time, del_flg) VALUES ";
		$sql .= " ($headquarters_auth_id, '$auth_part', '$ary_target_emp[$i]', '$upd_date', 'f'); ";
	}
	$ins = insert_into_table_no_content($con, $sql, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// トランザクションをコミット
	pg_query($con, "commit");

}
//======================================
// 検索処理
//======================================
$sql = "SELECT ".
	" 	auth.headquarters_auth_id ".
	" 	, auth.auth_part ".
	" 	, auth.emp_id ".
	" 	, mst.emp_lt_nm || ' ' || mst.emp_ft_nm as emp_name ".
	" FROM ".
	" 	fplus_headquarters_auth_emp auth ".
	" 	, empmst mst ".
	" WHERE ".
	" 	auth.emp_id = mst.emp_id ".
	" 	AND auth.auth_part = '$auth_part' ".
	" 	AND auth.del_flg = 'f' ".
	" ORDER BY ".
	" 	auth.headquarters_auth_id ";

$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$headquarters_auth_list = pg_fetch_all($sel);

?>

<?
//====================================
// HTML
//====================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | オプション設定 | <?=($auth_part == "1" ? "医療事故管理表担当者" : "本部集計表担当者")?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script src="js/prototype/dist/prototype.js"></script>
<script language="javascript">

var childwin = null;
function openEmployeeList(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=$emp_id?>';
	url += '&mode=6';
	url += '&item_id='+item_id;
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

function closeEmployeeList() {
	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;
}

// 職員指定画面から呼ばれる関数
function add_target_list(item_id, input_emp_id,input_emp_nm)
{
	ids   = "disp_area_ids_" + item_id;
	names = "disp_area_names_" + item_id;

	//既存入力されている職員
	emp_ids = document.getElementById(ids).value;
	emp_nms = document.getElementById(names).value;


	//子画面から入力された職員
	arr_input_emp_id   = input_emp_id.split(", ");
	arr_input_emp_name = input_emp_nm.split(", ");

	tmp_ids   = "";
	tmp_names = "";
	if(emp_ids != "")
	{
		arr_emp_id = emp_ids.split(",");

		for(i=0; i<arr_input_emp_id.length; i++)
		{
			dpl_flg = false;
			for(j=0; j<arr_emp_id.length; j++)
			{
				// 重複した場合
				if(arr_input_emp_id[i] == arr_emp_id[j])
				{
					dpl_flg = true;
				}
			}
			if(!dpl_flg)
			{
				if(tmp_ids != "")
				{
					tmp_ids   += ",";
					tmp_names += ",";
				}
				tmp_ids   += arr_input_emp_id[i];
				tmp_names += arr_input_emp_name[i];
			}
		}
	}
	else
	{
		for(i=0; i<arr_input_emp_id.length; i++)
		{
			if(tmp_ids != "")
			{
				tmp_ids   += ",";
				tmp_names += ",";
			}
			tmp_ids   += arr_input_emp_id[i];
			tmp_names += arr_input_emp_name[i];
		}
	}

	if(emp_ids != "" && tmp_ids != "")
	{
		emp_ids += ",";
	}
	emp_ids += tmp_ids;
	document.getElementById(ids).value = emp_ids;
	if(emp_nms != ""  && tmp_names != "") {
		emp_nms += ",";
	}
	emp_nms += tmp_names;
	document.getElementById(names).value = emp_nms;

	set_disp_area(emp_ids, emp_nms, item_id);

}

//表示変更
function set_disp_area(emp_id, emp_nm, item_id) {

	disp_area = "disp_area_" + item_id;

	arr_emp_id = emp_id.split(",");
	arr_emp_nm = emp_nm.split(",");
	disp_area_content = "";
	for(i=0; i<arr_emp_id.length; i++)
	{
		if(i > 0)
		{
			disp_area_content += ',';
		}

		disp_area_content += "<a href=\"javascript:delete_emp('" + arr_emp_id[i] + "', '" + arr_emp_nm[i] + "', '" + item_id + "');\">";
		disp_area_content += arr_emp_nm[i];
		disp_area_content += "</a>";
	}
	document.getElementById(disp_area).innerHTML = disp_area_content;
}

//職員リンククリック時
function delete_emp(del_emp_id, del_emp_nm, item_id)
{
	if(confirm("「" + del_emp_nm + "」さんを削除します。よろしいですか？"))
	{
		ids   = "disp_area_ids_" + item_id;
		names = "disp_area_names_" + item_id;

		emp_ids = document.getElementById(ids).value;
		emp_nms = document.getElementById(names).value;

		new_emp_ids = "";
		new_emp_nms = "";

		cnt = 0;
		if(emp_ids != "") {

			arr_emp_id = emp_ids.split(",");
			arr_emp_nm = emp_nms.split(",");

			for(i=0; i<arr_emp_id.length; i++)
			{
				if(arr_emp_id[i] == del_emp_id)
				{
					continue;
				}
				else
				{
					if(cnt > 0)
					{
					    new_emp_ids += ",";
						new_emp_nms += ",";
					}
				    new_emp_ids += arr_emp_id[i];
					new_emp_nms += arr_emp_nm[i];
					cnt++;
				}
			}
		}
		document.getElementById(ids).value = new_emp_ids;
		document.getElementById(names).value = new_emp_nms;
		
		set_disp_area(new_emp_ids, new_emp_nms, item_id);
	}

}

//クリアボタン押下時
function emp_clear(div)
{
	if(document.getElementById('disp_area_' + div))
	{
		document.getElementById('disp_area_' + div).innerHTML   = '';
		document.getElementById('disp_area_ids_' + div).value   = '';
		document.getElementById('disp_area_names_' + div).value = '';
	}
}

//画面初期表示
function initPage() {
	ids   = "disp_area_ids_1";
	names = "disp_area_names_1";

	tmp_ids   = "";
	tmp_names = "";

	<? if($headquarters_auth_list[0]["emp_id"] != ""){ ?>
		<? for($i=0; $i<count($headquarters_auth_list); $i++) { ?>
			<? if($i != 0){ ?>
				tmp_ids   += ",";
				tmp_names += ",";
			<? } ?>
			tmp_ids   += "<?=$headquarters_auth_list[$i]['emp_id']?>";
			tmp_names += "<?=$headquarters_auth_list[$i]['emp_name']?>";
		<? } ?>
	<? } ?>

	document.getElementById(ids).value = tmp_ids;
	document.getElementById(names).value = tmp_names;
	document.getElementById("sel_emp_ids").value = tmp_ids;

	set_disp_area(tmp_ids, tmp_names, "1");
}


function DataIns(){
	if (window.InputCheck) {
		if (!InputCheck()) {
			return;
		}
	}

	if(document.getElementById("disp_area_ids_1").value == ""){
		alert("職員を選択してください。");
		return false;
	}
	if (confirm('登録します。よろしいですか？')) {
		//登録モードON
		document.wkfw.insert_mode.value = "1";
		document.wkfw.target = "_self";
		document.wkfw.submit();
		return false;
	}
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#E6B3D4 solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#E6B3D4 solid 0px;}
table.block_in td td {border-width:1;}
// -->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<form name="wkfw" action="fplusadm_headquarters_auth_emp.php" method="post">
<input type="hidden" id="session" name="session" value="<? echo($session); ?>" />
<input type="hidden" id="auth_part" name="auth_part" value="<?=$auth_part?>">
<input type="hidden" id="sel_emp_ids" name="sel_emp_ids" value="">
<input type="hidden" id="insert_mode" name="insert_mode" value="">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="32" height="32" class="spacing"><a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a></td>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplusadm_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="fplusadm_option.php?session=<? echo($session); ?>"><b>本部機能設定</b></a></font></td>
					<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="fplus_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table style="width:100%; margin-top:4px">
			<tr>
			<td style="vertical-align:top; width:120px"><?= fplusadm_common_show_admin_left_menu($session); ?></td>
			<td style="vertical-align:top">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #c488ae; margin-bottom:2px">
					<tr>
						<td width="3" align="right"><img src="img/menu-fplus-left.gif" alt="" width="3" height="22"></td>
						<td width="120" align="center" bgcolor="#FFDFFF"><a href="fplusadm_option.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>本部機能設定</nobr></font></a></td>
						<td width="3" align="left"><img src="img/menu-fplus-right.gif" alt="" width="3" height="22"></td>
						<td width="10">&nbsp;</td>
						<td width="3" align="right"><img src="<?=($auth_part == "1" ? "img/menu-fplus-left-on.gif" : "img/menu-fplus-left.gif")?>" alt="" width="3" height="22"></td>
						<td width="120" align="center" bgcolor="<?=($auth_part == "1" ? "#FF99CC": "#FFDFFF")?>"><a href="fplusadm_headquarters_auth_emp.php?session=<?=$session?>&auth_part=1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="<?=($auth_part == "1" ? "#ffffff" : "")?>" ><nobr>医療事故管理表担当者</nobr></font></a></td>
						<td width="3" align="left"><img src="<?=($auth_part == "1" ? "img/menu-fplus-right-on.gif" : "img/menu-fplus-right.gif")?>" alt="" width="3" height="22"></td>
						<td width="10">&nbsp;</td>
						<td width="3" align="right"><img src="<?=($auth_part == "2" ? "img/menu-fplus-left-on.gif" : "img/menu-fplus-left.gif")?>" alt="" width="3" height="22"></td>
						<td width="120" align="center" bgcolor="<?=($auth_part == "2" ? "#FF99CC": "#FFDFFF")?>"><a href="fplusadm_headquarters_auth_emp.php?session=<?=$session?>&auth_part=2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="<?=($auth_part == "2" ? "#ffffff" : "")?>" ><nobr>本部集計表担当者</nobr></font></a></td>
						<td width="3" align="left"><img src="<?=($auth_part == "2" ? "img/menu-fplus-right-on.gif" : "img/menu-fplus-right.gif")?>" alt="" width="3" height="22"></td>
						<td>&nbsp;</td>
					</tr>
				</table>

				<img src="img/spacer.gif" width="1" height="3" alt=""><br>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr height="32" bgcolor="#E6B3D4">
						<td class="spacing" style="text-align:center;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=($auth_part == "1" ? "医療事故管理表担当者" : "本部集計表担当者")?></b></font></td>
					</tr>
				</table>

				<img src="img/spacer.gif" width="1" height="3" alt=""><br>

				<table width="100%" border='0' cellspacing='0' cellpadding='0' class="block">
					<!-- 職員指定 -->
					<tr height="100">
						<td width="20%" bgcolor="#feeae9" align="center">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
							<input type="button" name="emplist_btn" value="職員名簿" onclick="openEmployeeList('1');"><br>
							<input type="button" value="クリア" onclick="emp_clear('1');">
						</td>
						<td colspan="2" bgcolor="#FFFFFF">
							<table width="100%" height="100%" border="0" cellspacing="3" cellpadding="0" class="non_in_list">
								<tr>
									<td>
										<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><span id="disp_area_1"></span></font>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<input type="hidden" name="disp_area_ids_1" value="" id="disp_area_ids_1">
					<input type="hidden" name="disp_area_names_1" value="" id="disp_area_names_1">
					
				</table>

				<img src="img/spacer.gif" width="1" height="5" alt=""><br>

				<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td align="right">
							<input type="button" value="登録" onclick="DataIns();">
						</td>
					</tr>
				</table>
			</td>
			</tr>
			</table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
<?
pg_close($con);
?>