<?php

set_include_path('../');
require_once("about_postgres.php");
require_once("get_values.php");
require_once("hiyari_report_class.php");


//DBコネクション
$con = connect2db($fname);
if($con == "0"){
	showErrorPage();
	exit;
}

//----------------------------------------------------------
// 職員
//----------------------------------------------------------
//ユーザーID取得
$emp_id = get_emp_id($con, $_POST["fuld_session"], $fname);

$rep_obj = new hiyari_report_class($con, $fname);


//画面から来たファイル名を取得
$fileTmpName1 = $_FILES['img']['tmp_name'];

//スペースを削除
$fileTmpName2 = preg_replace('/(\s|　)/','',$_FILES['img']['name']);	

if($StateTrns == "")
{


	//画面上から「ファイル登録」
			
	$emp_dir = $rep_obj->get_tmp_dir("Tmp",$emp_id, $_POST['report_id']);
	//$dir = scandir($emp_dir);
		
//	$file_max = sprintf("%04d",(count($dir)-2));

	//tempファイル名の元はマイクロ秒で設定する
	$dateM=microtime();
	$dateMar = explode(" ",$dateM);
	$fileDate=$dateMar[0]+$dateMar[1];

	//tempファイル名を暗号化
	$md_file = md5($fileDate);
	$ext = strrchr($fileTmpName2, ".");
	$tmpFileName = $emp_dir."/".$md_file.$ext;
	
	
	
	//tempファイルを指定ディレクトリにアップロード
	if (move_uploaded_file($fileTmpName1, $tmpFileName)) {
		$data = array('filename' => $filename);
	} else {
		$data = array('error' => 'Failed to save');
	}

	

		
	setcookie('hiyariFileEnc['.$md_file.']', $fileTmpName2);
	
	$tmpDispFile = $_POST['file1'];
	$tmpEncFile = $_POST['file2'];

	/*
	//----------------------------------------------------------
	// JSON
	//----------------------------------------------------------
	/*
	echo cmx_json_encode(
			array(
				'emp_id' => $_POST['emp_id'],
				'name' => mb_convert_encoding(mb_convert_encoding($_POST['file'],"sjis-win","UTF-8"),"eucJP-win","sjis-win"),
				'filename' => $fileTmpName
				)
			);
	*/
?>
<div id="<?=$md_file?>"><input type="button" name="<?=$md_file?>" class="delF" value="削除" /> <?=$fileTmpName2?></div>

<?

	$arrFileCheck = array();
	$arrFileCheck = array_unique($_COOKIE['hiyariFileEnc']);

	foreach ($arrFileCheck as $enc => $name) {

		//cookieの整合性チェック
		$ary = glob($emp_dir."/".$enc.".*") ;
		if(!count($ary))
		{
			//cookieに登録されているファイルなし
			//cookieから削除
			setcookie('hiyariFileEnc['.$enc.']', 'noFile', time() - 1800);
			continue;
		}

?>
<div id="<?=$enc?>"><input type="button" name="<?=$enc?>" class="delF" value="削除" /> <?=$name?></div>
	
<?
	}
	$end = "end";
}
elseif($StateTrns == "delFile")
{
	
	//画面上から「ファイル削除」
	$del_file = $_POST['func_id'];
	
	$emp_dir = $rep_obj->get_tmp_dir("Tmp",$emp_id, $_POST["Ids"]);
	
	
	foreach (glob($emp_dir."/".$_POST['func_id'].".*") as $sFilePath) {

		if (!unlink($sFilePath)) {
			continue;
		}
	}

	//Cookieから削除
	setcookie('hiyariFileEnc['.$_POST['func_id'].']', 'noFile', time() - 1800);
	echo(str_replace(array("\r\n","\r","\n"), '', $_POST['func_id']));

}
elseif($StateTrns == "delFileReg")
{
	
	$del_dir = $rep_obj->get_tmp_dir("",$emp_id,$_POST["Ids"]);
	
	//画面上から「ファイル削除」
	$del_file = $_POST['func_id'];
	
	foreach (glob($del_dir."/".$_POST['func_id']."*") as $sFilePath) {
		


		if (!unlink($sFilePath)) {
			continue;
		}
		
		//画像表示用ファイルの削除
		//unlink($sFilePath);

		$sql = "DELETE FROM inci_file WHERE report_file_enc = '{$_POST['func_id']}'";
		$result = delete_from_table($rep_obj->_db_con, $sql, "", $rep_obj->file_name);
		if ($result == 0) {
			pg_query($rep_obj->_db_con,"rollback");
			pg_close($rep_obj->_db_con);
			showErrorPage();
			exit;
		}
	}
	
	echo(str_replace(array("\r\n","\r","\n"), '', $_POST['func_id']));
	
}
elseif($StateTrns == "delExitTmpFile")
{
	
}
?>
