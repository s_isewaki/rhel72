<?php
class hiyari_report_xml_data { 
	
	function hiyari_report_xml_data($db) { 
		$this->_db = $db;
	}
	
	function set_report_id($report_id) { 
		$this->_report_id = $report_id;
	}
	
	function get_report_id() { 
		return $this->_report_id;
	}
	
	function set_folder_id($folder_id) { 
		$this->_folder_id = $folder_id;
	}
	
	function get_data() { 
		$sql  = " select grp_code,easy_item_code,input_item from";
		$sql .= " (";
		$sql .= " select eid_id from inci_report where report_id = $this->_report_id";
		$sql .= " ) r";
		$sql .= " natural inner join inci_easyinput_data";
		
		$this->_db->query($sql);
		$this->_data = $this->_db->getAll();

		return $this->_data;
	}
	
	function format_data() { 
		foreach($this->_data as $value) { 
			$this->_format_data[$value['grp_code']][$value['easy_item_code']][] = trim($value['input_item']);
		}
	}
	
	function get_tab2comma($data) { 
		return ereg_replace("\t", ",", $data);
	}
	
	function get_date() { 
		return $this->_format_data[100][5][0];
	}
	
	function get_holiday() { 
		$tmp_date = explode("/", $this->get_date());
		$tmp_ymd = $tmp_date[0].$tmp_date[1].$tmp_date[2];
		$sql = "SELECT type FROM calendar WHERE date = '{$tmp_ymd}'";
		
		$this->_db->query($sql);
		$holiday = $this->_db->getOne();
		
		if($holiday >= 4) { 
			return "02";
		} else if($holiday > 0 && $holiday <= 3) { 
			return "01";
		} else { 
			return "90";
		}
	}
	
	function get_timezone() { 
		return $this->_format_data[100][40][0];
	}
	
	function get_timezone_text() { 
		return $this->_format_data[100][50][0];
	}
	
	function get_execution() { 
		return $this->_format_data[1100][10][0];
	}
	
	function get_level() { 
		return $this->_format_data[1100][20][0];
	}
	
	function get_sequelae() { 
		return $this->_format_data[1100][30][0];
	}
	
	function get_sequelae_text() { 
		return $this->_format_data[1100][40][0];
	}
	
	function get_influence() { 
		return $this->_format_data[1100][50][0];
	}
	
	function get_site() { 
		if(empty($this->_format_data[110][60][0])) return false;
		
		$sql = "SELECT def_code FROM inci_report_materials WHERE grp_code = 110 and easy_item_code = 60 and easy_code = '{$this->_format_data[110][60][0]}'";
		$this->_db->query($sql);
		$site = $this->_db->getOne();
		return $site;
	}
	
	function get_site_text() { 
		return $this->_format_data[110][70][0];
	}
	
	function get_summary() {
        if(empty($this->_format_data[900][10][0])) return false;
        
        $where = $this->get_tab2comma($this->_format_data[900][10][0]);
        $sql = "SELECT * FROM inci_super_item_2010 WHERE super_item_id in ({$where})";
        $this->_db->query($sql);
        $result =  $this->_db->getRow();
        
        return $result;
	}
	
	function get_summary_text() { 
		return $this->_format_data[900][20][0];
	}
	
	function get_specially() { 
		return $this->_format_data[1110][10][0];
	}
	
	function get_specially_text() { 
		return $this->_format_data[1110][20][0];
	}
	
	function get_department() { 
		if(empty($this->_format_data[130][90][0])) return false;
		
		$where = $this->get_tab2comma($this->_format_data[130][90][0]);
		$where = "'" . ereg_replace(",", "','", $where) . "'";
		$sql = "SELECT def_code FROM inci_report_materials WHERE grp_code = 130 and easy_item_code = 90 and easy_code in ({$where})";
		$this->_db->query($sql);
		return $this->_db->getAll();
	}
	
	function get_department_text() { 
		return $this->_format_data[130][100][0];
	}
	
	function get_patient_num() { 
		if(empty($this->_format_data[200][10][0])) return false;

		$where = "'" . ereg_replace(",", "','", $this->_format_data[200][10][0]) . "'";
		$sql = "SELECT def_code FROM inci_report_materials WHERE grp_code = 200 and easy_item_code = 10 and easy_code in ($where)";
		$this->_db->query($sql);

		return (int)$this->_db->getOne('def_code'); 
	}
	
	function get_patient_text() { 
		return $this->_format_data[200][20][0];
	}
	
	function get_patient_year() { 
		return $this->_format_data[210][40][0];
	}
	
	function get_patient_month() { 
		return $this->_format_data[210][50][0];
	}
	
	function get_patient_sex() { 
		return $this->_format_data[210][30][0];
	}
	
	function get_patient_div() { 
		if(empty($this->_format_data[230][60][0])) return false;
		
		$sql = "SELECT def_code FROM inci_report_materials WHERE grp_code = 230 and easy_item_code = 60 and easy_code = '{$this->_format_data[230][60][0]}'";
		$this->_db->query($sql);
		return $this->_db->getOne();
	}
	
	function get_disease0() { 
		return $this->_format_data[250][80][0];
	}
	
	function get_disease1() { 
		return $this->_format_data[260][90][0];
	}
	
	function get_disease2() { 
		return $this->_format_data[270][100][0];
	}
	
	function get_disease3() { 
		return $this->_format_data[280][110][0];
	}
	
	function get_patient_state() { 
		if(empty($this->_format_data[290][120][0])) return false;

		$where = $this->get_tab2comma($this->_format_data[290][120][0]);
		$where = "'" . ereg_replace(",", "','", $where) . "'";
		$sql = "SELECT def_code FROM inci_report_materials WHERE grp_code = 290 and easy_item_code = 120 and easy_code in ({$where})";
		$this->_db->query($sql);
		return $this->_db->getAll();
	}
	
	function get_patient_state_text() { 
		return $this->_format_data[290][130][0];
	}
	
	function get_discoverer() { 
		return $this->_format_data[3000][10][0];
	}
	
	function get_discoverer_text() { 
		return $this->_format_data[3000][20][0];
	}
	
	///////////////////////////////////////////////26page////////////////////////////
	function get_concerned_num() { 
		for($i=0; $i<=9; $i++) { 
			$grp_code = '305'.$i;
			if(is_null($this->_format_data[$grp_code][30][0])) { 
				break;
			} else { 
				$j++;
			}
		}
		
		return $j;
	}
	
	function get_concerned($i) { 
		$grp_code = '305'.$i;
		$where = trim($this->_format_data[$grp_code][30][0]);
		$sql = "SELECT def_code FROM inci_report_materials WHERE grp_code = 305{$i} and easy_item_code = 30 and easy_code = '{$where}'";
		$this->_db->query($sql);
		return $this->_db->getOne();
	}
	
	function get_concerned_text($i) { 
		$grp_code = '305'.$i;
		return $this->_format_data[$grp_code][35][0];
	}
	
	function get_qualification($i) { 
		$grp_code = '320'.$i;
		return $this->_format_data[$grp_code][60][0];
	}
	
	function get_experience_year($i) { 
		$grp_code = '325'.$i;
		return $this->_format_data[$grp_code][70][0];
	}
	
	function get_experience_month($i) { 
		$grp_code = '325'.$i;
		return $this->_format_data[$grp_code][80][0];
	}
	
	function get_length_year($i) { 
		$grp_code = '330'.$i;
		return $this->_format_data[$grp_code][90][0];
	}
	
	function get_length_month($i) { 
		$grp_code = '330'.$i;
		return $this->_format_data[$grp_code][100][0];
	}
	
	function get_duty($i) { 
		$grp_code = '335'.$i;
		return $this->_format_data[$grp_code][110][0];
	}
	
	function get_duty_text($i) { 
		$grp_code = '335'.$i;
		return $this->_format_data[$grp_code][115][0];
	}
	
	function get_shift($i) { 
		$grp_code = '340'.$i;
		$data = $this->_format_data[$grp_code][120][0];
		if($data == 1) { 
			$data = 8;
		} else if($data == 5) { 
			$data = 99;
		}
		
		return sprintf("%02d", $data);
	}
	
	function get_shift_text($i) { 
		$grp_code = '340'.$i;
		return $this->_format_data[$grp_code][130][0];
	}
	
	function get_hours($i) { 
		$grp_code = '345'.$i;
		return $this->_format_data[$grp_code][140][0];
	}
	/////////////////////////////////////////////////////////////////////////////////
	
	function get_relcategory() { 
		if(empty($this->_format_data[3500][150][0])) return false;
		
		$where = $this->get_tab2comma($this->_format_data[3500][150][0]);
		$where = "'" . ereg_replace(",", "','", $where) . "'";
		$sql = "SELECT def_code FROM inci_report_materials WHERE grp_code = 3500 and easy_item_code = 150 and easy_code in ({$where})";
		$this->_db->query($sql);
		return $this->_db->getAll();
	}
	
	function get_relcategory_text() { 
		return $this->_format_data[3500][160][0];
	}
	
	function get_type() { 
		if(empty($this->_format_data[920][10][0])) return false;
		
		$sql = "SELECT code FROM inci_sub_item_2010 WHERE sub_item_id = {$this->_format_data[920][10][0]}";
		$this->_db->query($sql);
		return $this->_db->getOne();
	}
	
	function get_scene() { 
		if(empty($this->_format_data[950][10][0])) return false;
		
		$sql = "SELECT code FROM inci_sub_item_2010 WHERE sub_item_id = {$this->_format_data[950][10][0]}";
		$this->_db->query($sql);
		return $this->_db->getOne();
	}
	
	function get_detail() { 
		if(empty($this->_format_data[980][10][0])) return false;
		
		$sql = "SELECT code FROM inci_sub_item_2010 WHERE sub_item_id = {$this->_format_data[980][10][0]}";
		$this->_db->query($sql);
		return $this->_db->getOne();
	}

	function get_type_text() { 
		return $this->_format_data[930][10][0];
	}
	
	function get_scene_text() { 
		return $this->_format_data[960][10][0];
	}
	
	function get_detail_text() { 
		return $this->_format_data[990][10][0];
	}
	
	function get_medical_supplies_name() { 
		return $this->_format_data[1000][5][0];
	}
	
	function get_medical_supplies_code() { 
		return $this->_format_data[1000][10][0];
	}
	
	function get_medical_supplies_manufacture() { 
		return $this->_format_data[1000][15][0];
	}
	
	function get_medical_supplies() { 
		$arr_data = array('name' => $this->get_medical_supplies_name(),
			'code' => $this->get_medical_supplies_code(),
			'manufacture' => $this->get_medical_supplies_manufacture());
		
		return $arr_data;
	}
	
	function get_material_name() { 
		return $this->_format_data[1000][20][0];
	}
	
	function get_material_manufacture() { 
		return $this->_format_data[1000][25][0];
	}
	
	function get_material_purchase() { 
		return $this->_format_data[1000][30][0];
	}
	
	function get_material() { 
		$arr_data = array('name' => $this->get_material_name(), 
			'manufacture' => $this->get_material_manufacture(), 
			'purchase' => $this->get_material_purchase());
		
		return $arr_data;
	}
	
	function get_equipment_name() { 
		return $this->_format_data[1000][35][0];
	}
	
	function get_equipment_manufacture() { 
		return $this->_format_data[1000][40][0];
	}
	
	function get_equipment_dateofmanufacture() { 
		return $this->_format_data[1000][45][0];
	}
	
	function get_equipment_purchase() { 
		return $this->_format_data[1000][50][0];
	}
	
	function get_equipment_maintenance() { 
		return $this->_format_data[1000][55][0];
	}
	
	function get_equipment() { 
		$arr_data = array('name' => $this->get_equipment_name(), 
			'manufacture' => $this->get_equipment_manufacture(), 
			'dateofmanufacture' => $this->get_equipment_dateofmanufacture(), 
			'purchase' => $this->get_equipment_purchase(), 
			'maintenance' => $this->get_equipment_maintenance());
		
		return $arr_data;
	}
	
	function get_purpose() { 
		return $this->_format_data[500][10][0];
	}
	
	function get_contenttext() { 
		return $this->_format_data[520][30][0];
	}
	
	function get_factor() { 
		if(empty($this->_format_data[600][10][0])) return false;
		
		$where = $this->get_tab2comma($this->_format_data[600][10][0]);
		$where = "'" . ereg_replace(",", "','", $where) . "'";
		$sql = "SELECT def_code FROM inci_report_materials WHERE grp_code = 600 and easy_item_code = 10 and easy_code in ({$where})";
		$this->_db->query($sql);
		return $this->_db->getAll();
	}
	
	function get_factor_text() { 
		return $this->_format_data[600][20][0];
	}
	
	function get_background() { 
		return $this->_format_data[610][20][0];
	}
	
	function get_committee() { 
		switch($this->_format_data[590][90][0]) { 
		case '1' :
		case '3' :
			$code = '07';
			break;
		case '2' :
		case '4' :
			$code = '08';
			break;
		case '5' :
			$code = '05';
			break;
		case '6' :
			$code = '06';
			break;
		case '99' :
			$code = '99';
			break;
		}
		
		return $code;
	}
	
	function get_committee_text() { 
		return $this->_format_data[590][91][0];
	}

	function get_improvementtext() { 
		return $this->_format_data[620][30][0];
	}
	
	function get_patient_level() { 
		return $this->_format_data[90][10][0];
	}
	
	function get_mode() { 
		if ($this->_folder_id != "") {
			$sql = "SELECT hiyari_data_type FROM inci_hiyari_xml_folder WHERE folder_id = " . $this->_folder_id;
			$this->_db->query($sql);
			return $this->_db->getOne();
		} else {
			return "";
		}
	}
}
?>
