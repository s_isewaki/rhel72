<?
ob_start();

require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	header("HTTP/1.0 403 Forbidden");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 32, $fname);
if ($checkauth == "0") {
	header("HTTP/1.0 403 Forbidden");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 文書カテゴリIDから委員会IDを取得
$sql = "select lib_link_id from libcate";
$cond = "where lib_cate_id = $category";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	header("HTTP/1.0 500 Internal Server Error");
	exit;
}
$project_id = pg_fetch_result($sel, 0, "lib_link_id");

// 責任者の職員IDと名前を取得
$sql = "select empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm from empmst inner join project on project.pjt_response = empmst.emp_id";
$cond = "where project.pjt_id = $project_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	header("HTTP/1.0 500 Internal Server Error");
	exit;
}
$response_id = pg_fetch_result($sel, 0, "emp_id");
$response_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");

// メンバーの職員IDと名前を取得
$sql = "select empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, promember.pjt_member_id from empmst inner join promember on promember.emp_id = empmst.emp_id";
$cond = "where promember.pjt_id = $project_id order by promember.pjt_member_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	header("HTTP/1.0 500 Internal Server Error");
	exit;
}
$members = array();
while ($row = pg_fetch_array($sel)) {
	$members[] = array(
		"id" => $row["emp_id"],
		"name" => $row["emp_lt_nm"] . " " . $row["emp_ft_nm"]
	);
}

// データベース接続を閉じる
pg_close($con);

ob_clean();

header('Content-Type: text/xml; charset=EUC-JP');
echo('<');
echo('?xml version="1.0" encoding="EUC-JP"?');
echo(">\n");
?>
<ProjectMembers>
<Member>
<ID><? echo($response_id); ?></ID>
<Name><? echo($response_nm); ?></Name>
</Member>
<?
foreach ($members as $member) {
	echo("<Member>\n");
	echo("<ID>{$member["id"]}</ID>\n");
	echo("<Name>{$member["name"]}</Name>\n");
	echo("</Member>\n");
}
?>
</ProjectMembers>
