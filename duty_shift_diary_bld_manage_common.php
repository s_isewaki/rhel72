<?php

//固定職種（看護師、准看護師）ID設定(emp_job)
define("DEF_KANGO",		"看護師"); //看護師
define("DEF_JUNKANGO",	"准看護師"); //准看護師

//固定業務形態（常勤、非常勤）ID設定(jobmst)
define("DEF_JOKIN",		1); //常勤
define("DEF_HIJOKIN",	2); //非常勤
define("DEF_TANJIKAN",	3); //短時間正職員 集計は常勤と同じ行とする 20120413

//勤怠パターンテーブル登録用フラグ設定(shift_pattern_id)
define("YAKIN_AKE",   "0001"); //夜勤明
define("NIKKIN",      "0002"); //日勤
define("HANKIN",      "0003"); //半勤
define("YAKIN_IRI",   "0004"); //夜勤入
define("KOUKYU",      "0005"); //公休
define("NENKYU",      "0006"); //年休
define("OTHER",       "9999"); //その他

$arr_title = array(
        "head1"=>"看護職員状況",	//0
        "head2-1"=>"夜勤明",
        "head2-2"=>"日勤",
        "head2-3"=>"半勤",
        "head2-4"=>"夜勤入",
        "head2-5"=>"休・その他",	//5
        "head3-1"=>"看",
        "head3-2"=>"准",
        "head3-3"=>"補",
        "head3-4"=>"小計",
        "head3-5"=>"公休",			//10
        "head3-6"=>"年休",
        "head3-7"=>"その他",        //追加
        "gokei"=>"合計",
        "jokin"=>"常勤",
        "hijokin"=>"非常勤"
        );

$arr_title2 = array(
        "duty"=>"当直医師",	
        "internist"=>"内科",
        "surgeon"=>"外科",
        "charge"=>"日勤責任者"
        );

$arr_title3 = array(
        "head1"=>"重症・要注意・手術・その他",
        "head2-1"=>"種別",
        "head2-2"=>"病室",
        "head2-3"=>"時間",
        "head2-4"=>"患者氏名",
        "head2-5"=>"年齢",	
        "head2-6"=>"性別",	
        "head2-7"=>"病名",	
        "head2-8"=>"記事（主症状・転帰等）",	
        "head3-1"=>"巡視",
        "head3-2"=>"時刻",
        "head3-3"=>"看護師サイン",
        "head3-4"=>"看護師長サイン",
        "head3-5"=>"特記事項（人事・行事・研修・その他）"
        );

/**
 * get_building_manage_diary_data 表示するデータを取得し、表示エリアごとに配列別で振り分ける
 *
 * @param mixed $con 接続情報
 * @param mixed $start_date 指定された日付
 * @param mixed $group_id 指定されたグループID
 * @param mixed $fname 画面名
 * @return mixed 下記配列
 *	$arr_ya_ake_jokin_kango  = array();			//夜勤明け、常勤、看護師メンバー
 *	$arr_ya_ake_hi_jokin_kango  = array();		//夜勤明け、非常勤、看護師メンバー
 *	
 *	$arr_ya_ake_jokin_jun_kango  = array();		//夜勤明け、常勤、准看護師メンバー
 *	$arr_ya_ake_hi_jokin_jun_kango  = array();	//夜勤明け、非常勤、准看護師メンバー
 *	
 *	$arr_ya_ake_jokin_hojo  = array();			//夜勤明け、常勤、補助メンバー
 *	$arr_ya_ake_hi_jokin_hojo  = array();		//夜勤明け、非常勤、補助メンバー
 *	
 *	$arr_ya_iri_jokin_kango  = array();			//夜勤入り、常勤、看護師メンバー
 *	$arr_ya_iri_hi_jokin_kango  = array();		//夜勤入り、非常勤、看護師メンバー
 *	
 *	$arr_ya_iri_jokin_jun_kango  = array();		//夜勤入り、常勤、准看護師メンバー
 *	$arr_ya_iri_hi_jokin_jun_kango  = array();	//夜勤入り、非常勤、准看護師メンバー
 *	
 *	$arr_ya_iri_jokin_hojo  = array();			//夜勤入り、常勤、補助メンバー
 *	$arr_ya_iri_hi_jokin_hojo  = array();		//夜勤入り、非常勤、補助メンバー
 *
 *	$arr_nikkin_jokin_kango  = array();			//日勤、常勤、看護師メンバー
 *	$arr_nikkin_hi_jokin_kango  = array();		//日勤、非常勤、看護師メンバー
 *	
 *	$arr_nikkin_jokin_jun_kango  = array();		//日勤、常勤、准看護師メンバー
 *	$arr_nikkin_hi_jokin_jun_kango  = array();	//日勤、非常勤、准看護師メンバー
 *	
 *	$arr_nikkin_jokin_hojo  = array();			//日勤、常勤、補助メンバー
 *	$arr_nikkin_hi_jokin_hojo  = array();		//日勤、非常勤、補助メンバー
 *	
 *	$arr_hankin_jokin_kango  = array();			//半勤、常勤、看護師メンバー
 *	$arr_hankin_hi_jokin_kango  = array();		//半勤、非常勤、看護師メンバー
 *	
 *	$arr_hankin_jokin_jun_kango  = array();		//半勤、常勤、准看護師メンバー
 *	$arr_hankin_hi_jokin_jun_kango  = array();	//半勤、非常勤、准看護師メンバー
 *	
 *	$arr_hankin_jokin_hojo  = array();			//半勤、常勤、補助メンバー
 *	$arr_hankin_hi_jokin_hojo  = array();		//半勤、非常勤、補助メンバー
 *	
 *	$arr_jokin_kokyu  = array();				//常勤、公休メンバー
 *	$arr_jokin_nenkyu  = array();				//常勤、年休メンバー
 *	
 *	$arr_hi_jokin_kokyu  = array();				//非常勤、公休メンバー
 *	$arr_hi_jokin_nenkyu  = array();			//非常勤、年休メンバー
 *
 *  $arr_jokin_other  = array();				//常勤、その他メンバー
 *  $arr_hi_jokin_other  = array();				//非常勤、その他メンバー
 *
 */
function get_building_manage_diary_data($con,$start_date,$group_id,$fname) 
{
    
    $arr_ya_ake_jokin_kango  = array();			//夜勤明け、常勤、看護師メンバー
    $arr_ya_ake_hi_jokin_kango  = array();		//夜勤明け、非常勤、看護師メンバー
    
    $arr_ya_ake_jokin_jun_kango  = array();		//夜勤明け、常勤、准看護師メンバー
    $arr_ya_ake_hi_jokin_jun_kango  = array();	//夜勤明け、非常勤、准看護師メンバー
    
    $arr_ya_ake_jokin_hojo  = array();			//夜勤明け、常勤、補助メンバー
    $arr_ya_ake_hi_jokin_hojo  = array();		//夜勤明け、非常勤、補助メンバー
    
    $arr_ya_iri_jokin_kango  = array();			//夜勤入り、常勤、看護師メンバー
    $arr_ya_iri_hi_jokin_kango  = array();		//夜勤入り、非常勤、看護師メンバー
    
    $arr_ya_iri_jokin_jun_kango  = array();		//夜勤入り、常勤、准看護師メンバー
    $arr_ya_iri_hi_jokin_jun_kango  = array();	//夜勤入り、非常勤、准看護師メンバー
    
    $arr_ya_iri_jokin_hojo  = array();			//夜勤入り、常勤、補助メンバー
    $arr_ya_iri_hi_jokin_hojo  = array();		//夜勤入り、非常勤、補助メンバー
    
    $arr_nikkin_jokin_kango  = array();			//日勤、常勤、看護師メンバー
    $arr_nikkin_hi_jokin_kango  = array();		//日勤、非常勤、看護師メンバー
    
    $arr_nikkin_jokin_jun_kango  = array();		//日勤、常勤、准看護師メンバー
    $arr_nikkin_hi_jokin_jun_kango  = array();	//日勤、非常勤、准看護師メンバー
    
    $arr_nikkin_jokin_hojo  = array();			//日勤、常勤、補助メンバー
    $arr_nikkin_hi_jokin_hojo  = array();		//日勤、非常勤、補助メンバー
    
    $arr_hankin_jokin_kango  = array();			//半勤、常勤、看護師メンバー
    $arr_hankin_hi_jokin_kango  = array();		//半勤、非常勤、看護師メンバー
    
    $arr_hankin_jokin_jun_kango  = array();		//半勤、常勤、准看護師メンバー
    $arr_hankin_hi_jokin_jun_kango  = array();	//半勤、非常勤、准看護師メンバー
    
    $arr_hankin_jokin_hojo  = array();			//半勤、常勤、補助メンバー
    $arr_hankin_hi_jokin_hojo  = array();		//半勤、非常勤、補助メンバー
    
    $arr_jokin_kokyu  = array();					//常勤、公休メンバー
    $arr_jokin_nenkyu  = array();					//常勤、年休メンバー
    
    $arr_hi_jokin_kokyu  = array();				//非常勤、公休メンバー
    $arr_hi_jokin_nenkyu  = array();				//非常勤、年休メンバー
    
    //その他 20130205
    $arr_jokin_other  = array();					//常勤、その他メンバー
    $arr_hi_jokin_other  = array();				//非常勤、その他メンバー
    
    $arr_hojo_id   = array();//看護補助者のIDのリスト
    
    $arr_kinmu_pattern  = array();				//勤務パターン
    
    
    
    ////看護師、准看護師job_id(emp_id)取得
    $arr_kango_id = get_nurse_job_id($con, $fname, 1);
    $arr_jun_kango_id = get_nurse_job_id($con, $fname, 2);
    
    ////補助者リスト取得
    $sql ="select job_id from duty_shift_rpt_assist_job";
    $cond = "where 1 = 1";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) 
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $num5 = pg_numrows($sel);
    //取得したデータを配列へ設定する
    for ($i=0; $i<$num5; $i++) 
    {
        $arr_hojo_id[$i] = pg_result($sel,$i,"job_id");
    }
    
    ////勤務パターンリスト取得(duty_shift_group_shift_pattern)
    $sql ="select a.shift_pattern_id, a.atdptn_id, a.reason_id from duty_shift_group_shift_pattern a, duty_shift_group b";
    $cond = " where b.group_id = '".$group_id."' and CAST(b.pattern_id AS varchar) = a.group_id";
    
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) 
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    
    $num1 = pg_numrows($sel);
    
    //取得したデータを配列へ設定する
    for ($i=0; $i<$num1; $i++) 
    {
        $arr_kinmu_pattern[$i]["shift_pattern_id"] = pg_result($sel,$i,"shift_pattern_id");
        $arr_kinmu_pattern[$i]["atdptn_id"] = pg_result($sel,$i,"atdptn_id");
        $arr_kinmu_pattern[$i]["reason_id"] = pg_result($sel,$i,"reason_id");
    }
    
    
    $dy_yyyy = intval(substr($start_date, 0, 4));
    $dy_mm = intval(substr($start_date, 4, 2));
    
    /*
    	***		病棟ごとに勤務実績を取得する方法	***
    	勤務実績(atdbkrslt)に病棟ID(group_id)をもっていないので以下の方法で取得する
    	１．指定された病棟にデフォルトで設定されている社員（勤務対象者）を抽出する（duty_shift_staffテーブル）
    	２．応援追加されている場合があるのでその対象者を取得するために（duty_shift_plan_staff）を対象の月、病棟で抽出する
    	３．「１」と「２」で取得した社員IDと対象月で勤務実績(atdbkrslt)に実績があるものが「病棟ごとの勤務実績」となる
    		→「２」については必ず実績があるはず、「１」は実績が無い可能性あり
    */
    
    $arr_shain_work  = array();				//勤務した可能性のある社員リスト
    
    
    /*１．指定された病棟にデフォルトで設定されている社員（勤務対象者）を抽出する*/
    $sql ="select a.group_id , a.emp_id from duty_shift_staff a ";
    $cond = "where a.group_id = '".$group_id."'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) 
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $num2 = pg_numrows($sel);
    
    //取得したデータを配列へ設定する
    for ($i=0; $i<$num2; $i++) 
    {
        $arr_shain_work[$i] = pg_result($sel,$i,"emp_id");
        
    }
    
    
    
    /*２．応援追加されている場合があるのでその対象者を取得するために（duty_shift_plan_staff）を対象の月、病棟で抽出する*/
    //     $sql ="select a.group_id , a.emp_id from duty_shift_plan_staff a, duty_shift_group b ";
    //     $cond = "where a.group_id = '".$group_id."' and a.duty_yyyy = '".$dy_yyyy."' and duty_mm = '".$dy_mm."' and a.group_id = b.group_id";
    $sql = "select a.emp_id from duty_shift_plan_assist a left join atdbk b on b.emp_id = a.emp_id and b.date = a.duty_date";
    $cond = "where a.duty_date = '$start_date' and a.group_id = '$group_id' and main_group_id <> '$group_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) 
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $num3 = pg_numrows($sel);
    
    //取得したデータを配列へ設定する
    for ($h=0; $h<$num3; $h++) 
    {
        
        $fall_flg = 0;
        for ($k=0; $k<$num2; $k++) 
        {
            
            //デフォルトの勤務者（病棟職員設定者）とシフト登録者（duty_shift_plan_staff）と
            //かぶっていることがあるのでその場合はリストに登録しない
            if($arr_shain_work[$k] == pg_result($sel,$h,"emp_id"))
            {
                //勤務実績対象者としてかぶってしまったのでリストに登録しない
                $fall_flg = 1;
            }
            
        }
        
        if($fall_flg < 1)
        {
            //シフト登録者（duty_shift_plan_staff）となっていて、デフォルトの勤務者（病棟職員設定者）ではない→応援追加者
            //なのでリストに登録する
            $arr_shain_work[$i] = pg_result($sel,$h,"emp_id");
            $i++;
        }
        
        
    }
    
    
    
    $arr_kinmu_result = array();
    
    //取得したデータ（指定病棟単位実績）から勤務実績情報を取得してそれを配列へ設定する
    for ($i=0; $i<count($arr_shain_work); $i++) 
    {
        
        
        //初期化
        $w_emp_id	= "";
        $w_emp_job = "";
        $w_duty_form = "";
        
        $arr_kinmu_result = array(); 
        
        //対象(実績がある可能性のある)社員IDを取得
        $w_emp_id		= $arr_shain_work[$i];
        
        $sql ="select a.emp_id, a.reason, a.pattern, b.emp_lt_nm, b.emp_ft_nm, b.emp_job, c.duty_form from atdbkrslt a, empmst b, empcond c ";
        $cond = "where a.date = '".$start_date."' and a.emp_id = b.emp_id and a.emp_id = c.emp_id and a.emp_id = '".$w_emp_id."'";
        $sel2 = select_from_table($con, $sql, $cond, $fname);
        if ($sel2 == 0) 
        {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        
        $w_emp_id		= pg_result($sel2,0,"emp_id");
        $w_emp_job		= pg_result($sel2,0,"emp_job");
        $w_duty_form	= pg_result($sel2,0,"duty_form");
        
        //勤務実績を配列に設定
        $arr_kinmu_result["pattern"] = pg_result($sel2,0,"pattern");
        $arr_kinmu_result["reason"] = pg_result($sel2,0,"reason");
        $arr_kinmu_result["emp_lt_nm"] = pg_result($sel2,0,"emp_lt_nm");
        $arr_kinmu_result["emp_ft_nm"] = pg_result($sel2,0,"emp_ft_nm");
        
        //常勤メンバーの設定、短時間正職員追加 20120413
        if($w_duty_form == DEF_JOKIN || $w_duty_form == DEF_TANJIKAN)
        {
            //看護師
            if (in_array($w_emp_job, $arr_kango_id)) {
                //「夜勤明」リスト設定
                $arr_ya_ake_jokin_kango = fix_display_data($arr_ya_ake_jokin_kango,YAKIN_AKE,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「日勤」リスト設定
                $arr_nikkin_jokin_kango = fix_display_data($arr_nikkin_jokin_kango,NIKKIN,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「半勤」リスト設定
                $arr_hankin_jokin_kango = fix_display_data($arr_hankin_jokin_kango,HANKIN,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「夜勤入」リスト設定
                $arr_ya_iri_jokin_kango = fix_display_data($arr_ya_iri_jokin_kango,YAKIN_IRI,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「公休」リスト設定
                $arr_jokin_kokyu = fix_display_data($arr_jokin_kokyu,KOUKYU,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「年休」リスト設定
                $arr_jokin_nenkyu = fix_display_data($arr_jokin_nenkyu,NENKYU,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「その他」リスト設定
                $arr_jokin_other = fix_display_data($arr_jokin_other,OTHER,$arr_kinmu_pattern,$arr_kinmu_result);
            }
            //准看護師
            else if (in_array($w_emp_job, $arr_jun_kango_id)) {
                //「夜勤明」リスト設定
                $arr_ya_ake_jokin_jun_kango = fix_display_data($arr_ya_ake_jokin_jun_kango,YAKIN_AKE,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「日勤」リスト設定
                $arr_nikkin_jokin_jun_kango = fix_display_data($arr_nikkin_jokin_jun_kango,NIKKIN,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「半勤」リスト設定
                $arr_hankin_jokin_jun_kango = fix_display_data($arr_hankin_jokin_jun_kango,HANKIN,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「夜勤入」リスト設定
                $arr_ya_iri_jokin_jun_kango = fix_display_data($arr_ya_iri_jokin_jun_kango,YAKIN_IRI,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「公休」リスト設定
                $arr_jokin_kokyu = fix_display_data($arr_jokin_kokyu,KOUKYU,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「年休」リスト設定
                $arr_jokin_nenkyu = fix_display_data($arr_jokin_nenkyu,NENKYU,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「その他」リスト設定
                $arr_jokin_other = fix_display_data($arr_jokin_other,OTHER,$arr_kinmu_pattern,$arr_kinmu_result);
                
            }
            //看護補助者
            else if (in_array($w_emp_job, $arr_hojo_id)) {
                
                //「夜勤明」リスト設定
                $arr_ya_ake_jokin_hojo = fix_display_data($arr_ya_ake_jokin_hojo,YAKIN_AKE,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「日勤」リスト設定
                $arr_nikkin_jokin_hojo = fix_display_data($arr_nikkin_jokin_hojo,NIKKIN,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「半勤」リスト設定
                $arr_hankin_jokin_hojo = fix_display_data($arr_hankin_jokin_hojo,HANKIN,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「夜勤入」リスト設定
                $arr_ya_iri_jokin_hojo = fix_display_data($arr_ya_iri_jokin_hojo,YAKIN_IRI,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「公休」リスト設定
                $arr_jokin_kokyu = fix_display_data($arr_jokin_kokyu,KOUKYU,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「年休」リスト設定
                $arr_jokin_nenkyu = fix_display_data($arr_jokin_nenkyu,NENKYU,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「その他」リスト設定
                $arr_jokin_other = fix_display_data($arr_jokin_other,OTHER,$arr_kinmu_pattern,$arr_kinmu_result);
                
            }
        }
        else
        {
            //非常勤メンバーの設定
            //看護師
            if (in_array($w_emp_job, $arr_kango_id)) {
                //「夜勤明」リスト設定
                $arr_ya_ake_hi_jokin_kango = fix_display_data($arr_ya_ake_hi_jokin_kango,YAKIN_AKE,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「日勤」リスト設定
                $arr_nikkin_hi_jokin_kango = fix_display_data($arr_nikkin_hi_jokin_kango,NIKKIN,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「半勤」リスト設定
                $arr_hankin_hi_jokin_kango = fix_display_data($arr_hankin_hi_jokin_kango,HANKIN,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「夜勤入」リスト設定
                $arr_ya_iri_hi_jokin_kango = fix_display_data($arr_ya_iri_hi_jokin_kango,YAKIN_IRI,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「公休」リスト設定
                $arr_hi_jokin_kokyu = fix_display_data($arr_hi_jokin_kokyu,KOUKYU,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「年休」リスト設定
                $arr_hi_jokin_nenkyu = fix_display_data($arr_hi_jokin_nenkyu,NENKYU,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「その他」リスト設定
                $arr_hi_jokin_other = fix_display_data($arr_hi_jokin_other,OTHER,$arr_kinmu_pattern,$arr_kinmu_result);
            }
            //准看護師
            else if (in_array($w_emp_job, $arr_jun_kango_id)) {
                //「夜勤明」リスト設定
                $arr_ya_ake_hi_jokin_jun_kango = fix_display_data($arr_ya_ake_hi_jokin_jun_kango,YAKIN_AKE,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「日勤」リスト設定
                $arr_nikkin_hi_jokin_jun_kango = fix_display_data($arr_nikkin_hi_jokin_jun_kango,NIKKIN,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「半勤」リスト設定
                $arr_hankin_hi_jokin_jun_kango = fix_display_data($arr_hankin_hi_jokin_jun_kango,HANKIN,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「夜勤入」リスト設定
                $arr_ya_iri_hi_jokin_jun_kango = fix_display_data($arr_ya_iri_hi_jokin_jun_kango,YAKIN_IRI,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「公休」リスト設定
                $arr_hi_jokin_kokyu = fix_display_data($arr_hi_jokin_kokyu,KOUKYU,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「年休」リスト設定
                $arr_hi_jokin_nenkyu = fix_display_data($arr_hi_jokin_nenkyu,NENKYU,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「その他」リスト設定
                $arr_hi_jokin_other = fix_display_data($arr_hi_jokin_other,OTHER,$arr_kinmu_pattern,$arr_kinmu_result);
                
            }
            //看護補助者
            else if (in_array($w_emp_job, $arr_hojo_id)) {
                
                //「夜勤明」リスト設定
                $arr_ya_ake_hi_jokin_hojo = fix_display_data($arr_ya_ake_hi_jokin_hojo,YAKIN_AKE,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「日勤」リスト設定
                $arr_nikkin_hi_jokin_hojo = fix_display_data($arr_nikkin_hi_jokin_hojo,NIKKIN,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「半勤」リスト設定
                $arr_hankin_hi_jokin_hojo = fix_display_data($arr_hankin_hi_jokin_hojo,HANKIN,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「夜勤入」リスト設定
                $arr_ya_iri_hi_jokin_hojo = fix_display_data($arr_ya_iri_hi_jokin_hojo,YAKIN_IRI,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「公休」リスト設定
                $arr_hi_jokin_kokyu = fix_display_data($arr_hi_jokin_kokyu,KOUKYU,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「年休」リスト設定
                $arr_hi_jokin_nenkyu = fix_display_data($arr_hi_jokin_nenkyu,NENKYU,$arr_kinmu_pattern,$arr_kinmu_result);
                
                //「その他」リスト設定
                $arr_hi_jokin_other = fix_display_data($arr_hi_jokin_other,OTHER,$arr_kinmu_pattern,$arr_kinmu_result);
                
            }
            
        }
        
    }
    
    return array($arr_ya_ake_jokin_kango,$arr_ya_ake_hi_jokin_kango,
            $arr_ya_ake_jokin_jun_kango,$arr_ya_ake_hi_jokin_jun_kango,
            $arr_ya_ake_jokin_hojo,$arr_ya_ake_hi_jokin_hojo,
            $arr_ya_iri_jokin_kango,$arr_ya_iri_hi_jokin_kango,
            $arr_ya_iri_jokin_jun_kango,$arr_ya_iri_hi_jokin_jun_kango,
            $arr_ya_iri_jokin_hojo,$arr_ya_iri_hi_jokin_hojo,
            $arr_nikkin_jokin_kango,$arr_nikkin_hi_jokin_kango,
            $arr_nikkin_jokin_jun_kango,$arr_nikkin_hi_jokin_jun_kango,
            $arr_nikkin_jokin_hojo,$arr_nikkin_hi_jokin_hojo,
            $arr_hankin_jokin_kango,$arr_hankin_hi_jokin_kango,
            $arr_hankin_jokin_jun_kango,$arr_hankin_hi_jokin_jun_kango,
            $arr_hankin_jokin_hojo,$arr_hankin_hi_jokin_hojo,
            $arr_jokin_kokyu,$arr_jokin_nenkyu,
            $arr_hi_jokin_kokyu,$arr_hi_jokin_nenkyu,
            $arr_jokin_other,$arr_hi_jokin_other			
            );
}


/**
 * fix_display_data 勤務実績から勤務パターンを取得し、勤務パターンが指定した配列の種類と同じであれば配列に名前を設定する
 *
 * @param mixed $arr_set 表示用配列
 * @param mixed $define_pattern 指定されたシフトパターンID、9999:その他
 * @param mixed $arr_kinmu_pattern 表示用設定シフトパターン配列
 * @param mixed $arr_kinmu_result 勤務実績配列
 * @return mixed $arr_set　表示用配列
 *
 */
function fix_display_data($arr_set,$define_pattern,$arr_kinmu_pattern,$arr_kinmu_result) 
{
    
    $cnt = count($arr_set);
    $arr_cnt = 0;
    $cnt_kinmu = count($arr_kinmu_pattern);
    
    //その他以外の場合
    if ($define_pattern != OTHER) {
        for ($ii=0; $ii<$cnt_kinmu; $ii++) 
        {
            if($arr_kinmu_pattern[$ii]["shift_pattern_id"] == $define_pattern)
            {
                
                if($define_pattern == KOUKYU || $define_pattern == NENKYU)
                {
                    //公休・年休情報の設定
                    if($arr_kinmu_pattern[$ii]["reason_id"] == $arr_kinmu_result["reason"])
                    {
                        //メンバー確定(名前設定)
                        $arr_set[$cnt]["emp_lt_nm"] = $arr_kinmu_result["emp_lt_nm"];
                        $arr_set[$cnt]["emp_ft_nm"] = $arr_kinmu_result["emp_ft_nm"];
                        
                        break;
                    }
                }
                else
                {
                    //公休・年休以外（勤務）情報の設定
                    if($arr_kinmu_pattern[$ii]["atdptn_id"] == $arr_kinmu_result["pattern"])
                    {
                        //メンバー確定(名前設定)
                        $arr_set[$cnt]["emp_lt_nm"] = $arr_kinmu_result["emp_lt_nm"];
                        $arr_set[$cnt]["emp_ft_nm"] = $arr_kinmu_result["emp_ft_nm"];
                        
                        break;
                    }
                }
            }
        }
    }
    //その他の場合
    else {
        //公休・年休・指定パターン内にない場合その他とする
        $find_flg = false;
        for ($ii=0; $ii<$cnt_kinmu; $ii++) {
            if(($arr_kinmu_pattern[$ii]["reason_id"] != "" &&
                $arr_kinmu_pattern[$ii]["reason_id"] == $arr_kinmu_result["reason"]) ||
                    $arr_kinmu_pattern[$ii]["atdptn_id"] == $arr_kinmu_result["pattern"]) {
                $find_flg = true;
                break;
            }
        }
        if (!$find_flg) {
            //メンバー確定(名前設定)
            $arr_set[$cnt]["emp_lt_nm"] = $arr_kinmu_result["emp_lt_nm"];
            $arr_set[$cnt]["emp_ft_nm"] = $arr_kinmu_result["emp_ft_nm"];
        }
    }
    return $arr_set;
}
/***
 * 看護職員の職種取得
 */
function get_nurse_job_id($con, $fname, $switch) {
    
    $nurse = array();
    
    switch ($switch) {
        case 1:
            $sql = "select * from duty_shift_rpt_nurse order by no";
            break;
        case 2:
            $sql = "select * from duty_shift_rpt_sub_nurse order by no";
            break;
        default:
            $sql = "select * from duty_shift_rpt_assist_job order by no";
            break;
    }
    
    $sel = select_from_table($con, $sql, '', $fname);
    if ($sel === 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_assoc($sel)) {
        $nurse[] = $row['job_id'];
    }
    return $nurse;
}
