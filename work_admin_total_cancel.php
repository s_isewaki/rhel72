<?php
ini_set("max_execution_time", "0");

require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0" && $mmode != "usr") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

// チェックされた職員をループ
foreach ($cancel_id as $tmp_emp_id) {

	// 締めデータ（日）を削除
	$sql = "delete from wktotalp";
	$cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$c_yyyymm'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rolllback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$sql = "delete from wktotald";
	$cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$c_yyyymm'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rolllback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	// 締めデータ（月）を削除
	$sql = "delete from atdbkclose";
	$cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$c_yyyymm'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rolllback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$sql = "delete from wktotalm";
	$cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$c_yyyymm'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rolllback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
    // 残業データを削除
    $sql = "delete from wktotalovtm";
    $cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$c_yyyymm'";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_query($con, "rolllback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 勤務時間集計画面を再表示
//$url_emp_ids = "";
//foreach ($emp_id as $tmp_id) {
//	$url_emp_ids .= "&emp_id[]=$tmp_id";
//}
//$url_srch_name = urlencode($srch_name);
//echo("<script type=\"text/javascript\">location.href = 'work_admin_total.php?session=$session$url_emp_ids&yyyymm=$yyyymm&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&srch_id=$srch_id';</script>");
?>
<form name="book" action="work_admin_total.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<?
foreach ($emp_id as $tmp_id) {
    echo("<input type=\"hidden\" name=\"emp_id[]\" value=\"$tmp_id\">\n");
}
?>

<input type="hidden" name="c_yyyymm" value="<? echo($c_yyyymm); ?>">
<input type="hidden" name="yyyymm" value="<? echo($yyyymm); ?>">
<input type="hidden" name="srch_id" value="<? echo($srch_id); ?>">
<input type="hidden" name="srch_name" value="<? echo($srch_name); ?>">
<input type="hidden" name="cls" value="<? echo($cls); ?>">
<input type="hidden" name="atrb" value="<? echo($atrb); ?>">
<input type="hidden" name="dept" value="<? echo($dept); ?>">
<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="shift_group_id" value="<? echo($shift_group_id); ?>">
<input type="hidden" name="csv_layout_id" value="<? echo($csv_layout_id); ?>">
<input type="hidden" name="duty_form_jokin" value="<? echo($duty_form_jokin); ?>">
<input type="hidden" name="duty_form_hijokin" value="<? echo($duty_form_hijokin); ?>">
<input type="hidden" name="sus_flg" value="<? echo($sus_flg); ?>">
<input type="hidden" name="mmode" value="<? echo($mmode); ?>">
</form>

<?
echo("<script language='javascript'>document.book.submit();</script>");
