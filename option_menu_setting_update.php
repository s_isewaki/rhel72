<?

require("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 重複チェック
$valid_menu_ids = array();
foreach ($menu_ids as $menu_id) {
    if ($menu_id == "-") {
        continue;
    }

    if (in_array($menu_id, $valid_menu_ids)) {
        echo("<script type=\"text/javascript\">alert('ヘッダメニューの選択肢が重複しています。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }

    array_push($valid_menu_ids, $menu_id);
}

// 登録値の編集
if ($sidemenu_position == "") {
    $sidemenu_position = "1";
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

// ヘッダメニュー設定をDELETE
$sql = "delete from headermenu";
$cond = "where emp_id = '$emp_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// ヘッダメニュー設定をINSERT
$menu_order = 1;
foreach ($valid_menu_ids as $menu_id) {
    $sql = "insert into headermenu values (";
    $content = array($emp_id, $menu_id, $menu_order);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $menu_order++;
}

// オプション情報を更新
$sql = "update option set";
$set = array("sidemenu_show_flg", "sidemenu_position");
$setvalue = array($sidemenu_show_flg, $sidemenu_position);
$cond = "where emp_id = '$emp_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 全画面をリフレッシュ
$f_url = urlencode("option_menu_setting.php?session=$session");
echo("<script type=\"text/javascript\">parent.location.href = 'main_menu.php?session=$session&f_url=$f_url';</script>");
