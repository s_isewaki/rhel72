<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$hiyari = check_authority($session, 48, $fname);
if ($hiyari == "0")
{
	showLoginPage();
	exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==============================
//ポストバック時の処理
//==============================
if( $postback != "")
{
	
	//==============================
	//割当て処理
	//==============================
	//SQLのin句
	$in=" in (";
	$count=count($linkeis);
	for($i=0;$i<$count;$i++){
		if(!$linkeis[$i]==""){
			$in.="'$linkeis[$i]'";
			if($i<($count-1)){
				$in.=",";
			}
		}
	}
	$in.=")";
	
	
	//全て未選択フラグ
	$is_no_select = false;
	if($in == " in ()")
	{
		$is_no_select = true;;
	}
	
	
	
//使用中であっても紐付け解除可能となった。	
//	//使用中チェック　･･･使用中様式を紐付け解除することはできない。
//	//↓メールで使用されていないかチェック
//	$tbl = "select * from inci_mail_send_info left join inci_easyinput_set on inci_mail_send_info.eis_id = inci_easyinput_set.eis_id";
//	$cond = " where inci_mail_send_info.job_id = '$job_id' ";
//	if(!$is_no_select)
//	{
//		$cond .= " and not inci_easyinput_set.eis_no $in";
//	}
//	$sel = select_from_table($con,$tbl,$cond,$fname);
//	if($sel == 0){
//		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
//		echo("<script language='javascript'>showErrorPage(window);</script>");
//		pg_close($con);
//		exit;
//	}
//	$num = pg_numrows($sel);
//	if($num > 0)
//	{
//		echo("<script language='javascript'>alert(\"使用済みの報告書様式を割り当て解除することはできません。\");</script>");
//		echo("<script language='javascript'>history.back();</script>");
//		pg_close($con);
//		exit;
//	}
//	//↓下書きファイルで使用されていないかチェック
//	$tbl = "select * from inci_report left join inci_easyinput_set on inci_report.eis_id = inci_easyinput_set.eis_id";
//	$cond = " where inci_report.job_id = '$job_id' ";
//	if(!$is_no_select)
//	{
//		$cond .= " and not inci_easyinput_set.eis_no $in";
//	}
//	$sel = select_from_table($con,$tbl,$cond,$fname);
//	if($sel == 0){
//		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
//		echo("<script language='javascript'>showErrorPage(window);</script>");
//		pg_close($con);
//		exit;
//	}
//	$num = pg_numrows($sel);
//	if($num > 0)
//	{
//		echo("<script language='javascript'>alert(\"使用済みの報告書様式を割り当て解除することはできません。\");</script>");
//		echo("<script language='javascript'>history.back();</script>");
//		pg_close($con);
//		exit;
//	}





	
	
	// トランザクションの開始
	pg_query($con, "begin");
	
	//inci_set_job_linkテーブルより、対象の職種のデフォルト様式番号を取得
	$tbl = "select inci_set_job_link.eis_no from inci_set_job_link ";
	$cond = " where inci_set_job_link.job_id = '$job_id' and default_flg";
	$sel = select_from_table($con,$tbl,$cond,$fname);
	if($sel == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);
	$old_default_eis_no = "";
	if($num != 0)
	{
		$old_default_eis_no = pg_result($sel,0,"eis_no");
	}
	
	
	//新デフォルト様式番号の決定
	$new_default_eis_no = "";
	$count=count($linkeis);
	for($i=0;$i<$count;$i++){
		if(!$linkeis[$i]==""){
			$eis_no = $linkeis[$i];//対象様式番号
	
			//旧デフォルトが今回も指定されている場合はそれをデフォルトとする。
			if($eis_no == $old_default_eis_no)
			{
				$new_default_eis_no = $old_default_eis_no;
				break;
			}
			//先頭のテンプレートをデフォルトテンプレートとする。(旧デフォルトが未指定の場合)
			if($new_default_eis_no == "")
			{
				$new_default_eis_no = $eis_no;
			}
		}
	}
	

	//inci_set_job_linkテーブルより、対象の職種の紐付けデータを全削除
	$delete_status="delete from inci_set_job_link where inci_set_job_link.job_id = '$job_id' ";
	$result_delete=pg_exec($con,$delete_status);
	if($result_delete==false){
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		pg_query($con, "rolllback");
		pg_close($con);
		exit;
	}
	
	
	
	//inci_set_job_linkテーブルへ、紐付け情報を登録
	$count=count($linkeis);
	for($i=0;$i<$count;$i++){
		if(!$linkeis[$i]==""){
			$eis_no = $linkeis[$i];//対象様式番号
	
			//デフォルト判定
			$default_flg = "f";
			if($eis_no == $new_default_eis_no)
			{
				$default_flg = "t";
			}
	
			$intbl = "insert into inci_set_job_link (job_id,eis_no,default_flg) values(";
			$content = array($job_id,$eis_no,$default_flg);
			$ins = insert_into_table($con,$intbl,$content,$fname);
			if ($ins == 0) {
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				pg_query($con, "rolllback");
				pg_close($con);
				exit;
			}
	
		}
	}

	// トランザクションのコミット
	pg_query($con, "commit");
	
	//==============================
	//トップ画面に遷移
	//==============================
	?>
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<script language='javascript'>
	location.href="hiyari_easyinput_set_main.php?session=<?=$session ?>";
	</script>
	</head>
	<body>
	</body>
	</html>
	<?
	exit;
	
}

//==============================
//職種名を取得
//==============================
$job_name = get_job_name($con,$job_id,$fname);
$job_name_html = htmlspecialchars($job_name);


//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 様式割当</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">




</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

.in_list {border-collapse:collapse;}
.in_list td {border:#FFFFFF solid 0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<?
show_hiyari_header($session,$fname,true,array('job_id' => $job_id));
?>
<!-- ヘッダー END -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<!-- 本体 START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>



<!-- 外の枠 START -->
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="600" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">	



<!-- 送信領域 START-->
<form name="form1" action="hiyari_easyinput_set_job_link.php" method="post">
<input type="hidden" name="postback" value="true">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="job_id" value="<?=$job_id?>">


<!-- 一覧 START-->
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22">
	<td width="30" align="left" bgcolor="#DFFFDC" >
		&nbsp
	</td>
	<td width="570" align="left" bgcolor="#DFFFDC">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$job_name_html?>の報告書様式</font>
	</td>
</tr>

<?

//様式一覧取得の取得
$sql = "";
$sql .= " select eis_all.*,(case when use_eis.eis_no is null then false else true end) as job_use from";
$sql .= " (";
$sql .= " select * from inci_easyinput_set where not del_flag order by order_no";
$sql .= " ) eis_all";
$sql .= " left join";
$sql .= " (";
$sql .= " select eis_no from inci_set_job_link where job_id = $job_id";
$sql .= " ) use_eis";
$sql .= " on eis_all.eis_no = use_eis.eis_no";
$sel_eis = select_from_table($con,$sql,"",$fname);

//診療区分ループ(一覧用)
$eis_count = pg_numrows($sel_eis);
for($i=0;$i<$eis_count;$i++)
{
	
	//職種情報
	$eis_no      = pg_result($sel_eis,$i,"eis_no");
	$eis_name    = pg_result($sel_eis,$i,"eis_name");
	$eis_name_html = htmlspecialchars($eis_name);
	$job_use     = pg_result($sel_eis,$i,"job_use");
	$checked_element = "";
	if($job_use == "t")
	{
		$checked_element = "checked";
	}
?>

<tr height="22">
<td align="center" bgcolor="#FFFFFF">
	<input type="checkbox" name="linkeis[]" value="<?=$eis_no?>" <?=$checked_element?>>
</td>
<td align="left" bgcolor="#FFFFFF">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$eis_name_html?></font>
</td>
</tr>

<?
}
?>
</table>

<!-- 一覧 END -->


<!-- 送信ボタン START -->
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><input type="submit" value="更新"></font></td>
</tr>
</table>
<!-- 送信ボタン END -->



</form>
<!-- 送信領域 END -->



</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<!-- 外の枠 END -->

</td>
</tr>
</table>
<!-- 本体 END -->

<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->

</td>
</tr>
</table>
<!-- BODY全体 END -->

</body>
</html>
<?



//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>
