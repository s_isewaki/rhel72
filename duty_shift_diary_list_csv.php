<?
ob_start();

require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
//require("show_address.ini");
require_once("duty_shift_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_diary_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 69, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// ログインユーザの職員ID・氏名を取得
///-----------------------------------------------------------------------------
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

///-----------------------------------------------------------------------------
//ＤＢ(empmst)より職員情報を取得
//ＤＢ(wktmgrp)より勤務パターン情報を取得
///-----------------------------------------------------------------------------

$data_emp = $obj->get_empmst_array("");
$data_wktmgrp = $obj->get_wktmgrp_array();
///-----------------------------------------------------------------------------
// 勤務シフトグループ情報を取得
///-----------------------------------------------------------------------------
$group_array = $obj->get_duty_shift_group_array("", $data_emp, $data_wktmgrp);
// 更新権限あり、職員設定されているグループ取得
$group_array = $obj->get_valid_group_array($group_array, $emp_id, "", "");
if (count($group_array) <= 0) {
	$err_flg_1 = "1";
} else {

//更新権限があるグループを設定

	for ($i=0; $i<count($group_array); $i++) {
		$person_charge_id = $group_array[$i]["person_charge_id"];			//シフト管理者ID（責任者）
		$caretaker_id = $group_array[$i]["caretaker_id"];					//シフト管理者ID（代行者）
		$caretaker2_id = $group_array[$i]["caretaker2_id"];
		$caretaker3_id = $group_array[$i]["caretaker3_id"];
		$caretaker4_id = $group_array[$i]["caretaker4_id"];
		$caretaker5_id = $group_array[$i]["caretaker5_id"];
		$caretaker6_id = $group_array[$i]["caretaker6_id"];
		$caretaker7_id = $group_array[$i]["caretaker7_id"];	
		$caretaker8_id = $group_array[$i]["caretaker8_id"];
		$caretaker9_id = $group_array[$i]["caretaker9_id"];
		$caretaker10_id = $group_array[$i]["caretaker10_id"];	
		
		if (($emp_id == $person_charge_id) || ($emp_id == $caretaker_id) || ($emp_id == $caretaker2_id) || ($emp_id == $caretaker3_id) || ($emp_id == $caretaker4_id) || ($emp_id == $caretaker5_id) || ($emp_id == $caretaker6_id) || ($emp_id == $caretaker7_id) || ($emp_id == $caretaker8_id) || ($emp_id == $caretaker9_id) || ($emp_id == $caretaker10_id)) {
			$group_array[$i]["create_flg"] = "1";
			
		}
	}

}

if (empty($start_date)){
	$start_date = date('Ymd');
	$duty_yyyy = date('Y');
	$duty_mm = date('m');	
}else{
	$duty_yyyy = substr($start_date, 0, 4);
	$duty_mm = substr($start_date, 4, 2);
}
	
$data_atdptn_all = $obj->get_atdptn_array("");
$data_pattern_all = $obj->get_duty_shift_pattern_array("", $data_atdptn_all);
$reason_2_array = $obj->get_reason_2("1");

if ($group_id == "all") {
	for ($i=0; $i<count($group_array); $i++) {
		//対象年月を取得
		$arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $group_array[$i]["start_month_flg1"], $group_array[$i]["start_day1"], $group_array[$i]["month_flg1"], $group_array[$i]["end_day1"]);
		$target_date = get_target_date($arr_date, $duty_yyyy, $duty_mm, $start_date);
		$group_array[$i]["target_date"] = $target_date["year"] . sprintf("%02d", $target_date["month"]) ;
	}
}else{
	for ($i=0; $i<count($group_array); $i++) {
		if ($group_id == $group_array[$i]["group_id"]) {
			//対象年月を取得
			$arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $group_array[$i]["start_month_flg1"], $group_array[$i]["start_day1"], $group_array[$i]["month_flg1"], $group_array[$i]["end_day1"]);
			$target_date = get_target_date($arr_date, $duty_yyyy, $duty_mm, $start_date);
			$group_array[$i]["target_date"] = $target_date["year"] . sprintf("%02d", $target_date["month"]) ;
				
			break;
		}
	}
}

$data = get_diary_list($con, $fname, $group_id, $group_array, $start_date, $data_pattern_all, $reason_2_array, $data_atdptn_all);

// 情報をCSV形式で取得
$csv = get_list_csv($con, $session, $data);

// データベース接続を閉じる
pg_close($con);

// CSVを出力
$file_name = "shift_diary_list_" . $start_date. ".csv";
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

// 情報をCSV形式で取得
function get_list_csv($con, $session, $data) {

	$titles = array(
		"シフトグループ",	//0
		"職員ID",
		"職種",
		"役職",
		"氏名",					//5
		"出勤時刻",
		"勤務シフト実績",
		"出勤実績",				//10
		"事由実績",
		"部署"					//15
	);
//print_r($data);

	$num = count($data);

	$item_num = count($titles);

	$title_flg = "t";

	$buf = "";
	if ($title_flg == "t") {
		for ($j=0;$j<$item_num;$j++) {
			if ($j != 0) {
				$buf .= ",";
			}
			$buf .= $titles[$j];
		}
		$buf .= "\r\n";
	}

//echo("b=$buf");
	for($i=0;$i<$num;$i++){
		for ($j=0;$j<$item_num;$j++) {
			if ($j != 0) {
				$buf .= ",";
			}
			//部署
			if ($j == 9) {
				$buf .= str_replace("&gt;", ">", $data[$i][$j]);
			} else {
				$buf .= $data[$i][$j];
			}
		}
		$buf .= "\r\n";
	}
	return mb_convert_encoding($buf, "SJIS", "EUC-JP");

}

?>
