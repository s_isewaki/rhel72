<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 端数処理</title>
<?
require("about_session.php");
require("about_authority.php");
require("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("atdbk_menu_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

// 端数処理設定を取得
//$sql = "select fraction1, fraction1_min, fraction2, fraction2_min, fraction3, fraction4, fraction4_min, fraction5, fraction5_min, fraction_over_unit, fraction_out_unit, fraction_ret_unit from timecard";
$sql = "select fraction1, fraction1_min, fraction2, fraction2_min, fraction3, fraction4, fraction4_min, fraction5, fraction5_min, fraction_over_unit from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$fraction1 = pg_fetch_result($sel, 0, "fraction1");
	$fraction1_min = pg_fetch_result($sel, 0, "fraction1_min");
	$fraction2 = pg_fetch_result($sel, 0, "fraction2");
	$fraction2_min = pg_fetch_result($sel, 0, "fraction2_min");
	$fraction3 = pg_fetch_result($sel, 0, "fraction3");
	$fraction4 = pg_fetch_result($sel, 0, "fraction4");
	$fraction4_min = pg_fetch_result($sel, 0, "fraction4_min");
	$fraction5 = pg_fetch_result($sel, 0, "fraction5");
	$fraction5_min = pg_fetch_result($sel, 0, "fraction5_min");
	$fraction_over_unit = pg_fetch_result($sel, 0, "fraction_over_unit");
//	$fraction_out_unit = pg_fetch_result($sel, 0, "fraction_out_unit");
//	$fraction_ret_unit = pg_fetch_result($sel, 0, "fraction_ret_unit");
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	setMinDisabled(document.update.fraction1, document.update.fraction1_min);
	setMinDisabled(document.update.fraction2, document.update.fraction2_min);
	setMinDisabled(document.update.fraction4, document.update.fraction4_min);
	setMinDisabled(document.update.fraction5, document.update.fraction5_min);
}

function setMinDisabled(sel, sel_min) {
	var disabled = (sel.selectedIndex == 0 || sel.selectedIndex == 1);
	if (disabled) {
		sel_min.selectedIndex = 0;
	}
	sel_min.disabled = disabled;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<!-- atdbk_timecard.phpを atdbk_timecard_shift.phpに変更 20130129 -->
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="update" action="./timecard_fraction_update_exe.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>毎日の出退勤時刻</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="160" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤時刻</font></td>
<td><select name="fraction1" onchange="setMinDisabled(this, this.form.fraction1_min);"><option value="--"><option value="1"<? if ($fraction1 == "1") {echo(" selected");} ?>>行わない<option value="2"<? if ($fraction1 == "2") {echo(" selected");} ?>>繰り下げ<option value="3"<? if ($fraction1 == "3") {echo(" selected");} ?>>繰上げ</select>&nbsp;<select name="fraction1_min"><? show_fraction_min_options($fraction1_min); ?></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤時刻</font></td>
<td><select name="fraction2" onchange="setMinDisabled(this, this.form.fraction2_min);"><option value="--"><option value="1"<? if ($fraction2 == "1") {echo(" selected");} ?>>行わない<option value="2"<? if ($fraction2 == "2") {echo(" selected");} ?>>繰り下げ<option value="3"<? if ($fraction2 == "3") {echo(" selected");} ?>>繰上げ</select>&nbsp;<select name="fraction2_min"><? show_fraction_min_options($fraction2_min); ?></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻早退時の端数処理</font></td>
<td><select name="fraction3"><option value="--"><option value="1"<? if ($fraction3 == "1") {echo(" selected");} ?>>遅刻早退時もこの設定を適用する<option value="2"<? if ($fraction3 == "2") {echo(" selected");} ?>>遅刻早退時は端数処理を行わない</select></td>
</tr>
<? /*
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外出時刻単位</font></td>
<td><select name="fraction_out_unit"><option value="--">
<option value="15"<? if ($fraction_out_unit == "15") {echo(" selected");} ?>>15分
<option value="30"<? if ($fraction_out_unit == "30") {echo(" selected");} ?>>30分
</select>&nbsp;</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退復（呼出）時刻単位</font></td>
<td><select name="fraction_ret_unit"><option value="--">
<option value="15"<? if ($fraction_ret_unit == "15") {echo(" selected");} ?>>15分
<option value="30"<? if ($fraction_ret_unit == "30") {echo(" selected");} ?>>30分
</select>&nbsp;</td>
</tr>
*/ ?>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>月集計値の端数処理</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="160" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">実働・残業・休日出勤</font></td>
<td><select name="fraction4" onchange="setMinDisabled(this, this.form.fraction4_min);"><option value="--"><option value="1"<? if ($fraction4 == "1") {echo(" selected");} ?>>行わない<option value="2"<? if ($fraction4 == "2") {echo(" selected");} ?>>切り上げ<option value="3"<? if ($fraction4 == "3") {echo(" selected");} ?>>切捨て</select>&nbsp;<select name="fraction4_min"><? show_fraction_min_options($fraction4_min); ?></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻・早退</font></td>
<td><select name="fraction5" onchange="setMinDisabled(this, this.form.fraction5_min);"><option value="--"><option value="1"<? if ($fraction5 == "1") {echo(" selected");} ?>>行わない<option value="2"<? if ($fraction5 == "2") {echo(" selected");} ?>>切り上げ<option value="3"<? if ($fraction5 == "3") {echo(" selected");} ?>>切捨て</select>&nbsp;<select name="fraction5_min"><? show_fraction_min_options($fraction5_min); ?></select></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>残業時刻の端数処理</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="160" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業時刻の申請単位</font></td>
<td><select name="fraction_over_unit"><option value="--">
<option value="5"<? if ($fraction_over_unit == "5") {echo(" selected");} ?>>5分
<option value="10"<? if ($fraction_over_unit == "10") {echo(" selected");} ?>>10分
<option value="15"<? if ($fraction_over_unit == "15") {echo(" selected");} ?>>15分
<option value="30"<? if ($fraction_over_unit == "30") {echo(" selected");} ?>>30分
</select>&nbsp;</td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_fraction_min_options($fraction) {
	$options = array(
		"A" => "5分",
		"B" => "10分",
		"1" => "15分",
		"C" => "20分",
		"2" => "30分",
		"D" => "40分",
		"E" => "45分",
		"3" => "60分"
	);

	echo("<option value=\"--\">\n");
	foreach ($options as $value => $text) {
		echo("<option value=\"$value\"");
		if ($value == $fraction) {echo(" selected");}
		echo(">$text\n");
	}
}
?>
