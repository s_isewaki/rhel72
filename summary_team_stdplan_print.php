<?php
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require("CustomMBFPDF.php");
$version = explode('.', phpversion());
if ($version && $version[0] >= 5 && $version[1] >= 1) {    // PHPExcelはPHP ver5.1以降利用可能
    require('duty_shift_menu_excel_5_workshop.php');
}

/**
 * チーム計画PDF作成クラス
 */
class CustomStdplanMBFPDF extends CustomMBFPDF
{
    /**
     * 左側出力エリアの幅
     *
     * @var float
     */
    var $widthLeft = 0.0;
    
    /**
     * 中央出力エリアの幅
     * 
     * @var float
     */
    var $widthCenter = 0.0;
    
    /**
     * 右側出力エリアの幅
     * 
     * @var float
     */
    var $widthRight = 0.0;
    
    /**
     * 出力エリアの高さ
     * 
     * @var float
     */
    var $height = 0.0;

    /**
     * コンストラクタ
     * 
     * @param string $ptifId      患者ID
     * @param float  $widthLeft   左側出力エリアの幅
     * @param float  $widthCenter 中央出力エリアの幅
     * @param float  $widthRight  右側出力エリアの幅
     * @param float  $height      出力エリアの高さ
     *
     * @return void
     */
    function CustomStdplanMBFPDF($ptifId, $widthLeft, $widthCenter, $widthRight, $height)
    {
        $this->widthLeft = $widthLeft;
        $this->widthCenter = $widthCenter;
        $this->widthRight = $widthRight;
        $this->height = $height;
        
        parent::CustomMBFPDF($ptifId);
    }
    
    /**
     * ヘッダー出力(オーバーライド)
     *
     * @return void
     */
    function Header()
    {
        // 枠線を描画(2ページ目以降)
        if ($this->PageNo() > 1) {
            $this->Cell($this->widthLeft, $this->height, '', 1);
            $this->Cell($this->widthCenter, $this->height, '', 1);
            $this->Cell($this->widthRight, $this->height, '', 1);
        }
    }
}

//====================================
// ページ名
//====================================
$fname = $PHP_SELF;

//====================================
// セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
// DBコネクション
//====================================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
// パラメータを取得
//====================================
$pt_id = isset($_GET['pt_id']) ? $_GET['pt_id'] : '';    // 患者ID
$team_id = isset($_GET['team_id']) ? intval($_GET['team_id']) : -1;    // 標準チーム計画id
$type = isset($_GET['type']) ? $_GET['type'] : 'pdf';    // 出力種別

//====================================
// 標準チーム計画データを取得
//====================================
$data = getTeamStandardPlanData($con, $pt_id, $team_id, $fname);
pg_close($con);    // DB切断
if ($data === false) {
    require_once("summary_common.ini");
    summary_common_show_error_page_and_die();
}

//====================================
// HTML又はExcelを出力
//====================================
// チーム情報
$teamProperties = array(
        array('key' => 'kyoyo', 'type' => 0, 'caption' => '共用'),
        array('key' => 'kango', 'type' => 1, 'caption' => '看護'),
        array('key' => 'kaigo', 'type' => 2, 'caption' => '介護'),
        array('key' => 'rihabiri', 'type' => 3, 'caption' => 'リハビリ')
);

if ($type == 'pdf') {
    // PDF作成
    createPDF($data);
} elseif ($type == 'excel') {
    // Excel作成
    createExcel($data);
} else {
    // 印刷用HTML作成
    createHTML($data);
}

/**
 * チーム計画データを取得する
 * 
 * @param resource $con     データベース接続リソース
 * @param string   $pt_id   患者ID
 * @param integer  $team_id 標準チーム計画id
 * @param string   $fname   ページ名
 * 
 * @return mixed 成功時:取得したレコードの配列/失敗時:false
 */
function getTeamStandardPlanData($con, $pt_id, $team_id, $fname)
{
    //====================================
    // 患者氏名を取得
    //====================================
    $sql = "SELECT ptif_id,ptif_lt_kaj_nm,ptif_ft_kaj_nm FROM ptifmst WHERE ptif_id = '$pt_id' ";
    $sel = select_from_table($con, $sql, '', $fname);
    if ($sel == 0) {
        return false;
    }
    if(pg_num_rows($sel) != 1) {
        $data['kname'] = '';
    } else {
        $ptif_lt_name = pg_fetch_result($sel, 0, 'ptif_lt_kaj_nm');
        $ptif_ft_name = pg_fetch_result($sel, 0, 'ptif_ft_kaj_nm');
        $data['kname'] = $ptif_lt_name . ' ' . $ptif_ft_name;
    }

    //====================================
    // プロブレム名を取得
    //====================================
    $sql = "SELECT problem FROM summary_problem_list ";
    $sql .= "INNER JOIN sum_med_std_team ON summary_problem_list.problem_id = sum_med_std_team.problem_id ";
    $sql .= "WHERE summary_problem_list.ptif_id = '$pt_id' AND sum_med_std_team.team_id = $team_id";
    $sel = select_from_table($con, $sql, '', $fname);
    if ($sel == 0) {
        return false;
    }
    if(pg_num_rows($sel) != 1) {
        $data['problem'] = '';
    } else {
        $data['problem'] = pg_fetch_result($sel, 0, 'problem');
    }
        
    //====================================
    // チーム計画マスターを取得
    //====================================
    $sql = <<<_SQL_END_
SELECT 
M.team_id,
problem_id,
ptif_id,
create_date,
emp_name,
M.emp_job,
def_value,
s_detail,
e_detail,
risk_detail,
outcome,
left_lock_flag,
lef_lock_time,
lef_lock_emp_id,
kyoyo_op,
kyoyo_tp,
kyoyo_ep,
c0_lock_flag,
c0_lock_time,
c0_lock_emp_id,
kango_op,
kango_tp,
kango_ep,
c1_lock_flag,
c1_lock_time,
c1_lock_emp_id,
kaigo_op,
kaigo_tp,
kaigo_ep,
c2_lock_flag,
c2_lock_time,
c2_lock_emp_id,
rihabiri_op,
rihabiri_tp,
rihabiri_ep,
c3_lock_flag,
c3_lock_time,
c3_lock_emp_id,
left_jobname||' '||left_emp_name AS left_creater_name,
kyoyo_jobname||' '||kyoyo_emp_name AS kyoyo_creater_name,
kango_jobname||' '||kango_emp_name AS kango_creater_name,
kaigo_jobname||' '||kaigo_emp_name AS kaigo_creater_name,
rihabiri_jobname||' '||rihabiri_emp_name AS rihabiri_creater_name
FROM sum_med_std_team AS M
LEFT OUTER JOIN sum_med_priactice0 AS P0 ON M.team_id=P0.team_id 
LEFT OUTER JOIN sum_med_priactice1 AS P1 ON M.team_id=P1.team_id 
LEFT OUTER JOIN sum_med_priactice2 AS P2 ON M.team_id=P2.team_id 
LEFT OUTER JOIN sum_med_priactice3 AS P3 ON M.team_id=P3.team_id 
_SQL_END_;
    $cond = " WHERE M.team_id={$team_id}";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        return false;
    }
    $mst = pg_fetch_assoc($sel);
    if ($mst !== false) {
        $data = array_merge($data, $mst);
    } else {
        $data = array_merge($data, array('ptif_id' => '',
                                         'create_date' => '',
                                         'emp_name' => '',
                                         'emp_job' => '',
                                         'def_value' => '',
                                         's_detail' => '',
                                         'e_detail' => '',
                                         'risk_detail' => '',
                                         'outcome' => '',
                                         'left_creater_name' => '',
                                         'kyoyo_op' => '',
                                         'kyoyo_tp' => '',
                                         'kyoyo_ep' => '',
                                         'kyoyo_creater_name' => '',
                                         'kango_op' => '',
                                         'kango_tp' => '',
                                         'kango_ep' => '',
                                         'kango_creater_name' => '',
                                         'kaigo_op' => '',
                                         'kaigo_tp' => '',
                                         'kaigo_ep' => '',
                                         'kaigo_creater_name' => '',
                                         'rihabiri_op' => '',
                                         'rihabiri_tp' => '',
                                         'rihabiri_ep' => '',
                                         'rihabiri_creater_name' => ''));
    }
    
    //====================================
    // 観察項目を取得
    //====================================
    $sql  = "SELECT MST.item_cd , TMP.item_name AS name FROM sum_med_std_obse AS MST ";
    $sql .= "INNER JOIN tmplitem AS TMP ON TMP.item_cd=MST.item_cd AND TMP.mst_cd='A263' ";
    $sql .= " WHERE team_id={$team_id} ORDER BY MST.update ";
    $sel = select_from_table($con, $sql, '', $fname);
    if ($sel == 0) {
        return false;
    }
    
    $observation_items = pg_fetch_all($sel);
    if ($observation_items === false) {
        $observation_items = array();
    }
    $data['observation_items'] = $observation_items;
    
    //==========================================
    // 患者の問題及びアウトカムの更新記録リストを取得
    //==========================================
    $sql  = "SELECT update ,left_jobname||' '||left_emp_name AS name FROM sum_med_rec_left ";
    $sql .= " WHERE team_id=$team_id ORDER BY update DESC ";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        return false;
    }
    $left_update = pg_fetch_all($sel);
    if ($left_update === false) {
        $left_update = array();
    }
    $data['left_update'] = $left_update;
    
    //====================================
    // チーム実践の更新記録リストを取得
    //====================================
    // 共用
    $sql = "SELECT update_c0 AS update ,jobname||' '||emp_name AS name FROM sum_med_recorder0 ";
    $sql .= " WHERE team_id=$team_id ORDER BY update_c0 DESC";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        return false;
    }
    $center_rec_all0 = pg_fetch_all($sel);
    if ($center_rec_all0) {
        foreach ($center_rec_all0 as $value) {
            $center_update[0][] = array('name' => $value['name'], 'update' => $value['update']);
        }
    } else {
        $center_update[0] = array();
    }

    // 看護
    $sql = "SELECT update_c1 AS update,jobname||' '||emp_name AS name FROM sum_med_recorder1 AS M ";
    $sql .= " WHERE team_id=$team_id ORDER BY update_c1 DESC";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        return false;
    }
    $center_rec_all1 = pg_fetch_all($sel);
    if ($center_rec_all1) {
        foreach ($center_rec_all1 as $value) {
            $center_update[1][] = array('name' => $value['name'], 'update' => $value['update']);
        }
    } else {
        $center_update[1] = array();
    }

    // 介護
    $sql = "SELECT update_c2 AS update,jobname||' '||emp_name AS name  FROM sum_med_recorder2 AS M ";
    $sql .= " WHERE team_id=$team_id ORDER BY update_c2 DESC";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        return false;
    }
    $center_rec_all2 = pg_fetch_all($sel);
    if ($center_rec_all2) {
        foreach ($center_rec_all2 as $value) {
            $center_update[2][] = array('name' => $value['name'], 'update' => $value['update']);
        }
    } else {
        $center_update[2] = array();
    }

    // リハビリ
    $sql = "SELECT update_c3 AS update,jobname||' '||emp_name AS name FROM sum_med_recorder3 AS M ";
    $sql .= " WHERE team_id=$team_id ORDER BY update_c3 DESC";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        return false;
    }
    $center_rec_all3 = pg_fetch_all($sel);
    if ($center_rec_all3) {
        foreach ($center_rec_all3 as $value) {
            $center_update[3][] = array('name' => $value['name'], 'update' => $value['update']);
        }
    } else {
        $center_update[3] = array();
    }

    $data['center_update'] = $center_update;
    
    //====================================
    // 評価コメントを取得
    //====================================
    $sql = "SELECT team_type,emp_id,update,comment,jobname||' '||emp_name AS name FROM sum_med_std_asses ";
    $sql .= " WHERE team_id=$team_id ORDER BY team_type , update DESC";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        return false;
    }
    $fetch_all = pg_fetch_all($sel);
    if ($fetch_all) {
        foreach ($fetch_all as $value) {
            $type = intval($value['team_type']);
            $ass_list[$type][] = array('update' => $value['update'], 'name' => $value['name'], 'comment' => $value['comment']);
        }
    } else {
        $ass_list = array();
    }
    
    $data['ass_list'] = $ass_list;
    
    return $data;
}

/**
 * Excel作成
 * 
 * @param array $data チーム計画データ
 * 
 * @return void
 */
function createExcel($data)
{
    global $teamProperties;
    
    $fontMSPgothic = mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP');
    $fontMSPmincho = mb_convert_encoding('ＭＳ Ｐ明朝','UTF-8','EUC-JP');
    $excelObj = new ExcelWorkShop();    // PHP Excel 利用クラスのインスタンス
    $excelObj->SetSheetName(mb_convert_encoding(mb_convert_encoding('チーム計画', "sjis-win", "eucJP-win"), "UTF-8", "sjis-win"));
    $excelObj->PageRatio('USER', '92');
    $excelObj->SetMargin(1, 0.5, 1.5, 1.5, 0.5, 0.5);
    
    ////////// 行の高さ設定
    // 1ページ目
    $excelObj->SetRowDim(5, 3);    // タイトル
    $excelObj->SetRowDim(7, 190);
    $excelObj->SetRowDim(8, 190);
    $excelObj->SetRowDim(9, 190);
    $excelObj->SetRowDim(10, 190);
    
    // 2ページ目
    $excelObj->SetRowDim(11, 200);
    $excelObj->SetRowDim(12, 200);
    $excelObj->SetRowDim(13, 200);
    $excelObj->SetRowDim(14, 200);
    
    // 3ページ目
    $excelObj->SetRowDim(15, 200);
    $excelObj->SetRowDim(16, 200);
    $excelObj->SetRowDim(17, 200);
    $excelObj->SetRowDim(18, 200);
    
    // 4ページ目
    $excelObj->SetRowDim(19, 200);
    $excelObj->SetRowDim(20, 200);
    $excelObj->SetRowDim(21, 200);
    $excelObj->SetRowDim(22, 200);
    
    ////////// タイトル
    $excelObj->SetValueJP2('F1', 'チーム計画', 'BOLD', $fontMSPgothic, 14);
    
    ////////// ヘッダ部
    createHeaderCells($excelObj, $data, $fontMSPmincho);
    
    ////////// 患者の問題及びアウトカム
    $lastNumProblemAndOutcome = createProblemAndOutcomeCells($excelObj, $data, $fontMSPmincho);
    
    ////////// チーム実践
    $excelObj->CellMerge2('E6:H6', '', '', 'CCFFFF');
    $excelObj->SetValueJP2('E6', 'チーム実践', 'HCENTER BOLD', $fontMSPmincho, 10);
    
    $rowNum = 6;    // 行番号
    $isDataExist = false;
    // 拡張機能有効時のみ「共用」「看護」「介護」「リハビリ」のデータを出力
    // foreach ($teamProperties as $value) {
    //     if (isTeamPracticeDataExist($data, $value['key'])) {
    //         $rowNum = createTeamPracticeCells($excelObj, $data, $value['key'], $value['type'], $value['caption'], ++$rowNum, $fontMSPmincho);
    //         $isDataExist = true;
    //     }
    // }
    // 「共用」のみ出力(拡張機能有効時はコメントアウト)
    $value = $teamProperties[0];
    if (isTeamPracticeDataExist($data, $value['key'])) {
        $rowNum = createTeamPracticeCells($excelObj, $data, $value['key'], $value['type'], $value['caption'], ++$rowNum, $fontMSPmincho);
        $isDataExist = true;
    }
    
    // チーム実践の全てのデータが無かった場合は、空欄の「共用」を出力
    if (!$isDataExist) {
        $rowNum = createTeamPracticeCells($excelObj, $data, 'kyoyo', 0, '共用', ++$rowNum, $fontMSPmincho);
    }
    $lastNumTeamPractice = $rowNum;
    
    ////////// 評価日／解決・継続／サイン
    $excelObj->CellMerge2('I6:L6', '', '', 'CCFFFF');
    $excelObj->SetValueJP2('I6', '評価日／解決・継続／サイン', 'HCENTER BOLD', $fontMSPmincho, 10);
    $rowNum = 6;    // 行番号
    // 拡張機能有効時のみ「看護」「介護」「リハビリ」のデータを出力
    // for ($i = 1; $i < count($teamProperties); $i++) {
    //     if (array_key_exists($teamProperties[$i]['type'], $data['ass_list'])) {
    //         $rowNum = createAssessmentCells($excelObj, $data['ass_list'][$teamProperties[$i]['type']], $teamProperties[$i]['key'], $teamProperties[$i]['caption'], ++$rowNum, $fontMSPmincho);
    //     }
    // }
    // 「看護」のみ出力(拡張機能有効時はコメントアウト)
    $i = 1;
    if (array_key_exists($teamProperties[$i]['type'], $data['ass_list'])) {
        $rowNum = createAssessmentCells($excelObj, $data['ass_list'][$teamProperties[$i]['type']], $teamProperties[$i]['key'], $teamProperties[$i]['caption'], ++$rowNum, $fontMSPmincho);
    }
    $lastNumAssessment = $rowNum;
    
    ////////// 罫線
    $lastNumMax = max($lastNumProblemAndOutcome, $lastNumTeamPractice, $lastNumAssessment);
    $excelObj->SetArea("A6:L{$lastNumMax}");    // 外枠
    $excelObj->SetBorder2('thin', '', 'outline');
    $excelObj->SetArea('A6:L6');
    $excelObj->SetBorder2('thin', '', 'outline');
    $excelObj->SetArea("E6:H{$lastNumMax}");
    $excelObj->SetBorder2('thin', '', 'left');
    $excelObj->SetBorder2('thin', '', 'right');
    if ($lastNumMax > 10) {
        $excelObj->SetArea('E10:L10');
        $excelObj->SetBorder2('thin', '', 'bottom');
        $excelObj->SetArea('E11:L11');
        $excelObj->SetBorder2('thin', '', 'top');
    }
    if ($lastNumMax > 14) {
        $excelObj->SetArea('E14:L14');
        $excelObj->SetBorder2('thin', '', 'bottom');
        $excelObj->SetArea('E15:L15');
        $excelObj->SetBorder2('thin', '', 'top');
    }
    if ($lastNumMax > 18) {
        $excelObj->SetArea('E18:H18');
        $excelObj->SetBorder2('thin', '', 'bottom');
    }
    
    // エクセル画面を開いたときのカーソル位置を左上に設定
    $excelObj->SetArea('A1');
    $excelObj->SetPosV('');
    
    //====================================
    // ダウンロード
    //====================================
    $filename = 'summary_team_stdplan.xls';
    ob_clean();
    header("Content-Type: application/vnd.ms-excel");
    header('Content-Disposition: attachment; filename=' . $filename);
    header('Cache-Control: max-age=0');
    ob_end_flush();
    $excelObj->OutPut();
}

/**
 * ヘッダ部のセルを作成
 *
 * @param object $excelObj Excel生成インスタンス
 * @param array  $data     チーム計画データ
 * @param string $font     フォント
 *
 * @return void
 */
function createHeaderCells($excelObj, $data, $font)
{
    // 患者情報
    $excelObj->SetValueJP2('A2', sprintf('患者ID:%s', $data['ptif_id']), '', $font, 10);
    $excelObj->SetValueJP2('D2', sprintf('患者氏名:%s', $data['kname']), '', $font, 10);

    // プロブレム
    $excelObj->SetValueJP2('A3', sprintf('プロブレム:%s', $data['problem']), '', $font, 10);

    // 立案年月日
    $excelObj->SetValueJP2('A4', strftime('立案年月日:%Y年%m月%d日', strtotime($data['create_date'])), '', $font, 10);

    // 責任者
    $excelObj->SetValueJP2('D4', sprintf('責任者　職種:%s 氏名:%s', $data['emp_job'], $data['emp_name']), '', $font, 10);
}

/**
 * 患者の問題及びアウトカムのセルを作成
 *
 * @param object $excelObj Excel生成インスタンス
 * @param array  $data     チーム計画データ
 * @param string $font     フォント
 *
 * @return integer 最下行の行番号
 */
function createProblemAndOutcomeCells($excelObj, $data, $font)
{
    $rowNum = 6;
    
    $excelObj->CellMerge2("A{$rowNum}:D{$rowNum}", '', '', 'CCFFFF');
    $excelObj->SetValueJP2("A{$rowNum}", '患者の問題及びアウトカム', 'HCENTER BOLD', $font, 10);
    $rowNum++;
    $excelObj->CellMerge("A{$rowNum}:D{$rowNum}");
    $excelObj->SetValueJP2("A{$rowNum}", sprintf("■定義:%s\n%s", $data['problem'], replaceTagToBreak($data['def_value'])), 'WRAP TOP', $font, 10);
    $rowNum++;
    $excelObj->CellMerge("A{$rowNum}:D{$rowNum}");
    $excelObj->SetValueJP2("A{$rowNum}", sprintf("■S:症状・兆候\n%s", replaceTagToBreak($data['s_detail'])), 'WRAP TOP', $font, 10);
    $rowNum++;
    $excelObj->CellMerge("A{$rowNum}:D{$rowNum}");
    $excelObj->SetValueJP2("A{$rowNum}", sprintf("■E:原因・関連因子\n%s", replaceTagToBreak($data['e_detail'])), 'WRAP TOP', $font, 10);
    $rowNum++;
    $excelObj->CellMerge("A{$rowNum}:D{$rowNum}");
    $excelObj->SetValueJP2("A{$rowNum}", sprintf("■危険因子\n%s", replaceTagToBreak($data['risk_detail'])), 'WRAP TOP', $font, 10);
    $rowNum++;
    $excelObj->CellMerge("A{$rowNum}:D{$rowNum}");
    $excelObj->SetValueJP2("A{$rowNum}", sprintf("■アウトカム\n%s", replaceTagToBreak($data['outcome'])), 'WRAP TOP', $font, 10);
    $rowNum++;

    // 観察項目
    $startRow = $rowNum;
    $endRow = $rowNum + 1;
    $excelObj->CellMerge("A{$startRow}:D{$endRow}");
    $observation_items = '';
    foreach ($data['observation_items'] as $value) {
        if (!empty($observation_items)) {
            $observation_items .= "\n";
        }
        $observation_items .= $value['name'];
    }
    $excelObj->SetValueJP2("A{$startRow}", sprintf("■観察項目\n%s", $observation_items), 'WRAP TOP', $font, 10);
    $rowNum = $endRow + 1;

    // 患者の問題及びアウトカムの更新記録
    $excelObj->CellMerge("A{$rowNum}:D{$rowNum}");
    $left_update = sprintf('記載者:%s', $data['left_creater_name']);
    foreach ($data['left_update'] as $value) {
        if (!empty($left_update)) {
            $left_update .= "\n";
        }
        $left_update .= strftime("更新日:%Y/%m/%d %H:%M\n", strtotime($value['update']));
        $left_update .= sprintf('更新者:%s', $value['name']);
    }
    $excelObj->SetValueJP2("A{$rowNum}", $left_update, 'WRAP TOP', $font, 10);

    return $rowNum;
}

/**
 * チーム実践のセルを作成
 * 
 * @param object  $excelObj    Excel生成インスタンス
 * @param array   $data        チーム計画データ
 * @param string  $key         チーム区分のキーワード
 * @param integer $type        チーム区分
 * @param string  $caption     見出し
 * @param integer $startRowNum 開始行番号
 * @param string  $font        フォント
 *
 * @return integer 最下行の行番号
 */
function createTeamPracticeCells($excelObj, $data, $key, $type, $caption, $startRowNum, $font)
{
    $rowNum = $startRowNum;
    $excelObj->CellMerge("E{$rowNum}:H{$rowNum}");
    // 拡張機能有効時のみ見出し「共用」「看護」「介護」「リハビリ」を表示
//     $excelObj->SetValueJP2("E{$rowNum}", sprintf("＜{$caption}＞\n■O-P:観察計画\n%s", replaceTagToBreak($data["{$key}_op"])), 'WRAP TOP', $font, 10);
    $excelObj->SetValueJP2("E{$rowNum}", sprintf("■O-P:観察計画\n%s", replaceTagToBreak($data["{$key}_op"])), 'WRAP TOP', $font, 10);
    $rowNum++;
    $excelObj->CellMerge("E{$rowNum}:H{$rowNum}");
    $excelObj->SetValueJP2("E{$rowNum}", sprintf("■T-P:実践計画\n%s", replaceTagToBreak($data["{$key}_tp"])), 'WRAP TOP', $font, 10);
    $rowNum++;
    $excelObj->CellMerge("E{$rowNum}:H{$rowNum}");
    $excelObj->SetValueJP2("E{$rowNum}", sprintf("■E-P:教育・指導計画\n%s", replaceTagToBreak($data["{$key}_ep"])), 'WRAP TOP', $font, 10);
    $rowNum++;
    
    // 更新記録
    $excelObj->CellMerge("E{$rowNum}:H{$rowNum}");
    $update = sprintf('記載者:%s', $data["{$key}_creater_name"]);
    if (array_key_exists($type, $data['center_update'])) {
        foreach ($data['center_update'][$type] as $value) {
            if (!empty($update)) {
                $update .= "\n";
            }
            $update .= strftime("更新日:%Y/%m/%d %H:%M\n", strtotime($value['update']));
            $update .= sprintf('更新者:%s', $value['name']);
        }
    }
    $excelObj->SetValueJP2("E{$rowNum}", $update, 'WRAP TOP', $font, 10);

    return $rowNum;
}

/**
 * 評価日／解決・継続／サインのセルを作成
 *
 * @param object  $excelObj      Excel生成インスタンス
 * @param array   $each_ass_list 評価データ
 * @param string  $key           チーム区分のキーワード
 * @param string  $caption       見出し
 * @param integer $startRowNum   開始行番号
 * @param string  $font          フォント
 *
 * @return integer 最下行の行番号
 */
function createAssessmentCells($excelObj, $each_ass_list, $key, $caption, $startRowNum, $font)
{
    $rowNum = $startRowNum;
    // 拡張機能有効時のみ「共用」「看護」「介護」「リハビリ」を表示
    //    $assessment = "＜{$caption}＞";
    foreach ($each_ass_list as $value) {
        if (!empty($assessment)) {
            $assessment .= "\n";
        }
        $assessment .= sprintf("評価日:%s\n", date( "Y年m月d日", strtotime($value['update'])));
        $assessment .= sprintf("評価者:%s\n", $value['name']);
        $assessment .= sprintf("評価:%s", $value['comment']);
    }
    $startRow = $rowNum;
    $endRow = $rowNum + 3;
    $excelObj->CellMerge("I{$startRow}:L{$endRow}");
    $excelObj->SetValueJP2("I{$startRow}", $assessment, 'WRAP TOP', $font, 10);

    return $endRow;
}

/**
 * 印刷用HTML作成
 * 
 * @param array $data チーム計画データ
 * 
 * @return void
 */
function createHTML($data)
{
    global $teamProperties;
    
    $html = createHeaderHTML($data);
    $html .= <<<_HTML_
      <TABLE STYLE="width: 700px;" CLASS="print_table" STYLE="margin-top:4px">
        <TR>
          <TH STYLE="width: 240px;">患者の問題及びアウトカム</TH>
          <TH STYLE="width: 240px;">チーム実践</TH>
          <TH STYLE="widht: 220px;">評価日／解決・継続／サイン</TH>
        </TR>\n
_HTML_;

    // 患者の問題及びアウトカム
    $html .= createProblemAndOutcomeHTML($data);
    $html .= '          <TD VALIGN="TOP">' . "\n";
    
    // チーム実践
    $isDataExist = false;
    // foreach ($teamProperties as $value) {
    //     if (isTeamPracticeDataExist($data, $value['key'])) {
    //         $html .= createTeamPracticeHTML($data, $value['key'], $value['type'], $value['caption']);
    //         $isDataExist = true;
    //     }
    // }
    // 「共用」のみ出力(拡張機能有効時はコメントアウト)
    $value = $teamProperties[0];
    if (isTeamPracticeDataExist($data, $value['key'])) {
        $html .= createTeamPracticeHTML($data, $value['key'], $value['type'], $value['caption']);
        $isDataExist = true;
    }
    
    // チーム実践の全てのデータが無かった場合は、空欄の「共用」を出力
    if (!$isDataExist) {
        $html .= createTeamPracticeHTML($data, 'kyoyo', 0, '共用');
    }
    
    $html .= '          </TD>' . "\n";
    
    // 評価日／解決・継続／サイン
    $html .= '          <TD VALIGN="TOP" STYLE="width: 220px;">' . "\n";
    // 拡張機能有効時のみ「看護」「介護」「リハビリ」のデータを出力
    // for ($i = 1; $i < count($teamProperties); $i++) {
    //     if (array_key_exists($teamProperties[$i]['type'], $data['ass_list'])) {
    //         $html .= createAssessmentHTML($data['ass_list'][$teamProperties[$i]['type']], $teamProperties[$i]['key'], $teamProperties[$i]['type'], $teamProperties[$i]['caption']);
    //     }
    // }
    // 「看護」のみ出力(拡張機能有効時はコメントアウト)
    $i = 1;
    if (array_key_exists($teamProperties[$i]['type'], $data['ass_list'])) {
        $html .= createAssessmentHTML($data['ass_list'][$teamProperties[$i]['type']], $teamProperties[$i]['key'], $teamProperties[$i]['caption']);
    }
    
    $html .= <<<_HTML_
          </TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>
</BODY>
</HTML>
_HTML_;
    echo $html;
}

/**
 * HTMLヘッダ部作成
 * 
 * @param array $data チーム計画データ
 * 
 * @return string 作成したHTML
 */
function createHeaderHTML($data)
{
    $create_date = date("Y年m月d日", strtotime($data['create_date']));
    
    $html = <<<_HTML_
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML XMLNS="http://www.w3.org/1999/xhtml">
<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=EUC-JP">
<TITLE>CoMedix メドレポート｜標準チーム計画印刷</TITLE>
<LINK REL="STYLEsheet" TYPE="text/css" HREF="css/main_summary.css"/>
<style type="text/css">
.print_table { border-collapse:collapse; }
.print_table tr {}
.print_table th { padding: px 1px; font-size: 15px; font-weight: border; text-align: left; border: 1px solid black; }
.print_table td { padding: 1px; border: 1px solid black; }
.no_border_table { border-collapse: collapse; }
.no_border_table tr {}
.no_border_table th { padding:5px 1px; font-weight:normal; text-align:left; border:0px solid #ffffff; }
.no_border_table td { padding:1px; border:0px solid #ffffff; }
</style>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript">
window.onload = function() {
  self.print();
  self.close();
}
</script>
</HEAD>
<BODY>
<DIV STYLE="padding:4px">
<!-- ヘッダエリア -->
<script type="text/javascript" src="js/swapimg.js" charset="euc-jp"></script>
<TABLE STYLE="width: 650px; margin-top: 4px; margin-left: 4px;">
  <TR>
    <TD><!-- 外枠 -->
      <BR>
      <DIV STYLE="font-weight: bold; text-align: center; font-size: 20px;">チーム計画</DIV>
<!-- 冒頭部 -->
      <TABLE SYTLE="width: 650px;" BORDER="0"> 
        <TR><TD STYLE="font-family: ＭＳ Ｐゴシック, Osaka; font-size: 15px; font-weight: bold;">患者ID:{$data['ptif_id']}&nbsp&nbsp&nbsp患者氏名:&nbsp{$data['kname']}&nbsp</TD></TR>
        <TR><TD>プロブレム:{$data['problem']}</font></TD></TR>
      </TABLE>
      <TABLE SYTLE="width: 650px;" BORDER="0">
        <TR>
          <TD WIDTH="200">立案年月日&nbsp{$create_date}</TD>
          <TD WIDTH="400">&nbsp&nbsp&nbsp責任者&nbsp&nbsp職種:{$data['emp_job']}&nbsp氏名:{$data['emp_name']}</TD>
        </TR>
      </TABLE>\n
_HTML_;
    return $html;
}

/**
 * 患者の問題及びアウトカムのHTMLを作成
 * 
 * @param array $data チーム計画データ
 * 
 * @return string 作成したHTML
*/
function createProblemAndOutcomeHTML($data)
{
    $observationItemsHTML = createObservationItemsHTML($data['observation_items']);
    $recordHTML = createUpdateRecordHTML($data['left_update']);
    $html = <<<_HTML_
        <TR>
          <TD VALIGN="TOP" STYLE="width: 240px;">
            <TABLE CLASS="no_border_table">
              <TR><TD STYLE="font-weight: bold;">■定義:{$data['problem']}</TD></TR>
            </TABLE>
            <TABLE CLASS="no_border_table" STYLE="margin-top:4px">
              <TR><TD>{$data['def_value']}</TD></TR>
            </TABLE>
            <BR>
            <DIV STYLE="font-weight: bold;">■S:症状・兆候</DIV>
            <TABLE CLASS="no_border_table" STYLE="margin-top:4px">
              <TR><TD>{$data['s_detail']}</TD></TR>
            </TABLE>
            <BR>
            <DIV STYLE="font-weight: bold;">■E:原因・関連因子</DIV>
            <TABLE CLASS="no_border_table" STYLE="margin-top:4px">
              <TR><TD>{$data['e_detail']}</TD></TR>
            </TABLE>
            <BR>
            <DIV STYLE="font-weight: bold;">■危険因子</DIV>
            <TABLE CLASS="no_border_table" STYLE="margin-top:4px">
              <TR><TD>{$data['risk_detail']}</TD></TR>
            </TABLE>
            <BR>
            <DIV STYLE="font-weight: bold;">■アウトカム</DIV>
            <TABLE CLASS="no_border_table" STYLE="margin-top:4px">
              <TR><TD>{$data['outcome']}</TD></TR>
            </TABLE>
            <BR>
            <DIV STYLE="font-weight: bold;">■観察項目</DIV>
            <TABLE CLASS="no_border_table" STYLE="margin-top:4px">
              <TR><TD>{$observationItemsHTML}</TD></TR>
            </TABLE>
            <BR>
<!--記載者-->
            <TABLE CLASS="no_border_table">
              <TR>
                <TD>記載者:</TD>
                <TD>{$data['left_creater_name']}</TD>
              </TR>
            </TABLE>
<!--更新者-->
            <TABLE CLASS="no_border_table">
              <TR><TD>{$recordHTML}</TD></TR>
            </TABLE>
            <BR>
          </TD>\n
_HTML_;
    return $html;
}

/**
 * 観察項目HTML作成
 * 
 * @param array $observation_items 観察項目データ
 * 
 * @return string 作成したHTML
 */
function createObservationItemsHTML($observation_items)
{
    foreach ($observation_items as $value) {
        $observationItemsHTML .= '<div>';
        $observationItemsHTML .= $value['name'] . '</div>';
    }
    return $observationItemsHTML;
}

/**
 * 更新履歴HTML作成
 * 
 * @param array $update_record 更新履歴データ
 * 
 * @return string 作成したHTML
 */
function createUpdateRecordHTML($update_record)
{
    foreach ($update_record as $value) {
        if (!empty($recordHTML)) {
            $recordHTML .= '<BR>';
        }
        $recordHTML .= strftime('更新日:%Y/%m/%d %H:%M<BR>', strtotime($value['update']));
        $recordHTML .= sprintf('更新者:%s', $value['name']);
    }
    return $recordHTML;
}

/**
 * チーム実践HTMLを作成
 * 
 * @param array   $data    チーム計画データ
 * @param string  $key     チーム区分のキーワード
 * @param integer $type    チーム区分
 * @param string  $caption 見出し
 * 
 * @return string 作成したHTML
 */
function createTeamPracticeHTML($data, $key, $type, $caption)
{
    $op = $data["{$key}_op"];
    $tp = $data["{$key}_tp"];
    $ep = $data["{$key}_ep"];
    $creater_name = $data["{$key}_creater_name"];
    $update = createUpdateRecordHTML($data['center_update'][$type]);
    $html = <<<_HTML_
            <TABLE CLASS="no_border_table">
              <TR>
                <TD style="width: 240px;">\n
_HTML_;
    // 拡張機能有効時は「共用」「看護」「介護」「リハビリ」を出力する
//    $html .= "＜{$caption}＞&nbsp<BR>";
    $html .= <<<_HTML_
                  <DIV STYLE="font-weight: bold;">■O-P:観察計画</DIV>
                  <TABLE CLASS="no_border_table" STYLE="margin-top:4px">
                    <TR><TD>{$op}</TD></TR>
                  </TABLE>
                  <BR>
                  <DIV STYLE="font-weight: bold;">■T-P:実践計画</DIV>
                  <TABLE CLASS="no_border_table" STYLE="margin-top:4px">
                    <TR><TD>{$tp}</TD></TR>
                  </TABLE>
                  <BR>
                  <DIV STYLE="font-weight: bold;">■E-P:教育・指導計画</DIV>
                  <TABLE CLASS="no_border_table" STYLE="margin-top:4px">
                    <TR><TD>{$ep}</TD></TR>
                  </TABLE>
                  <BR>
<!--記載者-->
                  <TABLE CLASS="no_border_table">
                    <TR><TD>記載者:<TD>{$creater_name}</TD></TR>
                  </TABLE>
<!--更新者-->
                  <TABLE CLASS="no_border_table">
                    <TR><TD>{$update}</TD></TR>
                  </TABLE>
                  <BR>
                </TD>
              </TR>
            </TABLE>\n
_HTML_;
    return $html;
}

/**
 * 評価日／解決・継続／サインHTMLを作成
 * 
 * @param array   $each_ass_list 評価データ
 * @param string  $key           チーム区分のキーワード
 * @param string  $caption       見出し
 * 
 * @return string 作成したHTML
 */
function createAssessmentHTML($each_ass_list, $key, $caption)
{
    $assessment = '';
    foreach ($each_ass_list as $value) {
        if (!empty($assessment)) {
            $assessment .= '<BR>';
        }
        $assessment .= sprintf("評価日:%s<BR>", date( "Y年m月d日", strtotime($value['update'])));
        $assessment .= sprintf("評価者:%s<BR>", $value['name']);
        $assessment .= sprintf("評価:%s", $value['comment']);
    }

    // 拡張機能有効時のみ「看護」「介護」「リハビリ」のデータを出力
//    $html = "＜{$caption}＞<BR>";
    $html .= <<<_HTML_
            <TABLE CLASS="no_border_table" STYLE="margin-top:4px">
              <TR><TD>{$assessment}</TD></TR>
            </TABLE>
            <BR>\n
_HTML_;
    return $html;
}

/**
 * PDF作成
 * 
 * @param array $data チーム計画データ
 * 
 * @return void
 */
function createPDF($data)
{
    global $teamProperties;
    
    $width_left_box = 60.0;
    $width_center_box = 80.0;
    $width_right_box = 50.0;
    $offset = 10.0;
    
    $pdf = new CustomStdplanMBFPDF($data['ptif_id'], $width_left_box, $width_center_box, $width_right_box, 265.0);
    $pdf->AddMBFont(GOTHIC ,'SJIS');
    $pdf->AddMBFont(PMINCHO,'SJIS');
    $pdf->Open();
    $pdf->AddPage();

    // ヘッダー出力
    createHeaderPDF($pdf, $data);

    // 一覧ヘッダー
    $pdf->SetFont(GOTHIC, '', 10);
    $pdf->Cell($width_left_box, 5, '患者の問題及びアウトカム', 1, 0);
    $pdf->Cell($width_center_box, 5, 'チーム実践', 1, 0);
    $pdf->Cell($width_right_box, 5, '評価日／解決・継続／サイン', 1, 1);
    
    // 枠線を描画(1ページ目のみ)
    $pdf->Cell($width_left_box, 240, '', 1);
    $pdf->Cell($width_center_box, 240, '', 1);
    $pdf->Cell($width_right_box, 240, '', 1);
    
    $ypos = $pdf->GetY();
    $height = 4.0;    // 1行の高さ
    
    //====================================
    // ※改ページが発生する確率が低い順に出力する
    //====================================
    // 【出力1】:評価日／解決・継続／サイン
    // 拡張機能有効時のみ「看護」「介護」「リハビリ」のデータを出力
    // for ($i = 1; $i < count($teamProperties); $i++) {
    //     if (array_key_exists($teamProperties[$i]['type'], $data['ass_list'])) {
    //         createAssessmentPDF($pdf, $data['ass_list'][$teamProperties[$i]['type']], $teamProperties[$i]['key'],
    //             $teamProperties[$i]['caption'], $width_right_box, $height, $width_left_box + $width_center_box + $offset);
    //     }
    // }
    // 「看護」のみ出力(拡張機能有効時はコメントアウト)
    $i = 1;
    if (array_key_exists($teamProperties[$i]['type'], $data['ass_list'])) {
        createAssessmentPDF($pdf, $data['ass_list'][$teamProperties[$i]['type']], $teamProperties[$i]['key'],
            $teamProperties[$i]['caption'], $width_right_box, $height, $width_left_box + $width_center_box + $offset);
    }
    
    // 【出力2】:患者の問題及びアウトカム
    $pdf->SetY($ypos);
    createProblemAndOutcomePDF($pdf, $data, $width_left_box, $height);
    
    // 【出力3】:チーム実践
    $isDataExist = false;
    $pdf->SetY($ypos);
    // 拡張機能有効時のみ「共用」「看護」「介護」「リハビリ」のデータを出力
    // foreach ($teamProperties as $value) {
    //     if (isTeamPracticeDataExist($data, $value['key'])) {
    //         createTeamPracticePDF($pdf, $data, $value['key'], $value['type'], $value['caption'], $width_center_box, $height, $width_left_box + $offset);
    //         $isDataExist = true;
    //     }
    // }
    // 「共用」のみ出力(拡張機能有効時はコメントアウト)
    $value = $teamProperties[0];
    if (isTeamPracticeDataExist($data, $value['key'])) {
        createTeamPracticePDF($pdf, $data, $value['key'], $value['type'], $value['caption'], $width_center_box, $height, $width_left_box + $offset);
        $isDataExist = true;
    }
    
    // チーム実践の全てのデータが無かった場合は、空欄の「共用」を出力
    if (!$isDataExist) {
        createTeamPracticePDF($pdf, $data, 'kyoyo', 0, '共用', $width_center_box, $height, $width_left_box + $offset);
    }
    
    ob_clean();
    $pdf->Output();
}

/**
 * ヘッダ部のPDFを作成
 * 
 * @param object $pdf  PDF作成オブジェクト
 * @param array  $data チーム計画データ
 * 
 * @return void
 */
function createHeaderPDF($pdf, $data)
{
    // タイトル
    $pdf->SetFont(GOTHIC, '', 18);
    $pdf->Cell(0, 8, 'チーム計画', 0, 1, 'C');
    $pdf->SetFont(GOTHIC, '', 10);
    
    // 患者情報
    $pdf->Cell(50, 4, sprintf('患者ID:%s', $data['ptif_id']), 0, 0, 'L');
    $pdf->Cell(80, 4, sprintf('患者氏名:%s', $data['kname']), 0, 1, 'L');
    
    // プロブレム
    $pdf->Cell(0, 4, sprintf('プロブレム:%s', $data['problem']), 0, 1);
    
    // 立案年月日
    $pdf->Cell(50, 4, sprintf('立案年月日:%s', date("Y年m月d日", strtotime($data['create_date']))), 0, 0, 'L');
    
    // 記載者
    $pdf->Cell(80, 4, sprintf('責任者  職種:%s 氏名:%s', $data['emp_job'], $data['emp_name']), 0, 1, 'L');
}

/**
 * 患者の問題及びアウトカムのPDFを作成
 * 
 * @param object $pdf    PDF作成オブジェクト
 * @param array  $data   チーム計画データ
 * @param float  $width  幅
 * @param float  $height 1行の高さ
 * 
 * @return void
 */
function createProblemAndOutcomePDF($pdf, $data, $width, $height)
{
    $pdf->MBMultiCell($width, $height, sprintf("■定義%s\n%s", $data['problem'], replaceTagToBreak($data['def_value'])));
    $pdf->Ln();
    $pdf->MBMultiCell($width, $height, sprintf("■S:症状・兆候\n%s", replaceTagToBreak($data['s_detail'])));
    $pdf->Ln();
    $pdf->MBMultiCell($width, $height, sprintf("■E:原因・関連因子\n%s", replaceTagToBreak($data['e_detail'])));
    $pdf->Ln();
    $pdf->MBMultiCell($width, $height, sprintf("■危険因子\n%s", replaceTagToBreak($data['risk_detail'])));
    $pdf->Ln();
    $pdf->MBMultiCell($width, $height, sprintf("■アウトカム\n%s", replaceTagToBreak($data['outcome'])));
    $pdf->Ln();
    
    // 観察項目
    $observation_items = '';
    foreach ($data['observation_items'] as $value) {
        if (!empty($observation_items)) {
            $observation_items .= "\n";
        }
        $observation_items .= $value['name'];
    }
    $pdf->Cell($width, $height, '■観察項目', 0, 1);
    $pdf->MBMultiCell($width, $height, $observation_items);
    $pdf->Ln();
    
    // 患者の問題及びアウトカムの更新記録
    $left_update = sprintf('記載者:%s', $data['left_creater_name']);
    foreach ($data['left_update'] as $value) {
        if (!empty($left_update)) {
            $left_update .= "\n";
        }
        $left_update .= strftime("更新日:%Y/%m/%d %H:%M\n", strtotime($value['update']));
        $left_update .= sprintf('更新者:%s', $value['name']);
    }
    $pdf->MBMultiCell($width, $height, $left_update);
}

/**
 * チーム実践のセルを作成
 * 
 * @param object  $pdf     PDF作成オブジェクト
 * @param array   $data    チーム計画データ
 * @param string  $key     チーム区分のキーワード
 * @param integer $type    チーム区分
 * @param string  $caption 見出し
 * @param float   $width   幅
 * @param float   $height  1行の高さ
 * @param float   $xpos    開始X座標
 * 
 * @return void
 */
function createTeamPracticePDF($pdf, $data, $key, $type, $caption, $width, $height, $xpos)
{
    // 拡張機能有効時のみ見出し「共用」「看護」「介護」「リハビリ」を表示
//    $pdf->SetX($xpos);
//    $pdf->Cell($width, $height, "＜{$caption}＞");

    $pdf->SetX($xpos);
    $pdf->MBMultiCell($width, $height, sprintf("■O-P:観察計画\n%s", replaceTagToBreak($data["{$key}_op"])));
    $pdf->Ln();
    $pdf->SetX($xpos);
    $pdf->MBMultiCell($width, $height, sprintf("■T-P:実践計画\n%s", replaceTagToBreak($data["{$key}_tp"])));
    $pdf->Ln();
    $pdf->SetX($xpos);
    $pdf->MBMultiCell($width, $height, sprintf("■E-P:教育・指導計画\n%s", replaceTagToBreak($data["{$key}_ep"])));
    $pdf->Ln();
    
    // 更新記録
    $update = sprintf('記載者:%s', $data["{$key}_creater_name"]);
    if (array_key_exists($type, $data['center_update'])) {
        foreach ($data['center_update'][$type] as $value) {
            if (!empty($update)) {
                $update .= "\n";
            }
            $update .= strftime("更新日:%Y/%m/%d %H:%M\n", strtotime($value['update']));
            $update .= sprintf('更新者:%s', $value['name']);
        }
    }
    $pdf->SetX($xpos);
    $pdf->MBMultiCell($width, $height, $update);
}

/**
 * 評価日／解決・継続／サインPDFを作成
 * 
 * @param object $pdf           PDF作成オブジェクト
 * @param array  $each_ass_list 評価データ
 * @param string $key           チーム区分のキーワード
 * @param string $caption       見出し
 * @param float  $width         幅
 * @param float  $height        1行の高さ
 * @param float  $xpos          開始X座標
 * 
 * @return void
 */
function createAssessmentPDF($pdf, $each_ass_list, $key, $caption, $width, $height, $xpos)
{
    $pdf->SetX($xpos);
// 拡張機能有効時のみ見出し「共用」「看護」「介護」「リハビリ」を表示
//    $assessment = "＜{$caption}＞";
    foreach ($each_ass_list as $value) {
        if (!empty($assessment)) {
            $assessment .= "\n";
        }
        $assessment .= sprintf("評価日:%s\n", date( "Y年m月d日", strtotime($value['update'])));
        $assessment .= sprintf("評価者:%s\n", $value['name']);
        $assessment .= sprintf("評価:%s", $value['comment']);
    }

    $pdf->MBMultiCell($width, $height, $assessment);
}

/**
 * チーム実践データの存在判定
 * 
 * @param array  $data チーム計画データ
 * @param string $key  チーム区分のキーワード
 * 
 * @return boolean OP、TP、EPのうち、少なくとも1つはデータが存在する:true/OP、TP、EPの全データが存在しない:false
 */
function isTeamPracticeDataExist($data, $key)
{
    if (empty($data["{$key}_op"]) && empty($data["{$key}_tp"])  && empty($data["{$key}_ep"])) {
        return false;
    } else {
        return true;
    }
}

/**
 * 文字列内の<BR>タグを改行コードに変換する
 * 
 * @param string $string 変換対象文字列
 * 
 * @return string 変換した文字列
 */
function replaceTagToBreak($string)
{
    return preg_replace('/<BR>/i', "\n", $string);
}
?>
