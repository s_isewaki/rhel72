<?
/*
画面パラメータ
	$session
		セッションID
	$kind_flg
		種別フラグ 1:個人 2:共用
*/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css"/>
<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require("label_by_profile_type.ini");
require("get_values.ini");
require_once("summary_common.ini");
require_once("get_menu_label.ini");


//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session, ($kind_flg=="1"?57:59) ,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 組織タイプを取得
//==============================
$profile_type = get_profile_type($con, $fname);

$kind_str = "定型文更新".($kind_flg=="1" ? "(個人)" : "(共用)");

$med_report_title = get_report_menu_label($con, $fname);


//SQL実行
$sql = "select fixedform_sentence, sect_id from fixedform where fixedform_id = '".pg_escape_string($fixedform_id)."'";
$sel = select_from_table($con,$sql,"",$fname);
$sentence = pg_fetch_result($sel,0,"fixedform_sentence");
$sel_sect_id = pg_fetch_result($sel,0,"sect_id");


//==========================================================================================
//HTML出力
//==========================================================================================
?>
<title><?=$med_report_title?>｜<?=$kind_str?></title>
</head>
<body onload="document.main_form.fixedform_sentence.focus();">
<center>
<div style="width:580px; padding-top:4px">

<!-- ヘッダ -->
<?= summary_common_show_dialog_header($kind_str); ?>


<?
//==============================
//入力フィールド 出力
//==============================
?>
<form name="main_form" action="summary_fixedform_update_exe.php" method="post">
	<input type="hidden" name="session" value="<?=$session?>">
	<input type="hidden" name="kind_flg" value="<?=$kind_flg?>">
	<input type="hidden" name="fixedform_id" value="<?=$fixedform_id?>">



	<? if ($kind_flg!="1"){ ?>
	<div class="dialog_searchfield">
		<table style="width:100%">
			<tr>
				<td style="width:10%">診療科</td>
				<td>
					<select name="sel_sect_id">
						<option value=""></option>
						<?
							$sql =
									" select sect_id, sect_nm from (".
									"   select * from (".
									"     select *, case when sect_sort_order = '' then null else sect_sort_order end as sort_order from sectmst".
									"   ) d".
									" ) d".
									" where sect_del_flg = 'f' and (sect_hidden_flg is null or sect_hidden_flg <> 1) order by sort_order, sect_id";
							$sel_sect = select_from_table($con, $sql, $cond, $fname);
							while($row = pg_fetch_array($sel_sect)){
								?><option value="<?=$row["sect_id"]?>"<?=($row["sect_id"] == @$sel_sect_id) ? " selected" : ""?>><?=$row["sect_nm"]?></option><?
							}
						?>
					</select>
				</td>
			</tr>
		</table>
	</div>
	<? } ?>



	<div style="margin-top:4px">
		<textarea name="fixedform_sentence" cols="80" rows="5" style="width:574px; ime-mode: active;"><?=$sentence?></textarea>
	</div>
	<div class="search_button_div">
		<input type="button" onclick="document.main_form.submit();" value="更新"/>
	</div>
</form>

</div>
</center>
</body>
</html>
<? pg_close($con); ?>
