<?php
ob_start();
require_once("about_comedix.php");
require_once("about_session.php");
require_once("settings_common.php");
require_once("about_authority.php");
require_once("show_timecard_common.ini");
require_once("date_utils.php");
require_once("get_values.ini");
require_once("timecard_tantou_class.php");
require_once("atdbk_close_class.php");

/**
 * 時間外統計資料　残業理由別CSVの生成と出力.
 *
 */

$fname = $PHP_SELF;

/** 対象月 */
define('MONTH_COUNT', 2);

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0" && $mmode != "usr") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
if ($mmode == "usr") {
	$emp_id = get_emp_id($con, $session, $fname);
	$timecard_tantou_class = new timecard_tantou_class($con, $fname);
	$timecard_tantou_branches = $timecard_tantou_class->get_tantou_branches($emp_id);
}

$duty_type_flag = get_settings_value("employment_working_other_flg", "f"); // 雇用区分表示

// 常勤/非常勤ごとの締め日情報を取得
$sql = "select closing, closing_parttime, closing_month_flg from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0){
	$tmp_full = pg_fetch_result($sel, 0, "closing");
	$tmp_part = pg_fetch_result($sel, 0, "closing_parttime");
	$tmp_flag = pg_fetch_result($sel, 0, "closing_month_flg");
	//常勤
	$closing_full = (empty($tmp_full)) ? "6" : $tmp_full;
	//非常勤
	$closing_part = (empty($tmp_part)) ? $closing_full : $tmp_part;
	
	$closing_month_flg = (empty($tmp_flag)) ? "1" : $tmp_flag;
}else{
	$closing_full = "6";
	$closing_part = $closing_jyoukin; 
	$closing_month_flg = "1";
}

$overtime = ($_POST["base_three"]) ? intval($_POST["base_three"]) : 0; // 基準残業時間

$period = array();
$tmp_yyyymm = $_POST["start_yr_3month"] . $_POST["start_mon_3month"];
$yyyymm = (empty($tmp_yyyymm)) ? date("Ym") : $tmp_yyyymm;
$tmp_start = strtotime($yyyymm . "01");
$atdbk_close_class = new atdbk_close_class($con, $fname);
$arr_csv_closing_info = $atdbk_close_class->get_csv_closing_info();
$arr_closing = array();
$i=0;
$arr_closing[$i] = $closing_full;
$i++;
if ($closing_part != $closing_full) {
	$arr_closing[$i] = $closing_part;
	$i++;
}
foreach ($arr_csv_closing_info as $csv_id => $wk_closing) {
	if ($wk_closing != $closing_full) {
		$arr_closing[$i] = $wk_closing;
		$i++;
	}
}
for ($i = MONTH_COUNT; $i >= 0; $i--){
	$buf_date = date("Ym", strtotime("-$i month", $tmp_start));
	$full_time = getClosingDateFromTo($buf_date, $closing_full, $closing_month_flg);
	$part_time = getClosingDateFromTo($buf_date, $closing_part, $closing_month_flg);
	$period["full"][] = $full_time;
	$period["part"][] = $part_time;

	// 検索開始日
	if ($i == MONTH_COUNT){
		$arr_term = $atdbk_close_class->get_term_multi_closing($buf_date, $arr_closing, $closing_month_flg);
		$search_start = $arr_term["start"];
		$buf_date1 = $buf_date;
		//$search_start = min(intval($full_time["start_date"]), intval($part_time["start_date"]));
	}
	
	if ($i == 1){
		$buf_date2 = $buf_date;
	}
	// 検索終了日
	if ($i == 0){
		$arr_term = $atdbk_close_class->get_term_multi_closing($buf_date, $arr_closing, $closing_month_flg);
		$search_end = $arr_term["end"];
		$buf_date3 = $buf_date;
		//$search_end = max(intval($full_time["end_date"]), intval($part_time["end_date"]));
	}
}
//常勤
$full_st0 = strtotime($period["full"][0]["start_date"]);
$full_ed0 = strtotime($period["full"][0]["end_date"]);
$full_st1 = strtotime($period["full"][1]["start_date"]);
$full_ed1 = strtotime($period["full"][1]["end_date"]);
$full_st2 = strtotime($period["full"][2]["start_date"]);
$full_ed2 = strtotime($period["full"][2]["end_date"]);

//非常勤
$part_st0 = strtotime($period["part"][0]["start_date"]);
$part_ed0 = strtotime($period["part"][0]["end_date"]);
$part_st1 = strtotime($period["part"][1]["start_date"]);
$part_ed1 = strtotime($period["part"][1]["end_date"]);
$part_st2 = strtotime($period["part"][2]["start_date"]);
$part_ed2 = strtotime($period["part"][2]["end_date"]);


$arr_duty = array("1"=>"常勤", "2"=>"非常勤", "3"=>"短時間正職員");
if ($duty_type_flag == "t"){
	$arr_duty_type = array("1"=>"正職員", "2"=>"再雇用職員", "3"=>"嘱託職員", "4"=>"臨時職員", "5"=>"パート職員");
}

$sql  = "SELECT o.emp_id, o.target_date,";
$sql .= "f.emp_personal_id, f.emp_lt_nm, f.emp_ft_nm, deptmst.link_key1, deptmst.dept_nm, ec.duty_form, ec.duty_form_type,";
$sql .= "r.over_start_time as over_start_time1,r.over_end_time as over_end_time1,r.over_start_next_day_flag as over_start_next_day_flag1,r.over_end_next_day_flag as over_end_next_day_flag1,";
$sql .= "r.rest_start_time1,r.rest_end_time1,r.rest_start_next_day_flag1,r.rest_end_next_day_flag1,";
$sql .= "r.over_start_time2,r.over_end_time2,r.over_start_next_day_flag2,r.over_end_next_day_flag2,r.rest_start_time2,r.rest_end_time2,r.rest_start_next_day_flag2,r.rest_end_next_day_flag2,";
$sql .= "r.over_start_time3,r.over_end_time3,r.over_start_next_day_flag3,r.over_end_next_day_flag3,r.rest_start_time3,r.rest_end_time3,r.rest_start_next_day_flag3,r.rest_end_next_day_flag3,";
$sql .= "r.over_start_time4,r.over_end_time4,r.over_start_next_day_flag4,r.over_end_next_day_flag4,r.rest_start_time4,r.rest_end_time4,r.rest_start_next_day_flag4,r.rest_end_next_day_flag4,";
$sql .= "r.over_start_time5,r.over_end_time5,r.over_start_next_day_flag5,r.over_end_next_day_flag5,r.rest_start_time5,r.rest_end_time5,r.rest_start_next_day_flag5,r.rest_end_next_day_flag5 ";
$sql .= "FROM ovtmapply o ";
$sql .= "INNER JOIN  empmst f on o.emp_id = f.emp_id ";
$sql .= "LEFT JOIN deptmst ON f.emp_dept = deptmst.dept_id AND deptmst.dept_del_flg = 'f' ";
$sql .= "LEFT JOIN empcond ec ON f.emp_id = ec.emp_id ";
$sql .= "LEFT JOIN atdbkrslt r ON o.emp_id = r.emp_id AND o.target_date = CAST(r.date AS varchar) ";
$cond  = "WHERE o.target_date >= '$search_start' AND o.target_date <= '$search_end' AND o.apply_status = '1' AND o.delete_flg = 'f' ";
$cond .= "AND o.over_start_time <> '' AND o.over_end_time <> '' ";
//担当所属
if ($mmode == "usr") {
    $cond .= " and (1 <> 1";
    foreach ($timecard_tantou_branches as $r) {
        if ($r["class_id"]) $cond .= " or (";
        if ($r["class_id"]) $cond .= "     f.emp_class=".$r["class_id"];
        if ($r["atrb_id"])  $cond .= "     and f.emp_attribute=".$r["atrb_id"];
        if ($r["dept_id"])  $cond .= "     and f.emp_dept=".$r["dept_id"];
        if ($r["class_id"]) $cond .= " )";
    }
    //兼務
    $cond_ccr = " or exists (select * from concurrent where concurrent.emp_id = f.emp_id and (1 <> 1";
    foreach ($timecard_tantou_branches as $r) {
        if ($r["class_id"]) $cond_ccr .= " or (";
        if ($r["class_id"]) $cond_ccr .= "     concurrent.emp_class=".$r["class_id"];
        if ($r["atrb_id"])  $cond_ccr .= "     and concurrent.emp_attribute=".$r["atrb_id"];
        if ($r["dept_id"])  $cond_ccr .= "     and concurrent.emp_dept=".$r["dept_id"];
        if ($r["class_id"]) $cond_ccr .= " )";
    }
    $cond_ccr .= "))";
    $cond .= $cond_ccr;
    $cond .= ") ";
}
$cond .= "ORDER BY o.target_date, o.emp_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$array_info = array();
$array_item = array();
while($row = pg_fetch_array($sel)){

	$emp_id = $row["emp_id"];
	$e_date = $row["target_date"];
	$e_duty = $row["duty_form"];
	$e_duty_type = $row["duty_form_type"];
	$target_date = strtotime($e_date);
	
	$wk_closing = $atdbk_close_class->get_empcond_closing($emp_id);
	$arr_term = $atdbk_close_class->get_term($buf_date1, $wk_closing, $closing_month_flg);
	$full_st0 = date_utils::to_timestamp_from_ymd($arr_term["start"]);
	$full_ed0 = date_utils::to_timestamp_from_ymd($arr_term["end"]);
	$arr_term = $atdbk_close_class->get_term($buf_date2, $wk_closing, $closing_month_flg);
	$full_st1 = date_utils::to_timestamp_from_ymd($arr_term["start"]);
	$full_ed1 = date_utils::to_timestamp_from_ymd($arr_term["end"]);
	$arr_term = $atdbk_close_class->get_term($buf_date3, $wk_closing, $closing_month_flg);
	$full_st2 = date_utils::to_timestamp_from_ymd($arr_term["start"]);
	$full_ed2 = date_utils::to_timestamp_from_ymd($arr_term["end"]);

	$part_st0 = $full_st0;
	$part_ed0 = $full_ed0;
	$part_st1 = $full_st1;
	$part_ed1 = $full_ed1;
	$part_st2 = $full_st2;
	$part_ed2 = $full_ed2;

	if (empty($array_info[$emp_id])){
		$array_info[$emp_id][] = $row["emp_personal_id"]; // 職員ID
		$array_info[$emp_id][] = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"] ; // 職員名
		$array_info[$emp_id][] = $row["link_key1"]; // 所属コード
		$array_info[$emp_id][] = $row["dept_nm"];  // 科

		$array_info[$emp_id][] = $arr_duty[$e_duty];  // 雇用勤務形態
		if ($duty_type_flag == "t"){
			$array_info[$emp_id][] = $arr_duty_type[$e_duty_type]; // 雇用区分
		}
		
		$array_item[$emp_id] = array("month0"=>0, "month1"=>0, "month2"=>0);
	}
	
	for ($i = 1; $i < 6; $i++){
		$next_date = date("Ymd", strtotime($e_date. " +1 days"));
		
		// 残業開始日時
		$tmp_start = ($row["over_start_next_day_flag" . $i] == "1") ? $next_date : $e_date;
		$over_start = $tmp_start . $row["over_start_time" . $i];
			
		// 残業終了日時
		$tmp_end = ($row["over_end_next_day_flag" . $i] == "1") ? $next_date : $e_date;
		$over_end = $tmp_end . $row["over_end_time" . $i];
			
		// 残業時間計算
		$tmp_over = date_utils::get_diff_minute($over_end, $over_start);
		$over_count = ($tmp_over > 0) ? $tmp_over : 0;
			
		// 休憩開始日時
		$tmp_start_rest = ($row["rest_start_next_day_flag" . $i] == "1") ? $next_date : $e_date;
		$rest_start = $tmp_start_rest . $row["rest_start_time" . $i];
			
		// 休憩終了日時
		$tmp_end_rest = ($row["rest_end_next_day_flag" . $i] == "1") ? $next_date : $e_date;
		$rest_end = $tmp_end_rest . $row["rest_end_time" . $i];
			
		// 休憩時間計算
		$tmp_rest = date_utils::get_diff_minute($rest_end, $rest_start);
		$rest_count = ($tmp_rest > 0) ? $tmp_rest : 0; 
			
		// 順残業時間
		$result_count = $over_count - $rest_count;
		
		if ($row["duty_form"] == "2"){
			// 非常勤
			switch (true){
				case ($part_st0 <= $target_date and $target_date <= $part_ed0) : // 前々月
					$array_item[$emp_id]["month0"] = $array_item[$emp_id]["month0"] + $result_count;
					break;
					
				case ($part_st1 <= $target_date and $target_date <= $part_ed1) ; // 前月
					$array_item[$emp_id]["month1"] = $array_item[$emp_id]["month1"] + $result_count;
					break;
					
				case ($part_st2 <= $target_date and $target_date <= $part_ed2) ; // 今月
					$array_item[$emp_id]["month2"] = $array_item[$emp_id]["month2"] + $result_count;
			}
		}else{
			// 常勤
			switch (true){
				case ($full_st0 <= $target_date and $target_date <= $full_ed0) : // 前々月
					$array_item[$emp_id]["month0"] = $array_item[$emp_id]["month0"] + $result_count;
					break;
					
				case ($full_st1 <= $target_date and $target_date <= $full_ed1) ; // 前月
					$array_item[$emp_id]["month1"] = $array_item[$emp_id]["month1"] + $result_count;
					break;
					
				case ($full_st2 <= $target_date and $target_date <= $full_ed2) ; // 今月
					$array_item[$emp_id]["month2"] = $array_item[$emp_id]["month2"] + $result_count;
			}
		}
	}
}

// 基準時間超の項目を分ける
foreach ($array_item as $key=>$item){
	$count_time = $item["month0"] + $item["month1"] + $item["month2"];
	
	$average = $count_time / 3;
	if ($average >= ($overtime * 60)){
		$array_info[$key][] = (empty($item["month0"])) ? "0:00" : minute_to_hmm($item["month0"]);
		$array_info[$key][] = (empty($item["month1"])) ? "0:00" : minute_to_hmm($item["month1"]);
		$array_info[$key][] = (empty($item["month2"])) ? "0:00" : minute_to_hmm($item["month2"]);
		
		$array_info[$key][] = $average;
	}else{
		unset($array_info[$key]);
	}
}

// 残業時間数ソート順
$key_total = array();
foreach ($array_info as $key=>$item){
	$key_total[$key] = $item[9];
}

array_multisort($key_total, SORT_DESC, $array_info);

// 情報をCSV形式で出力
$csv = get_list_csv($array_info, $yyyymm, $overtime, $duty_type_flag);

// データベース接続を閉じる
pg_close($con);

// CSVを出力
$file_name = "OvertimeThreeMonths" . "_" . $yyyymm . "_" . $overtime . "Hour.csv";
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

/**
 * CSVデータの生成.
 * @param array $array_info 職員別月別残業時間集計結果(連想配列) 時間外合計で降順に並び替え済
 * @param string 集計対象期間 基準月
 * @param int $overtime 基準値
 * @return string $duty_type_flag　雇用形態区分フラグ
 * @return string CSV形式に整形された閾値以上の残業者リスト
 */
function get_list_csv($array_info, $yyyymm, $overtime, $duty_type_flag) {

	$titles = array(
			"職員ID",
			"氏名",
			"所属コード",
			"所属名",
			"雇用勤務形態",
			"雇用区分",
			"前々月時間",
			"前月時間外",
			"当月時間外",
			"３ヶ月平均"
	);
	if ($duty_type_flag != "t"){
		array_splice($titles, 5, 1);
	}

	$item_num = count($titles);
	$last_num = $item_num -1;
	$num =count($array_info);

	$buf = "";
	$buf .= $yyyymm . "," . $overtime;
	$buf .= "\r\n";

	// タイトル
	for ($j=0;$j<$item_num;$j++) {
		if ($j != 0) {
			$buf .= ",";
		}
		$buf .= $titles[$j];
	}
	$buf .= "\r\n";

	// 内容
	foreach ($array_info as $key=>$info){
		for ($j=0;$j<$item_num;$j++) {
			if ($j != 0) {
				$buf .= ",";
			}
			
			$buf .= ($last_num == $j) ?  minute_to_hmm($array_info[$key][$j]) : $array_info[$key][$j];
		}
		$buf .= "\r\n";
	}
	return mb_convert_encoding($buf, "SJIS", "EUC-JP");
}



/**
 * 締め日を考慮した月度の開始日、終了日の算出.
 * @param String $yyyymm
 * @param String $closing
 * @param String $closing_month_flg
 * @return array:string 月度に応じた月の開始日, 終了日
 */
function getClosingDateFromTo($yyyymm, $closing, $closing_month_flg){

	$arr_date = array();
	
	// 締め日が登録済みの場合
	if ($closing != "") {

		if ($yyyymm != "") {
			$year = substr($yyyymm, 0, 4);
			$month = intval(substr($yyyymm, 4, 2));
			$month_set_flg = true; //月が指定された場合
		} else {
			$year = date("Y");
			$month = date("n");
			$yyyymm = $year . date("m");
			$month_set_flg = false; //月が未指定の場合
		}

		// 開始日を算出
		$calced = false;
		switch ($closing) {
			case "1":  // 1日
				$closing_day = 1;
				break;
			case "2":  // 5日
				$closing_day = 5;
				break;
			case "3":  // 10日
				$closing_day = 10;
				break;
			case "4":  // 15日
				$closing_day = 15;
				break;
			case "5":  // 20日
				$closing_day = 20;
				break;
			case "6":  // 末日
				$start_year = $year;
				$start_month = $month;
				$start_day = 1;
				$end_year = $start_year;
				$end_month = $start_month;
				$end_day = days_in_month($end_year, $end_month);
				$calced = true;
				break;
		}
		if (!$calced) {
			$day = date("j");
			$start_day = $closing_day + 1;
			$end_day = $closing_day;

			//月が指定された場合
			if ($month_set_flg) {
				if ($closing_month_flg != "2") {
					$start_year = $year;
					$start_month = $month;

					$end_year = $year;
					$end_month = $month + 1;
					if ($end_month == 13) {
						$end_year++;
						$end_month = 1;
					}
				}
				else {
					$start_year = $year;
					$start_month = $month - 1;
					if ($start_month == 0) {
						$start_year--;
						$start_month = 12;
					}

					$end_year = $year;
					$end_month = $month;
				}

			} else {
				if ($day <= $closing_day) {
					$start_year = $year;
					$start_month = $month - 1;
					if ($start_month == 0) {
						$start_year--;
						$start_month = 12;
					}

					$end_year = $year;
					$end_month = $month;
				} else {
					$start_year = $year;
					$start_month = $month;

					$end_year = $year;
					$end_month = $month + 1;
					if ($end_month == 13) {
						$end_year++;
						$end_month = 1;
					}
				}
			}
		}
		$arr_date["start_date"] = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
		$arr_date["end_date"] = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);
	}
	return $arr_date;
}
?>
