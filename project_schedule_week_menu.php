<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>委員会・WG | スケジュール（週）</title>
<?
$fname = $PHP_SELF;

require("about_session.php");
require("about_authority.php");
require("time_check3.ini");
require("show_date_navigation_week.ini");
require("holiday.php");
require("show_project_schedule_common.ini");
require("show_project_schedule_chart.ini");
require("label_by_profile_type.ini");
require("show_calendar_memo.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 日付の設定
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}
$day = date("d", $date);
$month = date("m", $date);
$year = date("Y", $date);

// 「前月」「前日」「翌日」「翌月」のタイムスタンプを取得
$last_month = get_last_month($date);
$last_week = get_last_week($date);
$next_week = get_next_week($date);
$next_month = get_next_month($date);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 院内行事チェックボックスの処理
if ($timegd == "on" || $timegd == "off") {
	time_update($con, $emp_id, $timegd, $fname);
} else if ($timegd == "") {
	$timegd = time_check($con, $emp_id, $fname);
}

// 表示対象週のスタート日のタイムスタンプを取得
$start_date = get_start_date_timestamp($con, $date, $emp_id, $fname);

// 表示対象週の7日分の日付を配列で取得
$dates = get_dates_in_week($start_date);

// 時間指定のないスケジュールの情報を配列で取得
$timeless_schedules = get_timeless_schedules($con, $pjt_id, $timegd, $dates, $fname);

// 時間指定のあるスケジュール情報を配列で取得
$normal_schedules = get_normal_schedules($con, $emp_id, $pjt_id, $timegd, $dates, $fname);

// 委員会・WG名を取得
$sql = "select pjt_name from project";
$cond = "where pjt_id = $pjt_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$project_name = pg_result($sel, 0, "pjt_name");
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 委員会・WK管理権限を取得
$pjt_admin_auth = check_authority($session, 82, $fname);

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">
function showGuide() {
	var timegd = (document.timegd.timeguide.checked) ? 'on' : 'off';
	location.href = 'project_schedule_week_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>&date=<? echo($date); ?>&timegd=' + timegd;
}

function openPrint() {
	window.open('project_schedule_print_week_menu.php?&session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>', 'newwin', 'width=800,height=600,scrollbars=yes');
}

function changeDate(dt) {
	location.href = 'project_schedule_week_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>&date=' + dt;
}

function popupScheduleDetail(title, place, date, time, type, detail, regname, e) {
	popupDetailBlue(
		new Array(
			'タイトル', title,
			'行先', place,
			'日付', date,
			'時刻', time,
			'種別', type,
			'詳細', detail,
			'登録者', regname
		), 400, 80, e
	);
}

function popupEventDetail(fcl_name, evt_date, evt_time, evt_name, evt_content, evt_contact, e) {
	popupDetailBlue(
		new Array(
			'施設', fcl_name,
			'日付', evt_date,
			'時刻', evt_time,
			'行事名', evt_name,
			'詳細', evt_content,
			'連絡先', evt_contact
		), 400, 80, e
	);
}

function highlightCells(class_name) {
	changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" colspan="2" class="spacing"><a href="project_menu.php?session=<? echo($session); ?>"><img src="img/icon/b03.gif" width="32" height="32" border="0" alt="委員会・WG"></a></td>

<? if ($pjt_admin_auth == "1") { ?>

	<? if($adm_flg=="t") {?>
		<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="project_menu.php?session=<? echo($session); ?>&entity=1"><b>ユーザ画面へ</b></a></font></td>
	<? }else{ ?>
		<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">　<a href="project_menu_adm.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
	<? } ?>

<? }else{ ?>
	<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>

<? } ?>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>

<? if($adm_flg=="t") {?>
	<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu_adm.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<? }else{ ?>
	<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<? } ?>

<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="project_schedule_week_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>スケジュール</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5" colspan="10"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo($project_name); ?></b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="project_schedule_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#5279a5"><a href="project_schedule_week_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>週</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="project_schedule_month_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="project_schedule_year_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($year); ?>&s_date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="project_schedule_list_upcoming.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="project_schedule_option.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td align="right"><input type="button" value="印刷" onclick="openPrint();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="timegd">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="project_schedule_week_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($last_month); ?>&adm_flg=<?=$adm_flg?>">&lt;&lt;前月</a>&nbsp;<a href="project_schedule_week_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($last_week); ?>&adm_flg=<?=$adm_flg?>">&lt;前週</a>&nbsp;<select onchange="changeDate(this.options[this.selectedIndex].value);"><? show_date_options_w($date, $start_date); ?></select>&nbsp;<a href="project_schedule_week_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($next_week); ?>&adm_flg=<?=$adm_flg?>">翌週&gt;</a>&nbsp;<a href="project_schedule_week_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($next_month); ?>&adm_flg=<?=$adm_flg?>">翌月&gt;&gt;</a></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? 
// 院内行事/所内行事
echo ($_label_by_profile["EVENT"][$profile_type]); ?></font><input type="checkbox" name="timeguide" onclick="showGuide();"<? if ($timegd == "on") {echo(" checked");} ?>></td>
</tr>
</table>
<?
$start_ymd = date("Ymd", $start_date);
$end_ymd = date("Ymd", strtotime("+6 days", $start_date));
// カレンダーのメモを取得
$arr_calendar_memo = get_calendar_memo($con, $fname, $start_ymd, $end_ymd);
$holiday_memo_flg = false;
foreach($arr_calendar_memo as $tmp_date => $tmp_memo) {
	if ($tmp_memo != "" && strlen($tmp_date) == 8) {
		$y = substr($tmp_date, 0, 4);
		$m = substr($tmp_date, 4, 2);
		$d = substr($tmp_date, 6, 2);
		$holiday_name = ktHolidayName(mktime(0, 0, 0, $m, $d, $y));
		if ($holiday_name != "") {
			$holiday_memo_flg = true;
			break;
		}
	}
}
if ($holiday_memo_flg == true) {
	$tmp_height = "58";
} else {
	$tmp_height = "42";
}

echo("<div style=\"height:{$tmp_height}px; overflow: auto;border:#CCCCCC solid 1px;\">\n");
echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">\n");

// 日付行を表示
show_dates($con, $dates, $pjt_id, $emp_id, $normal_schedules, $session, $fname, $arr_calendar_memo, $tmp_height,$adm_flg);

// 時間指定なし行を表示
show_timeless_schedules($timeless_schedules, $normal_schedules, $session, false);

// 時間指定あり行を表示
show_normal_schedules($con, $timeless_schedules, $normal_schedules, $pjt_id, $emp_id, $session, $fname, false, false, $adm_flg);

echo("</table>\n");
echo("</div>\n");

echo("<div style=\"height:500px; overflow: scroll;border:#CCCCCC solid 1px;\">\n");
echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">\n");

// 時間指定なし行を表示
show_timeless_schedules($timeless_schedules, $normal_schedules, $session, false);

// 時間指定あり行を表示
show_normal_schedules($con, $timeless_schedules, $normal_schedules, $pjt_id, $emp_id, $session, $fname, false, false, $adm_flg);

echo("</table>\n");
echo("</div>\n");
?>
</form>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<? /* 凡例非表示
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width=""><? show_proschd_type_color_list($con, $fname); ?></td>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
*/ ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
// 表示対象週のスタート日のタイムスタンプを取得
function get_start_date_timestamp($con, $date, $emp_id, $fname) {
	$sql = "select calendar_start2 from option";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$calendar_start = pg_fetch_result($sel, 0, "calendar_start2");

	$start_wd = ($calendar_start == 1) ? 0 : 1;  // 0：日曜、1：月曜

	return get_start_day($date, $start_wd);
}

// 表示対象週の7日分の日付を配列で取得
function get_dates_in_week($start_date) {
	$ret = array();
	for ($i = 0; $i < 7; $i++) {
		$ret[] = date("Ymd", strtotime("+$i days", $start_date));
	}
	return $ret;
}

// 日付行を表示
function show_dates($con, $dates, $pjt_id, $emp_id, $normal_schedules, $session, $fname, $arr_calendar_memo, $height,$adm_flg) {
	$today = date("Ymd");

	echo("<tr height=\"$height\">\n");
	echo("<td>&nbsp;</td>\n");
	foreach ($dates as $date) {
		$colspan = get_overlap_schedule_count($normal_schedules[$date]);
		$timestamp = to_timestamp($date);
		$month = date("m", $timestamp);
		$day = date("d", $timestamp);
		$weekday_info = get_weekday_info($timestamp);
		$holiday = ktHolidayName($timestamp);
		if ($date == $today) {
			$bgcolor = "#ccffcc";
		} elseif ($holiday != "") {
			$bgcolor = "#fadede";
		} else if ($arr_calendar_memo["{$date}_type"] == "5") {
			$bgcolor = "#defafa";
		} else {
			$bgcolor = $weekday_info["color"];
		}

		echo("<td width=\"14%\" align=\"center\" valign=\"top\" colspan=\"$colspan\" bgcolor=\"$bgcolor\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><b><a href=\"project_schedule_menu.php?session=$session&pjt_id=$pjt_id&date=$timestamp&adm_flg=$adm_flg\">{$month}/{$day}（{$weekday_info["label"]}）</a></b><br>");
		// カレンダーのメモがある場合は設定する
		if ($arr_calendar_memo["$date"] != "") {
			if ($holiday == "") {
				$holiday = $arr_calendar_memo["$date"];
			} else {
				$holiday .= "<br>".$arr_calendar_memo["$date"];
			}
		}
		if ($holiday != "") {
			echo("<font color=\"red\">$holiday</font>&nbsp;");
		}
		echo("</font></td>\n");
	}
	echo("</tr>\n");
}

// タイムスタンプから曜日情報を取得
function get_weekday_info($date) {
	$wd = date("w", $date);
	switch ($wd) {
	case "0":
		return array("label" => "日", "color" => "#fadede");
		break;
	case "1":
		return array("label" => "月", "color" => "#fefcdf");
		break;
	case "2":
		return array("label" => "火", "color" => "#fefcdf");
		break;
	case "3":
		return array("label" => "水", "color" => "#fefcdf");
		break;
	case "4":
		return array("label" => "木", "color" => "#fefcdf");
		break;
	case "5":
		return array("label" => "金", "color" => "#fefcdf");
		break;
	case "6":
		return array("label" => "土", "color" => "#defafa");
		break;
	}
}
?>
