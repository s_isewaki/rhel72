<?
require_once("cl_application_workflow_common_class.php");


function show_application_approve_detail($con, $session, $fname, $apply_id, $mode, $comment, $approve, $drag_flg, $changes_flg, $history_flg, $apv_order_fix, $apv_sub_order_fix, $send_apved_order_fix)
{

	$obj = new cl_application_workflow_common_class($con, $fname);

	// 申請・ワークフロー情報取得
	$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
	$apply_stat       = $arr_apply_wkfwmst[0]["apply_stat"];
	$apply_date       = $arr_apply_wkfwmst[0]["apply_date"];
	$apply_content    = $arr_apply_wkfwmst[0]["apply_content"];
	$apply_title      = $arr_apply_wkfwmst[0]["apply_title"];
	$re_apply_id      = $arr_apply_wkfwmst[0]["re_apply_id"];
	$wkfw_id          = $arr_apply_wkfwmst[0]["wkfw_id"];
	$wkfw_appr        = $arr_apply_wkfwmst[0]["wkfw_appr"];
	$apply_stat       = $arr_apply_wkfwmst[0]["apply_stat"];
	$apply_no         = $arr_apply_wkfwmst[0]["apply_no"];
    $short_wkfw_name  = $arr_apply_wkfwmst[0]["short_wkfw_name"];
	$wkfw_history_no  = $arr_apply_wkfwmst[0]["wkfw_history_no"];
	$wkfwfile_history_no = $arr_apply_wkfwmst[0]["wkfwfile_history_no"];
	$apply_title_disp_flg  = $arr_apply_wkfwmst[0]["apply_title_disp_flg"];

	$year = substr($apply_date, 0, 4);
	$md   = substr($apply_date, 4, 4);
	if($md >= "0101" and $md <= "0331")
	{
		$year = $year - 1;
	}

	$apply_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

	$notice_sel_flg = $arr_apply_wkfwmst[0]["notice_sel_flg"];

	$wkfw_appr_nm   = ($wkfw_appr == "1") ? "同報" : "稟議（回覧）";
	$apply_date     = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $apply_date);

	$wkfw_content   = $arr_apply_wkfwmst[0]["wkfw_content"];
	$wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
	// 本文形式タイプのデフォルトを「テキスト」とする
	if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

	if ($wkfw_content_type == "2")
	{
		$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
		$wkfw_content = $arr_wkfw_template_history["wkfw_content"];
	}

	$apply_lt_name  = $arr_apply_wkfwmst[0]["emp_lt_nm"];
	$apply_ft_name  = $arr_apply_wkfwmst[0]["emp_ft_nm"];
	$apply_full_nm  = "$apply_lt_name $apply_ft_name";

	$wkfw_title     = $arr_apply_wkfwmst[0]["wkfw_title"];
	$wkfw_nm        = $arr_apply_wkfwmst[0]["wkfw_nm"];
	$wkfw_folder_id = $arr_apply_wkfwmst[0]["wkfw_folder_id"];

	$emp_class_nm      = $arr_apply_wkfwmst[0]["apply_class_nm"];
	$emp_attribute_nm  = $arr_apply_wkfwmst[0]["apply_atrb_nm"];
	$emp_dept_nm       = $arr_apply_wkfwmst[0]["apply_dept_nm"];
	$emp_room_nm       = $arr_apply_wkfwmst[0]["apply_room_nm"];

	$class_nm  = $emp_class_nm;
	$class_nm .= " > ";
	$class_nm .= $emp_attribute_nm;
	$class_nm .= " > ";
	$class_nm .= $emp_dept_nm;

	if ($emp_room_nm != "")
	{
		$class_nm .= " > ";
		$class_nm .= $emp_room_nm;
	}

	// カテゴリ名(フォルダパス)取得
	$folder_path = $wkfw_nm;
	if($wkfw_folder_id != "")
	{
		// フォルダ名
		$folder_list = $obj->get_folder_path($wkfw_folder_id);
		foreach($folder_list as $folder)
		{
			if($folder_path != "")
			{
				$folder_path .= " > ";
			}
			$folder_path .= $folder["name"];
		}
	}

	// 申請結果通知情報取得
	$arr_applynotice = $obj->get_applynotice($apply_id);


	// フォーマットファイル情報取得
	$wkfw_file_id = array();
	$wkfw_filename = array();
	if($wkfwfile_history_no != "")
	{
		$arr_wkfwfile = $obj->get_wkfwfile_history($wkfw_id, $wkfwfile_history_no);
		foreach($arr_wkfwfile as $wkfwfile)
		{
			array_push($wkfw_file_id, $wkfwfile["wkfwfile_no"]);
			array_push($wkfw_filename, $wkfwfile["wkfwfile_name"]);
		}
	}

	// 添付ファイル情報取得
	$arr_applyfile = $obj->get_applyfile($apply_id);
	$file_id = array();
	$filename = array();
	foreach($arr_applyfile as $applyfile)
	{
		$tmp_file_no = $applyfile["applyfile_no"];
		$tmp_filename = $applyfile["applyfile_name"];

		array_push($file_id, $tmp_file_no);
		array_push($filename, $tmp_filename);

		// 一時フォルダにコピー
		$ext = strrchr($tmp_filename, ".");
		copy("cl/apply/{$apply_id}_{$tmp_file_no}{$ext}", "cl/apply/tmp/{$session}_{$tmp_file_no}{$ext}");
	}

	// 前提とする申請書取得
	$arr_applyprecond = $obj->get_applyprecond($apply_id);
	$precond_wkfw_id = array();
	$precond_wkfw_title = array();
	$precond_apply_id = array();
	$precond_apply_no = array();
	foreach($arr_applyprecond as $applyprecond)
	{
		array_push($precond_wkfw_id, $applyprecond["precond_wkfw_id"]);
		array_push($precond_wkfw_title, $applyprecond["wkfw_title"]);
		array_push($precond_apply_id, $applyprecond["precond_apply_id"]);

		$tmp_apply_date = $applyprecond["apply_date"];
		$tmp_short_wkfw_name = $applyprecond["short_wkfw_name"];
		$tmp_apply_no        = $applyprecond["apply_no"];
		$year = substr($tmp_apply_date, 0, 4);
		$md   = substr($tmp_apply_date, 4, 4);
		if($md >= "0101" and $md <= "0331")
		{
			$year = $year - 1;
		}
		$tmp_apply_no = $tmp_short_wkfw_name."-".$year."".sprintf("%04d", $tmp_apply_no);

		$tmp_apply_title = $applyprecond["apply_title"];
		if($tmp_apply_title != "")
		{
			$tmp_apply_no = $tmp_apply_no." (".$tmp_apply_title.")";
		}
		array_push($precond_apply_no, $tmp_apply_no);
	}


	// 職員ＩＤ取得
	$login_emp_id = search_emp_id($con, $fname, $session);

	// 承認者情報取得
	$arr_applyapv = $obj->get_applyapv($apply_id);
	$approve_num  = count($arr_applyapv);    // 承認者数

	// 他承認者コメント・承認状況の非公開チェック
	// 稟議の場合
	$closed_flg = false;
    if($wkfw_appr == "2")
    {
		$arr_applyapv_per_hierarchy = $obj->get_applyapv_per_hierarchy($apply_id, $apv_order_fix);
		// 通知タイミング取得
		$next_notice_div = $arr_applyapv_per_hierarchy[0]["next_notice_div"];

		// 非同期の場合
		if($next_notice_div == "1")
		{
		    $same_hierarchy_apvcnt = $obj->get_same_hierarchy_apvcnt($apply_id, $apv_order_fix);
		    // 該当承認者の同一階層に他の承認者がいる場合(複数承認者)
		    if($same_hierarchy_apvcnt > 1)
		    {
                 $closed_flg = true;
		    }
		}
    }

	// 非同期通知の場合の送信側、コメント・承認状況表示制限
	if($wkfw_appr == "2" && $send_apved_order_fix != "")
	{
		$arr_applyasyncrecv = $obj->get_applyasyncrecv($apply_id, $apv_order_fix, $apv_sub_order_fix, $send_apved_order_fix);
	}

?>
    <div id="dtl_tbl">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="block_in">
    <tr>
    <td align="center">
    <table width="100%" border="0" cellspacing="0" cellpadding="2" class="block2">

    <col class="col1" width="160"/>
    <col class="col2" width="140"/>
    <col class="col3" width="100"/>
    <col class="col4" width="120"/>
    <col class="col5" width="100"/>
    <col class="col6" width="160"/>

    <tr>
    <td height="22" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請書名</font></td>
    <td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($wkfw_title)?></font></td>
    <td height="22" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
    <td ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$apply_date?></font></td>
    </tr>

    <tr>
    <td height="22" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請番号</font></td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($apply_no)?></font></td>
    <td height="22" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font></td>
    <td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($folder_path)?></font></td>
    </tr>

    <tr>
    <td height="22" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者</font></td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($apply_full_nm)?></font></td>
    <td height="22" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属</font></td>
    <td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($class_nm)?></font></td>
    </tr>

<?
	if($apply_title_disp_flg == "t")
	{
?>
<!-- 表題 -->
    <tr height="22">
    <td align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題</font></td>
    <td colspan="5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($apply_title)?></font></td>
    </tr>
<?
	}
?>
<!-- 本文 -->
    <tr>
    <td height="60" valign="top" colspan="6">
<?
	// テンプレート 履歴番号
	$wkfw_history_no = str_pad($wkfw_history_no, 4, 0, STR_PAD_LEFT);

	// テンプレートファイル
	$savefilename = "cl/template/alias/{$short_wkfw_name}/{$short_wkfw_name}_{$wkfw_id}_{$wkfw_history_no}.php";

	// テンプレートファイル存在チェック
	if (!is_file($savefilename)) {
		echo("<script language=\"javascript\">alert('テンプレートファイルがありません。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}

	include($savefilename);
?>
    </td>
    </tr>

<!-- 申請者以外の結果通知 -->
<?
if(count($arr_applynotice) > 0)
{
?>
    <tr height="60">
    <td height="22" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者以外の結果通知</font>
    </td>
    <td colspan="5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="disp_notice_emp_nm"></span></font></td>
    </tr>


    <script type="text/javascript">
<?
	$disp_notice_emp_nm = "";
    foreach($arr_applynotice as $applynotice)
    {
		if($disp_notice_emp_nm != "")
        {
			$disp_notice_emp_nm .= ", ";
        }
		$emp_lt_nm   = $applynotice["emp_lt_nm"];
		$emp_ft_nm   = $applynotice["emp_ft_nm"];
		$emp_full_nm = $emp_lt_nm." ".$emp_ft_nm;

		$disp_notice_emp_nm .= $emp_full_nm;
    }
?>
    document.getElementById("disp_notice_emp_nm").innerHTML = '<?=$disp_notice_emp_nm?>'

    </script>
<?
}
?>

<!-- 前提とする申請書 -->
<?
if(count($precond_wkfw_id) > 0)
{
?>
    <tr height="40">
    <td height="22" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前提とする申請書</font></td>
    <td colspan="5">
	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="block">
	<tr bgcolor="#E5F6CD">
	<td width="200"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請書様式</font></td>
	<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認確定の申請書</font></td>
	</tr>
<?
	for ($i=0; $i<count($precond_wkfw_id); $i++)
	{
		$tmp_precond_wkfw_id = $precond_wkfw_id[$i];
		$tmp_precond_wkfw_title = $precond_wkfw_title[$i];
		$tmp_precond_apply_id = $precond_apply_id[$i];
		$tmp_precond_apply_no = $precond_apply_no[$i];
?>
        <tr>
        <td width="200"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($tmp_precond_wkfw_title)?></font></td>
        <td>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="precond_<?=$i+1?>"><?=htmlspecialchars($tmp_precond_apply_no)?></span></font>
        </td>
        </tr>
<?
	}
?>
	</table>
    </td>
    </tr>
<?
}
?>


<!-- フォーマットファイル -->
<?
if(count($wkfw_filename) > 0)
{
?>
    <tr>
    <td height="22" align="right" bgcolor="#E5F6CD" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォーマットファイル</font></td>
    <td colspan="5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

<?
	for ($i=0; $i<count($wkfw_filename); $i++) {
		$tmp_wkfw_file_id = $wkfw_file_id[$i];
		$tmp_wkfw_filename = $wkfw_filename[$i];
		$ext = strrchr($tmp_wkfw_filename, ".");
?>
        <p id="w_<?=$tmp_wkfw_file_id?>">
        <a href="cl/template/<?=$wkfw_id?>_<?=$tmp_wkfw_file_id?><?=$ext?>" target="_blank"><?=$tmp_wkfw_filename?></a>
        </p>
<?
	}
?>
    </font></td>
    </tr>
<?
}
?>


<!-- 添付ファイル -->

    <tr>
    <td height="22" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">添付ファイル</font></td>
    <td colspan="5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

<?
	for ($i = 0; $i < count($filename); $i++) {
		$tmp_file_id = $file_id[$i];
		$tmp_filename = $filename[$i];
		$ext = strrchr($tmp_filename, ".");
?>
        <p id="p_<?=$tmp_file_id?>">
        <a href="cl/apply/<?=$apply_id?>_<?=$tmp_file_id?><?=$ext?>" target="_blank"><?=$tmp_filename?></a>
        </p>
<?
	}
?>
    </font></td>
    </tr>

<?


	// 他承認者のコメント
	$other_comment = "";
	$apv_comment = "";

	for($i=0; $i<$approve_num; $i++)
	{
		$applyapv = $arr_applyapv[$i];
		$apv_order = $applyapv["apv_order"];
		$apv_sub_order = $applyapv["apv_sub_order"];

		if($apv_order == $apv_order_fix && $apv_sub_order == $apv_sub_order_fix)
		{
			$apv_comment = $applyapv["apv_comment"];
			$apv_stat = $applyapv["apv_stat"];
		}
		else
		{
			$apv_cmt_flg = false;
			if($apv_order != $apv_order_fix)
			{
				$apv_cmt_flg = true;
			}
			else
			{
				if(!$closed_flg)
				{
					$apv_cmt_flg = true;
				}
			}

			// 非同期通知の場合の送信側、コメント表示制限
			if(count($arr_applyasyncrecv) > 0)
			{
				$send_apv_order = $arr_applyasyncrecv[0]["send_apv_order"];

				if($apv_order == $send_apv_order)
				{
					$send_apv_flg = false;
					foreach($arr_applyasyncrecv as $applyasyncrecv)
					{
						$send_apv_sub_order = $applyasyncrecv["send_apv_sub_order"];
						if($apv_sub_order == $send_apv_sub_order || $send_apv_sub_order == "")
						{
							$send_apv_flg = true;
						}
					}
					if($send_apv_flg)
					{
						$apv_cmt_flg = true;
					}
					else
					{
						$apv_cmt_flg = false;
					}
				}
			}

			if($apv_cmt_flg)
			{
				$emp_lt_nm   = $applyapv["emp_lt_nm"];
				$emp_ft_nm   = $applyapv["emp_ft_nm"];
				$tmp_comment = $applyapv["apv_comment"];
				$emp_full_nm = $emp_lt_nm." ".$emp_ft_nm;

				if($tmp_comment != "") {
					$other_comment .= "【";
					$other_comment .= $emp_full_nm;
					$other_comment .= "さんのコメント】";
					$other_comment .= "\n";
					$other_comment .= $tmp_comment;
					$other_comment .= "\n\n";
				}
			}
		}

	}

	$other_comment = str_replace("\n", "<br>", $other_comment);
// 印刷時は画面入力値で置き換え
	if ($mode == "approve_print") {
		$apv_comment = str_replace("\n", "<br>", $comment);
	}

	echo("<tr>\n");
	echo("<td align=\"right\" bgcolor=\"#E5F6CD\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">コメント</font></td>\n");
	$comment_bgcolor = "#ffcccc";
	if ($mode == "approve_print" || $history_flg == true || $apply_stat == 1 || $apv_stat > 0) {
		$comment_bgcolor = "#ffffff";
	}
	echo("<td colspan=\"5\">");
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"block3\">\n");

	echo("<tr>");
	echo("<td>");
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$other_comment</font>\n");
	echo("</td>");
	echo("</tr>");

	echo("<tr>");
	echo("<td bgcolor=\"$comment_bgcolor\">");

	if ($apply_stat != 1 && $apv_stat == 0 && $mode != "approve_print" && $history_flg == false) {

		if ($changes_flg == 1) {

			$apv_comment = $apv_comment."所属が変更されているため差戻しとなりました。新しい所属で再申請してください。";
		} else if ($changes_flg == 2) {
			$apv_comment = $apv_comment."承認者の所属が変更されているため差戻しとなりました。再申請してください。";
		} else if ($changes_flg == 3) {
			$apv_comment = $apv_comment."承認者の役職が変更されているため差戻しとなりました。再申請してください。";
		}
	}

	if ($mode == "approve_print")
	{
		if($apv_comment != "")
		{
			for($i=0; $i<$approve_num; $i++)
			{
				$applyapv = $arr_applyapv[$i];

				if($login_emp_id == $applyapv["emp_id"])
				{
					$emp_lt_nm = $applyapv["emp_lt_nm"];
					$emp_ft_nm = $applyapv["emp_ft_nm"];

					$login_emp_full_nm .= "【";
					$login_emp_full_nm .= $emp_lt_nm." ".$emp_ft_nm;;
					$login_emp_full_nm .= "さんのコメント】";
					$login_emp_full_nm .= "\n";

					$login_emp_full_nm = str_replace("\n", "<br>", $login_emp_full_nm);
					break;
				}
			}
			echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$login_emp_full_nm</font>\n");
			echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$apv_comment</font>\n");
		}
	}
	else
	{
		echo("<textarea name=\"apv_comment\" rows=\"5\" cols=\"70\" style=\"ime-mode: active;\" onFocus=\"new ResizingTextArea(this);\"");
		echo(">".$apv_comment."</textarea></td>\n");
	}

	echo("</tr>\n");
	echo("</table>\n");

	echo("</td>\n");
	echo("</tr>\n");

	echo("</table>\n");

?>
    <img src="img/spacer.gif" width="1" height="3"><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認者数：<?=$approve_num?>人　承認タイプ：<?=$wkfw_appr_nm?></font>
    </td>
    </tr>
    </table>


<!-- 承認者段組表示 -->
    <img src="img/spacer.gif" width="1" height="3"><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="2" class="block2">

<?
	$regist_btn_flg = false;
	$col_num = 5;

	$rows = ceil($approve_num / $col_num);

	for ($i=0; $i<$rows; $i++)
	{
?>
        <tr height="22">
<?
		// 番号表示
		for ($j=$i*$col_num; $j<$i*$col_num+$col_num; $j++)
		{

?>
            <td align="center" width="20%" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
			if($j < $approve_num)
			{
				$applyapv = $arr_applyapv[$j];
				$apv_order = $applyapv["apv_order"];
				$apv_sub_order = $applyapv["apv_sub_order"];

				$no = $apv_order;
				if($apv_sub_order != "")
				{
					$no .= "_";
					$no .= $apv_sub_order;
				}
			 	echo("$no\n");
			}
?>
            </font></td>
<?
		}
?>
        </tr>
        <tr height="22">

<?
		// 役職表示
		for ($j=$i*$col_num; $j<$i*$col_num+$col_num; $j++)
		{

?>
            <td align="center" width="20%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
			if($j < $approve_num)
			{
				$applyapv        = $arr_applyapv[$j];
				$st_nm           = $applyapv["st_nm"];
				$st_div          = $applyapv["st_div"];
				$parent_pjt_name = $applyapv["parent_pjt_name"];
				$child_pjt_name  = $applyapv["child_pjt_name"];

				switch($st_div){
					case 3:
						$pjt_nm = $parent_pjt_name;
						if($child_pjt_name != "")
						{
							$pjt_nm .= " > ";
							$pjt_nm .= $child_pjt_name;
						}
						echo($pjt_nm);
						break;
					case 5:
?>
						主催者
<?
						break;
					case 6:
?>
						講師
<?
						break;
					case 7:
?>
						課題提出先
<?
						break;
					default:
						if($st_nm != "")
						{
							echo($st_nm);
						}
				}
			}
?>
			</font></td>
<?
		}
?>
		</tr>
		<tr height="100">

<?
		// 職員名表示
		for ($j=$i*$col_num; $j<$i*$col_num+$col_num; $j++)
		{
?>
			<td align="center" width="20%">
<?
			if ($j < $approve_num)
			{
				$applyapv        = $arr_applyapv[$j];
				$multi_apv_flg   = $applyapv["multi_apv_flg"];
				$apv_order       = $applyapv["apv_order"];
				$apv_sub_order   = $applyapv["apv_sub_order"];
				$next_notice_div = $applyapv["next_notice_div"];
				$emp_lt_nm       = $applyapv["emp_lt_nm"];
				$emp_ft_nm       = $applyapv["emp_ft_nm"];
				$apply_id        = $applyapv["apply_id"];
				$apv_emp_id      = $applyapv["emp_id"];
				$apv_full_nm     = "$emp_lt_nm $emp_ft_nm";
				$apv_stat        = $applyapv["apv_stat"];
				$apv_date        = $applyapv["apv_date"];

				$apv_date = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $apv_date);

				$other_apv_flg   = $applyapv["other_apv_flg"];

				$apv_status_flg = false;
				if($apv_order != $apv_order_fix)
				{
					$apv_status_flg = true;
				}
				else
				{
					if(!$closed_flg)
					{
						$apv_status_flg = true;
					}
					else
					{
						if($apv_order == $apv_order_fix && $apv_sub_order == $apv_sub_order_fix)
						{
							$apv_status_flg = true;
						}
					}
				}
				// 非同期通知の場合の送信側、承認状況表示制限
				$apv_send_status_flg = true;

				if(count($arr_applyasyncrecv) > 0)
				{
					$send_apv_order = $arr_applyasyncrecv[0]["send_apv_order"];
					if($apv_order == $send_apv_order)
					{
						$send_apv_flg = false;
						foreach($arr_applyasyncrecv as $applyasyncrecv)
						{
							$send_apv_sub_order = $applyasyncrecv["send_apv_sub_order"];
							if($apv_sub_order == $send_apv_sub_order || $send_apv_sub_order == "")
							{
								$send_apv_flg = true;
							}
						}
						if($send_apv_flg)
						{
							$apv_status_flg = true;
						}
						else
						{
							$apv_status_flg = false;
							$apv_send_status_flg = false;
						}
					}
				}

				// 印影機能を利用
				$imprint_flg = get_imprint_flg($con, $apv_emp_id, $fname);
				if($apv_stat != 0)
				{
					if($apv_status_flg)
					{
						show_imprint_image($session, $apv_emp_id, $apv_stat, $imprint_flg, "f");
					}
				}

				if($mode == "approve_print" && $drag_flg == 't' && $apv_order == $apv_order_fix && $apv_sub_order == $apv_sub_order_fix)
				{
					show_imprint_image($session, $apv_emp_id, $approve, $imprint_flg, "f");
				}

				if(($apv_order == $apv_order_fix && $apv_sub_order == $apv_sub_order_fix) && $mode != "approve_print" && $history_flg == false)
				{

					if($apv_stat == 0)
					{
					// ドラッグアンドドロップのターゲット
?>
					<div id="target1" style="width: 60px; height: 60px; background-color: #ffcccc;"><img id="img1" src="img/spacer.gif">
					</div>
<?
					}
?>
					<input type="hidden" name="next_notice_div" value="<?=$next_notice_div?>">
					<input type="hidden" name="apv_order" value="<?=$apv_order?>">
					<input type="hidden" name="apv_sub_order" value="<?=$apv_sub_order?>">
					<input type="hidden" name="apv_emp_id" value="<?=$apv_emp_id?>">

<?
				}
?>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($apv_full_nm)?></font><br>
<?

				if(($apv_order == $apv_order_fix && $apv_sub_order == $apv_sub_order_fix ) && $apv_stat == 0 && $history_flg == false)
				{

					echo("<select name=\"approve\"");
					if($mode != "approve_print") {
						echo(" onchange=\"set_target(this.value);\"");
					}
					echo(">\n");
					echo("<option value=\"1\">承認</option>\n");
					echo("<option value=\"2\">否認</option>\n");
					echo("<option value=\"3\">差戻し</option>\n");
					echo("</select>\n");
					echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">&nbsp;$apv_date</font>\n");
					$regist_btn_flg = true;

					if($mode == "approve_print") {
						echo("<script language='javascript'>\n");
						echo("document.apply.approve.value = \"".$approve."\";\n");
						echo("</script>\n");
					}
				} else {

					if($apv_status_flg)
					{
						if($apv_stat == 0)
						{
							$apv_stat_nm = '未承認';
						}
						else if($apv_stat == 1)
						{
							$apv_stat_nm = '承認';
						}
						else if($apv_stat == 2)
						{
							$apv_stat_nm = '否認';
						}
						else if($apv_stat == 3)
						{
							$apv_stat_nm = '差戻し';
						}

						if($other_apv_flg == 't')
						{
							$apv_stat_nm .= '(他者)';
						}
?>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;<?=$apv_stat_nm?>&nbsp;<br><?=$apv_date?></font>
<?
					}
					else
					{
						if($apv_send_status_flg)
						{
?>
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非公開</font>
<?
						}
						else
						{
?>
							&nbsp;
<?
						}
					}
				}
?>
				</td>
<?
			}
		}
?>
		</tr>
<?
	}

?>
	<input type="hidden" name="wkfw_appr" value=<?=$wkfw_appr?>>
	</table>
	</td>
	</tr>
	</table>
	</div>

	<img src="img/spacer.gif" width="1" height="5"><br>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr align="right">
	<td>

<?
	if($mode != "approve_print" ) {
		if($regist_btn_flg == true && $history_flg == false) {
?>
			<input type="button" value="下書き保存" onclick="approve_draft_regist();">
			<input type="button" value="登録" onclick="approve_regist();">
<?
		} else {
?>
			<input type="button" value="下書き保存" onclick="approve_draft_regist();" disabled>
<?
		$disabled = "disabled";
		$last_apv_order = $obj->get_last_apv_order($apply_id);
		if($apv_order_fix < $last_apv_order)
		{
			$same_hierarchy_apvcnt = $obj->get_same_hierarchy_apvcnt($apply_id, $last_apv_order);
			$non_same_hierarchy_apvstatcnt = $obj->get_same_hierarchy_apvstatcnt($apply_id, $last_apv_order, "0");
			if($same_hierarchy_apvcnt == $non_same_hierarchy_apvstatcnt)
			{
				$disabled = "";
			}
		}

		// レベルアップ志願書の場合は無条件で更新可とする
		if ($short_wkfw_name == "c101" || $short_wkfw_name == "c102") {$disabled = "";}
?>
		<input type="button" value="更新" onclick="approve_update();" <?=$disabled?>>
<?
		}
?>
		<input type="button" value="印刷" onclick="approve_print();">
<?
	}
?>
	</td>
	</tr>
	</table>

	<img src="img/spacer.gif" width="1" height="10"><br>
	<input type="hidden" name="drag_flg" value="">

<script type="text/javascript">

// 印刷
function approve_print()
{



	window.open('', 'approve_detail_print_window', 'width=670,height=700,scrollbars=yes');

	document.apply.action="cl_application_approve_detail_print.php";
	document.apply.target="approve_detail_print_window";
	document.apply.mode.value = "approve_print";
	document.apply.print_history_flg.value = '<?=$history_flg?>';
	document.apply.print_apv_order_fix.value = '<?=$apv_order_fix?>';
	document.apply.print_apv_sub_order_fix.value = '<?=$apv_sub_order_fix?>';
	document.apply.print_send_apved_order_fix.value = '<?=$send_apved_order_fix?>';
	//session,target_apply_id,approve,drag_flg,apv_commentはhidden初期設定値を使用
	document.apply.submit();

	document.apply.target="_self";





//	document.approve_print_form.history_flg.value = '<?=$history_flg?>';
//	document.approve_print_form.apv_order_fix.value = '<?=$apv_order_fix?>';
//	document.approve_print_form.apv_sub_order_fix.value = '<?=$apv_sub_order_fix?>';
//	document.approve_print_form.send_apved_order_fix.value = '<?=$send_apved_order_fix?>';

<?
if ($regist_btn_flg == true)
{
?>
//	document.approve_print_form.approve.value = document.apply.approve.value;
//	document.approve_print_form.drag_flg.value = document.apply.drag_flg.value;
<?
}
?>

//	document.approve_print_form.apv_print_comment.value = document.apply.apv_comment.value;

//	var h = 700;
//	var w = 670;
//	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=" + w + ",height=" + h;
//	window.open('', 'approve_detail_print_window', option);
//	document.approve_print_form.submit();
}

function approve_simpleprint() {
	document.approve_print_form.simple.value = "t";
	approve_print();
	document.approve_print_form.simple.value = "";
}

function openGraderList(input_div, level) {

	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 680;
	wy = 500;
	var url = 'cl_grader_list.php';
	url += '?session=<?=$session?>&input_div=' + input_div + '&level=' + level;
	childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

/**
 * Grader List を表示する
 * @param input_div	String	フィールドid
 * @param level		String	レベル
 * @param nurse_type	String	看護種別
 * @auther 2011.12.14 Add by matsuura
 */
function openGraderList2(input_div, level, nurse_type) {

	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 680;
	wy = 500;
	var url = 'cl_grader_list.php';
	url += '?session=<?=$session?>&input_div=' + input_div + '&level=' + level + '&nurse_type=' + nurse_type;;
	childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

// 職員指定画面からよばれる関数
function add_target_list(item_id, emp_id, emp_name)
{

	var emp_ids = emp_id.split(",");
	var emp_names = emp_name.split(",");

	// casテンプレートにある職員名簿ボタンの呼び出しかどうか判断する。
	regObj = new RegExp('cl');
	result = item_id.match(regObj);

	if(result != 'cl')
	{
		// 承認者の場合
		if(item_id != "0")
		{
			if(emp_ids.length > 1)
			{
				alert('職員は１名のみ指定してください。');
				return false;
			}

			var post = 'post_' + item_id;
			var apvemp = 'apvemp_' + item_id;
			document.getElementById(apvemp).innerHTML = emp_names;
			var emp_id = 'regist_emp_id' + item_id;
			document.apply.elements[emp_id].value = emp_ids;

			get_empst_ajax(emp_ids, item_id);
		}
		// 申請結果通知者の場合
		else if(item_id == "0")
		{
			//既存入力されている職員
			existed_emp_ids = document.getElementById('notice_emp_id').value;
			existed_emp_nms = document.getElementById('notice_emp_nm').value;
			existed_rslt_ntc_div = document.getElementById('rslt_ntc_div').value;

			tmp_ids   = "";
			tmp_names = "";
			tmp_rslt_ntc_div = "";
			if(existed_emp_ids != "")
			{
				arr_existed_emp_id = existed_emp_ids.split(",");
				for(i=0; i<emp_ids.length; i++)
				{
					// ログインユーザの場合
					if(emp_ids[i] == '<?=$login_emp_id?>')
					{
						continue;
					}

					dpl_flg = false;
					for(j=0; j<arr_existed_emp_id.length; j++)
					{
						// 重複した場合
						if(emp_ids[i] == arr_existed_emp_id[j])
						{
							dpl_flg = true;
						}
					}
					if(!dpl_flg)
					{
						if(tmp_ids != "")
						{
							tmp_ids   += ",";
							tmp_names += ",";
							tmp_rslt_ntc_div += ",";
						}
						tmp_ids   += emp_ids[i];
						tmp_names += emp_names[i];
						tmp_rslt_ntc_div += "2";
					}
				}
			}
			else
			{
				for(i=0; i<emp_ids.length; i++)
				{
					// ログインユーザの場合
					if(emp_ids[i] == '<?=$login_emp_id?>')
					{
						continue;
					}

					if(tmp_ids != "")
					{
						tmp_ids   += ",";
						tmp_names += ",";
						tmp_rslt_ntc_div += ",";
					}
					tmp_ids   += emp_ids[i];
					tmp_names += emp_names[i];
					tmp_rslt_ntc_div += "2";
				}
			}

			if(existed_emp_ids != "" && tmp_ids != "")
			{
				existed_emp_ids += ",";
			}
			existed_emp_ids += tmp_ids;
			document.getElementById('notice_emp_id').value = existed_emp_ids;

			if(existed_emp_nms != ""  && tmp_names != "")
			{
				existed_emp_nms += ",";
			}
			existed_emp_nms += tmp_names;
			document.getElementById('notice_emp_nm').value = existed_emp_nms;

			if(existed_rslt_ntc_div != "" && tmp_rslt_ntc_div != "")
			{
				existed_rslt_ntc_div += ",";
			}
			existed_rslt_ntc_div += tmp_rslt_ntc_div;
			document.getElementById('rslt_ntc_div').value = existed_rslt_ntc_div;

			set_disp_area(existed_emp_ids, existed_emp_nms, existed_rslt_ntc_div);
		}
	}
	else
	{



		// レベルアップ志願書の場合
		if (item_id == 'cl_chief') {

			if(emp_ids.length > 1){
				alert('職員は１名のみ指定してください。');
				return false;
			}

			document.getElementById('clp_chief_emp_id').value = emp_ids[0];
			document.getElementById('clp_apply_chief_nm').value = emp_names[0];

		} else if (item_id == 'cl_general') {

			if(emp_ids.length > 1){
				alert('職員は１名のみ指定してください。');
				return false;
			}

			document.getElementById('clp_colleague_emp_id').value = emp_ids[0];
			document.getElementById('clp_apply_general_nm').value = emp_names[0];

		}
	}
}
</script>

<?
}



function search_emp_id($con, $fname, $session) {

	$sql = "select emp_id from session";
	$cond = "where session_id='$session'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$emp_id = pg_fetch_result($sel,0,"emp_id");

	return $emp_id;
}


// XMLから取得した値をもとに、入力項目の初期設定をするJS出力
function show_js_set_value($node_array, $mode) {
	foreach ($node_array as $child) {
		if ($child->node_type() == XML_ELEMENT_NODE) {
			$itemvalue = mb_convert_encoding($child->get_content(), "EUC-JP", "UTF-8");
			// \、"をエスケープする
			if ($type != "checkbox" && $type != "radio") {
				$itemvalue = str_replace("\\", "\\\\", $itemvalue);
				$itemvalue = str_replace("\"", "\\\"", $itemvalue);
			}

			$itemvalue = str_replace("\r\n", "\n", $itemvalue);
			$itemvalue = str_replace("\n", "\\n", $itemvalue);

			$type = $child->get_attribute("type");
			// チェックボックス
			if( $type == "checkbox" ) {
				$id = $child->get_attribute("id");
				echo("document.apply.elements['$child->tagname[$id]'].checked = true;\n");

			} else if( $type == "radio" ) {
			// ラジオボタン
				echo("for(i=0; i<document.apply.elements['$child->tagname[]'].length; i++) {\n");

				echo("	if(document.apply.elements['$child->tagname[]'][i].value == '$itemvalue') {\n");
				echo("		document.apply.elements['$child->tagname[]'][i].checked = true;\n");

				echo("	}\n");
				echo("}\n");

			} else {
			// テキスト、テキストエリア、セレクト
				if ($mode != "approve_print") {
					echo("document.apply.".$child->tagname.".value = \"".$itemvalue."\";\n");
				} else {
					//印刷時カレンダー月日をテキストにする
					if (strstr($child->tagname, "date_m") ||
						strstr($child->tagname, "date_d")) {
						if ($itemvalue == "-") {
							$itemvalue_text = "";
						} else {
							$itemvalue_text = intval($itemvalue);
						}
						echo("if (document.apply.".$child->tagname.".getAttribute('type') == 'text') {\n");
						echo("document.apply.".$child->tagname.".value = \"".$itemvalue_text."\";\n");
						echo("} else {\n");
						echo("document.apply.".$child->tagname.".value = \"".$itemvalue."\";\n");
						echo("}\n");
					} else {
						echo("document.apply.".$child->tagname.".value = \"".$itemvalue."\";\n");
					}
				}
			}
		}
	}
}

// 評価者名取得
function get_eval_emp_name($con, $fname, $apply_id, $apv_order_fix, $apv_sub_order_fix, $send_apved_order_fix, $obj) {

	if($apv_order_fix == "2") {
		$tmp_order = 1;
		$tmp_sub_order = $obj->get_send_apv_sub_order($apply_id, $send_apved_order_fix);
	} else {
		$tmp_order = $apv_order_fix;
		$tmp_sub_order = $apv_sub_order_fix;
	}

	$sql = "select emp_lt_nm||' '||emp_ft_nm as emp_name from cl_applyapv a left join empmst b on b.emp_id = a.emp_id";
	$cond = "where apply_id = '$apply_id' and apv_order = $tmp_order and apv_sub_order = $tmp_sub_order";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$num = pg_numrows($sel);
	if ($num > 0) {
		$emp_name = pg_fetch_result($sel,0,"emp_name");
	} else {
		$emp_name = "";
	}

	return $emp_name;

}


?>