<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病棟情報権限チェック
$ward = check_authority($session,21,$fname);
if($ward == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//**********DB処理**********
//----------ＤＢのコネクション作成----------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//----------Transaction begin----------
pg_exec($con,"begin transaction");

//----------正規表現チェック----------
if($bldg_cd == ""){
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

if($ward_cd == ""){
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

if($ptrm_nm == ""){
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"病室名が入力されていません。\");</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

// 病棟区分未使用チェック
$sql = "select wddv_use_flg from wddvmst";
$cond = "where wddv_id = '$ward_type'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_fetch_result($sel, 0, "wddv_use_flg") == "f") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('選択された病棟区分は「使用しない」と設定されています。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

if($max_bed_no < $bed_cur){
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"現行の病床数が最大病床数をこえています。\");</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

if($max_bed_no > 10){
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"最大病床数の値が不正です。\");</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

if($window == ""){
	$window = "f";
}else{
	$window = "t";
}

if($window == ""){
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"部屋タイプの値が不正です。\");</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

if($sex == "" ){
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"性別を選択してください。\");</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

if($sex > 3){
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"性別の値が不正です。\");</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

if($adult == "" ){
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"大人・子供を選択してください。\");</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

if($adult > 3){
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"大人・子供の値が不正です。\");</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

if($sp_flg == ""){
	$sp_flg = "f";
}else{
	$sp_flg = "t";
}

if($sp_flg == "" ){
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"特殊部屋の値が不正です。\");</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

$check = positive_number_validation($bed_chg);
if($check == 0){
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"ベット差額は半角数字で入力してください。\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

$check = positive_number_validation($res_fee);
if($check == 0){
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"貸切室料は半角数字で入力してください。\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

if ($vertical == "t") {
	if ($bed_cur < 2 || $bed_cur > 5) {
		pg_query($con, "rollback");
		pg_close($con);
		$vertical_label = ($door_pos == "u" || $door_pos == "d") ? "縦置き配置" : "横置き配置";
		echo("<script language=\"javascript\">alert(\"{$vertical_label}は、現在病床数が2〜5床の場合のみ選択可能です。\");</script>\n");
		echo("<script language=\"javascript\">history.back();</script>\n");
		exit;
	}
} else {
	$vertical = "f";
}

$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd'";
$sel = select_from_table($con,$SQL70,$cond,$fname);			//max値を取得

if($sel==0){
	pg_exec($con,"rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$max = pg_result($sel,0,"max");
if($max == ""){
	$rm_no = "1";
}else{
	$rm_no = $max+1;
}

$sql = "insert into ptrmmst (ptrm_room_no, bldg_cd, ward_cd, ptrm_name, ptrm_bed_max, ptrm_bed_cur, ptrm_ocpy_no, ptrm_male_no, ptrm_female_no, ptrm_window, ptrm_adult, ptrm_sex, ptrm_bed_chg, ptrm_sp_flg, ptrm_type, ptrm_res_fee, ptrm_up_dt, ptrm_up_tm, ward_type, door_pos, vertical) values(";
$content = array($rm_no, $bldg_cd, $ward_cd, $ptrm_nm, $max_bed_no, $bed_cur, 0, 0, 0, $window, $adult, $sex, $bed_chg, $sp_flg, $room_type, $res_fee, date("Ymd"), date("Hi"), $ward_type, $door_pos, $vertical);
$in_room = insert_into_table($con,$sql,$content,$fname);
if($in_room==0){
	pg_exec($con,"rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// ライセンスチェック
$sql = "select lcs_bed_count from license";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$lcs_bed_count = pg_fetch_result($sel, 0, "lcs_bed_count");
if ($lcs_bed_count != "" && $lcs_bed_count != "FULL") {
	$sql = "select sum(r.ptrm_bed_max) from ptrmmst r";
	$cond = "where not r.ptrm_del_flg and exists (select * from bldgmst b where b.bldg_cd = r.bldg_cd and not b.bldg_del_flg) and exists (select * from wdmst w where w.ward_cd = r.ward_cd and not w.ward_del_flg)";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$reg_bed_count = pg_fetch_result($sel, 0, 0);

	if (intval($reg_bed_count) > intval($lcs_bed_count)) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script language='javascript'>alert('病床数がライセンスを超えるため登録できません。');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
}

// 棟の事業所IDを取得
$sql = "select enti_id from bldgmst";
$cond = "where bldg_cd = '$bldg_cd'";
$sel_enti = select_from_table($con, $sql, $cond, $fname);
if ($sel_enti == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$enti_id = pg_fetch_result($sel_enti, 0, "enti_id");

for($i=1;$i<=$bed_cur;$i++){
	$content = array($enti_id,$bldg_cd,$ward_cd,$rm_no,$i);

	$in_inpt = insert_into_table($con,$SQL100,$content,$fname);

	if($in_inpt==0){
		pg_exec($con,"rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}

pg_exec($con, "commit");
pg_close($con);
echo("<script language=\"javascript\">location.href=\"room_list.php?session=$session&bldg_cd=$bldg_cd&ward_cd=$ward_cd\"</script>");
?>