<?php
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_class_name.ini");
require_once("get_values.ini");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 69, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}
    
// データベースに接続
$con = connect2db($fname);
$emp_name = get_emp_kanji_name($con, $emp_id, $fname);
// 変更履歴を取得
if (!empty($_REQUEST["duty_shift_staff_history_id"])) {
    $empcond_duty_form_history_id = pg_escape_string($_REQUEST["duty_shift_staff_history_id"]);
    $sql = "SELECT * FROM duty_shift_staff_history WHERE duty_shift_staff_history_id=$duty_shift_staff_history_id";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $hist = pg_fetch_assoc($sel);
}
else {
    $hist = array("histdate" => date('Y-m-d H:i:s'));
}
//シフトグループ情報
$arr_group = array();
$sql = "select group_id, group_name from duty_shift_group";
$cond = "order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$num = pg_numrows($sel);
while ($row = pg_fetch_array($sel)) {
    $arr_group[$row["group_id"]] = $row["group_name"];
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成 | シフトグループ履歴</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>シフトグループ履歴</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>
<? echo("$emp_name"); ?>
</b></font><br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">■シフトグループ 変更履歴の追加/変更</font><br />
<br />
<form action="duty_shift_staff_history_submit.php" method="post">
<table width="500" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
    <td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフトグループ（病棟）名</font></td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    		<select name="group_id">
<?php
foreach ($arr_group as $wk_group_id => $wk_group_name) {
    echo("<option  value=\"$wk_group_id\"");
    if ($hist["group_id"] == $wk_group_id) {
			echo(" selected");
		}
    echo(">$wk_group_name  \n");
}
?>
		</select>
    </font></td>
</tr>
<tr height="22">
    <td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">変更日時</font></td>
    <td>
        <input type="text" name="histdate" value="<?=substr($hist["histdate"],0,19)?>" size="25"><br />
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">　9999-99-99 99:99:99の形式で入力してください</font>
    </td>
</tr>
</table>

<p>
<button type="submit">登録</button>
</p>

<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="duty_shift_staff_history_id" value="<? echo($duty_shift_staff_history_id); ?>">
</form>

</td>
</tr>
</table>

</body>
</html>
<? pg_close($con); ?>