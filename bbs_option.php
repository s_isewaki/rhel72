<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>掲示板 | オプション設定</title>
<?
require("about_session.php");
require("about_authority.php");
require("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 1, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 掲示板管理権限を取得
$bbs_admin_auth = check_authority($session, 62, $fname);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// オプション設定情報を取得
if ($back != "t") {
	$sql = "select whatsnew_flg, whatsnew_days, newpost_days, count_per_page from bbsoption";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$whatsnew_flg = pg_fetch_result($sel, 0, "whatsnew_flg");
		$whatsnew_days = pg_fetch_result($sel, 0, "whatsnew_days");
		$newpost_days = pg_fetch_result($sel, 0, "newpost_days");
		$count_per_page = pg_fetch_result($sel, 0, "count_per_page");
	}
}

// デフォルト値の設定
if ($whatsnew_flg == "") {$whatsnew_flg = "t";}
if ($whatsnew_days == "") {$whatsnew_days = 7;}
if ($newpost_days == "") {$newpost_days = 3;}
if ($count_per_page == "") {$count_per_page = 0;}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function setWhatsnewDaysDisabled() {
	var whatsnew_flg = (document.mainform.whatsnew_flg[0].checked);
	document.mainform.whatsnew_days.disabled = !whatsnew_flg;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="setWhatsnewDaysDisabled();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bbsthread_menu.php?session=<? echo($session); ?>"><img src="img/icon/b10.gif" width="32" height="32" border="0" alt="掲示板・電子会議室"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bbsthread_menu.php?session=<? echo($session); ?>"><b>掲示板・電子会議室</b></a> &gt; <a href="bbs_option.php?session=<? echo($session); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>"><b>オプション設定</b></a></font></td>
<? if ($bbs_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bbs_admin_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bbsthread_menu.php?session=<? echo($session); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">投稿一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="130" align="center" bgcolor="#5279a5"><a href="bbs_option.php?session=<? echo($session); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>オプション設定</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bbs_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<!--<img src="img/spacer.gif" width="1" height="2" alt=""><br>-->
<form name="mainform" action="bbs_option_update_exe.php" method="post">
<table width="700" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td align="left" valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>一般設定</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="1" class="list">
<tr height="22">
<td width="220" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">NEW とする期間</font></td>
<td>
<select name="newpost_days">
<? show_days_options($newpost_days); ?>
</select>
</td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td align="left" valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>最新情報</b>（マイページ）</font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="1" class="list">
<tr height="22">
<td width="220" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最近更新されたテーマを表示</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="whatsnew_flg" value="t"<? if ($whatsnew_flg == "t") {echo(" checked");} ?> onclick="setWhatsnewDaysDisabled();">する
<input type="radio" name="whatsnew_flg" value="f"<? if ($whatsnew_flg == "f") {echo(" checked");} ?> onclick="setWhatsnewDaysDisabled();">しない
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示期間</font></td>
<td>
<select name="whatsnew_days">
<? show_days_options($whatsnew_days); ?>
</select>
</td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td align="left" valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>投稿一覧</b>（マイページ、投稿一覧画面）</font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="1" class="list">
<tr height="22">
<td width="220" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ページあたりの表示件数</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="count_per_page">
<option value="0"<? if ($count_per_page == 0) {echo(" selected");} ?>>全件表示
<option value="5"<? if ($count_per_page == 5) {echo(" selected");} ?>>5件
<option value="10"<? if ($count_per_page == 10) {echo(" selected");} ?>>10件
<option value="20"<? if ($count_per_page == 20) {echo(" selected");} ?>>20件
</select>
※ツリー表示の場合、ルート投稿（○印）のみカウントされます
</font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="sort" value="<? echo($sort); ?>">
<input type="hidden" name="show_content" value="<? echo($show_content); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_days_options($selected) {
	echo("<option value=\"1\"");
	if ($selected == "1") {echo(" selected");}
	echo(">1日間（当日分のみ）\n");

	for ($i = 2; $i <= 31; $i++) {
		echo("<option value=\"$i\"");
		if ($selected == $i) {echo(" selected");}
		echo(">{$i}日間\n");
	}
}
?>
