<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix �¾����� | ���ౡ�������� | ����ͽ��ɽ</title>
<?
require_once("about_comedix.php");
require_once("show_select_values.ini");

$fname = $PHP_SELF;

// ���å����Υ����å�
$session = qualify_session($session, $fname);
if ($session == "0") {
	js_login_exit();
}

// ���¤Υ����å�
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	js_login_exit();
}

// ���Ų���Ͽ���¤����
$section_admin_auth = check_authority($session, 28, $fname);

// ������Ͽ���¤����
$ward_admin_auth = check_authority($session, 21, $fname);

// �ǡ����١�������³
$con = connect2db($fname);

// ɽ�����֤�����
if ($year == "") {$year = date("Y");}
if ($month == "") {$month = date("m");}
if ($day == "") {$day = date("d");}
if ($term == "") {$term = "1w";}
$start_date = "$year$month$day";
switch ($term) {
case "1w":
	$end_date = date("Ymd", strtotime("+6 days", mktime(0, 0, 0, $month, $day, $year)));
	break;
case "2w":
	$end_date = date("Ymd", strtotime("+13 days", mktime(0, 0, 0, $month, $day, $year)));
	break;
case "3w":
	$end_date = date("Ymd", strtotime("+20 days", mktime(0, 0, 0, $month, $day, $year)));
	break;
case "1m":
	$end_date = date("Ymd", strtotime("+1 month -1 day", mktime(0, 0, 0, $month, $day, $year)));
	break;
}

// �Ҳ𸵥ޥ��������
$inst_master = get_inst_master($con, $fname);

// ���԰��������
if ($mode == "") $mode = "r";  // �ǥե���Ȥ�ͽ���r�ˡ����ӡ�i��
if ($mode == "r") {

	// ����ͽ�괵�Ԥ�����ͽ��
	$sql = q("select bool 'f' as back_flg, i.inpt_in_res_dt as in_dt, i.inpt_in_res_tm as in_tm, w.ward_name, r.ptrm_name, r.ptrm_bed_cur, r.ptrm_type, r.ptrm_bed_chg, i.inpt_hotline, i.inpt_short_stay, i.inpt_outpt_test, p.ptif_lt_kana_nm, p.ptif_ft_kana_nm, p.ptif_lt_kaj_nm, p.ptif_ft_kaj_nm, p.ptif_birth, p.ptif_sex, d.dr_nm, i.inpt_patho_from, i.inpt_patho_to, i.inpt_pre_div, i.inpt_intro_inst_cd, i.ptif_id, i.in_res_updated as updated, null as in_dttm, (select max(n.note) from bed_innote n where n.ptif_id = i.ptif_id and n.type = 'p' and n.mode = 'r' and n.in_dttm is null and n.back_flg = 'f') as note from inptres i inner join ptifmst p on p.ptif_id = i.ptif_id left join wdmst w on w.bldg_cd = i.bldg_cd and w.ward_cd = i.ward_cd left join ptrmmst r on r.bldg_cd = i.bldg_cd and r.ward_cd = i.ward_cd and r.ptrm_room_no = i.ptrm_room_no left join drmst d on d.sect_id = i.inpt_sect_id and d.dr_id = i.dr_id where i.inpt_in_res_dt between '%s' and '%s'", $start_date, $end_date);

	// �ౡͽ�괵�Ԥ��������ͽ��
	$sql .= q(" union select 't', i.inpt_back_in_dt, i.inpt_back_in_tm, w.ward_name, r.ptrm_name, r.ptrm_bed_cur, r.ptrm_type, r.ptrm_bed_chg, null, null, null, p.ptif_lt_kana_nm, p.ptif_ft_kana_nm, p.ptif_lt_kaj_nm, p.ptif_ft_kaj_nm, p.ptif_birth, p.ptif_sex, d.dr_nm, null, null, null, null, i.ptif_id, i.back_res_updated as updated, null, (select max(n.note) from bed_innote n where n.ptif_id = i.ptif_id and n.type = 'p' and n.mode = 'r' and n.in_dttm is null and n.back_flg = 't') from inptmst i inner join ptifmst p on p.ptif_id = i.ptif_id left join wdmst w on w.bldg_cd = i.inpt_back_bldg_cd and w.ward_cd = i.inpt_back_ward_cd left join ptrmmst r on r.bldg_cd = i.inpt_back_bldg_cd and r.ward_cd = i.inpt_back_ward_cd and r.ptrm_room_no = i.inpt_back_ptrm_room_no left join drmst d on d.sect_id = i.inpt_back_sect_id and d.dr_id = i.inpt_back_dr_id where i.inpt_out_res_flg = 't' and i.inpt_back_in_dt between '%s' and '%s'", $start_date, $end_date);

	// �ౡ���Ԥ��������ͽ��
	$sql .= q(" union select 't', i.inpt_back_in_dt, i.inpt_back_in_tm, w.ward_name, r.ptrm_name, r.ptrm_bed_cur, r.ptrm_type, r.ptrm_bed_chg, null, null, null, p.ptif_lt_kana_nm, p.ptif_ft_kana_nm, p.ptif_lt_kaj_nm, p.ptif_ft_kaj_nm, p.ptif_birth, p.ptif_sex, d.dr_nm, null, null, null, null, i.ptif_id, i.back_res_updated as updated, (i.inpt_in_dt || i.inpt_in_tm), (select max(n.note) from bed_innote n where n.ptif_id = i.ptif_id and n.type = 'p' and n.mode = 'r' and n.in_dttm = cast((i.inpt_in_dt || i.inpt_in_tm) as varchar) and n.back_flg = 't') from inpthist i inner join ptifmst p on p.ptif_id = i.ptif_id left join wdmst w on w.bldg_cd = i.inpt_back_bldg_cd and w.ward_cd = i.inpt_back_ward_cd left join ptrmmst r on r.bldg_cd = i.inpt_back_bldg_cd and r.ward_cd = i.inpt_back_ward_cd and r.ptrm_room_no = i.inpt_back_ptrm_room_no left join drmst d on d.sect_id = i.inpt_back_sect_id and d.dr_id = i.inpt_back_dr_id where i.inpt_back_in_dt between '%s' and '%s' and not exists (select * from inptmst where inptmst.ptif_id = i.ptif_id)", $start_date, $end_date);

	$cond = "order by in_dt, in_tm";
} else {

	// �����洵�ԡ��������ԡ��ౡͽ�괵�ԡˤ���������
	$sql = q("select bool 'f' as back_flg, i.inpt_in_dt as in_dt, i.inpt_in_tm as in_tm, null as out_dt, null as out_tm, w.ward_name, r.ptrm_name, r.ptrm_bed_cur, r.ptrm_type, r.ptrm_bed_chg, i.inpt_hotline, i.inpt_short_stay, i.inpt_outpt_test, p.ptif_lt_kana_nm, p.ptif_ft_kana_nm, p.ptif_lt_kaj_nm, p.ptif_ft_kaj_nm, p.ptif_birth, p.ptif_sex, d.dr_nm, i.inpt_patho_from, i.inpt_patho_to, i.inpt_pre_div, i.inpt_intro_inst_cd, i.ptif_id, i.in_updated as updated, null as in_dttm, (select max(n.note) from bed_innote n where n.ptif_id = i.ptif_id and n.type = 'p' and n.mode = 'i' and n.in_dttm is null and n.back_flg = 'f') as note from inptmst i inner join ptifmst p on p.ptif_id = i.ptif_id inner join wdmst w on w.bldg_cd = i.bldg_cd and w.ward_cd = i.ward_cd inner join ptrmmst r on r.bldg_cd = i.bldg_cd and r.ward_cd = i.ward_cd and r.ptrm_room_no = i.ptrm_room_no left join drmst d on d.sect_id = i.inpt_sect_id and d.dr_id = i.dr_id where i.inpt_in_dt between '%s' and '%s'", $start_date, $end_date);

	// �ౡ�Ѥߴ��Ԥ���������
	$sql .= q(" union select bool 'f', i.inpt_in_dt, i.inpt_in_tm, i.inpt_out_dt, i.inpt_out_tm, w.ward_name, r.ptrm_name, r.ptrm_bed_cur, r.ptrm_type, r.ptrm_bed_chg, i.inpt_hotline, i.inpt_short_stay, i.inpt_outpt_test, p.ptif_lt_kana_nm, p.ptif_ft_kana_nm, p.ptif_lt_kaj_nm, p.ptif_ft_kaj_nm, p.ptif_birth, p.ptif_sex, d.dr_nm, i.inpt_patho_from, i.inpt_patho_to, i.inpt_pre_div, i.inpt_intro_inst_cd, i.ptif_id, in_updated as updated, (i.inpt_in_dt || i.inpt_in_tm), (select max(n.note) from bed_innote n where n.ptif_id = i.ptif_id and n.type = 'p' and n.mode = 'i' and n.in_dttm = cast((i.inpt_in_dt || i.inpt_in_tm) as varchar) and n.back_flg = 'f') as note from inpthist i inner join ptifmst p on p.ptif_id = i.ptif_id inner join wdmst w on w.bldg_cd = i.bldg_cd and w.ward_cd = i.ward_cd inner join ptrmmst r on r.bldg_cd = i.bldg_cd and r.ward_cd = i.ward_cd and r.ptrm_room_no = i.ptrm_room_no left join drmst d on d.sect_id = i.inpt_sect_id and d.dr_id = i.dr_id where i.inpt_in_dt between '%s' and '%s'", $start_date, $end_date);

	$cond = "order by in_dt, in_tm";
}
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	js_error_exit();
}
$patients = array();
$notes = array();
while ($row = pg_fetch_array($sel)) {

	// ���Ӥξ�硢���������¼������ɽ������
	if ($mode != "r") {
		$sql = "select w.ward_name, r.ptrm_name, r.ptrm_type, r.ptrm_bed_cur, r.ptrm_bed_chg from inptmove m inner join ptrmmst r on m.from_bldg_cd = r.bldg_cd and m.from_ward_cd = r.ward_cd and m.from_ptrm_room_no = r.ptrm_room_no inner join wdmst w on r.bldg_cd = w.bldg_cd and r.ward_cd = w.ward_cd";
		$cond = q("where m.ptif_id = '%s' and m.move_cfm_flg and not m.move_del_flg and ((m.move_dt = '%s' and m.move_tm >= '%s') or m.move_dt > '%s')", $row["ptif_id"], $row["in_dt"], $row["in_tm"], $row["in_dt"]);
		if ($row["out_dt"] != "") {
			$cond .= q(" and (m.move_dt < '%s' or (m.move_dt = '%s' and m.move_tm <= '%s'))", $row["out_dt"], $row["out_dt"], $row["out_tm"]);
		}
		$cond .= " order by m.move_dt, m.move_tm limit 1";

		$sel_mv = select_from_table($con, $sql, $cond, $fname);
		if ($sel_mv == 0) {
			pg_close($con);
			js_error_exit();
		}

		if (pg_num_rows($sel_mv) == 1) {
			$row["ward_name"] = pg_fetch_result($sel_mv, 0, "ward_name");
			$row["ptrm_name"] = pg_fetch_result($sel_mv, 0, "ptrm_name");
			$row["ptrm_type"] = pg_fetch_result($sel_mv, 0, "ptrm_type");
			$row["ptrm_bed_cur"] = pg_fetch_result($sel_mv, 0, "ptrm_bed_cur");
			$row["ptrm_bed_chg"] = pg_fetch_result($sel_mv, 0, "ptrm_bed_chg");
		}
	}

	$patients[$row["in_dt"]][] = array(
		"ward_name" => $row["ward_name"],
		"ptrm_name" => $row["ptrm_name"],
		"ptrm_type" => format_ptrm_type($row["ptrm_type"], $row["ptrm_bed_cur"]),
		"ptrm_bed_chg" => format_ptrm_bed_chg($row["ptrm_bed_chg"]),
		"hotline" => format_checked($row["inpt_hotline"]),
		"short_stay" => format_checked($row["inpt_short_stay"]),
		"outpt_test" => format_checked($row["inpt_outpt_test"]),
		"kana_name" => $row["ptif_lt_kana_nm"] . " " . $row["ptif_ft_kana_nm"],
		"kanji_name" => $row["ptif_lt_kaj_nm"] . " " . $row["ptif_ft_kaj_nm"],
		"birth" => format_date($row["ptif_birth"]),
		"age" => format_age($row["ptif_birth"]),
		"sex" => format_sex($row["ptif_sex"]),
		"dr_nm" => $row["dr_nm"],
		"operators" => get_operators($con, $mode, $row["ptif_id"], $row["back_flg"], $fname),
		"patho" =>  format_term($row["inpt_patho_from"], $row["inpt_patho_to"]),
		"intro_inst" => format_intro_inst($row["inpt_pre_div"], $inst_master, $row["inpt_intro_inst_cd"]),
		"in_tm" => format_time($row["in_tm"]),
		"updated" => $row["updated"],
		"ptif_id" => $row["ptif_id"],
		"back_flg" => $row["back_flg"],
		"in_dttm" => $row["in_dttm"]
	);

	$notes["p"][$row["ptif_id"]][$row["in_dttm"]][$row["back_flg"]] = $row["note"];
}

// ���ͥꥹ�Ȥ����������ʬ��
$sql = "select * from bed_innote";
$cond = q("where type = 'd' and mode = '%s' and date between '%s' and '%s'", $mode, $start_date, $end_date);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	js_error_exit();
}
while ($row = pg_fetch_array($sel)) {
	$notes["d"][$row["date"]] = $row["note"];
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function exportExcel() {
	document.excelform.submit();
}

function changeDate() {
	location.href = 'bed_menu_schedule_in.php?session=' + document.mainform.session.value + '&mode=' + document.mainform.mode.value + '&year=' + document.dateform.year.value + '&month=' + document.dateform.month.value + '&day=' + document.dateform.day.value + '&term=' + document.dateform.term.value;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? ehu($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="�¾�����"></a></td>
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? ehu($session); ?>"><b>�¾�����</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j16"><a href="entity_menu.php?session=<? ehu($session); ?>&entity=1"><b>�������̤�</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j16"><a href="building_list.php?session=<? ehu($session); ?>"><b>�������̤�</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#5279a5"><a href="bed_menu.php?session=<? ehu($session); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12" color="#ffffff"><b>�¾������ȥå�</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? ehu($session); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����Ȳ�</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? ehu($session); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���ౡ��Ͽ</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? ehu($session); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�٥åȥᥤ��</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? ehu($session); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�¾�����Ψ����</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? ehu($session); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�߱���������</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? ehu($session); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���ԥǡ������</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- ��˥塼 -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr height="22">
<td width="17%" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu.php?session=<? ehu($session); ?>">���Τ餻</a></font></td>
<td width="17%" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_status.php?session=<? ehu($session); ?>">�����������ɽ</a></font></td>
<td width="17%" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���ౡ��������</font></td>
<td width="17%" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_inpatient_search.php?session=<? ehu($session); ?>">�������Ը���</a></font></td>
<td width="17%" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_vacant_search.php?session=<? ehu($session); ?>">��������</a></font></td>
<td width="17%" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_occupation.php?session=<? ehu($session); ?>">�¾����Ѿ���</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- ���ౡ�������� -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22" bgcolor="#bdd1e7">
<td width="45"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_schedule.php?session=<? ehu($session); ?>">����</a></font></td>
<td width="90"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><b>����ͽ��ɽ</b></font></td>
<td width="90"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_schedule_out.php?session=<? ehu($session); ?>">�ౡͽ��ɽ</a></font></td>
<td width="150"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_schedule_report.php?session=<? ehu($session); ?>">�����̶����������</a></font></td>
<td width="110"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_schedule_move.php?session=<? ehu($session); ?>">�����ư����</a></font></td>
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><a href="bed_menu_schedule_change.php?session=<? ehu($session); ?>">ž���԰���</a></font></td>
</tr>
</table>
</td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="4" alt=""><br>

<form name="dateform">
<table border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
ǯ�����ʼ��ˡ�<select name="year">
<? show_select_years_span(1960, date("Y") + 1, $year, false); ?>
</select>/<select name="month">
<? show_select_months($month); ?>
</select>/<select name="day">
<? show_select_days($day); ?>
</select>
<select name="term">
<option value="1w"<? if ($term == "1w") {eh(" selected");} ?>>1����
<option value="2w"<? if ($term == "2w") {eh(" selected");} ?>>2����
<option value="3w"<? if ($term == "3w") {eh(" selected");} ?>>3����
<option value="1m"<? if ($term == "1m") {eh(" selected");} ?>>1����
</select>
<input type="button" value="�ѹ�" onclick="changeDate();" style="margin-right:30px;">
<span style="margin-right:10px;">
<? if ($mode == "i") { ?>
<a href="bed_menu_schedule_in.php?session=<? ehu($session); ?>&mode=r&year=<? ehu($year); ?>&month=<? ehu($month); ?>&day=<? ehu($day); ?>&term=<? ehu($term); ?>">
<? } else { ?>
<b>
<? } ?>
ͽ��
<? if ($mode == "i") { ?>
</a>
<? } else { ?>
</b>
<? } ?>
</span>
<span style="margin-right:30px;">
<? if ($mode == "r") { ?>
<a href="bed_menu_schedule_in.php?session=<? ehu($session); ?>&mode=i&year=<? ehu($year); ?>&month=<? ehu($month); ?>&day=<? ehu($day); ?>&term=<? ehu($term); ?>">
<? } else { ?>
<b>
<? } ?>
����
<? if ($mode == "r") { ?>
</a>
<? } else { ?>
</b>
<? } ?>
</span>
<input type="button" value="Excel����" onclick="exportExcel();">
</font></td>
</tr>
</table>
</form>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>

<form name="mainform" action="bed_menu_schedule_in_update.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block">
<tr height="22" bgcolor="#f6f9ff">
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">NEW</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�����ֹ�</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����������</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ۥå�</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">û��</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">������</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����̾�ʤ��ʡ�</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����̾</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��ǯ����</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ǯ��</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�缣��</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ô��</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ȯ��ǯ����</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�Ҳ𸵰��ŵ���</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��������</font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
</tr>
<?
$new_start = date("YmdHis", strtotime("-1 day"));

$date = $start_date;
while ($date <= $end_date) {
	$tmp_year = substr($date, 0, 4);
	$tmp_month = substr($date, 4, 2);
	$tmp_day = substr($date, 6, 2);
	$timestamp = mktime(0, 0, 0, $tmp_month, $tmp_day, $tmp_year);
	$rowspan = count($patients[$date]);
	if ($rowspan == 0) $rowspan = 1;
?>
<tr height="22">
<td nowrap align="center" rowspan="<? eh($rowspan); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh(format_date($date)); ?></font></td>
<td nowrap align="center" rowspan="<? eh($rowspan); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh(get_weekday($timestamp)); ?></font></td>
<?
	if (isset($patients[$date][0])) {
?>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? show_label_if_new($new_start, $patients[$date][0]["updated"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["ward_name"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["ptrm_name"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["ptrm_type"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["ptrm_bed_chg"]); ?></font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["hotline"]); ?></font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["short_stay"]); ?></font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["outpt_test"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["kana_name"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["kanji_name"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["birth"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["age"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["sex"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["dr_nm"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? show_operators($patients[$date][0]["operators"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["patho"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["intro_inst"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][0]["in_tm"]); ?></font></td>
<td nowrap>
<input type="button" value="����" onclick="window.open('bed_menu_schedule_in_detail.php?session=<? ehu($session); ?>&mode=<? ehu($mode); ?>&target=p|<? ehu($patients[$date][0]["ptif_id"]); ?>|<? ehu($patients[$date][0]["in_dttm"]); ?>|<? ehu($patients[$date][0]["back_flg"]); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');">
<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($notes["p"][$patients[$date][0]["ptif_id"]][$patients[$date][0]["in_dttm"]][$patients[$date][0]["back_flg"]]); ?></font>
</td>
<?
	} else {
		for ($j = 1; $j <= 18; $j++) {
?>
<td></td>
<?
		}
?>
<td nowrap>
<input type="button" value="����" onclick="window.open('bed_menu_schedule_in_detail.php?session=<? ehu($session); ?>&mode=<? ehu($mode); ?>&target=d|<? ehu($date); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');">
<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($notes["d"][$date]); ?></font>
</td>
<?
	}
?>
</tr>
<?
	for ($j = 1; $j < $rowspan; $j++) {
?>
<tr height="22">
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? show_label_if_new($new_start, $patients[$date][$j]["updated"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["ward_name"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["ptrm_name"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["ptrm_type"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["ptrm_bed_chg"]); ?></font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["hotline"]); ?></font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["short_stay"]); ?></font></td>
<td nowrap align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["outpt_test"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["kana_name"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["kanji_name"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["birth"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["age"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["sex"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["dr_nm"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? show_operators($patients[$date][$j]["operators"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["patho"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["intro_inst"]); ?></font></td>
<td nowrap><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($patients[$date][$j]["in_tm"]); ?></font></td>
<td nowrap>
<input type="button" value="����" onclick="window.open('bed_menu_schedule_in_detail.php?session=<? ehu($session); ?>&mode=<? ehu($mode); ?>&target=p|<? ehu($patients[$date][$j]["ptif_id"]); ?>|<? ehu($patients[$date][$j]["in_dttm"]); ?>|<? ehu($patients[$date][$j]["back_flg"]); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');">
<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><? eh($notes["p"][$patients[$date][$j]["ptif_id"]][$patients[$date][$j]["in_dttm"]][$patients[$date][$j]["back_flg"]]); ?></font>
</td>
</tr>
<?
	}
	$date = date("Ymd", strtotime("+1 day", $timestamp));
}
?>
</table>
<input type="hidden" name="session" value="<? eh($session); ?>">
<input type="hidden" name="mode" value="<? eh($mode); ?>">
<input type="hidden" name="year" value="<? eh($year); ?>">
<input type="hidden" name="month" value="<? eh($month); ?>">
<input type="hidden" name="day" value="<? eh($day); ?>">
<input type="hidden" name="term" value="<? eh($term); ?>">
</form>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td align="left" style="padding-left:10px;"><a href="javascript:void(0);" onclick="window.scrollBy(99999, 0);">��</a></td>
<td align="right" style="padding-right:10px;"><a href="javascript:void(0);" onclick="window.scrollBy(-99999, 0);">��</a></td>
<tr>
</table>

<form name="excelform" action="bed_menu_schedule_in_excel.php" method="get" target="excelframe">
<input type="hidden" name="session" value="<? eh($session); ?>">
<input type="hidden" name="mode" value="<? eh($mode); ?>">
<input type="hidden" name="year" value="<? eh($year); ?>">
<input type="hidden" name="month" value="<? eh($month); ?>">
<input type="hidden" name="day" value="<? eh($day); ?>">
<input type="hidden" name="term" value="<? eh($term); ?>">
</form>
<iframe name="excelframe" width="0" height="0" frameborder="0"></iframe>

</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function get_inst_master($con, $fname) {
	$sql = "select mst_cd, mst_name from institemmst";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		js_error_exit();
	}

	$master = array();
	while ($row = pg_fetch_array($sel)) {
		$master[$row["mst_cd"]] = $row["mst_name"];
	}
	return $master;
}

function get_weekday($timestamp) {
	switch (date("w", $timestamp)) {
	case "0":
		return "��";
	case "1":
		return "��";
	case "2":
		return "��";
	case "3":
		return "��";
	case "4":
		return "��";
	case "5":
		return "��";
	case "6":
		return "��";
	}
}

function show_label_if_new($new_start, $updated) {
	if ($updated >= $new_start) {
?>
<img src="img/icon/new.gif" alt="NEW" width="24" height="10" style="vertical-align:middle;">
<?
	}
}

function format_ptrm_type($ptrm_type, $ptrm_bed_cur) {
	if ($ptrm_type == "") return "";

	switch ($ptrm_type) {
	case "1":
		$ptrm_type = "����";
		break;
	case "2":
		$ptrm_type = "�ļ�";
		break;
	case "3":
		$ptrm_type = "������";
		break;
	case "4":
		$ptrm_type = "ICU";
		break;
	case "5":
		$ptrm_type = "HCU";
		break;
	case "6":
		$ptrm_type = "NICU";
		break;
	case "7":
		$ptrm_type = "GCU";
		break;
	}
	return "$ptrm_type($ptrm_bed_cur)";
}

function format_ptrm_bed_chg($ptrm_bed_chg) {
	return ($ptrm_bed_chg != 0) ? "{$ptrm_bed_chg}��" : "";
}

function format_checked($checked) {
	return ($checked == "t") ? "��" : "";
}

function format_date($date) {
	return preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $date);
}

function format_age($birth) {
	if ($birth == "") return "";

	$birth_yr = substr($birth, 0, 4);
	$birth_md = substr($birth, 4, 8);
	$age = date("Y") - $birth_yr;
	if ($birth_md > date("md")) {
		$age--;
	}
	return "{$age}��";
}

function format_sex($sex) {
	switch ($sex) {
	case "0":
		return "����";
	case "1":
		return "����";
	case "2":
		return "����";
	default:
		return "";
	}
}

function get_operators($con, $mode, $ptif_id, $back_flg, $fname) {
	if ($back_flg == "t") {
		return array();
	}

	if ($mode == "i") {
		$sql = "select e.emp_lt_nm from inptop";
		$cond = q("where i.ptif_id = '%s'", $ptif_id);
	} else {
		$sql = "select e.emp_lt_nm from inptopres";
		$cond = q("where i.ptif_id = '%s'", $ptif_id);
	}
	$sql .= " i inner join empmst e on e.emp_id = i.emp_id";
	$cond .= " and exists (select * from jobmst j where j.job_id = e.emp_job and j.bed_op_flg) order by i.order_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		js_error_exit();
	}

	$operators = array();
	while ($row = pg_fetch_array($sel)) {
		$operators[] = $row["emp_lt_nm"];
	}
	return $operators;
}

function show_operators($operators) {
	for ($i = 0, $j = count($operators); $i < $j; $i++) {
		if ($i > 0) {
?>
<br>
<?
		}
		eh($operators[$i]);
	}
}

function format_term($from, $to) {
	if ($from == "" && $to == "") return "";
	return trim(format_date($from) . "��" . format_date($to));
}

function format_intro_inst($pre_div, $inst_master, $intro_inst_cd) {
	if ($inst_master[$intro_inst_cd] != "") return $inst_master[$intro_inst_cd];

	switch ($pre_div) {
	case "1":
		return "����";
	case "2":
		return "���ŵ���¾";
	default:
		return "";
	}
}

function format_time($time) {
	return preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $time);
}
