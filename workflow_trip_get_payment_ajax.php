<?php
// 出張旅費
// 評価基準額Ajax
require_once("about_comedix.php");
require_once("workflow_trip_common.php");

$fname = $_SERVER["PHP_SELF"];

// セッションと権限のチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0") {
	echo "0";
	exit;
}

//パラメータ
$y1 = $_REQUEST["y1"];
$m1 = $_REQUEST["m1"];
$d1 = $_REQUEST["d1"];
$h1 = $_REQUEST["h1"];
$i1 = $_REQUEST["i1"];
$y2 = $_REQUEST["y2"];
$m2 = $_REQUEST["m2"];
$d2 = $_REQUEST["d2"];
$h2 = $_REQUEST["h2"];
$i2 = $_REQUEST["i2"];
$date1 = sprintf("%04d-%02d-%02d", $y1, $m1, $d1);
$date2 = sprintf("%04d-%02d-%02d", $y2, $m2, $d2);
$date1_t = mktime(0, 0, 0, $m1, $d1, $y1);
$date2_t = mktime(0, 0, 0, $m2, $d2, $y2);
$before = false;

// データベースに接続
$con = connect2db($fname);

// オプション設定の取得
$sql = "SELECT * FROM trip_option";
$sel = select_from_table($con,$sql,"",$fname);
if ($sel == 0) {
	echo "{'result':0}";
	exit;
}
$opt = pg_fetch_assoc($sel);

// 職員
$sql = <<<__SQL_END__
SELECT emp_st FROM empmst JOIN session USING (emp_id)
WHERE session_id='$session'
__SQL_END__;
$sel = select_from_table($con,$sql,"",$fname);
if ($sel == 0) {
	echo "{'result':0}";
	exit;
}
$st_id = pg_fetch_result($sel, 0, 0);
$before_st_id = $st_id;

// 役職
$sql = <<<__SQL_END__
SELECT s.*, TO_CHAR(DATE_TRUNC('month', histdate),'YYYY-MM-DD') as histdate2 FROM st_history s JOIN session USING (emp_id)
WHERE session_id='$session'
ORDER BY histdate DESC LIMIT 1;
__SQL_END__;
$sel = select_from_table($con,$sql,"",$fname);
if ($sel == 0) {
	echo "{'result':0}";
	exit;
}
$xdate = "";
if (pg_num_rows($sel) > 0) {
	$sthist = pg_fetch_assoc($sel);
	if ($date1 < $sthist["histdate2"] and $sthist["histdate2"] <= $date2) {
		$before_st_id = $sthist["st_id"];
		$before = true;
		$xdate = $sthist["histdate2"];
	}
	elseif ($date2 < $sthist["histdate2"]) {
		$st_id = $sthist["st_id"];
		$before_st_id = $st_id;
	}
}

// 支給基準改訂日
// 1が最新（1と2が同じ場合1が優先）
if ($opt["payment1_date"] >= $opt["payment2_date"]) {
	if ($date1 < $opt["payment1_date"] and $opt["payment1_date"] <= $date2) {
		$type = 1;
		$before_type = 2;
		$before = true;
		$xdate = $opt["payment1_date"];
	}
	elseif ($date2 < $opt["payment1_date"]) {
		$type = 2;
		$before_type = 2;
	}
	else {
		$type = 1;
		$before_type = 1;
	}
}
// 2が最新
else {
	if ($date1 < $opt["payment2_date"] and $opt["payment2_date"] <= $date2) {
		$type = 2;
		$before_type = 1;
		$before = true;
		$xdate = $opt["payment2_date"];
	}
	elseif ($date2 < $opt["payment2_date"]) {
		$type = 1;
		$before_type = 1;
	}
	else {
		$type = 2;
		$before_type = 2;
	}
}

$result = array();
$result['result'] = 1;
$result['kikan'] = kikan_calc($date1_t,$h1,$i1,$date2_t,$h2,$i2);
$result['payment'] = get_trip_payment_st_and_type($st_id, $type);
if ($before) {
	$before_payment = get_trip_payment_st_and_type($before_st_id, $before_type);
	if ($result['payment']['daytrip'] != $before_payment['daytrip'] OR
			$result['payment']['stay'] != $before_payment['stay'] OR
			$result['payment']['hotel'] != $before_payment['hotel'] OR
			$result['payment']['hotel_tokyo'] != $before_payment['hotel_tokyo']	) {
		$xdates = explode("-", $xdate);
		$xdate_t = mktime(0, 0, 0, $xdates[1], $xdates[2], $xdates[0]);
		$result['result'] = 2;
		$result['date'] = str_replace("-", "/", $xdate);
		$result['kikan2'] = kikan_calc($xdate_t,0,0,$date2_t,$h2,$i2);
		$result['kikan1'] = $result['kikan'] - $result['kikan2'];
		$result['before_payment'] = get_trip_payment_st_and_type($before_st_id, $before_type);
	}
}
pg_close($con);

echo cmx_json_encode($result);

function kikan_calc($date1,$h1,$i1,$date2,$h2,$i2) {
	$hour1 = sprintf("%02d%02d", $h1, $i1);
	$hour2 = sprintf("%02d%02d", $h2, $i2);

	// 日帰り
	if ($date1 == $date2) {
		// 5時間以内
		$hh1 = mktime($h1,$i1,0,1,1,1970);
		$hh2 = mktime($h2,$i2,0,1,1,1970);
		if (intval(($hh2 - $hh1) / 3600) < 5) {
			return 0;
		}
		// 出発が11:30以降
		if ($hour1 >= "1130") {
			return 0.5;
		}
		// 帰着が13:30以前
		if ($hour2 <= "1330") {
			return 0.5;
		}
		return 1;
	}

	// 宿泊
	else {
		$kikan = intval(($date2 - $date1) / 86400) + 1;
		// 12:00以降の出発
		if ($hour1 >= "1200") {
			$kikan -= 0.5;
		}
		// 12:00以前の帰着
		if ($hour2 <= "1200") {
			$kikan -= 0.5;
		}
		return $kikan;
	}
}
