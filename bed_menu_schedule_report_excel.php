<?
ob_start();
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_ward_div.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// 表示期間の設定
$dates = array();
for ($i = 0; $i <= 6; $i++) {
	$dates[] = date("Ymd", strtotime("+$i days"));
}
$start_date = $dates[0];
$end_date = $dates[6];

// データベースに接続
$con = connect2db($fname);

// 入院患者リストを作成
$sql = "select i.bldg_cd, i.ward_cd, i.ptrm_room_no, p.ptif_sex, i.inpt_short_stay from inptmst i inner join ptifmst p on p.ptif_id = i.ptif_id";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$inpatients = array();
while ($row = pg_fetch_array($sel)) {
	$inpatients[$row["bldg_cd"]][$row["ward_cd"]][$row["ptrm_room_no"]][] = array(
		"sex" => $row["ptif_sex"], "short_stay" => $row["inpt_short_stay"]
	);
}

// 期間内在院患者リストを作成
$inpatients_0000 = array();
$sel = get_patient_list($con, $start_date, $fname);
while ($row = pg_fetch_array($sel)) {
	$tmp_out_tm = ($row["pt_type"] == "in") ? "9999" : $row["inpt_out_tm"];

	// 当日0時直後の移転確定情報を取得
	$sql_move = "select m.from_bldg_cd, m.from_ward_cd, m.from_ptrm_room_no from inptmove m";
	$cond_move = "where m.ptif_id = '{$row["ptif_id"]}' and m.move_cfm_flg = 't' and m.move_dt = '$start_date' and m.move_tm < '$tmp_out_tm' order by m.move_tm limit 1";
	$sel_move = select_from_table($con, $sql_move, $cond_move, $fname);
	if ($sel_move == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if (pg_num_rows($sel_move) > 0) {
		$tmp_bldg_cd = pg_fetch_result($sel_move, 0, "from_bldg_cd");
		$tmp_ward_cd = pg_fetch_result($sel_move, 0, "from_ward_cd");
		$tmp_ptrm_room_no = pg_fetch_result($sel_move, 0, "from_ptrm_room_no");
	} else {
		$tmp_bldg_cd = $row["bldg_cd"];
		$tmp_ward_cd = $row["ward_cd"];
		$tmp_ptrm_room_no = $row["ptrm_room_no"];
	}
	$inpatients_0000[$tmp_bldg_cd][$tmp_ward_cd][$tmp_ptrm_room_no][] = array(
		"sex" => $row["ptif_sex"], "short_stay" => $row["inpt_short_stay"]
	);
}

// 病棟区分リストを作成
$ward_divs = get_ward_div($con, $fname, true);
foreach ($ward_divs as $ward_div => $ward_div_name) {
	$ward_divs[$ward_div] = array(
		"div_name" => $ward_div_name,
		"bed_max" => 0,
		"bed_cur" => 0,
		"wards" => array()
	);
}

// 病棟区分リストに病棟リストを追加
$sql = "select w.bldg_cd, w.ward_cd, w.ward_name, w.ward_type from wdmst w";
$cond = "where w.ward_del_flg = 'f' and exists (select * from bldgmst b where b.bldg_cd = w.bldg_cd and b.bldg_del_flg = 'f') order by w.bldg_cd, w.ward_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	if (!isset($ward_divs[$row["ward_type"]])) continue;
	$ward_divs[$row["ward_type"]]["wards"]["{$row["bldg_cd"]}-{$row["ward_cd"]}"] = array(
		"data_type" => "ward",
		"ward_name" => $row["ward_name"],
		"bed_max" => 0,
		"bed_cur" => 0,
		"ptrms" => array()
	);
}

// 病棟区分リストに病室リストを追加
$sql = "select r.bldg_cd, r.ward_cd, r.ptrm_room_no, w.ward_name, r.ptrm_name, w.ward_type, r.ward_type as ptrm_ward_type, r.ptrm_bed_max, r.ptrm_bed_cur, r.ptrm_type from ptrmmst r inner join wdmst w on w.bldg_cd = r.bldg_cd and w.ward_cd = r.ward_cd";
$cond = "where r.ptrm_del_flg = 'f' and w.ward_del_flg = 'f' and exists (select * from bldgmst b where b.bldg_cd = w.bldg_cd and b.bldg_del_flg = 'f') order by r.bldg_cd, r.ward_cd, r.ptrm_room_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$ward_type = $row["ward_type"];
	$ptrm_ward_type = $row["ptrm_ward_type"];
	$ptrm_bed_max = intval($row["ptrm_bed_max"]);
	$ptrm_bed_cur = intval($row["ptrm_bed_cur"]);

	// 病室に病棟区分が設定されていない場合
	if ($ptrm_ward_type == 0) {
		if (!isset($ward_divs[$ward_type])) continue;
		$key = "{$row["bldg_cd"]}-{$row["ward_cd"]}";
		$ward_divs[$ward_type]["wards"][$key]["ptrms"][$row["ptrm_room_no"]] = array(
			"bed_cur" => $ptrm_bed_cur,
			"ptrm_type" => $row["ptrm_type"]
		);
		$ward_divs[$ward_type]["wards"][$key]["bed_max"] += $ptrm_bed_max;
		$ward_divs[$ward_type]["wards"][$key]["bed_cur"] += $ptrm_bed_cur;
		$ward_divs[$ward_type]["bed_max"] += $ptrm_bed_max;
		$ward_divs[$ward_type]["bed_cur"] += $ptrm_bed_cur;

	// 病室に病棟区分が設定されている場合
	} else {
		if (!isset($ward_divs[$ptrm_ward_type])) continue;
		$ward_divs[$ptrm_ward_type]["wards"]["{$row["bldg_cd"]}-{$row["ward_cd"]}-{$row["ptrm_room_no"]}"] = array(
			"data_type" => "ptrm",
			"ward_name" => $row["ward_name"] . $row["ptrm_name"],
			"bed_max" => $ptrm_bed_max,
			"bed_cur" => $ptrm_bed_cur,
			"ptrm_type" => $row["ptrm_type"]
		);
		$ward_divs[$ptrm_ward_type]["bed_max"] += $ptrm_bed_max;
		$ward_divs[$ptrm_ward_type]["bed_cur"] += $ptrm_bed_cur;
	}
}

// 入院予定患者リストを作成
$sql = "select i.bldg_cd, i.ward_cd, i.ptrm_room_no, p.ptif_sex, i.inpt_short_stay, i.inpt_in_res_dt from inptres i inner join ptifmst p on p.ptif_id = i.ptif_id";
$cond = "where i.inpt_in_res_dt between '$start_date' and '$end_date'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$in_res_inpatients = array();
while ($row = pg_fetch_array($sel)) {
	$in_res_inpatients[$row["inpt_in_res_dt"]][$row["bldg_cd"]][$row["ward_cd"]][$row["ptrm_room_no"]][] = array(
		"sex" => $row["ptif_sex"],
		"short_stay" => $row["inpt_short_stay"]
	);
}

// 退院予定患者リストを作成
$sql = "select i.bldg_cd, i.ward_cd, i.ptrm_room_no, p.ptif_sex, i.inpt_short_stay, i.inpt_out_res_dt from inptmst i inner join ptifmst p on p.ptif_id = i.ptif_id";
$cond = "where i.inpt_out_res_dt between '$start_date' and '$end_date'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$out_res_inpatients = array();
while ($row = pg_fetch_array($sel)) {
	$out_res_inpatients[$row["inpt_out_res_dt"]][$row["bldg_cd"]][$row["ward_cd"]][$row["ptrm_room_no"]][] = array(
		"sex" => $row["ptif_sex"],
		"short_stay" => $row["inpt_short_stay"]
	);
}

// 転床予定リストを作成
$sql = "select move_dt, from_bldg_cd, from_ward_cd, from_ptrm_room_no, to_bldg_cd, to_ward_cd, to_ptrm_room_no from inptmove";
//$cond = "where move_dt between'$start_date' and '$end_date' and move_cfm_flg = 'f' and move_del_flg = 'f' and not (from_bldg_cd = to_bldg_cd and from_ward_cd = from_ptrm_room_no and to_ward_cd = to_ptrm_room_no)";
$cond = "where move_dt between'$start_date' and '$end_date' and move_cfm_flg = 'f' and move_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$movings = array();
while ($row = pg_fetch_array($sel)) {
	$movings[$row["move_dt"]][] = array(
		"from_bldg_cd" => $row["from_bldg_cd"],
		"from_ward_cd" => $row["from_ward_cd"],
		"from_ptrm_room_no" => $row["from_ptrm_room_no"],
		"to_bldg_cd" => $row["to_bldg_cd"],
		"to_ward_cd" => $row["to_ward_cd"],
		"to_ptrm_room_no" => $row["to_ptrm_room_no"]
	);
}

// 備考リストを作成
$sql = "select note_key, note_val from bed_repnote";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$notes = array();
while ($row = pg_fetch_array($sel)) {
	$notes[$row["note_key"]] = $row["note_val"];
}

unset($sel);
?>
</head>
<body>
<table border="1" cellspacing="0" cellpadding="1">
<tr bgcolor="#CCFFFF">
<td nowrap align="center" rowspan="5" colspan="2">病床・<br>病棟別</td>
<td nowrap align="center" rowspan="5"><? echo(format_vertical("病棟定床数")); ?></td>
<td nowrap align="center" rowspan="2" colspan="7">現在数</td>
<td nowrap align="center" rowspan="2" colspan="5">空床内訳（0時）</td>
<td nowrap align="center" rowspan="5"><? echo(format_vertical("空床・現在")); ?></td>
<td rowspan="5"></td>
<? foreach ($dates as $date) { ?>
<td nowrap align="center" colspan="5"><? echo(format_date($date)); ?></td>
<? } ?>
<td nowrap align="center" rowspan="2" colspan="5">合計</td>
<td nowrap align="center" rowspan="5"><? echo(format_vertical("一週間後空床")); ?></td>
<td nowrap align="center" rowspan="5">備考</tr>
<tr bgcolor="#CCFFFF">
<? foreach ($dates as $date) { ?>
<td nowrap align="center" colspan="5"><? echo(get_weekday($date)); ?></td>
<? } ?>
</tr>
<tr bgcolor="#CCFFFF">
<td nowrap align="center" colspan="2">男性</td>
<td nowrap align="center" colspan="2">女性</td>
<td nowrap align="center" colspan="2">合計</td>
<td nowrap align="center" rowspan="3">病床<br>利用率</td>
<td nowrap align="center" rowspan="3"><? echo(format_vertical("男性")); ?></td>
<td nowrap align="center" rowspan="3"><? echo(format_vertical("女性")); ?></td>
<td nowrap align="center" rowspan="3"><? echo(format_vertical("個室")); ?></td>
<td nowrap align="center" rowspan="3"><? echo(format_vertical("二人部屋")); ?></td>
<td nowrap align="center" rowspan="3"><? echo(format_vertical("合計")); ?></td>
<? for ($i = 1; $i <= 8; $i++) { ?>
<td nowrap align="center" colspan="5">入院（上段）<br>退院（下段）</td>
<? } ?>
</tr>
<tr bgcolor="#CCFFFF">
<? for ($i = 1; $i <= 3; $i++) { ?>
<td nowrap align="center" rowspan="2">長期</td>
<td nowrap align="center" rowspan="2">短期</td>
<? } ?>
<? for ($i = 1; $i <= 8; $i++) { ?>
<td nowrap align="center" colspan="2">男性</td>
<td nowrap align="center" colspan="2">女性</td>
<td nowrap align="center" rowspan="2"><? echo(format_vertical("転入出")); ?></td>
<? } ?>
</tr>
<tr bgcolor="#CCFFFF">
<? for ($i = 1; $i <= 16; $i++) { ?>
<td nowrap align="center">長</td>
<td nowrap align="center">短</td>
<? } ?>
</tr>
<?
$total_bed_max = 0;
$total_bed_cur = 0;
$total_male_long = 0;
$total_male_short = 0;
$total_female_long = 0;
$total_female_short = 0;
$total_vacancy_male = 0;
$total_vacancy_female = 0;
$total_vacancy_single = 0;
$total_vacancy_double = 0;
$total_vacancy_current = 0;
foreach ($dates as $date) {
	$total_in_res_male_long[$date] = 0;
	$total_in_res_male_short[$date] = 0;
	$total_in_res_female_long[$date] = 0;
	$total_in_res_female_short[$date] = 0;
	$total_in_res_moving[$date] = 0;
	$total_out_res_male_long[$date] = 0;
	$total_out_res_male_short[$date] = 0;
	$total_out_res_female_long[$date] = 0;
	$total_out_res_female_short[$date] = 0;
	$total_out_res_moving[$date] = 0;
}

foreach ($ward_divs as $ward_div => $ward_div_detail) {
	if ($ward_div_detail["bed_cur"] == 0) continue;

	$wards = $ward_div_detail["wards"];
	$ward_count = count($wards);

	// 病棟と病室が混在しているのでコード順に並べ替える
	uksort($wards, "sort_by_ward_key");

	$sum_male_long = 0;
	$sum_male_short = 0;
	$sum_female_long = 0;
	$sum_female_short = 0;
	$sum_vacancy_male = 0;
	$sum_vacancy_female = 0;
	$sum_vacancy_single = 0;
	$sum_vacancy_double = 0;
	$sum_vacancy_current = 0;
	foreach ($dates as $date) {
		$sum_in_res_male_long[$date] = 0;
		$sum_in_res_male_short[$date] = 0;
		$sum_in_res_female_long[$date] = 0;
		$sum_in_res_female_short[$date] = 0;
		$sum_in_res_moving[$date] = 0;
		$sum_out_res_male_long[$date] = 0;
		$sum_out_res_male_short[$date] = 0;
		$sum_out_res_female_long[$date] = 0;
		$sum_out_res_female_short[$date] = 0;
		$sum_out_res_moving[$date] = 0;
	}
?>
<tr>
<td nowrap align="center" rowspan="<? echo($ward_count * 2); ?>"><? echo(format_vertical($ward_div_detail["div_name"])); ?></td>
<?
	$first_ward = true;
	foreach ($wards as $ward_key => $ward) {
		if (!$first_ward) {
?>
<tr>
<?
		} else {
			$first_ward = false;
		}

		list($male_long, $male_short, $female_long, $female_short) = cale_inpatient($ward_key, $ward, $inpatients);
		$sum_male_long += $male_long;
		$sum_male_short += $male_short;
		$sum_female_long += $female_long;
		$sum_female_short += $female_short;

		list($vacancy_male, $vacancy_female, $vacancy_single, $vacancy_double) = calc_vacancy($ward_key, $ward, $inpatients_0000);
		$sum_vacancy_male += $vacancy_male;
		$sum_vacancy_female += $vacancy_female;
		$sum_vacancy_single += $vacancy_single;
		$sum_vacancy_double += $vacancy_double;

		$vacancy_current = calc_vacancy_current($ward_key, $ward, $inpatients);
		$sum_vacancy_current += $vacancy_current;

		foreach ($dates as $date) {
			list(
				$in_res_male_long[$date],
				$in_res_male_short[$date],
				$in_res_female_long[$date],
				$in_res_female_short[$date],
				$in_res_moving[$date],
				$out_res_male_long[$date],
				$out_res_male_short[$date],
				$out_res_female_long[$date],
				$out_res_female_short[$date],
				$out_res_moving[$date]
			) = calc_reservation($ward_key, $ward, $in_res_inpatients[$date], $out_res_inpatients[$date], $movings[$date]);

			$sum_in_res_male_long[$date] += $in_res_male_long[$date];
			$sum_in_res_male_short[$date] += $in_res_male_short[$date];
			$sum_in_res_female_long[$date] += $in_res_female_long[$date];
			$sum_in_res_female_short[$date] += $in_res_female_short[$date];
			$sum_in_res_moving[$date] += $in_res_moving[$date];
			$sum_out_res_male_long[$date] += $out_res_male_long[$date];
			$sum_out_res_male_short[$date] += $out_res_male_short[$date];
			$sum_out_res_female_long[$date] += $out_res_female_long[$date];
			$sum_out_res_female_short[$date] += $out_res_female_short[$date];
			$sum_out_res_moving[$date] += $out_res_moving[$date];
		}

		$vacancy = $vacancy_male + $vacancy_female + $vacancy_single + $vacancy_double;
		$week_in_res_male_long = array_sum($in_res_male_long);
		$week_in_res_male_short = array_sum($in_res_male_short);
		$week_in_res_female_long = array_sum($in_res_female_long);
		$week_in_res_female_short = array_sum($in_res_female_short);
		$week_in_res_moving = array_sum($in_res_moving);
		$week_out_res_male_long = array_sum($out_res_male_long);
		$week_out_res_male_short = array_sum($out_res_male_short);
		$week_out_res_female_long = array_sum($out_res_female_long);
		$week_out_res_female_short = array_sum($out_res_female_short);
		$week_out_res_moving = array_sum($out_res_moving);
?>
<td nowrap rowspan="2"><? echo($ward["ward_name"]); ?></td>
<td nowrap align="right" rowspan="2"><? echo(format_bed_count($ward["bed_max"], $ward["bed_cur"])); ?></td>
<td nowrap align="right" rowspan="2"><? write_number_or_empty($male_long); ?></td>
<td nowrap align="right" rowspan="2"><? write_number_or_empty($male_short); ?></td>
<td nowrap align="right" rowspan="2"><? write_number_or_empty($female_long); ?></td>
<td nowrap align="right" rowspan="2"><? write_number_or_empty($female_short); ?></td>
<td nowrap align="right" rowspan="2"><? echo($male_long + $female_long); ?></td>
<td nowrap align="right" rowspan="2"><? echo($male_short + $female_short); ?></td>
<td nowrap align="right" rowspan="2"><? echo(format_bed_rate($ward["bed_cur"], ($male_long + $female_long + $male_short + $female_short))); ?></td>
<td nowrap align="right" rowspan="2"><? write_number_or_empty($vacancy_male); ?></td>
<td nowrap align="right" rowspan="2"><? write_number_or_empty($vacancy_female); ?></td>
<td nowrap align="right" rowspan="2"><? write_number_or_empty($vacancy_single); ?></td>
<td nowrap align="right" rowspan="2"><? write_number_or_empty($vacancy_double); ?></td>
<td nowrap align="right" rowspan="2"><? echo($vacancy); ?></td>
<td nowrap align="right" rowspan="2"><? echo($vacancy_current); ?></td>
<td nowrap align="center">入</td>
<?
		foreach ($dates as $date) {
?>
<td nowrap align="right"><? write_number_or_empty($in_res_male_long[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($in_res_male_short[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($in_res_female_long[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($in_res_female_short[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($in_res_moving[$date]); ?></td>
<?
		}
?>
<td nowrap align="right"><? write_number_or_empty($week_in_res_male_long); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_in_res_male_short); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_in_res_female_long); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_in_res_female_short); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_in_res_moving); ?></td>
<td nowrap align="right" rowspan="2"><? echo($vacancy_current - ($week_in_res_male_long + $week_in_res_male_short + $week_in_res_female_long + $week_in_res_female_short + $week_in_res_moving) + ($week_out_res_male_long + $week_out_res_male_short + $week_out_res_female_long + $week_out_res_female_short + $week_out_res_moving)); ?></td>
<td><? echo($notes["ward1_$ward_key"]); ?></td>
</tr>
<tr>
<td nowrap align="center">退</td>
<?
		foreach ($dates as $date) {
?>
<td nowrap align="right"><? write_number_or_empty($out_res_male_long[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($out_res_male_short[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($out_res_female_long[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($out_res_female_short[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($out_res_moving[$date]); ?></td>
<?
		}
?>
<td nowrap align="right"><? write_number_or_empty($week_out_res_male_long); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_out_res_male_short); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_out_res_female_long); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_out_res_female_short); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_out_res_moving); ?></td>
<td><? echo($notes["ward2_$ward_key"]); ?></td>
</tr>
<?
	}

	$sum_vacancy = $sum_vacancy_male + $sum_vacancy_female + $sum_vacancy_single + $sum_vacancy_double;
	$week_sum_in_res_male_long = array_sum($sum_in_res_male_long);
	$week_sum_in_res_male_short = array_sum($sum_in_res_male_short);
	$week_sum_in_res_female_long = array_sum($sum_in_res_female_long);
	$week_sum_in_res_female_short = array_sum($sum_in_res_female_short);
	$week_sum_in_res_moving = array_sum($sum_in_res_moving);
	$week_sum_out_res_male_long = array_sum($sum_out_res_male_long);
	$week_sum_out_res_male_short = array_sum($sum_out_res_male_short);
	$week_sum_out_res_female_long = array_sum($sum_out_res_female_long);
	$week_sum_out_res_female_short = array_sum($sum_out_res_female_short);
	$week_sum_out_res_moving = array_sum($sum_out_res_moving);
?>
<tr>
<td nowrap align="center" colspan="2" rowspan="2">小計</td>
<td nowrap align="right" rowspan="2"><? echo(format_bed_count($ward_div_detail["bed_max"], $ward_div_detail["bed_cur"])); ?></td>
<td nowrap align="right" rowspan="2"><? echo($sum_male_long); ?></td>
<td nowrap align="right" rowspan="2"><? echo($sum_male_short); ?></td>
<td nowrap align="right" rowspan="2"><? echo($sum_female_long); ?></td>
<td nowrap align="right" rowspan="2"><? echo($sum_female_short); ?></td>
<td nowrap align="right" rowspan="2"><? echo($sum_male_long + $sum_female_long); ?></td>
<td nowrap align="right" rowspan="2"><? echo($sum_male_short + $sum_female_short); ?></td>
<td nowrap align="right" rowspan="2"><? echo(format_bed_rate($ward_div_detail["bed_cur"], ($sum_male_long + $sum_female_long + $sum_male_short + $sum_female_short))); ?></td>
<td nowrap align="right" rowspan="2"><? echo($sum_vacancy_male); ?></td>
<td nowrap align="right" rowspan="2"><? echo($sum_vacancy_female); ?></td>
<td nowrap align="right" rowspan="2"><? echo($sum_vacancy_single); ?></td>
<td nowrap align="right" rowspan="2"><? echo($sum_vacancy_double); ?></td>
<td nowrap align="right" rowspan="2"><? echo($sum_vacancy); ?></td>
<td nowrap align="right" rowspan="2"><? echo($sum_vacancy_current); ?></td>
<td nowrap align="center">入</td>
<?
		foreach ($dates as $date) {
?>
<td nowrap align="right"><? write_number_or_empty($sum_in_res_male_long[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($sum_in_res_male_short[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($sum_in_res_female_long[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($sum_in_res_female_short[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($sum_in_res_moving[$date]); ?></td>
<?
		}
?>
<td nowrap align="right"><? write_number_or_empty($week_sum_in_res_male_long); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_sum_in_res_male_short); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_sum_in_res_female_long); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_sum_in_res_female_short); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_sum_in_res_moving); ?></td>
<td nowrap align="right" rowspan="2"><? echo($sum_vacancy_current - ($week_sum_in_res_male_long + $week_sum_in_res_male_short + $week_sum_in_res_female_long + $week_sum_in_res_female_short + $week_sum_in_res_moving) + ($week_sum_out_res_male_long + $week_sum_out_res_male_short + $week_sum_out_res_female_long + $week_sum_out_res_female_short + $week_sum_out_res_moving)); ?></td>
<td><? echo($notes["sum1_$ward_div"]); ?></td>
</tr>
<tr>
<td nowrap align="center">退</td>
<?
		foreach ($dates as $date) {
?>
<td nowrap align="right"><? write_number_or_empty($sum_out_res_male_long[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($sum_out_res_male_short[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($sum_out_res_female_long[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($sum_out_res_female_short[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($sum_out_res_moving[$date]); ?></td>
<?
		}
?>
<td nowrap align="right"><? write_number_or_empty($week_sum_out_res_male_long); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_sum_out_res_male_short); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_sum_out_res_female_long); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_sum_out_res_female_short); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_sum_out_res_moving); ?></td>
<td><? echo($notes["sum2_$ward_div"]); ?></td>
</tr>
<?
	$total_bed_max += $ward_div_detail["bed_max"];
	$total_bed_cur += $ward_div_detail["bed_cur"];
	$total_male_long += $sum_male_long;
	$total_male_short += $sum_male_short;
	$total_female_long += $sum_female_long;
	$total_female_short += $sum_female_short;
	$total_vacancy_male += $sum_vacancy_male;
	$total_vacancy_female += $sum_vacancy_female;
	$total_vacancy_single += $sum_vacancy_single;
	$total_vacancy_double += $sum_vacancy_double;
	$total_vacancy_current += $sum_vacancy_current;
	foreach ($dates as $date) {
		$total_in_res_male_long[$date] += $sum_in_res_male_long[$date];
		$total_in_res_male_short[$date] += $sum_in_res_male_short[$date];
		$total_in_res_female_long[$date] += $sum_in_res_female_long[$date];
		$total_in_res_female_short[$date] += $sum_in_res_female_short[$date];
		$total_in_res_moving[$date] += $sum_in_res_moving[$date];
		$total_out_res_male_long[$date] += $sum_out_res_male_long[$date];
		$total_out_res_male_short[$date] += $sum_out_res_male_short[$date];
		$total_out_res_female_long[$date] += $sum_out_res_female_long[$date];
		$total_out_res_female_short[$date] += $sum_out_res_female_short[$date];
		$total_out_res_moving[$date] += $sum_out_res_moving[$date];
	}
}

$total_vacancy = $total_vacancy_male + $total_vacancy_female + $total_vacancy_single + $total_vacancy_double;
$week_total_in_res_male_long = array_sum($total_in_res_male_long);
$week_total_in_res_male_short = array_sum($total_in_res_male_short);
$week_total_in_res_female_long = array_sum($total_in_res_female_long);
$week_total_in_res_female_short = array_sum($total_in_res_female_short);
$week_total_in_res_moving = array_sum($total_in_res_moving);
$week_total_out_res_male_long = array_sum($total_out_res_male_long);
$week_total_out_res_male_short = array_sum($total_out_res_male_short);
$week_total_out_res_female_long = array_sum($total_out_res_female_long);
$week_total_out_res_female_short = array_sum($total_out_res_female_short);
$week_total_out_res_moving = array_sum($total_out_res_moving);
?>
<tr>
<td nowrap align="center" colspan="2" rowspan="2">合計</td>
<td nowrap align="right" rowspan="2"><? echo(format_bed_count($total_bed_max, $total_bed_cur)); ?></td>
<td nowrap align="right" rowspan="2"><? echo($total_male_long); ?></td>
<td nowrap align="right" rowspan="2"><? echo($total_male_short); ?></td>
<td nowrap align="right" rowspan="2"><? echo($total_female_long); ?></td>
<td nowrap align="right" rowspan="2"><? echo($total_female_short); ?></td>
<td nowrap align="right" rowspan="2"><? echo($total_male_long + $total_female_long); ?></td>
<td nowrap align="right" rowspan="2"><? echo($total_male_short + $total_female_short); ?></td>
<td nowrap align="right" rowspan="2"><? echo(format_bed_rate($total_bed_cur, ($total_male_long + $total_female_long + $total_male_short + $total_female_short))); ?></td>
<td nowrap align="right" rowspan="2"><? echo($total_vacancy_male); ?></td>
<td nowrap align="right" rowspan="2"><? echo($total_vacancy_female); ?></td>
<td nowrap align="right" rowspan="2"><? echo($total_vacancy_single); ?></td>
<td nowrap align="right" rowspan="2"><? echo($total_vacancy_double); ?></td>
<td nowrap align="right" rowspan="2"><? echo($total_vacancy); ?></td>
<td nowrap align="right" rowspan="2"><? echo($total_vacancy_current); ?></td>
<td nowrap align="center">入計</td>
<?
foreach ($dates as $date) {
?>
<td nowrap align="right"><? write_number_or_empty($total_in_res_male_long[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($total_in_res_male_short[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($total_in_res_female_long[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($total_in_res_female_short[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($total_in_res_moving[$date]); ?></td>
<?
}
?>
<td nowrap align="right"><? write_number_or_empty($week_total_in_res_male_long); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_total_in_res_male_short); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_total_in_res_female_long); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_total_in_res_female_short); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_total_in_res_moving); ?></td>
<td nowrap align="right" rowspan="2"><? echo($total_vacancy_current - ($week_total_in_res_male_long + $week_total_in_res_male_short + $week_total_in_res_female_long + $week_total_in_res_female_short + $week_total_in_res_moving) + ($week_total_out_res_male_long + $week_total_out_res_male_short + $week_total_out_res_female_long + $week_total_out_res_female_short + $week_total_out_res_moving)); ?></td>
<td><? echo($notes["total1"]); ?></td>
</tr>
<tr>
<td nowrap align="center">退計</td>
<?
foreach ($dates as $date) {
?>
<td nowrap align="right"><? write_number_or_empty($total_out_res_male_long[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($total_out_res_male_short[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($total_out_res_female_long[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($total_out_res_female_short[$date]); ?></td>
<td nowrap align="right"><? write_number_or_empty($total_out_res_moving[$date]); ?></td>
<?
}
?>
<td nowrap align="right"><? write_number_or_empty($week_total_out_res_male_long); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_total_out_res_male_short); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_total_out_res_female_long); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_total_out_res_female_short); ?></td>
<td nowrap align="right"><? write_number_or_empty($week_total_out_res_moving); ?></td>
<td><? echo($notes["total2"]); ?></td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
$excel_data = mb_convert_encoding(ob_get_clean(), "Shift_JIS", 'EUC-JP');
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=vacancy_report.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");
echo($excel_data);

function get_patient_list($con, $start_date, $fname) {
	$sql = " select 'in' as pt_type, i.ptif_id, i.inpt_out_tm, i.bldg_cd, i.ward_cd, i.ptrm_room_no, p.ptif_sex, i.inpt_short_stay from inptmst i inner join wdmst w on w.bldg_cd = i.bldg_cd and w.ward_cd = i.ward_cd inner join ptrmmst r on r.bldg_cd = i.bldg_cd and r.ward_cd = i.ward_cd and r.ptrm_room_no = i.ptrm_room_no inner join ptifmst p on p.ptif_id = i.ptif_id where i.inpt_in_dt is not null and i.inpt_in_dt <= '$start_date' ";
	$sql .= " union select 'out' as pt_type, i.ptif_id, i.inpt_out_tm, i.bldg_cd, i.ward_cd, i.ptrm_room_no, p.ptif_sex, i.inpt_short_stay from inpthist i inner join wdmst w on w.bldg_cd = i.bldg_cd and w.ward_cd = i.ward_cd inner join ptrmmst r on r.bldg_cd = i.bldg_cd and r.ward_cd = i.ward_cd and r.ptrm_room_no = i.ptrm_room_no inner join ptifmst p on p.ptif_id = i.ptif_id where i.inpt_in_dt <= '$start_date' and i.inpt_out_dt >= '$start_date'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

function get_weekday($date) {
	$year = substr($date, 0, 4);
	$month = substr($date, 4, 2);
	$day = substr($date, 6, 2);

	switch (date("w", mktime(0, 0, 0, $month, $day, $year))) {
	case "0":
		return "日";
	case "1":
		return "月";
	case "2":
		return "火";
	case "3":
		return "水";
	case "4":
		return "木";
	case "5":
		return "金";
	case "6":
		return "土";
	}
}

function format_date($date) {
	return preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $date);
}

function format_vertical($str) {
	$buf = array();
	for ($i = 0, $j = mb_strlen($str); $i < $j; $i++) {
		$buf[] = mb_substr($str, $i, 1);
	}
	return join("<br>", $buf);
}

function format_bed_count($max, $cur) {
	return ($max == $cur) ? $cur : "[{$max}]<br>$cur";
}

function sort_by_ward_key($key1, $key2) {
	list($bldg_cd1, $ward_cd1, $ptrm_room_no1) = split("-", $key1);
	list($bldg_cd2, $ward_cd2, $ptrm_room_no2) = split("-", $key2);
	if (intval($bldg_cd1) < intval($bldg_cd2)) return -1;
	if (intval($bldg_cd1) > intval($bldg_cd2)) return 1;
	if (intval($ward_cd1) < intval($ward_cd2)) return -1;
	if (intval($ward_cd1) > intval($ward_cd2)) return 1;
	if (intval($ptrm_room_no1) < intval($ptrm_room_no2)) return -1;
	if (intval($ptrm_room_no1) > intval($ptrm_room_no2)) return 1;
	return 0;
}


function cale_inpatient($ward_key, $ward, $inpatients) {
	$male_long = 0;
	$male_short = 0;
	$female_long = 0;
	$female_short = 0;

	list($bldg_cd, $ward_cd, $ptrm_room_no_list) = resolve_ptrm_list($ward_key, $ward);

	foreach ($ptrm_room_no_list as $ptrm_room_no) {
		foreach ($inpatients[$bldg_cd][$ward_cd][$ptrm_room_no] as $inpatient) {

			// 男性の場合
			if ($inpatient["sex"] == "1") {
				if ($inpatient["short_stay"] == "t") {
					$male_short++;
				} else {
					$male_long++;
				}

			// 女性の場合
			} else if ($inpatient["sex"] == "2") {
				if ($inpatient["short_stay"] == "t") {
					$female_short++;
				} else {
					$female_long++;
				}
			}
		}
	}

	return array($male_long, $male_short, $female_long, $female_short);
}

function calc_vacancy($ward_key, $ward, $inpatients) {
	$vacancy_male = 0;
	$vacancy_female = 0;
	$vacancy_single = 0;
	$vacancy_double = 0;

	switch ($ward["data_type"]) {
	case "ward":
		list($bldg_cd, $ward_cd) = split("-", $ward_key);
		$ptrms = $ward["ptrms"];
		break;
	case "ptrm":
		list($bldg_cd, $ward_cd, $ptrm_room_no) = split("-", $ward_key);
		$ptrms[$ptrm_room_no] = $ward;
		break;
	}

	foreach ($ptrms as $ptrm_room_no => $ptrm) {
		$inpatiens_in_ptrm = $inpatients[$bldg_cd][$ward_cd][$ptrm_room_no];

		// 個室の場合
		if ($ptrm["ptrm_type"] == "2") {
			$vacancy_single += (1 - count($inpatiens_in_ptrm));

		// 二人部屋の場合
		} else if ($ptrm["bed_cur"] == 2) {
			$vacancy_double += (2 - count($inpatiens_in_ptrm));

		// 三人部屋以上の場合
		} else {

			// 男性部屋かどうか判定
			$male_count = 0;
			$female_count = 0;
			foreach ($inpatiens_in_ptrm as $inpatient) {
				switch ($inpatient["sex"]) {
				case "1":
					$male_count++;
					break;
				case "2":
					$female_count++;
					break;
				}
			}
			$is_male_room = ($male_count >= $female_count);

			if ($is_male_room) {
				$vacancy_male += ($ptrm["bed_cur"] - count($inpatiens_in_ptrm));
			} else {
				$vacancy_female += ($ptrm["bed_cur"] - count($inpatiens_in_ptrm));
			}
		}
	}

	return array($vacancy_male, $vacancy_female, $vacancy_single, $vacancy_double);
}

function calc_vacancy_current($ward_key, $ward, $inpatients) {
	$vacancy = 0;

	switch ($ward["data_type"]) {
	case "ward":
		list($bldg_cd, $ward_cd) = split("-", $ward_key);
		$ptrms = $ward["ptrms"];
		break;
	case "ptrm":
		list($bldg_cd, $ward_cd, $ptrm_room_no) = split("-", $ward_key);
		$ptrms[$ptrm_room_no] = $ward;
		break;
	}

	foreach ($ptrms as $ptrm_room_no => $ptrm) {
		$inpatiens_in_ptrm = $inpatients[$bldg_cd][$ward_cd][$ptrm_room_no];

		// 個室の場合
		if ($ptrm["ptrm_type"] == "2") {
			$vacancy += (1 - count($inpatiens_in_ptrm));

		// 二人部屋の場合
		} else if ($ptrm["bed_cur"] == 2) {
			$vacancy += (2 - count($inpatiens_in_ptrm));

		// 三人部屋以上の場合
		} else {
			$vacancy += ($ptrm["bed_cur"] - count($inpatiens_in_ptrm));
		}
	}

	return $vacancy;
}

function write_number_or_empty($num) {
	if ($num == 0) return;
	echo($num);
}

function format_bed_rate($bed_cur, $inpatient_count) {
	if ($bed_cur == 0 || $inpatient_count == 0) return "0.0%";
	return sprintf("%.1f", round(($inpatient_count / $bed_cur * 100), 1)) . "%";
}

function resolve_ptrm_list($ward_key, $ward, $permit_room_undec = false) {
	switch ($ward["data_type"]) {
	case "ward":
		list($bldg_cd, $ward_cd) = split("-", $ward_key);
		if ($permit_room_undec) {
			$ptrm_room_no_list = array_merge(array(0), array_keys($ward["ptrms"]));
		} else {
			$ptrm_room_no_list = array_keys($ward["ptrms"]);
		}
		break;
	case "ptrm":
		list($bldg_cd, $ward_cd, $ptrm_room_no) = split("-", $ward_key);
		$ptrm_room_no_list = array($ptrm_room_no);
		break;
	}
	return array($bldg_cd, $ward_cd, $ptrm_room_no_list);
}

function calc_reservation($ward_key, $ward, $in_res_inpatients, $out_res_inpatients, $movings) {
	$in_res_male_long = 0;
	$in_res_male_short = 0;
	$in_res_female_long = 0;
	$in_res_female_short = 0;
	$in_res_moving = 0;
	$out_res_male_long = 0;
	$out_res_male_short = 0;
	$out_res_female_long = 0;
	$out_res_female_short = 0;
	$out_res_moving = 0;

	list($bldg_cd, $ward_cd, $ptrm_room_no_list) = resolve_ptrm_list($ward_key, $ward, true);

	foreach ($ptrm_room_no_list as $ptrm_room_no) {
		foreach ($in_res_inpatients[$bldg_cd][$ward_cd][$ptrm_room_no] as $patient) {

			// 男性の場合
			if ($patient["sex"] == "1") {
				if ($patient["short_stay"] == "t") {
					$in_res_male_short++;
				} else {
					$in_res_male_long++;
				}

			// 女性の場合
			} else if ($patient["sex"] == "2") {
				if ($patient["short_stay"] == "t") {
					$in_res_female_short++;
				} else {
					$in_res_female_long++;
				}
			}
		}

		foreach ($out_res_inpatients[$bldg_cd][$ward_cd][$ptrm_room_no] as $patient) {

			// 男性の場合
			if ($patient["sex"] == "1") {
				if ($patient["short_stay"] == "t") {
					$out_res_male_short++;
				} else {
					$out_res_male_long++;
				}

			// 女性の場合
			} else if ($patient["sex"] == "2") {
				if ($patient["short_stay"] == "t") {
					$out_res_female_short++;
				} else {
					$out_res_female_long++;
				}
			}
		}

		foreach ($movings as $moving) {
			if ($bldg_cd == $moving["to_bldg_cd"] && $ward_cd == $moving["to_ward_cd"] && $ptrm_room_no == $moving["to_ptrm_room_no"]) {
				$in_res_moving++;
			}

			if ($bldg_cd == $moving["from_bldg_cd"] && $ward_cd == $moving["from_ward_cd"] && $ptrm_room_no == $moving["from_ptrm_room_no"]) {
				$out_res_moving++;
			}
		}
	}

	return array(
		$in_res_male_long,
		$in_res_male_short,
		$in_res_female_long,
		$in_res_female_short,
		$in_res_moving,
		$out_res_male_long,
		$out_res_male_short,
		$out_res_female_long,
		$out_res_female_short,
		$out_res_moving
	);
}
?>
