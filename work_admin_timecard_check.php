<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?php 
switch ($menuname)
{
	case "check_timecard":
		$check_name = "打刻チェック";
		$check_work_flg = 1;
		break;
	case "check_overtime":
		$check_name = "残業時間チェック";
		$check_work_flg = 2;
		break;
	case "check_unapproved":
		$check_name = "残業未承認チェック";
		$check_work_flg = 3;
		break;
}
$title = "CoMedix 出勤表 | " . $check_name;
?>
<title><?php echo($title); ?></title>

<?php
require_once("work_admin_timecard_check_class.php");
require_once("timecard_common_class.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_timecard_common.ini");
require_once("show_select_values.ini");
require_once("atdbk_close_class.php");

$fname = $PHP_SELF;
// データベースに接続
$con = connect2db($fname);

if ($check_work_flg == 1 && $error_list){ 
	$err_msg = ($error_list == "err_all")? "" : $error_list ;
}

if (is_array($emp_id_list)){
	$emp_id_list = implode(",", $emp_id_list);
}

// セッションのチェック
$session = qualify_session($session,$fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

if ($shift_admin_flg == "t"){
	$checkauth = check_authority($session, 69, $fname);
	if ($checkauth == "0") {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
	$wherefrom_flg = "10";
}else{
	// 勤務管理権限チェック
	$checkauth = check_authority($session, 42, $fname);
	if ($checkauth == "0" && $mmode != "usr") {
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showLoginPage(window);</script>");
		exit;
	}
	$wherefrom_flg = "";
}

$sql = "select closing, closing_parttime, closing_month_flg from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
//$closing = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing") : "6";
//$closing_parttime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_parttime") : $closing;
//$closing_month_flg = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_month_flg") : "1";
$atdbk_close_class = new atdbk_close_class($con, $fname);
$top_emp_id = substr($emp_id_list, 0, 12);
$closing = $atdbk_close_class->get_empcond_closing($top_emp_id);
$closing_month_flg = $atdbk_close_class->closing_month_flg;


$year = substr($yyyymm, 0, 4);
$month = intval(substr($yyyymm, 4, 2));

$default_year = $year;
$default_month = $month;

if($mypage == "1"){
	$tmp_start = getdate(strtotime($start_date));
	$tmp_end = getdate(strtotime($end_date));
	
	$start_year = $tmp_start["year"];
	$start_month = $tmp_start["mon"];
	$start_day = $tmp_start["mday"];
	$end_year = $tmp_end["year"];
	$end_month = $tmp_end["mon"];
	$end_day = $tmp_end["mday"];
}else{
	// 開始日・締め日を算出
	$calced = false;
	switch ($closing) {
		case "1":  // 1日
			$closing_day = 1;
			break;
		case "2":  // 5日
			$closing_day = 5;
			break;
		case "3":  // 10日
			$closing_day = 10;
			break;
		case "4":  // 15日
			$closing_day = 15;
			break;
		case "5":  // 20日
			$closing_day = 20;
			break;
		case "6":  // 末日
			$start_year = $year;
			$start_month = $month;
			$start_day = 1;
			$end_year = $start_year;
			$end_month = $start_month;
			$end_day = days_in_month($end_year, $end_month);
			$calced = true;
			break;
	}
	
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;
	
		//当月〜翌月
		if ($closing_month_flg != "2") {
			$start_year = $year;
			$start_month = $month;
	
			$end_year = $year;
			$end_month = $month + 1;
			if ($end_month == 13) {
				$end_year++;
				$end_month = 1;
			}
		}
		//前月〜当月
		else {
			$start_year = $year;
			$start_month = $month - 1;
			if ($start_month == 0) {
				$start_year--;
				$start_month = 12;
			}
	
			$end_year = $year;
			$end_month = $month;
		}
	
	}
	
	$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);
}	

if ($select_start_yr != "") {
	$start_year = $select_start_yr;
	$start_month = $select_start_mon;
	$start_day = $select_start_day;
	$end_year = $select_end_yr;
	$end_month = $select_end_mon;
	$end_day = $select_end_day;
	
	$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);
}else{
	//当月〜翌月
	if ($closing_month_flg != "2") {
		$default_y = $start_year;
		$default_m = $start_month;
	}
	//前月〜当月
	else {
		$default_y = $end_year;
		$default_m = $end_month;
	}
}

// 20131210
if($shift_flg == "1"){
	$shift_groups = array();
	$arr_emp_id = explode("," , $emp_id_list);
	$tmp_emp_id = implode("','", $arr_emp_id);
	
	$sql = "select s.emp_id, g.group_id, g.group_name from duty_shift_staff s ";
	$sql .= "inner join duty_shift_group g on s.group_id = g.group_id";
	$cond = "where s.emp_id in ('$tmp_emp_id')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo ("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo ("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)){
		$tmp_id = $row["emp_id"];
		$tmp_group_id = $row["group_id"];
		$tmp_group_name = $row["group_name"];
		$shift_groups[$tmp_id] = array("id"=>$tmp_group_id, "name"=>$tmp_group_name);
	}
}

// 対象年月度を変数にセット
$c_yyyymm = $yyyymm;

// 組織階層情報取得
$arr_org_level = array ();
$level_cnt = 0;
$sql = "select class_nm, atrb_nm, dept_nm, room_nm, class_cnt from classname";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo ("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo ("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$arr_org_level[0] = pg_result($sel, 0, "class_nm");
$arr_org_level[1] = pg_result($sel, 0, "atrb_nm");
$arr_org_level[2] = pg_result($sel, 0, "dept_nm");
$arr_org_level[3] = pg_result($sel, 0, "room_nm");

//チェック項目の情報を取得する
$cls_timecard = new check_timecard();
$arr_sortkey = array($sortkey1, $sortkey2, $sortkey3,$sortkey4,$sortkey5);
$list_info = $cls_timecard->get_show_list($con, $fname, $emp_id_list, $start_date, $end_date, $menuname, $arr_sortkey);

?>
<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--
//月度変更時、期間も変更
function chg_ym(ym) {

	var default_y = <?=$default_y?>;
	var default_m = <?=$default_m?>;

	yyyy=ym.substring(0, 4);
	mm=eval(ym.substring(4, 6));

	//初期年月との月の差
	diff = (yyyy*12+mm) - (default_y*12+default_m);

	//開始年月を計算し設定
	sy = <?=$start_year?>;
	sm = <?=intval($start_month);?>;
	sd = 1;

	dates = new Date();

	dates.setYear(sy - 1900);
	dates.setMonth(sm - 1);
	dates.setDate(sd);

	dates.setMonth(dates.getMonth()+diff);

	yyyy1 = dates.getYear() + 1900;
	yyyy = (yyyy1 < 1900) ? yyyy1 + 1900 : yyyy1;
	mm = dates.getMonth()+1;
	mm = (mm < 10) ? '0'+mm : mm;

	document.mainform.select_start_yr.value = yyyy;
	document.mainform.select_start_mon.value = mm;

	//終了年月を計算し設定
	ey = <?=$end_year?>;
	em = <?=intval($end_month);?>;
	ed = 1;

	datee = new Date();

	datee.setYear(ey - 1900);
	datee.setMonth(em - 1);
	datee.setDate(ed);

	datee.setMonth(datee.getMonth()+diff);

	yyyy1 = datee.getYear()+1900;
	yyyy = (yyyy1 < 1900) ? yyyy1 + 1900 : yyyy1;
	mm = datee.getMonth()+1;
	mm = (mm < 10) ? '0'+mm : mm;
	document.mainform.select_end_yr.value = yyyy;
	document.mainform.select_end_mon.value = mm;
	document.mainform.yyyymm.value = ym;
	set_last_day();

	document.mainform.default_y.value = yyyy;
	document.mainform.default_m.value = mm;
}

//終了日月末設定
function set_last_day() {
	day = document.mainform.select_end_day.value;
	day = parseInt(day,10);
	if (day >= 28) {
		year = document.mainform.select_end_yr.value;
		month = document.mainform.select_end_mon.value;
		last_day = days_in_month(year, month);
		if (last_day != day) {
			day = last_day;
			dd = (day < 10) ? '0'+day : day;
			document.mainform.select_end_day.value = dd;
		}
	}
	
}
//月末取得
function days_in_month(year, month) {
	//月別日数配列
	lastDay = new Array(31,28,31,30,31,30,31,31,30,31,30,31,29);
	//閏年
	wk_year = parseInt(year,10);
	wk_mon = parseInt(month,10);
	if ((wk_mon == 2) &&
		((wk_year % 4) == 0 && ((wk_year % 100) != 0 || (wk_year % 400)))) {
		wk_mon = 13;
	}

	days = lastDay[wk_mon-1];
	return days;
}

function openTimecardAll(emp_id, check_work_flg) {

	var wx = 1200;
	var wy = window.screen.availHeight;
	var dx = screen.width / 2 - (wx / 2);
	var dy = screen.top;
	var base = screen.height / 2 - (wy / 2);
	window.open('', 'timecard', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	
	document.workshift.emp_id.value 		= emp_id;
	document.workshift.yyyymm.value 		= <?php echo($c_yyyymm); ?>;
	document.workshift.check_work_flg.value = check_work_flg;
	
	document.workshift.action 				= 'work_admin_timecard_shift.php';
	document.workshift.target 				= 'timecard';
	document.workshift.submit();
	
}

function btn_execute(){
       document.mainform.mypage.value = '0';
       document.mainform.action = 'work_admin_timecard_check.php';
       document.mainform.target = '';
       document.mainform.submit();
}

function output_pdf(){
	window.open("",
			"pdf", "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=1000,height=721");

		document.mainform.action = "work_admin_timecard_check_pdf.php";
		document.mainform.target = "pdf";
		document.mainform.submit();
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.condition { margin: auto;} 
div.bg-icon-line { background: url(css/img/bg-icon-line.gif) top repeat-x;}
table.main_tab_menu td { padding-left:5px;}
table.employlist  tr:hover { background-color: #D2E9FF; }
input.exec { padding:5px; width:80px; }

table.employlist { margin: auto; border-collapse:separate; border-spacing:1px; background-color:#9bc8ec; empty-cells:show;}
table.employlist tr { background-color:#fff }
table.employlist th { padding:5px; font-weight:normal; text-align:left; background:url(css/img/bg-b-gura.gif) top repeat-x; }
table.employlist td { padding:3px; }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="3" leftmargin="5" marginheight="5" marginwidth="5">
	
	<div class="bg-icon-line" style="height:40px">
	<table class="main_tab_menu" style="height:100%;width:100%" >
		<tr>
			<td><font size="5" face="ＭＳ Ｐゴシック, Osaka" color="#005AB5" ><b><?php echo($check_name); ?></b></font></td>
			<?php if(!$err_msg){ ?>
				<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
			<?php } ?>
		</tr>
	</table>
	</div>

	<form name="mainform" method="post">

	<table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor = "#F4F4F4" class="condition">
		<tr>
			<td align="left">
				<table border="0" cellspacing="0" cellpadding="2">
				<tr>
				<td align="left" colspan="5">
			<?php
			echo("<select name=\"_yyyymm2\" onChange=\"chg_ym(this.value);\">\n");
			$buf_year = date("Y");
			for ($y = $buf_year; $y >= date("Y")-2; $y--) {
				for ($m = 12; $m >= 1; $m--) {
					$mm = sprintf("%02d", $m);
					echo("<option value=\"$y$mm\"");
					if ("$y$mm" == $c_yyyymm) {
						echo(" selected");
					}
					echo(">{$y}年{$m}月度\n");
				}
			}
			echo("</select>");
				?>
						<select name="select_start_yr">
						<? show_select_years(10, $start_year, false, true); ?>
						</select>/<select name="select_start_mon">
						<? show_select_months($start_month, false); ?>
						</select>/<select name="select_start_day">
						<? show_select_days($start_day, false); ?>
						</select> 〜 <select name="select_end_yr" onchange="set_last_day();">
						<? show_select_years(10, $end_year, false, true); ?>
						</select>/<select name="select_end_mon" onchange="set_last_day();">
						<? show_select_months($end_month, false); ?>
						</select>/<select name="select_end_day">
						<? show_select_days($end_day, false); ?>
						</select>&nbsp;
					</td>
				</tr>
				<tr height="32">
			<?php
			//部署項目名
			$arr_sortitem = array("", "職員ID", "氏名", "$arr_org_level[0]", "$arr_org_level[1]", "$arr_org_level[2]", "職種");
			
			for ($key_idx=1; $key_idx<=5; $key_idx++) {
				echo("<td>");
				echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">ソートキー{$key_idx}</font><br>");
			
				$varname = "sortkey".$key_idx;
				echo("<select name=\"$varname\" id=\"$varname\">");
				$sortkey_val = $$varname;
				for ($i=0; $i<count($arr_sortitem); $i++) {
					//選択
					if ($sortkey_val == $i) {
						$selected = " selected";
					} else {
						$selected = "";
					}
					echo("<option value=\"$i\" $selected>");
			
					echo($arr_sortitem[$i]);
					echo("</option>");
				}
				echo("</select>");
				echo("</td>");
			}
			
			?>
				</tr>
			<?php if($check_work_flg == "1"){ ?>
				<tr>
					<td>
						<select name="error_list" id="error_list">
						<option value="err_all" <?php echo (empty($err_msg)) ? "selected" : ""; ?>>全て</option>
						<option value="err_time" <?php echo ($err_msg == "err_time") ? "selected" : ""; ?>>打刻漏れ</option>
						<option value="late" <?php echo ($err_msg == "late") ? "selected" : ""; ?>>遅刻</option>
						<option value="early" <?php echo ($err_msg == "early") ? "selected" : ""; ?>>早退</option>
						<option value="err_holiday" <?php echo ($err_msg == "err_holiday") ? "selected" : ""; ?>>休日打刻</option>
						<option value="err_pattern" <?php echo ($err_msg == "err_pattern") ? "selected" : ""; ?>>勤務実績未登録</option>
						</select>
					</td>
				</tr>
			<?php } ?>
				</table>
			</td>
		</tr>
	</table>
	<div>
		<input type="button" value="実行" onclick="btn_execute();" class ="exec">
		<input type="button" value="PDF印刷" onclick="output_pdf();" class ="exec">
	</div>
<hr class="cline">
<table width="100%" border="2" class="employlist">
	<tr>
		<th width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></th>
		<th width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員氏名</font></th>
		<th width="34%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></th>
		<?php if ($shift_flg == "1"){ ?>
			<th width="16%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフトグループ</font></th>
		<?php } ?>
		<th width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></th>
		<th width="20%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">エラー内容</font></th>
	</tr>
<?php
	//日付並び順にする
	foreach ($list_info as $key=>$item) {
		ksort($list_info[$key]["date_err"]); 
	}
	//データを加工する
	if (!empty($err_msg)) {
		foreach ($list_info as $key=>$item) {
			foreach ($item["date_err"] as $err_key=>$err){
				foreach ($err as $msg_key=>$msg){
					if ($msg_key != $err_msg){
						unset($list_info[$key]["date_err"][$err_key][$msg_key]);
					}
				}
				
				if (empty($list_info[$key]["date_err"][$err_key])) {
					unset($list_info[$key]["date_err"][$err_key]);
				}
			}
		}
	}		

	//検索対象一覧を表示する
	$arr_empid = array();
	foreach ($list_info as $key=>$item) {
		$emp_id 		= $item["emp_personal_id"];
		$emp_name 		= $item["emp_name"];
		$emp_department = $item["emp_department"];
		$shift_name = ($shift_groups[$key]) ? $shift_groups[$key]["name"] : "";
		
		$rows = count($item["date_err"]);
		if ($rows > 0){
			$arr_empid[] = $key;
			foreach ($item["date_err"] as $err_key=>$err){
				$error_msg = implode("、", $err); //エラー内容
				$tmp_date = date("m月d日",strtotime($err_key));
				
				echo("<tr height=\"22\">\n");
				echo("<td width=\"10%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$emp_id</font></td>\n");
				echo("<td width=\"10%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" style=\"border-bottom:solid 1px #0080FF;cursor:pointer;color:#0080FF\" onClick=\"openTimecardAll('$key', $check_work_flg);\">$emp_name</font></td>\n"); //前ゼロありの場合職員IDが変わって検索できない場合の対応 20140311
				echo("<td width=\"34%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$emp_department</font></td>\n");
				if ($shift_flg == "1"){ 
					echo("<td width=\"16%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$shift_name</font></td>\n");
				}
				echo("<td width=\"10%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_date</font></td>\n");
				echo("<td width=\"20%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$error_msg</font></td>\n");
				echo("</tr>\n");
			}
		}
	}
	$empid_list = implode(",", $arr_empid);
?>
</table>
	<input type="hidden" name="session" value="<? echo($session); ?>">
	<input type="hidden" name="default_start_date" value="<?php echo($start_date); ?>">
	<input type="hidden" name="default_end_date" value="<?php echo($end_date); ?>">
	<input type="hidden" name="menuname" value="<?php echo($menuname); ?>">
	<input type="hidden" name="emp_id_list" value="<?php echo($emp_id_list); ?>">
	<input type="hidden" name="yyyymm" value="<?php echo($c_yyyymm); ?>">
	
	<input type="hidden" name="start_date" value="<?php echo($start_date); ?>">
	<input type="hidden" name="end_date" value="<?php echo($end_date); ?>">
	
	<input type="hidden" name="default_y" value="<?php echo($default_year); ?>">
	<input type="hidden" name="default_m" value="<?php echo($default_month); ?>">
		
	<input type="hidden" name="closing" value="<?php echo($closing); ?>">
	<input type="hidden" name="closing_month_flg" value="<?php echo($closing_month_flg); ?>">
	<input type="hidden" name="shift_flg" value="<?php echo($shift_flg); ?>">
	<input type="hidden" name="err_msg" value="<?php echo($err_msg); ?>">
	<input type="hidden" name="title" value="<?php  echo($check_name); ?>">
	<input type="hidden" name="shift_admin_flg" value="<?php echo($shift_admin_flg); ?>">
	<input type="hidden" name="shift_from_flg" value="<?php echo($shift_from_flg); ?>">
	<input type="hidden" name="mypage" value="<?php echo($mypage); ?>">
	</form>
	<iframe name="showlist" width="0" height="0" frameborder="0"></iframe>
	
	<form name="workshift" target="timecard" method="post">
		<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="emp_id"  value="">
		<input type="hidden" name="yyyymm"  value="<? echo($c_yyyymm); ?>">
		<input type="hidden" name="wherefrom" value="<?php echo($wherefrom_flg); ?>">
		<input type="hidden" name="empid_list" value="<? echo($empid_list); ?>">
		<input type="hidden" name="check_work_flg" value="">
		<input type="hidden" name="shift_admin_flg" value="<?php echo($shift_admin_flg); ?>">
		<input type="hidden" name="shift_from_flg" value="<?php echo($shift_from_flg); ?>">
		<input type="hidden" name="mypage" value="<?php echo($mypage); ?>">
	</form>
		
</body>
<? pg_close($con); ?>
</html>

