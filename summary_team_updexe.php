<?
// 更新処理
// ・存在しない場合は空INSERTでデータを生成
// ・・team_id生成処理
// ・以後はUPDATE
// 削除処理
// ・存在したらDELETE
// ・存在しなかったら（あり得ない）システムエラー


//ソート条件定数
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 57, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// チーム計画がすでに存在しているかチェック

if( $team_id == 0 && $updatetype != 99 ){ // 新規のときは、一旦キー値だけでINSERTして後方のUPDATE処理へ
	
	// 新規登録しようとしている患者ID,プロブレムID,日付のチーム計画がすでに存在しているかチェック
	// 他ユーザーが登録した可能性があるため
	$master_count = exsit_check($con,$fname,$pt_id,$problem_id,$select_date);
	if ( $master_count == 0 ){
		$sql="SELECT MAX(team_id) AS team_id FROM sum_med_std_team "; // 新しいteam_idを生成する処理
		$sel = select_from_table($con,$sql,"",$fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			require_once("summary_common.ini");
			summary_common_show_error_page_and_die();
		}
		$ready_team_id = pg_fetch_result($sel, 0, "team_id");
		if( pg_num_rows($sel) == 0 ){
			$new_team_id = 1;
		}else{
			$new_team_id = $ready_team_id + 1;
		}
		$sql  = "INSERT INTO sum_med_std_team (";
		$sql .= "team_id ,";		// プライマリキー
		$sql .= "problem_id ,";		// プロブレムID
		$sql .= "ptif_id ,";		// 患者ID
		$sql .= "create_date ,";	// 立案日
		$sql .= "emp_name ,";		// 起票職員
		$sql .= "emp_job ";		// 起票職員の職種
		$sql .= ") VALUES (";
		$content = array($new_team_id,$problem_id,$pt_id,$select_date,$creater_name, $creater_job);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			summary_common_show_error_page_and_die();
		}
		$team_id = $new_team_id;
		$mode = "normal";
	}else{
		$mode = "already";
		$updatetype= -1 ; // マスター更新をパスさせる
	}
}

// マスターが存在しない場合は更新できない
$master_count = exsit_check($con,$fname,$pt_id,$problem_id,$select_date);
if ( $team_id != 0  && $master_count == 0 ){
	$mode = "noexsit";
	$updatetype= -1 ; // マスター更新をパスさせる
}


// マスター更新または削除
$wkdate="00-00-00 00:00";
$wkdate = date("Y-m-d H:i",strtotime($wkdate));
switch ($updatetype){
	case 4: // 左側
		if( $creater != "" ){
			$name = getempname($con,$fname,$creater);
			$set = array("def_value", "s_detail","e_detail","risk_detail","outcome","left_jobname","left_emp_name","left_lock_flag",);
			$setvalue = array($leftdefval , $leftS , $leftE , $leftRisk , $leftO , $name[0]["job_name"],$name[0]["name"],"f");
		}else{
			$name = getempname($con,$fname,$emp_id);
			$set = array("def_value", "s_detail","e_detail","risk_detail","outcome","left_lock_flag");
			$setvalue = array($leftdefval , $leftS , $leftE , $leftRisk ,$leftO ,"f");
		}
		update_master($con,$fname,$team_id,$set,$setvalue);
		update_left_recorder($con,$fname,$team_id, $name[0]["job_name"],$name[0]["name"],$upd_date);
		break;
	case 41:// 左側・観察項目
		update_observation($con,$fname,$team_id,$leftObservation);
		break;
	case 0: // 中央 共用
		update_center_master(0,$con,$fname,$team_id,$emp_id,$OP , $TP , $EP , $emp_id,"f",$wkdate,"",$centerupd_date);
		break;
	case 1: // 中央 看護
		update_center_master(1,$con,$fname,$team_id,$emp_id,$OP , $TP , $EP , $emp_id,"f",$wkdate,"",$centerupd_date);
		break;
	case 2: // 中央 介護
		update_center_master(2,$con,$fname,$team_id,$emp_id,$OP , $TP , $EP , $emp_id,"f",$wkdate,"",$centerupd_date);
		break;
	case 3: // 中央 リハビリ
		update_center_master(3,$con,$fname,$team_id,$emp_id,$OP , $TP , $EP , $emp_id,"f",$wkdate,"",$centerupd_date);
		break;
	case 5:	// 右側 看護
	case 6:	// 右側 介護
	case 7:	// 右側 リハビリ
		$team_type = $updatetype - 4;
		if ( $updatemode == "insert" ){
			insert_asses($con,$fname,$team_id,$team_type,$emp_id,$rightdate,$rightvalu);
		}elseif( $updatemode == "delete" ){
			delete_asses($con,$fname,$team_id,$team_type,$emp_id,$rightdate);
		}
		break;
	case 10:// ヘッダ部、職名、起票者氏名(いずれもemp_idと関連させないフリーテキスト入力)
		$set = array("emp_name", "emp_job","left_lock_flag");
		$setvalue = array($creater_name, $creater_job,"f");
		update_master($con,$fname,$team_id,$set,$setvalue);
		break;
	case 99:// チーム計画を削除する。PostgreSQLでは結合DELETEができないため個別に消す
		team_delete_all($con,$fname,"sum_med_std_team",$team_id);
		team_delete_all($con,$fname,"sum_med_std_obse",$team_id);
		team_delete_all($con,$fname,"sum_med_std_asses",$team_id);
		team_delete_all($con,$fname,"sum_med_rec_left",$team_id);
		team_delete_all($con,$fname,"sum_med_priactice0",$team_id);
		team_delete_all($con,$fname,"sum_med_priactice1",$team_id);
		team_delete_all($con,$fname,"sum_med_priactice2",$team_id);
		team_delete_all($con,$fname,"sum_med_priactice3",$team_id);
		team_delete_all($con,$fname,"sum_med_recorder0",$team_id);
		team_delete_all($con,$fname,"sum_med_recorder1",$team_id);
		team_delete_all($con,$fname,"sum_med_recorder2",$team_id);
		team_delete_all($con,$fname,"sum_med_recorder3",$team_id);
		break;
}


pg_query($con, "commit");
pg_close($con);
$back_show = "<script type=\"text/javascript\">location.href = 'summary_team_stdplan.php?session=$session&emp_id=".$emp_id."&pt_id=".$pt_id."&problem_id=".$problem_id."&team_id=".$team_id."&sts=0&select_date=".$select_date."&mode=".$mode."'</script>";
echo($back_show);


// チーム計画・マスター更新
function update_master($con,$fname,$team_id,$set, $setvalue){
	$sql = "UPDATE sum_med_std_team SET";
	$cond = "WHERE team_id = $team_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		summary_common_show_error_page_and_die();
	}
}

// 左側の更新リスト
function update_left_recorder($con,$fname,$team_id,$emp_job_name,$emp_name,$upd_date){
	$sql  = "INSERT INTO sum_med_rec_left (team_id,left_jobname,left_emp_name,update) VALUES ( ";
	$cont = array( $team_id , $emp_job_name,$emp_name , $upd_date) ;
	$ins = insert_into_table($con, $sql, $cont, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		summary_common_show_error_page_and_die();
	}
}

// 左側の観察項目
function update_observation($con,$fname,$team_id,$leftObservation){
	$sql="DELETE FROM sum_med_std_obse ";
	$cond = "WHERE team_id=$team_id ";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		summary_common_show_error_page_and_die();
	}
	$obs_list=explode(",",$leftObservation);
	$sql  = "INSERT INTO sum_med_std_obse (team_id,item_cd) VALUES ( ";
	for($i=0; $i<count($obs_list); $i++){
		$cont = array( $team_id ,$obs_list[$i]) ;
		$ins = insert_into_table($con, $sql, $cont, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			summary_common_show_error_page_and_die();
		}
	}
}

// 中側の更新内容
function update_center_master($type,$con,$fname,$team_id,$emp_id,$OP , $TP , $EP , $emp_id,$bool_sts,$wkdate,$lock_emp_id,$centerupd_date){
	$tbl_name=array("sum_med_priactice0","sum_med_priactice1","sum_med_priactice2","sum_med_priactice3");
	$insset=array();
	$updset=array();
	$insset[0]="team_id,kyoyo_op,kyoyo_tp,kyoyo_ep,kyoyo_jobname,kyoyo_emp_name,c0_lock_flag,c0_lock_time,c0_lock_emp_id";
	$updset[0]=array("team_id","kyoyo_op","kyoyo_tp","kyoyo_ep","c0_lock_flag","c0_lock_time","c0_lock_emp_id");
	
	$insset[1]="team_id,kango_op,kango_tp,kango_ep,kango_jobname,kango_emp_name,c1_lock_flag,c1_lock_time,c1_lock_emp_id";
	$updset[1]=array("team_id","kango_op","kango_tp","kango_ep","c1_lock_flag","c1_lock_time","c1_lock_emp_id");
	
	$insset[2]="team_id,kaigo_op,kaigo_tp,kaigo_ep,kaigo_jobname,kaigo_emp_name,c2_lock_flag,c2_lock_time,c2_lock_emp_id";
	$updset[2]=array("team_id","kaigo_op","kaigo_tp","kaigo_ep","c2_lock_flag","c2_lock_time","c2_lock_emp_id");
	
	$insset[3]="team_id,rihabiri_op,rihabiri_tp,rihabiri_ep,rihabiri_jobname,rihabiri_emp_name,c3_lock_flag,c3_lock_time,c3_lock_emp_id";
	$updset[3]=array("team_id","rihabiri_op","rihabiri_tp","rihabiri_ep","c3_lock_flag","c3_lock_time","c3_lock_emp_id");
	
	// emp_idから職名と氏名を得る
	$name = getempname($con,$fname,$emp_id);
	$emp_job_name = $name[0]["job_name"];
	$emp_name = $name[0]["name"];
	
	// テーブルがあるかどうか見る
	$sql="SELECT team_id FROM ".$tbl_name[$type];
	$sql .= " WHERE team_id=$team_id ";
	$sel = select_from_table($con,$sql,"",$fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	if( pg_num_rows($sel) == 0 ){
		// 無い。新規
		$set .= $col_name[$type];
		$sql  = "INSERT INTO ".$tbl_name[$type]." (".$insset[$type].") VALUES ( ";
		$cont =  array($team_id,$OP , $TP , $EP,$emp_job_name,$emp_name,$bool_sts,$wkdate,$lock_emp_id);
		$ins = insert_into_table($con, $sql, $cont, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			summary_common_show_error_page_and_die();
		}
	}else{
		// ある。更新。「記載者」を更新しない
		$sql  = "UPDATE ".$tbl_name[$type]." SET ";
		$cond = "WHERE team_id = $team_id";
		$setvalue =  array($team_id,$OP , $TP , $EP,$bool_sts,$wkdate,$lock_emp_id);
		$upd = update_set_table($con, $sql, $updset[$type], $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			summary_common_show_error_page_and_die();
		}
		
		// 更新者リスト
		$tbl_name=array("sum_med_recorder0","sum_med_recorder1","sum_med_recorder2","sum_med_recorder3");
		$col_name=array("update_c0","update_c1","update_c2","update_c3");
		$sql  = "INSERT INTO ".$tbl_name[$type]." (team_id,jobname,emp_name,".$col_name[$type].") VALUES ( ";
		$cont = array( $team_id ,$emp_job_name,$emp_name , $centerupd_date) ;
		$ins = insert_into_table($con, $sql, $cont, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			summary_common_show_error_page_and_die();
		}
	}
}

// 右側評価日/解決・継続/サインの更新
// 削除
function delete_asses($con,$fname,$team_id,$team_type,$emp_id,$rightdate){
	$sql="DELETE FROM sum_med_std_asses ";
	$cond = "WHERE team_id=$team_id AND team_type=$team_type AND emp_id='$emp_id' AND update='$rightdate' ";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		summary_common_show_error_page_and_die();
	}
}
// 追加
function insert_asses($con,$fname,$team_id,$team_type,$emp_id,$rightdate,$rightvalu){
	// emp_idから職名と氏名を得る
	$name = getempname($con,$fname,$emp_id);
	$emp_job_name = $name[0]["job_name"];
	$emp_name = $name[0]["name"];
	$sql  = "INSERT INTO sum_med_std_asses (team_id,team_type,emp_id,update,comment,jobname,emp_name) VALUES ( ";
	$cont = array( $team_id ,$team_type, $emp_id , $rightdate , $rightvalu,$emp_job_name,$emp_name) ;
	$ins = insert_into_table($con, $sql, $cont, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		summary_common_show_error_page_and_die();
	}
}

// 新規登録しようとしている患者ID,プロブレムID,日付のチーム計画がすでに存在しているかチェック
// 他ユーザーが登録した可能性があるため
function exsit_check($con,$fname,$pt_id,$problem_id,$select_date){
	$last_date = date("Y-m-d H:i",strtotime($select_date));
	
	$sql  = "SELECT COUNT(*) AS count ";
	$sql .= " FROM sum_med_std_team  ";
	$sql .= " WHERE ptif_id='$pt_id' AND problem_id=$problem_id AND create_date='$last_date' ";
	$cond = "";
	$sel = select_from_table($con,$sql,$cond,$fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	
	return pg_fetch_result($sel,0,"count");
}

// emp_idから、職名と氏名を得る
function getempname($con,$fname,$emp_id){
	$sql  = "SELECT J.job_nm AS job_name,E.emp_lt_nm||E.emp_ft_nm AS name ";
	$sql .= " FROM empmst AS E ";
	$sql .= " INNER JOIN jobmst AS J ON J.job_id=E.emp_job ";
	$sql .= " WHERE E.emp_id='$emp_id' ";
	$cond = "";
	$sel = select_from_table($con,$sql,$cond,$fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	$ret = pg_fetch_all($sel);
	return $ret;
}

// 消す
function team_delete_all($con,$fname,$table_name,$team_id){
	$sql="DELETE FROM ".$table_name." ";
	$cond = "WHERE team_id=$team_id ";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		summary_common_show_error_page_and_die();
	}
}
?>
