<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 46, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// コメントなしで締切り日時後の場合、エラー
if ($comment_flg == "f" && $deadline != "" && $deadline <= date('YmdHi')) {
    echo("<script type=\"text/javascript\">alert('締切り日時を過ぎたため登録できません。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}
// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// 確認時
if ($submit_button == "確認") {

    $sql = "select ref_time from newsref";
    $cond = "where news_id = $news_id and emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    if (pg_num_rows($sel) == 0) {
        // ここに来ることは実際にはありえないはず
        $sql = "insert into newsref (news_id, emp_id, ref_time, read_flg) values (";
        $content = array($news_id, $emp_id, date("YmdHi"), "t");
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    } else if (pg_fetch_result($sel, 0, "ref_time") == "") {
        $sql = "update newsref set";
        $set = array("ref_time");
        $setvalue = array(date("YmdHi"));
        $cond = "where news_id = $news_id and emp_id = '$emp_id'";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

// 締切り前の場合にアンケートの回答登録
if ($deadline != "" && $deadline > date('YmdHi')) {
    // 回答設定を取得
    $select_num = array();
    $multi_flg = array();
    $type = array();
    if ($qa_cnt > 0) {
        $sql = "select select_num, multi_flg, type from newsenquet_q";
        $cond = "where news_id = $news_id order by question_no";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $i = 1;
        while ($row = pg_fetch_array($sel)) {
            $select_num[$i] = $row["select_num"];
            $multi_flg[$i] = $row["multi_flg"];
             $type[$i] = $row["type"];
            $i++;
        }
    }

    // 回答を削除
    $sql = "delete from newsanswer";
    $cond = "where news_id = $news_id and emp_id = '$emp_id'";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    // 回答を登録
    for ($i = 1; $i <= $qa_cnt; $i++) {
        if($type[$i]==0){
            for ($j = 1; $j <= $select_num[$i]; $j++) {
                // 複数回答可 チェックボックス
                if ($multi_flg[$i] == "t") {
                    $varname = "answer" . $i . "_" . $j;
                } else {
                    // ラジオボタン
                    $varname = "answer" . $i;
                }
                $var_com = "answer_comment" . $i . "_" . $j;
                if ($$varname == $j) {
                    $sql = "insert into newsanswer (news_id, question_no, item_no, emp_id, answer_comment) values (";
                    $content = array($news_id, $i, $j, $emp_id, pg_escape_string($var_com));
                    $ins = insert_into_table($con, $sql, $content, $fname);
                    if ($ins == 0) {
                        pg_query($con, "rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                }
            }
        }else if($type[$i] == 1){
            $varname = "free_format" . $i;
            $sql = "insert into newsanswer (news_id, question_no, item_no, emp_id, answer_comment,free_format) values (";
            $content = array($news_id, $i, '1', $emp_id, pg_escape_string($$var_com), pg_escape_string($$varname));
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            
        }
    }
}

// コメント
if ($comment_flg == "t") {
    $sql = "select count(*) as cnt from newscomment";
    $cond = "where news_id = $news_id and emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $cnt = pg_fetch_result($sel, 0, "cnt");

    if ($cnt > "0") {
        $sql = "update newscomment set";
        $set = array("update_time", "comment");
        $setvalue = array(date("YmdHi"), pg_escape_string($comment));
        $cond = "where news_id = $news_id and emp_id = '$emp_id'";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    } else {
        $sql = "insert into newscomment (news_id, emp_id, update_time, comment) values (";
        $content = array($news_id, $emp_id, date("YmdHi"), pg_escape_string($comment));
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面遷移
switch ($from_page_id) {
    case "1":
        echo("<script type=\"text/javascript\">opener.location.href = 'left.php?session=$session';</script>");
        echo("<script type=\"text/javascript\">self.close();</script>");
        break;
    case "2":
        echo("<script type=\"text/javascript\">location.href = 'newsuser_detail.php?session=$session&news_id=$news_id&page=$page&class_page=$class_page&job_page=$job_page';</script>");
        break;
}
