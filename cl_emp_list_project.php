<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("cl_common.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_title_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	showLoginPage();
	exit;
}

// データベースに接続
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);
// 呼び元画面が「病棟評価表」画面の場合
if($call_screen == "WARD_GRADE")
{
    $arr_empmst = $obj->get_empmst($session);
    $login_emp_id = $arr_empmst[0]["emp_id"];

    $employee_div = "";

    // 該当職員が「評価者」どうか判断する
    if($obj->is_grader($login_emp_id))
    {
        $employee_div = "GRADER";
    }
    // 該当職員が「師長」どうか判断する
    if($obj->is_nursing_commander($login_emp_id))
    {
        $employee_div = "NURSING_COMMANDER";
    }
    // 該当職員が「特定職員」どうか判断する
    if($obj->is_particular_employee($login_emp_id))
    {
        $employee_div = "PARTICULAR_EMPLOYEE";
    }
}

//======================================
// 委員会・ＷＧ情報取得
//======================================
// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp_id = pg_result($sel, 0, "emp_id");


$arr_emplist_for_ward_grade = $obj->get_emp_list_book("", $employee_div, $login_emp_id);

// 委員会情報を取得
$sql = " select project.*, empmst.emp_lt_nm, empmst.emp_ft_nm from project inner join empmst on project.pjt_response = empmst.emp_id";
$cond = " where (project.pjt_public_flag = 't' or project.pjt_response = '$emp_id' or exists (select * from promember where promember.pjt_id = project.pjt_id and promember.emp_id = '$emp_id')) and project.pjt_parent_id is null and project.pjt_delete_flag = 'f'";
if ($view == "1") {
	$cond .= " and (project.pjt_real_end_date = '' or project.pjt_real_end_date is null)";
}

$order_by = "order by project.pjt_start_date desc";
$sel = select_from_table($con, $sql, "$cond $order_by", $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 0件の場合は復帰
if (pg_numrows($sel) == 0) {
    return;
}

while ($row = pg_fetch_array($sel)) {
    $projects[$row["pjt_id"]][0] = array("name" => $row["pjt_name"]);
    $tmp_pjt_id = $row["pjt_id"];

	// 選択されたアイコンのIDと同じならば以下の情報を読み込む
	$split_id = split("-", $selected_id);
	if ($split_id[0] != $row["pjt_id"]) {
		continue;
	}
    $sql_emp  = "select a.pjt_id, get_emp_name(a.pjt_response), get_mail_login_id(a.pjt_response), a.pjt_response, a.pjt_name,  e.emp_id, get_emp_name(e.emp_id), get_mail_login_id(l.emp_id), e.emp_id from project a, promember c, authmst d, empmst e,  login l  where d.emp_del_flg = 'f' and a.pjt_id = c.pjt_id and a.pjt_id = $tmp_pjt_id and c.emp_id = e.emp_id and l.emp_id = c.emp_id and l.emp_id = e.emp_id and d.emp_id = e.emp_id $approve_cond";
    $cond = " order by e.emp_kn_lt_nm, e.emp_kn_ft_nm";

    $sel_emp = select_from_table($con, $sql_emp, $cond, $fname);
    if ($sel_emp == 0) {
    	pg_close($con);
     	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
      	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
      	exit;
    }

    $i=0;
    while ($row_emp = pg_fetch_array($sel_emp))
    {
	  	if($i==0)
	  	{
            foreach($arr_emplist_for_ward_grade as $emplist_for_ward_grade)
            {
                if($emplist_for_ward_grade["emp_id"] == $row_emp[3])
                {
		            $employees[$row[0]][0][] = array("name" => $row_emp[1], "email" => $row_emp[2], "emp_id" => $row_emp[3]);
                }
            }
	  	}
	  	if(strlen($row_emp[7]) > 0)
	  	{
            foreach($arr_emplist_for_ward_grade as $emplist_for_ward_grade)
            {
                if($emplist_for_ward_grade["emp_id"] == $row_emp[8])
                {
					$employees[$row[0]][0][] = array("name" => $row_emp[6], "email" => $row_emp[7], "emp_id" => $row_emp[8]);
                }
            }
	  	}
      	$i++;
    }

    // WG情報を取得
    $sql2 = "select project.*, empmst.emp_lt_nm, empmst.emp_ft_nm from project inner join empmst on project.pjt_response = empmst.emp_id";
    $cond2 = "where (project.pjt_public_flag = 't' or project.pjt_response = '$emp_id' or exists (select * from promember where promember.pjt_id = project.pjt_id and promember.emp_id = '$emp_id')) and project.pjt_parent_id = $tmp_pjt_id and project.pjt_delete_flag = 'f'";

    if ($view == "1") {
    	$cond2 .= " and (project.pjt_real_end_date = '' or project.pjt_real_end_date is null)";
    }
    $sel2 = select_from_table($con, $sql2, "$cond2 $order_by", $fname);
    if ($sel2 == 0) {
      	pg_close($con);
      	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
      	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
      	exit;
    }

    while ($row2 = pg_fetch_array($sel2)) {
      	$projects[$row["pjt_id"]][$row2["pjt_id"]] = array("name" => $row2["pjt_name"]);

	    $sql_emp  = "select a.pjt_id, get_emp_name(a.pjt_response), get_mail_login_id(a.pjt_response), a.pjt_response, a.pjt_name,  e.emp_id, get_emp_name(e.emp_id), get_mail_login_id(l.emp_id), e.emp_id from project a, promember c, authmst d, empmst e,  login l  where d.emp_del_flg = 'f' and a.pjt_id = c.pjt_id and a.pjt_id = $row2[pjt_id] and c.emp_id = e.emp_id and l.emp_id = c.emp_id and l.emp_id = e.emp_id and d.emp_id = e.emp_id $approve_cond";
    	$cond = " order by e.emp_kn_lt_nm, e.emp_kn_ft_nm";

      	$sel_emp = select_from_table($con, $sql_emp, $cond, $fname);
      	if ($sel_emp == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
	    }

    	$i=0;
        while ($row_emp = pg_fetch_array($sel_emp))
        {
			if($i==0)
			{
	            foreach($arr_emplist_for_ward_grade as $emplist_for_ward_grade)
	            {
	                if($emplist_for_ward_grade["emp_id"] == $row_emp[3])
	                {
						$employees[$row['pjt_id']][$row2["pjt_id"]][] = array("name" => $row_emp[1], "email" => $row_emp[2], "emp_id" => $row_emp[3]);
	                }
	            }
			}

			if(strlen($row_emp[7]) > 0)
			{
	            foreach($arr_emplist_for_ward_grade as $emplist_for_ward_grade)
	            {
	                if($emplist_for_ward_grade["emp_id"] == $row_emp[8])
	                {
				        $employees[$tmp_pjt_id][$row2['pjt_id']][] = array("name" => $row_emp[6], "email" => $row_emp[7], "emp_id" => $row_emp[8]);
	                }
	            }
			}
			$i++;
        }
	}
}

uasort($projects, "sort_project_by_name");
foreach ($projects as $tmp_pjt_id => $tmp_pjt) {
	uasort($tmp_pjt, "sort_wg_by_name");
	$projects[$tmp_pjt_id] = $tmp_pjt;
}

$cl_title = cl_title_name();

//==============================
// HTML
//==============================
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cl_title?> | 職員検索</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script src="js/prototype/dist/prototype.js"></script>
<script language="javascript">

function changeDisplay(class_id) {
	document.mainform.selected_id.value = class_id;
	var img = document.getElementById('class'.concat(class_id));
	if (img.className == 'close') {
		img.className = 'open';
		row_display = '';
	} else {
		img.className = 'close';
		row_display = 'none';
	}

	var rows = document.getElementsByTagName('tr');
	var row_class = 'children_of_'.concat(class_id);
	for (var i = 0, j = rows.length; i < j; i++) {
		if (rows[i].className == '') {
			continue;
		}

		if (rows[i].className == row_class) {
			rows[i].style.display = row_display;
			continue;
		}

		if (rows[i].className.indexOf(row_class.concat('-')) >= 0) {
			if (img.className == 'close') {
				rows[i].style.display = row_display;
				continue;
			}

			if (img.className == 'open') {
				var parent_img_id = 'class'.concat(rows[i].className.replace('children_of_', ''));
				var parent_img = document.getElementById(parent_img_id);
				if (parent_img.className == 'close') {
					rows[i].style.display = 'none';
					continue;
				} else if (parent_img.className == 'open') {
					rows[i].style.display = '';
					continue;
				}
			}
		}
	}
}

function readEmpData(class_id) {
	document.mainform.mode.value = 'disp';
	document.mainform.selected_id.value = class_id;
	document.mainform.action = "cl_emp_list_project.php?session=<?=$session?>&call_screen=<?=$call_screen?>";
	document.mainform.submit();
}

// コピー処理
function copy_emp(emp_id, emp_nm)
{
    if(window.opener && !window.opener.closed && window.opener.add_target_list)
    {

		if (emp_id == "")
		{
		    alert('職員が存在しません');
			return;
		}
        set_wm_counter(emp_id);
        window.opener.add_target_list('<?=$input_div?>', emp_id, emp_nm);
    }
}

function copyEmpData(data_id, col_name)
{
	document.mainform.mode.value = 'copy';
	document.mainform.data_id.value = data_id;
	document.mainform.col_name.value = col_name;
	document.mainform.action = "cl_emp_list_project.php?session=<?=$session?>&call_screen=<?=$call_screen?>";
	document.mainform.submit();
}

// ajax 「よく使う人」カウントアップ処理
function set_wm_counter(ids)
{
	var url = 'cl_wm_counter.php';
	var params = $H({'session':'<?=$session?>','used_emp_ids':ids}).toQueryString();
	var myAjax = new Ajax.Request(
		url,
		{
			method: 'post',
			postBody: params
		});
}

<?
// コピーボタン押下時、対象を追加する関数を呼出す。
if ($mode == "copy") {
	$emp_info = get_project_emp_ids($con, $data_id, $col_name, $fname, $arr_emplist_for_ward_grade);
?>
function copy_target() {

	copy_emp('<?=$emp_info["emp_id"]?>', '<?=$emp_info["emp_name"]?>');

<?
// 最後にクリックされたアイコンの位置を展開する。
	$split_id = split("-", $selected_id);
	if ($split_id[0] != "") {
		echo ("changeDisplay('$split_id[0]');");
	}
	if ($split_id[1] != "") {
		echo ("changeDisplay('$split_id[0]-$split_id[1]');");
	}
?>

}
<?
}
?>

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

<!--
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

img.close {
	background-image:url("img/icon/plus.gif");
	vertical-align:middle;
}
img.open {
	background-image:url("img/icon/minus.gif");
	vertical-align:middle;
}

// -->

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"

<?
// 展開用アイコン押下時
if ($mode == "disp") {
	echo (" onload=\"changeDisplay('$selected_id');\"");
}

// コピーボタン押下時
if ($mode == "copy") {
	echo (" onload=\"copy_target();\"");
}

?>
>

<form name="mainform" action="#" method="post">
<input type="hidden" name="selected_id" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="data_id" value="">
<input type="hidden" name="col_name" value="">
<input type="hidden" name="input_div" value="<?=$input_div?>">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#35B341">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>職員検索</b></font></td>
<td>&nbsp</td>
<td width="10">&nbsp</td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" width="10" height="10" alt=""><br>

<?
// タブ表示
show_emp_list_header_tab($session, $fname, $input_div, $call_screen);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td>

<!-- ここから -->

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="26" bgcolor="#DFFFDC">
<td width="15%">&nbsp;</td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG名</font></td>
</tr>
<?  // 委員会
foreach ($projects as $tmp_class_id => $tmp_atrbs) {
?>
<tr height="26">
<td align="center">
<input type=button value="コピー" onclick="copyEmpData('<? echo ($tmp_class_id);?>', '0');">
</td>
<?
if ($selected_id != $tmp_class_id) {
	$func_name = "readEmpData";
} else {
	$func_name = "changeDisplay";
}
?>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><img id="class<? echo($tmp_class_id); ?>" src="img/spacer.gif" alt="" width="20" height="20" class="close" style="margin-right:3px;" onclick="<? echo("$func_name") ?>('<? echo($tmp_class_id); ?>')"><? echo($tmp_atrbs[0]["name"]); ?>
</font>
</td>
</tr>

<? // 委員会ごとの職員
foreach ($employees[$tmp_class_id][0] as $employee ) {
?>
<tr class="children_of_<? echo("{$tmp_class_id}"); ?>" height="26" style="display:none;">
<td align="center">
<input type=button value="コピー" onclick="copy_emp('<? echo $employee['emp_id']; ?>', '<? echo $employee['name']; ?>');">
</td>
<td style="padding-left:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? print $employee['name'] . " &lt;" . $employee['email']  ."&gt;" ; ?></font></td>
</tr>
<?
}
?>
<? // ＷＧ
foreach ($tmp_atrbs as $tmp_atrb_id => $tmp_depts) {
	if ($tmp_atrb_id == "0") {continue;}
?>
<tr class="children_of_<? echo($tmp_class_id); ?>" height="26" style="display:none;">
<td align="center">
<input type=button value="コピー" onclick="copyEmpData('<? echo ($tmp_class_id);?>', '<? echo ($tmp_atrb_id); ?>');">
</td>
<td style="padding-left:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<img id="class<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?>" src="img/spacer.gif" alt="" width="20" height="20" class="close" style="margin-right:3px;" onclick="changeDisplay('<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?>')"><? echo($tmp_depts["name"]); ?>
</font>
</td>
</tr>

<? // ＷＧごとの職員
foreach ($employees[$tmp_class_id][$tmp_atrb_id] as $employee ) {
?>

<tr class="children_of_<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?>" height="26" style="display:none;">
<td align="center">
<input type=button value="コピー" onclick="copy_emp( '<? echo $employee['emp_id']; ?>', '<? echo $employee['name']; ?>');">
</td>
<td style="padding-left:60px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? print $employee['name'] . " &lt;" . $employee['email']  ."&gt;" ; ?></font></td>
</tr>
<?
}
?>

<? // 委員会 end
}
?>

<? // ＷＧ end
}
?>
</table>

<!-- ここまで -->

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>

</form>
</body>
</html>




<?
//-------------------------------------------
// 委員会・ＷＧごとに属する職員を一括取得する。
//-------------------------------------------
function get_project_emp_ids($con, $pjt_id, $wg_id, $fname, $arr_emplist_for_ward_grade)
{
    if($wg_id == 0){
    	  $sql_emp  = "select a.pjt_id, get_emp_name(a.pjt_response), a.pjt_response, get_mail_login_id(a.pjt_response), a.pjt_name,  b.pjt_id, get_emp_name(b.pjt_response), get_mail_login_id(b.pjt_response), b.pjt_response, b.pjt_name, e.emp_id, get_emp_name(e.emp_id), get_mail_login_id(l.emp_id) from project a left outer join project b on a.pjt_id = b.pjt_parent_id, promember c, authmst d, empmst e,  login l  where d.emp_del_flg = 'f' and (c.pjt_id = a.pjt_id or c.pjt_id = b.pjt_id ) and a.pjt_id = $pjt_id and c.emp_id = e.emp_id and l.emp_id = c.emp_id and l.emp_id = e.emp_id and d.emp_id = e.emp_id";
    }elseif($wg_id > 0){
      	$sql_emp = " select a.pjt_id, get_emp_name(a.pjt_response),  a.pjt_response, get_mail_login_id(a.pjt_response), a.pjt_name,  '', '', '', '', '', e.emp_id, get_emp_name(e.emp_id), get_mail_login_id(l.emp_id) from project a, promember c, authmst d, empmst e,  login l  where d.emp_del_flg = 'f' and c.pjt_id = a.pjt_id  and a.pjt_id = $wg_id and c.emp_id = e.emp_id and l.emp_id = c.emp_id and l.emp_id = e.emp_id and d.emp_id = e.emp_id";
    }
    $cond = " order by e.emp_kn_lt_nm, e.emp_kn_ft_nm";
    $sel_emp = select_from_table($con, $sql_emp, $cond, $fname);
    if ($sel_emp == 0) {
		pg_close($con);
    	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
      	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
      	exit;
    }

//    $arr_project = array();
	$arr_emp_id = array();
	$arr_emp_name = array();
    $i=0;
    while ($row_emp = pg_fetch_array($sel_emp)) {
    	if($i==0){
//			$arr_project[] = array("name" => $row_emp[1], "email" => $row_emp[3], "emp_id" => $row_emp[2]);

            foreach($arr_emplist_for_ward_grade as $emplist_for_ward_grade)
            {
                if($emplist_for_ward_grade["emp_id"] == $row_emp[2])
                {
					$arr_emp_id[] = $row_emp[2];
		      		$arr_emp_name[]  = $row_emp[1];
                }
            }
      	}
      	if($last_wg != $row_emp[5]){
//			$arr_project[] = array("name" => $row_emp[6], "email" => $row_emp[7], "emp_id" => $row_emp[8]);

            foreach($arr_emplist_for_ward_grade as $emplist_for_ward_grade)
            {
                if($emplist_for_ward_grade["emp_id"] == $row_emp[8])
                {
					$arr_emp_id[] = $row_emp[8];
		      		$arr_emp_name[]  = $row_emp[6];
                }
            }
      	}
      	$last_wg = $row_emp[5];
        if(strlen($row_emp[12]) > 0 ){
//			array_push($arr_project,  array("name" => $row_emp[11], "email" => $row_emp[12], "emp_id" => $row_emp[10]));

            foreach($arr_emplist_for_ward_grade as $emplist_for_ward_grade)
            {
                if($emplist_for_ward_grade["emp_id"] == $row_emp[10])
                {
					$arr_emp_id[] = $row_emp[10];
		      		$arr_emp_name[]  = $row_emp[11];
                }
            }
      	}
 	    $i++;
    }

	// 重複データ除去
	$arr_emp_id = array_unique($arr_emp_id);
	$arr_emp_name = array_unique($arr_emp_name);

	$ids = join(",", $arr_emp_id);
	$names = join(",", $arr_emp_name);

	$arr = array("emp_id" => $ids, "emp_name" => $names);

    return $arr;
}
//==============================
//データベース接続を閉じる
//==============================
pg_close($con);
?>
<?
function sort_project_by_name($p1, $p2) {
	return strcasecmp($p1["0"]["name"], $p2["0"]["name"]);
}

function sort_wg_by_name($w1, $w2) {
	if ($w1["type"] == "p") {
		return -1;
	}
	if ($w2["type"] == "p") {
		return 1;
	}
	return strcasecmp($w1["name"], $w2["name"]);
}
?>
