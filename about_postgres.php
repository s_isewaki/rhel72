<?php
require_once("about_error.php");
require_once("about_sql_log.php");

if (!function_exists("connect2db")) {

/**
 * connect2db
 * PostgreSQLデータベースとの接続
 *
 * @param $fname
 * @return $con
 */
function connect2db($fname) {
	@require("conf/conf.inf");
	if (!defined("CONF_FILE")) {
		write_error($fname, $ERRORLOG, "cannot find conf.inf");
		return 0;
	}

	if (!@($con = (pg_connect("host=$IP4DBSERVER port=$PORT4DB dbname=$NAME4DB user=$USER4DB password=$PASSWD4DB")))) {
		write_error($fname, $ERRORLOG, "cannot connect to db:" . pg_last_error($con));
		return 0;
	}

	return $con;
}

/**
 * select_from_table
 * テーブルSELECT
 *
 * @param $con
 * @param $SQL
 * @param $cond
 * @param $fname
 * @return $result
 */
function select_from_table($con, $SQL, $cond, $fname) {
	if ($SQL=="vacuum full analyze") return 1;
	@require("conf/conf.inf");
	if (!defined("CONF_FILE")) {
		write_error($fname, $ERRORLOG, "cannot find conf.inf");
		return 0;
	}

	$SQL .= " $cond";
	$SQL = trim($SQL);

	// $SQLLOGがonの場合、SQLログを出力
	write_sql_log($SQLLOG, $fname, $SQL, $SQLLOGFILE, $ERRORLOG);

	if (!@($result = (pg_query($con, $SQL)))) {
		write_error($fname, $ERRORLOG, "fail to execute \"$SQL\"; " . pg_last_error($con));
		return 0;
	}

	return $result;
}

/**
 * insert_into_table
 * テーブルINSERT
 *
 * @param $con
 * @param $SQL
 * @param $content
 * @param $fname
 * @return 成功 → 1 失敗 → 0
 */
function insert_into_table($con, $SQL, $content, $fname) {
	@require("conf/conf.inf");
	if (!defined("CONF_FILE")) {
		write_error($fname, $ERRORLOG, "cannot find conf.inf");
		return 0;
	}

	$sql = "";
	$content_key_max = count($content) - 1;
	while (list($key, $val) = each($content)) {
		$sql .= (is_null($val)) ? "null" : "'$val'";
		if ($key != $content_key_max) {
			$sql .= ",";
		}
	}
	$SQL .= $sql . ")";
	$SQL = trim($SQL);

	// $SQLLOGがonの場合、SQLログを出力
	write_sql_log($SQLLOG, $fname, $SQL, $SQLLOGFILE, $ERRORLOG);

	if (!@($result = (pg_query($con, $SQL)))) {
		write_error($fname, $ERRORLOG, "fail to execute \"$SQL\"; " . pg_last_error($con));
		return 0;
	}

	return 1;
}

/**
 * insert_into_table_no_content
 * テーブルINSERT (contentなし)
 *
 * @param $con
 * @param $SQL
 * @param $fname
 * @return 成功 → 1 失敗 → 0
 */
function insert_into_table_no_content($con, $SQL, $fname) {
	@require("conf/conf.inf");
	if (!defined("CONF_FILE")) {
		write_error($fname, $ERRORLOG, "cannot find conf.inf");
		return 0;
	}

	$SQL = trim($SQL);

	// SQLLOGがonの場合、SQLログを出力
	write_sql_log($SQLLOG, $fname, $SQL, $SQLLOGFILE, $ERRORLOG);

	if (!@($result = (pg_query($con, $SQL)))) {
		write_error($fname, $ERRORLOG, "fail to execute \"$SQL\"; " . pg_last_error($con));
		return 0;
	}

	return 1;
}

/**
 * update_set_table
 * テーブルUPDATE
 *
 * @param $con
 * @param $SQL
 * @param $set
 * @param $setvalue
 * @param $cond
 * @param $fname
 * @return 成功 → 1 失敗 → 0
 */
function update_set_table($con, $SQL, $set, $setvalue, $cond, $fname) {
	@require("conf/conf.inf");
	if (!defined("CONF_FILE")) {
		write_error($fname, $ERRORLOG, "cannot find conf.inf");
		return 0;
	}

	$sql = "";
	$set_key_max = count($set) - 1;
	while (list($key, $val) = each($set)) {
		$sql .= " $val = ";
		$sql .= (is_null($setvalue[$key])) ? "null " : "'$setvalue[$key]' ";
		if ($key != $set_key_max) {
			$sql .= ",";
		}
	}
	$SQL .= $sql . $cond;
	$SQL = trim($SQL);

	// $SQLLOGがonの場合、SQLログを出力
	write_sql_log($SQLLOG, $fname, $SQL, $SQLLOGFILE, $ERRORLOG);

	if (!@($result = (pg_query($con, $SQL)))) {
		write_error($fname, $ERRORLOG, "fail to execute \"$SQL\"; " . pg_last_error($con));
		return 0;
	}

	return 1;
}

/**
 * delete_from_table
 * テーブルDELETE
 *
 * @param $con
 * @param $SQL
 * @param $cond
 * @param $fname
 * @return 成功 → 1 失敗 → 0
 */
function delete_from_table($con, $SQL, $cond, $fname) {
	@require("conf/conf.inf");
	if (!defined("CONF_FILE")) {
		write_error($fname, $ERRORLOG, "cannot find conf.inf");
		return 0;
	}

	$SQL .= " " . $cond;
	$SQL = trim($SQL);

	// $SQLLOGがonの場合、SQLログを出力
	write_sql_log($SQLLOG, $fname, $SQL, $SQLLOGFILE, $ERRORLOG);

	if (!@($result = (pg_query($con, $SQL)))) {
		write_error($fname, $ERRORLOG, "fail to execute \"$SQL\"; " . pg_last_error($con));
		return 0;
	}

	return 1;
}

/**
 * check_record
 * $resultのレコード数を返す
 *
 * @param $result
 * @return function
 */
function check_record($result) {
	return pg_num_rows($result);
}

}
