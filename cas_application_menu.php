<?php
ini_set("display_errors","1");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_cas_application_category_list.ini");
require_once("cas_application_template.ini");
require_once("show_select_values.ini");
require_once("cas_yui_calendar_util.ini");
require_once("get_values.ini");
require_once("cas_application_draft_template.ini");
require_once("cas_application_workflow_common_class.php");
require_once("cas_application_common.ini");
require_once("cas_common.ini");
require_once("get_cas_title_name.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session, $CAS_MENU_AUTH, $fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CAS_MANAGE_AUTH, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// ログインユーザの職員IDを取得
//====================================
$sql = "select emp_id from session where session_id = '$session'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$obj = new cas_application_workflow_common_class($con, $fname);

$cas_title = get_cas_title_name();

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$cas_title?> | 申請</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<?php
if($wkfw_id != "")
{
	if($apply_id == "")
	{
		// ワークフロー情報取得
		$arr_wkfwmst       = $obj->get_wkfwmst($wkfw_id);
		$wkfw_content      = $arr_wkfwmst[0]["wkfw_content"];
		$wkfw_content_type = $arr_wkfwmst[0]["wkfw_content_type"];
		$short_wkfw_name   = $arr_wkfwmst[0]["short_wkfw_name"];

		// 本文形式タイプのデフォルトを「テキスト」とする
		if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

		// 本文形式タイプのデフォルトを「テンプレート」の場合
		if($wkfw_content_type == "2")
		{
			$wkfw_history_no = $obj->get_max_wkfw_history_no($wkfw_id);
		}

		$num = 0;
		$pos = 0;
		while (1) {
			$pos = strpos($wkfw_content, 'show_cal', $pos);
			if ($pos === false) {
				break;
			} else {
				$num++;
			}
			$pos++;
		}

		if ($num > 0) {
			// 外部ファイルを読み込む
			write_yui_calendar_use_file_read_0_12_2();
		}

		$arr_option = "";
		// レベルアップ志願書 2011.12.16 Edit by matsuura
//		if($short_wkfw_name == "c101" || $short_wkfw_name == "c102")
		if($short_wkfw_name == "c101" || $short_wkfw_name == "c102" || $short_wkfw_name == "c103" || $short_wkfw_name == "g101" || $short_wkfw_name == "g102" || $short_wkfw_name == "g103" || $short_wkfw_name == "g104")
		{
 		    $arr_option = array("2");
        }

		// カレンダー作成、関数出力
		write_yui_calendar_script2($num, $arr_option);

		if ($mode == "apply_printwin") {

		//---------テンプレートの場合XML形式のテキスト$contentを作成---------

			if ($wkfw_content_type == "2") {
				// XML変換用コードの保存
				$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
				$savexmlfilename = "cas_workflow/tmp/{$session}_x.php";
				$fp = fopen($savexmlfilename, "w");

				if (!$fp) {
					echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
					echo("<script language='javascript'>history.back();</script>");
				}
				if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
					fclose($fp);
					echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
					echo("<script language='javascript'>history.back();</script>");
				}
				fclose($fp);

				include( $savexmlfilename );

			}
			$savexmlfilename = "cas_workflow/tmp/{$session}_d.php";
			$fp = fopen($savexmlfilename, "w");

			if (!$fp) {
				echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
				echo("<script language='javascript'>history.back();</script>");
			}

			if(!fwrite($fp, $content, 2000000)) {
				fclose($fp);
				echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
				echo("<script language='javascript'>history.back();</script>");
			}

			fclose($fp);
		}
	}
	else
	{
		$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
		$wkfw_content      = $arr_apply_wkfwmst[0]["wkfw_content"];
		$wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
		$apply_content     = $arr_apply_wkfwmst[0]["apply_content"];
		$wkfw_history_no   = $arr_apply_wkfwmst[0]["wkfw_history_no"];
		$short_wkfw_name   = $arr_apply_wkfwmst[0]["short_wkfw_name"];

		// ワークフロー情報取得
		/*
		$sel_apply_wkfwmst = search_apply_wkfwmst($con, $fname, $target_apply_id);
		$wkfw_content = pg_fetch_result($sel_apply_wkfwmst, 0, "wkfw_content");
		$wkfw_content_type = pg_fetch_result($sel_apply_wkfwmst, 0, "wkfw_content_type");
		$apply_content = pg_fetch_result($sel_apply_wkfwmst, 0, "apply_content");
		*/
		// 本文形式タイプのデフォルトを「テキスト」とする
		if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

		// 形式をテキストからテンプレートに変更した場合の古いデータ対応
		// wkfw_content_typeは2:テンプレートだが、登録データがXMLでない場合、1:テキストとして処理する
		if ($wkfw_content_type == "2") {
			if (strpos($apply_content, "<?xml") === false) {
				$wkfw_content_type = "1";
			}
		}

		if($wkfw_history_no != "")
		{
			$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
			$wkfw_content = $arr_wkfw_template_history["wkfw_content"];
		}

		$num = 0;
		if ($wkfw_content_type == "2") {
			$pos = 0;
			while (1) {
				$pos = strpos($wkfw_content, 'show_cal', $pos);
				if ($pos === false) {
					break;
				} else {
					$num++;
				}
				$pos++;
			}
		}

		if ($num > 0) {
			// 外部ファイルを読み込む
			write_yui_calendar_use_file_read_0_12_2();
		}

		$arr_option = "";
		// レベルアップ志願書
		if($short_wkfw_name == "c201" || $short_wkfw_name == "c202")
        {
 		    $arr_option = array("2");
        }

		// カレンダー作成、関数出力
		write_yui_calendar_script2($num,  $arr_option);

		if ($mode == "apply_printwin") {

		//---------テンプレートの場合XML形式のテキスト$contentを作成---------

			if ($wkfw_content_type == "2") {
				// XML変換用コードの保存
				$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
				$savexmlfilename = "cas_workflow/tmp/{$session}_x.php";
				$fp = fopen($savexmlfilename, "w");

				if (!$fp) {
					echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
					echo("<script language='javascript'>history.back();</script>");
				}
				if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
					fclose($fp);
					echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
					echo("<script language='javascript'>history.back();</script>");
				}
				fclose($fp);

				include( $savexmlfilename );

			}
			$savexmlfilename = "cas_workflow/tmp/{$session}_d.php";
			$fp = fopen($savexmlfilename, "w");

			if (!$fp) {
				echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
				echo("<script language='javascript'>history.back();</script>");
			}

			if(!fwrite($fp, $content, 2000000)) {
				fclose($fp);
				echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
				echo("<script language='javascript'>history.back();</script>");
			}
			fclose($fp);

		}
	}
}


?>
<script type="text/javascript">

function reload_apply_page()
{
<?
    if($apply_id != "" && $wkfw_id != "")
    {
?>
	document.apply.action="cas_application_menu.php?session=<?=$session?>&apply_id=<?=$apply_id?>&wkfw_id=<?=$wkfw_id?>";
<?
    }
    else
    {
?>
	document.apply.action="cas_application_menu.php?session=<?=$session?>";
<?
    }
?>
	document.apply.submit();
}


function init() {

	if(window.start_auto_session_update)
    {
		start_auto_session_update();
	}

	if('<?=$back?>' == 't') {
<?

		$approve_num = $_POST[approve_num];
		for($i=1; $i<=$approve_num;$i++) {

			$regist_emp_id = "regist_emp_id$i";
			$radio_emp_id = "radio_emp_id$i";
			$approve_name = "approve_name$i";

?>
			var radio_emp_id = 'radio_emp_id' + <?=$i?>;
			if(document.getElementById('<?=$radio_emp_id?>')) {
				for(j=0; j<document.apply.elements['<?=$radio_emp_id?>'].length; j++) {
					if(document.apply.elements['<?=$radio_emp_id?>'][j].value == '<?=$_POST[$regist_emp_id];?>') {
						document.apply.elements['<?=$radio_emp_id?>'][j].checked = true;
					}
				}
				document.apply.elements['<?=$regist_emp_id?>'].value = '<?=$_POST[$regist_emp_id];?>';
			}
			if(document.getElementById('<?=$approve_name?>')) {
				document.apply.elements['<?=$approve_name?>'].value = '<?=$_POST[$approve_name];?>';
				document.apply.elements['<?=$regist_emp_id?>'].value = '<?=$_POST[$regist_emp_id];?>';
			}
<?
		}
?>
	}
}

function attachFile() {
	window.open('cas_apply_attach.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function detachFile(e) {
	if (e == undefined) {
		e = window.event;
	}

	var btn_id;
	if (e.target) {
		btn_id = e.target.getAttribute('id');
	} else {
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_', '');

	var p = document.getElementById('p_' + id);
	document.getElementById('attach').removeChild(p);
}


// 申請処理
function apply_regist(apv_num, precond_num) {

<?
// 病棟評価表チェック
show_js_ward_regist_chk($con, $fname, $short_wkfw_name, $session);
// 評価申請チェック
show_js_eval_regist_chk($con, $fname, $short_wkfw_name);
// レベルアップ申請チェック
show_js_levelup_apply_chk($con, $fname, $short_wkfw_name);

?>

	// 未入力チェック
	for(i=1; i<=precond_num; i++)
	{
		obj = 'precond_apply_id' + i;
		apply_id = document.apply.elements[obj].value;
		if(apply_id == "") {
			alert('承認確定の申請書を設定してくだい。');
			return;
		}
	}

	var apv_selected = false;
	var apv_all_selected = true;
	for(i=1; i<=apv_num; i++) {
		obj = 'regist_emp_id' + i;
		emp_id = document.apply.elements[obj].value;
		if (emp_id == "") {
			apv_all_selected = false;
		} else {
			apv_selected = true;
		}
	}
	if (!apv_selected) {
		alert('承認者を選択してください。');
		return;
	}
	if (!apv_all_selected && !confirm('指定されていない承認者がありますが、送信しますか？')) {
		return;
	}

	if (window.InputCheck) {
		if (!InputCheck()) {
			return;
		}
	}

	// 重複チェック
	dpl_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj_src = 'regist_emp_id' + i;
		emp_id_src = document.apply.elements[obj_src].value;
		if (emp_id_src == '') continue;

		for(j=i+1; j<=apv_num; j++) {
			obj_dist = 'regist_emp_id' + j;
			emp_id_dist = document.apply.elements[obj_dist].value;
			if(emp_id_src == emp_id_dist) {
				dpl_flg = true;
			}
		}
	}

	if(dpl_flg)
	{
		if (!confirm('承認者が重複していますが、申請しますか？'))
		{
			return;
		}
	}
	document.apply.action="cas_application_submit.php";
	document.apply.submit();

}

// 下書き保存
function draft_regist()
{
	document.apply.action="cas_application_submit.php?draft=on";
	document.apply.submit();
}

function apply_printwin(id) {

	document.apply.mode.value = "apply_printwin";
	if(id == "")
	{
		document.apply.action="cas_application_menu.php";
	}
	else
	{
		document.apply.action="cas_application_menu.php?apply_id=" + id;
	}
	document.apply.submit();
}

function apply_simpleprintwin(id) {
	document.apply.simple.value = "t";
	apply_printwin(id);
}

function apply_draft(id, apv_num, precond_num)
{
<?
// 病棟評価表チェック
show_js_ward_regist_chk($con, $fname, $short_wkfw_name, $session);
// 評価申請チェック
show_js_eval_regist_chk($con, $fname, $short_wkfw_name);
// レベルアップ申請チェック
show_js_levelup_apply_chk($con, $fname, $short_wkfw_name);

?>

	// 未入力チェック
	for(i=1; i<=precond_num; i++)
	{
		obj = 'precond_apply_id' + i;
		apply_id = document.apply.elements[obj].value;
		if(apply_id == "") {
			alert('承認確定の申請書を設定してくだい。');
			return;
		}
	}

	var apv_selected = false;
	var apv_all_selected = true;
	for(i=1; i<=apv_num; i++) {
		obj = 'regist_emp_id' + i;
		emp_id = document.apply.elements[obj].value;
		if (emp_id == "") {
			apv_all_selected = false;
		} else {
			apv_selected = true;
		}
	}
	if (!apv_selected) {
		alert('承認者を選択してください。');
		return;
	}
	if (!apv_all_selected && !confirm('指定されていない承認者がありますが、送信しますか？')) {
		return;
	}

	if (window.InputCheck) {
		if (!InputCheck()) {
			return;
		}
	}

	// 重複チェック
	dpl_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj_src = 'regist_emp_id' + i;
		emp_id_src = document.apply.elements[obj_src].value;
		if (emp_id_src == '') continue;

		for(j=i+1; j<=apv_num; j++) {
			obj_dist = 'regist_emp_id' + j;
			emp_id_dist = document.apply.elements[obj_dist].value;
			if(emp_id_src == emp_id_dist) {
				dpl_flg = true;
			}
		}
	}

	if(dpl_flg)
	{
		if (!confirm('承認者が重複していますが、申請しますか？'))
		{
			return;
		}
	}

	document.apply.action="cas_application_draft_submit.php?apply_id=" + id;
	document.apply.submit();

}

function draft_update(id)
{
	document.apply.action="cas_application_draft_submit.php?apply_id=" + id + "&draft=on";
	document.apply.submit();
}

function draft_delete(id)
{
	document.apply.action="cas_application_draft_delete.php?apply_id=" + id;
	document.apply.submit();
}


// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];
		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">


.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}

table.block2 {border-collapse:collapse;}
table.block2 td {border:#A0D25A solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#A0D25A solid 0px;}
table.block3 td {border-width:0;}


p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init();
<? if ($wkfw_id != "") { ?>
initcal();if(window.cas_load){cas_load();};if(window.no_7_load){no_7_load();};
<? } ?>
<?php
if ($mode == "apply_printwin")
{

if($wkfw_id != "" && $apply_id == "")
{
?>
window.open('cas_application_template_print.php?session=<?=$session?>&fname=<?=$fname?>&wkfw_id=<?=$wkfw_id?>&achievement_order=<?=$achievement_order?>&back=f&mode=apply_print&simple=<?=$simple?>', 'apply_print', 'width=670,height=700,scrollbars=yes');
<?
}
else if($wkfw_id != "" && $apply_id != "")
{
?>
window.open('cas_application_draft_template_print.php?session=<?=$session?>&fname=<?=$fname?>&target_apply_id=<?=$apply_id?>&achievement_order=<?=$achievement_order?>&back=f&mode=apply_print&simple=<?=$simple?>', 'apply_print', 'width=670,height=700,scrollbars=yes');
<?
}
?>

<?
}
?>

if (window.OnloadSub) { OnloadSub(); };resizeAllTextArea();">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="cas_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/s42.gif" width="32" height="32" border="0" alt="<? echo($cas_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="cas_application_menu.php?session=<? echo($session); ?>"><b><? echo($cas_title); ?></b></a> &gt; <a href="cas_application_menu.php?session=<? echo($session); ?>"><b>申請</b></a></font></td>
<? if ($workflow_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="cas_workflow_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>

<?
show_applycation_menuitem($session,$fname,"");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="2"><br>



<?php if($wkfw_id != "") { ?>
<!-- セッションタイムアウト防止 START -->
<SCRIPT type="text/javascript">
function start_auto_session_update()
{
    //10分(10*60*1000ms)後に開始
    setTimeout(auto_sesson_update,600000);
}
function auto_sesson_update()
{
    //セッション更新
    document.session_update_form.submit();

    //10分(10*60*1000ms)後に最呼び出し
    setTimeout(auto_sesson_update,600000);
}
</SCRIPT>
<form name="session_update_form" action="cas_session_update.php" method="post" target="session_update_frame">
<input type="hidden" name="session" value="{$session}">
</form>
<iframe name="session_update_frame" width="0" height="0" frameborder="0"></iframe>
<!-- セッションタイムアウト防止 END -->
<?php } ?>

<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="wkfw_id" value="<?=$wkfw_id?>">
<input type="hidden" name="wkfw_type" value="<?=$wkfw_type?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="simple" value="">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">

<input type="hidden" id="short_wkfw_name" name="short_wkfw_name" value="<?=$short_wkfw_name?>">
<input type="hidden" id="achievement_order" name="achievement_order" value="<?=$achievement_order?>">
<input type="hidden" id="ward_dpl_chk" name="ward_dpl_chk" value="">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="2" cellpadding="2">
<tr valign="top">
<td width="25%">
<?php show_application_cate_list($con, $session, $fname, $wkfw_type, $wkfw_id, $apply_id); ?>
</td>
<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td width="75%">
<?php
if($wkfw_id != "" && $apply_id == "")
{
    $style = array("c208", "c216", "g210", "g221", "g231", "g242");
    if (in_array($short_wkfw_name, $style)) {
        show_application_template($con, $session, $fname, $wkfw_id, $apply_title, $content, $file_id, $filename, $back, $mode, $achievement_order, $application_book_no, $application_book_apply_id, $date_y, $date_m, $date_d);
    } else {
        show_application_template($con, $session, $fname, $wkfw_id, $apply_title, $content, $file_id, $filename, $back, $mode, $achievement_order, $application_book_no, $application_book_apply_id);
    }
}
else if($wkfw_id != "" && $apply_id != "")
{
    // 2012/02/20 Yamagawa upd(s)
    //show_draft_template($con, $session, $fname, $apply_id, $apply_title, $content, $file_id, $filename, $back, $mode, $achievement_order);
    show_draft_template($con, $session, $fname, $apply_id, $apply_title, $content, $file_id, $filename, $back, $mode, $achievement_order, $application_book_no, $application_book_apply_id);
    // 2012/02/20 Yamagawa upd(e)
}
?>
</td>
</tr>
</table>

</td>
</tr>
</table>


</form>

</td>
</tr>
</table>
</body>
</html>
<?php

// 病棟評価申請の場合
if($obj->is_ward_template($short_wkfw_name));
{
// 達成期間画面でデータ選択後のリロードされた場合
if($achievement_order != "")
{

$arr_ward_template_db = $obj->get_ward_template_db($short_wkfw_name);

// リロード前の入力データをセット
?>

<script type="text/javascript">

<?
foreach($arr_ward_template_db as $ward_template_db)
{
?>
document.getElementById('<?=$ward_template_db?>').value = '<?=$$ward_template_db?>';
<?
}
?>
</script>
<?
}
}

// cas_workflow/tmpディレクトリ作成
$obj->create_wkfwtmp_directory();
// cas_apply/tmpディレクトリ作成
$obj->create_applytmp_directory();

pg_close($con);
?>

