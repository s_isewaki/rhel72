<?php
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_workflow_all_apply_list.ini");
require_once("referer_common.ini");
require_once("workflow_common.ini");
require_once("application_workflow_select_box.ini");
require_once("application_workflow_common_class.php");
require_once("yui_calendar_util.ini");
require_once("timecard_bean.php");
require_once('Cmx/Model/SystemConfig.php');

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// ワークフロー権限のチェック
$checkauth = check_authority($session, 3, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

$obj = new application_workflow_common_class($con, $fname);


// トランザクション開始
pg_query($con, "begin");
//勤怠申請機能を使用するかファイルで確認
$kintai_flag = file_exists("kintai/common/mdb.php");
// 総合届け使用フラグ
$general_flag = file_exists("opt/general_flag");
// ----- メディアテック Start  -----
if (phpversion() >= '5.1' && $kintai_flag) {
    require_once 'kintai/common/mdb_wrapper.php';
    require_once 'kintai/common/mdb.php';
    MDBWrapper::init();
    $mdb = new MDB();
    $mdb->beginTransaction(); // トランザクション開始

    function get_module($id) {

        // テンプレートタイプを取得
        require_once 'kintai/common/workflow_common_util.php';
        require_once 'kintai/model/kintai_late_basic_model.php';
        require_once 'kintai/model/kintai_late_record.php';
        $template_type = WorkflowCommonUtil::get_template_type($id);

        $kintai_late_info = KintaiLateBasicModel::get($id); // 20140725
        
        $module = null;
        switch ($template_type) {
            case "ovtm": // 時間外勤務命令の場合
                if (module_check(WORKFLOW_OVTM_MODULE, WORKFLOW_OVTM_CLASS)) {
                    $class = WORKFLOW_OVTM_CLASS;
                    $module = new $class;
                }
                break;
            case "hol": // 休暇申請の場合
                //総合届け
                if ($general_flag) {
                    if (module_check(WORKFLOW_HOL_MODULE, WORKFLOW_HOL_CLASS)) {
                        $class = WORKFLOW_HOL_CLASS;
                        $module = new $class;
                    }
                }
                //休暇申請
                else {
                    if (module_check(WORKFLOW_HOL2_MODULE, WORKFLOW_HOL2_CLASS)) {
                        $class = WORKFLOW_HOL2_CLASS;
                        $module = new $class;
                    }
                }
                break;
            case "travel": // 旅行命令
                if (module_check(WORKFLOW_TRVL_MODULE, WORKFLOW_TRVL_CLASS)) {
                    $class = WORKFLOW_TRVL_CLASS;
                    $module = new $class;
                }
                break;
            case "late": // 遅刻早退の場合
            	if ($kintai_late_info->transfer == "1" or $kintai_late_info->transfer == "2"){
            		if (module_check(WORKFLOW_LATE2_MODULE, WORKFLOW_LATE2_CLASS)) {
            			$class = WORKFLOW_LATE2_CLASS;
            			$module = new $class;
            		}
            	}else{
	                if (module_check(WORKFLOW_LATE_MODULE, WORKFLOW_LATE_CLASS)) {
	                    $class = WORKFLOW_LATE_CLASS;
	                    $module = new $class;
	                }
            	}
                break;
        }
        return $module;
    }
}
function module_check($module, $class) {
    if (file_exists(WORKFLOW_KINTAI_DIR."/".$module)) {
        require_once WORKFLOW_KINTAI_DIR."/".$module;
        return class_exists($class);
    }
    return false;
}

// ----- メディアテック End  -----
switch ($mode) {
    case "DELETE":
        foreach ($apply_ids as $id) {
            $obj->update_delflg_all_apply($id, "t");
            if (phpversion() >= '5.1' && $kintai_flag) {
                $module = get_module($id);
                if (!empty($module)) {
                    $module->delete($id);
                }
            }
        }
        break;

    case "DELETE_CANCEL":
        foreach ($apply_ids as $id) {
            $obj->update_delflg_all_apply($id, "f");
        }
        break;

    case "DELETE_COMPLETE":
        foreach ($apply_ids as $id) {
            $obj->update_killflg_apply($id, "t");
        }
        break;

    case "DRAG":
        if($select_box == "DUST")
        {
            $obj->update_delflg_all_apply($del_apply_id, "t");
        }
        else
        {
            $obj->update_delflg_all_apply($del_apply_id, "f");
        }
        break;
}
// ----- メディアテック Start -----
// MDBトランザクションをコミット
if (phpversion() >= '5.1' && $kintai_flag) {
    $mdb->endTransaction();
}
// ----- メディアテック End -----
// トランザクションをコミット
pg_query($con, "commit");

$arr_wkfwcatemst = array();
$sel_wkfwcate = search_wkfwcatemst($con, $fname);
while($row_cate = pg_fetch_array($sel_wkfwcate)) {
    $tmp_wkfw_type = $row_cate["wkfw_type"];
    $tmp_wkfw_nm = $row_cate["wkfw_nm"];

    $arr_wkfwmst_tmp = array();
    $sel_wkfwmst = search_wkfwmst($con, $fname, $tmp_wkfw_type);
    while($row_wkfwmst = pg_fetch_array($sel_wkfwmst)) {
        $tmp_wkfw_id = $row_wkfwmst["wkfw_id"];
        $wkfw_title = $row_wkfwmst["wkfw_title"];

        $arr_wkfwmst_tmp[$tmp_wkfw_id] = $wkfw_title;
    }
    $arr_wkfwcatemst[$tmp_wkfw_type] = array("name" => $tmp_wkfw_nm, "wkfw" => $arr_wkfwmst_tmp);
}
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出勤務";
// 「CoMedix」というカテゴリ追加
$arr_comedix_tmp["1"] = "議事録公開申請（委員会・WG）";
$arr_comedix_tmp["2"] = "残業申請";
$arr_comedix_tmp["3"] = "勤務時間修正申請";
$arr_comedix_tmp["4"] = "{$ret_str}申請";
$arr_wkfwcatemst["0"] = array("name" => "CoMedix", "wkfw" => $arr_comedix_tmp);

// 遷移元の設定
if ($workflow != "") {
    set_referer($con, $session, "workflow", $workflow, $fname);
    $referer = $workflow;
} else {
    $referer = get_referer($con, $session, "workflow", $fname);
}

$conf = new Cmx_SystemConfig();
$title_label = ($conf->get('apply.view.title_label') == "2") ? "標題" : "表題";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー | 申請参照一覧</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>

<?php
// 外部ファイルを読み込む
write_yui_calendar_use_file_read_0_12_2();

// カレンダー作成、関数出力
write_yui_calendar_script2(2);
?>

<script type="text/javascript">

//ロード時の処理
function init_action()
{
    //ドラッグアンドドロップを初期設定
    constructDragDrop();
    cateOnChange();
}


function search_apply()
{
    document.wkfw.action = "<?=$fname?>";
    document.wkfw.select_box.value = '';
    document.wkfw.selected_cate.value = '';
    document.wkfw.selected_folder.value = '';
    document.wkfw.selected_wkfw_id.value = '';
    document.wkfw.submit();
}

function cateOnChange() {

    var obj_cate = document.wkfw.search_category;
    var obj_wkfw = document.wkfw.search_workflow;

    var cate_id = getSelectedValue(obj_cate);
    // 申請書セレクトボックスのオプションを全削除
    deleteAllOptions(obj_wkfw);

    // 申請書セレクトボックスのオプションを作成
    addOption(obj_wkfw, '-', 'すべて');

<?php foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) { ?>
    if (cate_id == '<?php echo $tmp_cate_id; ?>') {
    <?php foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm)
       {
           $tmp_wkfw_nm = h($tmp_wkfw_nm, ENT_QUOTES);
           $tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
           $tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);
    ?>
        addOption(obj_wkfw, '<?=$tmp_wkfw_id?>', '<?=$tmp_wkfw_nm?>',  '<?=$search_workflow?>');
    <?php } ?>
    }
<?php } ?>
}



function getSelectedValue(sel) {
    return sel.options[sel.selectedIndex].value;
}


function deleteAllOptions(box) {
    for (var i = box.length - 1; i >= 0; i--) {
        box.options[i] = null;
    }
}

function addOption(box, value, text, selected) {

    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;
    if (selected == value) {
        opt.selected = true;
    }
    box.options[box.length] = opt;
    try {box.style.fontSize = 'auto';} catch (e) {}
    box.style.overflow = 'auto';
}

function highlightCells(tr) {
    changeCellsColor(tr, '#ffff66');
}

function dehighlightCells(tr) {
    changeCellsColor(tr, '');
}

function changeCellsColor(tr, color) {
    var cells = tr.getElementsByTagName('td');
    for (var i = 0, j = cells.length; i < j; i++) {
        cells[i].style.backgroundColor = color;
    }
}

function stopPropagation(e) {
    if (e.stopPropagation) {
        e.stopPropagation();
    } else {
        event.cancelBubble = true;
    }
}
</script>

<!-- 2015.07.01 isewaki もともと show_workflow_all_apply_list.ini の show_apply_folder関数にあったが、IE8でエラーになるため下記に移動 -->
<script type="text/javascript" src="js/yui_0.12.2/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/event/event-min.js"></script>
<link rel="stylesheet" type="text/css" href="js/yui_0.12.2/build/treeview/assets/folders/tree.css">
<script type="text/javascript" src="js/yui_0.12.2/build/treeview/treeview-min.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/dom/dom-min.js"></script>
<script type="text/javascript" src="js/yui/3.10.3/build/yui/yui-min.js"></script>
<!-- /2015.07.01 isewaki -->

<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/workflow/workflow.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init_action();initcal();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<?php if ($referer == "2") { ?>
<table id="header">
<tr>
<td class="icon"><a href="workflow_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><a href="workflow_menu.php?session=<?=$session?>"><b>ワークフロー</b></a></td>
</tr>
</table>
<?php } else { ?>
<table id="header">
<tr>
<td class="icon"><a href="application_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b08.gif" width="32" height="32" border="0" alt="決裁・申請"></a></td>
<td>
    <a href="application_menu.php?session=<?php echo($session); ?>"><b>決裁・申請</b></a> &gt;
    <a href="workflow_menu.php?session=<?php echo($session); ?>"><b>管理画面</b></a> &gt;
    <a href="workflow_all_apply_list.php?session=<?php echo($session); ?>"><b>申請一覧</b></a>
</td>
<td class="kanri"><a href="application_menu.php?session=<?php echo($session); ?>"><b>ユーザ画面へ</b></a></td>
</tr>
</table>
<?php } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="120"><?php show_wkfw_sidemenu($session);?></td>
<td width="5"><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top">
<?php show_wkfw_menuitem($session, $fname, ""); ?>
<form name="wkfw" action="#" method="post">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="20%"></td>
<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td width="80%">

<table width="100%" border="0" cellspacing="1" cellpadding="1" class="list">
<tr>
<td>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="block">
<tr>
<td width="10%" bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font></td>
<td width="50%" ><select name="search_category" onchange="cateOnChange();"><?php show_cate_options($arr_wkfwcatemst, $search_category); ?></select></td>
<td width="10%" bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請書名</font>
<td width="30%" ><select name="search_workflow"></select></td>
</tr>

<tr>
<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo($title_label); ?></font></td>
<td><input type="text" size="40" maxlength="40" name="search_apply_title" value="<?=h($search_apply_title)?>"></td>
<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者</font></td>
<td><input type="text" size="30" maxlength="30" name="search_apply_emp_nm" value="<?=h($search_apply_emp_nm)?>"></td>
</tr>

<tr>
<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
<td>

<table border="0" cellspacing="0" cellpadding="0" class="block_in">
<tr><td>
<select id="date_y1" name="date_y1"><?php show_select_years(10, $date_y1, true); ?></select>/<select id="date_m1" name="date_m1"><?php show_select_months($date_m1, true); ?></select>/<select id="date_d1" name="date_d1"><?php show_select_days($date_d1, true); ?></select>
</td><td>
    <img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>

<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
</font>
</td><td>
&nbsp;〜<select id="date_y2" name="date_y2"><?php show_select_years(10, $date_y2, true); ?></select>/<select id="date_m2" name="date_m2"><?php show_select_months($date_m2, true); ?></select>/<select id="date_d2" name="date_d2"><?php show_select_days($date_d2, true); ?></select>

</td><td>
<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal2()"/><br>
<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
<div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
</font>
</td></tr>
</table>

</td>
<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請状況</font></td>
<td>
<select name="search_apply_stat"><?php show_apply_stat_options($search_apply_stat); ?></select>
</td>
</tr>

<tr>
<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属</font></td>
<td colspan="3">
<?php show_post_select_box($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room); ?>
</td>
</tr>

<tr>
<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文字列検索</font></td>
<td><input type="text" size="50" name="search_apply_content" value="<?=h($search_apply_content)?>"></td>
<td bgcolor="#fefcdf"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請番号</font></td>
<td>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr>
<td><input type="text" size="20" maxlength="20" name="search_apply_no" value="<?=h($search_apply_no)?>"></td>
<td align="right"><input type="button" value="検索" onclick="search_apply();"></td>
</tr>
</table>
</td>
</tr>
</table>


</td>
</tr>
</table>

</td>
</tr>

<tr><td colspan="3"><img src="img/spacer.gif" width="1" height="3" alt=""></td></tr>

<?php
$arr_condition = array(
    "wkfw_type" => $search_category,
    "wkfw_id" => $search_workflow,
    "apply_title" => $search_apply_title,
    "apply_emp_nm" => $search_apply_emp_nm,
    "apply_yyyy_from" => $date_y1,
    "apply_mm_from" => $date_m1,
    "apply_dd_from" => $date_d1,
    "apply_yyyy_to" => $date_y2,
    "apply_mm_to" => $date_m2,
    "apply_dd_to" => $date_d2,
    "apply_stat" => $search_apply_stat,
    "class" => $search_emp_class,
    "attribute" => $search_emp_attribute,
    "dept" => $search_emp_dept,
    "room" => $search_emp_room,
    "apply_content" => $search_apply_content,
    "apply_no" => $search_apply_no
);

/*
if($select_box == "")
{
    $select_box = "ALL";
}
*/

show_all_apply_list($con, $session, $fname, $select_box, $page, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition);
?>
</table>
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="select_box" id="select_box" value="<?php echo($select_box); ?>">
<input type="hidden" name="selected_cate" id="selected_cate" value="<?php echo($selected_cate); ?>">
<input type="hidden" name="selected_folder" id="selected_folder" value="<?php echo($selected_folder); ?>">
<input type="hidden" name="selected_wkfw_id" id="selected_wkfw_id" value="<?php echo($selected_wkfw_id); ?>">

</form>
</td>
</tr>
</table>

</td>
</tr>
</table>

<form id="csvform" name="csvform" action="workflow_all_apply_csv.php" method="post" target="download">
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="select_box" value="<?php echo($select_box); ?>">
<input type="hidden" name="selected_cate" value="<?php echo($selected_cate); ?>">
<input type="hidden" name="selected_folder" value="<?php echo($selected_folder); ?>">
<input type="hidden" name="selected_wkfw_id" value="<?php echo($selected_wkfw_id); ?>">
<?php foreach ($arr_condition as $k => $v) { ?>
<input type="hidden" name="<?php echo($k); ?>" value="<?php echo($v); ?>">
<?php } ?>
<input type="hidden" name="mode" value="">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
</body>
</html>
<?php
// ワークフロー情報取得
function search_wkfwmst($con, $fname, $wkfw_type) {

    $sql = "select * from wkfwmst";
    $cond="where wkfw_type='$wkfw_type' and wkfw_del_flg = 'f' order by wkfw_id asc";
    $sel=select_from_table($con,$sql,$cond,$fname);
    if($sel==0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    return $sel;
}

// カテゴリ名称を取得
function search_wkfwcatemst($con, $fname) {

    $sql = "select * from wkfwcatemst";
    $cond = "where wkfwcate_del_flg = 'f' order by wkfw_type";
    $sel=select_from_table($con,$sql,$cond,$fname);
    if($sel==0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    return $sel;
}

// カテゴリオプションを出力
function show_cate_options($arr_cate, $cate) {
    echo("<option value=\"-\">すべて");
    foreach ($arr_cate as $tmp_cate_id => $arr) {
        $tmp_cate_name = $arr["name"];
        echo("<option value=\"$tmp_cate_id\"");
        if($cate != "" && $cate != '-') {
            if ($cate == $tmp_cate_id) {
                echo(" selected");
            }
        }
        echo(">$tmp_cate_name\n");
    }
}

// 申請状況オプションを出力
function show_apply_stat_options($stat) {

    $arr_apply_stat_nm = array("未承認", "承認確定", "否認", "差戻し");
    $arr_apply_stat_id = array("0", "1", "2", "3");

    echo("<option value=\"-\">すべて");
    for($i=0;$i<count($arr_apply_stat_nm);$i++) {

        echo("<option value=\"$arr_apply_stat_id[$i]\"");
        if($stat == $arr_apply_stat_id[$i]) {
            echo(" selected");
        }
        echo(">$arr_apply_stat_nm[$i]\n");
    }
}
pg_close($con);
