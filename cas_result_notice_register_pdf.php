<?php

ob_start();
ini_set("max_execution_time", 0);

require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once('fpdf153/mbfpdf.php');
require_once("cas_common.ini");
require_once("get_cas_title_name.ini");
require_once("cas_application_workflow_common_class.php");

ob_end_clean();

// 定数
// ヘッダー行
define("HEADER_HEIGHT",16);			// 高さ
define("HEADER_FONT",16);			// フォント
// 明細行
define("STANDARD_HEIGHT",6);		// 高さ
define("STANDARD_FONT",12);			// フォント

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($_POST['session'],$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//DBコネクション取得
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cas_application_workflow_common_class($con, $fname);
$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
$short_wkfw_name = $arr_apply_wkfwmst[0]["short_wkfw_name"];

// PDF出力
$pdf = new MBFPDF('P', 'mm', 'A4');
$pdf->SetAutoPageBreak(true, 5);
$pdf->AddMBFont(MINCHO,'EUC-JP');
$pdf->AddMBFont(GOTHIC,'EUC-JP');

$pdf->Open();

$pdf->AddPage();

$pdf->setY(12);

// タイトル出力
$pdf->SetFont(GOTHIC,'',HEADER_FONT);
$write_wk = get_cas_title_name();
$write_wk.= "結果通知";
$pdf->Cell(0,HEADER_HEIGHT,$write_wk,0,1,'L');

// チャレンジレベル出力
$pdf->SetFont(GOTHIC,'B',STANDARD_FONT);
$write_wk = "チャレンジレベル";
$write_wk.= output_level($_POST['charange_level']);
$write_wk.= output_nurse_type($_POST['nurse_type']);
$pdf->Cell(0,STANDARD_HEIGHT,$write_wk,0,1,'L');

// 志願者・所属出力
$pdf->SetFont(GOTHIC,'',STANDARD_FONT);
$write_wk = "志願者：".$_POST['emp_full_nm'];
$pdf->Cell(50,STANDARD_HEIGHT,$write_wk,0,0,'L');
$write_wk = "所属：".$_POST['class'];
$pdf->Cell(160,STANDARD_HEIGHT,$write_wk,0,1,'L');

$write_wk = "あなたのキャリアアップチャレンジは、看護の質向上に貢献するものであり、専門職としての姿";
$pdf->Cell(0,STANDARD_HEIGHT,$write_wk,0,1,'L');

$write_wk = "そのものであります。心より経緯を評すとともに結果について報告させていただきます。";
$pdf->Cell(0,STANDARD_HEIGHT,$write_wk,0,1,'L');

// 空白行出力
output_blunk($pdf);

$pdf->SetFont(GOTHIC,'B',STANDARD_FONT);
$write_wk = "1.結果";
$pdf->Cell(0,STANDARD_HEIGHT,$write_wk,0,1,'L');

$pdf->SetFont(GOTHIC,'',STANDARD_FONT);
$write_wk = "達成している項目 ： 優れた能力でさらに磨いて頂きたい能力(チェック)";
$pdf->Cell(0,STANDARD_HEIGHT,$write_wk,0,1,'L');

// 達成側結果出力
output_no($pdf,$_POST['charange_level'],'achieved',$_POST['nurse_type'],$short_wkfw_name);

// 空白行出力
output_blunk($pdf);

$write_wk = "達成していない項目 ： 次のステップアップの課題として磨いて頂きたい能力(チェック)";
$pdf->Cell(0,STANDARD_HEIGHT,$write_wk,0,1,'L');

// 未達成側結果出力
output_no($pdf,$_POST['charange_level'],'unachieved',$_POST['nurse_type'],$short_wkfw_name);

// 空白行出力
output_blunk($pdf);

$pdf->SetFont(GOTHIC,'B',STANDARD_FONT);
$write_wk = "2.認定レベル/コメント(該当のところにチェック)";
$pdf->Cell(0,STANDARD_HEIGHT,$write_wk,0,1,'L');

// レベル認定可・不可出力
$pdf->SetFont(GOTHIC,'',STANDARD_FONT);
$write_wk = output_radio('radio_level_recognize','1');
$write_wk.= "レベル認定可 ";
$write_wk.= output_radio('radio_level_recognize','2');
$write_wk.= "レベル認定不可";
$pdf->Cell(0,STANDARD_HEIGHT,$write_wk,0,1,'L');

// 認定レベル・区分出力(レベル認定可のみ)
if ($_POST['radio_level_recognize'] == "1")
{
	$write_wk = "レベル";
	$write_wk.= output_level($_POST['level']);
	$write_wk.= " 区分";
	$write_wk.= $_POST['level_div'];
} else {
	$write_wk = "";
}
$pdf->Cell(0,STANDARD_HEIGHT,$write_wk,0,1,'L');

// 再チャレンジ(半年)出力
$write_wk = output_check('chk_2_1');
$write_wk.= "半年が経過した後、目標達成していない項目のみ再チャレンジする。";
$pdf->Cell(0,STANDARD_HEIGHT,$write_wk,0,1,'L');

// 再チャレンジ(１年)出力
$write_wk = output_check('chk_2_2');
$write_wk.= "1年が経過した後、全ての項目を再チャレンジする。";
$pdf->Cell(0,STANDARD_HEIGHT,$write_wk,0,1,'L');

// コメント出力（１行88バイト）
$arr_text = output_text($_POST['chk_2_3_text'],88);
foreach($arr_text as $write_wk)
{
	$pdf->Cell(0,STANDARD_HEIGHT,$write_wk,0,1,'L');
}

// 空白行出力
output_blunk($pdf);

$pdf->SetFont(GOTHIC,'B',STANDARD_FONT);
$write_wk = "3.認定結果についての疑問のある場合";
$pdf->Cell(0,STANDARD_HEIGHT,$write_wk,0,1,'L');

$pdf->SetFont(GOTHIC,'',STANDARD_FONT);
$write_wk = "疑問の生じた評価項目とその理由を文書にし、評価用紙一式と共に看護管理部(認定委員会)";
$pdf->Cell(0,STANDARD_HEIGHT,$write_wk,0,1,'L');

$write_wk = "へ提出して下さい。";
$pdf->Cell(0,STANDARD_HEIGHT,$write_wk,0,1,'L');

// 空白行出力
output_blunk($pdf);

// 提出期限出力(期限入力時のみ)
if ($_POST['date_y1'] != "" || $_POST['date_m1'] != "-" || $_POST['date_d1'] != "-")
{
	$write_wk = "提出期限：";
	if ($_POST['date_y1'] != "")
	{
		$write_wk.= $_POST['date_y1'];
		$write_wk.= "年";
	}
	if ($_POST['date_m1'] != "-")
	{
		$write_wk.= $_POST['date_m1'];
		$write_wk.= "月";
	}
	if ($_POST['date_d1'] != "-")
	{
		$write_wk.= $_POST['date_d1'];
		$write_wk.= "日";
	}
	$pdf->Cell(0,STANDARD_HEIGHT,$write_wk,0,1,'L');
}

pg_close($con);
$pdf->Output();

die;

/*************************************************************************/
// 結果出力
// @param &$pdf				PDF作成クラス
// @param $level			チャレンジレベル
// @param $achieved			達成(achieved)・未達成(unachieved)
// @param $nurse_type		看護区分
// @param $short_wkfw_name	管理CD
/*************************************************************************/
function output_no(&$pdf,$level,$achieved,$nurse_type,$short_wkfw_name)
{
	// プロセス名
	$process_name = array('ナーシングプロセス'
						 ,'教育能力/自己学習能力'
						 ,'リーダーシップ能力'
						 ,'経験年数/経験領域');

	// レベル・看護区分ごとにNoが異なるため、設定
	// レベルII
	if ($level == 2) {

		// 病棟
		if ($nurse_type == 1) {

			$no_all = array(array(1,3)
							,array(5,6,8,9)
							,array(10)
							,array(12));

		// 外来
		} else if ($nurse_type == 2) {

			$no_all = array(array(1,3,31)
							,array(5,6,8,9)
							,array(10,32)
							,array(12,33));

		// 外来（准看護師）
		} else if ($nurse_type == 3) {

			$no_all = array(array(1,3,31)
							,array(5,6,8,9)
							,array(10,32)
							,array(12,33));

		}

	// レベルIII
	} else if ($level == 3) {

		// 病棟
		if ($nurse_type == 1) {
			if ($short_wkfw_name == "c303") {
				$no_all = array(array(4)
								,array(8,9)
								,array(11));
			} else {
				$no_all = array(array(2,4)
								,array(5,7,8,9)
								,array(11)
								,array(12));
			}

		// 外来
		} else if ($nurse_type == 2) {

			$no_all = array(array(2,4)
							,array(5,7,8,9)
							,array(11,32)
							,array(12,33));

		// 外来（准看護師）
		} else if ($nurse_type == 3) {

			$no_all = array(array(1,3,31)
							,array(5,7,8,9)
							,array(11,32)
							,array(12,33));

		}

	}

	for ($i=0; $i < count($no_all); $i++)
	{
		$write_wk = $process_name[$i];
		$pdf->Cell(50,STANDARD_HEIGHT,$write_wk,0,0,'L');

		$write_wk = "：";
		$pdf->Cell(5,STANDARD_HEIGHT,$write_wk,0,0,'C');

		$no_row = $no_all[$i];
		for ($j=0; $j < count($no_row); $j++)
		{
			$no = $no_row[$j];
			$write_wk = output_check('chk_'.$achieved.'_no'.$no);
			$write_wk.= "No.";
			$write_wk.= $no;
			if ($j == count($no_row) - 1)
			{
				$row_break = 1;
			} else {
				$row_break = 0;
			}
			$pdf->Cell(20,STANDARD_HEIGHT,$write_wk,0,$row_break,'L');
		}
	}

}

/*************************************************************************/
// チェックボックス出力
// @param $input_id	項目名
/*************************************************************************/
function output_check($input_id)
{
	if ($_POST[$input_id])
	{
		return "■";
	} else {
		return "□";
	}
}

/*************************************************************************/
// ラジオボタン出力
// @param $input_id		項目名
// @param $input_value	選択時の値
/*************************************************************************/
function output_radio($input_id,$input_value)
{
	if ($_POST[$input_id] == $input_value)
	{
		return "●";
	} else {
		return "○";
	}
}

/*************************************************************************/
// 空白行出力
// @param &$pdf	PDF作成クラス
/*************************************************************************/
function output_blunk(&$pdf)
{
	$pdf->Cell(0,STANDARD_HEIGHT,'',0,1);
}

/*************************************************************************/
// レベル表示
// @param $level	レベル
/*************************************************************************/
function output_level($level)
{
	$res = "";
	switch ($level)
	{
		case 1:
			$res = "I";
			break;
		case 2:
			$res = "II";
			break;
		case 3:
			$res = "III";
			break;
	}
	return $res;
}

/*************************************************************************/
// 看護区分表示
// @param $nurse_type	看護区分
/*************************************************************************/
function output_nurse_type($nurse_type)
{
	$res = "";
	switch ($nurse_type)
	{
		case 1:
			$res = "";
			break;
		case 2:
			$res = "外来";
			break;
		case 3:
			$res = "外来（准看護師）";
			break;
	}
	return $res;
}

/*************************************************************************/
// テキスト項目表示
// @param $value		値
// @param $row_length	１行毎のバイト数
/*************************************************************************/
function output_text($value,$row_length)
{
	$i = 0;
	$arr = array('');

	while (strlen($value) > 0)
	{
		if (strlen($value) > $row_length)
		{
			// 改行する際にマルチバイト文字が行を跨ぐ場合、跨いだ文字は次行に出力する
			$arr[$i] = substr($value,0,$row_length + 1);
			$arr[$i] = mb_substr($arr[$i],0,mb_strlen($arr[$i]) - 1);
			$value = substr($value,strlen($arr[$i]));
		} else {
			$arr[$i] = $value;
			$value = "";
		}
		$i++;
	}

	return $arr;

}

?>