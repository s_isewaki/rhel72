<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="config_other.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="multi_login_alert" value="<? echo($multi_login_alert); ?>">
<input type="hidden" name="weak_pwd_flg" value="<? echo($weak_pwd_flg); ?>">
<input type="hidden" name="pass_expire_days" value="<? echo($pass_expire_days); ?>">
<input type="hidden" name="login_autocomplete" value="<? echo($login_autocomplete); ?>">
<input type="hidden" name="hide_passwd" value="<? echo($hide_passwd); ?>">
<input type="hidden" name="pass_length" value="<? echo($pass_length); ?>">
<input type="hidden" name="timeout_minutes" value="<? echo($timeout_minutes); ?>">
<input type="hidden" name="lib_count_flg" value="<? echo($lib_count_flg); ?>">
<input type="hidden" name="lib_dragdrop_flg" value="<? echo($lib_dragdrop_flg); ?>">
<input type="hidden" name="popup_news" value="<? echo($popup_news); ?>">
<input type="hidden" name="use_email" value="<? echo($use_email); ?>">
<input type="hidden" name="use_web" value="<? echo($use_web); ?>">
<input type="hidden" name="use_cyrus" value="<? echo($use_cyrus); ?>">
<input type="hidden" name="webmail_from_order1" value="<? echo($webmail_from_order1); ?>">
<input type="hidden" name="webmail_from_order2" value="<? echo($webmail_from_order2); ?>">
<input type="hidden" name="webmail_from_order3" value="<? echo($webmail_from_order3); ?>">
<input type="hidden" name="use_flash_logo" value="<? echo($use_flash_logo); ?>">
<input type="hidden" name="kview_url" value="<? echo($kview_url); ?>">
<input type="hidden" name="user_belong_change" value="<? echo($user_belong_change); ?>">
<input type="hidden" name="user_job_change" value="<? echo($user_job_change); ?>">
<input type="hidden" name="user_st_change" value="<? echo($user_st_change); ?>">
<input type="hidden" name="schedule.alarm" value="<? echo($schedule_alarm); ?>">
<input type="hidden" name="schedule.alarm.start.h" value="<? echo($schedule_alarm_start_h); ?>">
<input type="hidden" name="schedule.alarm.start.m" value="<? echo($schedule_alarm_start_m); ?>">
<input type="hidden" name="schedule.alarm.end.h" value="<? echo($schedule_alarm_end_h); ?>">
<input type="hidden" name="schedule.alarm.end.m" value="<? echo($schedule_alarm_end_m); ?>">
<input type="hidden" name="security_client_auth" value="<? echo($security_client_auth); ?>">
<input type="hidden" name="password_first_time" value="<? echo($password_first_time); ?>">
<input type="hidden" name="back" value="t">
</form>
<?php
require_once("about_comedix.php");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$check_auth = check_authority($session, 20, $fname);
if ($check_auth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// マスク設定を取得
$sql = "select hide_passwd from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$cur_hide_passwd = pg_fetch_result($sel, 0, "hide_passwd");

// 入力チェック
if ($pass_expire_days == "") {
    echo("<script type=\"text/javascript\">alert('パスワード変更期限が入力されていません。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (preg_match("/^\d{1,3}$/", $pass_expire_days) == 0) {
    echo("<script type=\"text/javascript\">alert('パスワード変更期限を数字で入力してください。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (preg_match("/^\d{1,3}$/", $pass_length) == 0) {
    echo("<script type=\"text/javascript\">alert('パスワードの長さを数字で入力してください。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if ($pass_length > 999) {
    echo("<script type=\"text/javascript\">alert('パスワードの長さは999以下の数字で入力してください。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (trim($timeout_minutes) == "") {
    $timeout_minutes = "0";
}
else {
    if (preg_match("/^\d{1,3}$/", $timeout_minutes) == 0) {
        echo("<script type=\"text/javascript\">alert('セッションタイムアウト時間は数字で入力してください。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
    if ($timeout_minutes > 999) {
        echo("<script type=\"text/javascript\">alert('セッションタイムアウト時間は999以下の数字で入力してください。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}
if ($schedule_alarm === "1") {
    if (empty($schedule_alarm_start_h) and empty($schedule_alarm_start_m) and empty($schedule_alarm_end_h) and empty($schedule_alarm_end_m)) {
        echo("<script type=\"text/javascript\">alert('スケジュールアラーム設定可能時間を入力してください。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
    if (preg_match("/\D/", $schedule_alarm_start_h) or preg_match("/\D/", $schedule_alarm_start_m) or preg_match("/\D/", $schedule_alarm_end_h) or preg_match("/\D/", $schedule_alarm_end_m)) {
        echo("<script type=\"text/javascript\">alert('スケジュールアラーム設定可能時間は数字で入力してください。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }

    $schedule_alarm_start = $schedule_alarm_start_h * 60 + $schedule_alarm_start_m;
    $schedule_alarm_end = $schedule_alarm_end_h * 60 + $schedule_alarm_end_m;
    if ($schedule_alarm_start > $schedule_alarm_end) {
        $schedule_alarm_start = $schedule_alarm_end_h * 60 + $schedule_alarm_end_m;
        $schedule_alarm_end = $schedule_alarm_start_h * 60 + $schedule_alarm_start_m;
    }
}

if ($cur_hide_passwd == "t" && $hide_passwd == "f") {
    $sql = "select prf_org_cd from profile";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $org_cd = (pg_num_rows($sel) == 1) ? pg_fetch_result($sel, 0, "prf_org_cd") : "";
    if ($org_cd == "") {
        echo("<script type=\"text/javascript\">alert('組織プロフィール未登録のため、ライセンスキー番号が確認できません。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }

    $sql = "select * from license";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    require_once("class/Cmx/Core2/C2App.php");
    $c2app = c2GetC2App();
    $licenseRow = pg_fetch_assoc($sel);
    $licenseRow["prf_org_cd"] = $org_cd;
    $licenseRow["today_year"] = date("Y");
    $licenseRow["today_month"] = date("m");
    $obj = $c2app->genLicenseKey($licenseRow, 1);
    $gen_license_key = $obj["gen_license_key"];
    $errmsg = $obj["errmsg"];
    if ($errmsg) {
        echo("<script type=\"text/javascript\">alert('" . $errmsg . "');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
    if ($license_cd != $gen_license_key) {
        echo("<script type=\"text/javascript\">alert('ライセンスキー番号が不正です。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}

if ($webmail_from_order2 == $webmail_from_order1) {
    echo("<script type=\"text/javascript\">alert('差出人表示順2を正しく選択してください。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if ($webmail_from_order3 == $webmail_from_order1 || ($webmail_from_order3 != "" && $webmail_from_order3 == $webmail_from_order2)) {
    echo("<script type=\"text/javascript\">alert('差出人表示順3を正しく選択してください。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (strlen($kview_url) > 500) {
    echo("<script type=\"text/javascript\">alert('カルテビューワーのURLが長すぎます。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}

// 入力値の編集
if ($webmail_from_order2 == "") {
    $webmail_from_order2 = null;
}
if ($webmail_from_order3 == "") {
    $webmail_from_order3 = null;
}

// トランザクションを開始
pg_query($con, "begin");

// 環境設定情報を更新
$sql = "update config set";
$set = array("multi_login_alert", "weak_pwd_flg", "popup_news", "use_web", "use_email", "pass_expire_days", "webmail_from_order1", "webmail_from_order2", "webmail_from_order3", "use_cyrus", "hide_passwd", "login_autocomplete", "use_flash_logo", "kview_url", "pass_length", "user_belong_change", "user_st_change", "user_job_change");
$setvalue = array($multi_login_alert, $weak_pwd_flg, $popup_news, $use_web, $use_email, $pass_expire_days, $webmail_from_order1, $webmail_from_order2, $webmail_from_order3, $use_cyrus, $hide_passwd, $login_autocomplete, $use_flash_logo, $kview_url, $pass_length, $user_belong_change, $user_st_change, $user_job_change);
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// WEB接続をしない場合
if ($use_web == "f") {

    // オプションのWICを「表示しない」に更新
    $sql = "update option set";
    $set = array("top_wic_flg");
    $setvalue = array("f");
    $cond = "";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

// 文書管理の環境設定情報を取得
$sql = "select section_archive_use, project_archive_use, private_archive_use, lock_show_flg, real_name_flg, show_login_flg, stick_folder, show_tree_flg, pjt_private_flg, show_newlist_flg, all_createfolder_flg from libconfig";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
if (pg_num_rows($sel) > 0) {
    $section_archive_use = pg_fetch_result($sel, 0, "section_archive_use");
    $project_archive_use = pg_fetch_result($sel, 0, "project_archive_use");
    $private_archive_use = pg_fetch_result($sel, 0, "private_archive_use");
    $lock_show_flg = pg_fetch_result($sel, 0, "lock_show_flg");
    $real_name_flg = pg_fetch_result($sel, 0, "real_name_flg");
    $show_login_flg = pg_fetch_result($sel, 0, "show_login_flg");
    $stick_folder = pg_fetch_result($sel, 0, "stick_folder");
    $show_tree_flg = pg_fetch_result($sel, 0, "show_tree_flg");
    $pjt_private_flg = pg_fetch_result($sel, 0, "pjt_private_flg");
    $show_newlist_flg = pg_fetch_result($sel, 0, "show_newlist_flg");
    $all_createfolder_flg = pg_fetch_result($sel, 0, "all_createfolder_flg");
}
else {
    $section_archive_use = "t";
    $project_archive_use = "t";
    $private_archive_use = "t";
    $lock_show_flg = "t";
    $real_name_flg = "t";
    $show_login_flg = "t";
    $stick_folder = "t";
    $show_tree_flg = "t";
    $pjt_private_flg = "f";
    $show_newlist_flg = "t";
    $all_createfolder_flg = "t";
}

// 文書管理の環境設定情報をDELETE〜INSERT
$sql = "delete from libconfig";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$sql = "insert into libconfig (section_archive_use, project_archive_use, private_archive_use, lock_show_flg, real_name_flg, show_login_flg, stick_folder, show_tree_flg, count_flg, dragdrop_flg, pjt_private_flg, show_newlist_flg, all_createfolder_flg) values (";
$content = array($section_archive_use, $project_archive_use, $private_archive_use, $lock_show_flg, $real_name_flg, $show_login_flg, $stick_folder, $show_tree_flg, $lib_count_flg, $lib_dragdrop_flg, $pjt_private_flg, $show_newlist_flg, $all_createfolder_flg);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// SystemConfig
$conf = new Cmx_SystemConfig();
$conf->set('schedule.alarm', $schedule_alarm);
$conf->set('schedule.alarm.start', $schedule_alarm_start);
$conf->set('schedule.alarm.end', $schedule_alarm_end);
$conf->set('security.client_auth', $security_client_auth);
$conf->set('config.timeout_minutes', strval(intval($timeout_minutes)));
$conf->set('password.first_time', $password_first_time);

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'config_other.php?session=$session';</script>");

// ユーザ数コードからユーザ数を取得
function get_user_count_by_code($code, $max_user_count = "NONE")
{
    // 無制限の場合
    if ($code == "FULL") {

        // 登録可能ユーザが求められている場合は10000に仮設定
        if ($max_user_count == "NONE") {
            return 10000;

            // それ以外の場合は登録可能ユーザ数を返す
        }
        else {
            return $max_user_count;
        }
    }

    // 未選択の場合、nullを返す
    if ($code === "") {
        return null;
    }

    return intval($code);
}
?>
</body>
