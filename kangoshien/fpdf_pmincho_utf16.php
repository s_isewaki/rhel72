<?php
require('fpdf153/fpdf.php');

$SJIS_widths = array( ' '=>278,'!'=>299,'"'=>353,'#'=>614,'$'=>614,'%'=>721,'&'=>735,'\''=>216,
    '('=>323,')'=>323,'*'=>449,'+'=>529,','=>219,'-'=>306,'.'=>219,'/'=>453,'0'=>614,'1'=>614,
    '2'=>614,'3'=>614,'4'=>614,'5'=>614,'6'=>614,'7'=>614,'8'=>614,'9'=>614,':'=>519,';'=>219,
    '<'=>529,'='=>529,'>'=>529,'?'=>486,'@'=>744,'A'=>646,'B'=>604,'C'=>617,'D'=>681,'E'=>567,
    'F'=>537,'G'=>647,'H'=>738,'I'=>320,'J'=>433,'K'=>637,'L'=>566,'M'=>904,'N'=>710,'O'=>716,
    'P'=>605,'Q'=>716,'R'=>623,'S'=>517,'T'=>601,'U'=>690,'V'=>668,'W'=>990,'X'=>681,'Y'=>634,
    'Z'=>578,'['=>516,'\\'=>614,']'=>516,'^'=>529,'_'=>500,'`'=>387,'a'=>509,'b'=>566,'c'=>478,
    'd'=>565,'e'=>503,'f'=>337,'g'=>549,'h'=>580,'i'=>275,'j'=>266,'k'=>544,'l'=>276,'m'=>854,
    'n'=>579,'o'=>550,'p'=>578,'q'=>566,'r'=>410,'s'=>444,'t'=>340,'u'=>575,'v'=>512,'w'=>760,
    'x'=>503,'y'=>529,'z'=>453,'{'=>326,'|'=>380,'}'=>326,'~'=>387);

class FPDF_PMINCHO_UTF16 extends FPDF {


function AddUniJISFont() {
    $name = 'MS-PMincho';
    $family = 'FontFamily1';
    $cw=$GLOBALS['SJIS_widths'];
    $CMap='UniJIS-UCS2-H';
    $registry=array('ordering'=>'Japan1','supplement'=>2);
    $this->AddCIDFont($family,'',$name,$cw,$CMap,$registry);
}


function AddCIDFont($family, $style, $name, $cw, $CMap, $registry) {
    $fontkey=strtolower($family).strtoupper($style);
    if(isset($this->fonts[$fontkey])) $this->Error("CID font already added: $family $style");
    $i=count($this->fonts)+1;
    $this->fonts[$fontkey]=array('i'=>$i,'type'=>'Type0','name'=>$name,'up'=>-120,'ut'=>40,'cw'=>$cw,'CMap'=>$CMap,'registry'=>$registry);
}


// オーバーライド
function GetStringWidth($uni) {
    $ww=0;
    $cw=&$this->CurrentFont['cw'];
    $nb= strlen($uni);
    $i=0;
    while($i<$nb) {
        $ucode = ord($uni[$i])*256 + ord($uni[$i+1]);
        if     ($ucode<0x8F) $ww += $cw[$uni[$i+1]]; // ASCII
        elseif ($ucode>=0xFF61 && $ucode<=0xFF9F) $ww += 500; // 半角ｶﾅ
        elseif ($ucode>=0xD800 && $ucode<=0xDC00) { $ww += 1000; $i+=4; continue; } // サロゲートペアの前半 (0x55296 - 0x56320)
        else $ww += 1000; // Full-width character
        $i+=2;
    }
    return $ww*$this->FontSize/1000;
}


function GetCharArray($uni) {
    $l=0;
    $nb= strlen($uni);
    $i=0;
    $out = array();
    while($i<$nb) {
        $ucode = ord($uni[$i])*256 + ord($uni[$i+1]);
        if ($ucode>=0xD800 && $ucode<=0xDC00) { // サロゲートペアの前半 (0x55296 - 0x56320)
            $out[] = $uni[$i].$uni[$i+1].$uni[$i+2].$uni[$i+3];
            $i+= 4;
        } else {
            $out[] = $uni[$i].$uni[$i+1];
            $i+= 2;
        }
    }
    return $out;
}


// オーバーライド
function _dounderline($x,$y,$stringWidth)
{
    //Underline text
    $up=$this->CurrentFont['up'];
    $ut=$this->CurrentFont['ut'];
    $w=$stringWidth+$this->ws*substr_count($txt,' ');
    return sprintf('%.2f %.2f %.2f %.2f re f',$x*$this->k,($this->h-($y-$up/1000*$this->FontSize))*$this->k,$w*$this->k,-$ut/1000*$this->FontSizePt);
}


// オーバーライド
function Cell($w,$h=0,$uni='',$border=0,$ln=0,$align='',$fill=0,$link='')
{
    //Output a cell
    $k=$this->k;
    if($this->y+$h>$this->PageBreakTrigger && !$this->InFooter && $this->AcceptPageBreak())
    {
        //Automatic page break
        $x=$this->x;
        $ws=$this->ws;
        if($ws>0)
        {
            $this->ws=0;
            $this->_out('0 Tw');
        }
        $this->AddPage($this->CurOrientation);
        $this->x=$x;
        if($ws>0)
        {
            $this->ws=$ws;
            $this->_out(sprintf('%.3f Tw',$ws*$k));
        }
    }
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $s='';
    if($fill==1 || $border==1)
    {
        if($fill==1)
            $op=($border==1) ? 'B' : 'f';
        else
            $op='S';
        $s=sprintf('%.2f %.2f %.2f %.2f re %s ',$this->x*$k,($this->h-$this->y)*$k,$w*$k,-$h*$k,$op);
    }
    if(is_string($border))
    {
        $x=$this->x;
        $y=$this->y;
        if(strpos($border,'L')!==false)
            $s.=sprintf('%.2f %.2f m %.2f %.2f l S ',$x*$k,($this->h-$y)*$k,$x*$k,($this->h-($y+$h))*$k);
        if(strpos($border,'T')!==false)
            $s.=sprintf('%.2f %.2f m %.2f %.2f l S ',$x*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-$y)*$k);
        if(strpos($border,'R')!==false)
            $s.=sprintf('%.2f %.2f m %.2f %.2f l S ',($x+$w)*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
        if(strpos($border,'B')!==false)
            $s.=sprintf('%.2f %.2f m %.2f %.2f l S ',$x*$k,($this->h-($y+$h))*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
    }
    if($uni!=='')
    {
        if($align=='R')
            $dx=$w-$this->cMargin-$this->GetStringWidth($uni);
        elseif($align=='C')
            $dx=($w-$this->GetStringWidth($uni))/2;
        else
            $dx=$this->cMargin;
        if($this->ColorFlag)
            $s.='q '.$this->TextColor.' ';
        $uni2=str_replace("\n",'\\n',str_replace("\r",'\\r',str_replace(')','\\)',str_replace('(','\\(',str_replace('\\','\\\\',$uni)))));
        $s.=sprintf('BT %.2f %.2f Td (%s) Tj ET',($this->x+$dx)*$k,($this->h-($this->y+.5*$h+.3*$this->FontSize))*$k,$uni2);
        if($this->underline)
            $s.=' '.$this->_dounderline($this->x+$dx,$this->y+.5*$h+.3*$this->FontSize,$uni);
        if($this->ColorFlag)
            $s.=' Q';
        if($link)
            $this->Link($this->x+$dx,$this->y+.5*$h-.5*$this->FontSize,$this->GetStringWidth($uni),$this->FontSize,$link);
    }
    if($s)
        $this->_out($s);
    $this->lasth=$h;
    if($ln>0)
    {
        //Go to next line
        $this->y+=$h;
        if($ln==1)
            $this->x=$this->lMargin;
    }
    else
        $this->x+=$w;
}


//｢EUC-JP → UNICODE変換のためのラッパー
function UniCell($w, $h = 0, $euc = '', $border = 0, $ln = 0, $align = '', $fill = 0, $link = '') {
      $this->Cell($w, $h, utf16($euc), $border, $ln, $align, $fill, $link);
}


// オーバーライド
function MultiCell($w, $h, $euc, $border=0, $align='L', $fill=false, $is_ignore_enter=1) {
    // Output text with automatic or explicit line breaks
    if($w==0) $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize-500; // この500は適当である
    $euc = str_replace("\r",'',$euc);
    $uni =  utf16($euc);
    $ary = $this->GetCharArray($uni);
    $nb=count($ary);
    if($border==1) $border='LTRB';
    $b='';
    if(is_int(strpos($border,'L'))) $b.='L';
    if(is_int(strpos($border,'R'))) $b.='R';
    if(is_int(strpos($border,'T'))) $b.='T';

    $ww=0;
    $nl=1;
    $line = array();
    $utf16LF = pack('C*',0x00,0x0A);
    $utf16CR = pack('C*',0x00,0x0D);
    for ($i=0; $i<count($ary); $i++) {
        $c=$ary[$i];
        if ($c!=$utf16LF) {
	        $line[]= $c;
	        $ww += $this->GetStringWidth($c)*1000/$this->FontSize;
		}
        if($ww>$wmax || $i==count($ary)-1 || ($ww>0 && !$is_ignore_enter && $c==$utf16LF)) { // 次の行を作成する場合か、行の最終の場合か、明示的改行の場合
            if ($i==count($ary)-1 && is_int(strpos($border,'B'))) $b.='B'; // 最終行ならボトムボーダー追加
            $this->Cell($w,$h,implode("",$line),$b,2,$align,$fill); // ここまでを出力
            $nl++;
            $ww=0;
            $line = array();
            $b = str_replace($b, "T",""); // これより２行目以降。トップボーダーを削除。
        }
    }
    $this->x=$this->lMargin;
}

function _putType0($font) {
    // Type0
    $this->_newobj();
    $this->_out('<</Type /Font');
    $this->_out('/Subtype /Type0');
    $this->_out('/BaseFont /'.$font['name'].'-'.$font['CMap']);
    $this->_out('/Encoding /'.$font['CMap']);
    $this->_out('/DescendantFonts ['.($this->n+1).' 0 R]');
    $this->_out('>>');
    $this->_out('endobj');
    // CIDFont
    $this->_newobj();
    $this->_out('<</Type /Font');
    $this->_out('/Subtype /CIDFontType0');
    $this->_out('/BaseFont /'.$font['name']);
    $this->_out('/CIDSystemInfo <</Registry (Adobe) /Ordering ('.$font['registry']['ordering'].') /Supplement '.$font['registry']['supplement'].'>>');
    $this->_out('/FontDescriptor '.($this->n+1).' 0 R');
    $W='/W [1 [';
    foreach($font['cw'] as $w)
        $W.=$w.' ';
    $this->_out($W.'] 231 325 500 631 [500] 326 389 500]');
    $this->_out('>>');
    $this->_out('endobj');
    // Font descriptor
    $this->_newobj();
    $this->_out('<</Type /FontDescriptor');
    $this->_out('/FontName /'.$font['name']);
    $this->_out('/Flags 6');
    $this->_out('/FontBBox [0 -200 1000 900]');
    $this->_out('/ItalicAngle 0');
    $this->_out('/Ascent 800');
    $this->_out('/Descent -200');
    $this->_out('/CapHeight 800');
    $this->_out('/StemV 60');
    $this->_out('>>');
    $this->_out('endobj');
}
}
?>
