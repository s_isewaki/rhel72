<?
    define("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING", 1);
    require_once("ks_common.php");
    require_once("ks_ajax.php");


    // 隠しコマンド
    if ($_REQUEST["kanri_get_sum_ks_mst_pat_nyuin_taiin"]) {
        $sel = dbFind("select * from sum_ks_mst_pat_nyuin_taiin");
        while ($row = @pg_fetch_array($sel)) {
            echo $row["ptif_id"].",".$row["nyuin_ymd"].",".$row["taiin_ymd"]."\n";
        }
        die;
    }


    // （Ａ）指示板と温度版、基盤
    // （Ｂ）画面トップボタン系
    // （Ｃ）バイタル入力関連
    // （Ｄ）その他メニュー以下、マスタメンテ関連
    // （Ｅ）ケア入力
    // （Ｆ）指示板操作
    // （Ｇ）指示
    // （Ｈ）その他汎用

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/start/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" type="text/css" href="css/common.css?v=4">
<!--[if IE]><script type="text/javascript" src="js/VMLCanvas-1.1.1.min.js"></script><![endif]-->
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.js?v=4"></script>
<script type="text/javascript" src="js/common.js?v=4"></script>
<script type="text/javascript" src="js/showpage.js"></script>
<script type="text/javascript" src="js/custom_graph.js?v=5"></script>
<title>CoMedix | 看護支援</title>
<script type="text/javascript">
$.ajaxSettings.type = "POST";
$.ajaxSettings.async = false;

//window.moveTo(0,0);
//window.resizeTo(screen.availWidth,screen.availHeight);


<?$WYL = create_week_ymd_list($_REQUEST["ymd"]);?>
var gg_session_id = "<?=GG_SESSION_ID?>";
var single_ymd = "<?=$_REQUEST["single_ymd"]?>";
var baseUrl = "ks_ajax.php?session="+gg_session_id;
var monday_ymd = "<?=$WYL[0]?>";
var ext_view_ymd = "<?=($_REQUEST["single_ymd"] ? $_REQUEST["single_ymd"] : date("Ymd"))?>";
var ptif_id = "<?=@$_REQUEST["ptif_id"]?>";
var ptif_name = "";
var ptif_ward_name = "";
var ptif_bldg_ward = "";
var last_bldg_ward = "<?=GG_LAST_BLDG_WARD?>";
var view_mode = ""; // ondoban or soap or sijiban
var ext_view_mode = ""; // input_care
var stickyMenubar = 1;
var stickySijibar = 0;
var scrollTops = {"ondoban":0, "sijiban":0};

var karuteViewerUrl = "<?=dbGetOne("select value from system_config where key = 'kangoshien.karuteViewerUrl'")?>";
var pdfBackupDir = "<?=dbGetOne("select value from system_config where key = 'kangoshien.pdfBackupDir'")?>";
var pdfBackupMinute = "<?=(int)dbGetOne("select value from system_config where key = 'kangoshien.pdfBackupMinute'")?>";

function loaded() {
    if (isNotPC) ee("body").className = "is_not_pc";
    else ee("body").className = "is_pc";
    ee("header_menu_div").position = (stickyMenubar ? "fixed" : "relative");
    dfmMstOptions.reloadMasterData();
    dfmMstBldgWard.reloadMasterData();
    dfmMainMenu.reloadPtifMaster(ptif_id);
    dfmMainMenu.selectBldgWardCombo(last_bldg_ward);
    dfmMainMenu.changeView("<?=(@$_REQUEST["view_mode"]=="ondoban"?"ondoban":(@$_REQUEST["view_mode"]=="soap"?"soap":"sijiban"))?>", ptif_id);

    $(document).on("focus", 'input.text, select, textarea', function(){ $(this).addClass("focussing");});

    $(document).on({
        "focus":function(){ $(this).addClass("focussing");},
        "blur": function(){ $(this).removeClass("focussing"); }
    }, 'input.text, select, textarea')
    $(document).on({
        "focus":function(){ $(this.parentNode).addClass("focussing");},
        "blur": function(){ $(this.parentNode).removeClass("focussing"); }
    }, 'input.radio, input.checkbox');
    $(document).on({
        "submit": function(){ return false; }
    }, 'form');
    if (!isNotPC) gNavi.initialize();

}
</script>
</head>
<body onload="loaded()" onresize="getBaseWidth(); refreshGraphAndAdjustWidth(1);" id="body">

<iframe frameborder="0" name="iframe_print_target" style="position:absolute; left:0; top:0; width:0; height:0" tabIndex="-1"></iframe>







<? //**************************************************************************************************************** ?>
<? // 画面上部 メインメニュー                                                                                         ?>
<? //**************************************************************************************************************** ?>
<div id="header_menu_div">
<form id="efmHeaderMenu" name="efmHeaderMenu">
<table cellspacing="0" cellpadding="0" id="header_menu_table" style="width:100%"><tr>
    <td style="width:50px;" class="weekbtn"><a class="menubtn menubtn50" id="menubtn_sijiban" href="javascript:void(0)"
        onclick="dfmMainMenu.changeView('sijiban');">指示板</a></td>
    <td style="width:50px;" class="weekbtn"><a class="menubtn menubtn50" id="menubtn_soap" href="javascript:void(0)"
        onclick="dfmMainMenu.changeView('soap');">SOAP</a></td>
    <td style="width:50px;"><a class="menubtn menubtn50"
        id="menubtn_ondoban" href="javascript:void(0)" onclick="dfmMainMenu.changeView('ondoban',ptif_id);">温度板</a></td>
    <td style="width:50px;"><a class="menubtn menubtn50" href="javascript:void(0)" onclick="dfmKanriMenu.popupDrVital()">主治医</a></td>
    <td style="width:50px;"><a class="menubtn menubtn50" href="javascript:void(0)" onclick="dfmKanriMenu.popup()" id="menubtn_sonota">その他</a></td>
    <td style="width:50px;" class="ondoban"><a class="menubtn menubtn50" href="javascript:void(0)" onclick="dfmKanriMenu.popupKV()" id="menubtn_sonota" style="font-size:11px; line-height:1.1;">カルテ<br>ビュ-ワ</a></td>
    <td style="width:40px;" class="weekbtn ondoban sijiban_soap hide_at_ext_view_mode"><a class="menubtn" href="javascript:void(0)" onclick="dfmMainMenu.popupWeekCalendar()" title="日付選択"><img src="img/calendar_link.gif" style="vertical-align:middle" /></a></td>
    <td style="width:40px;" class="weekbtn ondoban sijiban_soap hide_at_ext_view_mode"><a class="menubtn" href="javascript:void(0)" onclick="dfmMainMenu.moveWeek('-7');" title="前週">≪</a></td>
    <td style="width:40px;" class="weekbtn ondoban sijiban_soap hide_at_ext_view_mode"><a class="menubtn" href="javascript:void(0)" onclick="dfmMainMenu.moveWeek('7');" title="翌週">≫</a></td>
    <td style="width:40px; display:none" class="weekbtn ext_view_mode_only hide_at_daily"><a class="menubtn" href="javascript:void(0)" onclick="dfmMainMenu.popupExtViewDayCalendar()" title="日付選択"><img src="img/calendar_link.gif" style="vertical-align:middle" /></a></td>
    <td style="width:40px; display:none;" class="weekbtn ext_view_mode_only hide_at_daily"><a class="menubtn" href="javascript:void(0)" onclick="dfmMainMenu.moveExtDay('-1');" title="前日">＜</a></td>
    <td style="width:40px; display:none;" class="weekbtn ext_view_mode_only hide_at_daily"><a class="menubtn" href="javascript:void(0)" onclick="dfmMainMenu.moveExtDay('1');" title="翌日">＞</a></td>
    <td style="width:auto; padding-left:10px; text-align:left"><nobr>
        <span class="sijiban_soap" style="padding-left:6px">病棟</span>
        <span class="sijiban_soap" style="padding-left:2px;" id="efmHeaderMenu_bldg_ward_container"></span>
        <span class="ext_view_mode_only" style="padding-left:20px; padding-right:0; display:none">指示日時点の入院状態</span>
        <span class="sijiban_soap hide_at_ondoban hide_at_ext_view_mode" style="padding-left:20px; padding-right:0">対象週の入院状態</span>
        <span class="sijiban_soap" style="padding-left:6px"><select name="is_nyuin_taiin" onchange="dfmMainMenu.changeView(view_mode, '', ext_view_mode)">
            <option value="nyuin_only">入院中患者</option>
            <option value="taiin_only">入院中患者以外</option>
            <option value="">（すべて）</option></select></span>
        <span class="sijiban_soap hide_at_ondoban" style="padding-left:20px; padding-right:0">
            患者名検索
            <input type="text" value="" name="ptif_name_find" id="ptif_name_find"
                style="width:100px" navi="漢字名またはかな名を指定してエンターキーを押してください。部分一致検索を行います。"
                onchange="dfmMainMenu.changeView(view_mode, '', ext_view_mode)"
                onkeydown="if(isEnterKey(this,event)) dfmMainMenu.changeView(view_mode, '', ext_view_mode)" />
        </span>

    </nobr></td>
    <td style="width:auto;"><div id="efmHeaderMenu_ptif_info" style="display:none"><nobr>
        <span class="ondoban literal" name="ptrm_name" style="font-size:12px; padding-right:8px"></span>
        <span class="ondoban literal" name="ptif_id" style="font-size:12px; background-color:#3171dd; padding:0 2px"></span><span
            class="ondoban literal" style="padding:0 12px 0 8px; font-size:18px; letter-spacing:4px" name="ptif_name"></span><span
            class="ondoban" style="color:#0c53c1; padding:0">(</span><span class="ondoban literal" name="ptif_gender" style="font-size:12px;"></span><span
            class="ondoban" style="padding:0">/</span><span class="ondoban literal" name="ptif_age" style="font-size:12px;"></span><span
            class="ondoban" style="color:#0c53c1; padding:0">)</span>
    </nobr></div></td>
    <? if (GG_ONDOBAN_ADMIN_FLG) { ?>
    <td style="width:50px"><a class="menubtn menubtn50" style="border-left:1px solid #88aaee" href="javascript:void(0)"
        onclick="dfmPrintSelector.popup();">印刷</a></td>
    <td style="width:50px"><a class="menubtn menubtn50" href="javascript:void(0)" onclick="dfmSetting.popup();">環境</a></td>
    <? } ?>
    <td style="width:130px; display:none" class="dailybtn"><a class="menubtn closebtn" href="javascript:void(0)" onclick="window.close();" style="width:130px">単日表示を閉じる</a></td>
</tr></table>
</form>
</div>
<div id="header_menu_div_margin" style="height:30px">&nbsp;</div>
<script type="text/javascript">
var dfmMainMenu = {
    ondoban_container : 0,
    moveWeek : function(day_incriment){
        var dt = new Date(int(substr2(monday_ymd,0,4)), int(substr2(monday_ymd,4,2))-1, int(substr2(monday_ymd,6,2)));
        dt.setTime(dt.getTime() + day_incriment * 86400000);
        monday_ymd = dt.getFullYear() + ("00"+(dt.getMonth()+1)).slice(-2) + ("00"+dt.getDate()).slice(-2);
        this.changeView(view_mode, ptif_id, ext_view_mode);
    },
    moveExtDay : function(day_incriment){
        var dt = new Date(int(substr2(ext_view_ymd,0,4)), int(substr2(ext_view_ymd,4,2))-1, int(substr2(ext_view_ymd,6,2)));
        dt.setTime(dt.getTime() + day_incriment * 86400000);
        ext_view_ymd = dt.getFullYear() + ("00"+(dt.getMonth()+1)).slice(-2) + ("00"+dt.getDate()).slice(-2);
        var aYmd = ymdToArray(ext_view_ymd);
        monday_ymd = ymdDateAdd(ext_view_ymd, -1*(aYmd.weekNumber-1));
        this.changeView(view_mode, ptif_id, ext_view_mode);
    },
    reloadPtifMaster : function(_ptif_id) {
        _ptif_id = trim(_ptif_id);
        $.ajax({ url:baseUrl+"&ajax_action=get_ptif_attribute&ptif_id="+uEnc(_ptif_id), success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            ee("efmHeaderMenu_ptif_info").style.display = (_ptif_id ? "" : "none");
            bindDataToForm("efmHeaderMenu_ptif_info", obj);
            ptif_id = _ptif_id;
            ptif_name = obj.ptif_name;
            ptif_ward_name = obj.ward_name;
            ptif_bldg_ward = obj.bldg_ward;
        }});
    },
    selectBldgWardCombo : function(last_bldg_ward) {
        setComboValue(efmHeaderMenu.bldg_ward, last_bldg_ward);
    },
    changeView : function(_view_mode, _ptif_id, _ext_view_mode) {
        var self = this;
        setTimeout(function(){ self._changeView(_view_mode, _ptif_id, _ext_view_mode); }, 10);
    },
    // 温度板をコールする場合は、_ptif_id必須
    _changeView : function(_view_mode, _ptif_id, _ext_view_mode) {
        if (!_ext_view_mode) _ext_view_mode = "";
        if (!_ptif_id) _ptif_id = "";
        var self = this;
        gNavi.hide();
        if (_view_mode=="ondoban" && _ptif_id=="") return alert("患者を指定してください。");
        scrollTops[(view_mode=="ondoban"?"ondoban":"sijiban")] = $(window).scrollTop();
        ee("header_menu_div").style.position = (stickyMenubar ? "fixed" : "relative");
        ee("header_menu_div_margin").style.display = (stickyMenubar ? "" : "none");
        if (!this.ondoban_container) this.ondoban_container = ee("ondoban_container");
        var bActiveShowAll = (!ee("siji_actives_show_all") || ee("siji_actives_show_all").checked ? "1" : "");


        if (_view_mode!=view_mode || _ext_view_mode!=ext_view_mode) {
            ee("menubtn_ondoban").className = "menubtn menubtn50";
            ee("menubtn_sijiban").className = "menubtn menubtn50";
            ee("menubtn_soap").className = "menubtn menubtn50";
            ee("menubtn_sonota").className = "menubtn menubtn50";
            if (!_ext_view_mode) ee("menubtn_"+_view_mode).className = "menubtn menubtn50 menubtn_on";
            if (_ext_view_mode) ee("menubtn_sonota").className = "menubtn menubtn50 menubtn_on";

            var hide_view = (_view_mode=="ondoban"?"sijiban_soap" : "ondoban");
            var show_view = (_view_mode=="ondoban"?"ondoban" : "sijiban_soap");

            self.ondoban_container.innerHTML = "";
            ee("sijiban_soap_container").innerHTML = "";
            $("."+hide_view).hide();
            $("."+show_view).show();
            $("#efmHeaderMenu .weekbtn").each(function(){  this.style.display = (single_ymd ? "none": "");  });
            $("#efmHeaderMenu .dailybtn").each(function(){  this.style.display = (single_ymd ? "": "none");  });

            $("#efmHeaderMenu .ext_view_mode_only").each(function(){ this.style.display = (_ext_view_mode ? "" : "none"); });
            if (_ext_view_mode) $("#efmHeaderMenu .hide_at_ext_view_mode").each(function(){ this.style.display = "none"; });
            if (_view_mode=="ondoban") $("#efmHeaderMenu .hide_at_ondoban").each(function(){ this.style.display = "none"; });
            if (single_ymd) $("#efmHeaderMenu .hide_at_daily").each(function(){ this.style.display = "none"; });
        }

        if (_view_mode=="sijiban" || _view_mode=="soap") {
            var sData = serializeUnicodeEntity("efmHeaderMenu");
            var url =
            "ks_ajax_sijiban.php?session="+gg_session_id+"&ajax_action=get_sijiban_soap_list&view_mode="+_view_mode+"&monday_ymd="+
            monday_ymd+"&sticky_menubar="+stickyMenubar+"&ptif_id="+ptif_id+"&ext_view_mode="+_ext_view_mode+"&ext_view_ymd="+ext_view_ymd;
            $.ajax({url:url, data:sData, success:function(msg){
                if (getCustomTrueAjaxResult(msg, "", 1)==AJAX_ERROR) return;
                ee("sijiban_soap_container").innerHTML = msg;
                window.scrollTo(0,scrollTops["sijiban"]);
                view_mode = _view_mode;
                ext_view_mode = _ext_view_mode;
            }});
        } else {
            getBaseWidth();
            var url =
            "ks_ajax_ondoban.php?session="+gg_session_id+"&ajax_action=get_ondoban_list&ptif_id="+uEnc(_ptif_id)+"&ymd="+monday_ymd+"&single_ymd="+single_ymd+
            "&sticky_menubar="+stickyMenubar+"&sticky_sijibar="+stickySijibar+"&is_not_pc="+isNotPC+"&day_width="+ondobanDayWidth;
            $.ajax({url:url, success:function(msg){
                if (getCustomTrueAjaxResult(msg, "", 1)==AJAX_ERROR) return;
                self.ondoban_container.style.display = "none";
                if (ee("ondoban_siji")) ee("ondoban_siji").innerHTML = "";
                if (ee("ondoban_tooltip")) ee("ondoban_tooltip").innerHTML = "";
                self.ondoban_container.innerHTML = msg;
                if (ee("siji_actives_show_all")) ee("siji_actives_show_all").checked = bActiveShowAll;
                view_mode = _view_mode;
                refreshGraphAndAdjustWidth();
                self.setSijiActivesData(single_ymd);
                window.scrollTo(0,scrollTops["ondoban"]);
            }});
        }
    },
    setSijiActivesData : function(ymd) { // 引数はsingle_ymd
        if (!ymd) ymd = getComboValue(ee("siji_actives_selector"));
        var sijiAllElementsData = eval(ee("sijiAllElementsData").value);
        var isShowAll = 0;
        if (ee("siji_actives_show_all")) isShowAll = ee("siji_actives_show_all").checked;

        if (!sijiAllElementsData) return;
        for (var idx=0; idx<sijiAllElementsData.length; idx++) {
            ee(sijiAllElementsData[idx]).style.display = "none";
            ee(sijiAllElementsData[idx]+"_wrap").style.display = (isShowAll ? "" : "none");
        }
        var sijiActivesData = "";
        eval('sijiActivesData = '+ee("sijiActivesData").value);
        var ary = sijiActivesData["YMD_"+ymd];
        for (var idx=0; idx<ary.length; idx++) {
            if (!ary[idx]) continue;
            ee(ary[idx]).style.display = "";
            if (!isShowAll) ee(ary[idx]+"_wrap").style.display = "";
        }
    },
    popupWeekCalendar : function() {
        var self = this;
        var ymdCallbackFunc = function(_monday_ymd, aYmd) {
            monday_ymd = _monday_ymd;
            self.changeView(view_mode, ptif_id, ext_view_mode);
        }
        dfmYmdAssist.popup("week", monday_ymd, "", ymdCallbackFunc);
    },
    popupExtViewDayCalendar : function() {
        var self = this;
        var ymdCallbackFunc = function(_ymd, aYmd, _monday_ymd) {
            monday_ymd = _monday_ymd;
            ext_view_ymd = _ymd;
            self.changeView(view_mode, ptif_id, ext_view_mode);
        }
        dfmYmdAssist.popup("ymd", ext_view_ymd, "", ymdCallbackFunc);
    },
    single_window_object : "",
    showSingleDayMode : function(ymd) {
        if (!ptif_id) return alert("先に患者を指定してください。");
        var url = "ks_main.php?session="+gg_session_id+"&ptif_id="+uEnc(ptif_id)+"&single_ymd="+ymd+"&view_mode=ondoban";
        try { this.single_window_object.close(); } catch(ex){}
        var winParams = ", left=0, top=0, location=no, directories=no, menubar=no, status=no, alwaysRaised=yes, dependent=no, resizable=yes, scrollbars=yes";
        this.single_window_object = window.open(url,"ondoban_single_mode", "width="+window.screen.width+", height="+window.screen.height+""+winParams);
    }
}
</script>




<? //**************************************************************************************************************** ?>
<? // （Ａ）                                                                                                          ?>
<? // 指示板と温度板                                                                                                  ?>
<? //**************************************************************************************************************** ?>
<div id="ondoban_container" class="ondoban" style=" padding-bottom:20px; position:absolute"></div>
<div id="sijiban_soap_container" class="sijiban_soap" style="display:none; padding-bottom:20px; position:absolute"></div>








<div style="display:none">





<? //**************************************************************************************************************** ?>
<? // （Ｂ−１）                                                                                                      ?>
<? // その他メニュー                                                                                                  ?>
<? //**************************************************************************************************************** ?>
<form id="efmKanriMenu">

    <div style="padding-top:10px">
    <table cellspacing="0" cellpadding="0" style="width:100%"><tr><td style="width:300px">
        <div style="padding:0 0 5px 0"><div style="background-color:#ace0ff; padding:2px"><span style="letter-spacing:8px">マスタ設定</span>（システム管理者のみ）</div></div>
    <? if (GG_ADMIN_FLG) { ?>
    <ul>
    <li><a class="block2" href="javascript:void(0)" onclick="dfmMstBldgWard.popup()"><span>病棟・病室マスタ</span></a></li>
    <li><a class="block2" href="javascript:void(0)" onclick="dfmMstOptions.popup()"><span>薬剤・選択肢マスタ</span></a></li>
    <li><a class="block2" href="javascript:void(0)"
        onclick="dfmMstBSScale.popup('BSSCALE_COMMON0', '')"><span>BSスケール表（院内固定O）</span></a></li>
    <li><a class="block2" href="javascript:void(0)"
        onclick="dfmMstBSScale.popup('BSSCALE_COMMON1', '')"><span>BSスケール表（院内固定I）</span></a></li>
    <li><a class="block2" href="javascript:void(0)"
        onclick="dfmMstBSScale.popup('BSSCALE_COMMON2', '')"><span>BSスケール表（院内固定II）</span></a></li>
    <li><a class="block2" href="javascript:void(0)"
        onclick="dfmMstBSScale.popup('BSSCALE_COMMON3', '')"><span>BSスケール表（院内固定III）</span></a></li>
    <li><a class="block2" href="javascript:void(0)"
        onclick="dfmMstBSScale.popup('BSSCALE_COMMON4', '')"><span>BSスケール表（院内固定IV）</span></a></li>
    <li><a class="block2" href="javascript:void(0)" onclick="dfmMstEmpAuth.popup()"><span>権限マスタ</span></a></li>
        <li><a class="block2" href="javascript:void(0)" onclick="dfmKanriMenu.popupImport()"><span>データインポート</span></a></li></ul>
    <? } else { ?>
    <ul>
        <li class="hover" style="cursor:default"><span class="instruction">病棟・病室マスタ</span></li>
        <li class="hover" style="cursor:default"><span class="instruction">薬剤・選択肢マスタ</span></li>
        <li class="hover" style="cursor:default"><span class="instruction">BSスケール表（院内固定O）</span></li>
        <li class="hover" style="cursor:default"><span class="instruction">BSスケール表（院内固定I）</span></li>
        <li class="hover" style="cursor:default"><span class="instruction">BSスケール表（院内固定II）</span></li>
        <li class="hover" style="cursor:default"><span class="instruction">BSスケール表（院内固定III）</span></li>
        <li class="hover" style="cursor:default"><span class="instruction">BSスケール表（院内固定IV）</span></li>
        <li class="hover" style="cursor:default"><span class="instruction">権限マスタ</span></li>
        <li class="hover" style="cursor:default"><span class="instruction">データインポート</span></li></ul>
    <? } ?>
    </td>
    <td style="padding-left:20px">
        <div style="padding:0 0 5px 0"><div style="background-color:#ace0ff; padding:2px; letter-spacing:8px">補助機能</div></div>
    <ul>
        <li><a class="block2" href="javascript:void(0)" onclick="dfmKanriMenu.popupBed()"><span>ベッド利用状況</span></a></li>
        <li><a class="block2" href="javascript:void(0)" onclick="dfmKanriMenu.popupKeikouGoukei()"><span>経口合計表</span></a></li>
        <li><a class="block2" href="javascript:void(0)" onclick="dfmKanriMenu.popupSoapMemoSokuhou()"><span>SOAP/MEMO速報</span></a></li>
        <li><a class="block2" href="javascript:void(0)" onclick="dfmKanriMenu.changeViewForInputVital()"><span>一括バイタル・ケア入力</span></a></li>
        <li><a class="block2" href="javascript:void(0)" onclick="dfmKanriMenu.popupCareRireki()"><span>ケア記入履歴検索</span></a></li>
        <li><a class="block2" href="javascript:void(0)" onclick="dfmKanriMenu.popupSijiRireki()"><span>指示記入履歴検索</span></a></li>
    </ul>
    </tr></table>
    </div>
</form>
<script type="text/javascript">
var dfmKanriMenu = {
    soap_memo_window : 0,
    bed_window : 0,
    keikou_goukei_window : 0,
    dr_vital_window : 0,
    import_window : 0,
    $dialogForm : 0,
    popup : function() {
        this.$dialogForm = $('#efmKanriMenu').dialog({ title:'その他メニュー', width:650, height:420, position:"left" });
    },
    popupBed : function() {
        if (this.bed_window) {
            try {this.bed_window.close();} catch(ex){}
        }
        var url = "ks_view_bed.php?session="+gg_session_id;
        var winParams = ", location=no, directories=no, menubar=no, status=no, alwaysRaised=yes, dependent=no, resizable=yes, scrollbars=yes";
        this.bed_window = window.open(url,"ks_view_bed", "width=1000, height=550"+winParams);
    },
    popupSoapMemoSokuhou : function() {
        if (this.soap_memo_window) {
            try {this.soap_memo_window.close();} catch(ex){}
        }
        var bldg_ward = ptif_bldg_ward;
        if (view_mode!="ondoban") {
            bldg_ward = getComboValue(efmHeaderMenu.bldg_ward);
            if (this.cur_bldg_ward=="" || this.cur_bldg_ward=="all") bldg_ward = "";
        }
        var url = "ks_view_soap_memo_sokuhou.php?session="+gg_session_id+"&monday_ymd="+monday_ymd+"&bldg_ward="+bldg_ward;
        var winParams = ", location=no, directories=no, menubar=no, status=no, alwaysRaised=yes, dependent=no, resizable=yes, scrollbars=yes";
        this.soap_memo_window = window.open(url,"ks_soap_memo_sokuhou", "width=1000, height=550"+winParams);
    },
    popupKeikouGoukei : function() {
        if (this.keikou_goukei_window) {
            try {this.keikou_goukei_window.close();} catch(ex){}
        }
        var bldg_ward = ptif_bldg_ward;
        if (view_mode!="ondoban") {
            bldg_ward = getComboValue(efmHeaderMenu.bldg_ward);
            if (this.cur_bldg_ward=="" || this.cur_bldg_ward=="all") bldg_ward = "";
        }
        var url = "ks_view_keikou_goukei.php?session="+gg_session_id+"&monday_ymd="+monday_ymd+"&bldg_ward="+bldg_ward;
        var winParams = ", location=no, directories=no, menubar=no, status=no, alwaysRaised=yes, dependent=no, resizable=yes, scrollbars=yes";
        this.keikou_goukei_window = window.open(url,"ks_view_bed", "width=850, height=550"+winParams);
    },
    popupDrVital : function() {
        if (this.dr_vital_window) {
            try {this.dr_vital_window.close();} catch(ex){}
        }
        var url = "ks_view_dr_vital.php?session="+gg_session_id+"&init_dr_emp_id=<?=GG_LOGIN_EMP_ID?>&ymd="+single_ymd;
        var winParams = ", location=no, directories=no, menubar=no, status=no, alwaysRaised=yes, dependent=no, resizable=yes, scrollbars=yes";
        this.dr_vital_window = window.open(url,"ks_view_dr_vital", "width=1000, height=550"+winParams);
    },
    popupKV : function() {
        if (!karuteViewerUrl) {
            alert("カルテビューワは準備されていません。\n\n「環境」ボタンを押して「カルテビューワURL」を設定してください。");
            return false;
        }
        window.open(karuteViewerUrl+uEnc(ptif_id), "", "");
    },
    popupImport : function() {
        if (this.import_window) {
            try {this.import_window.close();} catch(ex){}
        }
        var url = "ks_view_import.php?session="+gg_session_id;
        var winParams = ", location=no, directories=no, menubar=no, status=no, alwaysRaised=yes, dependent=no, resizable=yes, scrollbars=yes";
        this.import_window = window.open(url,"ks_view_import", "width=1000, height=550"+winParams);
    },
    changeViewForInputVital : function() {
        this.$dialogForm.dialog("close");
        this.$dialogForm = 0;
        dfmMainMenu.changeView('sijiban','','input_care');
    },
    popupCareRireki : function() {
        if (this.care_rireki_window) {
            try {this.care_rireki_window.close();} catch(ex){}
        }
        var url = "ks_view_care_rireki.php?session="+gg_session_id+"&kako_check=1&monday_ymd="+monday_ymd;
        var winParams = ", location=no, directories=no, menubar=no, status=no, alwaysRaised=yes, dependent=no, resizable=yes, scrollbars=yes";
        this.care_rireki_window = window.open(url,"ks_view_care_rireki", "width=1000, height=550"+winParams);
    },
    popupSijiRireki : function() {
        if (this.siji_rireki_window) {
            try {this.siji_rireki_window.close();} catch(ex){}
        }
        var url = "ks_view_siji_rireki.php?session="+gg_session_id+"&kako_check=1&monday_ymd="+monday_ymd;
        var winParams = ", location=no, directories=no, menubar=no, status=no, alwaysRaised=yes, dependent=no, resizable=yes, scrollbars=yes";
        this.siji_rireki_window = window.open(url,"ks_view_siji_rireki", "width=1000, height=550"+winParams);
    }
}
</script>








<? //**************************************************************************************************************** ?>
<? // （Ｂ−２）                                                                                                      ?>
<? // 環境メニュー・タブレット対策メニュー                                                                            ?>
<? //**************************************************************************************************************** ?>
<form id="efmSetting" name="efmSetting">
    <div style="padding:8px 0 3px 0">※ 固定環境設定</div>
    <table cellspacing="0" cellpadding="0" class="general_list" style="width:auto">
    <tr><th style="padding:3px">日勤時間帯</th><td style="padding:3px">
        <?=substr(NIKKIN_START_HHMI,0,2)?>:<?=substr(NIKKIN_START_HHMI,2)?>〜<?=substr(NIKKIN_END_HHMI,0,2)?>:<?=substr(NIKKIN_END_HHMI,2)?>
    </td></tr>
    <tr><th style="padding:3px">準夜時間帯</th><td style="padding:3px">
        <?=substr(JUNYA_START_HHMI,0,2)?>:<?=substr(JUNYA_START_HHMI,2)?>〜<?=substr(JUNYA_END_HHMI,0,2)?>:<?=substr(JUNYA_END_HHMI,2)?>
    </td></tr>
    <tr><th style="padding:3px">深夜時間帯</th><td style="padding:3px">
        <?=substr(SINYA_START_HHMI,0,2)?>:<?=substr(SINYA_START_HHMI,2)?>〜<?=substr(SINYA_END_HHMI,0,2)?>:<?=substr(SINYA_END_HHMI,2)?>
    </td></tr>
    </table>
    <div class="instruction" style="padding-top:4px">※上記の時刻はSOAP事象時刻時間帯の自動判定に影響します。</div>
    <div class="instruction" style="padding-top:2px">※この設定は変更できません。変更するには管理者に連絡ください。</div>


    <div style="padding-top:20px">※ 簡易環境設定</div>
    <div><label><input type="checkbox" name="sticky_menubar" value="1" onclick="dfmSetting.changeSetting()">
        ヘッダ部とダイアログ位置をスクロールさせない（タブレット時はオフにしてください）</label></div>
    <div><label><input type="checkbox" name="sticky_sijibar" value="1" onclick="dfmSetting.changeSetting()">
        指示部（温度板画面右）をスクロールさせない（タブレット時はオフにしてください）</label></div>
    <div class="instruction" style="padding-top:4px">※この設定は画面を閉じるまで有効です。</div>

    <div style="padding-top:20px">※ 全体設定</div>
    <div id="efmSetting__karuteViewerUrl">
        カルテビューワURL
        <input type="text" class="text" name="sc_value" id="efmSetting__karuteViewerUrl__value" style="width:400px; ime-mode:disabled" />
        <button type="button" onclick="dfmSetting.saveKaruteViewerUrl(this.value)">保存</button><br>
        <div class="instruction" style="padding-top:4px">※末尾に患者IDが付与されます。URL末尾は「=」（イコール文字）となります。</div>
    </div>
    <div style="padding-top:10px" navititle="PDFバックアップ" navi="指定時刻になると、指定された場所に「温度板週間報告」のPDFを自動保存します。<br>・先週分および今週分の温度板<br>・対象週内に１日以上入院していること<br>※「週ごと／患者ごと」に１ファイルが作成されます。<br>※ファイル名は[月曜日のYYYYMMDD]+[アンダーバー]+[患者ID]+[.pdf]で作成されます。<br>※指示やケア情報がなくとも、ファイルは作成されます。<br>※作成済PDFファイルがあれば、上書きされます。">
        <table cellspacing="0" cellpadding="0"><tr>
            <td><nobr>PDFバックアップ</nobr></td>
            <td>
                <span id="efmSetting__pdfBackupDir">
                    場所
                    <input type="text" class="text" name="sc_value" id="efmSetting__pdfBackupDir__value" style="width:400px; ime-mode:disabled" />
                    <button type="button" onclick="dfmSetting.savePdfBackupDir()">保存</button>
                </span>
                <br>
                <span id="efmSetting__pdfBackupMinute">
                    毎時
                    <input type="text" class="text imeoff" name="sc_value" maxlength="2" pattern="[0-9]*" id="efmSetting__pdfBackupMinute__value" style="width:50px" />
                    分に作成を実施
                    <button type="button" onclick="dfmSetting.savePdfBackupMinute()">保存</button>
                </span>
            </td>
        </tr></table>
        <div class="instruction" style="padding-top:4px">※有効なバックアップ場所が指定されている場合に稼動を開始します。</div>
    </div>

    <!-- div><label><input type="checkbox" name="kouseizaiDistanceInput" value="1" onclick="dfmSetting.saveSetting()">
        抗生剤の期間入力</label></div>
    <div class="instruction" style="padding-top:4px">※この設定は保存されます。</div -->
</form>
<script type="text/javascript">
var dfmSetting = {
    $dialogForm : 0,
    popup : function() {
        var obj = {};
        obj["sticky_menubar"] = stickyMenubar;
        obj["sticky_sijibar"] = stickySijibar;
        bindDataToForm("efmSetting", obj);
        ee("efmSetting__karuteViewerUrl__value").value = karuteViewerUrl;
        ee("efmSetting__pdfBackupDir__value").value = pdfBackupDir;
        ee("efmSetting__pdfBackupMinute__value").value = pdfBackupMinute;
        this.$dialogForm = $('#efmSetting').dialog({ title:'環境', width:800, height:480 });
    },
    changeSetting : function() {
        stickyMenubar = (efmSetting.sticky_menubar.checked ? 1 : 0);
        stickySijibar = (efmSetting.sticky_sijibar.checked ? 1 : 0);
        this.$dialogForm.dialog("close");
        dfmMainMenu.changeView(view_mode, ptif_id, ext_view_mode);
    },
    saveKaruteViewerUrl : function(url) {
        var sData = serializeUnicodeEntity("efmSetting__karuteViewerUrl");
        var self = this;
        $.ajax({url:baseUrl+"&ajax_action=save_system_config&sc_key=karuteViewerUrl", data:sData, success:function(msg) {
            var msg = getCustomTrueAjaxResult(msg); if(msg==AJAX_ERROR) return;
            karuteViewerUrl = msg;
            self.$dialogForm.dialog("close");
            dfmMainMenu.changeView(view_mode, ptif_id, ext_view_mode);
        }});
    },
    savePdfBackupDir : function() {
        var sData = serializeUnicodeEntity("efmSetting__pdfBackupDir");
        var self = this;
        $.ajax({url:baseUrl+"&ajax_action=save_system_config&sc_key=pdfBackupDir", data:sData, success:function(msg) {
            var msg = getCustomTrueAjaxResult(msg); if(msg==AJAX_ERROR) return;
            pdfBackupDir = msg;
            self.$dialogForm.dialog("close");
            dfmMainMenu.changeView(view_mode, ptif_id, ext_view_mode);
        }});
    },
    savePdfBackupMinute : function() {
        var sData = serializeUnicodeEntity("efmSetting__pdfBackupMinute");
        var self = this;
        $.ajax({url:baseUrl+"&ajax_action=save_system_config&sc_key=pdfBackupMinute", data:sData, success:function(msg) {
            var msg = getCustomTrueAjaxResult(msg); if(msg==AJAX_ERROR) return;
            pdfBackupMinute = int(msg);
            self.$dialogForm.dialog("close");
            dfmMainMenu.changeView(view_mode, ptif_id, ext_view_mode);
        }});
    },
    saveSetting : function() {
        stickyMenubar = (efmSetting.sticky_menubar.checked ? 1 : 0);
        stickySijibar = (efmSetting.sticky_sijibar.checked ? 1 : 0);
        this.$dialogForm.dialog("close");
        dfmMainMenu.changeView(view_mode, ptif_id, ext_view_mode);
    }
}
</script>









<? //**************************************************************************************************************** ?>
<? // （Ｂ−３）                                                                                                      ?>
<? // 指示メニュー                                                                                                    ?>
<? //**************************************************************************************************************** ?>
<form id="efmSijiMenu"></form>
<script type="text/javascript">
var dfmSijiMenu = {
    sijibi : "",
    ymd : "",
    popup : function(sijibi){
        if (!ptif_id) return alert("先に患者を指定してください。");
        this.sijibi = sijibi;
        this.ymd = sijibi;
        var aYmd = ymdToArray(sijibi);
        var sijibi_html = '<button type="button" class="ymd_button" onclick="dfmSijiMenu.popupCalendar(this)">'+aYmd.slash_mdw+'</button>';
        if (single_ymd) sijibi_html = aYmd.slash_mdw;
        ee("efmSijiMenu").innerHTML =
        '<div style="padding:3px 0 4px 0">'+
        '<div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>'+
        '<th class="literal ptif_name" name="ptif_name_disp" style="padding-bottom:3px">'+htmlEscape(ptif_name)+'</th>'+
        '<th>指示日：'+sijibi_html+'</th>'+
        '<td class="right">'+
        '<button type="button" onclick="dfmSijiMenu.showSingleDayMode()"'+(single_ymd ? " disabled" : "")+' style="width:100px">単日表示</button>'+
        '</td>'+
        '</tr></table></div></div>'+
        '<table cellspacing="0" cellpadding="0"><tr><td style="width:220px">'+
        '<div style="padding:0 0 5px 0"><div style="background-color:#ace0ff; padding:2px; letter-spacing:8px">指示</div></div>'+
        '<ul>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmSijiFreeword)">'+
        '<span>フリーワード１指示入力</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmSijiFree2word)">'+
        '<span>フリーワード２指示入力</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmSijiKeikan)">'+
        '<span>食事（経管）指示入力</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmSijiKeikou)">'+
        '<span>食事（経口）指示入力</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmSijiKeiKei)">'+
        '<span>食事（補食）指示入力</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmSijiInsulinFix)">'+
        '<span>インスリン固定投与入力</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmSijiBSScale)">'+
        '<span>BSスケール指示入力</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmSijiInsulinSR)">'+
        '<span>インスリン食事指示量</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmSijiGezai)">'+
        '<span>下剤指示入力</span></a></li></ul>'+
        '</td><td style="padding-left:20px; width:220px">'+
        '<div style="padding:0 0 5px 0"><div style="background-color:#ace0ff; padding:2px; letter-spacing:8px">ケア</div></div>'+
        '<ul>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmCareSanso)">'+
        '<span>酸素</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmCareKouseizai, \'kouseizai\')">'+
        '<span>抗生剤</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmCareSyokujiIn)">'+
        '<span>食事・IN</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmCareHoeki)">'+
        '<span>補液</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmCareInsulin)">'+
        '<span>インスリン</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmCareOut)">'+
        '<span>OUT</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmCareKensanado)">'+
        '<span>検査等</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmCareKansatu)">'+
        '<span>観察項目</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmCareSign)">'+
        '<span>サイン</span></a></li></ul>'+
        '</td><td style="padding-left:20px">'+
        '<div style="padding:0 0 5px 0"><div style="background-color:#ace0ff; padding:2px; letter-spacing:5px">その他</div></div>'+
        '<ul>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmVitalSelector)">'+
        '<span>バイタル</span></a></li>'+
        '<li><a class="block2" href="javascript:void(0)" onclick="dfmSijiMenu.popupDialog(dfmRisyoRiyuSelector)">'+
        '<span>離床理由</span></a></li></ul>'+
        '</td></tr></table>';
        $('#efmSijiMenu').dialog({ title:'日単位メニュー', width:750, height:450 });
    },
    popupDialog : function(targetObj, name, _ext_view_ymd, _ptif_id, _ptif_name) {
        if (_ext_view_ymd) {
            if (ee("divpt_"+ptif_id)) ee("divpt_"+ptif_id).style.borderLeft = "";
            if (ee("divpt_"+_ptif_id)) ee("divpt_"+_ptif_id).style.borderLeft = "6px solid #ff0000";
            this.ymd = _ext_view_ymd;
            var ptifChanging = (ptif_id===_ptif_id ? false : true);
            ptif_id = _ptif_id;
            ptif_name = _ptif_name;
            if (ptifChanging) dfmMainMenu.reloadPtifMaster(_ptif_id);
        }
        if (name=="kouseizai") {
            targetObj.popup(ptif_id, this.ymd, "");
            return;
        }
        targetObj.popup(this.ymd);
    },
    showSingleDayMode : function() {
        dfmMainMenu.showSingleDayMode(this.ymd)
    },
    popupCalendar : function(btn) {
        var self = this;
        var ymdCallbackFunc = function(ymd, aYmd) {
            btn.innerHTML = (aYmd.slash_mdw ? aYmd.slash_mdw : "");
            self.ymd = ymd;
        }
        dfmYmdAssist.popup("ymd", this.ymd, this.sijibi, ymdCallbackFunc, 1);
    }
}
</script>






<? //**************************************************************************************************************** ?>
<? // （Ｃ−１）                                                                                                      ?>
<? // 印刷セレクタ                                                                                                    ?>
<? //**************************************************************************************************************** ?>
<form id="efmPrintSelector" name="efmPrintSelector">
    <div style="padding:10px 0">
    <table cellspacing="0" cellpadding="0">
        <tr>
            <th width="80" style="padding-bottom:20px">帳票種別</th>
            <td style="padding-bottom:20px"><select name="daily_weekly" onchange="dfmPrintSelector.dailyWeeklyChanged()">
                <option value="weekly">週間報告</option><option value="daily">単日報告</option></select></td>
        </tr>
        <tr id="efmPrintSelector_tr_kikan_weekly">
            <th style="padding-bottom:20px">対象期間</th>
            <td style="padding-bottom:20px">
                <table cellspacing="0" cellpadding="0" class="no_border"><tr>
                    <td style="width:250px" id="efmPrintSelector_tr_kikan"></td>
                    <td class="right"><button type="button" onclick="dfmPrintSelector.incrementDay('-7');">≪ 前週</button>
                    <button type="button" onclick="dfmPrintSelector.incrementDay('7');">翌週 ≫</button></td>
                </tr></table>
            </td>
        </tr>
        <tr id="efmPrintSelector_tr_kikan_daily">
            <th style="padding-bottom:20px">対象日</th>
            <td style="padding-bottom:20px">
                <table cellspacing="0" cellpadding="0" class="no_border"><tr>
                    <td><button type="button" id="efmPrintSelector_tr_daybtn" onclick="dfmPrintSelector.popupCalendar(this)">&nbsp;</button></td>
                    <td class="right"><button type="button" onclick="dfmPrintSelector.incrementDay('-1');">≪ 前日</button>
                    <button type="button" onclick="dfmPrintSelector.incrementDay('1');">翌日 ≫</button></td>
                </tr></table>
            </td>
        </tr>
        <tr>
            <th width="80" style="padding-bottom:20px">対象患者</th>
            <td style="padding-bottom:20px"><select name="ptif_type" id="efmPrintSelector_ptif_type">
                <option value="ptif"></option><option value="bldg_ward"></option><option value="all">全入院患者</option></select></td>
        </tr>
    </table>
    </div>
    <hr>
    <div style="padding-top:10px">
    <table cellspacing="0" cellpadding="0" style="width:auto">
        <tr>
            <th class="left"><nobr>PDFファイル作成</nobr></th>
            <td style="padding-left:10px">
                <button type="button" onclick="dfmPrintSelector.printPdf('ondoban')" style="width:90px">温度板</button>
            </td>
        </tr>
        <tr>
            <th class="left" style="padding-bottom:20px; padding-top:10px"><nobr>ブラウザ印刷</nobr></th>
            <td style="padding-top:10px; padding-left:10px">
                <table cellspacing="0" cellpadding="0"><tr>
                <td><button type="button" onclick="dfmPrintSelector.printPdf('sijiban')" style="width:90px">指示板</button></td>
                <td><span style="padding-left:3px"><button type="button" onclick="dfmPrintSelector.printPdf('soap')" style="width:90px">SOAP</button></span></td>
                <td class="center"><span style="padding-left:3px"><button type="button" onclick="dfmPrintSelector.printPdf('sijiban','input_care')"
                    style="width:160px" id="efmPrintSelector_input_care_btn">一括バイタル・ケア</button></span>
                    <div class="instruction">（単日報告のみ）</div>
                </td>
                <td><label><input type="checkbox" class="checkbox" checked name="kaipage" id="efmPrintSelector_kaipage">患者ごとに改ページ</label></td>
                </tr></table>
            </td>
        </tr>
    </table>
    <div class="instruction">※患者名検索ボックスの抽出条件は無視されます。</div>
    </div>
</form>
<script type="text/javascript">
var dfmPrintSelector = {
    $dialogForm: "",
    form : document.efmPrintSelector,
    ymd : "",
    weekNumber : 1,
    cur_bldg_ward : "",
    cur_bldg_ward_name : "",
    popup : function() {
        this.ymd = (single_ymd ? single_ymd : monday_ymd);
        setComboValue(this.form.daily_weekly, (single_ymd ? "daily" : "weekly"));
        this.dailyWeeklyChanged();
        this.incrementDay(0);
        self.$dialogForm = $('#efmPrintSelector').dialog({ title:'印刷', width:680, height:400 });
        var aYmd = ymdToArray(this.ymd);
        this.weekNumber = aYmd.weekNumber;
        this.cur_bldg_ward = ptif_bldg_ward;
        this.cur_bldg_ward_name = ptif_ward_name;
        if (view_mode!="ondoban") {
            this.cur_bldg_ward = getComboValue(efmHeaderMenu.bldg_ward);
            this.cur_bldg_ward_name = getComboText(efmHeaderMenu.bldg_ward);
            if (this.cur_bldg_ward=="" || this.cur_bldg_ward=="all") {
                this.cur_bldg_ward = "";
                this.cur_bldg_ward_name = "";
            }
        }
        var cmb = ee("efmPrintSelector_ptif_type");
        cmb.options[0].text = "選択中患者：" + (ptif_name ? ptif_name : "(患者が指定されていません)");
        cmb.options[1].text = "病棟入院中患者：" + (this.cur_bldg_ward ? this.cur_bldg_ward_name : "(病棟が指定されていません)");
        cmb.selectedIndex = (ptif_name && view_mode=="ondoban" ? 0 : (this.cur_bldg_ward ? 1 : 2));
        ee("efmPrintSelector_tr_daybtn").innerHTML = aYmd.slash_ymd;
    },
    dailyWeeklyChanged : function() {
        var dailyWeekly = getComboValue(this.form.daily_weekly);
        if (dailyWeekly=="weekly" && this.weekNumber>1) this.incrementDay(-1*(this.weekNumber-1)); // 月曜に戻す
        ee("efmPrintSelector_tr_kikan_weekly").style.display = (dailyWeekly=="weekly" ? "" : "none");
        ee("efmPrintSelector_tr_kikan_daily").style.display = (dailyWeekly=="daily" ? "" : "none");
        ee("efmPrintSelector_input_care_btn").disabled = (dailyWeekly=="daily" ? "" : "disabled");


    },
    incrementDay : function(day_incriment){
        var dt = new Date(int(substr2(this.ymd,0,4)), int(substr2(this.ymd,4,2))-1, int(substr2(this.ymd,6,2)));
        dt.setTime(dt.getTime() + day_incriment * 86400000);
        var ymd_fr_disp = dt.getFullYear() + "/" + ("00"+(dt.getMonth()+1)).slice(-2) + "/" + ("00"+dt.getDate()).slice(-2);
        this.ymd = ymd_fr_disp.replace(/\//g, "");
        var aYmd = ymdToArray(this.ymd);
        this.weekNumber = aYmd.weekNumber;
        dt.setTime(dt.getTime() + 7 * 86400000);
        var ymd_to_disp = dt.getFullYear() + "/" + ("00"+(dt.getMonth()+1)).slice(-2) + "/" + ("00"+dt.getDate()).slice(-2);
        var ymd_to = ymd_to_disp.replace(/\//g, "");
        ee("efmPrintSelector_tr_kikan").innerHTML = ymd_fr_disp + " 〜 " + ymd_to_disp;
        ee("efmPrintSelector_tr_daybtn").innerHTML = ymd_fr_disp;
    },
    closeDialog : function() {
        if (this.$dialogForm) this.$dialogForm.dialog("close");
        this.$dialogForm = "";
    },
    popupCalendar : function(btn) {
        var self = this;
        var ymdCallbackFunc = function(ymd, aYmd) {
            self.ymd = ymd;
            btn.innerHTML = aYmd.slash_ymd;
        }
        dfmYmdAssist.popup("ymd", this.ymd, "", ymdCallbackFunc, 1);
    },
    printPdf : function(viewMode, extViewMode) { // ondoban or sijiban or soap
        if (!extViewMode) extViewMode = "";
        var dailyWeekly = getComboValue(this.form.daily_weekly);
        var ptifType = getComboValue(this.form.ptif_type);
        if (ptifType=="ptif" && !ptif_id) return alert("印刷するには、指示板より患者を指定してください。");
        if (ptifType=="bldg_ward" && !this.cur_bldg_ward) return alert("病棟が指定されていません。");
        var param = "&ptif_id="+uEnc(ptif_id);
        if (viewMode=="ondoban") {
            if (ptifType=="bldg_ward") param = "&bldg_ward="+this.cur_bldg_ward;
            if (ptifType=="all") param = "&bldg_ward=all";
            //param += "&ptif_name_find="+ee("ptif_name_find").value;
            var url = "ks_print_pdf_utf16_ondoban.php?session="+gg_session_id+"&daily_weekly="+dailyWeekly+"&ymd="+this.ymd+param;
            window.open(url,"iframe_print_target1", "width=300,height=300");
        }
        else {
            if (ptifType=="bldg_ward") param = "&is_nyuin_taiin=nyuin_only&bldg_ward="+this.cur_bldg_ward;
            if (ptifType=="all") param = "&is_nyuin_taiin=nyuin_only&bldg_ward=all";
            param += "&print_mode="+(ee("efmPrintSelector_kaipage").checked?"2":"1"); // print_mode。通常印刷1、患者ごと改ページ印刷2
            //param += "&ptif_name_find="+ee("ptif_name_find").value;
            var url =
            "ks_print_html_sijiban_soap.php?session="+gg_session_id+"&view_mode="+viewMode+
            "&monday_ymd="+this.ymd+"&ext_view_ymd="+this.ymd+"&ext_view_mode="+extViewMode+param;
            var winParams = ", location=no, directories=no, menubar=yes, status=no, alwaysRaised=yes, dependent=no, resizable=yes, scrollbars=yes";
            window.open(url,"iframe_print_target1", "width=1000,height=300"+winParams);
        }
    }
}
</script>










<? //**************************************************************************************************************** ?>
<? // （Ｃ−１）                                                                                                      ?>
<? // バイタル選択。対象指示日に登録がなければこのダイアログをスルー、直接バイタル入力ダイアログを新規モードで開く    ?>
<? //**************************************************************************************************************** ?>
<form id="efmVitalSelector"></form>
<script type="text/javascript">
var dfmVitalSelector = {
    $dialogForm: "",
    popup : function(ymd, ymdhm) {
        hideOndobanTooltip();
        var self = this;
        if (!ptif_id) return alert("先に患者を指定してください。");
        $.ajax({url:baseUrl+"&ajax_action=get_vital_per_day_list&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd, success:function(msg) {
            var ary = getCustomTrueAjaxResult(msg, TYPE_JSON); if(ary==AJAX_ERROR) return;
            if (!ary.length) { dfmVital.popup(ymd); return; }
            var vital_list = [];
            for (var idx=0; idx<ary.length; idx++) {
                vital_list.push(
                    '<li><a class="block2" onclick="dfmVital.popup(\''+ary[idx].ymd+'\',\''+ary[idx].ymdhm+'\'); return false;"'+
                    ' href="javascript:void(0)"><span>'+ary[idx].ymdhm_jp+'</span></a></li>'
                );
            }
            ee("efmVitalSelector").innerHTML =
            '<ul><li><a class="block2" href="javascript:void(0)" onclick="dfmVital.popup(\''+ymd+'\'); return false;">'+
            '<span>新規登録</span></a></li>'+ vital_list.join("")+'</ul>';
            self.$dialogForm = $('#efmVitalSelector').dialog({ title:'バイタル選択', width:200, height:(vital_list.length*35)+130 });
        }});
    },
    closeDialog : function() {
        if (this.$dialogForm) this.$dialogForm.dialog("close");
        this.$dialogForm = "";
    }
}
</script>











<? //**************************************************************************************************************** ?>
<? // （Ｃ−２）                                                                                                      ?>
<? // 離床理由のポップアップセレクタ、登録がなければ直接離床理由入力ダイアログを開く                                  ?>
<? //**************************************************************************************************************** ?>
<form id="efmRisyoRiyuSelector"></form>
<script type="text/javascript">
var dfmRisyoRiyuSelector = {
    $dialogForm: "",
    popup : function(ymd) {
        var self = this;
        if (!ptif_id) return alert("先に患者を指定してください。");
        var url = baseUrl+"&ajax_action=get_care_kikan_per_day_list&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd+"&care_content_list=risyo_riyu";
        $.ajax({ url:url, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            var field_count = int(obj.risyo_riyu__field_count);
            if (!field_count) { dfmRisyoRiyu.popup(ymd); return; }

            var risyo_list = [];
            for (var idx=1; idx<=field_count; idx++) {
                var prefix = "risyo_riyu__"+idx+"__";
                var aYmdFr = ymdToArray(obj[prefix+"ymdhm_fr"]);
                var aYmdTo = ymdToArray(obj[prefix+"ymdhm_to"]);
                var kikan_jp = aYmdFr.slash_md_hm+" 〜 "+aYmdTo.slash_md_hm;
                var risyo_riyu = trim(obj[prefix+"disp2"]);
                if (!risyo_riyu) risyo_riyu = trim((obj[prefix+"disp1"].split("\t"))[0]);
                if (risyo_riyu) risyo_riyu = '<span style="padding-left:20px">'+htmlEscape(trim(risyo_riyu))+'</span>';

                risyo_list.push(
                    '<li><a class="block2" onclick="dfmRisyoRiyu.popup(\''+ymd+'\',\''+obj[prefix+"ymdhm_fr"]+'\'); return false;"'+
                    ' href="javascript:void(0)"><span>'+kikan_jp+'</span>'+risyo_riyu+'</a>'
                );
            }
            ee("efmRisyoRiyuSelector").innerHTML =
            '<ul><li><a class="block2" href="javascript:void(0)" onclick="dfmRisyoRiyu.popup(\''+ymd+'\'); return false;">'+
            '<span>新規登録</span></a></li>'+risyo_list.join("")+'</ul>';
            self.$dialogForm = $('#efmRisyoRiyuSelector').dialog({ title:'離床理由選択', width:550, height:(risyo_list.length*22)+160 });
        }});
    },
    closeDialog : function() {
        if (this.$dialogForm) this.$dialogForm.dialog("close");
        this.$dialogForm = "";
    }
}
</script>









<? //**************************************************************************************************************** ?>
<? // （Ｃ−３）                                                                                                      ?>
<? // バイタル入力ダイアログ                                                                                          ?>
<? //**************************************************************************************************************** ?>
<form id="efmVital" name="efmVital">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="slash_mdw"></th><td></td>
    </tr></table></div>


    <table cellspacing="0" cellpadding="0">
    <tr>
        <td style="width:100px;">測定時刻</td>
        <td colspan="2">
            <button type="button" class="hm_button" name="hm@button" onclick="dfmVital.popupTimeAssist(this)">:</button>
            <input type="hidden" name="hm" />
        </td>
    </tr>
    </table>

    <hr>

    <table cellspacing="0" cellpadding="0">
    <tr>
        <td width="100">体温</td>
        <td width="24" class="vital_t fontsize12 middle">T</td>
        <td>
            <input type="text" title="半角数値のみ、最大２文字" pattern="[0-9]*" class="text num3" name="taion1" maxlength="2" />
            <span>度</span>
            <input type="text" title="半角数値のみ、最大１文字" pattern="[0-9]*" class="text num3" name="taion2" maxlength="1" />
            <span>分</span>
        </td>
        <td><span class="subcolor">33〜43</span></td>
    </tr>
    <tr>
        <td style="padding:3px 0">脈拍数</td>
        <td class="vital_p fontsize12 middle">P</td>
        <td style="padding:3px 0"><input type="text" title="半角数値のみ、最大３文字" pattern="[0-9]*" class="text num3"
            name="myakuhakusu" maxlength="3"><span>回</span>/<span>分</span></td>
        <td style="padding:3px 0"><span class="subcolor">10〜210</span>
            <label style="display:none">
                <input type="checkbox" class="checkbox" id="myakuhakusu_marker" name="myakuhakusu_x_marker" value="1">マーカーを「×」に変更
            </label>
        </td>
    </tr>
    <tr>
        <td>収縮期血圧</td>
        <td class="vital_bp fontsize12 middle">BP</td>
        <td><input type="text" title="半角数値のみ、最大３文字" pattern="[0-9]*" class="text num3" name="ketsuatsu_upper" maxlength="3"><span>mmHg</span></td>
        <td><span class="subcolor">〜240</span></td>
    </tr>
    <tr>
        <td style="padding:3px 0">拡張時血圧</td>
        <td class="vital_bp fontsize12 middle">BP</td>
        <td style="padding:3px 0"><input type="text" title="半角数値のみ、最大３文字" pattern="[0-9]*" class="text num3"
            name="ketsuatsu_bottom" maxlength="3"><span>mmHg</span></td>
        <td style="padding:3px 0"><span class="subcolor">40〜</span></td>
    </tr>
    <tr>
        <td>呼吸数</td>
        <td class="vital_r fontsize12 middle">R</td>
        <td><input type="text" title="半角数値のみ、最大２文字" pattern="[0-9]*" class="text num3" name="kokyusu" maxlength="2"><span>回</span>/<span>分</span></td>
        <td><span class="subcolor">5〜55</span></td>
    </tr>
    <tr>
        <td colspan="4"><hr></td>
    </tr>
    <tr>
        <td style="padding:3px 0">心拍数</td>
        <td class="vital_hr fontsize12 middle">HR</td>
        <td style="padding:3px 0"><input type="text" title="半角数値のみ、最大３文字" pattern="[0-9]*" class="text num3" name="hr_kaisu" maxlength="3"><span>回</span>/<span>分</span></td>
        <td style="padding:3px 0"><span class="subcolor">10〜210</span></td>
    </tr>
    <tr>
        <td>SpO2</td>
        <td class="vital_sp fontsize12 middle">SP</td>
        <td><input type="text" title="半角数値のみ、最大３文字" pattern="[0-9]*" class="text num3" name="spo2_percent" maxlength="3"><span>％</span></td>
        <td><span class="subcolor">21〜100</span></td>
    </tr>
    <tr>
        <td style="padding:3px 0">体重</td>
        <td class="vital_w fontsize12 middle">W</td>
        <td style="padding:3px 0"><input type="text" title="半角数値のみ、小数１桁まで、最大５文字" onfocus="ipadHackFocusIn(this)" pattern="^[0-9]?[0-9]?[0-9]?([0-9][\.][0-9])?$" class="text num4" name="taiju" maxlength="5"><span>kg</span></td>
        <td style="padding:3px 0"><span class="subcolor"></span></td>
    </tr>
    </table>
</form>

<script type="text/javascript">
var dfmVital = {
    popup : function(ymd, ymdhm, _ptif_id, _ptif_name){
        if (ee("divpt_"+ptif_id)) ee("divpt_"+ptif_id).style.borderLeft = "";
        if (ee("divpt_"+_ptif_id)) ee("divpt_"+_ptif_id).style.borderLeft = "6px solid #ff0000";
        var ptifChanging = (ptif_id===_ptif_id ? false : true);
        if (_ptif_id) ptif_id = _ptif_id;
        if (_ptif_name) {
            ptif_name = _ptif_name;
            if (ptifChanging) dfmMainMenu.reloadPtifMaster(_ptif_id);
        }

        hideOndobanTooltip();
//        if (view_mode=="ondoban") _ptif_id = ptif_id; // 温度板画面ならグローバル患者idを利用する。指示板からならptif_id、ptif_nameがセットされている。

        if (!ymdhm) ymdhm = "";
        if (ymdhm) ymd = ymdhm.substring(0,8);
        var saveVital = function() {
            if (!checkInputError("efmVital")) return false;
            var ret = false;
            var sData = serializeUnicodeEntity("efmVital");
            $.ajax({url:baseUrl+"&ajax_action=save_vital_one_data&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd+"&ymdhm="+ymdhm, data:sData, success:function(msg) {
                var obj = getCustomTrueAjaxResult(msg); if(obj==AJAX_ERROR) return;
                dfmVitalSelector.closeDialog();
                dfmMainMenu.changeView(view_mode, ptif_id, ext_view_mode);
                ret = true;
            }});
            return ret;
        }
        var deleteVital = function() {
            var ret = false;
            if (!confirm("削除します。よろしいですか？")) return ret;
            $.ajax({url:baseUrl+"&ajax_action=delete_vital_data&ptif_id="+uEnc(ptif_id)+"&ymdhm="+ymdhm, success:function(msg) {
                var obj = getCustomTrueAjaxResult(msg); if(obj==AJAX_ERROR) return;
                dfmMainMenu.changeView(view_mode, ptif_id, ext_view_mode);
                ret = true;
                dfmVitalSelector.closeDialog();
            }});
            return ret;
        }
        $.ajax({ url:baseUrl+"&ajax_action=get_vital_one_data&ptif_id="+uEnc(ptif_id)+"&ymdhm="+ymdhm, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            var aYmd = ymdToArray(ymdhm);
            if (!ymdhm) aYmd = ymdToArray(ymd);
            obj["slash_mdw"] = aYmd["slash_mdw"];
            obj["sijibi"] = "指示日："+aYmd.slash_mdw;
            bindDataToForm("efmVital", obj);
            var buttons = ["save"];
            if (ymdhm) buttons = ["save", "delete"];
            $('#efmVital').dialog({ title:'バイタル入力', width:450, height:430, dlgFooter:obj, extButtons:buttons
                , saveExec:saveVital, deleteExec:deleteVital });
        }});
    },
    popupTimeAssist : function(btn) {
        var nm = btn.getAttribute("name").split("@");
        var callbackFunc = function(hm, colon_hm) {
            document.efmVital[nm[0]].value = hm;
            btn.innerHTML = colon_hm;
        }
        dfmHMAssist.popup(document.efmVital[nm[0]].value, callbackFunc);
    }
}
</script>











<? //**************************************************************************************************************** ?>
<? // （Ｃ−４）                                                                                                      ?>
<? // 離床理由入力ダイアログ                                                                                          ?>
<? //**************************************************************************************************************** ?>
<form id="efmRisyoRiyu" name="efmRisyoRiyu">
    <input type="hidden" name="risyo_riyu__field_count" />
    <input type="hidden" name="risyo_riyu__1__ymdhm_fr" />

    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="sijibi"></th><td></td>
    </tr></table></div>

    <table cellspacing="0" cellpadding="0">
    <tr>
        <th style="padding:3px 0">開始日時</th>
        <td style="padding:3px 0">
            <button type="button" class="ymd_button" name="risyo_riyu__1__fr_ymd@button" onclick="dfmRisyoRiyu.popupCalendar(this)">&nbsp;</button>
            <input type="hidden" name="risyo_riyu__1__fr_ymd" />
            <button type="button" class="hm_button" name="risyo_riyu__1__fr_hm@button" onclick="dfmRisyoRiyu.popupTimeAssist(this)">:</button>
            <input type="hidden" name="risyo_riyu__1__fr_hm" />
        </td>
    </tr>
    <tr>
        <th>終了日時</th>
        <td>
            <button type="button" class="ymd_button" name="risyo_riyu__1__to_ymd@button" onclick="dfmRisyoRiyu.popupCalendar(this)">&nbsp;</button>
            <input type="hidden" name="risyo_riyu__1__to_ymd" />
            <button type="button" class="hm_button" name="risyo_riyu__1__to_hm@button" onclick="dfmRisyoRiyu.popupTimeAssist(this)">:</button>
            <input type="hidden" name="risyo_riyu__1__to_hm" />
        </td>
    </tr>
    <tr>
        <th style="padding:3px 0">理由</th>
        <td style="padding:3px 0">
            <select name="risyo_riyu__1__disp1" opt_group="risyo_riyu"></select>
            または
            <input type="text" class="text" title="最大３０文字" maxlength="30" name="risyo_riyu__1__disp2" />
        </td>
    </tr>
    <tr>
        <th>備考</th>
        <td>
            <textarea name="risyo_riyu__1__remark" rows="5" cols="40" style="width:400px"></textarea>
        </td>
    </table>
    <div style="padding-top:30px" class="instruction">
    ※グラフへは「理由」が表示されます。
    </div>
</form>
<script type="text/javascript">
var dfmRisyoRiyu = {
    sijibi : "",
    popup : function(ymd, ymdhm_fr){
        this.sijibi = ymd;
        if (!ymdhm_fr) ymdhm_fr = "";
        var saveRisyoRiyu = function() {
            if (!checkInputError("efmRisyoRiyu")) return false;
            var ret = false;
            var sData = serializeUnicodeEntity("efmRisyoRiyu");
            var url = baseUrl+"&ajax_action=save_care_kikan&ptif_id="+uEnc(ptif_id)+"&care_content_list=risyo_riyu&ymd="+ymd;
            $.ajax({ url:url, data:sData, success:function(msg) {
                var obj = getCustomTrueAjaxResult(msg); if(obj==AJAX_ERROR) return;
                dfmMainMenu.changeView("ondoban", ptif_id);
                ret = true;
                dfmRisyoRiyuSelector.closeDialog();
            }});
            return ret;
        }
        var deleteRisyoRiyu = function() {
            if (!confirm("削除します。よろしいですか？")) return false;
            var ret = false;
            var sData = serializeUnicodeEntity("efmRisyoRiyu");
            var url = baseUrl+"&ajax_action=delete_care_kikan&ptif_id="+uEnc(ptif_id)+"&care_content=risyo_riyu&ymdhm_fr="+ymdhm_fr;
            $.ajax({ url:url, data:sData, success:function(msg) {
                var obj = getCustomTrueAjaxResult(msg); if(obj==AJAX_ERROR) return;
                dfmMainMenu.changeView("ondoban", ptif_id);
                ret = true;
                dfmRisyoRiyuSelector.closeDialog();
            }});
            return ret;
        }
        var url = baseUrl+"&ajax_action=get_care_kikan_one_data&ptif_id="+uEnc(ptif_id)+"&ymdhm_fr="+ymdhm_fr+"&care_content=risyo_riyu";
        $.ajax({ url:url, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            var aYmd = ymdToArray(ymd);
            obj["sijibi"] = "指示日："+aYmd.slash_mdw;
            if (!obj.risyo_riyu__field_count) obj.risyo_riyu__field_count = "1";
            bindDataToForm("efmRisyoRiyu", obj);
            var buttons = ["save"];
            if (ymdhm_fr) buttons = ["save", "delete"];
            $('#efmRisyoRiyu').dialog({
                title:'離床理由', width:500, height:340, extButtons:buttons, saveExec:saveRisyoRiyu, deleteExec:deleteRisyoRiyu
            });
        }});
    },
    popupCalendar : function(btn) {
        var nm = btn.getAttribute("name").split("@");
        var hidden = document.efmRisyoRiyu[nm[0]];
        var ymdCallbackFunc = function(ymd, aYmd) {
            hidden.value = ymd;
            btn.innerHTML = (aYmd.slash_mdw!=""?aYmd.slash_mdw:"&nbsp;");
        }
        dfmYmdAssist.popup("ymd", hidden.value, this.sijibi, ymdCallbackFunc);
    },
    popupTimeAssist : function(btn) {
        var nm = btn.getAttribute("name").split("@");
        var callbackFunc = function(hm, colon_hm) {
            document.efmRisyoRiyu[nm[0]].value = hm;
            btn.innerHTML = colon_hm;
        }
        dfmHMAssist.popup(document.efmRisyoRiyu[nm[0]].value, callbackFunc);
    }
}
</script>










<? //**************************************************************************************************************** ?>
<? // （Ｄ−１）                                                                                                      ?>
<? // 権限マスタ                                                                                                      ?>
<? //**************************************************************************************************************** ?>
<form id="efmMstEmpAuth" name="efmMstEmpAuth" onsubmit="return false" navititle="権限マスタ" navi="システム管理者でない「一般操作者」は、「職員名」列がハイパーリンクとなります。<br>「一般操作者」リンク押下で、「温度板管理者権限」の付与と解除を行えます。<br>「温度板管理者権限」があると、「患者附帯情報」ダイアログの更新が行えるようになります。<br>「患者附帯情報」ダイアログは、「指示板」画面等で患者名の上に表示されている「病室」を押下すると表示されます。">
    <div style="height:30px; top:0; padding-top:5px">
        職員ID／職員名
        <input type="text" class="text" name="keyword" title="職員IDまたは職員名での部分一致検索、スペース区切でAND条件検索、最大３０文字、職員名カナ昇順表示" maxlength="30" onkeydown="if(isEnterKey(this,event)) dfmMstEmpAuth.trySearch()" />
        <button type="button" onclick="dfmMstEmpAuth.trySearch()" class="w60">検索</button>
    </div>

    <div id="efmMstEmpAuth_list_container"></div>
</form>
<script type="text/javascript">
var dfmMstEmpAuth = {
    popup : function(){
        document.efmMstEmpAuth.keyword.value = "";
        this.trySearch();
        $('#efmMstEmpAuth').dialog({ title:'権限マスタ', width:600, height:550, extButtons:["up","down"] });
    },
    trySearch : function() {
        var sData = serializeUnicodeEntity("efmMstEmpAuth");
        $.ajax({ url:baseUrl+"&ajax_action=get_emp_auth_list", data:sData, success:function(msg) {
            var html = getCustomTrueAjaxResult(msg); if(html==AJAX_ERROR) return;
            ee("efmMstEmpAuth_list_container").innerHTML =
            '<table cellspacing="0" cellpadding="0" class="general_list white_list"><tr><th>職員ID</th><th>職員名</th><th>権限</th></tr>'+html+'</table>';
        }});
    },
    saveData : function(emp_id, current_auth) {
        var self = this;
        var sTop = ee("efmMstEmpAuth").scrollTop;
        if (!current_auth) current_auth = "";
        var msg = "温度板管理者権限を外します。よろしいですか？";
        if (!current_auth) msg = "温度板管理者権限を付与します。よろしいですか？";
        if (!confirm(msg)) return;
        var url = baseUrl+"&ajax_action=save_emp_auth&emp_id="+uEnc(emp_id)+"&current_auth="+current_auth;
        $.ajax({ url:url, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg); if(obj==AJAX_ERROR) return;
            self.trySearch();
            ee("efmMstEmpAuth").scrollTop = sTop;
        }});
    }
}
</script>










<? //**************************************************************************************************************** ?>
<? // （Ｄ−２）                                                                                                      ?>
<? // 病棟・病室マスタ                                                                                                ?>
<? //**************************************************************************************************************** ?>
<form id="efmMstBldgWard" name="efmMstBldgWard">
    <div style="padding-top:12px">
    <table cellspacing="0" cellpadding="0">
        <tr><th width="100">病棟</th><td id="efmMstBldgWard_bldg_ward_container"></td></tr>
    </table>
    </div>
    <hr>
    <table cellspacing="0" cellpadding="0">
    <tr><td class="dot">選択病棟の名称変更</td>
        <td><input type="text" class="text" name="mod_ward_name" title="最大２０文字" maxlength="20" /><button type="button" onclick="dfmMstBldgWard.saveData('mod_bldg_ward')" class="w60">変更</button></td>
    </tr>
    <tr><td class="dot">病棟の追加</td>
        <td><input type="text" class="text" name="add_ward_name" title="最大２０文字" maxlength="20" /><button type="button" onclick="dfmMstBldgWard.saveData('add_bldg_ward')" class="w60">追加</button></td>
    </tr>
    <tr><td class="dot">選択病棟の削除</td><td><button type="button" onclick="dfmMstBldgWard.saveData('del_bldg_ward')" class="w60">削除</button></td></tr>
    </table>

    <div style="padding-top:40px">
    <table cellspacing="0" cellpadding="0">
        <tr><th width="100">病室</th><td id="efmMstBldgWard_ptrm_room_no_container"></td></tr>
    </table>
    </div>
    <hr>

    <table cellspacing="0" cellpadding="0">
    <tr><td class="dot">選択病室の名称・ベッド数変更</td>
        <td><input type="text" class="text" name="mod_ptrm_name" title="最大２０文字" maxlength="20" /><input type="text" title="半角数値のみ、最大２文字" pattern="[0-9]*" class="text num2" name="mod_bed_count" maxlength="2" /><button type="button"
            onclick="dfmMstBldgWard.saveData('mod_ptrm')" class="w60">変更</button></td>
    </tr>
    <tr><td class="dot">病室の追加</td>
        <td><input type="text" class="text" name="add_ptrm_name" title="最大２０文字" maxlength="20" /><input type="text" title="半角数値のみ、最大２文字" pattern="[0-9]*" class="text num2" name="add_bed_count" maxlength="2" /><button type="button"
            onclick="dfmMstBldgWard.saveData('add_ptrm')" class="w60">追加</button></td>
    </tr>
    <tr><td class="dot">選択病室の削除</td><td><button type="button"
        onclick="dfmMstBldgWard.saveData('del_ptrm')" class="w60">削除</button></td>
    </tr></table>

    <div class="instruction" style="padding-top:40px">
    <div>※病棟を削除すると、配下の病室も削除されます。</div>
    <div>※患者の存在する病棟/病室を削除すると、患者は「未割当」にリセットされます。</div>
    </div>
</form>
<script type="text/javascript">
var dfmMstBldgWard = {
    mstPtrm : [],
    popup : function(){
        var self = this;
        bindDataToForm("efmMstBldgWard", {});
        self.bldgWardChanged(getComboValue(efmMstBldgWard.bldg_ward));
        $('#efmMstBldgWard').dialog({ title:'病棟・病室マスタ', width:600, height:430 });
    },
    reloadMasterData : function() {
        var self = this;
        $.ajax({ url:baseUrl+"&ajax_action=get_mst_bldg_ward_ptrm", success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            ee("efmHeaderMenu_bldg_ward_container").innerHTML =
            '<select name="bldg_ward" onchange="dfmMainMenu.changeView(view_mode, \'\', ext_view_mode)">'+
            '<option value="">（未割当）</option><option value="all">（すべて）</option>'+obj.bldg_ward_options+'</select>';
            ee("efmMstBldgWard_bldg_ward_container").innerHTML =
            '<select name="bldg_ward" onchange="dfmMstBldgWard.bldgWardChanged(this.value)">'+obj.bldg_ward_options+'</select>';
            ee("efmPtifDetail_bldg_ward_container").innerHTML =
            '<select name="bldg_ward" onchange="dfmPtifDetail.remakePtrmDropdown(this.value)">'+
            '<option value="">（未割当）</option>'+obj.bldg_ward_options+'</select>';
            self.mstPtrm = obj.ptrm_list;
        }});
    },
    bldgWardChanged : function(bldg_ward, ptrm_room_no) {
        var options = [];
        for (var idx=0; idx<this.mstPtrm.length; idx++) {
            var row = this.mstPtrm[idx];
            if (row.bldg_ward != bldg_ward) continue;
            options.push('<option value="'+row.ptrm+'">'+row.name+'</option>');
        }
        ee("efmMstBldgWard_ptrm_room_no_container").innerHTML =
        '<select name="ptrm_room_no" onchange="dfmMstBldgWard.ptrmChanged(\''+bldg_ward+'\')">'+options.join("")+'</select>';
        setComboValue(efmMstBldgWard.bldg_ward, bldg_ward);
        setComboValue(efmMstBldgWard.ptrm_room_no, ptrm_room_no);
        efmMstBldgWard.mod_ward_name.value = getComboText(efmMstBldgWard.bldg_ward);
        efmMstBldgWard.add_ward_name.value = "";
        this.ptrmChanged(bldg_ward);
    },
    ptrmChanged : function(bldg_ward) {
        var bedCount = "";
        var ptrm_room_no = getComboValue(efmMstBldgWard.ptrm_room_no);
        for (var idx=0; idx<this.mstPtrm.length; idx++) {
            var row = this.mstPtrm[idx];
            if (row.bldg_ward != bldg_ward) continue;
            if (ptrm_room_no==row.ptrm) bedCount = row.bed_count;
        }
        if (!bedCount && efmMstBldgWard.ptrm_room_no.value) bedCount = 0;
        efmMstBldgWard.mod_ptrm_name.value = getComboText(efmMstBldgWard.ptrm_room_no);
        efmMstBldgWard.mod_bed_count.value = bedCount;
        efmMstBldgWard.add_ptrm_name.value = "";
        efmMstBldgWard.add_bed_count.value = "";
    },
    saveData : function(mode, tryCommit) {
        var self = this;
        if (!checkInputError("efmMstBldgWard")) return false;
        if (!tryCommit) tryCommit = "";
        if (tryCommit) {
            var msg = "変更";
            if (mode.substring(0,3)=="add") msg= "追加";
            if (mode.substring(0,3)=="del") msg= "削除";
            if (!confirm(msg+"します。よろしいですか？")) return;
        }
        var sData = serializeUnicodeEntity("efmMstBldgWard");
        $.ajax({ url:baseUrl+"&ajax_action=save_mst_bldg_ward_ptrm&mode="+mode+"&try_commit="+tryCommit, data:sData, success:function(msg) {
            var ret = getCustomTrueAjaxResult(msg, TYPE_JSON); if (ret==AJAX_ERROR) return;
            if (!tryCommit) { self.saveData(mode, 1); return; }
            bindDataToForm("efmMstBldgWard", {});
            self.reloadMasterData();
            if (!ret.bldg_ward) ret.bldg_ward = getComboValue(efmMstBldgWard.bldg_ward);
            self.bldgWardChanged(ret.bldg_ward, ret.ptrm_room_no);
        }});
    }
}
</script>










<? //**************************************************************************************************************** ?>
<? // （Ｄ−３）                                                                                                      ?>
<? // 薬剤・選択肢マスタ                                                                                              ?>
<? //**************************************************************************************************************** ?>
<form id="efmMstOptions" name="efmMstOptions">
    <div style="padding-top:12px">
    <table cellspacing="0" cellpadding="0">
        <tr><th width="120">用途</th><td>
            <select onchange="dfmMstOptions.categoryChanged(this.value)" name="opt_group">
            <? foreach ($opt_groups as $k => $v){ ?>
            <?   if ($v=="") continue; ?>
                <option value="<?=$k?>"><?=$v?></option>
            <? } ?>
            </select><span class="instruction"> ※用途の追加・削除・編集はできません</span>
        </td></tr>
        <tr><th>登録済項目</th><td id="efmMstOptions_opt_container"><select></select></td></tr>
    </table>
    </div>
    <hr>

    <table cellspacing="0" cellpadding="0" style="width:auto">
    <tr><td></td><td>コード</td><td>項目名</td><td>温度板短縮表示</td><td>従属選択肢</td><td></td></tr>
    <tr><td class="dot_title" style="padding-right:30px">選択項目の名称変更</td>
        <td><input type="text" class="text imeoff" name="mod_opt_cd" style="width:80px" title="半角のみ、最大２０文字" maxlength="20" /></td>
        <td><input type="text" class="text" name="mod_opt_name" style="width:230px" title="最大３０文字" maxlength="30" /></td>
        <td><input type="text" class="text" name="mod_opt_alias" style="width:130px" title="最大３０文字" maxlength="30" /></td>
        <td><input type="text" class="text" name="mod_opt_comma_selection" style="width:160px" title="最大２０００文字" maxlength="2000" /></td>
        <td><button type="button" onclick="dfmMstOptions.saveOption('mod_opt')" class="w60">変更</button></td>
    </tr>
    <tr><td class="dot_title">項目の追加</td>
        <td><input type="text" class="text imeoff" name="add_opt_cd" style="width:80px" title="半角のみ、最大２０文字" maxlength="20" /></td>
        <td><input type="text" class="text" name="add_opt_name" style="width:230px" title="最大３０文字" maxlength="30" /></td>
        <td><input type="text" class="text" name="add_opt_alias" style="width:130px" title="最大３０文字" maxlength="30" /></td>
        <td><input type="text" class="text" name="add_opt_comma_selection" style="width:160px" title="最大５００文字" maxlength="500" /></td>
        <td><button type="button" onclick="dfmMstOptions.saveOption('add_opt')" class="w60">追加</button></td>
    </tr>
    <tr><td class="dot_title">選択項目の削除</td>
        <td colspan="4"><button type="button" onclick="dfmMstOptions.saveOption('del_opt')" class="w60">削除</button></td>
    </tr></table>

    <div style="padding-top:30px" class="instruction">※コードは選択肢表示順に利用されます。任意な値を指定できます。(半角のみ)</div>
    <div class="instruction">※項目名は、用途ごとに一意名の必要があります。</div>
    <div class="instruction">※温度板短縮名を指定しない場合、温度板へは項目名がそのまま表示されます。</div>
    <div class="instruction">※従属選択肢は、半角カンマ区切りで複数指定してください。（「ケア：観察項目」のみ）</div>
    <div class="instruction">※ここで項目の名称変更や削除を行っても、患者へ適用済の情報については、既に実績化されており、影響しません。</div>
    <div class="instruction">※既に患者へ指定済の項目名や短縮表示を一括で変更することはできません。必要に応じ、箇所ごとにひとつずつ変更してください。</div>
    <div class="instruction" style="padding-top:6px; color:#ff8888">
        【短縮表示を変更／削除した後で、各所プルダウンを再選択する場合について】<br>
        ※既に患者へ設定した値は、同一のデータがマスタ側に存在しない場合、プルダウン選択肢の末尾に加えられます。<br>
        ※本画面で「項目名を変更せず、短縮表示のみ変更した場合」においては、<br>
        &nbsp;&nbsp;&nbsp;プルダウン選択肢では、「あたかも同一選択肢が２つ表示」されますが、内部的には短縮名が異なります。<br>
        &nbsp;&nbsp;&nbsp;この場合、上位に表示された方を再選択してください。
    </div>
</form>
<script type="text/javascript">
var dfmMstOptions = {
    form:document.efmMstOptions,
    opt_group : "",
    opt_name : "",
    mstOptions : {},
    popup : function(){
        this.reloadMasterData();
        bindDataToForm("efmMstOptions", {});
        this.opt_group = this.form.opt_group.value;
        this.categoryChanged(this.opt_group);
        $('#efmMstOptions').dialog({ title:'薬剤・選択肢マスタ', width:900, height:550 });
    },
    reloadMasterData : function() {
        var self = this;
        $.ajax({ url:baseUrl+"&ajax_action=get_mst_options", success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            self.mstOptions = obj;
        }});
    },
    getOptionDispName : function(opt_cd, opt_name) {
        if (opt_name=="") return "";
        return htmlEscape(opt_cd)+ "：  " +htmlEscape(opt_name);
    },
    categoryChanged : function(_opt_group) {
        var med_options = [];
        this.opt_group = _opt_group;
        for (var idx=0; idx<this.mstOptions[this.opt_group].length; idx++) {
            var obj = this.mstOptions[this.opt_group][idx];
            med_options.push('<option value="'+htmlEscape(obj.opt_name).replace(/&nbsp;/g, ' ')+'">'+this.getOptionDispName(obj.opt_cd, obj.opt_name)+'</option>');
        }
        ee("efmMstOptions_opt_container").innerHTML =
        '<select name="opt_name" onchange="dfmMstOptions.optionChanged(this.value)">'+med_options.join("")+'</select>';
        this.optionChanged(getComboValue(this.form.opt_name));
    },
    optionChanged : function(_opt_name) {
        this.opt_name = _opt_name;
        for (var idx=0; idx<this.mstOptions[this.opt_group].length; idx++) {
            var obj = this.mstOptions[this.opt_group][idx];
            if (obj.opt_name!=this.opt_name) continue;
            this.form.mod_opt_name.value = obj.opt_name;
            this.form.mod_opt_cd.value = obj.opt_cd;
            this.form.mod_opt_alias.value = obj.opt_alias;
            this.form.mod_opt_comma_selection.value = obj.opt_comma_selection;
            return;
        }
        this.form.mod_opt_name.value = "";
        this.form.mod_opt_cd.value = "";
        this.form.mod_opt_alias.value = "";
        this.form.mod_opt_comma_selection.value = "";
    },
    saveOption : function(mode, tryCommit) {
        var self = this;
        if (!tryCommit) tryCommit = "";
        if (tryCommit) {
            var msg = "変更";
            if (mode.substring(0,3)=="add") msg= "追加";
            if (mode.substring(0,3)=="del") msg= "削除";
            if (!confirm(msg+"します。よろしいですか？")) return;
        }
        var sData = serializeUnicodeEntity("efmMstOptions");
        $.ajax({ url:baseUrl+"&ajax_action=save_mst_options&mode="+mode+"&try_commit="+tryCommit, data:sData, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if (obj==AJAX_ERROR) return;
            if (!tryCommit) { self.saveOption(mode, 1); return; }
            self.reloadMasterData();
            bindDataToForm("efmMstOptions", {});
            setComboValue(self.form.opt_group, obj.opt_group);
            self.categoryChanged(getComboValue(self.form.opt_group));
            setComboValue(self.form.opt_name, obj.opt_name, self.getOptionDispName(obj.opt_cd, obj.opt_name));
            self.optionChanged(getComboValue(self.form.opt_name));
        }});
    }
}
</script>










<? //**************************************************************************************************************** ?>
<? // （Ｄ−４）                                                                                                      ?>
<? // BSスケール表（院内固定・患者個別）マスタ                                                                        ?>
<? //**************************************************************************************************************** ?>
<form id="efmMstBSScale" name="efmMstBSScale">
    <input type="hidden" name="bs_group" />
    <div id="efmMstBSScale_target_name" style="padding-top:8px"></div>
    <hr>
    <div style="padding-bottom:5px">
    <select onchange="dfmMstBSScale.changeCount(this.value)" name="field_count">
        <? for ($idx=1; $idx<=12; $idx++) { ?><option value="<?=$idx?>"><?=$idx?></option><? } ?>
    </select></div>
    <table cellspacing="0" cellpadding="0" class="" style="table-layout:fixed; width:auto"><tr>
    <? for ($cidx=0; $cidx<=1; $cidx++) { ?>
        <td style="width:430px; <?=($cidx==1?'padding-left:20px;':'')?>">
        <table cellspacing="0" cellpadding="0" class="general_list" style="table-layout:fixed; width:100%">
            <tr><th style="width:110px">測定値</th><th style="border-right:0; width:auto">薬剤</th><th style="border-left:0; width:80px">量</th></tr>
            <? for ($ridx=1; $ridx<=6; $ridx++) { ?>
            <? $idx = ($cidx * 6) + $ridx; ?>
            <?     $id = "efmMstBSScale__".$idx."__"; ?>
            <tr id="<?=$id?>tr">
                <td class="right">
                    <span id="<?=$id?>span"></span>
                    <input type="text" title="半角数値のみ、最大３文字" pattern="[0-9]*" class="text num3" maxlength="3" name="<?=$id?>bs_upper"
                        id="<?=$id?>bs_upper" onkeyup="dfmMstBSScale.changeNext(<?=$idx?>, value)" onchange="dfmMstBSScale.changeNext(<?=$idx?>, value)" />
                </td>
                <td colspan="2"><nobr>
                    <input type="text" class="text" style="width:180px" name="<?=$id?>bs_name1" id="<?=$id?>bs_name1" title="最大３０文字"
                        maxlength="30" /><button type="button" onclick="dfmMstBSScale.showAssist('<?=$id?>bs_name1')" class="w60">候補</button><input
                        type="text" title="半角英数＋ピリオドのみ、最大１０文字" onfocus="ipadHackFocusIn(this)" pattern="[0-9a-zA-Z\.]*" class="text num3" name="<?=$id?>bs_ryou1"
                        id="<?=$id?>bs_ryou1" style="width:60px" maxlength="10" /><br/>
                    <input type="text" class="text" style="width:180px" name="<?=$id?>bs_name2" id="<?=$id?>bs_name2" title="最大３０文字"
                        maxlength="30" /><button type="button" onclick="dfmMstBSScale.showAssist('<?=$id?>bs_name2')" class="w60">候補</button><input
                        type="text" title="半角英数＋ピリオドのみ、最大１０文字" onfocus="ipadHackFocusIn(this)" pattern="[0-9a-zA-Z\.]*" class="text num3" name="<?=$id?>bs_ryou2"
                        id="<?=$id?>bs_ryou2" style="width:60px" maxlength="10" /></nobr>
                </td>
            </tr>
            <? } ?>
        </table>
        </td>
    <? } ?>
    </tr></table>
</form>
<script type="text/javascript">
var dfmMstBSScale = {
    mstBSScale : {},
    bsCallbackFunc : 0,
    ptif_id : "",
    bs_type : "",
    bsscale_captions : {
        BSSCALE_COMMON0:"院内固定O",BSSCALE_COMMON1:"院内固定I",BSSCALE_COMMON2:"院内固定II",
        BSSCALE_COMMON3:"院内固定III",BSSCALE_COMMON4:"院内固定IV",PATIENT:"患者個別"
    },
    popup : function(bs_type, _ptif_id, ptif_name, bsCallbackFunc){
        this.bsCallbackFunc = bsCallbackFunc;
        var self = this;
        this.bs_type = bs_type;
        this.ptif_id = _ptif_id;
        this.reloadMasterData(_ptif_id);
        var prev = 0;
        var typeDisp = this.bsscale_captions[bs_type];
        if (ptif_name) typeDisp += "（"+ptif_name+"）";
        ee("efmMstBSScale_target_name").innerHTML  = "編集対象スケールタイプ："+typeDisp;
        var len = this.mstBSScale[bs_type].length;
        for (var idx=1; idx<=12; idx++) {
            var obj = (idx<=len ? this.mstBSScale[bs_type][idx-1] : {});
            var id = "efmMstBSScale__"+idx+"__";
            ee(id+"span").innerHTML = prev + "〜";
            ee(id+"bs_upper").value = str(obj["bs_upper"]);
            ee(id+"bs_ryou1").value = str(obj["bs_ryou1"]);
            ee(id+"bs_ryou2").value = str(obj["bs_ryou2"]);
            ee(id+"bs_name1").value = str(obj["bs_name1"]);
            ee(id+"bs_name2").value = str(obj["bs_name2"]);
            prev = str(obj["bs_upper"]);
            if (prev) { prev = int(prev); prev++; }
        }
        var buttons = ["save"];
        if (len) buttons = ["save", "delete"];
        if (!len) len = 1;
        setComboValue(document.efmMstBSScale.field_count, len);
        this.changeCount(len);
        $('#efmMstBSScale').dialog({ title:'BSスケール表編集', width:900, height:460, extButtons:buttons,
            saveExec:function() { return self.saveMstBSScale(bs_type, _ptif_id); }, deleteExec:function() { return self.deleteMstBSScale(bs_type, _ptif_id);}
        });
    },
    // 固定院内設定４つと、患者設定を取得
    reloadMasterData : function(_ptif_id) {
        var self = this;
        $.ajax({ url:baseUrl+"&ajax_action=get_mst_bsscale&ptif_id="+uEnc(_ptif_id), success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            self.mstBSScale = obj;
        }});
    },
    changeCount : function (cnt) {
        for (var idx=1; idx<=12; idx++) ee("efmMstBSScale__"+idx+"__tr").style.display = (idx<=cnt ? "" : "none");
    },
    changeNext : function (idx, v) {
        idx = int(idx) + 1;
        if (idx>12) return;
        ee("efmMstBSScale__"+idx+"__span").innerHTML = (v!="" ? (int(v)+1) : "") +"〜";
    },
    saveMstBSScale : function(bs_type, _ptif_id, tryCommit) {
        var self = this;
        var ret = false;
        if (!tryCommit) tryCommit = "";
        if (tryCommit && !confirm("保存します。よろしいですか？")) return ret;
        var sData = serializeUnicodeEntity("efmMstBSScale");
        $.ajax({ url:baseUrl+"&ajax_action=save_mst_bsscale&bs_type="+bs_type+"&ptif_id="+_ptif_id+"&try_commit="+tryCommit, data:sData, success:function(msg){
            if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
            if (!tryCommit) { ret = self.saveMstBSScale(bs_type, _ptif_id, 1); return; }
            self.reloadMasterData(self.ptif_id);
            if (self.bsCallbackFunc) self.bsCallbackFunc();
            ret = true;
        }});
        return ret;
    },
    deleteMstBSScale : function (bs_type, _ptif_id) {
        var self = this;
        if (!confirm("削除します。よろしいですか？")) return false;
        var ret = false;
        $.ajax({ url:baseUrl+"&ajax_action=delete_mst_bsscale&bs_type="+bs_type+"&ptif_id="+_ptif_id, success:function(msg) {
            if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
            if (self.bsCallbackFunc) self.bsCallbackFunc();
            ret = true;
        }});
        return ret;
    },
    showAssist : function(targetId) {
        var optCallbackFunc = function(opt_name) { ee(targetId).value = opt_name; }
        dfmOptAssist.popup(dfmMstOptions.mstOptions["insulin_bs"], optCallbackFunc, 350);
    }
}
</script>










<? //**************************************************************************************************************** ?>
<? // （Ｅ−１）                                                                                                      ?>
<? // ケア入力（酸素）                                                                                      ?>
<? //**************************************************************************************************************** ?>
<form id="efmCareSansoTemplate">
    <div class="repeat_object_template" namespace="sanso"><div>
        <input type="hidden" basename="ymdhm_fr" />
        <button type="button" class="ymd_button" basename="fr_ymd@button" onclick="dfmCareSanso.popupCalendar(this)">&nbsp;</button>
        <input type="hidden" basename="fr_ymd" />
        <button type="button" class="hm_button" basename="fr_hm@button" onclick="dfmCareSanso.popupTimeAssist(this)">:</button>
        <input type="hidden" basename="fr_hm" />
        〜
        <button type="button" class="ymd_button" basename="to_ymd@button" onclick="dfmCareSanso.popupCalendar(this)">&nbsp;</button>
        <input type="hidden" basename="to_ymd" />
        <button type="button" class="hm_button" basename="to_hm@button" onclick="dfmCareSanso.popupTimeAssist(this)">:</button>
        <input type="hidden" basename="to_hm" />
        <input type="text" title="半角数値のみ、小数１桁まで、最大４文字" onfocus="ipadHackFocusIn(this)" pattern="^[0-9]?[0-9]?([0-9][\.][0-9])?$" class="text num3" basename="litter" maxlength="4" /><span>L/min</span>
        <select opt_group="sanso_kigu" basename="kigu"></select>
        <button type="button" onclick="{TOGGLE_EVENT}" class="w60">削除</button>
    </div></div>

    <div class="repeat_object_template" namespace="kokyuki"><div>
        <input type="hidden" basename="ymdhm_fr" />
        <button type="button" class="ymd_button" basename="fr_ymd@button" onclick="dfmCareSanso.popupCalendar(this)">&nbsp;</button>
        <input type="hidden" basename="fr_ymd" />
        <button type="button" class="hm_button" basename="fr_hm@button" onclick="dfmCareSanso.popupTimeAssist(this)">:</button>
        <input type="hidden" basename="fr_hm" />
        〜
        <button type="button" class="ymd_button" basename="to_ymd@button" onclick="dfmCareSanso.popupCalendar(this)">&nbsp;</button>
        <input type="hidden" basename="to_ymd" />
        <button type="button" class="hm_button" basename="to_hm@button" onclick="dfmCareSanso.popupTimeAssist(this)">:</button>
        <input type="hidden" basename="to_hm" />
        <span>酸素濃度</span>
        <input type="text" title="半角数値のみ、小数１桁まで、最大４文字" pattern="^[0-9]?[0-9]?[0-9]?([0-9][\.][0-9])?$" class="text num4" basename="noudo" maxlength="4" />
        <select opt_group="sanso_noudo_unit" basename="noudo_unit"></select>
        <button type="button" onclick="{TOGGLE_EVENT}" class="w60">削除</button>
    </div></div>
</form>
<form id="efmCareSanso" name="efmCareSanso">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="sijibi"></th><td></td>
    </tr></table></div>
    <table cellspacing="0" cellpadding="0"><tr>
    <th style="width:100px">酸素吸入</th>
    <td class="repeat_object_container" namespace="sanso"></td>
    <td class="bottom" style="width:50px"><button type="button" onclick="repeatObjectFieldAdd('efmCareSanso','sanso')" class="w60">追加</button></td>
    </tr></table>

    <hr>
    <table cellspacing="0" cellpadding="0"><tr>
    <th style="width:100px">人工呼吸器</th>
    <td><table cellspacing="0" cellpadding="0"><tr>
            <td class="repeat_object_container" namespace="kokyuki"></td>
            <td class="bottom" style="width:50px"><button type="button" onclick="repeatObjectFieldAdd('efmCareSanso','kokyuki')" class="w60">追加</button></td>
        </tr></table>
    </td></tr></table>
    <div class="instruction" style="padding:10px 0 0 10px">※開始日時、終了日時ともに指定されていない場合は、無効データとみなされ、登録されません。<br>&nbsp;&nbsp;&nbsp;登録済データの開始日時と終了日時をクリアすると、保存時に自動的に削除されます。</div>
</form>
<script type="text/javascript">
var dfmCareSanso = {
    sijibi : "",
    popup : function(ymd){
        this.sijibi = ymd;
        var saveCareSanso = function() {
            if (!checkInputError("efmCareSanso")) return false;
            var ret = false;
            var sData = serializeUnicodeEntity("efmCareSanso");
            var url = baseUrl+"&ajax_action=save_care_kikan&care_content_list=sanso,kokyuki&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd;
            $.ajax({ url:url, data:sData, success:function(msg) {
                if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
                $('#efmCareSanso').dialog("close");
                dfmMainMenu.changeView(view_mode, ptif_id, ext_view_mode);
                ret = true;
            }});
            return ret;
        }
        var url = baseUrl+"&ajax_action=get_care_kikan_per_day_list&care_content_list=sanso,kokyuki&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd;
        $.ajax({ url:url, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            var aYmd = ymdToArray(ymd);
            obj["sijibi"] = "指示日："+aYmd.slash_mdw;
            bindDataToForm("efmCareSanso", obj, ymd);
            $('#efmCareSanso').dialog({
                title:'ケア入力（酸素）', width:900, height:550, extButtons:["up","down","save"], saveExec:saveCareSanso
            });
        }});
    },
    popupCalendar : function(btn) {
        var nm = btn.getAttribute("name").split("@");
        var hidden = getFormElementByName(document.efmCareSanso, nm[0]);
        var ymdCallbackFunc = function(ymd, aYmd) {
            hidden.value = ymd;
            btn.innerHTML = (aYmd.slash_mdw!=""?aYmd.slash_mdw:"&nbsp;");
        }
        dfmYmdAssist.popup("ymd", hidden.value, this.sijibi, ymdCallbackFunc);
    },
    popupTimeAssist : function(btn) {
        var nm = btn.getAttribute("name").split("@");
        var hidden = getFormElementByName(document.efmCareSanso, nm[0]);
        var callbackFunc = function(hm, colon_hm) {
            hidden.value = hm;
            btn.innerHTML = colon_hm;
        }
        dfmHMAssist.popup(hidden.value, callbackFunc);
    }
}
</script>










<? //**************************************************************************************************************** ?>
<? // （Ｅ−２）                                                                                                      ?>
<? // ケア入力（抗生剤）                                                                                    ?>
<? //**************************************************************************************************************** ?>
<form id="efmCareKouseizaiTemplate">
    <table>
    <tbody class="repeat_object_template" namespace="kouseizai">
        <tr>
        <td>
            <input type="hidden" basename="sijibi_ymd" />
            <button type="button" class="ymd_button" basename="sijibi_ymd@button" onclick="dfmCareKouseizai.popupCalendar(this)">&nbsp;</button>
            <select basename="siji_az" opt_group="siji_az" style="width:50px"></select>
        </td>
        <td>
            <input type="hidden" basename="ymdhm_fr" />
            <button type="button" class="ymd_button" basename="fr_ymd@button" onclick="dfmCareKouseizai.popupCalendar(this)">&nbsp;</button>
            <input type="hidden" basename="fr_ymd" />
            <button type="button" class="hm_button" basename="fr_hm@button" onclick="dfmCareKouseizai.popupTimeAssist(this)">:</button>
            <input type="hidden" basename="fr_hm" />
            <!--
            〜
            <button type="button" class="ymd_button" basename="to_ymd@button" onclick="dfmCareKouseizai.popupCalendar(this)">&nbsp;</button>
            <button type="button" class="hm_button" basename="to_hm@button" onclick="dfmCareKouseizai.popupTimeAssist(this)">:</button>
            -->
            <input type="hidden" basename="to_ymd" />
            <input type="hidden" basename="to_hm" />
            <select opt_group="kouseizai" basename="med_name"></select>
            <button type="button" onclick="{TOGGLE_EVENT}" class="w60">削除</button>
        </td>
        </tr>
    </tbody>
    </table>
</form>
<form id="efmCareKouseizai" name="efmCareKouseizai">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="sijibi"></th><td></td>
        <td class="right"><button type="button" onclick="repeatObjectFieldAdd('efmCareKouseizai','kouseizai')" class="w60">追加</button></td>
    </tr></table></div>

    <table cellspacing="0" cellpadding="0" class="general_list">
        <tr><th style="width:150px; text-align:left; padding-left:30px">指示</th>
        <th style="width:auto; text-align:left; padding-left:30px">薬剤</th></tr>
        <tbody class="repeat_object_container" namespace="kouseizai"></tbody>
    </table>

    <div class="instruction" style="padding:10px 0 0 0">※開始日時が指定されていない場合は、無効データとみなされ、登録されません。<br>&nbsp;&nbsp;&nbsp;登録済データの開始日時をクリアすると、保存時に自動的に削除されます。</div>
</form>
<script type="text/javascript">
var dfmCareKouseizai = {
    sijibi : "",
    siji_az  : "",
    ptif_id : "",
    popup : function(_ptif_id, ymd, siji_az){
        var self = this;
        this.sijibi = ymd;
        this.ptif_id = _ptif_id;
        this.siji_az = str(siji_az);
        var saveCareKouseizai = function() {
            if (!checkInputError("efmCareKouseizai")) return false;
            var ret = false;
            var sData = serializeUnicodeEntity("efmCareKouseizai");
            var url = baseUrl+"&ajax_action=save_care_kikan&care_content_list=kouseizai&ptif_id="+uEnc(self.ptif_id)+"&ymd="+ymd;
            $.ajax({ url:url, data:sData, success:function(msg) {
                if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
                $('#efmCareKouseizai').dialog("close");
                dfmMainMenu.changeView(view_mode, self.ptif_id, ext_view_mode);
                ret = true;
            }});
            return ret;
        }
        var url = baseUrl+"&ajax_action=get_care_kikan_per_day_list&care_content_list=kouseizai&ptif_id="+uEnc(self.ptif_id)+"&ymd="+ymd;
        $.ajax({ url:url, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            if (!obj || int(obj["kouseizai__field_count"])<1) {
                obj["kouseizai__field_count"] = 1;
                obj["kouseizai__1__siji_az"] = self.siji_az;
                obj["kouseizai__1__sijibi_ymd"] = self.sijibi;
                obj["kouseizai__1__fr_ymd"] = self.sijibi;
                obj["kouseizai__1__to_ymd"] = self.sijibi;
            }
            var aYmd = ymdToArray(ymd);
            obj["sijibi"] = "指示日："+aYmd.slash_mdw;
            bindDataToForm("efmCareKouseizai", obj, ymd);
            $('#efmCareKouseizai').dialog({
                title:'ケア入力（抗生剤）', width:900, height:350, extButtons:["up","down","save"], saveExec:saveCareKouseizai
            });
        }});
    },
    popupCalendar : function(btn) {
        var nm = btn.getAttribute("name").split("@");
        var hidden_fr = getFormElementByName(document.efmCareKouseizai, nm[0]);
        var hidden_to = getFormElementByName(document.efmCareKouseizai, nm[0].replace("fr","to"));
        var ymdCallbackFunc = function(ymd, aYmd) {
            hidden_fr.value = ymd;
            hidden_to.value = ymd;
            btn.innerHTML = (aYmd.slash_mdw!=""?aYmd.slash_mdw:"&nbsp;");
        }
        dfmYmdAssist.popup("ymd", hidden_fr.value, this.sijibi, ymdCallbackFunc);
    },
    popupTimeAssist : function(btn) {
        var nm = btn.getAttribute("name").split("@");
        var hidden_fr = getFormElementByName(document.efmCareKouseizai, nm[0]);
        var hidden_to = getFormElementByName(document.efmCareKouseizai, nm[0].replace("fr","to"));
        var callbackFunc = function(hm, colon_hm) {
            hidden_fr.value = hm;
            hidden_to.value = hm;
            btn.innerHTML = colon_hm;
        }
        dfmHMAssist.popup(hidden_fr.value, callbackFunc);
    }
}
</script>









<? //**************************************************************************************************************** ?>
<? // （Ｅ−３）                                                                                                      ?>
<? // ケア入力（食事・IN）                                                                                  ?>
<? //**************************************************************************************************************** ?>
<form id="efmCareSyokujiInTemplate">
    <div class="repeat_object_template" namespace="hosyoku"><div>
        <table cellspacing="0" cellpadding="0"><tr>
            <td>
                <input type="text" class="text" style="width:80px" basename="hosyoku_timing" title="最大２０文字" maxlength="20" /><button type="button"
                    onclick="dfmCareSyokujiIn.showAssist(this)" class="w60" basename="hosyoku_timing@btn">候補</button>
                <span>種類</span><select opt_group="hosyoku" basename="hosyoku"></select>
                <span>摂取量</span><select opt_group="hosyoku_ryou" basename="hosyoku_ryou"></select></td>
            <td>
                <button type="button" onclick="{TOGGLE_EVENT}" class="w60">削除</button>
            </td>
        </tr></table>
    </div></div>
</form>
<form id="efmCareSyokujiIn" name="efmCareSyokujiIn">

    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="sijibi"></th><td></td>
    </tr></table></div>

    <table cellspacing="0" cellpadding="0" style="width:auto"><tr>
        <th style="width:90px"><nobr>経口食事量</nobr></th>
        <td navi="食事量は「0」割〜「10」割で指定します。<br>または、ひとクチふたクチの場合は、小数を指定してください。ひとクチなら「0.1」、最大９クチ「0.9」が指定できます。">



        <table cellspacing="0" cellpadding="0" class="general_list" style="width:auto">
        <tr>
            <th>
                <span style="padding:0 40px 0 10px">朝</span>
            <button type="button" class="w60" onclick="dfmCareSyokujiIn.setKeikouAll('asa', '10')">10割</button><button type="button" class="w60" onclick="dfmCareSyokujiIn.setKeikouAll('asa', '')">クリア</button>
            </th>
            <th>
                <span style="padding:0 40px 0 10px">昼</span>
            <button type="button" class="w60" onclick="dfmCareSyokujiIn.setKeikouAll('hiru', '10')">10割</button><button type="button" class="w60" onclick="dfmCareSyokujiIn.setKeikouAll('hiru', '')">クリア</button>
            </th>
            <th>
                <span style="padding:0 40px 0 10px">夕</span>
            <button type="button" class="w60" onclick="dfmCareSyokujiIn.setKeikouAll('yu', '10')">10割</button><button type="button" class="w60" onclick="dfmCareSyokujiIn.setKeikouAll('yu', '')">クリア</button>
            </th>
        </tr>
        <tr>
            <td><nobr>
                <span>副</span><input type="text" title="半角数値のみ、最大２文字、0〜10まで／または0.1〜0.9まで" onfocus="ipadHackFocusIn(this)" pattern="^(0|1|2|3|4|5|6|7|8|9|10|0\.1|0\.2|0\.3|0\.4|0\.5|0\.6|0\.7|0\.8|0\.9)?$" class="text num2" name="keikou_asa_fukusyoku" maxlength="3" />／<span>主</span><input type="text" title="半角数値のみ、最大２文字、0〜10まで／または0.1〜0.9まで" onfocus="ipadHackFocusIn(this)" pattern="^(0|1|2|3|4|5|6|7|8|9|10|0\.1|0\.2|0\.3|0\.4|0\.5|0\.6|0\.7|0\.8|0\.9)?$" class="text num2" name="keikou_asa_syusyoku" maxlength="3" />
                <label style="padding-right:10px"><input type="checkbox" class="checkbox" name="keikou_asa_zessyoku" onclick="dfmCareSyokujiIn.changeKeikouZessyoku('asa')" value="1">絶食</label>
            </nobr>
            </td>

            <td><nobr>
                <span>副</span><input type="text" title="半角数値のみ、最大２文字、0〜10まで／または0.1〜0.9まで" onfocus="ipadHackFocusIn(this)" pattern="^(0|1|2|3|4|5|6|7|8|9|10|0\.1|0\.2|0\.3|0\.4|0\.5|0\.6|0\.7|0\.8|0\.9)?$" class="text num2" name="keikou_hiru_fukusyoku" maxlength="3" />／<span>主</span><input type="text" title="半角数値のみ、最大２文字、0〜10まで／または0.1〜0.9まで" onfocus="ipadHackFocusIn(this)" pattern="^(0|1|2|3|4|5|6|7|8|9|10|0\.1|0\.2|0\.3|0\.4|0\.5|0\.6|0\.7|0\.8|0\.9)?$" class="text num2" name="keikou_hiru_syusyoku" maxlength="3" />
                <label style="padding-right:10px"><input type="checkbox" class="checkbox" name="keikou_hiru_zessyoku" onclick="dfmCareSyokujiIn.changeKeikouZessyoku('hiru')" value="1">絶食</label>
            </nobr>
            </td>
            <td><nobr>
                <span>副</span><input type="text" title="半角数値のみ、最大２文字、0〜10まで／または0.1〜0.9まで" onfocus="ipadHackFocusIn(this)" pattern="^(0|1|2|3|4|5|6|7|8|9|10|0\.1|0\.2|0\.3|0\.4|0\.5|0\.6|0\.7|0\.8|0\.9)?$" class="text num2" name="keikou_yu_fukusyoku" maxlength="3" />／<span>主</span><input type="text" title="半角数値のみ、最大２文字、0〜10まで／または0.1〜0.9まで" onfocus="ipadHackFocusIn(this)" pattern="^(0|1|2|3|4|5|6|7|8|9|10|0\.1|0\.2|0\.3|0\.4|0\.5|0\.6|0\.7|0\.8|0\.9)?$" class="text num2" name="keikou_yu_syusyoku" maxlength="3" />
                <label style="padding-right:10px"><input type="checkbox" class="checkbox" name="keikou_yu_zessyoku" onclick="dfmCareSyokujiIn.changeKeikouZessyoku('yu')" value="1">絶食</label>
            </nobr>
            </td>

        </tr></table>
        </td>
        <td style="vertical-align:bottom; padding-left:10px">

        <nobr>
        </nobr>
        </td>
    </tr></table>


    <hr>
    <table cellspacing="0" cellpadding="0"><tr>
        <th style="width:90px">経管食事量</th>
        <td>
            <div id="efmCareSyokujiIn_keikan_container" style="padding-top:5px"></div>
        </td>
    </tr></table>


    <div style="display:none" id="old_hosyoku_input">
        <input type="text" class="text" style="width:80px" name="hosyoku_timing" title="最大２０文字" maxlength="20" />
        <select opt_group="hosyoku" name="hosyoku"></select>
        <select opt_group="hosyoku_ryou" name="hosyoku_ryou"></select>
    </div>


    <hr>
    <table cellspacing="0" cellpadding="0"><tr>
    <th style="width:90px">補食</th>
    <td><table cellspacing="0" cellpadding="0"><tr>
            <td class="repeat_object_container" namespace="hosyoku"></td>
            <td class="bottom" style="width:50px"><button type="button" onclick="repeatObjectFieldAdd('efmCareSyokujiIn','hosyoku')" class="w60">追加</button></td>
        </tr></table>
    </td></tr></table>

    <hr>
    <table cellspacing="0" cellpadding="0" style="width:auto"><tr>
        <th style="width:90px">水分摂取量</th>
        <td><select name="suibun_unit" onchange="dfmCareSyokujiIn.changeSuibunUnit()">
            <option></option><option value="経口_割">経口(割)</option><option value="経口_mL">経口(mL)</option><option value="経管_mL">経管</option></select>
            <span id="efmCareSyokujiIn_suibun_container">
            �� <input type="text" title="半角数値のみ、最大４文字" onfocus="ipadHackFocusIn(this)" pattern="[0-9]*" class="text num3" name="suibun_ryou_1" maxlength="4"><span id="efmCareSyokujiIn_suibun_unit1" style="padding-right:20px"></span>
            �� <input type="text" title="半角数値のみ、最大４文字" onfocus="ipadHackFocusIn(this)" pattern="[0-9]*" class="text num3" name="suibun_ryou_2" maxlength="4"><span id="efmCareSyokujiIn_suibun_unit2" style="padding-right:20px"></span>
            �� <input type="text" title="半角数値のみ、最大４文字" onfocus="ipadHackFocusIn(this)" pattern="[0-9]*" class="text num3" name="suibun_ryou_3" maxlength="4"><span id="efmCareSyokujiIn_suibun_unit3" style="padding-right:20px"></span>
            �� <input type="text" title="半角数値のみ、最大４文字" onfocus="ipadHackFocusIn(this)" pattern="[0-9]*" class="text num3" name="suibun_ryou_4" maxlength="4"><span id="efmCareSyokujiIn_suibun_unit4" style="padding-right:20px"></span>
            �� <input type="text" title="半角数値のみ、最大４文字" onfocus="ipadHackFocusIn(this)" pattern="[0-9]*" class="text num3" name="suibun_ryou_5" maxlength="4"><span id="efmCareSyokujiIn_suibun_unit5" style="padding-right:20px"></span>
            </span>
        </td>
    </tr></table>
</form>
<script type="text/javascript">
var dfmCareSyokujiIn = {
    sijibi : "",
    weekNumber : "",
    keikanSijiObj : 0,
    form : document.efmCareSyokujiIn,
    popup : function(ymd){
        var self = this;
        this.sijibi = ymd;
        var aYmd = ymdToArray(ymd);
        var self = this;
        var saveCareSyokujiIn = function() {
            if (!checkInputError("efmCareSyokujiIn")) return false;
            var ret = false;
            var sData = serializeUnicodeEntity("efmCareSyokujiIn");
            $.ajax({ url:baseUrl+"&ajax_action=save_care_ymd&section=in&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd, data:sData, success:function(msg) {
                if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
                $('#efmCareSyokujiIn').dialog("close");
                dfmMainMenu.changeView(view_mode, ptif_id, ext_view_mode);
                ret = true;
            }});
            return ret;
        }
        var sijiObj = this.loadSiji();
        $.ajax({ url:baseUrl+"&ajax_action=get_care_ymd&section=in&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            if (!int(obj["hosyoku__field_count"])) { // 補食は単数入力から複数入力（テンプレート型）になった。
                obj["hosyoku__field_count"] = 1;
                obj["hosyoku__1__hosyoku_timing"] = obj["hosyoku_timing"];
                obj["hosyoku__1__hosyoku"] = obj["hosyoku"];
                obj["hosyoku__1__hosyoku_ryou"] = obj["hosyoku_ryou"];
            }
            var aYmd = ymdToArray(self.sijibi);
            self.weekNumber = aYmd["weekNumber"];
            obj["sijibi"] = "指示日："+aYmd.slash_mdw;
            obj["keikan__field_count"] = self.keikanSijiObj["keikan__field_count"];
            for (var idx=1; idx<=int(obj["keikan__field_count"]); idx++) {
                obj["keikan__"+idx+"__eiyouzai"] = getAlias(self.keikanSijiObj["eiyouzai"+idx]);
            }
            bindDataToForm("efmCareSyokujiIn", obj);
            self.changeSuibunUnit();
            $('#efmCareSyokujiIn').dialog({
                title:'ケア入力（食事・IN）', width:900, height:550, extButtons:["save"], dlgFooter:obj,
                saveExec:saveCareSyokujiIn
            });
            var ahy = ["asa", "hiru", "yu"];
            for (var ahyIdx in ahy) self.changeKeikouZessyoku(ahy[ahyIdx]);
            var keikan_field_count = int(obj["keikan__field_count"]);
            for (var fcnt=1; fcnt<=keikan_field_count; fcnt++) {
                for (var ahyIdx in ahy) {
                    self.changeKeikanZessyoku(fcnt, ahy[ahyIdx]);
                }
            }
            self.changeSuibunUnit(); // 最初の一回ではmaxlengthがうまく切り替わらない。もう一回やる
            self.form.suibun_unit.options.length = 4;
        }});
    },
    loadSiji : function() {
        var sijiObj = {};
        var self = this;
        var url = baseUrl+"&ajax_action=get_siji_ymd&section=insulin_fix&ptif_id="+uEnc(ptif_id)+"&ymd="+this.sijibi+"&is_get_last_siji=1";
        $.ajax({ url:url, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            sijiObj = obj;
        }});
        var url = baseUrl+"&ajax_action=get_siji_ymd&section=keikan&ptif_id="+uEnc(ptif_id)+"&ymd="+this.sijibi+"&is_get_last_siji=1";
        $.ajax({ url:url, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            var aYmd = ymdToArray(str(obj["siji_ymd"]));
            var html = [];
            var field_count = int(obj["keikan__field_count"]);
            html.push('<input type="hidden" name="keikan__field_count" value="'+field_count+'" />');
            var isNoSiji = 0;
            if (field_count < 1) {
                field_count = 1;
                isNoSiji = 1;
                obj["eiyouzai1"] = "（指示はありません）";
            }
            var siji_ymd = (isNoSiji ? "" : '指示日：<span style="color:red; padding-right:20px">'+aYmd["slash_mdw"]+'</span>');
            html.push('<table cellspacing="0" cellpadding="0" class="general_list">');
            html.push(' <tr>');
            html.push(' <th>'+siji_ymd+'品名</th>');
            html.push(' <th>朝</th>');
            html.push(' <th>昼</th>');
            html.push(' <th>夕</th>');
            html.push(' <th style="width:50px">'+htmlEscape(obj["when4"])+'</th>');
            html.push(' <th style="width:50px">'+htmlEscape(obj["when5"])+'</th>');
            html.push(' <th style="width:50px">'+htmlEscape(obj["when6"])+'</th>');
            html.push(' <th></th>');
            html.push(' </tr>');
            for (var idx=1; idx<=field_count; idx++) {
                if (obj["eiyouzai"+idx]=="") obj["eiyouzai"+idx] = "(薬剤指定なし)";
                html.push('<tr>');
                html.push('<td style="padding:3px 0 0 3px">');
                html.push(htmlEscape(getAlias(obj["eiyouzai"+idx])));
                html.push('<input type="hidden" name="keikan__'+idx+'__eiyouzai" value="" />');
                html.push('</td>');
                html.push('<td style="width:113px"><input type="text" title="半角数値のみ、最大４文字、単位：mL" pattern="[0-9]*"');
                html.push(' class="text num3" name="keikan__'+idx+'__asa_ml" maxlength="4" />');
                html.push('<label style="padding-right:10px">');
                html.push('<input type="checkbox" class="checkbox" name="keikan__'+idx+'__asa_zessyoku"');
                html.push(' onclick="dfmCareSyokujiIn.changeKeikanZessyoku(\''+idx+'\', \'asa\')" value="1">絶食</label></td>');
                html.push('<td style="width:113px"><input type="text" title="半角数値のみ、最大４文字、単位：mL" pattern="[0-9]*"');
                html.push(' class="text num3" name="keikan__'+idx+'__hiru_ml" maxlength="4" />');
                html.push('<label style="padding-right:10px">');
                html.push('<input type="checkbox" class="checkbox" name="keikan__'+idx+'__hiru_zessyoku"');
                html.push(' onclick="dfmCareSyokujiIn.changeKeikanZessyoku(\''+idx+'\', \'hiru\')" value="1">絶食</label></td>');
                html.push('<td style="width:113px"><input type="text" title="半角数値のみ、最大４文字、単位：mL" pattern="[0-9]*"');
                html.push(' class="text num3" name="keikan__'+idx+'__yu_ml" maxlength="4" />');
                html.push('<label style="padding-right:10px">');
                html.push('<input type="checkbox" class="checkbox" name="keikan__'+idx+'__yu_zessyoku"');
                html.push(' onclick="dfmCareSyokujiIn.changeKeikanZessyoku(\''+idx+'\', \'yu\')" value="1">絶食</label></td>');
                html.push('<td style="width:50px"><input type="text" title="半角数値のみ、最大４文字、単位：mL"');
                html.push(' pattern="[0-9]*" class="text num3" name="keikan__'+idx+'__ryou4_ml" maxlength="4" /></td>');
                html.push('<td style="width:50px"><input type="text" title="半角数値のみ、最大４文字、単位：mL"');
                html.push(' pattern="[0-9]*" class="text num3" name="keikan__'+idx+'__ryou5_ml" maxlength="4" /></td>');
                html.push('<td style="width:50px"><input type="text" title="半角数値のみ、最大４文字、単位：mL"');
                html.push(' pattern="[0-9]*" class="text num3" name="keikan__'+idx+'__ryou6_ml" maxlength="4" /></td>');
                html.push('<td style="width:90px"><button type="button" name="keikan__'+idx+'__btn_ref"');
                html.push(' onclick="dfmCareSyokujiIn.applyKeikanSiji('+idx+')"'+(isNoSiji?" disabled":"")+'>指示の引用</button></td>');
                html.push('</tr>');
            }
            html.push('</table>');
            ee("efmCareSyokujiIn_keikan_container").innerHTML = html.join("");
            self.keikanSijiObj = obj;
        }});
        return sijiObj;
    },
    changeKeikouZessyoku : function(ahy) {
        var chk = this.form["keikou_"+ahy+"_zessyoku"];
        setDisabled(this.form["keikou_"+ahy+"_fukusyoku"], chk.checked);
        setDisabled(this.form["keikou_"+ahy+"_syusyoku"], chk.checked);
    },
    changeKeikanZessyoku : function(rowIdx, ahy) {
        var chk = this.form["keikan__"+rowIdx+"__"+ahy+"_zessyoku"];
        setDisabled(this.form["keikan__"+rowIdx+"__"+ahy+"_ml"], chk.checked);
    },
    changeSuibunUnit : function () {
        var suibun_unit = efmCareSyokujiIn.suibun_unit.value.split("_");
        ee("efmCareSyokujiIn_suibun_container").style.display = (suibun_unit.length>1 ? "" : "none");
        for (var idx=1; idx<=5; idx++) {
            ee("efmCareSyokujiIn_suibun_unit"+idx).innerHTML = suibun_unit[1];
            var tbox = this.form["suibun_ryou_"+idx];
            var len = (suibun_unit[1]=="割" ? 2 : 4);
            var pattern = (suibun_unit[1]=="割" ? "^(0|1|2|3|4|5|6|7|8|9|10)?$" : "[0-9]*");
            tbox.setAttribute("maxlength", len);
            tbox.setAttribute("pattern", pattern);
            tbox.setAttribute("title", (len==4 ? "半角数値のみ、最大４文字" : "半角数値のみ、最大２文字、0〜10まで"));
            if (tbox.value.length > len) tbox.value = tbox.value.substring(0,len);
        }
    },
    showAssist : function(btn) {
        var nm = btn.getAttribute("name").split("@");
        var tbox = getFormElementByName(document.efmCareSyokujiIn, nm[0]);
        var self = this;
        var callbackFunc = function(opt_name) { tbox.value = opt_name; }
        dfmOptAssist.popup(dfmMstOptions.mstOptions["hosyoku_timing"], callbackFunc, 250, 200);
    },
    applyKeikanSiji : function(fieldIdx) {
        var ryous = {};
        var whens = {};
        var aWhen = ["", "", "", "", "��", "��", "��"];
        for (var tidx=1; tidx<=6; tidx++) ryous["t"+tidx] = htmlEscape(this.keikanSijiObj["ryou"+fieldIdx+"_"+this.weekNumber+"_"+tidx]);
        for (var tidx=4; tidx<=6; tidx++) {
            whens["t"+tidx] = htmlEscape(this.keikanSijiObj["when"+tidx]);
            if (whens["t"+tidx]=="") whens["t"+tidx] = aWhen[tidx];
        }
        var aYmd = ymdToArray(this.keikanSijiObj["siji_ymd"]);
        var msg = [
            "以下の指示値で上書きします。",
            "絶食チェックの状態は変更されません。",
            "-----------------------------",
            "【指示日】"+aYmd["slash_ymd"],
            "朝："+(ryous["t1"]!="" ? ryous["t1"] : "空値"),
            "昼："+(ryous["t2"]!="" ? ryous["t2"] : "空値"),
            "夕："+(ryous["t3"]!="" ? ryous["t3"] : "空値"),
            whens["t4"]+"："+(ryous["t4"]!="" ? ryous["t4"] : "空値"),
            whens["t5"]+"："+(ryous["t5"]!="" ? ryous["t5"] : "空値"),
            whens["t6"]+"："+(ryous["t6"]!="" ? ryous["t6"] : "空値"),
            "",
            "よろしいですか？"
        ];
        if (!confirm(msg.join("\n"))) return;
        this.form["keikan__"+fieldIdx+"__asa_ml"].value =   ryous["t1"];
        this.form["keikan__"+fieldIdx+"__hiru_ml"].value  = ryous["t2"];
        this.form["keikan__"+fieldIdx+"__yu_ml"].value    = ryous["t3"];
        this.form["keikan__"+fieldIdx+"__ryou4_ml"].value = ryous["t4"];
        this.form["keikan__"+fieldIdx+"__ryou5_ml"].value = ryous["t5"];
        this.form["keikan__"+fieldIdx+"__ryou6_ml"].value = ryous["t6"];
    },
    setKeikouAll : function(_ahy, vv) {
        var aryAhy = ["asa", "hiru", "yu"];
        for (var idx=0; idx<=2; idx++) {
            var ahy = aryAhy[idx];
            if (_ahy && _ahy!=ahy) continue;
            this.form["keikou_"+ahy+"_zessyoku"].checked = false;
            setDisabled(this.form["keikou_"+ahy+"_fukusyoku"], false);
            setDisabled(this.form["keikou_"+ahy+"_syusyoku"], false);
            this.form["keikou_"+ahy+"_fukusyoku"].value = vv;
            this.form["keikou_"+ahy+"_syusyoku"].value = vv;
        }

    }
}
</script>













<? //**************************************************************************************************************** ?>
<? // （Ｅ−４）                                                                                                      ?>
<? // ケア入力（補液）                                                                                      ?>
<? //**************************************************************************************************************** ?>
<form id="efmCareHoekiTemplate">
    <table>
        <tbody class="repeat_object_template" namespace="hoeki" delete_type="elimination">
            <tr>
                <td><select basename="hoeki_az" opt_group="siji_az" style="width:60px"></select></td>
                <td style="padding:6px 20px">
                    <table cellspacing="0" cellpadding="0" style="width:100%"><tr>
                    <td style="border:0">
                        <div basename="children_container" class="children_container">
                        </div>
                    </td>
                    <td style="border:0" class="bottom right">
                        <button type="button" basename="add_btn" onclick="dfmCareHoeki.addChildField(this)" class="w60">追加</button>
                    </td>
                    </tr>
                    </table>
                </td>
                <td><button type="button" basename="del_btn" onclick="{TOGGLE_EVENT}; dfmCareHoeki.deleteHoekiField(this)" class="w60">削除</button></td>
            </tr>
        </tbody>
    </table>
</form>
<form id="efmCareHoeki" name="efmCareHoeki">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="sijibi"></th><td></td>
    </tr></table></div>

    <table cellspacing="0" cellpadding="0"><tr>
        <td>
            <table cellspacing="0" cellpadding="0" style="width:auto"><tr>
            <td>
                <table cellspacing="0" cellpadding="0" class="general_list" style="width:auto">
                <tr>
                    <th style="width:70px">指示</th>
                    <th style="width:470px">ケア内容</th>
                    <th></th>
                </tr>
                <tbody class="repeat_object_container" namespace="hoeki"></tbody>
                </table>
            </td>
            <td class="bottom">
                <button type="button" onclick="repeatObjectFieldAdd('efmCareHoeki','hoeki'); dfmCareHoeki.addHoekiField()" class="w60">追加</button>
            </td>
            </tr></table>
        </td>
    </tr></table>
</form>
<script type="text/javascript">
var dfmCareHoeki = {
    sijibi : "",
    childObj : {},
    form : document.efmCareHoeki,
    popup : function(ymd){
        var self = this;
        this.sijibi = ymd;
        var aYmd = ymdToArray(ymd);
        var self = this;
        var saveCareHoeki = function() {
            if (!checkInputError("efmCareHoeki")) return false;
            var ret = false;
            var sData = serializeUnicodeEntity("efmCareHoeki");
            $.ajax({ url:baseUrl+"&ajax_action=save_care_ymd&section=hoeki&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd, data:sData, success:function(msg) {
                if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
                $('#efmCareHoeki').dialog("close");
                dfmMainMenu.changeView(view_mode, ptif_id, ext_view_mode);
                ret = true;
            }});
            return ret;
        }
        $.ajax({ url:baseUrl+"&ajax_action=get_care_ymd&section=hoeki&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            var aYmd = ymdToArray(self.sijibi);
            if (str(obj["hoeki__field_count"])=="") {
                obj["hoeki__field_count"] = 1;
                obj["hoeki__1__field_count"] = 1;
            }
            obj["sijibi"] = "指示日："+aYmd.slash_mdw;
            bindDataToForm("efmCareHoeki", obj);
            $('#efmCareHoeki').dialog({
                title:'ケア入力（補液）', width:700, height:550, extButtons:["up","down","save"], dlgFooter:obj,
                saveExec:saveCareHoeki
            });
            var hoekiFieldCount = obj["hoeki__field_count"];
            self.childObj = [];
            for (var fidx=1; fidx<=hoekiFieldCount; fidx++) {
                self.childObj[fidx-1] = [];
                var childrenCount = Math.max(0, int(obj["hoeki__"+fidx+"__field_count"]));
                for (var cidx=1; cidx<=childrenCount; cidx++) {
                    var pname = "hoeki__"+fidx+"__child"+cidx+"__";
                    self.childObj[fidx-1][cidx-1] = {
                        hoeki_hm:str(obj[pname+"hoeki_hm"]),
                        hoeki_koushin:str(obj[pname+"hoeki_koushin"]),
                        hoeki_zan:str(obj[pname+"hoeki_zan"]),
                        hoeki_ryusoku:str(obj[pname+"hoeki_ryusoku"])
                    };
                }
            }
            for (var idx=1; idx<=hoekiFieldCount; idx++) {
                self.redrawChildren(idx);
            }
        }});
    },
    copyValuesElemToObj : function(fidx) {
        var childrenCount = this.childObj[fidx-1].length;
        var newObj = [];
        for (var cidx=1; cidx<=childrenCount; cidx++) {
            var pname = "hoeki__"+fidx+"__child"+cidx+"__";
            newObj[cidx-1] = {
                hoeki_hm:str(this.form[pname+"hoeki_hm"].value),
                hoeki_koushin:(this.form[pname+"hoeki_koushin"].checked ? "1":""),
                hoeki_zan:str(this.form[pname+"hoeki_zan"].value),
                hoeki_ryusoku:str(this.form[pname+"hoeki_ryusoku"].value)
            };
        }
        this.childObj[fidx-1] = newObj;
    },
    addHoekiField : function() {
        var fidx = ee("efmCareHoeki@hoeki__field_count").value;
        this.childObj[fidx-1] = [{}];
        this.redrawChildren(fidx);
    },
    deleteHoekiField : function(btn) {
        var hoekiFieldCount = obj["hoeki__field_count"];
        var nms = btn.getAttribute("name").split("__");
        var fidx = nms[1];
        var newObj = [];
        for (var idx=1; idx<fidx; idx++) {
            newObj[idx-1] = this.childObj[idx-1];
        }
        fidx++;
        for (var idx=fidx; idx<=hoekiFieldCount; idx++) {
            newObj[idx-2] = this.childObj[idx-1];
        }
        this.childObj = newObj;
    },
    addChildField : function(btn) {
        var nms = btn.getAttribute("name").split("__");
        var fidx = nms[1];
        this.copyValuesElemToObj(fidx);
        var childrenCount = this.childObj[fidx-1].length;
        childrenCount++;
        this.childObj[fidx-1][childrenCount-1] = {};
        this.redrawChildren(fidx);
    },
    deleteChildField : function(btn) {
        var nms = btn.getAttribute("name").split("__");
        var fidx = nms[1];
        var cidx = nms[2].replace("child","");
        this.copyValuesElemToObj(fidx);

        var childrenCount = this.childObj[fidx-1].length;
        var newObj = [];
        for (var idx=1; idx<cidx; idx++) {
            newObj[idx-1] = this.childObj[fidx-1][idx-1];
        }
        cidx++;
        for (var idx=cidx; idx<=childrenCount; idx++) {
            newObj[idx-2] = this.childObj[fidx-1][idx-1];
        }
        this.childObj[fidx-1] = newObj;
        this.redrawChildren(fidx);
    },
    redrawChildren : function(fidx) {
        var cAry = this.childObj[fidx-1];
        if (!cAry) cAry = [];

        var childrenCount = cAry.length;
        var $div = "";
        $("#efmCareHoeki div.children_container").each(function() {
            if (this.getAttribute("name")=="hoeki__"+fidx+"__children_container") div = this;
        });
        var html = [];
        if (childrenCount) {
            html.push('<input type="hidden" bansename="field_count" name="hoeki__'+fidx+'__field_count"');
            html.push(' id="efmCareHoeki@hoeki__'+fidx+'__field_count" value="'+childrenCount+'" />');
            html.push('<table cellspacing="0" cellpadding="0"><tr>');
            html.push('<th>時刻</th><th>更新時</th><th>輸液残量</th><th>流速</th><th></th>');
            html.push('</tr>');

            for (var cidx=1; cidx<=childrenCount; cidx++) {
                var cname = "child"+cidx+"__";
                var pname = "hoeki__"+fidx+"__"+cname;
                var cObj = cAry[cidx-1];
                if (!cObj) cObj = {};
                var hm = str(cObj["hoeki_hm"]);
                var colon_hm = (hm.length==4 ? hm.substring(0,2)+":"+hm.substring(2) : ":");
                var chk = (cObj["hoeki_koushin"]?' checked':'');

                html.push('<tr>');
                html.push('<td><button type="button" class="hm_button" name="'+pname+'hoeki_hm@button"');
                html.push(' basename="'+cname+'hoeki_hm@button" onclick="dfmCareHoeki.popupTimeAssist(this)">'+colon_hm+'</button></td>');
                html.push('<td><input type="hidden" basename="'+cname+'hoeki_hm" name="'+pname+'hoeki_hm" value="'+str(cObj["hoeki_hm"])+'" />');
                html.push(' <label style="padding-right:16px"><input type="checkbox" name="'+pname+'hoeki_koushin"');
                html.push('    class="checkbox" basename="'+cname+'hoeki_koushin"');
                html.push(' onclick="dfmCareHoeki.changeHoekiKoushin(this)" value="1"'+chk+' />更新</label>');
                html.push('</td>');
                html.push('<td style="padding-right:3px">');
                html.push(' <input type="text" title="半角数値のみ、最大４文字" pattern="[0-9]*" class="text num3'+(chk?" text_readonly":"")+'"');
                html.push(' basename="'+cname+'hoeki_zan" maxlength="4" name="'+pname+'hoeki_zan" value="'+str(cObj["hoeki_zan"])+'"'+(chk?" disabled":"")+'/>');
                html.push(' mL');
                html.push('</td>');
                html.push('<td style="padding-right:3px">');
                html.push('<input type="text" title="半角数値のみ、最大４文字" pattern="[0-9]*" maxlength="4" class="text num3'+(chk?" text_readonly":"")+'"');
                html.push(' basename="'+cname+'hoeki_ryusoku" name="'+pname+'hoeki_ryusoku" value="'+str(cObj["hoeki_ryusoku"])+'"'+(chk?" disabled":"")+'>');
                html.push('mL/h');
                html.push('</td>');
                html.push('<td>');
                html.push('<button type="button" onclick="dfmCareHoeki.deleteChildField(this)"');
                html.push(' class="w60" basename="'+cname+'del_btn" name="'+pname+'del_btn">削除</button>');
                html.push('</td>');
                html.push('</tr>');
            }

            html.push('</table>');
        }
        div.innerHTML = html.join("");

    },
    changeHoekiKoushin : function(cbox) {
        var zanName = cbox.name.replace("hoeki_koushin", "hoeki_zan");
        var ryuName = cbox.name.replace("hoeki_koushin", "hoeki_ryusoku");
        setDisabled(this.form[zanName], cbox.checked);
        setDisabled(this.form[ryuName], cbox.checked);
    },
    popupTimeAssist : function(btn) {
        var self = this;
        var nm = btn.getAttribute("name").split("@");
        var callbackFunc = function(hm, colon_hm) {
            self.form[nm[0]].value = hm;
            btn.innerHTML = colon_hm;
        }
        dfmHMAssist.popup(this.form[nm[0]].value, callbackFunc);
    }
}
</script>



















<? //**************************************************************************************************************** ?>
<? // （Ｅ−５）                                                                                                      ?>
<? // ケア入力（インスリン）                                                                                ?>
<? //**************************************************************************************************************** ?>
<form id="efmCareInsulinTemplate">
    <table>
        <tbody class="repeat_object_template" namespace="bsscale" delete_type="elimination">
        <tr>
            <td style="vertical-align:middle"><button type="button" class="hm_button" basename="bs_hm@button" onclick="dfmCareInsulin.popupTimeAssist(this)">:</button>
                <input type="hidden" basename="bs_hm" />
            </td>
            <td style="vertical-align:middle"><input type="text" title="半角数値のみ、最大３文字" pattern="[0-9]*" class="text num4" basename="bs_upper" maxlength="3" /></td>
            <td><nobr>
                <input type="text" class="text" basename="bs_name1" style="width:250px" title="最大１００文字" maxlength="100" /><input type="text"
                    title="半角英数＋ピリオドのみ、最大１０文字" onfocus="ipadHackFocusIn(this)" pattern="[0-9a-zA-Z\.]*"
                    class="text num4" basename="bs_ryou1" maxlength="10" /><br/>
                <input type="text" class="text" basename="bs_name2" style="width:250px" title="最大１００文字" maxlength="100" /><input type="text"
                    title="半角英数＋ピリオドのみ、最大１０文字" onfocus="ipadHackFocusIn(this)" pattern="[0-9a-zA-Z\.]*"
                    class="text num4" basename="bs_ryou2" maxlength="10" />
            </nobr></td>
            <td style="vertical-align:middle">
                <button type="button" basename="btn_ref" onclick="dfmCareInsulin.loadBSSiji(this.name)">指示の引用</button><button
                type="button" onclick="{TOGGLE_EVENT}" class="w60">削除</button>
            </td>
        </tr>
        </tbody>
    </table>
</form>
<form id="efmCareInsulin" name="efmCareInsulin">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="sijibi"></th><td></td>
    </tr></table></div>

    <table cellspacing="0" cellpadding="0"><tr>
        <td style="width:100px"><label style="display:block; height:25px; line-height:25px"><input type="checkbox" class="checkbox" value="1" name="insulin_teiki_ari" onclick="dfmCareInsulin.changeInsulin('teiki')" />定期</label></td>
        <td style="padding-left:20px">
            <div id="efmCareInsulin_insulin_teiki_container" style="display:none;">
                <div style="padding-top:5px">
                    <label style="padding-right:20px"><input type="checkbox" class="checkbox" name="insulin_teiki_asa" value="1">朝</label>
                    <label style="padding-right:20px"><input type="checkbox" class="checkbox" name="insulin_teiki_hiru" value="1">昼</label>
                    <label style="padding-right:20px"><input type="checkbox" class="checkbox" name="insulin_teiki_yu" value="1">夕</label>
                    <input type="hidden" name="insulin_teiki_when4" />
                    <input type="hidden" name="insulin_teiki_when5" />
                    <input type="hidden" name="insulin_teiki_when6" />
                    <input type="hidden" name="insulin_teiki_when7" />
                    <label id="insulin_teiki_check4_label" style="padding-right:20px"><input type="checkbox" class="checkbox" name="insulin_teiki_check4"
                        value="1"><span id="efmCareInsulin_check4"></span></label>
                    <label id="insulin_teiki_check5_label" style="padding-right:20px"><input type="checkbox" class="checkbox" name="insulin_teiki_check5"
                        value="1"><span id="efmCareInsulin_check5"></span></label>
                    <label id="insulin_teiki_check6_label" style="padding-right:20px"><input type="checkbox" class="checkbox" name="insulin_teiki_check6"
                        value="1"><span id="efmCareInsulin_check6"></span></label>
                    <label id="insulin_teiki_check7_label" style="padding-right:20px"><input type="checkbox" class="checkbox" name="insulin_teiki_check7"
                        value="1"><span id="efmCareInsulin_check7"></span></label>
                </div>
            </div>
        </td>
        </tr>
        <tr><td colspan="2"><hr></td></tr>
        <tr>
        <td><label style="display:block; height:25px; line-height:25px"><input type="checkbox" class="checkbox" value="1" name="insulin_bs_ari" onclick="dfmCareInsulin.changeInsulin('bs')" />BS測定値</label></td>
        <td style="padding-left:20px; padding-top:6px">
            <div id="efmCareInsulin_insulin_bs_container" style="display:none">
                <table cellspacing="0" cellpadding="0" style="width:auto"><tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" class="general_list" style="width:auto">
                            <tr><th>時刻</th><th>測定値</th><th>薬剤・量</th><th></th></tr>
                            <tbody class="repeat_object_container" namespace="bsscale"></tbody>
                        </table>
                    </td>
                    <td class="bottom"><button type="button" onclick="repeatObjectFieldAdd('efmCareInsulin','bsscale')" class="w60">追加</button></td>
                </tr></table>
            </div>
        </td>
        </tr>
        <tr><td colspan="2"><hr></td></tr>
        <tr>
        <td><label style="display:block; height:25px; line-height:25px"><input type="checkbox" class="checkbox" value="1" name="insulin_sr_ari" onclick="dfmCareInsulin.changeInsulin('sr')" />食事量</label></td>
        <td style="padding-left:20px;">
            <div id="efmCareInsulin_insulin_sr_container" style="display:none; padding-top:5px"></div>
        </td>
        </tr>
    </table>
</form>
<script type="text/javascript">
var dfmCareInsulin = {
    sijibi : "",
    weekNumber : "",
    keikanSijiObj : 0,
    form : document.efmCareInsulin,
    popup : function(ymd){
        var self = this;
        this.sijibi = ymd;
        var aYmd = ymdToArray(ymd);
        var self = this;
        var saveCareInsulin = function() {
            if (!checkInputError("efmCareInsulin")) return false;
            var ret = false;
            var sData = serializeUnicodeEntity("efmCareInsulin");
            $.ajax({ url:baseUrl+"&ajax_action=save_care_ymd&section=insulin&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd, data:sData, success:function(msg) {
                if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
                $('#efmCareInsulin').dialog("close");
                dfmMainMenu.changeView(view_mode, ptif_id, ext_view_mode);
                ret = true;
            }});
            return ret;
        }
        var sijiObj = this.loadSiji();
        $.ajax({ url:baseUrl+"&ajax_action=get_care_ymd&section=insulin&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            var aYmd = ymdToArray(self.sijibi);
            self.weekNumber = aYmd["weekNumber"];
            obj["sijibi"] = "指示日："+aYmd.slash_mdw;
            for (var idx=4; idx<=7; idx++) {
                var caption = str(sijiObj["when"+idx]);
                var checkValue = obj["insulin_teiki_check"+idx];
                ee("insulin_teiki_check"+idx+"_label").style.display = (caption=="" && !checkValue ? "none" : "");
                ee("efmCareInsulin_check"+idx).innerHTML = caption;
                obj["insulin_teiki_when"+idx] = caption;
            }
            bindDataToForm("efmCareInsulin", obj);
            self.changeInsulin('teiki');
            self.changeInsulin('sr');
            self.changeInsulin('bs');
            $('#efmCareInsulin').dialog({
                title:'ケア入力（インスリン）', width:800, height:550, extButtons:["up","down","save"], dlgFooter:obj,
                saveExec:saveCareInsulin
            });
            var sny = ["sinya", "nikkin", "junya"];
            var hoekiFieldCount = obj["hoeki__field_count"];
            for (var idx=1; idx<=hoekiFieldCount; idx++) {
                for (var snyIdx in sny) self.changeHoekiNasi(idx, sny[snyIdx]);
            }
        }});
    },
    loadSiji : function() {
        var sijiObj = {};
        var self = this;
        var url = baseUrl+"&ajax_action=get_siji_ymd&section=insulin_fix&ptif_id="+uEnc(ptif_id)+"&ymd="+this.sijibi+"&is_get_last_siji=1";
        $.ajax({ url:url, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            sijiObj = obj;
        }});
        var url = baseUrl+"&ajax_action=get_siji_ymd&section=insulin_sr&ptif_id="+uEnc(ptif_id)+"&ymd="+this.sijibi+"&is_get_last_siji=1";
        $.ajax({ url:url, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            var html = [];
            var aYmd = ymdToArray(obj.siji_ymd);
            var field_count = int(obj["insulin_sr__field_count"]);
            if (field_count > 0) {
                html.push('<div style="padding-bottom:5px; width:600px">指示日：<span style="color:red; padding-right:20px">'+aYmd.slash_mdw+'</span>薬剤名：'+htmlEscape(obj.med_name)+'</div>');
            } else {
                html.push('<div>（指示はありません）</div>');
            }
            html.push('<table cellspacing="0" cellpadding="0" class="general_list" style="width:auto">');
            html.push('<tr><th style="width:150px">摂取量</th>');
            html.push('<th><span style="padding:0 20px">朝</span></th>');
            html.push('<th><span style="padding:0 20px">昼</span></th>');
            html.push('<th><span style="padding:0 20px">夕</span></th></tr>');
            if (field_count>0) {
                for (var idx=1; idx<=field_count; idx++) {
                    var prefix = "insulin_sr__"+idx+"__";
                    html.push('<tr>');
                    html.push('<td>'+htmlEscape(obj[prefix+"ryou"])+'</td>');
                    if (trim(obj[prefix+"asa"])!="") {
                    html.push('<td><label style="padding-right:20px"><input type="checkbox" class="checkbox" onclick="dfmCareInsulin.haita(this,'+field_count+','+idx+')" name="insulin_sr_asa" id="insulin_sr_asa'+idx+'" value="'+htmlEscape(obj[prefix+"asa"])+'">'+htmlEscape(obj[prefix+"asa"])+'</label></td>');
                    } else html.push("<td>&nbsp;</td>");
                    if (trim(obj[prefix+"hiru"])!="") {
                    html.push('<td><label style="padding-right:20px"><input type="checkbox" class="checkbox" onclick="dfmCareInsulin.haita(this,'+field_count+','+idx+')" name="insulin_sr_hiru" id="insulin_sr_hiru'+idx+'" value="'+htmlEscape(obj[prefix+"hiru"])+'">'+htmlEscape(obj[prefix+"hiru"])+'</label></td>');
                    } else html.push("<td>&nbsp;</td>");
                    if (trim(obj[prefix+"yu"])!="") {
                    html.push('<td><label style="padding-right:20px"><input type="checkbox" class="checkbox" onclick="dfmCareInsulin.haita(this,'+field_count+','+idx+')" name="insulin_sr_yu" id="insulin_sr_yu'+idx+'" value="'+htmlEscape(obj[prefix+"yu"])+'">'+htmlEscape(obj[prefix+"yu"])+'</label></td>');
                    } else html.push("<td>&nbsp;</td>");
                    html.push('</tr>');
                }
            } else {
                html.push('<tr><td>（指示はありません）</td><td></td><td></td><td></td></tr>');
            }
            html.push('</table>');
            ee("efmCareInsulin_insulin_sr_container").innerHTML = html.join("");
        }});
        return sijiObj;
    },
    haita : function(cbox, field_count, idx) {
        if (!cbox.checked) return;
        var nm = cbox.name;
        for (var ii=1; ii<=field_count; ii++) {
            if (ii!=idx && ee(nm+ii)) ee(nm+ii).checked = false;
        }
    },
    loadBSSiji : function(btn_name) {
        var ary = btn_name.split("__");
        var prefix = ary[0]+"__"+ary[1]+"__";
        var self = this;
        var bsCallbackFunc = function(bs_upper, bs_name1, bs_ryou1, bs_name2, bs_ryou2) {
            var elem = getFormElementByName(self.form, prefix+"bs_name1");
            elem.value = bs_name1;
            var elem = getFormElementByName(self.form, prefix+"bs_ryou1");
            elem.value = bs_ryou1;
            var elem = getFormElementByName(self.form, prefix+"bs_name2");
            elem.value = bs_name2;
            var elem = getFormElementByName(self.form, prefix+"bs_ryou2");
            elem.value = bs_ryou2;
        }
        dfmCareBSScale.popup(this.sijibi, bsCallbackFunc);
    },
    changeInsulin : function(target) {
        var checked = this.form["insulin_"+target+"_ari"].checked;
        ee("efmCareInsulin_insulin_"+target+"_container").style.display = (checked ? "" : "none");
    },
    popupTimeAssist : function(btn) {
        var self = this;
        var nm = btn.getAttribute("name").split("@");
        var hidden = getFormElementByName(this.form, nm[0]);
        var callbackFunc = function(hm, colon_hm) {
            hidden.value = hm;
            btn.innerHTML = colon_hm;
        }
        dfmHMAssist.popup(hidden.value, callbackFunc);
    }
}
</script>









<? //**************************************************************************************************************** ?>
<? // （Ｅ−６）                                                                                                      ?>
<? // ケア入力（OUT）                                                                                       ?>
<? //**************************************************************************************************************** ?>
<form id="efmCareOutTemplate">
    <div class="repeat_object_template" namespace="ben"><div>
        <span>
        <input type="text" class="text" basename="ben_benryou" style="width:50px" maxlength="20" title="最大２０文字" /><button
            type="button" onclick="dfmCareOut.showAssistBenryou(this)" class="w60">候補</button></span>
        <span>
        <input type="text" class="text" basename="ben_type" style="width:110px" maxlength="20" title="最大２０文字" /><button
            type="button" onclick="dfmCareOut.showAssistBentype(this)" class="w60">候補</button></span>
        <input type="text" title="半角数値のみ、最大４文字" pattern="[0-9]*" class="text num3" basename="ryou" maxlength="4" /><span>g</span>
        <button type="button" onclick="{TOGGLE_EVENT}" class="w60">削除</button>
    </div></div>

    <div class="repeat_object_template" namespace="nyou"><div>
        <button type="button" class="hm_button" basename="nyou_hm@button" onclick="dfmCareOut.popupTimeAssist(this)">:</button>
        <input type="hidden" basename="nyou_hm" />
        <span>量</span><input type="text" title="半角数値のみ、最大４文字" pattern="[0-9]*" class="text num4" basename="ryou" maxlength="4" /><span>mL</span>
        <select basename="nyou_more" opt_group="nyou_more"></select>
        <button type="button" onclick="{TOGGLE_EVENT}" class="w60">削除</button>
    </div></div>

    <div class="repeat_object_template" namespace="haieki_jikantai"><div>
        <button type="button" class="hm_button" basename="haieki_hm@button" onclick="dfmCareOut.popupTimeAssist(this)">:</button>
        <input type="hidden" basename="haieki_hm" />
        <span>量</span><input type="text" title="半角数値のみ、最大４文字" pattern="[0-9]*" class="text num4" basename="ryou" maxlength="4" /><span>mL</span>
        <select basename="haieki_more" opt_group="haieki_more"></select>
        <button type="button" onclick="{TOGGLE_EVENT}" class="w60">削除</button>
    </div></div>
    <div class="repeat_object_template" namespace="haieki2_jikantai"><div>
        <button type="button" class="hm_button" basename="haieki_hm@button" onclick="dfmCareOut.popupTimeAssist(this)">:</button>
        <input type="hidden" basename="haieki_hm" />
        <span>量</span><input type="text" title="半角数値のみ、最大４文字" pattern="[0-9]*" class="text num4" basename="ryou" maxlength="4" /><span>mL</span>
        <select basename="haieki_more" opt_group="haieki_more"></select>
        <button type="button" onclick="{TOGGLE_EVENT}" class="w60">削除</button>
    </div></div>
    <div class="repeat_object_template" namespace="haieki3_jikantai"><div>
        <button type="button" class="hm_button" basename="haieki_hm@button" onclick="dfmCareOut.popupTimeAssist(this)">:</button>
        <input type="hidden" basename="haieki_hm" />
        <span>量</span><input type="text" title="半角数値のみ、最大４文字" pattern="[0-9]*" class="text num4" basename="ryou" maxlength="4" /><span>mL</span>
        <select basename="haieki_more" opt_group="haieki_more"></select>
        <button type="button" onclick="{TOGGLE_EVENT}" class="w60">削除</button>
    </div></div>
</form>
<form id="efmCareOut" name="efmCareOut">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="sijibi"></th><td></td>
    </tr></table></div>

    <table cellspacing="0" cellpadding="0"><tr>
        <th width="50px">便</th>
        <th width="110px" class="right" style="padding-top:3px">
            <label style="padding-right:20px"><input type="checkbox" class="checkbox" name="ben_nasi" onclick="dfmCareOut.changeBenNasi()" value="1">無し</label>
        </th>
        <td>
            <div id="efmCareOut_ben_container">
            <table cellspacing="0" cellpadding="0"><tr>
                <td class="repeat_object_container" namespace="ben"></td>
            </tr></table>
            </div>
        </td>
        <td class="bottom right" width="60"><button type="button" id="efmCareOut_btn_ben_add" onclick="repeatObjectFieldAdd('efmCareOut','ben')" class="w60">追加</button></td>
    </tr></table>

    <hr>
    <table cellspacing="0" cellpadding="0">
        <tr>
            <th width="50px">尿量</th>
            <th width="110px" class="right">＜時間尿＞</th>
            <td style="border-bottom:1px dashed #888888; padding-bottom:3px">
                <table cellspacing="0" cellpadding="0"><tr>
                    <td class="repeat_object_container" namespace="nyou"></td>
                </tr></table>
            </td>
            <td class="bottom right" width="60" style="border-bottom:1px dashed #888888; padding-bottom:3px">
                <button type="button" onclick="repeatObjectFieldAdd('efmCareOut','nyou')" class="w60">追加</button>
            </td>
        </tr>
        <tr navititle="尿量 一日量" navi="一日量は、時間量とは関連しません。">
            <th></th>
            <th class="right" style="padding-top:5px">＜一日量＞</th>
            <td colspan="2" style="padding-top:5px">
                <input type="text" title="半角数値のみ" pattern="[0-9]*" class="text num4" name="day_nyou"><span>mL</span>
                <span>コメント：</span><input type="text" class="text" title="最大３００文字" name="day_nyou_comment" maxlength="300" />
            </td>
        </tr>
    </table>

    <hr>
    <table cellspacing="0" cellpadding="0">
        <tr>
            <th width="50px">排液</th>
            <td>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <th width="110px" class="right" style="padding-bottom:5px">＜種類＞</th>
                        <td style="padding-bottom:5px">
                            <select name="haieki_syurui" opt_group="haieki"></select>
                            <input type="text" class="text" title="最大３００文字" name="haieki_syurui_comment" maxlength="300" />
                        </td>
                    </tr>
                    <tr>
                        <th class="right" style="padding-top:3px">＜時間廃液量＞</th>
                        <td style="padding-top:5px; border-bottom:1px dashed #888888; padding-bottom:3px; border-top:1px dashed #888888">
                        <table cellspacing="0" cellpadding="0"><tr>
                            <td class="repeat_object_container" namespace="haieki_jikantai"></td>
                        </tr></table>
                        </td>
                        <td class="bottom right" width="60" style="border-bottom:1px dashed #888888; border-top:1px dashed #888888; padding-bottom:3px">
                            <button type="button" onclick="repeatObjectFieldAdd('efmCareOut','haieki_jikantai')" class="w60">追加</button>
                        </td>
                    </tr>
                    <tr navititle="排液 一日量" navi="一日量は、時間量とは関連しません。">
                        <th class="right" style="padding-top:5px">＜一日量＞</th>
                        <td colspan="2" style="padding-top:5px">
                            <input type="text" title="半角数値のみ、最大４文字" pattern="[0-9]*" class="text num4" name="haieki_day_ryou" maxlength="4" /><span>mL</span>
                            <span>コメント：</span><input type="text" class="text" title="最大３００文字" name="haieki_day_comment" maxlength="300" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <hr>
    <table cellspacing="0" cellpadding="0">
        <tr>
            <th width="50px"><label>排液２<input type="checkbox" value="1" name="haieki2_exist" onclick="ee('dfmCareOut_haieki2_container').style.display=(this.checked?'':'none');"></label></th>
            <td>
                <table cellspacing="0" cellpadding="0" style="display:none" id="dfmCareOut_haieki2_container">
                    <tr>
                        <th width="110px" class="right" style="padding-bottom:5px">＜種類＞</th>
                        <td style="padding-bottom:5px">
                            <select name="haieki2_syurui" opt_group="haieki"></select>
                            <input type="text" class="text" name="haieki2_syurui_comment" title="最大３００文字" maxlength="300" />
                        </td>
                    </tr>
                    <tr>
                        <th class="right" style="padding-top:3px">＜時間廃液量＞</th>
                        <td style="padding-top:5px; border-bottom:1px dashed #888888; padding-bottom:3px; border-top:1px dashed #888888">
                        <table cellspacing="0" cellpadding="0"><tr>
                            <td class="repeat_object_container" namespace="haieki2_jikantai"></td>
                        </tr></table>
                        </td>
                        <td class="bottom right" width="60" style="border-bottom:1px dashed #888888; border-top:1px dashed #888888; padding-bottom:3px">
                            <button type="button" onclick="repeatObjectFieldAdd('efmCareOut','haieki2_jikantai')" class="w60">追加</button>
                        </td>
                    </tr>
                    <tr>
                        <th class="right" style="padding-top:5px">＜一日量＞</th>
                        <td colspan="2" style="padding-top:5px">
                            <input type="text" title="半角数値のみ、最大４文字" pattern="[0-9]*" class="text num4" name="haieki2_day_ryou" maxlength="4" /><span>mL</span>
                            <span>コメント：</span><input type="text" class="text" title="最大３００文字" name="haieki2_day_comment" maxlength="300" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <hr>
    <table cellspacing="0" cellpadding="0">
        <tr>
            <th width="50px"><label>排液３<input type="checkbox" value="1" name="haieki3_exist" onclick="ee('dfmCareOut_haieki3_container').style.display=(this.checked?'':'none');"></label></th>
            <td>
                <table cellspacing="0" cellpadding="0" style="display:none" id="dfmCareOut_haieki3_container">
                    <tr>
                        <th width="110px" class="right" style="padding-bottom:5px">＜種類＞</th>
                        <td style="padding-bottom:5px">
                            <select name="haieki3_syurui" opt_group="haieki"></select>
                            <input type="text" class="text" name="haieki3_syurui_comment" title="最大３００文字" maxlength="300" />
                        </td>
                    </tr>
                    <tr>
                        <th class="right" style="padding-top:3px">＜時間廃液量＞</th>
                        <td style="padding-top:5px; border-bottom:1px dashed #888888; padding-bottom:3px; border-top:1px dashed #888888">
                        <table cellspacing="0" cellpadding="0"><tr>
                            <td class="repeat_object_container" namespace="haieki3_jikantai"></td>
                        </tr></table>
                        </td>
                        <td class="bottom right" width="60" style="border-bottom:1px dashed #888888; border-top:1px dashed #888888; padding-bottom:3px">
                            <button type="button" onclick="repeatObjectFieldAdd('efmCareOut','haieki3_jikantai')" class="w60">追加</button>
                        </td>
                    </tr>
                    <tr>
                        <th class="right" style="padding-top:5px">＜一日量＞</th>
                        <td colspan="2" style="padding-top:5px">
                            <input type="text" title="半角数値のみ、最大４文字" pattern="[0-9]*" class="text num4" name="haieki3_day_ryou" maxlength="4" /><span>mL</span>
                            <span>コメント：</span><input type="text" class="text" title="最大３００文字" name="haieki3_day_comment" maxlength="300" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>
<script type="text/javascript">
var dfmCareOut = {
    popup : function(ymd){
        var self = this;
        var saveCareOut = function() {
            if (!checkInputError("efmCareOut")) return false;
            if (document.efmCareOut.ben_nasi.checked) repeatObjectFieldDispose('efmCareOut','ben');
            var ret = false;
            var sData = serializeUnicodeEntity("efmCareOut");
            var url = baseUrl+"&ajax_action=save_care_ymd&section=out&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd;
            $.ajax({ url:url, data:sData, success:function(msg) {
                var obj = getCustomTrueAjaxResult(msg); if(obj==AJAX_ERROR) return;
                $('#efmCareOut').dialog("close");
                dfmMainMenu.changeView(view_mode, ptif_id, ext_view_mode);
                ret = true;
            }});
            return ret;
        }

        $.ajax({ url:baseUrl+"&ajax_action=get_care_ymd&section=out&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            var aYmd = ymdToArray(ymd);
            obj["sijibi"] = "指示日："+aYmd.slash_mdw;
            bindDataToForm("efmCareOut", obj);
            $('#efmCareOut').dialog({
                title:'ケア入力（OUT）', width:720, height:550, extButtons:["up","down","save"], dlgFooter:obj, saveExec:saveCareOut
            });
            ee('dfmCareOut_haieki2_container').style.display = (document.efmCareOut.haieki2_exist.checked ? '' : 'none');
            ee('dfmCareOut_haieki3_container').style.display = (document.efmCareOut.haieki3_exist.checked ? '' : 'none');
            self.changeBenNasi();
        }});
    },
    showAssistBenryou : function(btn) {
        var self = this;
        var callbackFunc = function(opt_name) { $(btn).parent().find("input").val(opt_name); }
        dfmOptAssist.popup(dfmMstOptions.mstOptions["ben_benryou"], callbackFunc, 300, 150);
    },
    showAssistBentype : function(btn) {
        var self = this;
        var callbackFunc = function(opt_name) { $(btn).parent().find("input").val(opt_name); }
        dfmOptAssist.popup(dfmMstOptions.mstOptions["ben_type"], callbackFunc, 300, 250);
    },
    changeBenNasi : function() {
        var chk = document.efmCareOut.ben_nasi.checked;
        ee("efmCareOut_btn_ben_add").style.display = (chk ? "none": "");
        ee("efmCareOut_ben_container").style.display = (chk ? "none": "");
    },
    popupTimeAssist : function(btn) {
        var nm = btn.getAttribute("name").split("@");
        var hidden = getFormElementByName(document.efmCareOut, nm[0]);
        var callbackFunc = function(hm, colon_hm) {
            hidden.value = hm;
            btn.innerHTML = colon_hm;
        }
        dfmHMAssist.popup(hidden.value, callbackFunc);
    }
}
</script>









<? //**************************************************************************************************************** ?>
<? // （Ｅ−７）                                                                                                      ?>
<? // ケア入力（検査等）                                                                                              ?>
<? //**************************************************************************************************************** ?>
<form id="efmCareKensanadoTemplate">
    <div class="repeat_object_template" namespace="gezai"><div>
        <button type="button" class="hm_button" basename="hm@button" onclick="dfmCareKensanado.popupTimeAssist(this)">:</button>
        <input type="hidden" basename="hm" />
        <select basename="med_name"  opt_group="gezai"></select>
        <button type="button" onclick="{TOGGLE_EVENT}" class="w60">削除</button>
    </div></div>
    <div class="repeat_object_template" namespace="zayaku"><div>
        <button type="button" class="hm_button" basename="hm@button" onclick="dfmCareKensanado.popupTimeAssist(this)">:</button>
        <input type="hidden" basename="hm" />
        <select basename="med_name"  opt_group="zayaku"></select>
        <button type="button" onclick="{TOGGLE_EVENT}" class="w60">削除</button>
    </div></div>
</form>
<form id="efmCareKensanado" name="efmCareKensanado">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="sijibi"></th><td></td>
    </tr></table></div>

    <table cellspacing="0" cellpadding="0">
        <tr>
            <th width="80">下剤</th>
            <td class="repeat_object_container" namespace="gezai"></td>
            <td class="bottom right" width="60"><button type="button" onclick="repeatObjectFieldAdd('efmCareKensanado','gezai')" class="w60">追加</button></td>
        </tr>
    </table>

    <hr>
    <table cellspacing="0" cellpadding="0"><tr>
        <th width="80">座薬</th>
        <td class="repeat_object_container" namespace="zayaku"></td>
        <td class="bottom right" width="60"><button type="button" onclick="repeatObjectFieldAdd('efmCareKensanado','zayaku')" class="w60">追加</button></td>
    </tr></table>

    <hr>
    <table cellspacing="0" cellpadding="0"><tr>
        <th width="80">発赤</th>
        <td><input type="text" class="text" name="hosseki" style="width:300px" title="最大３００文字" maxlength="300" /></td>
    </tr></table>

    <hr>
    <table cellspacing="0" cellpadding="0"><tr>
        <th width="80">検査</th>
        <td>
            <input type="hidden" name="kensa_names" id="efmCareKensanado__kensa_names" />
            <div id="efmCareKensanado__kensa"></div>
            <div style="padding:5px 0 8px 0">
            <span>その他</span>
            <input type="text" class="text" name="kensa_other_comment" style="width:300px" title="最大３００文字" maxlength="300" />
            </div>
        </td>
    </tr></table>

</form>
<script type="text/javascript">
var dfmCareKensanado = {
    popup : function(ymd){
        var saveCareKensanado = function() {
            if (!checkInputError("efmCareKensanado")) return false;
            var ret = false;
            var sData = serializeUnicodeEntity("efmCareKensanado");
            $.ajax({ url:baseUrl+"&ajax_action=save_care_ymd&section=kensanado&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd, data:sData, success:function(msg) {
                var obj = getCustomTrueAjaxResult(msg); if(obj==AJAX_ERROR) return;
                dfmMainMenu.changeView(view_mode, ptif_id, ext_view_mode);
                ret = true;
            }});
            return ret;
        }
        $.ajax({ url:baseUrl+"&ajax_action=get_care_ymd&section=kensanado&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            var aYmd = ymdToArray(ymd);
            obj["sijibi"] = "指示日："+aYmd.slash_mdw;
            bindDataToForm("efmCareKensanado", obj);
            var kensa_names_stream = ","+(obj.kensa_names ? obj.kensa_names+"," : "");
            <? global $care_kensa_names; // 旧形式も確認 ?>
            <? foreach ($care_kensa_names as $k => $v) { // 旧形式も確認 ?>
                if (obj["kensa_names_<?=$k?>"]) kensa_names_stream += ",<?=$v?>";<? // 旧形式も確認 ?>
            <? } ?>
            var html = [];
            var idx = 0;
            for (var key in dfmMstOptions.mstOptions["kensa"]) {
                idx++;
                var opt_name = dfmMstOptions.mstOptions["kensa"][key].opt_name;
                var checked = (kensa_names_stream.indexOf(opt_name)>=0 ? " checked" : "");
                html.push(
                    '<nobr><label><input type="checkbox" class="checkbox" value="'+opt_name+'"'+checked+
                    ' onclick="dfmCareKensanado.kensaChecked(this)">'+opt_name+'</label></nobr> <wbr> '
                );
            }
            ee("efmCareKensanado__kensa").innerHTML = html.join("");
            $('#efmCareKensanado').dialog({
                title:'ケア入力（検査等）', width:750, height:550, extButtons:["up","down","save"], dlgFooter:obj, saveExec:saveCareKensanado
            });
        }});
    },
    kensaChecked : function(chk) {
        var ary = [];
        $("#efmCareKensanado__kensa input").each(function(){
            if (this.checked) ary.push(this.value);
        });
        ee("efmCareKensanado__kensa_names").value = ary.join(",");
    },
    popupTimeAssist : function(btn) {
        var nm = btn.getAttribute("name").split("@");
        var hidden = getFormElementByName(document.efmCareKensanado, nm[0]);
        var callbackFunc = function(hm, colon_hm) {
            hidden.value = hm;
            btn.innerHTML = colon_hm;
        }
        dfmHMAssist.popup(hidden.value, callbackFunc);
    }
}
</script>








<? //**************************************************************************************************************** ?>
<? // （Ｅ−８）                                                                                                      ?>
<? // ケア入力（観察項目）                                                                                            ?>
<? //**************************************************************************************************************** ?>
<form id="efmCareKansatuTemplate">
    <table>
        <tbody class="repeat_object_template" namespace="kansatu" delete_type="elimination">
        <tr>
        <td><select basename="koumoku" style="width:250px" opt_group="kansatu" onchange="dfmCareKansatu.koumokuChanged(TEMPLATE_IDX)" /></select></td>
        <td><select basename="sinya" style="width:120px"></select></td>
        <td><select basename="nikkin" style="width:120px"></select></td>
        <td><select basename="junya" style="width:120px"></select></td>
        <td><button type="button" onclick="{TOGGLE_EVENT}" class="w60">削除</button></td>
        </tr>
        </tbody>
    </table>
</form>
<form id="efmCareKansatu" name="efmCareKansatu">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="sijibi"></th>
        <td class="right"><button type="button" onclick="dfmCareKansatu.loadLastKansatu()">前回観察項目内容の引用</button></td>
    </tr></table></div>

    <div id="efmCareKansatu_MasqueradeInfo" style="padding-bottom:5px;"></div>

    <table cellspacing="0" cellpadding="0"><tr>
        <td>
            <table cellspacing="0" cellpadding="0" class="general_list" style="width:auto">
            <tr>
                <th>観察項目内容</th><th>深夜</th><th>日勤</th><th>準夜</th><th style="width:40px"></th>
            </tr>
            <tbody class="repeat_object_container" namespace="kansatu"></tbody>
            </table>

        </td>
        <td class="bottom"><button type="button" onclick="repeatObjectFieldAdd('efmCareKansatu','kansatu')" class="w60">追加</button></td>
    </tr></table>
</form>
<script type="text/javascript">
var dfmCareKansatu = {
    form : document.efmCareKansatu,
    sijibi : "",
    $dialogForm: "",
    popup : function(ymd, isLoadMasqueradeLastData){
        this.sijibi = ymd;
        var self = this;
        var saveCareKansatu = function() {
            if (!checkInputError("efmCareKansatu")) return false;
            var ret = false;
            var sData = serializeUnicodeEntity("efmCareKansatu");
            $.ajax({ url:baseUrl+"&ajax_action=save_care_ymd&section=kansatu&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd, data:sData, success:function(msg) {
                var obj = getCustomTrueAjaxResult(msg); if(obj==AJAX_ERROR) return;
                dfmMainMenu.changeView(view_mode, ptif_id, ext_view_mode);
                ret = true;
            }});
            return ret;
        }
        var param = (isLoadMasqueradeLastData ? "&load_masquerade_last_data=force" : "&load_masquerade_last_data=if_not");
        $.ajax({ url:baseUrl+"&ajax_action=get_care_ymd&section=kansatu&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd+param, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            var field_count = int(obj["kansatu__field_count"]);
            obj["kansatu__field_count"] = field_count;
            var aYmd = ymdToArray(ymd);
            obj["sijibi"] = "指示日："+aYmd.slash_mdw;
            bindDataToForm("efmCareKansatu", obj);

            ee("efmCareKansatu_MasqueradeInfo").innerHTML = "";
            var stayValue = 1;
            if (obj["ymd"]!=ymd) {
                stayValue = "";
                if (obj["ymd"]) {
                    var aYmd = ymdToArray(obj["ymd"]);
                    ee("efmCareKansatu_MasqueradeInfo").innerHTML = '【引用観察項目】指示日：<span style="color:red">'+aYmd["slash_mdw"]+'</span>';
                }
            }
            for (var idx=1; idx<=field_count; idx++) self.koumokuChanged(idx, stayValue);
            self.$dialogForm = $('#efmCareKansatu').dialog({
                title:'ケア入力（観察項目）', width:800, height:550, extButtons:["up","down","save"], dlgFooter:obj, saveExec:saveCareKansatu
            });
        }});
    },
    koumokuChanged : function(rowIdx, stayValue) {
        var cmb = getFormElementByName(this.form, "kansatu__"+rowIdx+"__koumoku");
        var text = cmb.options[cmb.selectedIndex].text;
        var comma_selection = "";
        for (var idx=0; idx<dfmMstOptions.mstOptions["kansatu"].length; idx++) {
            var nm = dfmMstOptions.mstOptions["kansatu"][idx]["opt_name"];
            if (nm==text) { comma_selection = dfmMstOptions.mstOptions["kansatu"][idx]["opt_comma_selection"]; break; }
        }
        var ary = comma_selection.split(",");
        var cmbs = [
            getFormElementByName(this.form, "kansatu__"+rowIdx+"__sinya"),
            getFormElementByName(this.form, "kansatu__"+rowIdx+"__nikkin"),
            getFormElementByName(this.form, "kansatu__"+rowIdx+"__junya")
        ];
        for (var cidx=0; cidx<=2; cidx++) {
            var cmb = cmbs[cidx];
            var cVal = getComboValue(cmb);
            cmb.options.length = 1;
            cmb.selectedIndex = 0;
            cmb.options[0] = new Option("", "");
            var ridx = 0;
            for (var idx=1; idx<=ary.length; idx++) {
                if (ary[idx-1]=="") continue;
                ridx++;
                cmb.options.length = cmb.options.length + 1;
                cmb.options[ridx] = new Option(ary[idx-1], ary[idx-1]);
            }
            if (stayValue) setComboValue(cmb, cVal);
        }
    },
    loadLastKansatu : function() {
        if (!confirm("この指示日の直近過去の観察項目があれば読み込みます。\nこのダイアログは開きなおされます。\n\nよろしいですか？")) return;
        if (this.$dialogForm) this.$dialogForm.dialog("destroy");
        this.$dialogForm = 0;
        this.popup(this.sijibi,1);
    }
}
</script>













<? //**************************************************************************************************************** ?>
<? // （Ｅ−９）                                                                                                      ?>
<? // ケア入力（サイン）                                                                                              ?>
<? //**************************************************************************************************************** ?>
<form id="efmCareSign" name="efmCareSign">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="sijibi"></th><td></td>
    </tr></table></div>

    <input type="hidden" name="sign_emp_nm_sinya" />
    <input type="hidden" name="sign_emp_nm_nikkin" />
    <input type="hidden" name="sign_emp_nm_junya" />
    <input type="hidden" name="sign_ymdhms_sinya" />
    <input type="hidden" name="sign_ymdhms_nikkin" />
    <input type="hidden" name="sign_ymdhms_junya" />
    <table cellspacing="0" cellpadding="0" class="general_list">
        <tr>
            <th width="200">深夜</th>
            <th width="200">日勤</th>
            <th width="200">準夜</th>
        </tr>
        <tr height="26px">
            <td class="literal center" name="sign_emp_nm_sinya"></td>
            <td class="literal center" name="sign_emp_nm_nikkin"></td>
            <td class="literal center" name="sign_emp_nm_junya"></td>
        </tr>
        <tr height="26px">
            <td class="literal center" name="sign_ymdhms_sinya_disp"></td>
            <td class="literal center" name="sign_ymdhms_nikkin_disp"></td>
            <td class="literal center" name="sign_ymdhms_junya_disp"></td>
        </tr>
        <tr>
            <td style="text-align:center">
                <button type="button" onclick="dfmCareSign.saveCareSign('sinya')" class="w120">確認サイン登録</button>
                <button type="button" onclick="dfmCareSign.saveCareSign('sinya', 1)" class="w60">クリア</button>
            </td>
            <td style="text-align:center">
                <button type="button" onclick="dfmCareSign.saveCareSign('nikkin')" class="w120">確認サイン登録</button>
                <button type="button" onclick="dfmCareSign.saveCareSign('nikkin', 1)" class="w60">クリア</button>
            </td>
            <td style="text-align:center">
                <button type="button" onclick="dfmCareSign.saveCareSign('junya')" class="w120">確認サイン登録</button>
                <button type="button" onclick="dfmCareSign.saveCareSign('junya', 1)" class="w60">クリア</button>
            </td>
        </tr>
    </table>
</form>
<script type="text/javascript">
var dfmCareSign = {
    sijibi : "",
    $dialogForm: "",
    popup : function(ymd){
        this.sijibi = ymd;
        var self = this;
        $.ajax({ url:baseUrl+"&ajax_action=get_care_ymd&section=sign&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            var aYmd = ymdToArray(ymd);
            obj["sijibi"] = "指示日："+aYmd.slash_mdw;
            var aYmdS = ymdToArray(obj["sign_ymdhms_sinya"]);
            var aYmdN = ymdToArray(obj["sign_ymdhms_nikkin"]);
            var aYmdJ = ymdToArray(obj["sign_ymdhms_junya"]);
            obj["sign_ymdhms_sinya_disp"] = aYmdS.slash_md_hms;
            obj["sign_ymdhms_nikkin_disp"] = aYmdN.slash_md_hms;
            obj["sign_ymdhms_junya_disp"] = aYmdJ.slash_md_hms;
            bindDataToForm("efmCareSign", obj);
            self.$dialogForm = $('#efmCareSign').dialog({
                title:'ケア入力（確認サイン）', width:700, height:250
            });
        }});
    },
    saveCareSign : function(jikantai, isClear) {
        var self = this;
        $.ajax({ url:baseUrl+"&ajax_action=get_ymdhms", success:function(msg) {
            msg = getCustomTrueAjaxResult(msg); if(msg==AJAX_ERROR) return;
            document.efmCareSign["sign_ymdhms_"+jikantai].value = msg;
            var nm = "<?=GG_LOGIN_EMP_NAME?>";
            if (isClear) nm = "";
            document.efmCareSign["sign_emp_nm_"+jikantai].value = nm;
            var sData = serializeUnicodeEntity("efmCareSign");
            $.ajax({ url:baseUrl+"&ajax_action=save_care_ymd&section=sign&ptif_id="+uEnc(ptif_id)+"&ymd="+self.sijibi, data:sData, success:function(msg) {
                var obj = getCustomTrueAjaxResult(msg); if(obj==AJAX_ERROR) return;
                dfmMainMenu.changeView(view_mode, ptif_id, ext_view_mode);
                self.$dialogForm.dialog("close");
            }});
        }});
    }
}
</script>











<? //**************************************************************************************************************** ?>
<? // （Ｆ−１）                                                                                                      ?>
<? // 患者附帯情報                                                                                                    ?>
<? //**************************************************************************************************************** ?>
<form id="efmPtifDetail" name="efmPtifDetail">
    <input type="hidden" name="nyuin_taiin_list" value="" />
    <table cellspacing="0" cellpadding="0" style="width:auto">
        <tr>
            <th style="line-height:26px;" width="90">患者名</th>
            <td style="line-height:26px;" class="literal" name="ptif_name"></td>
            <td width="auto"></td>
        </tr>
        <tr>
            <th style="line-height:26px;">患者名かな</th>
            <td style="line-height:26px;" class="literal" name="ptif_kana_name"></td>
            <td style="line-height:26px; padding-left:20px; color:#aaaaaa">※一覧表示順で利用</td>
        </tr>
    </table>
    <hr>
    <table cellspacing="0" cellpadding="0"><tr>
    <td>
        <table cellspacing="0" cellpadding="0">
            <tr><th width="90">病棟</th><td id="efmPtifDetail_bldg_ward_container"></td></tr>
            <tr><th>病室</th><td id="efmPtifDetail_ptrm_room_no_container" style="padding:3px 0"></td></tr>
            <tr><th>主治医</th><td id="efmPtifDetail_dr_name_container" style="padding:3px 0"></td></tr>
            <tr><th>入院履歴</th><td id="efmPtifDetail_nyuin_taiin_container"></td></tr>
        </table>
    </td>
    <td style="padding-left:20px; border-left:1px solid #dddddd; vertical-align:top">
        <table cellspacing="0" cellpadding="0"><tr>
        <td>備考</td>
        <td style="padding-left:2px"><textarea rows="4" cols="25" name="ptif_bikou" style="width:350px"></textarea></td>
        </tr></table>
    </td>
    </tr></table>
</form>
<script type="text/javascript">
var dfmPtifDetail = {
    $dialogForm: "",
    nyuin_taiin_error : "",
    popup : function(_ptif_id){
        var self = this;
        var savePtifDetail = function() {
            if (!checkInputError("efmPtifDetail")) return false;
            if (self.nyuin_taiin_error=="1") { alert("入院履歴の指定に不備があります。"); return false; }
            if (self.nyuin_taiin_error=="2") { alert("入院履歴の日付並び順に不備があります。"); return false; }
            var ret = false;
            var sData = serializeUnicodeEntity("efmPtifDetail");
            $.ajax({ url:baseUrl+"&ajax_action=save_ptif_attribute&ptif_id="+uEnc(_ptif_id), data:sData, success:function(msg) {
                if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
                dfmMainMenu.changeView(view_mode, '', ext_view_mode);
                ret = true;
            }});
            return ret;
        }
        $.ajax({ url:baseUrl+"&ajax_action=get_ptif_attribute&ptif_id="+uEnc(_ptif_id), success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            self.remakePtrmDropdown(obj.bldg_ward);
            bindDataToForm("efmPtifDetail", obj);
            var nt_list = obj.nyuin_taiin_list.split(",");
            var nt_options = [];
            for ( var idx=0; idx<nt_list.length; idx++) {
                if (nt_list[idx]=="") continue;
                var aNT = (nt_list[idx]+"-").split("-");
                var aYmdN = ymdToArray(aNT[0]);
                var aYmdT = ymdToArray(aNT[1]);
                nt_options.push('<option value="N'+aNT[0]+'">[入院]  '+aYmdN.slash_ymd+'</option>');
                nt_options.push('<option value="T'+aNT[1]+'">[退院]  '+aYmdT.slash_ymd+'</option>');
            }
            ee("efmPtifDetail_nyuin_taiin_container").innerHTML =
                '<select name="nyuin_taiin" size="5" onclick="dfmPtifDetail.popupCalendar(this)" style="width:200px"'+
                ' onkeydown="if(isEnterKey(this,event)) dfmPtifDetail.popupCalendar(this); return false;" onblur="this.selectedIndex=-1;">'+
                nt_options.join("")+"</select>";
            ee("efmPtifDetail_dr_name_container").innerHTML =
                '<input type="text" class="text text_readonly" style="width:130px" readOnly value="'+htmlEscape(obj.dr_emp_name)+'">'+
                '<input type="hidden" name="dr_emp_id" value="'+htmlEscape(obj.dr_emp_id)+'" />'+
                '<button onclick="dfmPtifDetail.popupDrSelector()" class="w60"<?=(!GG_ONDOBAN_ADMIN_FLG ?" disabled":"")?>>候補</button>';
            self.nyuinTaiinCheck(document.efmPtifDetail.nyuin_taiin);
            var aryButtons = ["save"];
            <? if (!GG_ONDOBAN_ADMIN_FLG) { ?>
                aryButtons = [];
                efmPtifDetail.bldg_ward.disabled = true;
                efmPtifDetail.ptrm_room_no.disabled = true;
                efmPtifDetail.nyuin_taiin.disabled = true;
            <? } ?>
            self.$dialogForm = $('#efmPtifDetail').dialog({ title:'患者附帯情報（システム管理者＋温度板管理者のみ）', width:750, height:330, position:"left",
                extButtons:aryButtons, saveExec:savePtifDetail });
        }});
    },
    remakePtrmDropdown : function(bldg_ward) {
        var options = [];
        for (var idx=0; idx<dfmMstBldgWard.mstPtrm.length; idx++) {
            var row = dfmMstBldgWard.mstPtrm[idx];
            if (row.bldg_ward != bldg_ward) continue;
            options.push('<option value="'+row.ptrm+'">'+row.name+'</option>');
        }
        options.push('<option></option>');
        ee("efmPtifDetail_ptrm_room_no_container").innerHTML = '<select name="ptrm_room_no">'+options.join("")+'</select>';
    },
    selectPtifAndShowOndoban : function(_ptif_id) {
        if (this.$dialogForm) this.$dialogForm.dialog("destroy");
        this.$dialogForm = 0;
        dfmMainMenu.reloadPtifMaster(_ptif_id);
        dfmMainMenu.changeView("ondoban", _ptif_id);
    },
    popupCalendar : function(ddl) {
        var selectedIndex = ddl.selectedIndex;
        var self = this;
        var nt = getComboValue(ddl);
        var nyuin_or_taiin = substr2(nt, 0, 1);
        var ymd = substr2(nt, 1);
        var ymdCallbackFunc = function(ymd, aYmd) {
            var opt = ddl.options[selectedIndex];
            opt.value = nyuin_or_taiin + ymd;
            opt.text = (nyuin_or_taiin=="N"?"[入院]":"[退院]") + "  " + aYmd.slash_ymd;
            self.nyuinTaiinCheck(ddl);
        }
        dfmYmdAssist.popup("ymd", ymd, "", ymdCallbackFunc);
    },
    popupDrSelector : function() {
        var callbackFunc = function(emp_id, emp_name) {
            ee("efmPtifDetail_dr_name_container").innerHTML =
                '<input type="text" class="text text_readonly" style="width:130px" readOnly value="'+htmlEscape(emp_name)+'">'+
                '<input type="hidden" name="dr_emp_id" value="'+htmlEscape(emp_id)+'" />'+
                '<button onclick="dfmPtifDetail.popupDrSelector()" class="w60">候補</button>';
        }
        dfmDrSelector.popup(callbackFunc);
    },
    nyuinTaiinCheck : function(ddl) {
        var needAppend = "N";
        var len = ddl.options.length;
        for (var idx=len-1; idx>=0; idx--) {
            var nt = ddl.options[idx].value;
            if (nt.length==1) { ddl.options.length = idx; continue; }
            needAppend = (nt.substring(0,1)=="N"?"T":"N");
            break;
        }
        var len = ddl.options.length;
        this.nyuin_taiin_error = "";
        var nyuin_taiin_list = "";
        var prev = "";
        for (var idx=0; idx<ddl.options.length; idx++) {
            var nt = ddl.options[idx].value;
            if (nt.length==1) { this.nyuin_taiin_error = "1"; break; }
            if (prev > nt.substring(1)) { this.nyuin_taiin_error = "2"; break; }
            nyuin_taiin_list += nt;
            prev = nt.substring(1);
        }
        nyuin_taiin_list = nyuin_taiin_list.replace(/N/g, ",").replace(/T/g, "-");
        if (nyuin_taiin_list) nyuin_taiin_list = nyuin_taiin_list.substring(1);
        document.efmPtifDetail.nyuin_taiin_list.value = nyuin_taiin_list;
        ddl.options[ddl.options.length] = new Option((needAppend=="T"?"（退院未登録）":"（入院未登録）"), (needAppend=="T"?"T":"N"));
    }
}
</script>











<? //**************************************************************************************************************** ?>
<? // （Ｆ−２）                                                                                                      ?>
<? // 主治医選択                                                                                                      ?>
<? //**************************************************************************************************************** ?>
<form id="efmDrSelector" name="efmDrSelector">
    <div id="dfmDrSelector_list_container"></div>
    <div style="padding-top:4px"><button type="button" onclick="dfmDrSelector.settle()" style="width:80px">クリア</button></div>
</form>
<script type="text/javascript">
var dfmDrSelector = {
    callbackFunc : "",
    $dialogForm: "",
    popup : function(_callbackFunc){
        this.callbackFunc = _callbackFunc;
        var self = this;
        $.ajax({ url:baseUrl+"&ajax_action=get_dr_list", success:function(msg) {
            var list = getCustomTrueAjaxResult(msg, TYPE_JSON); if(html==AJAX_ERROR) return;
            if (!list || !list.length) return alert("選択肢はありません。");
            var html = [];
            html.push('<select size="20" style="width:100%"');
            html.push(' onclick="dfmDrSelector.settle(this, selectedIndex);"');
            html.push(' onkeydown="if (isEnterKey(this,event)) { dfmDrSelector.settle(this, selectedIndex); return false; }"');
            html.push('>');
            for (var idx=0; idx<list.length; idx++) {
                var obj = list[idx];
                html.push('<option value="'+htmlEscape(obj.emp_id)+'">'+htmlEscape(obj.emp_nm)+'</option>');
            }
            html.push('</select>');
            ee("dfmDrSelector_list_container").innerHTML = html.join("");
            self.$dialogForm = $('#efmDrSelector').dialog({ title:'主治医選択', width:250, height:550 });
        }});
    },
    settle : function(combo, selectedIndex) {
        var emp_id = "";
        var emp_nm = "";
        if (combo) {
            emp_id = combo.options[combo.selectedIndex].value;
            emp_nm = combo.options[combo.selectedIndex].text;
        }
        this.callbackFunc(emp_id, emp_nm);
        this.$dialogForm.dialog("close");
    }
}
</script>












<? //**************************************************************************************************************** ?>
<? // （Ｆ−３）                                                                                                      ?>
<? // 指示板詳細：注射処方等指示                                                                                      ?>
<? //**************************************************************************************************************** ?>
<form id="efmSijiban" name="efmSijiban">

    <div style="width:384px;">
        <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
            <th class="literal ptif_name" name="ptif_name"></th><th class="literal sijibi" name="slash_mdw"></th>
            <td style="width:auto"></td>
            <td style="width:90px; background-color:#2a9cf4; color:#ffffff; line-height:26px; text-align:center; vertical-align:baseline" class="literal" name="field_number" id="efmSijiban_field_number"></td>
        </tr></table></div>

        <div id="efmSijiban_controller_container"></div>

        <div><textarea rows="4" cols="35" name="siji_text" style="width:376px"></textarea></div>

        <div>
            <div style="background-color:#ace0ff">
                <input type="hidden" name="siji_picture_names" />
                <table cellspacing="0" cellpadding="0"><tr>
                <td>画像</td><td class="right"><button type="button" onclick="dfmSijiban.popupPictureUploader()">画像添付</button></td>
                </tr></table>
            </div>
        </div>
        <div class="picture_area literal" style="height:40px" name="picture_area_s_html" id="efmSijiban_picture_area_s_html"></div>

        <div id="efmSijiban_zenkai_area" style="padding-top:5px; height:210px"></div>

        <div>
            <div style="background-color:#ace0ff; padding:3px 0 0 3px">
                <table cellspacing="0" cellpadding="0"><tr>
                <td>抗生剤</td><td class="right"><button type="button" onclick="dfmSijiban.popupKouseizai()" class="w60">抗生剤</button></td>
                </tr></table>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
var dfmSijiban = {
    form : document.efmSijiban,
    org_ymd : "",
    ptif_id : "",
    ymd : "",
    field_number : "",
    $dialogForm: "",
    $uploadForm: "",
    popup : function(_ptif_id, ymd, field_number) {
        var self = this;
        this.ptif_id = _ptif_id;
        this.org_ymd = ymd;
        this.ymd = ymd;
        this.field_number = int(field_number);
        var self = this;
        ee("efmSijiban_zenkai_area").innerHTML = "";
        ee("efmSijiban_controller_container").innerHTML =
        '<table cellspacing="0" cellpadding="0"><tr>'+
        '<td><select opt_group="siji_az" name="siji_az" onchange="dfmSijiban.refreshZenkaiArea();" style="width:50px" title="az値"></select></td>'+
        '<td><label><select name="is_continue" title="継続指定"><option value="">(当日のみ)</option><option value="1">継続</option></select></td>'+
        '<td class="right">'+
        '<label style="padding-right:2px"><input type="checkbox" class="checkbox" name="is_sumi" value="1"'+
        ' onclick="dfmSijiban.changeSumiColor()">実施済み</label>'+
        '&nbsp;<label style="padding-right:16px"><input type="checkbox" class="checkbox" name="is_stop" value="1"'+
        ' onclick="dfmSijiban.changeStopColor()">中止</label>'+
        '</td></tr></table>';

            var obj = this.startEdit(this.ymd, this.field_number);

        var buttons = ["save"];
        if (this.field_number) buttons = ["save", "delete"];
        this.$dialogForm = $('#efmSijiban').dialog({
            title:"注射処方等指示", width:400, height:550, extButtons:buttons, dlgFooter:obj,
            saveExec:function() { return self.saveSijiban('',''); }, deleteExec:function() { return self.saveSijiban('', 1); }
        });
    },
    saveSijiban : function(tryCommit, is_delete) {
        var self = this;
        if (!tryCommit) tryCommit = "";
        if (!is_delete) is_delete = "";
        if (is_delete && this.form.siji_picture_names.value!="") {
            alert("削除する場合は、先に画像を削除してください。");
            return false;
        }
        var ret = false;
        var sData = serializeUnicodeEntity("efmSijiban");
        var url =
        baseUrl+"&ajax_action=save_siji_ymd_one&section=chusya&ptif_id="+uEnc(this.ptif_id)+
        "&ymd="+this.ymd+"&field_number="+self.field_number+"&try_commit="+tryCommit+"&is_delete="+is_delete;
        $.ajax({ url:url, data:sData, success:function(msg) {
            if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
            if (!tryCommit) {
                if (is_delete && !confirm("削除します。よろしいですか？")) return;
                ret = self.saveSijiban(1, is_delete);
                return;
            }
            dfmMainMenu.changeView(view_mode, self.ptif_id, ext_view_mode);
            ret = true;
        }});
        return ret;
    },
    refreshZenkaiArea : function() {
        var az = getComboValue(efmSijiban.siji_az);
        var self = this;
        var url = baseUrl+"&ajax_action=get_siji_ymd_zenkai_az&ptif_id="+uEnc(this.ptif_id)+"&ymd="+this.ymd+"&az="+az;
        $.ajax({ url:url, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;

            html = [];
            if (!obj || !obj.ymd) {
                html.push('<div style="background-color:#ace0ff; padding:4px">'+(az?"「"+az+"」の":"")+'前回情報</div>');
                if (az) html.push('<div style="padding-top:5px" class="instruction">※前回情報はありません。</div>');
                else html.push('<div style="padding-top:5px" class="instruction">※a-zを指定してください。</div>');
            } else {
                html.push('<div style="background-color:#ace0ff;">');
                html.push('<table cellspacing="0" cellpadding="0"><tr>');
                html.push('<td style="padding:3px">「'+az+'」の前回情報</td>');
                html.push('<td class="right"><button onclick="dfmSijiban.appendZenkaiSiji()">↑指示文章を追記</button></td>');
                html.push('</tr><tr>');
                html.push('<td class="right" colspan="2" style="padding:3px">');
                html.push('<span style="padding-right:5px">指示日：'+obj["slash_mdw"]+'</span>');
                if (obj["is_continue"]) html.push('<span class="is_continue">継続</span>');
                if (obj["is_sumi"]) html.push('<span class="is_sumi">実施済み</span>');
                if (obj["is_stop"]) html.push('<span class="is_stop">中止</span>');
                html.push('</td></tr></table></div>');

                html.push('<table cellspacing="0" cellpadding="0" class="siji_panel" style="width:100%"><tr>');
                html.push('</tr></table>');
                html.push('<textarea rows="4" cols="35" style="width:376px" class="text_readonly" id="dfmSijiban_ta_zenkai_siji">');
                html.push(htmlEscape(obj["siji_text"])+'</textarea>');
                html.push('<div class="picture_area" style="height:40px">'+obj["siji_picture_area_s_html"]+'</div>');
            }

            ee("efmSijiban_zenkai_area").innerHTML = html.join("");
        }});
    },
    changeSumiColor : function() {
        if (this.form.is_sumi) this.form.is_sumi.parentNode.className = (this.form.is_sumi.checked ? "is_sumi" : "");
    },
    changeStopColor : function() {
        if (this.form.is_stop) this.form.is_stop.parentNode.className = (this.form.is_stop.checked ? "is_stop" : "");
    },
    popupKouseizai : function() {
        dfmCareKouseizai.popup(this.ptif_id, this.ymd, getComboValue(efmSijiban.siji_az));
    },
    startEdit : function(ymd, field_number) {
        if (!field_number && this.$dialogForm) this.$dialogForm.dialog("hideDeleteButton");
        var self = this;
        if (ymd=="") ymd = this.ymd;
        //ee("dfmSijiban_new_button").style.display = (field_number ? "" : "none");
        this.ymd = ymd;
        this.field_number = int(field_number);
        var ret = {};
        var url =
        baseUrl+"&ajax_action=get_siji_ymd_one&section=chusya&ptif_id="+uEnc(this.ptif_id)+
        "&ymd="+this.ymd+"&field_number="+this.field_number;
        $.ajax({ url:url, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            ret = obj;
            obj["field_number"] = "No."+self.field_number;
            if (!self.field_number) obj["field_number"] = "(新規作成)";
            bindDataToForm("efmSijiban", obj);
            self.changeSumiColor();
            self.changeStopColor();
            self.refreshZenkaiArea();
        }});
        return ret;
    },
    popupPictureUploader : function() {
        var html =
        '<div><iframe src="ks_picture.php?session='+gg_session_id+'&ptif_id='+this.ptif_id+'&ymd='+this.ymd+'&field_number='+this.field_number+'"'+
        ' frameborder="0" width="600" height="470" scrolling="no" marginheight="0" marginwidth="0" noresize></iframe></div>';
        this.$uploadForm = $(html).dialog({ title:"添付画像アップロード", width:616, height:530 });
    },
    puctureUploadCallback : function(field_number, siji_picture_names, picture_area_html) {
        this.field_number = field_number;
        ee("efmSijiban_field_number").innerHTML = "No."+field_number;
        this.form.siji_picture_names.value = siji_picture_names;
        ee("efmSijiban_picture_area_s_html").innerHTML = picture_area_html;
        dfmMainMenu.changeView(view_mode, this.ptif_id, ext_view_mode);
    },
    appendZenkaiSiji : function() {
        this.form.siji_text.value += ee("dfmSijiban_ta_zenkai_siji").value;
    }
}
</script>













<? //**************************************************************************************************************** ?>
<? // （Ｆ−４）                                                                                                      ?>
<? // 指示板備考・MEMO                                                                                                ?>
<? //**************************************************************************************************************** ?>
<form id="efmSijibanBikou" name="efmSijibanBikou">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name"></th><th class="literal sijibi" name="slash_mdw"></th>
        <td style="width:auto"></td>
        <td style="width:90px; background-color:#2a9cf4; color:#ffffff; line-height:26px; text-align:center; vertical-align:baseline" class="literal" name="field_number"></td>
    </tr></table></div>
    <div style="padding-bottom:3px"><label><input type="checkbox" class="checkbox" name="is_henkou" value="1"
     onclick="dfmSijibanBikou.changeHenkouColor()">指示変更</label></div>
    <div><textarea rows="8" cols="39" name="siji_text" style="width:418px"></textarea></div>
</form>
<script type="text/javascript">
var dfmSijibanBikou = {
    form : document.efmSijibanBikou,
    org_ymd : "",
    ptif_id : "",
    ymd : "",
    field_number : "",
    $dialogForm: "",
    section : "",
    popup : function(section, _ptif_id, ymd, field_number, real_ymd) {
        var self = this;
        this.section = section;
        this.ptif_id = _ptif_id;
        this.org_ymd = ymd;
        this.ymd = ymd;
        this.field_number = int(field_number);
        var self = this;
        var dTitle = "MEMO";
        if (section=="weekly_bikou") dTitle = "備考（週次）";
        var obj = {};
        obj = this.refreshEditArea();
        if (real_ymd) obj = this.startEdit(real_ymd, this.field_number);
        var buttons = ["save"];
        if (this.field_number) buttons = ["save", "delete"];
        this.$dialogForm = $('#efmSijibanBikou').dialog({
            title:dTitle, width:440, height:300, extButtons:buttons, dlgFooter:obj,
            saveExec:function() { return self.saveSijibanBikou('',''); }, deleteExec:function() { return self.saveSijibanBikou('', 1); }
        });
        this.changeHenkouColor();
    },
    changeHenkouColor : function() {
        if (this.form.is_henkou) this.form.is_henkou.parentNode.className = (this.form.is_henkou.checked ? "is_stop" : "");
    },

    saveSijibanBikou : function(tryCommit, is_delete) {
        var self = this;
        if (!tryCommit) tryCommit = "";
        if (!is_delete) is_delete = "";
        var ret = false;
        var sData = serializeUnicodeEntity("efmSijibanBikou");
        var url =
        baseUrl+"&ajax_action=save_siji_ymd_one&section="+this.section+"&ptif_id="+uEnc(this.ptif_id)+
        "&ymd="+this.ymd+"&field_number="+self.field_number+"&try_commit="+tryCommit+"&is_delete="+is_delete;
        $.ajax({ url:url, data:sData, success:function(msg) {
            if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
            if (!tryCommit) {
                if (is_delete && !confirm("削除します。よろしいですか？")) return;
                ret = self.saveSijibanBikou(1, is_delete);
                return;
            }
            dfmMainMenu.changeView(view_mode, self.ptif_id, ext_view_mode);
            ret = true;
        }});
        return ret;
    },
    refreshEditArea : function() {
        var ret = {};
        var self = this;
        var url =
        baseUrl+"&ajax_action=get_siji_ymd_one&section="+this.section+"&ptif_id="+uEnc(this.ptif_id)+
        "&ymd="+this.ymd+"&field_number="+this.field_number;
        $.ajax({ url:url, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            ret = obj;
            obj["field_number"] = "No."+self.field_number;
            if (!self.field_number) obj["field_number"] = "(新規作成)";
            bindDataToForm("efmSijibanBikou", obj);
        }});
        return ret;
    },
    startEdit : function(ymd, field_number) {
        if (!field_number && this.$dialogForm) this.$dialogForm.dialog("hideDeleteButton");
        if (ymd=="") ymd = this.ymd;
        //ee("dfmSijibanBikou_new_button").style.display = (field_number ? "" : "none");
        this.ymd = ymd;
        this.field_number = int(field_number);
        return this.refreshEditArea();
    },
    popupTimeAssist : function(btn) {
        var self = this;
        var nm = btn.getAttribute("name").split("@");
        var callbackFunc = function(hm, colon_hm) {
            document.efmSijibanBikou[nm[0]].value = hm;
            btn.innerHTML = colon_hm;
        }
        dfmHMAssist.popup(document.efmSijibanBikou[nm[0]].value, callbackFunc);
    }
}
</script>











<? //**************************************************************************************************************** ?>
<? // （Ｆ−５）                                                                                                      ?>
<? // 指示板SOAP                                                                                                      ?>
<? //**************************************************************************************************************** ?>
<form id="efmSijibanSoap" name="efmSijibanSoap">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name"></th><th class="literal sijibi" name="slash_mdw"></th>
        <td style="width:auto"></td>
        <td style="width:90px; background-color:#2a9cf4; color:#ffffff; line-height:26px; text-align:center; vertical-align:baseline" class="literal" name="field_number"></td>
    </tr></table></div>

    <div id="efmSijibanSoap_action_hm" style="padding:3px 0">
        <table cellspacing="0" cellpadding="0"><tr><td>
        事象時刻：<button type="button" class="hm_button" name="action_hm@button" onclick="dfmSijibanSoap.popupTimeAssist(this)">:</button>
        <input type="hidden" name="action_hm" />
        </td>
        <td id="efmSijibanSoap_jikantai_kouho" style="height:24px; line-height:24px; color:#999999">
        </td>
        <td class="right">
        <span style="padding-left:20px">時間帯：<select name="soap_jikantai">
            <option value=""></option>
            <option value="sinya">深夜</option>
            <option value="nikkin">日勤</option>
            <option value="junya">準夜</option>
        </select></span>
        </td></tr></table>
    </div>
    <div style="padding-bottom:2px">[S] - [O] - [A]</div>
    <div><textarea rows="5" cols="39" name="siji_text" style="width:418px"></textarea></div>
    <div style="padding-top:6px; padding-bottom:2px">[P]</div>
    <div><textarea rows="5" cols="39" name="siji_ptext" style="width:418px"></textarea></div>
</form>
<script type="text/javascript">
var dfmSijibanSoap = {
    form : document.efmSijibanSoap,
    org_ymd : "",
    ptif_id : "",
    ymd : "",
    field_number : "",
    $dialogForm: "",
    section : "",
    popup : function(section, _ptif_id, ymd, field_number, real_ymd) {
        var self = this;
        this.section = section;
        this.ptif_id = _ptif_id;
        this.org_ymd = ymd;
        this.ymd = ymd;
        this.field_number = int(field_number);
        var self = this;
        //ee("dfmSijibanSoap_new_button").style.display = (field_number ? "" : "none");
        var obj = {};
        obj = this.refreshEditArea();
        if (real_ymd) obj = this.startEdit(real_ymd, this.field_number);
        self.showJikantaiKouho(obj["action_hm"]);
        var buttons = ["save"];
        if (this.field_number) buttons = ["save", "delete"];
        this.$dialogForm = $('#efmSijibanSoap').dialog({
            title:"SOAP", width:440, height:450, extButtons:buttons, dlgFooter:obj,
            saveExec:function() { return self.saveSijibanBikou('',''); }, deleteExec:function() { return self.saveSijibanBikou('', 1); }
        });
    },
    saveSijibanBikou : function(tryCommit, is_delete) {
        var self = this;
        if (!tryCommit) tryCommit = "";
        if (!is_delete) is_delete = "";
        var ret = false;
        var sData = serializeUnicodeEntity("efmSijibanSoap");
        //if (!is_delete) {
        //  if (self.form.action_hm.value=="") { alert("事象時刻を指定してください。"); return false; }
        //}
        var url =
        baseUrl+"&ajax_action=save_siji_ymd_one&section="+this.section+"&ptif_id="+uEnc(this.ptif_id)+
        "&ymd="+this.ymd+"&field_number="+self.field_number+"&try_commit="+tryCommit+"&is_delete="+is_delete;
        $.ajax({ url:url, data:sData, success:function(msg) {
            if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
            if (!tryCommit) {
                if (is_delete && !confirm("削除します。よろしいですか？")) return;
                ret = self.saveSijibanBikou(1, is_delete);
                return;
            }
            dfmMainMenu.changeView(view_mode, self.ptif_id, ext_view_mode);
            ret = true;
        }});
        return ret;
    },
    refreshEditArea : function() {
        var ret = {};
        var self = this;
        var url =
        baseUrl+"&ajax_action=get_siji_ymd_one&section="+this.section+"&ptif_id="+uEnc(this.ptif_id)+
        "&ymd="+this.ymd+"&field_number="+this.field_number;
        $.ajax({ url:url, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            ret = obj;
            obj["field_number"] = "No."+self.field_number;
            if (!self.field_number) obj["field_number"] = "(新規作成)";
            bindDataToForm("efmSijibanSoap", obj);
        }});
        return ret;
    },
    startEdit : function(ymd, field_number) {
        if (!field_number && this.$dialogForm) this.$dialogForm.dialog("hideDeleteButton");
        if (ymd=="") ymd = this.ymd;
        this.ymd = ymd;
        this.field_number = int(field_number);
        return this.refreshEditArea();
    },
    popupTimeAssist : function(btn) {
        var self = this;
        var nm = btn.getAttribute("name").split("@");
        var callbackFunc = function(hm, colon_hm) {
            document.efmSijibanSoap[nm[0]].value = hm;
            btn.innerHTML = colon_hm;
            self.showJikantaiKouho(hm);
        }
        dfmHMAssist.popup(document.efmSijibanSoap[nm[0]].value, callbackFunc);
    },
    showJikantaiKouho : function(hm) {
        hm = int(hm);
        var ns = int("<?=NIKKIN_START_HHMI?>");
        var ne = int("<?=NIKKIN_END_HHMI?>");
        var js = int("<?=JUNYA_START_HHMI?>");
        var je = int("<?=JUNYA_END_HHMI?>");
        var ss = int("<?=SINYA_START_HHMI?>");
        var se = int("<?=SINYA_END_HHMI?>");
        var html = ['<span style="padding:0 5px">深夜</span>','<span style="padding:0 5px">日勤</span>','<span style="padding:0 5px">準夜</span>'];
        if (ss<=hm && hm < se) html[0] = '<a href="javascript:void(0)" onclick="document.efmSijibanSoap.soap_jikantai.selectedIndex=1;" style="padding:0 5px">深夜</a>';
        if (ns<=hm && hm < ne) html[1] = '<a href="javascript:void(0)" onclick="document.efmSijibanSoap.soap_jikantai.selectedIndex=2;" style="padding:0 5px">日勤</a>';
        if (js<=hm && hm < je) html[2] = '<a href="javascript:void(0)" onclick="document.efmSijibanSoap.soap_jikantai.selectedIndex=3;" style="padding:0 5px">準夜</a>';
        ee("efmSijibanSoap_jikantai_kouho").innerHTML = "("+html.join("/")+")";
    }
}
</script>








<? //**************************************************************************************************************** ?>
<? // （Ｇ−１a）                                                                                                     ?>
<? // フリーワード１指示入力                                                                                          ?>
<? //**************************************************************************************************************** ?>
<form id="efmSijiFreeword">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="slash_mdw"></th><td></td>
    </tr></table></div>

    <div>指示内容<br/><textarea name="siji_naiyou" rows="5" cols="70"></textarea></div>
</form>
<script type="text/javascript">
var dfmSijiFreeword = {
    popup : function(ymd){
        var saveSijiFreeword = function(tryCommit) {
            if (!checkInputError("efmSijiFreeword")) return false;
            if (!tryCommit) tryCommit = "";
            var ret = false;
            var sData = serializeUnicodeEntity("efmSijiFreeword");
            var url = baseUrl+"&ajax_action=save_siji_ymd&ptif_id="+uEnc(ptif_id)+"&section=freeword&ymd="+ymd+"&try_commit="+tryCommit;
            $.ajax({ url:url, data:sData, success:function(msg) {
                if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
                if (!tryCommit) { ret = saveSijiFreeword(1); return; }
                dfmMainMenu.changeView("ondoban", ptif_id);
                ret = true;
            }});
            return ret;
        }
        var aYmd = ymdToArray(ymd);
        $.ajax({ url:baseUrl+"&ajax_action=get_siji_ymd&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd+"&section=freeword", success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            obj["slash_mdw"] = "指示日："+aYmd.slash_mdw;
            bindDataToForm("efmSijiFreeword", obj);
            $('#efmSijiFreeword').dialog({ title:"フリーワード１指示入力", width:690, height:230, extButtons:["save"], saveExec:function() { return saveSijiFreeword(); } });
        }});
    }
}
</script>








<? //**************************************************************************************************************** ?>
<? // （Ｇ−１b）                                                                                                     ?>
<? // フリーワード２指示入力                                                                                          ?>
<? //**************************************************************************************************************** ?>
<form id="efmSijiFree2word">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="slash_mdw"></th><td></td>
    </tr></table></div>

    <div>指示内容<br/><textarea name="siji_naiyou" rows="5" cols="70"></textarea></div>
</form>
<script type="text/javascript">
var dfmSijiFree2word = {
    popup : function(ymd){
        var saveSijiFree2word = function(tryCommit) {
            if (!checkInputError("efmSijiFree2word")) return false;
            if (!tryCommit) tryCommit = "";
            var ret = false;
            var sData = serializeUnicodeEntity("efmSijiFree2word");
            var url = baseUrl+"&ajax_action=save_siji_ymd&ptif_id="+uEnc(ptif_id)+"&section=free2word&ymd="+ymd+"&try_commit="+tryCommit;
            $.ajax({ url:url, data:sData, success:function(msg) {
                if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
                if (!tryCommit) { ret = saveSijiFree2word(1); return; }
                dfmMainMenu.changeView("ondoban", ptif_id);
                ret = true;
            }});
            return ret;
        }
        var aYmd = ymdToArray(ymd);
        $.ajax({ url:baseUrl+"&ajax_action=get_siji_ymd&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd+"&section=free2word", success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            obj["slash_mdw"] = "指示日："+aYmd.slash_mdw;
            bindDataToForm("efmSijiFree2word", obj);
            $('#efmSijiFree2word').dialog({ title:"フリーワード２指示入力", width:690, height:230, extButtons:["save"], saveExec:function() { return saveSijiFree2word(); } });
        }});
    }
}
</script>








<? //**************************************************************************************************************** ?>
<? // （Ｇ−２）                                                                                                      ?>
<? // 食事（経管）指示入力                                                                                            ?>
<? //**************************************************************************************************************** ?>
<form id="efmSijiKeikan" name="efmSijiKeikan">
<div id="efmSijiKeikan_content">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="slash_mdw"></th><td></td>
    </tr></table></div>

    <table cellspacing="0" cellpadding="0" style="width:100%; height:300px"><tr>
    <td style="width:300px">
        <div style="height:32px">指示数：<select name="keikan__field_count" onchange="dfmSijiKeikan.fieldCountChanged(value)" style="width:60px"><?for($pidx=0; $pidx<=5; $pidx++){?><option value="<?=$pidx?>"><?=$pidx?></option><?}?></select></div>
        <div style="height:194px">
        <table cellspacing="0" cellpadding="0" class="general_list line_height26">
            <tr id="efmSijiKeikan_ptr1"><th width="26">1</th><td>
                <a href="javascript:void(0)" class="block1" onclick="dfmSijiKeikan.changeKeikanPanel(1)" id="efmSijiKeikan_plink1" style="height:26px; padding-left:4px"></a></td></tr>
            <tr id="efmSijiKeikan_ptr2"><th>2</th><td>
                <a href="javascript:void(0)" class="block1" onclick="dfmSijiKeikan.changeKeikanPanel(2)" id="efmSijiKeikan_plink2" style="height:26px; padding-left:4px"></a></td></tr>
            <tr id="efmSijiKeikan_ptr3"><th>3</th><td>
                <a href="javascript:void(0)" class="block1" onclick="dfmSijiKeikan.changeKeikanPanel(3)" id="efmSijiKeikan_plink3" style="height:26px; padding-left:4px"></a></td></tr>
            <tr id="efmSijiKeikan_ptr4"><th>4</th><td>
                <a href="javascript:void(0)" class="block1" onclick="dfmSijiKeikan.changeKeikanPanel(4)" id="efmSijiKeikan_plink4" style="height:26px; padding-left:4px"></a></td></tr>
            <tr id="efmSijiKeikan_ptr5"><th>5</th><td>
                <a href="javascript:void(0)" class="block1" onclick="dfmSijiKeikan.changeKeikanPanel(5)" id="efmSijiKeikan_plink5" style="height:26px; padding-left:4px"></a></td></tr>
        </table>
        </div>

        <div id="efmSijiKeikan_when_container" style="display:none">
            <hr>
            <table cellspacing="0" cellpadding="0" style="width:auto"><tr>
            <td>拡張時間帯：</td>
            <td style="padding-left:6px">
                <table cellspacing="0" cellpadding="0" class="general_list" style="width:auto"><tr>
                    <td><button type="button" onclick="dfmSijiKeikan.showAssistWhen(4)" style="width:50px">候補</button><br><input
                        type="text" class="text" name="when4" style="width:40px; text-align:center" title="最大８文字" maxlength="8"
                        onkeyup="dfmSijiKeikan.changeWhen(4, value)" onchange="dfmSijiKeikan.changeWhen(4, value)" /></td>
                    <td><button type="button" onclick="dfmSijiKeikan.showAssistWhen(5)" style="width:50px">候補</button><br><input
                        type="text" class="text" name="when5" style="width:40px; text-align:center" title="最大８文字" maxlength="8"
                        onkeyup="dfmSijiKeikan.changeWhen(5, value)" onchange="dfmSijiKeikan.changeWhen(5, value)" /></td>
                    <td><button type="button" onclick="dfmSijiKeikan.showAssistWhen(6)" style="width:50px">候補</button><br><input
                        type="text" class="text" name="when6" style="width:40px; text-align:center" title="最大８文字" maxlength="8"
                        onkeyup="dfmSijiKeikan.changeWhen(6, value)" onchange="dfmSijiKeikan.changeWhen(6, value)" /></td>
                </tr></table>
            </td></tr></table>
        </div>
    </td>
    <td style="padding-left:10px; position:relative" id="efmSijiKeikan_content_container">
        <? for ($pidx=1; $pidx<=5; $pidx++) { ?>
        <div id="efmSijiKeikan_container<?=$pidx?>" style="z-index:<?=$pidx?>; display:none; visibility:hidden; position:absolute; background-color:#d7f0ff;">
            <div style="padding-bottom:4px">
                <table cellspacing="0" cellpadding="0" class="line_height26" style="width:auto"><tr>
                <th width="26" rowspan="2" style="background-color:#ace0ff"><?=$pidx?></th>
                <td style="padding-left:4px;">品名：</td>
                <td><select name="eiyouzai<?=$pidx?>" opt_group="eiyouzai" style="width:250px"
                    onchange="dfmSijiKeikan.eiyouzaiChanged(<?=$pidx?>)"></select></td>
                </tr>
                <tr>
                <td style="padding:3px 0 0 4px;">特記事項：</td>
                <td style="padding-top:3px"><input type="text" class="text" name="tokki<?=$pidx?>" style="width:250px"
                    title="最大１００文字" maxlength="100" /></td>
                </tr></table>
            </div>

            <table cellspacing="0" cellpadding="0" class="general_list" style="width:auto;"><tr>
                <th class="bottom">曜日</th><th class="bottom">朝</th><th class="bottom">昼</th><th class="bottom">夕</th>
                <th id="efmSijiKeikan_when<?=$pidx?>_4"></th>
                <th id="efmSijiKeikan_when<?=$pidx?>_5"></th>
                <th id="efmSijiKeikan_when<?=$pidx?>_6"></th>
                <th class="bottom">合計</th><th class="bottom">塩分追加</th></tr>

                <? for ($widx=1; $widx<=7; $widx++) { ?>
                <?     $ary = array("","月","火","水","木","金","土","日"); ?>
                <tr>
                <td><?=$ary[$widx]?></td>
                <td><input type="text" title="半角英数、最大４文字、単位：mL" onfocus="ipadHackFocusIn(this)" onkeyup="dfmSijiKeikan.calcEiyouzai(<?=$pidx?>, <?=$widx?>)" pattern="[0-9]*" class="text num3" name="ryou<?=$pidx?>_<?=$widx?>_1" maxlength="4" /></td>
                <td><input type="text" title="半角英数、最大４文字、単位：mL" onfocus="ipadHackFocusIn(this)" onkeyup="dfmSijiKeikan.calcEiyouzai(<?=$pidx?>, <?=$widx?>)" pattern="[0-9]*" class="text num3" name="ryou<?=$pidx?>_<?=$widx?>_2" maxlength="4" /></td>
                <td><input type="text" title="半角英数、最大４文字、単位：mL" onfocus="ipadHackFocusIn(this)" onkeyup="dfmSijiKeikan.calcEiyouzai(<?=$pidx?>, <?=$widx?>)" pattern="[0-9]*" class="text num3" name="ryou<?=$pidx?>_<?=$widx?>_3" maxlength="4" /></td>
                <td><input type="text" title="半角英数、最大４文字、単位：mL" onfocus="ipadHackFocusIn(this)" pattern="[0-9]*" onkeyup="dfmSijiKeikan.calcEiyouzai(<?=$pidx?>, <?=$widx?>)" class="text num3" name="ryou<?=$pidx?>_<?=$widx?>_4" maxlength="4" /></td>
                <td><input type="text" title="半角英数、最大４文字、単位：mL" onfocus="ipadHackFocusIn(this)" pattern="[0-9]*" onkeyup="dfmSijiKeikan.calcEiyouzai(<?=$pidx?>, <?=$widx?>)" class="text num3" name="ryou<?=$pidx?>_<?=$widx?>_5" maxlength="4" /></td>
                <td><input type="text" title="半角英数、最大４文字、単位：mL" onfocus="ipadHackFocusIn(this)" pattern="[0-9]*" onkeyup="dfmSijiKeikan.calcEiyouzai(<?=$pidx?>, <?=$widx?>)" class="text num3" name="ryou<?=$pidx?>_<?=$widx?>_6" maxlength="4" /></td>
                <td><input type="text" class="text_readonly num4" name="ryou<?=$pidx?>_<?=$widx?>_ttl" maxlength="5" /></td>
                <td><select name="enbun<?=$pidx?>_<?=$widx?>" opt_group="enbun" style="width:80px"></select></td>
                </tr>
                <? } ?>
            </table>
        </div><!-- // efmSijiKeikan_container -->
        <? } ?>
    </td>
    </tr></table>

    <hr>
    <div>備考<br/><textarea name="bikou" rows="3" cols="60"></textarea></div>
</div>
</form>
<div style="display:none" id="efmSijiKeikan_content_evacuate">
</div>
<script type="text/javascript">
var dfmSijiKeikan = {
    form: document.efmSijiKeikan,
    curPIdx: 0,
    popup : function(ymd){
        var self = this;
        var saveSijiKeikan = function(tryCommit) {
            if (!checkInputError("efmSijiKeikan")) return false;
            if (!tryCommit) tryCommit = "";
            var ret = false;
            var sData = serializeUnicodeEntity("efmSijiKeikan");
            var url = baseUrl+"&ajax_action=save_siji_ymd&ptif_id="+uEnc(ptif_id)+"&section=keikan&ymd="+ymd+"&try_commit="+tryCommit;
            $.ajax({ url:url, data:sData, success:function(msg) {
                if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
                if (!tryCommit) { ret = saveSijiKeikan(1); return; }
                dfmMainMenu.changeView("ondoban", ptif_id);
                ret = true;
            }});
            return ret;
        }
        var aYmd = ymdToArray(ymd);
        $("#efmSijiKeikan_content_evacuate").append($("#efmSijiKeikan_content")); // いったん退避
        $.ajax({ url:baseUrl+"&ajax_action=get_siji_ymd&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd+"&section=keikan", success:function(msg) {
            $('#efmSijiKeikan').dialog({
                title:"食事（経管）指示入力", width:820, height:500, extButtons:["save"],
                saveExec:function() { return saveSijiKeikan(); }
            });
            $("#efmSijiKeikan").append($("#efmSijiKeikan_content")); // ダイアログを開いてから中身をセット
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            obj["slash_mdw"] = "指示日："+aYmd.slash_mdw;
            bindDataToForm("efmSijiKeikan", obj);
            var fieldCount = getComboValue(self.form["keikan__field_count"]);
            self.fieldCountChanged(fieldCount);
            self.curPIdx = (fieldCount > 0 ? 1 : 0);
            self.changeKeikanPanel(self.curPIdx);
            for (var idx=1; idx<=5; idx++) {
                self.eiyouzaiChanged(idx);
            }
            for (var idx=4; idx<=6; idx++) {
                self.changeWhen(idx, str(obj["when"+idx]));
            }
        }});
    },
    showAssistWhen : function(targetIdx) {
        var self = this;
        var callbackFunc = function(opt_name) {
            var elem = getFormElementByName(self.form, "when"+targetIdx);
            elem.value = opt_name;
            self.changeWhen(targetIdx, opt_name);
        }
        dfmOptAssist.popup(dfmMstOptions.mstOptions["keikan_when"], callbackFunc, 300, 200);
    },
    changeWhen : function(idx, v) {
        for (var pidx=1; pidx<=5; pidx++) {
            ee("efmSijiKeikan_when"+pidx+"_"+idx).innerHTML = v;
        }
    },
    calcEiyouzai : function(pidx, widx) {
        var ttl = "";
        for (var tidx = 1; tidx<=6; tidx++) {
            var v = this.form["ryou"+pidx+"_"+widx+"_"+tidx].value;
            if (v=="")  continue;
            ttl = int(ttl) + int(v);
        }
        this.form["ryou"+pidx+"_"+widx+"_ttl"].value = ttl;
    },
    fieldCountChanged : function(size) {
        if (this.curPIdx>size) this.curPIdx=0;
        for (var pidx=1; pidx<=5; pidx++) {
            ee("efmSijiKeikan_ptr"+pidx).style.display = (pidx>size ? "none" : "");
            ee("efmSijiKeikan_container"+pidx).style.display = (pidx>size ? "none" : "");
            ee("efmSijiKeikan_container"+pidx).style.visibility = (this.curPIdx==pidx ? "visible" : "hidden");
        }
        ee("efmSijiKeikan_when_container").style.display = (this.curPIdx==0 ? "none": "");
        this.changeKeikanPanel(this.curPIdx);
    },
    changeKeikanPanel : function(pidx) {
        this.curPIdx = pidx;
        for (var idx=1; idx<=5; idx++) {
            ee("efmSijiKeikan_container"+idx).style.zIndex = (idx==pidx? 20 : idx);
            ee("efmSijiKeikan_plink"+idx).className = (idx==pidx ? "block1 a_selected" : "block1");
        }
        if (pidx>0) ee("efmSijiKeikan_container"+pidx).style.display = "";
        if (pidx>0) ee("efmSijiKeikan_container"+pidx).style.visibility = "visible";
        ee("efmSijiKeikan_when_container").style.display = (pidx==0 ? "none": "");
    },
    eiyouzaiChanged : function(pidx) {
        var cmb = this.form["eiyouzai"+pidx];
        var medName = cmb.options[cmb.selectedIndex].text;
        ee("efmSijiKeikan_plink"+pidx).innerHTML = (medName!="" ? medName : "（薬剤未指定）");
    }
}
</script>








<? //**************************************************************************************************************** ?>
<? // （Ｇ−３）                                                                                                      ?>
<? // 食事（経口）指示入力                                                                                      ?>
<? //**************************************************************************************************************** ?>
<form id="efmSijiKeikou">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="slash_mdw"></th><td></td>
    </tr></table></div>

    主食形態 <select name="syusyoku" opt_group="syusyoku"></select>
    副食形態 <select name="fukusyoku" opt_group="fukusyoku"></select>
    食種 <select name="syokusyu" opt_group="syokusyu"></select>
    <hr>
    <div>備考<br/><textarea name="bikou" rows="3" cols="60"></textarea></div>
</form>
<script type="text/javascript">
var dfmSijiKeikou = {
    popup : function(ymd){
        var saveSijiKeikou = function(tryCommit) {
            if (!checkInputError("efmSijiKeikou")) return false;
            if (!tryCommit) tryCommit = "";
            var ret = false;
            var sData = serializeUnicodeEntity("efmSijiKeikou");
            var url = baseUrl+"&ajax_action=save_siji_ymd&ptif_id="+uEnc(ptif_id)+"&section=keikou&ymd="+ymd+"&try_commit="+tryCommit;
            $.ajax({ url:url, data:sData, success:function(msg) {
                if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
                if (!tryCommit) { ret = saveSijiKeikou(1); return; }
                dfmMainMenu.changeView("ondoban", ptif_id);
                ret = true;
            }});
            return ret;
        }
        var aYmd = ymdToArray(ymd);
        $.ajax({ url:baseUrl+"&ajax_action=get_siji_ymd&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd+"&section=keikou", success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            obj["slash_mdw"] = "指示日："+aYmd.slash_mdw;
            bindDataToForm("efmSijiKeikou", obj);
            $('#efmSijiKeikou').dialog({ title:"食事（経口）指示入力", width:700, height:250, extButtons:["save"], saveExec:function() { return saveSijiKeikou(); } });
        }});
    }
}
</script>








<? //**************************************************************************************************************** ?>
<? // （Ｇ−４）                                                                                                      ?>
<? // 食事（補食）指示入力                                                                                      ?>
<? //**************************************************************************************************************** ?>
<form id="efmSijiKeikei">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="slash_mdw"></th><td></td>
    </tr></table></div>

    <hr>
    禁止食品（※アレルギーのみ）<input type="text" class="text" style="width:400px" name="kinsi_syokuhin" title="最大３００文字" maxlength="300" />
    <div style="padding:5px 0 8px 0"><label><input type="checkbox" class="checkbox" name="warfarin" value="1">ワーファリン対応</label></div>
    補食 <select name="oyatsu" opt_group="oyatsu"></select>
    <hr>
    <div>備考<br/><textarea name="bikou" rows="3" cols="60"></textarea></div>
</form>
<script type="text/javascript">
var dfmSijiKeiKei = {
    popup : function(ymd){
        var saveSijiKeikei = function(tryCommit) {
            if (!checkInputError("efmSijiKeiKei")) return false;
            if (!tryCommit) tryCommit = "";
            var ret = false;
            var sData = serializeUnicodeEntity("efmSijiKeikei");
            var url = baseUrl+"&ajax_action=save_siji_ymd&ptif_id="+uEnc(ptif_id)+"&section=keikei&ymd="+ymd+"&try_commit="+tryCommit;
            $.ajax({ url:url, data:sData, success:function(msg) {
                if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
                if (!tryCommit) { ret = saveSijiKeikei(1); return; }
                dfmMainMenu.changeView("ondoban", ptif_id);
                ret = true;
            }});
            return ret;
        }
        var aYmd = ymdToArray(ymd);
        $.ajax({ url:baseUrl+"&ajax_action=get_siji_ymd&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd+"&section=keikei", success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            obj["slash_mdw"] = "指示日："+aYmd.slash_mdw;
            bindDataToForm("efmSijiKeikei", obj);
            $('#efmSijiKeikei').dialog({ title:"食事（補食）指示入力", width:650, height:300, extButtons:["save"], saveExec:function() { return saveSijiKeikei(); } });
        }});
    }
}
</script>








<? //**************************************************************************************************************** ?>
<? // （Ｇ−５）                                                                                                      ?>
<? // インスリン固定投与指示入力                                                                                ?>
<? //**************************************************************************************************************** ?>
<form id="efmSijiInsulinFixTemplate">
    <table>
        <tbody class="repeat_object_template" namespace="insulin_fix" delete_type="elimination">
        <tr>
            <td><nobr><input type="text" class="text" basename="med_name" style="width:280px" /><button
                    type="button" onclick="dfmSijiInsulinFix.showAssistMedName(this)" class="w60">候補</button></nobr></td>
            <td><input type="text" title="半角数値のみ、小数１桁まで、最大６文字" onfocus="ipadHackFocusIn(this)" pattern="^[0-9]?[0-9]?[0-9]?([0-9][\.][0-9])?$" class="text num4" basename="asa" maxlength="6" /></td>
            <td><input type="text" title="半角数値のみ、小数１桁まで、最大６文字" onfocus="ipadHackFocusIn(this)" pattern="^[0-9]?[0-9]?[0-9]?([0-9][\.][0-9])?$" class="text num4" basename="hiru" maxlength="6" /></td>
            <td><input type="text" title="半角数値のみ、小数１桁まで、最大６文字" onfocus="ipadHackFocusIn(this)" pattern="^[0-9]?[0-9]?[0-9]?([0-9][\.][0-9])?$" class="text num4" basename="yu" maxlength="6" /></td>
            <td><input type="text" title="半角数値のみ、小数１桁まで、最大６文字" onfocus="ipadHackFocusIn(this)" pattern="^[0-9]?[0-9]?[0-9]?([0-9][\.][0-9])?$" class="text num4" basename="ryou4" maxlength="6" /></td>
            <td><input type="text" title="半角数値のみ、小数１桁まで、最大６文字" onfocus="ipadHackFocusIn(this)" pattern="^[0-9]?[0-9]?[0-9]?([0-9][\.][0-9])?$" class="text num4" basename="ryou5" maxlength="6" /></td>
            <td><input type="text" title="半角数値のみ、小数１桁まで、最大６文字" onfocus="ipadHackFocusIn(this)" pattern="^[0-9]?[0-9]?[0-9]?([0-9][\.][0-9])?$" class="text num4" basename="ryou6" maxlength="6" /></td>
            <td><input type="text" title="半角数値のみ、小数１桁まで、最大６文字" onfocus="ipadHackFocusIn(this)" pattern="^[0-9]?[0-9]?[0-9]?([0-9][\.][0-9])?$" class="text num4" basename="ryou7" maxlength="6" /></td>
        </tr>
        </tbody>
    </table>
</form>
<form id="efmSijiInsulinFix" name="efmSijiInsulinFix">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="slash_mdw"></th><td></td>
    </tr></table></div>

    <table cellspacing="0" cellpadding="0" width="100%"><tr>
    <td>投与 <select name="timing" opt_group="insulin_fix_timing"></select></td>
    <td class="right"><button type="button" onclick="dfmSijiInsulinFix.setDefaults()" navititle="規定薬剤" navi="「投与タイミング」「朝昼夕タイミング」以外をクリアしたのち、「薬剤」にヒューマリンとレベミルをセットます。">規定薬剤</button></td>
    </tr></table>
    <div style="padding-top:10px">
        <table cellspacing="0" cellpadding="0" class="general_list" style="width:auto">
            <tr><th style="vertical-align:bottom">薬剤</th>
                <th style="vertical-align:bottom">朝</th>
                <th style="vertical-align:bottom">昼</th>
                <th style="vertical-align:bottom">夕</th>
                <th><button type="button" onclick="dfmSijiInsulinFix.showAssistWhen(4)" style="width:50px">候補</button><br><input
                    type="text" class="text" name="when4" style="width:40px; text-align:center" title="最大１０文字" maxlength="10" /></th>
                <th><button type="button" onclick="dfmSijiInsulinFix.showAssistWhen(5)" style="width:50px">候補</button><br><input
                    type="text" class="text" name="when5" style="width:40px; text-align:center" title="最大１０文字" maxlength="10" /></th>
                <th><button type="button" onclick="dfmSijiInsulinFix.showAssistWhen(6)" style="width:50px">候補</button><br><input
                    type="text" class="text" name="when6" style="width:40px; text-align:center" title="最大１０文字" maxlength="10" /></th>
                <th><button type="button" onclick="dfmSijiInsulinFix.showAssistWhen(7)" style="width:50px">候補</button><br><input
                    type="text" class="text" name="when7" style="width:40px; text-align:center" title="最大１０文字" maxlength="10" /></th>
            </tr>
            <tbody class="repeat_object_container" namespace="insulin_fix"></tbody>
        </table>
    </div>
    <div style="padding-top:10px">
        <nobr><span style="padding-right:4px">備考</span><input type="text" class="text" name="insulin_fix_bikou" style="width:530px" /></nobr>
    </div>
</form>
<script type="text/javascript">
var dfmSijiInsulinFix = {
    sijibi : "",
    form : document.efmSijiInsulinFix,
    popup : function(ymd){
        var saveSijiInsulinFix = function(tryCommit) {
            if (!checkInputError("efmSijiInsulinFix")) return false;
            if (!tryCommit) tryCommit = "";
            var ret = false;
            var sData = serializeUnicodeEntity("efmSijiInsulinFix");
            var url = baseUrl+"&ajax_action=save_siji_ymd&ptif_id="+uEnc(ptif_id)+"&section=insulin_fix&ymd="+ymd+"&try_commit="+tryCommit;
            $.ajax({ url:url, data:sData, success:function(msg) {
                if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
                if (!tryCommit) { ret = saveSijiInsulinFix(1); return; }
                dfmMainMenu.changeView("ondoban", ptif_id);
                ret = true;
            }});
            return ret;
        }
        this.sijibi = ymd;
        $.ajax({ url:baseUrl+"&ajax_action=get_siji_ymd&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd+"&section=insulin_fix", success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            var aYmd = ymdToArray(ymd);
            obj["slash_mdw"] = "指示日："+aYmd.slash_mdw;
            obj["insulin_fix__field_count"] = 3;
            bindDataToForm("efmSijiInsulinFix", obj);
            $('#efmSijiInsulinFix').dialog({
                title:"インスリン固定投与指示入力", width:800, height:320, extButtons:["save"], saveExec:function() { return saveSijiInsulinFix(); }
            });
        }});
    },
    setDefaults : function() {
        var aYmd = ymdToArray(this.sijibi);
        var obj = {};
        obj["timing"] = getComboValue(efmSijiInsulinFix.timing);
        obj["slash_mdw"] = "指示日："+aYmd.slash_mdw;
        obj["insulin_fix__1__med_name"] = "ヒューマリン";
        obj["insulin_fix__2__med_name"] = "レベミル";
        obj["insulin_fix__field_count"] = 3;
        obj["when4"] = this.form["when4"].value;
        obj["when5"] = this.form["when5"].value;
        obj["when6"] = this.form["when6"].value;
        obj["when7"] = this.form["when7"].value;
        bindDataToForm("efmSijiInsulinFix", obj);
    },
    showAssistMedName : function(btn) {
        var self = this;
        var callbackFunc = function(opt_name) { $(btn).parent().find("input").val(opt_name); }
        dfmOptAssist.popup(dfmMstOptions.mstOptions["insulin_fix_med_name"], callbackFunc, 300, 150);
    },
    showAssistWhen : function(targetIdx) {
        var self = this;
        var elem = getFormElementByName(self.form, "when"+targetIdx);
        var callbackFunc = function(opt_name) { elem.value = opt_name; }
        dfmOptAssist.popup(dfmMstOptions.mstOptions["insulin_fix_when"], callbackFunc, 300, 150);
    }
}
</script>








<? //**************************************************************************************************************** ?>
<? // （Ｇ−６）                                                                                                      ?>
<? // BSスケール指示入力                                                                                              ?>
<? //**************************************************************************************************************** ?>
<form id="efmSijiBSScale" name="efmSijiBSScale">
    <input type="hidden" name="is_siji_stop" value="" />
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="slash_mdw"></th><td></td>
    </tr></table></div>

    <div id="efmSijiBSScale_bsscale_select_header">
        <div class="instruction" style="padding-bottom:10px">
            BSスケール指示は、登録済み院内共通パターン（O〜IV）を指定してください。<br>
            院内共通パターンに合致しない指示を行う場合は、「個別」を指定すると、<br>患者別で指示をカスタマイズできます。<br>
            「個別」で作成した指示は、次に指示入力を行う際に呼び出しやすいように、マスタ登録されます。<br>
            <span style="color:#ff8888">ラジオボタンを選択し、「保存」ボタンを押してください。</span>
        </div>
        <label style="padding-right:20px;"><input type="radio" class="radio" name="bs_type" value="" id="efmSijiBSScale_bs_type_radio"
            onclick="dfmSijiBSScale.refreshByMaster(this.value)">指示なし</label>
        <label style="padding-right:20px;"><input type="radio" class="radio" name="bs_type" value="BSSCALE_COMMON0"
            onclick="dfmSijiBSScale.refreshByMaster(this.value)">O</label>
        <label style="padding-right:20px;"><input type="radio" class="radio" name="bs_type" value="BSSCALE_COMMON1"
            onclick="dfmSijiBSScale.refreshByMaster(this.value)">I</label>
        <label style="padding-right:20px;"><input type="radio" class="radio" name="bs_type" value="BSSCALE_COMMON2"
            onclick="dfmSijiBSScale.refreshByMaster(this.value)">II</label>
        <label style="padding-right:20px;"><input type="radio" class="radio" name="bs_type" value="BSSCALE_COMMON3"
            onclick="dfmSijiBSScale.refreshByMaster(this.value)">III</label>
        <label style="padding-right:20px;"><input type="radio" class="radio" name="bs_type" value="BSSCALE_COMMON4"
            onclick="dfmSijiBSScale.refreshByMaster(this.value)">IV</label>
        <label style="padding-right:20px;"><input type="radio" class="radio" name="bs_type" value="PATIENT"
            onclick="dfmSijiBSScale.refreshByMaster(this.value)" id="efmSijiBSScale_bs_type_patient">個別</label>
        <button type="button" onclick="dfmSijiBSScale.showEditor()" id="efmSijiBSScale_editor_button" disabled>個別指定</button>
        <button type="button" onclick="dfmSijiBSScale.endSelector()" id="efmSijiBSScale_back_button">キャンセル</button>
    </div>

    <div id="efmSijiBSScale_bsscale_saved" style="padding-top:10px"></div>
    <div class="literal" name="ptif_name"></div>
    <div id="efmSijiBSScale_bsscale_container" style="padding-top:10px">
        <input type="hidden" name="bsscale__field_count" />
        <table cellspacing="0" cellpadding="0"><tr>
        <td>
            <table cellspacing="0" cellpadding="0" class="general_list" style="table-layout:fixed; width:auto">
                <tr><th style="width:80px">測定値</th><th style="width:330px">薬剤・量</th></tr>
                <? for ($idx=1; $idx<=6; $idx++) { ?>
                <tr id="efmSijiBSScale__scale<?=$idx?>">
                    <td class="literal center" name="bsscale__<?=$idx?>__bs_upper_disp"></td>
                    <td>
                        <input type="hidden" name="bsscale__<?=$idx?>__bs_upper" />
                        <input type="hidden" name="bsscale__<?=$idx?>__bs_name1" />
                        <input type="hidden" name="bsscale__<?=$idx?>__bs_ryou1" />
                        <input type="hidden" name="bsscale__<?=$idx?>__bs_name2" />
                        <input type="hidden" name="bsscale__<?=$idx?>__bs_ryou2" />
                        <table cellspacing="0" cellpadding="0" class="no_border cellspacing1">
                            <tr><td class="literal b_r b_b" name="bsscale__<?=$idx?>__bs_name1"></td>
                            <td class="literal b_b center" name="bsscale__<?=$idx?>__bs_ryou1" style="width:80px"></td></tr>
                            <tr><td class="literal b_r" name="bsscale__<?=$idx?>__bs_name2"></td>
                            <td class="literal center" name="bsscale__<?=$idx?>__bs_ryou2"></td></tr>
                        </table>
                    </td>
                </tr>
                <? } ?>
            </table>
        </td>
        <td style="padding-left:20px">
            <table cellspacing="0" cellpadding="0" class="general_list" style="table-layout:fixed; width:auto" id="efmSijiBSScale__right_table">
                <tr><th style="width:80px">測定値</th><th style="width:330px">薬剤・量</th></tr>
                <? for ($idx=7; $idx<=12; $idx++) { ?>
                <tr id="efmSijiBSScale__scale<?=$idx?>">
                    <td class="literal center" name="bsscale__<?=$idx?>__bs_upper_disp"></td>
                    <td>
                        <input type="hidden" name="bsscale__<?=$idx?>__bs_upper" />
                        <input type="hidden" name="bsscale__<?=$idx?>__bs_name1" />
                        <input type="hidden" name="bsscale__<?=$idx?>__bs_ryou1" />
                        <input type="hidden" name="bsscale__<?=$idx?>__bs_name2" />
                        <input type="hidden" name="bsscale__<?=$idx?>__bs_ryou2" />
                        <table cellspacing="0" cellpadding="0" class="no_border cellspacing1">
                            <tr><td class="literal b_r b_b" name="bsscale__<?=$idx?>__bs_name1"></td>
                            <td class="literal b_b center" name="bsscale__<?=$idx?>__bs_ryou1" style="width:80px"></td></tr>
                            <tr><td class="literal b_r" name="bsscale__<?=$idx?>__bs_name2"></td>
                            <td class="literal center" name="bsscale__<?=$idx?>__bs_ryou2"></td></tr>
                        </table>
                    </td>
                </tr>
                <? } ?>
            </table>
        </td></tr></table>
    </div>
    <div id="efmSijiBSScale_bsscale_noscale" class="instruction"
        style="display:none">プリセット登録はありません。<br>このまま保存すると「指示なし」となります。
    </div>
    <div style="padding-top:10px">
        <nobr><span style="padding-right:4px">備考</span><input type="text" class="text" id="efmSijiBSScale_bsscale_bikou" name="bsscale_bikou" style="width:530px" /></nobr>
    </div>
</form>
<script type="text/javascript">
var dfmSijiBSScale = {
    form :document.efmSijiBSScale,
    callbackFunc : 0,
    bs_list : [],
    slash_mdw : "",
    $dialogForm : 0,
    ymd:"",

    popup : function(ymd, callbackFunc){
        ee("efmSijiBSScale_editor_button").disabled = true;
        //this.form.is_siji_stop.value = "";
        this.callbackFunc = callbackFunc;
        this.ymd = ymd;
        var self = this;
        var saveSijiBSScale = function(tryCommit) {
            if (!checkInputError("efmSijiBSScale")) return false;
            if (!self.bs_list.length) {
                ee("efmSijiBSScale_bs_type_radio").checked = true;
                self.refreshByMaster("");
            }
            //self.form.is_siji_stop.value = "";
            //if (ee("efmSijiBSScale_bs_type_radio").checked) self.form.is_siji_stop.value = "1";

            if (!tryCommit) tryCommit = "";
            var ret = false;
            if (tryCommit && !confirm("保存します。よろしいですか？")) return ret;
            var sData = serializeUnicodeEntity("efmSijiBSScale");
            var url = baseUrl+"&ajax_action=save_siji_ymd&ptif_id="+uEnc(ptif_id)+"&try_commit="+tryCommit+"&section=bsscale&ymd="+ymd;
            $.ajax({ url:url, data:sData, success:function(msg) {
                if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
                if (!tryCommit) { ret = saveSijiBSScale(ptif_id, 1); return; }
                dfmMainMenu.changeView("ondoban", ptif_id);
                ret = true;
            }});
            return ret;
        }
        var aYmd = ymdToArray(ymd);
        this.slash_mdw = "指示日："+aYmd.slash_mdw;

        this.refreshByDB(ymd);
        this.$dialogForm = $('#efmSijiBSScale').dialog({ title:"BSスケール指示入力", width:900, height:540, extButtons:["save"],
            saveExec:function() { return saveSijiBSScale(); }
        });
    },
    startSelector : function() {
        this.refreshByMaster("");
        ee("efmSijiBSScale_bsscale_saved").style.display = "none";
        ee ("efmSijiBSScale_bsscale_select_header").style.display = "";
    },
    deleteSiji : function() {
        var self = this;
        if (!confirm("指示を削除します。よろしいですか？")) return;
        var url = baseUrl+"&ajax_action=delete_siji_ymd&ptif_id="+uEnc(ptif_id)+"&section=bsscale&ymd="+this.ymd;
        $.ajax({ url:url, success:function(msg) {
            if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
            dfmMainMenu.changeView("ondoban", ptif_id);
            self.$dialogForm.dialog("close");
        }});
    },
    endSelector : function() {
        this.refreshByDB(this.ymd);
    },
    refreshByDB : function(ymd) {
        var self = this;
        $.ajax({ url:baseUrl+"&ajax_action=get_siji_ymd&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd+"&section=bsscale", success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            obj["slash_mdw"] = self.slash_mdw;
            // BSスケールの指示が既に保存されている場合
            var bsTypeDisp = str(dfmMstBSScale.bsscale_captions[str(obj["bs_type"])]);
            if (bsTypeDisp) {
                ee("efmSijiBSScale_bsscale_saved").style.display = "";
                ee ("efmSijiBSScale_bsscale_select_header").style.display = "none";
                ee("efmSijiBSScale_bsscale_container").style.display = "";
                ee("efmSijiBSScale_back_button").style.display = "";
                ee("efmSijiBSScale_bsscale_noscale").style.display = "none";
                bsTypeDisp = "指定スケールタイプ：" + bsTypeDisp;
                ee("efmSijiBSScale_bsscale_saved").innerHTML =
                '<table cellspacing="0" cellpadding="0" width="100%"><tr><td>'+bsTypeDisp+'</td><td>'+
                //'<button type="button" onclick="dfmSijiBSScale.deleteSiji()" class="w60">削除</button>'+
                '<button type="button" onclick="dfmSijiBSScale.startSelector()" class="w60">変更</button></td></tr></table>';
                var field_count = obj["bsscale__field_count"];
                var lastUpper = 0;
                for (var idx=1; idx<=field_count; idx++) {
                    var prefix = "bsscale__"+idx+"__";
                    if (int(obj[prefix+"bs_upper"])>0) obj[prefix+"bs_upper_disp"] = "〜"+obj[prefix+"bs_upper"];
                    else if (lastUpper > 0 && idx==field_count) obj[prefix+"bs_upper_disp"] = (1+parseInt(lastUpper))+"〜";
                    lastUpper = obj[prefix+"bs_upper"];
                }
                for (var idx=1; idx<=12; idx++) {
                    ee("efmSijiBSScale__scale"+idx).style.display = (idx<=field_count ? "" : "none");
                }
                ee("efmSijiBSScale__right_table").style.display = (field_count>=7 ? "" : "none");
            }
            // BSスケール指示が未登録の場合
            else {
                ee("efmSijiBSScale_bsscale_saved").innerHTML = "";
                ee ("efmSijiBSScale_bsscale_select_header").style.display = "";
                ee("efmSijiBSScale_bsscale_container").style.display = "none";
                ee("efmSijiBSScale_back_button").style.display = "none";
            }
            bindDataToForm("efmSijiBSScale", obj);
        }});
    },
    refreshByMaster : function(bs_type) {
        ee("efmSijiBSScale_editor_button").disabled = (bs_type=="PATIENT" ? false : true);
        dfmMstBSScale.reloadMasterData(ptif_id);
        this.bs_list = dfmMstBSScale.mstBSScale[bs_type];
        if (!this.bs_list) this.bs_list = [];
        var obj = {};
        obj["bsscale_bikou"] = ee("efmSijiBSScale_bsscale_bikou").value;
        obj["bs_type"] = bs_type;
        obj["slash_mdw"] = this.slash_mdw;
        obj["bsscale__field_count"] = this.bs_list.length;
        if (this.bs_list && this.bs_list.length) {
            var lastUpper = 0;
            for (var idx=1; idx<=this.bs_list.length; idx++) {
                var prefix = "bsscale__"+idx+"__";
                obj[prefix+"bs_upper"] = this.bs_list[idx-1].bs_upper;
                obj[prefix+"bs_name1"] = this.bs_list[idx-1].bs_name1;
                obj[prefix+"bs_name2"] = this.bs_list[idx-1].bs_name2;
                obj[prefix+"bs_ryou1"] = this.bs_list[idx-1].bs_ryou1;
                obj[prefix+"bs_ryou2"] = this.bs_list[idx-1].bs_ryou2;
                if (int(obj[prefix+"bs_upper"])>0) obj[prefix+"bs_upper_disp"] = "〜"+obj[prefix+"bs_upper"];
                else if (idx==this.bs_list.length && lastUpper>0) obj[prefix+"bs_upper_disp"] = (1+parseInt(lastUpper,10)) + "〜";
                lastUpper = int(obj[prefix+"bs_upper"]);
            }
        }
        for (var idx=1; idx<=12; idx++) {
            ee("efmSijiBSScale__scale"+idx).style.display = (idx<=this.bs_list.length ? "" : "none");
        }
        ee("efmSijiBSScale__right_table").style.display = (this.bs_list.length>=7 ? "" : "none");
        bindDataToForm("efmSijiBSScale", obj);
        ee("efmSijiBSScale_bsscale_noscale").style.display = (this.bs_list.length || !bs_type ? "none" : "");
        ee("efmSijiBSScale_bsscale_container").style.display = (this.bs_list.length ? "" : "none");
    },
    showEditor : function() {
        var self = this;
        dfmMstBSScale.popup(
            'PATIENT',ptif_id, ptif_name,
            function() {
                ee("efmSijiBSScale_bs_type_patient").checked = true;
                self.refreshByMaster("PATIENT");
            }
        );
    }
}
</script>














<? //**************************************************************************************************************** ?>
<? // （Ｇ−６）                                                                                                      ?>
<? // BSスケール指示選択                                                                                        ?>
<? //**************************************************************************************************************** ?>
<form id="efmCareBSScaleTemplate">
    <table>
        <tbody class="repeat_object_template" namespace="bsscale" delete_type="elimination">
        <tr>
            <td class="literal center" basename="bs_upper_disp"></td>
            <td>
                <input type="hidden" basename="bs_upper" />
                <input type="hidden" basename="bs_name1" />
                <input type="hidden" basename="bs_ryou1" />
                <input type="hidden" basename="bs_name2" />
                <input type="hidden" basename="bs_ryou2" />
                <table cellspacing="0" cellpadding="0" class="no_border cellspacing1">
                    <tr><td class="literal b_r b_b" basename="bs_name1" style="height:22px"></td><td class="literal b_b center" basename="bs_ryou1"></td></tr>
                    <tr><td class="literal b_r" basename="bs_name2" style="height:22px"></td><td class="literal center" basename="bs_ryou2"></td></tr>
                </table>
            </td>
            <td class="literal" basename="reference_button">
                <button type="button" onclick="dfmCareBSScale.reference('+idx+')" class="w60">選択</button>
            </td>
        </tr>
        </tbody>
    </table>
</form>
<form id="efmCareBSScale" name="efmCareBSScale">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="slash_mdw"></th><td></td>
    </tr></table></div>

    <div id="efmCareBSScale_bsscale_saved"></div>
    <div class="literal" name="ptif_name"></div>
    <div id="efmCareBSScale_bsscale_container">
        <table cellspacing="0" cellpadding="0" class="general_list" style="table-layout:fixed; width:auto">
            <tr><th style="width:120px">測定値</th><th style="width:350px">薬剤・量</th><th style="width:60px"></th></tr>
            <tbody class="repeat_object_container" namespace="bsscale"></tbody>
        </table>
    </div>
    <div id="efmCareBSScale_bsscale_noscale" class="instruction" style="display:none">プリセット登録はありません。</div>
</form>
<script type="text/javascript">
var dfmCareBSScale = {
    callbackFunc : 0,
    $dialogForm : 0,
    bs_list : [],
    slash_mdw : "",
    ymd:"",
    popup : function(ymd, callbackFunc){
        this.dbObj = {};
        this.callbackFunc = callbackFunc;
        this.ymd = ymd;
        var self = this;
        var aYmd = ymdToArray(ymd);
        this.slash_mdw = "指示日："+aYmd.slash_mdw;
        this.refreshByDB(ymd);
        this.$dialogForm = $('#efmCareBSScale').dialog({ title:"BSスケール指示選択", width:650, height:480 });
    },
    refreshByDB : function(ymd) {
        var self = this;
        var url = baseUrl+"&ajax_action=get_siji_ymd&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd+"&section=bsscale&is_get_last_siji=1";
        $.ajax({ url:url, success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            obj["slash_mdw"] = self.slash_mdw;
            // BSスケールの指示が既に保存されている場合
            var bsTypeDisp = str(dfmMstBSScale.bsscale_captions[str(obj["bs_type"])]);
            if (bsTypeDisp) {
                ee("efmCareBSScale_bsscale_saved").style.display = "";
                ee("efmCareBSScale_bsscale_container").style.display = "";
                bsTypeDisp = "指定スケールタイプ：" + bsTypeDisp;
                if (obj["siji_ymd"]!=ymd) {
                    var aYmd = ymdToArray(obj["siji_ymd"]);
                    bsTypeDisp += '<span style="padding-left:20px; color:#ff0000">スケール指示入力日:'+aYmd["slash_ymd"]+'</span>';
                }
                ee("efmCareBSScale_bsscale_saved").innerHTML = '<table cellspacing="0" cellpadding="0" width="100%"><tr><td>'+bsTypeDisp+'</td></tr></table>';
            }
            // BSスケール指示が未登録の場合
            else {
                ee("efmCareBSScale_bsscale_saved").innerHTML = '<span class="instruction">指示はありません。</span>';
                ee("efmCareBSScale_bsscale_container").style.display = "none";
            }
            for (var idx=1; idx<=obj["bsscale__field_count"]; idx++) {
                var prefix = "bsscale__"+idx+"__";
                obj[prefix+"reference_button"] = ('<button type="button" onclick="dfmCareBSScale.reference('+idx+')" class="w60">選択</button>');
                if (obj[prefix+"bs_upper"]!="") obj[prefix+"bs_upper_disp"] = "〜"+obj[prefix+"bs_upper"];
            }
            bindDataToForm("efmCareBSScale", obj);
            self.dbObj = obj;
        }});
    },
    reference : function(idx) {
        var prefix = "bsscale__"+idx+"__";
        this.callbackFunc(
            str(this.dbObj[prefix+"bs_upper"]),
            str(this.dbObj[prefix+"bs_name1"]),
            str(this.dbObj[prefix+"bs_ryou1"]),
            str(this.dbObj[prefix+"bs_name2"]),
            str(this.dbObj[prefix+"bs_ryou2"])
        );
        if (this.$dialogForm) this.$dialogForm.dialog("close");
        this.$dialogForm = 0;
    }
}
</script>







<? //**************************************************************************************************************** ?>
<? // （Ｇ−７）                                                                                                      ?>
<? // インスリン食事指示量入力                                                                                  ?>
<? //**************************************************************************************************************** ?>
<form id="efmSijiInsulinSRTemplate">
    <table>
        <tbody class="repeat_object_template" namespace="insulin_sr" delete_type="elimination">
        <tr>
            <td><nobr><input type="text" class="text" basename="ryou" style="width:180px" title="最大１００文字" maxlength="100" /><button
                    type="button" onclick="dfmSijiInsulinSR.showAssist(this)" class="w60">候補</button></nobr>
            </td>
            <td><input type="text" title="半角数値のみ、小数１桁まで、最大６文字" onfocus="ipadHackFocusIn(this)" pattern="^[0-9]?[0-9]?[0-9]?([0-9][\.][0-9])?$" class="text num4" basename="asa" maxlength="6" /></td>
            <td><input type="text" title="半角数値のみ、小数１桁まで、最大６文字" onfocus="ipadHackFocusIn(this)" pattern="^[0-9]?[0-9]?[0-9]?([0-9][\.][0-9])?$" class="text num4" basename="hiru" maxlength="6" /></td>
            <td><input type="text" title="半角数値のみ、小数１桁まで、最大６文字" onfocus="ipadHackFocusIn(this)" pattern="^[0-9]?[0-9]?[0-9]?([0-9][\.][0-9])?$" class="text num4" basename="yu" maxlength="6" /></td>
            <td><button type="button" onclick="{TOGGLE_EVENT}" class="w60">削除</button></td>
        </tr>
        </tbody>
    </table>
</form>
<form id="efmSijiInsulinSR" name="efmSijiInsulinSR">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="slash_mdw"></th><td></td>
    </tr></table></div>

    薬剤名 <input type="text" class="text" name="med_name" style="width:300px" title="最大１００文字" maxlength="100" /><button
        type="button" onclick="dfmSijiInsulinSR.showAssist('')" class="w60">候補</button>
    <div style="padding-top:10px">
        <table cellspacing="0" cellpadding="0" style="width:100%"><tr>
        <td><button type="button" onclick="repeatObjectFieldAdd('efmSijiInsulinSR','insulin_sr')" class="w60">追加</button></td>
        <td class="right"><button type="button" onclick="dfmSijiInsulinSR.setDefault()">規定状態</button></td>
        </tr></table>

        <table cellspacing="0" cellpadding="0" class="general_list" style="width:auto">
        <tr><th>摂取量</th><th>朝</th><th>昼</th><th>夕</th><th></th></tr>
        <tbody class="repeat_object_container" namespace="insulin_sr"></tbody>
        </table>
    </div>
    <div style="padding-top:12px">
        <nobr><span style="padding-right:4px">備考</span><input type="text" class="text" name="insulin_sr_bikou" style="width:530px" /></nobr>
    </div>
</form>
<script type="text/javascript">
var dfmSijiInsulinSR = {
    dispObj : {},
    popup : function(ymd){
        var self = this;
        var saveSijiInsulinSR = function(tryCommit) {
            if (!tryCommit) tryCommit = "";
            if (!checkInputError("efmSijiInsulinSR")) return false;
            var ret = false;
            if (tryCommit && !confirm("保存します。よろしいですか？")) return ret;
            var sData = serializeUnicodeEntity("efmSijiInsulinSR");
            var url = baseUrl+"&ajax_action=save_siji_ymd&ptif_id="+uEnc(ptif_id)+"&try_commit="+tryCommit+"&section=insulin_sr&ymd="+ymd;
            $.ajax({ url:url, data:sData, success:function(msg) {
                if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
                if (!tryCommit) { ret = saveSijiInsulinSR(ptif_id, 1); return; }
                dfmMainMenu.changeView("ondoban", ptif_id);
                ret = true;
            }});
            return ret;
        }
        var aYmd = ymdToArray(ymd);
        $.ajax({ url:baseUrl+"&ajax_action=get_siji_ymd&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd+"&section=insulin_sr", success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            obj["slash_mdw"] = "指示日："+aYmd.slash_mdw;
            bindDataToForm("efmSijiInsulinSR", obj);
            self.dispObj = obj;
            $('#efmSijiInsulinSR').dialog({ title:'インスリン食事指示量', width:600, height:400, extButtons:["up","down","save"],
                saveExec:function() { return saveSijiInsulinSR(); }
            });
        }});
    },
    setDefault : function() {
        var nobj = {};
        nobj["slash_mdw"] = this.dispObj["slash_mdw"];
        nobj["ptif_name_disp"] = this.dispObj["ptif_name_disp"];
        nobj["insulin_sr__field_count"] = 3;
        nobj["insulin_sr__1__ryou"] = "全量";
        nobj["insulin_sr__2__ryou"] = "半量";
        bindDataToForm("efmSijiInsulinSR", nobj);
    },
    showAssist : function(btn) {
        if (!btn) {
            var callbackFunc = function(opt_name) { document.efmSijiInsulinSR.med_name.value = opt_name; }
            dfmOptAssist.popup(dfmMstOptions.mstOptions["insulin_bs"], callbackFunc, 500, 300);
            return;
        }

        var callbackFunc = function(opt_name) { $(btn).parent().find("input").val(opt_name); }
        dfmOptAssist.popup(dfmMstOptions.mstOptions["sessyu_ryou"], callbackFunc, 200, 150);
    }
}
</script>











<? //**************************************************************************************************************** ?>
<? // （Ｇ−８）                                                                                                      ?>
<? // 下剤指示入力                                                                                                    ?>
<? //**************************************************************************************************************** ?>
<form id="efmSijiGezaiTemplate">
    <table>
        <tbody class="repeat_object_template" namespace="gezai" delete_type="elimination">
        <tr>
        <td><select basename="med_name" opt_group="gezai"></select></td>
        <td><input type="text" class="text" basename="text" title="最大１００文字" maxlength="100" style="width:100px" /></td>
        <td><button type="button" onclick="{TOGGLE_EVENT}" class="w60">削除</button></td>
        </tr>
        </tbody>
    </table>
</form>
<form id="efmSijiGezai" name="efmSijiGezai">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="slash_mdw"></th><td></td>
    </tr></table></div>

    <div><button type="button" onclick="repeatObjectFieldAdd('efmSijiGezai','gezai')" class="w60">追加</button></div>
    <table cellspacing="0" cellpadding="0" class="general_list" style="width:auto">
        <tr><th>薬剤名</th><th>日数</th><th></th></tr>
        <tbody class="repeat_object_container" namespace="gezai"></tbody>
    </table>


    <div style="padding-top:12px">
        <nobr><span style="padding-right:4px">備考</span><input type="text" class="text" name="gezai_bikou" style="width:530px" /></nobr>
    </div>
</form>
<script type="text/javascript">
var dfmSijiGezai = {
    form : document.efmSijiGezai,
    popup : function(ymd){
        var self = this;
        var saveSijiGezai = function(tryCommit) {
            if (!checkInputError("efmSijiGezai")) return false;
            if (!tryCommit) tryCommit = "";
            var ret = false;
            var sData = serializeUnicodeEntity("efmSijiGezai");
            var url = baseUrl+"&ajax_action=save_siji_ymd&ptif_id="+uEnc(ptif_id)+"&section=gezai&ymd="+ymd+"&try_commit="+tryCommit;
            $.ajax({ url:url, data:sData, success:function(msg) {
                if (getCustomTrueAjaxResult(msg)==AJAX_ERROR) return;
                if (!tryCommit) { ret = saveSijiGezai(1); return; }
                dfmMainMenu.changeView("ondoban", ptif_id);
                ret = true;
            }});
            return ret;
        }
        var aYmd = ymdToArray(ymd);
        $.ajax({ url:baseUrl+"&ajax_action=get_siji_ymd&ptif_id="+uEnc(ptif_id)+"&ymd="+ymd+"&section=gezai", success:function(msg) {
            var obj = getCustomTrueAjaxResult(msg, TYPE_JSON); if(obj==AJAX_ERROR) return;
            obj["slash_mdw"] = "指示日："+aYmd.slash_mdw;
            bindDataToForm("efmSijiGezai", obj);
            $('#efmSijiGezai').dialog({
                title:"下剤指示入力", width:600, height:400, extButtons:["up","down","save"], saveExec:function(){ return saveSijiGezai(); }
            });
        }});
    }
}
</script>










<? //**************************************************************************************************************** ?>
<? // （Ｈ−１）                                                                                                      ?>
<? // 記入者履歴                                                                                                      ?>
<? //**************************************************************************************************************** ?>
<form id="efmUpdateHistory" name="efmUpdateHistory" navititle="ケア記入者履歴" navi="温度板ケア入力を行ったログイン職員を表示します。<br>基本的にバイタル入力・離床理由ダイアログを含む、ケア入力ダイアログ単位で、行登録が行われます。<br>◆指示ダイアログである「注射処方等指示」ダイアログは、記録されません。<br>◆ケア入力（酸素）ダイアログでは、「酸素吸入」「人工呼吸器」に分かれて記録されます。<br>ケア記入者は「最終確認者」を兼ねています。ダイアログ内容を無変更のまま「保存」を押しても登録されます。<br>操作列は「新規」「更新」「削除」のいずかが表示されます。うち「削除」については、以下の場合にのみ表示されます。<br>◆「離床理由」を削除した場合<br>◆酸素/抗生剤ダイアログで、すべての保存済みデータを削除した場合<br>「酸素吸入」「人工呼吸器」「抗生剤」については、指示日ではなく、「期間開始日」と「期間終了日」に対し、登録されます。<br>期間を変更した場合は、変更前と変更後の日付に対し登録されます。このため最大４日間に渡り、登録されることになります。<br>この記入者履歴データは、削除することはできません。">
    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name" name="ptif_name_disp"></th><th class="literal sijibi" name="sijibi"></th><td></td>
    </tr></table></div>
    <div id="efmUpdateHistory_list_container"></div>
</form>
<script type="text/javascript">
var dfmUpdateHistory = {
    popup : function(ymd){
        $.ajax({ url:baseUrl+"&ajax_action=get_update_history&ptif_id="+ptif_id+"&ymd="+ymd, success:function(msg) {
            var html = getCustomTrueAjaxResult(msg); if(html==AJAX_ERROR) return;
            ee("efmUpdateHistory_list_container").innerHTML =
            '<table cellspacing="0" cellpadding="0" class="general_list" style="background-color:#ffffff">' +
            '<tr><th width="180">更新日</th><th width="90">更新対象</th><th width="auto">職員名</th><th width="50">操作</th></tr>'+
            html+'</table>';
            var obj = {};
            var aYmd = ymdToArray(ymd);
            obj["sijibi"] = "指示日："+aYmd.slash_mdw;
            bindDataToForm("efmUpdateHistory", obj); // 指示日と患者名をバインドするためのみに呼ぶ。
            $('#efmUpdateHistory').dialog({ title:'ケア記入者履歴', width:600, height:600, extButtons:["up","down"] });
        }});
    }
}
</script>











<? //**************************************************************************************************************** ?>
<? // （Ｈ−２）                                                                                                      ?>
<? // 日付選択ダイアログ。コンテンツと制御はcommon.js参照                                                             ?>
<? //**************************************************************************************************************** ?>
<form id="efmYmdAssist" name="efmYmdAssist"></form>










<? //**************************************************************************************************************** ?>
<? // （Ｈ−３）                                                                                                      ?>
<? // 時刻指定ダイアログ                                                                                              ?>
<? //**************************************************************************************************************** ?>
<form id="efmHMAssist" name="efmHMAssist">
    <div style="padding-top:10px">
        <button type="button" onclick="dfmHMAssist.selectHM('now')">現在時刻 (@)</button>
        <span style="padding-left:30px; padding-top:4px"><button type="button" onclick="dfmHMAssist.selectHM('clear')">クリア (-)</button></span>
    </div>
    <hr>
    <div>
        <table cellspacing="0" cellpadding="0"><tr><td>時刻：<input type="text" title="半角数値のみ、最大４文字" pattern="[0-9]*" class="text num4"
        onkeyup="if (!isEnterKey(this, event)) dfmHMAssist.showInputTimeJp(this.value);"
        onkeydown="
            if (isKeyboradKey(this, event, 13)) { dfmHMAssist.selectHM('',this.value); return stopEvent(event); }
            if (isKeyboradKey(this, event, 64) || isKeyboradKey(this, event, 192)) { dfmHMAssist.selectHM('now'); return stopEvent(event); }
            if (isKeyboradKey(this, event, 109) || isKeyboradKey(this, event, 173) || isKeyboradKey(this, event, 189)) {
                dfmHMAssist.selectHM('clear'); return stopEvent(event);
            }
        "
        id="efmHMAssist_hm" maxlength="4" /><span id="efmHMAssist_msg" style="padding-left:4px"></span></td>
        <td style="text-align:right">
            <button type="button" onclick="dfmHMAssist.selectHM('',ee('efmHMAssist_hm').value);">決定 (Enter)</button>
        </td></tr></table>
    </div>
    <div class="instruction" style="padding-top:10px">
        ※数値3桁または4桁で指定してください。<br>
        ※0時00分から35時59分までが有効範囲です。<br>
        <div style="padding-top:6px">例）</div>
        <div style="padding-left:16px">
            <table cellspacing="0" cellpadding="0" style="width:auto">
            <tr><td><em>12</em>時<em>55</em>分</td><td style="padding:0 10px">⇒</td><td><em>1255</em></td></tr>
            <tr><td><em>8</em>時<em>5</em>分</td><td style="padding:0 10px">⇒</td>
                <td><em>805</em><span style="padding:0 4px">または</span><em>0805</em></td></tr>
            </table>
        </div>
    </div>
</form>
<script type="text/javascript">
var dfmHMAssist = {
    callbackFunc : 0,
    $dialogForm : 0,
    popup : function(currentHM, callbackFunc){
        this.callbackFunc = callbackFunc;
        ee("efmHMAssist_hm").value = currentHM;
        this.$dialogForm = $('#efmHMAssist').dialog({ title:'時刻指定', width:480, height:250 });
        this.showInputTimeJp(currentHM);
        setTimeout(function(){ tryFocus("efmHMAssist_hm"); }, 100);
    },
    execError : function(err) {
        ee("efmHMAssist_msg").innerHTML = '<span style="color:#ff0000;">'+err+'</span>';
        setTimeout(function(){ tryFocus("efmHMAssist_hm"); }, 100);
        return false;
    },
    showInputTimeJp : function(hm) {
        ee("efmHMAssist_msg").innerHTML = "";
        if (!hm.match("^[0-9][0-9][0-9][0-9]?$")) return;
        if (hm.length==3) hm = "0" + hm;
        var hh = hm.substring(0,2);
        var mi = hm.substring(2);
        if (int(hh)>35) return;
        if (int(mi)>59) return;
        if (int(hh)<24) ee("efmHMAssist_msg").innerHTML = "⇒ <em>"+int(hh)+"</em>時<em>"+mi+"</em>分";
        else ee("efmHMAssist_msg").innerHTML = "⇒ (翌日) <em>"+(hh-24)+"</em>時<em>"+mi+"</em>分";
    },
    selectHM : function(mode, hm) {
        if (mode=="now") {
            var now = new Date();
            var hh = ("0"+now.getHours()).slice(-2);
            var mi = ("0"+now.getMinutes()).slice(-2);
            this.callbackFunc(hh+mi, hh+":"+mi);
        }
        else if (mode=="clear") {
            this.callbackFunc("", ":");
        } else {
            if (!hm.match("^[0-9][0-9][0-9][0-9]?$")) return this.execError("3桁〜4桁の数値を指定してください。");
            if (hm.length==3) hm = "0" + hm;
            var hh = hm.substring(0,2);
            var mi = hm.substring(2);
            if (int(hh)>35) return this.execError("時刻「"+hh+"」は範囲外です。");
            if (int(mi)>59) return this.execError("分数「"+mi+"」は範囲外です。");
            this.callbackFunc(hm, hh+":"+mi);
        }
        this.$dialogForm.dialog("close");
        this.$dialogForm = 0;
    }
}
</script>










<? //**************************************************************************************************************** ?>
<? // （Ｈ−４）                                                                                                      ?>
<? // 候補ダイアログ                                                                                                  ?>
<? //**************************************************************************************************************** ?>
<form id="efmOptAssist">
    <div style="padding-top:10px">
    <div id="efmOptAssist_selector_container"></div>
    </div>
    <table cellspacing="0" cellpadding="0"><tr>
        <td><button type="button" onclick="dfmOptAssist.settle('clear')" style="width:80px">クリア</button></td>
        <td class="right"><button type="button" onclick="dfmOptAssist.settle('ok')" style="width:80px" id="efmOptAssist_btn_ok">決定</button></td>
    </tr></table>
</form>
<script type="text/javascript">
var dfmOptAssist = {
    callbackFunc : "",
    $dialogForm : 0,
    popup : function(list, callbackFunc, nWidth, nHeight){
        ee("efmOptAssist_btn_ok").disabled = true;
        this.callbackFunc = callbackFunc;
        if (!nWidth) nWidth = 200;
        if (!nHeight) nHeight = 400;
        if (!list || !list.length) return alert("選択肢はありません。");
        var html = [];
        html.push('<select size="'+int((nHeight-50)/26)+'" style="width:100%" id="efmOptAssist_selector"');
        html.push(' onclick="dfmOptAssist.settle(\'ok\');" onchange="dfmOptAssist.selectorChanged()"');
        html.push(' onkeydown="if (isEnterKey(this,event)) { dfmOptAssist.settle(\'ok\'); return false; }"');
        html.push('>');
        for (var idx=0; idx<list.length; idx++) {
            html.push('<option value="'+htmlEscape(list[idx].opt_name)+'">'+htmlEscape(list[idx].opt_name)+'</option>');
        }
        html.push('</select>');
        ee("efmOptAssist_selector_container").innerHTML = html.join("");
        this.$dialogForm = $('#efmOptAssist').dialog({ title:'候補', width:nWidth, height:nHeight });
        tryFocus("efmOptAssist_selector");
        ee("efmOptAssist_selector").selectedIndex = -1;
    },
    selectorChanged : function() {
        ee("efmOptAssist_btn_ok").disabled = false;
    },
    settle : function(which) {
        var obj = ee("efmOptAssist_selector");
        var v = (which=="ok" ? getComboValue(ee("efmOptAssist_selector")) : "");
        this.callbackFunc(v);
        this.$dialogForm.dialog("close");
    }
}
</script>











</div>

</body>
</html>
<?
pg_close($con);
