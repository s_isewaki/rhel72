<?php
    define("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING", 1);
    require_once("ks_common.php");
    require_once("ks_ajax.php");

    $ptif_id = $_REQUEST["ptif_id"];
    $ptif_name = get_ptif_name($ptif_id);
    $ymd = $_REQUEST["ymd"];
    $a_ymd = ymdhm_to_array($ymd);
    $image_ymdhmd = $_REQUEST["image_ymdhmd"];
    $field_number = $_REQUEST["field_number"];
    $file_base_url = GG_PICTURE_FOLDER."/".$ptif_id."/";
    $file_dir = GG_BASE_FOLDER."/".$file_base_url;

    $serial_data = dbGetOne("select serial_data_chusya from sum_ks_siji_ymd where ptif_id = ".dbES($ptif_id)." and ymd = ".dbES($ymd));
    $data = unserialize($serial_data);
    $field_count = (int)$data["chusya__field_count"];
    if (!$field_number) $field_number = $field_count + 1;
    $siji_picture_names = $data["chusya__".$field_number."__siji_picture_names"];

    $picture_m_html = create_picture_img_tag($ptif_id, $siji_picture_names, "m");
    $picture_s_html = create_picture_img_tag($ptif_id, $siji_picture_names, "s");

    if ($_REQUEST["del_file_id"]) {
        $del_file_id = $_REQUEST["del_file_id"];
        $file_pattern = $file_dir.$del_file_id."_*";
        foreach (glob($file_pattern) as $filename) unlink($filename);

        $ary = explode(",", $siji_picture_names);
        $new_ary = array();
        foreach ($ary as $fname) {
            if (substr($fname, 0,  23) != $del_file_id) $new_ary[] = $fname;
        }

        $data["chusya__".$field_number."__siji_picture_names"] = implode(",",$new_ary);
        $serial_data = serialize($data);
        dbExec("update sum_ks_siji_ymd set serial_data_chusya = ".dbES($serial_data)." where ptif_id = ".dbES($ptif_id)." and ymd = ".dbES($ymd));
        header("Location: ks_picture.php?session=".GG_SESSION_ID."&ptif_id=".$ptif_id."&ymd=".$ymd."&field_number=".$field_number."&upload_status=ok");
        break;
    }




    $err = "";
    if ($_REQUEST["try_upload"]) {
        for (;;) {
            $tmp_name = @$_FILES["siji_picture"]["tmp_name"];
            if (!$tmp_name) { $err = "ファイルを指定してください。"; break; }
            $size = @$_FILES["siji_picture"]["size"];
            if (!$size) { $err = "空ファイルを指定したか、ファイルが不正です。"; break; }

            $fp = fopen($tmp_name, r);
            $binary = fread($fp, 10);
            fclose($fp);
            $file_ext = "";
            if (preg_match('/^\x89PNG\x0d\x0a\x1a\x0a/', $binary)) $file_ext = "png";
            elseif (preg_match('/^GIF8[79]a/', $binary) ) $file_ext = "gif";
            elseif (preg_match('/^\xff\xd8/', $binary) ) $file_ext = "jpg";
            if (!$file_ext) { $err = "画像ファイルでないか、サポート外のファイルです。"; break; }

            $file_base = $ymd."_".date("YmdHis");
            $file_name = $file_base."_o.".$file_ext;
            $thumb_m_name = $file_base."_m.".$file_ext;
            $thumb_s_name = $file_base."_s.".$file_ext;
            $file_path = $file_dir.$file_name;
            $file_pattern = $file_dir.$file_base."_*";

            if (!is_dir($file_dir)) mkdir($file_dir, 0777);

            foreach (glob($file_pattern) as $filename) unlink($filename);

            if (!@move_uploaded_file($tmp_name, $file_path)) {
                $err = "アップロードファイルのディスク書込みに失敗しました。(".$file_path.")";
                break;
            }
            chmod($file_path, 0666);

            $image = 0;
            if ($file_ext=="jpg") $image = ImageCreateFromJPEG($file_path); //JPEGファイルを読み込む
            if ($file_ext=="gif") $image = ImageCreateFromGIF($file_path); //GIFファイルを読み込む
            if ($file_ext=="png") $image = ImageCreateFromPNG($file_path); //PNGファイルを読み込む
            if (!$image) { $err = "画像ファイルのサムネイル作成に失敗しました。"; break; }
            $ow = imagesx($image); //横幅（ピクセル）
            $oh = imagesy($image); //縦幅（ピクセル）

            $rate_s = ($ow > $oh ? 120/$ow : 120/$oh); // 圧縮比。長辺120pxとする。
            $nw = $rate_s * $ow;
            $nh = $rate_s * $oh;
            $new_image = ImageCreateTrueColor($nw, $nh);
            ImageCopyResampled($new_image, $image, 0,0,0,0, $nw, $nh, $ow, $oh);
            if ($file_ext=="jpg") ImageJPEG($new_image, $file_dir.$thumb_m_name, 100);
            if ($file_ext=="gif") ImageGIF($new_image, $file_dir.$thumb_m_name);
            if ($file_ext=="png") ImagePNG($new_image, $file_dir.$thumb_m_name);
            chmod($file_dir.$thumb_m_name, 0666);

            $nw = $nw / 4; // 長辺30pxとする
            $nh = $nh / 4;
            $new_image = ImageCreateTrueColor($nw, $nh);
            ImageCopyResampled($new_image, $image, 0,0,0,0, $nw, $nh, $ow, $oh);
            if ($file_ext=="jpg") ImageJPEG($new_image, $file_dir.$thumb_s_name, 100);
            if ($file_ext=="gif") ImageGIF($new_image, $file_dir.$thumb_s_name);
            if ($file_ext=="png") ImagePNG($new_image, $file_dir.$thumb_s_name);
            chmod($file_dir.$thumb_s_name, 0666);

            if (!dbGetOne("select count(*) from sum_ks_siji_ymd where ptif_id = ".dbES($ptif_id)." and ymd = ".dbES($ymd))) {
                dbExec("insert into sum_ks_siji_ymd (ptif_id, ymd) values (".dbES($ptif_id).",".dbES($ymd).")");
            }
            $siji_picture_names .= ($siji_picture_names ? "," : "") . $file_name;

            $data["chusya__".$field_number."__siji_picture_names"] = $siji_picture_names;
            if ($field_count < $field_number) $data["chusya__field_count"] = $field_number;
            $serial_data = serialize($data);
            dbExec("update sum_ks_siji_ymd set serial_data_chusya = ".dbES($serial_data)." where ptif_id = ".dbES($ptif_id)." and ymd = ".dbES($ymd));
            header("Location: ks_picture.php?session=".GG_SESSION_ID."&ptif_id=".$ptif_id."&ymd=".$ymd."&field_number=".$field_number."&upload_status=ok");
            break;
        }
    }

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/start/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" type="text/css" href="css/common.css">
<!--[if IE]><script type="text/javascript" src="../excanvas_r3/excanvas.compiled.js"></script><![endif]-->
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/custom_graph.js"></script>
<title>CoMedix | 看護支援 - 画像管理</title>


<? if ($_REQUEST["upload_status"]=="ok") { ?>
    <script type="text/javascript">
        parent.dfmSijiban.puctureUploadCallback(<?=$field_number?>,"<?=js($siji_picture_names)?>", "<?=js($picture_s_html)?>");
    </script>
<? } ?>

<script type="text/javascript">
    function delFile(fid) {
        if (!confirm("削除します。よろしいですか？")) return;
        location.href="ks_picture.php?session=<?=GG_SESSION_ID?>&ptif_id=<?=$ptif_id?>&ymd=<?=$ymd?>&field_number=<?=$field_number?>&del_file_id="+fid;
    }
    function loaded() {
        if (isNotPC) ee("body").className = "is_not_pc";
        else ee("body").className = "is_pc";
    }
</script>
</head>
<body onload="loaded()" id="body" style="background-color:#d7f0ff">
<form name="frm" action="ks_picture.php?session=<?=GG_SESSION_ID?>&ptif_id=<?=$ptif_id?>&ymd=<?=$ymd?>&field_number=<?=$field_number?>&try_upload=1" method="post" enctype="multipart/form-data">
    <div style="color:#ff0000"><?=$err?></div>

    <div class="dialog_first_line"><table cellspacing="0" cellpadding="0"><tr>
        <th class="literal ptif_name"><?=hh($ptif_name)?></th><th class="literal sijibi">指示日：<?=$a_ymd["slash_mdw"]?></th><td></td>
    </tr></table></div>

    <table cellspacing="0" cellpadding="0" style="height:30px">
    <tr>
        <? if (count(explode(",",$siji_picture_names))>=8) { ?>
        <th class="left instruction">画像は最大８個までです。</th>
        <td class="right"><button type="button" disabled>アップロード</button></td>
        <? } else { ?>
        <th width="100">ファイル指定</th>
        <td><input type="file" name="siji_picture"></td>
        <td class="right"><button type="button" onclick="if (confirm('アップロードします。よろしいですか？')) document.frm.submit()">アップロード</button></td>
        <? } ?>
    </tr>
    </table>
    <hr>
    <div style="padding-top:10px">
    <div class="picture_area" style="height:360px"><?=$picture_m_html?></div>
    </div>
</form>
</body>
</html>
<?
pg_close($con);
