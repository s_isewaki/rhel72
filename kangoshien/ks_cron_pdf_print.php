<?
// 看護支援 定期 温度板PDF作成プログラム
//
// このプログラムは、cronに呼ばれ、所定のディレクトリにPDFを作成するものです。
//
//-------------------------------------------------------------------------------
// ■cron設定
//-------------------------------------------------------------------------------
// #crontab -e
//
// */1 * * * * php /var/www/html/comedix/kangoshien/ks_cron_pdf_print.php --check-minute
//
// ※cron的には1分おきに稼動します。
// ※実際にPDF作成が行われるのは、1時間に1回です。
//
//
//
//-------------------------------------------------------------------------------
// ■仕様
//-------------------------------------------------------------------------------
// ※以下は、【看護支援画面＞環境＞PDFバックアップ付近】
// 　のマウスホバーメッセージとして閲覧できます：：
//
// 指定時刻になると、指定された場所に「温度板週間報告」のPDFを自動保存します。
// ・先週分および今週分の温度板
// ・対象週内に１日以上入院していること
// ※「週ごと／患者ごと」に１ファイルが作成されます。
// ※ファイル名は[月曜日のYYYYMMDD]+[アンダーバー]+[患者ID]+[.pdf]で作成されます。
// ※指示やケア情報がなくとも、ファイルは作成されます。
// ※作成済PDFファイルがあれば、上書きされます。
//
//
//-------------------------------------------------------------------------------
// ■参考情報
//-------------------------------------------------------------------------------
// ◆コマンドオプションは 「--check-minute」 のみです。
// 　--check-minuteを外すと、即時生成を行います。
//
// ◆ブラウザから直接、
// 　http://xxx.xxx.xxx.xxx/comedix/kangoshien/ks_cron_pdf_print.php
// 　を呼び出しても、同様に即時生成を試みます。
//
// ◆ターミナルから以下いずれかを実行すると、
// 　実行ステップが確認できます。（英語）
// 　うまく動作しているか不明の場合、ターミナル実行で、状況がわかる手筈にしています。
// 　#php /var/www/html/comedix/kangoshien/ks_cron_pdf_print.php --check-minute
// 　#php /var/www/html/comedix/kangoshien/ks_cron_pdf_print.php
//
// ◆実行ログは
// 　/comedix/log/kangoshien_cron.log
// 　に格納されますが、肥大化の懸念より、現状、
// 　1年前の削除を実行した場合のみ、削除されたフォルダを1行、追記します。
// 　（実質、週に1行追加されることになります）"

$IS_CRON = 1;
chdir (dirname(__FILE__));
ob_start();
require_once("ks_common.php");
ob_end_clean();


//================================================
// デバッガ出力
//================================================
function echo2($s) {
    echo $s;
    echo "\n";
}

//================================================
// 実行ログファイル。
// このバッチが実行されると、ひとまずタイムスタンプを更新する。
//================================================
$LOGPATH = dirname(dirname(__FILE__))."/log/kangoshien_cron.log";
touch($LOGPATH);
chmod ($LOGPATH, 0666);
function execlog($s) {
    echo2($s);
    global $LOGPATH;
    $fp = fopen($LOGPATH, 'a');
    fwrite($fp, "\n".$s);
    fclose($fp);
}

//------------------------------------------------------------------------------
// 設定値：実行フン数を取得。設定がなければ終了、フンがあわなければ終了。
//------------------------------------------------------------------------------
if (in_array("--check-minute", $argv)) {
    $pdfBackupMinute = (int)dbGetOne("Select value from system_config where key = 'kangoshien.pdfBackupMinute'");
    $_nowMinute = (int)date("i");
    if ($_nowMinute!=$pdfBackupMinute) { echo2('[ABORT!] Minute Missmatch (Now)'.$_nowMinute." (pdfBackupMinute) ".$pdfBackupMinute); die; }
    echo2("-- (minute-check-ok) ".$pdfBackupMinute);
}


//------------------------------------------------------------------------------
// 設定値：保存先ディレクトリを取得。設定がなければ終了
//------------------------------------------------------------------------------
$pdfBackupDir = dbGetOne("Select value from system_config where key = 'kangoshien.pdfBackupDir'");
if (!is_dir($pdfBackupDir)) { echo2('[ABORT!] Setting "kangoshien.pdfBackupDir" is not directory. [value]'.$pdfBackupDir); die; }
if (substr($pdfBackupDir, -1)!="/") $pdfBackupDir .= "/";
if (!is_dir($pdfBackupDir)) { echo2('[ABORT!] Setting "kangoshien.pdfBackupDir" is not directory. [value]'.$pdfBackupDir); die; }


echo2("-- (ymdhms) ".date("Y/m/d H:i:s"));
echo2("-- (backup-dir) ".$pdfBackupDir);


//------------------------------------------------------------------------------
// 今週月曜、先週月曜を求める
//------------------------------------------------------------------------------
$WYL1 = create_week_ymd_list(date("Ymd")); // Weekly Ymd List
$konsyu_monday_ymd = $WYL1[0];
echo2("-- (konsyu-monday-ymd) ".$konsyu_monday_ymd);

$WYL0 = create_week_ymd_list(date("Ymd", strtotime(slash_ymd($konsyu_monday_ymd)." - 7 day")));
$sensyu_monday_ymd = $WYL0[0];
echo2("-- (sensyu-monday-ymd) ".$sensyu_monday_ymd);

//------------------------------------------------------------------------------
// 古いファイルとフォルダを削除。
// 一年以上前の名前のファイルは消す。
//------------------------------------------------------------------------------
$del_ymd = (substr($sensyu_monday_ymd,0,4)-1).substr($sensyu_monday_ymd,4);
echo2("-- (delete-limit-ymd) ".$del_ymd);

$fpath_list = glob($pdfBackupDir."*");
foreach($fpath_list as $fpath){
    if(is_dir($fpath)) continue;
    $ymd = substr(basename($fpath), 0, 8);
    if ($ymd < $del_ymd) {
        execlog("[DELETE FILE] "."[".$ymd."]".$fpath);
        unlink($fpath);
        if (file_exists($fpath)) execlog("[DELETE ERROR] ".$fpath);
    }
}


//------------------------------------------------------------------------------
// ファイル作成
//------------------------------------------------------------------------------
$wyl_ary = array($WYL0, $WYL1);
foreach ($wyl_ary as $_WYL) {
    //------------------------------------------------------------------------------
    // 対象週の入院患者一覧
    //------------------------------------------------------------------------------
    $_monday_ymd = $_WYL[0];
    $_sunday_ymd = $_WYL[6];
    echo2("-- [MONDAY] ".$_monday_ymd);
    $sql =
    " select pt.ptif_id from ptifmst pt".
    " left outer join (".
    "     select ptif_id from sum_ks_mst_pat_nyuin_taiin".
    "     where ( nyuin_ymd is not null and nyuin_ymd <> '' and nyuin_ymd <= ".dbES($_sunday_ymd).")".
    "     and ( taiin_ymd is null or taiin_ymd = '' or taiin_ymd >= ".dbES($_monday_ymd).")".
    "     group by ptif_id".
    " ) nt on ( nt.ptif_id = pt.ptif_id )".
    " where nt.ptif_id is not null";

    $sel = dbFind($sql);
    $_ptif_id_list = array();
    while($_row = pg_fetch_array($sel)){
        echo2("-- [TARGET] [monday] ".$_monday_ymd." [ptif_id] ".$_row["ptif_id"]);
        $_ptif_id_list[]= $_row["ptif_id"];
    }

    //------------------------------------------------------------------------------
    // PDFを上書き作成する
    //------------------------------------------------------------------------------
    foreach ($_ptif_id_list as $_ptif_id) {
        // 患者ごとに２週間ぶんのPDF作成
        $_REQUEST = array(
            "empty_print_and_show_preview" => 1, // PDF出力を標準出力へ
            "ptif_id" => $_ptif_id, // 患者名
            "bldg_ward"=>"",// 部署はカラ
            "ymd"=>$_monday_ymd, // 対象日（対象の月曜でよい）
            "daily_weekly"=>"weekly", // 週単位
            "ptif_name_find" => "", // 「患者名検索用文字列」は存在しない
        );
        ob_clean();
        ob_start();
        require("ks_print_pdf_utf16_ondoban.php");
        $_pdf_content = ob_get_contents();
        ob_end_clean();
        if (!$_pdf_content) {
            echo2("-- [!NO CONTENT] [ptif_id] ".$_ptif_id);
            continue;
        }
        $_filename = $_monday_ymd."_".$_ptif_id.".pdf";
        $_filepath = $pdfBackupDir.$_filename;
        unlink($_filepath);
        $_fp = fopen($_filepath, 'w');
        fwrite($_fp, $_pdf_content);
        fclose($_fp);

        chmod($_filepath, 0666);
        echo2("-- [PDF-OUTPUT] ".$_filepath);
    }
}
header("text/html; charset=utf-8");
echo2("-- end");
