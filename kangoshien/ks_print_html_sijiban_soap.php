<?php
    define("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING", 1);
    require_once("ks_common.php");
    require_once("ks_ajax_sijiban.php");

    if (!$_REQUEST["ymd"]) $_REQUEST["ymd"] = date("Ymd");


	$win_title = "指示板";
	if ($_REQUEST["view_mode"]=="soap") $win_title = "SOAP";
	if ($_REQUEST["ext_view_mode"]=="input_care") $win_title = "一括バイタル・ケア";
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/start/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" type="text/css" href="css/common.css">
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/showpage.js"></script>
<title><?=$win_title?></title>
<style type="text/css">
    <? // 印刷用打ち消しスタイル ?>
    table.sijiban_soap_table td.hover1 div.hover1child { background-color:#d7f0ff; }
    table.sijiban_soap_table div.hover2 div.hover2child { background-color:#d7f0ff; }
    table.sijiban_soap_table td table.ward_ptrm_panel td:hover { border:1px solid #cfc054; color:#948729; padding:0px 3px; background-color:#ffffcc }
    table.sijiban_soap_table td table.ward_ptrm_panel td.no_ptrm:hover { border:1px solid #ff0000; color:#ffffff; padding:0px 3px; background-color:#ff6666 }
    .hover:hover { background-color:#ffffff; cursor:default; }
    td.bikou_cell:hover, div.bikou_cell, td.bikou_cell:hover, div.bikou_cell:hover { background-color:#f5f5f5 }
</style>
<script type="text/javascript">
    <? // 打ち消しJavaScript ?>
    var dfmPtifDetail = { selectPtifAndShowOndoban : function(){} };
    var dfmSijibanBikou = { popup : function(){} };
    var dfmSijiban = { popup : function() {} };
</script>
</head>
<body onload="print()">


<?
    get_sijiban_soap_list(
        $_REQUEST["ptif_id"], $_REQUEST["monday_ymd"], $_REQUEST["bldg_ward"], $_REQUEST["is_nyuin_taiin"],
        $_REQUEST["view_mode"], "", $_REQUEST["print_mode"], $_REQUEST["ext_view_mode"], $_REQUEST["ext_view_ymd"], $_REQUEST["ptif_name_find"]
    );
?>




</body>
</html>
<?
pg_close($con);
