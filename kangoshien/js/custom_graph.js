


//*************************************************************************
// リサイズイベント、グラフの作成、各種位置調整
//*************************************************************************
var ondobanDayWidth = 0;
var image_width = 0;
var parentResized = 0;
var canvasWidth = 0;
var vitalPosData = [];
var curClientWidth = 0;
var clientWidth = 0;
var day_size = 7;
function getBaseWidth() {
    day_size = 7;
    if (single_ymd) day_size = 1;
    var LEFT_HEADER_WIDTH = 120;
    var RIGHT_SIJI_WIDTH = 120;
    var LR_BORDER_WIDTH = 2;
    var LEFT_HEADER_BORDER_WIDTH = 1;
    var INNER_BORDER_WIDTH = day_size - 1;
    clientWidth = (document.getElementById("header_menu_div").clientWidth || document.getElementById("header_menu_div").offsetWidth);
    if (clientWidth<850) clientWidth = 850;
    var otherFixedWidth = LR_BORDER_WIDTH+LEFT_HEADER_WIDTH+LEFT_HEADER_BORDER_WIDTH+RIGHT_SIJI_WIDTH+INNER_BORDER_WIDTH;
    var innerWidth = clientWidth-otherFixedWidth;
    ondobanDayWidth = Math.floor(innerWidth / day_size); // ボーダを抜いた１セルの幅
    canvasWidth = ondobanDayWidth * day_size + INNER_BORDER_WIDTH;
}

function refreshGraphAndAdjustWidth(isResize){
    try {
        if (view_mode!="ondoban") return;
        if (isResize && !isNotPC) {
            ee("ondoban_container").style.display = "none";
            ee("ondoban_siji").style.display = "none";
        }
        var dataT = ee("serialDataT").value.split(",");
        var dataR = ee("serialDataR").value.split(",");
        var dataP = ee("serialDataP").value.split(",");
        var dataL = ee("serialDataL").value.split("\n");
        var dataK = ee("serialDataK").value.split("\n");
        var dataBP = ee("serialDataBP").value.split(",");
        var dataSK = ee("serialDataSK").value.split("\n");
        vitalPosData = eval(ee("serialVitalPosData").value);

        var table = ee("ondoban_table");
        var table_h = ee("ondoban_table_float_header");

        if (!stickySijibar) {
            ee("ondoban_siji").style.left = (120+canvasWidth + 6) + "px";
        }
        if (isResize) {
            $("#ondoban_table .clickbox").each(function(){
                this.style.width = ondobanDayWidth + "px";
                var dayIndex = int(this.getAttribute("day_index"));
                this.style.left = (ondobanDayWidth * dayIndex + dayIndex) + "px";
            });
            for (var idx=1; idx<=day_size; idx++) {
                ee("ondobanTHeader"+idx).style.width = ondobanDayWidth+"px";
                if (stickyMenubar) ee("ondobanFloatTHeader"+idx).style.width = ondobanDayWidth+"px";
            }
            if (!isNotPC) {
                $("#ondoban_table div.o_abs, #ondoban_float_header_toprow div.o_abs").each(function(){
                    var cls = trim(this.className.replace("o_abs", ""));
                    var ww = ondobanDayWidth;
                    if (cls=="ww2")       ww=int(ww / 2)-1;
                    else if (cls=="ww3")  ww=int(ww / 3)-1;
                    else if (cls=="ww4")  ww=int(ww / 4)-1;
                    else if (cls=="ww015") ww=int(ww / 100 * 15)-1;
                    else if (cls=="ww02") ww=int(ww / 10 * 2)-1;
                    else if (cls=="ww03") ww=int(ww / 10 * 3)-1;
                    else if (cls=="ww035") ww=int(ww / 100 * 35)-1;
                    else if (cls=="ww04") ww=int(ww / 10 * 4)-1;
                    else if (cls=="ww06") ww=int(ww / 10 * 6)-1;
                    else if (cls=="ww07") ww=int(ww / 10 * 7)-1;
                    else if (cls=="ww08") ww=int(ww / 10 * 8)-1;
                    this.style.width = ww+"px";
                });
            }
        }
        curClientWidth = clientWidth;

        var canvasDivNames = ["main","head","sanso","kouseizai"];
        for (var dn = 0; dn <= 3; dn++){
            $("div#graph_"+canvasDivNames[dn]+"_canvas_div div.graph_text").each(function() {
                var elem = this.parentNode.removeChild(this);
                delete elem;
            });
        }

        var GRAPH_MAX_TIME_WIDTH = 60*24*day_size; // 7日分
        for (var t in vitalPosData){
            vitalPosData[t].leftpx = int(vitalPosData[t].graph_day / GRAPH_MAX_TIME_WIDTH * (ondobanDayWidth*day_size+day_size));
        }

        var canvasHeight = 300;
        ee("graph_main_canvas").width = canvasWidth;
        ee("graph_main_canvas").height = canvasHeight;
        ee("graph_main_canvas").style.width = canvasWidth + "px";
        ee("graph_main_canvas").style.height = canvasHeight + "px";
        var widthPerMinute = canvasWidth / (60*24*day_size);
        if (single_ymd) widthPerMinute = widthPerMinute * day_size;
        ee("graph_sanso_canvas").width = canvasWidth;
        ee("graph_sanso_canvas").style.width = canvasWidth + "px";
        ee("graph_kouseizai_canvas").width = canvasWidth;
        ee("graph_kouseizai_canvas").style.width = canvasWidth + "px";

        //*************************************
        // メイングラフ
        //*************************************
        var graphMain = new customGraph.line("graph_main_canvas", canvasWidth, canvasHeight);
        var heightPerGaugeT = canvasHeight / (43 - 33);
        var listT = [];
        for (var i=0; i<dataT.length; i++){
            var p = dataT[i].split("=");
            if (p.length!=2) continue;
            p[0] = parseInt(p[0] * widthPerMinute);
            p[1] = parseInt((43-p[1]) * heightPerGaugeT);
            listT.push({"x":p[0],"y":p[1]});
        }
        var heightPerGaugeR = canvasHeight / (55 - 5);
        var listR = [];
        for (var i=0; i<dataR.length; i++){
            var p = dataR[i].split("=");
            if (p.length!=2) continue;
            p[0] = parseInt(p[0] * widthPerMinute);
            p[1] = parseInt((55-p[1]) * heightPerGaugeR);
            listR.push({"x":p[0],"y":p[1]});
        }
        var heightPerGaugeP = canvasHeight / (210 - 10);
        var listP = [];
        for (var i=0; i<dataP.length; i++){
            var p = dataP[i].split("=");
            if (p.length!=3) continue;
            p[0] = parseInt(p[0] * widthPerMinute);
            p[1] = parseInt((210-p[1]) * heightPerGaugeP);
            p[2] = (p[2] == "") ? "circle" : "batsu";
            listP.push({"x":p[0],"y":p[1],"dottype":p[2]});
        }
        var heightPerGaugeBP = canvasHeight / (240 - 40);
        var listBP = [];
        for (var i=0; i<dataBP.length; i++){
            var p = dataBP[i].split("=");
            if (p.length!=2) continue;
            var ud = p[1].split("_");
            if (ud.length!=2) continue;
            p[0] = parseInt(p[0] * widthPerMinute);
            ud[0] = parseInt((240-ud[0]) * heightPerGaugeBP);
            ud[1] = parseInt((240-ud[1]) * heightPerGaugeBP);
            listBP.push({"x":p[0],"u":ud[0],"d":ud[1]});
        }
        var listL = [];
        var extLabelList = [];
        for (var i=0; i<dataL.length; i++){
            var p = dataL[i].split("\t");
            if (p.length!=3) continue;
            p[0] = parseInt(p[0] * widthPerMinute);
            p[1] = parseInt(p[1] * widthPerMinute);
            listL.push({"x":p[0],"y":0,"w":p[1]-p[0], "h":canvasHeight});
            if (p[2]) {
                extLabelList.push({"type":"LABEL", "color":"#6600ff", "pos":{"x":p[0]+3,  "y":0}, "label":p[2],  "size":"12px"});
            }
        }


        var itemsM = [,
            {"type":"SQUARE", "color":"rgba(100,0,255,0.15)","items":listL},
            {"type":"BAR","color":"#958f22","dottype":"circle","items":listBP},
            {"type":"ORESEN", "color":"#0000ff","dottype":"wsquare","items":listT},
            {"type":"ORESEN", "color":"#ff0000","dottype":"circle","items":listP},
            {"type":"ORESEN", "color":"#008822","dottype":"cross","items":listR}
        ];
        for (var idx=0; idx<extLabelList.length; idx++) {
            itemsM.push(extLabelList[idx]);
        }

        //*************************************
        // ヘッダグラフ
        //*************************************
        var graphHead = new customGraph.line("graph_head_canvas", 120, canvasHeight);
        var bordersH = [];
        bordersH.push({"type":"solid","color":"#9bc8ec","fx":0,"fy":15.5,"tx":120,"ty":15.5}); // 横軸(一番上水色)
        bordersH.push({"type":"solid","color":"#ff60cf","fx":0,"fy":180.5,"tx":120,"ty":180.5}); // 横軸(中間ピンク)
        for (var i = 1; i <= 9; i++){ // 横軸(灰色)
            if (i != 6) bordersH.push({"type":"solid","color":"#dddddd","fx":0,"fy":(i*30)+0.5,"tx":150,"ty":(i*30)+0.5});
            bordersH.push({"type":"holizont-dashed","color":"#e5e5e5","fx":0,"fy":(i*30)+15.5,"tx":150,"ty":(i*30)+15.5});
        }
        for (var i = 1; i <= 5; i++) bordersH.push({"type":"solid","color":"#9bc8ec","fx":30*i+i-0.5,"fy":0,"tx":30*i+i-0.5,"ty":canvasHeight}); // 縦軸(水色)
        bordersH.push({"type":"solid","color":"#0000ff","fx":16,"fy":7.5,"tx":28,"ty":7.5});
        bordersH.push({"type":"solid","color":"#ff0000","fx":47,"fy":7.5,"tx":59,"ty":7.5});
        bordersH.push({"type":"solid","color":"#008822","fx":78,"fy":7.5,"tx":91,"ty":7.5});
        var itemsH = [
            {"type":"MARK", "color":"#0000ff","dottype":"wsquare", "items":[{"x":22, "y":8}]},
            {"type":"MARK", "color":"#ff0000","dottype":"circle",  "items":[{"x":53, "y":7.5}]},
            {"type":"MARK", "color":"#008822","dottype":"cross",   "items":[{"x":85, "y":8}]},
            {"type":"LABEL", "color":"#0000ff", "pos":{"x":3,  "y":2}, "label":"T",  "size":"11px"},
            {"type":"LABEL", "color":"#ff0000", "pos":{"x":34, "y":2}, "label":"P",  "size":"11px"},
            {"type":"LABEL", "color":"#008822", "pos":{"x":65, "y":2}, "label":"R",  "size":"11px"},
            {"type":"LABEL", "color":"#958f22", "pos":{"x":94,"y":2}, "label":"BP", "size":"11px"}
        ];
        for (var i = 0; i < 10; i++){
            var lblT = lblP = lblR = lblS = lblBP = 0;
            if (i > 0) {
                lblT = 33 + (i);
                lblP = 10 + (i*20);
                lblR = 5 + (i*5);
                lblBP = 40 + (i*20);
            }
            var y = 287-(i*30);
            itemsH.push({"type":"LABEL", "color":"#0000ff", "pos":{"x":3,  "y":y}, "label":lblT,  "size":"11px"});
            itemsH.push({"type":"LABEL", "color":"#ff0000", "pos":{"x":34, "y":y}, "label":lblP,  "size":"11px"});
            itemsH.push({"type":"LABEL", "color":"#008822", "pos":{"x":65, "y":y}, "label":lblR,  "size":"11px"});
            itemsH.push({"type":"LABEL", "color":"#958f22", "pos":{"x":96,"y":y}, "label":lblBP, "size":"11px"});
        }

        //*************************************
        // 酸素吸入グラフ
        //*************************************
        var bordersSK = [];
        var itemsSK = [];
        // 縦軸(水色)/日単位  単日なら6/12/18時水色
        var rowIdx = -1;
        var prevToX = -1;
        for (var idx=0; idx<dataSK.length; idx++) {
            if (!dataSK[idx]) continue;
            var row = dataSK[idx].split("\t");
            var frX = widthPerMinute*row[0];
            var toX = widthPerMinute*row[1];
            //if (frX < prevToX && prevToX >= 0) rowIdx++;
            rowIdx++;
            prevToX = toX;
            var addHeight = (24*rowIdx);
            var clr = (row[4]=="sanso" ? "#8064a2" : "#ff9155");
            var dottype = (row["not_ended"]=="y" ? "wsquare" : "square" );
            bordersSK.push({"type":"solid","color":clr,"fx":frX,"fy":18.5+addHeight,"tx":toX,"ty":18.5+addHeight});
            if (!row[2]) itemsSK.push({"type":"MARK", "color":clr,"dottype":"square", "items":[{"x":frX, "y":18+addHeight}]}); // not is_fr_overflow
            if (!row[3]) itemsSK.push({"type":"MARK", "color":clr,"dottype":dottype,  "items":[{"x":toX, "y":18+addHeight}]}); // not is_to_overflow
            if (row[5]!="") itemsSK.push({"type":"LABEL", "color":"#000000", "pos":{"x":frX+3,"y":2+addHeight}, "label":row[5], "size":"11px"}); // litter
        }
        if (rowIdx==-1) rowIdx = 0;
        ee("graph_sanso_canvas_div").style.height = ((rowIdx+1)*24) + "px";
        $(".sanso_clickbox").each(function(){ this.style.height = (rowIdx+1)*24 + "px"; });
        var graphSanso = new customGraph.line("graph_sanso_canvas", canvasWidth, (rowIdx+1)*24);
        var rowSizeSK = rowIdx;

        //*************************************
        // 抗生剤グラフ
        //*************************************
        var bordersK = [];
        var itemsK = [];
        // 縦軸(水色)/日単位  単日なら6/12/18時水色
        var rowChecker = [];
        var rowIdx = 0;
        var prevToX = -1;
        for (var idx=0; idx<dataK.length; idx++) {
            if (!dataK[idx]) continue;
            var row = dataK[idx].split("\t");
            var frX = widthPerMinute*row[0];
            var toX = widthPerMinute*row[1];
            if (frX <= prevToX && prevToX >= 0) rowIdx++;
            prevToX = toX;
            var addHeight = (24*rowIdx);
            var dottype = (row["not_ended"]=="y" ? "wsquare" : "square" );
            bordersK.push({"type":"solid","color":"#9bbb59","fx":frX,"fy":18.5+addHeight,"tx":toX,"ty":18.5+addHeight});
            if (!row[2]) itemsK.push({"type":"MARK", "color":"#9bbb59","dottype":"square", "items":[{"x":frX, "y":18+addHeight}]}); // not is_fr_overflow
            if (!row[3]) itemsK.push({"type":"MARK", "color":"#9bbb59","dottype":dottype,  "items":[{"x":toX, "y":18+addHeight}]}); // not is_to_overflow
            if (row[4]!="") itemsK.push({"type":"LABEL", "color":"#000000", "pos":{"x":frX-3,"y":1+addHeight}, "label":row[4], "size":"11px"}); // 薬剤名(med_name)
        }
        ee("graph_kouseizai_canvas_div").style.height = ((rowIdx+1)*24) + "px";
        $(".kouseizai_clickbox").each(function(){ this.style.height = (rowIdx+1)*24 + "px"; });
        var graphKouseizai = new customGraph.line("graph_kouseizai_canvas", canvasWidth, (rowIdx+1)*24);
        var rowSizeK = rowIdx;


        var bordersM = [];

        // 縦軸(水色) 日単位   単日なら6/12/18時水色
        var dw = ondobanDayWidth;
        var h6 = dw / 4;
        var h1 = dw / 24;
        if (!single_ymd) {
            for (var i1 = 0; i1 <= 6; i1++) {
                var dp = dw*i1+i1-0.5;
                for (var i2 = 1; i2 <= 3; i2++) {
                    bordersM.push({"type":"solid","color":"#d1e6f5","fx":dp+h6*i2,"fy":0,"tx":dp+h6*i2,"ty":canvasHeight}); // 温度板：6h間隔で
                    bordersSK.unshift({"type":"solid","color":"#d1e6f5","fx":dp+h6*i2,"fy":0,"tx":dp+h6*i2,"ty":(rowSizeSK+1)*24}); // 酸素
                    bordersK.unshift({"type":"solid","color":"#d1e6f5","fx":dp+h6*i2,"fy":0,"tx":dp+h6*i2,"ty":(rowSizeK+1)*24}); // 抗生剤
                }
                if (i1==0) continue;
                bordersM.push({"type":"solid","color":"#9bc8ec","fx":dp,"fy":0,"tx":dp,"ty":canvasHeight}); // 温度板：1日間隔で
                bordersSK.unshift({"type":"solid","color":"#9bc8ec","fx":dp,"fy":0,"tx":dp,"ty":(rowSizeSK+1)*24}); // 酸素
                bordersK.unshift({"type":"solid","color":"#9bc8ec","fx":dp,"fy":0,"tx":dp,"ty":(rowSizeK+1)*24}); // 抗生剤
            }
        }
        else {
            for (var i1=0; i1<=3; i1++) {
                var dp = h6*i1-0.5;
                for (var i2 = 1; i2 <= 5; i2++) {
                    bordersM.push({"type":"solid","color":"#d1e6f5","fx":dp+h1*i2,"fy":0,"tx":dp+h1*i2,"ty":canvasHeight}); // 温度板：1h間隔で
                    bordersSK.unshift({"type":"solid","color":"#d1e6f5","fx":dp+h1*i2,"fy":0,"tx":dp+h1*i2,"ty":(rowSizeSK+1)*24}); // 酸素
                    bordersK.unshift({"type":"solid","color":"#d1e6f5","fx":dp+h1*i2,"fy":0,"tx":dp+h1*i2,"ty":(rowSizeK+1)*24}); // 抗生剤
                }
                if (i1==0) continue;
                bordersM.push({"type":"solid","color":"#9bc8ec","fx":dp,"fy":0,"tx":dp,"ty":canvasHeight}); // 温度板：1日間隔で
                bordersSK.unshift({"type":"solid","color":"#9bc8ec","fx":dp,"fy":0,"tx":dp,"ty":(rowSizeSK+1)*24}); // 酸素
                bordersK.unshift({"type":"solid","color":"#9bc8ec","fx":dp,"fy":0,"tx":dp,"ty":(rowSizeK+1)*24}); // 抗生剤
            }
        }

        bordersM.push({"type":"solid","color":"#ff60cf","fx":0,"fy":180.5,"tx":canvasWidth,"ty":180.5}); // 横軸(中間ピンク)
        for (var i = 0; i <= 9; i++){ // 横軸(灰色)
            if (i > 0 && i != 6) bordersM.push({"type":"solid","color":"#dddddd","fx":0,"fy":(i*30)+0.5,"tx":canvasWidth,"ty":(i*30)+0.5});
            bordersM.push({"type":"holizont-dashed","color":"#e5e5e5","fx":0,"fy":(i*30)+15.5,"tx":canvasWidth,"ty":(i*30)+15.5});
        }

        //*************************************
        // まとめて画面表示
        //*************************************
        graphMain.draw(itemsM, bordersM);
        graphHead.draw(itemsH, bordersH);
        graphSanso.draw(itemsSK, bordersSK);
        graphKouseizai.draw(itemsK, bordersK);
    } catch (ex){}
    ee("ondoban_container").style.display = "";
    ee("ondoban_siji").style.display = "";
}








//*************************************************************************
// グラフ作成クラス
//*************************************************************************

var customGraph = new Object();

//********************************
// コンストラクタ
//********************************
customGraph.line = function (id, width, height) {
    if (window.Canvas) {
        var elm = window.Canvas(width, height, document.getElementById(id));
        elm.id = id;
    }
    else {
        var elm = document.getElementById(id);
        elm.width = width;
        elm.style.width = width+"px";
        elm.height = height;
        elm.style.height = height+"px";
    }

    if(! elm) { return; }
    if( ! elm.nodeName.match(/^CANVAS$/i) ) { return; }
    if( ! elm.parentNode.nodeName.match(/^DIV$/i) ) { return; };
    // CANVAS要素
    if ( ! elm.getContext ){ return; }
    this.canvas = elm;
    // 2D コンテクストの生成
    this.ctx = this.canvas.getContext('2d');
    this.canvas.style.margin = "0";
    this.canvas.parentNode.style.position = "relative";
    this.canvas.parentNode.style.padding = "0";
    // CANVAS要素の親要素となるDIV要素の幅と高さをセット
    this.canvas.parentNode.style.width = this.canvas.width + "px";
    this.canvas.parentNode.style.height = this.canvas.height + "px";
    this.xMax = this.canvas.width;
    this.yMax = this.canvas.height;
};

//********************************
// 描画
//********************************
customGraph.line.prototype.draw = function(items, borders) {
    if( ! this.ctx ) {return;}

    // 背景を白に
    this.ctx.fillStyle = "#ffffff";
    this.ctx.fillRect(0, 0, this.xMax, this.yMax);

    // 補助線類の描画
    for (b in borders){
        this.ctx.beginPath();
        if (borders[b].type=="holizont-dashed"){
            var frX = borders[b].fx;
            for(;;) {
                var toX = Math.min(borders[b].tx, frX+4);
                this.ctx.moveTo(frX, borders[b].fy);
                this.ctx.lineTo(toX, borders[b].fy);
                frX += 8;
                if (frX >= borders[b].tx) break;
            }
        } else {
            this.ctx.moveTo(borders[b].fx, borders[b].fy);
            this.ctx.lineTo(borders[b].tx, borders[b].ty);
        }
        this.ctx.lineWidth = 1;
        this.ctx.strokeStyle = borders[b].color;
        this.ctx.stroke();
    }

    // 折線グラフを描写
    for(var i in items) {
        this.ctx.beginPath();
        this.ctx.lineJoin = "round";

        // 折れ線グラフを描画
        if (items[i].type=="ORESEN"){
            this.ctx.lineWidth = 1;
            for(var j=0; j<items[i].items.length; j++) {
                if(j == 0) {
                    this.ctx.moveTo(items[i].items[j].x-0.5, items[i].items[j].y-0.5);
                } else {
                    this.ctx.lineTo(items[i].items[j].x-0.5, items[i].items[j].y-0.5);
                }
            }
        }
        // BPタイプ棒グラフを描画
        if (items[i].type=="BAR"){
            this.ctx.lineWidth = 6;
            for(var j=0; j<items[i].items.length; j++) {
                this.ctx.moveTo(items[i].items[j].x, items[i].items[j].u);
                this.ctx.lineTo(items[i].items[j].x, items[i].items[j].d);
            }
        }
        // 離床理由の矩形グラフを描画
        if (items[i].type=="SQUARE"){
            this.ctx.lineWidth = 1;
            this.ctx.fillStyle = items[i].color;
            for(var j=0; j<items[i].items.length; j++) {
                this.ctx.fillRect(items[i].items[j].x, items[i].items[j].y, items[i].items[j].w, items[i].items[j].h);
            }
        }
        this.ctx.strokeStyle = items[i].color;
        this.ctx.stroke();

        // ドットを描画
        if (items[i].type=="ORESEN" || items[i].type=="MARK"){
            for(var k=0; k<items[i].items.length; k++) {
                var dottype = (items[i].items[k].dottype && items[i].items[k].dottype != "") ? items[i].items[k].dottype : items[i].dottype;
                this._draw_dot(items[i].items[k].x, items[i].items[k].y, 3, dottype, items[i].color);
            }
        }
        // 文字を描画
        if (items[i].type=="LABEL"){
            var posX = items[i].pos.x;
            var posY = items[i].pos.y;
            if(posX < 0 || posX > this.xMax || posY > this.yMax || posY < 0) continue;
            var margin = 3;
            this._drawText(posX, posY, items[i].label, items[i].size, items[i].color);
        }
    }
};

//********************************
// 頂点の描画
//********************************
customGraph.line.prototype._draw_dot = function(x, y, rad, type, color) {
    this.ctx.beginPath();
    if( type == "circle" ) {
        this.ctx.fillStyle = color;
        this.ctx.arc(x, y, rad, 0, Math.PI*2, false);
        this.ctx.closePath();
        this.ctx.fill();
    } else if( type == "wsquare" ) {
        this.ctx.fillStyle = "#ffffff";
        this.ctx.moveTo(x-rad, y-rad);
        this.ctx.lineTo(x+rad, y-rad);
        this.ctx.lineTo(x+rad, y+rad);
        this.ctx.lineTo(x-rad, y+rad);
        this.ctx.closePath();
        this.ctx.fill();
        this.ctx.beginPath();
        this.ctx.strokeStyle = color;
        this.ctx.moveTo(x-rad-0.5, y-rad-0.5);
        this.ctx.lineTo(x+rad+0.5, y-rad-0.5);
        this.ctx.lineTo(x+rad+0.5, y+rad+0.5);
        this.ctx.lineTo(x-rad-0.5, y+rad+0.5);
        this.ctx.closePath();
        this.ctx.stroke();
    } else if( type == "cross" ) {
        this.ctx.strokeStyle = color;
        this.ctx.moveTo(x-0.5, y-rad-1);
        this.ctx.lineTo(x-0.5, y+rad);
        this.ctx.moveTo(x-rad-1, y-0.5);
        this.ctx.lineTo(x+rad, y-0.5);
        this.ctx.stroke();
    } else if( type == "square" ) {
        this.ctx.fillStyle = color;
        this.ctx.moveTo(x-rad, y-rad);
        this.ctx.lineTo(x+rad, y-rad);
        this.ctx.lineTo(x+rad, y+rad);
        this.ctx.lineTo(x-rad, y+rad);
        this.ctx.closePath();
        this.ctx.fill();
    } else if( type == "triangle" ) {
        this.ctx.fillStyle = color;
        this.ctx.moveTo(x, y-rad);
        this.ctx.lineTo(x+rad, y+rad);
        this.ctx.lineTo(x-rad, y+rad);
        this.ctx.closePath();
        this.ctx.fill();
    } else if( type == "i-triangle" ) {
        this.ctx.fillStyle = color;
        this.ctx.moveTo(x, y+rad);
        this.ctx.lineTo(x+rad, y-rad);
        this.ctx.lineTo(x-rad, y-rad);
        this.ctx.closePath();
        this.ctx.fill();
    } else if( type == "diamond" ) {
        this.ctx.fillStyle = color;
        this.ctx.moveTo(x, y-rad);
        this.ctx.lineTo(x+rad, y);
        this.ctx.lineTo(x, y+rad);
        this.ctx.lineTo(x-rad, y);
        this.ctx.closePath();
        this.ctx.fill();
    } else if( type == "batsu" ) {
        this.ctx.strokeStyle = "#000000";
        this.ctx.moveTo(x-rad, y-rad);
        this.ctx.lineTo(x+rad, y+rad);
        this.ctx.moveTo(x-rad, y+rad);
        this.ctx.lineTo(x+rad, y-rad);
        this.ctx.stroke();
    } else {
        this.ctx.fillStyle = color;
        this.ctx.arc(x, y, rad, 0, Math.PI*2, false);
        this.ctx.closePath();
        this.ctx.fill();
    }
};

//********************************
// キャプションの描画
//********************************
customGraph.line.prototype._drawText = function(x, y, text, font_size, color) {
    var div = document.createElement('DIV');
    div.appendChild(document.createTextNode(text));
    div.style.fontSize = font_size;
    div.style.fontWeight = "normal";
    div.style.color = color;
    div.style.margin = "0";
    div.style.padding = "0";
    div.style.position = "absolute";
    div.style.left = x.toString() + "px";
    div.style.top = y.toString() + "px";
    div.className = "graph_text";
    this.canvas.parentNode.appendChild(div);
}
