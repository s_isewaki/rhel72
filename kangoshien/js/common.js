var pcs = ["msie","chrome","safari","firefox","opera","trident"];
var no_pcs = ["ipod","ipad","android","mobile"];
//var sticky = 0;
var isNotPC = 0;
var ua = window.navigator.userAgent.toLowerCase();
//for (var idx=0; idx<pcs.length; idx++) if (ua.indexOf(pcs[idx])>=0) sticky = 1;
//for (var idx=0; idx<no_pcs.length; idx++) if (ua.indexOf(no_pcs[idx])>=0) { sticky = 0; isNotPC = 1; }



var str = function(s) { if (!s && s!=0) return ""; return ""+s; }
var substr2=function(s,p,l){s=str(s);if(!s||s.length==0)return "";if(s.length<=p)return "";if(!l || s.length<=p+l) return s.substring(p);return s.substring(p,p+l);}
var stopEvent = function(evt) { if(!evt) evt=window.event; try{evt.stopPropagation();}catch(ex){} try{evt.preventDefault();}catch(ex){} try{evt.cancelBubble=true;}catch(ex){} return false; }
var int = function(s) { if (!s) return 0; var num = parseInt(s,10); if (!num || isNaN(num)) return 0; return num; };
var ee = function(id) { return document.getElementById(id); }
var trim = function(s) { if (s==undefined) return ""; s+=""; return s.replace(/^[\s　]+|[\s　]+$/g, ''); }
var tryFocus = function(id) { if (!id) return; var e=ee(id); tryFocusByElem(e); }
var tryFocusByElem = function(e) { if (!e) return; if(!e || e.disabled || e.readOnly) return; e.focus(); try{e.select();}catch(ex){} }
var isEnterKey = function(elem, evt){if(!elem.disabled && !elem.readOnly) var o=window.event; var key=(o?o.keyCode||o.which:evt.keyCode); return key==13; }
var isKeyboradKey=function(elem, evt, k){if(!elem.disabled && !elem.readOnly) var o=window.event; var key=(o?o.keyCode||o.which:evt.keyCode); return key==k; }
var getComboValue = function(obj) { if (obj.selectedIndex<0) return ""; return obj.options[obj.selectedIndex].value; }
var getComboText = function(obj) { if (obj.selectedIndex<0) return ""; return obj.options[obj.selectedIndex].text; }
var uEnc = function(s) { return encodeURIComponent(s); }
var zeroSuppress = function(s) { return s.replace(/^0/, ""); }
var setDisabled = function(elem, bool) { elem.disabled = bool; if (bool) $(elem).addClass("text_readonly"); else $(elem).removeClass("text_readonly"); }
function getAlias(s) { var a = str(s).split("\t"); if (a.length>1 && a[1]!="") return a[1]; return a[0]; }
var htmlEscape = function(str) {
    var ret = trim(str).replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/ /g,"&nbsp;").replace(/\n/g, '\\n');
    return eval('"'+ret.replace(/\&amp\;\#([x]?)([0-9A-Za-z]+)\;/g, '"+toEntity(\"$1\",$2)+"')+'"');
}
var getFormElementByName = function(form, name) { return $(form).find("[name="+name+"]").get(0); }

var ipadHackIdx = 0;
var ipadHackEdits = {};
var ipadHackElem = 0;
var ipadHackKeydowning = 0;
var ipadHackFocusIn = function(elem) {
    if (!isNotPC) return;
    if (ipadHackKeydowning) return;
    var idx = elem.getAttribute("ipad_numhack_swap_tbox_idx");
    if (idx) var numE = ipadHackEdits[idx];
    if (!idx) {
        ipadHackIdx++;
        idx = ipadHackIdx;
        var html =
        '<input type="text" class="text" pattern="[0-9]*" style="display:none" tabIndex="-1"'+
        ' onchange="ipadHackChange(this.value)" onkeydown="ipadHackKeydown(this, event)"'+
        ' onkeyup="ipadHackKeyup(this, event)" onblur="ipadHackFocusOut(this)"'+
        ' />';
        var numE = $(html).get(0);
        var pNode = elem.parentNode;
        pNode.insertBefore(numE, elem);
        ipadHackEdits[idx] = numE;
        numE.value = elem.value;
        numE.style.width = elem.style.width;
        numE.className = elem.className;
        var mlen = int(elem.getAttribute("maxlength"));
        numE.setAttribute("maxlength", mlen);
        numE.setAttribute("navi", elem.getAttribute("navi"));
        numE.setAttribute("navititle", elem.getAttribute("navititle"));
    }
    numE.style.display = "";
    elem.style.display = "none";
    tryFocusByElem(numE);
    ipadHackElem = elem;
    var pattern = ("^"+elem.getAttribute("pattern")+"$").replace("^^", "^").replace("$$","$");
    setTimeout(function(){
        numE.setAttribute("pattern", elem.getAttribute("pattern"));
    }, 100);
}
var ipadHackFocusOut = function(elem) {
    elem.value = ipadHackElem.value;
    elem.style.display = "none";
    if (ipadHackKeydowning) return;
    elem.setAttribute("pattern", "[0-9]*");
    ipadHackElem.style.display = "";
}
var ipadHackChange = function(v) {
    ipadHackElem.value = v;
}
var ipadHackKeyup = function(elem, evt) {
    ipadHackElem.value = elem.value;
}
var ipadHackKeydown = function(elem, evt) {
    ipadHackElem.value = elem.value;
    var wevt = window.event;
    var key = (wevt ? wevt.keyCode || wevt.which : evt.keyCode);
    if (key==9) {
        ipadHackKeydowning = 1;
        ipadHackElem.style.display = "";
        tryFocusByElem(ipadHackElem);
        ipadHackKeydowning = 0;
        elem.setAttribute("pattern", "[0-9]*");
        alert("test");
        return stopEvent();
    }
}


var htmlTBoxBinary = function(str) {
    var ret = trim(str).replace(/\n/g, '\\n').replace(/\"/g, '\\"');
    return eval('"'+ret.replace(/\&\#([x]?)([0-9A-Za-z]+)\;/g, '"+toBinary(\"$1\",$2)+"')+'"');
}
var htmlBinary = function(str) {
//    var ret = trim(str).replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/ /g,"&nbsp;").replace(/\n/g, '\\n').replace(/\"/g, '\\"');
    var ret = trim(str).replace(/&/g,"&amp;").replace(/\n/g, '\\n').replace(/\"/g, '\\"');
//    var ret = trim(str).replace(/\n/g, '\\n').replace(/\"/g, '\\"');
    return eval('"'+ret.replace(/\&amp\;\#([x]?)([0-9A-Za-z]+)\;/g, '"+toBinary(\"$1\",$2)+"')+'"');
}
var toEntity = function(x, n) {
    if (x) {
        var hex = eval("0x"+n);
        if (hex<0xFF) return '&amp;#'+x+n+";";
        return "&#0x"+n+";";
    }
    var ord = eval(n);
    if (ord < 128) return '&amp;#'+n+";";
    return "&#"+n+";";
}
var toBinary = function(x, n) {
    if (x) {
        var hex = eval("0x"+n);
        if (hex<0xFF) return '&amp;#'+x+n+";";
        return cFromCharCode(eval("0x"+n));
    }
    var ord = eval(n);
    if (ord < 128) return '&amp;#'+n+";";
    return cFromCharCode(eval(n));
}
var cFromCharCode = function(){
    var buf = '';
    for ( var i = 0, l = arguments.length; i < l; i++ ) {
        var n = arguments[i];
        if ( n < 0x10000 ) {
            buf += String.fromCharCode(n);
            continue;
        }
        n -= 0x10000;
        buf += String.fromCharCode(
            0xD800 + ( n >> 10 ),
            0xDC00 + ( n & 0x3FF )
        );
    }
    return buf;
};

var setComboValue = function(obj,v, t){
    var o=obj.options;
    for(var i=0; i<o.length; i++) {
        if(o[i].value==v) {
            obj.selectedIndex=i;
            return;
        }
    }
    if (v && !t) {
        o[o.length] = new Option(v);
        obj.selectedIndex = o.length-1;
        return;
    }
    if (v && t) {
        o[o.length] = new Option(t, v);
        obj.selectedIndex = o.length-1;
        return;
    }
    obj.selectedIndex=0;
}
function getClientWidth(){
    if (document.documentElement && document.documentElement.clientWidth) return document.documentElement.clientWidth;
    if (document.body.clientWidth) return document.body.clientWidth;
    if (window.innerWidth) return window.innerWidth;
    return 0;
}
function ymdToJpWeek(ymd) {
    var aYmd = ymdToArray(ymd);
    var dt = new Date(aYmd.yy, aYmd.zmm-1, aYmd.zdd);
    var ary = ["日","月","火","水","木","金","土"];
    return ary[dt.getDay()];
}
function ymdDateAdd(ymd, inc) {
    var yy = substr2(ymd, 0, 4);
    var mm = substr2(ymd, 4, 2);
    var dd = substr2(ymd, 6, 2);
    var dt = new Date(yy, int(mm)-1, int(dd)).getTime();
    var ndt = new Date(dt + (60*60*24*1000) * inc);
    return ndt.getFullYear() + ("00"+(ndt.getMonth()+1)).slice(-2) + ("00"+(ndt.getDate())).slice(-2);
}

function ymdToArray(ymdhm) {
    var yy = substr2(ymdhm, 0, 4);
    var mm = substr2(ymdhm, 4, 2);
    var dd = substr2(ymdhm, 6, 2);
    var hh = substr2(ymdhm, 8, 2);
    var mi = substr2(ymdhm, 10, 2);
    var ss = substr2(ymdhm, 12, 2);
    var ymd = yy + mm + dd;
    var hm = hh + mi;
    var zmm = zeroSuppress(mm);
    var zdd = zeroSuppress(dd);
    var zhh = zeroSuppress(hh);
    var dt = new Date(yy, zmm-1, zdd);
    var ary = ["","月","火","水","木","金","土","日"];
    var weekNumber = dt.getDay();
    if (weekNumber==0) weekNumber = 7;// 月(1)〜日(7)
    var weekJp = ary[weekNumber];
    return {
        yy:yy, mm:mm, dd:dd, hh:hh, mi:mi, ymd:ymd,
        zmm:zmm, zdd:zdd, zhh:zhh,
        weekNumber:weekNumber,
        md_jp : (mm ? mm+"月"+dd+"日" : ""),
        mdw_jp : (mm ? zmm+"月"+dd+"日("+ weekJp+")": ""),
        slash_mdw: (mm ? zmm+"/"+zdd+"("+ weekJp+")": ""),
        slash_ymd: (yy ? yy+"/"+mm+"/"+dd : ""),
        slash_zymd: (yy ? yy+"/"+zmm+"/"+zdd : ""),
        zmd_jp:(zmm ? zmm+"月"+zdd+"日" : ""),
        slash_md_hm:(yy ? yy+"/"+mm+"/"+dd+" "+hh+":"+mi : ""),
        slash_md_hms:(yy ? yy+"/"+mm+"/"+dd+" "+hh+":"+mi+":"+ss : "")
    };
}



function encodeToUncodeEntity(str) {
    var surrogate_1st = 0;
    var out = [];
    for (var i = 0; i < str.length; ++i) {
        var utf16_code = str.charCodeAt(i);
        if (surrogate_1st != 0) {
            if (utf16_code >= 0xDC00 && utf16_code <= 0xDFFF) {
                var surrogate_2nd = utf16_code;
                var unicode_code = (surrogate_1st - 0xD800) * (1 << 10) + (1 << 16) + (surrogate_2nd - 0xDC00);
                out.push("&#"+unicode_code.toString(10).toUpperCase() + ";");
            } else {
                // Malformed surrogate pair ignored.
            }
            surrogate_1st = 0;
        } else if (utf16_code >= 0xD800 && utf16_code <= 0xDBFF) {
            surrogate_1st = utf16_code;
        } else {
            out.push("&#"+utf16_code.toString(10).toUpperCase() + ";");
        }
    }
    return encodeURIComponent(out.join(""));
}


var AJAX_ERROR = "AJAX_ERROR";
var TYPE_JSON = "TYPE_JSON";
function getCustomTrueAjaxResult(msg, type_json, isCheckOnly) {
    var errMsg = "";
    if (msg=="SESSION_NOT_FOUND") errMsg = "画面呼び出しが不正です。（SESSION_NOT_FOUND）";
    else if (msg=="DATABASE_CONNECTION_ERROR") errMsg = "データベース接続に失敗しました。";
    else if (msg=="NO_AUTH_83") errMsg = "機能操作権限がありません。（NO_AUTH_83）";
    else if (msg=="SESSION_EXPIRED") errMsg = "長時間利用されませんでしたのでタイムアウトしました。\n再度ログインしてください。";
    if (errMsg) {
        alert(errMsg);
        showLoginPage(window);
        return AJAX_ERROR;
    }
    if (isCheckOnly) return "";

    if (substr2(msg,0,2)!="ok") {
        if (!msg) alert("通信エラーが発生しました。");
        else alert(msg);
        return AJAX_ERROR;
    }
    if (type_json!=TYPE_JSON) return msg.substring(2);
    try { eval("var ret = "+msg.substring(2)+";"); } catch(ex) {
        ee("dump").innerHTML = htmlEscape(msg);
        alert("通信エラーが発生しました。\n"+msg.substring(2)); return AJAX_ERROR;
    }
    return ret;
}

function isParentEnabling(formId, elem) {
    if (!elem) return true;
    if (elem.disabled) return false;
    if (elem.style.display=="none") return false;
    if (elem.id==formId) return true;
    return isParentEnabling(formId, elem.parentNode);
}
function checkInputError(formId) {
    var ret = 1;
    var errorElem = 0;
    $("#"+formId+" select").each(function(){
        if (!isParentEnabling(formId, this)) this.selectedIndex = 0;
    });
    $("#"+formId+" input[type=checkbox]").each(function(){
        if (!isParentEnabling(formId, this)) this.checked = false;
    });
    $("#"+formId+" input[type=text]").each(function(){
        if (!isParentEnabling(formId, this)) this.value = ""; // 自分または親がdisabledだったり、display=noneだったりすれば、値をカラにする
        var pattern = this.getAttribute("pattern");
        if (!pattern) return;
        pattern = ("^"+pattern+"$").replace("^^", "^").replace("$$","$");
        if (this.value.match(pattern)) return;
        if (!errorElem) errorElem = this;
        ret = 0;
    });
    if (!ret) {
        alert("入力値が正しくない箇所があります。");
        tryFocusByElem(errorElem);
    }
    return ret;
}

function serializeUnicodeEntity(formId) {
    var serials = [];
    $("#"+formId+" input[type=text]").each(function(){ serials.push("uni_entity__"+this.name+"="+encodeToUncodeEntity(this.value)); });
    $("#"+formId+" input[type=hidden]").each(function(){ serials.push("uni_entity__"+this.name+"="+encodeToUncodeEntity(this.value)); });
    $("#"+formId+" input[type=checkbox]").each(function(){ if (this.checked) serials.push("uni_entity__"+this.name+"="+encodeToUncodeEntity(this.value)); });
    $("#"+formId+" input[type=radio]").each(function(){ if (this.checked) serials.push("uni_entity__"+this.name+"="+encodeToUncodeEntity(this.value)); });
    $("#"+formId+" select").each(function(){ serials.push("uni_entity__"+this.name+"="+encodeToUncodeEntity(getComboValue(this))); });
    $("#"+formId+" textarea").each(function(){ serials.push("uni_entity__"+this.name+"="+encodeToUncodeEntity(this.value)); });
    return serials.join("&");
}




var templates = {};
function repeatObjectFieldDispose(formId, namespace) {
    var nsid = formId + "@" + namespace;
    for (var idx=1; idx<=templates[nsid].is_disabled.length; idx++) {
        repeatObjectFieldDel(formId, namespace, idx);
    }
}
function repeatObjectFieldAdd(formId, namespace) {
    var nsid = formId + "@" + namespace;
    var obj = templates[nsid];
    if (!obj) alert("プログラムエラー：テンプレートのnamespace「"+namespace+"」が定義されていません。"+obj);
    var idx = obj.is_disabled.length + 1;
    var js = '"repeatObjectFieldDel(\''+formId+'\', \''+namespace+'\', '+idx+');"';
    var html = obj.tmpl.replace(/TEMPLATE_IDX/g, idx).replace(/{TOGGLE_EVENT}/g, js).replace(/\"\"/g, '"');
    templates[nsid].is_disabled[idx-1] = 0;
    obj.$container.append(html);
    var topNode = ee(templates[nsid].topNodeId+idx);
    $(topNode).find("select").each(function(){
        var opt_group = this.getAttribute("opt_group");
        if (opt_group) {
            if (dfmMstOptions.mstOptions[opt_group]) bindMstOptionsToCombo(this, dfmMstOptions.mstOptions[opt_group]);
            return;
        }
    });
    repeatObjectFieldRename(formId, namespace);
}
function repeatObjectFieldDel(formId, namespace, idx) {
    var nsid = formId + "@" + namespace;
    templates[nsid].is_disabled[idx-1] = (templates[nsid].is_disabled[idx-1]=="" ? "1" : "");
    repeatObjectFieldRename(formId, namespace);
}

// リネーム
function repeatObjectFieldRename(formId, namespace) {
    var nsid = formId + "@" + namespace;
    for (var idx=1; idx<=templates[nsid].is_disabled.length; idx++) {
        if (templates[nsid].is_eliminated[idx-1]) continue;

        var topNode = ee(templates[nsid].topNodeId+idx);
        $(topNode).find("[basename]").each(function(){
            this.removeAttribute("name");
            this.disabled = (templates[nsid].is_disabled[idx-1] ? true : false);
        });
        topNode.style.border = (templates[nsid].is_disabled[idx-1] ? "1px solid #ff0000" : "1px solid #d7f0ff");
        if (templates[nsid].deleteType=="elimination" && templates[nsid].is_disabled[idx-1]) {
            topNode.parentNode.removeChild(topNode);
            templates[nsid].is_eliminated[idx-1] = 1;
        }
    }
    var nameIndex = 0;
    for (var idx=1; idx<=templates[nsid].is_disabled.length; idx++) {
        if (templates[nsid].is_disabled[idx-1]) continue;
        var topNode = ee(templates[nsid].topNodeId+idx);
        nameIndex++;
        $(topNode).find("[basename]").each(function(){
            this.setAttribute("name", namespace + "__" + nameIndex + "__" + this.getAttribute("basename"));
        });
    }
    templates[nsid].fieldCountObj.value = nameIndex;
}
function bindDataToForm(formId, data, ymd) {
    if (!data["ptif_name_disp"]) data["ptif_name_disp"] = ptif_name; // 患者名。事前にバインドしていなければ、グローバル値を固定バインド
    $("#"+formId+"Template .repeat_object_template").each(function(){
        var ns = this.getAttribute("namespace");
        var nsid = formId + "@" + ns;
        templates[nsid] = { $container:0, is_disabled:[], is_eliminated:[] }
        templates[nsid].deleteType = this.getAttribute("delete_type");
        var topNode = $(this).find(":first").get(0);
        topNode.id = formId+"@"+ns+"__topnodeTEMPLATE_IDX";
        topNode.className = "repeat_object";
        topNode.style.border ="1px solid #f6f9ff";
        templates[nsid].topNodeId = formId+"@"+ns+"__topnode";
        templates[nsid].tmpl = this.innerHTML;
        templates[nsid].ymd = (ymd? ymd : "");
    });
    $("#"+formId+" .repeat_object_container").each(function(){
        var namespace = this.getAttribute("namespace");
        var nsid = formId + "@" + namespace;
        var fieldCountName = namespace+"__field_count";
        var fieldCountId = formId+"@"+fieldCountName;
        if (!ee(fieldCountId)) {
            $("#"+formId).append('<input type="hidden" id="'+fieldCountId+'" name="'+fieldCountName+'" />');
        }

        var obj = templates[nsid];
        if (!obj) alert("プログラムエラー：テンプレートのnamespace「"+namespace+"」が定義されていません。");
        var dataCount = int(data[fieldCountName]);
        if (!dataCount) {
            dataCount = 1;
            data[fieldCountName] = 1;
        }
        var rawdata = str(data[namespace]);
        var htmls = [];
        for (var idx=1; idx<=dataCount; idx++) {
            var js = '"repeatObjectFieldDel(\''+formId+'\', \''+namespace+'\', '+idx+');"';
            var html = obj.tmpl.replace(/TEMPLATE_IDX/g, idx).replace(/{TOGGLE_EVENT}/g, js).replace(/\"\"/g, '"');
            htmls.push(html);
            templates[nsid].is_disabled[idx-1] = "";
            templates[nsid].is_eliminated[idx-1] = "";
        }
        $(this).empty();
        $(this).append(htmls.join(""));
        templates[nsid].$container = $(this);
        templates[nsid].fieldCountObj = ee(fieldCountId);
        repeatObjectFieldRename(formId, namespace);
    });

    $("#"+formId+" input[type=text]").each(function(){ this.value = htmlTBoxBinary(data[this.name]); });
    $("#"+formId+" input[type=hidden]").each(function(){ this.value = str(data[this.name]); });
    $("#"+formId+" input[type=radio]").each(function(){ this.checked = (this.value==str(data[this.name])); });
    $("#"+formId+" input[type=checkbox]").each(function(){ this.checked = (this.value==str(data[this.name])); });
    $("#"+formId+" textarea").each(function(){ this.value = htmlBinary(data[this.name]); });
    $("#"+formId+" .literal").each(function(){ this.innerHTML = str(data[this.getAttribute("name")]); });
    $("#"+formId+" select").each(function(){
        var opt_group = this.getAttribute("opt_group");
        if (opt_group) {
            if (dfmMstOptions.mstOptions[opt_group]) bindMstOptionsToCombo(this, dfmMstOptions.mstOptions[opt_group]);
        }
        setComboValue(this, str(data[this.name]));
    });
    $("#"+formId+" .ymd_button").each(function(){
        var nm = this.getAttribute("name").split("@");
        var aYmd = ymdToArray(str(data[nm[0]]));
        var v = aYmd.slash_mdw;
        if (v=="") v = "&nbsp;";
        this.innerHTML = v;
    });
    $("#"+formId+" .hm_button").each(function(){
        var nm = this.getAttribute("name").split("@");
        var hm = str(data[nm[0]]);
        this.innerHTML = substr2(hm,0,2) + ":" + substr2(hm,2,2);
    });
}



function bindMstOptionsToCombo(combo, option_list) {
    if (!option_list) option_list = dfmMstOptions.mstOptions;
    combo.options.length = option_list.length+1;
    combo.options[0] = new Option("","");
    for (var idx=1; idx<=option_list.length; idx++) {
        combo.options[idx] = new Option(option_list[idx-1].opt_name, option_list[idx-1].opt_name+"\t"+option_list[idx-1].opt_alias);
    }
}

var uAgent = navigator.userAgent.toLowerCase();
var isIE = (uAgent.indexOf("msie") != -1);





var tooltipYmdhm = "";
function showOndobanTooltip(event, dayNumber){
    var GRAPH_LEFT = 152; //
    var MOUSE_MARGIN = 20; //
    var isVisibleExist = false;
    var posx = (event.offsetX || event.x || event.layerX);
    var posy = (event.offsetY || event.y || event.layerY);
    var mouseLeft = dayNumber * (ondobanDayWidth+1) + posx + 2;

    tooltipYmdhm = "";
    for (var t in vitalPosData){
        var isVisible = (Math.abs(vitalPosData[t].leftpx - mouseLeft) < 7);
        if (isVisible) { isVisibleExist = true; tooltipYmdhm = vitalPosData[t].ymdhm; }
        ee("tooltip_detail_"+vitalPosData[t].ymdhm).style.display = isVisible ? "" : "none";
    }
    var tooltip = ee("ondoban_tooltip");
    tooltip.style.display = isVisibleExist ? "" : "none";
    if (isVisibleExist){
        tooltip.style.height = "";
        tooltip.style.width = "";
        var ttWidth = (tooltip.offsetWidth || tooltip.clientWidth);
        var halfWidth = ttWidth / 2;
        var ttHeight = (tooltip.offsetHeight || tooltip.clientHeight);
        var l = parseInt(mouseLeft) + GRAPH_LEFT - halfWidth;
        if (l + ttWidth > canvasWidth + GRAPH_LEFT) l = canvasWidth + GRAPH_LEFT - ttWidth;
        var t = parseInt(posy) + MOUSE_MARGIN + 40;
        tooltip.style.left = l+"px";
        tooltip.style.top  = t+"px";
    }
}
function hideOndobanTooltip() {
    if (ee("ondoban_tooltip")) ee("ondoban_tooltip").style.display='none';
    tooltipYmdhm = "";
}



//**************************************************************************************************
// ナビ
//**************************************************************************************************
var gNavi = {
    globalMouseX : 0,
    globalMouseY : 0,
    $tblNavi : 0,
    iframeNavi : 0,
    tdNavi : 0,
    thNaviTitle : 0,
    initialize : function() {
        var self = this;
        var tag =
        '<table cellspacing="0" cellpadding="0" id="tblNavi" onmouseover="gNavi.hide(this)"><tr>' +
        '    <th id="thNaviTitle"></th><td id="tdNavi"></td>' +
        '</tr></table>';
        $("body").append(tag);
        this.$tblNavi = $("#tblNavi");
        this.iframeNavi = ee("iframeNavi");
        this.tdNavi = ee("tdNavi");
        this.thNaviTitle = ee("thNaviTitle");
        $("html").mousemove(function(evt){
            self.globalMouseX = evt.pageX;
            self.globalMouseY = evt.pageY;
        });
        $(document).on('mouseenter', '[navi],[navititle]', function(){ self.show(this); });
        $(document).on('mouseleave', '[navi],[navititle]', function(){ self.hide(); });
    },
    show : function(target){
        var gmsg = trim($(target).attr("navi"));
        var gtitle = trim($(target).attr("navititle"));
        var bGmsg = (gmsg!="");
        var bGtitle = (gtitle!="");
        if (!bGmsg && !bGtitle) return;

        var win = window;
        var scTop = $(window).scrollTop();
        var wh = win.innerHeight;
        if (document.all || window.opera) wh = win.document.documentElement.clientHeight;
        if (!wh) wh = win.document.getElementsByTagName("body")[0].clientHeight;
        if (!wh) wh = document.body.clientHeight;

        if (wh<1) return;

        var ww = window.innerWidth;
        if (!ww && document.documentElement) ww = document.documentElement.clientWidth;
        if (!ww) ww = document.body.clientWidth;
        if (!ww) return;

        this.tdNavi.innerHTML = gmsg.replace(/(\n)/g, "<br />");
        this.tdNavi.style.display = (bGmsg ? "" : "none");
        this.thNaviTitle.innerHTML = trim(gtitle);
        this.thNaviTitle.style.display = (bGtitle ? "" : "none");

        this.$tblNavi.css({top:-1000, width:"auto"});
        this.$tblNavi.show();

        var tblh = this.$tblNavi.get(0).offsetHeight;
        var tblw = this.$tblNavi.get(0).offsetWidth;
        if (tblw > ww - 20) { tblw = ww - 20; this.$tblNavi.css({"width":tblw}); }
        this.$tblNavi.hide();

        var wy = (wh - tblh - 20);
        if (wy <= this.globalMouseY-scTop) wy = this.globalMouseY - scTop - tblh - 30;

        this.$tblNavi.css({top:wy, left:18});

        this.$tblNavi.fadeIn(300);
    },
    hide : function() {
        if (this.$tblNavi) this.$tblNavi.stop(true, true).hide();
    }
};




var dfmYmdAssist = {
    todayYmd : "",
    sijibi : "",
    currentYmd : "",
    mode : "",
    $dialogForm : 0,
    currentY : 0,
    currentM : 0,
    popup : function(mode, currentYmd, sijibi, ymdCallbackFunc, isExhibitClear){
        this.sijibi = sijibi;
        this.mode = mode;
        this.currentYmd = currentYmd;
        this.ymdCallbackFunc = ymdCallbackFunc;

        var now = new Date();
        this.todayYmd = now.getFullYear() + ("0"+(now.getMonth()+1)).slice(-2) + ("0"+now.getDate()).slice(-2);

        var targetYmd = (currentYmd ? currentYmd : (sijibi ? sijibi : this.todayYmd));
        var aYmd0 = ymdToArray(this.todayYmd);
        var aYmd1 = ymdToArray(sijibi);
        var aYmd2 = ymdToArray(targetYmd);
        var mondayYmd = ymdDateAdd(this.todayYmd, -1*(aYmd0.weekNumber-1));
        this.currentY = int(aYmd2.yy);
        this.currentM = int(aYmd2.mm);
        var html = [];
        if (this.mode=="ymd") {
            html.push(
                '<div style="padding:8px 0 10px 0">'+
                (this.sijibi ? '<button type="button" onclick="dfmYmdAssist.selectDay(\''+this.sijibi+'\')">指示日：'+aYmd1.slash_mdw+'</button>' : "")+
                '<button type="button" onclick="dfmYmdAssist.selectDay(\''+this.todayYmd+'\')">本日：'+aYmd0.slash_mdw+'</button>'+
                (isExhibitClear ? "" : '<button type="button" onclick="dfmYmdAssist.selectDay(\'\')">クリア</button></div>')+
                '<hr>'
            );
        }
        if (this.mode=="week") {
            html.push(
            '<div style="padding:8px 0 10px 0"><button type="button" style="width:80px" onclick="dfmYmdAssist.selectDay(\''+mondayYmd+'\')">'+
            '今週</button></div>');
        }
        html.push(
            '<div style="padding-bottom:10px">'+
            '<table cellspacing="0" cellpadding="0"><tr>'+
            '<td class="bottom" style="width:80px"><button type="button" onclick="dfmYmdAssist.moveYM(\'year-\')" style="width:80px">前年≪</button></td>'+
            '<td class="bottom right" style="width:62px"><button type="button" onclick="dfmYmdAssist.moveYM(\'month-\')" class="w60">前月≪</button></td>'+
            '<th class="bottom" style="font-size:16px; letter-spacing:3px; vertical-align:middle;"'+
            ' id="efmYmdAssist_ym"><span style="color:#ae0000">'+this.currentY+'</span>年'+
            ' <span style="color:#ae0000; font-size:24px">'+this.currentM+'</span>月</th>'+
            '<td class="bottom" style="width:62px"><button type="button" onclick="dfmYmdAssist.moveYM(\'month+\')" class="w60">≫次月</button></td>'+
            '<td class="bottom" style="width:80px"><button type="button" onclick="dfmYmdAssist.moveYM(\'year+\')" style="width:80px">≫次年</button></td>'+
            '</tr></table</div>'
        );
        ee("efmYmdAssist").innerHTML = html.join("");
        this.$calendarContainer = $('<div></div>');
        $("#efmYmdAssist").append(this.$calendarContainer);
        this.createCalendar(this.currentY, this.currentM);
        this.$dialogForm = $('#efmYmdAssist').dialog({ title:'日付選択', width:464, height:400 });
    },
    moveYM : function(moveType) {
        if (moveType=="month+") this.currentM++;
        if (moveType=="month-") this.currentM--;
        if (moveType=="year+" || this.currentM>12) this.currentY++;
        if (moveType=="year-" || this.currentM<1) this.currentY--;
        if (this.currentM>12) this.currentM = 1;
        if (this.currentM<1) this.currentM = 12;
        ee("efmYmdAssist_ym").innerHTML =
        '<span style="color:#ae0000">'+this.currentY+'</span>年'+
        ' <span style="color:#ae0000; font-size:24px">'+this.currentM+'</span>月';
        this.createCalendar(this.currentY, this.currentM);
    },
    createCalendar : function(yy, mm) {
        var oneDay = (60*60*24*1000);
        var getumatubi = new Date(yy, mm, 0);
        var ippi = new Date(yy, mm-1, 1);
        var ippiYoubi = ippi.getDay();
        if (ippiYoubi==0) ippiYoubi = 7;

        if (this.mode=="ymd") {
            var html = ['<div class="week">月</div><div class="week">火</div><div class="week">水</div>'+
            '<div class="week">木</div><div class="week">金</div><div class="week saturday">土</div><div class="week sunday">日</div>'];
            var tDate = new Date(ippi - oneDay * ippiYoubi); // １日を含む週よりひとつ前の週の日曜
            for (;;) {
                tDate = new Date(tDate.getTime() + oneDay*1);
                tYoubi = tDate.getDay();
                if (tYoubi==0) tYoubi = 7;

                var tYmd = tDate.getFullYear() + ("00"+(tDate.getMonth()+1)).slice(-2) + ("00"+(tDate.getDate())).slice(-2);

                var cls = '';
                var tMM = tDate.getMonth()+1;
                var md = tDate.getDate();
                if (tMM!=int(mm)) { md = tMM + "/" + md; cls += " pale"; }

                if (tYmd==this.sijibi)   { md += "<br>指示日"; cls += ' sijibi'; }
                else if (tYmd==this.todayYmd) { md += "<br>本日"; cls += ' today'; }
                if (tYmd==this.currentYmd) { cls += ' current'; }

                html.push('<div><a tabIndex="0" class="block1'+cls+'" onclick="dfmYmdAssist.selectDay(\''+tYmd+'\')">'+md+'</a></div>');
                if (tYoubi==7 && tDate >=getumatubi) break;
            }
            this.$calendarContainer.html('<div id="calendarButtonContainer">'+html.join("")+'<div class="clear"></div></div>');
        }
        if (this.mode=="week") {
            var html = [];
            var tMonday = new Date(ippi - oneDay * (ippiYoubi-1)); // １日を含む週の月曜
            var idx = 0;
            for (;;) {
                idx++;
                var tSunday = new Date(tMonday.getTime() + oneDay*6);
                var tMYmd = tMonday.getFullYear() + ("00"+(tMonday.getMonth()+1)).slice(-2) + ("00"+(tMonday.getDate())).slice(-2);
                var tSYmd = tSunday.getFullYear() + ("00"+(tSunday.getMonth()+1)).slice(-2) + ("00"+(tSunday.getDate())).slice(-2);
                var aClass = (tMYmd==this.currentYmd ? 'a_selected' : "");
                var md =
                tMonday.getFullYear()+"/"+(tMonday.getMonth()+1) + "/" + tMonday.getDate() + " 〜 " +
                tSunday.getFullYear()+"/"+ (tSunday.getMonth()+1) + "/" + tSunday.getDate();
                var md2 = "";
                if (tMYmd <= this.todayYmd && this.todayYmd <= tSYmd) md2 = "&nbsp;&nbsp;&nbsp;&nbsp;今週";
                html.push('<li><a class="block2 '+aClass+'" href="javascript:void(0)"');
                html.push(' style="padding-left:4px;" onclick="dfmYmdAssist.selectDay(\''+tMYmd+'\')">');
                html.push('第'+idx+'週&nbsp;&nbsp;&nbsp;<span>'+md+'</span>'+md2+'</a></li>');
                tMonday = new Date(tMonday.getTime() + oneDay*7);
                if (tMonday >=getumatubi) break;
            }
            this.$calendarContainer.html('<ul>'+html.join("")+"</ul>");
        }
    },
    selectDay : function(ymd) {
        this.$dialogForm.dialog("close");
        var aYmd = ymdToArray(ymd);
        var mondayYmd = ymdDateAdd(ymd, -1*(aYmd.weekNumber-1));
        this.ymdCallbackFunc(ymd, aYmd, mondayYmd);
    }
}