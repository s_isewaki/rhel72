<?php
    define("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING", 1);
    require_once("ks_common.php");
    require_once("ks_ajax.php");

    if (!$_REQUEST["ymd"]) $_REQUEST["ymd"] = date("Ymd");
    $ymd = $_REQUEST["ymd"];
    $a_ymd = ymdhm_to_array($ymd);

    $ymd_type = ($_REQUEST["ymd_type"]=="ymd" ? "ymd" : "update_timestamp");


//****************************************************************************************************************
// SOAP/MEMO速報
//****************************************************************************************************************
function get_content_list($ymd_type, $ymd, $bldg_ward) {
    list($bldg_cd, $ward_cd) = explode("_", $bldg_ward);
    $bldg_cd = (int)$bldg_cd;
    if (!$bldg_cd) $bldg_cd = 1;
    $ward_cd = (int)$ward_cd;

    $a_ymd = ymdhm_to_array($ymd);
    $sql_ymd = $a_ymd["yy"]."-".$a_ymd["mm"]."-".$a_ymd["dd"];

    $where = " and hs.ymd = ".dbES($ymd);
    if ($ymd_type == "update_timestamp") $where = " and hs.update_timestamp between '".$sql_ymd." 00:00:00' and '".$sql_ymd." 23:59:59.999999'";

    //=================================================
    // 指定週の入院中患者一覧
    //=================================================
    $sql =
    " select hs.ptif_id, hs.siji_content, hs.ymd, hs.update_emp_name, hs.display_time, hs.content_text, hs.ins_upd_del".
    ",pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm".
    ",ptrm.ptrm_name".
    " from sum_ks_update_history_siji hs".
    " inner join ptifmst pt on (pt.ptif_id = hs.ptif_id)".
    " left outer join sum_ks_mst_pat_attribute pa on (pa.ptif_id = hs.ptif_id)".
    " left outer join sum_ks_mst_ptrm ptrm on (ptrm.bldg_cd = pa.bldg_cd and ptrm.ward_cd = pa.ward_cd and ptrm.ptrm_room_no = pa.ptrm_room_no)".
    " where (siji_content = 'soap' or siji_content = 'daily_bikou')".
    " ". $where.
    " ".($ward_cd ? " and pa.bldg_cd = ".(int)$bldg_cd." and pa.ward_cd = ".(int)$ward_cd : "").
    " order by hs.update_timestamp desc, hs.ptif_id";
    $sel = dbFind($sql);


    $html = array();
    $html[]= '<table cellspacing="0" cellpadding="0" class="sijiban_soap_table">';
    $html[] = '<tr><th width="156">更新日時</th><th width="76">病室</th><th width="110">患者名</th><th width="88">指示日</th>';
    $html[]= '<th width="48">区分</th><th width="36">操作</th><th width="auto">内容</th><th width="110">記載者</th></tr>';
    while ($row = @pg_fetch_array($sel)) {
        $kubun = "SOAP";
        if ($row["siji_content"]=="daily_bikou") $kubun = "MEMO";
        $html[]='<tr>';
        $html[]= '<td>'.hh($row["display_time"]).'</td>';
        $html[]= '<td>'.hh($row["ptrm_name"]).'</td>';
        $html[]= '<td>'.hh($row["ptif_lt_kaj_nm"]." ".$row["ptif_ft_kaj_nm"]).'</td>';
        $html[]= '<td>'.slash_ymd($row["ymd"]).'</td>';
        $html[]= '<td>'.$kubun.'</td>';
        $html[]= '<td>'.$row["ins_upd_del"].'</td>';
        $html[]= '<td>'.str_replace("\n", "<br>", wbr($row["content_text"])).'</td>';
        $html[]= '<td>'.hh($row["update_emp_name"]).'</td>';
        $html[]='</tr>';
    }
    $html[]="</table>";
    if (defined("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING")) {
        return implode("\n",$html);
    }
    echo "ok".implode("\n",$html);
}


?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/start/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" type="text/css" href="css/common.css?v=2">
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.js?v=2"></script>
<script type="text/javascript" src="js/common.js?v=2"></script>
<script type="text/javascript" src="js/showpage.js"></script>
<script type="text/javascript">
    var ymd = "<?=$_REQUEST["ymd"]?>";
    function popupCalendar() {
        var ymdCallbackFunc = function(ymd, aYmd) {
            document.frm.ymd.value = ymd;
            document.frm.submit();
        }
        dfmYmdAssist.popup('ymd', ymd, "", ymdCallbackFunc, 1);
    }
    $(document).on({
        "focus":function(){ $(this).addClass("focussing");},
        "blur": function(){ $(this).removeClass("focussing"); }
    }, 'select')
    function showInstruction(){
        $('#efmDrEmpIdInstruction').dialog({ title:'主治医バイタル一覧　ヘルプ', width:800, height:400 });
    }
    function loaded() {
        if (isNotPC) ee("body").className = "is_not_pc";
        else ee("body").className = "is_pc";
    }
</script>
<title>CoMedix | 看護支援 - SOAP/MEMO速報</title>
</head>
<body onload="loaded()" id="body">





<? //**************************************************************************************************************** ?>
<? // 画面上部 メインメニュー                                                                                         ?>
<? //**************************************************************************************************************** ?>
<div id="header_menu_div">
<form id="frm" name="frm" method="get">
<input type="hidden" name="ymd" value="<?=h($_REQUEST["ymd"])?>" />
<input type="hidden" name="session" value="<?=GG_SESSION_ID?>" />
<table cellspacing="0" cellpadding="0" id="header_menu_table" style="width:100%; height:30px"><tr>
    <td class="weekbtn" style="text-align:center; width:130px; background:url(img/menubtn_on.gif) repeat-x">SOAP/MEMO速報</td>
    <td style="width:auto; padding-left:10px;"><nobr>
        <table cellspacing="0" cellpadding="0" style="width:auto"><tr>
            <td style="vertical-align:middle">
            <select name="ymd_type" onchange="document.frm.submit()">
                <option value="update_timestamp">更新日</option>
                <option value="ymd" <?=($ymd_type=="ymd" ? " selected": "")?>>指示日</option>
            </select>＝<button type="button" class="ymd_button" style="width:110px"
                onclick="popupCalendar()"><?=$a_ymd["slash_ymd"]?></button></td>

            <td style="padding-left:40px; vertical-align:middle">病棟</td>
            <td style="padding-left:6px; vertical-align:middle;">
            <select name="bldg_ward" onchange="document.frm.submit()">
            <option value="all">（すべて）</option>
            <?
            $mst_data = get_mst_bldg_ward_ptrm();
            echo implode("\n",$mst_data["bldg_ward_options"]);
            ?>
            </select>
            <script type="text/javascript">
            setComboValue(document.frm.bldg_ward, "<?=js($_REQUEST["bldg_ward"])?>");
            </script>



            </td>
        </tr></table>
    </nobr></td>
    <td style="width:50px"><a class="menubtn closebtn" href="javascript:void(0)" onclick="window.close();" style="border-left:1px solid #88aaee">×</a></td>
</tr></table>
</form>
</div>







<div id="bed_list_container" style="padding-bottom:20px;">
<?=get_content_list($ymd_type, $ymd, $_REQUEST["bldg_ward"])?>
</div>





<? //**************************************************************************************************************** ?>
<? // 日付選択ダイアログ                                                                                              ?>
<? //**************************************************************************************************************** ?>
<div style="display:none">
<form id="efmYmdAssist" name="efmYmdAssist"></form>
</div>



</body>
</html>
<?
pg_close($con);
