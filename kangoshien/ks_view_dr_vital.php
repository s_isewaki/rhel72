<?php
    define("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING", 1);
    require_once("ks_common.php");
    require_once("ks_ajax.php");

    $dr_emp_id = $_REQUEST["dr_emp_id"];

    if (!$_REQUEST["ymd"]) $_REQUEST["ymd"] = date("Ymd");
    $ymd = $_REQUEST["ymd"];
    $a_ymd = ymdhm_to_array($ymd);
    $yesterday_ymd = get_next_ymd($ymd, "-");
    $ymdhm_to = $ymd."2359"; // 対象日23:59まで
    $ymdhm_fr = $yesterday_ymd."2400"; // 前日24時から

    //========================
    // そもそも主治医かどうか
    //========================
    if ($_REQUEST["init_dr_emp_id"]) {
        if (dbGetOne("select ptif_id from sum_ks_mst_pat_attribute where dr_emp_id = ".dbES($_REQUEST["init_dr_emp_id"])." limit 1")) {
            $dr_emp_id = $_REQUEST["init_dr_emp_id"];
        }
    }

    //========================
    // 対象日の全バイタル記録
    //========================
    $sql =
    " select ptif_id, ymdhm, taion, myakuhakusu, ketsuatsu_bottom, ketsuatsu_upper, spo2_percent, hr_kaisu from sum_ks_vital".
    " where ymdhm between ".dbES($ymdhm_fr)." and ".dbES($ymdhm_to).
    " and (taion <> '' or myakuhakusu <> '' or ketsuatsu_bottom <> '' or ketsuatsu_upper <> '' or spo2_percent <> '' or hr_kaisu <> '')";
    $sel = dbFind($sql);
    $vital_list = array();
    $sql_exist_pt_list = array();
    while($row = pg_fetch_array($sel)){
        $sql_exist_pt_list[dbES($row["ptif_id"])] = 1;
        $ymdhm = to_24hour($row["ymdhm"]);
        $vital_list[$row["ptif_id"]][$ymdhm] = $row; // 一覧表示対象。
    }


    //========================
    // 対象日の全ケア(便取得)
    //========================
    $sql =
    " select ptif_id, ymd, serial_data_out from sum_ks_care_ymd".
    " where ymd = ".dbES($ymd).
    " and (serial_data_out like '%ben__field_count%')".
    " order by ymd";
    $sel = dbFind($sql);
    $spo2_list = array();
    $ben_list = array();
    while($row = pg_fetch_array($sel)){
        $_ymd = $row["ymd"];
        $serial_data_out = unserialize($row["serial_data_out"]);
        $ben_field_count = (int)$serial_data_out["ben__field_count"];
        for ($idx=1; $idx<=$ben_field_count; $idx++) {
            $ben_benryou = $serial_data_out["ben__".$idx."__ben_benryou"];
            $ben_type = get_alias($serial_data_out["ben__".$idx."__ben_type"]);
            $ryou = $serial_data_out["ben__".$idx."__ryou"];
            if ($ben_benryou=="" && $ben_type=="" && $ryou=="") continue;
            $sql_exist_pt_list[dbES($row["ptif_id"])] = 1; // 便のデータ有りで確定
            if ($ben_type=="") $ben_type = "(性状未設定)";
            if ($ben_benryou=="") $ben_benryou = "(便量未設定)";
            $ben_list[$row["ptif_id"]][] = array("ben_benryou"=>$ben_benryou, "ben_type"=>$ben_type, "ryou"=>$ryou); // 一覧表示対象。
        }
    }
    if (!count($sql_exist_pt_list)) $sql_exist_pt_list["''"]= 1;



    //========================
    // 指定主治医の患者一覧（対象日に入院中のもののみ）：一覧表示対象
    //========================
    $sql =
    " select nt.ptif_id, pt.ptif_id, pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm, mpt.ptrm_name, mpa.dr_emp_id".
    " from (".
    "     select ptif_id from sum_ks_mst_pat_nyuin_taiin".
    "     where (".
    "         ( nyuin_ymd is not null and nyuin_ymd <> '' and nyuin_ymd <= ".dbES($ymd).")".
    "         and ( taiin_ymd is null or taiin_ymd = '' or taiin_ymd >= ".dbES($ymd).")".
    "     ) or (".
    "         ptif_id in (".implode(",", array_keys($sql_exist_pt_list)).")".
    "     )".
    "     group by ptif_id".
    " ) nt".
    " inner join ptifmst pt on (pt.ptif_id = nt.ptif_id)".
    " inner join sum_ks_mst_pat_attribute mpa on (".
    "     mpa.ptif_id = nt.ptif_id".
    " )".
    " left outer join sum_ks_mst_ptrm mpt on ( mpa.bldg_cd = mpt.bldg_cd and mpa.ward_cd = mpt.ward_cd and mpa.ptrm_room_no = mpt.ptrm_room_no )".
    " order by mpa.bldg_cd, mpa.ward_cd, mpt.ptrm_name, pt.ptif_lt_kana_nm, pt.ptif_ft_kana_nm";
    $sel = dbFind($sql);
    $disp_ptif_list = array();
    $nyuin_ptif_dr_list = array();
    while($row = pg_fetch_array($sel)){
        if ($row["dr_emp_id"]==$dr_emp_id) $disp_ptif_list[$row["ptif_id"]] = $row; // 入院中。表示対象。
        $nyuin_ptif_dr_list[$row["dr_emp_id"]][$row["ptif_id"]] = 1;
    }



    //========================
    // 主治医の全患者ループ
    //========================
    $data_html = array();
    foreach($disp_ptif_list as $ptif_id => $row) {
        $ptif_id = $row["ptif_id"];
        $vital_data = $vital_list[$ptif_id];
        $ary_taion = array();
        $ary_myaku = array();
        $ary_ketsu = array();
        $ary_spo2  = array();
        $ary_hr = array();
        if (is_array($vital_data)) {
	        krsort($vital_data);
            foreach ($vital_data as $ymdhm => $data) {
                $hh = substr($ymdhm, 8, 2);
                if ($data["taion"]!="") $ary_taion[] = hh($hh."時 ".$data["taion"]);
                if ($data["myakuhakusu"]!="") $ary_myaku[] = hh($hh."時 ".$data["myakuhakusu"]);
                if ($data["ketsuatsu_bottom"]!="" || $data["ketsuatsu_upper"]) {
                    $ary_ketsu[] = hh($hh."時 ".$data["ketsuatsu_upper"]."/".$data["ketsuatsu_bottom"]);
                }
                if ($data["spo2_percent"]!="") $ary_spo2[]= hh($hh."時 ".$data["spo2_percent"]."％");
                if ($data["hr_kaisu"]!="") $ary_hr[]= hh($hh."時 ".$data["hr_kaisu"]."％");
            }
        }

        $ben_data = $ben_list[$ptif_id];
        $ary_ben = array();
        if (is_array($ben_data)) {
            foreach ($ben_data as $data) {
                $ary_ben[] = $data["ben_benryou"]."／".$data["ben_type"]."／".$data["ryou"].($data["ryou"]!="" ? "g":"");
            }
        }

        if (count($ary_taion)) $ary_taion[0] = '<span class="topred" title="最新データです">'.$ary_taion[0].'</span>';
        if (count($ary_myaku)) $ary_myaku[0] = '<span class="topred" title="最新データです">'.$ary_myaku[0].'</span>';
        if (count($ary_ketsu)) $ary_ketsu[0] = '<span class="topred" title="最新データです">'.$ary_ketsu[0].'</span>';
        if (count($ary_spo2))  $ary_spo2[0]  = '<span class="topred" title="最新データです">'.$ary_spo2[0]. '</span>';
        if (count($ary_ben))   $ary_ben[0]   = '<span class="topred" title="最新データです">'.$ary_ben[0].  '</span>';


        $data_html[]= '<tr>';
        // 病室
        $data_html[]= '<td>'.hh($row["ptrm_name"]).(@$row["ptrm_name"]=="" ? '<span style="color:red">（未割当）</span>':"").'</td>';
        // 患者氏名
        $data_html[]= '<td><a href="javascript:void(0)" class="block2" onclick="tryOpenerChangeView(\''.hh($ptif_id).'\')">'.hh($row["ptif_lt_kaj_nm"]." ".$row["ptif_ft_kaj_nm"]).'</a></td>';
        // 体温
        $data_html[]= '<td>'.implode("<br>", $ary_taion).'</td>';
        // 脈拍数
        $data_html[]= '<td>'.implode("<br>", $ary_myaku).'</td>';
        // 血圧
        $data_html[]= '<td>'.implode("<br>", $ary_ketsu).'</td>';
        // SPO2
        $data_html[]= '<td>'.implode("<br>", $ary_spo2).'</td>';
        // 便
        $data_html[]= '<td>'.implode("<br>", $ary_ben).'</td>';
        $data_html[]= '</tr>';
    }




    //========================
    // データが存在する患者の主治医
    //========================
    $db_emp_options = array();
    $sql =
    " select emp.emp_id, emp.emp_lt_nm || ' ' || emp.emp_ft_nm as emp_nm, mpa.dr_emp_id, mpa2.ptif_id_count".
    " from empmst emp".
    " left outer join jobmst job on (".
    "     job.job_id = emp.emp_job".
    "     and (job.job_nm = '医師' or job.job_nm = '常勤医師' or job.job_nm = '非常勤医師') and job_del_flg = 'f'".
    " )".
    " left outer join (".
    "     select dr_emp_id from sum_ks_mst_pat_attribute where ptif_id in (".implode(",", array_keys($sql_exist_pt_list)).")".
    " ) mpa on (mpa.dr_emp_id = emp.emp_id)".
    " left outer join (".
    "     select dr_emp_id, count(ptif_id) as ptif_id_count from sum_ks_mst_pat_attribute group by dr_emp_id".
    " ) mpa2 on (mpa2.dr_emp_id = emp.emp_id)".
    " where ptif_id_count > 0".
    " and (".
    "     (".
    "         job.job_id is not null".
    "         and ( emp.emp_retire is null or emp.emp_retire = '' or emp.emp_retire > '".date("Ymd")."')".
    "     ) or (".
    "         mpa.dr_emp_id is not null".
    "     )".
    " )".
    " group by emp.emp_id, emp.emp_lt_nm, emp.emp_ft_nm, mpa.dr_emp_id, mpa2.ptif_id_count".
    " order by emp_nm";
    $sel = dbFind($sql);

    $db_emp_options[]= '<option value="">≪未割当≫　　　（入院中:'.count($nyuin_ptif_dr_list[""]).'名）</option>';

    while($row = pg_fetch_array($sel)){
        $opt =
        '<option value="'.hh($row["emp_id"]).'"'.
        ($row["emp_id"]==$dr_emp_id?" selected":"").
        '>'.hh($row["emp_nm"]).
        //"　　　（担当患者:".$row["ptif_id_count"]."名/うち入院中:".count($nyuin_ptif_dr_list[$row["emp_id"]])."名）".
        "　　　（入院中:".count($nyuin_ptif_dr_list[$row["emp_id"]])."名）".
        '</option>';
        $db_emp_options[]=$opt;
    }
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/start/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" type="text/css" href="css/common.css?v=2">
<style type="text/css">
span.topred { padding:0; color:#ff0000 }
</style>
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.js?v=2"></script>
<script type="text/javascript" src="js/common.js?v=2"></script>
<script type="text/javascript" src="js/showpage.js"></script>
<script type="text/javascript">
    var ymd = "<?=$_REQUEST["ymd"]?>";
    function popupCalendar() {
        var ymdCallbackFunc = function(ymd, aYmd) {
            document.frm.ymd.value = ymd;
            document.frm.submit();
        }
        dfmYmdAssist.popup('ymd', ymd, "", ymdCallbackFunc, 1);
    }
    $(document).on({
        "focus":function(){ $(this).addClass("focussing");},
        "blur": function(){ $(this).removeClass("focussing"); }
    }, 'select')
    function showInstruction(){
        $('#efmDrEmpIdInstruction').dialog({ title:'主治医バイタル一覧　ヘルプ', width:800, height:400 });
    }
    function tryOpenerChangeView(ptif_id) {
		if (!opener) { alert("操作できません。(1)"); return; }
		if (!opener.dfmPtifDetail) { alert("操作できません。(2)"); return; }
		if (!opener.dfmPtifDetail.selectPtifAndShowOndoban) { alert("操作できません。(3)"); return; }
		opener.dfmPtifDetail.selectPtifAndShowOndoban(ptif_id);
		window.close();
	}
    function loaded() {
        if (isNotPC) ee("body").className = "is_not_pc";
        else ee("body").className = "is_pc";
    }
</script>
<title>CoMedix | 看護支援 - 主治医バイタル一覧</title>
</head>
<body onload="loaded()" id="body">




<? //**************************************************************************************************************** ?>
<? // 画面上部 メインメニュー                                                                                         ?>
<? //**************************************************************************************************************** ?>
<div id="header_menu_div">
<form id="frm" name="frm" method="get">
<input type="hidden" name="ymd" value="<?=h($_REQUEST["ymd"])?>" />
<input type="hidden" name="session" value="<?=GG_SESSION_ID?>" />
<table cellspacing="0" cellpadding="0" id="header_menu_table" style="width:100%; height:30px"><tr>
    <td class="weekbtn" style="text-align:center; width:150px; background:url(img/menubtn_on.gif) repeat-x">主治医バイタル一覧</td>
    <td style="width:auto; padding-left:10px;"><nobr>
        <table cellspacing="0" cellpadding="0" style="width:auto"><tr>
            <td style="vertical-align:middle">対象日 <button type="button" class="ymd_button"
                onclick="popupCalendar()"><?=$a_ymd["slash_mdw"]?></button></td>
            <td style="padding-left:10px; vertical-align:middle">主治医</td>
            <td style="padding-left:6px; vertical-align:middle;">
            <select name="dr_emp_id" onchange="style.color=options[selectedIndex].style.color;document.frm.submit()">
            <?= implode("\n", $db_emp_options)?>
            </select>
            </td>
        </tr></table>
    </nobr></td>
    <td style="width:50px"><a class="menubtn menubtn50" href="javascript:void(0)" onclick="showInstruction()"
         style="border-left:1px solid #88aaee">ヘルプ</a></td>
    <td style="width:50px"><a class="menubtn closebtn" href="javascript:void(0)" onclick="window.close();">×</a></td>
</tr></table>
</form>
</div>







<div id="bed_list_container" style="padding-bottom:20px">
<table cellspacing="0" cellpadding="0" class="sijiban_soap_table">
<tr>
    <th width="10%">病室</th>
    <th width="18%">患者氏名</th>
    <th width="11%">体温</th>
    <th width="11%">脈拍数</th>
    <th width="11%">血圧</th>
    <th width="11%">SpO2</th>
    <th width="28%">便</th>
</tr>
<?=implode("\n", $data_html)?>
</table>
</div>




<div style="display:none">


<? //**************************************************************************************************************** ?>
<? // 日付選択ダイアログ                                                                                              ?>
<? //**************************************************************************************************************** ?>
<form id="efmYmdAssist" name="efmYmdAssist"></form>


<? //**************************************************************************************************************** ?>
<? // ヘルプ                                                                                                          ?>
<? //**************************************************************************************************************** ?>
<form id="efmDrEmpIdInstruction">
<div style="padding-top:10px">この画面は、「対象日」に入院中の患者のバイタル測定値を、「主治医」別で表示します。</div>
<hr>

<div style="padding-top:10px">◆「対象日」は24時間制です。</div>
<div style="padding-left:20px">※対象日の「前日24時以降」のデータが含まれます。</div>
<div style="padding-left:20px">※対象日の「当日24時以降」のデータは含まれません。翌日分を参照してください。</div>
<div style="padding-left:20px">※「便」については画面登録時の「指示日」に依存します。</div>

<div style="padding-top:10px">◆「主治医」プルダウンは、「対象日／患者の入退院／主治医の退職」を問わず、</div>
<div style="padding-left:20px">患者の主治医として登録されている職員が含まれます。</div>

<div style="padding-top:10px">◆「一覧表」には、対象日に入院中の患者が、「体温〜便」データ登録の有無に関わらず表示されます。</div>
<div style="padding-left:20px">但し、万一入院中でなくとも、「体温〜便」いずれかのデータ登録がある場合は表示されます。</div>

<div style="padding-top:10px">◆通常は、「主治医」プルダウンの「うち入院中」人数と、「一覧表」の行数が等しくなります。</div>
<div style="padding-left:20px">但し、入院中でなくとも「体温〜便」データ登録済患者が存在すれば、そのぶん「一覧表」の行数が多くなります。</div>

<div style="padding-top:10px">◆一覧表の各バイタル値は時刻順で表示されます。表示の際、分数は省略されます。</div>
<div style="padding-left:20px">便については登録順（≒時刻順）に表示されます。量/性状/グラム量のいずれかが入力されていれば表示されます。</div>

</form>




</div>


</body>
</html>
<?
pg_close($con);
