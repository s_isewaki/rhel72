<?
//====================================================================
// デファイン（パート１）
//====================================================================
define("GG_CS_EUCJP", (phpversion() < '5.2.1' ? "eucJP-win" : "CP51932"));


define("NIKKIN_START_HHMI","0830");
define("NIKKIN_END_HHMI","1730");
define("JUNYA_START_HHMI","1630");
define("JUNYA_END_HHMI","2530");
define("SINYA_START_HHMI","2400");
define("SINYA_END_HHMI","3600");


// ケア名。ちなみにこの並び順は、「ケア記入履歴検索」でのソート順になっている。よって温度板画面での出現順にしておくのが望ましい。
$care_content_names = array(
    "risyo_riyu"=>"離床理由",
    "vital"=>"バイタル", "hr_spo2"=>"バイタル", // hr_spo2（バイタル）は廃止統合された。
    "sanso"=>"酸素吸入", "kokyuki"=>"人工呼吸器", "kouseizai"=>"抗生剤",
	"in"=>"食事/IN", "hoeki"=>"補液", "insulin"=>"インスリン", "out"=>"OUT",
	"kensanado"=>"検査等", "kansatu"=>"観察項目", "sign"=>"サイン"
);
// 指示名。ちなみにこの並び順は、「指示記入履歴検索」でのソート順になっている。よって温度板画面での出現順にしておくのが望ましい。
$siji_content_names = array(
    "freeword"=>"フリーワード１", "free2word"=>"フリーワード２",
    "keikan"=>"食事（経管）", "keikou"=>"食事（経口）", "keikei"=>"食事（捕食）",
    "insulin_fix"=>"インスリン固定投与", "insulin_sr"=>"インスリン食事", "bsscale"=>"BSスケール",
	"gezai"=>"下剤", "chusya"=>"注射処方等"
);

$care_kensa_names = array(
    "cbc"=>"CBC", "cpr"=>"CPR", "wbc"=>"WBC分画", "nyou"=>"尿一般沈渣", "seika"=>"生化学", "baiyou"=>"培養", "gus"=>"血ガス", "xray"=>"X線", "ct"=>"CT"
);

$marumoji_list = array("","��","��","��","��","��","��","��","��","��","��");


$opt_groups = array(
    "gezai"                =>"指示：下剤／ケア：検査等：下剤",
    "kouseizai"            =>"ケア：抗生剤",
    "insulin_bs"           =>"インスリン薬剤(BSスケール、食事指示量)",
    "sanso_kigu"           =>"ケア：酸素：酸素吸入器具",
    "sanso_noudo_unit"     =>"ケア：酸素：酸素濃度単位",
    "hosyoku_timing"       =>"ケア：食事・IN：補食タイミング",
    "hosyoku"              =>"ケア：食事・IN：補食",
    "hosyoku_ryou"         =>"ケア：食事・IN：捕食摂取量",
    "nyou_more"            =>"ケア：OUT：尿：漏れ",
    "ben_benryou"          =>"ケア：OUT：便：量",
    "ben_type"             =>"ケア：OUT：便：性状",
    "haieki"               =>"ケア：OUT：排液：種類",
    "haieki_more"          =>"ケア：OUT：排液：漏れ",
    "zayaku"               =>"ケア：検査等：座薬",
    "kensa"                =>"ケア：検査等：検査項目",
    "kansatu"              =>"ケア：観察項目",
    "keikan_when"          =>"指示：食事(経管)：拡張時間帯",
    "eiyouzai"             =>"指示：食事(経管)：栄養剤",
    "enbun"                =>"指示：食事(経管)：塩分追加",
    "syusyoku"             =>"指示：食事(経口)：主食形態",
    "fukusyoku"            =>"指示：食事(経口)：副食形態",
    "syokusyu"             =>"指示：食事(経口)：食種",
    "oyatsu"               =>"指示：食事(補食)：補食",
    "insulin_fix_med_name" =>"指示：インスリン固定：薬剤",
    "insulin_fix_timing"   =>"指示：インスリン固定：食前中後タイミング",
    "insulin_fix_when"     =>"指示：インスリン固定：拡張時間帯",
    "sessyu_ryou"          =>"指示：インスリン食事指示量：摂取量",
    "risyo_riyu"           =>"離床理由",
    "siji_az"              =>"" // 名前を空欄にした場合は、薬剤・選択肢マスタ上は非取扱とする。つまり画面メンテ不可。データは直接DBへ登録のこと。
);

define("DEV_MODE", 0);







//====================================================================
// ユーティリティ
//====================================================================

//--------------------------------------------------------------------
// ymdhmのうち、hm部分が24時以上なら、翌日の23:59以下に換算した時刻を返す
//--------------------------------------------------------------------
function to_24hour($ymdhm) {
    $hh = substr($ymdhm,8,2);
    if ($hh < 24) return $ymdhm;
    return get_next_ymd(substr($ymdhm,0,8), "+") . sprintf("%02d", $hh-24) . substr($ymdhm,10,2);
}
//--------------------------------------------------------------------
// ymdhmのうち、hm部分が24時未満なら、前日の23:59以下に換算した時刻を返す
//--------------------------------------------------------------------
function to_48hour($ymdhm) {
    $hh = substr($ymdhm,8,2);
    if ($hh >= 24) return $ymdhm;
    return get_next_ymd(substr($ymdhm,0,8), "-") . sprintf("%02d", $hh+24) . substr($ymdhm,10,2);
}
//--------------------------------------------------------------------
// PDF印刷用 UTF16BE変換
//--------------------------------------------------------------------
function utf16($s) {
    $str = mb_convert_encoding($s, "SJIS-win", "EUC-JP");
    $str = mb_convert_encoding($str, "HTML-ENTITIES", "SJIS-win");
    return mb_convert_encoding($str, "UTF-16BE", "HTML-ENTITIES");
}
//--------------------------------------------------------------------
// CSV出力用 Shift_JIS変換
//--------------------------------------------------------------------
function sjis($s) {
    $str = mb_convert_encoding($s, "SJIS-win", "EUC-JP");
    $str = mb_convert_encoding($str, "HTML-ENTITIES", "SJIS-win");
    return mb_convert_encoding($str, "SJIS-win", "HTML-ENTITIES");
}
//--------------------------------------------------------------------
// nl2brの別バージョン。
// nl2brだと、「\n」は「\n」＋「<br />」に変換される。\nを無くすための関数。
//--------------------------------------------------------------------
function nl2br2($var) {
    return preg_replace("/\n/","<br />", $var);
}
//--------------------------------------------------------------------
// about_comedixのh()のラッパー関数。文字列をHTMLエスケープするが、
// そのうち数値参照文字については、EUCだと物理的に完全エスケープは不可能。いささか小細工を含んでいる。
//
// たとえば非EUC「はしご�癲廚鬟罅璽兇�入力したとき、ブラウザは、「&#39641;」として扱い、実際にPOST送信もする。しかし、
// ユーザが「はしご�癲廚鯑�力したのか、実は「&#39641;」のとおりに入力したのか、それを判断することはできない。
//
// よって、数値参照文字のまま、エスケープしないようにする、なのだが、それはそれで問題があって、
// 1) 「&#39641;」 は、はしご�發任△襦�「&#39641;」このまま入力した、とは「考えにくい」。よって、「&」→「&amp;」にエスケープしないでおく。
// 2)「&#9;」は、タブ文字である。ブラウザに渡せばタブ文字として認識されるが、普通にタブ文字でなく、わざわざこの表現で入力されることは、基本無い。
// 3)「&#0;」は、文字ではない。ブラウザに渡せば問題が発生する可能性もある。普通に「&」→「&amp;」にすべき。
// 上記から、
// ※ASCII内の文字で無い制御文字コードは出力しないようにする。
// ※ASCII以上なら、もし非文字であっても、それは単に「フォントが無い」という解釈が可能となる。
// ※１文字コードずつ判断するのは面倒なので、ASCII以下の文字なら、それは数値参照文字でなく、ただの文字列なのさ、ということにする。
//--------------------------------------------------------------------
function hh($var) {
    // 文字列をh()でエスケープするのだが、
    // 「&#NNNN;」または「&#xNNNN;」形式の部分については、そこだけ抜き取って、
    // to_entity()関数にその部分だけエスケープを任せる。
    return preg_replace_callback("/\&amp\;\#([xX]?)([0-9A-Fa-f]+);/","to_entity", h($var));
}

//--------------------------------------------------------------------
// 数値文字参照形式を整形する。
// 先頭に「&#」をつけて、末尾に";"をつけて返したい。
// $matchesは要素２の配列。第３要素は数値。第２要素は、エックスか、空値。
// 数値参照文字であっても、非文字（バイナリデータか制御文字）をブラウザに渡すのは危険なため、
// ただの文字列として返す。
//--------------------------------------------------------------------
function to_entity($matches) {
    $x = $matches[1]; // 第２要素：エックスか、空値
    $n = $matches[2]; // 第３要素：数値
    if ($x) { // エックスがある。つまり16進数である。
        if ($n<0xFF) return '&amp;#'.$x.$n.";"; // 数値部分が0xFF以下。つまりASCIIか、非文字。これはエスケープして普通の文字列として返す。
        return "&#x".$n.";"; // ASCII以上。数値参照文字形式（&#x...;）にして返す。
    }
    // エックスが無い場合
    if ($n < 128) return '&amp;#'.$n.";"; // 数値部分が0xFF以下。つまりASCIIか、非文字。これはエスケープして普通の文字列として返す。
    return "&#".$n.";"; // ASCII以上。数値参照文字形式（&#x...;）にして返す。
}
//--------------------------------------------------------------------
// ワードブレーク関数
// 半角文字が英単語のように改行されないのを防ぐため、
// 文字列を1文字ずつ分断して、<wbr>でつなぐ。
// しかし昨今では、 <wbr>では頑固に改行しないままのブラウザもある。よって「&#8203;」でつなぐようにする。
// （しかし「&#8203;」では、余分な文字間スペースが発生するレガシーブラウザもある。）
//--------------------------------------------------------------------
function wbr($str, $s2=""){
    $str = trim($str);
    $s2 = trim($s2);
    if (!mb_strlen($str) && !mb_strlen($s2)) return "";
    $out = array();
    $matches = array();
    $dmy = array("","","");
    while (strlen($str)) {
        if ($str[0]=="&" && preg_match("/^\&\#[xX]?[0-9A-Fa-f]+;/", $str, $matches)==1) {
            $len = strlen($matches[0]);
            $str = substr($str, $len);
            $mat = substr($matches[0], 2, strlen($matches[0])-3);
            $dmy[1] = ($mat[0]=="x" || $mat[0]=="X" ? "x" : "");
            if ($dmy[1]) $mat = substr($mat, 1);
            $dmy[2] = $mat;
            $out[] = to_entity($dmy);
        } else {
            $out[] = hh(mb_substr($str, 0, 1));
            $str = mb_substr($str, 1);
        }
    }
    return implode("&#8203;", $out).$s2;
}
//--------------------------------------------------------------------
// 配列をワードブレークマークアップでつなぐ。
// 「&#8203;」と「<wbr>」のダブルパンチにする。
//--------------------------------------------------------------------
function ary_wbr($ary) {
    return implode("&#8203;<wbr>",$ary);
}
//--------------------------------------------------------------------
// スラッシュつき年月日にする
//--------------------------------------------------------------------
function slash_ymd($yyyymmdd){
    if (strlen($yyyymmdd)!=8) return "";
    if (date("Ymd", strtotime($yyyymmdd)) != $yyyymmdd) return "";
    return substr($yyyymmdd,0,4)."/".substr($yyyymmdd,4,2)."/".substr($yyyymmdd,6,2);
}

//--------------------------------------------------------------------
// 指定年月日の漢字曜日を返す
//--------------------------------------------------------------------
function week_jp($yyyymmdd) {
    $ymd = slash_ymd($yyyymmdd);
    if (!$ymd) return "";
    $weeks = array("日", "月", "火", "水", "木", "金", "土");
    $week = date("w", strtotime($ymd));
    return $weeks[$week];
}
//--------------------------------------------------------------------
// 指定「年月日時分」の別表現をいろいろ作る
//--------------------------------------------------------------------
function ymdhm_to_array($ymdhm) {
    $yy = substr($ymdhm, 0, 4);
    $mm = substr($ymdhm, 4, 2);
    $dd = substr($ymdhm, 6, 2);
    $hh = substr($ymdhm, 8, 2);
    $mi = substr($ymdhm, 10, 2);
    $ymd = $yy.$mm.$dd;
    $hm = $hh.$mi;
    $zmm = ltrim($mm, "0");
    $zdd = ltrim($dd, "0");
    $zhh = ltrim($hh, "0"); if ($zhh=="") $zhh = "0";
    $weeks = array("日", "月", "火", "水", "木", "金", "土");
    $week_jp = "";
    if ($ymd) {
        $week = date("w", strtotime($ymd));
        $week_jp = $weeks[$week];
    }
    return array(
        "yy"=>$yy, "mm"=>$mm, "dd"=>$dd, "hh"=>$hh, "mi"=>$mi, "ymd"=>$yy.$mm.$dd,
        "zmm"=>$zmm, "zdd"=>$zdd, "zhh"=>$zhh,
        "ymdhm_en"=>$yy."/".$zmm."/".$zdd." ".$zhh.":".$mi,
        "slash_md" => $zmm."/".$zdd,
        "slash_mdw" => $zmm."/".$zdd."(".$week_jp.")",
        "slash_ymd"=>$yy."/".$zmm."/".$zdd, "hm"=>$hh.$mi, "hhmi"=>$hh.":".$mi, "zhhmi"=>$zhh.":".$mi
    );
}
//--------------------------------------------------------------------
// 連想配列をJSONへ変換
// 外部LIBは細かい変換仕様が不明瞭かつ口にあわないし、
// バージョンUPによって挙動が変わる可能性や懸念は排除したい。あまり利用したくない。
// やりたいことは些細なことなので、自作定義。
//--------------------------------------------------------------------
function to_json($var, $is_recursive=0) {
    if (gettype($var) == 'array') {
        // 連想配列の場合
        if (is_array($var) && count($var) && (array_keys($var) !== range(0, sizeof($var) - 1))) {
            $properties = array();
            foreach ($var as $k => $v) $properties[] = $k .":".to_json($v, 1);
            return '{' . join(',', $properties) . '}';
        }
        // 通常の配列の場合
        $properties = array();
        foreach ($var as $v) $properties[] = to_json($v, 1);

        return '[' . join(',', $properties) . ']';
    }
    // それ以外なら、文字列として処理
    if (!$is_recursive && $var=="") return "{}";
    return '"' . js($var) . '"';
}

//--------------------------------------------------------------------
// JavaScript構文としてエスケープ
// ダブルクォート、改行文字、バックスラッシュの３つでＯＫ
//--------------------------------------------------------------------
function js($s) {
    return mb_ereg_replace('"', '\"', mb_ereg_replace("\r", '', mb_ereg_replace("\n", "\\n", mb_ereg_replace("\\\\", '\\\\', $s))));
}
//--------------------------------------------------------------------
// 薬剤・選択肢マスタデータを、指示やケアダイアログ画面で、プルダウンで選んだとき、
// DBデータには「正式名」＋タブ文字＋「短縮名(エイリアス)」として保存されている。
// 短縮名部分を抜き取って返す。もし短縮名がなければ、正式名を返す。
//--------------------------------------------------------------------
function get_alias($s) {
    $a = explode("\t", $s);
    if (@$a[1]!="") return $a[1];
    return $a[0];
}
//--------------------------------------------------------------------
// 指定年月日を含む週の月曜日を求めて、月曜から日曜までの７要素日付を含む配列を返す。
//--------------------------------------------------------------------
function create_week_ymd_list($ymd) {
    $ymd = slash_ymd($ymd);
    if (!$ymd) $ymd = date("Y/m/d");
    $week = date("w", strtotime($ymd));
    if (!$week) $week = 7;
    $monday_ymd = date("Ymd", strtotime($ymd." -".($week-1)." day"));
    // 検索対象日付がない場合は「次の日曜日」を「一番右列」に設定
    $week_ymd_list = array();
    for ($idx=0; $idx<=6; $idx++) {
        $week_ymd_list[] = date("Ymd", strtotime($monday_ymd." + ".$idx." day")); // 月から日曜
    }
    return $week_ymd_list;
}
//--------------------------------------------------------------------
// 指定日の翌日のYYYYMMDDを返す。第二引数にマイナス記号を指定すると、指定日の前日YYYYMMDDを返す。
//--------------------------------------------------------------------
function get_next_ymd($ymd, $plus_or_minus = "+") {
    return date("Ymd", strtotime(slash_ymd($ymd)." ".$plus_or_minus."1 day"));
}
//--------------------------------------------------------------------
// 入力バリデーション用。空値または数値ならTRUE
//--------------------------------------------------------------------
function is_empty_or_number($s) {
    if ($s=="") return true;
    return is_number($s);
}
//--------------------------------------------------------------------
// 入力バリデーション用。数値ならTRUE
//--------------------------------------------------------------------
function is_number($s) {
    return preg_match("/^[0-9]+$/", $s);
}

//--------------------------------------------------------------------
// 患者名検索用SQL作成
//--------------------------------------------------------------------
function make_ptif_name_sql($ptif_name_find) {
	if ($ptif_name_find=="") return "";
	$ary = explode(" ", $ptif_name_find);
	$tsql = array();
	foreach ($ary as $keywd) {
		if (trim($keywd)=="") continue;
		$loop++;
		$tsql[]=
		" (  ptif_lt_kana_nm like " .dbES("%".$keywd."%").
		"    or ptif_ft_kana_nm like ".dbES("%".$keywd."%").
		"    or ptif_lt_kaj_nm like ".dbES("%".$keywd."%").
		"    or ptif_ft_kaj_nm like ".dbES("%".$keywd."%").
		" )";
		if (count($tsql)>=3) break;
	}
	if (!count($tsql)) return "";
	return " and ". implode(" and ", $tsql);
}

//--------------------------------------------------------------------
// 部分HTML作成。画像８個を並べて表示する暗灰色部分。$size=mなら大きめ、$size=sなら、小さめ。
//--------------------------------------------------------------------
function create_picture_img_tag($ptif_id, $siji_picture_names, $size) {

    $file_base_url = GG_PICTURE_FOLDER."/".$ptif_id."/";
    $file_dir = GG_BASE_FOLDER."/".$file_base_url;

    $picture_list = array();
    $cur_pictures = explode(",", $siji_picture_names);
    foreach ($cur_pictures as $org_file_name) {

        if ($size=="m") {
            $mid_file_name = substr($org_file_name, 0, 24)."m".substr($org_file_name,25);
            if (!file_exists($file_dir.$mid_file_name)) continue;
            $iinfo = getimagesize($file_dir.$mid_file_name);
            $fw = $iinfo[0]; //横幅
            $fh = $iinfo[1]; //縦幅
            $leftpos = (int)((130 - $fw) / 2);
            $toppos =  26 + (int)((130 - $fh) / 2);
            $picture_list[] =
            '<div class="picture_m_container">'.
            '<a href="'.$file_base_url.$org_file_name.'" onclick="window.open(\''.$file_base_url.$org_file_name.'\',\'\',\''.
            'width=\'+window.screen.width+\',height=\'+window.screen.height);return false;"'.
            ' style="left:'.$leftpos.'px; top:'.$toppos.'px" target="_blank">'.
            '<img src="'.$file_base_url.$mid_file_name.'" alt="" /></a>'.
            '<button type="button" onclick="delFile(\''.substr($org_file_name,0,23).'\')">削除</button></div>';
        } else {
            $small_file_name = substr($org_file_name, 0, 24)."s".substr($org_file_name,25);
            if (!file_exists($file_dir.$small_file_name)) continue;
            $iinfo = getimagesize($file_dir.$small_file_name);
            $fw = $iinfo[0]; //横幅
            $fh = $iinfo[1]; //縦幅
            $leftpos = (int)((34 - $fw) / 2);
            $toppos =  (int)((34 - $fh) / 2);
            $picture_list[] =
            '<div class="picture_s_container">'.
            '<a href="'.$file_base_url.$org_file_name.'" onclick="window.open(\''.$file_base_url.$org_file_name.'\',\'\',\''.
            'width=\'+window.screen.width+\',height=\'+window.screen.height);return false;"'.
            ' style="left:'.$leftpos.'px; top:'.$toppos.'px" target="_blank">'.
            '<img src="'.$file_base_url.$small_file_name.'" alt="" /></a></div>';
        }
    }
    return implode("", $picture_list).'<div style="clear:both"></div>';
}




//====================================================================
// DB制御系ラッパー
//====================================================================

//--------------------------------------------------------------------
// INSERT / UPDATE / DELETEの実行
//--------------------------------------------------------------------
function dbExec($sql) {
    global $con;
    global $fname;
    $ret = update_set_table($con, $sql, array(), null, "", $fname);
    if ($ret==0) {
        echo $sql."\n\n".pg_last_error($con);
        die;
    }
}
//--------------------------------------------------------------------
// SELECT実行
//--------------------------------------------------------------------
function dbFind($sql) {
    global $con;
    global $fname;
    $ret = select_from_table($con, $sql, "", $fname);
    if ($ret==0) {
        echo $sql."\n\n".pg_last_error($con);
        die;
    }
    return $ret;
}
function dbEI($s) {
    if ($s=="") return "NULL";
    return (int)$s;
}
function dbES($s) {
    return "'".pg_escape_string($s)."'";
}
function dbGetAll($sql) {
    $sel = dbFind($sql);
    return (array)pg_fetch_all($sel);
}
function dbGetTopRow($sql) {
    $sel = dbFind($sql);
    return pg_fetch_array($sel, $row, PGSQL_ASSOC);
}
function dbGetOne($sql) {
    $sel = dbFind($sql);
    $row = pg_fetch_row($sel);
    return @$row[0];
}







//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 【共通】ここから、すべての始まり
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――


//******************************************************************************
// 最初にリクエストをデコードする
// ２つの関数定義の直後に、関数を呼んでいる
//******************************************************************************
function unicode_entity_decode($str) {
    $ary = explode(";", $str);
    $out = array();
    foreach($ary as $s) {
        if (!$s) continue;
        if ($s=="&#9") { $out[]= "\t"; continue; }
        $ss = mb_convert_encoding($s.";", GG_CS_EUCJP, "HTML-ENTITIES");
        if ($ss=="\t") { $out[] = $s.";"; continue; }
        $ss = mb_convert_encoding(mb_convert_encoding($ss, 'sjis-win', 'eucjp'), GG_CS_EUCJP, 'sjis-win');
        if ($ss=="\t") $out[] = $s.";";
        else $out[] = $ss;
    }
    return implode("", $out);
}
function decode_request() {
    $def = mb_substitute_character(); // たぶんハテナマーク
    mb_substitute_character(0x9); // 適当にタブ文字
    $request_names = array_keys($_REQUEST);
    foreach ($request_names as $key) {
        if (substr($key, 0, 12)!="uni_entity__") continue;
        $val = $_REQUEST[$key];
        if (is_array($val)) {
            foreach ($val as $idx=>$v) $val[$idx] = unicode_entity_decode($v);
            $_REQUEST[substr($key,12)] = $val;
        } else {
            $_REQUEST[substr($key,12)] = unicode_entity_decode($val);
        }
        unset($_REQUEST[$key]);
    }
    $post_names = array_keys($_POST);
    foreach ($post_names as $key) {
        if (substr($key, 0, 12)!="uni_entity__") continue;
        $val = $_POST[$key];
        if (is_array($val)) {
            foreach ($val as $idx=>$v) $val[$idx] = unicode_entity_decode($v);
            $_POST[substr($key,12)] = $val;
        } else {
            $_POST[substr($key,12)] = unicode_entity_decode($val);
        }
        unset($_POST[$key]);
    }
    mb_substitute_character($def); // 戻す
}
decode_request();







//******************************************************************************
// comedix直下へソースコードを配置しないで良くするための仕組み --- ディレクトリパス調整   【chdir】
//
// カレントディレクトリを強制変更しないと、comedix直下のLIB系ファイルのロードができない。
//
// 拡張温度板のURLは、comedix/kangoshien/ というように、comedixよりひとつ下階層になる。
// よってこのファイルは「comedix/kangoshien/ks_main.php」であり、ひとつ下階層に存在することになる。
// クライアントからのURL呼出があって、apacheがphpエンジンを動かして、phpエンジンがphpソースをコールしたとき、
// コールされたphpソースの場所がカレントディレクトリになるわけだが、
// もしカレントディレクトリがこのファイルと同じディレクトリなら、ひとつ上にのぼって、comedix直下とする。という話。
//******************************************************************************
define("GG_BASE_FOLDER", basename(dirname(__FILE__))); // 通常 「kangoshien」となる。
set_include_path("./".GG_BASE_FOLDER . PATH_SEPARATOR . get_include_path()); // 上階層にいく前に、このディレクトリをLIBインクルード先の第一優先で追加。
if (dirname(__FILE__) == getcwd()) chdir("../"); // カレントディレクトリがkangoshienなら、ひとつ上階層のcomedixへカレントディレクトリ移動
define("GG_PICTURE_FOLDER", "siji_picture");

//******************************************************************************
// 共通コール
// 「about_xxxx」系などは、カレントディレクトリをcomedix直下に変更してから読み込まなければ、
//  インクルードPATHを指定しても無理。その先の先の読み込みのどこかでエラーとなる。
//******************************************************************************
require_once("about_comedix.php"); // DB接続設定を呼んだりする。多用するh()関数もここに。
require_once("about_certify.php"); // ログイン確認・数少ないセッション変数管理としても。重要。
//require_once("common_log_class.ini"); // ログの排出関連。ログは、comedix/log/ へ出力される。



//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $_SERVER["PHP_SELF"];
$session = qualify_session($_REQUEST["session"], $fname);
$con = @connect2db($fname);
if (!@$IS_CRON) {
	$emp_amb_flg = @check_authority($session,83,$fname);
	if(!$session || !$emp_amb_flg || !$con){
	    ob_clean();
	    if (defined("IS_AJAX_REQUEST")) {
	        if (!$_REQUEST["session"]) echo "SESSION_NOT_FOUND";
	        else if (!$session) echo "SESSION_EXPIRED";
	        else if (!$con) echo "DATABASE_CONNECTION_ERROR";
	        else if (!$emp_amb_flg) echo "NO_AUTH_83";
	    } else {
	        echo("<html><head><script type='text/javascript' src='js/showpage.js'></script>");
	        echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
	    }
	    exit;
	}
}
define("GG_SESSION_ID", $session);


if (!@$IS_CRON) {
	$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst where emp_id = (select emp_id from session where session_id = ".dbES(GG_SESSION_ID).")";
	$emp_row = dbGetTopRow($sql);
	if (!@$emp_row["emp_id"]) {
	    if (defined("IS_AJAX_REQUEST")) {
	        echo "SESSION_EXPIRED";
	    } else {
	        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	    }
	    exit;
	}
}
define("GG_LOGIN_EMP_ID", $emp_row["emp_id"]);
define("GG_LOGIN_EMP_NAME", $emp_row["emp_lt_nm"]." ".$emp_row["emp_ft_nm"]);



// メドレポート管理者。何でもできる。
if (!@$IS_CRON) {
	$emp_ambadm_flg = @check_authority($session,84,$fname);
	define("GG_ADMIN_FLG", $emp_ambadm_flg);
}


// メドレポート管理者または温度板管理者なら、温度板管理者フラグをオンに
if (!GG_ADMIN_FLG) {
    $flg = dbGetOne("select patient_control_auth from sum_ks_mst_emp_auth where emp_id = ".dbES(GG_LOGIN_EMP_ID));
}
define("GG_ONDOBAN_ADMIN_FLG", (GG_ADMIN_FLG || $flg ? 1 : ""));

$last_bldg_ward = dbGetOne("select last_bldg_ward from sum_ks_mst_emp_auth where emp_id = ".dbES(GG_LOGIN_EMP_ID));
define("GG_LAST_BLDG_WARD", $last_bldg_ward);


