<?
if (!defined("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING")) { // 印刷時は既にdefineされている。この中へは入らない。
    define("IS_AJAX_REQUEST", 1);
    require_once("ks_common.php");
    ob_clean();
    $ajax_action = $_REQUEST["ajax_action"];
    if ($ajax_action=="get_sijiban_soap_list") {
        get_sijiban_soap_list(
            $_REQUEST["ptif_id"], $_REQUEST["monday_ymd"], $_REQUEST["bldg_ward"], $_REQUEST["is_nyuin_taiin"],
            $_REQUEST["view_mode"], $_REQUEST["sticky_menubar"], 0, $_REQUEST["ext_view_mode"], $_REQUEST["ext_view_ymd"],
            $_REQUEST["ptif_name_find"]
        );
    }
    else echo "ajax_action=".$ajax_action; // ajax_action該当なし。プログラム実装エラーである
    pg_close($con);
    die;
}

function calc_taiin_classname($nyuin_taiin_list, $WYL) {
    $taiin_classname = array("_taiin","_taiin","_taiin","_taiin","_taiin","_taiin","_taiin"); // 初期値。表示週間はすべて退院状態、とする
    if ($nyuin_taiin_list) {
        $ntlist = explode(",", $nyuin_taiin_list);
        foreach ($ntlist as $ntjoint) {
            $nt = explode("-", $ntjoint);
            if ($nt[0] > $WYL[6]) continue; // 入院日が週末以降。コンティニュー
            if ($nt[1]!="" && $nt[1] < $WYL[0]) continue; // 退院日があるが週頭以前。コンティニュー
            for ($i=0; $i<=6; $i++) {
                if ($nt[0] > $WYL[$i]) continue; // 入院日が指定日以降。コンティニュー
                if ($nt[1]!="" && $nt[1] < $WYL[$i]) continue; // 退院日が指定日以前。コンティニュー
                $taiin_classname[$i]=""; // 対象日は入院であることが判明。退院クラス名を削除
            }
        }
    }
    return $taiin_classname;
}



function instant_add(&$ary, $v, $pre="", $post="") {
    if ($v=="") return;
    $ary[] = $pre.wbr($v).$post;
}
function instant_ret($v, $pre="", $post="") {
    if ($v=="") return;
    return $pre.wbr($v).$post;
}


//****************************************************************************************************************
// 指示板コンテンツ排出
// printMode=1は通常ブラウザ印刷。2なら患者ごと改ページ印刷。
// stickyMenubarフラグがあれば、要は画面スクロールしても固定ポジションのテーブルヘッダを、追加で出力するモードとなる
//****************************************************************************************************************
function get_sijiban_soap_list($ptif_id, $monday_ymd, $bldg_ward, $is_nyuin_taiin, $view_mode, $stickyMenubar, $printMode, $ext_view_mode, $ext_view_ymd, $ptif_name_find) {

    // 印刷時is_print==1の場合。
    // このとき患者ptif_idが指定されていれば、入院状態is_nyuin_taiinは無視。病棟bldg_wardは全部、とする。
    if ($printMode) {
        if ($ptif_id) {
            $is_nyuin_taiin = "";
            $bldg_ward = "all";
        }
    }

    list($bldg_cd, $ward_cd) = explode("_", $bldg_ward);
    $view_mode = ($view_mode=="soap" ? "soap" : "sijiban");
    $ext_view_mode = ($ext_view_mode=="input_care" ? "input_care" : "");

    $WYL = create_week_ymd_list(@$monday_ymd);
    $monday_ymd = $WYL[0];
    $sunday_ymd = $WYL[6];

    $next_monday_ymd = get_next_ymd($sunday_ymd);
    $where1 =
    " ".($is_nyuin_taiin=="nyuin_only" ? " and nt.ptif_id is not null" : "").
    " ".($is_nyuin_taiin=="taiin_only" ? " and nt.ptif_id is null" : "").
    " ".($printMode && $ptif_id ?         " and nt.ptif_id = ".dbES($ptif_id) : "").
    " ".($bldg_cd!="all" && $ward_cd ? " and opa.bldg_cd = ".(int)$bldg_cd." and opa.ward_cd = ".(int)$ward_cd : "").
    " ".($bldg_cd!="all" && !$ward_cd ? " and ( opa.ward_cd is null or opa.ward_cd = 0 )" : "").
    " ".($bldg_cd=="clear" ? " and 1 <> 1" : ""); // clearなら検索結果をゼロにする
    $where2 =
    " where ( nyuin_ymd is not null and nyuin_ymd <> '' and nyuin_ymd <= ".dbES($sunday_ymd).")".
    " and ( taiin_ymd is null or taiin_ymd = '' or taiin_ymd >= ".dbES($monday_ymd).")";
    $where3 = ($printMode && $ptif_id ?         " and pt.ptif_id = ".dbES($ptif_id) : "");
    if ($ext_view_mode=="input_care") {
        // 一括バイタル・ケアなら単日
        if ($is_nyuin_taiin=="nyuin_only") {
            $where2 =
            " where ( nyuin_ymd is not null and nyuin_ymd <> '' and nyuin_ymd <= ".dbES($ext_view_ymd).")".
            " and ( taiin_ymd is null or taiin_ymd = '' or taiin_ymd >= ".dbES($ext_view_ymd).")";
        }
        // 特定患者を指定した場合
        else {
            $where2 = " where ptif_id = ".dbES($ptif_id);
        }
    }

    //=================================================
    // 最後にアクセスした病棟を保存（印刷時は保存しない）。レコードがなければカラでINSERTしておく
    //=================================================
    if (!dbGetOne("select emp_id from sum_ks_mst_emp_auth where emp_id = ".dbES(GG_LOGIN_EMP_ID))) {
        dbExec("insert into sum_ks_mst_emp_auth ( emp_id, patient_control_auth ) values (".dbES(GG_LOGIN_EMP_ID).", 0)");
    }
    if (!$printMode) {
        dbExec("update sum_ks_mst_emp_auth set last_bldg_ward = ".dbES($bldg_ward)." where emp_id = ".dbES(GG_LOGIN_EMP_ID));
    }

    //=================================================
    // 指定週の入院中患者一覧
    //=================================================
    $sql =
    " select * from (".
    "   select pt.ptif_id, pt.ptif_lt_kana_nm, pt.ptif_ft_kana_nm, pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm, opa.nyuin_taiin_list".
    "  ,bw.ward_name, ptrm.ptrm_name".
    "  ,coalesce(opa.bldg_cd,0) as bldg_cd, coalesce(opa.ward_cd,0) as ward_cd, coalesce(opa.ptrm_room_no,0) as ptrm_room_no".
    "   from ptifmst pt".
    "   left outer join sum_ks_mst_pat_attribute opa on (opa.ptif_id = pt.ptif_id)".
    "   left outer join sum_ks_mst_bldg_ward bw on (bw.bldg_cd = opa.bldg_cd and bw.ward_cd = opa.ward_cd)".
    "   left outer join sum_ks_mst_ptrm ptrm on (ptrm.bldg_cd = opa.bldg_cd and ptrm.ward_cd = opa.ward_cd and ptrm.ptrm_room_no = opa.ptrm_room_no)".
    "   left outer join (".
    "       select ptif_id from sum_ks_mst_pat_nyuin_taiin".$where2.
    "       group by ptif_id".
    "   ) nt on ( nt.ptif_id = pt.ptif_id )".
    "   where 1 = 1 ".$where1.make_ptif_name_sql($ptif_name_find).
    " ) d".
    " order by bldg_cd, ward_cd, ptrm_name, ptrm_room_no, ptif_lt_kana_nm, ptif_ft_kana_nm, ptif_lt_kaj_nm, ptif_ft_kaj_nm";
    $ptif_all = dbFind($sql);
?>


<? //**************************************************************************************************************** ?>
<? // 一括バイタル・ケア入力の場合                                                                                    ?>
<? //**************************************************************************************************************** ?>
<? if ($ext_view_mode=="input_care") { ?>
<?
    $a_ymd = ymdhm_to_array($ext_view_ymd);


?>
<? if($stickyMenubar && !$printMode) { ?>
<div style="height:24px; position:fixed; top:31px; background-color:#2a9cf4; width:100%; color:#ffffff; border-top:1px solid #88caff; border-bottom:1px solid #2366d2">
    <table cellspacing="0" cellpadding="0" style="width:100%"><tr>
    <td style="width:180px"><div style="width:170px; height:24px; line-height:22px; background:url(img/menubtn_on.gif) 0 -5px repeat-x; text-align:center;">一括バイタル・ケア入力</div></td>
    <td class="left" style="font-size:14px; line-height:22px; letter-spacing:2px">指示日：<?=$a_ymd["slash_mdw"]?></td>
    </tr></table>
</div>
<table cellspacing="0" cellpadding="0" class="sijiban_soap_table" id="sijiban_soap_table_float_header" style="top:57px">
    <tr>
        <th height="24" width="12%">患者名</th>
        <th width="11%">バイタル</th><th width="11%">酸素</th><th width="11%">抗生剤</th><th width="11%">食事・IN</th>
        <th width="11%">補液</th><th width="11%">インスリン</th><th width="11%">OUT</th><th width="11%">検査等</th>
    </tr>
</table>
<? } ?>



<? if(!$printMode) { ?>
<div style="height:24px; position:static; top:31px; background-color:#2a9cf4; width:100%; color:#ffffff; border-top:1px solid #88caff; border-bottom:1px solid #1f59ba">
    <table cellspacing="0" cellpadding="0" style="width:100%"><tr>
    <td style="width:180px"><div style="width:170px; height:24px; line-height:22px; background:url(img/menubtn_on.gif) 0 -5px repeat-x; text-align:center;">一括バイタル・ケア入力</div></td>
    <td class="left" style="font-size:14px; line-height:22px; letter-spacing:2px">指示日：<?=$a_ymd["slash_mdw"]?></td>
    </tr></table>
</div>
<? } ?>
<? // 印刷患者ごと改ページモードでない、通常モードの場合 ?>
<? if ($printMode!="2") { ?>
<table cellspacing="0" cellpadding="0" class="sijiban_soap_table input_care_table">
    <tr>
        <th height="24" width="12%">患者名</th>
        <th width="11%">バイタル</th><th width="11%">酸素</th><th width="11%">抗生剤</th><th width="11%">食事・IN</th>
        <th width="11%">補液</th><th width="11%">インスリン</th><th width="11%">OUT</th><th width="11%">検査等</th>
    </tr>
<? } ?>

    <?
        //=================================================
        // 指定日のバイタル一覧
        //=================================================
        $ptif_name_sql = make_ptif_name_sql($ptif_name_find);
        $sql =
        " select kv.* from sum_ks_vital kv".
        " left outer join sum_ks_mst_pat_attribute opa on (opa.ptif_id = kv.ptif_id)".
        " left outer join (".
        "     select ptif_id from sum_ks_mst_pat_nyuin_taiin".$where2.
        "     group by ptif_id".
        " ) nt on ( nt.ptif_id = kv.ptif_id )".
        " where 1 = 1 ".$where1.
        " ".($ptif_name_sql ? " and kv.ptif_id in (select ptif_id from ptifmst where 1 = 1 ".$ptif_name_sql.")" : "").
        " and kv.ymdhm >= " . dbES($ext_view_ymd."0000") . // 指定日から
        " and kv.ymdhm <= " . dbES($ext_view_ymd."9999"). // 指定日終了まで
        " order by kv.ymdhm";
        $sel = dbFind($sql);
        $vital_list = array();
        while ($row = @pg_fetch_array($sel)) {
            $vital_list[$row["ptif_id"]][] = $row;
        }

        //=================================================
        // 指定日の期間ケア一覧(酸素、抗生剤)
        //=================================================
        $ptif_name_sql = make_ptif_name_sql($ptif_name_find);
        $sql =
        " select ck.ptif_id, ck.ymdhm_fr, ck.ymdhm_to, ck.serial_data, ck.care_content, ck.sijibi_ymd, ck.siji_az from sum_ks_care_kikan ck".
        " left outer join sum_ks_mst_pat_attribute opa on (opa.ptif_id = ck.ptif_id)".
        " left outer join (".
        "     select ptif_id from sum_ks_mst_pat_nyuin_taiin".$where2.
        "     group by ptif_id".
        " ) nt on ( nt.ptif_id = ck.ptif_id )".
        " where 1 = 1 ".$where1.
        " ".($ptif_name_sql ? " and ck.ptif_id in (select ptif_id from ptifmst where 1 = 1 ".$ptif_name_sql.")" : "").
        " and ck.ymdhm_fr <= ".dbES($ext_view_ymd."9999"). // スケジュールの開始日は、指定日付よりは過去
        " and (ck.ymdhm_to = '' or ck.ymdhm_to >= ".dbES($ext_view_ymd."0000").")". // 終了日は、指定日付よりは未来か、未設定永続中
        " and ck.care_content in ('kouseizai','sanso','kokyuki')".
        " order by ck.ymdhm_fr";
        $sel = dbFind($sql);
        $care_sanso = array();
        $care_kokyuki = array();
        $care_kouseizai = array();
        $next_fr = array();
        while ($row = @pg_fetch_array($sel)) {
            $_ptif_id = $row["ptif_id"];
            $a_ymd1 = ymdhm_to_array($row["ymdhm_fr"]);
            $a_ymd2 = ymdhm_to_array($row["ymdhm_to"]);
            $kikan_disp = wbr($a_ymd1["slash_mdw"].$a_ymd1["zhhmi"]."〜".($row["ymdhm_to"] && $row["ymdhm_fr"]!=$row["ymdhm_to"] ? $a_ymd2["slash_mdw"].$a_ymd2["zhhmi"] : ""));

            $sdata = unserialize($row["serial_data"]);
            $cont = array();

            //--------------------------
            // 抗生剤（K）
            //--------------------------
            if ($row["care_content"]=="kouseizai") {
                $num = count($care_kouseizai[$_ptif_id])+1;

                $a_siji = array();
                if ($row["sijibi_ymd"] && $ext_view_ymd!=$row["sijibi_ymd"]) { // 入力された指示日は、画面指示日とまず同一。独断で、異なる場合のみ表示とする。
                    $a_ymd3 = ymdhm_to_array($row["sijibi_ymd"]);
                    $a_siji[]= $a_ymd3["slash_mdw"];
                }
                if ($row["siji_az"]) $a_siji[] = $row["siji_az"];
                $siji_disp = implode(":",$a_siji);

                $care_kouseizai[$_ptif_id][] =
                "<em>[".$num."]</em>".
                ($siji_disp ? '<span class="action_hm">'.wbr($siji_disp).'</span>' : "").
                $kikan_disp.
                "<em>薬</em>".wbr(get_alias($sdata["med_name"])?get_alias($sdata["med_name"]):"指定なし");
            }
            //--------------------------
            // 酸素吸入 or 人工呼吸器（SK）
            //--------------------------
            if ($row["care_content"]=="sanso") {
                $num = count($care_sanso[$_ptif_id])+1;
                $care_sanso[$_ptif_id][] = "<em>[".$num."]</em>".$kikan_disp.
                instant_ret($sdata["litter"],"<em>量</em>").
                "<em>器</em>".
                wbr(get_alias($sdata["kigu"])?get_alias($sdata["kigu"]):"指定なし");
            }
            if ($row["care_content"]=="kokyuki") {
                $num = count($care_kokyuki[$_ptif_id])+1;
                $noudo = $sdata["noudo"].get_alias($sdata["noudo_unit"]);
                $care_kokyuki[$_ptif_id][] = "<em>[".$num."]</em>".$kikan_disp.instant_ret($noudo,"<em>濃</em>");
            }
        }

        //=================================================
        // 指定日のケア一覧(食事IN、補液、インスリン、OUT、検査等)
        //=================================================
        $ptif_name_sql = make_ptif_name_sql($ptif_name_find);
        $sql =
        " select kc.* from sum_ks_care_ymd kc".
        " left outer join sum_ks_mst_pat_attribute opa on (opa.ptif_id = kc.ptif_id)".
        " left outer join (".
        "     select ptif_id from sum_ks_mst_pat_nyuin_taiin".$where2.
        "     group by ptif_id".
        " ) nt on ( nt.ptif_id = kc.ptif_id )".
        " where 1 = 1 ".$where1.
        " ".($ptif_name_sql ? " and kc.ptif_id in (select ptif_id from ptifmst where 1 = 1 ".$ptif_name_sql.")" : "").
        " and kc.ymd = " . dbES($ext_view_ymd); // 指定された指示日のデータのみ取得。時間またぎなどは無視
        $sel = dbFind($sql);
        $care_list = array();
        while ($row = @pg_fetch_array($sel)) {
            $care_list[$row["ptif_id"]] = $row;
        }

        $ymd_index = 0;
        foreach ($WYL as $_idx => $_ymd) if ($ext_view_ymd==$_ymd) $ymd_index = $_idx;
        $loop = 0;
        while ($row = @pg_fetch_array($ptif_all)) {
            $loop++;
            $ptif_name = $row["ptif_lt_kaj_nm"]." ".$row["ptif_ft_kaj_nm"];
            $taiin_classnames = calc_taiin_classname($row["nyuin_taiin_list"], $WYL);
            $taiin_classname = @$taiin_classnames[$ymd_index];
        ?>

        <? // 印刷患者ごと改ページモードの場合 ?>
        <? if ($printMode=="2") { ?>
<table cellspacing="0" cellpadding="0" class="sijiban_soap_table input_care_table" style="margin-bottom:5px;<?=($loop>1 ? 'page-break-before:always;':'')?>">
    <tr>
        <th height="24" width="12%">患者名</th>
        <th width="11%">バイタル</th><th width="11%">酸素</th><th width="11%">抗生剤</th><th width="11%">食事・IN</th>
        <th width="11%">補液</th><th width="11%">インスリン</th><th width="11%">OUT</th><th width="11%">検査等</th>
    </tr>
        <? } ?>

    <tr>

        <td class="hover" title="<?=hh($row["ptif_id"])?>"
            onclick="dfmPtifDetail.selectPtifAndShowOndoban('<?=js($row["ptif_id"])?>')">
            <?if ($bldg_cd=="all" && @$row["ward_name"]) {?>
            <table cellspacing="0" cellpadding="0" class="ward_ptrm_panel" onmousedown="dfmPtifDetail.popup('<?=js($row["ptif_id"])?>');return false;"><tr>
            <td><?=wbr(@$row["ward_name"])?></td></tr></table>
            <? } ?>
            <table cellspacing="0" cellpadding="0" class="ward_ptrm_panel" onmousedown="dfmPtifDetail.popup('<?=js($row["ptif_id"])?>');return false;"><tr>
            <?if (@$row["ptrm_name"]) {?>
            <td><?=wbr(@$row["ptrm_name"])?></td>
            <? } else { ?>
            <td class="no_ptrm">病室指定なし</td>
            <? } ?>
            </tr></table>
            <div id="divpt_<?=$row["ptif_id"]?>"
                style="padding:3px 0 2px 3px;<?if ($ptif_id==$row["ptif_id"]){?>border-left:6px solid #ff0000;<? } ?>"><?=wbr($ptif_name)?></div>
        </td>

        <?
            // ----------------------------------------------------------------
            // １列目：バイタル入力
            // ----------------------------------------------------------------
            $vital_data = $vital_list[$row["ptif_id"]];
        ?>
            <td style="height:22px" class="soap_cell<?=$taiin_classname?>"
                onmouseover="$(this).addClass('hover')" onmouseout="$(this).removeClass('hover')"
                onclick="stopEvent(event); dfmVital.popup('<?=$ext_view_ymd?>','', '<?=js($row["ptif_id"])?>', '<?=js(hh($ptif_name))?>')">
                <?  if (is_array($vital_data)) {
                    foreach ($vital_data as $vital_row) {
                        $ymdhm = $vital_row["ymdhm"];
                        $a_ymd = ymdhm_to_array($ymdhm);
                        $opts = array();
                        if ($vital_row["taion"]!="") $opts[] = '<span class="vital_t">T</span><span>'.$vital_row["taion"]."</span>";
                        if ($vital_row["myakuhakusu"]!="") $opts[] = '<span class="vital_p">P</span><span>'.$vital_row["myakuhakusu"]."</span>";
                        if ($vital_row["kokyusu"]!="") $opts[] = '<span class="vital_r">R</span><span>'.$vital_row["kokyusu"]."</span>";
                        if ($vital_row["ketsuatsu_bottom"]!="" || $vital_row["ketsuatsu_upper"]!="") {
                            $opts[] = '<span class="vital_bp">BP</span><span>'.$vital_row["ketsuatsu_upper"]."/".$vital_row["ketsuatsu_bottom"]."</span>";
                        }
                        if ($vital_row["hr_kaisu"]!="") $opts[] = '<span class="vital_hr">HR</span><span>'.$vital_row["hr_kaisu"]."</span>";
                        if ($vital_row["spo2_percent"]!="") $opts[] = '<span class="vital_sp">SP</span><span>'.$vital_row["spo2_percent"]."</span>";
                        if ($vital_row["taiju"]!="") $opts[] = '<span class="vital_w">W</span><span>'.$vital_row["taiju"]."</span>";
                    ?>
                    <div style="border-bottom:1px dashed #ff9999;" class="hover soap_cell<?=$taiin_classname?>" onmouseover="return stopEvent(event);"
                    onclick="stopEvent(event); dfmVital.popup('<?=$ext_view_ymd?>','<?=$ymdhm?>', '<?=js($row["ptif_id"])?>', '<?=js(hh($ptif_name))?>')">
                        <span class="action_hm"><?=$a_ymd["hhmi"]?></span><?=ary_wbr($opts)?>
                    </div>
                <? }} ?>
                <? if(!$printMode){?><div class="new_entry"><div>【新規作成】</div></div><?}?>
            </td>

        <?
            // ----------------------------------------------------------------
            // ２列目：酸素／３行目：抗生剤
            // ----------------------------------------------------------------
            $vital_data = $vital_list[$row["ptif_id"]];
        ?>

            <td style="height:22px" class="hover soap_cell<?=$taiin_classname?>"
                onclick="dfmSijiMenu.popupDialog(dfmCareSanso,'','<?=$ext_view_ymd?>','<?=js($row["ptif_id"])?>', '<?=js(hh($ptif_name))?>')">
                <? if (count($care_sanso[$row["ptif_id"]])) { ?>
                <span><h4>酸素吸入</h4></span><?=implode("<br>",$care_sanso[$row["ptif_id"]])?><br/>
                <? } ?>
                <? if (count($care_kokyuki[$row["ptif_id"]])) { ?>
                <span><h4>人工呼吸器</h4></span><?=implode("<br>",$care_kokyuki[$row["ptif_id"]])?>
                <? } ?>
            </td>

            <td style="height:22px" class="hover soap_cell<?=$taiin_classname?>"
                onclick="dfmSijiMenu.popupDialog(dfmCareKouseizai,'kouseizai','<?=$ext_view_ymd?>','<?=js($row["ptif_id"])?>', '<?=js(hh($ptif_name))?>')">
                <?=implode("<br>",(array)$care_kouseizai[$row["ptif_id"]])?>
            </td>

        <?
            // ----------------------------------------------------------------
            // ４列目〜８列目：ケア入力
            // ----------------------------------------------------------------
            $fields = array("","","","","in","hoeki","insulin","out","kensanado");
            for ($ffidx=4; $ffidx<=8; $ffidx++) {
                $field = $fields[$ffidx];
                $care_cont = array();
                if ($field=="in") {
                    $module="dfmCareSyokujiIn";
                    $data_in = unserialize($care_list[$row["ptif_id"]]["serial_data_in"]);
                    //--------------------------
                    // 経口食事量
                    //--------------------------
                    $cont = array();
                    $ary = array("asa"=>"朝", "hiru"=>"昼", "yu"=>"夕");
                    foreach ($ary as $ahy=>$jp) {
                        if ($data_in["keikou_".$ahy."_zessyoku"]) { $cont[]="<em>".$jp."</em>絶食"; continue; }
                        $sf = instant_ret($data_in["keikou_".$ahy."_fukusyoku"], "副");
                        $ss = instant_ret($data_in["keikou_".$ahy."_syusyoku"], "主");
                        if ($sf || $ss) $cont[]= "<em>".$jp."</em>".$sf.$ss;
                    }
                    if (count($cont)) $care_cont[] = "<span><h4>経口</h4></span>".ary_wbr($cont);
                    //--------------------------
                    // 経管
                    //--------------------------
                    $ary = array("asa"=>"朝", "hiru"=>"昼", "yu"=>"夕", "ryou4"=>"��", "ryou5"=>"��", "ryou6"=>"��");
                    $keikan_field_count = (int)$data_in["keikan__field_count"];
                    $cont = array();
                    for ($idx=1; $idx<=$keikan_field_count; $idx++) {
                        $cont2 = array();
                        foreach ($ary as $ahy=>$jp) {
                            $zessyoku = $data_in["keikan__".$idx."__".$ahy."_zessyoku"];
                            if ($zessyoku) $zessyoku = "絶食";
                            $sz = instant_ret($zessyoku, "<em>".$jp."</em>");
                            if ($sz=="") $sz = instant_ret($data_in["keikan__".$idx."__".$ahy."_ml"], "<em>".$jp."</em>");
                            if ($sz) $cont2[]= $sz;
                        }
                        if (count($cont2)) $cont[] = "<em>[".$idx."]</em>".ary_wbr($cont2);
                    }
                    if (count($cont)) $care_cont[] = "<span><h4>経管</h4></span>".ary_wbr($cont);
                    //--------------------------
                    // 補食
                    //--------------------------
                    $hosyoku_field_count = $data_in["hosyoku__field_count"];
                    if (!$hosyoku_field_count) {
                        if ($data_in["hosyoku_timing"].$data_in["hosyoku"].$data_in["hosyoku_ryou"]!="") {
                            $hosyoku_field_count = 1;
                            $data_in["hosyoku__1__hosyoku_timing"] = $data_in["hosyoku_timing"];
                            $data_in["hosyoku__1__hosyoku"] = $data_in["hosyoku"];
                            $data_in["hosyoku__1__hosyoku_ryou"] = $data_in["hosyoku_ryou"];
                        }
                    }
                    $cont = array();
                    for ($idx=1; $idx<=$hosyoku_field_count; $idx++) {
                        if (!$data_in["hosyoku__".$idx."__hosyoku_timing"] && !$data_in["hosyoku__".$idx."__hosyoku"]) continue;
                        $cont[]= "<em>[".$idx."]</em>";
                        instant_add($cont, get_alias($data_in["hosyoku__".$idx."__hosyoku_timing"]));
                        instant_add($cont, get_alias($data_in["hosyoku__".$idx."__hosyoku"]), "<em>種類</em>");
                        instant_add($cont, get_alias($data_in["hosyoku__".$idx."__hosyoku_ryou"]), "<em>摂取量</em>");
                    }
                    if (count($cont)) $care_cont[] = "<span><h4>補食</h4></span>".ary_wbr($cont);


                    //--------------------------
                    // 水分摂取量
                    //--------------------------
                    $cont = array();
                    $nums = array("", "��", "��", "��", "��", "��");
                    instant_add($cont, str_replace("_","",$data_in["suibun_unit"])); // 経口_ml 経口_割 経管_ml の３種いずれか
                    for ($idx=1; $idx<=5; $idx++) {
                        instant_add($cont, $data_in["suibun_ryou_".$idx], "<em>".$nums[$idx]."</em>");
                    }
                    if (count($cont)) $care_cont[] = "<span><h4>水分</h4></span>".ary_wbr($cont);
                }
                if ($field=="hoeki") {
                    $module="dfmCareHoeki";
                    //--------------------------
                    // 補液
                    //--------------------------
                    $data_hoeki = unserialize($care_list[$row["ptif_id"]]["serial_data_hoeki"]);
                    $field_count = $data_hoeki["hoeki__field_count"];
                    for ($fidx=1; $fidx<=$field_count; $fidx++) {
                        $cont = array();
                        $oaz = $data_hoeki["hoeki__".$fidx."__hoeki_az"];
                        $az = $oaz;
                        if ($az=="") $az = "？";
                        $child_count = $data_hoeki["hoeki__".$fidx."__field_count"];
                        for ($cidx=1; $cidx<=$child_count; $cidx++) {
                            $key = "hoeki__".$fidx."__child".$cidx."__hoeki_";
                            $hm = $data_hoeki[$key."hm"];
                            if (strlen($hm)==4) $hm = substr($hm,0,2).":".substr($hm,2);
                            $koushin = $data_hoeki[$key."koushin"];
                            $zan = $data_hoeki[$key."zan"];
                            $ryusoku = $data_hoeki[$key."ryusoku"];
                            $is_empty = 0;
                            if ($oaz=="" && $hm=="" && $koushin=="" && $zan=="" && $ryusoku=="") $is_empty = 1;
                            if (!$is_empty) {
                                if ($hm=="") $hm = "時刻なし";
                                $cont[]= "<em>[".$cidx."]</em>".wbr($hm."-".($koushin ? "更新" : $zan."/".$ryusoku));
                            }
                        }
                        if (count($cont)) $care_cont[] = '<span class="action_hm">'.$az.'</span>'.ary_wbr($cont);
                    }
                }
                if ($field=="insulin") {
                    $module="dfmCareInsulin";
                    $data_insulin = unserialize($care_list[$row["ptif_id"]]["serial_data_insulin"]);
                    //--------------------------
                    // インスリン 定期
                    //--------------------------
                    if ($data_insulin["insulin_teiki_ari"]) {
                        $tmp = array();
                        if ($data_insulin["insulin_teiki_asa"]) $tmp[]= "朝";
                        if ($data_insulin["insulin_teiki_hiru"]) $tmp[]= "昼";
                        if ($data_insulin["insulin_teiki_yu"]) $tmp[]= "夕";
                        for ($idx=4; $idx<=7; $idx++) {
                            if ($data_insulin["insulin_teiki_check".$idx]) $tmp[]= $data_insulin["insulin_teiki_when".$idx];
                        }
                        $care_cont[] = "<span><h4>定期</h4></span>".wbr(implode("/",$tmp));
                    }
                    //--------------------------
                    // インスリン BS測定値
                    //--------------------------
                    if ($data_insulin["insulin_bs_ari"]) {
                        $field_count = $data_insulin["bsscale__field_count"];
                        $cont = array();
                        for ($idx=1; $idx<=$field_count; $idx++) {
                            $key = "bsscale__".$idx."__";
                            $hm = $data_insulin[$key."bs_hm"];
                            if (strlen($hm)==4) $hm = substr($hm,0,2).":".substr($hm,2);
                            if ($hm=="") $hm = "時刻なし";
                            $cont[]=
                            "<em>[".$idx."]</em>".wbr($hm).
                            instant_ret($data_insulin[$key."bs_upper"], "<em>測</em>").
                            instant_ret($data_insulin[$key."bs_name1"], "<em>薬A</em>").
                            instant_ret($data_insulin[$key."bs_ryou1"], "<em>量A</em>").
                            instant_ret($data_insulin[$key."bs_name2"], "<em>薬B</em>").
                            instant_ret($data_insulin[$key."bs_ryou2"], "<em>量B</em>");
                        }
                        $care_cont[] = "<span><h4>BS測定値</h4></span>".implode("<br>",$cont);
                    }
                    //--------------------------
                    // インスリン 食事量
                    //--------------------------
                    if ($data_insulin["insulin_sr_ari"]) {
                        $tmp =
                        instant_ret($data_insulin["insulin_sr_asa"], "<em>朝</em>").
                        instant_ret($data_insulin["insulin_sr_hiru"], "<em>昼</em>").
                        instant_ret($data_insulin["insulin_sr_yu"], "<em>夕</em>");
                        $care_cont[] = "<span><h4>食事量</h4></span>".$tmp;
                    }
                }
                if ($field=="out") {
                    $module="dfmCareOut";
                    $data_out = unserialize($care_list[$row["ptif_id"]]["serial_data_out"]);
                    //--------------------------
                    // 便
                    //--------------------------
                    if ($data_out["ben_nasi"]) { $care_cont[]="<span><h4>便</h4></span>なし"; }
                    else {
                        $field_count = (int)@$data_out["ben__field_count"];
                        $cont = array();
                        for ($idx=1; $idx<=$field_count; $idx++) {
                            $key = "ben__".$idx."__";
                            $ben_benryou = $data_out[$key."ben_benryou"];
                            $ben_type = ($data_out[$key."ben_type"]!="" ? "/".$data_out[$key."ben_type"] : "");
                            $ben_ryou = ($data_out[$key."ryou"]!="" ? "/".$data_out[$key."ryou"]."g" : "");
                            if ($ben_benryou!="" || $ben_type!="" || $ben_ryou!="") $cont[]= "<em>[".$idx."]</em>".wbr($ben_benryou.$ben_type.$ben_ryou);
                        }
                        if (count($cont)) $care_cont[] = "<span><h4>便</h4></span>".ary_wbr($cont);
                    }

                    //--------------------------
                    // 尿
                    //--------------------------
                    $field_count = (int)@$data_out["nyou__field_count"];
                    $cont = array();
                    for ($idx=1; $idx<=$field_count; $idx++) {
                        $hm = $data_out["nyou__".$idx."__nyou_hm"];
                        $_ryou = $data_out["nyou__".$idx."__ryou"];
                        $_more = $data_out["nyou__".$idx."__nyou_more"];
                        if ($hm=="" && $_ryou=="" && $_more=="") continue;
                        $cont[] =
                        "<em>[".$idx."]</em>".($hm=="" ? "" : ((int)substr($hm,0,2)) . ":" . substr($hm,2)).
                        instant_ret($data_out["nyou__".$idx."__ryou"], "<em>量</em>").
                        "<em>".get_alias($data_out["nyou__".$idx."__nyou_more"])."</em>";
                    }
                    if (count($cont)) $care_cont[] = "<span><h4>時間尿</h4></span>".ary_wbr($cont);
                    $cont = array();
                    instant_add($cont, $data_out["day_nyou"]);
                    instant_add($cont, $data_out["day_nyou_comment"], "<em>ｺﾒﾝﾄ</em>");
                    if (count($cont)) $care_cont[] = "<span><h4>尿１日量</h4></span>".ary_wbr($cont);

                    //--------------------------
                    // 排液
                    //--------------------------
                    $cont = array();
                    instant_add($cont, get_alias($data_out["haieki_syurui"]));
                    instant_add($cont, $data_out["haieki_syurui_comment"], "/");
                    if (count($cont)) $care_cont[] = "<span><h4>排液</h4></span>".ary_wbr($cont);

                    $field_count = (int)@$data_out["haieki_jikantai__field_count"];
                    $cont = array();
                    for ($idx=1; $idx<=$field_count; $idx++) {
                        $cont2 = array();
                        $hm =@$data_out["haieki_jikantai__".$idx."__haieki_hm"];
                        instant_add($cont2, ($hm=="" ? "" : ((int)substr($hm,0,2)) . ":" . substr($hm,2)));
                        instant_add($cont2, $data_out["haieki_jikantai__".$idx."__ryou"], "<em>量</em>");
                        instant_add($cont2, get_alias($data_out["haieki_jikantai__".$idx."__haieki_more"]), "[", "]");
                        if (count($cont2)) $cont[] = "<em>[".$idx."]</em>".ary_wbr($cont2);
                    }
                    if (count($cont)) $care_cont[] = "<span><h4>時間排液量</h4></span>".ary_wbr($cont);

                    $cont = array();
                    instant_add($cont, get_alias($data_out["haieki_day_ryou"]));
                    instant_add($cont, $data_out["haieki_day_comment"], "<em>ｺﾒﾝﾄ</em>");
                    if (count($cont)) $care_cont[] = "<span><h4>排液一日量</h4></span>".ary_wbr($cont);

                    //--------------------------
                    // 排液2
                    //--------------------------
                    $cont = array();
                    instant_add($cont, get_alias($data_out["haieki2_syurui"]));
                    instant_add($cont, $data_out["haieki2_syurui_comment"], "/");
                    if (count($cont)) $care_cont[] = "<span><h4>排液2</h4></span>".ary_wbr($cont);

                    $field_count = (int)@$data_out["haieki2_jikantai__field_count"];
                    $cont = array();
                    for ($idx=1; $idx<=$field_count; $idx++) {
                        $cont2 = array();
                        $hm =@$data_out["haieki2_jikantai__".$idx."__haieki_hm"];
                        instant_add($cont2, ($hm=="" ? "" : ((int)substr($hm,0,2)) . ":" . substr($hm,2)));
                        instant_add($cont2, $data_out["haieki2_jikantai__".$idx."__ryou"], "<em>量</em>");
                        instant_add($cont2, get_alias($data_out["haieki2_jikantai__".$idx."__haieki_more"]), "[", "]");
                        if (count($cont2)) $cont[] = "<em>[".$idx."]</em>".ary_wbr($cont2);
                    }
                    if (count($cont)) $care_cont[] = "<span><h4>時間排液量2</h4></span>".ary_wbr($cont);

                    $cont = array();
                    instant_add($cont, get_alias($data_out["haieki2_day_ryou"]));
                    instant_add($cont, $data_out["haieki2_day_comment"], "<em>ｺﾒﾝﾄ</em>");
                    if (count($cont)) $care_cont[] = "<span><h4>排液2一日量</h4></span>".ary_wbr($cont);

                    //--------------------------
                    // 排液3
                    //--------------------------
                    $cont = array();
                    instant_add($cont, get_alias($data_out["haieki3_syurui"]));
                    instant_add($cont, $data_out["haieki3_syurui_comment"], "/");
                    if (count($cont)) $care_cont[] = "<span><h4>排液3</h4></span>".ary_wbr($cont);

                    $field_count = (int)@$data_out["haieki3_jikantai__field_count"];
                    $cont = array();
                    for ($idx=1; $idx<=$field_count; $idx++) {
                        $cont2 = array();
                        $hm =@$data_out["haieki3_jikantai__".$idx."__haieki_hm"];
                        instant_add($cont2, ($hm=="" ? "" : ((int)substr($hm,0,2)) . ":" . substr($hm,2)));
                        instant_add($cont2, $data_out["haieki3_jikantai__".$idx."__ryou"], "<em>量</em>");
                        instant_add($cont2, get_alias($data_out["haieki3_jikantai__".$idx."__haieki_more"]), "[", "]");
                        if (count($cont2)) $cont[] = "<em>[".$idx."]</em>".ary_wbr($cont2);
                    }
                    if (count($cont)) $care_cont[] = "<span><h4>時間排液量3</h4></span>".ary_wbr($cont);

                    $cont = array();
                    instant_add($cont, get_alias($data_out["haieki3_day_ryou"]));
                    instant_add($cont, $data_out["haieki3_day_comment"], "<em>ｺﾒﾝﾄ</em>");
                    if (count($cont)) $care_cont[] = "<span><h4>排液3一日量</h4></span>".ary_wbr($cont);
                }
                if ($field=="kensanado") {
                    $module="dfmCareKensanado";
                    $data_kensanado = unserialize($care_list[$row["ptif_id"]]["serial_data_kensanado"]);
                    //--------------------------
                    // 下剤
                    //--------------------------
                    $field_count = (int)$data_kensanado["gezai__field_count"];
                    $cont = array();
                    for ($idx=1; $idx<=$field_count; $idx++) {
                        $hm = $data_kensanado["gezai__".$idx."__hm"];
                        $med = get_alias($data_kensanado["gezai__".$idx."__med_name"]);
                        if ($hm=="" && $med=="") continue;
                        $hm = ($hm=="" ? "時刻なし" : ((int)substr($hm,0,2)) . ":" . substr($hm,2));
                        $cont[] = "<em>[".$idx."]</em>".$hm."<em>薬</em>".wbr($med=="" ? "指定なし" : $med);
                    }
                    if (count($cont)) $care_cont[] = "<span><h4>下剤</h4></span>".ary_wbr($cont);

                    //--------------------------
                    // 座薬
                    //--------------------------
                    $field_count = (int)$data_kensanado["zayaku__field_count"];
                    $cont = array();
                    for ($idx=1; $idx<=$field_count; $idx++) {
                        $hm = $data_kensanado["zayaku__".$idx."__hm"];
                        $med = get_alias($data_kensanado["zayaku__".$idx."__med_name"]);
                        if ($hm=="" && $med=="") continue;
                        $hm = ($hm=="" ? "時刻なし" : ((int)substr($hm,0,2)) . ":" . substr($hm,2));
                        $cont[] = "<em>[".$idx."]</em>".$hm."<em>薬</em>".wbr($med=="" ? "指定なし" : $med);
                    }
                    if (count($cont)) $care_cont[] = "<span><h4>座薬</h4></span>".ary_wbr($cont);
                    //--------------------------
                    // 発赤
                    //--------------------------
                    if ($data_kensanado["hosseki"]!="") $care_cont[] = "<span><h4>発赤</h4></span>".wbr($data_kensanado["hosseki"]);
                    //--------------------------
                    // 検査
                    //--------------------------
                    $kensa_names = array();
                    if (strlen($data_kensanado["kensa_names"])) $kensa_names = explode(",", $data_kensanado["kensa_names"]);
                    global $care_kensa_names; // 旧形式に対応
                    foreach ($care_kensa_names as $key => $v) { // 旧形式に対応
                        if ($data_kensanado["kensa_names_".$key]) $kensa_names[]= $v; // 旧形式に対応
                    }
                    if ($data_kensanado["kensa_other_comment"]!="") $kensa_names[] = $data_kensanado["kensa_other_comment"];
                    if (count($kensa_names)) $care_cont[] = "<span><h4>検査</h4></span>".wbr(implode("/",$kensa_names));
                }
        ?>
            <td style="height:22px" class="hover soap_cell<?=$taiin_classname?>"
                onclick="dfmSijiMenu.popupDialog(<?=$module?>,'','<?=$ext_view_ymd?>','<?=js($row["ptif_id"])?>', '<?=js(hh($ptif_name))?>')">
                <?=implode("<br>",$care_cont)?>
            </td>
        <? } ?>
        </tr>
        <? // 印刷患者ごと改ページモードの場合 ?>
        <? if ($printMode=="2") { ?></table><? } ?>

    <? } ?>
<? // 印刷患者ごと改ページモードでない、通常モードの場合 ?>
<? if ($printMode!="2") { ?>
</table>
<? } ?>










<? //**************************************************************************************************************** ?>
<? // 指示板の場合                                                                                                    ?>
<? //**************************************************************************************************************** ?>
<? } else if ($view_mode=="sijiban") { ?>



<? if($stickyMenubar) { ?>
<table cellspacing="0" cellpadding="0" class="sijiban_soap_table" id="sijiban_soap_table_float_header">
    <tr>
        <th width="10%" height="24">患者名</th>
        <th width="10%">備考</th>
        <? for ($i = 1; $i <= 7; $i++){ ?><th width="10%"><? $a_ymd = ymdhm_to_array($WYL[$i-1]); echo $a_ymd["slash_mdw"];?></th><? } ?>
        <th width="10%">翌週備考</th>
    </tr>
</table>
<? } ?>


<? // 印刷患者ごと改ページモードでない、通常モードの場合 ?>
<? if ($printMode!="2") { ?>
<table cellspacing="0" cellpadding="0" class="sijiban_soap_table">
    <tr>
        <th width="10%" height="24">患者名</th>
        <th width="10%">備考</th>
        <? for ($i = 1; $i <= 7; $i++){ ?><th width="10%"><? $a_ymd = ymdhm_to_array($WYL[$i-1]); echo $a_ymd["slash_mdw"];?></th><? } ?>
        <th width="10%">翌週備考</th>
    </tr>
<? } ?>



    <?
        //=================================================
        // 指定週の指示内容・MEMO・週次備考一覧
        //=================================================
        $ptif_name_sql = make_ptif_name_sql($ptif_name_find);
        $sql =
        " select siji.ptif_id, siji.serial_data_chusya, siji.serial_data_daily_bikou, siji.serial_data_weekly_bikou, siji.ymd".
        " from sum_ks_siji_ymd siji".
        " left outer join sum_ks_mst_pat_attribute opa on (opa.ptif_id = siji.ptif_id)".
        " left outer join (".
        "     select ptif_id from sum_ks_mst_pat_nyuin_taiin".$where2.
        "     group by ptif_id".
        " ) nt on ( nt.ptif_id = siji.ptif_id )".
        " where 1 = 1 ".$where1.
        " ".($ptif_name_sql ? " and siji.ptif_id in (select ptif_id from ptifmst where 1 = 1 ".$ptif_name_sql.")" : "").
        " and siji.ymd >= " . dbES($monday_ymd) . // 週頭月曜から
        " and siji.ymd <= " . dbES($next_monday_ymd); // 週末日曜まで
        $sel = dbFind($sql);
        $siji_list = array();
        while ($row = @pg_fetch_array($sel)) {
            $prefix = $row["ptif_id"]."@".$row["ymd"];
            $siji_list[$prefix]["chusya_list"] = unserialize($row["serial_data_chusya"]);
            $siji_list[$prefix]["daily_bikou_list"] = unserialize($row["serial_data_daily_bikou"]);
            $siji_list[$prefix]["weekly_bikou_list"] = unserialize($row["serial_data_weekly_bikou"]);
        }
        $loop = 0;
        while ($row = @pg_fetch_array($ptif_all)) {
            $loop++;
            $ptif_name = $row["ptif_lt_kaj_nm"]." ".$row["ptif_ft_kaj_nm"];
            $taiin_classname = calc_taiin_classname($row["nyuin_taiin_list"], $WYL);
    ?>

        <? // 印刷患者ごと改ページモードの場合 ?>
        <? if ($printMode=="2") { ?>
        <table cellspacing="0" cellpadding="0" class="sijiban_soap_table" style="margin-bottom:5px;<?=($loop>1 ? 'page-break-before:always;':'')?>">
            <tr>
                <th width="10%" height="24">患者名</th>
                <th width="10%">備考</th>
                <? for ($i = 1; $i <= 7; $i++){ ?><th width="10%"><? $a_ymd = ymdhm_to_array($WYL[$i-1]); echo $a_ymd["slash_mdw"];?></th><? } ?>
                <th width="10%">翌週備考</th>
            </tr>
        <? } ?>

        <tr>
            <td rowspan="2" class="hover" title="患者ID：<?=hh($row["ptif_id"])?>"
                onclick="dfmPtifDetail.selectPtifAndShowOndoban('<?=js($row["ptif_id"])?>')">
                <?if ($bldg_cd=="all" && @$row["ward_name"]) {?>
                <table cellspacing="0" cellpadding="0" class="ward_ptrm_panel" onmousedown="dfmPtifDetail.popup('<?=js($row["ptif_id"])?>');return false;"><tr>
                <td><?=wbr(@$row["ward_name"])?></td></tr></table>
                <? } ?>
                <table cellspacing="0" cellpadding="0" class="ward_ptrm_panel" onmousedown="dfmPtifDetail.popup('<?=js($row["ptif_id"])?>');return false;"><tr>
                <?if (@$row["ptrm_name"]) {?>
                <td><?=wbr(@$row["ptrm_name"])?></td>
                <? } else { ?>
                <td class="no_ptrm">病室指定なし</td>
                <? } ?>
                </tr></table>
                <div style="padding:3px 0 2px 3px;<?if ($ptif_id==$row["ptif_id"]){?>border-left:6px solid #ff0000;<? } ?>"><?=wbr($ptif_name)?></div>
            </td>

            <?
                // ----------------------------------------------------------------
                // 週次備考(今週)
                // ----------------------------------------------------------------
                $data_list = $siji_list[$row["ptif_id"]."@".$monday_ymd]["weekly_bikou_list"];
                $field_count = $data_list["weekly_bikou__field_count"];
            ?>
            <td rowspan="2" class="bikou_cell"
                onmouseover="$(this).addClass('hover');" onmouseout="$(this).removeClass('hover')"
                onclick="stopEvent(event); dfmSijibanBikou.popup('weekly_bikou','<?=js($row["ptif_id"])?>','<?=$monday_ymd?>',0)">
                <? for ($fnum=1; $fnum<=$field_count; $fnum++) { ?>
                    <div style="border-bottom:1px dashed #ff9999;" class="hover bikou_cell" onmouseover="return stopEvent(event);"
                    onclick="stopEvent(event); dfmSijibanBikou.popup('weekly_bikou','<?=js($row["ptif_id"])?>','<?=$monday_ymd?>',<?=$fnum?>)">
                        <? if ($data_list["weekly_bikou__".$fnum."__is_henkou"]) echo '<span class="is_stop">変</span>'; ?>
                        <?=nl2br2(wbr($data_list["weekly_bikou__".$fnum."__siji_text"]))?></div>
                <? } ?>
                <? if(!$printMode){?><div class="new_entry"><div>【新規作成】</div></div><?}?>
            </td>

            <?
                // ----------------------------------------------------------------
                // 注射指示
                // ----------------------------------------------------------------
                for ($idx = 0; $idx <= 6; $idx++){
                    $data_list = $siji_list[$row["ptif_id"]."@".$WYL[$idx]]["chusya_list"];
                    $field_count = $data_list["chusya__field_count"];
            ?>
            <td style="height:22px" class="siji_cell<?=@$taiin_classname[$idx]?>" onmouseover="$(this).addClass('hover').addClass('hover1')"
                onmouseout="$(this).removeClass('hover').removeClass('hover1')"
                onclick="stopEvent(event); dfmSijiban.popup('<?=js($row["ptif_id"])?>','<?=$WYL[$idx]?>',0)">
                <? for ($fnum=1; $fnum<=$field_count; $fnum++) { ?>

                    <div style="border-bottom:1px dashed #ff9999;" class="siji_cell<?=@$taiin_classname[$idx]?> hover"
                    onmouseover="$(this).addClass('hover2');return stopEvent(event);" onmouseout="$(this).removeClass('hover2')"
                    onclick="stopEvent(event); dfmSijiban.popup('<?=js($row["ptif_id"])?>','<?=$WYL[$idx]?>',<?=$fnum?>)">
                        <div class="siji_panel hover2child">
                        <?
                        $prefix = "chusya__".$fnum."__";
                        echo $fnum.".";
                        if ($data_list[$prefix."siji_az"]) echo '<span class="az">'.wbr(get_alias($data_list[$prefix."siji_az"])).'</span>';
                        if ($data_list[$prefix."is_continue"]) echo '<span class="is_continue">継</span>';
                        if ($data_list[$prefix."siji_picture_names"]) echo '<span class="pict">画'.count(explode(",",$data_list[$prefix."siji_picture_names"])).'</span>';
                        if ($data_list[$prefix."is_sumi"]) echo '<span class="is_sumi">済</span>';
                        if ($data_list[$prefix."is_stop"]) echo '<span class="is_stop">止</span>';
                        ?>
                        </div>
                        <div><?=nl2br2(wbr($data_list[$prefix."siji_text"]))?></div>
                    </div>
                <? } ?>
                <? if(!$printMode){?><div class="new_entry"><div>【新規作成】</div></div><?}?>
            </td>
            <? } ?>
            <?
                // ----------------------------------------------------------------
                // 週次備考(翌週)
                // ----------------------------------------------------------------
                $data_list = $siji_list[$row["ptif_id"]."@".$next_monday_ymd]["weekly_bikou_list"];
                $field_count = $data_list["weekly_bikou__field_count"];
            ?>
            <td rowspan="2" class="bikou_cell"
                onmouseover="$(this).addClass('hover')" onmouseout="$(this).removeClass('hover')"
                onclick="stopEvent(event); dfmSijibanBikou.popup('weekly_bikou','<?=js($row["ptif_id"])?>','<?=$monday_ymd?>',0, '<?=$next_monday_ymd?>')">
                <? for ($fnum=1; $fnum<=$field_count; $fnum++) { ?>
                    <div style="border-bottom:1px dashed #ff9999;" class="hover bikou_cell" onmouseover="return stopEvent(event);"
                    onclick="stopEvent(event); dfmSijibanBikou.popup('weekly_bikou','<?=js($row["ptif_id"])?>','<?=$monday_ymd?>',<?=$fnum?>, '<?=$next_monday_ymd?>')">
                        <? if ($data_list["weekly_bikou__".$fnum."__is_henkou"]) echo '<span class="is_stop">変</span>'; ?>
                        <?=nl2br2(wbr($data_list["weekly_bikou__".$fnum."__siji_text"]))?></div>
                <? } ?>
                <? if(!$printMode){?><div class="new_entry"><div>【新規作成】</div></div><?}?>
            </td>
        </tr>
        <tr>
        <?
            // ----------------------------------------------------------------
            // MEMO
            // ----------------------------------------------------------------
            for ($idx = 0; $idx <= 6; $idx++){
                $data_list = $siji_list[$row["ptif_id"]."@".$WYL[$idx]]["daily_bikou_list"];
                $field_count = $data_list["daily_bikou__field_count"];
        ?>
            <td style="height:22px" class="bikou_cell<?=@$taiin_classname[$idx]?>"
                onmouseover="$(this).addClass('hover')" onmouseout="$(this).removeClass('hover')"
                onclick="stopEvent(event); dfmSijibanBikou.popup('daily_bikou','<?=js($row["ptif_id"])?>','<?=$WYL[$idx]?>',0)">
                <? for ($fnum=1; $fnum<=$field_count; $fnum++) { ?>
                    <div style="border-bottom:1px dashed #ff9999;" class="hover bikou_cell<?=@$taiin_classname[$idx]?>" onmouseover="return stopEvent(event);"
                    onclick="stopEvent(event); dfmSijibanBikou.popup('daily_bikou','<?=js($row["ptif_id"])?>','<?=$WYL[$idx]?>',<?=$fnum?>)">
                        <? if ($data_list["daily_bikou__".$fnum."__is_henkou"]) echo '<span class="is_stop">変</span>'; ?>
                        <?=nl2br2(wbr($data_list["daily_bikou__".$fnum."__siji_text"]))?></div>
                <? } ?>
                <? if(!$printMode){?><div class="new_entry"><div>【新規作成】</div></div><?}?>
            </td>
        <? } ?>
        </tr>

        <? // 印刷患者ごと改ページモードの場合 ?>
        <? if ($printMode=="2") { ?></table><? } ?>


    <? } ?>

<? // 印刷患者ごと改ページモードでない、通常モードの場合 ?>
<? if ($printMode!="2") { ?>
</table>
<? } ?>












<? //**************************************************************************************************************** ?>
<? // SOAP板の場合                                                                                                    ?>
<? //**************************************************************************************************************** ?>
<? } else if ($view_mode=="soap") { ?>

<? if($stickyMenubar) { ?>
<table cellspacing="0" cellpadding="0" class="sijiban_soap_table" id="sijiban_soap_table_float_header">
    <tr>
        <th height="24" width="9%">患者名</th>
        <? for ($i = 1; $i <= 7; $i++){ ?><th width="13%"><? $a_ymd = ymdhm_to_array($WYL[$i-1]); echo $a_ymd["slash_mdw"];?></th><? } ?>
    </tr>
</table>
<? } ?>


<? // 印刷患者ごと改ページモードでない、通常モードの場合 ?>
<? if ($printMode!="2") { ?>
<table cellspacing="0" cellpadding="0" class="sijiban_soap_table">
    <tr>
        <th width="9%" height="24">患者名</th>
        <? for ($i = 1; $i <= 7; $i++){ ?><th width="13%"><? $a_ymd = ymdhm_to_array($WYL[$i-1]); echo $a_ymd["slash_mdw"];?></th><? } ?>
    </tr>
<? } ?>


    <?
        //=================================================
        // 指定週のSOAP内容・日次備考一覧
        //=================================================
        $ptif_name_sql = make_ptif_name_sql($ptif_name_find);
        $sql =
        " select siji.ptif_id, siji.serial_data_daily_bikou, siji.serial_data_soap, siji.ymd".
        " from sum_ks_siji_ymd siji".
        " left outer join sum_ks_mst_pat_attribute opa on (opa.ptif_id = siji.ptif_id)".
        " left outer join (".
        "     select ptif_id from sum_ks_mst_pat_nyuin_taiin".$where2.
        "     group by ptif_id".
        " ) nt on ( nt.ptif_id = siji.ptif_id )".
        " where 1 = 1 ".$where1.
        " ".($ptif_name_sql ? " and siji.ptif_id in (select ptif_id from ptifmst where 1 = 1 ".$ptif_name_sql.")" : "").
        " and siji.ymd >= " . dbES($monday_ymd) . // 週頭月曜から
        " and siji.ymd <= " . dbES($next_monday_ymd); // 週末日曜まで
        $sel = dbFind($sql);
        $siji_list = array();
        while ($row = @pg_fetch_array($sel)) {
            $prefix = $row["ptif_id"]."@".$row["ymd"];
            $siji_list[$prefix]["soap_list"] = unserialize($row["serial_data_soap"]);
            $siji_list[$prefix]["daily_bikou_list"] = unserialize($row["serial_data_daily_bikou"]);
        }
        $loop = 0;
        while ($row = @pg_fetch_array($ptif_all)) {
            $loop++;
            $ptif_name = $row["ptif_lt_kaj_nm"]." ".$row["ptif_ft_kaj_nm"];
            $taiin_classname = calc_taiin_classname($row["nyuin_taiin_list"], $WYL);
        ?>

        <? // 印刷患者ごと改ページモードの場合 ?>
        <? if ($printMode=="2") { ?>
        <table cellspacing="0" cellpadding="0" class="sijiban_soap_table" style="margin-bottom:5px;<?=($loop>1 ? 'page-break-before:always;':'')?>">
            <tr>
                <th width="9%" height="24">患者名</th>
                <? for ($i = 1; $i <= 7; $i++){ ?><th width="13%"><? $a_ymd = ymdhm_to_array($WYL[$i-1]); echo $a_ymd["slash_mdw"];?></th><? } ?>
            </tr>
        <? } ?>

        <tr>

        <td rowspan="2" class="hover" title="<?=hh($row["ptif_id"])?>"
            onclick="dfmPtifDetail.selectPtifAndShowOndoban('<?=js($row["ptif_id"])?>')">
            <?if ($bldg_cd=="all" && @$row["ward_name"]) {?>
            <table cellspacing="0" cellpadding="0" class="ward_ptrm_panel" onmousedown="dfmPtifDetail.popup('<?=js($row["ptif_id"])?>');return false;"><tr>
            <td><?=wbr(@$row["ward_name"])?></td></tr></table>
            <? } ?>
            <table cellspacing="0" cellpadding="0" class="ward_ptrm_panel" onmousedown="dfmPtifDetail.popup('<?=js($row["ptif_id"])?>');return false;"><tr>
            <?if (@$row["ptrm_name"]) {?>
            <td><?=wbr(@$row["ptrm_name"])?></td>
            <? } else { ?>
            <td class="no_ptrm">病室指定なし</td>
            <? } ?>
            </tr></table>
            <div style="padding:3px 0 2px 3px;<?if ($ptif_id==$row["ptif_id"]){?>border-left:6px solid #ff0000;<? } ?>"><?=wbr($ptif_name)?></div>
        </td>

        <?
            // ----------------------------------------------------------------
            // SOAP
            // ----------------------------------------------------------------
            for ($idx = 0; $idx <= 6; $idx++){
                $data_list = $siji_list[$row["ptif_id"]."@".$WYL[$idx]]["soap_list"];
                $field_count = $data_list["soap__field_count"];
                $soap_list = array();
                for ($idx2 = 1; $idx2 <= $field_count; $idx2++) {
                    $prefix = "soap__".$idx2."__";
                    $hm = sprintf("%4s", trim(@$data_list[$prefix."action_hm"])).sprintf("%3s", $idx2);
                    //$soap_list[$hm] = array(
                    $soap_list[] = array(
                        "soap_jikantai"=>$data_list[$prefix."soap_jikantai"],
                        "siji_text"=>$data_list[$prefix."siji_text"],
                        "siji_ptext"=>$data_list[$prefix."siji_ptext"],
                        "action_hm"=>$data_list[$prefix."action_hm"],
                        "field_idx"=>$idx2
                    );
                }
                //ksort($soap_list);


        ?>
            <td style="height:22px" class="soap_cell<?=@$taiin_classname[$idx]?>"
                onmouseover="$(this).addClass('hover')" onmouseout="$(this).removeClass('hover')"
                onclick="stopEvent(event); dfmSijibanSoap.popup('soap','<?=js($row["ptif_id"])?>','<?=$WYL[$idx]?>',0)">
                <?
                    foreach ($soap_list as $soap_row) {
                        $hm = $soap_row["action_hm"];
                        $soap_jikantai = trim($soap_row["soap_jikantai"]);
                        $jikantai = "";
                        if ($soap_jikantai=="nikkin") $jikantai = '<span class="nikkin">日</span>';
                        if ($soap_jikantai=="junya") $jikantai = '<span class="junya">準</span>';
                        if ($soap_jikantai=="sinya") $jikantai = '<span class="sinya">深</span>';
                        if (strlen($hm)==4) $hm = substr($hm,0,2).":".substr($hm,2);
                        if ($hm!="") $hm = '<span class="action_hm">'.$hm.'</span>';
                    ?>
                    <div style="border-bottom:1px dashed #ff9999;" class="hover soap_cell<?=@$taiin_classname[$idx]?>" onmouseover="return stopEvent(event);"
                    onclick="stopEvent(event); dfmSijibanSoap.popup('soap','<?=js($row["ptif_id"])?>','<?=$WYL[$idx]?>',<?=$soap_row["field_idx"]?>)">
                        <?=$hm.$jikantai.nl2br2(wbr($soap_row["siji_text"]))?>
                        <? if ($soap_row["siji_ptext"]!="") {?>
                        <br><span class="action_hm">P</span>
                        <?=nl2br2(wbr($soap_row["siji_ptext"]))?>
                        <? } ?>
                    </div>
                <? } ?>
                <? if(!$printMode){?><div class="new_entry"><div>【新規作成】</div></div><?}?>
            </td>
        <? } ?>
        </tr>

        <tr>
        <?
            // ----------------------------------------------------------------
            // MEMO
            // ----------------------------------------------------------------
            for ($idx = 0; $idx <= 6; $idx++){
                $data_list = $siji_list[$row["ptif_id"]."@".$WYL[$idx]]["daily_bikou_list"];
                $field_count = $data_list["daily_bikou__field_count"];
        ?>
            <td style="height:22px" class="bikou_cell<?=@$taiin_classname[$idx]?>"
                onmouseover="$(this).addClass('hover')" onmouseout="$(this).removeClass('hover')"
                onclick="stopEvent(event); dfmSijibanBikou.popup('daily_bikou','<?=js($row["ptif_id"])?>','<?=$WYL[$idx]?>',0)">
                <? for ($fnum=1; $fnum<=$field_count; $fnum++) { ?>
                    <div style="border-bottom:1px dashed #ff9999;" class="bikou_cell<?=@$taiin_classname[$idx]?> hover" onmouseover="return stopEvent(event);"
                    onclick="stopEvent(event); dfmSijibanBikou.popup('daily_bikou','<?=js($row["ptif_id"])?>','<?=$WYL[$idx]?>',<?=$fnum?>)">
                        <? if ($data_list["daily_bikou__".$fnum."__is_henkou"]) echo '<span class="is_stop">変</span>'; ?>
                        <?=nl2br2(wbr($data_list["daily_bikou__".$fnum."__siji_text"]))?></div>
                <? } ?>
                <? if(!$printMode){?><div class="new_entry"><div>【新規作成】</div></div><?}?>
            </td>
        <? } ?>
        </tr>
        <? // 印刷患者ごと改ページモードの場合 ?>
        <? if ($printMode=="2") { ?></table><? } ?>
    <? } ?>



<? // 印刷患者ごと改ページモードでない、通常モードの場合 ?>
<? if ($printMode!="2") { ?></table><? } ?>


<? } ?>
<?
}

