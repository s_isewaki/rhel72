<?php
    define("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING", 1);
    require_once("ks_common.php");
    require_once("ks_ajax.php");

    if (!$_REQUEST["monday_ymd"]) $_REQUEST["monday_ymd"] = date("Ymd");
    $WYL = create_week_ymd_list($_REQUEST["monday_ymd"]);
    $monday_ymd = $WYL[0];
    $sunday_ymd = $WYL[6];
    $a_ymd0 = ymdhm_to_array($WYL[0]);
    $a_ymd6 = ymdhm_to_array($WYL[6]);
    $monday_slash_ymd = $a_ymd0["slash_ymd"];
    $sunday_slash_ymd = $a_ymd6["slash_ymd"];


	$orderby = $_REQUEST["orderby"];
	if (!$orderby) $orderby = "display_time@desc";

	$a_orderby = explode("@", $orderby);
	$ary = array("display_time", "ptif_id", "ptif_name", "ymd", "siji_content_idx", "update_emp_name");
	if (!in_array($a_orderby[0], $ary)) $a_orderby[0] = "display_time";

	if ($a_orderby[1]!="desc") $a_orderby[1] = "asc";
	$orderby = implode("@", $a_orderby);

	$ymd_type = ($_REQUEST["ymd_type"]=="siji_ymd" ? "siji_ymd": "display_time");


//****************************************************************************************************************
// 指示記入履歴の一覧
//****************************************************************************************************************
function get_siji_rireki_list($monday_ymd, $sunday_ymd, $kako_check, $a_orderby) {
	global $siji_content_names;
	$siji_content_cases = array();
	$idx = 0;
	foreach ($siji_content_names as $k => $jp) {
		$idx++;
		$siji_content_cases[]= " when '".$k."' then ".$idx;
	}
	$ymd_sql = " and ymd between '".$monday_ymd."' and '".$sunday_ymd."'";
	if ($ymd_type=="display_time") $ymd_sql = " and display_time between '".slash_ymd($monday_ymd)." 00:00:00' and '".slash_ymd($sunday_ymd)." 99:99:99'";

    $sql =
    " select * from (".
    " select uh.ptif_id, uh.siji_content, uh.display_time, uh.ymd, uh.ins_upd_del, uh.update_emp_name".
    ",case siji_content ".implode("", $siji_content_cases)." else 0 end as siji_content_idx".
    ",pm.ptif_lt_kaj_nm || ' ' || pm.ptif_ft_kaj_nm as ptif_name".
    " from sum_ks_update_history_siji uh".
    " left outer join ptifmst pm on (pm.ptif_id = uh.ptif_id)".
    " where 1 = 1".
    " ".$ymd_sql.
    " and siji_content in ('".implode("','", array_keys($siji_content_names))."')".
    " ".($kako_check=="1" ? " and (ymd < '".$monday_ymd."' or ymd > '".$sunday_ymd."')" : ""). // 期間外
    " ".($kako_check=="2" ? " and ymd between '".$monday_ymd."' and '".$sunday_ymd."'" : ""). // 期間内
    " ) d order by ".$a_orderby[0]." ".$a_orderby[1];
    $sel = dbFind($sql);
    $html = array();
    $html[]= '<table cellspacing="0" cellpadding="0" class="sijiban_soap_table">';
    $html[]= '<tr><th width="100">患者ID</th><th width="150">患者氏名</th><th width="180">指示日(更新対象日)</th>';
    $html[]= '<th width="180">記入日時</th><th width="120">指示更新対象</th><th width="150">職員名</th></tr>';
    while ($row = @pg_fetch_array($sel)) {

        $html[]='<tr>';
        $html[]= '<td>'.hh($row["ptif_id"]).'</td>';
        $html[]= '<td>'.hh($row["ptif_name"]).'</td>';
        $html[]= '<td>'.slash_ymd($row["ymd"]).'</td>';
        $html[]= '<td>'.hh($row["display_time"]).'</td>';
        $html[]= '<td>'.$siji_content_names[$row["siji_content"]].'</td>';
        $html[]= '<td>'.hh($row["update_emp_name"]).'</td>';
        $html[]='</tr>';
    }
    $html[]="</table>";
    if (defined("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING")) {
        return implode("\n",$html);
    }
    echo "ok".implode("\n",$html);
}



?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/start/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" type="text/css" href="css/common.css?v=2">
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.js?v=2"></script>
<script type="text/javascript" src="js/common.js?v=2"></script>
<script type="text/javascript" src="js/showpage.js"></script>

<script type="text/javascript">
    var moveWeek = function(day_incriment){
        var monday_ymd = "<?=$monday_ymd?>";
        var dt = new Date(int(substr2(monday_ymd,0,4)), int(substr2(monday_ymd,4,2))-1, int(substr2(monday_ymd,6,2)));
        dt.setTime(dt.getTime() + day_incriment * 86400000);
        document.frm.monday_ymd.value = dt.getFullYear() + ("00"+(dt.getMonth()+1)).slice(-2) + ("00"+dt.getDate()).slice(-2);
        document.frm.submit();
    }
    function loaded() {
        if (isNotPC) ee("body").className = "is_not_pc";
        else ee("body").className = "is_pc";
    }
</script>
<title>CoMedix | 看護支援 - 指示記入履歴検索</title>
</head>
<body onload="loaded()" id="body">





<? //**************************************************************************************************************** ?>
<? // 画面上部 メインメニュー                                                                                         ?>
<? //**************************************************************************************************************** ?>
<div id="header_menu_div">
<form id="frm" name="frm">
<input type="hidden" name="monday_ymd" value="<?=h($monday_ymd)?>" />
<input type="hidden" name="session" value="<?=GG_SESSION_ID?>" />
<table cellspacing="0" cellpadding="0" id="header_menu_table" style="width:100%; height:30px"><tr>
    <td class="weekbtn" style="text-align:center; width:140px; background:url(img/menubtn_on.gif) repeat-x"><nobr>指示記入履歴検索</nobr></td>
    <td style="width:auto; padding-left:10px;"><nobr>
        <table cellspacing="0" cellpadding="0" style="width:auto"><tr>
            <td style="vertical-align:middle">
            <select name="ymd_type" onchange="document.frm.submit()">
            	<option value="siji_ymd"<?=($ymd_type=="siji_ymd"?" selected":"")?>>指示日</option>
            	<option value="display_time"<?=($ymd_type=="display_time"?" selected":"")?>>記入日</option>
            </select>
            </td>
            <td style="padding-left:6px; vertical-align:middle; width:190px">
                <span style="background-color:#3171dd"><?=$monday_slash_ymd?> 〜 <?=$sunday_slash_ymd?></span>
            </td>
            <td style="padding-left:6px; vertical-align:middle">
                <button type="button" onclick="moveWeek('-7');">≪前週</button>
                <button type="button" onclick="moveWeek('7');">翌週≫</button>
            </td>
            <td style="padding-left:20px; vertical-align:middle">


            </td>
            <td style="padding-left:6px; vertical-align:middle;">
            <select name="kako_check" onchange="document.frm.submit()">
            	<option value=""></option>
            	<option value="1"<?=($kako_check=="1"?" selected":"")?>>指示日が記入日期間に含まれない</option>
            	<option value="2"<?=($kako_check=="2"?" selected":"")?>>指示日が記入日期間に含まれる</option>
            </select>
            <select name="orderby" onchange="document.frm.submit()">
            	<option value="ptif_id@asc"<?=($orderby=="ptif_id@asc"?" selected":"")?>>患者ID 昇順</option>
            	<option value="ptif_name@asc"<?=($orderby=="ptif_name@asc"?" selected":"")?>>患者氏名(漢字) 昇順</option>
            	<option value="ymd@asc"<?=($orderby=="ymd@asc"?" selected":"")?>>指示日 昇順</option>
            	<option value="display_time@desc"<?=($orderby=="display_time@desc"?" selected":"")?>>記入日時 降順</option>
            	<option value="siji_content_idx@asc"<?=($orderby=="siji_content_idx@asc"?" selected":"")?>>指示更新対象別</option>
            	<option value="update_emp_name@asc"<?=($orderby=="update_emp_name@asc"?" selected":"")?>>職員名(漢字) 昇順</option>
            </select>
            </td>
        </tr></table>
    </nobr></td>
    <td style="width:50px"><a class="menubtn closebtn" href="javascript:void(0)" onclick="window.close();">×</a></td>
</tr></table>
</form>
</div>





<div style="display:none">


<? //**************************************************************************************************************** ?>
<? // ヘルプ                                                                                                          ?>
<? //**************************************************************************************************************** ?>
<form id="efmDrEmpIdInstruction">
<div style="padding-top:10px">この画面は、患者別で経口摂取量の週間合計を表示します。</div>
<hr>

<div style="padding-top:10px">◆「分子/分母」形式で表示されます。単位は「割/割」です。分子は摂取量の加算値です。</div>
<div style="padding-left:20px">分母は10の倍数となります。１週間（＝７日）すべての経口が登録された場合に、最大値70となります。</div>

<div style="padding-top:10px">◆経口摂取量に0〜10を登録されている場合に計算対象となります。空欄の場合は計算されません。</div>
<div style="padding-left:20px">絶食をチェックしている場合は、経口摂取量はゼロとみなし、計算対象に含まれます。</div>

<div style="padding-top:10px">例１）任意日付に「朝：副食」＝1、「朝：主食」＝3　を指定した場合</div>
<div style="padding-left:20px">⇒一覧上『朝（副）』：　分母に10加算／分子に1加算</div>
<div style="padding-left:20px">⇒一覧上『朝（主）』：　分母に10加算／分子に3加算</div>
<div style="padding-top:10px">例２）任意日付に「朝：副食」＝0、「朝：主食」＝10　を指定した場合</div>
<div style="padding-left:20px">⇒一覧上『朝（副）』：　分母に10加算／分子は加算なし</div>
<div style="padding-left:20px">⇒一覧上『朝（主）』：　分母に10加算／分子にも10加算</div>
<div style="padding-top:10px">例３）任意日付に「朝：副食」＝空欄、「朝：主食」＝9　を指定した場合</div>
<div style="padding-left:20px">⇒一覧上『朝（副）』：　分子分母ともに加算なし</div>
<div style="padding-left:20px">⇒一覧上『朝（主）』：　分母に10加算／分子に9加算</div>
<div style="padding-top:10px">例４）任意日付の「朝」に「絶食」を指定した場合</div>
<div style="padding-left:20px">⇒一覧上『朝（副）』『朝（主）』ともに、分母に10加算／分子は加算なし</div>
</form>




</div>


<div id="bed_list_container" style="padding-bottom:20px;">
<?=get_siji_rireki_list($monday_ymd, $sunday_ymd, $_REQUEST["kako_check"], $a_orderby)?>
</div>




</body>
</html>
<?
pg_close($con);
