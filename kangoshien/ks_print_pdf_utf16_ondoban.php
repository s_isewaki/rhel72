<?
define("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING", 1);
require_once("ks_common.php");
require_once("ks_ajax_ondoban.php");
require_once('fpdf_pmincho_utf16.php');
ob_clean();

$ptif_id = $_REQUEST["ptif_id"];
list($bldg_cd, $ward_cd) = explode("_", $_REQUEST["bldg_ward"]);
$target_ymd = $_REQUEST["ymd"];
$daily_weekly = $_REQUEST["daily_weekly"]; // daily or weekly
$ptif_name_find = $_REQUEST["ptif_name_find"];

$WYL = create_week_ymd_list($target_ymd);
if ($daily_weekly=="daily") $WYL = array($target_ymd);
$day_size = count($WYL);
$ymd_to = $WYL[$day_size-1];


// 横サイズ 195-15 = 180
// １列目：8x5 = 40
// ２列目以降の７列：20 * 7 = 140
// 行高；4
// グラフ高さ

$most_left = 15;
$most_right = 194;
$most_top = 25;
$most_btm = 290;
$hwidth = 32; // 一番左の列（ヘッダ列）の幅
$fwidth = ($most_right - $most_left - $hwidth) / $day_size; // １日あたりのデータフィールドの幅
$tmpw = $most_left + $hwidth;
$lpos = array($tmpw, $tmpw+($fwidth*1), $tmpw+($fwidth*2), $tmpw+($fwidth*3), $tmpw+($fwidth*4), $tmpw+($fwidth*5), $tmpw+($fwidth*6));
if ($daily_weekly=="daily") $lpos = array($tmpw);
$graphHeight = 60;
$graphWidth = $fwidth * $day_size;
$graphStartLeft = $most_left + $hwidth;
$widthPerMinute = $graphWidth / (60*24*$day_size);
$graphStartTop = 30;
$graphEndTop = 0;

















//*************************************************************************
// PDFここから（その１）
//*************************************************************************

//=================================================
// PDF生成ライブラリの拡張クラス定義
// ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
//=================================================
if (!defined("CustomFPDF_LOADED")) {
class CustomFPDF extends FPDF_PMINCHO_UTF16 {

  var $currentPageNo;       // 現在描画しているページの番号

    // ■の描画
    function DotSquare1($x0,$y0,$clr="") {
        $this->setXY($x0-0.1, $y0-0.1);
        $lw = $this->LineWidth;
        $this->_out('2.5 w'); // 線幅
        if (!$clr) $this->SetDrawColor(27, 156, 171);
        if ($clr=="sanso") $this->SetDrawColor(128, 100, 162); // 紫
        if ($clr=="kouseizai") $this->SetDrawColor(155, 187, 89); // 草色
        if ($clr=="kokyuki") $this->SetDrawColor(255, 145, 85); // みかん色
        $this->UniCell(0.2, 0.2, "",1,0);
        $this->_out(sprintf('%.2f w', $lw)); // 線幅を戻す
        $this->SetDrawColor(90,90,90); // 色を戻す
    }

    // □の描画
    function DotSquare2($x0,$y0,$clr="") {
        $this->_out('0.2 w'); // 線幅
        $this->_out('1 1 1 rg'); // 白色
        $this->Rect($x0-0.7, $y0-0.7, 1.5, 1.5 , "F");
        if (!$clr) $this->SetDrawColor(0, 0, 255); // 青
        if ($clr=="sanso") $this->SetDrawColor(128, 100, 162); // 紫
        if ($clr=="kouseizai") $this->SetDrawColor(155, 187, 89); // 草色
        if ($clr=="kokyuki") $this->SetDrawColor(255, 145, 85); // みかん色
        $this->Rect($x0-0.7, $y0-0.7, 1.5, 1.5 , "");
        $this->SetDrawColor(90,90,90); // 色を戻す
        $this->_out(sprintf('%.2f w', $lw)); // 線幅を戻す
    }

    // ＋の描画
    function DotCross($x0,$y0) {
        $lw = $this->LineWidth;
        $this->SetDrawColor(0, 136, 34);
        $this->_out('0.3 w'); // 線幅
        $this->Line($x0, $y0-0.7, $x0, $y0+0.7);
        $this->Line($x0-0.7, $y0, $x0+0.7, $y0);
        $this->_out(sprintf('%.2f w', $lw)); // 線幅を戻す
        $this->SetDrawColor(90,90,90); // 色を戻す
    }

    // バツの描画
    function DotBatsu($x0,$y0) {
        $lw = $this->LineWidth;
        $this->SetDrawColor(0, 0, 0);
        $this->_out('0.3 w'); // 線幅
        $this->Line($x0-0.7, $y0-0.7, $x0+0.7, $y0+0.7);
        $this->Line($x0-0.7, $y0+0.7, $x0+0.7, $y0-0.7);
        $this->_out(sprintf('%.2f w', $lw)); // 線幅を戻す
        $this->SetDrawColor(90,90,90); // 色を戻す
    }

    // ●の描画
    function DotCircle($x0,$y0) {
        $w1 = 4;
        $y1 = ($this->h-($y0)) * $this->k + 2;
        $x1 = ($x0) *  $this->k;
        $lw = $this->LineWidth;
        $fc = $this->FillColor;
        $this->_out('0.1 w'); // 線幅
        $this->_out('1 0 0 rg'); // 赤色、ぬりつぶし色
        $r = 0.66; // 真円のレート
        $this->_out(sprintf('%.2f %.2f m', $x1, $y1)); // 開始点に移動
        $this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c', ($x1+$w1*$r), $y1, ($x1+$w1*$r), ($y1-$w1), $x1, $y1-$w1)); // ベジエ曲線で半円描画
        $this->_out('f'); // 塗りつぶしセット
        $this->_out(sprintf('%.2f %.2f m', $x1, $y1)); // 開始点に移動
        $this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c', ($x1-$w1*$r), $y1, ($x1-$w1*$r), ($y1-$w1), $x1, $y1-$w1)); // ベジエ曲線で半円描画
        $this->_out('f'); // 塗りつぶしセット
        $this->_out(sprintf('%.2f w', $lw)); // 線幅を戻す
        $this->SetDrawColor(90,90,90); // 色を戻す
    }

    // 文字列を設定して、利用した領域の高さを返す
    function Plot($x, $y, $cellWidth, $txt, $defaultHeight=4, $minsize=5.0, $align="C", $is_ignore_enter=1){
        $txt = trim($txt);
        if (!$y) $y = $this->GetY();
        if ($txt=="") return 0;
        $fsize = 8.5;// 標準フォントサイズ8とする（直後に-0.5となる）
        $rate = 0;
        $wmax = $cellWidth - 2*$this->cMargin; // 実質描画可能幅
        // 収まるサイズにフォントを縮小する
        while ($fsize >= $minsize && $rate <=1.0){
            $fsize = $fsize - 0.5;
            $this->SetFontSize($fsize);
            $str_width = $this->GetStringWidth(utf16($txt));
            $rate = $wmax / $str_width;
        }
        $h = $defaultHeight - (8-$fsize)*0.5;
        $this->SetXY($x-$this->cMargin+0.1, $y);
        $this->MultiCell($cellWidth+($this->cMargin-0.1)*2, $h, $txt, 0, $align, false, $is_ignore_enter);
        $ret = $this->GetY() - $y;
        if ($ret > $defaultHeight) $ret = $ret + 0.5;
        return $ret;
    }

  // ページ共通のヘッダー部を出力する
  function writeHeader($pt_info) {
    global $most_left;
    global $most_right;
    global $WYL;
    global $lpos;
    global $fwidth;
    global $hwidth;

    $this->setXY(0, 10);
    $this->SetFontSize(10);
    $this->UniCell(0, 5.0, "個人情報用紙",'',0,'C');
    $this->setXY(0, 10);

    $this->setXY(0, 15);
    $this->SetFontSize(8);
    //$this->UniCell(0, 5.0, date("Y/m/d H:i:s")."　　",'',0,'R');

    $this->setXY($most_left, 20);
    $this->SetFontSize(9);
    $this->UniCell(0, 5.0, $pt_info["ward_name"]."　".$pt_info["ptrm_name"]."　　患者：".$pt_info["ptif_id"]."　".$pt_info["ptif_name"],'',0,'L');
    // 日付
    for ($i = 1; $i <= 7; $i++){
        $a_ymd = ymdhm_to_array($WYL[$i-1]);
      $this->Plot($lpos[$i-1], 25, $fwidth, $a_ymd["slash_mdw"]);
    }
     $this->Plot($most_left, 25, $hwidth, substr($WYL[0],0,4)."年", 4, 7, "C");
    // 日付直下の太い線
    global $graphStartTop;
    $this->SetLineWidth(0.4);
    $this->Line($most_left+0.2, $graphStartTop-0.2, $most_right-0.2, $graphStartTop-0.2);
    $this->SetLineWidth(0.2);
    return $graphStartTop;
  }

    // 改ページする
  function AddPage2() {
    $this->AddPage();
    $this->currentPageNo++;
  }

  function check_kaipage(&$curY, &$kaipage_counter, $addHeight, $pt_info, $is_end=0) {
    $kaipage_counter++;
    $this->SetDrawColor(90,90,90);
    global $most_top;
    global $most_btm;
    global $most_left;
    global $most_right;
    global $fwidth;
    global $lpos;
    global $WYL;
    global $graphEndTop;

    if (($curY + $addHeight <= $most_btm - 4) && !$is_end) {
        $curY += $addHeight;
        return false; // 改ページは実施しなかった。
    }

    if ($kaipage_counter==1) {
        if (!$is_end) {
            // 描いたばかりの行を消す。（指定範囲を白で塗りつぶす）
            $this->SetFillColor(255, 255, 255);
            $this->Rect($most_left, $curY, $most_right - $most_left, 297 - $curY, "F");
        }
    }
    // ここまでの縦罫線を描く。最初のページは、いちばん左の線以外は、グラフ部には描線しない。
    for ($i = 0; $i < count($lpos); $i++) {
        $topY = $most_top;
        if ($this->currentPageNo==1 && $i > 0) $topY = $graphEndTop;
        $this->Line($lpos[$i], $topY, $lpos[$i], $curY);
    }

    // 外周罫線を描く
    $this->SetDrawColor(90,90,90);
    $this->Line($most_left, $most_top, $most_right, $most_top);
    $this->Line($most_right, $most_top, $most_right, $curY);
    $this->Line($most_right, $curY, $most_left, $curY);
    $this->Line($most_left, $curY, $most_left, $most_top);

    // ページ番号を描く（複数ページがある場合）
    if (!$is_end || $this->currentPageNo>1) {
        $this->Plot($most_left, $most_btm, $most_right - $most_left, $this->currentPageNo."ページ");
    }

    if (!$is_end) {
        $this->AddPage2(); // 新ページを作成
        // ヘッダーを出力する
        $curY = $this->writeHeader($pt_info);
    }
    return ($kaipage_counter > 1 ? false : true); // 改ページ実施したらTRUE。ただし、やりなおしている場合はFALSE
  }

}
}
define("CustomFPDF_LOADED", 1);
//=================================================
// ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
// PDF生成ライブラリの拡張クラス定義
//=================================================

mb_internal_encoding("unicode");
$pdf = new CustomFPDF();
$pdf->AddUniJISFont();
$pdf->SetFont("FontFamily1", "", 10);
$pdf->SetAutoPageBreak(false);
$pdf->Open();



//*************************************************************************
// PDFここまで（その１）
//*************************************************************************





//=================================================
// 指定週の入院中患者一覧
//=================================================
$sql =
" select * from (".
" select pt.ptif_id, pt.ptif_lt_kaj_nm || ' ' || pt.ptif_ft_kaj_nm as ptif_name, bw.ward_name, ptrm.ptrm_name".
",coalesce(opa.bldg_cd,0) as bldg_cd, coalesce(opa.ward_cd,0) as ward_cd, coalesce(opa.ptrm_room_no,0) as ptrm_room_no".
" from ptifmst pt".
" left outer join sum_ks_mst_pat_attribute opa on (opa.ptif_id = pt.ptif_id)".
" left outer join sum_ks_mst_bldg_ward bw on (bw.bldg_cd = opa.bldg_cd and bw.ward_cd = opa.ward_cd)".
" left outer join sum_ks_mst_ptrm ptrm on (ptrm.bldg_cd = opa.bldg_cd and ptrm.ward_cd = opa.ward_cd and ptrm.ptrm_room_no = opa.ptrm_room_no)".
" left outer join (".
"     select ptif_id from sum_ks_mst_pat_nyuin_taiin".
"     where ( nyuin_ymd is not null and nyuin_ymd <> '' and nyuin_ymd <= ".dbES($ymd_to).")".
"     and ( taiin_ymd is null or taiin_ymd = '' or taiin_ymd >= ".dbES($target_ymd).")".
"     group by ptif_id".
" ) nt on ( nt.ptif_id = pt.ptif_id )".
" where 1 = 1".
" ".(!$ptif_id ? " and nt.ptif_id is not null" : "").
" ".($ptif_id ? "and pt.ptif_id = ".dbES($ptif_id) : "").
" ".($bldg_cd && $bldg_cd!="all" && $ward_cd ? " and opa.bldg_cd = ".(int)$bldg_cd." and opa.ward_cd = ".(int)$ward_cd : "").
" ".($bldg_cd && $bldg_cd!="all" && !$ward_cd ? " and ( opa.ward_cd is null or opa.ward_cd = 0 )" : "").
" ) d".
" order by bldg_cd, ward_cd, ptrm_name, ptrm_room_no, ptif_name";
$pt_list = dbGetAll($sql);


foreach($pt_list as $idx => $pt_info) {

    $pdf->currentPageNo = 0;

    $prstY = $pdf->GetY();

    $ondoban_data = get_ondoban_list($pt_info["ptif_id"], $target_ymd, ($daily_weekly=="daily"?$target_ymd:""), "", "", "");
    $graphT = $ondoban_data["graphT"];
    $graphR = $ondoban_data["graphR"];
    $graphP = $ondoban_data["graphP"];
    $graphBP = $ondoban_data["graphBP"];
    $graphL = $ondoban_data["graphL"];
    $graphK = $ondoban_data["graphK"];
    $graphSK = $ondoban_data["graphSK"];
    $update_history = $ondoban_data["update_history"];
    $care_list = $ondoban_data["care_list"];
    $siji_list = $ondoban_data["siji_list"];
    $weekly_insulin_header = $ondoban_data["weekly_insulin_header"];
    $weekly_ben_ari = $ondoban_data["weekly_ben_ari"];
    $weekly_kansatu_header = $ondoban_data["weekly_kansatu_header"];
    $haieki2_exist = $ondoban_data["haieki2_exist"];
    $haieki3_exist = $ondoban_data["haieki3_exist"];


    //*************************************************************************
    // ループ内PDF処理ここから（その２）
    //*************************************************************************
    $pdf->AddPage2();
    $pdf->SetFontSize(9);

    //==================
    // 最初に離床理由の色を塗る
    //==================
    foreach($graphL as $idx => $v){
      $p = explode("\t", $v);
      $frL = $graphStartLeft+$widthPerMinute*$p[0];
      $toL = $graphStartLeft+$widthPerMinute*$p[1];
      $pdf->SetFillColor(232, 217, 255);
      $pdf->Rect($frL, $graphStartTop, $toL-$frL, $graphHeight, "F");
    }


    //==================
    // 次にグラフ内罫線を描く（データ部のみ。左側ヘッダ部はまだ描く必要なし）
    //==================
    $pdf->SetLineWidth(0.2);
    // 細い縦罫線
    $h6 = $fwidth / 4;
    $h1 = $fwidth / 24;
    for ($i = 0; $i < count($lpos); $i++){
        if ($daily_weekly=="daily") {
            for ($i2 = 0; $i2 <= 3; $i2++) {
                $pdf->SetDrawColor(230,230,230);
                for ($i3 = 1; $i3 <= 5; $i3++) {
                    $pdf->Line($lpos[$i]+($h6*$i2)+($h1*$i3), $graphStartTop, $lpos[$i]+($h6*$i2)+($h1*$i3), $graphStartTop+$graphHeight);
                }
                $pdf->SetDrawColor(180,180,180);
                if ($i2>0) $pdf->Line($lpos[$i]+($h6*$i2), $graphStartTop, $lpos[$i]+($h6*$i2), $graphStartTop+$graphHeight);
            }
        } else {
            $pdf->SetDrawColor(230,230,230);
            for ($i2 = 1; $i2 <= 3; $i2++) {
                $pdf->Line($lpos[$i]+($h6*$i2), $graphStartTop, $lpos[$i]+($h6*$i2), $graphStartTop+$graphHeight);
            }
        }
    }

    $pdf->SetDrawColor(180,180,180);
    for ($i = 1; $i <= 9; $i++){
        $pdf->Line($most_left,  30+($i*6), $most_right, 30+($i*6));
    }

    $pdf->SetDrawColor(230,230,230);
    for ($i = 0; $i <= 9; $i++){
        $pdf->Line($most_left,  33+($i*6), $most_right, 33+($i*6));
    }

    $pdf->SetDrawColor(180,180,180);
    for ($i = 1; $i <= 4; $i++){
        $pdf->Line($most_left+($i*8),  30, $most_left+($i*8), 90);
    }


    $pdf->SetDrawColor(90,90,90);
    $pdf->SetLineWidth(0.4);
    $pdf->Line($most_left+0.2, 90,  $most_right-0.2, 90);

    // ピンク線
    $pdf->SetDrawColor(255, 96, 207);
    $pdf->SetLineWidth(0.2);
    $pdf->Line($most_left,  30+(6*6), $most_right, 30+(6*6));

    // 縦罫線
    $h6 = $fwidth / 4;
    $pdf->SetDrawColor(90,90,90);
    for ($i = 0; $i < count($lpos); $i++){
        $pdf->Line($lpos[$i], $most_top, $lpos[$i], $graphStartTop+$graphHeight);
    }

    //==================
    // 次にグラフデータを描く
    //==================



    //**********
    // メイン・BP
    //**********
    $heightPerGaugeBP = $graphHeight / (240 - 40);
    $pdf->SetLineWidth(1.2);
    foreach($graphBP as $idx => $v){
      $p = explode("=", $v);
      $curL = $graphStartLeft+$widthPerMinute*$p[0];
      $ud = explode("_", $p[1]);
      $curU = min($graphStartTop+$graphHeight-1, (240-$ud[0])*$heightPerGaugeBP + $graphStartTop);
      $curD = max($graphStartTop, (240-$ud[1])*$heightPerGaugeBP + $graphStartTop);
      $pdf->SetDrawColor(149, 143, 34); // 濃黄色
      $pdf->Line($curL, $curU, $curL, $curD);
    }
    $pdf->SetLineWidth(0.2);

    //**********
    // メイン・T
    //**********
    $heightPerGaugeT = $graphHeight / (43 - 33);
    $prevL = 0;
    $prevT = 0;
    foreach($graphT as $idx => $v){
      $p = explode("=", $v);
      $curL = $graphStartLeft+$widthPerMinute*$p[0];
      $curT = (43-$p[1])*$heightPerGaugeT + $graphStartTop;
      $pdf->SetDrawColor(0, 0, 255); // 青
      if ($prevL || $prevT) $pdf->Line($prevL, $prevT, $curL, $curT);
      $prevL = $curL;
      $prevT = $curT;
    }
    foreach($graphT as $idx => $v){
      $p = explode("=", $v);
      $curL = $graphStartLeft+$widthPerMinute*$p[0];
      $curT = (43-$p[1])*$heightPerGaugeT + $graphStartTop;
      $pdf->SetDrawColor(0, 0, 255); // 青
      $pdf->DotSquare2($curL, $curT);
    }

    //**********
    // メイン・P
    //**********
    $heightPerGaugeP = $graphHeight / (210 - 10);
    $prevL = 0;
    $prevT = 0;
    foreach($graphP as $idx => $v){
      $p = explode("=", $v);
      $curL = $graphStartLeft+$widthPerMinute*$p[0];
      $curT = (210-$p[1])*$heightPerGaugeP + $graphStartTop;
      $pdf->SetDrawColor(255, 0, 0); // 赤
      if ($prevL || $prevT) $pdf->Line($prevL, $prevT, $curL, $curT);
      if ($p[2] == "x") {
        $pdf->DotBatsu($curL, $curT);
      } else {
        $pdf->DotCircle($curL, $curT);
      }
      $prevL = $curL;
      $prevT = $curT;
    }

    //**********
    // メイン・R
    //**********
    $heightPerGaugeR = $graphHeight / (55 - 5);
    $prevL = 0;
    $prevT = 0;
    foreach($graphR as $idx => $v){
      $p = explode("=", $v);
      $curL = $graphStartLeft+$widthPerMinute*$p[0];
      $curT = (55-$p[1])*$heightPerGaugeR + $graphStartTop;
      $pdf->SetDrawColor(0, 136, 34); // 緑
      if ($prevL || $prevT) $pdf->Line($prevL, $prevT, $curL, $curT);
      $pdf->DotCross($curL, $curT);
      $prevL = $curL;
      $prevT = $curT;
    }

    //**********
    // メイン・L：離床理由（文言のみ）
    //**********
    foreach($graphL as $idx => $v){
      $p = explode("\t", $v);
      $frL = $graphStartLeft+$widthPerMinute*$p[0];
      $pdf->setXY($frL, $graphStartTop+2);
      $pdf->SetFontSize(7);
      $pdf->UniCell(0, 0, $p[2], 0, 0);
    }

    $pdf->SetDrawColor(90,90,90); //
    $pdf->SetTextColor(0); // 黒に戻す

    //==================
    // グラフデータを描いたら、はみ出た左右をクリア。斜め上下にもはみ出るかもしれない。
    //==================
    $pdf->setXY(0, $graphStartTop);
    $pdf->SetFillColor(255);
    $pdf->UniCell($graphStartLeft, $graphHeight, '', 0, 1, 'L', 1); // 左側
    $pdf->setXY($most_right, $graphStartTop);
    $pdf->UniCell(100, $graphHeight, '', 0, 1, 'L', 1); // 右側

    // ページ共通のヘッダー部を出力する
    $curY = $pdf->writeHeader($pt_info);
    $curY += $graphHeight;


    //**********
    // 酸素吸入
    //**********
    $pdf->Plot($most_left, $curY, $hwidth, "酸素吸入");
    $prevToX = "";

    $pdf->SetLineWidth(0.2); // 下の縦線を先に
    for ($i = 0; $i < count($lpos); $i++){
        if ($daily_weekly=="daily") {
            for ($i2 = 0; $i2 <= 3; $i2++) {
                $pdf->SetDrawColor(230,230,230);
                for ($i3 = 1; $i3 <= 5; $i3++) {
                    $pdf->Line($lpos[$i]+($h6*$i2)+($h1*$i3), $curY+0.3, $lpos[$i]+($h6*$i2)+($h1*$i3), $curY+5.2);
                }
                $pdf->SetDrawColor(180,180,180);
                $pdf->Line($lpos[$i]+($h6*$i2), $curY+0.3, $lpos[$i]+($h6*$i2), $curY+5.2);
            }
        } else {
            $pdf->SetDrawColor(230,230,230);
            for ($i2 = 1; $i2 <= 3; $i2++) {
                $pdf->Line($lpos[$i]+($h6*$i2), $curY+0.3, $lpos[$i]+($h6*$i2), $curY+5.2);
            }
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($lpos[$i], $curY, $lpos[$i], $curY+5.2);
    }

    foreach($graphSK as $idx => $d){
        $ary = explode("\t", $d);
        $fromX = $graphStartLeft + $widthPerMinute * $ary[0];
        $toX   = $graphStartLeft + $widthPerMinute * $ary[1];
//        if ($prevToX && $prevToX > $fromX) {
            $curY += 4;
            $pdf->SetLineWidth(0.2); // 下の縦線を先に
            for ($i = 0; $i < count($lpos); $i++){
                if ($daily_weekly=="daily") {
                    for ($i2 = 0; $i2 <= 3; $i2++) {
                        $pdf->SetDrawColor(230,230,230);
                        for ($i3 = 1; $i3 <= 5; $i3++) {
                            $pdf->Line($lpos[$i]+($h6*$i2)+($h1*$i3), $curY, $lpos[$i]+($h6*$i2)+($h1*$i3), $curY+5.2);
                        }
                        $pdf->SetDrawColor(180,180,180);
                        $pdf->Line($lpos[$i]+($h6*$i2), $curY, $lpos[$i]+($h6*$i2), $curY+5.2);
                    }
                } else {
                    $pdf->SetDrawColor(230,230,230);
                    for ($i2 = 1; $i2 <= 3; $i2++) {
                        $pdf->Line($lpos[$i]+($h6*$i2), $curY, $lpos[$i]+($h6*$i2), $curY+5.2);
                    }
                }
                $pdf->SetDrawColor(90,90,90);
                $pdf->Line($lpos[$i], $curY, $lpos[$i], $curY+5.2); // 縦線
            }
//        }

        $sanso_or_kokyuki = $ary[4]; // sanso or kokyuki
        $pdf->SetDrawColor(255, 145, 85); // 呼吸器はみかん色
        if ($sanso_or_kokyuki=="sanso") $pdf->SetDrawColor(128, 100, 162); // 酸素は紫

        $pdf->SetLineWidth(0.3);
        $pdf->Line($fromX+0.15, $curY+3.7, $toX-0.15, $curY+3.7); // 罫線の太さの半分を考慮

        if (!$ary[2]) { // is_fr_overflowでなければマーク
            $pdf->DotSquare1($fromX, $curY+3.7, $sanso_or_kokyuki);
        }
        if (!$ary[3]) { // is_to_overflow でなければマーク
            $pdf->DotSquare1($toX, $curY+3.7, $sanso_or_kokyuki);
        }
        if ($ary[5]!="") { // caption 器具名とリットル
            $pdf->setXY($fromX+0.5, $curY);
            $pdf->SetFontSize(7);
            $pdf->UniCell(100, 4, $ary[5], 0, 0);
        }
        $prevToX = $toX;
    }
    $pdf->SetTextColor(0); // 黒に戻す
    $curY += 5.2;


    //**********
    // 抗生剤
    //**********
    $pdf->SetDrawColor(90,90,90);
    $pdf->SetLineWidth(0.2); // 上縦線
    $pdf->Line($most_left, $curY,  $most_right, $curY);
    $pdf->Plot($most_left, $curY, $hwidth, "抗生剤");

    $pdf->SetLineWidth(0.2); // 下の縦線を先に
    for ($i = 0; $i < count($lpos); $i++){
        if ($daily_weekly=="daily") {
            for ($i2 = 0; $i2 <= 3; $i2++) {
                $pdf->SetDrawColor(230,230,230);
                for ($i3 = 1; $i3 <= 5; $i3++) {
                    $pdf->Line($lpos[$i]+($h6*$i2)+($h1*$i3), $curY+0.3, $lpos[$i]+($h6*$i2)+($h1*$i3), $curY+5.2);
                }
                $pdf->SetDrawColor(180,180,180);
                $pdf->Line($lpos[$i]+($h6*$i2), $curY+0.3, $lpos[$i]+($h6*$i2), $curY+5.2);
            }
        } else {
            $pdf->SetDrawColor(230,230,230);
            for ($i2 = 1; $i2 <= 3; $i2++) {
                $pdf->Line($lpos[$i]+($h6*$i2), $curY+0.3, $lpos[$i]+($h6*$i2), $curY+5.2);
            }
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($lpos[$i], $curY, $lpos[$i], $curY+5.2);
    }

    $prevToX = "";
    foreach($graphK as $idx => $d){
        $ary = explode("\t", $d);
        $fromX = $graphStartLeft + $widthPerMinute * $ary[0];
        $toX   = $graphStartLeft + $widthPerMinute * $ary[1];
        if ($prevToX && $prevToX > $fromX) {
            $curY += 4;
            $pdf->SetLineWidth(0.2); // 下の縦線を先に
            for ($i = 0; $i < count($lpos); $i++){
                if ($daily_weekly=="daily") {
                    for ($i2 = 0; $i2 <= 3; $i2++) {
                        $pdf->SetDrawColor(230,230,230);
                        for ($i3 = 1; $i3 <= 5; $i3++) {
                            $pdf->Line($lpos[$i]+($h6*$i2)+($h1*$i3), $curY, $lpos[$i]+($h6*$i2)+($h1*$i3), $curY+5.2);
                        }
                        $pdf->SetDrawColor(180,180,180);
                        $pdf->Line($lpos[$i]+($h6*$i2), $curY, $lpos[$i]+($h6*$i2), $curY+5.2);
                    }
                } else {
                    $pdf->SetDrawColor(230,230,230);
                    for ($i2 = 1; $i2 <= 3; $i2++) {
                        $pdf->Line($lpos[$i]+($h6*$i2), $curY, $lpos[$i]+($h6*$i2), $curY+5.2);
                    }
                }
                $pdf->SetDrawColor(90,90,90);
                $pdf->Line($lpos[$i], $curY, $lpos[$i], $curY+5.2);
            }
        }
        $pdf->SetDrawColor(155, 187, 89); // 抗生剤は草色
        $pdf->SetLineWidth(0.3);
        $pdf->Line($fromX+0.15, $curY+3.7, $toX-0.15, $curY+3.7);

        if (!$ary[2]) { // is_fr_overflowでなければマーク
            $pdf->DotSquare1($fromX, $curY+3.7, "kouseizai");
        }
        if (!$ary[3]) { // is_to_overflow でなければマーク
            $pdf->DotSquare1($toX, $curY+3.7, "kouseizai");
        }
        if ($ary[4]!="") { // caption 器具名とリットル
            $pdf->setXY($fromX-1.5, $curY);
            $pdf->SetFontSize(7);
            $pdf->UniCell(100, 4, $ary[4], 0, 0);
        }
        $prevToX = $toX;
    }
    $curY += 5.2;
    $pdf->SetTextColor(0); // 黒に戻す



    //==================
    // グラフ内罫線（ヘッダ部のみ）
    //==================
    $pdf->SetLineWidth(0.2);
    $pdf->SetDrawColor(180,180,180);
    for ($i = 1; $i <= 9; $i++){
        $pdf->Line($most_left,  $graphStartTop+($i*6), $graphStartLeft, $graphStartTop+($i*6)); // 横線（灰色）
    }

    $pdf->SetDrawColor(230,230,230);
    for ($i = 0; $i <= 9; $i++){
        $pdf->Line($most_left,  33+($i*6), $graphStartLeft, 33+($i*6)); // 横線（さらに薄い灰色）
    }

    $pdf->SetDrawColor(180,180,180);
    for ($i = 1; $i <= 3; $i++){
        $pdf->Line($most_left+($i*8),  $graphStartTop, $most_left+($i*8), $graphStartTop+$graphHeight); // 縦線（灰色）
    }

    $pdf->SetDrawColor(90,90,90);
    $pdf->Line($most_left+0.2,  $graphStartTop+3, $graphStartLeft-0.2, $graphStartTop+3); // 横線（T/P/R/BP直下の黒線）
    $pdf->Line($most_left+0.2,  $graphStartTop, $graphStartLeft-0.2, $graphStartTop); // 横線（T/P/R/BP直上の黒線）

    $pdf->SetLineWidth(0.4);
    $pdf->Line($most_left+0.2, $graphStartTop+$graphHeight,  $graphStartLeft-0.2, $graphStartTop+$graphHeight); // 横線（グラフ直下の太い箇所）

    $pdf->SetDrawColor(255, 96, 207);
    $pdf->SetLineWidth(0.2);
    $pdf->Line($most_left,  $graphStartTop+(6*6), $graphStartLeft, $graphStartTop+(6*6)); // ピンク線


    //==================
    // グラフヘッダ
    //==================
    $pdf->SetTextColor(0, 0, 255); // 青
    $pdf->SetDrawColor(0, 0, 255); // 青
    $pdf->setXY($most_left, 29.5);
    $pdf->UniCell(8, 4, "T", 0, 0);
    $pdf->Line($most_left+5,  31.3, $most_left+7.5, 31.3);
    $pdf->DotSquare2($most_left+6.25, 31.3);

    $pdf->SetTextColor(255, 0, 0); // 赤
    $pdf->SetDrawColor(255, 0, 0); // 赤
    $pdf->setXY($most_left+8, 29.5);
    $pdf->UniCell(8, 4, "P", 0, 0);
    $pdf->Line($most_left+8+5,  31.3, $most_left+8+7.5, 31.3);
    $pdf->DotCircle($most_left+8+6.25, 31.3);

    $pdf->SetTextColor(0, 136, 34); // 緑
    $pdf->SetDrawColor(0, 136, 34); // 緑
    $pdf->setXY($most_left+16, 29.5);
    $pdf->UniCell(8, 4, "R", 0, 0);
    $pdf->Line($most_left+16+5,  31.3, $most_left+16+7.5, 31.3);
    $pdf->DotCross($most_left+16+6.25, 31.3);

    $pdf->SetTextColor(149, 143, 34); // 濃黄色
    $pdf->SetDrawColor(149, 143, 34); // 濃黄色
    $pdf->setXY($most_left+24, 29.5);
    $pdf->UniCell(8, 4, "BP", 0, 0);

    for ($i = 0; $i < 10; $i++){
        $lblT = "0"; $lblP = "0"; $lblR = "0"; $lblS = "0"; $lblBP = "0";
        if ($i > 0) {
            $lblT = 33 + ($i);
            $lblP = 10 + ($i*20);
            $lblR = 5 + ($i*5);
            $lblS = 30 + ($i*10);
            $lblBP = 40 + ($i*20);
        }
        $grpy = 86.8-($i*6);
        $pdf->SetTextColor(0, 0, 255); // 青
        $pdf->setXY($most_left, $grpy);
        $pdf->UniCell(8, 4, $lblT, 0, 0);
        $pdf->SetTextColor(255, 0, 0); // 赤
        $pdf->setXY($most_left+8, $grpy);
        $pdf->UniCell(8, 4, $lblP, 0, 0);
        $pdf->SetTextColor(0, 136, 34); // 緑
        $pdf->setXY($most_left+16, $grpy);
        $pdf->UniCell(8, 4, $lblR, 0, 0);
        $pdf->SetTextColor(149, 143, 34); // 濃黄色
        $pdf->setXY($most_left+24, $grpy);
        $pdf->UniCell(8, 4, $lblBP, 0, 0);
        $pdf->SetTextColor(0); // 黒に戻す
    }
    $pdf->SetLineWidth(0.2);

    $graphEndTop = $curY;

    //==================
    // 注射処方等指示
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "注射処方等指示"));
        for ($dd = 0; $dd < $day_size; $dd++){
            $chusya_list = $siji_list["chusya"][$WYL[$dd]];
            $field_count = $chusya_list["chusya__field_count"];
            $sectionHeight = 0;
            for ($fnum = 1; $fnum<=$field_count; $fnum++) {
                if ($sectionHeight) {
                    $pdf->SetDrawColor(200,200,200);
                    $pdf->Line($lpos[$dd]+0.1,  $curY + $sectionHeight, $lpos[$dd]+$fwidth-0.1, $curY + $sectionHeight); // 横線
                }
                $prefix = "chusya__".$fnum."__";
                $icons = array();
                if ($chusya_list[$prefix."siji_az"]) $icons[] = get_alias($chusya_list[$prefix."siji_az"]);
                if ($chusya_list[$prefix."is_continue"]) $icons[] = "継";
                if ($chusya_list[$prefix."siji_picture_names"]) $icons[] = "画".count(explode(",",$chusya_list[$prefix."siji_picture_names"]));
                if ($chusya_list[$prefix."is_sumi"]) $icons[] = "済";
                if ($chusya_list[$prefix."is_stop"]) $icons[] = "止";
//                $msg = (count($icons) ? "[".implode("/",$icons)."]" : "") . implode(" ", mb_split("\n", $chusya_list[$prefix."siji_text"]));
                $msg = (count($icons) ? "[".implode("/",$icons)."]" : "") . $chusya_list[$prefix."siji_text"];
                $sectionHeight += $pdf->Plot($lpos[$dd], $curY + $sectionHeight, $fwidth, $msg, 4, 7, "L", 0);
            }
            $lineHeight = max($lineHeight, $sectionHeight);
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }


    //==================
    // 心拍数(HR)
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $pdf->SetDrawColor(200,200,200);
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "心拍数"));
        $exist_dd = array();
        for ($dd = 0; $dd < $day_size; $dd++){
            $sectionHeight = 0;
            if (is_array($care_list["hr"][$WYL[$dd]])) {
                foreach ($care_list["hr"][$WYL[$dd]] as $hm => $item) {
                    if ($sectionHeight) {
                        $pdf->Line($lpos[$dd]+0.1,  $curY + $sectionHeight, $lpos[$dd]+$fwidth-0.1, $curY + $sectionHeight); // 横線
                    }
                    $tmp = $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth/2, $hm);
                    $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/2), $curY+$sectionHeight, $fwidth/2, @$item["hr_kaisu"]));
                    $sectionHeight += $tmp;
                }
            }
            if ($sectionHeight) $exist_dd[] = $dd;
            $lineHeight = max($lineHeight, $sectionHeight);
        }
        foreach ($exist_dd as $dd){
            $pdf->Line($lpos[$dd]+($fwidth/2),  $curY+0.1, $lpos[$dd]+($fwidth/2), $curY + $lineHeight-0.1); // 縦線
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }



    //==================
    // SpO2
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $pdf->SetDrawColor(200,200,200);
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "SpO2"));
        $exist_dd = array();
        for ($dd = 0; $dd < $day_size; $dd++){
            $sectionHeight = 0;
            if (is_array($care_list["spo2"][$WYL[$dd]])) {
                foreach ($care_list["spo2"][$WYL[$dd]] as $hm => $item) {
                    if ($sectionHeight) {
                        $pdf->Line($lpos[$dd]+0.1,  $curY + $sectionHeight, $lpos[$dd]+$fwidth-0.1, $curY + $sectionHeight); // 横線
                    }
                    $tmp = $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth/2, $hm);
                    $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/2), $curY+$sectionHeight, $fwidth/2, @$item["spo2_percent"]));
                    $sectionHeight += $tmp;
                }
            }
            if ($sectionHeight) $exist_dd[] = $dd;
            $lineHeight = max($lineHeight, $sectionHeight);
        }
        foreach ($exist_dd as $dd){
            $pdf->Line($lpos[$dd]+($fwidth/2),  $curY+0.1, $lpos[$dd]+($fwidth/2), $curY + $lineHeight-0.1); // 縦線
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }



    //==================
    // 体重
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $pdf->SetDrawColor(200,200,200);
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "体重"));
        $exist_dd = array();
        for ($dd = 0; $dd < $day_size; $dd++){
            $sectionHeight = 0;
            if (is_array($care_list["taiju"][$WYL[$dd]])) {
                foreach ($care_list["taiju"][$WYL[$dd]] as $hm => $item) {
                    if ($sectionHeight) {
                        $pdf->Line($lpos[$dd]+0.1,  $curY + $sectionHeight, $lpos[$dd]+$fwidth-0.1, $curY + $sectionHeight); // 横線
                    }
                    $tmp = $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth/2, $hm);
                    $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/2), $curY+$sectionHeight, $fwidth/2, @$item["taiju"]));
                    $sectionHeight += $tmp;
                }
            }
            if ($sectionHeight) $exist_dd[] = $dd;
            $lineHeight = max($lineHeight, $sectionHeight);
        }
        foreach ($exist_dd as $dd){
            $pdf->Line($lpos[$dd]+($fwidth/2),  $curY+0.1, $lpos[$dd]+($fwidth/2), $curY + $lineHeight-0.1); // 縦線
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }



    //==================
    // 食事量（経口）
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $pdf->SetDrawColor(200,200,200);
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "食事量（経口）"));
        $exist_dd = array();
        for ($dd = 0; $dd < $day_size; $dd++){
            $sectionHeight = 0;
            $data_exist = 0;

            $asa = $care_list["keikou_asa_fukusyoku"][$WYL[$dd]];
            $hiru = $care_list["keikou_hiru_fukusyoku"][$WYL[$dd]];
            $yu = $care_list["keikou_yu_fukusyoku"][$WYL[$dd]];
            $tmp1 = 4;
            if ($asa.$hiru.$yu!="") {
                $data_exist = 1;
                $tmp1 = max($tmp1, $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth/3, $asa));
                $tmp1 = max($tmp1, $pdf->Plot($lpos[$dd]+($fwidth/3), $curY, $fwidth/3, $hiru));
                $tmp1 = max($tmp1, $pdf->Plot($lpos[$dd]+($fwidth/3*2), $curY, $fwidth/3, $yu));
            }
            $sectionHeight += $tmp1;

            $asa = $care_list["keikou_asa_syusyoku"][$WYL[$dd]];
            $hiru = $care_list["keikou_hiru_syusyoku"][$WYL[$dd]];
            $yu = $care_list["keikou_yu_syusyoku"][$WYL[$dd]];
            if ($asa.$hiru.$yu!="") $data_exist = 1;
            $tmp2 = 4;
            if ($asa.$hiru.$yu!="") {
                $data_exist = 1;
                $tmp2 = max($tmp2, $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth/3, $asa));
                $tmp2 = max($tmp2, $pdf->Plot($lpos[$dd]+($fwidth/3), $curY+$sectionHeight, $fwidth/3, $hiru));
                $tmp2 = max($tmp2, $pdf->Plot($lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight, $fwidth/3, $yu));
            }
            $sectionHeight += $tmp2;
            if ($data_exist) {
                $exist_dd[] = $dd;
                $pdf->Line($lpos[$dd]+0.1,  $curY + $tmp1, $lpos[$dd]+$fwidth-0.1, $curY + $tmp1); // 横線
            }
            $lineHeight = max($lineHeight, $sectionHeight);
        }
        foreach ($exist_dd as $dd){
            $pdf->Line($lpos[$dd]+($fwidth/3),  $curY+0.1, $lpos[$dd]+($fwidth/3), $curY + $lineHeight-0.1); // 縦線
            $pdf->Line($lpos[$dd]+($fwidth/3*2),  $curY+0.1, $lpos[$dd]+($fwidth/3*2), $curY + $lineHeight-0.1); // 縦線
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }


    //==================
    // 食事量（経管）
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $pdf->SetDrawColor(200,200,200);
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "食事量（経管）"));
        $exist_ddsize = array();
        for ($dd = 0; $dd < $day_size; $dd++){
            $keikan_field_count = (int)$care_list["keikan__field_count"][$WYL[$dd]];
            $sectionHeight = 0;
            for ($idx=1; $idx<=$keikan_field_count; $idx++) {
                $eiyouzai = $care_list["keikan__".$idx."__eiyouzai"][$WYL[$dd]];
                $asa = $care_list["keikan__".$idx."__asa_ml"][$WYL[$dd]];
                $hiru = $care_list["keikan__".$idx."__hiru_ml"][$WYL[$dd]];
                $yu = $care_list["keikan__".$idx."__yu_ml"][$WYL[$dd]];
                $ryou4 = $care_list["keikan__".$idx."__ryou4_ml"][$WYL[$dd]];
                $ryou5 = $care_list["keikan__".$idx."__ryou5_ml"][$WYL[$dd]];
                $ryou6 = $care_list["keikan__".$idx."__ryou6_ml"][$WYL[$dd]];

                $sectionHeight += $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth, $eiyouzai);
                $pdf->Line($lpos[$dd]+0.1,  $curY+$sectionHeight, $lpos[$dd]+$fwidth-0.1, $curY+$sectionHeight); // 横線

                $tmp = 4;
                $tmp = max($tmp, $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth/3, $asa));
                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/3), $curY+$sectionHeight, $fwidth/3, $hiru));
                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight, $fwidth/3, $yu));
                $pdf->Line($lpos[$dd]+($fwidth/3),  $curY+$sectionHeight+0.1, $lpos[$dd]+($fwidth/3), $curY+$sectionHeight+$tmp-0.1); // 縦線
                $pdf->Line($lpos[$dd]+($fwidth/3*2),  $curY+$sectionHeight+0.1, $lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight+$tmp-0.1); // 縦線
                $sectionHeight += $tmp;
                $pdf->Line($lpos[$dd]+0.1,  $curY+$sectionHeight, $lpos[$dd]+$fwidth-0.1, $curY+$sectionHeight); // 横線

                if ($ryou4!="" || $ryou5!="" || $ryou6!="") {
                    $tmp = 4;
                    $tmp = max($tmp, $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth/3, $ryou4));
                    $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/3), $curY+$sectionHeight, $fwidth/3, $ryou5));
                    $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight, $fwidth/3, $ryou6));
                    $pdf->Line($lpos[$dd]+($fwidth/3),  $curY+$sectionHeight+0.1, $lpos[$dd]+($fwidth/3), $curY+$sectionHeight+$tmp-0.1); // 縦線
                    $pdf->Line($lpos[$dd]+($fwidth/3*2),  $curY+$sectionHeight+0.1, $lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight+$tmp-0.1); // 縦線
                    $sectionHeight += $tmp;
                    if ($idx<$keikan_field_count) {
                        $pdf->Line($lpos[$dd]+0.1, $curY+$sectionHeight, $lpos[$dd]+$fwidth-0.1,$curY+$sectionHeight);// 横線
                    }
                }
            }
            if ($sectionHeight) $exist_ddsize[$dd] = $sectionHeight;
            $lineHeight = max($lineHeight, $sectionHeight);
        }
        foreach ($exist_ddsize as $dd => $sectionHeight){
            $pdf->Line($lpos[$dd]+($fwidth/3),  $curY+$sectionHeight+0.1, $lpos[$dd]+($fwidth/3), $curY+$lineHeight-0.1); // 縦線
            $pdf->Line($lpos[$dd]+($fwidth/3*2),  $curY+$sectionHeight+0.1, $lpos[$dd]+($fwidth/3*2), $curY+$lineHeight-0.1); // 縦線
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }



    //==================
    // 補食
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $pdf->SetDrawColor(200,200,200);
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "補食"));
        //$height1 = 0;
        //$height2 = 0;
        $exist_dd = array();
        for ($dd = 0; $dd < $day_size; $dd++){
            $field_count = $care_list["hosyoku__field_count"][$WYL[$dd]];
            $blockHeight = 0;
            for ($idx=1; $idx<=$field_count; $idx++) {
                $timing = $care_list["hosyoku__".$idx."__hosyoku_timing"][$WYL[$dd]];
                $hosyoku = get_alias($care_list["hosyoku__".$idx."__hosyoku"][$WYL[$dd]]);
                $ryou = get_alias($care_list["hosyoku__".$idx."__hosyoku_ryou"][$WYL[$dd]]);
                $height2 = 0;
                if ($timing.$hosyoku.$ryou!="") {
                    if ($idx>1) $pdf->Line($lpos[$dd]+0.1,  $curY+$blockHeight, $lpos[$dd]+$fwidth-0.1, $curY+$blockHeight); // 横線
                    $height1 = $pdf->Plot($lpos[$dd], $curY+$blockHeight, $fwidth, $timing);
                    $pdf->Line($lpos[$dd]+0.1,  $curY+$height1+$blockHeight, $lpos[$dd]+$fwidth-0.1, $curY+$height1+$blockHeight); // 横線
                    $tmp = $pdf->Plot($lpos[$dd], $curY+$height1+$blockHeight, $fwidth/10*6, $hosyoku);
                    $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/10*6), $curY+$height1+$blockHeight, $fwidth/10*4, $ryou));
                    $height2 = $height1 + $tmp;
                    $bottomPos = $curY+$height1+$tmp+$blockHeight;
                    if ($idx==$field_count) $bottomPos = 0;
                    $exist_dd[] = array("dd"=>$dd, "top"=>$curY+$height1+$blockHeight, "bottom"=>$bottomPos);
                }
                $blockHeight += $height2;
            }
            $lineHeight = max($lineHeight, $blockHeight);
        }
        foreach ($exist_dd as $heightInfo){
            $dd = $heightInfo["dd"];
            $lineTop = $heightInfo["top"];
            $lineBottom = $heightInfo["bottom"];
            if (!$lineBottom) $lineBottom = $curY + $lineHeight;
            $pdf->Line($lpos[$dd]+($fwidth/10*6),  $lineTop+0.1, $lpos[$dd]+($fwidth/10*6), $lineBottom-0.1); // 縦線
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }



    //==================
    // 水分摂取量
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $pdf->SetDrawColor(200,200,200);
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "水分摂取量"));
        $exist_dd = array();
        for ($dd = 0; $dd < $day_size; $dd++){
            $sectionHeight = $pdf->Plot($lpos[$dd], $curY, $fwidth/5, $care_list["suibun_ryou_1"][$WYL[$dd]]);
            $sectionHeight = max($sectionHeight, $pdf->Plot($lpos[$dd]+($fwidth/5), $curY, $fwidth/5, $care_list["suibun_ryou_2"][$WYL[$dd]]));
            $sectionHeight = max($sectionHeight, $pdf->Plot($lpos[$dd]+($fwidth/5*2), $curY, $fwidth/5, $care_list["suibun_ryou_3"][$WYL[$dd]]));
            $sectionHeight = max($sectionHeight, $pdf->Plot($lpos[$dd]+($fwidth/5*3), $curY, $fwidth/5, $care_list["suibun_ryou_4"][$WYL[$dd]]));
            $sectionHeight = max($sectionHeight, $pdf->Plot($lpos[$dd]+($fwidth/5*4), $curY, $fwidth/5, $care_list["suibun_ryou_5"][$WYL[$dd]]));
            if ($sectionHeight) $exist_dd[] = $dd;
            $lineHeight = max($lineHeight, $sectionHeight);
        }
        foreach ($exist_dd as $dd){
            $pdf->Line($lpos[$dd]+($fwidth/5),  $curY+0.1, $lpos[$dd]+($fwidth/5), $curY + $lineHeight-0.1); // 縦線
            $pdf->Line($lpos[$dd]+($fwidth/5*2),  $curY+0.1, $lpos[$dd]+($fwidth/5*2), $curY + $lineHeight-0.1); // 縦線
            $pdf->Line($lpos[$dd]+($fwidth/5*3),  $curY+0.1, $lpos[$dd]+($fwidth/5*3), $curY + $lineHeight-0.1); // 縦線
            $pdf->Line($lpos[$dd]+($fwidth/5*4),  $curY+0.1, $lpos[$dd]+($fwidth/5*4), $curY + $lineHeight-0.1); // 縦線
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }


    //==================
    // 補液
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $pdf->SetDrawColor(200,200,200);
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "補液"));
        $exist_dd = array();

        for ($dd = 0; $dd < $day_size; $dd++){
            $sectionHeight = 0;
            $field_count = count($care_list["hoeki__data"][$WYL[$dd]]);
            for ($fnum = 0; $fnum<$field_count; $fnum++) {
                if ($fnum>0) {
                    $pdf->Line($lpos[$dd]+0.1,  $curY+$sectionHeight, $lpos[$dd]+$fwidth-0.1, $curY+$sectionHeight); // 横線
                }
                $tmp = 0;
                $tmp = max($tmp, $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth/100*15, $care_list["hoeki__data"][$WYL[$dd]][$fnum]["az"]));
                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/100*15), $curY+$sectionHeight, $fwidth/100*35, $care_list["hoeki__data"][$WYL[$dd]][$fnum]["hm"]));
                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/2), $curY+$sectionHeight, $fwidth/2, $care_list["hoeki__data"][$WYL[$dd]][$fnum]["data"]));
                $sectionHeight += $tmp;
            }
            $lineHeight = max($lineHeight, $sectionHeight);
            if ($sectionHeight) $exist_dd[] = $dd;
        }
        foreach ($exist_dd as $dd){
            $pdf->Line($lpos[$dd]+($fwidth/100*15),  $curY+0.1, $lpos[$dd]+($fwidth/100*15), $curY + $lineHeight-0.1); // 縦線
            $pdf->Line($lpos[$dd]+($fwidth/2),  $curY+0.1, $lpos[$dd]+($fwidth/2), $curY + $lineHeight-0.1); // 縦線
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }



    //==================
    // インスリン
    //==================
    for ($i0=0; $i0<count($weekly_insulin_header); $i0++) {
        $kaipage_counter = 0;
        for (;;) {
            $pdf->SetDrawColor(200,200,200);
            $msg = "ｲﾝｽﾘﾝ" . ($weekly_insulin_header[$i0] ? "：".$weekly_insulin_header[$i0] : "");
            $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, $msg, 4, "", "C"));
            $sectionHeight = 0;
            $exist_dd = array();
            for ($dd = 0; $dd < $day_size; $dd++){
                $tmp = 0;
                if ($weekly_insulin_header[$i0]=="定期") {
                    $tmp = $pdf->Plot($lpos[$dd], $curY, $fwidth, $care_list["insulin_teiki"][$WYL[$dd]]);
                }
                if ($weekly_insulin_header[$i0]=="BS測定値") {
                    for ($iii=0; $iii<count($care_list["insulin_bs"][$WYL[$dd]]); $iii++) {
                        $bse = $care_list["insulin_bs"][$WYL[$dd]][$iii];
                        $tmp1 = max(4,     $pdf->Plot($lpos[$dd], $curY+$tmp, $fwidth/2, $bse["bs_hm"]));
                        $tmp1 = max($tmp1, $pdf->Plot($lpos[$dd]+($fwidth/2), $curY+$tmp, $fwidth/2, $bse["bs_upper"]));
                        $pdf->Line($lpos[$dd]+($fwidth/2),  $curY+$tmp, $lpos[$dd]+($fwidth/2), $curY+$tmp+$tmp1); // 縦線
                        $tmp += $tmp1;
                        $pdf->Line($lpos[$dd]+0.1,  $curY+$tmp, $lpos[$dd]+$fwidth-0.1, $curY+$tmp); // 横線

                        if ($bse["bs_name1"] || $bse["bs_ryou1"]) {
                            $tmp1 = $pdf->Plot($lpos[$dd], $curY+$tmp, $fwidth/10*8, $bse["bs_name1"]);
                            $tmp1 = max($tmp1, $pdf->Plot($lpos[$dd]+($fwidth/10*8), $curY+$tmp, $fwidth/10*2, $bse["bs_ryou1"]));
                            $pdf->Line($lpos[$dd]+($fwidth/10*8),  $curY+$tmp, $lpos[$dd]+($fwidth/10*8), $curY+$tmp+$tmp1); // 縦線
                            $tmp += $tmp1;
                            $pdf->Line($lpos[$dd]+0.1,  $curY+$tmp, $lpos[$dd]+$fwidth-0.1, $curY+$tmp); // 横線
                        }
                        if ($bse["bs_name2"] || $bse["bs_ryou2"]) {
                            $tmp1 = $pdf->Plot($lpos[$dd], $curY+$tmp, $fwidth/10*8, $bse["bs_name2"]);
                            $tmp1 = max($tmp1, $pdf->Plot($lpos[$dd]+($fwidth/10*8), $curY+$tmp, $fwidth/10*2, $bse["bs_ryou2"]));
                            $pdf->Line($lpos[$dd]+($fwidth/10*8),  $curY+$tmp, $lpos[$dd]+($fwidth/10*8), $curY+$tmp+$tmp1); // 縦線
                            $tmp += $tmp1;
                            $pdf->Line($lpos[$dd]+0.1,  $curY+$tmp, $lpos[$dd]+$fwidth-0.1, $curY+$tmp); // 横線
                        }
                    }
                }
                if ($weekly_insulin_header[$i0]=="食事量") {
                    $tmp = $pdf->Plot($lpos[$dd], $curY, $fwidth/3, $care_list["insulin_sr_asa"][$WYL[$dd]]);
                    $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/3), $curY, $fwidth/3, $care_list["insulin_sr_hiru"][$WYL[$dd]]));
                    $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/3*2), $curY, $fwidth/3, $care_list["insulin_sr_yu"][$WYL[$dd]]));
                    if ($tmp) $exist_dd[] = $dd;
                }
                $sectionHeight = max($sectionHeight, $tmp);
            }
            $lineHeight = max($lineHeight, $sectionHeight);
            foreach ($exist_dd as $dd){
                if ($weekly_insulin_header[$i0]=="食事量") {
                    $pdf->Line($lpos[$dd]+($fwidth/3),  $curY+0.1, $lpos[$dd]+($fwidth/3), $curY + $lineHeight-0.1); // 縦線
                    $pdf->Line($lpos[$dd]+($fwidth/3*2),  $curY+0.1, $lpos[$dd]+($fwidth/3*2), $curY + $lineHeight-0.1); // 縦線
                }
            }
            $pdf->SetDrawColor(90,90,90);
            $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
            if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
        }
    }

    //==================
    // 便
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $lineHeight = 4;
        $pdf->SetDrawColor(200,200,200);
        $sectionHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "便", 4, "", "L"));
        $sectionHeight = max($sectionHeight, $pdf->Plot($most_left, $curY, $hwidth, "合計", 4, "", "R"));
        $exist_dd = array();
        for ($dd = 0; $dd < $day_size; $dd++){
            $tmp = 0;
            if ($care_list["ben__ttl_ryou"][$WYL[$dd]]=="無し"){
                $tmp = $pdf->Plot($lpos[$dd], $curY, $fwidth, "無し");
            } else {
                $tmp = $pdf->Plot($lpos[$dd], $curY, $fwidth/2, $care_list["ben__field_count"][$WYL[$dd]]);
                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/2), $curY, $fwidth/2, $care_list["ben__ttl_ryou"][$WYL[$dd]]));
                if ($tmp) $exist_dd[] = $dd;
            }
            $sectionHeight = max($sectionHeight, $tmp);
        }
        $lineHeight = max($lineHeight, $sectionHeight);
        foreach ($exist_dd as $dd){
            $pdf->Line($lpos[$dd]+($fwidth/2),  $curY+0.1, $lpos[$dd]+($fwidth/2), $curY + $lineHeight-0.1); // 縦線
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }


    //==================
    // 便
    //==================
    if ($weekly_ben_ari) {
        $kaipage_counter = 0;
        for (;;) {
            $pdf->SetDrawColor(200,200,200);
            $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "性状・量（g）", 4, "", "R"));
            for ($dd = 0; $dd < $day_size; $dd++){
                $sectionHeight = 0;
                $cnt = count((array)@$care_list["ben"][$WYL[$dd]]);
                for ($iii=0; $iii<$cnt; $iii++) {
                    $tmp = 0;
                    $item = $care_list["ben"][$WYL[$dd]][$iii];
                    if ($item["ben_nasi"]){
                        $tmp = max($tmp, $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth, "無し"));
                    } else {
                        $tmp = max($tmp, $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth/3, get_alias($item["ben_benryou"])));
                        $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/3), $curY+$sectionHeight, $fwidth/3, get_alias($item["ben_type"])));
                        if ($item["ryou"]!="") {
	                        $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight, $fwidth/3, $item["ryou"]));
	                    }
                        $pdf->Line($lpos[$dd]+($fwidth/3), $curY+$sectionHeight+0.1, $lpos[$dd]+($fwidth/3), $curY+$sectionHeight+$tmp-0.1); // 縦線
                        $pdf->Line($lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight+0.1, $lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight+$tmp-0.1); // 縦線
                    }
                    if ($iii>0) $pdf->Line($lpos[$dd]+0.1,  $curY+$sectionHeight, $lpos[$dd]+$fwidth-0.1, $curY+$sectionHeight); // 横線
                    $sectionHeight += $tmp;
                }
                $lineHeight = max($lineHeight, $sectionHeight);
            }
            $pdf->SetDrawColor(90,90,90);
            $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
            if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
        }
    }


    //==================
    // 尿：時間
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $pdf->SetDrawColor(200,200,200);
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "尿", 4, "", "L"));
        $lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "時間（mL）", 4, "", "R"));
        for ($dd = 0; $dd < $day_size; $dd++){
            $sectionHeight = 0;
            $cnt = count((array)@$care_list["nyou_list"][$WYL[$dd]]);
            for ($iii=0; $iii<$cnt; $iii++) {
                $tmp = 0;
                $item = $care_list["nyou_list"][$WYL[$dd]][$iii];
                $tmp = max($tmp, $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth/3, $item["nyou_hm"]));
                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/3), $curY+$sectionHeight, $fwidth/3, $item["nyou_ryou"]));
                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight, $fwidth/3, $item["nyou_more"]));
                $pdf->Line($lpos[$dd]+($fwidth/3), $curY+$sectionHeight+0.1, $lpos[$dd]+($fwidth/3), $curY+$sectionHeight+$tmp-0.1); // 縦線
                $pdf->Line($lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight+0.1, $lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight+$tmp-0.1); // 縦線
                if ($iii>0) $pdf->Line($lpos[$dd]+0.1,  $curY+$sectionHeight, $lpos[$dd]+$fwidth-0.1, $curY+$sectionHeight); // 横線
                $sectionHeight += $tmp;
            }
            $lineHeight = max($lineHeight, $sectionHeight);
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }


    //==================
    // 尿 一日
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "一日（mL）", 4, "", "R"));
        for ($dd = 0; $dd < $day_size; $dd++){
            $lineHeight = max($lineHeight, $pdf->Plot($lpos[$dd], $curY, $fwidth, $care_list["day_nyou"][$WYL[$dd]]));
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }


    //==================
    // 尿 コメント
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "コメント", 4, "", "R"));
        for ($dd = 0; $dd < $day_size; $dd++){
            $lineHeight = max($lineHeight, $pdf->Plot($lpos[$dd], $curY, $fwidth, $care_list["day_nyou_comment"][$WYL[$dd]]));
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }


    //==================
    // 排液：種類
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $pdf->SetDrawColor(200,200,200);
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "排液", 4, "", "L"));
        $lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "種類", 4, "", "R"));
        $exist_dd = array();
        for ($dd = 0; $dd < $day_size; $dd++){
                $tmp = $pdf->Plot($lpos[$dd], $curY, $fwidth/10*2, $care_list["haieki_syurui"][$WYL[$dd]]);
                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/10*2), $curY, $fwidth/10*8, $care_list["haieki_syurui_comment"][$WYL[$dd]]));
                $lineHeight = max($lineHeight, $tmp);
                if ($tmp) $exist_dd[] = $dd;
        }
        foreach ($exist_dd as $dd){
            $pdf->Line($lpos[$dd]+($fwidth/10*2), $curY+0.1, $lpos[$dd]+($fwidth/10*2), $curY + $lineHeight-0.1); // 縦線
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }


    //==================
    // 排液：量
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $pdf->SetDrawColor(200,200,200);
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "量（mL）", 4, "", "R"));
        for ($dd = 0; $dd < $day_size; $dd++){
            $sectionHeight = 0;
            $cnt = count((array)@$care_list["haieki_list"][$WYL[$dd]]);
            for ($iii=0; $iii<$cnt; $iii++) {
                $tmp = 0;
                $item = $care_list["haieki_list"][$WYL[$dd]][$iii];
                $tmp = max($tmp, $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth/3, $item["haieki_hm"]));
                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/3), $curY+$sectionHeight, $fwidth/3, $item["haieki_ryou"]));
                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight, $fwidth/3, $item["haieki_more"]));
                $pdf->Line($lpos[$dd]+($fwidth/3), $curY+$sectionHeight+0.1, $lpos[$dd]+($fwidth/3), $curY+$sectionHeight+$tmp-0.1); // 縦線
                $pdf->Line($lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight+0.1, $lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight+$tmp-0.1); // 縦線
                if ($iii>0) $pdf->Line($lpos[$dd]+0.1,  $curY+$sectionHeight, $lpos[$dd]+$fwidth-0.1, $curY+$sectionHeight); // 横線
                $sectionHeight += $tmp;
            }
            $lineHeight = max($lineHeight, $sectionHeight);
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }


    //==================
    // 排液：一日
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "一日（mL）", 4, "", "R"));
        for ($dd = 0; $dd < $day_size; $dd++){
            $lineHeight = max($lineHeight, $pdf->Plot($lpos[$dd], $curY, $fwidth, $care_list["haieki_day_ryou"][$WYL[$dd]]));
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }


    //==================
    // 排液：コメント
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "コメント", 4, "", "R"));
        for ($dd = 0; $dd < $day_size; $dd++){
            $lineHeight = max($lineHeight, $pdf->Plot($lpos[$dd], $curY, $fwidth, $care_list["haieki_day_comment"][$WYL[$dd]]));
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }


    //==================
    // 排液２：種類
    //==================
    if ($haieki2_exist) {
        $kaipage_counter = 0;
        for (;;) {
            $pdf->SetDrawColor(200,200,200);
            $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "排液２", 4, "", "L"));
            $lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "種類", 4, "", "R"));
            $exist_dd = array();
            for ($dd = 0; $dd < $day_size; $dd++){
                $tmp = $pdf->Plot($lpos[$dd], $curY, $fwidth/10*2, $care_list["haieki2_syurui"][$WYL[$dd]]);
                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/10*2), $curY, $fwidth/10*8, $care_list["haieki2_syurui_comment"][$WYL[$dd]]));
                $lineHeight = max($lineHeight, $tmp);
                if ($tmp) $exist_dd[] = $dd;
            }
            $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
            $pdf->SetDrawColor(90,90,90);
            foreach ($exist_dd as $dd){
                $pdf->Line($lpos[$dd]+($fwidth/10*2), $curY+0.1, $lpos[$dd]+($fwidth/10*2), $curY + $lineHeight-0.1); // 縦線
            }
            if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
        }
    }



    //==================
    // 排液２：量
    //==================
    if ($haieki2_exist) {
        $kaipage_counter = 0;
        for (;;) {
            $pdf->SetDrawColor(200,200,200);
            $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "量（mL）", 4, "", "R"));
	        for ($dd = 0; $dd < $day_size; $dd++){
	            $sectionHeight = 0;
	            $cnt = count((array)@$care_list["haieki2_list"][$WYL[$dd]]);
	            for ($iii=0; $iii<$cnt; $iii++) {
	                $tmp = 0;
	                $item = $care_list["haieki2_list"][$WYL[$dd]][$iii];
	                $tmp = max($tmp, $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth/3, $item["haieki_hm"]));
	                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/3), $curY+$sectionHeight, $fwidth/3, $item["haieki_ryou"]));
	                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight, $fwidth/3, $item["haieki_more"]));
	                $pdf->Line($lpos[$dd]+($fwidth/3), $curY+$sectionHeight+0.1, $lpos[$dd]+($fwidth/3), $curY+$sectionHeight+$tmp-0.1); // 縦線
	                $pdf->Line($lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight+0.1, $lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight+$tmp-0.1); // 縦線
	                if ($iii>0) $pdf->Line($lpos[$dd]+0.1,  $curY+$sectionHeight, $lpos[$dd]+$fwidth-0.1, $curY+$sectionHeight); // 横線
	                $sectionHeight += $tmp;
	            }
	            $lineHeight = max($lineHeight, $sectionHeight);
	        }
            $pdf->SetDrawColor(90,90,90);
            $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
            if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
        }
    }


    //==================
    // 排液２：一日
    //==================
    if ($haieki2_exist) {
        $kaipage_counter = 0;
        for (;;) {
            $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "一日（mL）", 4, "", "R"));
            for ($dd = 0; $dd < $day_size; $dd++){
                $lineHeight = max($lineHeight, $pdf->Plot($lpos[$dd], $curY, $fwidth, $care_list["haieki2_day_ryou"][$WYL[$dd]]));
            }
            $pdf->SetDrawColor(90,90,90);
            $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
            if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
        }
    }


    //==================
    // 排液２：コメント
    //==================
    if ($haieki2_exist) {
        $kaipage_counter = 0;
        for (;;) {
            $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "コメント", 4, "", "R"));
            for ($dd = 0; $dd < $day_size; $dd++){
                $lineHeight = max($lineHeight, $pdf->Plot($lpos[$dd], $curY, $fwidth, $care_list["haieki2_day_comment"][$WYL[$dd]]));
            }
            $pdf->SetDrawColor(90,90,90);
            $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
            if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
        }
    }




    //==================
    // 排液３：種類
    //==================
    if ($haieki3_exist) {
        $kaipage_counter = 0;
        for (;;) {
            $pdf->SetDrawColor(200,200,200);
            $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "排液３", 4, "", "L"));
            $lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "種類", 4, "", "R"));
            $exist_dd = array();
            for ($dd = 0; $dd < $day_size; $dd++){
                $tmp = $pdf->Plot($lpos[$dd], $curY, $fwidth/10*2, $care_list["haieki3_syurui"][$WYL[$dd]]);
                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/10*2), $curY, $fwidth/10*8, $care_list["haieki3_syurui_comment"][$WYL[$dd]]));
                $lineHeight = max($lineHeight, $tmp);
                if ($tmp) $exist_dd[] = $dd;
            }
            foreach ($exist_dd as $dd){
                $pdf->Line($lpos[$dd]+($fwidth/10*2), $curY+0.1, $lpos[$dd]+($fwidth/10*2), $curY + $lineHeight-0.1); // 縦線
            }
            $pdf->SetDrawColor(90,90,90);
            $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
            if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
        }
    }



    //==================
    // 排液３：量
    //==================
    if ($haieki3_exist) {
        $kaipage_counter = 0;
        for (;;) {
            $pdf->SetDrawColor(200,200,200);
            $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "量（mL）", 4, "", "R"));
	        for ($dd = 0; $dd < $day_size; $dd++){
	            $sectionHeight = 0;
	            $cnt = count((array)@$care_list["haieki3_list"][$WYL[$dd]]);
	            for ($iii=0; $iii<$cnt; $iii++) {
	                $tmp = 0;
	                $item = $care_list["haieki3_list"][$WYL[$dd]][$iii];
	                $tmp = max($tmp, $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth/3, $item["haieki_hm"]));
	                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/3), $curY+$sectionHeight, $fwidth/3, $item["haieki_ryou"]));
	                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight, $fwidth/3, $item["haieki_more"]));
	                $pdf->Line($lpos[$dd]+($fwidth/3), $curY+$sectionHeight+0.1, $lpos[$dd]+($fwidth/3), $curY+$sectionHeight+$tmp-0.1); // 縦線
	                $pdf->Line($lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight+0.1, $lpos[$dd]+($fwidth/3*2), $curY+$sectionHeight+$tmp-0.1); // 縦線
	                if ($iii>0) $pdf->Line($lpos[$dd]+0.1,  $curY+$sectionHeight, $lpos[$dd]+$fwidth-0.1, $curY+$sectionHeight); // 横線
	                $sectionHeight += $tmp;
	            }
	            $lineHeight = max($lineHeight, $sectionHeight);
	        }
            $pdf->SetDrawColor(90,90,90);
            $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
            if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
        }
    }


    //==================
    // 排液３：一日
    //==================
    if ($haieki3_exist) {
        $kaipage_counter = 0;
        for (;;) {
            $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "一日（mL）", 4, "", "R"));
            for ($dd = 0; $dd < $day_size; $dd++){
                $lineHeight = max($lineHeight, $pdf->Plot($lpos[$dd], $curY, $fwidth, $care_list["haieki3_day_ryou"][$WYL[$dd]]));
            }
            $pdf->SetDrawColor(90,90,90);
            $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
            if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
        }
    }


    //==================
    // 排液３：コメント
    //==================
    if ($haieki3_exist) {
        $kaipage_counter = 0;
        for (;;) {
            $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "コメント", 4, "", "R"));
            for ($dd = 0; $dd < $day_size; $dd++){
                $lineHeight = max($lineHeight, $pdf->Plot($lpos[$dd], $curY, $fwidth, $care_list["haieki3_day_comment"][$WYL[$dd]]));
            }
            $pdf->SetDrawColor(90,90,90);
            $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
            if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
        }
    }



    //==================
    // 下剤
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $pdf->SetDrawColor(200,200,200);
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "下剤"));
        $exist_dd = array();
        for ($dd = 0; $dd < $day_size; $dd++){
            $sectionHeight = 0;
            $cnt = count((array)@$care_list["gezai_list"][$WYL[$dd]]);
            for ($iii=0; $iii<$cnt; $iii++) {
                $item = $care_list["gezai_list"][$WYL[$dd]][$iii];
                $tmp = 0;
                $tmp = max($tmp, $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth/10*4, get_alias($item["gezai_hm"])));
                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/10*4), $curY+$sectionHeight, $fwidth/10*6, get_alias($item["gezai_med_name"])));
                if ($tmp && $iii>0 && $sectionHeight) {
                    $exist_dd[] = $dd;
                    $pdf->Line($lpos[$dd]+0.1,  $curY+$sectionHeight, $lpos[$dd]+$fwidth-0.1, $curY+$sectionHeight); // 横線
                }
                $sectionHeight += $tmp;
            }
            $lineHeight = max($lineHeight, $sectionHeight);
        }
        foreach ($exist_dd as $dd){
            $pdf->Line($lpos[$dd]+($fwidth/10*4),  $curY, $lpos[$dd]+($fwidth/10*4), $curY + $lineHeight-0.2); // 縦線
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }


    //==================
    // 座薬
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $pdf->SetDrawColor(200,200,200);
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "座薬"));
        $exist_dd = array();
        for ($dd = 0; $dd < $day_size; $dd++){
            $sectionHeight = 0;
            $cnt = count((array)@$care_list["zayaku_list"][$WYL[$dd]]);
            for ($iii=0; $iii<$cnt; $iii++) {
                $item = $care_list["zayaku_list"][$WYL[$dd]][$iii];
                $tmp = 0;
                $tmp = max($tmp, $pdf->Plot($lpos[$dd], $curY+$sectionHeight, $fwidth/10*4, get_alias($item["zayaku_hm"])));
                $tmp = max($tmp, $pdf->Plot($lpos[$dd]+($fwidth/10*4), $curY+$sectionHeight, $fwidth/10*6, get_alias($item["zayaku_med_name"])));
                if ($tmp && $iii>0 && $sectionHeight) {
                    $exist_dd[] = $dd;
                    $pdf->Line($lpos[$dd]+0.1,  $curY+$sectionHeight, $lpos[$dd]+$fwidth-0.1, $curY+$sectionHeight); // 横線
                }
                $sectionHeight += $tmp;
            }
            $lineHeight = max($lineHeight, $sectionHeight);
        }
        foreach ($exist_dd as $dd){
            $pdf->Line($lpos[$dd]+($fwidth/10*4),  $curY, $lpos[$dd]+($fwidth/10*4), $curY + $lineHeight-0.2); // 縦線
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }



    //==================
    // 発赤
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "発赤"));
        for ($dd = 0; $dd < $day_size; $dd++){
            $lineHeight = max($lineHeight, $pdf->Plot($lpos[$dd], $curY, $fwidth, $care_list["hosseki"][$WYL[$dd]]));
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }

    //==================
    // 検査
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $pdf->SetDrawColor(200,200,200);
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "検査"));
        for ($dd = 0; $dd < $day_size; $dd++){
            $names = $care_list["kensa_names_jp"][$WYL[$dd]];
            $comment = $care_list["kensa_other_comment"][$WYL[$dd]];
            $tmp = $pdf->Plot($lpos[$dd], $curY, $fwidth, $names);
            if ($names && $comment) {
                $pdf->Line($lpos[$dd]+0.1,  $curY+$tmp, $lpos[$dd]+$fwidth-0.1, $curY+$tmp); // 横線
            }
            $tmp += $pdf->Plot($lpos[$dd], $curY+$tmp, $fwidth, $comment);
            $lineHeight = max($lineHeight, $tmp);
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }


    //==================
    // 観察項目 日勤準夜深夜タイトル
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "観察項目", 4, "", "L"));
        for ($dd = 0; $dd < $day_size; $dd++){
          $lineHeight = max($lineHeight, $pdf->Plot($lpos[$dd],      $curY, $fwidth/3, "深夜"));
          $lineHeight = max($lineHeight, $pdf->Plot($lpos[$dd]+($fwidth/3),  $curY, $fwidth/3, "日勤"));
          $lineHeight = max($lineHeight, $pdf->Plot($lpos[$dd]+($fwidth/3*2),  $curY, $fwidth/3, "準夜"));
        }
        $pdf->SetDrawColor(200,200,200);
        for ($dd = 0; $dd < $day_size; $dd++){
          $pdf->Line($lpos[$dd]+($fwidth/3), $curY,  $lpos[$dd]+($fwidth/3), $curY + $lineHeight); // 縦線
          $pdf->Line($lpos[$dd]+($fwidth/3*2), $curY,  $lpos[$dd]+($fwidth/3*2), $curY + $lineHeight); // 縦線
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }



    //==================
    // 観察項目
    //==================
    for ($i0=0; $i0<count($weekly_kansatu_header); $i0++) {
        $kaipage_counter = 0;
        for (;;) {
            $pdf->SetDrawColor(200,200,200);
            $pdf->Line($most_left+0.1, $curY, $most_right-0.1, $curY); // 横線
            $lineHeight = 4;
            $lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, $weekly_kansatu_header[$i0], 4, "", "R"));
            $sectionHeight = 0;
            for ($dd = 0; $dd < $day_size; $dd++){
                $item = @$care_list["kansatu"][$WYL[$dd]][$weekly_kansatu_header[$i0]];
                $sectionHeight = $pdf->Plot($lpos[$dd], $curY, $fwidth/3, @$item["sinya"]);
                $sectionHeight = max($sectionHeight, $pdf->Plot($lpos[$dd]+($fwidth/3), $curY, $fwidth/3, @$item["nikkin"]));
                $sectionHeight = max($sectionHeight, $pdf->Plot($lpos[$dd]+($fwidth/3*2), $curY, $fwidth/3, $item["junya"]));
            }
            $lineHeight = max($lineHeight, $sectionHeight);
            $pdf->SetDrawColor(200,200,200);
            $pdf->Line($most_left+0.1, $curY+$lineHeight, $most_right-0.1, $curY+$lineHeight); // 横線
            for ($dd = 0; $dd < $day_size; $dd++){
                $pdf->Line($lpos[$dd]+($fwidth/3),  $curY, $lpos[$dd]+($fwidth/3), $curY + $lineHeight-0.2); // 縦線
                $pdf->Line($lpos[$dd]+($fwidth/3*2),  $curY, $lpos[$dd]+($fwidth/3*2), $curY + $lineHeight-0.2); // 縦線
            }
            if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
        }
    }


    //==================
    // SOAP
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "SOAP"));
        for ($dd = 0; $dd < $day_size; $dd++){
            $soap_list = $siji_list["soap"][$WYL[$dd]];
            if (!is_array($soap_list)) $soap_list = array();
            //ksort($soap_list);
            $sectionHeight = 0;
            foreach ($soap_list as $soap_row) {
                if ($sectionHeight) {
                    $pdf->SetDrawColor(200,200,200);
                    $pdf->Line($lpos[$dd]+0.1,  $curY + $sectionHeight, $lpos[$dd]+$fwidth-0.1, $curY + $sectionHeight); // 横線
                }

                $hm = trim(@$soap_row["action_hm"]);
                $soap_jikantai = trim(@$soap_row["soap_jikantai"]);
                $jikantai = "";
                if ($soap_jikantai=="nikkin") $jikantai = '[日]';
                if ($soap_jikantai=="junya") $jikantai = '[準]';
                if ($soap_jikantai=="sinya") $jikantai = '[深]';
                if (strlen($hm)==4) $hm = substr($hm,0,2).":".substr($hm,2);
                if ($hm!="") $hm = '['.$hm.']';

                $msg = $hm.$jikantai.$soap_row["siji_text"];
                if ($soap_row["siji_ptext"]!="") {
					$msg .= "\n".'[P]'.$soap_row["siji_ptext"];
                }

                $sectionHeight += $pdf->Plot($lpos[$dd], $curY + $sectionHeight, $fwidth, $msg, 4, 7, "L", false, 0);
            }
            $lineHeight = max($lineHeight, $sectionHeight);
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }


    //==================
    // 最終確認者
    //==================
    /*
    $lineHeight = 4;
    $pdf->SetDrawColor(90,90,90);
    $pdf->Line($most_left, $curY,  $most_right, $curY); // 横線
    $lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "最終確認者"));
    for ($dd = 0; $dd < $day_size; $dd++){
        $lineHeight = max($lineHeight, $pdf->Plot($lpos[$dd], $curY, $fwidth, @$update_history[$WYL[$dd]]));
    }
    $curY += $lineHeight;
    */



    //==================
    // サイン
    //==================
    $kaipage_counter = 0;
    for (;;) {
        $lineHeight = max(4, $pdf->Plot($most_left, $curY, $hwidth, "サイン", 4, ""));
        for ($dd = 0; $dd < $day_size; $dd++){
          $lineHeight = max($lineHeight, $pdf->Plot($lpos[$dd],      $curY, $fwidth/3, $care_list["sign"][$WYL[$dd]]["sinya"]));
          $lineHeight = max($lineHeight, $pdf->Plot($lpos[$dd]+($fwidth/3),  $curY, $fwidth/3, $care_list["sign"][$WYL[$dd]]["nikkin"]));
          $lineHeight = max($lineHeight, $pdf->Plot($lpos[$dd]+($fwidth/3*2),  $curY, $fwidth/3, $care_list["sign"][$WYL[$dd]]["junya"]));
        }
        $pdf->SetDrawColor(200,200,200);
        for ($dd = 0; $dd < $day_size; $dd++){
          $pdf->Line($lpos[$dd]+($fwidth/3), $curY,  $lpos[$dd]+($fwidth/3), $curY + $lineHeight); // 縦線
          $pdf->Line($lpos[$dd]+($fwidth/3*2), $curY,  $lpos[$dd]+($fwidth/3*2), $curY + $lineHeight); // 縦線
        }
        $pdf->SetDrawColor(90,90,90);
        $pdf->Line($most_left, $curY,  $most_right, $curY); // 開始横線
        if (!$pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info)) break;
    }




    $pdf->check_kaipage($curY, $kaipage_counter, $lineHeight, $pt_info, 1);


    //*************************************************************************
    // ループ内PDF処理ここまで（その２）
    //*************************************************************************

} // $pt_list ループ終了

if (@$_REQUEST["empty_print_and_show_preview"]){
    $pdf->Output();
} else {
    $pdf->Output("ondoban".date("Ymd_His").".pdf", "D");
}

?>
