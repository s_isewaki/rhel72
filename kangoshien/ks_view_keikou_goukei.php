<?
    define("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING", 1);
    require_once("ks_common.php");
    require_once("ks_ajax.php");

    if (!$_REQUEST["monday_ymd"]) $_REQUEST["monday_ymd"] = date("Ymd");
    $WYL = create_week_ymd_list($_REQUEST["monday_ymd"]);
    $monday_ymd = $WYL[0];
    $sunday_ymd = $WYL[6];
    $a_ymd0 = ymdhm_to_array($WYL[0]);
    $a_ymd6 = ymdhm_to_array($WYL[6]);
    $monday_slash_ymd = $a_ymd0["slash_ymd"];
    $sunday_slash_ymd = $a_ymd6["slash_ymd"];







//****************************************************************************************************************
// ベッド利用状況一覧
//****************************************************************************************************************
function get_keikou_goukei_list($monday_ymd, $sunday_ymd, $bldg_ward) {
    list($bldg_cd, $ward_cd) = explode("_", $bldg_ward);
    $bldg_cd = (int)$bldg_cd;
    if (!$bldg_cd) $bldg_cd = 1;
    $ward_cd = (int)$ward_cd;

    //=================================================
    // 患者単位で集約した経口情報を、先に取得しておく
    //=================================================
    $sql =
    " select cy.ptif_id, cy.serial_data_in".
    " from sum_ks_care_ymd cy".
    " left outer join sum_ks_mst_pat_attribute opa on (opa.ptif_id = cy.ptif_id)".
    " where serial_data_in like 'a%'".
    " and ymd between ".dbES($monday_ymd)." and ".dbES($sunday_ymd).
    " ".($ward_cd ? " and opa.bldg_cd = ".(int)$bldg_cd." and opa.ward_cd = ".(int)$ward_cd : "");
    $sel = dbFind($sql);
    $keikou_all = array();
    while ($row = @pg_fetch_array($sel)) {
        $ptif_id = $row["ptif_id"];
        $ary = $keikou_all[$ptif_id];
        $data = unserialize($row["serial_data_in"]);

        $asa_f = @$data["keikou_asa_fukusyoku"];   // 朝 副食
        $hiru_f = @$data["keikou_hiru_fukusyoku"]; // 昼 副食
        $yu_f = @$data["keikou_yu_fukusyoku"];     // 夕 副食
        $asa_s = @$data["keikou_asa_syusyoku"];   // 朝 主食
        $hiru_s = @$data["keikou_hiru_syusyoku"]; // 昼 主食
        $yu_s = @$data["keikou_yu_syusyoku"];     // 夕 主食
        $asa_z = @$data["keikou_asa_zessyoku"];   // 朝 絶食フラグ
        $hiru_z = @$data["keikou_hiru_zessyoku"]; // 昼 絶食フラグ
        $yu_z = @$data["keikou_yu_zessyoku"];     // 夕 絶食フラグ

        if ($asa_f!=""  || $asa_z)  { $ary["asa_f"]  = (int)@$ary["asa_f"]  + (int)$asa_f;   $ary["asa_fz"]  = (int)@$ary["asa_fz"]  + 10; }
        if ($hiru_f!="" || $hiru_z) { $ary["hiru_f"] = (int)@$ary["hiru_f"] + (int)$hiru_f;  $ary["hiru_fz"] = (int)@$ary["hiru_fz"] + 10; }
        if ($yu_f!=""   || $yu_z)   { $ary["yu_f"]   = (int)@$ary["yu_f"]   + (int)$yu_f;    $ary["yu_fz"]   = (int)@$ary["yu_fz"]   + 10; }
        if ($asa_s!=""  || $asa_z)  { $ary["asa_s"]  = (int)@$ary["asa_s"]  + (int)$asa_s;   $ary["asa_sz"]  = (int)@$ary["asa_sz"]  + 10; }
        if ($hiru_s!="" || $hiru_z) { $ary["hiru_s"] = (int)@$ary["hiru_s"] + (int)$hiru_s;  $ary["hiru_sz"] = (int)@$ary["hiru_sz"] + 10; }
        if ($yu_s!=""   || $yu_z)   { $ary["yu_s"]   = (int)@$ary["yu_s"]   + (int)$yu_s;    $ary["yu_sz"]   = (int)@$ary["yu_sz"]   + 10; }

        if (substr($asa_f,0,2)=='0.')  { $ary["asa_kf"]  = (int)@$ary["asa_kf"]  + (int)substr($asa_f,2);  }
        if (substr($hiru_f,0,2)=='0.') { $ary["hiru_kf"] = (int)@$ary["hiru_kf"] + (int)substr($hiru_f,2); }
        if (substr($yu_f,0,2)=='0.')   { $ary["yu_kf"]   = (int)@$ary["yu_kf"]   + (int)substr($yu_f,2);   }
        if (substr($asa_s,0,2)=='0.')  { $ary["asa_ks"]  = (int)@$ary["asa_ks"]  + (int)substr($asa_s,2);  }
        if (substr($hiru_s,0,2)=='0.') { $ary["hiru_ks"] = (int)@$ary["hiru_ks"] + (int)substr($hiru_s,2); }
        if (substr($yu_s,0,2)=='0.')   { $ary["yu_ks"]   = (int)@$ary["yu_ks"]   + (int)substr($yu_s,2);   }

        $keikou_all[$ptif_id] = $ary;
    }

    //=================================================
    // 指定週の入院中患者一覧
    //=================================================
    $sql =
    " select bw.ward_name, ptrm.ptrm_name, ptrm.bed_count, bw.bldg_cd, bw.ward_cd, ptrm.ptrm_room_no".
    ",pt.ptif_id, pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm, pt.ptif_lt_kana_nm, pt.ptif_ft_kana_nm".
    " from ptifmst pt".
    " left outer join sum_ks_mst_pat_attribute opa on (opa.ptif_id = pt.ptif_id)".
    " left outer join sum_ks_mst_bldg_ward bw on (bw.bldg_cd = opa.bldg_cd and bw.ward_cd = opa.ward_cd)".
    " left outer join sum_ks_mst_ptrm ptrm on (ptrm.bldg_cd = opa.bldg_cd and ptrm.ward_cd = opa.ward_cd and ptrm.ptrm_room_no = opa.ptrm_room_no)".
    " where 1 = 1 ".
    " ".($ward_cd ? " and bw.bldg_cd = ".(int)$bldg_cd." and bw.ward_cd = ".(int)$ward_cd : "").
    " order by bw.bldg_cd, bw.ward_cd, ptrm.ptrm_name, ptrm.ptrm_room_no, pt.ptif_lt_kana_nm, pt.ptif_ft_kana_nm, pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm";
    $sel = dbFind($sql);

    $html = array();
    $html[]= '<table cellspacing="0" cellpadding="0" class="sijiban_soap_table">';
    $html[]= '<tr><th width="100">病棟</th><th width="80">病室</th><th width="auto">患者氏名</th>';
    $html[]= '<th width="80">朝（副）</th><th width="80">朝（主）</th><th width="80">昼（副）</th>';
    $html[]= '<th width="80">昼（主）</th><th width="80">夜（副）</th><th width="80">夜（主）</th>';
    while ($row = @pg_fetch_array($sel)) {
        $key = ((int)$row["bldg_cd"])."_".((int)$row["ward_cd"])."_".((int)$row["ptrm_room_no"]);
        $data = $keikou_all[$row["ptif_id"]];
        $html[]='<tr>';
        $html[]= '<td>'.hh($row["ward_name"]).($row["ward_name"]==""?'<span style="color:red">（未割当）</span>':'').'</td>';
        $html[]= '<td>'.hh($row["ptrm_name"]).($row["ptrm_name"]==""?'<span style="color:red">（未割当）</span>':'').'</td>';
        $html[]= '<td>'.hh($row["ptif_lt_kaj_nm"]." ".$row["ptif_ft_kaj_nm"]).'</td>';
        $html[]= '<td class="center">'.$data["asa_f"] .($data["asa_kf"] ? "+".$data["asa_kf"] : "")  .($data["asa_fz"]?'/':"") .$data["asa_fz"] .'</td>';
        $html[]= '<td class="center">'.$data["asa_s"] .($data["asa_ks"] ? "+".$data["asa_ks"] : "")  .($data["asa_sz"]?'/':"") .$data["asa_sz"] .'</td>';
        $html[]= '<td class="center">'.$data["hiru_f"].($data["hiru_kf"] ? "+".$data["hiru_kf"] : "").($data["hiru_fz"]?'/':"").$data["hiru_fz"].'</td>';
        $html[]= '<td class="center">'.$data["hiru_s"].($data["hiru_ks"] ? "+".$data["hiru_ks"] : "").($data["hiru_sz"]?'/':"").$data["hiru_sz"].'</td>';
        $html[]= '<td class="center">'.$data["yu_f"]  .($data["yu_kf"] ? "+".$data["yu_kf"] : "")    .($data["yu_fz"]?'/':"")  .$data["yu_fz"]  .'</td>';
        $html[]= '<td class="center">'.$data["yu_s"]  .($data["yu_ks"] ? "+".$data["yu_ks"] : "")    .($data["yu_sz"]?'/':"")  .$data["yu_sz"]  .'</td>';
        $html[]='</tr>';
    }
    $html[]="</table>";
    if (defined("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING")) {
        return implode("\n",$html);
    }
    echo "ok".implode("\n",$html);
}



?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/start/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" type="text/css" href="css/common.css?v=2">
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.js?v=2"></script>
<script type="text/javascript" src="js/common.js?v=2"></script>
<script type="text/javascript" src="js/showpage.js"></script>

<script type="text/javascript">
    var moveWeek = function(day_incriment){
        var monday_ymd = "<?=$monday_ymd?>";
        var dt = new Date(int(substr2(monday_ymd,0,4)), int(substr2(monday_ymd,4,2))-1, int(substr2(monday_ymd,6,2)));
        dt.setTime(dt.getTime() + day_incriment * 86400000);
        document.frm.monday_ymd.value = dt.getFullYear() + ("00"+(dt.getMonth()+1)).slice(-2) + ("00"+dt.getDate()).slice(-2);
        document.frm.submit();
    }
    function showInstruction(){
        $('#efmDrEmpIdInstruction').dialog({ title:'経口合計表　ヘルプ', width:800, height:450 });
    }
    function loaded() {
        if (isNotPC) ee("body").className = "is_not_pc";
        else ee("body").className = "is_pc";
    }
</script>
<title>CoMedix | 看護支援 - 経口合計表</title>
</head>
<body onload="loaded()" id="body">





<? //**************************************************************************************************************** ?>
<? // 画面上部 メインメニュー                                                                                         ?>
<? //**************************************************************************************************************** ?>
<div id="header_menu_div">
<form id="frm" name="frm">
<input type="hidden" name="monday_ymd" value="<?=h($monday_ymd)?>" />
<input type="hidden" name="session" value="<?=GG_SESSION_ID?>" />
<table cellspacing="0" cellpadding="0" id="header_menu_table" style="width:100%; height:30px"><tr>
    <td class="weekbtn" style="text-align:center; width:100px; background:url(img/menubtn_on.gif) repeat-x">経口合計表</td>
    <td style="width:auto; padding-left:10px;"><nobr>
        <table cellspacing="0" cellpadding="0" style="width:auto"><tr>
            <td style="vertical-align:middle">対象週</td>
            <td style="padding-left:6px; vertical-align:middle; width:190px">
                <span style="background-color:#3171dd"><?=$monday_slash_ymd?> 〜 <?=$sunday_slash_ymd?></span>
            </td>
            <td style="padding-left:6px; vertical-align:middle">
                <button type="button" onclick="moveWeek('-7');">≪前週</button>
                <button type="button" onclick="moveWeek('7');">翌週≫</button>
            </td>
            <td style="padding-left:20px; vertical-align:middle">病棟</td>
            <td style="padding-left:6px; vertical-align:middle;">
            <select name="bldg_ward" onchange="document.frm.submit()">
            <?
            $mst_data = get_mst_bldg_ward_ptrm();
            echo implode("\n",$mst_data["bldg_ward_options"]);
            ?>
            <option value="all">（すべて）</option>
            </select>
            <script type="text/javascript">
            setComboValue(document.frm.bldg_ward, "<?=js($_REQUEST["bldg_ward"])?>");
            </script>
            </td>
        </tr></table>
    </nobr></td>
    <td style="width:50px"><a class="menubtn menubtn50" href="javascript:void(0)" onclick="showInstruction()"
         style="border-left:1px solid #88aaee">ヘルプ</a></td>
    <td style="width:50px"><a class="menubtn closebtn" href="javascript:void(0)" onclick="window.close();">×</a></td>
</tr></table>
</form>
</div>





<div style="display:none">


<? //**************************************************************************************************************** ?>
<? // ヘルプ                                                                                                          ?>
<? //**************************************************************************************************************** ?>
<form id="efmDrEmpIdInstruction">
<div style="padding-top:10px">この画面は、患者別で経口摂取量の週間合計を表示します。</div>
<hr>

<div style="padding-top:10px">◆「分子/分母」形式で表示されます。単位は「割/割」です。分子は摂取量の加算値です。</div>
<div style="padding-left:20px">分母は10の倍数となります。１週間（＝７日）すべての経口が登録された場合に、最大値70となります。</div>

<div style="padding-top:10px">◆経口摂取量に0〜10を登録されている場合に計算対象となります。空欄の場合は計算されません。</div>
<div style="padding-left:20px">絶食をチェックしている場合は、経口摂取量はゼロとみなし、計算対象に含まれます。</div>

<div style="padding-top:10px">例１）任意日付に「朝：副食」＝1、「朝：主食」＝3　を指定した場合</div>
<div style="padding-left:20px">⇒一覧上『朝（副）』：　分母に10加算／分子に1加算</div>
<div style="padding-left:20px">⇒一覧上『朝（主）』：　分母に10加算／分子に3加算</div>
<div style="padding-top:10px">例２）任意日付に「朝：副食」＝0、「朝：主食」＝10　を指定した場合</div>
<div style="padding-left:20px">⇒一覧上『朝（副）』：　分母に10加算／分子は加算なし</div>
<div style="padding-left:20px">⇒一覧上『朝（主）』：　分母に10加算／分子にも10加算</div>
<div style="padding-top:10px">例３）任意日付に「朝：副食」＝空欄、「朝：主食」＝9　を指定した場合</div>
<div style="padding-left:20px">⇒一覧上『朝（副）』：　分子分母ともに加算なし</div>
<div style="padding-left:20px">⇒一覧上『朝（主）』：　分母に10加算／分子に9加算</div>
<div style="padding-top:10px">例４）任意日付の「朝」に「絶食」を指定した場合</div>
<div style="padding-left:20px">⇒一覧上『朝（副）』『朝（主）』ともに、分母に10加算／分子は加算なし</div>

<div style="padding-top:10px">◆ひとクチふたクチで指定された場合は、分子に「クチ数合計」が、プラス記号の後に表示されます。</div>
</form>




</div>


<div id="bed_list_container" style="padding-bottom:20px;">
<?=get_keikou_goukei_list($monday_ymd, $sunday_ymd, $_REQUEST["bldg_ward"])?>
</div>




</body>
</html>
<?
pg_close($con);
