<?
if (!defined("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING")) { // 印刷時は既にdefineされている。この中へは入らない。
    define("IS_AJAX_REQUEST", 1);
    require_once("ks_common.php");
    ob_clean();

    $ajax_action = $_REQUEST["ajax_action"];
    if ($ajax_action=="get_ymdhms") {
		echo "ok".date("YmdHis");
		die;
	}
    if      ($ajax_action=="get_emp_auth_list") get_emp_auth_list(); // 従業員権限 一覧取得（権限マスタ 一覧）
    else if ($ajax_action=="get_dr_list") get_dr_list();
    else if ($ajax_action=="get_mst_options") get_mst_options();
    else if ($ajax_action=="get_mst_bsscale") get_mst_bsscale();
    else if ($ajax_action=="get_mst_bldg_ward_ptrm") get_mst_bldg_ward_ptrm();
    else if ($ajax_action=="get_ptif_attribute") get_ptif_attribute();
    else if ($ajax_action=="get_vital_per_day_list") get_vital_per_day_list();
    else if ($ajax_action=="get_vital_one_data") get_vital_one_data();
    else if ($ajax_action=="get_care_ymd") get_care_ymd();
    else if ($ajax_action=="get_care_kikan_per_day_list") get_care_kikan_per_day_list();
    else if ($ajax_action=="get_care_kikan_one_data") get_care_kikan_one_data();
    else if ($ajax_action=="get_siji_ymd") get_siji_ymd();
    else if ($ajax_action=="get_siji_ymd_one") get_siji_ymd_one();
    else if ($ajax_action=="get_siji_ymd_zenkai_az") get_siji_ymd_zenkai_az();
    else if ($ajax_action=="get_update_history") get_update_history();

	else if ($ajax_action=="save_system_config") save_system_config();
    else if ($ajax_action=="save_emp_auth") save_emp_auth();
    else if ($ajax_action=="save_mst_options") save_mst_options();
    else if ($ajax_action=="save_mst_bsscale") save_mst_bsscale();
    else if ($ajax_action=="save_mst_bldg_ward_ptrm") save_mst_bldg_ward_ptrm();
    else if ($ajax_action=="save_ptif_attribute") save_ptif_attribute();
    else if ($ajax_action=="save_care_ymd") save_care_ymd();
    else if ($ajax_action=="save_care_kikan") save_care_kikan();
    else if ($ajax_action=="save_siji_ymd") save_siji_ymd();
    else if ($ajax_action=="save_siji_ymd_one") save_siji_ymd_one();
    else if ($ajax_action=="save_vital_one_data") save_vital_one_data();

    else if ($ajax_action=="delete_vital_data") delete_vital_data();
    else if ($ajax_action=="delete_care_kikan") delete_care_kikan();
    else if ($ajax_action=="delete_mst_bsscale") delete_mst_bsscale();
    else if ($ajax_action=="delete_siji_ymd") delete_siji_ymd();
    else echo "ajax_action=".$ajax_action; // ajax_action該当なし。プログラム実装エラーである
    pg_close($con);
    die;
}


//****************************************************************************************************************
// 患者名取得
//****************************************************************************************************************
function get_ptif_name($ptif_id) {
    return dbGetOne("select ptif_lt_kaj_nm || ' ' ||ptif_ft_kaj_nm from ptifmst where ptif_id = ".dbES($ptif_id));
}



//****************************************************************************************************************
// 指示の変更履歴 作成
//****************************************************************************************************************
function save_update_history_siji($ptif_id, $siji_content, $ymd, $ins_upd_del, $content_text){
    $sql =
    " insert into sum_ks_update_history_siji (".
    "     ptif_id, siji_content, ymd, display_time, update_emp_id, update_timestamp, update_emp_name, ins_upd_del, content_text".
    " ) values (".
    " ".dbES($ptif_id).",".dbES($siji_content).",".dbES($ymd).",".dbES(date("Y/m/d H:i:s")).",".dbES(GG_LOGIN_EMP_ID).
    ",current_timestamp".",".dbES(GG_LOGIN_EMP_NAME).",".dbES($ins_upd_del).",".dbES($content_text).")";
    dbExec($sql);
}
//****************************************************************************************************************
// ケアの変更履歴 作成
//****************************************************************************************************************
function save_update_history($ptif_id, $care_content, $ymd, $is_delete = ""){
    $sql =
    " select ptif_id from sum_ks_update_history".
    " where ptif_id = ".dbES($ptif_id).
    " and care_content = ".dbES($care_content).
    " and ymd = ".dbES($ymd);
    $is_new = (dbGetOne($sql) ? "" : "1");

    $sql =
    " insert into sum_ks_update_history ( ptif_id, care_content, ymd, display_time, update_emp_id, update_timestamp, update_emp_name, is_delete, is_new".
    " ) values (".
    " ".dbES($ptif_id).",".dbES($care_content).",".dbES($ymd).",".dbES(date("Y/m/d H:i:s")).",".dbES(GG_LOGIN_EMP_ID).
    ",current_timestamp".",".dbES(GG_LOGIN_EMP_NAME).", ".dbES($is_delete).", '".$is_new."')";
    dbExec($sql);
}


//****************************************************************************************************************
// 最終確認者（変更履歴） 一覧取得
//****************************************************************************************************************
function get_update_history(){
    global $care_content_names;
    $ptif_id = dbES($_REQUEST["ptif_id"]);
    $ymd = dbES($_REQUEST["ymd"]);
    $html = array();
    $sql = "select * from sum_ks_update_history where ptif_id = ".$ptif_id." and ymd = ".$ymd." order by update_timestamp";
    $sel = dbFind($sql);
    while($row = pg_fetch_array($sel)){
        $cc = $row["care_content"];
        $new_upd_del = ($row["is_new"] ? "新規" : "更新");
        if ($row["is_delete"]) $new_upd_del = "削除";
        $nm = @$care_content_names[$cc];
        if (!$nm) $nm = $cc;

        array_unshift($html, '<tr class="hover2"><td>'.$row["display_time"].'</td><td>'.$nm.'</td><td>'.hh($row["update_emp_name"]).'</td><td>'.$new_upd_del.'</td></tr>');
    }
    echo 'ok'.implode("\n", $html);
}


//****************************************************************************************************************
// 従業員権限 保存
//****************************************************************************************************************
function save_system_config() {
    $sc_key = $_REQUEST["sc_key"];
    $sc_value = $_REQUEST["sc_value"];
    if (!$sc_key) { echo "登録できませんでした。"; die; }
    dbExec("delete from system_config where key = 'kangoshien.".$sc_key."'");
    dbExec("insert into system_config (key, value) values ('kangoshien.".$sc_key."', ".dbES($sc_value).")");
    echo 'ok'.$sc_value;
}

//****************************************************************************************************************
// 従業員権限 保存
//****************************************************************************************************************
function save_emp_auth() {
    $emp_id = $_REQUEST["emp_id"];
    if (!dbGetOne("select emp_id from sum_ks_mst_emp_auth where emp_id = ".dbES($emp_id))) {
        dbExec("insert into sum_ks_mst_emp_auth ( emp_id, patient_control_auth ) values (".dbES($emp_id).", 0)");
    }
    $flg = ($_REQUEST["current_auth"] ? "0" : "1");
    dbExec("update sum_ks_mst_emp_auth set patient_control_auth = ".$flg." where emp_id = ".dbES($emp_id));
    echo 'ok';
}


//****************************************************************************************************************
// 従業員権限 一覧取得（権限マスタ 一覧）
//****************************************************************************************************************
function get_emp_auth_list(){
    $keyword = trim(mb_ereg_replace("　", " ", $_REQUEST["keyword"]));
    $sql_keyword = "";
    if ($keyword) {
        $ary = explode(" ", $keyword);
        foreach ($ary as $key) {
            if (!$key) continue;
            $k = pg_escape_string($key);
            $sql_keyword .= " and (emp.emp_id like '%".$k."%' or emp.emp_lt_nm like '%".$k."%' or emp.emp_ft_nm like '%".$k."%')";
        }
    }

    $sql =
    " select emp.emp_id, emp.emp_lt_nm, emp.emp_ft_nm, auth.patient_control_auth, am.emp_ambadm_flg from empmst emp".
    " left outer join sum_ks_mst_emp_auth auth on ( auth.emp_id = emp.emp_id)".
    " left outer join authmst am on (am.emp_id = emp.emp_id)".
    " where am.emp_amb_flg = true" . $sql_keyword.
    " order by emp.emp_kn_lt_nm, emp.emp_kn_ft_nm";
    $sel = dbFind($sql);
    echo 'ok';
    while($row = pg_fetch_array($sel)){
        $is_admin = ($row["emp_ambadm_flg"]=="t" ? 1 :"");
        echo '<tr><td>'.hh($row["emp_id"]).'</td><td>';
        if (GG_ONDOBAN_ADMIN_FLG && !$is_admin) {
            echo '<a href="javascript:void(0)" onclick="dfmMstEmpAuth.saveData(\''.js($row["emp_id"]).'\',\''.($row["patient_control_auth"]?"1":"").'\')">';
        }
        echo hh($row["emp_lt_nm"]." ".$row["emp_ft_nm"]);
        if (GG_ONDOBAN_ADMIN_FLG && !$is_admin) echo '</a>';
        echo '</td>';
        echo '<td class="center">';
        if ($is_admin) echo '<span style="color:#aaaaaa">（システム管理者）</span>';
        else if ($row["patient_control_auth"]) echo "温度板管理者";
        echo '</td></tr>';
    }
}

//****************************************************************************************************************
// 主治医候補 一覧取得
//****************************************************************************************************************
function get_dr_list(){
    $sql =
    " select emp.emp_id, emp.emp_lt_nm || ' ' || emp.emp_ft_nm as emp_nm from empmst emp".
    " inner join jobmst job on (job.job_id = emp.emp_job)".
    " where (job.job_nm = '医師' or job.job_nm = '常勤医師' or job.job_nm = '非常勤医師') and job_del_flg = 'f'".
    " and ( emp.emp_retire is null or emp.emp_retire = '' or emp.emp_retire > '".date("Ymd")."')".
    " order by emp.emp_lt_nm, emp.emp_ft_nm";
    $sel = dbFind($sql);
    $out = array();
    while($row = pg_fetch_array($sel)){
        $out[] = '{emp_id:"'.js($row["emp_id"]).'", emp_nm:"'.js($row["emp_nm"]).'"}';
    }
    echo 'ok['.implode(",", $out).']';
}

//****************************************************************************************************************
// 指示情報 保存
//****************************************************************************************************************
function save_siji_ymd(){
    $ptif_id = dbES($_REQUEST["ptif_id"]);
    $ymd = dbES($_REQUEST["ymd"]);
    $section = $_REQUEST["section"];
    $try_commit = $_REQUEST["try_commit"];

    foreach ($_POST as $k => $v) {
        if ($v=="") unset($_POST[$k]);
    }

    $serial_data = "''";
    foreach($_POST as $k => $v) {
        if ($k==$section."__field_count") continue;
        if ($v!="") { $serial_data = dbES(serialize($_POST)); break; }
    }

    if (!$try_commit) { echo "ok"; return; }
    if (!dbGetOne("select count(*) from sum_ks_siji_ymd where ptif_id = ".$ptif_id." and ymd = ".$ymd)) {
        dbExec("insert into sum_ks_siji_ymd (ptif_id, ymd) values (".$ptif_id.",".$ymd.")");
    }
    $sql =
    " update sum_ks_siji_ymd set".
    " serial_data_".$section." = ".$serial_data.
    ",update_emp_id = ".dbES(GG_LOGIN_EMP_ID).
    ",update_emp_name = ".dbES(GG_LOGIN_EMP_NAME).
    ",update_timestamp = current_timestamp".
    " where ptif_id = ".$ptif_id." and ymd = ".$ymd;
    dbExec($sql);

    save_update_history_siji($_REQUEST["ptif_id"], $section, $_REQUEST["ymd"], "", "");

    echo 'ok';
}


//****************************************************************************************************************
// 指示情報 保存（指示板系）
//****************************************************************************************************************
function save_siji_ymd_one(){
    $ptif_id = dbES($_REQUEST["ptif_id"]);
    $ymd = dbES($_REQUEST["ymd"]);
    $section = $_REQUEST["section"];
    $try_commit = $_REQUEST["try_commit"];
    $is_delete = $_REQUEST["is_delete"];
    $field_number = $_REQUEST["field_number"];

    if (!$try_commit) { echo "ok"; return; }
    if (!dbGetOne("select count(*) from sum_ks_siji_ymd where ptif_id = ".$ptif_id." and ymd = ".$ymd)) {
        dbExec("insert into sum_ks_siji_ymd (ptif_id, ymd) values (".$ptif_id.",".$ymd.")");
    }

    $serial_data = dbGetOne("select serial_data_".$section." from sum_ks_siji_ymd where ptif_id = ".$ptif_id." and ymd = ".$ymd);
    $data = unserialize($serial_data);
    $field_count = (int)$data[$section."__field_count"];

    $new_serial_data = "";
    $ins_upd_del = "";
    $unset_fields = array();
    $new_data = array();
    if ($is_delete) {
        $ins_upd_del = "削除";
        $ary = array();
        if (is_array($data)) {
            foreach($data as $key => $v) {
                $keys = explode("__", $key);
                if (count($keys)==3) {
                    if ($keys[1]==$field_number) continue;
                    $new_key = $keys[0]."__".($keys[1]>$field_number ? $keys[1]-1 : $keys[1])."__".$keys[2];
                    $new_data[$new_key] = $v;
                } else {
                    $new_data[$key] = $v;
                }
            }
            $new_data[$section."__field_count"] = max(0, $field_count-1);
            $new_serial_data = serialize($new_data);
        }
    }
    else {
        $ins_upd_del = "更新";
        if (!$field_number) {
            $field_number = $field_count + 1;
            $ins_upd_del = "新規";
        }

        $prefix = $section."__".$field_number."__";
        if (is_array($data)) {
            foreach($data as $key => $v) {
                if (strpos($key, $prefix)===0) unset($data[$key]);
            }
        }
        foreach ($_POST as $key => $v) {
            $data[$prefix.$key] = $v;
        }
        $unset_fields[] = $prefix."dlg_display_time";
        $data[$prefix."dlg_display_time"] = date("Y/m/d H:i:s");
        $data[$prefix."dlg_update_emp_id"] = GG_LOGIN_EMP_ID;
        $data[$prefix."dlg_update_emp_name"] = GG_LOGIN_EMP_NAME;
        if ($field_count < $field_number) $data[$section."__field_count"] = $field_number;
        $new_serial_data = serialize($data);
        $new_data = $data;
    }
    $sql_az = "";
    if ($section=="chusya") {
        $field_count = (int)$new_data[$section."__field_count"];
        for ($idx=1; $idx<=$field_count; $idx++) {
            $az = get_alias($new_data["chusya__".$idx."__siji_az"]);
            if ($az) $az_list[] = $az;
        }
        $sql_az = ",chusya_az_list = ".(count($az_list) ? "'\t".implode("\t",$az_list)."\t'" : "NULL");
    }

    $sql =
    " update sum_ks_siji_ymd set".
    " serial_data_".$section." = ".dbES($new_serial_data).
    " ".$sql_az.
    ",update_emp_id = ".dbES(GG_LOGIN_EMP_ID).
    ",update_emp_name = ".dbES(GG_LOGIN_EMP_NAME).
    ",update_timestamp = current_timestamp".
    " where ptif_id = ".$ptif_id." and ymd = ".$ymd;
    dbExec($sql);

    if ($section=="soap" || $section=="daily_bikou") {
        $content_text = "";
        if ($ins_upd_del!="削除") {
            if ($section=="soap") {
                $ary = array();
                if ($_REQUEST["siji_text"]!="") $ary[]= "≪SOA≫".$_REQUEST["siji_text"];
                if ($_REQUEST["siji_ptext"]!="") $ary[]= "≪P≫".$_REQUEST["siji_ptext"];
                $content_text = implode("\n",$ary);
            }
            if ($section=="daily_bikou") {
				if ($_REQUEST["is_henkou"]) $content_text = "【変】";
                $content_text .= $_REQUEST["siji_text"];
            }
        }
        // 変更があれば履歴作成
        if (isDataDifferent($serial_data, $new_serial_data, $unset_fields)) {
            save_update_history_siji($_REQUEST["ptif_id"], $section, $_REQUEST["ymd"], $ins_upd_del, $content_text);
        }
    }
    if ($section=="chusya") {
	    save_update_history_siji($_REQUEST["ptif_id"], $section, $_REQUEST["ymd"], "", "");
	}
    echo 'ok';
}

function isDataDifferent($serial1, $serial2, $unset_fields){
    $obj1 = unserialize($serial1);
    $obj2 = unserialize($serial2);
    foreach ($unset_fields as $f) {
        unset($obj1[$f]);
        unset($obj2[$f]);
    }
    return (serialize($obj1) !== serialize($obj2));
}


//****************************************************************************************************************
// 指示情報 削除
//****************************************************************************************************************
function delete_siji_ymd(){
    $ptif_id = dbES($_REQUEST["ptif_id"]);
    $ymd = dbES($_REQUEST["ymd"]);
    $section = $_REQUEST["section"];
    $sql =
    " update sum_ks_siji_ymd set".
    " serial_data_".$section." = ''".
    ",update_emp_id = ".dbES(GG_LOGIN_EMP_ID).
    ",update_emp_name = ".dbES(GG_LOGIN_EMP_NAME).
    ",update_timestamp = current_timestamp".
    " where ptif_id = ".$ptif_id." and ymd = ".$ymd;
    dbExec($sql);
    echo 'ok';
}


//****************************************************************************************************************
// ケア情報 保存
//****************************************************************************************************************
function save_care_ymd(){
    foreach ($_POST as $k => $v) {
        if ($v=="") unset($_POST[$k]);
    }
    $ptif_id = dbES($_REQUEST["ptif_id"]);
    $ymd = dbES($_REQUEST["ymd"]);
    $section = $_REQUEST["section"];
    $_POST["dlg_display_time"] = date("Y/m/d H:i:s");
    $_POST["dlg_update_emp_id"] = GG_LOGIN_EMP_ID;
    $_POST["dlg_update_emp_name"] = GG_LOGIN_EMP_NAME;

    $serial_data = dbES(serialize($_POST));
    if (!dbGetOne("select count(*) from sum_ks_care_ymd where ptif_id = ".$ptif_id." and ymd = ".$ymd)) {
        dbExec("insert into sum_ks_care_ymd (ptif_id, ymd) values (".$ptif_id.",".$ymd.")");
    }
    $sql =
    " update sum_ks_care_ymd set".
    " serial_data_".$section." = ".$serial_data.
    ",update_emp_id = ".dbES(GG_LOGIN_EMP_ID).
    ",update_emp_name = ".dbES(GG_LOGIN_EMP_NAME).
    ",update_timestamp = current_timestamp".
    " where ptif_id = ".$ptif_id." and ymd = ".$ymd;
    dbExec($sql);
    save_update_history($_REQUEST["ptif_id"], $_REQUEST["section"], $_REQUEST["ymd"]);
    echo 'ok';
}


//****************************************************************************************************************
// 期間ケア情報 保存
// データはデリート＋インサート登録する。
//****************************************************************************************************************
function save_care_kikan(){
    global $care_content_names;
    $ptif_id = dbES($_REQUEST["ptif_id"]);
    $ymd = $_REQUEST["ymd"];
    $care_content_list = explode(",", $_REQUEST["care_content_list"]);
    $sql_list = array();
    foreach ($care_content_list as $care_content) {
        if (!$care_content) continue;
        // DBに存在するものの一覧を$del_keysに取得しておく。
        // この$del_keysから、画面から渡ってきたものを引いたものは、ゴミデータにつき削除される。
        $sql =
        " select ymdhm_fr from sum_ks_care_kikan".
        " where ptif_id = ".$ptif_id.
        " and ymdhm_fr <= '".$ymd."9999'".
        " and (ymdhm_to >= '".$ymd."0000' or ymdhm_to = '')".
        " and care_content = ".dbES($care_content);
        $sel = dbFind($sql);
        $del_keys = array();
        while ($raw_row = @pg_fetch_array($sel)) {
            $del_keys[$raw_row["ymdhm_fr"]] = "DBに存在！";
        }
        // 画面から送信された値を取得
        $field_count = (int)$_REQUEST[$care_content."__field_count"]; // 画面での登録数
        $reqary = array(); // 登録対象アレイ。１オリジン。
        foreach($_REQUEST as $k => $v) {
            $keyary = explode("__", $k);
            if (count($keyary)!=3) continue; // [care_content] + "__" + [番号] + "__" + [ﾌｨｰﾙﾄﾞ名] 形式でなければスルー
            if ($keyary[0]!=$care_content) continue; // 上位ループ内で決まっている「保存更新対象」のcare_contentでなければスルー
            $reqary[(int)$keyary[1]][$keyary[2]] = $v;  // 登録用アレイに[番号]&[ﾌｨｰﾙﾄﾞ名]キーで値を格納。１オリジン
            if ($keyary[2]=="ymdhm_fr") unset($del_keys[$v]); // 画面から渡ってきた。DBエントリから削除する。
        }
        // 画面から渡ってきたものを差し引いた残りは、ゴミデータにつき削除対象である。
        // 登録対象アレイに、日付だけのカラエントリを追加してゆく。
        foreach ($del_keys as $ymdhm_fr => $dummy) {
            $field_count++; // 新番号を発番
            $reqary[$field_count]["ymdhm_fr"] = $ymdhm_fr; // 新インデックスに、日付フィールドだけのエントリを追加。何度もいうが、$reqaryは１オリジン。
        }

        $is_delete = "1";
        for ($idx=1; $idx<=$field_count; $idx++) {
            $info = array();
            $ccn = $care_content_names[$care_content];
            if ($care_content!="risyo_riyu") $ccn .= "(".$idx.")";
            $ccn .= "：";
            $req = $reqary[$idx]; // レコードを呼び出す。$reqは連想配列。削除対象なら日付だけの連想配列。
            $serial_data = serialize($req); // データは丸ごと直列化文字列データにする
            $org_ymdhm_fr = $req["ymdhm_fr"]; // 変更前の期間開始年月。あるいは削除対象となる開始年月。

            $sijibi_ymd = trim($req["sijibi_ymd"]);
            $siji_az = trim($req["siji_az"]);

            // 期間取得。もし画面で削除ボタンを押していたとしたら、
            // 以下の４つはすべてカラとなっているであろう。
            $fr_ymd = trim($req["fr_ymd"]); // 画面指定した期間開始年月日
            $fr_hm = trim($req["fr_hm"]); // 画面指定した期間開始時刻
            $to_ymd = trim($req["to_ymd"]); // 画面指定した期間終了年月日
            $to_hm = trim($req["to_hm"]); // 画面指定した期間終了時刻

            $change_ymd_list = array();

            // 削除SQLだけ、先に作成だけ、しておく。
            if ($care_content!="risyo_riyu") {
	            $info["del"] =
	            "delete from sum_ks_care_kikan where ptif_id = ".$ptif_id." and ymdhm_fr = ".dbES($org_ymdhm_fr)." and care_content = ".dbES($care_content);
            }
            else if ($_REQUEST["risyo_riyu__1__ymdhm_fr"]) { // 離床理由の場合
	            $info["del"] =
	            "delete from sum_ks_care_kikan where ptif_id = ".$ptif_id." and ymdhm_fr = ".dbES($_REQUEST["risyo_riyu__1__ymdhm_fr"])." and care_content = ".dbES($care_content);
			}

            // 期間終了年月を取得する
            $org_ymdhm_to = ""; // 変更前の期間終了年月を、のちのち格納するかもしれない。
            if ($org_ymdhm_fr) {
                $sql =
                " select ymdhm_to from sum_ks_care_kikan where ptif_id = ".$ptif_id." and ymdhm_fr = ".dbES($org_ymdhm_fr).
                " and care_content = ".dbES($care_content);
                $org_ymdhm_to = dbGetOne($sql);
                if ($fr_ymd=="" && $fr_hm=="") { // 削除の場合
                    $change_ymd_list[substr($org_ymdhm_fr, 0 ,8)] = 1;
                    $change_ymd_list[substr($org_ymdhm_to, 0 ,8)] = 1;
                }
            }

            // 開始年月が無い場合。
            // 終了年月があれば、入力不備とする。
            // ただし、終了年月も無い場合は、上記の削除SQLのみ発行し、データを消すのみで、のちインサートはしないこととする。
            if ($fr_ymd=="" && $fr_hm=="") {
                if ($to_ymd!="" || $to_hm!="") { echo $ccn."開始日時は必須です。"; return; }
            }
            // 開始年月がある場合
            if ($fr_ymd!="" || $fr_hm!="") {
                if (!is_number($fr_ymd) || !is_number($fr_hm) ) { echo $ccn."開始日時を指定してください。"; return; }
                $ymdhm_fr = $fr_ymd.$fr_hm;

                $ymdhm_to = "";
                if ($to_ymd!="" || $to_hm!="") {
                    if (!is_number($to_ymd) || !is_number($to_hm) ) {  echo $ccn."終了日時を指定してください。"; return; }
                    $ymdhm_to = $to_ymd.$to_hm;
                    if (to_24hour($ymdhm_fr) > to_24hour($ymdhm_to)) { echo $ccn."終了日時は開始日時より過去を指定できません。"; return; }
                }

                if ($fr_ymd > $ymd) { echo $ccn."期間の誤入力の可能性があります。指示日を含まない期間は指定できません。"; return; }
                if ($to_ymd && $to_ymd < $ymd) { echo $ccn."期間の誤入力の可能性があります。指示日を含まない期間は指定できません。"; return; }

                $where = " where ptif_id = ".$ptif_id." and care_content = ".dbES($care_content);
                if ($org_ymdhm_fr && $org_ymdhm_fr!=$ymdhm_fr && $care_content!="kouseizai") {
                    if (dbGetOne("select count(*) from sum_ks_care_kikan ".$where." and ymdhm_fr = ".dbES($ymdhm_fr))) {
                        echo $ccn."指定した開始日時のデータは既に存在するため、登録できません。"; return;
                    }
                }

                if ($org_ymdhm_fr) $change_ymd_list[substr($org_ymdhm_fr, 0 ,8)] = 1;
                if ($ymdhm_fr) $change_ymd_list[substr($ymdhm_fr, 0 ,8)] = 1;
                if ($ymdhm_to) $change_ymd_list[substr($ymdhm_to, 0 ,8)] = 1;
                if ($org_ymdhm_to) $change_ymd_list[substr($org_ymdhm_to, 0 ,8)] = 1;

                // インサート文を作成のみ、しておく。
                $is_delete = "";
                $info["ins"] =
                " insert into sum_ks_care_kikan (".
                "     ptif_id, care_content, ymdhm_fr, ymdhm_to, serial_data, sijibi_ymd, siji_az, update_emp_id, update_timestamp".
                " ) values (".
                $ptif_id.",".dbES($care_content).",".dbES($ymdhm_fr).",".dbES($ymdhm_to).",".dbES($serial_data).",".dbES($sijibi_ymd).
                ",".dbES($siji_az).",".dbES(GG_LOGIN_EMP_ID).",current_timestamp)";
            }
            $sql_list[] = $info;
        }
        $info = array();
        foreach ($change_ymd_list as $tmp_ymd => $dummy) {
            $info["hist"][] = array("ptif_id"=>$_REQUEST["ptif_id"], "care_content"=>$care_content, "ymd"=>$tmp_ymd, "is_delete"=>$is_delete);
        }
        $sql_list[] = $info;
    }
    foreach ($sql_list as $info) {
        if ($info["del"]) dbExec($info["del"]);
    }
    foreach ($sql_list as $info) {
        if ($info["ins"]) dbExec($info["ins"]);
        if (is_array($info["hist"])) {
            foreach ($info["hist"] as $idx => $row) {
                save_update_history($row["ptif_id"], $row["care_content"], $row["ymd"], $row["is_delete"]);
            }
        }
    }
    echo 'ok';
}


//****************************************************************************************************************
// 期間ケア情報 削除（離床理由のみ）
//****************************************************************************************************************
function delete_care_kikan(){
    $ptif_id = $_REQUEST["ptif_id"];
    $ymdhm_fr = $_REQUEST["ymdhm_fr"];
    $care_content = $_REQUEST["care_content"];
    $sql =
    " select serial_data from sum_ks_care_kikan where ptif_id = ".dbES($ptif_id).
    " and ymdhm_fr = ".dbES($ymdhm_fr)." and care_content = ".dbES($care_content);
    $serial_data = unserialize(dbGetOne($sql));
    dbExec("delete from sum_ks_care_kikan where ptif_id = ".dbES($ptif_id)." and ymdhm_fr = ".dbES($ymdhm_fr)." and care_content = ".dbES($care_content));
    if (strlen($serial_data["fr_ymd"])==8) save_update_history($ptif_id, $care_content, $serial_data["fr_ymd"], "1");
    if (strlen($serial_data["to_ymd"])==8) save_update_history($ptif_id, $care_content, $serial_data["to_ymd"], "1");
    echo 'ok';
}

//****************************************************************************************************************
// 病棟・部屋マスタ追加削除
//****************************************************************************************************************
function save_mst_bldg_ward_ptrm(){
    $mode = $_REQUEST["mode"];
    $try_commit = $_REQUEST["try_commit"];
    list($bldg_cd, $ward_cd) = explode("_", $_REQUEST["bldg_ward"]);
    $bldg_cd = (int)$bldg_cd;
    if (!$bldg_cd) $bldg_cd = 1;
    $ward_cd = (int)$ward_cd;
    $add_ward_name = dbES($_REQUEST["add_ward_name"]);
    $mod_ward_name = dbES($_REQUEST["mod_ward_name"]);
    $ptrm_room_no = (int)$_REQUEST["ptrm_room_no"];
    $add_ptrm_name = dbES($_REQUEST["add_ptrm_name"]);
    $mod_ptrm_name = dbES($_REQUEST["mod_ptrm_name"]);
    $add_bed_count = (int)$_REQUEST["add_bed_count"];
    $mod_bed_count = (int)$_REQUEST["mod_bed_count"];
    if ($mode=="mod_bldg_ward" && !$ward_cd) { echo "名称変更する病棟が指定されていません。"; return; }
    if ($mode=="mod_bldg_ward" && !$_REQUEST["mod_ward_name"]) { echo "変更後の病棟名を指定してください。"; return; }
    if ($mode=="add_bldg_ward" && !$_REQUEST["add_ward_name"]) { echo "追加する病棟名を指定してください。"; return; }
    if ($mode=="del_bldg_ward" && !$ward_cd) { echo "削除する病棟が指定されていません。"; return; }
    if ($mode=="mod_ptrm" && !$_REQUEST["ptrm_room_no"]) { echo "名称変更対象の病室が指定されていません。"; return; }
    if ($mode=="mod_ptrm" && !$_REQUEST["mod_ptrm_name"]) { echo "変更後の病室名を指定してください。"; return; }
    if ($mode=="add_ptrm" && !$_REQUEST["add_ptrm_name"]) { echo "追加する病室名を指定してください。"; return; }
    if ($mode=="del_ptrm" && !$_REQUEST["ptrm_room_no"]) { echo "削除する病室が指定されていません。"; return; }
    $sql = "select count(*) from sum_ks_mst_bldg_ward where ward_name = ".$add_ward_name;
    if ($mode=="add_bldg_ward" && dbGetOne($sql)) { echo "同名の病棟が既に存在します。"; return; }
    $sql = "select count(*) from sum_ks_mst_bldg_ward where ( bldg_cd <> ".$bldg_cd. " or ward_cd <> ".$ward_cd.") and ward_name=".$mod_ward_name;
    if ($mode=="mod_bldg_ward" && dbGetOne($sql)) { echo "同名の病棟が既に存在します。"; return; }
    $sql = "select count(*) from sum_ks_mst_ptrm where bldg_cd=".$bldg_cd." and ward_cd = ".$ward_cd." and ptrm_name = ".$add_ptrm_name;
    if ($mode=="add_ptrm" && dbGetOne($sql)) { echo "同名の病室が既に存在します。"; return; }
    $sql =
    " select count(*) from sum_ks_mst_ptrm where bldg_cd = ".$bldg_cd. " and ward_cd = ".$ward_cd.
    " and ptrm_room_no <> ".$ptrm_room_no." and ptrm_name = ".$mod_ptrm_name;
    if ($mode=="mod_ptrm" && dbGetOne($sql)) { echo "同名の病室が既に存在します。"; return; }
    if (!$try_commit) { echo "ok{}"; return; }

    $where = " where bldg_cd = ".$bldg_cd. " and ward_cd = ".$ward_cd;
    $where2 = $where." and ptrm_room_no = ".$ptrm_room_no;

    if ($mode=="del_bldg_ward") {
        dbExec("delete from sum_ks_mst_bldg_ward" . $where);
        dbExec("delete from sum_ks_mst_ptrm" . $where);
        echo 'ok{bldg_ward:"", ptrm_room_no:""}'; return;
    }
    if ($mode=="add_bldg_ward") {
        $new_ward_cd = 1 + ((int)dbGetOne("select max(ward_cd) from sum_ks_mst_bldg_ward where bldg_cd = ".$bldg_cd));
        dbExec("insert into sum_ks_mst_bldg_ward (bldg_cd, ward_cd, ward_name) values (".$bldg_cd.",".$new_ward_cd.",".$add_ward_name.")");
        echo 'ok{bldg_ward:"'.$bldg_cd."_".$new_ward_cd.'", ptrm_room_no:""}'; return;
    }
    if ($mode=="mod_bldg_ward") {
        dbExec("update sum_ks_mst_bldg_ward set ward_name = ".$mod_ward_name.$where);
        echo 'ok{bldg_ward:"'.js($_REQUEST["bldg_ward"]).'", ptrm_room_no:""}'; return;
    }
    if ($mode=="del_ptrm") {
        dbExec("delete from sum_ks_mst_ptrm".$where2);
        echo 'ok{bldg_ward:"'.js($_REQUEST["bldg_ward"]).'", ptrm_room_no:""}'; return;
    }
    if ($mode=="add_ptrm") {
        $new_ptrm_room_no = 1 + ((int)dbGetOne("select max(ptrm_room_no) from sum_ks_mst_ptrm".$where));
        dbExec("insert into sum_ks_mst_ptrm (bldg_cd, ward_cd, ptrm_room_no, ptrm_name, bed_count) values (".
            $bldg_cd.",".$ward_cd.",".$new_ptrm_room_no.",".$add_ptrm_name.",".$add_bed_count.")"
        );
        echo 'ok{bldg_ward:"'.js($_REQUEST["bldg_ward"]).'", ptrm_room_no:"'.$new_ptrm_room_no.'"}'; return;
    }
    if ($mode=="mod_ptrm") {
        dbExec("update sum_ks_mst_ptrm set ptrm_name = ".$mod_ptrm_name.", bed_count=".$mod_bed_count.$where2);
        echo 'ok{bldg_ward:"'.js($_REQUEST["bldg_ward"]).'", ptrm_room_no:"'.$ptrm_room_no.'"}'; return;
    }
}


//****************************************************************************************************************
// 採用薬マスタ追加削除
//****************************************************************************************************************
function save_mst_options(){
    $mode = $_REQUEST["mode"];
    $try_commit = $_REQUEST["try_commit"];

    $opt_cd = "";
    $opt_name = trim($_REQUEST["opt_name"]);
    $add_opt_cd = trim($_REQUEST["add_opt_cd"]);
    $mod_opt_cd = trim($_REQUEST["mod_opt_cd"]);
    $add_opt_name = trim($_REQUEST["add_opt_name"]);
    $mod_opt_name = trim($_REQUEST["mod_opt_name"]);
    $add_opt_alias = trim($_REQUEST["add_opt_alias"]);
    $mod_opt_alias = trim($_REQUEST["mod_opt_alias"]);
    $add_opt_comma_selection = trim($_REQUEST["add_opt_comma_selection"]);
    $mod_opt_comma_selection = trim($_REQUEST["mod_opt_comma_selection"]);
    $opt_group = trim($_REQUEST["opt_group"]);

    if ($mode=="mod_opt" && !strlen($_REQUEST["opt_name"])) { echo "名称変更対象の項目が指定されていません。"; return; }
    if ($mode=="mod_opt" && !strlen($_REQUEST["mod_opt_name"])) { echo "変更後の項目名を指定してください。"; return; }
    if ($mode=="mod_opt" && !strlen($_REQUEST["mod_opt_cd"])) { echo "変更後のコードを指定してください。"; return; }
    if ($mode=="add_opt" && !strlen($_REQUEST["add_opt_name"])) { echo "追加する項目名を指定してください。"; return; }
    if ($mode=="add_opt" && !strlen($_REQUEST["add_opt_cd"])) { echo "追加する項目のコードを指定してください。"; return; }
    if ($mode=="del_opt" && !strlen($_REQUEST["opt_name"])) { echo "削除する項目が指定されていません。"; return; }

    $sql_base = "select count(*) from sum_ks_mst_options where opt_group = ".dbES($opt_group);
    if ($mode=="add_opt"){
        if (dbGetOne($sql_base." and opt_name = ".dbES($add_opt_name))) { echo "同じ項目名が既に存在します。"; return; }
    }
    if ($mode=="mod_opt"){
        if (dbGetOne($sql_base." and opt_name <> ".dbES($opt_name)." and opt_name = ".dbES($mod_opt_name))) { echo "同じ項目名が既に存在します。"; return; }
    }
    if (!$try_commit) { echo 'ok{}'; return; }

    if ($mode=="del_opt") {
        dbExec("delete from sum_ks_mst_options where opt_group = ".dbES($opt_group)." and opt_name = ".dbES($opt_name));
        $opt_name = "";
    }
    if ($mode=="add_opt") {
        $sql =
        " insert into sum_ks_mst_options (opt_cd, opt_name, opt_group, opt_alias, opt_comma_selection) values (".
        " ".dbES($add_opt_cd).",".dbES($add_opt_name).",".dbES($opt_group).",".dbES($add_opt_alias).",".dbES($add_opt_comma_selection).")";
        dbExec($sql);
        $opt_cd = $add_opt_cd;
        $opt_name = $add_opt_name;
    }
    if ($mode=="mod_opt") {
        $sql =
        " update sum_ks_mst_options set".
        " opt_name = ".dbES($mod_opt_name).", opt_cd = ".dbES($mod_opt_cd).", opt_alias = ".dbES($mod_opt_alias).
        ",opt_comma_selection = ".dbES($mod_opt_comma_selection).
        " where opt_group = ".dbES($opt_group)." and opt_name = ".dbES($opt_name);
        dbExec($sql);
        $opt_cd = $mod_opt_cd;
        $opt_name = $mod_opt_name;
    }
    echo 'ok{opt_group:"'.$opt_group.'", opt_cd:"'.js($opt_cd).'", opt_name:"'.js($opt_name).'"}';
}


//****************************************************************************************************************
// BSスケールマスタ クリア
//****************************************************************************************************************
function delete_mst_bsscale(){
    $ptif_id = dbES($_REQUEST["ptif_id"]);
    $bs_type = dbES($_REQUEST["bs_type"]);
    dbExec("delete from sum_ks_mst_bsscale where (bs_type = ".$bs_type." and ptif_id = ".$ptif_id.")");
    echo "ok";
}


//****************************************************************************************************************
// BSスケールマスタ 保存
//****************************************************************************************************************
function save_mst_bsscale(){
    $bs_type = dbES($_REQUEST["bs_type"]);
    $ptif_id = dbES($_REQUEST["ptif_id"]);
    $try_commit = $_REQUEST["try_commit"];
    $field_count = (int)$_REQUEST["field_count"];
    $upper = @$_REQUEST["efmMstBSScale__".$field_count."__bs_upper"];
    if ($upper=="") $_REQUEST["efmMstBSScale__".$field_count."__bs_upper"] = "9999";
    $prev = -1;
    for ($idx=1; $idx<=$field_count; $idx++) {
        $id = "efmMstBSScale__".$idx."__";
        $upper = @$_REQUEST[$id."bs_upper"];
        if ($upper=="" && $idx<$field_count) { echo $idx."番目の測定値を指定してください。"; return; }
        if (!is_empty_or_number($upper)) { echo $idx."番目の測定値を半角数値で指定してください。"; return; }
        $upper = (int)$upper;
        if ($upper < 0) { echo $idx."番目の測定値に、マイナス値を指定できません。"; return; }
        if ($upper<=$prev) { echo $idx."番目の測定値を、前行の測定値より大きくしてください。"; return; }
        $prev = $upper;
    }
    dbExec("delete from sum_ks_mst_bsscale where bs_type = ".$bs_type." and ptif_id = ".$ptif_id);
    for ($idx=1; $idx<=$field_count; $idx++) {
        $id = "efmMstBSScale__".$idx."__";
        $sql =
        " insert into sum_ks_mst_bsscale ( bs_type, ptif_id, bs_upper, bs_name1, bs_ryou1, bs_name2, bs_ryou2 ) values (".
        " ".$bs_type.", ".$ptif_id.", ".((int)$_REQUEST[$id."bs_upper"]).", ".dbES($_REQUEST[$id."bs_name1"]).", ".dbES($_REQUEST[$id."bs_ryou1"]).
        ", ".dbES($_REQUEST[$id."bs_name2"]).", ".dbES($_REQUEST[$id."bs_ryou2"]).")";
        dbExec($sql);
    }
    echo "ok保存しました。";
}


//****************************************************************************************************************
// バイタルデータ保存
//****************************************************************************************************************
function save_vital_one_data(){
    $ptif_id = $_REQUEST["ptif_id"];
    $ymd = $_REQUEST["ymd"];
    $new_ymdhm = $ymd.$_REQUEST["hm"];

    if (!$_REQUEST["hm"]) { echo "測定時刻を指定してください。"; return; }

    if (!is_number($_REQUEST["taion1"]) && is_number($_REQUEST["taion2"])) { echo "体温を指定してください。"; return; }
    $taion = (is_number($_REQUEST["taion1"]) ? $_REQUEST["taion1"].".".(int)$_REQUEST["taion2"] : "");
    if ($taion!="" && ((int)$taion < 30 || (int)$taion>>45)) { echo "体温の値が範囲外です。"; return; }

    if (!is_empty_or_number($_REQUEST["myakuhakusu"])) { echo "脈拍数には半角数値を指定してください。"; return; }
    if (!is_empty_or_number($_REQUEST["ketsuatsu_upper"])) { echo "収縮期血圧には半角数値を指定してください。"; return; }
    if (!is_empty_or_number($_REQUEST["ketsuatsu_bottom"])) { echo "拡張時期血圧には半角数値を指定してください。"; return; }
    $kUp = $_REQUEST["ketsuatsu_upper"];
    $kBo = $_REQUEST["ketsuatsu_bottom"];
    if ($kUp!="" && $kBo!="" && (int)$kUp <= $kBo) {
        echo "収縮期血圧と拡張時血圧の値が不正です。"; return;
    }
    if (!is_empty_or_number($_REQUEST["kokyusu"])) { echo "呼吸数には半角数値を指定してください。"; return; }

    if (!is_empty_or_number($_REQUEST["hr_kaisu"])) { echo "HRには半角数値を指定してください。"; return; }
    if (!is_empty_or_number($_REQUEST["spo2_percent"])) { echo "SpO2には半角数値を指定してください。"; return; }
//    if (!is_empty_or_number($_REQUEST["taiju"])) { echo "体重には半角数値を指定してください。"; return; }

    if (
    	!$taion && !$_REQUEST["myakuhakusu"] && !$kUp && !$kBo && !$_REQUEST["kokyusu"] && !$_REQUEST["hr_kaisu"] &&
    	!$_REQUEST["spo2_percent"] && !$_REQUEST["taiju"]) {
		echo "データをひとつ以上入力してください。"; return;
	}

    $ymdhm24 = to_24hour($new_ymdhm);
    if ($ymdhm24 <> $new_ymdhm){
        if (dbGetOne("select ymdhm from sum_ks_vital where ptif_id = ".dbES($ptif_id)." and ymdhm = ".dbES($ymdhm24))) {
            echo "既に翌日の他時間帯で同時刻の登録が行われています。"; return;
        }
    }
    $ymdhm48 = to_48hour($new_ymdhm);
    if ($ymdhm48 <> $new_ymdhm){
        if (dbGetOne("select ymdhm from sum_ks_vital where ptif_id = ".dbES($ptif_id)." and ymdhm = ".dbES($ymdhm48))) {
            echo "既に前日の他時間帯で同時刻の登録が行われています。"; return;
        }
    }
    if ($_REQUEST["ymdhm"]<>$new_ymdhm) {
	    if (dbGetOne("select ymdhm from sum_ks_vital where ptif_id = ".dbES($ptif_id)." and ymdhm = ".dbES($new_ymdhm))) {
	        echo "既に同時刻の登録が行われています。"; return;
	    }
	}

    dbExec("delete from sum_ks_vital where ptif_id = ".dbES($ptif_id)." and ymdhm = ".dbES($_REQUEST["ymdhm"]));
    $sql =
    " insert into sum_ks_vital (".
    " ptif_id, ymdhm, taion, myakuhakusu, ketsuatsu_bottom, ketsuatsu_upper, kokyusu, myakuhakusu_x_marker".
    ",hr_kaisu, spo2_percent, taiju".
    ",update_emp_id, update_emp_name, update_timestamp".
    " ) values (".
    " ".dbES($ptif_id).
    ",".dbES($new_ymdhm).
    ",".dbES($taion).
    ",".dbES($_REQUEST["myakuhakusu"]).
    ",".dbES($_REQUEST["ketsuatsu_bottom"]).
    ",".dbES($_REQUEST["ketsuatsu_upper"]).
    ",".dbES($_REQUEST["kokyusu"]).
    ",".dbES($_REQUEST["myakuhakusu_x_marker"]).
    ",".dbES($_REQUEST["hr_kaisu"]).
    ",".dbES($_REQUEST["spo2_percent"]).
    ",".dbES($_REQUEST["taiju"]).
    ",".dbES(GG_LOGIN_EMP_ID).
    ",".dbES(GG_LOGIN_EMP_NAME).
    ",current_timestamp".
    " )";
    dbExec($sql);
    save_update_history($ptif_id, "vital", $ymd);
    echo "ok保存しました。"; return;
}


//****************************************************************************************************************
// 採用薬マスタ追加削除
//****************************************************************************************************************
function delete_vital_data(){
    dbExec("delete from sum_ks_vital where ptif_id = ".dbES($_REQUEST["ptif_id"])." and ymdhm = ".dbES($_REQUEST["ymdhm"]));
    save_update_history($_REQUEST["ptif_id"], "vital", substr($_REQUEST["ymdhm"], 0, 8), "1");
    echo "ok削除しました。"; return;
}


//****************************************************************************************************************
// 病棟・病室情報の取得
//****************************************************************************************************************
function get_mst_bldg_ward_ptrm() {
    //=================================================
    // 病棟一覧。患者の病棟がどこかは、どうでもよい。
    //=================================================
    $bldg_ward_options = array();
    $sel = dbFind("select * from sum_ks_mst_bldg_ward order by ward_name");
    while($row = pg_fetch_array($sel)){
        $bldg_ward_options[] = '<option value="'.$row["bldg_cd"]."_".$row["ward_cd"].'">'.hh($row["ward_name"]).'</option>';
    }
    //=================================================
    // 病棟に対する病室の一覧
    //=================================================
    $sql =
    " select bwm.bldg_cd, bwm.ward_cd, ptrm.ptrm_room_no, ptrm.ptrm_name, ptrm.bed_count from sum_ks_mst_bldg_ward bwm".
    " inner join sum_ks_mst_ptrm ptrm on (ptrm.bldg_cd = bwm.bldg_cd and ptrm.ward_cd = bwm.ward_cd)".
    " order by ptrm.ptrm_room_no";
    $sel = dbFind($sql);
    $ptrm_list = array();
    while($row = @pg_fetch_array($sel)){
        $ptrm_list[]=
        '{bldg_ward:"'.$row["bldg_cd"].'_'.$row["ward_cd"].'",ptrm:"'.$row["ptrm_room_no"].
        '",name:"'.js($row["ptrm_name"]).'",bed_count:"'.((int)$row["bed_count"]).'"}';
    }
    if (defined("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING")) {
        return array("bldg_ward_options"=>$bldg_ward_options, "ptrm_list"=>$ptrm_list);
    }

    echo 'ok{bldg_ward_options:"'.js(implode("",$bldg_ward_options)).'",ptrm_list:['.implode(",",$ptrm_list).']}';
    return;
}


//****************************************************************************************************************
// 薬剤その他情報 取得
//****************************************************************************************************************
function get_mst_options() {
    global $opt_groups;
    $sel = dbFind("select opt_cd, opt_name, opt_group, opt_alias, opt_comma_selection from sum_ks_mst_options order by opt_cd, opt_name");
    $out = array();
    while($row = @pg_fetch_array($sel)){
        $opt_disp = $row["opt_alias"];
        if ($opt_disp=="") $opt_disp = $row["opt_name"];
        $out[$row["opt_group"]][]=
        '{opt_cd:"'.js($row["opt_cd"]).'",opt_name:"'.js($row["opt_name"]).'",opt_alias:"'.js($row["opt_alias"]).
        '",opt_comma_selection:"'.js($row["opt_comma_selection"]).'",opt_disp:"'.js($opt_disp).'"}';
    }
    echo 'ok{';
    foreach ($opt_groups as $nm => $caption) {
        if ($nm!="gezai") echo ",";
        echo $nm.':['.@implode(",",$out[$nm]).']';
    }
    echo '}';
    return;
}


//****************************************************************************************************************
// BSスケールマスタ 取得
//****************************************************************************************************************
function get_mst_bsscale() {
    $ptif_id = $_REQUEST["ptif_id"];
    $sql =
    " select bs_type, ptif_id, bs_upper, bs_name1, bs_ryou1, bs_name2, bs_ryou2 from sum_ks_mst_bsscale".
    " where (ptif_id is null or ptif_id = '' or ptif_id = ".dbES($ptif_id).")".
    " order by bs_upper";
    $sel = dbFind($sql);
    $out = array();
    while($row = @pg_fetch_array($sel)){
        if ($row["bs_upper"]=="9999") $row["bs_upper"] = "";
        $out[$row["bs_type"]][] =
        '{bs_upper:"'.js($row["bs_upper"]).'",bs_name1:"'.js($row["bs_name1"]).'",bs_ryou1:"'.js($row["bs_ryou1"]).'"'.
        ',bs_name2:"'.js($row["bs_name2"]).'",bs_ryou2:"'.js($row["bs_ryou2"]).'"}';
    }
    echo 'ok{'.
        'BSSCALE_COMMON0:['.@implode(",",$out["BSSCALE_COMMON0"]).']'.
        ',BSSCALE_COMMON1:['.@implode(",",$out["BSSCALE_COMMON1"]).']'.
        ',BSSCALE_COMMON2:['.@implode(",",$out["BSSCALE_COMMON2"]).']'.
        ',BSSCALE_COMMON3:['.@implode(",",$out["BSSCALE_COMMON3"]).']'.
        ',BSSCALE_COMMON4:['.@implode(",",$out["BSSCALE_COMMON4"]).']'.
        ',PATIENT:['.@implode(",",$out["PATIENT"]).']'.
    '}';
    return;
}


//****************************************************************************************************************
// 患者所属情報の取得
//****************************************************************************************************************
function get_ptif_attribute() {
    $ptif_id = $_REQUEST["ptif_id"];

    //========================
    // 患者名・性別・年齢
    //========================
    $sql = "select ptif_lt_kana_nm, ptif_ft_kana_nm, ptif_lt_kaj_nm, ptif_ft_kaj_nm, ptif_birth, ptif_sex from ptifmst where ptif_id = ".dbES($ptif_id);
    $row1 = dbGetTopRow($sql);
    $tmp_birth = slash_ymd(@$row1["ptif_birth"]);

    //========================
    // 患者の病室、入院退院リスト
    //========================
    $sql =
      " select bw.ward_name, ptrm.ptrm_name, opa.bldg_cd, opa.ward_cd, opa.ptrm_room_no, opa.ptif_bikou, opa.dr_emp_id, opa.nyuin_taiin_list".
      " from sum_ks_mst_pat_attribute opa".
      " left outer join sum_ks_mst_bldg_ward bw on (bw.bldg_cd = opa.bldg_cd and bw.ward_cd = opa.ward_cd)".
      " left outer join sum_ks_mst_ptrm ptrm on (ptrm.bldg_cd = opa.bldg_cd and ptrm.ward_cd = opa.ward_cd and ptrm.ptrm_room_no = opa.ptrm_room_no)".
      " where opa.ptif_id = ". dbES($ptif_id);
    $row2 = dbGetTopRow($sql);

    //========================
    // 患者の主治医
    //========================
    $sql = "select emp.emp_lt_nm || ' ' || emp.emp_ft_nm from empmst emp where emp_id = ".dbES($row2["dr_emp_id"]);
    $dr_emp_name = dbGetOne($sql);

    $bldg_ward = @$row2["bldg_cd"].'_'.@$row2["ward_cd"];
    if ($bldg_ward=="_") $bldg_ward = "";
    $info = array(
        "ptif_id" => $ptif_id,
        "ptif_name" => trim(@$row1["ptif_lt_kaj_nm"] . " " . @$row1["ptif_ft_kaj_nm"]),
        "ptif_kana_name" => trim(@$row1["ptif_lt_kana_nm"] . " " . @$row1["ptif_ft_kana_nm"]),
        "ptif_age" => ($tmp_birth ? (date("Y")-substr($tmp_birth,0,4)) - (date("md")<substr($tmp_birth,5,2) ? 1 : 0) . "歳" : ""),
        "ptif_gender" => mb_substr("−男女",(int)$row1["ptif_sex"], 1),
        "ptrm_name" => (strlen(@$row2["ptrm_name"]) ? js(@$row2["ptrm_name"]) : "(病室未指定)"),
        "ward_name" => $row2["ward_name"],
        "bldg_ward" => $bldg_ward,
        "dr_emp_name" => $dr_emp_name,
        "dr_emp_id" => @$row2["dr_emp_id"],
        "ptrm_room_no" => @$row2["ptrm_room_no"],
        "ptif_bikou" => @$row2["ptif_bikou"],
        "nyuin_taiin_list" => @$row2["nyuin_taiin_list"] // nyuin-taiin,nyuin-taiin, ... の文字列
    );
    if (defined("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING")) return $info;
    echo "ok".to_json($info);
}


//****************************************************************************************************************
// 患者附帯情報 保存
//****************************************************************************************************************
function save_ptif_attribute() {
    $ptif_id = $_REQUEST["ptif_id"];
    $nyuin_taiin_list = $_REQUEST["nyuin_taiin_list"];
    $ary_nyuin_taiin_list = explode(",", $nyuin_taiin_list);
    $ptrm_room_no = $_REQUEST["ptrm_room_no"];
    $ptif_bikou = $_REQUEST["ptif_bikou"];
    $dr_emp_id = $_REQUEST["dr_emp_id"];
    list($bldg_cd, $ward_cd) = explode("_", $_REQUEST["bldg_ward"]);
    if (dbGetOne("select count(*) from sum_ks_mst_pat_attribute where ptif_id = ".dbES($ptif_id))) {
        $sql =
        " update sum_ks_mst_pat_attribute set".
        " bldg_cd = ".dbEI($bldg_cd).
        ",ward_cd = ".dbEI($ward_cd).
        ",ptrm_room_no = ".dbEI($ptrm_room_no).
        ",ptif_bikou = ".dbES($ptif_bikou).
        ",dr_emp_id = ".dbES($dr_emp_id).
        ",nyuin_taiin_list = ".dbES($nyuin_taiin_list).
        " where ptif_id = ".dbES($ptif_id);
    } else {
        $sql =
        "insert into sum_ks_mst_pat_attribute ( ptif_id, bldg_cd, ward_cd, ptrm_room_no, ptif_bikou, dr_emp_id, nyuin_taiin_list ) values (".
        " ".dbES($ptif_id).",".dbEI($bldg_cd).",".dbEI($ward_cd).",".dbEI($ptrm_room_no).",".dbES($ptif_bikou).",".dbES($dr_emp_id).",".dbES($nyuin_taiin_list).")";
    }
    dbExec($sql);

    dbExec("delete from sum_ks_mst_pat_nyuin_taiin where ptif_id = ".dbES($ptif_id));
    foreach ($ary_nyuin_taiin_list as $nt) { // nyuin-taiin,nyuin-taiin, ... の文字列
        $nt_ary = explode("-", $nt);
        $sql =
        " insert into sum_ks_mst_pat_nyuin_taiin (seq, ptif_id, nyuin_ymd, taiin_ymd) values (".
        " nextval('sum_ks_mst_pat_nyuin_taiin_seq')".
        ",".dbES($ptif_id).",".dbES($nt_ary[0]).",".dbES($nt_ary[1]).")";
        dbExec($sql);
    }
    echo "ok";
}


//****************************************************************************************************************
// 指定日バイタルデータの登録日時リスト取得
//****************************************************************************************************************
function get_vital_per_day_list() {
    $ptif_id = $_REQUEST["ptif_id"];
    $ymd = $_REQUEST["ymd"];
    $yesterday_ymd = get_next_ymd($ymd,"-");
    $sql =
    " select * from sum_ks_vital".
    " where ptif_id = ".dbES($ptif_id).
    " and ymdhm >= '".$yesterday_ymd."2400'".
    " and ymdhm <= '".$ymd."9999' order by ymdhm";
    $sel = dbFind($sql);
    $ret = array();
    while ($row = @pg_fetch_array($sel)) {
        $a_ymd = ymdhm_to_array($row["ymdhm"]);
        $jp = "";
        if ($a_ymd["zhh"]>=24) {
            if ($a_ymd["ymd"]==$ymd) $jp = "(翌日)";
            if ($a_ymd["ymd"]==$yesterday_ymd) {
                $jp = "(前日入力)&nbsp;";
                $a_ymd["zhh"] = $a_ymd["zhh"] - 24;
            }
        }

        $ret[] = '{ymd:"'.$a_ymd["ymd"].'", ymdhm:"'.$row["ymdhm"].'", ymdhm_jp:"'.$jp.$a_ymd["zhh"]."時".$a_ymd["mi"]."分".'"}';
    }
    echo "ok[".implode(",", $ret)."]";
}


//****************************************************************************************************************
// 期間系（離床理由・酸素・抗生剤）のリスト取得、指定日にまたがるものを取得
//****************************************************************************************************************
function get_care_kikan_per_day_list() {
    $ptif_id = dbES($_REQUEST["ptif_id"]);
    $ymd = (int)$_REQUEST["ymd"];
    $care_content_list = explode(",", $_REQUEST["care_content_list"]);
    $ret = array();
    foreach ($care_content_list as $care_content) {
        if (!$care_content) continue;
        $sql =
        " select ymdhm_fr, ymdhm_to, serial_data from sum_ks_care_kikan".
        " where ptif_id = ".$ptif_id.
        " and ymdhm_fr <= '".$ymd."9999'".
        " and (ymdhm_to >= '".$ymd."0000' or ymdhm_to = '')".
        " and care_content = ".dbES($care_content).
        " order by ymdhm_fr";
        $sel = dbFind($sql);
        $field_count = 0;
        while ($raw_row = @pg_fetch_array($sel)) {
            $field_count++;
            $prefix = $care_content."__".$field_count."__";
            $data = unserialize($raw_row["serial_data"]);
            if (is_array($data)) {
                foreach ($data as $k => $d) $ret[$prefix.$k] = $d;
            }
            $ret[$prefix."ymdhm_fr"] = $raw_row["ymdhm_fr"];
            $ret[$prefix."ymdhm_to"] = $raw_row["ymdhm_to"];
            $a_ymd_fr = ymdhm_to_array($ret[$prefix."ymdhm_fr"]);
            $a_ymd_to = ymdhm_to_array($ret[$prefix."ymdhm_to"]);
            $ret[$prefix."fr_ymd"] = $a_ymd_fr["ymd"];
            $ret[$prefix."fr_hm"] = $a_ymd_fr["hm"];
            $ret[$prefix."to_ymd"] = $a_ymd_to["ymd"];
            $ret[$prefix."to_hm"] = $a_ymd_to["hm"];
        }
        $ret[$care_content."__field_count"] = $field_count;
    }
    $ret["ptif_name_disp"] = get_ptif_name($_REQUEST["ptif_id"]);
    echo "ok".to_json($ret);
}


//****************************************************************************************************************
// バイタル 該当データ取得
//****************************************************************************************************************
function get_vital_one_data() {
    $ptif_id = dbES($_REQUEST["ptif_id"]);
    $ymdhm = dbES($_REQUEST["ymdhm"]);
    $row = dbGetTopRow(
        " select *, to_char(update_timestamp, 'YYYY/MM/DD HH:MI:SS') as dlg_display_time".
        " from sum_ks_vital where ptif_id = ".$ptif_id." and ymdhm = ".$ymdhm
    );
    if ($row) {
        $a_ymd = ymdhm_to_array($row["ymdhm"]);
        $row["hm"] = $a_ymd["hm"];
        $aTaion = explode(".",$row["taion"].".");
        $row["taion1"] = $aTaion[0];
        $row["taion2"] = $aTaion[1];
        $row["dlg_update_emp_id"] = $row["update_emp_id"];
        $row["dlg_update_emp_name"] = $row["update_emp_name"];
    }
    echo "ok".to_json($row);
}


//****************************************************************************************************************
// 離床理由 該当データ取得
//****************************************************************************************************************
function get_care_kikan_one_data() {
    $ptif_id = dbES($_REQUEST["ptif_id"]);
    $ymdhm_fr = $_REQUEST["ymdhm_fr"];
    $care_content = $_REQUEST["care_content"];
    $sql = "select * from sum_ks_care_kikan where ptif_id = ".$ptif_id." and ymdhm_fr = ".dbES($ymdhm_fr)." and care_content = ".dbES($care_content);
    $raw_row = dbGetTopRow($sql);
    $row = array();
    $prefix = $care_content."__1__";

    $data = unserialize($raw_row["serial_data"]);
    if (is_array($data)) {
        foreach ($data as $k => $d) $row[$prefix.$k] = $d;
    }
    $row[$prefix."ymdhm_fr"] = $raw_row["ymdhm_fr"];
    $row[$prefix."ymdhm_to"] = $raw_row["ymdhm_to"];

    $a_ymd_fr = ymdhm_to_array($row[$prefix."ymdhm_fr"]);
    $a_ymd_to = ymdhm_to_array($row[$prefix."ymdhm_to"]);
    $row[$prefix."fr_ymd"] = $a_ymd_fr["ymd"];
    $row[$prefix."fr_hm"] = $a_ymd_fr["hm"];
    $row[$prefix."to_ymd"] = $a_ymd_to["ymd"];
    $row[$prefix."to_hm"] = $a_ymd_to["hm"];
    echo "ok".to_json($row);
}


//****************************************************************************************************************
// 詳細入力画面用の指示データ 前回記号のもの
//****************************************************************************************************************
function get_siji_ymd_zenkai_az() {
    $ptif_id = $_REQUEST["ptif_id"];
    $ymd = $_REQUEST["ymd"];
    $az = $_REQUEST["az"];
    $sql =
    " select ymd, serial_data_chusya from sum_ks_siji_ymd".
    " where ptif_id = ".dbES($ptif_id)." and ymd < ".dbES($ymd)." and chusya_az_list like ".dbES("%\t".$az."\t%")." order by ymd desc limit 1";
    $row = dbGetTopRow($sql);
    $sdata = unserialize($row["serial_data_chusya"]);
    $ret = array();
    $field_count = (int)$sdata["chusya__field_count"];
    for ($fnum = 1; $fnum<=$field_count; $fnum++) {
        if (get_alias($sdata["chusya__".$fnum."__siji_az"])!=$az) continue;
        $a_ymd = ymdhm_to_array($row["ymd"]);
        $ret = array(
            "ymd"=>$row["ymd"],
            "slash_mdw"=>$a_ymd["slash_mdw"],
            "is_continue"=>$sdata["chusya__".$fnum."__is_continue"],
            "is_sumi"=>$sdata["chusya__".$fnum."__is_sumi"],
            "is_stop"=>$sdata["chusya__".$fnum."__is_stop"],
            "siji_az"=>$sdata["chusya__".$fnum."__siji_az"],
            "siji_text"=>$sdata["chusya__".$fnum."__siji_text"],
            "siji_picture_names"=>$sdata["chusya__".$fnum."__siji_picture_names"],
            "siji_picture_area_s_html" => create_picture_img_tag($ptif_id, $sdata["chusya__".$fnum."__siji_picture_names"], "s")
        );
    }
    echo "ok".to_json($ret);
}


//****************************************************************************************************************
// 詳細入力画面用の指示データ 取得
//****************************************************************************************************************
function get_siji_ymd() {
    $ptif_id = $_REQUEST["ptif_id"];
    $ymd = $_REQUEST["ymd"];
    $section = $_REQUEST["section"];
    $is_get_last_siji = $_REQUEST["is_get_last_siji"];
    $data = array();
    if ($is_get_last_siji) {
        $sql =
        " select ymd from sum_ks_siji_ymd".
        " where ptif_id = ".dbES($ptif_id)." and ymd <= ".dbES($ymd).
        " and serial_data_".$section." like 'a:%'".
        " order by ymd desc limit 1";
        $ymd = dbGetOne($sql);
    }
    $sql = "select serial_data_".$section." from sum_ks_siji_ymd where ptif_id = ".dbES($ptif_id)." and ymd = ".dbES($ymd);
    $serial_data = dbGetOne($sql);
    if ($serial_data) {
        $data = unserialize($serial_data);
        $data["siji_ymd"] = $ymd;
    }
    if ($section=="chusya") {
        $field_count = (int)$data["chusya__field_count"];
        for ($idx=1; $idx<=$field_count; $idx++) {
            $data["chusya__".$idx."__picture_area_s_html"] = create_picture_img_tag($ptif_id, $data["chusya__".$idx."__siji_picture_names"], "s");
        }
    }
    echo "ok".to_json($data);
}


//****************************************************************************************************************
// 詳細入力画面用の指示データ 取得
//****************************************************************************************************************
function get_siji_ymd_last() {
    $ptif_id = $_REQUEST["ptif_id"];
    $ymd = dbES($_REQUEST["ymd"]);
    $section = $_REQUEST["section"];
    $data = array();
    $serial_data = dbGetOne("select serial_data_".$section." from sum_ks_siji_ymd where ptif_id = ".dbES($ptif_id)." and ymd = ".$ymd);
    if ($serial_data) $data = unserialize($serial_data);

    if ($section=="chusya") {
        $field_count = (int)$data["chusya__field_count"];
        for ($idx=1; $idx<=$field_count; $idx++) {
            $data["chusya__".$idx."__picture_area_s_html"] = create_picture_img_tag($ptif_id, $data["chusya__".$idx."__siji_picture_names"], "s");
        }
    }
    echo "ok".to_json($data);
}


//****************************************************************************************************************
// 詳細入力画面用の指示データ 取得
//****************************************************************************************************************
function get_siji_ymd_one() {
    $ptif_id = $_REQUEST["ptif_id"];
    $ymd = dbES($_REQUEST["ymd"]);
    $a_ymd = ymdhm_to_array($_REQUEST["ymd"]);
    $section = $_REQUEST["section"];
    $field_number  = (int)$_REQUEST["field_number"];
    $data = array();
    $serial_data = dbGetOne("select serial_data_".$section." from sum_ks_siji_ymd where ptif_id = ".dbES($ptif_id)." and ymd = ".$ymd);
    if ($serial_data) $data = unserialize($serial_data);

    $out = array();
    $prefix = $section."__".$field_number."__";
    foreach($data as $key => $v) {
        if (strpos($key, $prefix)!==0) continue;
        $short_key_name = substr($key, strlen($prefix));
        $out[$short_key_name] = $v;
        if ($short_key_name=="siji_picture_names") $out["picture_area_s_html"] = create_picture_img_tag($ptif_id, $v, "s");
    }
    $out["sijibi_mm"] = $a_ymd["zmm"];
    $out["sijibi_dd"] = $a_ymd["zdd"];
    $out["slash_mdw"] = $a_ymd["slash_mdw"];
    $out["ptif_name"] = get_ptif_name($ptif_id);

    echo "ok".to_json($out);
}


//****************************************************************************************************************
// 詳細入力画面用のケアデータ 取得
//****************************************************************************************************************
function get_care_ymd() {
    $ptif_id = dbES($_REQUEST["ptif_id"]);
    $ymd = dbES($_REQUEST["ymd"]);
    $section = $_REQUEST["section"];
    $load_masquerade_last_data = $_REQUEST["load_masquerade_last_data"];
    if ($load_masquerade_last_data!="force") {
        $sql = "select ymd, update_emp_name, update_timestamp, serial_data_".$section." from sum_ks_care_ymd where ptif_id = ".$ptif_id." and ymd = ".$ymd;
        $row = dbGetTopRow($sql);
    }
    if ($load_masquerade_last_data=="force" || ($load_masquerade_last_data=="if_not" && !$row)) {
        $sql =
        " select ymd, update_emp_name, update_timestamp, serial_data_".$section.
        " from sum_ks_care_ymd where ptif_id = ".$ptif_id." and ymd < ".$ymd.
        " and serial_data_".$section." like 'a:%'".
        " order by ymd desc limit 1";
        $row = dbGetTopRow($sql);
    }
    $serial_data = @$row["serial_data_".$section];
    if ($serial_data) {
        $data = unserialize($serial_data);
        foreach ($data as $k=>$v) @$row[$k] = $v;
    }
    echo "ok".to_json(@$row);
}


