<?
if (!defined("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING")) { // 印刷時は既にdefineされている。この中へは入らない。
    define("IS_AJAX_REQUEST", 1);
    require_once("ks_common.php");
    ob_clean();

    $ajax_action = $_REQUEST["ajax_action"];
    if ($ajax_action=="get_ondoban_list") {
        get_ondoban_list(
            $_REQUEST["ptif_id"], $_REQUEST["ymd"], $_REQUEST["single_ymd"], $_REQUEST["sticky_menubar"],
            $_REQUEST["sticky_sijibar"], $_REQUEST["is_not_pc"], $_REQUEST["day_width"]
        );
    }
    else echo "ajax_action=".$ajax_action; // ajax_action該当なし。プログラム実装エラーである
    pg_close($con);
    die;
}

//****************************************************************************************************************
// 温度板コンテンツ排出
//****************************************************************************************************************
function get_ondoban_list($ptif_id, $ymd, $single_ymd, $stickyMenubar, $stickySijibar, $is_not_pc, $dayWidth=100) {
    if($stickyMenubar && !defined("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING")) {
        echo '<div style="height:56px; width:100%; border-bottom:1px solid #9bc8ec;';
        echo ' position:fixed; left:0; top:0; background-color:#d7f0ff; z-index:90">&nbsp;</div>';
        echo '<div style="height:9px">&nbsp;</div>'; // ★★★開発都合により先にマージン出力。デバッグで途中停止させても、ダンプが見えるように。
    }
    $WYL = create_week_ymd_list(@$ymd); // Weekly Ymd List
    $monday_ymd = $WYL[0];
    if ($single_ymd) $WYL = array($single_ymd);
    $day_size = count($WYL);
    $lastday_ymd = $WYL[$day_size-1];

    $vitalPosData = array();

    $tridx = 0;
    $ww1 = ' style="width:'.((int)$dayWidth-1).'px"';
    $ww2 = ' style="width:'.((int)($dayWidth / 2)-1).'px"';
    $ww3 = ' style="width:'.((int)($dayWidth / 3)-1).'px"';
    $ww4 = ' style="width:'.((int)($dayWidth / 4)-1).'px"';
    $ww015= ' style="width:'.((int)($dayWidth / 100 * 15)-1).'px"';
    $ww02 = ' style="width:'.((int)($dayWidth / 10 * 2)-1).'px"';
    $ww03 = ' style="width:'.((int)($dayWidth / 10 * 3)-1).'px"';
    $ww035 = ' style="width:'.((int)($dayWidth / 100 * 35)-1).'px"';
    $ww04 = ' style="width:'.((int)($dayWidth / 10 * 4)-1).'px"';
    $ww06 = ' style="width:'.((int)($dayWidth / 10 * 6)-1).'px"';
    $ww07 = ' style="width:'.((int)($dayWidth / 10 * 7)-1).'px"';
    $ww08 = ' style="width:'.((int)($dayWidth / 10 * 8)-1).'px"';

    //=================================================
    // 対象日付リストを作成する。日曜日を右端にして七日間。
    // $ymd_indexには "[1](６日前)" 〜 "[7](指定日)" まで格納
    //=================================================
    $ymd_index = array();
    for ($i = 1; $i <= $day_size; $i++){
        $ymd_index[$WYL[$i-1]] = $i;
    }
    $ymd_after1 = get_next_ymd($WYL[$day_size-1]);
    $ymd_after2 = get_next_ymd($ymd_after1);
    $ymd_before1 = get_next_ymd($WYL[0], "-");
    $ymd_before2 = get_next_ymd($ymd_before1, "-");
    $ymd_index[$ymd_before1] = 0;
    $ymd_index[$ymd_before2] = -1;
    $ymd_index[$ymd_after1] = $day_size+1;
    $ymd_index[$ymd_after2] = $day_size+2;
    $tooltipData = array();

    //==============================================================================
    // 入院退院状態
    //==============================================================================
    $nyuin_taiin_list = dbGetOne("select nyuin_taiin_list from sum_ks_mst_pat_attribute where ptif_id = ".dbES($ptif_id));
    $taiin_classname = array("_taiin","_taiin","_taiin","_taiin","_taiin","_taiin","_taiin");
    if ($nyuin_taiin_list) {
        $ntlist = explode(",", $nyuin_taiin_list);
        foreach ($ntlist as $ntjoint) {
            $nt = explode("-", $ntjoint);
            if ($nt[0] > $WYL[$day_size-1]) continue; // 入院日が週末以降。コンティニュー
            if ($nt[1]!="" && $nt[1] < $WYL[0]) continue; // 退院日があるが週頭以前。コンティニュー
            for ($i=0; $i<=$day_size-1; $i++) {
                if ($nt[0] > $WYL[$i]) continue; // 入院日が指定日以降。コンティニュー
                if ($nt[1]!="" && $nt[1] < $WYL[$i]) continue; // 退院日が指定日以前。コンティニュー
                $taiin_classname[$i]=""; // 対象日は入院であることが判明。退院クラス名を削除
            }
        }
    }

    //==============================================================================
    // 離床理由・酸素・抗生剤など期間系グラフ情報の収集
    //==============================================================================
    $sql =
    " select ymdhm_fr, ymdhm_to, serial_data, care_content from sum_ks_care_kikan".
    " where ptif_id = ".dbES($ptif_id).
    " and ymdhm_fr <= ".dbES($WYL[$day_size-1]."9999"). // スケジュールの開始日は、画面最右列の日付けよりは過去
    " and (ymdhm_to = '' or ymdhm_to >= ".dbES($WYL[0]."0000").")". // 終了日は、画面最左列の日付けよりは未来か、未設定永続中
    " order by ymdhm_fr desc";
    $risyo_riyu_db_data = dbGetAll($sql);
    $graphL = array(); // 離床理由
    $graphK = array(); // 抗生剤
    $graphSK = array(); // 酸素吸入
    $next_fr = array();
    foreach($risyo_riyu_db_data as $idx => $row){
        $a_ymd1 = ymdhm_to_array(to_24hour($row["ymdhm_fr"]));
        $a_ymd1_48 = ymdhm_to_array($row["ymdhm_fr"]);
        $day_index = @$ymd_index[$a_ymd1["ymd"]];
        $hour =   (int)trim($a_ymd1["hh"]); // 測定時
        $minute = (int)trim($a_ymd1["mi"]); // 測定分
        $graph_day_fr = ((int)$day_index-1)*24*60 + $hour*60 + $minute; // グラフ上の横位置（分単位）
        $is_fr_overflow = "";
        if ($a_ymd1["ymd"]<$WYL[0]) { $graph_day_fr = 0; $is_fr_overflow = "1"; }

        $a_ymd2 = ymdhm_to_array(to_24hour($row["ymdhm_to"]));
        $a_ymd2_48 = ymdhm_to_array($row["ymdhm_to"]);
        $day_index = @$ymd_index[$a_ymd2["ymd"]];
        $hour =   (int)trim($a_ymd2["hh"]); // 測定時
        $minute = (int)trim($a_ymd2["mi"]); // 測定分
        $graph_day_to = ((int)$day_index-1)*24*60 + $hour*60 + $minute; // グラフ上の横位置（分単位）
        $is_to_overflow = "";
        if ($a_ymd2["ymd"]>$WYL[$day_size-1] || !$a_ymd2["ymd"]) { $graph_day_to = ($day_size-1)*24*60 + 24*60; $is_to_overflow = "1"; }

        $data = unserialize($row["serial_data"]);

        //--------------------------
        // 離床理由（L）
        //--------------------------
        if ($row["care_content"]=="risyo_riyu") {
            $risyo_riyu = mb_ereg_replace("\t", "", mb_ereg_replace("\n", "<br>", hh(get_alias($data["disp2"]))));
            if (!$risyo_riyu) $risyo_riyu = mb_ereg_replace("\t", "", mb_ereg_replace("\n", "<br>", hh(get_alias($data["disp1"]))));
            array_unshift($graphL, $graph_day_fr."\t".$graph_day_to."\t".$risyo_riyu);
        }
        //--------------------------
        // 抗生剤（K）
        //--------------------------
        if ($row["care_content"]=="kouseizai") {
            array_unshift($graphK, $graph_day_fr."\t".$graph_day_to."\t".$is_fr_overflow."\t".$is_to_overflow."\t".$data["siji_az"]);
        }
        //--------------------------
        // 酸素吸入 or 人工呼吸器（SK）
        //--------------------------
        if ($row["care_content"]=="sanso") {
            if ($graph_day_to > $next_fr["sanso"] && $next_fr["sanso"]) $grahp_day_to = $next_fr["sanso"];
            $label = $a_ymd1_48["zhhmi"]." ".get_alias($data["kigu"])." ".$data["litter"];
            array_unshift($graphSK, $graph_day_fr."\t".$graph_day_to."\t".$is_fr_overflow."\t".$is_to_overflow."\tsanso\t".$label);
            $next_fr["sanso"] = $graph_day_fr;
        }
        if ($row["care_content"]=="kokyuki") {
            $noudo = $data["noudo"].get_alias($data["noudo_unit"]);
            if ($graph_day_to > $next_fr["kokyuki"] && $next_fr["kokyuki"]) $grahp_day_to = $next_fr["kokyuki"];
            $label = $a_ymd1_48["zhhmi"]." ".$noudo;
            array_unshift($graphSK, $graph_day_fr."\t".$graph_day_to."\t".$is_fr_overflow."\t".$is_to_overflow."\tkokyuki\t".$label);
            $next_fr["kokyuki"] = $graph_day_fr;
        }
    }

    //==============================================================================
    // 単日ケア情報の収集
    //==============================================================================
    $care_list = array();

    $sql =
    " select * from sum_ks_care_ymd".
    " where ptif_id = ".dbES($ptif_id).
    " and ymd >= '".$WYL[0]."' and ymd <= '".$WYL[$day_size-1]."'";
    $sel = dbFind($sql);
    $weekly_insulin_header = array("","",""); // ケアが存在したときに、添え字0:定期、1=BS測定値、2=食事量 が入る
    $weekly_ben_ari = 0;
    $weekly_kansatu_header = array();
    $haieki2_exist = 0;
    $haieki3_exist = 0;
    while ($row = @pg_fetch_array($sel)) {

        $ymd = $row["ymd"];

        $data_in = unserialize($row["serial_data_in"]);
        $data_hoeki = unserialize($row["serial_data_hoeki"]);
        $data_insulin = unserialize($row["serial_data_insulin"]);
        $data_out = unserialize($row["serial_data_out"]);
        $data_kensanado = unserialize($row["serial_data_kensanado"]);
        $data_kansatu = unserialize($row["serial_data_kansatu"]);
        $data_sign = unserialize($row["serial_data_sign"]);
        //--------------------------
        // 経口
        //--------------------------
        $ary = array("asa", "hiru", "yu");
        foreach ($ary as $ahy) {
            $fuku = $data_in["keikou_".$ahy."_fukusyoku"];
            if ($fuku=="10") $fuku = "全";
            else if ($fuku!="") $fuku .= "";
            $syu = $data_in["keikou_".$ahy."_syusyoku"];
            if ($syu=="10") $syu = "全";
            else if ($syu!="") $syu .= "";
            if ($data_in["keikou_".$ahy."_zessyoku"]) { $fuku = "絶食"; $syu = "―"; }
            $care_list["keikou_".$ahy."_fukusyoku"][$ymd] = $fuku;
            $care_list["keikou_".$ahy."_syusyoku"][$ymd] = $syu;
        }
        //--------------------------
        // 経管
        //--------------------------
        $ary = array("asa", "hiru", "yu", "ryou4", "ryou5", "ryou6");
        $keikan_field_count = (int)$data_in["keikan__field_count"];
        $true_keikan_field_count = 0;
        for ($idx=1; $idx<=$keikan_field_count; $idx++) {
            $is_exist = 0;
            foreach ($ary as $ahy) {
                $key = "keikan__".$idx."__".$ahy."_ml";
                if ($data_in[$key]!="") { $is_exist = 1; break; }
                if ($data_in["keikan__".$idx."__".$ahy."_zessyoku"]) { $is_exist = 1; break; }
            }
            if (!$is_exist) continue;
            $true_keikan_field_count++;
            $eiyouzai = $data_in["keikan__".$idx."__eiyouzai"];
            if ($eiyouzai=="") $eiyouzai = "(薬剤指定なし)";
            $care_list["keikan__".$true_keikan_field_count."__eiyouzai"][$ymd] = $eiyouzai;
            foreach ($ary as $ahy) {
                $key = "keikan__".$idx."__".$ahy."_ml";
                $newkey = "keikan__".$true_keikan_field_count."__".$ahy."_ml";
                if ($data_in[$key]!="") $care_list[$newkey][$ymd] = $data_in[$key]."mL";
                if ($data_in["keikan__".$idx."__".$ahy."_zessyoku"]) $care_list[$newkey][$ymd] = "絶食";
            }
        }
        $care_list["keikan__field_count"][$ymd] = $true_keikan_field_count;
        //--------------------------
        // 補食
        //--------------------------
        $hosyoku_field_count = $data_in["hosyoku__field_count"];
        if (!$hosyoku_field_count) {
            if ($data_in["hosyoku_timing"] || $data_in["hosyoku"] || $data_in["hosyoku_ryou"]) {
                $hosyoku_field_count = 1;
                $data_in["hosyoku__1__hosyoku_timing"][$ymd] = $data_in["hosyoku_timing"];
                $data_in["hosyoku__1__hosyoku"][$ymd] = $data_in["hosyoku"];
                $data_in["hosyoku__1__hosyoku_ryou"][$ymd] = $data_in["hosyoku_ryou"];
            }
        }
        $care_list["hosyoku__field_count"][$ymd] = $hosyoku_field_count;
        for ($idx=1; $idx<=$hosyoku_field_count; $idx++) {
            $care_list["hosyoku__".$idx."__hosyoku_timing"][$ymd] = $data_in["hosyoku__".$idx."__hosyoku_timing"];
            $care_list["hosyoku__".$idx."__hosyoku"][$ymd] = $data_in["hosyoku__".$idx."__hosyoku"];
            $care_list["hosyoku__".$idx."__hosyoku_ryou"][$ymd] = $data_in["hosyoku__".$idx."__hosyoku_ryou"];
        }
        //--------------------------
        // 水分摂取量
        //--------------------------
        for ($idx=1; $idx<=5; $idx++) {
            $key = "suibun_ryou_".$idx;
            $suibun_unit = explode("_", $data_in["suibun_unit"]);
            $kk = "";
            if (strpos($suibun_unit[0],"経口")===0) $kk = "口";
            if (strpos($suibun_unit[0],"経管")===0) $kk = "管";
            $care_list[$key][$ymd] = ($data_in[$key]!="" ? $kk.$data_in[$key].@$suibun_unit[1] : "");
        }
        //--------------------------
        // 補液
        //--------------------------
        if ($data_hoeki) {
            $field_count = $data_hoeki["hoeki__field_count"];
            $empty_count = 0;
            for ($fidx=1; $fidx<=$field_count; $fidx++) {
                $oaz = $data_hoeki["hoeki__".$fidx."__hoeki_az"];
                $az = $oaz;
                if ($az=="") $az = "？";
                $child_count = $data_hoeki["hoeki__".$fidx."__field_count"];
                for ($cidx=1; $cidx<=$child_count; $cidx++) {
                    $key = "hoeki__".$fidx."__child".$cidx."__hoeki_";
                    $hm = $data_hoeki[$key."hm"];
                    if (strlen($hm)==4) $hm = substr($hm,0,2).":".substr($hm,2);
                    $koushin = $data_hoeki[$key."koushin"];
                    $zan = $data_hoeki[$key."zan"];
                    $ryusoku = $data_hoeki[$key."ryusoku"];
                    $is_empty = 0;
                    if ($oaz=="" && $hm=="" && $koushin=="" && $zan=="" && $ryusoku=="") $is_empty = 1;
                    if ($is_empty) $empty_count++;
                    if (!$is_empty) {
                        $care_list["hoeki__data"][$ymd][] = array("az" =>$az, "hm"=>$hm, "data"=>($koushin ? "更新" : $zan."/".$ryusoku));
                    }
                    $az = "";
                }
            }
        }
        //--------------------------
        // インスリン 定期
        //--------------------------
        $care_list["insulin_teiki_ari"][$ymd] = $data_insulin["insulin_teiki_ari"];
        $care_list["insulin_bs_ari"][$ymd] = $data_insulin["insulin_bs_ari"];
        $care_list["insulin_sr_ari"][$ymd] = $data_insulin["insulin_sr_ari"];
        if ($data_insulin["insulin_teiki_ari"]) {
            $weekly_insulin_header[0] = "定期";
            $tmp = array();
            if ($data_insulin["insulin_teiki_asa"]) $tmp[]= "朝";
            if ($data_insulin["insulin_teiki_hiru"]) $tmp[]= "昼";
            if ($data_insulin["insulin_teiki_yu"]) $tmp[]= "夕";
            for ($idx=4; $idx<=7; $idx++) {
                if ($data_insulin["insulin_teiki_check".$idx]) $tmp[]= $data_insulin["insulin_teiki_when".$idx];
            }
            $care_list["insulin_teiki"][$ymd] = implode("/", $tmp);
        }
        //--------------------------
        // インスリン BS測定値
        //--------------------------
        if ($data_insulin["insulin_bs_ari"]) {
            $weekly_insulin_header[1] = "BS測定値";
            $field_count = $data_insulin["bsscale__field_count"];
            for ($idx=1; $idx<=$field_count; $idx++) {
                $key = "bsscale__".$idx."__";
                $hm = $data_insulin[$key."bs_hm"];
                if (strlen($hm)==4) $hm = substr($hm,0,2).":".substr($hm,2);
                $care_list["insulin_bs"][$ymd][] = array(
                    "bs_hm"=>$hm,
                    "bs_upper"=>$data_insulin[$key."bs_upper"],
                    "bs_name1"=>$data_insulin[$key."bs_name1"],
                    "bs_ryou1"=>$data_insulin[$key."bs_ryou1"],
                    "bs_name2"=>$data_insulin[$key."bs_name2"],
                    "bs_ryou2"=>$data_insulin[$key."bs_ryou2"]
                );
            }
        }
        //--------------------------
        // インスリン 食事量
        //--------------------------
        if ($data_insulin["insulin_sr_ari"]) {
            $weekly_insulin_header[2] = "食事量";
            $care_list["insulin_sr_asa"][$ymd] = $data_insulin["insulin_sr_asa"];
            $care_list["insulin_sr_hiru"][$ymd] = $data_insulin["insulin_sr_hiru"];
            $care_list["insulin_sr_yu"][$ymd] = $data_insulin["insulin_sr_yu"];
        }
        //--------------------------
        // 便 性状
        //--------------------------
        if ($data_out) {
            $field_count = (int)@$data_out["ben__field_count"];
            $ben_nasi = @$data_out["ben_nasi"];
            if ($field_count || $ben_nasi) $weekly_ben_ari = 1;
            $care_list["ben"][$ymd] = array();
            $ttl_ryou = "";
            if ($ben_nasi) {
                $care_list["ben"][$ymd][] = array("ben_nasi"=>1);
                $care_list["ben__ttl_ryou"][$ymd] = "無し";
            } else {
                $ben_ari = 0;
                for ($idx=1; $idx<=$field_count; $idx++) {
                    $key = "ben__".$idx."__";
                    $ben_type = $data_out[$key."ben_type"];
                    $ben_benryou = $data_out[$key."ben_benryou"];
                    $ben_ryou = $data_out[$key."ryou"];
                    if ($ben_type!="" || $ben_benryou!="" || $ben_ryou!="") $ben_ari = 1;
                    if ($ben_ari) {
                        $care_list["ben"][$ymd][] = array(
                            "ben_type"=>$data_out[$key."ben_type"],
                            "ben_benryou"=>$data_out[$key."ben_benryou"],
                            "ryou"=>($ben_ryou!="" ? $ben_ryou."g" : "")
                        );
                        if ($ben_ryou!="") {
							if ($ttl_ryou=="") $ttl_ryou = 0;
	                        $ttl_ryou += (int)$ben_ryou;
	                    }
                    }
                }
                if ($ben_ari) {
                    $care_list["ben__field_count"][$ymd] = $field_count."回";
                    $care_list["ben__ttl_ryou"][$ymd] = ($ttl_ryou==="" ? ""  :  $ttl_ryou."g");
                }
            }
        }
        //--------------------------
        // 尿
        //--------------------------
        $care_list["day_nyou_comment"][$ymd] = $data_out["day_nyou_comment"];
        $care_list["day_nyou"][$ymd] = $data_out["day_nyou"];
        $field_count = (int)@$data_out["nyou__field_count"];
        for ($idx=1; $idx<=$field_count; $idx++) {
            $nyou_hm = $data_out["nyou__".$idx."__nyou_hm"];
            $nyou_ryou = $data_out["nyou__".$idx."__ryou"];
            $nyou_more = $data_out["nyou__".$idx."__nyou_more"];
            if ($nyou_hm!="" || $nyou_ryou!="" || $nyou_more!="") {
				$care_list["nyou_list"][$ymd][] = array(
					"nyou_hm"=>($nyou_hm=="" ? "" : ((int)substr($nyou_hm,0,2)) . ":" . substr($nyou_hm,2)),
					"nyou_ryou"=>$nyou_ryou,"nyou_more"=>get_alias($nyou_more)
				);
			}
        }
        //--------------------------
        // 排液
        //--------------------------
        $care_list["haieki_syurui"][$ymd] = get_alias($data_out["haieki_syurui"]);
        $care_list["haieki_syurui_comment"][$ymd] = $data_out["haieki_syurui_comment"];
        $care_list["haieki_day_ryou"][$ymd] = $data_out["haieki_day_ryou"];
        $care_list["haieki_day_comment"][$ymd] = $data_out["haieki_day_comment"];
        $field_count = (int)@$data_out["haieki_jikantai__field_count"];
        for ($idx=1; $idx<=$field_count; $idx++) {
            $hm = $data_out["haieki_jikantai__".$idx."__haieki_hm"];
            $ryou = (int)$data_out["haieki_jikantai__".$idx."__ryou"];
            if ($hm=="" && !$ryou) continue;
            $haieki_more = get_alias($data_out["haieki_jikantai__".$idx."__haieki_more"]);
            $care_list["haieki_list"][$ymd][] = array(
            	"haieki_hm"=>($hm=="" ? "" : ((int)substr($hm,0,2)) . ":" . substr($hm,2)), "haieki_ryou"=>$ryou, "haieki_more"=>$haieki_more
            );
            $care_list["haieki_total"][$ymd] = (int)$data_out["haieki_total"] + (int)$ryou;
        }

        //--------------------------
        // 排液２
        //--------------------------
        $care_list["haieki2_exist"][$ymd] = get_alias($data_out["haieki2_exist"]);
        $care_list["haieki2_syurui"][$ymd] = get_alias($data_out["haieki2_syurui"]);
        $care_list["haieki2_syurui_comment"][$ymd] = $data_out["haieki2_syurui_comment"];
        $care_list["haieki2_day_ryou"][$ymd] = $data_out["haieki2_day_ryou"];
        $care_list["haieki2_day_comment"][$ymd] = $data_out["haieki2_day_comment"];
        $field_count = (int)@$data_out["haieki2_jikantai__field_count"];

        for ($idx=1; $idx<=$field_count; $idx++) {
            $hm = $data_out["haieki2_jikantai__".$idx."__haieki_hm"];
            $ryou = (int)$data_out["haieki2_jikantai__".$idx."__ryou"];
            if ($hm=="" && !$ryou) continue;
            $haieki_more = get_alias($data_out["haieki2_jikantai__".$idx."__haieki_more"]);
            $care_list["haieki2_list"][$ymd][] = array(
            	"haieki_hm"=>($hm=="" ? "" : ((int)substr($hm,0,2)) . ":" . substr($hm,2)), "haieki_ryou"=>$ryou, "haieki_more"=>$haieki_more
            );
            $care_list["haieki2_total"][$ymd] = (int)$data_out["haieki2_total"] + (int)$ryou;
        }
        if ($care_list["haieki2_exist"][$ymd]) $haieki2_exist = 1;

        //--------------------------
        // 排液３
        //--------------------------
        $care_list["haieki3_exist"][$ymd] = get_alias($data_out["haieki3_exist"]);
        $care_list["haieki3_syurui"][$ymd] = get_alias($data_out["haieki3_syurui"]);
        $care_list["haieki3_syurui_comment"][$ymd] = $data_out["haieki3_syurui_comment"];
        $care_list["haieki3_day_ryou"][$ymd] = $data_out["haieki3_day_ryou"];
        $care_list["haieki3_day_comment"][$ymd] = $data_out["haieki3_day_comment"];
        $field_count = (int)@$data_out["haieki3_jikantai__field_count"];
        for ($idx=1; $idx<=$field_count; $idx++) {
            $hm = $data_out["haieki3_jikantai__".$idx."__haieki_hm"];
            $ryou = (int)$data_out["haieki3_jikantai__".$idx."__ryou"];
            if ($hm=="" && !$ryou) continue;
            $haieki_more = get_alias($data_out["haieki3_jikantai__".$idx."__haieki_more"]);
            $care_list["haieki3_list"][$ymd][] = array(
            	"haieki_hm"=>($hm=="" ? "" : ((int)substr($hm,0,2)) . ":" . substr($hm,2)), "haieki_ryou"=>$ryou, "haieki_more"=>$haieki_more
            );
            $care_list["haieki3_total"][$ymd] = (int)$data_out["haieki3_total"] + (int)$ryou;
        }
        if ($care_list["haieki3_exist"][$ymd]) $haieki3_exist = 1;

        //--------------------------
        // 下剤
        //--------------------------
        $field_count = (int)$data_kensanado["gezai__field_count"];
        for ($idx=1; $idx<=$field_count; $idx++) {
            $hm = $data_kensanado["gezai__".$idx."__hm"];
            $care_list["gezai_list"][$ymd][] = array(
                "gezai_med_name" => $data_kensanado["gezai__".$idx."__med_name"],
                "gezai_hm" => ($hm=="" ? "" : ((int)substr($hm,0,2)) . ":" . substr($hm,2))
            );
        }
        //--------------------------
        // 座薬
        //--------------------------
        $field_count = (int)$data_kensanado["zayaku__field_count"];
        for ($idx=1; $idx<=$field_count; $idx++) {
            $hm = $data_kensanado["zayaku__".$idx."__hm"];
            $care_list["zayaku_list"][$ymd][] = array(
                "zayaku_med_name" => $data_kensanado["zayaku__".$idx."__med_name"],
                "zayaku_hm" => ($hm=="" ? "" : ((int)substr($hm,0,2)) . ":" . substr($hm,2))
            );
        }
        //--------------------------
        // 発赤
        //--------------------------
        $care_list["hosseki"][$ymd] = $data_kensanado["hosseki"];
        //--------------------------
        // 検査
        //--------------------------
        $kensa_names = array();
        if (strlen($data_kensanado["kensa_names"])) $kensa_names = explode(",", $data_kensanado["kensa_names"]);
        global $care_kensa_names; // 旧形式に対応
        foreach ($care_kensa_names as $key => $v) { // 旧形式に対応
            if ($data_kensanado["kensa_names_".$key]) $kensa_names[]= $v; // 旧形式に対応
        } // 旧形式に対応
        if (count($kensa_names)) $care_list["kensa_names_jp"][$ymd] = "・".implode("・", $kensa_names);
        $care_list["kensa_other_comment"][$ymd] = $data_kensanado["kensa_other_comment"];
        //--------------------------
        // 観察項目
        //--------------------------
        $field_count = $data_kansatu["kansatu__field_count"];
        for ($idx=1; $idx<=$field_count; $idx++) {
            $key = "kansatu__".$idx."__";
            $koumoku = get_alias($data_kansatu[$key."koumoku"]);
            if (!$koumoku) continue;
            $sinya = $data_kansatu[$key."sinya"];
            $nikkin = $data_kansatu[$key."nikkin"];
            $junya = $data_kansatu[$key."junya"];
            if (!$sinya && !$nikkin && !$junya) continue;
            if (!in_array($koumoku, $weekly_kansatu_header)) $weekly_kansatu_header[] = $koumoku;
            $care_list["kansatu"][$ymd][$koumoku]["sinya"] = $sinya;
            $care_list["kansatu"][$ymd][$koumoku]["nikkin"] = $nikkin;
            $care_list["kansatu"][$ymd][$koumoku]["junya"] = $junya;
        }
        //--------------------------
        // サイン
        //--------------------------
        $care_list["sign"][$ymd]["sinya"] = $data_sign["sign_emp_nm_sinya"];
        $care_list["sign"][$ymd]["nikkin"] = $data_sign["sign_emp_nm_nikkin"];
        $care_list["sign"][$ymd]["junya"] = $data_sign["sign_emp_nm_junya"];
    }
    $weekly_insulin_header = explode(" ", trim(implode(" ", $weekly_insulin_header))); // INDEXを詰める

    //=================================================
    // 指示
    //=================================================
    $sql =
    " select * from sum_ks_siji_ymd".
    " where ptif_id = ".dbES($ptif_id).
    " and ymd >= " . dbES($WYL[0]) . // 週頭月曜から
    " and ymd <= " . dbES($WYL[$day_size-1]). // 週末日曜まで
    " order by ymd desc";
    $sel = dbFind($sql);
    $siji_html = array();
    $siji_list = array();
    $siji_sections = array("freeword", "free2word", "keikan", "keikou","keikei","insulin_fix","bsscale","insulin_sr", "gezai", "soap");
    $siji_actives_data = array();
    $siji_all_elements_data = array();
    $min_ymd_list = array();
    while ($row = @pg_fetch_array($sel)) {
        $a_ymd = ymdhm_to_array($row["ymd"]);
        $hash = unserialize($row["serial_data_daily_bikou"]);
        $siji_list["daily_bikou"][$row["ymd"]] = $hash["daily_bikou"];
        $hash = unserialize($row["serial_data_weekly_bikou"]);
        $siji_list["weekly_bikou"][$row["ymd"]] = $hash["weekly_bikou"];

        $tmp = unserialize($row["serial_data_chusya"]); if (is_array($tmp)) $siji_list["chusya"][$row["ymd"]] = array_merge($tmp, $a_ymd);
        $tmp = unserialize($row["serial_data_freeword"]);  if (is_array($tmp)) $siji_list["freeword"][]  = array_merge($tmp, $a_ymd);
        $tmp = unserialize($row["serial_data_free2word"]);  if (is_array($tmp)) $siji_list["free2word"][]  = array_merge($tmp, $a_ymd);
        $tmp = unserialize($row["serial_data_keikan"]); if (is_array($tmp)) $siji_list["keikan"][] = array_merge($tmp, $a_ymd);
        $tmp = unserialize($row["serial_data_keikou"]); if (is_array($tmp)) $siji_list["keikou"][] = array_merge($tmp, $a_ymd);
        $tmp = unserialize($row["serial_data_keikei"]); if (is_array($tmp)) $siji_list["keikei"][] = array_merge($tmp, $a_ymd);
        $tmp = unserialize($row["serial_data_insulin_fix"]); if (is_array($tmp)) $siji_list["insulin_fix"][] = array_merge($tmp, $a_ymd);
        $tmp = unserialize($row["serial_data_bsscale"]);     if (is_array($tmp)) $siji_list["bsscale"][] = array_merge($tmp, $a_ymd);
        $tmp = unserialize($row["serial_data_insulin_sr"]);  if (is_array($tmp)) $siji_list["insulin_sr"][] = array_merge($tmp, $a_ymd);
        $tmp = unserialize($row["serial_data_gezai"]);  if (is_array($tmp)) $siji_list["gezai"][]  = array_merge($tmp, $a_ymd);
        // SOAPの場合は時間ソートしてから追加
        $tmp = unserialize($row["serial_data_soap"]);
        if (is_array($tmp)) {
            $field_count = $tmp["soap__field_count"];
            for ($idx = 1; $idx <= $field_count; $idx++) {
	            $prefix = "soap__".$idx."__";
                $hm = sprintf("%4s", trim(@$tmp[$prefix."action_hm"])).sprintf("%3s", $idx);
                $soap_row = array(
                	"soap_jikantai"=>$tmp[$prefix."soap_jikantai"],
                	"siji_text"=>$tmp[$prefix."siji_text"],
                	"siji_ptext"=>$tmp[$prefix."siji_ptext"],
                	"action_hm"=>$tmp[$prefix."action_hm"],
                	"field_idx"=>$idx
                );
            	$siji_list["soap"][$row["ymd"]][$hm] = array_merge($soap_row, $a_ymd);
            }
		}
    }

    // 月曜開始がなければ過去を探索
    foreach ($siji_sections as $section) {
		if ($section=="soap") continue; // ただしSOAPは過去不問。
        $_ymd = $siji_list[$section][count($siji_list[$section])-1]["ymd"];
        if ($_ymd==$WYL[0]) continue;
        $sql =
        " select ymd, serial_data_".$section." from sum_ks_siji_ymd".
        " where ptif_id = ".dbES($ptif_id).
        " and ymd < " . dbES($WYL[0]) . // 先週以前
        " and serial_data_".$section." <> ''".
        " and serial_data_".$section." like 'a:%'".
        " order by ymd desc limit 1";
        $row = dbGetTopRow($sql);
        if (!$row["ymd"]) continue;
        $a_ymd = ymdhm_to_array($row["ymd"]);
        $hash = unserialize($row["serial_data_".$section]);
        if (!is_array($siji_list[$section])) $siji_list[$section] = array();
        if (is_array($hash)) $siji_list[$section][] = array_merge($hash, $a_ymd);
    }

    if ($single_ymd) {
        $a_ymd = ymdhm_to_array($WYL[$day_size-1]);
        $siji_html[]= '<div style="padding:2px; background-color:#3171dd; color:#ffffff; text-align:center; font-size:12px">指示受記録</div>';
        $siji_html[]= '<div style="padding:4px; background-color:#2a9cf4; color:#ffffff; text-align:center; font-size:12px">';
        $siji_html[]= '<span style="font-size:14px; color:#ffffff">'.$a_ymd["slash_mdw"].'</span><br/>時点の最新指示</div>';
    } else {
        $opts = array();
        for ($idx=0; $idx<$day_size; $idx++) {
            $a_ymd = ymdhm_to_array($WYL[$idx]);
            $opts[] = '<option value="'.$WYL[$idx].'" '.($idx==$day_size-1 ? " selected" : "").'>'.$a_ymd["slash_mdw"].'</option>';
        }
        $siji_html[]= '<div style="padding:2px; background-color:#3171dd; color:#ffffff; text-align:center; font-size:12px">指示受記録</div>';
        $siji_html[]= '<div style="padding:4px; background-color:#2a9cf4; color:#ffffff; font-size:12px; text-align:center">';
        $siji_html[]= '<select style="font-size:14px; width:100px" id="siji_actives_selector"';
        $siji_html[]= ' onchange="dfmMainMenu.setSijiActivesData()">'.implode("",$opts).'</select>';
        $siji_html[]= '<div style="color:#ffffff; padding-top:2px">時点の最新指示</div>';
        $siji_html[]= '<div style="color:#ffffff; padding-top:2px"><label class="reverse">';
        $siji_html[]= '<input type="checkbox" id="siji_actives_show_all" onclick="dfmMainMenu.setSijiActivesData()" checked>週の全指示</label></div>';
        $siji_html[]= '</div>';
    }

    foreach ($siji_sections as $section) {
		if ($section=="soap") continue; // ただしSOAPは過去不問。
        for ($idx = 0; $idx < count($siji_list[$section]); $idx++) { // 下剤なら下剤を、古い順からループ
            if ($idx==0) $siji_html[]='<div style="padding-top:4px"><div style="border-bottom:1px solid #aad2f0; border-top:1px solid #c4e6ff;"><div style="border:1px solid #88caff; border-bottom:1px solid #175588; border-top:1px solid #88caff;">';
            $siji_all_elements_data[] = 'siji_'.$section.'_field'.$idx; // 指示HTMLセルブロックのID
            $siji_html[]= make_siji_right_field_block($section, $siji_list[$section][$idx], $idx); // 指示のHTMLセルブロックを作成
            if ($idx==count($siji_list[$section])-1) $siji_html[]= "</div></div></div>";
        }
    }

    for ($ii=0; $ii<$day_size; $ii++) { // 日付をループして
    	// 有効な指示を抽出する
        foreach ($siji_sections as $section) {
			if ($section=="soap") continue; // SOAPは、有効指示はどうでもいい
            $nearly_id = "";
            for ($idx = count($siji_list[$section])-1; $idx>=0; $idx--) { // 下剤なら下剤を、古い順からループ
                if ($siji_list[$section][$idx]["ymd"]>$WYL[$ii]) break; // もし下剤のYMDが、表示日付より未来なら終了
                $nearly_id = 'siji_'.$section.'_field'.$idx; // 有効なIDを上書き
            }
            $siji_actives_data['YMD_'.$WYL[$ii]][] = $nearly_id;
        }
    }

    //=================================================
    // 記入者（最終記入者）
    //=================================================
    $sql =
    " select h1.ymd, h1.update_emp_name from sum_ks_update_history h1".
    " inner join (".
    "     select ptif_id, ymd, max(update_timestamp) as max_timestamp".
    "     from sum_ks_update_history".
    "     where ptif_id = ".dbES($ptif_id)." and ymd >= " . dbES($WYL[0])." and ymd <= ".dbES($WYL[$day_size-1]).
    "     group by ptif_id, ymd".
    " ) h2 on (h2.ptif_id = h1.ptif_id and h2.ymd = h1.ymd and h2.max_timestamp = h1.update_timestamp)".
    " where h1.ptif_id = ".dbES($ptif_id)." and h1.ymd >= " . dbES($WYL[0])." and h1.ymd <= ".dbES($WYL[$day_size-1]);
    $sel = dbFind($sql);
    $update_history = array();
    while ($row = @pg_fetch_array($sel)) {
        $update_history[$row["ymd"]] = $row["update_emp_name"];
    }

    //==============================================================================
    // 温度板バイタル（グラフ情報）の収集
    //==============================================================================
    $sql =
    " select * from sum_ks_vital".
    " where ptif_id = ".dbES($ptif_id)." and ymdhm >= '".$ymd_before1."2400' and ymdhm <= '".$ymd_after1."0000'".
    " order by ymdhm";
    $vital_db_data = array();
    $sel = dbFind($sql);
    while ($row = @pg_fetch_array($sel)) $vital_db_data[to_24hour($row["ymdhm"])] = $row;

    $base_sql = "select 1 as is_out_of_term, * from sum_ks_vital where ptif_id = ".dbES($ptif_id)." ";
    $row = dbGetTopRow($base_sql."and ymdhm < '".$ymd_before1 ."2400' and taion <> '' order by ymdhm desc limit 1");
    if ($row && !$vital_db_data[to_24hour($row["ymdhm"])]) $vital_db_data[to_24hour($row["ymdhm"])] = $row;

    $row = dbGetTopRow($base_sql."and ymdhm > '".$ymd_after1 ."0000' and taion <> '' order by ymdhm limit 1");
    if ($row && !$vital_db_data[to_24hour($row["ymdhm"])]) $vital_db_data[to_24hour($row["ymdhm"])] = $row;

    $row = dbGetTopRow($base_sql."and ymdhm < '".$ymd_before1 ."2400' and myakuhakusu <> '' order by ymdhm desc limit 1");
    if ($row && !$vital_db_data[to_24hour($row["ymdhm"])]) $vital_db_data[to_24hour($row["ymdhm"])] = $row;

    $row = dbGetTopRow($base_sql."and ymdhm > '".$ymd_after1 ."0000' and myakuhakusu <> '' order by ymdhm limit 1");
    if ($row && !$vital_db_data[to_24hour($row["ymdhm"])]) $vital_db_data[to_24hour($row["ymdhm"])] = $row;

    $row = dbGetTopRow($base_sql."and ymdhm < '".$ymd_before1 ."2400' and (ketsuatsu_upper <> '' or ketsuatsu_bottom <> '') order by ymdhm desc limit 1");
    if ($row && !$vital_db_data[to_24hour($row["ymdhm"])]) $vital_db_data[to_24hour($row["ymdhm"])] = $row;

    $row = dbGetTopRow($base_sql."and ymdhm > '".$ymd_after1 ."0000' and (ketsuatsu_upper <> '' or ketsuatsu_bottom <> '') order by ymdhm limit 1");
    if ($row && !$vital_db_data[to_24hour($row["ymdhm"])]) $vital_db_data[to_24hour($row["ymdhm"])] = $row;

    ksort($vital_db_data);

    $graphP = array(); // 脈拍数
    $graphT = array(); // 体温
    $graphR = array(); // 呼吸数
    $graphBP = array(); // 血圧

    $dt_zeroday = strtotime($WYL[0]) - 86400;
/*
        $data_hr_spo2 = unserialize($row["serial_data_hr_spo2"]);
*/

    foreach($vital_db_data as $ymdhm24 => $row){
        $a_ymd = ymdhm_to_array($row["ymdhm"]);

        $dt_curday = strtotime($a_ymd["slash_ymd"]);
        $delta = abs($dt_curday - $dt_zeroday);
        $days = floor($delta / 86400);
        $day_index = $days * ($dt_zeroday > $dt_curday ? -1 : 1);

        $hour =   (int)trim($a_ymd["hh"]); // 測定時
        $minute = (int)trim($a_ymd["mi"]); // 測定分

        $graph_day = ((int)$day_index-1)*24*60 + $hour*60 + $minute; // グラフ上の横位置（分単位）
        $params = array();
        $params["ymd"] = $a_ymd["ymd"];
        $params["ymdhm"] = $row["ymdhm"];
        $params["hh"] = $a_ymd["hh"];
        $params["mi"] = $a_ymd["mi"];
        $params["hhmi"] = $a_ymd["hhmi"];
        $params["hhmm_jp"] = $a_ymd["hh"]."時".$a_ymd["mi"]."分";
        $params["ymdhm_en"] = $a_ymd["ymdhm_en"];
        $params["left"] = $graph_day;
        $params["leftpx"] = 0;
        $params["ketsuatsu_upper"] = trim($row["ketsuatsu_upper"]); // 収縮期血圧 (最大240)
        $params["ketsuatsu_bottom"] = trim($row["ketsuatsu_bottom"]); // 拡張時血圧 (最小40)
        $params["myakuhakusu"] = trim($row["myakuhakusu"]); // 脈拍数 (10-210)
        $params["kokyusu"] = trim($row["kokyusu"]); // 呼吸数 (5-55)
        $params["taion"] = trim($row["taion"]); // 体温 (33-43度)
        $params["myakuhakusu_x_marker"] = trim($row["myakuhakusu_x_marker"]); // 脈拍数マーカ（空文字または「x」）
        $params["datetime_jp"] = $a_ymd["slash_mdw"] . " " . (int)$hour .":" . sprintf("%02d",$minute);
        $aTaion = explode(".",$params["taion"].".");
        $params["taion1"] = $aTaion[0];
        $params["taion2"] = $aTaion[1];

        //--------------------------
        // 血圧（BP）
        //--------------------------
        if ($params["ketsuatsu_bottom"]!="" || $params["ketsuatsu_upper"]!=""){
            $vBPu = trim($params["ketsuatsu_upper"]); // 収縮期
            $vBPb = trim($params["ketsuatsu_bottom"]); // 拡張時
            $ivBPu = (int)$vBPu;
            $ivBPb = (int)$vBPb;
            if ($vBPu=="") $ivBPu = 240;
            if ($vBPb=="") $ivBPb = 40;
            if ($ivBPb < $ivBPu) {
                $params["disp_ketsuatsu"] = $vBPu." 〜 ".$vBPb;
                $graphBP[] = $graph_day."=".$ivBPb."_".$ivBPu;
            }
        }
        //--------------------------
        // 脈拍（P）
        //--------------------------
        if ($params["myakuhakusu"]!=""){
            $vP = (float)$params["myakuhakusu"];
            $params["disp_myakuhaku"] = $vP;
            if ($vP< 10) $vP = 10;
            if ($vP> 210) $vP = 210;
            $graphP[] = $graph_day."=".$vP."=".($row["myakuhakusu_x_marker"] ? "x" : "");
        }
        //--------------------------
        // 呼吸数（R）
        //--------------------------
        if ($params["kokyusu"]!=""){
            $vR = (float)$params["kokyusu"];
            $params["disp_kokyusu"] = $vR;
            if ($vR < 5) $vR = 5;
            if ($vR > 55) $vR = 55;
            $graphR[] = $graph_day."=".$vR;
        }
        //--------------------------
        // 体温（T）
        //--------------------------
        if ($params["taion"]!=""){
            $vT = (float)$params["taion"];
            $params["disp_taion"] = $vT;
            if ($vT < 33) $vT = 33;
            if ($vT > 43) $vT = 43;
            $graphT[] = $graph_day."=".$vT;
        }
        $vitalPosData[] = array("ymdhm"=>$row["ymdhm"], "graph_day"=>$graph_day);
        $tooltipData[$row["ymdhm"]] = array_merge((array)$tooltipData[$row["ymdhm"]], $params);

        //--------------------------
        // SPO2（S） (30-100％)
        //--------------------------
        if ($row["spo2_percent"]) {
            $care_list["spo2"][$params["ymd"]][$params["hhmi"]] = array("ymdhm"=>$params["ymdhm"], "spo2_percent"=>$row["spo2_percent"]);
        }
        //--------------------------
        // HR
        //--------------------------
        if ($row["hr_kaisu"]) {
            $care_list["hr"][$params["ymd"]][$params["hhmi"]] = array("ymdhm"=>$params["ymdhm"], "hr_kaisu"=>$row["hr_kaisu"]);
        }
        //--------------------------
        // 体重
        //--------------------------
        if ($row["taiju"]) {
            $care_list["taiju"][$params["ymd"]][$params["hhmi"]] = array("ymdhm"=>$params["ymdhm"], "taiju"=>$row["taiju"]);
        }
    }

    if (defined("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING")) {
        return array(
            "graphT" => $graphT,
            "graphR" => $graphR,
            "graphP" => $graphP,
            "graphBP" => $graphBP,
            "graphL" => $graphL,
            "graphK" => $graphK,
            "graphSK" => $graphSK,
            "update_history" => $update_history,
            "care_list" => $care_list,
            "siji_list" => $siji_list,
            "weekly_insulin_header" => $weekly_insulin_header,
            "weekly_ben_ari" => $weekly_ben_ari,
            "weekly_kansatu_header" => $weekly_kansatu_header,
            "haieki2_exist" => $haieki2_exist,
            "haieki3_exist" => $haieki3_exist
        );
    }
?>
<input type="hidden" id="sijiAllElementsData" value = "<?=hh(to_json($siji_all_elements_data))?>" />
<input type="hidden" id="sijiActivesData" value = "<?=hh(to_json($siji_actives_data))?>" />
<input type="hidden" id="serialDataT" value = "<?=implode(",",$graphT)?>" />
<input type="hidden" id="serialDataR" value = "<?=implode(",",$graphR)?>" />
<input type="hidden" id="serialDataP" value = "<?=implode(",",$graphP)?>" />

<input type="hidden" id="serialDataBP" value = "<?=implode(",",$graphBP)?>" />
<input type="hidden" id="serialDataL" value = "<?=implode("\n",$graphL)?>" />
<input type="hidden" id="serialDataK" value = "<?=implode("\n",$graphK)?>" />
<input type="hidden" id="serialDataSK" value = "<?=hh(implode("\n",$graphSK))?>" />
<input type="hidden" id="serialVitalPosData" value="<?=hh(to_json($vitalPosData))?>"/>


<? //=========================================================================?>
<? // 温度板マウスムーブのツールチップ                                        ?>
<? //=========================================================================?>
<div id="ondoban_tooltip" style="position:absolute; top:0; height:0; z-index:1000; display:none">
<? foreach ($tooltipData as $row){ ?>
<div id="tooltip_detail_<?=$row["ymdhm"]?>">
    <table cellspacing="0" cellpadding="0" style="border:0; border-collapse:collapse" class="plane"><tr>
    <td style="border:2px solid #b70080; background-color:#fff; padding:2px;">
    <table cellspacing="0" cellpadding="0" class="tooltip" style="margin:1px; background-color:#fff; border:1px solid #fbb">
        <tr><th colspan="4">
            <div style='border-bottom:2px solid #9d88ff; margin:3px; color:#7d62ff' id="tooltip_ymdhm"><nobr><?=$row["ymdhm_en"]?></nobr></div>
        </th></tr>
        <? if ($row["disp_taion"]){ ?><tr>
            <th style='color:#0000ff'>T</th><td style='color:#777; font-size:12px'>体温</td>
            <td id="tooltip_td_t"><?=$row["disp_taion"]?></td><td style='color:#aaa; font-size:12px'>℃</td>
        </tr><? } ?>
        <? if ($row["disp_myakuhaku"]){ ?><tr>
            <th style='color:#ff0000'>P</th><td style='color:#777; font-size:12px'>脈拍</td>
            <td id="tooltip_td_p"><?=$row["disp_myakuhaku"]?></td><td style='color:#aaa; font-size:12px'><nobr>回/分</nobr></td>
        </tr><? } ?>
        <? if ($row["disp_kokyusu"]){ ?><tr>
            <th style='color:#008822'>R</th><td style='color:#777; font-size:12px'><nobr>呼吸数</nobr></td>
            <td id="tooltip_td_r"><?=$row["disp_kokyusu"]?></td><td style='color:#aaa; font-size:12px'><nobr>回/分</nobr></td>
        </tr><? } ?>
        <? if ($row["disp_ketsuatsu"]){ ?><tr>
            <th style='color:#8a8a41'>BP</th><td style='color:#777; font-size:12px'>血圧</td>
            <td id="tooltip_td_bp"><nobr><?=$row["disp_ketsuatsu"]?></nobr></td><td style='color:#aaa; font-size:12px'>mmHg</td>
        </tr><? } ?>
        <? if ($row["disp_spo2"]){ ?><tr>
            <th style='color:#1b9cab'>SP</th><td style='color:#777; font-size:12px'>SPO2</td>
            <td id="tooltip_td_s"><nobr><?=$row["disp_spo2"]?></nobr></td><td style='color:#aaa; font-size:12px'>％</td>
        </tr><? } ?>
    </table></td></tr></table>
</div>
<? } ?>
</div>


<? //=========================================================================?>
<? // トップに貼り付く 日付、バイタル入力ボタン                               ?>
<? //=========================================================================?>
<? if($stickyMenubar) { ?>
<table cellspacing="1" cellpadding="0" class="ondoban_table" id="ondoban_table_float_header">
<tr class="rowhead" id="ondoban_float_header_toprow">
    <th style="width:120px; line-height:22px; color:#0a66b1"><?=substr($WYL[0],0,4)?>年</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <th id="ondobanFloatTHeader<?=$ii+1?>" style="width:<?=$dayWidth?>px">
        <div style="position:relative; width:100%; text-align:center;">
        <button type="button" onclick="dfmSijiMenu.popup('<?=$WYL[$ii]?>')" style="width:100%; padding:2px 0">
        <? $a_ymd = ymdhm_to_array($WYL[$ii]); ?><nobr>
        <span style="font-size:14px;"><?=$a_ymd["zmm"]."/".$a_ymd["zdd"]?></span><span style="font-size:12px;">(<?=week_jp($WYL[$ii])?>)</span></nobr>
        </button></div>
    </th>
    <? } ?>
</tr>
</table>
<? } ?>




<table cellspacing="1" cellpadding="0" class="ondoban_table" id="ondoban_table">
<tbody>

<? if(!$stickyMenubar) { ?>
<tr class="rowhead" id="ondoban_float_header_toprow">
    <th style="width:120px; line-height:22px; color:#0a66b1"><?=substr($WYL[0],0,4)?>年</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <th id="ondobanTHeader<?=$ii+1?>" style="width:<?=($is_not_pc?($day_size==1 ? '737' : '104'):$dayWidth)?>px">
        <div style="position:relative; width:100%; text-align:center;">
        <button type="button" onclick="dfmSijiMenu.popup('<?=$WYL[$ii]?>')" style="width:100%; padding:2px 0">
        <? $a_ymd = ymdhm_to_array($WYL[$ii]); ?><nobr>
        <span style="font-size:14px;"><?=$a_ymd["slash_mdw"]?></span></nobr>
        </button></div>
    </th>
    <? } ?>
</tr>
<? } ?>


<? //=========================================================================?>
<? // 日付ヘッダ行（隠れている）                                              ?>
<? //=========================================================================?>
<? if($stickyMenubar) { ?>
<tr class="rowhead">
    <th style="width:120px"></th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <th id="ondobanTHeader<?=$ii+1?>" style="width:<?=($is_not_pc? ($day_size==1 ? '737' : '104'):$dayWidth)?>px"><?$a_ymd = ymdhm_to_array($WYL[$ii]); echo $a_ymd["slash_mdw"];?></th>
    <? } ?>
</tr>
<? } ?>


<? //=========================================================================?>
<? // バイタル目盛り画像と折れ線メイングラフ                                  ?>
<? //=========================================================================?>
<tr onmouseout="className='';">
    <th class="left"><div style="width:120px; height:300px;"
        id="graph_head_canvas_div"><canvas width="120" height="300" id="graph_head_canvas"></canvas></div></th>
    <th class="left" colspan="<?=$day_size?>">
        <div id="graph_main_canvas_div" style="position:relative">
        <? for ($ii=0; $ii<$day_size; $ii++){ ?>
            <div class="clickbox ondoban_clickbox" style="width:<?=$dayWidth?>px; left:<?=$dayWidth*$ii+$ii?>px; height:300px; overflow:hidden"
                day_index="<?=$ii?>" onclick="dfmVital.popup(<?=$WYL[$ii]?>, tooltipYmdhm)"
                onmousemove="showOndobanTooltip(event, <?=$ii?>)" onmouseout="hideOndobanTooltip()"><img src="../img/spacer.gif" alt="" width="1000" height="300" /></div>
        <? }?>
        <canvas width="400" height="300" id="graph_main_canvas"></canvas>
        </div>
    </th>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th navititle="酸素" navi="[紫]酸素吸入／[橙]人工呼吸器<br>酸素吸入＝「期間開始時刻」＋「薬剤名」＋「量」<br>人工呼吸器＝「期間開始時刻」＋「酸素濃度」＋「濃度単位」<br>開始日時、終了日時は四角で表示されます。表示期間の前後に継続している場合は、四角が表示されません。<br>日時が重複する登録が存在しなければ１行で表示されますが、重複する場合は、自動的に複数行に分かれて、上から時系列順にグラフ表示されます。">酸素</th>
    <td colspan="<?=$day_size?>" class="left"><div id="graph_sanso_canvas_div" style="position:relative">
        <? for ($ii=0; $ii<$day_size; $ii++){ ?>
            <div class="clickbox sanso_clickbox" style="width:<?=$dayWidth?>px; left:<?=$dayWidth*$ii+$ii?>px; overflow:hidden" title="酸素"
                day_index="<?=$ii?>" onclick="dfmCareSanso.popup(<?=$WYL[$ii]?>)"><img src="../img/spacer.gif" alt="" width="1000" height="300" /></div>
        <? }?>
        <canvas id="graph_sanso_canvas"></canvas>
        </div>
    </td>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th class="th_color2" navititle="抗生剤" navi="グラフには薬剤名が併記されます。<br>開始日時、終了日時は四角で表示されます。表示期間の前後に継続している場合は、四角が表示されません。<br>日時が重複する登録が存在しなければ１行で表示されますが、重複する場合は、自動的に複数行に分かれて、上から時系列順にグラフ表示されます。">抗生剤</th>
    <td class="left" colspan="<?=$day_size?>"><div id="graph_kouseizai_canvas_div" style="position:relative">
        <? for ($ii=0; $ii<$day_size; $ii++){ ?>
            <div class="clickbox kouseizai_clickbox" style="width:<?=$dayWidth?>px; left:<?=$dayWidth*$ii+$ii?>px; overflow:hidden" title="抗生剤"
                day_index="<?=$ii?>" onclick="dfmCareKouseizai.popup(ptif_id, <?=$WYL[$ii]?>)"><img src="../img/spacer.gif" alt="" width="1000" height="300" /></div>
        <? }?>
        <canvas id="graph_kouseizai_canvas"></canvas>
        </div>
    </td>
</tr>

<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th navititle="注射処方等指示" navi="表示内容、および列をクリックして表示される「注射処方等指示」ダイアログは、「指示板」画面から呼び出されるものと同じものです。<br>最終更新者はダイアログ下部に表示されますが、他のケア関連ダイアログのように、更新履歴を参照することはできません。">注射処方等指示</th>
    <?
        for ($ii=0; $ii<$day_size; $ii++){
            $chusya_list = $siji_list["chusya"][$WYL[$ii]];
            $field_count = $chusya_list["chusya__field_count"];
    ?>
        <td style="height:22px" class="siji_cell<?=$taiin_classname[$ii]?>" onmouseover="$(this).addClass('hover').addClass('hover1')"
            onmouseout="$(this).removeClass('hover').removeClass('hover1')" title="注射処方等指示"
            onclick="stopEvent(event); dfmSijiban.popup(ptif_id,'<?=$WYL[$ii]?>',0)">

        <? for ($fnum = 1; $fnum<=$field_count; $fnum++) { ?>
                <div class="o_abs left borderb hover siji_cell<?=$taiin_classname[$ii]?>"<?=$ww1?>
                    onclick="stopEvent(event); dfmSijiban.popup(ptif_id,'<?=$WYL[$ii]?>',<?=$fnum?>)"
                    onmouseover="this.parentNode.parentNode.className='hover_tr'; return stopEvent(event);">
                <?
                    $prefix = "chusya__".$fnum."__";
                    if ($chusya_list[$prefix."siji_az"]) echo '<span class="az">'.wbr(get_alias($chusya_list[$prefix."siji_az"])).'</span>';
                    if ($chusya_list[$prefix."is_continue"]) echo '<span class="is_continue">継</span>';
                    if ($chusya_list[$prefix."siji_picture_names"]) echo '<span class="pict">画'.count(explode(",",$chusya_list[$prefix."siji_picture_names"])).'</span>';
                    if ($chusya_list[$prefix."is_sumi"]) echo '<span class="is_sumi">済</span>';
                    if ($chusya_list[$prefix."is_stop"]) echo '<span class="is_stop">止</span>';
                ?>
                <?=nl2br2(wbr($chusya_list["chusya__".$fnum."__siji_text"]))?>
                <br/>
            </div>
        <? } ?>
        <div class="new_entry"><div>【新規作成】</div></div>
        </td>
    <? } ?>
</tr>

<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th class="th_color2" navititle="心拍数">心拍数</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
        <td style="height:22px" class="siji_cell<?=$taiin_classname[$ii]?>" onmouseover="$(this).addClass('hover').addClass('hover1')"
            onmouseout="$(this).removeClass('hover').removeClass('hover1')" title="心拍数"
            onclick="stopEvent(event); dfmVital.popup('<?=$WYL[$ii]?>')">

        <? if (is_array($care_list["hr"][$WYL[$ii]])) { ?>
        <table cellspacing="0" cellpadding="0" class="o_child"><tbody>
        <?
            $cnt=0;
            foreach ($care_list["hr"][$WYL[$ii]] as $hm => $item) {
                $cnt++;
        ?>
            <tr class="hover borderb siji_cell<?=$taiin_classname[$ii]?>" id="otr_<?=++$tridx?>"
                onclick="stopEvent(event); dfmVital.popup(<?=$WYL[$ii]?>, '<?=$item["ymdhm"]?>')"
                onmouseover="this.parentNode.parentNode.parentNode.parentNode.className='hover_tr'; return stopEvent(event);">
                <td class="wp50"><div class="o_abs ww2"<?=$ww2?>><?=wbr($hm)?></div></td>
                <td class="borderl wp50"><div class="o_abs ww2"<?=$ww2?>><?=wbr($item["hr_kaisu"])?></div></td>
            </tr>
        <? } ?>
        </tbody></table>
        <? } ?>
        <div class="new_entry"><div>【新規作成】</div></div>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th class="th_color2" navititle="SpO2">SpO2</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
        <td style="height:22px" class="siji_cell<?=$taiin_classname[$ii]?>" onmouseover="$(this).addClass('hover').addClass('hover1')"
            onmouseout="$(this).removeClass('hover').removeClass('hover1')" title="SpO2"
            onclick="stopEvent(event); dfmVital.popup('<?=$WYL[$ii]?>')">

        <? if (is_array($care_list["spo2"][$WYL[$ii]])) { ?>
        <table cellspacing="0" cellpadding="0" class="o_child"><tbody>
        <?
            $cnt=0;
            foreach ($care_list["spo2"][$WYL[$ii]] as $hm => $item) {
                $cnt++;
        ?>
            <tr class="hover borderb siji_cell<?=$taiin_classname[$ii]?>" id="otr_<?=++$tridx?>"
                onclick="stopEvent(event); dfmVital.popup(<?=$WYL[$ii]?>, '<?=$item["ymdhm"]?>')"
                onmouseover="this.parentNode.parentNode.parentNode.parentNode.className='hover_tr'; return stopEvent(event);">
                <td class="wp50"><div class="o_abs ww2"<?=$ww2?>><?=wbr($hm)?></div></td>
                <td class="borderl wp50"><div class="o_abs ww2"<?=$ww2?>><?=wbr($item["spo2_percent"])?></div></td>
            </tr>
        <? } ?>
        </tbody></table>
        <? } ?>
        <div class="new_entry"><div>【新規作成】</div></div>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th class="th_color2" navititle="体重">体重</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
        <td style="height:22px" class="siji_cell<?=$taiin_classname[$ii]?>" onmouseover="$(this).addClass('hover').addClass('hover1')"
            onmouseout="$(this).removeClass('hover').removeClass('hover1')" title="体重"
            onclick="stopEvent(event); dfmVital.popup('<?=$WYL[$ii]?>')">
        <? if (is_array($care_list["taiju"][$WYL[$ii]])) { ?>
        <table cellspacing="0" cellpadding="0" class="o_child">
        <?
            $cnt=0;
            foreach ($care_list["taiju"][$WYL[$ii]] as $hm => $item) {
                $cnt++;
        ?>
            <tr class="hover borderb siji_cell<?=$taiin_classname[$ii]?>" id="otr_<?=++$tridx?>"
                onclick="stopEvent(event); dfmVital.popup(<?=$WYL[$ii]?>, '<?=$item["ymdhm"]?>')" onmouseover="return stopEvent(event);">
                <td class="wp50"><div class="o_abs ww2"<?=$ww2?>><?=wbr($hm)?></div></td>
                <td class="borderl wp50"><div class="o_abs ww2"<?=$ww2?>><?=wbr($item["taiju"])?></div></td>
            </tr>
        <? } ?>
        </table>
        <? } ?>
        <div class="new_entry"><div>【新規作成】</div></div>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th style="background-color:#dddddd">【年月日】</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td style="background-color:#dddddd; text-align:center">
    <?
        $a_ymd = ymdhm_to_array($WYL[$ii]);
        echo $a_ymd["slash_mdw"];
    ?>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th navititle="食事量（経口）">食事量（経口）</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareSyokujiIn.popup(<?=$WYL[$ii]?>)" title="食事量（経口）">
        <?
        $fuku_a = @$care_list["keikou_asa_fukusyoku"][$WYL[$ii]];
        $fuku_h = @$care_list["keikou_hiru_fukusyoku"][$WYL[$ii]];
        $fuku_y = @$care_list["keikou_yu_fukusyoku"][$WYL[$ii]];
        $syu_a = @$care_list["keikou_asa_syusyoku"][$WYL[$ii]];
        $syu_h = @$care_list["keikou_hiru_syusyoku"][$WYL[$ii]];
        $syu_y = @$care_list["keikou_yu_syusyoku"][$WYL[$ii]];
        if ($fuku_a!="" || $fuku_h!="" || $fuku_y!="" || $syu_a!="" || $syu_h!="" || $syu_y!="") {
        if ($fuku_a=="") $fuku_a = "　";
        ?>
        <table cellspacing="0" cellpadding="0" class="o_child">
        <tr id="otr_<?=++$tridx?>">
            <td class="wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($fuku_a)?></div></td>
            <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($fuku_h)?></div></td>
            <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($fuku_y)?></div></td>
        </tr>
        <tr class="bordert" id="otr_<?=++$tridx?>">
            <td class="wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($syu_a)?></div></td>
            <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($syu_h)?></div></td>
            <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($syu_y)?></div></td>
        </tr>
        </table>
        <? } ?>
    </td>
    <? } ?>
</tr>

<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th navititle="食事量（経管）">食事量（経管）</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareSyokujiIn.popup(<?=$WYL[$ii]?>)" title="食事量（経管）">
        <? $keikan_field_count = (int)$care_list["keikan__field_count"][$WYL[$ii]]; ?>
        <? for ($idx=1; $idx<=$keikan_field_count; $idx++) { ?>
            <?
                $ryou4 = @$care_list["keikan__".$idx."__ryou4_ml"][$WYL[$ii]];
                $ryou5 = @$care_list["keikan__".$idx."__ryou5_ml"][$WYL[$ii]];
                $ryou6 = @$care_list["keikan__".$idx."__ryou6_ml"][$WYL[$ii]];
            ?>
            <div<?=($idx>1 ?' class="bordert"':"")?>><?=$care_list["keikan__".$idx."__eiyouzai"][$WYL[$ii]]?></div>
            <table cellspacing="0" cellpadding="0" class="o_child">
            <tr id="otr_<?=++$tridx?>" class="bordert<?=(($ryou4!="" || $ryou5!="" || $ryou6!="") ? "" : " borderb")?>">
            <td class="wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr(@$care_list["keikan__".$idx."__asa_ml"][$WYL[$ii]])?></div></td>
            <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr(@$care_list["keikan__".$idx."__hiru_ml"][$WYL[$ii]])?></div></td>
            <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr(@$care_list["keikan__".$idx."__yu_ml"][$WYL[$ii]])?></div></td>
            </tr>
            <? if ($ryou4!="" || $ryou5!="" || $ryou6!="") { ?>
                <tr id="otr_<?=++$tridx?>" class="bordert">
                <td class="wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($ryou4)?></div></td>
                <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($ryou5)?></div></td>
                <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($ryou6)?></div></td>
                </tr>
            <? } ?>
            </table>
        <? } ?>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th navititle="補食">補食</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareSyokujiIn.popup(<?=$WYL[$ii]?>)" title="補食">
        <?
            $hosyoku_field_count = (int)$care_list["hosyoku__field_count"][$WYL[$ii]];
            if ($hosyoku_field_count) {
        ?>
        <table cellspacing="0" cellpadding="0" class="o_child">
        <? for ($idx=1; $idx<=$hosyoku_field_count; $idx++) { ?>
            <tr class="bordert" id="otr_<?=++$tridx?>">
                <td colspan="2">
                    <div class="o_abs"<?=$ww1?>><?=wbr(get_alias(@$care_list["hosyoku__".$idx."__hosyoku_timing"][$WYL[$ii]]))?></div>
                </td>
            </tr>
            <tr class="bordert" id="otr_<?=++$tridx?>">
                <td class="wp60"><div class="o_abs ww06"<?=$ww06?>><?=wbr(get_alias(@$care_list["hosyoku__".$idx."__hosyoku"][$WYL[$ii]]))?></div></td>
                <td class="borderl wp40"><div class="o_abs ww04"<?=$ww04?>><?=wbr(get_alias(@$care_list["hosyoku__".$idx."__hosyoku_ryou"][$WYL[$ii]]))?></div></td>
            </tr>
        <? } ?>
        </table>
        <? } ?>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th navititle="水分摂取量" navi="「経口経管区分」＋「摂取量数値」＋「単位」<br>※経口経管区分＝経口：先頭文字が口／経管：先頭文字が管<br>※単位＝「mL」または「割」">水分摂取量</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareSyokujiIn.popup(<?=$WYL[$ii]?>)" title="水分摂取量">
        <table cellspacing="0" cellpadding="0" class="o_child"><tr id="otr_<?=++$tridx?>">
            <td class="wp20"><div class="o_abs ww02"<?=$ww02?>><?=wbr(@$care_list["suibun_ryou_1"][$WYL[$ii]])?></div></td>
            <td class="borderl wp20"><div class="o_abs ww02"<?=$ww02?>><?=wbr(@$care_list["suibun_ryou_2"][$WYL[$ii]])?></div></td>
            <td class="borderl wp20"><div class="o_abs ww02"<?=$ww02?>><?=wbr(@$care_list["suibun_ryou_3"][$WYL[$ii]])?></div></td>
            <td class="borderl wp20"><div class="o_abs ww02"<?=$ww02?>><?=wbr(@$care_list["suibun_ryou_4"][$WYL[$ii]])?></div></td>
            <td class="borderl wp20"><div class="o_abs ww02"<?=$ww02?>><?=wbr(@$care_list["suibun_ryou_5"][$WYL[$ii]])?></div></td>
        </tr></table>
    </td>
    <? } ?>
</tr>


<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th class="th_color2" navititle="補液" navi="「指示az」＋「時刻」＋「輸液量/流速」または「更新」">補液(輸液残/流速)</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareHoeki.popup(<?=$WYL[$ii]?>)" title="補液">
        <table cellspacing="0" cellpadding="0" class="o_child fontsize11">
        <? $field_count = count($care_list["hoeki__data"][$WYL[$ii]]); ?>
        <? for ($fnum = 0; $fnum<$field_count; $fnum++) { ?>
        <tr id="otr_<?=++$tridx?>" class="<?=($fnum>0?" bordert":"")?>">
        <td class="wp15"><div class="o_abs ww015"<?=$ww015?>><?=wbr(@$care_list["hoeki__data"][$WYL[$ii]][$fnum]["az"])?></div></td>
        <td class="borderl wp35"><div class="o_abs ww035"<?=$ww035?>><?=wbr(@$care_list["hoeki__data"][$WYL[$ii]][$fnum]["hm"])?></div></td>
        <td class="borderl wp50"><div class="o_abs ww2"<?=$ww2?>><?=wbr(@$care_list["hoeki__data"][$WYL[$ii]][$fnum]["data"])?></div></td>
        </tr>
        <? } ?>
        </table>
    </td>
    <? } ?>
</tr>



<? for ($i0=0; $i0<count($weekly_insulin_header); $i0++) { ?>
<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th navititle="ｲﾝｽﾘﾝ<?=($weekly_insulin_header[$i0] ? "：".$weekly_insulin_header[$i0]: "")?>">ｲﾝｽﾘﾝ<?=($weekly_insulin_header[$i0] ? "：".$weekly_insulin_header[$i0]: "")?></th>

    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareInsulin.popup(<?=$WYL[$ii]?>)" title="ｲﾝｽﾘﾝ<?=($weekly_insulin_header[$i0] ? "：".$weekly_insulin_header[$i0]: "")?>">

            <? if ($weekly_insulin_header[$i0]=="定期") { ?>
                <div class="o_abs"<?=$ww1?>><?=wbr(@$care_list["insulin_teiki"][$WYL[$ii]])?></div>
            <? } ?>

            <? if ($weekly_insulin_header[$i0]=="BS測定値") { ?>
            <?
                for ($iii=0; $iii<count($care_list["insulin_bs"][$WYL[$ii]]); $iii++) {
                    $bs_entry = $care_list["insulin_bs"][$WYL[$ii]][$iii];
            ?>
                <table cellspacing="0" cellpadding="0" class="o_child">
                <tr id="otr_<?=++$tridx?>"><td class="borderb wp50"><div class="o_abs ww2"<?=$ww2?>><?=wbr($bs_entry["bs_hm"])?></div></td>
                <td class="borderl borderb wp50"><div class="o_abs ww2"<?=$ww2?>><?=wbr($bs_entry["bs_upper"])?></div></td></tr>
                </table>

                <table cellspacing="0" cellpadding="0" class="o_child">
                <? if ($bs_entry["bs_name1"] || $bs_entry["bs_ryou1"]) { ?>
                    <tr id="otr_<?=++$tridx?>"><td class="borderb wp70"><div class="o_abs ww07"<?=$ww08?>><?=wbr($bs_entry["bs_name1"])?></div></td>
                    <td class="borderb borderl wp30"><div class="o_abs ww03"<?=$ww02?>><?=wbr($bs_entry["bs_ryou1"])?></div></td></tr>
                <? } ?>
                <? if ($bs_entry["bs_name2"] || $bs_entry["bs_ryou2"]) { ?>
                    <tr id="otr_<?=++$tridx?>"><td class="borderb wp70"><div class="o_abs ww07"<?=$ww08?>><?=wbr($bs_entry["bs_name2"])?></div></td>
                    <td class="borderb borderl wp30"><div class="o_abs ww03"<?=$ww02?>><?=wbr($bs_entry["bs_ryou2"])?></div></td></tr>
                <? } ?>
                </table>
                <? }  // end for ?>
            <? } // end if ?>

            <? if ($weekly_insulin_header[$i0]=="食事量") { ?>
                <table cellspacing="0" cellpadding="0" class="o_child fontsize11"><tr id="otr_<?=++$tridx?>">
                <td class="wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr(@$care_list["insulin_sr_asa"][$WYL[$ii]])?></div></td>
                <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr(@$care_list["insulin_sr_hiru"][$WYL[$ii]])?></div></td>
                <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr(@$care_list["insulin_sr_yu"][$WYL[$ii]])?></div></td>
                </tr></table>
            <? } ?>

    </td>
    <? } ?>
</tr>
<? } ?>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th class="th_color2" navititle="便 合計" navi="登録数は空行を含みます。">
        <table cellspacing="0" cellpadding="0" class="o_child"><tr>
            <td class="left">便</td>
            <td class="right">合計</td>
        </tr></table>
    </th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="便 合計">
        <table cellspacing="0" cellpadding="0" class="o_child"><tr id="otr_<?=++$tridx?>">
            <? if ($care_list["ben__ttl_ryou"][$WYL[$ii]]=="無し"){ ?>
                <td><div class="o_abs"<?=$ww1?>>無し</div></td>
            <? } else { ?>
                <td class="wp50"><div class="o_abs ww2"<?=$ww2?>><?=$care_list["ben__field_count"][$WYL[$ii]]?></div></td>
                <td class="borderl wp50"><div class="o_abs ww2"<?=$ww2?>><?=$care_list["ben__ttl_ryou"][$WYL[$ii]]?></div></td>
            <? } ?>
        </tr></table>
    </td>
    <? } ?>
</tr>
<? if ($weekly_ben_ari) { ?>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th class="right th_color2" navititle="便 性状・量" title="空行を含めて表示します。<br>便のグラム数が指定されていない場合は、ゼロを表示します。">性状・量（g）</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="便 性状・量">
        <table cellspacing="0" cellpadding="0" class="o_child">
        <?
            $cnt = count((array)@$care_list["ben"][$WYL[$ii]]);
            for ($iii=0; $iii<$cnt; $iii++) {
                $item = $care_list["ben"][$WYL[$ii]][$iii];
        ?>
            <tr<?=($iii>0 ? ' class="bordert"':"")?> id="otr_<?=++$tridx?>">
                <? if ($item["ben_nasi"]) { ?>
                    <td><div class="o_abs"<?=$ww1?>>無し</div></td>
                <? } else { ?>
                    <td class="wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr(get_alias($item["ben_benryou"]))?></div></td>
                    <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr(get_alias($item["ben_type"]))?></div></td>
                    <td class="borderl wp33"><div class="o_abs ww4"<?=$ww3?>><?=$item["ryou"]?></div></td>
                <? } ?>
            </tr>
            <? } ?>
        </table>
    </td>
    <? } ?>
</tr>
<? } ?>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th class="th_color2" navititle="尿 時間" navi="ダイアログ上での「尿量」−「時間尿」を表示します。">
        <table cellspacing="0" cellpadding="0" class="o_child"><tr>
            <td class="left">尿</td>
            <td class="right">時間（mL）</td>
        </tr></table>
    </th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="尿量 時間量">
        <table cellspacing="0" cellpadding="0" class="o_child">
        <?
            $cnt = count((array)@$care_list["nyou_list"][$WYL[$ii]]);
            for ($iii=0; $iii<$cnt; $iii++) {
                $item = $care_list["nyou_list"][$WYL[$ii]][$iii];
        ?>
            <tr<?=($iii>0 ? ' class="bordert"':"")?> id="otr_<?=++$tridx?>">
                <td class="wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($item["nyou_hm"])?></div></td>
                <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($item["nyou_ryou"])?></div></td>
                <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($item["nyou_more"])?></div></td>
            </tr>
            <? } ?>
        </table>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th class="right th_color2" navititle="尿 一日">一日（mL）</th>
    <?
        for ($ii=0; $ii<$day_size; $ii++){
            $day_nyou = $care_list["day_nyou"][$WYL[$ii]];
    ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="尿 一日">
        <div class="o_abs"<?=$ww1?>><?=wbr($day_nyou)?></div>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th class="right th_color2" navititle="尿 コメント">コメント</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="尿 コメント">
        <div class="o_abs"<?=$ww1?>><?=wbr($care_list["day_nyou_comment"][$WYL[$ii]])?></div>
    </td>
    <? } ?>
</tr>



<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th class="th_color2" navititle="排液 種類">
        <table cellspacing="0" cellpadding="0" class="o_child"><tr><td class="left">排液</td><td class="right">種類</td></tr></table></th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
        <? $haieki_total = $care_list["haieki_total"][$WYL[$ii]]; ?>
        <? if ($haieki_total!="") $haieki_total = "／".$haieki_total."mL"; ?>
        <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="排液 種類">
            <table cellspacing="0" cellpadding="0" class="o_child"><tr id="otr_<?=++$tridx?>">
            <td class="wp20"><div class="o_abs ww02"<?=$ww02?>><?=wbr($care_list["haieki_syurui"][$WYL[$ii]])?></div></td>
            <td class="borderl wp80"><div class="o_abs ww08"<?=$ww08?>><?=wbr($care_list["haieki_syurui_comment"][$WYL[$ii]])?></div></td>
            </tr></table>
        </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th class="right th_color2" navititle="排液 時間排液量">量（mL）</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="排液 量">
        <table cellspacing="0" cellpadding="0" class="o_child">
        <?
        $cnt = count((array)@$care_list["haieki_list"][$WYL[$ii]]);
        for ($iii=0; $iii<$cnt; $iii++) {
            $item = $care_list["haieki_list"][$WYL[$ii]][$iii];
        ?>
            <tr<?=($iii>0 ? ' class="bordert"':"")?> id="otr_<?=++$tridx?>">
                <td class="wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($item["haieki_hm"])?></div></td>
                <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($item["haieki_ryou"])?></div></td>
                <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($item["haieki_more"])?></div></td>
            </tr>
        <? } ?>
        </table>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th class="right th_color2" navititle="排液 一日量">一日（mL）</th>
    <?
        for ($ii=0; $ii<$day_size; $ii++){
            $haieki_day_ryou = $care_list["haieki_day_ryou"][$WYL[$ii]];
    ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="排液 一日">
        <div class="o_abs"<?=$ww1?>><?=wbr($haieki_day_ryou)?></div>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th class="right th_color2" navititle="排液 コメント">コメント</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="排液 コメント">
        <div class="o_abs"<?=$ww1?>><?=wbr($care_list["haieki_day_comment"][$WYL[$ii]])?></div>
    </td>
    <? } ?>
</tr>


<? if ($haieki2_exist) { ?>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th class="th_color2" navititle="排液２ 種類">
        <table cellspacing="0" cellpadding="0" class="o_child"><tr><td class="left">排液２</td><td class="right">種類</td></tr></table></th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
        <? $haieki2_total = $care_list["haieki2_total"][$WYL[$ii]]; ?>
        <? if ($haieki2_total!="") $haieki2_total = "／".$haieki2_total."mL"; ?>
        <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="排液２ 種類">
            <table cellspacing="0" cellpadding="0" class="o_child"><tr id="otr_<?=++$tridx?>">
            <td class="wp20"><div class="o_abs ww02"<?=$ww02?>><?=wbr($care_list["haieki2_syurui"][$WYL[$ii]])?></div></td>
            <td class="borderl wp80"><div class="o_abs ww08"<?=$ww08?>><?=wbr($care_list["haieki2_syurui_comment"][$WYL[$ii]])?></div></td>
            </tr></table>

        </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th class="right th_color2" navititle="排液２ 時間排液量">量（mL）</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="排液 量">
        <table cellspacing="0" cellpadding="0" class="o_child">
        <?
        $cnt = count((array)@$care_list["haieki2_list"][$WYL[$ii]]);
        for ($iii=0; $iii<$cnt; $iii++) {
            $item = $care_list["haieki2_list"][$WYL[$ii]][$iii];
        ?>
            <tr<?=($iii>0 ? ' class="bordert"':"")?> id="otr_<?=++$tridx?>">
                <td class="wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($item["haieki_hm"])?></div></td>
                <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($item["haieki_ryou"])?></div></td>
                <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($item["haieki_more"])?></div></td>
            </tr>
        <? } ?>
        </table>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th class="right th_color2" navititle="排液２ 一日量">一日（mL）</th>
    <?
        for ($ii=0; $ii<$day_size; $ii++){
            $haieki2_day_ryou = $care_list["haieki2_day_ryou"][$WYL[$ii]];
    ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="排液２ 一日">
        <div class="o_abs"<?=$ww1?>><?=wbr($haieki2_day_ryou)?></div>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th class="right th_color2" navititle="排液２ コメント">コメント</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="排液２ コメント">
        <div class="o_abs"<?=$ww1?>><?=wbr($care_list["haieki2_day_comment"][$WYL[$ii]])?></div>
    </td>
    <? } ?>
</tr>
<? } ?>


<? if ($haieki3_exist) { ?>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th class="th_color2" navititle="排液３ 種類">
        <table cellspacing="0" cellpadding="0" class="o_child"><tr><td class="left">排液３</td><td class="right">種類</td></tr></table></th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
        <? $haieki3_total = $care_list["haieki3_total"][$WYL[$ii]]; ?>
        <? if ($haieki3_total!="") $haieki3_total = "／".$haieki3_total."mL"; ?>
        <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="排液３ 種類">
            <table cellspacing="0" cellpadding="0" class="o_child"><tr id="otr_<?=++$tridx?>">
            <td class="wp20"><div class="o_abs ww02"<?=$ww02?>><?=wbr($care_list["haieki3_syurui"][$WYL[$ii]])?></div></td>
            <td class="borderl wp80"><div class="o_abs ww08"<?=$ww08?>><?=wbr($care_list["haieki3_syurui_comment"][$WYL[$ii]])?></div></td>
            </tr></table>
        </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th class="right th_color2" navititle="排液３ 時間排液量">量（mL）</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="排液 量">
        <table cellspacing="0" cellpadding="0" class="o_child">
        <?
        $cnt = count((array)@$care_list["haieki3_list"][$WYL[$ii]]);
        for ($iii=0; $iii<$cnt; $iii++) {
            $item = $care_list["haieki3_list"][$WYL[$ii]][$iii];
        ?>
            <tr<?=($iii>0 ? ' class="bordert"':"")?> id="otr_<?=++$tridx?>">
                <td class="wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($item["haieki_hm"])?></div></td>
                <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($item["haieki_ryou"])?></div></td>
                <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($item["haieki_more"])?></div></td>
            </tr>
        <? } ?>
        </table>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th class="right th_color2" navititle="排液３ 一日量">一日（mL）</th>
    <?
        for ($ii=0; $ii<$day_size; $ii++){
            $haieki3_day_ryou = $care_list["haieki3_day_ryou"][$WYL[$ii]];
    ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="排液３ 一日">
        <div class="o_abs"<?=$ww1?>><?=wbr($haieki3_day_ryou)?></div>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th class="right th_color2" navititle="排液３ コメント">コメント</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareOut.popup(<?=$WYL[$ii]?>)" title="排液３ コメント">
        <div class="o_abs"<?=$ww1?>><?=wbr($care_list["haieki3_day_comment"][$WYL[$ii]])?></div>
    </td>
    <? } ?>
</tr>
<? } ?>



<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th style="background-color:#dddddd">【年月日】</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td style="background-color:#dddddd; text-align:center">
    <?
        $a_ymd = ymdhm_to_array($WYL[$ii]);
        echo $a_ymd["slash_mdw"];
    ?>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th navititle="下剤">下剤</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareKensanado.popup(<?=$WYL[$ii]?>)" title="下剤">
        <table cellspacing="0" cellpadding="0" class="o_child">
        <?
            $cnt = count((array)@$care_list["gezai_list"][$WYL[$ii]]);
            for ($iii=0; $iii<$cnt; $iii++) {
                $ary = $care_list["gezai_list"][$WYL[$ii]][$iii];
        ?>
            <tr<?=($iii>0 ? ' class="bordert"':"")?> id="otr_<?=++$tridx?>">
        <td class="wp40"><div class="o_abs ww04"<?=$ww04?>><?=wbr($ary["gezai_hm"])?></div></td>
        <td class="borderl wp60"><div class="o_abs ww06"<?=$ww06?>><?=wbr(get_alias(@$ary["gezai_med_name"]))?></div></td>
        </tr>
        <? }?>
        </table>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th navititle="座薬">座薬</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareKensanado.popup(<?=$WYL[$ii]?>)" title="座薬">
        <table cellspacing="0" cellpadding="0" class="o_child">
        <?
            $cnt = count((array)@$care_list["zayaku_list"][$WYL[$ii]]);
            for ($iii=0; $iii<$cnt; $iii++) {
                $ary = $care_list["zayaku_list"][$WYL[$ii]][$iii];
        ?>
            <tr<?=($iii>0 ? ' class="bordert"':"")?> id="otr_<?=++$tridx?>">
        <td class="wp40"><div class="o_abs ww04"<?=$ww04?>><?=wbr(@$ary["zayaku_hm"])?></div></td>
        <td class="borderl wp60"><div class="o_abs ww06"<?=$ww06?>><?=wbr(get_alias(@$ary["zayaku_med_name"]))?></div></td>
        </tr>
        <? } ?>
        </table>
    </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th navititle="発赤">発赤</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
        <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareKensanado.popup(<?=$WYL[$ii]?>)" title="発赤">
            <div class="o_abs"<?=$ww1?>><?=wbr(@$care_list["hosseki"][$WYL[$ii]])?></div>
        </td>
    <? } ?>
</tr>
<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th navititle="検査">検査</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareKensanado.popup(<?=$WYL[$ii]?>)" title="検査">
        <div class="o_abs"<?=$ww1?>>
        <? $exist = ($care_list["kensa_names_jp"][$WYL[$ii]] && $care_list["kensa_other_comment"][$WYL[$ii]] ? 1 : 0); ?>
        <?=wbr(@$care_list["kensa_names_jp"][$WYL[$ii]])?>
        <div <?=($exist ? ' class="bordert"' : "")?>>
            <?=wbr(@$care_list["kensa_other_comment"][$WYL[$ii]])?>
        </div>
        </div>
    </td>
    <? } ?>
</tr>

<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th class="th_color2" style="text-align:left" navititle="観察項目" navi="入力された項目があれば行が追加表示されます。<br>週内に（単日表示時は対象日に）深夜/日勤/準夜のいずれかに値を指定された項目が、行表示対象となります。<br>観察項目行の表示順は、「月曜の観察項目」〜「日曜の観察項目」順です。各曜日内では、マスタ登録順です。">≪ 観察項目 ≫</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareKansatu.popup(<?=$WYL[$ii]?>)" title="観察項目">
        <table cellspacing="0" cellpadding="0" class="o_child"><tr id="otr_<?=++$tridx?>">
        <td class="wp33"><div class="o_abs ww3"<?=$ww3?>>深夜</div></td>
        <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>>日勤</div></td>
        <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>>準夜</div></td>
        </tr></table>
    </td>
    <? } ?>
</tr>

<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th navititle="SOAP" navi="表示内容、および列をクリックして表示される「SOAP」ダイアログは、「SOAP」画面から呼び出されるものと同じものです。<br>最終更新者はダイアログ下部に表示されますが、他のケア関連ダイアログのように、更新履歴を参照することはできません。">SOAP</th>
    <?
        for ($ii=0; $ii<$day_size; $ii++){
            $soap_list = $siji_list["soap"][$WYL[$ii]];
            if (!is_array($soap_list)) $soap_list = array();
            //ksort($soap_list);
    ?>
        <td style="height:22px" class="siji_cell<?=$taiin_classname[$ii]?>" onmouseover="$(this).addClass('hover').addClass('hover1')"
            onmouseout="$(this).removeClass('hover').removeClass('hover1')" title="SOAP"
            onclick="stopEvent(event); dfmSijibanSoap.popup('soap', ptif_id,'<?=$WYL[$ii]?>',0, '')">
        <? foreach ($soap_list as $soap_row) { ?>
                <div class="o_abs left borderb hover siji_cell<?=$taiin_classname[$ii]?>"<?=$ww1?>
                    onclick="stopEvent(event); dfmSijibanSoap.popup('soap', ptif_id,'<?=$WYL[$ii]?>',<?=$soap_row["field_idx"]?>, '')"
                    onmouseover="this.parentNode.parentNode.className='hover_tr'; return stopEvent(event);">
                <?
	                $hm = trim(@$soap_row["action_hm"]);
	                $soap_jikantai = trim(@$soap_row["soap_jikantai"]);
	                $jikantai = "";
	                if ($soap_jikantai=="nikkin") $jikantai = '<span class="nikkin">日</span>';
	                if ($soap_jikantai=="junya") $jikantai = '<span class="junya">準</span>';
	                if ($soap_jikantai=="sinya") $jikantai = '<span class="sinya">深</span>';
	                if (strlen($hm)==4) $hm = substr($hm,0,2).":".substr($hm,2);
	                if ($hm!="") $hm = '<span class="action_hm">'.$hm.'</span>';
                ?>
                <?=$hm.$jikantai.nl2br2(wbr($soap_row["siji_text"]))?>
                <? if ($soap_row["siji_ptext"]!="") {?>
                <br><span class="action_hm">P</span>
                <?=nl2br2(wbr($soap_row["siji_ptext"]))?>
                <? } ?>
                <br/>
            </div>
        <? } ?>
        <div class="new_entry"><div>【新規作成】</div></div>
        </td>
    <? } ?>
</tr>

<? for ($i0=0; $i0<count($weekly_kansatu_header); $i0++) { ?>
<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th class="th_color2" style="text-align:right" navititle="観察項目 <?=@$weekly_kansatu_header[$i0]?>" navi="週内に（単日表示時は対象日に）深夜/日勤/準夜のいずれかに値が指定されているため、表示対象となっています。"><?=@$weekly_kansatu_header[$i0]?></th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareKansatu.popup(<?=$WYL[$ii]?>)" title="観察項目 <?=@$weekly_kansatu_header[$i0]?>">
        <table cellspacing="0" cellpadding="0" class="o_child"><tr id="otr_<?=++$tridx?>">
        <td class="wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr(@$care_list["kansatu"][$WYL[$ii]][$weekly_kansatu_header[$i0]]["sinya"])?></div></td>
        <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr(@$care_list["kansatu"][$WYL[$ii]][$weekly_kansatu_header[$i0]]["nikkin"])?></div></td>
        <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr(@$care_list["kansatu"][$WYL[$ii]][$weekly_kansatu_header[$i0]]["junya"])?></div></td>
        </tr></table>
    </td>
    <? } ?>
</tr>
<? } ?>

<tr onmouseover="className='hover_tr';" onmouseout="className='';" id="otr_<?=++$tridx?>">
    <th navititle="最終確認者" navi="各列をクリックして表示される「ケア記入者履歴」ダイアログのうち、最新日時の職員名が表示されます。<br>ケア記入者（最終確認者）は、「注射処方等指示」を除いた各ダイアログで、「保存」または「削除」ボタンを押下したタイミングで記録されます。<br>ダイアログで何も変更せず「保存」を行っても、ケア記入者（最終確認者）が記録されるようになっています。">最終確認者</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmUpdateHistory.popup(<?=$WYL[$ii]?>)" title="最終確認者">
        <div class="o_abs"<?=$ww1?>><?=wbr(@$update_history[$WYL[$ii]])?></div>
    </td>
    <? } ?>
</tr>


<tr onmouseover="className='hover_tr';" onmouseout="className='';">
    <th navititle="サイン" navi="">サイン</th>
    <? for ($ii=0; $ii<$day_size; $ii++){ ?>
    <td class="hover cell<?=$taiin_classname[$ii]?>" onclick="dfmCareSign.popup(<?=$WYL[$ii]?>)" title="サイン">
        <table cellspacing="0" cellpadding="0" class="o_child"><tr id="otr_<?=++$tridx?>">
        <td class="wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($care_list["sign"][$WYL[$ii]]["sinya"])?></div></td>
        <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($care_list["sign"][$WYL[$ii]]["nikkin"])?></div></td>
        <td class="borderl wp33"><div class="o_abs ww3"<?=$ww3?>><?=wbr($care_list["sign"][$WYL[$ii]]["junya"])?></div></td>
        </tr></table>
    </td>
    <? } ?>
</tr>



</tbody>
</table>


<div id="ondoban_siji" style="display:none; <? if ($stickySijibar){ ?>position:fixed; top:<?=($stickyMenubar?"59":"35")?>px; right:1px<?} else { ?>position:absolute; top:<?=($stickyMenubar?"35":"3")?>px;<? } ?>; width:114px; z-index:80;"><?=implode("\n",$siji_html)?></div>


<?
}


function make_siji_right_field_block($section, $obj, $index) {
    global $marumoji_list;
    $field_count = (int)@$obj[$section."__field_count"];
    $field_jp = "";
    if ($section=="freeword") { $field_jp = "ﾌﾘｰﾜｰﾄﾞ1"; $funcname = "dfmSijiFreeword"; }
    if ($section=="free2word") { $field_jp = "ﾌﾘｰﾜｰﾄﾞ2"; $funcname = "dfmSijiFree2word"; }
    if ($section=="keikan") { $field_jp = "経管"; $funcname = "dfmSijiKeikan"; }
    if ($section=="keikou") { $field_jp = "経口"; $funcname = "dfmSijiKeikou"; }
    if ($section=="keikei") { $field_jp = "補食"; $funcname = "dfmSijiKeiKei"; }
    if ($section=="insulin_fix") { $field_jp = "ｲﾝｽﾘﾝ固"; $funcname = "dfmSijiInsulinFix"; }
    if ($section=="bsscale") { $field_jp = "BSｽｹｰﾙ"; $funcname = "dfmSijiBSScale"; }
    if ($section=="insulin_sr") { $field_jp = "ｲﾝｽﾘﾝ食"; $funcname = "dfmSijiInsulinSR"; }
    if ($section=="gezai")  { $field_jp = "下剤"; $funcname = "dfmSijiGezai"; }

    $html = array();
    $html[] = '<div id="siji_'.$section.'_field'.$index.'_wrap">';
    if (!$obj) {
        $html[] = '<table cellspacing="0" cellpadding="0" class="siji_hist"><tr>';
        $html[] = '<th style="color:#aaaaff">'.$field_jp.'</th>';
        $html[] = '<td style="color:#cccccc">(なし)</td>';
        $html[] = '</tr></table></div>';
    } else {
        $html[] = '<table cellspacing="0" cellpadding="0" class="siji_hist"><tr>';
        $html[] = '<th><a class="block1" href="javascript:void(0)" onclick="$(\'#siji_'.$section.'_field'.$index.'\').toggle()"><span>'.$field_jp.'</span></a></th>';
        $html[] = '<td><a class="block1" href="javascript:void(0)" onclick="'.$funcname.'.popup(\''.$obj["ymd"].'\')"><span>'.$obj["slash_md"].'</span></a></td>';
        $html[] = '</tr></table></div>';
        $html[] = '<div id="siji_'.$section.'_field'.$index.'" style="display:none; padding:0 2px; border-bottom:1px solid #88caff">';

        if ($section=="gezai") {
			$gezai_exist = 0;
            for ($idx=1; $idx<=$field_count; $idx++) {
                $html[]= wbr(get_alias($obj["gezai__".$idx."__med_name"])).'<em>：</em>'.wbr($obj["gezai__".$idx."__text"]);
                if ($idx<$field_count) $html[] = "<hr>";
                $gezai_exist = 1;
            }
            if ($obj["gezai_bikou"]) {
				if ($gezai_exist) $html[]= "<hr>";
				$html[]= '<em>備</em>'.wbr($obj["gezai_bikou"]);
			}
        }
        if ($section=="freeword") {
            if ($obj["siji_naiyou"]) $html[]= ($field_count>0?"<hr>":"").'<em>内容</em>'.wbr($obj["siji_naiyou"]);
        }
        if ($section=="free2word") {
            if ($obj["siji_naiyou"]) $html[]= ($field_count>0?"<hr>":"").'<em>内容</em>'.wbr($obj["siji_naiyou"]);
        }
        if ($section=="keikan") {
            for ($idx=1; $idx<=$field_count; $idx++) {
                $html[]= wbr(get_alias($obj["eiyouzai".$idx]));
                $html[]= '<em>特記'.$idx.'</em>'.wbr($obj["tokki".$idx]);
                if ($idx<$field_count) $html[] = "<hr>";
            }
            if ($obj["bikou"]) $html[]= ($field_count>0?"<hr>":"").'<em>備</em>'.wbr($obj["bikou"]);
        }
        if ($section=="keikou") {
            if ($obj["syusyoku"]!="") $html[]= '<em>主</em>'.wbr(get_alias($obj["syusyoku"]));
            if ($obj["fukusyoku"]!="") $html[]= '<em>副</em>'.wbr(get_alias($obj["fukusyoku"]));
            if ($obj["syokusyu"]!="") $html[]= '<em>種</em>'.wbr(get_alias($obj["syokusyu"]));
            if ($obj["bikou"]!="") $html[]= '<em>備</em>'.wbr($obj["bikou"]);
        }
        if ($section=="keikei") {
            if ($obj["kinsi_syokuhin"]!="") $html[]= '<em>禁</em>'.wbr(get_alias($obj["kinsi_syokuhin"]));
            if ($obj["warfarin"]) $html[]= '<em>'.wbr("[WF対応]").'</em>';
            if ($obj["oyatsu"]!="") $html[]= '<em>補</em>'.wbr(get_alias($obj["oyatsu"]));
            if ($obj["bikou"]!="") $html[]= '<em>備</em>'.wbr($obj["bikou"]);
        }
        if ($section=="insulin_fix") {
            if ($obj["timing"]) $html[]= "[".wbr(get_alias($obj["timing"]))."]";
            for ($idx=1; $idx<=$field_count; $idx++) {
                $tmp = array();
                $med_name = @$obj["insulin_fix__".$idx."__med_name"];
                if ($med_name!="") $tmp[]= "<em>◆</em>".wbr(get_alias(@$obj["insulin_fix__".$idx."__med_name"]));
                if (@$obj["insulin_fix__".$idx."__asa"]!="") $tmp[]= '<em>朝</em>'.wbr($obj["insulin_fix__".$idx."__asa"]);
                if (@$obj["insulin_fix__".$idx."__hiru"]!="") $tmp[]= '<em>昼</em>'.wbr($obj["insulin_fix__".$idx."__hiru"]);
                if (@$obj["insulin_fix__".$idx."__yu"]!="") $tmp[]= '<em>夕</em>'.wbr($obj["insulin_fix__".$idx."__yu"]);
                for ($idx2=4; $idx2<=7; $idx2++) {
                    $when = @$obj["when".$idx2];
                    if (!$when) $when = $marumoji_list[$idx2];
                    if (@$obj["insulin_fix__".$idx."__ryou".$idx2]!="") $tmp[]= '<em>'.wbr($when).'</em>'.wbr($obj["insulin_fix__".$idx."__ryou".$idx2]);
                }
                if (count($tmp)) {
                    $html[]= implode("", $tmp);
                }
            }
            if ($obj["insulin_fix_bikou"]!="") $html[]= "<br><em>備</em>".wbr($obj["insulin_fix_bikou"]);
        }
        if ($section=="bsscale") {
			$lastUpper = 0;
            for ($idx=1; $idx<=$field_count; $idx++) {
                $upp = @$obj["bsscale__".$idx."__bs_upper"];
                $nm1 = @$obj["bsscale__".$idx."__bs_name1"];
                $nm2 = @$obj["bsscale__".$idx."__bs_name2"];
                $ry1 = @$obj["bsscale__".$idx."__bs_ryou1"];
                $ry2 = @$obj["bsscale__".$idx."__bs_ryou2"];
                if ($upp=="" && $nm1=="" && $nm2=="") continue;
                $tmp = array();
                if ($upp!="" && $upp!="undefined") $html[] = '<em>[〜'.wbr($upp)."]</em>";
                else if ($idx==$field_count && $lastUpper>0) $html[]='<em>['.($lastUpper+1)."〜]</em>";
                if ($nm1!="") $tmp[] = wbr($nm1)."=".wbr($ry1);
                if ($nm2!="") $tmp[] = wbr($nm2)."=".wbr($ry2);
                $html[]= implode("<em>／</em>", $tmp);
                if ($idx<$field_count) $html[] = "<hr>";
                $lastUpper = (int)$upp;
            }
            if ($obj["bsscale_bikou"]!="") {
				if (count($html)) $html[] = "<hr>";
				$html[]= "<em>備</em>".wbr($obj["bsscale_bikou"]);
			}
        }
        if ($section=="insulin_sr") {
            $existed = 0;
            if ($obj["med_name"]!="") { $html[] = wbr($obj["med_name"]); $existed = 1; }
            $field_count = (int)$obj["insulin_sr__field_count"];
            for ($idx=1; $idx<=$field_count; $idx++) {
                $ryou = @$obj["insulin_sr__".$idx."__ryou"];
                $asa = @$obj["insulin_sr__".$idx."__asa"];
                $hiru = @$obj["insulin_sr__".$idx."__hiru"];
                $yu = @$obj["insulin_sr__".$idx."__yu"];
                if ($ryou=="" && $asa=="" && $hiru=="" && $yu=="") continue;
                $tmp = array();
                if ($asa!="") $tmp[] = '<em>朝</em>'.wbr($asa);
                if ($hiru!="") $tmp[] = '<em>昼</em>'.wbr($hiru);
                if ($yu!="") $tmp[] = '<em>夕</em>'.wbr($yu);
                if ($ryou!="" || count($tmp)) {
                    if ($existed) $html[] = "<hr>";
                    $html[] = wbr($ryou).($tmp?":":"").implode("", $tmp);
                    $existed = 1;
                }
            }
            if ($obj["insulin_sr_bikou"]) {
                if ($existed) $html[] = "<hr>";
				$html[]= "<em>備</em>".wbr($obj["insulin_sr_bikou"]);
			}
        }


        $html[] = '</div>';
    }

    return implode("\n", $html);
}
