<?php
    define("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING", 1);
    require_once("ks_common.php");
    require_once("ks_ajax.php");


    $ptif_id_list = array();
    $sel = dbFind("select ptif_id, ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst");
    while($row = pg_fetch_array($sel)) $ptif_id_list[] = $row["ptif_id"];


    $errors = array();
    $ok_list = array();
    $cnt = 0;


    if ($_REQUEST["import_data_type"]=="pat_nyuin_taiin") {

        // CSV全行
        $readLines = explode("\n",mb_convert_encoding(file_get_contents($_FILES['import_file']['tmp_name']), GG_CS_EUCJP, "sjis-win"));

        // ループ開始
        $ptif_infos = array();
        foreach ($readLines as $line) {
            $cnt++;
            if (trim($line)=="") continue;

            $err = "【".$cnt."行目】";
            list($ptif_id, $nyuin_ymd, $taiin_ymd) = explode(",", str_replace('"', '', trim($line)));
            if ($ptif_id=="") { $errors[]= $err."患者IDが空値です。"; continue; }
            if (!in_array($ptif_id, $ptif_id_list, true)) { $errors[]= $err."指定した患者IDは存在しません。"; continue; }

            if ($nyuin_ymd=="") { $errors[]= $err."入院日が空値です。"; continue; }
            if (strlen($nyuin_ymd)!=8 || slash_ymd($nyuin_ymd)=="") { $errors[]= $err."入院日が不正です。"; continue; }

            if ($taiin_ymd!="") {
                if (strlen($taiin_ymd)!=8 || slash_ymd($taiin_ymd)=="") { $errors[]= $err."退院日が不正です。"; continue; }
                if ($nyuin_ymd > $taiin_ymd) { $errors[]= $err."入院日より退院日が過去です。"; continue; }
            }
            $ptif_infos[$ptif_id][] = $nyuin_ymd."_".$taiin_ymd."_".$cnt;
        }

        // 入院日昇順ソート
        foreach ($ptif_infos as $ptif_id => $nyutai_info) {
            sort($ptif_infos[$ptif_id]);
        }

        foreach ($ptif_infos as $ptif_id => $rows) {
            for ($idx=1; $idx<=count($rows); $idx++) {
                list($nyuin_ymd, $taiin_ymd, $cnt) = explode("_", $rows[$idx-1]);
                list($nyuin_ymd2, $taiin_ymd2, $cnt2) = explode("_", @$rows[$idx]."__");
                $err = "【".$cnt."行目】";
                if (!$taiin_ymd && $nyuin_ymd2) { $errors[] = $err."退院日が指定されていませんが、".$cnt2."行目に次の入院日が指定されています。"; continue; }
                if ($taiin_ymd && $nyuin_ymd2) {
                    $tmp = $err."この入院日（".$nyuin_ymd."から）の次の入院日(".$cnt2."行目：".$nyuin_ymd2."から)";
                    if ($taiin_ymd == $nyuin_ymd2) { $errors[] = $tmp."は、この退院日（".$taiin_ymd."）と同じにできません。"; continue; }
                    if ($taiin_ymd > $nyuin_ymd2) { $errors[] = $tmp."が、この退院日（".$taiin_ymd."）より過去です。"; continue; }
                }
            }
        }

        // エラーが１件もなければ登録
        if (!count($errors)) {
            foreach ($ptif_infos as $ptif_id => $rows) {
                $nyuin_taiin_list = array();
                for ($idx=1; $idx<=count($rows); $idx++) {
                    list($nyuin_ymd, $taiin_ymd, $cnt) = explode("_", $rows[$idx-1]);
                    $nyuin_taiin_list[] = $nyuin_ymd."-".$taiin_ymd;
                    dbExec("delete from sum_ks_mst_pat_nyuin_taiin where ptif_id = ".dbES($ptif_id));
                    $sql =
                    " insert into sum_ks_mst_pat_nyuin_taiin ( ptif_id, nyuin_ymd, taiin_ymd ) values (".
                    " ".dbES($ptif_id).",".dbES($nyuin_ymd).",".dbES($taiin_ymd).")";
                    dbExec($sql);
                    $ok_list[]= (count($ok_list)+1) . "．".$ptif_id."：".$nyuin_ymd."〜".$taiin_ymd;
                }
                if (dbGetOne("select ptif_id from sum_ks_mst_pat_attribute where ptif_id = ".dbES($ptif_id))) {
                    $sql =
                    " update sum_ks_mst_pat_attribute set".
                    " nyuin_taiin_list = ".dbES(implode(",",$nyuin_taiin_list)).
                    " where ptif_id = ".dbES($ptif_id);
                } else {
                    $sql =
                    " insert into sum_ks_mst_pat_attribute ( ptif_id, nyuin_taiin_list ) values (".
                    " ".dbES($ptif_id).",".dbES(implode(",",$nyuin_taiin_list)).")";
                }
                dbExec($sql);
            }
        }
        if (!count($errors) && !count($ptif_infos)) { $errors[]= "登録対象はありません。"; }

    }


    if ($_REQUEST["import_data_type"]=="pat_bldg_ward_ptrm") {

        // 病棟名一覧
        $reg_list = array();
        $ward_names = array();
        $sel = dbFind("select * from sum_ks_mst_bldg_ward");
        while($row = pg_fetch_array($sel)) $ward_names[$row["bldg_cd"]."_".$row["ward_cd"]] = $row["ward_name"];

        // 「病棟名＠病室名」の一覧
        $ward_ptrm_names = array();
        $sel = dbFind("select * from sum_ks_mst_ptrm");
        while($row = pg_fetch_array($sel)) {
            $ward_name = @$ward_names[$row["bldg_cd"]."_".$row["ward_cd"]];
            $row["ward_name"] = $ward_name;
            if ($ward_name) $ward_ptrm_names[$ward_name."\t".$row["ptrm_name"]] = $row;
        }

        // CSV全行
        $readLines = explode("\n",mb_convert_encoding(file_get_contents($_FILES['import_file']['tmp_name']), GG_CS_EUCJP, "sjis-win"));

        // ループ開始
        foreach ($readLines as $line) {
            $cnt++;
            if (trim($line)=="") continue;
            $err = "【".$cnt."行目】";
            list($ptif_id, $ward_name, $ptrm_name) = explode(",", str_replace('"', '', trim($line)));
            if ($ptif_id=="") { $errors[]= $err."患者IDが空値です。"; continue; }
            if ($ward_name=="") { $errors[]= $err."病棟名が空値です。"; continue; }
            if ($ptrm_name=="") { $errors[]= $err."病室名が空値です。"; continue; }
            if (!in_array($ptif_id, $ptif_id_list, true)) { $errors[]= $err."指定した患者IDは存在しません。（".$ptif_id."）"; continue; }
            if (!in_array($ward_name, $ward_names)) { $errors[]= $err."指定した病棟は存在しません。（".$ward_name."）"; continue; }
            $row = $ward_ptrm_names[$ward_name."\t".$ptrm_name];
            if (!$row) { $errors[]= $err."指定した病棟の病室は存在しません。（".$ward_name."@".$ptrm_name."）"; continue; }
            $reg_list[$ptif_id] = $row;
        }
        // エラーが１件もなければ登録
        if (!count($errors)) {
            foreach ($reg_list as $ptif_id => $row) {
                if (dbGetOne("select ptif_id from sum_ks_mst_pat_attribute where ptif_id = ".dbES($ptif_id))) {
                    $sql =
                    " update sum_ks_mst_pat_attribute set".
                    " bldg_cd = ".dbEI($row["bldg_cd"]).
                    ",ward_cd = ".dbEI($row["bldg_cd"]).
                    ",ptrm_room_no = ".dbEI($row["ptrm_room_no"]).
                    " where ptif_id = ".dbES($ptif_id);
                } else {
                    $sql =
                    " insert into sum_ks_mst_pat_attribute ( ptif_id, bldg_cd, ward_cd, ptrm_room_no ) values (".
                    " ".dbES($ptif_id).",".dbEI($row["bldg_cd"]).",".dbEI($row["ward_cd"]).",".dbEI($row["ptrm_room_no"]).")";
                }
                dbExec($sql);
                $ok_list[]= (count($ok_list)+1) . "．".$ptif_id."：".$row["ward_name"]."@".$row["ptrm_name"];
            }
        }
        if (!count($errors) && !count($reg_list)) { $errors[]= "登録対象はありません。正しいインポートファイルを指定したか、確認してください。"; }
    }







?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/start/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" type="text/css" href="css/common.css?v=2">
<style type="text/css">
    table.padding3 td { padding:3px; }
    table.padding3 * { vertical-align:baseline;  }
    #divContent * { font-size:13px; }
</style>
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.js?v=2"></script>
<script type="text/javascript" src="js/common.js?v=2"></script>
<script type="text/javascript" src="js/showpage.js"></script>
<script type="text/javascript">
    function loaded() {
        if (isNotPC) ee("body").className = "is_not_pc";
        else ee("body").className = "is_pc";
    }
</script>
<title>CoMedix | 看護支援 - データインポート</title>
</head>
<body onload="loaded()" id="body">
<form name="frm" action="ks_view_import.php?session=<?=GG_SESSION_ID?>" method="post" enctype="multipart/form-data">




<? //**************************************************************************************************************** ?>
<? // 画面上部 メインメニュー                                                                                         ?>
<? //**************************************************************************************************************** ?>
<div id="header_menu_div">
<table cellspacing="0" cellpadding="0" id="header_menu_table" style="width:100%; height:30px"><tr>
    <td class="weekbtn" style="text-align:center; width:140px; background:url(img/menubtn_on.gif) repeat-x">データインポート</td>
    <td style="width:auto;"></td>
    <td style="width:50px"><a class="menubtn closebtn" href="javascript:void(0)" onclick="window.close();"
        style="border-left:1px solid #88aaee">×</a></td>
</tr></table>
</div>

<div style="padding:10px;" id="divContent">
    <table cellspacing="0" cellpadding="0" class="general_list padding3" style="width:auto"><tr>
        <th>インポート内容</th>
        <td>
        <select name="import_data_type" onchange="changeInstruction()">
            <option value="pat_bldg_ward_ptrm">【患者病室情報CSV】≪Shift_JIS≫ 患者ID，病棟名(必須)，病室名(必須)</option>
            <option value="pat_nyuin_taiin">【患者入退院情報CSV】患者ID，入院日(YYYYMMDD/必須)，退院日(YYYYMMDD)</option>
        </select>
        </td>
    </tr>
    <tr>
        <th>データファイル</th>
        <td><input type="file" name="import_file" /></td>
    </tr>
    </table>
    <div class="instruction" style="padding-top:5px; display:none" id="div_pat_bldg_ward_ptrm">
    ※指定した患者IDに対し、病棟と病室を割り当てます。<br>
    ※患者IDはCoMedix業務メニュー「患者管理」にて事前に登録済のIDのみ指定できます。<br>
    ※病棟名、病室名は「その他メニュー」＞「病棟・病室マスタ」にて事前に登録済の名称のみ指定できます。<br>
    ※カンマ区切りで指定してください。「ダブルクォート文字」「カンマ文字」をデータとして含めることはできません。<br>
    ※患者ID/病棟名/病室名は、文字の全角・半角も一致させる必要があります。<br>
    ※データファイルに含めない患者の病棟/病室に対しては、何も変更されません。<br>
    ※データファイルに不備が１件でもあった場合は、何も登録せず終了します。<br>
    ※本機能の代わりに「指示板」＞患者の「病室」クリック＞「患者附帯情報」ダイアログにて、患者ごとに割当登録が行えます。<br>
    ※本機能では病棟病室情報の解除を行うことはできません。（「患者附帯情報」から解除を行えます）<br>
    ※画面下部「実行ログ」一行目に、「正常に登録が完了しました。」と表示されれば登録完了です。「指示板」画面等で割当結果を確認してください。<br>
    </div>
    <div class="instruction" style="padding-top:5px; display:none" id="div_pat_nyuin_taiin">
    ※指定した患者IDに対し、入退院日を割り当てます。入院日は必須、退院日は任意指定です。<br>
    ※患者IDはCoMedix業務メニュー「患者管理」にて事前に登録済のIDのみ指定できます。<br>
    ※複数回の入退院がある場合は、複数行指定してください。<br>
    ※データファイルに含めない患者の入退院情報に対しては、何も変更されません。<br>
    ※データ保存時、指定された患者IDの入退院データを抹消し、データファイルのものに置き換えられます。（入退院日は置換されます。追加登録されません）<br>
    ※カンマ区切りで指定してください。「ダブルクォート文字」「カンマ文字」をデータとして含めることはできません。<br>
    ※文字はすべて半角で指定してください。<br>
    ※データファイルに不備が１件でもあった場合は、何も登録せず終了します。<br>
    ※本機能の代わりに「指示板」＞患者の「病室」クリック＞「患者附帯情報」ダイアログにて患者ごとに同様の操作が行えます。<br>
    ※本機能では個々の患者の入退院日を完全抹消することはできません。患者ごとに１件以上の入退院情報が必要です。<br>
    &nbsp;&nbsp;&nbsp;（「患者附帯情報」から完全抹消を行えます。）<br>
    ※画面下部「実行ログ」一行目に、「正常に登録が完了しました。」と表示されれば登録完了です。「患者附帯情報」ダイアログで結果を確認してください。<br>
    </div>
    <div style="padding:16px 0"><button type="button" onclick="document.frm.submit()">インポート実行</button></div>

    <div style="padding:20px 0 5px 0">実行ログ</div>
    <?if (count($errors)){?>
        <div style="background-color:#eeeeee; padding:5px; color:#ff0000">登録は行われませんでした。<br><br>◆<?=implode("<br>◆", $errors)?></div>
    <? } else if (count($ok_list)) { ?>
        <div style="background-color:#eeeeee; padding:5px">正常に登録が完了しました。（<?=count($ok_list)?>件）<br><br>◆<?=implode("<br>◆", $ok_list)?></div>
    <? } else { ?>
        <div style="background-color:#eeeeee; padding:5px; height:100px"></div>
    <? } ?>
</div>

<script type="text/javascript">
    function changeInstruction() {
        $('.instruction').hide();
        $('#div_'+getComboValue(document.frm.import_data_type)).show();
    }
    setComboValue(document.frm.import_data_type, "<?=hh($_REQUEST["import_data_type"])?>");
    changeInstruction();
</script>



</form>
</body>
</html>
<?
pg_close($con);
