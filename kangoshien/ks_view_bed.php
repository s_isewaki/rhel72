<?php
    define("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING", 1);
    require_once("ks_common.php");
    require_once("ks_ajax.php");

    if (!$_REQUEST["ymd"]) $_REQUEST["ymd"] = date("Ymd");



    if ($_REQUEST["is_export_csv"]) {
        get_bed_list($_REQUEST["ymd"], $_REQUEST["bldg_ward"], 1);
        die;
    }

//****************************************************************************************************************
// ベッド利用状況一覧
//****************************************************************************************************************
function get_bed_list($ymd, $bldg_ward, $is_export_csv=0) {
    list($bldg_cd, $ward_cd) = explode("_", $bldg_ward);
    $bldg_cd = (int)$bldg_cd;
    if (!$bldg_cd) $bldg_cd = 1;
    $ward_cd = (int)$ward_cd;

    //=================================================
    // 病棟病室単位で集約した入院中患者情報を、先に取得しておく
    //=================================================
    $sql =
    " select * from (".
    " select pt.ptif_id, pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm, pt.ptif_lt_kana_nm, pt.ptif_ft_kana_nm".
    ",opa.bldg_cd, opa.ward_cd, opa.ptrm_room_no, opa.ptif_bikou".
    " from sum_ks_mst_pat_attribute opa".
    " inner join ptifmst pt on (pt.ptif_id = opa.ptif_id)".
    " inner join (".
    "     select ptif_id from sum_ks_mst_pat_nyuin_taiin".
    "     where ( nyuin_ymd is not null and nyuin_ymd <> '' and nyuin_ymd <= ".dbES($ymd).")".
    "     and ( taiin_ymd is null or taiin_ymd = '' or taiin_ymd >= ".dbES($ymd).")".
    "     group by ptif_id".
    " ) nt on ( nt.ptif_id = pt.ptif_id )".
    " where 1 = 1 ".
    " ".($ward_cd ? " and opa.bldg_cd = ".(int)$bldg_cd." and opa.ward_cd = ".(int)$ward_cd : "").
    " ) d".
    " order by bldg_cd, ward_cd, ptrm_room_no, ptif_lt_kana_nm, ptif_ft_kana_nm, ptif_lt_kaj_nm, ptif_ft_kaj_nm";
    $sel = dbFind($sql);
    $ptif_all = array();
    while ($row = @pg_fetch_array($sel)) {
        $key = ((int)$row["bldg_cd"])."_".((int)$row["ward_cd"])."_".((int)$row["ptrm_room_no"]);
        $ptif_name = $row["ptif_lt_kaj_nm"]." ".$row["ptif_ft_kaj_nm"];
        if ($is_export_csv) {
             $ptif_name = sjis($ptif_name);
             if ($row["ptif_bikou"]) $ptif_name .= sjis("／".str_replace("\n"," ",$row["ptif_bikou"]));
        }
        $ptif_all[$key][] = $ptif_name;
    }

    //=================================================
    // 指定週の入院中患者一覧
    //=================================================
    $sql =
    " select bw.ward_name, ptrm.ptrm_name, ptrm.bed_count, bw.bldg_cd, bw.ward_cd, ptrm.ptrm_room_no".
    " from sum_ks_mst_bldg_ward bw".
    " inner join sum_ks_mst_ptrm ptrm on (ptrm.bldg_cd = bw.bldg_cd and ptrm.ward_cd = bw.ward_cd)".
    " where 1 = 1 ".
    " ".($ward_cd ? " and bw.bldg_cd = ".(int)$bldg_cd." and bw.ward_cd = ".(int)$ward_cd : "").
    " order by bw.bldg_cd, bw.ward_cd, ptrm.ptrm_name, ptrm_room_no";
    $sel = dbFind($sql);

    if ($is_export_csv) {
        $csv = array();
        $csv[] = sjis('"病棟","病室","ベッド数","利用数","空床数","ベッド利用患者名／備考"');
        while ($row = @pg_fetch_array($sel)) {
            $line = array();
            $key = ((int)$row["bldg_cd"])."_".((int)$row["ward_cd"])."_".((int)$row["ptrm_room_no"]);
            $ptif_info = $ptif_all[$key];
            if (!is_array($ptif_info)) $ptif_info = array();
            $line[]= '"'. str_replace('"', '""', sjis($row["ward_name"])).'"';
            $line[]= '"'. str_replace('"', '""', sjis($row["ptrm_name"])).'"';
            $line[]= '"'. str_replace('"', '""', sjis($row["bed_count"])).'"';
            $line[]= count($ptif_info);
            $line[]= ((int)$row["bed_count"]) - count($ptif_info);
            $line[]= '"'. str_replace('"', '""', implode("\r\n",$ptif_info)).'"';
            $csv[] = implode(",", $line);
        }
        ob_clean();
        $file_name = "ks_view_bed_ptif_typeA_" . date("Ymd") . ".csv";
        header("Content-Disposition: attachment; filename=$file_name");
        header("Content-Type: application/octet-stream; name=$file_name");
        header("Content-Length: " . strlen(implode("\n",$csv)));
        echo(implode("\r\n",$csv));
        die;
    }



    $html = array();
    $html[]= '<table cellspacing="0" cellpadding="0" class="sijiban_soap_table">';
    $html[] = '<tr><th width="120">病棟</th><th width="120">病室</th><th width="80">�．戰奪豹�</th><th width="80">�⇒�用数</th>';
    $html[]= '<th width="60">空床数<br>（��-�◆�</th><th width="auto" class="left">ベッド利用患者名</th></tr>';
    while ($row = @pg_fetch_array($sel)) {
        $key = ((int)$row["bldg_cd"])."_".((int)$row["ward_cd"])."_".((int)$row["ptrm_room_no"]);
        $ptif_info = $ptif_all[$key];
        if (!is_array($ptif_info)) $ptif_info = array();
        $html[]='<tr>';
        $html[]= '<td>'.hh($row["ward_name"]).'</td>';
        $html[]= '<td>'.hh($row["ptrm_name"]).'</td>';
        $html[]= '<td class="center">'.hh($row["bed_count"]).'</td>';
        $html[]= '<td class="center">'.count($ptif_info).'</td>';
        $kusyo = ((int)$row["bed_count"]) - count($ptif_info);
        if ($kusyo < 0) $html[]= '<td class="center" style="background-color:#ff6666; color:#ffffff">'.$kusyo.'</td>';
        if ($kusyo > 0) $html[]= '<td class="center" style="background-color:#ffffaa;">'.$kusyo.'</td>';
        if ($kusyo == 0) $html[]= '<td class="center">0</td>';
        $html[]= '<td>'.hh(implode("／",$ptif_info)).'</td>';
        $html[]='</tr>';
    }
    $html[]="</table>";
    if (defined("ONDOBAN_AJAX_PHP_USING_AS_INCLUDING")) {
        return implode("\n",$html);
    }
    echo "ok".implode("\n",$html);
}


?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/start/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" type="text/css" href="css/common.css?v=2">
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.js?v=2"></script>
<script type="text/javascript" src="js/common.js?v=2"></script>
<script type="text/javascript" src="js/showpage.js"></script>
<script type="text/javascript">
    function loaded() {
        if (isNotPC) ee("body").className = "is_not_pc";
        else ee("body").className = "is_pc";
    }
</script>
<title>CoMedix | 看護支援 - ベッド利用状況</title>
</head>
<body onload="loaded()" id="body">





<? //**************************************************************************************************************** ?>
<? // 画面上部 メインメニュー                                                                                         ?>
<? //**************************************************************************************************************** ?>
<div id="header_menu_div">
<form id="frm" name="frm" method="get">
<input type="hidden" name="ymd" value="<?=h($_REQUEST["ymd"])?>" />
<input type="hidden" name="session" value="<?=GG_SESSION_ID?>" />
<input type="hidden" name="is_export_csv" value="" />
<table cellspacing="0" cellpadding="0" id="header_menu_table" style="width:100%; height:30px"><tr>
    <td class="weekbtn" style="text-align:center; width:120px; background:url(img/menubtn_on.gif) repeat-x">ベッド利用状況</td>
    <td style="width:auto; padding-left:10px;"><nobr>
        <table cellspacing="0" cellpadding="0" style="width:auto"><tr>
            <td style="padding-left:20px; vertical-align:middle">病棟</td>
            <td style="padding-left:6px; vertical-align:middle;">
            <select name="bldg_ward" onchange="document.frm.is_export_csv.value=''; document.frm.submit()">
            <option value="all">（すべて）</option>
            <?
            $mst_data = get_mst_bldg_ward_ptrm();
            echo implode("\n",$mst_data["bldg_ward_options"]);
            ?>
            </select>
            <script type="text/javascript">
            setComboValue(document.frm.bldg_ward, "<?=js($_REQUEST["bldg_ward"])?>");
            </script>
            </td>
        </tr></table>
    </nobr></td>
    <td width="auto" style="vertical-align:middle; padding-right:10px" class="right">
        対象日：本日時点（<?=date("Y/m/d")?>）
        <button type="button" onclick="document.frm.is_export_csv.value='1'; document.frm.submit()">CSV出力</button>
    </td>
    <td style="width:50px"><a class="menubtn closebtn" href="javascript:void(0)" onclick="window.close();"
        style="border-left:1px solid #88aaee">×</a></td>
</tr></table>
</form>
</div>







<div id="bed_list_container" style="padding-bottom:20px;">
<?=get_bed_list($_REQUEST["ymd"], $_REQUEST["bldg_ward"])?>
</div>




</body>
</html>
<?
pg_close($con);
