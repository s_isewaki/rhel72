<?
// 入院状況レコードを更新
function update_inpatient_condition($con, $pt_id, $date, $time, $status, $session, $fname, $postback = false) {

	// 入院情報を取得
	$sql = "select inpt_in_dt, inpt_in_tm, inpt_out_res_flg from inptmst";
	$cond = "where ptif_id = '$pt_id'";
	$sel = select_from_table($con, $sql ,$cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$inpt_in_dt = pg_fetch_result($sel, 0, "inpt_in_dt");
	$inpt_in_tm = pg_fetch_result($sel, 0, "inpt_in_tm");
	$inpt_out_res_flg = pg_fetch_result($sel, 0, "inpt_out_res_flg");

	// 関連チェック
	if ($date < $inpt_in_dt) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('入院日より前の登録はできません。');</script>\n");
		if ($postback) {
			echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		} else {
			echo("<script type=\"text/javascript\">history.back();</script>\n");
		}
		exit;
	}
	if ($time != "" && ($date == $inpt_in_dt && $time <= $inpt_in_tm)) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('入院日時より前の登録はできません。');</script>\n");
		if ($postback) {
			echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		} else {
			echo("<script type=\"text/javascript\">history.back();</script>\n");
		}
		exit;
	}

	// 指定日付に移転予定情報があれば論理削除
	$sql = "update inptmove set";
	$set = array("move_del_flg");
	$setvalue = array("t");
	$cond = "where ptif_id = '$pt_id' and move_dt = '$date' and move_cfm_flg = 'f'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 指定日付に外出情報があれば削除
	$sql = "delete from inptgoout";
	$cond = "where ptif_id = '$pt_id' and out_type = '1' and out_date = '$date'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 指定日付を含む外泊情報があれば削除
	$sql = "select out_date, ret_date from inptgoout";
	$cond = "where ptif_id = '$pt_id' and out_type = '2' and to_date('$date', 'YYYYMMDD') between to_date(out_date, 'YYYYMMDD') and to_date(ret_date, 'YYYYMMDD')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$sql = "delete from inptcond";
		$cond = "where ptif_id = '$pt_id' and to_date(date, 'YYYYMMDD') between to_date('{$row["out_date"]}', 'YYYYMMDD') and to_date('{$row["ret_date"]}', 'YYYYMMDD') and hist_flg = 'f'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	$sql = "delete from inptgoout";
	$cond = "where ptif_id = '$pt_id' and out_type = '2' and to_date('$date', 'YYYYMMDD') between to_date(out_date, 'YYYYMMDD') and to_date(ret_date, 'YYYYMMDD')";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 「退院」の場合、退院予定・移転予定を削除
	if ($status == "2") {
		$sql = "delete from inptcond";
		$cond = "where ptif_id = '$pt_id' and cond_check in ('3', '9') and hist_flg ='f'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$sql = "update inptmove set";
		$set = array("move_del_flg");
		$setvalue = array("t");
		$cond = "where ptif_id = '$pt_id' and move_cfm_flg = 'f' and move_del_flg = 'f'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 入院状況IDを取得
	$cond_id = select_from_inpatient_condition_id($con, $pt_id, $fname);

	// 入院状況レコードをDELETE〜INSERT（「在院」の場合はDELETEのみ）
	$sql = "delete from inptcond";
	$cond = "where ptif_id = '$pt_id' and date = '$date' and hist_flg = 'f'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if ($status != "8") {
		$sql = "insert into inptcond (pt_cond_id, ptif_id, cond_check, date) values (";
		$content = array($cond_id, $pt_id, $status, $date);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 入院日に入院状況レコードがなければ作成
	$sql = "select count(*) from inptcond";
	$cond = "where ptif_id = '$pt_id' and date = '$inpt_in_dt' and hist_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_fetch_result($sel, 0, 0) == 0) {
		$sql = "insert into inptcond (pt_cond_id, ptif_id, cond_check, date) values (";
		$content = array($cond_id, $pt_id, "1", $inpt_in_dt);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 退院予定患者の退院予定がなくなった場合、入院患者扱いに戻す
	if ($inpt_out_res_flg == "t" && $status != "2") {
		$sql = "select count(*) from inptcond";
		$cond = "where ptif_id = '$pt_id' and cond_check = '9' and hist_flg = 'f'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_fetch_result($sel, 0, 0) == 0) {
			$sql = "select emp_id from session";
			$cond = "where session_id = '$session'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$emp_id = pg_fetch_result($sel, 0, "emp_id");

			$sql = "update inptmst set";
			$set = array("inpt_in_flg", "inpt_out_res_dt", "inpt_out_res_flg", "inpt_up_dt", "inpt_up_tm", "inpt_op_no", "inpt_out_res_tm", "inpt_result", "inpt_result_dtl", "inpt_out_pos_rireki", "inpt_out_pos_cd", "inpt_out_pos_dtl", "inpt_fd_end", "inpt_out_comment", "inpt_out_inst_cd", "inpt_out_sect_rireki", "inpt_out_sect_cd", "inpt_out_doctor_no", "inpt_out_city", "inpt_back_bldg_cd", "inpt_back_ward_cd", "inpt_back_ptrm_room_no", "inpt_back_bed_no", "inpt_back_in_dt", "inpt_back_in_tm", "inpt_back_sect_id", "inpt_back_dr_id", "inpt_back_change_purpose", "inpt_out_rsn_rireki", "inpt_out_rsn_cd", "inpt_pr_inst_cd", "inpt_pr_sect_rireki", "inpt_pr_sect_cd", "inpt_pr_doctor_no", "out_res_updated", "back_res_updated");
			$setvalue = array("t", "", "f", date("Ymd"), date("Hi"), $emp_id, "", "", "", null, null, "", "", "", "", null, "", null, "", null, null, null, null, null, null, null, null, "", null, null, null, null, null, null, null, null);
			$cond = "where ptif_id = '$pt_id'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
}

// 病床レコードを更新して空床にする
function remove_inpatient_from_bed($con, $bldg_cd, $ward_cd, $ptrm_room_no, $inpt_bed_no, $fname) {
	$sql = "delete from inptmst";
	$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd' and ptrm_room_no = '$ptrm_room_no' and inpt_bed_no = '$inpt_bed_no'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$sql = "insert into inptmst (inpt_enti_id, bldg_cd, ward_cd, ptrm_room_no, inpt_bed_no) values (";
	$content = array("1", $bldg_cd, $ward_cd, $ptrm_room_no, $inpt_bed_no);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 入院状況IDを取得
function select_from_inpatient_condition_id($con, $pt_id, $fname) {
	$sql = "select max(pt_cond_id) from inptcond";
	$cond = "where ptif_id = '$pt_id' and hist_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$cond_id = pg_fetch_result($sel, 0, 0);
	if ($cond_id > 0) {
		return $cond_id;
	}

	$sql = "select max(pt_cond_id) from inptcond";
	$cond = "where ptif_id = '$pt_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$cond_id = pg_fetch_result($sel, 0, 0);
	return ($cond_id + 1);
}

// 日付の加算
function add_days($date, $add) {
	$yr = substr($date, 0, 4);
	$mon = substr($date, 4, 2);
	$day = substr($date, 6, 2);
	$time = mktime(0, 0, 0, $mon, $day, $yr);
	return date("Ymd", strtotime("+{$add} day", $time));
}

// マスタ情報の取得
function get_rireki_index_and_master($con, $mst_cd, $fname) {

	// 履歴インデックスを取得
	$sql = "select max(rireki_index) from tmplitemrireki";
	$cond = "where mst_cd = '$mst_cd'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$rireki_index = intval(pg_fetch_result($sel, 0, 0));

	// マスタが削除されている場合
	if ($rireki_index == 0) {
		return array(0, array());
	}

	// マスタを取得
	$sql = "select item_cd, item_name from tmplitemrireki";
	$cond = "where mst_cd = '$mst_cd' and rireki_index = $rireki_index and disp_flg order by disp_order";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$master = array();
	while ($row = pg_fetch_array($sel)) {
		$master[$row["item_cd"]] = $row["item_name"];
	}

	return array($rireki_index, $master);
}

// 回復期リハ算定上限日数の取得
function get_decubation_max_days($con, $fname) {
	$sql = "select * from bed_decubation";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$max_days = array();
	for ($i = 1; $i <= 6; $i++) {
		$max_days[$i] = intval(pg_fetch_result($sel, 0, "max_days$i"));
	}
	return $max_days;
}

// リハビリ算定期限日数の取得
function get_rehabilitation_limit_days($con, $fname) {
	$sql = "select * from bed_rehabilitation";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$limit_days = array();
	for ($i = 1; $i <= 5; $i++) {
		$limit_days[$i] = intval(pg_fetch_result($sel, 0, "limit_days$i"));
	}
	return $limit_days;
}
?>
