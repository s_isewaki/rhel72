<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 手当</title>
<?
require_once("about_comedix.php");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);


// 手当を取得
$sql = "select allow_cd, allow_contents, allow_price, input_type from tmcdallow";
$cond = "where allow_id = $allow_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$allow_contents = pg_fetch_result($sel, 0, "allow_contents");
$allow_price = pg_fetch_result($sel, 0, "allow_price");
$allow_input_type = pg_fetch_result($sel, 0, "input_type");
$allow_cd = pg_fetch_result($sel, 0, "allow_cd");

$selected_daily = "";
$selected_monthly = "";
switch($allow_input_type) {
	case 1:
		$selected_daily = "checked";
		break;
	case 2:
		$selected_monthly = "checked";
		break;
}
// opt以下のphpを表示するかどうか
$opt_display_flag = file_exists("opt/flag");

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="atdbk_register.php?session=<? echo($session); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="atdbk_register.php?session=<? echo($session); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="atdbk_register.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="updform" action="timecard_allowance_update_exe.php" method="post">
<table width="500" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当</font></td>
<td><input type="text" name="allow_contents" value="<? echo($allow_contents); ?>" size="50" maxlength="100" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一日あたりの金額</font></td>
<td><input type="text" name="allow_price" value="<? echo($allow_price); ?>" size="10" maxlength="7" style="ime-mode:inactive;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">円</font></td>
</tr>
<? if ($opt_display_flag) { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入力種別</font></td>
<td><input type="radio" id="input_daily" name="input_type" value="1" <?= $selected_daily; ?> />日別&nbsp;<input type="radio" id="input_monthly" name="input_type" value="2" <?= $selected_monthly; ?> />月別</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当コード</font></td>
<td><input type="text" name="allow_cd" value="<? echo($allow_cd); ?>" size="4" maxlength="4" style="ime-mode:disabled;"></td>
</tr>
<? } ?>

</table>
<table width="500" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right">
<input type="submit" value="更新">
<input type="button" value="戻る" onclick="history.back();">
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="allow_id" value="<? echo($allow_id); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
