<?php
ob_start();
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_validation.php");
require_once("conf/sql.inf");

//ページ名
$fname = $_SERVER["PHP_SELF"];

//セッションチェック
$session = qualify_session($_REQUEST["session"],$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//**********DB処理**********

//----------ＤＢのコネクション作成----------
$con = connect2db($fname);
//----------Transaction begin----------
pg_exec($con,"begin transaction");

$cond = " where session_id='$session'";
$sel = select_from_table($con,$SQL1,$cond,$fname);
if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id=pg_result($sel,0,"emp_id");

$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con,$SQL151,$cond,$fname);
if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$num = pg_numrows($sel);
$memo = $_REQUEST["memo"];

if($num > 0){
	$cond = "where emp_id = '$emp_id'";
	$set = array(memo);
	if($button==1){
		$memo="";
	}
	$setvalue = array(pg_escape_string(trim($memo)));
	$up_memo = update_set_table($con,$SQL153,$set,$setvalue,$cond,$fname);
	if($up_memo==0){
		pg_exec($con,"rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}
else {
	$content = array($emp_id,pg_escape_string(trim($memo)));
	$in_memo = insert_into_table($con,$SQL152,$content,$fname);
	if($in_memo==0){
		pg_exec($con,"rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}
pg_exec($con, "commit");
pg_close($con);
ob_end_clean();
?>
<script language="javascript">location.href="left.php?session=<?=$session?>"</script>