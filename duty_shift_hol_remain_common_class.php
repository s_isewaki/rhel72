<?php
//duty_shift_hol_remain_common_class.php
//繰越公休残日数共通
require_once("show_timecard_common.ini");
require_once("timecard_bean.php");

class duty_shift_hol_remain_common_class
{
    var $file_name;     // 呼び出し元ファイル名
    var $_db_con;       // DBコネクション
    var $transfer_holiday_days_flg = "f"; //繰越公休残日数表示 t:する f:しない
    var $charge_flg = false; //管理者、代行者 true or false
    var $legal_hol_idx = -1; //公休数の集計列の位置
    var $hol_remain_total = 0;
    /*************************************************************************/
    // コンストラクタ
    // @param object $con DBコネクション
    // @param string $fname 画面名
    /*************************************************************************/
    function duty_shift_hol_remain_common_class($con, $fname, $group_id, $emp_id, $legal_hol_idx)
    {
        // 呼び出し元ファイル名を保持
        $this->file_name = $fname;
        // DBコネクションを保持
        $this->_db_con = $con;
        // フラグ情報を取得して設定する
        if ($group_id != "" && $emp_id != "") {
            $this->get_group_info($group_id, $emp_id);
        }
        if ($legal_hol_idx != "") {
            $this->legal_hol_idx = $legal_hol_idx;
        }
    }
    
    /**
     * get_hol_remain
     * 対象シフトグループ、年月の繰越公休残を取得する
     * @param string $group_id シフトグループID
     * @param string $duty_yyyy 年
     * @param string $duty_mm 月
     * @return array 職員IDをキー、繰越公休残を値とする配列
     *
     */
    function get_hol_remain($group_id, $duty_yyyy, $duty_mm) {
        $sql = "select a.emp_id, b.remain from duty_shift_plan_staff a "
            ." left join duty_shift_hol_remain b on b.emp_id = a.emp_id and b.duty_yyyy = a.duty_yyyy and b.duty_mm = a.duty_mm ";
        $cond = "where a.group_id = '$group_id' and a.duty_yyyy=$duty_yyyy and a.duty_mm=$duty_mm "
            ." order by a.no ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $arr_data = array();
        while ($row = pg_fetch_array($sel)) {
            $arr_data[ $row["emp_id"] ] = $row["remain"];
        }
        if (count($arr_data) == 0) {
            $sql = "select a.emp_id, b.remain from duty_shift_staff a "
                ." left join duty_shift_hol_remain b on b.emp_id = a.emp_id and b.duty_yyyy = $duty_yyyy and b.duty_mm = $duty_mm ";
            $cond = "where a.group_id = '$group_id' "
                ." order by a.no ";
            $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($sel == 0) {
                pg_close($this->_db_con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
                exit;
            }
            $arr_data = array();
            while ($row = pg_fetch_array($sel)) {
                $arr_data[ $row["emp_id"] ] = $row["remain"];
            }
        }
        return $arr_data;
    }
    
    /**
     * get_hol_remain_prev
     * 前月の繰越公休残を取得する
     * @param string $group_id シフトグループID
     * @param string $duty_yyyy 年
     * @param string $duty_mm 月
     * @return array 職員IDをキー、繰越公休残を値とする配列
     *
     */
    function get_hol_remain_prev($group_id, $duty_yyyy, $duty_mm) {
        
        $prev_yyyy = $duty_yyyy;
        $prev_mm = $duty_mm - 1;
        if ($prev_mm == 0) {
            $prev_mm = 12;
            $prev_yyyy = $prev_yyyy - 1;
        }
        
        $sql = "select emp_id, remain from duty_shift_hol_remain  ";
        $cond = "where emp_id in (
                select a.emp_id from duty_shift_plan_staff a
                where a.group_id = '$group_id' and ((a.duty_yyyy=$duty_yyyy and a.duty_mm=$duty_mm) or (a.duty_yyyy=$prev_yyyy and a.duty_mm=$prev_mm)))
                and duty_yyyy=$prev_yyyy and duty_mm=$prev_mm ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $arr_data = array();
        while ($row = pg_fetch_array($sel)) {
            $arr_data[ $row["emp_id"] ] = $row["remain"];
        }
        if (count($arr_data) == 0) {
            $sql = "select emp_id, remain from duty_shift_hol_remain  ";
            $cond = "where emp_id in (
                    select a.emp_id from duty_shift_staff a
                    where a.group_id = '$group_id')
                    and duty_yyyy=$prev_yyyy and duty_mm=$prev_mm ";
            $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($sel == 0) {
                pg_close($this->_db_con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
                exit;
            }
            $arr_data = array();
            while ($row = pg_fetch_array($sel)) {
                $arr_data[ $row["emp_id"] ] = $row["remain"];
            }
        }
        return $arr_data;
    }
    
    /**
     * get_hol_cnt
     * 対象シフトグループ、年月の取得公休数を取得する
     * @param string $group_id シフトグループID
     * @param string $duty_yyyy 年
     * @param string $duty_mm 月
     * @param string $start_date 開始日
     * @param string $end_date 終了日
     * @param string $tablename atdbk:予定 atdbkrslt:実績
     * @return array 職員IDをキー、公休数を値とする配列
     *
     */
    function get_hol_cnt($group_id, $duty_yyyy, $duty_mm, $start_date, $end_date, $tablename="atdbkrslt") {
        
        $sql = "select a.emp_id,sum(case when a.reason in ('35', '36', '44', '54', '63') then 0.5 else 1 end) as cnt  from $tablename a  ";
        $cond = "where a.emp_id in (
                select b.emp_id from duty_shift_plan_staff b
                where b.group_id = '$group_id' and b.duty_yyyy=$duty_yyyy and b.duty_mm=$duty_mm)
                and a.date>='$start_date' and a.date<='$end_date' and (reason in ('24', '35', '36', '44', '45', '46', '47', '54', '59', '63')) group by a.emp_id ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $arr_data = array();
        while ($row = pg_fetch_array($sel)) {
            $arr_data[ $row["emp_id"] ] = $row["cnt"];
        }
        return $arr_data;
        
        /*
        事由コード 名称 日数
        24	公休		1.0
        35	午前公休	0.5
        36	午後公休	0.5
        44	半有半公	0.5
        45	希望(公休)	1.0
        46	待機(公休)	1.0
        47	管理当直前(公休) 1.0
        59	半特半公	0.5
        54	半夏半公	0.5
        63	半正半公	0.5
        */
    }
    
    
    /**
     * get_hol_empcond
     * 勤務条件情報取得（対象シフトグループ、年月に所属する職員分）
     * @param string $group_id シフトグループID
     * @param string $duty_yyyy 年
     * @param string $duty_mm 月
     * @return array 職員IDをキー、勤務パターングループ等を値とする配列
     *
     */
    function get_hol_empcond($group_id, $duty_yyyy, $duty_mm) {
        $sql = "select a.emp_id as staff_id, c.* from duty_shift_plan_staff a "
            ." left join empcond c on c.emp_id = a.emp_id ";
        $cond = "where a.group_id = '$group_id' and a.duty_yyyy=$duty_yyyy and a.duty_mm=$duty_mm "
            ." order by a.no ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $arr_data = array();
        while ($row = pg_fetch_array($sel)) {
            $wk_tmcd_group_id = $row["tmcd_group_id"];
            if ($wk_tmcd_group_id == "") {
                $wk_tmcd_group_id = "1";
            }
            $arr_data[ $row["staff_id"] ]["tmcd_group_id"] = $wk_tmcd_group_id;
            $arr_data[ $row["staff_id"] ]["wage"] = $row["wage"];
            $arr_data[ $row["staff_id"] ]["duty_form"] = $row["duty_form"];
        }
        //ない場合は、職員設定から取得
        if (count($arr_data) == 0) {
            $sql = "select a.emp_id as staff_id, c.* from duty_shift_staff a "
                ." left join empcond c on c.emp_id = a.emp_id ";
            $cond = "where a.group_id = '$group_id' "
                ." order by a.no ";
            $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($sel == 0) {
                pg_close($this->_db_con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
                exit;
            }
            $arr_data = array();
            while ($row = pg_fetch_array($sel)) {
                $wk_tmcd_group_id = $row["tmcd_group_id"];
                if ($wk_tmcd_group_id == "") {
                    $wk_tmcd_group_id = "1";
                }
                $arr_data[ $row["staff_id"] ]["tmcd_group_id"] = $wk_tmcd_group_id;
                $arr_data[ $row["staff_id"] ]["wage"] = $row["wage"];
                $arr_data[ $row["staff_id"] ]["duty_form"] = $row["duty_form"];
            }
        }
        return $arr_data;
    }
    
    
    /**
     * get_group_info
     * フラグ情報を取得
     * @param string $group_id シフトグループID
     * @param string $emp_id 職員ID
     * @return なし
     *
     */
    function get_group_info($group_id, $emp_id) {
        $sql = "select * from duty_shift_group ";
        $cond = "where group_id = '$group_id' ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        if (pg_num_rows($sel)>0) {
            $ret = pg_fetch_result($sel, 0, "transfer_holiday_days_flg");
            if ($ret == "t" || $ret == "f") {
                $this->transfer_holiday_days_flg = $ret;
            }
            //未設定は、f:しない
            else if ($ret == "f") 
                $this->transfer_holiday_days_flg = "f";
        }
        //管理者、代行者
        if (pg_fetch_result($sel, 0, "person_charge_id") == $emp_id ||
                pg_fetch_result($sel, 0, "caretaker_id") == $emp_id) {
            $this->charge_flg = true;
        }
        else {
            for ($i=2; $i<=10; $i++) {
                $var_name = "caretaker{$i}_id";
                $caretaker_id = pg_fetch_result($sel, 0, "$var_name");
                if ($caretaker_id == $emp_id) {
                    $this->charge_flg = true;
                    break;
                }
            }
        }
    }
    
    /**
     * get_transfer_holiday_days_flg
     * 繰越公休残日数表示フラグ取得
     * @param string $group_id シフトグループIDn
     * @return string "t":表示する "f":しない
     *
     */
    function get_transfer_holiday_days_flg($group_id) {
        $sql = "select transfer_holiday_days_flg from duty_shift_group ";
        $cond = "where group_id = '$group_id' ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $ret = pg_fetch_result($sel, 0, "transfer_holiday_days_flg");
        if ($ret == "") {
            //未設定は、f:しない
            $ret = "f";
        }
        return $ret;
    }
    
    /**
     * method set_hol_remain_data
     * データをまとめて取得しplan_arrayに設定
     * 公休残hol_remain、前月残prev_remain、公休使用数hol_cnt、所定公休数std_hol_cnt
     * @param array $plan_array 勤務シフト用データ $plan_array[$i]["key"]に値を設定
     * @param string $group_id シフトグループID
     * @param string $duty_yyyy 年
     * @param string $duty_mm 月
     * @param string $start_date 開始日
     * @param string $end_date 終了日
     * @return なし
     *
     */
    function set_hol_remain_data(&$plan_array, $group_id, $duty_yyyy, $duty_mm, $start_date, $end_date, $wk_plan_copy_flg, $gyo_array) {
        //タイムカード情報の取得
        $timecard_bean = new timecard_bean();
        $timecard_bean->select($this->_db_con, $this->file_name);
        
        $arr_remain = $this->get_hol_remain($group_id, $duty_yyyy, $duty_mm);
        $arr_remain_prev = $this->get_hol_remain_prev($group_id, $duty_yyyy, $duty_mm);
        if ($wk_plan_copy_flg != "1") {
            $tablename = "atdbkrslt"; //atdbkrslt:実績 atdbk:予定
            $arr_hol_cnt = $this->get_hol_cnt($group_id, $duty_yyyy, $duty_mm, $start_date, $end_date, $tablename);
        }
        else {
            //画面更新後の公休数を取得
            $arr_hol_cnt = array();
            if ($this->legal_hol_idx >= 0 && count($gyo_array[0]) > 1) {
                //2段目は$gyo_array配列の後半部分に設定されている
                $gyo_array_cnt = count($gyo_array[0])/2;
                $l_hol_idx = $gyo_array_cnt + $this->legal_hol_idx;
                for ($i=0; $i<count($plan_array); $i++) {
                    $wk_staff_id = $plan_array[$i]["staff_id"];
                    $arr_hol_cnt[$wk_staff_id] = $gyo_array[$i][$l_hol_idx];
                }
            }
        }
        //$tablename = "atdbk"; //atdbkrslt:実績 atdbk:予定 ※追加された場合はコメントをはずす
        //$arr_plan_hol_cnt = $this->get_hol_cnt($group_id, $duty_yyyy, $duty_mm, $start_date, $end_date, $tablename);
        $arr_legal_hol_cnt = array();
        
        $arr_legal_hol_cnt_part = array();
        $arr_holwk = get_timecard_holwk_day($this->_db_con, $this->file_name);
        //勤務パターングループを取得
        $arr_tmcd_group_id = $this->get_tmcd_group_id($group_id, $duty_yyyy, $duty_mm);
        $arr_tmcd_group = array();
        foreach ($arr_tmcd_group_id as $staff_id => $staff_data) {
            $wk_tmcd_group_id = $staff_data["tmcd_group_id"];
            $arr_tmcd_group[$wk_tmcd_group_id] = $wk_tmcd_group_id;
        }
        
        //勤務パターングループ別の調整日数を確認する
        $duty_form = "";
        $arr_std_hol_cnt = array();
        foreach ($arr_tmcd_group as $wk_tmcd_group_id => $dummy) {
            $arr_std_hol_cnt[$wk_tmcd_group_id] = get_legal_hol_cnt_month($this->_db_con, $this->file_name, $duty_yyyy, $duty_mm, $timecard_bean->closing, $timecard_bean->closing_month_flg, $wk_tmcd_group_id, $duty_form, $arr_legal_hol_cnt, $arr_legal_hol_cnt_part, $arr_holwk);
        }
        $data_cnt = count($plan_array);
        for ($i=0; $i<$data_cnt; $i++) {
            $wk_staff_id = $plan_array[$i]["staff_id"];
            $plan_array[$i]["hol_remain"] = $arr_remain[$wk_staff_id];
            $plan_array[$i]["prev_remain"] = $arr_remain_prev[$wk_staff_id];
            $plan_array[$i]["hol_cnt"] = $arr_hol_cnt[$wk_staff_id];
            //$plan_array[$i]["plan_hol_cnt"] = $arr_plan_hol_cnt[$wk_staff_id];
            //所定公休数
            $wk_tmcd_group_id = $arr_tmcd_group_id[$wk_staff_id]["tmcd_group_id"];
            $plan_array[$i]["std_hol_cnt"] = $arr_std_hol_cnt[$wk_tmcd_group_id];
            //表示、集計用
            if ($plan_array[$i]["hol_remain"] != "") {  
                $plan_array[$i]["hol_remain_disp"] = $plan_array[$i]["hol_remain"];
            }
            else {
                //前月残がある場合（無い場合は集計しない）
                if ($plan_array[$i]["prev_remain"] != "") {
                    //前月残＋所定公休数−当月実績公休数
                    $wk = $plan_array[$i]["prev_remain"] + $plan_array[$i]["std_hol_cnt"] - $plan_array[$i]["hol_cnt"];
                    $plan_array[$i]["hol_remain_disp"] = $wk;
                }
            }
            $this->hol_remain_total += $plan_array[$i]["hol_remain_disp"];
            
        }
        
    }
    
    /**
    * get_tmcd_group_id
    * 勤務パターングループ(シフトグループ)取得（対象シフトグループ、年月に所属する職員分）
    * @param string $group_id シフトグループID
    * @param string $duty_yyyy 年
    * @param string $duty_mm 月
    * @return array 職員IDをキー、勤務パターングループを値とする配列
    *select a.emp_id as staff_id, b.group_id, c.pattern_id from duty_shift_plan_staff a
    left join duty_shift_staff b on b.emp_id = a.emp_id
    left join duty_shift_group c on c.group_id = b.group_id
    */
    function get_tmcd_group_id($group_id, $duty_yyyy, $duty_mm) {
        $sql = "select a.emp_id as staff_id, c.pattern_id as tmcd_group_id from duty_shift_plan_staff a "
            ." left join duty_shift_staff b on b.emp_id = a.emp_id "
            ." left join duty_shift_group c on c.group_id = b.group_id ";
        $cond = "where a.group_id = '$group_id' and a.duty_yyyy=$duty_yyyy and a.duty_mm=$duty_mm "
            ." order by a.no ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $arr_data = array();
        while ($row = pg_fetch_array($sel)) {
            $wk_tmcd_group_id = $row["tmcd_group_id"];
            if ($wk_tmcd_group_id == "") {
                $wk_tmcd_group_id = "1";
            }
            $arr_data[ $row["staff_id"] ]["tmcd_group_id"] = $wk_tmcd_group_id;
        }
        //ない場合は、職員設定から取得
        if (count($arr_data) == 0) {
            $sql = "select a.emp_id as staff_id, c.pattern_id as tmcd_group_id from duty_shift_staff a "
                ." left join duty_shift_group c on c.group_id = a.group_id ";
            $cond = "where a.group_id = '$group_id' "
                ." order by a.no ";
            $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($sel == 0) {
                pg_close($this->_db_con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
                exit;
            }
            $arr_data = array();
            while ($row = pg_fetch_array($sel)) {
                $wk_tmcd_group_id = $row["tmcd_group_id"];
                $arr_data[ $row["staff_id"] ]["tmcd_group_id"] = $wk_tmcd_group_id;
            }
        }
        return $arr_data;
        
    }
}

?>