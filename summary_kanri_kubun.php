<? ob_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
$fname = $_SERVER["PHP_SELF"];
require("about_authority.php");
require("about_session.php");
require("get_values.php");
require("show_sinryo_top_header.ini");
require_once("summary_common.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");

$initerror = true;
for (;;){
	//セッションのチェック
	$session = qualify_session($session,$fname);
	if(!$session) break;
	// 役職登録権限チェック
	$checkauth = check_authority($session,59,$fname);
	if(!$checkauth) break;
	//DB接続
	$con = connect2db($fname);
	if(!$con) break;
	$initerror = false;
	break;
}
// エラー時は終了
if ($initerror){
	ob_end_flush();
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo $c_sot_util->javascript("showLoginPage(window);");
	exit;
}

if (@$_REQUEST["mode"] == "update"){
	$sql =
		" update divmst set".
		" div_hidden_flg = " .(int)@$_REQUEST["div_hidden_flg"].
		",div_sort_order = '" . pg_escape_string(@$_REQUEST["div_sort_order"]) . "'".
		" where div_id = ".(int)@$_REQUEST["div_id"];
	$upd = update_set_table($con, $sql, array(), null, "", $fname);
	ob_end_clean();
	header("Location: summary_kanri_kubun.php?session=".$session);
	die;
}
if (@$_REQUEST["mode"] == "delete"){
	$sql =
		" update divmst set".
		" div_del_flg = true".
		" where div_id = ".(int)@$_REQUEST["div_id"];
	$upd = update_set_table($con, $sql, array(), null, "", $fname);
	ob_end_clean();
	header("Location: summary_kanri_kubun.php?session=".$session);
	die;
}

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';




// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);

// DBコネクションの作成
$con = connect2db($fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

// メドレポート/ＰＯレポート(Problem Oriented)
//$med_report_title = get_report_menu_label($con, $fname);

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];
?>
<title>CoMedix <? echo(@$med_report_title); ?>管理 | <?=$summary_kubun_title?>一覧</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list th { border:#5279a5 solid 1px; white-space:nowrap; font-weight:normal; }
.list td { border:#5279a5 solid 1px; }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? show_sinryo_top_header($session,$fname,"KANRI_KUBUN"); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->






<!-- right -->
<td valign="top">
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="110" align="center" bgcolor="#5279a5"><a href="./summary_kanri_kubun.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><? echo ($summary_kubun_title); ?>一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="./sinryo_kubun_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo ($summary_kubun_title); ?>登録</font></a></td>
<td align="right"><!--input type="button" value="削除" onclick="deleteStatus();"--></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>


<img src="img/spacer.gif" alt="" width="1" height="3"><br>


<?//==========================================================================?>
<?// 一覧表示                                                                 ?>
<?//==========================================================================?>
<table width="500px" border="0" cellspacing="0" cellpadding="2" class="list">
<tr bgcolor="#f6f9ff">
<th style="width:80%"><?=$font?><?=$summary_kubun_title?>名</font></th>
<th style="width:5%"><?=$font?>表示</font></th>
<th style="width:5%"><?=$font?>表示順</font></th>
<th style="width:5%"><?=$font?>表示状態更新</font></th>
<th style="width:5%"><?=$font?>削除</font></th>
</tr>
<? $sql = "select *, case when div_sort_order = '' then null else div_sort_order end as sort_order from divmst"; ?>
<? $sel = select_from_table($con,"select * from (".$sql.") d where div_del_flg = 'f' order by sort_order, div_id","",$fname); ?>
<? while($row = @pg_fetch_array($sel)){ ?>
	<form action="summary_kanri_kubun.php" method="get">
		<input type="hidden" name="session" value="<?=$session ?>">
		<input type="hidden" name="div_id" value="<?=$row["div_id"] ?>">
		<input type="hidden" name="mode" value="update">
		<tr>
			<td><?=$font?><a href="" onclick="gotoEdit('<?=$row["div_id"]?>'); return false;"><?=$row["diag_div"]?></a></font></td>
			<td><select name="div_hidden_flg"><option value="">表示</option><option value="1" <?=$row["div_hidden_flg"]?" selected":""?>>非表示</option></select></td>
			<td><input type="text" name="div_sort_order" value="<?=$row["div_sort_order"]?>" style="width:40px"></td>
			<td style="text-align:center"><input type="submit" value="更新" style="width:42px" /></td>
			<td><input type="button" value="削除" style="width:42px" onclick="deleteRecord('<?=$row["div_id"]?>');" /></td>
		</tr>
	</form>
<? } ?>
</table>




<script type="text/javascript">
	function gotoEdit(div_id){
		location.href = "sinryo_kubun_update.php?session=<?=$session?>&div_id="+div_id;
	}
	function deleteRecord(div_id){
		if (!confirm("削除します。よろしいですか？")) return;
		location.href = "summary_kanri_kubun.php?session=<?=$session?>&div_id="+div_id+"&mode=delete";
	}
</script>







</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
