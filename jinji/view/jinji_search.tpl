<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<title>CoMedix 人事管理｜検索</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<script type="text/javascript" src="js/fontsize.js"></script>
	<script type="text/javascript" src="./jinji/js/jinji.js"></script>
	<script type="text/javascript" src="./jinji/js/jinji_search.js"></script>
	<style type="text/css">
	{literal}
		.list {border-collapse:collapse;}
		.list td {border:#5279a5 solid 1px;}
	{/literal}
	</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="frmJinji" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#f6f9ff">
					<td width="32" height="32" class="spacing">
						<a href="./jinji_menu.php?session={$session}&mode=search"><img src="img/icon/b50.gif" width="32" height="32" border="0" alt="人事管理"></a>
					</td>
					<td >
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="./jinji_menu.php?session={$session}&mode=search"><b>人事管理</b></a></font>
					</td>
{if ($jinji_admin_auth == 1)}
					<td align="right" style="padding-right:6px;">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="./jinji_menu.php?session={$session}&mode=ctrl_item"><b>管理画面へ</b></a></font>
					</td>
{/if}
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr height="22">
					<td width="70" align="center" bgcolor="#5279a5">
						<a href="javascript:move('search','');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>検索</b></font></a>
					</td>
					<td width="5">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
			</table>
			<img src="img/spacer.gif" width="1" height="5" alt=""><br>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr height="30" bgcolor="#f6f9ff">
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$className[0].class_nm}</font>
			<select name="seClass" onchange="chgClass();">
				<option value="" >すべて</option>
{section name=item loop=$clssList}
				<option value="{$clssList[item].class_id}" {if ($seClass == $clssList[item].class_id)}selected{/if}>{$clssList[item].class_nm}</option>
{/section}
			</select>
			</select>
			&nbsp;
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$className[0].atrb_nm}</font>
			<select name="seAtrb" onchange="chgAtrb();">
				<option value="" >すべて</option>
{section name=item loop=$atrbList}
				<option value="{$atrbList[item].atrb_id}" {if ($seAtrb == $atrbList[item].atrb_id)}selected{/if}>{$atrbList[item].atrb_nm}</option>
{/section}
			</select>
			&nbsp;
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$className[0].dept_nm}</font>
			<select name="seDept" onchange="chgDept();">
				<option value="" >すべて</option>
{section name=item loop=$deptList}
				<option value="{$deptList[item].dept_id}" {if ($seDept == $deptList[item].dept_id)}selected{/if}>{$deptList[item].dept_nm}</option>
{/section}
			</select>
{if ($className[0].class_cnt == 4)}
			&nbsp;
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$className[0].room_nm}</font>
			<select name="seRoom" onchange="">
				<option value="" >すべて</option>
{section name=item loop=$roomList}
				<option value="{$roomList[item].room_id}" {if ($seRoom == $roomList[item].room_id)}selected{/if}>{$roomList[item].room_nm}</option>
{/section}
			</select>
{/if}
		</td>
		<td align="right">
			<select name="seView" onchange="document.frmJinji.mode.value='search';initConditions();this.form.submit();">
				<option value="1" {if ($seView == "1")}selected{/if}>利用中の職員を表示</option>
				<option value="2" {if ($seView == "2")}selected{/if}>利用停止中の職員を表示</option>
			</select>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr height="30" bgcolor="#f6f9ff">
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font>
			<select name="seJob">
				<option value="" >すべて</option>
{if ($jobList != 0)}
	{section name=item loop=$jobList}
				<option value="{$jobList[item].job_id}" {if ($seJob == $jobList[item].job_id)}selected{/if}>{$jobList[item].job_nm}</option>
	{/section}
{/if}
			</select>
{if ($authRefer[3].auth_flg == "t" || $authEdit[3].auth_flg == "t")}
	{if ($dispInfo[12].item_flg > 0)}
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$dispInfo[12].item_name}</font><input type="text" name="seKey1" value="{$seKey1}">&nbsp;
	{/if}
	{if ($dispInfo[13].item_flg > 0)}
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$dispInfo[13].item_name}</font><input type="text" name="seKey2" value="{$seKey2}">&nbsp;
	{/if}
{/if}
		</td>
	</tr>
{if ($authRefer[4].auth_flg == "t"|| $authEdit[4].auth_flg == "t")}
	{if ($dispInfo[21].item_flg > 0 || $dispInfo[23].item_flg > 0)}
	<tr height="30" bgcolor="#f6f9ff">
		<td>
		{if ($dispInfo[21].item_flg > 0)}
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$dispInfo[21].item_name}</font><input type="text" name="seKey3" value="{$seKey3}">&nbsp;
		{/if}
		{if ($dispInfo[22].item_flg > 0)}
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$dispInfo[22].item_name}</font><input type="text" name="seKey4" value="{$seKey4}">&nbsp;
		{/if}
		{if ($dispInfo[23].item_flg > 0)}
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$dispInfo[23].item_name}</font><input type="text" name="seKey5" value="{$seKey5}">&nbsp;
		{/if}
		</td>
	</tr>
	{/if}
{/if}
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr height="30" bgcolor="#f6f9ff">
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font>
			<input type="text" name="seEmpID" value="{$seEmpID}" maxlength="15" style="ime-mode:inactive;">&nbsp;
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員氏名</font>
			<input type="text" name="seEmpName" value="{$seEmpName}" maxlength="15" style="ime-mode:active;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" onclick="document.frmJinji.mode.value='search';initConditions();" value="検索">
		</td>
	</tr>
</table>

{if ($dataNum > 0)}
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right">
	{if ($prtList[0].print_auth_iryo == 't')}
			<input type="button" value="医療従事者名簿" onclick="print_list();">&nbsp;
	{/if}
	{if ($total_page > 1)}
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ページ&nbsp;</font>
		{if ($sePage != 0)}
			{assign var="pageBack" value=$sePage-1 }
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:setPage('{$pageBack}');move('search','');">←</a></font>
		{/if}
		{section name=cnt start=0  loop=$total_page}  
			{assign var="pageNum" value=$smarty.section.cnt.index+1 }
			{if ($sePage != $smarty.section.cnt.index)}
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"> <a href="javascript:setPage('{$smarty.section.cnt.index}');move('search','');">{$pageNum}</a> </font>
			{else}
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">［{$pageNum}］</font>
			{/if}
		{/section} 
		{if ($check_last != $sePage)}
			{assign var="pageNext" value=$sePage+1 }
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:setPage('{$pageNext}');move('search','');">→</a></font>
		{/if}
	{/if}
		</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr height="22" bgcolor="#f6f9ff" valign="top">
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:setSort('{$arrSort[0]}');move('search','');">職員ID</a></font>
		</td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:setSort('{$arrSort[1]}');move('search','');">職員氏名</a></font>
		</td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属{$className[0].class_nm}</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
	</tr>
	{section name=item loop=$retData}
	<tr valign="top">
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$retData[item].emp_personal_id}</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$retData[item].emp_lt_nm}&nbsp;{$retData[item].emp_ft_nm}</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$retData[item].class_nm}</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$retData[item].st_nm}</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$retData[item].job_nm}</font></td>
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="button" value="参照" onclick="move('refer','{$retData[item].emp_id}');"></font></td>
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="button" value="個人名簿" onclick="print1_pdf('{$retData[item].emp_id}');"></font></td>
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="button" value="更新" onclick="move('edit','{$retData[item].emp_id}');"></font></td>
		</td>
	</tr>
	{/section}
</table>
{/if}
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="mode" value="">
<input type="hidden" name="seSort" value="{$seSort}">
<input type="hidden" name="sePage" value="{$sePage}">
<input type="hidden" name="seSelID" value="">

<input type="hidden" name="print1ID" value="">

<input type="hidden" name="atrbData" value="{$atrbData}">
<input type="hidden" name="deptData" value="{$deptData}">
<input type="hidden" name="roomData" value="{$roomData}">
{$hidNameADR}

</form>
</body>
</html>

