<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr height="24">
		<td width="120" align="center" bgcolor="#bdd1e7">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>基本属性</b></font>
		</td>
		<td align="right">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			一括印刷対象&nbsp;
			<input type="radio" name="print_all" value="t" {if ($print_all == "t")}checked{/if}>する&nbsp;
			<input type="radio" name="print_all" value="f" {if ($print_all == "f")}checked{/if}>しない&nbsp;
			<input type="submit" value="更新" >
			</font>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr>
		<td valign="top" width="80%">
			<table border="0">
				<tr>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font>
					</td>
					<td colspan="4">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$emp_personal_id}</font>
					</td>
				</tr>
				<tr>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名(漢字)</font>
					</td>
					<td colspan="4">
						<input name="emp_lt_nm" type="text" maxlength="10" value="{$emp_lt_nm}" style="ime-mode:active;">
						<input name="emp_ft_nm" type="text" maxlength="10" value="{$emp_ft_nm}" style="ime-mode:active;">
{if ($old_lt_nm != "")}
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">旧姓</font>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$old_lt_nm}</font>
{else}
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font>
{/if}
					</td>
				</tr>
				<tr>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名(ひらがな)</font>
					</td>
					<td colspan="4">
						<input name="emp_kn_lt_nm" type="text" maxlength="10" value="{$emp_kn_lt_nm}" style="ime-mode:active;">
						<input name="emp_kn_ft_nm" type="text" maxlength="10" value="{$emp_kn_ft_nm}" style="ime-mode:active;">
{if ($old_kn_lt_nm != "")}
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">旧姓</font>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$old_kn_lt_nm}</font>
{else}
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font>
{/if}
					</td>
				</tr>
				<tr>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">生年月日</font>
					</td>
					<td colspan="4">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<select name="birth_y">
										<option value=""></option>
{section name=item loop=$arrY}
										<option value="{$arrY[item]}" {if ($arrY[item] == $birth_y)}selected{/if}>{$arrY[item]}</option>
{/section}
									</select>
								</td>
								<td>/</td>
								<td>
									<select name="birth_m">
										<option value=""></option>
{section name=item loop=$arrM}
										<option value="{$arrM[item]}" {if ($arrM[item] == $birth_m)}selected{/if}>{$arrM[item]}</option>
{/section}
									</select>
								</td>
								<td>/</td>
								<td>
									<select name="birth_d">
										<option value=""></option>
{section name=item loop=$arrD}
										<option value="{$arrD[item]}" {if ($arrD[item] == $birth_d)}selected{/if}>{$arrD[item]}</option>
{/section}
									</select>
								</td>
								<td>
{if ($birth_y != "")}
									&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年齢&nbsp;{$now_age}</font>
{/if}
								</td>
								<td>
									&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">性別</font>
								</td>
								<td>
									&nbsp;
									<select name="emp_sex">
										<option value="3" {if ($emp_sex == "3")}selected{/if}>未設定</option>
										<option value="1" {if ($emp_sex == "1")}selected{/if}>男</option>
										<option value="2" {if ($emp_sex == "2")}selected{/if}>女</option>
									</select>
								</td>
								<td>
									&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font>
								</td>
								<td>
									&nbsp;
									<select name="job_id">
{section name=item loop=$jobList}
										<option value="{$jobList[item].job_id}" {if ($jobList[item].job_id == $job_id)}selected{/if}>{$jobList[item].job_nm}</option>
{/section}
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">婚姻</font>
					</td>
					<td colspan="4">
						<input type="radio" name="marriage_sts" value="0" {if ($marriage_sts == "0")}checked{/if}><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">未婚</font>&nbsp;
						<input type="radio" name="marriage_sts" value="1" {if ($marriage_sts == "1")}checked{/if}><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">既婚</font>&nbsp;
						<input type="radio" name="marriage_sts" value="2" {if ($marriage_sts == "2")}checked{/if}><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">その他</font>&nbsp;
					</td>
				</tr>
				<tr>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">郵便番号</font>
					</td>
					<td colspan="4">
						<input name="emp_zip1" type="text" size="3" maxlength="3" value="{$emp_zip1}" style="ime-mode:inactive;">-
						<input name="emp_zip2" type="text" size="4" maxlength="4" value="{$emp_zip2}" style="ime-mode:inactive;">
					</td>
				</tr>
				<tr>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">都道府県</font>
					</td>
					<td colspan="4">
						<select name="emp_prv">
							<option value=""></option>
{section name=item loop=$jinjiState}
							<option value="{$smarty.section.item.index}" {if ($emp_prv != "" && $emp_prv == $smarty.section.item.index)}selected{/if}>{$jinjiState[item]}</option>
{/section}
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">住所1</font>
					</td>
					<td colspan="4">
						<input name="emp_addr1" type="text" size="50" maxlength="50" value="{$emp_addr1}" style="ime-mode:active;">
					</td>
				</tr>
				<tr>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">住所2</font>
					</td>
					<td colspan="4">
						<input name="emp_addr2" type="text" size="50" maxlength="50" value="{$emp_addr2}" style="ime-mode:active;">
					</td>
				</tr>
				<tr>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">電話番号</font>
					</td>
					<td colspan="4">
						<input name="emp_tel1" type="text" size="5" maxlength="6" value="{$emp_tel1}" style="ime-mode:inactive;">-
						<input name="emp_tel2" type="text" size="5" maxlength="6" value="{$emp_tel2}" style="ime-mode:inactive;">-
						<input name="emp_tel3" type="text" size="5" maxlength="6" value="{$emp_tel3}" style="ime-mode:inactive;">
					</td>
				</tr>
				<tr>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">E-Mail</font>
					</td>
					<td colspan="4">
						<input name="emp_email2" type="text" size="40" maxlength="100" value="{$emp_email2}" style="ime-mode:inactive;">
					</td>
				</tr>

			</table>
		</td>
		<td valign="top">
			<img id="ed_Photo" src="{$photoPath}" height="152" width="114" style="border:solid 1px #5279a5; margin:5px auto 2px;">
		</td>
	</tr>
</table>

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>＜勤務日及び勤務時間＞</b></font>
<br>
<input name="working_hours" type="text" size="100" maxlength="200" value="{$working_hours}" style="ime-mode:active;">
<br>

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>＜コメント＞</b></font>
<br>
<input name="working_comment" type="text" size="100" maxlength="200" value="{$working_comment}" style="ime-mode:active;">
<br>

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>＜職歴＞</b></font>
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="2" class="list2">
				<tr height="22" bgcolor="#f6f9ff" valign="top">
					<td align="center" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病院名</font></td>
					<td align="center" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
					<td align="center" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
					<td align="center" width="160"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始年月日</font></td>
					<td align="center" width="160"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">終了年月日</font></td>
					<td align="center" width="60"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
				</tr>
				<tbody id="rowCareer">
{section name=item loop=$crH}
				<tr>
					<td align="center">
						<input name="crH[]" type="text" size="16" maxlength="50" value="{$crH[item]}" style="ime-mode:active;">
					</td>
					<td align="center">
						<input name="crD[]" type="text" size="16" maxlength="50" value="{$crD[item]}" style="ime-mode:active;">
					</td>
					<td align="center">
						<input name="crP[]" type="text" size="16" maxlength="50" value="{$crP[item]}" style="ime-mode:active;">
					</td>
					<td align='center'>
						<select name="crST_Y[]">
							<option value=""></option>
	{section name=item2 loop=$arrY}
							<option value="{$arrY[item2]}" {if ($arrY[item2] == $crST_Y[item])}selected{/if}>{$arrY[item2]}</option>
	{/section}
						</select>/
						<select name="crST_M[]">
							<option value=""></option>
	{section name=item2 loop=$arrM}
							<option value="{$arrM[item2]}" {if ($arrM[item2] == $crST_M[item])}selected{/if}>{$arrM[item2]}</option>
	{/section}
						</select>/
						<select name="crST_D[]">
							<option value=""></option>
	{section name=item2 loop=$arrD}
							<option value="{$arrD[item2]}" {if ($arrD[item2] == $crST_D[item])}selected{/if}>{$arrD[item2]}</option>
	{/section}
						</select>
					</td>
					<td align='center'>
						<select name="crED_Y[]">
							<option value=""></option>
	{section name=item2 loop=$arrY}
							<option value="{$arrY[item2]}" {if ($arrY[item2] == $crED_Y[item])}selected{/if}>{$arrY[item2]}</option>
	{/section}
						</select>/
						<select name="crED_M[]">
							<option value=""></option>
	{section name=item2 loop=$arrM}
							<option value="{$arrM[item2]}" {if ($arrM[item2] == $crED_M[item])}selected{/if}>{$arrM[item2]}</option>
	{/section}
						</select>/
						<select name="crED_D[]">
							<option value=""></option>
	{section name=item2 loop=$arrD}
							<option value="{$arrD[item2]}" {if ($arrD[item2] == $crED_D[item])}selected{/if}>{$arrD[item2]}</option>
	{/section}
						</select>
					</td>
					<td align='center'>
						<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><a class='delCareer' href='#'>削除</a></font>
					</td>
				</tr>
{/section}
				</tbody>

				<tr>
					<td colspan="5"></td>
					<td align='center'>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a id="addCareer" name="addCareer" href="#">追加</a></font>
					</td>
				</tr>

			</table>
		</td>
	</tr>
</table>
