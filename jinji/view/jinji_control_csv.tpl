<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<title>CoMedix 人事管理｜管理</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<script type="text/javascript" src="js/fontsize.js"></script>
	<script type="text/javascript" src="./jinji/js/jinji.js"></script>
	<script type="text/javascript" src="./jinji/js/jinji_control_csv.js"></script>
	<style type="text/css">
	{literal}
		.list {border-collapse:collapse;}
		.list td {border:#5279a5 solid 1px;}
	{/literal}
	</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="frmJinji" action="" method="post" enctype="multipart/form-data" onsubmit="return checkForm();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#f6f9ff">
					<td width="32" height="32" class="spacing">
						<a href="javascript:move('search','');"><img src="img/icon/b50.gif" width="32" height="32" border="0" alt="人事管理"></a>
					</td>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">
							&nbsp;<a href="javascript:move('search','');"><b>人事管理</b></a>
						 	&gt; <a href="javascript:move('ctrl_item','');"><b>管理画面</b></a>
						</font>
					</td>
					<td align="right" style="padding-right:6px;">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="./jinji_menu.php?session={$session}&mode=search"><b>ユーザー画面へ</b></a></font>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr height="22">
					<td width="70" align="center" bgcolor="#bdd1e7">
						<a href="javascript:move('ctrl_item','');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理項目</font></a>
					</td>
					<td width="5">&nbsp;</td>
					<td width="70" align="center" bgcolor="#bdd1e7">
						<a href="javascript:move('ctrl_user','');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">利用者一覧</font></a>
					</td>
					<td width="5">&nbsp;</td>
					<td width="70" align="center" bgcolor="#5279a5">
						<a href="javascript:move('ctrl_csv','');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>インポート</b></font></a>
					</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
			</table>
			<img src="img/spacer.gif" width="1" height="5" alt=""><br>
		</td>
	</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr height="22">
		<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル</font></td>
		<td><input name="csvfile" type="file" value="" size="50"></td>
	</tr>
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイルの文字コード</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<input type="radio" name="encoding" value="1" {if ($encoding == "1")}checked{/if}>Shift_JIS（Windows/Mac標準）
			<input type="radio" name="encoding" value="2" {if ($encoding == "2")}checked{/if}>EUC-JP
			<input type="radio" name="encoding" value="3" {if ($encoding == "3")}checked{/if}>UTF-8
			</font>
		</td>
	</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
	<tr height="22">
		<td align="right"><input type="submit" value="登録"></td>
	</tr>
</table>
{if ($result == "t")}
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録処理が完了しました。</font>
<br>
{elseif ($result == "f")}
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">登録処理が実行できませんでした。</font>
<br>
{/if}
<br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>注意</b></font>
<br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録済みデータの場合は「上書」もしくは「追加」されます。</font>
<br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">「上書」になっている項目が空の場合、空データで上書きされます。</font>
<br>
<br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>CSVのフォーマット</b></font>
<table border="0" cellspacing="0" cellpadding="2" class="list">
	<tr height="22" bgcolor="#f6f9ff" valign="top">
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カラム</font></td>
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目名</font></td>
<!--
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">必須</font></td>
-->
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新</font></td>
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">補足説明</font></td>
	</tr>
	{section name=item loop=$csvFrmt}
	<tr height="22" valign="top">
		<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$csvFrmt[item].dataNo}</font></td>
		<td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$csvFrmt[item].dataName}</font></td>
<!--
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#FF0000"><b>{$csvFrmt[item].dataHissu}</b></font></td>
-->
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$csvFrmt[item].dataUpdate}</font></td>
		<td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$csvFrmt[item].dataMemo}</font></td>
		</td>
	</tr>
	{/section}
</table>

<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="mode" value="ctrl_csv_update">
<input type="hidden" name="seSelID" value="">

</form>
</body>
</html>

