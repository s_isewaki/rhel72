<?php
//************************************
//人事管理機能 管理　管理項目
//************************************

//====================================
//自ファイル名設定
//====================================
$fname = basename(__FILE__);

//====================================
//共通初期設定
//====================================
require_once("./jinji/jinji_common_init.php");

//====================================
//Smarty設定
//====================================
require_once("./jinji/conf/smarty.conf");

//====================================
//人事管理　参照機能専用モデル
//====================================
require_once("./jinji/model/jinji_control_item_model.php");
$mdb2 = new JinjiControlItemModel($log, CMX_DB_DSN, $jinji_UserEmpID, $sts);
if($sts == "err_exit") err_exit();

//====================================
//処理開始ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.管理項目：処理開始");

//====================================
//フォームデータ取得
//====================================


//====================================
//管理項目取得
//====================================
$retData = $mdb2->getItems();
if($retData == "err_exit") err_exit();

$ctgName = "";	//機能区分名
$itemData = "";	//機能データ
$ctgCnt = 0;
$itemCnt = 0;
for($i=0;$i<count($retData);$i++){
	if($i == 0){
		//最初のデータ
		$ctgName[$ctgCnt] = $retData[$i]["ctg_name"];
	}else if($retData[$i]["item_ctg"] != $retData[$i-1]["item_ctg"]){
		//次の区分
		$ctgCnt++;
		$itemCnt = 0;
		$ctgName[$ctgCnt] = $retData[$i]["ctg_name"];
	}
	$itemData[$ctgCnt][$itemCnt]["item_id"] = $retData[$i]["item_id"];
	$itemData[$ctgCnt][$itemCnt]["item_name"] = $retData[$i]["item_name"];
	$itemData[$ctgCnt][$itemCnt]["item_flg"] = $retData[$i]["item_flg"];
	$itemCnt++;
}

//====================================
//画面データ・出力
//====================================
$smarty->assign("session", $session);

$smarty->assign("ctgName", $ctgName);
$smarty->assign("itemData", $itemData);

$smarty->display(basename(__FILE__, ".php").".tpl");

//====================================
//処理終了ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.管理項目：処理終了");

?>