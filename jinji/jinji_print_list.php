<?
//************************************
//人事管理機能 個人帳票
//************************************

//====================================
//自ファイル名設定
//====================================
$fname = basename(__FILE__);

//====================================
//共通初期設定
//====================================
require_once("./jinji/jinji_common_init_print.php");

//====================================
//人事管理　参照機能専用モデル
//====================================
require_once("./jinji/model/jinji_print_list_model.php");
$mdb2 = new JinjiPrintListModel($log, CMX_DB_DSN, $jinji_UserEmpID, $sts);
if($sts == "err_exit") err_exit();

//====================================
//処理開始ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.医療事者名簿：処理開始");

$seEmpID   = $_REQUEST["seEmpID"];
$seEmpName = $_REQUEST["seEmpName"];
$seView    = $_REQUEST["seView"];
$seClass   = $_REQUEST["seClass"];
$seAtrb    = $_REQUEST["seAtrb"];
$seDept    = $_REQUEST["seDept"];
$seRoom    = $_REQUEST["seRoom"];
$seJob     = $_REQUEST["seJob"];
$seKey1    = $_REQUEST["seKey1"];
$seKey2    = $_REQUEST["seKey2"];
$seKey3    = $_REQUEST["seKey3"];
$seKey4    = $_REQUEST["seKey4"];
$seKey5    = $_REQUEST["seKey5"];

//初期値設定
if( trim($seView) == ""){
	$seView = "1";
}
//削除フラグ設定
$delFlg = ($seView == "1") ? "f" : "t";

//人事ID取得(作成)
//操作者
$res = $mdbC->startTransaction();					//トランザクション開始
if($res == "err_exit") err_exit();
$user_jinjiID = $mdbC->getJinjiID($jinji_UserEmpID);	//人事ID取得(作成)
if($user_jinjiID == "err_exit") err_exit();
$res = $mdbC->commitTransaction();				//コミット
if($res == "err_exit") err_exit();

//部署階層数
$class_cnt = $mdb2->getClassCnt();
if($class_cnt == "err_exit") err_exit();

//施設名取得
$prf_name = $mdb2->getPrfName();
if($prf_name == "err_exit") err_exit();

//職種名取得
if($seJob != ""){
	$job_nm = $mdb2->getJobName($seJob);
	if($job_nm == "err_exit") err_exit();

}else{
	$job_nm = "指定なし";
}

//一覧データ取得
$retData = $mdb2->getList($user_jinjiID, $seEmpID, $seEmpName, 
 						  $seClass, $seAtrb, $seDept, $seRoom, $seJob, 
						  $seKey1, $seKey2, $seKey3, $seKey4, $seKey5, 
						  $delFlg);
if($retData == "err_exit") err_exit();

$prtData = array();

$stCnt = 0;	//シート数
$dtCnt = 0;	//シート内データ数

for($i=0;$i<count($retData);$i++){

	//部署ごとシート分け
	if($i > 0){
		if($class_cnt == 4){
			if($retData[$i]["room_id"] != $retData[$i-1]["room_id"]){
				$stCnt++;
				$dtCnt = 0;
			}
		}else{
			if($retData[$i]["dept_id"] != $retData[$i-1]["dept_id"]){
				$stCnt++;
				$dtCnt = 0;
			}
		}
	}

	//部署
	if($class_cnt == 4){
		$prtData[$stCnt][$dtCnt]["basho"] = $retData[$i]["room_nm"];
	}else{
		$prtData[$stCnt][$dtCnt]["basho"] = $retData[$i]["dept_nm"];
	}
	if($prtData[$stCnt][$dtCnt]["basho"] == ""){
		$prtData[$stCnt][$dtCnt]["basho"] = "勤務場所未設定";
	}
	//役職
	$prtData[$stCnt][$dtCnt]["st_nm"] = $retData[$i]["st_nm"];
	//資格・免許・備考
	$retARR = $mdb2->getShikaku($retData[$i]["jinji_id"]);
	if($retARR == "err_exit") err_exit();
	$prtData[$stCnt][$dtCnt]["item_id"]      = $retARR["item_id"];
	$prtData[$stCnt][$dtCnt]["license_name"] = $retARR["license_name"];
	if($retARR["item_id"] == 18 && $retARR["license_prv"] != ""){
		$prtData[$stCnt][$dtCnt]["license_prv"] = $jinjiState[(int)$retARR["license_prv"]];
	}else{
		$prtData[$stCnt][$dtCnt]["license_prv"] = "";
	}
	$prtData[$stCnt][$dtCnt]["license_no"]   = " ".$retARR["license_no"]." ";
	$prtData[$stCnt][$dtCnt]["license_date"] = $retARR["license_date"];
	$prtData[$stCnt][$dtCnt]["license_biko"] = $retARR["license_biko"];
	//氏名
	$prtData[$stCnt][$dtCnt]["name"] = $retData[$i]["emp_lt_nm"]." ".$retData[$i]["emp_ft_nm"];
	//生年月日
	if($retData[$i]["emp_birth"] != ""){
		$prtData[$stCnt][$dtCnt]["birth"] = date("Y/n/j", strtotime($retData[$i]["emp_birth"]));
	}else{
		$prtData[$stCnt][$dtCnt]["birth"] = "";
	}
	//採用年月日
	if($retData[$i]["emp_join"] != ""){
		$prtData[$stCnt][$dtCnt]["emp_join"] =date("Y/n/j", strtotime($retData[$i]["emp_join"]));
	}else{
		$prtData[$stCnt][$dtCnt]["emp_join"] = "";
	}
	//常勤・非常勤
	switch ($retData[$i]["duty_form"]){
		case 1: 
			$prtData[$stCnt][$dtCnt]["duty_form"] = "常勤";
			break;
		case 2: 
			$prtData[$stCnt][$dtCnt]["duty_form"] = "非常勤";
			break;
		default:	
			$prtData[$stCnt][$dtCnt]["duty_form"] = "";
	}
	//勤務時間	
	$prtData[$stCnt][$dtCnt]["working_time"] = $retData[$i]["working_time"];
	//被保険者証　記号番号	
	$prtData[$stCnt][$dtCnt]["kenko_number"] = " ".$retData[$i]["kenko_number"]." ";
	
	$dtCnt++;
}

//該当データなし
if(count($retData) == 0){
	$prtData[0][0]["basho"] = "該当データなし";
	$job_nm = "";
	$prtData[0][0]["st_nm"] = "";
	$prtData[0][0]["license_name"] = "";
	$prtData[0][0]["name"] = "";
	$prtData[0][0]["birth"] = "";
	$prtData[0][0]["license_prv"] = "";
	$prtData[0][0]["license_no"] = "";
	$prtData[0][0]["license_date"] = "";
	$prtData[0][0]["emp_join"] = "";
	$prtData[0][0]["duty_form"] = "";
	$prtData[0][0]["working_time"] = "";
	$prtData[0][0]["kenko_number"] = "";
	$prtData[0][0]["license_biko"] = "";
}

//====================================
//	エクセル出力クラス
//====================================
require_once("jinji_print_excel_class.php");
// Excelオブジェクト生成
$excelObj = new ExcelWorkShop();
// Excel列名。列名をloopで連続するときに使うと便利です。
$excelColumnName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN');



for($stCnt=0;$stCnt<count($prtData);$stCnt++){
	if($stCnt > 0){
		//シート作成
		$excelObj->setNewSheet($stCnt);
	}
	//■■全体の指定
	// 用紙の向き。横方向の例
	$excelObj->PageSetUp("Horizon");
	// 用紙サイズ
	$excelObj->PaperSize( "A4" );
	// 倍率と自動或いは利用者指定。利用者が指定して95%で印刷する場合
	$excelObj->PageRatio( "USER" , "95" );
	// 余白指定、左,右,上,下,ヘッダ,フッタ
	$excelObj->SetMargin( "1.0" , "1,0", "1.0" , "2.0" , "1.0" , "1.0" );
	// シート名
	$excelObj->SetSheetName(mb2($prtData[$stCnt][0]["basho"]));
	//行のタイトル
	$excelObj->setRowTitle(1, 5);
	//フッター
	$strFooter  = '&C&P / &N';
	$strFooter .= "&R".date("Y.n.j 現在");
	$excelObj->SetPrintFooter($strFooter);
	// 列幅
	//$colWdArr = array(4,10, 10, 15, 10, 9, 9, 9, 10, 8, 27, 8, 18);
	$colWdArr = array(5,11, 11, 16, 11, 10, 14, 10, 11, 9, 28, 14, 19);
	for($i=0;$i<count($colWdArr);$i++){
		$excelObj->SetColDim($excelColumnName[$i], $colWdArr[$i]);
	}
	//デフォルトフォント
	$excelObj->setDefaultFont("ＭＳ Ｐゴシック", 9);
		//ウィンド枠固定
	$excelObj->setPane(0,6);
	
	//■■行のタイトル部分
	//帳票タイトル
	$excelObj->SetRowDim(1, 30);
	$excelObj->CellMerge("A1:M1");
	$excelObj->SetValueJP2("A1", "医　療　従　事　者　名　簿", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 18);
	
	//職種
	$excelObj->SetValueJP2("A2", "職種", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->CellMerge("B2:C2");
	$excelObj->SetValueJP2("B2", $job_nm, "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetArea("A2:C2");
	$excelObj->SetBorder("THIN", "", "all");
	
	//勤務場所
	$excelObj->SetValueJP2("G2", "勤務場所", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->CellMerge("H2:I2");
	$excelObj->SetValueJP2("H2", $prtData[$stCnt][0]["basho"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetArea("G2:I2");
	$excelObj->SetBorder("THIN", "", "all");
	
	//施設名
	$excelObj->SetValueJP2("L2", "施設名", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetValueJP2("M2", $prf_name, "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetArea("L2:M2");
	$excelObj->SetBorder("THIN", "", "all");
	
	//タイトル行
	$excelObj->SetValueJP2("A4", "整理", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetValueJP2("A5", "番号", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetArea("A4:A5");
	$excelObj->SetBorder("THIN", "", "outline");
	$excelObj->CellMerge("B4:B5");
	$excelObj->CellMerge("C4:C5");
	$excelObj->CellMerge("D4:D5");
	$excelObj->CellMerge("E4:E5");
	$excelObj->CellMerge("F4:H4");
	$excelObj->CellMerge("I4:I5");
	$excelObj->SetValueJP2("B4", "役職", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetValueJP2("C4", "資格", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetValueJP2("D4", "氏　　　名", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetValueJP2("E4", "生年月日", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetValueJP2("F4", "免　　　許", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetValueJP2("F5", "都道府県", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetValueJP2("G5", "免許番号", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetValueJP2("H5", "取得年月日", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetValueJP2("I4", "採用年月日", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetArea("B4:I5");
	$excelObj->SetBorder("THIN", "", "all");
	$excelObj->SetValueJP2("J4", "常勤・非", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetValueJP2("J5", "常勤の別", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetArea("J4:J5");
	$excelObj->SetBorder("THIN", "", "outline");
	$excelObj->CellMerge("K4:K5");
	$excelObj->SetValueJP2("K4", "勤務日及び勤務時間", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetArea("K4:K5");
	$excelObj->SetBorder("THIN", "", "outline");
	$excelObj->SetValueJP2("L4", "被保険者証", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetValueJP2("L5", "記号番号", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetArea("L4:L5");
	$excelObj->SetBorder("THIN", "", "outline");
	$excelObj->CellMerge("M4:M5");
	$excelObj->SetValueJP2("M4", "備考", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->SetArea("M4:M5");
	$excelObj->SetBorder("THIN", "", "outline");

	//名簿
	for($i=0;$i<count($prtData[$stCnt]);$i++){
		//整理番号
		$excelObj->SetValueJP2("A".(6 + $i), ($i + 1), "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		//役職
		$excelObj->SetValueJP2("B".(6 + $i), $prtData[$stCnt][$i]["st_nm"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		//資格
		$excelObj->SetValueJP2("C".(6 + $i), $prtData[$stCnt][$i]["license_name"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		//氏名
		$excelObj->SetValueJP2("D".(6 + $i), $prtData[$stCnt][$i]["name"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		//生年月日
		$excelObj->SetValueJP2("E".(6 + $i), $prtData[$stCnt][$i]["birth"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		//免許.都道府県
		$excelObj->SetValueJP2("F".(6 + $i), $prtData[$stCnt][$i]["license_prv"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		//免許.免許番号
		$excelObj->SetValueJP2("G".(6 + $i), $prtData[$stCnt][$i]["license_no"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		//免許.取得年月日
		$excelObj->SetValueJP2("H".(6 + $i), $prtData[$stCnt][$i]["license_date"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		//採用年月日
		$excelObj->SetValueJP2("I".(6 + $i), $prtData[$stCnt][$i]["emp_join"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		//常勤・非常勤
		$excelObj->SetValueJP2("J".(6 + $i), $prtData[$stCnt][$i]["duty_form"] , "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		//勤務日及び勤務時間
		$excelObj->SetValueJP2("K".(6 + $i), $prtData[$stCnt][$i]["working_time"] , "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		//被保険者証記号番号
		$excelObj->SetValueJP2("L".(6 + $i), $prtData[$stCnt][$i]["kenko_number"] , "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		//備考
		$excelObj->SetValueJP2("M".(6 + $i), $prtData[$stCnt][$i]["license_biko"] , "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	}
	
	//枠 最低30行
	/*
	if(count($prtData[$stCnt]) < 30){
		$row_max = 30;
	}else{
		$row_max = count($prtData[$stCnt]);
	}
	*/
	//枠 常に30行
	if(count($prtData[$stCnt]) <= 30){
		$row_max = 30;
	}else{
		$plusNum = 30 - (count($prtData[$stCnt]) % 30);
		$row_max = count($prtData[$stCnt]) + $plusNum;
	}

	$excelObj->SetArea("A6:M".(5 + $row_max));
	$excelObj->SetBorder("THIN", "", "all");

	//フォーカスデフォルト
	$excelObj->SetArea("A6"); 
	$excelObj->SetBorder("THIN", "", "all");
	
}

//初期表示シート
$excelObj->setActiveSheet(0);

// 出力ファイル名
$filename = "jinji_list.xls";
// ダウンロード形式
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$filename);
header('Cache-Control: max-age=0');

$excelObj->OutPut();

//====================================
//処理終了ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.医療事者名簿：処理終了");

function mb1($str){
	return mb_convert_encoding($str, "UTF-8", "EUC-JP");
}
function mb2($str){
	return mb_convert_encoding(mb_convert_encoding($str, "sjis-win", "eucJP-win"), "UTF-8", "sjis-win");
}
?>
