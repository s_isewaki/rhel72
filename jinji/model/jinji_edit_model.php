<?php
class JinjiEditModel
{
    protected $empID;
    protected $mdb2;
    protected $objLog;

	public function __construct($log, $conString, $id, &$sts)
	{
		$this->empID = $id;
		$this->objLog = $log;
		$con =& MDB2::connect($conString);
		if(PEAR::isError($con)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")DB接続エラー： ".$con->getDebugInfo());
			$sts = "err_exit";
			return;
		} 
		$this->mdb2 = $con;
	}

	//表示設定取得
	function getDispItemInfo()
	{
		$sql  = "SELECT ";
		$sql .= " item_id, ";
		$sql .= " item_ctg, ";
		$sql .= " item_name, ";
		$sql .= " item_flg ";
		$sql .= "FROM ";
		$sql .= " jinji_manage_item ";
		$sql .= "WHERE ";
		$sql .= " del_flg = 'f' AND ";
		$sql .= " item_flg > 0 ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getDispItemInfo().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getDispItemInfo().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//編集権限取得
	function getAuthEdit($jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);
		
		$sql  = "SELECT ";
		$sql .= " jinji_manage_category.item_ctg, ";
		$sql .= " jinji_manage_category.ctg_name, ";
		$sql .= " jinji_auth_edit.auth_flg ";
		$sql .= "FROM ";
		$sql .= " jinji_manage_category ";
		$sql .= "LEFT JOIN ";
		$sql .= " jinji_auth_edit ";
		$sql .= "ON ";
		$sql .= " jinji_auth_edit.jinji_id = ? AND ";
		$sql .= " jinji_auth_edit.item_ctg = jinji_manage_category.item_ctg ";
		$sql .= "WHERE ";
		$sql .= " jinji_manage_category.del_flg = 'f' ";
		$sql .= "ORDER BY ";
		$sql .= " jinji_manage_category.order_num ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getAuthEdit().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getAuthEdit().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//基本属性取得
	function getBaseInfo($userID)
	{
		$types = array("text");
		$data  = array($userID);
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " empmst.emp_personal_id, ";
		$sql .= " empmst.emp_lt_nm, ";
		$sql .= " empmst.emp_ft_nm, ";
		$sql .= " empmst.emp_kn_lt_nm, ";
		$sql .= " empmst.emp_kn_ft_nm, ";
		$sql .= " empmst.emp_birth, ";
		$sql .= " to_char(to_date(empmst.emp_birth, 'YYYYMMDD'), 'YYYY') AS birth_y, ";
		$sql .= " to_char(to_date(empmst.emp_birth, 'YYYYMMDD'), 'FMMM') AS birth_m, ";
		$sql .= " to_char(to_date(empmst.emp_birth, 'YYYYMMDD'), 'FMDD') AS birth_d, ";
		$sql .= " extract(year from age(current_date, to_date(empmst.emp_birth, 'YYYYMMDD'))) AS now_age, ";
		$sql .= " empmst.emp_sex, ";
		$sql .= " jobmst.job_id, ";
		$sql .= " empmst.emp_zip1, ";
		$sql .= " empmst.emp_zip2, ";
		$sql .= " empmst.emp_prv, ";
		$sql .= " empmst.emp_addr1, ";
		$sql .= " empmst.emp_addr2, ";
		$sql .= " empmst.emp_tel1, ";
		$sql .= " empmst.emp_tel2, ";
		$sql .= " empmst.emp_tel3, ";
		$sql .= " empmst.emp_email2, ";
		$sql .= " empmst.emp_join, ";
		$sql .= " to_char(to_date(empmst.emp_join, 'YYYYMMDD'), 'YYYY') AS join_y, ";
		$sql .= " to_char(to_date(empmst.emp_join, 'YYYYMMDD'), 'FMMM') AS join_m, ";
		$sql .= " to_char(to_date(empmst.emp_join, 'YYYYMMDD'), 'FMDD') AS join_d, ";
		$sql .= " empmst.emp_retire, ";
		$sql .= " to_char(to_date(empmst.emp_retire, 'YYYYMMDD'), 'YYYY') AS retire_y, ";
		$sql .= " to_char(to_date(empmst.emp_retire, 'YYYYMMDD'), 'FMMM') AS retire_m, ";
		$sql .= " to_char(to_date(empmst.emp_retire, 'YYYYMMDD'), 'FMDD') AS retire_d, ";
		$sql .= " extract(year from age(to_date(empmst.emp_retire, 'YYYYMMDD'), to_date(empmst.emp_join, 'YYYYMMDD'))) AS y_jion2retire, ";
		$sql .= " extract(year from age(current_date, to_date(empmst.emp_join, 'YYYYMMDD'))) AS y_jion2now, ";
		$sql .= " jinji_relation.print_all, ";
		$sql .= " jinji_relation.working_time, ";
		$sql .= " jinji_relation.working_comm, ";
		$sql .= " COALESCE(jinji_relation.marriage, 0) AS marriage_sts, ";
		$sql .= " empcond.duty_form ";
		$sql .= "FROM ";
		$sql .= " empmst ";
		$sql .= "JOIN ";
		$sql .= " jinji_relation ";
		$sql .= "ON ";
		$sql .= " jinji_relation.emp_id = empmst.emp_id ";
		$sql .= "JOIN ";
		$sql .= " jobmst ";
		$sql .= "ON ";
		$sql .= " jobmst.job_id = empmst.emp_job ";
		$sql .= "LEFT JOIN ";
		$sql .= " empcond ";
		$sql .= "ON ";
		$sql .= " empcond.emp_id = empmst.emp_id ";
		$sql .= "WHERE ";
		$sql .= " empmst.emp_id = ? ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getBaseInfo().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getBaseInfo().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//職歴
	function getCareer($jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);

		$sql  = "SELECT ";
		$sql .= " career_hospital, ";
		$sql .= " career_department, ";
		$sql .= " career_post, ";
		$sql .= " career_st_date, ";
		$sql .= " to_char(to_date(career_st_date, 'YYYYMMDD'), 'YYYY') AS st_y, ";
		$sql .= " to_char(to_date(career_st_date, 'YYYYMMDD'), 'FMMM') AS st_m, ";
		$sql .= " to_char(to_date(career_st_date, 'YYYYMMDD'), 'FMDD') AS st_d, ";
		$sql .= " career_ed_date, ";
		$sql .= " to_char(to_date(career_ed_date, 'YYYYMMDD'), 'YYYY') AS ed_y, ";
		$sql .= " to_char(to_date(career_ed_date, 'YYYYMMDD'), 'FMMM') AS ed_m, ";
		$sql .= " to_char(to_date(career_ed_date, 'YYYYMMDD'), 'FMDD') AS ed_d ";
		$sql .= "FROM ";
		$sql .= " jinji_career ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? ";
		$sql .= "ORDER BY ";
		$sql .= " career_st_date ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getCareer().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getCareer().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		return $result;

	}

	//氏名履歴
	function getNameHistory($userID)
	{
		$types = array("text");
		$data  = array($userID);
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " lt_nm, ";
		$sql .= " kn_lt_nm ";
		$sql .= "FROM ";
		$sql .= " name_history ";
		$sql .= "WHERE ";
		$sql .= " emp_id = ? ";
		$sql .= "ORDER BY ";
		$sql .= " histdate DESC ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getNameHistory().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getNameHistory().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//部署履歴
	function getBoshuInfo($userID)
	{
		$types = array("text");
		$data  = array($userID);
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " classmst.class_nm, ";
		$sql .= " atrbmst.atrb_nm, ";
		$sql .= " deptmst.dept_nm, ";
		$sql .= " classroom.room_nm, ";
		$sql .= " to_char(histdate, 'YYYY/FMMM/FMDD') AS date_ymd ";
		$sql .= "FROM ";
		$sql .= " class_history ";
		$sql .= "LEFT JOIN ";
		$sql .= " classmst ";
		$sql .= "ON ";
		$sql .= " classmst.class_id = class_history.class_id ";
		$sql .= "LEFT JOIN ";
		$sql .= " atrbmst ";
		$sql .= "ON ";
		$sql .= " atrbmst.atrb_id = class_history.atrb_id ";
		$sql .= "LEFT JOIN ";
		$sql .= " deptmst ";
		$sql .= "ON ";
		$sql .= " deptmst.dept_id = class_history.dept_id ";
		$sql .= "LEFT JOIN ";
		$sql .= " classroom ";
		$sql .= "ON ";
		$sql .= " classroom.room_id = class_history.room_id ";
		$sql .= "WHERE ";
		$sql .= " class_history.emp_id = ? ";
		$sql .= "ORDER BY ";
		$sql .= " class_history.histdate ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getBoshuInfo().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getBoshuInfo().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//職種リスト
	function getJobList($jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);

		$sql  = "SELECT ";
		$sql .= " jobmst.job_id, ";
		$sql .= " jobmst.job_nm, ";
		$sql .= " jobmst.order_no ";
		$sql .= "FROM ";
		$sql .= " jinji_auth_job ";
		$sql .= "JOIN ";
		$sql .= " jobmst ";
		$sql .= "ON ";
		$sql .= " jobmst.job_id = jinji_auth_job.job_id ";
		$sql .= "WHERE ";
		$sql .= " jinji_auth_job.jinji_id = ? AND ";
		$sql .= " jinji_auth_job.auth_flg = 't' ";
		$sql .= "ORDER BY ";
		$sql .= " jobmst.order_no ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getAtrbList().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getAtrbList().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		return $result;

	}
	
	//勤務経緯
	function getWork($jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);

		$sql  = "SELECT ";
		$sql .= " retire_division,  ";
		$sql .= " retire_reason, ";
		$sql .= " emp_course, ";
		$sql .= " referral, ";
		$sql .= " kenko_number, ";
		$sql .= " kiso_number, ";
		$sql .= " kosei_number, ";
		$sql .= " koyo_number ";
		$sql .= "FROM ";
		$sql .= " jinji_work_history ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getWork().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getWork().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		return $result;

	}

	//産休育休期間
	function getKyuka($jinjiID, $tblName)
	{
		$types = array("text");
		$data  = array($jinjiID);

		$sql  = "SELECT ";
		$sql .= " start_date, ";
		$sql .= " to_char(to_date(start_date, 'YYYYMMDD'), 'YYYY') AS st_y, ";
		$sql .= " to_char(to_date(start_date, 'YYYYMMDD'), 'FMMM') AS st_m, ";
		$sql .= " to_char(to_date(start_date, 'YYYYMMDD'), 'FMDD') AS st_d, ";
		$sql .= " end_date, ";
		$sql .= " to_char(to_date(end_date, 'YYYYMMDD'), 'YYYY') AS ed_y, ";
		$sql .= " to_char(to_date(end_date, 'YYYYMMDD'), 'FMMM') AS ed_m, ";
		$sql .= " to_char(to_date(end_date, 'YYYYMMDD'), 'FMDD') AS ed_d ";
		$sql .= "FROM ";
		$sql .= " ".$tblName." ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? ";
		$sql .= "ORDER BY ";
		$sql .= " start_date ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getKyuka().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getKyuka().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		return $result;

	}

	//専門学歴
	function getGakureki($jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);

		$sql  = "SELECT ";
		$sql .= " school_name, ";
		$sql .= " graduation_date, ";
		$sql .= " to_char(to_date(graduation_date, 'YYYYMMDD'), 'YYYY') AS ed_y, ";
		$sql .= " to_char(to_date(graduation_date, 'YYYYMMDD'), 'FMMM') AS ed_m, ";
		$sql .= " to_char(to_date(graduation_date, 'YYYYMMDD'), 'FMDD') AS ed_d ";
		$sql .= "FROM ";
		$sql .= " jinji_educational_bg ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? ";
		$sql .= "ORDER BY ";
		$sql .= " graduation_date ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getGakureki().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getGakureki().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		return $result;

	}

	//専門・認定看護師資格
	function getShikakuPro($jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);

		$sql  = "SELECT ";
		$sql .= " q_pro_field, ";
		$sql .= " q_pro_agency, ";
		$sql .= " q_pro_date, ";
		$sql .= " to_char(to_date(q_pro_date, 'YYYYMMDD'), 'YYYY') AS ed_y, ";
		$sql .= " to_char(to_date(q_pro_date, 'YYYYMMDD'), 'FMMM') AS ed_m, ";
		$sql .= " to_char(to_date(q_pro_date, 'YYYYMMDD'), 'FMDD') AS ed_d ";
		$sql .= "FROM ";
		$sql .= " jinji_q_pro ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? ";
		$sql .= "ORDER BY ";
		$sql .= " q_pro_date ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getShikakuPro().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getShikakuPro().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		return $result;

	}

	//その他(学会・研修等)の認定資格
	function getShikakuEtc($jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);

		$sql  = "SELECT ";
		$sql .= " q_etc_field, ";
		$sql .= " q_etc_agency, ";
		$sql .= " q_etc_date, ";
		$sql .= " to_char(to_date(q_etc_date, 'YYYYMMDD'), 'YYYY') AS ed_y, ";
		$sql .= " to_char(to_date(q_etc_date, 'YYYYMMDD'), 'FMMM') AS ed_m, ";
		$sql .= " to_char(to_date(q_etc_date, 'YYYYMMDD'), 'FMDD') AS ed_d ";
		$sql .= "FROM ";
		$sql .= " jinji_q_etc ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? ";
		$sql .= "ORDER BY ";
		$sql .= " q_etc_date ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getShikakuEtc().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getShikakuEtc().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		return $result;

	}

	//免許番号
	function getMenkyoNo($jinjiID, $id)
	{
		$types = array("text", "integer");
		$data  = array($jinjiID, $id);

		$sql  = "SELECT ";
		$sql .= " license_no, ";
		$sql .= " license_date, ";
		$sql .= " license_prv, ";
		$sql .= " to_char(to_date(license_date, 'YYYYMMDD'), 'YYYY') AS ed_y, ";
		$sql .= " to_char(to_date(license_date, 'YYYYMMDD'), 'FMMM') AS ed_m, ";
		$sql .= " to_char(to_date(license_date, 'YYYYMMDD'), 'FMDD') AS ed_d ";
		$sql .= "FROM ";
		$sql .= " jinji_license ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? AND ";
		$sql .= " item_id  = ? ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getMenkyoNo().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getMenkyoNo().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		return $result;

	}

	//協会
	function getKyoukai($jinjiID, $tblName)
	{
		$types = array("text");
		$data  = array($jinjiID);

		$sql  = "SELECT ";
		$sql .= " association_name, ";
		$sql .= " membership_no, ";
		$sql .= " association_date, ";
		$sql .= " to_char(to_date(association_date, 'YYYYMMDD'), 'YYYY') AS ed_y, ";
		$sql .= " to_char(to_date(association_date, 'YYYYMMDD'), 'FMMM') AS ed_m, ";
		$sql .= " to_char(to_date(association_date, 'YYYYMMDD'), 'FMDD') AS ed_d ";
		$sql .= "FROM ";
		$sql .= " ".$tblName." ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? ";
		$sql .= "ORDER BY ";
		$sql .= " association_date ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getKyoukai().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getKyoukai().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		return $result;

	}

	//院外委員会
	function getIinkai($jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);

		$sql  = "SELECT ";
		$sql .= " committee_name, ";
		$sql .= " certification_date, ";
		$sql .= " to_char(to_date(certification_date, 'YYYYMMDD'), 'YYYY') AS st_y, ";
		$sql .= " to_char(to_date(certification_date, 'YYYYMMDD'), 'FMMM') AS st_m, ";
		$sql .= " to_char(to_date(certification_date, 'YYYYMMDD'), 'FMDD') AS st_d, ";
		$sql .= " end_date, ";
		$sql .= " to_char(to_date(end_date, 'YYYYMMDD'), 'YYYY') AS ed_y, ";
		$sql .= " to_char(to_date(end_date, 'YYYYMMDD'), 'FMMM') AS ed_m, ";
		$sql .= " to_char(to_date(end_date, 'YYYYMMDD'), 'FMDD') AS ed_d ";
		$sql .= "FROM ";
		$sql .= " jinji_committee ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? ";
		$sql .= "ORDER BY ";
		$sql .= " certification_date ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getIinkai().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getIinkai().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		return $result;

	}

	//院外発表
	function getHappyou($jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);

		$sql  = "SELECT ";
		$sql .= " prsnt_theme, ";
		$sql .= " prsnt_agency, ";
		$sql .= " prsnt_date, ";
		$sql .= " to_char(to_date(prsnt_date, 'YYYYMMDD'), 'YYYY') AS ed_y, ";
		$sql .= " to_char(to_date(prsnt_date, 'YYYYMMDD'), 'FMMM') AS ed_m, ";
		$sql .= " to_char(to_date(prsnt_date, 'YYYYMMDD'), 'FMDD') AS ed_d ";
		$sql .= "FROM ";
		$sql .= " jinji_presentation ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? ";
		$sql .= "ORDER BY ";
		$sql .= " prsnt_date ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getHappyou().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getHappyou().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		return $result;

	}

	//院外研修
	function getKenshu($jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);

		$sql  = "SELECT ";
		$sql .= " trng_theme, ";
		$sql .= " trng_sponsor, ";
		$sql .= " trng_venue , ";
		$sql .= " trng_cost , ";
		$sql .= " trng_date, ";
		$sql .= " to_char(to_date(trng_date, 'YYYYMMDD'), 'YYYY') AS ed_y, ";
		$sql .= " to_char(to_date(trng_date, 'YYYYMMDD'), 'FMMM') AS ed_m, ";
		$sql .= " to_char(to_date(trng_date, 'YYYYMMDD'), 'FMDD') AS ed_d ";
		$sql .= "FROM ";
		$sql .= " jinji_training ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? ";
		$sql .= "ORDER BY ";
		$sql .= " trng_date ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getKenshu().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getKenshu().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		return $result;

	}
}

?>