<?php
class JinjiEditUpdateModel
{
    protected $empID;
    protected $mdb2;
    protected $objLog;

	public function __construct($log, $conString, $id, &$sts)
	{
		$this->empID = $id;
		$this->objLog = $log;
		$con =& MDB2::connect($conString);
		if(PEAR::isError($con)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")DB接続エラー： ".$con->getDebugInfo());
			$sts = "err_exit";
			return;
		} 
		$this->mdb2 = $con;
	}

	//トランザクション　スタート
	function startTransaction()
	{
		$res = $this->mdb2->beginTransaction();
		// エラー処理 
		if(PEAR::isError($res)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")トランザクション開始エラー： ".$res->getDebugInfo());
			return "err_exit";
		} 
	}
	
	//トランザクション　コミット
	function commitTransaction()
	{
		$res = $this->mdb2->commit();
		// エラー処理 
		if(PEAR::isError($res)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")トランザクションコミットエラー： ".$res->getDebugInfo());
			return "err_exit";
		} 
	}
	
	//削除処理
	function delJinjiData($jinjiID, $tblName)
	{
		$types = array("text");
		$data = array($jinjiID);
		//SQL生成
		$sql  = "";
		$sql .= "DELETE FROM ";
		$sql .= " ".$tblName." ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[delJinjiData().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[delJinjiData().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();

		return "";
	}

	//表示設定取得
	function getDispItemInfo()
	{
		$sql  = "SELECT ";
		$sql .= " item_id, ";
		$sql .= " item_ctg, ";
		$sql .= " item_name, ";
		$sql .= " item_flg ";
		$sql .= "FROM ";
		$sql .= " jinji_manage_item ";
		$sql .= "WHERE ";
		$sql .= " del_flg = 'f' AND ";
		$sql .= " item_flg > 0 ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getDispItemInfo().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getDispItemInfo().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//編集権限取得
	function getAuthEdit($jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);
		
		$sql  = "SELECT ";
		$sql .= " jinji_manage_category.item_ctg, ";
		$sql .= " jinji_manage_category.ctg_name, ";
		$sql .= " jinji_auth_edit.auth_flg ";
		$sql .= "FROM ";
		$sql .= " jinji_manage_category ";
		$sql .= "LEFT JOIN ";
		$sql .= " jinji_auth_edit ";
		$sql .= "ON ";
		$sql .= " jinji_auth_edit.jinji_id = ? AND ";
		$sql .= " jinji_auth_edit.item_ctg = jinji_manage_category.item_ctg ";
		$sql .= "WHERE ";
		$sql .= " jinji_manage_category.del_flg = 'f' ";
		$sql .= "ORDER BY ";
		$sql .= " jinji_manage_category.order_num ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getAuthEdit().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getAuthEdit().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//基本属性 更新
	function updateBaseInfo($seSelID,   $emp_lt_nm,  $emp_ft_nm, $emp_kn_lt_nm, $emp_kn_ft_nm, 
							$birth_ymd, $emp_sex,    $job_id,    $emp_zip1,     $emp_zip2, 
							$emp_prv,   $emp_addr1,  $emp_addr2, $emp_tel1,     $emp_tel2, 
							$emp_tel3,  $emp_email2)
	{
		$this->mdb2->setOption('portability',	MDB2_PORTABILITY_ALL ^ MDB2_PORTABILITY_EMPTY_TO_NULL);

		$types = array("text", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "integer", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text");
					   
		$data  = array("emp_lt_nm"		=>$emp_lt_nm, 
					   "emp_ft_nm"		=>$emp_ft_nm, 
					   "emp_kn_lt_nm"	=>$emp_kn_lt_nm, 
					   "emp_kn_ft_nm"	=>$emp_kn_ft_nm, 
					   "birth_ymd"		=>$birth_ymd, 
					   "emp_sex"		=>$emp_sex, 
					   "job_id"			=>$job_id, 
					   "emp_zip1"		=>$emp_zip1, 
					   "emp_zip2"		=>$emp_zip2, 
					   "emp_prv"		=>$emp_prv, 
					   "emp_addr1"		=>$emp_addr1, 
					   "emp_addr2"		=>$emp_addr2, 
					   "emp_tel1"		=>$emp_tel1, 
					   "emp_tel2"		=>$emp_tel2, 
					   "emp_tel3"		=>$emp_tel3, 
					   "emp_email2"		=>$emp_email2, 
					   "seSelID"		=>$seSelID);

		//SQL生成
		$sql  = "";
		$sql .= "UPDATE ";
		$sql .= " empmst ";
		$sql .= "SET ";
		$sql .= " emp_lt_nm = :emp_lt_nm, ";
		$sql .= " emp_ft_nm = :emp_ft_nm, ";
		$sql .= " emp_kn_lt_nm = :emp_kn_lt_nm, ";
		$sql .= " emp_kn_ft_nm = :emp_kn_ft_nm, ";
		$sql .= " emp_birth = :birth_ymd, ";
		$sql .= " emp_sex = :emp_sex, ";
		$sql .= " emp_job = :job_id, ";
		$sql .= " emp_zip1 = :emp_zip1,  ";
		$sql .= " emp_zip2 = :emp_zip2,  ";
		$sql .= " emp_prv = :emp_prv,  ";
		$sql .= " emp_addr1 = :emp_addr1,  ";
		$sql .= " emp_addr2 = :emp_addr2,  ";
		$sql .= " emp_tel1 = :emp_tel1,  ";
		$sql .= " emp_tel2 = :emp_tel2,  ";
		$sql .= " emp_tel3 = :emp_tel3,  ";
		$sql .= " emp_email2 = :emp_email2  ";
		$sql .= "WHERE ";
		$sql .= " emp_id = :seSelID ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[updateBaseInfo().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[updateBaseInfo().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();

	}
	
	//印刷フラグ 更新
	function updatePrint($jinjiID, $print_all, $marriage_sts, $working_hours, $working_comment)
	{
		$types = array("text", 
					   "text",
					   "text",
					   "text",
					   "integer");
					   
		$data  = array("jinjiID"	=>$jinjiID, 
					   "print_all"	=>$print_all, 
					   "working_time"	=>$working_hours, 
					   "working_comm"	=>$working_comment, 
					   "marriage"	=>$marriage_sts);
		//SQL生成
		$sql  = "";
		$sql .= "UPDATE ";
		$sql .= " jinji_relation ";
		$sql .= "SET ";
		$sql .= " print_all = :print_all,  ";
		$sql .= " working_time = :working_time,  ";
		$sql .= " working_comm = :working_comm,  ";
		$sql .= " marriage = :marriage  ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = :jinjiID ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[updatePrint().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[updatePrint().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	}

	//職歴
	function insertCareer($jinjiID, $crH, $crD, $crP, $st_ymd, $ed_ymd)
	{
		$types = array("text", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text");
					   
		$data  = array("jinji_id"			=>$jinjiID, 
					   "career_hospital"	=>$crH, 
					   "career_department"	=>$crD, 
					   "career_post"		=>$crP, 
					   "career_st_date"		=>$st_ymd, 
					   "career_ed_date"		=>$ed_ymd);
		//SQL生成
		$sql  = "";
		$sql .= "INSERT INTO ";
		$sql .= " jinji_career ";
		$sql .= "(jinji_id, career_hospital, career_department, career_post, career_st_date, career_ed_date) ";
		$sql .= "VALUES(:jinji_id, :career_hospital, :career_department, :career_post, :career_st_date, :career_ed_date) ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[insertCareer().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[insertCareer().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	}

	//休暇期間
	function insertKyuka($jinjiID, $tblName, $st, $ed)
	{
		$types = array("text", 
					   "text", 
					   "text");
					   
		$data  = array("jinji_id"	=>$jinjiID, 
					   "start_date"	=>$st, 
					   "end_date"	=>$ed);
		//SQL生成
		$sql  = "";
		$sql .= "INSERT INTO ";
		$sql .= " ".$tblName." ";
		$sql .= "(jinji_id, start_date, end_date) ";
		$sql .= "VALUES(:jinji_id, :start_date, :end_date) ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[insertKyuka().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[insertKyuka().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	}

	//勤務経緯
	function insertWork($jinjiID, 
						$retire_kubun, $retire_riyu, $koyou_keika, 
						$shoukaisaki, $wk_hiho_no, $wk_kiso_no, $wk_kousei_no, $wk_koyou_no)	
	{
		$types = array("text", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text");
					   
		$data  = array("jinji_id"			=>$jinjiID, 
					   "retire_division"	=>$retire_kubun, 
					   "retire_reason"		=>$retire_riyu, 
					   "emp_course"			=>$koyou_keika, 
					   "referral"			=>$shoukaisaki, 
					   "kenko_number"		=>$wk_hiho_no, 
					   "kiso_number"		=>$wk_kiso_no, 
					   "kosei_number"		=>$wk_kousei_no, 
					   "koyo_number"		=>$wk_koyou_no);
		//SQL生成
		$sql  = "";
		$sql .= "INSERT INTO ";
		$sql .= " jinji_work_history ";
		$sql .= "(jinji_id, retire_division, retire_reason, ";
		$sql .= " emp_course, referral, kenko_number, kiso_number, ";
		$sql .= " kosei_number, koyo_number) ";
		$sql .= "VALUES(:jinji_id, :retire_division, :retire_reason, ";
		$sql .= "       :emp_course, :referral, :kenko_number, :kiso_number, ";
		$sql .= "       :kosei_number, :koyo_number) ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[insertWork().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[insertWork().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	}

	//勤務経緯 勤務条件マスタ
	function updateCond($seSelID, $join_kubun)
	{
		//存在確認
		$sql  = "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " empcond ";
		$sql .= "WHERE ";
		$sql .= " emp_id = '".$seSelID."' ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[updateCond().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[updateCond().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$resSelect = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		//更新処理
		$types = array("text", 
					   "integer");
					   
		$data  = array("emp_id"	=>$seSelID, 
					   "duty_form"	=>$join_kubun);
		if(count($resSelect) > 0){
			//存在時⇒update
			$sql  = "";
			$sql .= "UPDATE ";
			$sql .= " empcond ";
			$sql .= "SET ";
			$sql .= " duty_form = :duty_form ";
			$sql .= "WHERE ";
			$sql .= " emp_id = :emp_id ";
		}else{
			//非存在時⇒insert
			$sql  = "";
			$sql .= "INSERT INTO ";
			$sql .= " empcond ";
			$sql .= "(emp_id, wage, duty_form) ";
			$sql .= "VALUES(:emp_id, 0, :duty_form) ";
		}
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[updateCond().3] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[updateCond().4] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	}

	//勤務経緯 empmst
	function updateEmpMst($seSelID, $fldName, $upData)
	{
		$types = array("text", 
					   "text");
					   
		$data  = array("emp_id"	=>$seSelID, 
					   "upData"	=>$upData);

		$sql  = "";
		$sql .= "UPDATE ";
		$sql .= " empmst ";
		$sql .= "SET ";
		$sql .= " ".$fldName." = :upData ";
		$sql .= "WHERE ";
		$sql .= " emp_id = :emp_id ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[updateEmpMst().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[updateEmpMst().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	}

	//学歴
	function insertGakureki($jinjiID, $gakuName, $gakuED_YMD)	
	{
		$types = array("text", 
					   "text", 
					   "text");
					   
		$data  = array("jinji_id"			=>$jinjiID, 
					   "school_name"		=>$gakuName, 
					   "graduation_date"	=>$gakuED_YMD);
		//SQL生成
		$sql  = "";
		$sql .= "INSERT INTO ";
		$sql .= " jinji_educational_bg ";
		$sql .= "(jinji_id, school_name, graduation_date) ";
		$sql .= "VALUES(:jinji_id, :school_name, :graduation_date) ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[insertGakureki().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[insertGakureki().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	}

	//専門資格
	function insertShikakuPro($jinjiID, $bunya, $kikan, $ymd)
	{
		$types = array("text", 
					   "text", 
					   "text", 
					   "text");
					   
		$data  = array("jinji_id"		=>$jinjiID, 
					   "q_pro_field"	=>$bunya, 
					   "q_pro_agency"	=>$kikan, 
					   "q_pro_date"		=>$ymd);
		//SQL生成
		$sql  = "";
		$sql .= "INSERT INTO ";
		$sql .= " jinji_q_pro ";
		$sql .= "(jinji_id, q_pro_field, q_pro_agency, q_pro_date) ";
		$sql .= "VALUES(:jinji_id, :q_pro_field, :q_pro_agency, :q_pro_date) ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[insertShikakuPro().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[insertShikakuPro().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	}
	
	//その他資格
	function insertShikakuEtc($jinjiID, $bunya, $kikan, $ymd)
	{
		$types = array("text", 
					   "text", 
					   "text", 
					   "text");
					   
		$data  = array("jinji_id"		=>$jinjiID, 
					   "q_etc_field"	=>$bunya, 
					   "q_etc_agency"	=>$kikan, 
					   "q_etc_date"		=>$ymd);
		//SQL生成
		$sql  = "";
		$sql .= "INSERT INTO ";
		$sql .= " jinji_q_etc ";
		$sql .= "(jinji_id, q_etc_field, q_etc_agency, q_etc_date) ";
		$sql .= "VALUES(:jinji_id, :q_etc_field, :q_etc_agency, :q_etc_date) ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[insertShikakuEtc().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[insertShikakuEtc().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	}

	//免許番号
	function insertLicense($jinjiID, $id, $no, $ymd, $prv)
	{
		$types = array("text", 
					   "integer", 
					   "text", 
					   "text", 
					   "text");
					   
		$data  = array("jinji_id"		=>$jinjiID, 
					   "item_id"		=>$id, 
					   "license_prv"	=>$prv, 
					   "license_no"		=>$no, 
					   "license_date"	=>$ymd);
		//SQL生成
		$sql  = "";
		$sql .= "INSERT INTO ";
		$sql .= " jinji_license ";
		$sql .= "(jinji_id, item_id, license_no, license_date, license_prv) ";
		$sql .= "VALUES(:jinji_id, :item_id, :license_no, :license_date, :license_prv) ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[insertLicense().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[insertLicense().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	}

	//協会
	function insertKyoukai($jinjiID, $tblname, $nm, $no, $ymd)
	{
		$types = array("text", 
					   "text", 
					   "text", 
					   "text");
					   
		$data  = array("jinji_id"			=>$jinjiID, 
					   "association_name"	=>$nm, 
					   "membership_no"		=>$no, 
					   "association_date"	=>$ymd);
		//SQL生成
		$sql  = "";
		$sql .= "INSERT INTO ";
		$sql .= " ".$tblname." ";
		$sql .= "(jinji_id, association_name, membership_no, association_date) ";
		$sql .= "VALUES(:jinji_id, :association_name, :membership_no, :association_date) ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[insertKyoukai().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[insertKyoukai().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	}

	//院外委員会
	function insertIinkai($jinjiID, $nm, $ymd1, $ymd2)
	{
		$types = array("text", 
					   "text", 
					   "text", 
					   "text");
					   
		$data  = array("jinji_id"			=>$jinjiID, 
					   "committee_name"		=>$nm, 
					   "certification_date"	=>$ymd1, 
					   "end_date"			=>$ymd2);
		//SQL生成
		$sql  = "";
		$sql .= "INSERT INTO ";
		$sql .= " jinji_committee ";
		$sql .= "(jinji_id, committee_name, certification_date, end_date) ";
		$sql .= "VALUES(:jinji_id, :committee_name, :certification_date, :end_date) ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[insertIinkai().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[insertIinkai().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	}

	//院外発表
	function insertHappyou($jinjiID, $tm, $ag, $ymd)
	{
		$types = array("text", 
					   "text", 
					   "text", 
					   "text");
					   
		$data  = array("jinji_id"			=>$jinjiID, 
					   "prsnt_theme"		=>$tm, 
					   "prsnt_agency"		=>$ag, 
					   "prsnt_date"			=>$ymd);
		//SQL生成
		$sql  = "";
		$sql .= "INSERT INTO ";
		$sql .= " jinji_presentation ";
		$sql .= "(jinji_id, prsnt_theme, prsnt_agency, prsnt_date) ";
		$sql .= "VALUES(:jinji_id, :prsnt_theme, :prsnt_agency, :prsnt_date) ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[insertHappyou().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[insertHappyou().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	}
	
	//院外研修
	function insertKenshu($jinjiID, $tm, $sp, $vn, $ymd, $ct)
	{
		$types = array("text", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text");
					   
		$data  = array("jinji_id"		=>$jinjiID, 
					   "trng_theme"		=>$tm, 
					   "trng_sponsor"	=>$sp, 
					   "trng_venue"		=>$vn, 
					   "trng_date"		=>$ymd, 
					   "trng_cost"		=>$ct);
		//SQL生成
		$sql  = "";
		$sql .= "INSERT INTO ";
		$sql .= " jinji_training ";
		$sql .= "(jinji_id, trng_theme, trng_sponsor, trng_venue, trng_date, trng_cost) ";
		$sql .= "VALUES(:jinji_id, :trng_theme, :trng_sponsor, :trng_venue, :trng_date, :trng_cost) ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[insertKenshu().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[insertKenshu().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	}
	
}

?>