<?php
class JinjiControlAuthUpdateModel
{
    protected $empID;
    protected $mdb2;
    protected $objLog;

	public function __construct($log, $conString, $id, &$sts)
	{
		$this->empID = $id;
		$this->objLog = $log;
		$con =& MDB2::connect($conString);
		if(PEAR::isError($con)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")DB接続エラー： ".$con->getDebugInfo());
			$sts = "err_exit";
			return;
		} 
		$this->mdb2 = $con;
	}

	//トランザクション　スタート
	function startTransaction()
	{
		$res = $this->mdb2->beginTransaction();
		// エラー処理 
		if(PEAR::isError($res)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")トランザクション開始エラー： ".$res->getDebugInfo());
			return "err_exit";
		} 
	}
	
	//トランザクション　コミット
	function commitTransaction()
	{
		$res = $this->mdb2->commit();
		// エラー処理 
		if(PEAR::isError($res)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")トランザクションコミットエラー： ".$res->getDebugInfo());
			return "err_exit";
		} 
	}

	//ライセンス数取得
	function getLicense()
	{
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " license ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getLicense().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getLicense().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		if (count($result) <= 0){
			$this->objLog->error("ライセンス情報なし：[getLicense().3] ");
			return "err_exit";
		}
		return $result;
	}

	//該当部署所属総数
	function getUseTotal($jobIdArr)
	{
		$wh = "";
		//部署条件
		for($i=0;$i<count($jobIdArr);$i++){
			$whArr[$i] = " emp_job = ".$jobIdArr[$i]." ";
		}
		$wh = join(" OR ", $whArr);
		$wh = " AND (".$wh.")";
	
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " emp_id ";
		$sql .= "FROM ";
		$sql .= " empmst ";
		$sql .= "WHERE ";
		$sql .= " EXISTS ( ";
		$sql .= "  SELECT ";
		$sql .= "   * ";
		$sql .= "  FROM ";
		$sql .= "   authmst ";
		$sql .= "  WHERE ";
		$sql .= "   authmst.emp_id = empmst.emp_id and ";
		$sql .= "   emp_del_flg = 'f') ";
		$sql .= $wh;

		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getUseTotal().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getUseTotal().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		return $result;
	}


	//削除処理
	function delJinjiData($jinjiID, $tblName)
	{
		$types = array("text");
		$data = array($jinjiID);
		//SQL生成
		$sql  = "";
		$sql .= "DELETE FROM ";
		$sql .= " ".$tblName." ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[delJinjiData().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[delJinjiData().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();

		return "";
	}

	//管理対象職種 更新
	function insertJob($jinjiID, $jobID)
	{
		$types = array("text", 
					   "integer");
					   
		$data  = array("jinjiID"	=>$jinjiID, 
					   "jobID"		=>$jobID);
		//SQL生成
		$sql  = "";
		$sql .= "INSERT INTO ";
		$sql .= " jinji_auth_job ";
		$sql .= "( ";
		$sql .= " jinji_id, ";
		$sql .= " job_id, ";
		$sql .= " auth_flg) ";
		$sql .= "VALUES( ";
		$sql .= " :jinjiID, ";
		$sql .= " :jobID, ";
		$sql .= " 't') ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[insertJob().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[insertJob().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();

		return $result;
	}
	
	//職種リスト取得
	function getJobList()
	{
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " job_id ";
		$sql .= "FROM ";
		$sql .= " jobmst ";
		$sql .= "WHERE ";
		$sql .= " job_del_flg = 'f' ";
		$sql .= "ORDER BY ";
		$sql .= " order_no ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[getJobList().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[getJobList().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	
	//区分リスト取得
	function getCategory()
	{
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " item_ctg ";
		$sql .= "FROM ";
		$sql .= " jinji_manage_category ";
		$sql .= "WHERE ";
		$sql .= " del_flg = 'f' ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[getCategory().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[getCategory().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//更新・参照権限 更新
	function insertAuth($tblName, $jinjiID, $ctgID)
	{
		$types = array("text", 
					   "integer");
					   
		$data  = array("jinjiID"	=>$jinjiID, 
					   "ctgID"		=>$ctgID);
		//SQL生成
		$sql  = "";
		$sql .= "INSERT INTO ";
		$sql .= " ".$tblName." ";
		$sql .= "( ";
		$sql .= " jinji_id, ";
		$sql .= " item_ctg, ";
		$sql .= " auth_flg) ";
		$sql .= "VALUES( ";
		$sql .= " :jinjiID, ";
		$sql .= " :ctgID, ";
		$sql .= " 't') ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[insertJob().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[insertJob().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();

		return $result;
	}

	//帳票出力権限 更新
	function upPrintAuth($jinjiID, $prtIryo)
	{
		$types = array("text", 
					   "text");
					   
		$data  = array("jinji_id"			=>$jinjiID, 
					   "print_auth_iryo"	=>$prtIryo);
		//SQL生成
		$sql  = "";
		$sql .= "UPDATE ";
		$sql .= " jinji_relation ";
		$sql .= "SET ";
		$sql .= " print_auth_iryo = :print_auth_iryo ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = :jinji_id ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[upPrintAuth().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[upPrintAuth().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();

		return $result;
	}
}

?>