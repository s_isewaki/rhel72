<?php
class JinjiControlAuthModel
{
    protected $empID;
    protected $mdb2;
    protected $objLog;

	public function __construct($log, $conString, $id, &$sts)
	{
		$this->empID = $id;
		$this->objLog = $log;
		$con =& MDB2::connect($conString);
		if(PEAR::isError($con)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")DB接続エラー： ".$con->getDebugInfo());
			$sts = "err_exit";
			return;
		} 
		$this->mdb2 = $con;
	}

	//ユーザ取得
	function getUser($userID)
	{
		$types = array("text");
		$data  = array($userID);
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " emp_personal_id, ";
		$sql .= " emp_lt_nm, ";
		$sql .= " emp_ft_nm ";
		$sql .= "FROM ";
		$sql .= " empmst ";
		$sql .= "WHERE ";
		$sql .= " emp_id = ? ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getUser().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getUser().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//職種リスト取得
	function getJobList($jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " jobmst.job_id, ";
		$sql .= " jobmst.job_nm, ";
		$sql .= " jinji_auth_job.auth_flg ";
		$sql .= "FROM ";
		$sql .= " jobmst ";
		$sql .= "LEFT JOIN ";
		$sql .= " jinji_auth_job ";
		$sql .= "ON ";
		$sql .= " jinji_auth_job.jinji_id = ? AND ";
		$sql .= " jinji_auth_job.job_id   = jobmst.job_id ";
		$sql .= "WHERE ";
		$sql .= " jobmst.job_del_flg = 'f' ";
		$sql .= "ORDER BY ";
		$sql .= " jobmst.order_no ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getJobList().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getJobList().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//権限リスト取得
	function getAuthList($tblName, $jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " jinji_manage_category.item_ctg, ";
		$sql .= " jinji_manage_category.ctg_name, ";
		$sql .= " ".$tblName.".auth_flg ";
		$sql .= "FROM ";
		$sql .= " jinji_manage_category ";
		$sql .= "LEFT JOIN ";
		$sql .= " ".$tblName." ";
		$sql .= "ON ";
		$sql .= " ".$tblName.".jinji_id = ? AND ";
		$sql .= " ".$tblName.".item_ctg = jinji_manage_category.item_ctg ";
		$sql .= "WHERE ";
		$sql .= " jinji_manage_category.del_flg = 'f' ";
		$sql .= "ORDER BY ";
		$sql .= " jinji_manage_category.order_num ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getJobList().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getJobList().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//帳票出力権限
	function getAuthPrint($jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " print_auth_iryo ";
		$sql .= "FROM ";
		$sql .= " jinji_relation ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getAuthPrint().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getAuthPrint().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}
	
}

?>