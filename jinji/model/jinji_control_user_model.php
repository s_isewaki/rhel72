<?php
class JinjiControlUserModel
{
    protected $empID;
    protected $mdb2;
    protected $objLog;

	public function __construct($log, $conString, $id, &$sts)
	{
		$this->empID = $id;
		$this->objLog = $log;
		$con =& MDB2::connect($conString);
		if(PEAR::isError($con)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")DB接続エラー： ".$con->getDebugInfo());
			$sts = "err_exit";
			return;
		} 
		$this->mdb2 = $con;
	}

	//一覧表示データ取得
	function getList($delFlg, $ctrlSort)
	{
		//ソート条件
		$sqlOrd = "";
		$arrSort = "";
		if ($ctrlSort == "1") {
			$sqlOrd .= " ORDER BY emp_personal_id";
		} else if ($ctrlSort == "2") {
			$sqlOrd .= " ORDER BY emp_personal_id DESC";
		} else if ($ctrlSort == "3") {
			$sqlOrd .= " ORDER BY emp_lt_nm, emp_ft_nm";
		} else {
			$sqlOrd .= " ORDER BY emp_lt_nm DESC, emp_ft_nm DESC";
		}

		$types = array("text");
		$data  = array($delFlg);
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " authmst.emp_id, ";
		$sql .= " emp_personal_id, ";
		$sql .= " emp_lt_nm, ";
		$sql .= " emp_ft_nm, ";
		$sql .= " class_nm, ";
		$sql .= " st_nm, ";
		$sql .= " job_nm ";
		$sql .= "FROM ";
		$sql .= " empmst ";
		$sql .= "INNER JOIN ";
		$sql .= " classmst ";
		$sql .= "ON ";
		$sql .= " empmst.emp_class = classmst.class_id ";
		$sql .= "INNER JOIN ";
		$sql .= " stmst ";
		$sql .= "ON ";
		$sql .= " empmst.emp_st = stmst.st_id ";
		$sql .= "INNER JOIN ";
		$sql .= " jobmst ";
		$sql .= "ON ";
		$sql .= " empmst.emp_job = jobmst.job_id ";	
		$sql .= "INNER JOIN ";
		$sql .= " authmst ";
		$sql .= "ON ";
		$sql .= " empmst.emp_id = authmst.emp_id ";	
		$sql .= "WHERE ";
		$sql .= " EXISTS ( ";
		$sql .= "  SELECT ";
		$sql .= "   * ";
		$sql .= "  FROM ";
		$sql .= "   authmst ";
		$sql .= "  WHERE ";
		$sql .= "   authmst.emp_id = empmst.emp_id and ";
		$sql .= "   emp_del_flg = ?) AND ";
		$sql .= " (authmst.emp_jinji_flg = 't' OR ";
		$sql .= "  authmst.emp_jinjiadm_flg = 't' ) ";
		$sql .= $sqlOrd;
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getList().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getList().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}
	
}

?>