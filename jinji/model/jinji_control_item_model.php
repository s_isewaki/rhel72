<?php
class JinjiControlItemModel
{
    protected $empID;
    protected $mdb2;
    protected $objLog;

	public function __construct($log, $conString, $id, &$sts)
	{
		$this->empID = $id;
		$this->objLog = $log;
		$con =& MDB2::connect($conString);
		if(PEAR::isError($con)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")DB接続エラー： ".$con->getDebugInfo());
			$sts = "err_exit";
			return;
		} 
		$this->mdb2 = $con;
	}

	//人事管理項目取得
	function getItems()
	{
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " jinji_manage_category.item_ctg, ";
		$sql .= " jinji_manage_category.ctg_name, ";
		$sql .= " jinji_manage_item.item_id, ";
		$sql .= " jinji_manage_item.item_name, ";
		$sql .= " jinji_manage_item.item_flg ";
		$sql .= "FROM ";
		$sql .= " jinji_manage_category ";
		$sql .= "INNER JOIN ";
		$sql .= " jinji_manage_item ";
		$sql .= "ON ";
		$sql .= " jinji_manage_item.item_ctg = jinji_manage_category.item_ctg AND ";
		$sql .= " jinji_manage_item.del_flg = 'f' ";
		$sql .= "WHERE ";
		$sql .= " jinji_manage_category.del_flg = 'f' ";
		$sql .= "ORDER BY";
		$sql .= " jinji_manage_category.order_num, ";
		$sql .= " jinji_manage_item.order_num ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getItems().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getItems().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}
	
}

?>