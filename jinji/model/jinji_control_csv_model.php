<?php
class JinjiControlCsvModel
{
    protected $empID;
    protected $mdb2;
    protected $objLog;

	public function __construct($log, $conString, $id, &$sts)
	{
		$this->empID = $id;
		$this->objLog = $log;
		$con =& MDB2::connect($conString);
		if(PEAR::isError($con)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")DB接続エラー： ".$con->getDebugInfo());
			$sts = "err_exit";
			return;
		} 
		$this->mdb2 = $con;
	}

	//表示設定取得
	function getDispItemInfo()
	{
		$sql  = "SELECT ";
		$sql .= " item_id, ";
		$sql .= " item_ctg, ";
		$sql .= " item_name, ";
		$sql .= " item_flg ";
		$sql .= "FROM ";
		$sql .= " jinji_manage_item ";
		$sql .= "WHERE ";
		$sql .= " del_flg = 'f'";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getDispItemInfo().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getDispItemInfo().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}
	
}

?>