<?php
class JinjiPrintListModel
{
    protected $empID;
    protected $mdb2;
    protected $objLog;

	public function __construct($log, $conString, $id, &$sts)
	{
		$this->empID = $id;
		$this->objLog = $log;
		$con =& MDB2::connect($conString);
		if(PEAR::isError($con)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")DB��³���顼�� ".$con->getDebugInfo());
			$sts = "err_exit";
			return;
		} 
		$this->mdb2 = $con;
	}
	
	//�����ؿ�
	function getClassCnt(){
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " class_cnt ";
		$sql .= "FROM ";
		$sql .= " classname ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL�������顼��[getClassCnt().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL�¹ԥ��顼��[getClassCnt().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		if(count($result) > 0){
			return $result[0]["class_cnt"];
		}else{
			return "3";
		}
	}

	//����̾
	function getPrfName(){
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " prf_name ";
		$sql .= "FROM ";
		$sql .= " profile ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL�������顼��[getPrfName().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL�¹ԥ��顼��[getPrfName().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		if(count($result) > 0){
			return $result[0]["prf_name"];
		}else{
			return "";
		}
	}
	
	//����̾
	function getJobName($id){
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " job_nm ";
		$sql .= "FROM ";
		$sql .= " jobmst ";
		$sql .= "WHERE ";
		$sql .= " job_id = ".$id." AND ";
		$sql .= " job_del_flg = 'f' ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL�������顼��[getJobName().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL�¹ԥ��顼��[getJobName().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		if(count($result) > 0){
			return $result[0]["job_nm"];
		}else{
			return "";
		}
	}

	//����ɽ���ǡ�������
	function getList($user_jinjiID, $seEmpID, $seEmpName, 
					 $seClass, $seAtrb, $seDept, $seRoom, $seJob, 
					 $seKey1, $seKey2, $seKey3, $seKey4, $seKey5, 
					 $delFlg)
	{
		//�����оݿ��� ����
		$types = array("text");
		$data = array($user_jinjiID);
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " job_id ";
		$sql .= "FROM ";
		$sql .= " jinji_auth_job ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? AND ";
		$sql .= " auth_flg = 't' ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL�������顼��[getListCount().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL�¹ԥ��顼��[getListCount().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		$jobArr = "";
		$jobWH  = "";
		if(count($result) > 0){
			for($i=0;$i<count($result);$i++){
				$jobArr[$i] = " emp_job = ".$result[$i]["job_id"]." ";
			}
			$jobWH = " AND (".join(" OR ", $jobArr).") ";
		}else{
			return "";
		}


		$fullnm = str_replace(" ", "", $seEmpName);
		$fullnm = str_replace("��", "", $fullnm);
		$seKey1 = str_replace(" ", "", $seKey1);
		$seKey2 = str_replace("��", "", $seKey2);
		$seKey3 = str_replace(" ", "", $seKey3);
		$seKey4 = str_replace("��", "", $seKey4);
		$seKey5 = str_replace(" ", "", $seKey5);

		$types = array("text", 
					   "text", 
					   "text", 
					   "integer", 
					   "integer", 
					   "integer", 
					   "integer", 
					   "integer", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text");
					   
		$data  = array("seEmpID"		=>$seEmpID, 
					   "LIKE_seEmpName"	=>"%".$seEmpName."%", 
					   "LIKE_fullnm"	=>"%".$fullnm."%", 
					   "seClass"		=>$seClass, 
					   "seAtrb"			=>$seAtrb, 
					   "seDept"			=>$seDept, 
					   "seRoom"			=>$seRoom, 
					   "seJob"			=>$seJob, 
					   "seKey1"			=>"%".$seKey1."%", 
					   "seKey2"			=>"%".$seKey2."%", 
					   "seKey3"			=>"%".$seKey3."%", 
					   "seKey4"			=>"%".$seKey4."%", 
					   "seKey5"			=>"%".$seKey5."%", 
					   "delFlg"			=>$delFlg);

		$sqlWhe  = "";
		//ID
		if ($seEmpID != "") {
			$sqlWhe .= " emp_personal_id = :seEmpID ";
		}
		//̾��
		if ($seEmpName != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (emp_lt_nm LIKE :LIKE_seEmpName OR";
			$sqlWhe .= "  emp_ft_nm LIKE :LIKE_seEmpName OR ";
			$sqlWhe .= "  emp_kn_lt_nm LIKE :LIKE_seEmpName OR ";
			$sqlWhe .= "  emp_kn_ft_nm LIKE :LIKE_seEmpName OR ";
			$sqlWhe .= "  (emp_lt_nm || emp_ft_nm) LIKE :LIKE_fullnm OR ";
			$sqlWhe .= "  (emp_kn_lt_nm || emp_kn_ft_nm) LIKE :LIKE_fullnm) ";
		}
		//���硦�ݡ��ʡ���
		if ($seClass != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (emp_class = :seClass ";
			if ($seAtrb != "") {
				$sqlWhe .= " AND emp_attribute = :seAtrb ";
				if ($seDept != "") {
					$sqlWhe .= " AND emp_dept = :seDept ";
					if ($seRoom != "") {
						$sqlWhe .= " AND emp_room = :seRoom ";
					}
				}
			}
			$sqlWhe .= " ) ";	
		}
		//����
		if ($seJob != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " emp_job = :seJob ";
		}
		//�������
		if ($seKey1 != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (SELECT ";
			$sqlWhe .= "   COUNT(*) ";
			$sqlWhe .= "  FROM ";
			$sqlWhe .= "   jinji_educational_bg ";
			$sqlWhe .= "  WHERE ";
			$sqlWhe .= "   jinji_educational_bg.jinji_id = jinji_relation.jinji_id AND ";
			$sqlWhe .= "   jinji_educational_bg.school_name LIKE :seKey1 ";
			$sqlWhe .= " ) > 0 ";
		}
		//ǧ����
		if ($seKey2 != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (SELECT ";
			$sqlWhe .= "   COUNT(*) ";
			$sqlWhe .= "  FROM ";
			$sqlWhe .= "   jinji_q_pro ";
			$sqlWhe .= "  WHERE ";
			$sqlWhe .= "   jinji_q_pro.jinji_id = jinji_relation.jinji_id AND ";
			$sqlWhe .= "   (jinji_q_pro.q_pro_field LIKE :seKey2 OR  ";
			$sqlWhe .= "    jinji_q_pro.q_pro_agency LIKE :seKey2)  ";
			$sqlWhe .= " ) > 0 ";
		}
		//�����Ѱ���
		if ($seKey3 != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (SELECT ";
			$sqlWhe .= "   COUNT(*) ";
			$sqlWhe .= "  FROM ";
			$sqlWhe .= "   jinji_committee ";
			$sqlWhe .= "  WHERE ";
			$sqlWhe .= "   jinji_committee.jinji_id = jinji_relation.jinji_id AND ";
			$sqlWhe .= "   jinji_committee.committee_name LIKE :seKey3 ";
			$sqlWhe .= " ) > 0 ";
		}
		//����ȯɽ
		if ($seKey4 != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (SELECT ";
			$sqlWhe .= "   COUNT(*) ";
			$sqlWhe .= "  FROM ";
			$sqlWhe .= "   jinji_presentation ";
			$sqlWhe .= "  WHERE ";
			$sqlWhe .= "   jinji_presentation.jinji_id = jinji_relation.jinji_id AND ";
			$sqlWhe .= "   (jinji_presentation.prsnt_theme LIKE :seKey4 OR  ";
			$sqlWhe .= "    jinji_presentation.prsnt_agency LIKE :seKey4)  ";
			$sqlWhe .= " ) > 0 ";
		}
		//��������
		if ($seKey5 != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (SELECT ";
			$sqlWhe .= "   COUNT(*) ";
			$sqlWhe .= "  FROM ";
			$sqlWhe .= "   jinji_training ";
			$sqlWhe .= "  WHERE ";
			$sqlWhe .= "   jinji_training.jinji_id = jinji_relation.jinji_id AND ";
			$sqlWhe .= "   (jinji_training.trng_theme LIKE :seKey5 OR  ";
			$sqlWhe .= "    jinji_training.trng_sponsor LIKE :seKey5 OR  ";
			$sqlWhe .= "    jinji_training.trng_venue LIKE :seKey5 OR  ";
			$sqlWhe .= "    jinji_training.trng_cost LIKE :seKey5)  ";
			$sqlWhe .= " ) > 0 ";
		}

		if($sqlWhe != "" ){
			$sqlWhe = " AND (".$sqlWhe.") ";
		}
				
		//SQL����
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " empmst.emp_id, ";
		$sql .= " empmst.emp_personal_id, ";
		$sql .= " empmst.emp_lt_nm, ";
		$sql .= " empmst.emp_ft_nm, ";
		$sql .= " empmst.emp_birth, ";
		$sql .= " empmst.emp_join, ";
		$sql .= " empmst.emp_dept, ";
		$sql .= " empmst.emp_room, ";
		$sql .= " deptmst.dept_id, ";
		$sql .= " deptmst.dept_nm, ";
		$sql .= " classroom.room_id, ";
		$sql .= " classroom.room_nm, ";
		$sql .= " jinji_relation.jinji_id, ";
		$sql .= " jinji_relation.working_time, ";
		$sql .= " jinji_work_history.kenko_number, ";
		$sql .= " class_nm, ";
		$sql .= " st_nm, ";
		$sql .= " job_nm, ";
		$sql .= " empcond.duty_form ";
		$sql .= "FROM ";
		$sql .= " empmst ";
		$sql .= "INNER JOIN ";
		$sql .= " classmst ";
		$sql .= "ON ";
		$sql .= " empmst.emp_class = classmst.class_id ";
		$sql .= "LEFT JOIN ";
		$sql .= " deptmst ";
		$sql .= "ON ";
		$sql .= " empmst.emp_dept = deptmst.dept_id ";	
		$sql .= "LEFT JOIN ";
		$sql .= " classroom ";
		$sql .= "ON ";
		$sql .= " empmst.emp_room = classroom.room_id ";	
		$sql .= "INNER JOIN ";
		$sql .= " stmst ";
		$sql .= "ON ";
		$sql .= " empmst.emp_st = stmst.st_id ";
		$sql .= "INNER JOIN ";
		$sql .= " jobmst ";
		$sql .= "ON ";
		$sql .= " empmst.emp_job = jobmst.job_id ";	
		$sql .= "LEFT JOIN ";
		$sql .= " jinji_relation ";
		$sql .= "ON ";
		$sql .= " jinji_relation.emp_id = empmst.emp_id ";	
		$sql .= "LEFT JOIN ";
		$sql .= " jinji_work_history ";
		$sql .= "ON ";
		$sql .= " jinji_work_history.jinji_id = jinji_relation.jinji_id ";	
		$sql .= "LEFT JOIN ";
		$sql .= " empcond ";
		$sql .= "ON ";
		$sql .= " empcond.emp_id = empmst.emp_id ";
		$sql .= "WHERE ";
		$sql .= " EXISTS ( ";
		$sql .= "  SELECT ";
		$sql .= "   * ";
		$sql .= "  FROM ";
		$sql .= "   authmst ";
		$sql .= "  WHERE ";
		$sql .= "   authmst.emp_id = empmst.emp_id and ";
		$sql .= "   emp_del_flg = :delFlg) ";
		$sql .= $jobWH.$sqlWhe;
		$sql .= "ORDER BY ";
		$sql .= " deptmst.dept_id ASC, ";
		$sql .= " classroom.room_id ASC, ";
		$sql .= " classroom.room_id ASC, ";
		$sql .= " empmst.emp_personal_id ASC ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL�������顼��[getList().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL�¹ԥ��顼��[getList().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//���
	function getShikaku($jinjiID){
	
		$retArr["item_id"]      = "";
		$retArr["license_name"] = "";
		$retArr["license_prv"]  = "";
		$retArr["license_no"]   = "";
		$retArr["license_date"] = "";
		$retArr["license_biko"] = "";
	
		if($jinjiID == ""){
			return $retArr;
		}

		//�Ǹ��
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " jinji_license ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = '".$jinjiID."' AND ";
		$sql .= " item_id = 15 ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL�������顼��[getShikaku().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL�¹ԥ��顼��[getShikaku().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		if(count($result) > 0){
			$retArr["item_id"]      = $result[0]["item_id"];
			$retArr["license_name"] = "�Ǹ��";
			$retArr["license_prv"]  = $result[0]["license_prv"];
			$retArr["license_no"]   = $result[0]["license_no"];
			if($result[0]["license_date"] != ""){
				$retArr["license_date"] = date("Y/n/j", strtotime($result[0]["license_date"]));
			}else{
				$retArr["license_date"] = "";
			}

		}else{
		
			//�ڴǸ��
			$sql  = "";
			$sql .= "SELECT ";
			$sql .= " * ";
			$sql .= "FROM ";
			$sql .= " jinji_license ";
			$sql .= "WHERE ";
			$sql .= " jinji_id = '".$jinjiID."' AND ";
			$sql .= " item_id = 18 ";
			$stmt=$this->mdb2->prepare($sql);
			if (PEAR::isError($stmt)){
				$this->objLog->error("SQL�������顼��[getShikaku().3] ".$stmt->getDebugInfo());
				return "err_exit";
			}
			$res = $stmt->execute();
			if (PEAR::isError($res)){
				$this->objLog->error("SQL�¹ԥ��顼��[getShikaku().4] ".$res->getDebugInfo());
				return "err_exit";
			}
			$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
			$stmt->free();
			$res->free();
			if(count($result) > 0){
				$retArr["item_id"]      = $result[0]["item_id"];
				$retArr["license_name"] = "�ڴǸ��";
				$retArr["license_prv"]  = $result[0]["license_prv"];
				$retArr["license_no"]   = $result[0]["license_no"];
				if($result[0]["license_date"] != ""){
					$retArr["license_date"] = date("Y/n/j", strtotime($result[0]["license_date"]));
				}else{
					$retArr["license_date"] = "";
				}
			}
		}
		
		//�ݷ��
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " jinji_license ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = '".$jinjiID."' AND ";
		$sql .= " item_id = 16 ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL�������顼��[getShikaku().5] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL�¹ԥ��顼��[getShikaku().6] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		if(count($result) > 0){
			$retArr["license_biko"]  .= "�ݷ�� ";
		}
		
		//������
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " jinji_license ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = '".$jinjiID."' AND ";
		$sql .= " item_id = 17 ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL�������顼��[getShikaku().7] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL�¹ԥ��顼��[getShikaku().8] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		if(count($result) > 0){
			$retArr["license_biko"]  .= "������ ";
		}
		
		return $retArr;
		
	}
	
}

?>