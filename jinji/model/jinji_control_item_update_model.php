<?php
class JinjiControlItemUpdateModel
{
    protected $empID;
    protected $mdb2;
    protected $objLog;

	public function __construct($log, $conString, $id, &$sts)
	{
		$this->empID = $id;
		$this->objLog = $log;
		$con =& MDB2::connect($conString);
		if(PEAR::isError($con)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")DB接続エラー： ".$con->getDebugInfo());
			$sts = "err_exit";
			return;
		} 
		$this->mdb2 = $con;
	}
	
	//トランザクション　スタート
	function startTransaction()
	{
		$res = $this->mdb2->beginTransaction();
		// エラー処理 
		if(PEAR::isError($res)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")トランザクション開始エラー： ".$res->getDebugInfo());
			return "err_exit";
		} 
	}
	
	//トランザクション　コミット
	function commitTransaction()
	{
		$res = $this->mdb2->commit();
		// エラー処理 
		if(PEAR::isError($res)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")トランザクションコミットエラー： ".$res->getDebugInfo());
			return "err_exit";
		} 
	}

	//人事管理項目最大ID取得
	function getItemCount()
	{
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " MAX(item_id) AS maxid ";
		$sql .= "FROM ";
		$sql .= " jinji_manage_item ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getItemCount().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getItemCount().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result[0]["maxid"];
	}

	//人事管理項目 更新
	//正常終了：0　異常終了：3
	function setItems($id, $flg)
	{
		$types = array("text", 
					   "text");
					   
		$data  = array("id"		=>$id, 
					   "flg"	=>$flg);
					   
		//SQL生成
		$sql  = "";
		$sql .= "UPDATE ";
		$sql .= " jinji_manage_item ";
		$sql .= "SET ";
		$sql .= " item_flg = :flg ";
		$sql .= "WHERE ";
		$sql .= " item_id = :id ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[setItems().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[setItems().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	}
}

?>