<?php
class JinjiCommonModel
{
    protected $mdb2;
    protected $objLog;

	public function __construct($log, $conString, &$sts)
	{
		$this->objLog = $log;
		$con =& MDB2::connect($conString);
		if(PEAR::isError($con)) 
		{ 
			$this->objLog->error("DB接続エラー： ".$con->getDebugInfo());
			$sts = "err_exit";
		}else{
			$this->mdb2 = $con;
		}
	}

	//トランザクション　スタート
	function startTransaction()
	{
		$res = $this->mdb2->beginTransaction();
		// エラー処理 
		if(PEAR::isError($res)) 
		{ 
			$this->objLog->error("トランザクション開始エラー： ".$res->getDebugInfo());
			return "err_exit";
		} 
	}
	
	//トランザクション　コミット
	function commitTransaction()
	{
		$res = $this->mdb2->commit();
		// エラー処理 
		if(PEAR::isError($res)) 
		{ 
			$this->objLog->error("トランザクションコミットエラー： ".$res->getDebugInfo());
			return "err_exit";
		} 
	}

	//人事ID取得、INSERT
	function getJinjiID($userID)
	{
		//SQL生成
		$types = array("text");
		$data = array($userID);
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " jinji_id ";
		$sql .= "FROM ";
		$sql .= " jinji_relation ";
		$sql .= "WHERE ";
		$sql .= " emp_id = ?";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[getJinjiID().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[getJinjiID().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		
		if(count($result) > 0){
			return $result[0]["jinji_id"];
		}else{
			$JINJI_ID = date("YmdHi");			//仮ＩＤ
			
			$sql  = "SELECT NEXTVAL('jinji_seq') AS newseq";
			$res = $this->mdb2->query($sql);
			if(PEAR::isError($res)) 
			{ 
				$ret = $this->mdb2->rollback();	//ロールバック
				$this->objLog->error("SQL実行エラー：[getJinjiID().3] ".$res->getDebugInfo());
				return "err_exit";
			} 
			$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
			$res->free();
			$JINJI_ID = sprintf("JNJ%010d",$result[0]["newseq"]);
			
			$types = array("text", "text", "text", "integer");
			$data = array($userID, $JINJI_ID, "f", "0");
			$sql  = "";
			$sql .= "INSERT INTO ";
			$sql .= " jinji_relation ";
			$sql .= "( ";
			$sql .= " emp_id, ";
			$sql .= " jinji_id, ";
			$sql .= " print_all, ";
			$sql .= " marriage) ";
			$sql .= "VALUES( ";
			$sql .= " ?, ";
			$sql .= " ?, ";
			$sql .= " ?, ";
			$sql .= " ?) ";
			$stmt=$this->mdb2->prepare($sql, $types, MDB2_PREPARE_MANIP);
			if (PEAR::isError($stmt)){
				$ret = $this->mdb2->rollback();	//ロールバック
				$this->objLog->error("SQL準備エラー：[getJinjiID().4] ".$stmt->getDebugInfo());
				return "err_exit";
			}
			$res = $stmt->execute($data);
			if (PEAR::isError($res)){
				$ret = $this->mdb2->rollback();	//ロールバック
				$this->objLog->error("SQL実行エラー：[getJinjiID().5] ".$res->getDebugInfo());
				return "err_exit";
			}
			$stmt->free();

			return $JINJI_ID;
		}
	}

	//使用者ID等
	function getUserData($sess)
	{
		//SQL生成
		$types = array("text");
		$data = array($sess);
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " emp_id, ";
		$sql .= " emp_lt_nm, ";
		$sql .= " emp_ft_nm ";
		$sql .= "FROM ";
		$sql .= " empmst ";
		$sql .= "WHERE ";
		$sql .= " emp_id = (SELECT ";
		$sql .= "            emp_id ";
		$sql .= "           FROM ";
		$sql .= "            session ";
		$sql .= "           WHERE ";
		$sql .= "            session_id = ?)";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[getUserData().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[getUserData().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		if(count($result) == 0){
			$this->objLog->error("ID取得エラー：[getUserData().3] 該当IDなし");
			return "err_exit";
		}
		return $result;
	}

	
}

?>