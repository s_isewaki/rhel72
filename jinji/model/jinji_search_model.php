<?php
class JinjiSearchModel
{
    protected $empID;
    protected $mdb2;
    protected $objLog;

	public function __construct($log, $conString, $id, &$sts)
	{
		$this->empID = $id;
		$this->objLog = $log;
		$con =& MDB2::connect($conString);
		if(PEAR::isError($con)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")DB接続エラー： ".$con->getDebugInfo());
			$sts = "err_exit";
			return;
		} 
		$this->mdb2 = $con;
	}
	
	//表示設定取得
	function getDispItemInfo()
	{
		$sql  = "SELECT ";
		$sql .= " item_id, ";
		$sql .= " item_ctg, ";
		$sql .= " item_name, ";
		$sql .= " item_flg ";
		$sql .= "FROM ";
		$sql .= " jinji_manage_item ";
		$sql .= "WHERE ";
		$sql .= " del_flg = 'f' AND ";
		$sql .= " item_flg > 0 ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getDispItemInfo().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getDispItemInfo().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//参照集権限取得
	function getAuthRefer($jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);

		$sql  = "SELECT ";
		$sql .= " jinji_manage_category.item_ctg, ";
		$sql .= " jinji_manage_category.ctg_name, ";
		$sql .= " jinji_auth_refer.auth_flg ";
		$sql .= "FROM ";
		$sql .= " jinji_manage_category ";
		$sql .= "LEFT JOIN ";
		$sql .= " jinji_auth_refer ";
		$sql .= "ON ";
		$sql .= " jinji_auth_refer.jinji_id = ? AND ";
		$sql .= " jinji_auth_refer.item_ctg = jinji_manage_category.item_ctg ";
		$sql .= "WHERE ";
		$sql .= " jinji_manage_category.del_flg = 'f' ";
		$sql .= "ORDER BY ";
		$sql .= " jinji_manage_category.order_num ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getAuthRefer().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getAuthRefer().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//編集権限取得
	function getAuthEdit($jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);
		
		$sql  = "SELECT ";
		$sql .= " jinji_manage_category.item_ctg, ";
		$sql .= " jinji_manage_category.ctg_name, ";
		$sql .= " jinji_auth_edit.auth_flg ";
		$sql .= "FROM ";
		$sql .= " jinji_manage_category ";
		$sql .= "LEFT JOIN ";
		$sql .= " jinji_auth_edit ";
		$sql .= "ON ";
		$sql .= " jinji_auth_edit.jinji_id = ? AND ";
		$sql .= " jinji_auth_edit.item_ctg = jinji_manage_category.item_ctg ";
		$sql .= "WHERE ";
		$sql .= " jinji_manage_category.del_flg = 'f' ";
		$sql .= "ORDER BY ";
		$sql .= " jinji_manage_category.order_num ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getAuthEdit().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getAuthEdit().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//一覧件数取得
	function getListCount($user_jinjiID, $limit, $seEmpID, $seEmpName, 
						  $seClass, $seAtrb, $seDept, $seRoom, $seJob, 
						  $seKey1, $seKey2, $seKey3, $seKey4, $seKey5, 
						  $delFlg, &$dataNum)
	{
		//管理対象職種 取得
		$types = array("text");
		$data = array($user_jinjiID);
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " job_id ";
		$sql .= "FROM ";
		$sql .= " jinji_auth_job ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? AND ";
		$sql .= " auth_flg = 't' ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getListCount().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getListCount().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		$jobArr = "";
		$jobWH  = "";
		if(count($result) > 0){
			for($i=0;$i<count($result);$i++){
				$jobArr[$i] = " emp_job = ".$result[$i]["job_id"]." ";
			}
			$jobWH = " AND (".join(" OR ", $jobArr).") ";
		}else{
			return "";
		}

		$fullnm = str_replace(" ", "", $seEmpName);
		$fullnm = str_replace("　", "", $fullnm);
		$seKey1 = str_replace(" ", "", $seKey1);
		$seKey2 = str_replace("　", "", $seKey2);
		$seKey3 = str_replace(" ", "", $seKey3);
		$seKey4 = str_replace("　", "", $seKey4);
		$seKey5 = str_replace(" ", "", $seKey5);

		$types = array("text", 
					   "text", 
					   "text", 
					   "integer", 
					   "integer", 
					   "integer", 
					   "integer", 
					   "integer", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text");
					   
		$data  = array("seEmpID"		=>$seEmpID, 
					   "LIKE_seEmpName"	=>"%".$seEmpName."%", 
					   "LIKE_fullnm"	=>"%".$fullnm."%", 
					   "seClass"		=>$seClass, 
					   "seAtrb"			=>$seAtrb, 
					   "seDept"			=>$seDept, 
					   "seRoom"			=>$seRoom, 
					   "seJob"			=>$seJob, 
					   "seKey1"			=>"%".$seKey1."%", 
					   "seKey2"			=>"%".$seKey2."%", 
					   "seKey3"			=>"%".$seKey3."%", 
					   "seKey4"			=>"%".$seKey4."%", 
					   "seKey5"			=>"%".$seKey5."%", 
					   "delFlg"			=>$delFlg);

		$sqlWhe  = "";
		//ID
		if ($seEmpID != "") {
			$sqlWhe .= " emp_personal_id = :seEmpID ";
		}
		//名前
		if ($seEmpName != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (emp_lt_nm LIKE :LIKE_seEmpName OR";
			$sqlWhe .= "  emp_ft_nm LIKE :LIKE_seEmpName OR ";
			$sqlWhe .= "  emp_kn_lt_nm LIKE :LIKE_seEmpName OR ";
			$sqlWhe .= "  emp_kn_ft_nm LIKE :LIKE_seEmpName OR ";
			$sqlWhe .= "  (emp_lt_nm || emp_ft_nm) LIKE :LIKE_fullnm OR ";
			$sqlWhe .= "  (emp_kn_lt_nm || emp_kn_ft_nm) LIKE :LIKE_fullnm) ";
		}
		//部門・課・科・室
		if ($seClass != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (emp_class = :seClass ";
			if ($seAtrb != "") {
				$sqlWhe .= " AND emp_attribute = :seAtrb ";
				if ($seDept != "") {
					$sqlWhe .= " AND emp_dept = :seDept ";
					if ($seRoom != "") {
						$sqlWhe .= " AND emp_room = :seRoom ";
					}
				}
			}
			$sqlWhe .= " ) ";	
		}
		//職種
		if ($seJob != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " emp_job = :seJob ";
		}
		//専門学歴
		if ($seKey1 != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (SELECT ";
			$sqlWhe .= "   COUNT(*) ";
			$sqlWhe .= "  FROM ";
			$sqlWhe .= "   jinji_educational_bg ";
			$sqlWhe .= "  WHERE ";
			$sqlWhe .= "   jinji_educational_bg.jinji_id = jinji_relation.jinji_id AND ";
			$sqlWhe .= "   jinji_educational_bg.school_name LIKE :seKey1 ";
			$sqlWhe .= " ) > 0 ";
		}
		//認定資格
		if ($seKey2 != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (SELECT ";
			$sqlWhe .= "   COUNT(*) ";
			$sqlWhe .= "  FROM ";
			$sqlWhe .= "   jinji_q_pro ";
			$sqlWhe .= "  WHERE ";
			$sqlWhe .= "   jinji_q_pro.jinji_id = jinji_relation.jinji_id AND ";
			$sqlWhe .= "   (jinji_q_pro.q_pro_field LIKE :seKey2 OR  ";
			$sqlWhe .= "    jinji_q_pro.q_pro_agency LIKE :seKey2)  ";
			$sqlWhe .= " ) > 0 ";
		}
		//院外委員会
		if ($seKey3 != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (SELECT ";
			$sqlWhe .= "   COUNT(*) ";
			$sqlWhe .= "  FROM ";
			$sqlWhe .= "   jinji_committee ";
			$sqlWhe .= "  WHERE ";
			$sqlWhe .= "   jinji_committee.jinji_id = jinji_relation.jinji_id AND ";
			$sqlWhe .= "   jinji_committee.committee_name LIKE :seKey3 ";
			$sqlWhe .= " ) > 0 ";
		}
		//院外発表
		if ($seKey4 != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (SELECT ";
			$sqlWhe .= "   COUNT(*) ";
			$sqlWhe .= "  FROM ";
			$sqlWhe .= "   jinji_presentation ";
			$sqlWhe .= "  WHERE ";
			$sqlWhe .= "   jinji_presentation.jinji_id = jinji_relation.jinji_id AND ";
			$sqlWhe .= "   (jinji_presentation.prsnt_theme LIKE :seKey4 OR  ";
			$sqlWhe .= "    jinji_presentation.prsnt_agency LIKE :seKey4)  ";
			$sqlWhe .= " ) > 0 ";
		}
		//院外研修
		if ($seKey5 != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (SELECT ";
			$sqlWhe .= "   COUNT(*) ";
			$sqlWhe .= "  FROM ";
			$sqlWhe .= "   jinji_training ";
			$sqlWhe .= "  WHERE ";
			$sqlWhe .= "   jinji_training.jinji_id = jinji_relation.jinji_id AND ";
			$sqlWhe .= "   (jinji_training.trng_theme LIKE :seKey5 OR  ";
			$sqlWhe .= "    jinji_training.trng_sponsor LIKE :seKey5 OR  ";
			$sqlWhe .= "    jinji_training.trng_venue LIKE :seKey5 OR  ";
			$sqlWhe .= "    jinji_training.trng_cost LIKE :seKey5)  ";
			$sqlWhe .= " ) > 0 ";
		}

		if($sqlWhe != "" ){
			$sqlWhe = " AND (".$sqlWhe.") ";
		}
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " empmst.emp_id, ";
		$sql .= " empmst.emp_personal_id, ";
		$sql .= " empmst.emp_lt_nm, ";
		$sql .= " empmst.emp_ft_nm, ";
		$sql .= " class_nm, ";
		$sql .= " st_nm, ";
		$sql .= " job_nm ";
		$sql .= "FROM ";
		$sql .= " empmst ";
		$sql .= "INNER JOIN ";
		$sql .= " classmst ";
		$sql .= "ON ";
		$sql .= " empmst.emp_class = classmst.class_id ";
		$sql .= "INNER JOIN ";
		$sql .= " stmst ";
		$sql .= "ON ";
		$sql .= " empmst.emp_st = stmst.st_id ";
		$sql .= "INNER JOIN ";
		$sql .= " jobmst ";
		$sql .= "ON ";
		$sql .= " empmst.emp_job = jobmst.job_id ";	
		$sql .= "LEFT JOIN ";
		$sql .= " jinji_relation ";
		$sql .= "ON ";
		$sql .= " jinji_relation.emp_id = empmst.emp_id ";	
		$sql .= "WHERE ";
		$sql .= " EXISTS ( ";
		$sql .= "  SELECT ";
		$sql .= "   * ";
		$sql .= "  FROM ";
		$sql .= "   authmst ";
		$sql .= "  WHERE ";
		$sql .= "   authmst.emp_id = empmst.emp_id and ";
		$sql .= "   emp_del_flg = :delFlg) ";
		$sql .= $jobWH.$sqlWhe;
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getListCount().3] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getListCount().4] ".$res->getDebugInfo());
			return "err_exit";
		}
		$dataNum = $res->numRows();
		$stmt->free();
		$res->free();
		return "";
	}

	//一覧表示データ取得
	function getList($user_jinjiID, $limit, $seEmpID, $seEmpName, 
					 $seClass, $seAtrb, $seDept, $seRoom, $seJob, 
					 $seKey1, $seKey2, $seKey3, $seKey4, $seKey5, 
					 $delFlg, $seSort, $sePage)
	{
		//管理対象職種 取得
		$types = array("text");
		$data = array($user_jinjiID);
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " job_id ";
		$sql .= "FROM ";
		$sql .= " jinji_auth_job ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? AND ";
		$sql .= " auth_flg = 't' ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getListCount().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getListCount().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		$jobArr = "";
		$jobWH  = "";
		if(count($result) > 0){
			for($i=0;$i<count($result);$i++){
				$jobArr[$i] = " emp_job = ".$result[$i]["job_id"]." ";
			}
			$jobWH = " AND (".join(" OR ", $jobArr).") ";
		}else{
			return "";
		}


		$fullnm = str_replace(" ", "", $seEmpName);
		$fullnm = str_replace("　", "", $fullnm);
		$seKey1 = str_replace(" ", "", $seKey1);
		$seKey2 = str_replace("　", "", $seKey2);
		$seKey3 = str_replace(" ", "", $seKey3);
		$seKey4 = str_replace("　", "", $seKey4);
		$seKey5 = str_replace(" ", "", $seKey5);

		$types = array("text", 
					   "text", 
					   "text", 
					   "integer", 
					   "integer", 
					   "integer", 
					   "integer", 
					   "integer", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text", 
					   "text");
					   
		$data  = array("seEmpID"		=>$seEmpID, 
					   "LIKE_seEmpName"	=>"%".$seEmpName."%", 
					   "LIKE_fullnm"	=>"%".$fullnm."%", 
					   "seClass"		=>$seClass, 
					   "seAtrb"			=>$seAtrb, 
					   "seDept"			=>$seDept, 
					   "seRoom"			=>$seRoom, 
					   "seJob"			=>$seJob, 
					   "seKey1"			=>"%".$seKey1."%", 
					   "seKey2"			=>"%".$seKey2."%", 
					   "seKey3"			=>"%".$seKey3."%", 
					   "seKey4"			=>"%".$seKey4."%", 
					   "seKey5"			=>"%".$seKey5."%", 
					   "delFlg"			=>$delFlg);

		$sqlWhe  = "";
		//ID
		if ($seEmpID != "") {
			$sqlWhe .= " emp_personal_id = :seEmpID ";
		}
		//名前
		if ($seEmpName != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (emp_lt_nm LIKE :LIKE_seEmpName OR";
			$sqlWhe .= "  emp_ft_nm LIKE :LIKE_seEmpName OR ";
			$sqlWhe .= "  emp_kn_lt_nm LIKE :LIKE_seEmpName OR ";
			$sqlWhe .= "  emp_kn_ft_nm LIKE :LIKE_seEmpName OR ";
			$sqlWhe .= "  (emp_lt_nm || emp_ft_nm) LIKE :LIKE_fullnm OR ";
			$sqlWhe .= "  (emp_kn_lt_nm || emp_kn_ft_nm) LIKE :LIKE_fullnm) ";
		}
		//部門・課・科・室
		if ($seClass != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (emp_class = :seClass ";
			if ($seAtrb != "") {
				$sqlWhe .= " AND emp_attribute = :seAtrb ";
				if ($seDept != "") {
					$sqlWhe .= " AND emp_dept = :seDept ";
					if ($seRoom != "") {
						$sqlWhe .= " AND emp_room = :seRoom ";
					}
				}
			}
			$sqlWhe .= " ) ";	
		}
		//職種
		if ($seJob != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " emp_job = :seJob ";
		}
		//専門学歴
		if ($seKey1 != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (SELECT ";
			$sqlWhe .= "   COUNT(*) ";
			$sqlWhe .= "  FROM ";
			$sqlWhe .= "   jinji_educational_bg ";
			$sqlWhe .= "  WHERE ";
			$sqlWhe .= "   jinji_educational_bg.jinji_id = jinji_relation.jinji_id AND ";
			$sqlWhe .= "   jinji_educational_bg.school_name LIKE :seKey1 ";
			$sqlWhe .= " ) > 0 ";
		}
		//認定資格
		if ($seKey2 != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (SELECT ";
			$sqlWhe .= "   COUNT(*) ";
			$sqlWhe .= "  FROM ";
			$sqlWhe .= "   jinji_q_pro ";
			$sqlWhe .= "  WHERE ";
			$sqlWhe .= "   jinji_q_pro.jinji_id = jinji_relation.jinji_id AND ";
			$sqlWhe .= "   (jinji_q_pro.q_pro_field LIKE :seKey2 OR  ";
			$sqlWhe .= "    jinji_q_pro.q_pro_agency LIKE :seKey2)  ";
			$sqlWhe .= " ) > 0 ";
		}
		//院外委員会
		if ($seKey3 != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (SELECT ";
			$sqlWhe .= "   COUNT(*) ";
			$sqlWhe .= "  FROM ";
			$sqlWhe .= "   jinji_committee ";
			$sqlWhe .= "  WHERE ";
			$sqlWhe .= "   jinji_committee.jinji_id = jinji_relation.jinji_id AND ";
			$sqlWhe .= "   jinji_committee.committee_name LIKE :seKey3 ";
			$sqlWhe .= " ) > 0 ";
		}
		//院外発表
		if ($seKey4 != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (SELECT ";
			$sqlWhe .= "   COUNT(*) ";
			$sqlWhe .= "  FROM ";
			$sqlWhe .= "   jinji_presentation ";
			$sqlWhe .= "  WHERE ";
			$sqlWhe .= "   jinji_presentation.jinji_id = jinji_relation.jinji_id AND ";
			$sqlWhe .= "   (jinji_presentation.prsnt_theme LIKE :seKey4 OR  ";
			$sqlWhe .= "    jinji_presentation.prsnt_agency LIKE :seKey4)  ";
			$sqlWhe .= " ) > 0 ";
		}
		//院外研修
		if ($seKey5 != "") {
			if($sqlWhe != "" ){
				$sqlWhe .= " AND ";
			}
			$sqlWhe .= " (SELECT ";
			$sqlWhe .= "   COUNT(*) ";
			$sqlWhe .= "  FROM ";
			$sqlWhe .= "   jinji_training ";
			$sqlWhe .= "  WHERE ";
			$sqlWhe .= "   jinji_training.jinji_id = jinji_relation.jinji_id AND ";
			$sqlWhe .= "   (jinji_training.trng_theme LIKE :seKey5 OR  ";
			$sqlWhe .= "    jinji_training.trng_sponsor LIKE :seKey5 OR  ";
			$sqlWhe .= "    jinji_training.trng_venue LIKE :seKey5 OR  ";
			$sqlWhe .= "    jinji_training.trng_cost LIKE :seKey5)  ";
			$sqlWhe .= " ) > 0 ";
		}

		if($sqlWhe != "" ){
			$sqlWhe = " AND (".$sqlWhe.") ";
		}
				
		//ソート条件
		$sqlOrd = "";
		$arrSort = "";
		if ($seSort == "1") {
			$sqlOrd .= " ORDER BY emp_personal_id";
		} else if ($seSort == "2") {
			$sqlOrd .= " ORDER BY emp_personal_id DESC";
		} else if ($seSort == "3") {
			$sqlOrd .= " ORDER BY emp_lt_nm, emp_ft_nm";
		} else {
			$sqlOrd .= " ORDER BY emp_lt_nm DESC, emp_ft_nm DESC";
		}
		//取得件数
		$sqlLim = "";
		if ($sePage == "0" || $sePage == "") {
			$sqlLim .= " LIMIT ".$limit."";
		} else {
			$offset = $sePage * $limit;
			$sqlLim .= " LIMIT ".$limit." OFFSET ".$offset."";
		}
		
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " empmst.emp_id, ";
		$sql .= " empmst.emp_personal_id, ";
		$sql .= " empmst.emp_lt_nm, ";
		$sql .= " empmst.emp_ft_nm, ";
		$sql .= " class_nm, ";
		$sql .= " st_nm, ";
		$sql .= " job_nm ";
		$sql .= "FROM ";
		$sql .= " empmst ";
		$sql .= "INNER JOIN ";
		$sql .= " classmst ";
		$sql .= "ON ";
		$sql .= " empmst.emp_class = classmst.class_id ";
		$sql .= "INNER JOIN ";
		$sql .= " stmst ";
		$sql .= "ON ";
		$sql .= " empmst.emp_st = stmst.st_id ";
		$sql .= "INNER JOIN ";
		$sql .= " jobmst ";
		$sql .= "ON ";
		$sql .= " empmst.emp_job = jobmst.job_id ";	
		$sql .= "LEFT JOIN ";
		$sql .= " jinji_relation ";
		$sql .= "ON ";
		$sql .= " jinji_relation.emp_id = empmst.emp_id ";	
		$sql .= "WHERE ";
		$sql .= " EXISTS ( ";
		$sql .= "  SELECT ";
		$sql .= "   * ";
		$sql .= "  FROM ";
		$sql .= "   authmst ";
		$sql .= "  WHERE ";
		$sql .= "   authmst.emp_id = empmst.emp_id and ";
		$sql .= "   emp_del_flg = :delFlg) ";
		$sql .= $jobWH.$sqlWhe.$sqlOrd.$sqlLim;
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getList().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getList().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}
	
	//所属名称
	function getClassName()
	{
		$sql  = "SELECT ";
		$sql .= " class_nm, ";
		$sql .= " atrb_nm, ";
		$sql .= " dept_nm, ";
		$sql .= " room_nm, ";
		$sql .= " class_cnt ";
		$sql .= "FROM ";
		$sql .= " classname";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getClassName().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getClassName().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		if (count($result) == 0) {
			$this->objLog->warn("getClassName 該当データなし");
			return "err_exit";
		}
		
		return $result;

	}

	//部門リスト
	function getClassList()
	{
		$sql  = "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " classmst ";
		$sql .= "WHERE ";
		$sql .= " class_del_flg = 'f' ";
		$sql .= "ORDER BY ";
		$sql .= " class_id ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getClassList().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getClassList().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//課リスト
	function getAtrbList($prm)
	{
		$types = array("integer");
		$data = array($prm);

		$sql  = "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " atrbmst ";
		$sql .= "WHERE ";
		$sql .= " class_id = ? AND ";
		$sql .= " atrb_del_flg = 'f' ";
		$sql .= "ORDER BY ";
		$sql .= " atrb_id ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getAtrbList().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getAtrbList().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//科リスト
	function getDeptList($prm)
	{
		$types = array("integer");
		$data = array($prm);

		$sql  = "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " deptmst ";
		$sql .= "WHERE ";
		$sql .= " atrb_id = ? AND ";
		$sql .= " dept_del_flg = 'f' ";
		$sql .= "ORDER BY ";
		$sql .= " dept_id ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getDeptList().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getDeptList().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//室リスト
	function getRoomList($prm)
	{
		$types = array("integer");
		$data = array($prm);

		$sql  = "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " classroom ";
		$sql .= "WHERE ";
		$sql .= " dept_id = ? AND ";
		$sql .= " room_del_flg = 'f' ";
		$sql .= "ORDER BY ";
		$sql .= " room_id ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getRoomList().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getRoomList().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//職種リスト
	function getJobList($jinjiID)
	{
		$types = array("text");
		$data = array($jinjiID);

		$sql  = "SELECT ";
		$sql .= " jobmst.job_id, ";
		$sql .= " jobmst.job_nm, ";
		$sql .= " jobmst.order_no ";
		$sql .= "FROM ";
		$sql .= " jinji_auth_job ";
		$sql .= "JOIN ";
		$sql .= " jobmst ";
		$sql .= "ON ";
		$sql .= " jobmst.job_id = jinji_auth_job.job_id ";
		$sql .= "WHERE ";
		$sql .= " jinji_auth_job.jinji_id = ? AND ";
		$sql .= " jinji_auth_job.auth_flg = 't' ";
		$sql .= "ORDER BY ";
		$sql .= " jobmst.order_no ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getJobList().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getJobList().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}
	
	//課リスト 切替用データ
	function getAtrbAllData()
	{
		$sql  = "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " atrbmst ";
		$sql .= "WHERE ";
		$sql .= " atrb_del_flg = 'f' ";
		$sql .= "ORDER BY ";
		$sql .= " atrb_id ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getAtrbAllData().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getAtrbAllData().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//科リスト 切替用データ
	function getDeptAllData()
	{
		$sql  = "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " deptmst ";
		$sql .= "WHERE ";
		$sql .= " dept_del_flg = 'f' ";
		$sql .= "ORDER BY ";
		$sql .= " dept_id ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getDeptAllData().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getDeptAllData().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}

	//室リスト 切替用データ
	function getRoomAllData()
	{
		$sql  = "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " classroom ";
		$sql .= "WHERE ";
		$sql .= " room_del_flg = 'f' ";
		$sql .= "ORDER BY ";
		$sql .= " room_id ";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getRoomAllData().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getRoomAllData().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}
	
	//帳票出力権限
	function getAuthPrint($jinjiID)
	{
		$types = array("text");
		$data  = array($jinjiID);
		//SQL生成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " print_auth_iryo ";
		$sql .= "FROM ";
		$sql .= " jinji_relation ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getAuthPrint().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getAuthPrint().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}	

}

?>