<?php
class JinjiControlCsvUpdateModel
{
    protected $empID;
    protected $mdb2;
    protected $objLog;

	public function __construct($log, $conString, $id, &$sts)
	{
		$this->empID = $id;
		$this->objLog = $log;
		$con =& MDB2::connect($conString);
		if(PEAR::isError($con)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")DB接続エラー： ".$con->getDebugInfo());
			$sts = "err_exit";
			return;
		} 
		$this->mdb2 = $con;
	}

	//トランザクション　スタート
	function startTransaction()
	{
		$res = $this->mdb2->beginTransaction();
		// エラー処理 
		if(PEAR::isError($res)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")トランザクション開始エラー： ".$res->getDebugInfo());
			return "err_exit";
		} 
	}
	
	//トランザクション　コミット
	function commitTransaction()
	{
		$res = $this->mdb2->commit();
		// エラー処理 
		if(PEAR::isError($res)) 
		{ 
			$this->objLog->error("(ID:".$this->empID.")トランザクションコミットエラー： ".$res->getDebugInfo());
			return "err_exit";
		} 
	}
	
	//表示設定取得
	function getDispItemInfo()
	{
		$sql  = "SELECT ";
		$sql .= " item_id, ";
		$sql .= " item_ctg, ";
		$sql .= " item_name, ";
		$sql .= " item_flg ";
		$sql .= "FROM ";
		$sql .= " jinji_manage_item ";
		$sql .= "WHERE ";
		$sql .= " del_flg = 'f'";
		$stmt=$this->mdb2->prepare($sql);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[getDispItemInfo().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute();
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[getDispItemInfo().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();

		return $result;
	}
	
	//職員ID正当性
	function chk_csvID($uid){
	
		$types = array("text");
		$data  = array($uid);
		
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " empmst ";
		$sql .= "WHERE ";
		$sql .= " emp_personal_id = ? ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$this->objLog->error("SQL準備エラー：[chk_csvID().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$this->objLog->error("SQL実行エラー：[chk_csvID().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		
		return $result;
	}
	
	
	//jinji_work_history 更新
	function upWorkHistory($jinjiID, $hiho, $kiso, $kose, $koyo){
	
		//レコード存在チェック
		$types = array("text");
		$data  = array($jinjiID);
		
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " jinji_work_history ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[upWorkHistory().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[upWorkHistory().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$result = $res->fetchAll(MDB2_FETCHMODE_ASSOC);
		$stmt->free();
		$res->free();
		
		//更新
		$types = array();
		$data  = array();
		$types = array("text", 
					   "text",
					   "text",
					   "text",
					   "text");
					   
		$data  = array("jinji_id"		=>$jinjiID, 
					   "kenko_number"	=>$hiho, 
					   "kiso_number"	=>$kiso, 
					   "kosei_number"	=>$kose, 
					   "koyo_number"	=>$koyo);
		if(count($result) > 0){
			$sql  = "";
			$sql .= "UPDATE ";
			$sql .= " jinji_work_history ";
			$sql .= "SET ";
			$sql .= " kenko_number = :kenko_number, ";
			$sql .= " kiso_number  = :kiso_number, ";
			$sql .= " kosei_number = :kosei_number, ";
			$sql .= " koyo_number  = :koyo_number ";
			$sql .= "WHERE ";
			$sql .= " jinji_id = :jinji_id ";
		}else{
			$sql  = "";
			$sql .= "INSERT INTO ";
			$sql .= " jinji_work_history ";
			$sql .= "(jinji_id, kenko_number, kiso_number, kosei_number, koyo_number) ";
			$sql .= "VALUES(:jinji_id, :kenko_number, :kiso_number, :kosei_number, :koyo_number) ";
		}
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[upWorkHistory().3] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[upWorkHistory().4] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	
	}

	//jinji_educational_bg 追加
	function upEducation($jinjiID, $ed_nm, $ed_dt){

		$types = array("text", 
					   "text",
					   "text");
					   
		$data  = array("jinji_id"			=>$jinjiID, 
					   "school_name"		=>$ed_nm, 
					   "graduation_date"	=>$ed_dt);
		$sql  = "";
		$sql .= "INSERT INTO ";
		$sql .= " jinji_educational_bg ";
		$sql .= "(jinji_id, school_name, graduation_date) ";
		$sql .= "VALUES(:jinji_id, :school_name, :graduation_date) ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[upEducation().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[upEducation().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();

	}
	
	//jinji_q_pro 追加
	function upQualifiPro($jinjiID, $bunya, $kikan, $pro_dt){
	
		$types = array("text", 
					   "text",
					   "text",
					   "text");
					   
		$data  = array("jinji_id"		=>$jinjiID, 
					   "q_pro_field"	=>$bunya, 
					   "q_pro_agency"	=>$kikan, 
					   "q_pro_date" 	=>$pro_dt);
		$sql  = "";
		$sql .= "INSERT INTO ";
		$sql .= " jinji_q_pro ";
		$sql .= "(jinji_id, q_pro_field, q_pro_agency, q_pro_date) ";
		$sql .= "VALUES(:jinji_id, :q_pro_field, :q_pro_agency, :q_pro_date) ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[upQualifiPro().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[upQualifiPro().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();
	
	}

	//jinji_license 追加
	function upLicense($jinjiID, $id, $l_no, $l_dt, $l_prv){
	
		$types = array("text", 
					   "integer",
					   "text",
					   "text",
					   "text");
					   
		$data  = array("jinji_id"		=>$jinjiID, 
					   "item_id"		=>$id, 
					   "license_no"		=>$l_no, 
					   "license_date"	=>$l_dt, 
					   "license_prv" 	=>$l_prv);
		$sql  = "";
		$sql .= "INSERT INTO ";
		$sql .= " jinji_license ";
		$sql .= "(jinji_id, item_id, license_no, license_date, license_prv) ";
		$sql .= "VALUES(:jinji_id, :item_id, :license_no, :license_date, :license_prv) ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[upLicense().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[upLicense().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();

	}

	//jinji_relation 更新
	function upJinjiRelation($jinjiID, $wk_tm){

		$types = array("text", 
					   "text");
					   
		$data  = array("jinji_id"		=>$jinjiID, 
					   "working_time" 	=>$wk_tm);

		$sql  = "";
		$sql .= "UPDATE ";
		$sql .= " jinji_relation ";
		$sql .= "SET ";
		$sql .= " working_time = :working_time ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = :jinji_id ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[upJinjiRel().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[upJinjiRel().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();

	}

	//削除処理
	function delJinjiData($jinjiID, $tblName)
	{
		$types = array("text");
		$data = array($jinjiID);
		//SQL生成
		$sql  = "";
		$sql .= "DELETE FROM ";
		$sql .= " ".$tblName." ";
		$sql .= "WHERE ";
		$sql .= " jinji_id = ? ";
		$stmt=$this->mdb2->prepare($sql, $types);
		if (PEAR::isError($stmt)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL準備エラー：[delJinjiData().1] ".$stmt->getDebugInfo());
			return "err_exit";
		}
		$res = $stmt->execute($data);
		if (PEAR::isError($res)){
			$ret = $this->mdb2->rollback();	//ロールバック
			$this->objLog->error("SQL実行エラー：[delJinjiData().2] ".$res->getDebugInfo());
			return "err_exit";
		}
		$stmt->free();
		$res->free();

		return "";
	}

}

?>