<?php
//************************************
//人事管理機能 管理　CSVインポート
//************************************

//====================================
//自ファイル名設定
//====================================
$fname = basename(__FILE__);

//====================================
//共通初期設定
//====================================
require_once("./jinji/jinji_common_init.php");

//====================================
//Smarty設定
//====================================
require_once("./jinji/conf/smarty.conf");

//====================================
//人事管理　CSVインポート専用モデル
//====================================
require_once("./jinji/model/jinji_control_csv_model.php");
$mdb2 = new JinjiControlCsvModel($log, CMX_DB_DSN, $jinji_UserEmpID, $sts);
if($sts == "err_exit") err_exit();

//====================================
//処理開始ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート：処理開始");

//====================================
//フォームデータ取得
//====================================
$encoding = $_REQUEST["encoding"];

if($encoding == ""){
	$encoding = "1";
}

$result = $_REQUEST["result"];

//■表示項目■■■■■■■■■■■■■■■■■■■
$res = $mdb2->getDispItemInfo();
if($res == "err_exit") err_exit();
$dispInfo = "";
for($i=0;$i<count($res);$i++){
	$dispInfo[$res[$i]["item_id"]] = $res[$i];
}

//CSVフォーマット説明
$csvFrmt = array();

$csvFrmt[0]["dataNo"] 		= "1";
$csvFrmt[0]["dataName"] 	= "職員ID";
$csvFrmt[0]["dataHissu"] 	= "必須";
$csvFrmt[0]["dataMemo"] 	= "<font color='#FF0000'><b>必須</b></font>（存在しない場合はエラー）";
$csvFrmt[0]["dataUpdate"] 	= "無し";

$csvFrmt[1]["dataNo"] 		= "2";
$csvFrmt[1]["dataName"] 	= "氏名";
$csvFrmt[1]["dataHissu"] 	= "";
$csvFrmt[1]["dataMemo"] 	= "確認用の参考項目（更新されません）";
$csvFrmt[1]["dataUpdate"] 	= "無し";

$csvFrmt[2]["dataNo"] 		= "3";
$csvFrmt[2]["dataName"] 	= $dispInfo[8]["item_name"];
$csvFrmt[2]["dataHissu"] 	= flg2str_hissu($dispInfo[8]["item_flg"]);
$csvFrmt[2]["dataMemo"] 	= "";
$csvFrmt[2]["dataUpdate"] 	= "上書";

$csvFrmt[3]["dataNo"] 		= "4";
$csvFrmt[3]["dataName"] 	= $dispInfo[9]["item_name"];
$csvFrmt[3]["dataHissu"] 	= flg2str_hissu($dispInfo[9]["item_flg"]);
$csvFrmt[3]["dataMemo"] 	= "";
$csvFrmt[3]["dataUpdate"] 	= "上書";

$csvFrmt[4]["dataNo"] 		= "5";
$csvFrmt[4]["dataName"] 	= $dispInfo[10]["item_name"];
$csvFrmt[4]["dataHissu"] 	= flg2str_hissu($dispInfo[10]["item_flg"]);
$csvFrmt[4]["dataMemo"] 	= "";
$csvFrmt[4]["dataUpdate"] 	= "上書";

$csvFrmt[5]["dataNo"] 		= "6";
$csvFrmt[5]["dataName"] 	= $dispInfo[11]["item_name"];
$csvFrmt[5]["dataHissu"] 	= flg2str_hissu($dispInfo[11]["item_flg"]);
$csvFrmt[5]["dataMemo"] 	= "";
$csvFrmt[5]["dataUpdate"] 	= "上書";

$csvFrmt[6]["dataNo"] 		= "7";
$csvFrmt[6]["dataName"] 	= $dispInfo[12]["item_name"]." 学校名称";
$csvFrmt[6]["dataHissu"] 	= flg2str_hissu($dispInfo[12]["item_flg"]);
$csvFrmt[6]["dataMemo"] 	= "";
$csvFrmt[6]["dataUpdate"] 	= "追加";

$csvFrmt[7]["dataNo"] 		= "8";
$csvFrmt[7]["dataName"] 	= $dispInfo[12]["item_name"]." 卒業年月日";
$csvFrmt[7]["dataHissu"] 	= flg2str_hissu($dispInfo[12]["item_flg"]);
$csvFrmt[7]["dataMemo"] 	= "YYYYMMDD形式";
$csvFrmt[7]["dataUpdate"] 	= "追加";

$csvFrmt[8]["dataNo"] 		= "9";
$csvFrmt[8]["dataName"] 	= $dispInfo[13]["item_name"]." 分野";
$csvFrmt[8]["dataHissu"] 	= flg2str_hissu($dispInfo[13]["item_flg"]);
$csvFrmt[8]["dataMemo"] 	= "";
$csvFrmt[8]["dataUpdate"] 	= "追加";

$csvFrmt[9]["dataNo"] 		= "10";
$csvFrmt[9]["dataName"] 	= $dispInfo[13]["item_name"]." 認定機関";
$csvFrmt[9]["dataHissu"] 	= flg2str_hissu($dispInfo[13]["item_flg"]);
$csvFrmt[9]["dataMemo"] 	= "";
$csvFrmt[9]["dataUpdate"] 	= "追加";

$csvFrmt[10]["dataNo"] 		= "11";
$csvFrmt[10]["dataName"] 	= $dispInfo[13]["item_name"]." 認定年月日";
$csvFrmt[10]["dataHissu"] 	= flg2str_hissu($dispInfo[13]["item_flg"]);
$csvFrmt[10]["dataMemo"] 	= "YYYYMMDD形式";
$csvFrmt[10]["dataUpdate"] 	= "追加";

$csvFrmt[11]["dataNo"] 		= "12";
$csvFrmt[11]["dataName"] 	= $dispInfo[15]["item_name"]." 免許番号";
$csvFrmt[11]["dataHissu"] 	= flg2str_hissu($dispInfo[15]["item_flg"]);
$csvFrmt[11]["dataMemo"] 	= "";
$csvFrmt[11]["dataUpdate"] 	= "上書";

$csvFrmt[12]["dataNo"] 		= "13";
$csvFrmt[12]["dataName"] 	= $dispInfo[15]["item_name"]." 取得年月日";
$csvFrmt[12]["dataHissu"] 	= flg2str_hissu($dispInfo[15]["item_flg"]);
$csvFrmt[12]["dataMemo"] 	= "YYYYMMDD形式";
$csvFrmt[12]["dataUpdate"] 	= "上書";

$csvFrmt[13]["dataNo"] 		= "14";
$csvFrmt[13]["dataName"] 	= $dispInfo[16]["item_name"]." 免許番号";
$csvFrmt[13]["dataHissu"] 	= flg2str_hissu($dispInfo[16]["item_flg"]);
$csvFrmt[13]["dataMemo"] 	= "";
$csvFrmt[13]["dataUpdate"] 	= "上書";

$csvFrmt[14]["dataNo"] 		= "15";
$csvFrmt[14]["dataName"] 	= $dispInfo[16]["item_name"]." 取得年月日";
$csvFrmt[14]["dataHissu"] 	= flg2str_hissu($dispInfo[16]["item_flg"]);
$csvFrmt[14]["dataMemo"] 	= "YYYYMMDD形式";
$csvFrmt[14]["dataUpdate"] 	= "上書";

$csvFrmt[15]["dataNo"] 		= "16";
$csvFrmt[15]["dataName"] 	= $dispInfo[17]["item_name"]." 免許番号";
$csvFrmt[15]["dataHissu"] 	= flg2str_hissu($dispInfo[17]["item_flg"]);
$csvFrmt[15]["dataMemo"] 	= "";
$csvFrmt[15]["dataUpdate"] 	= "上書";

$csvFrmt[16]["dataNo"] 		= "17";
$csvFrmt[16]["dataName"] 	= $dispInfo[17]["item_name"]." 取得年月日";
$csvFrmt[16]["dataHissu"] 	= flg2str_hissu($dispInfo[17]["item_flg"]);
$csvFrmt[16]["dataMemo"] 	= "YYYYMMDD形式";
$csvFrmt[16]["dataUpdate"] 	= "上書";

$csvFrmt[17]["dataNo"] 		= "18";
$csvFrmt[17]["dataName"] 	= $dispInfo[18]["item_name"]." 免許番号";
$csvFrmt[17]["dataHissu"] 	= flg2str_hissu($dispInfo[18]["item_flg"]);
$csvFrmt[17]["dataMemo"] 	= "";
$csvFrmt[17]["dataUpdate"] 	= "上書";

$csvFrmt[18]["dataNo"] 		= "19";
$csvFrmt[18]["dataName"] 	= $dispInfo[18]["item_name"]." 都道府県";
$csvFrmt[18]["dataHissu"] 	= flg2str_hissu($dispInfo[18]["item_flg"]);
$csvFrmt[18]["dataMemo"] 	= "例）○○県";
$csvFrmt[18]["dataUpdate"] 	= "上書";

$csvFrmt[19]["dataNo"] 		= "20";
$csvFrmt[19]["dataName"] 	= $dispInfo[18]["item_name"]." 取得年月日";
$csvFrmt[19]["dataHissu"] 	= flg2str_hissu($dispInfo[18]["item_flg"]);
$csvFrmt[19]["dataMemo"] 	= "YYYYMMDD形式";
$csvFrmt[19]["dataUpdate"] 	= "上書";

$csvFrmt[20]["dataNo"] 		= "21";
$csvFrmt[20]["dataName"] 	= "勤務日及び勤務時間";
$csvFrmt[20]["dataHissu"] 	= "";
$csvFrmt[20]["dataMemo"] 	= "";
$csvFrmt[20]["dataUpdate"] 	= "上書";


function flg2str_hissu($prm){
	if($prm == 2){
		return "必須";
	}else{
		return "&nbsp;";
	}
}



//====================================
//画面データ・出力
//====================================
$smarty->assign("session", $session);
$smarty->assign("encoding", $encoding);
$smarty->assign("csvFrmt", $csvFrmt);
$smarty->assign("result", $result);


$smarty->display(basename(__FILE__, ".php").".tpl");

//====================================
//処理終了ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート：処理終了");

?>