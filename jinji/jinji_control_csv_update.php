<?php
//************************************
//人事管理機能 管理　CSV登録処理
//************************************

//====================================
//自ファイル名設定
//====================================
$fname = basename(__FILE__);

//====================================
//共通初期設定
//====================================
require_once("./jinji/jinji_common_init.php");

//====================================
//Smarty設定
//====================================
require_once("./jinji/conf/smarty.conf");

//====================================
//人事管理　CSVインポート専用モデル
//====================================
require_once("./jinji/model/jinji_control_csv_update_model.php");
$mdb2 = new JinjiControlCsvUpdateModel($log, CMX_DB_DSN, $jinji_UserEmpID, $sts);
if($sts == "err_exit") err_exit();

//====================================
//処理開始ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理開始");

//====================================
//フォームデータ取得
//====================================
$session   = $_REQUEST["session"];
$encoding  = $_REQUEST["encoding"];

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
</head>
<body>
<form name="frmJinji" method="post" action="./jinji_menu.php">
<input type="hidden" name="session"      value="<?php echo $session; ?>">
<input type="hidden" name="encoding" value="<?php echo $encoding; ?>">
<input type="hidden" name="result" value="">
<input type="hidden" name="mode" value="ctrl_csv">

</form>
</body>
</html>

<?php	

// ファイルが正常にアップロードされたかどうかチェック
$uploaded = false;
if (array_key_exists("csvfile", $_FILES)) {
	if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
		$uploaded = true;
	}
}
if (!$uploaded) {
	echo("<script type=\"text/javascript\">alert('ファイルのアップロードに失敗しました。');</script>\n");
	echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
	echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(アップロード失敗)");
	exit;
}

// 文字コードの設定
switch ($encoding) {
	case "1":
		$file_encoding = "SJIS";
		break;
	case "2":
		$file_encoding = "EUC-JP";
		break;
	case "3":
		$file_encoding = "UTF-8";
		break;
	default:
		exit;
}

// EUC-JPで扱えない文字列は「・」に変換するよう設定
mb_substitute_character(0x30FB);

//■表示項目■■■■■■■■■■■■■■■■■■■
$res = $mdb2->getDispItemInfo();
if($res == "err_exit") err_exit();
$dispInfo = "";
for($i=0;$i<count($res);$i++){
	$dispInfo[$res[$i]["item_id"]] = $res[$i];
}

// CSVデータを配列に格納
$outArray = array();
$jinjiArray = array();
$csv_data = array();
$csv_no = 0;
$lines = file($_FILES["csvfile"]["tmp_name"]);
foreach ($lines as $line) {
	// 文字コードを変換
	$line = trim(mb_convert_encoding($line, "EUC-JP", $file_encoding));
	// EOFを削除
	$line = str_replace(chr(0x1A), "", $line);
	// 空行は無視
	if ($line == "") {
		continue;
	}
	// カラム数チェック
	$csv_data = split(",", $line);
	if (count($csv_data) != 21) {
		echo("<script type=\"text/javascript\">alert('カラム数が不正です。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(カラム数不正)");
		exit;
	}

	//職員ID確認＆キーID取得・設定
	if($csv_data[0] == "" ){
		echo("<script type=\"text/javascript\">alert('職員IDが不正です。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(職員ID不正)");
		exit;
	}
	$res = array();
	$res = $mdb2->chk_csvID($csv_data[0]);
	if($res == "err_exit") err_exit();
	if(count($res) == 0){
		echo("<script type=\"text/javascript\">alert('職員IDが不正です。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(職員ID不正)");
		exit;
	}else{
		$outArray[$csv_no][0] = $csv_data[0];
		
		//emp_id
		$jinjiArray[$csv_no]["empID"] = $res[0]["emp_id"];

		//jinjiID
		$resC = "";
		$resC = $mdbC->startTransaction();			//トランザクション開始
		if($resC == "err_exit") err_exit();
		
		$jinjiID = $mdbC->getJinjiID($jinjiArray[$csv_no]["empID"]);		//人事ID取得(作成)
		if($jinjiID == "err_exit") err_exit();
		$jinjiArray[$csv_no]["jinjiID"] = $jinjiID;
		
		$res = $mdbC->commitTransaction();			//コミット
		if($res == "err_exit") err_exit();
	}
	
	$outArray[$csv_no][1] = $csv_data[1];	//参照用　なまえ
	
	if (strlen($csv_data[2]) > 100) {
		echo("<script type='text/javascript'>alert('".$dispInfo[8]["item_name"]."が長すぎます。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[8]["item_name"]." 文字超過)");
		exit;
	}else{
		$outArray[$csv_no][2] = $csv_data[2];
	}

	if (strlen($csv_data[3]) > 100) {
		echo("<script type='text/javascript'>alert('".$dispInfo[9]["item_name"]."が長すぎます。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[9]["item_name"]." 文字超過)");
		exit;
	}else{
		$outArray[$csv_no][3] = $csv_data[3];
	}

	if (strlen($csv_data[4]) > 100) {
		echo("<script type='text/javascript'>alert('".$dispInfo[10]["item_name"]."が長すぎます。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[10]["item_name"]." 文字超過)");
		exit;
	}else{
		$outArray[$csv_no][4] = $csv_data[4];
	}

	if (strlen($csv_data[5]) > 100) {
		echo("<script type='text/javascript'>alert('".$dispInfo[11]["item_name"]."が長すぎます。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[11]["item_name"]." 文字超過)");
		exit;
	}else{
		$outArray[$csv_no][5] = $csv_data[5];
	}

	if (strlen($csv_data[6]) > 100) {
		echo("<script type='text/javascript'>alert('".$dispInfo[12]["item_name"]." 学校名称が長すぎます。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[12]["item_name"]." 学校名称 文字超過)");
		exit;
	}else{
		$outArray[$csv_no][6] = $csv_data[6];
	}

	if (chk_csvYMD($csv_data[7]) != "OK") {
		echo("<script type='text/javascript'>alert('".$dispInfo[12]["item_name"]." 卒業年月日が不正です。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[12]["item_name"]." 卒業年月日不正)");
		exit;
	}else{
		$outArray[$csv_no][7] = $csv_data[7];
	}

	if (strlen($csv_data[8]) > 100) {
		echo("<script type='text/javascript'>alert('".$dispInfo[13]["item_name"]." 分野が長すぎます。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[13]["item_name"]." 分野 文字超過)");
		exit;
	}else{
		$outArray[$csv_no][8] = $csv_data[8];
	}

	if (strlen($csv_data[9]) > 100) {
		echo("<script type='text/javascript'>alert('".$dispInfo[13]["item_name"]." 認定機関が長すぎます。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[13]["item_name"]." 認定機関 文字超過)");
		exit;
	}else{
		$outArray[$csv_no][9] = $csv_data[9];
	}

	if (chk_csvYMD($csv_data[10]) != "OK") {
		echo("<script type='text/javascript'>alert('".$dispInfo[13]["item_name"]." 認定年月日が不正です。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[13]["item_name"]." 認定年月日不正)");
		exit;
	}else{
		$outArray[$csv_no][10] = $csv_data[10];
	}

	if (strlen($csv_data[11]) > 100) {
		echo("<script type='text/javascript'>alert('".$dispInfo[15]["item_name"]." 免許番号が長すぎます。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[15]["item_name"]." 免許番号 文字超過)");
		exit;
	}else{
		$outArray[$csv_no][11] = $csv_data[11];
	}

	if (chk_csvYMD($csv_data[12]) != "OK") {
		echo("<script type='text/javascript'>alert('".$dispInfo[15]["item_name"]." 取得年月日が不正です。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[15]["item_name"]." 取得年月日不正)");
		exit;
	}else{
		$outArray[$csv_no][12] = $csv_data[12];
	}

	if (strlen($csv_data[13]) > 100) {
		echo("<script type='text/javascript'>alert('".$dispInfo[16]["item_name"]." 免許番号が長すぎます。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[16]["item_name"]." 免許番号 文字超過)");
		exit;
	}else{
		$outArray[$csv_no][13] = $csv_data[13];
	}

	if (chk_csvYMD($csv_data[14]) != "OK") {
		echo("<script type='text/javascript'>alert('".$dispInfo[16]["item_name"]." 取得年月日が不正です。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[16]["item_name"]." 取得年月日不正)");
		exit;
	}else{
		$outArray[$csv_no][14] = $csv_data[14];
	}

	if (strlen($csv_data[15]) > 100) {
		echo("<script type='text/javascript'>alert('".$dispInfo[17]["item_name"]." 免許番号が長すぎます。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[17]["item_name"]." 免許番号 文字超過)");
		exit;
	}else{
		$outArray[$csv_no][15] = $csv_data[15];
	}

	if (chk_csvYMD($csv_data[16]) != "OK") {
		echo("<script type='text/javascript'>alert('".$dispInfo[17]["item_name"]." 取得年月日が不正です。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[17]["item_name"]." 取得年月日不正)");
		exit;
	}else{
		$outArray[$csv_no][16] = $csv_data[16];
	}

	if (strlen($csv_data[17]) > 100) {
		echo("<script type='text/javascript'>alert('".$dispInfo[18]["item_name"]." 免許番号が長すぎます。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[18]["item_name"]." 免許番号 文字超過)");
		exit;
	}else{
		$outArray[$csv_no][17] = $csv_data[17];
	}

	$retPrev = chk_csvPrev($csv_data[18], $jinjiState);
	if ($retPrev == "NG") {
		echo("<script type='text/javascript'>alert('".$dispInfo[18]["item_name"]." 都道府県名が不正です。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[18]["item_name"]." 都道府県不正)");
		exit;
	}else{
		//$outArray[$csv_no][18] = $csv_data[18];
		$outArray[$csv_no][18] = $retPrev;
	}

	if (chk_csvYMD($csv_data[19]) != "OK") {
		echo("<script type='text/javascript'>alert('".$dispInfo[18]["item_name"]." 取得年月日が不正です。レコード番号：{".($csv_no + 1)."}');</script>\n");
		echo("<script language='javascript'>document.frmJinji.result.value='f';</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了(".$dispInfo[18]["item_name"]." 取得年月日不正)");
		exit;
	}else{
		$outArray[$csv_no][19] = $csv_data[19];
	}

	$outArray[$csv_no][20] = $csv_data[20];

	$csv_no++;
}

function chk_csvYMD($prm){
	
	if(strlen($prm) == 0){
		return "OK";
	}
	if(strlen($prm) != 8){
		return "NG";
	}
	$y = (int)substr($prm, 0, 4);
	$m = (int)substr($prm, 4, 2);
	$d = (int)substr($prm, 6, 2);
	if (!checkdate($m, $d, $y)) {
		return "NG";
	}
	return "OK";
}

function chk_csvPrev($prm, $stts){

	if($prm == ""){
		return "";
	}
	for($i=0;$i<count($stts);$i++){
		if($prm == $stts[$i]){
			return $i;
		}
	}
	return "NG";
}


//更新処理■■■■■■■■■■■■■■
$res = $mdb2->startTransaction();							//トランザクション開始
if($res == "err_exit") err_exit();

for($i=0;$i<count($outArray);$i++){
	
	//jinji_work_history
	$res = $mdb2->upWorkHistory($jinjiArray[$i]["jinjiID"], $outArray[$i][2], $outArray[$i][3], $outArray[$i][4], $outArray[$i][5]);
	if($res == "err_exit") err_exit();
	
	//jinji_educational_bg
	if($outArray[$i][6] != "" || $outArray[$i][7] != ""){
		$res = $mdb2->upEducation($jinjiArray[$i]["jinjiID"], $outArray[$i][6], $outArray[$i][7]);
		if($res == "err_exit") err_exit();
	}
		
	//jinji_q_pro
	if($outArray[$i][8] != "" || $outArray[$i][9] != "" ||$outArray[$i][10] != ""){
		$res = $mdb2->upQualifiPro($jinjiArray[$i]["jinjiID"], $outArray[$i][8], $outArray[$i][9], $outArray[$i][10]);
		if($res == "err_exit") err_exit();
	}
	
	//jinji_license
	//DELETE
	$res = $mdb2->delJinjiData($jinjiID, "jinji_license");	
	if($res == "err_exit") err_exit();
	//INSERT
	$res = $mdb2->upLicense($jinjiArray[$i]["jinjiID"], "15", $outArray[$i][11], $outArray[$i][12], "");
	if($res == "err_exit") err_exit();
	$res = $mdb2->upLicense($jinjiArray[$i]["jinjiID"], "16", $outArray[$i][13], $outArray[$i][14], "");
	if($res == "err_exit") err_exit();
	$res = $mdb2->upLicense($jinjiArray[$i]["jinjiID"], "17", $outArray[$i][15], $outArray[$i][16], "");
	if($res == "err_exit") err_exit();
	$res = $mdb2->upLicense($jinjiArray[$i]["jinjiID"], "18", $outArray[$i][17], $outArray[$i][19], $outArray[$i][18]);
	if($res == "err_exit") err_exit();
	
	//jinji_relation
	$res = $mdb2->upJinjiRelation($jinjiArray[$i]["jinjiID"], $outArray[$i][20]);
	if($res == "err_exit") err_exit();
	
}

$res = $mdb2->commitTransaction();							//コミット
if($res == "err_exit") err_exit();

//====================================
//処理終了ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.CSVインポート 登録：処理終了");
echo("<script language='javascript'>document.frmJinji.result.value='t';</script>");
echo("<script language='javascript'>document.frmJinji.submit();</script>");
exit;
?>