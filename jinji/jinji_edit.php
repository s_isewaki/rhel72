<?php
//************************************
//人事管理機能 更新画面
//************************************

//====================================
//自ファイル名設定
//====================================
$fname = basename(__FILE__);

//====================================
//共通初期設定
//====================================
require_once("./jinji/jinji_common_init.php");

//====================================
//Smarty設定
//====================================
require_once("./jinji/conf/smarty.conf");

//====================================
//人事管理　更新機能専用モデル
//====================================
require_once("./jinji/model/jinji_edit_model.php");
$mdb2 = new JinjiEditModel($log, CMX_DB_DSN, $jinji_UserEmpID, $sts);
if($sts == "err_exit") err_exit();

//====================================
//処理開始ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面：処理開始");

//====================================
//フォームデータ取得
//====================================
$seEmpID   = $_REQUEST["seEmpID"];
$seEmpName = $_REQUEST["seEmpName"];
$seView    = $_REQUEST["seView"];
$seClass   = $_REQUEST["seClass"];
$seAtrb    = $_REQUEST["seAtrb"];
$seDept    = $_REQUEST["seDept"];
$seRoom    = $_REQUEST["seRoom"];
$seJob     = $_REQUEST["seJob"];
$seKey1    = $_REQUEST["seKey1"];
$seKey2    = $_REQUEST["seKey2"];
$seKey3    = $_REQUEST["seKey3"];
$seKey4    = $_REQUEST["seKey4"];
$seKey5    = $_REQUEST["seKey5"];
$seSort    = $_REQUEST["seSort"];
$sePage    = $_REQUEST["sePage"];
$seSelID   = $_REQUEST["seSelID"];

$errBack   = $_REQUEST["errBack"];	//入力エラー

//人事ID取得(作成)
//操作者
$res = $mdbC->startTransaction();					//トランザクション開始
if($res == "err_exit") err_exit();
$user_jinjiID = $mdbC->getJinjiID($jinji_UserEmpID);	//人事ID取得(作成)
if($user_jinjiID == "err_exit") err_exit();
$res = $mdbC->commitTransaction();				//コミット
if($res == "err_exit") err_exit();
//選択対象
$res = $mdbC->startTransaction();				//トランザクション開始
if($res == "err_exit") err_exit();
$jinjiID = $mdbC->getJinjiID($seSelID);			//人事ID取得(作成)
if($jinjiID == "err_exit") err_exit();
$res = $mdbC->commitTransaction();				//コミット
if($res == "err_exit") err_exit();

//写真
if(file_exists("profile/".$seSelID.".jpg")){
	$photoPath = "./profile/".$seSelID.".jpg";
}else{
	$photoPath = "./img/noprofile.jpg";
}
//年リスト
$arrY = "";
for($i=0;$i<100;$i++){
	$arrY[$i] = date("Y") - $i;
}
//月リスト
$arrM = "";
for($i=0;$i<12;$i++){
	$arrM[$i] = $i + 1;
}
//日リスト
$arrD = "";
for($i=0;$i<31;$i++){
	$arrD[$i] = $i + 1;
}
//職種リスト
$jobList = $mdb2->getJobList($user_jinjiID);
if($jobList == "err_exit") err_exit();

//■表示項目■■■■■■■■■■■■■■■■■■■
$res = $mdb2->getDispItemInfo();
if($res == "err_exit") err_exit();
$dispInfo = "";
for($i=0;$i<count($res);$i++){
	$dispInfo[$res[$i]["item_id"]] = $res[$i];
}

//■更新権限■■■■■■■■■■■■■■■■■■■
$res = $mdb2->getAuthEdit($user_jinjiID);
if($res == "err_exit") err_exit();
$authEdit = "";
for($i=0;$i<count($res);$i++){
	$authEdit[$res[$i]["item_ctg"]] = $res[$i];
}

//■基本属性■■■■■■■■■■■■■■■■■■■
$baseInfo = $mdb2->getBaseInfo($seSelID);
if($baseInfo == "err_exit") err_exit();

$emp_personal_id = $baseInfo[0]["emp_personal_id"];		//ID
$now_age         = $baseInfo[0]["now_age"];

//在職年数取得
$zaishoku_y = "-";
//採用年月日表示なし　⇒　×
//採用年月日表示あり
//　採用年月日入力あり
//　　退職年月日表示なし　⇒　採用⇔今日
//　　退職年月日表示あり
//　　　退職年月日入力あり　⇒　採用⇔退職
//　　　退職年月日入力なし　⇒　採用⇔今日
//　採用年月日入力なし　⇒　×
if((int)$dispInfo[1]["item_flg"] > 0){
	if($baseInfo[0]["emp_join"] != ""){
		if((int)$dispInfo[2]["item_flg"] > 0){
			if($baseInfo[0]["emp_retire"] != ""){
				//採用⇔退職
				$zaishoku_y = calc_year($baseInfo[0]["emp_join"], $baseInfo[0]["emp_retire"]);
			}else{	
				//採用⇔今日
				$zaishoku_y = calc_year($baseInfo[0]["emp_join"], date("Ymd"));
			}
		}else{
			//採用⇔今日
			$zaishoku_y = calc_year($baseInfo[0]["emp_join"], date("Ymd"));
		}
	}	
}
function calc_year($start, $end){
	list($sy, $sm, $sd) = sscanf($start, "%4s%2s%2s");
	list($ey, $em, $ed) = sscanf($end, "%4s%2s%2s");
	$y = $ey - $sy;
	if($em * 100 + $ed < $sm * 100 + $sd) $y--;
	return $y;
}

//■職歴■■■■■■■■■■■■■■■■■■■
$career = $mdb2->getCareer($jinjiID);
if($career == "err_exit") err_exit();

//■氏名履歴■■■■■■■■■■■■■■■■■■■
$nameInfo = $mdb2->getNameHistory($seSelID);
if($nameInfo == "err_exit") err_exit();
if(count($nameInfo) > 0){
	$old_lt_nm    = $nameInfo[0]["lt_nm"];
	$old_kn_lt_nm = $nameInfo[0]["kn_lt_nm"];
}else{
	$old_lt_nm    = "";
	$old_kn_lt_nm = "";

}

//■部署履歴■■■■■■■■■■■■■■■■■■■
$bushoInfo = $mdb2->getBoshuInfo($seSelID);
if($bushoInfo == "err_exit") err_exit();

//■勤務経緯■■■■■■■■■■■■■■■■■■■
$kinmu = "";
$kinmu = $mdb2->getWork($jinjiID);
if($kinmu == "err_exit") err_exit();

//■産休期間■■■■■■■■■■■■■■■■■■■
$sankyu = $mdb2->getKyuka($jinjiID, "jinji_maternity_leave");
if($sankyu == "err_exit") err_exit();

//■育休期間■■■■■■■■■■■■■■■■■■■
$ikukyu = $mdb2->getKyuka($jinjiID, "jinji_parental_leave");
if($ikukyu == "err_exit") err_exit();

//■育休期間■■■■■■■■■■■■■■■■■■■
$ikukyu = $mdb2->getKyuka($jinjiID, "jinji_parental_leave");
if($ikukyu == "err_exit") err_exit();

//■専門学歴■■■■■■■■■■■■■■■■■■■
$gakureki = $mdb2->getGakureki($jinjiID);
if($gakureki == "err_exit") err_exit();

//■専門・認定看護師資格■■■■■■■■■■■■■■■■■■■
$shikakuPro = $mdb2->getShikakuPro($jinjiID);
if($shikakuPro == "err_exit") err_exit();

//■専門・認定看護師資格■■■■■■■■■■■■■■■■■■■
$shikakuEtc = $mdb2->getShikakuEtc($jinjiID);
if($shikakuEtc == "err_exit") err_exit();

//■免許番号■■■■■■■■■■■■■■■■■■■
$menkyo15 = $mdb2->getMenkyoNo($jinjiID,"15");
if($menkyo15 == "err_exit") err_exit();
$menkyo16 = $mdb2->getMenkyoNo($jinjiID,"16");
if($menkyo16 == "err_exit") err_exit();
$menkyo17 = $mdb2->getMenkyoNo($jinjiID,"17");
if($menkyo17 == "err_exit") err_exit();
$menkyo18 = $mdb2->getMenkyoNo($jinjiID,"18");
if($menkyo18 == "err_exit") err_exit();

//■看護師協会■■■■■■■■■■■■■■■■■■■
$kyoukai = $mdb2->getKyoukai($jinjiID, "jinji_association");
if($kyoukai == "err_exit") err_exit();

//■その他協会■■■■■■■■■■■■■■■■■■■
$kyoukaiEtc = $mdb2->getKyoukai($jinjiID, "jinji_association_etc");
if($kyoukaiEtc == "err_exit") err_exit();

//■院外委員会■■■■■■■■■■■■■■■■■■■
$iinkai = $mdb2->getIinkai($jinjiID);
if($iinkai == "err_exit") err_exit();

//■院外発表■■■■■■■■■■■■■■■■■■■
$happyou = $mdb2->getHappyou($jinjiID);
if($happyou == "err_exit") err_exit();

//■院外研修■■■■■■■■■■■■■■■■■■■
$kenshu = $mdb2->getKenshu($jinjiID);
if($kenshu == "err_exit") err_exit();


if($errBack == "1"){
	//入力エラーで戻ってきた場合、入力値
	$print_all    = $_REQUEST["print_all"];
	$emp_lt_nm    = $_REQUEST["emp_lt_nm"];
	$emp_ft_nm    = $_REQUEST["emp_ft_nm"];
	$emp_kn_lt_nm = $_REQUEST["emp_kn_lt_nm"];
	$emp_kn_ft_nm = $_REQUEST["emp_kn_ft_nm"];
	$birth_y      = $_REQUEST["birth_y"];
	$birth_m      = $_REQUEST["birth_m"];
	$birth_d      = $_REQUEST["birth_d"];
	$emp_sex      = $_REQUEST["emp_sex"];
	$marriage_sts = $_REQUEST["marriage_sts"];
	$job_id       = $_REQUEST["job_id"];
	$emp_zip1     = $_REQUEST["emp_zip1"];
	$emp_zip2     = $_REQUEST["emp_zip2"];
	$emp_prv      = $_REQUEST["emp_prv"];
	$emp_addr1    = $_REQUEST["emp_addr1"];
	$emp_addr2    = $_REQUEST["emp_addr2"];
	$emp_tel1     = $_REQUEST["emp_tel1"];
	$emp_tel2     = $_REQUEST["emp_tel2"];
	$emp_tel3     = $_REQUEST["emp_tel3"];
	$emp_email2   = $_REQUEST["emp_email2"];
	$join_y       = $_REQUEST["join_y"];
	$join_m       = $_REQUEST["join_m"];
	$join_d       = $_REQUEST["join_d"];
	$retire_y     = $_REQUEST["retire_y"];
	$retire_m     = $_REQUEST["retire_m"];
	$retire_d     = $_REQUEST["retire_d"];
	$retire_kubun = $_REQUEST["retire_kubun"];
	$retire_riyu  = $_REQUEST["retire_riyu"];
	$koyou_keika  = $_REQUEST["koyou_keika"];
	$shoukaisaki  = $_REQUEST["shoukaisaki"];
	$join_kubun   = $_REQUEST["join_kubun"];
	$wk_hiho_no   = $_REQUEST["wk_hiho_no"];
	$wk_kiso_no   = $_REQUEST["wk_kiso_no"];
	$wk_kousei_no = $_REQUEST["wk_kousei_no"];
	$wk_koyou_no  = $_REQUEST["wk_koyou_no"];

	$working_hours   = $_REQUEST["working_hours"];
	$working_comment = $_REQUEST["working_comment"];

	$crH     = $_REQUEST["crH"];
	$crD     = $_REQUEST["crD"];
	$crP     = $_REQUEST["crP"];
	$crST_Y  = $_REQUEST["crST_Y"];
	$crST_M  = $_REQUEST["crST_M"];
	$crST_D  = $_REQUEST["crST_D"];
	$crED_Y  = $_REQUEST["crED_Y"];
	$crED_M  = $_REQUEST["crED_M"];
	$crED_D  = $_REQUEST["crED_D"];

	$license15No  = $_REQUEST["license15No"];
	$license15_Y  = $_REQUEST["license15_Y"];
	$license15_M  = $_REQUEST["license15_M"];
	$license15_D  = $_REQUEST["license15_D"];
	$license16No  = $_REQUEST["license16No"];
	$license16_Y  = $_REQUEST["license16_Y"];
	$license16_M  = $_REQUEST["license16_M"];
	$license16_D  = $_REQUEST["license16_D"];
	$license17No  = $_REQUEST["license17No"];
	$license17_Y  = $_REQUEST["license17_Y"];
	$license17_M  = $_REQUEST["license17_M"];
	$license17_D  = $_REQUEST["license17_D"];
	$license18No  = $_REQUEST["license18No"];
	$license18_Y  = $_REQUEST["license18_Y"];
	$license18_M  = $_REQUEST["license18_M"];
	$license18_D  = $_REQUEST["license18_D"];
	$license18Prv = $_REQUEST["license18Prv"];
		
	$sanST_Y      = $_REQUEST["sanST_Y"];
	$sanST_M      = $_REQUEST["sanST_M"];
	$sanST_D      = $_REQUEST["sanST_D"];
	$sanED_Y      = $_REQUEST["sanED_Y"];
	$sanED_M      = $_REQUEST["sanED_M"];
	$sanED_D      = $_REQUEST["sanED_D"];
	$ikuST_Y      = $_REQUEST["ikuST_Y"];
	$ikuST_M      = $_REQUEST["ikuST_M"];
	$ikuST_D      = $_REQUEST["ikuST_D"];
	$ikuED_Y      = $_REQUEST["ikuED_Y"];
	$ikuED_M      = $_REQUEST["ikuED_M"];
	$ikuED_D      = $_REQUEST["ikuED_D"];
	
	$gakuName      = $_REQUEST["gakuName"];
	$gakuED_Y      = $_REQUEST["gakuED_Y"];
	$gakuED_M      = $_REQUEST["gakuED_M"];
	$gakuED_D      = $_REQUEST["gakuED_D"];
	
	$proBunya   = $_REQUEST["proBunya"];
	$proKikan   = $_REQUEST["proKikan"];
	$proED_Y    = $_REQUEST["proED_Y"];
	$proED_M    = $_REQUEST["proED_M"];
	$proED_D    = $_REQUEST["proED_D"];
	$etcBunya   = $_REQUEST["etcBunya"];
	$etcKikan   = $_REQUEST["etcKikan"];
	$etcED_Y    = $_REQUEST["etcED_Y"];
	$etcED_M    = $_REQUEST["etcED_M"];
	$etcED_D    = $_REQUEST["etcED_D"];
	$assKanName = $_REQUEST["assKanName"];
	$assKanNo   = $_REQUEST["assKanNo"];
	$assKanED_Y = $_REQUEST["assKanED_Y"];
	$assKanED_M = $_REQUEST["assKanED_M"];
	$assKanED_D = $_REQUEST["assKanED_D"];
	$assEtcName = $_REQUEST["assEtcName"];
	$assEtcNo   = $_REQUEST["assEtcNo"];
	$assEtcED_Y = $_REQUEST["assEtcED_Y"];
	$assEtcED_M = $_REQUEST["assEtcED_M"];
	$assEtcED_D = $_REQUEST["assEtcED_D"];
	$cmmtName   = $_REQUEST["cmmtName"];
	$cmmtNT_Y   = $_REQUEST["cmmtNT_Y"];
	$cmmtNT_M   = $_REQUEST["cmmtNT_M"];
	$cmmtNT_D   = $_REQUEST["cmmtNT_D"];
	$cmmtED_Y   = $_REQUEST["cmmtED_Y"];
	$cmmtED_M   = $_REQUEST["cmmtED_M"];
	$cmmtED_D   = $_REQUEST["cmmtED_D"];
	$preTheme   = $_REQUEST["preTheme"];
	$preAgency  = $_REQUEST["preAgency"];
	$preED_Y    = $_REQUEST["preED_Y"];
	$preED_M    = $_REQUEST["preED_M"];
	$preED_D    = $_REQUEST["preED_D"];
	$trnTheme   = $_REQUEST["trnTheme"];
	$trnAgency  = $_REQUEST["trnAgency"];
	$trnArea    = $_REQUEST["trnArea"];
	$trnED_Y    = $_REQUEST["trnED_Y"];
	$trnED_M    = $_REQUEST["trnED_M"];
	$trnED_D    = $_REQUEST["trnED_D"];
	$trnHiyou   = $_REQUEST["trnHiyou"];
	
}else{
	//一覧からの場合、DB値
	$print_all    = $baseInfo[0]["print_all"];	//印刷フラグ
	$marriage_sts = $baseInfo[0]["marriage_sts"];
	$emp_lt_nm    = $baseInfo[0]["emp_lt_nm"];
	$emp_ft_nm    = $baseInfo[0]["emp_ft_nm"];
	$emp_kn_lt_nm = $baseInfo[0]["emp_kn_lt_nm"];
	$emp_kn_ft_nm = $baseInfo[0]["emp_kn_ft_nm"];
	if($baseInfo[0]["emp_birth"] == ""){
		$birth_y      = "";
		$birth_m      = "";
		$birth_d      = "";
	}else{
		$birth_y      = $baseInfo[0]["birth_y"];
		$birth_m      = $baseInfo[0]["birth_m"];
		$birth_d      = $baseInfo[0]["birth_d"];
	}
	$emp_sex      = $baseInfo[0]["emp_sex"];
	$job_id       = $baseInfo[0]["job_id"];
	$emp_zip1     = $baseInfo[0]["emp_zip1"];
	$emp_zip2     = $baseInfo[0]["emp_zip2"];
	$emp_prv      = $baseInfo[0]["emp_prv"];
	$emp_addr1    = $baseInfo[0]["emp_addr1"];
	$emp_addr2    = $baseInfo[0]["emp_addr2"];
	$emp_tel1     = $baseInfo[0]["emp_tel1"];
	$emp_tel2     = $baseInfo[0]["emp_tel2"];
	$emp_tel3     = $baseInfo[0]["emp_tel3"];
	$emp_email2   = $baseInfo[0]["emp_email2"];

	$join_kubun   = $baseInfo[0]["duty_form"];
	
	$working_hours   = $baseInfo[0]["working_time"];
	$working_comment = $baseInfo[0]["working_comm"];

	$crH     = "";
	$crD     = "";
	$crP     = "";
	$crST_Y  = "";
	$crST_M  = "";
	$crST_D  = "";
	$crED_Y  = "";
	$crED_M  = "";
	$crED_D  = "";
	for($i=0;$i<count($career);$i++){
		$crH[$i] = $career[$i]["career_hospital"];
		$crD[$i] = $career[$i]["career_department"];
		$crP[$i] = $career[$i]["career_post"];
		if($career[$i]["career_st_date"] == ""){
			$crST_Y[$i] = "";
			$crST_M[$i] = "";
			$crST_D[$i] = "";
		}else{
			$crST_Y[$i] = $career[$i]["st_y"];
			$crST_M[$i] = $career[$i]["st_m"];
			$crST_D[$i] = $career[$i]["st_d"];
		}
		if($career[$i]["career_ed_date"] == ""){
			$crED_Y[$i] = "";
			$crED_M[$i] = "";
			$crED_D[$i] = "";
		}else{
			$crED_Y[$i] = $career[$i]["ed_y"];
			$crED_M[$i] = $career[$i]["ed_m"];
			$crED_D[$i] = $career[$i]["ed_d"];
		}
	}	
	
	if($baseInfo[0]["emp_join"] == ""){
		$join_y      = "";
		$join_m      = "";
		$join_d      = "";
	}else{
		$join_y      = $baseInfo[0]["join_y"];
		$join_m      = $baseInfo[0]["join_m"];
		$join_d      = $baseInfo[0]["join_d"];
	}
	if($baseInfo[0]["emp_retire"] == ""){
		$retire_y     = "";
		$retire_m     = "";
		$retire_d     = "";
	}else{
		$retire_y     = $baseInfo[0]["retire_y"];
		$retire_m     = $baseInfo[0]["retire_m"];
		$retire_d     = $baseInfo[0]["retire_d"];
	}
	$retire_kubun = $kinmu[0]["retire_division"];
	$retire_riyu  = $kinmu[0]["retire_reason"];
	$koyou_keika  = $kinmu[0]["emp_course"];
	$shoukaisaki  = $kinmu[0]["referral"];
	$wk_hiho_no   = $kinmu[0]["kenko_number"];
	$wk_kiso_no   = $kinmu[0]["kiso_number"];
	$wk_kousei_no = $kinmu[0]["kosei_number"];
	$wk_koyou_no  = $kinmu[0]["koyo_number"];

	$sanST_Y = "";
	$sanST_M = "";
	$sanST_D = "";
	$sanED_Y = "";
	$sanED_M = "";
	$sanED_D = "";
	for($i=0;$i<count($sankyu);$i++){
		if($sankyu[$i]["start_date"] == ""){
			$sanST_Y[$i] = "";
			$sanST_M[$i] = "";
			$sanST_D[$i] = "";
		}else{
			$sanST_Y[$i] = $sankyu[$i]["st_y"];
			$sanST_M[$i] = $sankyu[$i]["st_m"];
			$sanST_D[$i] = $sankyu[$i]["st_d"];
		}
		if($sankyu[$i]["end_date"] == ""){
			$sanED_Y[$i] = "";
			$sanED_M[$i] = "";
			$sanED_D[$i] = "";
		}else{
			$sanED_Y[$i] = $sankyu[$i]["ed_y"];
			$sanED_M[$i] = $sankyu[$i]["ed_m"];
			$sanED_D[$i] = $sankyu[$i]["ed_d"];
		}
		
	}

	$ikuST_Y = "";
	$ikuST_M = "";
	$ikuST_D = "";
	$ikuED_Y = "";
	$ikuED_M = "";
	$ikuED_D = "";
	for($i=0;$i<count($ikukyu);$i++){
		if($ikukyu[$i]["start_date"] == ""){
			$ikuST_Y[$i] = "";
			$ikuST_M[$i] = "";
			$ikuST_D[$i] = "";
		}else{
			$ikuST_Y[$i] = $ikukyu[$i]["st_y"];
			$ikuST_M[$i] = $ikukyu[$i]["st_m"];
			$ikuST_D[$i] = $ikukyu[$i]["st_d"];
		}
		if($sankyu[$i]["end_date"] == ""){
			$ikuED_Y[$i] = "";
			$ikuED_M[$i] = "";
			$ikuED_D[$i] = "";
		}else{
			$ikuED_Y[$i] = $ikukyu[$i]["ed_y"];
			$ikuED_M[$i] = $ikukyu[$i]["ed_m"];
			$ikuED_D[$i] = $ikukyu[$i]["ed_d"];
		}
		
	}

	$gakuName = "";
	$gakuED_Y = "";
	$gakuED_M = "";
	$gakuED_D = "";
	for($i=0;$i<count($gakureki);$i++){
		$gakuName[$i] = $gakureki[$i]["school_name"];
		if($gakureki[$i]["graduation_date"] == ""){
			$gakuED_Y[$i] = "";
			$gakuED_M[$i] = "";
			$gakuED_D[$i] = "";
		}else{
			$gakuED_Y[$i] = $gakureki[$i]["ed_y"];
			$gakuED_M[$i] = $gakureki[$i]["ed_m"];
			$gakuED_D[$i] = $gakureki[$i]["ed_d"];
		}
	}

	$proBunya   = "";
	$proKikan   = "";
	$proED_Y    = "";
	$proED_M    = "";
	$proED_D    = "";
	for($i=0;$i<count($shikakuPro);$i++){
		$proBunya[$i] = $shikakuPro[$i]["q_pro_field"];
		$proKikan[$i] = $shikakuPro[$i]["q_pro_agency"];
		if($shikakuPro[$i]["q_pro_date"] == ""){
			$proED_Y[$i] = "";
			$proED_M[$i] = "";
			$proED_D[$i] = "";
		}else{
			$proED_Y[$i] = $shikakuPro[$i]["ed_y"];
			$proED_M[$i] = $shikakuPro[$i]["ed_m"];
			$proED_D[$i] = $shikakuPro[$i]["ed_d"];
		}
	}
	
	$etcBunya   = "";
	$etcKikan   = "";
	$etcED_Y    = "";
	$etcED_M    = "";
	$etcED_D    = "";
	for($i=0;$i<count($shikakuEtc);$i++){
		$etcBunya[$i] = $shikakuEtc[$i]["q_etc_field"];
		$etcKikan[$i] = $shikakuEtc[$i]["q_etc_agency"];
		if($shikakuEtc[$i]["q_etc_date"] == ""){
			$etcED_Y[$i] = "";
			$etcED_M[$i] = "";
			$etcED_D[$i] = "";
		}else{
			$etcED_Y[$i] = $shikakuEtc[$i]["ed_y"];
			$etcED_M[$i] = $shikakuEtc[$i]["ed_m"];
			$etcED_D[$i] = $shikakuEtc[$i]["ed_d"];
		}
	}

	if(count($menkyo15) > 0){
		$license15No  = $menkyo15[0]["license_no"];
		if($menkyo15[0]["license_date"] == ""){
			$license15_Y = "";
			$license15_M = "";
			$license15_D = "";
		}else{
			$license15_Y = $menkyo15[0]["ed_y"];
			$license15_M = $menkyo15[0]["ed_m"];
			$license15_D = $menkyo15[0]["ed_d"];
		}
	}else{
		$license15No  = "";
		$license15_Y  = "";
		$license15_M  = "";
		$license15_D  = "";
	}
	if(count($menkyo16) > 0){
		$license16No  = $menkyo16[0]["license_no"];
		if($menkyo16[0]["license_date"] == ""){
			$license16_Y = "";
			$license16_M = "";
			$license16_D = "";
		}else{
			$license16_Y = $menkyo16[0]["ed_y"];
			$license16_M = $menkyo16[0]["ed_m"];
			$license16_D = $menkyo16[0]["ed_d"];
		}
	}else{
		$license16No  = "";
		$license16_Y  = "";
		$license16_M  = "";
		$license16_D  = "";
	}
	if(count($menkyo17) > 0){
		$license17No  = $menkyo17[0]["license_no"];
		if($menkyo17[0]["license_date"] == ""){
			$license17_Y = "";
			$license17_M = "";
			$license17_D = "";
		}else{
			$license17_Y = $menkyo17[0]["ed_y"];
			$license17_M = $menkyo17[0]["ed_m"];
			$license17_D = $menkyo17[0]["ed_d"];
		}
	}else{
		$license17No  = "";
		$license17_Y  = "";
		$license17_M  = "";
		$license17_D  = "";
	}
	if(count($menkyo18) > 0){
		$license18No  = $menkyo18[0]["license_no"];
		$license18Prv = $menkyo18[0]["license_prv"];
		if($menkyo18[0]["license_date"] == ""){
			$license18_Y = "";
			$license18_M = "";
			$license18_D = "";
		}else{
			$license18_Y = $menkyo18[0]["ed_y"];
			$license18_M = $menkyo18[0]["ed_m"];
			$license18_D = $menkyo18[0]["ed_d"];
		}
	}else{
		$license18No  = "";
		$license18_Y  = "";
		$license18_M  = "";
		$license18_D  = "";
	}

	$assKanName = "";
	$assKanNo   = "";
	$assKanED_Y = "";
	$assKanED_M = "";
	$assKanED_D = "";
	for($i=0;$i<count($kyoukai);$i++){
		$assKanName[$i] = $kyoukai[$i]["association_name"];
		$assKanNo[$i] = $kyoukai[$i]["membership_no"];
		if($kyoukai[$i]["association_date"] == ""){
			$assKanED_Y[$i] = "";
			$assKanED_M[$i] = "";
			$assKanED_D[$i] = "";
		}else{
			$assKanED_Y[$i] = $kyoukai[$i]["ed_y"];
			$assKanED_M[$i] = $kyoukai[$i]["ed_m"];
			$assKanED_D[$i] = $kyoukai[$i]["ed_d"];
		}
	}

	$assEtcName = "";
	$assEtcNo   = "";
	$assEtcED_Y = "";
	$assEtcED_M = "";
	$assEtcED_D = "";
	for($i=0;$i<count($kyoukaiEtc);$i++){
		$assEtcName[$i] = $kyoukaiEtc[$i]["association_name"];
		$assEtcNo[$i] = $kyoukaiEtc[$i]["membership_no"];
		if($kyoukaiEtc[$i]["association_date"] == ""){
			$assEtcED_Y[$i] = "";
			$assEtcED_M[$i] = "";
			$assEtcED_D[$i] = "";
		}else{
			$assEtcED_Y[$i] = $kyoukaiEtc[$i]["ed_y"];
			$assEtcED_M[$i] = $kyoukaiEtc[$i]["ed_m"];
			$assEtcED_D[$i] = $kyoukaiEtc[$i]["ed_d"];
		}
	}

	$cmmtName   = "";
	$cmmtNT_Y   = "";
	$cmmtNT_M   = "";
	$cmmtNT_D   = "";
	$cmmtED_Y   = "";
	$cmmtED_M   = "";
	$cmmtED_D   = "";
	for($i=0;$i<count($iinkai);$i++){
		$cmmtName[$i] = $iinkai[$i]["committee_name"];
		if($iinkai[$i]["certification_date"] == ""){
			$cmmtNT_Y[$i] = "";
			$cmmtNT_M[$i] = "";
			$cmmtNT_D[$i] = "";
		}else{
			$cmmtNT_Y[$i] = $iinkai[$i]["st_y"];
			$cmmtNT_M[$i] = $iinkai[$i]["st_m"];
			$cmmtNT_D[$i] = $iinkai[$i]["st_d"];
		}
		if($iinkai[$i]["end_date"] == ""){
			$cmmtED_Y[$i] = "";
			$cmmtED_M[$i] = "";
			$cmmtED_D[$i] = "";
		}else{
			$cmmtED_Y[$i] = $iinkai[$i]["ed_y"];
			$cmmtED_M[$i] = $iinkai[$i]["ed_m"];
			$cmmtED_D[$i] = $iinkai[$i]["ed_d"];
		}
	}

	$preTheme   = "";
	$preAgency  = "";
	$preED_Y    = "";
	$preED_M    = "";
	$preED_D    = "";
	for($i=0;$i<count($happyou);$i++){
		$preTheme[$i] = $happyou[$i]["prsnt_theme"];
		$preAgency[$i] = $happyou[$i]["prsnt_agency"];
		if($happyou[$i]["prsnt_date"] == ""){
			$preED_Y[$i] = "";
			$preED_M[$i] = "";
			$preED_D[$i] = "";
		}else{
			$preED_Y[$i] = $happyou[$i]["ed_y"];
			$preED_M[$i] = $happyou[$i]["ed_m"];
			$preED_D[$i] = $happyou[$i]["ed_d"];
		}
	}

	$trnTheme   = "";
	$trnAgency  = "";
	$trnArea    = "";
	$trnED_Y    = "";
	$trnED_M    = "";
	$trnED_D    = "";
	$trnHiyou   = "";
	for($i=0;$i<count($kenshu);$i++){
		$trnTheme[$i] = $kenshu[$i]["trng_theme"];
		$trnAgency[$i] = $kenshu[$i]["trng_sponsor"];
		$trnArea[$i] = $kenshu[$i]["trng_venue"];
		$trnHiyou[$i] = $kenshu[$i]["trng_cost"];
		if($kenshu[$i]["trng_date"] == ""){
			$trnED_Y[$i] = "";
			$trnED_M[$i] = "";
			$trnED_D[$i] = "";
		}else{
			$trnED_Y[$i] = $kenshu[$i]["ed_y"];
			$trnED_M[$i] = $kenshu[$i]["ed_m"];
			$trnED_D[$i] = $kenshu[$i]["ed_d"];
		}
	}

}

//====================================
//画面データ・出力
//====================================
$smarty->assign("jinji_admin_auth", $jinji_admin_auth);
$smarty->assign("session", $session);
$smarty->assign("seEmpID", $seEmpID);
$smarty->assign("seEmpName", $seEmpName);
$smarty->assign("seView", $seView);
$smarty->assign("seJob", $seJob);
$smarty->assign("seClass", $seClass);
$smarty->assign("seAtrb", $seAtrb);
$smarty->assign("seDept", $seDept);
$smarty->assign("seRoom", $seRoom);
$smarty->assign("seKey1", $seKey1);
$smarty->assign("seKey2", $seKey2);
$smarty->assign("seKey3", $seKey3);
$smarty->assign("seKey4", $seKey4);
$smarty->assign("seKey5", $seKey5);
$smarty->assign("sePage", $sePage);
$smarty->assign("arrSort", $arrSort);
$smarty->assign("seSelID", $seSelID);
$smarty->assign("jinjiID", $jinjiID);

$smarty->assign("dispInfo", $dispInfo);
$smarty->assign("authEdit", $authEdit);

$smarty->assign("old_lt_nm", $old_lt_nm);
$smarty->assign("old_kn_lt_nm", $old_kn_lt_nm);

$smarty->assign("emp_personal_id", $emp_personal_id);
$smarty->assign("print_all", $print_all);
$smarty->assign("emp_lt_nm", $emp_lt_nm);
$smarty->assign("emp_ft_nm", $emp_ft_nm);
$smarty->assign("emp_kn_lt_nm", $emp_kn_lt_nm);
$smarty->assign("emp_kn_ft_nm", $emp_kn_ft_nm);
$smarty->assign("birth_y", $birth_y);
$smarty->assign("birth_m", $birth_m);
$smarty->assign("birth_d", $birth_d);
$smarty->assign("now_age", $now_age);
$smarty->assign("emp_sex", $emp_sex);
$smarty->assign("marriage_sts", $marriage_sts);
$smarty->assign("job_id", $job_id);
$smarty->assign("emp_zip1", $emp_zip1);
$smarty->assign("emp_zip2", $emp_zip2);
$smarty->assign("emp_prv", $emp_prv);
$smarty->assign("emp_addr1", $emp_addr1);
$smarty->assign("emp_addr2", $emp_addr2);
$smarty->assign("emp_tel1", $emp_tel1);
$smarty->assign("emp_tel2", $emp_tel2);
$smarty->assign("emp_tel3", $emp_tel3);
$smarty->assign("emp_email2", $emp_email2);
$smarty->assign("join_y", $join_y);
$smarty->assign("join_m", $join_m);
$smarty->assign("join_d", $join_d);
$smarty->assign("retire_y", $retire_y);
$smarty->assign("retire_m", $retire_m);
$smarty->assign("retire_d", $retire_d);
$smarty->assign("retire_kubun", $retire_kubun);
$smarty->assign("retire_riyu", $retire_riyu);
$smarty->assign("koyou_keika", $koyou_keika);
$smarty->assign("shoukaisaki", $shoukaisaki);
$smarty->assign("join_kubun", $join_kubun);
$smarty->assign("wk_hiho_no", $wk_hiho_no);
$smarty->assign("wk_kiso_no", $wk_kiso_no);
$smarty->assign("wk_kousei_no", $wk_kousei_no);
$smarty->assign("wk_koyou_no", $wk_koyou_no);

$smarty->assign("working_hours",   $working_hours);
$smarty->assign("working_comment", $working_comment);

$smarty->assign("crH",    $crH);
$smarty->assign("crD",    $crD);
$smarty->assign("crP",    $crP);
$smarty->assign("crST_Y", $crST_Y);
$smarty->assign("crST_M", $crST_M);
$smarty->assign("crST_D", $crST_D);
$smarty->assign("crED_Y", $crED_Y);
$smarty->assign("crED_M", $crED_M);
$smarty->assign("crED_D", $crED_D);

$smarty->assign("bushoInfo", $bushoInfo);

$smarty->assign("license15No", $license15No);
$smarty->assign("license15_Y", $license15_Y);
$smarty->assign("license15_M", $license15_M);
$smarty->assign("license15_D", $license15_D);
$smarty->assign("license16No", $license16No);
$smarty->assign("license16_Y", $license16_Y);
$smarty->assign("license16_M", $license16_M);
$smarty->assign("license16_D", $license16_D);
$smarty->assign("license17No", $license17No);
$smarty->assign("license17_Y", $license17_Y);
$smarty->assign("license17_M", $license17_M);
$smarty->assign("license17_D", $license17_D);
$smarty->assign("license18No", $license18No);
$smarty->assign("license18_Y", $license18_Y);
$smarty->assign("license18_M", $license18_M);
$smarty->assign("license18_D", $license18_D);
$smarty->assign("license18Prv", $license18Prv);

$smarty->assign("sanST_Y", $sanST_Y);
$smarty->assign("sanST_M", $sanST_M);
$smarty->assign("sanST_D", $sanST_D);
$smarty->assign("sanED_Y", $sanED_Y);
$smarty->assign("sanED_M", $sanED_M);
$smarty->assign("sanED_D", $sanED_D);
$smarty->assign("ikuST_Y", $ikuST_Y);
$smarty->assign("ikuST_M", $ikuST_M);
$smarty->assign("ikuST_D", $ikuST_D);
$smarty->assign("ikuED_Y", $ikuED_Y);
$smarty->assign("ikuED_M", $ikuED_M);
$smarty->assign("ikuED_D", $ikuED_D);

$smarty->assign("gakuName", $gakuName);
$smarty->assign("gakuED_Y", $gakuED_Y);
$smarty->assign("gakuED_M", $gakuED_M);
$smarty->assign("gakuED_D", $gakuED_D);

$smarty->assign("proBunya", $proBunya);
$smarty->assign("proKikan", $proKikan);
$smarty->assign("proED_Y", $proED_Y);
$smarty->assign("proED_M", $proED_M);
$smarty->assign("proED_D", $proED_D);
$smarty->assign("etcBunya", $etcBunya);
$smarty->assign("etcKikan", $etcKikan);
$smarty->assign("etcED_Y", $etcED_Y);
$smarty->assign("etcED_M", $etcED_M);
$smarty->assign("etcED_D", $etcED_D);
$smarty->assign("assKanName", $assKanName);
$smarty->assign("assKanNo", $assKanNo);
$smarty->assign("assKanED_Y", $assKanED_Y);
$smarty->assign("assKanED_M", $assKanED_M);
$smarty->assign("assKanED_D", $assKanED_D);
$smarty->assign("assEtcName", $assEtcName);
$smarty->assign("assEtcNo", $assEtcNo);
$smarty->assign("assEtcED_Y", $assEtcED_Y);
$smarty->assign("assEtcED_M", $assEtcED_M);
$smarty->assign("assEtcED_D", $assEtcED_D);
$smarty->assign("cmmtName", $cmmtName);
$smarty->assign("cmmtNT_Y", $cmmtNT_Y);
$smarty->assign("cmmtNT_M", $cmmtNT_M);
$smarty->assign("cmmtNT_D", $cmmtNT_D);
$smarty->assign("cmmtED_Y", $cmmtED_Y);
$smarty->assign("cmmtED_M", $cmmtED_M);
$smarty->assign("cmmtED_D", $cmmtED_D);
$smarty->assign("preTheme", $preTheme);
$smarty->assign("preAgency", $preAgency);
$smarty->assign("preED_Y", $preED_Y);
$smarty->assign("preED_M", $preED_M);
$smarty->assign("preED_D", $preED_D);
$smarty->assign("trnTheme", $trnTheme);
$smarty->assign("trnAgency", $trnAgency);
$smarty->assign("trnArea", $trnArea);
$smarty->assign("trnED_Y", $trnED_Y);
$smarty->assign("trnED_M", $trnED_M);
$smarty->assign("trnED_D", $trnED_D);
$smarty->assign("trnHiyou", $trnHiyou);

$smarty->assign("zaishoku_y", $zaishoku_y);



$smarty->assign("photoPath", $photoPath);
$smarty->assign("arrY", $arrY);
$smarty->assign("arrM", $arrM);
$smarty->assign("arrD", $arrD);
$smarty->assign("jobList", $jobList);
$smarty->assign("jinjiState", $jinjiState);

$smarty->display(basename(__FILE__, ".php").".tpl");

//====================================
//処理終了ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面：処理終了");

?>