<?php
//************************************
//人事管理機能 管理　権限
//************************************

//====================================
//自ファイル名設定
//====================================
$fname = basename(__FILE__);

//====================================
//共通初期設定
//====================================
require_once("./jinji/jinji_common_init.php");

//====================================
//Smarty設定
//====================================
require_once("./jinji/conf/smarty.conf");

//====================================
//人事管理　参照機能専用モデル
//====================================
require_once("./jinji/model/jinji_control_auth_model.php");
$mdb2 = new JinjiControlAuthModel($log, CMX_DB_DSN, $jinji_UserEmpID, $sts);
if($sts == "err_exit") err_exit();

//====================================
//処理開始ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.権限：処理開始");

//====================================
//フォームデータ取得
//====================================
$seSelID   = $_REQUEST["seSelID"];

//====================================
//表示データ取得
//====================================
$res = $mdbC->startTransaction();			//トランザクション開始
if($res == "err_exit") err_exit();

$jinjiID = $mdbC->getJinjiID($seSelID);		//人事ID取得(作成)
if($jinjiID == "err_exit") err_exit();

$res = $mdbC->commitTransaction();			//コミット
if($res == "err_exit") err_exit();

//ユーザ
$usrData = $mdb2->getUser($seSelID);
if($usrData == "err_exit") err_exit();

//職種
$jobList = $mdb2->getJobList($jinjiID);
if($jobList == "err_exit") err_exit();

//参照権限
$refList = $mdb2->getAuthList('jinji_auth_refer', $jinjiID);
if($refList == "err_exit") err_exit();

//更新権限
$edtList = $mdb2->getAuthList('jinji_auth_edit', $jinjiID);
if($edtList == "err_exit") err_exit();

//帳票出力権限
$prtList = $mdb2->getAuthPrint($jinjiID);
if($prtList == "err_exit") err_exit();

//====================================
//画面データ・出力
//====================================
$smarty->assign("session", $session);
$smarty->assign("seSelID", $seSelID);
$smarty->assign("usrData", $usrData);
$smarty->assign("jobList", $jobList);
$smarty->assign("refList", $refList);
$smarty->assign("edtList", $edtList);
$smarty->assign("prtList", $prtList);

$smarty->assign("jinjiID", $jinjiID);

$smarty->display(basename(__FILE__, ".php").".tpl");

//====================================
//処理終了ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.権限：処理終了");

?>