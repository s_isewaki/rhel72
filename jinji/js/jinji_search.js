//javascript関数

function initConditions(){
	document.frmJinji.seSort.value="";
	document.frmJinji.sePage.value="";
}

function setSort(flg){
	document.frmJinji.seSort.value=flg;

}

function setPage(page){
	document.frmJinji.sePage.value=page;

}

function chgClass(){
	clearOptions(document.frmJinji.seAtrb);
	addOption(document.frmJinji.seAtrb, '', 'すべて');
	var tempID = document.frmJinji.seClass.value;
	var arrData = document.frmJinji.atrbData.value.split("/");
	for(i=0;i<arrData.length-1;i++){
		tmpArr = arrData[i].split(",");
		if(tmpArr[0]==tempID){
			var objHid = document.getElementById("hidAtrbNM_"+tmpArr[1]);
			addOption(document.frmJinji.seAtrb, tmpArr[1], objHid.value);
		}
	}
	clearOptions(document.frmJinji.seDept);
	addOption(document.frmJinji.seDept, '', 'すべて');
	if(document.frmJinji.seRoom){
		clearOptions(document.frmJinji.seRoom);
		addOption(document.frmJinji.seRoom, '', 'すべて');
	}
}

function chgAtrb(){
	clearOptions(document.frmJinji.seDept);
	addOption(document.frmJinji.seDept, '', 'すべて');
	var tempID = document.frmJinji.seAtrb.value;
	var arrData = document.frmJinji.deptData.value.split("/");
	for(i=0;i<arrData.length-1;i++){
		tmpArr = arrData[i].split(",");
		if(tmpArr[0]==tempID){
			var objHid = document.getElementById("hidDeptNM_"+tmpArr[1]);
			addOption(document.frmJinji.seDept, tmpArr[1], objHid.value);
		}
	}
	if(document.frmJinji.seRoom){
		clearOptions(document.frmJinji.seRoom);
		addOption(document.frmJinji.seRoom, '', 'すべて');
	}
}

function chgDept(){
	clearOptions(document.frmJinji.seRoom);
	addOption(document.frmJinji.seRoom, '', 'すべて');
	var tempID = document.frmJinji.seDept.value;
	var arrData = document.frmJinji.roomData.value.split("/");
	for(i=0;i<arrData.length-1;i++){
		tmpArr = arrData[i].split(",");
		if(tmpArr[0]==tempID){
			var objHid = document.getElementById("hidRoomNM_"+tmpArr[1]);
			addOption(document.frmJinji.seRoom, tmpArr[1], objHid.value);
		}
	}
}

function clearOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function print_list(){

	document.frmJinji.mode.value="print_list";
	document.frmJinji.action = "./jinji_menu.php";
	document.frmJinji.method="POST";
	document.frmJinji.submit();

window.close();
}
function print1_pdf(empid){

	document.frmJinji.print1ID.value=empid;
	document.frmJinji.mode.value="print_1";
	document.frmJinji.action = "./jinji_menu.php";
	document.frmJinji.method="POST";
	document.frmJinji.submit();

window.close();
}