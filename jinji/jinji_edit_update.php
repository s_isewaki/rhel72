<?php
//************************************
//人事管理機能 更新画面.更新
//************************************

//====================================
//自ファイル名設定
//====================================
$fname = basename(__FILE__);

//====================================
//共通初期設定
//====================================
require_once("./jinji/jinji_common_init.php");

//====================================
//Smarty設定
//====================================
require_once("./jinji/conf/smarty.conf");

//====================================
//人事管理　更新機能専用モデル
//====================================
require_once("./jinji/model/jinji_edit_update_model.php");
$mdb2 = new JinjiEditUpdateModel($log, CMX_DB_DSN, $jinji_UserEmpID, $sts);
if($sts == "err_exit") err_exit();

//====================================
//処理開始ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理開始");

//====================================
//フォームデータ取得
//====================================
$session   = $_REQUEST["session"];
$seSelID   = $_REQUEST["seSelID"];
$jinjiID   = $_REQUEST["jinjiID"];
//入力値
$print_all    = $_REQUEST["print_all"];
$emp_lt_nm    = $_REQUEST["emp_lt_nm"];
$emp_ft_nm    = $_REQUEST["emp_ft_nm"];
$emp_kn_lt_nm = $_REQUEST["emp_kn_lt_nm"];
$emp_kn_ft_nm = $_REQUEST["emp_kn_ft_nm"];
$birth_y      = $_REQUEST["birth_y"];
$birth_m      = $_REQUEST["birth_m"];
$birth_d      = $_REQUEST["birth_d"];
$emp_sex      = $_REQUEST["emp_sex"];
$marriage_sts = $_REQUEST["marriage_sts"];
$job_id       = $_REQUEST["job_id"];
$emp_zip1     = $_REQUEST["emp_zip1"];
$emp_zip2     = $_REQUEST["emp_zip2"];
$emp_prv      = $_REQUEST["emp_prv"];
$emp_addr1    = $_REQUEST["emp_addr1"];
$emp_addr2    = $_REQUEST["emp_addr2"];
$emp_tel1     = $_REQUEST["emp_tel1"];
$emp_tel2     = $_REQUEST["emp_tel2"];
$emp_tel3     = $_REQUEST["emp_tel3"];
$emp_email2   = $_REQUEST["emp_email2"];

$working_hours   = $_REQUEST["working_hours"];
$working_comment = $_REQUEST["working_comment"];
$crH    = $_REQUEST["crH"];
$crD    = $_REQUEST["crD"];
$crP    = $_REQUEST["crP"];
$crST_Y = $_REQUEST["crST_Y"];
$crST_M = $_REQUEST["crST_M"];
$crST_D = $_REQUEST["crST_D"];
$crED_Y = $_REQUEST["crED_Y"];
$crED_M = $_REQUEST["crED_M"];
$crED_D = $_REQUEST["crED_D"];

$join_y       = $_REQUEST["join_y"];
$join_m       = $_REQUEST["join_m"];
$join_d       = $_REQUEST["join_d"];
$retire_y     = $_REQUEST["retire_y"];
$retire_m     = $_REQUEST["retire_m"];
$retire_d     = $_REQUEST["retire_d"];
$retire_kubun = $_REQUEST["retire_kubun"];
$retire_riyu  = $_REQUEST["retire_riyu"];
$koyou_keika  = $_REQUEST["koyou_keika"];
$shoukaisaki  = $_REQUEST["shoukaisaki"];
$join_kubun   = $_REQUEST["join_kubun"];
$wk_hiho_no   = $_REQUEST["wk_hiho_no"];
$wk_kiso_no   = $_REQUEST["wk_kiso_no"];
$wk_kousei_no = $_REQUEST["wk_kousei_no"];
$wk_koyou_no  = $_REQUEST["wk_koyou_no"];

$license15No  = $_REQUEST["license15No"];
$license15_Y  = $_REQUEST["license15_Y"];
$license15_M  = $_REQUEST["license15_M"];
$license15_D  = $_REQUEST["license15_D"];
$license16No  = $_REQUEST["license16No"];
$license16_Y  = $_REQUEST["license16_Y"];
$license16_M  = $_REQUEST["license16_M"];
$license16_D  = $_REQUEST["license16_D"];
$license17No  = $_REQUEST["license17No"];
$license17_Y  = $_REQUEST["license17_Y"];
$license17_M  = $_REQUEST["license17_M"];
$license17_D  = $_REQUEST["license17_D"];
$license18No  = $_REQUEST["license18No"];
$license18_Y  = $_REQUEST["license18_Y"];
$license18_M  = $_REQUEST["license18_M"];
$license18_D  = $_REQUEST["license18_D"];
$license18Prv = $_REQUEST["license18Prv"];

$sanST_Y    = $_REQUEST["sanST_Y"];
$sanST_M    = $_REQUEST["sanST_M"];
$sanST_D    = $_REQUEST["sanST_D"];
$sanED_Y    = $_REQUEST["sanED_Y"];
$sanED_M    = $_REQUEST["sanED_M"];
$sanED_D    = $_REQUEST["sanED_D"];
$ikuST_Y    = $_REQUEST["ikuST_Y"];
$ikuST_M    = $_REQUEST["ikuST_M"];
$ikuST_D    = $_REQUEST["ikuST_D"];
$ikuED_Y    = $_REQUEST["ikuED_Y"];
$ikuED_M    = $_REQUEST["ikuED_M"];
$ikuED_D    = $_REQUEST["ikuED_D"];
$gakuName   = $_REQUEST["gakuName"];
$gakuED_Y   = $_REQUEST["gakuED_Y"];
$gakuED_M   = $_REQUEST["gakuED_M"];
$gakuED_D   = $_REQUEST["gakuED_D"];
$proBunya   = $_REQUEST["proBunya"];
$proKikan   = $_REQUEST["proKikan"];
$proED_Y    = $_REQUEST["proED_Y"];
$proED_M    = $_REQUEST["proED_M"];
$proED_D    = $_REQUEST["proED_D"];
$etcBunya   = $_REQUEST["etcBunya"];
$etcKikan   = $_REQUEST["etcKikan"];
$etcED_Y    = $_REQUEST["etcED_Y"];
$etcED_M    = $_REQUEST["etcED_M"];
$etcED_D    = $_REQUEST["etcED_D"];
$assKanName = $_REQUEST["assKanName"];
$assKanNo   = $_REQUEST["assKanNo"];
$assKanED_Y = $_REQUEST["assKanED_Y"];
$assKanED_M = $_REQUEST["assKanED_M"];
$assKanED_D = $_REQUEST["assKanED_D"];
$assEtcName = $_REQUEST["assEtcName"];
$assEtcNo   = $_REQUEST["assEtcNo"];
$assEtcED_Y = $_REQUEST["assEtcED_Y"];
$assEtcED_M = $_REQUEST["assEtcED_M"];
$assEtcED_D = $_REQUEST["assEtcED_D"];
$cmmtName   = $_REQUEST["cmmtName"];
$cmmtNT_Y   = $_REQUEST["cmmtNT_Y"];
$cmmtNT_M   = $_REQUEST["cmmtNT_M"];
$cmmtNT_D   = $_REQUEST["cmmtNT_D"];
$cmmtED_Y   = $_REQUEST["cmmtED_Y"];
$cmmtED_M   = $_REQUEST["cmmtED_M"];
$cmmtED_D   = $_REQUEST["cmmtED_D"];
$preTheme   = $_REQUEST["preTheme"];
$preAgency  = $_REQUEST["preAgency"];
$preED_Y    = $_REQUEST["preED_Y"];
$preED_M    = $_REQUEST["preED_M"];
$preED_D    = $_REQUEST["preED_D"];
$trnTheme   = $_REQUEST["trnTheme"];
$trnAgency  = $_REQUEST["trnAgency"];
$trnArea    = $_REQUEST["trnArea"];
$trnED_Y    = $_REQUEST["trnED_Y"];
$trnED_M    = $_REQUEST["trnED_M"];
$trnED_D    = $_REQUEST["trnED_D"];
$trnHiyou   = $_REQUEST["trnHiyou"];

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
</head>
<body>
<form name="frmJinji" method="post" action="./jinji_menu.php">

<input type="hidden" name="errBack" value="1">
<input type="hidden" name="mode" value="edit">

<input type="hidden" name="session"      value="<?php echo $session; ?>">
<input type="hidden" name="seSelID"      value="<?php echo $seSelID; ?>">
<input type="hidden" name="jinjiID"      value="<?php echo $jinjiID; ?>">

<input type="hidden" name="seEmpID"      value="<?php echo $_REQUEST["seEmpID"]; ?>">
<input type="hidden" name="seEmpName"    value="<?php echo $_REQUEST["seEmpName"]; ?>">
<input type="hidden" name="seView"       value="<?php echo $_REQUEST["seView"]; ?>">
<input type="hidden" name="seClass"      value="<?php echo $_REQUEST["seClass"]; ?>">
<input type="hidden" name="seAtrb"       value="<?php echo $_REQUEST["seAtrb"]; ?>">
<input type="hidden" name="seDept"       value="<?php echo $_REQUEST["seDept"]; ?>">
<input type="hidden" name="seRoom"       value="<?php echo $_REQUEST["seRoom"]; ?>">
<input type="hidden" name="seJob"        value="<?php echo $_REQUEST["seJob"]; ?>">
<input type="hidden" name="seKey1"       value="<?php echo $_REQUEST["seKey1"]; ?>">
<input type="hidden" name="seKey2"       value="<?php echo $_REQUEST["seKey2"]; ?>">
<input type="hidden" name="seKey3"       value="<?php echo $_REQUEST["seKey3"]; ?>">
<input type="hidden" name="seKey4"       value="<?php echo $_REQUEST["seKey4"]; ?>">
<input type="hidden" name="seKey5"       value="<?php echo $_REQUEST["seKey5"]; ?>">
<input type="hidden" name="seSort"       value="<?php echo $_REQUEST["seSort"]; ?>">
<input type="hidden" name="sePage"       value="<?php echo $_REQUEST["sePage"]; ?>">

<input type="hidden" name="print_all"    value="<?php echo $print_all; ?>">
<input type="hidden" name="emp_lt_nm"    value="<?php echo $emp_lt_nm; ?>">
<input type="hidden" name="emp_ft_nm"    value="<?php echo $emp_ft_nm; ?>">
<input type="hidden" name="emp_kn_lt_nm" value="<?php echo $emp_kn_lt_nm; ?>">
<input type="hidden" name="emp_kn_ft_nm" value="<?php echo $emp_kn_ft_nm; ?>">
<input type="hidden" name="birth_y"      value="<?php echo $birth_y; ?>">
<input type="hidden" name="birth_m"      value="<?php echo $birth_m; ?>">
<input type="hidden" name="birth_d"      value="<?php echo $birth_d; ?>">
<input type="hidden" name="emp_sex"      value="<?php echo $emp_sex; ?>">
<input type="hidden" name="marriage_sts" value="<?php echo $marriage_sts; ?>">
<input type="hidden" name="job_id"       value="<?php echo $job_id; ?>">
<input type="hidden" name="emp_zip1"     value="<?php echo $emp_zip1; ?>">
<input type="hidden" name="emp_zip2"     value="<?php echo $emp_zip2; ?>">
<input type="hidden" name="emp_prv"      value="<?php echo $emp_prv; ?>">
<input type="hidden" name="emp_addr1"    value="<?php echo $emp_addr1; ?>">
<input type="hidden" name="emp_addr2"    value="<?php echo $emp_addr2; ?>">
<input type="hidden" name="emp_tel1"     value="<?php echo $emp_tel1; ?>">
<input type="hidden" name="emp_tel2"     value="<?php echo $emp_tel2; ?>">
<input type="hidden" name="emp_tel3"     value="<?php echo $emp_tel3; ?>">
<input type="hidden" name="emp_email2"   value="<?php echo $emp_email2; ?>">

<input type="hidden" name="working_hours"   value="<?php echo $working_hours; ?>">
<input type="hidden" name="working_comment" value="<?php echo $working_comment; ?>">

<input type="hidden" name="join_y"       value="<?php echo $join_y; ?>">
<input type="hidden" name="join_m"       value="<?php echo $join_m; ?>">
<input type="hidden" name="join_d"       value="<?php echo $join_d; ?>">
<input type="hidden" name="retire_y"     value="<?php echo $retire_y; ?>">
<input type="hidden" name="retire_m"     value="<?php echo $retire_m; ?>">
<input type="hidden" name="retire_d"     value="<?php echo $retire_d; ?>">
<input type="hidden" name="retire_kubun" value="<?php echo $retire_kubun; ?>">
<input type="hidden" name="retire_riyu"  value="<?php echo $retire_riyu; ?>">
<input type="hidden" name="koyou_keika"  value="<?php echo $koyou_keika; ?>">
<input type="hidden" name="shoukaisaki"  value="<?php echo $shoukaisaki; ?>">
<input type="hidden" name="join_kubun"   value="<?php echo $join_kubun; ?>">
<input type="hidden" name="wk_hiho_no"   value="<?php echo $wk_hiho_no; ?>">
<input type="hidden" name="wk_kiso_no"   value="<?php echo $wk_kiso_no; ?>">
<input type="hidden" name="wk_kousei_no" value="<?php echo $wk_kousei_no; ?>">
<input type="hidden" name="wk_koyou_no"  value="<?php echo $wk_koyou_no; ?>">

<input type="hidden" name="license15No"  value="<?php echo $license15No; ?>">
<input type="hidden" name="license15_Y"  value="<?php echo $license15_Y; ?>">
<input type="hidden" name="license15_M"  value="<?php echo $license15_M; ?>">
<input type="hidden" name="license15_D"  value="<?php echo $license15_D; ?>">
<input type="hidden" name="license16No"  value="<?php echo $license16No; ?>">
<input type="hidden" name="license16_Y"  value="<?php echo $license16_Y; ?>">
<input type="hidden" name="license16_M"  value="<?php echo $license16_M; ?>">
<input type="hidden" name="license16_D"  value="<?php echo $license16_D; ?>">
<input type="hidden" name="license17No"  value="<?php echo $license17No; ?>">
<input type="hidden" name="license17_Y"  value="<?php echo $license17_Y; ?>">
<input type="hidden" name="license17_M"  value="<?php echo $license17_M; ?>">
<input type="hidden" name="license17_D"  value="<?php echo $license17_D; ?>">
<input type="hidden" name="license18No"  value="<?php echo $license18No; ?>">
<input type="hidden" name="license18_Y"  value="<?php echo $license18_Y; ?>">
<input type="hidden" name="license18_M"  value="<?php echo $license18_M; ?>">
<input type="hidden" name="license18_D"  value="<?php echo $license18_D; ?>">
<input type="hidden" name="license18Prv"  value="<?php echo $license18Prv; ?>">
<?php
for($i=0;$i<count($crH);$i++){
	echo "<input type='hidden' name='crH[]'    value='".$crH[$i]."'>\n";
	echo "<input type='hidden' name='crD[]'    value='".$crD[$i]."'>\n";
	echo "<input type='hidden' name='crP[]'    value='".$crP[$i]."'>\n";
	echo "<input type='hidden' name='crST_Y[]' value='".$crST_Y[$i]."'>\n";
	echo "<input type='hidden' name='crST_M[]' value='".$crST_M[$i]."'>\n";
	echo "<input type='hidden' name='crST_D[]' value='".$crST_D[$i]."'>\n";
	echo "<input type='hidden' name='crED_Y[]' value='".$crED_Y[$i]."'>\n";
	echo "<input type='hidden' name='crED_M[]' value='".$crED_M[$i]."'>\n";
	echo "<input type='hidden' name='crED_D[]' value='".$crED_D[$i]."'>\n";
}
for($i=0;$i<count($sanST_Y);$i++){
	echo "<input type='hidden' name='sanST_Y[]'   value='".$sanST_Y[$i]."'>\n";
	echo "<input type='hidden' name='sanST_M[]'   value='".$sanST_M[$i]."'>\n";
	echo "<input type='hidden' name='sanST_D[]'   value='".$sanST_D[$i]."'>\n";
	echo "<input type='hidden' name='sanED_Y[]'   value='".$sanED_Y[$i]."'>\n";
	echo "<input type='hidden' name='sanED_M[]'   value='".$sanED_M[$i]."'>\n";
	echo "<input type='hidden' name='sanED_D[]'   value='".$sanED_D[$i]."'>\n";
}
for($i=0;$i<count($ikuST_Y);$i++){
	echo "<input type='hidden' name='ikuST_Y[]'   value='".$ikuST_Y[$i]."'>\n";
	echo "<input type='hidden' name='ikuST_M[]'   value='".$ikuST_M[$i]."'>\n";
	echo "<input type='hidden' name='ikuST_D[]'   value='".$ikuST_D[$i]."'>\n";
	echo "<input type='hidden' name='ikuED_Y[]'   value='".$ikuED_Y[$i]."'>\n";
	echo "<input type='hidden' name='ikuED_M[]'   value='".$ikuED_M[$i]."'>\n";
	echo "<input type='hidden' name='ikuED_D[]'   value='".$ikuED_D[$i]."'>\n";
}
for($i=0;$i<count($gakuName);$i++){
	echo "<input type='hidden' name='gakuName[]'   value='".$gakuName[$i]."'>\n";
	echo "<input type='hidden' name='gakuED_Y[]'   value='".$gakuED_Y[$i]."'>\n";
	echo "<input type='hidden' name='gakuED_M[]'   value='".$gakuED_M[$i]."'>\n";
	echo "<input type='hidden' name='gakuED_D[]'   value='".$gakuED_D[$i]."'>\n";
}
for($i=0;$i<count($proBunya);$i++){
	echo "<input type='hidden' name='proBunya[]'  value='".$proBunya[$i]."'>\n";
	echo "<input type='hidden' name='proKikan[]'  value='".$proKikan[$i]."'>\n";
	echo "<input type='hidden' name='proED_Y[]'   value='".$proED_Y[$i]."'>\n";
	echo "<input type='hidden' name='proED_M[]'   value='".$proED_M[$i]."'>\n";
	echo "<input type='hidden' name='proED_D[]'   value='".$proED_D[$i]."'>\n";
}
for($i=0;$i<count($etcBunya);$i++){
	echo "<input type='hidden' name='etcBunya[]'  value='".$etcBunya[$i]."'>\n";
	echo "<input type='hidden' name='etcKikan[]'  value='".$etcKikan[$i]."'>\n";
	echo "<input type='hidden' name='etcED_Y[]'   value='".$etcED_Y[$i]."'>\n";
	echo "<input type='hidden' name='etcED_M[]'   value='".$etcED_M[$i]."'>\n";
	echo "<input type='hidden' name='etcED_D[]'   value='".$etcED_D[$i]."'>\n";
}
for($i=0;$i<count($assKanName);$i++){
	echo "<input type='hidden' name='assKanName[]' value='".$assKanName[$i]."'>\n";
	echo "<input type='hidden' name='assKanNo[]'   value='".$assKanNo[$i]."'>\n";
	echo "<input type='hidden' name='assKanED_Y[]' value='".$assKanED_Y[$i]."'>\n";
	echo "<input type='hidden' name='assKanED_M[]' value='".$assKanED_M[$i]."'>\n";
	echo "<input type='hidden' name='assKanED_D[]' value='".$assKanED_D[$i]."'>\n";
}
for($i=0;$i<count($assEtcName);$i++){
	echo "<input type='hidden' name='assEtcName[]' value='".$assEtcName[$i]."'>\n";
	echo "<input type='hidden' name='assEtcNo[]'   value='".$assEtcNo[$i]."'>\n";
	echo "<input type='hidden' name='assEtcED_Y[]' value='".$assEtcED_Y[$i]."'>\n";
	echo "<input type='hidden' name='assEtcED_M[]' value='".$assEtcED_M[$i]."'>\n";
	echo "<input type='hidden' name='assEtcED_D[]' value='".$assEtcED_D[$i]."'>\n";
}
for($i=0;$i<count($cmmtName);$i++){
	echo "<input type='hidden' name='cmmtName[]' value='".$cmmtName[$i]."'>\n";
	echo "<input type='hidden' name='cmmtNT_Y[]' value='".$cmmtNT_Y[$i]."'>\n";
	echo "<input type='hidden' name='cmmtNT_M[]' value='".$cmmtNT_M[$i]."'>\n";
	echo "<input type='hidden' name='cmmtNT_D[]' value='".$cmmtNT_D[$i]."'>\n";
	echo "<input type='hidden' name='cmmtED_Y[]' value='".$cmmtED_Y[$i]."'>\n";
	echo "<input type='hidden' name='cmmtED_M[]' value='".$cmmtED_M[$i]."'>\n";
	echo "<input type='hidden' name='cmmtED_D[]' value='".$cmmtED_D[$i]."'>\n";
}
for($i=0;$i<count($preTheme);$i++){
	echo "<input type='hidden' name='preTheme[]'  value='".$preTheme[$i]."'>\n";
	echo "<input type='hidden' name='preAgency[]' value='".$preAgency[$i]."'>\n";
	echo "<input type='hidden' name='preED_Y[]'   value='".$preED_Y[$i]."'>\n";
	echo "<input type='hidden' name='preED_M[]'   value='".$preED_M[$i]."'>\n";
	echo "<input type='hidden' name='preED_D[]'   value='".$preED_D[$i]."'>\n";
}
for($i=0;$i<count($trnTheme);$i++){
	echo "<input type='hidden' name='trnTheme[]'  value='".$trnTheme[$i]."'>\n";
	echo "<input type='hidden' name='trnAgency[]' value='".$trnAgency[$i]."'>\n";
	echo "<input type='hidden' name='trnArea[]'   value='".$trnArea[$i]."'>\n";
	echo "<input type='hidden' name='trnED_Y[]'   value='".$trnED_Y[$i]."'>\n";
	echo "<input type='hidden' name='trnED_M[]'   value='".$trnED_M[$i]."'>\n";
	echo "<input type='hidden' name='trnED_D[]'   value='".$trnED_D[$i]."'>\n";
	echo "<input type='hidden' name='trnHiyou[]'  value='".$trnHiyou[$i]."'>\n";
}
?>
</form>
</body>
</html>
<?php

//人事ID取得(作成)
//操作者
$res = $mdbC->startTransaction();					//トランザクション開始
if($res == "err_exit") err_exit();
$user_jinjiID = $mdbC->getJinjiID($jinji_UserEmpID);	//人事ID取得(作成)
if($user_jinjiID == "err_exit") err_exit();
$res = $mdbC->commitTransaction();				//コミット

//■表示項目■■■■■■■■■■■■■■■■■■■
$res = $mdb2->getDispItemInfo();
if($res == "err_exit") err_exit();
$dispInfo = "";
for($i=0;$i<count($res);$i++){
	$dispInfo[$res[$i]["item_id"]] = $res[$i];
}
//■更新権限■■■■■■■■■■■■■■■■■■■
$res = $mdb2->getAuthEdit($user_jinjiID);
if($res == "err_exit") err_exit();
$authEdit = "";
for($i=0;$i<count($res);$i++){
	$authEdit[$res[$i]["item_ctg"]] = $res[$i];
}

//■■■■■■基本属性　入力チェック■■■■■■
//苗字（漢字）null check
if($emp_lt_nm ==""){
	echo("<script language='javascript'>alert('苗字（漢字）を入力してください。');</script>\n");
	echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
	exit;
}
//苗字（漢字）文字数チェック
if(strlen($emp_lt_nm) > 20){
	echo("<script language='javascript'>alert('苗字（漢字）が長すぎます。');</script>\n");
	echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
	exit;
}
//名前（漢字）null check
if($emp_ft_nm ==""){
	echo("<script language='javascript'>alert('名前（漢字）を入力してください。');</script>\n");
	echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
	exit;
}
//名前（漢字）文字数チェック
if(strlen($emp_kn_lt_nm) > 20){
	echo("<script language='javascript'>alert('名前（漢字）が長すぎます。');</script>\n");
	echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
	exit;
}
//苗字（かな）null check
if($emp_kn_lt_nm ==""){
	echo("<script language='javascript'>alert('苗字（ひらがな）を入力してください。');</script>\n");
	echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
	exit;
}
$emp_kn_lt_nm = mb_convert_kana($emp_kn_lt_nm, "HVc");
//苗字（かな）文字数チェック
if(strlen($emp_kn_lt_nm) > 20){
	echo("<script language='javascript'>alert('苗字（ひらがな）が長すぎます。');</script>\n");
	echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
	exit;
}
//名前（かな）null check
if($emp_kn_ft_nm ==""){
	echo("<script language='javascript'>alert('名前（ひらがな）を入力してください。');</script>\n");
	echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
	exit;
}
$emp_kn_ft_nm = mb_convert_kana($emp_kn_ft_nm, "HVc");
//名前（かな）文字数チェック
if(strlen($emp_kn_ft_nm) > 20){
	echo("<script language='javascript'>alert('名前（ひらがな）が長すぎます。');</script>\n");
	echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
	exit;
}

// 入力チェック
$birth_ymd = "";
if ($birth_y == "" && $birth_m == "" && $birth_d == "") {
	$birth_ymd = "";
} else if ($birth_y != "" && $birth_m != "" && $birth_d != "") {
	if (!checkdate($birth_m, $birth_d, $birth_y)) {
		echo("<script type='text/javascript'>alert('生年月日が不正です。');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		exit;
	}
	$birth_ymd = $birth_y.sprintf("%02d", $birth_m).sprintf("%02d", $birth_d);
} else {
	echo("<script type='text/javascript'>alert('生年月日が不正です。');</script>\n");
	echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
	exit;
}
if (strlen($emp_zip1) > 3) {
	echo("<script type='text/javascript'>alert('郵便番号1が長すぎます。');</script>\n");
	echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
	exit;
}
if (strlen($emp_zip2) > 4) {
	echo("<script type='text/javascript'>alert('郵便番号2が長すぎます。');</script>\n");
	echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
	exit;
}
if (strlen($emp_addr1) > 100) {
	echo("<script type='text/javascript'>alert('住所1が長すぎます。');</script>\n");
	echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
	exit;
}
if (strlen($emp_addr2) > 100) {
	echo("<script type='text/javascript'>alert('住所2が長すぎます。');</script>\n");
	echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
	exit;
}
if (strlen($emp_tel1) > 6) {
	echo("<script type='text/javascript'>alert('電話番号1が長すぎます。');</script>\n");
	echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
	exit;
}
if (strlen($emp_tel2) > 6) {
	echo("<script type='text/javascript'>alert('電話番号2が長すぎます。');</script>\n");
	echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
	exit;
}
if (strlen($emp_tel3) > 6) {
	echo("<script type='text/javascript'>alert('電話番号3が長すぎます。');</script>\n");
	echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
	exit;
}
if (strlen($emp_email2) > 120) {
	echo("<script type='text/javascript'>alert('E-Mailが長すぎます。');</script>\n");
	echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
	$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
	exit;
}

//職歴日付
$crST_ymd = "";
$crED_ymd = "";
for($i=0;$i<count($crST_Y);$i++){
	
	if ($crST_Y[$i] == "" && $crST_M[$i] == "" && $crST_D[$i] == "") {
		$crST_ymd[$i] = "";
	} else if ($crST_Y[$i] != "" && $crST_M[$i] != "" && $crST_D[$i] != "") {
		if (!checkdate($crST_M[$i], $crST_D[$i], $crST_Y[$i])) {
			echo("<script type='text/javascript'>alert('職歴の開始年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		$crST_ymd[$i] = $crST_Y[$i].sprintf("%02d", $crST_M[$i]).sprintf("%02d", $crST_D[$i]);
	} else {
		echo("<script type='text/javascript'>alert('職歴の開始年月日が不正です。');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}
	
	if ($crED_Y[$i] == "" && $crED_M[$i] == "" && $crED_D[$i] == "") {
		$crED_ymd[$i] = "";
	} else if ($crED_Y[$i] != "" && $crED_M[$i] != "" && $crED_D[$i] != "") {
		if (!checkdate($crED_M[$i], $crED_D[$i], $crED_Y[$i])) {
			echo("<script type='text/javascript'>alert('職歴の終了年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		$crED_ymd[$i] = $crED_Y[$i].sprintf("%02d", $crED_M[$i]).sprintf("%02d", $crED_D[$i]);
	} else {
		echo("<script type='text/javascript'>alert('職歴の終了年月日が不正です。');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}
}
//■■■■■■勤務経緯　入力チェック■■■■■■
if($authEdit[1]["auth_flg"] == true){	//更新権限
	//採用年月日
	$join_ymd = "";
	if($dispInfo[1]["item_flg"] == 2){	//必須チェック
		if ($join_y == "" || $join_m == "" || $join_d == "") {
			echo("<script type='text/javascript'>alert('".$dispInfo[1]["item_name"]."を入力してください');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
	}
	if ($join_y == "" && $join_m == "" && $join_d == "") {
		$join_ymd = "";
	} else if ($join_y != "" && $join_m != "" && $join_d != "") {
		if (!checkdate($join_m, $join_d, $join_y)) {
			echo("<script type='text/javascript'>alert('".$dispInfo[1]["item_name"]."が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		$join_ymd = $join_y.sprintf("%02d", $join_m).sprintf("%02d", $join_d);
	} else {
		echo("<script type='text/javascript'>alert('".$dispInfo[1]["item_name"]."が不正です。');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}
	//退職年月日
	$retire_ymd = "";
	if($dispInfo[2]["item_flg"] == 2){	//必須チェック
		if ($retire_y == "" || $retire_m == "" || $retire_d == "") {
			echo("<script type='text/javascript'>alert('".$dispInfo[2]["item_name"]."を入力してください');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
	}
	if ($retire_y == "" && $retire_m == "" && $retire_d == "") {
		$retire_ymd = "";
	} else if ($retire_y != "" && $retire_m != "" && $retire_d != "") {
		if (!checkdate($retire_m, $retire_d, $retire_y)) {
			echo("<script type='text/javascript'>alert('退職年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		$retire_ymd = $retire_y.sprintf("%02d", $retire_m).sprintf("%02d", $retire_d);
	} else {
		echo("<script type='text/javascript'>alert('退職年月日が不正です。');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}
	//退職区分     
	if($dispInfo[3]["item_flg"] == 2 && $retire_kubun == ""){	//必須チェック
		echo("<script type='text/javascript'>alert('".$dispInfo[3]["item_name"]."を入力してください');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}
	//退職の理由又は原因     
	if($dispInfo[4]["item_flg"] == 2 && $retire_riyu == ""){	//必須チェック
		echo("<script type='text/javascript'>alert('".$dispInfo[4]["item_name"]."を入力してください');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}
	//雇用の経過     
	if($dispInfo[5]["item_flg"] == 2 && $koyou_keika == ""){	//必須チェック
		echo("<script type='text/javascript'>alert('".$dispInfo[5]["item_name"]."を入力してください');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}
	//紹介先     
	if($dispInfo[6]["item_flg"] == 2 && $shoukaisaki == ""){	//必須チェック
		echo("<script type='text/javascript'>alert('".$dispInfo[6]["item_name"]."を入力してください');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}
	//採用区分     
	if($dispInfo[7]["item_flg"] == 2 && $join_kubun == "0"){	//必須チェック
		echo("<script type='text/javascript'>alert('".$dispInfo[7]["item_name"]."を入力してください');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}
	//被保険者証の記号番号     
	if($dispInfo[8]["item_flg"] == 2 && $wk_hiho_no == ""){	//必須チェック
		echo("<script type='text/javascript'>alert('".$dispInfo[8]["item_name"]."を入力してください');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}
	//基礎年金番号     
	if($dispInfo[9]["item_flg"] == 2 && $wk_kiso_no == ""){	//必須チェック
		echo("<script type='text/javascript'>alert('".$dispInfo[9]["item_name"]."を入力してください');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}
	//厚生年金基金番号     
	if($dispInfo[10]["item_flg"] == 2 && $wk_kousei_no == ""){	//必須チェック
		echo("<script type='text/javascript'>alert('".$dispInfo[10]["item_name"]."を入力してください');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}
	//雇用保険番号    
	if($dispInfo[11]["item_flg"] == 2 && $wk_koyou_no == ""){	//必須チェック
		echo("<script type='text/javascript'>alert('".$dispInfo[11]["item_name"]."を入力してください');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}

	//■■■■■■勤務経緯　産休期間　入力チェック■■■■■■
	$sanST_YMD = "";
	$sanED_YMD = "";
	for($i=0;$i<count($sanST_Y);$i++){
		//不完全入力はエラー
		//開始年月日
		if ($sanST_Y[$i] == "" && $sanST_M[$i] == "" && $sanST_D[$i] == "") {
			$sanST_YMD[$i] = "";
		} else if ($sanST_Y[$i] != "" && $sanST_M[$i] != "" && $sanST_D[$i] != "") {
			if (!checkdate($sanST_M[$i], $sanST_D[$i], $sanST_Y[$i])) {
				echo("<script type='text/javascript'>alert('産休期間開始年月日が不正です。');</script>\n");
				echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
				$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
				exit;
			}
			$sanST_YMD[$i] = $sanST_Y[$i].sprintf("%02d", $sanST_M[$i]).sprintf("%02d", $sanST_D[$i]);
		} else {
			echo("<script type='text/javascript'>alert('産休期間開始年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		//終了年月日
		if ($sanED_Y[$i] == "" && $sanED_M[$i] == "" && $sanED_D[$i] == "") {
			$sanED_YMD[$i] = "";
		} else if ($sanED_Y[$i] != "" && $sanED_M[$i] != "" && $sanED_D[$i] != "") {
			if (!checkdate($sanED_M[$i], $sanED_D[$i], $sanED_Y[$i])) {
				echo("<script type='text/javascript'>alert('産休期間終了年月日が不正です。');</script>\n");
				echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
				$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
				exit;
			}
			$sanED_YMD[$i] = $sanED_Y[$i].sprintf("%02d", $sanED_M[$i]).sprintf("%02d", $sanED_D[$i]);
		} else {
			echo("<script type='text/javascript'>alert('産休期間終了年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
	}
	
	//■■■■■■勤務経緯　育休期間　入力チェック■■■■■■
	$ikuST_YMD = "";
	$ikuED_YMD = "";
	for($i=0;$i<count($ikuST_Y);$i++){
		//不完全入力はエラー
		//開始年月日
		if ($ikuST_Y[$i] == "" && $ikuST_M[$i] == "" && $ikuST_D[$i] == "") {
			$ikuST_YMD[$i] = "";
		} else if ($ikuST_Y[$i] != "" && $ikuST_M[$i] != "" && $ikuST_D[$i] != "") {
			if (!checkdate($ikuST_M[$i], $ikuST_D[$i], $ikuST_Y[$i])) {
				echo("<script type='text/javascript'>alert('育休期間開始年月日が不正です。');</script>\n");
				echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
				$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
				exit;
			}
			$ikuST_YMD[$i] = $ikuST_Y[$i].sprintf("%02d", $ikuST_M[$i]).sprintf("%02d", $ikuST_D[$i]);
		} else {
			echo("<script type='text/javascript'>alert('育休期間開始年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		//終了年月日
		if ($ikuED_Y[$i] == "" && $ikuED_M[$i] == "" && $ikuED_D[$i] == "") {
			$ikuED_YMD[$i] = "";
		} else if ($ikuED_Y[$i] != "" && $ikuED_M[$i] != "" && $ikuED_D[$i] != "") {
			if (!checkdate($ikuED_M[$i], $ikuED_D[$i], $ikuED_Y[$i])) {
				echo("<script type='text/javascript'>alert('育休期間終了年月日が不正です。');</script>\n");
				echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
				$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
				exit;
			}
			$ikuED_YMD[$i] = $ikuED_Y[$i].sprintf("%02d", $ikuED_M[$i]).sprintf("%02d", $ikuED_D[$i]);
		} else {
			echo("<script type='text/javascript'>alert('育休期間終了年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
	}
}

//■■■■■■看護学歴・資格
if($authEdit[3]["auth_flg"] == true){	//更新権限
	//■■■■■■看護学歴・資格 専門学歴　入力チェック■■■■■■
	$chkCnt = 0;
	$gakuED_YMD = "";
	for($i=0;$i<count($gakuName);$i++){
		//不完全入力はエラー
		//年月日
		if ($gakuED_Y[$i] == "" && $gakuED_M[$i] == "" && $gakuED_D[$i] == "") {
			$gakuED_YMD[$i] = "";
		} else if ($gakuED_Y[$i] != "" && $gakuED_M[$i] != "" && $gakuED_D[$i] != "") {
			if (!checkdate($gakuED_M[$i], $gakuED_D[$i], $gakuED_Y[$i])) {
				echo("<script type='text/javascript'>alert('".$dispInfo[12]["item_name"]."の卒業年月日が不正です。');</script>\n");
				echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
				$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
				exit;
			}
			$gakuED_YMD[$i] = $gakuED_Y[$i].sprintf("%02d", $gakuED_M[$i]).sprintf("%02d", $gakuED_D[$i]);
		} else {
			echo("<script type='text/javascript'>alert('".$dispInfo[12]["item_name"]."の卒業年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		if($gakuED_YMD[$i] != "" && $gakuName[$i] != ""){
			$chkCnt++;
		}
	}
	if($dispInfo[12]["item_flg"] == 2 && $chkCnt == 0){
			echo("<script type='text/javascript'>alert('".$dispInfo[12]["item_name"]."を入力してください。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
	}
	
	//■■■■■■看護学歴・資格 認定看護師資格　入力チェック■■■■■■
	$chkCnt = 0;
	$proED_YMD = "";
	for($i=0;$i<count($proBunya);$i++){
		//不完全入力はエラー
		//年月日
		if ($proED_Y[$i] == "" && $proED_M[$i] == "" && $proED_D[$i] == "") {
			$proED_YMD[$i] = "";
		} else if ($proED_Y[$i] != "" && $proED_M[$i] != "" && $proED_D[$i] != "") {
			if (!checkdate($proED_M[$i], $proED_D[$i], $proED_Y[$i])) {
				echo("<script type='text/javascript'>alert('".$dispInfo[13]["item_name"]."の認定年月日が不正です。');</script>\n");
				echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
				$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
				exit;
			}
			$proED_YMD[$i] = $proED_Y[$i].sprintf("%02d", $proED_M[$i]).sprintf("%02d", $proED_D[$i]);
		} else {
			echo("<script type='text/javascript'>alert('".$dispInfo[13]["item_name"]."の認定年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		if($proED_YMD[$i] != "" && $proBunya[$i] != "" && $proKikan[$i] != ""){
			$chkCnt++;
		}
	}
	if($dispInfo[13]["item_flg"] == 2 && $chkCnt == 0){
			echo("<script type='text/javascript'>alert('".$dispInfo[13]["item_name"]."を入力してください。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
	}
	
	//■■■■■■看護学歴・資格 その他の認定資格　入力チェック■■■■■■
	$chkCnt = 0;
	$etcED_YMD = "";
	for($i=0;$i<count($etcBunya);$i++){
		//不完全入力はエラー
		//年月日
		if ($etcED_Y[$i] == "" && $etcED_M[$i] == "" && $etcED_D[$i] == "") {
			$etcED_YMD[$i] = "";
		} else if ($etcED_Y[$i] != "" && $etcED_M[$i] != "" && $etcED_D[$i] != "") {
			if (!checkdate($etcED_M[$i], $etcED_D[$i], $etcED_Y[$i])) {
				echo("<script type='text/javascript'>alert('".$dispInfo[14]["item_name"]."の認定年月日が不正です。');</script>\n");
				echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
				$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
				exit;
			}
			$etcED_YMD[$i] = $etcED_Y[$i].sprintf("%02d", $etcED_M[$i]).sprintf("%02d", $etcED_D[$i]);
		} else {
			echo("<script type='text/javascript'>alert('".$dispInfo[14]["item_name"]."の認定年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		if($etcED_YMD[$i] != "" && $etcBunya[$i] != "" && $etcKikan[$i] != ""){
			$chkCnt++;
		}
	}
	if($dispInfo[14]["item_flg"] == 2 && $chkCnt == 0){
			echo("<script type='text/javascript'>alert('".$dispInfo[14]["item_name"]."を入力してください。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
	}
	
	//■■■■■■看護学歴・資格 免許番号　入力チェック■■■■■■
	$license15_YMD = "";
	if($dispInfo[15]["item_flg"] == 2){
		if($license15No == "" || $license15_Y == "" || $license15_M == "" || $license15_D == ""){
			echo("<script type='text/javascript'>alert('".$dispInfo[15]["item_name"]."を入力してください。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
	}
	if ($license15_Y == "" && $license15_M == "" && $license15_D == "") {
		$license15_YMD = "";
	} else if ($license15_Y != "" && $license15_M != "" && $license15_D != "") {
		if (!checkdate($license15_M, $license15_D, $license15_Y)) {
			echo("<script type='text/javascript'>alert('".$dispInfo[15]["item_name"]."の取得年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		$license15_YMD = $license15_Y.sprintf("%02d", $license15_M).sprintf("%02d", $license15_D);
	} else {
		echo("<script type='text/javascript'>alert('".$dispInfo[15]["item_name"]."の取得年月日が不正です。');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}
	$license16_YMD = "";
	if($dispInfo[16]["item_flg"] == 2){
		if($license16No == "" || $license16_Y == "" || $license16_M == "" || $license16_D == ""){
			echo("<script type='text/javascript'>alert('".$dispInfo[16]["item_name"]."を入力してください。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
	}
	if ($license16_Y == "" && $license16_M == "" && $license16_D == "") {
		$license16_YMD = "";
	} else if ($license16_Y != "" && $license16_M != "" && $license16_D != "") {
		if (!checkdate($license16_M, $license16_D, $license16_Y)) {
			echo("<script type='text/javascript'>alert('".$dispInfo[16]["item_name"]."の取得年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		$license16_YMD = $license16_Y.sprintf("%02d", $license16_M).sprintf("%02d", $license16_D);
	} else {
		echo("<script type='text/javascript'>alert('".$dispInfo[16]["item_name"]."の取得年月日が不正です。');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}
	$license17_YMD = "";
	if($dispInfo[17]["item_flg"] == 2){
		if($license17No == "" || $license17_Y == "" || $license17_M == "" || $license17_D == ""){
			echo("<script type='text/javascript'>alert('".$dispInfo[17]["item_name"]."を入力してください。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
	}
	if ($license17_Y == "" && $license17_M == "" && $license17_D == "") {
		$license17_YMD = "";
	} else if ($license17_Y != "" && $license17_M != "" && $license17_D != "") {
		if (!checkdate($license17_M, $license17_D, $license17_Y)) {
			echo("<script type='text/javascript'>alert('".$dispInfo[17]["item_name"]."の取得年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		$license17_YMD = $license17_Y.sprintf("%02d", $license17_M).sprintf("%02d", $license17_D);
	} else {
		echo("<script type='text/javascript'>alert('".$dispInfo[17]["item_name"]."の取得年月日が不正です。');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}
	$license18_YMD = "";
	if($dispInfo[18]["item_flg"] == 2){
		if($license18No == "" || $license18_Y == "" || $license18_M == "" || $license18_D == "" || $license18Prv == ""){
			echo("<script type='text/javascript'>alert('".$dispInfo[18]["item_name"]."を入力してください。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
	}
	if ($license18_Y == "" && $license18_M == "" && $license18_D == "") {
		$license18_YMD = "";
	} else if ($license18_Y != "" && $license18_M != "" && $license18_D != "") {
		if (!checkdate($license18_M, $license18_D, $license18_Y)) {
			echo("<script type='text/javascript'>alert('".$dispInfo[18]["item_name"]."の取得年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		$license18_YMD = $license18_Y.sprintf("%02d", $license18_M).sprintf("%02d", $license18_D);
	} else {
		echo("<script type='text/javascript'>alert('".$dispInfo[18]["item_name"]."の取得年月日が不正です。');</script>\n");
		echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
		exit;
	}
	
	//■■■■■■看護学歴・資格 看護師協会　入力チェック■■■■■■
	$chkCnt = 0;
	$assKanED_YMD = "";
	for($i=0;$i<count($assKanName);$i++){
		//不完全入力はエラー
		//年月日
		if ($assKanED_Y[$i] == "" && $assKanED_M[$i] == "" && $assKanED_D[$i] == "") {
			$assKanED_YMD[$i] = "";
		} else if ($assKanED_Y[$i] != "" && $assKanED_M[$i] != "" && $assKanED_D[$i] != "") {
			if (!checkdate($assKanED_M[$i], $assKanED_D[$i], $assKanED_Y[$i])) {
				echo("<script type='text/javascript'>alert('".$dispInfo[19]["item_name"]."の入会年月日が不正です。');</script>\n");
				echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
				$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
				exit;
			}
			$assKanED_YMD[$i] = $assKanED_Y[$i].sprintf("%02d", $assKanED_M[$i]).sprintf("%02d", $assKanED_D[$i]);
		} else {
			echo("<script type='text/javascript'>alert('".$dispInfo[19]["item_name"]."の入会年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		if($assKanED_YMD[$i] != "" && $assKanName[$i] != "" && $assKanNo[$i] != ""){
			$chkCnt++;
		}
	}
	if($dispInfo[19]["item_flg"] == 2 && $chkCnt == 0){
			echo("<script type='text/javascript'>alert('".$dispInfo[19]["item_name"]."を入力してください。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
	}
	
	//■■■■■■看護学歴・資格 その他の協会等　入力チェック■■■■■■
	$chkCnt = 0;
	$assEtcED_YMD = "";
	for($i=0;$i<count($assEtcName);$i++){
		//不完全入力はエラー
		//年月日
		if ($assEtcED_Y[$i] == "" && $assEtcED_M[$i] == "" && $assEtcED_D[$i] == "") {
			$assEtcED_YMD[$i] = "";
		} else if ($assEtcED_Y[$i] != "" && $assEtcED_M[$i] != "" && $assEtcED_D[$i] != "") {
			if (!checkdate($assEtcED_M[$i], $assEtcED_D[$i], $assEtcED_Y[$i])) {
				echo("<script type='text/javascript'>alert('".$dispInfo[20]["item_name"]."の入会年月日が不正です。');</script>\n");
				echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
				$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
				exit;
			}
			$assEtcED_YMD[$i] = $assEtcED_Y[$i].sprintf("%02d", $assEtcED_M[$i]).sprintf("%02d", $assEtcED_D[$i]);
		} else {
			echo("<script type='text/javascript'>alert('".$dispInfo[20]["item_name"]."の入会年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		if($assEtcED_YMD[$i] != "" && $assEtcName[$i] != "" && $assEtcNo[$i] != ""){
			$chkCnt++;
		}
	}
	if($dispInfo[20]["item_flg"] == 2 && $chkCnt == 0){
			echo("<script type='text/javascript'>alert('".$dispInfo[20]["item_name"]."を入力してください。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
	}
}

//■■■■■■院外活動・研修
if($authEdit[4]["auth_flg"] == true){	//更新権限
	//■■■■■■院外活動・研修　院外委員会　入力チェック■■■■■■
	$chkCnt = 0;
	$cmmtNT_YMD = "";
	$cmmtED_YMD = "";
	for($i=0;$i<count($cmmtName);$i++){
		//不完全入力はエラー
		//認定年月日
		if ($cmmtNT_Y[$i] == "" && $cmmtNT_M[$i] == "" && $cmmtNT_D[$i] == "") {
			$cmmtNT_YMD[$i] = "";
		} else if ($cmmtNT_Y[$i] != "" && $cmmtNT_M[$i] != "" && $cmmtNT_D[$i] != "") {
			if (!checkdate($cmmtNT_M[$i], $cmmtNT_D[$i], $cmmtNT_Y[$i])) {
				echo("<script type='text/javascript'>alert('".$dispInfo[21]["item_name"]."の認定年月日が不正です。');</script>\n");
				echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
				$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
				exit;
			}
			$cmmtNT_YMD[$i] = $cmmtNT_Y[$i].sprintf("%02d", $cmmtNT_M[$i]).sprintf("%02d", $cmmtNT_D[$i]);
		} else {
			echo("<script type='text/javascript'>alert('".$dispInfo[21]["item_name"]."の認定年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		//終了年月日
		if ($cmmtED_Y[$i] == "" && $cmmtED_M[$i] == "" && $cmmtED_D[$i] == "") {
			$cmmtED_YMD[$i] = "";
		} else if ($cmmtED_Y[$i] != "" && $cmmtED_M[$i] != "" && $cmmtED_D[$i] != "") {
			if (!checkdate($cmmtED_M[$i], $cmmtED_D[$i], $cmmtED_Y[$i])) {
				echo("<script type='text/javascript'>alert('".$dispInfo[21]["item_name"]."の終了年月日が不正です。');</script>\n");
				echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
				$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
				exit;
			}
			$cmmtED_YMD[$i] = $cmmtED_Y[$i].sprintf("%02d", $cmmtED_M[$i]).sprintf("%02d", $cmmtED_D[$i]);
		} else {
			echo("<script type='text/javascript'>alert('".$dispInfo[21]["item_name"]."の終了年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		if($cmmtNT_YMD[$i] != "" && $cmmtED_YMD[$i] != "" && $cmmtName[$i] != ""){
			$chkCnt++;
		}
	}
	if($dispInfo[21]["item_flg"] == 2 && $chkCnt == 0){
			echo("<script type='text/javascript'>alert('".$dispInfo[21]["item_name"]."を入力してください。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
	}
	
	//■■■■■■院外活動・研修　院外発表　入力チェック■■■■■■
	$chkCnt = 0;
	$preED_YMD = "";
	for($i=0;$i<count($preTheme);$i++){
		//不完全入力はエラー
		//年月日
		if ($preED_Y[$i] == "" && $preED_M[$i] == "" && $preED_D[$i] == "") {
			$preED_YMD[$i] = "";
		} else if ($preED_Y[$i] != "" && $preED_M[$i] != "" && $preED_D[$i] != "") {
			if (!checkdate($preED_M[$i], $preED_D[$i], $preED_Y[$i])) {
				echo("<script type='text/javascript'>alert('".$dispInfo[22]["item_name"]."の発表年月日が不正です。');</script>\n");
				echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
				$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
				exit;
			}
			$preED_YMD[$i] = $preED_Y[$i].sprintf("%02d", $preED_M[$i]).sprintf("%02d", $preED_D[$i]);
		} else {
			echo("<script type='text/javascript'>alert('".$dispInfo[22]["item_name"]."の発表年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		if($preED_YMD[$i] != "" && $preTheme[$i] != "" && $preAgency[$i] != ""){
			$chkCnt++;
		}
	}
	if($dispInfo[22]["item_flg"] == 2 && $chkCnt == 0){
			echo("<script type='text/javascript'>alert('".$dispInfo[22]["item_name"]."を入力してください。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
	}
	
	//■■■■■■院外活動・研修　院外研修　入力チェック■■■■■■
	$chkCnt = 0;
	$trnED_YMD = "";
	for($i=0;$i<count($trnTheme);$i++){
		//不完全入力はエラー
		//年月日
		if ($trnED_Y[$i] == "" && $trnED_M[$i] == "" && $trnED_D[$i] == "") {
			$trnED_YMD[$i] = "";
		} else if ($trnED_Y[$i] != "" && $trnED_M[$i] != "" && $trnED_D[$i] != "") {
			if (!checkdate($trnED_M[$i], $trnED_D[$i], $trnED_Y[$i])) {
				echo("<script type='text/javascript'>alert('".$dispInfo[23]["item_name"]."の開催年月日が不正です。');</script>\n");
				echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
				$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
				exit;
			}
			$trnED_YMD[$i] = $trnED_Y[$i].sprintf("%02d", $trnED_M[$i]).sprintf("%02d", $trnED_D[$i]);
		} else {
			echo("<script type='text/javascript'>alert('".$dispInfo[23]["item_name"]."の開催年月日が不正です。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
		}
		if($trnED_YMD[$i] != "" && $trnTheme[$i] != "" && $trnAgency[$i] != "" && $trnArea[$i] != "" && $trnHiyou[$i] != ""){
			$chkCnt++;
		}
	}
	if($dispInfo[23]["item_flg"] == 2 && $chkCnt == 0){
			echo("<script type='text/javascript'>alert('".$dispInfo[23]["item_name"]."を入力してください。');</script>\n");
			echo("<script type='text/javascript'>document.frmJinji.submit();</script>\n");
			$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了(入力不備)");
			exit;
	}
}


//■■■■■■更新処理　基本■■■■■■
$res = $mdb2->startTransaction();							//トランザクション開始
if($res == "err_exit") err_exit();
//empmst
$res = $mdb2->updateBaseInfo($seSelID,   $emp_lt_nm,  $emp_ft_nm, $emp_kn_lt_nm, $emp_kn_ft_nm, 
							 $birth_ymd, $emp_sex,    $job_id,    $emp_zip1,     $emp_zip2, 
							 $emp_prv,   $emp_addr1,  $emp_addr2, $emp_tel1,     $emp_tel2, 
							 $emp_tel3,  $emp_email2);	
if($res == "err_exit") err_exit();
//jinji_relation
$res = $mdb2->updatePrint($jinjiID, $print_all, $marriage_sts, $working_hours, $working_comment);	
if($res == "err_exit") err_exit();
$res = $mdb2->commitTransaction();							//コミット
if($res == "err_exit") err_exit();

//職歴
$res = $mdb2->startTransaction();							//トランザクション開始
if($res == "err_exit") err_exit();
//DELETE
$res = $mdb2->delJinjiData($jinjiID, "jinji_career");	
if($res == "err_exit") err_exit();
//INSERT
for($i=0;$i<count($crH);$i++){
	if($crH[$i] != "" || $crD[$i] != "" || $crP[$i] != "" || $crST_ymd[$i] != "" || $crED_ymd[$i] != ""){
		$res = $mdb2->insertCareer($jinjiID, $crH[$i], $crD[$i], $crP[$i], $crST_ymd[$i], $crED_ymd[$i]);	
		if($res == "err_exit") err_exit();
	}
}
$res = $mdb2->commitTransaction();							//コミット
if($res == "err_exit") err_exit();


//■■■■■■更新処理　職員マスタ　特殊■■■■■■
if($authEdit[1]["auth_flg"] == true){	//更新権限
	if($dispInfo[1]["item_flg"] > 0){
		$res = $mdb2->startTransaction();							//トランザクション開始
		if($res == "err_exit") err_exit();

		$res = $mdb2->updateEmpMst($seSelID, "emp_join", $join_ymd);	
		if($res == "err_exit") err_exit();

		$res = $mdb2->commitTransaction();							//コミット
		if($res == "err_exit") err_exit();
	}

	if($dispInfo[2]["item_flg"] > 0){
		$res = $mdb2->startTransaction();							//トランザクション開始
		if($res == "err_exit") err_exit();

		$res = $mdb2->updateEmpMst($seSelID, "emp_retire", $retire_ymd);	
		if($res == "err_exit") err_exit();

		$res = $mdb2->commitTransaction();							//コミット
		if($res == "err_exit") err_exit();
	}
}

//■■■■■■更新処理　勤務条件マスタ■■■■■■
if($authEdit[1]["auth_flg"] == true){	//更新権限
	if($dispInfo[7]["item_flg"] > 0 ){
		$res = $mdb2->startTransaction();							//トランザクション開始
		if($res == "err_exit") err_exit();

		$res = $mdb2->updateCond($seSelID, $join_kubun);	
		if($res == "err_exit") err_exit();

		$res = $mdb2->commitTransaction();							//コミット
		if($res == "err_exit") err_exit();
	}
}



//■■■■■■更新処理　勤務経緯■■■■■■
if($authEdit[1]["auth_flg"] == true){	//更新権限
	$res = $mdb2->startTransaction();							//トランザクション開始
	if($res == "err_exit") err_exit();
	//DELETE
	$res = $mdb2->delJinjiData($jinjiID, "jinji_work_history");	
	if($res == "err_exit") err_exit();
	//INSERT
	$res = $mdb2->insertWork($jinjiID, 
							 $retire_kubun, $retire_riyu, $koyou_keika, 
							 $shoukaisaki, $wk_hiho_no, $wk_kiso_no, $wk_kousei_no, $wk_koyou_no);	
	if($res == "err_exit") err_exit();
	$res = $mdb2->commitTransaction();							//コミット
	if($res == "err_exit") err_exit();

	//■■■■■■更新処理　産休■■■■■■
	$res = $mdb2->startTransaction();							//トランザクション開始
	if($res == "err_exit") err_exit();
	//DELETE
	$res = $mdb2->delJinjiData($jinjiID, "jinji_maternity_leave");	
	if($res == "err_exit") err_exit();
	//INSERT
	for($i=0;$i<count($sanST_YMD);$i++){
		if($sanST_YMD[$i] != "" || $sanED_YMD[$i] != ""){
			$res = $mdb2->insertKyuka($jinjiID, "jinji_maternity_leave", $sanST_YMD[$i], $sanED_YMD[$i]);	
			if($res == "err_exit") err_exit();
		}
	}
	$res = $mdb2->commitTransaction();							//コミット
	if($res == "err_exit") err_exit();
	
	//■■■■■■更新処理　育休■■■■■■
	$res = $mdb2->startTransaction();							//トランザクション開始
	if($res == "err_exit") err_exit();
	//DELETE
	$res = $mdb2->delJinjiData($jinjiID, "jinji_parental_leave");	
	if($res == "err_exit") err_exit();
	//INSERT
	for($i=0;$i<count($ikuST_YMD);$i++){
		if($ikuST_YMD[$i] != "" || $ikuED_YMD[$i] != ""){
			$res = $mdb2->insertKyuka($jinjiID, "jinji_parental_leave", $ikuST_YMD[$i], $ikuED_YMD[$i]);	
			if($res == "err_exit") err_exit();
		}
	}
	$res = $mdb2->commitTransaction();							//コミット
	if($res == "err_exit") err_exit();
}

//■■■■■看護学歴・資格
if($authEdit[3]["auth_flg"] == true){	//更新権限
	//■■■■■■更新処理　専門学歴■■■■■■
	$res = $mdb2->startTransaction();							//トランザクション開始
	if($res == "err_exit") err_exit();
	//DELETE
	$res = $mdb2->delJinjiData($jinjiID, "jinji_educational_bg");	
	if($res == "err_exit") err_exit();
	//INSERT
	for($i=0;$i<count($gakuName);$i++){
		if($gakuName[$i] != "" || $gakuED_YMD[$i] != ""){
			$res = $mdb2->insertGakureki($jinjiID, $gakuName[$i], $gakuED_YMD[$i]);	
			if($res == "err_exit") err_exit();
		}
	}
	$res = $mdb2->commitTransaction();							//コミット
	if($res == "err_exit") err_exit();
	
	//■■■■■■更新処理　専門・認定看護師資格■■■■■■
	$res = $mdb2->startTransaction();							//トランザクション開始
	if($res == "err_exit") err_exit();
	//DELETE
	$res = $mdb2->delJinjiData($jinjiID, "jinji_q_pro");	
	if($res == "err_exit") err_exit();
	//INSERT
	for($i=0;$i<count($proED_YMD);$i++){
		if($proBunya[$i] != "" || $proKikan[$i] != "" || $proED_YMD[$i] != ""){
			$res = $mdb2->insertShikakuPro($jinjiID,  $proBunya[$i], $proKikan[$i], $proED_YMD[$i]);	
			if($res == "err_exit") err_exit();
		}
	}
	$res = $mdb2->commitTransaction();							//コミット
	if($res == "err_exit") err_exit();
	
	//■■■■■■更新処理　その他認定資格■■■■■■
	$res = $mdb2->startTransaction();							//トランザクション開始
	if($res == "err_exit") err_exit();
	//DELETE
	$res = $mdb2->delJinjiData($jinjiID, "jinji_q_etc");	
	if($res == "err_exit") err_exit();
	//INSERT
	for($i=0;$i<count($etcBunya);$i++){
		if($etcBunya[$i] != "" || $etcKikan[$i] != "" || $etcED_YMD[$i] != ""){
			$res = $mdb2->insertShikakuEtc($jinjiID, $etcBunya[$i], $etcKikan[$i], $etcED_YMD[$i]);	
			if($res == "err_exit") err_exit();
		}
	}
	$res = $mdb2->commitTransaction();							//コミット
	if($res == "err_exit") err_exit();
	
	//■■■■■■更新処理　免許番号■■■■■■
	$res = $mdb2->startTransaction();							//トランザクション開始
	if($res == "err_exit") err_exit();
	//DELETE
	$res = $mdb2->delJinjiData($jinjiID, "jinji_license");	
	if($res == "err_exit") err_exit();
	//INSERT
	if($license15No != "" || $license15_YMD != ""){
		$res = $mdb2->insertLicense($jinjiID, "15", $license15No, $license15_YMD, "");	
		if($res == "err_exit") err_exit();
	}
	if($license16No != "" || $license16_YMD != ""){
		$res = $mdb2->insertLicense($jinjiID, "16", $license16No, $license16_YMD, "");	
		if($res == "err_exit") err_exit();
	}
	if($license17No != "" || $license17_YMD != ""){
		$res = $mdb2->insertLicense($jinjiID, "17", $license17No, $license17_YMD, "");	
		if($res == "err_exit") err_exit();
	}
	if($license18No != "" || $license18_YMD != "" || $license18Prv != ""){
		$res = $mdb2->insertLicense($jinjiID, "18", $license18No, $license18_YMD, $license18Prv);	
		if($res == "err_exit") err_exit();
	}
	$res = $mdb2->commitTransaction();							//コミット
	if($res == "err_exit") err_exit();
	
	//■■■■■■更新処理　看護師協会■■■■■■
	$res = $mdb2->startTransaction();							//トランザクション開始
	if($res == "err_exit") err_exit();
	//DELETE
	$res = $mdb2->delJinjiData($jinjiID, "jinji_association");	
	if($res == "err_exit") err_exit();
	//INSERT
	for($i=0;$i<count($assKanName);$i++){
		if($assKanName[$i] != "" || $assKanNo[$i] != "" || $assKanED_YMD[$i] != ""){
			$res = $mdb2->insertKyoukai($jinjiID, "jinji_association", $assKanName[$i], $assKanNo[$i], $assKanED_YMD[$i]);	
			if($res == "err_exit") err_exit();
		}
	}
	$res = $mdb2->commitTransaction();							//コミット
	if($res == "err_exit") err_exit();
	
	//■■■■■■更新処理　その他の協会等■■■■■■
	$res = $mdb2->startTransaction();							//トランザクション開始
	if($res == "err_exit") err_exit();
	//DELETE
	$res = $mdb2->delJinjiData($jinjiID, "jinji_association_etc");	
	if($res == "err_exit") err_exit();
	//INSERT
	for($i=0;$i<count($assEtcName);$i++){
		if($assEtcName[$i] != "" || $assEtcNo[$i] != "" || $assEtcED_YMD[$i] != ""){
			$res = $mdb2->insertKyoukai($jinjiID, "jinji_association_etc", $assEtcName[$i], $assEtcNo[$i], $assEtcED_YMD[$i]);	
			if($res == "err_exit") err_exit();
		}
	}
	$res = $mdb2->commitTransaction();							//コミット
	if($res == "err_exit") err_exit();
}

//■■■■■看護学歴・資格
if($authEdit[4]["auth_flg"] == true){	//更新権限
	//■■■■■■更新処理　院外委員会■■■■■■
	$res = $mdb2->startTransaction();							//トランザクション開始
	if($res == "err_exit") err_exit();
	//DELETE
	$res = $mdb2->delJinjiData($jinjiID, "jinji_committee");	
	if($res == "err_exit") err_exit();
	//INSERT
	for($i=0;$i<count($cmmtName);$i++){
		if($cmmtName[$i] != "" || $cmmtNT_YMD[$i] != "" || $cmmtED_YMD[$i] != ""){
			$res = $mdb2->insertIinkai($jinjiID, $cmmtName[$i], $cmmtNT_YMD[$i], $cmmtED_YMD[$i]);	
			if($res == "err_exit") err_exit();
		}
	}
	$res = $mdb2->commitTransaction();							//コミット
	if($res == "err_exit") err_exit();
	
	//■■■■■■更新処理　院外発表■■■■■■
	$res = $mdb2->startTransaction();							//トランザクション開始
	if($res == "err_exit") err_exit();
	//DELETE
	$res = $mdb2->delJinjiData($jinjiID, "jinji_presentation");	
	if($res == "err_exit") err_exit();
	//INSERT
	for($i=0;$i<count($preTheme);$i++){
		if($preTheme[$i] != "" || $preAgency[$i] != "" || $preED_YMD[$i] != ""){
			$res = $mdb2->insertHappyou($jinjiID, $preTheme[$i], $preAgency[$i], $preED_YMD[$i]);	
			if($res == "err_exit") err_exit();
		}
	}
	$res = $mdb2->commitTransaction();							//コミット
	if($res == "err_exit") err_exit();
	
	//■■■■■■更新処理　院外研修■■■■■■
	$res = $mdb2->startTransaction();							//トランザクション開始
	if($res == "err_exit") err_exit();
	//DELETE
	$res = $mdb2->delJinjiData($jinjiID, "jinji_training");	
	if($res == "err_exit") err_exit();
	//INSERT
	for($i=0;$i<count($trnTheme);$i++){
		if($trnTheme[$i] != "" || $trnAgency[$i] != "" || $trnArea[$i] != "" || $trnED_YMD[$i] != "" || $trnHiyou[$i] != ""){
			$res = $mdb2->insertKenshu($jinjiID, $trnTheme[$i], $trnAgency[$i], $trnArea[$i], $trnED_YMD[$i], $trnHiyou[$i]);	
			if($res == "err_exit") err_exit();
		}
	}
	$res = $mdb2->commitTransaction();							//コミット
	if($res == "err_exit") err_exit();
}


//====================================
//処理終了ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.更新画面.更新：処理終了");
echo("<script language='javascript'>document.frmJinji.errBack.value='0';</script>");
echo("<script language='javascript'>document.frmJinji.submit();</script>");
exit;
?>
