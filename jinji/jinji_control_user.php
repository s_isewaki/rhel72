<?php
//************************************
//人事管理機能 管理　利用者一覧
//************************************

//====================================
//自ファイル名設定
//====================================
$fname = basename(__FILE__);

//====================================
//共通初期設定
//====================================
require_once("./jinji/jinji_common_init.php");

//====================================
//Smarty設定
//====================================
require_once("./jinji/conf/smarty.conf");

//====================================
//人事管理　参照機能専用モデル
//====================================
require_once("./jinji/model/jinji_control_user_model.php");
$mdb2 = new JinjiControlUserModel($log, CMX_DB_DSN, $jinji_UserEmpID, $sts);
if($sts == "err_exit") err_exit();

//====================================
//処理開始ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.利用者一覧：処理開始");

//====================================
//フォームデータ取得
//====================================
$ctrlView    = $_REQUEST["ctrlView"];
$ctrlSort    = $_REQUEST["ctrlSort"];

//初期値設定
if( trim($ctrlView) == ""){
	$ctrlView = "1";
}
if( trim($ctrlSort) == ""){
	$ctrlSort = "1";
}
//削除フラグ設定
$delFlg = ($ctrlView == "1") ? "f" : "t";
//ソートフラグ
$arrSort = "";
if ($ctrlSort == "1") {
	$arrSort[0] = "2";
	$arrSort[1] = "3";
} else if ($ctrlSort == "2") {
	$arrSort[0] = "1";
	$arrSort[1] = "3";
} else if ($ctrlSort == "3") {
	$arrSort[0] = "1";
	$arrSort[1] = "4";
} else {
	$arrSort[0] = "1";
	$arrSort[1] = "3";
}

//====================================
//一覧表示データ取得
//====================================
//一覧データ取得
$retData = $mdb2->getList($delFlg, $ctrlSort);
if($retData == "err_exit") err_exit();

//====================================
//画面データ・出力
//====================================
$smarty->assign("session", $session);
$smarty->assign("ctrlView", $ctrlView);
$smarty->assign("arrSort", $arrSort);
$smarty->assign("retData", $retData);

$smarty->display(basename(__FILE__, ".php").".tpl");

//====================================
//処理終了ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.利用者一覧：処理終了");

?>