<?php
//************************************
//人事管理機能 管理　権限.更新
//************************************

//====================================
//自ファイル名設定
//====================================
$fname = basename(__FILE__);

//====================================
//共通初期設定
//====================================
require_once("./jinji/jinji_common_init.php");

//====================================
//Smarty設定
//====================================
require_once("./jinji/conf/smarty.conf");

//====================================
//人事管理　参照機能専用モデル
//====================================
require_once("./jinji/model/jinji_control_auth_update_model.php");
$mdb2 = new JinjiControlAuthUpdateModel($log, CMX_DB_DSN, $jinji_UserEmpID, $sts);
if($sts == "err_exit") err_exit();

//====================================
//処理開始ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.権限.更新：処理開始");

//====================================
//フォームデータ取得
//====================================
$seSelID   = $_REQUEST["seSelID"];
$jinjiID   = $_REQUEST["jinjiID"];
$chkJobAll = $_REQUEST["chkJobAll"];
$arrJob    = $_REQUEST["chkJob"];
$chkRefAll = $_REQUEST["chkRefAll"];
$arrRef    = $_REQUEST["chkRef"];
$chkEdtAll = $_REQUEST["chkEdtAll"];
$arrEdt    = $_REQUEST["chkEdt"];

$chkPrintIryo = $_REQUEST["chkPrintIryo"];

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<script language="javascript">
function page_submit(){
	document.frmJinji.action = "./jinji_menu.php";
	document.frmJinji.method="POST";
	document.frmJinji.submit();
}
</script>
</head>

<body onLoad="page_submit();">
<form name="frmJinji" method="post">

<input type="hidden" name="session" value="<?php echo $session; ?>">
<input type="hidden" name="seSelID" value="<?php echo $seSelID; ?>">
<input type="hidden" name="mode" value="ctrl_auth">

</form>
</body>
</html>
<?php

//====================================
//ライセンス数チェック
//====================================
$res = $mdb2->getLicense();		//ライセンス数
if($res == "err_exit") err_exit();
$licenseMax = $res[0]["lcs_jinji_emps"];

$jobArray = array();
if($chkJobAll == "t"){
	//全て
	$res = $mdb2->startTransaction();							//トランザクション開始
	$res = $mdb2->getJobList();
	if($res == "err_exit") err_exit();
	$res = $mdb2->commitTransaction();							//コミット
	if(count($res) > 0){
		//職種　選択されている場合
		for($i=0;$i<count($res);$i++){
			$jobArray[$i] =  $res[$i]["job_id"];
		}
	}
}else{
	//「全て」以外で選択あり
	if(count($arrJob) > 0){
		//職種　選択されている場合
		$jobArray = $arrJob;
	}
}

if(count($jobArray) > 0)
{
	$res = $mdb2->getUseTotal($jobArray);		//該当部署所属総数
	if($res == "err_exit") err_exit();
	if(count($res) > $licenseMax){
		echo("<script language='javascript'>alert('ライセンス数が超えています。');</script>\n");
		echo("<script language='javascript'>document.frmJinji.submit();</script>\n");
		$log->info("(ID:".$jinji_UserEmpID.")人事管理.権限.更新：ライセンス数超過エラー");
		exit;
	}
}

//====================================
//更新処理
//====================================
//管理対象職種
$res = $mdb2->startTransaction();							//トランザクション開始
if($res == "err_exit") err_exit();

$res = $mdb2->delJinjiData($jinjiID, 'jinji_auth_job');		//一旦削除
if($res == "err_exit") err_exit();

if($chkJobAll == "t"){
	//全て
	$res = $mdb2->getJobList();
	if($res == "err_exit") err_exit();
	if(count($res) > 0){
		//職種　選択されている場合
		for($i=0;$i<count($res);$i++){
			$res2 = $mdb2->insertJob($jinjiID, $res[$i]["job_id"]);	//更新(INSERT)
			if($res2 == "err_exit") err_exit();
		}
	}
}else{
	//「全て」以外で選択あり
	if(count($arrJob) > 0){
		//職種　選択されている場合
		for($i=0;$i<count($arrJob);$i++){
			$res2 = $mdb2->insertJob($jinjiID, $arrJob[$i]);			//更新(INSERT)
			if($res2 == "err_exit") err_exit();
		}
	}
}
$res = $mdb2->commitTransaction();							//コミット
if($res == "err_exit") err_exit();

//参照権限
$res = $mdb2->startTransaction();							//トランザクション開始
if($res == "err_exit") err_exit();

$res = $mdb2->delJinjiData($jinjiID, 'jinji_auth_refer');	//一旦削除
if($res == "err_exit") err_exit();

if($chkRefAll == "t"){
	//全て
	$res = $mdb2->getCategory();
	if($res == "err_exit") err_exit();
	if(count($res) > 0){
		//職種　選択されている場合
		for($i=0;$i<count($res);$i++){
			$res2 = $mdb2->insertAuth('jinji_auth_refer', $jinjiID, $res[$i]["item_ctg"]);	//更新(INSERT)
			if($res2 == "err_exit") err_exit();
		}
	}
}else{
	//「全て」以外で選択あり
	if(count($arrRef) > 0){
		//職種　選択されている場合
		for($i=0;$i<count($arrRef);$i++){
			$res2 = $mdb2->insertAuth('jinji_auth_refer', $jinjiID, $arrRef[$i]);			//更新(INSERT)
			if($res2 == "err_exit") err_exit();
		}
	}
}
$res = $mdb2->commitTransaction();							//コミット
if($res == "err_exit") err_exit();

//更新権限
$res = $mdb2->startTransaction();							//トランザクション開始
if($res == "err_exit") err_exit();

$res = $mdb2->delJinjiData($jinjiID, 'jinji_auth_edit');	//一旦削除
if($res == "err_exit") err_exit();
if($chkEdtAll == "t"){
	//全て
	$res = $mdb2->getCategory();
	if($res == "err_exit") err_exit();
	if(count($res) > 0){
		//職種　選択されている場合
		for($i=0;$i<count($res);$i++){
			$res2 = $mdb2->insertAuth('jinji_auth_edit', $jinjiID, $res[$i]["item_ctg"]);	//更新(INSERT)
			if($res2 == "err_exit") err_exit();
		}
	}
}else{
	//「全て」以外で選択あり
	if(count($arrEdt) > 0){
		//職種　選択されている場合
		for($i=0;$i<count($arrEdt);$i++){
			$res2 = $mdb2->insertAuth('jinji_auth_edit', $jinjiID, $arrEdt[$i]);		//更新(INSERT)
			if($res2 == "err_exit") err_exit();
		}
	}
}
$res = $mdb2->commitTransaction();							//コミット
if($res == "err_exit") err_exit();

//帳票出力権限
$res = $mdb2->startTransaction();							//トランザクション開始
if($res == "err_exit") err_exit();

if($chkPrintIryo != "t"){
	$chkPrintIryo = "f";
}
$res = $mdb2->upPrintAuth($jinjiID, $chkPrintIryo);		//更新(UPDATE)
if($res == "err_exit") err_exit();

$res = $mdb2->commitTransaction();							//コミット
if($res == "err_exit") err_exit();

//====================================
//処理終了ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.権限.更新：処理終了");

?>
