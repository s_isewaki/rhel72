<?
//************************************
//人事管理機能 個人帳票
//************************************

//====================================
//自ファイル名設定
//====================================
$fname = basename(__FILE__);

//====================================
//共通初期設定
//====================================
require_once("./jinji/jinji_common_init_print.php");

//====================================
//処理開始ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.個人帳票：処理開始");

//====================================
//印刷対象ID
//====================================
$seSelID = $_REQUEST["print1ID"];

//人事ID取得(作成)
//操作者
$res = $mdbC->startTransaction();					//トランザクション開始
if($res == "err_exit") err_exit();
$user_jinjiID = $mdbC->getJinjiID($jinji_UserEmpID);	//人事ID取得(作成)
if($user_jinjiID == "err_exit") err_exit();
$res = $mdbC->commitTransaction();				//コミット
if($res == "err_exit") err_exit();
//選択対象
$res = $mdbC->startTransaction();				//トランザクション開始
if($res == "err_exit") err_exit();
$jinjiID = $mdbC->getJinjiID($seSelID);			//人事ID取得(作成)
if($jinjiID == "err_exit") err_exit();
$res = $mdbC->commitTransaction();				//コミット
if($res == "err_exit") err_exit();

//====================================
//人事管理　参照機能専用モデル
//====================================
require_once("./jinji/model/jinji_refer_model.php");
$mdb2 = new JinjiReferModel($log, CMX_DB_DSN, $jinji_UserEmpID, $sts);
if($sts == "err_exit") err_exit();

//■表示項目■■■■■■■■■■■■■■■■■■■
$res = $mdb2->getDispItemInfo();
if($res == "err_exit") err_exit();
$dispInfo = "";
for($i=0;$i<count($res);$i++){
	$dispInfo[$res[$i]["item_id"]] = $res[$i];
}

//■参照権限■■■■■■■■■■■■■■■■■■■
$res = $mdb2->getAuthRefer($user_jinjiID);
if($res == "err_exit") err_exit();
$authRefer = "";
for($i=0;$i<count($res);$i++){
	$authRefer[$res[$i]["item_ctg"]] = $res[$i];
}

//■基本属性■■■■■■■■■■■■■■■■■■■
$baseInfo = $mdb2->getBaseInfo($seSelID);
if($baseInfo == "err_exit") err_exit();

//都道府県
if($baseInfo[0]["emp_prv"] == ""){
	$addrState = "";
}else{
	$addrState = $jinjiState[$baseInfo[0]["emp_prv"]];
}
//写真
if(file_exists("profile/".$seSelID.".jpg")){
	$photoPath = "./profile/".$seSelID.".jpg";
}else{
	$photoPath = "./img/noprofile.jpg";
}

$emp_personal_id = $baseInfo[0]["emp_personal_id"];		//ID
if($baseInfo[0]["emp_birth"] != ""){
	$now_age         = $baseInfo[0]["now_age"];
}else{
	$now_age         = "-";
}

//採用区分
switch ($baseInfo[0]["duty_form"]){
	case 1: 
		$saiyo_kubun = "常勤";
		break;
	case 2: 
		$saiyo_kubun = "非常勤";
		break;
	default:	
		$saiyo_kubun = "未設定";
}

//在職年数取得
$zaishoku_y = "-";
//採用年月日表示なし　⇒　×
//採用年月日表示あり
//　採用年月日入力あり
//　　退職年月日表示なし　⇒　採用⇔今日
//　　退職年月日表示あり
//　　　退職年月日入力あり　⇒　採用⇔退職
//　　　退職年月日入力なし　⇒　採用⇔今日
//　採用年月日入力なし　⇒　×
if((int)$dispInfo[1]["item_flg"] > 0){
	if($baseInfo[0]["emp_join"] != ""){
		if((int)$dispInfo[2]["item_flg"] > 0){
			if($baseInfo[0]["emp_retire"] != ""){
				//採用⇔退職
				$zaishoku_y = calc_year($baseInfo[0]["emp_join"], $baseInfo[0]["emp_retire"]);
			}else{	
				//採用⇔今日
				$zaishoku_y = calc_year($baseInfo[0]["emp_join"], date("Ymd"));
			}
		}else{
			//採用⇔今日
			$zaishoku_y = calc_year($baseInfo[0]["emp_join"], date("Ymd"));
		}
	}	
}
function calc_year($start, $end){
	list($sy, $sm, $sd) = sscanf($start, "%4s%2s%2s");
	list($ey, $em, $ed) = sscanf($end, "%4s%2s%2s");
	$y = $ey - $sy;
	if($em * 100 + $ed < $sm * 100 + $sd) $y--;
	return $y;
}

//■職歴■■■■■■■■■■■■■■■■■■■
$career = $mdb2->getCareer($jinjiID);
if($career == "err_exit") err_exit();

//■氏名履歴■■■■■■■■■■■■■■■■■■■
$nameInfo = $mdb2->getNameHistory($seSelID);
if($nameInfo == "err_exit") err_exit();
if(count($nameInfo) > 0){
	$old_lt_nm    = $nameInfo[0]["lt_nm"];
	$old_kn_lt_nm = $nameInfo[0]["kn_lt_nm"];
}else{
	$old_lt_nm    = "";
	$old_kn_lt_nm = "";

}

//■部署履歴■■■■■■■■■■■■■■■■■■■
$bushoInfo = $mdb2->getBoshuInfo($seSelID);
if($bushoInfo == "err_exit") err_exit();

//■勤務経緯■■■■■■■■■■■■■■■■■■■
$kinmu = "";
$kinmu = $mdb2->getWork($jinjiID);
if($kinmu == "err_exit") err_exit();

//■産休期間■■■■■■■■■■■■■■■■■■■
$sankyu = $mdb2->getKyuka($jinjiID, "jinji_maternity_leave");
if($sankyu == "err_exit") err_exit();

//■育休期間■■■■■■■■■■■■■■■■■■■
$ikukyu = $mdb2->getKyuka($jinjiID, "jinji_parental_leave");
if($ikukyu == "err_exit") err_exit();

//■専門学歴■■■■■■■■■■■■■■■■■■■
$gakureki = $mdb2->getGakureki($jinjiID);
if($gakureki == "err_exit") err_exit();

//■専門・認定看護師資格■■■■■■■■■■■■■■■■■■■
$shikakuPro = $mdb2->getShikakuPro($jinjiID);
if($shikakuPro == "err_exit") err_exit();

//■その他認定資格■■■■■■■■■■■■■■■■■■■
$shikakuEtc = $mdb2->getShikakuEtc($jinjiID);
if($shikakuEtc == "err_exit") err_exit();

//■免許番号■■■■■■■■■■■■■■■■■■■
$menkyo15 = $mdb2->getMenkyoNo($jinjiID,"15");
if($menkyo15 == "err_exit") err_exit();
$menkyo16 = $mdb2->getMenkyoNo($jinjiID,"16");
if($menkyo16 == "err_exit") err_exit();
$menkyo17 = $mdb2->getMenkyoNo($jinjiID,"17");
if($menkyo17 == "err_exit") err_exit();
$menkyo18 = $mdb2->getMenkyoNo($jinjiID,"18");
if($menkyo18 == "err_exit") err_exit();

//■看護師協会■■■■■■■■■■■■■■■■■■■
$kyoukai = $mdb2->getKyoukai($jinjiID, "jinji_association");
if($kyoukai == "err_exit") err_exit();

//■その他協会■■■■■■■■■■■■■■■■■■■
$kyoukaiEtc = $mdb2->getKyoukai($jinjiID, "jinji_association_etc");
if($kyoukaiEtc == "err_exit") err_exit();

//■院外委員会■■■■■■■■■■■■■■■■■■■
$iinkai = $mdb2->getIinkai($jinjiID);
if($iinkai == "err_exit") err_exit();

//■院外発表■■■■■■■■■■■■■■■■■■■
$happyou = $mdb2->getHappyou($jinjiID);
if($happyou == "err_exit") err_exit();

//■院外研修■■■■■■■■■■■■■■■■■■■
$kenshu = $mdb2->getKenshu($jinjiID);
if($kenshu == "err_exit") err_exit();

//出力データ
if( $baseInfo[0]["marriage_sts"] == 1){
	$marriage_sts       = "既婚";
}else if($baseInfo[0]["marriage_sts"] == 2){
	$marriage_sts       = "その他";
}else{
	$marriage_sts       = "未婚";
}
$emp_lt_nm    = $baseInfo[0]["emp_lt_nm"];
$emp_ft_nm    = $baseInfo[0]["emp_ft_nm"];
$emp_kn_lt_nm = $baseInfo[0]["emp_kn_lt_nm"];
$emp_kn_ft_nm = $baseInfo[0]["emp_kn_ft_nm"];
if($baseInfo[0]["emp_birth"] == ""){
	$birth_y      = "----";
	$birth_m      = "--";
	$birth_d      = "--";
}else{
	$birth_y      = $baseInfo[0]["birth_y"];
	$birth_m      = $baseInfo[0]["birth_m"];
	$birth_d      = $baseInfo[0]["birth_d"];
}

if($baseInfo[0]["emp_sex"] == 1){
	$emp_sex       = "男";
}else if($baseInfo[0]["emp_sex"] == 2){
	$emp_sex       = "女";
}else{
	$emp_sex       = "未設定";
}

$job_nm       = $baseInfo[0]["job_nm"];
$emp_zip1     = $baseInfo[0]["emp_zip1"];
$emp_zip2     = $baseInfo[0]["emp_zip2"];
$emp_prv      = $addrState;

$emp_addr1    = $baseInfo[0]["emp_addr1"];
$emp_addr2    = $baseInfo[0]["emp_addr2"];
$emp_tel1     = $baseInfo[0]["emp_tel1"];
$emp_tel2     = $baseInfo[0]["emp_tel2"];
$emp_tel3     = $baseInfo[0]["emp_tel3"];
$emp_email2   = $baseInfo[0]["emp_email2"];

$working_hours   = $baseInfo[0]["working_time"];
$working_comment = $baseInfo[0]["working_comm"];

$crH     = array();
$crD     = array();
$crP     = array();
$crST_Y  = array();
$crST_M  = array();
$crST_D  = array();
$crED_Y  = array();
$crED_M  = array();
$crED_D  = array();
for($i=0;$i<count($career);$i++){
	$crH[$i] = $career[$i]["career_hospital"];
	$crD[$i] = $career[$i]["career_department"];
	$crP[$i] = $career[$i]["career_post"];
	if($career[$i]["career_st_date"] == ""){
		$crST_Y[$i] = "----";
		$crST_M[$i] = "--";
		$crST_D[$i] = "--";
	}else{
		$crST_Y[$i] = $career[$i]["st_y"];
		$crST_M[$i] = $career[$i]["st_m"];
		$crST_D[$i] = $career[$i]["st_d"];
	}
	if($career[$i]["career_ed_date"] == ""){
		$crED_Y[$i] = "----";
		$crED_M[$i] = "--";
		$crED_D[$i] = "--";
	}else{
		$crED_Y[$i] = $career[$i]["ed_y"];
		$crED_M[$i] = $career[$i]["ed_m"];
		$crED_D[$i] = $career[$i]["ed_d"];
	}
}	

if($baseInfo[0]["emp_join"] == ""){
	$join_y      = "----";
	$join_m      = "--";
	$join_d      = "--";
}else{
	$join_y      = $baseInfo[0]["join_y"];
	$join_m      = $baseInfo[0]["join_m"];
	$join_d      = $baseInfo[0]["join_d"];
}
if($baseInfo[0]["emp_retire"] == ""){
	$retire_y     = "----";
	$retire_m     = "--";
	$retire_d     = "--";
}else{
	$retire_y     = $baseInfo[0]["retire_y"];
	$retire_m     = $baseInfo[0]["retire_m"];
	$retire_d     = $baseInfo[0]["retire_d"];
}

$retire_kubun = $kinmu[0]["retire_division"];
$retire_riyu  = $kinmu[0]["retire_reason"];
$koyou_keika  = $kinmu[0]["emp_course"];
$shoukaisaki  = $kinmu[0]["referral"];
$join_kubun   = $saiyo_kubun;
$wk_hiho_no   = $kinmu[0]["kenko_number"]." ";
$wk_kiso_no   = $kinmu[0]["kiso_number"]." ";
$wk_kousei_no = $kinmu[0]["kosei_number"]." ";
$wk_koyou_no  = $kinmu[0]["koyo_number"]." ";

$sanST_Y = array();
$sanST_M = array();
$sanST_D = array();
$sanED_Y = array();
$sanED_M = array();
$sanED_D = array();
for($i=0;$i<count($sankyu);$i++){
	if($sankyu[$i]["start_date"] == ""){
		$sanST_Y[$i] = "----";
		$sanST_M[$i] = "--";
		$sanST_D[$i] = "--";
	}else{
		$sanST_Y[$i] = $sankyu[$i]["st_y"];
		$sanST_M[$i] = $sankyu[$i]["st_m"];
		$sanST_D[$i] = $sankyu[$i]["st_d"];
	}
	if($sankyu[$i]["end_date"] == ""){
		$sanED_Y[$i] = "----";
		$sanED_M[$i] = "--";
		$sanED_D[$i] = "--";
	}else{
		$sanED_Y[$i] = $sankyu[$i]["ed_y"];
		$sanED_M[$i] = $sankyu[$i]["ed_m"];
		$sanED_D[$i] = $sankyu[$i]["ed_d"];
	}
	
}

$ikuST_Y = array();
$ikuST_M = array();
$ikuST_D = array();
$ikuED_Y = array();
$ikuED_M = array();
$ikuED_D = array();
for($i=0;$i<count($ikukyu);$i++){
	if($ikukyu[$i]["start_date"] == ""){
		$ikuST_Y[$i] = "----";
		$ikuST_M[$i] = "----";
		$ikuST_D[$i] = "";
	}else{
		$ikuST_Y[$i] = $ikukyu[$i]["st_y"];
		$ikuST_M[$i] = $ikukyu[$i]["st_m"];
		$ikuST_D[$i] = $ikukyu[$i]["st_d"];
	}
	if($sankyu[$i]["end_date"] == ""){
		$ikuED_Y[$i] = "----";
		$ikuED_M[$i] = "--";
		$ikuED_D[$i] = "--";
	}else{
		$ikuED_Y[$i] = $ikukyu[$i]["ed_y"];
		$ikuED_M[$i] = $ikukyu[$i]["ed_m"];
		$ikuED_D[$i] = $ikukyu[$i]["ed_d"];
	}
	
}

$gakuName = array();
$gakuED_Y = array();
$gakuED_M = array();
$gakuED_D = array();
for($i=0;$i<count($gakureki);$i++){
	$gakuName[$i] = $gakureki[$i]["school_name"];
	if($gakureki[$i]["graduation_date"] == ""){
		$gakuED_Y[$i] = "----";
		$gakuED_M[$i] = "--";
		$gakuED_D[$i] = "--";
	}else{
		$gakuED_Y[$i] = $gakureki[$i]["ed_y"];
		$gakuED_M[$i] = $gakureki[$i]["ed_m"];
		$gakuED_D[$i] = $gakureki[$i]["ed_d"];
	}
}

$proBunya   = array();
$proKikan   = array();
$proED_Y    = array();
$proED_M    = array();
$proED_D    = array();
for($i=0;$i<count($shikakuPro);$i++){
	$proBunya[$i] = $shikakuPro[$i]["q_pro_field"];
	$proKikan[$i] = $shikakuPro[$i]["q_pro_agency"];
	if($shikakuPro[$i]["q_pro_date"] == ""){
		$proED_Y[$i] = "----";
		$proED_M[$i] = "--";
		$proED_D[$i] = "--";
	}else{
		$proED_Y[$i] = $shikakuPro[$i]["ed_y"];
		$proED_M[$i] = $shikakuPro[$i]["ed_m"];
		$proED_D[$i] = $shikakuPro[$i]["ed_d"];
	}
}

$etcBunya   = array();
$etcKikan   = array();
$etcED_Y    = array();
$etcED_M    = array();
$etcED_D    = array();
for($i=0;$i<count($shikakuEtc);$i++){
	$etcBunya[$i] = $shikakuEtc[$i]["q_etc_field"];
	$etcKikan[$i] = $shikakuEtc[$i]["q_etc_agency"];
	if($shikakuEtc[$i]["q_etc_date"] == ""){
		$etcED_Y[$i] = "----";
		$etcED_M[$i] = "--";
		$etcED_D[$i] = "--";
	}else{
		$etcED_Y[$i] = $shikakuEtc[$i]["ed_y"];
		$etcED_M[$i] = $shikakuEtc[$i]["ed_m"];
		$etcED_D[$i] = $shikakuEtc[$i]["ed_d"];
	}
}

if(count($menkyo15) > 0){
	$license15No  = " ".$menkyo15[0]["license_no"]." ";
	if($menkyo15[0]["license_date"] == ""){
		$license15_Y = "----";
		$license15_M = "--";
		$license15_D = "--";
	}else{
		$license15_Y = $menkyo15[0]["ed_y"];
		$license15_M = $menkyo15[0]["ed_m"];
		$license15_D = $menkyo15[0]["ed_d"];
	}
}else{
	$license15No  = "";
	$license15_Y  = "----";
	$license15_M  = "--";
	$license15_D  = "--";
}
if(count($menkyo16) > 0){
	$license16No  = " ".$menkyo16[0]["license_no"]." ";
	if($menkyo16[0]["license_date"] == ""){
		$license16_Y = "----";
		$license16_M = "--";
		$license16_D = "--";
	}else{
		$license16_Y = $menkyo16[0]["ed_y"];
		$license16_M = $menkyo16[0]["ed_m"];
		$license16_D = $menkyo16[0]["ed_d"];
	}
}else{
	$license16No  = "";
	$license16_Y  = "----";
	$license16_M  = "--";
	$license16_D  = "--";
}
if(count($menkyo17) > 0){
	$license17No  = " ".$menkyo17[0]["license_no"]." ";
	if($menkyo17[0]["license_date"] == ""){
		$license17_Y = "----";
		$license17_M = "--";
		$license17_D = "--";
	}else{
		$license17_Y = $menkyo17[0]["ed_y"];
		$license17_M = $menkyo17[0]["ed_m"];
		$license17_D = $menkyo17[0]["ed_d"];
	}
}else{
	$license17No  = "";
	$license17_Y  = "----";
	$license17_M  = "--";
	$license17_D  = "--";
}
if(count($menkyo18) > 0){
	$license18No  = " ".$menkyo18[0]["license_no"]." ";
	if($menkyo18[0]["license_prv"] == ""){
		$license18Prv = "";
	}else{
		$license18Prv = $jinjiState[$menkyo18[0]["license_prv"]];
	}
	if($menkyo18[0]["license_date"] == ""){
		$license18_Y = "----";
		$license18_M = "--";
		$license18_D = "--";
	}else{
		$license18_Y = $menkyo18[0]["ed_y"];
		$license18_M = $menkyo18[0]["ed_m"];
		$license18_D = $menkyo18[0]["ed_d"];
	}
}else{
	$license18No  = "";
	$license18_Y  = "----";
	$license18_M  = "--";
	$license18_D  = "--";
}

$assKanName = array();
$assKanNo   = array();
$assKanED_Y = array();
$assKanED_M = array();
$assKanED_D = array();
for($i=0;$i<count($kyoukai);$i++){
	$assKanName[$i] = $kyoukai[$i]["association_name"];
	$assKanNo[$i] = " ".$kyoukai[$i]["membership_no"]." ";
	if($kyoukai[$i]["association_date"] == ""){
		$assKanED_Y[$i] = "----";
		$assKanED_M[$i] = "--";
		$assKanED_D[$i] = "--";
	}else{
		$assKanED_Y[$i] = $kyoukai[$i]["ed_y"];
		$assKanED_M[$i] = $kyoukai[$i]["ed_m"];
		$assKanED_D[$i] = $kyoukai[$i]["ed_d"];
	}
}

$assEtcName = array();
$assEtcNo   = array();
$assEtcED_Y = array();
$assEtcED_M = array();
$assEtcED_D = array();
for($i=0;$i<count($kyoukaiEtc);$i++){
	$assEtcName[$i] = $kyoukaiEtc[$i]["association_name"];
	$assEtcNo[$i] = " ".$kyoukaiEtc[$i]["membership_no"]." ";
	if($kyoukaiEtc[$i]["association_date"] == ""){
		$assEtcED_Y[$i] = "----";
		$assEtcED_M[$i] = "--";
		$assEtcED_D[$i] = "--";
	}else{
		$assEtcED_Y[$i] = $kyoukaiEtc[$i]["ed_y"];
		$assEtcED_M[$i] = $kyoukaiEtc[$i]["ed_m"];
		$assEtcED_D[$i] = $kyoukaiEtc[$i]["ed_d"];
	}
}

$cmmtName   = array();
$cmmtNT_Y   = array();
$cmmtNT_M   = array();
$cmmtNT_D   = array();
$cmmtED_Y   = array();
$cmmtED_M   = array();
$cmmtED_D   = array();
for($i=0;$i<count($iinkai);$i++){
	$cmmtName[$i] = $iinkai[$i]["committee_name"];
	if($iinkai[$i]["certification_date"] == ""){
		$cmmtNT_Y[$i] = "----";
		$cmmtNT_M[$i] = "--";
		$cmmtNT_D[$i] = "--";
	}else{
		$cmmtNT_Y[$i] = $iinkai[$i]["st_y"];
		$cmmtNT_M[$i] = $iinkai[$i]["st_m"];
		$cmmtNT_D[$i] = $iinkai[$i]["st_d"];
	}
	if($iinkai[$i]["end_date"] == ""){
		$cmmtED_Y[$i] = "----";
		$cmmtED_M[$i] = "--";
		$cmmtED_D[$i] = "--";
	}else{
		$cmmtED_Y[$i] = $iinkai[$i]["ed_y"];
		$cmmtED_M[$i] = $iinkai[$i]["ed_m"];
		$cmmtED_D[$i] = $iinkai[$i]["ed_d"];
	}
}

$preTheme   = array();
$preAgency  = array();
$preED_Y    = array();
$preED_M    = array();
$preED_D    = array();
for($i=0;$i<count($happyou);$i++){
	$preTheme[$i] = $happyou[$i]["prsnt_theme"];
	$preAgency[$i] = $happyou[$i]["prsnt_agency"];
	if($happyou[$i]["prsnt_date"] == ""){
		$preED_Y[$i] = "----";
		$preED_M[$i] = "--";
		$preED_D[$i] = "--";
	}else{
		$preED_Y[$i] = $happyou[$i]["ed_y"];
		$preED_M[$i] = $happyou[$i]["ed_m"];
		$preED_D[$i] = $happyou[$i]["ed_d"];
	}
}

$trnTheme   = array();
$trnAgency  = array();
$trnArea    = array();
$trnED_Y    = array();
$trnED_M    = array();
$trnED_D    = array();
$trnHiyou   = array();
for($i=0;$i<count($kenshu);$i++){
	$trnTheme[$i] = $kenshu[$i]["trng_theme"];
	$trnAgency[$i] = $kenshu[$i]["trng_sponsor"];
	$trnArea[$i] = $kenshu[$i]["trng_venue"];
	$trnHiyou[$i] = $kenshu[$i]["trng_cost"];
	if($kenshu[$i]["trng_date"] == ""){
		$trnED_Y[$i] = "----";
		$trnED_M[$i] = "--";
		$trnED_D[$i] = "--";
	}else{
		$trnED_Y[$i] = $kenshu[$i]["ed_y"];
		$trnED_M[$i] = $kenshu[$i]["ed_m"];
		$trnED_D[$i] = $kenshu[$i]["ed_d"];
	}
}

//====================================
//	エクセル出力クラス
//====================================
require_once("jinji_print_excel_class.php");

// Excelオブジェクト生成
$excelObj = new ExcelWorkShop();

// Excel列名。列名をloopで連続するときに使うと便利です。
$excelColumnName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN');

// 全体の指定

// 用紙の向き。横方向の例
$excelObj->PageSetUp("virtical");

// 用紙サイズ
$excelObj->PaperSize( "A4" );

// 倍率と自動或いは利用者指定。利用者が指定して95%で印刷する場合
$excelObj->PageRatio( "USER" , "95" );

// 余白指定、左,右,上,下,ヘッダ,フッタ
$excelObj->SetMargin( "1.0" , "1,0", "1.5" , "1.5" , "0.5" , "0.5" );

// ヘッダー情報
$HeaderMessage = '&L人事情報';
$HeaderMessage = $HeaderMessage . "&R".date("Y.n.j");
$excelObj->SetPrintHeader( $HeaderMessage );

//フッター
$strFooter  = '&C&P / &N';
$excelObj->SetPrintFooter($strFooter);

// シート名
$excelObj->SetSheetName(mb2($emp_lt_nm." ".$emp_ft_nm));

// 列幅
$colWdArr = array(3, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5);
for($i=0;$i<count($colWdArr);$i++){
	$excelObj->SetColDim($excelColumnName[$i], $colWdArr[$i]);
}
//デフォルトフォント
$excelObj->setDefaultFont("ＭＳ Ｐゴシック", 9);

//基本属性■■■■■■■■■■■■■■■■■■
//ID
$excelObj->SetArea("A1"); 
$excelObj->SetFontBold();
$excelObj->SetValueJP2("A1", "職員ＩＤ：".$emp_personal_id, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
//氏名（漢字）
$excelObj->CellMerge2("B2:E2", "", "", "CCFFFF");
$excelObj->SetValueJP2("B2", "氏名（漢字）", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
$excelObj->CellMerge("F2:K2");
$excelObj->SetValueJP2("F2", $emp_lt_nm." ".$emp_ft_nm, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
//氏名（ひらがな）
$excelObj->CellMerge2("B3:E3", "", "", "CCFFFF");
$excelObj->SetValueJP2("B3", "氏名（ひらがな）", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
$excelObj->CellMerge("F3:K3");
$excelObj->SetValueJP2("F3", $emp_kn_lt_nm." ".$emp_kn_ft_nm, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
//生年月日・年齢
$excelObj->CellMerge2("B4:E4", "", "", "CCFFFF");
$excelObj->SetValueJP2("B4", "生年月日・年齢", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
$excelObj->CellMerge("F4:K4");
$excelObj->SetValueJP2("F4", $birth_y."/".$birth_m."/".$birth_d." ".$now_age."歳", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
//職種
$excelObj->CellMerge2("B5:E5", "", "", "CCFFFF");
$excelObj->SetValueJP2("B5", "職種", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
$excelObj->CellMerge("F5:K5");
$excelObj->SetValueJP2("F5", $job_nm, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
//住所
$excelObj->CellMerge2("B6:E6", "", "", "CCFFFF");
$excelObj->SetValueJP2("B6", "住所", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
$excelObj->CellMerge("F6:U6");
$excelObj->SetValueJP2("F6", "〒".$emp_zip1."-".$emp_zip2." ".$emp_prv.$emp_addr1." ".$emp_addr2 , "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
//電話番号
$excelObj->CellMerge2("B7:E7", "", "", "CCFFFF");
$excelObj->SetValueJP2("B7", "電話番号", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
$excelObj->CellMerge("F7:K7");
$excelObj->SetValueJP2("F7", $emp_tel1."-".$emp_tel2."-".$emp_tel3, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
//旧姓
$excelObj->CellMerge2("L2:M2", "", "", "CCFFFF");
$excelObj->SetValueJP2("L2", "旧姓", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
$excelObj->CellMerge("N2:U2");
$excelObj->SetValueJP2("N2", $old_lt_nm, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
//旧姓
$excelObj->CellMerge2("L3:M3", "", "", "CCFFFF");
$excelObj->SetValueJP2("L3", "旧姓", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
$excelObj->CellMerge("N3:U3");
$excelObj->SetValueJP2("N3", $old_kn_lt_nm, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
//性別
$excelObj->CellMerge2("L4:M4", "", "", "CCFFFF");
$excelObj->SetValueJP2("L4", "性別", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
$excelObj->CellMerge("N4:U4");
$excelObj->SetValueJP2("N4", $emp_sex, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
//婚姻
$excelObj->CellMerge2("L5:M5", "", "", "CCFFFF");
$excelObj->SetValueJP2("L5", "婚姻", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
$excelObj->CellMerge("N5:U5");
$excelObj->SetValueJP2("N5", $marriage_sts, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
//E-Mail
$excelObj->CellMerge2("L7:M7", "", "", "CCFFFF");
$excelObj->SetValueJP2("L7", "E-Mail", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
$excelObj->CellMerge("N7:U7");
$excelObj->SetValueJP2("N7", $emp_email2, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
//罫線
$excelObj->SetArea("B2:U7"); 
$excelObj->SetBorder("THIN", "", "all");
//＜勤務日及び勤務時間＞ 
$excelObj->SetValueJP2("B8", "＜勤務日及び勤務時間＞", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
$excelObj->CellMerge("B9:U9");
$excelObj->SetValueJP2("B9", $working_hours, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
$excelObj->SetArea("B9:U9"); 
$excelObj->SetBorder("THIN", "", "all");
//＜コメント＞ 
$excelObj->SetValueJP2("B10", "＜コメント＞", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
$excelObj->CellMerge("B11:U11");
$excelObj->SetValueJP2("B11", $working_comment, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
$excelObj->SetArea("B11:U11"); 
$excelObj->SetBorder("THIN", "", "all");
//＜職歴＞ 
$excelObj->SetValueJP2("B12", "＜職歴＞ ", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
//******ここから行数可変************************************************************************************
$cR = 13; //開始位置
if(count($crH) > 0){
	//タイトル行
	$stR = $cR;	//開始行退避（罫線開始位置）
	$excelObj->CellMerge2("B".$cR.":E".$cR, "", "", "CCFFFF");
	$excelObj->SetValueJP2("B".$cR, "病院名", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->CellMerge2("F".$cR.":I".$cR, "", "", "CCFFFF");
	$excelObj->SetValueJP2("F".$cR, "診療科", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->CellMerge2("J".$cR.":M".$cR, "", "", "CCFFFF");
	$excelObj->SetValueJP2("J".$cR, "役職", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->CellMerge2("N".$cR.":Q".$cR, "", "", "CCFFFF");
	$excelObj->SetValueJP2("N".$cR, "開始年月日", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$excelObj->CellMerge2("R".$cR.":U".$cR, "", "", "CCFFFF");
	$excelObj->SetValueJP2("R".$cR, "終了年月日", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$cR++;
	for($i=0;$i<count($crH);$i++){
		$excelObj->CellMerge("B".$cR.":E".$cR);
		$excelObj->SetValueJP2("B".$cR, $crH[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge("F".$cR.":I".$cR);
		$excelObj->SetValueJP2("F".$cR, $crD[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge("J".$cR.":M".$cR);
		$excelObj->SetValueJP2("J".$cR, $crH[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge("N".$cR.":Q".$cR);
		$excelObj->SetValueJP2("N".$cR, $crST_Y[$i]."/".$crST_M[$i]."/".$crST_D[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge("R".$cR.":U".$cR);
		$excelObj->SetValueJP2("R".$cR, $crED_Y[$i]."/".$crED_M[$i]."/".$crED_D[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$cR++;
	}
	//罫線
	$excelObj->SetArea("B".$stR.":U".($cR-1)); 
	$excelObj->SetBorder("THIN", "", "all");
	
}else{
	$excelObj->SetValueJP2("B13", "なし", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	$cR++;
}

//勤務経緯■■■■■■■■■■■■■■■■■■
if ($authRefer[1]["auth_flg"] == "t"){
	$cR++;
	//勤務経緯タイトル
	$excelObj->SetArea("A".$cR); 
	$excelObj->SetFontBold();
	$excelObj->SetValueJP2("A".$cR, $authRefer[1]["ctg_name"], "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	//位置情報
	$oR = $cR;
	for($i=1;$i<=12;$i++){
		if(($i % 2) == 0){
			$posT_S[$i] = "L".$oR;
			$posT_E[$i] = "P".$oR;
			$posD_S[$i] = "Q".$oR;
			$posD_E[$i] = "U".$oR;
		}else{
			$oR++;
			$posT_S[$i] = "B".$oR;
			$posT_E[$i] = "F".$oR;
			$posD_S[$i] = "G".$oR;
			$posD_E[$i] = "K".$oR;
		}
	}

	$out = 1;
	//採用年月日
	if($dispInfo[1]["item_flg"] > 0){
		$excelObj->CellMerge2($posT_S[$out].":".$posT_E[$out], "", "", "CCFFFF");
		$excelObj->SetValueJP2($posT_S[$out], $dispInfo[1]["item_name"], "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge($posD_S[$out].":".$posD_E[$out]);
		$excelObj->SetValueJP2($posD_S[$out], $join_y."/".$join_m."/".$join_d, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->SetArea($posT_S[$out].":".$posD_E[$out]); 
		$excelObj->SetBorder("THIN", "", "all");
		$out++;
	}
	//在職年数
	if($dispInfo[1]["item_flg"] > 0){
		$excelObj->CellMerge2($posT_S[$out].":".$posT_E[$out], "", "", "CCFFFF");
		$excelObj->SetValueJP2($posT_S[$out], "在職年数", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge($posD_S[$out].":".$posD_E[$out]);
		$excelObj->SetValueJP2($posD_S[$out], $zaishoku_y." 年", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->SetArea($posT_S[$out].":".$posD_E[$out]); 
		$excelObj->SetBorder("THIN", "", "all");
		$out++;
	}
	//退職年月日
	if($dispInfo[2]["item_flg"] > 0){
		$excelObj->CellMerge2($posT_S[$out].":".$posT_E[$out], "", "", "CCFFFF");
		$excelObj->SetValueJP2($posT_S[$out], $dispInfo[2]["item_name"], "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge($posD_S[$out].":".$posD_E[$out]);
		$excelObj->SetValueJP2($posD_S[$out], $retire_y."/".$retire_m."/".$retire_d, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->SetArea($posT_S[$out].":".$posD_E[$out]); 
		$excelObj->SetBorder("THIN", "", "all");
		$out++;
	}
	//退職区分
	if($dispInfo[3]["item_flg"] > 0){
		$excelObj->CellMerge2($posT_S[$out].":".$posT_E[$out], "", "", "CCFFFF");
		$excelObj->SetValueJP2($posT_S[$out], $dispInfo[3]["item_name"], "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge($posD_S[$out].":".$posD_E[$out]);
		$excelObj->SetValueJP2($posD_S[$out], $retire_kubun, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->SetArea($posT_S[$out].":".$posD_E[$out]); 
		$excelObj->SetBorder("THIN", "", "all");
		$out++;
	}
	//退職の理由又は原因
	if($dispInfo[4]["item_flg"] > 0){
		$excelObj->CellMerge2($posT_S[$out].":".$posT_E[$out], "", "", "CCFFFF");
		$excelObj->SetValueJP2($posT_S[$out], $dispInfo[4]["item_name"], "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge($posD_S[$out].":".$posD_E[$out]);
		$excelObj->SetValueJP2($posD_S[$out], $retire_riyu, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->SetArea($posT_S[$out].":".$posD_E[$out]); 
		$excelObj->SetBorder("THIN", "", "all");
		$out++;
	}
	//雇用の経過
	if($dispInfo[5]["item_flg"] > 0){
		$excelObj->CellMerge2($posT_S[$out].":".$posT_E[$out], "", "", "CCFFFF");
		$excelObj->SetValueJP2($posT_S[$out], $dispInfo[5]["item_name"], "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge($posD_S[$out].":".$posD_E[$out]);
		$excelObj->SetValueJP2($posD_S[$out], $koyou_keika, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->SetArea($posT_S[$out].":".$posD_E[$out]); 
		$excelObj->SetBorder("THIN", "", "all");
		$out++;
	}
	//紹介先
	if($dispInfo[6]["item_flg"] > 0){
		$excelObj->CellMerge2($posT_S[$out].":".$posT_E[$out], "", "", "CCFFFF");
		$excelObj->SetValueJP2($posT_S[$out], $dispInfo[6]["item_name"], "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge($posD_S[$out].":".$posD_E[$out]);
		$excelObj->SetValueJP2($posD_S[$out], $shoukaisaki, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->SetArea($posT_S[$out].":".$posD_E[$out]); 
		$excelObj->SetBorder("THIN", "", "all");
		$out++;
	}
	//採用区分
	if($dispInfo[7]["item_flg"] > 0){
		$excelObj->CellMerge2($posT_S[$out].":".$posT_E[$out], "", "", "CCFFFF");
		$excelObj->SetValueJP2($posT_S[$out], $dispInfo[7]["item_name"], "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge($posD_S[$out].":".$posD_E[$out]);
		$excelObj->SetValueJP2($posD_S[$out], $join_kubun, "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->SetArea($posT_S[$out].":".$posD_E[$out]); 
		$excelObj->SetBorder("THIN", "", "all");
		$out++;
	}
	//被保険者証の記号番号
	if($dispInfo[8]["item_flg"] > 0){
		$excelObj->CellMerge2($posT_S[$out].":".$posT_E[$out], "", "", "CCFFFF");
		$excelObj->SetValueJP2($posT_S[$out], $dispInfo[8]["item_name"], "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge($posD_S[$out].":".$posD_E[$out]);
		$excelObj->SetValueJP2($posD_S[$out], $wk_hiho_no, "VCENTER HLEFT", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->SetArea($posT_S[$out].":".$posD_E[$out]); 
		$excelObj->SetBorder("THIN", "", "all");
		$out++;
	}
	//基礎年金番号
	if($dispInfo[9]["item_flg"] > 0){
		$excelObj->CellMerge2($posT_S[$out].":".$posT_E[$out], "", "", "CCFFFF");
		$excelObj->SetValueJP2($posT_S[$out], $dispInfo[9]["item_name"], "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge($posD_S[$out].":".$posD_E[$out]);
		$excelObj->SetValueJP2($posD_S[$out], $wk_kiso_no, "VCENTER HLEFT", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->SetArea($posT_S[$out].":".$posD_E[$out]); 
		$excelObj->SetBorder("THIN", "", "all");
		$out++;
	}
	//厚生年金基金番号
	if($dispInfo[10]["item_flg"] > 0){
		$excelObj->CellMerge2($posT_S[$out].":".$posT_E[$out], "", "", "CCFFFF");
		$excelObj->SetValueJP2($posT_S[$out], $dispInfo[10]["item_name"], "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge($posD_S[$out].":".$posD_E[$out]);
		$excelObj->SetValueJP2($posD_S[$out], $wk_kousei_no, "VCENTER HLEFT", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->SetArea($posT_S[$out].":".$posD_E[$out]); 
		$excelObj->SetBorder("THIN", "", "all");
		$out++;
	}
	//雇用保険番号
	if($dispInfo[11]["item_flg"] > 0){
		$excelObj->CellMerge2($posT_S[$out].":".$posT_E[$out], "", "", "CCFFFF");
		$excelObj->SetValueJP2($posT_S[$out], $dispInfo[11]["item_name"], "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge($posD_S[$out].":".$posD_E[$out]);
		$excelObj->SetValueJP2($posD_S[$out], $wk_koyou_no, "VCENTER HLEFT", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->SetArea($posT_S[$out].":".$posD_E[$out]); 
		$excelObj->SetBorder("THIN", "", "all");
		$out++;
	}
	$cR = $cR + ceil(($out - 1) /2) + 1;
	
	$cRsan = $cR;	//産休行数カウンタ
	$cRiku = $cR;	//育児行数カウンタ
	//＜産休期間＞ 
	$excelObj->SetValueJP2("B".$cRsan, "＜産休期間＞ ", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	if(count($sanST_Y) > 0){
		$cRsan++;
		$stR = $cRsan;	//開始行退避
		$excelObj->CellMerge2("B".$cRsan.":E".$cRsan, "", "", "CCFFFF");
		$excelObj->SetValueJP2("B".$cRsan, "開始期間", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge2("F".$cRsan.":I".$cRsan, "", "", "CCFFFF");
		$excelObj->SetValueJP2("F".$cRsan, "終了期間", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		for($i=0;$i<count($sanST_Y);$i++){
			$cRsan++;
			$excelObj->CellMerge("B".$cRsan.":E".$cRsan);
			$excelObj->SetValueJP2("B".$cRsan, $sanST_Y[$i]."/".$sanST_M[$i]."/".$sanST_D[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge("F".$cRsan.":I".$cRsan);
			$excelObj->SetValueJP2("F".$cRsan, $sanED_Y[$i]."/".$sanED_M[$i]."/".$sanED_D[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		}
		//罫線
		$excelObj->SetArea("B".$stR.":I".$cRsan); 
		$excelObj->SetBorder("THIN", "", "all");
	}else{
		$cRsan++;
		$excelObj->SetValueJP2("B".$cRsan, "なし ", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	}
	//＜育休期間＞ 
	$excelObj->SetValueJP2("L".$cRiku, "＜育休期間＞ ", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	if(count($ikuST_Y) > 0){
		$cRiku++;
		$stR = $cRiku;	//開始行退避
		$excelObj->CellMerge2("L".$cRiku.":O".$cRiku, "", "", "CCFFFF");
		$excelObj->SetValueJP2("L".$cRiku, "開始期間", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge2("P".$cRiku.":T".$cRiku, "", "", "CCFFFF");
		$excelObj->SetValueJP2("P".$cRiku, "終了期間", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		for($i=0;$i<count($ikuST_Y);$i++){
			$cRiku++;
			$excelObj->CellMerge("L".$cRiku.":O".$cRiku);
			$excelObj->SetValueJP2("L".$cRiku, $ikuST_Y[$i]."/".$ikuST_M[$i]."/".$ikuST_D[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge("P".$cRiku.":T".$cRiku);
			$excelObj->SetValueJP2("P".$cRiku, $ikuED_Y[$i]."/".$ikuED_M[$i]."/".$ikuED_D[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		}
		//罫線
		$excelObj->SetArea("L".$stR.":T".$cRiku); 
		$excelObj->SetBorder("THIN", "", "all");
	}else{
		$cRiku++;
		$excelObj->SetValueJP2("L".$cRiku, "なし ", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	}
	
	//行カウンタ設定
	if($cRsan > $cRiku){
		$cR = $cRsan;
	}else{
		$cR = $cRiku;
	}
	$cR++;
}

//部署履歴■■■■■■■■■■■■■■■■■■
if ($authRefer[2]["auth_flg"] == "t"){
	$cR++;
	//部署履歴タイトル
	$excelObj->SetArea("A".$cR); 
	$excelObj->SetFontBold();
	$excelObj->SetValueJP2("A".$cR, $authRefer[2]["ctg_name"], "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	if(count($bushoInfo) > 0){
		$cR++;
		$stR = $cR;	//開始行退避
		$excelObj->CellMerge2("B".$cR.":E".$cR, "", "", "CCFFFF");
		$excelObj->SetValueJP2("B".$cR, "配属年月日", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge2("F".$cR.":U".$cR, "", "", "CCFFFF");
		$excelObj->SetValueJP2("F".$cR, "所属", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		for($i=0;$i<count($bushoInfo);$i++){
			$cR++;
			$excelObj->CellMerge("B".$cR.":E".$cR);
			$excelObj->SetValueJP2("B".$cR, $bushoInfo[$i]["date_ymd"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge("F".$cR.":U".$cR);
			$excelObj->SetValueJP2("F".$cR, $bushoInfo[$i]["class_nm"]." ".$bushoInfo[$i]["atrb_nm"]." ".$bushoInfo[$i]["dept_nm"]." ".$bushoInfo[$i]["room_nm"], "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		}
		$excelObj->SetArea("B".$stR.":U".$cR); 
		$excelObj->SetBorder("THIN", "", "all");
	}else{
		$cR++;
		$excelObj->SetValueJP2("B".$cR, "なし ", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	}
	$cR++;
}

//看護学歴・資格■■■■■■■■■■■■■■■■■■
if ($authRefer[3]["auth_flg"] == "t"){
	$cR++;
	//看護学歴・資格タイトル
	$excelObj->SetArea("A".$cR); 
	$excelObj->SetFontBold();
	$excelObj->SetValueJP2("A".$cR, $authRefer[3]["ctg_name"], "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	//専門学歴
	if($dispInfo[12]["item_flg"] > 0){
		$cR++;
		$excelObj->SetValueJP2("B".$cR, "＜".$dispInfo[12]["item_name"]."＞", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		if(count($gakuName) > 0){
			$cR++;
			$stR = $cR;	//開始行退避（罫線開始位置）
			$excelObj->CellMerge2("B".$cR.":I".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("B".$cR, "学校名称", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("J".$cR.":M".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("J".$cR, "卒業年月日", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			for($i=0;$i<count($gakuName);$i++){
				$cR++;
				$excelObj->CellMerge("B".$cR.":I".$cR);
				$excelObj->SetValueJP2("B".$cR, $gakuName[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("J".$cR.":M".$cR);
				$excelObj->SetValueJP2("J".$cR, $gakuED_Y[$i]."/".$gakuED_M[$i]."/".$gakuED_D[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			}
			$excelObj->SetArea("B".$stR.":M".$cR); 		
			$excelObj->SetBorder("THIN", "", "all");
		}else{
			$cR++;
			$excelObj->SetValueJP2("B".$cR, "なし", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		}
	}
	//専門・認定看護師資格
	if($dispInfo[13]["item_flg"] > 0){
		$cR++;
		$excelObj->SetValueJP2("B".$cR, "＜".$dispInfo[13]["item_name"]."＞", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		if(count($proBunya) > 0){
			$cR++;
			$stR = $cR;	//開始行退避（罫線開始位置）
			$excelObj->CellMerge2("B".$cR.":I".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("B".$cR, "分野", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("J".$cR.":Q".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("J".$cR, "認定期間", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("R".$cR.":U".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("R".$cR, "認定年月日", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			for($i=0;$i<count($proBunya);$i++){
				$cR++;
				$excelObj->CellMerge("B".$cR.":I".$cR);
				$excelObj->SetValueJP2("B".$cR, $proBunya[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("J".$cR.":Q".$cR);
				$excelObj->SetValueJP2("J".$cR, $proKikan[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("R".$cR.":U".$cR);
				$excelObj->SetValueJP2("R".$cR, $proED_Y[$i]."/".$proED_M[$i]."/".$proED_D[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			}
			$excelObj->SetArea("B".$stR.":U".$cR); 		
			$excelObj->SetBorder("THIN", "", "all");
		}else{
			$cR++;
			$excelObj->SetValueJP2("B".$cR, "なし", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		}
	}
	//その他(学会・研修等)の認定資格
	if($dispInfo[14]["item_flg"] > 0){
		$cR++;
		$excelObj->SetValueJP2("B".$cR, "＜".$dispInfo[14]["item_name"]."＞", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		if(count($etcBunya) > 0){
			$cR++;
			$stR = $cR;	//開始行退避（罫線開始位置）
			$excelObj->CellMerge2("B".$cR.":I".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("B".$cR, "分野", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("J".$cR.":Q".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("J".$cR, "認定期間", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("R".$cR.":U".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("R".$cR, "認定年月日", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			for($i=0;$i<count($etcBunya);$i++){
				$cR++;
				$excelObj->CellMerge("B".$cR.":I".$cR);
				$excelObj->SetValueJP2("B".$cR, $etcBunya[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("J".$cR.":Q".$cR);
				$excelObj->SetValueJP2("J".$cR, $etcKikan[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("R".$cR.":U".$cR);
				$excelObj->SetValueJP2("R".$cR, $etcED_Y[$i]."/".$etcED_M[$i]."/".$etcED_D[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			}
			$excelObj->SetArea("B".$stR.":U".$cR); 		
			$excelObj->SetBorder("THIN", "", "all");
		}else{
			$cR++;
			$excelObj->SetValueJP2("B".$cR, "なし", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		}
	}
	//免許番号
	if($dispInfo[15]["item_flg"] > 0 || $dispInfo[16]["item_flg"] > 0 || $dispInfo[17]["item_flg"] > 0 || $dispInfo[18]["item_flg"] > 0){
		$cR++;
		$excelObj->SetValueJP2("B".$cR, "＜免許番号＞", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$cR++;
		$stR = $cR;	//開始行退避（罫線開始位置）
		$excelObj->CellMerge2("B".$cR.":G".$cR, "", "", "CCFFFF");
		$excelObj->SetValueJP2("B".$cR, "免許種類", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge2("H".$cR.":I".$cR, "", "", "CCFFFF");
		$excelObj->SetValueJP2("H".$cR, "都道府県", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge2("J".$cR.":Q".$cR, "", "", "CCFFFF");
		$excelObj->SetValueJP2("J".$cR, "免許番号", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		$excelObj->CellMerge2("R".$cR.":U".$cR, "", "", "CCFFFF");
		$excelObj->SetValueJP2("R".$cR, "取得年月日", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		if($dispInfo[15]["item_flg"] > 0){
			$cR++;
			$excelObj->CellMerge("B".$cR.":G".$cR);
			$excelObj->SetValueJP2("B".$cR, $dispInfo[15]["item_name"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge("H".$cR.":I".$cR);
			$excelObj->SetValueJP2("H".$cR, "", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge("J".$cR.":Q".$cR);
			$excelObj->SetValueJP2("J".$cR, $license15No, "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge("R".$cR.":U".$cR);
			$excelObj->SetValueJP2("R".$cR, $license15_Y."/".$license15_M."/".$license15_D, "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		}
		if($dispInfo[16]["item_flg"] > 0){
			$cR++;
			$excelObj->CellMerge("B".$cR.":G".$cR);
			$excelObj->SetValueJP2("B".$cR, $dispInfo[16]["item_name"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge("H".$cR.":I".$cR);
			$excelObj->SetValueJP2("H".$cR, "", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge("J".$cR.":Q".$cR);
			$excelObj->SetValueJP2("J".$cR, $license16No, "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge("R".$cR.":U".$cR);
			$excelObj->SetValueJP2("R".$cR, $license16_Y."/".$license16_M."/".$license16_D, "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		}
		if($dispInfo[17]["item_flg"] > 0){
			$cR++;
			$excelObj->CellMerge("B".$cR.":G".$cR);
			$excelObj->SetValueJP2("B".$cR, $dispInfo[17]["item_name"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge("H".$cR.":I".$cR);
			$excelObj->SetValueJP2("H".$cR, "", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge("J".$cR.":Q".$cR);
			$excelObj->SetValueJP2("J".$cR, $license17No, "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge("R".$cR.":U".$cR);
			$excelObj->SetValueJP2("R".$cR, $license17_Y."/".$license17_M."/".$license17_D, "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		}
		if($dispInfo[18]["item_flg"] > 0){
			$cR++;
			$excelObj->CellMerge("B".$cR.":G".$cR);
			$excelObj->SetValueJP2("B".$cR, $dispInfo[18]["item_name"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge("H".$cR.":I".$cR);
			$excelObj->SetValueJP2("H".$cR, $license18Prv, "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge("J".$cR.":Q".$cR);
			$excelObj->SetValueJP2("J".$cR, $license18No, "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge("R".$cR.":U".$cR);
			$excelObj->SetValueJP2("R".$cR, $license18_Y."/".$license18_M."/".$license18_D, "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		}
		$excelObj->SetArea("B".$stR.":U".$cR); 		
		$excelObj->SetBorder("THIN", "", "all");
	}
	//看護師協会
	if($dispInfo[19]["item_flg"] > 0){
		$cR++;
		$excelObj->SetValueJP2("B".$cR, "＜".$dispInfo[19]["item_name"]."＞", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		if(count($assKanName) > 0){
			$cR++;
			$stR = $cR;	//開始行退避（罫線開始位置）
			$excelObj->CellMerge2("B".$cR.":I".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("B".$cR, $dispInfo[19]["item_name"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("J".$cR.":Q".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("J".$cR, "会員番号", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("R".$cR.":U".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("R".$cR, "入会年月日", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			for($i=0;$i<count($assKanName);$i++){
				$cR++;
				$excelObj->CellMerge("B".$cR.":I".$cR);
				$excelObj->SetValueJP2("B".$cR, $assKanName[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("J".$cR.":Q".$cR);
				$excelObj->SetValueJP2("J".$cR, $assKanNo[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("R".$cR.":U".$cR);
				$excelObj->SetValueJP2("R".$cR, $assKanED_Y[$i]."/".$assKanED_M[$i]."/".$assKanED_D[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			}
			$excelObj->SetArea("B".$stR.":U".$cR); 		
			$excelObj->SetBorder("THIN", "", "all");
		}else{
			$cR++;
			$excelObj->SetValueJP2("B".$cR, "なし", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		}
	}
	//その他の協会等
	if($dispInfo[20]["item_flg"] > 0){
		$cR++;
		$excelObj->SetValueJP2("B".$cR, "＜".$dispInfo[20]["item_name"]."＞", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		if(count($assEtcName) > 0){
			$cR++;
			$stR = $cR;	//開始行退避（罫線開始位置）
			$excelObj->CellMerge2("B".$cR.":I".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("B".$cR, $dispInfo[20]["item_name"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("J".$cR.":Q".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("J".$cR, "会員番号", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("R".$cR.":U".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("R".$cR, "入会年月日", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			for($i=0;$i<count($assEtcName);$i++){
				$cR++;
				$excelObj->CellMerge("B".$cR.":I".$cR);
				$excelObj->SetValueJP2("B".$cR, $assEtcName[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("J".$cR.":Q".$cR);
				$excelObj->SetValueJP2("J".$cR, $assEtcNo[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("R".$cR.":U".$cR);
				$excelObj->SetValueJP2("R".$cR, $assEtcED_Y[$i]."/".$assEtcED_M[$i]."/".$assEtcED_D[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			}
			$excelObj->SetArea("B".$stR.":U".$cR); 		
			$excelObj->SetBorder("THIN", "", "all");
		}else{
			$cR++;
			$excelObj->SetValueJP2("B".$cR, "なし", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		}
	}
	$cR++;
}

//院外活動・研修■■■■■■■■■■■■■■■■■■
if ($authRefer[4]["auth_flg"] == "t"){
	$cR++;
	//看護学歴・資格タイトル
	$excelObj->SetArea("A".$cR); 
	$excelObj->SetFontBold();
	$excelObj->SetValueJP2("A".$cR, $authRefer[4]["ctg_name"], "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
	//院外委員会
	if($dispInfo[21]["item_flg"] > 0){
		$cR++;
		$excelObj->SetValueJP2("B".$cR, "＜".$dispInfo[21]["item_name"]."＞", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		if(count($cmmtName) > 0){
			$cR++;
			$stR = $cR;	//開始行退避（罫線開始位置）
			$excelObj->CellMerge2("B".$cR.":M".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("B".$cR, $dispInfo[21]["item_name"], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("N".$cR.":Q".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("N".$cR, "認定年月日", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("R".$cR.":U".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("R".$cR, "終了年月日", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			for($i=0;$i<count($cmmtName);$i++){
				$cR++;
				$excelObj->CellMerge("B".$cR.":M".$cR);
				$excelObj->SetValueJP2("B".$cR, $cmmtName[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("N".$cR.":Q".$cR);
				$excelObj->SetValueJP2("N".$cR, $cmmtNT_Y[$i]."/".$cmmtNT_M[$i]."/".$cmmtNT_D[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("R".$cR.":U".$cR);
				$excelObj->SetValueJP2("R".$cR, $cmmtED_Y[$i]."/".$cmmtED_M[$i]."/".$cmmtED_D[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			}
			$excelObj->SetArea("B".$stR.":U".$cR); 		
			$excelObj->SetBorder("THIN", "", "all");
		}else{
			$cR++;
			$excelObj->SetValueJP2("B".$cR, "なし", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		}
	}
	//院外発表
	if($dispInfo[22]["item_flg"] > 0){
		$cR++;
		$excelObj->SetValueJP2("B".$cR, "＜".$dispInfo[22]["item_name"]."＞", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		if(count($preTheme) > 0){
			$cR++;
			$stR = $cR;	//開始行退避（罫線開始位置）
			$excelObj->CellMerge2("B".$cR.":I".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("B".$cR, "テーマ", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("J".$cR.":Q".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("J".$cR, "発表機関", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("R".$cR.":U".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("R".$cR, "発表年月日", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			for($i=0;$i<count($preTheme);$i++){
				$cR++;
				$excelObj->CellMerge("B".$cR.":I".$cR);
				$excelObj->SetValueJP2("B".$cR, $preTheme[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("J".$cR.":Q".$cR);
				$excelObj->SetValueJP2("J".$cR, $preAgency[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("R".$cR.":U".$cR);
				$excelObj->SetValueJP2("R".$cR, $preED_Y[$i]."/".$preED_M[$i]."/".$preED_D[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			}
			$excelObj->SetArea("B".$stR.":U".$cR); 		
			$excelObj->SetBorder("THIN", "", "all");
		}else{
			$cR++;
			$excelObj->SetValueJP2("B".$cR, "なし", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		}
	}
	//院外研修
	if($dispInfo[23]["item_flg"] > 0){
		$cR++;
		$excelObj->SetValueJP2("B".$cR, "＜".$dispInfo[23]["item_name"]."＞", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		if(count($trnTheme) > 0){
			$cR++;
			$stR = $cR;	//開始行退避（罫線開始位置）
			$excelObj->CellMerge2("B".$cR.":F".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("B".$cR, "テーマ", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("G".$cR.":K".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("G".$cR, "主催", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("L".$cR.":O".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("L".$cR, "開催地", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("P".$cR.":R".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("P".$cR, "開催年月日", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			$excelObj->CellMerge2("S".$cR.":U".$cR, "", "", "CCFFFF");
			$excelObj->SetValueJP2("S".$cR, "費用負担", "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			for($i=0;$i<count($trnTheme);$i++){
				$cR++;
				$excelObj->CellMerge("B".$cR.":F".$cR);
				$excelObj->SetValueJP2("B".$cR, $trnTheme[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("G".$cR.":K".$cR);
				$excelObj->SetValueJP2("G".$cR, $trnAgency[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("L".$cR.":O".$cR);
				$excelObj->SetValueJP2("L".$cR, $trnArea[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("P".$cR.":R".$cR);
				$excelObj->SetValueJP2("P".$cR, $trnED_Y[$i]."/".$trnED_M[$i]."/".$trnED_D[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
				$excelObj->CellMerge("S".$cR.":U".$cR);
				$excelObj->SetValueJP2("S".$cR, $trnHiyou[$i], "VCENTER HCENTER", mb1("ＭＳ Ｐゴシック"), 9);
			}
			$excelObj->SetArea("B".$stR.":U".$cR); 		
			$excelObj->SetBorder("THIN", "", "all");
		}else{
			$cR++;
			$excelObj->SetValueJP2("B".$cR, "なし", "VCENTER", mb1("ＭＳ Ｐゴシック"), 9);
		}
	}
}

$excelObj->SetArea("A1"); 
// 出力ファイル名
$filename = "Jinji_1_".$emp_personal_id.".xls";
// ダウンロード形式
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$filename);
header('Cache-Control: max-age=0');

$excelObj->OutPut();

// ファイル直接出力
//$excelObj->Save( $filename );

// その他、メソッドを用意していますのでduty_shift_menu_excel_5_workshop.phpのコメントをご参照ください
// 必要に応じてメソッドを追加、或いは継承クラスを作ってください。

//====================================
//処理終了ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.個人帳票：処理終了");

function mb1($str){
	return mb_convert_encoding($str, "UTF-8", "EUC-JP");
}
function mb2($str){
	return mb_convert_encoding(mb_convert_encoding($str, "sjis-win", "eucJP-win"), "UTF-8", "sjis-win");
}
?>
