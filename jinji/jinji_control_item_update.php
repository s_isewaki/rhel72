<?php
//************************************
//人事管理機能 管理　管理項目.更新
//************************************

//====================================
//自ファイル名設定
//====================================
$fname = basename(__FILE__);

//====================================
//共通初期設定
//====================================
require_once("./jinji/jinji_common_init.php");

//====================================
//Smarty設定
//====================================
require_once("./jinji/conf/smarty.conf");

//====================================
//人事管理　管理項目.更新機能専用モデル
//====================================
require_once("./jinji/model/jinji_control_item_update_model.php");
$mdb2 = new JinjiControlItemUpdateModel($log, CMX_DB_DSN, $jinji_UserEmpID, $sts);
if($sts == "err_exit") err_exit();

//====================================
//処理開始ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.管理項目.更新：処理開始");

//====================================
//フォームデータ取得
//====================================
$session   = $_REQUEST["session"];


//====================================
//管理項目最大ID取得
//====================================
$itemMax = $mdb2->getItemCount();
if($itemMax == "err_exit") err_exit();

//====================================
//管理フラグ取得・設定
//====================================
//トランザクション開始
$res = $mdb2->startTransaction();
if($res == "err_exit") err_exit();

//更新処理	
for($i=1;$i<=$itemMax;$i++){
	//if($_REQUEST["rqrdCilck".$i] == )
	if($_REQUEST["chkRqrd".$i] == "t"){
		$res = $mdb2->setItems($i, 2);
		if($res == "err_exit") err_exit();
	}else if($_REQUEST["chkDisp".$i] == "t"){
		$res = $mdb2->setItems($i, 1);
		if($res == "err_exit") err_exit();
	}else{
		$res = $mdb2->setItems($i, 0);
		if($res == "err_exit") err_exit();
	}
}
//コミット
$res = $mdb2->commitTransaction();
if($res == "err_exit") err_exit();

//====================================
//処理終了ログ出力
//====================================
$log->info("(ID:".$jinji_UserEmpID.")人事管理.管理項目.更新：処理終了");

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<script language="javascript">
function page_submit(){
	document.frmJinji.action = "./jinji_menu.php";
	document.frmJinji.method="POST";
	document.frmJinji.submit();
}
</script>
</head>

<body onLoad="page_submit();">
<form name="frmJinji" method="post">

<input type="hidden" name="session" value="<?php echo $session; ?>">
<input type="hidden" name="mode" value="ctrl_item">

</form>
</body>
</html>
