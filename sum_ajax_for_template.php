<?
ob_start();
$fname = $_SERVER["PHP_SELF"];
require_once("about_comedix.php");
require("get_values.php");
require("show_sinryo_top_header.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require("summary_common.ini");
ob_end_clean();

$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 59, $fname);
$con = @connect2db($fname);
if(!$session || !$con){
  echo('{"error":"initial_error"}');
  exit;
}


//******************************************************************************
// 担当医師、担当看護師、担当者情報の取得
// [request_tantousya_info] [pt_id] [nyuin_ymd] が必要
// tmpl_NyuinSinryoKeikakusyo.phpで利用
//******************************************************************************
if (@$_REQUEST["request_tantousya_info"]) {
    $nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, @$_REQUEST["pt_id"], @$_REQUEST["nyuin_ymd"], "", @$_REQUEST["is_about_mode"]);


    $sql =
        " select dr_emp_id, dr_nm from drmst".
        " where enti_id = ". (int)@$nyuin_info["inpt_enti_id"].
        " and sect_id = ". (int)@$nyuin_info["inpt_sect_id"].
        " and dr_id = ". (int)@$nyuin_info["dr_id"];
    $sel = select_from_table($con, $sql, "", $fname);
    $row = pg_fetch_array($sel);
    $dr_info = '{"dr_id":"'.$nyuin_info["dr_id"].'","dr_emp_id":"'.$row["dr_emp_id"].'","dr_name":"'.str_replace('"','\"',trim(@$row["dr_nm"])).'"}';


    $sql =
        " select nurse_emp_id, nurse_nm from numst".
        " where enti_id = ". (int)@$nyuin_info["inpt_enti_id"].
        " and sect_id = ". (int)@$nyuin_info["inpt_sect_id"].
        " and nurse_id = ". (int)@$nyuin_info["nurse_id"];
    $sel = select_from_table($con, $sql, "", $fname);
    $row = pg_fetch_array($sel);
    $nurse_info = '{"nurse_id":"'.$nyuin_info["nurse_id"].'","nurse_emp_id":"'.$row["nurse_emp_id"].'","nurse_name":"'.str_replace('"','\"',trim(@$row["nurse_nm"])).'"}';

    $sql =
        " select iop.emp_id, iop.order_no, emp.emp_lt_nm, emp.emp_ft_nm, job.job_nm from inptop iop".
        " left outer join empmst emp on (emp.emp_id = iop.emp_id)".
        " left outer join jobmst job on (job.job_id = emp.emp_job)".
        " where ptif_id = '".pg_escape_string(@$_REQUEST["pt_id"])."' order by iop.order_no";
    $sel = select_from_table($con, $sql, "", $fname);
    $tantou_list = array();
    while ($row = pg_fetch_array($sel)){
        $tantou_list[] = '{"order_no":"'.$row["order_no"].'","emp_id":"'.$row["emp_id"].'", "emp_name":"'.str_replace('"','\"',$row["emp_lt_nm"]." ".$row["emp_ft_nm"]).'","job_name":"'.str_replace('"','\"',$row["job_nm"]).'"}';
    }

    echo '{"request":"request_tantousya_info","dr_info":'.$dr_info.',"nurse_info":'.$nurse_info.',"tantousya_list":['.implode(",",$tantou_list).']}';
    die;
}


//******************************************************************************
// 担当医師、担当看護師、担当者情報の取得
// [request_nyuin_extra_info] [pt_id] [target_ymd] が必要
// tmpl_tmos_TaiinjiSummary.phpで利用
//******************************************************************************
if (@$_REQUEST["request_nyuin_extra_info"]) {
    $nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, @$_REQUEST["pt_id"], @$_REQUEST["target_ymd"], "", @$_REQUEST["is_about_mode"]);
    $xml = new template_xml_class();
//  $taiinsaki_kubun = trim($xml->get_mst_item_name(0, 0, "A307", $nyuin_info["inpt_out_pos_cd"])." ".$nyuin_info["inpt_out_pos_dtl"]);
  $taiinsaki_kubun = trim($xml->get_mst_item_name(0, 0, "A307", $nyuin_info["inpt_out_pos_cd"]));
  $taiinriyu = $xml->get_mst_item_name(0, 0, "A308", $nyuin_info["inpt_out_rsn_cd"]);
    $kaigodo = $xml->get_mst_item_name(0, 0, "A302", $nyuin_info["inpt_care_grd_cd"]);
    $sinsyousya = $xml->get_mst_item_name(0, 0, "A303", $nyuin_info["inpt_dis_grd_cd"]);

  $out_dt = $nyuin_info["inpt_out_dt"] ;
  if (!$out_dt) $out_dt = $nyuin_info["inpt_out_res_dt"] ;

  $result_array = array(""=>"", "1"=>"死亡", "2"=>"完治", "3"=>"不変", "4"=>"軽快", "5"=>"後遺症残", "6"=>"不明", "7"=>"悪化", "8"=>"その他") ;
//  $sql =
//    " select" .
//    "   inpt_result" .
//    "  ,inpt_result_dtl" .
//    " from" .
//    "   inpthist" .
//    " where" .
//    "       ptif_id     = '" . @$_REQUEST["pt_id"] . "'" .
//    "   and inpt_out_dt = '" . $nyuin_info["inpt_out_dt"] . "'" ;
//  $sel = $c_sot_util->select_from_table($sql) ;
//  $inpt_result     = @pg_fetch_result($sel, 0, "inpt_result"    ) ;
//  $inpt_result_dtl = @pg_fetch_result($sel, 0, "inpt_result_dtl") ;
  $inpt_result     = $nyuin_info["inpt_result"] ;
  $inpt_result_dtl = $nyuin_info["inpt_result_dtl"] ;
  $inpt_result     = @$result_array[$inpt_result] ;
  $inpt_result .= (" " . $inpt_result_dtl) ;
//  if (strlen($inpt_result) > 1) $taiinsaki_kubun .= (" (" . $inpt_result . ")") ;

    $inpt_out_inst_cd = pg_escape_string($nyuin_info["inpt_out_inst_cd"]);
    $sql =
        " select a.mst_cd, a.mst_name, b.rireki_index, a.address1".
        " from institemmst a".
        " left join (".
        "   select mst_cd, max(rireki_index) as rireki_index".
        "   from institemrireki group by mst_cd".
        " ) b on b.mst_cd = a.mst_cd".
        " where a.mst_cd = '" . $inpt_out_inst_cd . "'".
        " order by a.mst_cd";
    $sel = $c_sot_util->select_from_table($sql);
    $syoukaisaki_inst = @pg_fetch_result($sel, 0, "mst_name");
    $out_rireki_index = @pg_fetch_result($sel, 0, "rireki_index");

    $inpt_out_sect_cd = @pg_escape_string($nyuin_info["inpt_out_sect_cd"]);
    $inpt_out_doctor_no = @pg_escape_string($nyuin_info["inpt_out_doctor_no"]);
    $sql =
        " select * from institemrireki".
        " where mst_cd = '" . $inpt_out_inst_cd . "'".
        " and rireki_index = " . (int)$out_rireki_index.
        " and item_cd = '" . $inpt_out_sect_cd . "'";
    $sel = $c_sot_util->select_from_table($sql);
    $syoukaisaki_sect = @pg_fetch_result($sel, 0, "item_name");
    if ($inpt_out_doctor_no) $syoukaisaki_doctor_name = @pg_fetch_result($sel, 0, "doctor_name".$inpt_out_doctor_no);
    $syoukaisaki = trim(trim($syoukaisaki_inst . "  " . $syoukaisaki_sect) . "  " . @$syoukaisaki_doctor_name);

    $inpt_intro_inst_cd = pg_escape_string($nyuin_info["inpt_intro_inst_cd"]);
    $sql =
        " select a.mst_cd, a.mst_name, b.rireki_index, a.address1".
        " from institemmst a".
        " left join (".
        "   select mst_cd, max(rireki_index) as rireki_index".
        "   from institemrireki group by mst_cd".
        " ) b on b.mst_cd = a.mst_cd".
        " where a.mst_cd = '" . $inpt_intro_inst_cd . "'".
        " order by a.mst_cd";
    $sel = $c_sot_util->select_from_table($sql);
    $syoukaimoto_inst = @pg_fetch_result($sel, 0, "mst_name");
    $intro_rireki_index = @pg_fetch_result($sel, 0, "rireki_index");

    $inpt_intro_sect_cd = @pg_escape_string($nyuin_info["inpt_intro_sect_cd"]);
    $inpt_intro_doctor_no = @pg_escape_string($nyuin_info["inpt_intro_doctor_no"]);
    $sql =
        " select * from institemrireki".
        " where mst_cd = '" . $inpt_intro_inst_cd . "'".
        " and rireki_index = " . (int)$intro_rireki_index.
        " and item_cd = '" . $inpt_intro_sect_cd . "'";
    $sel = $c_sot_util->select_from_table($sql);
    $syoukaimoto_sect = @pg_fetch_result($sel, 0, "item_name");
    if ($inpt_intro_doctor_no) $syoukaimoto_doctor_name = @pg_fetch_result($sel, 0, "doctor_name".$inpt_intro_doctor_no);
    $syoukaimoto = trim(trim($syoukaimoto_inst . "  " . $syoukaimoto_sect) . "  " . @$syoukaimoto_doctor_name);
    echo '{';
    echo '"request":"request_nyuin_extra_info"';
    echo ',"target_ymd_jp":"'.str_replace('"','\"',$c_sot_util->kanji_ymd(@$_REQUEST["target_ymd"])).'"';
    echo ',"nyuin_ymd_jp":"'.str_replace('"','\"',$c_sot_util->kanji_ymd($nyuin_info["inpt_in_dt"])).'"';
  echo ',"taiin_ymd_jp":"'.str_replace('"','\"',$c_sot_util->kanji_ymd($out_dt)).'"';
echo ',"nyuin_ymd":"'.str_replace('"','\"',$nyuin_info["inpt_in_dt"]).'"';
echo ',"taiin_ymd":"'.str_replace('"','\"',$out_dt).'"';
    echo ',"taiinsaki_kubun_html":"'.str_replace('"','\"',h($taiinsaki_kubun)).'"';
echo ',"inpt_result":"'.str_replace('"','\"',h($inpt_result)).'"';
    echo ',"syoukaisaki_all_html":"'.str_replace('"','\"',h($syoukaisaki)).'"';
    echo ',"syoukaimoto_all_html":"'.str_replace('"','\"',h($syoukaimoto)).'"';
    echo ',"kaigodo_html":"'.str_replace('"','\"',h($kaigodo)).'"';
    echo ',"sinsyou_tecyou_html":"'.str_replace('"','\"',h($sinsyousya)).'"';
    echo '}';
    die;
}

//******************************************************************************
// FIM評価表の情報の取得
// [request_fim_info] [pt_id] [nyuin_ymd] [taiin_ymd] が必要
// tmpl_tmos_TaiinjiSummary.phpで利用
//******************************************************************************
if (@$_REQUEST["request_fim_info"])
{
  // 処理対象のXMLファイルを取得する
  $sql =
    " select" .
    "   xml_file_name" .
    " from" .
    "   sum_xml sx" .
    "   inner join summary sm on (sm.ptif_id = sx.ptif_id and sm.summary_id = sx.summary_id)" .
    "   inner join tmplmst tm on (sm.tmpl_id = tm.tmpl_id)" .
    " where" .
    "       sx.ptif_id = '" . @$_REQUEST["pt_id"] . "'" .
    "   and (sx.delete_flg is null or sx.delete_flg <> '1')" .
    "   and tm.tmpl_name = 'FIM評価表'" ;
  $sel = $c_sot_util->select_from_table($sql) ;

  // すべてのXMLファイルからコンテンツを取得する
  $xml = new template_xml_class() ;
  $conts = array() ;
  while($row = pg_fetch_array($sel))
  {
    $xmlFileName = $row["xml_file_name"] ;
    $data = $xml->get_xml_object_array_from_db($con, $fname, $xmlFileName) ;
    $worksheet = @$data->nodes["template"]->nodes["FIMHyoukahyou"] ;
    $hyouka_ymd = @$worksheet->nodes["hyouka_ymd"]->value ; // 例：20101206
    if (!$hyouka_ymd) continue ;
    $undou_subtotal  = @$worksheet->nodes["undou" ]->nodes["subtotal"]->value ;
    $ninchi_subtotal = @$worksheet->nodes["ninchi"]->nodes["subtotal"]->value ;
    $total           = @$worksheet->nodes["total" ]->value ;
    $totals = array() ;
    $totals[0] = $undou_subtotal  ;
    $totals[1] = $ninchi_subtotal ;
    $totals[2] = $total           ;
    $conts[$hyouka_ymd] = $totals ;
  }
  // 構築した配列をキー(評価日)で昇順ソートする
  ksort($conts) ;

  $date_diff = 0 ;
  $nyuin_ymd = @$_REQUEST["nyuin_ymd"] ;
  $taiin_ymd = @$_REQUEST["taiin_ymd"] ;
  if ($nyuin_ymd && $taiin_ymd)
  {
    $tmp_time = strtotime($c_sot_util->slash_yyyymmdd($taiin_ymd)) - strtotime($c_sot_util->slash_yyyymmdd($nyuin_ymd)) ;
    $date_diff = $tmp_time / (60 * 60 * 24) + 1 ;
  }
  $nyuin_undou_subtotal  = "" ;
  $nyuin_ninchi_subtotal = "" ;
  $nyuin_total           = "" ;
  $taiin_undou_subtotal  = "" ;
  $taiin_ninchi_subtotal = "" ;
  $taiin_total           = "" ;
  $nyuinOn = true ;
  foreach ($conts as $ymd => $ttls)
  {
    // 201203 入院履歴参照追加によりロジック追加 ▼
    if ($nyuin_ymd && $nyuinOn) {
        if ($taiin_ymd) {
            if ($ymd <= $taiin_ymd && $ymd >= $nyuin_ymd)
            {
                $nyuin_undou_subtotal  = $ttls[0] ;
                $nyuin_ninchi_subtotal = $ttls[1] ;
                $nyuin_total           = $ttls[2] ;
                $nyuinOn = false ;
            }
        }
        else {
            if ($ymd >= $nyuin_ymd)
            {
                $nyuin_undou_subtotal  = $ttls[0] ;
                $nyuin_ninchi_subtotal = $ttls[1] ;
                $nyuin_total           = $ttls[2] ;
                $nyuinOn = false ;
            }
        }
    }
/*
    if ($nyuin_ymd)
    {
      if ($nyuinOn && ($ymd >= $nyuin_ymd))
      {
        $nyuin_undou_subtotal  = $ttls[0] ;
        $nyuin_ninchi_subtotal = $ttls[1] ;
        $nyuin_total           = $ttls[2] ;
        $nyuinOn = false ;
      }
    }
*/
    // 201203 入院履歴参照追加によりロジック追加 ▲
    if ($taiin_ymd)
    {
      if ($ymd <= $taiin_ymd)
      {
        $taiin_undou_subtotal  = $ttls[0] ;
        $taiin_ninchi_subtotal = $ttls[1] ;
        $taiin_total           = $ttls[2] ;
      }
    }

  }

  echo '{' ;
  echo '"request":"request_fim_info",' ;
  echo '"nyuin_undou_subtotal":"' . $nyuin_undou_subtotal . '",' ;
  echo '"nyuin_ninchi_subtotal":"' . $nyuin_ninchi_subtotal . '",' ;
  echo '"nyuin_total":"' . $nyuin_total . '",' ;
  echo '"taiin_undou_subtotal":"' . $taiin_undou_subtotal . '",' ;
  echo '"taiin_ninchi_subtotal":"' . $taiin_ninchi_subtotal . '",' ;
  echo '"taiin_total":"' . $taiin_total . '",' ;
  echo '"date_diff":"' . $date_diff . '"' ;
  echo '}';

  die;
}

//******************************************************************************
// 最新のFIM評価表の情報の取得
// [request_latest_fim_info] [pt_id] が必要
// tmpl_RehaSougou_Kaifuku.phpで利用
//******************************************************************************
if (@$_REQUEST["request_latest_fim_info"]) {
    $fimItems = array(
        'undou' => array(                // 運動項目
            'selfcare' => array(         // セルフケア
                'meal',                  // 食事
                'seiyou',                // 整容
                'seisiki',               // 清拭
                'kouiupr',               // 更衣・上半身
                'kouilwr'),              // 更衣・下半身
            'haisetu' => array(          // 排泄コントロール
                'toilet',                // トイレ動作
                'nyou',                  // 排尿管理
                'ben'),                  // 排便管理
            'ijou' => array(             // 移乗
                'bedisu',                // ベッド・椅子・車椅子
                'toilet'),               // トイレ
            'idou' => array(             // 移動
                'yokusou',               // 浴槽・シャワー
                'kurumaisu',             // 歩行・車椅子
                'kaidan'),               // 階段
        ),
        'ninchi' => array(               // 認知項目
            'comm' => array(             // コミュニケーション
                'rikai',                 // 理解
                'hyoushutu'),            // 表出
            'shakai' => array(           // 社会的認知
                'kouryu',                // 社会的交流
                'kaiketu',               // 問題解決
                'kioku')                 // 記憶
        )
    );

    // 最新のFIM評価表情報を取得
    $sql =
    "select ".
    "  sum_xml.xml_file_name,".
    "  sum_xml.summary_id, ".
    "  sum_xml.update_timestamp, ".
    "  sum_xml.ptif_id, ".
    "  summary.summary_id, ".
    "  summary.ptif_id, ".
    "  summary.tmpl_id, ".
    "  tmplmst.tmpl_id, ".
    "  tmplmst.tmpl_name ".
    "from ".
    "  sum_xml left join summary on(sum_xml.summary_id = summary.summary_id and sum_xml.ptif_id = summary.ptif_id) ".
    "          left join tmplmst on(summary.tmpl_id = tmplmst.tmpl_id) ".
    "where ".
    "  sum_xml.ptif_id='". pg_escape_string(@$_REQUEST["pt_id"]) . "'" .
    "  and tmplmst.tmpl_name ='FIM評価表'" .
    "  order by sum_xml.update_timestamp desc limit 1";

    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        echo '';
        die;
    }

    // XMLファイル名を取得
    $file = pg_fetch_row($sel);
    if (!$file) {
        echo '';
        die;
    }

    $xml = new template_xml_class();
    $fim = $xml->get_xml_object_array_from_db($con, $fname, $file[0]);
    $worksheet = $fim->nodes['template']->nodes['FIMHyoukahyou'];

    // 応答データ作成
    echo '{' ;
    echo '"request":"request_latest_fim_info",';
    $fim_list = '';
    foreach ($fimItems as $key0 => $value0) {
        foreach ($value0 as $key1 => $value1) {
            foreach ($value1 as $item) {
                if ($fim_list != '') {
                    $fim_list .= ',';
                }
                $score = @$worksheet->nodes[$key0]->nodes[$key1]->nodes[$item]->value;
                $fim_list .= "\"fimh_{$key0}_{$key1}_{$item}\":\"{$score}\"";
            }
        }
        $subtotal = @$worksheet->nodes[$key0]->nodes['subtotal']->value;
        echo "\"fimh_{$key0}_subtotal\":\"{$subtotal}\",";
    }
    $total = @$worksheet->nodes[total]->value;
    echo "\"fimh_total\":\"{$total}\",";
    echo '"fim_list":{' . $fim_list . '}}';

    die;
}
?>
