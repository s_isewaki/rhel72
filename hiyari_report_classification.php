<?php
/*
 * 報告書の分類処理用のファイル
 * 
 * 分類登録を報告画面以外（メッセージ画面・報告書登録画面）からも利用することになったため、
 * hiyari_rpt_report_classification.phpから処理を分離して作成
 */

require_once('about_comedix.php');
require_once("get_values.php");

//==============================
//画面名
//==============================
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);

//==============================
// 分類ラベル登録
//==============================
if ($mode == "label"){
    $folder_info = get_all_classification_folder_info($con, $fname, $emp_id);
    foreach($folder_info as $info){
        $folder_id = $info["folder_id"];
        $is_selected = is_report_in_folder($con, $fname, $report_id, $folder_id);

        //ラベルの登録
        if (in_array($folder_id, $selected_folder)){
            if (!$is_selected){
                regist_report_classification($con, $fname, $folder_id, $report_id, "0");
            }
        }
        //ラベルの解除
        else{
            if ($is_selected){
                regist_report_classification($con, $fname, $folder_id, $report_id, "1");
            }
        }
    }
    echo ('ラベルを登録しました。');
    exit;
}

/**
 * 分類フォルダからレポートを削除・追加します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param string $folder_id フォルダID
 * @param string $report_id レポートID
 * @param string $add_del_div 追加削除区分
 */
function regist_report_classification($con,$fname,$folder_id,$report_id,$add_del_div)
{

    $sql  = "delete from inci_report_classification";
    $cond = "where folder_id = $folder_id and report_id = $report_id";
    $result = delete_from_table($con, $sql, $cond, $fname);
    if ($result == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    //追加
    $sql = "insert into inci_report_classification(folder_id,report_id,add_del_div) values(";
    $registry_data = array(
        $folder_id,
        $report_id,
        $add_del_div
    );
    $result = insert_into_table($con, $sql, $registry_data, $fname);
    if ($result == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

/**
 * ユーザーに対する全分類フォルダ情報を返します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param string $emp_id ユーザーID
 * @return array 全分類フォルダ情報
 */
function get_all_classification_folder_info($con,$fname,$emp_id)
{
    $sql  = " select t1.*,case when t2.cnt isnull then 0 else t2.cnt end as report_count from ";
    $sql .= " (select * from inci_classification_folder where common_flg or creator_emp_id = '$emp_id') t1";
    $sql .= " left join";
    $sql .= " (";
    $sql .= " select folder_id, count(report_id) as cnt from inci_report_classification group by folder_id";
    $sql .= " ) t2";
    $sql .= " on t1.folder_id = t2.folder_id";
    $sql .= " order by common_flg desc,folder_id";
    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    return  pg_fetch_all($sel);
}

//自動分類条件を取得
function get_folder_cond($con, $fname, $folder_id) {

    if ($folder_id == "" || $folder_id == "delete") {
        return array("", array());
    }

    $arr_auto_classification_folder = get_auto_classification_folder_info($con, $fname,$emp_id, $folder_id);
    $arr_folder_info_400 = get_folder_info_400($con, $fname, $folder_id);
    $arr_folder_info_1000 = get_folder_info_1000($con, $fname, $folder_id);
    $arr_folder_info_1000_detail = get_folder_info_1000_detail($con, $fname, $folder_id);

    $cond_folder = "";
    $join_field = array();
    $arr_cond_item = array();

    $arr_folder_info = $arr_auto_classification_folder[0];

    // 患者影響レベル・未入力チェック
    if($arr_folder_info["inci_level_regist_flg"] == "f") {
        $arr_cond_item[] .= " a_90_10.input_item is null ";
        $join_field[] = "90_10";
    }
    // 発生要因・未入力チェック
    if($arr_folder_info["happened_cause_regist_flg"] == "f") {
        $arr_cond_item[] .= " a_600_10.input_item is null ";
        $join_field[] = "600_10";
    }
    // インシデント背景要因・未入力チェック
    if($arr_folder_info["inci_background_regist_flg"] == "f") {
        $arr_cond_item[] .= " a_610_20.input_item is null ";
        $join_field[] = "610_20";
    }
    // インシデント改善策・未入力チェック
    if($arr_folder_info["improve_plan_regist_flg"] == "f") {
        $arr_cond_item[] .= " a_620_30.input_item is null ";
        $join_field[] = "620_30";
    }
    // 効果の確認・未入力チェック
    if($arr_folder_info["effect_confirm_regist_flg"] == "f") {
        $arr_cond_item[] .= " a_625_35.input_item is null ";
        $join_field[] = "625_35";
    }
    // 警鐘事例・入力チェック
    if($arr_folder_info["warning_case_regist_flg"] == "t") {
        $arr_cond_item[] .= " a_510_20.input_item is not null ";
        $join_field[] = "510_20";
    }
    // 事例分析検討対象・対象チェック
    if($arr_folder_info["analysis_target_flg"] == "t") {
        $arr_cond_item[] .= " a_650_50.input_item = '1' ";
        $join_field[] = "650_50";
    }
    // 表題
    if($arr_folder_info["title_keyword_condition"] != "") {
        $title_keyword = mb_ereg_replace("　"," ",$arr_folder_info["title_keyword_condition"]); //全角スペース除去
        $title_keyword = trim($title_keyword); //前後スペース除去
        $title_keyword_list = mb_split(" ", $title_keyword); //スペース区切り

        $wk_str = "";
        //マルチワードについては常にOR結合
        for ($i=0; $i<count($title_keyword_list); $i++) {
            if ($i > 0) {
                if($arr_folder_info["title_and_or_div"] == 1) {
                    $wk_str .= " and ";
                } else {
                    $wk_str .= " or ";
                }
            }
            $wk_str .= " a.report_title like '%{$title_keyword_list[$i]}%' ";
        }
        $arr_cond_item[] .= " ($wk_str)";
    }

    // インシデントの内容
    if($arr_folder_info["incident_contents_condition"] != "") {
        $incident_contents = mb_ereg_replace("　"," ",$arr_folder_info["incident_contents_condition"]); //全角スペース除去
        $incident_contents = trim($incident_contents); //前後スペース除去
        $incident_contents_list = mb_split(" ", $incident_contents); //スペース区切り

        $wk_str = "";
        //マルチワードについては常にOR結合
        for ($i=0; $i<count($incident_contents_list); $i++) {
            if ($i > 0) {
                if($arr_folder_info["incident_and_or_div"] == 1) {
                    $wk_str .= " and ";
                } else {
                    $wk_str .= " or ";
                }
            }
            $wk_str .= " a_520_30.input_item like '%{$incident_contents_list[$i]}%' ";
        }
        $arr_cond_item[] .= " ($wk_str)";
        $join_field[] = "520_30";
    }

    //報告部署
    if($arr_folder_info["emp_class_condition"] != "")
    {
        $wk_str = " a.registrant_class = ".$arr_folder_info["emp_class_condition"]." ";
        if($arr_folder_info["emp_attribute_condition"] != "") {
            $wk_str .= " and a.registrant_attribute = ".$arr_folder_info["emp_attribute_condition"]." ";
        }
        if($arr_folder_info["emp_dept_condition"] != "") {
            $wk_str .= " and a.registrant_dept = ".$arr_folder_info["emp_dept_condition"]." ";
        }
        if($arr_folder_info["emp_room_condition"] != "") {
            $wk_str .= " and a.registrant_room = ".$arr_folder_info["emp_room_condition"]." ";
        }
        $arr_cond_item[] .= " ($wk_str)";
    }

    //報告者氏名
    if($arr_folder_info["emp_name_condition"] != "")
    {
        //トレイ検索と合わせるため、マルチ検索にはしない。
        $tmp_folder_conditions = mb_ereg_replace("　"," ",$arr_folder_info["emp_name_condition"]); //全角スペース除去
        $tmp_folder_conditions = trim($tmp_folder_conditions); //前後スペース除去
        //匿名
        $anonymous_name = '匿名';   //h(get_anonymous_name($con,$fname));
        if ($anonymous_name == $tmp_folder_conditions) {
            $wk_str = " a.anonymous_report_flg = 't' ";
        } else {
            $wk_str = " (e.emp_lt_nm || e.emp_ft_nm) like '%{$tmp_folder_conditions}%' or (e.emp_kn_lt_nm || e.emp_kn_ft_nm) like '%{$tmp_folder_conditions}%' ";
        }
        $arr_cond_item[] .= " ($wk_str)";
    }
    //報告者職種
    if($arr_folder_info["emp_job_condition"] != "")
    {
        $arr_cond_item[] .= " (a.registrant_job = ".$arr_folder_info["emp_job_condition"].")";
    }
    //患者ID
    if($arr_folder_info["patient_id_condition"] != "")
    {
        $arr_cond_item[] .= " a_210_10.input_item = '".$arr_folder_info["patient_id_condition"]."' ";
        $join_field[] = "210_10";
    }
    //患者氏名
    if($arr_folder_info["patient_name_condition"] != "")
    {
        //トレイ検索と合わせるため、マルチ検索にはしない。
        $tmp_folder_conditions = mb_ereg_replace("　"," ",$arr_folder_info["patient_name_condition"]); //全角スペース除去
        $tmp_folder_conditions = trim($tmp_folder_conditions); //前後スペース除去
        $arr_cond_item[] .= " a_210_20.input_item like '%{$tmp_folder_conditions}%' ";
        $join_field[] = "210_20";
    }
    //患者年齢
    if($arr_folder_info["patient_year_from_condition"] != "" || $arr_folder_info["patient_year_to_condition"] != "")
    {
        $tmp_f = $arr_folder_info["patient_year_from_condition"];
        $tmp_t = $arr_folder_info["patient_year_to_condition"];
        //不明
        if($tmp_f == "900")
        {
            $wk_str = " a_210_40.input_item = '900' ";
        }
        //from - to
        else if($tmp_f != "" && $tmp_t != "")
        {
            $wk_str = " a_210_40.input_item >= '$tmp_f' and a_210_40.input_item <= '$tmp_t' ";
        }
        //from - ""
        else if($tmp_f != "" && $tmp_t == "")
        {
            $wk_str = " a_210_40.input_item >= '$tmp_f' and a_210_40.input_item < '900' ";
        }
        //"" - to
        else if($tmp_f == "" && $tmp_t != "")
        {
            $wk_str = " a_210_40.input_item < '$tmp_t' ";
        }

        $arr_cond_item[] .= " ($wk_str)";
        $join_field[] = "210_40";
    }
    //評価予定日
    if($arr_folder_info["evaluation_date_condition"] != "")
    {
        if($arr_folder_info["evaluation_date_condition"] == "today")
        {
            $date_min = date("Y/m/d");
            $date_max = $date_min;
        }
        elseif($arr_folder_info["evaluation_date_condition"] == "tomorrow")
        {
            $date_min = date("Y/m/d", strtotime("+1 day"));
            $date_max = $date_min;
        }
        elseif($arr_folder_info["evaluation_date_condition"] == "1week")
        {
            $date_min = date("Y/m/d");
            $date_max = date("Y/m/d", strtotime("+7 day"));
        }
        elseif($arr_folder_info["evaluation_date_condition"] == "2week")
        {
            $date_min = date("Y/m/d");
            $date_max = date("Y/m/d", strtotime("+14 day"));
        }
        $arr_cond_item[] = "(a_620_33.input_item >= '$date_min' and a_620_33.input_item <= '$date_max')";
        $join_field[] = "620_33";
    }
    //患者影響レベル
    if($arr_folder_info["use_level_list"] != "")
    {
        $arr_use_level = split(",", $arr_folder_info["use_level_list"] );
        $wk_str = "";
        for ($i=0; $i<count($arr_use_level); $i++) {
            if ($i > 0) {
                $wk_str .= " or ";
            }
            $wk_str .= " a_90_10.input_item = '$arr_use_level[$i]' ";
        }

        $arr_cond_item[] = "($wk_str)";
        if (!in_array("90_10", $join_field)) {
            $join_field[] = "90_10";
        }
    }
    //ヒヤリハット分類
    if($arr_folder_info["use_hiyari_summary_list"] != "")
    {
        $arr_use_hiyari_summary = split(",", $arr_folder_info["use_hiyari_summary_list"] );
        $wk_str = "";
        for ($i=0; $i<count($arr_use_hiyari_summary); $i++) {
            if ($i > 0) {
                $wk_str .= " or ";
            }
            $wk_str .= " a_125_85.input_item = '$arr_use_hiyari_summary[$i]' ";
        }

        $arr_cond_item[] = "($wk_str)";
        $join_field[] = "125_85";
    }
    //インシデントの概要
    if($arr_folder_info["use_incident_summary_list"] != "")
    {
        //item_400 [インシデントの概要 - 1][0場面、1内容]
        // 400_10,400_20
        // 410_10,410_30
        // 420_10,420_20...
        for ($i=0; $i<2; $i++) {
            for ($j=0; $j<10; $j++) {
                if ($i == 0) {
                    $grp_item = "4".$j."0_10";
                } else {
                    if ($j == 1) {
                        $grp_item = "4".$j."0_30";
                    } else {
                        $grp_item = "4".$j."0_20";
                    }
                }
                $arr_item_400[$j][$i] = $grp_item;
            }
        }
        $folder_id = $arr_folder_info["folder_id"];
        $use_incident_summary_list = $arr_folder_info["use_incident_summary_list"];
        $use_incident_summary_arr = split(",", $use_incident_summary_list);

        $incident_str = "";
        foreach($use_incident_summary_arr as $use_incident_summary)
        {
            $incident_str = "a_120_80.input_item = '$use_incident_summary'";
            $index_400 = $use_incident_summary - 1;

            $code_str = "";
            //発生場面・内容
            if($arr_folder_info_400[$folder_id][$index_400] != "")
            {

                //0:発生場面、1:内容に対して
                for($i = 0; $i < 2; $i++)
                {
                    //制限がある場合
                    if($arr_folder_info_400[$folder_id][$index_400][$i] != "")
                    {
                        //各制限コードに対して
                        foreach($arr_folder_info_400[$folder_id][$index_400][$i] as $use_400_code)
                        {
                            if ($code_str != "") {
                                $code_str .= " or ";
                            }
                            //arr_item?
                            $code_str .= " a_{$arr_item_400[$index_400][$i]}.input_item = '$use_400_code' ";

                            if (!in_array($arr_item_400[$index_400][$i], $join_field)) {
                                $join_field[] = $arr_item_400[$index_400][$i];
                            }
                        }
                    }
                }

            }
            if ($code_str != "") {
                $incident_str .= " and ($code_str) ";
            }
            $arr_incident_str[] = "($incident_str)";
        }
        $wk_incident_str = join(" or ", $arr_incident_str);
        $arr_cond_item[] = "($wk_incident_str)";

        if (!in_array("120_80", $join_field)) {
            $join_field[] = "120_80";
        }
    }
    //概要･場面･内容
    if($arr_folder_info["use_2010_summary_list"] != "")
    {
        //item_400 [インシデントの概要 - 1][0場面、1内容]
        // 400_10,400_20
        // 410_10,410_30
        // 420_10,420_20...
        for ($i=0; $i<3; $i++) {
            for ($j=0; $j<8; $j++) {
                if ($i == 0) {
                    $grp_item = "4".$j."0_10";
                } else {
                    if ($j == 1) {
                        $grp_item = "4".$j."0_30";
                    } else {
                        $grp_item = "4".$j."0_20";
                    }
                }
                $arr_item_400[$j][$i] = $grp_item;
            }
        }
        $folder_id = $arr_folder_info["folder_id"];
        $use_2010_summary_list = $arr_folder_info["use_2010_summary_list"];
        $use_2010_summary_arr = split(",", $use_2010_summary_list);

        $incident_str = "";
        foreach($use_2010_summary_arr as $use_2010_summary)
        {
            $incident_str = "a_900_10.input_item = '$use_2010_summary'";
            $index_1000 = $use_2010_summary - 1;

            $code_str = "";
            $detail_code_str = "";
            //発生場面・内容
            if($arr_folder_info_1000[$folder_id][$index_1000] != "")
            {

                //0:種類、1:場面、2:内容に対して
                for($i = 0; $i < 3; $i++)
                {
                    //制限がある場合
                    if($arr_folder_info_1000[$folder_id][$index_1000][$i] != "")
                    {
                        //各制限コードに対して
                        foreach($arr_folder_info_1000[$folder_id][$index_1000][$i] as $use_1000_code)
                        {
                            if ($code_str != "") {
                                $code_str .= " or ";
                            }
                            //arr_item?
                            //$code_str .= " a_{$arr_item_400[$index_400][$i]}.input_item = '$use_1000_code' ";
                            if($i == 0) {
                                $code_str .= " a_910_10.input_item = '$use_1000_code' ";
                                $join_field[] = "910_10";

                                foreach($arr_folder_info_1000_detail[$folder_id][$use_1000_code][$i] as $use_1000_detail_code) {
                                    if($detail_code_str != "") $detail_code_str .= " or ";

                                    $detail_code_str .= " a_920_10.input_item = '$use_1000_detail_code' ";
                                    $join_field[] = "920_10";
                                }
                            } else if($i == 1) {
                                $code_str .= " a_940_10.input_item = '$use_1000_code' ";
                                $join_field[] = "940_10";

                                foreach($arr_folder_info_1000_detail[$folder_id][$use_1000_code][$i] as $use_1000_detail_code) {
                                    if($detail_code_str != "") $detail_code_str .= " or ";

                                    $detail_code_str .= " a_950_10.input_item = '$use_1000_detail_code' ";
                                    $join_field[] = "950_10";
                                }
                            } else {
                                $code_str .= " a_970_10.input_item = '$use_1000_code' ";
                                $join_field[] = "970_10";

                                foreach($arr_folder_info_1000_detail[$folder_id][$use_1000_code][$i] as $use_1000_detail_code) {
                                    if($detail_code_str != "") $detail_code_str .= " or ";

                                    $detail_code_str .= " a_980_10.input_item = '$use_1000_detail_code' ";
                                    $join_field[] = "980_10";
                                }
                            }
                        }
                    }
                }
            }
            if ($code_str != "") {
                $incident_str .= " and ($code_str) ";
            }
            if($detail_code_str != "") $incident_str .= " and ($detail_code_str) ";
            $arr_incident_str[] = "($incident_str)";
        }
        $wk_incident_str = join(" or ", $arr_incident_str);
        $arr_cond_item[] = "($wk_incident_str)";

        if (!in_array("900_10", $join_field)) {
            $join_field[] = "900_10";
        }
    }
    $and_or = ($arr_folder_info["and_or_div"] == "0") ? " and " : " or ";
    $cond_tmp = "";
    //分類
    if (count($arr_cond_item) > 0) {
        $cond_tmp = " (".join($and_or, $arr_cond_item).") ";
    }
    //手動追加分
    if ($folder_id != "" && $folder_id != "delete") {
        if ($cond_tmp != "") {
            $cond_tmp .= " or ";
        }
        $cond_tmp .= " exists (select f.report_id from inci_report_classification f where f.report_id = a.report_id and f.folder_id = $folder_id and f.add_del_div = 0) ";
    }

    $cond_folder = " and ($cond_tmp)";

    //手動削除分
    if ($folder_id != "" && $folder_id != "delete") {
        $cond_folder .= "and (not exists (select f.report_id from inci_report_classification f where f.report_id = a.report_id and f.folder_id = $folder_id and f.add_del_div = 1)) ";
    }

    return array($cond_folder, $join_field);
}

/**
 * 自動分類の条件を設定したフォルダ情報を取得します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param string $emp_id ユーザーID
 * @param string $folder_id フォルダID 空白：すべて 空白以外：指定フォルダ
 * @return array 自動分類の条件を設定したフォルダ情報
 */
function get_auto_classification_folder_info($con, $fname, $emp_id, $folder_id)
{

    if ($folder_id == "") {
        $cond_folder = "common_flg or creator_emp_id = '$emp_id'";
    } else {
        $cond_folder = "folder_id = $folder_id";
    }

    $sql  = "select * from inci_classification_folder";
    $cond = "where ($cond_folder) ";
    $cond .= "and ( ";
    $cond .= "false ";
    $cond .= "or not inci_level_regist_flg ";
    $cond .= "or not happened_cause_regist_flg ";
    $cond .= "or not inci_background_regist_flg ";
    $cond .= "or not improve_plan_regist_flg ";
    $cond .= "or not effect_confirm_regist_flg ";
    $cond .= "or analysis_target_flg ";
    $cond .= "or warning_case_regist_flg ";
//  $cond .= "or inci_summary_condition is not null ";
    $cond .= "or title_keyword_condition is not null ";
    $cond .= "or incident_contents_condition is not null ";
    $cond .= "or emp_class_condition is not null ";
    $cond .= "or emp_name_condition is not null ";
    $cond .= "or emp_job_condition is not null ";
    $cond .= "or patient_id_condition is not null ";
    $cond .= "or patient_year_from_condition is not null ";
    $cond .= "or patient_year_to_condition is not null ";
    $cond .= "or patient_name_condition is not null ";
    $cond .= "or evaluation_date_condition is not null ";
    $cond .= "or use_level_list is not null ";
    $cond .= "or use_hiyari_summary_list is not null ";
    $cond .= "or use_incident_summary_list is not null ";
    $cond .= "or use_2010_summary_list is not null ";
    $cond .= ") ";
    $cond .= "order by folder_id";

    $sel  = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0){
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    return pg_fetch_all($sel);
}

function get_folder_info_400($con, $fname, $folder_id)
{
    $sql  = "select * from inci_classification_folder_400_condition";
    if ($folder_id != "") {
        $cond = " where folder_id = $folder_id";
    } else {
        $cond = "";
    }
    $sel  = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0){
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $info = null;
    $db_data = pg_fetch_all($sel);
    foreach($db_data as $db_record)
    {
        $info[ $db_record["folder_id"] ][ $db_record["index_400"] ][ $db_record["target_item"] ] = split(",",$db_record["use_list"]);
    }

    return $info;
}

function get_folder_info_1000($con, $fname, $folder_id)
{
    $sql  = "select * from inci_classification_folder_1000_condition";
    if ($folder_id != "") {
        $cond = " where folder_id = $folder_id";
    } else {
        $cond = "";
    }
    $sel  = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0){
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $info = null;
    $db_data = pg_fetch_all($sel);
    foreach($db_data as $db_record)
    {
        $info[ $db_record["folder_id"] ][ $db_record["index_1000"] ][ $db_record["code"] ] = split(",",$db_record["use_list"]);
    }

    return $info;
}

function get_folder_info_1000_detail($con, $fname, $folder_id)
{
    $sql  = "select * from inci_classification_folder_1000_detail_condition";
    if ($folder_id != "") {
        $cond = " where folder_id = $folder_id";
    } else {
        $cond = "";
    }
    $sel  = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0){
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $info = null;
    $db_data = pg_fetch_all($sel);
    foreach($db_data as $db_record)
    {
        $info[ $db_record["folder_id"] ][ $db_record["use_index"] ][ $db_record["code"] ] = split(",",$db_record["use_detail_list"]);
    }

    return $info;
}

function is_report_in_folder($con, $fname, $report_id, $folder_id)
{
    $sql  = "select count(*) as cnt";
    $sql .= " from (select * from inci_report where report_id=$report_id) a ";
    $sql .= " left join empmst e on e.emp_id = a.registrant_id ";
    $sql .= " left join inci_report_link_view l on a.report_id = l.report_id ";

    //自動分類条件
    list ($cond_folder, $arr_join_field) = get_folder_cond($con, $fname, $folder_id);
    $arr_join_field = array_unique($arr_join_field);

    $join_str = "";
    foreach ($arr_join_field as $wk_i => $wk_join_field) {
        list ($gi_g, $gi_i) = split("_", $wk_join_field);
        $join_str .= "left join inci_easyinput_data a_{$gi_g}_{$gi_i} on a.eid_id = a_{$gi_g}_{$gi_i}.eid_id and a_{$gi_g}_{$gi_i}.grp_code = {$gi_g} and a_{$gi_g}_{$gi_i}.easy_item_code = {$gi_i} ";
    }
    $sql .= $join_str;

    $cond  = " where not a.shitagaki_flg and not a.del_flag and not a.kill_flg " . $cond_folder;

    $sel = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0){
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    return (pg_fetch_result($sel,0,"cnt") > 0);
}