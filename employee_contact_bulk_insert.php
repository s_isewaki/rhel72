<?
ini_set("max_execution_time", 0);

require("about_session.php");
require("about_authority.php");
require("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ファイルが正常にアップロードされたかどうかチェック
$uploaded = false;
if (array_key_exists("csvfile", $_FILES)) {
	if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
		$uploaded = true;
	}
}
if (!$uploaded) {
	echo("<script type=\"text/javascript\">alert('ファイルのアップロードに失敗しました。');</script>\n");
	echo("<script type=\"text/javascript\">location.href = 'employee_contact_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
	exit;
}

// 文字コードの設定
switch ($encoding) {
case "1":
	$file_encoding = "SJIS";
	break;
case "2":
	$file_encoding = "EUC-JP";
	break;
case "3":
	$file_encoding = "UTF-8";
	break;
default:
	exit;
}


// EUC-JPで扱えない文字列は「・」に変換するよう設定
mb_substitute_character(0x30FB);

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");


// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);


// 拡張empmst項目1から5の表示可能判定と編集可能判定を取得
// ・利用判定：disp_empがall_okか、そうでないかを判定すればよい。
$sql =
" select field_id from spfm_field_struct_auth".
" where table_id = 'spft_empmst' and field_id like 'empmst_ext%'".
" and target_place = 'all' and disp_emp = 'all_ok'".
" order by order_no";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$fieldStructAuthRows = array();
while ($row = pg_fetch_assoc($sel)) {
	$fieldStructAuthRows[]= $row["field_id"];
}

// カラム数
$column_count = 6 + count($fieldStructAuthRows);

// CSVデータを配列に格納
$employees = array();
$employee_no = 1;
$lines = file($_FILES["csvfile"]["tmp_name"]);
foreach ($lines as $line) {

	// 文字コードを変換
	$line = trim(mb_convert_encoding($line, "EUC-JP", $file_encoding));

	// EOFを削除
	$line = str_replace(chr(0x1A), "", $line);

	// 空行は無視
	if ($line == "") {
		continue;
	}

	// カラム数チェック
	$employee_data = split(",", $line);
	if (count($employee_data) != $column_count) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('カラム数が不正です。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_contact_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	$employees[$employee_no] = $employee_data;
	$employee_no++;
}

// 職員データを登録
$extra_ids = array();
foreach ($employees as $employee_no => $employee_data) {
	$extra_id = insert_employee_data($con, $employee_no, $employee_data, $session, $fname, $fieldStructAuthRows);
	if ($extra_id != "") {
		$extra_ids[] = $extra_id;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 存在しない職員IDが指定されていたらalert
if (count($extra_ids) > 0) {
	$tmp_extra_ids = "・" . implode("\\n・", $extra_ids);
	echo("<script type=\"text/javascript\">alert('下記の職員は存在しないため、無視されました。\\n\\n$tmp_extra_ids');</script>\n");
}

// 成功画面を表示
echo("<script type=\"text/javascript\">location.href = 'employee_contact_bulk_register.php?session=$session&result=t&encoding=$encoding';</script>");

// 職員データを登録
function insert_employee_data($con, $employee_no, $employee_data, $session, $fname, &$fieldStructAuthRows) {
	global $_label_by_profile;
	global $profile_type;

	$i = 0;
	$personal_id = trim($employee_data[$i++]);
	$emp_nm = trim($employee_data[$i++]);
	$mobile = trim($employee_data[$i++]);
	$extension = trim($employee_data[$i++]);
	$phs = trim($employee_data[$i++]);
	$email = trim($employee_data[$i++]);

	$spftSet = array();
	$spftSetValue = array();
	foreach ($fieldStructAuthRows as $field_id) {
		$spftSet[]= $field_id;
		$spftSetValue[]= trim($employee_data[$i++]);
	}

	// 当該職員が存在するかチェック
	$sql = "select emp_id from empmst";
	$cond = "where emp_personal_id = '$personal_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$emp_id = trim(pg_fetch_result($sel, 0, 0));
	if ($emp_id == "") {
		return $personal_id;
	}

	// 入力チェック
	if ($mobile != "") {
		if (preg_match("/^(\d{1,6})-(\d{1,6})-(\d{1,6})$/", $mobile, $matches)) {
			$mobile1 = $matches[1];
			$mobile2 = $matches[2];
			$mobile3 = $matches[3];
		} else {
			$mobile = preg_replace("/\D/", "", $mobile);
			if (strlen($mobile) > 12) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\">alert('携帯端末・外線が長すぎます。レコード番号：{$employee_no}');</script>\n");
				echo("<script type=\"text/javascript\">location.href = 'employee_contact_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
				exit;
			}
			$mobile1 = substr($mobile, 0, 4);
			$mobile2 = substr($mobile, 4, 4);
			$mobile3 = substr($mobile, 8);
		}
	}
	if (strlen($extension) > 10) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('内線番号が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_contact_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($phs) > 6) {
		pg_query($con, "rollback");
		pg_close($con);
		// 院内PHS/所内PHS
		$phs_name = $_label_by_profile["PHS"][$profile_type];
		echo("<script type=\"text/javascript\">alert('{$phs_name}が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_contact_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($email) > 120) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('E-Mailが長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_contact_bulk_register.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	$sql = "update empmst set";
	$set = array("emp_mobile1", "emp_mobile2", "emp_mobile3", "emp_ext", "emp_phs", "emp_email2");
	$setvalue = array($mobile1, $mobile2, $mobile3, $extension, $phs, $email);
	$cond = "where emp_personal_id = '$personal_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 拡張職員マスタが存在するかチェック
	$sel = select_from_table($con, "select count(*) from spft_empmst where emp_id = '$emp_id'", "", $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) == 0) {
		$sql = "insert into spft_empmst (emp_id) values (";
		echo $sql;
		$ins = insert_into_table($con, $sql, array($emp_id), $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	if (count($spftSet)) {
		$sql = "update spft_empmst set";
		$cond = " where emp_id = '$emp_id'";
		$upd = update_set_table($con, $sql, $spftSet, $spftSetValue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}
?>
