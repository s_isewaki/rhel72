<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("menu_common.ini");
require("show_date_navigation_month.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 55, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 院内行事登録権限の取得
$event_setting_auth = check_authority($session, 12, $fname);

// デフォルト日付はシステム日付
if ($date == "") {$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));}
$last_month = get_last_month($date);
$next_month = get_next_month($date);

// データベースに接続
$con = connect2db($fname);

// 施設一覧を取得
$sql = "select fcl_id, name from timegdfcl";
$cond = "order by fcl_id";
$sel_fcl = select_from_table($con, $sql, $cond, $fname);
if ($sel_fcl == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu2 = pg_fetch_result($sel, 0, "menu2");
$menu2_1 = pg_fetch_result($sel, 0, "menu2_1");
?>
<title>イントラネット | <? echo($menu2); ?> | <? echo($menu2_1); ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">
function changeDate(dt) {
	location.href = 'intra_info_event.php?session=<? echo($session); ?>&date=' + dt;
}

function popupEventDetail(fcl_name, evt_date, evt_time, evt_name, evt_content, evt_contact, e) {
	popupDetailBlue(
		new Array(
			'施設', fcl_name,
			'日付', evt_date,
			'時刻', evt_time,
			'行事名', evt_name,
			'詳細', evt_content,
			'連絡先', evt_contact
		), 400, 80, e
	);
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_info.php?session=<? echo($session); ?>"><b><? echo($menu2); ?></b></a> &gt; <a href="intra_info_event.php?session=<? echo($session); ?>"><b><? echo($menu2_1); ?></b></a></font></td>
<? if ($event_setting_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="time_guide_list.php?session=<? echo($session); ?>&event=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="105" align="center" bgcolor="#bdd1e7"><a href="intra_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メインメニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($menu2)); ?>" align="center" bgcolor="#bdd1e7"><a href="intra_info.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($menu2); ?></font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($menu2_1)); ?>" align="center" bgcolor="#5279a5"><a href="intra_info_event.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><? echo($menu2_1); ?></b></font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="intra_info_event.php?session=<? echo($session); ?>&date=<? echo($last_month); ?>">&lt;前月</a>&nbsp;<select onchange="changeDate(this.value);"><? show_date_options_m($date); ?></select>&nbsp;<a href="intra_info_event.php?session=<? echo($session); ?>&date=<? echo($next_month); ?>">翌月&gt;</a></font></td>
</tr>
</table>
<?
$fcls = array();
$fcls["-"] = "全体";
while ($row = pg_fetch_array($sel_fcl)) {
	$fcls[$row["fcl_id"]] = $row["name"];
}
$fcls_chunk = array_chunk($fcls, 5, true);

$year = date("Y", $date);
$month = intval(date("m", $date));

foreach ($fcls_chunk as $fcls) {
	echo("<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
	echo("<tr height=\"22\" valign=\"top\" bgcolor=\"#f6f9ff\">\n");
	echo("<td width=\"150\"></td>\n");
	foreach ($fcls as $fcl_name) {
		echo("<td width=\"120\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$fcl_name</font></td>\n");
	}
	echo("</tr>\n");

	echo("<tr height=\"22\" valign=\"top\">\n");
	echo("<td height=\"80\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">行事</font></td>\n");
	foreach ($fcls as $fcl_id => $fcl_name) {
		$sql = "select event_id, event_date, event_time, event_name, event_dur, event_content, time_flg, fcl_detail, contact from timegd";
		$cond = "where extract(year from event_date) = '$year' and extract(month from event_date) = '$month'";
		$cond .= ($fcl_id == "-") ? " and fcl_id is null" : " and fcl_id = $fcl_id";
		$cond .= " order by event_date, event_time";
		$sel_event = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		echo("<td>");
		if (pg_num_rows($sel_event) > 0) {
			echo("<table width=\"120\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
			for ($i = 0, $j = pg_num_rows($sel_event); $i < $j; $i++) {
				list($event_year, $event_month, $event_day) = explode("-", pg_fetch_result($sel_event, $i, "event_date"));
				$event_wd = date("w", mktime(0, 0, 0, $event_month, $event_day, $event_year));
				switch ($event_wd) {
				case 0:
					$event_wd = "日";
					break;
				case 1:
					$event_wd = "月";
					break;
				case 2:
					$event_wd = "火";
					break;
				case 3:
					$event_wd = "水";
					break;
				case 4:
					$event_wd = "木";
					break;
				case 5:
					$event_wd = "金";
					break;
				case 6:
					$event_wd = "土";
					break;
				}

				list($start_hour, $start_min) = explode(":", pg_fetch_result($sel_event, $i, "event_time"));
				list($end_hour, $end_min) = explode(":", pg_fetch_result($sel_event, $i, "event_dur"));

				if (pg_fetch_result($sel_event, $i, "time_flg") == "t") {
					$start_time = "$start_hour:{$start_min}〜";
					$time = "$start_hour:$start_min 〜 $end_hour:$end_min";
				} else {
					$start_time = "";
					$time = "";
				}

				$event_name = pg_fetch_result($sel_event, $i, "event_name");
				$event_content = preg_replace("/\r?\n/", "<br>", pg_fetch_result($sel_event, $i, "event_content"));

				$tmp_fcl_detail = pg_fetch_result($sel_event, $i, "fcl_detail");
				$tmp_fcl_name = ($tmp_fcl_detail != "") ? "$fcl_name $tmp_fcl_detail" : $fcl_name;
				$tmp_contact = preg_replace("/\r?\n/", "<br>", pg_fetch_result($sel_event, $i, "contact"));

				if ($i < $j - 1) {
					$style = " style=\"padding-bottom:2px;border-bottom:#5279a5 dotted 1px;\"";
				} else {
					$style = " style=\"padding-bottom:2px;\"";
				}

				echo("<tr>\n");
				echo("<td$style><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><span onmousemove=\"popupEventDetail('$tmp_fcl_name', '$event_year/$event_month/$event_day', '$time', '$event_name', '$event_content', '$tmp_contact', event);\" onmouseout=\"closeDetail();\">{$event_day}日($event_wd) $start_time<br><font color=\"blue\">$event_name</font></span></font></td>\n");
				echo("</tr>\n");
				echo("<tr>\n");
				echo("<td><img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"2\"></td>\n");
				echo("</tr>\n");
			}
			echo("</table>\n");
		} else {
			echo("&nbsp;");
		}
		echo("</td>\n");
	}
	echo("</tr>\n");

	echo("</table>\n");
	echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"2\"><br>\n");
}
?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
