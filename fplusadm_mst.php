<?
$fname = $PHP_SELF;
ob_start();
require_once("about_comedix.php");
require_once("show_fplusadm_lib_list.ini");
require_once("fplusadm_common.ini");
require_once("summary_tmplitemrireki.ini"); // 兼用
ob_end_clean();

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$checkauth = check_authority($session, 79, $fname);
$con = @connect2db($fname);
if(!$session || !$checkauth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}








//==============================
// マスタ一覧を取得
//==============================
$sql = "select mst_cd, mst_name from tmplitemmst";
$cond = "order by mst_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$mst_list = pg_fetch_all($sel);

//マスタ管理コードが指定されていない場合は先頭マスタを指定
if(@$mst_cd == "" && $mst_list != "")
{
	$mst_cd = $mst_list[0]["mst_cd"];
}

//==============================
//ポストバック時
//==============================
if(@$postback == "true")
{
	if($postback_mode == "item_delete")
	{
		// トランザクションの開始
		pg_query($con, "begin");

		$delete_target_list = "";
		foreach($item_select as $item_cd)
		{
			if($delete_target_list != "")
			{
				$delete_target_list .= ",";
			}
			$delete_target_list .= "'".pg_escape_string($item_cd)."'";
		}
		$sql = "delete from tmplitem where mst_cd = '$mst_cd' and item_cd in($delete_target_list)";
		$del = delete_from_table($con, $sql, "", $fname);
		if ($del == 0)
		{
			pg_query($con,"rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//履歴更新
		entry_tmplitemrireki($con,$fname,$mst_cd);

		// トランザクションのコミット
		pg_query($con, "commit");
	}
	elseif($postback_mode == "item_disp_update")
	{
		// トランザクションの開始
		pg_query($con, "begin");

		foreach($list_item_cd as $item_cd)
		{
			$param_name = "disp_order_".$item_cd;
			$disp_order = $$param_name;
			$param_name = "disp_flg_".$item_cd;
			$disp_flg = $$param_name;

			$sql = "update tmplitem set";
			$set = array("disp_flg", "disp_order");
			$setvalue = array(
				$disp_flg,
				intval($disp_order)
				);
			$cond = "where mst_cd = '$mst_cd' and item_cd = '$item_cd'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

		}

		//履歴更新
		entry_tmplitemrireki($con,$fname,$mst_cd);

		// トランザクションのコミット
		pg_query($con, "commit");
	}
}




//==============================
//項目一覧を取得
//==============================
$item_list = "";
if($mst_cd != "")
{
	$sql = "select * from tmplitem";
	$cond = "where mst_cd = '$mst_cd' order by disp_order";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$item_list = pg_fetch_all($sel);
}





?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 管理画面 | マスタ管理</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function registMst() {

	window.open('fplusadm_mst_registration.php?session=<?=$session?>', 'newwin', 'width=760,height=400,scrollbars=yes');
}

function deleteMst() {
	var checked_ids = getCheckedIds(document.delmst.elements['mst_cds[]']);
	if (checked_ids.length == 0) {
		alert('マスタが選択されていません。');
		return;
	}

	if (!confirm('選択されたマスタを削除します。よろしいですか？')) {
		return;
	}

	document.delmst.submit();
}

function updateMst() {
	document.updmst.submit();
}

function getCheckedIds(boxes) {
	var checked_ids = new Array();
	if (boxes) {
		if (!boxes.length) {
			if (boxes.checked) {
				checked_ids.push(boxes.value);
			}
		} else {
			for (var i = 0, j = boxes.length; i < j; i++) {
				if (boxes[i].checked) {
					checked_ids.push(boxes[i].value);
				}
			}
		}
	}
	return checked_ids;
}



function registItem()
{
	window.open('fplusadm_mst_item_input.php?session=<?=$session?>&mode=insert&mst_cd=<?=$mst_cd?>', 'newwin', 'width=760,height=400,scrollbars=yes');
}
function updateItem(item_cd)
{
	window.open('fplusadm_mst_item_input.php?session=<?=$session?>&mode=update&mst_cd=<?=$mst_cd?>&item_cd='+item_cd , 'newwin', 'width=760,height=400,scrollbars=yes');
}
function deleteItem()
{
	var is_selected = false;
	var objs = document.getElementsByName("item_select[]");
	for(var i=0;i<objs.length;i++)
	{
		if(objs[i].checked)
		{
			is_selected = true;
			break;
		}
	}

	if(!is_selected)
	{
		alert("選択されていません。");
		return;
	}


	if (!confirm('選択された項目を削除します。よろしいですか？')) {
		return;
	}


	document.itemform.postback_mode.value = "item_delete";
	document.itemform.submit();
}
function updateItemDisp()
{
	var objs = document.getElementsByName("item_select[]");
	for(var i=0;i<objs.length;i++)
	{
		var item_cd = objs[i].value;
		var objs = document.getElementsByName("disp_order_"+item_cd);
		var val = objs[0].value;
		if(val == "" || !val.match(/^[0-9]+$/))
		{
			alert("表示順は数値を入力してください。");
			return;
		}
	}

	document.itemform.postback_mode.value = "item_disp_update";
	document.itemform.submit();
}

function reload_page()
{
	location.href = "fplusadm_mst.php?session=<?=$session?>&mst_cd=<?=$mst_cd?>";
}

//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}
.list td td {border-width:0;}

.list2 {border-collapse:collapse;}
.list2 td {border:#E6B3D4 solid 1px;}
.list2_in1 {border-collapse:collapse;}
.list2_in1 td {border-width:0;}
.list2_in2 {border-collapse:collapse;}
.list2_in2 td {border:#E6B3D4 solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#E6B3D4 solid 0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">


<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="32" height="32" class="spacing"><a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplusadm_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="fplusadm_mst.php?session=<? echo($session); ?>"><b>マスタ管理</b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="fplus_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
	</tr>
</table>


<table style="width:100%; margin-top:4px"><tr>
<td style="vertical-align:top; width:120px"><?= fplusadm_common_show_admin_left_menu($session); ?></td>
<td style="vertical-align:top">

	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #c488ae; margin-bottom:2px">
		<tr>
			<td width="3" align="right"><img src="img/menu-fplus-left-on.gif" alt="" width="3" height="22"></td>
			<td width="120" align="center" bgcolor="#FF99CC"><a href="fplusadm_mst.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><nobr>マスタ作成</nobr></font></a></td>
			<td width="3" align="left"><img src="img/menu-fplus-right-on.gif" alt="" width="3" height="22"></td>
			<td width="10">&nbsp;</td>
			<td width="3" align="right"><img src="img/menu-fplus-left.gif" alt="" width="3" height="22"></td>
			<td width="120" align="center" bgcolor="#FFDFFF"><a href="fplusadm_hospital_mst.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>病院属性</nobr></font></a></td>
			<td width="3" align="left"><img src="img/menu-fplus-right.gif" alt="" width="3" height="22"></td>
			<td>&nbsp;</td>
		</tr>
	</table>



	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:6px">
		<tr>
			<td width="30%" valign="top">


				<!-- theme list start -->
				<form name="delmst" action="fplusadm_mst_delete.php" method="post">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="1">
									<tr bgcolor="#feeae9">
										<td class="spacing" height="24"><?=$font?>マスタ一覧</font></td>
										<td align="right">
											<input type="button" value="作成" onclick="registMst();">
											<input type="button" value="削除" onclick="deleteMst();">
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr height="400">
							<td valign="top">
								<? show_mst_list($con, $mst_cd, $session, $fname); ?>
							</td>
						</tr>
					</table>

					<input type="hidden" name="session" value="<? echo($session); ?>">
					<input type="hidden" name="mst_cd" value="<? echo($mst_cd); ?>">
				</form>
				<!-- theme list end -->

			</td>


			<td><img src="img/spacer.gif" alt="" width="2" height="1"></td>

			<td width="70%" valign="top">
				<!-- theme detail start -->
				<form name="updmst" action="fplusadm_mst_update_exe.php" method="post">
					<? if ($mst_cd != "") { show_mst_detail($con, $mst_cd, $fname); ?><img src="img/spacer.gif" alt="" width="1" height="2"><br><? } ?>
					<input type="hidden" name="session" value="<? echo($session); ?>">
					<input type="hidden" name="mst_cd" value="<? echo($mst_cd); ?>">
				</form>
				<!-- theme detail end -->


				<!-- comment view start -->
				<form name="itemform" action="fplusadm_mst.php" method="post">
					<input type="hidden" name="session" value="<? echo($session); ?>">
					<input type="hidden" name="mst_cd" value="<? echo($mst_cd); ?>">
					<input type="hidden" name="postback" value="true">
					<input type="hidden" name="postback_mode" value="">

					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list2">
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list2_in1">
									<tr bgcolor="#feeae9">
										<td width="20%" class="spacing" height="24"><?=$font?>管理項目一覧</font></td>
										<td align="right" nowrap>
											<?=$font?>
												<? if ($mst_cd != "") { ?>
													<? if($item_list != "") { ?>
														<input type="button" value="削除"     onclick="deleteItem()"     style="width:80px">
													<? } ?>
													<input type="button" value="項目追加" onclick="registItem()"     style="width:80px">
													<? if($item_list != "") { ?>
														<input type="button" value="表示更新" onclick="updateItemDisp()" style="width:80px">
													<? } ?>
												<? } ?>
											</font>
										</td>
									</tr>
								</table>
							</td>
						</tr>

						<tr height="328">
							<td valign="top" style="padding:2px;">
								<? if($item_list == "") { ?>
									<?=$font?>登録済みの項目はありません。</font>
								<? } else { ?>
									<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list2_in2">
										<tr>
											<td bgcolor="#feeae9" width="30"><?=$font?>&nbsp</font></td>
											<td bgcolor="#feeae9" width="70"><?=$font?>項目コード</font></td>
											<td bgcolor="#feeae9" width="*"><?=$font?>項目名称</font></td>
											<td bgcolor="#feeae9" width="100"><?=$font?>表示区分</font></td>
											<td bgcolor="#feeae9" width="70"><?=$font?>表示順</font></td>
										</tr>
										<?
											foreach($item_list as $item_list1) {
												$item_cd = $item_list1['item_cd'];
												$item_name = $item_list1['item_name'];
												$disp_flg = $item_list1['disp_flg'];
												$disp_order = $item_list1['disp_order'];
										?>
										<input type="hidden" name="list_item_cd[]" value="<?=$item_cd?>">

										<tr>
											<td bgcolor="#ffffff" align="center"><?=$font?><input type="checkbox" name="item_select[]" value="<?=$item_cd?>"></font></td>
											<td bgcolor="#ffffff"><?=$font?><?=$item_cd?></font></td>
											<td bgcolor="#ffffff"><?=$font?><a href="javascript:updateItem('<?=$item_cd?>')"><?=h($item_name) ?></a></font></td>
											<td bgcolor="#ffffff">
												 <?=$font?>
													<select name="disp_flg_<?=$item_cd?>">
														<option value="t" <?if($disp_flg != "f"){?>selected<?}?> >表示</option>
														<option value="f" <?if($disp_flg == "f"){?>selected<?}?> >非表示</option>
													</select>
												 </font>
											</td>
											<td bgcolor="#ffffff">
												 <?=$font?><input type="text" name="disp_order_<?=$item_cd?>" value="<?=$disp_order?>" maxlength="4" size="4" style="ime-mode: inactive;"></font>
											</td>
										</tr>
										<? } ?>
									</table>
								<? } ?>
							</td>
						</tr>
					</table>
				</form>
				<!-- comment view end -->


			</td>
		</tr>
	</table>


</td></tr></table>

</td></tr></table>

</body>
</html>


<?
// マスタ情報内容を表示
function show_mst_detail($con, $mst_cd, $fname) {

	// マスタ情報を取得
	$sql = "select a.* from tmplitemmst a";
	$cond = "where a.mst_cd = '$mst_cd'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$mst_name = pg_fetch_result($sel, 0, "mst_name");
	$mst_content = pg_fetch_result($sel, 0, "mst_content");
	$date = pg_fetch_result($sel, 0, "create_time");
	$date = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $date);
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
	echo("<tr height=\"22\">\n");
	echo("<td width=\"22%\" align=\"right\" bgcolor=\"#feeae9\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">マスタ管理コード</font></td>\n");
	echo("<td width=\"10%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".h($mst_cd)."</font></td>\n");
	echo("<td width=\"15%\" align=\"right\" bgcolor=\"#feeae9\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">マスタ名称</font></td>\n");
	echo("<td><input type=\"text\" name=\"mst_name\" value=\"".h($mst_name)."\" maxlength=\"40\" size=\"40\" style=\"ime-mode: active;width:100%\"></td>\n");
	echo("<td width=\"10%\" align=\"center\"><input type=\"button\" value=\"更新\" onclick=\"updateMst();\"></td>\n");
	echo("</tr>\n");
	echo("<tr height=\"22\">\n");
	echo("<td align=\"right\" bgcolor=\"#feeae9\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">内容</font></td>\n");
	echo("<td colspan=\"4\"><textarea name=\"mst_content\" style=\"ime-mode: active;width:100%\" rows=\"3\">".h($mst_content)."</textarea></td>\n");
	echo("</tr>\n");
	echo("</table>\n");

}

// マスタ一覧を表示
function show_mst_list($con, $mst_cd, $session, $fname) {

	$ret_mst_cd = "";

	// マスタ一覧を取得
	$sql = "select mst_cd, mst_name from tmplitemmst";
	$cond = "order by mst_cd";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if (pg_num_rows($sel) == 0) {
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">\n");
		echo("<tr height=\"22\">\n");
		echo("<td class=\"spacing\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">登録済みのマスタはありません。</font></td>\n");
		echo("</tr>\n");
		echo("</table>\n");
		return $ret_mst_cd;
	}

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">\n");

	while ($row = pg_fetch_array($sel)) {

		$tmp_mst_cd = $row["mst_cd"];
		$tmp_mst_name = $row["mst_name"];
		$tmp_title = $tmp_mst_cd ." ". $tmp_mst_name;

		if ($mst_cd == "") {
			$mst_cd = $tmp_mst_cd;
		}

		$cbox = "<input type=\"checkbox\" name=\"mst_cds[]\" value=\"$tmp_mst_cd\">";
		$label = ($tmp_mst_cd == $mst_cd) ? $tmp_title : "<a href=\"fplusadm_mst.php?session=$session&mst_cd=$tmp_mst_cd\">$tmp_title</a>";
		echo("<tr height=\"22\">\n");
		echo("<td>\n");
		echo("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		echo("<tr>\n");
		if ($tmp_mst_cd == $mst_cd) {
			echo("<td><img src=\"img/right_red.gif\" alt=\"\" width=\"17\" height=\"17\"></td>\n");
		} else {
			echo("<td><img src=\"img/spacer.gif\" alt=\"\" width=\"17\" height=\"17\"></td>\n");
		}
		echo("<td valign=\"top\">$cbox</td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$label</font></td>\n");
		echo("</tr>\n");
		echo("</table>\n");
		echo("</td>\n");
		echo("</tr>\n");

		if ($tmp_mst_cd == $mst_cd) {
			$ret_mst_cd = $tmp_mst_cd;
		}
	}
	echo("</table>\n");

	return $ret_mst_cd;
}
