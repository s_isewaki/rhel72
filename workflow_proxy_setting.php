<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">

<?php
//代理申請設定
//workflow_proxy_setting.php

require_once("about_comedix.php");
require_once("referer_common.ini");
require_once("workflow_common.ini");
require_once("workflow_proxy_common_class.php");

//require_once("duty_shift_common.ini");
//require_once("duty_shift_manage_tab_common.ini");
//require_once("duty_shift_common_class.php");
//require_once("duty_shift_mprint_common_class.php");
//require_once("get_menu_label.ini");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
$checkauth = check_authority($session, 3, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

$obj_proxy = new workflow_proxy_common_class($con, $fname);

// 遷移元の設定
if ($workflow != "") {
    set_referer($con, $session, "workflow", $workflow, $fname);
    $referer = $workflow;
} else {
    $referer = get_referer($con, $session, "workflow", $fname);
}

///-----------------------------------------------------------------------------
// スタッフ情報を取得
///-----------------------------------------------------------------------------
$set_array = array();
if ($reload_flg == "1") {
    ///-----------------------------------------------------------------------------
    //再表示の場合、元データ取得
    ///-----------------------------------------------------------------------------
    $wk_array = array();
    $wk_emp_id_array = split(",", $emp_id_array);
    $m=0;
    foreach ($wk_emp_id_array as $emp_id) {
        $wk_array[$m]["id"] = $emp_id;
        $m++;
    }
    ///-----------------------------------------------------------------------------
    //指定職員IDの追加／削除
    ///-----------------------------------------------------------------------------
    if ($del_emp_id != "") {
        //削除の場合
        $m=0;
        for($i=0; $i<count($wk_array); $i++) {
            if ($wk_array[$i]["id"] != $del_emp_id) {
                $set_array[$m] = $wk_array[$i];
                $m++;
            }
        }
    } else {
        //追加データの場合
        $m=0;
        $wk_add_array = array();
        $wk_emp_id_array = split(",", $add_emp_id_array);
        foreach ($wk_emp_id_array as $emp_id) {
            $wk_add_array[$m]["id"] = $emp_id;
            $m++;
        }
        $m=0;
        if ($add_idx == -1) {
            for($k=0; $k<count($wk_add_array); $k++) {
                $set_array[$m] = $wk_add_array[$k];
                $m++;
            }
        }
        if ($emp_id_array != "") {
            for($i=0; $i<count($wk_array); $i++) {
                if ($i == $add_idx) {
                    $set_array[$m] = $wk_array[$i];
                    $m++;
                    for($k=0; $k<count($wk_add_array); $k++) {
                        $set_array[$m] = $wk_add_array[$k];
                        $m++;
                    }
                } else {
                    $set_array[$m] = $wk_array[$i];
                    $m++;
                }
            }
        }
    }
    ///-----------------------------------------------------------------------------
    //データ再設定
    ///-----------------------------------------------------------------------------
    $staff_name_array = $obj_proxy->get_workflow_proxy_staff_name($set_array);
    $staff_array = array();
    
    for($i=0;$i<count($set_array);$i++){
        $staff_array[$i]["id"] = $set_array[$i]["id"];			//スタッフＩＤ（職員ＩＤ）
        $staff_array[$i]["no"] = $i + 1;						//表示順
        $wk_id = $set_array[$i]["id"];
        $staff_array[$i]["name"] = $staff_name_array["$wk_id"]["name"];			//スタッフ名称
        $staff_array[$i]["job_nm"] = $staff_name_array["$wk_id"]["job_nm"];			//
        $staff_array[$i]["st_nm"] = $staff_name_array["$wk_id"]["st_nm"];			//
    }
} else {
    ///-----------------------------------------------------------------------------
    //初期表示の場合
    ///-----------------------------------------------------------------------------
    $staff_array = $obj_proxy->get_workflow_proxy_staff_array();
}

?>
<title>CoMedix ワークフロー ｜ 代理申請設定</title>
<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/prototype-1.7.1.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
	jQuery.noConflict();
	jQuery(document).ready(function($) {
		$('#container > tbody').sortable({
			items: '> tr',
			axis: 'y',
			cancel: 'input,a,button',
			placeholder: 'droptarget',
			opacity: 0.4,
			scroll: false
		});
	});



	var childwin = null;

	///-----------------------------------------------------------------------------
	// openEmployeeList
	///-----------------------------------------------------------------------------
	function openEmployeeList(item_id) {
		dx = screen.width;
		dy = screen.top;
		base = 0;
		wx = 720;
		wy = 600;
		var url = './emplist_popup.php';
		url += '?session=<?=$session?>';
		url += '&emp_id=<?=$emp_id?>';
		url += '&mode=19';
		url += '&item_id='+item_id;
		childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

		childwin.focus();
	}
	///-----------------------------------------------------------------------------
	// closeEmployeeList
	///-----------------------------------------------------------------------------
	function closeEmployeeList() {
		if (childwin != null && !childwin.closed) {
			childwin.close();
		}
		childwin = null;
	}
	///-----------------------------------------------------------------------------
	// add_target_list
	///-----------------------------------------------------------------------------
	function add_target_list(item_id, emp_id, emp_name)
	{
		var emp_ids = emp_id.split(", ");
		var emp_names = emp_name.split(", ");

		///-----------------------------------------------------------------------------
		//変更用（先頭の人で判定）
		///-----------------------------------------------------------------------------
		wk_emp_id = emp_ids[0];
		wk_emp_name = emp_names[0];
		///-----------------------------------------------------------------------------
		//重複チェック（重複データは対象外）
		///-----------------------------------------------------------------------------
		var wk = document.mainform.emp_id_array.value;
		var emp_ids2 = wk.split(",");
		var ok_emp_ids="";
		var err_flg = 0;
		for(i=0;i<emp_ids.length;i++){
			err_flg = 0;
			for(k=0;k<emp_ids2.length;k++){
				if (emp_ids[i] == emp_ids2[k]) {
					err_flg = 1;
					break;
				}
			}
			if (err_flg == 0){
				if (ok_emp_ids != "") {
					ok_emp_ids += ",";
				}
				ok_emp_ids += emp_ids[i];
			}
		}

		if (ok_emp_ids == "") {
			alert('職員の重複指定はできません。');
			return;
		}
		///-----------------------------------------------------------------------------
		//画面再表示
		///-----------------------------------------------------------------------------
		document.mainform.reload_flg.value = "1";
		document.mainform.del_emp_id.value = "";
		document.mainform.add_emp_id_array.value = ok_emp_ids;
		document.mainform.action="workflow_proxy_setting.php";
		document.mainform.submit();
	}
	///-----------------------------------------------------------------------------
	// 追加
	///-----------------------------------------------------------------------------
	function addEmp(idx)
	{
		//追加行
		document.mainform.add_idx.value = idx;
		document.mainform.del_emp_id.value = "";
		//職員名簿表示
		openEmployeeList('1');
	}
	///-----------------------------------------------------------------------------
	// 削除
	///-----------------------------------------------------------------------------
	function delEmp(emp_id)
	{
		//画面再表示
		document.mainform.reload_flg.value = "1";
		document.mainform.add_idx.value = "";
		document.mainform.del_emp_id.value = emp_id;
		document.mainform.action="workflow_proxy_setting.php";
		document.mainform.submit();
	}
	///-----------------------------------------------------------------------------
	// 更新
	///-----------------------------------------------------------------------------
	function editData() {
		//職員登録子画面をＣＬＯＳＥ
		closeEmployeeList();

		//更新処理
		document.mainform.action="workflow_proxy_setting_update.php"
		document.mainform.submit();
	}
    // テーブル行の削除
    function remove_table_row(obj) {
	    var tr = obj.parentNode.parentNode;
	    tr.parentNode.deleteRow(tr.sectionRowIndex);
    }

    // テーブル行の追加
    // obj ボタン、flg 0:見出し行 1:データ行
    function add_table_row(obj, flg) {
	    var table = document.getElementById("container");
        if (flg == 0) {
            var idx = 1;
        }
        else {
    	    var idx = obj.parentNode.parentNode.sectionRowIndex + 2;
        }
     
	    var row = table.insertRow(idx);
        row.className = 'mprintrow';
        row.style.cursor = 'move';
        row.style.height = '26';
	    var cell01 = row.insertCell(0);
	    var cell02 = row.insertCell(1);
	    var cell03 = row.insertCell(2);
	    var cell04 = row.insertCell(3);
	    var cell05 = row.insertCell(4);
	    var cell06 = row.insertCell(5);

	    cell01.innerHTML = '-';
	    cell02.innerHTML = '<input type="text" name="layout_name[]" size="50" maxlength="60" style="ime-mode:active;" value=""><input type="hidden" name="layout_id[]" value="-1">';
	    cell03.innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#C0C0C0">【帳票定義】</font>';
	    cell04.innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#C0C0C0">【帳票表示】</font>';
	    cell05.innerHTML = '<button type="button" onclick="add_table_row(this,1);">追加</button>';
	    cell06.innerHTML = '<button type="button" onclick="remove_table_row(this);">削除</button>';

        cell01.align = 'center';
        cell03.align = 'center';
        cell04.align = 'center';
        cell05.align = 'center';
        cell06.align = 'center';

        cell01.width = '50';
        cell02.width = '340';
        cell03.width = '100';
        cell04.width = '100';
        cell05.width = '75';
        cell06.width = '75';

    }
    //帳票定義画面
    function openLayout(layout_id) {

	    wx = 820;
	    wy = 750;
	    dx = screen.width;
	    dy = screen.top;
	    base = screen.height / 2 - (wy / 2);
	    var url = 'work_admin_csv_layout.php';
	    url += '?session=<?=$session?>';
	    url += '&layout_id='+layout_id;
	    url += '&from_flg=2';
	    childwin_print = window.open(url, 'printLayoutPopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	    childwin_print.focus();

    }
    //帳票表示子画面
    function openPreview(layout_id) {

        base_left = 0;
        base = 0;
        wx = window.screen.availWidth;
        wy = 280;
	    var url = 'duty_shift_mprint_setting_preview.php';
	    url += '?session=<?=$session?>';
	    url += '&layout_id='+layout_id;
	    childwin_preview = window.open(url, 'printPreviewPopup', 'left='+base_left +',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	    childwin_preview.focus();

    }
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/workflow/workflow.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table id="header">
<tr>
<td class="icon"><a href="workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><a href="workflow_menu.php?session=<?=$session?>"><b>ワークフロー</b></a></td>
</tr>
</table>
<? } else { ?>
<table id="header">
<tr>
<td class="icon"><a href="application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b08.gif" width="32" height="32" border="0" alt="決裁・申請"></a></td>
<td>
	<a href="application_menu.php?session=<? echo($session); ?>"><b>決裁・申請</b></a> &gt; 
	<a href="workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; 
	<a href="workflow_notice_setting.php?session=<? echo($session); ?>"><b>オプション</b></a>
</td>
<td class="kanri"><a href="application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></td>
</tr>
</table>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="120"><?show_wkfw_sidemenu($session);?></td>
<td width="5"><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top">

<? show_wkfw_menuitem($session, $fname, ""); ?>

<form action="workflow_notice_setting_update_exe.php" method="post" name="mainform">

		<table width="650" border="0" cellspacing="0" cellpadding="2">
        <tr>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        代理申請をすることができる職員<br>
        ※休暇申請テンプレート（オプション）を使うときのみ有効</font></td>
		<td align="right"><input type="button" value="更新" onclick="editData();" ></td>
        </tr>
		</table>
		<table width="650" border="0" cellspacing="0" cellpadding="2" class="list">
        <tr>
		<td width="200" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        氏名
        </font></td>
		<td width="150" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        職種
        </font></td>
		<td width="150" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        役職
        </font></td>
		<td width="75" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<input type="button" value="追加" onclick="addEmp(-1);">
        </font></td>
		<td width="75" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">削除
        </font></td>
        </tr>
<?
for ($i=0; $i<count($staff_array); $i++) {
    $wk_id = $staff_array[$i]["id"];
    
    echo("<tr>");
    echo("<td align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$staff_array[$i]["name"]}</font></td> \n");
    echo("<td align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$staff_array[$i]["job_nm"]}</font></td> \n");
    echo("<td align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$staff_array[$i]["st_nm"]}</font></td> \n");
    // 追加／削除
    echo("<td align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
    echo("<input type=\"button\" value=\"追加\" onclick=\"addEmp($i);\"> \n");
    echo("</font></td> \n");
    echo("<td align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
    echo("<input type=\"button\" value=\"削除\" onclick=\"delEmp('$wk_id');\"> \n");
    echo("</font></td> \n");
    
    echo("</tr>\n");
}
?>
		</table>

<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="emp_id_array" value="<? show_emp_id_array($staff_array); ?>">
		<input type="hidden" name="add_idx" value="<? echo($add_idx); ?>">
		<input type="hidden" name="del_emp_id" value="<? echo($del_emp_id); ?>">
		<input type="hidden" name="add_emp_id_array" value="<? echo($add_emp_id_array); ?>">
		<input type="hidden" name="reload_flg" value="<? echo($reload_flg); ?>">
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>

<?
///-----------------------------------------------------------------------------
// show_emp_id_array
///-----------------------------------------------------------------------------
function show_emp_id_array($staff_array) {
    for ($i = 0; $i < count($staff_array); $i++) {
        if ($i > 0) {
            echo(",");
        }
        echo($staff_array[$i]["id"]);
    }
}
