<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
// CSVレイアウト調整、帳票定義
// work_admin_csv_layout.php
//勤務シフト作成＞管理帳票対応 20130423
//"":検索後月集計CSVダウンロード実行 1:検索画面のボタンから呼ばれた場合
if ($from_flg != "2") {
    $title1 = "出勤表";
    $title2 = "CSVレイアウト調整";
}
//2:勤務シフト作成から
else {
    $title1 = "勤務シフト作成";
    $title2 = "帳票定義";
}
?>
<title>CoMedix <? echo ($title1." | ".$title2); ?></title>
<?
require_once("about_comedix.php");
require_once("show_attendance_pattern.ini");
require_once("atdbk_menu_common.ini");
require_once("work_admin_csv_common.ini");
require_once("atdbk_common_class.php");
require_once("show_select_values.ini");
require_once("show_timecard_common.ini");
require_once("duty_shift_mprint_common_class.php");
require_once("atdbk_close_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
if ($from_flg != "2") {
    $checkauth = check_authority($session, 42, $fname);
}
else {
    $checkauth = check_authority($session, 70, $fname);
}
if ($checkauth == "0" && $mmode != "usr") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
$obj_mprint = new duty_shift_mprint_common_class($con, $fname);
$atdbk_close_class = new atdbk_close_class($con, $fname);

	///-----------------------------------------------------------------------------
	//初期値設定
	///-----------------------------------------------------------------------------
$org_layout_id = $layout_id;
if ($layout_id == "-") {
	$layout_id = "01";
}
//検索なしで呼ばれた場合、レイアウトIDを元の状態でCSV処理に渡す
$hid_layout_id = ($emp_id_list == "") ? $org_layout_id : $layout_id;

$arr_csv_list = array();
//CSVレイアウト登録時
if ($postback == "1") {
	$arr_csv_list = split(",", $csv_list);
    update_csv_list($con, $fname, $arr_csv_list, $header_flg, $csv_ext_name, $layout_id, $layout_name, $sortkey1, $sortkey2, $sortkey3, $sortkey4, $sortkey5, $from_flg, $paper_size, $orientation, $zerodata_no_out_flg, $fraction_flg);
} else {
	//初期表示時、DBから取得
	$arr_csv_list = get_csv_list($con, $fname, $layout_id, $from_flg);
}

//レイアウト名称取得
if ($from_flg != "2") {
    $arr_layout = get_layout_name($con, $fname, "");
}
//レイアウト別に設定 20101102
$arr_config = get_timecard_csv_config($con, $fname, $layout_id, $from_flg);
$header_flg = $arr_config["csv_header_flg"];
$wk_ext = $arr_config["csv_ext_name"];
//集計項目見出し
$total_item_title = $arr_config["total_item_title"];
for ($i=2; $i<=8; $i++) {
	$varname = "total_item_title".$i;
	$$varname = $arr_config["$varname"];
}

//項目情報取得 work_admin_csv_common.ini
$arr_items = get_items($con, $fname, $shift_group_id, $from_flg);
//項目名配列
$arr_itemname = array();
for ($i=0; $i<count($arr_items); $i++) {
	$tmp_id = $arr_items[$i]["id"];
	//集計項目見出し
	if ($tmp_id == "2_74" && $total_item_title != "") {
		$arr_items[$i]["name"] .= "（".$total_item_title."）";
	}
	//集計項目"2_69"〜"2_63"見出し
	for ($j=2; $j<=8; $j++) {
		$wk_code = "2_".(69+2-$j);
		if ($tmp_id == $wk_code) {
			$varname = "total_item_title".$j;
			$wk_title = $$varname;
			if ($wk_title != "") {
				$arr_items[$i]["name"] .= "（".$wk_title."）";
			}
		}
	}
	$tmp_name = $arr_items[$i]["name"];
	$arr_itemname[$tmp_id] = $tmp_name;
}
$arr_itemname["9_0"] = "空セル";

$sql = "select closing, closing_parttime, closing_month_flg from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
//20150707
//	$closing = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing") : "";
$closing = $atdbk_close_class->get_empcond_closing($top_emp_id);

$closing_month_flg = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_month_flg") : "1";


if ($yyyymm != "") {
	$year = substr($yyyymm, 0, 4);
	$month = intval(substr($yyyymm, 4, 2));
} else {
	$year = date("Y");
	$month = date("n");
	$yyyymm = $year . date("m");
}

$last_month_y = $year;
$last_month_m = $month - 1;
if ($last_month_m == 0) {
	$last_month_y--;
	$last_month_m = 12;
}
$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);

$next_month_y = $year;
$next_month_m = $month + 1;
if ($next_month_m == 13) {
	$next_month_y++;
	$next_month_m = 1;
}
$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);

// 開始日・締め日を算出
$calced = false;
switch ($closing) {
case "1":  // 1日
	$closing_day = 1;
	break;
case "2":  // 5日
	$closing_day = 5;
	break;
case "3":  // 10日
	$closing_day = 10;
	break;
case "4":  // 15日
	$closing_day = 15;
	break;
case "5":  // 20日
	$closing_day = 20;
	break;
case "6":  // 末日
	$start_year = $year;
	$start_month = $month;
	$start_day = 1;
	$end_year = $start_year;
	$end_month = $start_month;
	$end_day = days_in_month($end_year, $end_month);
	$calced = true;
	break;
}
if (!$calced) {
	$day = date("j");
	$start_day = $closing_day + 1;
	$end_day = $closing_day;

	//当月〜翌月
	if ($closing_month_flg != "2") {
		$start_year = $year;
		$start_month = $month;
		
		$end_year = $year;
		$end_month = $month + 1;
		if ($end_month == 13) {
			$end_year++;
			$end_month = 1;
		}
	}
	//前月〜当月
	else {
		$start_year = $year;
		$start_month = $month - 1;
		if ($start_month == 0) {
			$start_year--;
			$start_month = 12;
		}
		
		$end_year = $year;
		$end_month = $month;
	}
}
$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);

// 対象年月度を変数にセット
$c_yyyymm = substr($start_date, 0, 6);
if ($select_start_yr == "") {
	$select_start_yr = $start_year;
	$select_start_mon = $start_month;
	$select_start_day = $start_day;
	$select_end_yr = $end_year;
	$select_end_mon = $end_month;
	$select_end_day = $end_day;
}
?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

function selectItem() {

	for (var i = 0, j = document.mainform.csv_item_moto.options.length; i < j; i++) {
		if (document.mainform.csv_item_moto.options[i].selected) {
			var id = document.mainform.csv_item_moto.options[i].value;
			var name = document.mainform.csv_item_moto.options[i].text;

			var in_group = false;
			for (var k = 0, l = document.mainform.csv_item.options.length; k < l; k++) {
				if (document.mainform.csv_item.options[k].value == id) {
					in_group = true;
					break;
				}
			}

			if (!in_group) {
				addOption(document.mainform.csv_item, id, name, '');
			}
		}
	}
}
	
function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
	if (selected == value) {
		box.selectedIndex = box.options.length -1;
		return;
	}
}

function deleteItem() {

	for (var i = document.mainform.csv_item.options.length - 1; i >= 0; i--) {
		if (document.mainform.csv_item.options[i].selected) {
			document.mainform.csv_item.options[i] = null;
		}
	}
}

function raiseItem() {
	var selected_items = new Array();
	var normal_items = new Array();

	var sel_start_pos = 0;
	var first_flg = true;

	for (var i = 0, j = document.mainform.csv_item.options.length; i < j; i++) {
		var item_id = document.mainform.csv_item.options[i].value;
		var item_name = document.mainform.csv_item.options[i].text;
		if (document.mainform.csv_item.options[i].selected) {
			selected_items.push(new Array(item_id, item_name));
			if (first_flg == true) {
				sel_start_pos = i;
				first_flg = false;
			}
		} else {
			normal_items.push(new Array(item_id, item_name));
		}
	}

	deleteAllOptions(document.mainform.csv_item);

	first_len = sel_start_pos - 1;
	if (first_len < 0) {
		first_len = 0;
	}

	for (var i = 0, j = first_len; i < j; i++) {
		var item_id = normal_items[i][0];
		var item_name = normal_items[i][1];
		addOption(document.mainform.csv_item, item_id, item_name, false);
	}

	for (var i = 0, j = selected_items.length; i < j; i++) {
		var item_id = selected_items[i][0];
		var item_name = selected_items[i][1];
		addOption(document.mainform.csv_item, item_id, item_name, item_id);
	}

	for (var i = first_len, j = normal_items.length; i < j; i++) {
		var item_id = normal_items[i][0];
		var item_name = normal_items[i][1];
		addOption(document.mainform.csv_item, item_id, item_name, false);
	}

	for (var i = 0, j = selected_items.length; i < j; i++) {
		document.mainform.csv_item.options[first_len+i].selected = true;
	}

}

function dropItem() {
	var selected_items = new Array();
	var normal_items = new Array();

	var max_len = document.mainform.csv_item.options.length;
	var sel_start_pos = max_len;

	for (var i = max_len-1; i >= 0; i--) {
		if (document.mainform.csv_item.options[i].selected) {
			sel_start_pos = i;
			break;
		}
	}

	kijun_pos = sel_start_pos + 1;
	if (kijun_pos > max_len) {
		kijun_pos = max_len;
	}

	var first_len = 0;
	for (var i = 0, j = document.mainform.csv_item.options.length; i < j; i++) {
		var item_id = document.mainform.csv_item.options[i].value;
		var item_name = document.mainform.csv_item.options[i].text;
		if (document.mainform.csv_item.options[i].selected) {
			selected_items.push(new Array(item_id, item_name));
		} else {
			normal_items.push(new Array(item_id, item_name));
			if (i <= kijun_pos) {
				first_len++;
			}
		}
	}

	deleteAllOptions(document.mainform.csv_item);

	for (var i = 0, j = first_len; i < j; i++) {
		var item_id = normal_items[i][0];
		var item_name = normal_items[i][1];
		addOption(document.mainform.csv_item, item_id, item_name, false);
	}

	for (var i = 0, j = selected_items.length; i < j; i++) {
		var item_id = selected_items[i][0];
		var item_name = selected_items[i][1];
		addOption(document.mainform.csv_item, item_id, item_name, item_id);
	}

	for (var i = first_len, j = normal_items.length; i < j; i++) {
		var item_id = normal_items[i][0];
		var item_name = normal_items[i][1];
		addOption(document.mainform.csv_item, item_id, item_name, false);
	}

	for (var i = 0, j = selected_items.length; i < j; i++) {
		document.mainform.csv_item.options[first_len + i].selected = true;
	}

}

function deleteAllOptions(box) {

	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}

}

function addEmptyCell() {

	var pos = 0;
	var cnt = 0;
	for (var i = 0, j = document.mainform.csv_item.options.length; i < j; i++) {
		if (document.mainform.csv_item.options[i].selected) {
			cnt++;
			pos = i + 1;
		}
	}
	
	if (cnt != 1) {
		alert('追加したい位置を一箇所選択してください');
		return;
	}
	var normal_items = new Array();
	for (var i = 0, j = document.mainform.csv_item.options.length; i < j; i++) {
		var item_id = document.mainform.csv_item.options[i].value;
		var item_name = document.mainform.csv_item.options[i].text;
		normal_items.push(new Array(item_id, item_name));
	}
	deleteAllOptions(document.mainform.csv_item);
	for (var i = 0, j = pos; i < j; i++) {
		var item_id = normal_items[i][0];
		var item_name = normal_items[i][1];
		addOption(document.mainform.csv_item, item_id, item_name, false);
	}
	addOption(document.mainform.csv_item, '9_0', '空セル', false);
	for (var i = pos, j = normal_items.length; i < j; i++) {
		var item_id = normal_items[i][0];
		var item_name = normal_items[i][1];
		addOption(document.mainform.csv_item, item_id, item_name, false);
	}

}

function registLayout() {

    //制限なしとする 20140220
    //if (document.mainform.csv_item.options.length > 256) {
    //    alert('項目は256件以内で登録してください');
    //    return false;
    //}

	document.mainform.csv_list.value = '';
	for (var i = 0, j = document.mainform.csv_item.options.length; i < j; i++) {
		if (i > 0) {
			document.mainform.csv_list.value += ',';
		}
		document.mainform.csv_list.value += document.mainform.csv_item.options[i].value;
	}
	//拡張子
	for (var i=0; i<document.mainform.ext.length; i++) {
		if (document.mainform.ext[i].checked) {
			document.mainform.csv_ext_name.value = document.mainform.ext[i].value;
		}
	}

	document.mainform.postback.value = "1";
	document.mainform.action = "work_admin_csv_layout.php";
	document.mainform.submit();
}

//ダウンロード
function downloadCSV() {
	
	if (window.opener.document.csv && window.opener.document.csv.elements['emp_id_list']) {
		document.csv.emp_id_list.value = window.opener.document.csv.emp_id_list.value;
		if (window.opener.document.mainform._yyyymm) {
			document.csv.yyyymm.value = window.opener.document.mainform._yyyymm.value;
		}
        //var len = window.opener.document.mainform.elements['emp_id[]'].length;
        //for (var i=0; i< len; i++) {
        //    if (window.opener.document.mainform.elements['emp_id[]'][i].checked) {
        //        var ele = document.createElement('input');
        //        // データを設定
        //        ele.setAttribute('type', 'hidden');
        //        ele.setAttribute('name', 'emp_id[]');
        //        ele.setAttribute('value', window.opener.document.mainform.elements['emp_id[]'][i].value);
        //        // 要素を追加
        //        document.csv.appendChild(ele);
        //    }
        //}
        
	}
	document.csv.csv_list.value = '';
	//for (var i = 0, j = document.mainform.csv_item.options.length; i < j; i++) {
	//	if (i > 0) {
	//		document.csv.csv_list.value += ',';
	//	}
	//	document.csv.csv_list.value += document.mainform.csv_item.options[i].value;
	//}
	//if (document.csv.csv_list.value == '') {
	//	alert('項目を選択してください');
	//	return;
	//}

	document.csv.action = "work_admin_csv_download.php";
	//ヘッダー出力
	if (document.mainform.header_flg.checked) {
		document.csv.header_flg.value = "t";
	} else {
		document.csv.header_flg.value = "";
	}
	//拡張子
	for (var i=0; i<document.mainform.ext.length; i++) {
		if (document.mainform.ext[i].checked) {
			document.csv.csv_ext_name.value = document.mainform.ext[i].value;
		}
	}
	//すべてゼロの職員は出力しない
	if (document.mainform.zerodata_no_out_flg.checked) {
		document.csv.zerodata_no_out_flg.value = "t";
	} else {
		document.csv.zerodata_no_out_flg.value = "";
	}
	//残業集計項目端数処理
	if (document.mainform.fraction_flg.checked) {
		document.csv.fraction_flg.value = "t";
	} else {
		document.csv.fraction_flg.value = "";
	}
	//メニュー画面から検索なしで呼ばれた場合
	if (document.csv.emp_id_list.value == '') {
		document.csv.srch_name.value = window.opener.document.mainform.srch_name.value;
		document.csv.cls.value = window.opener.document.mainform.cls.value;
		document.csv.atrb.value = window.opener.document.mainform.atrb.value;
		document.csv.dept.value = window.opener.document.mainform.dept.value;
		document.csv.duty_form_jokin.value = (window.opener.document.mainform.duty_form_jokin.checked) ? "checked" : "";
		document.csv.duty_form_hijokin.value = (window.opener.document.mainform.duty_form_hijokin.checked) ? "checked" : "";
		document.csv.group_id.value = window.opener.document.mainform.group_id.value;
		document.csv.shift_group_id.value = window.opener.document.mainform.shift_group_id.value;
	}

	//期間指定
	document.csv.select_start_yr.value = document.mainform.select_start_yr.value;
	document.csv.select_start_mon.value = document.mainform.select_start_mon.value;
	document.csv.select_start_day.value = document.mainform.select_start_day.value;
	document.csv.select_end_yr.value = document.mainform.select_end_yr.value;
	document.csv.select_end_mon.value = document.mainform.select_end_mon.value;
	document.csv.select_end_day.value = document.mainform.select_end_day.value;
	
	//ソートキー
	document.csv.sortkey1.value = document.mainform.sortkey1.value;
	document.csv.sortkey2.value = document.mainform.sortkey2.value;
	document.csv.sortkey3.value = document.mainform.sortkey3.value;
	document.csv.sortkey4.value = document.mainform.sortkey4.value;
	document.csv.sortkey5.value = document.mainform.sortkey5.value;

	document.csv.sus_flg.value = (window.opener.document.mainform.sus_flg.checked) ? "checked" : "";

	document.csv.submit();
	
}

function jobidset() {
		wx = 500;
		wy = 600;
		dx = screen.width / 2 - (wx / 2);
		dy = screen.top;
		base = screen.height / 2 - (wy / 2);
		var url = 'work_admin_csv_job_id.php';
		url += '?session=<?=$session?>';
		url += '&layout_id=<?=$layout_id?>';
		url += '&from_flg=<? echo($from_flg); ?>';
		window.open(url, 'JobIdSetPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

}

//項目出力形式設定
function csvconfig() {
		wx = 500;
		wy = 300;
		dx = screen.width / 2 - (wx / 2);
		dy = screen.top;
		base = screen.height / 2 - (wy / 2);
		var url = 'work_admin_csv_config.php';
		url += '?session=<?=$session?>';
		url += '&layout_id=<?=$layout_id?>';
		url += '&from_flg=<? echo($from_flg); ?>';
		window.open(url, 'CSVCONFIG', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

}

//勤務時間の設定
function outputtarget(){
		wx = 500;
		wy = 300;
		dx = screen.width / 2 - (wx / 2);
		dy = screen.top;
		base = screen.height / 2 - (wy / 2);
		var url = 'work_admin_csv_output_target.php';
		url += '?session=<?=$session?>';
		url += '&layout_id=<?=$layout_id?>';
		url += '&from_flg=<? echo($from_flg); ?>';
		window.open(url, 'OUTPUTTARGET', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

}
//休日出勤日数勤務パターンの設定
function ptnidset() {
		wx = 500;
		wy = 500;
		dx = screen.width / 2 - (wx / 2);
		dy = screen.top;
		base = screen.height / 2 - (wy / 2);
		var url = 'work_admin_csv_ptn_id.php';
		url += '?session=<?=$session?>';
		url += '&layout_id=<?=$layout_id?>';
		url += '&from_flg=<? echo($from_flg); ?>';
		window.open(url, 'PtnIdSetPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

}

//所定時間の設定
function fixedset() {
		wx = 500;
		wy = 300;
		dx = screen.width / 2 - (wx / 2);
		dy = screen.top;
		base = screen.height / 2 - (wy / 2);
		var url = 'work_admin_csv_fixed_set.php';
		url += '?session=<?=$session?>';
		url += '&layout_id=<?=$layout_id?>';
		url += '&from_flg=<? echo($from_flg); ?>';
		window.open(url, 'FixedSetPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

}
//集計項目
function totalitem() {
		wx = 500;
		wy = 500;
		dx = screen.width / 2 - (wx / 2);
		dy = screen.top;
		base = screen.height / 2 - (wy / 2);
		var url = 'work_admin_csv_total_item.php';
		url += '?session=<? echo($session); ?>';
		url += '&layout_id=<? echo($layout_id); ?>';
		url += '&from_flg=<? echo($from_flg); ?>';
		window.open(url, 'TotalItemPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

}
//集計項目２
function totalitem2(total_item_no) {
		wx = 500;
		wy = 500;
		dx = screen.width / 2 - (wx / 2);
		dy = screen.top;
		base = screen.height / 2 - (wy / 2);
		var url = 'work_admin_csv_total_item2.php';
		url += '?session=<? echo($session); ?>';
		url += '&layout_id=<? echo($layout_id); ?>';
		url += '&total_item_no='+total_item_no;
		url += '&from_flg=<? echo($from_flg); ?>';
		window.open(url, 'TotalItemPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

}

//交通費
function traitem() {
		wx = 520;
		wy = 500;
		dx = screen.width / 2 - (wx / 2);
		dy = screen.top;
		base = screen.height / 2 - (wy / 2);
		var url = 'work_admin_csv_tra.php';
		url += '?session=<? echo($session); ?>';
		url += '&layout_id=<? echo($layout_id); ?>';
		url += '&from_flg=<? echo($from_flg); ?>';
		window.open(url, 'TotalItemPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

}


</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
		<tr height="32" bgcolor="#5279a5">
		<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><? echo ($title2); ?></b></font></td>
		<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<form name="mainform" method="post">
<?
//CSVレイアウト調整
if ($from_flg != "2") {
    $wk_width = "1080";
}
//帳票定義
else {
    $wk_width = "800";
}
    ?>
	<table width="<? echo($wk_width); ?>" border="0" cellspacing="0" cellpadding="2">
	<tr height="22">
<? // CSVレイアウト調整の場合に、左側のレイアウト名を表示
    if ($from_flg != "2") { ?>
	<td width="280" style="border-right: #000000 solid 1px">
    <?
    for ($i=1; $i<=20; $i++) {
        echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        $wk_id = sprintf("%02d", $i);
        if ($wk_id != $layout_id) {
            echo("<a href=\"work_admin_csv_layout.php?session=$session&layout_id=$wk_id&from_flg=$from_flg&yyyymm=$yyyymm&mmode=$mmode\">");
        } else {
            echo("<b>");
        }
        echo("レイアウト".$wk_id);
        if ($wk_id != $layout_id) {
            echo("</a>\n");
        } else {
            echo("</b>");
        }
        echo("<br>&nbsp;&nbsp;&nbsp;&nbsp;".$arr_layout["$wk_id"]["name"]."</font><br>\n");
    }
	?>
	</td>
    <? } ?>
<td valign="top">
	<table width="800" border="0" cellspacing="0" cellpadding="2">
	<tr height="22">
		<td>
		</td>
		<td colspan="3">
<?
//検索対象画面から直接呼ばれた場合はダウンロードなしとする 20101022
if ($from_flg == "") {
			?>
			<select name="select_start_yr">
			<? show_select_years(10, $select_start_yr, false, true); ?>
			</select>/<select name="select_start_mon">
			<? show_select_months($select_start_mon, false); ?>
			</select>/<select name="select_start_day">
			<? show_select_days($select_start_day, false); ?>
			</select> 〜 <select name="select_end_yr">
			<? show_select_years(10, $select_end_yr, false, true); ?>
			</select>/<select name="select_end_mon">
			<? show_select_months($select_end_mon, false); ?>
			</select>/<select name="select_end_day">
			<? show_select_days($select_end_day, false); ?>
			</select>&nbsp;

			<input type="button" value="ダウンロード" onclick="downloadCSV();">&nbsp;
<? }
if ($from_flg == "2") {
    $arr_layout_mst = $obj_mprint->get_duty_shift_layout_mst_one($layout_id);
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b>帳票名称：<?
echo($arr_layout_mst["layout_name"]); ?></b></font>
<?
}
?>

		</td>
	</tr>
	<tr height="22">
		<td>
		</td>
		<td align="left" nowrap colspan="2">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($from_flg != "2") {
    $title3 = "出力レイアウト";
    $btn_name = "CSVレイアウト登録";
}
else {    
    $title3 = "データ出力設定";
    $btn_name = "登録";
}
echo ($title3);
?>
&nbsp;<label><input type="checkbox" name="header_flg" value="t" <?if ($header_flg == "t") { echo(" checked");} ?>>ヘッダーを出力する</label></font>
&nbsp;
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">拡張子　<label for="ext1"><input type="radio" id="ext1" name="ext" value="csv"<? if ($wk_ext == "csv") {echo " checked ";} ?>>csv</label>
<label for="ext2"><input type="radio"  id="ext2" name="ext" value="txt"<? if ($wk_ext == "txt") {echo " checked ";} ?>>txt</label></font>	
<? if ($from_flg != "2") { ?>
    <br>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b>レイアウト名称</b>&nbsp;</font>
    <?
    $wk_layout_name = $arr_layout["$layout_id"]["name"];
    echo("<input type=\"text\" name=\"layout_name\" value=\"$wk_layout_name\" size=\"50\" maxlength=\"60\" style=\"ime-mode:active;\"><br>\n");
		?>
<? } ?>
        </td>
	    <td align="left">
			<input type="button" value="<? echo($btn_name); ?>" onclick="registLayout();">&nbsp;
		</td>
	</tr>
	<?php
	//ゼロデータを出力しない設定
	?>
	<tr height="">
	    <td>&nbsp;</td>
	    <td align="left">
	    	<label>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<label><input type="checkbox" name="zerodata_no_out_flg" value="t" <?php if ($arr_config["zerodata_no_out_flg"] == "t") { echo " checked"; } ?>>すべてゼロの職員は出力しない</label>
			</font>
	    	</label>
		</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	</tr>
	<?php
	//残業集計項目端数処理
	?>
	<tr height="">
	    <td>&nbsp;</td>
	    <td align="left" colspan="3">
	    	<label>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<label><input type="checkbox" name="fraction_flg" value="t" <?php if ($arr_config["fraction_flg"] == "t") { echo " checked"; } ?>>残業集計項目の３０分未満は、切り捨て、３０分以上は切り上げて１時間単位にする</label>
			</font>
	    	</label>
		</td>
	</tr>
	<tr height="">
		<td align="center" width="120">上へ<input type="button" value="↑" onclick="raiseItem();" style="height:2em;width:2em;"><br><br>下へ<input type="button" value="↓" onclick="dropItem();" style="height:2em;width:2em;">
		</td>
		<td>
<select name="csv_item" size="38" multiple style="width:280px;">
<?
$arr_new_list = array();

for ($i=0; $i<count($arr_csv_list); $i++) {
	$tmp_id = $arr_csv_list[$i];
	$tmp_name = $arr_itemname[$tmp_id];
	if ($tmp_name != "") {
		$arr_new_list[] = array("id" => $tmp_id, "name" => $tmp_name);
	}
}
show_csv_options($arr_new_list);

?>
</select>
		</td>
		<td align="left" width="120">
		<table border="0" cellspacing="0" cellpadding="2">
		<tr >
		<td height="140">&nbsp;
		</td>
		</tr>
		<tr >
		<td align="center" nowrap>
		選択<br>
<input type="button" name="select_item" value="←" style="margin-left:2em;width:5.5em;" onclick="selectItem();">&nbsp;&nbsp;
<br><br>
		<input type="button" value="→" style="margin-left:2em;width:5.5em;" onclick="deleteItem();">&nbsp;&nbsp;<br>除外
<br><br>
		<input type="button" value="←" style="margin-left:2em;width:5.5em;" onclick="addEmptyCell();">&nbsp;
		<br>&nbsp;&nbsp;&nbsp;空セル挿入
		</td>
		<td width="30">&nbsp;&nbsp;
		</td>
		</tr>
		<tr >
		<td height="22">&nbsp;
		</td>
		</tr>
		<tr >
		<td align="center" nowrap>
<?
for ($i=0; $i<count($arr_items); $i++) {
	if ($arr_items[$i]["id"] == "1_2") {
		$class_nm = $arr_items[$i]["name"];
	}
	if ($arr_items[$i]["id"] == "1_3") {
		$atrb_nm = $arr_items[$i]["name"];
	}
	if ($arr_items[$i]["id"] == "1_4") {
		$dept_nm = $arr_items[$i]["name"];
		break;
	}
}
//部署項目名
$arr_sortitem = array("", "職員ID", "氏名", "$class_nm", "$atrb_nm", "$dept_nm", "職種");
if ($from_flg == "2") {
    $arr_sortitem = array_merge($arr_sortitem, array("シフトグループ", "シフト作成表示順"));
}

for ($key_idx=1; $key_idx<=5; $key_idx++) {
	
	echo("&nbsp;&nbsp;&nbsp;ソートキー{$key_idx}<br>");
	
	$varname = "sortkey".$key_idx;
	echo("&nbsp;&nbsp;&nbsp;<select name=\"$varname\" id=\"$varname\">");
	$sortkey_val = $arr_config[$varname];
	for ($i=0; $i<count($arr_sortitem); $i++) {
		//選択
		if ($sortkey_val == $i) {
			$selected = " selected";
		} else {
			$selected = "";
		}
		echo("<option value=\"$i\" $selected>");
		
		echo($arr_sortitem[$i]);
		echo("</option>");
	}
	echo("</select><br>");
	
}

?>
		</td>
		</tr>
		</table>
		<table border="0" cellspacing="0" cellpadding="2">
		<tr >
		<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		</font>
		</td>
		</tr>
		</table>

		</td>
		<td>
<select name="csv_item_moto" size="38" multiple style="width:280px;">
<?
show_csv_options($arr_items);
?>
</select>
		</td>
	</tr>
	<tr>
		<td colspan="4" align="right">
		<table align="right">
		<tr>
        <?
if ($from_flg == "2" && phpversion() >= "5.1.6") { 
    $paper_size = $arr_config["paper_size"];
    $orientation = $arr_config["orientation"];
    
    if ($paper_size == "") {
        $paper_size = "A4";
    }
    if ($orientation == "") {
        $orientation = "2";
    }
        ?>
		<td valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
用紙サイズ
<input type="radio" name="paper_size" id="paper_size" value="A4"<? if ($paper_size=="A4") {echo(" checked");} ?>>A4
<input type="radio" name="paper_size" id="paper_size" value="A3"<? if ($paper_size=="A3") {echo(" checked");} ?>>A3
<input type="radio" name="paper_size" id="paper_size" value="B4"<? if ($paper_size=="B4") {echo(" checked");} ?>>B4
&nbsp;
<br>
用紙方向
<input type="radio" name="orientation" id="orientation" value="1"<? if ($orientation=="1") {echo(" checked");} ?>>縦
<input type="radio" name="orientation" id="orientation" value="2"<? if ($orientation=="2") {echo(" checked");} ?>>横

		</font>
		</td>
<? } ?>
		<td valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<a href="javascript:void(0);" onclick="totalitem();">集計項目１</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<br>
		<a href="javascript:void(0);" onclick="totalitem2(2);">集計項目２</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<br>
		<a href="javascript:void(0);" onclick="totalitem2(3);">集計項目３</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<br>
		<a href="javascript:void(0);" onclick="totalitem2(4);">集計項目４</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<br>
		<a href="javascript:void(0);" onclick="totalitem2(5);">集計項目５</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</font>
		</td>
		<td valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<a href="javascript:void(0);" onclick="totalitem2(6);">集計項目６</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<br>
		<a href="javascript:void(0);" onclick="totalitem2(7);">集計項目７</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<br>
		<a href="javascript:void(0);" onclick="totalitem2(8);">集計項目８</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<br>
		<a href="javascript:void(0);" onclick="traitem();">交通費の設定</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<br>
		</font>
		</td>
		<td align="right">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<a href="javascript:void(0);" onclick="jobidset();">休日出勤日数職種の設定</a><br>
		<a href="javascript:void(0);" onclick="csvconfig();">項目出力形式設定</a><br>
		<a href="javascript:void(0);" onclick="outputtarget();">勤務時間の設定</a><br>
		<a href="javascript:void(0);" onclick="ptnidset();">休日出勤日数勤務の設定</a><br>
		<a href="javascript:void(0);" onclick="fixedset();">所定時間の設定</a><br>
		</font>
		</td></tr></table>
		</td>
	</tr>
	</table>

		</td>
	</tr>
	</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="postback" value="">
		<input type="hidden" name="csv_list" value="">
		<input type="hidden" name="layout_id" value="<? echo($layout_id); ?>">
		<input type="hidden" name="csv_ext_name" value="">
		<input type="hidden" name="shift_group_id" value="<? echo($shift_group_id); ?>">
		<input type="hidden" name="from_flg" value="<? echo($from_flg); ?>">
		<input type="hidden" name="yyyymm" value="<? echo($yyyymm); ?>">
		<input type="hidden" name="mmode" value="<? echo($mmode); ?>">
	</form>

	<form name="csv" method="post" target="download">
	<? // target="download" ?>
	<input type="hidden" name="session" value="<? echo($session); ?>">
	<input type="hidden" name="csv_list" value="">
	<input type="hidden" name="emp_id_list" value="">
	<input type="hidden" name="yyyymm" value="">
	<input type="hidden" name="header_flg" value="">
	<input type="hidden" name="csv_ext_name" value="">
	<input type="hidden" name="zerodata_no_out_flg" value="">
	<input type="hidden" name="fraction_flg" value="">
	<input type="hidden" name="shift_group_id" value="<? echo($shift_group_id); ?>">
	<input type="hidden" name="csv_layout_id" value="<? echo($hid_layout_id); ?>">
	<input type="hidden" name="group_id" value="">
	<input type="hidden" name="srch_name" value="">
	<input type="hidden" name="cls" value="">
	<input type="hidden" name="atrb" value="">
	<input type="hidden" name="dept" value="">
	<input type="hidden" name="duty_form_jokin" value="">
	<input type="hidden" name="duty_form_hijokin" value="">
	<input type="hidden" name="select_start_yr" value="">
	<input type="hidden" name="select_start_mon" value="">
	<input type="hidden" name="select_start_day" value="">
	<input type="hidden" name="select_end_yr" value="">
	<input type="hidden" name="select_end_mon" value="">
	<input type="hidden" name="select_end_day" value="">
	<input type="hidden" name="sortkey1" value="">
	<input type="hidden" name="sortkey2" value="">
	<input type="hidden" name="sortkey3" value="">
	<input type="hidden" name="sortkey4" value="">
	<input type="hidden" name="sortkey5" value="">
	<input type="hidden" name="sus_flg" value="">
	<input type="hidden" name="mmode" value="<? echo($mmode); ?>">
	</form>
	<iframe name="download" width="0" height="0" frameborder="0"></iframe>

</body>
<? pg_close($con); ?>
</html>
<?
function show_csv_options($arr_items) {
	foreach ($arr_items as $row) {
		echo("<option value=\"{$row["id"]}\">{$row["name"]}\n");
	}
}

//CSVレイアウトの項目更新
function update_csv_list($con, $fname, $arr_csv_list, $csv_header_flg, $csv_ext_name, $layout_id, $layout_name, $sortkey1, $sortkey2, $sortkey3, $sortkey4, $sortkey5, $from_flg, $paper_size, $orientation, $zerodata_no_out_flg, $fraction_flg) {

///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
	pg_query($con, "begin transaction");
	
    //CSVレイアウト調整
    if ($from_flg != "2") {
        $tablename = "timecard_csv_layout_item";
        $tablename_config = "timecard_csv_config";
        $wk_layout_id = "'$layout_id'";
    }
    //帳票定義
    else {
        $tablename = "duty_shift_mprint_layout_item";
        $tablename_config = "duty_shift_mprint_config";
        $wk_layout_id = "$layout_id";
    }
    $sql = "delete from $tablename";
    $cond = "where layout_id = $wk_layout_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	for ($i=0; $i<count($arr_csv_list); $i++) {

        $sql = "insert into $tablename (layout_id, no, id";
		$sql .= ") values (";
		$content = array($layout_id, $i+1, $arr_csv_list[$i]);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

//CSVヘッダーフラグ
	if ($csv_header_flg == "") {
		$csv_header_flg = "f";
	}
	if ($zerodata_no_out_flg == "") {
		$zerodata_no_out_flg = "f";
	}
	if ($fraction_flg == "") {
		$fraction_flg = "f";
	}
	//レイアウト別設定
    $sql = "select * from $tablename_config";
	$cond = "where layout_id = $wk_layout_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if (pg_num_rows($sel) > 0) {
        $sql = "update $tablename_config set";
		$set = array("csv_header_flg", "csv_ext_name", "sortkey1", "sortkey2", "sortkey3", "sortkey4", "sortkey5", "zerodata_no_out_flg", "fraction_flg");
        if ($from_flg == "2") {
            $set = array_merge($set, array("paper_size", "orientation"));
        }
        $setvalue = array($csv_header_flg, $csv_ext_name, $sortkey1, $sortkey2, $sortkey3, $sortkey4, $sortkey5, $zerodata_no_out_flg, $fraction_flg);
        if ($from_flg == "2") {
            $setvalue = array_merge($setvalue, array($paper_size, $orientation));
        }
        $cond = "where layout_id = $wk_layout_id";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	} else {
		
        $sql = "insert into $tablename_config (csv_header_flg, csv_ext_name, layout_id, sortkey1, sortkey2, sortkey3, sortkey4, sortkey5, zerodata_no_out_flg, fraction_flg";
        if ($from_flg == "2") {
            $sql .= ", paper_size, orientation";
        }
		$sql .= ") values (";
		
		$content = array($csv_header_flg, $csv_ext_name, $layout_id, $sortkey1, $sortkey2, $sortkey3, $sortkey4, $sortkey5, $zerodata_no_out_flg, $fraction_flg);
        if ($from_flg == "2") {
            $content = array_merge($content, array($paper_size, $orientation));
        }
        $ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}	
	
	//レイアウト名称更新
    if ($from_flg != "2") {
        $sql = "select * from timecard_csv_layout_mst";
        $cond = "where layout_id = '$layout_id' ";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_numrows($sel);
        
        if ($num > 0) {
            $sql = "update timecard_csv_layout_mst set";
            $set = array("layout_name");
            $setvalue = array($layout_name);
            $cond = "where layout_id = '$layout_id'";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_close($con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
                exit;
            }
        } else {
            $sql = "insert into timecard_csv_layout_mst (layout_id, layout_name";
            $sql .= ") values (";
            $content = array($layout_id, $layout_name);
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }
	
///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
	pg_query("commit");
}

//CSVヘッダーフラグ取得
function get_csv_header_flg($con, $fname) {

	$ret_flg = "f";
	
	$sql = "select csv_header_flg from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);

	if ($num > 0) {
		$ret_flg = pg_result($sel,0,"csv_header_flg");
		if ($ret_flg == "") {
			$ret_flg = "f";
		}
	}
	return $ret_flg;
}

/**
 *※未使用、勤務シフト作成＞管理帳票でもCSV出力するため、work_admin_csv_common.ini::get_timecard_csv_configが新しい関数
 * CSV設定取得
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @return mixed CSV設定の配列、csv_header_flg、csv_ext_name
 *
 */
function get_csv_setting($con, $fname) {
	
	$ret_flg = "f";
	
	$sql = "select csv_header_flg,csv_ext_name from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);
	
	$arr_data = array();
	if ($num > 0) {
		$csv_header_flg = pg_result($sel,0,"csv_header_flg");
		$csv_ext_name = pg_result($sel,0,"csv_ext_name");
	}
	if ($csv_header_flg == "") {
		$csv_header_flg = "f";
	}
	if ($csv_ext_name == "") {
		$csv_ext_name = "csv";
	}
	$arr_data["csv_header_flg"] = $csv_header_flg;
	$arr_data["csv_ext_name"] = $csv_ext_name;
	
	return $arr_data;
}


?>
