<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>GPLライセンス | GPLライセンス</title>
<?
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="gpl_menu.php?session=<? echo($session); ?>"><img src="img/icon/b31.gif" width="32" height="32" border="0" alt="GPLライセンス"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="gpl_menu.php?session=<? echo($session); ?>"><b>GPLライセンス</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" style="margin-top:10px;">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">下記機能において、GPLおよびLGPLライセンスのソフトウェアを使用しております。</font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="200"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">機能</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ソフトウェア</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ウェブメール</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">SquirrelMail</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">WYSIWYGエディタ</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">TinyMCE</font></td>
</tr>
<?
//XX <tr height="22">
//XX <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コミュニティサイト</font></td>
//XX <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">XOOPS</font></td>
//XX </tr>
?>
</table>
</td>
</tr>
</table>
</body>
</html>
