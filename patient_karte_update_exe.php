<?
require("about_session.php");
require("about_authority.php");
require("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 患者管理各権限の取得
$checkauth = check_authority($session, 22, $fname);

// 権限のチェック
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$profile_type = get_profile_type($con, $fname);

// 入力チェック
foreach ($karte_cds as $tmp_karte_cd) {
	if (strlen($tmp_karte_cd) > 20) {
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('{$_label_by_profile["KARTE_NO"][$profile_type]}「{$tmp_karte_cd}」は長すぎます。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}

// トランザクションを開始
pg_query($con, "begin");

// カルテ情報をDELETE
$sql = "delete from ptkarte";
$cond = "where ptif_id = '$pt_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// カルテ情報をINSERT
$sql = "insert into ptkarte values (";
$content = $karte_cds;
array_unshift($content, $pt_id);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$karte_cds = array();
for ($i = 1; $i <= 20; $i++) {
	$karte_cds[$i] = pg_fetch_result($sel, 0, "karte_cd$i");
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// カルテ番号画面を再表示
$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);
echo("<script type=\"text/javascript\">location.href = 'patient_karte.php?session=$session&pt_id=$pt_id&key1=$url_key1&key2=$url_key2&page=$page';</script>");
?>
