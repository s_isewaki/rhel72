<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>ネットカンファレンス | ネットカンファレンス</title>
<?
//------------------------------------------------------------------------------
// Title     :  ネットカンファレンス画面
// File Name :  net_conference.php
// History   :  2004/07/16 - リリース（岩本隆史）
// Copyright :
//------------------------------------------------------------------------------
require("about_session.php");
require("about_authority.php");
require("get_values.ini");

//==============================================================================
// 共通処理
//==============================================================================

$fname = $PHP_SELF;  // スクリプトパス

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ネットカンファレンス権限のチェック
$auth_nc = check_authority($session, 10, $fname);
if ($auth_nc == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================================================================
// 入室可能チェック
//==============================================================================

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// ネットカンファレンス情報取得SQLの実行
$sql_nmrm = "select nmrm_start, nmrm_end, nmrm_lmt_flg, nmrm_end_flg, nmrm_del_flg from nmrmmst";
$cond_nmrm = "where nmrm_id = '$nmrm_id'";
$sel_nmrm = select_from_table($con, $sql_nmrm, $cond_nmrm, $fname);
if ($sel_nmrm == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$nmrm_lmt_flg = pg_fetch_result($sel_nmrm, 0, "nmrm_lmt_flg");

// 制限フラグが立っていたらネットカンファレンスメンバーかどうかチェック
if ($nmrm_lmt_flg == 't') {
	$sql_nmme = "select count(*) from nmmemst";
	$cond_nmme = "where nmrm_id = '$nmrm_id' and emp_id = '$emp_id'";
	$sel_nmme = select_from_table($con, $sql_nmme, $cond_nmme, $fname);
	if ($sel_nmme == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	if (pg_fetch_result($sel_nmme, 0, 0) == 0) {
		pg_close($con);
		echo("<script language='javascript'>alert('メンバー以外は入室できません');");
		echo("history.back();</script>");
		exit;
	}
}

// データベース接続をクローズ
pg_close($con);

$now = date("YmdHi");
$nmrm_start = pg_fetch_result($sel_nmrm, 0, "nmrm_start");
$nmrm_end = pg_fetch_result($sel_nmrm, 0, "nmrm_end");
$nmrm_end_flg = pg_fetch_result($sel_nmrm, 0, "nmrm_end_flg");
$nmrm_del_flg = pg_fetch_result($sel_nmrm, 0, "nmrm_del_flg");

// 開始日時
if ($nmrm_start > $now) {
	echo("<script language='javascript'>alert('カンファレンスは開始されていません');");
	echo("history.back();</script>");
	exit;
}

// 終了日時・終了フラグ
if (($nmrm_end <= $now) || ($nmrm_end_flg == "t")) {
	echo("<script language='javascript'>alert('カンファレンスは終了しました');");
	echo("history.back();</script>");
	exit;
}

// 削除フラグ
if ($nmrm_del_flg == "t") {
	echo("<script language='javascript'>alert('カンファレンスは中止されました');");
	echo("history.back();</script>");
	exit;
}

//==============================================================================
// HTML
//==============================================================================
?>
<script type="text/javascript">
function exitRoom() {
	document.exit.action = 'net_conference_menu.php';
	document.exit.submit();
}

function closeRoom() {
	var cfm = confirm('カンファレンスを終了します。よろしいですか？');
	if (cfm) {
		document.exit.action = 'net_conference_end.php';
		document.exit.submit();
	}
}
</script>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" colspan="2" class="spacing"><a href="net_conference_menu.php?session=<? echo($session); ?>"><img src="img/icon/b09.gif" width="32" height="32" border="0" alt="ネットカンファレンス"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="net_conference_menu.php?session=<? echo($session); ?>"><b>ネットカンファレンス</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="160" height="22" align="center" bgcolor="#5279a5"><a href="net_conference_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>ネットカンファレンス一覧</b></font></a></td>
<td width=""></td>
</tr>
<tr>
<td bgcolor="#5279a5" colspan="2"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
<tr>
<!--
<td><iframe style="BORDER-RIGHT: silver 0px dotted; BORDER-TOP: silver 0px dotted; BORDER-LEFT: silver 0px dotted; BORDER-BOTTOM: silver 0px dotted" name="chatframe" topmargin="0" leftmargin="0" margin="0" marginheight="0" marginwidth="0 src="chat.php?session=<? echo $session; ?>&nmrm_id=<? echo $nmrm_id ?>&emp_id=<? echo $emp_id; ?>" width="600" height="300" scrolling="yes""></iframe></td>
-->
<td><iframe style="BORDER-RIGHT: silver 0px dotted; BORDER-TOP: silver 0px dotted; BORDER-LEFT: silver 0px dotted; BORDER-BOTTOM: silver 0px dotted" name="chatframe" topmargin="0" leftmargin="0" margin="0" marginheight="0" marginwidth="0" src="chat.php?session=<? echo $session; ?>&nmrm_id=<? echo $nmrm_id ?>&emp_id=<? echo $emp_id; ?>" width="600" height="300" scrolling="yes"></iframe></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="22"><form name="mainform" target="chatframe" action="chat.php" method="post">
<input type="text" name="ntmt_msg" size="80" style="ime-mode: active;">&nbsp;<input type="submit" value="発言">
<input type="hidden" name="session" value="<? echo $session; ?>">
<input type="hidden" name="nmrm_id" value="<? echo $nmrm_id; ?>">
<input type="hidden" name="emp_id" value="<? echo $emp_id; ?>">
</form></td>
<td align="right" nowrap>
<form name="exit" method="post">
<input type="button" value="退室" onclick="exitRoom();">&nbsp;<input type="button" value="終了" onclick="closeRoom();"><input type="hidden" name="session" value="<? echo $session; ?>">
<input type="hidden" name="nmrm_id" value="<? echo $nmrm_id; ?>">
</form></td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>
