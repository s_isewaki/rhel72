<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="cas_application_apply_list.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="category" value="<?echo($category)?>">
<input type="hidden" name="workflow" value="<?echo($workflow)?>">
<input type="hidden" name="apply_title" value="<?echo($apply_title)?>">
<input type="hidden" name="approve_emp_nm" value="<?echo($approve_emp_nm)?>">
<input type="hidden" name="date_y1" value="<?echo($date_y1)?>">
<input type="hidden" name="date_m1" value="<?echo($date_m1)?>">
<input type="hidden" name="date_d1" value="<?echo($date_d1)?>">
<input type="hidden" name="date_y2" value="<?echo($date_y2)?>">
<input type="hidden" name="date_m2" value="<?echo($date_m2)?>">
<input type="hidden" name="date_d2" value="<?echo($date_d2)?>">
<input type="hidden" name="apply_stat" value="<?echo($apply_stat)?>">
</form>


<?
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("cas_application_workflow_common_class.php");
require_once("cas_common.ini");


$fname=$PHP_SELF;


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 決裁・申請権限のチェック
$checkauth = check_authority($session, $CAS_MENU_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}


// データベースに接続
$con = connect2db($fname);

$obj = new cas_application_workflow_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザ取得
$arr_empmst = $obj->get_empmst($session);
$login_emp_id = $arr_empmst[0]["emp_id"];

$arr_next_appry_id = array();
$count = count($re_apply_chk);
for ($i = 0; $i < $count; $i++) {
	if ($re_apply_chk[$i] != "") {

		$apply_id = $re_apply_chk[$i];

		// 再申請チェック
		$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
		$apply_stat = $arr_apply_wkfwmst[0]["apply_stat"];
		if($apply_stat != "3") {
			echo("<script type=\"text/javascript\">alert(\"申請状況が変更されたため、再申請できません。\");</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}

		// 新規申請(apply_id)採番
		$sql = "select max(apply_id) from cas_apply";
		$cond = "";
		$apply_max_sel = select_from_table($con,$sql,$cond,$fname);
		$max = pg_result($apply_max_sel,0,"max");
		if($max == ""){
			$new_apply_id = "1";
		}else{
			$new_apply_id = $max + 1;
		}


		// 申請登録
		$obj->regist_re_apply($new_apply_id, $apply_id, "", "", "LIST");

		// 承認登録
		$obj->regist_re_applyapv($new_apply_id, $apply_id);

		// 承認者候補登録
		$obj->regist_re_applyapvemp($new_apply_id, $apply_id);

		// 添付ファイル登録
		$obj->regist_re_applyfile($new_apply_id, $apply_id);

		// 非同期・同期受信登録
		$obj->regist_re_applyasyncrecv($new_apply_id, $apply_id);

		// 申請結果通知登録
		$obj->regist_re_applynotice($new_apply_id, $apply_id);

		// 前提とする申請書(申請用)登録
		$obj->regist_re_applyprecond($new_apply_id, $apply_id);

		// 元の申請書に再申請ＩＤを更新
		$obj->update_re_apply_id($apply_id, $new_apply_id);

        $short_wkfw_name = $arr_apply_wkfwmst[0]["short_wkfw_name"];
        // 評価申請の場合
        if($obj->is_eval_template($short_wkfw_name))
        {
        	if($obj->is_eval_level2_template($short_wkfw_name))
        	{
                $level = "2";
	        }
	        else if($obj->is_eval_level3_template($short_wkfw_name))
            {
                $level = "3";
            }

            $application_book_apply_id = $obj->get_cas_recognize_schedule_apply_id($apply_id, $short_wkfw_name);
            if($application_book_apply_id > 0)
            {
                $arr_eval_apply_id = $obj->create_arr_eval_apply_id($application_book_apply_id, $new_apply_id, $short_wkfw_name, "ADD");
   			    $obj->update_cas_recognize_schedule_for_apply_id($arr_eval_apply_id);
            }
        }

  	    // レベルアップ申請の場合
	    if($obj->is_levelup_apply_template($short_wkfw_name))
	    {
            $apply_content = $arr_apply_wkfwmst[0]["apply_content"];
	    	$levelup_application_book_id = $obj->get_levelup_application_book_id($apply_content);
            $obj->update_levelup_apply_id($levelup_application_book_id, $new_apply_id);
	    }

		array_push($arr_next_appry_id, $new_apply_id);
	}
}



// 添付ファイルをコピー
// 添付ファイルの確認
if (!is_dir("cas_apply")) {
	mkdir("cas_apply", 0755);
}
for ($i = 0; $i < $count; $i++) {
	if ($re_apply_chk[$i] != "") {

		$apply_id = $re_apply_chk[$i];
		$sql = "select * from cas_applyfile";
		$cond = "where apply_id = $apply_id order by apply_id asc";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if($sel==0){
			pg_exec($con,"rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {

			$fine_no = $row["applyfile_no"];
			$file_name = $row["applyfile_name"];
			$ext = strrchr($file_name, ".");

			$tmp_filename = "cas_apply/{$apply_id}_{$fine_no}{$ext}";
			copy($tmp_filename, "cas_apply/{$arr_next_appry_id[$i]}_{$fine_no}{$ext}");
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 一覧画面に遷移
echo("<script type=\"text/javascript\">document.items.submit();</script>");

?>
</body>