<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require_once("about_comedix.php");
require_once("show_kanja_joho.ini");
require_once("summary_common.ini");
require_once("show_problem_input_form.ini");
require_once("summary_ymd_select_util.ini");
require_once("show_select_values.ini");
require_once("get_menu_label.ini");
require_once("label_by_profile_type.ini");
require_once("yui_calendar_util.ini");

// ページ名
$fname = $PHP_SELF;

//==============================
// パラメータを取得
//==============================
$session     = isset($_REQUEST['session'])  ? $_REQUEST['session']  : '';        // セッション
$mode        = isset($_REQUEST['mode'])     ? $_REQUEST['mode']     : 'insert';  // モード(insert:新規登録(デフォルト)/update:更新)
$pt_id       = isset($_REQUEST['pt_id'])    ? $_REQUEST['pt_id']    : '';        // 患者ID
$goal_id     = isset($_REQUEST['goal_id'])  ? $_REQUEST['goal_id']  : '';        // 主目標ID
$is_postback = isset($_POST['is_postback']) ? $_POST['is_postback'] : false;     // ポストバック処理フラグ

$data['reg_date'] = isset($_POST['reg_date']) ? $_POST['reg_date'] : '';         // 登録日
if ($is_postback === false && $mode == 'insert') {
    // 新規登録の場合は、登録日は当日日付
    $data['reg_date'] = date('Ymd');
}
$data['main_goal'] = isset($_POST['main_goal']) ? $_POST['main_goal'] : '';      // 主目標

//==============================
// セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

//==============================
// DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
// 表示文言切り替え
//==============================
$submit_name = ($mode == 'update') ? '更新' : '登録';

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ポストバック時の処理
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if ($is_postback == 'true') {
    if ($mode == 'insert' || $mode == 'update') {
        $data['main_goal'] = pg_escape_string($data['main_goal']);

        // 重複チェック
        $count = isExistMainGoalData($con, $goal_id, $pt_id, $data['reg_date'], $fname);
        if ($count === false) {
            pg_close($con);
            echo "<script type=\"text/javascript\">alert('重複チェックに失敗しました。'); window.close();</script>\n";
            exit;
        }

        if ($count !== 0) {
            pg_close($con);
            echo "<script type=\"text/javascript\">alert('指定された登録年月日のデータは既に存在しています。'); history.back();</script>\n";
            exit;
        }
    }

    if ($mode == 'insert') {          // 登録
        $result = insertMainGoalData($con, $pt_id, $data, $fname);
    } elseif ($mode == 'update') {    // 更新
        $result = updateMainGoalData($con, $goal_id, $data, $fname);
    } elseif ($mode == 'delete') {    // 削除
        $result = deleteMainGoalData($con, $goal_id, $fname);
    }
    pg_close($con);
    
    if ($result === false) {
        echo "<script type=\"text/javascript\" src=\"js/showpage.js\"></script>";
        echo "<script type=\"text/javascript\">showErrorPage(window);</script>";
        exit;
    }

    echo "<script language='javascript'>";
    echo "window.opener.location.reload();";
    echo "window.close();";
    echo "</script>";
    exit;
} elseif ($mode == 'update') {    // 更新画面表示
    if (($data = getEachMainGoalData($con, $goal_id, $fname)) === false) {
        pg_close($con);
        echo "<script type=\"text/javascript\" src=\"js/showpage.js\"></script>";
        echo "<script type=\"text/javascript\">showErrorPage(window);</script>";
        exit;
    }
}

$menuLabel = get_report_menu_label($con, $fname);

pg_close($con);    // DB切断

/**
 * 主目標履歴データを取得する
 * 
 * @param resource $con     データベース接続リソース
 * @param integer  $goal_id 主目標ID
 * @param string   $fname   ページ名
 * 
 * @return mixed 成功時:取得したレコードの配列/結果の行数が 0だった場合、又はその他のエラーが発生:false
 */
function getEachMainGoalData($con, $goal_id, $fname)
{
    $sql = "SELECT * FROM sum_main_goal WHERE goal_id = {$goal_id}";
    $sel = select_from_table($con, $sql, '', $fname);
    
    if (!$sel) {
        return false;
    }
    $data = pg_fetch_assoc($sel);
    return $data;
}

/**
 * 主目標履歴データを新規登録する
 * 
 * @param resource $con   データベース接続リソース
 * @param string   $pt_id 患者ID
 * @param array    $data  登録するデータ
 * @param string   $fname ページ名
 * 
 * @return boolean 成功時:true/失敗時:false
 */
function insertMainGoalData($con, $pt_id, $data, $fname)
{
    $sql = "INSERT INTO sum_main_goal (ptif_id, main_goal, reg_date) VALUES ('{$pt_id}', '{$data['main_goal']}', '{$data['reg_date']}')";
    $in = insert_into_table_no_content($con, $sql, $fname);
    if (!$in) {
        return false;
    }
    return true;
}

/**
 * 主目標履歴データを更新する
 * 
 * @param resource $con     データベース接続リソース
 * @param integer  $goal_id 主目標ID
 * @param array    $data    更新するデータ
 * @param string   $fname   ページ名
 * 
 * @return boolean 成功時:true/失敗時:false
 */
function updateMainGoalData($con, $goal_id, $data, $fname)
{
    $sql = "UPDATE sum_main_goal SET";
    $cond = "WHERE goal_id = {$goal_id}";
    $upd = update_set_table($con, $sql, array('main_goal', 'reg_date'), array($data['main_goal'], $data['reg_date']), $cond, $fname);
    if (!$upd) {
        return false;
    }
    return true;
}

/**
 * 主目標履歴データを削除する
 * 
 * @param resource $con     データベース接続リソース
 * @param integer  $goal_id 主目標ID
 * @param string   $fname   ページ名
 * 
 * @return boolean 成功時:true/失敗時:false
 */
function deleteMainGoalData($con, $goal_id, $fname)
{
    $sql = "DELETE FROM sum_main_goal";
    $cond = "WHERE goal_id = {$goal_id}";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if (!$del) {
        return false;
    }
    return true;
}

/**
 * 患者IDと日付を指定して、データの存在有無をチェックする
 * 
 * @param resource $con      データベース接続リソース
 * @param integer  $goal_id  主目標ID
 * @param string   $pt_id    患者ID
 * @param string   $reg_date 登録日
 * @param string   $fname    ページ名
 * 
 * @return mixed 成功時:存在有無(0:存在しない/1:存在する)/失敗時:false
 */
function isExistMainGoalData($con, $goal_id, $pt_id, $reg_date, $fname)
{
    $sql = "SELECT COUNT(*) FROM sum_main_goal";
    
    // 患者IDと登録日が同じ
    $cond = "WHERE ptif_id = '{$pt_id}' AND reg_date = '{$reg_date}'";
    if ($goal_id !== '') {
        // 主目標IDが指定されていれば、「主目標IDが異なる」条件を追加(内容の更新はOKなので)
        $cond .= " AND goal_id <> {$goal_id}";
    }
    $sel = select_from_table($con, $sql, $cond, $fname);
    if (!$sel) {
        return false;
    }
    $count = pg_fetch_result($sel, 0, 0);
    if ($count == 0) {
        return 0;
    } else {
        return 1;
    }
}
?>

<title><? echo $menuLabel; ?>｜主目標<? echo $submit_name; ?></title>
<script type="text/javascript">
/****************************************************************
 * save()
 * 保存(insert/update)
 *
 * 引　数：mode モード
 *         goal_id 主目標ID
 * 戻り値：なし
 * 
 * 2012/09/12 追加
 ****************************************************************/
function save(mode, goal_id) {
  // 登録年月日文字列を取得
  var str_year  = document.getElementById('date_y1').value;
  var str_month = document.getElementById('date_m1').value;
  var str_day   = document.getElementById('date_d1').value;

  // 未入力チェック
  if (str_year == '-' && str_month == '-' && str_day == '-') {
    alert('登録年月日が未入力です。');
    return;
  }

  if (document.maingoal_form.main_goal.value == '') {
    alert('内容が未入力です。');
    return;
  }

  // 日付の妥当性チェック
  var year  = Number(str_year);
  var month = Number(str_month);
  var day   = Number(str_day);
  
  if (!isValidDate(year, month, day)) {
    alert('登録年月日が正しくありません。');
    return;
  }
    
  // 登録年月日をフォーム要素に追加
  var reg_date = year + ('0' + month).slice(-2) + ('0' + day).slice(-2);
  
  var tag = document.createElement('input');
  tag.setAttribute('type', 'hidden');
  tag.setAttribute('name', 'reg_date');
  tag.setAttribute('value', reg_date);
  document.maingoal_form.appendChild(tag);
  
  document.maingoal_form.action += '?mode=' + mode;
  if (mode == 'update') {
    document.maingoal_form.action += '&goal_id=' + goal_id;
  }

  // データを更新(このスクリプトにsubmit)
  document.maingoal_form.submit();
}

/****************************************************************
 * del()
 * 削除
 *
 * 引　数：goal_id 主目標ID
 * 戻り値：なし
 * 
 * 2012/09/12 追加
 ****************************************************************/
function del(goal_id) {
  if (!confirm('削除してよろしいですか？')) {
    return;
  }

  document.maingoal_form.action += '?mode=delete&goal_id=' + goal_id;
  document.maingoal_form.submit();
}

/****************************************************************
 * isValidDate()
 * 日付の妥当性をチェック
 *
 * 引　数：year 年
 *         month 月
 *         day 日
 * 戻り値：true:妥当/false:妥当でない
 * 
 * 2012/09/12 追加
 ****************************************************************/
function isValidDate(year, month, day) {
  // 日付オブジェクトを生成
  var date = new Date(year, month - 1, day);

  // 年月日を生成した日付オブジェクトから取得
  var checkY = date.getFullYear();
  var checkM = date.getMonth() + 1;
  var checkD = date.getDate();

  // 指定された年月日と比較
  if (year != checkY || month != checkM || day != checkD) {
    return false;
  } else {
    return true;
  }
}

/****************************************************************
 * checkOnKeyPress()
 * キー押下時の入力文字数チェック
 *
 * 引　数：obj チェック対象要素
 *         max 最大入力文字数
 * 戻り値：OK:true/NG:false
 * 
 * 2012/09/20 追加
 ****************************************************************/
function checkOnKeyPress(obj, max) {
  var value = obj.value;
  if (value.length > max) {
    return false;
  }
  return true;
}

/****************************************************************
 * checkOnKeyUp()
 * キー押下時の入力文字数チェック
 *
 * 引　数：obj チェック対象要素
 *         max 最大入力文字数
 * 戻り値：OK:true/NG:false
 * 
 * 2012/09/20 追加
 ****************************************************************/
function checkOnKeyUp(obj, max) {
  var value = obj.value;
  if (value.length > max) {
    alert('５０文字まで入力可能です。');
    obj.value = obj.value.substring(0, max);
    return false;
  }
  return true;
}

<?
// カレンダーJavaScript
write_common_ymd_ctl_script();
?>
</script>
<?
// カレンダー外部ファイル
write_yui_calendar_use_file_read_0_12_2();
write_yui_calendar_script2(1);
?>
</head>
<body onload="javascript:if (document.getElementById('cal1Container')) {initcal();}">
<div style="padding:4px">
<?
// ヘッダー 出力
summary_common_show_dialog_header("主目標{$submit_name}");

//入力フィールド 出力
?>
<div style="margin-top:8px">
  <form name="maingoal_form" action="summary_maingoal_input.php" method="post">
    <table class="prop_table" width="100%" cellspacing="1">
      <tr>
        <th width="130">登録年月日</th>
        <td>
          <nobr>
          <select id="date_y1" name="selectyy"><? show_select_years(10, substr($data['reg_date'], 0, 4), true); ?></select>年
          <select id="date_m1" name="selectmo"><? show_select_months(substr($data['reg_date'], 4, 2), true); ?></select>月
          <select id="date_d1" name="selectdd"><? show_select_days(substr($data['reg_date'], 6, 2), true); ?></select>日
          <img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"><div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
          &nbsp;
          <input type="button" onclick="clear_ymd_select(document.maingoal_form.date_y1,document.maingoal_form.date_m1,document.maingoal_form.date_d1);update_calendar_ymd_from_select__start_date();" value="クリア"/>
          </nobr>
        </td>
      </tr>
      <tr>
        <th>内容</th>
        <td>
          <textarea id="main_goal" name="main_goal" style="width: 400px; height: 40px; resize: none; ime-mode: active; overflow: hidden;" onkeyup="checkOnKeyUp(this, 50);" onkeypress="checkOnKeyPress(this, 50)"><? echo h($data['main_goal']); ?></textarea>
        </td>
      </tr>
    </table>

    <div class="search_button_div">
<? if ($mode == 'insert') { ?>
      <input type="button" onclick="save('insert', '');" value="登録"/>
<? } else { ?>
      <input type="button" onclick="del('<? echo $goal_id; ?>');" value="削除"/>
      <input type="button" onclick="save('update', '<? echo $goal_id; ?>');" value="更新"/>
<? } ?>
    </div>
    <input type="hidden" name="session" value="<? echo $session; ?>">
    <input type="hidden" name="pt_id" value="<? echo $pt_id; ?>">
    <input type="hidden" name="goal_id" value="<? echo $goal_id; ?>">
    <input type="hidden" name="is_postback" value="true">
  </form>
</div>

</div>
</div>
</body onload="javascript:if (document.getElementById('cal1Container')) {initcal();}">
</html>
