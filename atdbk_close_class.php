<?php
require_once("about_postgres.php");

class atdbk_close_class
{

	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション

	var $closing = "";
	var $closing_parttime = "";
	var $closing_month_flg = "";
	//0:未使用 1:1日 2:5日 3:10日 4:15日 5:20日 6:月末
	var $arr_closing_day = array("", "1", "5", "10", "15", "20", "99");
	
	var $arr_duty_form = array();

	var $arr_close_stat = array();
	var $arr_closing = array();

	/*************************************************************************/
	// コンストラクタ
	// @param object $con DBコネクション
	// @param string $fname 画面名
	/*************************************************************************/
	function atdbk_close_class($con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		// 全項目リストを初期化
		
		$this->get_closing_info();
	}
	
	//締め日情報取得
	function get_closing_info() {
		$sql = "select closing, closing_parttime, closing_month_flg from timecard";
		$cond = "";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0) {
			$this->closing = pg_fetch_result($sel, 0, "closing");
			$this->closing_parttime = pg_fetch_result($sel, 0, "closing_parttime");
			if ($this->closing_parttime == "") {
				$this->closing_parttime = $closing;
			}
			$this->closing_month_flg = pg_fetch_result($sel, 0, "closing_month_flg");
			if ($this->closing_month_flg == "") {
				$this->closing_month_flg = "1";
			}
		}
	}

	//指定職員の雇用・勤務形態取得
	function get_duty_form($emp_id) {
	
		if ($this->arr_duty_form[$emp_id] != "") {
			return $this->arr_duty_form[$emp_id];
		}
		
		$sql = "select duty_form from empcond";
		$cond = "where emp_id = '$emp_id'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$duty_form = "";
		if (pg_num_rows($sel) > 0) {
			$duty_form = pg_fetch_result($sel, 0, "duty_form");
		}
		$this->arr_duty_form[$emp_id] = $duty_form;
		return $duty_form;

	}
	
	//締め状況取得、職員、日付（期間の開始日、終了日は省略可）
	function get_close_stat($emp_id, $start_date, $end_date="") {
	
		$target_yyyymm = $this->get_target_month($emp_id, $start_date);
	
		if ($this->arr_close_stat[$emp_id][$target_yyyymm] != "") {
			return "1";
		}
		
		if ($end_date != "") {
			$target_yyyymm2 = $this->get_target_month($emp_id, $end_date);
		
			if ($this->arr_close_stat[$emp_id][$target_yyyymm2] != "") {
				return "1";
			}
		}
	
		$sql = "select count(*) as cnt from atdbkclose";
		$cond = "where emp_id = '$emp_id' and yyyymm = '$target_yyyymm'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$cnt = pg_fetch_result($sel, 0, "cnt");
		if ($cnt > 0) {
			$ret = "1";
			$this->arr_close_stat[$emp_id][$target_yyyymm] = "1";
		}
		else {
			$ret = "0";
		}
		
		//終了側の対象月が違う場合は再確認
		if ($ret == false && $target_yyyymm != $target_yyyymm2) {
			$sql = "select count(*) as cnt from atdbkclose";
			$cond = "where emp_id = '$emp_id' and yyyymm = '$target_yyyymm2'";
			$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			$cnt = pg_fetch_result($sel, 0, "cnt");
			if ($cnt > 0) {
				$ret = "1";
				$this->arr_close_stat[$emp_id][$target_yyyymm2] = "1";
			}
		}
		
		return $ret;
	}
	
	//対象日から月度を取得
	function get_target_month($emp_id, $date) {
	
		//$duty_form = $this->get_duty_form($emp_id);
		//$closing = ($duty_form != "2") ? $this->closing : $this->closing_parttime;
		$closing = $this->get_empcond_closing($emp_id);
		if ($closing == "6") {
			$target_yyyymm = substr($date, 0, 6);
		}
		else {
			$closing_day = $this->arr_closing_day[$closing];
			
			$year = substr($date, 0, 4);
			$mon = intval(substr($date, 4, 2), 10);
			$day = intval(substr($date, 6, 2), 10);
			
			if (($this->closing_month_flg == "1" && $day > $closing_day) ||
				($this->closing_month_flg == "2" && $day <= $closing_day)) {
				$target_yyyymm = substr($date, 0, 6);
			}
			else if ($this->closing_month_flg == "1" && $day <= $closing_day) {
				$mon--;
				if ($mon <= 0) {
					$mon = 12;
					$year--;
				}
				$target_yyyymm = $year.sprintf("%02d", $mon);
			}
			else if ($this->closing_month_flg == "2" && $day > $closing_day) {
				$mon++;
				if ($mon >= 13) {
					$mon = 1;
					$year++;
				}
				$target_yyyymm = $year.sprintf("%02d", $mon);
			}
		}
		
		return $target_yyyymm;
	}

	//申請内容取得
	function get_apply_content($apply_id) {
        $sql  = "select apply_content ";
        $sql .= "from apply ";
        $cond = "where apply_id = $apply_id ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $content = "";
        if (pg_num_rows($sel) > 0) {
			$content = pg_fetch_result($sel, 0, "apply_content");
		}
        return $content;

	}
	
	//申請内容から職員ID取得
	function get_apply_emp_id($apply_id) {
		$content = $this->get_apply_content($apply_id);
		$utf_content = mb_convert_encoding(mb_convert_encoding($content,"sjis-win","eucJP-win"),"UTF-8","sjis-win");
		$utf_content = str_replace("EUC-JP", "UTF-8", $utf_content);
        $utf_content = preg_replace("/[\x01-\x08\x0b\x0c\x0e-\x1f\x7f]+/","",$utf_content);
        $utf_content = str_replace("\0","",$utf_content);
		$doc = domxml_open_mem($utf_content);
		if ($doc == null) {
			return "";
		}
		$root = $doc->get_elements_by_tagname('apply');
		$node_array = $root[0]->child_nodes();
		$values = $this->create_xml_map1($node_array);
		
		return $values["apply_emp_id"];
	}

	function create_xml_map1($node_array) {
		$values = array();
		
		foreach ($node_array as $child) {
			if ($child->node_type() == XML_ELEMENT_NODE) {
				$itemvalue = mb_convert_encoding($child->get_content(), "eucJP-win", "UTF-8");
				$itemvalue = str_replace("\r\n", "\n", $itemvalue);
				$itemvalue = str_replace("\n", "\\n", $itemvalue);
				
				$values[$child->tagname] = $itemvalue;
			}
		}
		return $values;
	}
	//指定職員の締め日取得
	function get_empcond_closing($emp_id) {
		if ($this->arr_closing[$emp_id] != "") {
			return $this->arr_closing[$emp_id];
		}
		$sql = "select closing, duty_form from empcond";
		$cond = "where emp_id = '$emp_id'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$closing = "";
		$duty_form = "";
		if (pg_num_rows($sel) > 0) {
			$closing = pg_fetch_result($sel, 0, "closing");
			$duty_form = pg_fetch_result($sel, 0, "duty_form");
		}
		if ($closing == "") {
			$closing = ($duty_form != "2") ? $this->closing : $this->closing_parttime;
		}
		$this->arr_duty_form[$emp_id] = $duty_form;
		$this->arr_closing[$emp_id] = $closing;
		return $closing;
	}

	// 指定年月の期間を返す
	// @param $yyyymm 年月
	// @param $closing 月締め日
	// @param mixed $closing_month_flg 月度 1:開始日の月とする 2:終了日の月とする
	function get_term($yyyymm, $closing, $closing_month_flg) {
		$yyyy = substr($yyyymm, 0, 4);
		$mm = substr($yyyymm, 4, 2);

	    //末日以外
	    if ($closing != "6") {
	        //月度終了日
	        if ($closing_month_flg == "2") {
	            $next_yyyy = $yyyy;
	            $next_mm = $mm;
	            $mm--;
	            if ($mm < 1) {
	                $yyyy--;
	                $mm = 12;
	            }

	        }
	        //月度開始日
	        else {
	            $next_yyyy = $yyyy;
	            $next_mm = $mm + 1;
	            if ($next_mm > 12) {
	                $next_yyyy++;
	                $next_mm = 1;
	            }
	        }
	    }

		switch ($closing) {
		case "1":  // 1日
			$start_date = sprintf("%04d%02d%02d", $yyyy, $mm, 2);
			$end_date = sprintf("%04d%02d%02d", $next_yyyy, $next_mm, 1);
			break;
		case "2":  // 5日
			$start_date = sprintf("%04d%02d%02d", $yyyy, $mm, 6);
			$end_date = sprintf("%04d%02d%02d", $next_yyyy, $next_mm, 5);
			break;
		case "3":  // 10日
			$start_date = sprintf("%04d%02d%02d", $yyyy, $mm, 11);
			$end_date = sprintf("%04d%02d%02d", $next_yyyy, $next_mm, 10);
			break;
		case "4":  // 15日
			$start_date = sprintf("%04d%02d%02d", $yyyy, $mm, 16);
			$end_date = sprintf("%04d%02d%02d", $next_yyyy, $next_mm, 15);
			break;
		case "5":  // 20日
			$start_date = sprintf("%04d%02d%02d", $yyyy, $mm, 21);
			$end_date = sprintf("%04d%02d%02d", $next_yyyy, $next_mm, 20);
			break;
		case "6":  // 末日
			$start_date = sprintf("%04d%02d%02d", $yyyy, $mm, 1);
			$end_date = sprintf("%04d%02d%02d", $yyyy, $mm, days_in_month($yyyy, $mm));
			break;
		}

		return array("start" => $start_date, "end" => $end_date);
	}
	
	function get_csv_closing_info() {
		$arr_closing = array();
		$sql = "select distinct csv_layout_id, closing from empcond ";
		$cond = "where closing != '' and  exists (select * from authmst where authmst.emp_id = empcond.emp_id and authmst.emp_del_flg = 'f') group by csv_layout_id, closing order by csv_layout_id, closing";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$rows = pg_fetch_all($sel);
		foreach ($rows as $row) {
			$arr_closing[$row["csv_layout_id"]] = $row["closing"];
		}
		return $arr_closing;
	}

	function get_term_multi_closing($yyyymm, $arr_closing, $closing_month_flg) {

		$arr_term = $this->get_term($yyyymm, $arr_closing[0], $closing_month_flg);
		$start = $arr_term["start"];
		$end = $arr_term["end"];
		
		for ($i=1; $i<count($arr_closing); $i++) {
			$arr_term = $this->get_term($yyyymm, $arr_closing[$i], $closing_month_flg);
			$start = min(intval($start), intval($arr_term["start"]));
			$end = max(intval($end), intval($arr_term["end"]));
		}
		return array("start" => $start, "end" => $end);
	}
}
