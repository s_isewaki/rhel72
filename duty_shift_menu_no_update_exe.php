<!-- ************************************************************************ -->
<!-- 勤務シフト作成｜表示順、チーム更新 -->
<!-- ************************************************************************ -->

<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");

$arr_sort_data = array();
for ($i=0; $i<$data_cnt; $i++) {
	$arr_sort_data[$i]["new_no"] = $no[$i];
	$arr_sort_data[$i]["old_no"] = $i;
	$arr_sort_data[$i]["emp_id"] = $staff_id[$i];
}

//列方向の配列を得る
$wk_key1 = array();
$wk_key2 = array();
for ($i=0; $i<count($arr_sort_data); $i++) {
	$wk_key1[$i] = $arr_sort_data[$i]["new_no"];
	$wk_key2[$i] = $arr_sort_data[$i]["old_no"];
}

//キーでソートする。
array_multisort($wk_key1, SORT_ASC, $wk_key2, SORT_DESC, $arr_sort_data);

//順番が変わった行を更新
for ($i=0; $i<count($arr_sort_data); $i++) {
	if ($i != $arr_sort_data[$i]["old_no"]) {
		$sql = "update duty_shift_plan_staff set";
		$cond = "where group_id = '$group_id' and emp_id = '{$arr_sort_data[$i]["emp_id"]}' ";
		$cond .= " and duty_yyyy = $duty_yyyy and duty_mm = $duty_mm ";
		$upd = update_set_table($con, $sql, array("no"), array($i+1), $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
}

//チームの更新
$obj->update_duty_shift_plan_team($data_cnt, $group_id, $duty_yyyy, $duty_mm, $staff_id, $team);

///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query("commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);
///-----------------------------------------------------------------------------
// 遷移元画面を再表示
///-----------------------------------------------------------------------------
echo("<script type=\"text/javascript\">location.href = 'duty_shift_menu.php?session=$session&group_id=$group_id&duty_yyyy=$duty_yyyy&duty_mm=$duty_mm&fullscreen_flg=$fullscreen_flg&cause_group_id=$group_id&plan_results_flg=$plan_results_flg&plan_hope_flg=$plan_hope_flg&plan_duty_flg=$plan_duty_flg&term_chg_id=$term_chg_id&total_disp_flg=$total_disp_flg&hol_dt_flg=$hol_dt_flg';</script>");
?>