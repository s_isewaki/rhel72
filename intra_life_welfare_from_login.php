<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_postgres.php");
require("about_authority.php");

$fname = $PHP_SELF;

// データベースに接続
$con = connect2db($fname);

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu3 = pg_fetch_result($sel, 0, "menu3");
$menu3_1 = pg_fetch_result($sel, 0, "menu3_1");
$menu3_2 = pg_fetch_result($sel, 0, "menu3_2");
$menu3_3 = pg_fetch_result($sel, 0, "menu3_3");

// ライフステージ情報を配列に格納
$sql = "select lifestage_id, name from lifestage";
$cond = "where exists (select * from welfare where welfare.lifestage_id = lifestage.lifestage_id) order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$arr_stage = array();
while ($row = pg_fetch_array($sel)) {
	$lifestage_id = $row["lifestage_id"];
	$stage_name = $row["name"];

	$arr_stage[$lifestage_id] = array();
	$arr_stage[$lifestage_id]["name"] = $stage_name;
	$arr_stage[$lifestage_id]["welfare"] = array();
}
$lifestage_count = count($arr_stage);

// 文書情報を配列に追加
$sql = "select * from welfare";
$cond = "order by welfare.welfare_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$lifestage_id = $row["lifestage_id"];
	$welfare_id = $row["welfare_id"];
	$system = $row["system"];
	$description = $row["description"];

	$arr_stage[$lifestage_id]["welfare"]["$welfare_id"] = array(
		"system" => $system, "description" => $description
	);
}
?>
<title>イントラネット | <? echo($menu3_2); ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function changeDisplay(stage_id) {
	var img = document.getElementById('stage'.concat(stage_id));
	if (img.className == 'close') {
		img.className = 'open';
		row_display = '';
	} else {
		img.className = 'close';
		row_display = 'none';
	}

	var row_class = 'children_of_'.concat(stage_id);
	var rows = document.getElementsByTagName('tr');
	for (var i = 0, j = rows.length; i < j; i++) {
		if (rows[i].className == row_class) {
			rows[i].style.display = row_display;
		}
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
img.close {
	background-image:url("img/icon/plus.gif");
	vertical-align:middle;
}
img.open {
	background-image:url("img/icon/minus.gif");
	vertical-align:middle;
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><? echo($menu3_2); ?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="6"><br>
<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="intra_life_from_login.php"><? echo($menu3); ?></a> &gt; <? echo($menu3_2); ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="6"><br>
<?
if ($lifestage_count == 0) {
	echo("<table width=\"760\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	echo("<tr height=\"22\">\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">文書は登録されていません。</font></td>\n");
	echo("</tr>\n");
	echo("</table>\n");
} else {
?>
<table width="760" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="25%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">ライフステージ</font></td>
<td width="22%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">制度</font></td>
<td width="24%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">概要</font></td>
<td width="11%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">詳細説明</font></td>
<td width="9%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">記入例</font></td>
<td width="9%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">申請書</font></td>
</tr>
<?
	foreach ($arr_stage as $lifestage_id => $stage_data) {
		$stage_name = $stage_data["name"];
		$arr_welfare = $stage_data["welfare"];

		// ライスステージ行を出力
		echo("<tr valign=\"top\">\n");
		echo("<td><img id=\"stage$lifestage_id\" src=\"img/spacer.gif\" alt=\"\" width=\"20\" height=\"20\" class=\"close\" style=\"margin-right:3px;\" onclick=\"changeDisplay('$lifestage_id')\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">$stage_name</font></td>\n");
		echo("<td></td>\n<td></td>\n<td></td>\n<td></td>\n<td></td>\n");
		echo("</tr>\n");

		// 文書行を出力
		foreach ($arr_welfare as $welfare_id => $welfare_data) {
			$welfare_system = $welfare_data["system"];
			$welfare_description = $welfare_data["description"];

			echo("<tr valign=\"top\" class=\"children_of_$lifestage_id\" style=\"display:none;height:26px;\">\n");
			echo("<td></td>\n");
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">$welfare_system</font></td>\n");
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">$welfare_description</font></td>\n");
			for ($i = 1; $i <= 3; $i++) {
				$paths = glob("intra/welfare/{$welfare_id}_{$i}.*");
				$path = (is_array($paths)) ? $paths[0] : "";

				echo("<td align=\"center\">");
				if ($path != "") {
					echo("<input type=\"button\" value=\"参照\" onclick=\"window.open('$path');\">");
				}
				echo("</td>\n");
			}
			echo("</tr>\n");
		}
	}
?>
</table>
<?
}
?>
</center>
</body>
<? pg_close($con); ?>
</html>
