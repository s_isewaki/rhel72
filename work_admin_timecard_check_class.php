<?php
require_once("date_utils.php");
require_once("timecard_common_class.php");

class check_timecard
{
	function get_show_list ($con, 
							$fname, 
							$id_list, 
							$start_date, 
							$end_date, 
							$menuname, 
							$arr_sortkey){ 
		
		$arr_list = array();
		$today = date("Ymd");
		$end_date = min($today,$end_date);
		
		$arr_emp_id = explode("," , $id_list);
		$emp_id_info ="'" . implode("','", $arr_emp_id) . "'";

		//グループ毎のテーブルから勤務時間を取得する
		$timecard_common_class = new timecard_common_class($con, $fname, $arr_emp_id, $start_date, $end_date);

		$emp_info = $this->get_empinfo($con, $fname, $emp_id_info, $arr_sortkey);
		//給与区分を設定してない職員を除く
		foreach ($arr_emp_id as $id_key => $emp_id){
			if (!$emp_info[$emp_id]) {
				unset($arr_emp_id[$id_key]);
			}
		}
		
		$emp_id_list = "'" . implode("','", $arr_emp_id) . "'";
		
		$officehours = $this->get_officehours($con, $fname);
		$emp_officehours = $this->get_emp_officehours($con, $fname, $emp_id_list);
		
		switch ($menuname) {
			case "check_timecard":
				/* 勤務パターン・打刻漏れ・休日打刻チェック */
				$emp_info = $this->check_punchtime($con, $fname, $emp_id_list, $start_date, $end_date, $emp_info, $arr_emp_id);
				
				/* 遅刻・早退チェック */
				$emp_info = $this->check_attendance($con, $fname, $emp_id_list, $start_date, $end_date, $emp_info, $officehours, $emp_officehours, $timecard_common_class);
				break;

			case "check_overtime":
				/* 残業時間チェック */
				$emp_info = $this->check_overtime($con, $fname, $emp_id_list, $start_date, $end_date, $emp_info, $officehours, $emp_officehours, $timecard_common_class);
				break;
				
			case "check_unapproved":
				/* 残業未承認チェック */
				$emp_info = $this->check_overtime_apply($con, $fname, $emp_id_list, $start_date, $end_date, $emp_info);
				
				/* 呼出未承認チェック */
				$emp_info = $this->check_return_apply($con, $fname, $emp_id_list, $start_date, $end_date, $emp_info);
				break;
		}
		
		return $emp_info;
	}
	
	function get_classmst($con, $fname){
		
		$arr_class_list = array();
		
		$sql  = "select class_id, class_nm from classmst";
		$cond = "where class_del_flg = 'f'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		while ($row = pg_fetch_array($sel)) {
			$tmp_id = $row["class_id"];
			$tmp_nm = $row["class_nm"];
			$arr_class_list[$tmp_id] = $tmp_nm;
		}
		
		return $arr_class_list;
	}
	
	function get_atrbmst($con, $fname){
		
		$arr_atrb_list = array();
		
		$sql  = "select atrb_id, atrb_nm from atrbmst";
		$cond = "where atrb_del_flg = 'f'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		while ($row = pg_fetch_array($sel)) {
			$tmp_id = $row["atrb_id"];
			$tmp_nm = $row["atrb_nm"];
			$arr_atrb_list[$tmp_id] = $tmp_nm;
				
		}
		
		return $arr_atrb_list;
	}
	
	function get_deptmst($con, $fname){
		
		$arr_dept_list = array();
		
		$sql  = "select dept_id, dept_nm from deptmst";
		$cond = "where dept_del_flg = 'f'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		while ($row = pg_fetch_array($sel)) {
			$tmp_id = $row["dept_id"];
			$tmp_nm = $row["dept_nm"];
			$arr_dept_list[$tmp_id] = $tmp_nm;
				
		}
		
		return $arr_dept_list;
	}
	
	//職員情報
	function get_empinfo($con, $fname, $emp_id, $arr_sortkey){
		
		$arr_empinfo_list = array();
		$arr_class_list = $this->get_classmst($con, $fname);  //部門
		$arr_atrb_list = $this->get_atrbmst($con, $fname);	  //課
		$arr_dept_list = $this->get_deptmst($con, $fname);	  //科
		
		//ソートキー対応
		$str_sort = "";
		$arr_sort = array  ("1"=>"emp_personal_id,", 
							"2"=>"emp_kn_lt_nm,emp_kn_ft_nm,", 
							"3"=>"emp_class,",
							"4"=>"emp_attribute,",
							"5"=>"emp_dept,",
							"6"=>"emp_job,");

		foreach ($arr_sortkey as $sort){
			if ($arr_sort[$sort]) {
				$str_sort .= $arr_sort[$sort]; 
			}
		}
		
		$sql  = "select t.emp_id, t.emp_personal_id, t.emp_class, t.emp_attribute, t.emp_dept, t.emp_lt_nm, t.emp_ft_nm,";
		$sql .= " d.wage, d.duty_form from empmst t inner join empcond d on t.emp_id = d.emp_id";
		$cond = "where t.emp_id in ($emp_id) ";
		
		if ($str_sort != "") {
			$cond .= "order by " . rtrim($str_sort, ",");
		}
		
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		while ($row = pg_fetch_array($sel)) {
			
			$tmp_id 		= $row["emp_id"];
			$tmp_personal 	= $row["emp_personal_id"];
			$tmp_class 		= $row["emp_class"];
			$tmp_attribute  = $row["emp_attribute"];
			$tmp_dept 		= $row["emp_dept"];
			$tmp_lt_nm 		= $row["emp_lt_nm"];
			$tmp_ft_nm 		= $row["emp_ft_nm"];
			$tmp_wage 		= $row["wage"];
			$tmp_duty 		= $row["duty_form"];
			
			//職員氏名
			$name = $tmp_lt_nm . " " . $tmp_ft_nm;
				
			//部署
			$tmp_class 	   = ($arr_class_list[$tmp_class]) ?  $arr_class_list[$tmp_class] : "";
			$tmp_attribute = ($arr_atrb_list[$tmp_attribute]) ? $arr_atrb_list[$tmp_attribute] : "";
			$tmp_dept 	   = ($arr_dept_list[$tmp_dept]) ? $arr_dept_list[$tmp_dept] : "";
				
			$department = $tmp_class . ">" . $tmp_attribute . ">" . $tmp_dept;
			
			$arr_empinfo_list[$tmp_id] = array(
												"emp_personal_id" => $tmp_personal,
												"emp_name" 		  => $name,
												"emp_department"  => $department,
												"wage"            => $tmp_wage,
												"duty_form"       => $tmp_duty
												);
			
		}
		
		return $arr_empinfo_list;
	}
	
	function get_emp_officehours($con, $fname, $emp_id) {
		
		$arr_emp_officehours = array();
		
		$sql  = "select * from empcond_officehours";
		$cond = "where emp_id in ($emp_id)";
		
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		while ($row = pg_fetch_array($sel)) {
			$tmp_id     = $row["emp_id"];
			$tmp_week   = $row["weekday"];
			$tmp_start2 = str_replace(":", "", $row["officehours2_start"]);
			$tmp_end2   = str_replace(":", "", $row["officehours2_end"]);
			$tmp_start4 = str_replace(":", "", $row["officehours4_start"]);
			$tmp_end4   = str_replace(":", "", $row["officehours4_end"]);
			
			$arr_emp_officehours[$tmp_id][$tmp_week] = array(
											"weekday" => $tmp_week,
											"start2"  => $tmp_start2,
											"end2"    => $tmp_end2,
											"start4"  => $tmp_start4,
											"end4"    => $tmp_end4
											);
			
		}
		return $arr_emp_officehours;
	}
	
	function get_officehours($con, $fname) {
		
		$arr_officehours = array();
		
		$sql = "select tmcd_group_id, pattern, officehours_id, officehours2_start, officehours2_end, officehours4_start, officehours4_end from officehours";
		$cond = "";
		
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		while ($row = pg_fetch_array($sel)) {
			$tmp_group 		= $row["tmcd_group_id"];
			$tmp_pattern 	= $row["pattern"];
			$tmp_id 		= $row["officehours_id"];
			$tmp_start2     = str_replace(":", "", $row["officehours2_start"]);
			$tmp_end2 		= str_replace(":", "", $row["officehours2_end"]);
			$tmp_start4     = str_replace(":", "", $row["officehours4_start"]);
			$tmp_end4 		= str_replace(":", "", $row["officehours4_end"]);
			
			$arr_officehours[$tmp_group] [$tmp_pattern][$tmp_id] = array("start2" => $tmp_start2, "end2" => $tmp_end2, "start4" => $tmp_start4, "end4" => $tmp_end4);
			
		}
		return $arr_officehours;
	}
	
	/* 勤務パターン・打刻漏れ・休日打刻のチェック */
	function check_punchtime($con, 
										$fname, 
										$emp_id_list, 
										$start_date, 
										$end_date, 
										$emp_info, 
										$id_list)
	{
		
		$arr_list = $emp_info;
		
		$sql  = "select at.emp_id, at.date, at.pattern, at.start_time, at.end_time, at.over_start_time, at.over_end_time,";
		$sql .= " an.after_night_duty_flag";
		$sql .= " from atdbkrslt at left join atdptn an";
		$sql .= " on at.tmcd_group_id = an.group_id and at.pattern = cast(an.atdptn_id as varchar)";
		$cond = "where at.emp_id in ($emp_id_list)";
		$cond .= " and at.date >= '$start_date' and at.date <= '$end_date'";
		$cond .= " order by at.emp_id, at.date";
		
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		while ($row = pg_fetch_array($sel)) {
				
			$tmp_id 		= $row["emp_id"];
			$tmp_date 		= $row["date"];
			$tmp_pattern 	= $row["pattern"];
			$tmp_start 		= $row["start_time"];
			$tmp_end 		= $row["end_time"];
			$tmp_duty_flag  = $row["after_night_duty_flag"];
			$tmp_over_start = $row["over_start_time"];
			$tmp_over_end   = $row["over_end_time"];
			
				
			$tmp_list[$tmp_id][$tmp_date] = array("date" => $tmp_date,
												 "pattern" => $tmp_pattern,
												 "start" => $tmp_start,
												 "end" => $tmp_end,
												 "duty_flag" => $tmp_duty_flag,
												 "over_start" => $tmp_over_start,
												 "over_end" => $tmp_over_end
												);
			
			
		}
		
		foreach ($id_list as $id){
			
			$i = 1;
			$date = $start_date;
			while($date <= $end_date){
				
				$error_msg = array();
				$results = $tmp_list[$id][$date];
				if ($results) {
					if ($results["duty_flag"] != "1") {
						if (($emp_info[$id]["duty_form"] == "1") && ($results["pattern"] == "")) {
							$error_msg["err_pattern"] = "勤務実績未登録あり";
						}						
						
						if (($results["pattern"] != 10) && ($results["start"] == "" || $results["end"] == "")) {
							$error_msg["err_time"] = "打刻漏れあり";
						}
						
						if (($results["pattern"] == 10) && ($results["start"] != "" || $results["end"] != "") && ($results["over_start"] == "") && ($results["over_end"] == "")) {
							$error_msg["err_holiday"] = "休日打刻あり";
						}
					}
				}else{
					if ($emp_info[$id]["duty_form"] == "1"){
						$error_msg["err_pattern"] = "勤務実績未登録あり";
					}
					$error_msg["err_time"] = "打刻漏れあり";
				}
				
				if (!empty($error_msg)){
				
					foreach ($error_msg as $buf_key=>$buf_value){
						//全体のエラー内容
						if (!array_key_exists($buf_key, $arr_list[$id]["month_msg"])) {
							$arr_list[$id]["month_msg"][$buf_key] = $buf_value;
						}
					}
				
					//日付毎のエラー内容
					$arr_list[$id]["date_err"][$date] = $error_msg;
				}
				$date= date("Ymd", strtotime("+$i day",date_utils::to_timestamp_from_ymd($date) ));
			}
		}
		
		return $arr_list;
	}
	
	/* 遅刻と早退のチェック */
	function check_attendance($con, 
							$fname, 
							$emp_id_list, 
							$start_date, 
							$end_date, 
							$emp_info, 
							$officehours, 
							$emp_officehours, 
							$timecard_common_class)
	{
		$arr_list = $emp_info;
		
		$sql  = "select at.emp_id, at.date, at.pattern, at.start_time, at.end_time, at.tmcd_group_id, at.previous_day_flag, at.next_day_flag,";
		$sql .= " an.workday_count, an.empcond_officehours_flag, an.after_night_duty_flag, an.previous_day_flag as an_day_flag, an.over_24hour_flag,";
		$sql .= " cr.type,";
		$sql .= " ak.prov_start_time, ak.prov_end_time,";
		$sql .= " kl.transfer";
		$sql .= " from atdbkrslt at";
		$sql .= " left join atdptn an on at.tmcd_group_id = an.group_id and at.pattern = cast(an.atdptn_id as varchar)";
		$sql .= " left join calendar cr on at.date = cr.date";
		$sql .= " left join atdbk ak on at.emp_id = ak.emp_id and at.date = ak.date";
		$sql .= " left join kintai_late kl on at.emp_id = kl.emp_id and at.date = kl.start_date and kl.apply_stat = '1' and kl.kind_flag in('1','2') and kl.transfer in(1,2)";

		$cond = "where at.emp_id in ($emp_id_list)";
		$cond .= " and (at.date >= '$start_date' and at.date <= '$end_date')";
		$cond .= " and at.pattern != '' and at.start_time != '' and at.end_time != ''";

		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//エラー内容チェック
		$error_msg = array();
		while ($row = pg_fetch_array($sel)) {
			$tmp_id 		= $row["emp_id"];
			$tmp_date 		= $row["date"];
			$tmp_pattern 	= $row["pattern"];
			$tmp_start 		= $row["start_time"];
			$tmp_end 		= $row["end_time"];
			$tmp_group 		= $row["tmcd_group_id"];
			$tmp_pre_flag 	= $row["previous_day_flag"];
			$tmp_next_flag 	= $row["next_day_flag"];
			$tmp_workday 	= floatval($row["workday_count"]);
			$tmp_flag 		= $row["empcond_officehours_flag"];
			$tmp_duty_flag 	= $row["after_night_duty_flag"];
			$tmp_anday_flag = $row["an_day_flag"];
			$tmp_24h_flag   = $row["over_24hour_flag"];
	
			$tmp_type 		= $row["type"];
			$prov_start 	= $row["prov_start_time"];
			$prov_end 		= $row["prov_end_time"];

			$transfer 		= $row["transfer"];
			
			if (($tmp_type != "2") && ($tmp_type != "3")){
				$tmp_type = "1";
			}
			
			if ($tmp_workday > 0 && $tmp_duty_flag != "1"){
				$error_msg = array();

				$start_date_time = $timecard_common_class->get_timecard_start_date_time($tmp_date, $tmp_start, $tmp_pre_flag);
				$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_end, $tmp_next_flag);
				
				/* 所定時間ゲット */
				$fixedtime = $this->get_fixedtime($tmp_flag, $tmp_id, $tmp_date, $emp_officehours, 
												  $officehours, $tmp_group, $tmp_pattern, $tmp_type, $tmp_anday_flag,
												  $tmp_24h_flag, $timecard_common_class, $prov_start, $prov_end, $emp_info);
				$start2_date_time = $fixedtime["start2_date_time"];
				$end2_date_time   = $fixedtime["end2_date_time"];
				
				
				if (($start2_date_time != "") && ($end2_date_time != "")){
					if ($start_date_time > $start2_date_time) {
						
						switch ($transfer){
							case "1":
								$error_msg["late"] = "遅刻あり(年休振替)";
								break;
							case "2":
								$error_msg["late"] = "遅刻あり(欠勤振替)";
								break;
							default:
								$error_msg["late"] = "遅刻あり";
						}
						
					}
	
					if ($end_date_time < $end2_date_time) {
						
						switch ($transfer){
							case "1":
								$error_msg["early"] = "早退あり(年休振替)";
								break;
							case "2":
								$error_msg["early"] = "早退あり(欠勤振替)";
								break;
							default:
								$error_msg["early"] = "早退あり";
						}
					}
				}

				if (!empty($error_msg)){

					foreach ($error_msg as $buf_key=>$buf_value){
						//全体のエラー内容
						if (!array_key_exists($buf_key, $arr_list[$tmp_id]["month_msg"])) {
							$arr_list[$tmp_id]["month_msg"][$buf_key] = $buf_value;
						}
					}

					//日付毎のエラー内容
					$arr_list[$tmp_id]["date_err"][$tmp_date] = $error_msg;
				}
			}
		}
		
		return $arr_list;
	}
	
	/* 残業チェック */
	function check_overtime($con,
							$fname,
							$emp_id_list,
							$start_date,
							$end_date,
							$emp_info,
							$officehours,
							$emp_officehours,
							$timecard_common_class)
	{
		$arr_list = $emp_info;
	
		$sql  = "select at.emp_id, at.date, at.pattern, at.tmcd_group_id,";
		$sql .= " at.o_start_time1, at.o_end_time1, at.o_start_time2, at.o_end_time2,";
		$sql .= " at.o_start_time3, at.o_end_time3, at.o_start_time4, at.o_end_time4,";
		$sql .= " at.o_start_time5, at.o_end_time5, at.o_start_time6, at.o_end_time6,";
		$sql .= " at.o_start_time7, at.o_end_time7, at.o_start_time8, at.o_end_time8,";
		$sql .= " at.o_start_time9, at.o_end_time9, at.o_start_time10, at.o_end_time10,";
		$sql .= " at.over_start_time, at.over_end_time, at.over_start_next_day_flag, at.over_end_next_day_flag,";
		$sql .= " at.over_start_time2, at.over_end_time2, at.over_start_next_day_flag2, at.over_end_next_day_flag2,";
		$sql .= " an.empcond_officehours_flag, an.previous_day_flag as an_day_flag, an.over_24hour_flag,";
		$sql .= " cr.type,";
		$sql .= " ak.prov_start_time, ak.prov_end_time,";
		$sql .= " at.end_time, at.next_day_flag,";
		$sql .= " at.over_start_time3, at.over_end_time3, at.over_start_next_day_flag3, at.over_end_next_day_flag3,";
		$sql .= " at.over_start_time4, at.over_end_time4, at.over_start_next_day_flag4, at.over_end_next_day_flag4,";
		$sql .= " at.over_start_time5, at.over_end_time5, at.over_start_next_day_flag5, at.over_end_next_day_flag5";
		$sql .= " from atdbkrslt at";
		$sql .= " left join atdptn an on at.tmcd_group_id = an.group_id and at.pattern = cast(an.atdptn_id as varchar)";
		$sql .= " left join calendar cr on at.date = cr.date";
		$sql .= " left join atdbk ak on at.emp_id = ak.emp_id and at.date = ak.date";
	
		$cond = "where at.emp_id in ($emp_id_list)";
		$cond .= " and (at.date >= '$start_date' and at.date <= '$end_date')";
		$cond .= " and at.pattern != '' and at.start_time != '' and at.end_time != ''";
	
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	
		//エラー内容チェック
		$error_msg = array();
		while ($row = pg_fetch_array($sel)) {
			$tmp_id 		= $row["emp_id"];
			$tmp_date 		= $row["date"];
			$tmp_pattern 	= $row["pattern"];
			$tmp_group 		= $row["tmcd_group_id"];
			$tmp_over_start = $row["over_start_time"];
			$tmp_over_end 	= $row["over_end_time"];
			$tmp_start_flg   = $row["over_start_next_day_flag"];
			$tmp_end_flg 	 = $row["over_end_next_day_flag"];
			$tmp_over_start2 = $row["over_start_time2"];
			$tmp_over_end2 	 = $row["over_end_time2"];
			$tmp_start2_flg   = $row["over_start_next_day_flag2"];
			$tmp_end2_flg 	 = $row["over_end_next_day_flag2"];
			
			$tmp_flag 		= $row["empcond_officehours_flag"];
			$tmp_anday_flag = $row["an_day_flag"];
			$tmp_24h_flag   = $row["over_24hour_flag"];
	
			$tmp_type 		= $row["type"];
			$prov_start 	= $row["prov_start_time"];
			$prov_end 		= $row["prov_end_time"];

			$end_time 		= $row["end_time"];
			$next_day_flag 		= $row["next_day_flag"];
			$tmp_over_start3 = $row["over_start_time3"];
			$tmp_over_end3 	 = $row["over_end_time3"];
			$tmp_start3_flg   = $row["over_start_next_day_flag3"];
			$tmp_end3_flg 	 = $row["over_end_next_day_flag3"];
			$tmp_over_start4 = $row["over_start_time4"];
			$tmp_over_end4 	 = $row["over_end_time4"];
			$tmp_start4_flg   = $row["over_start_next_day_flag4"];
			$tmp_end4_flg 	 = $row["over_end_next_day_flag4"];
			$tmp_over_start5 = $row["over_start_time5"];
			$tmp_over_end5 	 = $row["over_end_time5"];
			$tmp_start5_flg   = $row["over_start_next_day_flag5"];
			$tmp_end5_flg 	 = $row["over_end_next_day_flag5"];
	
			$error_msg = array();
			/* 所定時間ゲット */
			$fixedtime = $this->get_fixedtime($tmp_flag, $tmp_id, $tmp_date, $emp_officehours,
					$officehours, $tmp_group, $tmp_pattern, $tmp_type, $tmp_anday_flag,
					$tmp_24h_flag, $timecard_common_class, $prov_start, $prov_end, $emp_info);
			$start2_date_time = $fixedtime["start2_date_time"];
			$end2_date_time   = $fixedtime["end2_date_time"];
			

			if (($start2_date_time != "") && ($end2_date_time != "")){
				$arr_overtime = array();
				//残業時間1
				$start_date_time = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_over_start, $tmp_start_flg);
				$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_over_end, $tmp_end_flg);
				$arr_overtime[] = $start_date_time;
				$arr_overtime[] = $end_date_time;
				
				//残業時間2
				$start_date_time = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_over_start2, $tmp_start2_flg);
				$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_over_end2, $tmp_end2_flg);
				$arr_overtime[] = $start_date_time;
				$arr_overtime[] = $end_date_time;
				
				if ($this->judge_overtime($start2_date_time, $end2_date_time, $arr_overtime)){
					$error_msg["overtime"] = "残業時間エラー";
				}
				
				
				//呼出
				if (!$error_msg["overtime"]){
					$chk_o_start = substr($start2_date_time,8,4);
					$chk_o_end = substr($end2_date_time,8,4);
					
					for ($i=1; $i <= 10; $i++){
						$tmp_o_start 	= $row["o_start_time".$i];
						$tmp_o_end 		= $row["o_end_time".$i];
						
						if (($chk_o_start < $tmp_o_start) && ($tmp_o_start < $chk_o_end)){
							$error_msg["overtime"] = "残業時間エラー";
							break;
						}
						if (($chk_o_start < $tmp_o_end) && ($tmp_o_end < $chk_o_end)){
							$error_msg["overtime"] = "残業時間エラー";
							break;
						}
					}
				}
			}
			//残業時刻が退勤時刻より後の場合エラーとする 20150703
			if (!$error_msg["overtime"] && $end_time != ""){
				$arr_overtime = array();
				//残業時間1
				$start_date_time = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_over_start, $tmp_start_flg);
				$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_over_end, $tmp_end_flg);
				if ($start_date_time != "") {
					$arr_overtime[] = $start_date_time;
				}
				if ($start_date_time != "") {
					$arr_overtime[] = $end_date_time;
				}
				
				//残業時間2
				$start_date_time = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_over_start2, $tmp_start2_flg);
				$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_over_end2, $tmp_end2_flg);
				if ($start_date_time != "") {
					$arr_overtime[] = $start_date_time;
				}
				if ($start_date_time != "") {
					$arr_overtime[] = $end_date_time;
				}
			
				//残業時間3
				$start_date_time = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_over_start3, $tmp_start3_flg);
				$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_over_end3, $tmp_end3_flg);
				if ($start_date_time != "") {
					$arr_overtime[] = $start_date_time;
				}
				if ($start_date_time != "") {
					$arr_overtime[] = $end_date_time;
				}
			
				//残業時間4
				$start_date_time = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_over_start4, $tmp_start4_flg);
				$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_over_end4, $tmp_end4_flg);
				if ($start_date_time != "") {
					$arr_overtime[] = $start_date_time;
				}
				if ($start_date_time != "") {
					$arr_overtime[] = $end_date_time;
				}
			
				//残業時間5
				$start_date_time = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_over_start5, $tmp_start5_flg);
				$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $tmp_over_end5, $tmp_end5_flg);
				if ($start_date_time != "") {
					$arr_overtime[] = $start_date_time;
				}
				if ($start_date_time != "") {
					$arr_overtime[] = $end_date_time;
				}
			
				//退勤時間
				$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $end_time, $next_day_flag);
				if (count($arr_overtime) > 0) {
					if ($this->judge_overtime2($end_date_time, $arr_overtime)){
						$error_msg["overtime"] = "残業時間エラー";
					}
				}
			}
			if (!empty($error_msg)){

				foreach ($error_msg as $buf_key=>$buf_value){
					//全体のエラー内容
					if (!array_key_exists($buf_key, $arr_list[$tmp_id]["month_msg"])) {
						$arr_list[$tmp_id]["month_msg"][$buf_key] = $buf_value;
					}
				}

				//日付毎のエラー内容
				$arr_list[$tmp_id]["date_err"][$tmp_date] = $error_msg;
			}
		}
	
		return $arr_list;
	}
	
	/* 残業未承認チェック */
	function check_overtime_apply($con,
								  $fname,
								  $emp_id_list,
								  $start_date,
								  $end_date,
								  $emp_info)
	{
		$arr_list = $emp_info;
		
		$sql   = "select emp_id, target_date from ovtmapply";
		$cond  = "where emp_id in ($emp_id_list)";
		$cond .= " and (target_date >= '$start_date' and target_date <= '$end_date') and apply_status = '0' and delete_flg <> 't'";
		
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		//未承認チェック
		$error_msg = array();
		while ($row = pg_fetch_array($sel)) {
			$tmp_id 		= $row["emp_id"];
			$tmp_date 		= $row["target_date"];
			
			$error_msg["unapproved"] = "残業未承認";
			
			foreach ($error_msg as $buf_key=>$buf_value){
				//全体のエラー内容
				if (!array_key_exists($buf_key, $arr_list[$tmp_id]["month_msg"])) {
					$arr_list[$tmp_id]["month_msg"][$buf_key] = $buf_value;
				}
			}
		
			//日付毎のエラー内容
			$arr_list[$tmp_id]["date_err"][$tmp_date] = $error_msg;
		}
		
		return $arr_list;
	}
	
	/* 呼出未承認チェック */
	function check_return_apply($con,
								$fname,
								$emp_id_list,
								$start_date,
								$end_date,
								$emp_info)
	{
		$arr_list = $emp_info;
	
		$sql   = "select emp_id, target_date from rtnapply";
		$cond  = "where emp_id in ($emp_id_list)";
		$cond .= " and (target_date >= '$start_date' and target_date <= '$end_date') and apply_status = '0' and delete_flg <> 't'";
	
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	
		//未承認チェック
		$error_msg = array();
		while ($row = pg_fetch_array($sel)) {
			$tmp_id 		= $row["emp_id"];
			$tmp_date 		= $row["target_date"];
				
			$error_msg["unapproved"] = "残業未承認";
				
			foreach ($error_msg as $buf_key=>$buf_value){
				//全体のエラー内容
				if (!array_key_exists($buf_key, $arr_list[$tmp_id]["month_msg"])) {
					$arr_list[$tmp_id]["month_msg"][$buf_key] = $buf_value;
				}
			}
	
			//日付毎のエラー内容
			$arr_list[$tmp_id]["date_err"][$tmp_date] = $error_msg;
		}
	
		return $arr_list;
	}
	
	function get_fixedtime ($tmp_flag, 
							$tmp_id, 
							$tmp_date, 
							$emp_officehours, 
							$officehours, 
							$tmp_group, 
							$tmp_pattern, 
							$tmp_type, 
							$tmp_anday_flag, 
							$tmp_24h_flag, 
							$timecard_common_class,
							$prov_start,
							$prov_end,
							$emp_info)
	{
		
		if ($tmp_flag == "1") {
			//所定労働・休憩は個人の勤務条件を優先する
			$arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($tmp_id, $tmp_date); //個人別勤務時間帯履歴対応 
			$arr_hist = $timecard_common_class->empcond_officehours_hist[0];
			
			$weekday = date("w",date_utils::to_timestamp_from_ymd($tmp_date));
// 			$arr_empcond_officehours2 = $timecard_common_class->get_empcond_officehours($weekday, $tmp_date); //個人別勤務時間帯履歴対応
			
			if (empty($arr_hist)){
				if ($emp_officehours[$tmp_id][$weekday]){
			
					if ((date_utils::is_holiday($tmp_date) == "1") && $weekday != "0"){
						//祝日
						$fixed_start = $emp_officehours[$tmp_id]["7"]["start2"];
						$fixed_end   = $emp_officehours[$tmp_id]["7"]["end2"];
						
						$rest_start = $emp_officehours[$tmp_id]["7"]["start4"]; //休憩時間 20131107
						$rest_end   = $emp_officehours[$tmp_id]["7"]["end4"];
					}else{
						//曜日
						$fixed_start = $emp_officehours[$tmp_id][$weekday]["start2"];
						$fixed_end   = $emp_officehours[$tmp_id][$weekday]["end2"];
						
						$rest_start = $emp_officehours[$tmp_id][$weekday]["start4"]; //休憩時間 20131107
						$rest_end   = $emp_officehours[$tmp_id][$weekday]["end4"];
					}
			
				}else{
					//基本
					$fixed_start = $emp_officehours[$tmp_id]["8"]["start2"];
					$fixed_end   = $emp_officehours[$tmp_id]["8"]["end2"];
					
					$rest_start = $emp_officehours[$tmp_id]["8"]["start4"]; //休憩時間 20131107
					$rest_end   = $emp_officehours[$tmp_id]["8"]["end4"];
				}
			}else{
				if ($arr_hist["data"][$weekday]){
						
					if ((date_utils::is_holiday($tmp_date) == "1") && $weekday != "0"){
						//祝日
						$fixed_start = $arr_hist["data"]["7"]["officehours2_start"];
						$fixed_end   = $arr_hist["data"]["7"]["officehours2_end"];
						
						$rest_start = $arr_hist["data"]["7"]["officehours4_start"]; //休憩時間 20131107
						$rest_end   = $arr_hist["data"]["7"]["officehours4_end"];
					}else{
						//曜日
						$fixed_start = $arr_hist["data"][$weekday]["officehours2_start"];
						$fixed_end   = $arr_hist["data"][$weekday]["officehours2_end"];
						
						$rest_start = $arr_hist["data"][$weekday]["officehours4_start"]; //休憩時間 20131107
						$rest_end   = $arr_hist["data"][$weekday]["officehours4_end"];
					}
						
				}else{
					//基本
					$fixed_start = $arr_hist["data"]["8"]["officehours2_start"];
					$fixed_end   = $arr_hist["data"]["8"]["officehours2_end"];
					
					$rest_start = $arr_hist["data"]["8"]["officehours4_start"]; //休憩時間 20131107
					$rest_end   = $arr_hist["data"]["8"]["officehours4_end"];
				}
			}
		
		}else{
			$fixed_start = "";
			$fixed_end = "";
			$rest_start = "";
			$rest_end = "";
			$arr_time = $officehours[$tmp_group][$tmp_pattern][$tmp_type];
		
			if (($arr_time["start2"] != "") && ($arr_time["end2"] != "")) {
				$fixed_start = $arr_time["start2"];
				$fixed_end   = $arr_time["end2"];
				
				$rest_start = $arr_time["start4"];
				$rest_end   = $arr_time["end4"];
			}else{
				//所定労働・休憩を指定しない（勤務開始予定・終了予定を時間給者が個別に登録する）
				if ($emp_info[$tmp_id]["wage"] == "4") {
					$fixed_start = $prov_start;
					$fixed_end   = $prov_end;
				}
			}
		}
		
		// 所定労働・休憩開始終了日時の取得
		if ($fixed_start != "" && $fixed_end != ""){
			if ($tmp_anday_flag == "1") {
				$tmp_prev_date = $this->last_date($tmp_date);
				$start2_date_time = $tmp_prev_date.$fixed_start;
				//24時間以上勤務の場合
				if ($tmp_24h_flag == "1") {
					$tmp_prev_date = $tmp_date;
				}
				$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_prev_date, $fixed_start, $fixed_end);
			} else {
				$start2_date_time = $tmp_date.$fixed_start;
				//24時間以上勤務の場合
				if ($tmp_24h_flag == "1") {
					$tmp_end_date = $this->next_date($tmp_date);
				} else {
					$tmp_end_date = $tmp_date;
				}
				$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_end_date, $fixed_start, $fixed_end);
			}
		}else{
			$start2_date_time = "";
			$end2_date_time = "";
		}
		$arr_fixedtime = array("start2_date_time" => $start2_date_time, "end2_date_time" => $end2_date_time, "rest_start_time" => $rest_start, "rest_end_time" => $rest_end);
		return $arr_fixedtime;
	}
	
	function judge_overtime($start2_date_time, $end2_date_time, $arr_overtime)
	{
		$err_flag = false;
		
		foreach ($arr_overtime as $overtime){
			
			if (($start2_date_time < $overtime) && ($overtime < $end2_date_time)){
				$err_flag = true;
				break;
			}
		}
		
		return $err_flag;
	}
	
	//残業時刻が退勤時刻より後の場合エラー
	function judge_overtime2($end_date_time, $arr_overtime)
	{
		$err_flag = false;
		
		foreach ($arr_overtime as $overtime){
			
			if ($end_date_time < $overtime){
				$err_flag = true;
				break;
			}
		}
		
		return $err_flag;
	}
	
	// 翌日日付をyyyymmdd形式で取得
	function next_date($yyyymmdd) {
		return date("Ymd", strtotime("+1 day", $this->to_timestamp($yyyymmdd)));
	}
	
	// 前日日付をyyyymmdd形式で取得
	function last_date($yyyymmdd) {
		return date("Ymd", strtotime("-1 day", $this->to_timestamp($yyyymmdd)));
	}
	
	// 日付をタイムスタンプに変換
	function to_timestamp($yyyymmdd) {
		$y = substr($yyyymmdd, 0, 4);
		$m = substr($yyyymmdd, 4, 2);
		$d = substr($yyyymmdd, 6, 2);
		return mktime(0, 0, 0, $m, $d, $y);
	}
}
?>