<?
// PDF作成については以下を参照しました。
// http://www.phpbook.jp/fpdf/index.html

ob_start();
ini_set("max_execution_time", 0);
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require_once("html_to_text_for_tinymce.php");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require_once("sum_application_workflow_common_class.php");

ob_end_clean();

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 57, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

if ($select_flg == 0 && $ids == "") {
	echo("<script type=\"text/javascript\">alert('出力するデータを選択してください。');</script>");
	echo("<script type=\"text/javascript\">close();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

$obj = new application_workflow_common_class($con, $fname);

//==============================
// データ検索
//==============================
$sql =
	" select".
	" summary.*".
	",empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_job".
	",ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm, tmplmst.tmpl_name".
	",sum_xml.smry_xml, sum_xml.xml_file_name, sum_xml.apply_id".
	",sum_apply.delete_flg as apply_delete_flg, sum_apply.apply_date, sum_apply.apply_no".
	",sum_wkfwmst.short_wkfw_name".
	",bldgmst.bldg_name, wdmst.ward_name, ptrmmst.ptrm_name".
	" from summary ".
	" left join (".
	"   select".
	"   summary_id, summary_seq, ptif_id, delete_flg".
	"  ,xml_file_name, coalesce(apply_id, 0) as apply_id".
	"  ,update_timestamp, smry_xml, smry_html".
	"   from sum_xml".
	" ) sum_xml on (sum_xml.summary_seq = summary.summary_seq)".
	" left join sum_apply on (sum_apply.summary_id = summary.summary_id and sum_apply.apply_id = sum_xml.apply_id)".
	" left join empmst on empmst.emp_id = summary.emp_id ".
	" left join sum_wkfwmst on (sum_wkfwmst.wkfw_id = sum_apply.wkfw_id)".
	" left join ptifmst on ptifmst.ptif_id = summary.ptif_id ".
	" left join inptmst on inptmst.ptif_id = summary.ptif_id ".
	" left join tmplmst on tmplmst.tmpl_id = summary.tmpl_id ".
	" left join divmst on divmst.diag_div = summary.diag_div ".
	" left join bldgmst on bldgmst.bldg_cd = inptmst.bldg_cd ".
	" left join wdmst on wdmst.bldg_cd = inptmst.bldg_cd and wdmst.ward_cd = inptmst.ward_cd".
	" left join ptrmmst on ptrmmst.bldg_cd = inptmst.bldg_cd and ptrmmst.ward_cd = inptmst.ward_cd and ptrmmst.ptrm_room_no = inptmst.ptrm_room_no".
	" where 1=1";

$rows = array();

//==============================
// select_flg:1  検索条件で指定されたデータを抽出し、レコードセット取得
//==============================
if (!@$_REQUEST["is_print_history"]){
	$sql .= " and (sum_apply.summary_id is null or sum_apply.delete_flg = 'f')";
}
if ($_REQUEST["select_flg"] == 1) {
	// 診療記録区分
	if (trim($_REQUEST["sel_diag"])) {
		$sql .= " and divmst.div_id = ".(int)$_REQUEST["sel_diag"];
	}
	// テンプレート
	if ($_REQUEST["tmpl_id"] == "-") {
		$sql .= " and summary.tmpl_id is null";
	}
	if ((int)$_REQUEST["tmpl_id"]) {
		$sql .= " and summary.tmpl_id = ".(int)$_REQUEST["tmpl_id"];
	}
	// 記載者
	if (trim($_REQUEST["recorder_id"])) {
		$sql .= " and summary.emp_id = '".pg_escape_string(trim($_REQUEST["recorder_id"]))."'";
	}
	// 開始年月
	if ((int)$_REQUEST["start_yr"] && (int)$_REQUEST["start_mon"] && (int)$_REQUEST["start_day"]) {
		$sql .= " and summary.cre_date >= ".( ((int)$_REQUEST["start_yr"])*10000 + ((int)$_REQUEST["start_mon"])*100 + (int)$_REQUEST["start_day"] );
	}
	// 終了年月
	if ((int)$_REQUEST["end_yr"] && (int)$_REQUEST["end_mon"] && (int)$_REQUEST["end_day"]) {
		$sql .= " and summary.cre_date <= ".( ((int)$_REQUEST["end_yr"])*10000 + ((int)$_REQUEST["end_mon"])*100 + (int)$_REQUEST["end_day"] );
	}
	// 開始月(妙な検索？）
	if (!(int)$_REQUEST["start_yr"] && (int)$_REQUEST["start_mon"] && !(int)$_REQUEST["start_day"]) {
		$arr_cond[] = "(substring(summary.cre_date from 5 for 2) >= '$start_mon')";
	}
	// 終了月(妙な検索？）
	if (!(int)$_REQUEST["end_yr"] == "-" && (int)$_REQUEST["end_mon"] != "-" && !(int)$_REQUEST["end_day"]) {
		$arr_cond[] = "(substring(summary.cre_date from 5 for 2) <= '$end_mon')";
	}
	// 記載内容
	if (trim($_REQUEST["search_str"]) != "") {
		$sql .= " and (1<>1";
		$arr_key = split(" ", $_REQUEST["search_str"]);
		for ($i=0; $i<count($arr_key); $i++) $sql .= " or smry_cmt like '%".pg_escape_string($arr_key[$i])."%'";
		$sql .= ")";
	}
	$sql .= " order by summary.cre_date, summary.summary_id, sum_xml.apply_id, sum_xml.update_timestamp";
	$sel = select_from_table($con, $sql, "", $fname);
	$rows = pg_fetch_all($sel);
}
//==============================
// select_flg:0  チェックボックスで指定されたデータを抽出し、レコードセット取得
// $idsには"{emp_id}_{pt_id}_{summary_id}"がカンマ区切りで設定
//==============================
else {
	$arr = split(",", $_REQUEST["ids"]);
	sort($arr);
	// 降順でやる
	foreach($arr as $row) {
		list($emp_id, $pt_id, $summary_id) = split("_", $row."__");
		$sql2 = $sql . " and summary.summary_id = ".(int)$summary_id." and summary.ptif_id = '".pg_escape_string($pt_id)."'";
		$sel = select_from_table($con,$sql2,"",$fname);
		$rows[] = pg_fetch_array($sel);
	}
}


//==============================
// DBレコードセットをループし、各レコードのXMLを取得する。XMLの内容をCSVのための配列へ格納
//==============================
$pdata = array();
$ptif_info = array();
$summary_seq_list = array();

foreach($rows as $i => $row){
	if (!$row) continue;

	$key1 = $row["ptif_id"]; // 第一階層：患者iD
	$key2 = $row["diag_div"]; // 第二階層：診療記録区分
	$key3 = $row["tmpl_name"]; // 第三階層：テンプレート名
	$key4 = $row["summary_id"]; // 第四階層：サマリオーダ
	if (!$key3 && !$row["tmpl_id"]) $row["tmpl_name"] = "（フリー入力）";

	// 非共通データ（テンプレートの場合）
	$data = "";
	if ($row["tmpl_id"] != "") {
		// CSV作成のように、convertXmlFileName2NewVersion()は行わない。
		// このPDF出力機能は、旧データファイルの扱いがなくなった後の機能であるため。
		$xml = new template_xml_class();
		$doc = $xml->xml_text_to_object_ary(mb_convert_encoding($row["smry_xml"], "UTF-8", "EUC-JP"));
		$data = get_recursive_xml_for_print(@$doc->nodes["template"], 1)."〆";
	}
	// 非共通データ(フリー入力の場合）
	else {
		$data = $row["smry_cmt"];
		$data = html_to_text_for_tinymce($data)."〆";
	}

	$summary_seq_list[$key1][$key2][$key3][$key4] = $row["summary_seq"];

	$apply_date = "";
	if ($row["apply_date"]){
		$apply_date = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $row["apply_date"]);
		if ($apply_date) $apply_date = "<".$apply_date.">  ";
	}

	$order_bangou = "";
	if ($row["short_wkfw_name"].$row["apply_date"]!=""){
		$order_bangou = $obj->generate_apply_no_for_display($row["short_wkfw_name"], $row["apply_date"], $row["apply_no"]);
		if ($order_bangou){
			$order_bangou = "[オーダ番号]".$order_bangou."  ";
		}
	}

	$apply_id = "";
	if ($row["apply_id"]){
		$apply_id = "[送信連番]".$row["apply_id"]."  ";
	}

	$ptif_info[$key1] = array(
		"ptif_name" => $row["ptif_lt_kaj_nm"]." ".$row["ptif_ft_kaj_nm"],
		"byoto" => $row["ward_name"],
		"byositu" => $row["ptrm_name"]
	);


	$pdata[$key1][$key2][$key3][$key4][] = array(
		"head" =>
		$apply_date.
//		$apply_id.
//		$order_bangou.
		"[指示日]". substr($row["cre_date"], 0, 4)."/".substr($row["cre_date"], 4, 2)."/".substr($row["cre_date"], 6, 2).
		"  [指示者]".$row["emp_lt_nm"]." ".$row["emp_ft_nm"],
//		"  [プロブレム]".$row["problem"],
		"body" => $data,
		"is_history" => ($row["apply_delete_flg"] == "t" ? 1 : 0),
		"apply_id" => $row["apply_id"],
	);
}


require('fpdf153/mbfpdf.php');
// EUC-JP->SJIS 変換を自動的に行なわせる場合に mbfpdf.php 内の $EUC2SJIS を
// true に修正するか、このように実行時に true に設定しても変換してくます。

class CustomMBFPDF extends MBFPDF {
	var $current_ptif_name;
	var $current_ptif_id;
	var $current_byoto;
	var $current_byositu;
	var $current_diag_div;
	var $ymdhms;
	function SetToday($s){
		$this->ymdhms = $s;
	}
	function SetHeader1Caption($ptif_name, $diag_div, $ptif_id, $byoto, $byositu){
		$this->current_ptif_name = $ptif_name;
		$this->current_ptif_id   = $ptif_id;
		$this->current_byoto     = $byoto;
		$this->current_byositu   = $byositu;
		$this->current_diag_div  = $diag_div;
	}
	function Header(){
//		$this->SetFont(PMINCHO,'B',16);
//		$this->Cell(0,20, "指示履歴表".'',0,0,'C');
		$this->SetFont(PMINCHO,'B',10);
		$this->Cell(0,10, $this->ymdhms,0,0,'R');

		$this->SetY(22);
		$this->Cell(12.0, 5.0, "病棟",'B',0);
		$this->Cell(50.0, 5.0,    "： ".$this->current_byoto,'B',0);
		$this->Cell(12.0, 5.0, "病室",'B',0);
		$this->Cell(40.0, 5.0,    "： ".$this->current_byositu,'B',0);

		$this->SetY(30);
		$this->Cell(12.0, 5.0, "患者ID",'B',0);
		$this->Cell(50.0, 5.0,    "： ".$this->current_ptif_id,'B',0);
		$this->Cell(12.0, 5.0, "氏名",'B',0);
		$this->Cell(40.0, 5.0,    "： ".$this->current_ptif_name,'B',1);

		$this->SetY(46);
/*
		$this->Cell(25.0, 5.0, "診療記録区分",'B',0);
		$this->Cell(0, 5.0, "： ".$this->current_diag_div,'B',0);
		$this->SetY(38.6);
		$this->Cell(0, 5.0, "",'B',0); // 二本目の罫線のみ描画するためのセル
		$this->SetY(50);
*/
	}
	function Footer(){
		$this->SetFont(PMINCHO,'B',10);
		$this->SetY(-15);//下から1.5センチのところ
		$this->Cell(0,10, $this->PageNo().'',0,0,'C');
	}
}

$GLOBALS['EUC2SJIS'] = true;
$pdf = new CustomMBFPDF();
$pdf->SetToday(date("Y/n/j G:i:s"));
$pdf->AddMBFont(PMINCHO,'SJIS');
$pdf->Open();

$cnt = 0;
foreach($pdata as $ptif_id => $ptif_list){ // 患者ループ
	$ptinfo = $ptif_info[$ptif_id];
	// 明示的な改ページ
	$pdf->SetHeader1Caption($ptinfo["ptif_name"], $diag_div, $ptif_id, $ptinfo["byoto"], $ptinfo["byositu"]);
	$pdf->AddPage();
	$is_page_started = 1;

	foreach($ptif_list as $diag_div => $diag_list){ // 指示記録区分ループ

		if (!$is_page_started) $pdf->Cell(0, 8, '' ,0,1); // 行間を少々
		$is_page_started = 0;

//		$this->SetY(38);
		$pdf->Cell(100.0, 5.0, "■ 診療記録区分：".$diag_div,'B',1);
		$pdf->Cell(0, 3, '' ,0,1); // 行間を少々
//		$this->Cell(0, 5.0, "",'B',0); // 二本目の罫線のみ描画するためのセル
//		$this->SetY(50);

		foreach($diag_list as $tmpl_name => $tmpl_list){ // テンプレートループ
//			if (!$is_page_started) $pdf->Cell(0, 8, '' ,0,1); // 行間を少々
//			$is_page_started = 0;

			$pdf->SetFillColor(220, 220, 220);
			$pdf->Cell(4, 0, '' ,0,0); // パディング少々
			$pdf->Cell(0, 5.0, $tmpl_name,1,1,'L',1);
			$pdf->Cell(0, 3, '' ,0,1); // 行間を少々

			foreach($tmpl_list as $summary_id => $apply_list){ // 指示オーダループ
				$summary_seq = $summary_seq_list[$ptif_id][$diag_div][$tmpl_name][$summary_id];
//				$pdf->Cell(60.0, 5.0, "[指示連番]".$summary_seq."  [指示ID]".$summary_id ,1,1,'L',1);
//				$pdf->Cell(0, 1, '' ,0,1); // 行間を少々

				foreach($apply_list as $idx => $row){ // 履歴ループ
					$pdf->Cell(8, 0, '' ,0,0); // パディング少々
					$pdf->SetFillColor(220, 220, 220);
					if ($row["is_history"]){
						$pdf->Cell(14.0, 5.0, "修正前",0,0,'C',1);
					} else {
						$pdf->Cell(14.0, 5.0, "最新",0,0,'C',1);
					}
					if (!(int)$row["apply_id"]){
						$pdf->Cell(14.0, 5.0, "未送信",0,0,'C',1);
					}
					$pdf->Cell(0, 5.0, $row["head"],0,1,'L',0);
					$pdf->Cell(0, 1, '' ,0,1); // 行間を少々
					$pdf->Cell(8, 0, '' ,0,0); // パディング少々
					$pdf->MultiCell(0, 5, str_replace('"','',$row["body"]), 1, 'L', 0);
					$pdf->Cell(0, 3, '' ,0,1); // 行間を少々
					$cnt++;
				}
			}
		}
	}
}

$pdf->Output();

pg_close($con);

die;


//------------------------------------------------------------------------------
// XMLノードを再帰的に探索して情報取得
//------------------------------------------------------------------------------
function get_recursive_xml_for_print($parent_node, $kaisou){
	if (!$parent_node) return;
	$out = array();
	$tag1 = "";
	$tag2 = "";
	if ($kaisou==2) { $tag1 = "【"; $tag2 = "】"; }
	if ($kaisou==3) { $tag1 = "《";  $tag2 = "》"; }
	if ($kaisou>=4) { $tag1 = "[";  $tag2 = "]"; }
	foreach($parent_node->nodes as $idx => $node){
		if (!$node) break;
		if (@$node->attr["hidden"]) continue;
		$title = "";
		if ($kaisou > 1){
			$title = $tag1 . (trim(@$node->attr["disp"])!="" ? trim(@$node->attr["disp"]) : trim(@$node->attr["main"])) . $tag2;
		}
		if (count($node->nodes)){
			$value = get_recursive_xml_for_print($node, $kaisou+1);
		} else {
			$value = @$node->value;
		}
		if ($value=="") continue;
		$out[] = $title . str_replace("\r", " ", str_replace("\n", " ", $value));
	}
	return implode("", $out);
}

