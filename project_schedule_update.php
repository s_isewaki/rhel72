<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 委員会・WG | スケジュール更新</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");
require("show_project_schedule_common.ini");
require("show_facility.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// スケジュール情報を取得
$table_name = ($timeless != "t") ? "proschd" : "proschd2";
$sql = "select $table_name.*, empmst.emp_lt_nm, empmst.emp_ft_nm, proceeding.prcd_create_emp_id from $table_name
left join proceeding on proceeding.pjt_schd_id = $table_name.pjt_schd_id
inner join empmst on $table_name.emp_id = empmst.emp_id
		";
$cond = " where $table_name.pjt_schd_id = '$pjt_schd_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$schd_title = pg_fetch_result($sel, 0, "pjt_schd_title");
$place_id = pg_fetch_result($sel, 0, "pjt_schd_place_id");
$schd_plc = pg_fetch_result($sel, 0, "pjt_schd_plc");
$schd_start_date = pg_fetch_result($sel, 0, "pjt_schd_start_date");
list($year, $month, $day) = split("-", $schd_start_date);
$schd_start_time = pg_fetch_result($sel, 0, "pjt_schd_start_time_v");
$schd_start_hrs = substr($schd_start_time, 0, 2);
$schd_start_min = substr($schd_start_time, 2, 2);
$schd_end_time = pg_fetch_result($sel, 0, "pjt_schd_dur_v");
$schd_end_hrs = substr($schd_end_time, 0, 2);
$schd_end_min = substr($schd_end_time, 2, 2);
$schd_type = pg_fetch_result($sel, 0, "pjt_schd_type");
$schd_detail = pg_fetch_result($sel, 0, "pjt_schd_detail");
$schd_emp_id = pg_fetch_result($sel, 0, "emp_id");
$schd_prcd_emp_id = pg_fetch_result($sel, 0, "prcd_create_emp_id");
$repeat_flg = (pg_fetch_result($sel, 0, "repeat_flg") == "t");
$register_name = pg_fetch_result($sel, 0, "emp_lt_nm").pg_fetch_result($sel, 0, "emp_ft_nm");

// 繰返し登録でも1件のみになっている場合はオプションを表示しない
if ($repeat_flg) {
	require("schedule_repeat_common.php");
	$repeat_schedules = get_repeated_project_schedules($con, $pjt_schd_id, $timeless, 2, $fname);
	if (count($repeat_schedules) == 1) {
		$repeat_flg = false;
	}
}

// 議事録ボタンのHTMLを取得
$cur_url = "project_schedule_update.php?session=$session&pjt_schd_id=$pjt_schd_id&pjt_id=$pjt_id&date=$date&time=$time&timeless=$timeless";
$prcd_btn = get_prcd_btn_html($con, $pjt_schd_id, $timeless, $emp_id, $session, $cur_url, $fname);

// ログインユーザが委員会責任者かどうかチェック
$sql = "select count(*) from project";
$cond = "where pjt_id = $pjt_id and pjt_response = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$response_flg = (intval(pg_fetch_result($sel, 0, 0)) > 0);

// ログインユーザが事務局メンバーかどうかチェック
$sql = "select count(*) from promember";
$cond = "where pjt_id = $pjt_id and member_kind = '1' and emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$secretariat_flg = (intval(pg_fetch_result($sel, 0, 0)) > 0);
?>
<script language="javascript" type="text/javascript">
function initPage() {
	setDayOptions('<? echo($day); ?>');
	setTimeDisabled();
}

function setDayOptions(selectedDay) {
	var dayPulldown = document.regist.schd_start_day;
	if (!selectedDay) {
		selectedDay = dayPulldown.value;
	}
	deleteAllOptions(dayPulldown);

	var year = parseInt(document.regist.schd_start_yrs.value, 10);
	var month = parseInt(document.regist.schd_start_mth.value, 10);
	var daysInMonth = new Date(year, month, 0).getDate();

	for (var d = 1; d <= daysInMonth; d++) {
		var tmpDayValue = d.toString();
		if (tmpDayValue.length == 1) {
			tmpDayValue = '0'.concat(tmpDayValue);
		}

		var tmpDayText = d.toString();

		var tmpDate = new Date(year, month - 1, d);
		var tmpWeekDay = tmpDate.getDay();
		switch (tmpWeekDay) {
		case 0:
			tmpDayText = tmpDayText.concat('（日）');
			break;
		case 1:
			tmpDayText = tmpDayText.concat('（月）');
			break;
		case 2:
			tmpDayText = tmpDayText.concat('（火）');
			break;
		case 3:
			tmpDayText = tmpDayText.concat('（水）');
			break;
		case 4:
			tmpDayText = tmpDayText.concat('（木）');
			break;
		case 5:
			tmpDayText = tmpDayText.concat('（金）');
			break;
		case 6:
			tmpDayText = tmpDayText.concat('（土）');
			break;
		}

		addOption(dayPulldown, tmpDayValue, tmpDayText);
	}

	while (parseInt(selectedDay, 10) > daysInMonth) {
		selectedDay = (parseInt(selectedDay, 10) - 1).toString();
	}
	dayPulldown.value = selectedDay;
}

function setDateDisabled(disabled) {
	document.regist.schd_start_yrs.disabled = disabled;
	document.regist.schd_start_mth.disabled = disabled;
	document.regist.schd_start_day.disabled = disabled;
}

function setTimeDisabled() {
	var disabled = document.regist.timeless.checked;
	document.regist.schd_start_hrs.disabled = disabled;
	document.regist.schd_start_min.disabled = disabled;
	document.regist.schd_dur_hrs.disabled = disabled;
	document.regist.schd_dur_min.disabled = disabled;
}

function deleteSchedule() {
	if (confirm('削除します。よろしいですか？')) {
		var rptopt = '';
		if (document.regist.rptopt) {
			for (var i = 0, j = document.regist.rptopt.length; i < j; i++) {
				if (document.regist.rptopt[i].checked) {
					rptopt = i + 1;
					break;
				}
			}
		}
		location.href = 'project_schedule_delete_checker.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&pjt_schd_id=<? echo($pjt_schd_id); ?>&timeless=<? echo($timeless); ?>&rptopt='.concat(rptopt);
	}
}

function checkEndMinute() {
	if (document.regist.schd_dur_hrs.value == '24' &&
		document.regist.schd_dur_min.value != '00') {
		document.regist.schd_dur_min.value = '00';
		alert('終了時刻が24時の場合には00分しか選べません。');
	}
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.inner td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>スケジュール更新</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="regist" action="project_schedule_update_checker.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="120" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td><input type="text" name="schd_title" maxlength="100" size="65" value="<? echo($schd_title); ?>" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行先</font></td>
<td><select name="place_id"><option value=""<? if ($place_id == "") {echo(" selected");} ?>></option><? show_schedule_place_selected($con, $place_id, $fname); ?><option value="0"<? if ($place_id == "0") {echo(" selected");} ?>>その他</option></select>&nbsp;<input type="text" name="schd_plc" maxlength="100" size="35" value="<? echo($schd_plc); ?>" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="schd_start_yrs" onchange="setDayOptions();"><? show_select_years_span(min($year, 2000), date("Y") + 3, $year); ?></select>/<select name="schd_start_mth" onchange="setDayOptions();"><? show_select_months($month); ?></select>/<select name="schd_start_day"></select></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

<select name="schd_start_hrs"><? show_hour_options_0_23($schd_start_hrs, false); ?></select>：<select name="schd_start_min"><? show_min_options_5($schd_start_min) ?></select>〜<select name="schd_dur_hrs" onchange="checkEndMinute();"><? show_select_hrs_by_args(0, 24, $schd_end_hrs, false); ?></select>：<select name="schd_dur_min" onchange="checkEndMinute();"><? show_min_options_5($schd_end_min) ?></select><br>
<!--
<select name="schd_start_hrs"><? show_hour_options_0_23($schd_start_hrs, false); ?></select>：<select name="schd_start_min"><option value="00"<? if ($schd_start_min == "00") {echo(" selected");} ?>>00<option value="30"<? if ($schd_start_min == "30") {echo(" selected");} ?>>30</select>〜<select name="schd_dur_hrs" onchange="checkEndMinute();"><? show_select_hrs_by_args(0, 24, $schd_end_hrs, false); ?></select>：<select name="schd_dur_min" onchange="checkEndMinute();"><option value="00"<? if ($schd_end_min == "00") {echo(" selected");} ?>>00<option value="30"<? if ($schd_end_min == "30") {echo(" selected");} ?>>30</select><br>
-->

<input type="checkbox" name="timeless" value="t"<? if ($timeless == "t") {echo(" checked");} ?> onclick="setTimeDisabled();">指定しない
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_proschd_type_radio($con, $schd_type, $fname); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">詳細</font></td>
<td><textarea name="schd_detail" rows="5" cols="40" style="ime-mode:active;"><? echo($schd_detail); ?></textarea></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録者</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($register_name); ?></font></td>
</tr>


<? if ($repeat_flg) { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">繰返しオプション</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="rptopt" value="1" checked onclick="setDateDisabled(false);">1件のみ
<input type="radio" name="rptopt" value="2" onclick="setDateDisabled(true);">全て
<input type="radio" name="rptopt" value="3" onclick="setDateDisabled(true);">将来の予定のみ<br>
<font color="red">※この予定は繰返し登録されています。更新／削除時にはこのオプションが有効になります。</font>
</font></td>
</tr>
<? } ?>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right">
<? echo("$prcd_btn\n"); ?>

<?

// 議事録の承認状態を取得
$sql = "select  prcd_status from proceeding";
$cond = "where pjt_schd_id = '$pjt_schd_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$prcd_status = pg_fetch_result($sel, 0, "prcd_status");


//一部承認されていれば更新させない
$sql = "select pjt_schd_id from prcdaprv";
$cond = "where pjt_schd_id = '$pjt_schd_id' and prcdaprv_date is not null and prcdaprv_date != '00000000'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$part_approve_cnt = pg_numrows($sel);


if((($prcd_status != "3") && ($part_approve_cnt < 1))
&& (($schd_emp_id == $emp_id || $schd_prcd_emp_id == $emp_id || $response_flg || $secretariat_flg)))
{
	//差戻し以外であり、一人も承認していなく、スケジュール作成者か議事録作成者か責任者か事務職メンバーの場合は削除ボタンを表示

//	if($schd_emp_id == $emp_id)
	if($schd_emp_id == $emp_id || $schd_prcd_emp_id == $emp_id || $response_flg || $secretariat_flg)
	{
		//スケジュールの更新はスケジュール作成者か議事録作成者か責任者か事務職メンバー
?>
<input type="submit" name="submit" value="更新">
<?
	}
?>
<input type="button" value="削除" onclick="deleteSchedule();">
<?
}elseif(($prcd_status == "3")&&
	($schd_emp_id == $emp_id || $schd_prcd_emp_id == $emp_id || $response_flg || $secretariat_flg))
{
	//差戻しの場合は削除ボタンも表示する
?>
<input type="button" value="削除" onclick="deleteSchedule();">
<?
}
?>

</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pjt_schd_id" value="<? echo($pjt_schd_id); ?>">
<input type="hidden" name="original_timeless" value="<? echo($timeless); ?>">
<input type="hidden" name="pjt_id" value="<? echo($pjt_id); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
<input type="hidden" name="time" value="<? echo($time); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
