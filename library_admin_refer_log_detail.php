<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 63, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// デフォルト値の設定
if ($page == "") {$page = 1;}  // ページ数：1

// データベースに接続
$con = connect2db($fname);

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$intra_menu2_3 = pg_fetch_result($sel, 0, "menu2_3");

// 文書名を取得
$sql = "select lib_nm from libinfo";
$cond = "where lib_id = $lib_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$lib_nm = pg_fetch_result($sel, 0, "lib_nm");
?>
<title>CoMedix <? echo($intra_menu2_3); ?> | 参照履歴詳細</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
table.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>参照履歴詳細</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo($lib_nm); ?></b></font></td>
</tr>
</table>
<? show_library_list($con, $lib_id, $page, $session, $fname); ?>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
function show_library_list($con, $lib_id, $page, $session, $fname) {
	$limit = 20;

	// ログ数を取得（ページング処理のため）
	$sql = "select count(*) from libreflog";
	$cond = "where lib_id = $lib_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$log_count = intval(pg_fetch_result($sel, 0, 0));
	if ($log_count == 0) {
		return;
	}

	// ログ情報の取得
	$sql = "select libreflog.emp_id, libreflog.ref_date, empmst.emp_lt_nm, empmst.emp_ft_nm from libreflog left join empmst on libreflog.emp_id = empmst.emp_id";
	$cond = "where libreflog.lib_id = $lib_id order by libreflog.ref_date desc";
	$offset = $limit * ($page - 1);
	$cond .= " offset $offset limit $limit";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// ページ番号の表示
	if ($log_count > $limit) {
		echo("<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin:5px 0 2px;\">\n");
		echo("<tr>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">\n");

		if ($page > 1) {
			echo("<a href=\"$fname?session=$session&lib_id=$lib_id&page=" . ($page - 1) . "\">＜前</a>　\n");
		} else {
			echo("＜前　\n");
		}

		$page_count = ceil($log_count / $limit);
		for ($i = 1; $i <= $page_count; $i++) {
			if ($i != $page) {
				echo("<a href=\"$fname?session=$session&lib_id=$lib_id&page=$i\">$i</a>\n");
			} else {
				echo("<b>$i</b>\n");
			}
		}

		if ($page < $page_count) {
			echo("　<a href=\"$fname?session=$session&lib_id=$lib_id&page=" . ($page + 1) . "\">次＞</a>\n");
		} else {
			echo("　次＞\n");
		}

		echo("</font></td>\n");
		echo("</tr>\n");

		echo("</table>\n");
	} else {
		echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"5\"><br>\n");
	}

	// ログ一覧の表示
	echo("<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
	echo("<tr height=\"22\" bgcolor=\"#f6f9ff\">\n");
	echo("<td width=\"40%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">参照日時</font></td>\n");
	echo("<td width=\"60%\" style=\"padding-right:4px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">参照者</font></td>\n");
	echo("</tr>\n");
	while ($row = pg_fetch_array($sel)) {
		$tmp_ref_date = format_datetime($row["ref_date"]);
		if ($row["emp_id"] != "") {
			$tmp_emp_nm = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];
		} else {
			$tmp_emp_nm = "ログイン画面から参照";
		}

		echo("<tr height=\"22\">\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_ref_date</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_emp_nm</font></td>\n");
		echo("</tr>\n");
	}
	echo("</table>\n");
}

function format_datetime($datetime) {
	return preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3　$4:$5", $datetime);
}
?>
