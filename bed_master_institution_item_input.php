<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("bed_institemrireki.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 21, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

//画面情報
$page_title = "診療科登録";
$submit_name = "登録";
if($mode == "update")
{
	$page_title = "診療科更新";
	$submit_name = "更新";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//ポストバック時の処理
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if($is_postback == "true")
{
	//==============================
	//入力チェック
	//==============================
	$err_msg = "";

	//診療科コード:必須チェック
	if($err_msg == "")
	{
		if($item_cd == "")
		{
			$err_msg = "診療科コードを入力してください。";
		}
	}
	//診療科コード:文字種、文字長チェック
	if($err_msg == "")
	{
		if (!preg_match("/^[a-zA-Z0-9]+$/", $item_cd) || (strlen($item_cd) > 4))
		{
			$err_msg = "診療科コードは半角英数４桁以内で入力してください。";
		}
	}
	//診療科コード:重複チェック
	if($err_msg == "")
	{
		$sql  = "select count(*) as cnt from institem";
		$cond = "where mst_cd = '$mst_cd' and item_cd = '$item_cd' and item_cd != '$target_item_cd'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$cnt = pg_fetch_result($sel, 0, "cnt");
		if ($cnt > 0) {
			$err_msg = "診療科コード「".$item_cd."」は既に使用されております。";
		}
	}
	//診療科名称:必須チェック
	if($err_msg == "")
	{
		if($item_name == "")
		{
			$err_msg = "診療科名称を入力してください。";
		}
	}

	//診療科名称:長さチェック
	if (strlen($item_name) > 200)
	{
		$err_msg = "診療科名称を全角100文字以内で入力してください。";
	}

	//診療科名称:重複チェック
	if($err_msg == "")
	{
		$sql  = "select count(*) as cnt from institem";
		$cond = "where mst_cd = '$mst_cd' and item_name = '".pg_escape_string($item_name)."' and item_cd != '$target_item_cd'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$cnt = pg_fetch_result($sel, 0, "cnt");
		if ($cnt > 0) {
			$err_msg = "診療科名称「".$item_name."」は既に使用されております。";
		}
	}
	//表示順:数値チェック
	if($err_msg == "")
	{
		if (!preg_match("/^[0-9]*$/", $disp_order) )
		{
			$err_msg ="表示順は数値を入力してください。";
		}
	}

	$doctors = array();
	if ($err_msg == "") {
		for ($i = 1; $i <= 10; $i++) {
			$doctor_name_var = "doctor_name$i";
			$doctor_hide_var = "doctor_hide$i";
			$doctor_order_var = "doctor_order$i";

			if ($$doctor_name_var == "") {
				continue;
			}

			if (strlen($$doctor_name_var) > 60) {
				$err_msg = "医師氏名は全角30文字以内で入力してください。";
				break;
			}

			if (!preg_match("/^[0-9]*$/", $$doctor_order_var)) {
				$err_msg = "医師の表示順は数値を入力してください。";
				break;
			}

			$doctors[] = array(
				"name" => $$doctor_name_var,
				"hide" => $$doctor_hide_var,
				"order" => $$doctor_order_var
			);
		}
	}

	usort($doctors, "sort_doctors");

	if($err_msg != "")
	{
		//==============================
		//エラー表示
		//==============================
		echo("<script language='javascript'>");
		echo("alert('$err_msg');");
		echo("</script>");
	}
	else
	{
		//==============================
		//登録処理
		//==============================
		if($mode == "insert")
		{
			// トランザクションの開始
			pg_query($con, "begin");

			$sql = "insert into institem (mst_cd, item_cd, item_name, disp_flg, disp_order, doctor_name1, doctor_hide1, doctor_order1, doctor_name2, doctor_hide2, doctor_order2, doctor_name3, doctor_hide3, doctor_order3, doctor_name4, doctor_hide4, doctor_order4, doctor_name5, doctor_hide5, doctor_order5, doctor_name6, doctor_hide6, doctor_order6, doctor_name7, doctor_hide7, doctor_order7, doctor_name8, doctor_hide8, doctor_order8, doctor_name9, doctor_hide9, doctor_order9, doctor_name10, doctor_hide10, doctor_order10, create_time, create_emp_id, update_time, update_emp_id) values (";
			$content = array(
				$mst_cd,
				$item_cd,
				pg_escape_string($item_name),
				$disp_flg,
				intval($disp_order),
				pg_escape_string($doctors[0]["name"]),
				($doctors[0]["hide"] != "t") ? "f" : "t",
				($doctors[0]["order"] == "") ? null : intval($doctors[0]["order"]),
				pg_escape_string($doctors[1]["name"]),
				($doctors[1]["hide"] != "t") ? "f" : "t",
				($doctors[1]["order"] == "") ? null : intval($doctors[1]["order"]),
				pg_escape_string($doctors[2]["name"]),
				($doctors[2]["hide"] != "t") ? "f" : "t",
				($doctors[2]["order"] == "") ? null : intval($doctors[2]["order"]),
				pg_escape_string($doctors[3]["name"]),
				($doctors[3]["hide"] != "t") ? "f" : "t",
				($doctors[3]["order"] == "") ? null : intval($doctors[3]["order"]),
				pg_escape_string($doctors[4]["name"]),
				($doctors[4]["hide"] != "t") ? "f" : "t",
				($doctors[4]["order"] == "") ? null : intval($doctors[4]["order"]),
				pg_escape_string($doctors[5]["name"]),
				($doctors[5]["hide"] != "t") ? "f" : "t",
				($doctors[5]["order"] == "") ? null : intval($doctors[5]["order"]),
				pg_escape_string($doctors[6]["name"]),
				($doctors[6]["hide"] != "t") ? "f" : "t",
				($doctors[6]["order"] == "") ? null : intval($doctors[6]["order"]),
				pg_escape_string($doctors[7]["name"]),
				($doctors[7]["hide"] != "t") ? "f" : "t",
				($doctors[7]["order"] == "") ? null : intval($doctors[7]["order"]),
				pg_escape_string($doctors[8]["name"]),
				($doctors[8]["hide"] != "t") ? "f" : "t",
				($doctors[8]["order"] == "") ? null : intval($doctors[8]["order"]),
				pg_escape_string($doctors[9]["name"]),
				($doctors[9]["hide"] != "t") ? "f" : "t",
				($doctors[9]["order"] == "") ? null : intval($doctors[9]["order"]),
				date("YmdHis"),
				$emp_id,
				date("YmdHis"),
				$emp_id);
			$in = insert_into_table($con,$sql,$content,$fname);
			if($in == 0){
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}

			//履歴更新
			entry_institemrireki($con,$fname,$mst_cd);

			// トランザクションのコミット
			pg_query($con, "commit");

		}
		//==============================
		//更新処理
		//==============================
		if($mode == "update")
		{
			// トランザクションの開始
			pg_query($con, "begin");

			$sql = "update institem set";
			$set = array("item_cd", "item_name", "disp_flg", "disp_order", "doctor_name1", "doctor_hide1", "doctor_order1", "doctor_name2", "doctor_hide2", "doctor_order2", "doctor_name3", "doctor_hide3", "doctor_order3", "doctor_name4", "doctor_hide4", "doctor_order4", "doctor_name5", "doctor_hide5", "doctor_order5", "doctor_name6", "doctor_hide6", "doctor_order6", "doctor_name7", "doctor_hide7", "doctor_order7", "doctor_name8", "doctor_hide8", "doctor_order8", "doctor_name9", "doctor_hide9", "doctor_order9", "doctor_name10", "doctor_hide10", "doctor_order10", "update_time", "update_emp_id");
			$setvalue = array(
				$item_cd,
				pg_escape_string($item_name),
				$disp_flg,
				intval($disp_order),
				pg_escape_string($doctors[0]["name"]),
				($doctors[0]["hide"] != "t") ? "f" : "t",
				($doctors[0]["order"] == "") ? null : intval($doctors[0]["order"]),
				pg_escape_string($doctors[1]["name"]),
				($doctors[1]["hide"] != "t") ? "f" : "t",
				($doctors[1]["order"] == "") ? null : intval($doctors[1]["order"]),
				pg_escape_string($doctors[2]["name"]),
				($doctors[2]["hide"] != "t") ? "f" : "t",
				($doctors[2]["order"] == "") ? null : intval($doctors[2]["order"]),
				pg_escape_string($doctors[3]["name"]),
				($doctors[3]["hide"] != "t") ? "f" : "t",
				($doctors[3]["order"] == "") ? null : intval($doctors[3]["order"]),
				pg_escape_string($doctors[4]["name"]),
				($doctors[4]["hide"] != "t") ? "f" : "t",
				($doctors[4]["order"] == "") ? null : intval($doctors[4]["order"]),
				pg_escape_string($doctors[5]["name"]),
				($doctors[5]["hide"] != "t") ? "f" : "t",
				($doctors[5]["order"] == "") ? null : intval($doctors[5]["order"]),
				pg_escape_string($doctors[6]["name"]),
				($doctors[6]["hide"] != "t") ? "f" : "t",
				($doctors[6]["order"] == "") ? null : intval($doctors[6]["order"]),
				pg_escape_string($doctors[7]["name"]),
				($doctors[7]["hide"] != "t") ? "f" : "t",
				($doctors[7]["order"] == "") ? null : intval($doctors[7]["order"]),
				pg_escape_string($doctors[8]["name"]),
				($doctors[8]["hide"] != "t") ? "f" : "t",
				($doctors[8]["order"] == "") ? null : intval($doctors[8]["order"]),
				pg_escape_string($doctors[9]["name"]),
				($doctors[9]["hide"] != "t") ? "f" : "t",
				($doctors[9]["order"] == "") ? null : intval($doctors[9]["order"]),
				date("YmdHis"),
				$emp_id);
			$cond = "where mst_cd = '$mst_cd' and item_cd = '$target_item_cd'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			//履歴更新
			entry_institemrireki($con,$fname,$mst_cd);

			// トランザクションのコミット
			pg_query($con, "commit");

		}
		//==============================
		//画面終了
		//==============================
		echo("<script language='javascript'>");
		echo("if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}");
		echo("window.close();");
		echo("</script>");
		exit;
	}
}
//初回アクセス時
else
{
	$target_item_cd = $item_cd;

	if($mode == "update")
	{
		//現在値の取得／変数展開
		$sql = "select * from institem";
		$cond = "where mst_cd = '$mst_cd' and item_cd = '$target_item_cd'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$item_info = pg_fetch_array($sel);
		foreach($item_info as $key => $val)
		{
			$$key = $val;
		}
	}
}

function sort_doctors($doctor1, $doctor2) {
	if ($doctor1["order"] == $doctor2["order"]) {
		return 0;
	}
	return  (intval($doctor1["order"]) < intval($doctor2["order"])) ? -1 : 1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//HTML出力
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>
<title>病床管理 | マスタ管理 | <?=$page_title?></title>

<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td, .inner td .list td {border:#5279a5 solid 1px;}
.inner td {border-style:none;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<form name="mainform" action="bed_master_institution_item_input.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="mode" value="<?=$mode?>">
<input type="hidden" name="mst_cd" value="<?=$mst_cd?>">
<input type="hidden" name="target_item_cd" value="<?=$target_item_cd?>">
<input type="hidden" name="is_postback" value="true">
<table width="720" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$page_title?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="720" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22">
<td bgcolor="#f6f9ff" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科コード</font></td>
<td><input type="text" name="item_cd" value="<?=$item_cd?>" maxlength="4" size="6" style="ime-mode: inactive;"></td>
</td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科名称</font></td>
<td><input type="text" name="item_name" value="<?=$item_name?>" maxlength="200" size="46" style="ime-mode: active;"></td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示区分</font></td>
<td>
<select name="disp_flg">
<option value="t" <?if($disp_flg != "f"){?>selected<?}?> >表示</option>
<option value="f" <?if($disp_flg == "f"){?>selected<?}?> >非表示</option>
</select>
</td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示順</font></td>
<td><input type="text" name="disp_order" value="<?=$disp_order?>" maxlength="4" size="4" style="ime-mode: inactive;"></td>
</tr>

<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">医師</font></td>
<td style="padding:4px;">
<table cellspacing="0" cellpadding="2" class="list" width="250">
<tr height="22" bgcolor="#f6f9ff" align="center">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">医師氏名</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非表示</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示順</font></td>
</tr>
<?
for ($i = 1; $i <= 10; $i++) {
	$doctor_name_var = "doctor_name$i";
	$doctor_hide_var = "doctor_hide$i";
	$doctor_order_var = "doctor_order$i";
?>
<tr height="22">
<td><input type="text" name="doctor_name<?=$i?>" value="<?=$$doctor_name_var?>" maxlength="60" size="20" style="ime-mode: active;"></td>
<td align="center"><input type="checkbox" name="doctor_hide<?=$i?>" value="t"<? if ($$doctor_hide_var == "t") {echo(" checked");} ?>></td>
<td align="center"><input type="text" name="doctor_order<?=$i?>" value="<?=$$doctor_order_var?>" maxlength="2" size="3" style="ime-mode:inactive;"></td>
</tr>
<?
}
?>
</table>
</td>
</tr>

<?
if($mode == "update")
{
?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新日付</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?=substr($update_time,0,4)."/".substr($update_time,4,2)."/".substr($update_time,6,2)?>
<input type="hidden" name="update_time" value="<?=$update_time?>">
</font></td>
</tr>
<?
}
?>

</table>
<table width="720" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="22" align="right"><input type="submit" value="<?=$submit_name?>"></td>
</tr>
</table>
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
