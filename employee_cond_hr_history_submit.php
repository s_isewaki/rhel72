<?php
require_once("about_comedix.php");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 18, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 職員登録権限を取得
$reg_auth = check_authority($session, 19, $fname);

$emp_id = $_POST["emp_id"];

// 入力内容のチェック
//個人別勤務時間帯
for ($i=0; $i<9; $i++) {
	$varname1 = "start_hour".$i."_2";
	$varname2 = "start_min".$i."_2";
	$varname3 = "end_hour".$i."_2";
    $varname4 = "end_min".$i."_2";
    $varname5 = "start_hour".$i."_4";
    $varname6 = "start_min".$i."_4";
    $varname7 = "end_hour".$i."_4";
    $varname8 = "end_min".$i."_4";
    
    if (($$varname1 == "--" && $$varname2 != "--") || ($$varname1 != "--" && $$varname2 == "--") ||
            ($$varname3 == "--" && $$varname4 != "--") || ($$varname3 != "--" && $$varname4 == "--") ||
            ($$varname5 == "--" && $$varname6 != "--") || ($$varname5 != "--" && $$varname6 == "--") ||
            ($$varname7 == "--" && $$varname8 != "--") || ($$varname7 != "--" && $$varname8 == "--") ) {
        echo("<script type=\"text/javascript\">alert('時間と分の一方のみは登録できません。');</script>");
        echo("<script language=\"javascript\">history.back();</script>");
        exit;
    }
    if (($$varname1 == "--"  && $$varname3 != "--") || ($$varname1 != "--"  && $$varname3 == "--") ||
            ($$varname5 == "--"  && $$varname7 != "--") || ($$varname5 != "--"  && $$varname7 == "--")) {
        echo("<script type=\"text/javascript\">alert('開始時刻と終了時刻の一方のみは登録できません。');</script>");
        echo("<script language=\"javascript\">history.back();</script>");
        exit;
    }
    if (($$varname1 == "--"  && $$varname5 != "--")) {
        echo("<script type=\"text/javascript\">alert('休憩時間のみは登録できません。');</script>");
        echo("<script language=\"javascript\">history.back();</script>");
        exit;
    }
}

//基本(8)以外の曜日のデータ件数 日0,月1,土6、祝7
$sub_rec_cnt = 0;
$arr_officehours_i = array();
for ($i=0; $i<9; $i++) {
    $varname1 = "start_hour".$i."_2";
    $varname2 = "start_min".$i."_2";
    $varname3 = "end_hour".$i."_2";
    $varname4 = "end_min".$i."_2";
    $varname5 = "start_hour".$i."_4";
    $varname6 = "start_min".$i."_4";
    $varname7 = "end_hour".$i."_4";
    $varname8 = "end_min".$i."_4";
    
    if ($$varname1 != "--") {
        $arr_officehours_i[$i]["officehours2_start"] = $$varname1.":".$$varname2;
        $arr_officehours_i[$i]["officehours2_end"] = $$varname3.":".$$varname4;
        $arr_officehours_i[$i]["officehours4_start"] = ($$varname5 != "--") ? $$varname5.":".$$varname6 : "";
        $arr_officehours_i[$i]["officehours4_end"] = ($$varname7 != "--") ? $$varname7.":".$$varname8 : "";
        if ($i < 8) {
            $sub_rec_cnt++;
        } 
    }
}
//全て未設定の場合を確認
if ($arr_officehours_i[8]["officehours2_start"] == "" && $sub_rec_cnt == 0) {
    echo("<script type=\"text/javascript\">alert('基本時間と曜日別時間の1件以上の時間帯を指定してください。');</script>");
    echo("<script language=\"javascript\">history.back();</script>");
    exit;
}
if (!preg_match("/^\d\d\d\d\-\d\d\-\d\d \d\d:\d\d:\d\d$/", $_POST["histdate"])) {
    echo("<script type=\"text/javascript\">alert('日付が正しく入力されていません。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}

// データベースに接続
$con = connect2db($fname);
pg_query($con, "begin");

// UPDATE
if (!empty($_POST["empcond_officehours_history_id"])) {
    $sql = "UPDATE empcond_officehours_history SET ";
    $cond = "WHERE empcond_officehours_history_id={$_POST["empcond_officehours_history_id"]}";
    $upd = update_set_table($con, $sql, array('emp_id', 'officehours2_start', 'officehours2_end', 'officehours4_start', 'officehours4_end', 'sub_rec_cnt', 'histdate'), array($emp_id, $arr_officehours_i[8]["officehours2_start"], $arr_officehours_i[8]["officehours2_end"], $arr_officehours_i[8]["officehours4_start"], $arr_officehours_i[8]["officehours4_end"], $sub_rec_cnt, $_POST["histdate"]), $cond, $fname);
    if ($upd == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    //既存の曜日別を削除
    $sql = "delete from empcond_officehours_history_sub";
    $cond = "where empcond_officehours_history_sub_id = {$_POST["empcond_officehours_history_id"]} ";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    //曜日別を登録
    if ($sub_rec_cnt > 0) {
        $histdate = date("Y-m-d H:i:s");
        for ($i=0; $i<8; $i++) {
            if ($arr_officehours_i[$i]["officehours2_start"] != "") {
                $sql = "INSERT INTO empcond_officehours_history_sub (empcond_officehours_history_sub_id, emp_id, weekday, officehours2_start, officehours2_end, officehours4_start, officehours4_end) VALUES (";
                $content = array($_POST["empcond_officehours_history_id"], $emp_id, $i, $arr_officehours_i[$i]["officehours2_start"],  $arr_officehours_i[$i]["officehours2_end"], $arr_officehours_i[$i]["officehours4_start"], $arr_officehours_i[$i]["officehours4_end"]);
                $ins = insert_into_table($con, $sql, $content, $fname);
                if ($ins == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
            }
        }
        
    }
}
// INSERT
else {
    $histdate = date("Y-m-d H:i:s");

    $sql = "INSERT INTO empcond_officehours_history (emp_id, weekday, officehours2_start, officehours2_end, officehours4_start, officehours4_end, sub_rec_cnt, histdate) VALUES (";
    $content = array($emp_id, 0, $arr_officehours_i[8]["officehours2_start"],  $arr_officehours_i[8]["officehours2_end"], $arr_officehours_i[8]["officehours4_start"], $arr_officehours_i[8]["officehours4_end"], $sub_rec_cnt, $histdate);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if ($sub_rec_cnt > 0) {
        //hist_id取得
        $sql = "select max(empcond_officehours_history_id) as hist_id from empcond_officehours_history";
        $cond = "where emp_id = '$emp_id'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $hist_id = pg_fetch_result($sel, 0, "hist_id");
        
        for ($i=0; $i<8; $i++) {
            if ($arr_officehours_i[$i]["officehours2_start"] != "") {
                $sql = "INSERT INTO empcond_officehours_history_sub (empcond_officehours_history_sub_id, emp_id, weekday, officehours2_start, officehours2_end, officehours4_start, officehours4_end) VALUES (";
                $content = array($hist_id, $emp_id, $i, $arr_officehours_i[$i]["officehours2_start"],  $arr_officehours_i[$i]["officehours2_end"], $arr_officehours_i[$i]["officehours4_start"], $arr_officehours_i[$i]["officehours4_end"]);
                $ins = insert_into_table($con, $sql, $content, $fname);
                if ($ins == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
            }
        }
    }
}

pg_query($con, "commit");
pg_close($con);
?>
<script type="text/javascript">
location.href = 'employee_condition_setting.php?session=<?=$session?>&emp_id=<?=$emp_id?>';
</script>