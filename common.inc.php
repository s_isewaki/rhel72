<?
require_once("./about_postgres.php");
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$select_id="select emp_id from login where emp_id in(select emp_id from session where session_id='$session')";
//echo($select_id);
$result_select=pg_exec($con, $select_id);
if($result_select==false){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$emp_id=pg_result($result_select,0,"emp_id");

$select_server="select * from server where emp_id='$emp_id'";
$result_select_server = pg_exec($con,$select_server);
if($result_select_server == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$num=pg_numrows($result_select_server);
if($num>0){
$server_pop3=pg_result($result_select_server,0,"pop3");
$server_smtp=pg_result($result_select_server,0,"smtp");
$account_name=pg_result($result_select_server,0,"account_name");
$password=pg_result($result_select_server,0,"password");
$port_pop3=pg_result($result_select_server,0,"port_pop3");
$port_smtp=pg_result($result_select_server,0,"port_smtp");
$server_page=pg_result($result_select_server,0,"page");
}
$select_empmst="select emp_email from empmst where emp_id='$emp_id'";
$result_select_empmst = pg_exec($con,$select_empmst);
if($result_select_empmst == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$num1=pg_numrows($result_select_empmst);
if($num1>0){
$emp_email=pg_result($result_select_empmst,0,"emp_email");
}
?>
<?

//  include_pathにローカルのPEARシステムを追加する
define( "_SYS_ROOT_DIR_", dirname( __FILE__)."/");
$include_path[] = _SYS_ROOT_DIR_."";
ini_set( 'include_path', implode( ":", $include_path));

require_once 'Net/POP3.php';
require_once 'Mail.php';
require_once 'Mail/RFC822.php';
require_once 'Mail/mime.php';
require_once 'Mail/mimePart.php';
require_once 'Mail/mimeDecode.php';

/**
* SMTPの設定
*/
//  SMTPサーバのアドレス
$config[ 'smtp'][ 'host'] = $server_smtp;
//  SMTPサーバのポート番号
$config[ 'smtp'][ 'port'] = $port_smtp;
//  SMTP認証を使用するか
$config[ 'smtp'][ 'auth'] = false;
/**
* POP3の設定
*/
//  POP3サーバのアドレス
$config[ 'pop3'][ 'host'] = $server_pop3;
//  POP3サーバのポート番号
$config[ 'pop3'][ 'port'] = $port_pop3;
//  POP3認証のアカウント名
$config[ 'pop3'][ 'user'] = $account_name;
//  POP3認証のパスワード
$config[ 'pop3'][ 'pass'] = $password;
//  認証にAPOPを使用するかどうか
$config[ 'pop3'][ 'apop'] = false;
/**
* メーラの設定
*/
// 自分のメールアドレス
$config[ 'mua'][ 'fromAddress'] = $emp_email;
// メール一覧で表示する個数
$config[ 'mua'][ 'showList'] = $server_page;
// ドメイン名が省略された場合に付加するドメイン名
$config[ 'mua'][ 'defaultDomain'] = $server_pop3;
// 表示上(HTML出力上)の文字コード
//$config[ 'mua'][ 'showCharSet'] = 'euc-jp';
$config[ 'mua'][ 'showCharSet'] = 'euc-jp';
// メールに使用する文字コード
$config[ 'mua'][ 'mailCharSet'] = 'iso-2022-jp';
// アップロード可能なファイルサイズ
$config[ 'mua'][ 'uploadMaxSize'] = 1000000;

mb_language( 'Japanese');
mb_internal_encoding( $config[ 'mua'][ 'showCharSet']);

class cMyPop3 extends Net_POP3 {
  //  設定のリファレンス
  var $config;

  /**
   *  コンストラクタ
   */
  function cMyPop3() {
    global $config;
    $this->config =& $config;
    parent::Net_POP3();
  }

  /**
   *  接続処理を行う
   */
  function connect() {
    $config =& $this->config[ 'pop3'];
    return parent::connect( $config[ 'host'], $config[ 'port'], $config[ 'apop']);
  }

  /**
   *  認証を行う
   */
  function login() {
    $config =& $this->config[ 'pop3'];
    return parent::login( $config[ 'user'], $config[ 'pass'], $config[ 'apop']);
  }
}

/**
*  メールアドレスのリストからコメントを削除し，メールアドレス一覧だけにする
*/
function exclude_mail_comment( $addressList, $defaultDomain) {
  $parsedAddress = Mail_RFC822::parseAddressList( $addressList, $defaultDomain, true);
  $newList = array();
  foreach ( $parsedAddress as $info) {
    $newList[] = "{$info->mailbox}@{$info->host}";
  }
  return implode( ',', $newList);
}
/**
*  複数のフィールド値をまとめる
*/
function mail_header_join( $value, $char = ' ') {
  return is_array( $value)? implode( $char, $value): $value;
}

/**
*  メールヘッダの日付を日本語表記にする
*/
function mail_header_date( $value) {
//  return $value !=''? date( "Y年g月j日 G:i:s", strtotime( $value)): '不明';
  return $value !=''?
  strftime( "%Y/%m/%d %H:%M:%S", strtotime( $value)): '不明';

}

/**
*  配列化したメールヘッダの値を解析し，MIMEデコードする
*/
/*function mb_decode_mimeheaders( $headers, $toCharSet) {
  foreach ( $headers as $name=>$value) {
    if ( is_array( $value)) {
      array_walk( $value, 'mb_decode_mimeheaders_callback', $toCharSet);
    } else {
      mb_decode_mimeheaders_callback( $value, $toCharSet);
    }
    $headers[ $name] = $value;
  }
  return $headers;
}
*/
function mb_decode_mimeheaders( $headers, $toCharSet) {
foreach ( $headers as $name=>$value) {
$headers[$name]=is_array($value)?
array_walk($value,'mb_decode_mimeheaders_callback', $toCharSet):
mb_decode_mimeheaders_callback($value,$toCharset);
}
return $headers;
}
/**
*  MIMEヘッダの値をデコードする．生ISO-2022-JPもデコードする
*/
function mb_decode_mimeheaders_callback( $value, $toCharSet) {
  $value = mb_decode_mimeheader( $value);
  if ( strstr( strtoupper( $value), "\x1B\$B")) {
    //  ISO-2022-JPの開始シーケンスなので変換(つまり生JIS)
    $value = mb_convert_encoding( $value, $toCharSet, 'iso-2022-jp');
  }
  return $value;
}

/**
*  Message-Idフィールドの値を生成する
*/
function create_message_id( $baseString) {
  return sprintf( "%s.%s", md5( microtime()), $baseString);
}

/**
*  Dateフィールドの値を生成する
*/
function create_date( $time = null) {
  $time = $time === null? time(): $time;
  return date( 'r', $time);
}

/**
* ツールバー
*/
function toolbar() {
  global $PHP_SELF;
  print <<<EOD
<div>
  <a href="create_mail.php">メール作成へ</a>
  <a href="show_mail.php">メール一覧へ</a>
</div>

EOD;
}

/**
* 開始HTMLを表示する
*/
function html_header() {
  global $config;
  $showCharSet = $config[ 'mua'][ 'showCharSet'];
  print <<<EOD
  <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
  <html>
  <head><meta http-equiv=Content-Type content="text/html; 
charset=$showCharSet"></head>
  <body>
  <h1>簡易メーラ</h1>
EOD;
}

/**
* 終了HTMLを表示する
*/
function html_footer() {
  print <<<EOD
</body>
</html>

EOD;
}


?>
