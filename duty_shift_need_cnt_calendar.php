<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成｜必要人数設定(カレンダー)</title>

<?
require_once("about_comedix.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_user_tab_common.ini");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//管理画面用
$section_admin_auth = $obj->check_authority_Management($session, $fname);
///-----------------------------------------------------------------------------
//初期値設定
///-----------------------------------------------------------------------------
if ($data_cnt == "") { $data_cnt = 0; }
// group_id 全画面対応
if ($group_id == "") { $group_id = $cause_group_id; }
///-----------------------------------------------------------------------------
//日数
///-----------------------------------------------------------------------------
$day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);
$start_day = 1;
$end_day = $day_cnt;
///-----------------------------------------------------------------------------
//ＤＢ(empmst)より職員情報を取得
//ＤＢ(wktmgrp)より勤務パターン情報を取得
//ＤＢ(jobmst)より職種情報取得
///-----------------------------------------------------------------------------
$data_emp = $obj->get_empmst_array("");
$data_wktmgrp = $obj->get_wktmgrp_array();
$data_job = $obj->get_jobmst_array();
///-----------------------------------------------------------------------------
// チーム情報を取得
///-----------------------------------------------------------------------------
$team_array = $obj->get_duty_shift_staff_team_array();
///-----------------------------------------------------------------------------
// カレンダー(calendar)情報を取得
///-----------------------------------------------------------------------------
$start_date = sprintf("%04d%02d%02d",$duty_yyyy, $duty_mm, "01");
$end_date = sprintf("%04d%02d%02d",$duty_yyyy, $duty_mm, $day_cnt);
$calendar_array = $obj->get_calendar_array($start_date, $end_date);
///-----------------------------------------------------------------------------
// 指定グループの出勤パターン(atdptn)情報を取得
///-----------------------------------------------------------------------------
//勤務シフトグループ情報ＤＢより情報取得
$wk_group = $obj->get_duty_shift_group_array($group_id, $data_emp, $data_wktmgrp);
$atdptn_array = array();
$pattern_id = "";
if (count($wk_group) > 0) {
	$pattern_id = $wk_group[0]["pattern_id"];
	$group_name = $wk_group[0]["group_name"];
}
///-----------------------------------------------------------------------------
// 出勤パターン情報を取得
///-----------------------------------------------------------------------------
$atdptn_array = $obj->get_atdptn_array($pattern_id);
$atdptn_cnt = count($atdptn_array);
///-----------------------------------------------------------------------------
//指定月の全曜日を設定
///-----------------------------------------------------------------------------
$week_array = array();
for ($k=1; $k<=$day_cnt; $k++) {
	$tmp_date = mktime(0, 0, 0, $duty_mm, $k, $duty_yyyy);
	$week_array[$k]["name"] = $obj->get_weekday($tmp_date);
}
///-----------------------------------------------------------------------------
//必要人数情報（カレンダー）の取得
///-----------------------------------------------------------------------------
$need_array = array();
if ($data_cnt > 0) {
	///-----------------------------------------------------------------------------
	// 再表示時、データ再設定
	///-----------------------------------------------------------------------------
	$m=0;
	for($i=0; $i<$data_cnt; $i++) {
		//削除行以外
		$wk_flg = "";
		if ($del_idx == "") {
			$wk_flg = "1";
		} else {
			if ($i != $del_idx) {
				$wk_flg = "1";
			}
		}
		if ($wk_flg == "1") {
			$need_array[$m]["group_id"] = $group_id;					//勤務シフトパターンＩＤ
			$need_array[$m]["atdptn_ptn_id"] = $atdptn_ptn_id[$i];		//出勤パターンＩＤ
			$need_array[$m]["job_id"] = $job_id[$i];					//職種ＩＤ
			$need_array[$m]["job_id2"] = $job_id2[$i];					//職種ＩＤ
			$need_array[$m]["job_id3"] = $job_id3[$i];					//職種ＩＤ
			$need_array[$m]["team"] = $team[$i];						//チーム
			$need_array[$m]["sex"] = $sex[$i];							//性別
			//必要人数
			for($k=1; $k<=$day_cnt; $k++) {
				$need_array[$m]["need_cnt"][$k] = $need_cnt[$k][$i];
			}
			$m++;
		}
		//追加時
		if (($add_idx != "") && ($i == $add_idx)) {
			$need_array[$m]["group_id"] = $group_id;				//勤務シフトパターンＩＤ
			$need_array[$m]["atdptn_ptn_id"] = $atdptn_ptn_id[$i];	//出勤パターンＩＤ
			$need_array[$m]["job_id"] = "";							//職種ＩＤ
			$need_array[$m]["job_id2"] = "";						//職種ＩＤ
			$need_array[$m]["job_id3"] = "";						//職種ＩＤ
			$need_array[$m]["team"] = 0;							//チーム
			$need_array[$m]["sex"] = "";							//性別
			//必要人数
			for($k=start_day; $k<=$end_cnt; $k++) {
				$need_array[$m]["need_cnt"][$k] = "";
			}
			$m++;
		}
	}
} else {
	///-----------------------------------------------------------------------------
	//必要人数情報（カレンダー）の取得
	///-----------------------------------------------------------------------------
	$need_array = $obj->get_duty_shift_need_cnt_calendar_array(
			$group_id, $duty_yyyy, $duty_mm, $day_cnt,
			$data_job, $atdptn_array, $week_array, $calendar_array); 
	///-----------------------------------------------------------------------------
	//削除不可行数を算出
	///-----------------------------------------------------------------------------
	$del_not_cnt=1;
}
?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
	///-----------------------------------------------------------------------------
	// 追加
	///-----------------------------------------------------------------------------
	function addData(idx)
	{
		//行追加
		document.mainform.del_idx.value = "";
		document.mainform.add_idx.value = idx;
		document.mainform.action="duty_shift_need_cnt_calendar.php";
		document.mainform.submit();
	}
	///-----------------------------------------------------------------------------
	// 削除
	///-----------------------------------------------------------------------------
	function delData(idx)
	{
		//行削除
		document.mainform.del_idx.value = idx;
		document.mainform.add_idx.value = "";
		document.mainform.action="duty_shift_need_cnt_calendar.php";
		document.mainform.submit();
	}
	///-----------------------------------------------------------------------------
	// 登録
	///-----------------------------------------------------------------------------
	function insertData()
	{
		var data_cnt = document.mainform.data_cnt.value;
		var atdptn_cnt = document.mainform.atdptn_cnt.value;
		var atdptn_ptn_id = new Array(data_cnt);
		var job_id = new Array(data_cnt);
		var wk = "";

		///-----------------------------------------------------------------------------
		// 未入力チェック
		///-----------------------------------------------------------------------------
		for(i=0;i<data_cnt;i++){
			var job_id_1 = document.mainform.elements['job_id[]'][i].value;
			if (job_id_1 == "") {
				alert("職種が未選択の勤務シフトが存在します。");
				return;
			}
		}
		///-----------------------------------------------------------------------------
		// 重複チェック
		///-----------------------------------------------------------------------------
		var dup_table = new Array();
		var chk_flg = "";
		for(i=0;i<data_cnt;i++){
			var atdptn = document.mainform.elements['atdptn_ptn_id[]'][i].value;
			var team   = document.mainform.elements['team[]'][i].value;
			var sex    = document.mainform.elements['sex[]'][i].value;
			
			var idx1 = atdptn + "_" + team + "_" + sex + "_" + document.mainform.elements['job_id[]'][i].value;
			if ( dup_table[idx1] == 1 ) {
				chk_flg = "1";
				break;
			} else {
				dup_table[idx1] = 1;
			}
			
			if ( document.mainform.elements['job_id2[]'][i].value != "" ) {
				var idx2 = atdptn + "_" + team + "_" + sex + "_" + document.mainform.elements['job_id2[]'][i].value;
				if ( dup_table[idx2] == 1 ) {
					chk_flg = "1";
					break;
				} else {
					dup_table[idx2] = 1;
				}
			}
			
			if ( document.mainform.elements['job_id3[]'][i].value != "" ) {
				var idx3 = atdptn + "_" + team + "_" + sex + "_" + document.mainform.elements['job_id3[]'][i].value;
				if ( dup_table[idx3] == 1 ) {
					chk_flg = "1";
					break;
				} else {
					dup_table[idx3] = 1;
				}
			}
		}
		if (chk_flg == "1") {
			alert("職種が重複している勤務シフトが存在します。");
			return;
		}
		///-----------------------------------------------------------------------------
		// 数値チェック
		///-----------------------------------------------------------------------------
		var day_cnt = document.mainform.day_cnt.value;
		var num = "";
		for(i=0;i<data_cnt;i++){
			for(k=1;k<=day_cnt;k++){
				num = document.mainform.elements['need_cnt['+k+'][]'][i].value;
				if (num.match(/[^0-9]/g)) {
					alert("「人数」に数値以外が設定されています。");
					return;
				}
			}
		}
		///-----------------------------------------------------------------------------
		// 更新
		///-----------------------------------------------------------------------------
		document.mainform.action="duty_shift_need_cnt_calendar_update_exe.php";
		document.mainform.submit();
	}

	///-----------------------------------------------------------------------------
	//メッセージ画面（表示／非表示）
	///-----------------------------------------------------------------------------
	function showMemo(day, memo) {
		try {
			if (memo == "") {
				return;
			}

		    //--このHTMLテンプレートを書き換えればヘルプのデザインが変わります。
			var msg1  = '<table bgcolor="#dddddd" border="1">\n'
				msg1 += '<tr><td>'+memo+'</td></tr>\n'
				msg1 += '</table>\n'
			showMsg(msg1);
		} 
		catch(err){
		}
	}
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

.non_list {border-collapse:collapse;}
.non_list td {border:#FFFFFF solid 1px;}

img.close {
	background-image:url("images/plus.gif");
	vertical-align:middle;
}
img.open {
	background-image:url("images/minus.gif");
	vertical-align:middle;

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<?
// 全画面時は表示しない
if ($fullscreen_flg != "1") {
?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
	<?
	// 画面遷移
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
	show_user_title($session, $section_admin_auth);			//duty_shift_common.ini
	echo("</table>\n");
	
	// タブ
	$arr_option = "&group_id=" . $group_id . "&duty_yyyy=" . $duty_yyyy ."&duty_mm=" . $duty_mm;
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
	show_user_tab_menuitem($session, $fname, $arr_option);	//duty_shift_user_tab_common.ini
	echo("</table>\n");
	
	// 下線
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
?>
</td></tr></table>
	<?
} else {
	// 全画面時
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279a5">
<td width="100%" height="22" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>必要人数設定 <?=$group_name?></b></font></td>
<td width="32" bgcolor="#dddddd" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
	<?
}
?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 入力エリア -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="mainform" method="post">
		<!-- ------------------------------------------------------------------------ -->
		<!-- 年月 -->
		<!-- 更新ボタン -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="1000" border="0" cellspacing="0" cellpadding="2">
		<tr height="22">
		<td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
echo("&nbsp;&nbsp;" . $duty_yyyy . "年" . $duty_mm . "月");
?>
		</font></td>
		<td align="right"><input type="button" value="更新" onclick="insertData()"></td>
		</tr>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 見出し -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="1000" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="non_list">
<?
//-------------------------------------------------------------------------------
//週段
//-------------------------------------------------------------------------------
echo("<tr> \n");
echo("<td width=\"40\"></td>");
echo("<td width=\"40\"></td>");
echo("<td width=\"120\"></td>");
echo("<td width=\"60\"></td>");
$wk_cnt = 1;
for ($k=1; $k<=$day_cnt; $k++) {
	//週の最後日を算出
	for ($m=$k; $m<=$day_cnt; $m++) {
		if (($week_array[$m]["name"] == "日") || ($m == $day_cnt)){
			break;
		}
	}
	//表示
	$wk_id_img = "img" . $wk_cnt;
	$wk_td_id = "week" . $k;
	if (($week_array[$k]["name"] == "月") || ($k == 1)){
		echo("<td id=\"$wk_td_id\" width=\"30\" align=\"left\" style=\"cursor:pointer;\">");
		echo("<img src=\"img/spacer.gif\" width=\"11\" height=\"11\" id=\"$wk_id_img\" class=\"close\" onclick=\"changeDisplay(this, $k, $m);\">");
		$wk_cnt++;
	} else {
		echo("<td id=\"$wk_td_id\" width=\"30\" align=\"left\">");
	}
	echo("</td> \n");
}
		?>
		<td width="50"></td>
		<td width="50"></td>
		</tr>
		</table>
		<table width="1000" id="header" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed;" class="list">
<?
//-------------------------------------------------------------------------------
//日段
//-------------------------------------------------------------------------------
echo("<tr> \n");
echo("<td width=\"40\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">出勤<br>ﾊﾟﾀｰﾝ</font></td>");
echo("<td width=\"40\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">チーム</font></td>");
echo("<td width=\"120\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">職種</font></td>");
echo("<td width=\"60\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">性別</font></td>");

for ($k=1; $k<=$day_cnt; $k++) {
	$wk_td_id = "day" . $k;
	echo("<td id=\"$wk_td_id\" width=\"25\" align=\"center\" colspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$k</font></td>");
}
echo("<td width=\"50\" rowspan=\"2\"></td>");
echo("<td width=\"50\" rowspan=\"2\"></td>");
echo("</tr> \n");
//-------------------------------------------------------------------------------
//曜日段
//-------------------------------------------------------------------------------
echo("<tr> \n");
for ($k=1; $k<=$day_cnt; $k++) {
	$wk_name = $week_array[$k]["name"];
	//土、日、祝日の文字色変更
	$wk_color = "";
	if ($week_array[$k]["name"] == "土"){
		$wk_color = "#0000ff";
	} else if ($week_array[$k]["name"] == "日") {
		$wk_color = "#ff0000";
	} else {
		if (($calendar_array[$k-1]["type"] == "4") ||
				($calendar_array[$k-1]["type"] == "5") ||
				($calendar_array[$k-1]["type"] == "6")) {
			$wk_color = "#ff0000";
		}
	}
	//表示
	$wk_memo = trim($calendar_array[$k-1]["memo"]);
	$wk_td_id = "day_of_week" . $k;
	echo("<td id=\"$wk_td_id\" width=\"40\" align=\"center\" colspan=\"1\" onmouseover=\"showMemo($k,'$wk_memo');\" onmouseout=\"hideMsg();\" style=\"cursor:pointer;\">");
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=$wk_color>$wk_name</font></td> \n");
}
echo("</tr> \n");
		?>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- データ -->
		<!-- ------------------------------------------------------------------------ -->
		<div id="region" style="cursor:pointer; overflow-x:hidden; overflow-y:scroll; overflow:auto; border:#5279a5 solid 0px;">
		<table id="data" width="1000" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed;" class="list">
<?
$cnt=0;
$data_cnt=0;
for ($i=0; $i<$atdptn_cnt; $i++) {
	///-----------------------------------------------------------------------------
	// パターン毎の出力値取得
	///-----------------------------------------------------------------------------
	$wk_array = array();
	$m=0;
	for($k=0;$k<count($need_array);$k++){
		if($need_array[$k]["atdptn_ptn_id"] == $atdptn_array[$i]["id"]){
			$wk_array[$m]["job_id"] = $need_array[$k]["job_id"];
			$wk_array[$m]["job_id2"] = $need_array[$k]["job_id2"];
			$wk_array[$m]["job_id3"] = $need_array[$k]["job_id3"];
			$wk_array[$m]["team"] = $need_array[$k]["team"];
			$wk_array[$m]["sex"] = $need_array[$k]["sex"];
			for($p=1;$p<=$day_cnt;$p++){
				$wk_array[$m]["need_cnt"][$p] = $need_array[$k]["need_cnt"][$p];
			}
			$m++;
		}
	}
	///-----------------------------------------------------------------------------
	// 出力値設定
	///-----------------------------------------------------------------------------
	for ($k=0; $k<count($wk_array); $k++) {
		echo("<tr> \n");
		///-----------------------------------------------------------------------------
		// 出勤パターン
		///-----------------------------------------------------------------------------
		if ($k == 0) {
			$wk = count($wk_array);
			echo("<td width=\"40\" align=\"center\" rowspan=\"$wk\" style=\"word-break:break-all;\><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");
			echo($atdptn_array[$i]["name"]);
			if ($atdptn_array[$i]["id"] == "10") {
				echo("(公休)");
			}
			echo("</font></td> \n");
		}
		
		///-----------------------------------------------------------------------------
		// チーム
		///-----------------------------------------------------------------------------
		echo("<td width=\"40\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("<select name=\"team[]\">\n");
		echo("<option value=\"0\"></option>");
		foreach($team_array as $team){
			$selected = "";
			if($team["team_id"] == $wk_array[$k]["team"]){
				$selected = 'selected';
			}
			echo("<option value=\"{$team["team_id"]}\" $selected>{$team["team_name"]}</option>");
		}
		echo("</select>\n");
		echo("</font></td>\n");
		
		///-----------------------------------------------------------------------------
		// 職種
		///-----------------------------------------------------------------------------
		echo("<td width=\"120\" align=\"left\">\n");
		echo("<select name=\"job_id[]\">\n");
		echo("<option value=\"\">\n");
		for ($m=0; $m<count($data_job); $m++) {
			$wk_id = $data_job[$m]["id"];
			$wk_name = $data_job[$m]["name"];
			echo("<option value=\"$wk_id\"");
			if ($wk_array[$k]["job_id"] == $wk_id) {
				echo(" selected");
			}
			echo(">$wk_name\n");
		}
		echo("</select>\n");
		echo("<select name=\"job_id2[]\">\n");
		echo("<option value=\"\">\n");
		for ($m=0; $m<count($data_job); $m++) {
			$wk_id = $data_job[$m]["id"];
			$wk_name = $data_job[$m]["name"];
			echo("<option value=\"$wk_id\"");
			if ($wk_array[$k]["job_id2"] == $wk_id) {
				echo(" selected");
			}
			echo(">$wk_name\n");
		}
		echo("</select>\n");
		echo("<select name=\"job_id3[]\">\n");
		echo("<option value=\"\">\n");
		for ($m=0; $m<count($data_job); $m++) {
			$wk_id = $data_job[$m]["id"];
			$wk_name = $data_job[$m]["name"];
			echo("<option value=\"$wk_id\"");
			if ($wk_array[$k]["job_id3"] == $wk_id) {
				echo(" selected");
			}
			echo(">$wk_name\n");
		}
		echo("</select>\n");
		echo("</td>\n");
		
		///-----------------------------------------------------------------------------
		// 性別
		///-----------------------------------------------------------------------------
?>
<td width="60" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="sex[]">
<option value=""></option>
<option value="1" <? if ($wk_array[$k]["sex"] == "1") {echo "selected";} ?>>男性</option>
<option value="2" <? if ($wk_array[$k]["sex"] == "2") {echo "selected";} ?>>女性</option>
</select>
</font></td>
		<?
		
		///-----------------------------------------------------------------------------
		// 必要人数（１〜３１）
		///-----------------------------------------------------------------------------
		for ($m=1; $m<=$day_cnt; $m++) {
			$wk_name = h($wk_array[$k]["need_cnt"][$m]);
			$wk_cnt = $cnt + 1;
			$wk_td_id = "data" . $wk_cnt . "_" . $m;
			echo("<td id=\"$wk_td_id\" width=\"25\" align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
			echo("<input name=\"need_cnt[$m][]\" type=\"text\" value=\"$wk_name\" size=\"2\" maxlength=\"3\" style=\"ime-mode:disabled;\">\n");
			echo("</font></td>\n");
		}
		///-----------------------------------------------------------------------------
		// 追加
		///-----------------------------------------------------------------------------
		echo("<td width=\"50\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
		if ($k == count($wk_array) - 1) {
			echo("<input type=\"button\" value=\"追加\" onclick=\"addData($cnt);\">\n");
		}
		echo("</font></td> \n");
		
		///-----------------------------------------------------------------------------
		// 削除
		///-----------------------------------------------------------------------------
		echo("<td width=\"50\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
		if ($k >= $del_not_cnt) {
			echo("<input type=\"button\" value=\"削除\" onclick=\"delData($cnt);\">\n");
		}
		echo("</font></td> \n");
		echo("</tr> \n");
		
		$cnt++;
		$data_cnt++;
	}
}
		?>
		</table>
		</div>
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
		<input type="hidden" name="duty_yyyy" value="<? echo($duty_yyyy); ?>">
		<input type="hidden" name="duty_mm" value="<? echo($duty_mm); ?>">

		<input type="hidden" name="data_cnt" value="<? echo($data_cnt); ?>">
		<input type="hidden" name="day_cnt" value="<? echo($day_cnt); ?>">

		<input type="hidden" name="atdptn_cnt" value="<? echo($atdptn_cnt); ?>">
		<input type="hidden" name="del_not_cnt" value="<? echo($del_not_cnt); ?>">
		<input type="hidden" name="add_idx" value="<? echo($add_idx); ?>">
		<input type="hidden" name="del_idx" value="<? echo($del_idx); ?>">

		<input type="hidden" name="fullscreen_flg" value="<? echo($fullscreen_flg); ?>">
<?
//人数
for($i=0;$i<$data_cnt;$i++){
	$wk = $need_array[$i]["atdptn_ptn_id"];
	echo("<input type=\"hidden\" name=\"atdptn_ptn_id[]\" value=$wk >\n");
}
/// 年月日（カレンダーより）
for ($i=0; $i<count($calendar_array); $i++) {
	$wk = $calendar_array[$i]["date"];
	$k = $i + 1;
	echo("<input type=\"hidden\" name=\"duty_date[$k]\" value=$wk >\n");
}
		?>
	</form>

</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>

<!-- ************************************************************************ -->
<!-- JavaScript（１週間単位での表示／非表示） -->
<!-- ************************************************************************ -->
<script language="JavaScript">
	var data = document.getElementById("data");
	var header = document.getElementById("header");
	///-----------------------------------------------------------------------------
	// スクロールの位置調整
	///-----------------------------------------------------------------------------
	document.getElementById("region").style.width = data.offsetWidth + 20;
	///-----------------------------------------------------------------------------
	// スクロールの縦サイズ調整
	///-----------------------------------------------------------------------------
	var data_row = data.rows.length;
	if(data_row > 15)
	{
		var max = 15;
		var scroll_size = 0;
		for(i=0; i<max; i++)
		{
			scroll_size += data.rows[i].offsetHeight;
		}
		document.getElementById('region').style.height = scroll_size;
	}
	///-----------------------------------------------------------------------------
	//表の表示／非表示
	///-----------------------------------------------------------------------------
	function changeDisplay(img, start, end)
	{
		var week_id = img.id.replace('img', '');

		var data = document.getElementById("data");
		var data_cnt = data.rows.length;

		img.className = (img.className == 'close') ? 'open' : 'close';
		var disp = (img.className == 'close') ? '' : 'none' ;

		// 週
		for(i=start+1; i<=end; i++)
	    {
			document.getElementById('week' + i).style.display = disp;
	    }

		// 日付・曜日
		for(i=start+1; i<=end; i++)
	    {
			document.getElementById('day' + i).style.display = disp;
			document.getElementById('day_of_week' + i).style.display = disp;
	    }

		// データ
		for(i=1; i<=data_cnt; i++)
		{
			for(j=start+1; j<=end; j++)
			{
				document.getElementById('data' + i + '_' + j).style.display = disp;
			}
		}

	    // スクロールの位置調整
		document.getElementById("region").style.width = data.offsetWidth + 20;
	}
</script>

<!-- ************************************************************************ -->
<!-- JavaScript（メッセージ子画面表示処理） -->
<!-- ************************************************************************ -->
<script language="JavaScript">
	//(注1)

	//--HTML出力
	function outputLAYER(layName,html){
	if(document.getElementById){        //e5,e6,n6,n7,m1,o7,s1用
	  document.getElementById(layName).innerHTML=html
	} else if(document.all){            //e4用
	  document.all(layName).innerHTML=html
	} else if(document.layers) {        //n4用
	   with(document.layers[layName].document){
	     open()
	     write(html)
	     close()
	  }
	}
	}

	//--マウス追跡
	/*==================================================================
	followingLAYER()

	Syntax :
	 追跡レイヤー名 = new followingLAYER('レイヤー名'
	                            ,右方向位置,下方向位置,動作間隔,html)

	 レイヤー名 マウスを追跡させるレイヤー名
	 右方向位置 マウスから右方向へ何ピクセル離すか
	 下方向位置 マウスから下方向へ何ピクセル離すか
	 動作間隔   マウスを追跡する間隔(1/1000秒単位 何秒後に動くか?)
	 html       マウスを追跡するHTML

	------------------------------------------------------------------*/
	/*--/////////////ここから下は触らなくても動きます/////////////--*/

	//--追跡オブジェクト
	//e4,e5,e6,n4,n6,n7,m1,o6,o7,s1用
	function followingLAYER(layName,ofx,ofy,delay,html){
	this.layName = layName   //マウスを追跡させるレイヤー名
	this.ofx     = ofx       //マウスから右方向へ何ピクセル離すか
	this.ofy     = ofy       //マウスから下方向へ何ピクセル離すか
	this.delay   = delay     //マウスを追跡するタイミング
	if(document.layers)
	  this.div='<layer name="'+layName+'" left="-100" top="-100">\n'
	          + html + '</layer>\n'
	else
	  this.div='<div id="'+layName+'"\n'
	          +'style="position:absolute;left:-100px;top:-100px">\n'
	          + html + '</div>\n'
	document.write(this.div)
	}

	//--メソッドmoveLAYER()を追加する
	followingLAYER.prototype.moveLAYER = moveLAYER //メソッドを追加する
	function moveLAYER(layName,x,y){
	if(document.getElementById){        //e5,e6,n6,n7,m1,o6,o7,s1用
	    document.getElementById(layName).style.left = x
	    document.getElementById(layName).style.top  = y
	} else if(document.all){            //e4用
	    document.all(layName).style.pixelLeft = x
	    document.all(layName).style.pixelTop  = y
	} else if(document.layers)          //n4用
	    document.layers[layName].moveTo(x,y)
	}

	//--Eventをセットする(マウスを動かすとdofollow()を実行します)
	document.onmousemove = dofollow
	//--n4マウスムーブイベント走査開始
	if(document.layers)document.captureEvents(Event.MOUSEMOVE)
	//--oの全画面のEventを拾えないことへの対策
	if(window.opera){
	op_dmydoc ='<div id="dmy" style="position:absolute;z-index:0'
	          +'                     left:100%;top:100%"></div> '
	document.write(op_dmydoc)
	}

	//--イベント発生時にマウス追跡実行
	function dofollow(e){
	for(var i=0 ; i < a.length ; i++ )
	  setTimeout("a["+i+"].moveLAYER(a["+i+"].layName,"
	         +(getMouseX(e)+a[i].ofx)+","+(getMouseY(e)+a[i].ofy)+")"
	    ,a[i].delay)
	}

	//--マウスX座標get
	function getMouseX(e){
	if(navigator.userAgent.search(
	         "Opera(\ |\/)6") != -1 )   //o6用
	    return e.clientX
	else if(document.all)               //e4,e5,e6用
	    return document.body.scrollLeft+event.clientX
	else if(document.layers ||
	        document.getElementById)    //n4,n6,n7,m1,o7,s1用
	    return e.pageX
	}

	//--マウスY座標get
	function getMouseY(e){
	if(navigator.userAgent.search(
	         "Opera(\ |\/)6") != -1 )   //o6用
	    return e.clientY
	else if(document.all)               //e4,e5,e6用
	    return document.body.scrollTop+event.clientY
	else if(document.layers ||
	        document.getElementById)    //n4,n6,n7,m1,o7,s1用
	    return e.pageY
	}

	/*////////////////////////////// マウスを追跡するレイヤーここまで */

	//テンプレートを作成しoutputLAYERへ渡す
	function showMsg(msg1){
		outputLAYER('test0',msg1)
	}

	//レイヤーの中身を消す
	function hideMsg(){
		var msg1 =''
		outputLAYER('test0',msg1)
	}

	//レイヤーの数だけa[i]=…部分を増減して使ってください
	var a = new Array()
	a[0]  = new followingLAYER('test0',20,10,100,'')
	a[1]  = new followingLAYER('test1',20,10,200,'')
	a[2]  = new followingLAYER('test2',20,10,300,'')
</script>