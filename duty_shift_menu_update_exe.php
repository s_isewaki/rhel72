<!-- ************************************************************************ -->
<!-- 勤務シフト作成｜勤務シフト作成「登録」 -->
<!-- ************************************************************************ -->

<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");

///-----------------------------------------------------------------------------
//日編集
///-----------------------------------------------------------------------------
$start_date = $duty_start_date;
$end_date = $duty_end_date;
$calendar_array = $obj->get_calendar_array($start_date, $end_date);

for ($i=0; $i<count($calendar_array); $i++) {
	$duty_date[$i + 1] = $calendar_array[$i]["date"];
}

//年月から月またがり確認
$wk_duty_yyyymm = $duty_yyyy.sprintf("%02d",$duty_mm);
$month_flg = "1"; //当月から当月
if (substr($start_date, 0, 6) != $wk_duty_yyyymm) {
	$month_flg = "0"; //前月から当月
} elseif (substr($end_date, 0, 6) != $wk_duty_yyyymm) {
	$month_flg = "2"; //当月から翌月
}

/******************************************************************************/
// 勤務シフト情報（スタッフ情報）
/******************************************************************************/
// group_id 全画面対応
if ($group_id == "") { $group_id = $cause_group_id; }

//月またがり
$next_yyyy = $duty_yyyy;
$next_mm   = $duty_mm;
if ($month_flg == "0"){
	$next_mm--;
	if( $next_mm < 1) {
		$next_yyyy--;
		$next_mm = 12;
	}
}
elseif ($month_flg == "2"){
	$next_mm++;
	if( $next_mm > 12) {
		$next_yyyy++;
		$next_mm = 1;
	}
}

//役職IDを取得
$st_id_array = array();
//所属するグループID
$belong_group_array = array();

if ($data_cnt > 0) {
	$cond = "";
	for ($i=0;$i<$data_cnt;$i++) {
		$wk_id = $staff_id[$i];
		if ($cond == "") {
			$cond .= "where (a.emp_id = '$wk_id' ";
		} else {
			$cond .= "or a.emp_id = '$wk_id' ";
		}
	}
	$cond .= ")";
	$sql = "select a.emp_id, b.duty_yyyy, b.duty_mm, b.st_id, a.emp_st, c.group_id as belong_group_id from empmst a ";
	$sql .= " left join duty_shift_plan_staff b on ";
	$sql .= " b.emp_id = a.emp_id and  ";
	$sql .= " b.group_id = '$group_id' ";
	//月またがりがあっても当月を基準とする 20110301
	$sql .= " and (b.duty_yyyy = $duty_yyyy and b.duty_mm = $duty_mm) ";
	//	$sql .= " and ((b.duty_yyyy = $duty_yyyy and b.duty_mm = $duty_mm) ";
	//	$sql .= " or (b.duty_yyyy = $next_yyyy and b.duty_mm = $next_mm)) ";
	$sql .= " left join duty_shift_staff c on c.emp_id = a.emp_id ";
	
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);
	for($i=0;$i<$num;$i++){
		$wk_emp_id = pg_result($sel,$i,"emp_id");
		$wk_yyyy = pg_result($sel,$i,"duty_yyyy");
		$wk_mm = pg_result($sel,$i,"duty_mm");
		$wk_st_id = pg_result($sel,$i,"st_id");
		$wk_emp_st = pg_result($sel,$i,"emp_st");
		$wk_group_id = pg_result($sel,$i,"belong_group_id");
		
		if ($wk_yyyy != "" && $wk_mm != "" && $wk_st_id != "") {
			$st_id_array["$wk_emp_id"]["$wk_yyyy$wk_mm"] = $wk_st_id;
		} else {
			$st_id_array["$wk_emp_id"]["emp_st"] = $wk_emp_st;
		}
		$belong_group_array["$wk_emp_id"] = $wk_group_id;
	}
}
/* 職員設定のグループIDを使用 20110315
//新規確認用情報取得
//初回に職員設定で登録されたグループを設定するため
$arr_old_group_id = array();
$sql = "select emp_id, main_group_id from duty_shift_plan_staff ";
$cond = "where ";
$cond .= " duty_yyyy = $duty_yyyy ";
$cond .= "and duty_mm = $duty_mm ";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$num = pg_numrows($sel);
for($i=0;$i<$num;$i++){
	$wk_emp_id = pg_result($sel,$i,"emp_id");
	$wk_main_group_id = pg_result($sel,$i,"main_group_id");
	if ($arr_old_group_id["$wk_emp_id"] == "") { //未設定の時（設定済みなら変えない）
		$arr_old_group_id["$wk_emp_id"] = $wk_main_group_id;
	}
}
*/
//チェックフラグ取得
$sql = "select emp_id, check_flag from duty_shift_plan_staff ";
$cond = "where group_id = '$group_id' and duty_yyyy = $duty_yyyy and duty_mm = $duty_mm ";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$arr_check_flag = array();
while ($row = pg_fetch_array($sel)) {
	$wk_emp_id = $row["emp_id"];
	$arr_check_flag["$wk_emp_id"] = $row["check_flag"];
}
///-----------------------------------------------------------------------------
//削除
///-----------------------------------------------------------------------------
$sql = "delete from duty_shift_plan_staff";
$cond = "where group_id = '$group_id' ";
$cond .= "and duty_yyyy = $duty_yyyy ";
$cond .= "and duty_mm = $duty_mm ";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
//月またがりがあっても当月を基準とする 20110301
/*
//月またがり有り
if ($month_flg == "0" || $month_flg == "2") {
	$sql = "delete from duty_shift_plan_staff";
	$cond = "where group_id = '$group_id' ";
	$cond .= "and duty_yyyy = $next_yyyy ";
	$cond .= "and duty_mm = $next_mm ";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
*/
///-----------------------------------------------------------------------------
//登録
///-----------------------------------------------------------------------------
for($i=0;$i<$data_cnt;$i++) {
	//役職ID追加
	$wk_st_id = $st_id_array[$staff_id[$i]]["$duty_yyyy$duty_mm"];
	if ($wk_st_id == "") {
		$wk_st_id = $st_id_array[$staff_id[$i]]["emp_st"];
	}
	//既存の主グループがある場合はそれを設定
	//if ($arr_old_group_id[$staff_id[$i]] != "") { //職員設定のグループIDを使用 20110315
	//	$wk_main_group_id = $arr_old_group_id[$staff_id[$i]];
	//} else {
		//新規の場合は職員設定のグループID
		$wk_main_group_id = $belong_group_array[$staff_id[$i]];
	//}
	//チェックフラグ設定
	$wk_check_flag = $arr_check_flag["$wk_emp_id"];
	if ($wk_check_flag == "") {
		$wk_check_flag = "f";
	}
	
	$sql = "insert into duty_shift_plan_staff (group_id, emp_id, duty_yyyy, duty_mm, no, st_id, main_group_id, check_flag) values (";
	$content = array($group_id, $staff_id[$i], $duty_yyyy, $duty_mm, $i + 1,$wk_st_id, $wk_main_group_id, $wk_check_flag);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	//月またがりがあっても当月を基準とする 20110301
/*
	//月またがり有り
	if ($month_flg == "0" || $month_flg == "2") {
		
		//役職ID追加
		$wk_st_id = $st_id_array[$staff_id[$i]]["$next_yyyy$next_mm"];
		if ($wk_st_id == "") {
			$wk_st_id = $st_id_array[$staff_id[$i]]["emp_st"];
		}
		$sql = "insert into duty_shift_plan_staff (group_id, emp_id, duty_yyyy, duty_mm, no, st_id, main_group_id) values (";
		$content = array($group_id, $staff_id[$i], $next_yyyy, $next_mm, $i + 1,$wk_st_id, $wk_main_group_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
*/
}

//チームを更新する
$obj->update_duty_shift_plan_team($data_cnt, $group_id, $duty_yyyy, $duty_mm, $staff_id, $team);
//月またがりがあっても当月を基準とする 20110301
//if ($month_flg != 1){
//	$obj->update_duty_shift_plan_team($data_cnt, $group_id, $next_yyyy, $next_mm, $staff_id, $team);
//}

/******************************************************************************/
//勤務シフト情報テーブル（下書き）
/******************************************************************************/
$inp_flg = "";
if ($draft_flg == "1") {
	for($i=0;$i<$data_cnt;$i++) {
		
		//応援情報
		$assist_str = "assist_group_$i";
		$arr_assist_group = split(",", $$assist_str);
		///-----------------------------------------------------------------------------
		//項目の配列編集
		///-----------------------------------------------------------------------------
		$emp_id = $staff_id[$i];
		///-----------------------------------------------------------------------------
		//指定職員ＩＤの下書きデータを削除
		///-----------------------------------------------------------------------------
		$sql = "delete from duty_shift_plan_draft";
		$cond = "where emp_id = '$emp_id' ";
		$cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		//予定データも削除 20090223
		$sql = "delete from atdbk ";
		$cond = "where emp_id = '$emp_id' ";
		$cond .= "and date >= '$start_date' and date <= '$end_date' ";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		//応援追加情報削除 20150515
		$sql = "delete from duty_shift_plan_assist";
		$cond = "where emp_id = '$emp_id' ";
		$cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		///-----------------------------------------------------------------------------
		//登録
		///-----------------------------------------------------------------------------
		for($k=1;$k<=$day_cnt;$k++) {
			///-----------------------------------------------------------------------------
			//項目の配列編集
			///-----------------------------------------------------------------------------
			$wk_date = $duty_date[$k];
			$ptn_id = "pattern_id_$i" . "_" . $k;
			
			$atdptn_ptn_id = "";
			$reason = "";
			$wk = "atdptn_ptn_id_$i" . "_" . $k;
			if ($$wk != "") {
				if ((int)substr($$wk,0,2) > 0) {
					$atdptn_ptn_id = (int)substr($$wk,0,2);
				}
				if ((int)substr($$wk,2,2) > 0) {
					$reason = (int)substr($$wk,2,2);
				}
			}
			$reason_2 = "reason_2_$i" . "_" . $k;
			if ($$reason_2 != "") {
				$reason = $$reason_2;
			}
			///-----------------------------------------------------------------------------
			//データが存在する場合のみ登録
			///-----------------------------------------------------------------------------
			if (($group_id != "") &&
					($staff_id[$i] != "") &&
					($duty_date[$k] != "") &&
					($$ptn_id != "") &&
					($atdptn_ptn_id != "")) {
				///-----------------------------------------------------------------------------
				//ＳＱＬ
				///-----------------------------------------------------------------------------
				$sql = "insert into duty_shift_plan_draft (group_id, emp_id, duty_date, pattern_id, atdptn_ptn_id, reason";
				$sql .= ") values (";
				//グループID（応援追加対応）
				$wk_group_id = $arr_assist_group[$k-1];
				$content = array($wk_group_id, $emp_id, $wk_date, $$ptn_id, $atdptn_ptn_id, $reason);
				$ins = insert_into_table($con, $sql, $content, $fname);
				if ($ins == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				
				$inp_flg = "1";
			}
		}
	}
	///-----------------------------------------------------------------------------
	//入力がない場合、空データを１件だけ登録
	///-----------------------------------------------------------------------------
	if ($inp_flg == "") {
		$i = 0;
		$k = 1;
		///-----------------------------------------------------------------------------
		//項目の配列編集
		///-----------------------------------------------------------------------------
		$emp_id = $staff_id[$i];
		$ptn_id = "pattern_id_$i" . "_" . $k;
		$atdptn_ptn_id = null;
		$reason = null;
		$wk_date = $duty_date[$k];
		///-----------------------------------------------------------------------------
		//ＳＱＬ
		///-----------------------------------------------------------------------------
		$sql = "insert into duty_shift_plan_draft (group_id, emp_id, duty_date, pattern_id, atdptn_ptn_id, reason";
		$sql .= ") values (";
		$content = array($group_id, $emp_id, $wk_date, $$ptn_id, $atdptn_ptn_id, $reason);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
} else {
	//勤務希望非表示にする場合を除く
	if ($hope_save_flg != "1") {
		///-----------------------------------------------------------------------------
		//対象全データ削除
		///-----------------------------------------------------------------------------
		//検索条件（職員）
		$cond_add = "";
		for($i=0;$i<$data_cnt;$i++) {
			$wk_id = $staff_id[$i];
			if ($cond_add == "") {
				$cond_add .= "and (emp_id = '$wk_id' ";
			} else {
				$cond_add .= "or emp_id = '$wk_id' ";
			}
		}
		if ($cond_add != "") {
			$cond_add .= ") ";
		}
		//削除
		$sql = "delete from duty_shift_plan_draft";
		$cond = "where 1 = 1 ";
		$cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
		$cond .= $cond_add;
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

/******************************************************************************/
// 勤務シフト情報（コメント）を登録
/******************************************************************************/
for($i=0;$i<$data_cnt;$i++) {
	
	//画面上で更新があった場合のみ以下の処理を行う。ただし、職員の削除がある場合は無条件に行う。
	if ($cmt_upd_flg[$i] != "1" && $delete_id_cnt == 0) {
		continue;
	}
	
	///-----------------------------------------------------------------------------
	//項目の配列編集
	///-----------------------------------------------------------------------------
	$emp_id = $staff_id[$i];
	///-----------------------------------------------------------------------------
	// 勤務シフト情報（コメント）を削除
	///-----------------------------------------------------------------------------
	$sql = "delete from duty_shift_plan_comment";
	$cond = "where emp_id = '$emp_id' ";
	$cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	///-----------------------------------------------------------------------------
	// 登録
	///-----------------------------------------------------------------------------
	for($k=1;$k<=$day_cnt;$k++) {
		///-----------------------------------------------------------------------------
		//項目の配列編集
		///-----------------------------------------------------------------------------
		$comment = "comment_$i" . "_" . $k;
		///-----------------------------------------------------------------------------
		//データが存在する場合のみ登録
		///-----------------------------------------------------------------------------
		if (($staff_id[$i] != "") &&
				($duty_date[$k] != "") &&
				($$comment != "")) {
			
			$wk_date = $duty_date[$k];
			///-----------------------------------------------------------------------------
			//ＳＱＬ
			///-----------------------------------------------------------------------------
			$sql = "insert into duty_shift_plan_comment (emp_id, duty_date, comment";
			$sql .= ") values (";
			$content = array($emp_id, $wk_date, pg_escape_string($$comment));
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
}


/******************************************************************************/
//atdbkテーブル
/******************************************************************************/
///-----------------------------------------------------------------------------
// 勤務予定ＤＢ(atdbk）へのデータ反映
///-----------------------------------------------------------------------------
// 下書き以外
if ($draft_flg == "" && $hope_save_flg == "") {
    //シフトグループの件数確認 20120924
    $arr_emp_group_cnt = array();
    if ($data_cnt > 0) {
        $cond_emp = "";
        for ($i=0;$i<$data_cnt;$i++) {
            $wk_id = $staff_id[$i];
            if ($cond_emp == "") {
                $cond_emp .= " (emp_id = '$wk_id' ";
            } else {
                $cond_emp .= "or emp_id = '$wk_id' ";
            }
        }
        $cond_emp .= ")";
        $sql = "select emp_id, count(*) as cnt from duty_shift_plan_staff ";
        $cond = "where duty_yyyy = $duty_yyyy and duty_mm = $duty_mm ";
        $cond .= " and group_id != '$group_id' and $cond_emp ";
        $cond .= " group by emp_id order by emp_id ";
        $sel = select_from_table($con, $sql, $cond, $fname);
	    if ($sel == 0) {
		    pg_close($con);
		    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		    exit;
	    }
	    $num = pg_numrows($sel);
        for($i=0;$i<$num;$i++){
            $wk_emp_id = pg_result($sel,$i,"emp_id");
            $arr_emp_group_cnt[$wk_emp_id] = pg_result($sel,$i,"cnt");
        }
    }    
	for($i=0;$i<$data_cnt;$i++) {
		
		$emp_id = $staff_id[$i];
        $wk_emp_group_cnt = $arr_emp_group_cnt[$emp_id];
        
        //画面上で更新があった場合のみ以下の処理を行う。ただし、職員の削除がある場合は無条件に行う。
        if ($upd_flg[$i] != "1" && $delete_id_cnt == 0
                && $wk_emp_group_cnt == 0 //別のシフトグループ件数がある場合を除く
            ) {
			continue;
		}
		
		//応援情報
		$assist_str = "assist_group_$i";
		$arr_assist_group = split(",", $$assist_str);
		//応援追加情報削除
		$sql = "delete from duty_shift_plan_assist";
		$cond = "where emp_id = '$emp_id' ";
		$cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
        for($k=1;$k<=$day_cnt;$k++) {
            ///-----------------------------------------------------------------------------
            //項目の配列編集
            ///-----------------------------------------------------------------------------
            $wk_date = $duty_date[$k];
            $emp_id = $staff_id[$i];
            $g_id = "group_id_$i" . "_" . $k;
            $ptn_id = "pattern_id_$i" . "_" . $k;
            
            $atdptn_ptn_id = "";
            $reason = "";
            $wk = "atdptn_ptn_id_$i" . "_" . $k;
            if ($$wk != "") {
                if ((int)substr($$wk,0,2) > 0) {
                    $atdptn_ptn_id = (int)substr($$wk,0,2);
                }
                if ((int)substr($$wk,2,2) > 0) {
                    $reason = (int)substr($$wk,2,2);
                }
            }
            
            $reason_2 = "reason_2_$i" . "_" . $k;
            if ($$reason_2 != "") {
                $reason = $$reason_2;
            }
            ///-----------------------------------------------------------------------------
            //他病棟の下書きデータを削除
            ///-----------------------------------------------------------------------------
            /* 上でまとめて削除しているため不要 20120926
            if ($pattern_id == $$ptn_id) {
                if ($atdptn_ptn_id != "") {
                    //下書き
                    $sql = "delete from duty_shift_plan_draft";
                    $cond = "where emp_id = '$emp_id' ";
                    $cond .= "and duty_date = '$wk_date' ";
                    $del = delete_from_table($con, $sql, $cond, $fname);
                    if ($del == 0) {
                        pg_query($con, "rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                }
            }
            */
            //-------------------------------------------------------------------
            // データが存在するかチェック
            //-------------------------------------------------------------------
            $sql = "select * from atdbk";
            $cond = "where emp_id = '$emp_id' and date = '$wk_date' ";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $num = pg_numrows($sel);
            if ($num > 0) {
                ///-----------------------------------------------------------------------------
                // 値が同じ場合、更新しない
                ///-----------------------------------------------------------------------------
                $wk1 = pg_result($sel,0,"pattern");
                $wk2 = pg_result($sel,0,"tmcd_group_id");
                $wk3 = pg_result($sel,0,"reason");
                if ($pattern_id == $$ptn_id) {
                    ///-----------------------------------------------------------------------------
                    // 存在する場合、ＵＰＤＡＴＥ
                    ///-----------------------------------------------------------------------------
                    if ($atdptn_ptn_id != $wk1) {
                        //出勤グループＩＤ／出勤パターンＩＤ
                        $sql = "update atdbk set";
                        $set = array("pattern", "tmcd_group_id");
                        $setvalue = array($atdptn_ptn_id, $$ptn_id);
                        $cond = "where emp_id = '$emp_id' and date = '$wk_date'";
                        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                        if ($upd == 0) {
                            pg_query($con, "rollback");
                            pg_close($con);
                            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                            echo("<script language='javascript'>showErrorPage(window);</script>");
                            exit;
                        }
                    }
                    if ($reason != $wk3) {
                        // 空白の場合はnullを設定する
                        if ($reason == "") {
                            $reason = null;
                        }
                        // 事由
                        $sql = "update atdbk set";
                        $set = array("reason");
                        $setvalue = array($reason);
                        $cond = "where emp_id = '$emp_id' and date = '$wk_date'";
                        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                        if ($upd == 0) {
                            pg_query($con, "rollback");
                            pg_close($con);
                            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                            echo("<script language='javascript'>showErrorPage(window);</script>");
                            exit;
                        }
                    }
                }
            } else {
                if ($atdptn_ptn_id != "") {
                    ///-----------------------------------------------------------------------------
                    // 存在しない場合、ＩＮＳＥＲＴ
                    ///-----------------------------------------------------------------------------
                    $sql = "insert into atdbk (emp_id, date, pattern, reason, tmcd_group_id ";
                    $sql .= ") values (";
                    // シフトグループ $pattern_id -> $$ptn_id 2008/06/09
                    $content = array($emp_id, $wk_date, $atdptn_ptn_id, $reason, $$ptn_id);
                    $ins = insert_into_table($con, $sql, $content, $fname);
                    if ($ins == 0) {
                        pg_query($con, "rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                }
            }
            //応援追加情報更新
            //日別の確認、勤務パターン有、かつ、
            //（職員設定のシフトグループと違うかまたは未設定の場合、または、
            //対象シフトグループと違う場合、または、
            //対象以外のシフトグループに登録されている場合）
            if (($atdptn_ptn_id != "") && 
                    ($arr_assist_group[$k-1] != $belong_group_array["$emp_id"] ||
                        $arr_assist_group[$k-1] != $group_id ||
                        $wk_emp_group_cnt > 0)
                ) {
                //登録
                $sql = "insert into duty_shift_plan_assist (emp_id, duty_date, group_id, assist_flg, main_group_id ";
                $sql .= ") values (";
                $content = array($emp_id, $wk_date, $arr_assist_group[$k-1], "1", $belong_group_array["$emp_id"]);
                $ins = insert_into_table($con, $sql, $content, $fname);
                if ($ins == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
            }
        }
	}
}



/******************************************************************************/
// atdbkrsltへのデータ反映
/******************************************************************************/

// 実績への反映を行わない 20090703
/*
///-----------------------------------------------------------------------------
// 勤務実績ＤＢ(atdbkrslt）へのデータ反映
///-----------------------------------------------------------------------------
// 下書き以外、希望非表示時以外
if ($draft_flg == "" && $hope_save_flg == "") {
	for($i=0;$i<$data_cnt;$i++) {

	//画面上で更新があった場合のみ以下の処理を行う。ただし、職員の削除がある場合は無条件に行う。
	if ($upd_flg[$i] != "1" && $delete_id_cnt == 0) {
		continue;
	}

	for($k=1;$k<=$day_cnt;$k++) {
		///-----------------------------------------------------------------------------
		//項目の配列編集
		///-----------------------------------------------------------------------------
		$wk_date = $duty_date[$k];
		$emp_id = $staff_id[$i];
		$g_id = "group_id_$i" . "_" . $k;
		$ptn_id = "pattern_id_$i" . "_" . $k;

		$atdptn_ptn_id = "";
		$reason = "";
		$wk = "atdptn_ptn_id_$i" . "_" . $k;
		if ($$wk != "") {
			if ((int)substr($$wk,0,2) > 0) {
				$atdptn_ptn_id = (int)substr($$wk,0,2);
			}
			if ((int)substr($$wk,2,2) > 0) {
				$reason = (int)substr($$wk,2,2);
			}
		}

		$reason_2 = "reason_2_$i" . "_" . $k;
		if ($$reason_2 != "") {
			$reason = $$reason_2;
		}
		//-------------------------------------------------------------------
		// データが存在するかチェック
		//-------------------------------------------------------------------
		$sql = "select * from atdbkrslt";
		$cond = "where emp_id = '$emp_id' and date = '$wk_date' ";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		if ($num > 0) {
			///-----------------------------------------------------------------------------
			// 値が同じ場合、更新しない
			///-----------------------------------------------------------------------------
			$wk1 = pg_result($sel,0,"pattern");
			$wk2 = pg_result($sel,0,"tmcd_group_id");
			$wk3 = pg_result($sel,0,"reason");
			if ($pattern_id == $$ptn_id) {
				///-----------------------------------------------------------------------------
				// 存在する場合、ＵＰＤＡＴＥ
				///-----------------------------------------------------------------------------
//				// 時刻が設定済みの場合、更新しない 2009/02/23
//				$wk_start_time = pg_result($sel,0,"start_time");
//				$wk_end_time = pg_result($sel,0,"end_time");
//				if ($atdptn_ptn_id != $wk1 && 
//					($wk_start_time == "" && $wk_end_time == "")) {
				// 出勤パターンIDが未設定なら設定する
				if ($wk1 == "") {
					//出勤グループＩＤ／出勤パターンＩＤ
					$sql = "update atdbkrslt set";
					$set = array("pattern", "tmcd_group_id");
					$setvalue = array($atdptn_ptn_id, $$ptn_id);
					$cond = "where emp_id = '$emp_id' and date = '$wk_date'";
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
					if ($upd == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type='text/javascript' src='./js/showpage.js'></script>");
						echo("<script language='javascript'>showErrorPage(window);</script>");
						exit;
					}
					if ($reason != $wk3) {
						// 空白の場合はnullを設定する
						if ($reason == "") {
							$reason = null;
						}
						// 事由
						$sql = "update atdbkrslt set";
						$set = array("reason");
						$setvalue = array($reason);
						$cond = "where emp_id = '$emp_id' and date = '$wk_date'";
						$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
						if ($upd == 0) {
							pg_query($con, "rollback");
							pg_close($con);
							echo("<script type='text/javascript' src='./js/showpage.js'></script>");
							echo("<script language='javascript'>showErrorPage(window);</script>");
							exit;
						}
					}
				}
			}
		} else {
			if ($atdptn_ptn_id != "") {
				///-----------------------------------------------------------------------------
				// 存在しない場合、ＩＮＳＥＲＴ
				///-----------------------------------------------------------------------------
			    $sql = "insert into atdbkrslt (emp_id, date, pattern, reason, tmcd_group_id ";
				$sql .= ") values (";
				// シフトグループ $pattern_id -> $$ptn_id 2008/06/09
				$content = array($emp_id, $wk_date, $atdptn_ptn_id, $reason, $$ptn_id);
				$ins = insert_into_table($con, $sql, $content, $fname);
				if ($ins == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}
		}
	}
	}
}
*/
//勤務シフト希望登録、勤務シフト希望非表示時
if ($plan_hope_flg == "2" || $hope_save_flg == "1") {
	///-----------------------------------------------------------------------------
	// 選択された勤務パターングループの勤務シフト情報を登録
	///-----------------------------------------------------------------------------
	for($i=0;$i<$data_cnt;$i++) {
		
		//画面上で更新があった場合のみ以下の処理を行う。ただし、職員の削除がある場合は無条件に行う。
		if ($rslt_upd_flg[$i] != "1" && $delete_id_cnt == 0) {
			continue;
		}
		
		
		//応援情報
		$rslt_assist_str = "rslt_assist_group_$i";
		$arr_rslt_assist_group = split(",", $$rslt_assist_str);
		///-----------------------------------------------------------------------------
		//項目の配列編集
		///-----------------------------------------------------------------------------
		$emp_id = $staff_id[$i];
		///-----------------------------------------------------------------------------
		//指定職員ＩＤのデータを削除
		///-----------------------------------------------------------------------------
		$sql = "delete from duty_shift_plan_individual";
		$cond = "where emp_id = '$emp_id' ";
		$cond .= "and duty_date >= '$start_date' and duty_date <= '$end_date' ";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		///-----------------------------------------------------------------------------
		//登録
		///-----------------------------------------------------------------------------
		for($k=1;$k<=$day_cnt;$k++) {
			///-----------------------------------------------------------------------------
			//項目の配列編集
			///-----------------------------------------------------------------------------
			$ptn_id = "rslt_pattern_id_$i" . "_" . $k;
			$wk_date = $duty_date[$k];
			
			$atdptn_ptn_id = "";
			$reason = "";
			$wk = "rslt_id_$i" . "_" . $k;
			if ($$wk != "") {
				if ((int)substr($$wk,0,2) > 0) {
					$atdptn_ptn_id = (int)substr($$wk,0,2);
				}
				if ((int)substr($$wk,2,2) > 0) {
					$reason = (int)substr($$wk,2,2);
				}
			}
			
			$reason_2 = "rslt_reason_2_$i" . "_" . $k;
			if ($$reason_2 != "") {
				$reason = $$reason_2;
			}
			///-----------------------------------------------------------------------------
			//データが存在する場合のみ登録
			///-----------------------------------------------------------------------------
			if (($group_id != "") &&
					($$ptn_id != "") &&
					($atdptn_ptn_id != "") &&
					($duty_date[$k] != "")) {
				///-----------------------------------------------------------------------------
				//ＳＱＬ
				///-----------------------------------------------------------------------------
				$sql = "insert into duty_shift_plan_individual (group_id, emp_id, duty_date, pattern_id, atdptn_ptn_id, reason";
				$sql .= ") values (";
				//グループID（応援追加対応）
				$wk_group_id = $arr_rslt_assist_group[$k-1];
				$content = array($wk_group_id, $emp_id, $wk_date, $$ptn_id, $atdptn_ptn_id, $reason);
				$ins = insert_into_table($con, $sql, $content, $fname);
				if ($ins == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}
		}
	}
	
}
///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query("commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);

//全画面の場合、呼出元も再表示
if ($fullscreen_flg == "1") {
	echo("<script type=\"text/javascript\">opener.location.href = 'duty_shift_menu.php?session=$session&group_id=$group_id&duty_yyyy=$duty_yyyy&duty_mm=$duty_mm&cause_group_id=$group_id&plan_results_flg=$plan_results_flg&plan_hope_flg=$plan_hope_flg&plan_duty_flg=$plan_duty_flg&plan_comment_flg=$plan_comment_flg&term_chg_id=$term_chg_id&total_disp_flg=$total_disp_flg&hol_dt_flg=$hol_dt_flg&stamp_flg=$stamp_flg';</script>");
}

///-----------------------------------------------------------------------------
// 遷移元画面を再表示
///-----------------------------------------------------------------------------
echo("<script type=\"text/javascript\">location.href = 'duty_shift_menu.php?session=$session&group_id=$group_id&duty_yyyy=$duty_yyyy&duty_mm=$duty_mm&fullscreen_flg=$fullscreen_flg&cause_group_id=$group_id&plan_results_flg=$plan_results_flg&plan_hope_flg=$plan_hope_flg&plan_duty_flg=$plan_duty_flg&plan_comment_flg=$plan_comment_flg&term_chg_id=$term_chg_id&total_disp_flg=$total_disp_flg&hol_dt_flg=$hol_dt_flg&stamp_flg=$stamp_flg';</script>");
?>