<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理｜入退院登録−栄養問診表参照</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");
require("get_values.ini");
require("conf/sql.inf");
require("inpatient_list_common.php");

$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病床管理権限チェック
$auth = check_authority($session,14,$fname);
if($auth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

//DBへのコネクション作成
$con = connect2db($fname);

// 患者氏名の取得
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f'";
$sel = select_from_table($con,$SQL85,$cond,$fname);		//ptifmstから取得
if($sel == 0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$lt_kj_nm = pg_result($sel,0,"ptif_lt_kaj_nm");
$ft_kj_nm = pg_result($sel,0,"ptif_ft_kaj_nm");

// 栄養問診表情報の取得
$sql = "select * from inptnuthist";
$cond = "where ptif_id = '$pt_id' and inpt_in_dt = '$in_dt' and inpt_in_tm = '$in_tm'";
$sel_nut = select_from_table($con,$sql,$cond,$fname);
if($sel_nut == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel_nut) > 0) {
	$qq1 = pg_result($sel_nut,0,"nut_q1");
	$qq1_1 = pg_result($sel_nut,0,"nut_q1_1");
	$qq1_2 = pg_result($sel_nut,0,"nut_q1_2");
	$qq1_3 = pg_result($sel_nut,0,"nut_q1_3");
	$qq1_4 = pg_result($sel_nut,0,"nut_q1_4");
	$qq1_5_com = pg_result($sel_nut,0,"nut_q1_5_com");
	$qq1_6_com = pg_result($sel_nut,0,"nut_q1_6_com");
	$qq2_1 = pg_result($sel_nut,0,"nut_q2_1");
	$qq2_2 = pg_result($sel_nut,0,"nut_q2_2");
	$qq2_3 = pg_result($sel_nut,0,"nut_q2_3");
	$qq2_4 = pg_result($sel_nut,0,"nut_q2_4");
	$qq2_5 = pg_result($sel_nut,0,"nut_q2_5");
	$qq2_6 = pg_result($sel_nut,0,"nut_q2_6");
	$qq2_7 = pg_result($sel_nut,0,"nut_q2_7");
	$qq2_8 = pg_result($sel_nut,0,"nut_q2_8");
	$qq3 = pg_result($sel_nut,0,"nut_q3");
	$qq4 = pg_result($sel_nut,0,"nut_q4");
	$qq5_1 = pg_result($sel_nut,0,"nut_q5_1");
	$qq5_2 = pg_result($sel_nut,0,"nut_q5_2");
}

// 病棟一覧を取得
$sql = "select bldg_cd, ward_cd, ward_name from wdmst";
$cond = "where ward_del_flg = 'f' order by bldg_cd, ward_cd";
$sel_ward = select_from_table($con, $sql, $cond, $fname);
if ($sel_ward == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
?>
<script language="javascript" type="text/javascript">
function initPage() {
	setListDateDisabled();
}

<? show_inpatient_list_common_javascript(); ?>

function openUpdateWin() {

	var url = 'inpatient_update.php';
	url += '?session=<? echo $session; ?>';
	url += '&pt_id=<? echo $pt_id; ?>';

	window.open(url, 'newwin', 'width=540,height=440,scrollbars=yes');

}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#bdd1e7"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床管理トップ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#5279a5"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>入退院登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr>
<td width="70%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>入院予定〜退院登録</b></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>検索条件設定</b></font></td>
</tr>
<tr>
<td valign="middle">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td align="center"><form action="inpatient_vacant_search.php" method="get"><input type="submit" value="空床検索"><input type="hidden" name="session" value="<? echo($session); ?>"></form><br><form action="inpatient_waiting_list.php" method="get"><input type="submit" value="待機患者"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_reserve_register_list.php" method="get"><input type="submit" value="入院予定"><input type="hidden" name="session" value="<? echo($session); ?>"><input type="hidden" name="mode" value="search"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_register_list.php" method="get"><input type="submit" value="入院"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_go_out_register_list.php" method="get"><input type="submit" value="外出・外泊"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_move_register_list.php" method="get"><input type="submit" value="転棟"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="out_inpatient_reserve_register_list.php" method="get">
<input type="submit" value="退院予定"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="out_inpatient_register_list.php" method="get"><input type="submit" value="退院"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
</tr>
</table>
</td>
<td valign="middle">
<? show_inpatient_list_form($con, $sel_ward, "", "", "", "", "", "", "", "", "", $session, $fname); ?>
</td>
</tr>
</table>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<!-- タブ2 -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基礎データ</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="inpatient_detail_ref.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="inpatient_check_list_ref.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">チェックリスト</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#5279a5"><a href="inpatient_nutrition_ref.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>栄養問診表</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="170" align="center" bgcolor="#bdd1e7"><a href="inpatient_exception.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均在院日数除外指定</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="outpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院情報</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pt_id); ?></font></td>
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者氏名</font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$lt_kj_nm $ft_kj_nm"); ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="800" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="54%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">問1-1）食品のアレルギーがありますか？</font></td>
<td width="46%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="qq1" value="t" <? if($qq1 == t){echo("checked");} ?>>ある&nbsp;<input type="radio" name="qq1" value="f" <? if($qq1 == f){echo("checked");} ?>>なし</font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">問1-2）問1-1で”ある”とお答えの方はその食品も教えてください</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="qq1_1" <? if($qq1_1 == t){echo("checked");} ?>>卵&nbsp;<input type="checkbox" name="qq1_2" <? if($qq1_2 == t){echo("checked");} ?>>肉&nbsp;<input type="checkbox" name="qq1_3" <? if($qq1_3 == t){echo("checked");} ?>>大豆製品<br>
<input type="checkbox" name="qq1_4" <? if($qq1_4 == t){echo("checked");} ?>>魚&nbsp;&nbsp;種類：（<? echo($qq1_5_com); ?>）<br>
&nbsp;その他：（<? echo($qq1_6_com); ?>）</font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">問2）召し上がれないものがありますか？</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="qq2_1" <? if($qq2_1 == t){echo("checked");} ?>>牛肉&nbsp;<input type="checkbox" name="qq2_2" <? if($qq2_2 == t){echo("checked");} ?>>豚肉&nbsp;<input type="checkbox" name="qq2_3" <? if($qq2_3 == t){echo("checked");} ?>>鶏肉&nbsp;<input type="checkbox" name="qq2_4" <? if($qq2_4 == t){echo("checked");} ?>>魚&nbsp;<input type="checkbox" name="qq2_5" <? if($qq2_5 == t){echo("checked");} ?>>卵<br><input type="checkbox" name="qq2_6" <? if($qq2_6 == t){echo("checked");} ?>>豆腐&nbsp;<input type="checkbox" name="qq2_7" <? if($qq2_7 == t){echo("checked");} ?>>納豆&nbsp;<input type="checkbox" name="qq2_8" <? if($qq2_8 == t){echo("checked");} ?>>牛乳</font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">問3）食事を細かくすることを希望しますか？</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="qq3" value="t" <? if($qq3 == t){echo("checked");} ?>>する&nbsp;<input type="radio" name="qq3" value="f" <? if($qq3 == f){echo("checked");} ?>>しない</font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">問4）朝食は和食（ご飯食）と洋食（パン食）どちらを希望しますか？</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="qq4" value="t" <? if($qq4 == t){echo("checked");} ?>>和食&nbsp;<input type="radio" name="qq4" value="f" <? if($qq4 == f){echo("checked");} ?>>パン食</font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">問5）食事指示はありますか？</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="qq5_1" value="t" <? if($qq5_1 == t){echo("checked");} ?>>ある（<input type="radio" name="qq5_2" value="t" <? if($qq5_2 == t){echo("checked");} ?>>治療食&nbsp;<input type="radio" name="qq5_2" value="f" <? if($qq5_2 == f){echo("checked");} ?>>常食）&nbsp;<input type="radio" name="qq5_1" value="f" <? if($qq5_1 == f){echo("checked");} ?>>なし</font></td>
</tr>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
