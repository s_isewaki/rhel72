<?php
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_comedix.php");

$fname = $_SERVER['PHP_SELF'];
$mode = $_REQUEST['mode'];
$order = $_REQUEST['order'];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($mode === 'delete' and !is_array($newscate_ids)) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('削除対象が選択されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 削除の場合
if ($mode === 'delete') {
    // チェックされたカテゴリをループ

    // アクセスログ
    aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

    foreach ($newscate_ids as $tmp_newscate_id) {

        // お知らせに使用されていないかチェック
        $sql = "select count(*) from news";
        $cond = "where news_add_category = $tmp_newscate_id";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        if (pg_fetch_result($sel, 0, 0) > 0) {
            $sql = "select newscate_name from newscate";
            $cond = "where newscate_id = $tmp_newscate_id";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $tmp_newscate_name = pg_fetch_result($sel, 0, "newscate_name");

            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('「{$tmp_newscate_name}」は、お知らせに使用されているため削除できません。');</script>");
            echo("<script type=\"text/javascript\">history.back();(window);</script>");
            exit;
        }

        // カテゴリを削除
        $sql = "delete from newscate";
        $cond = "where newscate_id = $tmp_newscate_id";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        // カテゴリ部署情報を削除
        $sql = "delete from newscatedept";
        $cond = "where newscate_id = $tmp_newscate_id";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        // カテゴリ職種情報を削除
        $sql = "delete from newscatejob";
        $cond = "where newscate_id = $tmp_newscate_id";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        // カテゴリ役職情報を削除
        $sql = "delete from newscatest";
        $cond = "where newscate_id = $tmp_newscate_id";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

// 並べ替えの場合
else if ($mode === 'sort') {
    $no = 1;
    foreach ($order as $newscate_id) {
        $data = array("order_no" => $no++);
        $upd = update_set_table($con, "update newscate set", array_keys($data), array_values($data), "where newscate_id = " . p($newscate_id), $fname);
        if ($upd == 0) {
            pg_close($con);
            js_error_exit();
        }
    }
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// カテゴリ一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'news_cate_list.php?session=$session&page=$page';</script>");
?>
