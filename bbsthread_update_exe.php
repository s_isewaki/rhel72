<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="bbsthread_update.php" method="post">
<input type="hidden" name="bbsthread_title" value="<? echo($bbsthread_title); ?>">
<input type="hidden" name="bbsthread" value="<? echo($bbsthread); ?>">
<input type="hidden" name="ref_dept_st_flg" value="<? echo($ref_dept_st_flg); ?>">
<input type="hidden" name="ref_dept_flg" value="<? echo($ref_dept_flg); ?>">
<input type="hidden" name="ref_class_src" value="<? echo($ref_class_src); ?>">
<input type="hidden" name="ref_atrb_src" value="<? echo($ref_atrb_src); ?>">
<?
if (is_array($hid_ref_dept)) {
	foreach ($hid_ref_dept as $tmp_dept_id) {
		echo("<input type=\"hidden\" name=\"ref_dept[]\" value=\"$tmp_dept_id\">\n");
	}
} else {
	$hid_ref_dept = array();
}
?>
<input type="hidden" name="ref_st_flg" value="<? echo($ref_st_flg); ?>">
<?
if (is_array($ref_st)) {
	foreach ($ref_st as $tmp_st_id) {
		echo("<input type=\"hidden\" name=\"ref_st[]\" value=\"$tmp_st_id\">\n");
	}
} else {
	$ref_st = array();
}
?>
<input type="hidden" name="target_id_list1" value="<? echo($target_id_list1); ?>">
<input type="hidden" name="target_id_list2" value="<? echo($target_id_list2); ?>">
<?
if ($target_id_list1 != "") {
	$hid_ref_emp = split(",", $target_id_list1);
} else {
	$hid_ref_emp = array();
}
?>
<input type="hidden" name="upd_dept_st_flg" value="<? echo($upd_dept_st_flg); ?>">
<input type="hidden" name="upd_dept_flg" value="<? echo($upd_dept_flg); ?>">
<input type="hidden" name="upd_class_src" value="<? echo($upd_class_src); ?>">
<input type="hidden" name="upd_atrb_src" value="<? echo($upd_atrb_src); ?>">
<?
if (is_array($hid_upd_dept)) {
	foreach ($hid_upd_dept as $tmp_dept_id) {
		echo("<input type=\"hidden\" name=\"upd_dept[]\" value=\"$tmp_dept_id\">\n");
	}
} else {
	$hid_upd_dept = array();
}
?>
<input type="hidden" name="upd_st_flg" value="<? echo($upd_st_flg); ?>">
<?
if (is_array($upd_st)) {
	foreach ($upd_st as $tmp_st_id) {
		echo("<input type=\"hidden\" name=\"upd_st[]\" value=\"$tmp_st_id\">\n");
	}
} else {
	$upd_st = array();
}
?>
<?
if ($target_id_list2 != "") {
	$hid_upd_emp = split(",", $target_id_list2);
} else {
	$hid_upd_emp = array();
}
?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="category_id" value="<? echo($category_id); ?>">
<input type="hidden" name="theme_id" value="<? echo($theme_id); ?>">
<input type="hidden" name="sort" value="<? echo($sort); ?>">
<input type="hidden" name="show_content" value="<? echo($show_content); ?>">
<input type="hidden" name="ref_toggle_mode" value="<? echo($ref_toggle_mode); ?>">
<input type="hidden" name="upd_toggle_mode" value="<? echo($upd_toggle_mode); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="path" value="<? echo($path); ?>">
</form>
<?
require("about_session.php");
require("about_authority.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$auth_id = ($path == "2") ? 62 : 1;
$check_auth = check_authority($session, $auth_id, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック
if ($bbsthread_title == "") {
	echo("<script type=\"text/javascript\">alert('タイトルを入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($bbsthread_title) > 100) {
	echo("<script type=\"text/javascript\">alert('タイトルが長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($bbsthread == "") {
	echo("<script type=\"text/javascript\">alert('内容を入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($bbsthread) > 500) {
	echo("<script type=\"text/javascript\">alert('内容が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ((($ref_dept_flg != "1" && count($hid_ref_dept) == 0) || ($ref_st_flg != "1" && count($ref_st) == 0)) && count($hid_ref_emp) == 0) {
	echo("<script type=\"text/javascript\">alert('参照可能範囲が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ((($upd_dept_flg != "1" && count($hid_upd_dept) == 0) || ($upd_st_flg != "1" && count($upd_st) == 0)) && count($hid_upd_emp) == 0) {
	echo("<script type=\"text/javascript\">alert('投稿可能範囲が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// 登録内容の編集
if ($ref_dept_st_flg != "t") {$ref_dept_st_flg = "f";}
if ($ref_dept_flg == "") {$ref_dept_flg = null;}
if ($ref_st_flg == "") {$ref_st_flg = null;}
if ($upd_dept_st_flg != "t") {$upd_dept_st_flg = "f";}
if ($upd_dept_flg == "") {$upd_dept_flg = null;}
if ($upd_st_flg == "") {$upd_st_flg = null;}

// トランザクションの開始
pg_query($con, "begin transaction");

// テーマ情報を更新
$sql = "update bbsthread set";
$set = array("bbsthread_title", "bbsthread", "ref_dept_st_flg", "ref_dept_flg", "ref_st_flg", "upd_dept_st_flg", "upd_dept_flg", "upd_st_flg", "bbscategory_id");
$setvalue = array($bbsthread_title, $bbsthread, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg, $category_id);
$cond = "where bbsthread_id = $theme_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 参照可能部署情報を登録
$sql = "delete from bbsrefdept";
$cond = "where bbsthread_id = $theme_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_ref_dept as $tmp_val) {
	list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = split("-", $tmp_val);
	$sql = "insert into bbsrefdept (bbsthread_id, class_id, atrb_id, dept_id) values (";
	$content = array($theme_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 参照可能役職情報を登録
$sql = "delete from bbsrefst";
$cond = "where bbsthread_id = $theme_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($ref_st as $tmp_st_id) {
	$sql = "insert into bbsrefst (bbsthread_id, st_id) values (";
	$content = array($theme_id, $tmp_st_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 参照可能職員情報を登録
$sql = "delete from bbsrefemp";
$cond = "where bbsthread_id = $theme_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_ref_emp as $tmp_emp_id) {
	$sql = "insert into bbsrefemp (bbsthread_id, emp_id) values (";
	$content = array($theme_id, $tmp_emp_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 投稿可能部署情報を登録
$sql = "delete from bbsupddept";
$cond = "where bbsthread_id = $theme_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_upd_dept as $tmp_val) {
	list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = split("-", $tmp_val);
	$sql = "insert into bbsupddept (bbsthread_id, class_id, atrb_id, dept_id) values (";
	$content = array($theme_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 投稿可能役職情報を登録
$sql = "delete from bbsupdst";
$cond = "where bbsthread_id = $theme_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($upd_st as $tmp_st_id) {
	$sql = "insert into bbsupdst (bbsthread_id, st_id) values (";
	$content = array($theme_id, $tmp_st_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 投稿可能職員情報を登録
$sql = "delete from bbsupdemp";
$cond = "where bbsthread_id = $theme_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($hid_upd_emp as $tmp_emp_id) {
	$sql = "insert into bbsupdemp (bbsthread_id, emp_id) values (";
	$content = array($theme_id, $tmp_emp_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 投稿一覧画面に遷移
if ($path == "2") {
	echo("<script type=\"text/javascript\">location.href = 'bbs_admin_menu.php?session=$session&category_id=$category_id&theme_id=$theme_id&sort=$sort&show_content=$show_content';</script>");
} else {
	echo("<script type=\"text/javascript\">location.href = 'bbsthread_menu.php?session=$session&category_id=$category_id&theme_id=$theme_id&sort=$sort&show_content=$show_content';</script>");
}
?>
