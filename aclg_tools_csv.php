<?
ob_start();

require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;
//$detail = $_GET['detail'];
$file = "accessLogCsv";



$file = mb_convert_encoding($file,'sjis-win', 'auto');
// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 92, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}


// データベースに接続
$con = connect2db($fname);

if($date_y1 == '-' ) {
	$date_y1 = date("Y");
}
if($date_m1 == '-' ) {
	$date_m1 = date("m");
}
if($date_d1 == '-' ) {
	$date_d1 = date("d");
}
if($date_y2 == '-' ) {
	$date_y2 = date("Y");
}
if($date_m2 == '-' ) {
	$date_m2 = date("m");
}
if($date_d2 == '-' ) {
	$date_d2 = date("d");
}

$date_ymd1 = $date_y1.$date_m1.$date_d1;
$date_ymd2 = $date_y2.$date_m2.$date_d2;


// 職員一覧をCSV形式で取得
$data_contents = get_data_csv($set_prty, $con, $date_ymd1, $date_ymd2, $logcate);

// データベース接続を閉じる
pg_close($con);

// CSVを出力
$file_name = $file.".csv";


ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($data_contents));
echo($data_contents);
ob_end_flush();

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

// 情報をCSV形式で取得
function get_data_csv($set_prty, $con, $date_ymd1, $date_ymd2, $logcate)
{

	$str_priority = array();
	$str_priority["0"] = "重要情報";
	$str_priority["1"] = "一般情報";
	$str_priority["2"] = "付加情報";

	
	
	// ヘッダ行を設定
	$buf = '"アクセス日時","職員名","機能","処理","レベル","内容"'. "\r\n";
	
	$sql = "select aclg_data.emp_id,aclg_data.data,empmst.emp_lt_nm,empmst.emp_ft_nm,ac_date, aclg_data_link.cate_id,aclg_data_link.detail_id, aclg_data_link.log_cate,aclg_data_link.log_detail,aclg_data_link.priority,aclg_data_link.log_type,aclg_data.status1 from aclg_data";
	$cond = " join aclg_data_link on aclg_data.cate_id = aclg_data_link.cate_id and aclg_data.detail_id = aclg_data_link.detail_id and aclg_data.filename = aclg_data_link.filename and aclg_data.cate_id != '0006'";

	if($set_prty != "0")
	{
		$cond .= " and aclg_data_link.priority = '".$set_prty."' ";
	}
	
	$cond .= " join empmst on empmst.emp_id=aclg_data.emp_id ";
	if($emp_name != "")
	{
		$fullnm = str_replace(" ", "", $emp_name);
		$fullnm = str_replace("　", "", $fullnm);
		$cond .= "and (emp_lt_nm like '%$emp_name%' or emp_ft_nm like '%$emp_name%' or emp_kn_lt_nm like '%$emp_name%' or emp_kn_ft_nm like '%$emp_name%' or (emp_lt_nm || emp_ft_nm) like '%$fullnm%' or (emp_kn_lt_nm || emp_kn_ft_nm) like '%$fullnm%')";
	}	
	
	if($date_ymd1 != '') {
		$cond .= "where $date_ymd1 <= to_number(substr(ac_date,1,8),'00000000')";
	}

	if($date_ymd2 != '') {
		if($date_ymd1 != '') {
			$cond .= " and to_number(substr(ac_date,1,8),'00000000') <= $date_ymd2";
		}
		else {
			$cond .= "where to_number(substr(ac_date,1,8),'00000000') <= $date_ymd2";
		}
	}

	if($logcate != "")
	{
		if($date_ymd1 != '' || $date_ymd2 != '') 
		{
			$cond .= " and aclg_data.cate_id = '$logcate'";
		}
		else {
			$cond .= "where aclg_data.cate_id = '$logcate'";
		}
		
	}	



	$cond .= " order by ac_date";
	// [降順]に並べ替える
	if ($_POST['set_sort'] == 2) {
		$cond .= " desc";
	}

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// ボディ行を設定
	while ($row = pg_fetch_array($sel)) 
	{
		
		switch ($row["priority"]){
			case '1':
				$priority = "重要情報";
				break;
			case '2':
				$priority = "一般情報";
				break;
			case '3':
				$priority = "付加情報";
				break;
			default :
				$priority = "取得しない";
				break;
		}
		$yyyy = substr($row["ac_date"],0,4);
		$mm = substr($row["ac_date"],4,2);
		$dd = substr($row["ac_date"],6,2);
		$HH = substr($row["ac_date"],8,2);
		$MM = substr($row["ac_date"],10,2);
		$SS = substr($row["ac_date"],12,2);
		
		//内容
		$aclg_contents = str_replace("<br />",";",$row["status1"]);
		//$aclg_contents = $row["status1"];
		
		$buf .= $yyyy."/".$mm."/".$dd." ".$HH.":".$MM.":".$SS.",";
//		$buf .= $row["emp_id"].",";
		$buf .= $row["emp_lt_nm"]." ".$row["emp_ft_nm"].",";
		$buf .= $row["log_cate"].",";
		$buf .= $row["log_detail"].",";
		$buf .= $priority.",";
		$buf .= $aclg_contents."\r\n";
	}

	// Shift_JISに変換
	return mb_convert_encoding($buf, "SJIS", "EUC-JP");
}
?>
