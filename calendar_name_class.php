<?php
class calendar_name_class
{

	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション
    var $arr_calendarname = array(); //カレンダー名テーブル
	var $arr_history = array(); //所定労働時間の履歴

	/** コンストラクタ
	 * 
	 * @param $con          DBコネクション
	 * @param $fname        画面名
	 *
	 */
	function calendar_name_class($con, $fname)
	{

		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
        $this->arr_calendarname = $this->get_calendarname();
        $this->arr_history = $this->get_history("asc");

	}
	/** 勤務日設定値を取得
	 *
     * @return mixed カレンダー名テーブル
     */
	function get_calendarname() {
		$sql = "select * from calendarname";
		$cond = "";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr_calendarname = array();
		if (pg_num_rows($sel) > 0) {
			$arr_calendarname = pg_fetch_array($sel);
		}
		return $arr_calendarname;
	}
	/**
	 * 所定労働時間の履歴を取得
	 * 
	 * @param mixed $order 並び順 "","asc" or "desc"
     * @return mixed 所定労働時間履歴テーブルの配列
	 *
	 */
	function get_history($order="asc") {
		$sql = "select * from calendarname_time_history ";
		$cond = "order by histdate $order";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
        $arr_history = array();
        if (pg_num_rows($sel) > 0) {
            $arr_history = pg_fetch_all($sel);
        }
        /*
		$arr_history = array();
		while ($r = pg_fetch_array($sel)) {
			$arr_history[] = $r;
		}
        */
		return $arr_history;
	}
	/**
	 * 所定労働時間を取得
	 * 
     * @param mixed $date 対象日付YYYYMMDD形式
     * @return mixed 対象日付に対応する変更日時の所定労働時間履歴の配列、履歴がない場合は、calendarnameテーブルのデータ
     * （履歴の先頭から確認し変更日時以前であればその履歴レコードを返す）
	 */
	function get_calendarname_time($date) {

        $arr_history = $this->arr_history;
		if (count($arr_history) == 0) {
            return $this->arr_calendarname;
		}

        $match_flg = false;
		for ($i=0; $i<count($arr_history); $i++) {
            //日付YYYYMMDD形式
            $wk_histdate = substr($arr_history[$i]["histdate"], 0, 4).substr($arr_history[$i]["histdate"], 5, 2).substr($arr_history[$i]["histdate"], 8, 2);
            if ($date <= $wk_histdate) {
				$match_flg = true;
				break;
			}
		}
		if ($match_flg ) {
			return $arr_history[$i];
		}
		else {
            return $this->arr_calendarname;
		}
	}
}
