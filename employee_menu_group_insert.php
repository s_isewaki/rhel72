<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($group_nm == "") {
	echo("<script type=\"text/javascript\">alert('メニューグループ名が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($group_nm) > 30) {
	echo("<script type=\"text/javascript\">alert('メニューグループ名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// メニューグループIDを採番
$sql = "select max(group_id) from menugroup";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$group_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// メニューグループ名を登録
$sql = "insert into menugroup (group_id, group_nm) values (";
$content = array($group_id, $group_nm);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュし、自画面を閉じる
echo("<script type=\"text/javascript\">opener.location.href = 'employee_menu_group.php?session=$session&group_id=$group_id';</script>");
echo("<script type=\"text/javascript\">self.close();</script>");
?>
