<?php
require_once("about_comedix.php");
require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0") {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "SELECT * FROM session WHERE session_id='{$session}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}
$emp = pg_fetch_assoc($sel);


// スケジュールを取得
$schd_id = $_REQUEST["schd_id"];
$table_name = ($_REQUEST["timeless"] != "t") ? "schdmst" : "schdmst2";
$sql = "SELECT * FROM $table_name WHERE schd_id='{$schd_id}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}
$schd = pg_fetch_assoc($sel);

// 利用者とスケジュールの所有者を確認
if ($emp["emp_id"] != $schd["emp_id"]) {
    exit;
}

// トランザクションを開始
pg_query($con, "begin");

// アラーム確認を実行
$sql = "UPDATE $table_name SET";
$cond = "WHERE schd_id='{$schd_id}'";
$set = array("alarm_confirm");
$setvalue = array(1);
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

