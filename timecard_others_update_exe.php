<?
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');

require_once("about_comedix.php");
require_once("show_timecard_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 入力チェック
if ($irrg_time_w != "") {
    if (preg_match("/^\d{1,2}$/", $irrg_time_w) == 0) {
        echo("<script type=\"text/javascript\">alert('週の所定労働時間は2桁以下の半角数字で入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
    else {
        $irrg_time_w = intval($irrg_time_w);
    }
    if ($irrg_time_wd == "") {
        echo("<script type=\"text/javascript\">alert('週の開始曜日を選択してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}
if ($irrg_time_m != "") {
    if (preg_match("/^\d{1,3}$/", $irrg_time_m) == 0) {
        echo("<script type=\"text/javascript\">alert('月の所定労働時間は3桁以下の半角数字で入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
    else {
        $irrg_time_m = intval($irrg_time_m);
    }
}
if ($rest4 != "") {
    if (preg_match("/^\d{1,2}$/", $rest4) == 0) {
        echo("<script type=\"text/javascript\">alert('時間給者の休憩時間(4時間超)は2桁以下の半角数字で入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
    else {
        $rest4 = intval($rest4);
    }
}
if ($rest5 != "") {
    if (preg_match("/^\d{1,2}$/", $rest5) == 0) {
        echo("<script type=\"text/javascript\">alert('時間給者の休憩時間(4時間超)は2桁以下の半角数字で入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
    else {
        $rest5 = intval($rest5);
    }
}
if ($rest1 != "") {
    if (preg_match("/^\d{1,2}$/", $rest1) == 0) {
        echo("<script type=\"text/javascript\">alert('時間給者の休憩時間(6時間超)は2桁以下の半角数字で入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
    else {
        $rest1 = intval($rest1);
    }
}
if ($rest2 != "") {
    if (preg_match("/^\d{1,2}$/", $rest2) == 0) {
        echo("<script type=\"text/javascript\">alert('時間給者の休憩時間(8時間超)は2桁以下の半角数字で入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
    else {
        $rest2 = intval($rest2);
    }
}
if ($rest3 != "") {
    if (preg_match("/^\d{1,2}$/", $rest3) == 0) {
        echo("<script type=\"text/javascript\">alert('時間給者の休憩時間(9時間超)は2桁以下の半角数字で入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
    else {
        $rest3 = intval($rest3);
    }
}
if ($rest6 != "") {
    if (preg_match("/^\d{1,3}$/", $rest6) == 0) {
        echo("<script type=\"text/javascript\">alert('時間給者の休憩時間(12時間超)は3桁以下の半角数字で入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
    else {
        $rest6 = intval($rest6);
    }
}
if ($rest7 != "") {
    if (preg_match("/^\d{1,3}$/", $rest7) == 0) {
        echo("<script type=\"text/javascript\">alert('時間給者の休憩時間(20時間超)は3桁以下の半角数字で入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
    else {
        $rest7 = intval($rest7);
    }
}
//月間残業時間45時間超計算
if ($_POST['over_disp_fortyfive_flg'] == "t" &&
	$_POST['over_disp_sixty_flg'] == "f") {
    echo("<script type=\"text/javascript\">alert('月間残業時間45時間超計算をする場合は月間残業時間60時間超計算もするにしてください。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}

//タイトル
if ($timecard_title == "") {
    echo("<script type=\"text/javascript\">alert('タイムカードタイトルを入力してください。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}

if ($timecard_title2 == "") {
    echo("<script type=\"text/javascript\">alert('出勤簿タイトルを入力してください。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}

if ($over_time_apply_type == 1 && $ovtmscr_tm == "") {
    echo("<script type=\"text/javascript\">alert('残業申請画面の表示時間が入力されていません。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

if (empty($ovtmscr_tm) == false && !preg_match("/^\d{1,3}$/", $ovtmscr_tm)) {
    echo("<script type=\"text/javascript\">alert('残業申請画面の表示時間は1〜3桁の数字で入力してください。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

if (empty($over_yet_apply_disp_min) == false && !preg_match("/^\d{1,3}$/", $over_yet_apply_disp_min)) {
    echo("<script type=\"text/javascript\">alert('残業未申請を表示しないための時間は1〜2桁の数字で入力してください。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

// メニュー表示チェック 20130129
if ($menu_view1 == "") {
    $menu_view1 = "f";
}
if ($menu_view2 == "") {
    $menu_view2 = "f";
}
if ($menu_view3 == "") {
    $menu_view3 = "f";
}
if ($menu_view4 == "") {
    $menu_view4 = "f";
}

if ($menu_view5 == "") {
    $menu_view5 = "f";
}
if ($menu_view6 == "") {
    $menu_view6 = "f";
}

//時間有休の項目チェック
if ($paid_hol_hour_full_flag == "") {
    $paid_hol_hour_full_flag = "f";
}
if ($paid_hol_hour_part_flag == "") {
    $paid_hol_hour_part_flag = "f";
}
if ($paid_hol_hour_flag == "t") {
    if ($paid_hol_hour_full_flag == "f" && $paid_hol_hour_part_flag == "f") {
        echo("<script type=\"text/javascript\">alert('有給休暇の時間単位取得を許可する場合は時間有休の対象者を選択してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}

if ($others1 == "--") {
    $others1 = "";
}
if ($others2 == "--") {
    $others2 = "";
}
if ($others3 == "--") {
    $others3 = "";
}
if ($others4 == "--") {
    $others4 = "";
}
if ($others5 == "--") {
    $others5 = "";
}
if ($return_flg != "t") {
    $return_flg = "f";
}

//所属の表示
for ($i = 0; $i < 4; $i++) {
    $varname = "org_disp_flg_" . $i;
    $$varname = ($$varname == "t") ? "t" : "f";
}

if ($all_apply_disp_flg != "t") {
    $all_apply_disp_flg = "f";
}
if ($all_duty_disp_flg != "t") {
    $all_duty_disp_flg = "f";
}
if ($all_legal_over_disp_flg != "t") {
    $all_legal_over_disp_flg = "f";
}
if ($workflow_menu_flg != "t") {
    $workflow_menu_flg = "f";
}
if ($shift_menu_flg != "t") {
    $shift_menu_flg = "f";
}
if ($shift_refer_menu_flg != "t") {
    $shift_refer_menu_flg = "f";
}
if ($shift_list_menu_flg != "t") {
    $shift_list_menu_flg = "f";
}
if ($shift_duty_menu_flg != "t") {
    $shift_duty_menu_flg = "f";
}
if ($year_paid_hol_menu_flg != "t") {
    $year_paid_hol_menu_flg = "f";
}

//残業限度時間　月単位
$ovtm_limit_month = $_POST['ovtm_limit_month'];
if ($ovtm_limit_month != "") {
    if (preg_match("/^\d{1,2}$/", $ovtm_limit_month) == 0) {
        echo("<script type=\"text/javascript\">alert('残業限度時間の月単位は2桁以下の半角数字で入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
    else {
        $ovtm_limit_month = intval($ovtm_limit_month);
    }
}
/*
//残業限度時間　年単位
$ovtm_limit_year = $_POST['ovtm_limit_year'];
if ($ovtm_limit_year != "") {
    if (preg_match("/^\d{1,3}$/", $ovtm_limit_year) == 0) {
        echo("<script type=\"text/javascript\">alert('残業限度時間の年単位は3桁以下の半角数字で入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
    else {
        $ovtm_limit_year = intval($ovtm_limit_year);
    }
}
*/
//ｎ時までの打刻は前日の残業とする
$import_base_time = $_POST['import_base_time'];
if ($import_base_time != "") {
    if (preg_match("/^\d{1,2}$/", $import_base_time) == 0) {
        echo("<script type=\"text/javascript\">alert('打刻を前日の残業とする基準の時刻は2桁以下の半角数字で入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
    else {
        $import_base_time = intval($import_base_time);
        if ($import_base_time < 1 || $import_base_time >= 24) {
            echo("<script type=\"text/javascript\">alert('打刻を前日の残業とする基準の時刻は1から23の範囲で入力してください。');</script>");
            echo("<script type=\"text/javascript\">history.back();</script>");
            exit;
        }
    }
}
// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// 週の時間数が未入力の場合、変則労働期間が週の職員の存在チェック
if ($irrg_time_w == "") {
    $sql = "select count(*) from empcond";
    $cond = "where irrg_type = '1' and exists (select * from authmst where authmst.emp_id = empcond.emp_id and authmst.emp_del_flg = 'f')";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $emp_cnt = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, 0) : 0;
    if ($emp_cnt > 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('変則労働期間が週の職員が存在します。\\n週の時間数を入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}

// 月の時間数が未入力の場合、変則労働期間が月の職員の存在チェック
if ($irrg_time_m == "") {
    $sql = "select count(*) from empcond";
    $cond = "where irrg_type = '2' and exists (select * from authmst where authmst.emp_id = empcond.emp_id and authmst.emp_del_flg = 'f')";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $emp_cnt = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, 0) : 0;
    if ($emp_cnt > 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('変則労働期間が月の職員が存在します。\\n月の時間数を入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}

if (empty($ovtmscr_tm)) {
    $ovtmscr_tm = null;
}

// タイムカードレコードがなければ作成、あれば更新
$sql = "select count(*) from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
if (pg_fetch_result($sel, 0, 0) == 0) {
    $sql = "insert into timecard (" .
        "others1, others2, others3, others4, others5, " .
        "irrg_time_w, irrg_time_wd, irrg_time_m, " .
        "rest1, rest2, rest3, rest4, rest5, rest6, rest7, modify_flg, ovtmscr_tm, show_time_flg, return_flg, " .
        "over_time_apply_type, time_move_flag, type_display_flag, group_display_flag, zen_display_flag, yoku_display_flag, ret_display_flag, time_big_font_flag, delay_overtime_flg, overtime_40_flg, early_leave_time_flg, timecard_title, timecard_appr_disp_flg," .
        "class_disp_flg, atrb_disp_flg, dept_disp_flg, room_disp_flg, over_apply_disp_flg, return_icon_flg, over_disp_split_flg," .
        "all_apply_disp_flg, all_duty_disp_flg, all_legal_over_disp_flg, over_yet_apply_disp_min, " .
        "workflow_menu_flg, shift_menu_flg, shift_refer_menu_flg, shift_list_menu_flg, shift_duty_menu_flg, year_paid_hol_menu_flg, " .
        "menu_view1, menu_view2, menu_view3, menu_view4, menu_view5, menu_view6, menu_view_default, timecard_title2, over_disp_sixty_flg, " .
        "over_disp_forty_flg, over_disp_forty_week, over_forty_persons_full, over_forty_persons_short, over_forty_persons_part, modify_plan_flg";
    ") values (";
    $content = array(
        $others1, $others2, $others3, $others4, $others5,
        $irrg_time_w, $irrg_time_wd, $irrg_time_m,
        $rest1, $rest2, $rest3, $rest4, $rest5, $rest6, $rest7, $modify_flg, $ovtmscr_tm, $show_time_flg, $return_flg,
        $over_time_apply_type, $time_move_flag, $type_display_flag, $group_display_flag, $zen_display_flag, $yoku_display_flag, $ret_display_flag, $time_big_font_flag, $delay_overtime_flg, $overtime_40_flg, $early_leave_time_flg, pg_escape_string($timecard_title), $timecard_appr_disp_flg,
        $org_disp_flg_0, $org_disp_flg_1, $org_disp_flg_2, $org_disp_flg_3, $over_apply_disp_flg, $return_icon_flg, $over_disp_split_flg,
        $all_apply_disp_flg, $all_duty_disp_flg, $all_legal_over_disp_flg, $over_yet_apply_disp_min,
        $workflow_menu_flg, $shift_menu_flg, $shift_refer_menu_flg, $shift_list_menu_flg, $shift_duty_menu_flg, $year_paid_hol_menu_flg,
        $menu_view1, $menu_view2, $menu_view3, $menu_view4, $menu_view5, $menu_view6, $menu_view_default, $timecard_title2, $over_disp_sixty_flg,
        $over_disp_forty_flg, $over_disp_forty_week, $over_forty_persons_full, $over_forty_persons_short, $over_forty_persons_part, $modify_plan_flg
    );
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}
else {
    $sql = "update timecard set";
    $set = array(
        "others1", "others2", "others3", "others4", "others5",
        "irrg_time_w", "irrg_time_wd", "irrg_time_m",
        "rest1", "rest2", "rest3", "rest4", "rest5", "rest6", "rest7", "modify_flg", "ovtmscr_tm", "show_time_flg", "return_flg",
        "over_time_apply_type", "time_move_flag", "type_display_flag", "group_display_flag", "zen_display_flag", "yoku_display_flag", "ret_display_flag", "time_big_font_flag", "delay_overtime_flg", "overtime_40_flg", "early_leave_time_flg", "timecard_title", "timecard_appr_disp_flg",
        "class_disp_flg", "atrb_disp_flg", "dept_disp_flg", "room_disp_flg", "over_apply_disp_flg", "return_icon_flg", "over_disp_split_flg",
        "all_apply_disp_flg", "all_duty_disp_flg", "all_legal_over_disp_flg", "over_yet_apply_disp_min",
        "workflow_menu_flg", "shift_menu_flg", "shift_refer_menu_flg", "shift_list_menu_flg", "shift_duty_menu_flg", "year_paid_hol_menu_flg",
        "menu_view1", "menu_view2", "menu_view3", "menu_view4", "menu_view5", "menu_view6", "menu_view_default", "timecard_title2", "over_disp_sixty_flg",
        "over_disp_forty_flg", "over_disp_forty_week", "over_forty_persons_full", "over_forty_persons_short", "over_forty_persons_part", "modify_plan_flg"
    );
    $setvalue = array(
        $others1, $others2, $others3, $others4, $others5,
        $irrg_time_w, $irrg_time_wd, $irrg_time_m,
        $rest1, $rest2, $rest3, $rest4, $rest5, $rest6, $rest7, $modify_flg, $ovtmscr_tm, $show_time_flg, $return_flg,
        $over_time_apply_type, $time_move_flag, $type_display_flag, $group_display_flag, $zen_display_flag, $yoku_display_flag, $ret_display_flag, $time_big_font_flag, $delay_overtime_flg, $overtime_40_flg, $early_leave_time_flg, pg_escape_string($timecard_title), $timecard_appr_disp_flg,
        $org_disp_flg_0, $org_disp_flg_1, $org_disp_flg_2, $org_disp_flg_3, $over_apply_disp_flg, $return_icon_flg, $over_disp_split_flg,
        $all_apply_disp_flg, $all_duty_disp_flg, $all_legal_over_disp_flg, $over_yet_apply_disp_min,
        $workflow_menu_flg, $shift_menu_flg, $shift_refer_menu_flg, $shift_list_menu_flg, $shift_duty_menu_flg, $year_paid_hol_menu_flg,
        $menu_view1, $menu_view2, $menu_view3, $menu_view4, $menu_view5, $menu_view6, $menu_view_default, $timecard_title2, $over_disp_sixty_flg,
        $over_disp_forty_flg, $over_disp_forty_week, $over_forty_persons_full, $over_forty_persons_short, $over_forty_persons_part, $modify_plan_flg
    );
    $cond = "";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

//決裁欄役職
///-----------------------------------------------------------------------------
// レコードを全削除
///-----------------------------------------------------------------------------
$sql = "delete from timecard_st_name";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
// 登録
///-----------------------------------------------------------------------------
$wk_st_name_cnt = $st_name_cnt + 3;
$wk_st_name[0] = $st_name1;
$wk_st_name[1] = $st_name2;
$wk_st_name[2] = $st_name3;
for ($i = 0; $i < $st_name_cnt; $i++) {
    $varname = "st_name" . ($i + 4);
    $wk_st_name[$i + 3] = $$varname;
}

for ($i = 0; $i < $wk_st_name_cnt; $i++) {
    //ＳＱＬ
    $sql = "insert into timecard_st_name (no, st_name) values (";
    $content = array($i + 1, pg_escape_string($wk_st_name[$i]));
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

//集計項目出力設定
// レコードを全削除
$sql = "delete from timecard_total_config";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
// 集計項目出力設定登録
$arr_total_flg = get_timecard_total_flg($con, $fname);
for ($i = 0; $i < count($arr_total_flg); $i++) {
    $varname = "total_$i";
    if ($$varname != "") {
        $sql = "insert into timecard_total_config (id) values (";
        $content = array($$varname);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

//時間有休設定
$sql = "update timecard_paid_hol_hour_conf set";
$set = array("paid_hol_hour_flag", "paid_hol_hour_full_flag", "paid_hol_hour_part_flag", "paid_hol_hour_max_flag", "paid_hol_hour_max_day", "paid_hol_hour_unit_time", "paid_hol_hour_carry_flg");
$setvalue = array($paid_hol_hour_flag, $paid_hol_hour_full_flag, $paid_hol_hour_part_flag, $paid_hol_hour_max_flag, $paid_hol_hour_max_day, $paid_hol_hour_unit_time, $paid_hol_hour_carry_flg);
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}


// トランザクションをコミット
pg_query("commit");
// オプション追加
if ($_POST['ovtm_tab_flg'] !== 't') {
    $_POST['ovtm_tab_flg'] = 'f';
}
if ($_POST['keyboard_input_flg'] !== 't') {
    $_POST['keyboard_input_flg'] = 'f';
}
if ($_POST['ovtm1_no_default_flg'] !== 't') {
    $_POST['ovtm1_no_default_flg'] = 'f';
}
if ($_POST['ovtm_rest_disp_flg'] !== 't') {
    $_POST['ovtm_rest_disp_flg'] = 'f';
}
if ($_POST['rest_check_flg'] !== 't') {
    $_POST['rest_check_flg'] = 'f';
}
if ($_POST['ovtm_total_disp_flg'] !== 't') {
    $_POST['ovtm_total_disp_flg'] = 'f';
}
if ($_POST['ovtm_limit_check_flg'] !== 't') {
    $_POST['ovtm_limit_check_flg'] = 'f';
}
if ($_POST['import_5time_flg'] !== 't') {
    $_POST['import_5time_flg'] = 'f';
}
if ($_POST['no_nextday_hol_ovtm_flg'] !== 't') {
    $_POST['no_nextday_hol_ovtm_flg'] = 'f';
}
if ($_POST['rest_disp_flg'] !== 't') {
    $_POST['rest_disp_flg'] = 'f';
}
if ($_POST['sousai_sinai_flg'] !== 't') {
    $_POST['sousai_sinai_flg'] = 'f';
}
if ($_POST['delay_ovtm_inout_flg'] !== 't') {
    $_POST['delay_ovtm_inout_flg'] = 'f';
}
if ($_POST['ovtm_reason2_input_flg'] !== 't') {
    $_POST['ovtm_reason2_input_flg'] = 'f';
}
if ($_POST['duty_input_nodisp_flg'] !== 't') {
    $_POST['duty_input_nodisp_flg'] = 'f';
}
if ($_POST['allowance_input_nodisp_flg'] !== 't') {
    $_POST['allowance_input_nodisp_flg'] = 'f';
}
if ($_POST['rest_input_nodisp_flg'] !== 't') {
    $_POST['rest_input_nodisp_flg'] = 'f';
}
if ($_POST['out_input_nodisp_flg'] !== 't') {
    $_POST['out_input_nodisp_flg'] = 'f';
}
if ($_POST['hol_hour_input_nodisp_flg'] !== 't') {
    $_POST['hol_hour_input_nodisp_flg'] = 'f';
}
if ($_POST['meeting_input_nodisp_flg'] !== 't') {
    $_POST['meeting_input_nodisp_flg'] = 'f';
}
if ($_POST['list_allowance_disp_flg'] !== 't') {
    $_POST['list_allowance_disp_flg'] = 'f';
}
if ($_POST['list_meeting_disp_flg'] !== 't') {
    $_POST['list_meeting_disp_flg'] = 'f';
}
if ($_POST['list_absence_disp_flg'] !== 't') {
    $_POST['list_absence_disp_flg'] = 'f';
}
$conf = new Cmx_SystemConfig();
$conf->set('timecard.ovtm_tab_flg', $_POST['ovtm_tab_flg']);
$conf->set('timecard.keyboard_input_flg', $_POST['keyboard_input_flg']);
$conf->set('timecard.ovtm1_no_default_flg', $_POST['ovtm1_no_default_flg']);
$conf->set('timecard.ovtm_rest_disp_flg', $_POST['ovtm_rest_disp_flg']);
$conf->set('timecard.rest_check_flg', $_POST['rest_check_flg']);
$conf->set('timecard.ovtm_total_disp_flg', $_POST['ovtm_total_disp_flg']);
$conf->set('timecard.ovtm_limit_check_flg', $_POST['ovtm_limit_check_flg']);
$conf->set('timecard.ovtm_limit_month', $ovtm_limit_month);
//$conf->set('timecard.ovtm_limit_year', $ovtm_limit_year);
$conf->set('timecard.import_5time_flg', $_POST['import_5time_flg']);
$conf->set('timecard.import_base_time', $import_base_time);
$conf->set('timecard.no_nextday_hol_ovtm_flg', $_POST['no_nextday_hol_ovtm_flg']);
$conf->set('timecard.rest_disp_flg', $_POST['rest_disp_flg']);
$conf->set('timecard.sousai_sinai_flg', $_POST['sousai_sinai_flg']);
//遅刻早退があった日に残業した場合の法定内残業を法定外残業に集計する t:する "",f:しない 20141024
$conf->set('timecard.delay_overtime_inout_flg', $_POST['delay_overtime_inout_flg']);
//時間帯画面の設定追加 20141111
$conf->set('timecard.officehours_min_unit', $_POST['officehours_min_unit']);
//残業時刻2以降にも理由を入力する設定追加 20141226
$conf->set('timecard.ovtm_reason2_input_flg', $_POST['ovtm_reason2_input_flg']);
//ユーザ側の入力画面で表示しない項目 20150109
$conf->set('timecard.duty_input_nodisp_flg', $_POST['duty_input_nodisp_flg']);
$conf->set('timecard.allowance_input_nodisp_flg', $_POST['allowance_input_nodisp_flg']);
$conf->set('timecard.rest_input_nodisp_flg', $_POST['rest_input_nodisp_flg']);
$conf->set('timecard.out_input_nodisp_flg', $_POST['out_input_nodisp_flg']);
$conf->set('timecard.hol_hour_input_nodisp_flg', $_POST['hol_hour_input_nodisp_flg']);
$conf->set('timecard.meeting_input_nodisp_flg', $_POST['meeting_input_nodisp_flg']);
//手当、会議研修、欠勤の表示 20150113
$conf->set('timecard.list_allowance_disp_flg', $_POST['list_allowance_disp_flg']);
$conf->set('timecard.list_meeting_disp_flg', $_POST['list_meeting_disp_flg']);
$conf->set('timecard.list_absence_disp_flg', $_POST['list_absence_disp_flg']);
$conf->set('timecard.over_disp_fortyfive_flg', $_POST['over_disp_fortyfive_flg']);
//当直・待機
$conf->set('timecard.duty_or_oncall_flg', $_POST['duty_or_oncall_flg']);

// データベース接続を閉じる
pg_close($con);

// 全画面をリフレッシュ 20130129
$f_url = urlencode("timecard_others.php?session=$session");
echo("<script type=\"text/javascript\">parent.location.href = 'main_menu.php?session=$session&f_url=$f_url';</script>");
