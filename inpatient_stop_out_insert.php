<?
require("about_session.php");
require("about_authority.php");
require("inpatient_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (!checkdate($from_mon, $from_day, $from_yr) || !checkdate($to_mon, $to_day, $to_yr)) {
	echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
$from_date = "$from_yr$from_mon$from_day";
$to_date = "$to_yr$to_mon$to_day";
if ($from_date >= $to_date) {
	echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($place) > 30) {
	echo("<script type=\"text/javascript\">alert('外出先が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($comment) > 200) {
	echo("<script type=\"text/javascript\">alert('コメントが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// From日付〜To日付の入院状況レコードを更新
$tmp_date = $from_date;
while ($tmp_date <= $to_date) {
	update_inpatient_condition($con, $pt_id, $tmp_date, null, "5", $session, $fname);
	$tmp_date = add_days($tmp_date, 1);
}

// 外泊情報を登録
$sql = "insert into inptgoout (ptif_id, out_type, out_date, ret_date, stop_out_fd1, stop_out_fd2, place, comment) values (";
$content = array($pt_id, "2", $from_date, $to_date, $stop_out_fd1, $stop_out_fd2, $place, $comment);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリロードする
echo "<script type=\"text/javascript\">window.opener.location.reload();</script>";

// 画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'inpatient_stop_out_register.php?session=$session&pt_id=$pt_id';</script>\n");
?>
