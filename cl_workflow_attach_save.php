<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("./about_session.php");
require("./about_authority.php");
require_once("cl_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"./js/showpage.js\"></script>");
	echo("<script language=\"javascript\">showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$checkauth = check_authority($session, $CL_MANAGE_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"./js/showpage.js\"></script>");
	echo("<script language=\"javascript\">showLoginPage(window);</script>");
	exit;
}

// ファイルアップロードチェック
define(UPLOAD_ERR_OK, 0);
define(UPLOAD_ERR_INI_SIZE, 1);
define(UPLOAD_ERR_FORM_SIZE, 2);
define(UPLOAD_ERR_PARTIAL, 3);
define(UPLOAD_ERR_NO_FILE, 4);

$filename = $_FILES["file"]["name"];
if ($filename == "") {
	echo("<script language=\"javascript\">alert('ファイルを選択してください。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

switch ($_FILES["file"]["error"]) {
case UPLOAD_ERR_INI_SIZE:
case UPLOAD_ERR_FORM_SIZE:
	echo("<script language=\"javascript\">alert('ファイルサイズが大きすぎます。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
case UPLOAD_ERR_PARTIAL:
case UPLOAD_ERR_NO_FILE:
	echo("<script language=\"javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

// ファイル保存用ディレクトリがなければ作成
if (!is_dir("cl/template")) {
	mkdir("cl/template", 0755);
}
if (!is_dir("cl/template/tmp")) {
	mkdir("cl/template/tmp", 0755);
}

// 30分以上前に保存されたファイルを削除
foreach (glob("cl/template/tmp/*.*") as $tmpfile) {
	if (time() - filemtime($tmpfile) >= 30 * 60) {
		unlink($tmpfile);
	}
}

// 添付ファイルIDの最大値を取得
$max_id = 0;
foreach (glob("cl/template/tmp/{$session}_*.*") as $tmpfile) {
	preg_match("/{$session}_(\d*)\./", $tmpfile, $matches);
	$tmp_id = $matches[1];
	if ($tmp_id > $max_id) {
		$max_id = $tmp_id;
	}
}
$file_id = $max_id + 1;

// アップロードされたファイルを保存
$ext = strrchr($filename, ".");
copy($_FILES["file"]["tmp_name"], "cl/template/tmp/{$session}_{$file_id}{$ext}");
?>
<script type="text/javascript">
var fname = '<? echo($filename); ?>';

var a = opener.document.createElement('a');
a.href = 'cl/template/tmp/<? echo("{$session}_{$file_id}{$ext}"); ?>';
a.target = '_blank';
a.appendChild(opener.document.createTextNode(fname));

var inputA = opener.document.createElement('input');
inputA.type = 'button';
inputA.name = 'btn_<? echo($file_id); ?>';
inputA.value = '削除';
inputA.id = 'btn_<? echo($file_id); ?>';
if (inputA.onclick === null) {  // IE, Safari
	inputA.setAttribute('onclick', 'detachFile();');
}
if (typeof inputA.onclick == 'string') {  // IE
	inputA.setAttribute('onclick', null);
}
if (inputA.onclick == null) {  // IE, FireFox
	inputA.onclick = opener.detachFile;
}

var inputB = opener.document.createElement('input');
inputB.type = 'hidden';
inputB.name = 'filename[]';
inputB.value = fname;

var inputC = opener.document.createElement('input');
inputC.type = 'hidden';
inputC.name = 'file_id[]';
inputC.value = '<? echo($file_id); ?>';

var p = opener.document.createElement('p');
p.id = 'p_<? echo($file_id); ?>';
p.appendChild(a);
p.appendChild(opener.document.createTextNode(' '));
p.appendChild(inputA);
p.appendChild(inputB);
p.appendChild(inputC);

var div = opener.document.getElementById('attach');
div.appendChild(p);

self.close();
</script>
