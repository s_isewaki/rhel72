
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
if ($atdbk_duty_shift_flg == "1") {
	$title = "出勤表";
}else {
	$title = "勤務シフト作成";
}
if ($plan_hope_flg == "2" || $atdbk_duty_shift_flg == "1") {
	$sub_title = "勤務希望登録";
}else {
	$sub_title = "勤務パターン登録";
}
?>
<title>CoMedix <?=$title?> | <?=$sub_title?></title>
<?
//ini_set("display_errors","1");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("duty_shift_common_class.php");
require_once("date_utils.php");
require_once("get_values.ini");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
//職員名
$emp_name = get_emp_kanji_name($con,$emp_id,$fname);

//期間変更対応
$start_day = substr($start_date, 6, 2);
//if ($start_day == "01") {
	//曜日
//	$wk_date = $duty_yyyy.sprintf("%02d%02d", $duty_mm, $day);
//	$weekday = date_utils::get_weekday_name($wk_date);
//	$wk_duty_yyyy = $duty_yyyy;
//	$wk_duty_mm = $duty_mm;
//	$wk_day = $day;
//} else {
	//開始日と位置$dayから日付を取得
	$wk_timestamp = date_utils::to_timestamp_from_ymd($start_date);
	$wk_day_idx = $day - 1;
	$wk_date = date("Ymd", strtotime($wk_day_idx." day", $wk_timestamp));
	//曜日
	$weekday = date_utils::get_weekday_name($wk_date);
	$wk_duty_yyyy = substr($wk_date, 0, 4);
	$wk_duty_mm = substr($wk_date, 4, 2);
	$wk_day = substr($wk_date, 6, 2);
//echo("start_date=$start_date wk_day_idx=$wk_day_idx"); //debug
//echo("wk_date=$wk_date");//debug
//	$wk_prev_date = date("Ymd", strtotime("-1 day", $wk_timestamp));
//}

//前月末日
//$this_first_date = $duty_yyyy.sprintf("%02d",$duty_mm)."010000";
// 期間変更対応のため、開始日前日となる
$this_first_date = $start_date."0000";
$last_month_date = date_utils::add_day_ymdhi($this_first_date, -1);
$last_month_date = substr($last_month_date, 0, 8);
$wk_prev_mmdd = substr($last_month_date, 4, 2) ."/". substr($last_month_date, 6, 2);

//今月末日
$this_month_last_day = days_in_month($duty_yyyy, $duty_mm);

// 前月末データを取得
if ($plan_hope_flg == "2" || $atdbk_duty_shift_flg == "1") {
// 希望の場合
	$sql = "select pattern_id, atdptn_ptn_id, reason from duty_shift_plan_individual";
	$cond = "where emp_id = '$emp_id' ";
	$cond .= "and duty_date = '$last_month_date' ";
} else {
// 予定の場合
	$sql = "select tmcd_group_id, pattern, reason from atdbk";
	$cond = "where emp_id = '$emp_id' ";
	$cond .= "and date = '$last_month_date' ";
}
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	if ($plan_hope_flg == "2" || $atdbk_duty_shift_flg == "1") {
	// 希望の場合
		$last_pattern_id = pg_fetch_result($sel, 0, "pattern_id");
		$pattern = pg_fetch_result($sel, 0, "atdptn_ptn_id");
	} else {
	// 予定の場合
		$last_pattern_id = pg_fetch_result($sel, 0, "tmcd_group_id");
		$pattern = pg_fetch_result($sel, 0, "pattern");
	}
	$last_reason_2 = pg_fetch_result($sel, 0, "reason");

	$wk_reason = ($pattern == "10") ? $last_reason_2 : "00";
	$last_atdptn_ptn_id = sprintf("%02d%02d", $pattern, $wk_reason);
} else {
	$last_pattern_id = "";
	$last_atdptn_ptn_id = "0000";
	$last_reason_2 = "";
}
// 前回の情報を取得
//デフォルト設定、前回指定情報とする
//if ($atdptn_ptn_id == "0000") {
	$login_emp_id = get_emp_id($con,$session,$fname);
	$arr_last_ptn = $obj->get_last_ptn($login_emp_id);
	$atdptn_ptn_id = $arr_last_ptn["last_ptn_id"];
	$reason_2 = $arr_last_ptn["last_reason_2"];
$copy_cnt = $arr_last_ptn["last_copy_cnt"];
//}

//シフトグループ情報取得
$group_array = $obj->get_duty_shift_group_array($group_id, array(), array());
//事由設定フラグ
$reason_setting_flg = $group_array[0]["reason_setting_flg"];
?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
	///-----------------------------------------------------------------------------
	// 「更新」
	///-----------------------------------------------------------------------------
	function updateBtn() {

		//親画面の更新用関数呼び出し
		if(parent.opener && !parent.opener.closed && parent.opener.callbackPtnUpdate){
			atdptn_ptn_id = document.mainform.atdptn_ptn_id.value;
			reason_2 = document.mainform.reason_2.value;
			copy_cnt = document.mainform.copy_cnt.value;
			parent.opener.callbackPtnUpdate(<?=$plan_hope_flg?>,<?=$emp_idx?>,<?=$day?>,atdptn_ptn_id,reason_2, copy_cnt);
		}

		window.close();
	}
	///-----------------------------------------------------------------------------
	// 「コピー」
	///-----------------------------------------------------------------------------
	function copyBtn() {
		
		if(parent.opener && !parent.opener.closed && parent.opener.callbackPtnCopy){
			var ptn_info = new Object();
			seq_flg = (document.mainform.seq_flg.checked) ? "1" : "0" ;
			copy_day = document.mainform.copy_day.value;
			// 前月末
			if (copy_day == 0) {
				atdptn_ptn_id = '<?=$last_atdptn_ptn_id?>';
				pattern_id =  '<?=$last_pattern_id?>';
			} else {
				<? if ($plan_hope_flg == "0") { ?>
				src = 'atdptn_ptn_id_<?=$emp_idx?>_'+copy_day;
				src1 = 'pattern_id_<?=$emp_idx?>_'+copy_day;
				<? } else { ?>
				src = 'rslt_id_<?=$emp_idx?>_'+copy_day;
				src1 = 'rslt_pattern_id_<?=$emp_idx?>_'+copy_day;
				<? } ?>
				atdptn_ptn_id = parent.opener.document.mainform.elements[src].value;
				pattern_id = parent.opener.document.mainform.elements[src1].value;
			}
			if (atdptn_ptn_id == "0000") {
				alert('未設定の予定はコピーできません。');
				return;
			}
			if (pattern_id != parent.opener.document.mainform.pattern_id.value) {
				alert('他シフトグループの予定はコピーできません。');
				return;
			}
			
			ptn_info.pattern_id = '<?=$last_pattern_id?>';
			ptn_info.atdptn_ptn_id = '<?=$last_atdptn_ptn_id?>';
			ptn_info.reason_2 = '<?=$last_reason_2?>';
			
			parent.opener.callbackPtnCopy(<?=$plan_hope_flg?>,<?=$emp_idx?>,<?=$day?>,seq_flg,copy_day, ptn_info);
		}
		
		window.close();
		return;
	}
	
	//コピー元日付設定
	function set_default_day() {
		var idx = 0;
		if(parent.opener && !parent.opener.closed){
			var day = <?=$day?> - 1;
			for (i=day; i>0; i--) {
				<? // 予定か希望の項目名を設定
					//設定がある日付を探してコピー元日付に設定
				 if ($plan_hope_flg == "0") { ?>
				src = 'atdptn_ptn_id_<?=$emp_idx?>_'+i;
				src1 = 'pattern_id_<?=$emp_idx?>_'+i;
				<? } else { ?>
				src = 'rslt_id_<?=$emp_idx?>_'+i;
				src1 = 'rslt_pattern_id_<?=$emp_idx?>_'+i;
				<? } ?>
				atdptn_ptn_id = parent.opener.document.mainform.elements[src].value;
				pattern_id = parent.opener.document.mainform.elements[src1].value;

				if (pattern_id == parent.opener.document.mainform.pattern_id.value && atdptn_ptn_id != '0000') {
					idx = i;
					break;
				}
			}
		}
		document.mainform.copy_day.value = idx;
	}

	var arr_atdptn_reason = new Array();
<?
//事由情報を出力
$arr_group_pattern_reason = $obj->get_pattern_reason_array();
foreach ($arr_group_pattern_reason as $wk_tmcd_group_id => $arr_pattern_reason) {
	foreach ($arr_pattern_reason as $wk_atdptn_id => $reason) {
		if ($reason != "") {
            //勤務シフト希望で事由設定欄を表示しない場合、公休設定を除く 20130520
            if ($atdbk_duty_shift_flg != "1" || $reason_setting_flg == "t" ||
                    ($atdbk_duty_shift_flg == "1" && $reason_setting_flg == "f" && $wk_atdptn_id != "10")) {  
                echo("arr_atdptn_reason['{$wk_tmcd_group_id}_{$wk_atdptn_id}'] = '$reason';\n");
            }
		}
	}
}
?>
function setReason(tmcd_group_id, atdptn_id) {

	//連続を1に設定
	document.getElementById("copy_cnt").value = "1";
	if (atdptn_id == '0000') {
		document.mainform.reason_2.value = '';
		return;
	}
	wk_atdptn_id = parseInt(atdptn_id.substring(0, 2),10);
	wk_id = tmcd_group_id+'_'+wk_atdptn_id;
	if (arr_atdptn_reason[wk_id] == undefined) {
		document.mainform.reason_2.value = '';
		return;
	}
	document.mainform.reason_2.value = arr_atdptn_reason[wk_id];
}

</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<!-- <body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initcal();"> -->
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" 
<?
// 呼出元が実績の場合はコピー処理を表示しない 20091005
//if ($where_from != "2") {
if ($dummy == "1") { //表示しない 20110729
?>
onload="set_default_day();"
<? } ?>
>
<center>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
		<tr height="32" bgcolor="#5279a5">
		<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$sub_title?></b></font></td>
		<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<form name="mainform" method="post">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr height="22">
		<td width="20%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><?=$emp_name?><br><?=$wk_duty_yyyy?>年<?=$wk_duty_mm?>月<?=$wk_day?>日（<?=$weekday?>）</font></td>
		</tr>
	<tr height="22">
		<td width="80%" align="left"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
		<tr height="22">
		<td width="" align="center" bgcolor="#f6f9ff"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><b>勤務シフト</b></font></td>
		<td width="" align="center">
<?

	$data_atdptn = $obj->get_atdptn_array($pattern_id);
	$pattern_array = $obj->get_duty_shift_pattern_array($pattern_id, $data_atdptn);
	$reason_2_array = $obj->get_reason_2("");
	$reason_2_all_array = $obj->get_reason_2("1");

	echo("　<select id=\"atdptn_ptn_id\" name=\"atdptn_ptn_id\" onChange=\"setReason($pattern_id, this.value);\">");
	$selected = ($atdptn_ptn_id == "0000") ? " selected" : "";
	echo("<option value=\"0000\" $selected>未設定</option>");
	for ($i=0; $i<count($pattern_array); $i++) {
		// 出勤表の休暇種別等画面の非表示を確認
		if($pattern_array[$i]["reason_display_flag"] == "f") {
			continue;
		}
        // 出勤表のパターン名＞出勤パターン名画面の非表示を確認 20120830
        if($pattern_array[$i]["display_flag"] == "f") {
            continue;
        }
    //ＩＤ
		$wk_id = sprintf("%02d%02d", $pattern_array[$i]["atdptn_ptn_id"], $pattern_array[$i]["reason"]);
		$wk_name = $pattern_array[$i]["atdptn_ptn_name"];
		// 半有半公等
		if ($pattern_array[$i]["reason"] >= "44" &&
			$pattern_array[$i]["reason"] <= "47") {
			$wk_name = $pattern_array[$i]["reason_name"];
			for ($r_idx=0; $r_idx<count($reason_2_all_array); $r_idx++) {
				if ($reason_2_all_array[$r_idx]["id"] == $pattern_array[$i]["reason"]) {
					$wk_name = $reason_2_all_array[$r_idx]["name"];
					break;
				}
			}
		} else
		if ($pattern_array[$i]["reason_name"] != "") {
			$wk_name .= "(" . $pattern_array[$i]["reason_name"] . ")";
		}

		$selected = ($atdptn_ptn_id == $wk_id) ? " selected" : "";
		echo("<option value=\"$wk_id\" $selected>$wk_name</option>\n");
	}
	echo("</select>");
?>
		</font>
		</td>
	</tr>
<?
//事由設定欄の表示
if ($reason_setting_flg == "t") {
?>
	<tr height="22">
		<td width="" align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>事由</b></font></td>
		<td width="" align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
<?
	echo("　<select id=\"reason_2\" name=\"reason_2\">");
	$selected = ($reason_2 == "") ? " selected" : "";
	echo("<option value=\"\" $selected>未設定</option>");
	for ($i=0; $i<count($reason_2_array); $i++) {
		$wk_id = $reason_2_array[$i]["id"];
		$wk_name = $reason_2_array[$i]["name"];
		$selected = ((int)$reason_2 == $wk_id) ? " selected" : "";
		echo("<option value=\"$wk_id\" $selected>$wk_name</option>\n");
	}
	echo("</select>");
?>
		</font>
		</td>
	</tr>
<?
} else {
	echo("<input type=\"hidden\" name=\"reason_2\" value=\"\">");
}
?>
	<tr height="22">
		<td width="" align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>連続</b></font></td>
		<td width="" align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		
	　<select id="copy_cnt" name="copy_cnt">
<?
// プルダウン
$cnt = 32;
for ($i=1; $i<$cnt; $i++) {
	$selected = ($i == $copy_cnt) ? " selected" : "";
	echo("<option value=\"$i\" $selected>$i</option>\n");
}
		?>
		</select>
		</td>
	</tr>
	</table>
		</td>
	</tr>
	<tr>
		<td width="100%" align="right">
			<input type="button" id="upd" name="upd" value="　　更新　　" onclick="updateBtn();">
		</td>
		</tr>
<?
// 呼出元が実績の場合はコピー処理を表示しない 20091005
//if ($where_from != "2") {
if ($dummy == "1") { //表示しない 20110729
?>
	<tr height="22">
		<td width="20%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>＜コピー処理＞</b></font></td>
		</tr>
	<tr height="22">
		<td width="20%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">　<input type="checkbox" id="seq_flg" name="seq_flg" checked>連続してコピー</font><br>
		　<select id="copy_day" name="copy_day">
	<?
	echo("<option value=\"0\">$wk_prev_mmdd</option>\n");
	// 日付プルダウン
	$cnt = 32;
	for ($i=1; $i<=$cnt; $i++) {
		if ($i == $day) {
			continue;
		}
		// selectedの設定はJavaScriptで行う
		$wk_day_idx = $i - 1;
		$wk_date = date("Ymd", strtotime($wk_day_idx." day", $wk_timestamp));
		if ($wk_date > $end_date) {
			break;
		}
		$wk_day_str = substr($wk_date, 4, 2) ."/". substr($wk_date, 6, 2);
		echo("<option value=\"$i\">$wk_day_str</option>\n");
	}
		?>
		</select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日の勤務をコピー</font>
		</td>
		</tr>
<? } ?>
	</table>

<?
// 呼出元が実績の場合はコピー処理を表示しない 20091005
//if ($where_from != "2") {
if ($dummy == "1") { //表示しない 20110729
?>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 「コピー」ボタン -->
		<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="100%" align="right">
			<input type="button" value="　　コピー　" onclick="copyBtn();">
		</td>
		</tr>
	</table>
<? } ?>
	<input type="hidden" name="session" value="<?=$session?>">
	<input type="hidden" name="emp_idx" value="<?=$emp_idx?>">
	<input type="hidden" name="day" value="<?=$day?>">
	</form>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
function days_in_month($year, $month) {
	for ($day = 31; $day >= 28; $day--) {
		if (checkdate($month, $day, $year)) {
			return $day;
		}
	}
	return false;
}
?>
