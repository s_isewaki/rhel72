<?php
chdir(dirname(__FILE__));
require_once("Cmx.php");
require_once("l4pWClass.php");

if (count($argv) < 4) {
	die("Usage: {$argv[0]} <level> <function_id> <message>\n");
}

$level = $argv[1];
$function_id = $argv[2];
$message = $argv[3];

$valid_levels = array("info", "debug", "error", "warn", "fatal");
if (!in_array($level, $valid_levels)) {
	die("level \"$level\" is not defined.\n");
}

$lfp = new l4pWClass($function_id);
switch ($level) {
case "info":
	$lfp->infoRst($function_id, $message);
	break;
case "debug":
	$lfp->debugRst($function_id, $message);
	break;
case "error":
	$lfp->errorRst($function_id, $message);
	break;
case "warn":
	$lfp->warnRst($function_id, $message);
	break;
case "fatal":
	$lfp->fatalRst($function_id, $message);
	break;
}
