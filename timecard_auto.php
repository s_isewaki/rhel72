<?
//====================================================================
// 勤怠管理
// 自動打刻 php
//====================================================================
echo("Time Card Auto [START]\n");

// 引数の取得
$proc_flg = $argv[1];	// 1:朝 2:夕
//$proc_flg = 1; //debug

ob_start();
require_once('about_postgres.php');
require_once('timecard_common_class.php');
ob_end_clean();

//プログラム名
$fname = $argv[0];
//ＤＢのコネクション作成
$con = connect2db($fname);

//当日
$today = date("Ymd");
//前日
$yesterday = date("Ymd", strtotime("-1 day")); 

//職員ID
$emp_id = null;

// タイムカード共通クラス
$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $yesterday, $today);


// 処理日付の勤務日種別を取得
$arr_type = array();
$sql = "select date, type from calendar";
$cond = "where date = '$today'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$num = pg_num_rows($sel);
if ($num > 0) {
	$type = pg_fetch_result($sel, 0, "type");
}

//type:"2勤務日1,3勤務日2"以外は"1通常"とする。"4法定休日,5所定休日,6祝日,7年末年始休暇"
$tmp_type = ($type != "2" && $type != "3") ? "1" : $type;

//朝バッチ
if ($proc_flg == "1") {
	sub_proc1($con, $fname, $session, $timecard_common_class, $tmp_type);
}
//夕バッチ
else {
	sub_proc2($con, $fname, $session, $timecard_common_class, $tmp_type);
}

//ＤＢのコネクションクローズ
pg_close($con);

echo("Time Card Auto [END]\n");

// 朝バッチ処理関数

/**
 * 朝バッチ処理関数
 * 　予定が休暇の場合に、実績データへ勤務パターンと事由をコピー
 * 　予定が自動打刻設定の勤務パターンの場合、勤務パターン、出勤時刻等をコピー
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $session セッション
 * @param mixed $timecard_common_class タイムカード共通クラス
 * @param mixed $type カレンダーの日付属性
 * @return なし
 *
 */
function sub_proc1($con, $fname, $session, $timecard_common_class, $type) {
	
	$today = date("Ymd");
    // トランザクションを開始
	pg_query($con, "begin");
	
	//予定が休暇の職員の勤務パターンと事由を実績へ設定
	$sql = <<<_SQL_END_
select a.emp_id, a.pattern, a.reason, a.tmcd_group_id, b.date as rslt_date from atdbk a 
left join atdbkrslt b on b.emp_id = a.emp_id and b.date = a.date
_SQL_END_;
	//予定勤務パターンが休日10、実績勤務パターンが未設定か実績レコードなし
	$cond = "where a.date = '$today' and a.pattern = '10' and (b.pattern is null or b.pattern = '')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo "  db select error\n";
		exit;
	}
	$num = pg_numrows($sel);
	
	for ($i=0; $i<$num; $i++) {
		
		$emp_id = pg_fetch_result($sel, $i, "emp_id");
		$pattern = pg_fetch_result($sel, $i, "pattern");
		$reason = pg_fetch_result($sel, $i, "reason");
		$tmcd_group_id = pg_fetch_result($sel, $i, "tmcd_group_id");
		$rslt_date = pg_fetch_result($sel, $i, "rslt_date"); //実績レコードの日付、存在チェック用
		
		//実績レコードなしの場合、登録
		if ($rslt_date == "") {
			$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, tmcd_group_id ";
			$sql .= ") values (";
			
			$content = array($emp_id, $today, $pattern, $reason, $tmcd_group_id);
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo "  db insert error\n";
				exit;
			}
			
		}
		//更新
		else {
			$sql = "update atdbkrslt set";
			$set = array("pattern", "reason", "tmcd_group_id");
			$setvalue = array($pattern, $reason, $tmcd_group_id);
			$cond = "WHERE emp_id = '$emp_id' and date = '$today'";
			
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo "  db update error\n";
				exit;
			}
		}
		
	}
	
	// トランザクションをコミット
	pg_query($con, "commit");
	
    // トランザクションを開始
	pg_query($con, "begin");
	
	// 朝バッチ、自動打刻
	
	//予定a, 実績b, 出勤パターンc
	$sql = <<<_SQL_END_
select a.emp_id, a.tmcd_group_id, a.pattern, a.reason, a.night_duty, a.allow_id,
a.prov_start_time, a.prov_end_time, b.date as rslt_date,
b.tmcd_group_id as rslt_group_id, b.pattern as rslt_pattern, b.reason as rslt_reason,
b.night_duty as rslt_night_duty, b.allow_id as rslt_allow_id, c.previous_day_flag as atdptn_previous_day_flag
, c.empcond_officehours_flag
from atdbk a 
left join atdbkrslt b on a.emp_id = b.emp_id and a.date = b.date
left join atdptn c on a.tmcd_group_id = c.group_id and a.pattern = CAST(c.atdptn_id AS varchar)
_SQL_END_;
	//予定の勤務パターン設定済
	//自動打刻フラグがある勤務パターン
	//実績の時刻未設定
	$cond = "where a.date = '$today' and a.pattern != '' and (c.auto_clock_in_flag = 1 or after_night_duty_flag = 1) and (b.start_time is null or b.start_time = '')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo "  db select error\n";
		exit;
	}
    $arr_weekday_flg = $timecard_common_class->get_arr_weekday_flag($today, $today); //20140318
    $num = pg_numrows($sel);
	
	for ($i=0; $i<$num; $i++) {
		$emp_id = pg_fetch_result($sel, $i, "emp_id");
		$tmcd_group_id = pg_fetch_result($sel, $i, "tmcd_group_id");
		$pattern = pg_fetch_result($sel, $i, "pattern");
		$reason = pg_fetch_result($sel, $i, "reason");
		$night_duty = pg_fetch_result($sel, $i, "night_duty");
		$allow_id = pg_fetch_result($sel, $i, "allow_id");
		$prov_start_time = pg_fetch_result($sel, $i, "prov_start_time");
		$prov_end_time = pg_fetch_result($sel, $i, "prov_end_time");
		$rslt_date = pg_fetch_result($sel, $i, "rslt_date"); //実績レコードの日付、存在チェック用
		$rslt_group_id = pg_fetch_result($sel, $i, "rslt_group_id");
		$rslt_pattern = pg_fetch_result($sel, $i, "rslt_pattern");
		$rslt_reason = pg_fetch_result($sel, $i, "rslt_reason");
		$rslt_night_duty = pg_fetch_result($sel, $i, "rslt_night_duty");
		$rslt_allow_id = pg_fetch_result($sel, $i, "rslt_allow_id");
		$previous_day_flag = pg_fetch_result($sel, $i, "atdptn_previous_day_flag");
        $empcond_officehours_flag = pg_fetch_result($sel, $i, "empcond_officehours_flag"); //20140318
        //所定時刻取得
		if ($prov_start_time != "") {
			$start2 = $prov_start_time;
		}
        else {
        //個人別所定時刻対応 20140318
            $start2 = "";
            if ($empcond_officehours_flag != "") {
                $arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($emp_id, $today);
                $wk_weekday = $arr_weekday_flg[$today];
                $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $today);
                if ($arr_empcond_officehours["officehours2_start"] != "") {
                    $start2 = $arr_empcond_officehours["officehours2_start"];
                }
            }
            //未設定時は時間帯画面の時刻を使用
            if ($start2 == "") {
                $start2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $type, "start2" );
            }
		}
		$wk_start2 = str_replace(":", "", $start2);
		//実績レコードなしの場合、登録
		if ($rslt_date == "") {
			$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, night_duty, allow_id, tmcd_group_id, start_time, start_btn_time, start_date, previous_day_flag ";
			$sql .= ") values (";
			
			$content = array($emp_id, $today, $pattern, $reason, $night_duty, $allow_id, $tmcd_group_id, $wk_start2, $wk_start2, $today, $previous_day_flag);
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo "  db insert error\n";
				exit;
			}
		}
		//更新
		else {
			$sql = "update atdbkrslt set";
			$set = array("start_time", "start_btn_time", "start_date", "previous_day_flag");
			$setvalue = array($wk_start2, $wk_start2, $today, $previous_day_flag);
			//実績項目の内容確認 
			if ($rslt_group_id == "" && $tmcd_group_id != "") {
				array_push($set, "tmcd_group_id");
				array_push($setvalue, $tmcd_group_id);
			}
			if ($rslt_pattern == "" && $pattern != "") {
				array_push($set, "pattern");
				array_push($setvalue, $pattern);
			}
			if ($rslt_reason == "" && $reason != "") {
				array_push($set, "reason");
				array_push($setvalue, $reason);
			}
			if ($rslt_night_duty == "" && $night_duty != "") {
				array_push($set, "night_duty");
				array_push($setvalue, $night_duty);
			}
			if ($rslt_allow_id == "" && $allow_id != "") {
				array_push($set, "allow_id");
				array_push($setvalue, $allow_id);
			}
			
			$cond = "WHERE emp_id = '$emp_id' and date = '$today'";
			
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo "  db update error\n";
				exit;
			}
		}
		
	}

	// トランザクションをコミット
	pg_query($con, "commit");
	

}

// 夕バッチ処理関数
/**
 * 夕バッチ処理関数
 * 　予定が自動打刻設定の勤務パターンの場合、退勤時刻を設定
 * 　前日の退勤時刻が未設定で、前日予定が自動打刻設定の勤務パターンの場合、退勤時刻を設定
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $session セッション
 * @param mixed $timecard_common_class タイムカード共通クラス
 * @param mixed $type カレンダーの日付属性
 * @return なし
 *
 */
function sub_proc2($con, $fname, $session, $timecard_common_class, $type) {
	
	$today = date("Ymd");

	// トランザクションを開始
	pg_query($con, "begin");
	
	// 夕バッチ、自動打刻
	
	//予定a, 実績b, 出勤パターンc
	$sql = <<<_SQL_END_
select a.emp_id, a.tmcd_group_id, a.pattern, a.reason, a.night_duty, a.allow_id,
a.prov_start_time, a.prov_end_time, b.date as rslt_date,
b.tmcd_group_id as rslt_group_id, b.pattern as rslt_pattern, b.reason as rslt_reason,
b.night_duty as rslt_night_duty, b.allow_id as rslt_allow_id, c.over_24hour_flag
, c.empcond_officehours_flag
, b.meeting_start_time as rslt_meeting_start_time, b.meeting_end_time as rslt_meeting_end_time
, c.meeting_start_time, c.meeting_end_time
from atdbk a 
left join atdbkrslt b on a.emp_id = b.emp_id and a.date = b.date
left join atdptn c on a.tmcd_group_id = c.group_id and a.pattern = CAST(c.atdptn_id AS varchar)
_SQL_END_;
	//予定の勤務パターン設定済
	//自動打刻フラグがある勤務パターン
	//実績の退勤時刻未設定、出勤時刻設定済
	$cond = "where a.date = '$today' and a.pattern != '' and c.auto_clock_out_flag = 1 and b.start_time != '' and (b.end_time is null or b.end_time = '')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo "  db select error\n";
		exit;
	}
    $arr_weekday_flg = $timecard_common_class->get_arr_weekday_flag($today, $today); //20140318
    $num = pg_numrows($sel);
	
	for ($i=0; $i<$num; $i++) {
		$emp_id = pg_fetch_result($sel, $i, "emp_id");
		$tmcd_group_id = pg_fetch_result($sel, $i, "tmcd_group_id");
		$pattern = pg_fetch_result($sel, $i, "pattern");
		$reason = pg_fetch_result($sel, $i, "reason");
		$night_duty = pg_fetch_result($sel, $i, "night_duty");
		$allow_id = pg_fetch_result($sel, $i, "allow_id");
		$prov_start_time = pg_fetch_result($sel, $i, "prov_start_time");
		$prov_end_time = pg_fetch_result($sel, $i, "prov_end_time");
		$rslt_date = pg_fetch_result($sel, $i, "rslt_date"); //実績レコードの日付、存在チェック用
		$rslt_group_id = pg_fetch_result($sel, $i, "rslt_group_id");
		$rslt_pattern = pg_fetch_result($sel, $i, "rslt_pattern");
		$rslt_reason = pg_fetch_result($sel, $i, "rslt_reason");
		$rslt_night_duty = pg_fetch_result($sel, $i, "rslt_night_duty");
		$rslt_allow_id = pg_fetch_result($sel, $i, "rslt_allow_id");
		$over_24hour_flag = pg_fetch_result($sel, $i, "over_24hour_flag");
        $empcond_officehours_flag = pg_fetch_result($sel, $i, "empcond_officehours_flag"); //20140318
		$rslt_meeting_start_time = pg_fetch_result($sel, $i, "rslt_meeting_start_time"); //20151009
		$rslt_meeting_end_time = pg_fetch_result($sel, $i, "rslt_meeting_end_time");
		$meeting_start_time = pg_fetch_result($sel, $i, "meeting_start_time");
		$meeting_end_time = pg_fetch_result($sel, $i, "meeting_end_time");
        //所定時刻取得
		if ($prov_start_time != "") {
			$start2 = $prov_start_time;
		}
        else {
        //個人別所定時刻対応 20140318
            $start2 = "";
            $end2 = "";
            if ($empcond_officehours_flag != "") {
                $arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($emp_id, $today);
                $wk_weekday = $arr_weekday_flg[$today];
                $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $today);
                if ($arr_empcond_officehours["officehours2_start"] != "") {
                    $start2 = $arr_empcond_officehours["officehours2_start"];
                    $end2 = $arr_empcond_officehours["officehours2_end"];
                }
            }
            //未設定時は時間帯画面の時刻を使用
            if ($start2 == "") {
                $start2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $type, "start2" );
            }
		}
		$wk_start2 = str_replace(":", "", $start2);
		if ($prov_end_time != "") {
			$end2 = $prov_end_time;
		}
        else {
            //個人別所定時刻対応 上で設定した$end2を使用する 20140318
            //未設定時は時間帯画面の時刻を使用
            if ($end2 == "") {
                $end2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $type, "end2" );
            }
		}
		$wk_end2 = str_replace(":", "", $end2);
		//実績レコードなしの場合、登録 ※出勤時刻設定済をSQLの条件としているため実際はレコードなしはない
		if ($rslt_date == "") {
			$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, night_duty, allow_id, tmcd_group_id, start_time, start_btn_time, start_date, end_time, end_btn_time, end_date ";
			$sql .= ") values (";
			
			$content = array($emp_id, $today, $pattern, $reason, $night_duty, $allow_id, $tmcd_group_id, $wk_start2, $wk_start2, $today, $wk_end2, $wk_end2, $today);
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo "  db insert error\n";
				exit;
			}
		}
		//更新
		else {
			$sql = "update atdbkrslt set";
			$set = array("end_time", "end_btn_time", "end_date");
			$setvalue = array($wk_end2, $wk_end2, $today);
			//実績項目の内容確認 
			if ($rslt_group_id == "" && $tmcd_group_id != "") {
				array_push($set, "tmcd_group_id");
				array_push($setvalue, $tmcd_group_id);
			}
			if ($rslt_pattern == "" && $pattern != "") {
				array_push($set, "pattern");
				array_push($setvalue, $pattern);
			}
			if ($rslt_reason == "" && $reason != "") {
				array_push($set, "reason");
				array_push($setvalue, $reason);
			}
			if ($rslt_night_duty == "" && $night_duty != "") {
				array_push($set, "night_duty");
				array_push($setvalue, $night_duty);
			}
			if ($rslt_allow_id == "" && $allow_id != "") {
				array_push($set, "allow_id");
				array_push($setvalue, $allow_id);
			}
			//翌日フラグ
			if ($wk_start2 > $wk_end2 || $over_24hour_flag == "1") {
				array_push($set, "next_day_flag");
				array_push($setvalue, "1");
			}
			//会議・研修・病棟外勤務 20151009
			if ($rslt_meeting_start_time == "" && $meeting_start_time != "") {
				array_push($set, "meeting_start_time");
				array_push($setvalue, $meeting_start_time);
			}
			if ($rslt_meeting_end_time == "" && $meeting_end_time != "") {
				array_push($set, "meeting_end_time");
				array_push($setvalue, $meeting_end_time);
			}
			$cond = "WHERE emp_id = '$emp_id' and date = '$today'";
			
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo "  db update error\n";
				exit;
			}
		}
		
	}
	
	// トランザクションをコミット
	pg_query($con, "commit");

	
	//前日の退勤が未設定の場合の設定
	$yesterday = date("Ymd", strtotime("-1 day")); 
	
	// トランザクションを開始
	pg_query($con, "begin");
	
	//予定a, 実績b, 出勤パターンc
	$sql = <<<_SQL_END_
select a.emp_id, a.tmcd_group_id, a.pattern, a.reason, a.night_duty, a.allow_id,
a.prov_start_time, a.prov_end_time, b.date as rslt_date,
b.tmcd_group_id as rslt_group_id, b.pattern as rslt_pattern, b.reason as rslt_reason,
b.night_duty as rslt_night_duty, b.allow_id as rslt_allow_id, c.over_24hour_flag
, b.meeting_start_time as rslt_meeting_start_time, b.meeting_end_time as rslt_meeting_end_time
, c.meeting_start_time, c.meeting_end_time
from atdbk a 
left join atdbkrslt b on a.emp_id = b.emp_id and a.date = b.date
left join atdptn c on a.tmcd_group_id = c.group_id and a.pattern = CAST(c.atdptn_id AS varchar)
_SQL_END_;
	//前日予定の勤務パターン設定済
	//自動打刻フラグがある勤務パターン
	//実績の退勤時刻未設定、出勤時刻設定済
	$cond = "where a.date = '$yesterday' and a.pattern != '' and c.auto_clock_out_flag = 1 and b.start_time != '' and (b.end_time is null or b.end_time = '')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo "  db select error\n";
		exit;
	}
	$num = pg_numrows($sel);
	
	for ($i=0; $i<$num; $i++) {
		$emp_id = pg_fetch_result($sel, $i, "emp_id");
		$tmcd_group_id = pg_fetch_result($sel, $i, "tmcd_group_id");
		$pattern = pg_fetch_result($sel, $i, "pattern");
		$reason = pg_fetch_result($sel, $i, "reason");
		$night_duty = pg_fetch_result($sel, $i, "night_duty");
		$allow_id = pg_fetch_result($sel, $i, "allow_id");
		$prov_start_time = pg_fetch_result($sel, $i, "prov_start_time");
		$prov_end_time = pg_fetch_result($sel, $i, "prov_end_time");
		$rslt_date = pg_fetch_result($sel, $i, "rslt_date"); //実績レコードの日付、存在チェック用
		$rslt_group_id = pg_fetch_result($sel, $i, "rslt_group_id");
		$rslt_pattern = pg_fetch_result($sel, $i, "rslt_pattern");
		$rslt_reason = pg_fetch_result($sel, $i, "rslt_reason");
		$rslt_night_duty = pg_fetch_result($sel, $i, "rslt_night_duty");
		$rslt_allow_id = pg_fetch_result($sel, $i, "rslt_allow_id");
		$over_24hour_flag = pg_fetch_result($sel, $i, "over_24hour_flag");
		$rslt_meeting_start_time = pg_fetch_result($sel, $i, "rslt_meeting_start_time"); //20151009
		$rslt_meeting_end_time = pg_fetch_result($sel, $i, "rslt_meeting_end_time");
		$meeting_start_time = pg_fetch_result($sel, $i, "meeting_start_time");
		$meeting_end_time = pg_fetch_result($sel, $i, "meeting_end_time");
		//所定時刻取得
		if ($prov_start_time != "") {
			$start2 = $prov_start_time;
		} else {
			$start2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $type, "start2" );
		}
		$wk_start2 = str_replace(":", "", $start2);
		if ($prov_end_time != "") {
			$end2 = $prov_end_time;
		} else {
			$end2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $type, "end2" );
		}
		$wk_end2 = str_replace(":", "", $end2);
		//更新
		$sql = "update atdbkrslt set";
		$set = array("end_time", "end_btn_time", "end_date");
		$setvalue = array($wk_end2, $wk_end2, $today);
		//実績項目の内容確認 
		if ($rslt_group_id == "" && $tmcd_group_id != "") {
			array_push($set, "tmcd_group_id");
			array_push($setvalue, $tmcd_group_id);
		}
		if ($rslt_pattern == "" && $pattern != "") {
			array_push($set, "pattern");
			array_push($setvalue, $pattern);
		}
		if ($rslt_reason == "" && $reason != "") {
			array_push($set, "reason");
			array_push($setvalue, $reason);
		}
		if ($rslt_night_duty == "" && $night_duty != "") {
			array_push($set, "night_duty");
			array_push($setvalue, $night_duty);
		}
		if ($rslt_allow_id == "" && $allow_id != "") {
			array_push($set, "allow_id");
			array_push($setvalue, $allow_id);
		}
		//翌日フラグ
		if ($wk_start2 > $wk_end2 || $over_24hour_flag == "1") {
			array_push($set, "next_day_flag");
			array_push($setvalue, "1");
		}
		//会議・研修・病棟外勤務 20151009
		if ($rslt_meeting_start_time == "" && $meeting_start_time != "") {
			array_push($set, "meeting_start_time");
			array_push($setvalue, $meeting_start_time);
		}
		if ($rslt_meeting_end_time == "" && $meeting_end_time != "") {
			array_push($set, "meeting_end_time");
			array_push($setvalue, $meeting_end_time);
		}
		$cond = "WHERE emp_id = '$emp_id' and date = '$yesterday'";
		
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo "  db update error\n";
			exit;
		}
		
	}
	
	// トランザクションをコミット
	pg_query($con, "commit");
}

?>
