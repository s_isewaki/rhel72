<?
require_once("about_comedix.php");
require_once("fplus_common_class.php");

$fname=$PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 78, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$obj = new fplus_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

// 申請論理削除
$obj->update_delflg_all_apply($apply_id, "t");

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 添付ファイルの移動
foreach (glob("fplus/apply/{$apply_id}_*.*") as $tmpfile) {
	unlink($tmpfile);
}

foreach (glob("fplus/apply/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}

// 一覧画面に遷移
echo("<script type=\"text/javascript\">location.href='fplus_menu.php?session=$session';</script>");
?>
