<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix タイムカード | 残業申請</title>
<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("show_clock_in_common.ini");
require_once("show_select_values.ini");
require_once("show_overtime_template.ini");
require_once("timecard_common_class.php");
require_once("timecard_bean.php");
require_once("ovtm_class.php"); //20140619

$fname = $PHP_SELF;

// 日付の設定
if ($date == "") {$date = date("Ymd");}

// データベースに接続
$con = connect2db($fname);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

$ovtm_class = new ovtm_class($con, $fname); //20140619

if ($session != "") {

	// セッションのチェック
	$session = qualify_session($session, $fname);
	if ($session == "0") {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}

	// ログインユーザの職員IDを取得
	$emp_id = get_emp_id($con, $session, $fname);
} else {

	// 職員の存在チェック
	$sql = "select emp_id from login";
	$cond = "where emp_login_id = '$id' and emp_login_pass = '$pass' and exists (select * from authmst where authmst.emp_id = login.emp_id and authmst.emp_del_flg = 'f')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
}

$timestamp_today = date_utils::to_timestamp_from_ymd($date);
$previous_day       = date("Ymd", strtotime("-1 day", $timestamp_today));
$next_day           = date("Ymd", strtotime("1 day", $timestamp_today));

$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $previous_day, $next_day);
$arr_ovtm_data = $timecard_common_class->get_ovtm_month_total($emp_id, $date, "input");
//print_r($arr_ovtm_data);

// 個人指定の承認者が設定されているか確認
$sql = "select emp_id from empmst";
$cond = "where emp_id = (select overtime_id from tmcdwkfw where emp_id = '$emp_id')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$overtime_id = pg_fetch_result($sel, 0, "emp_id");
}


// 承認者ワークフロー情報取得
$sql = "select * from atdbk_wkfw";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$ovtm_wkfw_div = pg_fetch_result($sel, 0, "ovtm_wkfw_div");
$wkfw_approve_num = pg_fetch_result($sel, 0, "ovtmcfm");



// 勤務実績情報を取得
//$arr_result = get_atdbkrslt($con, $emp_id, $date, $fname);
$arr_result = $timecard_common_class->get_clock_in_atdbkrslt_array($con, $emp_id, $fname, "1", $date);

//登録済みの情報表示 20110830
$sql = "select apply_id, reason_id, reason, reason_detail, apply_status from ovtmapply";
$cond = "where emp_id = '$emp_id' and target_date = '$date' and delete_flg = 'f' order by apply_id desc limit 1";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0)
{
	$apply_id = pg_fetch_result($sel, 0, "apply_id");
	$reason_id = pg_fetch_result($sel, 0, "reason_id");
	$reason = pg_fetch_result($sel, 0, "reason");
	$reason_detail = pg_fetch_result($sel, 0, "reason_detail");
	$apply_status = pg_fetch_result($sel, 0, "apply_status");
}

// 残業理由一覧を取得
$sql = "select reason_id, reason from ovtmrsn";
$cond = "where del_flg = 'f' order by reason_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$ovtmadddisp = "none";
if ($postback != "t") {
    $start_hour = ($arr_result["start_time"] != "") ? intval(substr($arr_result["start_time"], 0, 2), 10) : "";
    $start_min = substr($arr_result["start_time"], 2, 2);
    $end_hour = ($arr_result["end_time"] != "") ? intval(substr($arr_result["end_time"], 0, 2), 10) : "";
    $end_min = substr($arr_result["end_time"], 2, 2);
}
else {
    $ovtmadddisp = ($ovtmadd_disp_flg == '1') ? '' : 'none';
}
//デフォルト
// 未設定時のデフォルト取得
if ($arr_result["over_start_time"] == "" || $arr_result["over_end_time"] == "") {
    $emp_officehours = $timecard_common_class->get_officehours_emp($emp_id, $date, $arr_result["tmcd_group_id"], $arr_result["pattern"]);
    
    $arr_default_time = $timecard_common_class->get_default_overtime($arr_result["start_time"], $arr_result["end_time"], $arr_result["next_day_flag"], $arr_result["tmcd_group_id"], $arr_result["pattern"], $arr_result["type"], $arr_result["previous_day_flag"], $emp_officehours["office_end_time"] );
}

if ($postback != "t") {
    //前日チェックがあるかフラグ
    $previous_day_flag = $arr_result["previous_day_flag"];
    //翌日チェックがあるかフラグ
    $next_day_flag = $arr_result["next_day_flag"];
}
//前日チェックがあるかフラグ
$previous_day_flag_checked = "";
if ($previous_day_flag == 1){
    $previous_day_flag_checked = "checked";
}
//翌日チェックがあるかフラグ
$next_day_flag_checked = "";
if ($next_day_flag == 1){
    $next_day_flag_checked = "checked";
}
/*
$over_start_next_day_flag_checked = "";
if ($over_start_next_day_flag == 1){
	$over_start_next_day_flag_checked = "checked";
}
$over_end_next_day_flag_checked = "";
if ($over_end_next_day_flag == 1){
	$over_end_next_day_flag_checked = "checked";
}
$over_start_next_day_flag2_checked = "";
if ($over_start_next_day_flag2 == 1){
	$over_start_next_day_flag2_checked = "checked";
}
$over_end_next_day_flag2_checked = "";
if ($over_end_next_day_flag2 == 1){
	$over_end_next_day_flag2_checked = "checked";
}
*/
//
//$arr_result = $timecard_common_class->get_clock_in_atdbkrslt_array($con, $emp_id, $fname, $date);

//残業申請画面に来る場合は、データのある日付が指定されるため、翌日のデータ確認は不要
/* 個人別勤務時間帯履歴対応、$timecard_common_class->get_clock_in_atdbkrslt_array内で取得 20130220 */
//$table_width = ($ovtm_class->keyboard_input_flg != "t" && $ovtm_class->ovtm_rest_disp_flg == "t") ? 700 : 640;
$table_width = 700;
//if ($ovtm_class->ovtm_reason2_input_flg == "t") {
//	$table_width += 210;
//}
$maxline = ($_POST["ovtmadd_disp_flg"] == '1') ? 5 : 2; 

$arr_rest_data = $timecard_common_class->get_office_rest_data($emp_id, $date, $arr_result);
//残業理由
$arr_ovtmrsn = $ovtm_class->get_ovtmrsn($arr_result["tmcd_group_id"]);

?>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/event/event-min.js" ></script>
<script type="text/javascript" src="js/yui_0.12.2/build/dom/dom-min.js" ></script>
<script type="text/javascript" src="js/yui_0.12.2/build/dragdrop/dragdrop-min.js"></script>

<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/focustonext.js"></script>
<script type="text/javascript" src="js/atdbk_timecard_edit.js?t=<? echo time(); ?>"></script>
<script type="text/javascript">
//resizeTo(<? echo $table_width+55; ?>, 740);

var arr_over_no_apply_flag = new Array();
<?
foreach ($arr_ovtmrsn as $i => $arr_info) {
    
    echo "arr_over_no_apply_flag[{$arr_info["reason_id"]}] = '{$arr_info["over_no_apply_flag"]}';\n"; 
}

?>
function setOtherReasonDisabled(item_idx) {
	var reason_id_nm = 'reason_id'+item_idx;
	var other_reason_nm = 'other_reason'+item_idx;
	
	if (document.getElementById(other_reason_nm)) {
		document.getElementById(other_reason_nm).style.display = (document.getElementById(reason_id_nm).value == 'other') ? '' : 'none';
	}
}

//休憩不足時間（分）
var rest_fusoku_min = 0;
//残業限度超時間（メッセージ用）
var over_limit_str = '';

// 申請処理
function apply_regist() {

    var approve_num = 'approve_num';
    var apv_num = document.mainform.elements[approve_num].value;

	// 承認者が全く存在しない場合
    var regist_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj = 'regist_emp_id' + i;
		emp_id = document.mainform.elements[obj].value;

		if(emp_id != "") {
			regist_flg = true;
            break;            
		}
	}

    if(!regist_flg)
    {
        alert('承認者が存在しないため、申請できません。');
        return;
    }

	// 重複チェック
	dpl_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj_src = 'regist_emp_id' + i;
		emp_id_src = document.mainform.elements[obj_src].value;

        if(emp_id_src == '')
        {
            continue;
        }

		for(j=i+1; j<=apv_num; j++) {
			obj_dist = 'regist_emp_id' + j;
			emp_id_dist = document.mainform.elements[obj_dist].value;
			if(emp_id_src == emp_id_dist) {
				dpl_flg = true;
			}
		}
	}

	if(dpl_flg)
	{
		if (!confirm('承認者が重複していますが、申請しますか？'))
		{
			return;
		}
	}
    //申請不要時理由確認
    //その他はチェックなしで登録
    if (document.mainform.reason_id.value != 'other') {
        var reason_id = document.mainform.reason_id.value;
 	    // 申請ボタンが押された場合
        if (document.mainform.no_apply.value != 'y') {
    		//理由の確認（不要以外）
		    if (arr_over_no_apply_flag[reason_id] == "t") {
                alert('残業申請の場合には、残業申請不要理由以外を選択してください。');
                return;
            }
        }
    }
	//理由
    if (!document.mainform.reason_id2 &&
		document.mainform.reason_id.value == 'other') {
        if (document.mainform.reason.value == '') {
            alert('理由が入力されていません。');
            return;
        }
    }
    //休憩時間、残業時間チェック
    chk_flg = checkTime(2);
	if(chk_flg)
	{
        if (chk_flg == 1) {
            return false;
        }
		if (!confirm(msg))
		{
			return;
		}
	}
    
	document.mainform.action = 'overtime_apply_exe.php';
	document.mainform.submit();

}

function no_apply_submit() {
    //その他はチェックなしで登録
    if (document.mainform.reason_id.value != 'other') {
        var reason_id = document.mainform.reason_id.value;
		//理由の申請不要
	    if (arr_over_no_apply_flag[reason_id] != "t") {
            alert('残業申請不要の場合には、残業申請不要理由を選択してください。');
            return;
        }
    }
    <?
    if ($ovtm_class->ovtm_reason2_input_flg == "t") {
    ?>
    if (!confirm('残業申請不要では1番目の理由のみ登録し残業時刻はクリアされます。残業申請不要で登録してよろしいですか？')) {
    	return;
    }
    for (var i=1; i<=5; i++) {

        var wk_line = (i == 1) ? '' : i;

	    //項目名
        var s_h = 'over_start_hour'+wk_line;
        var s_m = 'over_start_min'+wk_line;
        var s_f = 'over_start_next_day_flag'+wk_line;
        var e_h = 'over_end_hour'+wk_line;
        var e_m = 'over_end_min'+wk_line;
        var e_f = 'over_end_next_day_flag'+wk_line;
        //value
        document.getElementById(s_h).value = '';
        document.getElementById(s_m).value = '';
        //var s_flag = document.getElementById(s_f).checked;
        document.getElementById(e_h).value = '';
        document.getElementById(e_m).value = '';
        //var e_flag = document.getElementById(e_f).checked;
	}
    <?
    }
    ?>
    document.mainform.no_apply.value = 'y';
	document.mainform.action = 'overtime_apply_exe.php';
	document.mainform.submit();
}
// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);

    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];
		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}


function ovtmAddDisp(flg) {
    var ovtmadddisp = (flg == true) ? '' : 'none';
    var val = (flg) ? '1' : '0';
    document.mainform.ovtmadd_disp_flg.value = val;
    document.mainform.action = 'overtime_apply.php';
    document.mainform.submit();
    return false;
}

<? //翌日フラグチェック ?>
var base_time = '<?
$base_time = $arr_result["office_end_time"];
if ($arr_result["office_end_time"] < $arr_result["office_start_time"]) {
    $base_time = $arr_result["office_start_time"];  
}
$base_time = str_replace(":", "", $base_time);
echo $base_time; ?>';

    //労働基準法の休憩時間が取れていない場合は警告する
    var rest_check_flg = '<? echo $ovtm_class->rest_check_flg; ?>';
    //36協定の残業限度時間をこえたら警告する
    var ovtm_limit_check_flg = '<? echo $ovtm_class->ovtm_limit_check_flg; ?>';
 
    //行数
    var maxline = <? echo $maxline; ?>;
    //休憩時間チェック
    var kinmu_total_min = 0;
    var rest_total_min = 0;
    var office_time_min = <? echo $arr_rest_data["office_time_min"]; ?>;
    var rest_time_min = <? echo $arr_rest_data["rest_time_min"]; ?>;
    
    //36協定残業限度時間
var ovtm_limit_month = <?
$limit_month = ($ovtm_class->ovtm_limit_month == '') ? 0 : $ovtm_class->ovtm_limit_month;
echo $limit_month;
    ?>;
    var ovtm_limit_month_min = ovtm_limit_month * 60;
    //当月残業累計時間申請中含む
    var ovtm_approve_apply_min = <? echo $arr_ovtm_data["ovtm_approve_apply_min"]; ?>;

    //当月残業累計時間承認のみ
    var ovtm_approve_min = <? echo $arr_ovtm_data["ovtm_approve_min"]; ?>;

    var ovtm_rest_disp_flg = '<? echo $ovtm_class->ovtm_rest_disp_flg; ?>';

    var rest_disp_flg = '<? echo $ovtm_class->rest_disp_flg; ?>';

    var over_time_apply_type = '<? echo $timecard_bean->over_time_apply_type; ?>';

    var no_overtime = '<? echo $no_overtime; ?>';

    var apply_update_flg = '1'; //1:申請 2:更新


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.fblock3 td {border-width:0;}

p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="resizeTo(document.getElementById('main_table').offsetWidth+40, 740);resizeAllTextArea();setOtherReasonDisabled(''); document.mainform.no_apply.value = '';document.onkeydown=focusToNext;document.mainform.over_start_hour.focus();<?php
if ($ovtm_class->ovtm_reason2_input_flg == "t") {
	for ($i=2; $i<=5; $i++) {
		echo "setOtherReasonDisabled('$i');";
	}
}
?>">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>残業申請</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<? if ($ovtm_wkfw_div != "1" && $overtime_id == "") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認者が設定されていません。承認者設定画面で残業申請の承認者を設定してください。</font></td>
</tr>
</table>
<? } else { ?>
<form name="mainform" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="left" width="120">
<input type="button" value="残業申請不要" onclick="no_apply_submit();" onblur="document.onkeydown=focusToNext;" onfocus="document.onkeydown='';">
</td>
<td align="left">
    <? //タブを追加
    if ($ovtm_class->ovtm_tab_flg == "t") {
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($ovtmadd_disp_flg == '1') { ?>
<a href="javascript:void(0);" onclick="return ovtmAddDisp(false);">
<? } ?>
残業申請
<? if ($ovtmadd_disp_flg == '1') { ?>
</a>
<? } ?>
&nbsp;
<? if ($ovtmadd_disp_flg != '1') { ?>
<a href="javascript:void(0);" onclick="return ovtmAddDisp(true);">
<? } ?>
残業追加
<? if ($ovtmadd_disp_flg != '1') { ?>
</a>
<? } ?>
</font>
<? } ?>
</td>
<td align="right">
<input type="button" id="apply_button" value="残業申請" style="width:100px;" onclick="apply_regist();" onblur="document.onkeydown=focusToNext;" onfocus="document.onkeydown='';">
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list" id="main_table">
<tr height="22">
<td width="70" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $date)); ?></font></td>
</tr>
<tr height="22" id="timearea1">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">始業時刻</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_result["office_start_time"])); ?></font></td>
</tr>
<tr height="22" id="timearea2">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤時刻</font></td>
<td colspan="2">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">
    <?
    if ($ovtm_class->keyboard_input_flg == "t") { 
        ?>
        <input type="text" name="start_hour"  id="start_hour" value="<? echo $start_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='start_min';backElement='<?
        //
        $backElement =($ovtm_class->ovtm_rest_disp_flg == "t") ? "rest_end_min$maxline" : "over_end_min$maxline";
        echo $backElement;
              ?>';">：<input type="text" name="start_min"  id="start_min" value="<? echo $start_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='end_hour';backElement='start_hour';">
        <?
    }
    else {
    ?>
		<select name="start_hour" id="start_hour"><? show_hour_options_0_23($start_hour, true); ?></select>：<select name="start_min" id="start_min"><? show_min_options_00_59($start_min, true); ?></select>
        <? } ?>
		<label for="zenjitsu"><input type="checkbox" name="previous_day_flag" id="zenjitsu" <?=$previous_day_flag_checked ?> value="1">前日</label>
	</font>
    <?
  //打刻 20140624
?>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <?
    echo ("　　打刻&nbsp;");
    echo($ovtm_class->get_btn_date_format($arr_result["start_btn_date1"]));
    $tmp_start_btn_time = $ovtm_class->get_btn_time_format($arr_result["start_btn_time"]);
    echo($tmp_start_btn_time);
    if ($arr_result["start_btn_time2"] != "") {
        echo ("　　　出勤2&nbsp;");
	    echo($ovtm_class->get_btn_date_format($arr_result["start_btn_date2"]));
        $tmp_start_btn_time2 = $ovtm_class->get_btn_time_format($arr_result["start_btn_time2"]);
        echo($tmp_start_btn_time2);
    }
    ?>
	</font>
</td>
</tr>
<tr height="22" id="timearea3">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">終業時刻</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $arr_result["office_end_time"])); ?></font></td>
</tr>
<tr height="22" id="timearea4">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤時刻</font></td>
<td colspan="2">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">
    <?
    if ($ovtm_class->keyboard_input_flg == "t") { 
        ?>
            <input type="text" name="end_hour"  id="end_hour" value="<? echo $end_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='end_min';backElement='start_min';">：<input type="text" name="end_min"  id="end_min" value="<? echo $end_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='over_start_hour';backElement='end_hour';">
        <?
    }
    else {
    ?>
		<select name="end_hour" id="end_hour"><? show_hour_options_0_23($end_hour, true); ?></select>：<select name="end_min" id="end_min"><? show_min_options_00_59($end_min, true); ?></select>
        <? } ?>
		<label for="yokujitsu"><input type="checkbox" name="next_day_flag" id="yokujitsu" <?=$next_day_flag_checked ?> value="1">翌日</label>
	</font>
    <?
    //打刻 20140624
?>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <?
    echo ("　　打刻&nbsp;");
    echo($ovtm_class->get_btn_date_format($arr_result["end_btn_date1"]));
    $tmp_end_btn_time = $ovtm_class->get_btn_time_format($arr_result["end_btn_time"]);
    echo($tmp_end_btn_time);
    if ($arr_result["end_btn_time2"] != "") {
        echo ("　　　退勤2&nbsp;");
	    echo($ovtm_class->get_btn_date_format($arr_result["end_btn_date2"]));
        $tmp_end_btn_time2 = $ovtm_class->get_btn_time_format($arr_result["end_btn_time2"]);
        echo($tmp_end_btn_time2);
    }
    ?>
	</font>
</td>
</tr>
    <?
    $ovtm_class->disp_btn_time_data($arr_result, "input");
?>
    <? //20140620
    //残業申請不要理由を引数の配列に入れる
    $arr_ovtmapply = array();
    if ($apply_status == "4") {
    	$arr_ovtmapply["reason_id"] = $reason_id;
    	$arr_ovtmapply["reason"] = $reason;
    	$arr_ovtmapply["apply_status"] = $apply_status;
    }
    //タイムカード修正画面から呼ばれた場合
    if ($pnt_url != "") {
    	$arr_ovtmapply["reason_id"] = $reason_id;
    	$arr_ovtmapply["reason"] = $reason;
    	for ($i=2; $i<=5; $i++) {
    		$reason_id_nm = "reason_id".$i;
    		$reason_nm = "reason".$i;
    		$arr_ovtmapply["$reason_id_nm"] = $$reason_id_nm;
    		$arr_ovtmapply["$reason_nm"] = $$reason_nm;
    	}
    }
    $ovtm_class->disp_input_form($arr_result, $timecard_bean, $arr_default_time, $apply_status, $arr_ovtmapply);
    $ovtm_class->disp_ovtm_total_time($arr_ovtm_data);

    //残業時刻2以降にも理由を入力する場合は既存の理由を表示しない
    if ($ovtm_class->ovtm_reason2_input_flg != "t") {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由</font></td>
<td>
<select name="reason_id" id="reason_id" onchange="setOtherReasonDisabled('');" onblur="document.onkeydown=focusToNext;" onfocus="document.onkeydown='';">
<?
//20140619
    $str = $ovtm_class->get_ovtmrsn_option($arr_result["tmcd_group_id"], $reason_id);
    echo $str;
//理由が入力済みの場合、その他とする
	$selected = ($reason != "") ? " selected" : "";
?>
<option value="other" <? echo($selected); ?>>その他</option>
</select><br>
<div id="other_reason" style="display:none;"><input type="text" name="reason" value="<? echo($reason); ?>" size="50" maxlength="100" style="ime-mode:active;" onblur="document.onkeydown=focusToNext;" onfocus="document.onkeydown='';"></div>
</td>
</tr>
<?php } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由詳細</font></td>
<td colspan="2">
    <textarea name="reason_detail" rows="5" cols="40" style="ime-mode: active;" onFocus="new ResizingTextArea(this);document.onkeydown='';" onblur="document.onkeydown=focusToNext;"><?
echo($reason_detail);?></textarea>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td colspan="2" valign="top" align="center">

<?
$wkfw_flg = ($ovtm_wkfw_div != "1") ? false : true;
show_overtime_template($con, $session, $fname, $emp_id, $wkfw_flg);
?>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="left">
<input type="button" value="残業申請不要" onclick="no_apply_submit();" onblur="document.onkeydown=focusToNext;" onfocus="document.onkeydown='';">
</td>
<td align="right">
<input type="button" id="apply_button" value="残業申請" style="width:100px;" onclick="apply_regist();" onblur="document.onkeydown=focusToNext;" onfocus="document.onkeydown='';">
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
<input type="hidden" name="pnt_url" value="<? echo($pnt_url); ?>">
<input type="hidden" name="no_apply" value="">
<input type="hidden" name="pre_start_time" value="<? echo($arr_result["start_time"]); ?>">
<input type="hidden" name="pre_previous_day_flag" value="<? echo($arr_result["previous_day_flag"]); ?>">
<input type="hidden" name="pre_end_time" value="<? echo($arr_result["end_time"]); ?>">
<input type="hidden" name="pre_next_day_flag" value="<? echo($arr_result["next_day_flag"]); ?>">
<input type="hidden" name="wkfw_approve_num" value="<? echo($wkfw_approve_num); ?>">
<input type="hidden" name="pre_over_start_time" value="<? echo($arr_result["over_start_time"]); ?>">
<input type="hidden" name="pre_over_end_time" value="<? echo($arr_result["over_end_time"]); ?>">
<input type="hidden" name="pre_over_start_next_day_flag" value="<? echo($arr_result["over_start_next_day_flag"]); ?>">
<input type="hidden" name="pre_over_end_next_day_flag" value="<? echo($arr_result["over_end_next_day_flag"]); ?>">
<input type="hidden" name="pre_over_start_time2" value="<? echo($arr_result["over_start_time2"]); ?>">
<input type="hidden" name="pre_over_end_time2" value="<? echo($arr_result["over_end_time2"]); ?>">
<input type="hidden" name="pre_over_start_next_day_flag2" value="<? echo($arr_result["over_start_next_day_flag2"]); ?>">
<input type="hidden" name="pre_over_end_next_day_flag2" value="<? echo($arr_result["over_end_next_day_flag2"]); ?>">
<input type="hidden" name="keyboard_input_flg" value="<? echo($ovtm_class->keyboard_input_flg); ?>">
    <?/* 追加 */
    for ($i=3; $i<=5; $i++) {
        $s_t = "over_start_time".$i;
        $e_t = "over_end_time".$i;
        $s_f = "over_start_next_day_flag".$i;
        $e_f = "over_end_next_day_flag".$i;
?>
<input type="hidden" name="pre_<? echo $s_t; ?>" value="<? echo($arr_result[$s_t]); ?>">
<input type="hidden" name="pre_<? echo $e_t; ?>" value="<? echo($arr_result[$e_t]); ?>">
<input type="hidden" name="pre_<? echo $s_f; ?>" value="<? echo($arr_result[$s_f]); ?>">
<input type="hidden" name="pre_<? echo $e_f; ?>" value="<? echo($arr_result[$e_f]); ?>">
<?
    }    
    for ($i=1; $i<=5; $i++) {
        $s_t = "rest_start_time".$i;
        $e_t = "rest_end_time".$i;
        $s_f = "rest_start_next_day_flag".$i;
        $e_f = "rest_end_next_day_flag".$i;
?>
<input type="hidden" name="pre_<? echo $s_t; ?>" value="<? echo($arr_result[$s_t]); ?>">
<input type="hidden" name="pre_<? echo $e_t; ?>" value="<? echo($arr_result[$e_t]); ?>">
<input type="hidden" name="pre_<? echo $s_f; ?>" value="<? echo($arr_result[$s_f]); ?>">
<input type="hidden" name="pre_<? echo $s_f; ?>" value="<? echo($arr_result[$s_f]); ?>">
        <?
    }    
?>
<input type="hidden" name="postback" value="t">
<input type="hidden" name="ovtmadd_disp_flg" value="<? echo($ovtmadd_disp_flg); ?>">
</form>
<? } ?>
</center>
</body>
<? pg_close($con); ?>
</html>
