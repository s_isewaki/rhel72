<?
require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");
require_once("about_comedix.php");
require_once("referer_common.ini");
require_once("news_common.ini");

$fname = $_SERVER['PHP_SELF'];

// セッションのチェック
$session = qualify_session($_REQUEST['session'], $fname);
if ($session == "0") {
    js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "news", $fname);

// お知らせ集計機能設定を取得
$conf = new Cmx_SystemConfig();
$setting_aggregate = $conf->get('setting_aggregate');

//オプションから引用した設定が"t"ではなかった場合に、"f"を設定する。
if ($setting_aggregate != "t") {
    $setting_aggregate = "f";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ管理 | お知らせ区分一覧</title>
<? include_js("js/fontsize.js"); ?>
<? include_js("js/jquery/jquery-1.9.1.min.js"); ?>
<? include_js("js/jquery/jquery-ui-1.10.3.min.js"); ?>
<script type="text/javascript">
$(function() {
    $('#check').click(function() {
        if($(this).get(0).checked) {
            $('input.delete').prop('checked', true);
        }
        else {
            $('input.delete').prop('checked', false);
        }
    });

    $('#sortable > tbody').sortable({
        items: '> tr',
        handle: '.handle',
        axis: 'y',
        opacity: 0.4
    });

    $('#btn_delete').click(function() {
        if ($('input.delete:checked').length <= 0) {
            alert('お知らせ区分がチェックされていません');
            return;
        }
        if (confirm('チェックしたお知らせ区分を削除してよろしいですか？')) {
            $('#mode').val('delete');
            $('#notice_form').submit();
        }
    });

    $('#btn_sort').click(function() {
        $('#mode').val('sort');
        $('#notice_form').submit();
    });
});
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/smoothness/jquery-ui-1.10.3.custom.min.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="news_menu.php?session=<?=h($session);?>"><img src="img/icon/b21.gif" width="32" height="32" border="0" alt="お知らせ・回覧板管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="news_menu.php?session=<?=h($session);?>"><b>お知らせ・回覧板管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="newsuser_menu.php?session=<?=h($session);?>"><img src="img/icon/b35.gif" width="32" height="32" border="0" alt="お知らせ・回覧板"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="newsuser_menu.php?session=<?=h($session);?>"><b>お知らせ・回覧板</b></a> &gt; <a href="news_menu.php?session=<?=h($session);?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="newsuser_menu.php?session=<?=h($session);?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="90" height="22" align="center" bgcolor="#bdd1e7"><a href="news_menu.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_register.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_search.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_list.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_register.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#5279a5"><a href="news_notice2_list.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>お知らせ区分<br>一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice2_register.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_cate_list.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_cate_register.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="news_trash.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ゴミ箱</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="news_option.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<?php //オプション機能で集計表機能を許可するにチェックを入れることで、タブに集計表が表示される。?>
<td width="5">&nbsp;</td>
<?php if($setting_aggregate == 't'){?>
<td width="70" align="center" bgcolor="#bdd1e7">
    <a href="news_aggregate.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計表</font></a>
</td>
<?php }?>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10">

<form id="notice_form" action="news_notice2_delete.php" method="post">
<table id="sortable" border="0" cellspacing="0" cellpadding="2" class="list">
    <thead>
        <tr height="22" bgcolor="#f6f9ff">
            <td width="30"></td>
            <td width="30" align="center"><input type="checkbox" id="check"></td>
            <td width="400"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分</font></td>
            <td width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">件数</font></td>
        </tr>
    </thead>
    <tbody>
<?
	$sql = "select notice2_id, notice2_name, (select count(*) from news where news.notice2_id=newsnotice2.notice2_id) as count from newsnotice2 order by order_no, notice2_id";
	$sel_notice = select_from_table($con, $sql, "", $fname);
	if ($sel_notice == 0) {
		pg_close($con);
        js_error_exit();
	}

	while ($row = pg_fetch_array($sel_notice)) {
		print('<tr height="22">');
		printf('<td width="30" align="center" style="cursor:move;" class="handle"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><input type="hidden" name="order[]" value="%s"></td>', $row["notice2_id"]);
		printf('<td width="30" align="center"><input type="checkbox" name="notice_ids[]" value="%s" %s class="%s"></td>', $row["notice2_id"], ($row["count"] ? 'disabled' : ''), ($row["count"] ? 'disabled' : 'delete'));
		printf('<td width="400"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="news_notice2_update.php?session=%s&notice_id=%s">%s</a></font></td>', $session, $row["notice2_id"], $row["notice2_name"]);
		printf('<td width="50" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">%s</font></td>', $row["count"]);
		print('</tr>');
	}
?>
    </tbody>
</table>
    
<p>
    <button type="button" id="btn_sort">並び順を更新する</button>
    <button type="button" id="btn_delete">チェックしたお知らせ区分を削除する</button>
</p>

<input type="hidden" name="session" value="<?=h($session);?>">
<input type="hidden" id="mode" name="mode" value="">
</form>

<p class="j12">
    ※使用されているお知らせ区分は削除できません<br>
    ※矢印をドラッグすると並べ替えできます<br>
</p>
</body>
<? pg_close($con); ?>
</html>
