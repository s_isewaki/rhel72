<?php

ob_start();
ini_set("max_execution_time", 0);

require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once('fpdf153/mbfpdf.php');

require_once("atdbk_common_class.php");
require_once("atdbk_duty_shift_common_class.php");
require_once("atdbk_workflow_common_class.php");
require_once("date_utils.php");
require_once("holiday.php");
require_once("timecard_bean.php");
require_once("timecard_common_class.php");
require_once("show_attendance_pattern.ini");
require_once("show_timecard_common.ini");
require_once("atdbk_info_common.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("calendar_name_class.php");
require_once("atdbk_close_class.php");

ob_end_clean();

$left_margin = 9;

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
// system_config
$conf = new Cmx_SystemConfig();
$pdf_tmcd_group_id = $_POST['tmcd_group_id'];
$conf->set('timecard.pdf_tmcd_group_id', $pdf_tmcd_group_id);
$duty_or_oncall_flg = $conf->get('timecard.duty_or_oncall_flg');
if ($duty_or_oncall_flg == "") {
	$duty_or_oncall_flg = "1";
}
$duty_str = ($duty_or_oncall_flg == "1") ? "当直" : "待機";

//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);

//ワークフロー関連共通クラス
$atdbk_workflow_common_class = new atdbk_workflow_common_class($con, $fname);

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();
//カレンダー名、所定労働時間履歴用
$calendar_name_class = new calendar_name_class($con, $fname);

$atdbk_close_class = new atdbk_close_class($con, $fname);

//追加表示する事由IDを取得
$arr_reason_id = $atdbk_common_class->get_add_disp_reason_id();
$arr_disp_flg = array();
$disp_cnt = 0;
for ($i=0; $i<count($arr_reason_id); $i++) {
	//休暇種別等画面の非表示フラグ
	$tmp_display_flag = $atdbk_common_class->get_reason_display_flag($arr_reason_id[$i]);
	$arr_disp_flg[$i] = $tmp_display_flag;
	if ($tmp_display_flag == 't') {
		$disp_cnt++;
	}
}
//タイムカード集計項目出力設定からデータ取得
$arr_total_flg = get_timecard_total_flg($con, $fname); //show_timecard_common.ini
$arr_total_id = get_timecard_total_id($con, $fname); //出力項目のid

// 事由ID、表示用名称、表示フラグの配列
$arr_reason_id = $atdbk_common_class->get_add_disp_reason_id();
$arr_reason_name = $atdbk_common_class->get_add_disp_reason_name2();
$arr_disp_flg = array();
$disp_cnt = 0;
for ($i=0; $i<count($arr_reason_id); $i++) {
	$tmp_display_flag = $atdbk_common_class->get_reason_display_flag($arr_reason_id[$i]);
	$arr_disp_flg[$i] = $tmp_display_flag;
}

// 決裁欄役職情報を取得
$st_name_array = get_timecard_st_name_array($con, $fname);

//勤務シフト実績入力以外
if ($wherefrom != "7") {
	//期間指定
	$start_date = $select_start_yr.$select_start_mon.$select_start_day;
	$end_date = $select_end_yr.$select_end_mon.$select_end_day;

	//ソート順
	$arr_sortkey = array();
	for($i=1; $i<=5; $i++) {
		$varname = "sortkey".$i;
		$arr_sortkey[$varname] = $$varname;
	}
	//対象者検索画面からの場合
	if ($target_emp_id == "") {
		$arr_emp_id = get_arr_emp_id_from_layout_id($con, $fname, $srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $shift_group_id, $csv_layout_id, $arr_sortkey, $emp_id_list);
	}
	//A4横画面からの場合
	else {
		$arr_emp_id = array($target_emp_id);
		//管理画面側からの場合、集計項目、勤務実績回数の印刷フラグを設定から取得
		if ($wherefrom == "3") {
			$pdf_total_print_flg = ($timecard_bean->pdf_total_print_flg == "") ? "1" : $timecard_bean->pdf_total_print_flg;
			$pdf_count_print_flg = ($timecard_bean->pdf_count_print_flg == "") ? "1" : $timecard_bean->pdf_count_print_flg;
		}
	}
}
//勤務シフト実績入力からの場合
else {
	$arr_emp_id = split(",", $emp_id_list);

	$closing = $timecard_bean->closing;
	$closing_part = $timecard_bean->closing_parttime;
	if ($closing_part == "") {
		$closing_part = $closing;
	}
	$year = substr($yyyymm, 0, 4);
	$month = intval(substr($yyyymm, 4, 2));
	// 開始日・締め日を算出
	$calced = false;
	switch ($closing) {
		case "1":  // 1日
			$closing_day = 1;
			break;
		case "2":  // 5日
			$closing_day = 5;
			break;
		case "3":  // 10日
			$closing_day = 10;
			break;
		case "4":  // 15日
			$closing_day = 15;
			break;
		case "5":  // 20日
			$closing_day = 20;
			break;
		case "6":  // 末日
			$start_year = $year;
			$start_month = $month;
			$start_day = 1;
			$end_year = $start_year;
			$end_month = $start_month;
			$end_day = days_in_month($end_year, $end_month);
			$calced = true;
			break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		//当月〜翌月
		$closing_month_flg = $timecard_bean->closing_month_flg;
		if ($closing_month_flg != "2") {
			$start_year = $year;
			$start_month = $month;

			$end_year = $year;
			$end_month = $month + 1;
			if ($end_month == 13) {
				$end_year++;
				$end_month = 1;
			}
		}
		//前月〜当月
		else {
			$start_year = $year;
			$start_month = $month - 1;
			if ($start_month == 0) {
				$start_year--;
				$start_month = 12;
			}

			$end_year = $year;
			$end_month = $month;
		}

	}
	$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);

	// 開始日・締め日を算出
	$calced = false;
	switch ($closing_part) {
		case "1":  // 1日
			$closing_day = 1;
			break;
		case "2":  // 5日
			$closing_day = 5;
			break;
		case "3":  // 10日
			$closing_day = 10;
			break;
		case "4":  // 15日
			$closing_day = 15;
			break;
		case "5":  // 20日
			$closing_day = 20;
			break;
		case "6":  // 末日
			$start_year = $year;
			$start_month = $month;
			$start_day = 1;
			$end_year = $start_year;
			$end_month = $start_month;
			$end_day = days_in_month($end_year, $end_month);
			$calced = true;
			break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		//当月〜翌月
		$closing_month_flg = $timecard_bean->closing_month_flg;
		if ($closing_month_flg != "2") {
			$start_year = $year;
			$start_month = $month;

			$end_year = $year;
			$end_month = $month + 1;
			if ($end_month == 13) {
				$end_year++;
				$end_month = 1;
			}
		}
		//前月〜当月
		else {
			$start_year = $year;
			$start_month = $month - 1;
			if ($start_month == 0) {
				$start_year--;
				$start_month = 12;
			}

			$end_year = $year;
			$end_month = $month;
		}

	}
	$start_date_part = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date_part = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);
}

//公休数取得用の情報
$closing_month_flg = $timecard_bean->closing_month_flg;
//公休数取得 月毎、属性毎の配列
$year  = substr($yyyymm, 0, 4);
if (count($arr_emp_id) > 1) { //1件より多い場合
	$arr_legal_hol_cnt = get_legal_hol_cnt($con, $fname, $year, $timecard_bean->closing, $closing_month_flg);
	if ($timecard_bean->closing != $timecard_bean->closing_parttime) {
		$arr_legal_hol_cnt_part = get_legal_hol_cnt($con, $fname, $year, $timecard_bean->closing_parttime, $closing_month_flg);
	} else {
		$arr_legal_hol_cnt_part = $arr_legal_hol_cnt;
	}
} else {
	//1件以下の場合は、不要。get_legal_hol_cnt_monthの内部で取得する。
	$arr_legal_hol_cnt = array();
	$arr_legal_hol_cnt_part = array();
}
$arr_holwk = get_timecard_holwk_day($con, $fname);

if ($default_start_date != $start_date || $default_end_date != $end_date) {
	$legal_hol_cnt_flg = true;
	$legal_hol_cnt_kikan = get_legal_hol_cnt_kikan($con, $fname, $start_date, $end_date, $arr_holwk);
} else {
	$legal_hol_cnt_flg = false;
}

//A4横
$pdf = new MBFPDF('L', 'mm', 'A4');
$pdf->SetAutoPageBreak(true, 5);
$pdf->AddMBFont(MINCHO,'EUC-JP');
$pdf->AddMBFont(GOTHIC,'EUC-JP');
$pdf->Open();

//会議研修病棟外の列を常に表示とする 20120319
$meeting_display_flag = true;

for ($i=0; $i<count($arr_emp_id); $i++) {
	$emp_id = $arr_emp_id[$i];

	//締め日考慮(期間が変更されている場合は、そちらを使用する) 20160404
	if ($default_start_date == $start_date && $default_end_date == $end_date) {
		$closing = $atdbk_close_class->get_empcond_closing($emp_id);
		$arr_date = $atdbk_close_class->get_term($yyyymm, $closing, $closing_month_flg);
		$start_date = $arr_date["start"];
		$end_date = $arr_date["end"];
		$start_date_part = $start_date;
		$end_date_part = $end_date;
	}

	$page = $i+1;
    $arr_atdbk_info = get_atdbk_info($con, $fname, $session, $emp_id, $yyyymm, $atdbk_common_class, $start_date, $end_date, $atdbk_workflow_common_class, $timecard_bean, $meeting_display_flag, $arr_legal_hol_cnt, $arr_legal_hol_cnt_part, $arr_holwk, $legal_hol_cnt_flg, $legal_hol_cnt_kikan, $obj_hol_hour, $wherefrom, $start_date_part, $end_date_part, "1", $calendar_name_class);
	output_pdf($con, $fname, $emp_id, $pdf, $page, $timecard_bean, $st_name_array, $left_margin, $arr_atdbk_info, $arr_total_id, $arr_total_flg, $arr_reason_id, $arr_reason_name, $arr_disp_flg, $atdbk_common_class, $start_date, $end_date, $wherefrom, $pdf_total_print_flg, $pdf_count_print_flg);
}



//対象者検索画面からの場合
if ($target_emp_id == "" && $wherefrom != "7") {
	pg_query($con, "begin transaction");
	//ソート順の保存
	$sql = "update timecard set";
	$set = array("sortkey1", "sortkey2", "sortkey3", "sortkey4", "sortkey5", "pdf_total_print_flg", "pdf_count_print_flg");
	$setvalue = array($sortkey1, $sortkey2, $sortkey3, $sortkey4, $sortkey5, $pdf_total_print_flg, $pdf_count_print_flg);
	$cond = "";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	pg_query("commit");
}

pg_close($con);


$pdf->Output();

die;

/**
 * レイアウトIDの条件にあう職員取得
 *   レイアウト調整画面からの場合は、指定職員をソートする
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $srch_name 職員名
 * @param mixed $cls 施設
 * @param mixed $atrb 部
 * @param mixed $dept 科
 * @param mixed $duty_form_jokin 常勤
 * @param mixed $duty_form_hijokin 非常勤
 * @param mixed $group_id 出勤グループ
 * @param mixed $shift_group_id シフトグループ
 * @param mixed $csv_layout_id 月集計CSVレイアウト
 * @param mixed $arr_sortkey ソートキー
 * @param mixed $emp_id_list 指定職員ID
 * @return mixed 職員ID
 *
 */
function get_arr_emp_id_from_layout_id($con, $fname, $srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $shift_group_id, $csv_layout_id, $arr_sortkey, $emp_id_list) {

	$sql = "select empmst.emp_id, emp_personal_id, emp_lt_nm, emp_ft_nm from empmst ";
	$sql .= "left join classmst on classmst.class_id = empmst.emp_class ";
	$sql .= "left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute ";
	$sql .= "left join deptmst on deptmst.dept_id = empmst.emp_dept ";
	$sql .= "left join jobmst on jobmst.job_id = empmst.emp_job ";

	$cond = "where exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f') ";

	//一覧のチェックボックスを判断する方式
	//職員ID
	if ($emp_id_list != "") {
		$emp_ids = explode(",", $emp_id_list);
		$emp_id_str = "";
		for ($i=0; $i<count($emp_ids); $i++) {
			if ($i > 0) {
				$emp_id_str .= ",";
			}
			$emp_id_str .= "'".$emp_ids[$i]."'";
		}
		$cond .= "and empmst.emp_id in ($emp_id_str) ";
	}
//以下はcsvダウンロードの方式（チェックボックスは関係なく検索条件で判断）をコメントで残す
/*
	//検索条件　職員名
	if ($srch_name != "") {
		$tmp_srch_name = str_replace(" ", "", $srch_name);
		$cond .= " and (emp_lt_nm like '%$tmp_srch_name%' or emp_ft_nm like '%$tmp_srch_name%' or emp_kn_lt_nm like '%$tmp_srch_name%' or emp_kn_ft_nm like '%$tmp_srch_name%' or emp_lt_nm || emp_ft_nm like '%$tmp_srch_name%' or emp_kn_lt_nm || emp_kn_ft_nm like '%$tmp_srch_name%')";
	}
	//検索条件　属性：事務所
	if ($cls != "" && substr($cls, 0, 1) != "-") {
		$cond .= " and ((empmst.emp_class = $cls) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = $cls))";
	}
	//検索条件　属性：課
	if ($atrb != "" && substr($atrb, 0, 1) != "-") {
		$cond .= " and ((empmst.emp_attribute = $atrb) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_attribute = $atrb))";
	}
	//検索条件　属性：科
	if ($dept != "" && substr($dept, 0, 1) != "-") {
		$cond .= " and ((empmst.emp_dept = $dept) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_dept = $dept))";
	}
	//検索条件　雇用・勤務形態
	if ($duty_form_jokin != $duty_form_hijokin) {
		if ($duty_form_hijokin == "checked") {
			//非常勤
			$cond .= " and ";
		}
		else {
			//常勤
			$cond .= " and NOT ";
		}
		$cond .= "EXISTS(SELECT * FROM empcond WHERE empcond.emp_id = empmst.emp_id AND duty_form = 2)";
	}
	//検索条件　出勤グループ
	if ($group_id != "" && $group_id != "-") {
		$cond .= " and exists (select * from empcond where empcond.emp_id = empmst.emp_id and empcond.tmcd_group_id = $group_id)";
	}

	//検索条件　シフトグループ
	if ($shift_group_id != "" && $shift_group_id != "-") {
		$cond .= " and exists (select * from duty_shift_staff where duty_shift_staff.emp_id = empmst.emp_id and duty_shift_staff.group_id = '$shift_group_id')"; //職員設定の情報から
	}

	//検索条件　月集計CSVレイアウト
	if ($csv_layout_id != "" && $csv_layout_id != "-") {
		$cond .= " and exists (select * from empcond where empcond.emp_id = empmst.emp_id and (empcond.csv_layout_id = '$csv_layout_id'";
		if ($csv_layout_id == "01") {
			$cond .= " or empcond.csv_layout_id is null";
		}
		$cond .= "))";
	}
*/
	//ソートキー指定数
	$sort_num = 0;
	for ($i=1; $i<=5; $i++) {
		$varname = "sortkey".$i;
		if ($arr_sortkey[$varname] > 0) {
			$sort_num++;
		}
	}
	if ($sort_num == 0) {
		$cond .= " order by emp_personal_id";
	} else {
		//出力順項目
		$arr_sortitem = array("", "emp_personal_id", "emp_kn_lt_nm, emp_kn_ft_nm", "classmst.link_key, empmst.emp_class", "atrbmst.link_key, empmst.emp_attribute", "deptmst.link_key1, empmst.emp_dept", "jobmst.link_key, jobmst.order_no");
		$cond .= " order by ";
		$first_flg = true;
		for ($i=1; $i<=5; $i++) {
			$varname = "sortkey".$i;
			$idx = $arr_sortkey[$varname];
			$wk_sortitem = $arr_sortitem[$idx];
			if ($idx > 0) {
				if ($first_flg) {
					$first_flg = false;
				} else {
					$cond .= ", ";
				}
				$cond .= $wk_sortitem;
			}
		}
	}

	$sel_emp = select_from_table($con, $sql, $cond, $fname);
	if ($sel_emp == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$arr_emp_id = array();
	while ($row = pg_fetch_array($sel_emp)) {
		$arr_emp_id[] = $row["emp_id"];
	}
	return $arr_emp_id;
}


/**
 * pdf出力
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $emp_id 職員ID
 * @param mixed $pdf PDF
 * @param mixed $page ページ
 * @param mixed $timecard_bean タイムカード設定
 * @param mixed $st_name_array 役職名
 * @param mixed $left_margin 左位置
 * @param mixed $arr_info タイムカード情報
 * @param mixed $arr_total_id 集計項目
 * @param mixed $arr_total_flg 集計項目表示フラグ
 * @param mixed $arr_reason_id 事由
 * @param mixed $arr_reason_name 事由名
 * @param mixed $arr_disp_flg 事由表示フラグ
 * @param mixed $atdbk_common_class 共通クラス
 * @param mixed $start_date 開始日
 * @param mixed $end_date 終了日
 * @param mixed $wherefrom 呼出元 3:管理側A4横画面 4:ユーザ側A4横画面
 * @param mixed $$pdf_total_print_flg 集計項目印刷フラグ  1:する 0:しない
 * @param mixed $$pdf_count_print_flg 勤務実績回数印刷フラグ 1:する 0:しない
 * @return なし
 *
 */
function output_pdf($con, $fname, $emp_id, &$pdf, $page, $timecard_bean, $st_name_array, $left_margin, $arr_info, $arr_total_id, $arr_total_flg, $arr_reason_id, $arr_reason_name, $arr_disp_flg, $atdbk_common_class, $start_date, $end_date, $wherefrom, $pdf_total_print_flg, $pdf_count_print_flg) {

	$pdf->AddPage();

	$pdf->setY(14);
	$pdf->SetFont(GOTHIC,'',12); //GOTHIC,MINCHO

	$timecard_title = $arr_info["timecard_title"];
	$pdf->Cell(0, 2.0, $timecard_title,'',0,'C');

	$pdf->SetFont(GOTHIC,'',9);
	$pdf->setXY($left_margin,17);
	$pdf->Cell(16, 4, '対象期間：','0', 0,'L');
	$yyyymm_str = $arr_info["year"]."年".$arr_info["month"]."月度　";
	$pdf->Cell(24, 4, $yyyymm_str,'0', 0,'L');

	//$start_m = intval(substr($arr_info["start_date"], 4, 2));
	//$start_d = intval(substr($arr_info["start_date"], 6, 2));
	//$end_m = intval(substr($arr_info["end_date"], 4, 2));
	//$end_d = intval(substr($arr_info["end_date"], 6, 2));
	$start_m = intval(substr($start_date, 4, 2));
	$start_d = intval(substr($start_date, 6, 2));
	$end_m = intval(substr($end_date, 4, 2));
	$end_d = intval(substr($end_date, 6, 2));
	$period_str = "開始日 ".$start_m."月".$start_d."日 〜 締め日 ".$end_m."月".$end_d."日";
	$pdf->Cell(60, 4, $period_str,'0', 0,'L');
	$pdf->Cell(8, 4, '所属：','0', 0,'L');

	$pdf->Cell(50, 4, $arr_info["str_orgname"],'0', 0,'L');

	$pdf->setXY($left_margin,21);
	$pdf->Cell(16, 4, '　職員ID：','0', 0,'L');
	$pdf->Cell(30, 4, $arr_info["emp_personal_id"],'0', 0,'L');
	$pdf->Cell(10, 4, '氏名：','0', 0,'L');
	$wk_str = $arr_info["emp_name"]."（".$arr_info["str_empcond"];
	if ($arr_info["str_shiftname"] != "") {
		$wk_str .= "　".$arr_info["str_shiftname"];
	}
	$wk_str .= "）";
	$pdf->Cell(50, 4, $wk_str,'0', 1,'L');

	//決裁欄

	$pdf->SetFont(GOTHIC,'',8);
	if ($timecard_bean->timecard_appr_disp_flg == "t") {
		$appr_wd = 5;	//決裁欄の幅
		$wd = 18;		//決裁者欄の幅
		//左側位置
		$x1 = 297 - 8 - $appr_wd - (count($st_name_array) * $wd);
		$abox_top = 7;		//上側位置

		$pdf->setXY($x1,$abox_top);
		$pdf->Cell($appr_wd, 18, ' ','1', 1,'C');
		$pdf->setXY($x1,$abox_top+4);
		$pdf->Write(4,"決");
		$pdf->setXY($x1,$abox_top+10);
		$pdf->Write(4,"裁");

		for ($i=0; $i<count($st_name_array); $i++) {
			$xpos = $x1 + $appr_wd + ($wd * $i);
			$pdf->setXY($xpos,$abox_top);
			$str = mb_substr($st_name_array[$i]["st_name"], 0, 6);
			$pdf->Cell($wd, 4, $str,'1', 1,'C');
		}
		for ($i=0; $i<count($st_name_array); $i++) {
			$xpos = $x1 + $appr_wd + ($wd * $i);
			$pdf->setXY($xpos,$abox_top + 4);
			$pdf->Cell($wd, 14,'','1', 1,'C');
		}
	}

	$pdf->SetFont(GOTHIC,'',8);

	$pdf->setXY($left_margin,26);

	//$arr_title = array("日付", "曜日", "勤務実績", "事由", "前", "出勤", "翌", "退勤", "遅刻", "早退", "外出", "休憩", "勤務", "有休", "法定内", "法定外", "深夜残", "休日残", "呼出", "日数", "残業理由", "手当", "会議研修");
	$arr_title = get_a4_title(); //atdbk_info_common.phpへ移動

	$time_width = 10;
	$arr_width = array(
			14,	//0 日付
			6, //
			20, //2 勤務実績
			20, //3 事由
			6, //4 前
			$time_width, //5
			6, //6 翌
			$time_width,
			$time_width,
			$time_width,
			$time_width,
			$time_width, //11 休憩時間
			11,
			11,
			11,
			11,
			$time_width, //16 深夜残業
			$time_width,
			$time_width+4, //18 呼出
			10, //19 日数
			27, //20 残業理由
			20, //21 手当
			13  //22 会議研修
			);

	//layout test
	//	$arr_str = array("99月99日", "ｘ", "１２３４５６", "１２３４５６", "ｘ", "99:99", "ｘ", "99:99", "99:99", "99:99", "99:99", "99:99", "99:99", "99:99", "99:99", "99:99", "99:99", "99:99", "99:99", "9.9", "１２３４５６７８", "１２３４５６", "99:99");
	//テキスト位置の配列
	//$arr_align = array("C", "C", "L", "L", "C", "R", "C", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R", "C", "L", "L", "R");
	$arr_align = get_a4_align(); //atdbk_info_common.phpへ移動

	// Cell(幅、高さ、文字列、境界線、次の位置）
	for ($i=0; $i<count($arr_title); $i++) {
		$str = $arr_title[$i];
		//次の位置
		$ln = ($i==22) ? 1 : 0;
		$wd = $arr_width[$i];
		$pdf->Cell($wd, 5.0, $str,'1',$ln,'C');
	}

	$pdf->SetFont(MINCHO,'',9);

	$day_count = $arr_info["day_count"];

	for ($line=0; $line<$day_count; $line++) {

		$pdf->setX($left_margin);

		for ($i=0; $i<count($arr_title); $i++) {
			$str = $arr_info[$line][$i];
            //修正申請中の場合 20121121 残業申請中、復帰申請中の場合に「*」をつける 20130311
            if ($i == 2) {
                if ($arr_info[$line]["modify_link_type"] == "2" ||
                		$arr_info[$line]["overtime_link_type"] == "2" ||
                		$arr_info[$line]["return_link_type"] == "2")
                {
                    $str .= " *";
                    $pdf->SetTextColor(255, 0, 0);
                }
                else {
                    $pdf->SetTextColor(0, 0, 0);
                }

            }
			//長い文字列対応
			if ($i == 3 || $i == 21) {
				$str = mb_substr($str, 0, 6);
			} elseif ($i == 20) {
				$str = mb_substr($str, 0, 8);
			}
			//残業理由
			//if ($i == 20) {
			//	$overtime_link_type = $arr_info[$line]["overtime_link_type"];
			//申請不要　※申請不要でも印刷する 20110830
			//	if ($overtime_link_type == "6") {
			//		$str = "";
			//	}
			//}
			//呼出の設定が表示しないの場合
			if ($i == 18 && $timecard_bean->ret_display_flag == "f") {
				$str = "";
			}
			//次の位置
			$ln = ($i==22) ? 1 : 0;
			$wd = $arr_width[$i];

			$align = $arr_align[$i];
			$type = $arr_info[$line]["type"];
			//背景色変更
			if ($type == "4" || $type == "6") {//法定、祝日、ピンク
				$pdf->SetFillColor(250, 222, 222);
				$fill = 1;
			} elseif ($type == "5" || $type == "7") {//所定、年末年始、水色
				$pdf->SetFillColor(222, 250, 250);
				$fill = 1;
			} else {
				$fill = 0;
			}
			//遅刻早退の色
			if ($i == 8 || $i == 9) {
				$pdf->SetTextColor(255, 0, 0);
			} elseif ($i != 2) { //勤務実績以外
				$pdf->SetTextColor(0, 0, 0);
			}
			//ユーザ側A4画面の場合
			if ($wherefrom == "4") {
				//出勤時刻
				if ($i == 5) {
					// 出退勤時刻の表示設定
					if ($str != "" && $timecard_bean->show_time_flg == "f" ){
						$str = ($arr_info[$line][8] != "") ? "遅刻" : "○";
					}
					$align = "C";
				}
				//退勤時刻
				if ($i == 7) {
					// 出退勤時刻の表示設定
					if ($str != "" && $timecard_bean->show_time_flg == "f" ){
						$str = ($arr_info[$line][9] != "") ? "早退" : "○";
					}
					$align = "C";
				}
			}
			$pdf->Cell($wd, 4.0, $str,'1',$ln,$align, $fill);
		}

	}
	//合計行位置
	$pdf->setX($left_margin);

	//数値を表示配列に設定
	$sums = $arr_info["sums"];
	$arr_str = array("合計", "", "", "", "", "", "", "",
			$arr_info["delay_time_total"], //遅刻
			$arr_info["early_leave_time_total"], //早退
			$arr_info["out_time_total"], //外出
			$arr_info["rest_time_total"], //休憩
			$sums[13], //勤務
			$arr_info["paid_time_total"], //有給
			$sums[29], //法定内残業
			$sums[30], //法定外残業
			$sums[15], //深夜残業
			$sums[31], //休日残業
			$arr_info["return_time_total"], //呼出（退復）
			$sums[1], //日数
			 "", "",
			$arr_info["meeting_time_total"] //会議・件数

			);
	//layout test
	//	$arr_str = array("合計", "", "", "", "", "", "", "", "99:99", "99:99", "99:99", "999:99", "999:99", "999:99", "999:99", "999:99", "99:99", "99:99", "99:99", "99.9", "", "", "999:99");
	//合計行出力
	for ($i=0; $i<count($arr_title); $i++) {
		$str = $arr_str[$i];
		//次の位置
		$ln = ($i==22) ? 1 : 0;
		$wd = $arr_width[$i];
		//テキスト位置
		$align = $arr_align[$i];
		//遅刻早退の色
		if (($i == 8 || $i == 9) && $str != "0:00") {
			$pdf->SetTextColor(255, 0, 0);
		} else {
			$pdf->SetTextColor(0, 0, 0);
		}
		//合計のフォント変更
		if ($i == 0) {
			$pdf->SetFont(GOTHIC,'',9);
		} elseif ($i == 1) {
			$pdf->SetFont(MINCHO,'',9);
		}
		if ($i == 19) {
			$str = sprintf("%.1f", $str);
			//小数以下が０の場合は整数のみとする
			$wk_num = split("\.", $str);
			if ($wk_num[1] == "0") {
				$str = $wk_num[0];
			}
		}
		//呼出の設定が表示しないの場合
		if ($i == 18 && $timecard_bean->ret_display_flag == "f") {
			$str = "";
		}
		$pdf->Cell($wd, 4.0, $str,'1',$ln,$align);

	}
	$total_margin = 10;

	//集計項目印刷フラグ
	if ($pdf_total_print_flg != "0") {

		//集計項目
		$pdf->SetFont(GOTHIC,'',9);

		$pdf->setX($left_margin+$total_margin);
		$pdf->Cell(13, 4, '集計項目','0', 1,'L');

		$pdf->SetFont(MINCHO,'',9);
		$arr_total_info = array();
		//要勤務日数〜早退
		$idx = 0;
		for ($i=0; $i<13; $i++) {
			if ($arr_total_flg[$i]["flg"] == "t") {
				$arr_total_info[$idx]["name"] = $arr_total_flg[$i]["name"];
				if ($i < 8) {
					$wk_str = $sums[$i];
				} elseif ($i== 8) {
					$wk_str = $sums[35]; //年残
				} else {
					$wk_idx = $i - 1;
					$wk_str = $sums[$wk_idx];
				}

				$arr_total_info[$idx]["sum"] = $wk_str;
				$idx++;
			}
		}
		if (in_array("10_0", $arr_total_id)) {
			for ($i=0; $i<count($arr_reason_id); $i++) {
				if ($arr_disp_flg[$i] == 't') {
					$arr_total_info[$idx]["name"] = $arr_reason_name[$i];
					// $sumsのindexを調整、0公休,1法定〜 $atdbk_common_class->get_add_disp_reason_name()参照
					//法定所定追加 20110121
					if ($i == 1) { //法定
						$wk_idx = 36;
					} elseif ($i == 2) { //所定
						$wk_idx = 37;
					} elseif ($i > 2 && $i < 11) {
						$wk_idx = $i + 14; //[17]:特別
					} elseif ($i == 11) { //年末年始
						$wk_idx = 33;
					} elseif ($i > 11) {
						$wk_idx = $i + 13; //[25]:結婚休
					} else {
						$wk_idx = $i + 16; //[16]:公休
					}

					$arr_total_info[$idx]["sum"] = $sums[$wk_idx];
					$idx++;
				}
			}
		}

		//集計項目表示、28件より増える場合は$j<?を変更
		for ($i=0; $i<4; $i++) {
			$pdf->setX($left_margin+$total_margin);
			for ($j=0; $j<7; $j++) {
				//集計項目名
				$idx = $i + $j*4;
				$item_name = $arr_total_info[$idx]["name"];
				$item_name = str_replace("<br>", "", $item_name);
                $item_name = mb_substr($item_name, 0, 7);
                $pdf->SetTextColor(0, 0, 0);
				$pdf->Cell(23, 4, $item_name,'0', 0,'L');
				$ln = ($j==6) ? 1 : 0;
				//日数
				$str = $arr_total_info[$idx]["sum"];
				if ($str == "" && $item_name != "") {
					$str = "0";
				}
				if (($item_name == "遅刻" || $item_name == "早退") && $str != "0") {
					$pdf->SetTextColor(255, 0, 0);
				} else {
					$pdf->SetTextColor(0, 0, 0);
				}
				$pdf->Cell(13, 4, $str,'0', 0,'L');
				$pdf->Cell(3, 4, ' ','0', $ln,'L');

			}
		}
	}

	//勤務実績回数印刷フラグ
	if ($pdf_count_print_flg != "0") {
		output_shift_summary($con, $fname, $emp_id, $start_date, $end_date, $arr_total_id, $pdf, $left_margin, $total_margin, $atdbk_common_class, $arr_info);
	}

	$pdf->setXY($left_margin, 200);

	$pdf->Cell(0, 4, "{$page}ページ",'0', 0,'C');

}
/*************************************************************************/
// シフト集計表示
// @param $con DBコネクション
// @param $fname 画面名
// @param $emp_id 職員ID
// @param $start_date 開始日
// @param $end_date 終了日
// @param $arr_total_id 集計項目出力設定
// @param $pdf PDF
// @param $left_margin 左位置
// @param $total_margin 集計項目位置
// @param $atdbk_common_class 共通クラス
// @param $arr_info タイムカード情報
/*************************************************************************/
function output_shift_summary($con, $fname, $emp_id, $start_date, $end_date, $arr_total_id, &$pdf, $left_margin, $total_margin, $atdbk_common_class, $arr_info)
{
	//勤務グループ指定 20150820
	$target_tmcd_group_id = $_REQUEST["tmcd_group_id"];
	if ($target_tmcd_group_id == "all") {
		$target_tmcd_group_id = "";
	}

	$arr_pattern    = array();
	$arr_night_duty = array();
	$arr_allow      = array();
	$arr_group_pattern = array();
	$work_arr_pattern = array();

	$table_name = "atdbkrslt";

	$today = date("Ymd");
	//////////////////////////////////////////
	// 勤務シフト集計
	//////////////////////////////////////////
	$arr_group_pattern = $arr_info["arr_group_pattern"];

	//////////////////////////////////////////
	// 当直集計
	//////////////////////////////////////////
	$arr_night_duty_date = $arr_info["arr_night_duty_date"];

	$holiday_cnt = 0;
	$satday_cnt = 0;
	$weekday_cnt = 0;

	foreach($arr_night_duty_date as $night_duty_date)
	{
		$date = $night_duty_date["date"];
		$timestamp = to_timestamp($date);

		// 日付から休日かを求める
		$holiday = ktHolidayName($timestamp);
		// 日付から曜日を求める
		$wd = get_weekday($timestamp);

		if($holiday != "" || $wd == "日")
		{
			$holiday_cnt++;
		}
		else if($wd == "土")
		{
			$satday_cnt++;
		}
		else
		{
			$weekday_cnt++;
		}
	}
	$arr_night_duty["holiday_cnt"] = $holiday_cnt;
	$arr_night_duty["satday_cnt"]  = $satday_cnt;
	$arr_night_duty["weekday_cnt"] = $weekday_cnt;


	//////////////////////////////////////////
	// 手当集計
	//////////////////////////////////////////
	$arr_allow = $arr_info["arr_allow"];

	//所属グループ
	$sql = "select a.group_id,b.pattern_id from duty_shift_staff a inner join duty_shift_group b on b.group_id = a.group_id ";
	$cond = "where a.emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);
	if ($num > 0) {
		$belong_group_id = pg_result($sel,0,"pattern_id");
	} else {
		$belong_group_id = $arr_info["tmcd_group_id"]; //勤務条件のグループ
	}
	// 名称と回数のテーブル
	$arr_summary = array();
	$i = 0;
	// 勤務シフト
	if (in_array("10_3", $arr_total_id)) {
		//勤務グループ指定 20150820
		if ($target_tmcd_group_id != "") {
			foreach($arr_group_pattern as $group){

				//所属グループ
				$group_id    = $group["group_id"];
				if ($group_id == $target_tmcd_group_id) {
					$group_name  = $group["group_name"];
					$arr_pattern = $group["arr_pattern"];
					foreach($arr_pattern as $pattern)
					{
						$arr_summary[$i]["name"] = $pattern["atdptn_nm"];
						$arr_summary[$i]["cnt"] = $pattern["pattern_cnt"];
						$i++;
					}
					break;
				}
			}
		}
		else {
			foreach($arr_group_pattern as $group){

				//所属グループ
				$group_id    = $group["group_id"];
				if ($group_id == $belong_group_id) {
					$group_name  = $group["group_name"];
					$arr_pattern = $group["arr_pattern"];
					foreach($arr_pattern as $pattern)
					{
						$arr_summary[$i]["name"] = $pattern["atdptn_nm"];
						$arr_summary[$i]["cnt"] = $pattern["pattern_cnt"];
						$i++;
					}
					break;
				}
			}
		}
	}
	// 当直
	if (in_array("10_1", $arr_total_id)) {
		global $duty_str;
		$arr_summary[$i]["name"] = "{$duty_str}平日";
		$arr_summary[$i]["cnt"] = $weekday_cnt;
		$i++;
		$arr_summary[$i]["name"] = "{$duty_str}土曜日";
		$arr_summary[$i]["cnt"] = $satday_cnt;
		$i++;
		$arr_summary[$i]["name"] = "{$duty_str}日曜・休日";
		$arr_summary[$i]["cnt"] = $holiday_cnt;
		$i++;
	}
	// 手当て
	if (in_array("10_2", $arr_total_id)) {
		foreach($arr_allow as $allow)
		{
			$arr_summary[$i]["name"] = $allow["allow_contents"];
			$arr_summary[$i]["cnt"] = $allow["allow_cnt"];
			$i++;
		}
	}

	if (count($arr_summary) > 0) {

		//見出し
		$pdf->setX($left_margin+$total_margin);
		$pdf->SetFont(GOTHIC,'',9);
		$pdf->Cell(22, 4, '勤務実績回数：','0', 0,'L');
		//勤務グループ指定 20150820
		if ($belong_group_id != $target_tmcd_group_id &&
			$target_tmcd_group_id != "") {
			$group_name = $atdbk_common_class->get_group_name($target_tmcd_group_id);
		}
		else {
			$group_name = $atdbk_common_class->get_group_name($belong_group_id);
		}

		$pdf->Cell(30, 4, $group_name,'0', 1,'L');
		$pdf->SetFont(MINCHO,'',9);

		$pdf->setX($left_margin+$total_margin);
		// 繰返し
		for ($i=0; $i<count($arr_summary); $i++) {
			//改行
			if (($i+1) % 7 == 0 || $i == count($arr_summary)-1) {
				$ln = 1;
			} else {
				$ln = 0;
			}
            //数値900等がある場合は長くする
            if (preg_match("/\d{3}/", $arr_summary[$i]["name"]) == 1) {
                $wk_len = 9;
            }
            else {
                $wk_len = 7;
            }
            $str = mb_substr($arr_summary[$i]["name"], 0, $wk_len);
			$pdf->Cell(23, 4, $str,'0', 0,'L');
			$str = $arr_summary[$i]["cnt"];
			if ($str == "") {
				$str = "0";
			}
			$pdf->Cell(13, 4, $str,'0', 0,'L');
			$pdf->Cell(3, 4, ' ','0', $ln,'L');
			if ($ln == 1) {
				$pdf->setX($left_margin+$total_margin);
			}
		}
	}

	//応援追加分
	if (in_array("10_3", $arr_total_id)) {
		foreach($arr_group_pattern as $group){
			//所属グループ以外
			$group_id    = $group["group_id"];

			if ($group_id == $belong_group_id) {
				continue;
			}
			//勤務グループ指定 20150820
			if ($target_tmcd_group_id != "") {
				continue;
			}

			$group_name  = $group["group_name"];
			$arr_pattern = $group["arr_pattern"];
			$arr_summary = array();
			$i = 0;
			foreach($arr_pattern as $pattern)
			{
				$arr_summary[$i]["name"] = $pattern["atdptn_nm"];
				$arr_summary[$i]["cnt"] = $pattern["pattern_cnt"];
				$i++;
			}
			$pdf->setX($left_margin+$total_margin);
			$pdf->SetFont(GOTHIC,'',9);
			$pdf->Cell(30, 4, $group_name,'0', 1,'L');
			$pdf->SetFont(MINCHO,'',9);

			$pdf->setX($left_margin+$total_margin);
			// 繰返し
			for ($i=0; $i<count($arr_summary); $i++) {
			//改行
				if (($i+1) % 7 == 0 || $i == count($arr_summary)-1) {
					$ln = 1;
				} else {
					$ln = 0;
				}
                //数値900等がある場合は長くする
                if (preg_match("/\d{3}/", $arr_summary[$i]["name"]) == 1) {
                    $wk_len = 9;
                }
                else {
                    $wk_len = 7;
                }
                $str = mb_substr($arr_summary[$i]["name"], 0, $wk_len);
				$pdf->Cell(23, 4, $str,'0', 0,'L');
				$str = $arr_summary[$i]["cnt"];
				if ($str == "") {
					$str = "0";
				}
                $pdf->Cell(13, 4, $str,'0', 0,'L');
				$pdf->Cell(3, 4, ' ','0', $ln,'L');
				if ($ln == 1) {
					$pdf->setX($left_margin+$total_margin);
				}
			}
		}
	}

}

?>