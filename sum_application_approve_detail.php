<?
ob_start();
require_once("about_comedix.php");
require_once("sum_application_imprint_common.ini");
require_once("get_values.ini");
require_once("sum_application_workflow_common_class.php");
require_once("sot_util.php");
require_once("summary_common.ini");
require_once("sum_apply_generate_revision.ini");
require_once("get_menu_label.ini");
ob_end_clean();
ob_start();
//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
    echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
    exit;
}

//====================================================================
// 主要リクエストパラメータ、よく使う変数
//====================================================================
$login_emp_id = get_emp_id($con, $session, $fname);
$apply_id = (int)@$_REQUEST["apply_id"]; // 最新の申請ID
$target_apply_id = @$_REQUEST["target_apply_id"]; // 履歴の場合に指定あり。過去履歴の申請ID
$apv_order_fix = (int)@$_REQUEST["apv_order_fix"]; // // この指示を開くときの、階層番号
$apv_sub_order_fix = (int)@$_REQUEST["apv_sub_order_fix"]; // この指示を開くときの、階層内の受信職員順番号
$worksheet_date = @$_REQUEST["worksheet_date"]; // テンプレートに与える日付。ワークシート上で開いた場合は値が入る
$command_mode = @$_REQUEST["command_mode"]; // "cyusi"(中止送信)、"update"(更新)、"update_and_resend"(更新)、"approve_regist"(送信)
$exec_mode = @$_REQUEST["exec_mode"]; // "apply": 送信一覧画面または温度板からの遷移。空：受信一覧画面からの遷移かワークシートからの遷移
$goto_opener_reload = @$_REQUEST["goto_opener_reload"];
$goto_close_self_window = "";
$is_rireki_order_update_error = "";
$is_new_order_equal_old_order = "";

if(@$target_apply_id == "") $target_apply_id = $apply_id;



$obj = new application_workflow_common_class($con, $fname);

//====================================================================
// リロードした瞬間に、既に他者により更新済みなら、apply_idを最新にする
//====================================================================
for(;;){
    $rs_apply1 = select_from_table($con, "select re_apply_id from sum_apply where apply_id = " . $apply_id, "", $fname);
    $row_apply1 = pg_fetch_array($rs_apply1);
    $re_apply_id = $row_apply1["re_apply_id"];
    if (!$re_apply_id) break;
    $apply_id = $re_apply_id;
}

//====================================================================
// 送信用に、各種の更新を行う。
//====================================================================
$is_window_close = false;

if ($command_mode == "cyusi" || $command_mode == "update" || $command_mode == "update_and_resend"){
    $is_window_close = true;

    // 送信テンプレート内容更新のために、申請テーブルを取得
    $rs_apply = select_from_table($con, "select * from sum_apply where apply_id = " . $target_apply_id, "", $fname);
    $row_apply = pg_fetch_array($rs_apply);
    $summary_id = $row_apply["summary_id"];
    $ptif_id =    $row_apply["ptif_id"];
    $apply_date_ymd = substr($row_apply["apply_date"], 0, 8);

    if ($row_apply["re_apply_id"]) $is_rireki_order_update_error = 1;

    // 送信テンプレート内容更新のために、サマリテーブルを取得
    $rs_summary = select_from_table($con, "select * from summary where summary_id = ".$summary_id." and ptif_id = '".$ptif_id."'", "", $fname);
    $row_summary = pg_fetch_array($rs_summary);
    $summary_seq = $row_summary["summary_seq"];

    // ワークフローマスタ取得
    $rs_wkfwmst = select_from_table($con, "select * from sum_wkfwmst where wkfw_id = ".(int)$row_apply["wkfw_id"], "", $fname);
    $row_wkfwmst = @pg_fetch_array($rs_wkfwmst);
    $is_modifyable_by_other_emp = $row_wkfwmst["is_modifyable_by_other_emp"];

    // 登録前後のテンプレートと添付ファイルを比較して、変更がなされているか確認
    if (!$is_rireki_order_update_error){
        // その１ まずは、テンプレートの比較
        $sql = "select smry_xml from sum_xml where xml_file_name = '".$summary_seq."_".$target_apply_id.".xml'";
        $rs = select_from_table($con, $sql, "", $fname);
        $old_smry_xml = @pg_fetch_result($rs, 0, "smry_xml");

        $sql = "select smry_xml from sum_xml where xml_file_name = 'wk_".$login_emp_id.".xml'";
        $rs = select_from_table($con, $sql, "", $fname);
        $new_smry_xml = @pg_fetch_result($rs, 0, "smry_xml");

        if ($old_smry_xml == $new_smry_xml) $is_new_order_equal_old_order = 1;

        // その２ テンプレートが同じ内容の場合、添付ファイルの数の比較
        $old_filenames = array();
        $new_filenames = array();
        if ($is_new_order_equal_old_order){
            foreach (glob("summary/attach/".$apply_id."_*.*") as $fpath) $old_filenames[] = $fpath;
            $attach_filenames = (array)@$_REQUEST["filename"];
            $attach_file_ids = (array)@$_REQUEST["file_id"];
            for ($i = 0; $i < count($attach_filenames); $i++) {
                $ext = strrchr($attach_filenames[$i], ".");
                $new_filenames[] = "summary/tmp/".$session."_".$attach_file_ids[$i].$ext;
            }
            if (count($old_filenames) != count($new_filenames)) $is_new_order_equal_old_order = "";
        }

        // その３ 添付ファイルの数の比較が同じ場合、添付ファイルの内容の比較
        if ($is_new_order_equal_old_order){
            for ($i = 0; $i < count($old_filenames); $i++){
                $fsize_old = filesize($old_filenames[$i]);
                $fsize_new = filesize($new_filenames[$i]);

                if (!$fsize_old || !$fsize_new) {
                    $is_new_order_equal_old_order = ""; //差異ありと判断
                    break;
                }

                $fp_old = fopen($old_filenames[$i], "r");
                $fp_new = fopen($new_filenames[$i], "r");

                if (!$fp_old || !$fp_new) {
                    $is_new_order_equal_old_order = ""; //差異ありと判断
                    break;
                }

                $fstream_old = fread($fp_old, $fsize_old);
                $fstream_new = fread($fp_new, $fsize_new);

                if ($fstream_old != $fstream_new){
                    $is_new_order_equal_old_order = ""; //差異ありと判断
                    break;
                }
            }
        }

        // その４ 添付ファイルの内容も同じだが、そもそもログインユーザは差し戻されている場合
        if ($is_new_order_equal_old_order){
            $sql =
                " select count(*) from sum_apply_sasimodosi_target".
                " where apply_id = " .$target_apply_id.
                " and to_emp_id = '" .$login_emp_id."'".
                " and to_apv_order = " .($exec_mode == "apply" ? 0 : $apv_order_fix).
                " and update_ymdhm is null";
            $sel1 = select_from_table($con, $sql, "", $fname);
            if (pg_fetch_result($sel1, 0, 0) > 0){
                $is_new_order_equal_old_order = ""; //差異ありと判断
            }
        }
    }




    // 送信モードか、受信モードだけど他者編集可能
    // かつ、履歴でない場合は以下を実行
    if (!$is_rireki_order_update_error){

        // 受信モードで（送信ではない）、しかし更新不可の場合は、承認ステータスのみ変更
        if ($exec_mode !="apply" && !$is_modifyable_by_other_emp){
            //==============================================
            // 他者が発行したが、自分が受付実施したオーダに、差戻しを受けていた場合は、修正済み日時をセット
            //==============================================
            $sql =
                " update sum_apply_sasimodosi_target set".
                " update_ymdhm = '".date("YmdHi")."'".
                " where apply_id = ".$apply_id.
                " and to_emp_id = '". pg_escape_string($login_emp_id)."'";
            $ret = update_set_table($con, $sql, array(), null, "", $fname);

            //==============================================
            // このオーダの代理送信者がログイン者の場合は、本来の送信者の「階層ゼロ番」へ、修正済み日時をセット
            //==============================================
            if ($row_apply["dairi_emp_id"] == $login_emp_id && !$row_apply["dairi_kakunin_ymdhms"]){
                $sql =
                    " update sum_apply_sasimodosi_target set".
                    " update_ymdhm = '".date("YmdHi")."'".
                    " where apply_id = ".$apply_id.
                    " and to_apv_order = 0".
                    " and to_emp_id = '". pg_escape_string($row_apply["emp_id"])."'";
                $ret = update_set_table($con, $sql, array(), null, "", $fname);
            }

            //==============================================
            // 自分が発行したオーダか、代理送信したオーダに、差戻しまたは否認を受けていた場合は、修正済み日時をセット
            // 「差戻し」の場合は、実質この更新は不要。「否認」の場合みが、update_ymdhmを参照している。
            //==============================================
            if ($row_apply["emp_id"] == $login_emp_id || ($row_apply["dairi_emp_id"] == $login_emp_id && !$row_apply["dairi_kakunin_ymdhms"])){
                $sql =
                    " update sum_apply_hinin_sasimodosi set".
                    " update_ymdhm = '".date("YmdHi")."'".
                    " where apply_id = ".$apply_id;
                $ret = update_set_table($con, $sql, array(), null, "", $fname);
            }
        }


        // 送信モードか、受信だけど更新可能の場合
        else {

            // 添付ファイル削除のため一時的に保持
            $old_apply_id = $apply_id;

            // オーダ変更があった場合にのみ、テンプレートを更新して履歴作成。のちにステータスを取り消す。
            // ステータスが変更される場合も作成。
            $is_status_changed = "";
            if ($row_apply["apply_stat"] == "0" && $command_mode != "update") $is_status_changed = 1;
            if ($row_apply["apply_stat"] == "1" && $command_mode != "update_and_resend") $is_status_changed = 1;
            if ($row_apply["apply_stat"] == "2" && $command_mode != "cyusi") $is_status_changed = 1;
            if (!$is_new_order_equal_old_order || $is_status_changed){

                // 送信テンプレート内容の更新を行い、履歴を作成する。戻り値は新しいapply_id
                $apply_id = apply_generate_revision(
                    $con, $fname, $command_mode, $apply_id, $login_emp_id,
                    $summary_id, $summary_seq, $ptif_id, $row_apply["wkfw_id"],
                    0, 0, "yes", 0, 0, @$_REQUEST["filename"], @$_REQUEST["tmpl_id"]
                );

                //==========================
                // 「受信時」の「稟議回覧」なら、現在開いている階層を含め、これ以降の階層は、承認済みデータフラグを取り消す。
                //==========================
                if ($exec_mode != "apply" && (int)$wkfw_appr=="2"){
                    $sql =
                        " update sum_applyapv set ".
                        " apv_stat_accepted = 0, apv_stat_operated = 0".
                        ",apv_date_accepted = null, apv_date_operated = null".
                        ",apv_stat_accepted_by_other = 0, apv_stat_operated_by_other = 0".
                        " where apply_id = ".(int)$apply_id.
                        " and apv_order >= ". $apv_order_fix;
                    $upd = update_set_table($con, $sql, array(), array(), "", $fname);
                    // 受信押印可能階層を現在の階層マイナス1にする。
                    $sql = "update sum_apply set visibility_apv_order = ".max(1, $apv_order_fix)." where apply_id = ".(int)$apply_id;
                    $upd = update_set_table($con, $sql, array(), array(), "", $fname);
                }
                //==========================
                // 「送信時」か「受信時の同報」なら、すべての承認済みを取り消す。
                //==========================
                if ($exec_mode == "apply" || (int)$wkfw_appr!="2"){
                    $sql =
                        " update sum_applyapv set ".
                        " apv_stat_accepted = 0, apv_stat_operated = 0".
                        ",apv_date_accepted = null, apv_date_operated = null".
                        ",apv_stat_accepted_by_other = 0, apv_stat_operated_by_other = 0".
                        " where apply_id = ".(int)$apply_id;
                    $upd = update_set_table($con, $sql, array(), array(), "", $fname);

                    // 受信押印可能階層を1にする。
                    if ($exec_mode == "apply"){
                        $sql = "update sum_apply set visibility_apv_order = 1 where apply_id = ".(int)$apply_id;
                        $upd = update_set_table($con, $sql, array(), array(), "", $fname);
                    }
                }
            }

            $target_apply_id = $apply_id;

            // 添付ファイルの移動。この時点で添付ファイルは「/summary/tmp/」に存在する。
            // ファイル名に新しい申請IDをつけたものを作成する。「中止送信」でも作成する。
            $attach_filenames = (array)@$_REQUEST["filename"];
            $attach_file_ids = (array)@$_REQUEST["file_id"];
            foreach (glob("summary/attach/".$old_apply_id."_*.*") as $fpath) unlink($fpath); // １）旧申請IDを持つファイルがあれば削除
            foreach (glob("summary/attach/".$apply_id."_*.*") as $fpath) unlink($fpath); // ２）新申請IDを持つファイルがあれば削除
            // ３）summary/tmpにあるファイルを、/summary/attach/に正規版として格納
            if (is_array($attach_filenames)){
                for ($i = 0; $i < count($attach_filenames); $i++) {
                    $ext = strrchr($attach_filenames[$i], ".");
                    copy("summary/tmp/".$_REQUEST["session"]."_".$attach_file_ids[$i].$ext, "summary/attach/".$apply_id."_".($i+1).$ext);
                }
            }
            // ４）summary/tmpにあるファイルを削除
            foreach (glob("summary/tmp/".$_REQUEST["session"]."_*.*") as $fpath) unlink($fpath);

        }
        // 画面を閉じる
        $goto_close_self_window = 1;
        $goto_opener_reload = 1;
    }
}

//====================================================================
// 画面表示値の取得
//====================================================================

// 申請テーブル取得
$rs_apply = select_from_table($con, "select * from sum_apply where apply_id = " . $target_apply_id, "", $fname);
$row_apply = pg_fetch_array($rs_apply);
$summary_id = $row_apply["summary_id"];
$ptif_id =    $row_apply["ptif_id"];
$apply_date_ymd = substr($row_apply["apply_date"], 0, 8);

// サマリテーブル取得
$rs_summary = select_from_table($con, "select * from summary where summary_id = ".$summary_id." and ptif_id = '".$ptif_id."'", "", $fname);
$row_summary = pg_fetch_array($rs_summary);
$summary_seq = $row_summary["summary_seq"];

// ワークフローマスタ取得
$rs_wkfwmst = select_from_table($con, "select * from sum_wkfwmst where wkfw_id = ".(int)$row_apply["wkfw_id"], "", $fname);
$row_wkfwmst = @pg_fetch_array($rs_wkfwmst);
$is_modifyable_by_other_emp = $row_wkfwmst["is_modifyable_by_other_emp"];

// テンプレートマスタ取得
$rs_tmplmst = select_from_table($con, "select * from tmplmst where tmpl_id = ".(int)@$row_wkfwmst["tmpl_id"], "", $fname);
$row_tmplmst = @pg_fetch_array($rs_tmplmst);

// このオーダが、ログイン者により代理送信した指示なら"1"
$is_dairi_emp = 0;
if ($login_emp_id != $row_apply["emp_id"] && $login_emp_id == $row_apply["dairi_emp_id"] && !$row_apply["dairi_kakunin_ymdhms"]) $is_dairi_emp = 1;

// 送信モード(送信一覧画面からの遷移または温度板画面からの遷移)か、受信モード(受信一覧画面からの遷移またはワークシートからの遷移）を判断
$sousin_or_jusin = "受信";
if (($login_emp_id == $row_apply["emp_id"] || $is_dairi_emp) && $exec_mode=="apply") $sousin_or_jusin = "送信";
$is_template_readonly = ($sousin_or_jusin=="受信" ? "yes": "");




//====================================================================
// 受信完了時、送信モードで送信者（オーダ発行者）が閲覧した場合は、オーダを閉める
//====================================================================
if ($sousin_or_jusin == "送信" && !$is_rireki_order_update_error){
    if ($login_emp_id == $row_apply["emp_id"]){
        if ($row_apply["is_approve_complete"] && !$row_apply["approve_complete_check_ymdhms"]){
            $sql =
                " update sum_apply set approve_complete_check_ymdhms = '".date("YmdHi")."'".
                " where apply_id = " . (int)$apply_id;
            $upd = update_set_table($con, $sql, array(), array(), "", $fname);
            $goto_opener_reload = 1;
        }
    }
}





//====================================================================
// 受信用に、ステータス変更を行う。
//====================================================================
if ($command_mode == "update_and_resend" && $sousin_or_jusin == "受信" && !$is_rireki_order_update_error){
    pg_query($con, "begin");// トランザクションを開始

    //==============================
    // コメントの更新
    //==============================
    $sql =
        " update sum_applyapv set".
        " apv_comment = '" . pg_escape_string($apv_comment)."'".
        " where apply_id = " . $apply_id.
        " and apv_order = ". $apv_order_fix.
        " and apv_sub_order " . ($apv_sub_order_fix ? " = ". $apv_sub_order_fix : "is null");
    $upd = update_set_table($con, $sql, array(), null, "", $fname);


    //==============================
    // 承認処理 ($approveA：受付、$approveB：実施)
    // もし未実施でない場合、未受付なら受付したことにする
    //==============================
    $approveA = @$_REQUEST["approveA"];
    $approveB = @$_REQUEST["approveB"];
    if (!@$approveA && @$approveB) $approveA = "1";

    $hinin_or_sasimodosi = "";
    if ($approveA=="1" || $approveB=="1") $hinin_or_sasimodosi = "uketukejissi";
    if ($approveA=="2" || $approveB=="2") $hinin_or_sasimodosi = "hinin";
    if ($approveA=="3" || $approveB=="3") $hinin_or_sasimodosi = "sasimodosi";


    //==============================
    // 否認差し戻しを一旦クリア
    //==============================
    $sql =
        " delete from sum_apply_hinin_sasimodosi".
        " where apply_id = ". $apply_id.
        " and from_emp_id = '" . pg_escape_string($login_emp_id) ."'".
        " and from_apv_order = " . $apv_order_fix;
    $upd = update_set_table($con, $sql, array(), null, "", $fname);

    $sql =
        " delete from sum_apply_sasimodosi_target".
        " where apply_id = ". $apply_id.
        " and from_emp_id = '" . pg_escape_string($login_emp_id) ."'".
        " and from_apv_order = " . $apv_order_fix;
    $upd = update_set_table($con, $sql, array(), null, "", $fname);


    //==============================
    // 否認か差戻しの場合は、否認差戻しテーブルに登録
    //==============================
    if ($hinin_or_sasimodosi!="uketukejissi"){
        $sql =
            " insert into sum_apply_hinin_sasimodosi (".
            "   apply_id".
            "  ,hinin_or_sasimodosi".
            "  ,from_emp_id".
            "  ,from_apv_order".
            "  ,from_apply_stat_accepted".
            "  ,from_apply_stat_operated".
            "  ,summary_seq".
            "  ,ptif_id".
            "  ,regist_ymdhm".
            "  ,update_ymdhm".
            " ) values (".
            "   ".$apply_id.
            "  ,'".$hinin_or_sasimodosi."'".
            "  ,'".pg_escape_string($login_emp_id)."'".
            "  ,".$apv_order_fix.
            "  ,".(($approveA=="2"||$approveA=="3") ? 1 : 0).
            "  ,".(($approveB=="2"||$approveB=="3") ? 1 : 0).
            "  ,".$summary_seq.
            "  ,'".$ptif_id."'".
            "  ,'".date("YmdHi")."'".
            "  ,null".
            " )";
        $upd = update_set_table($con, $sql, array(), null, "", $fname);
    }

    //==============================
    // 差戻しの場合は差戻し対象者テーブルに登録
    //==============================
    if ($hinin_or_sasimodosi=="sasimodosi"){
        $sasimodosi_targets = array();
        foreach($_REQUEST as $key => $to_emp_id){
            // POSTにて、
            // "approve_order_X_sasimodosi_Z" = "emp_id"
            // "approve_order_X_Y_sasimodosi_Z" = "emp_id"
            //   の形式のパラメータがくる。
            // X: 階層(apv_order)
            // Y: 階層内職員番号(apv_sub_order)
            // Z: "a"(受付) か "b"(実施)
            // 例外的に、「送信者への差戻し通知」送信者のIDが渡ってくる。(代理送信者は不要)
            // "approve_order_0_sasimodosi_a"が渡ってくる。「階層ゼロ」「受付」として登録。
            if (!trim($to_emp_id)) continue;
            if (substr($key, 0, 13) != "approve_order") continue;
            $p = strpos($key, "_sasimodosi_");
            if ($p===FALSE) continue;
            $aorb = substr($key, $p+12, 1);
            $k = str_replace("approve_order", "", $key);
            $k = str_replace("_sasimodosi_".$aorb, "", $k);
            $orderstr = explode("_", $k."_");
            $to_apv_order = (int)$orderstr[0];

            $sql =
                " insert into sum_apply_sasimodosi_target (".
                "   apply_id".
                "  ,from_emp_id".
                "  ,from_apv_order".
                "  ,from_apply_stat_accepted".
                "  ,from_apply_stat_operated".
                "  ,to_emp_id".
                "  ,to_apv_order".
                "  ,to_apply_stat_accepted".
                "  ,to_apply_stat_operated".
                "  ,summary_seq".
                "  ,ptif_id".
                "  ,regist_ymdhm".
                "  ,update_ymdhm".
                " ) values (".
                "   ".$apply_id.
                "  ,'".pg_escape_string($login_emp_id)."'".
                "  ,".$apv_order_fix.
                "  ,".(($approveA=="3") ? 1 : 0).
                "  ,".(($approveB=="3") ? 1 : 0).
                "  ,'".pg_escape_string($to_emp_id)."'".
                "  ,".$to_apv_order.
                "  ,".($aorb=="a" ? 1 : 0).
                "  ,".($aorb=="b" ? 1 : 0).
                "  ,".$summary_seq.
                "  ,'".$ptif_id."'".
                "  ,'".date("YmdHi")."'".
                "  ,null".
                " )";
            $upd = update_set_table($con, $sql, array(), null, "", $fname);
        }
    }
    //==============================
    // （否認とか差戻しでなく）受付実施か解除の場合は、受信、実施ステータスの更新
    //  否認か差戻しの場合は、受付済実施済を戻す
    //==============================
    if ($approveA!="" || $approveB!=""){
        $sql = " update sum_applyapv set apply_id = ". $apply_id;
        if ($approveA!="") $sql .= ",apv_stat_accepted = ".($approveA=="1"?1:0).",apv_date_accepted = " . date("YmdHi");
        if ($approveB!="") $sql .= ",apv_stat_operated = ".($approveB=="1"?1:0).",apv_date_operated = " . date("YmdHi");
        $sql .=
            " where apply_id = ". $apply_id ." and apv_order = ". $apv_order_fix;
        if ($apv_sub_order_fix) $sql .= " and apv_sub_order = ". $apv_sub_order_fix;
        else $sql .= " and apv_sub_order is null";
        $upd = update_set_table($con, $sql, array(), null, "", $fname);
    }

    //==============================
    // 受信押印可能階層を変更
    //==============================
    $obj->approve_application($apply_id, $apv_order_fix, $next_notice_div, $next_notice_recv_div, $session, (int)$wkfw_appr);

    pg_query($con, "commit");// トランザクションをコミット
    $goto_opener_reload = 1;
    $goto_close_self_window = 1;
}


// 受信対象者情報取得
$sql =
    " select a.*, b.emp_lt_nm, b.emp_ft_nm, c.st_nm".
    ",d.pjt_name as parent_pjt_name, f.pjt_name as child_pjt_name, g.group_nm".
    " from sum_applyapv a".
    " left join empmst b on a.emp_id = b.emp_id".
    " left join stmst c on a.emp_st = c.st_id".
    " left join project d on a.parent_pjt_id = d.pjt_id".
    " left join project f on a.child_pjt_id = f.pjt_id".
    " left join comgroupmst g on g.group_id = a.apv_common_group_id".
    " where a.apply_id = ".$target_apply_id." order by a.apv_order, a.apv_sub_order asc";
$sel = select_from_table($con, $sql, "", $fname);
$rows_applyapv = (array)pg_fetch_all($sel);


$sql =
    " select max(apv_order) from sum_applyapv".
    " where apply_id = ".$apply_id.
    " and (apv_stat_accepted = '1' or apv_stat_operated = '1')";
$rs_apply1 = select_from_table($con, $sql, "", $fname);
$max_apv_order_fix = max(1, (int)@pg_fetch_result($rs_apply1, 0, 0));




//====================================================================
// 受信時、承認シーケンスが指定されていなければ、
// 編集可能階層であれば、ログインユーザが最後に承認した階層をセレクション
//====================================================================
if (!$apv_order_fix){
    $apv_order_fix = 0;
    $apv_sub_order_fix = 0;
    if ($sousin_or_jusin == "受信"){
        foreach($rows_applyapv as $applyapv){
            if ($login_emp_id != $applyapv["emp_id"]) continue; // ログイン者でない場合は無視
            if ($applyapv["apv_order"] > $row_apply["visibility_apv_order"]) continue; // 承認可能階層より以降の階層は選択できない
            if ($row_wkfwmst["wkfw_appr"]=="2" && $applyapv["apv_order"] < $max_apv_order_fix) continue; // 稟議回覧なら承認済みより以前の階層は選択不可
            if ($apv_order_fix < $applyapv["apv_order"]){
                $apv_order_fix = $applyapv["apv_order"];
            }
        }
    }
}

if (!$apv_sub_order_fix){
    $apv_sub_order_fix = 0;
    if ($sousin_or_jusin == "受信"){
        foreach($rows_applyapv as $applyapv){
            if ($applyapv["apv_order"] != $apv_order_fix) continue; // 現在の階層でなければスルー
            if ($login_emp_id != $applyapv["emp_id"]) continue; // ログイン者でない場合は無視
            if ($apv_sub_order_fix < $applyapv["apv_sub_order"]){
                $apv_sub_order_fix = $applyapv["apv_sub_order"];
            }
        }
    }
}


// 否認差戻しテーブル（誰かがこのオーダに、否認または差し戻ししたか）
$sql = "select * from sum_apply_hinin_sasimodosi where apply_id = ". $target_apply_id;
$rs_hinin_sasimodosi = select_from_table($con, $sql, "", $fname);
$rows_hinin_sasimodosi = pg_fetch_all($rs_hinin_sasimodosi);

// 差戻し対象テーブル（誰かが、誰に対して、差戻しをかけたか）
// 差戻しをかけた後、修正したなら取得しない。(update_ymdhmがヌルのもの）
$sql =
    " select from_emp_id, from_apv_order, to_emp_id, to_apv_order, to_apply_stat_accepted as m1, to_apply_stat_operated as m2".
    " from sum_apply_sasimodosi_target".
    " where apply_id = " .$target_apply_id.
    " and update_ymdhm is null";
$sel1 = select_from_table($con, $sql, "", $fname);
$rows_sasimodosare = pg_fetch_all($sel1);
if (!$rows_sasimodosare[0]) $rows_sasimodosare = array();


// ログイン者は誰かに差し戻され、そののち修正していないか
$is_login_user_sasimodosare = 0;
$is_sousin_user_sasimodosare = 0;
if (is_array($rows_sasimodosare)){
    $check_kaisou = $apv_order_fix;
    if ($sousin_or_jusin == "送信") $check_kaisou = 0;
    foreach ($rows_sasimodosare as $tkey => $trow){
        if ($trow["to_emp_id"] != $login_emp_id) continue;
        if ($trow["to_apv_order"] != $check_kaisou) continue;
        if ($trow["m1"] || $trow["m2"]) {
            if ($check_kaisou==0) $is_sousin_user_sasimodosare = 1;
            else $is_login_user_sasimodosare = 1;
        }
    }
}

// 送信者(オーダ発行者)は、ログインユーザに差し戻され、そののち修正していないか
// チェックボックスの初期値をゼロとしたいため、
// 「 他者を差戻ししたがオーダ発行者に対しては差し戻していない」状態を取得する。
$is_sousin_user_sasimodosare_checkbox = "checked";
$is_login_user_sasimodosi_sita = 0;

// (その1) ログイン者は、このオーダを差し戻したか
if (is_array($rows_hinin_sasimodosi)){
    foreach ($rows_hinin_sasimodosi as $tkey => $trow){
        if ($trow["hinin_or_sasimodosi"]!="sasimodosi") continue;
        if ($trow["from_emp_id"] != $login_emp_id) continue;
        if ($trow["from_apv_order"] != $apv_order_fix) continue;
        $is_login_user_sasimodosi_sita = 1;
        $is_sousin_user_sasimodosare_checkbox = "";
    }
}

// (その2) ログイン者は、送信者を差し戻したか
if ($is_sousin_user_sasimodosare_checkbox==""){
    $sql =
        " select count(*) from sum_apply_sasimodosi_target".
        " where apply_id = " .$target_apply_id.
        " and update_ymdhm is null".
        " and from_apv_order = ".$apv_order_fix. // この階層からの送信
        " and from_emp_id = '".pg_escape_string($login_emp_id)."'". // ログイン者による送信
        " and to_apv_order = 0"; // ゼロ階層(オーダ発行者の階層)への送信
    $sel1 = select_from_table($con, $sql, "", $fname);
    if (pg_fetch_result($sel1, 0, 0) > 0){
        $is_sousin_user_sasimodosare_checkbox = "checked";
    }
}

// オーダはすべて完了したか
$is_all_approve_complete = ($row_apply["is_approve_complete"] && $row_apply["approve_complete_check_ymdhms"]);

// ログイン者は押印したか
$is_login_user_ouinzumi = 0;


for($i = 0; $i < count($rows_applyapv); $i++){

    // 押印欄の職種部分の文字列
    $st_div = $rows_applyapv[$i]["st_div"];
    if ($st_div == "5") { $rows_applyapv[$i]["display_caption"] = "病棟担当"; continue; }
    if ($st_div == "6") { $rows_applyapv[$i]["display_caption"] = $rows_applyapv[$i]["group_nm"]; continue; }
    if ($st_div == "3") {
        $rows_applyapv[$i]["display_caption"] = $ary[$i]["parent_pjt_name"];
        if ($rows_applyapv[$i]["child_pjt_name"] != "") $rows_applyapv[$i]["display_caption"] .= " > ".$rows_applyapv[$i]["child_pjt_name"];
    }


    // ログイン者は押印したか
    if ($rows_applyapv[$i]["apv_order"] == $apv_order_fix){
        if ($rows_applyapv[$i]["emp_id"]==$login_emp_id){
            if ($rows_applyapv[$i]["apv_stat_accepted"]==1){
                if ($rows_applyapv[$i]["apv_stat_operated"]==1 || $rows_applyapv[$i]["next_notice_recv_div"]!="2"){ // 実施押印済みか、そもそも実施では無いか
                    $is_login_user_ouinzumi = 1;
                }
            }
        }
    }

    // リスト内受信者はログインユーザに差し戻されたのち、修正していないか
    if (is_array($rows_sasimodosare)){
        foreach ($rows_sasimodosare as $tkey => $trow){
            if ($trow["from_emp_id"] != $login_emp_id) continue;
            if ($trow["from_apv_order"] != $apv_order_fix) continue;
            if ($trow["to_emp_id"] != $rows_applyapv[$i]["emp_id"]) continue;
            if ($trow["to_apv_order"] != $rows_applyapv[$i]["apv_order"]) continue;
            $rows_applyapv[$i]["apply_stat_accepted_sasimodosare"] = $trow["m1"] ? "sasimodosi" : "";
            $rows_applyapv[$i]["apply_stat_operated_sasimodosare"] = $trow["m2"] ? "sasimodosi" : "";
        }
    }

    if (is_array($rows_hinin_sasimodosi)){
        foreach ($rows_hinin_sasimodosi as $tkey => $trow){

            // リスト内受信者は、誰かに差戻しをかけたか
            if ($trow["hinin_or_sasimodosi"]=="sasimodosi"){
                if ($trow["from_emp_id"] == $rows_applyapv[$i]["emp_id"] && $trow["from_apv_order"] == $rows_applyapv[$i]["apv_order"]) {
                    if ($trow["from_apply_stat_accepted"]){
                        $rows_applyapv[$i]["apv_stat_accepted"] = "3";
                        $rows_applyapv[$i]["apv_date_accepted"] = $trow["regist_ymdhm"];
                    }
                    if ($trow["from_apply_stat_operated"]){
                        $rows_applyapv[$i]["apv_stat_operated"] = "3";
                        $rows_applyapv[$i]["apv_date_operated"] = $trow["regist_ymdhm"];
                    }
                }
            }
            // リスト内受信者は、誰かに否認をかけたか
            else {
                if ($trow["from_emp_id"] == $rows_applyapv[$i]["emp_id"] && $trow["from_apv_order"] == $rows_applyapv[$i]["apv_order"]){
                    if ($trow["from_apply_stat_accepted"]){
                        $rows_applyapv[$i]["apv_stat_accepted"] = "2";
                        $rows_applyapv[$i]["apv_date_accepted"] = $trow["regist_ymdhm"];
                    }
                    if ($trow["from_apply_stat_operated"]){
                        $rows_applyapv[$i]["apv_stat_operated"] = "2";
                        $rows_applyapv[$i]["apv_date_operated"] = $trow["regist_ymdhm"];
                    }
                }
            }
        }
    }
    $rows_applyapv[$i]["display_caption"] = $rows_applyapv[$i]["st_nm"];
}


$imprint_flg = get_imprint_flg($con, $login_emp_id, $fname);

$imgsrc_accepted = "img/accepted.gif";
if ($imprint_flg == "t") {
    $imgsrc_accepted = "sum_application_imprint_image.php?session=".$session."&emp_id=".$login_emp_id."&apv_flg=1&imprint_flg=".$imprint_flg."&t=".date('YmdHis') . "&approve_label=" . urlencode("受信");
}

$dirname = "summary/imprint/";
$img_file_accepted = "img/accepted.gif";
if ($imprint_flg == "t") {
    $img_file1 = $dirname.$login_emp_id."_accepted.gif";
    $img_file2 = $dirname.$login_emp_id."_accepted.jpg";
    // 当該職員の画像が登録済みか確認
    if (is_file($img_file1)) {
        $img_file_accepted = $img_file1;
    } else if (is_file($img_file2)) {
        $img_file_accepted = $img_file2;
    }
}

$imgsrc_operated = "img/operated.gif";
if ($imprint_flg == "t") {
    $imgsrc_operated = "sum_application_imprint_image.php?session=".$session."&emp_id=".$login_emp_id."&apv_flg=4&imprint_flg=".$imprint_flg."&t=".date('YmdHis') . "&approve_label=" . urlencode("受信");
}

$img_file_operated = "img/operated.gif";
if ($imprint_flg == "t") {
    $img_file1 = $dirname.$login_emp_id."_operated.gif";
    $img_file2 = $dirname.$login_emp_id."_operated.jpg";
    // 当該職員の画像が登録済みか確認
    if (is_file($img_file1)) {
        $img_file_operated = $img_file1;
    } else if (is_file($img_file2)) {
        $img_file_operated = $img_file2;
    }
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<script type="text/javascript" src="js/prototype/prototype-1.7.1.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/event/event-min.js" ></script>
<script type="text/javascript" src="js/yui_0.12.2/build/dom/dom-min.js" ></script>
<script type="text/javascript" src="js/yui/3.10.3/build/yui/yui-min.js"></script>
<title>CoMedix <?=get_report_menu_label($con, $fname)?> | <?=$sousin_or_jusin?>オーダ詳細</title>

<? //==================================================================== ?>
<? // JavaScript                                                          ?>
<? //==================================================================== ?>
<script type="text/javascript">

<? //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ?>
<? // 親画面リロードと画面クローズ                                        ?>
<? //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ?>
<? if ($is_rireki_order_update_error){ ?>
    alert("更新しようとした指示は、過去履歴です。\n他者により更新された可能性があります。再度設定してください。");
<? } ?>
<? if ($goto_opener_reload || $is_rireki_order_update_error){ ?>
    // IEでたまにエラーになるためtry〜catch
    try {
        if (window.opener && !window.opener.closed){
            if (window.opener.reload_page) window.opener.reload_page();
            else window.opener.location.reload();
        }
    } catch (e) {
    }
<? } ?>
<? if ($goto_close_self_window){ ?>
    window.close();
<? } ?>



<? //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ?>
<? // ドラッグ押印画像の定義                                              ?>
<? //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ?>

<? list($img_accepted_w, $img_accepted_h) = get_imprint_imagesize($img_file_accepted); ?>
<? list($img_operated_w, $img_operated_h) = get_imprint_imagesize($img_file_operated); ?>
<? list($img_hinin_w, $img_hinin_h) = get_imprint_imagesize("img/approve_ng.gif"); ?>
<? list($img_sasimodosi_w, $img_sasimodosi_h) = get_imprint_imagesize("img/returned.gif"); ?>

var img_accepted_w = <?=$img_accepted_w?>; var img_accepted_h = <?=$img_accepted_h?>;
var img_operated_w = <?=$img_operated_w?>; var img_operated_h = <?=$img_operated_h?>;
var img_hinin_w = <?=$img_hinin_w?>; var img_hinin_h = <?=$img_hinin_h?>;
var img_sasimodosi_w = <?=$img_sasimodosi_w?>; var img_sasimodosi_h = <?=$img_sasimodosi_h?>;

var spacer_imgA = new Image(60, 60);
spacer_imgA.src = 'img/spacer.gif';
var spacer_imgB = new Image(60, 60);
spacer_imgB.src = 'img/spacer.gif';

var accepted_img = new Image(img_accepted_w, img_accepted_h);
accepted_img.src = '<? echo($imgsrc_accepted); ?>';
var operated_img = new Image(img_operated_w, img_operated_h);
operated_img.src = '<? echo($imgsrc_operated); ?>';

var hinin_img = new Image(img_hinin_w, img_hinin_h);
hinin_img.src = 'img/approve_ng.gif';
var sasimodosi_img = new Image(img_sasimodosi_w, img_sasimodosi_h);
sasimodosi_img.src = 'img/returned.gif';


<? //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ?>
<? // ドラッグ押印画像の初期化                                            ?>
<? //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ?>
var applyA1_pos, applyA2_pos, applyA3_pos, applyB1_pos, applyB2_pos, applyB3_pos;
function initPage() {
    var ddA1 = new Y.DD.Drag({node: '#applyA1'}).plug(Y.Plugin.DDWinScroll);
    ddA1.dragEnd = function(e) {
        var el = document.getElementById('applyA1');
        YAHOO.util.Dom.setXY(el, this.startPos);
        el.style.backgroundColor = '';
        el.style.zIndex = 0;
    };
    ddA1.startPos = YAHOO.util.Dom.getXY("applyA1");
    applyA1_pos = ddA1.startPos;
    ddA1.on('drag:start', function(e) {
        document.getElementById('applyA1').style.zIndex = 999;
    });
    ddA1.on('drag:drophit', function(e) {
        if (e.drop.get('node').get('id') == 'targetA') {
            set_target(1, 'targetA');
        } else {
            ddA1.dragEnd();
        }
    });
    ddA1.on('drag:dropmiss', ddA1.dragEnd);

    var ddA2 = new Y.DD.Drag({node: '#applyA2'}).plug(Y.Plugin.DDWinScroll);
    ddA2.dragEnd = function(e) {
        var el = document.getElementById('applyA2');
        YAHOO.util.Dom.setXY(el, this.startPos);
        el.style.backgroundColor = '';
        el.style.zIndex = 0;
    };
    ddA2.startPos = YAHOO.util.Dom.getXY("applyA2");
    applyA2_pos = ddA2.startPos;
    ddA2.on('drag:start', function(e) {
        document.getElementById('applyA2').style.zIndex = 999;
    });
    ddA2.on('drag:drophit', function(e) {
        set_target(2, e.drop.get('node').get('id'));
        ddA2.dragEnd();
    });
    ddA2.on('drag:dropmiss', ddA2.dragEnd);

    var ddA3 = new Y.DD.Drag({node: '#applyA3'}).plug(Y.Plugin.DDWinScroll);
    ddA3.dragEnd = function(e) {
        var el = document.getElementById('applyA3');
        YAHOO.util.Dom.setXY(el, this.startPos);
        el.style.backgroundColor = '';
        el.style.zIndex = 0;
    };
    ddA3.startPos = YAHOO.util.Dom.getXY("applyA3");
    applyA3_pos = ddA3.startPos;
    ddA3.on('drag:start', function(e) {
        document.getElementById('applyA3').style.zIndex = 999;
    });
    ddA3.on('drag:drophit', function(e) {
        set_target(3, e.drop.get('node').get('id'));
        ddA3.dragEnd();
    });
    ddA3.on('drag:dropmiss', ddA3.dragEnd);

    var ddB1 = new Y.DD.Drag({node: '#applyB1'}).plug(Y.Plugin.DDWinScroll);
    ddB1.dragEnd = function(e) {
        var el = document.getElementById('applyB1');
        YAHOO.util.Dom.setXY(el, this.startPos);
        el.style.backgroundColor = '';
        el.style.zIndex = 0;
    };
    ddB1.startPos = YAHOO.util.Dom.getXY("applyB1");
    applyB1_pos = ddB1.startPos;
    ddB1.on('drag:start', function(e) {
        document.getElementById('applyB1').style.zIndex = 999;
    });
    ddB1.on('drag:drophit', function(e) {
        if (e.drop.get('node').get('id') == 'targetB') {
            set_target(1, 'targetB');
        } else {
            ddB1.dragEnd();
        }
    });
    ddB1.on('drag:dropmiss', ddB1.dragEnd);

    if (document.getElementById('targetA')) {
        new Y.DD.Drop({node: '#targetA'});
    }
    if (document.getElementById('targetB')) {
        new Y.DD.Drop({node: '#targetB'});
    }
}


<? //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ?>
<? // 押印のドラッグ＆ドロップイベント                                    ?>
<? //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ?>
function set_target(imprint_idx, id) {
    var flg = imprint_idx;
    var aorb = "";
    if (id == "targetA") aorb = "A";
    if (id == "targetB") aorb = "B";

    // 全部元に戻す
    reset_imprint(aorb=="A", aorb=="B");

    if (flg != "0"){

        // ターゲットに画像を表示
        var imgsrc = "";
        if (aorb=="A"){
            if (flg == 1) {
                imgsrc = "<?=$imgsrc_accepted?>";
                document.getElementById("apply"+aorb+flg).style.display = "none";
                document.getElementById("img"+aorb).style.width = img_accepted_w + "px";
                document.getElementById("img"+aorb).style.height = img_accepted_w + "px";
            } else if (flg == 2) {
                imgsrc = "img/approve_ng.gif";
                document.getElementById("img"+aorb).style.width = img_hinin_w + "px";
                document.getElementById("img"+aorb).style.height = img_hinin_h + "px";
            } else if (flg == 3) {
                imgsrc = "img/returned.gif";
                document.getElementById("img"+aorb).style.width = img_sasimodosi_w + "px";
                document.getElementById("img"+aorb).style.height = img_sasimodosi_h + "px";
            }
        }
        else if (aorb=="B"){
            if (flg == 1) {
                imgsrc = "<?=$imgsrc_operated?>";
                document.getElementById("apply"+aorb+flg).style.display = "none";
                document.getElementById("img"+aorb).style.width = img_operated_w + "px";
                document.getElementById("img"+aorb).style.height = img_operated_h + "px";
            } else if (flg == 2) {
                imgsrc = "img/approve_ng.gif";
                document.getElementById("img"+aorb).style.width = img_hinin_w + "px";
                document.getElementById("img"+aorb).style.height = img_hinin_h + "px";
            } else if (flg == 3) {
                imgsrc = "img/returned.gif";
                document.getElementById("img"+aorb).style.width = img_sasimodosi_w + "px";
                document.getElementById("img"+aorb).style.height = img_sasimodosi_h + "px";
            }
        }
        document.getElementById("img"+aorb).src = imgsrc;
        // 背景を白に
        document.getElementById("target"+aorb).style.backgroundColor = "#ffffff";
        // ドロップダウンを設定
        document.getElementById("approve"+aorb).value = imprint_idx;
    }
    sasimodosi_operation(aorb, imprint_idx);
}


function sasimodosi_operation(aorb, imprint_idx){

    <?// A(受付)またはB(実施)のどちらを指定されたかを判別する。デフォルトはA ?>
    if (aorb==""){
        if (approve_ordersB) aorb = "B";
        else if (approve_ordersA) aorb = "A";
        else return;
    }

    <?// 差戻すことにしたら、sasimodosuをtrueに ?>
    var sasimodosu = (imprint_idx==3);
    document.getElementById("div_sasimodosi_msg").style.display = (sasimodosu ? "" : "none");

    if (document.getElementById("sousinsya_sasimodosi_check")){
        var html =
            '<span id="span_approve_order_0_sasimodosi_a"'+
            ' style="padding:3px; background-color:<?=$is_sousin_user_sasimodosare_checkbox ? "#b1f8b7" : "#e2fce4" ?>; border:1px solid #9bc8ec">'+
            '<label>'+
            '<input name="approve_order_0_sasimodosi_a" id="approve_order_0_sasimodosi_a" value="<?=$row_apply["emp_id"]?>" type="checkbox" <?=$is_sousin_user_sasimodosare_checkbox?>'+
            ' onclick="changeSasimodosiCheckbox(this)" />'+
            '送信者への差戻し通知'+
            '</label>'+
            '</span>';
        if (!sasimodosu) html = "";
        document.getElementById("sousinsya_sasimodosi_check").innerHTML = html;
    }

    var lists = {};
    lists[0] = approve_ordersA;
    lists[1] = approve_ordersB;
    for (var i = 0; i <= 1; i++){
        var aorb2 = (i==0) ? "a" : "b";
        if (aorb=="A" && sasimodosu && aorb2=="b") sasimodosu = false; // 受信差戻しを、「差戻し」にしたとき、実施差戻しは一旦クリアする
        // 各階層ごとに、セルチェック
        for (var apv_order in lists[i]) {
            // もし子階層つきでなければ（対象階層に一人か、展開していなければ）
            if (!lists[i][apv_order]["opened"]){
                // ログイン者の階層であり、ログイン者のセルなら、何もしない
                if (apv_order == "<?=$apv_order_fix?>" && lists[i][apv_order]["emp_id"] == "<?=$login_emp_id?>") continue;
                // 他者セルの差戻し処理
                set_sasimodosi(lists[i][apv_order], apv_order, 0, lists[i][apv_order]["apv_sub_order"], aorb2, sasimodosu);
            }
            // もし子階層つきなら（階層内を展開済みなら）
            else {
                for (var apv_sub_order in lists[i][apv_order]["sub_orders"]) {
                    // ログイン者の階層であり、ログイン者のセルなら、何もしない
                    if (apv_order == "<?=$apv_order_fix?>" && lists[i][apv_order]["sub_orders"][apv_sub_order]["emp_id"] == "<?=$login_emp_id?>") continue;
                    // 他者セルの差戻し処理
                    set_sasimodosi(lists[i][apv_order]["sub_orders"][apv_sub_order], apv_order, apv_sub_order, apv_sub_order, aorb2, sasimodosu);
                }
            }
        }
    }
}

function set_sasimodosi(obj, apv_order, apv_sub_order, span_apv_sub_order, aorb2, sasimodosu){
    id = 'approve_order'+ apv_order;
    if (apv_sub_order > 0) id += "_" + apv_sub_order;

    if (span_apv_sub_order=="") span_apv_sub_order = "0";
    var spanid = "span_parent_approve_order_" + aorb2 + "_" + apv_order + "_" + span_apv_sub_order;

    <?// 他者のセルの、受信状態が"1"(受信)でなければ何もしない ?>
    if (obj["cur_stat"]!="1") return 0;

    <?// とりあえず背景を薄い緑にする。差し戻さなければ白にする ?>
    document.getElementById(id+'_emps_'+aorb2).style.backgroundColor = (sasimodosu ? "#e2fce4" : "#ffffff");
    document.getElementById(id+'_no_'+aorb2).style.backgroundColor = (sasimodosu ? "#e2fce4" : "#eff9fd");
    document.getElementById(id+'_status_'+aorb2).style.backgroundColor = (sasimodosu ? "#e2fce4" : "#ffffff");

    <? // セル内の文字列を、普通の文字からドロップダウンに置換、またはドロップダウンから普通の文字に置換 ?>
    var cur_html = document.getElementById(spanid).innerHTML;
    var new_html = (aorb2=="a" ? "受付済" : "実施済");
    if (sasimodosu) {
        new_html =
            '<select name="'+id+'_sasimodosi_'+aorb2+'" id="'+id+'_sasimodosi_'+aorb2+'"'+
            ' onchange="changeSasimodosiDropdown(\''+id+'\', \''+aorb2+'\', this.value)">'+
            '<option value="">'+new_html+'</option>'+
            '<option value="'+obj["emp_id"]+'">差戻し通知</option></select>';
    }
    if (cur_html != new_html){
        document.getElementById(spanid).innerHTML = new_html;
    }

    // もし他者セルに「差戻し通知」が指定されていれば、他者セルのドロップダウンインデックスを「差戻し通知」にして、背景を濃いグリンに ?>
    if (obj["new_stat"]=="sasimodosi"){
        if (document.getElementById(id+'_sasimodosi_'+aorb2)){
            document.getElementById(id+'_sasimodosi_'+aorb2).selectedIndex = 1;
            document.getElementById(id+'_emps_'+aorb2).style.backgroundColor = "#b1f8b7";
            document.getElementById(id+'_no_'+aorb2).style.backgroundColor = "#b1f8b7";
            document.getElementById(id+'_status_'+aorb2).style.backgroundColor = "#b1f8b7";
        }
    }
    return 1;
}

function changeSasimodosiDropdown(id ,aorb2, dropdown_value){
    sasimodosiDropdownSetClear(id ,aorb2, "");
}
function changeSasimodosiCheckbox(checkbox){
    sasimodosiDropdownSetClear("approve_order_0", "a", "checkbox");
}

function sasimodosiDropdownSetClear(id, aorb, etype){
    var clr_off = "#e2fce4";
    var clr_on = "#b1f8b7";

    var elem = document.getElementById(id+"_sasimodosi_"+aorb);
    var is_checkbox_enable = (etype=="checkbox" && elem.checked);

    for (var i = 0, j = document.apply.elements.length; i < j; i++) {
        var id2 = document.apply.elements[i].id;
        if (id2.substring(0, 13) != "approve_order") continue;
        var pos = id2.indexOf("_sasimodosi_");
        var len = id2.length;
        if (pos == -1) continue;
        if (id2 == "approve_order_0_sasimodosi_a") {
            document.getElementById("span_"+id2).style.backgroundColor = (is_checkbox_enable ? clr_on : clr_off);
            if (!is_checkbox_enable) document.getElementById(id2).checked = false;
        } else {
            var id3 = id2.substring(13, pos);
            var aorb2 = id2.substring(pos + 12);
            var is_selected = (etype!="checkbox" && id==id2.substring(0, pos) && aorb2==aorb && document.getElementsByName(id2)[0].selectedIndex);
            document.getElementsByName(id2)[0].selectedIndex = (is_checkbox_enable || !is_selected ? 0 : 1);
            document.getElementById("approve_order"+id3+'_emps_'+aorb2).style.backgroundColor = (is_checkbox_enable || !is_selected ? clr_off : clr_on);
            document.getElementById("approve_order"+id3+'_no_'+aorb2).style.backgroundColor = (is_checkbox_enable || !is_selected ? clr_off : clr_on);
            document.getElementById("approve_order"+id3+'_status_'+aorb2).style.backgroundColor = (is_checkbox_enable || !is_selected ? clr_off : clr_on);
        }
    }
}

<? //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ?>
<? // 左側履歴領域の高さを調節                                            ?>
<? //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ?>
function resize_history_tbl() {
    var flg = false;
    heigh1 = document.getElementById('history_tbl').style.height;
    var imprint_height = 0;
    if (document.getElementById('div_imprint_area')) imprint_height = document.getElementById('div_imprint_area').offsetHeight;
    document.getElementById('history_tbl').style.height = (document.getElementById('dtl_tbl').offsetHeight - imprint_height - 12) + "px";
    heigh2 = document.getElementById('history_tbl').style.height;

    if(heigh1 != heigh2) flg = true;
    return flg;
}
<? //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ?>
<? // なんかFireFox対応とかで、再格納している。                           ?>
<? //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ?>
function updateDOM() {
    var inputElements = document.getElementsByTagName('input');
    for (var i = 0, j = inputElements.length; i < j; i++) {
        var inputElement = inputElements[i];
        switch (inputElement.type) {
        case 'hidden':
            inputElement.setAttribute('value', inputElement.value);
            break;
        case 'radio':
            if (inputElement.checked) {
                inputElement.setAttribute("checked","checked");
            } else {
                inputElement.removeAttribute("checked");
            }
            break;
        }
    }
}

<? //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ?>
<? // onload処理                                                          ?>
<? //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ?>
var Y;
function loaded(){
    Y = YUI();
    Y.use('dd-drag', 'dd-drop', 'dd-scroll', function(Y) {
        if (window.OnloadSub) { OnloadSub(); }
        if (window.refreshApproveOrdersA) {refreshApproveOrdersA();}
        if (window.refreshApproveOrdersB) {refreshApproveOrdersB();}
        resize_history_tbl();
        initPage();
        reset_imprint(true, true);
        init_imprint();
    });
}

function showLeftHistoryArea(isShow){
    document.getElementById("td_left_history_area").style.display = (isShow ? "" : "none");
}
function showAlternateLeftHistoryArea(){
    var d = document.getElementById("td_left_history_area").style.display;
    document.getElementById("td_left_history_area").style.display = (d=="none" ? "" : "none");
}
</script>
</head>
<body onload="loaded()">

<div style="padding:4px">


<!-- ヘッダ -->
<?= summary_common_show_dialog_header($sousin_or_jusin."オーダ詳細"); ?>


<table style="width:100%; margin-top:4px">
<tr>
    <td valign="top" width="260px" id="td_left_history_area">


        <? //==================================================================== ?>
        <? // 画面左側の履歴一覧                                                  ?>
        <? //==================================================================== ?>
        <table class="list_table">
            <tr style="height:22px"><th style="text-align:center; padding:2px">送信履歴</th></tr>
        </table>
        <table id="history_tbl" class="list_table">
        <tr>
        <td valign="top" style="vertical-align:top; padding:3px; line-height:1.5;">
            <table>
                <? $re_apply_id = (int)$apply_id; ?>
                <? $re = ""; ?>
                <? for(;;) { ?>
                <?   $sql = "select apply_id, apply_date, apply_stat from sum_apply where ".$re."apply_id = ".$re_apply_id; ?>
                <?   $selz = select_from_table($con, $sql, "", $fname); ?>
                <?   if(pg_numrows($selz) == 0) break; ?>
                <?   $row = pg_fetch_array($selz); ?>
                <?   $re_apply_id = (int)$row["apply_id"]; ?>
                <?   $aplydt = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $row["apply_date"]); ?>
                <?   if ($row["apply_stat"] == "0") $aplydt .= " (未送信)"; ?>
                <?   if ($row["apply_stat"] == "1") $aplydt .= " 送信"; ?>
                <?   if ($row["apply_stat"] == "2") $aplydt .= " 中止送信"; ?>
                <?   $re = "re_"; ?>
                <tr>
                <td style="height:20px;">
                    <? if($target_apply_id == $row["apply_id"]) { ?>
                        <img height="13" width="13" hspace="2" src="images/arrow_right_red.gif" style="vertical-align:middle;">
                    <? } ?>
                </td>
                <td style="padding-left:3px"><a href="#" class="always_blue" onclick="history_select('<?=$row["apply_id"]?>');">[<?=$aplydt?>]</a></td>
                </tr>
                <? } ?>
            </table>
        </td>
        </tr>
        </table>
        <script type="text/javascript">
            function history_select(target_apply_id) {
                location.href =
                    "sum_application_approve_detail.php"+
                    "?session=<?=$session?>"+
                    "&apv_order_fix=<?=$apv_order_fix?>"+
                    "&apv_sub_order_fix=<?=$apv_sub_order_fix?>"+
                    "&apply_id=<?=$apply_id?>"+
                    "&worksheet_date=<?=$worksheet_date?>"+
                    "&target_apply_id="+ target_apply_id+
                    "&exec_mode=<?=$exec_mode?>";
            }
        </script>



        <? //==================================================================== ?>
        <? // 画面左側の押印ドラッグ元                                            ?>
        <? //==================================================================== ?>
        <? if ($target_apply_id==$apply_id && $sousin_or_jusin == "受信" && !$row_apply["re_apply_id"]) { ?>

        <div id="div_imprint_area">
        <div id="message1" style="margin-top:12px; white-space:nowrap; background-color:#ffb9ff; padding:2px; text-align:center; color:#b841b8; border:1px solid #ffddff">
            受付欄に印影をドラッグできます。&nbsp;<input type="button" value="リセット" onclick="reset_imprint(true, true); init_imprint();">
        </div>

        <table id="history_tbl" class="list_table">
            <tr>
                <th width="33%" style="text-align:center">差戻し</th>
                <th width="33%" style="text-align:center">否認</th>
                <th width="33%" style="text-align:center">受付</th>
            </tr>

            <tr style="height:84px">
                <td style="text-align:center; padding:16px 0px;">
                    <div id="applyA3" style="cursor:move;"><img src="img/returned.gif"></div>
                    <div id="applyB3" style="cursor:move; display:none"><img src="img/returned.gif"></div>
                </td>

                <td style="text-align:center; padding:16px 0px;">
                    <div id="applyA2" style="cursor:move;"><img src="img/approve_ng.gif"></div>
                    <div id="applyB2" style="cursor:move; display:none"><img src="img/approve_ng.gif"></div>
                </td>

                <td style="text-align:center; padding:16px 0px;">
                    <div id="applyA1" style="cursor:move;">
                        <? show_imprint_image($session, $login_emp_id, "1", $imprint_flg, "f", @$row_wkfwmst["approve_label"]); ?>
                    </div>
                </td>
            </tr>

            <tr style="background-color:#fff">
                <td colspan="2" rowspan="2"><br/></td>
                <th style="text-align:center">実施</th>
            </tr>
            <tr style="height:84px">
                <td style="text-align:center; padding:16px 0px;">
                    <div id="applyB1" style="cursor:move;">
                        <? show_imprint_image($session, $login_emp_id, "4", $imprint_flg, "f", @$row_wkfwmst["approve_label"]); ?>
                    </div>
                </td>
            </tr>
        </table>
        </div>
        <? } ?>




    </td>
    <td valign="top" style="padding-left:4px">

    <div id="dtl_tbl">

        <? //==================================================================== ?>
        <? // 画面右側のヘッダ部                                                  ?>
        <? //==================================================================== ?>
        <?
            $sql =
                " select".
                " emp.emp_lt_nm, emp.emp_ft_nm".
                ",cls.class_nm as apply_class_nm".
                ",atrb.atrb_nm as apply_atrb_nm".
                ",dept.dept_nm as apply_dept_nm".
                ",room.room_nm as apply_room_nm".
                ",pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm".
                " from sum_apply ap".
                " inner join empmst emp on (ap.emp_id = emp.emp_id)".
                " left join ptifmst pt on (pt.ptif_id = ap.ptif_id)".
                " left join classmst cls on (ap.emp_class = cls.class_id)".
                " left join atrbmst atrb on (ap.emp_attribute = atrb.atrb_id)".
                " left join deptmst dept on (ap.emp_dept = dept.dept_id)".
                " left join classroom room on (ap.emp_room = room.room_id)".
                " where ap.apply_id = ".$target_apply_id.
                " limit 1";
            $sel = select_from_table($con, $sql, "", $fname);
            $row_applyinfo = pg_fetch_array($sel);
        ?>

        <table class="prop_table">
            <col width="100"/><col width="140"/><col width="100"/><col width="120"/><col width="100"/><col width="160"/>

          <tr>
          <th>送信書名</th>
          <td colspan="3" id="td_sousin_syomei"><?=h($row_summary["diag_div"])?></td>
          <th>送信日</th>
          <td id="td_sousin_bi"><?=preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $row_apply["apply_date"]);?></td>
          </tr>

          <tr>
          <th>オーダ番号</th>
          <? $order_bangou = $obj->generate_apply_no_for_display($row_wkfwmst["short_wkfw_name"], $row_apply["apply_date"], $row_apply["apply_no"]); ?>
          <td id="td_order_bangou"><?=h($order_bangou)?></td>
          <th>テンプレート名</th>
          <td colspan="3" id="td_template_name"><?=h($row_tmplmst["tmpl_name"])?></td>
          </tr>

          <tr>
          <th>送信者</th>
          <td id="td_sousin_sya"><?=h($row_applyinfo["emp_lt_nm"]." ".$row_applyinfo["emp_ft_nm"])?></td>
            <? $syozoku  = $row_applyinfo["apply_class_nm"]." > ".$row_applyinfo["apply_atrb_nm"]." > ".$row_applyinfo["apply_dept_nm"]; ?>
            <? if ($row_applyinfo["apply_room_nm"] != "") $syozoku .= " > ".$row_applyinfo["apply_room_nm"]; ?>
          <th>所属</th>
          <td colspan="3" id="td_syozoku"><?=h($syozoku)?></td>
          </tr>

            <?
                // 代理送信者がいるか、作成者と違う人間が更新していれば
                if (($row_apply["dairi_emp_id"] && !$row_apply["dairi_kakunin_ymdhms"])
                || ($row_apply["update_emp_id"] && $row_apply["update_emp_id"]!=$row_apply["emp_id"])){
                    $sql =
                        " select emp_lt_nm || ' ' || emp_ft_nm from empmst".
                        " where emp_id = '".pg_escape_string($row_apply["dairi_emp_id"])."'";
                    $sel2 = select_from_table($con, $sql, "", $fname);
                    $dairi_emp_name = @pg_fetch_result($sel2, 0, 0);
                    $sql = "select emp_lt_nm || ' ' || emp_ft_nm from empmst where emp_id = '".pg_escape_string($row_apply["update_emp_id"])."'";
                    $sel2 = select_from_table($con, $sql, "", $fname);
                    $update_emp_name = pg_fetch_result($sel2, 0, 0);
            ?>
          <tr>
          <th>最終更新者</th>
          <td id="td_dairi_sousin_sya"><?=h($update_emp_name)?></td>
          <th>代理送信者</th>
          <td colspan="3" id="td_syozoku"><?=h($dairi_emp_name)?></td>
          </tr>
          <? } ?>

            <tr>
          <th>患者ID</th>
          <td id="td_kanja_id"><?=h($ptif_id)?></td>
          <th>患者名</th>
          <td colspan="3" id="td_kanjamei"><?=h($row_applyinfo["ptif_lt_kaj_nm"]." ".$row_applyinfo["ptif_ft_kaj_nm"])?></td>
          </tr>
        </table>


        <? //==================================================================== ?>
        <? // 画面右側の送信済・受付済・実施済状態 赤黄ミニパネル                 ?>
        <? //==================================================================== ?>
        <?
            $border_color = array("border:1px solid #d22846;", "border:1px solid #bdc122;");
            $back_color = array("background-color:#e98b9b;", "background-color:#e7e98b;");
            $accepted_count = 0;
            $operated_count = 0;

            $sql = "select apply_stat from sum_apply where apply_id = ".$target_apply_id;
            $sel = select_from_table($con, $sql, "", $fname);
            $send_apply_stat = (int)@pg_fetch_result($sel, 0, "apply_stat");
            $is_sending = ($send_apply_stat == 1 ? 1 : 0);

            $sql = "select count(*) from sum_applyapv where apply_id = ". $target_apply_id;
            $sel = select_from_table($con, $sql, "", $fname);
            $acceptor_count = (int)@pg_fetch_result($sel, 0, 0);

            $sql = "select count(*) from sum_applyapv where apply_id = ". $target_apply_id . " and next_notice_recv_div = 2";
            $sel = select_from_table($con, $sql, "", $fname);
            $operator_count = (int)@pg_fetch_result($sel, 0, 0);

            if ($send_apply_stat > 0){
                $sql =
                    "select count(*) from sum_applyapv".
                    " where apply_id = ". $target_apply_id. " and (apv_stat_accepted = '1' or apv_stat_accepted_by_other = '1')";
                $sel = select_from_table($con, $sql, "", $fname);
                $accepted_count = (int)@pg_fetch_result($sel, 0, 0);

                $sql =
                    "select count(*) from sum_applyapv".
                    " where apply_id = ". $target_apply_id. " and (apv_stat_operated = '1' or apv_stat_operated_by_other = '1') and next_notice_recv_div = 2";
                $sel = select_from_table($con, $sql, "", $fname);
                $operated_count = (int)@pg_fetch_result($sel, 0, 0);
            }
            $is_accepted = ($accepted_count == $acceptor_count);
            $is_operated = ($operated_count == $operator_count);
            if (!$operator_count && $is_operated) $is_operated = 0;
            if (!$acceptor_count && $is_accepted) $is_accepted = 0;

            $exist_accepted_sasimodosi = 0;
            $exist_accepted_hinin = 0;
            $exist_operated_sasimodosi = 0;
            $exist_operated_hinin = 0;
            if (is_array($rows_hinin_sasimodosi)){
                foreach ($rows_hinin_sasimodosi as $tkey => $trow){
                    if ($trow["update_ymdhm"]) continue;
                    if ($trow["hinin_or_sasimodosi"]=="sasimodosi") {
                        if ($trow["from_apply_stat_accepted"]) $exist_accepted_sasimodosi = 1;
                        if ($trow["from_apply_stat_operated"]) $exist_operated_sasimodosi = 1;
                    }
                    if ($trow["hinin_or_sasimodosi"]=="hinin") {
                        if ($trow["from_apply_stat_accepted"]) $exist_accepted_hinin = 1;
                        if ($trow["from_apply_stat_operated"]) $exist_operated_hinin = 1;
                    }
                }
            }

            $is_sending_caption_list = array("未送信", "送信済", "中止送信");
            $is_sending_caption =  $is_sending_caption_list[$send_apply_stat];
            $is_accepted_caption = ($is_accepted ? "受付済" : "未受付");
            if ($accepted_count > 0 && $acceptor_count > $accepted_count) $is_accepted_caption = "一部受付";
            if ($exist_accepted_sasimodosi) $is_accepted_caption = "差戻し";
            if ($exist_accepted_hinin) $is_accepted_caption = "否認";
            $is_operated_caption = ($is_operated ? "実施済" : "未実施");
            if ($operated_count > 0 && $operator_count > $operated_count) $is_operated_caption = "一部実施";
            if ($exist_operated_sasimodosi) $is_operated_caption = "差戻し";
            if ($exist_operated_hinin) $is_operated_caption = "否認";
        ?>

        <div>
            <div style="float:left; padding-top:4px">
                <div style="float:left; background-color:#fac">
                    <table class="list_table">
                        <tr style="height:22px"><th style="text-align:center; padding:2px 6px; ">
                            <img src="images/arrow_right_red.gif" style="vertical-align:top; padding-top:3px">
                            <a href="" onclick="showAlternateLeftHistoryArea(); return false;" class="always_blue">
                            送信履歴
                            </a>
                        </th></tr>
                    </table>
                </div>
            </div>
            <div style="float:right; padding-top:8px; padding-bottom:8px; text-align:right;" id="div_apply_status">
                <span style="padding:2px; <?=$back_color[$is_sending]?> <?=$border_color[$is_sending]?>"><?=$is_sending_caption?></span>
                <? if (!$row_wkfwmst["is_hide_approve_status"]){ ?>
                    <? if ($acceptor_count > 0){ ?>
                    <span style="padding:2px; <?=$back_color[$is_accepted]?> <?=$border_color[$is_accepted]?>"><?=$is_accepted_caption?></span>
                    <? } ?>
                    <? if ($operator_count > 0){ ?>
                    <span style="padding:2px; <?=$back_color[$is_operated]?> <?=$border_color[$is_operated]?>"><?=$is_operated_caption?></span>
                    <? } ?>
                <? } ?>
            </div>
        </div>
        <br style="clear:both" />


        <? //==================================================================== ?>
        <? // 本文・テンプレート                                                  ?>
        <? //==================================================================== ?>
        <?
            $xml_file_name = $row_apply["emp_id"]."_".$ptif_id."_".$summary_id.".xml";// XMLファイル取得(2010/1まで)
            // XMLファイル取得(2010/2から)
            if ($summary_seq!=""){
                $sql = "select xml_file_name from sum_xml where xml_file_name = '".$summary_seq."_".$target_apply_id.".xml'";
                $rs = select_from_table($con, $sql, "", $fname);
                $tmp_xml_file_name = @pg_fetch_result($rs, 0, "xml_file_name");
                if ($tmp_xml_file_name!="") $xml_file_name = $tmp_xml_file_name;
            }
        ?>
        <table class="prop_table" style="width:100%"><tr><td>

<? // 温度板の[C]ボタンから遷移している場合は、テンプレートと患者IDをチェックして ?>
<? // どちらかが異なる場合は、新規登録画面に遷移する                              ?>
<?
   if (array_key_exists("tmpl_id", $_REQUEST) && array_key_exists("ptif_id", $_REQUEST))
   {
     if ((@$row_wkfwmst["tmpl_id"] != $_REQUEST["tmpl_id"]) || ($ptif_id != $_REQUEST["ptif_id"]))
     {
       ob_end_clean();
       header("HTTP/1.1 301 Moved Permanently");
       header("Location: sum_application_apply_new.php?session=".@$_REQUEST["session"]."&tmpl_id=".@$_REQUEST["tmpl_id"]."&ptif_id=".@$_REQUEST["ptif_id"]."&ymd=".@$_REQUEST["ymd"]);
       exit ;
     }
   }
   ob_end_flush() ;
?>
        <iframe src="summary_tmpl_read.php?session=<?=$session?>&worksheet_date=<?=$worksheet_date?>&pt_id=<?=$ptif_id?>&emp_id=<?=$login_emp_id?>&summary_id=<?=$summary_id?>&xml_file=<?=$xml_file_name?>&tmpl_id=<?=@$row_wkfwmst["tmpl_id"]?>&cre_date=<?=$apply_date_ymd?>&apply_id=<?=$target_apply_id?>&sansho_flg=true&iframe=iframe_template&auto_parent_resize=1&is_template_readonly=<?=$is_template_readonly?>" name="iframe_template" id="iframe_template" style="width:100%; height:300px" frameborder="0"></iframe>
        </td></tr></table>


        <form name="apply" action="#" method="post">
            <input type="hidden" name="command_mode" value="">
            <input type="hidden" name="exec_mode" value="<?=$exec_mode?>">
            <input type="hidden" name="approve_num" value="<?=count($rows_applyapv)?>">
            <input type="hidden" name="session" value="<? echo($session); ?>">
            <input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
            <input type="hidden" name="target_apply_id" value="<? echo($target_apply_id); ?>">


            <? $next_notice_recv_div_count = 0; ?>
            <? foreach($rows_applyapv as $ar) { ?>
            <?   if ($ar["next_notice_recv_div"] == "2") $next_notice_recv_div_count++; ?>
            <?   if ($apply_id!=$target_apply_id) continue; ?>
            <?   if ($ar["apv_order"] != $apv_order_fix) continue; ?>
            <?   if ((int)$ar["apv_sub_order"] != (int)$apv_sub_order_fix) continue; ?>
            <input type="hidden" name="next_notice_div" value="<?=$ar["next_notice_div"]?>">
            <input type="hidden" name="next_notice_recv_div" value="<?=$ar["next_notice_recv_div"]?>">
            <input type="hidden" name="apv_order_fix" value="<?=$ar["apv_order"] ?>">
            <input type="hidden" name="apv_sub_order_fix" value="<?=$ar["apv_sub_order"]?>">
            <input type="hidden" name="apv_emp_id" value="<?=$ar["emp_id"]?>">
            <? } ?>



        <? //==================================================================== ?>
        <? // 送信者以外の結果通知(HIDDENで)いまのところ不要                      ?>
        <? //==================================================================== ?>
        <?
            if ($row_apply["notice_sel_flg"] == "t"){
                $sql =
                    " select a.*, b.emp_lt_nm, b.emp_ft_nm from sum_applynotice a".
                    " left join empmst b on a.recv_emp_id = b.emp_id".
                    " where a.apply_id = ".$target_apply_id." order by a.oid";
                $rs = select_from_table($con, $sql, "", $fname);
                $recv_emp_id_list = array();
                $recv_emp_nm_list = array();
                $rslt_ntc_div_list = array();
                while($row = pg_fetch_array($rs)){
                    $recv_emp_id_list[] = $row["recv_emp_id"];
                    $recv_emp_nm_list[] = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
                    $rslt_ntc_div_list[] = $row["rslt_ntc_div"];
                }
        ?>
    <input type="hidden" name="notice_emp_id" id="notice_emp_id" value="<?=implode(",", $recv_emp_id_list)?>">
    <input type="hidden" name="notice_emp_nm" id="notice_emp_nm" value="<?=implode(",", $recv_emp_nm_list)?>">
    <input type="hidden" name="rslt_ntc_div" id="rslt_ntc_div" value="<?=implode(",", $rslt_ntc_div_list)?>">
        <? } ?>




        <? //==================================================================== ?>
        <? // 添付ファイル                                                        ?>
        <? //==================================================================== ?>
        <script type="text/javascript">
            function attachFile() {
                window.open('sum_apply_attach.php?session=<?=$session?>', 'newwin3', 'width=640,height=480,scrollbars=yes');
            }
            function detachFile(e) {
                if (e == undefined) e = window.event;
                var btn_id = (e.target ? e.target.getAttribute('id') : e.srcElement.id);
                var id = btn_id.replace('btn_', '');
                var p = document.getElementById('p_' + id);
                document.getElementById('attach').removeChild(p);
            }
        </script>
        <?
            $attach_editable = 0;
            // 送信一覧画面から開いた場合は、送信者か代理送信者は登録可能
            // ただし送信済み(apply_stat=1)なら登録できないが、差戻しがあれば、即編集可能とする
            if ($sousin_or_jusin == "送信" && ($row_apply["emp_id"]==$login_emp_id || $is_dairi_emp)) {
                    $attach_editable = 1;
            }
            // 受信一覧画面から開いた場合は、誰でも編集可能なテンプレートのとき、受付実施が未登録なら登録可能
            // ただし他者から差し戻された人間も、更新可能
            if ($sousin_or_jusin == "受信"){
                if ($is_login_user_sasimodosare || $is_modifyable_by_other_emp){
                    if ($row_wkfwmst["wkfw_appr"]!="2" || $apv_order_fix >= $max_apv_order_fix) {// 稟議回覧で過ぎたオーダでなければ、添付可能
                        $attach_editable = 1;
                    }
                }
            }
        ?>
        <table class="prop_table" style="margin-top:4px">
            <tr>
            <th style="width:100px">添付ファイル</th>
            <td>
                <? if($row_apply["re_apply_id"]) {?>
                <span style="color:#aaa">過去履歴では、添付ファイルは表示されません。</span>
                <? } ?>
                <div id="attach">
            <?
                // 過去履歴の添付ファイルは保持していない
                if(!$row_apply["re_apply_id"]) {
                    $sql = "select applyfile_no, applyfile_name from sum_applyfile where apply_id = ".$apply_id;
                    $rs = select_from_table($con, $sql, "", $fname);
                    $applyfile_name_list = array();
                    while($row = pg_fetch_array($rs)) {
                        $fname = $row["applyfile_name"];
                        $fno = $row["applyfile_no"];
                        $ext = strrchr($fname, ".");
                        @copy("summary/attach/".$apply_id."_".$fno.$ext, "summary/tmp/".$session."_".$fno.$ext);// 一時フォルダにコピー
                        $applyfile_name_list[] = "<div>".$fname."</div>";
            ?>
                <div id="p_<?=$fno?>">
                    <? $param = "&file_flg=3&apply_id=".$apply_id."&file_id=".$fno."&filename=".urlencode($fname); ?>
                    <a href="sum_workflow_attach_refer.php?session=<?=$session?><?=$param?>" target="_blank"><?=$fname?></a>
                      <input type="button" id="btn_<?=$fno?>" name="btn_<?=$fno?>" value="削除"
                        onclick="detachFile(event);" <?=!$attach_editable?"disabled=\"disabled\"":""?>) />
                <input type="hidden" name="filename[]" value="<?=$fname?>">
                <input type="hidden" name="file_id[]" value="<?=$fno?>">
                </div>
            <?   } // end while ?>

                </div>
                <input type="button" onclick="attachFile();" value="追加" <?=!$attach_editable?"disabled=\"disabled\"":""?>  />
                <div style="display:none" id="print_attach_files"><?=implode("\n", $applyfile_name_list)?></div>
            <? } // if (not re_apply) ?>
            </td>
            </tr>


            <? //==================================================================== ?>
            <? // コメント                                                            ?>
            <? //==================================================================== ?>
            <?
                $other_comment = "";
                $self_comment = "";
                foreach($rows_applyapv as $applyapv){
                    // この階層の、このユーザによって書かれたコメントを取得
                    if($applyapv["apv_order"] == $apv_order_fix && $applyapv["apv_sub_order"] == $apv_sub_order_fix) {
                        $self_comment = trim($applyapv["apv_comment"]);
                    // それ以外
                    } else {
                        if(trim($applyapv["apv_comment"]) != "") {
                            $other_comment .= "【".$applyapv["emp_lt_nm"]." ".$applyapv["emp_ft_nm"]."さんのコメント】"."\n".$applyapv["apv_comment"]."\n\n";
                        }
                    }
                }
                $other_comment = str_replace("\n", "<br/>", h($other_comment));
            ?>
            <tr>
            <th style="width:100px">コメント</th>
            <td>
                <table width="100%">
                    <tr><td id="other_comment"><?=$other_comment?></td></tr>
                    <tr>
                        <td bgcolor="#ffffff">
                            <? if ($sousin_or_jusin == "受信"){ ?>
                            <textarea name="apv_comment" id="apv_comment" rows="5" style="ime-mode: active; width:500px"><?=h($self_comment)?></textarea></td>
                            <? } ?>
                    </tr>
                </table>
            </td>
            </tr>
        </table>


        <? //==================================================================== ?>
        <? // 受信者数と受信タイプ                                                ?>
        <? //==================================================================== ?>
        <div id="wkfw_appr_info" style="margin-top:16px; margin-bottom:2px">
            <div style="float:left;">
                受信者数：<?=count($rows_applyapv)?>人　受信タイプ：<?=($row_wkfwmst["wkfw_appr"] == "1") ? "同報" : "稟議（回覧）"?>
            </div>
            <div style="float:right; padding-bottom:3px" id="sousinsya_sasimodosi_check"></div>
            <div style="clear:both"></div>
            <? $display_style = ($is_login_user_sasimodosi_sita ? "" : "display:none"); ?>
            <div style="color:#0b7312; <?=$display_style?>" id="div_sasimodosi_msg">差戻しを指定しています。差戻しを登録するには、「差戻し通知」する職員を指定してください。</div>
            <? if ($is_login_user_sasimodosare){ ?>
                <div style="color:#f05;">他者により差戻しが通知されています。内容の修正を行って、更新してください。</div>
            <? } ?>
            <? if ($is_sousin_user_sasimodosare){ ?>
                <div style="color:#f05;">他者により送信者へ差戻しが通知されています。内容の修正を行って、更新してください。</div>
            <? } ?>
        </div>



            <? //==================================================================== ?>
            <? // 押印欄                                                              ?>
            <? //==================================================================== ?>
            <div style="border:1px solid #BAD9F2; border-bottom:0; padding:3px;
                background:url(css/img/bg-b-gura.gif) top repeat-x;">
                    受信状態
            </div>
            <table id="approve_ordersA" class="prop_table"></table>

            <? if ($next_notice_recv_div_count > 0){ ?>
                <div style="border:1px solid #BAD9F2; border-bottom:0; padding:3px; margin-top:10px;
                    background:url(css/img/bg-b-gura.gif) top repeat-x;">
                    実施状態
            </div>
                <table id="approve_ordersB" class="prop_table"></table>
            <? } ?>

            <input type="hidden" name="wkfw_appr" value="<?=$row_wkfwmst["wkfw_appr"]?>">



            <? //==================================================================== ?>
            <? // コマンドボタン                                                      ?>
            <? //==================================================================== ?>
            <?
                $kousin_dekinai_msg = "";
                if ($apply_id != $target_apply_id) $kousin_dekinai_msg = "過去履歴に対する登録操作は行えません。";
                else if ($row_apply["re_apply_id"])     $kousin_dekinai_msg = "過去履歴を参照しています。登録操作は行えません。";
                else if ($is_all_approve_complete) $kousin_dekinai_msg = "送受信はすべて完了しました。";
            ?>

            <div style="text-align:right; padding-top:8px">
                <? //==================================================================== ?>
                <? // コマンドボタン（送信）                                              ?>
                <? //==================================================================== ?>
                <script type="text/javascript">
                    function apply_command(command) {
                        var msg = "";
                        if (command=="cyusi") msg = "中止送信を行います。";
                        if (command=="update") msg = "更新を行います。";
                        if (command=="update_and_resend") {
//                          msg = "テンプレート内容、添付ファイル<?=$sousin_or_jusin=="受信" ? "、コメント" : ""?>の更新を行います。";
                            msg = "更新を行います。";
                        }
                        msg += "よろしいですか？";
                        if (!confirm(msg)) return;
                        document.apply.command_mode.value = command;
                        if (command == "update" || command == "update_and_resend"){
                            if(iframe_template.InputCheck) if(!iframe_template.InputCheck()) return;
                            iframe_template.document.tmplform.action =
                                "create_tmpl_xml.php?session=<?=$session?>&apply_id=<?=$apply_id?>&command_mode=" + command;
                            iframe_template.document.tmplform.submit();
                            return;
                        }
                        document.apply.action="sum_application_approve_detail.php?session=<?=$session?>";
                        document.apply.submit();
                    }
                    function approve_regist() {
                        var is_sasimodosi = false;
                        if (document.getElementById("approveA") && document.apply.approveA.value == "3") is_sasimodosi = true;
                        if (document.getElementById("approveB") && document.apply.approveB.value == "3") is_sasimodosi = true;
                        is_exist_sasimodosi_target = false;
                        if (is_sasimodosi){
                            if (document.apply.approve_order_0_sasimodosi_a && document.apply.approve_order_0_sasimodosi_a.checked) {
                                is_exist_sasimodosi_target = true;
                            }
                            else {
                                for (var i = 0, j = document.apply.elements.length; i < j; i++) {
                                    var nm = document.apply.elements[i].name;
                                    if (nm.substring(0, 13) != "approve_order") continue;
                                    if (nm.indexOf("_sasimodosi_") == -1) continue;
                                    if (nm == "approve_order_0_sasimodosi_a") continue;
                                    if (document.getElementsByName(nm)[0].value=="") continue;
                                    is_exist_sasimodosi_target = true;
                                }
                            }
                            if (!is_exist_sasimodosi_target){
                                alert("差戻しを行う場合は、「送信者への差戻し通知」を指定するか、\n差戻し通知する職員を指定してください。");
                                return;
                            }
                        }
                        apply_command('update_and_resend');
/*
                        if (!confirm('コメント、受信/実施状態を登録します。よろしいですか？')) return false;
                        if (window.InputCheck) if (!InputCheck()) return;
                        if(iframe_template.InputCheck) if(!iframe_template.InputCheck()) return;
                        document.apply.command_mode.value = "approve_regist";
                        document.apply.action = "sum_application_approve_detail.php?session=<?=$session?>";
                        document.apply.submit();
*/
                    }
                </script>
                <? if ($sousin_or_jusin == "送信"){ ?>
                    <span style="color:#aaa"><?=$kousin_dekinai_msg?></span>
                    <? $disabled_cyusi = (($row_apply["apply_stat"]!="1" || $kousin_dekinai_msg) ? "disabled" : ""); ?>
                    <input type="button" value="中止送信" onclick="apply_command('cyusi');" <?=$disabled_cyusi?>>
                    <? $disabled_kousin = (($row_apply["apply_stat"]=="1" || $kousin_dekinai_msg) ? "disabled" : ""); ?>
                    <input type="button" value="更新" onclick="apply_command('update');" <?=$disabled_kousin?>>
                <? } ?>


                <? //==================================================================== ?>
                <? // コマンドボタン（受信）                                              ?>
                <? //==================================================================== ?>
                <? if ($sousin_or_jusin == "受信"){ ?>
                    <script type="text/javascript">
                    </script>
                    <? if (!$worksheet_date || $apply_id != $target_apply_id) { ?>
                        <?
                            if (!$kousin_dekinai_msg){
                                if (!$is_login_user_sasimodosare && !$is_sousin_user_sasimodosare){ // 差し戻された場合は修正可能のためスルー
                                    if ($row_wkfwmst["wkfw_appr"] == "2"){ // 同報なら(稟議回覧でなければ)修正可能のためスルー
                                        if ($apv_order_fix < $max_apv_order_fix) {
                                            $kousin_dekinai_msg = "次階層が登録されています。送受信状態は登録できません。";
                                        }
                                    }
                                }
                            }
                        ?>
                        <span style="color:#aaa"><?=$kousin_dekinai_msg?></span>
                        <!-- input type="button" value="送信" onclick="approve_regist();" <?=$kousin_dekinai_msg=="" ? "" : "disabled"?> -->
                    <? } ?>
                <? } ?>

                <? $disabled_kousin_sousin = ($kousin_dekinai_msg ? "disabled" : ""); ?>
                <? if ($sousin_or_jusin == "送信"){ ?>
                    <input type="button" value="更新＋送信" onclick="apply_command('update_and_resend');" <?=$disabled_kousin_sousin?> >
                <? } ?>
                <? if ($sousin_or_jusin == "受信"){ ?>
                    <input type="button" value="更新" onclick="approve_regist();" <?=$disabled_kousin_sousin?> >
                <? } ?>

            <input type="button" value="印刷" onclick="trySubmitPrint('');">
            <input type="button" value="簡易印刷" onclick="trySubmitPrint('text');">
            </div>

        </form>

    </div>
    </td>
</tr>
</table>

<? //==================================================================== ?>
<? // 印刷画面表示用                                                      ?>
<? //==================================================================== ?>
<form name="approve_print_form" action="sum_application_approve_detail_print.php" method="post" target="approve_detail_print_window">
    <input type="hidden" name="session" value="<?=$session?>">
    <input type="hidden" name="apply_id" value="<?=$target_apply_id?>">
    <input type="hidden" name="ptif_id" value="<?=@$ptif_id?>">
    <input type="hidden" name="emp_id" value="<?=@$login_emp_id?>">
    <input type="hidden" name="summary_id" value="<?=@$summary_id?>">
    <input type="hidden" name="tmpl_id" value="<?=@$row_wkfwmst["tmpl_id"]?>">
    <input type="hidden" name="apply_date" value="<?=@$apply_date_ymd?>">
    <input type="hidden" name="summary_seq" value="<?=@(int)$summary_seq?>">
    <input type="hidden" name="worksheet_date" value="<?=$worksheet_date?>">
    <input type="hidden" name="mode" value="flowsheet">
    <input type="hidden" name="print_mode" value="">
</form>







<script type="text/javascript">

    var approve_ordersA = {};
    var approve_ordersB = {};









    <? //==================================================================== ?>
    <? // 受付の押印情報のJavaScriptを出力                                    ?>
    <? //==================================================================== ?>
    <?
        $imprint_images = array();
        $apv_no = 0;
        $self_apv_stat_accepted = 0;
        foreach($rows_applyapv as $ar) {
            $apv_stat_accepted = (int)$ar["apv_stat_accepted"];
            $apv_no++;
            $is_current_hierarchy = ($ar["apv_order"] == $apv_order_fix && $ar["apv_sub_order"] == $apv_sub_order_fix);

            $apv_date_accepted = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $ar["apv_date_accepted"]);

            // 印影機能を利用
            $imprint_img = "";
            if ($apv_stat_accepted=="1" || $apv_stat_accepted=="2" || $apv_stat_accepted=="3") {
                $imprint_flg = get_imprint_flg($con, $ar["emp_id"], $fname);
                $imprint_img = str_replace("\n", "", get_imprint_image_script($session, $ar["emp_id"], $apv_stat_accepted, $imprint_flg, "f", "受付"));
                if ($imprint_img != "") {
                    $tmp_img_no = count($imprint_images) + 1;
                    $tmp_img_src = preg_replace('/.*src="([^"]*)".*/', "$1", $imprint_img);
                    $tmp_img_width = preg_replace('/.*width="([^"]*)".*/', "$1", $imprint_img);
                    $tmp_img_height = preg_replace('/.*height="([^"]*)".*/', "$1", $imprint_img);
                    $imprint_img = str_replace($tmp_img_src, $tmp_img_src."&no=".$tmp_img_no, $imprint_img);
                    $imprint_images[] = array( "src" => $tmp_img_src."&no=".$tmp_img_no, "width" => $tmp_img_width, "height" => $tmp_img_height);
                }
            }

            $status = h($ar["display_caption"]);
            if (!$status) $status = "<br/>";

            $emps = "";
            // ドラッグアンドドロップのターゲット
            if ($sousin_or_jusin=="受信" && ($is_login_user_sasimodosare || $row_wkfwmst["wkfw_appr"]=="1" || (int)$ar["apv_order"] >= $max_apv_order_fix) && $is_current_hierarchy && $apply_id==$target_apply_id && !$row_apply["re_apply_id"] && !$is_all_approve_complete) {
                $emps = "<div id=\"targetA\" style=\"width: 60px; height: 60px; background-color: #ffcccc;margin:0 auto;\"><img id=\"imgA\" src=\"img/spacer.gif\"></div>";
            }

            $apv_full_nm = trim($ar["emp_lt_nm"]." ".$ar["emp_ft_nm"]);
            if (!$ar["emp_id"]) $apv_full_nm = "(受信者未指定)";
            $emps .= h($apv_full_nm) . "<br>";
            if ($sousin_or_jusin=="受信" && ($is_login_user_sasimodosare || $row_wkfwmst["wkfw_appr"]=="1" || (int)$ar["apv_order"] >= $max_apv_order_fix) && $is_current_hierarchy && $apply_id==$target_apply_id && !$row_apply["re_apply_id"] && !$is_all_approve_complete) {
                $self_apv_stat_accepted = $apv_stat_accepted;
                $emps .= "<select name=\"approveA\" id=\"approveA\" onchange=\"set_target(this.value, 'targetA');\">";
                $emps .= "<option value=\"0\">未受付</option>";
                $emps .= "<option value=\"1\"".($apv_stat_accepted=="1"? " selected":"").">受付済</option>";
                $emps .= "<option value=\"2\"".($apv_stat_accepted=="2"? " selected":"").">否認</option>";
                $emps .= "<option value=\"3\"".($apv_stat_accepted=="3"? " selected":"").">差戻し</option>";
                $emps .= "</select>";
                $emps .= "<br/>".$apv_date_accepted;
                $regist_btn_flg = true;
            } else {
                $a = array("未受付", "受付済", "否認", "差戻し");
                $tmp = @$a[$apv_stat_accepted];
                if ($ar["emp_id"] == "") $tmp = "";
                if ($tmp) $tmp = '<span id="span_parent_approve_order_a_'.(int)$ar["apv_order"]."_".(int)$ar["apv_sub_order"].'">'.$tmp."</span><br/>";
                if (@$ar["apply_stat_accepted_sasimodosare"]) $tmp .= "<span style='color:red'>(差戻されました)</span><br/>";
                if (!@$ar["apv_stat_accepted"] && @$ar["apv_stat_accepted_by_other"]) $tmp .= "<span style='color:#ba16be'>(他者受付済)</span><br/>";
else
{
  $hos = getHininOrSasimodosi($con, $fname, $ptif_id, $ar["apply_id"], $ar["apv_order"], $ar["emp_id"]) ;
  if ($hos) $tmp .= ("<span style='color:#ba16be'>(他者".$hos."済)</span><br/>") ;
}
                $emps .= $tmp.$apv_date_accepted;
                $emps = $imprint_img . $emps;
            }

            if ($ar["apv_sub_order"] == "") {
                $no = $ar["apv_order"];
    ?>
        approve_ordersA['<?=$ar["apv_order"]?>'] = {
            'opened': false,
            'no': '<?=str_replace("'", "\\'", $no) ?>',
            'status': '<?=str_replace("'", "\\'", $status) ?>',
            'emps': '<?=str_replace("'", "\\'", $emps) ?>',
            'cur_stat' : '<?=$apv_stat_accepted?>',
            'new_stat' : '<?=@$ar["apply_stat_accepted_sasimodosare"]?>',
            'emp_id'   : '<?=$ar["emp_id"]?>',
            'apv_sub_order': '<?=$ar["apv_sub_order"]?>',
            'sub_orders': {}
        };
    <?   } else { ?>
        if (!approve_ordersA['<?=$ar["apv_order"]?>']<? if ($ar["emp_id"] == $login_emp_id) {echo(" || true");} ?>) {
            var sub_orders = (approve_ordersA['<?=$ar["apv_order"]?>']) ? approve_ordersA['<?=$ar["apv_order"]?>']['sub_orders'] : {};
            approve_ordersA['<?=$ar["apv_order"]?>'] = {
                'opened': false,
                'no': '<a href="javascript:void(0);" onclick="refreshApproveOrdersA(\'<?=$ar["apv_order"]?>\');"><?=$ar["apv_order"]?>（複数あり）</a>',
                'status': '<? echo(str_replace("'", "\\'", $status)); ?>',
                'emps': '<? echo(str_replace("'", "\\'", $emps)); ?>',
                'cur_stat' : '<?=$apv_stat_accepted?>',
                'new_stat' : '<?=@$ar["apply_stat_accepted_sasimodosare"]?>',
                'emp_id'   : '<?=$ar["emp_id"]?>',
                'apv_sub_order': '<?=$ar["apv_sub_order"]?>',
                'sub_orders': sub_orders
            };
        }
        <? $no = $ar["apv_order"] . "_" . $ar["apv_sub_order"]; ?>
        approve_ordersA['<?=$ar["apv_order"]?>']['sub_orders']['<?=$ar["apv_sub_order"]?>'] = {
            'no': '<? echo(str_replace("'", "\\'", $no)); ?>',
            'status': '<? echo(str_replace("'", "\\'", $status)); ?>',
            'emps': '<? echo(str_replace("'", "\\'", $emps)); ?>',
            'cur_stat' : '<?=$apv_stat_accepted?>',
            'new_stat' : '<?=@$ar["apply_stat_accepted_sasimodosare"]?>',
            'emp_id'   : '<?=$ar["emp_id"]?>',
            'apv_sub_order': '<?=$ar["apv_sub_order"]?>'
        };
    <?   } ?>
    <? } ?>


    <? //==================================================================== ?>
    <? // 実施の押印情報のJavaScriptを出力                                    ?>
    <? //==================================================================== ?>
    <?
        $apv_no = 0;
        $self_apv_stat_operated = 0;
        foreach($rows_applyapv as $ar) {
            if ($ar["next_notice_recv_div"] != "2") continue;

            $apv_stat_operated = (int)$ar["apv_stat_operated"];
            $apv_no++;
            $apv_date_operated = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $ar["apv_date_operated"]);
            $is_current_hierarchy = ($ar["apv_order"] == $apv_order_fix && $ar["apv_sub_order"] == $apv_sub_order_fix);

            // 印影機能を利用
            $imprint_img = "";
            if ($apv_stat_operated=="1" || $apv_stat_operated=="2" || $apv_stat_operated=="3") {
                $imprint_flg = get_imprint_flg($con, $ar["emp_id"], $fname);
                $imprint_img = str_replace("\n", "", get_imprint_image_script($session, $ar["emp_id"], $apv_stat_operated, $imprint_flg, "f", '実施'));
                if ($imprint_img != "") {
                    $tmp_img_no = count($imprint_images) + 1;
                    $tmp_img_src = preg_replace('/.*src="([^"]*)".*/', "$1", $imprint_img);
                    $tmp_img_width = preg_replace('/.*width="([^"]*)".*/', "$1", $imprint_img);
                    $tmp_img_height = preg_replace('/.*height="([^"]*)".*/', "$1", $imprint_img);
                    $imprint_img = str_replace($tmp_img_src, $tmp_img_src."&no=".$tmp_img_no, $imprint_img);
                    $imprint_images[] = array( "src" => $tmp_img_src."&no=".$tmp_img_no, "width" => $tmp_img_width, "height" => $tmp_img_height);
                }
            }

            $status = h($ar["display_caption"]);
            if (!$status) $status = "<br/>";

            $emps = "";
            // ドラッグアンドドロップのターゲット
            if ($sousin_or_jusin=="受信" && ($is_login_user_sasimodosare || $row_wkfwmst["wkfw_appr"]=="1" || (int)$ar["apv_order"] >= $max_apv_order_fix) && $is_current_hierarchy && $apply_id==$target_apply_id && !$row_apply["re_apply_id"] && !$is_all_approve_complete) {
                $emps = "<div id=\"targetB\" style=\"width: 60px; height: 60px; background-color: #ffcccc;margin:0 auto;\">".
                                "<img id=\"imgB\" src=\"img/spacer.gif\"></div><img id=\"imgAB\" src=\"img/spacer.gif\">";
            }

            $apv_full_nm = trim($ar["emp_lt_nm"]." ".$ar["emp_ft_nm"]);
            if (!$ar["emp_id"]) $apv_full_nm = "(受信者未指定)";
            $emps .= h($apv_full_nm) . "<br>";

            if ($sousin_or_jusin=="受信" && ($is_login_user_sasimodosare || $row_wkfwmst["wkfw_appr"]=="1" || (int)$ar["apv_order"] >= $max_apv_order_fix) && $is_current_hierarchy && $apply_id==$target_apply_id && !$row_apply["re_apply_id"] && !$is_all_approve_complete) {
                $self_apv_stat_operated = $apv_stat_operated;
                $emps .= "<select name=\"approveB\" id=\"approveB\" onchange=\"set_target(this.value, 'targetB');\">";
                $emps .= "<option value=\"0\">未実施</option>";
                $emps .= "<option value=\"1\"".($apv_stat_operated=="1"? " selected":"").">実施済</option>";
                $emps .= "<option value=\"2\"".($apv_stat_operated=="2"? " selected":"").">否認</option>";
                $emps .= "<option value=\"3\"".($apv_stat_operated=="3"? " selected":"").">差戻し</option>";
                $emps .= "</select>";
                $emps .= "&nbsp;<br />".$apv_date_operated;

                $regist_btn_flg = true;
            } else {
                $a = array("未実施", "実施済", "否認", "差戻し");
                $tmp = @$a[$apv_stat_operated];
                if ($ar["emp_id"] == "") $tmp = "";
                if ($tmp) $tmp = '<span id="span_parent_approve_order_b_'.(int)$ar["apv_order"]."_".(int)$ar["apv_sub_order"].'">'.$tmp."</span><br/>";
                if (@$ar["apply_stat_operated_sasimodosare"]) $tmp .= "<span style='color:red'>(差戻されました)</span><br/>";
                if (!@$ar["apv_stat_operated"] && @$ar["apv_stat_operated_by_other"]) $tmp .= "<span style='color:#ba16be'>(他者実施済)</span><br/>";
                $emps .= $tmp.$apv_date_operated;
                $emps = $imprint_img . $emps;
            }

            if ($ar["apv_sub_order"] == "") {
                $no = $ar["apv_order"];
    ?>
        approve_ordersB['<?=$ar["apv_order"]?>'] = {
            'opened': false,
            'no': '<? echo(str_replace("'", "\\'", $no)); ?>',
            'status': '<? echo(str_replace("'", "\\'", $status)); ?>',
            'emps': '<? echo(str_replace("'", "\\'", $emps)); ?>',
            'cur_stat' : '<?=$apv_stat_operated?>',
            'new_stat' : '<?=@$ar["apply_stat_operated_sasimodosare"]?>',
            'emp_id'   : '<?=$ar["emp_id"]?>',
            'apv_sub_order': '<?=$ar["apv_sub_order"]?>',
            'sub_orders': {}
        };
    <?   } else { ?>
        if (!approve_ordersB['<?=$ar["apv_order"]?>']<? if ($ar["emp_id"] == $login_emp_id) {echo(" || true");} ?>) {
            var sub_orders = (approve_ordersB['<?=$ar["apv_order"]?>']) ? approve_ordersB['<?=$ar["apv_order"]?>']['sub_orders'] : {};
            approve_ordersB['<?=$ar["apv_order"]?>'] = {
                'opened': false,
                'no': '<a href="javascript:void(0);" onclick="refreshApproveOrdersB(\'<?=$ar["apv_order"]?>\');"><?=$ar["apv_order"]?>（複数あり）</a>',
                'status': '<? echo(str_replace("'", "\\'", $status)); ?>',
                'emps': '<? echo(str_replace("'", "\\'", $emps)); ?>',
                'cur_stat' : '<?=$apv_stat_operated?>',
                'new_stat' : '<?=@$ar["apply_stat_operated_sasimodosare"]?>',
                'emp_id'   : '<?=$ar["emp_id"]?>',
                'apv_sub_order': '<?=$ar["apv_sub_order"]?>',
                'sub_orders': sub_orders
            };
        }
        <? $no = $ar["apv_order"] . "_" . $ar["apv_sub_order"]; ?>
        approve_ordersB['<?=$ar["apv_order"]?>']['sub_orders']['<?=$ar["apv_sub_order"]?>'] = {
            'no': '<? echo(str_replace("'", "\\'", $no)); ?>',
            'status': '<? echo(str_replace("'", "\\'", $status)); ?>',
            'emps': '<? echo(str_replace("'", "\\'", $emps)); ?>',
            'cur_stat' : '<?=$apv_stat_operated?>',
            'new_stat' : '<?=@$ar["apply_stat_operated_sasimodosare"]?>',
            'emp_id'   : '<?=$ar["emp_id"]?>',
            'apv_sub_order': '<?=$ar["apv_sub_order"]?>'
        };
        <? } ?>
    <? } ?>



    <? //==================================================================== ?>
    <? // JavaScript画像ストック                                              ?>
    <? //==================================================================== ?>
    var imprint_images = {};
    <? foreach ($imprint_images as $tmp_imprint_image) { ?>
    imprint_images['<?=$tmp_imprint_image["src"]?>'] = new Image(<?=$tmp_imprint_image["width"] ?>, <?=$tmp_imprint_image["height"] ?>);
    imprint_images['<?=$tmp_imprint_image["src"]?>'].src = '<?=$tmp_imprint_image["src"] ?>';
    imprint_images['<?=$tmp_imprint_image["src"]?>'].className = 'imprint_image';
    <? } ?>


    <? //==================================================================== ?>
    <? // 受付側 押印欄を初期状態にして埋める                                 ?>
    <? //==================================================================== ?>
    function refreshApproveOrdersA(target_apv_order) {
        updateDOM();

        var approves = [];

        for (var apv_order in approve_ordersA) {
            if (apv_order == target_apv_order) approve_ordersA[apv_order]['opened'] = true;

            var orderObj = approve_ordersA[apv_order];
            if (!orderObj['opened']) {
                var id = 'approve_order' + apv_order;

                // 描画済みセルは書き換えない
                if (document.getElementById(id + '_no_a')) {
                    approves.push({
                        'id': id, 'no': document.getElementById(id + '_no_a').innerHTML, 'status': document.getElementById(id + '_status_a').innerHTML, 'emps': document.getElementById(id + '_emps_a').innerHTML,
                        'cur_stat': orderObj['cur_stat'], 'new_stat': orderObj['new_stat'], 'emp_id': orderObj['emp_id'], 'apv_sub_order': orderObj['apv_sub_order']
                    });
                } else {
                    approves.push({
                        'id': id, 'no': orderObj['no'], 'status': orderObj['status'], 'emps': orderObj['emps'],
                        'cur_stat': orderObj['cur_stat'], 'new_stat': orderObj['new_stat'], 'emp_id': orderObj['emp_id'], 'apv_sub_order': orderObj['apv_sub_order']
                    });
                }
            } else {
                for (var apv_sub_order in orderObj['sub_orders']) {
                    var id = 'approve_order' + apv_order + '_' + apv_sub_order;
                    var subOrderObj = orderObj['sub_orders'][apv_sub_order];
                    var newStat = "";
                    if (subOrderObj["cur_stat"]=="1" && orderObj["new_stat"]=="sasimodosi") newStat = "sasimodosi";

                    // 描画済みセルは書き換えない
                    if (document.getElementById(id + '_no_a')) {
                        approves.push({
                            'id': id, 'no': document.getElementById(id + '_no_a').innerHTML, 'status': document.getElementById(id + '_status_a').innerHTML, 'emps': document.getElementById(id + '_emps_a').innerHTML,
                            'cur_stat': subOrderObj['cur_stat'], 'new_stat': newStat, 'emp_id': subOrderObj['emp_id'], 'apv_sub_order': orderObj['apv_sub_order']
                        });
                    } else {
                        approves.push({
                            'id': id, 'no': subOrderObj['no'], 'status': subOrderObj['status'], 'emps': subOrderObj['emps'],
                            'cur_stat': subOrderObj['cur_stat'], 'new_stat': newStat, 'emp_id': subOrderObj['emp_id'], 'apv_sub_order': orderObj['apv_sub_order']
                        });
                    }
                }
            }
        }
        while (approves.length % 5 > 0) approves.push({'no': '', 'status': '', 'emps': '', 'cur_stat': '', 'new_stat': '', 'emp_id': '', 'apv_sub_order':''});

        var img_src = (document.getElementById('imgA')) ? document.getElementById('imgA').src : '';

        var approve_orders_table = document.getElementById('approve_ordersA');
        if(!approve_orders_table) return;
        while (approve_orders_table.rows.length > 0) {
            approve_orders_table.deleteRow(-1);
        }

        for (var row_no = 0, row_max = approves.length / 5; row_no < row_max; row_no++) {
            var row1 = approve_orders_table.insertRow(-1);
            row1.style.backgroundColor = '#eff9fd';
            row1.style.height = '22px';
            row1.style.textAlign = 'center';

            var row2 = approve_orders_table.insertRow(-1);
            row2.style.height = '22px';
            row2.style.textAlign = 'center';

            var row3 = approve_orders_table.insertRow(-1);
            row3.style.height = '100px';
            row3.style.textAlign = 'center';

            for (var cell_no = row_no * 5, cell_max = cell_no + 5; cell_no < cell_max; cell_no++) {
                var cell1 = row1.insertCell(-1);
                cell1.style.width = '20%';
                cell1.id = approves[cell_no]['id'] + '_no_a';
                cell1.innerHTML = approves[cell_no]['no'];

                var cell2 = row2.insertCell(-1);
                cell2.id = approves[cell_no]['id'] + '_status_a';
                cell2.innerHTML = approves[cell_no]['status'];

                var cell3 = row3.insertCell(-1);
                cell3.id = approves[cell_no]['id'] + '_emps_a';
                cell3.innerHTML = approves[cell_no]['emps'];

                if (approves[cell_no]['new_stat']=="sasimodosi"){
                    cell1.style.backgroundColor = "#b1f8b7";
                    cell2.style.backgroundColor = "#b1f8b7";
                    cell3.style.backgroundColor = "#b1f8b7";
                }
            }
        }

        var images = document.getElementsByTagName('img');
        for (var i = 0; i < images.length; i++) {
            if (images[i].className != 'imprint_image') {
                continue;
            }

            var tmp_img_src = images[i].src;
            tmp_img_src = tmp_img_src.substr(tmp_img_src.lastIndexOf('/') + 1);
            if (!imprint_images[tmp_img_src]) {
                var new_img = new Image(images[i].width, images[i].height);
                new_img.className = 'imprint_image';
                new_img.src = tmp_img_src;
                imprint_images[tmp_img_src] = new_img;
            }

            var parent = images[i].parentNode;
            parent.removeChild(images[i]);
            parent.insertBefore(imprint_images[tmp_img_src], parent.firstChild);
        }
        if (!document.getElementById('targetA')) return;

        var img = spacer_imgA;
        switch (img_src) {
        case accepted_img.src:
            img = accepted_img;
            document.getElementById('targetA').style.backgroundColor = '#ffffff';
            document.apply.approveA.value = '1';
            break;
        case hinin_img.src:
            img = hinin_img;
            document.getElementById('targetA').style.backgroundColor = '#ffffff';
            document.apply.approveA.value = '2';
            break;
        case sasimodosi_img.src:
            img = sasimodosi_img;
            document.getElementById('targetA').style.backgroundColor = '#ffffff';
            document.apply.approveA.value = '3';
            break;
        default:
            break;
        }
        document.getElementById('targetA').removeChild(document.getElementById('imgA'));
        img.id = 'imgA';
        document.getElementById('targetA').appendChild(img);
        new Y.DD.Drop({node: '#targetA'});

        if (target_apv_order) sasimodosi_operation("A", document.apply.approveA.value);
    }


    <? //==================================================================== ?>
    <? // 実施側 押印欄を初期状態にして埋める                                 ?>
    <? //==================================================================== ?>
    function refreshApproveOrdersB(target_apv_order) {
        updateDOM();
        var approves = [];
        for (var apv_order in approve_ordersB) {
            if (apv_order == target_apv_order) approve_ordersB[apv_order]['opened'] = true;

            var orderObj = approve_ordersB[apv_order];
            if (!orderObj['opened']) {
                var id = 'approve_order' + apv_order;

                // 描画済みセルは書き換えない
                if (document.getElementById(id + '_no_b')) {
                    approves.push({
                        'id': id, 'no': document.getElementById(id + '_no_b').innerHTML, 'status': document.getElementById(id + '_status_b').innerHTML, 'emps': document.getElementById(id + '_emps_b').innerHTML,
                        'cur_stat': orderObj['cur_stat'], 'new_stat': orderObj['new_stat'], 'emp_id': orderObj['emp_id'], 'apv_sub_order': orderObj['apv_sub_order']
                    });
                } else {
                    approves.push({
                        'id': id, 'no': orderObj['no'], 'status': orderObj['status'], 'emps': orderObj['emps'],
                        'cur_stat': orderObj['cur_stat'], 'new_stat': orderObj['new_stat'], 'emp_id': orderObj['emp_id'], 'apv_sub_order': orderObj['apv_sub_order']
                    });
                }
            } else {
                for (var apv_sub_order in orderObj['sub_orders']) {
                    var id = 'approve_order' + apv_order + '_' + apv_sub_order;
                    var subOrderObj = orderObj['sub_orders'][apv_sub_order];
                    var newStat = "";
                    if (subOrderObj["cur_stat"]=="1" && orderObj["new_stat"]=="sasimodosi") newStat = "sasimodosi";

                    // 描画済みセルは書き換えない
                    if (document.getElementById(id + '_no_b')) {
                        approves.push({
                            'id': id, 'no': document.getElementById(id + '_no_b').innerHTML, 'status': document.getElementById(id + '_status_b').innerHTML, 'emps': document.getElementById(id + '_emps_b').innerHTML,
                            'cur_stat': subOrderObj['cur_stat'], 'new_stat': newStat, 'emp_id': subOrderObj['emp_id'], 'apv_sub_order': orderObj['apv_sub_order']
                        });
                    } else {
                        approves.push({
                            'id': id, 'no': subOrderObj['no'], 'status': subOrderObj['status'], 'emps': subOrderObj['emps'],
                            'cur_stat': subOrderObj['cur_stat'], 'new_stat': newStat, 'emp_id': subOrderObj['emp_id'], 'apv_sub_order': orderObj['apv_sub_order']
                        });
                    }
                }
            }
        }

        while (approves.length % 5 > 0) approves.push({'no': '', 'status': '', 'emps': '', 'cur_stat': '', 'new_stat': '', 'emp_id': '', 'apv_sub_order':''});

        var img_src = (document.getElementById('imgB')) ? document.getElementById('imgB').src : '';

        var approve_orders_table = document.getElementById('approve_ordersB');
        if(!approve_orders_table) return;
        while (approve_orders_table.rows.length > 0) {
            approve_orders_table.deleteRow(-1);
        }

        for (var row_no = 0, row_max = approves.length / 5; row_no < row_max; row_no++) {
            var row1 = approve_orders_table.insertRow(-1);
            row1.style.backgroundColor = '#eff9fd';
            row1.style.height = '22px';
            row1.style.textAlign = 'center';

            var row2 = approve_orders_table.insertRow(-1);
            row2.style.height = '22px';
            row2.style.textAlign = 'center';

            var row3 = approve_orders_table.insertRow(-1);
            row3.style.height = '100px';
            row3.style.textAlign = 'center';

            for (var cell_no = row_no * 5, cell_max = cell_no + 5; cell_no < cell_max; cell_no++) {
                var cell1 = row1.insertCell(-1);
                cell1.style.width = '20%';
                cell1.id = approves[cell_no]['id'] + '_no_b';
                cell1.innerHTML = approves[cell_no]['no'];

                var cell2 = row2.insertCell(-1);
                cell2.id = approves[cell_no]['id'] + '_status_b';
                cell2.innerHTML = approves[cell_no]['status'];

                var cell3 = row3.insertCell(-1);
                cell3.id = approves[cell_no]['id'] + '_emps_b';
                cell3.innerHTML = approves[cell_no]['emps'];

                if (approves[cell_no]['new_stat']=="sasimodosi"){
                    cell1.style.backgroundColor = "#b1f8b7";
                    cell2.style.backgroundColor = "#b1f8b7";
                    cell3.style.backgroundColor = "#b1f8b7";
                }
            }
        }

        var images = document.getElementsByTagName('img');
        for (var i = 0; i < images.length; i++) {
            if (images[i].className != 'imprint_image') {
                continue;
            }

            var tmp_img_src = images[i].src;
            tmp_img_src = tmp_img_src.substr(tmp_img_src.lastIndexOf('/') + 1);
            if (!imprint_images[tmp_img_src]) {
                var new_img = new Image(images[i].width, images[i].height);
                new_img.className = 'imprint_image';
                new_img.src = tmp_img_src;
                imprint_images[tmp_img_src] = new_img;
            }

            var parent = images[i].parentNode;
            parent.removeChild(images[i]);
            parent.insertBefore(imprint_images[tmp_img_src], parent.firstChild);
        }

        if (!document.getElementById('targetB')) return;

        var imgB = spacer_imgB;
        switch (img_src) {
        case operated_img.src:
            imgB = operated_img;
            document.getElementById('targetB').style.backgroundColor = '#ffffff';
            document.apply.approveB.value = '1';
            break;
        case hinin_img.src:
            imgB = hinin_img;
            document.getElementById('targetB').style.backgroundColor = '#ffffff';
            document.apply.approveB.value = '2';
            break;
        case sasimodosi_img.src:
            imgB = sasimodosi_img;
            document.getElementById('targetB').style.backgroundColor = '#ffffff';
            document.apply.approveB.value = '3';
            break;
        default:
            break;
        }
        document.getElementById('targetB').removeChild(document.getElementById('imgB'));
        imgB.id = 'imgB';
        document.getElementById('targetB').appendChild(imgB);
        new Y.DD.Drop({node: '#targetB'});

        if (target_apv_order) sasimodosi_operation("B", document.apply.approveB.value);
    }




    <? //==================================================================== ?>
    <? // 印刷画面を開く                                                      ?>
    <? //==================================================================== ?>
    var approve_detail_print_window_object = null;
    function trySubmitPrint(printmode){
        if (printmode=="text") {
            window.open('summary_naiyo_print.php?session=<?=$session?>&pt_id=<?=$ptif_id?>&emp_id=<?=$row_apply["emp_id"]?>&sum_id=<?=$summary_id?>&mode=flowsheet', 'naiyo_print', 'width=800,height=600,scrollbars=yes');
            return;
        }
        if (approve_detail_print_window_object && !approve_detail_print_window_object.closed){
            document.approve_print_form.submit();
          return;
        }
      var h = 700;
      var w = 670;
      var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=" + w + ",height=" + h;
      approve_detail_print_window_object = window.open('', 'approve_detail_print_window', option);
      setTimeout("trySubmitPrint()", 500);
    }


    <? //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ?>
    <? // 押印ドラッグを全部元に戻す                                          ?>
    <? //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  ?>
    function reset_imprint(isResetA, isResetB) {
        if (isResetA) {
            if (document.getElementById("applyA1")) document.getElementById("applyA1").style.display = "";
            if (document.getElementById("applyA1")) YAHOO.util.Dom.setXY(document.getElementById("applyA1"), applyA1_pos);

            if (document.getElementById("applyA2")) document.getElementById("applyA2").style.display = "";
            YAHOO.util.Dom.setXY(document.getElementById("applyA2"), applyA2_pos);

            if (document.getElementById("applyA3")) document.getElementById("applyA3").style.display = "";
            YAHOO.util.Dom.setXY(document.getElementById("applyA3"), applyA3_pos);

            if (document.getElementById("targetA")) document.getElementById("targetA").style.backgroundColor = "#ffcccc";
            if (document.getElementById("imgA")) document.getElementById("imgA").src = "img/spacer.gif";
            if (document.getElementById("approveA")) document.getElementById("approveA").selectedIndex = 0;
        }

        if (isResetB) {
            if (document.getElementById("applyB1")) document.getElementById("applyB1").style.display = "";
            if (document.getElementById("applyB1")) YAHOO.util.Dom.setXY(document.getElementById("applyB1"), applyB1_pos);

            if (document.getElementById("targetB")) document.getElementById("targetB").style.backgroundColor = "#ffcccc";
            if (document.getElementById("imgB")) document.getElementById("imgB").src = "img/spacer.gif";
            if (document.getElementById("approveB")) document.getElementById("approveB").selectedIndex = 0;
        }

        sasimodosi_operation("", 0);// 差し戻しをクリア
    }

    function init_imprint(){
        if (document.getElementById("approveA")) {
            document.getElementById("approveA").selectedIndex = <?=(int)$self_apv_stat_accepted < 4 ? (int)$self_apv_stat_accepted : 0 ?>;
            if (<?=(int)$self_apv_stat_accepted < 4 ? (int)$self_apv_stat_accepted : 0 ?>){
                set_target(document.getElementById("approveA").value, 'targetA');
            }
        }
        if (document.getElementById("approveB")) {
            document.getElementById("approveB").selectedIndex = <?=(int)$self_apv_stat_operated < 4 ? (int)$self_apv_stat_operated : 0 ?>;
            if (<?=(int)$self_apv_stat_operated < 4 ? (int)$self_apv_stat_operated : 0 ?>){
                set_target(document.getElementById("approveB").value, 'targetB');
            }
        }
    }

</script>



</body>
</html>

<? pg_close($con); ?>

<?
function getHininOrSasimodosi($con, $fname, $pt_id, $apply_id, $apv_order, $emp_id)
{
  $hors = array("hinin"=>"否認", "sasimodosi"=>"差戻") ;

  $sql2 =
    " select" .
    "   hinin_or_sasimodosi" .
    " from" .
    "   sum_apply_hinin_sasimodosi" .
    " where" .
    "       ptif_id = '" . $pt_id . "'" .
    "   and apply_id = " . $apply_id .
    "   and from_apv_order = " . $apv_order .
    "   and from_emp_id <> '" . $emp_id . "'" ;

  $sel2 = @select_from_table($con, $sql2, "" , $fname) ;
  $stat2 = @pg_fetch_array($sel2) ;
  $hos = @$stat2["hinin_or_sasimodosi"] ;

  if ($hos) return $hors[$hos] ;
  return "" ;
}
?>
