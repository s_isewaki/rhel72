<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>委員会・WG | 承認待ち議事録</title>
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 委員会・WG権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 承認待ち議事録情報の取得
$sql = "select pjt_schd_id, prcd_subject, prcd_create_time, empmst.emp_lt_nm, empmst.emp_ft_nm from proceeding
		inner join empmst on proceeding.prcd_create_emp_id = empmst.emp_id 
";
$cond = "where prcd_status = '1' and pjt_schd_id in (select pjt_schd_id from prcdaprv where prcdaprv_emp_id = '$emp_id' and prcdaprv_date is null) order by prcd_create_time";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}


// 委員会・WK管理権限を取得
$pjt_admin_auth = check_authority($session, 82, $fname);


?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td {border:#5279a5 solid 1px; padding:3px; }
.list2 {border-collapse:collapse;}
.list2 td {border:#dfd95e solid 1px; padding:3px; }
</style>

<script type="text/javascript">
function openPrcdWin(pjt_schd_id) {
	window.open('proceeding_approve.php?session=<? echo($session) ?>&pjt_schd_id=' + pjt_schd_id + '&pnt_url=<? echo(urlencode("proceeding_list2.php?session=$session")); ?>', 'newwin2', 'width=640,height=700,scrollbars=yes');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" colspan="2" class="spacing"><a href="project_menu.php?session=<? echo($session); ?>"><img src="img/icon/b03.gif" width="32" height="32" border="0" alt="委員会・WG"></a></td>

<? if ($pjt_admin_auth == "1") { ?>
	<? if($adm_flg=="t") {?>
	<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
	<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="project_menu.php?session=<? echo($session); ?>&entity=1"><b>ユーザ画面へ</b></a></font></td>
	<? } else {?>
	<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
	<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">　<a href="project_menu_adm.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
	<? }?>
<? }else{ ?>
	<td width="100%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
<? } ?>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<? if($adm_flg=="t") {?>
	<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu_adm.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<? } else {?>
	<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<? }?>

<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="proceeding_list1.php?session=<? echo($session); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請中議事録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="proceeding_list2.php?session=<? echo($session); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>承認待ち議事録</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<table>
<tr height="22"><td>　</td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list1">

<tr bgcolor="#f6f9ff" height="22">
<td align="center" bgcolor="#defafa" width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認/差戻し</font></td>
<td align="center" bgcolor="#defafa" width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">作成日</font></td>
<td align="center" bgcolor="#defafa" width="*"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">議題</font></td>
<td align="center" bgcolor="#defafa" width="25%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG名</font></td>
<td align="center" bgcolor="#defafa" width="15%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">議事録申請者</font></td>
</tr>

<?

$row_cnt = 0;
while ($row = pg_fetch_array($sel)) {
	
	if($row_cnt % 2 == 0)
	{
		$bgcolor_str = "";
	}
	else
	{
		$bgcolor_str = "bgcolor=\"#fefcdf\"";
	}
	
	$tmp_pjt_schd_id = $row["pjt_schd_id"];
	$tmp_subject = $row["prcd_subject"];
	$tmp_create_time = $row["prcd_create_time"];
	$tmp_create_name = $row["emp_lt_nm"].$row["emp_ft_nm"];
	
	$tmp_create_date = preg_replace("/^(\d{4})(\d{2})(\d{2}).*/", "$1/$2/$3", $tmp_create_time);

	//承認階層が自分のレベルまで来ているかチェックする
	$sql3 = "select a.prcdaprv_order as my_order ,b.order_level from prcdaprv a ,
			(select MIN(prcdaprv_order) as order_level from prcdaprv where pjt_schd_id='$tmp_pjt_schd_id' and prcdaprv_date is null) b ";
	$cond3 = "where a.prcdaprv_emp_id='$emp_id' and a.pjt_schd_id='$tmp_pjt_schd_id' and prcdaprv_date is null";
	$sel3 = select_from_table($con, $sql3, $cond3, $fname);
	if ($sel3 == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//自分の承認階層数
	$my_order = pg_fetch_result($sel3, 0, "my_order");
	
	//現在の承認階層数
	$order_level = pg_fetch_result($sel3, 0, "order_level");
	
	if($my_order == $order_level)
	{
		//自分の承認階層と現在の承認階層が同じであったら表示する
		//改修前のデータはカラムが無いのでデータなしの場合も表示する

		$sql2 = "select pjt_name from project";
		$cond2 = "where pjt_id = (select pjt_id from proschd where pjt_schd_id = '$tmp_pjt_schd_id' union select pjt_id from proschd2 where pjt_schd_id = '$tmp_pjt_schd_id')";
		$sel2 = select_from_table($con, $sql2, $cond2, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_pjt_name = pg_fetch_result($sel2, 0, "pjt_name");

		echo("<tr height=\"22\">\n");
		echo("<td $bgcolor_str><input type=\"button\" style=\"margin-left:1em;width=5.5em;\" value=\"承認/差戻し\" onclick=\"openPrcdWin('$tmp_pjt_schd_id');\"></td>\n");
		echo("<td $bgcolor_str align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_create_date</font></td>\n");
		echo("<td $bgcolor_str><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_subject</font></td>\n");
		echo("<td $bgcolor_str><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_pjt_name</font></td>\n");
		echo("<td $bgcolor_str align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_create_name</font></td>\n");
		echo("</tr>\n");
	}
	
	$row_cnt++;
}
?>
</table>
</td>
</tr>
</table>
<? pg_close($con); ?>
</body>
</html>
