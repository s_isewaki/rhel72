<?

	//----------------------------------------------------------------------------------
	// インクルードパス設定
	//----------------------------------------------------------------------------------
	set_include_path(
		get_include_path()
		. PATH_SEPARATOR . '/var/www/html/comedix'
	);

	//----------------------------------------------------------------------------------
	// モジュールロード
	//----------------------------------------------------------------------------------
	require_once( "cl_common_log_class.inc"						);
	require_once( "cl_common_apply.inc"							);
	require_once( "Cmx.php"										);
	require_once( 'MDB2.php'									);
	//require_once( "get_values.ini"								);
	require_once( "cl_application_workflow_common_class.php"	);
	require_once( "cl_common_apply.inc"							);
	require_once(dirname(__FILE__) . "/../common/cl_workflow_api.php");


	$log = new cl_common_log_class(basename(__FILE__));
	$log->debug(basename(__FILE__)." START");
	$fname=__FILE__;

	//----------------------------------------------------------------------------------
	// 引数の取得
	//----------------------------------------------------------------------------------
	$log->debug('引数の取得　開始',__FILE__,__LINE__);
	$log->debug('$argc:'.$argc,__FILE__,__LINE__);
	for($i=0;$i<$argc;$i++){
		$log->debug('■■■$argv['.$i.']:'.$argv[$i],__FILE__,__LINE__);
	}
	if ( $argc > 0 ) {
		foreach( $argv as $elm ){
			if ( ( $arrTmp = explode('=', $elm) ) && count($arrTmp) > 1 ) {
				$CMDPARAM[$arrTmp[0]] = $arrTmp[1];
			}
		}
	}
	else{
		exit(1);
	}

	// 管理番号取得
	$cmdparam_short_wkfw_name = $CMDPARAM[ "short_wkfw_name"	];

	// ワークフロー申請ID取得
	$cmdparam_wkfw_id = $CMDPARAM[ "wkfw_id"	];

	// ログインユーザemp_id
	$cmdparam_login_emp_id = $CMDPARAM[ "login_emp_id"	];

	// レベル
	$cmdparam_level = str_replace(array("\r\n","\r","\n"),"",$CMDPARAM["level"]);

	$log->debug('●$cmdparam_short_wkfw_name:'.$cmdparam_short_wkfw_name,__FILE__,__LINE__);
	$log->debug('●$cmdparam_wkfw_id        :'.$cmdparam_wkfw_id,__FILE__,__LINE__);
	$log->debug('●$cmdparam_login_emp_id   :'.$cmdparam_login_emp_id,__FILE__,__LINE__);
	$log->debug('●$cmdparam_level          :'.$cmdparam_level,__FILE__,__LINE__);

	//short_wkfw_name=c350 wkfw_id=LUPDH00000001 login_emp_id=system level=1
	$log->debug('引数の取得　終了',__FILE__,__LINE__);

	//----------------------------------------------------------------------------------
	// データベースに接続
	//----------------------------------------------------------------------------------
	$log->debug('データベースに接続(con)　開始',__FILE__,__LINE__);
	$con = connect2db($fname);
	$log->debug('データベースに接続(con)　終了',__FILE__,__LINE__);

	$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
	$mdb2 = MDB2::connect(CMX_DB_DSN);
	if (PEAR::isError($mdb2)) {
		$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
		$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);
		exit(1);
	}
	$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

	//----------------------------------------------------------------------------------
	// 共通モジュールインスタンス生成
	//----------------------------------------------------------------------------------
	$log->debug('共通モジュールインスタンス生成　開始',__FILE__,__LINE__);
	$obj = new cl_application_workflow_common_class($con, $fname);
	$wkfw_api = new cl_workflow_api($mdb2);
	$wkfw_api->setArrEmpmstByEmpId($cmdparam_login_emp_id);
	$log->debug('共通モジュールインスタンス生成　終了',__FILE__,__LINE__);

	//----------------------------------------------------------------------------------
	// 同僚評価自動申請管理テーブルデータ取得
	//----------------------------------------------------------------------------------
	$log->debug('同僚評価自動申請管理テーブルデータ取得　開始',__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/search/cl_ldr_cwrkr_asmnt_auto_apply_manage_list_model.php");
	$cl_ldr_cwrkr_asmnt_auto_apply_manage_list_model = new cl_ldr_cwrkr_asmnt_auto_apply_manage_list_model($mdb2,$cmdparam_login_emp_id);
	$log->debug('getList level = '.$cmdparam_level,__FILE__,__LINE__);
	$auto_apply_manage_list=$cl_ldr_cwrkr_asmnt_auto_apply_manage_list_model->getList($cmdparam_level);

	$log->debug( 'count(data):'.count($auto_apply_manage_list) ,__FILE__,__LINE__);
	$log->debug('同僚評価自動申請管理テーブルデータ取得　終了',__FILE__,__LINE__);

	//require_once(dirname(__FILE__) . "/../model/sequence/cl_apply_id_seq_model.php");
	//require_once(dirname(__FILE__) . "/../model/workflow/cl_wkfwmst_model.php");
	require_once(dirname(__FILE__) . "/../model/workflow/cl_apply_model.php");
	require_once(dirname(__FILE__) . "/../model/workflow/cl_applyapv_model.php");
	require_once(dirname(__FILE__) . "/../model/search/cl_ldr_cwrkr_assmnt_acknowledger_manage_list_model.php");
	require_once(dirname(__FILE__) . "/../model/ladder/cl_ldr_cwrkr_asmnt_auto_apply_manage_model.php");
	require_once(dirname(__FILE__) . "/../model/sequence/cl_levelup_evaluation_colleague_id_seq_model.php");

	$cl_ldr_cwrkr_asmnt_auto_apply_manage_model = new cl_ldr_cwrkr_asmnt_auto_apply_manage_model($mdb2,$cmdparam_login_emp_id);

	//2012/06/06 K.Fujii ins(s)
	require_once(dirname(__FILE__) . "/../model/ladder/cl_ldr_cwrkr_assmnt_acknowledger_manage_model.php");
	$cl_ldr_cwrkr_assmnt_acknowledger_manage_model = new cl_ldr_cwrkr_assmnt_acknowledger_manage_model($mdb2,$cmdparam_login_emp_id);
	//2012/06/06 K.Fujii ins(e)

	//----------------------------------------------------------------------------------
	// 同僚評価自動申請管理テーブルデータ毎処理
	//----------------------------------------------------------------------------------
	$log->debug('同僚評価自動申請管理テーブルデータ毎処理　開始',__FILE__,__LINE__);
	$draft="off";
	for($i=0;$i<count($auto_apply_manage_list);$i++){

		//------------------------------------------------------------------------------
		// 同僚評価自動申請管理テーブルデータ取得
		//------------------------------------------------------------------------------
		$log->debug('同僚評価自動申請管理テーブルデータ取得　開始',__FILE__,__LINE__);
		$auto_apply_manage=$auto_apply_manage_list[$i];
		foreach( $auto_apply_manage as $key => $value ){
			$log->debug('■■■$auto_apply_manage['.$key.']:'.$value);
		}
		$log->debug('同僚評価自動申請管理テーブルデータ取得　終了',__FILE__,__LINE__);

		//------------------------------------------------------------------------------
		// 申請者職員情報設定
		//------------------------------------------------------------------------------
		$log->debug('申請者職員情報設定　開始',__FILE__,__LINE__);
		$arr_empmst = array();
		$arr_empmst["emp_id"		]	= $auto_apply_manage["from_emp_id"];
		$arr_empmst["emp_class"		]	= $auto_apply_manage["from_emp_class"];
		$arr_empmst["emp_attribute"	]	= $auto_apply_manage["from_emp_attr"];
		$arr_empmst["emp_dept"		]	= $auto_apply_manage["from_emp_dept"];
		$arr_empmst["emp_room"		]	= $auto_apply_manage["from_emp_room"];


		$log->debug('申請者職員情報設定　終了',__FILE__,__LINE__);

		//------------------------------------------------------------------------------
		// トランザクション開始
		//------------------------------------------------------------------------------
		$log->debug("トランザクション開始(con) START",__FILE__,__LINE__);
		pg_query($con, "begin");
		$log->debug("トランザクション開始(con) END",__FILE__,__LINE__);

		$log->debug("トランザクション開始(mdb2) START",__FILE__,__LINE__);
		$mdb2->beginTransaction();
		$log->debug("トランザクション開始(mdb2) END",__FILE__,__LINE__);

		//##############################################################################
		// ワークフローデータ登録
		//##############################################################################



		//------------------------------------------------------------------------------
		// 新規申請(apply_id)採番
		//------------------------------------------------------------------------------
		$log->debug('新規申請(apply_id)採番　開始',__FILE__,__LINE__);
		$apply_id = $wkfw_api->get_apply_id();
		//$cl_apply_id_model = new cl_apply_id_seq_model($mdb2,$cmdparam_login_emp_id);
		//$apply_id=$cl_apply_id_model->getApplyId();
		$log->debug("■NEW apply_id:".$apply_id,__FILE__,__LINE__);
		$log->debug('新規申請(apply_id)採番　終了',__FILE__,__LINE__);


 		//------------------------------------------------------------------------------
		// 新規申請(apply_no)採番
		//------------------------------------------------------------------------------
		$log->debug('新規申請(apply_no)採番　開始',__FILE__,__LINE__);
		$apply_no = null;
		if($draft != "on"){
			$apply_no = $wkfw_api->get_apply_no();
		}
		$log->debug("■NEW apply_no:".$apply_no,__FILE__,__LINE__);
		$log->debug('新規申請(apply_no)採番　終了',__FILE__,__LINE__);

 		//------------------------------------------------------------------------------
		// ワークフロー情報取得
 		//------------------------------------------------------------------------------
		$log->debug('ワークフロー情報取得　開始',__FILE__,__LINE__);
		$arr_wkfw = $wkfw_api->get_wkfw_info_by_wkfw_id($cmdparam_wkfw_id);
		$log->debug('ワークフロー情報取得　終了',__FILE__,__LINE__);

		//------------------------------------------------------------------------------
		// 申請登録
		//------------------------------------------------------------------------------
		$log->debug('申請登録　開始',__FILE__,__LINE__);
		$wkfw_api->regist_apply(
			$apply_id					,		// $apply_id
			$cmdparam_wkfw_id			,		// $wkfw_id
			""							,		// $content
			""							,		// $apply_title
			$draft						,		// $draft
			$apply_no					,		// $apply_no
			false						,		// $notice_sel_flg
			1							,		// $wkfw_history_no
			null						,		// $wkfwfile_history_no
			$arr_empmst					,		// $arr_empmst
			$arr_wkfw							// $arr_wkfw
		);
		$log->debug('申請登録　終了',__FILE__,__LINE__);

		//------------------------------------------------------------------------------
		// 同僚評価承認者管理テーブルデータ取得
		//------------------------------------------------------------------------------
		$log->debug('同僚評価承認者管理テーブルデータ取得　開始',__FILE__,__LINE__);
		$cl_ldr_cwrkr_assmnt_acknowledger_manage_list_model = new cl_ldr_cwrkr_assmnt_acknowledger_manage_list_model($mdb2,$cmdparam_login_emp_id);
		$acknowledger_manage_list = $cl_ldr_cwrkr_assmnt_acknowledger_manage_list_model->getList($auto_apply_manage["cwrkr_asmnt_auto_apply_manage_id"]);
		$approve_num=count($acknowledger_manage_list);
		$log->debug('同僚評価承認者管理テーブルデータ取得　終了',__FILE__,__LINE__);

		//------------------------------------------------------------------------------
		// ●承認登録
		//------------------------------------------------------------------------------
		//$apvteacher_info = $wkfw_api->get_apvteacher_info($_REQUEST);
		//$apvteacher_num = $_POST['apvteacher_num'];
		$apvteacher_info = array();
		$apvteacher_num = 0;

		$log->debug('承認登録　開始',__FILE__,__LINE__);
		$apv_info = get_apv_info($acknowledger_manage_list);
		$wkfw_api->regist_approval_data($cmdparam_wkfw_id,$apply_id,$draft,$apv_info,$apvteacher_num,$apvteacher_info);
		$log->debug('承認登録　終了',__FILE__,__LINE__);


		//------------------------------------------------------------------------------
		// ●承認者候補登録
		//------------------------------------------------------------------------------
		$log->debug('承認者候補登録　開始',__FILE__,__LINE__);

		// 承認者候補取得
		//$arr_applyapvemp = $wkfw_api->get_applyapvemp_by_req($_REQUEST);
		$arr_applyapvemp = array();

		// 承認者候補登録
		$wkfw_api->regist_applyapvemp($apply_id, $arr_applyapvemp);

		$log->debug('承認者候補登録　終了',__FILE__,__LINE__);

		//------------------------------------------------------------------------------
		// ●添付ファイル登録
		// ※同僚評価申請は添付ファイルがないので処理しない。
		//------------------------------------------------------------------------------
		//$log->debug('添付ファイル登録　開始',__FILE__,__LINE__);
		//$wkfw_api->regist_applyfile_data($apply_id, $filename);
		//$log->debug('添付ファイル登録　終了',__FILE__,__LINE__);

		//------------------------------------------------------------------------------
		// ●非同期・同期受信登録
		//------------------------------------------------------------------------------
		$log->debug('非同期・同期受信登録　開始',__FILE__,__LINE__);
		if ($arr_wkfw["wkfw_appr"           ] == "2") {
			// 講師数取得
			//$apvteacher_num = $_POST['apvteacher_num'];
			$apvteacher_num = 0;

			// 非同期・同期受信取得
			$arr_applyasyncrecv = $wkfw_api->get_applyasyncrecv_info($apvteacher_num, $apv_info);

			// 非同期・同期受信登録
			$wkfw_api->regist_applyasyncrecv($apply_id, $arr_applyasyncrecv);
		}
		$log->debug('非同期・同期受信登録　終了',__FILE__,__LINE__);

		//------------------------------------------------------------------------------
		// ●申請結果通知登録
		// ※現在クリニカルラダーアプリでは、結果通知を行なう職員ID（カンマ区切り）はＩ／Ｆされない。
		//------------------------------------------------------------------------------
		//$log->debug('申請結果通知登録　開始',__FILE__,__LINE__);
		//if ($notice_emp_id != "") {
		//	$arr_notice_emp_id	= explode(",", $notice_emp_id);
		//	$arr_rslt_ntc_div	= explode(",", $rslt_ntc_div);
		//	$wkfw_api->regist_applynotice($apply_id, $arr_notice_emp_id, $arr_rslt_ntc_div);
		//}
		//$log->debug('申請結果通知登録　終了',__FILE__,__LINE__);

		//------------------------------------------------------------------------------
		// ●前提とする申請書(申請用)登録
		// ※同僚評価表では、前提とする申請書(申請用)は設定しない。
		//------------------------------------------------------------------------------
		//$log->debug('前提とする申請書(申請用)登録　開始',__FILE__,__LINE__);
		//$arr_applyprecond = $wkfw_api->get_applyprecond_info($_REQUEST);
		//$wkfw_api->regist_applyprecond($apply_id, $arr_applyprecond);
		//$log->debug('前提とする申請書(申請用)登録　終了',__FILE__,__LINE__);


		//------------------------------------------------------------------------------
		// 添付ファイルの移動
		// ※同僚評価表では、添付ファイルはない。
		//------------------------------------------------------------------------------
		//$log->debug('添付ファイルの移動　開始',__FILE__,__LINE__);
		//$wkfw_api->move_applyfile($apply_id ,$filename ,$file_id ,$session);
		//$log->debug('添付ファイルの移動　終了',__FILE__,__LINE__);

		//##############################################################################
		// レベルアップ同僚評価申請データ登録
		//##############################################################################

		$log->debug('レベルアップ同僚評価申請ＩＤ採番　開始',__FILE__,__LINE__);
		$lvlup_evltn_clge_id_seq = new cl_levelup_evaluation_colleague_id_seq_model($mdb2,$cmdparam_login_emp_id);
		$levelup_evaluation_colleague_id= $lvlup_evltn_clge_id_seq->getSeqId();
		$log->debug('レベルアップ同僚評価申請ＩＤ：'.$levelup_evaluation_colleague_id,__FILE__,__LINE__);
		$log->debug('レベルアップ同僚評価申請ＩＤ採番　終了',__FILE__,__LINE__);

		//------------------------------------------------------------------------------
		// テンプレートの項目をパラメータに設定
		//------------------------------------------------------------------------------
		$param=array();
		$param[ 'apply_id'				] = $apply_id;											// 申請ID
		$param[ 'levelup_apply_id'		] = $auto_apply_manage[ "levelup_apply_apply_id"	];	// レベルアップ申請の申請書ID
		$param[ 'levelup_emp_id'		] = $auto_apply_manage[ "cnddt_emp_id"				];	// 評価対象者の職員ID
		$param[ 'level'					] = $cmdparam_level;									// 申請レベル
		$param[ 'colleague_division'	] = $auto_apply_manage[ "colleague_division"		];	// 同僚区分
		$param[ 'cnddt_emp_id'			] = $auto_apply_manage[ "cnddt_emp_id"				];	// 評価対象者の職員ID
		$param[ 'cwrkr_emp_id'			] = $auto_apply_manage[ "cwrkr_emp_id"				];	// 同僚評価者の職員ID
		$param[ 'levelup_evaluation_id'	] = $levelup_evaluation_colleague_id;					// 自動申請処理で採番したID

/*
 * 同僚評価自動申請管理ID							cwrkr_asmnt_auto_apply_manage_id
 * ワークフロー申請ID（レベルアップ申請）			levelup_wkfw_apply_id
 * 申請書ID（レベルアップ申請）						levelup_apply_apply_id
 * 評価レベル										level
 * 評価対象者の職員ID								cnddt_emp_id
 * 評価対象者の部署									cnddt_emp_class
 * 評価対象者の課									cnddt_emp_attr
 * 評価対象者の科									cnddt_emp_dept
 * 評価対象者の室									cnddt_emp_room
 * 同僚区分											colleague_division
 * 同僚評価者の職員ID								cwrkr_emp_id
 * 同僚評価者の部署									cwrkr_emp_class
 * 同僚評価者の課									cwrkr_emp_attr
 * 同僚評価者の科									cwrkr_emp_dept
 * 同僚評価者の室									cwrkr_emp_room
 * 差出人職員ID										from_emp_id
 * 差出人の部署										from_emp_class
 * 差出人の課										from_emp_attr
 * 差出人の科										from_emp_dept
 * 差出人の室										from_emp_room
 * ワークフロー申請ID（レベルアップ同僚評価申請）	cwrkr_asmnt_wkfw_apply_id
 * 申請書ID（レベルアップ同僚評価申請）				cwrkr_asmnt_apply_apply_id
 * 処理区分											proc_flg
 * 削除フラグ　										delete_flg
 * データ作成日										create_date
 * データ作成者										create_user
 * データ更新日										update_date
 * データ更新者										update_user

 */






		//------------------------------------------------------------------------------
		// ハンドラー呼出
		//------------------------------------------------------------------------------
		$wkfw_api->load_handler("apply_regist", $apply_id, $cmdparam_wkfw_id, $cmdparam_short_wkfw_name, $param);

		//------------------------------------------------------------------------------
		// 同僚評価自動申請管理テーブルデータ更新（処理済）
		//------------------------------------------------------------------------------
		$auto_apply_manage["proc_flg"] = 9;
		//2012/06/06 K.Fujii ins(s)
		$auto_apply_manage["cwrkr_asmnt_apply_apply_id"] = $levelup_evaluation_colleague_id;
		$auto_apply_manage["cwrkr_asmnt_wkfw_apply_id"] = $apply_id;
		//2012/06/06 K.Fujii ins(e)
		//2012/06/06 K.Fujii upd(s)
		//$auto_apply_manage_model->update($auto_apply_manage);
		$cl_ldr_cwrkr_asmnt_auto_apply_manage_model->update($auto_apply_manage);
		//2012/06/06 K.Fujii upd(e)

		//2012/06/06 K.Fujii ins(s)
		//--------------------------------------------------------------------------
		// 同僚評価承認者管理テーブルデータ用配列作成
		//--------------------------------------------------------------------------
		$acknowledger_upd_param = array(
									"cwrkr_asmnt_auto_apply_manage_id"	=>$auto_apply_manage["cwrkr_asmnt_auto_apply_manage_id"],
									"cwrkr_asmnt_wkfw_apply_id"			=>$apply_id,
									"cwrkr_asmnt_apply_apply_id"		=>$levelup_evaluation_colleague_id,
								);
		//------------------------------------------------------------------------------
		// 同僚評価承認者管理テーブルデータ更新（同僚評価表ID登録）
		//------------------------------------------------------------------------------
		$cl_ldr_cwrkr_assmnt_acknowledger_manage_model->update_cwrkr_apply_id($acknowledger_upd_param);
		//2012/06/06 K.Fujii ins(e)

		//------------------------------------------------------------------------------
		// トランザクションをコミット
		//------------------------------------------------------------------------------
		pg_query($con, "commit");

		$log->debug("トランザクション確定 START",__FILE__,__LINE__);
		$mdb2->commit();
		$log->debug("トランザクション確定 END",__FILE__,__LINE__);

	}
	$log->debug('同僚評価自動申請管理テーブルデータ毎処理　終了',__FILE__,__LINE__);


	//----------------------------------------------------------------------------------
	// データベース接続を閉じる
	//----------------------------------------------------------------------------------
	$log->debug("データベース切断 START",__FILE__,__LINE__);
	pg_close($con);
	$mdb2->disconnect();
	$log->debug("データベース切断 END",__FILE__,__LINE__);

	$log->debug(basename(__FILE__)." END");

	$log->shutdown();



/**
 * 承認登録パラメータ取得
 * @param $cmdparam_wkfw_id：ワークフローＩＤ
 * @param $apply_id：申請ＩＤ
 * @param $cwrkr_assmnt_acknowledger_manage：承認者情報
 * @return 承認登録パラメータ
 */
function get_applyapv_param(
	$cmdparam_wkfw_id					,
	$apply_id							,
	$cwrkr_assmnt_acknowledger_manage
){
	$cl_applyapv=array();
	$cl_applyapv["wkfw_id"			] = $cmdparam_wkfw_id;
	$cl_applyapv["apply_id"			] = $apply_id;
	$cl_applyapv["apv_order"		] = $cwrkr_assmnt_acknowledger_manage["apv_order"];
	$cl_applyapv["emp_id"			] = $cwrkr_assmnt_acknowledger_manage["acknowledger_emp_id"];
	$cl_applyapv["apv_stat"			] = "0";
	$cl_applyapv["apv_date"			] = "";
	$cl_applyapv["apv_comment"		] = "";
	$cl_applyapv["st_div"			] = $cwrkr_assmnt_acknowledger_manage["st_div"];
	$cl_applyapv["deci_flg"			] = "t";
	$cl_applyapv["emp_class"		] = $cwrkr_assmnt_acknowledger_manage["acknowledger_emp_class"];
	$cl_applyapv["emp_attribute"	] = $cwrkr_assmnt_acknowledger_manage["acknowledger_emp_attr"];
	$cl_applyapv["emp_dept"			] = $cwrkr_assmnt_acknowledger_manage["acknowledger_emp_dept"];
	$cl_applyapv["emp_st"			] = "";
	$cl_applyapv["apv_fix_show_flg"	] = "t";
	$cl_applyapv["emp_room"			] = $cwrkr_assmnt_acknowledger_manage["acknowledger_emp_room"];
	$cl_applyapv["apv_sub_order"	] = $cwrkr_assmnt_acknowledger_manage["apv_sub_order"];
	$cl_applyapv["multi_apv_flg"	] = "t";
	$cl_applyapv["next_notice_div"	] = "";
	$cl_applyapv["parent_pjt_id"	] = "";
	$cl_applyapv["child_pjt_id"		] = "";
	$cl_applyapv["other_apv_flg"	] = "f";
	$cl_applyapv["draft_flg"		] = "f";
	$cl_applyapv["eval_content"		] = null;
	return $cl_applyapv;
}


/**
 * 申請登録パラメータ取得
 * @param $cmdparam_wkfw_id：ワークフローＩＤ
 * @param $apply_id：申請ＩＤ
 * @param $apply_no：申請番号
 * @param $cl_wkfwmst：ワークフローマスタ情報
 * @param $auto_apply_manage：同僚評価自動申請管理情報
 * @return 申請登録パラメータ
 */
 function get_apply_param(
	$cmdparam_wkfw_id						,
	$apply_id								,
	$apply_no								,
	$cl_wkfwmst								,
	$auto_apply_manage
){
	$cl_apply=array();
	$cl_apply["apply_id"				] = $apply_id;
	$cl_apply["wkfw_id"					] = $cmdparam_wkfw_id;
	$cl_apply["apply_content"			] = "";
	$cl_apply["emp_id"					] = $auto_apply_manage["from_emp_id"];
	$cl_apply["apply_stat"				] = "0";
	$cl_apply["apply_date"				] = date("YmdHi");
	$cl_apply["apply_title"				] = $cl_wkfwmst["wkfw_title"];
	$cl_apply["re_apply_id"				] = null;
	$cl_apply["apv_fix_show_flg"		] = "t";
	$cl_apply["apv_bak_show_flg"		] = "t";
	$cl_apply["emp_class"				] = $auto_apply_manage["from_emp_class"];
	$cl_apply["emp_attribute"			] = $auto_apply_manage["from_emp_attr"];
	$cl_apply["emp_dept"				] = $auto_apply_manage["from_emp_dept"];
	$cl_apply["apv_ng_show_flg"			] = "t";
	$cl_apply["emp_room"				] = $auto_apply_manage["from_emp_room"];
	$cl_apply["draft_flg"				] = "f";
	$cl_apply["wkfw_appr"				] = $cl_wkfwmst["wkfw_appr"];
	$cl_apply["wkfw_content_type"		] = $cl_wkfwmst["wkfw_content_type"];
	$cl_apply["apply_title_disp_flg"	] = $cl_wkfwmst["apply_title_disp_flg"];
	$cl_apply["apply_no"				] = $apply_no;
	$cl_apply["notice_sel_flg"			] = "f";
	$cl_apply["wkfw_history_no"			] = 1;
	$cl_apply["wkfwfile_history_no"		] = null;
	return $cl_apply;
}
/**
 * 承認者情報取得
 * @param $acknowledger_manage_list：同僚評価承認者管理情報
 * @return 承認者情報
 */
 function get_apv_info(
	$acknowledger_manage_list
){
	global $log;
	$apv_info=array();
	$apv_info["approve_num"] = count($acknowledger_manage_list);
	$idx=1;
	for ($i = 0; $i < $apv_info["approve_num"]; $i++) {

		$log->debug('★★★$idx:'.$idx);

		$acknowledger_manage=$acknowledger_manage_list[$i];


		/**
		 * 承認者情報配列に、承認者情報を格納する。
		 */
		$apv_info["regist_emp_id$idx"		] = $acknowledger_manage["acknowledger_emp_id"	];
		$apv_info["st_div$idx"				] = $acknowledger_manage["st_div"				];
		$apv_info["parent_pjt_id$idx"		] = "";
		$apv_info["child_pjt_id$idx"		] = "";
		$apv_info["apv_order$idx"			] = $acknowledger_manage["apv_order"			];
		$apv_info["apv_sub_order$idx"		] = $acknowledger_manage["apv_sub_order"		];
		$apv_info["multi_apv_flg$idx"		] = "t";
		$apv_info["next_notice_div$idx"		] = "";

		$log->debug('★★★$acknowledger_manage["acknowledger_emp_id"]:'.$acknowledger_manage["acknowledger_emp_id"	]);
		$log->debug('★★★$acknowledger_manage["st_div"             ]:'.$acknowledger_manage["st_div"	]);
		$log->debug('★★★$acknowledger_manage["apv_order"          ]:'.$acknowledger_manage["apv_order"	]);
		$log->debug('★★★$acknowledger_manage["apv_sub_order"      ]:'.$acknowledger_manage["apv_sub_order"	]);

		$log->debug("★★★\$apv_info[\"regist_emp_id$idx\"		]:".$apv_info["regist_emp_id$idx"		]);
		$log->debug("★★★\$apv_info[\"st_div$idx\"		]:".$apv_info["st_div$idx"		]);
		$log->debug("★★★\$apv_info[\"apv_order$idx\"		]:".$apv_info["apv_order$idx"		]);
		$log->debug("★★★\$apv_info[\"apv_sub_order$idx\"		]:".$apv_info["apv_sub_order$idx"		]);

		$idx++;

	}
	return $apv_info;
}

