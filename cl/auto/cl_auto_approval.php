<?

	//----------------------------------------------------------------------------------
	// インクルードパス設定
	//----------------------------------------------------------------------------------
	set_include_path(
		get_include_path()
		. PATH_SEPARATOR . '/var/www/html/comedix'
	);

	//----------------------------------------------------------------------------------
	// モジュールロード
	//----------------------------------------------------------------------------------
	//ini_set( 'display_errors', 1 );
	require_once( "cl_common_log_class.inc"						);
	require_once( "cl_common_apply.inc"							);
	require_once( "Cmx.php"										);
	require_once( 'MDB2.php'									);
	require_once( "about_session.php"							);
	require_once( "about_authority.php"							);
	require_once( "get_values.ini"								);
	require_once( "cl_common.ini"								);
	require_once( "cl_common_apply.inc"							);

	require_once("about_postgres.php");

	$log = new cl_common_log_class(basename(__FILE__));
	//logmsg(basename(__FILE__)." START");
	//logmsg(basename(__FILE__)." START");
	$log->info(basename(__FILE__)." START");

	$fname=$PHP_SELF;

	//----------------------------------------------------------------------------------
	// 引数の取得
	//----------------------------------------------------------------------------------
	$log->debug('$argc:'.$argc.__FILE__,__LINE__);
	for($i=0;$i<$argc;$i++){
		$log->debug('■■■$argv['.$i.']:'.$argv[$i],__FILE__,__LINE__);
	}
	if ( $argc > 0 ) {
		foreach( $argv as $elm ){
			if ( ( $arrTmp = explode('=', $elm) ) && count($arrTmp) > 1 ) {
				$CMDPARAM[$arrTmp[0]] = $arrTmp[1];
			}
		}
	}
	else{
		exit(1);
	}

	// 管理番号取得
	$cmdparam_short_wkfw_name = $CMDPARAM[ "short_wkfw_name"	];

	// ログインユーザemp_id
	$cmdparam_login_emp_id = $CMDPARAM[ "login_emp_id"	];

	// レベル
	// 2012/10/03 Yamagawa upd(s)
	//$cmdparam_level = $CMDPARAM[ "level"	];
	$cmdparam_level = str_replace(array("\r\n","\r","\n"),"",$CMDPARAM["level"]);
	// 2012/10/03 Yamagawa upd(e)

	//echo "abc";
	//----------------------------------------------------------------------------------
	// データベースに接続
	//----------------------------------------------------------------------------------
	$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
	$mdb2 = MDB2::connect(CMX_DB_DSN);
	if (PEAR::isError($mdb2)) {
		//$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
		exit;
	}
	$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

	//----------------------------------------------------------------------------------
	// 自動承認管理テーブルデータ取得（申請ID一覧取得）
	//----------------------------------------------------------------------------------
	require_once(dirname(__FILE__) . "/../model/search/cl_ldr_auto_approve_manage_list_model.php");
	$cl_ldr_auto_approve_manage_list_model = new cl_ldr_auto_approve_manage_list_model($mdb2,$cmdparam_login_emp_id);
	$auto_approve_apply_id_list=$cl_ldr_auto_approve_manage_list_model->getApplyIdList($cmdparam_short_wkfw_name,$cmdparam_level);

	$log->debug('count(data):'.count($auto_approve_apply_id_list),__FILE__,__LINE__);

	//ワークフローマスタモデルインスタンス
	$log->debug('require cl_wkfwmst_model.php Start',__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/workflow/cl_wkfwmst_model.php");
	$cl_wkfwmst_model = new cl_wkfwmst_model($mdb2,$cmdparam_login_emp_id);

	//承認者テーブルモデルインスタンス
	$log->debug('require cl_applyapv_model.php Start',__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/workflow/cl_applyapv_model.php");
	$cl_applyapv_model = new cl_applyapv_model($mdb2,$cmdparam_login_emp_id);

	//申請テーブルモデルインスタンス
	$log->debug('require cl_apply_model.php Start',__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/workflow/cl_apply_model.php");
	$cl_apply_model = new cl_apply_model($mdb2,$cmdparam_login_emp_id);

	//申請結果通知テーブルモデルインスタンス
	//require_once(dirname(__FILE__) . "/cl/model/workflow/cl_applynotice_model.php");
	//$cl_applynotice_model = new cl_applynotice_model($mdb2, $emp_id);

	//自動承認管理テーブルモデルインスタンス
	$log->debug('require cl_ldr_auto_approve_manage_model.php Start',__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/ladder/cl_ldr_auto_approve_manage_model.php");
	$log->debug('require cl_ldr_auto_approve_manage_model.php End',__FILE__,__LINE__);
	$cl_ldr_auto_approve_manage_model = new cl_ldr_auto_approve_manage_model($mdb2,$cmdparam_login_emp_id);

	//----------------------------------------------------------------------------------
	// 申請データ毎処理
	//----------------------------------------------------------------------------------
	for($i=0;$i<count($auto_approve_apply_id_list);$i++){

		//----------------------------------------------------------------------------------
		// トランザクション開始
		//----------------------------------------------------------------------------------
		$log->debug("トランザクション開始(con) START",__FILE__,__LINE__);
		pg_query($con, "begin");
		$log->debug("トランザクション開始(con) END",__FILE__,__LINE__);

		$log->debug("トランザクション開始(mdb2) START",__FILE__,__LINE__);
		$mdb2->beginTransaction();
		$log->debug("トランザクション開始(mdb2) END",__FILE__,__LINE__);

		// 申請ID
		$auto_approve_apply_id = $auto_approve_apply_id_list[$i]["apply_id"];

		//----------------------------------------------------------------------------------
		// 自動承認管理テーブルデータ取得（承認者データ）
		//----------------------------------------------------------------------------------
		$cl_ldr_auto_approve_manage_list = $cl_ldr_auto_approve_manage_list_model->getListByApplyId($auto_approve_apply_id);

		//----------------------------------------------------------------------------------
		// 自動承認管理テーブルデータ毎処理
		//----------------------------------------------------------------------------------
		for($j=0;$j<count($cl_ldr_auto_approve_manage_list);$j++){

			//------------------------------------------------------------------------------
			// 自動承認管理テーブルデータ取得
			//------------------------------------------------------------------------------
			$cl_ldr_auto_approve_manage=$cl_ldr_auto_approve_manage_list[$j];
			foreach( $cl_ldr_auto_approve_manage as $key => $value ){
				$log->debug('■■■$cl_ldr_auto_approve_manage['.$key.']:'.$value,__FILE__,__LINE__);
			}

			// 承認階層
			$apv_order = $cl_ldr_auto_approve_manage["apv_order"];
			//承認サブ階層
			$apv_sub_order = $cl_ldr_auto_approve_manage["apv_sub_order"];

			// 承認ステータス更新
			$param = array(
				"apv_stat"		=> "1",
				"apv_date"		=> date("YmdHi"),
				"apv_comment"	=> "",
				"apply_id"		=> $auto_approve_apply_id,
				"apv_order"		=> $apv_order,
				"apv_sub_order"	=> $apv_sub_order,

				"agent_user"	=> ""		// 代理人$emp_id
			);
			$cl_applyapv_model->update_apvstat_detail($param);

			//------------------------------------------------------------------------------
			// 自動承認管理テーブルデータ更新（処理済）
			//------------------------------------------------------------------------------
			//パラメータ作成
			$auto_approve_upd_param = getauto_approve_upd_param($cl_ldr_auto_approve_manage);
			//データ登録
			$cl_ldr_auto_approve_manage_model->update($auto_approve_upd_param);
		}

		// 申請ステータス更新
		$apply_param = array(
			"apply_id"		=> $auto_approve_apply_id,
			"apply_stat"	=> "1",
			"agent_user"	=> ""		// 代理人$emp_id
		);
		$cl_apply_model->update_applystat($apply_param);

		// 申請結果通知更新
		//$applynotice_param = array(
		//	"apply_id"		=> $apply_id,
		//	"send_emp_id"	=> $emp_id,
		//	"send_date"		=> date("YmdHi")
		//);
		//$cl_applynotice_model->update_send($applynotice_param);


		//------------------------------------------------------------------------------
		// テンプレート独自の承認処理を呼び出す
		//------------------------------------------------------------------------------
		//ハンドラーモジュールロード
		$log->debug('ハンドラーモジュールロード　開始　管理番号：'.$cmdparam_short_wkfw_name,__FILE__,__LINE__);
		$handler_script=dirname(__FILE__) . "/../handler/".$cmdparam_short_wkfw_name."_handler.php";
		$log->debug('$handler_script：'.$handler_script,__FILE__,__LINE__);
		require_once($handler_script);
		$log->debug('ハンドラーモジュールロード　終了',__FILE__,__LINE__);

		$log->debug('ハンドラークラスインスタンス化　開始',__FILE__,__LINE__);
		$handler_name = $cmdparam_short_wkfw_name."_handler";
		$handler = new $handler_name();
		$log->debug('ハンドラークラスインスタンス化　終了',__FILE__,__LINE__);

		//パラメータ作成
		$param["apply_id"] = $cl_ldr_auto_approve_manage["apply_id"];

		//------------------------------------------------------------------------------
		// ハンドラー承認詳細　登録
		//------------------------------------------------------------------------------
		$log->debug('■ハンドラー承認詳細　登録　開始',__FILE__,__LINE__);
		$handler->ApvDetail_Regist($mdb2, $cmdparam_login_emp_id, $param);
		$log->debug('■ハンドラー承認詳細　登録　終了',__FILE__,__LINE__);

		$log->debug('★９　●パラメータに申請ＩＤを追加後　$emp_id:'.$emp_id,__FILE__,__LINE__);

		//----------------------------------------------------------------------------------
		// トランザクションをコミット
		//----------------------------------------------------------------------------------
		$log->debug("トランザクション確定 START",__FILE__,__LINE__);
		$mdb2->commit();
		$log->debug("トランザクション確定 END",__FILE__,__LINE__);

	}

	//----------------------------------------------------------------------------------
	// データベース接続を閉じる
	//----------------------------------------------------------------------------------
	$log->debug("データベース切断 START",__FILE__,__LINE__);
	$mdb2->disconnect();
	$log->debug("データベース切断 END",__FILE__,__LINE__);


	$log->info(basename(__FILE__)." END");

	$log->shutdown();

	$log->info(basename(__FILE__)." END");

function logmsg($msg){
	echo $msg."\n";
}



function getauto_approve_upd_param($cl_ldr_auto_approve_manage){
	global $log;

	$db_param = array(
		"auto_approve_manage_id"		=> $cl_ldr_auto_approve_manage["auto_approve_manage_id"],
		"apply_id"						=> $cl_ldr_auto_approve_manage["apply_id"],
		"apv_order"						=> $cl_ldr_auto_approve_manage["apv_order"],
		"apv_sub_order"					=> $cl_ldr_auto_approve_manage["apv_sub_order"],
		"last_approve_flg"				=> "1",
		"proc_flg"						=> "9"
	);

	return $db_param;
}
