path=/usr/bin:$path
date_str=`date '+%Y%m%d_%H%M%S'`
#echo `date '+%Y%m%d_%H%M%S'` > /var/www/html/comedix/cl/auto/${date_str}.txt

################################################################################
# レベルアップ評価表（同僚評価）　自動申請処理
################################################################################
php -q /var/www/html/comedix/cl/auto/cl_doryo_hyoka_auto_apply.php short_wkfw_name=c350 wkfw_id=LUPDH00000001 login_emp_id=system level=1
php -q /var/www/html/comedix/cl/auto/cl_doryo_hyoka_auto_apply.php short_wkfw_name=c360 wkfw_id=LUPDH00000002 login_emp_id=system level=2
php -q /var/www/html/comedix/cl/auto/cl_doryo_hyoka_auto_apply.php short_wkfw_name=c370 wkfw_id=LUPDH00000003 login_emp_id=system level=3
php -q /var/www/html/comedix/cl/auto/cl_doryo_hyoka_auto_apply.php short_wkfw_name=c380 wkfw_id=LUPDH00000004 login_emp_id=system level=4
php -q /var/www/html/comedix/cl/auto/cl_doryo_hyoka_auto_apply.php short_wkfw_name=c390 wkfw_id=LUPDH00000005 login_emp_id=system level=5

################################################################################
# レベルアップ評価表（同僚評価）　自動承認処理
################################################################################
# 2012/07/31 レベルアップ評価表（同僚評価）　自動承認処理廃止
# php -q /var/www/html/comedix/cl/auto/cl_auto_approval.php short_wkfw_name=c350 login_emp_id=system level=1
# php -q /var/www/html/comedix/cl/auto/cl_auto_approval.php short_wkfw_name=c360 login_emp_id=system level=2
# php -q /var/www/html/comedix/cl/auto/cl_auto_approval.php short_wkfw_name=c370 login_emp_id=system level=3
# php -q /var/www/html/comedix/cl/auto/cl_auto_approval.php short_wkfw_name=c380 login_emp_id=system level=4
# php -q /var/www/html/comedix/cl/auto/cl_auto_approval.php short_wkfw_name=c390 login_emp_id=system level=5

################################################################################
# レベルアップ認定　自動承認処理
################################################################################
php -q /var/www/html/comedix/cl/auto/cl_auto_approval.php short_wkfw_name=c400 login_emp_id=system level=1
php -q /var/www/html/comedix/cl/auto/cl_auto_approval.php short_wkfw_name=c410 login_emp_id=system level=2
php -q /var/www/html/comedix/cl/auto/cl_auto_approval.php short_wkfw_name=c420 login_emp_id=system level=3
php -q /var/www/html/comedix/cl/auto/cl_auto_approval.php short_wkfw_name=c430 login_emp_id=system level=4
