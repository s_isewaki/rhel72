<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_apl_levelup_apply_inside_training_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
    var $insert_sql = "INSERT INTO cl_apl_levelup_apply_inside_training VALUES ( :levelup_apply_inside_training_id, :levelup_apply_id, :training_id, :must_division, :pass_flg, :delete_flg, current_timestamp, :create_user)";

    /* 登録用カラムタイプ */
	//2012/05/07 K.Fujii upd(s)
    //var $insert_col_type = array('text','text','text','integer','integer','boolean','text');
	var $insert_col_type = array('text','text','text','integer','integer','integer','text');
	//2012/05/07 K.Fujii upd(e)

    /* 更新用SQL */
    var $update_sql = "UPDATE cl_apl_levelup_apply_inside_training SET levelup_apply_id = :levelup_apply_id, training_id = :training_id, must_division = :must_division, pass_flg = :pass_flg, update_date = current_timestamp, update_user = :update_user WHERE levelup_apply_inside_training_id = :levelup_apply_inside_training_id";
    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_apl_levelup_apply_inside_training WHERE levelup_apply_inside_training_id = :levelup_apply_inside_training_id ";
    /* 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_apl_levelup_apply_inside_training SET delete_flg = :delete_flg , update_date=current_timestamp,update_user = :update_user WHERE levelup_apply_inside_training_id = :levelup_apply_inside_training_id";
    /* 一括論理削除用SQL */
    var $logical_all_delete_sql = "UPDATE cl_apl_levelup_apply_inside_training SET delete_flg = :delete_flg , update_date=current_timestamp,update_user = :update_user WHERE levelup_apply_id = :levelup_apply_id";

    /* 更新用カラムタイプ */
    var $update_col_type = array('text','text','integer','integer','text','text');
    /* 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');
    /* 論理削除用カラムタイプ */
	//2012/05/07 K.Fujii upd(s)
    //var $logical_delete_col_type = array('boolean','text','text');
	var $logical_delete_col_type = array('integer','text','text');
	//2012/05/07 K.Fujii upd(e)
    /* 一括論理削除用カラムタイプ */
	//2012/05/07 K.Fujii upd(s)
    //var $logical_all_delete_col_type = array('boolean','text','text');
	var $logical_all_delete_col_type = array('integer','text','text');
	//2012/05/07 K.Fujii upd(e)



    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_apl_levelup_apply_inside_training_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert(cl_apl_levelup_apply_inside_training) START");

		$this->log->debug('★$this->user:'.$this->user,__FILE__,__LINE__);
        /**
         * Parameter
         */
		//2012/05/07 K.Fujii upd(s)
        //$param["delete_flg"]=false;
		$param["delete_flg"]=0;
		//2012/05/07 K.Fujii upd(e)
        $param["create_user"]=$this->user;
        //$param["update_user"]=$this->user;

// :create_user,
// :update_date,
// :update_user,
// :agent_user
        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $apply_id
     */
    function physical_delete($apply_id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["levelup_apply_inside_training_id"]=$levelup_apply_inside_training_id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $apply_id
     */
    function logical_delete($levelup_apply_inside_training_id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
		//2012/05/07 K.Fujii upd(s)
        //$param["delete_flg"]=TRUE;
		$param["delete_flg"]=1;
		//2012/05/07 K.Fujii upd(e)
        $param["update_user"]=$this->user;
        $param["levelup_apply_inside_training_id"]=$levelup_apply_inside_training_id;


        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * logical_all_delete
     * @param type $apply_id
     */
    function logical_all_delete($levelup_apply_id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
		//2012/05/07 K.Fujii upd(s)
        //$param["delete_flg"]=TRUE;
        $param["delete_flg"]=1;
		//2012/05/07 K.Fujii upd(e)
        $param["update_user"]=$this->user;
        $param["levelup_apply_id"]=$levelup_apply_id;


        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_all_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }
}
