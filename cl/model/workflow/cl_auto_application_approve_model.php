<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_auto_application_approve_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 申請更新用SQL */
    var $apply_update_sql = "UPDATE cl_apply SET apply_stat = 1 ,update_date = CURRENT_TIMESTAMP ,update_user = :update_user ,agent_user = 'system' WHERE apply_id = :apply_id";
    /* 承認更新用SQL */
    var $applyapv_update_sql = "UPDATE cl_applyapv SET apv_stat = 1 ,update_date = CURRENT_TIMESTAMP ,update_user = :update_user ,agent_user = 'system' WHERE apply_id = :apply_id";
    /* 非同期・同期受信更新SQL */
    var $applyasyncrecv_update_sql = "UPDATE cl_applyasyncrecv SET apv_show_flg = TRUE ,update_date = CURRENT_TIMESTAMP ,update_user = :update_user WHERE apply_id = :apply_id";

    /* 申請更新用カラムタイプ */
    var $apply_update_col_type = array('text','text');
    /* 承認更新用カラムタイプ */
    var $applyapv_update_col_type = array('text','text');
    /* 非同期・同期受信更新カラムタイプ */
    var $applyasyncrecv_update_col_type = array('text','text');

    /**
     * Constractor
     */
    function cl_auto_application_approve_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * apply_update
     * @param type $apply_id
     */
    function apply_update($apply_id) {

        $this->log->info("apply_update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;
        $param["apply_id"]=$apply_id;

        /**
         * 実行
         */
        $rtn=parent::execute($this->apply_update_sql,$this->apply_update_col_type,$param);

        $this->log->info("apply_update() END");
        return $rtn;
    }

    /**
     * applyapv_update
     * @param type $apply_id
     */
    function applyapv_update($apply_id) {

        $this->log->info("applyapv_update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;
        $param["apply_id"]=$apply_id;

        /**
         * 実行
         */
        $rtn=parent::execute($this->applyapv_update_sql,$this->applyapv_update_col_type,$param);

        $this->log->info("applyapv_update() END");
        return $rtn;
    }

    /**
     * applyasyncrecv_update
     * @param type $apply_id
     */
    function applyasyncrecv_update($apply_id) {

        $this->log->info("applyasyncrecv_update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;
        $param["apply_id"]=$apply_id;

        /**
         * 実行
         */
        $rtn=parent::execute($this->applyasyncrecv_update_sql,$this->applyasyncrecv_update_col_type,$param);

        $this->log->info("applyasyncrecv_update() END");
        return $rtn;
    }
}
