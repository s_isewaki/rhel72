<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class empmst_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 一覧取得用SQL */
    var $select_list_sql = "SELECT emp_id, emp_personal_id, emp_class, emp_attribute, emp_dept, emp_job, emp_st, emp_lt_nm, emp_ft_nm, emp_kn_lt_nm, emp_kn_ft_nm, emp_zip1, emp_zip2, emp_prv, emp_addr1, emp_addr2, emp_ext, emp_tel1, emp_tel2, emp_tel3, emp_mobile1, emp_mobile2, emp_mobile3, emp_email, emp_m_email, emp_sex, emp_birth, emp_join, emp_retire, emp_pic_flg, emp_keywd, emp_email2, emp_phs, emp_room, emp_idm, emp_profile, updated_on FROM empmst";
    /* レコード取得用SQL */
    var $select_rec_sql = "SELECT emp_id, emp_personal_id, emp_class, emp_attribute, emp_dept, emp_job, emp_st, emp_lt_nm, emp_ft_nm, emp_kn_lt_nm, emp_kn_ft_nm, emp_zip1, emp_zip2, emp_prv, emp_addr1, emp_addr2, emp_ext, emp_tel1, emp_tel2, emp_tel3, emp_mobile1, emp_mobile2, emp_mobile3, emp_email, emp_m_email, emp_sex, emp_birth, emp_join, emp_retire, emp_pic_flg, emp_keywd, emp_email2, emp_phs, emp_room, emp_idm, emp_profile, updated_on FROM empmst WHERE emp_id = :emp_id";
    /* ログインユーザー取得用SQL */
    var $select_login_emp_sql = "SELECT emp_id, emp_personal_id, emp_class, emp_attribute, emp_dept, emp_job, emp_st, emp_lt_nm, emp_ft_nm, emp_kn_lt_nm, emp_kn_ft_nm, emp_zip1, emp_zip2, emp_prv, emp_addr1, emp_addr2, emp_ext, emp_tel1, emp_tel2, emp_tel3, emp_mobile1, emp_mobile2, emp_mobile3, emp_email, emp_m_email, emp_sex, emp_birth, emp_join, emp_retire, emp_pic_flg, emp_keywd, emp_email2, emp_phs, emp_room, emp_idm, emp_profile, updated_on FROM empmst WHERE emp_id in (SELECT emp_id FROM session WHERE session_id = :session)";

    /**
     * コンストラクタ。
     * セッションからログインユーザー情報を取得する場合など、
     * ログインユーザーの情報が無い場合はログインユーザーの指定を省略可能です。
     *
     * @param $p_con MDB2のコネクション
     * @param $p_login_user ログインユーザー(省略可能)
     */
    function empmst_model($p_con, $p_login_user = "") {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * ログインユーザーを設定します。
     *
     * @param $p_login_user ログインユーザーのemp_id
     */
    function set_longin_user($p_login_user)
    {
        $this->user = $p_login_user;
    }



    /**
     * list
     * @param type $emp_id
     */
    function getList(){

        $this->log->debug("getList() START");
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("getList() END");
        return $data;

     }

    /**
     * select
     * @param type $emp_id
     */
    function select($id){

        $this->log->debug("select() START");
        $data=parent::select($this->select_rec_sql,array("text"),array("emp_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }

    /**
     * select_login_emp
     * @param type $session
     */
    function select_login_emp($session){

        $this->log->debug("select_login_emp() START");
        $data=parent::select($this->select_login_emp_sql,array("text"),array("session"=>$session),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select_login_emp() END");
        return $data[0];

    }

}
