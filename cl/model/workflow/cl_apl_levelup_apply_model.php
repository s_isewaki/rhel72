<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_apl_levelup_apply_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
    var $insert_sql = "INSERT INTO cl_apl_levelup_apply VALUES ( :levelup_apply_id, :apply_id, :levelup_emp_id, :level , :apply_date , :attached_target_flg, :attached_episode_flg, :chief_emp_id, :colleague_emp_id, :delete_flg, current_timestamp, :create_user)";
    /* 登録用カラムタイプ */
	//2012/05/07 K.Fujii upd(s)
    //var $insert_col_type = array('text','text','text','integer','date','integer','integer','text','text','boolean','text');
    var $insert_col_type = array('text','text','text','integer','date','integer','integer','text','text','integer','text');
	//2012/05/07 K.Fujii upd(e)

    /* 更新用SQL */
    var $update_sql = "UPDATE cl_apl_levelup_apply SET apply_id = :apply_id, levelup_emp_id = :levelup_emp_id, level = :level, apply_date = :apply_date, attached_target_flg = :attached_target_flg, attached_episode_flg = :attached_episode_flg, chief_emp_id = :chief_emp_id, colleague_emp_id = :colleague_emp_id, update_user = :update_user, update_date = current_timestamp WHERE levelup_apply_id = :levelup_apply_id";
    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_apl_levelup_apply WHERE levelup_apply_id = :levelup_apply_id ";
    /* 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_apl_levelup_apply SET delete_flg = :delete_flg , update_date = current_timestamp,update_user = :update_user WHERE levelup_apply_id = :levelup_apply_id";
    /* ステータス更新用SQL */
    var $update_emp_id_sql = "UPDATE cl_apl_levelup_apply SET chief_emp_id = :chief_emp_id, colleague_emp_id = :colleague_emp_id ,update_date = current_timestamp , update_user = :update_user WHERE levelup_apply_id = :levelup_apply_id";

    /* 更新用カラムタイプ */
    var $update_col_type = array('text','text','integer','date','integer','integer','integer','integer','text','text');
    /* 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');
    /* 論理削除用カラムタイプ */
	//2012/05/07 K.Fujii upd(s)
    //var $logical_delete_col_type = array('boolean','text','text');
	var $logical_delete_col_type = array('integer','text','text');
	//2012/05/07 K.Fujii upd(e)
    /* ステータス更新用カラムタイプ */
    var $update_emp_id_col_type = array('text','text','text','text');





    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_apl_levelup_apply_model(&$p_con,$p_login_user) {
        parent::cl_common_model(&$p_con,$p_user);
        $this->con = &$p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert(cl_apl_levelup_apply) START");

		$this->log->debug('★$this->user:'.$this->user,__FILE__,__LINE__);
        /**
         * Parameter
         */
		//2012/05/07 K.Fujii upd(s)
        //$param["delete_flg"]=FALSE;
        $param["delete_flg"]=0;
		//2012/05/07 K.Fujii upd(e)
        $param["create_user"]=$this->user;
        //$param["update_user"]=$this->user;

// :create_user,
// :update_date,
// :update_user,

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function apply_update($apply_title,$apply_id) {
    	//var $aplyupdt_sql = "update cl_apply set apply_content = :apply_content , apply_title = :apply_title where apply_id = :apply_id";
    	//var $aplyupdt_sql = "update cl_apply set apply_title = :apply_title , update_date = current_timestamp , update_user = :update_user where apply_id = :apply_id";

        $this->log->info("apply_update() START");

        /**
         * Parameter
         */
        $param = array();
        $param["apply_title"]=$apply_title;
		$param["update_user"]=$this->user;
        $param["apply_id"]=$apply_id;

        /**
         * 実行
         */
        $rtn=parent::execute($this->aplyupdt_sql,array("text","text","text"),$param);

        $this->log->info("apply_update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $apply_id
     */
    function physical_delete($cl_apl_levelup_apply) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["cl_apl_levelup_apply"]=$cl_apl_levelup_apply;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $apply_id
     */
    function logical_delete($cl_apl_levelup_apply) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
		//2012/05/07 K.Fujii upd(s)
        //$param["delete_flg"]=true;
		$param["delete_flg"]=1;
		//2012/05/07 K.Fujii upd(e)
        $param["update_user"]=$this->user;
        $param["levelup_apply_id"]=$cl_apl_levelup_apply;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

     /**
      * update_applystat
      * @param type $param
      */
    function update_emp_id($p_param){

        $this->log->info("update_emp_id() START");

        /**
         * Parameter
         */
        $param = array();
        $param["chief_emp_id"]=$p_param["chief_emp_id"];
        $param["colleague_emp_id"]=$p_param["colleague_emp_id"];
        $param["levelup_apply_id"]=$p_param["levelup_apply_id"];
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_emp_id_sql,$this->update_emp_id_col_type,$param);

        $this->log->info("update_emp_id() END");

        return $rtn;

    }

    //2012/05/25 K.Fujii ins(s)
    function getRecordByApplyId($apply_id){
	    $sql = "";
	    $sql .= " SELECT ";
		$sql .= " 	levelup_apply_id ";
		$sql .= " 	, apply_id ";
		$sql .= " 	, levelup_emp_id ";
		$sql .= " 	, level ";
		$sql .= " 	, apply_date ";
		$sql .= " 	, attached_target_flg ";
		$sql .= " 	, attached_episode_flg ";
		$sql .= " 	, chief_emp_id ";
		$sql .= " 	, colleague_emp_id ";
		$sql .= " 	, delete_flg ";
		$sql .= " 	, create_date ";
		$sql .= " 	, create_user ";
		$sql .= " 	, update_date ";
		$sql .= " 	, update_user ";
		$sql .= " FROM cl_apl_levelup_apply ";
		$sql .= " WHERE apply_id = :apply_id ";

		/* カラムタイプ */
        $type = array("text");

        $data=parent::select($sql,$type,array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data[0];
    }
    //2012/05/25 K.Fujii ins(e)

}
