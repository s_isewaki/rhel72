<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_apply_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
  //var $insert_sql = "INSERT INTO cl_apply ( apply_id,wkfw_id,apply_content,emp_id,apply_stat,apply_date,delete_flg,apply_title,re_apply_id,apv_fix_show_flg,apv_bak_show_flg,emp_class,emp_attribute,emp_dept,apv_ng_show_flg,emp_room,draft_flg,wkfw_appr,wkfw_content_type,apply_title_disp_flg,apply_no,notice_sel_flg,wkfw_history_no,wkfwfile_history_no,create_date,create_user ) VALUES ( :apply_id , :wkfw_id , :apply_content , :emp_id , :apply_stat , :apply_date , :delete_flg , :apply_title , :re_apply_id , :apv_fix_show_flg , :apv_bak_show_flg , :emp_class , :emp_attribute , :emp_dept , :apv_ng_show_flg , :emp_room , :draft_flg , :wkfw_appr , :wkfw_content_type , :apply_title_disp_flg , :apply_no , :notice_sel_flg , :wkfw_history_no , :wkfwfile_history_no , current_timestamp , :create_user )";
    var $insert_sql = "INSERT INTO cl_apply VALUES ( :apply_id, :wkfw_id, :apply_content, :emp_id, :apply_stat, :apply_date, :delete_flg, :apply_title, :re_apply_id, :apv_fix_show_flg, :apv_bak_show_flg, :emp_class, :emp_attribute, :emp_dept, :apv_ng_show_flg, :emp_room, :draft_flg, :wkfw_appr, :wkfw_content_type, :apply_title_disp_flg, :apply_no, :notice_sel_flg, :wkfw_history_no, :wkfwfile_history_no, current_timestamp, :create_user, :update_date, :update_user, :agent_user )";
    //apply_id,wkfw_id,apply_content,emp_id,apply_stat,apply_date,delete_flg,apply_title,re_apply_id,apv_fix_show_flg,apv_bak_show_flg,emp_class,emp_attribute,emp_dept,apv_ng_show_flg,emp_room,draft_flg,wkfw_appr,wkfw_content_type,apply_title_disp_flg,apply_no,notice_sel_flg,wkfw_history_no,wkfwfile_history_no,create_date,create_user,update_date,update_user,agent_user

    /* 登録用カラムタイプ */
  //var $insert_col_type = array('text','text','text','text','text','text','integer','text','text','boolean','boolean','integer','integer','integer','boolean','integer','boolean','text','text','boolean','integer','boolean','integer','integer','text');
    var $insert_col_type = array('text','text','text','text','text','text','boolean','text','text','boolean','boolean','integer','integer','integer','boolean','integer','boolean','text','text','boolean','integer','boolean','integer','integer','text','timestamp','text','text');

    /* 更新用SQL */
    var $update_sql = "UPDATE cl_apply SET wkfw_id = :wkfw_id , apply_content = :apply_content , emp_id = :emp_id , apply_stat = :apply_stat , apply_date = :apply_date , delete_flg = :delete_flg , apply_title = :apply_title , re_apply_id = :re_apply_id , apv_fix_show_flg = :apv_fix_show_flg , apv_bak_show_flg = :apv_bak_show_flg , emp_class = :emp_class , emp_attribute = :emp_attribute , emp_dept = :emp_dept , apv_ng_show_flg = :apv_ng_show_flg , emp_room = :emp_room , draft_flg = :draft_flg , wkfw_appr = :wkfw_appr , wkfw_content_type = :wkfw_content_type , apply_title_disp_flg = :apply_title_disp_flg , apply_no = :apply_no , notice_sel_flg = :notice_sel_flg , wkfw_history_no = :wkfw_history_no , wkfwfile_history_no = :wkfwfile_history_no , update_date = current_timestamp , update_user = :update_user WHERE apply_id = :apply_id";
    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_apply WHERE apply_id = :apply_id ";
    /* 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_apply SET delete_flg = :delete_flg , update_date=current_timestamp,update_user = :update_user WHERE apply_id = :apply_id";
    /* 一覧取得用SQL */
    var $select_list_sql = "SELECT apply_id,wkfw_id,apply_content,emp_id,apply_stat,apply_date,delete_flg,apply_title,re_apply_id,apv_fix_show_flg,apv_bak_show_flg,emp_class,emp_attribute,emp_dept,apv_ng_show_flg,emp_room,draft_flg,wkfw_appr,wkfw_content_type,apply_title_disp_flg,apply_no,notice_sel_flg,wkfw_history_no,wkfwfile_history_no,create_date,create_user,update_date,update_user,agent_user FROM cl_apply ORDER BY create_date";
    /* レコード取得用SQL */
    var $select_rec_sql = "SELECT apply_id,wkfw_id,apply_content,emp_id,apply_stat,apply_date,delete_flg,apply_title,re_apply_id,apv_fix_show_flg,apv_bak_show_flg,emp_class,emp_attribute,emp_dept,apv_ng_show_flg,emp_room,draft_flg,wkfw_appr,wkfw_content_type,apply_title_disp_flg,apply_no,notice_sel_flg,wkfw_history_no,wkfwfile_history_no,create_date,create_user,update_date,update_user,agent_user FROM cl_apply WHERE apply_id = :apply_id";
    /* ステータス更新用SQL */
    var $update_applystat_sql = "UPDATE cl_apply SET apply_stat = :apply_stat ,update_date = current_timestamp , update_user = :update_user WHERE apply_id = :apply_id";

    /* 更新用カラムタイプ */
    var $update_col_type = array('text','text','text','text','text','boolean','text','integer','boolean','boolean','integer','integer','integer','boolean','integer','boolean','text','text','boolean','integer','booleantext','integer','integer','text','text');
    /* 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');
    /* 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('integer','text','text');
    /* ステータス更新用カラムタイプ */
    var $update_applystat_col_type = array('text','text','text');

	// 申請更新用SQL(cl_application_workflow_common_class.php)
    var $aplyupdt_sql = "update cl_apply set apply_title = :apply_title , update_date = current_timestamp , update_user = :update_user where apply_id = :apply_id";

    // 2012/06/04 Yamagawa add(s)
    // 申請番号取得用SQL
    var $apply_cnt_per_year_sql = "SELECT count(apply_id) as cnt FROM cl_apply WHERE substring(apply_date from 1 for 8) BETWEEN :ymd_from AND :ymd_to AND NOT draft_flg";
    /* 申請番号取得用カラムタイプ */
    var $apply_cnt_per_year_col_type = array('text','text');
    // 2012/06/04 Yamagawa add(e)

	//---------------------------------------------
	// 代理承認用
	//---------------------------------------------
    /* ステータス更新用SQL */
    var $agent_update_applystat_sql = "UPDATE cl_apply SET apply_stat = :apply_stat ,update_date = current_timestamp , update_user = :update_user , agent_user=:agent_user WHERE apply_id = :apply_id";
    /* ステータス更新用カラムタイプ */
    var $agent_update_applystat_col_type = array('text','text','text','text');

    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_apply_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert() START");

		$this->log->debug('★$this->user:'.$this->user,__FILE__,__LINE__);
        /**
         * Parameter
         */
        $param["delete_flg"]="f";
        $param["create_user"]=$this->user;
        $param["update_user"]=null;
        $param["update_date"]=null;
        $param["agent_user"]=null;

// :create_user,
// :update_date,
// :update_user,
// :agent_user
        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function apply_update($apply_title,$apply_id) {
    	//var $aplyupdt_sql = "update cl_apply set apply_content = :apply_content , apply_title = :apply_title where apply_id = :apply_id";
    	//var $aplyupdt_sql = "update cl_apply set apply_title = :apply_title , update_date = current_timestamp , update_user = :update_user where apply_id = :apply_id";

        $this->log->info("apply_update() START");

        /**
         * Parameter
         */
        $param = array();
        $param["apply_title"]=$apply_title;
		$param["update_user"]=$this->user;
        $param["apply_id"]=$apply_id;

        /**
         * 実行
         */
        $rtn=parent::execute($this->aplyupdt_sql,array("text","text","text"),$param);

        $this->log->info("apply_update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $apply_id
     */
    function physical_delete($apply_id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["apply_id"]=$apply_id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $apply_id
     */
    function logical_delete($apply_id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flg"]="t";
        $param["apply_id"]=$apply_id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * getList
     */
    function getList(){

        $this->log->info("select() START");
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

     }

    /**
     * select
     * @param type $apply_id
     */
    function select($apply_id){

        $this->log->info("select() START");
        $data=parent::select($this->select_rec_sql,array("text"),array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }

     /**
      * update_applystat
      * @param type $param
      */
    function update_applystat($param){

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
		if($param["agent_user"] != "" ){
			$rtn=parent::execute($this->agent_update_applystat_sql,$this->agent_update_applystat_col_type,$param);
			$this->log->debug("update_applystat(代理承認)");

		}else{
			$param["agent_user"] = "";
			$rtn=parent::execute($this->agent_update_applystat_sql,$this->agent_update_applystat_col_type,$param);
			$this->log->debug("update_applystat(通常承認)");
		}

        //$rtn=parent::execute($this->update_applystat_sql,$this->update_applystat_col_type,$param);

        $this->log->info("update() END");

        return $rtn;

    }

    /**
     * 申請番号取得
     * @param
     * @return 申請番号
     */
//	function get_apply_cnt_per_year($year)
//	{
//
//		$this->log->info(__FUNCTION__." START");
//
//		$this_ymd = $year."0401";
//		$next_year = $year + 1;
//		$next_ymd = $next_year."0331";
//
//		$param = array();
//		$param["ymd_from"]	= $this_ymd;
//		$param["ymd_to"]	= $next_ymd;
//
//		$data=parent::select($this->apply_cnt_per_year_sql,$this->apply_cnt_per_year_col_type,$param,MDB2_FETCHMODE_ASSOC);
//
//		$this->log->info(__FUNCTION__." END");
//
//		return $data[0];
//	}


	/**
	 * 申請番号取得
	 * @return
	 */
	function get_apply_no()
	{

		$this->log->info(__FUNCTION__." START");

		//現在の年度
		$date = date("Ymd");
		$year = substr($date, 0, 4);
		$md   = substr($date, 4, 4);
		if($md >= "0101" and $md <= "0331")
		{
			$year = $year - 1;
		}

		//採番に使用するシーケンス名
		$seq_name = 'cl_apply_no_' . $year . '_seq';
        $this->log->debug("seq_name:".$seq_name);

		//SQL
		$sql = "select nextval(:seq_name) as apply_no";

		//パラメータの型
		$col_type = array('text');

		//パラメータ
		$param = array();
		$param["seq_name"]	= $seq_name;

		//実行
		$data = parent::select($sql,$col_type,$param,MDB2_FETCHMODE_ASSOC);
		$apply_no = $data[0]['apply_no'];

        $this->log->debug("apply_no:".$apply_no);
		$this->log->info(__FUNCTION__." END");

		return $apply_no;

	}


}
