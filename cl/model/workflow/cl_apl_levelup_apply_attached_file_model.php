<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_apl_levelup_apply_attached_file_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
    var $insert_sql = "INSERT INTO cl_apl_levelup_apply_attached_file VALUES ( :levelup_apply_attached_file_id, :levelup_apply_id, :disp_order, :file_division, :file_name, :delete_flg, current_timestamp, :create_user)";

    /* 登録用カラムタイプ */
	//2012/05/07 K.Fujii upd(s)
    //var $insert_col_type = array('text','text','integer','integer','text','boolean','text');
	var $insert_col_type = array('text','text','integer','integer','text','integer','text');
	//2012/05/07 K.Fujii upd(s)

    /* 更新用SQL */
    var $update_sql = "UPDATE cl_apl_levelup_apply_attached_file SET levelup_apply_attached_file_id = :levelup_apply_attached_file_id, levelup_apply_id = :levelup_apply_id, disp_order = :disp_order, file_division = :file_division, file_name = :file_name,  update_date = current_timestamp, update_user = :update_user WHERE levelup_apply_attached_file_id=:levelup_apply_attached_file_id";
    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_apl_levelup_apply_attached_file WHERE levelup_apply_attached_file_id = :levelup_apply_attached_file_id";
    /* 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_apl_levelup_apply_attached_file SET delete_flg = :delete_flg , update_date=current_timestamp,update_user = :update_user WHERE levelup_apply_attached_file_id = :levelup_apply_attached_file_id";
    /* 一括論理削除用SQL */
    var $logical_all_delete_sql = "UPDATE cl_apl_levelup_apply_attached_file SET delete_flg = :delete_flg , update_date=current_timestamp,update_user = :update_user WHERE levelup_apply_id = :levelup_apply_id";

    /* 更新用カラムタイプ */
    var $update_col_type = array('text','text','integer','integer','text','integer','text');
    /* 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');
    /* 論理削除用カラムタイプ */
	//2012/05/07 K.Fujii upd(s)
    //var $logical_delete_col_type = array('boolean','text','text');
	var $logical_delete_col_type = array('integer','text','text');
	//2012/05/07 K.Fujii upd(s)
    /* 一括論理削除用カラムタイプ */
	//2012/05/07 K.Fujii upd(s)
    //var $logical_all_delete_col_type = array('boolean','text','text');
	var $logical_all_delete_col_type = array('integer','text','text');
	//2012/05/07 K.Fujii upd(s)


    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_apl_levelup_apply_attached_file_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert(cl_apl_levelup_apply_attached_file) START");

		$this->log->debug('$this->user:'.$this->user,__FILE__,__LINE__);
        /**
         * Parameter
         */
		//2012/05/07 K.Fujii upd(s)
        //$param["delete_flg"]=false;
		$param["delete_flg"]=0;
		//2012/05/07 K.Fujii upd(e)
        $param["create_user"]=$this->user;
       // $param["update_user"]=$this->user;

// :create_user,
// :update_date,
// :update_user,
// :agent_user
        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $apply_id
     */
    function physical_delete($apply_id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["levelup_apply_attached_file_id"]=$levelup_apply_attached_file_id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $apply_id
     */
    function logical_delete($levelup_apply_attached_file_id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
		//2012/05/07 K.Fujii upd(s)
        //$param["delete_flg"]=true;
		$param["delete_flg"]=1;
		//2012/05/07 K.Fujii upd(e)
        $param["update_user"]=$this->user;
        $param["levelup_apply_attached_file_id"]=$levelup_apply_attached_file_id;


        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * logical_all_delete
     * @param type $apply_id
     */
    function logical_all_delete($levelup_apply_id) {

        $this->log->info("logical_all_delete() START");

        /**
         * Parameter
         */
        $param=array();
		//2012/05/07 K.Fujii upd(s)
        //$param["delete_flg"]=true;
		$param["delete_flg"]=1;
		//2012/05/07 K.Fujii upd(e)
        $param["update_user"]=$this->user;
        $param["levelup_apply_id"]=$levelup_apply_id;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_all_delete_sql,$this->logical_all_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete_from_update
     * @param type $apply_id
     */
    function logical_delete_from_update($levelup_apply_id, $arr_levelup_apply_attached_file_id) {

        $this->log->info("logical_delete_from_update() START");
        $param=array();
		//2012/05/07 K.Fujii upd(s)
        //$type=array('boolean','text','text');
		$type=array('integer','text','text');
		//2012/05/07 K.Fujii upd(e)

        $sql  = "";
        $sql .= "UPDATE ";
        $sql .= " cl_apl_levelup_apply_attached_file ";
        $sql .= "SET ";
        $sql .= " delete_flg = :delete_flg ";
        $sql .= " ,update_date=current_timestamp ";
        $sql .= " ,update_user = :update_user ";
        $sql .= "WHERE ";
        $sql .= "  levelup_apply_id = :levelup_apply_id ";

        if (count($arr_levelup_apply_attached_file_id) > 1) {
            $sql .= " AND levelup_apply_attached_file_id NOT IN (";
            for($i = 0; $i<count($arr_levelup_apply_attached_file_id); $i++) {
                  if ($i > 0) {
                      $sql .= ",";
                  }
                  $sql .= ":levelup_apply_attached_file_id".$i;
                  $param["levelup_apply_attached_file_id".$i] = $arr_levelup_apply_attached_file_id[$i];
                  array_push($type,'text');
            }
            $sql .= ")";
        } else if (count($arr_levelup_apply_attached_file_id) == 1) {
            $sql .= " AND levelup_apply_attached_file_id != :levelup_apply_attached_file_id";
            $param["levelup_apply_attached_file_id"] = $arr_levelup_apply_attached_file_id[0];
            array_push($type,'text');
        }

        /**
         * Parameter
         */
		//2012/05/07 K.Fujii upd(s)
        //$param["delete_flg"]=true;
		$param["delete_flg"]=1;
		//2012/05/07 K.Fujii upd(e)
        $param["update_user"]=$this->user;
        $param["levelup_apply_id"]=$levelup_apply_id;

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info("logical_delete_from_update() END");

        return $rtn;
    }

}
