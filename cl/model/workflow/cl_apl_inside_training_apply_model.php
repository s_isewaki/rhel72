<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

/**
 * 院内研修申請　ＤＢモデル
 *
 */
class cl_apl_inside_training_apply_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /** 登録用SQL */
    var $insert_sql = "INSERT INTO cl_apl_inside_training_apply ( training_apply_id, emp_id, apply_id, charange_level, inside_training_id, plan_id, inside_training_date, change_apply_date, before_apply_id, inside_training_status, change_reason, learn_memo, recommand_flag, delete_flag, create_date,create_user ) VALUES ( :training_apply_id, :emp_id, :apply_id, :charange_level, :inside_training_id, :plan_id, :inside_training_date, :change_apply_date, :before_apply_id, :inside_training_status, :change_reason, :learn_memo, :recommand_flag, :delete_flag, current_timestamp, :create_user )";
    /** 更新用SQL */
    var $update_sql = "UPDATE cl_apl_inside_training_apply SET emp_id=:emp_id, charange_level = :charange_level , inside_training_id=:inside_training_id, plan_id=:plan_id, inside_training_date = :inside_training_date , change_apply_date=:change_apply_date, before_apply_id=:before_apply_id, inside_training_status = :inside_training_status , change_reason=:change_reason, learn_memo = :learn_memo, recommand_flag=:recommand_flag, update_date=current_timestamp , update_user=:update_user WHERE training_apply_id=:training_apply_id";
    /** 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_apl_inside_training_apply WHERE training_apply_id = :training_apply_id ";
    /** 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_apl_inside_training_apply SET delete_flag = :delete_flag , update_date=current_timestamp,update_user = :update_user WHERE training_apply_id = :training_apply_id";
    /** 一覧取得用SQL */
    var $select_list_sql = "SELECT emp_id, apply_id, charange_level, inside_training_id, plan_id, inside_training_date, change_apply_date, before_apply_id, inside_training_status, change_reason, learn_memo, recommand_flag, delete_flag, create_date, create_user, update_date, update_user FROM cl_apl_inside_training_apply ORDER BY create_date";
    /** レコード取得用SQL */
    var $select_rec_sql = "SELECT emp_id, apply_id, charange_level, inside_training_id, plan_id, inside_training_date, change_apply_date, before_apply_id, inside_training_status, change_reason, learn_memo, recommand_flag, delete_flag, create_date, create_user, update_date, update_user FROM cl_apl_inside_training_apply WHERE training_apply_id = :training_apply_id";
    /** 研修講座管理(受付一覧)取得用SQL */
    var $select_reception_list_sql = "SELECT apl.emp_id , emp.emp_lt_nm || '　' || emp.emp_ft_nm as emp_name , apl.inside_training_date , case emp.emp_room when null then class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm else class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm || ' > ' || room.room_nm end as affiliation, roll.emp_roll FROM cl_apl_inside_training_apply apl inner join empmst emp on apl.emp_id = emp.emp_id left join classmst class on emp.emp_class = class.class_id left join atrbmst atrb on emp.emp_attribute = atrb.atrb_id left join deptmst dept on emp.emp_dept = dept.dept_id left join classroom room on emp.emp_room = room.room_id left join cl_inside_training_roll roll on apl.plan_id = roll.plan_id and apl.inside_training_id = roll.training_id and apl.emp_id = roll.emp_id WHERE apl.inside_training_id = :inside_training_id AND apl.plan_id = :plan_id";

    /** 登録用カラムタイプ */
    var $insert_col_type = array('text','text','text','integer','text','text','date','date','text','integer','text','text','integer','integer','text');
    /** 更新用カラムタイプ */
    var $update_col_type = array('text','integer','text','text','date','date','text','integer','text','text','integer','text','text');
    /** 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');
    /** 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('integer','text','text');
    /** 研修講座管理(受付一覧)取得用カラムタイプ */
    var $select_reception_list_col_type = array('text','text');

    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_inside_training_apply_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param["delete_flag"]=0;
        $param["create_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * 代理用 update
     * @param type $param
     */
    function sub_update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql2,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $training_apply_id
     */
    function physical_delete($id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["training_apply_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        //$rtn=$this->execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $training_apply_id
     */
    function logical_delete($id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flag"]=1;
        $param["training_apply_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        //$rtn=$this->execute($logical_delete_sql,$logical_delete_col_type,$param);
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * list
     * @param type $training_apply_id
     */
    function getList(){

        $this->log->info("select() START");
        //$select_sql = "SELECT apply_id,charange_level,inside_training_id,inside_training_date,learn_memo,delete_flag,create_date,create_user,update_date,update_user FROM cl_inside_training_apply ORDER BY create_date";
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

     }

    /**
     * select
     * @param type $training_apply_id
     */
    function select($id){

        $this->log->info("select() START");
        $data=parent::select($this->select_rec_sql,array("text"),array("training_apply_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }

    /**
     * getReceptionList
     * @param type $param
     */
    function getReceptionList($param){

        $this->log->debug("select() START");
        $data=parent::select($this->select_reception_list_sql,$this->select_reception_list_col_type,$param,MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

    }

}
