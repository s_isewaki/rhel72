<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_wkfwmst_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
    var $insert_sql = "INSERT INTO cl_wkfwmst ( wkfw_id, wkfw_type, wkfw_title, wkfw_content, wkfw_del_flg, wkfw_start_date, wkfw_end_date, wkfw_appr, wkfw_content_type, wkfw_folder_id, ref_dept_st_flg, ref_dept_flg, ref_st_flg, short_wkfw_name, apply_title_disp_flg, create_date, create_user ) VALUES ( :wkfw_id, :wkfw_type, :wkfw_title, :wkfw_content, :wkfw_del_flg, :wkfw_start_date, :wkfw_end_date, :wkfw_appr, :wkfw_content_type, :wkfw_folder_id, :ref_dept_st_flg, :ref_dept_flg, :ref_st_flg, :short_wkfw_name, :apply_title_disp_flg, current_timestamp, :create_user )";
    /* 更新用SQL */
    var $update_sql = "UPDATE cl_wkfwmst SET wkfw_type = :wkfw_type, wkfw_title = :wkfw_title, wkfw_content = :wkfw_content, wkfw_del_flg = :wkfw_del_flg, wkfw_start_date = :wkfw_start_date, wkfw_end_date = :wkfw_end_date, wkfw_appr = :wkfw_appr, wkfw_content_type = :wkfw_content_type, wkfw_folder_id = :wkfw_folder_id, ref_dept_st_flg = :ref_dept_st_flg, ref_dept_flg = :ref_dept_flg, ref_st_flg = :ref_st_flg, short_wkfw_name = :short_wkfw_name, apply_title_disp_flg = :apply_title_disp_flg, update_date = current_timestamp, update_user = :update_user WHERE wkfw_id = :wkfw_id";
    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_wkfwmst WHERE wkfw_id = :wkfw_id";
    /* 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_wkfwmst SET wkfw_del_flg = :wkfw_del_flg, update_date = current_timestamp, update_user = :update_user WHERE wkfw_id = :wkfw_id";
    /* 一覧取得用SQL */
    var $select_list_sql = "SELECT wkfw_id, wkfw_type, wkfw_title, wkfw_content, wkfw_del_flg, wkfw_start_date, wkfw_end_date, wkfw_appr, wkfw_content_type, wkfw_folder_id, ref_dept_st_flg, ref_dept_flg, ref_st_flg, short_wkfw_name, apply_title_disp_flg, create_date, create_user, update_date, update_user FROM cl_wkfwmst ORDER BY create_date";
    /* レコード取得用SQL */
    var $select_rec_sql = "SELECT wkfw_id, wkfw_type, wkfw_title, wkfw_content, wkfw_del_flg, wkfw_start_date, wkfw_end_date, wkfw_appr, wkfw_content_type, wkfw_folder_id, ref_dept_st_flg, ref_dept_flg, ref_st_flg, short_wkfw_name, apply_title_disp_flg, create_date, create_user, update_date, update_user FROM cl_wkfwmst WHERE wkfw_id = :wkfw_id";
    /* レコード取得用SQL:管理コードから */
    var $select_short_wkfw_name_sql = "SELECT wkfw_id, wkfw_type, wkfw_title, wkfw_content, wkfw_del_flg, wkfw_start_date, wkfw_end_date, wkfw_appr, wkfw_content_type, wkfw_folder_id, ref_dept_st_flg, ref_dept_flg, ref_st_flg, short_wkfw_name, apply_title_disp_flg, create_date, create_user, update_date, update_user FROM cl_wkfwmst WHERE short_wkfw_name = :short_wkfw_name";

    /* 申請ワークフロー取得用SQL */
    var $select_apply_sql = "SELECT app.apply_id, app.wkfw_id, app.apply_content, app.emp_id, app.apply_stat, app.apply_date, app.delete_flg, app.apply_title, app.re_apply_id, app.apv_fix_show_flg, app.apv_bak_show_flg, app.emp_class, app.emp_attribute, app.emp_dept, app.apv_ng_show_flg, app.emp_room, app.draft_flg, app.wkfw_appr, app.wkfw_content_type, app.apply_title_disp_flg, app.apply_no, app.notice_sel_flg, app.wkfw_history_no, app.wkfwfile_history_no, emp.emp_lt_nm, emp.emp_ft_nm, wkfw.wkfw_title, wkfw.wkfw_folder_id, cate.wkfw_nm, wkfw.wkfw_content, wkfw.short_wkfw_name, class.class_nm as apply_class_nm, atrb.atrb_nm as apply_atrb_nm, dept.dept_nm as apply_dept_nm, room.room_nm as apply_room_nm FROM cl_apply app inner join cl_wkfwmst wkfw on app.wkfw_id = wkfw.wkfw_id inner join cl_wkfwcatemst cate on wkfw.wkfw_type = cate.wkfw_type inner join empmst emp on app.emp_id = emp.emp_id left join classmst class on app.emp_class = class.class_id left join atrbmst atrb on app.emp_attribute = atrb.atrb_id left join deptmst dept on app.emp_dept = dept.dept_id left join classroom room on app.emp_room = room.room_id WHERE app.apply_id = :apply_id";

    /* 登録用カラムタイプ */
    var $insert_col_type = array('text','integer','text','text','boolean','text','text','text','text','integer','boolean','text','text','text','boolean','text');
    /* 更新用カラムタイプ */
    var $update_col_type = array('integer','text','text','boolean','text','text','text','text','integer','boolean','text','text','text','boolean','text','text');

    /* 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');
    /* 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('boolean','text','text');

    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_wkfwmst_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param["delete_flag"]=0;
        $param["create_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $wkfw_id
     */
    function physical_delete($id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["wkfw_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $wkfw_id
     */
    function logical_delete($id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flag"]=1;
        $param["wkfw_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * list
     * @param type $wkfw_id
     */
    function getList(){

        $this->log->info("select() START");
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

     }

    /**
     * select
     * @param type $wkfw_id
     */
    function select($id){

        $this->log->info("select() START");
        $data=parent::select($this->select_rec_sql,array("text"),array("wkfw_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }

    /**
     * select
     * @param type $short_wkfw_name
     */
    function select_from_short_wkfw_name($short_wkfw_name){

        $this->log->info("select() START");
        $data=parent::select($this->select_short_wkfw_name_sql,array("text"),array("short_wkfw_name"=>$short_wkfw_name),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }


     /**
      * get_apply_wkfwmst
      * @param type $apply_id
      */
    function get_apply_wkfwmst($id){

        $this->log->debug("select() START");
        $data=parent::select($this->select_apply_sql,array("text"),array("apply_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

    }

}
