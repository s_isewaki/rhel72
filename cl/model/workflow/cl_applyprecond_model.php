<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_applyprecond_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
    var $insert_sql = "INSERT INTO cl_applyprecond VALUES ( :apply_id, :precond_wkfw_id, :precond_order, :precond_apply_id, :delete_flg, current_timestamp, :create_user, :update_date, :update_user )";
    /* 更新用SQL */
    var $update_sql = "UPDATE cl_applyprecond SET precond_wkfw_id = :precond_wkfw_id,precond_order = :precond_order,precond_apply_id = :precond_apply_id,delete_flg = :delete_flg, update_date = current_timestamp, update_user = :update_user WHERE apply_id = :apply_id";
    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_applyprecond WHERE apply_id = :apply_id ";
    /* 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_applyprecond SET delete_flag = :delete_flag, update_date = current_timestamp, update_user = :update_user WHERE apply_id = :apply_id";
    /* 一覧取得用SQL */
    var $select_list_sql = "SELECT apply_id, precond_wkfw_id, precond_order, precond_apply_id, delete_flg, create_date, create_user, update_date, update_user FROM cl_applyprecond ORDER BY create_date";
    /* レコード取得用SQL */
    var $select_rec_sql = "SELECT apply_id, precond_wkfw_id, precond_order, precond_apply_id, delete_flg, create_date, create_user, update_date, update_user FROM cl_applyprecond WHERE apply_id = :apply_id";

    /* 登録用カラムタイプ */
    var $insert_col_type = array('text','text','integer','text','boolean','text','timestamp','text');
    /* 更新用カラムタイプ */
    var $update_col_type = array('text','integer','integer','boolean','text','text');

    /* 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');
    /* 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('boolean','text','text');

    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_applyprecond_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param["delete_flg"]="f";
        $param["create_user"]=$this->user;
        $param["update_user"]=null;
        $param["update_date"]=null;

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $apply_id
     */
    function physical_delete($id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["apply_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $apply_id
     */
    function logical_delete($id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flag"]=1;
        $param["apply_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * list
     * @param type $apply_id
     */
    function getList(){

        $this->log->info("select() START");
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

     }

    /**
     * select
     * @param type $apply_id
     */
    function select($id){

        $this->log->info("select() START");
        $data=parent::select($this->select_rec_sql,array("text"),array("apply_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }



}
