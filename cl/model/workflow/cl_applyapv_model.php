<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_applyapv_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
    var $insert_sql = "INSERT INTO cl_applyapv VALUES ( :apply_id, :wkfw_id, :apv_order, :emp_id, :apv_stat, :apv_date, :delete_flg, :apv_comment, :st_div, :deci_flg, :emp_class, :emp_attribute, :emp_dept, :emp_st, :apv_fix_show_flg, :emp_room, :apv_sub_order, :multi_apv_flg, :next_notice_div, :parent_pjt_id, :child_pjt_id, :other_apv_flg, :draft_flg, :eval_content, current_timestamp, :create_user, :update_date, :update_user, :agent_user )";

    /* 登録用カラムタイプ */
    var $insert_col_type = array('text','text','integer','text','text','text','boolean','text','integer','boolean','integer','integer','integer','integer','boolean','integer','integer','boolean','integer','integer','integer','boolean','boolean','text','text','timestamp','text','text');



    /* 更新用SQL */
    var $update_sql = "UPDATE cl_applyapv SET emp_id=:emp_id ,apv_stat=:apv_stat ,apv_date=:apv_date ,apv_comment=:apv_comment ,st_div=:st_div ,deci_flg=:deci_flg ,emp_class=:emp_class ,emp_attribute=:emp_attribute ,emp_dept=:emp_dept ,emp_st=:emp_st ,apv_fix_show_flg=:apv_fix_show_flg ,emp_room=:emp_room ,multi_apv_flg=:multi_apv_flg ,next_notice_div=:next_notice_div ,parent_pjt_id=:parent_pjt_id ,child_pjt_id=:child_pjt_id ,other_apv_flg=:other_apv_flg ,draft_flg=:draft_flg ,eval_content=:eval_content ,update_date=current_timestamp ,update_user = :update_user  WHERE apply_id = :apply_id and apv_order = :apv_order and apv_sub_order = :apv_sub_order";
    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_applyapv WHERE apply_id = :apply_id ";
    /* 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_applyapv SET delete_flg = :delete_flg , update_date=current_timestamp,update_user = :update_user WHERE apply_id = :apply_id";
    /* 一覧取得用SQL */
    var $select_list_sql = "SELECT wkfw_id,apply_id,apv_order,emp_id,apv_stat,apv_date,delete_flg,apv_comment,st_div,deci_flg,emp_class,emp_attribute,emp_dept,emp_st,apv_fix_show_flg,emp_room,apv_sub_order,multi_apv_flg,next_notice_div,parent_pjt_id,child_pjt_id,other_apv_flg,draft_flg,eval_content,create_date,create_user,update_date,update_user,agent_user FROM cl_applyapv ORDER BY create_date";
    /* レコード取得用SQL */
    var $select_rec_sql = "SELECT wkfw_id,apply_id,apv_order,emp_id,apv_stat,apv_date,delete_flg,apv_comment,st_div,deci_flg,emp_class,emp_attribute,emp_dept,emp_st,apv_fix_show_flg,emp_room,apv_sub_order,multi_apv_flg,next_notice_div,parent_pjt_id,child_pjt_id,other_apv_flg,draft_flg,eval_content,create_date,create_user,update_date,update_user,agent_user FROM cl_applyapv WHERE apply_id = :apply_id";
    /* 承認者数取得用SQL */
    var $allapvcnt_sql = "select count(*) as cnt from cl_applyapv where apply_id = :apply_id";
    /* 指定承認ステータス数取得用SQL */
    var $apvstatcnt_sql = "select count(*) as cnt from cl_applyapv where apply_id = :apply_id and apv_stat = :apv_stat";
    /* 最終承認階層番号取得用SQL */
    var $last_apv_order_sql = "select max(apv_order) as max from cl_applyapv where apply_id = :apply_id";
    /* 承認者数取得用SQL(同一階層) */
    var $same_hierarchy_apvcnt_sql = "select count(*) as cnt from cl_applyapv where apply_id = :apply_id and apv_order = :apv_order";
    /* 指定承認ステータス数取得用SQL(同一階層) */
    var $same_hierarchy_apvstatcnt_sql = "select count(*) as cnt from cl_applyapv where apply_id = :apply_id and apv_order = :apv_order and apv_stat = :apv_stat";
    /* 更新用SQL(権限並列用) */
    var $update_applyapv_for_parallel_sql = "update cl_applyapv set apv_stat = :apv_stat ,other_apv_flg = 't' ,apv_date = :apv_date , update_date = current_timestamp ,update_user = :update_user where apply_id = :apply_id and apv_order = :apv_order and apv_stat = 0";
    /* 更新用SQL(詳細) */
    var $update_apvstat_detail_sql = "update cl_applyapv set apv_stat = :apv_stat, apv_date = :apv_date, apv_comment = :apv_comment, draft_flg = 'f', update_date = current_timestamp ,update_user = :update_user where apply_id = :apply_id and apv_order = :apv_order and apv_sub_order = :apv_sub_order";
    /* 更新用SQL(一覧) */
    var $update_apvstat_list_sql = "update cl_applyapv set apv_stat = :apv_stat ,apv_date = :apv_date ,draft_flg = 'f' ,update_date = current_timestamp ,update_user = :update_user where apply_id = :apply_id and apv_order = :apv_order and apv_sub_order = :apv_sub_order";
    /* 下書き更新用SQL */
    var $update_draft_flg_sql = "update cl_applyapv set apv_comment = :apv_comment , draft_flg = :draft_flg ,update_date = current_timestamp ,update_user = :update_user where apply_id = :apply_id and apv_order = :apv_order and apv_sub_order = :apv_sub_order";


  //var $insert_col_type = array('text','text');
    /* 更新用カラムタイプ */
    var $update_col_type = array('text','text','text','text','integer','boolean','integer','integer','integer','integer','boolean','integer','boolean','integer','integer','integer','boolean','boolean','text','text','text','text','integer','integer');
    /* 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');
    /* 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('boolean','text','text');
    /* 指定承認ステータス数取得用カラムタイプ */
    var $apvstatcnt_col_type = array('text','text');
    /* 承認者数取得用カラムタイプ(同一階層) */
    var $same_hierarchy_apvcnt_col_type = array('text','integer');
    /* 指定承認ステータス数取得用カラムタイプ(同一階層) */
    var $same_hierarchy_apvstatcnt_col_type = array('text','integer','text');
    /* 更新用カラムタイプ(権限並列用) */
    var $update_applyapv_for_parallel_col_type = array('text','text','text','text','integer');
    /* 更新用カラムタイプ(詳細) */
    var $update_apvstat_detail_col_type = array('text','text','text','text','text','integer','integer');
    /* 更新用カラムタイプ(一覧) */
    var $update_apvstat_list_col_type = array('text','text','text','text','integer','integer');
    /* 下書き更新用カラムタイプ */
    var $update_draft_flg_col_type = array('text','boolean','text','text','integer','integer');


	//---------------------------------------------
	// 代理承認用
	//---------------------------------------------
    /* 更新用SQL(権限並列用 代理人) */
    var $agent_update_applyapv_for_parallel_sql = "update cl_applyapv set apv_stat = :apv_stat ,other_apv_flg = 't' ,apv_date = :apv_date , update_date = current_timestamp ,update_user = :update_user , agent_user=:agent_user WHERE apply_id = :apply_id and apv_order = :apv_order and apv_stat = 0";
    /* 更新用SQL(詳細 代理人) */
    var $agent_update_apvstat_detail_sql = "update cl_applyapv set apv_stat = :apv_stat, apv_date = :apv_date, apv_comment = :apv_comment, draft_flg = 'f', update_date = current_timestamp ,update_user = :update_user , agent_user=:agent_user WHERE apply_id = :apply_id and apv_order = :apv_order and apv_sub_order = :apv_sub_order";
    /* 更新用SQL(一覧 代理人) */
    var $agent_update_apvstat_list_sql = "update cl_applyapv set apv_stat = :apv_stat ,apv_date = :apv_date ,draft_flg = 'f' ,update_date = current_timestamp ,update_user = :update_user , agent_user=:agent_user WHERE apply_id = :apply_id and apv_order = :apv_order and apv_sub_order = :apv_sub_order";
    /* 下書き更新用SQL(代理更新用) */
    var $agent_update_draft_flg_sql = "update cl_applyapv set apv_comment = :apv_comment , draft_flg = :draft_flg ,update_date = current_timestamp ,update_user = :update_user , agent_user=:agent_user WHERE apply_id = :apply_id and apv_order = :apv_order and apv_sub_order = :apv_sub_order";

    /* 更新用カラムタイプ(権限並列用 代理人) */
    var $agent_update_applyapv_for_parallel_col_type = array('text','text','text','text','text','integer');
    /* 更新用カラムタイプ(詳細 代理人) */
    var $agent_update_apvstat_detail_col_type = array('text','text','text','text','text','text','integer','integer');
    /* 更新用カラムタイプ(一覧 代理人) */
    var $agent_update_apvstat_list_col_type = array('text','text','text','text','text','integer','integer');
    /* 下書き更新用カラムタイプ(代理人) */
    var $agent_update_draft_flg_col_type = array('text','boolean','text','text','text','integer','integer',);

/*
 *
apply_id			text
wkfw_id				text
apv_order			integer
emp_id				text
apv_stat			text
apv_date			text
delete_flg			boolean
apv_comment			text
st_div				integer
deci_flg			boolean
emp_class			integer
emp_attribute		integer
emp_dept			integer
emp_st				integer
apv_fix_show_flg	boolean
emp_room			integer
apv_sub_order		integer
multi_apv_flg		boolean
next_notice_div		integer
parent_pjt_id		integer
child_pjt_id		integer
other_apv_flg		boolean
draft_flg			boolean
eval_content		text
create_date			timestamp
create_user			text
update_date			timestamp
update_user			text
agent_user			text
 */

    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_applyapv_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param["delete_flg"]="f";
        $param["create_user"]=$this->user;
        $param["update_user"]=null;
        $param["update_date"]=null;
        $param["agent_user"]=null;


		/**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $apply_id
     */
    function physical_delete($apply_id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["apply_id"]=$apply_id;
        //$param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $apply_id
     */
    function logical_delete($apply_id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flg"]=true;
        $param["apply_id"]=$apply_id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * getList
     */
    function getList(){

        $this->log->info("select() START");
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

     }

    /**
     * select
     * @param type $apply_id
     */
    function select($apply_id){

        $this->log->info("select() START");
        $data=parent::select($this->select_rec_sql,array("text"),array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }


     /**
      * get_allapvcnt
      * @param type $apply_id
      */
    function get_allapvcnt($apply_id){

        $this->log->debug("select() START");
        $data=parent::select($this->allapvcnt_sql,array("text"),array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0]['cnt'];

    }

     /**
      * get_apvstatcnt
      * @param type $param
      */
    function get_apvstatcnt($param){

        $this->log->debug("select() START");
        $data=parent::select($this->apvstatcnt_sql,$this->apvstatcnt_col_type,$param,MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0]['cnt'];

    }

     /**
      * get_last_apv_order
      * @param type $apply_id
      */
    function get_last_apv_order($apply_id){

        $this->log->debug("select() START");
        $data=parent::select($this->last_apv_order_sql,array("text"),array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0]['max'];

    }

     /**
      * get_same_hierarchy_apvcnt
      * @param type $param
      */
    function get_same_hierarchy_apvcnt($param){

        $this->log->debug("select() START");
        $data=parent::select($this->same_hierarchy_apvcnt_sql,$this->same_hierarchy_apvcnt_col_type,$param,MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0]['cnt'];

    }

     /**
      * get_same_hierarchy_apvstatcnt
      * @param type $param
      */
    function get_same_hierarchy_apvstatcnt($param){

        $this->log->debug("select() START");
        $data=parent::select($this->same_hierarchy_apvstatcnt_sql,$this->same_hierarchy_apvstatcnt_col_type,$param,MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0]['cnt'];

    }

     /**
      * update_applyapv_for_parallel
      * @param type $param
      */
    function update_applyapv_for_parallel($param){

        $this->log->debug("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
		if($param["agent_user"] != "" ){
			$rtn=parent::execute($this->agent_update_applyapv_for_parallel_sql,$this->agent_update_applyapv_for_parallel_col_type,$param);
			$this->log->debug("update_applyapv_for_parallel(代理承認)");

		}else{
			$param["agent_user"] = "";
			$rtn=parent::execute($this->agent_update_applyapv_for_parallel_sql,$this->agent_update_applyapv_for_parallel_col_type,$param);
			$this->log->debug("update_applyapv_for_parallel(通常承認)");
		}

        //$rtn=parent::execute($this->update_applyapv_for_parallel_sql,$this->update_applyapv_for_parallel_col_type,$param);

        $this->log->debug("update() END");

        return $rtn;

    }


     /**
      * update_apvstat_detail
      * @param type $param
      */
    function update_apvstat_detail($param){

        $this->log->debug("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
		// agent_userに値がある場合(代理承認)の場合はagent_userを追加したSQLを使う
		if($param["agent_user"] != "" ){

			$rtn=parent::execute($this->agent_update_apvstat_detail_sql,$this->agent_update_apvstat_detail_col_type,$param);
			$this->log->debug("update_apvstat_detail(代理承認)");

		}else{
			$param["agent_user"] = "";
			$rtn=parent::execute($this->agent_update_apvstat_detail_sql,$this->agent_update_apvstat_detail_col_type,$param);
			$this->log->debug("update_apvstat_detail(通常承認)");
		}

        //$rtn=parent::execute($this->update_apvstat_detail_sql,$this->update_apvstat_detail_col_type,$param);

        $this->log->debug("update() END");

        return $rtn;

    }

     /**
      * update_applyapv_for_parallel
      * @param type $param
      */
    function update_apvstat_list($param){

        $this->log->debug("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
		// agent_userに値がある場合(代理承認)の場合はagent_userを追加したSQLを使う
		if($param["agent_user"] != "" ){

			$rtn=parent::execute($this->agent_update_apvstat_list_sql,$this->agent_update_apvstat_list_col_type,$param);
			$this->log->debug("update_applyapv_for_parallel(代理承認)");

		}else{

			$param["agent_user"] = "";
			$rtn=parent::execute($this->agent_update_apvstat_list_sql,$this->agent_update_apvstat_list_col_type,$param);
			$this->log->debug("update_applyapv_for_parallel(通常承認)");
		}

        //$rtn=parent::execute($this->update_apvstat_list_sql,$this->update_apvstat_list_col_type,$param);

        $this->log->debug("update() END");

        return $rtn;

    }

     /**
      * update_draft_flg
      * @param type $param
      */
    function update_draft_flg($param){

        $this->log->debug("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */

		// agent_userに値がある場合(代理承認)の場合はagent_userを追加した
		if($param["agent_user"] != "" ){

			$rtn=parent::execute($this->agent_update_draft_flg_sql,$this->agent_update_draft_flg_col_type,$param);
			$this->log->debug("update_draft_flg(代理承認)");

		}else{

			$param["agent_user"] = "";
			$rtn=parent::execute($this->agent_update_draft_flg_sql,$this->agent_update_draft_flg_col_type,$param);
			$this->log->debug("update_draft_flg(通常承認)");

			//$rtn=parent::execute($this->update_draft_flg_sql,$this->update_draft_flg_col_type,$param);
		}

        $this->log->debug("update() END");

        return $rtn;

    }

}
