<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_applynotice_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
    var $insert_sql = "INSERT INTO cl_applynotice VALUES ( :apply_id , :recv_emp_id, :confirmed_flg, :send_emp_id, :send_date, :delete_flg, :rslt_ntc_div, current_timestamp , :create_user, :update_date, :update_user )";
    /* 更新用SQL */
    var $update_sql = "UPDATE cl_applynotice SET recv_emp_id = :recv_emp_id, confirmed_flg = :confirmed_flg, send_emp_id = :send_emp_id, send_date = :send_date, delete_flg = :delete_flg, rslt_ntc_div = :rslt_ntc_div, update_date = current_timestamp, update_user = :update_user WHERE apply_id = :apply_id";
    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_applynotice WHERE apply_id = :apply_id ";
    /* 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_applynotice SET delete_flag = :delete_flag, update_date = current_timestamp, update_user = :update_user WHERE apply_id = :apply_id";
    /* 一覧取得用SQL */
    var $select_list_sql = "SELECT apply_id, recv_emp_id, confirmed_flg, send_emp_id, send_date, delete_flg, rslt_ntc_div, create_date, create_user, update_date, update_user FROM cl_applynotice ORDER BY create_date";
    /* レコード取得用SQL */
    var $select_rec_sql = "SELECT apply_id, recv_emp_id, confirmed_flg, send_emp_id, send_date, delete_flg, rslt_ntc_div, create_date, create_user, update_date, update_user FROM cl_applynotice WHERE apply_id = :apply_id";
    /* 申請結果通知・送信者更新用SQL */
    var $update_send_sql = "UPDATE cl_applynotice SET send_emp_id = :send_emp_id ,send_date = :send_date ,update_date = current_timestamp ,update_user = :update_user WHERE apply_id = :apply_id";

    /* 登録用カラムタイプ */
    var $insert_col_type = array('text','text','boolean','text','text','boolean','integer','text','timestamp','text');
    /* 更新用カラムタイプ */
    var $update_col_type = array('text','boolean','text','text','boolean','integer','text','text');

    /* 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');
    /* 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('boolean','text','text');

    /* 申請結果通知・送信者更新用カラムタイプ */
    var $update_send_col_type = array('text','text','text','text');

    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_applynotice_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param["delete_flg"]="f";
        $param["create_user"]=$this->user;
        $param["update_user"]=null;
        $param["update_date"]=null;

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $apply_id
     */
    function physical_delete($id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["apply_id"]=$id;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $apply_id
     */
    function logical_delete($id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flag"]=1;
        $param["apply_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * list
     * @param type $apply_id
     */
    function getList(){

        $this->log->info("select() START");
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

     }

    /**
     * select
     * @param type $apply_id
     */
    function select($id){

        $this->log->info("select() START");
        $data=parent::select($this->select_rec_sql,array("text"),array("apply_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }

    /**
     * update_send
     * @param type $param
     */
    function update_send($param){

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_send_sql,$this->update_send_col_type,$param);

        $this->log->info("update() END");

        return $rtn;

    }

}
