<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_applyasyncrecv_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
    var $insert_sql = "INSERT INTO cl_applyasyncrecv VALUES ( :apply_id, :send_apv_order, :send_apv_sub_order, :recv_apv_order, :recv_apv_sub_order, :send_apved_order, :apv_show_flg, :delete_flg, current_timestamp, :create_user, :update_date, :update_user )";

    /* 登録用カラムタイプ */
    var $insert_col_type = array('text','integer','integer','integer','integer','integer','boolean','boolean','text','timestamp','text');

    /* 更新用SQL */
    var $update_sql = "UPDATE cl_applyasyncrecv SET send_apv_order = :send_apv_order, send_apv_sub_order = :send_apv_sub_order, recv_apv_order = :recv_apv_order, recv_apv_sub_order = :recv_apv_sub_order, send_apved_order = :send_apved_order, apv_show_flg = :apv_show_flg, delete_flg = :delete_flg ,update_date = current_timestamp ,update_user = :update_user WHERE apply_id = :apply_id";
    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_applyasyncrecv WHERE apply_id = :apply_id ";
    /* 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_applyasyncrecv SET delete_flag = :delete_flag WHERE apply_id = :apply_id";
    /* 一覧取得用SQL */
    var $select_list_sql = "SELECT apply_id,send_apv_order,send_apv_sub_order,recv_apv_order,recv_apv_sub_order,send_apved_order,apv_show_flg,delete_flg FROM cl_applyasyncrecv ORDER BY create_date";
    /* レコード取得用SQL */
    var $select_rec_sql = "SELECT apply_id,send_apv_order,send_apv_sub_order,recv_apv_order,recv_apv_sub_order,send_apved_order,apv_show_flg,delete_flg FROM cl_applyasyncrecv WHERE apply_id = :apply_id";
    /* 承認更新番号取得用SQL */
    var $select_max_send_apved_order_sql = "SELECT max(send_apved_order) as max FROM cl_applyasyncrecv WHERE apply_id = :apply_id and send_apv_order = :send_apv_order";

    /* 更新用カラムタイプ */
    var $update_col_type = array('integer','integer','integer','integer','integer','boolean','boolean','text');

    /* 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');
    /* 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('boolean','text');

    /* 承認更新番号取得用カラムタイプ */
    var $select_max_send_apved_order_col_type = array('text','integer');

    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_applyasyncrecv_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param["delete_flg"]="f";
        $param["create_user"]=$this->user;
        $param["update_user"]=null;
        $param["update_date"]=null;

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $apply_id
     */
    function physical_delete($id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["apply_id"]=$id;
        //$param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $apply_id
     */
    function logical_delete($id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flag"]=1;
        $param["apply_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * list
     * @param type $apply_id
     */
    function getList(){

        $this->log->info("select() START");
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

     }

    /**
     * select
     * @param type $apply_id
     */
    function select($id){

        $this->log->info("select() START");
        $data=parent::select($this->select_rec_sql,array("text"),array("apply_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }

    /**
     * select_max_send_apved_order
     * @param type $param
     */
    function select_max_send_apved_order($param){

        $this->log->debug("select() START");
        $data=parent::select($this->select_max_send_apved_order_sql,$this->select_max_send_apved_order_col_type,$param,MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0]['max'];

     }

    /**
     * update_apv_show_flg
     * @param type $param
     */
    function update_apv_show_flg($param) {

        $this->log->info("update() START");

        $sql  = "";
        $sql .= "UPDATE ";
        $sql .= " cl_applyasyncrecv ";
        $sql .= "SET ";
        $sql .= " apv_show_flg = 't' ";
        $sql .= " ,send_apved_order = :send_apved_order ";
        $sql .= " ,update_date = current_timestamp ";
        $sql .= " ,update_user = :update_user ";
        $sql .= "WHERE ";
        $sql .= " apply_id = :apply_id ";
        $sql .= " and send_apv_order = :send_apv_order ";
        if ($param['apv_sub_order'] != ''){
            $sql .= " and send_apv_sub_order = :send_apv_sub_order";
        }

        /**
         * Parameter
         */
        $param_edit = array(
			"send_apved_order"		=> $param["apved_order"]
			,"update_user"			=> $this->user
			,"apply_id"				=> $param["apply_id"]
			,"send_apv_order"		=> $param["apv_order"]
		);
		$col_type = array("integer","text","text","integer");

		if ($param['apv_sub_order'] != ''){
			$param_edit += array("send_apv_sub_order" => $param['apv_sub_order']);
			array_push($col_type ,"integer");
		}

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$col_type,$param_edit);

        $this->log->info("update() END");

        return $rtn;
    }

}
