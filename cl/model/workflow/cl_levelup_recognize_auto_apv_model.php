<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_levelup_recognize_auto_apv_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* レコード取得用SQL */
    var $select_rec_sql = "SELECT emp_id , now_level , get_level1_date , get_level2_date , get_level3_date , get_level4_date , get_level5_date , delete_flg , create_date , create_user , update_date , update_user FROM cl_personal_profile WHERE emp_id = :emp_id";

    /**
     * Constractor
     */
    function cl_levelup_recognize_auto_apv_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * select
     * @param type $id
     */
    function select($id){

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	emp_id ";
		$sql .= " 	, now_level ";
		$sql .= " 	, get_level1_date ";
		$sql .= " 	, get_level2_date ";
		$sql .= " 	, get_level3_date ";
		$sql .= " 	, get_level4_date ";
		$sql .= " 	, get_level5_date ";
		$sql .= " 	, level1_total_value ";
		$sql .= " 	, level2_total_value ";
		$sql .= " 	, level3_total_value ";
		$sql .= " 	, level4_total_value ";
		$sql .= " 	, level5_total_value ";
		$sql .= " 	, level1_certificate_date ";
		$sql .= " 	, level2_certificate_date ";
		$sql .= " 	, level3_certificate_date ";
		$sql .= " 	, level4_certificate_date ";
		$sql .= " 	, level5_certificate_date ";
		$sql .= " 	, level1_levelup_recognize_id ";
		$sql .= " 	, level2_levelup_recognize_id ";
		$sql .= " 	, level3_levelup_recognize_id ";
		$sql .= " 	, level4_levelup_recognize_id ";
		$sql .= " 	, level5_levelup_recognize_id ";
		$sql .= " 	, get_level1_class ";
		$sql .= " 	, get_level2_class ";
		$sql .= " 	, get_level3_class ";
		$sql .= " 	, get_level4_class ";
		$sql .= " 	, get_level5_class ";
		$sql .= " 	, get_level1_atrb ";
		$sql .= " 	, get_level2_atrb ";
		$sql .= " 	, get_level3_atrb ";
		$sql .= " 	, get_level4_atrb ";
		$sql .= " 	, get_level5_atrb ";
		$sql .= " 	, get_level1_dept ";
		$sql .= " 	, get_level2_dept ";
		$sql .= " 	, get_level3_dept ";
		$sql .= " 	, get_level4_dept ";
		$sql .= " 	, get_level5_dept ";
		$sql .= " 	, get_level1_room ";
		$sql .= " 	, get_level2_room ";
		$sql .= " 	, get_level3_room ";
		$sql .= " 	, get_level4_room ";
		$sql .= " 	, get_level5_room ";
		$sql .= " FROM ";
		$sql .= " 	cl_personal_profile ";
		$sql .= " WHERE ";
		$sql .= " 	emp_id = :emp_id ";

        $this->log->info("select() START");
        $data=parent::select($sql,array("text"),array("emp_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }

    /**
     * insert
     * @param type $param
     */
    function insert_level($param) {

        $this->log->info("insert_level() START");

		$sql = "";
		$sql .= " INSERT INTO cl_personal_profile( ";
		$sql .= " 	emp_id ";
		$sql .= " 	, now_level ";
		$sql .= " 	, get_level1_date ";
		$sql .= " 	, get_level2_date ";
		$sql .= " 	, get_level3_date ";
		$sql .= " 	, get_level4_date ";
		$sql .= " 	, get_level5_date ";
		$sql .= " 	, level1_total_value ";
		$sql .= " 	, level2_total_value ";
		$sql .= " 	, level3_total_value ";
		$sql .= " 	, level4_total_value ";
		$sql .= " 	, level5_total_value ";
		$sql .= " 	, level1_certificate_date ";
		$sql .= " 	, level2_certificate_date ";
		$sql .= " 	, level3_certificate_date ";
		$sql .= " 	, level4_certificate_date ";
		$sql .= " 	, level5_certificate_date ";
		$sql .= " 	, level1_levelup_recognize_id ";
		$sql .= " 	, level2_levelup_recognize_id ";
		$sql .= " 	, level3_levelup_recognize_id ";
		$sql .= " 	, level4_levelup_recognize_id ";
		$sql .= " 	, level5_levelup_recognize_id ";
		$sql .= " 	, get_level1_class ";
		$sql .= " 	, get_level2_class ";
		$sql .= " 	, get_level3_class ";
		$sql .= " 	, get_level4_class ";
		$sql .= " 	, get_level5_class ";
		$sql .= " 	, get_level1_atrb ";
		$sql .= " 	, get_level2_atrb ";
		$sql .= " 	, get_level3_atrb ";
		$sql .= " 	, get_level4_atrb ";
		$sql .= " 	, get_level5_atrb ";
		$sql .= " 	, get_level1_dept ";
		$sql .= " 	, get_level2_dept ";
		$sql .= " 	, get_level3_dept ";
		$sql .= " 	, get_level4_dept ";
		$sql .= " 	, get_level5_dept ";
		$sql .= " 	, get_level1_room ";
		$sql .= " 	, get_level2_room ";
		$sql .= " 	, get_level3_room ";
		$sql .= " 	, get_level4_room ";
		$sql .= " 	, get_level5_room ";
		$sql .= " 	, delete_flg ";
		$sql .= " 	, create_date ";
		$sql .= " 	, create_user ";
		$sql .= " ) VALUES (:emp_id	 ";
		$sql .= " 	, :now_level ";
		$sql .= " 	, :get_level1_date ";
		$sql .= " 	, :get_level2_date ";
		$sql .= " 	, :get_level3_date ";
		$sql .= " 	, :get_level4_date ";
		$sql .= " 	, :get_level5_date ";
		$sql .= " 	, :level1_total_value ";
		$sql .= " 	, :level2_total_value ";
		$sql .= " 	, :level3_total_value ";
		$sql .= " 	, :level4_total_value ";
		$sql .= " 	, :level5_total_value ";
		$sql .= " 	, :level1_certificate_date ";
		$sql .= " 	, :level2_certificate_date ";
		$sql .= " 	, :level3_certificate_date ";
		$sql .= " 	, :level4_certificate_date ";
		$sql .= " 	, :level5_certificate_date ";
		$sql .= " 	, :level1_levelup_recognize_id ";
		$sql .= " 	, :level2_levelup_recognize_id ";
		$sql .= " 	, :level3_levelup_recognize_id ";
		$sql .= " 	, :level4_levelup_recognize_id ";
		$sql .= " 	, :level5_levelup_recognize_id ";
		$sql .= " 	, :get_level1_class ";
		$sql .= " 	, :get_level2_class ";
		$sql .= " 	, :get_level3_class ";
		$sql .= " 	, :get_level4_class ";
		$sql .= " 	, :get_level5_class ";
		$sql .= " 	, :get_level1_atrb ";
		$sql .= " 	, :get_level2_atrb ";
		$sql .= " 	, :get_level3_atrb ";
		$sql .= " 	, :get_level4_atrb ";
		$sql .= " 	, :get_level5_atrb ";
		$sql .= " 	, :get_level1_dept ";
		$sql .= " 	, :get_level2_dept ";
		$sql .= " 	, :get_level3_dept ";
		$sql .= " 	, :get_level4_dept ";
		$sql .= " 	, :get_level5_dept ";
		$sql .= " 	, :get_level1_room ";
		$sql .= " 	, :get_level2_room ";
		$sql .= " 	, :get_level3_room ";
		$sql .= " 	, :get_level4_room ";
		$sql .= " 	, :get_level5_room ";
		$sql .= " 	, :delete_flg ";
		$sql .= " 	, current_timestamp ";
		$sql .= " 	, :create_user) ";

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["create_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($sql,array("text","integer","date","date","date","date","date","integer","integer","integer","integer","integer","date","date","date","date","date","text","text","text","text","text","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","text"),$param);

        $this->log->info("insert_level() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update_level($param) {

        $this->log->info("update_level() START");

		$sql = "";
		$sql .= " UPDATE cl_personal_profile ";
		$sql .= "    SET ";
		$sql .= " 	now_level=:now_level ";
		$sql .= " 	, get_level1_date=:get_level1_date ";
		$sql .= " 	, get_level2_date=:get_level2_date ";
		$sql .= " 	, get_level3_date=:get_level3_date ";
		$sql .= " 	, get_level4_date=:get_level4_date ";
		$sql .= " 	, get_level5_date=:get_level5_date ";
		$sql .= " 	, level1_total_value=:level1_total_value ";
		$sql .= " 	, level2_total_value=:level2_total_value ";
		$sql .= " 	, level3_total_value=:level3_total_value ";
		$sql .= " 	, level4_total_value=:level4_total_value ";
		$sql .= " 	, level5_total_value=:level5_total_value ";
		$sql .= " 	, level1_certificate_date=:level1_certificate_date ";
		$sql .= " 	, level2_certificate_date=:level2_certificate_date ";
		$sql .= " 	, level3_certificate_date=:level3_certificate_date ";
		$sql .= " 	, level4_certificate_date=:level4_certificate_date ";
		$sql .= " 	, level5_certificate_date=:level5_certificate_date ";
		$sql .= " 	, level1_levelup_recognize_id=:level1_levelup_recognize_id ";
		$sql .= " 	, level2_levelup_recognize_id=:level2_levelup_recognize_id ";
		$sql .= " 	, level3_levelup_recognize_id=:level3_levelup_recognize_id ";
		$sql .= " 	, level4_levelup_recognize_id=:level4_levelup_recognize_id ";
		$sql .= " 	, level5_levelup_recognize_id=:level5_levelup_recognize_id ";
		$sql .= " 	, get_level1_class=:get_level1_class ";
		$sql .= " 	, get_level2_class=:get_level2_class ";
		$sql .= " 	, get_level3_class=:get_level3_class ";
		$sql .= " 	, get_level4_class=:get_level4_class ";
		$sql .= " 	, get_level5_class=:get_level5_class ";
		$sql .= " 	, get_level1_atrb=:get_level1_atrb ";
		$sql .= " 	, get_level2_atrb=:get_level2_atrb ";
		$sql .= " 	, get_level3_atrb=:get_level3_atrb ";
		$sql .= " 	, get_level4_atrb=:get_level4_atrb ";
		$sql .= " 	, get_level5_atrb=:get_level5_atrb ";
		$sql .= " 	, get_level1_dept=:get_level1_dept ";
		$sql .= " 	, get_level2_dept=:get_level2_dept ";
		$sql .= " 	, get_level3_dept=:get_level3_dept ";
		$sql .= " 	, get_level4_dept=:get_level4_dept ";
		$sql .= " 	, get_level5_dept=:get_level5_dept ";
		$sql .= " 	, get_level1_room=:get_level1_room ";
		$sql .= " 	, get_level2_room=:get_level2_room ";
		$sql .= " 	, get_level3_room=:get_level3_room ";
		$sql .= " 	, get_level4_room=:get_level4_room ";
		$sql .= " 	, get_level5_room=:get_level5_room ";
		$sql .= " 	, delete_flg=:delete_flg ";
		$sql .= " 	, update_date = current_timestamp  ";
		$sql .= " 	, update_user = :update_user ";
		$sql .= "  WHERE emp_id = :emp_id ";

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($sql,array("integer","date","date","date","date","date","integer","integer","integer","integer","integer","date","date","date","date","date","text","text","text","text","text","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","integer","text","text"),$param);

        $this->log->info("update_level() END");

        return $rtn;
    }
}
