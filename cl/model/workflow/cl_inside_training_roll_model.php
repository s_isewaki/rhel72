<?php
/*
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/cl_common_model.php");
require_once('MDB2.php');
*/
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_inside_training_roll_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
//    var $insert_sql = "INSERT INTO cl_inside_training_roll VALUES ( :training_roll_id, :plan_id, :training_id, :plan_no, :emp_id, :apply_id, :emp_roll, :delete_flg, current_timestamp, :create_user )";
    /* 更新用SQL */
    var $update_sql = "UPDATE cl_inside_training_roll SET plan_id = :plan_id, training_id = :training_id, plan_no = :plan_no, emp_id = :emp_id, apply_id = :apply_id, emp_roll = :emp_roll, update_date = current_timestamp, update_user = :update_user WHERE training_roll_id = :training_roll_id";
    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_inside_training_roll WHERE training_roll_id = :training_roll_id";
    var $physical_delete_from_plan_id_sql = "DELETE FROM cl_inside_training_roll WHERE apply_id = :apply_id AND plan_id = :plan_id";
    /* 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_inside_training_roll SET delete_flg = :delete_flg, update_date = current_timestamp, update_user = update_user  WHERE training_roll_id = :training_roll_id";
    /* 一覧取得用SQL */
    var $select_list_sql = "SELECT training_roll_id, plan_id, training_id, plan_no, emp_id, apply_id, emp_roll, delete_flg, create_date, create_user, update_date, update_user FROM cl_inside_training_roll ORDER BY create_date";
    /* レコード取得用SQL */
    var $select_rec_sql = "SELECT training_roll_id, plan_id, training_id, plan_no, emp_id, apply_id, emp_roll, delete_flg, create_date, create_user, update_date, update_user FROM cl_inside_training_roll WHERE training_roll_id = :training_roll_id";

    /* レコード検索用SQL */
 //   var $search_rec_sql = "SELECT * FROM cl_inside_training_roll WHERE plan_id=:plan_id AND emp_id=:emp_id";
    var $search_rec_sql = "SELECT * FROM cl_inside_training_roll WHERE plan_id=:plan_id AND emp_id=:emp_id AND apply_id=:apply_id";

    /* 研修講座管理(受付・出欠)一覧取得用SQL */
//  var $select_attendance_list_sql = "SELECT roll.emp_id, emp.emp_lt_nm || '　' || emp.emp_ft_nm as emp_name, apl.inside_training_date, case emp.emp_room when null then class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm else class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm || ' > ' || room.room_nm end as affiliation, roll.emp_roll FROM aplcl_inside_training_roll roll inner join empmst emp on apl.emp_id = emp.emp_id left join classmst class on emp.emp_class = class.class_id left join atrbmst atrb on emp.emp_attribute = atrb.atrb_id left join deptmst dept on emp.emp_dept = dept.dept_id left join classroom room on emp.emp_room = room.room_id left join cl_applied_inside_training on roll.plan_id = apl.plan_id and roll.training_id = apl.inside_training_id and roll.emp_id = apl.emp_id WHERE roll.plan_id LIKE :plan_id AND roll.training_id = :training_id";
//    var $select_attendance_list_sql = "SELECT roll.emp_id, emp.emp_lt_nm || '　' || emp.emp_ft_nm as emp_name, apl.inside_training_date, case emp.emp_room when null then class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm else class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm || ' > ' || room.room_nm end as affiliation, roll.emp_roll FROM cl_inside_training_roll roll inner join empmst emp on apl.emp_id = emp.emp_id left join classmst class on emp.emp_class = class.class_id left join atrbmst atrb on emp.emp_attribute = atrb.atrb_id left join deptmst dept on emp.emp_dept = dept.dept_id left join classroom room on emp.emp_room = room.room_id left join cl_applied_inside_training on roll.plan_id = apl.plan_id and roll.training_id = apl.inside_training_id and roll.emp_id = apl.emp_id WHERE roll.plan_id = :plan_id AND roll.training_id = :training_id";

    /* 更新用SQL（出欠登録） */
    //var $update_roll_sql = "UPDATE cl_inside_training_roll SET emp_roll = :emp_roll, update_date = current_timestamp, update_user = :update_user WHERE plan_id = :plan_id AND training_id = :training_id AND emp_id = :emp_id";

    /* 登録用カラムタイプ */
    var $insert_col_type = array('text','text','text','integer','text','text','integer','integer','text');
    /* 更新用カラムタイプ */
    var $update_col_type = array('text','text','integer','text','text','integer','text','text');

    /* 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');
    var $physical_delete_from_plan_id_col_type = array('text','text');
    /* 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('integer','text','text','text');

    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_inside_training_roll_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["create_user"]=$this->user;

        /**
         * 実行
         */
		$sql = "INSERT INTO cl_inside_training_roll VALUES ( :training_roll_id, :plan_id, :training_id, :plan_no, :emp_id, :apply_id, :emp_roll, :delete_flg, current_timestamp, :create_user )";


        $rtn=parent::execute($sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $training_roll_id
     */
    function physical_delete($id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["training_roll_id"]=$id;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     *
     *
     * @param $apply_id
     * @param $plan_id
     */
    function physical_delete_from_plan_id($apply_id,$plan_id) {

        $this->log->info("physical_delete_from_plan_id() START");

        /**
         * Parameter
         */
        $param=array();
        $param["apply_id"]=$apply_id;
        $param["plan_id"]=$plan_id;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_from_plan_id_sql,$this->physical_delete_from_plan_id_col_type,$param);

        $this->log->info("physical_delete_from_plan_id() END");

        return $rtn;
    }
    /**
     * logical_delete
     * @param type $training_roll_id
     */
    function logical_delete($id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flg"]=1;
        $param["training_roll_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * list
     * @param type $training_roll_id
     */
    function getList(){

        $this->log->info("select() START");
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

     }

    /**
     * select
     * @param type $training_roll_id
     */
    function select($id){

        $this->log->info("select() START");
        $data=parent::select($this->select_rec_sql,array("text"),array("training_roll_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }

    /**
     * search Rec
     * @param string $plan_id
     * @param string $emp_id
     * @param string $apply_id ← first_apply_id （cl_applied_training_apply の training_apply_id）
     */
    function searchRec($plan_id, $emp_id, $apply_id){

        $this->log->info("searchRec() START");
//        $data=parent::select($this->search_rec_sql,array("text", "text"),array("plan_id"=>$plan_id, "emp_id"=>$emp_id),MDB2_FETCHMODE_ASSOC);
        $data=parent::select($this->search_rec_sql,array("text", "text", "text"),array("plan_id"=>$plan_id, "emp_id"=>$emp_id, "apply_id"=>$apply_id ),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("searchRec() END");
        return $data[0];

     }

	/**
     * getReceptionList
     * @param string $plan_id
     * @param string $inside_training_id
     */
    function getReceptionList($param){
    	//$plan_id = "%". $plan_id . "%";
        //$this->log->debug("getReceptionList() START   plan_id = ". $plan_id);
        $this->log->debug("getReceptionList() START");

        //$sql = "SELECT roll.emp_id, emp.emp_lt_nm || '　' || emp.emp_ft_nm as emp_name, apl.inside_training_date, case emp.emp_room when null then class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm else class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm || ' > ' || room.room_nm end as affiliation, roll.emp_roll FROM cl_inside_training_roll roll inner join empmst emp on apl.emp_id = emp.emp_id left join classmst class on emp.emp_class = class.class_id left join atrbmst atrb on emp.emp_attribute = atrb.atrb_id left join deptmst dept on emp.emp_dept = dept.dept_id left join classroom room on emp.emp_room = room.room_id left join cl_applied_inside_training on roll.plan_id = apl.plan_id and roll.training_id = apl.inside_training_id and roll.emp_id = apl.emp_id WHERE roll.plan_id = :plan_id AND roll.training_id = :training_id";
		$sql_buff[] = "SELECT DISTINCT ";
		// 2012/08/22 Yamagawa upd(s)
		//$sql_buff[] = "      roll.emp_id, ";
		$sql_buff[] = "      emp.emp_personal_id AS emp_id, ";
		// 2012/08/22 Yamagawa upd(e)
		$sql_buff[] = "      emp.emp_lt_nm || '　' || emp.emp_ft_nm as emp_name, ";
		$sql_buff[] = "      apl.inside_training_date, ";
		$sql_buff[] = "      case when emp.emp_room is null then class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm ";
		$sql_buff[] = "	   else class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm || ' > ' || room.room_nm ";
		$sql_buff[] = "      end as affiliation, ";
		$sql_buff[] = "      roll.emp_roll ";
		// 2012/09/03 Yamagawa add(s)
		$sql_buff[] = " ,CASE ";
		$sql_buff[] = "   WHEN ";
		$sql_buff[] = "    rep.training_report_id IS NOT NULL ";
		$sql_buff[] = "   THEN ";
		$sql_buff[] = "    '○' ";
		$sql_buff[] = "   ELSE ";
		$sql_buff[] = "    '×' ";
		$sql_buff[] = "  END AS report_status ";
		// 2012/09/03 Yamagawa add(e)
		$sql_buff[] = " FROM cl_inside_training_roll AS roll ";
		$sql_buff[] = "INNER JOIN empmst                     AS emp   ON roll.emp_id       = emp.emp_id ";
		$sql_buff[] = " LEFT JOIN classmst                   AS class ON emp.emp_class     = class.class_id ";
		$sql_buff[] = " LEFT JOIN atrbmst                    AS atrb  ON emp.emp_attribute = atrb.atrb_id ";
		$sql_buff[] = " LEFT JOIN deptmst                    AS dept  ON emp.emp_dept      = dept.dept_id ";
		$sql_buff[] = " LEFT JOIN classroom                  AS room  ON emp.emp_room      = room.room_id ";
		$sql_buff[] = " LEFT JOIN cl_applied_inside_training AS apl   ON apl.plan_id  like '%'||roll.plan_id||'%' ";
		$sql_buff[] = "                                              AND roll.training_id  = apl.inside_training_id ";
		$sql_buff[] = "                                              AND roll.emp_id       = apl.emp_id ";
		$sql_buff[] = "                                              AND roll.apply_id     = apl.training_apply_id ";
		// 2012/09/03 Yamagawa add(s)
		$sql_buff[] = " LEFT JOIN ";
		$sql_buff[] = "  cl_apl_inside_training_report rep ";
		$sql_buff[] = " ON ";
		$sql_buff[] = "  rep.training_id = roll.training_id ";
		$sql_buff[] = "  AND rep.plan_id = roll.plan_id ";
		$sql_buff[] = "  AND rep.emp_id = roll.emp_id ";
		$sql_buff[] = "  AND EXISTS( ";
		$sql_buff[] = "   SELECT ";
		$sql_buff[] = "    app.apply_id ";
		$sql_buff[] = "   FROM ";
		$sql_buff[] = "    cl_apply app ";
		$sql_buff[] = "   WHERE ";
		$sql_buff[] = "    app.apply_id = rep.apply_id ";
		$sql_buff[] = "    AND app.apply_stat = 1 ";
		$sql_buff[] = "    AND app.draft_flg = FALSE ";
		$sql_buff[] = "    AND app.delete_flg = FALSE ";
		$sql_buff[] = "  ) ";
		// 2012/09/03 Yamagawa add(e)
		$sql_buff[] = "WHERE roll.training_id = :training_id ";
		$sql_buff[] = "  AND roll.plan_id     = :plan_id ";
        $sql = implode('', $sql_buff);

        $data=parent::select($sql,array("text","text"),$param,MDB2_FETCHMODE_ASSOC);
        $this->log->debug("getReceptionList() END");
        return $data;

    }

	/**
     * setAttendanse
     * @param string $training_id
     * @param string $plan_id
     * @param string $emp_id
     * @param string $emp_roll
     */
    function setAttendanse($param){

    	$sql = "";
    	$sql .= " UPDATE ";
        $sql .= " 	cl_inside_training_roll ";
        $sql .= " SET ";
        $sql .= " 	emp_roll = :emp_roll,  ";
        $sql .= " 	update_date = current_timestamp, ";
        $sql .= " 	update_user = :update_user ";
        $sql .= " WHERE ";
        $sql .= " 	plan_id = :plan_id ";
        $sql .= " 	AND training_id = :training_id ";
        // 2012/08/22 Yamagawa upd(s)
        //$sql .= " 	AND emp_id = :emp_id ";
        $sql .= " 	AND EXISTS( ";
        $sql .= " 	 SELECT ";
        $sql .= " 	  emp.emp_id ";
        $sql .= " 	 FROM ";
        $sql .= " 	  empmst emp ";
        $sql .= " 	 WHERE ";
        $sql .= " 	  emp.emp_personal_id = :emp_id ";
        $sql .= " 	  AND emp.emp_id = cl_inside_training_roll.emp_id ";
        $sql .= " 	) ";
        // 2012/08/22 Yamagawa upd(e)

    	$param["update_user"]=$this->user;
        $this->log->debug("setAttendanse() START");
        $rtn=parent::execute($sql,array("integer","text","text","text","text"),$param);
        $this->log->debug("setAttendanse() END");
        return $rtn;

    }

}
