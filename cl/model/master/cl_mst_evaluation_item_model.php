
<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_mst_evaluation_item_model extends cl_common_model{


    var $con;
    var $log;
    var $user;

    /**
     * Constractor コンストラクタ
     */
    function cl_mst_evaluation_item_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert 登録
     * @param type $param
     * パラメータの例：
     *     $param('item_id'=>item_id_value,'level'=>level_value,'evaluation_id'=>evaluation_id_value,'category_id'=>category_id_value,'group_id'=>group_id_value,'item_name'=>item_name_value,'disp_order'=>disp_order_value,'disp_flg'=>disp_flg_value,'must_value'=>must_value_value,'delete_flg'=>delete_flg_value,'create_user'=>create_user_value,'update_date'=>update_date_value,'update_user'=>update_user_value);
     */
    function insert($param) {

        $this->log->info(__FUNCTION__." START");

        /**
         * Parameter
         */
        //2012/05/02 K.fujii upd(s)
        //$param["delete_flg"]=false;
		$param["delete_flg"]=0;
		//2012/05/02 K.fujii upd(e)
        $param["create_user"]=$this->user;
        $param["update_date"]=null;
        $param["update_user"]=null;

        /* SQL */
        $sql = "INSERT INTO cl_mst_evaluation_item ( item_id,level,evaluation_id,category_id,group_id,item_name,disp_order,disp_flg,must_value,eval_disp_1_flg,eval_disp_2_flg,eval_disp_3_flg,eval_disp_4_flg,delete_flg,create_date,create_user,update_date,update_user ) VALUES ( :item_id,:level,:evaluation_id,:category_id,:group_id,:item_name,:disp_order,:disp_flg,:must_value,:eval_disp_1_flg,:eval_disp_2_flg,:eval_disp_3_flg,:eval_disp_4_flg,:delete_flg,current_timestamp,:create_user,:update_date,:update_user )";

        /* カラムタイプ */
		//2012/05/02 K.fujii upd(s)
        //$type = array('text','integer','text','text','text','text','integer','integer','integer','integer','integer','integer','integer','boolean','text','timestamp','text');
		$type = array('text','integer','text','text','text','text','integer','integer','integer','integer','integer','integer','integer','integer','text','timestamp','text');
		//2012/05/02 K.fujii upd(e)

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }

    /**
     * update 更新
     * @param type $param
     * パラメータの例：
     *     $param('level'=>level_value,'evaluation_id'=>evaluation_id_value,'category_id'=>category_id_value,'group_id'=>group_id_value,'item_name'=>item_name_value,'disp_order'=>disp_order_value,'disp_flg'=>disp_flg_value,'must_value'=>must_value_value,'update_user'=>update_user_value);
     */
    function update($p_param) {

        $this->log->info(__FUNCTION__." START");


        $param=$this->getRecord($p_param["item_id"]);

        /**
         * Ｉ／Ｆされたパラメータを設定する
         */
        foreach( $param as $key => $value ){
        	//if($p_param[$key]!=null && $p_param[$key]!=""){
        		$param[$key]=$p_param[$key];
        	//}
        }
        unset($param['update_date']);
        unset($param['create_date']);
        unset($param['create_user']);

        /**
         * Parameter
         */
        //2012/05/02 K.fujii upd(s)
        //$param["delete_flg"]=false;
		$param["delete_flg"]=0;
		//2012/05/02 K.fujii upd(e)
        $param["update_user"]=$this->user;

        /* SQL */
        $sql = "UPDATE cl_mst_evaluation_item SET level = :level ,evaluation_id = :evaluation_id ,category_id = :category_id ,group_id = :group_id ,item_name = :item_name ,disp_order = :disp_order ,disp_flg = :disp_flg ,must_value = :must_value , eval_disp_1_flg = :eval_disp_1_flg  , eval_disp_2_flg = :eval_disp_2_flg  , eval_disp_3_flg = :eval_disp_3_flg  , eval_disp_4_flg = :eval_disp_4_flg ,delete_flg = :delete_flg ,update_date = current_timestamp ,update_user = :update_user  WHERE item_id = :item_id";

        /* カラムタイプ */
		//2012/05/02 K.fujii upd(s)
        //$type = array('integer' , 'text' , 'text' , 'text' , 'text' , 'integer' , 'integer' ,'integer','integer','integer','integer', 'integer' , 'boolean' , 'text' , 'text');
		$type = array('integer' , 'text' , 'text' , 'text' , 'text' , 'integer' , 'integer' ,'integer','integer','integer','integer', 'integer' , 'integer' , 'text' , 'text');
		//2012/05/02 K.fujii upd(e)

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }
    /**
     * physical_delete 物理削除
     * @param type $item_id プライマリーキー
     */
    function physical_delete($item_id) {

        $this->log->info(__FUNCTION__." START");

        /**
         * Parameter
         */
        $param=array();
        $param["item_id"]=$item_id;

        /* SQL */
        $sql = "DELETE FROM cl_mst_evaluation_item WHERE item_id = :item_id";

        /* カラムタイプ */
        $type = array('text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }

    /**
     * logical_delete 論理削除
     * @param type $item_id プライマリーキー
     */
    function logical_delete($item_id) {

        $this->log->info(__FUNCTION__." START");

        /**
         * Parameter
         */
        $param=array();
        $param["item_id"]=$item_id;
        $param["update_user"]=$this->user;

        /* SQL */
		//2012/05/02 K.fujii upd(s)
        //$sql = "UPDATE cl_mst_evaluation_item SET delete_flg = true , update_date = current_timestamp , update_user = :update_user WHERE item_id = :item_id";
		$sql = "UPDATE cl_mst_evaluation_item SET delete_flg = 1 , update_date = current_timestamp , update_user = :update_user WHERE item_id = :item_id";
		//2012/05/02 K.fujii upd(e)

        /* カラムタイプ */
        $type = array('text','text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }

    /**
     * getList 全件取得
     * @param type $plan_id
     */
    function getList(){

        $this->log->info(__FUNCTION__." START");

        /* SQL */
        $sql = "SELECT * FROM cl_mst_evaluation_item ORDER BY item_id";

        /* カラムタイプ */
        $type = null;

        $data=parent::select($sql,$type,null,MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data;

     }

    /**
     * getRecord レコード取得（プライマリーキー指定）
     * @param type $item_id プライマリーキー
     */
    function getRecord($item_id){

        $this->log->info(__FUNCTION__." START");

        /* レコード取得用SQL */
        $sql = "SELECT * FROM cl_mst_evaluation_item WHERE item_id = :item_id";

        /* カラムタイプ */
        $type = array("text");

        $data=parent::select($sql,$type,array("item_id"=>$item_id),MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data[0];

     }

    /**
     * getRecord 任意SQLレコード取得（プライマリーキー指定）
     * @param type $item_id プライマリーキー
     */
    function getRecordBySql($sql,$item_id){

        $this->log->info(__FUNCTION__." START");

        /* レコード取得用SQL */
        //$sql = "SELECT * FROM cl_mst_evaluation_item WHERE item_id = :item_id";

        /* カラムタイプ */
        $type = array("text");

        $data=parent::select($sql,$type,array("item_id"=>$item_id),MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data[0];

     }
}
