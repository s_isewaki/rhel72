<?php
/**
 * 看護副部長画面の一覧データ取得用モデル
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_mst_setting_council_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /** 登録用SQL */
    var $insert_sql = "INSERT INTO cl_mst_setting_council ( emp_id ,create_date ,create_user ) VALUES ( :emp_id ,current_timestamp ,:create_user )";
    /** 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_mst_setting_council";
    /** 一覧取得用SQL */
    var $select_list_sql = "SELECT cou.emp_id ,emp.emp_lt_nm ,emp.emp_ft_nm ,cou.create_date ,cou.create_user ,cou.update_date ,cou.update_user FROM cl_mst_setting_council cou INNER JOIN empmst emp ON cou.emp_id = emp.emp_id";

    /** 登録用カラムタイプ */
    var $insert_col_type = array('text' ,'text');

    /**
     * Constractor
     */
    function cl_mst_setting_council_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $emp_id
     */
    function insert($emp_id) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param = array(
        	'emp_id'		=> $emp_id,
        	'create_user'	=> $this->user
        );

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * physical_delete
     */
    function physical_delete() {

        $this->log->info("physical_delete() START");

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,null,null);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * getList
     */
    function getList(){

        $this->log->info("select() START");
        $data=parent::select($this->select_list_sql,null,null,MDB2_FETCHMODE_ASSOC);
        $this->log->info("select() END");
        return $data;

     }

	/**
	 * 審議会チェック
	 */
	function check_council($emp_id){

		$this->log->info("check_council() START count(data):".count($data));

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " emp_id ";
		$sql .= "FROM ";
		$sql .= " cl_mst_setting_council ";
		$sql .= "WHERE ";
		$sql .= " emp_id = :emp_id ";

		$data=parent::select($sql,array('text'),array('emp_id'=>$emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->info("check_council() END count(data):".count($data));
		return $data;
	}
}
