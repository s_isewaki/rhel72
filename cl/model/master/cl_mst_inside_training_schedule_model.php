<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_mst_inside_training_schedule_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
    var $insert_sql = "INSERT INTO cl_mst_inside_training_schedule ( plan_id, first_plan_id, training_id, plan_no, year , term_div , training_time , final_flg , plan_date, from_time, to_time, report_division, answer_division, max_people, apply_status, place, remarks, delete_flg, create_date, create_user ) VALUES ( :plan_id, :first_plan_id, :training_id, :plan_no, :year , :term_div , :training_time , :final_flg , CAST( :plan_date AS DATE ) , CAST( :from_time AS TIME ) ,CAST( :to_time AS TIME ) , :report_division, :answer_division, :max_people, :apply_status, :place, :remarks, :delete_flg, current_timestamp, :create_user )";

    /* 更新用SQL */
    var $update_sql = "UPDATE cl_mst_inside_training_schedule SET first_plan_id = :first_plan_id, training_id = :training_id, plan_no = :plan_no, year = :year, term_div = :term_div, training_time = :training_time, final_flg = :final_flg, plan_date = CAST( :plan_date AS DATE), from_time = CAST( :from_time AS TIME), to_time = CAST( :to_time AS TIME), report_division = :report_division, answer_division = :answer_division, max_people = :max_people, apply_status = :apply_status, place = :place, remarks = :remarks, delete_flg = :delete_flg, update_date = current_timestamp, update_user = :update_user WHERE plan_id = :plan_id";

    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_mst_inside_training_schedule WHERE plan_id = :plan_id";
    /* 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_mst_inside_training_schedule SET delete_flg = :delete_flg, update_date = current_timestamp, update_user = :update_user  WHERE plan_id = :plan_id";

    /* 一覧取得用SQL */
    var $select_list_sql = "SELECT plan_id, training_id, plan_no, plan_date, from_time, to_time, report_division, answer_division, max_people, apply_status, place, remarks, delete_flg, create_date, create_user, update_date, update_user FROM cl_mst_inside_training_schedule ORDER BY create_date";

    /* 研修日程一覧取得用SQL */
    var $select_list_by_plan_id_sql = "SELECT * FROM cl_mst_inside_training_schedule WHERE plan_id IN(:plan_id) ORDER BY plan_date, from_time, to_time";

    /* レコード取得用SQL */
//    var $select_rec_sql = "SELECT plan_id, training_id, plan_no, plan_date, from_time, to_time, report_division, answer_division, max_people, apply_status, place, remarks, delete_flg, create_date, create_user, update_date, update_user FROM cl_mst_inside_training_schedule WHERE plan_id = :plan_id";
    var $select_rec_sql = "SELECT * FROM cl_mst_inside_training_schedule WHERE plan_id = :plan_id";

    /* 登録用カラムタイプ */
    var $insert_col_type = array('text','text','text','integer','integer','integer','integer','integer','text','text','text','integer','integer','integer','integer','text','text','integer','text');
    /* 更新用カラムタイプ */
    var $update_col_type = array('text','text','integer','integer','integer','integer','integer','text','text','text','integer','integer','integer','integer','text','text','integer','text','text');

    /* 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');
    /* 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('integer','text','text');

    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_mst_inside_training_schedule_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["create_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $plan_id
     */
    function physical_delete($id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["plan_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $plan_id
     */
    function logical_delete($id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flg"]=1;
        $param["plan_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * list
     * @param type $plan_id
     */
    function getList($plan_ids){

        $this->log->info("getList() select_list_by_plan_id_sql START  plan_id = ". $plan_ids);
    	//$plan_id_array = $plan_ids.split(",");

    	$plan_id_array = split(",", $plan_ids);


		$param=array();
		$ph_str="";
		$type=array();
		for($i=0;$i<count($plan_id_array);$i++){
    		$param["key".$i]=$plan_id_array[$i];
    		$ph_str = $ph_str.":key".$i.",";
    		$type[]="text";

    	}
        $this->log->debug('count($param)        :'.count($param),__FILE__,__LINE__);
        $this->log->debug('count($type)         :'.count($type),__FILE__,__LINE__);
        $this->log->debug('count($plan_id_array):'.count($plan_id_array),__FILE__,__LINE__);
        $this->log->debug('$ph_str              :'.$ph_str,__FILE__,__LINE__);

        $ph_str=rtrim($ph_str,",");


		/* 研修日程一覧取得用SQL */
		$select_list_by_plan_id_sql = "SELECT plan_id, training_id, training_time, final_flg, plan_date, from_time, to_time FROM cl_mst_inside_training_schedule WHERE plan_id IN($ph_str) ORDER BY plan_date, from_time, to_time";

        $this->log->debug('$select_list_by_plan_id_sql:'.$select_list_by_plan_id_sql,__FILE__,__LINE__);

        //$data=parent::select($this->select_list_by_plan_id_sql,array("text"),array("plan_id"=>$plan_id),MDB2_FETCHMODE_ASSOC);
        $data=parent::select($select_list_by_plan_id_sql,$type,$param,MDB2_FETCHMODE_ASSOC);
        $this->log->debug("getList() select_list_by_plan_id_sql END　　data count = ". count($data));
        return $data;

     }

    /**
     * select
     * @param type $plan_id
     */
    function select($id){

        $this->log->info("select() START");
        $data=parent::select($this->select_rec_sql,array("text"),array("plan_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }


    /**
     * list
     * @param type $plan_id
     */
    function getList_from_training_id($id){

        $this->log->info("getList() select_list_by_training_id_sql START  training_id = ". $id);
    	//$plan_id_array = $plan_ids.split(",");

    	$training_id_array = split(",", $id);


		$param=array();
		$ph_str="";
		$type=array();
		for($i=0;$i<count($training_id_array);$i++){
    		$param["key".$i]=$training_id_array[$i];
    		$ph_str = $ph_str.":key".$i.",";
    		$type[]="text";

    	}
        $this->log->debug('count($param)        :'.count($param),__FILE__,__LINE__);
        $this->log->debug('count($type)         :'.count($type),__FILE__,__LINE__);
        $this->log->debug('count($training_id_array):'.count($training_id_array),__FILE__,__LINE__);
        $this->log->debug('$ph_str              :'.$ph_str,__FILE__,__LINE__);

        $ph_str=rtrim($ph_str,",");


	    /* 研修日程一覧取得用SQL */
	    $select_list_by_training_id_sql = "SELECT plan_id, training_id, plan_date, from_time, to_time FROM cl_mst_inside_training_schedule WHERE training_id IN($ph_str) ORDER BY plan_date, from_time, to_time";

        $this->log->debug('$select_list_by_training_id_sql:'.$select_list_by_training_id_sql,__FILE__,__LINE__);

        //$data=parent::select($this->select_list_by_plan_id_sql,array("text"),array("plan_id"=>$plan_id),MDB2_FETCHMODE_ASSOC);
        $data=parent::select($select_list_by_training_id_sql,$type,$param,MDB2_FETCHMODE_ASSOC);
        $this->log->debug("getList_trainingid() select_list_by_training_id_sql END　　data count = ". count($data));
        return $data;

     }

}
