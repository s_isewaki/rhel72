<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_mst_inside_training_theme_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
    var $insert_sql = "INSERT INTO cl_mst_inside_training_theme ( theme_id, training_id, theme_division, theme_contents, theme_method, theme_deadline, theme_present, delete_flg, create_date, create_user ) VALUES ( :theme_id, :training_id, :theme_division, :theme_contents, :theme_method, :theme_deadline, :theme_present, :delete_flg, current_timestamp, :create_user )";
    /* 更新用SQL */
    var $update_sql = "UPDATE cl_mst_inside_training_theme SET training_id = :training_id, theme_division = :theme_division, theme_contents = :theme_contents, theme_method = :theme_method, theme_deadline = :theme_deadline, theme_present = :theme_present, delete_flg = :delete_flg, update_date = current_timestamp, update_user = :update_user WHERE theme_id = :theme_id";
    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_mst_inside_training_theme WHERE theme_id = :theme_id";
    /* 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_mst_inside_training_theme SET delete_flg = :delete_flg, update_date = current_timestamp, update_user = update_user  WHERE theme_id = :theme_id";
    /* 一覧取得用SQL */
    var $select_list_sql = "SELECT theme_id, training_id, theme_division, theme_contents, theme_method, theme_deadline, theme_present, delete_flg, create_date, create_user, update_date, update_user FROM cl_mst_inside_training_theme ORDER BY create_date";
    /* レコード取得用SQL */
    var $select_rec_sql = "SELECT theme_id, training_id, theme_division, theme_contents, theme_method, theme_deadline, theme_present, delete_flg, create_date, create_user, update_date, update_user FROM cl_mst_inside_training_theme WHERE theme_id = :theme_id";
    /* レコード取得用SQL(研修ID指定) */
    var $select_from_training_id_sql = "SELECT theme_id, training_id, theme_division, theme_contents, theme_method, theme_deadline, theme_present, delete_flg, create_date, create_user, update_date, update_user FROM cl_mst_inside_training_theme WHERE training_id = :training_id order by theme_division";
    /* 研修講座照会用SQL */
    var $select_training_view_sql = "SELECT theme.training_id, theme.theme_id, theme.theme_division, theme.theme_contents, theme.theme_method, theme.theme_deadline, theme.theme_present, theme.delete_flg, theme.create_date, theme.create_user, theme.update_date, theme.update_user, emp.emp_lt_nm, emp.emp_ft_nm FROM cl_mst_inside_training_theme theme left join empmst emp on theme.theme_present = emp.emp_id WHERE theme.training_id = :training_id AND theme.theme_division = :theme_division AND coalesce(theme.delete_flg,0) = 0";
    /* 承認者(課題提出先)取得用SQL */
    var $select_apvemp_theme_present_sql = "SELECT theme.theme_present ,emp.emp_lt_nm ,emp.emp_ft_nm ,st.st_nm FROM cl_mst_inside_training_theme theme INNER JOIN empmst emp ON theme.theme_present = emp.emp_id LEFT JOIN stmst st ON emp.emp_st = st.st_id WHERE theme.theme_id = :theme_id";

    /* 登録用カラムタイプ */
    var $insert_col_type = array('text','text','integer','text','text','text','text','integer','text');
    /* 更新用カラムタイプ */
    var $update_col_type = array('text','integer','text','text','text','text','integer','text','text');

    /* 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');
    /* 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('integer','text','text','text');
    /* 研修講座照会用SQL */
    var $select_training_view_col_type = array('text','integer');

    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_mst_inside_training_theme_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["create_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $theme_id
     */
    function physical_delete($id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["theme_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $theme_id
     */
    function logical_delete($id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flg"]=1;
        $param["theme_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * list
     * @param type $theme_id
     */
    function getList(){

        $this->log->info("select() START");
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->info("select() END");
        return $data;

     }

    /**
     * select
     * @param type $theme_id
     */
    function select($id){

        $this->log->info("select() START");
        $data=parent::select($this->select_rec_sql,array("text"),array("theme_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->info("select() END");
        return $data[0];

     }

    /**
     * 指定された研修IDの研修課題の一覧を取得します。
     *
     * @param $training_id 研修ID
     */
    function select_from_training_id($training_id)
    {

        $this->log->info("select_from_training_id() START training_id=".$training_id);
        $data = parent::select( $this->select_from_training_id_sql, array("text"), array("training_id"=>$training_id), MDB2_FETCHMODE_ASSOC);
        $this->log->info("select_from_training_id() END");
        return $data;

     }

    /**
     * select_training_view
     * @param type $param
     */
    function select_training_view($param){

        $this->log->info("select() START");
        $data=parent::select($this->select_training_view_sql,$this->select_training_view_col_type,$param,MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

    }

    /**
     * select_apvemp_theme_present
     * @param type $id
     */
    function select_apvemp_theme_present($id){

        $this->log->debug("select_apvemp_theme_present() START");
        //$data=parent::select($this->select_apvemp_theme_present_sql,array("text"),array("theme_id"=>$id),$param,MDB2_FETCHMODE_ASSOC);
        $data=parent::select($this->select_apvemp_theme_present_sql,array("text"),array("theme_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select_apvemp_theme_present() END");
        return $data[0];

    }

}
