<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_mst_inside_training_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    // 2012/07/25 Yamagawa upd(s)
    ///* 登録用SQL */
    //var $insert_sql = "INSERT INTO cl_mst_inside_training ( training_id, training_name, training_purpose, target_level1, target_level2, target_level3, target_level4, target_level5, max_training_time, training_require, time_division, training_opener, training_teacher1, training_teacher2, training_teacher3, training_teacher4, training_teacher5, training_slogan, training_contents, training_action, training_support, abolition_flag, delete_flg, create_date, create_user) VALUES ( :training_id, :training_name, :training_purpose, :target_level1, :target_level2, :target_level3, :target_level4, :target_level5, :max_training_time, :training_require, :time_division, :training_opener, :training_teacher1, :training_teacher2, :training_teacher3, :training_teacher4, :training_teacher5, :training_slogan, :training_contents, :training_action, :training_support, :abolition_flag, :delete_flg, current_timestamp, :create_user)";
    ///* 更新用SQL */
    //var $update_sql = "UPDATE cl_mst_inside_training SET training_name = :training_name, training_purpose = :training_purpose, target_level1 = :target_level1, target_level2 = :target_level2, target_level3 = :target_level3, target_level4 = :target_level4, target_level5 = :target_level5, max_training_time = :max_training_time, training_require = :training_require, time_division = :time_division, training_opener = :training_opener, training_teacher1 = :training_teacher1, training_teacher2 = :training_teacher2, training_teacher3 = :training_teacher3, training_teacher4 = :training_teacher4, training_teacher5 = :training_teacher5, training_slogan = :training_slogan, training_contents = :training_contents, training_action = :training_action, training_support = :training_support, abolition_flag = :abolition_flag, delete_flg = :delete_flg, update_date = current_timestamp, update_user = :update_user WHERE training_id = :training_id";
	/* 登録用SQL */
    // 2012/11/19 Yamagawa upd(s)
    //var $insert_sql = "INSERT INTO cl_mst_inside_training ( training_id, training_name, training_purpose, target_level1, target_level2, target_level3, target_level4, target_level5, max_training_time, training_require, time_division, training_opener, training_teacher1, training_teacher2, training_teacher3, training_teacher4, training_teacher5, training_slogan, training_contents, training_action, training_support, abolition_flag, delete_flg, create_date, create_user, auto_control_flg) VALUES ( :training_id, :training_name, :training_purpose, :target_level1, :target_level2, :target_level3, :target_level4, :target_level5, :max_training_time, :training_require, :time_division, :training_opener, :training_teacher1, :training_teacher2, :training_teacher3, :training_teacher4, :training_teacher5, :training_slogan, :training_contents, :training_action, :training_support, :abolition_flag, :delete_flg, current_timestamp, :create_user, :auto_control_flg)";
    var $insert_sql = "INSERT INTO cl_mst_inside_training ( training_id, training_name, training_purpose, target_level1, target_level2, target_level3, target_level4, target_level5, max_training_time, training_require, time_division, training_opener, training_teacher1, training_teacher2, training_teacher3, training_teacher4, training_teacher5, training_slogan, training_contents, training_action, training_support, abolition_flag, delete_flg, create_date, create_user, auto_control_flg, term_div) VALUES ( :training_id, :training_name, :training_purpose, :target_level1, :target_level2, :target_level3, :target_level4, :target_level5, :max_training_time, :training_require, :time_division, :training_opener, :training_teacher1, :training_teacher2, :training_teacher3, :training_teacher4, :training_teacher5, :training_slogan, :training_contents, :training_action, :training_support, :abolition_flag, :delete_flg, current_timestamp, :create_user, :auto_control_flg, :term_div)";
    // 2012/11/19 Yamagawa upd(e)
    /* 更新用SQL */
    // 2012/11/19 Yamagawa upd(s)
    //var $update_sql = "UPDATE cl_mst_inside_training SET training_name = :training_name, training_purpose = :training_purpose, target_level1 = :target_level1, target_level2 = :target_level2, target_level3 = :target_level3, target_level4 = :target_level4, target_level5 = :target_level5, max_training_time = :max_training_time, training_require = :training_require, time_division = :time_division, training_opener = :training_opener, training_teacher1 = :training_teacher1, training_teacher2 = :training_teacher2, training_teacher3 = :training_teacher3, training_teacher4 = :training_teacher4, training_teacher5 = :training_teacher5, training_slogan = :training_slogan, training_contents = :training_contents, training_action = :training_action, training_support = :training_support, abolition_flag = :abolition_flag, delete_flg = :delete_flg, update_date = current_timestamp, update_user = :update_user, auto_control_flg = :auto_control_flg WHERE training_id = :training_id";
    var $update_sql = "UPDATE cl_mst_inside_training SET training_name = :training_name, training_purpose = :training_purpose, target_level1 = :target_level1, target_level2 = :target_level2, target_level3 = :target_level3, target_level4 = :target_level4, target_level5 = :target_level5, max_training_time = :max_training_time, training_require = :training_require, time_division = :time_division, training_opener = :training_opener, training_teacher1 = :training_teacher1, training_teacher2 = :training_teacher2, training_teacher3 = :training_teacher3, training_teacher4 = :training_teacher4, training_teacher5 = :training_teacher5, training_slogan = :training_slogan, training_contents = :training_contents, training_action = :training_action, training_support = :training_support, abolition_flag = :abolition_flag, delete_flg = :delete_flg, update_date = current_timestamp, update_user = :update_user, auto_control_flg = :auto_control_flg, term_div = :term_div WHERE training_id = :training_id";
    // 2012/11/19 Yamagawa upd(e)
    // 2012/07/25 Yamagawa upd(e)
    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_mst_inside_training WHERE training_id = :training_id";
    /* 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_mst_inside_training SET delete_flg = :delete_flg, update_date = current_timestamp, update_user = :update_user  WHERE training_id = :training_id";
	// 2012/07/25 Yamagawa upd(s)
    ///* 一覧取得用SQL */
    //var $select_list_sql = "SELECT training_id, training_name, training_purpose, target_level1, target_level2, target_level3, target_level4, target_level5, max_training_time, training_require, time_division, training_opener, training_teacher1, training_teacher2, training_teacher3, training_teacher4, training_teacher5, training_slogan, training_contents, training_action, training_support, abolition_flag, delete_flg, create_date, create_user, update_date, update_user FROM cl_mst_inside_training ORDER BY create_date";
    ///* レコード取得用SQL */
    //var $select_rec_sql = "SELECT training_id, training_name, training_purpose, target_level1, target_level2, target_level3, target_level4, target_level5,max_training_time, training_require, time_division, training_opener, training_teacher1, training_teacher2, training_teacher3, training_teacher4, training_teacher5, training_slogan, training_contents, training_action, training_support, abolition_flag, delete_flg, create_date, create_user, update_date, update_user FROM cl_mst_inside_training WHERE training_id = :training_id";
    /* 一覧取得用SQL */
    var $select_list_sql = "SELECT training_id, training_name, training_purpose, target_level1, target_level2, target_level3, target_level4, target_level5, max_training_time, training_require, time_division, training_opener, training_teacher1, training_teacher2, training_teacher3, training_teacher4, training_teacher5, training_slogan, training_contents, training_action, training_support, abolition_flag, delete_flg, create_date, create_user, update_date, update_user, auto_control_flg FROM cl_mst_inside_training ORDER BY create_date";
    /* レコード取得用SQL */
    // 2012/11/20 Yamagawa upd(s)
    //var $select_rec_sql = "SELECT training_id, training_name, training_purpose, target_level1, target_level2, target_level3, target_level4, target_level5,max_training_time, training_require, time_division, training_opener, training_teacher1, training_teacher2, training_teacher3, training_teacher4, training_teacher5, training_slogan, training_contents, training_action, training_support, abolition_flag, delete_flg, create_date, create_user, update_date, update_user, auto_control_flg FROM cl_mst_inside_training WHERE training_id = :training_id";
    var $select_rec_sql = "SELECT training_id, training_name, training_purpose, target_level1, target_level2, target_level3, target_level4, target_level5,max_training_time, training_require, time_division, training_opener, training_teacher1, training_teacher2, training_teacher3, training_teacher4, training_teacher5, training_slogan, training_contents, training_action, training_support, abolition_flag, delete_flg, create_date, create_user, update_date, update_user, auto_control_flg, term_div FROM cl_mst_inside_training WHERE training_id = :training_id";
    // 2012/11/20 Yamagawa upd(e)
    // 2012/07/25 Yamagawa upd(e)
	// 2012/08/23 Yamagawa del(s)
    ///* 研修講座一覧取得用SQL */
    //var $select_management_list_sql = "select tra.training_id ,tra.training_name ,case tra.target_level1 when 1 then '必須' when 2 then '任意' else null end as target_level1 ,case tra.target_level2 when 1 then '必須' when 2 then '任意' else null end as target_level2 ,case tra.target_level3 when 1 then '必須' when 2 then '任意' else null end as target_level3 ,case tra.target_level4 when 1 then '必須' when 2 then '任意' else null end as target_level4 ,case tra.target_level5 when 1 then '必須' when 2 then '任意' else null end as target_level5 ,emp.emp_lt_nm ,emp.emp_ft_nm from cl_mst_inside_training tra left join empmst emp on tra.training_opener = emp.emp_id where coalesce(tra.delete_flg,0) = 0 ORDER BY tra.training_id ";
    // 2012/08/23 Yamagawa del(e)
	// 2012/07/25 Yamagawa upd(s)
    ///* 研修講座照会用SQL */
    //var $select_training_view_sql = "SELECT tra.training_id, tra.training_name, tra.training_purpose, tra.target_level1, tra.target_level2, tra.target_level3, tra.target_level4, tra.target_level5, tra.max_training_time, tra.training_require, tra.time_division, tra.training_opener, tra.training_teacher1, tra.training_teacher2, tra.training_teacher3, tra.training_teacher4, tra.training_teacher5, tra.training_slogan, tra.training_contents, tra.training_action, tra.training_support, tra.abolition_flag, tra.delete_flg, tra.create_date, tra.create_user, tra.update_date, tra.update_user, opener.emp_lt_nm as opener_lt_nm, opener.emp_ft_nm as opener_ft_nm, teacher1.emp_lt_nm as teacher1_lt_nm, teacher1.emp_ft_nm as teacher1_ft_nm, teacher2.emp_lt_nm as teacher2_lt_nm, teacher2.emp_ft_nm as teacher2_ft_nm, teacher3.emp_lt_nm as teacher3_lt_nm, teacher3.emp_ft_nm as teacher3_ft_nm, teacher4.emp_lt_nm as teacher4_lt_nm, teacher4.emp_ft_nm as teacher4_ft_nm, teacher5.emp_lt_nm as teacher5_lt_nm, teacher5.emp_ft_nm as teacher5_ft_nm FROM cl_mst_inside_training tra left join empmst opener on tra.training_opener = opener.emp_id left join empmst teacher1 on tra.training_teacher1 = teacher1.emp_id left join empmst teacher2 on tra.training_teacher2 = teacher2.emp_id left join empmst teacher3 on tra.training_teacher3 = teacher3.emp_id left join empmst teacher4 on tra.training_teacher4 = teacher4.emp_id left join empmst teacher5 on tra.training_teacher5 = teacher5.emp_id WHERE tra.training_id = :training_id";
    /* 研修講座照会用SQL */
    // 2012/11/19 Yamagawa upd(s)
    //var $select_training_view_sql = "SELECT tra.training_id, tra.training_name, tra.training_purpose, tra.target_level1, tra.target_level2, tra.target_level3, tra.target_level4, tra.target_level5, tra.max_training_time, tra.training_require, tra.time_division, tra.training_opener, tra.training_teacher1, tra.training_teacher2, tra.training_teacher3, tra.training_teacher4, tra.training_teacher5, tra.training_slogan, tra.training_contents, tra.training_action, tra.training_support, tra.abolition_flag, tra.delete_flg, tra.create_date, tra.create_user, tra.update_date, tra.update_user, opener.emp_lt_nm as opener_lt_nm, opener.emp_ft_nm as opener_ft_nm, teacher1.emp_lt_nm as teacher1_lt_nm, teacher1.emp_ft_nm as teacher1_ft_nm, teacher2.emp_lt_nm as teacher2_lt_nm, teacher2.emp_ft_nm as teacher2_ft_nm, teacher3.emp_lt_nm as teacher3_lt_nm, teacher3.emp_ft_nm as teacher3_ft_nm, teacher4.emp_lt_nm as teacher4_lt_nm, teacher4.emp_ft_nm as teacher4_ft_nm, teacher5.emp_lt_nm as teacher5_lt_nm, teacher5.emp_ft_nm as teacher5_ft_nm, tra.auto_control_flg FROM cl_mst_inside_training tra left join empmst opener on tra.training_opener = opener.emp_id left join empmst teacher1 on tra.training_teacher1 = teacher1.emp_id left join empmst teacher2 on tra.training_teacher2 = teacher2.emp_id left join empmst teacher3 on tra.training_teacher3 = teacher3.emp_id left join empmst teacher4 on tra.training_teacher4 = teacher4.emp_id left join empmst teacher5 on tra.training_teacher5 = teacher5.emp_id WHERE tra.training_id = :training_id";
    var $select_training_view_sql = "SELECT tra.training_id, tra.training_name, tra.training_purpose, tra.target_level1, tra.target_level2, tra.target_level3, tra.target_level4, tra.target_level5, tra.max_training_time, tra.training_require, tra.time_division, tra.training_opener, tra.training_teacher1, tra.training_teacher2, tra.training_teacher3, tra.training_teacher4, tra.training_teacher5, tra.training_slogan, tra.training_contents, tra.training_action, tra.training_support, tra.abolition_flag, tra.delete_flg, tra.create_date, tra.create_user, tra.update_date, tra.update_user, opener.emp_lt_nm as opener_lt_nm, opener.emp_ft_nm as opener_ft_nm, teacher1.emp_lt_nm as teacher1_lt_nm, teacher1.emp_ft_nm as teacher1_ft_nm, teacher2.emp_lt_nm as teacher2_lt_nm, teacher2.emp_ft_nm as teacher2_ft_nm, teacher3.emp_lt_nm as teacher3_lt_nm, teacher3.emp_ft_nm as teacher3_ft_nm, teacher4.emp_lt_nm as teacher4_lt_nm, teacher4.emp_ft_nm as teacher4_ft_nm, teacher5.emp_lt_nm as teacher5_lt_nm, teacher5.emp_ft_nm as teacher5_ft_nm, tra.auto_control_flg, tra.term_div FROM cl_mst_inside_training tra left join empmst opener on tra.training_opener = opener.emp_id left join empmst teacher1 on tra.training_teacher1 = teacher1.emp_id left join empmst teacher2 on tra.training_teacher2 = teacher2.emp_id left join empmst teacher3 on tra.training_teacher3 = teacher3.emp_id left join empmst teacher4 on tra.training_teacher4 = teacher4.emp_id left join empmst teacher5 on tra.training_teacher5 = teacher5.emp_id WHERE tra.training_id = :training_id";
    // 2012/11/19 Yamagawa upd(e)
    // 2012/07/25 Yamagawa upd(e)
    /* 承認者(主催者)取得用SQL */
    var $select_apvemp_opener_sql = "SELECT tra.training_opener ,emp.emp_lt_nm ,emp.emp_ft_nm ,st.st_nm FROM cl_mst_inside_training tra INNER JOIN empmst emp ON tra.training_opener = emp.emp_id LEFT JOIN stmst st ON emp.emp_st = st.st_id WHERE tra.training_id = :training_id";
    /* 承認者(講師)取得用SQL */
    var $select_apvemp_teacher_sql = "SELECT tra.training_teacher1 ,emp1.emp_lt_nm AS emp_lt_nm1 ,emp1.emp_ft_nm AS emp_ft_nm1 ,st1.st_nm AS st_nm1 ,tra.training_teacher2 ,emp2.emp_lt_nm AS emp_lt_nm2 ,emp2.emp_ft_nm AS emp_ft_nm2 ,st2.st_nm AS st_nm2 ,tra.training_teacher3 ,emp3.emp_lt_nm AS emp_lt_nm3 ,emp3.emp_ft_nm AS emp_ft_nm3 ,st3.st_nm AS st_nm3 ,tra.training_teacher4 ,emp4.emp_lt_nm AS emp_lt_nm4 ,emp4.emp_ft_nm AS emp_ft_nm4 ,st4.st_nm AS st_nm4 ,tra.training_teacher5 ,emp5.emp_lt_nm AS emp_lt_nm5 ,emp5.emp_ft_nm AS emp_ft_nm5 ,st5.st_nm AS st_nm5 FROM cl_mst_inside_training tra LEFT JOIN empmst emp1 ON tra.training_teacher1 = emp1.emp_id LEFT JOIN stmst st1 ON emp1.emp_st = st1.st_id LEFT JOIN empmst emp2 ON tra.training_teacher2 = emp2.emp_id LEFT JOIN stmst st2 ON emp2.emp_st = st2.st_id LEFT JOIN empmst emp3 ON tra.training_teacher3 = emp3.emp_id LEFT JOIN stmst st3 ON emp3.emp_st = st3.st_id LEFT JOIN empmst emp4 ON tra.training_teacher4 = emp4.emp_id LEFT JOIN stmst st4 ON emp4.emp_st = st4.st_id LEFT JOIN empmst emp5 ON tra.training_teacher5 = emp5.emp_id LEFT JOIN stmst st5 ON emp5.emp_st = st5.st_id WHERE tra.training_id = :training_id";

    // 2012/07/25 Yamagawa upd(s)
    ///* 登録用カラムタイプ */
    //var $insert_col_type = array('text','text','text','integer','integer','integer','integer','integer','integer','text','integer','text','text','text','text','text','text','text','text','text','text','integer','integer','text');
    ///* 更新用カラムタイプ */
    //var $update_col_type = array('text','text','integer','integer','integer','integer','integer','integer','text','integer','text','text','text','text','text','text','text','text','text','text','integer','integer','text','text');
    /* 登録用カラムタイプ */
    // 2012/11/19 Yamagawa upd(s)
    //var $insert_col_type = array('text','text','text','integer','integer','integer','integer','integer','integer','text','integer','text','text','text','text','text','text','text','text','text','text','integer','integer','text','boolean');
    var $insert_col_type = array('text','text','text','integer','integer','integer','integer','integer','integer','text','integer','text','text','text','text','text','text','text','text','text','text','integer','integer','text','boolean','integer');
    // 2012/11/19 Yamagawa upd(e)
    /* 更新用カラムタイプ */
    // 2012/11/19 Yamagawa upd(s)
    //var $update_col_type = array('text','text','integer','integer','integer','integer','integer','integer','text','integer','text','text','text','text','text','text','text','text','text','text','integer','integer','text','boolean','text');
    var $update_col_type = array('text','text','integer','integer','integer','integer','integer','integer','text','integer','text','text','text','text','text','text','text','text','text','text','integer','integer','text','boolean','integer','text');
    // 2012/11/19 Yamagawa upd(e)
    // 2012/07/25 Yamagawa upd(e)

    /* 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');
    /* 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('integer','text','text');

    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_mst_inside_training_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["create_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $training_id
     */
    function physical_delete($id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["training_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $training_id
     */
    function logical_delete($id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flg"]=1;
        $param["training_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * list
     * @param type $training_id
     */
    function getList(){

        $this->log->info("select() START");
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

     }

    /**
     * select
     * @param type $training_id
     */
    function select($id){

        $this->log->info("select() START");
        $data=parent::select($this->select_rec_sql,array("text"),array("training_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }


    /**
     * getManagementList
     */
	function getManagementList(){

		$this->log->info("select() START");
		// 2012/08/23 Yamagawa upd(s)
		//$data=parent::select($this->select_management_list_sql, null, array(),MDB2_FETCHMODE_ASSOC);
		$sql  = "";
		$sql .= " SELECT ";
		$sql .= "  tra.training_id ";
		$sql .= "  ,tra.training_name ";
		// 2012/11/16 Yamagawa upd(s)
		/*
		$sql .= "  ,case  ";
		$sql .= "    when ";
		$sql .= "     extract(month from sch.plan_date) >= 1 ";
		$sql .= "     and extract(month from sch.plan_date) <= 3 ";
		$sql .= "    then ";
		$sql .= "     cast(extract(year from sch.plan_date) - 1 as varchar) || ' - 下期' ";
		$sql .= "    when ";
		$sql .= "     extract(month from sch.plan_date) >= 4 ";
		$sql .= "     and extract(month from sch.plan_date) <= 9 ";
		$sql .= "    then ";
		$sql .= "     cast(extract(year from sch.plan_date) as varchar) || ' - 上期' ";
		$sql .= "    when ";
		$sql .= "     extract(month from sch.plan_date) >= 10 ";
		$sql .= "     and extract(month from sch.plan_date) <= 12 ";
		$sql .= "    then ";
		$sql .= "     cast(extract(year from sch.plan_date) as varchar) || ' - 下期' ";
		$sql .= "   end as year ";
		*/
		$sql .= "  ,case tra.term_div when 1 then '上期' when 2 then '下期' end as term_div ";
		// 2012/11/16 Yamagawa upd(e)
		$sql .= "  ,sch.plan_date ";
		$sql .= "  ,case tra.target_level1 ";
		$sql .= "    when 1 then '必須' ";
		$sql .= "    when 2 then '任意' ";
		$sql .= "    else null ";
		$sql .= "   end as target_level1 ";
		$sql .= "  ,case tra.target_level2 ";
		$sql .= "    when 1 then '必須' ";
		$sql .= "    when 2 then '任意' ";
		$sql .= "    else null ";
		$sql .= "   end as target_level2 ";
		$sql .= "  ,case tra.target_level3 ";
		$sql .= "    when 1 then '必須' ";
		$sql .= "    when 2 then '任意' ";
		$sql .= "    else null ";
		$sql .= "   end as target_level3 ";
		$sql .= "  ,case tra.target_level4 ";
		$sql .= "    when 1 then '必須' ";
		$sql .= "    when 2 then '任意' ";
		$sql .= "    else null ";
		$sql .= "   end as target_level4 ";
		$sql .= "  ,case tra.target_level5 ";
		$sql .= "    when 1 then '必須' ";
		$sql .= "    when 2 then '任意' ";
		$sql .= "    else null ";
		$sql .= "   end as target_level5 ";
		$sql .= "  ,emp.emp_lt_nm ";
		$sql .= "  ,emp.emp_ft_nm ";
		$sql .= " FROM ";
		$sql .= "  cl_mst_inside_training tra ";
		$sql .= "  left join ";
		$sql .= "   empmst emp ";
		$sql .= "  on ";
		$sql .= "   tra.training_opener = emp.emp_id ";
		$sql .= "  left join ";
		$sql .= "   (select ";
		$sql .= "     training_id ";
		$sql .= "     ,min(plan_date) as plan_date ";
		$sql .= "    from ";
		$sql .= "     cl_mst_inside_training_schedule ";
		$sql .= "    where ";
		$sql .= "     coalesce(delete_flg,0) = 0 ";
		$sql .= "    group by ";
		$sql .= "     training_id) sch ";
		$sql .= "  on ";
		$sql .= "   sch.training_id = tra.training_id ";
		$sql .= " where ";
		$sql .= "  coalesce(tra.delete_flg,0) = 0 ";
		$sql .= " order by ";
		$sql .= "  tra.training_id ";
		$data=parent::select($sql, null, array(),MDB2_FETCHMODE_ASSOC);
		// 2012/08/23 Yamagawa upd(e)
		$this->log->info("select() END");
		return $data;

	}

    /**
     * getManagementList_conditional
     * @param type $param
     */
    function getManagementList_conditional($param){

        $this->log->info("select() START");
        $col_type = array();
        $param_edit = array();
        $sql  = "SELECT ";
        $sql .= "    tra.training_id ";
        $sql .= "    ,tra.training_name ";
        $sql .= "    ,case tra.target_level1 when 1 then '必須' when 2 then '任意' else null end as target_level1 ";
        $sql .= "    ,case tra.target_level2 when 1 then '必須' when 2 then '任意' else null end as target_level2 ";
        $sql .= "    ,case tra.target_level3 when 1 then '必須' when 2 then '任意' else null end as target_level3 ";
        $sql .= "    ,case tra.target_level4 when 1 then '必須' when 2 then '任意' else null end as target_level4 ";
        $sql .= "    ,case tra.target_level5 when 1 then '必須' when 2 then '任意' else null end as target_level5 ";
        $sql .= "    ,opener.emp_lt_nm ";
        $sql .= "    ,opener.emp_ft_nm ";
		// 2012/11/16 Yamagawa upd(s)
		/*
		// 2012/08/24 Yamagawa add(s)
		$sql .= "    ,case  ";
		$sql .= "      when ";
		$sql .= "       extract(month from sch.plan_date) >= 1 ";
		$sql .= "       and extract(month from sch.plan_date) <= 3 ";
		$sql .= "      then ";
		$sql .= "       cast(extract(year from sch.plan_date) - 1 as varchar) || ' - 下期' ";
		$sql .= "      when ";
		$sql .= "       extract(month from sch.plan_date) >= 4 ";
		$sql .= "       and extract(month from sch.plan_date) <= 9 ";
		$sql .= "      then ";
		$sql .= "       cast(extract(year from sch.plan_date) as varchar) || ' - 上期' ";
		$sql .= "      when ";
		$sql .= "       extract(month from sch.plan_date) >= 10 ";
		$sql .= "       and extract(month from sch.plan_date) <= 12 ";
		$sql .= "      then ";
		$sql .= "       cast(extract(year from sch.plan_date) as varchar) || ' - 下期' ";
		$sql .= "     end as year ";
		// 2012/08/24 Yamagawa add(e)
		*/
        $sql .= "    ,case tra.term_div when 1 then '上期' when 2 then '下期' end as term_div ";
        // 2012/11/16 Yamagawa upd(e)
        $sql .= "FROM ";
        $sql .= "    cl_mst_inside_training tra ";
        $sql .= "    left join empmst opener on tra.training_opener = opener.emp_id ";
        $sql .= "    left join empmst teach1 on tra.training_teacher1 = teach1.emp_id ";
        $sql .= "    left join empmst teach2 on tra.training_teacher2 = teach2.emp_id ";
        $sql .= "    left join empmst teach3 on tra.training_teacher3 = teach3.emp_id ";
        $sql .= "    left join empmst teach4 on tra.training_teacher4 = teach4.emp_id ";
        $sql .= "    left join empmst teach5 on tra.training_teacher5 = teach5.emp_id ";
        // 2012/11/16 Yamagawa del(s)
        /*
		// 2012/08/24 Yamagawa add(s)
		$sql .= "    left join ";
		$sql .= "     (select ";
		$sql .= "       training_id ";
		$sql .= "       ,min(plan_date) as plan_date ";
		$sql .= "      from ";
		$sql .= "       cl_mst_inside_training_schedule ";
		$sql .= "      where ";
		$sql .= "       coalesce(delete_flg,0) = 0 ";
		$sql .= "      group by ";
		$sql .= "       training_id) sch ";
		$sql .= "    on ";
		$sql .= "     sch.training_id = tra.training_id ";
		// 2012/08/24 Yamagawa add(e)
		*/
        // 2012/11/16 Yamagawa del(e)
        $sql .= "WHERE ";
        $sql .= "    coalesce(tra.delete_flg,0) = 0 ";
        if ($param['training_id'] != "") {
            $sql .= "    and tra.training_id like '%' || :training_id || '%' ";
            array_push($col_type,'text');
            $param_edit['training_id'] = $param['training_id'];
        }
        if ($param['training_name'] != "") {
            $sql .= "    and tra.training_name like '%' || :training_name || '%' ";
            array_push($col_type,'text');
            $param_edit['training_name'] = $param['training_name'];
        }
        switch ($param['level']) {
            case 1:
                $sql .= "    and tra.target_level1 = 1 ";
                break;
            case 2:
                $sql .= "    and tra.target_level2 = 1 ";
                break;
            case 3:
                $sql .= "    and tra.target_level3 = 1 ";
                break;
            case 4:
                $sql .= "    and tra.target_level4 = 1 ";
                break;
            case 5:
                $sql .= "    and tra.target_level5 = 1 ";
                break;
            default:
                break;
        }
        if ($param['teacher_name'] != "") {
            $sql .= "    and ( ";
            $sql .= "           teach1.emp_lt_nm || teach1.emp_ft_nm like '%' || :teacher_name || '%' ";
            $sql .= "        or teach1.emp_kn_lt_nm || teach1.emp_kn_ft_nm like '%' || :teacher_name || '%' ";
            $sql .= "        or teach2.emp_lt_nm || teach2.emp_ft_nm like '%' || :teacher_name || '%' ";
            $sql .= "        or teach2.emp_kn_lt_nm || teach2.emp_kn_ft_nm like '%' || :teacher_name || '%' ";
            $sql .= "        or teach3.emp_lt_nm || teach3.emp_ft_nm like '%' || :teacher_name || '%' ";
            $sql .= "        or teach3.emp_kn_lt_nm || teach3.emp_kn_ft_nm like '%' || :teacher_name || '%' ";
            $sql .= "        or teach4.emp_lt_nm || teach4.emp_ft_nm like '%' || :teacher_name || '%' ";
            $sql .= "        or teach4.emp_kn_lt_nm || teach4.emp_kn_ft_nm like '%' || :teacher_name || '%' ";
            $sql .= "        or teach5.emp_lt_nm || teach5.emp_ft_nm like '%' || :teacher_name || '%' ";
            $sql .= "        or teach5.emp_kn_lt_nm || teach5.emp_kn_ft_nm like '%' || :teacher_name || '%' ";
            $sql .= "    ) ";
            array_push($col_type,'text');
            $param_edit['teacher_name'] = $param['teacher_name'];
        }

        if ($param['date_from'] != "" || $param['date_to'] != "") {
            $sql .= "    and exists( ";
            $sql .= "     select sch.plan_id from cl_mst_inside_training_schedule sch where tra.training_id = sch.training_id and coalesce(sch.delete_flg,0) = 0 ";
            if ($param['date_from'] != "" && $param['date_to'] != "") {
                $sql .= "    and sch.plan_date between :date_from and :date_to ";
            } else if ($param['date_from'] != "" && $param['date_to'] == "") {
                $sql .= "    and sch.plan_date >= :date_from ";
            } else if ($param['date_from'] == "" && $param['date_to'] != "") {
                $sql .= "    and sch.plan_date <= :date_to ";
            }
            $sql .= "    ) ";
        }

        if ($param['date_from'] != ""){
            array_push($col_type,'text');
            $param_edit['date_from'] = $param['date_from'];
        }
        if($param['date_to'] != "") {
            array_push($col_type,'text');
            $param_edit['date_to'] = $param['date_to'];
        }
		// 2012/11/16 Yamagawa add(s)
		if ($param['term_div'] != "") {
			$sql .= "    and tra.term_div = :term_div ";
			array_push($col_type,'integer');
			$param_edit['term_div'] = $param['term_div'];
		}
		// 2012/11/16 Yamagawa add(e)

        $sql .= "ORDER BY ";
        $sql .= " tra.training_id ";

        $this->log->debug('training_id：'.$param['training_id']);
        $this->log->debug('training_name：'.$param['training_name']);
        $this->log->debug('level：'.$param['level']);
        $this->log->debug('teacher_name：'.$param['teacher_name']);
        $this->log->debug('date_from：'.$param['date_from']);
        $this->log->debug('date_to：'.$param['date_to']);
        // 2012/11/16 Yamagawa add(s)
        $this->log->debug('term_div：'.$param['term_div']);
        // 2012/11/16 Yamagawa add(e)
        $this->log->debug($sql);
        $data=parent::select($sql,$col_type,$param_edit,MDB2_FETCHMODE_ASSOC);
        $this->log->info("select() END");
        return $data;

    }

    /**
     * select_training_view
     * @param type $id
     */
    function select_training_view($id){

        $this->log->info("select() START");
        $data=parent::select($this->select_training_view_sql,array("text"),array("training_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

    }

    /**
     * select_apvemp_opener
     * @param type $id
     */
    function select_apvemp_opener($id){

        $this->log->debug("select_apvemp_opener() START");
        $data=parent::select($this->select_apvemp_opener_sql,array("text"),array("training_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select_apvemp_opener() END");
        return $data[0];

    }

    /**
     * select_apvemp_teacher
     * @param type $id
     */
    function select_apvemp_teacher($id){

        $this->log->debug("select_apvemp_teacher() START");
        $data=parent::select($this->select_apvemp_teacher_sql,array("text"),array("training_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select_apvemp_teacher() END");
        return $data[0];

    }

}
