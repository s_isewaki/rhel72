<?php
/**
 * 看護副部長画面の一覧データ取得用モデル
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_mst_nurse_deputy_manager_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
    var $insert_sql = "INSERT INTO cl_mst_nurse_deputy_manager ( nurse_deputy_manager_id ,class_division ,class_id ,atrb_id ,dept_id ,room_id ,emp_id ,create_date ,create_user ) VALUES ( :nurse_deputy_manager_id ,:class_division ,:class_id ,:atrb_id ,:dept_id ,:room_id ,:emp_id ,current_timestamp ,:create_user )";
    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_mst_nurse_deputy_manager";

    /* 登録用カラムタイプ */
    var $insert_col_type = array('text' ,'integer' ,'integer' ,'integer' ,'integer' ,'integer' ,'text' ,'text');

    /**
     * Constractor
     */
    function cl_mst_nurse_deputy_manager_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $st_id
     */
    function insert($param) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param['create_user'] = $this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * physical_delete
     */
    function physical_delete() {

        $this->log->info("physical_delete() START");

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,null,null);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

}
