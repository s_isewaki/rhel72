<?php

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_mst_nurse_education_committee_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /** ��Ͽ��SQL */
    var $insert_sql = "INSERT INTO cl_mst_nurse_education_committee ( emp_id ,create_date ,create_user ) VALUES ( :emp_id ,current_timestamp ,:create_user )";
    /** ʪ�������SQL */
    var $physical_delete_sql = "DELETE FROM cl_mst_nurse_education_committee";
    /** ����������SQL */
    var $select_list_sql = "SELECT edu.emp_id ,emp.emp_lt_nm ,emp.emp_ft_nm ,edu.create_date ,edu.create_user ,edu.update_date ,edu.update_user FROM cl_mst_nurse_education_committee edu INNER JOIN empmst emp ON edu.emp_id = emp.emp_id";

    /** ��Ͽ�ѥ���ॿ���� */
    var $insert_col_type = array('text' ,'text');

    /**
     * Constractor
     */
    function cl_mst_nurse_education_committee_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $emp_id
     */
    function insert($emp_id) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param = array(
        	'emp_id'		=> $emp_id,
        	'create_user'	=> $this->user
        );

        /**
         * �¹�
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * physical_delete
     */
    function physical_delete() {

        $this->log->info("physical_delete() START");

        /**
         * �¹�
         */
        $rtn=parent::execute($this->physical_delete_sql,null,null);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * getList
     */
    function getList(){

        $this->log->info("select() START");
        $data=parent::select($this->select_list_sql,null,null,MDB2_FETCHMODE_ASSOC);
        $this->log->info("select() END");
        return $data;

     }

	/**
	 * �Ǹ��Ѱ�������å�
	 */
	function check_education_committee($emp_id){

		$this->log->info("check_education_committee() START count(data):".count($data));

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " emp_id ";
		$sql .= "FROM ";
		$sql .= " cl_mst_nurse_education_committee ";
		$sql .= "WHERE ";
		$sql .= " emp_id = :emp_id ";

		$data=parent::select($sql,array('text'),array('emp_id'=>$emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->info("check_education_committee() END count(data):".count($data));
		return $data;
	}
}
