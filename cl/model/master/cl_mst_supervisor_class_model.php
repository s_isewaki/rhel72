<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_mst_supervisor_class_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
    var $insert_sql = "INSERT INTO cl_mst_supervisor_class ( class_division, create_date, create_user ) VALUES ( :class_division, current_timestamp, :create_user )";
    /* 更新用SQL（１レコードしか作成されない為、WHERE句無し） */
    var $update_sql = "UPDATE cl_mst_supervisor_class SET class_division = :class_division, update_date = current_timestamp, update_user = :update_user";
	/* 物理削除用SQL（１レコードしか作成されない為、WHERE句無し） */
    var $physical_delete_sql = "DELETE FROM cl_mst_supervisor_class";
    /* 取得用SQL */
    var $select_sql = "SELECT class_division, create_date, create_user, update_date, update_user FROM cl_mst_supervisor_class";

    /**
     * Constractor
     */
    function cl_mst_supervisor_class_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $class_division
     */
    function insert($class_division) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param = array(
        	'class_division'	=> $class_division,
        	'create_user'		=> $this->user
        );

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,array('integer','text'),$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $class_division
     */
    function update($class_division) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param = array(
        	'class_division'	=> $class_division,
        	'update_user'		=> $this->user
        );

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,array('integer','text'),$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     */
    function physical_delete() {

        $this->log->info("physical_delete() START");

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,null,null);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * select
     */
    function select(){

        $this->log->info("select() START");
        $data=parent::select($this->select_sql,null,null,MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }

}
