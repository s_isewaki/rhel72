<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_mst_supervisor_st_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
    var $insert_sql = "INSERT INTO cl_mst_supervisor_st ( st_id, create_date, create_user ) VALUES ( :st_id, current_timestamp, :create_user )";
    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_mst_supervisor_st";

    /**
     * Constractor
     */
    function cl_mst_supervisor_st_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $st_id
     */
    function insert($st_id) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param = array(
        	'st_id'			=> $st_id,
        	'create_user'	=> $this->user
        );

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,array('integer','text'),$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * physical_delete
     */
    function physical_delete() {

        $this->log->info("physical_delete() START");

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,null,null);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

	/**
	 * 所属長チェック
	 */
	function check_supervisor($emp_id){

		$this->log->info("check_supervisor() START count(data):".count($data));

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " st.st_id ";
		$sql .= "FROM ";
		$sql .= " empmst emp ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_mst_supervisor_st st ";
		$sql .= " ON ";
		$sql .= "  emp.emp_st = st.st_id ";
		$sql .= "WHERE ";
		$sql .= " emp.emp_id = :emp_id ";
		$sql .= "UNION ALL ";
		$sql .= "SELECT ";
		$sql .= " st.st_id ";
		$sql .= "FROM ";
		$sql .= " empmst emp ";
		$sql .= " INNER JOIN ";
		$sql .= "  concurrent con ";
		$sql .= " ON ";
		$sql .= "  emp.emp_id = con.emp_id ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_mst_supervisor_st st ";
		$sql .= " ON ";
		$sql .= "  con.emp_st = st.st_id ";
		$sql .= "WHERE ";
		$sql .= " emp.emp_id = :emp_id ";

		$data=parent::select($sql,array('text'),array('emp_id'=>$emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->info("check_supervisor() END count(data):".count($data));
		return $data;
	}

}
