<?php
//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

//==================================
//ラダー評価マスタ⇒マスタ種類model
//==================================
class cl_mst_ludder_eval_model extends cl_common_model{

	var $con;
	var $log;
	var $user;

	/**
	 * Constractor
	*/
	function cl_mst_ludder_eval_model($p_con,$p_login_user) {

		parent::cl_common_model($p_con,$p_login_user);
		$this->con = $p_con;
		$this->user=$p_login_user;
		$this->log = new cl_common_log_class(basename(__FILE__,'.php'));
	}

	/**
	 * マスタ種類取得
	 */
	function select_mstKind($level){

		$this->log->info("select_mstKind() START count(data):".count($data));

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " cl_mst_evaluation ";
		$sql .= "WHERE ";
		$sql .= " level = :level ";
		$sql .= " and delete_flg = 0 ";
		$sql .= "ORDER BY ";
		$sql .= " disp_order ";

		$data=parent::select($sql,array('text'),array('level'=>$level),MDB2_FETCHMODE_ASSOC);

		$this->log->info("select_mstKind() END count(data):".count($data));
		return $data;
	}

	/**
	 * カテゴリ情報取得（Grid）
	 */
	function select_mst_cate($level,$mst_id){

		$this->log->info("select_mst_cate() START count(data):".count($data));

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " cl_mst_evaluation_category ";
		$sql .= "WHERE ";
		$sql .= " 1 = 1 ";
		$sql .= " and level = :level ";
		$sql .= " and evaluation_id = :evaluation_id ";
		$sql .= " and delete_flg = 0 ";
		$sql .= "ORDER BY ";
		$sql .= " disp_order ";

		//Parameter
		$param = array(
			'level' 		=> $level,
			'evaluation_id'	=> $mst_id
		);

		$data=parent::select($sql,array('integer','text'),$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("select_mst_cate() END count(data):".count($data));
		return $data;
	}


	/**
	 * 評価基準取得（Grid）
	 */
	function select_mst_grp($level,$mst_id,$cate_id){

		$this->log->info("select_mst_grp() START count(data):".count($data));

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= "  cl_mst_evaluation_group ";
		$sql .= "WHERE ";
		$sql .= " 1 = 1 ";
		$sql .= " and level = :level ";
		$sql .= " and evaluation_id = :evaluation_id ";
		$sql .= " and category_id = :category_id ";
		$sql .= " and delete_flg = 0 ";
		$sql .= "ORDER BY ";
		$sql .= " disp_order ";

		//Parameter
		$param = array(
			'level' 			=> $level,
			'evaluation_id'		=> $mst_id,
			'category_id'		=> $cate_id
		);

		$data=parent::select($sql,array('text','text','text'),$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("select_mst_grp() END count(data):".count($data));
		return $data;
	}

	/**
	 * 評価項目取得（Grid）
	 */
	function select_mst_item($level,$mst_id,$cate_id,$grp_id){

		$this->log->info("select_mst_item() START count(data):".count($data));

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= "  cl_mst_evaluation_item ";
		$sql .= "WHERE ";
		$sql .= " 1 = 1 ";
		$sql .= " and level = :level ";
		$sql .= " and evaluation_id = :evaluation_id ";
		$sql .= " and category_id = :category_id ";
		$sql .= " and group_id = :group_id ";
		$sql .= " and delete_flg = 0 ";
		$sql .= "ORDER BY ";
		$sql .= " disp_order ";

		//Parameter
		$param = array(
			'level' 			=> $level,
			'evaluation_id'		=> $mst_id,
			'category_id'		=> $cate_id,
			'group_id'			=> $grp_id
		);

		$data=parent::select($sql,array('text','text','text','text'),$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("select_mst_item() END count(data):".count($data));
		return $data;
	}

	/**
	 * テンプレート情報取得(コンボ)
	 */
	function select_get_tpl($level){

		$this->log->info("select_get_tpl() START count(data):".count($data));

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " cl_mst_ladder_wkfw_link ";
		$sql .= "WHERE ";
		$sql .= " 1 = 1 ";
		$sql .= " and delete_flg = 0 ";
		$sql .= " and level = :level ";

		$data=parent::select($sql,array('integer'),array('level'=> $level,),MDB2_FETCHMODE_ASSOC);

		$this->log->info("select_get_tpl() END count(data):".count($data));
		return $data;
	}

	/**
	 * マスタ種類情報取得（Cmb）
	 */
	// 2012/06/08 Yamagawa upd(s)
	//function select_get_evaluation(){
	function select_get_evaluation($level){
	// 2012/06/08 Yamagawa upd(e)

		$this->log->info("select_get_evaluation() START count(data):".count($data));

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " cl_mst_evaluation ";
		$sql .= "WHERE ";
		$sql .= " 1 = 1 ";
		// 2012/06/08 Yamagawa add(s)
		$sql .= " and level = :level ";
		// 2012/06/08 Yamagawa add(e)
		$sql .= " and delete_flg = 0 ";
		$sql .= "ORDER BY ";
		$sql .= " disp_order ";

		// 2012/06/08 Yamagawa upd(s)
		//$data=parent::select($sql,null,array(),MDB2_FETCHMODE_ASSOC);
		$data=parent::select($sql,array('integer'),array('level' => $level),MDB2_FETCHMODE_ASSOC);
		// 2012/06/08 Yamagawa upd(e)

		$this->log->info("select_get_evaluation() END count(data):".count($data));
		return $data;
	}

	/**
	 * カテゴリ情報取得（Cmb）
	 */
	function select_get_category($level,$mst_id){

		$this->log->info("select_get_category() START count(data):".count($data));
		//Sql
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " * ";
		$sql .= "FROM ";
		$sql .= " cl_mst_evaluation_category ";
		$sql .= "WHERE ";
		$sql .= " 1 = 1 ";
		$sql .= " and level = :level ";
		$sql .= " and evaluation_id = :evaluation_id ";
		$sql .= " and delete_flg = 0 ";
		$sql .= "ORDER BY ";
		$sql .= " disp_order ";

		//Parameter
		$param = array(
			'level' 		=> $level,
			'evaluation_id'	=> $mst_id
		);

		$data=parent::select($sql,array('integer','text'),$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("select_get_category() END count(data):".count($data));
		return $data;
	}
}
