
<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_mst_inside_training_schedule_model extends cl_common_model{


    var $con;
    var $log;
    var $user;

    /**
     * Constractor コンストラクタ
     */
    function cl_mst_inside_training_schedule_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert 登録
     * @param type $param
     * パラメータの例：
     *     $param('plan_id'=>plan_id_value,'training_id'=>training_id_value,'plan_no'=>plan_no_value,'year'=>year_value,'term_div'=>term_div_value,'plan_date'=>plan_date_value,'from_time'=>from_time_value,'to_time'=>to_time_value,'report_division'=>report_division_value,'answer_division'=>answer_division_value,'max_people'=>max_people_value,'apply_status'=>apply_status_value,'place'=>place_value,'remarks'=>remarks_value,'delete_flg'=>delete_flg_value,'create_user'=>create_user_value,'update_date'=>update_date_value,'update_user'=>update_user_value);
     */
    function insert($param) {

        $this->log->info(__FUNCTION__." START");

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["create_user"]=$this->user;
        $param["update_date"]=null;
        $param["update_user"]=null;

        /* SQL */
        $sql = "INSERT INTO cl_mst_inside_training_schedule ( plan_id,first_plan_id,training_id,plan_no,year,term_div,training_time,final_flg,plan_date,from_time,to_time,report_division,answer_division,max_people,apply_status,place,remarks,delete_flg,create_date,create_user,update_date,update_user ) VALUES ( :plan_id,:first_plan_id,:training_id,:plan_no,:year,:term_div,:training_time,:final_flg,:plan_date,:from_time,:to_time,:report_division,:answer_division,:max_people,:apply_status,:place,:remarks,:delete_flg,current_timestamp,:create_user,:update_date,:update_user )";

        /* カラムタイプ */
        $type = array('text','text','text','integer','integer','integer','integer','integer','date','time','time','integer','integer','integer','integer','text','text','integer','text','timestamp','text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }

    /**
     * update 更新
     * @param type $param
     * パラメータの例：
     *     $param('training_id'=>training_id_value,'plan_no'=>plan_no_value,'year'=>year_value,'term_div'=>term_div_value,'plan_date'=>plan_date_value,'from_time'=>from_time_value,'to_time'=>to_time_value,'report_division'=>report_division_value,'answer_division'=>answer_division_value,'max_people'=>max_people_value,'apply_status'=>apply_status_value,'place'=>place_value,'remarks'=>remarks_value,'delete_flg'=>delete_flg_value,'update_user'=>update_user_value);
     */
    function update($p_param) {

        $this->log->info(__FUNCTION__." START");


        //$param=$this->getRecord($p_param["plan_id"]);

        /**
         * Ｉ／Ｆされたパラメータを設定する
         */
        /*
        foreach( $p_param as $key => $value ){
        	if($p_param[$key]!=null && $p_param[$key]!=""){
        		$param[$key]=$p_param[$key];
        	}
        }
        unset($param['create_date']);
        unset($param['create_user']);
        */

        /**
         * Parameter
         */
        //$param["delete_flg"]=0;
        //$param["update_user"]=$this->user;
        $p_param["delete_flg"]=0;
        $p_param["update_user"]=$this->user;

        /* SQL */
        //$sql = "UPDATE cl_mst_inside_training_schedule SET training_id = :training_id ,plan_no = :plan_no ,year = :year ,term_div = :term_div ,plan_date = :plan_date ,from_time = :from_time ,to_time = :to_time ,report_division = :report_division ,answer_division = :answer_division ,max_people = :max_people ,apply_status = :apply_status ,place = :place ,remarks = :remarks ,delete_flg = :delete_flg ,update_date = current_timestamp ,update_user = :update_user  WHERE plan_id = :plan_id";
        $sql = "UPDATE cl_mst_inside_training_schedule SET first_plan_id = :first_plan_id, training_id = :training_id, plan_no = :plan_no, year = :year, term_div = :term_div, training_time = :training_time, final_flg = :final_flg, plan_date = CAST( :plan_date AS DATE), from_time = CAST( :from_time AS TIME), to_time = CAST( :to_time AS TIME), report_division = :report_division, answer_division = :answer_division, max_people = :max_people, apply_status = :apply_status, place = :place, remarks = :remarks, delete_flg = :delete_flg, update_date = current_timestamp, update_user = :update_user WHERE plan_id = :plan_id";

        /* カラムタイプ */
        //$type = array('text' , 'integer' , 'integer' , 'integer' , 'date' , 'time' , 'time' , 'integer' , 'integer' , 'integer' , 'integer' , 'text' , 'text' , 'integer' , 'text' , 'text');
        $type = array('text','text','integer','integer','integer','integer','integer','text','text','text','integer','integer','integer','integer','text','text','integer','text','text');

        /**
         * 実行
         */
        //$rtn=parent::execute($sql,$type,$param);
        $rtn=parent::execute($sql,$type,$p_param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }
    /**
     * physical_delete 物理削除
     * @param type $plan_id プライマリーキー
     */
    function physical_delete($plan_id) {

        $this->log->info(__FUNCTION__." START");

        /**
         * Parameter
         */
        $param=array();
        $param["plan_id"]=$plan_id;

        /* SQL */
        $sql = "DELETE FROM cl_mst_inside_training_schedule WHERE plan_id = :plan_id";

        /* カラムタイプ */
        $type = array('text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }

    /**
     * logical_delete 論理削除
     * @param type $plan_id プライマリーキー
     */
    function logical_delete($plan_id) {

        $this->log->info(__FUNCTION__." START");

        /**
         * Parameter
         */
        $param=array();
        $param["plan_id"]=$plan_id;
        $param["update_user"]=$this->user;

        /* SQL */
        $sql = "UPDATE cl_mst_inside_training_schedule SET delete_flg = 1 , update_date = current_timestamp , update_user = :update_user WHERE plan_id = :plan_id";

        /* カラムタイプ */
        $type = array('text','text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }

    /**
     * getList 全件取得
     * @param type $plan_id
     */
    function getList(){

        $this->log->info(__FUNCTION__." START");

        /* SQL */
        $sql = "SELECT * FROM cl_mst_inside_training_schedule ORDER BY plan_id";

        /* カラムタイプ */
        $type = null;

        $data=parent::select($sql,$type,null,MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data;

     }

    /**
     * getRecord レコード取得（プライマリーキー指定）
     * @param type $plan_id プライマリーキー
     */
    function getRecord($plan_id){

        $this->log->info(__FUNCTION__." START");

        /* レコード取得用SQL */
        $sql = "SELECT * FROM cl_mst_inside_training_schedule WHERE plan_id = :plan_id";

        /* カラムタイプ */
        //$type = array("text");

        //$data=parent::select($sql,$type,array("plan_id"=>$plan_id),MDB2_FETCHMODE_ASSOC);
        $data=$this->getRecordBySql($sql,$plan_id);

        $this->log->info(__FUNCTION__." END");

        return $data;

     }

    /**
     * getRecord 任意SQLレコード取得（プライマリーキー指定）
     * @param type $plan_id プライマリーキー
     */
    function getRecordBySql($sql,$plan_id){

        $this->log->info(__FUNCTION__." START");

        /* レコード取得用SQL */
        //$sql = "{/literal}{$rec_sql}{literal}";

        /* カラムタイプ */
        $type = array("text");

        $data=parent::select($sql,$type,array("plan_id"=>$plan_id),MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data[0];

     }

	/**
     * update_for_training_id 更新(研修単位一括)
     * @param type $p_param
	 */
	function update_for_training_id($p_param){

        $this->log->info(__FUNCTION__." START");

        /**
         * Parameter
         */
        $param["apply_status"] = $p_param["apply_status"];
        $param["remarks"]      = $p_param["remarks"];
        $param["update_user"]  = $this->user;
        $param["training_id"]  = $p_param["training_id"];
        // 2012/11/27 Yamagawa add(s)
        $param["year"]         = $p_param["year"];
        // 2012/11/27 Yamagawa add(e)

        /* SQL */
        // 2012/11/27 Yamagawa upd(s)
        //$sql = "UPDATE cl_mst_inside_training_schedule SET apply_status = :apply_status, remarks = :remarks, update_date = current_timestamp, update_user = :update_user WHERE training_id = :training_id AND COALESCE(delete_flg,0) = 0";
        $sql = "UPDATE cl_mst_inside_training_schedule SET apply_status = :apply_status, remarks = :remarks, update_date = current_timestamp, update_user = :update_user WHERE training_id = :training_id AND year = :year AND COALESCE(delete_flg,0) = 0";
        // 2012/11/27 Yamagawa upd(e)

        /* カラムタイプ */
        // 2012/11/27 Yamagawa upd(s)
        //$type = array('integer','text','text','text');
        $type = array('integer','text','text','text','integer');
        // 2012/11/27 Yamagawa upd(e)

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
	}
}
