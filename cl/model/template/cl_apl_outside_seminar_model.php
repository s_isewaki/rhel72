<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_apl_outside_seminar_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
    var $insert_sql = "INSERT INTO cl_apl_outside_seminar VALUES ( :seminar_apply_id, :seminar_id, :apply_id, :emp_id, :seminar_name, :report_date, :from_date, :to_date, :from_time, :to_time, :open_place, :teacher_name, :trip_division, :participation_division, :expense_division, :report_division, :delete_flg, current_timestamp, :create_user, :update_date, :update_user )";




    /* 更新用SQL */
    //var $update_sql = "UPDATE cl_apl_outside_seminar SET seminar_apply_id = :seminar_apply_id, seminar_id = :seminar_id, apply_id = :apply_id, emp_id = :emp_id, seminar_name = :seminar_name, report_date = :report_date, from_date = :from_date, to_date = :to_date, from_time = :from_time, to_time = :to_time, open_place = :open_place, teacher_name = :teacher_name, trip_division = :trip_division, participation_division = :participation_division, expense_division = :expense_division, report_division = :report_division, delete_flg = :delete_flg, update_date = current_timestamp, update_user = :update_user WHERE apply_id = :apply_id";
    var $update_sql = "UPDATE cl_apl_outside_seminar SET seminar_name = :seminar_name, report_date = :report_date, from_date = :from_date, to_date = :to_date, from_time = :from_time, to_time = :to_time, open_place = :open_place, teacher_name = :teacher_name, trip_division = :trip_division, participation_division = :participation_division, expense_division = :expense_division, report_division = :report_division, delete_flg = :delete_flg, update_date = current_timestamp, update_user = :update_user WHERE apply_id = :apply_id";

    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_apl_outside_seminar WHERE seminar_apply_id = :seminar_apply_id";
    /* 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_apl_outside_seminar SET delete_flg = :delete_flg, update_date = current_timestamp, update_user = :update_user  WHERE apply_id = :apply_id";
    /* 一覧取得用SQL */
    var $select_list_sql = "SELECT seminar_apply_id, seminar_id, apply_id, emp_id, seminar_name, report_date, from_date, to_date, from_time, to_time, open_place, teacher_name, trip_division, participation_division, expense_division, report_division, delete_flg, create_date, create_user, update_date, update_user FROM cl_apl_outside_seminar ORDER BY create_date";

    /* レコード取得用SQL */
  //var $select_rec_sql = "SELECT seminar_apply_id, seminar_id, apply_id, emp_id, seminar_name, report_date, from_date, to_date, from_time, to_time, open_place, teacher_name, trip_division, participation_division, expense_division, report_division, delete_flg, create_date, create_user, update_date, update_user FROM cl_apl_outside_seminar WHERE seminar_apply_id = :seminar_apply_id";
    var $select_rec_sql = "SELECT * FROM cl_apl_outside_seminar WHERE apply_id = :apply_id";

    /* 報告日更新用SQL */
    var $report_date_regist_sql = "UPDATE cl_apl_outside_seminar SET report_date = :report_date ,update_date = current_timestamp ,update_user = :login_user_id WHERE seminar_apply_id = :seminar_apply_id";
    /* 報告日削除用SQL */
    var $report_date_delete_sql = "UPDATE cl_apl_outside_seminar SET report_date = NULL ,update_date = current_timestamp ,update_user = :login_user_id WHERE seminar_apply_id = :seminar_apply_id";


    /* 登録用カラムタイプ */
    var $insert_col_type = array('text','text','text','text','text','date','date','date','time','time','text','text','integer','integer','integer','integer','integer','text','timestamp','text',);

	/* 承認者情報（院外研修承認時使用） */
    var $st_div_select_sql = "SELECT apply_id, emp_id, st_div FROM cl_applyapv WHERE apply_id = :apply_id";


    /* 更新用カラムタイプ */
   //var $update_col_type = array('text','text','text','text','date','date','date','time','time','text','text','integer','integer','integer','integer','integer','text','text');
    var $update_col_type = array('text','date','date','date','time','time','text','text','integer','integer','integer','integer','integer','text','text');

    /* 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');

    /* 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('integer','text','text');

    /* 報告日更新用カラムタイプ */
    var $report_date_regist_col_type = array('date','text','text');
    /* 報告日削除用カラムタイプ */
    var $report_date_delete_col_type = array('text','text');

    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_apl_outside_seminar_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["create_user"]=$this->user;
        $param["update_user"]=null;
        $param["update_date"]=null;
        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $seminar_apply_id
     */
    function physical_delete($id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["seminar_apply_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $seminar_apply_id
     */
    function logical_delete($id) {

        $this->log->info("logical_delete() START");

        //var $logical_delete_sql = "UPDATE cl_apl_outside_seminar SET delete_flg = :delete_flg, update_date = current_timestamp, update_user = :update_user  WHERE apply_id = :apply_id";
    	//var $logical_delete_col_type = array('integer','text','text');

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flg"]=1;
        $param["apply_id"]=$id;
        $param["update_user"]=$this->user;


        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * list
     * @param type $seminar_apply_id
     */
    function getList(){

        $this->log->info("select() START");
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

     }

    /**
     * select
     * @param type $seminar_apply_id
     */
    function select($id){

        $this->log->info("select() START");
        $data=parent::select($this->select_rec_sql,array("text"),array("apply_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }

    /**
     * report_date_regist
     * @param type $param
     */
	function report_date_regist($param){

		$this->log->debug("report_date_regist() START");
		$rtn=parent::execute($this->report_date_regist_sql,$this->report_date_regist_col_type,$param);
		$this->log->debug("report_date_regist() END");
		return $rtn;

	}

    /**
     * report_date_delete
     * @param type $param
     */
	function report_date_delete($param){

		$this->log->debug("report_date_delete() START");
		$rtn=parent::execute($this->report_date_delete_sql,$this->report_date_delete_col_type,$param);
		$this->log->debug("report_date_delete() END");
		return $rtn;

	}


    /**
     * select　院外研修　費用負担権限確認用
     * @param type $apply_id
     */
    function select_apv_st($apply_id){

        $this->log->info("select() START");
        $data=parent::select($this->st_div_select_sql,array("text"),array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

     }


}
