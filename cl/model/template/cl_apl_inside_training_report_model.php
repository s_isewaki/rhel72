<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_apl_inside_training_report_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
    var $insert_sql = "INSERT INTO cl_apl_inside_training_report ( emp_id, training_report_id, apply_id, training_apply_id, training_id, plan_id, training_learned, boss_comment, chief_comment, sponsor_comment, teacher_comment, boss_id, chief_id, sponsor_id, teacher_id, delete_flg, create_date, create_user) VALUES ( :emp_id, :training_report_id, :apply_id, :training_apply_id, :training_id, :plan_id, :training_learned, :boss_comment, :chief_comment, :sponsor_comment, :teacher_comment, :boss_id, :teacher_id, :sponsor_id, :chief_id, :delete_flg, current_timestamp, :create_user )";

    /* 更新用SQL */
    //var $update_sql = "UPDATE cl_apl_inside_training_report SET emp_id = :emp_id, apply_id = :apply_id, training_apply_id = :training_apply_id, training_id = :training_id, plan_id = :plan_id, training_learned = :training_learned, boss_comment = :boss_comment, chief_comment = :chief_comment, sponsor_comment = :sponsor_comment, teacher_comment = :teacher_comment, boss_id = :boss_id, chief_id = :chief_id, sponsor_id = :sponsor_id, teacher_id = :teacher_id, update_date = current_timestamp, update_user = :update_user WHERE training_report_id = :training_report_id";
    var $update_sql = "UPDATE cl_apl_inside_training_report SET apply_id = :apply_id, training_apply_id = :training_apply_id, training_id = :training_id, plan_id = :plan_id, training_learned = :training_learned, boss_comment = :boss_comment, chief_comment = :chief_comment, sponsor_comment = :sponsor_comment, teacher_comment = :teacher_comment, boss_id = :boss_id, chief_id = :chief_id, sponsor_id = :sponsor_id, teacher_id = :teacher_id, update_date = current_timestamp, update_user = :update_user WHERE training_report_id = :training_report_id";
    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_apl_inside_training_report WHERE training_report_id = :training_report_id";

    /* 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_apl_inside_training_report SET delete_flg = :delete_flg, update_date = current_timestamp, update_user = :update_user  WHERE apply_id = :apply_id";

    /* 一覧取得用SQL */
    var $select_list_sql = "SELECT emp_id, training_report_id, apply_id, training_id, plan_id, training_learned, boss_comment, chief_comment, sponsor_comment, teacher_comment, boss_id, chief_id, sponsor_id, teacher_id, delete_flg, create_date, create_user, update_date, update_user FROM cl_apl_inside_training_report ORDER BY create_date";

    /* レコード取得用SQL */
    //var $select_rec_sql = "SELECT emp_id, training_report_id, apply_id, training_id, plan_id, training_learned, delete_flg, create_date, create_user, update_date, update_user FROM cl_apl_inside_training_report WHERE training_report_id = :training_report_id";
//    var $select_rec_sql = "SELECT * from cl_apl_inside_training_report WHERE apply_id = :apply_id ";
    var $select_rec_sql = "SELECT * from cl_apl_inside_training_report LEFT JOIN cl_mst_inside_training_schedule ON cl_apl_inside_training_report.plan_id = cl_mst_inside_training_schedule.plan_id WHERE apply_id = :apply_id ORDER BY apply_id" ;

    /* 研修申請報告用レコード取得SQL */
//    var $select_report_rec_sql = "SELECT * from cl_apl_inside_training_report WHERE training_apply_id = :training_apply_id AND delete_flg=0 ORDER BY plan_id";
//    var $select_report_rec_sql = "SELECT * from cl_apl_inside_training_report WHERE training_apply_id = :training_apply_id AND delete_flg=0 ORDER BY apply_id";
//    var $select_report_rec_sql = "SELECT report.* , apv.apv_stat,apv.apv_order from cl_apl_inside_training_report AS report LEFT JOIN cl_applyapv AS apv ON report.apply_id = apv.apply_id WHERE training_apply_id = :training_apply_id AND report.delete_flg=0 ORDER BY report.apply_id";
    var $select_report_rec_sql = "SELECT report.* from cl_apl_inside_training_report AS report ,cl_apply AS apl WHERE training_apply_id = :training_apply_id AND  report.apply_id = apl.apply_id AND apl.apply_stat=1";

    var $apv_select_report_rec_sql = "SELECT apv.apply_id, apv.apv_order, apv.emp_id from cl_apl_inside_training_report AS report LEFT JOIN cl_applyapv AS apv ON report.apply_id = apv.apply_id WHERE apv.apply_id = :apply_id AND report.delete_flg=0 ORDER BY report.apply_id";



    /* 登録用カラムタイプ */
    var $insert_col_type = array('text','text','text','text','text','text','text','text','text','text','text','text','text','text','text','integer','text');
    /* 更新用カラムタイプ */
    //var $update_col_type = array('text','text','text','text','text','text','text','text','text','text','text','text','text','text','text','text');
    var $update_col_type = array('text','text','text','text','text','text','text','text','text','text','text','text','text','text','text');

    /* 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');
    /* 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('integer','text','text');

    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_apl_inside_training_report_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["create_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");
		$this->log->debug('$param='.str_replace("\n",",",print_r($param,true)));

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $training_report_id
     */
    function physical_delete($id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["training_report_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $training_report_id
     */
    function logical_delete($id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flg"]=1;
        $param["apply_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * list
     * @param type $training_report_id
     */
    function getList(){

        $this->log->info("select() START");
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

     }

    /**
     * select
     * @param type $training_report_id
     */
    function select($id){

        $this->log->info("select() START");
        //$data=parent::select($this->select_rec_sql,array("text"),array("training_report_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $data=parent::select($this->select_rec_sql,array("text"),array("apply_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }

    /**
     * 研修申請報告用　検索
     * @param type $training_apply_id
     */
    function select_report_rec($id){

        $this->log->info("select_report_rec() START");
        //$data=parent::select($this->select_rec_sql,array("text"),array("training_report_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $data=parent::select($this->select_report_rec_sql,array("text"),array("training_apply_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select_report_rec() END");
        return $data;

     }

    /**
     * 研修申請報告用　承認者ID取得
     * @param type $training_apply_id
     */
    function apv_data_select_report_rec($id){

        $this->log->info("select_report_rec() START");
        $data=parent::select($this->apv_select_report_rec_sql,array("text"),array("apply_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select_report_rec() END");
        return $data;

     }

	/**
	 * 報告中・報告済み研修申請取得
	 * @param type $training_apply_id
	 */
	function reported_apl_inside_training_rec($id){

		$this->log->info("reported_apl_inside_training_rec() START");

		$sql = "";
		$sql .= "SELECT ";
		$sql .= " rep.plan_id ";
		$sql .= "FROM ";
		$sql .= " cl_applied_inside_training applied ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_apl_inside_training_report rep ";
		$sql .= " ON ";
		$sql .= "  rep.training_apply_id = applied.training_apply_id ";
		$sql .= "  AND rep.delete_flg != 1 ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_apply app ";
		$sql .= " ON ";
		$sql .= "  app.apply_id = rep.apply_id ";
		$sql .= "  AND app.apply_stat IN ('0','1') ";
		$sql .= "  AND app.delete_flg = FALSE ";
		$sql .= "  AND app.draft_flg = FALSE ";
		$sql .= "WHERE ";
		$sql .= " applied.training_apply_id = :training_apply_id ";

		$data=parent::select($sql,array("text"),array("training_apply_id"=>$id),MDB2_FETCHMODE_ASSOC);

		$this->log->info("reported_apl_inside_training_rec() END");

		return $data;

	}
}
