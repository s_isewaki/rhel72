
<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_apl_levelup_evaluation_value_model extends cl_common_model{


    var $con;
    var $log;
    var $user;

    /**
     * Constractor コンストラクタ
     */
    function cl_apl_levelup_evaluation_value_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert 登録
     * @param type $param
     * パラメータの例：
     *     $param('levelup_evaluation_value_id'=>levelup_evaluation_value_id_value,'evaluation_emp_division'=>evaluation_emp_division_value,'levelup_evaluation_id'=>levelup_evaluation_id_value,'levelup_evaluation_colleague_id'=>levelup_evaluation_colleague_id_value,'levelup_apply_id'=>levelup_apply_id_value,'levelup_emp_id'=>levelup_emp_id_value,'evaluation_emp_id'=>evaluation_emp_id_value,'evaluation_id'=>evaluation_id_value,'category_id'=>category_id_value,'group_id'=>group_id_value,'item_id'=>item_id_value,'value'=>value_value,'delete_flg'=>delete_flg_value,'create_user'=>create_user_value,'update_date'=>update_date_value,'update_user'=>update_user_value);
     */
    function insert($p_param) {

        $this->log->info(__FUNCTION__." START");

        /**
         * Parameter
         */
        $p_param["delete_flg"]=0;
        $p_param["create_user"]=$this->user;
        $p_param["update_date"]=null;
        $p_param["update_user"]=null;

        /* SQL */
        $sql = "INSERT INTO cl_apl_levelup_evaluation_value ( levelup_evaluation_value_id,evaluation_emp_division,levelup_evaluation_id,levelup_evaluation_colleague_id,levelup_apply_id,levelup_emp_id,evaluation_emp_id,evaluation_id,category_id,group_id,item_id,value,delete_flg,create_date,create_user,update_date,update_user ) VALUES ( :levelup_evaluation_value_id,:evaluation_emp_division,:levelup_evaluation_id,:levelup_evaluation_colleague_id,:levelup_apply_id,:levelup_emp_id,:evaluation_emp_id,:evaluation_id,:category_id,:group_id,:item_id,:value,:delete_flg,current_timestamp,:create_user,:update_date,:update_user )";

        /* カラムタイプ */
        $type = array('text','integer','text','text','text','text','text','text','text','text','text','integer','integer','text','timestamp','text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$p_param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }

    /**
     * update 更新
     * @param type $param
     * パラメータの例：
     *     $param('evaluation_emp_division'=>evaluation_emp_division_value,'levelup_evaluation_id'=>levelup_evaluation_id_value,'levelup_evaluation_colleague_id'=>levelup_evaluation_colleague_id_value,'levelup_apply_id'=>levelup_apply_id_value,'levelup_emp_id'=>levelup_emp_id_value,'evaluation_emp_id'=>evaluation_emp_id_value,'evaluation_id'=>evaluation_id_value,'category_id'=>category_id_value,'group_id'=>group_id_value,'item_id'=>item_id_value,'value'=>value_value,'update_user'=>update_user_value);
     */
    function update($p_param) {

        $this->log->info(__FUNCTION__." START");


        $param=$this->getRecord($p_param["levelup_evaluation_value_id"]);

        /**
         * Ｉ／Ｆされたパラメータを設定する
         */
        foreach( $param as $key => $value ){
        	if($p_param[$key]!=null && $p_param[$key]!=""){
        		$param[$key]=$p_param[$key];
        	}
        }
        unset($param['create_date']);
        unset($param['create_user']);
        unset($param['update_date']);
        unset($param['update_user']);
        unset($param['delete_flg']);

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /* SQL */
        $sql = "UPDATE cl_apl_levelup_evaluation_value SET evaluation_emp_division = :evaluation_emp_division ,levelup_evaluation_id = :levelup_evaluation_id ,levelup_evaluation_colleague_id = :levelup_evaluation_colleague_id ,levelup_apply_id = :levelup_apply_id ,levelup_emp_id = :levelup_emp_id ,evaluation_emp_id = :evaluation_emp_id ,evaluation_id = :evaluation_id ,category_id = :category_id ,group_id = :group_id ,item_id = :item_id ,value = :value ,delete_flg = 0 ,update_date = current_timestamp ,update_user = :update_user  WHERE levelup_evaluation_value_id = :levelup_evaluation_value_id";

        /* カラムタイプ */
        $type = array('integer' , 'text' , 'text' , 'text' , 'text' , 'text' , 'text' , 'text' , 'text' , 'text' , 'integer' , 'text' , 'text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }
    /**
     * physical_delete 物理削除
     * @param type $levelup_evaluation_value_id プライマリーキー
     */
    function physical_delete($levelup_evaluation_value_id) {

        $this->log->info(__FUNCTION__." START");

        /**
         * Parameter
         */
        $param=array();
        $param["levelup_evaluation_value_id"]=$levelup_evaluation_value_id;

        /* SQL */
        $sql = "DELETE FROM cl_apl_levelup_evaluation_value WHERE levelup_evaluation_value_id = :levelup_evaluation_value_id";

        /* カラムタイプ */
        $type = array('text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }

    /**
     * logical_delete 論理削除
     * @param type $levelup_evaluation_value_id プライマリーキー
     */
    function logical_delete($levelup_evaluation_value_id) {

        $this->log->info(__FUNCTION__." START");

        /**
         * Parameter
         */
        $param=array();
        $param["levelup_evaluation_value_id"]=$levelup_evaluation_value_id;
        $param["update_user"]=$this->user;

        /* SQL */
        $sql = "UPDATE cl_apl_levelup_evaluation_value SET delete_flg = 1 , update_date = current_timestamp , update_user = :update_user WHERE levelup_evaluation_value_id = :levelup_evaluation_value_id";

        /* カラムタイプ */
        $type = array('text','text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }

    /**
     * getList 全件取得
     * @param type $plan_id
     */
    function getList(){

        $this->log->info(__FUNCTION__." START");

        /* SQL */
        $sql = "SELECT * FROM cl_apl_levelup_evaluation_value ORDER BY levelup_evaluation_value_id";

        /* カラムタイプ */
        $type = null;

        $data=parent::select($sql,$type,null,MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data;

     }

    /**
     * getRecord レコード取得（プライマリーキー指定）
     * @param type $levelup_evaluation_value_id プライマリーキー
     */
    function getRecord($levelup_evaluation_value_id){

        $this->log->info(__FUNCTION__." START");

        /* レコード取得用SQL */
        $sql = "SELECT * FROM cl_apl_levelup_evaluation_value WHERE levelup_evaluation_value_id = :levelup_evaluation_value_id";

        /* カラムタイプ */
        $type = array("text");

        //$data=$this->getRecordBySql($sql,$type,$levelup_evaluation_value_id,MDB2_FETCHMODE_ASSOC);
		$data=$this->getRecordBySql($sql,$levelup_evaluation_value_id);

        $this->log->info(__FUNCTION__." END");

        return $data;

     }

    /**
     * getRecord 任意SQLレコード取得（プライマリーキー指定）
     * @param type $levelup_evaluation_value_id プライマリーキー
     */
    function getRecordBySql($sql,$levelup_evaluation_value_id){

        $this->log->info(__FUNCTION__." START");

        /* レコード取得用SQL */
        //$sql = "SELECT * FROM cl_apl_levelup_evaluation_value WHERE levelup_evaluation_value_id = :levelup_evaluation_value_id";

        /* カラムタイプ */
        $type = array("text");

        $data=parent::select($sql,$type,array("levelup_evaluation_value_id"=>$levelup_evaluation_value_id),MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data[0];

     }

    /**
     * getRecord 任意SQLレコードリスト取得（パラメータ配列、カラムタイプ配列指定）
     * @param type $sql SQL
     * @param type $p_param パラメータ配列
     * @param type $type カラムタイプ
     */
    function getListBySql($sql,$p_param,$type){

        $this->log->info(__FUNCTION__." START");

        $data=parent::select($sql,$type,$p_param,MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data;

     }
}
