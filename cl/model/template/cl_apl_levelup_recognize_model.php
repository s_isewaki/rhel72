
<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_apl_levelup_recognize_model extends cl_common_model{


    var $con;
    var $log;
    var $user;

    /**
     * Constractor コンストラクタ
     */
    function cl_apl_levelup_recognize_model($p_con,$p_login_user) {
        parent::cl_common_model(&$p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert 登録
     * @param type $param
     * パラメータの例：
     *     $param('levelup_recognize_id'=>levelup_recognize_id_value,'apply_id'=>apply_id_value,'levelup_apply_id'=>levelup_apply_id_value,'levelup_emp_id'=>levelup_emp_id_value,'level'=>level_value,'recognize_emp_id'=>recognize_emp_id_value,'recognize_date'=>recognize_date_value,'recognize_flg'=>recognize_flg_value,'reason'=>reason_value,'total_value'=>total_value_value,'delete_flg'=>delete_flg_value,'create_user'=>create_user_value,'update_date'=>update_date_value,'update_user'=>update_user_value);
     */
    function insert($p_param) {

        $this->log->info(__FUNCTION__." START");

        /**
         * Parameter
         */
        $p_param["delete_flg"]=0;
        $p_param["create_user"]=$this->user;
        $p_param["update_date"]=null;
        $p_param["update_user"]=null;

        /* SQL */
        $sql = "INSERT INTO cl_apl_levelup_recognize ( levelup_recognize_id,apply_id,levelup_apply_id,levelup_emp_id,level,recognize_emp_id,recognize_date,recognize_flg,reason,total_value,delete_flg,create_date,create_user,update_date,update_user ) VALUES ( :levelup_recognize_id,:apply_id,:levelup_apply_id,:levelup_emp_id,:level,:recognize_emp_id,:recognize_date,:recognize_flg,:reason,:total_value,:delete_flg,current_timestamp,:create_user,:update_date,:update_user )";

        /* カラムタイプ */
        $type = array('text','text','text','text','integer','text','date','integer','text','integer','integer','text','timestamp','text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$p_param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }

    /**
     * update 更新
     * @param type $param
     * パラメータの例：
     *     $param('apply_id'=>apply_id_value,'levelup_apply_id'=>levelup_apply_id_value,'levelup_emp_id'=>levelup_emp_id_value,'level'=>level_value,'recognize_emp_id'=>recognize_emp_id_value,'recognize_date'=>recognize_date_value,'recognize_flg'=>recognize_flg_value,'reason'=>reason_value,'total_value'=>total_value_value,'update_user'=>update_user_value);
     */
    function update($p_param) {

        $this->log->info(__FUNCTION__." START");


        $param=$this->getRecord($p_param["levelup_recognize_id"]);

        /**
         * Ｉ／Ｆされたパラメータを設定する
         */
        foreach( $param as $key => $value ){
        	if($p_param[$key]!=null && $p_param[$key]!=""){
        		$param[$key]=$p_param[$key];
        	}
        }
        unset($param['create_date']);
        unset($param['create_user']);
        unset($param['update_date']);
        unset($param['update_user']);
        unset($param['delete_flg']);

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /* SQL */
        $sql = "UPDATE cl_apl_levelup_recognize SET apply_id = :apply_id ,levelup_apply_id = :levelup_apply_id ,levelup_emp_id = :levelup_emp_id ,level = :level ,recognize_emp_id = :recognize_emp_id ,recognize_date = :recognize_date ,recognize_flg = :recognize_flg ,reason = :reason ,total_value = :total_value ,delete_flg = 0 ,update_date = current_timestamp ,update_user = :update_user  WHERE levelup_recognize_id = :levelup_recognize_id";

        /* カラムタイプ */
        $type = array('text' , 'text' , 'text' , 'integer' , 'text' , 'date' , 'integer' , 'text' , 'integer' , 'text' , 'text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }
    /**
     * physical_delete 物理削除
     * @param type $levelup_recognize_id プライマリーキー
     */
    function physical_delete($levelup_recognize_id) {

        $this->log->info(__FUNCTION__." START");

        /**
         * Parameter
         */
        $param=array();
        $param["levelup_recognize_id"]=$levelup_recognize_id;

        /* SQL */
        $sql = "DELETE FROM cl_apl_levelup_recognize WHERE levelup_recognize_id = :levelup_recognize_id";

        /* カラムタイプ */
        $type = array('text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }

    /**
     * logical_delete 論理削除
     * @param type $levelup_recognize_id プライマリーキー
     */
    function logical_delete($levelup_recognize_id) {

        $this->log->info(__FUNCTION__." START");

        /**
         * Parameter
         */
        $param=array();
        $param["levelup_recognize_id"]=$levelup_recognize_id;
        $param["update_user"]=$this->user;

        /* SQL */
        $sql = "UPDATE cl_apl_levelup_recognize SET delete_flg = 1 , update_date = current_timestamp , update_user = :update_user WHERE levelup_recognize_id = :levelup_recognize_id";

        /* カラムタイプ */
        $type = array('text','text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }

    /**
     * getList 全件取得
     * @param type $plan_id
     */
    function getList(){

        $this->log->info(__FUNCTION__." START");

        /* SQL */
        $sql = "SELECT * FROM cl_apl_levelup_recognize ORDER BY levelup_recognize_id";

        /* カラムタイプ */
        $type = null;

        $data=parent::select($sql,$type,null,MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data;

     }

    /**
     * getRecord レコード取得（プライマリーキー指定）
     * @param type $levelup_recognize_id プライマリーキー
     */
    function getRecord($levelup_recognize_id){

        $this->log->info(__FUNCTION__." START");

        /* レコード取得用SQL */
        $sql = "SELECT * FROM cl_apl_levelup_recognize WHERE levelup_recognize_id = :levelup_recognize_id";

        /* カラムタイプ */
        $type = array("text");

        //$data=$this->getRecordBySql($sql,$type,$levelup_recognize_id,MDB2_FETCHMODE_ASSOC);
		$data=$this->getRecordBySql($sql,$levelup_recognize_id);

        $this->log->info(__FUNCTION__." END");

        return $data;

     }

    /**
     * getRecord 任意SQLレコード取得（プライマリーキー指定）
     * @param type $levelup_recognize_id プライマリーキー
     */
    function getRecordBySql($sql,$levelup_recognize_id){

        $this->log->info(__FUNCTION__." START");

        /* レコード取得用SQL */
        //$sql = "SELECT * FROM cl_apl_levelup_recognize WHERE levelup_recognize_id = :levelup_recognize_id";

        /* カラムタイプ */
        $type = array("text");

        $data=parent::select($sql,$type,array("levelup_recognize_id"=>$levelup_recognize_id),MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data[0];

     }

    /**
     * getRecord 任意SQLレコードリスト取得（パラメータ配列、カラムタイプ配列指定）
     * @param type $sql SQL
     * @param type $p_param パラメータ配列
     * @param type $type カラムタイプ
     */
    function getListBySql($sql,$p_param,$type){

        $this->log->info(__FUNCTION__." START");

        $data=parent::select($sql,$type,$p_param,MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data;

     }
}
