<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_mst_evaluation_model extends cl_common_model{

    var $con;
    var $log;
    var $user;
    /**
     * Constractor
     */
    function cl_mst_evaluation_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * cl_apl_levelup_apply_evaluation_item@insert
     * @param type $param
     */
    function insert_evaluation_item($param) {

		$sql  = " INSERT INTO cl_apl_levelup_apply_evaluation_item ";
		$sql .= "		(levelup_apply_item_id ";
		$sql .= "		,levelup_apply_id ";
		$sql .= "		,item_id ";
		$sql .= "		,level ";
		$sql .= "		,evaluation_id ";
		$sql .= "		,category_id ";
		$sql .= "		,group_id ";
		$sql .= "		,item_name ";
		$sql .= "		,disp_order ";
		$sql .= "		,disp_flg ";
		$sql .= "		,must_value ";
		$sql .= "		,eval_disp_1_flg ";
		$sql .= "		,eval_disp_2_flg ";
		$sql .= "		,eval_disp_3_flg ";
		$sql .= "		,eval_disp_4_flg ";
		$sql .= "		,delete_flg ";
		$sql .= "		,create_date ";
		$sql .= "		,create_user";
		$sql .= "		) ";
		$sql .= "	SELECT ";
		$sql .= "		'CLLAI' || LPAD(NEXTVAL('cl_levelup_apply_evaluation_item_id_seq'),8,'0') ";
		$sql .= "		,:levelup_apply_id ";
		$sql .= "		,item.item_id ";
		$sql .= "		,item.level ";
		$sql .= "		,item.evaluation_id ";
		$sql .= "		,item.category_id ";
		$sql .= "		,item.group_id ";
		$sql .= "		,item.item_name ";
		$sql .= "		,item.disp_order ";
		$sql .= "		,item.disp_flg ";
		$sql .= "		,item.must_value ";
		$sql .= "		,item.eval_disp_1_flg ";
		$sql .= "		,item.eval_disp_2_flg ";
		$sql .= "		,item.eval_disp_3_flg ";
		$sql .= "		,item.eval_disp_4_flg ";
		$sql .= "		,item.delete_flg ";
		$sql .= "		,current_timestamp ";
		$sql .= "		,:create_user ";
		//$sql .= "		,current_timestamp ";
		//$sql .= "		,:update_user ";
		$sql .= "	FROM ";
		$sql .= "				cl_mst_ladder_wkfw_link link ";
		$sql .= "			INNER JOIN  ";
		$sql .= "				cl_mst_evaluation evel ";
		$sql .= "			ON ";
		$sql .= "				link.evaluation_short_wkfw_name = evel.evaluation_short_wkfw_name ";
		$sql .= "			INNER JOIN ";
		$sql .= "				cl_mst_evaluation_category cate ";
		$sql .= "			ON ";
		$sql .= "				evel.evaluation_id = cate.evaluation_id ";
		$sql .= "			INNER JOIN ";
		$sql .= "				cl_mst_evaluation_group grop ";
		$sql .= "			ON ";
		$sql .= "				cate.category_id = grop.category_id ";
		$sql .= "			INNER JOIN ";
		$sql .= "				cl_mst_evaluation_item item ";
		$sql .= "			ON ";
		$sql .= "				grop.group_id = item.group_id ";
		$sql .= "	WHERE ";
		$sql .= "			link.apply_short_wkfw_name = :apply_short_wkfw_name ";
		$sql .= "		AND	link.level = :level ";

		//Parameter
		$param = array(
			'levelup_apply_id'			=> $param["levelup_apply_id"],
			'create_user'				=> $this->user,
			'update_user'				=> $this->user,
			'apply_short_wkfw_name'		=> $param["apply_short_wkfw_name"],
			'level'						=> $param["level"]
		);

		/* カラムタイプ */
		$type = array('text','text','text','text','integer');

		$rtn=parent::execute($sql,$type,$param);

    }

    /**
     * cl_apl_levelup_apply_evaluation_group@insert
     * @param type $param
     */
    function insert_evaluation_group($param) {

		$sql  = " INSERT INTO cl_apl_levelup_apply_evaluation_group ";
		$sql .= "		(levelup_apply_group_id ";
		$sql .= "		,levelup_apply_id ";
		$sql .= "		,group_id ";
		$sql .= "		,level ";
		$sql .= "		,evaluation_id ";
		$sql .= "		,category_id ";
		$sql .= "		,group_name ";
		$sql .= "		,group_no ";
		$sql .= "		,disp_order ";
		$sql .= "		,disp_flg ";
		$sql .= "		,delete_flg ";
		$sql .= "		,create_date ";
		$sql .= "		,create_user ";
		//$sql .= "		,update_date ";
		//$sql .= "		,update_user ";
		$sql .= "		) ";
		$sql .= "	SELECT ";
		$sql .= "		'CLLAG' || LPAD(NEXTVAL('cl_levelup_apply_evaluation_group_id_seq'),8,'0') ";
		$sql .= "		,:levelup_apply_id ";
		$sql .= "		,grop.group_id ";
		$sql .= "		,grop.level ";
		$sql .= "		,grop.evaluation_id ";
		$sql .= "		,grop.category_id ";
		$sql .= "		,grop.group_name ";
		$sql .= "		,grop.group_no ";
		$sql .= "		,grop.disp_order ";
		$sql .= "		,grop.disp_flg ";
		$sql .= "		,grop.delete_flg ";
		$sql .= "		,current_timestamp ";
		$sql .= "		,:create_user ";
		//$sql .= "		,current_timestamp ";
		//$sql .= "		,:update_user ";
		$sql .= "	FROM ";
		$sql .= "				cl_mst_ladder_wkfw_link link ";
		$sql .= "			INNER JOIN  ";
		$sql .= "				cl_mst_evaluation evel ";
		$sql .= "			ON ";
		$sql .= "				link.evaluation_short_wkfw_name = evel.evaluation_short_wkfw_name ";
		$sql .= "			INNER JOIN ";
		$sql .= "				cl_mst_evaluation_category cate ";
		$sql .= "			ON ";
		$sql .= "				evel.evaluation_id = cate.evaluation_id ";
		$sql .= "			INNER JOIN ";
		$sql .= "				cl_mst_evaluation_group grop ";
		$sql .= "			ON ";
		$sql .= "				cate.category_id = grop.category_id ";
		$sql .= "	WHERE ";
		$sql .= "			link.apply_short_wkfw_name = :apply_short_wkfw_name ";
		$sql .= "		AND	link.level = :level ";

		//Parameter
		$param = array(
			'levelup_apply_id'			=> $param["levelup_apply_id"],
			'create_user'				=> $this->user,
			'update_user'				=> $this->user,
			'apply_short_wkfw_name'		=> $param["apply_short_wkfw_name"],
			'level'						=> $param["level"]
		);

		/* カラムタイプ */
		$type = array('text','text','text','text','integer');

		$rtn=parent::execute($sql,$type,$param);

	}

    /**
     * cl_apl_levelup_apply_evaluation_category@insert
     * @param type $param
     */
    function insert_evaluation_category($param) {

		$sql  = " INSERT INTO cl_apl_levelup_apply_evaluation_category ";
		$sql .= "		(levelup_apply_category_id ";
		$sql .= "		,levelup_apply_id ";
		$sql .= "		,category_id ";
		$sql .= "		,level ";
		$sql .= "		,evaluation_id ";
		$sql .= "		,category_name ";
		$sql .= "		,disp_order ";
		$sql .= "		,disp_flg ";
		$sql .= "		,delete_flg ";
		$sql .= "		,create_date ";
		$sql .= "		,create_user ";
		//$sql .= "		,update_date ";
		//$sql .= "		,update_user ";
		$sql .= "		) ";
		$sql .= "	SELECT ";
		$sql .= "		'CLLAC' || LPAD(NEXTVAL('cl_levelup_apply_evaluation_category_id_seq'),8,'0') ";
		$sql .= "		,:levelup_apply_id ";
		$sql .= "		,cate.category_id ";
		$sql .= "		,cate.level ";
		$sql .= "		,cate.evaluation_id ";
		$sql .= "		,cate.category_name ";
		$sql .= "		,cate.disp_order ";
		$sql .= "		,cate.disp_flg ";
		$sql .= "		,cate.delete_flg ";
		$sql .= "		,current_timestamp ";
		$sql .= "		,:create_user ";
		//$sql .= "		,current_timestamp ";
		//$sql .= "		,:update_user ";
		$sql .= "	FROM ";
		$sql .= "				cl_mst_ladder_wkfw_link link ";
		$sql .= "			INNER JOIN  ";
		$sql .= "				cl_mst_evaluation evel ";
		$sql .= "			ON ";
		$sql .= "				link.evaluation_short_wkfw_name = evel.evaluation_short_wkfw_name ";
		$sql .= "			INNER JOIN ";
		$sql .= "				cl_mst_evaluation_category cate ";
		$sql .= "			ON ";
		$sql .= "				evel.evaluation_id = cate.evaluation_id ";
		$sql .= "	WHERE ";
		$sql .= "			link.apply_short_wkfw_name = :apply_short_wkfw_name ";
		$sql .= "		AND	link.level = :level ";

		//Parameter
		$param = array(
			'levelup_apply_id'			=> $param["levelup_apply_id"],
			'create_user'				=> $this->user,
			'update_user'				=> $this->user,
			'apply_short_wkfw_name'		=> $param["apply_short_wkfw_name"],
			'level'						=> $param["level"]
		);

		/* カラムタイプ */
		$type = array('text','text','text','text','integer');

		$rtn=parent::execute($sql,$type,$param);
	}

    /**
     * insert_evaluation@insert
     * @param type $param
     */
    function insert_evaluation($param) {

		$sql  = " INSERT INTO cl_apl_levelup_apply_evaluation ";
		$sql .= "		(levelup_apply_evaluation_id ";
		$sql .= "		,levelup_apply_id ";
		$sql .= "		,evaluation_id ";
		$sql .= "		,level ";
		$sql .= "		,evaluation_name ";
		$sql .= "		,passing_mark ";
		$sql .= "		,disp_order ";
		$sql .= "		,evaluation_short_wkfw_name ";
		$sql .= "		,delete_flg ";
		$sql .= "		,create_date ";
		$sql .= "		,create_user ";
		//$sql .= "		,update_date ";
		//$sql .= "		,update_user ";
		$sql .= "		) ";
		$sql .= "	SELECT ";
		$sql .= "		'CLLAE' || LPAD(NEXTVAL('cl_levelup_apply_evaluation_id_seq'),8,'0') ";
		$sql .= "		,:levelup_apply_id ";
		$sql .= "		,evel.evaluation_id ";
		$sql .= "		,evel.level ";
		$sql .= "		,evel.evaluation_name ";
		$sql .= "		,evel.passing_mark ";
		$sql .= "		,evel.disp_order ";
		$sql .= "		,evel.evaluation_short_wkfw_name ";
		$sql .= "		,evel.delete_flg ";
		$sql .= "		,current_timestamp ";
		$sql .= "		,:create_user ";
		//$sql .= "		,current_timestamp ";
		//$sql .= "		,:update_user ";
		$sql .= "	FROM ";
		$sql .= "				cl_mst_ladder_wkfw_link link ";
		$sql .= "			INNER JOIN  ";
		$sql .= "				cl_mst_evaluation evel ";
		$sql .= "			ON ";
		$sql .= "				link.evaluation_short_wkfw_name = evel.evaluation_short_wkfw_name ";
		$sql .= "	WHERE ";
		$sql .= "			link.apply_short_wkfw_name = :apply_short_wkfw_name ";
		$sql .= "		AND	link.level = :level ";

		//Parameter
		$param = array(
			'levelup_apply_id'			=> $param["levelup_apply_id"],
			'create_user'				=> $this->user,
			'update_user'				=> $this->user,
			'apply_short_wkfw_name'		=> $param["apply_short_wkfw_name"],
			'level'						=> $param["level"]
		);

		/* カラムタイプ */
		$type = array('text','text','text','text','integer');

		$rtn=parent::execute($sql,$type,$param);
	}

    /**
     * delete_evaluation
     * @param type $levelup_apply_id
     */
    function delete_evaluation($levelup_apply_id) {

        $this->log->info("delete_evaluation() START");

        /* 論理削除用SQL */
		$sql  = "";
		$sql .= "	UPDATE cl_apl_levelup_apply_evaluation SET ";
		$sql .= "			delete_flg = :delete_flg , ";
		$sql .= "			update_date = current_timestamp , ";
		$sql .= "			update_user = :update_user ";
		$sql .= "	WHERE ";
		$sql .= "			levelup_apply_id = :levelup_apply_id ";

        /**
         * Parameter
         */
        $param=array();
		//2012/05/07 K.Fujii upd(s)
        //$param["delete_flg"]=true;
		$param["delete_flg"]=1;
		//2012/05/07 K.Fujii upd(s)
        $param["update_user"]=$this->user;
        $param["levelup_apply_id"]=$levelup_apply_id;

		/* カラムタイプ */
		//2012/05/07 K.Fujii upd(s)
		//$type = array('boolean','text','text');
		$type = array('integer','text','text');
		//2012/05/07 K.Fujii upd(e)
        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info("delete_evaluation() END");

        return $rtn;
    }

    /**
     * delete_evaludelete_evaluation_categoryation
     * @param type $levelup_apply_id
     */
    function delete_evaluation_category($levelup_apply_id) {

        $this->log->info("delete_evaluation_category() START");

        /* 論理削除用SQL */
		$sql  = "";
		$sql .= "	UPDATE cl_apl_levelup_apply_evaluation_category SET ";
		$sql .= "			delete_flg = :delete_flg , ";
		$sql .= "			update_date = current_timestamp , ";
		$sql .= "			update_user = :update_user ";
		$sql .= "	WHERE ";
		$sql .= "			levelup_apply_id = :levelup_apply_id ";

        /**
         * Parameter
         */
        $param=array();
		//2012/05/07 K.Fujii upd(s)
        //$param["delete_flg"]=true;
		$param["delete_flg"]=1;
		//2012/05/07 K.Fujii upd(e)
        $param["update_user"]=$this->user;
        $param["levelup_apply_id"]=$levelup_apply_id;

		/* カラムタイプ */
		//2012/05/07 K.Fujii upd(s)
		//$type = array('boolean','text','text');
		$type = array('integer','text','text');
		//2012/05/07 K.Fujii upd(e)

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info("delete_evaluation_category() END");

        return $rtn;
    }

    /**
     * delete_evaluation_group
     * @param type $levelup_apply_id
     */
    function delete_evaluation_group($levelup_apply_id) {

        $this->log->info("delete_evaluation_group() START");

        /* 論理削除用SQL */
		$sql  = "";
		$sql .= "	UPDATE cl_apl_levelup_apply_evaluation_group SET ";
		$sql .= "			delete_flg = :delete_flg , ";
		$sql .= "			update_date = current_timestamp , ";
		$sql .= "			update_user = :update_user ";
		$sql .= "	WHERE ";
		$sql .= "			levelup_apply_id = :levelup_apply_id ";

        /**
         * Parameter
         */
        $param=array();
		//2012/05/07 K.Fujii upd(s)
        //$param["delete_flg"]=true;
        $param["delete_flg"]=1;
		//2012/05/07 K.Fujii upd(e)
        $param["update_user"]=$this->user;
        $param["levelup_apply_id"]=$levelup_apply_id;

		/* カラムタイプ */
		//2012/05/07 K.Fujii upd(s)
		//$type = array('boolean','text','text');
		$type = array('integer','text','text');
		//2012/05/07 K.Fujii upd(e)

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info("delete_evaluation_group() END");

        return $rtn;
    }

    /**
     * delete_evaluation_item
     * @param type $levelup_apply_id
     */
    function delete_evaluation_item($levelup_apply_id) {

        $this->log->info("delete_evaluation_group() START");

        /* 論理削除用SQL */
		$sql  = "";
		$sql .= "	UPDATE cl_apl_levelup_apply_evaluation_item SET ";
		$sql .= "			delete_flg = :delete_flg , ";
		$sql .= "			update_date = current_timestamp , ";
		$sql .= "			update_user = :update_user ";
		$sql .= "	WHERE ";
		$sql .= "			levelup_apply_id = :levelup_apply_id ";

        /**
         * Parameter
         */
        $param=array();
		//2012/05/07 K.Fujii upd(s)
        //$param["delete_flg"]=true;
        $param["delete_flg"]=1;
		//2012/05/07 K.Fujii upd(e)
        $param["update_user"]=$this->user;
        $param["levelup_apply_id"]=$levelup_apply_id;

		/* カラムタイプ */
		//2012/05/07 K.Fujii upd(s)
		//$type = array('boolean','text','text');
		$type = array('integer','text','text');
		//2012/05/07 K.Fujii upd(e)

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info("delete_evaluation_item() END");

        return $rtn;
    }

}
