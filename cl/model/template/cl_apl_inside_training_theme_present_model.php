<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

/**
 * 院内研修課題提出　ＤＢモデル
 */
class cl_apl_inside_training_theme_present_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /** 登録用SQL */
//    var $insert_sql = "INSERT INTO cl_apl_inside_training_theme_present ( theme_apply_id, emp_id, apply_id, training_id, theme_id, present_date, present_answer, attach_file, present_learnd, boss_comment, chief_comment, sponsor_comment, teacher_comment, boss_id, chief_id, sponsor_id, teacher_id, delete_flg, create_date, create_user ) VALUES ( :theme_apply_id, :emp_id, :apply_id , :training_id , :theme_id , :present_date , :present_answer , :attach_file , :present_learnd , :boss_comment , :chief_comment , :sponsor_comment , :teacher_comment , :boss_id , :chief_id , :sponsor_id , :teacher_id , :delete_flg , current_timestamp , :create_user )";
    var $insert_sql = "INSERT INTO cl_apl_inside_training_theme_present ( theme_apply_id, emp_id, apply_id, training_apply_id, training_id, theme_id, present_date, present_answer, boss_comment, sponsor_comment, teacher_comment ,boss_id, sponsor_id, teacher_id, delete_flg, create_date, create_user ) VALUES ( :theme_apply_id, :emp_id, :apply_id , :training_apply_id, :training_id , :theme_id , :present_date , :present_answer , :boss_comment , :sponsor_comment , :teacher_comment , :boss_id , :sponsor_id , :teacher_id , :delete_flg , current_timestamp , :create_user )";

    /** 更新用SQL */
//    var $update_sql = "UPDATE cl_apl_inside_training_theme_present SET emp_id=:emp_id, apply_id=:apply_id, training_id = :training_id , theme_id = :theme_id , present_date = :present_date , present_answer = :present_answer , attach_file = :attach_file , present_learnd = :present_learnd , boss_comment = :boss_comment , chief_comment = :chief_comment , sponsor_comment = :sponsor_comment , teacher_comment = :teacher_comment , boss_id = :boss_id , chief_id = :chief_id , sponsor_id = :sponsor_id , teacher_id = :teacher_id , delete_flg = :delete_flg , update_date = current_timestamp , update_user = :update_user WHERE theme_apply_id = :theme_apply_id";
    var $update_sql = "UPDATE cl_apl_inside_training_theme_present SET apply_id=:apply_id, training_apply_id=:training_apply_id, training_id = :training_id , theme_id = :theme_id , present_date = :present_date , present_answer = :present_answer , boss_comment = :boss_comment , sponsor_comment = :sponsor_comment , teacher_comment = :teacher_comment , boss_id = :boss_id , sponsor_id = :sponsor_id , teacher_id = :teacher_id , delete_flg = :delete_flg , update_date = current_timestamp , update_user = :update_user WHERE theme_apply_id = :theme_apply_id";


    /** 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_apl_inside_training_theme_present WHERE theme_apply_id = :theme_apply_id ";

    /** 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_apl_inside_training_theme_present SET delete_flg = :delete_flg , update_date = current_timestamp , update_user = :update_user WHERE apply_id = :apply_id";

    /** 一覧取得用SQL */
//    var $select_list_sql = "SELECT theme_apply_id, emp_id, apply_id, training_id, theme_id, present_date, present_answer, attach_file, present_learnd, boss_comment, chief_comment, sponsor_comment, teacher_comment, boss_id, chief_id, sponsor_id, teacher_id,delete_flg,create_date,create_user,update_date,update_user FROM cl_apl_inside_training_theme_present ORDER BY emp_id, training_id, theme_id";
    var $select_list_sql = "SELECT * FROM cl_apl_inside_training_theme_present ORDER BY emp_id, training_id, theme_id";

    /** レコード取得用SQL */
//    var $select_rec_sql = "SELECT theme_apply_id, emp_id, apply_id, training_id, theme_id, present_date, present_answer, attach_file, present_learnd, boss_comment, chief_comment, sponsor_comment, teacher_comment, boss_id, chief_id, sponsor_id, teacher_id, delete_flg, create_date, create_user, update_date, update_user FROM cl_apl_inside_training_theme_present WHERE apply_id = :apply_id";
    var $select_rec_sql = "SELECT theme_apply_id, emp_id, apply_id, training_apply_id, training_id, theme_id, present_date, present_answer, boss_comment, sponsor_comment, teacher_comment, boss_id, sponsor_id, teacher_id, delete_flg, create_date, create_user, update_date, update_user FROM cl_apl_inside_training_theme_present WHERE apply_id = :apply_id";


    var $apv_data_select_rec_sql = "SELECT theme_present.*, apv.apply_id, apv.apv_order, apv.emp_id FROM cl_apl_inside_training_theme_present AS theme_present LEFT JOIN cl_applyapv AS apv ON theme_present.apply_id = apv.apply_id WHERE apv.apply_id = :apply_id AND theme_present.delete_flg=0 ORDER BY theme_present.apply_id";


    /** 登録用カラムタイプ */
//    var $insert_col_type = array('text','text','text','text','text','date','text','text','text','text','text','text','text','text','text','text','text','integer','text');
    var $insert_col_type = array('text','text','text','text','text','text','date','text','text','text','text','text','text','text','integer','text');

    /** 更新用カラムタイプ */
//    var $update_col_type = array('text','text','text','text','date','text','text','text','text','text','text','text','text','text', 'text','text','integer','text','text');
    var $update_col_type = array('text','text','text','text','date','text','text','text','text','text','text','text','integer','text','text');

    /** 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');
    /** 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('integer','text','text');

    /** 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_apl_inside_training_theme_present_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["create_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $id theme_apply_id
     */
    function physical_delete($id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["theme_apply_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $id theme_apply_id
     */
    function logical_delete($id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flg"]=1;
        $param["apply_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * list
     * @param type $id theme_apply_id
     */
    function getList(){

        $this->log->info("select() START");
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

     }

    /**
     * select
     * @param type $id theme_apply_id
     */
    function select($id){

        $this->log->info("select() START");
        $data=parent::select($this->select_rec_sql,array("text"),array("apply_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }


    /**
     * 研修申請報告用　承認者ID取得
     * @param type $training_apply_id
     */
    function apv_data_select_rec($id){

        $this->log->info("select_report_rec() START");
        $data=parent::select($this->apv_data_select_rec_sql,array("text"),array("apply_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select_report_rec() END");
        return $data;

     }

}
