<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');
// 2012/08/22 Yamagawa add(s)
require_once(dirname(__FILE__) . "/../../common/cl_common_define.inc");
// 2012/08/22 Yamagawa add(e)

class cl_apl_inside_training_answer_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 登録用SQL */
    var $insert_sql = "INSERT INTO cl_apl_inside_training_answer ( emp_id, report_answer_id, answer_id, report_id, apply_id, training_id, plan_id, achievement_degree, mine_learned, training_plan_degree, training_plan_degree_reason, training_method, training_method_reason, training_contents, training_contents_reason, training_practice, training_practice_reason, point_notice, delete_flg, create_date, create_user ) VALUES ( :emp_id, :report_answer_id, :answer_id, :report_id, :apply_id, :training_id, :plan_id, :achievement_degree, :mine_learned, :training_plan_degree, :training_plan_degree_reason, :training_method, :training_method_reason, :training_contents, :training_contents_reason, :training_practice, :training_practice_reason, :point_notice, :delete_flg, current_timestamp, :create_user )";
    /* 更新用SQL */
    var $update_sql = "UPDATE cl_apl_inside_training_answer SET emp_id = :emp_id, answer_id = :answer_id, report_id = :report_id, apply_id = :apply_id, training_id = :training_id, plan_id = :plan_id, achievement_degree = :achievement_degree, mine_learned = :mine_learned, training_plan_degree = :training_plan_degree, training_plan_degree_reason=:training_plan_degree_reason, training_method = :training_method, training_method_reason = :training_method_reason, training_contents = :training_contents, training_contents_reason = :training_contents_reason, training_practice = :training_practice, training_practice_reason = :training_practice_reason, point_notice = :point_notice, delete_flg = :delete_flg, update_date = current_timestamp, update_user = :update_user WHERE report_answer_id = :report_answer_id";
    /* 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_apl_inside_training_answer WHERE report_answer_id = :report_answer_id";
    /* 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_apl_inside_training_answer SET delete_flg = :delete_flg, update_date = current_timestamp, update_user = update_user  WHERE report_answer_id = :report_answer_id";
    var $logical_delete_from_apply_id_sql = "UPDATE cl_apl_inside_training_answer SET delete_flg = :delete_flg, update_date = current_timestamp, update_user = update_user  WHERE apply_id = :apply_id";
    /* 一覧取得用SQL */
    var $select_list_sql = "SELECT emp_id, report_answer_id, answer_id, report_id, apply_id, training_id, plan_id, achievement_degree, mine_learned,training_plan_degree, training_plan_degree_reason, training_method, training_method_reason, training_contents, training_contents_reason, training_practice, training_practice_reason, point_notice, delete_flg, create_date, create_user, update_date, update_user FROM cl_apl_inside_training_answer ORDER BY create_date";
    /* レコード取得用SQL */
    var $select_rec_sql = "SELECT emp_id, report_answer_id, answer_id, report_id, apply_id, training_id, plan_id, achievement_degree, mine_learned, training_plan_degree, training_plan_degree_reason, training_method, training_method_reason, training_contents, training_contents_reason, training_practice, training_practice_reason, point_notice, delete_flg, create_date, create_user, update_date, update_user FROM cl_apl_inside_training_answer WHERE report_answer_id = :report_answer_id";

    /* apply_id でのレコード取得用SQL */
    var $select_rec_by_apply_id_sql = "SELECT * FROM cl_apl_inside_training_answer WHERE apply_id = :apply_id";

    // 2012/08/22 Yamagawa del(s)
    //var $select_rec_by_plan_id_sql = "SELECT * FROM cl_apl_inside_training_answer AS answer WHERE answer.plan_id = :plan_id AND cl_apply.apply_id = answer.apply_id AND cl_apply.apply_stat=1 AND answer.delete_flg=0";
    // 2012/08/22 Yamagawa del(e)

    /* 登録用カラムタイプ */
    var $insert_col_type = array('text','text','text','text','text','text','text','integer','integer','integer','text','integer','text','integer','text','integer','text','text','integer','text');
    /* 更新用カラムタイプ */
    var $update_col_type = array('text','text','text','text','text','text','integer','integer','integer','text','integer','text','integer','text','integer','text','text','integer','text','text');

    /* 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');

    /* 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('integer','text','text');
    var $logical_delete_from_apply_id_col_type = array('integer','text','text');

    /* 登録用ステートメント */
    //var $insert_stmt;
    //var $update_stmt;
    //var $select_stmt;

    /**
     * Constractor
     */
    function cl_apl_inside_training_answer_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["create_user"]=$this->user;

        $this->log->debug("insert parameter set OK");
        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $report_answer_id
     */
    function physical_delete($id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["report_answer_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $report_answer_id
     */
    function logical_delete($id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flg"]=1;
        $param["report_answer_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $apply_id
     */
    function logical_delete_from_apply_id($apply_id) {

        $this->log->info("logical_delete_from_apply_id() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flg"]=1;
        $param["apply_id"]=$apply_id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_from_apply_id_sql,$this->logical_delete_from_apply_id_col_type,$param);

        $this->log->info("logical_delete_from_apply_id() END");

        return $rtn;
    }

    /**
     * list
     * @param type $report_answer_id
     */
    function getList(){

        $this->log->info("select() START");
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

     }

    /**
     * select
     * @param type $report_answer_id
     */
    function select($id){

        $this->log->info("select() START");
        $data=parent::select($this->select_rec_sql,array("text"),array("report_answer_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }

    /**
     * select by apply_id
     * @param string $apply_id
     */
    function select_by_apply_id($id){

        $this->log->info("select_by_apply_id() START    apply_id = ". $id);
        $data=parent::select($this->select_rec_by_apply_id_sql,array("text"),array("apply_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select_by_apply_id() END");
        return $data[0];

     }


    /**
     * select by plan_id
     * @param string $plan_id
     */
	function select_by_plan_id($id){

		$this->log->debug("select_by_plan_id() START plan_id = ". $id);
		// 2012/08/22 Yamagawa upd(s)
		//$data=parent::select($this->select_rec_by_plan_id_sql,array("text"),array("plan_id"=>$id),MDB2_FETCHMODE_ASSOC);
		$sql  = "";
		$sql .= " SELECT ";
		$sql .= "  CASE ans.achievement_degree ";
		$sql .= "   WHEN ".ACHIEVEMENT_BEST_CD." THEN '".ACHIEVEMENT_BEST_NM."' ";
		$sql .= "   WHEN ".ACHIEVEMENT_GOOD_CD." THEN '".ACHIEVEMENT_GOOD_NM."' ";
		$sql .= "   WHEN ".ACHIEVEMENT_NOT_CD." THEN '".ACHIEVEMENT_NOT_NM."' ";
		$sql .= "   WHEN ".ACHIEVEMENT_BAD_CD." THEN '".ACHIEVEMENT_BAD_NM."' ";
		$sql .= "  END AS achievement_degree ";
		$sql .= "  ,CASE ans.mine_learned ";
		$sql .= "    WHEN ".LEARNED_BEST_CD." THEN '".LEARNED_BEST_NM."' ";
		$sql .= "    WHEN ".LEARNED_GOOD_CD." THEN '".LEARNED_GOOD_NM."' ";
		$sql .= "    WHEN ".LEARNED_NOT_CD." THEN '".LEARNED_NOT_NM."' ";
		$sql .= "    WHEN ".LEARNED_BAD_CD." THEN '".LEARNED_BAD_NM."' ";
		$sql .= "   END AS mine_learned ";
		$sql .= "  ,CASE ans.training_plan_degree ";
		$sql .= "    WHEN ".PLAN_BEST_CD." THEN '".PLAN_BEST_NM."' ";
		$sql .= "    WHEN ".PLAN_BAD_CD." THEN '".PLAN_BAD_NM."' ";
		$sql .= "   END AS training_plan_degree ";
		$sql .= "  ,ans.training_plan_degree_reason ";
		$sql .= "  ,CASE ans.training_method ";
		$sql .= "    WHEN ".METHOD_BEST_CD." THEN '".METHOD_BEST_NM."' ";
		$sql .= "    WHEN ".METHOD_GOOD_CD." THEN '".METHOD_GOOD_NM."' ";
		$sql .= "    WHEN ".METHOD_NOT_CD." THEN '".METHOD_NOT_NM."' ";
		$sql .= "   END AS training_method ";
		$sql .= "  ,ans.training_method_reason ";
		$sql .= "  ,CASE ans.training_contents ";
		$sql .= "    WHEN ".CONTENTS_BEST_CD." THEN '".CONTENTS_BEST_NM."' ";
		$sql .= "    WHEN ".CONTENTS_GOOD_CD." THEN '".CONTENTS_GOOD_NM."' ";
		$sql .= "    WHEN ".CONTENTS_NOT_CD." THEN '".CONTENTS_NOT_NM."' ";
		$sql .= "    WHEN ".CONTENTS_BAD_CD." THEN '".CONTENTS_BAD_NM."' ";
		$sql .= "   END AS training_contents ";
		$sql .= "  ,ans.training_contents_reason ";
		$sql .= "  ,CASE ans.training_practice ";
		$sql .= "    WHEN ".PRACTICE_BEST_CD." THEN '".PRACTICE_BEST_NM."' ";
		$sql .= "    WHEN ".PRACTICE_GOOD_CD." THEN '".PRACTICE_GOOD_NM."' ";
		$sql .= "    WHEN ".PRACTICE_NOT_CD." THEN '".PRACTICE_NOT_NM."' ";
		$sql .= "    WHEN ".PRACTICE_BAD_CD." THEN '".PRACTICE_BAD_NM."' ";
		$sql .= "   END AS training_practice ";
		$sql .= "  ,ans.training_practice_reason ";
		$sql .= "  ,ans.point_notice ";
		$sql .= " FROM ";
		$sql .= "  cl_apl_inside_training_answer ans ";
		$sql .= "  INNER JOIN ";
		$sql .= "   cl_apply app ";
		$sql .= "  ON ";
		$sql .= "   app.apply_id = ans.apply_id ";
		$sql .= "   AND app.apply_stat = 1 ";
		$sql .= " WHERE ";
		$sql .= "  ans.plan_id = :plan_id ";
		$sql .= "  AND ans.delete_flg = 0 ";
		$data=parent::select($sql,array("text"),array("plan_id"=>$id),MDB2_FETCHMODE_ASSOC);
		// 2012/08/22 Yamagawa upd(e)
		$this->log->debug("select_by_plan_id() END");
		return $data;

	}

     // 2012/08/22 Yamagawa add(s)
    /**
     * select by training_id
     * @param string $training_id
     * @param integer $year
     */
	// 2012/11/16 Yamagawa upd(s)
	//function select_by_training_id($id){
	function select_by_training_id($id ,$year){
	// 2012/11/16 Yamagawa upd(e)

		$this->log->debug("select_by_training_id() START training_id = ". $id);
		$sql  = "";
		$sql .= " SELECT ";
		$sql .= "  CASE ans.achievement_degree ";
		$sql .= "   WHEN ".ACHIEVEMENT_BEST_CD." THEN '".ACHIEVEMENT_BEST_NM."' ";
		$sql .= "   WHEN ".ACHIEVEMENT_GOOD_CD." THEN '".ACHIEVEMENT_GOOD_NM."' ";
		$sql .= "   WHEN ".ACHIEVEMENT_NOT_CD." THEN '".ACHIEVEMENT_NOT_NM."' ";
		$sql .= "   WHEN ".ACHIEVEMENT_BAD_CD." THEN '".ACHIEVEMENT_BAD_NM."' ";
		$sql .= "  END AS achievement_degree ";
		$sql .= "  ,CASE ans.mine_learned ";
		$sql .= "    WHEN ".LEARNED_BEST_CD." THEN '".LEARNED_BEST_NM."' ";
		$sql .= "    WHEN ".LEARNED_GOOD_CD." THEN '".LEARNED_GOOD_NM."' ";
		$sql .= "    WHEN ".LEARNED_NOT_CD." THEN '".LEARNED_NOT_NM."' ";
		$sql .= "    WHEN ".LEARNED_BAD_CD." THEN '".LEARNED_BAD_NM."' ";
		$sql .= "   END AS mine_learned ";
		$sql .= "  ,CASE ans.training_plan_degree ";
		$sql .= "    WHEN ".PLAN_BEST_CD." THEN '".PLAN_BEST_NM."' ";
		$sql .= "    WHEN ".PLAN_BAD_CD." THEN '".PLAN_BAD_NM."' ";
		$sql .= "   END AS training_plan_degree ";
		$sql .= "  ,ans.training_plan_degree_reason ";
		$sql .= "  ,CASE ans.training_method ";
		$sql .= "    WHEN ".METHOD_BEST_CD." THEN '".METHOD_BEST_NM."' ";
		$sql .= "    WHEN ".METHOD_GOOD_CD." THEN '".METHOD_GOOD_NM."' ";
		$sql .= "    WHEN ".METHOD_NOT_CD." THEN '".METHOD_NOT_NM."' ";
		$sql .= "   END AS training_method ";
		$sql .= "  ,ans.training_method_reason ";
		$sql .= "  ,CASE ans.training_contents ";
		$sql .= "    WHEN ".CONTENTS_BEST_CD." THEN '".CONTENTS_BEST_NM."' ";
		$sql .= "    WHEN ".CONTENTS_GOOD_CD." THEN '".CONTENTS_GOOD_NM."' ";
		$sql .= "    WHEN ".CONTENTS_NOT_CD." THEN '".CONTENTS_NOT_NM."' ";
		$sql .= "    WHEN ".CONTENTS_BAD_CD." THEN '".CONTENTS_BAD_NM."' ";
		$sql .= "   END AS training_contents ";
		$sql .= "  ,ans.training_contents_reason ";
		$sql .= "  ,CASE ans.training_practice ";
		$sql .= "    WHEN ".PRACTICE_BEST_CD." THEN '".PRACTICE_BEST_NM."' ";
		$sql .= "    WHEN ".PRACTICE_GOOD_CD." THEN '".PRACTICE_GOOD_NM."' ";
		$sql .= "    WHEN ".PRACTICE_NOT_CD." THEN '".PRACTICE_NOT_NM."' ";
		$sql .= "    WHEN ".PRACTICE_BAD_CD." THEN '".PRACTICE_BAD_NM."' ";
		$sql .= "   END AS training_practice ";
		$sql .= "  ,ans.training_practice_reason ";
		$sql .= "  ,ans.point_notice ";
		$sql .= " FROM ";
		$sql .= "  cl_apl_inside_training_answer ans ";
		$sql .= "  INNER JOIN ";
		$sql .= "   cl_apply app ";
		$sql .= "  ON ";
		$sql .= "   app.apply_id = ans.apply_id ";
		$sql .= "   AND app.apply_stat = 1 ";
		// 2012/11/16 Yamagawa add(s)
		$sql .= "  INNER JOIN ";
		$sql .= "   cl_apl_inside_training_report rep ";
		$sql .= "  ON ";
		$sql .= "   rep.training_report_id = ans.report_id ";
		$sql .= "  INNER JOIN ";
		$sql .= "   cl_applied_inside_training tra ";
		$sql .= "  ON ";
		$sql .= "   tra.training_apply_id = rep.training_apply_id ";
		$sql .= "   AND tra.year = :year ";
		// 2012/11/16 Yamagawa add(e)
		$sql .= " WHERE ";
		$sql .= "  ans.training_id = :training_id ";
		$sql .= "  AND ans.delete_flg = 0 ";
		// 2012/11/16 Yamagawa upd(s)
		//$data=parent::select($sql,array("text"),array("training_id"=>$id),MDB2_FETCHMODE_ASSOC);
		$data=parent::select($sql,array("integer","text"),array("year"=>$year,"training_id"=>$id),MDB2_FETCHMODE_ASSOC);
		// 2012/11/16 Yamagawa upd(e)
		$this->log->debug("select_by_training_id() END");
		return $data;

	}

     /**
      * totalization by training_id
      * @param string $training_id
      * @param integer $year
      */
	// 2012/11/16 Yamagawa upd(s)
	//function totalization_by_training_id($id){
	function totalization_by_training_id($id ,$year){
	// 2012/11/16 Yamagawa upd(e)

		$this->log->debug("totalization_by_training_id() START training_id = ". $id);
		$sql = "";
		$sql .= " SELECT ";
		$sql .= "  wk.achievement_best_count ";
		$sql .= "  ,wk.achievement_good_count ";
		$sql .= "  ,wk.achievement_not_count ";
		$sql .= "  ,wk.achievement_bad_count ";
		$sql .= "  ,wk.learned_best_count ";
		$sql .= "  ,wk.learned_good_count ";
		$sql .= "  ,wk.learned_not_count ";
		$sql .= "  ,wk.learned_bad_count ";
		$sql .= "  ,wk.plan_best_count ";
		$sql .= "  ,wk.plan_bad_count ";
		$sql .= "  ,wk.method_best_count ";
		$sql .= "  ,wk.method_good_count ";
		$sql .= "  ,wk.method_not_count ";
		$sql .= "  ,wk.contents_best_count ";
		$sql .= "  ,wk.contents_good_count ";
		$sql .= "  ,wk.contents_not_count ";
		$sql .= "  ,wk.contents_bad_count ";
		$sql .= "  ,wk.practice_best_count ";
		$sql .= "  ,wk.practice_good_count ";
		$sql .= "  ,wk.practice_not_count ";
		$sql .= "  ,wk.practice_bad_count ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.achievement_best_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS achievement_best_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.achievement_good_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS achievement_good_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.achievement_not_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS achievement_not_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.achievement_bad_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS achievement_bad_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.learned_best_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS learned_best_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.learned_good_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS learned_good_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.learned_not_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS learned_not_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.learned_bad_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS learned_bad_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.plan_best_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS plan_best_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.plan_bad_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS plan_bad_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.method_best_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS method_best_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.method_good_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS method_good_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.method_not_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS method_not_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.contents_best_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS contents_best_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.contents_good_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS contents_good_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.contents_not_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS contents_not_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.contents_bad_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS contents_bad_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.practice_best_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS practice_best_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.practice_good_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS practice_good_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.practice_not_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS practice_not_percentage ";
		$sql .= "  ,TRUNC( ";
		$sql .= "    ROUND( ";
		$sql .= "     CAST(wk.practice_bad_count AS DEC) / wk.total_count ";
		$sql .= "     ,4 ";
		$sql .= "    ) * 100 ";
		$sql .= "    ,2 ";
		$sql .= "   ) || '%' AS practice_bad_percentage ";
		$sql .= " FROM ";
		$sql .= "  (SELECT ";
		$sql .= "    SUM(CASE WHEN ans.achievement_degree = ".ACHIEVEMENT_BEST_CD." THEN 1 ELSE 0 END) AS achievement_best_count ";
		$sql .= "    ,SUM(CASE WHEN ans.achievement_degree = ".ACHIEVEMENT_GOOD_CD." THEN 1 ELSE 0 END) AS achievement_good_count ";
		$sql .= "    ,SUM(CASE WHEN ans.achievement_degree = ".ACHIEVEMENT_NOT_CD." THEN 1 ELSE 0 END) AS achievement_not_count ";
		$sql .= "    ,SUM(CASE WHEN ans.achievement_degree = ".ACHIEVEMENT_BAD_CD." THEN 1 ELSE 0 END) AS achievement_bad_count ";
		$sql .= "    ,SUM(CASE WHEN ans.mine_learned = ".LEARNED_BEST_CD." THEN 1 ELSE 0 END) AS learned_best_count ";
		$sql .= "    ,SUM(CASE WHEN ans.mine_learned = ".LEARNED_GOOD_CD." THEN 1 ELSE 0 END) AS learned_good_count ";
		$sql .= "    ,SUM(CASE WHEN ans.mine_learned = ".LEARNED_NOT_CD." THEN 1 ELSE 0 END) AS learned_not_count ";
		$sql .= "    ,SUM(CASE WHEN ans.mine_learned = ".LEARNED_BAD_CD." THEN 1 ELSE 0 END) AS learned_bad_count ";
		$sql .= "    ,SUM(CASE WHEN ans.training_plan_degree = ".PLAN_BEST_CD." THEN 1 ELSE 0 END) AS plan_best_count ";
		$sql .= "    ,SUM(CASE WHEN ans.training_plan_degree = ".PLAN_BAD_CD." THEN 1 ELSE 0 END) AS plan_bad_count ";
		$sql .= "    ,SUM(CASE WHEN ans.training_method = ".METHOD_BEST_CD." THEN 1 ELSE 0 END) AS method_best_count ";
		$sql .= "    ,SUM(CASE WHEN ans.training_method = ".METHOD_GOOD_CD." THEN 1 ELSE 0 END) AS method_good_count ";
		$sql .= "    ,SUM(CASE WHEN ans.training_method = ".METHOD_NOT_CD." THEN 1 ELSE 0 END) AS method_not_count ";
		$sql .= "    ,SUM(CASE WHEN ans.training_contents = ".CONTENTS_BEST_CD." THEN 1 ELSE 0 END) AS contents_best_count ";
		$sql .= "    ,SUM(CASE WHEN ans.training_contents = ".CONTENTS_GOOD_CD." THEN 1 ELSE 0 END) AS contents_good_count ";
		$sql .= "    ,SUM(CASE WHEN ans.training_contents = ".CONTENTS_NOT_CD." THEN 1 ELSE 0 END) AS contents_not_count ";
		$sql .= "    ,SUM(CASE WHEN ans.training_contents = ".CONTENTS_BAD_CD." THEN 1 ELSE 0 END) AS contents_bad_count ";
		$sql .= "    ,SUM(CASE WHEN ans.training_practice = ".PRACTICE_BEST_CD." THEN 1 ELSE 0 END) AS practice_best_count ";
		$sql .= "    ,SUM(CASE WHEN ans.training_practice = ".PRACTICE_GOOD_CD." THEN 1 ELSE 0 END) AS practice_good_count ";
		$sql .= "    ,SUM(CASE WHEN ans.training_practice = ".PRACTICE_NOT_CD." THEN 1 ELSE 0 END) AS practice_not_count ";
		$sql .= "    ,SUM(CASE WHEN ans.training_practice = ".PRACTICE_BAD_CD." THEN 1 ELSE 0 END) AS practice_bad_count ";
		$sql .= "    ,COUNT(ans.plan_id) AS total_count ";
		$sql .= "   FROM ";
		$sql .= "    cl_apl_inside_training_answer ans ";
		$sql .= "    INNER JOIN ";
		$sql .= "     cl_apply app ";
		$sql .= "    ON ";
		$sql .= "     app.apply_id = ans.apply_id ";
		$sql .= "     AND app.apply_stat = 1 ";
		// 2012/11/16 Yamagawa add(s)
		$sql .= "    INNER JOIN ";
		$sql .= "     cl_apl_inside_training_report rep ";
		$sql .= "    ON ";
		$sql .= "     rep.training_report_id = ans.report_id ";
		$sql .= "    INNER JOIN ";
		$sql .= "     cl_applied_inside_training tra ";
		$sql .= "    ON ";
		$sql .= "      tra.training_apply_id = rep.training_apply_id ";
		$sql .= "      AND tra.year = :year ";
		// 2012/11/16 Yamagawa add(e)
		$sql .= "   WHERE ";
		$sql .= "    ans.training_id = :training_id ";
		$sql .= "    AND ans.delete_flg = 0) wk ";

		// 2012/11/16 Yamagawa upd(s)
		//$data=parent::select($sql,array("text"),array("training_id"=>$id),MDB2_FETCHMODE_ASSOC);
		$data=parent::select($sql,array("integer","text"),array("year"=>$year,"training_id"=>$id),MDB2_FETCHMODE_ASSOC);
		// 2012/11/16 Yamagawa upd(e)
		$this->log->debug("totalization_by_training_id() END");
		return $data[0];

     }
     // 2012/08/22 Yamagawa add(e)

}
