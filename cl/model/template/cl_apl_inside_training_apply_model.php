<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");
/**
 * 院内研修申請　ＤＢモデル
 *
 */
class cl_apl_inside_training_apply_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /** 登録用SQL */
    // 2012/11/20 Yamagawa upd(s)
    //var $insert_sql = "INSERT INTO cl_apl_inside_training_apply VALUES ( :emp_id, :training_apply_id, :apply_id, :charange_level, :inside_training_id, :plan_id, :inside_training_date, :change_apply_date, :first_apply_id, :before_apply_id, :update_division, :change_reason, :learn_memo, :recommend_flag, :delete_flg, current_timestamp, :create_user )";
    var $insert_sql = "INSERT INTO cl_apl_inside_training_apply (emp_id, training_apply_id, apply_id, charange_level, inside_training_id, plan_id, inside_training_date, change_apply_date, first_apply_id, before_apply_id, update_division, change_reason, learn_memo, recommend_flag, delete_flg, create_date, create_user, year) VALUES ( :emp_id, :training_apply_id, :apply_id, :charange_level, :inside_training_id, :plan_id, :inside_training_date, :change_apply_date, :first_apply_id, :before_apply_id, :update_division, :change_reason, :learn_memo, :recommend_flag, :delete_flg, current_timestamp, :create_user, :year)";
    // 2012/11/20 Yamagawa upd(e)

    /** 更新用SQL */
    // 2012/11/20 Yamagawa upd(s)
    //var $update_sql = "UPDATE cl_apl_inside_training_apply SET emp_id=:emp_id, apply_id=:apply_id, charange_level=:charange_level, inside_training_id=:inside_training_id, plan_id=:plan_id, inside_training_date=:inside_training_date, change_apply_date=:change_apply_date, first_apply_id=:first_apply_id, before_apply_id=:before_apply_id, update_division=:update_division, change_reason=:change_reason, learn_memo=:learn_memo, recommend_flag=:recommend_flag, update_date=current_timestamp, update_user=:update_user WHERE training_apply_id=:training_apply_id";
    var $update_sql = "UPDATE cl_apl_inside_training_apply SET emp_id=:emp_id, apply_id=:apply_id, charange_level=:charange_level, inside_training_id=:inside_training_id, plan_id=:plan_id, inside_training_date=:inside_training_date, change_apply_date=:change_apply_date, first_apply_id=:first_apply_id, before_apply_id=:before_apply_id, update_division=:update_division, change_reason=:change_reason, learn_memo=:learn_memo, recommend_flag=:recommend_flag, update_date=current_timestamp, update_user=:update_user, year=:year WHERE training_apply_id=:training_apply_id";
    // 2012/11/20 Yamagawa upd(e)

    /** 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_apl_inside_training_apply WHERE training_apply_id = :training_apply_id ";
    /** 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_apl_inside_training_apply SET delete_flg = :delete_flg , update_date=current_timestamp,update_user = :update_user WHERE training_apply_id = :training_apply_id";
    /** 一覧取得用SQL */
    var $select_list_sql = "SELECT emp_id, apply_id, charange_level, inside_training_id, plan_id, inside_training_date, change_apply_date, before_apply_id, inside_training_status, change_reason, learn_memo, recommend_flag, delete_flg, create_date, create_user, update_date, update_user FROM cl_apl_inside_training_apply ORDER BY create_date";

    /** レコード取得用SQL */
    var $select_rec_sql = "SELECT * FROM cl_apl_inside_training_apply LEFT JOIN cl_mst_inside_training ON cl_apl_inside_training_apply.inside_training_id = cl_mst_inside_training.training_id WHERE training_apply_id = :training_apply_id";

    /** 登録用カラムタイプ */
    // 2012/11/20 Yamagawa upd(s)
    //var $insert_col_type = array('text', 'text', 'text', 'integer', 'text', 'text', 'date', 'date', 'text', 'text', 'integer', 'text', 'text', 'integer', 'integer', 'text');
	var $insert_col_type = array('text', 'text', 'text', 'integer', 'text', 'text', 'date', 'date', 'text', 'text', 'integer', 'text', 'text', 'integer', 'integer', 'text', 'integer');
	// 2012/11/20 Yamagawa upd(e)

    /** 更新用カラムタイプ */
	// 2012/11/20 Yamagawa upd(s)
    //var $update_col_type = array('text', 'text', 'integer', 'text', 'text', 'date', 'date', 'text', 'text', 'integer', 'text', 'text', 'integer', 'text', 'text');
    var $update_col_type = array('text', 'text', 'integer', 'text', 'text', 'date', 'date', 'text', 'text', 'integer', 'text', 'text', 'integer', 'text', 'integer', 'text');
    // 2012/11/20 Yamagawa upd(e)

    /** 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');

    /** 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('integer','text','text');

    /**
     * Constractor
     * @param type $p_con MDB2 接続情報
     * @param type $p_login_user　login user
     */
    function cl_apl_inside_training_apply_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {

        $this->log->info("model insert() START");

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["create_user"]=$this->user;

        $this->log->debug("param length = " . $param.length);

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("model insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * 代理用 update
     * @param type $param
     */
    function sub_update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql2,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $training_apply_id
     */
    function physical_delete($id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["training_apply_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $training_apply_id
     */
    function logical_delete($id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flg"]=1;
        $param["training_apply_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * list
     * @param type $training_apply_id
     */
    function getList(){

        $this->log->info("select() START");
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

     }

    /**
     * select
     * @param type $training_apply_id
     */
    function select($id){

        $this->log->info("select() START  training_apply_id = " . $id);
        $data=parent::select($this->select_rec_sql,array("text"),array("training_apply_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }

    /**
     * select by apply_id
     * @param String $apply_id
     */
    function select_by_apply_id($id){

        $this->log->info("select_by_apply_id() START");

        $sql_buff[] = "SELECT ita.*, ";
        $sql_buff[] = "it.training_id , ";
        $sql_buff[] = "it.training_name , ";
        $sql_buff[] = "it.target_level1 , ";
        $sql_buff[] = "it.target_level2 , ";
        $sql_buff[] = "it.target_level3 , ";
        $sql_buff[] = "it.target_level4 , ";
        $sql_buff[] = "it.target_level5 , ";
        $sql_buff[] = "it.training_require , ";
        $sql_buff[] = "it.time_division , ";
        $sql_buff[] = "it.training_opener , ";
        $sql_buff[] = "it.training_teacher1 , ";
        $sql_buff[] = "it.training_teacher2 , ";
        $sql_buff[] = "it.training_teacher3 , ";
        $sql_buff[] = "it.training_teacher4 , ";
        $sql_buff[] = "it.training_teacher5 , ";
        $sql_buff[] = "it.training_slogan , ";

        $sql_buff[] = "replace(replace(replace(it.training_purpose ,chr(13)||chr(10),'<br/>'),chr(13),'<br/>'),chr(10),'<br/>') as training_purpose  , ";
        $sql_buff[] = "replace(replace(replace(it.training_contents,chr(13)||chr(10),'<br/>'),chr(13),'<br/>'),chr(10),'<br/>') as training_contents , ";
        $sql_buff[] = "replace(replace(replace(it.training_action  ,chr(13)||chr(10),'<br/>'),chr(13),'<br/>'),chr(10),'<br/>') as training_action   , ";
        $sql_buff[] = "replace(replace(replace(it.training_support ,chr(13)||chr(10),'<br/>'),chr(13),'<br/>'),chr(10),'<br/>') as training_support  , ";

        $sql_buff[] = "it.abolition_flag , ";
        $sql_buff[] = "it.delete_flg , ";
        $sql_buff[] = "it.create_date , ";
        $sql_buff[] = "it.create_user , ";
        $sql_buff[] = "it.update_date , ";
        $sql_buff[] = "it.update_user ";

        // 2012/11/19 Yamagawa add(s)
        $sql_buff[] = ",ita.year ";
        // 2012/11/19 Yamagawa add(e)
        // 2012/11/30 Yamagawa add(s)
        $sql_buff[] = ",wk.training_apply_id AS already_training_apply_id ";
        // 2012/11/30 Yamagawa add(e)
        $sql_buff[] = "FROM cl_apl_inside_training_apply as ita ";
        $sql_buff[] = "LEFT JOIN cl_mst_inside_training AS it ";
        $sql_buff[] = "ON ita.inside_training_id = it.training_id ";
        // 2012/11/30 Yamagawa add(s)
        // 2012/12/04 Mizuno del(s)
        //$sql_buff[] = "LEFT JOIN ";
        //$sql_buff[] = " cl_apl_inside_training_apply wk ";
        //$sql_buff[] = "ON ";
        //$sql_buff[] = " wk.emp_id = ita.emp_id ";
        //$sql_buff[] = " AND wk.inside_training_id = ita.inside_training_id ";
        //$sql_buff[] = " AND wk.year = ita.year ";
        //$sql_buff[] = " AND wk.delete_flg = 0 ";
        //$sql_buff[] = " AND EXISTS( ";
        //$sql_buff[] = "  SELECT ";
        //$sql_buff[] = "   app.apply_id ";
        //$sql_buff[] = "  FROM ";
        //$sql_buff[] = "   cl_apply app ";
        //$sql_buff[] = "  WHERE ";
        //$sql_buff[] = "   app.apply_id = wk.apply_id ";
        //$sql_buff[] = "   AND app.apply_stat IN ('0','1') ";
        //$sql_buff[] = "   AND app.draft_flg = FALSE ";
        //$sql_buff[] = "   AND app.delete_flg = FALSE ";
        //$sql_buff[] = " ) ";
        // 2012/12/04 Mizuno del(e)
        // 2012/12/04 Mizuno add(s)

        $sql_buff[] = "LEFT JOIN ";
		$sql_buff[] = " ( ";

		//申請中の研修申込み
		$sql_buff[] = "  SELECT  ";
		$sql_buff[] = "   a.training_apply_id, ";
		$sql_buff[] = "   a.emp_id, ";
		$sql_buff[] = "   a.inside_training_id, ";
		$sql_buff[] = "   a.year ";
		$sql_buff[] = "  FROM cl_apl_inside_training_apply a ";
		$sql_buff[] = "  INNER JOIN cl_apply b ";
		$sql_buff[] = "   ON a.apply_id = b.apply_id ";
		$sql_buff[] = "   AND a.delete_flg = 0 ";
		$sql_buff[] = "   AND a.update_division = 0 ";
		$sql_buff[] = "   AND b.apply_stat = 0 ";
		$sql_buff[] = "   AND b.draft_flg = FALSE ";
		$sql_buff[] = "   AND b.delete_flg = FALSE ";

		$sql_buff[] = "  UNION ALL ";

		//承認済みの研修申込み(取り消されていないもの)
		$sql_buff[] = "  SELECT  ";
		$sql_buff[] = "   a.training_apply_id, ";
		$sql_buff[] = "   a.emp_id, ";
		$sql_buff[] = "   a.inside_training_id, ";
		$sql_buff[] = "   a.year ";
		$sql_buff[] = "  FROM cl_applied_inside_training a ";
		$sql_buff[] = "  WHERE delete_flg <> 1 ";

		$sql_buff[] = " ) wk ";
		$sql_buff[] = "ON ";
        $sql_buff[] = " wk.emp_id = ita.emp_id ";
        $sql_buff[] = " AND wk.inside_training_id = ita.inside_training_id ";
        $sql_buff[] = " AND wk.year = ita.year ";

        // 2012/12/04 Mizuno add(e)
        // 2012/11/30 Yamagawa add(e)
        $sql_buff[] = "WHERE ita.apply_id = :apply_id ";
        $sql=implode('', $sql_buff);

        $data=parent::select($sql,array("text"),array("apply_id"=>$id),MDB2_FETCHMODE_ASSOC);
	    $this->log->debug(print_r($data,true),"\n","");
        $this->log->debug("select_by_apply_id() END");
        return $data[0];

     }

	//2012/05/10K.Fujii ins(s)
	function update_by_apply_id($param){
	        $this->log->info("update_by_apply_id() START");

	        /**
	         * Parameter
	         */
	        $param["update_user"]=$this->user;

			$sql = "";
			$sql .= " UPDATE ";
			$sql .= " 	cl_apl_inside_training_apply ";
			$sql .= " SET ";
			$sql .= " 	emp_id=:emp_id, ";
			$sql .= " 	charange_level=:charange_level, ";
			$sql .= " 	inside_training_id=:inside_training_id, ";
			$sql .= " 	plan_id=:plan_id, ";
			$sql .= " 	inside_training_date=:inside_training_date, ";
			$sql .= " 	change_apply_date=:change_apply_date, ";
			$sql .= " 	first_apply_id=:first_apply_id, ";
			$sql .= " 	before_apply_id=:before_apply_id, ";
			$sql .= " 	update_division=:update_division, ";
			$sql .= " 	change_reason=:change_reason, ";
			$sql .= " 	learn_memo=:learn_memo, ";
			$sql .= " 	recommend_flag=:recommend_flag, ";
			$sql .= " 	update_date=current_timestamp, ";
			$sql .= " 	update_user=:update_user ";
			// 2012/11/20 Yamagawa add(s)
			$sql .= " 	,year=:year ";
			// 2012/11/20 Yamagawa add(e)
			$sql .= " WHERE ";
			$sql .= " 	apply_id=:apply_id ";

	        /**
	         * 実行
	         */
			// 2012/11/20 Yamagawa upd(s)
	        //$rtn=parent::execute($sql,array("text","integer","text","text","date","date","text","text","integer","text","text","integer","text","text"),$param);
	        $rtn=parent::execute($sql,array("text","integer","text","text","date","date","text","text","integer","text","text","integer","text","integer","text"),$param);
	        // 2012/11/20 Yamagawa upd(e)

	        $this->log->info("update_by_apply_id() END");

	        return $rtn;
	}
	//2012/05/10K.Fujii ins(e)

	// 2012/11/02 Yamagawa add(s)
	function get_count_by_first_apply_id($id){

		$this->log->info("select_by_first_apply_id() START");

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " COUNT(tra.training_apply_id) AS cnt ";
		$sql .= "FROM ";
		$sql .= " cl_apl_inside_training_apply tra ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_apply app ";
		$sql .= " ON ";
		$sql .= "  app.apply_id = tra.apply_id ";
		$sql .= "  AND app.apply_stat = 0 ";		// 申請中
		$sql .= "  AND app.delete_flg = FALSE ";	// 申請取消以外
		$sql .= "  AND app.draft_flg = FALSE ";		// 下書き以外
		$sql .= "WHERE ";
		$sql .= " tra.first_apply_id = :id ";
		$sql .= " AND tra.delete_flg = 0 ";

        $data=parent::select($sql,array("text"),array("id"=>$id),MDB2_FETCHMODE_ASSOC);
		$this->log->info("select_by_first_apply_id() END");
        return $data[0]["cnt"];

	}
	// 2012/11/02 Yamagawa add(e)

}

