
<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_ldr_cwrkr_asmnt_auto_apply_manage_model extends cl_common_model{


    var $con;
    var $log;
    var $user;

    /**
     * Constractor コンストラクタ
     */
    function cl_ldr_cwrkr_asmnt_auto_apply_manage_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert 登録
     * @param type $param
     * パラメータの例：
     *     $param('cwrkr_asmnt_auto_apply_manage_id'=>cwrkr_asmnt_auto_apply_manage_id_value,'levelup_wkfw_apply_id'=>levelup_wkfw_apply_id_value,'levelup_apply_apply_id'=>levelup_apply_apply_id_value,'level'=>level_value,'cnddt_emp_id'=>cnddt_emp_id_value,'cnddt_emp_class'=>cnddt_emp_class_value,'cnddt_emp_attr'=>cnddt_emp_attr_value,'cnddt_emp_dept'=>cnddt_emp_dept_value,'cnddt_emp_room'=>cnddt_emp_room_value,'colleague_division'=>colleague_division_value,'cwrkr_emp_id'=>cwrkr_emp_id_value,'cwrkr_emp_class'=>cwrkr_emp_class_value,'cwrkr_emp_attr'=>cwrkr_emp_attr_value,'cwrkr_emp_dept'=>cwrkr_emp_dept_value,'cwrkr_emp_room'=>cwrkr_emp_room_value,'from_emp_id'=>from_emp_id_value,'from_emp_class'=>from_emp_class_value,'from_emp_attr'=>from_emp_attr_value,'from_emp_dept'=>from_emp_dept_value,'from_emp_room'=>from_emp_room_value,'cwrkr_asmnt_wkfw_apply_id'=>cwrkr_asmnt_wkfw_apply_id_value,'cwrkr_asmnt_apply_apply_id'=>cwrkr_asmnt_apply_apply_id_value,'proc_flg'=>proc_flg_value,'delete_flg'=>delete_flg_value,'create_user'=>create_user_value,'update_date'=>update_date_value,'update_user'=>update_user_value);
     */
    function insert($p_param) {

        $this->log->info(__FUNCTION__." START");

        /**
         * Parameter
         */
        $p_param["delete_flg"]=0;
        $p_param["create_user"]=$this->user;
        $p_param["update_date"]=null;
        $p_param["update_user"]=null;

        /* SQL */
        $sql = "INSERT INTO cl_ldr_cwrkr_asmnt_auto_apply_manage ( cwrkr_asmnt_auto_apply_manage_id,levelup_wkfw_apply_id,levelup_apply_apply_id,level,cnddt_emp_id,cnddt_emp_class,cnddt_emp_attr,cnddt_emp_dept,cnddt_emp_room,colleague_division,cwrkr_emp_id,cwrkr_emp_class,cwrkr_emp_attr,cwrkr_emp_dept,cwrkr_emp_room,from_emp_id,from_emp_class,from_emp_attr,from_emp_dept,from_emp_room,cwrkr_asmnt_wkfw_apply_id,cwrkr_asmnt_apply_apply_id,proc_flg,delete_flg,create_date,create_user,update_date,update_user ) VALUES ( :cwrkr_asmnt_auto_apply_manage_id,:levelup_wkfw_apply_id,:levelup_apply_apply_id,:level,:cnddt_emp_id,:cnddt_emp_class,:cnddt_emp_attr,:cnddt_emp_dept,:cnddt_emp_room,:colleague_division,:cwrkr_emp_id,:cwrkr_emp_class,:cwrkr_emp_attr,:cwrkr_emp_dept,:cwrkr_emp_room,:from_emp_id,:from_emp_class,:from_emp_attr,:from_emp_dept,:from_emp_room,:cwrkr_asmnt_wkfw_apply_id,:cwrkr_asmnt_apply_apply_id,:proc_flg,:delete_flg,current_timestamp,:create_user,:update_date,:update_user )";

        /* カラムタイプ */
        $type = array('text','text','text','integer','text','integer','integer','integer','integer','integer','text','integer','integer','integer','integer','text','integer','integer','integer','integer','text','text','integer','integer','text','timestamp','text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$p_param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }

    /**
     * update 更新
     * @param type $param
     * パラメータの例：
     *     $param('levelup_wkfw_apply_id'=>levelup_wkfw_apply_id_value,'levelup_apply_apply_id'=>levelup_apply_apply_id_value,'level'=>level_value,'cnddt_emp_id'=>cnddt_emp_id_value,'cnddt_emp_class'=>cnddt_emp_class_value,'cnddt_emp_attr'=>cnddt_emp_attr_value,'cnddt_emp_dept'=>cnddt_emp_dept_value,'cnddt_emp_room'=>cnddt_emp_room_value,'colleague_division'=>colleague_division_value,'cwrkr_emp_id'=>cwrkr_emp_id_value,'cwrkr_emp_class'=>cwrkr_emp_class_value,'cwrkr_emp_attr'=>cwrkr_emp_attr_value,'cwrkr_emp_dept'=>cwrkr_emp_dept_value,'cwrkr_emp_room'=>cwrkr_emp_room_value,'from_emp_id'=>from_emp_id_value,'from_emp_class'=>from_emp_class_value,'from_emp_attr'=>from_emp_attr_value,'from_emp_dept'=>from_emp_dept_value,'from_emp_room'=>from_emp_room_value,'cwrkr_asmnt_wkfw_apply_id'=>cwrkr_asmnt_wkfw_apply_id_value,'cwrkr_asmnt_apply_apply_id'=>cwrkr_asmnt_apply_apply_id_value,'proc_flg'=>proc_flg_value,'update_user'=>update_user_value);
     */
    function update($p_param) {

        $this->log->info(__FUNCTION__." START");


        $param=$this->getRecord($p_param["cwrkr_asmnt_auto_apply_manage_id"]);

        /**
         * Ｉ／Ｆされたパラメータを設定する
         */
        foreach( $param as $key => $value ){
        	if($p_param[$key]!=null && $p_param[$key]!=""){
        		$param[$key]=$p_param[$key];
        	}
        }
        unset($param['create_date']);
        unset($param['create_user']);
        unset($param['update_date']);
        unset($param['update_user']);
        unset($param['delete_flg']);

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /* SQL */
        $sql = "UPDATE cl_ldr_cwrkr_asmnt_auto_apply_manage SET levelup_wkfw_apply_id = :levelup_wkfw_apply_id ,levelup_apply_apply_id = :levelup_apply_apply_id ,level = :level ,cnddt_emp_id = :cnddt_emp_id ,cnddt_emp_class = :cnddt_emp_class ,cnddt_emp_attr = :cnddt_emp_attr ,cnddt_emp_dept = :cnddt_emp_dept ,cnddt_emp_room = :cnddt_emp_room ,colleague_division = :colleague_division ,cwrkr_emp_id = :cwrkr_emp_id ,cwrkr_emp_class = :cwrkr_emp_class ,cwrkr_emp_attr = :cwrkr_emp_attr ,cwrkr_emp_dept = :cwrkr_emp_dept ,cwrkr_emp_room = :cwrkr_emp_room ,from_emp_id = :from_emp_id ,from_emp_class = :from_emp_class ,from_emp_attr = :from_emp_attr ,from_emp_dept = :from_emp_dept ,from_emp_room = :from_emp_room ,cwrkr_asmnt_wkfw_apply_id = :cwrkr_asmnt_wkfw_apply_id ,cwrkr_asmnt_apply_apply_id = :cwrkr_asmnt_apply_apply_id ,proc_flg = :proc_flg ,delete_flg = 0 ,update_date = current_timestamp ,update_user = :update_user  WHERE cwrkr_asmnt_auto_apply_manage_id = :cwrkr_asmnt_auto_apply_manage_id";

        /* カラムタイプ */
        $type = array('text' , 'text' , 'integer' , 'text' , 'integer' , 'integer' , 'integer' , 'integer' , 'integer' , 'text' , 'integer' , 'integer' , 'integer' , 'integer' , 'text' , 'integer' , 'integer' , 'integer' , 'integer' , 'text' , 'text' , 'integer' , 'text' , 'text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }
    /**
     * physical_delete 物理削除
     * @param type $cwrkr_asmnt_auto_apply_manage_id プライマリーキー
     */
    function physical_delete($cwrkr_asmnt_auto_apply_manage_id) {

        $this->log->info(__FUNCTION__." START");

        /**
         * Parameter
         */
        $param=array();
        $param["cwrkr_asmnt_auto_apply_manage_id"]=$cwrkr_asmnt_auto_apply_manage_id;

        /* SQL */
        $sql = "DELETE FROM cl_ldr_cwrkr_asmnt_auto_apply_manage WHERE cwrkr_asmnt_auto_apply_manage_id = :cwrkr_asmnt_auto_apply_manage_id";

        /* カラムタイプ */
        $type = array('text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }

    /**
     * logical_delete 論理削除
     * @param type $cwrkr_asmnt_auto_apply_manage_id プライマリーキー
     */
    function logical_delete($cwrkr_asmnt_auto_apply_manage_id) {

        $this->log->info(__FUNCTION__." START");

        /**
         * Parameter
         */
        $param=array();
        $param["cwrkr_asmnt_auto_apply_manage_id"]=$cwrkr_asmnt_auto_apply_manage_id;
        $param["update_user"]=$this->user;

        /* SQL */
        $sql = "UPDATE cl_ldr_cwrkr_asmnt_auto_apply_manage SET delete_flg = 1 , update_date = current_timestamp , update_user = :update_user WHERE cwrkr_asmnt_auto_apply_manage_id = :cwrkr_asmnt_auto_apply_manage_id";

        /* カラムタイプ */
        $type = array('text','text');

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$type,$param);

        $this->log->info(__FUNCTION__." END");

        return $rtn;
    }

    /**
     * getList 全件取得
     * @param type $plan_id
     */
    function getList(){

        $this->log->info(__FUNCTION__." START");

        /* SQL */
        $sql = "SELECT * FROM cl_ldr_cwrkr_asmnt_auto_apply_manage ORDER BY cwrkr_asmnt_auto_apply_manage_id";

        /* カラムタイプ */
        $type = null;

        $data=parent::select($sql,$type,null,MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data;

     }

    /**
     * getRecord レコード取得（プライマリーキー指定）
     * @param type $cwrkr_asmnt_auto_apply_manage_id プライマリーキー
     */
    function getRecord($cwrkr_asmnt_auto_apply_manage_id){

        $this->log->info(__FUNCTION__." START");

        /* レコード取得用SQL */
        $sql = "SELECT * FROM cl_ldr_cwrkr_asmnt_auto_apply_manage WHERE cwrkr_asmnt_auto_apply_manage_id = :cwrkr_asmnt_auto_apply_manage_id";

        /* カラムタイプ */
        $type = array("text");

        //$data=$this->getRecordBySql($sql,$type,$cwrkr_asmnt_auto_apply_manage_id,MDB2_FETCHMODE_ASSOC);
		$data=$this->getRecordBySql($sql,$cwrkr_asmnt_auto_apply_manage_id);

        $this->log->info(__FUNCTION__." END");

        return $data;

     }

    /**
     * getRecord 任意SQLレコード取得（プライマリーキー指定）
     * @param type $cwrkr_asmnt_auto_apply_manage_id プライマリーキー
     */
    function getRecordBySql($sql,$cwrkr_asmnt_auto_apply_manage_id){

        $this->log->info(__FUNCTION__." START");

        /* レコード取得用SQL */
        //$sql = "SELECT * FROM cl_ldr_cwrkr_asmnt_auto_apply_manage WHERE cwrkr_asmnt_auto_apply_manage_id = :cwrkr_asmnt_auto_apply_manage_id";

        /* カラムタイプ */
        $type = array("text");

        $data=parent::select($sql,$type,array("cwrkr_asmnt_auto_apply_manage_id"=>$cwrkr_asmnt_auto_apply_manage_id),MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data[0];

     }

    /**
     * getRecord 任意SQLレコードリスト取得（パラメータ配列、カラムタイプ配列指定）
     * @param type $sql SQL
     * @param type $p_param パラメータ配列
     * @param type $type カラムタイプ
     */
    function getListBySql($sql,$p_param,$type){

        $this->log->info(__FUNCTION__." START");

        $data=parent::select($sql,$type,$p_param,MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data;

     }
}
