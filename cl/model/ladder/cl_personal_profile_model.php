<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_personal_profile_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

//    /* 登録用SQL */
//    var $insert_sql = "INSERT INTO cl_mst_inside_training ( training_id, training_name, training_purpose, target_level1, target_level2, target_level3, target_level4, target_level5, training_require, time_division, training_opener, training_teacher1, training_teacher2, training_teacher3, training_teacher4, training_teacher5, training_slogan, training_contents, training_action, training_support, abolition_flag, delete_flg, create_date, create_user ) VALUES ( :training_id, :training_name, :training_purpose, :target_level1, :target_level2, :target_level3, :target_level4, :target_level5, :training_require, :time_division, :training_opener, :training_teacher1, :training_teacher2, :training_teacher3, :training_teacher4, :training_teacher5, :training_slogan, :training_contents, :training_action, :training_support, :abolition_flag, :delete_flg, current_timestamp, :create_user )";
//    /* 更新用SQL */
//    var $update_sql = "UPDATE cl_mst_inside_training SET training_name = :training_name, training_purpose = :training_purpose, target_level1 = :target_level1, target_level2 = :target_level2, target_level3 = :target_level3, target_level4 = :target_level4, target_level5 = :target_level5, training_require = :training_require, time_division = :time_division, training_opener = :training_opener, training_teacher1 = :training_teacher1, training_teacher2 = :training_teacher2, training_teacher3 = :training_teacher3, training_teacher4 = :training_teacher4, training_teacher5 = :training_teacher5, training_slogan = :training_slogan, training_contents = :training_contents, training_action = :training_action, training_support = :training_support, abolition_flag = :abolition_flag, delete_flg = :delete_flg, update_date = current_timestamp, update_user = :update_user WHERE training_id = :training_id";
//    /* 物理削除用SQL */
//    var $physical_delete_sql = "DELETE FROM cl_mst_inside_training WHERE training_id = :training_id";
//    /* 論理削除用SQL */
//    var $logical_delete_sql = "UPDATE cl_mst_inside_training SET delete_flg = :delete_flg, update_date = current_timestamp, update_user = update_user  WHERE training_id = :training_id";
//    /* 一覧取得用SQL */
//    var $select_list_sql = "SELECT training_id, training_name, training_purpose, target_level1, target_level2, target_level3, target_level4, target_level5, training_require, time_division, training_opener, training_teacher1, training_teacher2, training_teacher3, training_teacher4, training_teacher5, training_slogan, training_contents, training_action, training_support, abolition_flag, delete_flg, create_date, create_user, update_date, update_user FROM cl_mst_inside_training ORDER BY create_date";
    /* ラダーレベル認定日登録 */
    var $insert_level_sql = "INSERT INTO cl_personal_profile (emp_id ,now_level ,get_level1_date ,get_level2_date ,get_level3_date ,get_level4_date ,get_level5_date ,delete_flg ,create_date ,create_user) VALUES (:emp_id ,:now_level ,:get_level1_date ,:get_level2_date ,:get_level3_date ,:get_level4_date ,:get_level5_date ,:delete_flg ,current_timestamp ,:create_user)";
    /* ラダーレベル認定日更新 */
    var $update_level_sql = "UPDATE cl_personal_profile SET now_level = :now_level ,get_level1_date = :get_level1_date ,get_level2_date = :get_level2_date ,get_level3_date = :get_level3_date ,get_level4_date = :get_level4_date ,get_level5_date = :get_level5_date ,delete_flg = :delete_flg ,update_date = current_timestamp ,update_user = :update_user WHERE emp_id = :emp_id";
    /* レコード取得用SQL */
    var $select_rec_sql = "SELECT emp_id , now_level , get_level1_date , get_level2_date , get_level3_date , get_level4_date , get_level5_date , delete_flg , create_date , create_user , update_date , update_user FROM cl_personal_profile WHERE emp_id = :emp_id";

//    /* 登録用カラムタイプ */
//    var $insert_col_type = array('text','text','text','integer','integer','integer','integer','integer','text','integer','text','text','text','text','text','text','text','text','text','text','integer','integer','text');
//    /* 更新用カラムタイプ */
//    var $update_col_type = array('text','text','integer','integer','integer','integer','integer','text','integer','text','text','text','text','text','text','text','text','text','text','integer','integer','text','text');

//    /* 物理削除用カラムタイプ */
//    var $physical_delete_col_type = array('text');
//    /* 論理削除用カラムタイプ */
//    var $logical_delete_col_type = array('integer','text','text','text');

    /* ラダーレベル認定日登録 */
    var $insert_level_col_type = array("text","integer","date","date","date","date","date","integer","text");
    /* ラダーレベル認定日更新 */
    var $update_level_col_type = array("integer","date","date","date","date","date","integer","text","text");

    /**
     * Constractor
     */
    function cl_mst_inside_training_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

//    /**
//     * insert
//     * @param type $param
//     */
//    function insert($param) {
//
//        $this->log->info("insert() START");
//
//        /**
//         * Parameter
//         */
//        $param["delete_flg"]=0;
//        $param["create_user"]=$this->user;
//
//        /**
//         * 実行
//         */
//        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);
//
//        $this->log->info("insert() END");
//        return $rtn;
//    }
//
//    /**
//     * update
//     * @param type $param
//     */
//    function update($param) {
//
//        $this->log->info("update() START");
//
//        /**
//         * Parameter
//         */
//        $param["update_user"]=$this->user;
//
//        /**
//         * 実行
//         */
//        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);
//
//        $this->log->info("update() END");
//
//        return $rtn;
//    }
//
//    /**
//     * physical_delete
//     * @param type $training_id
//     */
//    function physical_delete($id) {
//
//        $this->log->info("physical_delete() START");
//
//        /**
//         * Parameter
//         */
//        $param=array();
//        $param["training_id"]=$id;
//        $param["update_user"]=$this->user;
//
//        /**
//         * 実行
//         */
//        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);
//
//        $this->log->info("physical_delete() END");
//
//        return $rtn;
//    }
//
//    /**
//     * logical_delete
//     * @param type $training_id
//     */
//    function logical_delete($id) {
//
//        $this->log->info("logical_delete() START");
//
//        /**
//         * Parameter
//         */
//        $param=array();
//        $param["delete_flg"]=1;
//        $param["training_id"]=$id;
//        $param["update_user"]=$this->user;
//
//        /**
//         * 実行
//         */
//        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);
//
//        $this->log->info("logical_delete() END");
//
//        return $rtn;
//    }
//
//    /**
//     * list
//     * @param type $training_id
//     */
//    function getList(){
//
//        $this->log->info("select() START");
//        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
//        $this->log->debug("select() END");
//        return $data;
//
//     }


    /**
     * select
     * @param type $id
     */
    function select($id){

        $this->log->info("select() START");
        $data=parent::select($this->select_rec_sql,array("text"),array("emp_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }

    /**
     * insert
     * @param type $param
     */
    function insert_level($param) {

        $this->log->info("insert_level() START");

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["create_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_level_sql,$this->insert_level_col_type,$param);

        $this->log->info("insert_level() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update_level($param) {

        $this->log->info("update_level() START");

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_level_sql,$this->update_level_col_type,$param);

        $this->log->info("update_level() END");

        return $rtn;
    }

    /**
     * insert
     * @param type $param
     */
    function insert_ladderlevel($param) {

        $this->log->info("insert_ladderlevel() START");

        /**
         * Parameter
         */
        $param_edit = array(
            "emp_id"		=> $param["emp_id"]
            ,"now_level"	=> $param["now_level"]
        );
        $col_type = array("text","integer");

        $sql  = "";
        $sql  = "INSERT INTO cl_personal_profile ( ";
        $sql .= "    emp_id ";
        $sql .= "    ,now_level ";

        if ($param['ladderlevel'] == '1')
        {
            if ($param['get_level_date'] != ''){
                $sql .= "    ,get_level1_date ";
            }

            if ($param['get_level_total_value'] != ''){
                $sql .= "    ,level1_total_value ";
            }

            if ($param['get_level_certificate_date'] != ''){
                $sql .= "    ,level1_certificate_date ";
            }
			//2012/05/15 K.Fujii ins(s)
			$sql .= "    ,get_level1_class ";
			$sql .= "    ,get_level1_atrb ";
			$sql .= "    ,get_level1_dept ";
			$sql .= "    ,get_level1_room ";
			//2012/05/15 K.Fujii ins(e)
        }

        if ($param['ladderlevel'] == '2')
        {
            if ($param['get_level_date'] != ''){
                $sql .= "    ,get_level2_date ";
            }

            if ($param['get_level_total_value'] != ''){
                $sql .= "    ,level2_total_value ";
            }

            if ($param['get_level_certificate_date'] != ''){
                $sql .= "    ,level2_certificate_date ";
            }
			//2012/05/15 K.Fujii ins(s)
			$sql .= "    ,get_level2_class ";
			$sql .= "    ,get_level2_atrb ";
			$sql .= "    ,get_level2_dept ";
			$sql .= "    ,get_level2_room ";
			//2012/05/15 K.Fujii ins(e)
        }

        if ($param['ladderlevel'] == '3')
        {
            if ($param['get_level_date'] != ''){
                $sql .= "    ,get_level3_date ";
            }

            if ($param['get_level_total_value'] != ''){
                $sql .= "    ,level3_total_value ";
            }

            if ($param['get_level_certificate_date'] != ''){
                $sql .= "    ,level3_certificate_date ";
            }
			//2012/05/15 K.Fujii ins(s)
			$sql .= "    ,get_level3_class ";
			$sql .= "    ,get_level3_atrb ";
			$sql .= "    ,get_level3_dept ";
			$sql .= "    ,get_level3_room ";
			//2012/05/15 K.Fujii ins(e)
        }

        if ($param['ladderlevel'] == '4')
        {
            if ($param['get_level_date'] != ''){
                $sql .= "    ,get_level4_date ";
            }

            if ($param['get_level_total_value'] != ''){
                $sql .= "    ,level4_total_value ";
            }

            if ($param['get_level_certificate_date'] != ''){
                $sql .= "    ,level4_certificate_date ";
            }
			//2012/05/15 K.Fujii ins(s)
			$sql .= "    ,get_level4_class ";
			$sql .= "    ,get_level4_atrb ";
			$sql .= "    ,get_level4_dept ";
			$sql .= "    ,get_level4_room ";
			//2012/05/15 K.Fujii ins(e)
        }

        if ($param['ladderlevel'] == '5')
        {
            if ($param['get_level_date'] != ''){
                $sql .= "    ,get_level5_date ";
            }

            if ($param['get_level_total_value'] != ''){
                $sql .= "    ,level5_total_value ";
            }

            if ($param['get_level_certificate_date'] != ''){
                $sql .= "    ,level5_certificate_date ";
            }
			//2012/05/15 K.Fujii ins(s)
			$sql .= "    ,get_level5_class ";
			$sql .= "    ,get_level5_atrb ";
			$sql .= "    ,get_level5_dept ";
			$sql .= "    ,get_level5_room ";
			//2012/05/15 K.Fujii ins(e)
        }

        $sql .= "    ,delete_flg  ";
        $sql .= "    ,create_date  ";
        $sql .= "    ,create_user ";
        $sql .= ") VALUES ( ";
        $sql .= "    :emp_id  ";
        $sql .= "    ,:now_level ";

        if ($param['get_level1_date'] != ''){
            $sql .= "    ,:get_level1_date ";

            $param_edit += array("get_level1_date" => $param['get_level1_date']);
            array_push($col_type ,"date");
        }

        if ($param['ladderlevel'] == '1')
        {
            if ($param['get_level_date'] != ''){
                $sql .= "    ,:get_level_date ";

                $param_edit += array("get_level_date" => $param['get_level_date']);
                array_push($col_type ,"date");
            }

            if ($param['get_level_total_value'] != ''){
                $sql .= "    ,:get_level_total_value ";

                $param_edit += array("get_level_total_value" => $param['get_level_total_value']);
                array_push($col_type ,"integer");
            }

            if ($param['get_level_certificate_date'] != ''){
                $sql .= "    ,:get_level_certificate_date ";

                $param_edit += array("get_level_certificate_date" => $param['get_level_certificate_date']);
                array_push($col_type ,"date");
            }
			//2012/05/15 K.Fujii ins(s)
			$sql .= "    ,:get_level1_class ";
			$param_edit += array("get_level1_class" => $param['level_class']);
			array_push($col_type ,"integer");
			$sql .= "    ,:get_level1_atrb ";
			$param_edit += array("get_level1_atrb" => $param['level_attribute']);
			array_push($col_type ,"integer");
			$sql .= "    ,:get_level1_dept ";
			$param_edit += array("get_level1_dept" => $param['level_dept']);
			array_push($col_type ,"integer");
			$sql .= "    ,:get_level1_room ";
			$param_edit += array("get_level1_room" => $param['level_room']);
			array_push($col_type ,"integer");
			//2012/05/15 K.Fujii ins(e)
        }

        if ($param['ladderlevel'] == '2')
        {
            if ($param['get_level_date'] != ''){
                $sql .= "    ,:get_level_date ";

                $param_edit += array("get_level_date" => $param['get_level_date']);
                array_push($col_type ,"date");
            }

            if ($param['get_level_total_value'] != ''){
                $sql .= "    ,:get_level_total_value ";

                $param_edit += array("get_level_total_value" => $param['get_level_total_value']);
                array_push($col_type ,"integer");
            }

            if ($param['get_level_certificate_date'] != ''){
                $sql .= "    ,:get_level_certificate_date ";

                $param_edit += array("get_level_certificate_date" => $param['get_level_certificate_date']);
                array_push($col_type ,"date");
            }
			//2012/05/15 K.Fujii ins(s)
			$sql .= "    ,:get_level2_class ";
			$param_edit += array("get_level2_class" => $param['level_class']);
			array_push($col_type ,"integer");
			$sql .= "    ,:get_level2_atrb ";
			$param_edit += array("get_level2_atrb" => $param['level_attribute']);
			array_push($col_type ,"integer");
			$sql .= "    ,:get_level2_dept ";
			$param_edit += array("get_level2_dept" => $param['level_dept']);
			array_push($col_type ,"integer");
			$sql .= "    ,:get_level2_room ";
			$param_edit += array("get_level2_room" => $param['level_room']);
			array_push($col_type ,"integer");
			//2012/05/15 K.Fujii ins(e)
        }

        if ($param['ladderlevel'] == '3')
        {
            if ($param['get_level_date'] != ''){
                $sql .= "    ,:get_level_date ";

                $param_edit += array("get_level_date" => $param['get_level_date']);
                array_push($col_type ,"date");
            }

            if ($param['get_level_total_value'] != ''){
                $sql .= "    ,:get_level_total_value ";

                $param_edit += array("get_level_total_value" => $param['get_level_total_value']);
                array_push($col_type ,"integer");
            }

            if ($param['get_level_certificate_date'] != ''){
                $sql .= "    ,:get_level_certificate_date ";

                $param_edit += array("get_level_certificate_date" => $param['get_level_certificate_date']);
                array_push($col_type ,"date");
            }
			//2012/05/15 K.Fujii ins(s)
			$sql .= "    ,:get_level3_class ";
			$param_edit += array("get_level3_class" => $param['level_class']);
			array_push($col_type ,"integer");
			$sql .= "    ,:get_level3_atrb ";
			$param_edit += array("get_level3_atrb" => $param['level_attribute']);
			array_push($col_type ,"integer");
			$sql .= "    ,:get_level3_dept ";
			$param_edit += array("get_level3_dept" => $param['level_dept']);
			array_push($col_type ,"integer");
			$sql .= "    ,:get_level3_room ";
			$param_edit += array("get_level3_room" => $param['level_room']);
			array_push($col_type ,"integer");
			//2012/05/15 K.Fujii ins(e)
        }

        if ($param['ladderlevel'] == '4')
        {
            if ($param['get_level_date'] != ''){
                $sql .= "    ,:get_level_date ";

                $param_edit += array("get_level_date" => $param['get_level_date']);
                array_push($col_type ,"date");
            }

            if ($param['get_level_total_value'] != ''){
                $sql .= "    ,:get_level_total_value ";

                $param_edit += array("get_level_total_value" => $param['get_level_total_value']);
                array_push($col_type ,"integer");
            }

            if ($param['get_level_certificate_date'] != ''){
                $sql .= "    ,:get_level_certificate_date ";

                $param_edit += array("get_level_certificate_date" => $param['get_level_certificate_date']);
                array_push($col_type ,"date");
            }
			//2012/05/15 K.Fujii ins(s)
			$sql .= "    ,:get_level4_class ";
			$param_edit += array("get_level4_class" => $param['level_class']);
			array_push($col_type ,"integer");
			$sql .= "    ,:get_level4_atrb ";
			$param_edit += array("get_level4_atrb" => $param['level_attribute']);
			array_push($col_type ,"integer");
			$sql .= "    ,:get_level4_dept ";
			$param_edit += array("get_level4_dept" => $param['level_dept']);
			array_push($col_type ,"integer");
			$sql .= "    ,:get_level4_room ";
			$param_edit += array("get_level4_room" => $param['level_room']);
			array_push($col_type ,"integer");
			//2012/05/15 K.Fujii ins(e)
        }

        if ($param['ladderlevel'] == '5')
        {
            if ($param['get_level_date'] != ''){
                $sql .= "    ,:get_level_date ";

                $param_edit += array("get_level_date" => $param['get_level_date']);
                array_push($col_type ,"date");
            }

            if ($param['get_level_total_value'] != ''){
                $sql .= "    ,:get_level_total_value ";

                $param_edit += array("get_level_total_value" => $param['get_level_total_value']);
                array_push($col_type ,"integer");
            }

            if ($param['get_level_certificate_date'] != ''){
                $sql .= "    ,:get_level_certificate_date ";

                $param_edit += array("get_level_certificate_date" => $param['get_level_certificate_date']);
                array_push($col_type ,"date");
            }
			//2012/05/15 K.Fujii ins(s)
			$sql .= "    ,:get_level5_class ";
			$param_edit += array("get_level5_class" => $param['level_class']);
			array_push($col_type ,"integer");
			$sql .= "    ,:get_level5_atrb ";
			$param_edit += array("get_level5_atrb" => $param['level_attribute']);
			array_push($col_type ,"integer");
			$sql .= "    ,:get_level5_dept ";
			$param_edit += array("get_level5_dept" => $param['level_dept']);
			array_push($col_type ,"integer");
			$sql .= "    ,:get_level5_room ";
			$param_edit += array("get_level5_room" => $param['level_room']);
			array_push($col_type ,"integer");
			//2012/05/15 K.Fujii ins(e)
        }

        $sql .= "    ,:delete_flg ";
        $sql .= "    ,current_timestamp ";
        $sql .= "    ,:create_user) ";
        $this->log->info($sql);

        /**
         * Parameter
         */
        $param_edit += array("delete_flg" => 0);
        array_push($col_type ,"integer");
        $param_edit += array("create_user" => $this->user);
        array_push($col_type ,"text");

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$col_type,$param_edit);

        $this->log->info("insert_ladderlevel() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update_ladderlevel($param) {

        $this->log->info("update_ladderlevel() START");

        /**
         * Parameter
         */
        $param_edit = array(
            "now_level"	=> $param["now_level"]
        );
        $col_type = array("integer");

        $sql  = "";
        $sql  = "UPDATE cl_personal_profile SET ";
        $sql .= "    now_level = :now_level ";

        if ($param['ladderlevel'] == '1')
        {
            if ($param['get_level_date'] != ''){
                $sql .= "    ,get_level1_date = :get_level_date ";

                $param_edit += array("get_level_date" => $param['get_level_date']);
                array_push($col_type ,"date");
            } else {
                $sql .= "    ,get_level1_date = null ";
            }

            if ($param['get_level_total_value'] != ''){
                $sql .= "    ,level1_total_value = :get_level_total_value ";

                $param_edit += array("get_level_total_value" => $param['get_level_total_value']);
                array_push($col_type ,"integer");
            } else {
                $sql .= "    ,level1_total_value = null ";
            }

            if ($param['get_level_certificate_date'] != ''){
                $sql .= "    ,level1_certificate_date = :get_level_certificate_date ";

                $param_edit += array("get_level_certificate_date" => $param['get_level_certificate_date']);
                array_push($col_type ,"date");
            } else {
                $sql .= "    ,level1_certificate_date = null ";
            }
			//2012/05/15 K.Fujii ins(s)
			$sql .= "    ,get_level1_class = :get_level1_class ";
			$param_edit += array("get_level1_class" => $param['level_class']);
			array_push($col_type ,"integer");
			$sql .= "    ,get_level1_atrb = :get_level1_atrb ";
			$param_edit += array("get_level1_atrb" => $param['level_attribute']);
			array_push($col_type ,"integer");
			$sql .= "    ,get_level1_dept = :get_level1_dept ";
			$param_edit += array("get_level1_dept" => $param['level_dept']);
			array_push($col_type ,"integer");
			$sql .= "    ,get_level1_room = :get_level1_room ";
			$param_edit += array("get_level1_room" => $param['level_room']);
			array_push($col_type ,"integer");
			//2012/05/15 K.Fujii ins(e)
        }

        if ($param['ladderlevel'] == '2')
        {
            if ($param['get_level_date'] != ''){
                $sql .= "    ,get_level2_date = :get_level_date ";

                $param_edit += array("get_level_date" => $param['get_level_date']);
                array_push($col_type ,"date");
            } else {
                $sql .= "    ,get_level2_date = null ";
            }

            if ($param['get_level_total_value'] != ''){
                $sql .= "    ,level2_total_value = :get_level_total_value ";

                $param_edit += array("get_level_total_value" => $param['get_level_total_value']);
                array_push($col_type ,"integer");
            } else {
                $sql .= "    ,level2_total_value = null ";
            }

            if ($param['get_level_certificate_date'] != ''){
                $sql .= "    ,level2_certificate_date = :get_level_certificate_date ";

                $param_edit += array("get_level_certificate_date" => $param['get_level_certificate_date']);
                array_push($col_type ,"date");
            } else {
                $sql .= "    ,level2_certificate_date = null ";
            }
			//2012/05/15 K.Fujii ins(s)
			$sql .= "    ,get_level2_class = :get_level2_class ";
			$param_edit += array("get_level2_class" => $param['level_class']);
			array_push($col_type ,"integer");
			$sql .= "    ,get_level2_atrb = :get_level2_atrb ";
			$param_edit += array("get_level2_atrb" => $param['level_attribute']);
			array_push($col_type ,"integer");
			$sql .= "    ,get_level2_dept = :get_level2_dept ";
			$param_edit += array("get_level2_dept" => $param['level_dept']);
			array_push($col_type ,"integer");
			$sql .= "    ,get_level2_room = :get_level2_room ";
			$param_edit += array("get_level2_room" => $param['level_room']);
			array_push($col_type ,"integer");
			//2012/05/15 K.Fujii ins(e)
        }

        if ($param['ladderlevel'] == '3')
        {
            if ($param['get_level_date'] != ''){
                $sql .= "    ,get_level3_date = :get_level_date ";

                $param_edit += array("get_level_date" => $param['get_level_date']);
                array_push($col_type ,"date");
            } else {
                $sql .= "    ,get_level3_date = null ";
            }

            if ($param['get_level_total_value'] != ''){
                $sql .= "    ,level3_total_value = :get_level_total_value ";

                $param_edit += array("get_level_total_value" => $param['get_level_total_value']);
                array_push($col_type ,"integer");
            } else {
                $sql .= "    ,level3_total_value = null ";
            }

            if ($param['get_level_certificate_date'] != ''){
                $sql .= "    ,level3_certificate_date = :get_level_certificate_date ";

                $param_edit += array("get_level_certificate_date" => $param['get_level_certificate_date']);
                array_push($col_type ,"date");
            } else {
                $sql .= "    ,level3_certificate_date = null ";
            }
			//2012/05/15 K.Fujii ins(s)
			$sql .= "    ,get_level3_class = :get_level3_class ";
			$param_edit += array("get_level3_class" => $param['level_class']);
			array_push($col_type ,"integer");
			$sql .= "    ,get_level3_atrb = :get_level3_atrb ";
			$param_edit += array("get_level3_atrb" => $param['level_attribute']);
			array_push($col_type ,"integer");
			$sql .= "    ,get_level3_dept = :get_level3_dept ";
			$param_edit += array("get_level3_dept" => $param['level_dept']);
			array_push($col_type ,"integer");
			$sql .= "    ,get_level3_room = :get_level3_room ";
			$param_edit += array("get_level3_room" => $param['level_room']);
			array_push($col_type ,"integer");
			//2012/05/15 K.Fujii ins(e)
        }

        if ($param['ladderlevel'] == '4')
        {
            if ($param['get_level_date'] != ''){
                $sql .= "    ,get_level4_date = :get_level_date ";

                $param_edit += array("get_level_date" => $param['get_level_date']);
                array_push($col_type ,"date");
            } else {
                $sql .= "    ,get_level4_date = null ";
            }

            if ($param['get_level_total_value'] != ''){
                $sql .= "    ,level4_total_value = :get_level_total_value ";

                $param_edit += array("get_level_total_value" => $param['get_level_total_value']);
                array_push($col_type ,"integer");
            } else {
                $sql .= "    ,level4_total_value = null ";
            }

            if ($param['get_level_certificate_date'] != ''){
                $sql .= "    ,level4_certificate_date = :get_level_certificate_date ";

                $param_edit += array("get_level_certificate_date" => $param['get_level_certificate_date']);
                array_push($col_type ,"date");
            } else {
                $sql .= "    ,level4_certificate_date = null ";
            }
			//2012/05/15 K.Fujii ins(s)
			$sql .= "    ,get_level4_class = :get_level4_class ";
			$param_edit += array("get_level4_class" => $param['level_class']);
			array_push($col_type ,"integer");
			$sql .= "    ,get_level4_atrb = :get_level4_atrb ";
			$param_edit += array("get_level4_atrb" => $param['level_attribute']);
			array_push($col_type ,"integer");
			$sql .= "    ,get_level4_dept = :get_level4_dept ";
			$param_edit += array("get_level4_dept" => $param['level_dept']);
			array_push($col_type ,"integer");
			$sql .= "    ,get_level4_room = :get_level4_room ";
			$param_edit += array("get_level4_room" => $param['level_room']);
			array_push($col_type ,"integer");
			//2012/05/15 K.Fujii ins(e)
        }

        if ($param['ladderlevel'] == '5')
        {
            if ($param['get_level_date'] != ''){
                $sql .= "    ,get_level5_date = :get_level_date ";

                $param_edit += array("get_level_date" => $param['get_level_date']);
                array_push($col_type ,"date");
            } else {
                $sql .= "    ,get_level5_date = null ";
            }

            if ($param['get_level_total_value'] != ''){
                $sql .= "    ,level5_total_value = :get_level_total_value ";

                $param_edit += array("get_level_total_value" => $param['get_level_total_value']);
                array_push($col_type ,"integer");
            } else {
                $sql .= "    ,level5_total_value = null ";
            }

            if ($param['get_level_certificate_date'] != ''){
                $sql .= "    ,level5_certificate_date = :get_level_certificate_date ";

                $param_edit += array("get_level_certificate_date" => $param['get_level_certificate_date']);
                array_push($col_type ,"date");
            } else {
                $sql .= "    ,level5_certificate_date = null ";
            }
			//2012/05/15 K.Fujii ins(s)
			$sql .= "    ,get_level5_class = :get_level5_class ";
			$param_edit += array("get_level5_class" => $param['level_class']);
			array_push($col_type ,"integer");
			$sql .= "    ,get_level5_atrb = :get_level5_atrb ";
			$param_edit += array("get_level5_atrb" => $param['level_attribute']);
			array_push($col_type ,"integer");
			$sql .= "    ,get_level5_dept = :get_level5_dept ";
			$param_edit += array("get_level5_dept" => $param['level_dept']);
			array_push($col_type ,"integer");
			$sql .= "    ,get_level5_room = :get_level5_room ";
			$param_edit += array("get_level5_room" => $param['level_room']);
			array_push($col_type ,"integer");
			//2012/05/15 K.Fujii ins(e)
        }

        $sql .= "    ,delete_flg = :delete_flg ";
        $sql .= "    ,update_date = current_timestamp ";
        $sql .= "    ,update_user = :update_user ";
        $sql .= "WHERE emp_id = :emp_id";
        $this->log->info($sql);

        /**
         * Parameter
         */
        $param_edit += array("delete_flg" => 0);
        array_push($col_type ,"integer");
        $param_edit += array("update_user" => $this->user);
        array_push($col_type ,"text");
        $param_edit += array("emp_id" => $param["emp_id"]);
        array_push($col_type ,"text");

        /**
         * 実行
         */
        $rtn=parent::execute($sql,$col_type,$param_edit);

        $this->log->info("update_ladderlevel() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update_print($param) {

        $this->log->info("update_print() START");

        $ladder_data = $param["ladder_data"];
        $ladder_data_array =split('@',$ladder_data);

        for($i=0;$i<count($ladder_data_array);$i++){

            $ladder_update_array =split(',',$ladder_data_array[$i]);

            if ($ladder_update_array[0] != '') {

                $sql  = "";
                $sql  = "UPDATE cl_personal_profile SET ";
                if ($ladder_update_array[3] == 'I') {
                    $sql .= "    level1_certificate_date = current_timestamp ";
                }
                if ($ladder_update_array[3] == 'II') {
                    $sql .= "    level2_certificate_date = current_timestamp ";
                }
                if ($ladder_update_array[3] == 'III') {
                    $sql .= "    level3_certificate_date = current_timestamp ";
                }
                if ($ladder_update_array[3] == 'IV') {
                    $sql .= "    level4_certificate_date = current_timestamp ";
                }
                if ($ladder_update_array[3] == 'V') {
                    $sql .= "    level5_certificate_date = current_timestamp ";
                }

                $sql .= "    ,update_date = current_timestamp ";
                $sql .= "    ,update_user = :update_user ";
                $sql .= "WHERE emp_id = :emp_id";

                if ($ladder_update_array[3] == 'I') {
                    $sql .= "   and level1_certificate_date is null ";
                }
                if ($ladder_update_array[3] == 'II') {
                    $sql .= "   and level2_certificate_date is null ";
                }
                if ($ladder_update_array[3] == 'III') {
                    $sql .= "   and level3_certificate_date is null ";
                }
                if ($ladder_update_array[3] == 'IV') {
                    $sql .= "   and level4_certificate_date is null ";
                }
                if ($ladder_update_array[3] == 'V') {
                    $sql .= "   and level5_certificate_date is null ";
                }
                $this->log->info($sql);

                /**
                 * Parameter
                 */
                $param_edit = array(
                    "update_user"   => $this->user
                    ,"emp_id"       => $ladder_update_array[0]
                );
                $col_type = array("text","text");

                /**
                 * 実行
                 */
                $rtn=parent::execute($sql,$col_type,$param_edit);

            }

        }

        $this->log->info("update_print() END");
        return $rtn;
    }

	//2012/05/14 K.Fujii ins(s)

	function select_profile_data($emp_id){

		$this->log->info("select_profile_data() START");

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	emp_id ";
		$sql .= " 	, now_level ";
		$sql .= " 	, get_level1_date ";
		$sql .= " 	, get_level2_date ";
		$sql .= " 	, get_level3_date ";
		$sql .= " 	, get_level4_date ";
		$sql .= " 	, get_level5_date ";
		$sql .= " 	, level1_total_value ";
		$sql .= " 	, level2_total_value ";
		$sql .= " 	, level3_total_value ";
		$sql .= " 	, level4_total_value ";
		$sql .= " 	, level5_total_value ";
		$sql .= " 	, level1_certificate_date ";
		$sql .= " 	, level2_certificate_date ";
		$sql .= " 	, level3_certificate_date ";
		$sql .= " 	, level4_certificate_date ";
		$sql .= " 	, level5_certificate_date ";
		$sql .= " 	, level1_levelup_recognize_id ";
		$sql .= " 	, level2_levelup_recognize_id ";
		$sql .= " 	, level3_levelup_recognize_id ";
		$sql .= " 	, level4_levelup_recognize_id ";
		$sql .= " 	, level5_levelup_recognize_id ";
		$sql .= " FROM ";
		$sql .= " 	cl_personal_profile ";
		$sql .= " WHERE ";
		$sql .= " 	emp_id = :emp_id ";

		$data=parent::select($sql,array("text"),array("emp_id"=>$emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("select_profile_data() END data:".print_r($data,true));
		$this->log->debug("select_profile_data() END count():".count($data));
		return $data;

		$this->log->info("select_profile_data() START");

	}

	function get_level_data_insert($param){
		$this->log->info("get_level_data_insert() START");

		$sql = "";
		$sql .= " INSERT INTO cl_personal_profile( ";
		$sql .= "             emp_id ";
		$sql .= "             , now_level ";
		$sql .= "             , get_level1_date ";
		$sql .= "             , get_level2_date ";
		$sql .= "             , get_level3_date ";
		$sql .= "             , get_level4_date ";
		$sql .= "             , get_level5_date ";
		$sql .= "             , level1_total_value ";
		$sql .= "             , level2_total_value ";
		$sql .= "             , level3_total_value ";
		$sql .= "             , level4_total_value ";
		$sql .= "             , level5_total_value ";
		$sql .= "             , level1_certificate_date ";
		$sql .= "             , level2_certificate_date ";
		$sql .= "             , level3_certificate_date ";
		$sql .= "             , level4_certificate_date ";
		$sql .= "             , level5_certificate_date ";
		$sql .= "             , level1_levelup_recognize_id ";
		$sql .= "             , level2_levelup_recognize_id ";
		$sql .= "             , level3_levelup_recognize_id ";
		$sql .= "             , level4_levelup_recognize_id ";
		$sql .= "             , level5_levelup_recognize_id ";
		$sql .= "             , get_level1_class ";
		$sql .= "             , get_level2_class ";
		$sql .= "             , get_level3_class ";
		$sql .= "             , get_level4_class ";
		$sql .= "             , get_level5_class ";
		$sql .= "             , get_level1_atrb ";
		$sql .= "             , get_level2_atrb ";
		$sql .= "             , get_level3_atrb ";
		$sql .= "             , get_level4_atrb ";
		$sql .= "             , get_level5_atrb ";
		$sql .= "             , get_level1_dept ";
		$sql .= "             , get_level2_dept ";
		$sql .= "             , get_level3_dept ";
		$sql .= "             , get_level4_dept ";
		$sql .= "             , get_level5_dept ";
		$sql .= "             , get_level1_room ";
		$sql .= "             , get_level2_room ";
		$sql .= "             , get_level3_room ";
		$sql .= "             , get_level4_room ";
		$sql .= "             , get_level5_room ";
		$sql .= "             , delete_flg ";
		$sql .= "             , create_date ";
		$sql .= "             , create_user) ";
		$sql .= "     VALUES ( ";
		$sql .= "             :emp_id ";
		$sql .= "             , :now_level ";
		$sql .= "             , :get_level1_date ";
		$sql .= "             , :get_level2_date ";
		$sql .= "             , :get_level3_date ";
		$sql .= "             , :get_level4_date ";
		$sql .= "             , :get_level5_date ";
		$sql .= "             , :level1_total_value ";
		$sql .= "             , :level2_total_value ";
		$sql .= "             , :level3_total_value ";
		$sql .= "             , :level4_total_value ";
		$sql .= "             , :level5_total_value ";
		$sql .= "             , :level1_certificate_date ";
		$sql .= "             , :level2_certificate_date ";
		$sql .= "             , :level3_certificate_date ";
		$sql .= "             , :level4_certificate_date ";
		$sql .= "             , :level5_certificate_date ";
		$sql .= "             , :level1_levelup_recognize_id ";
		$sql .= "             , :level2_levelup_recognize_id ";
		$sql .= "             , :level3_levelup_recognize_id ";
		$sql .= "             , :level4_levelup_recognize_id ";
		$sql .= "             , :level5_levelup_recognize_id ";
		$sql .= "             , :get_level1_class ";
		$sql .= "             , :get_level2_class ";
		$sql .= "             , :get_level3_class ";
		$sql .= "             , :get_level4_class ";
		$sql .= "             , :get_level5_class ";
		$sql .= "             , :get_level1_atrb ";
		$sql .= "             , :get_level2_atrb ";
		$sql .= "             , :get_level3_atrb ";
		$sql .= "             , :get_level4_atrb ";
		$sql .= "             , :get_level5_atrb ";
		$sql .= "             , :get_level1_dept ";
		$sql .= "             , :get_level2_dept ";
		$sql .= "             , :get_level3_dept ";
		$sql .= "             , :get_level4_dept ";
		$sql .= "             , :get_level5_dept ";
		$sql .= "             , :get_level1_room ";
		$sql .= "             , :get_level2_room ";
		$sql .= "             , :get_level3_room ";
		$sql .= "             , :get_level4_room ";
		$sql .= "             , :get_level5_room ";
		$sql .= "             , 0 ";
		$sql .= "             , current_timestamp ";
		$sql .= "             , :create_user) ";

		$param["create_user"] = $this->user;

		$type = array(
				"text"
				,"integer"
				,"date"
				,"date"
				,"date"
				,"date"
				,"date"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"date"
				,"date"
				,"date"
				,"date"
				,"date"
				,"text"
				,"text"
				,"text"
				,"text"
				,"text"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"integer"
				,"text"
				);

		/**
		* 実行
		*/
		$rtn=parent::execute($sql,$type,$param);

		$this->log->info("get_level_data_insert() END");

	}

	function get_level_data_update($param){

		$this->log->info("get_level_data_update() START");

		$sql = "";
		$sql .= " UPDATE cl_personal_profile ";
		$sql .= "    SET now_level = :now_level ";
		$sql .= " 	, get_level1_date = :get_level1_date ";
		$sql .= " 	, get_level2_date = :get_level2_date ";
		$sql .= " 	, get_level3_date = :get_level3_date ";
		$sql .= " 	, get_level4_date = :get_level4_date ";
		$sql .= " 	, get_level5_date = :get_level5_date ";
		$sql .= " 	, level1_total_value = :level1_total_value ";
		$sql .= " 	, level2_total_value = :level2_total_value ";
		$sql .= " 	, level3_total_value = :level3_total_value ";
		$sql .= " 	, level4_total_value = :level4_total_value ";
		$sql .= " 	, level5_total_value = :level5_total_value ";
		$sql .= " 	, level1_certificate_date = :level1_certificate_date ";
		$sql .= " 	, level2_certificate_date = :level2_certificate_date ";
		$sql .= " 	, level3_certificate_date = :level3_certificate_date ";
		$sql .= " 	, level4_certificate_date = :level4_certificate_date ";
		$sql .= " 	, level5_certificate_date = :level5_certificate_date ";
		$sql .= " 	, level1_levelup_recognize_id = :level1_levelup_recognize_id ";
		$sql .= " 	, level2_levelup_recognize_id = :level2_levelup_recognize_id ";
		$sql .= " 	, level3_levelup_recognize_id = :level3_levelup_recognize_id ";
		$sql .= " 	, level4_levelup_recognize_id = :level4_levelup_recognize_id ";
		$sql .= " 	, level5_levelup_recognize_id = :level5_levelup_recognize_id ";
		$sql .= " 	, get_level1_class = :get_level1_class ";
		$sql .= " 	, get_level2_class = :get_level2_class ";
		$sql .= " 	, get_level3_class = :get_level3_class ";
		$sql .= " 	, get_level4_class = :get_level4_class ";
		$sql .= " 	, get_level5_class = :get_level5_class ";
		$sql .= " 	, get_level1_atrb = :get_level1_atrb ";
		$sql .= " 	, get_level2_atrb = :get_level2_atrb ";
		$sql .= " 	, get_level3_atrb = :get_level3_atrb ";
		$sql .= " 	, get_level4_atrb = :get_level4_atrb ";
		$sql .= " 	, get_level5_atrb = :get_level5_atrb ";
		$sql .= " 	, get_level1_dept = :get_level1_dept ";
		$sql .= " 	, get_level2_dept = :get_level2_dept ";
		$sql .= " 	, get_level3_dept = :get_level3_dept ";
		$sql .= " 	, get_level4_dept = :get_level4_dept ";
		$sql .= " 	, get_level5_dept = :get_level5_dept ";
		$sql .= " 	, get_level1_room = :get_level1_room ";
		$sql .= " 	, get_level2_room = :get_level2_room ";
		$sql .= " 	, get_level3_room = :get_level3_room ";
		$sql .= " 	, get_level4_room = :get_level4_room ";
		$sql .= " 	, get_level5_room = :get_level5_room ";
		$sql .= " 	, delete_flg = 0 ";
		$sql .= " 	, update_date = current_timestamp ";
		$sql .= " 	, update_user = :update_user ";
		$sql .= " WHERE ";
		$sql .= " 	emp_id = :emp_id ";

		$param["update_user"] = $this->user;

		$type = array(
			"integer"
			,"date"
			,"date"
			,"date"
			,"date"
			,"date"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"date"
			,"date"
			,"date"
			,"date"
			,"date"
			,"text"
			,"text"
			,"text"
			,"text"
			,"text"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"integer"
			,"text"
			,"text"
			);

		/**
		* 実行
		*/
		$rtn=parent::execute($sql,$type,$param);

		$this->log->info("get_level_data_update() END");

	}
	//2012/05/14 K.Fujii ins(e)

}
