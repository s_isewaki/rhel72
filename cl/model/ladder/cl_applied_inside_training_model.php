<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");
/**
 * 院内研修申請済み　ＤＢモデル
 *
 */
class cl_applied_inside_training_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /** 登録用SQL */
    // 2012/11/20 Yamagawa upd(s)
    //var $insert_sql = "INSERT INTO cl_applied_inside_training VALUES ( :training_apply_id, :last_training_apply_id, :emp_id, :charange_level, :inside_training_id, :plan_id, :inside_training_date, :change_reason, :learn_memo, :recommend_flag, :delete_flg, current_timestamp, :create_user )";
    var $insert_sql = "INSERT INTO cl_applied_inside_training (training_apply_id, last_training_apply_id, emp_id, charange_level, inside_training_id, plan_id, inside_training_date, change_reason, learn_memo, recommend_flag, delete_flg, create_date, create_user, year) VALUES ( :training_apply_id, :last_training_apply_id, :emp_id, :charange_level, :inside_training_id, :plan_id, :inside_training_date, :change_reason, :learn_memo, :recommend_flag, :delete_flg, current_timestamp, :create_user, :year)";
    // 2012/11/20 Yamagawa upd(e)

    /** 更新用SQL */
    // 2012/11/20 Yamagawa upd(s)
    //var $update_sql = "UPDATE cl_applied_inside_training SET last_training_apply_id=:last_training_apply_id, emp_id=:emp_id, charange_level=:charange_level, inside_training_id=:inside_training_id, plan_id=:plan_id, inside_training_date=:inside_training_date, change_reason=:change_reason, learn_memo=:learn_memo, recommend_flag=:recommend_flag, delete_flg=:delete_flg, update_date=current_timestamp, update_user=:update_user WHERE training_apply_id=:training_apply_id";
    var $update_sql = "UPDATE cl_applied_inside_training SET last_training_apply_id=:last_training_apply_id, emp_id=:emp_id, charange_level=:charange_level, inside_training_id=:inside_training_id, plan_id=:plan_id, inside_training_date=:inside_training_date, change_reason=:change_reason, learn_memo=:learn_memo, recommend_flag=:recommend_flag, delete_flg=:delete_flg, update_date=current_timestamp, update_user=:update_user, year=:year WHERE training_apply_id=:training_apply_id";
    // 2012/11/20 Yamagawa upd(e)

    /** 物理削除用SQL */
    var $physical_delete_sql = "DELETE FROM cl_applied_inside_training WHERE training_apply_id = :training_apply_id ";

    /** 論理削除用SQL */
    var $logical_delete_sql = "UPDATE cl_applied_inside_training SET delete_flg = :delete_flg , update_date=current_timestamp,update_user = :update_user WHERE training_apply_id = :training_apply_id";

    /** 一覧取得用SQL */
    var $select_list_sql = "SELECT * FROM cl_applied_inside_training ORDER BY create_date";

    /** レコード取得用SQL */
    var $select_rec_sql = "SELECT * FROM cl_applied_inside_training LEFT JOIN cl_mst_inside_training ON cl_applied_inside_training.inside_training_id = cl_mst_inside_training.training_id WHERE training_apply_id = :training_apply_id";

    /** レコード取得用SQL */
    var $select_rec_sql_by_apply_id = "SELECT * FROM cl_applied_inside_training LEFT JOIN cl_mst_inside_training ON cl_applied_inside_training.inside_training_id = cl_mst_inside_training.training_id WHERE apply_id = :apply_id";

    /** 研修講座管理(受付一覧)取得用SQL */
    var $select_reception_list_sql = "SELECT apl.emp_id, emp.emp_lt_nm || '　' || emp.emp_ft_nm as emp_name, apl.inside_training_date, case emp.emp_room when null then class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm else class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm || ' > ' || room.room_nm end as affiliation, roll.emp_roll FROM cl_applied_inside_training apl inner join empmst emp on apl.emp_id = emp.emp_id left join classmst class on emp.emp_class = class.class_id left join atrbmst atrb on emp.emp_attribute = atrb.atrb_id left join deptmst dept on emp.emp_dept = dept.dept_id left join classroom room on emp.emp_room = room.room_id left join cl_inside_training_roll roll on apl.plan_id = roll.plan_id and apl.inside_training_id = roll.training_id and apl.emp_id = roll.emp_id WHERE apl.plan_id LIKE :plan_id";

    /** 登録用カラムタイプ */
    // 2012/11/20 Yamagawa upd(s)
    //var $insert_col_type = array('text', 'text', 'text', 'integer', 'text', 'text', 'date', 'text', 'text', 'integer', 'integer', 'text');
    var $insert_col_type = array('text', 'text', 'text', 'integer', 'text', 'text', 'date', 'text', 'text', 'integer', 'integer', 'text', 'integer');
    // 2012/11/20 Yamagawa upd(e)

    /** 更新用カラムタイプ */
    // 2012/11/20 Yamagawa upd(s)
    //var $update_col_type = array('text', 'text', 'integer', 'text', 'text', 'date', 'text', 'text', 'integer', 'integer', 'text', 'text');
    var $update_col_type = array('text', 'text', 'integer', 'text', 'text', 'date', 'text', 'text', 'integer', 'integer', 'text', 'text', 'integer');
    // 2012/11/20 Yamagawa upd(e)

    /** 物理削除用カラムタイプ */
    var $physical_delete_col_type = array('text');

    /** 論理削除用カラムタイプ */
    var $logical_delete_col_type = array('integer','text','text');

    /**
     * Constractor
     * @param type $p_con MDB2 接続情報
     * @param type $p_login_user　login user
     */
    function cl_inside_training_apply_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * insert
     * @param type $param
     */
    function insert($param) {
        $this->log->info("insert() START");

        /**
         * Parameter
         */
        $param["delete_flg"]=0;
        $param["create_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->insert_sql,$this->insert_col_type,$param);

        $this->log->info("insert() END");
        return $rtn;
    }

    /**
     * update
     * @param type $param
     */
    function update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * 代理用 update
     * @param type $param
     */
    function sub_update($param) {

        $this->log->info("update() START");

        /**
         * Parameter
         */
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->update_sql2,$this->update_col_type,$param);

        $this->log->info("update() END");

        return $rtn;
    }

    /**
     * physical_delete
     * @param type $training_apply_id
     */
    function physical_delete($id) {

        $this->log->info("physical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["training_apply_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        //$rtn=$this->execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);
        $rtn=parent::execute($this->physical_delete_sql,$this->physical_delete_col_type,$param);

        $this->log->info("physical_delete() END");

        return $rtn;
    }

    /**
     * logical_delete
     * @param type $training_apply_id
     */
    function logical_delete($id) {

        $this->log->info("logical_delete() START");

        /**
         * Parameter
         */
        $param=array();
        $param["delete_flg"]=1;
        $param["training_apply_id"]=$id;
        $param["update_user"]=$this->user;

        /**
         * 実行
         */
        $rtn=parent::execute($this->logical_delete_sql,$this->logical_delete_col_type,$param);

        $this->log->info("logical_delete() END");

        return $rtn;
    }

    /**
     * list
     * @param type $training_apply_id
     */
    function getList(){

        $this->log->info("select() START");
        //$select_sql = "SELECT apply_id,charange_level,inside_training_id,inside_training_date,learn_memo,delete_flg,create_date,create_user,update_date,update_user FROM cl_inside_training_apply ORDER BY create_date";
        $data=parent::select($this->select_list_sql,null,array(),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data;

     }

    /**
     * select
     * @param type $training_apply_id
     */
    function select($id){

        $this->log->info("select() START  training_apply_id = " . $id);
        $data=parent::select($this->select_rec_sql,array("text"),array("training_apply_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select() END");
        return $data[0];

     }

    /**
     * select by apply_id
     * @param String $apply_id
     */
    function select_by_apply_id($id){

        $this->log->info("select_by_apply_id() START");
        $data=parent::select($this->select_rec_sql_by_apply_id,array("text"),array("apply_id"=>$id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("select_by_apply_id() END");
        return $data[0];

     }

    /**
     * getReceptionList
     * @param string $plan_id
     */
    function getReceptionList($plan_id){
    	$plan_id = "%". $plan_id . "%";
        $this->log->debug("getReceptionList() START   plan_id = ". $plan_id);
//        $data=parent::select($this->select_reception_list_sql,$this->select_reception_list_col_type,$plan_id,MDB2_FETCHMODE_ASSOC);
        $data=parent::select($this->select_reception_list_sql,array("text"),array("plan_id"=>$plan_id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("getReceptionList() END");
        return $data;

    }

     /**
      * 院内研修申請が承認されることにより、
      * 申請書の内容から申込済み院内研修を作成するためのインサート処理
      *
      * @param $training_apply_id 院内研修申請の申請ID
      * @param $login_emp_id ログインユーザーのemp_id
      */
     function insert_from_apply_template($training_apply_id,$login_emp_id)
     {
		$this->log->debug("insert_from_apply_template() START");
		$this->log->info("insert_from_apply_template() training_apply_id:".$training_apply_id);
		$this->log->info("insert_from_apply_template() login_emp_id:".$login_emp_id);

		//SQL作成
		$sql = "";
		$sql .= " INSERT INTO cl_applied_inside_training";
		$sql .= " SELECT";
		$sql .= " training_apply_id,";
		$sql .= " training_apply_id AS last_training_apply_id,";
		$sql .= " emp_id,";
		$sql .= " charange_level,";
		$sql .= " inside_training_id,";
		$sql .= " plan_id,";
		$sql .= " inside_training_date,";
		$sql .= " change_reason,";
		$sql .= " learn_memo,";
		$sql .= " recommend_flag,";
		$sql .= " 0 AS delete_flg,";
		$sql .= " current_timestamp AS create_date,";
		$sql .= " :login_emp_id AS create_user,";
		$sql .= " current_timestamp AS update_date,";
		$sql .= " :login_emp_id AS update_user";
		// 2012/11/26 Yamagawa add(s)
		$sql .= " ,year ";
		// 2012/11/26 Yamagawa add(e)
		$sql .= " FROM cl_apl_inside_training_apply";
		$sql .= " WHERE training_apply_id = :training_apply_id";

		//パラメータのカラムタイプ
		$param_col_type = array('text', 'text');

		//パラメータ
		$param["training_apply_id"]=$training_apply_id;
		$param["login_emp_id"]=$login_emp_id;

		//実行
		$rtn=parent::execute($sql,$param_col_type,$param);

		$this->log->debug("insert_from_apply_template() END");
		return $rtn;
     }

     /**
      * 院内研修申請変更が承認されることにより、
      * 申請書の内容から申込済み研修を更新するためのアップデート処理
      *
      * @param $training_apply_id 院内研修申請変更の申請ID
      * @param $login_emp_id ログインユーザーのemp_id
      */
     function update_from_apply_modify_template($training_apply_id,$login_emp_id)
     {
		$this->log->info("update_from_apply_modify_template() START");
		$this->log->info("update_from_apply_modify_template() training_apply_id:".$training_apply_id);
		$this->log->info("update_from_apply_modify_template() login_emp_id:".$login_emp_id);

		//SQL作成
		$sql = "";
		$sql .= " UPDATE cl_applied_inside_training";
		$sql .= " SET";
		$sql .= " last_training_apply_id = A.training_apply_id,";
		$sql .= " plan_id = A.plan_id,";
		$sql .= " change_reason = A.change_reason,";
		$sql .= " learn_memo = A.learn_memo,";
		$sql .= " recommend_flag = A.recommend_flag,";
		$sql .= " delete_flg = A.applied_delete_flg,";
		$sql .= " update_date = current_timestamp,";
		$sql .= " update_user = :login_emp_id";
		$sql .= " FROM";
		$sql .= " (";
		$sql .= " SELECT";
		$sql .= " training_apply_id,";
		$sql .= " first_apply_id,";
		$sql .= " plan_id,";
		$sql .= " change_reason,";
		$sql .= " learn_memo,";
		$sql .= " recommend_flag,";
		$sql .= " CASE WHEN update_division = 2 THEN 1 ELSE 0 END AS applied_delete_flg";
		$sql .= " FROM cl_apl_inside_training_apply";
		$sql .= " WHERE training_apply_id = :training_apply_id";
		$sql .= " ) A";
		$sql .= " WHERE";
		$sql .= " cl_applied_inside_training.training_apply_id = A.first_apply_id";

		//パラメータのカラムタイプ
		$param_col_type = array('text', 'text');

		//パラメータ
		$param["training_apply_id"]=$training_apply_id;
		$param["login_emp_id"]=$login_emp_id;

		//実行
		$rtn=parent::execute($sql,$param_col_type,$param);

		$this->log->debug("update_from_apply_modify_template() END");
		return $rtn;
     }


}
$log->info(basename(__FILE__)." END");
