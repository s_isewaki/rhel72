<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");

/**
 * ���⸦���з�ID ��������
 */
class cl_inside_training_roll_id_seq_model extends cl_common_model {
    var $log;

    /**
     * Constractor
     */
    function cl_inside_training_roll_id_seq_model($p_con,$p_user) {
        parent::cl_common_model($p_con,$p_user);
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * getInsideTrainingRollID
     */
    function getInsideTrainingRollID() {
        $this->log->info("getInsideTrainingRollID() START");
        $id=parent::getId("TRO","cl_inside_training_roll_id_seq");
        $this->log->info("getInsideTrainingRollID() END id=".$id);
        return $id;
    }
}
?>
