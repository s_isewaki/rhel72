<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");

class cl_levelup_apply_inside_training_id_seq_model extends cl_common_model {

    var $log;

    /**
     * Constractor
     */
    function cl_levelup_apply_inside_training_id_seq_model($p_con,$p_user) {
        parent::cl_common_model($p_con,$p_user);
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * getSeqId
     */
    function getSeqId() {
        $this->log->info("getSeqId() START");
        $id=parent::getId("LAT","cl_levelup_apply_inside_training_id_seq");
        $this->log->info("getSeqId() END id=".$id);
        return $id;
    }

}
