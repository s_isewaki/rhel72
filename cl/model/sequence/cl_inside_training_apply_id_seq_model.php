<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");

/**
 * ���⸦������ID ��������
 */
class cl_inside_training_apply_id_seq_model extends cl_common_model {
    var $log;

    /**
     * Constractor
     */
    function cl_inside_training_apply_id_seq_model($p_con,$p_user) {
        parent::cl_common_model($p_con,$p_user);
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * getInTraApplyID
     */
    function getInsideTrainingApplyId() {
        $this->log->info("get Inside Training ApplyId() START");
        $id=parent::getId("TAL","cl_inside_training_apply_id_seq");
        $this->log->info("get Inside Training ApplyId() END id=".$id);
        return $id;
    }
}
?>
