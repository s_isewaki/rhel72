<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");

class cl_wkfw_id_seq_model extends cl_common_model {

    var $log;

    /**
     * Constractor
     */
    function cl_wkfw_id_seq_model($p_con,$p_user) {
        parent::cl_common_model($p_con,$p_user);
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * getApplyId
     */
    function getId() {
        $this->log->info("getId() START");
        $id=parent::getId("WKF","cl_wkfw_id_seq");
        $this->log->info("getId() END apply_id=".$id);
        return $id;
    }

}
