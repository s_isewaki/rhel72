<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");

class cl_nurse_deputy_manager_id_seq_model extends cl_common_model {

    var $log;

    /**
     * Constractor
     */
    function cl_nurse_deputy_manager_id_seq_model($p_con,$p_user) {
        parent::cl_common_model($p_con,$p_user);
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * getId
     */
    function getId() {
        $this->log->info("getId() START");
        $nurse_deputy_manager_id=parent::getId("NDM","cl_nurse_deputy_manager_id_seq");
        $this->log->info("getId() END nurse_deputy_manager_id=".$nurse_deputy_manager_id);
        return $nurse_deputy_manager_id;
    }

}
