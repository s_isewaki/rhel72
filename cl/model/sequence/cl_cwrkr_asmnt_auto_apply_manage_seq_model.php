<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");

class cl_cwrkr_asmnt_auto_apply_manage_seq_model extends cl_common_model {

    var $log;

    /**
     * Constractor
     */
    function cl_cwrkr_asmnt_auto_apply_manage_seq_model($p_con,$p_user) {
        parent::cl_common_model($p_con,$p_user);
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * getSeqId
     */
    function getSeqId() {
        $this->log->info("getSeqId() START");
        $id=parent::getId("CAM","cl_cwrkr_asmnt_auto_apply_manage_seq");
        $this->log->info("getSeqId() END id=".$id);
        return $id;
    }

}
