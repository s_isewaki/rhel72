<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");

/**
 * 院内研修マスタID シーケンス
 */
class cl_inside_training_id_seq_model extends cl_common_model {

    var $log;

    /**
     * Constractor
     */
    function cl_inside_training_id_seq_model($p_con,$p_user) {
        parent::cl_common_model($p_con,$p_user);
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * getInTrainingID
     */
    function getInTrainingId() {
        $this->log->info("getInTrainingId() START");
        $id=parent::getId("TRA","cl_intraining_id_seq");
        $this->log->info("getInTrainingId() END id=".$id);
        return $id;
    }

}
?>
