<?php
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
require_once('MDB2.php');
require_once('PEAR.php');
require_once(dirname(__FILE__) . "/../../cl_common.ini");

PEAR::setErrorHandling(PEAR_ERROR_CALLBACK, 'sqlError');

//define("SQL_SUCCESS",0);
//define("SQL_ERROR",9);

class cl_common_model {

    var $con;
    var $log;
    var $user;
    var $seq_sql = "SELECT 'CL' || :app_prefix || LPAD( cast(NEXTVAL( :seq_name ) as varchar(8)), 8, '0')";

    /**
     * Constractor
     */
    function cl_common_model($p_con,$p_user) {
        $this->con = $p_con;
        $this->user=$p_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    function setCon($p_con) {
        $this->con = $p_con;
    }
    function setUser($p_user) {
        $this->user = $p_user;
    }
    function getCon() {
        return $this->con;
    }
    function getUser() {
        return $this->user;
    }
    /**
     * execute
     * @param type $param
     */
    function execute($sql,$type,$param) {

        $this->log->info("execute() START");

		$this->log->debug('★$this->user:'.$this->user,__FILE__,__LINE__);

		foreach( $param as $key => $value ) {
			$this->log->debug("key:".$key." value:".$value,__FILE__,__LINE__);
		}

		/**
         * chkPlaceholderCount
         */
		if( $this->chkPlaceholderCount($sql,$type) == false){
        	return SQL_ERROR;
        }


        /**
         * chkParameterCount
         */
        if( $this->chkParameterCount($param,$type) == false){
        	return SQL_ERROR;
        }


        /**
         * Statement
         */
        $this->log->info("Statement START");
        $stmt = $this->con->prepare($sql, $type, MDB2_PREPARE_MANIP);
        //$stmt = $this->con->prepare($sql, null, MDB2_PREPARE_MANIP);
        $this->log->info("Statement END");

        $this->log->debug("execute start");
        $res = $stmt->execute($param);
        if(PEAR::isError($res)){
            $this->log->error("エラー詳細：".$res->getDebugInfo(),__FILE__,__LINE__);
            $this->log->error("execute error");
            echo '$res->getDebugInfo():'.$res->getDebugInfo();
            return SQL_ERROR;
       	}
        $this->log->debug("execute end ".$res,__FILE__,__LINE__);
        $stmt->free();
        $this->log->info("execute() END");
        return SQL_SUCCESS;
    }

    /**
     * select
     * @param type $apply_id
     */
    function select($sql,$type,$param,$fetch_mode){

        $this->log->info("select() START");

		/**
         * chkPlaceholderCount
         */
        if( $this->chkPlaceholderCount($sql,$type) == false){
        	return SQL_ERROR;
        }
        /**
         * chkParameterCount
         */
        if( $this->chkParameterCount($param,$type) == false){
        	return SQL_ERROR;
        }

//foreach( $param as $key => $value ) {
//	$this->log->debug("key:".$key,__FILE__,__LINE__);
//}
        $this->log->debug("setFetchMode() START",__FILE__,__LINE__);
        $this->con->setFetchMode($fetch_mode);
        $this->log->debug("setFetchMode() END",__FILE__,__LINE__);

        /**
         * 実行
         */
        $stmt = $this->con->prepare($sql, $type, MDB2_PREPARE_RESULT);

        $rows = $stmt->execute($param);
        $this->log->debug("execute END",__FILE__,__LINE__);
        if(PEAR::isError($rows)){
            $this->log->error("エラー詳細：".$rows->getDebugInfo(),__FILE__,__LINE__);
            $this->log->error("execute error",__FILE__,__LINE__);
            return SQL_ERROR;
        }

        $this->log->debug("res->numRows():".$rows->numRows(),__FILE__,__LINE__);

        $this->log->debug("fetchAll START",__FILE__,__LINE__);
        $data = $rows->fetchAll();
        $this->log->debug("fetchAll END",__FILE__,__LINE__);

        $this->log->debug("select() END",__FILE__,__LINE__);

        return $data;
    }

    /**
     * getList
     * @param type $apply_id
     */
    function getList($sql,$type,$param,$fetch_mode){

        $this->log->info("getList() START");

		$this->log->debug("setFetchMode(MDB2_FETCHMODE_ASSOC) START",__FILE__,__LINE__);
        $this->con->setFetchMode($fetch_mode);
        $this->log->debug("setFetchMode(MDB2_FETCHMODE_ASSOC) END",__FILE__,__LINE__);

        /**
         * 実行
         */
        $stmt = $this->con->prepare($sql, $type, MDB2_PREPARE_RESULT);

        $rows = $stmt->execute($param);
        $this->log->debug("execute END",__FILE__,__LINE__);
        if(PEAR::isError($rows)){
            $this->log->error("エラー詳細：".$rows->getDebugInfo(),__FILE__,__LINE__);
            $this->log->error("execute error",__FILE__,__LINE__);
            return SQL_ERROR;
        }

        $this->log->debug("res->numRows():".$rows->numRows(),__FILE__,__LINE__);

        $this->log->debug("fetchAll START",__FILE__,__LINE__);
        $data = $rows->fetchAll();
        $this->log->debug("fetchAll END",__FILE__,__LINE__);

        $this->log->debug("getList() END",__FILE__,__LINE__);

        return $data;
    }
    /**
     * getId
     */
    function getId($app_prefix,$seq_name) {

		$this->log->info("getId() START");
		$this->log->debug("app_prefix:".$app_prefix,__FILE__,__LINE__);
		$this->log->debug("seq_name:".$seq_name,__FILE__,__LINE__);

		/**
         * Statement
         */
		$this->log->debug("Statement START",__FILE__,__LINE__);
        $stmt = $this->con->prepare($this->seq_sql, array("text","text"), MDB2_PREPARE_RESULT);
        $this->log->debug("Statement END",__FILE__,__LINE__);

        $param=array();
        $param["app_prefix"]=$app_prefix;
        $param["seq_name"]=$seq_name;
        $this->log->debug("execute START",__FILE__,__LINE__);
        $rows = $stmt->execute($param);
        $this->log->debug("execute END",__FILE__,__LINE__);
        if(PEAR::isError($rows)){
            $this->log->error("エラー詳細：".$rows->getDebugInfo(),__FILE__,__LINE__);
            $this->log->error("execute END",__FILE__,__LINE__);
            return SQL_ERROR;
        }
        $id=$rows->fetchOne();
        $stmt->free();
        $this->log->info("getId() END",__FILE__,__LINE__);
        return $id;

    }

    /**
     * chkPlaceholderCount
     * @param type $sql
     * @param type $type
     */
    function chkPlaceholderCount($sql,$type){

        $this->log->info("chkPlaceholderCount() START");
		$this->log->debug('SQL:'.$sql,__FILE__,__LINE__);
        $rtn=true;
		$ps=split('\:',$sql);
		$pcount=count($ps) - 1;
		$tcount=count($type);
		if($pcount != $tcount){
            //$this->log->error("SQLのプレースホルダの数が、カラム属性の数と一致していない可能性があります。",__FILE__,__LINE__);
            $this->log->warn("SQLのプレースホルダの数が、カラム属性の数と一致していない可能性があります。",__FILE__,__LINE__);
            $rtn=false;
		}
        $this->log->info("chkPlaceholderCount() END pcount:".$pcount." tcount:".$tcount." rtn:".$rtn,__FILE__,__LINE__);

    	//TODO 単純にSQL中の「:」の数とパラメータの数が食い違っているかを判定してるため、SQL中に同じキーが登場するとエラーになる。要対応。
        //return $rtn;
		return true;

     }

    /**
     * chkPlaceholderCount
     * @param type $sql
     * @param type $type
     */
    function chkParameterCount($param,$type){

        $this->log->info("chkParameterCount() START");

		//$sql_img = $this->createSqlImage($sql,$param);
		//$this->log->debug('SQL_IMAGE:'.$sql_img,__FILE__,__LINE__);

        $rtn=true;
		$pcount=count($param);
		$tcount=count($type);
		if($pcount != $tcount){
            $this->log->error("パラメータの数が、SQLのプレースホルダの数と一致していません。",__FILE__,__LINE__);
            $rtn=false;
		}
        $this->log->info("chkParameterCount() END pcount:".$pcount." tcount:".$tcount." rtn:".$rtn,__FILE__,__LINE__);

		return $rtn;

     }

    /**
     * createSqlImage
     * @param type $sql
     * @param type $type
     * @param type $param
     */
    function createSqlImage($sql,$param){

        $this->log->info(__FUNCTION__.' START');


//str_replace("置換対象の文字列","置換する文字列","最初の文字列");

        $i=0;
        $str=$sql;
        foreach( $param as $key => $val ){
            //$this->log->debug('$key:'.$key.' $val:'.$val,__FILE__,__LINE__);
        	$str=str_replace(":".$key,"'".$val."'",$str);
            //$this->log->debug('$str:'.$str,__FILE__,__LINE__);
        }
        $this->log->info(__FUNCTION__.' END');
        return $str;



     }

     /**
     * getFirstRecord
     * Enter description here ...
     * @param unknown_type $sql
     * @param unknown_type $type
     * @param unknown_type $param
     * @param unknown_type $fetch_mode
     * @return Ambigous <NULL, unknown>
     */
    function getRecord($sql,$type,$param,$fetch_mode,$row){

        $this->log->info("getFirstRecord() START");
    	$data=$this->select($sql,$type,$param,$fetch_mode);
        $this->log->info("getFirstRecord() END");
    	return $data[$row];
    }

}
	function sqlError($error){
        //$this->log->info(__FUNCTION__." START");
		$log = new cl_common_log_class(basename(__FILE__,'.php'));
		$log->error($error->getMessage(),__FILE__,__LINE__);
		//$log->error($error,__FILE__,__LINE__);
		$log->error("<DB ERROR 詳細 START>",__FILE__,__LINE__);
		$log->error(print_r($error,true),__FILE__,__LINE__);
		$log->error("<DB ERROR 詳細 END>",__FILE__,__LINE__);
		//$log->error("backtracd:".print_r(debug_backtrace(),true),__FILE__,__LINE__);
		//$this->log->info(__FUNCTION__." END");

	}

