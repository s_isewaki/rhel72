<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class classmst_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function classmst_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	* 施設マスタ取得
	*/
	function getList(){
		$this->log->info("getList() START");

		//SQL作成
		$sql = "";
		$sql .= "SELECT";
		$sql .= " class_id,";
		$sql .= " class_nm ";
		$sql .= "FROM";
		$sql .= " classmst ";
		$sql .= "WHERE";
		$sql .= " class_del_flg = 'f' ";
		$sql .= "ORDER BY";
		$sql .= " order_no";

		$data=parent::select($sql,null,array(),MDB2_FETCHMODE_ASSOC);

		$this->log->info("getList() END");
		return $data;
	}

	/**
	* 所属施設マスタ取得（兼務込）
	*/
	function getList_add_conditions_emp_id($emp_id){
		$this->log->info("getList_add_conditions_emp_id() START");

		//SQL作成
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " class.class_id ";
		$sql .= " ,class.class_nm ";
		$sql .= "FROM ";
		$sql .= " classmst class ";
		$sql .= "WHERE ";
		$sql .= " class.class_del_flg = 'f' ";
		$sql .= " AND ( ";
		$sql .= "  EXISTS ( ";
		$sql .= "   SELECT ";
		$sql .= "    emp.emp_id ";
		$sql .= "   FROM ";
		$sql .= "    empmst emp ";
		$sql .= "   WHERE ";
		$sql .= "    emp.emp_id = :emp_id ";
		$sql .= "    AND emp.emp_class = class.class_id ";
		$sql .= "	) ";
		$sql .= "  OR EXISTS ( ";
		$sql .= "   SELECT ";
		$sql .= "    con.emp_id ";
		$sql .= "   FROM ";
		$sql .= "    concurrent con ";
		$sql .= "   WHERE ";
		$sql .= "    con.emp_id = :emp_id ";
		$sql .= "    AND con.emp_class = class.class_id ";
		$sql .= "  ) ";
		$sql .= " ) ";

		$data=parent::select($sql,array('text'),array('emp_id' => $emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->info("getList_add_conditions_emp_id() END");
		return $data;
	}

}
