<?php
/**
 * 研修講座一覧画面の一覧データ取得用モデル
 *
 *
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_applied_inside_training_plan_list_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_applied_inside_training_plan_list_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }




	/**
	* 院内研修日程一覧に必要なデータを取得します。
	*
	* @param $emp_id 研修の申請者
	*/
	function get_applied_training_plan_list($emp_id)
	{
		$this->log->debug("get_applied_training_plan_list() START");

		//SQL作成
		$sql = "";
		$sql .= " SELECT";
		$sql .= "   A.training_apply_id,";
		$sql .= "   A.last_training_apply_id,";
		$sql .= "   A.inside_training_date,";
		$sql .= "   A.learn_memo,";
		$sql .= "   A.recommend_flag,";
		$sql .= "   B.training_id,";
		$sql .= "   B.training_name,";
		$sql .= "   B.training_purpose,";
		$sql .= "   B.training_slogan,";
		$sql .= "   B.training_contents,";
		$sql .= "   C.plan_id,";
		$sql .= "   C.training_time,";
		$sql .= "   C.final_flg,";
		$sql .= "   C.plan_date,";
		$sql .= "   C.from_time,";
		$sql .= "   C.to_time,";
		$sql .= "   C.answer_division,";
		$sql .= "   case when D.emp_roll is null then 0 else D.emp_roll end as emp_roll,";
		$sql .= "   case when E.training_apply_id is null then 0 else 1 end as reported_flg";
		// 2012/11/02 Yamagawa add(s)
		$sql .= "   ,COALESCE(F.first_apply_id,'0') AS change_apply_flg ";
		// 2012/11/02 Yamagawa add(e)

		$sql .= " FROM";
		$sql .= " (";
		$sql .= "   SELECT * FROM cl_applied_inside_training WHERE emp_id = :emp_id AND delete_flg <> 1";
		$sql .= " ) A";

		$sql .= " INNER JOIN";
		$sql .= " (";
		$sql .= "   SELECT training_id, training_name, training_purpose, training_slogan, training_contents FROM cl_mst_inside_training";
		$sql .= " ) B";
		$sql .= " ON A.inside_training_id = B.training_id";

		$sql .= " INNER JOIN";
		$sql .= " (";
		$sql .= "   SELECT plan_id,training_time,final_flg,plan_date,from_time,to_time,answer_division FROM cl_mst_inside_training_schedule";
		$sql .= " ) C";
		$sql .= " ON A.plan_id like '%' || C.plan_id || '%'";

		$sql .= " LEFT JOIN";
		$sql .= " (";
		$sql .= "   SELECT plan_id, apply_id, emp_roll FROM cl_inside_training_roll WHERE emp_id = :emp_id AND delete_flg <> 1";
		$sql .= " ) D";
		$sql .= " ON C.plan_id = D.plan_id AND A.training_apply_id = D.apply_id";

		$sql .= " LEFT JOIN";
		$sql .= " (";
		$sql .= "   SELECT";
		$sql .= "   DISTINCT A.training_apply_id,A.plan_id";
		$sql .= "   FROM";
		$sql .= "   cl_apl_inside_training_report A";
		$sql .= "   INNER JOIN cl_apply B";
		$sql .= "   ON A.apply_id = B.apply_id ";
		$sql .= "   WHERE B.apply_stat in(0,1)";/* 0:申請中か1:申請済み(2:否認、3:差し戻し以外) */
		$sql .= "   AND not B.draft_flg";/* 下書き以外 */
		$sql .= "   AND not B.delete_flg";/* 申請取り消しなど以外 */
		$sql .= " ) E";
		$sql .= " ON A.training_apply_id = E.training_apply_id AND C.plan_id = E.plan_id";

		// 2012/11/02 Yamagawa add(s)
		// 申請中の院内研修変更申込を取得
		$sql .= " LEFT JOIN ";
		$sql .= "  (SELECT DISTINCT ";
		$sql .= "    A.first_apply_id ";
		$sql .= "   FROM ";
		$sql .= "    cl_apl_inside_training_apply A ";
		$sql .= "    INNER JOIN ";
		$sql .= "     cl_apply B ";
		$sql .= "    ON ";
		$sql .= "     B.apply_id = A.apply_id ";
		$sql .= "     AND B.apply_stat = 0 ";		// 申請中
		$sql .= "     AND B.delete_flg = FALSE ";	// 申請取消以外
		$sql .= "     AND B.draft_flg = FALSE ";	// 下書き以外
		$sql .= "   WHERE ";
		$sql .= "    A.delete_flg = 0 ";
		$sql .= "  ) F ";
		$sql .= " ON ";
		$sql .= "  F.first_apply_id = A.training_apply_id ";
		// 2012/11/02 Yamagawa add(e)

		$sql .= " ORDER BY A.inside_training_date, A.training_apply_id, C.training_time, C.plan_date, C.from_time, C.to_time, C.plan_id";

		$data=parent::select($sql,array("text"),array("emp_id"=>$emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_applied_training_plan_list() DB RETURN :".print_r($data,true));
		$this->log->debug("get_applied_training_plan_list() DB RETURN count(data):".count($data));

		$i_tra = -1;
		$i_plan = -1;
		$old_training_apply_id = "";
		$old_reported_flg = 0;
		$arr_training_list = array();
		for($i = 0; $i < count($data); $i++)//
		{
			$l_training_apply_id      = $data[$i]['training_apply_id'];
			$l_inside_training_date   = $data[$i]['inside_training_date'];
			$l_training_id            = $data[$i]['training_id'];
			$l_training_name          = $data[$i]['training_name'];
			$l_training_purpose       = $data[$i]['training_purpose'];
			$l_training_slogan        = $data[$i]['training_slogan'];
			$l_training_contents      = $data[$i]['training_contents'];
			$l_plan_id                = $data[$i]['plan_id'];
			$l_training_time          = $data[$i]['training_time'];
			$l_final_flg              = $data[$i]['final_flg'];
			$l_plan_date              = $data[$i]['plan_date'];
			$l_from_time              = $data[$i]['from_time'];
			$l_to_time                = $data[$i]['to_time'];
			$l_answer_division        = $data[$i]['answer_division'];
			$l_emp_roll               = $data[$i]['emp_roll'];
			$l_reported_flg           = $data[$i]['reported_flg'];
			// 2012/11/02 Yamagawa add(s)
			$l_change_apply_flg       = $data[$i]['change_apply_flg'];
			// 2012/11/02 Yamagawa add(e)

			$i_plan++;
			if($old_training_apply_id != $l_training_apply_id)
			{
				$i_tra++;
				$i_plan = 0;
			}

			//第１回の日程は過去の報告が不要
			if($l_training_time == 1)
			{
				$old_reported_flg = 1;
			}

			if($i_plan == 0)
			{
				$arr_training_list[$i_tra] = array();
				$arr_training_list[$i_tra]['training_apply_id']      = $l_training_apply_id;
				$arr_training_list[$i_tra]['inside_training_date']   = $l_inside_training_date;
				$arr_training_list[$i_tra]['training_id']            = $l_training_id;
				$arr_training_list[$i_tra]['training_name']          = $l_training_name;
				$arr_training_list[$i_tra]['training_purpose']       = $l_training_purpose;
				$arr_training_list[$i_tra]['training_slogan']        = $l_training_slogan;
				$arr_training_list[$i_tra]['training_contents']      = $l_training_contents;
				// 2012/11/02 Yamagawa add(s)
				$arr_training_list[$i_tra]['change_apply_flg']       = $l_change_apply_flg;
				// 2012/11/02 Yamagawa add(e)
				$arr_training_list[$i_tra]['plan_list']              = array();
			}
			$arr_training_list[$i_tra]['plan_list'][$i_plan] = array();
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['plan_id']   = $l_plan_id;
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['training_time'] = $l_training_time;
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['final_flg'] = $l_final_flg;
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['plan_date'] = $l_plan_date;
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['from_time'] = $l_from_time;
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['to_time']   = $l_to_time;
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['answer_division'] = $l_answer_division;
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['emp_roll']   = $l_emp_roll;
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['reported_flg']   = $l_reported_flg;
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['old_reported_flg']   = $old_reported_flg;

			$old_training_apply_id = $l_training_apply_id;

			//その回の日程の報告が無い場合は以降、過去の報告なし判定。
			if($l_reported_flg == 0)
			{
				$old_reported_flg = 0;
			}

		}

		$this->log->debug("get_applied_training_plan_list() END data:".print_r($arr_training_list,true));
		$this->log->debug("get_applied_training_plan_list() END count():".count($arr_training_list));
		return $arr_training_list;
	}
}
