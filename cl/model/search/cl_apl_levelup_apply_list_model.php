<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_apl_levelup_apply_list_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_apl_levelup_apply_list_model(&$p_con,$p_login_user) {
        parent::cl_common_model(&$p_con,$p_user);
        $this->con = &$p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * getLevelupApplyList_newEval レベルアップ申請の内容取得（職員ID、レベルから）
     *
     * @param $emp_id    職員ID
     * @param $level     レベル
     * @return           レベルアップ申請内容配列
     */
    function getLevelupApplyList_forNewEval($emp_id, $target_level){

        $this->log->info(__FUNCTION__." START");
        $this->log->debug("emp_id：".$emp_id);
        $this->log->debug("target_level：".$target_level);

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	lap.levelup_apply_id ";
		$sql .= " 	, wfm.short_wkfw_name||'-'||substr(apl.apply_date, 1, 4)||lpad(apl.apply_no, 4, '0') as levelup_apply_no ";
		$sql .= " 	, lap.level ";
		//$sql .= " 	, lap.apply_date ";
		$sql .= " 	, substr(apl.apply_date, 1, 4)||'/'|| substr(apl.apply_date, 5, 2)||'/'||substr(apl.apply_date, 7, 2) as apply_date";
		$sql .= " 	, lap.levelup_emp_id ";
		$sql .= " 	, emp_chief.emp_lt_nm || '　' || emp_chief.emp_ft_nm as chief_emp_name ";
		$sql .= " 	, emp_colleague.emp_lt_nm || '　' || emp_colleague.emp_ft_nm as colleague_emp_name ";
		$sql .= " FROM ";
		$sql .= " 	 cl_apl_levelup_apply lap ";
		$sql .= " 	 , cl_apply apl ";
		$sql .= " 	 , cl_wkfwmst wfm ";
		$sql .= " 	 , empmst emp_chief ";
		$sql .= " 	 , empmst emp_colleague ";
		$sql .= " WHERE ";
		$sql .= " 	lap.apply_id = apl.apply_id ";
		$sql .= " 	AND apl.apply_stat = 1 ";
		$sql .= " 	AND apl.wkfw_id = wfm.wkfw_id ";
		$sql .= " 	AND lap.chief_emp_id = emp_chief.emp_id ";
		$sql .= " 	AND lap.colleague_emp_id = emp_colleague.emp_id ";
		$sql .= " 	AND lap.delete_flg = 0 ";
		$sql .= " 	AND lap.levelup_emp_id = :emp_id ";
		$sql .= " 	AND lap.level = :target_level ";
		// 2012/08/02 Yamagawa add(s)
		// 評価申請が既に承認済み・申請中であれば、対象外とする
		$sql .= " 	AND NOT EXISTS( ";
		$sql .= " 		SELECT ";
		$sql .= " 			eva_apl.apply_id ";
		$sql .= " 		FROM ";
		$sql .= " 			cl_apl_levelup_evaluation eva ";
		$sql .= " 			,cl_apply eva_apl ";
		$sql .= " 		WHERE ";
		$sql .= " 			eva.levelup_apply_id = lap.levelup_apply_id ";
		$sql .= " 			AND eva.delete_flg = 0 ";
		$sql .= " 			AND eva_apl.apply_id = eva.apply_id ";
		$sql .= " 			AND eva_apl.delete_flg = FALSE ";
		// 2012/08/10 Yamagawa del(s)
		// 下書き中の申請が存在した場合でも対象外とする
		//$sql .= " 			AND eva_apl.draft_flg = FALSE ";
		// 2012/08/10 Yamagawa del(e)
		$sql .= " 			AND eva_apl.apply_stat in (0,1) ";
		$sql .= " 	) ";
		// 2012/08/02 Yamagawa add(e)
		$sql .= " ORDER BY ";
		$sql .= " 	lap.levelup_apply_id ";

		$data=parent::select($sql,array("text","integer"),array("emp_id"=>$emp_id,"target_level"=>$target_level),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;

	}

	/**
	 * getLevelupApplyList_forEdit レベルアップ申請の内容取得（評価申請の申請IDから）
	 *
	 * @param $apply_id						申請ID
	 * @param $evaluation_division			評価表区分
	 * @return								レベルアップ申請内容配列
	 */
	function getLevelupApplyList_forEdit($apply_id,$evaluation_division){

		$this->log->info(__FUNCTION__." START");
		$this->log->debug("apply_id：".$apply_id);
		$this->log->debug("evaluation_emp_division：".$evaluation_division);

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	lap.levelup_apply_id ";
		$sql .= " 	, wfm.short_wkfw_name||'-'||substr(apl.apply_date, 1, 4)||lpad(apl.apply_no, 4, '0') as levelup_apply_no ";
		if($evaluation_division == "1"){
			$sql .= " 	, eval.levelup_evaluation_id as levelup_evaluation_id ";
		}else{
			$sql .= " 	, eval.levelup_evaluation_colleague_id as levelup_evaluation_id ";
		}
		$sql .= " 	, lap.level ";
		$sql .= " 	, lap.apply_date ";
		$sql .= " 	, lap.levelup_emp_id ";
		$sql .= " 	, emp_chief.emp_lt_nm || '　' || emp_chief.emp_ft_nm as chief_emp_name ";
		$sql .= " 	, emp_colleague.emp_lt_nm || '　' || emp_colleague.emp_ft_nm as colleague_emp_name ";
		if($evaluation_division == "1"){
			$sql .= " 	, eval.self_evaluation_date ";
			$sql .= " 	, eval.supervisor_emp_id ";
			$sql .= " 	, eval.supervisor_evalution_date ";
			$sql .= " 	, '' as colleague_division ";
		}else{
			$sql .= " 	, '' as self_evaluation_date ";
			$sql .= " 	, '' as supervisor_emp_id ";
			$sql .= " 	, '' as supervisor_evalution_date ";
			$sql .= " 	, eval.colleague_division ";
		}
		$sql .= " FROM ";
		if($evaluation_division == "1"){
			$sql .= " 	cl_apl_levelup_evaluation eval ";
		}else{
			$sql .= " 	cl_apl_levelup_evaluation_colleague eval ";
		}
		$sql .= " 	, cl_apl_levelup_apply lap ";
		$sql .= " 	, cl_apply apl ";
		$sql .= " 	, cl_wkfwmst wfm ";
		$sql .= " 	, empmst emp_chief";
		$sql .= " 	, empmst emp_colleague";
		$sql .= " WHERE ";
		$sql .= " 	eval.levelup_apply_id = lap.levelup_apply_id ";
		$sql .= " 	AND lap.apply_id = apl.apply_id ";
		$sql .= " 	AND apl.wkfw_id = wfm.wkfw_id ";
		$sql .= " 	AND lap.chief_emp_id = emp_chief.emp_id ";
		$sql .= " 	AND lap.colleague_emp_id = emp_colleague.emp_id ";
		$sql .= " 	AND eval.apply_id = :apply_id ";
		$sql .= " 	AND eval.delete_flg = 0 ";
		$sql .= " 	AND lap.delete_flg = 0 ";
		$sql .= " 	AND apl.delete_flg = false ";


		$data=parent::select($sql,array("text"),array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data[0];

	}

    /**
     * getLevelupApplyList_newEval レベルアップ申請の内容取得（レベルアップ申請IDから）
     *
     * @param $levelup_apply_id    レベルアップ申請ID
     * @return           レベルアップ申請内容配列
     */
    function getLevelupApplyList_forNewEval2($levelup_apply_id){

        $this->log->info(__FUNCTION__." START");
        $this->log->debug("levelup_apply_id：".$levelup_apply_id);

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	lap.levelup_apply_id ";
		$sql .= " 	, wfm.short_wkfw_name||'-'||substr(apl.apply_date, 1, 4)||lpad(apl.apply_no, 4, '0') as levelup_apply_no ";
		$sql .= " 	, lap.level ";
		$sql .= " 	, lap.apply_date ";
		$sql .= " 	, lap.levelup_emp_id ";
		$sql .= " 	, emp_chief.emp_lt_nm || '　' || emp_chief.emp_ft_nm as chief_emp_name ";
		$sql .= " 	, emp_colleague.emp_lt_nm || '　' || emp_colleague.emp_ft_nm as colleague_emp_name ";
		$sql .= " FROM ";
		$sql .= " 	 cl_apl_levelup_apply lap ";
		$sql .= " 	 , cl_apply apl ";
		$sql .= " 	, cl_wkfwmst wfm ";
		$sql .= " 	 , empmst emp_chief ";
		$sql .= " 	 , empmst emp_colleague ";
		$sql .= " WHERE ";
		$sql .= " 	lap.apply_id = apl.apply_id ";
		$sql .= " 	AND apl.apply_stat = 1 ";
		$sql .= " 	AND apl.wkfw_id = wfm.wkfw_id ";
		$sql .= " 	AND lap.chief_emp_id = emp_chief.emp_id ";
		$sql .= " 	AND lap.colleague_emp_id = emp_colleague.emp_id ";
		$sql .= " 	AND lap.levelup_apply_id = :levelup_apply_id ";
		$sql .= " ORDER BY ";
		$sql .= " 	lap.levelup_apply_id ";

		$data=parent::select($sql,array("text"),array("levelup_apply_id"=>$levelup_apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;

	}

}
