<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class classname_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function classname_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	* 組織階層情報取得
	*/
	function getClassname()
	{
		$this->log->info("getClassname() START");

		//SQL作成
		$sql = "";
		$sql .= "SELECT";
		$sql .= " class_nm,";
		$sql .= " atrb_nm,";
		$sql .= " dept_nm,";
		$sql .= " room_nm,";
		$sql .= " class_cnt ";
		$sql .= "FROM ";
		$sql .= " classname";

		$data=parent::select($sql,null,array(),MDB2_FETCHMODE_ASSOC);

		$this->log->info("getClassname() END");
		return $data[0];
	}

}
