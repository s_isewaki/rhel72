<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_apl_inside_training_report_training_learned_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     */
    function cl_apl_inside_training_report_training_learned_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * getList 前回までの研修報告内容取得
     *
     * @param $training_apply_id 研修申込ID
     * @param $plan_id 今回の日程ID
     * @return  前回までの研修報告内容配列
     */
    function getList($training_apply_id, $plan_id ){

        $this->log->info(__FUNCTION__." START");
        $this->log->debug("training_apply_id：".$training_apply_id);
        $this->log->debug("plan_id：".$plan_id);

		$sql = "";
		$sql .= " SELECT";
		$sql .= "   C.training_time,";
		$sql .= "   C.final_flg,";
		$sql .= "   A.training_learned";
		// 2012/11/16 Yamagawa upd(s)
		$sql .= "   ,B.apply_stat ";
		// 2012/11/16 Yamagawa upd(e)

		//対象の院内研修報告に対して
		$sql .= " FROM";
		$sql .= " (";
		$sql .= "     SELECT";
		$sql .= "     apply_id,";
		$sql .= "     plan_id,";
		$sql .= "     training_learned";
		$sql .= "     FROM cl_apl_inside_training_report ";
		$sql .= "     WHERE training_apply_id = :training_apply_id";
		$sql .= " ) A";

		//承認中(apply_stat=2)と承認済み(apply_stat=1)のものを絞り込み(2:否認、3:差戻しは対象外)
		$sql .= " INNER JOIN";
		$sql .= " cl_apply B";
		$sql .= " ON A.apply_id = B.apply_id";
		$sql .= " AND B.apply_stat in( 0, 1 )";
		$sql .= " AND not B.draft_flg";
		$sql .= " AND not B.delete_flg";

		//日程情報を付加
		$sql .= " INNER JOIN";
		$sql .= " cl_mst_inside_training_schedule C";
		$sql .= " ON A.plan_id = C.plan_id";

		//今回の研修回数より前のもを絞込み
		$sql .= " INNER JOIN";
		$sql .= " (";
		$sql .= "     SELECT training_time as now_training_time";
		$sql .= "     FROM cl_mst_inside_training_schedule";
		$sql .= "     WHERE plan_id = :plan_id";
		$sql .= " ) D";
		$sql .= " on C.training_time < D.now_training_time";

		//研修回数順にソート
		$sql .= " order by C.training_time";


        /* カラムタイプ */
        $type = array('text','text');

        $param['training_apply_id'] = $training_apply_id;
        $param['plan_id'] = $plan_id;

        $data = parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

        $this->log->debug("SQL：".$sql);
        $this->log->debug("結果：".print_r($data,true));
        $this->log->info(__FUNCTION__." END");

        return $data;

     }


}
