<?php
/**
 * 研修講座一覧画面の一覧データ取得用モデル
 *
 *
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_applied_inside_training_theme_list_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_applied_inside_training_theme_list_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }



	/**
	* 院内研修課題一覧に必要なデータを取得します。
	*
	* @param $emp_id 研修の申請者
	*/
	function get_applied_training_theme_list($emp_id)
	{
		$this->log->debug("get_applied_training_theme_list() START");

		//SQL作成
		$sql = "";
		$sql .= " SELECT";
		$sql .= "   A.training_apply_id,";
		$sql .= "   A.inside_training_date,";
		$sql .= "   B.training_id,";
		$sql .= "   B.training_name,";
		$sql .= "   C.theme_id,";
		$sql .= "   C.theme_division,";
		$sql .= "   C.theme_contents,";
		$sql .= "   C.theme_method,";
		$sql .= "   C.theme_deadline,";
		$sql .= "   C.theme_present,";
		$sql .= "   D.plan_id,";
		$sql .= "   D.training_time,";
		$sql .= "   D.final_flg,";
		$sql .= "   D.plan_date,";
		$sql .= "   D.from_time,";
		$sql .= "   D.to_time";

		$sql .= " FROM";
		$sql .= " (";
		$sql .= "   SELECT * FROM cl_applied_inside_training WHERE emp_id = :emp_id AND delete_flg <> 1";
		$sql .= " ) A";

		$sql .= " INNER JOIN";
		$sql .= " (";
		$sql .= "   SELECT training_id,training_name FROM cl_mst_inside_training";
		$sql .= " ) B";
		$sql .= " ON A.inside_training_id = B.training_id";

		$sql .= " INNER JOIN";
		$sql .= " (";
		$sql .= "   SELECT * FROM cl_mst_inside_training_theme";
		$sql .= " ) C";
		$sql .= " ON A.inside_training_id = C.training_id";

		$sql .= " INNER JOIN";
		$sql .= " (";
		$sql .= "   SELECT plan_id,training_time,final_flg,plan_date,from_time,to_time FROM cl_mst_inside_training_schedule";
		$sql .= " ) D";
		$sql .= " ON A.plan_id like '%' || D.plan_id || '%'";

		$sql .= " ORDER BY A.inside_training_date, A.training_apply_id, C.theme_division, D.plan_date, D.from_time, D.to_time, D.plan_id";



		$data=parent::select($sql,array("text"),array("emp_id"=>$emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_applied_training_theme_list() DB RETURN count(data):".count($data));


		$i_tra = -1;
		$i_theme = -1;
		$i_plan = -1;
		$old_training_apply_id = "";
		$old_theme_id = "";
		$arr_training_list = array();
		for($i = 0; $i < count($data); $i++)//
		{
			$l_training_apply_id      = $data[$i]['training_apply_id'];
			$l_inside_training_date   = $data[$i]['inside_training_date'];
			$l_training_id            = $data[$i]['training_id'];
			$l_training_name          = $data[$i]['training_name'];

			$l_theme_id               = $data[$i]['theme_id'];
			$l_theme_division         = $data[$i]['theme_division'];
			$l_theme_contents         = $data[$i]['theme_contents'];
			$l_theme_method           = $data[$i]['theme_method'];
			$l_theme_deadline         = $data[$i]['theme_deadline'];
			$l_theme_present          = $data[$i]['theme_present'];

			$l_plan_id                = $data[$i]['plan_id'];
			$l_training_time          = $data[$i]['training_time'];
			$l_final_flg              = $data[$i]['final_flg'];
			$l_plan_date              = $data[$i]['plan_date'];
			$l_from_time              = $data[$i]['from_time'];
			$l_to_time                = $data[$i]['to_time'];

			$i_plan++;
			if($old_training_apply_id != $l_training_apply_id)
			{
				$i_tra++;
				$i_theme = 0;
				$i_plan = 0;
			}
			elseif($old_theme_id != $l_theme_id)
			{
				$i_theme++;
				$i_plan = 0;
			}

			if($i_theme == 0 && $i_plan == 0)
			{
				$arr_training_list[$i_tra] = array();
				$arr_training_list[$i_tra]['training_apply_id']      = $l_training_apply_id;
				$arr_training_list[$i_tra]['inside_training_date']   = $l_inside_training_date;
				$arr_training_list[$i_tra]['training_id']            = $l_training_id;
				$arr_training_list[$i_tra]['training_name']          = $l_training_name;
				$arr_training_list[$i_tra]['theme_list']             = array();
				$arr_training_list[$i_tra]['plan_list']              = array();
			}
			if($i_plan == 0)
			{
				$arr_training_list[$i_tra]['theme_list'][$i_theme] = array();
				$arr_training_list[$i_tra]['theme_list'][$i_theme]['theme_id']       = $l_theme_id;
				$arr_training_list[$i_tra]['theme_list'][$i_theme]['theme_division'] = $l_theme_division;
				$arr_training_list[$i_tra]['theme_list'][$i_theme]['theme_contents'] = $l_theme_contents;
				$arr_training_list[$i_tra]['theme_list'][$i_theme]['theme_method']   = $l_theme_method;
				$arr_training_list[$i_tra]['theme_list'][$i_theme]['theme_deadline'] = $l_theme_deadline;
				$arr_training_list[$i_tra]['theme_list'][$i_theme]['theme_present']  = $l_theme_present;
			}
			if($i_theme == 0)
			{
				$arr_training_list[$i_tra]['plan_list'][$i_plan] = array();
				$arr_training_list[$i_tra]['plan_list'][$i_plan]['plan_id']   = $l_plan_id;
				$arr_training_list[$i_tra]['plan_list'][$i_plan]['training_time'] = $l_training_time;
				$arr_training_list[$i_tra]['plan_list'][$i_plan]['final_flg'] = $l_final_flg;
				$arr_training_list[$i_tra]['plan_list'][$i_plan]['plan_date'] = $l_plan_date;
				$arr_training_list[$i_tra]['plan_list'][$i_plan]['from_time'] = $l_from_time;
				$arr_training_list[$i_tra]['plan_list'][$i_plan]['to_time']   = $l_to_time;
			}

			$old_training_apply_id = $l_training_apply_id;
			$old_theme_id = $l_theme_id;
		}

		$this->log->debug("get_applied_training_list() END :".print_r($arr_training_list,true));
		$this->log->debug("get_applied_training_list() END count():".count($arr_training_list));
		return $arr_training_list;
	}


}
