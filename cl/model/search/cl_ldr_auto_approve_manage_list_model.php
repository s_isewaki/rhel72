
<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_ldr_auto_approve_manage_list_model extends cl_common_model{


    var $con;
    var $log;
    var $user;

    /**
     * Constractor コンストラクタ
     */
    function cl_ldr_auto_approve_manage_list_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * getList 全件取得
     * @param type $plan_id
     */
    function getList(){

        $this->log->info(__FUNCTION__." START");

        /* SQL */
        $sql = "";
		$sql .= " SELECT ";
		$sql .= " 	auto_approve_manage_id ";
		$sql .= " 	, apply_id ";
		$sql .= " 	, apv_order ";
		$sql .= " 	, apv_sub_order ";
		$sql .= " 	, last_approve_flg ";
		$sql .= " 	, proc_flg ";
		$sql .= " 	, delete_flg ";
		$sql .= " FROM ";
		$sql .= " 	cl_ldr_auto_approve_manage ";
		$sql .= " WHERE ";
		$sql .= " 	proc_flg = 0 ";
		$sql .= " ORDER BY ";
		$sql .= " 	apply_id ";
		$sql .= " 	, auto_approve_manage_id ";

        $data=parent::select($sql,null,null,MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data;

     }

    /**
     * 申請ID 取得
     * @param $short_wkfw_name 管理CD
     * @param $level レベル
     */
    function getApplyIdList($short_wkfw_name,$level){

        $this->log->info(__FUNCTION__." START");

        /* SQL */
        $sql = "";
		$sql .= " SELECT DISTINCT ";
		$sql .= " 	apply_id ";
		$sql .= " 	, short_wkfw_name ";
		$sql .= " 	, level ";
		$sql .= " FROM ";
		$sql .= " 	cl_ldr_auto_approve_manage ";
		$sql .= " WHERE ";
		$sql .= " 	proc_flg = 0 ";
		$sql .= " 	AND short_wkfw_name = :short_wkfw_name ";
		$sql .= " 	AND level = :level ";
		$sql .= " ORDER BY ";
		$sql .= " 	apply_id ";

		// 2012/10/04 Yamagawa upd(s)
		//$type = array("text","text");
		$type = array("text","integer");
		// 2012/10/04 Yamagawa upd(e)

		$data=parent::select($sql,$type,array("short_wkfw_name" => $short_wkfw_name,"level" => $level),MDB2_FETCHMODE_ASSOC);

		$this->log->info(__FUNCTION__." END");

		return $data;

	}

    /**
     * 承認者 取得
     * @param $apply_id 申請ID
     */
    function getListByApplyId($apply_id){

        $this->log->info(__FUNCTION__." START");

        /* SQL */
        $sql = "";
		$sql .= " SELECT ";
		$sql .= " 	auto_approve_manage_id ";
		$sql .= " 	, apply_id ";
		$sql .= " 	, short_wkfw_name ";
		$sql .= " 	, level ";
		$sql .= " 	, apv_order ";
		$sql .= " 	, apv_sub_order ";
		$sql .= " 	, last_approve_flg ";
		$sql .= " 	, proc_flg ";
		$sql .= " FROM ";
		$sql .= " 	cl_ldr_auto_approve_manage ";
		$sql .= " WHERE ";
		$sql .= " 	apply_id = :apply_id ";

        $data=parent::select($sql,array("text"),array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data;

     }

}
