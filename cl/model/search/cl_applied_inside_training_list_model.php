<?php
/**
 * 研修講座一覧画面の一覧データ取得用モデル
 *
 *
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_applied_inside_training_list_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_applied_inside_training_list_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }



	/**
	* 院内研修申請一覧に必要なデータを取得します。
	*
	* @param $emp_id 研修の申請者
	*/
	function get_applied_training_list($emp_id)
	{
		$this->log->debug("get_applied_training_list() START");

		//SQL作成
		$sql = "";
		$sql .= " SELECT";
		$sql .= "   A.training_apply_id,";
		$sql .= "   A.last_training_apply_id,";
		$sql .= "   A.inside_training_date,";
		$sql .= "   A.learn_memo,";
		$sql .= "   A.recommend_flag,";
		$sql .= "   B.training_id,";
		$sql .= "   B.training_name,";
		$sql .= "   C.plan_id,";
		$sql .= "   C.training_time,";
		$sql .= "   C.final_flg,";
		$sql .= "   C.plan_date,";
		$sql .= "   C.from_time,";
		$sql .= "   C.to_time";
		// 2012/11/05 Yamagawa add(s)
		$sql .= "   ,CASE WHEN D.training_apply_id IS NULL THEN 0 ELSE 1 END AS reported_flg ";
		// 2012/11/05 Yamagawa add(e)
		// 2012/11/20 Yamagawa add(s)
		$sql .= "   ,A.year ";
		// 2012/11/20 Yamagawa add(e)

		$sql .= " FROM";
		$sql .= " (";
		$sql .= "   SELECT * FROM cl_applied_inside_training WHERE emp_id = :emp_id AND delete_flg <> 1";
		$sql .= " ) A";

		$sql .= " INNER JOIN";
		$sql .= " (";
		$sql .= "   SELECT training_id,training_name FROM cl_mst_inside_training";
		$sql .= " ) B";
		$sql .= " ON A.inside_training_id = B.training_id";

		$sql .= " INNER JOIN";
		$sql .= " (";
		$sql .= "   SELECT plan_id,training_time,final_flg,plan_date,from_time,to_time FROM cl_mst_inside_training_schedule";
		$sql .= " ) C";
		$sql .= " ON A.plan_id like '%' || C.plan_id || '%'";

		// 2012/11/05 Yamagawa add(s)
		// 報告中、報告済みかをチェックする
		$sql .= " LEFT JOIN ";
		$sql .= " ( ";
		$sql .= "  SELECT DISTINCT ";
		$sql .= "   rep.training_apply_id ";
		$sql .= "   ,rep.training_id ";
		$sql .= "   ,rep.plan_id ";
		$sql .= "  FROM ";
		$sql .= "   cl_apl_inside_training_report rep ";
		$sql .= "   INNER JOIN ";
		$sql .= "    cl_apply app ";
		$sql .= "   ON ";
		$sql .= "    app.apply_id = rep.apply_id ";
		$sql .= "    AND app.apply_stat IN ('0','1') ";		// 申請中 or 承認済み
		$sql .= "    AND app.delete_flg = FALSE ";		// 申請取消以外
		$sql .= "    AND app.draft_flg = FALSE ";		// 下書き以外
		$sql .= "  WHERE ";
		$sql .= "   rep.delete_flg != 1 ";			// 削除済み以外
		$sql .= " ) D ";
		$sql .= " ON ";
		$sql .= "  D.training_apply_id = A.last_training_apply_id ";
		$sql .= "  AND D.training_id = A.inside_training_id ";
		$sql .= "  AND D.plan_id = C.plan_id ";
		// 2012/11/05 Yamagawa add(e)

		$sql .= " ORDER BY A.inside_training_date, A.training_apply_id, C.plan_date, C.from_time, C.to_time, C.plan_id";

		$data=parent::select($sql,array("text"),array("emp_id"=>$emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_applied_training_list() DB RETURN count(data):".count($data));

		$i_tra = -1;
		$i_plan = -1;
		$old_training_apply_id = "";
		$arr_training_list = array();
		for($i = 0; $i < count($data); $i++)//
		{
			$l_training_apply_id      = $data[$i]['training_apply_id'];
			$l_last_training_apply_id = $data[$i]['last_training_apply_id'];
			$l_inside_training_date   = $data[$i]['inside_training_date'];
			$l_learn_memo             = $data[$i]['learn_memo'];
			$l_recommend_flag         = $data[$i]['recommend_flag'];
			$l_training_id            = $data[$i]['training_id'];
			$l_training_name          = $data[$i]['training_name'];
			$l_plan_id                = $data[$i]['plan_id'];
			$l_training_time          = $data[$i]['training_time'];
			$l_final_flg              = $data[$i]['final_flg'];
			$l_plan_date              = $data[$i]['plan_date'];
			$l_from_time              = $data[$i]['from_time'];
			$l_to_time                = $data[$i]['to_time'];
			// 2012/11/05 Yamagawa add(s)
			$l_reported_flg           = $data[$i]['reported_flg'];
			// 2012/11/05 Yamagawa add(e)
			// 2012/11/20 Yamagawa add(s)
			$l_year                   = $data[$i]['year'];
			// 2012/11/20 Yamagawa add(e)

			$i_plan++;
			if($old_training_apply_id != $l_training_apply_id)
			{
				$i_tra++;
				$i_plan = 0;
			}

			if($i_plan == 0)
			{
				$arr_training_list[$i_tra] = array();
				$arr_training_list[$i_tra]['training_apply_id']      = $l_training_apply_id;
				$arr_training_list[$i_tra]['last_training_apply_id'] = $l_last_training_apply_id;
				$arr_training_list[$i_tra]['inside_training_date']   = $l_inside_training_date;
				$arr_training_list[$i_tra]['learn_memo']             = $l_learn_memo;
				$arr_training_list[$i_tra]['recommend_flag']         = $l_recommend_flag;
				$arr_training_list[$i_tra]['training_id']            = $l_training_id;
				$arr_training_list[$i_tra]['training_name']          = $l_training_name;
				// 2012/11/20 Yamagawa add(s)
				$arr_training_list[$i_tra]['year']                   = $l_year;
				// 2012/11/20 Yamagawa add(e)
				$arr_training_list[$i_tra]['plan_list']              = array();
			}
			$arr_training_list[$i_tra]['plan_list'][$i_plan] = array();
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['plan_id']   = $l_plan_id;
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['training_time'] = $l_training_time;
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['final_flg'] = $l_final_flg;
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['plan_date'] = $l_plan_date;
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['from_time'] = $l_from_time;
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['to_time']   = $l_to_time;
			// 2012/11/05 Yamagawa add(s)
			$arr_training_list[$i_tra]['plan_list'][$i_plan]['reported_flg'] = $l_reported_flg;
			// 2012/11/05 Yamagawa add(e)

			$old_training_apply_id = $l_training_apply_id;
		}

		$this->log->debug("get_applied_training_list() END data:".print_r($arr_training_list,true));
		$this->log->debug("get_applied_training_list() END count():".count($arr_training_list));
		return $arr_training_list;
	}


}
