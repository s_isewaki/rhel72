<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class jobmst_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con �ǡ����١������ͥ������
     * @param $p_login_user ��������桼����
     */
    function jobmst_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	* ����ޥ�������
	*/
	function getList(){
		$this->log->info("getList() START");

		//SQL����
		$sql = "";
		$sql .= "SELECT";
		$sql .= " job_id,";
		$sql .= " job_nm ";
		$sql .= "FROM";
		$sql .= " jobmst ";
		$sql .= "WHERE";
		$sql .= " job_del_flg = 'f' ";
		$sql .= "ORDER BY";
		$sql .= " order_no";

		$data=parent::select($sql,null,array(),MDB2_FETCHMODE_ASSOC);

		$this->log->info("getList() END");
		return $data;
	}

	// 2012/08/31 Yamagawa add(s)
	/**
	 * ����ޥ����ʴǸ��Ϣ�˼���
	 */
	function getList_nurse(){

		$this->log->info("getList_nurse() START");

		//SQL����
		$sql = "";
		$sql .= "SELECT";
		$sql .= " job_id,";
		$sql .= " job_nm ";
		$sql .= "FROM";
		$sql .= " jobmst ";
		$sql .= "WHERE";
		$sql .= " job_del_flg = 'f' ";
		$sql .= " AND job_nm in ('�Ǹ��','������','�ݷ��','�ڴǸ��') ";
		$sql .= "ORDER BY";
		$sql .= " order_no";

		$data=parent::select($sql,null,array(),MDB2_FETCHMODE_ASSOC);

		$this->log->info("getList_nurse() END");
		return $data;

	}
	// 2012/08/31 Yamagawa add(e)

}
