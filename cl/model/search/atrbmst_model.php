<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class atrbmst_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function atrbmst_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	* 部署マスタ取得
	*/
	function getList(){
		$this->log->info("getList() START");

		//SQL作成
		$sql = "";
		$sql .= "SELECT";
		$sql .= " atrb_id,";
		$sql .= " class_id, ";
		$sql .= " atrb_nm ";
		$sql .= "FROM";
		$sql .= " atrbmst ";
		$sql .= "WHERE";
		$sql .= " atrb_del_flg = 'f' ";
		$sql .= "ORDER BY";
		$sql .= " order_no";

		$data=parent::select($sql,null,array(),MDB2_FETCHMODE_ASSOC);

		$this->log->info("getList() END");
		return $data;
	}

	/**
	 * 部署マスタ取得（施設指定有）
	 */
	function getList_add_conditions_class($class_id){
		$this->log->info("getList_add_conditions_class() START");

		//SQL作成
		$sql = "";
		$sql .= "SELECT";
		$sql .= " atrb_id,";
		$sql .= " class_id, ";
		$sql .= " atrb_nm ";
		$sql .= "FROM";
		$sql .= " atrbmst ";
		$sql .= "WHERE";
		$sql .= " class_id = :class_id ";
		$sql .= " AND atrb_del_flg = 'f' ";
		$sql .= "ORDER BY";
		$sql .= " order_no";

		$data=parent::select($sql,array("integer"),array("class_id" => $class_id),MDB2_FETCHMODE_ASSOC);

		$this->log->info("getList_add_conditions_class() END");
		return $data;
	}

	/**
	* 所属部署マスタ取得（兼務込）
	*/
	function getList_add_conditions_emp_id($class_division, $emp_id){
		$this->log->info("getList_add_conditions_emp_id() START");

		//SQL作成
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " atrb.atrb_id ";
		$sql .= " ,atrb.class_id ";
		$sql .= " ,atrb.atrb_nm ";
		$sql .= "FROM ";
		$sql .= " atrbmst atrb ";
		$sql .= "WHERE ";
		$sql .= " atrb.atrb_del_flg = 'f' ";
		$sql .= " AND ( ";
		$sql .= "  EXISTS ( ";
		$sql .= "   SELECT ";
		$sql .= "    emp.emp_id ";
		$sql .= "   FROM ";
		$sql .= "    empmst emp ";
		$sql .= "   WHERE ";
		$sql .= "    emp.emp_id = :emp_id ";

		if ($class_division == 1){
			$sql .= "    AND emp.emp_class = atrb.class_id ";
		}

		if ($class_division >= 2){
			$sql .= "    AND emp.emp_attribute = atrb.atrb_id ";
		}

		$sql .= "	) ";
		$sql .= "  OR EXISTS ( ";
		$sql .= "   SELECT ";
		$sql .= "    con.emp_id ";
		$sql .= "   FROM ";
		$sql .= "    concurrent con ";
		$sql .= "   WHERE ";
		$sql .= "    con.emp_id = :emp_id ";

		if ($class_division == 1){
			$sql .= "    AND con.emp_class = atrb.class_id ";
		}

		if ($class_division >= 2){
			$sql .= "    AND con.emp_attribute = atrb.atrb_id ";
		}

		$sql .= "  ) ";
		$sql .= " ) ";

		$data=parent::select($sql,array('text'),array('emp_id' => $emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->info("getList_add_conditions_emp_id() END");
		return $data;
	}
}
