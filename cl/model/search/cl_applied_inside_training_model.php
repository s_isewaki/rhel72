<?php
/**
 * 研修講座一覧画面の一覧データ取得用モデル
 *
 *
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_applied_inside_training_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_applied_inside_training_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	* 院内研修申請一覧に必要なデータを取得します。
	*
	* @param $emp_id 研修の申請者
	*/
	function get_applied_inside_training_list($apply_id)
	{
		$this->log->debug("get_applied_inside_training_list() START");

		//SQL作成
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " intr.levelup_apply_inside_training_id ";
		$sql .= " ,intr.levelup_apply_id ";
		$sql .= " ,intr.training_id ";
		$sql .= " ,msttr.training_name ";
		$sql .= " ,intr.must_division ";
		$sql .= " ,intr.pass_flg ";
		$sql .= "FROM ";
		$sql .= " cl_apl_levelup_apply lvapl ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_apl_levelup_apply_inside_training intr ";
		$sql .= " ON ";
		$sql .= "  intr.levelup_apply_id = lvapl.levelup_apply_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_mst_inside_training msttr ";
		$sql .= " ON ";
		$sql .= "  msttr.training_id = intr.training_id ";
		$sql .= "WHERE ";
		$sql .= " lvapl.apply_id = :apply_id ";
		$sql .= "ORDER BY ";
		$sql .= " intr.training_id ";

		$data=parent::select($sql,array("text"),array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_applied_inside_training_list() END data:".print_r($data,true));
		$this->log->debug("get_applied_inside_training_list() END count(data):".count($data));
		return $data;

	}

}
