<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_auto_application_apply_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 職員取得用SQL */
    var $emp_select_sql = "SELECT * FROM empmst WHERE emp_id = :emp_id";

    /* ワークフローマスタ取得SQL */
    var $wkfw_select_sql = "SELECT * FROM cl_wkfwmst WHERE short_wkfw_name = :short_wkfw_name";

    /* ワークフロー履歴No取得SQL */
    var $wkfw_history_no_sql = "SELECT MAX(wkfw_history_no) AS max FROM cl_wkfw_template_history WHERE wkfw_id = :wkfw_id";

    /* フォーマットファイル情報取得SQL */
    var $wkfwfile_sql = "SELECT * FROM cl_wkfwfile WHERE wkfw_id = :wkfw_id";

    /* ワークファイル履歴No取得SQL */
    var $max_wkfwfile_history_no_sql = "SELECT MAX(wkfwfile_history_no) AS max FROM cl_wkfwfile_history WHERE wkfw_id = :wkfw_id";

    /**
     * Constractor
     */
    function cl_auto_application_apply_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * getemp
     * @param type $emp_id
     */
    function getemp($emp_id){

        $this->log->info("getemp() START");
        $data=parent::select($this->emp_select_sql,array("text"),array("emp_id"=>$emp_id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("getemp() END count(data):".count($data));
        return $data[0];

     }

    /**
     * getwkfw
     * @param type $short_wkfw_name
     */
    function getwkfw($short_wkfw_name){

        $this->log->info("getwkfw() START");
        $data=parent::select($this->wkfw_select_sql,array("text"),array("short_wkfw_name"=>$short_wkfw_name),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("getwkfw() END count(data):".count($data));
        return $data[0];

     }

    /**
     * get_max_wkfw_history_no
     * @param type $wkfw_id
     */
    function get_max_wkfw_history_no($wkfw_id){

        $this->log->info("get_max_wkfw_history_no() START");
        $data=parent::select($this->wkfw_history_no_sql,array("text"),array("wkfw_id"=>$wkfw_id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("get_max_wkfw_history_no() END max:".$data[0]['max']);
        return $data[0]['max'];

     }

    /**
     * get_wkfwfile
     * @param type $wkfw_id
     */
    function get_wkfwfile($wkfw_id){

        $this->log->info("get_wkfwfile() START");
        $data=parent::select($this->wkfwfile_sql,array("text"),array("wkfw_id"=>$wkfw_id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("get_wkfwfile() END max:".$data[0]['max']);
        return $data;

     }

    /**
     * get_max_wkfw_history_no
     * @param type $wkfw_id
     */
    function get_max_wkfwfile_history_no($wkfw_id){

        $this->log->info("get_max_wkfwfile_history_no() START");
        $data=parent::select($this->max_wkfwfile_history_no_sql,array("text"),array("wkfw_id"=>$wkfw_id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("get_max_wkfwfile_history_no() END max:".$data[0]['max']);
        return $data[0]['max'];

     }

}
