<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class classroom_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function classroom_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	* 室マスタ取得
	*/
	function getList(){
		$this->log->info("getList() START");

		//SQL作成
		$sql = "";
		$sql .= "SELECT";
		$sql .= " room_id,";
		$sql .= " dept_id,";
		$sql .= " room_nm ";
		$sql .= "FROM";
		$sql .= " classroom ";
		$sql .= "WHERE";
		$sql .= " room_del_flg = 'f' ";
		$sql .= "ORDER BY";
		$sql .= " order_no";

		$data=parent::select($sql,null,array(),MDB2_FETCHMODE_ASSOC);

		$this->log->info("getList() END");
		return $data;
	}

	/**
	* 室マスタ取得（課・科指定有）
	*/
	function getList_add_conditions_dept($dept_id){
		$this->log->info("getList_add_conditions_dept() START");

		//SQL作成
		$sql = "";
		$sql .= "SELECT";
		$sql .= " room_id,";
		$sql .= " dept_id,";
		$sql .= " room_nm ";
		$sql .= "FROM";
		$sql .= " classroom ";
		$sql .= "WHERE";
		$sql .= " dept_id = :dept_id ";
		$sql .= " AND room_del_flg = 'f' ";
		$sql .= "ORDER BY";
		$sql .= " order_no";

		$data=parent::select($sql,array("integer"),array("dept_id" => $dept_id),MDB2_FETCHMODE_ASSOC);

		$this->log->info("getList_add_conditions_dept() END");
		return $data;
	}

	/**
	* 所属室マスタ取得（兼務込）
	*/
	function getList_add_conditions_emp_id($class_division ,$emp_id){
		$this->log->info("getList_add_conditions_emp_id() START");

		//SQL作成
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " room.room_id ";
		$sql .= " ,room.dept_id ";
		$sql .= " ,room.room_nm ";
		$sql .= "FROM ";
		$sql .= " classroom room ";

		if ($class_division <= 2) {
			$sql .= " INNER JOIN ";
			$sql .= "  deptmst dept ";
			$sql .= " ON ";
			$sql .= "  dept.dept_id = room.dept_id ";
			$sql .= "  AND dept.dept_del_flg = 'f' ";
		}

		if ($class_division == 1) {
			$sql .= " INNER JOIN ";
			$sql .= "  atrbmst atrb ";
			$sql .= " ON ";
			$sql .= "  atrb.atrb_id = dept.atrb_id ";
			$sql .= "  AND atrb.atrb_del_flg = 'f' ";
		}

		$sql .= "WHERE ";
		$sql .= " room.room_del_flg = 'f' ";
		$sql .= " AND ( ";
		$sql .= "  EXISTS ( ";
		$sql .= "   SELECT ";
		$sql .= "    emp.emp_id ";
		$sql .= "   FROM ";
		$sql .= "    empmst emp ";
		$sql .= "   WHERE ";
		$sql .= "    emp.emp_id = :emp_id ";

		switch($class_division){
			case 1:
				$sql .= "    AND emp.emp_class = atrb.class_id ";
				break;
			case 2:
				$sql .= "    AND emp.emp_attribute = dept.atrb_id ";
				break;
			case 3:
				$sql .= "    AND emp.emp_dept = room.dept_id ";
				break;
			case 4:
				$sql .= "    AND emp.emp_room = room.room_id ";
				break;
		}

		$sql .= "	) ";
		$sql .= "  OR EXISTS ( ";
		$sql .= "   SELECT ";
		$sql .= "    con.emp_id ";
		$sql .= "   FROM ";
		$sql .= "    concurrent con ";
		$sql .= "   WHERE ";
		$sql .= "    con.emp_id = :emp_id ";

		switch($class_division){
			case 1:
				$sql .= "    AND con.emp_class = atrb.class_id ";
				break;
			case 2:
				$sql .= "    AND con.emp_attribute = dept.atrb_id ";
				break;
			case 3:
				$sql .= "    AND con.emp_dept = room.dept_id ";
				break;
			case 4:
				$sql .= "    AND con.emp_room = room.room_id ";
				break;
		}

		$sql .= "  ) ";
		$sql .= " ) ";

		$data=parent::select($sql,array('text'),array('emp_id' => $emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->info("getList_add_conditions_emp_id() END");
		return $data;
	}
}
