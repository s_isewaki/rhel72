<?php
/**
 * ラダー管理の一覧データ取得用モデル
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_ladder_management_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_ladder_management_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	 * 一覧取得
	 */
	function get_user_list($param)
	{

		$this->log->info("get_user_list() START");

		$array_type = array();
		$array_param = array();
		//SQL作成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " emp.emp_id ";
		$sql .= " ,emp.emp_personal_id ";
		$sql .= " ,emp.emp_lt_nm ";
		$sql .= " ,emp.emp_ft_nm ";
		$sql .= " ,class.class_nm ";
		$sql .= " ,atrb.atrb_nm ";
		$sql .= " ,dept.dept_nm ";
		$sql .= " ,room.room_nm ";
		$sql .= " ,pro.now_level ";
		$sql .= " ,case when pro.now_level=1 then pro.level1_total_value ";
		$sql .= "       when pro.now_level=2 then pro.level2_total_value ";
		$sql .= "       when pro.now_level=3 then pro.level3_total_value ";
		$sql .= "       when pro.now_level=4 then pro.level4_total_value ";
		$sql .= "       when pro.now_level=5 then pro.level5_total_value ";
		$sql .= "       else null ";
		$sql .= "  end as level_total_value ";
		$sql .= " ,case when pro.now_level=1 then pro.get_level1_date ";
		$sql .= "       when pro.now_level=2 then pro.get_level2_date ";
		$sql .= "       when pro.now_level=3 then pro.get_level3_date ";
		$sql .= "       when pro.now_level=4 then pro.get_level4_date ";
		$sql .= "       when pro.now_level=5 then pro.get_level5_date ";
		$sql .= "       else null ";
		$sql .= "  end as get_level_date ";
		$sql .= " ,case when pro.now_level=1 then pro.level1_certificate_date ";
		$sql .= "       when pro.now_level=2 then pro.level2_certificate_date ";
		$sql .= "       when pro.now_level=3 then pro.level3_certificate_date ";
		$sql .= "       when pro.now_level=4 then pro.level4_certificate_date ";
		$sql .= "       when pro.now_level=5 then pro.level5_certificate_date ";
		$sql .= "       else null ";
		$sql .= "  end as level_certificate_date ";
		$sql .= "FROM ";
		$sql .= " empmst emp ";
		// 2012/08/24 Yamagawa add(s)
		$sql .= " INNER JOIN ";
		$sql .= "  jobmst job ";
		$sql .= " ON ";
		$sql .= "  job.job_id = emp.emp_job ";
		$sql .= "  AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/24 Yamagawa add(e)
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_personal_profile pro ";
		$sql .= " ON ";
		$sql .= "  emp.emp_id = pro.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  classmst class ";
		$sql .= " ON ";
		$sql .= "  emp.emp_class = class.class_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  atrbmst atrb ";
		$sql .= " ON ";
		$sql .= "  emp.emp_attribute = atrb.atrb_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  deptmst dept ";
		$sql .= " ON ";
		$sql .= "  emp.emp_dept = dept.dept_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  classroom room ";
		$sql .= " ON ";
		$sql .= "  emp.emp_room = room.room_id ";
		$sql .= "WHERE ";
		$sql .= " 1 = 1 ";

		// 2012/07/12 Yamagawa upd(s)
		//if ($param['login_emp_id'] != '') {
		if ($param['login_emp_id'] != '' && $param['class_division'] != 0) {
		// 2012/07/12 Yamagawa upd(e)
			$login_emp_id = $param['login_emp_id'];

			$sql .= " AND EXISTS( ";
			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";

			switch($param['class_division']){
				case 1:
					$sql .= "   AND emp.emp_class = loginemp.emp_class ";
					break;
				case 2:
					$sql .= "   AND emp.emp_attribute = loginemp.emp_attribute ";
					break;
				case 3:
					$sql .= "   AND emp.emp_dept = loginemp.emp_dept ";
					break;
				case 4:
					$sql .= "   AND emp.emp_room = loginemp.emp_room ";
					break;
			}

			$sql .= "  UNION ALL ";
			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent con ";
			$sql .= "   ON ";

			switch($param['class_division']){
				case 1:
					$sql .= "    loginemp.emp_class = con.emp_class ";
					break;
				case 2:
					$sql .= "    loginemp.emp_attribute = con.emp_attribute ";
					break;
				case 3:
					$sql .= "    loginemp.emp_dept = con.emp_dept ";
					break;
				case 4:
					$sql .= "    loginemp.emp_room = con.emp_room ";
					break;
			}

			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";
			$sql .= "   AND emp.emp_id = con.emp_id ";
			$sql .= "  UNION ALL ";
			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent logincon ";
			$sql .= "   ON ";
			$sql .= "    loginemp.emp_id = logincon.emp_id ";
			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";

			switch($param['class_division']){
				case 1:
					$sql .= "   AND emp.emp_class = logincon.emp_class ";
					break;
				case 2:
					$sql .= "   AND emp.emp_attribute = logincon.emp_attribute ";
					break;
				case 3:
					$sql .= "   AND emp.emp_dept = logincon.emp_dept ";
					break;
				case 4:
					$sql .= "   AND emp.emp_room = logincon.emp_room ";
					break;
			}

			$sql .= "  UNION ALL ";
			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent logincon ";
			$sql .= "   ON ";
			$sql .= "    loginemp.emp_id = logincon.emp_id ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent con ";
			$sql .= "   ON ";

			switch($param['class_division']){
				case 1:
					$sql .= "    logincon.emp_class = con.emp_class ";
					break;
				case 2:
					$sql .= "    logincon.emp_attribute = con.emp_attribute ";
					break;
				case 3:
					$sql .= "    logincon.emp_dept = con.emp_dept ";
					break;
				case 4:
					$sql .= "    logincon.emp_room = con.emp_room ";
					break;
			}

			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";
			$sql .= "   AND emp.emp_id = con.emp_id ";
			$sql .= " ) ";

		}

		if ($param['now_level'] != ''){
			$now_level = $param['now_level'];

			$sql .= " AND pro.now_level = $now_level ";
		}

		if ($param['emp_nm'] != '') {
			$emp_nm = $param['emp_nm'];

			$sql .= " AND ( ";
			$sql .= "  emp.emp_lt_nm || emp.emp_ft_nm LIKE '%' || '$emp_nm' || '%' ";
			$sql .= "  OR emp.emp_kn_lt_nm || emp.emp_kn_ft_nm LIKE '%' || '$emp_nm' || '%' ";
			$sql .= " ) ";
		}

		if ($param['class_id'] != '') {
			$class_id = $param['class_id'];
			$sql .= " AND emp.emp_class = $class_id ";
		}

		if ($param['atrb_id'] != '') {
			$atrb_id = $param['atrb_id'];
			$sql .= " AND emp.emp_attribute = $atrb_id ";
		}

		if ($param['dept_id'] != '') {
			$dept_id = $param['dept_id'];
			$sql .= " AND emp.emp_dept = $dept_id ";
		}

		if ($param['room_id'] != '') {
			$room_id = $param['room_id'];
			$sql .= " AND emp.emp_room = $room_id ";
		}

		if ($param['authorize_date_from'] != ''){
			$authorize_date_from = $param['authorize_date_from'];
			$sql .= " AND (case when pro.now_level=1 then pro.level1_certificate_date ";
			$sql .= "       when pro.now_level=2 then pro.level2_certificate_date ";
			$sql .= "       when pro.now_level=3 then pro.level3_certificate_date ";
			$sql .= "       when pro.now_level=4 then pro.level4_certificate_date ";
			$sql .= "       when pro.now_level=5 then pro.level5_certificate_date ";
			$sql .= "       else null ";
			$sql .= " end) >= '$authorize_date_from' ";
		}

		if ($param['authorize_date_to'] != ''){
			$authorize_date_to = $param['authorize_date_to'];
			$sql .= " AND (case when pro.now_level=1 then pro.level1_certificate_date ";
			$sql .= "       when pro.now_level=2 then pro.level2_certificate_date ";
			$sql .= "       when pro.now_level=3 then pro.level3_certificate_date ";
			$sql .= "       when pro.now_level=4 then pro.level4_certificate_date ";
			$sql .= "       when pro.now_level=5 then pro.level5_certificate_date ";
			$sql .= "       else null ";
			$sql .= " end) <= '$authorize_date_to' ";
		}

		// 2012/08/24 Yamagawa add(s)
		if ($param['retire_disp_flg'] == ''){
			$sql .= " AND ( ";
			$sql .= "  emp.emp_retire is null ";
			$sql .= "  OR trim(emp.emp_retire) = '' ";
			$sql .= "  OR to_date(trim(emp.emp_retire),'yyyymmdd') >= current_date ";
			$sql .= " ) ";
		}
		// 2012/08/24 Yamagawa add(e)

		$sql .= "ORDER BY ";
		$sql .= " emp.emp_personal_id ";

		$data=parent::select($sql,$array_type,$array_param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_user_list() END count(data):".count($data));
		return $data;
	}

}
