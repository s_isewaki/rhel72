<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_levelup_evaluation_colleague_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_levelup_evaluation_colleague_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	 * getEvaluationDataByLevelupApplyId レベルアップ申請の内容取得
	 *
	 * @param $apply_id    レベルアップ申請の申請ID
	 * @return           評価内容配列
	 */
	function getLevelup_Apply_List_byLevelupApplyId($levelup_apply_id){

		$this->log->info(__FUNCTION__." START");
		$this->log->debug("levelup_apply_id：".$levelup_apply_id);

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	lap.levelup_apply_id ";
		$sql .= " 	, lap.apply_id ";
		$sql .= " 	, lap.levelup_emp_id ";
		$sql .= " 	, lap.level ";
		$sql .= " 	, lap.apply_date ";
		$sql .= " 	, lap.attached_target_flg ";
		$sql .= " 	, lap.attached_episode_flg ";
		$sql .= " 	, lap.chief_emp_id ";
		$sql .= " 	, lap.colleague_emp_id ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_apply lap ";
		$sql .= " WHERE ";
		$sql .= " 	lap.levelup_apply_id = :levelup_apply_id ";

		$data=parent::select($sql,array("text"),array("levelup_apply_id"=>$levelup_apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;

	}

	/**
	 * getEvaluationDataByLevelupApplyId レベルアップ申請の内容取得
	 *
	 * @param $apply_id    レベルアップ申請の申請ID
	 * @return           評価内容配列
	 */
	function getLevelup_Evaluation_List_byApplyId($apply_id){

		$this->log->info(__FUNCTION__." START");
		$this->log->debug("levelup_apply_id：".$apply_id);

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	eval.levelup_evaluation_colleague_id ";
		$sql .= " 	, eval.apply_id ";
		$sql .= " 	, eval.levelup_apply_id ";
		$sql .= " 	, eval.levelup_emp_id ";
		$sql .= " 	, eval.level ";
		$sql .= " 	, eval.colleague_division ";
		$sql .= " 	, eval.colleague_emp_id ";
		$sql .= " 	, eval.colleague_evaluation_date ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_evaluation_colleague eval ";
		$sql .= " WHERE ";
		$sql .= " 	eval.apply_id = :apply_id ";

		$data=parent::select($sql,array("text"),array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;

	}

	/**
	 * getEvaluationItemList_byLevelupApplyId 評価表の内容取得
	 *
	 * @param $levelup_apply_id    レベルアップ申請ID
	 * @return           評価内容配列
	 */
	function getEvaluationItemList_byLevelupApplyId($levelup_apply_id){
		$this->log->info(__FUNCTION__." START");
		$this->log->debug("levelup_apply_id：".$levelup_apply_id);

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	lap.levelup_apply_id ";
		$sql .= " 	, lap.apply_id ";
		$sql .= " 	, lap.levelup_emp_id ";
		$sql .= " 	, lap.level ";
		$sql .= " 	, lap.apply_date ";
		$sql .= " 	, lap.attached_target_flg ";
		$sql .= " 	, lap.attached_episode_flg ";
		$sql .= " 	, lap.chief_emp_id ";
		$sql .= " 	, lap.colleague_emp_id ";
		$sql .= " 	, mst.evaluation_id ";
		$sql .= " 	, cat.category_id ";
		$sql .= " 	, grp.group_id ";
		$sql .= " 	, item.item_id ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_apply lap ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " WHERE ";
		$sql .= " 	lap.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 	AND mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 	AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 	AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 	AND cat.category_id = grp.category_id ";
		$sql .= " 	AND grp.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 	AND grp.evaluation_id = item.evaluation_id ";
		$sql .= " 	AND grp.category_id = item.category_id ";
		$sql .= " 	AND grp.group_id = item.group_id ";
		$sql .= " 	AND lap.levelup_apply_id = :levelup_apply_id ";
		$sql .= " ORDER BY ";
		$sql .= " 	mst.disp_order ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, grp.disp_order ";
		$sql .= " 	, item.disp_order ";

		$data=parent::select($sql,array("text"),array("levelup_apply_id"=>$levelup_apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;
	}

	/**
	 * getEvaluationList_forNewEval 評価表の内容取得
	 *
	 * @param $levelup_evaluation_colleague_id    レベルアップ評価表ID
	 * @return           評価内容配列
	 */
	function getEvaluationItemList_byEvaluationColleagueId($levelup_evaluation_colleague_id){

		$this->log->info(__FUNCTION__." START");
		$this->log->debug("levelup_evaluation_colleague_id：".$levelup_evaluation_colleague_id);

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	val.levelup_evaluation_value_id ";
		$sql .= " 	, val.evaluation_emp_division ";
		$sql .= " 	, val.levelup_evaluation_colleague_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.levelup_emp_id ";
		$sql .= " 	, val.evaluation_emp_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, val.group_id ";
		$sql .= " 	, val.item_id ";
		$sql .= " 	, val.value ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_evaluation_value val ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " WHERE ";
		$sql .= " 	val.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = mst.evaluation_id ";
		$sql .= " 	AND val.category_id = cat.category_id ";
		$sql .= " 	AND val.group_id = grp.group_id ";
		$sql .= " 	AND val.item_id = item.item_id ";
		$sql .= " 	AND mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 	AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 	AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 	AND cat.category_id = grp.category_id ";
		$sql .= " 	AND grp.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 	AND grp.evaluation_id = item.evaluation_id ";
		$sql .= " 	AND grp.category_id = item.category_id ";
		$sql .= " 	AND grp.group_id = item.group_id ";
		$sql .= " 	AND val.levelup_evaluation_colleague_id = :levelup_evaluation_colleague_id ";
		$sql .= " ORDER BY ";
		$sql .= " 	mst.disp_order ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, grp.disp_order ";
		$sql .= " 	, item.disp_order ";


		$data=parent::select($sql,array("text"),array("levelup_evaluation_colleague_id"=>$levelup_evaluation_colleague_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;

	}

	/**
	 * getEvaluationList_forEdit_div1 評価表の内容取得(自己、所属長評価)
	 *
	 * @param $levelup_apply_id    レベルアップ申請ID
	 * @return           評価内容配列
	**/
	function getEvaluationList_forEdit_div1($levelup_evaluation_id){

		$this->log->info(__FUNCTION__." START");
		$this->log->debug("levelup_evaluation_id：".$levelup_evaluation_id);

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	val.levelup_apply_id ";
		$sql .= " 	, mst.disp_order ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order AS cat_order ";
		$sql .= " 	, cat_grp_row.grp_cnt + cat_item_row.cat_item_cnt AS cat_rowspan ";
		$sql .= " 	, val.group_id ";
		$sql .= " 	, grp.group_name ";
		$sql .= " 	, grp.group_no ";
		$sql .= " 	, grp.disp_order AS grp_order ";
		$sql .= " 	, grprow.item_cnt + 1 AS grp_rowspan ";
		$sql .= " 	, item.item_id ";
		$sql .= " 	, item.item_name ";
		$sql .= " 	, item.disp_order AS item_order ";
		$sql .= " 	, item.eval_disp_1_flg ";
		$sql .= " 	, item.eval_disp_2_flg ";
		$sql .= " 	, item.eval_disp_3_flg ";
		$sql .= " 	, item.eval_disp_4_flg ";
		$sql .= " 	, max(CASE val.evaluation_emp_division ";
		$sql .= " 		WHEN 1 THEN val.value ";
		$sql .= " 		ELSE null ";
		$sql .= " 	END) AS eval1 ";
		$sql .= " 	, max(CASE val.evaluation_emp_division ";
		$sql .= " 		WHEN 2 THEN val.value ";
		$sql .= " 		ELSE null ";
		$sql .= " 	END) AS eval2 ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_evaluation_value as val ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	,(SELECT ";
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id ";
		$sql .= " 		, count(item_id) AS cat_item_cnt ";
		$sql .= " 	FROM ";
		$sql .= " 		cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	WHERE ";
		$sql .= " 		mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 		AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 		AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 		AND cat.category_id = grp.category_id ";
		$sql .= " 		AND grp.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 		AND grp.evaluation_id = item.evaluation_id ";
		$sql .= " 		AND grp.category_id = item.category_id ";
		$sql .= " 		AND grp.group_id = item.group_id ";
		$sql .= " 		AND cat.disp_flg = 1 ";
		$sql .= " 		AND cat.delete_flg = 0 ";
		$sql .= " 		AND grp.disp_flg = 1 ";
		$sql .= " 		AND grp.delete_flg = 0 ";
		$sql .= " 		AND item.disp_flg = 1 ";
		$sql .= " 		AND item.delete_flg = 0 ";
		$sql .= " 	group by ";
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id) cat_item_row ";
		$sql .= " 	,(SELECT ";
		$sql .= " 		grp.levelup_apply_id ";
		$sql .= " 		, grp.level ";
		$sql .= " 		, grp.evaluation_id ";
		$sql .= " 		, grp.category_id ";
		$sql .= " 		, count(grp.group_id) AS grp_cnt ";
		$sql .= " 	FROM ";
		$sql .= " 		cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	WHERE ";
		$sql .= " 		mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 		AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 		AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 		AND cat.category_id = grp.category_id ";
		//$sql .= " 		AND mst.use_flg = 1 ";
		$sql .= " 		AND cat.disp_flg = 1 ";
		$sql .= " 		AND cat.delete_flg = 0 ";
		$sql .= " 		AND grp.disp_flg = 1 ";
		$sql .= " 		AND grp.delete_flg = 0 ";
		$sql .= " 	group by ";
		$sql .= " 		grp.levelup_apply_id ";
		$sql .= " 		, grp.level ";
		$sql .= " 		, grp.evaluation_id ";
		$sql .= " 		, grp.category_id) AS cat_grp_row ";
		$sql .= " 	,(SELECT ";
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id ";
		$sql .= " 		, item.group_id ";
		$sql .= " 		, count(item_id) AS item_cnt ";
		$sql .= " 	FROM ";
		$sql .= " 		cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	WHERE ";
		$sql .= " 		mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 		AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 		AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 		AND cat.category_id = grp.category_id ";
		$sql .= " 		AND grp.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 		AND grp.evaluation_id = item.evaluation_id ";
		$sql .= " 		AND grp.category_id = item.category_id ";
		$sql .= " 		AND grp.group_id = item.group_id ";
		//$sql .= " 		AND mst.use_flg = 1 ";
		$sql .= " 		AND cat.disp_flg = 1 ";
		$sql .= " 		AND cat.delete_flg = 0 ";
		$sql .= " 		AND grp.disp_flg = 1 ";
		$sql .= " 		AND grp.delete_flg = 0 ";
		$sql .= " 		AND item.disp_flg = 1 ";
		$sql .= " 		AND item.delete_flg = 0 ";
		$sql .= " 	group by ";
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id ";
		$sql .= " 		, item.group_id) grprow ";
		$sql .= " WHERE ";
		$sql .= " 	val.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 	AND val.category_id = cat.category_id ";
		$sql .= " 	AND val.group_id = grp.group_id ";
		$sql .= " 	AND val.item_id = item.item_id ";
		$sql .= " 	AND mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 	AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 	AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 	AND cat.category_id = grp.category_id ";
		$sql .= " 	AND cat.levelup_apply_id = cat_item_row.levelup_apply_id ";
		$sql .= " 	AND cat.evaluation_id = cat_item_row.evaluation_id ";
		$sql .= " 	AND cat.category_id = cat_item_row.category_id ";
		$sql .= " 	AND cat.levelup_apply_id = cat_grp_row.levelup_apply_id ";
		$sql .= " 	AND cat.evaluation_id = cat_grp_row.evaluation_id ";
		$sql .= " 	AND cat.category_id = cat_grp_row.category_id ";
		$sql .= " 	AND grp.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 	AND grp.evaluation_id = item.evaluation_id ";
		$sql .= " 	AND grp.category_id = item.category_id ";
		$sql .= " 	AND grp.group_id = item.group_id ";
		$sql .= " 	AND grp.levelup_apply_id = grprow.levelup_apply_id ";
		$sql .= " 	AND grp.evaluation_id = grprow.evaluation_id ";
		$sql .= " 	AND grp.category_id = grprow.category_id ";
		$sql .= " 	AND grp.group_id = grprow.group_id ";
		//$sql .= " 	AND mst.use_flg = 1 ";
		$sql .= " 	AND cat.disp_flg = 1 ";
		$sql .= " 	AND cat.delete_flg = 0 ";
		$sql .= " 	AND grp.disp_flg = 1 ";
		$sql .= " 	AND grp.delete_flg = 0 ";
		$sql .= " 	AND item.disp_flg = 1 ";
		$sql .= " 	AND item.delete_flg = 0 ";
		$sql .= " 	AND val.evaluation_emp_division in (1,2) ";
		$sql .= " 	AND val.levelup_evaluation_id = :levelup_evaluation_id ";
		$sql .= " GROUP BY ";
		$sql .= " 	val.levelup_apply_id ";
		$sql .= " 	, mst.disp_order ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, cat_grp_row.grp_cnt + cat_item_row.cat_item_cnt ";
		$sql .= " 	, val.group_id ";
		$sql .= " 	, grp.group_no ";
		$sql .= " 	, grp.group_name ";
		$sql .= " 	, grp.disp_order ";
		$sql .= " 	, grprow.item_cnt + 1 ";
		$sql .= " 	, item.item_id ";
		$sql .= " 	, item.item_name ";
		$sql .= " 	, item.disp_order ";
		$sql .= " 	, item.eval_disp_1_flg ";
		$sql .= " 	, item.eval_disp_2_flg ";
		$sql .= " 	, item.eval_disp_3_flg ";
		$sql .= " 	, item.eval_disp_4_flg ";
		$sql .= " 	, mst.passing_mark ";
		$sql .= " ORDER BY ";
		$sql .= " 	mst.disp_order ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, grp.disp_order ";
		$sql .= " 	, item.disp_order ";


		$data=parent::select($sql,array("text"),array("levelup_evaluation_id"=>$levelup_evaluation_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;

	}

	/**
	 * getEvaluationList_forEdit_div2 評価表の内容取得(同僚評価)
	 *
	 * @param $levelup_apply_id    レベルアップ申請ID
	 * @return           評価内容配列
	**/
	function getEvaluationList_forEdit_div2($levelup_evaluation_id, $login_user){

		$this->log->info(__FUNCTION__." START");
		$this->log->debug("levelup_evaluation_colleague_id：".$levelup_evaluation_id);
		$this->log->debug("evaluation_emp_id：".$login_user);

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	item.levelup_apply_id ";
		$sql .= " 	, mst.disp_order ";
		$sql .= " 	, item.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order AS cat_order ";
		$sql .= " 	, cat_grp_row.grp_cnt + cat_item_row.cat_item_cnt AS cat_rowspan ";
		$sql .= " 	, item.group_id ";
		$sql .= " 	, grp.group_no ";
		$sql .= " 	, grp.group_name ";
		$sql .= " 	, grp.disp_order AS grp_order ";
		$sql .= " 	, grprow.item_cnt + 1 AS grp_rowspan ";
		$sql .= " 	, item.item_id ";
		$sql .= " 	, item.item_name ";
		$sql .= " 	, item.disp_order AS item_order ";
		$sql .= " 	, item.eval_disp_1_flg ";
		$sql .= " 	, item.eval_disp_2_flg ";
		$sql .= " 	, item.eval_disp_3_flg ";
		$sql .= " 	, item.eval_disp_4_flg ";
		$sql .= " 	, val.value AS eval1 ";
		$sql .= " 	, null AS eval2 ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	,(SELECT ";
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id ";
		$sql .= " 		, count(item_id) AS cat_item_cnt ";
		$sql .= " 	FROM ";
		$sql .= " 		cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	WHERE ";
		$sql .= " 		mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 		AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 		AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 		AND cat.category_id = grp.category_id ";
		$sql .= " 		AND grp.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 		AND grp.evaluation_id = item.evaluation_id ";
		$sql .= " 		AND grp.category_id = item.category_id ";
		$sql .= " 		AND grp.group_id = item.group_id ";
		$sql .= " 		AND cat.disp_flg = 1 ";
		$sql .= " 		AND cat.delete_flg = 0 ";
		$sql .= " 		AND grp.disp_flg = 1 ";
		$sql .= " 		AND grp.delete_flg = 0 ";
		$sql .= " 		AND item.disp_flg = 1 ";
		$sql .= " 		AND item.delete_flg = 0 ";
		$sql .= " 	group by ";
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id) cat_item_row ";
		$sql .= " 	,(SELECT ";
		$sql .= " 		grp.levelup_apply_id ";
		$sql .= " 		, grp.level ";
		$sql .= " 		, grp.evaluation_id ";
		$sql .= " 		, grp.category_id ";
		$sql .= " 		, count(grp.group_id) AS grp_cnt ";
		$sql .= " 	FROM ";
		$sql .= " 		cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	WHERE ";
		$sql .= " 		mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 		AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 		AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 		AND cat.category_id = grp.category_id ";
		$sql .= " 		AND cat.disp_flg = 1 ";
		$sql .= " 		AND cat.delete_flg = 0 ";
		$sql .= " 		AND grp.disp_flg = 1 ";
		$sql .= " 		AND grp.delete_flg = 0 ";
		$sql .= " 	group by ";
		$sql .= " 		grp.levelup_apply_id ";
		$sql .= " 		, grp.level ";
		$sql .= " 		, grp.evaluation_id ";
		$sql .= " 		, grp.category_id) AS cat_grp_row ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	,(SELECT ";
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id ";
		$sql .= " 		, item.group_id ";
		$sql .= " 		, count(item_id) AS item_cnt ";
		$sql .= " 	FROM ";
		$sql .= " 		cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	WHERE ";
		$sql .= " 		mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 		AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 		AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 		AND cat.category_id = grp.category_id ";
		$sql .= " 		AND grp.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 		AND grp.evaluation_id = item.evaluation_id ";
		$sql .= " 		AND grp.category_id = item.category_id ";
		$sql .= " 		AND grp.group_id = item.group_id ";
		$sql .= " 		AND cat.disp_flg = 1 ";
		$sql .= " 		AND cat.delete_flg = 0 ";
		$sql .= " 		AND grp.disp_flg = 1 ";
		$sql .= " 		AND grp.delete_flg = 0 ";
		$sql .= " 		AND item.disp_flg = 1 ";
		$sql .= " 		AND item.delete_flg = 0 ";
		$sql .= " 	group by ";
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id ";
		$sql .= " 		, item.group_id) grprow ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	, cl_apl_levelup_evaluation_value val ";
		$sql .= " WHERE ";
		$sql .= " 	mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 	AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 	AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 	AND cat.category_id = grp.category_id ";
		$sql .= " 	AND cat.levelup_apply_id = cat_item_row.levelup_apply_id ";
		$sql .= " 	AND cat.evaluation_id = cat_item_row.evaluation_id ";
		$sql .= " 	AND cat.category_id = cat_item_row.category_id ";
		$sql .= " 	AND cat.levelup_apply_id = cat_grp_row.levelup_apply_id ";
		$sql .= " 	AND cat.evaluation_id = cat_grp_row.evaluation_id ";
		$sql .= " 	AND cat.category_id = cat_grp_row.category_id ";
		$sql .= " 	AND grp.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 	AND grp.evaluation_id = item.evaluation_id ";
		$sql .= " 	AND grp.category_id = item.category_id ";
		$sql .= " 	AND grp.group_id = item.group_id ";
		$sql .= " 	AND grp.levelup_apply_id = grprow.levelup_apply_id ";
		$sql .= " 	AND grp.evaluation_id = grprow.evaluation_id ";
		$sql .= " 	AND grp.category_id = grprow.category_id ";
		$sql .= " 	AND grp.group_id = grprow.group_id ";
		$sql .= " 	AND item.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	AND item.evaluation_id = val.evaluation_id ";
		$sql .= " 	AND item.category_id = val.category_id ";
		$sql .= " 	AND item.group_id = val.group_id ";
		$sql .= " 	AND item.item_id = val.item_id ";
		$sql .= " 	AND cat.disp_flg = 1 ";
		$sql .= " 	AND cat.delete_flg = 0 ";
		$sql .= " 	AND grp.disp_flg = 1 ";
		$sql .= " 	AND grp.delete_flg = 0 ";
		$sql .= " 	AND item.disp_flg = 1 ";
		$sql .= " 	AND item.delete_flg = 0 ";
		$sql .= " 	AND val.levelup_evaluation_colleague_id = :levelup_evaluation_colleague_id ";
		$sql .= " 	AND val.evaluation_emp_id = :evaluation_emp_id ";
		$sql .= " ORDER BY ";
		$sql .= " 	mst.disp_order ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, grp.disp_order ";
		$sql .= " 	, item.disp_order ";


		$data=parent::select($sql,array("text","text"),array("levelup_evaluation_colleague_id"=>$levelup_evaluation_id,"evaluation_emp_id"=>$login_user),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;

	}

	function getEvaluationDataByApplyId($apply_id){
		$this->log->info(__FUNCTION__." START");

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	eval.levelup_evaluation_id ";
		$sql .= " 	, eval.apply_id ";
		$sql .= " 	, eval.levelup_apply_id ";
		$sql .= " 	, eval.levelup_emp_id ";
		$sql .= " 	, eval.level ";
		$sql .= " 	, eval.self_evaluation_date ";
		$sql .= " 	, eval.supervisor_emp_id ";
		$sql .= " 	, eval.supervisor_evalution_date ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_evaluation eval ";
		$sql .= " WHERE ";
		$sql .= " 	eval.apply_id = :apply_id ";

		$data=parent::select($sql,array("text"),array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;

	}

	function getEvaluationValueByLevelupEvaluationId($levelup_evaluation_id){

		$this->log->info(__FUNCTION__." START");

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	val.levelup_evaluation_value_id ";
		$sql .= " 	, val.evaluation_emp_division ";
		$sql .= " 	, val.levelup_evaluation_id ";
		$sql .= " 	, val.levelup_evaluation_colleague_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.levelup_emp_id ";
		$sql .= " 	, val.evaluation_emp_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, val.group_id ";
		$sql .= " 	, val.item_id ";
		$sql .= " 	, val.value ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_evaluation_value val ";
		$sql .= " 	, cl_apl_levelup_evaluation eval ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " WHERE ";
		$sql .= " 	val.levelup_evaluation_id = eval.levelup_evaluation_id ";
		$sql .= " 	AND val.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = mst.evaluation_id ";
		$sql .= " 	AND val.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = cat.evaluation_id ";
		$sql .= " 	AND val.category_id = cat.category_id ";
		$sql .= " 	AND val.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = grp.evaluation_id ";
		$sql .= " 	AND val.category_id = grp.category_id ";
		$sql .= " 	AND val.group_id = grp.group_id ";
		$sql .= " 	AND val.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = item.evaluation_id ";
		$sql .= " 	AND val.category_id = item.category_id ";
		$sql .= " 	AND val.group_id = item.group_id ";
		$sql .= " 	AND val.item_id = item.item_id ";
		$sql .= " 	AND eval.levelup_evaluation_id = :levelup_evaluation_id ";
		$sql .= " ORDER BY ";
		$sql .= " 	val.evaluation_emp_division ";
		$sql .= " 	, mst.disp_order ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, grp.disp_order ";
		$sql .= " 	, item.disp_order ";


		$data=parent::select($sql,array("text"),array("levelup_evaluation_id"=>$levelup_evaluation_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;
	}

	function getEvaluationValueByApplyIdAndDiv($apply_id,$evaluation_emp_division){

		$this->log->info(__FUNCTION__." START");

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	val.levelup_evaluation_value_id ";
		$sql .= " 	, val.evaluation_emp_division ";
		$sql .= " 	, val.levelup_evaluation_id ";
		$sql .= " 	, val.levelup_evaluation_colleague_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.levelup_emp_id ";
		$sql .= " 	, val.evaluation_emp_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, val.group_id ";
		$sql .= " 	, val.item_id ";
		$sql .= " 	, val.value ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_evaluation_value val ";
		$sql .= " 	, cl_apl_levelup_evaluation eval ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " WHERE ";
		$sql .= " 	val.levelup_evaluation_id = eval.levelup_evaluation_id ";
		$sql .= " 	AND val.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = mst.evaluation_id ";
		$sql .= " 	AND val.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = cat.evaluation_id ";
		$sql .= " 	AND val.category_id = cat.category_id ";
		$sql .= " 	AND val.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = grp.evaluation_id ";
		$sql .= " 	AND val.category_id = grp.category_id ";
		$sql .= " 	AND val.group_id = grp.group_id ";
		$sql .= " 	AND val.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = item.evaluation_id ";
		$sql .= " 	AND val.category_id = item.category_id ";
		$sql .= " 	AND val.group_id = item.group_id ";
		$sql .= " 	AND val.item_id = item.item_id ";
		$sql .= " 	AND eval.apply_id = :apply_id ";
		//$sql .= " 	AND val.levelup_apply_id = :apply_id ";
		$sql .= " 	AND val.evaluation_emp_division = :evaluation_emp_division ";
		$sql .= " ORDER BY ";
		$sql .= " 	val.evaluation_emp_division ";
		$sql .= " 	, mst.disp_order ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, grp.disp_order ";
		$sql .= " 	, item.disp_order ";


		$data=parent::select($sql,array("text","integer"),array("apply_id"=>$apply_id,"evaluation_emp_division"=>$evaluation_emp_division),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;
	}

}
