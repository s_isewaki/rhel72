<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_mst_inside_training_schedule_util extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     */
    function cl_mst_inside_training_schedule_util($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * getList 全件取得
     * @param type $plan_id
     * @param type $year
     */
    // 2012/11/16 Yamagawa upd(s)
    //function getList($training_id){
    function getList($training_id ,$year){
    // 2012/11/16 Yamagawa upd(e)

        $this->log->info(__FUNCTION__." START");

        /* SQL */
	    $sql_buff[] =  "SELECT sc.plan_id             , ";
	    $sql_buff[] =         "sc.training_id         , ";
	    $sql_buff[] =         "sc.plan_no             , ";
	    $sql_buff[] =         "sc.year                , ";
        $sql_buff[] =         "sc.term_div           , ";
	    $sql_buff[] =         "replace(replace(replace(sc.term_div,0,'指定なし'),1,'上期'),2,'下期') as term_div_str           , ";
	    $sql_buff[] =         "CASE WHEN sc.training_time > tr.max_training_time THEN NULL WHEN sc.final_flg = 1 THEN '最終回' ELSE '第' || sc.training_time || '回' END AS training_time, ";
	    $sql_buff[] =         "sc.plan_date           , ";
	    $sql_buff[] =         "to_char( sc.plan_date , 'yyyy/mm/dd') as plan_date_str          , ";
	    $sql_buff[] =         "sc.from_time           , ";
	    $sql_buff[] =         "to_char( sc.from_time , 'hh24') as from_time_hh24          , ";
	    $sql_buff[] =         "to_char( sc.from_time , 'mi'  ) as from_time_mi          , ";
	    $sql_buff[] =         "sc.to_time             , ";
	    $sql_buff[] =         "to_char( sc.to_time , 'hh24') as to_time_hh24          , ";
	    $sql_buff[] =         "to_char( sc.to_time , 'mi'  ) as to_time_mi          , ";
	    $sql_buff[] =         "sc.report_division     , ";
	    $sql_buff[] =         "replace(replace(sc.report_division,0,'なし'),1,'あり') as report_division_str           , ";
        $sql_buff[] =         "answer_division     , ";
	    $sql_buff[] =         "replace(replace(sc.answer_division,0,'なし'),1,'あり') as answer_division_str           , ";
        $sql_buff[] =         "sc.max_people          , ";

        $sql_buff[] =         "sc.apply_status        , ";

        $sql_buff[] =         "replace(replace(replace(replace(replace(sc.apply_status,0,'準備中'),1,'受付中'),2,'受付完了'),3,'中止'),4,'日程変更') as apply_status_str        , ";


        $sql_buff[] =         "sc.place               , ";
        $sql_buff[] =         "sc.remarks             , ";
        $sql_buff[] =         "replace(replace(replace(sc.remarks,chr(13)||chr(10),'<br>'),chr(13),'<br>'),chr(10),'<br>') as remarks_br           , ";
        $sql_buff[] =         "sc.delete_flg          , ";
        $sql_buff[] =         "sc.create_date         , ";
        $sql_buff[] =         "sc.create_user         , ";
        $sql_buff[] =         "sc.update_date         , ";
        $sql_buff[] =         "sc.update_user         , ";

        $sql_buff[] =         "COALESCE(u_cnt.cnt, 0) as u_cnt , ";
        $sql_buff[] =         "COALESCE(s_cnt.cnt, 0) as s_cnt   ";



        $sql_buff[] =   "FROM cl_mst_inside_training_schedule AS sc ";

        $sql_buff[] =   "LEFT JOIN ( select plan_id , count(*) AS cnt from cl_inside_training_roll where delete_flg <> 1                  group by plan_id ) AS u_cnt on u_cnt.plan_id = sc.plan_id ";
        $sql_buff[] =   "LEFT JOIN ( select plan_id , count(*) AS cnt from cl_inside_training_roll where delete_flg <> 1 and emp_roll = 1 group by plan_id ) AS s_cnt on s_cnt.plan_id = sc.plan_id ";
        $sql_buff[] =   "LEFT JOIN cl_mst_inside_training tr ON sc.training_id = tr.training_id ";

        $sql_buff[] =  "WHERE sc.training_id = :training_id ";
        $sql_buff[] =    "AND sc.delete_flg <> 1 ";
        // 2012/11/16 Yamagawa add(s)
		$sql_buff[] =  " AND sc.year = :year ";
        // 2012/11/16 Yamagawa add(e)
        $sql_buff[] =  "ORDER BY sc.year,sc.term_div,sc.plan_date , ";
	    $sql_buff[] =           "sc.from_time , ";
        $sql_buff[] =           "sc.to_time   , ";
        $sql_buff[] =           "sc.plan_id     ";
        $sql = implode('', $sql_buff);

        //select count(*) from cl_inside_training_roll where plan_id = 'CLITS00000003'
        //select count(*) from cl_inside_training_roll where plan_id = 'CLITS00000003' and emp_roll = 1


        /* カラムタイプ */
		// 2012/11/16 Yamagawa upd(s)
		//$type = array('text');
		//$param = array('training_id'=>$training_id);
		$type = array('text','integer');
		$param = array('training_id'=>$training_id,'year'=>$year);
		// 2012/11/16 Yamagawa upd(e)

        $data=parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data;

     }

    /**
     * numberingPlanNo
     * @param type $training_id
     */
    function numberingPlanNo($training_id){

    	$this->log->info("numberingPlanNo() START");

    	$cnt = $this->getCount($training_id);
        $this->log->debug("cnt:".$cnt);

        $rtn=0;
        if($cnt>0){
        	$max_plan_no=$this->getMaxPlanNo($training_id);
        	$rtn=$max_plan_no+1;

        }
        else{
        	$rtn=1;
        }
	    $this->log->info("numberingPlanNo() END");
	    return $rtn;

     }

    /**
     * getMaxPlanNo
     * @param type $training_id
     */
    function getMaxPlanNo($training_id){

    	$this->log->info("getMaxPlanNo() START");

	    /* レコード取得用SQL */
	    $sql_buff[] = "SELECT max(plan_no) as max_plan_no ";
	    $sql_buff[] =   "FROM cl_mst_inside_training_schedule ";
	    $sql_buff[] =  "WHERE training_id = :training_id";
        $sql = implode('', $sql_buff);

	    $data=parent::getRecord($sql,array("text"),array("training_id"=>$training_id),MDB2_FETCHMODE_ASSOC,0);

	    $this->log->info("getMaxPlanNo() END");
	    return $data["max_plan_no"];

     }

     /**
     * getCount
     * @param type $plan_id
     */
    function getCount($training_id){

	     /* レコード取得用SQL */
	    $sql_buff[] = "SELECT count(*) as count ";
	    $sql_buff[] =   "FROM cl_mst_inside_training_schedule ";
	    $sql_buff[] =  "WHERE training_id = :training_id";
        $sql = implode('', $sql_buff);

    	$this->log->info("getCount() START");
        $data=parent::getRecord($sql,array("text"),array("training_id"=>$training_id),MDB2_FETCHMODE_ASSOC,0);
        $this->log->info("getCount() END");
        return $data["count"];

     }

    /**
     * getListForReInsert
     * @param type $training_id
     */
    function getListForReInsert($training_id){

		/* 再登録用データ取得 */
		$sql_buff[] = "SELECT ";
		$sql_buff[] = " sch.plan_id ";
		$sql_buff[] = " ,sch.first_plan_id ";
		$sql_buff[] = " ,sch.training_id ";
		$sql_buff[] = " ,sch.plan_no ";
		$sql_buff[] = " ,sch.year ";
		$sql_buff[] = " ,sch.term_div ";
		$sql_buff[] = " ,sch.training_time ";
		$sql_buff[] = " ,sch.plan_date ";
		$sql_buff[] = " ,sch.from_time ";
		$sql_buff[] = " ,sch.to_time ";
		$sql_buff[] = " ,sch.report_division ";
		$sql_buff[] = " ,sch.answer_division ";
		$sql_buff[] = " ,sch.max_people ";
		$sql_buff[] = " ,sch.apply_status ";
		$sql_buff[] = " ,sch.place ";
		$sql_buff[] = " ,sch.remarks ";
		$sql_buff[] = " ,sch.delete_flg ";
		$sql_buff[] = "FROM ";
		$sql_buff[] = " cl_mst_inside_training_schedule sch ";
		$sql_buff[] = "WHERE ";
		$sql_buff[] = " sch.training_id = :training_id ";
		$sql_buff[] = " AND sch.delete_flg = 0 ";
		$sql = implode('', $sql_buff);

		$this->log->info("getListForReInsert() START");
		$data=parent::select($sql,array("text"),array("training_id"=>$training_id),MDB2_FETCHMODE_ASSOC);
		$this->log->info("getListForReInsert() END");
		return $data;

	}
}
