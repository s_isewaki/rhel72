<?php
/**
 * 研修講座一覧画面の一覧データ取得用モデル
 *
 *
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_training_select_list_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_training_select_list_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }





	/**
	* 研修講座一覧画面の研修一覧部分のデータを取得します。
	* ・対象者が次に目標とするラダーレベルで必須もしくは任意となっている研修の一覧を取得します。
	* ・師長推薦が指定された場合は、レベルに関係なく、全ての研修の一覧を取得します。
	*
	* @param $emp_id 申請者のemp_id。
	* @param $target_level 対象者が次に目標とするラダーレベル。
	* @param $head_nurse_recommend_flg 師長推薦フラグ。「1」の場合に師長推薦。
	* @param $year 受講年
	*/
    // 2012/11/20 Yamagawa upd(s)
	//function get_training_list($emp_id,$target_level,$head_nurse_recommend_flg)
	function get_training_list($emp_id,$target_level,$head_nurse_recommend_flg,$year)
	{
		$this->log->debug("get_training_list() START");

		//SQL作成
		$sql = "";
		$sql .= " SELECT ";
		$sql .= " A.*, ";
		$sql .= " case when B.inside_training_id is null then 0 else 1 end as applied_flg ";

		$sql .= " FROM";
		$sql .= " (";
		$sql .= " SELECT";
		$sql .= "   training_id,";
		$sql .= "   training_name,";
		$sql .= "   training_purpose,";
		if($target_level >= 1 and $target_level <= 5)
		{
			$sql .= "   target_level$target_level as target_level_required,";
		}
		elseif($target_level > 5)
		{
			$sql .= "   0 as target_level_required,";//既にレベル５の場合は全ての研修がレベルの対象外となる。
		}
		$sql .= "   training_require,";
		$sql .= "   time_division";
		$sql .= " FROM cl_mst_inside_training";
		$sql .= " WHERE abolition_flag <> 1";  //1:非廃止
		$sql .= " AND delete_flg <> 1";
		$sql .= " ) A";

		//申込み済みの研修IDリストと結合
		$sql .= " left join";
		$sql .= " (";
		// 2012/11/20 Yamagawa upd(s)
		//$sql .= "   select distinct inside_training_id from";
		$sql .= "   select distinct inside_training_id ,year from";
		// 2012/11/20 Yamagawa upd(e)
		$sql .= "   (";

		$sql .= "     select inside_training_id ";
		// 2012/11/20 Yamagawa add(s)
		$sql .= "     ,year ";
		// 2012/11/20 Yamagawa add(e)
		$sql .= "     from cl_applied_inside_training "; //申込み済みの申請
		$sql .= "     where emp_id = :emp_id";
		$sql .= "     and delete_flg = 0";

		$sql .= "     union all";

		$sql .= "     select a.inside_training_id";
		// 2012/11/20 Yamagawa add(s)
		$sql .= "     ,a.year ";
		// 2012/11/20 Yamagawa add(e)
		$sql .= "     from cl_apl_inside_training_apply a"; //申込み中の申請
		$sql .= "     inner join cl_apply b";
		$sql .= "     on a.apply_id = b.apply_id";
		$sql .= "     where a.emp_id = :emp_id";
		$sql .= "     and b.apply_stat = 0"; //0:申請中
		$sql .= "     and not b.draft_flg";  //下書き以外
		$sql .= "     and not b.delete_flg"; //未削除のみ

		$sql .= "   ) B";
		$sql .= " ) B";
		$sql .= " on A.training_id = B.inside_training_id";
		// 2012/11/20 Yamagawa add(s)
		$sql .= " AND B.year = :year ";
		// 2012/11/20 Yamagawa add(e)


		//師長推薦以外の場合は、レベルの対象となっていない研修を除外する。
		if($head_nurse_recommend_flg != 1)
		{
			$sql .= " WHERE target_level_required <> 0";
		}

		//研修ID順
		$sql .= " ORDER BY training_id";

		// 2012/11/20 Yamagawa upd(s)
		//$data=parent::select($sql,array("text"),array("emp_id"=>$emp_id),MDB2_FETCHMODE_ASSOC);
		$data=parent::select($sql,array("text","integer"),array("emp_id"=>$emp_id,"year"=>$year),MDB2_FETCHMODE_ASSOC);
		// 2012/11/20 Yamagawa upd(e)

		$this->log->debug("get_training_list() END data:".print_r($data,true));
		$this->log->debug("get_training_list() END count(data):".count($data));
		return $data;
	}


	/**
	* 研修講座一覧画面の日程一覧部分のデータを取得します。
	* ・指定された研修の日程一覧を取得します。
	* ・出欠の情報を集計し、申し込みの現在人数も取得します。
	*
	* @param $training_id 研修ID
	* @param $year 受講年
	*/
	// 2012/11/19 Yamagawa upd(s)
	//function get_plan_list($training_id)
	function get_plan_list($training_id ,$year)
	// 2012/11/19 Yamagawa upd(e)
	{
		$this->log->debug("get_plan_list() START");

		$sql = "";
		$sql .= " SELECT";
		$sql .= "   A.training_id,";
		$sql .= "   A.plan_id,";
		$sql .= "   A.plan_no,";
		$sql .= "   A.training_time,";
		$sql .= "   A.final_flg,";
		$sql .= "   A.plan_date,";
		$sql .= "   A.from_time,";
		$sql .= "   A.to_time,";
		$sql .= "   A.report_division,";
		$sql .= "   A.answer_division,";
		$sql .= "   A.max_people,";
		$sql .= "   A.apply_status,";
		$sql .= "   A.place,";
		$sql .= "   A.remarks,";
		$sql .= "   case when B.training_roll_count is null then 0 else B.training_roll_count end as training_roll_count,";
		$sql .= "   A.delete_flg";
		$sql .= " FROM";
		$sql .= " (";
		$sql .= "   SELECT";
		$sql .= "     training_id,";
		$sql .= "     plan_id,";
		$sql .= "     plan_no,";
		$sql .= "     training_time,";
		$sql .= "     final_flg,";
		$sql .= "     plan_date,";
		$sql .= "     from_time,";
		$sql .= "     to_time,";
		$sql .= "     report_division,";
		$sql .= "     answer_division,";
		$sql .= "     max_people,";
		$sql .= "     apply_status,";
		$sql .= "     place,";
		$sql .= "     remarks,";
		$sql .= "     delete_flg";
		$sql .= "   FROM cl_mst_inside_training_schedule ";
		$sql .= "   WHERE training_id = :training_id ";
		// 2012/11/19 Yamagawa add(s)
		$sql .= "   AND year = :year ";
		// 2012/11/19 Yamagawa add(e)
		$sql .= "   AND delete_flg <> 1";       //1:削除済み
		$sql .= " ) A";
		$sql .= " NATURAL LEFT JOIN";
		$sql .= " (";
		$sql .= "   SELECT";
		$sql .= "     training_id,";
		$sql .= "     plan_id,count(*) as training_roll_count";
		$sql .= "   FROM cl_inside_training_roll";
		$sql .= "   WHERE training_id = :training_id ";
		$sql .= "   AND delete_flg <> 1";       //1:削除済み
		$sql .= "   AND emp_roll <> 2";          //2:キャンセル
		$sql .= "   GROUP BY training_id,plan_id";
		$sql .= " ) B";
		$sql .= " ORDER BY training_time,plan_date,from_time,to_time,plan_no";

		// 2012/11/19 Yamagawa upd(s)
		//$data=parent::select($sql,array("text"),array("training_id"=>$training_id),MDB2_FETCHMODE_ASSOC);
		$data=parent::select($sql,array("text","integer"),array("training_id"=>$training_id,"year"=>$year),MDB2_FETCHMODE_ASSOC);
		// 2012/11/19 Yamagawa upd(e)

		$this->log->debug("get_plan_list() END data:".print_r($data,true));
		$this->log->debug("get_plan_list() END count(data):".count($data));
		return $data;
	}

	/**
	* 研修講座一覧画面の日程一覧部分のデータを取得します。
	* ・指定された研修の日程一覧を取得します。
	* ・出欠の情報を集計し、申し込みの現在人数も取得します。
	*
	* @param $training_id 研修ID
	* @param $selected_plan_id_csv 日程ID配列(CSV形式)
	* @param $year 受講年
	*/
	// 2012/11/19 Yamagawa upd(s)
	//function get_plan_list_for_modify($training_id, $selected_plan_id_csv)
	function get_plan_list_for_modify($training_id, $selected_plan_id_csv, $year)
	// 2012/11/19 Yamagawa upd(e)
	{
		$this->log->debug("get_plan_list_for_modify() START");
		$this->log->debug("training_id:".$training_id);
		$this->log->debug("selected_plan_id_csv:".$selected_plan_id_csv);

		//シングルクォート付きCSVに変換
		//例)CLITS00000001,CLITS00000003 →'CLITS00000001','CLITS00000003'
		$plan_id_list = "'".str_replace(",", "','", $selected_plan_id_csv)."'";
		$this->log->debug("plan_id_list:".$plan_id_list);

		$sql = "";
		$sql .= " SELECT";
		$sql .= "   A.training_id,";
		$sql .= "   A.plan_id,";
		$sql .= "   A.plan_no,";
		$sql .= "   A.training_time,";
		$sql .= "   A.final_flg,";
		$sql .= "   A.plan_date,";
		$sql .= "   A.from_time,";
		$sql .= "   A.to_time,";
		$sql .= "   A.report_division,";
		$sql .= "   A.answer_division,";
		$sql .= "   A.max_people,";
		$sql .= "   A.apply_status,";
		$sql .= "   A.place,";
		$sql .= "   A.remarks,";
		$sql .= "   case when B.training_roll_count is null then 0 else B.training_roll_count end as training_roll_count,";
		$sql .= "   A.delete_flg,";
		$sql .= "   case when A.plan_id in($plan_id_list) then 1 else 0 end as selected_flg";
		// 2012/11/05 Yamagawa add(s)
		$sql .= "   ,CASE WHEN C.training_id IS NULL THEN 0 ELSE 1 END AS reported_flg ";
		// 2012/11/05 Yamagawa add(e)
		$sql .= " FROM";
		$sql .= " (";
		$sql .= "   SELECT";
		$sql .= "     training_id,";
		$sql .= "     plan_id,";
		$sql .= "     plan_no,";
		$sql .= "     training_time,";
		$sql .= "     final_flg,";
		$sql .= "     plan_date,";
		$sql .= "     from_time,";
		$sql .= "     to_time,";
		$sql .= "     report_division,";
		$sql .= "     answer_division,";
		$sql .= "     max_people,";
		$sql .= "     apply_status,";
		$sql .= "     place,";
		$sql .= "     remarks,";
		$sql .= "     delete_flg,";
		//$sql .= "     case when plan_id in(:plan_id_list) then 1 else 0 end as selected_flg";
		$sql .= "     case when plan_id in($plan_id_list) then 1 else 0 end as selected_flg";
		$sql .= "   FROM cl_mst_inside_training_schedule ";
		$sql .= "   WHERE training_id = :training_id ";
		// 2012/11/19 Yamagawa add(s)
		$sql .= "   AND year = :year ";
		// 2012/11/19 Yamagawa add(e)
		$sql .= "   AND";
		$sql .= "   (";
		$sql .= "     delete_flg <> 1";
		$sql .= "     OR";
		//$sql .= "     plan_id in(:plan_id_list)";
		$sql .= "     plan_id in($plan_id_list)";
		$sql .= "   )";
		$sql .= " ) A";
		$sql .= " NATURAL LEFT JOIN";
		$sql .= " (";
		$sql .= "   SELECT";
		$sql .= "     training_id,";
		$sql .= "     plan_id,count(*) as training_roll_count";
		$sql .= "   FROM cl_inside_training_roll";
		$sql .= "   WHERE training_id = :training_id ";
		$sql .= "   AND delete_flg <> 1";       //1:削除済み
		$sql .= "   AND emp_roll <> 2";          //2:キャンセル
		$sql .= "   GROUP BY training_id,plan_id";
		$sql .= " ) B";
		// 2012/11/05 Yamagawa add(s)
		// 報告中、報告済みかをチェックする
		$sql .= " LEFT JOIN ";
		$sql .= " ( ";
		$sql .= "  SELECT DISTINCT ";
		$sql .= "   rep.training_id ";
		$sql .= "   ,rep.plan_id ";
		$sql .= "  FROM ";
		$sql .= "   cl_apl_inside_training_report rep ";
		$sql .= "   INNER JOIN ";
		$sql .= "    cl_apply app ";
		$sql .= "   ON ";
		$sql .= "    app.apply_id = rep.apply_id ";
		$sql .= "    AND app.apply_stat IN ('0','1') ";		// 申請中 or 承認済み
		$sql .= "    AND app.delete_flg = FALSE ";		// 申請取消以外
		$sql .= "    AND app.draft_flg = FALSE ";		// 下書き以外
		$sql .= "  WHERE ";
		$sql .= "   rep.emp_id = :emp_id ";
		$sql .= "   AND rep.training_id = :training_id ";
		$sql .= "   AND rep.delete_flg != 1 ";			// 削除済み以外
		$sql .= " ) C ";
		$sql .= " ON ";
		$sql .= "  C.training_id = A.training_id ";
		$sql .= "  AND C.plan_id = A.plan_id ";
		// 2012/11/05 Yamagawa add(e)
		$sql .= " ORDER BY training_time,selected_flg DESC,plan_date,from_time,to_time,plan_no";

		// 2012/11/05 Yamagawa upd(s)
		//$data=parent::select($sql,array("text"),array("training_id"=>$training_id),MDB2_FETCHMODE_ASSOC);
		$data=parent::select(
			$sql
			,array(
				"text"
				,"text"
				// 2012/11/19 Yamagawa add(s)
				,"integer"
				// 2012/11/19 Yamagawa add(e)
			)
			,array(
				"training_id"=>$training_id
				,"emp_id"=>$this->user
				// 2012/11/19 Yamagawa add(s)
				,"year"=>$year
				// 2012/11/19 Yamagawa add(e)
			)
			,MDB2_FETCHMODE_ASSOC
		);
		// 2012/11/05 Yamagawa upd(e)

		$this->log->debug("get_plan_list_for_modify() END data:".print_r($data,true));
		$this->log->debug("get_plan_list_for_modify() END count(data):".count($data));
		return $data;
	}

}
