<?php
/**
 * レベルアップ認定データ取得用モデル
 *
 *
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_levelup_recognize_list_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_levelup_recognize_list_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }





	/**
	* 院外研修一覧画面の研修一覧部分のデータを取得します。
	*
	* @param $apply_id 申請ID。
	*/
	function get_levelup_recognize_data($apply_id)
	{
		$this->log->debug("get_levelup_recognize_data() START");

		//SQL作成
		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	levelup_recognize_id ";
		$sql .= " 	, apply_id ";
		$sql .= " 	, levelup_apply_id ";
		$sql .= " 	, levelup_emp_id ";
		$sql .= " 	, level ";
		$sql .= " 	, recognize_emp_id ";
		$sql .= " 	, recognize_date ";
		$sql .= " 	, recognize_flg ";
		$sql .= " 	, reason ";
		$sql .= " 	, total_value ";
		$sql .= " 	, delete_flg ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_recognize ";
		$sql .= " WHERE ";
		$sql .= " 	apply_id = :apply_id ";


		$data=parent::select($sql,array("text"),array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_levelup_recognize_data() END data:".print_r($data,true));
		$this->log->debug("get_levelup_recognize_data() END count(data):".count($data));
		return $data[0];
	}

}
