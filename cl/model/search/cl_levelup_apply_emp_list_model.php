<?php
/**
 * レベルアップ対象選択画面の一覧データ取得用モデル
 *
 *
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_levelup_apply_emp_list_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_levelup_apply_emp_list_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	* レベルアップ対象選択画面のレベルアップ申請データを取得します。
	*
	* @param $target_level 対象レベル。
	*/
	function get_levelup_apply_emp_list($target_level,$emp_id,$class_division)
	{
		$this->log->debug("get_levelup_apply_data() START");

		//SQL作成
		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	lvapl.levelup_apply_id ";
		$sql .= " 	, lvapl.levelup_emp_id ";
		$sql .= " 	, emp.emp_lt_nm || '　' || emp.emp_ft_nm AS emp_name ";
		$sql .= " 	,  CASE WHEN emp.emp_room IS NULL THEN class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm ";
		$sql .= " 		ELSE class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm || ' > ' || room.room_nm ";
		$sql .= " 	END as affiliation ";
		// 2012/08/14 Yamagawa upd(s)
		//$sql .= " 	, CASE WHEN emp.emp_join IS NOT NULL THEN date_trunc('month',age(current_timestamp, to_timestamp(emp.emp_join,'YYYYMMDD'))) ";
		//$sql .= " 		ELSE NULL ";
		//$sql .= " 	END as exp_years ";
		$sql .= " 	,CASE ";
		$sql .= " 	  WHEN ";
		$sql .= " 	   OCTET_LENGTH(TRIM(emp.emp_join)) = 8 ";
		$sql .= " 	  THEN ";
		$sql .= " 	   CASE ";
		$sql .= " 	    WHEN ";
		$sql .= " 	     OCTET_LENGTH(TRIM(emp.emp_retire)) = 8 ";
		$sql .= " 	    THEN ";
		$sql .= " 	     TO_DATE(emp.emp_retire,'YYYYMMDD') - TO_DATE(emp.emp_join,'YYYYMMDD') + 1 ";
		$sql .= " 	    ELSE ";
		$sql .= " 	     CURRENT_DATE - TO_DATE(emp.emp_join,'YYYYMMDD') + 1 ";
		$sql .= " 	   END ";
		$sql .= " 	  ELSE ";
		$sql .= " 	   NULL ";
		$sql .= " 	 END AS exp_dates ";
		$sql .= " 	,COALESCE(car.car_exp_dates,0) AS car_exp_dates ";
		$sql .= " 	,COALESCE(mat.mat_exp_dates,0) AS mat_exp_dates ";
		$sql .= " 	,COALESCE(par.par_exp_dates,0) AS par_exp_dates ";
		// 2012/08/14 Yamagawa upd(e)
		$sql .= " 	, prof.now_level ";
		$sql .= " 	, lvapl.level ";
		$sql .= " 	, lvapl.apply_date ";
		$sql .= " 	, lvapl.chief_emp_id ";
		$sql .= " 	, lvapl.colleague_emp_id ";
		$sql .= " FROM ";
		$sql .= " 	(SELECT ";
		$sql .= " 		lva.levelup_apply_id ";
		$sql .= " 		, lva.apply_id ";
		$sql .= " 		, lva.levelup_emp_id ";
		$sql .= " 		, lva.level ";
		//$sql .= " 		, lva.apply_date ";
		$sql .= " 	, 	substr(apl1.apply_date, 1, 4)||'/'|| substr(apl1.apply_date, 5, 2)||'/'||substr(apl1.apply_date, 7, 2) as apply_date ";
		$sql .= " 		, lva.attached_target_flg ";
		$sql .= " 		, lva.attached_episode_flg ";
		$sql .= " 		, lva.chief_emp_id ";
		$sql .= " 		, lva.colleague_emp_id ";
		$sql .= " 	FROM ";
		$sql .= " 		cl_apl_levelup_apply lva ";
		$sql .= " 		, cl_apl_levelup_evaluation as eval ";
//		$sql .= " 		, cl_apl_levelup_evaluation_colleague coll1 ";
//		$sql .= " 		, cl_apl_levelup_evaluation_colleague coll2 ";
		$sql .= " 		, cl_apply apl1 ";
//		$sql .= " 		, cl_apply apl2 ";
//		$sql .= " 		, cl_apply apl3 ";
		$sql .= " 	WHERE ";
		$sql .= " 		lva.levelup_apply_id = eval.levelup_apply_id ";
		// 2012/07/20 Yamagawa upd(s)
//		$sql .= " 		AND coll1.apply_id = apl1.apply_id ";
		$sql .= "		AND eval.apply_id = apl1.apply_id ";
		// 2012/07/20 Yamagawa upd(e)
		// 2012/09/13 Yamagawa add(s)
		$sql .= " 		AND apl1.delete_flg = FALSE ";
		// 2012/09/13 Yamagawa add(e)
		$sql .= " 		AND apl1.apply_stat = '1' ";
//		$sql .= " 		AND lva.levelup_apply_id = coll1.levelup_apply_id ";
//		$sql .= " 		AND lva.chief_emp_id = coll1.colleague_emp_id ";
//		$sql .= " 		AND coll2.apply_id = apl2.apply_id ";
//		$sql .= " 		AND apl2.apply_stat = '1' ";
//		$sql .= " 		AND lva.levelup_apply_id = coll2.levelup_apply_id ";
//		$sql .= " 		AND lva.colleague_emp_id = coll2.colleague_emp_id ";
//		$sql .= " 		AND coll2.apply_id = apl3.apply_id ";
//		$sql .= " 		AND apl3.apply_stat = '1') lvapl ";
		$sql .= " 		) lvapl ";
		$sql .= " 	, empmst emp ";
		$sql .= " 	LEFT JOIN cl_personal_profile prof ON emp.emp_id = prof.emp_id ";
		$sql .= " 	LEFT JOIN classmst AS class ON emp.emp_class = class.class_id ";
		$sql .= " 	LEFT JOIN atrbmst AS atrb ON emp.emp_attribute = atrb.atrb_id ";
		$sql .= " 	LEFT JOIN deptmst AS dept ON emp.emp_dept = dept.dept_id ";
		$sql .= " 	LEFT JOIN classroom AS room ON emp.emp_room = room.room_id ";
// 2012/08/14 Yamagawa add(s)
		$sql .= "  INNER JOIN ";
		$sql .= "   ( ";
		$sql .= "    SELECT ";
		$sql .= "     emp.emp_id ";
		$sql .= "     ,SUM( ";
		$sql .= "      TO_DATE(car.career_ed_date,'YYYYMMDD') - TO_DATE(car.career_st_date,'YYYYMMDD') + 1 ";
		$sql .= "     ) AS car_exp_dates ";
		$sql .= "    FROM ";
		$sql .= "     empmst emp ";
		$sql .= "     LEFT JOIN ";
		$sql .= "      jinji_relation rel ";
		$sql .= "     ON ";
		$sql .= "      rel.emp_id = emp.emp_id ";
		$sql .= "     LEFT JOIN ";
		$sql .= "      jinji_career car ";
		$sql .= "     ON ";
		$sql .= "      car.jinji_id = rel.jinji_id ";
		$sql .= "      AND OCTET_LENGTH(car.career_st_date) = 8 ";
		$sql .= "      AND OCTET_LENGTH(car.career_ed_date) = 8 ";
		$sql .= "    GROUP BY ";
		$sql .= "     emp.emp_id ";
		$sql .= "   ) car ";
		$sql .= "  ON ";
		$sql .= "   car.emp_id = emp.emp_id ";
		$sql .= "  INNER JOIN ";
		$sql .= "   ( ";
		$sql .= "    SELECT ";
		$sql .= "     emp.emp_id ";
		$sql .= "     ,SUM( ";
		$sql .= "       CASE ";
		$sql .= "        WHEN ";
		$sql .= "         OCTET_LENGTH(mat.end_date) = 8 ";
		$sql .= "        THEN ";
		$sql .= "         TO_DATE(mat.end_date,'YYYYMMDD') - TO_DATE(mat.start_date,'YYYYMMDD') + 1 ";
		$sql .= "        ELSE ";
		$sql .= "         CURRENT_DATE - TO_DATE(mat.start_date,'YYYYMMDD') + 1 ";
		$sql .= "       END ";
		$sql .= "     ) AS mat_exp_dates ";
		$sql .= "    FROM ";
		$sql .= "     empmst emp ";
		$sql .= "     LEFT JOIN ";
		$sql .= "      jinji_relation rel ";
		$sql .= "     ON ";
		$sql .= "      rel.emp_id = emp.emp_id ";
		$sql .= "     LEFT JOIN ";
		$sql .= "      jinji_maternity_leave mat ";
		$sql .= "     ON ";
		$sql .= "      mat.jinji_id = rel.jinji_id ";
		$sql .= "      AND OCTET_LENGTH(mat.start_date) = 8 ";
		$sql .= "    GROUP BY ";
		$sql .= "     emp.emp_id ";
		$sql .= "   ) mat ";
		$sql .= "  ON ";
		$sql .= "   mat.emp_id = emp.emp_id ";
		$sql .= "  INNER JOIN ";
		$sql .= "   ( ";
		$sql .= "    SELECT ";
		$sql .= "     emp.emp_id ";
		$sql .= "     ,SUM( ";
		$sql .= "       CASE ";
		$sql .= "        WHEN ";
		$sql .= "         OCTET_LENGTH(par.end_date) = 8 ";
		$sql .= "        THEN ";
		$sql .= "         TO_DATE(par.end_date,'YYYYMMDD') - TO_DATE(par.start_date,'YYYYMMDD') + 1 ";
		$sql .= "        ELSE ";
		$sql .= "         CURRENT_DATE - TO_DATE(par.start_date,'YYYYMMDD') + 1 ";
		$sql .= "       END ";
		$sql .= "     ) AS par_exp_dates ";
		$sql .= "    FROM ";
		$sql .= "     empmst emp ";
		$sql .= "     LEFT JOIN ";
		$sql .= "      jinji_relation rel ";
		$sql .= "     ON ";
		$sql .= "      rel.emp_id = emp.emp_id ";
		$sql .= "     LEFT JOIN ";
		$sql .= "      jinji_parental_leave par ";
		$sql .= "     ON ";
		$sql .= "      par.jinji_id = rel.jinji_id ";
		$sql .= "      AND OCTET_LENGTH(par.start_date) = 8 ";
		$sql .= "    GROUP BY ";
		$sql .= "     emp.emp_id ";
		$sql .= "   ) par ";
		$sql .= "  ON ";
		$sql .= "   par.emp_id = emp.emp_id ";
// 2012/08/14 Yamagawa add(e)
		$sql .= " 	,(SELECT ";
		$sql .= " 		mst2.emp_id ";
		$sql .= " 	FROM ";
		$sql .= " 		empmst mst1 ";
		$sql .= " 		, empmst mst2 ";
		$sql .= " 	WHERE ";
		$sql .= " 		mst1.emp_id = '$emp_id' ";
		if($class_division >= "1"){
			$sql .= " 		AND mst1.emp_class = mst2.emp_class ";
		}
		if($class_division >= "2"){
			$sql .= " 		AND mst1.emp_attribute = mst2.emp_attribute ";
		}
		if($class_division >= "3"){
			$sql .= " 		AND mst1.emp_dept = mst2.emp_dept ";
		}
		if($class_division >= "4"){
			$sql .= " 		AND mst1.emp_room = mst2.emp_room ";
		}
		$sql .= " 	UNION ";
		$sql .= " 	SELECT ";
		$sql .= " 		mst2.emp_id ";
		$sql .= " 	FROM ";
		$sql .= " 		empmst mst1 ";
		$sql .= " 		, concurrent mst2 ";
		$sql .= " 	WHERE ";
		$sql .= " 		mst1.emp_id = '$emp_id' ";
		if($class_division >= "1"){
			$sql .= " 		AND mst1.emp_class = mst2.emp_class ";
		}
		if($class_division >= "2"){
			$sql .= " 		AND mst1.emp_attribute = mst2.emp_attribute ";
		}
		if($class_division >= "3"){
			$sql .= " 		AND mst1.emp_dept = mst2.emp_dept ";
		}
		if($class_division >= "4"){
			$sql .= " 		AND mst1.emp_room = mst2.emp_room ";
		}
		$sql .= " 	UNION ";
		$sql .= " 	SELECT ";
		$sql .= " 		mst2.emp_id ";
		$sql .= " 	FROM ";
		$sql .= " 		concurrent mst1 ";
		$sql .= " 		, empmst mst2 ";
		$sql .= " 	WHERE ";
		$sql .= " 		mst1.emp_id = '$emp_id' ";
		if($class_division >= "1"){
			$sql .= " 		AND mst1.emp_class = mst2.emp_class ";
		}
		if($class_division >= "2"){
			$sql .= " 		AND mst1.emp_attribute = mst2.emp_attribute ";
		}
		if($class_division >= "3"){
			$sql .= " 		AND mst1.emp_dept = mst2.emp_dept ";
		}
		if($class_division >= "4"){
			$sql .= " 		AND mst1.emp_room = mst2.emp_room ";
		}
		$sql .= " 	UNION ";
		$sql .= " 	SELECT ";
		$sql .= " 		mst2.emp_id ";
		$sql .= " 	FROM ";
		$sql .= " 		concurrent mst1 ";
		$sql .= " 		, concurrent mst2 ";
		$sql .= " 	WHERE ";
		$sql .= " 		mst1.emp_id = '$emp_id' ";
		if($class_division >= "1"){
			$sql .= " 		AND mst1.emp_class = mst2.emp_class ";
		}
		if($class_division >= "2"){
			$sql .= " 		AND mst1.emp_attribute = mst2.emp_attribute ";
		}
		if($class_division >= "3"){
			$sql .= " 		AND mst1.emp_dept = mst2.emp_dept ";
		}
		if($class_division >= "4"){
			$sql .= " 		AND mst1.emp_room = mst2.emp_room ";
		}
		$sql .= " 	) emp_list ";
		$sql .= " WHERE ";
		$sql .= " 	lvapl.levelup_emp_id = emp.emp_id ";
		$sql .= " 	AND emp.emp_id = emp_list.emp_id ";


//		$sql .= " 	AND NOT EXISTS (SELECT rec.levelup_apply_id FROM cl_apl_levelup_recognize rec WHERE rec.levelup_apply_id = lvapl.levelup_apply_id AND rec.delete_flg = 0 ) ";

		//「論理削除されていない、申請中もしくは申請済みのレベルアップ認定で、使用されているレベルアップ申請を対象外とする」
		$sql .= " 	AND NOT EXISTS (";
		$sql .= " 	SELECT rec.levelup_apply_id";
		$sql .= " 	FROM";
		$sql .= " 	(";

		//$sql .= " 	SELECT a.* FROM cl_apl_levelup_recognize a";
		//$sql .= " 	INNER JOIN";
		//$sql .= " 	(SELECT apply_id FROM cl_apply WHERE apply_stat IN(0,1)) b";//申請中、申請済み
		//$sql .= " 	ON a.apply_id = b.apply_id";
		//$sql .= " 	) rec";
		//$sql .= " 	WHERE rec.levelup_apply_id = lvapl.levelup_apply_id AND rec.delete_flg = 0";

		$sql .= " 	SELECT a.* FROM cl_apl_levelup_recognize a";
		$sql .= " 	INNER JOIN";
		$sql .= " 	(SELECT apply_id FROM cl_apply WHERE apply_stat IN(0,1) AND not delete_flg ) b";//申請中、申請済み(未削除)
		$sql .= " 	ON a.apply_id = b.apply_id";
		$sql .= " 	) rec";
		$sql .= " 	WHERE rec.levelup_apply_id = lvapl.levelup_apply_id";//rec.delete_flgは判定しない。管理画面で申請削除した場合は再認定を可能にする。

		$sql .= " 	) ";



		$sql .= " 	AND lvapl.level = :target_level ";
		$sql .= " ORDER BY ";
		$sql .= " 	levelup_emp_id ";
		$sql .= " 	,levelup_apply_id ";

		$data=parent::select($sql,array("integer"),array("target_level"=>$target_level),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_levelup_apply_data() END data:".print_r($data,true));
		$this->log->debug("get_levelup_apply_data() END count(data):".count($data));
		return $data;
	}
}
