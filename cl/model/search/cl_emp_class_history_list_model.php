<?php
/**
 * 研修講座一覧画面の一覧データ取得用モデル
 *
 *
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_emp_class_history_list_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_emp_class_history_list_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }



	/**
	* 現在の職員データを取得します。
	*
	* @param $emp_id 職員ID
	*/
	function get_emp_data($emp_id)
	{
		$this->log->debug("get_emp_data() START",__FILE__,__LINE__);

		//SQL作成
		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	emp_id ";
		$sql .= " 	, emp_personal_id ";
		$sql .= " 	, emp_class ";
		$sql .= " 	, emp_attribute ";
		$sql .= " 	, emp_dept ";
		$sql .= " 	, emp_room ";
		$sql .= " FROM ";
		$sql .= " 	empmst ";
		$sql .= " WHERE ";
		$sql .= " 	emp_id = :emp_id ";

		$data=parent::select($sql,array("text"),array("emp_id"=>$emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_emp_data() END data:".print_r($data,true),__FILE__,__LINE__);
		$this->log->debug("get_emp_data() END count():".count($data),__FILE__,__LINE__);
		return $data;
	}

	/**
	* 職員データの所属履歴を取得します。
	*
	* @param $emp_id 職員ID
	*/
	function get_emp_data_history($emp_id)
	{
		$this->log->debug("get_emp_data() START",__FILE__,__LINE__);

		//SQL作成
		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	emp.emp_id ";
		$sql .= " 	, emp.emp_class ";
		$sql .= " 	, emp.emp_attribute ";
		$sql .= " 	, emp.emp_dept ";
		$sql .= " 	, emp.emp_room ";
		$sql .= " 	, his.histdate ";
		$sql .= " 	, his.class_id AS his_class ";
		$sql .= " 	, his.atrb_id AS his_atrib ";
		$sql .= " 	, his.dept_id AS his_dept ";
		$sql .= " 	, his.room_id AS his_room ";
		$sql .= " FROM ";
		$sql .= " 	empmst emp LEFT JOIN class_history his ON emp.emp_id = his.emp_id ";
		$sql .= " WHERE ";
		$sql .= " 	emp.emp_id = :emp_id ";
		$sql .= " ORDER BY ";
		$sql .= " 	his.histdate ";

		$data=parent::select($sql,array("text"),array("emp_id"=>$emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_emp_data() END data:".print_r($data,true),__FILE__,__LINE__);
		$this->log->debug("get_emp_data() END count():".count($data),__FILE__,__LINE__);
		return $data;
	}

}
