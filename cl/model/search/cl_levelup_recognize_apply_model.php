<?php
/**
 * 院外研修一覧画面の一覧データ取得用モデル
 *
 *
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_levelup_recognize_apply_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_levelup_recognize_apply_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	* レベルアップ認定 研修受講データを取得します。
	*
	* @param $levelup_apply_id レベルアップ申請ID
	*/
	function get_levelup_apply_training_list($levelup_apply_id)
	{
		$this->log->debug("get_levelup_apply_training_list() START");

		//SQL作成
		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	lvup.levelup_apply_id ";
		$sql .= " 	, 1 as display_order ";
		$sql .= " 	, lvup.training_id ";
		$sql .= " 	, mst.training_name as training_nm ";
		$sql .= " 	, lvup.must_division ";
		$sql .= " 	, lvup.pass_flg ";
		$sql .= " 	, 1 as participation_division ";
		$sql .= " 	, varchar(1) '1' as in_out_kbn ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_apply_inside_training lvup ";
		$sql .= " 	, cl_mst_inside_training mst ";
		$sql .= " WHERE ";
		$sql .= " 	lvup.training_id = mst.training_id ";
		$sql .= " 	AND lvup.levelup_apply_id = :levelup_apply_id ";
		$sql .= " UNION ";
		$sql .= " SELECT ";
		$sql .= " 	lvup.levelup_apply_id ";
		$sql .= " 	, lvup.disp_order ";
		$sql .= " 	, lvup.seminar_apply_id as training_id ";
		$sql .= " 	, lvup.seminar_name as training_nm ";
		$sql .= " 	, 1 as must_division ";
		$sql .= " 	, 1 as pass_flg ";
		$sql .= " 	, lvup.participation_division ";
		$sql .= " 	, varchar(1) '2' as in_out_kbn ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_apply_outside_seminar lvup ";
		$sql .= " WHERE ";
		$sql .= " 	lvup.levelup_apply_id = :levelup_apply_id ";
		$sql .= " ORDER BY ";
		$sql .= " 	in_out_kbn ";
		$sql .= " 	, display_order ";
		$sql .= " 	, must_division ";
		$sql .= " 	, training_id ";

		$data=parent::select($sql,array("text"),array("levelup_apply_id"=>$levelup_apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_levelup_apply_training_list() END data:".print_r($data,true));
		$this->log->debug("get_levelup_apply_training_list() END count(data):".count($data));
		return $data;
	}

	/**
	* レベルアップ認定 評価データを取得します。
	*
	* @param $levelup_apply_id レベルアップ申請ID
	*/
	function get_evaluation_data_list($levelup_apply_id)
	{
		$this->log->debug("get_evaluation_data_list() START");

		//SQL作成
		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	val.evaluation_emp_division ";
		$sql .= " 	, val.levelup_evaluation_id ";
		$sql .= " 	, val.levelup_evaluation_colleague_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.levelup_emp_id ";
		$sql .= " 	, val.evaluation_emp_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, sum(val.value) as cat_value ";
		$sql .= " 	, count(CASE WHEN val.value < COALESCE(item.must_value,0) THEN 1 END) AS fales_count ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_evaluation_value val ";
		// 2012/09/13 Yamagawa upd(s)
		/*
		$sql .= " 	, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " WHERE ";
		$sql .= " 	val.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = cat.evaluation_id ";
		$sql .= " 	AND val.category_id = cat.category_id ";
		$sql .= " 	AND cat.disp_flg = 1 ";
		$sql .= " 	AND val.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = item.evaluation_id ";
		$sql .= " 	AND val.category_id = item.category_id ";
		$sql .= " 	AND val.group_id = item.group_id ";
		$sql .= " 	AND val.item_id = item.item_id ";
		$sql .= " 	AND item.disp_flg = 1 ";
		$sql .= " 	AND val.levelup_apply_id = :levelup_apply_id ";
		*/
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	ON ";
		$sql .= " 	 cat.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	 AND cat.evaluation_id = val.evaluation_id ";
		$sql .= " 	 AND cat.category_id = val.category_id ";
		$sql .= " 	 AND cat.disp_flg = 1 ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	ON ";
		$sql .= " 	 item.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	 AND item.evaluation_id = val.evaluation_id ";
		$sql .= " 	 AND item.category_id = val.category_id ";
		$sql .= " 	 AND item.group_id = val.group_id ";
		$sql .= " 	 AND item.item_id = val.item_id ";
		$sql .= " 	 AND item.disp_flg = 1 ";
		$sql .= " WHERE ";
		$sql .= " 	val.levelup_apply_id = :levelup_apply_id ";
		$sql .= " 	AND ( ";
		$sql .= " 	 EXISTS( ";
		$sql .= " 	  SELECT ";
		$sql .= " 	   eva.levelup_evaluation_id ";
		$sql .= " 	  FROM ";
		$sql .= " 	   cl_apl_levelup_evaluation eva ";
		$sql .= " 	   INNER JOIN ";
		$sql .= " 	    cl_apply apl ";
		$sql .= " 	   ON ";
		$sql .= " 	    apl.apply_id = eva.apply_id ";
		$sql .= " 	    AND apl.delete_flg = FALSE ";
		$sql .= " 	    AND apl.apply_stat = 1 ";
		$sql .= " 	  WHERE ";
		$sql .= " 	   eva.levelup_evaluation_id = val.levelup_evaluation_id ";
		$sql .= " 	  ) ";
		$sql .= " 	  OR EXISTS( ";
		$sql .= " 	   SELECT ";
		$sql .= " 	    eva.levelup_evaluation_colleague_id ";
		$sql .= " 	   FROM ";
		$sql .= " 	    cl_apl_levelup_evaluation_colleague eva ";
		$sql .= " 	    INNER JOIN ";
		$sql .= " 	     cl_apply apl ";
		$sql .= " 	    ON ";
		$sql .= " 	     apl.apply_id = eva.apply_id ";
		$sql .= " 	     AND apl.delete_flg = FALSE ";
		$sql .= " 	     AND apl.apply_stat = 1 ";
		$sql .= " 	   WHERE ";
		$sql .= " 	    eva.levelup_evaluation_colleague_id = val.levelup_evaluation_colleague_id ";
		$sql .= " 	  ) ";
		$sql .= " 	 ) ";
		// 2012/09/13 Yamagawa upd(e)
		$sql .= " GROUP BY ";
		$sql .= " 	val.evaluation_emp_division ";
		$sql .= " 	, val.levelup_evaluation_id ";
		$sql .= " 	, val.levelup_evaluation_colleague_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.levelup_emp_id ";
		$sql .= " 	, val.evaluation_emp_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " ORDER BY ";
		$sql .= " 	evaluation_emp_division ";
		$sql .= " 	, disp_order ";


		$data=parent::select($sql,array("text"),array("levelup_apply_id"=>$levelup_apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_evaluation_data_list() END data:".print_r($data,true));
		$this->log->debug("get_evaluation_data_list() END count(data):".count($data));
		return $data;
	}

	/**
	* 申請データ 合格条件を取得します。
	*
	* @param $levelup_apply_id レベルアップ申請ID
	*/
	function get_pass_conditions($levelup_apply_id){
		$this->log->debug("get_pass_conditions() START");

		//SQL作成
		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	item.levelup_apply_item_id ";
		$sql .= " 	, item.levelup_apply_id ";
		$sql .= " 	, item.item_id ";
		$sql .= " 	, item.level ";
		$sql .= " 	, item.evaluation_id ";
		$sql .= " 	, item.category_id ";
		$sql .= " 	, cat.disp_order AS cat_order ";
		$sql .= " 	, item.group_id ";
		// 2012/09/21 Yamagawa upd(s)
		//$sql .= " 	, grp.disp_order AS grp_order ";
		$sql .= " 	, grp.group_no AS grp_order ";
		// 2012/09/21 Yamagawa upd(e)
		$sql .= " 	, item.item_name ";
		// 2012/09/21 Yamagawa upd(s)
		//$sql .= " 	, item.disp_order AS item_order ";
		$sql .= " 	, COALESCE(item.disp_order,0) + 1 AS item_order ";
		// 2012/09/21 Yamagawa upd(e)
		$sql .= " 	, item.disp_flg ";
		$sql .= " 	, item.must_value ";
		$sql .= " 	, mst.passing_mark ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " WHERE ";
		$sql .= " 	mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 	AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 	AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 	AND cat.category_id = grp.category_id ";
		$sql .= " 	AND grp.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 	AND grp.evaluation_id = item.evaluation_id ";
		$sql .= " 	AND grp.category_id = item.category_id ";
		$sql .= " 	AND grp.group_id = item.group_id ";
		$sql .= " 	AND cat.disp_flg = 1 ";
		$sql .= " 	AND cat.delete_flg = 0 ";
		$sql .= " 	AND grp.disp_flg = 1 ";
		$sql .= " 	AND grp.delete_flg = 0 ";
		$sql .= " 	AND item.disp_flg = 1 ";
		$sql .= " 	AND item.delete_flg = 0 ";
		$sql .= " 	AND mst.levelup_apply_id = :levelup_apply_id ";
		$sql .= " ORDER BY ";
		$sql .= " 	mst.disp_order ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, grp.disp_order ";
		$sql .= " 	, item.disp_order ";

		$data=parent::select($sql,array("text"),array("levelup_apply_id"=>$levelup_apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_pass_conditions() END data:".print_r($data,true));
		$this->log->debug("get_pass_conditions() END count(data):".count($data));
		return $data;
	}

	/**
	* 評価マスタ カテゴリーを取得します。
	*
	* @param $target_level 対象レベル
	*/
	function get_mst_evaluation_category_list($target_level){

		$this->log->debug("get_mst_evaluation_category_list() START");

		//SQL作成
		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	mst.level ";
		$sql .= " 	, mst.evaluation_id ";
		$sql .= " 	, cat.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, grp.group_id ";
		$sql .= " 	, item.item_id ";
		$sql .= " 	, item.disp_order AS item_order ";
		$sql .= " 	, mst.passing_mark ";
		$sql .= " 	, item.must_value ";
		$sql .= " FROM ";
		$sql .= " 	cl_mst_evaluation mst ";
		$sql .= " 	, cl_mst_evaluation_category cat ";
		$sql .= " 	, cl_mst_evaluation_group grp ";
		$sql .= " 	, cl_mst_evaluation_item item ";
		$sql .= " WHERE ";
		$sql .= " 	mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 	AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 	AND cat.category_id = grp.category_id ";
		$sql .= " 	AND grp.evaluation_id = item.evaluation_id ";
		$sql .= " 	AND grp.category_id = item.category_id ";
		$sql .= " 	AND grp.group_id = item.group_id ";
		$sql .= " 	AND cat.disp_flg = 1 ";
		$sql .= " 	AND cat.delete_flg = 0 ";
		$sql .= " 	AND grp.disp_flg = 1 ";
		$sql .= " 	AND grp.delete_flg = 0 ";
		$sql .= " 	AND item.disp_flg = 1 ";
		$sql .= " 	AND item.delete_flg = 0 ";
		$sql .= " 	AND mst.level = :target_level ";
		$sql .= " ORDER BY ";
		$sql .= " 	mst.disp_order ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, grp.disp_order ";
		$sql .= " 	, item.disp_order ";


		$data=parent::select($sql,array("integer"),array("target_level"=>$target_level),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_mst_evaluation_category_list() END data:".print_r($data,true));
		$this->log->debug("get_mst_evaluation_category_list() END count(data):".count($data));
		return $data;

	}

	/**
	* レベルアップ認定データを取得します。
	*
	* @param $apply_id レベルアップ認定申請ID
	*/
	function get_levelup_recognize_emp_data($apply_id){

		$this->log->debug("get_levelup_apply_data() START");

		//SQL作成
		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	rec.levelup_recognize_id ";
		$sql .= " 	, rec.apply_id ";
		$sql .= " 	, rec.levelup_apply_id ";
		$sql .= " 	, rec.levelup_emp_id ";
		$sql .= " 	, emp.emp_lt_nm || '　' || emp.emp_ft_nm AS emp_name ";
		$sql .= " 	,  CASE WHEN emp.emp_room IS NULL THEN class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm ";
		$sql .= " 		ELSE class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm || ' > ' || room.room_nm ";
		$sql .= " 	END as affiliation ";
		$sql .= " 	, CASE WHEN emp.emp_join IS NOT NULL THEN date_trunc('month',age(current_timestamp, to_timestamp(emp.emp_join,'YYYYMMDD'))) ";
		$sql .= " 		ELSE NULL ";
		$sql .= " 	END as exp_years ";
		$sql .= " 	, CASE WHEN emp.emp_join IS NOT NULL THEN date_trunc('month',age(current_timestamp, to_timestamp(emp.emp_join,'YYYYMMDD'))) ";
		$sql .= " 		ELSE NULL ";
		$sql .= " 	END as exp_years_this_hos ";
		$sql .= " 	, prof.now_level ";
		$sql .= " 	, rec.level ";
		$sql .= " 	, rec.recognize_emp_id ";
		$sql .= " 	, rec.recognize_date ";
		$sql .= " 	, rec.recognize_flg ";
		$sql .= " 	, rec.reason ";
		$sql .= " 	, rec.total_value ";
		$sql .= " 	, rec.delete_flg ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_recognize rec ";
		$sql .= " 	, empmst emp ";
		$sql .= " 	LEFT JOIN cl_personal_profile prof ON emp.emp_id = prof.emp_id ";
		$sql .= " 	LEFT JOIN classmst AS class ON emp.emp_class = class.class_id ";
		$sql .= " 	LEFT JOIN atrbmst AS atrb ON emp.emp_attribute = atrb.atrb_id ";
		$sql .= " 	LEFT JOIN deptmst AS dept ON emp.emp_dept = dept.dept_id ";
		$sql .= " 	LEFT JOIN classroom AS room ON emp.emp_room = room.room_id ";
		$sql .= " WHERE ";
		$sql .= " 	rec.levelup_emp_id = emp.emp_id ";
		$sql .= " 	AND rec.apply_id = :apply_id ";



		$data=parent::select($sql,array("text"),array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_mst_evaluation_category_list() END data:".print_r($data,true));
		$this->log->debug("get_mst_evaluation_category_list() END count(data):".count($data));
		return $data;

	}

	/**
	* レベルアップ申請者データを取得します。
	* ※get_levelup_recognize_emp_data()から認定系の情報を除いた内容となります。
	*
	* @param $levelup_apply_id レベルアップ申請ID
	*/
	function get_levelup_emp_data($levelup_apply_id){

		$this->log->debug("get_levelup_emp_data() START");

		//SQL作成
		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	  lap.levelup_apply_id ";
		$sql .= " 	, lap.levelup_emp_id ";
		$sql .= " 	,  emp.emp_lt_nm || '　' || emp.emp_ft_nm AS emp_name ";
		$sql .= " 	, CASE WHEN emp.emp_room IS NULL THEN class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm ";
		$sql .= " 	       ELSE class.class_nm || ' > ' || atrb.atrb_nm || ' > ' || dept.dept_nm || ' > ' || room.room_nm ";
		$sql .= " 	       END as affiliation ";
		$sql .= " 	, CASE WHEN emp.emp_join IS NOT NULL THEN date_trunc('month',age(current_timestamp, to_timestamp(emp.emp_join,'YYYYMMDD'))) ";
		$sql .= " 	       ELSE NULL ";
		$sql .= " 	       END as exp_years ";
		$sql .= " 	, CASE WHEN emp.emp_join IS NOT NULL THEN date_trunc('month',age(current_timestamp, to_timestamp(emp.emp_join,'YYYYMMDD'))) ";
		$sql .= " 	       ELSE NULL ";
		$sql .= " 	       END as exp_years_this_hos ";
		$sql .= " 	, prof.now_level ";
		$sql .= " 	, lap.level ";
		$sql .= " FROM ";
		$sql .= " 	(select * from cl_apl_levelup_apply where levelup_apply_id = :levelup_apply_id) lap ";
		$sql .= " 	INNER JOIN empmst emp ON lap.levelup_emp_id = emp.emp_id";
		$sql .= " 	LEFT JOIN cl_personal_profile prof ON emp.emp_id = prof.emp_id ";
		$sql .= " 	LEFT JOIN classmst AS class ON emp.emp_class = class.class_id ";
		$sql .= " 	LEFT JOIN atrbmst AS atrb ON emp.emp_attribute = atrb.atrb_id ";
		$sql .= " 	LEFT JOIN deptmst AS dept ON emp.emp_dept = dept.dept_id ";
		$sql .= " 	LEFT JOIN classroom AS room ON emp.emp_room = room.room_id ";

		$data=parent::select($sql,array("text"),array("levelup_apply_id"=>$levelup_apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_levelup_emp_data() END data:".print_r($data,true));
		$this->log->debug("get_levelup_emp_data() END count(data):".count($data));
		return $data;

	}
}
