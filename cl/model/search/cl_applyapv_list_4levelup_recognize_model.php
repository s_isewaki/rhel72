<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_applyapv_list_4levelup_recognize_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 一覧取得用SQL */
    //var $select_list_sql = "SELECT * FROM cl_applyapv WHERE apply_id = :apply_id ORDER BY apv_order, apv_sub_order asc ";
    var $select_list_sql = "SELECT * FROM cl_applyapv apv LEFT JOIN empmst emp ON apv.emp_id = emp.emp_id WHERE apv.apply_id = :apply_id ORDER BY apv.apv_order, apv.apv_sub_order ASC";

    /**
     * Constractor
     */
    function cl_applyapv_list_4levelup_recognize_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * select
     * @param type $apply_id
     */
	function getListByApplyId($apply_id,$st_div){

		$this->log->info("getListByApplyId() START");
		//SQL作成
		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	apv.apply_id ";
		$sql .= " 	, apv.wkfw_id ";
		$sql .= " 	, apv.apv_stat ";
		$sql .= " 	, apv.apv_date ";
		$sql .= " 	, apv.st_div ";
		$sql .= " FROM ";
		$sql .= " 	cl_applyapv apv ";
		$sql .= " WHERE ";
		$sql .= " 	apv.apply_id = :apply_id ";
		$sql .= " 	AND apv.st_div = :st_div ";

		$data=parent::select($sql,array("text","integer"),array("apply_id"=>$apply_id,"st_div"=>$st_div),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("getListByApplyId() END data:".print_r($data,true));
		$this->log->debug("getListByApplyId() END count(data):".count($data));

		return $data;

	}


}
