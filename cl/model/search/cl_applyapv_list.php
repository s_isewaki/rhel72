<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_applyapv_list extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /* 一覧取得用SQL */
    //var $select_list_sql = "SELECT * FROM cl_applyapv WHERE apply_id = :apply_id ORDER BY apv_order, apv_sub_order asc ";
    //var $select_list_sql = "SELECT * FROM cl_applyapv apv LEFT JOIN empmst emp ON apv.emp_id = emp.emp_id WHERE apv.apply_id = :apply_id ORDER BY apv.apv_order, apv.apv_sub_order ASC";
    var $select_list_sql = "SELECT apv.*, emp.*, dept.dept_nm    FROM cl_applyapv apv    LEFT JOIN empmst emp ON apv.emp_id = emp.emp_id    LEFT JOIN deptmst dept ON emp.emp_dept = dept.dept_id     WHERE apv.apply_id = :apply_id ORDER BY apv.apv_order, apv.apv_sub_order ASC";

    /**
     * Constractor
     */
    function cl_applyapv_list($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * select
     * @param type $apply_id
     */
    function getListByApplyId($apply_id){

        $this->log->info("getListByApplyId() START");
        $data=parent::select($this->select_list_sql,array("text"),array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);
        $this->log->debug("getListByApplyId() END count(data):".count($data));
        return $data;

     }


}
