
<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_ldr_cwrkr_assmnt_acknowledger_manage_list_model extends cl_common_model{


    var $con;
    var $log;
    var $user;

    /**
     * Constractor コンストラクタ
     */
    function cl_ldr_cwrkr_assmnt_acknowledger_manage_list_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * getList 全件取得
     * @param type $plan_id
     */
    function getList($cwrkr_asmnt_auto_apply_manage_id){

        $this->log->info(__FUNCTION__." START");

        /* SQL */
        $sql = "SELECT * FROM cl_ldr_cwrkr_assmnt_acknowledger_manage WHERE cwrkr_asmnt_auto_apply_manage_id = :cwrkr_asmnt_auto_apply_manage_id ORDER BY cwrkr_asmnt_auto_apply_manage_id";

        /* カラムタイプ */
        $type = array('text');

        $data=parent::select($sql,$type,array("cwrkr_asmnt_auto_apply_manage_id"=>$cwrkr_asmnt_auto_apply_manage_id),MDB2_FETCHMODE_ASSOC);

        $this->log->info(__FUNCTION__." END");

        return $data;

     }

}
