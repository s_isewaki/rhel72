<?php
/**
 * 院外研修一覧画面の一覧データ取得用モデル
 *
 *
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_outside_seminar_select_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_outside_seminar_select_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }





	/**
	* 院外研修一覧画面の研修一覧部分のデータを取得します。
	*
	* @param $emp_id ログインユーザの職員ID。
	*/
	function get_outside_training_list($emp_id)
	{
		$this->log->debug("get_outside_training_list() START");

		//SQL作成
		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	out.seminar_apply_id ";
		$sql .= " 	, out.seminar_id ";
		$sql .= " 	, out.apply_id ";
		$sql .= " 	, out.emp_id ";
		$sql .= " 	, out.seminar_name ";
		$sql .= " 	, substr(apl.apply_date,1,4) || '/' || substr(apl.apply_date,5,2) || '/' || substr(apl.apply_date,7,2) as apply_date ";
		$sql .= " 	, to_char(out.report_date,'YYYY/MM/DD') as report_date ";
		$sql .= " 	, to_char(out.from_date,'YYYY/MM/DD') as from_date ";
		$sql .= " 	, to_char(out.to_date,'YYYY/MM/DD') as to_date ";
		$sql .= " 	, out.participation_division ";
		$sql .= " 	, CASE out.participation_division ";
		$sql .= " 		WHEN '2' THEN '（演者）' ";
		$sql .= " 		ELSE '' ";
		$sql .= " 	END AS participation_comment ";
		$sql .= " 	, out.expense_division ";
		$sql .= " 	,CASE out.expense_division ";
		$sql .= " 		WHEN 1 THEN '出張扱い' ";
		$sql .= " 		WHEN 2 THEN '自己研修' ";
		$sql .= " 		ELSE NULL ";
		$sql .= " 	END AS expense ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_outside_seminar out ";
		$sql .= " 	LEFT JOIN cl_apply apl on out.apply_id = apl.apply_id ";

		//NGリスト:合格のレベルアップ認定で使用された院外研修IDを付加
		$sql .= " 	LEFT JOIN";
		$sql .= " 	(";
		$sql .= " 	    SELECT c.seminar_apply_id";
		$sql .= " 	    FROM";
		$sql .= " 	    (";
		$sql .= " 	        SELECT apply_id,levelup_apply_id";
		$sql .= " 	        FROM cl_apl_levelup_recognize a";
		$sql .= " 	        WHERE levelup_emp_id = :emp_id";//ログインユーザーが認定者
		$sql .= " 	        AND recognize_flg = 1";//1:認定合格
		$sql .= " 	    ) a";
		$sql .= " 	    INNER JOIN cl_apply b";
		$sql .= " 	    ON a.apply_id = b.apply_id";
		$sql .= " 	    AND b.apply_stat = 1";//1:レベルアップ認定が承認確定
		$sql .= " 	    INNER JOIN cl_apl_levelup_apply_outside_seminar c";
		$sql .= " 	    ON a.levelup_apply_id = c.levelup_apply_id";
		$sql .= " 	) used_out";
		$sql .= " 	ON out.seminar_apply_id = used_out.seminar_apply_id";

		//NGリスト:開催終了日から２年経過した院外研修IDを付加
		$sql .= " 	LEFT JOIN";
		$sql .= " 	(";
		$sql .= " 	    SELECT seminar_apply_id";
		$sql .= " 	    FROM cl_apl_outside_seminar";
		$sql .= " 	    WHERE";
		$sql .= " 	    emp_id = :emp_id";
		$sql .= " 	    AND (to_date + interval '2 year') <= current_date";
		$sql .= " 	) old_out";
		$sql .= " 	ON out.seminar_apply_id = old_out.seminar_apply_id";

		$sql .= " WHERE ";
		$sql .= " 	out.delete_flg = 0 ";
		$sql .= " 	AND out.emp_id = :emp_id ";
		$sql .= " 	AND apl.apply_stat = 1 ";
		$sql .= " 	AND used_out.seminar_apply_id is null ";//合格認定で使用されていないもののみ使用可能
		$sql .= " 	AND old_out.seminar_apply_id is null ";//２年以内のもののみ使用可能
		$sql .= " ORDER BY ";
		$sql .= " 	out.seminar_apply_id ";

		$data=parent::select($sql,array("text"),array("emp_id"=>$emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_outside_training_list() END data:".print_r($data,true));
		$this->log->debug("get_outside_training_list() END count(data):".count($data));
		return $data;
	}

}
