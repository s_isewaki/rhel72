<?php
/**
 * レベルアップ申請ID取得用モデル
 *
 *
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_get_levelup_apply_id_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_get_levelup_apply_id_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }


	/**
	* レベルアップ申請IDを取得します。
	*
	*/
	function get_levelup_apply_id($apply_id)
	{
		$this->log->debug("get_levelup_apply_id() START");

		//SQL作成
		$sql = "";
		$sql .= " SELECT ";
		$sql .= "    intr.levelup_apply_id ";

		$sql .= "   ,intr.chief_emp_id ";
		$sql .= "   ,intr.colleague_emp_id ";

		$sql .= "   ,intr.attached_target_flg ";
		$sql .= "   ,intr.attached_episode_flg ";

		$sql .= "   ,chief.emp_lt_nm as chief_lt_nm ";
		$sql .= "   ,chief.emp_ft_nm as chief_ft_nm ";

		$sql .= "   ,colleague.emp_lt_nm as colleague_lt_nm ";
		$sql .= "   ,colleague.emp_ft_nm as colleague_ft_nm ";

		$sql .= " FROM ";
		$sql .= "    cl_apl_levelup_apply intr ";
		$sql .= "	LEFT JOIN  ";
		$sql .= "		empmst chief ";
		$sql .= "	ON ";
		$sql .= "		intr.chief_emp_id = chief.emp_id ";
		$sql .= "	LEFT JOIN  ";
		$sql .= "		empmst colleague ";
		$sql .= "	ON ";
		$sql .= "		intr.colleague_emp_id = colleague.emp_id ";
		$sql .= " WHERE ";
		$sql .= "     intr.apply_id = :apply_id ";

		$data=parent::select($sql,array("text"),array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_levelup_apply_id() END data:".print_r($data,true));
		$this->log->debug("get_levelup_apply_id() END count(data):".count($data));
		return $data;
	}





}
