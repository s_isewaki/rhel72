<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_ladder_authorize_employe_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     */
    function cl_ladder_authorize_employe_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * getList 全件取得
     * @param type $emp_id
     */
    function get_level_list($emp_id ){

        $this->log->info(__FUNCTION__." START");

        /* SQL */
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " emp.emp_id ";
		$sql .= " ,emp.emp_personal_id ";
		$sql .= " ,emp.emp_lt_nm ";
		$sql .= " ,emp.emp_ft_nm ";
		$sql .= " ,class.class_nm ";
		$sql .= " ,atrb.atrb_nm ";
		$sql .= " ,dept.dept_nm ";
		$sql .= " ,room.room_nm ";
		$sql .= " ,pro.now_level ";
		$sql .= " ,pro.get_level1_date ";
		$sql .= " ,pro.get_level2_date ";
		$sql .= " ,pro.get_level3_date ";
		$sql .= " ,pro.get_level4_date ";
		$sql .= " ,pro.get_level5_date ";
		$sql .= " ,pro.level1_total_value ";
		$sql .= " ,pro.level2_total_value ";
		$sql .= " ,pro.level3_total_value ";
		$sql .= " ,pro.level4_total_value ";
		$sql .= " ,pro.level5_total_value ";
		$sql .= " ,pro.level1_certificate_date ";
		$sql .= " ,pro.level2_certificate_date ";
		$sql .= " ,pro.level3_certificate_date ";
		$sql .= " ,pro.level4_certificate_date ";
		$sql .= " ,pro.level5_certificate_date ";
		$sql .= " ,pro.level1_levelup_recognize_id ";
		$sql .= " ,pro.level2_levelup_recognize_id ";
		$sql .= " ,pro.level3_levelup_recognize_id ";
		$sql .= " ,pro.level4_levelup_recognize_id ";
		$sql .= " ,pro.level5_levelup_recognize_id ";
		$sql .= "FROM ";
		$sql .= " empmst emp ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_personal_profile pro ";
		$sql .= " ON ";
		$sql .= "  emp.emp_id = pro.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  classmst class ";
		$sql .= " ON ";
		$sql .= "  emp.emp_class = class.class_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  atrbmst atrb ";
		$sql .= " ON ";
		$sql .= "  emp.emp_attribute = atrb.atrb_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  deptmst dept ";
		$sql .= " ON ";
		$sql .= "  emp.emp_dept = dept.dept_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  classroom room ";
		$sql .= " ON ";
		$sql .= "  emp.emp_room = room.room_id ";
		$sql .= "WHERE emp.emp_id = :emp_id ";

		$data=parent::select($sql,array("text"),array("emp_id"=>$emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_level_list() DB RETURN :".print_r($data,true));
		$this->log->debug("get_level_list() DB RETURN count(data):".count($data));


		$this->log->info(__FUNCTION__." END");

		return $data;

	}

	//2012/05/15 K.Fujii ins(s)
	/**
	 * getList 全件取得
	 * @param type $emp_id
	 */
	function get_emp_level_list($emp_id ){

		$this->log->info(__FUNCTION__." START");

		/* SQL */
		$sql  = "";
		$sql .= " SELECT DISTINCT ";
		$sql .= "  emp.emp_id ";
		$sql .= "  ,emp.emp_personal_id ";
		$sql .= "  ,emp.emp_lt_nm ";
		$sql .= "  ,emp.emp_ft_nm ";
		$sql .= "  ,class.class_nm ";
		$sql .= "  ,atrb.atrb_nm ";
		$sql .= "  ,dept.dept_nm ";
		$sql .= "  ,room.room_nm ";
		$sql .= "  ,pro.now_level ";
		$sql .= "  ,pro.get_level1_date AS get_level_date ";
		$sql .= "  ,pro.level1_total_value AS total_value ";
		$sql .= "  ,pro.level1_certificate_date AS certificate_date ";
		$sql .= "  ,pro.level1_levelup_recognize_id AS levelup_recognize_id ";
		$sql .= "  ,class_L1.class_nm AS class_nm_levelup ";
		$sql .= "  ,atrb_L1.atrb_nm AS atrb_nm_levelup ";
		$sql .= "  ,dept_L1.dept_nm AS dept_nm_levelup ";
		$sql .= "  ,room_L1.room_nm AS room_nm_levelup ";
		$sql .= "  ,cat_val_L1.disp_order AS disp_order ";
		$sql .= "  ,cat_val_L1.category_name AS category_name ";
		$sql .= "  ,cat_val_L1.cat_total_val AS cat_total_val ";
		$sql .= "  ,1 AS level ";
		$sql .= " FROM ";
		$sql .= "  empmst emp ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   cl_personal_profile pro ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_id = pro.emp_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   (SELECT ";
		$sql .= " 	rec.levelup_recognize_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, SUM(val.value) AS cat_total_val ";
		$sql .= "   FROM ";
		$sql .= " 	cl_apl_levelup_recognize rec ";
		$sql .= " 	, cl_apl_levelup_evaluation_value val ";
		$sql .= " 	LEFT JOIN cl_apl_levelup_apply_evaluation_category cat ON val.levelup_apply_id = cat.levelup_apply_id AND val.category_id = cat.category_id ";
		$sql .= "   WHERE ";
		$sql .= " 	rec.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_emp_division = 2 ";
		$sql .= "   GROUP BY ";
		$sql .= " 	rec.levelup_recognize_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order ";
		$sql .= "   ORDER BY ";
		$sql .= " 	cat.disp_order ";
		$sql .= " 	, val.category_id) cat_val_L1 ON  pro.level1_levelup_recognize_id = cat_val_L1.levelup_recognize_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classmst class ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_class = class.class_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   atrbmst atrb ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_attribute = atrb.atrb_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   deptmst dept ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_dept = dept.dept_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classroom room ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_room = room.room_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classmst class_L1 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level1_class = class_L1.class_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   atrbmst atrb_L1 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level1_atrb = atrb_L1.atrb_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   deptmst dept_L1 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level1_dept = dept_L1.dept_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classroom room_L1 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level1_room = room_L1.room_id ";
		$sql .= " WHERE emp.emp_id = :emp_id ";
		$sql .= " UNION ";
		$sql .= " SELECT DISTINCT ";
		$sql .= "  emp.emp_id ";
		$sql .= "  ,emp.emp_personal_id ";
		$sql .= "  ,emp.emp_lt_nm ";
		$sql .= "  ,emp.emp_ft_nm ";
		$sql .= "  ,class.class_nm ";
		$sql .= "  ,atrb.atrb_nm ";
		$sql .= "  ,dept.dept_nm ";
		$sql .= "  ,room.room_nm ";
		$sql .= "  ,pro.now_level ";
		$sql .= "  ,pro.get_level2_date AS get_level_date ";
		$sql .= "  ,pro.level2_total_value AS total_value ";
		$sql .= "  ,pro.level2_certificate_date AS certificate_date ";
		$sql .= "  ,pro.level2_levelup_recognize_id AS levelup_recognize_id ";
		$sql .= "  ,class_L2.class_nm AS class_nm_levelup ";
		$sql .= "  ,atrb_L2.atrb_nm AS atrb_nm_levelup ";
		$sql .= "  ,dept_L2.dept_nm AS dept_nm_levelup ";
		$sql .= "  ,room_L2.room_nm AS room_nm_levelup ";
		$sql .= "  ,cat_val_L2.disp_order ";
		$sql .= "  ,cat_val_L2.category_name ";
		$sql .= "  ,cat_val_L2.cat_total_val ";
		$sql .= "  ,2 AS level ";
		$sql .= " FROM ";
		$sql .= "  empmst emp ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   cl_personal_profile pro ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_id = pro.emp_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   (SELECT ";
		$sql .= " 	rec.levelup_recognize_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, SUM(val.value) AS cat_total_val ";
		$sql .= "   FROM ";
		$sql .= " 	cl_apl_levelup_recognize rec ";
		$sql .= " 	, cl_apl_levelup_evaluation_value val ";
		$sql .= " 	LEFT JOIN cl_apl_levelup_apply_evaluation_category cat ON val.levelup_apply_id = cat.levelup_apply_id AND val.category_id = cat.category_id ";
		$sql .= "   WHERE ";
		$sql .= " 	rec.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_emp_division = 2 ";
		$sql .= "   GROUP BY ";
		$sql .= " 	rec.levelup_recognize_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order ";
		$sql .= "   ORDER BY ";
		$sql .= " 	cat.disp_order ";
		$sql .= " 	, val.category_id) cat_val_L2 ON  pro.level2_levelup_recognize_id = cat_val_L2.levelup_recognize_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classmst class ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_class = class.class_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   atrbmst atrb ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_attribute = atrb.atrb_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   deptmst dept ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_dept = dept.dept_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classroom room ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_room = room.room_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classmst class_L2 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level2_class = class_L2.class_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   atrbmst atrb_L2 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level2_atrb = atrb_L2.atrb_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   deptmst dept_L2 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level2_dept = dept_L2.dept_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classroom room_L2 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level2_room = room_L2.room_id ";
		$sql .= " WHERE emp.emp_id = :emp_id ";
		$sql .= " UNION ";
		$sql .= " SELECT DISTINCT ";
		$sql .= "  emp.emp_id ";
		$sql .= "  ,emp.emp_personal_id ";
		$sql .= "  ,emp.emp_lt_nm ";
		$sql .= "  ,emp.emp_ft_nm ";
		$sql .= "  ,class.class_nm ";
		$sql .= "  ,atrb.atrb_nm ";
		$sql .= "  ,dept.dept_nm ";
		$sql .= "  ,room.room_nm ";
		$sql .= "  ,pro.now_level ";
		$sql .= "  ,pro.get_level3_date AS get_level_date ";
		$sql .= "  ,pro.level3_total_value AS total_value ";
		$sql .= "  ,pro.level3_certificate_date AS certificate_date ";
		$sql .= "  ,pro.level3_levelup_recognize_id AS levelup_recognize_id ";
		$sql .= "  ,class_L3.class_nm AS class_nm_levelup ";
		$sql .= "  ,atrb_L3.atrb_nm AS atrb_nm_levelup ";
		$sql .= "  ,dept_L3.dept_nm AS dept_nm_levelup ";
		$sql .= "  ,room_L3.room_nm AS room_nm_levelup ";
		$sql .= "  ,cat_val_L3.disp_order ";
		$sql .= "  ,cat_val_L3.category_name ";
		$sql .= "  ,cat_val_L3.cat_total_val ";
		$sql .= "  ,3 AS level ";
		$sql .= " FROM ";
		$sql .= "  empmst emp ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   cl_personal_profile pro ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_id = pro.emp_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   (SELECT ";
		$sql .= " 	rec.levelup_recognize_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, SUM(val.value) AS cat_total_val ";
		$sql .= "   FROM ";
		$sql .= " 	cl_apl_levelup_recognize rec ";
		$sql .= " 	, cl_apl_levelup_evaluation_value val ";
		$sql .= " 	LEFT JOIN cl_apl_levelup_apply_evaluation_category cat ON val.levelup_apply_id = cat.levelup_apply_id AND val.category_id = cat.category_id ";
		$sql .= "   WHERE ";
		$sql .= " 	rec.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_emp_division = 2 ";
		$sql .= "   GROUP BY ";
		$sql .= " 	rec.levelup_recognize_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order ";
		$sql .= "   ORDER BY ";
		$sql .= " 	cat.disp_order ";
		$sql .= " 	, val.category_id) cat_val_L3 ON  pro.level3_levelup_recognize_id = cat_val_L3.levelup_recognize_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classmst class ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_class = class.class_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   atrbmst atrb ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_attribute = atrb.atrb_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   deptmst dept ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_dept = dept.dept_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classroom room ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_room = room.room_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classmst class_L3 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level3_class = class_L3.class_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   atrbmst atrb_L3 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level3_atrb = atrb_L3.atrb_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   deptmst dept_L3 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level3_dept = dept_L3.dept_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classroom room_L3 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level3_room = room_L3.room_id ";
		$sql .= " WHERE emp.emp_id = :emp_id ";
		$sql .= " UNION ";
		$sql .= " SELECT DISTINCT ";
		$sql .= "  emp.emp_id ";
		$sql .= "  ,emp.emp_personal_id ";
		$sql .= "  ,emp.emp_lt_nm ";
		$sql .= "  ,emp.emp_ft_nm ";
		$sql .= "  ,class.class_nm ";
		$sql .= "  ,atrb.atrb_nm ";
		$sql .= "  ,dept.dept_nm ";
		$sql .= "  ,room.room_nm ";
		$sql .= "  ,pro.now_level ";
		$sql .= "  ,pro.get_level4_date AS get_level_date ";
		$sql .= "  ,pro.level4_total_value AS total_value ";
		$sql .= "  ,pro.level4_certificate_date AS certificate_date ";
		$sql .= "  ,pro.level4_levelup_recognize_id AS levelup_recognize_id ";
		$sql .= "  ,class_L4.class_nm AS class_nm_levelup ";
		$sql .= "  ,atrb_L4.atrb_nm AS atrb_nm_levelup ";
		$sql .= "  ,dept_L4.dept_nm AS dept_nm_levelup ";
		$sql .= "  ,room_L4.room_nm AS room_nm_levelup ";
		$sql .= "  ,cat_val_L4.disp_order ";
		$sql .= "  ,cat_val_L4.category_name ";
		$sql .= "  ,cat_val_L4.cat_total_val ";
		$sql .= "  ,4 AS level ";
		$sql .= " FROM ";
		$sql .= "  empmst emp ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   cl_personal_profile pro ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_id = pro.emp_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   (SELECT ";
		$sql .= " 	rec.levelup_recognize_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, SUM(val.value) AS cat_total_val ";
		$sql .= "   FROM ";
		$sql .= " 	cl_apl_levelup_recognize rec ";
		$sql .= " 	, cl_apl_levelup_evaluation_value val ";
		$sql .= " 	LEFT JOIN cl_apl_levelup_apply_evaluation_category cat ON val.levelup_apply_id = cat.levelup_apply_id AND val.category_id = cat.category_id ";
		$sql .= "   WHERE ";
		$sql .= " 	rec.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_emp_division = 2 ";
		$sql .= "   GROUP BY ";
		$sql .= " 	rec.levelup_recognize_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order ";
		$sql .= "   ORDER BY ";
		$sql .= " 	cat.disp_order ";
		$sql .= " 	, val.category_id) cat_val_L4 ON  pro.level4_levelup_recognize_id = cat_val_L4.levelup_recognize_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classmst class ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_class = class.class_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   atrbmst atrb ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_attribute = atrb.atrb_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   deptmst dept ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_dept = dept.dept_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classroom room ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_room = room.room_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classmst class_L4 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level4_class = class_L4.class_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   atrbmst atrb_L4 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level4_atrb = atrb_L4.atrb_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   deptmst dept_L4 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level4_dept = dept_L4.dept_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classroom room_L4 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level4_room = room_L4.room_id ";
		$sql .= " WHERE emp.emp_id = :emp_id ";
		$sql .= " UNION ";
		$sql .= " SELECT DISTINCT ";
		$sql .= "  emp.emp_id ";
		$sql .= "  ,emp.emp_personal_id ";
		$sql .= "  ,emp.emp_lt_nm ";
		$sql .= "  ,emp.emp_ft_nm ";
		$sql .= "  ,class.class_nm ";
		$sql .= "  ,atrb.atrb_nm ";
		$sql .= "  ,dept.dept_nm ";
		$sql .= "  ,room.room_nm ";
		$sql .= "  ,pro.now_level ";
		$sql .= "  ,pro.get_level5_date AS get_level_date ";
		$sql .= "  ,pro.level5_total_value AS total_value ";
		$sql .= "  ,pro.level5_certificate_date AS certificate_date ";
		$sql .= "  ,pro.level5_levelup_recognize_id AS levelup_recognize_id ";
		$sql .= "  ,class_L5.class_nm AS class_nm_levelup ";
		$sql .= "  ,atrb_L5.atrb_nm AS atrb_nm_levelup ";
		$sql .= "  ,dept_L5.dept_nm AS dept_nm_levelup ";
		$sql .= "  ,room_L5.room_nm AS room_nm_levelup ";
		$sql .= "  ,cat_val_L5.disp_order ";
		$sql .= "  ,cat_val_L5.category_name ";
		$sql .= "  ,cat_val_L5.cat_total_val ";
		$sql .= "  ,5 AS level ";
		$sql .= " FROM ";
		$sql .= "  empmst emp ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   cl_personal_profile pro ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_id = pro.emp_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   (SELECT ";
		$sql .= " 	rec.levelup_recognize_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, SUM(val.value) AS cat_total_val ";
		$sql .= "   FROM ";
		$sql .= " 	cl_apl_levelup_recognize rec ";
		$sql .= " 	, cl_apl_levelup_evaluation_value val ";
		$sql .= " 	LEFT JOIN cl_apl_levelup_apply_evaluation_category cat ON val.levelup_apply_id = cat.levelup_apply_id AND val.category_id = cat.category_id ";
		$sql .= "   WHERE ";
		$sql .= " 	rec.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_emp_division = 2 ";
		$sql .= "   GROUP BY ";
		$sql .= " 	rec.levelup_recognize_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order ";
		$sql .= "   ORDER BY ";
		$sql .= " 	cat.disp_order ";
		$sql .= " 	, val.category_id) cat_val_L5 ON  pro.level5_levelup_recognize_id = cat_val_L5.levelup_recognize_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classmst class ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_class = class.class_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   atrbmst atrb ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_attribute = atrb.atrb_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   deptmst dept ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_dept = dept.dept_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classroom room ";
		$sql .= "  ON ";
		$sql .= "   emp.emp_room = room.room_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classmst class_L5 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level5_class = class_L5.class_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   atrbmst atrb_L5 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level5_atrb = atrb_L5.atrb_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   deptmst dept_L5 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level5_dept = dept_L5.dept_id ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classroom room_L5 ";
		$sql .= "  ON ";
		$sql .= "   pro.get_level5_room = room_L5.room_id ";
		$sql .= " WHERE emp.emp_id = :emp_id ";
		$sql .= " ORDER BY ";
		$sql .= "   level ";
		$sql .= "   , disp_order ";

		$data=parent::select($sql,array("text"),array("emp_id"=>$emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_emp_level_list() DB RETURN :".print_r($data,true));
		$this->log->debug("get_emp_level_list() DB RETURN count(data):".count($data));


		$this->log->info(__FUNCTION__." END");

		return $data;

	}
	//2012/05/15 K.Fujii ins(e)

    /**
     * numberingPlanNo
     * @param type $training_id
     */
    function numberingPlanNo($training_id){

    	$this->log->info("numberingPlanNo() START");

    	$cnt = $this->getCount($training_id);
        $this->log->debug("cnt:".$cnt);

        $rtn=0;
        if($cnt>0){
        	$max_plan_no=$this->getMaxPlanNo($training_id);
        	$rtn=$max_plan_no+1;

        }
        else{
        	$rtn=1;
        }
	    $this->log->info("numberingPlanNo() END");
	    return $rtn;

     }

    /**
     * getMaxPlanNo
     * @param type $training_id
     */
    function getMaxPlanNo($training_id){

    	$this->log->info("getMaxPlanNo() START");

	    /* レコード取得用SQL */
	    $sql_buff[] = "SELECT max(plan_no) as max_plan_no ";
	    $sql_buff[] =   "FROM cl_mst_inside_training_schedule ";
	    $sql_buff[] =  "WHERE training_id = :training_id";
        $sql = implode('', $sql_buff);

	    $data=parent::getRecord($sql,array("text"),array("training_id"=>$training_id),MDB2_FETCHMODE_ASSOC,0);

	    $this->log->info("getMaxPlanNo() END");
	    return $data["max_plan_no"];

     }

     /**
     * getCount
     * @param type $plan_id
     */
    function getCount($training_id){

	     /* レコード取得用SQL */
	    $sql_buff[] = "SELECT count(*) as count ";
	    $sql_buff[] =   "FROM cl_mst_inside_training_schedule ";
	    $sql_buff[] =  "WHERE training_id = :training_id";
        $sql = implode('', $sql_buff);

    	$this->log->info("getCount() START");
        $data=parent::getRecord($sql,array("text"),array("training_id"=>$training_id),MDB2_FETCHMODE_ASSOC,0);
        $this->log->info("getCount() END");
        return $data["count"];

     }

    /**
     * getListForReInsert
     * @param type $training_id
     */
    function getListForReInsert($training_id){

		/* 再登録用データ取得 */
		$sql_buff[] = "SELECT ";
		$sql_buff[] = " sch.plan_id ";
		$sql_buff[] = " ,sch.first_plan_id ";
		$sql_buff[] = " ,sch.training_id ";
		$sql_buff[] = " ,sch.plan_no ";
		$sql_buff[] = " ,sch.year ";
		$sql_buff[] = " ,sch.term_div ";
		$sql_buff[] = " ,sch.training_time ";
		$sql_buff[] = " ,sch.plan_date ";
		$sql_buff[] = " ,sch.from_time ";
		$sql_buff[] = " ,sch.to_time ";
		$sql_buff[] = " ,sch.report_division ";
		$sql_buff[] = " ,sch.answer_division ";
		$sql_buff[] = " ,sch.max_people ";
		$sql_buff[] = " ,sch.apply_status ";
		$sql_buff[] = " ,sch.place ";
		$sql_buff[] = " ,sch.remarks ";
		$sql_buff[] = " ,sch.delete_flg ";
		$sql_buff[] = "FROM ";
		$sql_buff[] = " cl_mst_inside_training_schedule sch ";
		$sql_buff[] = "WHERE ";
		$sql_buff[] = " sch.training_id = :training_id ";
		$sql_buff[] = " AND sch.delete_flg = 0 ";
		$sql = implode('', $sql_buff);

		$this->log->info("getListForReInsert() START");
		$data=parent::select($sql,array("text"),array("training_id"=>$training_id),MDB2_FETCHMODE_ASSOC);
		$this->log->info("getListForReInsert() END");
		return $data;

	}
}
