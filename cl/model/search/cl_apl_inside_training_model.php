<?php
/**
 * Level1院内必修研修一覧データ取得用モデル
 *
 *
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_apl_inside_training_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_apl_inside_training_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }


	/**
	* 院内研修一覧のデータを取得します。(初期)
	*
	*/
	function get_inside_must_training_list($level)
	{
		$this->log->debug("get_inside_must_training_list() START");

		//SQL作成
		$sql = "";
		// 2012/11/22 Yamagawa upd(s)
		//$sql .= "SELECT ";
		$sql .= "SELECT DISTINCT ";
		// 2012/11/22 Yamagawa upd(e)
		$sql .= " NULL AS levelup_apply_inside_training_id ";
		$sql .= " ,NULL AS levelup_apply_id ";
		$sql .= " ,intr.training_id ";
		$sql .= " ,intr.training_name ";

		switch($level){
			case 1:
				$sql .= " ,intr.target_level1 AS must_division ";
				break;
			case 2:
				$sql .= " ,intr.target_level2 AS must_division ";
				break;
			case 3:
				$sql .= " ,intr.target_level3 AS must_division ";
				break;
			case 4:
				$sql .= " ,intr.target_level4 AS must_division ";
				break;
			case 5:
				$sql .= " ,intr.target_level5 AS must_division ";
				break;
		}

		$sql .= " ,CASE ";
		$sql .= "   WHEN ";
		$sql .= "    rep.training_report_id IS NOT NULL ";
		$sql .= "   THEN ";
		$sql .= "    1 ";
		$sql .= "   ELSE ";
		$sql .= "    0 ";
		$sql .= "  END AS pass_flg ";
		$sql .= "FROM ";
		$sql .= " cl_mst_inside_training intr ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_apl_inside_training_report rep ";
		$sql .= " ON ";
		$sql .= "  rep.training_id = intr.training_id ";
		$sql .= "  AND EXISTS( ";
		$sql .= "   SELECT ";
		$sql .= "    app.apply_id ";
		$sql .= "   FROM ";
		$sql .= "    cl_apply app ";
		$sql .= "   WHERE ";
		$sql .= "    app.apply_id = rep.apply_id ";
		$sql .= "    AND app.emp_id = :emp_id ";
		$sql .= "    AND app.apply_stat = 1 ";
		$sql .= "  ) ";
		$sql .= "  AND EXISTS( ";
		$sql .= "   SELECT ";
		$sql .= "    plan.plan_id ";
		$sql .= "   FROM ";
		$sql .= "    cl_mst_inside_training_schedule plan ";
		$sql .= "   WHERE ";
		$sql .= "    plan.training_id = rep.training_id ";
		$sql .= "    AND plan.plan_id = rep.plan_id ";
		$sql .= "    and plan.final_flg = 1 ";
		$sql .= "  ) ";
		$sql .= "WHERE ";

		switch($level){
			case 1:
				$sql .= " intr.target_level1 IN (1,2) ";
				break;
			case 2:
				$sql .= " intr.target_level2 IN (1,2) ";
				break;
			case 3:
				$sql .= " intr.target_level3 IN (1,2) ";
				break;
			case 4:
				$sql .= " intr.target_level4 IN (1,2) ";
				break;
			case 5:
				$sql .= " intr.target_level5 IN (1,2) ";
				break;
		}

		// 2012/09/03 Yamagawa add(s)
		$sql .= " AND COALESCE(intr.abolition_flag,0) = 0 ";
		$sql .= " AND COALESCE(intr.delete_flg,0) = 0 ";
		// 2012/09/03 Yamagawa add(e)

		$sql .= "ORDER BY ";
		$sql .= " intr.training_id ";

		$data=parent::select($sql,array('text'),array("emp_id" => $this->user),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_inside_must_training_list() END data:".print_r($data,true));
		$this->log->debug("get_inside_must_training_list() END count(data):".count($data));
		return $data;
	}

}
