<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_mst_ladder_wkfw_link_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     */
    function cl_mst_ladder_wkfw_link_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * getList　関連付けマスタを取得
     *
     * @param $apply_short_wkfw_name レベルアップ申請　管理CD
     * @return  前回までの研修報告内容配列
     */
    function getList($apply_short_wkfw_name){

        $this->log->info(__FUNCTION__." START");
        $this->log->debug("apply_short_wkfw_name：".$apply_short_wkfw_name);

		$sql = "";
		$sql .= "SELECT ";
		$sql .= " ladder_wkfw_link_id ";
		$sql .= " ,level ";
		$sql .= " ,apply_short_wkfw_name ";
		$sql .= " ,evaluation_short_wkfw_name ";
		$sql .= " ,evaluation_colleague_short_wkfw_name ";
		$sql .= " ,recognize_short_wkfw_name ";
		$sql .= " ,evaluation_wkfw_title ";
		$sql .= " ,delete_flg ";
		$sql .= " ,create_date ";
		$sql .= " ,create_user ";
		$sql .= " ,update_date ";
		$sql .= " ,update_user ";
		$sql .= "FROM ";
		$sql .= " cl_mst_ladder_wkfw_link ";
		$sql .= "WHERE ";
		$sql .= " apply_short_wkfw_name = :apply_short_wkfw_name ";

        /* カラムタイプ */
        $type = array('text');

        $param['training_apply_id'] = $training_apply_id;
        $param['plan_id'] = $plan_id;

        $data = parent::select($sql,array('text'),array("apply_short_wkfw_name" => $apply_short_wkfw_name),MDB2_FETCHMODE_ASSOC);

        $this->log->debug("SQL：".$sql);
        $this->log->debug("結果：".print_r($data,true));
        $this->log->info(__FUNCTION__." END");

        return $data;

     }


}
