<?php
/**
 * 研修受講進捗(管理者)の一覧データ取得用モデル
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_training_progress_manager_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_training_progress_manager_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	 * 所属長(部署)取得
	 */
	function get_supervisor_class(){

		$this->log->info("get_supervisor_class() START count(data):".count($data));

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " class_division ";
		$sql .= "FROM ";
		$sql .= " cl_mst_supervisor_class ";

		$data=parent::select($sql,null,null,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_supervisor_class() END count(data):".count($data));
		return $data[0];
	}

	/**
	 * 看護部長(部署)取得
	 */
	function get_nurse_manager_class(){

		$this->log->info("get_nurse_manager_class() START count(data):".count($data));

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " class_division ";
		$sql .= "FROM ";
		$sql .= " cl_mst_nurse_manager_class ";

		$data=parent::select($sql,null,null,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_nurse_manager_class() END count(data):".count($data));
		return $data[0];
	}

	/**
	 * 一覧取得
	 */
	function get_user_list($param)
	{

		$this->log->info("get_user_list() START");

		$array_type = array();
		$array_param = array();
		//SQL作成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " emp.emp_id ";
		$sql .= " ,emp.emp_personal_id ";
		$sql .= " ,emp.emp_lt_nm ";
		$sql .= " ,emp.emp_ft_nm ";
		$sql .= " ,class.class_nm ";
		$sql .= " ,atrb.atrb_nm ";
		$sql .= " ,dept.dept_nm ";
		$sql .= " ,room.room_nm ";
		$sql .= " ,pro.now_level ";
		$sql .= "FROM ";

		$sql .= " empmst emp ";
//		$sql .= " (SELECT * FROM empmst WHERE EXISTS(SELECT authmst.emp_id FROM authmst WHERE NOT emp_del_flg AND authmst.emp_id = empmst.emp_id)) emp ";

		$sql .= " LEFT JOIN ";
		$sql .= "  cl_personal_profile pro ";
		$sql .= " ON ";
		$sql .= "  emp.emp_id = pro.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  classmst class ";
		$sql .= " ON ";
		$sql .= "  emp.emp_class = class.class_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  atrbmst atrb ";
		$sql .= " ON ";
		$sql .= "  emp.emp_attribute = atrb.atrb_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  deptmst dept ";
		$sql .= " ON ";
		$sql .= "  emp.emp_dept = dept.dept_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  classroom room ";
		$sql .= " ON ";
		$sql .= "  emp.emp_room = room.room_id ";
		// 2012/08/27 Yamagawa add(s)
		$sql .= " INNER JOIN ";
		$sql .= "  jobmst job ";
		$sql .= " ON ";
		$sql .= "  job.job_id = emp.emp_job ";
		$sql .= "  AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/27 Yamagawa add(e)
		$sql .= "WHERE ";
		$sql .= " 1 = 1 ";

		if ($param['login_emp_id'] != '') {
			$login_emp_id = $param['login_emp_id'];

			$sql .= " AND EXISTS( ";
			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";

			switch($param['class_division']){
				case 1:
					$sql .= "   AND emp.emp_class = loginemp.emp_class ";
					break;
				case 2:
					$sql .= "   AND emp.emp_attribute = loginemp.emp_attribute ";
					break;
				case 3:
					$sql .= "   AND emp.emp_dept = loginemp.emp_dept ";
					break;
				case 4:
					$sql .= "   AND emp.emp_room = loginemp.emp_room ";
					break;
			}

			$sql .= "  UNION ALL ";
			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent con ";
			$sql .= "   ON ";

			switch($param['class_division']){
				case 1:
					$sql .= "    loginemp.emp_class = con.emp_class ";
					break;
				case 2:
					$sql .= "    loginemp.emp_attribute = con.emp_attribute ";
					break;
				case 3:
					$sql .= "    loginemp.emp_dept = con.emp_dept ";
					break;
				case 4:
					$sql .= "    loginemp.emp_room = con.emp_room ";
					break;
			}

			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";
			$sql .= "   AND emp.emp_id = con.emp_id ";

			$sql .= "  UNION ALL ";
			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent logincon ";
			$sql .= "   ON ";
			$sql .= "    loginemp.emp_id = logincon.emp_id ";
			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";

			switch($param['class_division']){
				case 1:
					$sql .= "   AND emp.emp_class = logincon.emp_class ";
					break;
				case 2:
					$sql .= "   AND emp.emp_attribute = logincon.emp_attribute ";
					break;
				case 3:
					$sql .= "   AND emp.emp_dept = logincon.emp_dept ";
					break;
				case 4:
					$sql .= "   AND emp.emp_room = logincon.emp_room ";
					break;
			}

			$sql .= "  UNION ALL ";
			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent logincon ";
			$sql .= "   ON ";
			$sql .= "    loginemp.emp_id = logincon.emp_id ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent con ";
			$sql .= "   ON ";

			switch($param['class_division']){
				case 1:
					$sql .= "    logincon.emp_class = con.emp_class ";
					break;
				case 2:
					$sql .= "    logincon.emp_attribute = con.emp_attribute ";
					break;
				case 3:
					$sql .= "    logincon.emp_dept = con.emp_dept ";
					break;
				case 4:
					$sql .= "    logincon.emp_room = con.emp_room ";
					break;
			}

			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";
			$sql .= "   AND emp.emp_id = con.emp_id ";
			$sql .= " ) ";

		}

		if ($param['now_level'] != ''){
			$now_level = $param['now_level'];

			$sql .= " AND pro.now_level = $now_level ";
		}

		if ($param['emp_nm'] != '') {
			$emp_nm = $param['emp_nm'];

			$sql .= " AND ( ";
			$sql .= "  emp.emp_lt_nm || emp.emp_ft_nm LIKE '%' || '$emp_nm' || '%' ";
			$sql .= "  OR emp.emp_kn_lt_nm || emp.emp_kn_ft_nm LIKE '%' || '$emp_nm' || '%' ";
			$sql .= " ) ";
		}

		if ($param['class_id'] != '') {
			$class_id = $param['class_id'];
			$sql .= " AND emp.emp_class = $class_id ";
		}

		if ($param['atrb_id'] != '') {
			$atrb_id = $param['atrb_id'];
			$sql .= " AND emp.emp_attribute = $atrb_id ";
		}

		if ($param['dept_id'] != '') {
			$dept_id = $param['dept_id'];
			$sql .= " AND emp.emp_dept = $dept_id ";
		}

		if ($param['room_id'] != '') {
			$room_id = $param['room_id'];
			$sql .= " AND emp.emp_room = $room_id ";
		}

		// 2012/08/27 Yamagawa add(s)
		if ($param['retire_disp_flg'] == ''){
			$sql .= " AND ( ";
			$sql .= "  emp.emp_retire is null ";
			$sql .= "  OR trim(emp.emp_retire) = '' ";
			$sql .= "  OR to_date(trim(emp.emp_retire),'yyyymmdd') >= current_date ";
			$sql .= " ) ";
		}
		// 2012/08/27 Yamagawa add(e)

		$sql .= "ORDER BY ";
		$sql .= " emp.emp_personal_id ";

		$data=parent::select($sql,$array_type,$array_param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_user_list() END count(data):".count($data));
		return $data;
	}

	/**
	 * 一覧取得
	 */
	function get_user_csv_list($param)
	{

		$this->log->info("get_user_list() START");

		$array_type = array();
		$array_param = array();
		//SQL作成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " emp.emp_id ";
		$sql .= " ,emp.emp_personal_id ";
		$sql .= " ,emp.emp_lt_nm ";
		$sql .= " ,emp.emp_ft_nm ";
		$sql .= " ,class.class_nm ";
		$sql .= " ,atrb.atrb_nm ";
		$sql .= " ,dept.dept_nm ";
		$sql .= " ,room.room_nm ";
		$sql .= " ,pro.now_level ";
		$sql .= " ,CASE ";
		$sql .= "   WHEN trim(emp.emp_join) = '' ";
		$sql .= "   THEN NULL ";
		$sql .= "   ELSE extract(year from age(to_date(emp.emp_join,'YYYYMMDD'))) ";
		$sql .= "  END AS year_cnt ";
		$sql .= " ,mst_tra.training_name ";
		$sql .= " ,apl.learn_memo ";
		$sql .= "FROM ";

		$sql .= " empmst emp ";
//		$sql .= " (SELECT * FROM empmst WHERE EXISTS(SELECT authmst.emp_id FROM authmst WHERE NOT emp_del_flg AND authmst.emp_id = empmst.emp_id)) emp ";

		$sql .= " LEFT JOIN ";
		$sql .= "  cl_personal_profile pro ";
		$sql .= " ON ";
		$sql .= "  emp.emp_id = pro.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  classmst class ";
		$sql .= " ON ";
		$sql .= "  emp.emp_class = class.class_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  atrbmst atrb ";
		$sql .= " ON ";
		$sql .= "  emp.emp_attribute = atrb.atrb_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  deptmst dept ";
		$sql .= " ON ";
		$sql .= "  emp.emp_dept = dept.dept_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  classroom room ";
		$sql .= " ON ";
		$sql .= "  emp.emp_room = room.room_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_apl_inside_training_apply apl ";
		$sql .= " ON ";
		$sql .= "  emp.emp_id = apl.emp_id ";
		$sql .= "  AND 0 = apl.delete_flg ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_mst_inside_training mst_tra ";
		$sql .= " ON ";
		$sql .= " apl.inside_training_id = mst_tra.training_id ";
		// 2012/08/27 Yamagawa add(s)
		$sql .= " INNER JOIN ";
		$sql .= "  jobmst job ";
		$sql .= " ON ";
		$sql .= "  job.job_id = emp.emp_job ";
		$sql .= "  AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/27 Yamagawa add(e)
		$sql .= "WHERE ";
		$sql .= " 1 = 1 ";

		if ($param['login_emp_id'] != '') {
			$login_emp_id = $param['login_emp_id'];

			$sql .= " AND EXISTS( ";
			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";

			switch($param['class_division']){
				case 1:
					$sql .= "   AND emp.emp_class = loginemp.emp_class ";
					break;
				case 2:
					$sql .= "   AND emp.emp_attribute = loginemp.emp_attribute ";
					break;
				case 3:
					$sql .= "   AND emp.emp_dept = loginemp.emp_dept ";
					break;
				case 4:
					$sql .= "   AND emp.emp_room = loginemp.emp_room ";
					break;
			}

			$sql .= "  UNION ALL ";
			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent con ";
			$sql .= "   ON ";

			switch($param['class_division']){
				case 1:
					$sql .= "    loginemp.emp_class = con.emp_class ";
					break;
				case 2:
					$sql .= "    loginemp.emp_attribute = con.emp_attribute ";
					break;
				case 3:
					$sql .= "    loginemp.emp_dept = con.emp_dept ";
					break;
				case 4:
					$sql .= "    loginemp.emp_room = con.emp_room ";
					break;
			}

			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";
			$sql .= "   AND emp.emp_id = con.emp_id ";

			$sql .= "  UNION ALL ";
			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent logincon ";
			$sql .= "   ON ";
			$sql .= "    loginemp.emp_id = logincon.emp_id ";
			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";

			switch($param['class_division']){
				case 1:
					$sql .= "   AND emp.emp_class = logincon.emp_class ";
					break;
				case 2:
					$sql .= "   AND emp.emp_attribute = logincon.emp_attribute ";
					break;
				case 3:
					$sql .= "   AND emp.emp_dept = logincon.emp_dept ";
					break;
				case 4:
					$sql .= "   AND emp.emp_room = logincon.emp_room ";
					break;
			}

			$sql .= "  UNION ALL ";
			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent logincon ";
			$sql .= "   ON ";
			$sql .= "    loginemp.emp_id = logincon.emp_id ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent con ";
			$sql .= "   ON ";

			switch($param['class_division']){
				case 1:
					$sql .= "    logincon.emp_class = con.emp_class ";
					break;
				case 2:
					$sql .= "    logincon.emp_attribute = con.emp_attribute ";
					break;
				case 3:
					$sql .= "    logincon.emp_dept = con.emp_dept ";
					break;
				case 4:
					$sql .= "    logincon.emp_room = con.emp_room ";
					break;
			}

			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";
			$sql .= "   AND emp.emp_id = con.emp_id ";
			$sql .= " ) ";

		}

		if ($param['now_level'] != ''){
			$now_level = $param['now_level'];

			$sql .= " AND pro.now_level = $now_level ";
		}

		if ($param['emp_nm'] != '') {
			$emp_nm = $param['emp_nm'];

			$sql .= " AND ( ";
			$sql .= "  emp.emp_lt_nm || emp.emp_ft_nm LIKE '%' || '$emp_nm' || '%' ";
			$sql .= "  OR emp.emp_kn_lt_nm || emp.emp_kn_ft_nm LIKE '%' || '$emp_nm' || '%' ";
			$sql .= " ) ";
		}

		if ($param['class_id'] != '') {
			$class_id = $param['class_id'];
			$sql .= " AND emp.emp_class = $class_id ";
		}

		if ($param['atrb_id'] != '') {
			$atrb_id = $param['atrb_id'];
			$sql .= " AND emp.emp_attribute = $atrb_id ";
		}

		if ($param['dept_id'] != '') {
			$dept_id = $param['dept_id'];
			$sql .= " AND emp.emp_dept = $dept_id ";
		}

		if ($param['room_id'] != '') {
			$room_id = $param['room_id'];
			$sql .= " AND emp.emp_room = $room_id ";
		}

		// 2012/08/27 Yamagawa add(s)
		if ($param['retire_disp_flg'] == ''){
			$sql .= " AND ( ";
			$sql .= "  emp.emp_retire is null ";
			$sql .= "  OR trim(emp.emp_retire) = '' ";
			$sql .= "  OR to_date(trim(emp.emp_retire),'yyyymmdd') >= current_date ";
			$sql .= " ) ";
		}
		// 2012/08/27 Yamagawa add(e)

		$sql .= "ORDER BY ";
		$sql .= " emp.emp_personal_id ";
		$sql .= " ,apl.training_apply_id ";

		$data=parent::select($sql,$array_type,$array_param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_user_list() END count(data):".count($data));
		return $data;
	}

	/**
	* 研修一覧表示
	*/
	function get_training_list($param)
	{
		$this->log->info("get_training_list() START");

		if ($param['login_emp_id'] != ''){
			$login_emp_id = $param['login_emp_id'];
		}

		if ($param['now_level'] != ''){
			$now_level = $param['now_level'];
		}

		if ($param['emp_nm'] != '') {
			$emp_nm = $param['emp_nm'];
		}

		if ($param['class_id'] != '') {
			$class_id = $param['class_id'];
		}

		if ($param['atrb_id'] != '') {
			$atrb_id = $param['atrb_id'];
		}

		if ($param['dept_id'] != '') {
			$dept_id = $param['dept_id'];
		}

		if ($param['room_id'] != '') {
			$room_id = $param['room_id'];
		}

		if ($param['training_id'] != '') {
			$training_id = $param['training_id'];
		}

		if ($param['training_name'] != '') {
			$training_name = $param['training_name'];
		}

		if ($param['teacher_name'] != ''){
			$teacher_name = $param['teacher_name'];
		}

		if ($param['apply_date_from'] != ''){
			$apply_date_from = $param['apply_date_from'];
		}

		if ($param['apply_date_to'] != ''){
			$apply_date_to = $param['apply_date_to'];
		}

		if ($param['students_date_from'] != ''){
			$students_date_from = $param['students_date_from'];
		}

		if ($param['students_date_to'] != ''){
			$students_date_to = $param['students_date_to'];
		}

		if ($param['training_div'] != ''){
			$training_div = $param['training_div'];
		}

		if ($param['eval_plan_date_from'] != ''){
			// レベルアップ時に対応
		}

		if ($param['eval_plan_date_to'] != ''){
			// レベルアップ時に対応
		}

		if ($param['report_date_from'] != ''){
			$report_date_from = $param['report_date_from'];
		}

		if ($param['report_date_to'] != ''){
			$report_date_to = $param['report_date_to'];
		}

		if ($param['eval_date_from'] != ''){
			// レベルアップ時に対応
		}

		if ($param['eval_date_to'] != ''){
			// レベルアップ時に対応
		}

		$array_type = array();
		$array_param = array();
		//SQL作成


		//==================================================
		//検索１：申請済みの院内研修を取得
		//==================================================
		$sql = "";



		$sql .= "SELECT ";
		$sql .= " applied.emp_id ";
		$sql .= " ,emp.emp_personal_id ";
		$sql .= " ,emp.emp_lt_nm ";
		$sql .= " ,emp.emp_ft_nm ";
		$sql .= " ,class.class_nm ";
		$sql .= " ,atrb.atrb_nm ";
		$sql .= " ,dept.dept_nm ";
		$sql .= " ,room.room_nm ";
		$sql .= " ,pro.now_level ";
		$sql .= " ,applied.inside_training_id AS training_id ";
		$sql .= " ,tra.training_name ";
		$sql .= " ,teacher1.emp_lt_nm AS teacher1_lt_nm ";
		$sql .= " ,teacher1.emp_ft_nm AS teacher1_ft_nm ";
		$sql .= " ,teacher2.emp_lt_nm AS teacher2_lt_nm ";
		$sql .= " ,teacher2.emp_ft_nm AS teacher2_ft_nm ";
		$sql .= " ,teacher3.emp_lt_nm AS teacher3_lt_nm ";
		$sql .= " ,teacher3.emp_ft_nm AS teacher3_ft_nm ";
		$sql .= " ,teacher4.emp_lt_nm AS teacher4_lt_nm ";
		$sql .= " ,teacher4.emp_ft_nm AS teacher4_ft_nm ";
		$sql .= " ,teacher5.emp_lt_nm AS teacher5_lt_nm ";
		$sql .= " ,teacher5.emp_ft_nm AS teacher5_ft_nm ";
		$sql .= " ,CAST(applied.inside_training_date AS VARCHAR) AS apply_date ";
		$sql .= " ,sch.plan_date ";
		$sql .= " ,CAST(NULL AS DATE) AS from_date ";
		$sql .= " ,CAST(NULL AS DATE) AS to_date ";
		$sql .= " ,apl.apply_date AS report_date ";
		//$sql .= " ,sch.year ";
		$sql .= " ,CASE ";
		$sql .= "   WHEN ";
		$sql .= "    CAST(to_char(applied.inside_training_date,'MM') AS INTEGER) BETWEEN 4 AND 12 ";
		$sql .= "   THEN ";
		$sql .= "    CAST(to_char(applied.inside_training_date,'YYYY') AS INTEGER) ";
		$sql .= "   ELSE ";
		$sql .= "    CAST(to_char(applied.inside_training_date,'YYYY') AS INTEGER) - 1 ";
		$sql .= "  END AS year ";
		$sql .= " ,sch.plan_date AS sort_key ";

		$sql .= "FROM ";
		$sql .= " cl_applied_inside_training applied ";

		$sql .= " LEFT JOIN ";
		$sql .= "  cl_personal_profile pro ";
		$sql .= " ON ";
		$sql .= "  applied.emp_id = pro.emp_id ";

		$sql .= " INNER JOIN ";
		//$sql .= " (SELECT * FROM empmst WHERE EXISTS(SELECT authmst.emp_id FROM authmst WHERE NOT emp_del_flg AND authmst.emp_id = empmst.emp_id)) emp ";
		$sql .= "  empmst emp ";
		$sql .= " ON ";
		$sql .= "  applied.emp_id = emp.emp_id ";

		// 2012/08/27 Yamagawa add(s)
		$sql .= " INNER JOIN ";
		$sql .= "  jobmst job ";
		$sql .= " ON ";
		$sql .= "  job.job_id = emp.emp_job ";
		$sql .= "  AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/27 Yamagawa add(e)

		$sql .= " LEFT JOIN ";
		$sql .= "  classmst class ";
		$sql .= " ON ";
		$sql .= "  emp.emp_class = class.class_id ";

		$sql .= " LEFT JOIN ";
		$sql .= "  atrbmst atrb ";
		$sql .= " ON ";
		$sql .= "  emp.emp_attribute = atrb.atrb_id ";

		$sql .= " LEFT JOIN ";
		$sql .= "  deptmst dept ";
		$sql .= " ON ";
		$sql .= "  emp.emp_dept = dept.dept_id ";

		$sql .= " LEFT JOIN ";
		$sql .= "  classroom room ";
		$sql .= " ON ";
		$sql .= "  emp.emp_room = room.room_id ";

		$sql .= " LEFT JOIN ";
		$sql .= "  cl_mst_inside_training tra ";
		$sql .= " ON ";
		$sql .= "  applied.inside_training_id = tra.training_id ";

		$sql .= " LEFT JOIN ";
		$sql .= "  empmst teacher1 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher1 = teacher1.emp_id ";

		$sql .= " LEFT JOIN ";
		$sql .= "  empmst teacher2 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher2 = teacher2.emp_id ";

		$sql .= " LEFT JOIN ";
		$sql .= "  empmst teacher3 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher3 = teacher3.emp_id ";

		$sql .= " LEFT JOIN ";
		$sql .= "  empmst teacher4 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher4 = teacher4.emp_id ";

		$sql .= " LEFT JOIN ";
		$sql .= "  empmst teacher5 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher5 = teacher5.emp_id ";

		$sql .= " LEFT JOIN ";
		$sql .= "  cl_mst_inside_training_schedule sch ";
		$sql .= " ON ";
		$sql .= "  applied.plan_id LIKE '%' || sch.plan_id || '%' ";
		//$sql .= "  AND 0 = sch.delete_flg ";

		$sql .= " LEFT JOIN ";
		$sql .= "  cl_apl_inside_training_report repo ";
		$sql .= " ON ";
		$sql .= "  applied.emp_id = repo.emp_id ";
		$sql .= "  AND applied.training_apply_id = repo.training_apply_id ";
		$sql .= "  AND applied.inside_training_id = repo.training_id ";
		$sql .= "  AND sch.plan_id = repo.plan_id ";
		$sql .= "  AND 0 = repo.delete_flg ";
		$sql .= "  AND EXISTS( ";
		$sql .= "   SELECT ";
		$sql .= "    apl.apply_id ";
		$sql .= "   FROM ";
		$sql .= "    cl_apply apl ";
		$sql .= "   WHERE ";
		$sql .= "    repo.apply_id = apl.apply_id ";
		$sql .= "    AND FALSE = apl.delete_flg ";
		$sql .= "    AND '1' = apl.apply_stat ";
		$sql .= "  ) ";

		$sql .= " LEFT JOIN ";
		$sql .= "  cl_apply apl ";
		$sql .= " ON ";
		$sql .= "  repo.apply_id = apl.apply_id ";
		$sql .= "  AND FALSE = apl.delete_flg ";
		$sql .= "  AND '1' = apl.apply_stat ";

		$sql .= "WHERE ";
		$sql .= " applied.delete_flg = 0 ";

		if ($param['login_emp_id'] != '') {
			$sql .= " AND EXISTS( ";//EXISTS()開始

			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";

			switch($param['class_division']){
				case 1:
					$sql .= "   AND emp.emp_class = loginemp.emp_class ";
					break;
				case 2:
					$sql .= "   AND emp.emp_attribute = loginemp.emp_attribute ";
					break;
				case 3:
					$sql .= "   AND emp.emp_dept = loginemp.emp_dept ";
					break;
				case 4:
					$sql .= "   AND emp.emp_room = loginemp.emp_room ";
					break;
			}

			$sql .= "  UNION ALL ";

			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent con ";
			$sql .= "   ON ";

			switch($param['class_division']){
				case 1:
					$sql .= "    loginemp.emp_class = con.emp_class ";
					break;
				case 2:
					$sql .= "    loginemp.emp_attribute = con.emp_attribute ";
					break;
				case 3:
					$sql .= "    loginemp.emp_dept = con.emp_dept ";
					break;
				case 4:
					$sql .= "    loginemp.emp_room = con.emp_room ";
					break;
			}

			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";
			$sql .= "   AND emp.emp_id = con.emp_id ";

			$sql .= "  UNION ALL ";

			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent logincon ";
			$sql .= "   ON ";
			$sql .= "    loginemp.emp_id = logincon.emp_id ";
			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";

			switch($param['class_division']){
				case 1:
					$sql .= "   AND emp.emp_class = logincon.emp_class ";
					break;
				case 2:
					$sql .= "   AND emp.emp_attribute = logincon.emp_attribute ";
					break;
				case 3:
					$sql .= "   AND emp.emp_dept = logincon.emp_dept ";
					break;
				case 4:
					$sql .= "   AND emp.emp_room = logincon.emp_room ";
					break;
			}

			$sql .= "  UNION ALL ";

			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent logincon ";
			$sql .= "   ON ";
			$sql .= "    loginemp.emp_id = logincon.emp_id ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent con ";
			$sql .= "   ON ";

			switch($param['class_division']){
				case 1:
					$sql .= "    logincon.emp_class = con.emp_class ";
					break;
				case 2:
					$sql .= "    logincon.emp_attribute = con.emp_attribute ";
					break;
				case 3:
					$sql .= "    logincon.emp_dept = con.emp_dept ";
					break;
				case 4:
					$sql .= "    logincon.emp_room = con.emp_room ";
					break;
			}

			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";
			$sql .= "   AND emp.emp_id = con.emp_id ";


			$sql .= " ) ";//EXISTS()終了
		}

		if ($param['now_level'] != ''){
			$sql .= " AND pro.now_level = $now_level ";
		}

		if ($param['emp_nm'] != '') {
			$sql .= " AND ( ";
			$sql .= "  emp.emp_lt_nm || emp.emp_ft_nm LIKE '%' || '$emp_nm' || '%' ";
			$sql .= "  OR emp.emp_kn_lt_nm || emp.emp_kn_ft_nm LIKE '%' || '$emp_nm' || '%' ";
			$sql .= " ) ";
		}

		if ($param['class_id'] != '') {
			$sql .= " AND emp.emp_class = $class_id  ";
		}

		if ($param['atrb_id'] != '') {
			$sql .= " AND emp.emp_attribute = $atrb_id ";
		}

		if ($param['dept_id'] != '') {
			$sql .= " AND emp.emp_dept = $dept_id ";
		}

		if ($param['room_id'] != '') {
			$sql .= " AND emp.emp_room = $room_id ";
		}

		if ($param['training_id'] != ''){
			$sql .= " AND applied.inside_training_id LIKE '%' || '$training_id' || '%' ";
		}

		if ($param['training_name'] != ''){
			$sql .= " AND tra.training_name LIKE '%' || '$training_name' || '%' ";
		}

		if ($param['teacher_name'] != ''){
			$sql .= " AND ( ";
			$sql .= "  teacher1.emp_lt_nm || teacher1.emp_ft_nm LIKE '%' || '$teacher_name' || '%' ";
			$sql .= "  OR teacher1.emp_kn_lt_nm || teacher1.emp_kn_ft_nm LIKE '%' || '$teacher_name' || '%' ";
			$sql .= "  OR teacher2.emp_lt_nm || teacher2.emp_ft_nm LIKE '%' || '$teacher_name' || '%' ";
			$sql .= "  OR teacher2.emp_kn_lt_nm || teacher2.emp_kn_ft_nm LIKE '%' || '$teacher_name' || '%' ";
			$sql .= "  OR teacher3.emp_lt_nm || teacher3.emp_ft_nm LIKE '%' || '$teacher_name' || '%' ";
			$sql .= "  OR teacher3.emp_kn_lt_nm || teacher3.emp_kn_ft_nm LIKE '%' || '$teacher_name' || '%' ";
			$sql .= "  OR teacher4.emp_lt_nm || teacher4.emp_ft_nm LIKE '%' || '$teacher_name' || '%' ";
			$sql .= "  OR teacher4.emp_kn_lt_nm || teacher4.emp_kn_ft_nm LIKE '%' || '$teacher_name' || '%' ";
			$sql .= "  OR teacher5.emp_lt_nm || teacher5.emp_ft_nm LIKE '%' || '$teacher_name' || '%' ";
			$sql .= "  OR teacher5.emp_kn_lt_nm || teacher5.emp_kn_ft_nm LIKE '%' || '$teacher_name' || '%' ";
			$sql .= " ) ";
		}

		if ($param['apply_date_from'] != ''){
			$sql .= " AND to_char(applied.inside_training_date,'YYYYMMDD') >= '$apply_date_from' ";
		}

		if ($param['apply_date_to'] != ''){
			$sql .= " AND to_char(applied.inside_training_date,'YYYYMMDD') <= '$apply_date_to' ";
		}

		if ($param['students_date_from'] != ''){
			$sql .= " AND to_char(sch.plan_date,'YYYYMMDD') >= '$students_date_from' ";
		}

		if ($param['students_date_to'] != ''){
			$sql .= " AND to_char(sch.plan_date,'YYYYMMDD') <= '$students_date_to' ";
		}

		if ($param['eval_plan_date_from'] != ''){
			// レベルアップ時に対応
		}

		if ($param['eval_plan_date_to'] != ''){
			// レベルアップ時に対応
		}

		if ($param['report_date_from'] != ''){
			$sql .= " AND apl.apply_date >= '$report_date_from' ";
		}

		if ($param['report_date_to'] != ''){
			$sql .= " AND apl.apply_date <= '$report_date_to' ";
		}

		if ($param['eval_date_from'] != ''){
			// レベルアップ時に対応
		}

		if ($param['eval_date_to'] != ''){
			// レベルアップ時に対応
		}

		// 2012/08/27 Yamagawa add(s)
		if ($param['retire_disp_flg'] == ''){
			$sql .= " AND ( ";
			$sql .= "  emp.emp_retire is null ";
			$sql .= "  OR trim(emp.emp_retire) = '' ";
			$sql .= "  OR to_date(trim(emp.emp_retire),'yyyymmdd') >= current_date ";
			$sql .= " ) ";
		}
		// 2012/08/27 Yamagawa add(e)

		$sql1 = $sql;

		//==================================================
		//検索２：申請中の院内研修を取得
		//==================================================
		$sql = "";

		//申請日、評価予定日が設定されていない場合
		if (
			$param['report_date_from'] == ''
			&& $param['report_date_to'] == ''
			&& $param['eval_date_from'] == ''
			&& $param['eval_date_to'] == ''
		){

			$sql .= "SELECT ";
			$sql .= " apl.emp_id ";
			$sql .= " ,emp.emp_personal_id ";
			$sql .= " ,emp.emp_lt_nm ";
			$sql .= " ,emp.emp_ft_nm ";
			$sql .= " ,class.class_nm ";
			$sql .= " ,atrb.atrb_nm ";
			$sql .= " ,dept.dept_nm ";
			$sql .= " ,room.room_nm ";
			$sql .= " ,pro.now_level ";
			$sql .= " ,apl.inside_training_id AS training_id ";
			$sql .= " ,tra.training_name ";
			$sql .= " ,teacher1.emp_lt_nm AS teacher1_lt_nm ";
			$sql .= " ,teacher1.emp_ft_nm AS teacher1_ft_nm ";
			$sql .= " ,teacher2.emp_lt_nm AS teacher2_lt_nm ";
			$sql .= " ,teacher2.emp_ft_nm AS teacher2_ft_nm ";
			$sql .= " ,teacher3.emp_lt_nm AS teacher3_lt_nm ";
			$sql .= " ,teacher3.emp_ft_nm AS teacher3_ft_nm ";
			$sql .= " ,teacher4.emp_lt_nm AS teacher4_lt_nm ";
			$sql .= " ,teacher4.emp_ft_nm AS teacher4_ft_nm ";
			$sql .= " ,teacher5.emp_lt_nm AS teacher5_lt_nm ";
			$sql .= " ,teacher5.emp_ft_nm AS teacher5_ft_nm ";
			$sql .= " ,CAST(apl.inside_training_date AS VARCHAR) AS apply_date  ";
			$sql .= " ,sch.plan_date ";
			$sql .= " ,CAST(NULL AS DATE) AS from_date ";
			$sql .= " ,CAST(NULL AS DATE) AS to_date ";
			$sql .= " ,CAST(NULL AS VARCHAR) AS report_date ";
			//$sql .= " ,sch.year ";
			$sql .= " ,CASE ";
			$sql .= "   WHEN ";
			$sql .= "    CAST(to_char(apl.inside_training_date,'MM') AS INTEGER) BETWEEN 4 AND 12 ";
			$sql .= "   THEN ";
			$sql .= "    CAST(to_char(apl.inside_training_date,'YYYY') AS INTEGER) ";
			$sql .= "   ELSE ";
			$sql .= "    CAST(to_char(apl.inside_training_date,'YYYY') AS INTEGER) - 1 ";
			$sql .= "  END AS year ";
			$sql .= " ,sch.plan_date AS sort_key ";

			$sql .= "FROM ";
			$sql .= " cl_apl_inside_training_apply apl ";
			$sql .= " LEFT JOIN ";
			$sql .= "  cl_personal_profile pro ";
			$sql .= " ON ";
			$sql .= "  apl.emp_id = pro.emp_id ";

			$sql .= " INNER JOIN ";
			//$sql .= " (SELECT * FROM empmst WHERE EXISTS(SELECT authmst.emp_id FROM authmst WHERE NOT emp_del_flg AND authmst.emp_id = empmst.emp_id)) emp ";
			$sql .= "  empmst emp ";
			$sql .= " ON ";
			$sql .= "  apl.emp_id = emp.emp_id ";

			// 2012/08/27 Yamagawa add(s)
			$sql .= " INNER JOIN ";
			$sql .= "  jobmst job ";
			$sql .= " ON ";
			$sql .= "  job.job_id = emp.emp_job ";
			$sql .= "  AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
			// 2012/08/27 Yamagawa add(e)

			$sql .= " LEFT JOIN ";
			$sql .= "  classmst class ";
			$sql .= " ON ";
			$sql .= "  emp.emp_class = class.class_id ";

			$sql .= " LEFT JOIN ";
			$sql .= "  atrbmst atrb ";
			$sql .= " ON ";
			$sql .= "  emp.emp_attribute = atrb.atrb_id ";

			$sql .= " LEFT JOIN ";
			$sql .= "  deptmst dept ";
			$sql .= " ON ";
			$sql .= "  emp.emp_dept = dept.dept_id ";

			$sql .= " LEFT JOIN ";
			$sql .= "  classroom room ";
			$sql .= " ON ";
			$sql .= "  emp.emp_room = room.room_id ";

			$sql .= " LEFT JOIN ";
			$sql .= "  cl_mst_inside_training tra ";
			$sql .= " ON ";
			$sql .= "  apl.inside_training_id = tra.training_id ";

			$sql .= " LEFT JOIN ";
			$sql .= "  empmst teacher1 ";
			$sql .= " ON ";
			$sql .= "  tra.training_teacher1 = teacher1.emp_id ";

			$sql .= " LEFT JOIN ";
			$sql .= "  empmst teacher2 ";
			$sql .= " ON ";
			$sql .= "  tra.training_teacher2 = teacher2.emp_id ";

			$sql .= " LEFT JOIN ";
			$sql .= "  empmst teacher3 ";
			$sql .= " ON ";
			$sql .= "  tra.training_teacher3 = teacher3.emp_id ";

			$sql .= " LEFT JOIN ";
			$sql .= "  empmst teacher4 ";
			$sql .= " ON ";
			$sql .= "  tra.training_teacher4 = teacher4.emp_id ";

			$sql .= " LEFT JOIN ";
			$sql .= "  empmst teacher5 ";
			$sql .= " ON ";
			$sql .= "  tra.training_teacher5 = teacher5.emp_id ";

			$sql .= " LEFT JOIN ";
			$sql .= "  cl_mst_inside_training_schedule sch ";
			$sql .= " ON ";
			$sql .= "  apl.plan_id LIKE '%' || sch.plan_id || '%' ";

			$sql .= "WHERE ";
			$sql .= " apl.delete_flg = 0 ";

			$sql .= " AND EXISTS( ";
			$sql .= "  SELECT";
			$sql .= "   apply.apply_id ";
			$sql .= "  FROM ";
			$sql .= "   cl_apply apply ";
			$sql .= "  WHERE ";
			$sql .= "   apl.apply_id = apply.apply_id ";
			$sql .= "   AND apply.apply_stat IN ('0','1') ";
			$sql .= "   AND apply.draft_flg = FALSE ";
			$sql .= "   AND apply.delete_flg = FALSE ";
			$sql .= " )";

			if ($param['login_emp_id'] != '') {
				$sql .= " AND EXISTS( ";//EXISTS()開始

				$sql .= "  SELECT ";
				$sql .= "   loginemp.emp_id ";
				$sql .= "  FROM ";
				$sql .= "   empmst loginemp ";
				$sql .= "  WHERE ";
				$sql .= "   '$login_emp_id' = loginemp.emp_id ";

				switch($param['class_division']){
					case 1:
						$sql .= "   AND emp.emp_class = loginemp.emp_class ";
						break;
					case 2:
						$sql .= "   AND emp.emp_attribute = loginemp.emp_attribute ";
						break;
					case 3:
						$sql .= "   AND emp.emp_dept = loginemp.emp_dept ";
						break;
					case 4:
						$sql .= "   AND emp.emp_room = loginemp.emp_room ";
						break;
				}

				$sql .= "  UNION ALL ";

				$sql .= "  SELECT ";
				$sql .= "   loginemp.emp_id ";
				$sql .= "  FROM ";
				$sql .= "   empmst loginemp ";
				$sql .= "   INNER JOIN ";
				$sql .= "    concurrent con ";
				$sql .= "   ON ";

				switch($param['class_division']){
					case 1:
						$sql .= "    loginemp.emp_class = con.emp_class ";
						break;
					case 2:
						$sql .= "    loginemp.emp_attribute = con.emp_attribute ";
						break;
					case 3:
						$sql .= "    loginemp.emp_dept = con.emp_dept ";
						break;
					case 4:
						$sql .= "    loginemp.emp_room = con.emp_room ";
						break;
				}

				$sql .= "  WHERE ";
				$sql .= "   '$login_emp_id' = loginemp.emp_id ";
				$sql .= "   AND emp.emp_id = con.emp_id ";

				$sql .= "  UNION ALL ";

				$sql .= "  SELECT ";
				$sql .= "   loginemp.emp_id ";
				$sql .= "  FROM ";
				$sql .= "   empmst loginemp ";
				$sql .= "   INNER JOIN ";
				$sql .= "    concurrent logincon ";
				$sql .= "   ON ";
				$sql .= "    loginemp.emp_id = logincon.emp_id ";
				$sql .= "  WHERE ";
				$sql .= "   '$login_emp_id' = loginemp.emp_id ";

				switch($param['class_division']){
					case 1:
						$sql .= "   AND emp.emp_class = logincon.emp_class ";
						break;
					case 2:
						$sql .= "   AND emp.emp_attribute = logincon.emp_attribute ";
						break;
					case 3:
						$sql .= "   AND emp.emp_dept = logincon.emp_dept ";
						break;
					case 4:
						$sql .= "   AND emp.emp_room = logincon.emp_room ";
						break;
				}

				$sql .= "  UNION ALL ";

				$sql .= "  SELECT ";
				$sql .= "   loginemp.emp_id ";
				$sql .= "  FROM ";
				$sql .= "   empmst loginemp ";
				$sql .= "   INNER JOIN ";
				$sql .= "    concurrent logincon ";
				$sql .= "   ON ";
				$sql .= "    loginemp.emp_id = logincon.emp_id ";
				$sql .= "   INNER JOIN ";
				$sql .= "    concurrent con ";
				$sql .= "   ON ";

				switch($param['class_division']){
					case 1:
						$sql .= "    logincon.emp_class = con.emp_class ";
						break;
					case 2:
						$sql .= "    logincon.emp_attribute = con.emp_attribute ";
						break;
					case 3:
						$sql .= "    logincon.emp_dept = con.emp_dept ";
						break;
					case 4:
						$sql .= "    logincon.emp_room = con.emp_room ";
						break;
				}

				$sql .= "  WHERE ";
				$sql .= "   '$login_emp_id' = loginemp.emp_id ";
				$sql .= "   AND emp.emp_id = con.emp_id ";

				$sql .= " ) ";//EXISTS()終了
			}

			$sql .= " AND NOT EXISTS( ";
			$sql .= "  SELECT ";
			$sql .= "   applied.training_apply_id ";
			$sql .= "  FROM ";
			$sql .= "   cl_applied_inside_training applied ";
			$sql .= "  WHERE ";
			$sql .= "   applied.training_apply_id = apl.first_apply_id ";
			$sql .= " ) ";

			if ($param['now_level'] != ''){
				$sql .= " AND pro.now_level = $now_level ";
			}

			if ($param['emp_nm'] != '') {
				$sql .= " AND ( ";
				$sql .= "  emp.emp_lt_nm || emp.emp_ft_nm LIKE '%' || '$emp_nm' || '%' ";
				$sql .= "  OR emp.emp_kn_lt_nm || emp.emp_kn_ft_nm LIKE '%' || '$emp_nm' || '%' ";
				$sql .= " ) ";
			}

			if ($param['class_id'] != '') {
				$sql .= " AND emp.emp_class = $class_id ";
			}

			if ($param['atrb_id'] != '') {
				$sql .= " AND emp.emp_attribute = $atrb_id ";
			}

			if ($param['dept_id'] != '') {
				$sql .= " AND emp.emp_dept = $dept_id ";
			}

			if ($param['room_id'] != '') {
				$sql .= " AND emp.emp_room = $room_id ";
			}

			if ($param['training_id'] != ''){
				$sql .= " AND apl.inside_training_id LIKE '%' || '$training_id' || '%' ";
			}

			if ($param['training_name'] != '') {
				$sql .= " AND tra.training_name LIKE '%' || '$training_name' || '%' ";
			}

			if ($param['teacher_name'] != ''){
				$sql .= " AND ( ";
				$sql .= "  teacher1.emp_lt_nm || teacher1.emp_ft_nm LIKE '%' || '$teacher_name' || '%' ";
				$sql .= "  OR teacher1.emp_kn_lt_nm || teacher1.emp_kn_ft_nm LIKE '%' || '$teacher_name' || '%' ";
				$sql .= "  OR teacher2.emp_lt_nm || teacher2.emp_ft_nm LIKE '%' || '$teacher_name' || '%' ";
				$sql .= "  OR teacher2.emp_kn_lt_nm || teacher2.emp_kn_ft_nm LIKE '%' || '$teacher_name' || '%' ";
				$sql .= "  OR teacher3.emp_lt_nm || teacher3.emp_ft_nm LIKE '%' || '$teacher_name' || '%' ";
				$sql .= "  OR teacher3.emp_kn_lt_nm || teacher3.emp_kn_ft_nm LIKE '%' || '$teacher_name' || '%' ";
				$sql .= "  OR teacher4.emp_lt_nm || teacher4.emp_ft_nm LIKE '%' || '$teacher_name' || '%' ";
				$sql .= "  OR teacher4.emp_kn_lt_nm || teacher4.emp_kn_ft_nm LIKE '%' || '$teacher_name' || '%' ";
				$sql .= "  OR teacher5.emp_lt_nm || teacher5.emp_ft_nm LIKE '%' || '$teacher_name' || '%' ";
				$sql .= "  OR teacher5.emp_kn_lt_nm || teacher5.emp_kn_ft_nm LIKE '%' || '$teacher_name' || '%' ";
				$sql .= " ) ";
			}

			if ($param['apply_date_from'] != ''){
				$sql .= " AND to_char(apl.inside_training_date,'YYYYMMDD') >= '$apply_date_from' ";
			}

			if ($param['apply_date_to'] != '') {
				$sql .= " AND to_char(apl.inside_training_date,'YYYYMMDD') <= '$apply_date_to' ";
			}

			if ($param['students_date_from'] != '') {
				$sql .= " AND to_char(sch.plan_date,'YYYYMMDD') >= '$students_date_from' ";
			}

			if ($param['students_date_to'] != '') {
				$sql .= " AND to_char(sch.plan_date,'YYYYMMDD') <= '$students_date_to' ";
			}

			if ($param['eval_plan_date_from'] != ''){
				// レベルアップ時に対応
			}

			if ($param['eval_plan_date_to'] != ''){
				// レベルアップ時に対応
			}

			// 2012/08/27 Yamagawa add(s)
			if ($param['retire_disp_flg'] == ''){
				$sql .= " AND ( ";
				$sql .= "  emp.emp_retire is null ";
				$sql .= "  OR trim(emp.emp_retire) = '' ";
				$sql .= "  OR to_date(trim(emp.emp_retire),'yyyymmdd') >= current_date ";
				$sql .= " ) ";
			}
			// 2012/08/27 Yamagawa add(e)

		}

		$sql2 = $sql;


		//==================================================
		//検索３：院外研修を取得
		//==================================================
		$sql = "";


		$sql .= "SELECT ";
		$sql .= " sem.emp_id ";
		$sql .= " ,emp.emp_personal_id ";
		$sql .= " ,emp.emp_lt_nm ";
		$sql .= " ,emp.emp_ft_nm ";
		$sql .= " ,class.class_nm ";
		$sql .= " ,atrb.atrb_nm ";
		$sql .= " ,dept.dept_nm ";
		$sql .= " ,room.room_nm ";
		$sql .= " ,pro.now_level ";
		$sql .= " ,sem.seminar_apply_id AS training_id ";
		$sql .= " ,sem.seminar_name AS training_name ";
		$sql .= " ,sem.teacher_name AS teacher1_lt_nm ";
		$sql .= " ,CAST(NULL AS VARCHAR) AS teacher1_ft_nm ";
		$sql .= " ,CAST(NULL AS VARCHAR) AS teacher2_lt_nm ";
		$sql .= " ,CAST(NULL AS VARCHAR) AS teacher2_ft_nm ";
		$sql .= " ,CAST(NULL AS VARCHAR) AS teacher3_lt_nm ";
		$sql .= " ,CAST(NULL AS VARCHAR) AS teacher3_ft_nm ";
		$sql .= " ,CAST(NULL AS VARCHAR) AS teacher4_lt_nm ";
		$sql .= " ,CAST(NULL AS VARCHAR) AS teacher4_ft_nm ";
		$sql .= " ,CAST(NULL AS VARCHAR) AS teacher5_lt_nm ";
		$sql .= " ,CAST(NULL AS VARCHAR) AS teacher5_ft_nm ";
		$sql .= " ,substr(apl.apply_date,1,4) || '-' || substr(apl.apply_date,5,2) || '-' || substr(apl.apply_date,7,2) AS apply_date ";
		$sql .= " ,CAST(NULL AS DATE) AS plan_date ";
		$sql .= " ,sem.from_date ";
		$sql .= " ,sem.to_date ";
		$sql .= " ,to_char(sem.report_date, 'YYYYMMDD') AS report_date ";
		/*
		$sql .= " ,CASE ";
		$sql .= "   WHEN ";
		$sql .= "    CAST(to_char(sem.from_date,'MM') AS INTEGER) BETWEEN 4 AND 12 ";
		$sql .= "   THEN ";
		$sql .= "    CAST(to_char(sem.from_date,'YYYY') AS INTEGER) ";
		$sql .= "   ELSE ";
		$sql .= "    CAST(to_char(sem.from_date,'YYYY') AS INTEGER) - 1 ";
		$sql .= "  END AS year ";
		*/
		$sql .= " ,CASE ";
		$sql .= "   WHEN ";
		$sql .= "    CAST(to_char(to_date(apl.apply_date,'YYYYMMDD'),'MM') AS INTEGER) BETWEEN 4 AND 12 ";
		$sql .= "   THEN ";
		$sql .= "    CAST(to_char(to_date(apl.apply_date,'YYYYMMDD'),'YYYY') AS INTEGER) ";
		$sql .= "   ELSE ";
		$sql .= "    CAST(to_char(to_date(apl.apply_date,'YYYYMMDD'),'YYYY') AS INTEGER) - 1 ";
		$sql .= "  END AS year ";
		$sql .= " ,sem.from_date AS sort_key ";

		$sql .= "FROM ";
		$sql .= " cl_apl_outside_seminar sem ";

		$sql .= " INNER JOIN ";
		//$sql .= " (SELECT * FROM empmst WHERE EXISTS(SELECT authmst.emp_id FROM authmst WHERE NOT emp_del_flg AND authmst.emp_id = empmst.emp_id)) emp ";
		$sql .= "  empmst emp ";
		$sql .= " ON ";
		$sql .= "  sem.emp_id = emp.emp_id ";

		// 2012/08/27 Yamagawa add(s)
		$sql .= " INNER JOIN ";
		$sql .= "  jobmst job ";
		$sql .= " ON ";
		$sql .= "  job.job_id = emp.emp_job ";
		$sql .= "  AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/27 Yamagawa add(e)

		$sql .= " LEFT JOIN ";
		$sql .= "  cl_personal_profile pro ";
		$sql .= " ON ";
		$sql .= "  sem.emp_id = pro.emp_id ";

		$sql .= " LEFT JOIN ";
		$sql .= "  classmst class ";
		$sql .= " ON ";
		$sql .= "  emp.emp_class = class.class_id ";

		$sql .= " LEFT JOIN ";
		$sql .= "  atrbmst atrb ";
		$sql .= " ON ";
		$sql .= "  emp.emp_attribute = atrb.atrb_id ";

		$sql .= " LEFT JOIN ";
		$sql .= "  deptmst dept ";
		$sql .= " ON ";
		$sql .= "  emp.emp_dept = dept.dept_id ";

		$sql .= " LEFT JOIN ";
		$sql .= "  classroom room ";
		$sql .= " ON ";
		$sql .= "  emp.emp_room = room.room_id ";

		$sql .= " INNER JOIN ";
		$sql .= "  cl_apply apl ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = sem.apply_id ";
		$sql .= "  AND apl.apply_stat IN ('0','1') ";
		$sql .= "  AND apl.draft_flg = FALSE ";
		$sql .= "  AND apl.delete_flg = FALSE ";

		$sql .= "WHERE ";
		$sql .= " sem.delete_flg = 0 ";

		if ($param['login_emp_id'] != '') {
			$sql .= " AND EXISTS( ";//EXISTS()開始

			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";

			switch($param['class_division']){
				case 1:
					$sql .= "   AND emp.emp_class = loginemp.emp_class ";
					break;
				case 2:
					$sql .= "   AND emp.emp_attribute = loginemp.emp_attribute ";
					break;
				case 3:
					$sql .= "   AND emp.emp_dept = loginemp.emp_dept ";
					break;
				case 4:
					$sql .= "   AND emp.emp_room = loginemp.emp_room ";
					break;
			}

			$sql .= "  UNION ALL ";

			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent con ";
			$sql .= "   ON ";

			switch($param['class_division']){
				case 1:
					$sql .= "    loginemp.emp_class = con.emp_class ";
					break;
				case 2:
					$sql .= "    loginemp.emp_attribute = con.emp_attribute ";
					break;
				case 3:
					$sql .= "    loginemp.emp_dept = con.emp_dept ";
					break;
				case 4:
					$sql .= "    loginemp.emp_room = con.emp_room ";
					break;
			}

			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";
			$sql .= "   AND emp.emp_id = con.emp_id ";
			$sql .= "  UNION ALL ";
			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent logincon ";
			$sql .= "   ON ";
			$sql .= "    loginemp.emp_id = logincon.emp_id ";
			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";

			switch($param['class_division']){
				case 1:
					$sql .= "   AND emp.emp_class = logincon.emp_class ";
					break;
				case 2:
					$sql .= "   AND emp.emp_attribute = logincon.emp_attribute ";
					break;
				case 3:
					$sql .= "   AND emp.emp_dept = logincon.emp_dept ";
					break;
				case 4:
					$sql .= "   AND emp.emp_room = logincon.emp_room ";
					break;
			}

			$sql .= "  UNION ALL ";

			$sql .= "  SELECT ";
			$sql .= "   loginemp.emp_id ";
			$sql .= "  FROM ";
			$sql .= "   empmst loginemp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent logincon ";
			$sql .= "   ON ";
			$sql .= "    loginemp.emp_id = logincon.emp_id ";
			$sql .= "   INNER JOIN ";
			$sql .= "    concurrent con ";
			$sql .= "   ON ";

			switch($param['class_division']){
				case 1:
					$sql .= "    logincon.emp_class = con.emp_class ";
					break;
				case 2:
					$sql .= "    logincon.emp_attribute = con.emp_attribute ";
					break;
				case 3:
					$sql .= "    logincon.emp_dept = con.emp_dept ";
					break;
				case 4:
					$sql .= "    logincon.emp_room = con.emp_room ";
					break;
			}

			$sql .= "  WHERE ";
			$sql .= "   '$login_emp_id' = loginemp.emp_id ";
			$sql .= "   AND emp.emp_id = con.emp_id ";

			$sql .= " ) ";//EXISTS()終了
		}

		if ($param['now_level'] != ''){
			$sql .= " AND pro.now_level = $now_level ";
		}

		if ($param['emp_nm'] != '') {
			$sql .= " AND ( ";
			$sql .= "  emp.emp_lt_nm || emp.emp_ft_nm LIKE '%' || '$emp_nm' || '%' ";
			$sql .= "  OR emp.emp_kn_lt_nm || emp.emp_kn_ft_nm LIKE '%' || '$emp_nm' || '%' ";
			$sql .= " ) ";
		}

		if ($param['class_id'] != '') {
			$sql .= " AND emp.emp_class = $class_id ";
		}

		if ($param['atrb_id'] != '') {
			$sql .= " AND emp.emp_attribute = $atrb_id ";
		}

		if ($param['dept_id'] != '') {
			$sql .= " AND emp.emp_dept = $dept_id ";
		}

		if ($param['room_id'] != '') {
			$sql .= " AND emp.emp_room = $room_id ";
		}

		if ($param['training_id'] != '') {
			$sql .= " AND sem.seminar_apply_id LIKE '%' || '$training_id' || '%' ";
		}

		if ($param['training_name'] != '') {
			$sql .= " AND sem.seminar_name LIKE '%' || '$training_name' || '%' ";
		}

		if ($param['teacher_name'] != ''){
			$sql .= " AND sem.teacher_name LIKE '%' || '$teacher_name' || '%' ";
		}

		if ($param['apply_date_from'] != ''){
			$sql .= " AND apl.apply_date >= '$apply_date_from' ";
		}

		if ($param['apply_date_to'] != ''){
			$sql .= " AND apl.apply_date <= '$apply_date_to' ";
		}

		if ($param['students_date_from'] != ''){
			$sql .= " AND ( ";
			$sql .= "  to_char(sem.from_date,'YYYYMMDD') >= '$students_date_from' ";
			$sql .= "  OR to_char(sem.to_date,'YYYYMMDD') >= '$students_date_from' ";
			$sql .= " ) ";
		}

		if ($param['students_date_to'] != ''){
			$sql .= " AND ( ";
			$sql .= "  to_char(sem.from_date,'YYYYMMDD') <= '$students_date_to' ";
			$sql .= "  OR to_char(sem.to_date,'YYYYMMDD') <= '$students_date_to' ";
			$sql .= " ) ";
		}

		if ($param['eval_plan_date_from'] != ''){
			// レベルアップ時に対応
		}

		if ($param['eval_plan_date_to'] != ''){
			// レベルアップ時に対応
		}

		if ($param['report_date_from'] != ''){
			$sql .= " AND to_char(sem.report_date,'YYYYMMDD') >= '$report_date_from' ";
		}

		if ($param['report_date_to'] != ''){
			$sql .= " AND to_char(sem.report_date,'YYYYMMDD') <= '$report_date_to' ";
		}

		if ($param['eval_date_from'] != ''){
			// レベルアップ時に対応
		}

		if ($param['eval_date_to'] != ''){
			// レベルアップ時に対応
		}

		// 2012/08/27 Yamagawa add(s)
		if ($param['retire_disp_flg'] == ''){
			$sql .= " AND ( ";
			$sql .= "  emp.emp_retire is null ";
			$sql .= "  OR trim(emp.emp_retire) = '' ";
			$sql .= "  OR to_date(trim(emp.emp_retire),'yyyymmdd') >= current_date ";
			$sql .= " ) ";
		}
		// 2012/08/27 Yamagawa add(e)

		$sql3 = $sql;




		//==================================================
		//検索１-３をUNION
		//==================================================
		$sql = "";

		//研修区分：院内研修の場合
		if($training_div == 1)
		{
			//検索１：申請済みの院内研修
			$sql .= $sql1;

			//検索２：申請中の院内研修
			if ($sql2 != "")
			{
				$sql .= " UNION ALL ".$sql2;
			}
		}
		//研修区分：院外研修の場合
		elseif($training_div == 2)
		{
			//検索３：院外研修
			$sql .= $sql3;
		}
		//研修区分：条件なしの場合
		else
		{
			//検索１：申請済みの院内研修
			$sql .= $sql1;

			//検索２：申請中の院内研修
			if ($sql2 != "")
			{
				$sql .= " UNION ALL ".$sql2;
			}

			//検索３：院外研修
			$sql .= " UNION ALL ".$sql3;
		}

		$sql .= "ORDER BY ";
		$sql .= " emp_personal_id ";
		$sql .= " ,sort_key DESC ";

		$data=parent::select($sql,$array_type,$array_param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_training_list() END count(data):".count($data));
		return $data;
	}

}
