<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class stmst_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function stmst_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	* 役職マスタ取得
	*/
	function getList(){
		$this->log->info("getList() START");

		//SQL作成
		$sql = "";
		$sql .= "SELECT";
		$sql .= " st_id,";
		$sql .= " st_nm ";
		$sql .= "FROM";
		$sql .= " stmst ";
		$sql .= "WHERE";
		$sql .= " st_del_flg = 'f' ";
		$sql .= "ORDER BY";
		$sql .= " order_no";

		$data=parent::select($sql,null,array(),MDB2_FETCHMODE_ASSOC);

		$this->log->info("getList() END");
		return $data;
	}

	/**
	 * 担当者設定 所属長取得
	 */
	function getList_supervisor(){

		$this->log->info("getList_supervisor() START");

		// SQL作成
		$sql = "";
		$sql .= "SELECT";
		$sql .= " st.st_id,";
		$sql .= " st.st_nm,";
		$sql .= " case when cl.st_id is not null then 'checked' else null end as checked ";
		$sql .= "FROM";
		$sql .= " stmst st";
		$sql .= " left join";
		$sql .= "  cl_mst_supervisor_st cl";
		$sql .= " on";
		$sql .= "  st.st_id = cl.st_id ";
		$sql .= "WHERE";
		$sql .= " st.st_del_flg = 'f' ";
		$sql .= "ORDER BY";
		$sql .= " st.order_no";

		$data=parent::select($sql,null,null,MDB2_FETCHMODE_ASSOC);

		$this->log->info("getList_supervisor() END");
		return $data;

	}

	/**
	 * 担当者設定 看護部長取得
	 */
	function getList_nurse_manager(){

		$this->log->info("getList_nurse_manager() START");

		// SQL作成
		$sql = "";
		$sql .= "SELECT";
		$sql .= " st.st_id,";
		$sql .= " st.st_nm,";
		$sql .= " case when cl.st_id is not null then 'checked' else null end as checked ";
		$sql .= "FROM";
		$sql .= " stmst st";
		$sql .= " left join";
		$sql .= "  cl_mst_nurse_manager_st cl";
		$sql .= " on";
		$sql .= "  st.st_id = cl.st_id ";
		$sql .= "WHERE";
		$sql .= " st.st_del_flg = 'f' ";
		$sql .= "ORDER BY";
		$sql .= " st.order_no";

		$data=parent::select($sql,null,null,MDB2_FETCHMODE_ASSOC);

		$this->log->info("getList_nurse_manager() END");
		return $data;

	}

}
