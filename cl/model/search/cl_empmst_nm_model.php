<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_empmst_nm_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_empmst_nm_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

    /**
     * getEvaluationList 評価表の内容取得
     *
     * @param $emp_id    職員ID
     * @return           評価内容配列
     */
    function getEmpmstName($emp_id){

        $this->log->info(__FUNCTION__." START");
        $this->log->debug("emp_id：".$emp_id);

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	emp_lt_nm || emp_ft_nm as emp_fl_nm ";
		$sql .= "  ";
		$sql .= " FROM ";
		$sql .= " 	 empmst ";
		$sql .= " WHERE ";
		$sql .= " 	emp_id = :emp_id ";


		/* カラムタイプ */
		//$type = array('text');
		//$param['emp_id'] = $emp_id;

		$data=parent::select($sql,array("text"),array("emp_id"=>$emp_id),MDB2_FETCHMODE_ASSOC);


		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;

	}

	function get_supervisor_class() {

		$this->log->info(__FUNCTION__." START");

		$sql  = "SELECT ";
		$sql .= " class_division ";
		$sql .= "FROM ";
		$sql .= " cl_mst_supervisor_class ";

		$data=parent::select($sql,null,array(),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data[0]["class_division"];
	}

		// 承認者取得　所属長(部署指定無)
	function get_supervisor_apv_class_none($emp_id) {

		$this->log->info(__FUNCTION__." START");

		$sql  = "SELECT ";
		$sql .= " apvemp.emp_id ";
		$sql .= " ,apvemp.emp_lt_nm || ' ' || apvemp.emp_ft_nm AS emp_full_nm ";
		$sql .= " ,st.st_nm ";
		$sql .= "FROM ";
		$sql .= " empmst apvemp ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st ";
		$sql .= " ON ";
		$sql .= "  apvemp.emp_st = st.st_id ";
		$sql .= "WHERE ";
		$sql .= " apvemp.emp_id != :emp_id ";
		$sql .= " AND EXISTS( ";
		$sql .= "  SELECT ";
		$sql .= "   visor.st_id ";
		$sql .= "  FROM ";
		$sql .= "   cl_mst_supervisor_st visor ";
		$sql .= "  WHERE ";
		$sql .= "   apvemp.emp_st = visor.st_id ";
		$sql .= " ) ";
		$sql .= "ORDER BY ";
		$sql .= " apvemp.emp_id ";

		$data=parent::select($sql,array("text"),array("emp_id" => $emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;

	}

	// 承認者取得　所属長(部署指定有)
	function get_supervisor_apv($emp_id,$class_division){

		$this->log->info(__FUNCTION__." START");
		$this->log->debug("emp_id：".$emp_id);
		$this->log->debug("class_division：".$class_division);

		$sql  = "SELECT ";
		$sql .= " empid.emp_id ";
		$sql .= " ,empnm.emp_lt_nm || ' ' || empnm.emp_ft_nm AS emp_full_nm ";
		$sql .= " ,st.st_nm ";
		$sql .= "FROM ";
		$sql .= " ( ";
		$sql .= "  SELECT ";
		$sql .= "   empwk.emp_id ";
		$sql .= "  FROM ";
		$sql .= "   ( ";
		$sql .= "    SELECT ";
		$sql .= "     apvemp.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     empmst aplemp ";
		$sql .= "     INNER JOIN ";
		$sql .= "      empmst apvemp ";
		$sql .= "     ON ";
		$sql .= "      aplemp.emp_class = apvemp.emp_class ";

		if ($class_division >= 2) {
			$sql .= "      AND aplemp.emp_attribute = apvemp.emp_attribute ";
		}

		if ($class_division >= 3) {
			$sql .= "      AND aplemp.emp_dept = apvemp.emp_dept ";
		}

		if ($class_division == 4) {
			$sql .= "      AND ( ";
			$sql .= "       aplemp.emp_room IS NULL ";
			$sql .= "       OR aplemp.emp_room = apvemp.emp_room ";
			$sql .= "      ) ";
		}

		$sql .= "      AND EXISTS( ";
		$sql .= "       SELECT ";
		$sql .= "        st_id ";
		$sql .= "       FROM ";
		$sql .= "        cl_mst_supervisor_st st ";
		$sql .= "       WHERE ";
		$sql .= "        apvemp.emp_st = st.st_id ";
		$sql .= "      ) ";
		$sql .= "    WHERE ";
		$sql .= "     aplemp.emp_id = :emp_id ";
		$sql .= "    UNION ALL ";
		$sql .= "    SELECT ";
		$sql .= "     apvemp.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     empmst aplemp ";
		$sql .= "     INNER JOIN ";
		$sql .= "      concurrent aplcon ";
		$sql .= "     ON ";
		$sql .= "      aplemp.emp_id = aplcon.emp_id ";
		$sql .= "     INNER JOIN ";
		$sql .= "      empmst apvemp ";
		$sql .= "     ON ";
		$sql .= "      aplcon.emp_class = apvemp.emp_class ";

		if ($class_division >= 2) {
			$sql .= "      AND aplcon.emp_attribute = apvemp.emp_attribute ";
		}

		if ($class_division >= 3) {
			$sql .= "      AND aplcon.emp_dept = apvemp.emp_dept ";
		}

		if ($class_division == 4) {
			$sql .= "      AND ( ";
			$sql .= "       aplcon.emp_room IS NULL ";
			$sql .= "       OR aplcon.emp_room = apvemp.emp_room ";
			$sql .= "      ) ";
		}

		$sql .= "      AND EXISTS( ";
		$sql .= "       SELECT ";
		$sql .= "        st_id ";
		$sql .= "       FROM ";
		$sql .= "        cl_mst_supervisor_st st ";
		$sql .= "       WHERE ";
		$sql .= "        apvemp.emp_st = st.st_id ";
		$sql .= "      ) ";
		$sql .= "    WHERE ";
		$sql .= "     aplemp.emp_id = :emp_id ";
		$sql .= "    UNION ALL ";
		$sql .= "    SELECT ";
		$sql .= "     apvemp.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     empmst aplemp ";
		$sql .= "     INNER JOIN ";
		$sql .= "      concurrent apvcon ";
		$sql .= "     ON ";
		$sql .= "      aplemp.emp_class = apvcon.emp_class ";

		if ($class_division >= 2) {
			$sql .= "      AND aplemp.emp_attribute = apvcon.emp_attribute ";
		}

		if ($class_division >= 3) {
				$sql .= "      AND aplemp.emp_dept = apvcon.emp_dept ";
		}

		if ($class_division == 4) {
				$sql .= "      AND ( ";
				$sql .= "       aplemp.emp_room IS NULL ";
				$sql .= "       OR aplemp.emp_room = apvcon.emp_room ";
				$sql .= "      ) ";
		}

		$sql .= "      AND EXISTS( ";
		$sql .= "       SELECT ";
		$sql .= "        st_id ";
		$sql .= "       FROM ";
		$sql .= "        cl_mst_supervisor_st st ";
		$sql .= "       WHERE ";
		$sql .= "        apvcon.emp_st = st.st_id ";
		$sql .= "      ) ";
		$sql .= "     INNER JOIN ";
		$sql .= "      empmst apvemp ";
		$sql .= "     ON ";
		$sql .= "      apvcon.emp_id = apvemp.emp_id ";
		$sql .= "    WHERE ";
		$sql .= "     aplemp.emp_id = :emp_id ";
		$sql .= "    UNION ALL ";
		$sql .= "    SELECT ";
		$sql .= "     apvemp.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     empmst aplemp ";
		$sql .= "     INNER JOIN ";
		$sql .= "      concurrent aplcon ";
		$sql .= "     ON ";
		$sql .= "      aplemp.emp_id = aplcon.emp_id ";
		$sql .= "     INNER JOIN ";
		$sql .= "      concurrent apvcon ";
		$sql .= "     ON ";
		$sql .= "      aplcon.emp_class = apvcon.emp_class ";

		if ($class_division >= 2) {
			$sql .= "      AND aplcon.emp_attribute = apvcon.emp_attribute ";
		}

		if ($class_division >= 3) {
			$sql .= "      AND aplcon.emp_dept = apvcon.emp_dept ";
		}

		if ($class_division == 4) {
			$sql .= "      AND ( ";
			$sql .= "       aplcon.emp_room IS NULL ";
			$sql .= "       OR aplcon.emp_room = apvcon.emp_room ";
			$sql .= "      ) ";
		}

		$sql .= "      AND EXISTS( ";
		$sql .= "       SELECT ";
		$sql .= "        st_id ";
		$sql .= "       FROM ";
		$sql .= "        cl_mst_supervisor_st st ";
		$sql .= "       WHERE ";
		$sql .= "        apvcon.emp_st = st.st_id ";
		$sql .= "      ) ";
		$sql .= "     INNER JOIN ";
		$sql .= "      empmst apvemp ";
		$sql .= "     ON ";
		$sql .= "      apvcon.emp_id = apvemp.emp_id ";
		$sql .= "    WHERE ";
		$sql .= "     aplemp.emp_id = :emp_id ";
		$sql .= "   ) empwk ";
		$sql .= "  WHERE ";
		$sql .= "   empwk.emp_id != :emp_id ";
		$sql .= "  GROUP BY ";
		$sql .= "   empwk.emp_id ";
		$sql .= " ) empid ";
		$sql .= " INNER JOIN ";
		$sql .= "  empmst empnm ";
		$sql .= " ON ";
		$sql .= "  empid.emp_id = empnm.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st ";
		$sql .= " ON ";
		$sql .= "  empnm.emp_st = st.st_id ";
		$sql .= "ORDER BY ";
		$sql .= " empid.emp_id ";

		$data=parent::select($sql,array("text"),array("emp_id" => $emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;

	}

	// 2012/09/24 Yamagawa add(s)
	/**
	 * get_applicant_name_affiliation
	 * @param type $emp_id
	 */
	function get_applicant_name_affiliation($id) {

		$this->log->debug("get_applicant_name_affiliation() START");

		$sql = '';
		$sql .= " SELECT ";
		$sql .= "  emp.emp_lt_nm ";
		$sql .= "  ,emp.emp_ft_nm ";
		$sql .= "  ,class.class_nm ";
		$sql .= "  ,atrb.atrb_nm ";
		$sql .= "  ,dept.dept_nm ";
		$sql .= "  ,room.room_nm ";
		$sql .= " FROM ";
		$sql .= "  empmst emp ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classmst class ";
		$sql .= "  ON ";
		$sql .= "   class.class_id = emp.emp_class ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   atrbmst atrb ";
		$sql .= "  ON ";
		$sql .= "   atrb.atrb_id = emp.emp_attribute ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   deptmst dept ";
		$sql .= "  ON ";
		$sql .= "   dept.dept_id = emp.emp_dept ";
		$sql .= "  LEFT JOIN ";
		$sql .= "   classroom room ";
		$sql .= "  ON ";
		$sql .= "   room.room_id = emp.emp_room ";
		$sql .= " WHERE ";
		$sql .= "  emp.emp_id = :id ";

		$data=parent::select($sql,array("text"),array("id"=>$id),MDB2_FETCHMODE_ASSOC);
		$this->log->debug("get_applicant_name_affiliation() END");
		return $data[0];

	}
	// 2012/09/24 Yamagawa add(e)

}
