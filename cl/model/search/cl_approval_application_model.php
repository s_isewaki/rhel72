<?php
/**
 * 決裁申請の院外研修申請データ取得用モデル
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_approval_application_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_training_progress_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	 * 院外研修
	 */
	function get_outside_seminar($seminar_apply_id)
	{
		$this->log->info("get_outside_seminar() START");

		//SQL作成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= "	seminar_apply_id ";
		$sql .= "	, seminar_id ";
		$sql .= "	, apply_id ";
		$sql .= "	, emp_id ";
		$sql .= "	, seminar_name ";
		$sql .= "	, report_date ";
		$sql .= "	, from_date ";
		$sql .= "	, to_date ";
		$sql .= "	, from_time ";
		$sql .= "	, to_time ";
		$sql .= "	, open_place ";
		$sql .= "	, teacher_name ";
		$sql .= "	, trip_division ";
		$sql .= "	, participation_division ";
		$sql .= "	, expense_division ";
		$sql .= "	, report_division ";
		$sql .= "FROM ";
		$sql .= "	cl_apl_outside_seminar ";
		$sql .= "WHERE ";
		$sql .= "	seminar_apply_id = :seminar_apply_id ";
		$sql .= "	AND delete_flg = 0 ";

		$data=parent::select($sql,array("text"),array("seminar_apply_id"=>$seminar_apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_outside_seminar() END count(data):".count($data));
		return $data[0];
	}
}
