<?php
/**
 * 研修受講進捗の一覧データ取得用モデル
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_training_progress_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_training_progress_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	 * ラダーレベル
	 */
	function get_level_list($emp_id)
	{
		$this->log->info("get_level_list() START");

		//SQL作成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " emp.emp_lt_nm ";
		$sql .= " ,emp.emp_ft_nm ";
		$sql .= " ,class.class_nm ";
		$sql .= " ,atrb.atrb_nm ";
		$sql .= " ,dept.dept_nm ";
		$sql .= " ,room.room_nm ";
		$sql .= " ,pro.get_level1_date ";
		$sql .= " ,pro.get_level2_date ";
		$sql .= " ,pro.get_level3_date ";
		$sql .= " ,pro.get_level4_date ";
		$sql .= " ,pro.get_level5_date ";
		$sql .= "FROM ";
		$sql .= " empmst emp ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_personal_profile pro ";
		$sql .= " ON ";
		$sql .= "  emp.emp_id = pro.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  classmst class ";
		$sql .= " ON ";
		$sql .= "  emp.emp_class = class.class_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  atrbmst atrb ";
		$sql .= " ON ";
		$sql .= "  emp.emp_attribute = atrb.atrb_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  deptmst dept ";
		$sql .= " ON ";
		$sql .= "  emp.emp_dept = dept.dept_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  classroom room ";
		$sql .= " ON ";
		$sql .= "  emp.emp_room = room.room_id ";
		$sql .= "WHERE ";
		$sql .= " emp.emp_id = :emp_id ";

		$data=parent::select($sql,array("text"),array("emp_id"=>$emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_level_list() END count(data):".count($data));
		return $data[0];
	}

	/**
	* 院内研修一覧表示
	*/
	function get_inside_training_list($param)
	{
		$this->log->info("get_inside_training_list() START");

		$emp_id = $param['emp_id'];
		$year = $param['year'];

		//SQL作成
		$sql = "";

		//==================================================
		//検索１：申請中の日程一覧を取得
		//==================================================
		$sql .= "SELECT ";
		$sql .= " apl.training_apply_id AS sort_id ";
		$sql .= " ,0 AS sort_id2 ";
		$sql .= " ,sch.training_time AS sort_id3 ";
		$sql .= " ,apl.inside_training_id AS no_id ";
		$sql .= " ,tra.training_name ";
		$sql .= " ,CAST(NULL AS DATE) AS inside_training_date ";
		$sql .= " ,CASE ";
		$sql .= "   WHEN tra.max_training_time < sch.training_time THEN CAST(NULL AS VARCHAR) ";
		$sql .= "   WHEN sch.final_flg = 1 THEN '最終回' ";
		$sql .= "   ELSE '第' || sch.training_time || '回' ";
		$sql .= "  END AS count ";
//		$sql .= " ,sch.plan_no AS count_index ";
//		$sql .= " ,schcnt.cnt AS count_all ";
		$sql .= " ,CAST(NULL AS DATE) AS plan_date ";
		$sql .= " ,CAST(NULL AS DATE) AS report_date ";
		$sql .= "FROM ";
		$sql .= " cl_apl_inside_training_apply apl ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_mst_inside_training tra ";
		$sql .= " ON ";
		$sql .= "  apl.inside_training_id = tra.training_id ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_mst_inside_training_schedule sch ";
		$sql .= " ON ";
		$sql .= "  apl.plan_id LIKE '%' || sch.plan_id || '%' ";
		//$sql .= "  AND 0 = sch.delete_flg ";
		//$sql .= "  AND $year = sch.year ";
/*
		$sql .= " LEFT JOIN ";
		$sql .= "  (SELECT ";
		$sql .= "    sch.training_id ";
		$sql .= "    ,count(sch.plan_id) AS cnt ";
		$sql .= "   FROM ";
		$sql .= "    cl_mst_inside_training_schedule sch ";
		$sql .= "   GROUP BY ";
		$sql .= "    sch.training_id) schcnt ";
		$sql .= " ON ";
		$sql .= "  apl.inside_training_id = schcnt.training_id ";
*/
		$sql .= "WHERE ";
		$sql .= " apl.emp_id = '$emp_id' ";
		$sql .= " AND apl.delete_flg = 0 ";
		$sql .= " AND CASE ";
		$sql .= "   WHEN ";
		$sql .= "    CAST(to_char(apl.inside_training_date,'MM') AS INTEGER) BETWEEN 4 AND 12 ";
		$sql .= "   THEN ";
		$sql .= "    CAST(to_char(apl.inside_training_date,'YYYY') AS INTEGER) ";
		$sql .= "   ELSE ";
		$sql .= "    CAST(to_char(apl.inside_training_date,'YYYY') AS INTEGER) - 1 ";
		$sql .= "  END = $year ";
		$sql .= " AND NOT EXISTS( ";
		$sql .= "  SELECT ";
		$sql .= "   applied.training_apply_id ";
		$sql .= "  FROM ";
		$sql .= "   cl_applied_inside_training applied ";
		$sql .= "  WHERE ";
		$sql .= "   applied.training_apply_id = apl.first_apply_id ";
		$sql .= " ) ";
		$sql .= " AND EXISTS( ";
		$sql .= "  SELECT";
		$sql .= "   apply.apply_id ";
		$sql .= "  FROM ";
		$sql .= "   cl_apply apply ";
		$sql .= "  WHERE ";
		$sql .= "   apl.apply_id = apply.apply_id ";
		$sql .= "   AND apply.apply_stat IN ('0','1') ";
		$sql .= "   AND apply.draft_flg = FALSE ";
		$sql .= "   AND apply.delete_flg = FALSE ";
		$sql .= " )";


		//==================================================
		//検索２：申請中の課題一覧を取得
		//==================================================
		$sql .= "UNION ALL ";

		$sql .= "SELECT ";
		$sql .= " apl.training_apply_id AS sort_id ";
		$sql .= " ,1 AS sort_id2 ";
		$sql .= " ,theme.theme_division AS sort_id3 ";
		$sql .= " ,CAST(NULL AS VARCHAR) AS no_id ";
		// 2012/05/24 M.Yamagawa upd(s)
		//$sql .= " ,theme.theme_contents AS training_name ";
		$sql .= " ,replace(replace(replace(theme.theme_contents,chr(13)||chr(10),'<br>'),chr(13),'<br>'),chr(10),'<br>') AS training_name ";
		// 2012/05/24 M.Yamagawa upd(e)
		$sql .= " ,CAST(NULL AS DATE) AS inside_training_date ";
		$sql .= " ,CAST(NULL AS VARCHAR) AS count ";
//		$sql .= " ,CAST(NULL AS INTEGER) AS count_index ";
//		$sql .= " ,CAST(NULL AS INTEGER) AS count_all ";
		$sql .= " ,CAST(NULL AS DATE) AS plan_date ";
		$sql .= " ,CAST(NULL AS DATE) AS report_date ";
		$sql .= "FROM ";
		$sql .= " cl_apl_inside_training_apply apl ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_mst_inside_training_theme theme ";
		$sql .= " ON ";
		$sql .= "  apl.inside_training_id = theme.training_id ";
		$sql .= "  and 0 = theme.delete_flg ";
		$sql .= "WHERE ";
		$sql .= " apl.emp_id = '$emp_id' ";
		$sql .= " AND apl.delete_flg = 0 ";
		$sql .= " AND CASE ";
		$sql .= "   WHEN ";
		$sql .= "    CAST(to_char(apl.inside_training_date,'MM') AS INTEGER) BETWEEN 4 AND 12 ";
		$sql .= "   THEN ";
		$sql .= "    CAST(to_char(apl.inside_training_date,'YYYY') AS INTEGER) ";
		$sql .= "   ELSE ";
		$sql .= "    CAST(to_char(apl.inside_training_date,'YYYY') AS INTEGER) - 1 ";
		$sql .= "  END = $year ";
		$sql .= " AND EXISTS( ";
		$sql .= "  SELECT ";
		$sql .= "   sch.plan_id ";
		$sql .= "  FROM ";
		$sql .= "   cl_mst_inside_training_schedule sch ";
		$sql .= "  WHERE ";
		$sql .= "   apl.plan_id LIKE '%' || sch.plan_id || '%' ";
		//$sql .= "   AND 0 = sch.delete_flg ";
		//$sql .= "   AND $year = sch.year ";
		$sql .= " ) ";
		$sql .= " AND NOT EXISTS( ";
		$sql .= "  SELECT ";
		$sql .= "   applied.training_apply_id ";
		$sql .= "  FROM ";
		$sql .= "   cl_applied_inside_training applied ";
		$sql .= "  WHERE ";
		$sql .= "   applied.training_apply_id = apl.first_apply_id ";
		$sql .= " ) ";
		$sql .= " AND EXISTS( ";
		$sql .= "  SELECT";
		$sql .= "   apply.apply_id ";
		$sql .= "  FROM ";
		$sql .= "   cl_apply apply ";
		$sql .= "  WHERE ";
		$sql .= "   apl.apply_id = apply.apply_id ";
		$sql .= "   AND apply.apply_stat IN ('0','1') ";
		$sql .= "   AND apply.draft_flg = FALSE ";
		$sql .= "   AND apply.delete_flg = FALSE ";
		$sql .= " )";


		//==================================================
		//検索３：申請済みの日程一覧を取得
		//==================================================
		$sql .= "UNION ALL ";


		$sql .= "SELECT ";
		$sql .= " applied.training_apply_id AS sort_id ";
		$sql .= " ,0 AS sort_id2 ";
		$sql .= " ,sch.training_time AS sort_id3 ";
		$sql .= " ,applied.inside_training_id AS no_id ";
		$sql .= " ,tra.training_name ";
		$sql .= " ,applied.inside_training_date ";
		$sql .= " ,CASE ";
		$sql .= "   WHEN tra.max_training_time < sch.training_time THEN CAST(NULL AS VARCHAR) ";
		$sql .= "   WHEN sch.final_flg = 1 THEN '最終回' ";
		$sql .= "   ELSE '第' || sch.training_time || '回' ";
		$sql .= "  END AS count ";
//		$sql .= " ,sch.plan_no AS count_index ";
//		$sql .= " ,schcnt.cnt AS count_all ";
		$sql .= " ,sch.plan_date ";
		$sql .= " ,CASE apl.apply_stat ";
		$sql .= "   WHEN '1' THEN to_date(substr(apl.apply_date,1,8),'yyyymmdd') ";
		$sql .= "   ELSE CAST(NULL AS DATE) ";
		$sql .= "  END AS report_date ";
		$sql .= "FROM ";
		$sql .= " cl_applied_inside_training applied ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_mst_inside_training tra ";
		$sql .= " ON ";
		$sql .= "  applied.inside_training_id = tra.training_id ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_mst_inside_training_schedule sch ";
		$sql .= " ON ";
		$sql .= "  applied.plan_id LIKE '%' || sch.plan_id || '%' ";
		//$sql .= "  AND 0 = sch.delete_flg ";
		//$sql .= "  AND $year = sch.year ";
/*
		$sql .= " LEFT JOIN ";
		$sql .= "  (SELECT ";
		$sql .= "    sch.training_id ";
		$sql .= "    ,count(sch.plan_id) AS cnt ";
		$sql .= "   FROM ";
		$sql .= "    cl_mst_inside_training_schedule sch ";
		$sql .= "   GROUP BY ";
		$sql .= "    sch.training_id) schcnt ";
		$sql .= " ON ";
		$sql .= "  applied.inside_training_id = schcnt.training_id ";
*/
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_apl_inside_training_report repo ";
		$sql .= " ON ";
		$sql .= "  applied.emp_id = repo.emp_id ";
		$sql .= "  AND applied.training_apply_id = repo.training_apply_id ";
		$sql .= "  AND applied.inside_training_id = repo.training_id ";
		$sql .= "  AND sch.plan_id = repo.plan_id ";
		$sql .= "  AND 0 = repo.delete_flg ";
		$sql .= "  AND EXISTS( ";
		$sql .= "   SELECT ";
		$sql .= "    apl.apply_id ";
		$sql .= "   FROM ";
		$sql .= "    cl_apply apl ";
		$sql .= "   WHERE ";
		$sql .= "    repo.apply_id = apl.apply_id ";
		$sql .= "    AND FALSE = apl.delete_flg ";
		$sql .= "    AND '1' = apl.apply_stat ";
		$sql .= "  ) ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_apply apl ";
		$sql .= " ON ";
		$sql .= "  repo.apply_id = apl.apply_id ";
		$sql .= "  AND FALSE = apl.delete_flg ";
		$sql .= "  AND '1' = apl.apply_stat ";
		$sql .= "WHERE ";
		$sql .= " applied.emp_id = '$emp_id' ";
		$sql .= " AND applied.delete_flg = 0 ";
		$sql .= " AND CASE ";
		$sql .= "   WHEN ";
		$sql .= "    CAST(to_char(applied.inside_training_date,'MM') AS INTEGER) BETWEEN 4 AND 12 ";
		$sql .= "   THEN ";
		$sql .= "    CAST(to_char(applied.inside_training_date,'YYYY') AS INTEGER) ";
		$sql .= "   ELSE ";
		$sql .= "    CAST(to_char(applied.inside_training_date,'YYYY') AS INTEGER) - 1 ";
		$sql .= "  END = $year ";


		//==================================================
		//検索４：申請済みの課題一覧を取得
		//==================================================
		$sql .= "UNION ALL ";


		$sql .= "SELECT ";
		$sql .= " applied.training_apply_id AS sort_id ";
		$sql .= " ,1 AS sort_id2 ";
		$sql .= " ,theme.theme_division AS sort_id3 ";
		$sql .= " ,CAST(NULL AS VARCHAR) AS no_id ";
		// 2012/05/24 M.Yamagawa upd(s)
		//$sql .= " ,theme.theme_contents ";
		$sql .= " ,replace(replace(replace(theme.theme_contents,chr(13)||chr(10),'<br>'),chr(13),'<br>'),chr(10),'<br>') AS training_name ";
		// 2012/05/24 M.Yamagawa upd(e)
		$sql .= " ,CAST(NULL AS DATE) AS inside_training_date ";
		$sql .= " ,CAST(NULL AS VARCHAR) AS count ";
//		$sql .= " ,CAST(NULL AS INTEGER) AS count_index ";
//		$sql .= " ,CAST(NULL AS INTEGER) AS count_all ";
		$sql .= " ,CAST(NULL AS DATE) AS plan_date ";
		$sql .= " ,CASE apl.apply_stat ";
		$sql .= "   WHEN '1' THEN present.present_date ";
		$sql .= "   ELSE CAST(NULL AS DATE) ";
		$sql .= "  END AS report_date ";
		$sql .= "FROM ";
		$sql .= " cl_applied_inside_training applied ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_mst_inside_training_theme theme ";
		$sql .= " ON ";
		$sql .= "  applied.inside_training_id = theme.training_id ";
		$sql .= "  AND 0 = theme.delete_flg ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_apl_inside_training_theme_present present ";
		$sql .= " ON ";
		$sql .= "  applied.emp_id = present.emp_id ";
		$sql .= "  AND applied.training_apply_id = present.training_apply_id ";
		$sql .= "  AND applied.inside_training_id = present.training_id ";
		$sql .= "  AND theme.theme_id = present.theme_id ";
		$sql .= "  AND 0 = present.delete_flg ";
		$sql .= "  AND EXISTS( ";
		$sql .= "   SELECT ";
		$sql .= "    apl.apply_id ";
		$sql .= "   FROM ";
		$sql .= "    cl_apply apl ";
		$sql .= "   WHERE ";
		$sql .= "    present.apply_id = apl.apply_id ";
		$sql .= "    AND FALSE = apl.delete_flg ";
		$sql .= "    AND '1' = apl.apply_stat ";
		$sql .= "  ) ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_apply apl ";
		$sql .= " ON ";
		$sql .= "  present.apply_id = apl.apply_id ";
		$sql .= "  AND FALSE = apl.delete_flg ";
		$sql .= "  AND '1' = apl.apply_stat ";
		$sql .= "WHERE ";
		$sql .= " applied.emp_id = '$emp_id' ";
		$sql .= " AND applied.delete_flg = 0 ";
		$sql .= " AND CASE ";
		$sql .= "   WHEN ";
		$sql .= "    CAST(to_char(applied.inside_training_date,'MM') AS INTEGER) BETWEEN 4 AND 12 ";
		$sql .= "   THEN ";
		$sql .= "    CAST(to_char(applied.inside_training_date,'YYYY') AS INTEGER) ";
		$sql .= "   ELSE ";
		$sql .= "    CAST(to_char(applied.inside_training_date,'YYYY') AS INTEGER) - 1 ";
		$sql .= "  END = $year ";
		$sql .= " AND EXISTS( ";
		$sql .= "  SELECT ";
		$sql .= "   sch.plan_id ";
		$sql .= "  FROM ";
		$sql .= "   cl_mst_inside_training_schedule sch ";
		$sql .= "  WHERE ";
		$sql .= "   applied.plan_id LIKE '%' || sch.plan_id || '%' ";
		//$sql .= "   AND 0 = sch.delete_flg ";
		//$sql .= "   AND $year = sch.year ";
		$sql .= " ) ";


		$sql .= "ORDER BY ";
		$sql .= " sort_id ";
		$sql .= " ,sort_id2 ";
		$sql .= " ,sort_id3 ";

		//$data=parent::select($sql,array("text","integer"),$param,MDB2_FETCHMODE_ASSOC);
		$data=parent::select($sql,null,null,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_inside_training_list() END count(data):".count($data));
		return $data;
	}

	/**
	 * 院外研修一覧
	 */
	function get_outside_training_list($param)
	{
		$this->log->info("get_outside_training_list() START");

		//SQL作成
		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " sem.seminar_apply_id ";
		$sql .= " ,sem.seminar_name ";
		$sql .= " ,apl.apply_date ";
		$sql .= " ,CASE sem.expense_division ";
		$sql .= "   WHEN 1 THEN '出張扱い' ";
		$sql .= "   WHEN 2 THEN '自己研修' ";
		$sql .= "   ELSE NULL ";
		$sql .= "  END AS expense ";
		$sql .= " ,sem.from_date ";
		$sql .= " ,sem.to_date ";
		$sql .= " ,sem.report_date ";
		$sql .= " ,sem.trip_division ";
		$sql .= " ,apl.apply_stat ";
		$sql .= " ,wmst.short_wkfw_name ";
		$sql .= "FROM ";
		$sql .= " cl_apl_outside_seminar sem ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_apply apl ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = sem.apply_id ";
		$sql .= "  AND apl.apply_stat IN ('0','1') ";
		$sql .= "  AND apl.draft_flg = FALSE ";
		$sql .= "  AND apl.delete_flg = FALSE ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_wkfwmst wmst ";
		$sql .= " ON ";
		$sql .= "  apl.wkfw_id = wmst.wkfw_id ";
		$sql .= "WHERE ";
		$sql .= " sem.emp_id = :emp_id ";
		$sql .= " AND sem.delete_flg = 0 ";
		/*
		$sql .= " AND CASE ";
		$sql .= "   WHEN ";
		$sql .= "    CAST(to_char(sem.from_date,'MM') AS INTEGER) BETWEEN 4 AND 12 ";
		$sql .= "   THEN ";
		$sql .= "    to_char(sem.from_date,'YYYY') ";
		$sql .= "   ELSE ";
		$sql .= "    CAST(CAST(to_char(sem.from_date,'YYYY') AS INTEGER) - 1 AS VARCHAR) ";
		$sql .= "  END = :year ";
		*/
		$sql .= " AND CASE ";
		$sql .= "   WHEN ";
		$sql .= "    CAST(to_char(to_date(apl.apply_date,'YYYYMMDD'),'MM') AS INTEGER) BETWEEN 4 AND 12 ";
		$sql .= "   THEN ";
		$sql .= "    to_char(to_date(apl.apply_date,'YYYYMMDD'),'YYYY') ";
		$sql .= "   ELSE ";
		$sql .= "    CAST(CAST(to_char(to_date(apl.apply_date,'YYYYMMDD'),'YYYY') AS INTEGER) - 1 AS VARCHAR) ";
		$sql .= "  END = :year ";
		$sql .= "ORDER BY ";
		$sql .= " sem.seminar_apply_id ";

		$data=parent::select($sql,array("text","text"),$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_outside_training_list() END count(data):".count($data));
		return $data;
	}

}
