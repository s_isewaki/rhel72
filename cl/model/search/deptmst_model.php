<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class deptmst_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function deptmst_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	* 課・科マスタ取得
	*/
	function getList(){
		$this->log->info("getList() START");

		//SQL作成
		$sql = "";
		$sql .= "SELECT";
		$sql .= " dept_id,";
		$sql .= " atrb_id,";
		$sql .= " dept_nm ";
		$sql .= "FROM";
		$sql .= " deptmst ";
		$sql .= "WHERE";
		$sql .= " dept_del_flg = 'f' ";
		$sql .= "ORDER BY";
		$sql .= " order_no";

		$data=parent::select($sql,null,array(),MDB2_FETCHMODE_ASSOC);

		$this->log->info("getList() END");
		return $data;
	}

	/**
	* 課・科マスタ取得（部署指定有）
	*/
	function getList_add_conditions_atrb($atrb_id){
		$this->log->info("getList_add_conditions_atrb() START");

		//SQL作成
		$sql = "";
		$sql .= "SELECT";
		$sql .= " dept_id,";
		$sql .= " atrb_id,";
		$sql .= " dept_nm ";
		$sql .= "FROM";
		$sql .= " deptmst ";
		$sql .= "WHERE";
		$sql .= " atrb_id = :atrb_id ";
		$sql .= " AND dept_del_flg = 'f' ";
		$sql .= "ORDER BY";
		$sql .= " order_no";

		$data=parent::select($sql,array("integer"),array("atrb_id" => $atrb_id),MDB2_FETCHMODE_ASSOC);

		$this->log->info("getList_add_conditions_atrb() END");
		return $data;
	}

	/**
	* 所属課・科マスタ取得（兼務込）
	*/
	function getList_add_conditions_emp_id($class_division ,$emp_id){
		$this->log->info("getList_add_conditions_emp_id() START");

		//SQL作成
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " dept.dept_id ";
		$sql .= " ,dept.atrb_id ";
		$sql .= " ,dept.dept_nm ";
		$sql .= "FROM ";
		$sql .= " deptmst dept ";

		if ($class_division == 1) {
			$sql .= " INNER JOIN ";
			$sql .= "  atrbmst atrb ";
			$sql .= " ON ";
			$sql .= "  atrb.atrb_id = dept.atrb_id ";
			$sql .= "  AND atrb.atrb_del_flg = 'f' ";
		}

		$sql .= "WHERE ";
		$sql .= " dept.dept_del_flg = 'f' ";
		$sql .= " AND ( ";
		$sql .= "  EXISTS ( ";
		$sql .= "   SELECT ";
		$sql .= "    emp.emp_id ";
		$sql .= "   FROM ";
		$sql .= "    empmst emp ";
		$sql .= "   WHERE ";
		$sql .= "    emp.emp_id = :emp_id ";

		switch($class_division){
			case 1:
				$sql .= "    AND emp.emp_class = atrb.class_id ";
				break;
			case 2:
				$sql .= "    AND emp.emp_attribute = dept.atrb_id ";
				break;
			default:
				$sql .= "    AND emp.emp_dept = dept.dept_id ";
				break;
		}

		$sql .= "	) ";
		$sql .= "  OR EXISTS ( ";
		$sql .= "   SELECT ";
		$sql .= "    con.emp_id ";
		$sql .= "   FROM ";
		$sql .= "    concurrent con ";
		$sql .= "   WHERE ";
		$sql .= "    con.emp_id = :emp_id ";

		switch($class_division){
			case 1:
				$sql .= "    AND con.emp_class = atrb.class_id ";
				break;
			case 2:
				$sql .= "    AND con.emp_attribute = dept.atrb_id ";
				break;
			default:
				$sql .= "    AND con.emp_dept = dept.dept_id ";
				break;
		}

		$sql .= "  ) ";
		$sql .= " ) ";

		$data=parent::select($sql,array('text'),array('emp_id' => $emp_id),MDB2_FETCHMODE_ASSOC);

		$this->log->info("getList_add_conditions_emp_id() END");
		return $data;
	}
}
