<?php
/**
 * 研修講座一覧画面の一覧データ取得用モデル
 *
 *
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_levelup_attach_file_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_levelup_attach_file_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	* 院内研修申請一覧に必要なデータを取得します。
	*
	* @param $emp_id 研修の申請者
	*/
	function get_levelup_attach_file($apply_id)
	{
		$this->log->debug("get_levelup_attach_file() START");

		//SQL作成
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " file.levelup_apply_attached_file_id ";
		$sql .= " ,file.disp_order ";
		$sql .= " ,file.file_division ";
		$sql .= " ,file.file_name ";
		$sql .= "FROM ";
		$sql .= " cl_apl_levelup_apply lvapp ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_apl_levelup_apply_attached_file file ";
		$sql .= " ON ";
		$sql .= "  file.levelup_apply_id = lvapp.levelup_apply_id ";
		//2012/05/07 K.Fujii upd(s)
		//$sql .= "  AND file.delete_flg = FALSE ";
		$sql .= "  AND file.delete_flg = 0 ";
		//2012/05/07 K.Fujii upd(e)
		$sql .= "WHERE ";
		$sql .= " lvapp.apply_id = :apply_id ";
		$sql .= "ORDER BY ";
		$sql .= " file.disp_order ";

		$data=parent::select($sql,array("text"),array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("get_levelup_attach_file() END data:".print_r($data,true));
		$this->log->debug("get_levelup_attach_file() END count(data):".count($data));
		return $data;

	}

}
