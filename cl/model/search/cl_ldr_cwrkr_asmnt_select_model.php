
<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_ldr_cwrkr_asmnt_select_model extends cl_common_model{


	var $con;
	var $log;
	var $user;

	/**
	 * Constractor コンストラクタ
	 */
	function cl_ldr_cwrkr_asmnt_select_model(&$p_con,$p_login_user) {
		parent::cl_common_model(&$p_con,$p_user);
		$this->con = &$p_con;
		$this->user=$p_login_user;
		$this->log = new cl_common_log_class(basename(__FILE__,'.php'));
	}

	/**
	 * getRecordByLevelupWkfwApplyId レベルアップ申請の申請IDによるデータ取得
	 * @param $apply_id
	 */
	function getAutoApplyManageRecordByLevelupWkfwApplyId($apply_id) {

		$this->log->info(__FUNCTION__." START");

		/* SQL */
		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	cwrkr_asmnt_auto_apply_manage_id ";
		$sql .= " 	, levelup_wkfw_apply_id ";
		$sql .= " 	, levelup_apply_apply_id ";
		$sql .= " 	, level ";
		$sql .= " 	, cnddt_emp_id ";
		$sql .= " 	, cnddt_emp_class ";
		$sql .= " 	, cnddt_emp_attr ";
		$sql .= " 	, cnddt_emp_dept ";
		$sql .= " 	, cnddt_emp_room ";
		$sql .= " 	, colleague_division ";
		$sql .= " 	, cwrkr_emp_id ";
		$sql .= " 	, cwrkr_emp_class ";
		$sql .= " 	, cwrkr_emp_attr ";
		$sql .= " 	, cwrkr_emp_dept ";
		$sql .= " 	, cwrkr_emp_room ";
		$sql .= " 	, from_emp_id ";
		$sql .= " 	, from_emp_class ";
		$sql .= " 	, from_emp_attr ";
		$sql .= " 	, from_emp_dept ";
		$sql .= " 	, from_emp_room ";
		$sql .= " 	, cwrkr_asmnt_wkfw_apply_id ";
		$sql .= " 	, cwrkr_asmnt_apply_apply_id ";
		$sql .= " 	, proc_flg ";
		$sql .= " FROM ";
		$sql .= " 	cl_ldr_cwrkr_asmnt_auto_apply_manage ";
		$sql .= " WHERE ";
		$sql .= " 	levelup_wkfw_apply_id = :levelup_wkfw_apply_id ";

		/**
		 * 実行
		 */
		$data=parent::select($sql,array("text"),array("levelup_wkfw_apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->info(__FUNCTION__." END");

		return $data;
	}

	/**
	 * getRecordByLevelupWkfwApplyId レベルアップ申請IDによるデータ取得
	 * @param $levelup_apply_id
	 */
	function getAutoApplyManageRecordByLevelupApplyId($levelup_apply_id) {

		$this->log->info(__FUNCTION__." START");

		/* SQL */
		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	cwrkr_asmnt_auto_apply_manage_id ";
		$sql .= " 	, levelup_wkfw_apply_id ";
		$sql .= " 	, levelup_apply_apply_id ";
		$sql .= " 	, level ";
		$sql .= " 	, cnddt_emp_id ";
		$sql .= " 	, cnddt_emp_class ";
		$sql .= " 	, cnddt_emp_attr ";
		$sql .= " 	, cnddt_emp_dept ";
		$sql .= " 	, cnddt_emp_room ";
		$sql .= " 	, colleague_division ";
		$sql .= " 	, cwrkr_emp_id ";
		$sql .= " 	, cwrkr_emp_class ";
		$sql .= " 	, cwrkr_emp_attr ";
		$sql .= " 	, cwrkr_emp_dept ";
		$sql .= " 	, cwrkr_emp_room ";
		$sql .= " 	, from_emp_id ";
		$sql .= " 	, from_emp_class ";
		$sql .= " 	, from_emp_attr ";
		$sql .= " 	, from_emp_dept ";
		$sql .= " 	, from_emp_room ";
		$sql .= " 	, cwrkr_asmnt_wkfw_apply_id ";
		$sql .= " 	, cwrkr_asmnt_apply_apply_id ";
		$sql .= " 	, proc_flg ";
		$sql .= " FROM ";
		$sql .= " 	cl_ldr_cwrkr_asmnt_auto_apply_manage ";
		$sql .= " WHERE ";
		$sql .= " 	levelup_apply_apply_id = :levelup_apply_apply_id ";

		/**
		 * 実行
		 */
		$data=parent::select($sql,array("text"),array("levelup_apply_apply_id"=>$levelup_apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->info(__FUNCTION__." END");

		return $data;
	}

	/**
	 * getRecordByLevelupWkfwApplyId 同僚評価の申請IDによるデータ取得
	 * @param $apply_id
	 */
	function getAcknowledgerByApplyId($apply_id) {

		$this->log->info(__FUNCTION__." START");

		/* SQL */
		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	cwrkr_asmnt_auto_apply_manage_id ";
		$sql .= " 	, cwrkr_asmnt_wkfw_apply_id ";
		$sql .= " 	, cwrkr_asmnt_apply_apply_id ";
		$sql .= " 	, acknowledger_emp_id ";
		$sql .= " 	, acknowledger_emp_class ";
		$sql .= " 	, acknowledger_emp_attr ";
		$sql .= " 	, acknowledger_emp_dept ";
		$sql .= " 	, acknowledger_emp_room ";
		$sql .= " 	, acknowledger_emp_st ";
		$sql .= " 	, st_div ";
		$sql .= " 	, apv_order ";
		$sql .= " 	, apv_sub_order ";
		$sql .= " FROM ";
		$sql .= " 	cl_ldr_cwrkr_assmnt_acknowledger_manage ";
		$sql .= " WHERE ";
		$sql .= " 	cwrkr_asmnt_wkfw_apply_id = :cwrkr_asmnt_wkfw_apply_id ";
		$sql .= " 	AND apv_order <> 1 ";

		/**
		 * 実行
		 */
		$data=parent::select($sql,array("text"),array("cwrkr_asmnt_wkfw_apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->info(__FUNCTION__." END");

		return $data;
	}

	/**
	 * logical_delete 同僚評価申請ID登録
	 * @param type $param パラメータ
	 */
	function cwrkr_asmnt_wkfw_apply_id_upd($param) {

	    $this->log->info(__FUNCTION__." START");

		$param["update_user"] = $this->user;

	    /* SQL */
	    $sql = "UPDATE cl_ldr_cwrkr_assmnt_acknowledger_manage SET	cwrkr_asmnt_wkfw_apply_id = :cwrkr_asmnt_wkfw_apply_id , cwrkr_asmnt_apply_apply_id = :cwrkr_asmnt_apply_apply_id, update_date = current_timestamp , update_user = :update_user WHERE cwrkr_asmnt_auto_apply_manage_id = :cwrkr_asmnt_auto_apply_manage_id";

	    /* カラムタイプ */
	    $type = array('text','text','text','text');

	    /**
	     * 実行
	     */
	    $rtn=parent::execute($sql,$type,$param);

	    $this->log->info(__FUNCTION__." END");

	    return $rtn;
	}


}
