<?php
require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_levelup_evaluation_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_levelup_evaluation_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	 * getEvaluationList_forNewEval 評価表の内容取得
	 *
	 * @param $levelup_apply_id    レベルアップ申請ID
	 * @return           評価内容配列
	 */
	function getEvaluationList_forNewEval($levelup_apply_id){

		$this->log->info(__FUNCTION__." START");
		$this->log->debug("levelup_apply_id：".$levelup_apply_id);

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	mst.levelup_apply_id ";
		$sql .= " 	, mst.disp_order ";
		$sql .= " 	, mst.evaluation_id ";
		$sql .= " 	, cat.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order AS cat_order ";
		$sql .= " 	, cat_grp_row.grp_cnt + cat_item_row.cat_item_cnt AS cat_rowspan ";
		$sql .= " 	, grp.group_id ";
		$sql .= " 	, grp.group_name ";
		$sql .= " 	, grp.group_no ";
		$sql .= " 	, grp.disp_order AS grp_order ";
		$sql .= " 	, grprow.item_cnt + 1 AS grp_rowspan ";
		$sql .= " 	, item.item_id ";
		$sql .= " 	, item.item_name ";
		$sql .= " 	, item.disp_order AS item_order ";
		$sql .= " 	, item.must_value ";
		$sql .= " 	, item.eval_disp_1_flg ";
		$sql .= " 	, item.eval_disp_2_flg ";
		$sql .= " 	, item.eval_disp_3_flg ";
		$sql .= " 	, item.eval_disp_4_flg ";
		$sql .= " 	, null AS levelup_evaluation_value_id ";
		$sql .= " 	, null AS eval1 ";
		$sql .= " 	, null AS eval2 ";

		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_apply_evaluation mst ";
		// 2012/08/09 Yamagawa upd(s)
		/*
		$sql .= " 	, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_item item ";
		*/
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	ON ";
		$sql .= " 	 cat.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 	 AND cat.evaluation_id = mst.evaluation_id ";
		$sql .= " 	 AND cat.disp_flg = 1 ";
		$sql .= " 	 AND cat.delete_flg = 0 ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	ON ";
		$sql .= " 	 grp.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	 AND grp.evaluation_id = cat.evaluation_id ";
		$sql .= " 	 AND grp.category_id = cat.category_id ";
		$sql .= " 	 AND grp.disp_flg = 1 ";
		$sql .= " 	 AND grp.delete_flg = 0 ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	ON ";
		$sql .= " 	 item.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 	 AND item.evaluation_id = grp.evaluation_id ";
		$sql .= " 	 AND item.category_id = grp.category_id ";
		$sql .= " 	 AND item.group_id = grp.group_id ";
		$sql .= " 	 AND item.disp_flg = 1 ";
		$sql .= " 	 AND item.delete_flg = 0 ";
		// 2012/08/09 Yamagawa upd(e)

		// 2012/08/09 Yamagawa upd(s)
		//$sql .= " 	,(SELECT ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	(SELECT ";
		// 2012/08/09 Yamagawa upd(e)
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id ";
		$sql .= " 		, count(item_id) AS cat_item_cnt ";
		$sql .= " 	FROM ";
		$sql .= " 		cl_apl_levelup_apply_evaluation mst ";
		// 2012/08/09 Yamagawa upd(s)
		/*
		$sql .= " 		, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	WHERE ";
		$sql .= " 		mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 		AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 		AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 		AND cat.category_id = grp.category_id ";
		$sql .= " 		AND grp.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 		AND grp.evaluation_id = item.evaluation_id ";
		$sql .= " 		AND grp.category_id = item.category_id ";
		$sql .= " 		AND grp.group_id = item.group_id ";
		$sql .= " 		AND cat.disp_flg = 1 ";
		$sql .= " 		AND cat.delete_flg = 0 ";
		$sql .= " 		AND grp.disp_flg = 1 ";
		$sql .= " 		AND grp.delete_flg = 0 ";
		$sql .= " 		AND item.disp_flg = 1 ";
		$sql .= " 		AND item.delete_flg = 0 ";
		*/
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		ON ";
		$sql .= " 		 cat.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 		 AND cat.evaluation_id = mst.evaluation_id ";
		$sql .= " 		 AND cat.disp_flg = 1 ";
		$sql .= " 		 AND cat.delete_flg = 0 ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 		ON ";
		$sql .= " 		 grp.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		 AND grp.evaluation_id = cat.evaluation_id ";
		$sql .= " 		 AND grp.category_id = cat.category_id ";
		$sql .= " 		 AND grp.disp_flg = 1 ";
		$sql .= " 		 AND grp.delete_flg = 0 ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 		ON ";
		$sql .= " 		 item.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 		 AND item.evaluation_id = grp.evaluation_id ";
		$sql .= " 		 AND item.category_id = grp.category_id ";
		$sql .= " 		 AND item.group_id = grp.group_id ";
		$sql .= " 		 AND item.disp_flg = 1 ";
		$sql .= " 		 AND item.delete_flg = 0 ";
		// 2012/08/09 Yamagawa upd(e)

		$sql .= " 	group by ";
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id) cat_item_row ";
		// 2012/08/09 Yamagawa add(s)
		$sql .= " 	ON ";
		$sql .= " 	 cat_item_row.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	 AND cat_item_row.evaluation_id = cat.evaluation_id ";
		$sql .= " 	 AND cat_item_row.category_id = cat.category_id ";
		// 2012/08/09 Yamagawa add(e)

		// 2012/08/09 Yamagawa upd(s)
		//$sql .= " 	,(SELECT ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 (SELECT ";
		// 2012/08/09 Yamagawa upd(e)
		$sql .= " 		grp.levelup_apply_id ";
		$sql .= " 		, grp.level ";
		$sql .= " 		, grp.evaluation_id ";
		$sql .= " 		, grp.category_id ";
		$sql .= " 		, count(grp.group_id) AS grp_cnt ";
		$sql .= " 	FROM ";
		$sql .= " 		cl_apl_levelup_apply_evaluation mst ";
		// 2012/08/09 Yamagawa upd(s)
		/*
		$sql .= " 		, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	WHERE ";
		$sql .= " 		mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 		AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 		AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 		AND cat.category_id = grp.category_id ";
		$sql .= " 		AND cat.disp_flg = 1 ";
		$sql .= " 		AND cat.delete_flg = 0 ";
		$sql .= " 		AND grp.disp_flg = 1 ";
		$sql .= " 		AND grp.delete_flg = 0 ";
		*/
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		ON ";
		$sql .= " 		 cat.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 		 AND cat.evaluation_id = mst.evaluation_id ";
		$sql .= " 		 AND cat.disp_flg = 1 ";
		$sql .= " 		 AND cat.delete_flg = 0 ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 		ON ";
		$sql .= " 		 grp.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		 AND grp.evaluation_id = cat.evaluation_id ";
		$sql .= " 		 AND grp.category_id = cat.category_id ";
		$sql .= " 		 AND grp.disp_flg = 1 ";
		$sql .= " 		 AND grp.delete_flg = 0 ";
		// 2012/08/09 Yamagawa upd(e)
		$sql .= " 	group by ";
		$sql .= " 		grp.levelup_apply_id ";
		$sql .= " 		, grp.level ";
		$sql .= " 		, grp.evaluation_id ";
		$sql .= " 		, grp.category_id) AS cat_grp_row ";
		// 2012/08/09 Yamagawa add(s)
		$sql .= " 	ON ";
		$sql .= " 	 cat_grp_row.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	 AND cat_grp_row.evaluation_id = cat.evaluation_id ";
		$sql .= " 	 AND cat_grp_row.category_id = cat.category_id ";
		// 2012/08/09 Yamagawa add(e)

		// 2012/08/09 Yamagawa upd(s)
		//$sql .= " 	,(SELECT ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 (SELECT ";
		// 2012/08/09 Yamagawa upd(e)
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id ";
		$sql .= " 		, item.group_id ";
		$sql .= " 		, count(item_id) AS item_cnt ";
		$sql .= " 	FROM ";
		$sql .= " 		cl_apl_levelup_apply_evaluation mst ";
		// 2012/08/09 Yamagawa upd(s)
		/*
		$sql .= " 		, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	WHERE ";
		$sql .= " 		mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 		AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 		AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 		AND cat.category_id = grp.category_id ";
		$sql .= " 		AND grp.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 		AND grp.evaluation_id = item.evaluation_id ";
		$sql .= " 		AND grp.category_id = item.category_id ";
		$sql .= " 		AND grp.group_id = item.group_id ";
		$sql .= " 		AND cat.disp_flg = 1 ";
		$sql .= " 		AND cat.delete_flg = 0 ";
		$sql .= " 		AND grp.disp_flg = 1 ";
		$sql .= " 		AND grp.delete_flg = 0 ";
		$sql .= " 		AND item.disp_flg = 1 ";
		$sql .= " 		AND item.delete_flg = 0 ";
		*/
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		ON ";
		$sql .= " 		 cat.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 		 AND cat.evaluation_id = mst.evaluation_id ";
		$sql .= " 		 AND cat.disp_flg = 1 ";
		$sql .= " 		 AND cat.delete_flg = 0 ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 		ON ";
		$sql .= " 		 grp.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		 AND grp.evaluation_id = cat.evaluation_id ";
		$sql .= " 		 AND grp.category_id = cat.category_id ";
		$sql .= " 		 AND grp.disp_flg = 1 ";
		$sql .= " 		 AND grp.delete_flg = 0 ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 		ON ";
		$sql .= " 		 item.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 		 AND item.evaluation_id = grp.evaluation_id ";
		$sql .= " 		 AND item.category_id = grp.category_id ";
		$sql .= " 		 AND item.group_id = grp.group_id ";
		$sql .= " 		 AND item.disp_flg = 1 ";
		$sql .= " 		 AND item.delete_flg = 0 ";
		// 2012/08/09 Yamagawa upd(e)
		$sql .= " 	group by ";
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id ";
		$sql .= " 		, item.group_id) grprow ";
		// 2012/08/09 Yamagawa add(s)
		$sql .= " 	ON ";
		$sql .= " 	 grprow.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 	 AND grprow.evaluation_id = grp.evaluation_id ";
		$sql .= " 	 AND grprow.category_id = grp.category_id ";
		$sql .= " 	 AND grprow.group_id = grp.group_id ";
		// 2012/08/09 Yamagawa add(e)

		$sql .= " WHERE ";
		// 2012/08/09 Yamagawa upd(s)
		/*
		$sql .= " 	mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 	AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 	AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 	AND cat.category_id = grp.category_id ";
		$sql .= " 	AND cat.levelup_apply_id = cat_item_row.levelup_apply_id ";
		$sql .= " 	AND cat.evaluation_id = cat_item_row.evaluation_id ";
		$sql .= " 	AND cat.category_id = cat_item_row.category_id ";
		$sql .= " 	AND cat.levelup_apply_id = cat_grp_row.levelup_apply_id ";
		$sql .= " 	AND cat.evaluation_id = cat_grp_row.evaluation_id ";
		$sql .= " 	AND cat.category_id = cat_grp_row.category_id ";
		$sql .= " 	AND grp.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 	AND grp.evaluation_id = item.evaluation_id ";
		$sql .= " 	AND grp.category_id = item.category_id ";
		$sql .= " 	AND grp.group_id = item.group_id ";
		$sql .= " 	AND grp.levelup_apply_id = grprow.levelup_apply_id ";
		$sql .= " 	AND grp.evaluation_id = grprow.evaluation_id ";
		$sql .= " 	AND grp.category_id = grprow.category_id ";
		$sql .= " 	AND grp.group_id = grprow.group_id ";
		$sql .= " 	AND cat.disp_flg = 1 ";
		$sql .= " 	AND cat.delete_flg = 0 ";
		$sql .= " 	AND grp.disp_flg = 1 ";
		$sql .= " 	AND grp.delete_flg = 0 ";
		$sql .= " 	AND item.disp_flg = 1 ";
		$sql .= " 	AND item.delete_flg = 0 ";
		$sql .= " 	AND mst.levelup_apply_id = :levelup_apply_id ";
		*/
		$sql .= " 	mst.levelup_apply_id = :levelup_apply_id ";
		// 2012/08/09 Yamagawa upd(e)
		$sql .= " ORDER BY ";
		$sql .= " 	mst.disp_order ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, grp.disp_order ";
		$sql .= " 	, item.disp_order ";

		$data=parent::select($sql,array("text"),array("levelup_apply_id"=>$levelup_apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;

	}

	/**
	 * getEvaluationList_forEdit_div1 評価表の内容取得(自己、所属長評価)
	 *
	 * @param $levelup_apply_id    レベルアップ申請ID
	 * @return           評価内容配列
	**/
	function getEvaluationList_forEdit_div1($levelup_evaluation_id){

		$this->log->info(__FUNCTION__." START");
		$this->log->debug("levelup_evaluation_id：".$levelup_evaluation_id);

		$sql = "";
		// 2012/09/25 Yamagawa upd(s)
		// SQL再構築
		/*
		$sql .= " SELECT ";
		$sql .= " 	val.levelup_apply_id ";
		$sql .= " 	, mst.disp_order ";
		$sql .= " 	, mst.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order AS cat_order ";
		$sql .= " 	, cat_grp_row.grp_cnt + cat_item_row.cat_item_cnt AS cat_rowspan ";
		$sql .= " 	, val.group_id ";
		$sql .= " 	, grp.group_name ";
		$sql .= " 	, grp.group_no ";
		$sql .= " 	, grp.disp_order AS grp_order ";
		$sql .= " 	, grprow.item_cnt + 1 AS grp_rowspan ";
		$sql .= " 	, item.item_id ";
		$sql .= " 	, item.item_name ";
		$sql .= " 	, item.disp_order AS item_order ";
		$sql .= " 	, item.must_value ";
		$sql .= " 	, item.eval_disp_1_flg ";
		$sql .= " 	, item.eval_disp_2_flg ";
		$sql .= " 	, item.eval_disp_3_flg ";
		$sql .= " 	, item.eval_disp_4_flg ";
		$sql .= " 	, max(CASE val.evaluation_emp_division ";
		$sql .= " 		WHEN 1 THEN val.levelup_evaluation_value_id ";
		$sql .= " 		ELSE null ";
		$sql .= " 	END) AS levelup_evaluation_value_id1";
		$sql .= " 	, max(CASE val.evaluation_emp_division ";
		$sql .= " 		WHEN 1 THEN val.value ";
		$sql .= " 		ELSE null ";
		$sql .= " 	END) AS eval1 ";
		$sql .= " 	, max(CASE val.evaluation_emp_division ";
		$sql .= " 		WHEN 2 THEN val.levelup_evaluation_value_id ";
		$sql .= " 		ELSE null ";
		$sql .= " 	END) AS levelup_evaluation_value_id2 ";
		$sql .= " 	, max(CASE val.evaluation_emp_division ";
		$sql .= " 		WHEN 2 THEN val.value ";
		$sql .= " 		ELSE null ";
		$sql .= " 	END) AS eval2 ";
		$sql .= " 	, max(CASE val.evaluation_emp_division ";
		$sql .= " 		WHEN 3 THEN val.levelup_evaluation_value_id ";
		$sql .= " 		ELSE null ";
		$sql .= " 	END) AS levelup_evaluation_value_id3 ";
		$sql .= " 	, max(CASE val.evaluation_emp_division ";
		$sql .= " 		WHEN 3 THEN val.value ";
		$sql .= " 		ELSE null ";
		$sql .= " 	END) AS eval3 ";
		$sql .= " 	, max(CASE val.evaluation_emp_division ";
		$sql .= " 		WHEN 4 THEN val.levelup_evaluation_value_id ";
		$sql .= " 		ELSE null ";
		$sql .= " 	END) AS levelup_evaluation_value_id4 ";
		$sql .= " 	, max(CASE val.evaluation_emp_division ";
		$sql .= " 		WHEN 4 THEN val.value ";
		$sql .= " 		ELSE null ";
		$sql .= " 	END) AS eval4 ";

		$sql .= " FROM ";

		//$sql .= " 	cl_apl_levelup_evaluation_value as val ";

		//サブクエリ：cl_apl_levelup_evaluation_valueのうち以下のデータを抽出
		//・levelup_evaluation_idが該当するデータ(自己評価、師長評価)
		//・levelup_evaluation_idが無く、levelup_apply_idが該当するデータ(同僚評価×２)
		$sql .= " 	(";
		$sql .= " 	    SELECT a.* ";
		$sql .= " 	    FROM cl_apl_levelup_evaluation_value a";
		$sql .= " 	    INNER JOIN";
		$sql .= " 	    (";
		$sql .= " 	        SELECT levelup_evaluation_id, levelup_apply_id";
		$sql .= " 	        FROM cl_apl_levelup_evaluation";
		$sql .= " 	        WHERE levelup_evaluation_id = :levelup_evaluation_id ";
		$sql .= " 	    ) b";
		$sql .= " 	    ON ";
		$sql .= " 	    a.levelup_evaluation_id = b.levelup_evaluation_id";
		$sql .= " 	    OR";
		$sql .= " 	    (";
		$sql .= " 	      a.levelup_evaluation_id is null";
		$sql .= " 	      AND ";
		$sql .= " 	      a.levelup_apply_id = b.levelup_apply_id";
		$sql .= " 	    )";
		$sql .= " 	) as val ";

		// 2012/08/02 Yamagawa upd(s)
		//$sql .= " 	, cl_apl_levelup_apply_evaluation mst ";
		//$sql .= " 	, cl_apl_levelup_apply_evaluation_category cat ";
		//$sql .= " 	, cl_apl_levelup_apply_evaluation_group grp ";
		//$sql .= " 	, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 		cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 	ON ";
		$sql .= " 		mst.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 		cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	ON ";
		$sql .= "       cat.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 		AND cat.evaluation_id = mst.evaluation_id  ";
		$sql .= " 		AND cat.disp_flg = 1 ";
		$sql .= " 		AND cat.delete_flg = 0 ";
		$sql .= "       AND cat.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 		AND cat.evaluation_id = val.evaluation_id  ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 		cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	ON ";
		$sql .= " 		grp.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		AND grp.evaluation_id = cat.evaluation_id ";
		$sql .= " 		AND grp.category_id = cat.category_id ";
		$sql .= " 		AND grp.disp_flg = 1 ";
		$sql .= " 		AND grp.delete_flg = 0 ";
		$sql .= " 		AND grp.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 		AND grp.evaluation_id = val.evaluation_id ";
		$sql .= " 		AND grp.category_id = val.category_id ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 		cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	ON ";
		$sql .= " 		item.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 		AND item.evaluation_id = grp.evaluation_id ";
		$sql .= " 		AND item.category_id = grp.category_id ";
		$sql .= " 		AND item.group_id = grp.group_id ";
		$sql .= " 		AND item.disp_flg = 1 ";
		$sql .= " 		AND item.delete_flg = 0 ";
		$sql .= " 		AND item.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 		AND item.evaluation_id = val.evaluation_id ";
		$sql .= " 		AND item.category_id = val.category_id ";
		$sql .= " 		AND item.group_id = val.group_id ";
		// 2012/08/02 Yamagawa upd(e)
		// 2012/08/02 Yamagawa add(s)
		$sql .= " 	INNER JOIN ";
		// 2012/08/02 Yamagawa add(e)
		$sql .= " 	(SELECT ";
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id ";
		$sql .= " 		, count(item_id) AS cat_item_cnt ";
		$sql .= " 	FROM ";
		$sql .= " 		cl_apl_levelup_apply_evaluation mst ";
		// 2012/08/02 Yamagawa upd(s)
		//$sql .= " 		, cl_apl_levelup_apply_evaluation_category cat ";
		//$sql .= " 		, cl_apl_levelup_apply_evaluation_group grp ";
		//$sql .= " 		, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= "		INNER JOIN ";
		$sql .= " 			cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= "		ON ";
		$sql .= " 			cat.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 			AND cat.evaluation_id = mst.evaluation_id ";
		$sql .= " 			AND cat.disp_flg = 1 ";
		$sql .= " 			AND cat.delete_flg = 0 ";
		$sql .= "		INNER JOIN ";
		$sql .= " 			cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= "		ON ";
		$sql .= " 			grp.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 			AND grp.evaluation_id = cat.evaluation_id ";
		$sql .= " 			AND grp.category_id = cat.category_id ";
		$sql .= " 			AND grp.disp_flg = 1 ";
		$sql .= " 			AND grp.delete_flg = 0 ";
		$sql .= "		INNER JOIN ";
		$sql .= " 			cl_apl_levelup_apply_evaluation_item item ";
		$sql .= "		ON ";
		$sql .= " 			item.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 			AND item.evaluation_id = grp.evaluation_id ";
		$sql .= " 			AND item.category_id = grp.category_id ";
		$sql .= " 			AND item.group_id = grp.group_id ";
		$sql .= " 			AND item.disp_flg = 1 ";
		$sql .= " 			AND item.delete_flg = 0 ";
		// 2012/08/02 Yamagawa upd(e)
		// 2012/08/02 Yamagawa del(s)
		//$sql .= " 	WHERE ";
		//$sql .= " 		mst.levelup_apply_id = cat.levelup_apply_id ";
		//$sql .= " 		AND mst.evaluation_id = cat.evaluation_id ";
		//$sql .= " 		AND cat.levelup_apply_id = grp.levelup_apply_id ";
		//$sql .= " 		AND cat.evaluation_id = grp.evaluation_id ";
		//$sql .= " 		AND cat.category_id = grp.category_id ";
		//$sql .= " 		AND grp.levelup_apply_id = item.levelup_apply_id ";
		//$sql .= " 		AND grp.evaluation_id = item.evaluation_id ";
		//$sql .= " 		AND grp.category_id = item.category_id ";
		//$sql .= " 		AND grp.group_id = item.group_id ";
		//$sql .= " 		AND cat.disp_flg = 1 ";
		//$sql .= " 		AND cat.delete_flg = 0 ";
		//$sql .= " 		AND grp.disp_flg = 1 ";
		//$sql .= " 		AND grp.delete_flg = 0 ";
		//$sql .= " 		AND item.disp_flg = 1 ";
		//$sql .= " 		AND item.delete_flg = 0 ";
		// 2012/08/02 Yamagawa del(e)
		$sql .= " 	group by ";
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id) cat_item_row ";
		// 2012/08/02 Yamagawa add(s)
		$sql .= " 	ON ";
		$sql .= " 		cat_item_row.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		AND cat_item_row.evaluation_id = cat.evaluation_id ";
		$sql .= " 		AND cat_item_row.category_id = cat.category_id ";
		$sql .= " 	INNER JOIN ";
		// 2012/08/02 Yamagawa add(e)
		$sql .= " 	(SELECT ";
		$sql .= " 		grp.levelup_apply_id ";
		$sql .= " 		, grp.level ";
		$sql .= " 		, grp.evaluation_id ";
		$sql .= " 		, grp.category_id ";
		$sql .= " 		, count(grp.group_id) AS grp_cnt ";
		$sql .= " 	FROM ";
		$sql .= " 		cl_apl_levelup_apply_evaluation mst ";
		// 2012/08/02 Yamagawa upd(s)
		//$sql .= " 		, cl_apl_levelup_apply_evaluation_category cat ";
		//$sql .= " 		, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= "		INNER JOIN ";
		$sql .= " 			cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= "		ON ";
		$sql .= " 			cat.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 			AND cat.evaluation_id = mst.evaluation_id ";
		$sql .= " 			AND cat.disp_flg = 1 ";
		$sql .= " 			AND cat.delete_flg = 0 ";
		$sql .= "		INNER JOIN ";
		$sql .= " 			cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= "		ON ";
		$sql .= " 			grp.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 			AND grp.evaluation_id = cat.evaluation_id ";
		$sql .= " 			AND grp.category_id = cat.category_id ";
		$sql .= " 			AND grp.disp_flg = 1 ";
		$sql .= " 			AND grp.delete_flg = 0 ";
		// 2012/08/02 Yamagawa upd(e)
		// 2012/08/02 Yamagawa del(s)
		//$sql .= " 	WHERE ";
		//$sql .= " 		mst.levelup_apply_id = cat.levelup_apply_id ";
		//$sql .= " 		AND mst.evaluation_id = cat.evaluation_id ";
		//$sql .= " 		AND cat.levelup_apply_id = grp.levelup_apply_id ";
		//$sql .= " 		AND cat.evaluation_id = grp.evaluation_id ";
		//$sql .= " 		AND cat.category_id = grp.category_id ";
		////$sql .= " 		AND mst.use_flg = 1 ";
		//$sql .= " 		AND cat.disp_flg = 1 ";
		//$sql .= " 		AND cat.delete_flg = 0 ";
		//$sql .= " 		AND grp.disp_flg = 1 ";
		//$sql .= " 		AND grp.delete_flg = 0 ";
		// 2012/08/02 Yamagawa del(e)
		$sql .= " 	group by ";
		$sql .= " 		grp.levelup_apply_id ";
		$sql .= " 		, grp.level ";
		$sql .= " 		, grp.evaluation_id ";
		$sql .= " 		, grp.category_id) AS cat_grp_row ";
		// 2012/08/02 Yamagawa add(s)
		$sql .= " 	ON ";
		$sql .= " 			cat_grp_row.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		AND cat_grp_row.evaluation_id = cat.evaluation_id ";
		$sql .= " 		AND cat_grp_row.category_id = cat.category_id ";
		$sql .= " 	INNER JOIN ";
		// 2012/08/02 Yamagawa add(e)
		$sql .= " 	(SELECT ";
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id ";
		$sql .= " 		, item.group_id ";
		$sql .= " 		, count(item_id) AS item_cnt ";
		$sql .= " 	FROM ";
		$sql .= " 		cl_apl_levelup_apply_evaluation mst ";
		// 2012/08/02 Yamagawa upd(s)
		//$sql .= " 		, cl_apl_levelup_apply_evaluation_category cat ";
		//$sql .= " 		, cl_apl_levelup_apply_evaluation_group grp ";
		//$sql .= " 		, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 			cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		ON ";
		$sql .= " 			cat.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 			AND cat.evaluation_id = mst.evaluation_id ";
		$sql .= " 			AND cat.disp_flg = 1 ";
		$sql .= " 			AND cat.delete_flg = 0 ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 			cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 		ON ";
		$sql .= " 			grp.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 			AND grp.evaluation_id = cat.evaluation_id ";
		$sql .= " 			AND grp.category_id = cat.category_id ";
		$sql .= " 			AND grp.disp_flg = 1 ";
		$sql .= " 			AND grp.delete_flg = 0 ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 			cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 		ON ";
		$sql .= " 			item.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 			AND item.evaluation_id = grp.evaluation_id ";
		$sql .= " 			AND item.category_id = grp.category_id ";
		$sql .= " 			AND item.group_id = grp.group_id ";
		$sql .= " 			AND item.disp_flg = 1 ";
		$sql .= " 			AND item.delete_flg = 0 ";
		// 2012/08/02 Yamagawa upd(e)
		// 2012/08/02 Yamagawa del(s)
		//$sql .= " 	WHERE ";
		//$sql .= " 		mst.levelup_apply_id = cat.levelup_apply_id ";
		//$sql .= " 		AND mst.evaluation_id = cat.evaluation_id ";
		//$sql .= " 		AND cat.levelup_apply_id = grp.levelup_apply_id ";
		//$sql .= " 		AND cat.evaluation_id = grp.evaluation_id ";
		//$sql .= " 		AND cat.category_id = grp.category_id ";
		//$sql .= " 		AND grp.levelup_apply_id = item.levelup_apply_id ";
		//$sql .= " 		AND grp.evaluation_id = item.evaluation_id ";
		//$sql .= " 		AND grp.category_id = item.category_id ";
		//$sql .= " 		AND grp.group_id = item.group_id ";
		////$sql .= " 		AND mst.use_flg = 1 ";
		//$sql .= " 		AND cat.disp_flg = 1 ";
		//$sql .= " 		AND cat.delete_flg = 0 ";
		//$sql .= " 		AND grp.disp_flg = 1 ";
		//$sql .= " 		AND grp.delete_flg = 0 ";
		//$sql .= " 		AND item.disp_flg = 1 ";
		//$sql .= " 		AND item.delete_flg = 0 ";
		// 2012/08/02 Yamagawa del(e)
		$sql .= " 	group by ";
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id ";
		$sql .= " 		, item.group_id) grprow ";
		// 2012/08/02 Yamagawa add(s)
		$sql .= " 	ON ";
		$sql .= " 		grprow.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 		AND grprow.evaluation_id = grp.evaluation_id ";
		$sql .= " 		AND grprow.category_id = grp.category_id ";
		$sql .= " 		AND grprow.group_id = grp.group_id ";
		// 2012/08/02 Yamagawa add(e)
		// 2012/08/02 Yamagawa del(s)
		//$sql .= " WHERE ";
		//$sql .= " 	val.levelup_apply_id = mst.levelup_apply_id ";
		//$sql .= " 	AND val.category_id = cat.category_id ";
		//$sql .= " 	AND val.group_id = grp.group_id ";
		//$sql .= " 	AND val.item_id = item.item_id ";
		//$sql .= " 	AND mst.levelup_apply_id = cat.levelup_apply_id ";
		//$sql .= " 	AND mst.evaluation_id = cat.evaluation_id ";
		//$sql .= " 	AND cat.levelup_apply_id = grp.levelup_apply_id ";
		//$sql .= " 	AND cat.evaluation_id = grp.evaluation_id ";
		//$sql .= " 	AND cat.category_id = grp.category_id ";
		//$sql .= " 	AND cat.levelup_apply_id = cat_item_row.levelup_apply_id ";
		//$sql .= " 	AND cat.evaluation_id = cat_item_row.evaluation_id ";
		//$sql .= " 	AND cat.category_id = cat_item_row.category_id ";
		//$sql .= " 	AND cat.levelup_apply_id = cat_grp_row.levelup_apply_id ";
		//$sql .= " 	AND cat.evaluation_id = cat_grp_row.evaluation_id ";
		//$sql .= " 	AND cat.category_id = cat_grp_row.category_id ";
		//$sql .= " 	AND grp.levelup_apply_id = item.levelup_apply_id ";
		//$sql .= " 	AND grp.evaluation_id = item.evaluation_id ";
		//$sql .= " 	AND grp.category_id = item.category_id ";
		//$sql .= " 	AND grp.group_id = item.group_id ";
		//$sql .= " 	AND grp.levelup_apply_id = grprow.levelup_apply_id ";
		//$sql .= " 	AND grp.evaluation_id = grprow.evaluation_id ";
		//$sql .= " 	AND grp.category_id = grprow.category_id ";
		//$sql .= " 	AND grp.group_id = grprow.group_id ";
		//$sql .= " 	AND mst.use_flg = 1 ";
		//$sql .= " 	AND cat.disp_flg = 1 ";
		//$sql .= " 	AND cat.delete_flg = 0 ";
		//$sql .= " 	AND grp.disp_flg = 1 ";
		//$sql .= " 	AND grp.delete_flg = 0 ";
		//$sql .= " 	AND item.disp_flg = 1 ";
		//$sql .= " 	AND item.delete_flg = 0 ";
		// 2012/08/02 Yamagawa del(e)
		//$sql .= " 	AND val.evaluation_emp_division in (1,2) ";
		//$sql .= " 	AND val.levelup_evaluation_id = :levelup_evaluation_id ";
		$sql .= " GROUP BY ";
		$sql .= " 	val.levelup_apply_id ";
		$sql .= " 	, mst.disp_order ";
		$sql .= " 	, mst.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, cat_grp_row.grp_cnt + cat_item_row.cat_item_cnt ";
		$sql .= " 	, val.group_id ";
		$sql .= " 	, grp.group_no ";
		$sql .= " 	, grp.group_name ";
		$sql .= " 	, grp.disp_order ";
		$sql .= " 	, grprow.item_cnt + 1 ";
		$sql .= " 	, item.item_id ";
		$sql .= " 	, item.item_name ";
		$sql .= " 	, item.disp_order ";
		$sql .= " 	, item.must_value ";
		$sql .= " 	, item.eval_disp_1_flg ";
		$sql .= " 	, item.eval_disp_2_flg ";
		$sql .= " 	, item.eval_disp_3_flg ";
		$sql .= " 	, item.eval_disp_4_flg ";
		$sql .= " 	, mst.passing_mark ";
		$sql .= " ORDER BY ";
		$sql .= " 	mst.disp_order ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, grp.disp_order ";
		$sql .= " 	, item.disp_order ";
		*/
		$sql .= " SELECT ";
		$sql .= "  val.levelup_apply_id ";
		$sql .= "  ,mst.disp_order ";
		$sql .= "  ,mst.evaluation_id ";
		$sql .= "  ,val.category_id ";
		$sql .= "  ,cat.category_name ";
		$sql .= "  ,cat.disp_order AS cat_order ";
		$sql .= "  ,cat_grp_row.grp_cnt + cat_item_row.cat_item_cnt AS cat_rowspan ";
		$sql .= "  ,val.group_id ";
		$sql .= "  ,grp.group_name ";
		$sql .= "  ,grp.group_no ";
		$sql .= "  ,grp.disp_order AS grp_order ";
		$sql .= "  ,grprow.item_cnt + 1 AS grp_rowspan ";
		$sql .= "  ,item.item_id ";
		$sql .= "  ,item.item_name ";
		$sql .= "  ,item.disp_order AS item_order ";
		$sql .= "  ,item.must_value ";
		$sql .= "  ,item.eval_disp_1_flg ";
		$sql .= "  ,item.eval_disp_2_flg ";
		$sql .= "  ,item.eval_disp_3_flg ";
		$sql .= "  ,item.eval_disp_4_flg ";
		$sql .= "  ,val.levelup_evaluation_value_id1 ";
		$sql .= "  ,val.eval1 ";
		$sql .= "  ,val.levelup_evaluation_value_id2 ";
		$sql .= "  ,val.eval2 ";
		$sql .= "  ,val.levelup_evaluation_value_id3 ";
		$sql .= "  ,val.eval3 ";
		$sql .= "  ,val.levelup_evaluation_value_id4 ";
		$sql .= "  ,val.eval4 ";
		$sql .= " FROM ";
		$sql .= "  (SELECT ";
		$sql .= "    a.levelup_apply_id ";
		$sql .= "    ,a.evaluation_id ";
		$sql .= "    ,a.category_id ";
		$sql .= "    ,a.group_id ";
		$sql .= "    ,a.item_id ";
		$sql .= "    ,MAX( ";
		$sql .= "      CASE ";
		$sql .= "       WHEN a.evaluation_emp_division = 1 ";
		$sql .= "       THEN a.levelup_evaluation_value_id ";
		$sql .= "       ELSE NULL ";
		$sql .= "      END ";
		$sql .= "     ) AS levelup_evaluation_value_id1 ";
		$sql .= "    ,MAX( ";
		$sql .= "      CASE ";
		$sql .= "       WHEN a.evaluation_emp_division = 2 ";
		$sql .= "       THEN a.levelup_evaluation_value_id ";
		$sql .= "       ELSE NULL ";
		$sql .= "      END ";
		$sql .= "     ) AS levelup_evaluation_value_id2 ";
		$sql .= "    ,MAX( ";
		$sql .= "      CASE ";
		$sql .= "       WHEN a.evaluation_emp_division = 3 ";
		$sql .= "       THEN a.levelup_evaluation_value_id ";
		$sql .= "       ELSE NULL ";
		$sql .= "      END ";
		$sql .= "     ) AS levelup_evaluation_value_id3 ";
		$sql .= "    ,MAX( ";
		$sql .= "      CASE ";
		$sql .= "       WHEN a.evaluation_emp_division = 4 ";
		$sql .= "       THEN a.levelup_evaluation_value_id ";
		$sql .= "       ELSE NULL ";
		$sql .= "      END ";
		$sql .= "     ) AS levelup_evaluation_value_id4 ";
		$sql .= "    ,MAX( ";
		$sql .= "      CASE ";
		$sql .= "       WHEN a.evaluation_emp_division = 1 ";
		$sql .= "       THEN a.value ";
		$sql .= "       ELSE NULL ";
		$sql .= "      END ";
		$sql .= "     ) AS eval1 ";
		$sql .= "    ,MAX( ";
		$sql .= "      CASE ";
		$sql .= "       WHEN a.evaluation_emp_division = 2 ";
		$sql .= "       THEN a.value ";
		$sql .= "       ELSE NULL ";
		$sql .= "      END ";
		$sql .= "     ) AS eval2 ";
		$sql .= "    ,MAX( ";
		$sql .= "      CASE ";
		$sql .= "       WHEN a.evaluation_emp_division = 3 ";
		$sql .= "       THEN a.value ";
		$sql .= "       ELSE NULL ";
		$sql .= "      END ";
		$sql .= "     ) AS eval3 ";
		$sql .= "    ,MAX( ";
		$sql .= "      CASE ";
		$sql .= "       WHEN a.evaluation_emp_division = 4 ";
		$sql .= "       THEN a.value ";
		$sql .= "       ELSE NULL ";
		$sql .= "      END ";
		$sql .= "     ) AS eval4 ";
		$sql .= "   FROM ";
		$sql .= "    cl_apl_levelup_evaluation_value a ";
		$sql .= "    INNER JOIN ";
		$sql .= "     (SELECT ";
		$sql .= "       levelup_evaluation_id ";
		$sql .= "       ,levelup_apply_id ";
		$sql .= "      FROM ";
		$sql .= "       cl_apl_levelup_evaluation ";
		$sql .= "      WHERE ";
		$sql .= "       levelup_evaluation_id = :levelup_evaluation_id ";
		$sql .= "     ) b ";
		$sql .= "    ON ";
		$sql .= "     a.levelup_evaluation_id = b.levelup_evaluation_id ";
		$sql .= "     OR ( ";
		$sql .= "      a.levelup_evaluation_id IS NULL ";
		$sql .= "      AND a.levelup_apply_id = b.levelup_apply_id ";
		$sql .= "     ) ";
		$sql .= "   GROUP BY ";
		$sql .= "    a.levelup_apply_id ";
		$sql .= "    ,a.evaluation_id ";
		$sql .= "    ,a.category_id ";
		$sql .= "    ,a.group_id ";
		$sql .= "    ,a.item_id ";
		$sql .= "  ) val ";
		$sql .= "  INNER JOIN ";
		$sql .= "   cl_apl_levelup_apply_evaluation mst ";
		$sql .= "  ON ";
		$sql .= "   mst.levelup_apply_id = val.levelup_apply_id ";
		$sql .= "   AND mst.evaluation_id = val.evaluation_id ";
		$sql .= "  INNER JOIN ";
		$sql .= "   cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= "  ON ";
		$sql .= "   cat.levelup_apply_id = val.levelup_apply_id ";
		$sql .= "   AND cat.evaluation_id = val.evaluation_id ";
		$sql .= "   AND cat.category_id = val.category_id ";
		$sql .= "   AND cat.disp_flg = 1 ";
		$sql .= "   AND cat.delete_flg = 0 ";
		$sql .= "  INNER JOIN ";
		$sql .= "   cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= "  ON ";
		$sql .= "   grp.levelup_apply_id = val.levelup_apply_id ";
		$sql .= "   AND grp.evaluation_id = val.evaluation_id ";
		$sql .= "   AND grp.category_id = val.category_id ";
		$sql .= "   AND grp.group_id = val.group_id ";
		$sql .= "   AND grp.disp_flg = 1 ";
		$sql .= "   AND grp.delete_flg = 0 ";
		$sql .= "  INNER JOIN ";
		$sql .= "   cl_apl_levelup_apply_evaluation_item item ";
		$sql .= "  ON ";
		$sql .= "   item.levelup_apply_id = val.levelup_apply_id ";
		$sql .= "   AND item.evaluation_id = val.evaluation_id ";
		$sql .= "   AND item.category_id = val.category_id ";
		$sql .= "   AND item.group_id = val.group_id ";
		$sql .= "   AND item.item_id = val.item_id ";
		$sql .= "   AND item.disp_flg = 1 ";
		$sql .= "   AND item.delete_flg = 0 ";
		$sql .= "  INNER JOIN ";
		$sql .= "   (SELECT ";
		$sql .= "     item.levelup_apply_id ";
		$sql .= "     ,item.evaluation_id ";
		$sql .= "     ,item.category_id ";
		$sql .= "     ,COUNT(item_id) AS cat_item_cnt ";
		$sql .= "    FROM ";
		$sql .= "     cl_apl_levelup_apply_evaluation mst ";
		$sql .= "     INNER JOIN ";
		$sql .= "      cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= "     ON ";
		$sql .= "      cat.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= "      AND cat.evaluation_id = mst.evaluation_id ";
		$sql .= "      AND cat.disp_flg = 1 ";
		$sql .= "      AND cat.delete_flg = 0 ";
		$sql .= "     INNER JOIN ";
		$sql .= "      cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= "     ON ";
		$sql .= "      grp.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= "      AND grp.evaluation_id = cat.evaluation_id ";
		$sql .= "      AND grp.category_id = cat.category_id ";
		$sql .= "      AND grp.disp_flg = 1 ";
		$sql .= "      AND grp.delete_flg = 0 ";
		$sql .= "     INNER JOIN ";
		$sql .= "      cl_apl_levelup_apply_evaluation_item item ";
		$sql .= "     ON ";
		$sql .= "      item.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= "      AND item.evaluation_id = grp.evaluation_id ";
		$sql .= "      AND item.category_id = grp.category_id ";
		$sql .= "      AND item.group_id = grp.group_id ";
		$sql .= "      AND item.disp_flg = 1 ";
		$sql .= "      AND item.delete_flg = 0 ";
		$sql .= "     GROUP BY ";
		$sql .= "      item.levelup_apply_id ";
		$sql .= "      ,item.evaluation_id ";
		$sql .= "      ,item.category_id ";
		$sql .= "   ) cat_item_row ";
		$sql .= "  ON ";
		$sql .= "   cat_item_row.levelup_apply_id = val.levelup_apply_id ";
		$sql .= "   AND cat_item_row.evaluation_id = val.evaluation_id ";
		$sql .= "   AND cat_item_row.category_id = val.category_id ";
		$sql .= "  INNER JOIN ";
		$sql .= "   (SELECT ";
		$sql .= "     grp.levelup_apply_id ";
		$sql .= "     ,grp.evaluation_id ";
		$sql .= "     ,grp.category_id ";
		$sql .= "     ,COUNT(grp.group_id) AS grp_cnt ";
		$sql .= "    FROM ";
		$sql .= "     cl_apl_levelup_apply_evaluation mst ";
		$sql .= "     INNER JOIN ";
		$sql .= "      cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= "     ON ";
		$sql .= "      cat.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= "      AND cat.evaluation_id = mst.evaluation_id ";
		$sql .= "      AND cat.disp_flg = 1 ";
		$sql .= "      AND cat.delete_flg = 0 ";
		$sql .= "     INNER JOIN ";
		$sql .= "      cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= "     ON ";
		$sql .= "      grp.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= "      AND grp.evaluation_id = cat.evaluation_id ";
		$sql .= "      AND grp.category_id = cat.category_id ";
		$sql .= "      AND grp.disp_flg = 1 ";
		$sql .= "      AND grp.delete_flg = 0 ";
		$sql .= "     GROUP BY ";
		$sql .= "      grp.levelup_apply_id ";
		$sql .= "      ,grp.evaluation_id ";
		$sql .= "      ,grp.category_id ";
		$sql .= "   ) cat_grp_row ";
		$sql .= "  ON ";
		$sql .= "   cat_grp_row.levelup_apply_id = val.levelup_apply_id ";
		$sql .= "   AND cat_grp_row.evaluation_id = val.evaluation_id ";
		$sql .= "   AND cat_grp_row.category_id = val.category_id ";
		$sql .= "  INNER JOIN ";
		$sql .= "   (SELECT ";
		$sql .= "     item.levelup_apply_id ";
		$sql .= "     ,item.evaluation_id ";
		$sql .= "     ,item.category_id ";
		$sql .= "     ,item.group_id ";
		$sql .= "     ,COUNT(item_id) AS item_cnt ";
		$sql .= "    FROM ";
		$sql .= "     cl_apl_levelup_apply_evaluation mst ";
		$sql .= "     INNER JOIN ";
		$sql .= "      cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= "     ON ";
		$sql .= "      cat.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= "      AND cat.evaluation_id = mst.evaluation_id ";
		$sql .= "      AND cat.disp_flg = 1 ";
		$sql .= "      AND cat.delete_flg = 0 ";
		$sql .= "     INNER JOIN ";
		$sql .= "      cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= "     ON ";
		$sql .= "      grp.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= "      AND grp.evaluation_id = cat.evaluation_id ";
		$sql .= "      AND grp.category_id = cat.category_id ";
		$sql .= "      AND grp.disp_flg = 1 ";
		$sql .= "      AND grp.delete_flg = 0 ";
		$sql .= "     INNER JOIN ";
		$sql .= "      cl_apl_levelup_apply_evaluation_item item ";
		$sql .= "     ON ";
		$sql .= "      item.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= "      AND item.evaluation_id = grp.evaluation_id ";
		$sql .= "      AND item.category_id = grp.category_id ";
		$sql .= "      AND item.group_id = grp.group_id ";
		$sql .= "      AND item.disp_flg = 1 ";
		$sql .= "      AND item.delete_flg = 0 ";
		$sql .= "     GROUP BY ";
		$sql .= "      item.levelup_apply_id ";
		$sql .= "      ,item.evaluation_id ";
		$sql .= "      ,item.category_id ";
		$sql .= "      ,item.group_id ";
		$sql .= "   ) grprow ";
		$sql .= "  ON ";
		$sql .= "   grprow.levelup_apply_id = val.levelup_apply_id ";
		$sql .= "   AND grprow.evaluation_id = val.evaluation_id ";
		$sql .= "   AND grprow.category_id = val.category_id ";
		$sql .= "   AND grprow.group_id = val.group_id ";
		$sql .= " ORDER BY ";
		$sql .= "  mst.disp_order ";
		$sql .= "  ,cat.disp_order ";
		$sql .= "  ,grp.disp_order ";
		$sql .= "  ,item.disp_order ";
		// 2012/09/25 Yamagawa upd(e)

		$data=parent::select($sql,array("text"),array("levelup_evaluation_id"=>$levelup_evaluation_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;

	}

	/**
	 * getEvaluationList_forEdit_div2 評価表の内容取得(同僚評価)
	 *
	 * @param $levelup_apply_id    レベルアップ申請ID
	 * @return           評価内容配列
	**/
	function getEvaluationList_forEdit_div2($levelup_evaluation_id){

		$this->log->info(__FUNCTION__." START");
		$this->log->debug("levelup_evaluation_id：".$levelup_evaluation_id);
		$this->log->debug("evaluation_emp_id：".$login_user);

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	item.levelup_apply_id ";
		$sql .= " 	, mst.disp_order ";
		$sql .= " 	, mst.evaluation_id ";
		$sql .= " 	, item.category_id ";
		$sql .= " 	, cat.category_name ";
		$sql .= " 	, cat.disp_order AS cat_order ";
		$sql .= " 	, cat_grp_row.grp_cnt + cat_item_row.cat_item_cnt AS cat_rowspan ";
		$sql .= " 	, item.group_id ";
		$sql .= " 	, grp.group_no ";
		$sql .= " 	, grp.group_name ";
		$sql .= " 	, grp.disp_order AS grp_order ";
		$sql .= " 	, grprow.item_cnt + 1 AS grp_rowspan ";
		$sql .= " 	, item.item_id ";
		$sql .= " 	, item.item_name ";
		$sql .= " 	, item.disp_order AS item_order ";
		$sql .= " 	, item.must_value ";
		$sql .= " 	, item.eval_disp_1_flg ";
		$sql .= " 	, item.eval_disp_2_flg ";
		$sql .= " 	, item.eval_disp_3_flg ";
		$sql .= " 	, item.eval_disp_4_flg ";
		$sql .= " 	, val.levelup_evaluation_value_id as levelup_evaluation_value_id1 ";
		$sql .= " 	, val.value AS eval1 ";
		$sql .= " 	, null AS eval2 ";
		$sql .= " FROM ";

		// 2012/08/09 Yamagawa upd(s)
		//$sql .= " 	cl_apl_levelup_apply_evaluation mst ";
		//$sql .= " 	, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	cl_apl_levelup_evaluation_value val ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 	ON ";
		$sql .= " 	 mst.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	 AND mst.evaluation_id = val.evaluation_id ";
		$sql .= " 	 AND mst.delete_flg = 0 ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	ON ";
		$sql .= " 	 cat.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	 AND cat.evaluation_id = val.evaluation_id ";
		$sql .= " 	 AND cat.category_id = val.category_id ";
		$sql .= " 	 AND cat.disp_flg = 1 ";
		$sql .= " 	 AND cat.delete_flg = 0 ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	ON ";
		$sql .= " 	 grp.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	 AND grp.evaluation_id = val.evaluation_id ";
		$sql .= " 	 AND grp.category_id = val.category_id ";
		$sql .= " 	 AND grp.group_id = val.group_id ";
		$sql .= " 	 AND grp.disp_flg = 1 ";
		$sql .= " 	 AND grp.delete_flg = 0 ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	ON ";
		$sql .= " 	 item.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	 AND item.evaluation_id = val.evaluation_id ";
		$sql .= " 	 AND item.category_id = val.category_id ";
		$sql .= " 	 AND item.group_id = val.group_id ";
		$sql .= " 	 AND item.item_id = val.item_id ";
		$sql .= " 	 AND item.disp_flg = 1 ";
		$sql .= " 	 AND item.delete_flg = 0 ";
		// 2012/08/09 Yamagawa upd(e)

		// 2012/08/09 Yamagawa upd(s)
		//$sql .= " 	,(SELECT ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	(SELECT ";
		// 2012/08/09 Yamagawa upd(e)
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id ";
		$sql .= " 		, count(item.item_id) AS cat_item_cnt ";
		$sql .= " 	FROM ";
		// 2012/08/09 Yamagawa upd(s)
		/*
		$sql .= " 		cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	WHERE ";
		$sql .= " 		mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 		AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 		AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 		AND cat.category_id = grp.category_id ";
		$sql .= " 		AND grp.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 		AND grp.evaluation_id = item.evaluation_id ";
		$sql .= " 		AND grp.category_id = item.category_id ";
		$sql .= " 		AND grp.group_id = item.group_id ";
		$sql .= " 		AND cat.disp_flg = 1 ";
		$sql .= " 		AND cat.delete_flg = 0 ";
		$sql .= " 		AND grp.disp_flg = 1 ";
		$sql .= " 		AND grp.delete_flg = 0 ";
		$sql .= " 		AND item.disp_flg = 1 ";
		$sql .= " 		AND item.delete_flg = 0 ";
		*/
		$sql .= " 		cl_apl_levelup_evaluation_value val ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 		ON ";
		$sql .= " 		 mst.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 		 AND mst.evaluation_id = val.evaluation_id ";
		$sql .= " 		 AND mst.delete_flg = 0 ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		ON ";
		$sql .= " 		 cat.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 		 AND cat.evaluation_id = val.evaluation_id ";
		$sql .= " 		 AND cat.category_id = val.category_id ";
		$sql .= " 		 AND cat.disp_flg = 1 ";
		$sql .= " 		 AND cat.delete_flg = 0 ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 		ON ";
		$sql .= " 		 grp.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 		 AND grp.evaluation_id = val.evaluation_id ";
		$sql .= " 		 AND grp.category_id = val.category_id ";
		$sql .= " 		 AND grp.group_id = val.group_id ";
		$sql .= " 		 AND grp.disp_flg = 1 ";
		$sql .= " 		 AND grp.delete_flg = 0 ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 		ON ";
		$sql .= " 		 item.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 		 AND item.evaluation_id = val.evaluation_id ";
		$sql .= " 		 AND item.category_id = val.category_id ";
		$sql .= " 		 AND item.group_id = val.group_id ";
		$sql .= " 		 AND item.item_id = val.item_id ";
		$sql .= " 		 AND item.disp_flg = 1 ";
		$sql .= " 		 AND item.delete_flg = 0 ";
		$sql .= " 		WHERE ";
		$sql .= " 		 val.levelup_evaluation_colleague_id = :levelup_evaluation_colleague_id ";
		// 2012/08/09 Yamagawa upd(e)
		$sql .= " 	group by ";
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id) cat_item_row ";
		// 2012/08/09 Yamagawa add(s)
		$sql .= " 	ON ";
		$sql .= " 	 cat_item_row.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	 AND cat_item_row.evaluation_id = cat.evaluation_id ";
		$sql .= " 	 AND cat_item_row.category_id = cat.category_id ";
		// 2012/08/09 Yamagawa add(e)

		// 2012/08/09 Yamagawa upd(s)
		//$sql .= " 	,(SELECT ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	(SELECT ";
		// 2012/08/09 Yamagawa upd(e)
		$sql .= " 		grp.levelup_apply_id ";
		$sql .= " 		, grp.level ";
		$sql .= " 		, grp.evaluation_id ";
		$sql .= " 		, grp.category_id ";
		$sql .= " 		, count(grp.group_id) AS grp_cnt ";
		$sql .= " 	FROM ";
		$sql .= " 		cl_apl_levelup_apply_evaluation mst ";
		// 2012/08/09 Yamagawa upd(s)
		/*
		$sql .= " 		, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	WHERE ";
		$sql .= " 		mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 		AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 		AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 		AND cat.category_id = grp.category_id ";
		$sql .= " 		AND cat.disp_flg = 1 ";
		$sql .= " 		AND cat.delete_flg = 0 ";
		$sql .= " 		AND grp.disp_flg = 1 ";
		$sql .= " 		AND grp.delete_flg = 0 ";
		*/
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		ON ";
		$sql .= " 		 cat.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 		 AND cat.evaluation_id = mst.evaluation_id ";
		$sql .= " 		 AND cat.disp_flg = 1 ";
		$sql .= " 		 AND cat.delete_flg = 0 ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 		ON ";
		$sql .= " 		 grp.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		 AND grp.evaluation_id = cat.evaluation_id ";
		$sql .= " 		 AND grp.category_id = cat.category_id ";
		$sql .= " 		 AND grp.disp_flg = 1 ";
		$sql .= " 		 AND grp.delete_flg = 0 ";
		$sql .= " 		WHERE ";
		$sql .= "        EXISTS( ";
		$sql .= " 			SELECT ";
		$sql .= " 			 val.levelup_evaluation_colleague_id ";
		$sql .= " 			FROM ";
		$sql .= " 			 cl_apl_levelup_evaluation_value val ";
		$sql .= " 			WHERE ";
		$sql .= " 			 val.levelup_evaluation_colleague_id = :levelup_evaluation_colleague_id ";
		$sql .= " 			 AND val.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 			 AND val.evaluation_id = mst.evaluation_id ";
		$sql .= " 		 ) ";
		// 2012/08/09 Yamagawa upd(e)
		$sql .= " 	group by ";
		$sql .= " 		grp.levelup_apply_id ";
		$sql .= " 		, grp.level ";
		$sql .= " 		, grp.evaluation_id ";
		$sql .= " 		, grp.category_id) AS cat_grp_row ";
		// 2012/08/09 Yamagawa add(s)
		$sql .= " 	ON ";
		$sql .= " 	 cat_grp_row.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	 AND cat_grp_row.evaluation_id = cat.evaluation_id ";
		$sql .= " 	 AND cat_grp_row.category_id = cat.category_id ";
		// 2012/08/09 Yamagawa add(e)
		// 2012/08/09 Yamagawa del(s)
		//$sql .= " 	, cl_apl_levelup_apply_evaluation_group grp ";
		// 2012/08/09 Yamagawa del(e)
		// 2012/08/09 Yamagawa upd(s)
		//$sql .= " 	,(SELECT ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	(SELECT ";
		// 2012/08/09 Yamagawa upd(e)
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id ";
		$sql .= " 		, item.group_id ";
		$sql .= " 		, count(item.item_id) AS item_cnt ";
		$sql .= " 	FROM ";
		// 2012/08/09 Yamagawa upd(s)
		/*
		$sql .= " 		cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 		, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	WHERE ";
		$sql .= " 		mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 		AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 		AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 		AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 		AND cat.category_id = grp.category_id ";
		$sql .= " 		AND grp.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 		AND grp.evaluation_id = item.evaluation_id ";
		$sql .= " 		AND grp.category_id = item.category_id ";
		$sql .= " 		AND grp.group_id = item.group_id ";
		$sql .= " 		AND cat.disp_flg = 1 ";
		$sql .= " 		AND cat.delete_flg = 0 ";
		$sql .= " 		AND grp.disp_flg = 1 ";
		$sql .= " 		AND grp.delete_flg = 0 ";
		$sql .= " 		AND item.disp_flg = 1 ";
		$sql .= " 		AND item.delete_flg = 0 ";
		*/
		$sql .= " 		cl_apl_levelup_evaluation_value val ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 		ON ";
		$sql .= " 		 mst.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 		 AND mst.evaluation_id = val.evaluation_id ";
		$sql .= " 		 AND mst.delete_flg = 0 ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 		ON ";
		$sql .= " 		 cat.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 		 AND cat.evaluation_id = val.evaluation_id ";
		$sql .= " 		 AND cat.category_id = val.category_id ";
		$sql .= " 		 AND cat.disp_flg = 1 ";
		$sql .= " 		 AND cat.delete_flg = 0 ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 		ON ";
		$sql .= " 		 grp.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 		 AND grp.evaluation_id = val.evaluation_id ";
		$sql .= " 		 AND grp.category_id = val.category_id ";
		$sql .= " 		 AND grp.group_id = val.group_id ";
		$sql .= " 		 AND grp.disp_flg = 1 ";
		$sql .= " 		 AND grp.delete_flg = 0 ";
		$sql .= " 		INNER JOIN ";
		$sql .= " 		 cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 		ON ";
		$sql .= " 		 item.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 		 AND item.evaluation_id = val.evaluation_id ";
		$sql .= " 		 AND item.category_id = val.category_id ";
		$sql .= " 		 AND item.group_id = val.group_id ";
		$sql .= " 		 AND item.item_id = val.item_id ";
		$sql .= " 		 AND item.disp_flg = 1 ";
		$sql .= " 		 AND item.delete_flg = 0 ";
		$sql .= " 		WHERE ";
		$sql .= " 		 val.levelup_evaluation_colleague_id = :levelup_evaluation_colleague_id ";
		// 2012/08/09 Yamagawa upd(e)
		$sql .= " 	group by ";
		$sql .= " 		item.levelup_apply_id ";
		$sql .= " 		, item.level ";
		$sql .= " 		, item.evaluation_id ";
		$sql .= " 		, item.category_id ";
		$sql .= " 		, item.group_id) grprow ";
		// 2012/08/09 Yamagawa add(s)
		$sql .= " 	ON ";
		$sql .= " 	 grprow.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 	 AND grprow.evaluation_id = grp.evaluation_id ";
		$sql .= " 	 AND grprow.category_id = grp.category_id ";
		$sql .= " 	 AND grprow.group_id = grp.group_id ";
		// 2012/08/09 Yamagawa add(e)
		// 2012/08/09 Yamagawa del(s)
		//$sql .= " 	, cl_apl_levelup_apply_evaluation_item item ";
		//$sql .= " 	, cl_apl_levelup_evaluation_value val ";
		// 2012/08/09 Yamagawa del(e)
		// 2012/08/09 Yamagawa add(s)

		// 2012/08/09 Yamagawa add(e)
		$sql .= " WHERE ";
		// 2012/08/09 Yamagawa upd(s)
		/*
		$sql .= " 	mst.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	AND mst.evaluation_id = cat.evaluation_id ";
		$sql .= " 	AND cat.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 	AND cat.evaluation_id = grp.evaluation_id ";
		$sql .= " 	AND cat.category_id = grp.category_id ";
		$sql .= " 	AND cat.levelup_apply_id = cat_item_row.levelup_apply_id ";
		$sql .= " 	AND cat.evaluation_id = cat_item_row.evaluation_id ";
		$sql .= " 	AND cat.category_id = cat_item_row.category_id ";
		$sql .= " 	AND cat.levelup_apply_id = cat_grp_row.levelup_apply_id ";
		$sql .= " 	AND cat.evaluation_id = cat_grp_row.evaluation_id ";
		$sql .= " 	AND cat.category_id = cat_grp_row.category_id ";
		$sql .= " 	AND grp.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 	AND grp.evaluation_id = item.evaluation_id ";
		$sql .= " 	AND grp.category_id = item.category_id ";
		$sql .= " 	AND grp.group_id = item.group_id ";
		$sql .= " 	AND grp.levelup_apply_id = grprow.levelup_apply_id ";
		$sql .= " 	AND grp.evaluation_id = grprow.evaluation_id ";
		$sql .= " 	AND grp.category_id = grprow.category_id ";
		$sql .= " 	AND grp.group_id = grprow.group_id ";
		$sql .= " 	AND item.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	AND item.evaluation_id = val.evaluation_id ";
		$sql .= " 	AND item.category_id = val.category_id ";
		$sql .= " 	AND item.group_id = val.group_id ";
		$sql .= " 	AND item.item_id = val.item_id ";
		$sql .= " 	AND cat.disp_flg = 1 ";
		$sql .= " 	AND cat.delete_flg = 0 ";
		$sql .= " 	AND grp.disp_flg = 1 ";
		$sql .= " 	AND grp.delete_flg = 0 ";
		$sql .= " 	AND item.disp_flg = 1 ";
		$sql .= " 	AND item.delete_flg = 0 ";
		$sql .= " 	AND val.levelup_evaluation_colleague_id = :levelup_evaluation_colleague_id ";
		*/
		$sql .= " 	val.levelup_evaluation_colleague_id = :levelup_evaluation_colleague_id ";
		// 2012/08/09 Yamagawa upd(e)
		$sql .= " ORDER BY ";
		$sql .= " 	mst.disp_order ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, grp.disp_order ";
		$sql .= " 	, item.disp_order ";

		$data=parent::select($sql,array("text"),array("levelup_evaluation_colleague_id"=>$levelup_evaluation_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;

	}

	function getEvaluationDataByApplyId($apply_id){
		$this->log->info(__FUNCTION__." START");

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	eval.levelup_evaluation_id ";
		$sql .= " 	, eval.apply_id ";
		$sql .= " 	, eval.levelup_apply_id ";
		$sql .= " 	, eval.levelup_emp_id ";
		$sql .= " 	, eval.level ";
		$sql .= " 	, eval.self_evaluation_date ";
		$sql .= " 	, eval.supervisor_emp_id ";
		$sql .= " 	, eval.supervisor_evalution_date ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_evaluation eval ";
		$sql .= " WHERE ";
		$sql .= " 	eval.apply_id = :apply_id ";

		$data=parent::select($sql,array("text"),array("apply_id"=>$apply_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;

	}

	function getEvaluationValueByLevelupEvaluationId($levelup_evaluation_id){

		$this->log->info(__FUNCTION__." START");

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	val.levelup_evaluation_value_id ";
		$sql .= " 	, val.evaluation_emp_division ";
		$sql .= " 	, val.levelup_evaluation_id ";
		$sql .= " 	, val.levelup_evaluation_colleague_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.levelup_emp_id ";
		$sql .= " 	, val.evaluation_emp_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, val.group_id ";
		$sql .= " 	, val.item_id ";
		$sql .= " 	, val.value ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_evaluation_value val ";
		// 2012/08/10 Yamagawa upd(s)
		/*
		$sql .= " 	, cl_apl_levelup_evaluation eval ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " WHERE ";
		$sql .= " 	val.levelup_evaluation_id = eval.levelup_evaluation_id ";
		$sql .= " 	AND val.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = mst.evaluation_id ";
		$sql .= " 	AND val.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = cat.evaluation_id ";
		$sql .= " 	AND val.category_id = cat.category_id ";
		$sql .= " 	AND val.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = grp.evaluation_id ";
		$sql .= " 	AND val.category_id = grp.category_id ";
		$sql .= " 	AND val.group_id = grp.group_id ";
		$sql .= " 	AND val.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = item.evaluation_id ";
		$sql .= " 	AND val.category_id = item.category_id ";
		$sql .= " 	AND val.group_id = item.group_id ";
		$sql .= " 	AND val.item_id = item.item_id ";
		$sql .= " 	AND eval.levelup_evaluation_id = :levelup_evaluation_id ";
		*/
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_evaluation eval ";
		$sql .= " 	ON ";
		$sql .= " 	 eval.levelup_evaluation_id = val.levelup_evaluation_id ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 	ON ";
		$sql .= " 	 mst.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	 AND mst.evaluation_id = val.evaluation_id ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	ON ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	ON ";
		$sql .= " 	 grp.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	 AND grp.evaluation_id = val.evaluation_id ";
		$sql .= " 	 AND grp.category_id = val.category_id ";
		$sql .= " 	 AND grp.group_id = val.group_id ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	ON ";
		$sql .= " 	 item.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	 AND item.evaluation_id = val.evaluation_id ";
		$sql .= " 	 AND item.category_id = val.category_id ";
		$sql .= " 	 AND item.group_id = val.group_id ";
		$sql .= " 	 AND item.item_id = val.item_id ";
		$sql .= " WHERE ";
		$sql .= " 	eval.levelup_evaluation_id = :levelup_evaluation_id ";
		// 2012/08/10 Yamagawa upd(e)
		$sql .= " ORDER BY ";
		$sql .= " 	val.evaluation_emp_division ";
		$sql .= " 	, mst.disp_order ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, grp.disp_order ";
		$sql .= " 	, item.disp_order ";


		$data=parent::select($sql,array("text"),array("levelup_evaluation_id"=>$levelup_evaluation_id),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;
	}

	function getEvaluationValueByApplyIdAndDiv($apply_id,$evaluation_emp_division){

		$this->log->info(__FUNCTION__." START");

		$sql = "";
		$sql .= " SELECT ";
		$sql .= " 	val.levelup_evaluation_value_id ";
		$sql .= " 	, val.evaluation_emp_division ";
		$sql .= " 	, val.levelup_evaluation_id ";
		$sql .= " 	, val.levelup_evaluation_colleague_id ";
		$sql .= " 	, val.levelup_apply_id ";
		$sql .= " 	, val.levelup_emp_id ";
		$sql .= " 	, val.evaluation_emp_id ";
		$sql .= " 	, val.evaluation_id ";
		$sql .= " 	, val.category_id ";
		$sql .= " 	, val.group_id ";
		$sql .= " 	, val.item_id ";
		$sql .= " 	, val.value ";
		$sql .= " FROM ";
		$sql .= " 	cl_apl_levelup_evaluation_value val ";
		// 2012/08/10 Yamagawa upd(s)
		/*
		$sql .= " 	, cl_apl_levelup_evaluation eval ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	, cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " WHERE ";
		$sql .= " 	val.levelup_evaluation_id = eval.levelup_evaluation_id ";
		$sql .= " 	AND val.levelup_apply_id = mst.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = mst.evaluation_id ";
		$sql .= " 	AND val.levelup_apply_id = cat.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = cat.evaluation_id ";
		$sql .= " 	AND val.category_id = cat.category_id ";
		$sql .= " 	AND val.levelup_apply_id = grp.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = grp.evaluation_id ";
		$sql .= " 	AND val.category_id = grp.category_id ";
		$sql .= " 	AND val.group_id = grp.group_id ";
		$sql .= " 	AND val.levelup_apply_id = item.levelup_apply_id ";
		$sql .= " 	AND val.evaluation_id = item.evaluation_id ";
		$sql .= " 	AND val.category_id = item.category_id ";
		$sql .= " 	AND val.group_id = item.group_id ";
		$sql .= " 	AND val.item_id = item.item_id ";
		$sql .= " 	AND eval.apply_id = :apply_id ";
		//$sql .= " 	AND val.levelup_apply_id = :apply_id ";
		$sql .= " 	AND val.evaluation_emp_division = :evaluation_emp_division ";
		*/
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_evaluation eval ";
		$sql .= " 	ON ";
		$sql .= " 	 eval.levelup_evaluation_id = val.levelup_evaluation_id ";
		$sql .= " 	 AND eval.apply_id = :apply_id ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation mst ";
		$sql .= " 	ON ";
		$sql .= " 	 mst.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	 AND mst.evaluation_id = val.evaluation_id ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation_category cat ";
		$sql .= " 	ON ";
		$sql .= " 	 cat.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	 AND cat.evaluation_id = val.evaluation_id ";
		$sql .= " 	 AND cat.category_id = val.category_id ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation_group grp ";
		$sql .= " 	ON ";
		$sql .= " 	 grp.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	 AND grp.evaluation_id = val.evaluation_id ";
		$sql .= " 	 AND grp.category_id = val.category_id ";
		$sql .= " 	 AND grp.group_id = val.group_id ";
		$sql .= " 	INNER JOIN ";
		$sql .= " 	 cl_apl_levelup_apply_evaluation_item item ";
		$sql .= " 	ON ";
		$sql .= " 	 item.levelup_apply_id = val.levelup_apply_id ";
		$sql .= " 	 AND item.evaluation_id = val.evaluation_id ";
		$sql .= " 	 AND item.category_id = val.category_id ";
		$sql .= " 	 AND item.group_id = val.group_id ";
		$sql .= " 	 AND item.item_id = val.item_id ";
		$sql .= " WHERE ";
		$sql .= " 	val.evaluation_emp_division = :evaluation_emp_division ";
		// 2012/08/10 Yamagawa upd(e)
		$sql .= " ORDER BY ";
		$sql .= " 	val.evaluation_emp_division ";
		$sql .= " 	, mst.disp_order ";
		$sql .= " 	, cat.disp_order ";
		$sql .= " 	, grp.disp_order ";
		$sql .= " 	, item.disp_order ";


		$data=parent::select($sql,array("text","integer"),array("apply_id"=>$apply_id,"evaluation_emp_division"=>$evaluation_emp_division),MDB2_FETCHMODE_ASSOC);

		$this->log->debug("SQL：".$sql);
		$this->log->debug("結果：".print_r($data,true));
		$this->log->info(__FUNCTION__." END");

		return $data;
	}

}
