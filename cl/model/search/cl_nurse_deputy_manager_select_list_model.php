<?php
/**
 * 看護副部長画面の一覧データ取得用モデル
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_nurse_deputy_manager_select_list_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_nurse_deputy_manager_select_list_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	* 看護副部長表示
	*/
	function get_nurse_deputy_manager_list()
	{
		$this->log->info("get_nurse_deputy_manager_list() START");

		//SQL作成
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " NULL AS row_id ";
		$sql .= " ,CAST(NULL AS SMALLINT) AS class_id ";
		$sql .= " ,CAST(NULL AS SMALLINT) AS atrb_id ";
		$sql .= " ,CAST(NULL AS SMALLINT) AS dept_id ";
		$sql .= " ,CAST(NULL AS SMALLINT) AS room_id ";
		$sql .= " ,0 AS class_division ";
		$sql .= " ,prof.prf_name AS nm ";
		$sql .= " ,manager.nurse_deputy_manager_id AS manager_id ";
		$sql .= " ,manager.emp_id ";
		$sql .= " ,emp.emp_lt_nm ";
		$sql .= " ,emp.emp_ft_nm ";
		$sql .= " ,FALSE AS class_flg ";
		$sql .= " ,FALSE AS atrb_flg ";
		$sql .= " ,FALSE AS dept_flg ";
		$sql .= " ,FALSE AS room_flg ";
		$sql .= "FROM ";
		$sql .= " profile AS prof ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_mst_nurse_deputy_manager manager ";
		$sql .= " ON ";
		$sql .= "  manager.class_division = 0 ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp ";
		$sql .= " ON ";
		$sql .= "  manager.emp_id = emp.emp_id ";
		$sql .= "UNION ALL ";
		$sql .= "SELECT ";
		$sql .= " CAST(class.class_id AS TEXT) AS row_id ";
		$sql .= " ,class.class_id ";
		$sql .= " ,CAST(NULL AS SMALLINT) AS atrb_id ";
		$sql .= " ,CAST(NULL AS SMALLINT) AS dept_id ";
		$sql .= " ,CAST(NULL AS SMALLINT) AS room_id ";
		$sql .= " ,1 AS class_division ";
		$sql .= " ,class.class_nm AS nm ";
		$sql .= " ,manager.nurse_deputy_manager_id AS manager_id ";
		$sql .= " ,manager.emp_id ";
		$sql .= " ,emp.emp_lt_nm ";
		$sql .= " ,emp.emp_ft_nm ";
		$sql .= " ,TRUE AS class_flg ";
		$sql .= " ,FALSE AS atrb_flg ";
		$sql .= " ,FALSE AS dept_flg ";
		$sql .= " ,FALSE AS room_flg ";
		$sql .= "FROM ";
		$sql .= " classmst class ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_mst_nurse_deputy_manager manager ";
		$sql .= " ON ";
		$sql .= "  class.class_id = manager.class_id ";
		$sql .= "  AND manager.class_division = 1 ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp ";
		$sql .= " ON ";
		$sql .= "  manager.emp_id = emp.emp_id ";
		$sql .= "WHERE ";
		$sql .= " class.class_del_flg = FALSE ";
		$sql .= "UNION ALL ";
		$sql .= "SELECT ";
		$sql .= " atrb.class_id || '-' || atrb.atrb_id AS row_id ";
		$sql .= " ,atrb.class_id ";
		$sql .= " ,atrb.atrb_id ";
		$sql .= " ,CAST(NULL AS SMALLINT) AS dept_id ";
		$sql .= " ,CAST(NULL AS SMALLINT) AS room_id ";
		$sql .= " ,2 AS class_division ";
		$sql .= " ,atrb.atrb_nm AS nm ";
		$sql .= " ,manager.nurse_deputy_manager_id AS manager_id ";
		$sql .= " ,manager.emp_id ";
		$sql .= " ,emp.emp_lt_nm ";
		$sql .= " ,emp.emp_ft_nm ";
		$sql .= " ,TRUE AS class_flg ";
		$sql .= " ,TRUE AS atrb_flg ";
		$sql .= " ,FALSE AS dept_flg ";
		$sql .= " ,FALSE AS room_flg ";
		$sql .= "FROM ";
		$sql .= " atrbmst atrb ";
		$sql .= " INNER JOIN ";
		$sql .= "  classmst class ";
		$sql .= " ON ";
		$sql .= "  atrb.class_id = class.class_id ";
		$sql .= "  AND class.class_del_flg = FALSE ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_mst_nurse_deputy_manager manager ";
		$sql .= " ON ";
		$sql .= "  atrb.class_id = manager.class_id ";
		$sql .= "  AND atrb.atrb_id = manager.atrb_id ";
		$sql .= "  AND manager.class_division = 2 ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp ";
		$sql .= " ON ";
		$sql .= "  manager.emp_id = emp.emp_id ";
		$sql .= "WHERE ";
		$sql .= " atrb.atrb_del_flg = FALSE ";
		$sql .= "UNION ALL ";
		$sql .= "SELECT ";
		$sql .= " atrb.class_id || '-' || dept.atrb_id || '-' || dept.dept_id AS row_id ";
		$sql .= " ,atrb.class_id ";
		$sql .= " ,dept.atrb_id ";
		$sql .= " ,dept.dept_id ";
		$sql .= " ,CAST(NULL AS SMALLINT) AS room_id ";
		$sql .= " ,3 AS class_division ";
		$sql .= " ,dept.dept_nm AS nm ";
		$sql .= " ,manager.nurse_deputy_manager_id AS manager_id ";
		$sql .= " ,manager.emp_id ";
		$sql .= " ,emp.emp_lt_nm ";
		$sql .= " ,emp.emp_ft_nm ";
		$sql .= " ,TRUE AS class_flg ";
		$sql .= " ,TRUE AS atrb_flg ";
		$sql .= " ,TRUE AS dept_flg ";
		$sql .= " ,FALSE AS room_flg ";
		$sql .= "FROM ";
		$sql .= " deptmst dept ";
		$sql .= " INNER JOIN ";
		$sql .= "  atrbmst atrb ";
		$sql .= " ON ";
		$sql .= "  dept.atrb_id = atrb.atrb_id ";
		$sql .= "  AND atrb.atrb_del_flg = FALSE ";
		$sql .= " INNER JOIN ";
		$sql .= "  classmst class ";
		$sql .= " ON ";
		$sql .= "  atrb.class_id = class.class_id ";
		$sql .= "  AND class.class_del_flg = FALSE ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_mst_nurse_deputy_manager manager ";
		$sql .= " ON ";
		$sql .= "  atrb.class_id = manager.class_id ";
		$sql .= "  AND dept.atrb_id = manager.atrb_id ";
		$sql .= "  AND dept.dept_id = manager.dept_id ";
		$sql .= "  AND manager.class_division = 3 ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp ";
		$sql .= " ON ";
		$sql .= "  manager.emp_id = emp.emp_id ";
		$sql .= "WHERE ";
		$sql .= " dept.dept_del_flg = FALSE ";
		$sql .= "UNION ALL ";
		$sql .= "SELECT ";
		$sql .= " atrb.class_id || '-' || dept.atrb_id || '-' || room.dept_id || '-' || room.room_id AS row_id ";
		$sql .= " ,atrb.class_id ";
		$sql .= " ,dept.atrb_id ";
		$sql .= " ,room.dept_id ";
		$sql .= " ,room.room_id ";
		$sql .= " ,4 AS class_division ";
		$sql .= " ,room.room_nm AS nm ";
		$sql .= " ,manager.nurse_deputy_manager_id AS manager_id ";
		$sql .= " ,manager.emp_id ";
		$sql .= " ,emp.emp_lt_nm ";
		$sql .= " ,emp.emp_ft_nm ";
		$sql .= " ,TRUE AS class_flg ";
		$sql .= " ,TRUE AS atrb_flg ";
		$sql .= " ,TRUE AS dept_flg ";
		$sql .= " ,TRUE AS room_flg ";
		$sql .= "FROM ";
		$sql .= " classroom room ";
		$sql .= " INNER JOIN ";
		$sql .= "  deptmst dept ";
		$sql .= " ON ";
		$sql .= "  room.dept_id = dept.dept_id ";
		$sql .= "  AND dept.dept_del_flg = FALSE ";
		$sql .= " INNER JOIN ";
		$sql .= "  atrbmst atrb ";
		$sql .= " ON ";
		$sql .= "  dept.atrb_id = atrb.atrb_id ";
		$sql .= "  AND atrb.atrb_del_flg = FALSE ";
		$sql .= " INNER JOIN ";
		$sql .= "  classmst class ";
		$sql .= " ON ";
		$sql .= "  atrb.class_id = class.class_id ";
		$sql .= "  AND class.class_del_flg = FALSE ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_mst_nurse_deputy_manager manager ";
		$sql .= " ON ";
		$sql .= "  atrb.class_id = manager.class_id ";
		$sql .= "  AND dept.atrb_id = manager.atrb_id ";
		$sql .= "  AND room.dept_id = manager.dept_id ";
		$sql .= "  AND room.room_id = manager.room_id ";
		$sql .= "  AND manager.class_division = 4 ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp ";
		$sql .= " ON ";
		$sql .= "  manager.emp_id = emp.emp_id ";
		$sql .= "WHERE ";
		$sql .= " room.room_del_flg = FALSE ";
		$sql .= "ORDER BY ";
		$sql .= " class_flg ";
		$sql .= " ,class_id ";
		$sql .= " ,atrb_flg ";
		$sql .= " ,atrb_id ";
		$sql .= " ,dept_flg ";
		$sql .= " ,dept_id ";
		$sql .= " ,room_flg ";
		$sql .= " ,room_id ";

		$data=parent::select($sql,null,null,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_nurse_deputy_manager_list() END count(data):".count($data));
		return $data;
	}

}
