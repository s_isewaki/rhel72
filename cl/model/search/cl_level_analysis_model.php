<?php
/**
 * レベル分析データ取得用モデル
 */

require_once(dirname(__FILE__) . "/../../../Cmx.php");
require_once(dirname(__FILE__) . "/../../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../cl_common_model.php");
require_once('MDB2.php');

class cl_level_analysis_model extends cl_common_model{

    var $con;
    var $log;
    var $user;

    /**
     * Constractor
     * @param $p_con データベースコネクション
     * @param $p_login_user ログインユーザー
     */
    function cl_level_analysis_model($p_con,$p_login_user) {
        parent::cl_common_model($p_con,$p_login_user);
        $this->con = $p_con;
        $this->user=$p_login_user;
        $this->log = new cl_common_log_class(basename(__FILE__,'.php'));
    }

	/**
	 * 所属長(部署)取得
	 */
	function get_supervisor_class(){

		$this->log->info("get_supervisor_class() START count(data):".count($data));

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " class_division ";
		$sql .= "FROM ";
		$sql .= " cl_mst_supervisor_class ";

		$data=parent::select($sql,null,null,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_supervisor_class() END count(data):".count($data));
		return $data[0];
	}

	/**
	 * 看護部長(部署)取得
	 */
	function get_nurse_manager_class(){

		$this->log->info("get_nurse_manager_class() START count(data):".count($data));

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " class_division ";
		$sql .= "FROM ";
		$sql .= " cl_mst_nurse_manager_class ";

		$data=parent::select($sql,null,null,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_nurse_manager_class() END count(data):".count($data));
		return $data[0];
	}

	/**
	 * 対象部署取得
	 */
	function get_dept($arr_class ,$arr_atrb ,$arr_dept ,$class_division) {

		$this->log->info("get_dept() START");
		$param = array();
		$type = array();

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " dept.dept_id ";
		$sql .= " ,dept.dept_nm ";
		$sql .= "FROM ";
		$sql .= " deptmst dept ";
		$sql .= " LEFT JOIN ";
		$sql .= "  atrbmst atrb ";
		$sql .= " ON ";
		$sql .= "  atrb.atrb_id = dept.atrb_id ";
		$sql .= "  AND atrb.atrb_del_flg = FALSE ";
		$sql .= "WHERE ";
		$sql .= " dept.dept_del_flg = FALSE ";

		if (count($arr_class) > 0) {
			$sql .= "   AND ( ";
			for ($i = 0; $i < count($arr_class); $i++) {
				if ($i > 0) {
					$sql .= "    OR ";
				}
				if ($arr_dept[$i] != '') {
					$sql .= "    dept.dept_id = :emp_dept".$i;
					$param['emp_dept'.$i] = $arr_dept[$i];
					array_push($type,'integer');
				} else if ($arr_atrb[$i] != '') {
					$sql .= "    dept.atrb_id = :emp_attribute".$i;
					$param['emp_attribute'.$i] = $arr_atrb[$i];
					array_push($type,'integer');
				} else if ($arr_class[$i] != '') {
					$sql .= "    atrb.class_id = :emp_class".$i;
					$param['emp_class'.$i] = $arr_class[$i];
					array_push($type,'integer');
				}
			}
			$sql .= "   ) ";
		}

		if ($class_division != '' && $class_division != 0) {
			$sql .= "   AND EXISTS ( ";
			$sql .= "    SELECT ";
			$sql .= "     loginemp.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     empmst loginemp ";
			$sql .= "    WHERE ";
			$sql .= "     loginemp.emp_id = :login_emp_id_mst ";

			switch($class_division) {
				case 1:
					$sql .= "     AND loginemp.emp_class = atrb.class_id ";
					break;
				case 2:
					$sql .= "     AND loginemp.emp_attribute = dept.atrb_id ";
					break;
				case 3:
				case 4:
					$sql .= "     AND loginemp.emp_dept = dept.dept_id ";
					break;
			}

			$sql .= "    UNION ALL ";
			$sql .= "    SELECT ";
			$sql .= "     logincon.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     concurrent logincon ";
			$sql .= "    WHERE ";
			$sql .= "     logincon.emp_id = :login_emp_id_con ";

			switch($class_division) {
				case 1:
					$sql .= "     AND logincon.emp_class = atrb.class_id ";
					break;
				case 2:
					$sql .= "     AND logincon.emp_attribute = dept.atrb_id ";
					break;
				case 3:
				case 4:
					$sql .= "     AND logincon.emp_dept = dept.dept_id ";
					break;
			}

			$sql .= "   ) ";

			$param['login_emp_id_mst'] = $this->user;
			array_push($type,'text');
			$param['login_emp_id_con'] = $this->user;
			array_push($type,'text');
		}

		$sql .= "ORDER BY ";
		$sql .= " dept.order_no ";

		$data=parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_dept() END count(data):".count($data));
		return $data;

	}

	/**
	 * 対象カテゴリ取得
	 */
	function get_category($level) {

		$this->log->info("get_category() START");
		$param = array();
		$type = array();

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " cate.level ";
		$sql .= " ,cate.category_id ";
		$sql .= " ,cate.category_name ";
		$sql .= "FROM ";
		$sql .= " cl_mst_evaluation_category cate ";
		$sql .= "WHERE ";
		$sql .= " 1 = 1 ";

		if ($level != '') {
			$sql .= " AND cate.level = :level ";
			$param['level'] = $level;
			array_push($type,'integer');
		}

		$sql .= "ORDER BY ";
		$sql .= " cate.level ";
		$sql .= " ,cate.evaluation_id ";
		$sql .= " ,cate.disp_order ";

		$data=parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_category() END count(data):".count($data));
		return $data;

	}

	/**
	 * 対象学校取得
	 */
	function get_school() {

		$this->log->info("get_school() START");

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " edu.school_name ";
		$sql .= "FROM ";
		$sql .= " jinji_educational_bg edu ";
		$sql .= "GROUP BY ";
		$sql .= " edu.school_name ";

		$data=parent::select($sql,null,null,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_school() END count(data):".count($data));
		return $data;

	}

	/**
	 * 部署別人数取得
	 */
	function get_item_unit_emp_cnt(
		$job
		, $level
		, $arr_class
		, $arr_atrb
		, $arr_dept
		, $arr_room
		, $year_from
		, $year_to
		, $class_division
		, $mode
		, $row_1_key
		, $row_2_key
		, $column_1_key
		, $column_2_key
	){

		$this->log->info("get_item_unit_emp_cnt() START");
		$param = array();
		$type = array();

		$sql  = "";
		$sql .= "SELECT ";

		if ($mode == 'analysis') {
			$sql .= "  emp.emp_dept ";
			$sql .= "  ,COALESCE(pro.now_level, 0) AS now_level ";
			$sql .= "  ,count(emp.emp_id) AS emp_cnt ";
		} else if ($mode == 'emp_list') {
			$sql .= " emp.emp_lt_nm ";
			$sql .= " ,emp.emp_ft_nm ";
			$sql .= " ,class.class_nm ";
			$sql .= " ,atrb.atrb_nm ";
			$sql .= " ,dept.dept_nm ";
			$sql .= " ,room.room_nm ";
		}

		$sql .= "  FROM ";
		$sql .= "   empmst emp ";

		// 削除されている部署に所属する職員は対象外
		$sql .= "   INNER JOIN ";
		$sql .= "    deptmst dept ";
		$sql .= "   ON ";
		$sql .= "    dept.dept_id = emp.emp_dept ";
		$sql .= "    AND dept.dept_del_flg = FALSE ";

		// 2012/08/31 Yamagawa add(s)
		$sql .= "   INNER JOIN ";
		$sql .= "    jobmst job ";
		$sql .= "   ON ";
		$sql .= "    job.job_id = emp.emp_job ";
		$sql .= "    AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/31 Yamagawa add(e)

		$sql .= "   LEFT JOIN ";
		$sql .= "    cl_personal_profile pro ";
		$sql .= "   ON ";
		$sql .= "    pro.emp_id = emp.emp_id ";

		if ($mode == 'emp_list') {
			$sql .= "   LEFT JOIN ";
			$sql .= "    classmst class ";
			$sql .= "   ON ";
			$sql .= "    class.class_id = emp.emp_class ";
			$sql .= "   LEFT JOIN ";
			$sql .= "    atrbmst atrb ";
			$sql .= "   ON ";
			$sql .= "    atrb.atrb_id = emp.emp_attribute ";
			$sql .= "   LEFT JOIN ";
			$sql .= "    classroom room ";
			$sql .= "   ON ";
			$sql .= "    room.room_id = emp.emp_room ";
		}

		$sql .= "  WHERE ";
		$sql .= "   1 = 1 ";

		if ($job != '') {
			$sql .= "   AND emp.emp_job = :emp_job ";
			$param['emp_job'] = $job;
			array_push($type,'integer');
		}

		if (count($arr_class) > 0) {
			$sql .= "   AND ( ";
			for ($i = 0; $i < count($arr_class); $i++) {
				if ($i > 0) {
					$sql .= "    OR ";
				}
				if ($arr_room[$i] != '') {
					$sql .= "    emp.emp_room = :emp_room".$i;
					$param['emp_room'.$i] = $arr_room[$i];
					array_push($type,'integer');
				} else if ($arr_dept[$i] != '') {
					$sql .= "    emp.emp_dept = :emp_dept".$i;
					$param['emp_dept'.$i] = $arr_dept[$i];
					array_push($type,'integer');
				} else if ($arr_atrb[$i] != '') {
					$sql .= "    emp.emp_attribute = :emp_attribute".$i;
					$param['emp_attribute'.$i] = $arr_atrb[$i];
					array_push($type,'integer');
				} else if ($arr_class[$i] != '') {
					$sql .= "    emp.emp_class = :emp_class".$i;
					$param['emp_class'.$i] = $arr_class[$i];
					array_push($type,'integer');
				}
			}
			$sql .= "   ) ";
		}

		if ($class_division != '' && $class_division != 0) {
			$sql .= "   AND EXISTS ( ";
			$sql .= "    SELECT ";
			$sql .= "     loginemp.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     empmst loginemp ";
			$sql .= "    WHERE ";
			$sql .= "     loginemp.emp_id = :login_emp_id_mst ";

			switch($class_division) {
				case 1:
					$sql .= "     AND loginemp.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND loginemp.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND loginemp.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND loginemp.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "    UNION ALL ";
			$sql .= "    SELECT ";
			$sql .= "     logincon.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     concurrent logincon ";
			$sql .= "    WHERE ";
			$sql .= "     logincon.emp_id = :login_emp_id_con ";

			switch($class_division) {
				case 1:
					$sql .= "     AND logincon.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND logincon.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND logincon.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND logincon.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "   ) ";

			$param['login_emp_id_mst'] = $this->user;
			array_push($type,'text');
			$param['login_emp_id_con'] = $this->user;
			array_push($type,'text');
		}

		if ($level != '') {
			$sql .= "    AND pro.now_level = :now_level ";
			$param['now_level'] = $level;
			array_push($type,'integer');
		}

		// 2012/08/30 Yamagawa upd(s)
		/*
		$sql .= "   AND EXISTS( ";
		$sql .= "    SELECT ";
		$sql .= "     auth.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     authmst auth ";
		$sql .= "    WHERE ";
		$sql .= "     auth.emp_id = emp.emp_id ";
		$sql .= "     AND auth.emp_del_flg = FALSE ";
		$sql .= "   ) ";
		*/
		$sql .= "   AND ( ";
		$sql .= "    emp.emp_retire is null ";
		$sql .= "    OR trim(emp.emp_retire) = '' ";
		$sql .= "    OR to_date(trim(emp.emp_retire),'yyyymmdd') >= current_date ";
		$sql .= "   ) ";
		// 2012/08/30 Yamagawa upd(e)

		if ($mode == 'emp_list') {

			if ($row_1_key != 'total') {
				$sql .= " AND emp.emp_dept = :row_1_key ";
				$param['row_1_key'] = $row_1_key;
				array_push($type,'integer');
			}

			if ($row_2_key != 'total') {
				// 未使用
			}

			if ($column_1_key != 'total') {
				$sql .= " AND COALESCE(pro.now_level, 0) = :column_1_key ";
				$param['column_1_key'] = $column_1_key;
				array_push($type,'integer');
			}

			if ($column_2_key != 'total') {
				// 未使用
			}

		}

		if ($mode == 'analysis') {
			$sql .= " GROUP BY ";
			$sql .= "  emp.emp_dept ";
			$sql .= "  ,COALESCE(pro.now_level, 0) ";
		} else if ($mode == 'emp_list') {
			$sql .= " ORDER BY ";
			$sql .= "  emp.emp_class ";
			$sql .= "  ,emp.emp_attribute ";
			$sql .= "  ,emp.emp_dept ";
			$sql .= "  ,emp.emp_room ";
			$sql .= "  ,emp.emp_id ";
		}

		$data=parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_item_unit_emp_cnt() END count(data):".count($data));
		return $data;

	}

	/**
	 * 部署年度別人数　職員取得
	 */
	function get_emp_item_unit_year_emp_cnt(
		$job
		, $level
		, $arr_class
		, $arr_atrb
		, $arr_dept
		, $arr_room
		, $year_from
		, $year_to
		, $class_division
		, $mode
		, $row_1_key
		, $row_2_key
		, $column_1_key
		, $column_2_key
	){

		$this->log->info("get_empitem_unit_year_emp_cnt() START");
		$param = array();
		$type = array();

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " emp.emp_id ";
		$sql .= " ,emp.emp_class ";
		$sql .= " ,emp.emp_attribute ";
		$sql .= " ,emp.emp_dept ";
		$sql .= " ,emp.emp_room ";
		$sql .= " ,emp.emp_job ";
		$sql .= " ,CASE ";
		$sql .= "  WHEN trim(emp.emp_join) = '' ";
		$sql .= "  THEN CAST(NULL AS INTEGER) ";
		$sql .= "  WHEN extract(MONTH FROM to_date(emp.emp_join,'YYYYMMDD')) BETWEEN 1 AND 3 ";
		$sql .= "  THEN extract(YEAR FROM to_date(emp.emp_join,'YYYYMMDD')) - 1 ";
		$sql .= "  ELSE extract(YEAR FROM to_date(emp.emp_join,'YYYYMMDD')) ";
		$sql .= " END AS join_year ";
		$sql .= " ,CASE ";
		$sql .= "  WHEN trim(emp.emp_retire) = '' ";
		$sql .= "  THEN CAST(NULL AS INTEGER) ";
		// 2012/09/03 Yamagawa upd(s)
		//$sql .= "  WHEN to_char(to_date(emp.emp_retire,'YYYYMMDD'),'MMDD') >= '0331' ";
		$sql .= "  WHEN to_char(to_date(emp.emp_retire,'YYYYMMDD'),'MMDD') >= '0401' ";
		// 2012/09/03 Yamagawa upd(e)
		$sql .= "  THEN extract(YEAR FROM to_date(emp.emp_retire,'YYYYMMDD')) ";
		$sql .= "  ELSE extract(YEAR FROM to_date(emp.emp_retire,'YYYYMMDD')) - 1 ";
		$sql .= " END AS retire_year ";
		// 2012/09/03 Yamagawa upd(s)
		/*
		$sql .= " ,CASE ";
		$sql .= "  WHEN to_char(pro.get_level1_date,'MMDD') > '0401' ";
		$sql .= "  THEN extract(YEAR FROM pro.get_level1_date) - 1 ";
		$sql .= "  ELSE extract(YEAR FROM pro.get_level1_date) - 2 ";
		$sql .= " END AS get_level1_year ";
		$sql .= " ,CASE ";
		$sql .= "  WHEN to_char(pro.get_level2_date,'MMDD') > '0401' ";
		$sql .= "  THEN extract(YEAR FROM pro.get_level2_date) - 1 ";
		$sql .= "  ELSE extract(YEAR FROM pro.get_level2_date) - 2 ";
		$sql .= " END AS get_level2_year ";
		$sql .= " ,CASE ";
		$sql .= "  WHEN to_char(pro.get_level3_date,'MMDD') > '0401' ";
		$sql .= "  THEN extract(YEAR FROM pro.get_level3_date) - 1 ";
		$sql .= "  ELSE extract(YEAR FROM pro.get_level3_date) - 2 ";
		$sql .= " END AS get_level3_year ";
		$sql .= " ,CASE ";
		$sql .= "  WHEN to_char(pro.get_level4_date,'MMDD') > '0401' ";
		$sql .= "  THEN extract(YEAR FROM pro.get_level4_date) - 1 ";
		$sql .= "  ELSE extract(YEAR FROM pro.get_level4_date) - 2 ";
		$sql .= " END AS get_level4_year ";
		$sql .= " ,CASE ";
		$sql .= "  WHEN to_char(pro.get_level5_date,'MMDD') > '0401' ";
		$sql .= "  THEN extract(YEAR FROM pro.get_level5_date) - 1 ";
		$sql .= "  ELSE extract(YEAR FROM pro.get_level5_date) - 2 ";
		$sql .= " END AS get_level5_year ";
		*/
		$sql .= " ,CASE ";
		$sql .= "  WHEN to_char(pro.get_level1_date,'MMDD') > '0401' ";
		$sql .= "  THEN extract(YEAR FROM pro.get_level1_date) + 1 ";
		$sql .= "  ELSE extract(YEAR FROM pro.get_level1_date) ";
		$sql .= " END AS get_level1_year ";
		$sql .= " ,CASE ";
		$sql .= "  WHEN to_char(pro.get_level2_date,'MMDD') > '0401' ";
		$sql .= "  THEN extract(YEAR FROM pro.get_level2_date) + 1 ";
		$sql .= "  ELSE extract(YEAR FROM pro.get_level2_date) ";
		$sql .= " END AS get_level2_year ";
		$sql .= " ,CASE ";
		$sql .= "  WHEN to_char(pro.get_level3_date,'MMDD') > '0401' ";
		$sql .= "  THEN extract(YEAR FROM pro.get_level3_date) + 1 ";
		$sql .= "  ELSE extract(YEAR FROM pro.get_level3_date) ";
		$sql .= " END AS get_level3_year ";
		$sql .= " ,CASE ";
		$sql .= "  WHEN to_char(pro.get_level4_date,'MMDD') > '0401' ";
		$sql .= "  THEN extract(YEAR FROM pro.get_level4_date) + 1 ";
		$sql .= "  ELSE extract(YEAR FROM pro.get_level4_date) ";
		$sql .= " END AS get_level4_year ";
		$sql .= " ,CASE ";
		$sql .= "  WHEN to_char(pro.get_level5_date,'MMDD') > '0401' ";
		$sql .= "  THEN extract(YEAR FROM pro.get_level5_date) + 1 ";
		$sql .= "  ELSE extract(YEAR FROM pro.get_level5_date) ";
		$sql .= " END AS get_level5_year ";
		// 2012/09/03 Yamagawa upd(e)
		$sql .= " ,CASE ";
		$sql .= "  WHEN dept.dept_id IS NULL ";
		$sql .= "  THEN FALSE ";
		$sql .= "  ELSE TRUE ";
		$sql .= " END AS dept_exists ";

		if (count($arr_class) > 0) {
			$sql .= " ,CASE ";
			$sql .= "  WHEN ";
			for ($i = 0; $i < count($arr_class); $i++) {
				if ($i > 0) {
					$sql .= "    OR ";
				}
				if ($arr_room[$i] != '') {
					$sql .= "    emp.emp_room = :emp_room".$i;
					$param['emp_room'.$i] = $arr_room[$i];
					array_push($type,'integer');
				} else if ($arr_dept[$i] != '') {
					$sql .= "    emp.emp_dept = :emp_dept".$i;
					$param['emp_dept'.$i] = $arr_dept[$i];
					array_push($type,'integer');
				} else if ($arr_atrb[$i] != '') {
					$sql .= "    emp.emp_attribute = :emp_attribute".$i;
					$param['emp_attribute'.$i] = $arr_atrb[$i];
					array_push($type,'integer');
				} else if ($arr_class[$i] != '') {
					$sql .= "    emp.emp_class = :emp_class".$i;
					$param['emp_class'.$i] = $arr_class[$i];
					array_push($type,'integer');
				}
			}
			$sql .= " THEN ";
			$sql .= "  TRUE ";
			$sql .= " ELSE ";
			$sql .= "  FALSE ";
			$sql .= " END AS affiliation ";
		} else {
			$sql .= " ,TRUE AS affiliation ";
		}

		if ($class_division != '' && $class_division != 0) {
			$sql .= " ,CASE ";
			$sql .= "  WHEN ";
			$sql .= "   loginemp.emp_id IS NULL ";
			$sql .= "   AND logincon.emp_id IS NULL ";
			$sql .= "  THEN ";
			$sql .= "   FALSE ";
			$sql .= "  ELSE ";
			$sql .= "   TRUE ";
			$sql .= " END AS login_reference ";
		} else {
			$sql .= " ,TRUE AS login_reference ";
		}

		if ($mode == 'emp_list') {
			$sql .= " ,emp.emp_lt_nm ";
			$sql .= " ,emp.emp_ft_nm ";
			$sql .= " ,class.class_nm ";
			$sql .= " ,atrb.atrb_nm ";
			$sql .= " ,dept.dept_nm ";
			$sql .= " ,room.room_nm ";
		}

		$sql .= "FROM ";
		$sql .= " empmst emp ";

		// 2012/08/31 Yamagawa add(s)
		$sql .= " INNER JOIN ";
		$sql .= "  jobmst job ";
		$sql .= " ON ";
		$sql .= "  job.job_id = emp.emp_job ";
		$sql .= "  AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/31 Yamagawa add(e)

		$sql .= " LEFT JOIN ";
		$sql .= "  cl_personal_profile pro ";
		$sql .= " ON ";
		$sql .= "  pro.emp_id = emp.emp_id ";

		if ($class_division != '' && $class_division != 0) {
			$sql .= " LEFT JOIN ";
			$sql .= "  empmst loginemp ";
			$sql .= " ON ";
			$sql .= "  loginemp.emp_id = :login_emp_id_mst ";

			switch($class_division) {
				case 1:
					$sql .= "  AND loginemp.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "  AND loginemp.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "  AND loginemp.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "  AND loginemp.emp_room = emp.emp_room ";
					break;
			}

			$sql .= " LEFT JOIN ";
			$sql .= "  concurrent logincon ";
			$sql .= " ON ";
			$sql .= "  logincon.emp_id = :login_emp_id_con ";

			switch($class_division) {
				case 1:
					$sql .= "  AND logincon.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "  AND logincon.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "  AND logincon.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "  AND logincon.emp_room = emp.emp_room ";
					break;
			}

			$param['login_emp_id_mst'] = $this->user;
			array_push($type,'text');
			$param['login_emp_id_con'] = $this->user;
			array_push($type,'text');
		}

		$sql .= " LEFT JOIN ";
		$sql .= "  deptmst dept ";
		$sql .= " ON ";
		$sql .= "  dept.dept_id = emp.emp_dept ";
		$sql .= "  AND dept.dept_del_flg = FALSE ";

		if ($mode == 'emp_list') {
			$sql .= " LEFT JOIN ";
			$sql .= "  classmst class ";
			$sql .= " ON ";
			$sql .= "  class.class_id = emp.emp_class ";
			$sql .= " LEFT JOIN ";
			$sql .= "  atrbmst atrb ";
			$sql .= " ON ";
			$sql .= "  atrb.atrb_id = emp.emp_attribute ";
			$sql .= " LEFT JOIN ";
			$sql .= "  classroom room ";
			$sql .= " ON ";
			$sql .= "  room.room_id = emp.emp_room ";
			$sql .= "ORDER BY ";
			$sql .= " emp.emp_class ";
			$sql .= " ,emp.emp_attribute ";
			$sql .= " ,emp.emp_dept ";
			$sql .= " ,emp.emp_room ";
			$sql .= " ,emp.emp_id ";
		}

		$data=parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_empitem_unit_year_emp_cnt() END count(data):".count($data));
		return $data;

	}

	/**
	 * 部署年度別人数　所属履歴取得
	 */
	function get_class_history_item_unit_year_emp_cnt(
		$job
		, $level
		, $arr_class
		, $arr_atrb
		, $arr_dept
		, $arr_room
		, $year_from
		, $year_to
		, $class_division
		, $mode
		, $row_1_key
		, $row_2_key
		, $column_1_key
		, $column_2_key
	){

		$this->log->info("get_class_history_item_unit_year_emp_cnt() START");
		$param = array();
		$type = array();

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " his.emp_id ";
		$sql .= " ,hist_wk2.histyear ";
		$sql .= " ,his.class_id ";
		$sql .= " ,his.atrb_id ";
		$sql .= " ,his.dept_id ";
		$sql .= " ,his.room_id ";
		$sql .= " ,CASE ";
		$sql .= "  WHEN dept.dept_id IS NULL ";
		$sql .= "  THEN FALSE ";
		$sql .= "  ELSE TRUE ";
		$sql .= " END AS dept_exists ";

		if (count($arr_class) > 0) {
			$sql .= " ,CASE ";
			$sql .= "  WHEN ";
			for ($i = 0; $i < count($arr_class); $i++) {
				if ($i > 0) {
					$sql .= "    OR ";
				}
				if ($arr_room[$i] != '') {
					$sql .= "    his.room_id = :emp_room".$i;
					$param['emp_room'.$i] = $arr_room[$i];
					array_push($type,'integer');
				} else if ($arr_dept[$i] != '') {
					$sql .= "    his.dept_id = :emp_dept".$i;
					$param['emp_dept'.$i] = $arr_dept[$i];
					array_push($type,'integer');
				} else if ($arr_atrb[$i] != '') {
					$sql .= "    his.atrb_id = :emp_attribute".$i;
					$param['emp_attribute'.$i] = $arr_atrb[$i];
					array_push($type,'integer');
				} else if ($arr_class[$i] != '') {
					$sql .= "    his.class_id = :emp_class".$i;
					$param['emp_class'.$i] = $arr_class[$i];
					array_push($type,'integer');
				}
			}
			$sql .= " THEN ";
			$sql .= "  TRUE ";
			$sql .= " ELSE ";
			$sql .= "  FALSE ";
			$sql .= " END AS affiliation ";
		} else {
			$sql .= " ,TRUE AS affiliation ";
		}

		if ($class_division != '' && $class_division != 0) {
			$sql .= " ,CASE ";
			$sql .= "  WHEN ";
			$sql .= "   loginemp.emp_id IS NULL ";
			$sql .= "   AND logincon.emp_id IS NULL ";
			$sql .= "  THEN ";
			$sql .= "   FALSE ";
			$sql .= "  ELSE ";
			$sql .= "   TRUE ";
			$sql .= " END AS login_reference ";
		} else {
			$sql .= " ,TRUE AS login_reference ";
		}

		$sql .= "FROM ";
		$sql .= " (SELECT ";
		$sql .= "  hist_wk.emp_id ";
		$sql .= "  ,hist_wk.histyear ";
		$sql .= "  ,MIN(hist_wk.histdate) AS histdate ";
		$sql .= " FROM ";
		$sql .= "  (SELECT ";
		$sql .= "   emp.emp_id ";
		$sql .= "   ,CASE ";
		// 2012/09/03 Yamagawa upd(s)
		/*
		$sql .= "    WHEN to_char(his.histdate,'MMDD') > '0331' ";
		$sql .= "    THEN extract(YEAR FROM his.histdate) - 1 ";
		$sql .= "    ELSE extract(YEAR FROM his.histdate) - 2 ";
		*/
		$sql .= "    WHEN to_char(his.histdate,'MMDD') > '0401' ";
		$sql .= "    THEN extract(YEAR FROM his.histdate) + 1 ";
		$sql .= "    ELSE extract(YEAR FROM his.histdate) ";
		// 2012/09/03 Yamagawa upd(e)
		$sql .= "   END AS histyear ";
		$sql .= "   ,his.histdate ";
		$sql .= "  FROM ";
		$sql .= "   empmst emp ";
		// 2012/08/31 Yamagawa add(s)
		$sql .= "   INNER JOIN ";
		$sql .= "    jobmst job ";
		$sql .= "   ON ";
		$sql .= "    job.job_id = emp.emp_job ";
		$sql .= "    AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/31 Yamagawa add(e)
		$sql .= "   INNER JOIN ";
		$sql .= "    class_history his ";
		$sql .= "   ON ";
		$sql .= "    his.emp_id = emp.emp_id) hist_wk ";
		$sql .= " WHERE ";
		$sql .= "  1 = 1 ";

		$sql .= "  AND hist_wk.histyear >= :year_from ";
		$param['year_from'] = $year_from;
		array_push($type,'integer');

		$sql .= " GROUP BY ";
		$sql .= "  hist_wk.emp_id ";
		$sql .= "  ,hist_wk.histyear) hist_wk2 ";
		$sql .= " INNER JOIN ";
		$sql .= "  class_history his ";
		$sql .= " ON ";
		$sql .= "  his.emp_id = hist_wk2.emp_id ";
		$sql .= "  AND his.histdate = hist_wk2.histdate ";

		if ($class_division != '' && $class_division != 0) {
			$sql .= " LEFT JOIN ";
			$sql .= "  empmst loginemp ";
			$sql .= " ON ";
			$sql .= "  loginemp.emp_id = :login_emp_id_mst ";

			switch($class_division) {
				case 1:
					$sql .= "  AND loginemp.emp_class = his.class_id ";
					break;
				case 2:
					$sql .= "  AND loginemp.emp_attribute = his.atrb_id ";
					break;
				case 3:
					$sql .= "  AND loginemp.emp_dept = his.dept_id ";
					break;
				case 4:
					$sql .= "  AND loginemp.emp_room = his.room_id ";
					break;
			}

			$sql .= " LEFT JOIN ";
			$sql .= "  concurrent logincon ";
			$sql .= " ON ";
			$sql .= "  logincon.emp_id = :login_emp_id_con ";

			switch($class_division) {
				case 1:
					$sql .= "  AND logincon.emp_class = his.class_id ";
					break;
				case 2:
					$sql .= "  AND logincon.emp_attribute = his.atrb_id ";
					break;
				case 3:
					$sql .= "  AND logincon.emp_dept = his.dept_id ";
					break;
				case 4:
					$sql .= "  AND logincon.emp_room = his.room_id ";
					break;
			}

			$param['login_emp_id_mst'] = $this->user;
			array_push($type,'text');
			$param['login_emp_id_con'] = $this->user;
			array_push($type,'text');
		}

		$sql .= " LEFT JOIN ";
		$sql .= "  deptmst dept ";
		$sql .= " ON ";
		$sql .= "  dept.dept_id = his.dept_id ";
		$sql .= "  AND dept.dept_del_flg = FALSE ";

		$sql .= "ORDER BY ";
		$sql .= " his.emp_id ";
		$sql .= " ,hist_wk2.histyear DESC ";

		$data=parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_class_history_item_unit_year_emp_cnt() END count(data):".count($data));
		return $data;

	}

	/**
	 * 部署年度別人数　職種履歴取得
	 */
	function get_job_history_item_unit_year_emp_cnt(
		$job
		, $level
		, $arr_class
		, $arr_atrb
		, $arr_dept
		, $arr_room
		, $year_from
		, $year_to
		, $class_division
		, $mode
		, $row_1_key
		, $row_2_key
		, $column_1_key
		, $column_2_key
	){

		$this->log->info("get_job_history_item_unit_year_emp_cnt() START");
		$param = array("year_from" => $year_from);
		$type = array("integer");

		$sql  = "";
		$sql .= "SELECT ";
		$sql .= " his.emp_id ";
		$sql .= " ,hist_wk2.histyear ";
		$sql .= " ,his.job_id ";
		$sql .= "FROM ";
		$sql .= " (SELECT ";
		$sql .= "  hist_wk.emp_id ";
		$sql .= "  ,hist_wk.histyear ";
		$sql .= "  ,MIN(hist_wk.histdate) AS histdate ";
		$sql .= " FROM ";
		$sql .= "  (SELECT ";
		$sql .= "   emp.emp_id ";
		$sql .= "   ,CASE ";
		// 2012/09/03 Yamagawa upd(s)
		/*
		$sql .= "    WHEN to_char(his.histdate,'MMDD') > '0331' ";
		$sql .= "    THEN extract(YEAR FROM his.histdate) - 1 ";
		$sql .= "    ELSE extract(YEAR FROM his.histdate) - 2 ";
		*/
		$sql .= "    WHEN to_char(his.histdate,'MMDD') > '0401' ";
		$sql .= "    THEN extract(YEAR FROM his.histdate) + 1 ";
		$sql .= "    ELSE extract(YEAR FROM his.histdate) ";
		// 2012/09/03 Yamagawa upd(e)
		$sql .= "   END AS histyear ";
		$sql .= "   ,his.histdate ";
		$sql .= "  FROM ";
		$sql .= "   empmst emp ";
		// 2012/08/31 Yamagawa add(s)
		$sql .= "   INNER JOIN ";
		$sql .= "    jobmst job ";
		$sql .= "   ON ";
		$sql .= "    job.job_id = emp.emp_job ";
		$sql .= "    AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/31 Yamagawa add(e)
		$sql .= "   INNER JOIN ";
		$sql .= "    job_history his ";
		$sql .= "   ON ";
		$sql .= "    his.emp_id = emp.emp_id) hist_wk ";
		$sql .= " WHERE ";
		$sql .= "  1 = 1 ";
		$sql .= "  AND hist_wk.histyear >= :year_from ";
		$sql .= " GROUP BY ";
		$sql .= "  hist_wk.emp_id ";
		$sql .= "  ,hist_wk.histyear) hist_wk2 ";
		$sql .= " INNER JOIN ";
		$sql .= "  job_history his ";
		$sql .= " ON ";
		$sql .= "  his.emp_id = hist_wk2.emp_id ";
		$sql .= "  AND his.histdate = hist_wk2.histdate ";
		$sql .= "ORDER BY ";
		$sql .= " his.emp_id ";
		$sql .= " ,hist_wk2.histyear DESC ";

		$data=parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_job_history_item_unit_year_emp_cnt() END count(data):".count($data));
		return $data;

	}

	/**
	 * 部署認定年度別人数取得
	 */
	function get_item_unit_recognition_year_emp_cnt(
		$job
		, $level
		, $arr_class
		, $arr_atrb
		, $arr_dept
		, $arr_room
		, $year_from
		, $year_to
		, $class_division
		, $mode
		, $row_1_key
		, $row_2_key
		, $column_1_key
		, $column_2_key
	){

		$this->log->info("get_item_unit_recognition_year_emp_cnt() START");
		$param = array();
		$type = array();
		$get_level_count = ($level == '') ? 5 : 1;

		$sql  = "";
		$sql .= "SELECT ";
		if ($mode == 'analysis') {
			$sql .= " wk.emp_get_level_dept AS emp_dept ";
			$sql .= " ,wk.level ";
			$sql .= " ,wk.get_year ";
			$sql .= " ,count(wk.emp_get_level_dept) AS cnt ";
		} else if ($mode == 'emp_list') {
			$sql .= " wk.emp_lt_nm ";
			$sql .= " ,wk.emp_ft_nm ";
			$sql .= " ,class.class_nm ";
			$sql .= " ,atrb.atrb_nm ";
			$sql .= " ,dept.dept_nm ";
			$sql .= " ,room.room_nm ";
		}
		$sql .= "FROM ";
		$sql .= " ( ";

		for ($j = 1; $j <= $get_level_count; $j++) {

			if ($j != 1) {
				$sql .= " UNION ALL ";
			}

			if ($level == '') {
				$get_level = $j;
			} else {
				$get_level = $level;
			}

			$sql .= " SELECT ";
			$sql .= "  pro.get_level".$get_level."_dept AS emp_get_level_dept ";
			$sql .= "  ,".$get_level." AS level ";
			$sql .= "  ,CASE ";
			$sql .= "    WHEN extract(MONTH FROM pro.get_level".$get_level."_date) BETWEEN 1 AND 3 ";
			$sql .= "    THEN extract(YEAR FROM pro.get_level".$get_level."_date) - 1 ";
			$sql .= "    ELSE extract(YEAR FROM pro.get_level".$get_level."_date) ";
			$sql .= "   END AS get_year ";
			$sql .= "  ,emp.emp_class ";
			$sql .= "  ,emp.emp_attribute AS emp_atrb ";
			$sql .= "  ,emp.emp_dept ";
			$sql .= "  ,emp.emp_room ";
			$sql .= "  ,emp.emp_id ";
			$sql .= "  ,emp.emp_lt_nm ";
			$sql .= "  ,emp.emp_ft_nm ";
			$sql .= " FROM ";
			$sql .= "  cl_personal_profile pro ";
			$sql .= "  INNER JOIN ";
			$sql .= "   empmst emp ";
			$sql .= "  ON ";
			$sql .= "   emp.emp_id = pro.emp_id ";

			if ($job != '') {
				$sql .= "    AND emp.emp_job = ".$job;
			}

			// 削除されている部署に所属する職員は対象外
			$sql .= "   INNER JOIN ";
			$sql .= "    deptmst dept ";
			$sql .= "   ON ";
			$sql .= "    dept.dept_id = emp.emp_dept ";
			$sql .= "    AND dept.dept_del_flg = FALSE ";

			// 2012/08/31 Yamagawa add(s)
			$sql .= "   INNER JOIN ";
			$sql .= "    jobmst job ";
			$sql .= "   ON ";
			$sql .= "    job.job_id = emp.emp_job ";
			$sql .= "    AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
			// 2012/08/31 Yamagawa add(e)

			$sql .= " WHERE ";
			$sql .= "  1 = 1 ";

			if (count($arr_class) > 0) {
				$sql .= "   AND ( ";
				for ($i = 0; $i < count($arr_class); $i++) {
					if ($i > 0) {
						$sql .= "    OR ";
					}
					if ($arr_room[$i] != '') {
						$sql .= "    pro.get_level".$get_level."_room = ".$arr_room[$i];
					} else if ($arr_dept[$i] != '') {
						$sql .= "    pro.get_level".$get_level."_dept = ".$arr_dept[$i];
					} else if ($arr_atrb[$i] != '') {
						$sql .= "    pro.get_level".$get_level."_atrb = ".$arr_atrb[$i];
					} else if ($arr_class[$i] != '') {
						$sql .= "    pro.get_level".$get_level."_class = ".$arr_class[$i];
					}
				}
				$sql .= "   ) ";
			}

			if ($class_division != '' && $class_division != 0) {
				$sql .= "   AND EXISTS ( ";
				$sql .= "    SELECT ";
				$sql .= "     loginemp.emp_id ";
				$sql .= "    FROM ";
				$sql .= "     empmst loginemp ";
				$sql .= "    WHERE ";
				$sql .= "     loginemp.emp_id = '".$this->user."' ";

				switch($class_division) {
					case 1:
						$sql .= "     AND loginemp.emp_class = pro.get_level".$get_level."_class ";
						break;
					case 2:
						$sql .= "     AND loginemp.emp_attribute = pro.get_level".$get_level."_atrb ";
						break;
					case 3:
						$sql .= "     AND loginemp.emp_dept = pro.get_level".$get_level."_dept ";
						break;
					case 4:
						$sql .= "     AND loginemp.emp_room = pro.get_level".$get_level."_room ";
						break;
				}

				$sql .= "    UNION ALL ";
				$sql .= "    SELECT ";
				$sql .= "     logincon.emp_id ";
				$sql .= "    FROM ";
				$sql .= "     concurrent logincon ";
				$sql .= "    WHERE ";
				$sql .= "     logincon.emp_id = '".$this->user."' ";

				switch($class_division) {
					case 1:
						$sql .= "     AND logincon.emp_class = pro.get_level".$get_level."_class ";
						break;
					case 2:
						$sql .= "     AND logincon.emp_attribute = pro.get_level".$get_level."_atrb ";
						break;
					case 3:
						$sql .= "     AND logincon.emp_dept = pro.get_level".$get_level."_dept ";
						break;
					case 4:
						$sql .= "     AND logincon.emp_room = pro.get_level".$get_level."_room ";
						break;
				}
				$sql .= "   ) ";
			}

			$sql .= "  AND ";
			$sql .= "   CASE ";
			$sql .= "    WHEN extract(MONTH FROM pro.get_level".$get_level."_date) BETWEEN 1 AND 3 ";
			$sql .= "    THEN extract(YEAR FROM pro.get_level".$get_level."_date) - 1 ";
			$sql .= "    ELSE extract(YEAR FROM pro.get_level".$get_level."_date) ";
			$sql .= "   END BETWEEN ".$year_from." AND ".$year_to;

			// 2012/08/30 Yamagawa del(s)
			/*
			$sql .= "  AND EXISTS( ";
			$sql .= "   SELECT ";
			$sql .= "    auth.emp_id ";
			$sql .= "   FROM ";
			$sql .= "    authmst auth ";
			$sql .= "   WHERE ";
			$sql .= "    auth.emp_id = emp.emp_id ";
			$sql .= "    AND auth.emp_del_flg = FALSE ";
			$sql .= "  ) ";
			*/
			// 2012/08/30 Yamagawa del(e)
		}

		$sql .= " ) wk ";

		if ($mode == 'analysis') {
			$sql .= "GROUP BY ";
			$sql .= " wk.emp_get_level_dept ";
			$sql .= " ,wk.level ";
			$sql .= " ,wk.get_year ";
		} else if ($mode == 'emp_list') {
			$sql .= " LEFT JOIN ";
			$sql .= "  classmst class ";
			$sql .= " ON ";
			$sql .= "  class.class_id = wk.emp_class ";
			$sql .= " LEFT JOIN ";
			$sql .= "  atrbmst atrb ";
			$sql .= " ON ";
			$sql .= "  atrb.atrb_id = wk.emp_atrb ";
			$sql .= " LEFT JOIN ";
			$sql .= "  deptmst dept ";
			$sql .= " ON ";
			$sql .= "  dept.dept_id = wk.emp_dept ";
			$sql .= " LEFT JOIN ";
			$sql .= "  classroom room ";
			$sql .= " ON ";
			$sql .= "  room.room_id = wk.emp_room ";

			$sql .= "WHERE ";
			$sql .= " 1 = 1 ";

			if ($row_1_key != 'total') {
				$sql .= " AND wk.emp_get_level_dept = :row_1_key ";
				$param['row_1_key'] = $row_1_key;
				array_push($type,'integer');
			}

			if ($row_2_key != 'total') {
				$sql .= " AND wk.level = :row_2_key ";
				$param['row_2_key'] = $row_2_key;
				array_push($type,'integer');
			}

			if ($column_1_key != 'total') {
				$sql .= " AND wk.get_year = :column_1_key ";
				$param['column_1_key'] = $column_1_key;
				array_push($type,'integer');
			}

			if ($column_2_key != 'total') {
				// 未使用
			}

			$sql .= "ORDER BY ";
			$sql .= " wk.emp_class ";
			$sql .= " ,wk.emp_atrb ";
			$sql .= " ,wk.emp_dept ";
			$sql .= " ,wk.emp_room ";
			$sql .= " ,wk.emp_id ";
		}

		$data=parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_item_unit_recognition_year_emp_cnt() END count(data):".count($data));
		return $data;

	}

	/**
	 * 認定後経過年数別人数取得
	 */
	function get_item_recognition_emp_cnt(
		$job
		, $level
		, $arr_class
		, $arr_atrb
		, $arr_dept
		, $arr_room
		, $year_from
		, $year_to
		, $class_division
		, $mode
		, $row_1_key
		, $row_2_key
		, $column_1_key
		, $column_2_key
	){

		$this->log->info("get_item_recognition_emp_cnt() START");
		$param = array();
		$type = array();

		$sql  = "";
		$sql .= "SELECT ";

		if ($mode == 'analysis') {
			$sql .= "  emp_dept ";
			$sql .= "  ,now_level ";
			$sql .= "  ,SUM(CASE WHEN year_cnt = 0 THEN cnt ELSE 0 END) AS year_0_cnt ";
			$sql .= "  ,SUM(CASE WHEN year_cnt = 1 THEN cnt ELSE 0 END) AS year_1_cnt ";
			$sql .= "  ,SUM(CASE WHEN year_cnt = 2 THEN cnt ELSE 0 END) AS year_2_cnt ";
			$sql .= "  ,SUM(CASE WHEN year_cnt = 3 THEN cnt ELSE 0 END) AS year_3_cnt ";
			$sql .= "  ,SUM(CASE WHEN year_cnt = 4 THEN cnt ELSE 0 END) AS year_4_cnt ";
			$sql .= "  ,SUM(CASE WHEN year_cnt BETWEEN 5 AND 7 THEN cnt ELSE 0 END) AS year_5_7_cnt ";
			$sql .= "  ,SUM(CASE WHEN year_cnt BETWEEN 8 AND 9 THEN cnt ELSE 0 END) AS year_8_9_cnt ";
			$sql .= "  ,SUM(CASE WHEN year_cnt >= 10 THEN cnt ELSE 0 END) AS year_10_cnt ";
			$sql .= " FROM ";
			$sql .= "  (SELECT ";
			$sql .= "   emp_dept ";
			$sql .= "   ,now_level ";
			$sql .= "   ,year_cnt ";
			$sql .= "   ,count(emp_attribute) as cnt ";
		} else if ($mode == 'emp_list') {
			$sql .= " wk.emp_lt_nm ";
			$sql .= " ,wk.emp_ft_nm ";
			$sql .= " ,class.class_nm ";
			$sql .= " ,atrb.atrb_nm ";
			$sql .= " ,dept.dept_nm ";
			$sql .= " ,room.room_nm ";
		}

		$sql .= "  FROM ";
		$sql .= "   (SELECT ";
		$sql .= "    emp.emp_class ";
		$sql .= "    ,emp.emp_attribute ";
		$sql .= "    ,emp.emp_dept ";
		$sql .= "    ,emp.emp_room ";
		$sql .= "    ,emp.emp_id ";
		$sql .= "    ,emp.emp_lt_nm ";
		$sql .= "    ,emp.emp_ft_nm ";
		$sql .= "    ,COALESCE(pro.now_level,0) AS now_level ";
		$sql .= "    ,date_part( ";
		$sql .= "      'year' ";
		$sql .= "      ,age( ";
		$sql .= "        CURRENT_TIMESTAMP ";
		$sql .= "        ,CASE pro.now_level ";
		$sql .= "          WHEN 1 THEN pro.get_level1_date ";
		$sql .= "          WHEN 2 THEN pro.get_level2_date ";
		$sql .= "          WHEN 3 THEN pro.get_level3_date ";
		$sql .= "          WHEN 4 THEN pro.get_level4_date ";
		$sql .= "          WHEN 5 THEN pro.get_level5_date ";
		$sql .= "         END ";
		$sql .= "       ) ";
		$sql .= "     ) AS year_cnt ";
		$sql .= "   FROM ";
		$sql .= "    empmst emp ";

		// 削除されている部署に所属する職員は対象外
		$sql .= "   INNER JOIN ";
		$sql .= "    deptmst dept ";
		$sql .= "   ON ";
		$sql .= "    dept.dept_id = emp.emp_dept ";
		$sql .= "    AND dept.dept_del_flg = FALSE ";

		$sql .= "    INNER JOIN ";
		$sql .= "     cl_personal_profile pro ";
		$sql .= "    ON ";
		$sql .= "     pro.emp_id = emp.emp_id ";

		if ($level == '') {
			$sql .= "     AND COALESCE(pro.now_level,0) != 0 ";
		} else {
			$sql .= "     AND pro.now_level = :now_level ";
			$param['now_level'] = $level;
			array_push($type,'integer');
		}

		// 2012/08/31 Yamagawa add(s)
		$sql .= "    INNER JOIN ";
		$sql .= "     jobmst job ";
		$sql .= "    ON ";
		$sql .= "     job.job_id = emp.emp_job ";
		$sql .= "     AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/31 Yamagawa add(e)

		$sql .= "   WHERE ";
		$sql .= "    1 = 1 ";

		if ($job != '') {
			$sql .= "    AND emp.emp_job = :emp_job ";
			$param['emp_job'] = $job;
			array_push($type,'integer');
		}

		if (count($arr_class) > 0) {
			$sql .= "   AND ( ";
			for ($i = 0; $i < count($arr_class); $i++) {
				if ($i > 0) {
					$sql .= "    OR ";
				}
				if ($arr_room[$i] != '') {
					$sql .= "    emp.emp_room = :emp_room".$i;
					$param['emp_room'.$i] = $arr_room[$i];
					array_push($type,'integer');
				} else if ($arr_dept[$i] != '') {
					$sql .= "    emp.emp_dept = :emp_dept".$i;
					$param['emp_dept'.$i] = $arr_dept[$i];
					array_push($type,'integer');
				} else if ($arr_atrb[$i] != '') {
					$sql .= "    emp.emp_attribute = :emp_attribute".$i;
					$param['emp_attribute'.$i] = $arr_atrb[$i];
					array_push($type,'integer');
				} else if ($arr_class[$i] != '') {
					$sql .= "    emp.emp_class = :emp_class".$i;
					$param['emp_class'.$i] = $arr_class[$i];
					array_push($type,'integer');
				}
			}
			$sql .= "   ) ";
		}

		if ($class_division != '' && $class_division != 0) {
			$sql .= "   AND EXISTS ( ";
			$sql .= "    SELECT ";
			$sql .= "     loginemp.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     empmst loginemp ";
			$sql .= "    WHERE ";
			$sql .= "     loginemp.emp_id = :login_emp_id_mst ";

			switch($class_division) {
				case 1:
					$sql .= "     AND loginemp.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND loginemp.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND loginemp.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND loginemp.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "    UNION ALL ";
			$sql .= "    SELECT ";
			$sql .= "     logincon.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     concurrent logincon ";
			$sql .= "    WHERE ";
			$sql .= "     logincon.emp_id = :login_emp_id_con ";

			switch($class_division) {
				case 1:
					$sql .= "     AND logincon.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND logincon.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND logincon.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND logincon.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "   ) ";

			$param['login_emp_id_mst'] = $this->user;
			array_push($type,'text');
			$param['login_emp_id_con'] = $this->user;
			array_push($type,'text');
		}

		// 2012/08/30 Yamagawa upd(s)
		/*
		$sql .= "    AND EXISTS( ";
		$sql .= "     SELECT ";
		$sql .= "      auth.emp_id ";
		$sql .= "     FROM ";
		$sql .= "      authmst auth ";
		$sql .= "     WHERE ";
		$sql .= "      auth.emp_id = emp.emp_id ";
		$sql .= "      AND auth.emp_del_flg = FALSE ";
		$sql .= "    ) ";
		*/
		$sql .= "   AND ( ";
		$sql .= "    emp.emp_retire is null ";
		$sql .= "    OR trim(emp.emp_retire) = '' ";
		$sql .= "    OR to_date(trim(emp.emp_retire),'yyyymmdd') >= current_date ";
		$sql .= "   ) ";
		// 2012/08/30 Yamagawa upd(e)
		$sql .= "   ) wk ";

		if ($mode == 'analysis') {
			$sql .= "  GROUP BY ";
			$sql .= "   emp_dept ";
			$sql .= "   ,now_level ";
			$sql .= "   ,year_cnt ";
			$sql .= "  ) wk2 ";
			$sql .= " GROUP BY ";
			$sql .= "  emp_dept ";
			$sql .= "  ,now_level ";
		} else if ($mode == 'emp_list') {
			$sql .= " LEFT JOIN ";
			$sql .= "  classmst class ";
			$sql .= " ON ";
			$sql .= "  class.class_id = wk.emp_class ";
			$sql .= " LEFT JOIN ";
			$sql .= "  atrbmst atrb ";
			$sql .= " ON ";
			$sql .= "  atrb.atrb_id = wk.emp_attribute ";
			$sql .= " LEFT JOIN ";
			$sql .= "  deptmst dept ";
			$sql .= " ON ";
			$sql .= "  dept.dept_id = wk.emp_dept ";
			$sql .= " LEFT JOIN ";
			$sql .= "  classroom room ";
			$sql .= " ON ";
			$sql .= "  room.room_id = wk.emp_room ";

			$sql .= "WHERE ";
			$sql .= " 1 = 1 ";

			if ($row_1_key != 'total') {
				$sql .= " AND wk.emp_dept = :row_1_key ";
				$param['row_1_key'] = $row_1_key;
				array_push($type,'integer');
			}

			if ($row_2_key != 'total') {
				$sql .= " AND wk.now_level = :row_2_key ";
				$param['row_2_key'] = $row_2_key;
				array_push($type,'integer');
			}

			if ($column_1_key != 'total') {

				switch($column_1_key){
					case 0:
						$sql .= "  AND wk.year_cnt = 0 ";
						break;
					case 1:
						$sql .= "  AND wk.year_cnt = 1 ";
						break;
					case 2:
						$sql .= "  AND wk.year_cnt = 2 ";
						break;
					case 3:
						$sql .= "  AND wk.year_cnt = 3 ";
						break;
					case 4:
						$sql .= "  AND wk.year_cnt = 4 ";
						break;
					case 5:
						$sql .= "  AND wk.year_cnt BETWEEN 5 AND 7 ";
						break;
					case 6:
						$sql .= "  AND wk.year_cnt BETWEEN 8 AND 9 ";
						break;
					case 7:
						$sql .= "  AND wk.year_cnt >= 10 ";
						break;
				}

			}

			if ($column_2_key != 'total') {
				// 未使用
			}

			$sql .= "ORDER BY ";
			$sql .= " wk.emp_class ";
			$sql .= " ,wk.emp_attribute ";
			$sql .= " ,wk.emp_dept ";
			$sql .= " ,wk.emp_room ";
			$sql .= " ,wk.emp_id ";
		}

		$data=parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_item_recognition_emp_cnt() END count(data):".count($data));
		return $data;

	}

	/**
	 * 部署別平均評価点数取得
	 */
	function get_item_unit_average_evaluation_point(
		$job
		, $level
		, $arr_class
		, $arr_atrb
		, $arr_dept
		, $arr_room
		, $year_from
		, $year_to
		, $class_division
		, $mode
		, $row_1_key
		, $row_2_key
		, $column_1_key
		, $column_2_key
	){

		$this->log->info("get_item_unit_average_evaluation_point() START");
		$param = array();
		$type = array();

		$sql  = "";
		$sql .= "SELECT ";

		if ($mode == 'analysis') {
			$sql .= " level ";
			$sql .= " ,CAST(category_id AS VARCHAR) AS category_id ";
			$sql .= " ,CAST(get_level_emp_dept AS VARCHAR) AS emp_dept ";
			$sql .= " ,round(AVG(COALESCE(value,0))) AS value ";
			$sql .= "FROM ";
			$sql .= " (SELECT ";
			$sql .= "  level ";
			$sql .= "  ,levelup_evaluation_id ";
			$sql .= "  ,category_id ";
			$sql .= "  ,get_level_emp_dept ";
			$sql .= "  ,SUM(COALESCE(value,0)) AS value ";
		} else if ($mode == 'emp_list') {
			$sql .= " emp.emp_lt_nm ";
			$sql .= " ,emp.emp_ft_nm ";
			$sql .= " ,class.class_nm ";
			$sql .= " ,atrb.atrb_nm ";
			$sql .= " ,dept.dept_nm ";
			$sql .= " ,room.room_nm ";
			$sql .= "FROM ";
			$sql .= " (SELECT ";
			$sql .= "  wk.emp_id ";
		}

		$sql .= " FROM ";
		$sql .= "  (SELECT ";
		$sql .= "   reco.level ";
		$sql .= "   ,eval.levelup_evaluation_id ";
		$sql .= "   ,eval.category_id ";
		$sql .= "   ,CASE ";
		$sql .= "     WHEN reco.level = 1 THEN pro.get_level1_dept ";
		$sql .= "     WHEN reco.level = 2 THEN pro.get_level2_dept ";
		$sql .= "     WHEN reco.level = 3 THEN pro.get_level3_dept ";
		$sql .= "     WHEN reco.level = 4 THEN pro.get_level4_dept ";
		$sql .= "     WHEN reco.level = 5 THEN pro.get_level5_dept ";
		$sql .= "    END AS get_level_emp_dept ";
		$sql .= "   ,emp.emp_class ";
		$sql .= "   ,emp.emp_attribute AS emp_atrb ";
		$sql .= "   ,emp.emp_dept ";
		$sql .= "   ,emp.emp_room ";
		$sql .= "   ,emp.emp_id ";
		$sql .= "   ,emp.emp_lt_nm ";
		$sql .= "   ,emp.emp_ft_nm ";
		$sql .= "   ,eval.value ";
		$sql .= "  FROM ";
		$sql .= "   empmst emp ";

		// 削除されている部署に所属する職員は対象外
		$sql .= "   INNER JOIN ";
		$sql .= "    deptmst dept ";
		$sql .= "   ON ";
		$sql .= "    dept.dept_id = emp.emp_dept ";
		$sql .= "    AND dept.dept_del_flg = FALSE ";

		$sql .= "   INNER JOIN ";
		$sql .= "    cl_personal_profile pro ";
		$sql .= "   ON ";
		$sql .= "    pro.emp_id = emp.emp_id ";
		$sql .= "   INNER JOIN ";
		$sql .= "    cl_apl_levelup_evaluation_value eval ";
		$sql .= "   ON ";
		$sql .= "    eval.levelup_emp_id = emp.emp_id ";
		$sql .= "    AND eval.evaluation_emp_division = 2 ";
		$sql .= "    AND eval.delete_flg = 0 ";
		$sql .= "   INNER JOIN ";
		$sql .= "    cl_apl_levelup_recognize reco ";
		$sql .= "   ON ";
		$sql .= "    reco.levelup_apply_id = eval.levelup_apply_id ";
		$sql .= "    AND reco.delete_flg = 0 ";

		if ($level != '') {
			$sql .= "  AND reco.level = :level ";
			$param['level'] = $level;
			array_push($type,'integer');
		}

		$sql .= "    AND CASE ";
		$sql .= "         WHEN extract(MONTH FROM reco.recognize_date) BETWEEN 1 AND 3 ";
		$sql .= "         THEN extract(YEAR FROM reco.recognize_date) - 1 ";
		$sql .= "         ELSE extract(YEAR FROM reco.recognize_date) ";
		$sql .= "        END = :year_from ";

		$param['year_from'] = $year_from;
		array_push($type,'integer');

		// 2012/08/31 Yamagawa add(s)
		$sql .= "   INNER JOIN ";
		$sql .= "    jobmst job ";
		$sql .= "   ON ";
		$sql .= "    job.job_id = emp.emp_job ";
		$sql .= "    AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/31 Yamagawa add(e)

		$sql .= "  WHERE ";
		$sql .= "   1 = 1 ";

		if ($job != '') {
			$sql .= "    AND emp.emp_job = :emp_job ";
			$param['emp_job'] = $job;
			array_push($type,'integer');
		}

		if (count($arr_class) > 0) {
			$sql .= "   AND ( ";
			for ($i = 0; $i < count($arr_class); $i++) {
				if ($i > 0) {
					$sql .= "    OR ";
				}
				if ($arr_room[$i] != '') {
					$sql .= "    emp.emp_room = :emp_room".$i;
					$param['emp_room'.$i] = $arr_room[$i];
					array_push($type,'integer');
				} else if ($arr_dept[$i] != '') {
					$sql .= "    emp.emp_dept = :emp_dept".$i;
					$param['emp_dept'.$i] = $arr_dept[$i];
					array_push($type,'integer');
				} else if ($arr_atrb[$i] != '') {
					$sql .= "    emp.emp_attribute = :emp_attribute".$i;
					$param['emp_attribute'.$i] = $arr_atrb[$i];
					array_push($type,'integer');
				} else if ($arr_class[$i] != '') {
					$sql .= "    emp.emp_class = :emp_class".$i;
					$param['emp_class'.$i] = $arr_class[$i];
					array_push($type,'integer');
				}
			}
			$sql .= "   ) ";
		}

		if ($class_division != '' && $class_division != 0) {
			$sql .= "   AND EXISTS ( ";
			$sql .= "    SELECT ";
			$sql .= "     loginemp.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     empmst loginemp ";
			$sql .= "    WHERE ";
			$sql .= "     loginemp.emp_id = :login_emp_id_mst ";

			switch($class_division) {
				case 1:
					$sql .= "     AND loginemp.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND loginemp.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND loginemp.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND loginemp.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "    UNION ALL ";
			$sql .= "    SELECT ";
			$sql .= "     logincon.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     concurrent logincon ";
			$sql .= "    WHERE ";
			$sql .= "     logincon.emp_id = :login_emp_id_con ";

			switch($class_division) {
				case 1:
					$sql .= "     AND logincon.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND logincon.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND logincon.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND logincon.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "   ) ";

			$param['login_emp_id_mst'] = $this->user;
			array_push($type,'text');
			$param['login_emp_id_con'] = $this->user;
			array_push($type,'text');
		}

		// 2012/08/30 Yamagawa del(s)
		/*
		$sql .= "   AND EXISTS( ";
		$sql .= "    SELECT ";
		$sql .= "     auth.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     authmst auth ";
		$sql .= "    WHERE ";
		$sql .= "     auth.emp_id = emp.emp_id ";
		$sql .= "     AND auth.emp_del_flg = FALSE ";
		$sql .= "   ) ";
		*/
		// 2012/08/30 Yamagawa del(e)
		$sql .= "  ) wk ";

		if ($mode == 'analysis') {
			$sql .= " GROUP BY ";
			$sql .= "  level ";
			$sql .= "  ,levelup_evaluation_id ";
			$sql .= "  ,category_id ";
			$sql .= "  ,get_level_emp_dept ";
			$sql .= " ) wk2 ";
			$sql .= "GROUP BY ";
			$sql .= " level ";
			$sql .= " ,category_id ";
			$sql .= " ,get_level_emp_dept ";
		} else if ($mode == 'emp_list') {
			$sql .= "WHERE ";
			$sql .= " 1 = 1 ";

			if ($row_1_key != 'total') {
				$sql .= " AND wk.get_level_emp_dept = :row_1_key ";
				$param['row_1_key'] = $row_1_key;
				array_push($type,'integer');
			}

			if ($row_2_key != 'total') {
				// 未使用
			}

			if ($column_1_key != 'total') {
				$sql .= " AND wk.level = :column_1_key ";
				$param['column_1_key'] = $column_1_key;
				array_push($type,'integer');
			}

			if ($column_2_key != 'total') {
				$sql .= " AND wk.category_id = :column_2_key ";
				$param['column_2_key'] = $column_2_key;
				array_push($type,'integer');
			}

			$sql .= " GROUP BY ";
			$sql .= "  wk.emp_id ";
			$sql .= " ) wk2 ";
			$sql .= " INNER JOIN ";
			$sql .= "  empmst emp ";
			$sql .= " ON ";
			$sql .= "  emp.emp_id = wk2.emp_id ";
			$sql .= " LEFT JOIN ";
			$sql .= "  classmst class ";
			$sql .= " ON ";
			$sql .= "  class.class_id = emp.emp_class ";
			$sql .= " LEFT JOIN ";
			$sql .= "  atrbmst atrb ";
			$sql .= " ON ";
			$sql .= "  atrb.atrb_id = emp.emp_attribute ";
			$sql .= " LEFT JOIN ";
			$sql .= "  deptmst dept ";
			$sql .= " ON ";
			$sql .= "  dept.dept_id = emp.emp_dept ";
			$sql .= " LEFT JOIN ";
			$sql .= "  classroom room ";
			$sql .= " ON ";
			$sql .= "  room.room_id = emp.emp_room ";

			$sql .= "ORDER BY ";
			$sql .= " emp.emp_class ";
			$sql .= " ,emp.emp_attribute ";
			$sql .= " ,emp.emp_dept ";
			$sql .= " ,emp.emp_room ";
			$sql .= " ,emp.emp_id ";
		}

		if ($mode == 'analysis') {
			// レベル＞カテゴリ
			$sql .= "UNION ALL ";
			$sql .= "SELECT ";
			$sql .= " level ";
			$sql .= " ,CAST(category_id AS VARCHAR) AS category_id ";
			$sql .= " ,'total' AS emp_dept ";
			$sql .= " ,round(AVG(COALESCE(value,0))) AS value ";
			$sql .= "FROM ";
			$sql .= " (SELECT ";
			$sql .= "  level ";
			$sql .= "  ,levelup_evaluation_id ";
			$sql .= "  ,category_id ";
			$sql .= "  ,SUM(COALESCE(value,0)) AS value ";
			$sql .= " FROM ";
			$sql .= "  (SELECT ";
			$sql .= "   reco.level ";
			$sql .= "   ,eval.levelup_evaluation_id ";
			$sql .= "   ,eval.category_id ";
			$sql .= "   ,eval.value ";
			$sql .= "  FROM ";
			$sql .= "   empmst emp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    cl_personal_profile pro ";
			$sql .= "   ON ";
			$sql .= "    pro.emp_id = emp.emp_id ";
			$sql .= "   INNER JOIN ";
			$sql .= "    cl_apl_levelup_evaluation_value eval ";
			$sql .= "   ON ";
			$sql .= "    eval.levelup_emp_id = emp.emp_id ";
			$sql .= "    AND eval.evaluation_emp_division = 2 ";
			$sql .= "    AND eval.delete_flg = 0 ";
			$sql .= "   INNER JOIN ";
			$sql .= "    cl_apl_levelup_recognize reco ";
			$sql .= "   ON ";
			$sql .= "    reco.levelup_apply_id = eval.levelup_apply_id ";
			$sql .= "    AND reco.delete_flg = 0 ";

			if ($level != '') {
				$sql .= "  AND reco.level = :level_2 ";
				$param['level_2'] = $level;
				array_push($type,'integer');
			}

			$sql .= "    AND CASE ";
			$sql .= "         WHEN extract(MONTH FROM reco.recognize_date) BETWEEN 1 AND 3 ";
			$sql .= "         THEN extract(YEAR FROM reco.recognize_date) - 1 ";
			$sql .= "         ELSE extract(YEAR FROM reco.recognize_date) ";
			$sql .= "        END = :year_from_2 ";

			$param['year_from_2'] = $year_from;
			array_push($type,'integer');

			// 2012/08/31 Yamagawa add(s)
			$sql .= "   INNER JOIN ";
			$sql .= "    jobmst job ";
			$sql .= "   ON ";
			$sql .= "    job.job_id = emp.emp_job ";
			$sql .= "    AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
			// 2012/08/31 Yamagawa add(e)

			$sql .= "  WHERE ";
			$sql .= "   1 = 1 ";

			if ($job != '') {
				$sql .= "    AND emp.emp_job = :emp_job_2 ";
				$param['emp_job_2'] = $job;
				array_push($type,'integer');
			}

			if (count($arr_class) > 0) {
				$sql .= "   AND ( ";
				for ($i = 0; $i < count($arr_class); $i++) {
					if ($i > 0) {
						$sql .= "    OR ";
					}
					if ($arr_room[$i] != '') {
						$sql .= "    emp.emp_room = :emp_room_2".$i;
						$param['emp_room_2'.$i] = $arr_room[$i];
						array_push($type,'integer');
					} else if ($arr_dept[$i] != '') {
						$sql .= "    emp.emp_dept = :emp_dept_2".$i;
						$param['emp_dept_2'.$i] = $arr_dept[$i];
						array_push($type,'integer');
					} else if ($arr_atrb[$i] != '') {
						$sql .= "    emp.emp_attribute = :emp_attribute_2".$i;
						$param['emp_attribute_2'.$i] = $arr_atrb[$i];
						array_push($type,'integer');
					} else if ($arr_class[$i] != '') {
						$sql .= "    emp.emp_class = :emp_class_2".$i;
						$param['emp_class_2'.$i] = $arr_class[$i];
						array_push($type,'integer');
					}
				}
				$sql .= "   ) ";
			}

			if ($class_division != '' && $class_division != 0) {
				$sql .= "   AND EXISTS ( ";
				$sql .= "    SELECT ";
				$sql .= "     loginemp.emp_id ";
				$sql .= "    FROM ";
				$sql .= "     empmst loginemp ";
				$sql .= "    WHERE ";
				$sql .= "     loginemp.emp_id = :login_emp_id_mst_2 ";

				switch($class_division) {
					case 1:
						$sql .= "     AND loginemp.emp_class = emp.emp_class ";
						break;
					case 2:
						$sql .= "     AND loginemp.emp_attribute = emp.emp_attribute ";
						break;
					case 3:
						$sql .= "     AND loginemp.emp_dept = emp.emp_dept ";
						break;
					case 4:
						$sql .= "     AND loginemp.emp_room = emp.emp_room ";
						break;
				}

				$sql .= "    UNION ALL ";
				$sql .= "    SELECT ";
				$sql .= "     logincon.emp_id ";
				$sql .= "    FROM ";
				$sql .= "     concurrent logincon ";
				$sql .= "    WHERE ";
				$sql .= "     logincon.emp_id = :login_emp_id_con_2 ";

				switch($class_division) {
					case 1:
						$sql .= "     AND logincon.emp_class = emp.emp_class ";
						break;
					case 2:
						$sql .= "     AND logincon.emp_attribute = emp.emp_attribute ";
						break;
					case 3:
						$sql .= "     AND logincon.emp_dept = emp.emp_dept ";
						break;
					case 4:
						$sql .= "     AND logincon.emp_room = emp.emp_room ";
						break;
				}

				$sql .= "   ) ";

				$param['login_emp_id_mst_2'] = $this->user;
				array_push($type,'text');
				$param['login_emp_id_con_2'] = $this->user;
				array_push($type,'text');
			}

			// 2012/08/30 Yamagawa del(s)
			/*
			$sql .= "   AND EXISTS( ";
			$sql .= "    SELECT ";
			$sql .= "     auth.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     authmst auth ";
			$sql .= "    WHERE ";
			$sql .= "     auth.emp_id = emp.emp_id ";
			$sql .= "     AND auth.emp_del_flg = FALSE ";
			$sql .= "   ) ";
			*/
			// 2012/08/30 Yamagawa del(e)
			$sql .= "  ) wk ";
			$sql .= " GROUP BY ";
			$sql .= "  level ";
			$sql .= "  ,levelup_evaluation_id ";
			$sql .= "  ,category_id ";
			$sql .= " ) wk2 ";
			$sql .= "GROUP BY ";
			$sql .= " level ";
			$sql .= " ,category_id ";

			// レベル＞部署
			$sql .= "UNION ALL ";
			$sql .= "SELECT ";
			$sql .= " level ";
			$sql .= " ,'total' AS category_id ";
			$sql .= " ,CAST(emp_dept AS VARCHAR) AS emp_dept ";
			$sql .= " ,round(AVG(COALESCE(value,0))) AS value ";
			$sql .= "FROM ";
			$sql .= " (SELECT ";
			$sql .= "  level ";
			$sql .= "  ,levelup_evaluation_id ";
			$sql .= "  ,emp_dept ";
			$sql .= "  ,SUM(COALESCE(value,0)) AS value ";
			$sql .= " FROM ";
			$sql .= "  (SELECT ";
			$sql .= "   reco.level ";
			$sql .= "   ,eval.levelup_evaluation_id ";
			$sql .= "   ,eval.category_id ";
			$sql .= "   ,CASE ";
			$sql .= "     WHEN reco.level = 1 THEN pro.get_level1_dept ";
			$sql .= "     WHEN reco.level = 2 THEN pro.get_level2_dept ";
			$sql .= "     WHEN reco.level = 3 THEN pro.get_level3_dept ";
			$sql .= "     WHEN reco.level = 4 THEN pro.get_level4_dept ";
			$sql .= "     WHEN reco.level = 5 THEN pro.get_level5_dept ";
			$sql .= "    END AS emp_dept ";
			$sql .= "   ,eval.value ";
			$sql .= "  FROM ";
			$sql .= "   empmst emp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    cl_personal_profile pro ";
			$sql .= "   ON ";
			$sql .= "    pro.emp_id = emp.emp_id ";
			$sql .= "   INNER JOIN ";
			$sql .= "    cl_apl_levelup_evaluation_value eval ";
			$sql .= "   ON ";
			$sql .= "    eval.levelup_emp_id = emp.emp_id ";
			$sql .= "    AND eval.evaluation_emp_division = 2 ";
			$sql .= "    AND eval.delete_flg = 0 ";
			$sql .= "   INNER JOIN ";
			$sql .= "    cl_apl_levelup_recognize reco ";
			$sql .= "   ON ";
			$sql .= "    reco.levelup_apply_id = eval.levelup_apply_id ";
			$sql .= "    AND reco.delete_flg = 0 ";

			if ($level != '') {
				$sql .= "  AND reco.level = :level_3 ";
				$param['level_3'] = $level;
				array_push($type,'integer');
			}

			$sql .= "    AND CASE ";
			$sql .= "         WHEN extract(MONTH FROM reco.recognize_date) BETWEEN 1 AND 3 ";
			$sql .= "         THEN extract(YEAR FROM reco.recognize_date) - 1 ";
			$sql .= "         ELSE extract(YEAR FROM reco.recognize_date) ";
			$sql .= "        END = :year_from_3 ";

			$param['year_from_3'] = $year_from;
			array_push($type,'integer');

			// 2012/08/31 Yamagawa add(s)
			$sql .= "    INNER JOIN ";
			$sql .= "     jobmst job ";
			$sql .= "    ON ";
			$sql .= "     job.job_id = emp.emp_job ";
			$sql .= "     AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
			// 2012/08/31 Yamagawa add(e)

			$sql .= "  WHERE ";
			$sql .= "   1 = 1 ";

			if ($job != '') {
				$sql .= "    AND emp.emp_job = :emp_job_3 ";
				$param['emp_job_3'] = $job;
				array_push($type,'integer');
			}

			if (count($arr_class) > 0) {
				$sql .= "   AND ( ";
				for ($i = 0; $i < count($arr_class); $i++) {
					if ($i > 0) {
						$sql .= "    OR ";
					}
					if ($arr_room[$i] != '') {
						$sql .= "    emp.emp_room = :emp_room_3".$i;
						$param['emp_room_3'.$i] = $arr_room[$i];
						array_push($type,'integer');
					} else if ($arr_dept[$i] != '') {
						$sql .= "    emp.emp_dept = :emp_dept_3".$i;
						$param['emp_dept_3'.$i] = $arr_dept[$i];
						array_push($type,'integer');
					} else if ($arr_atrb[$i] != '') {
						$sql .= "    emp.emp_attribute = :emp_attribute_3".$i;
						$param['emp_attribute_3'.$i] = $arr_atrb[$i];
						array_push($type,'integer');
					} else if ($arr_class[$i] != '') {
						$sql .= "    emp.emp_class = :emp_class_3".$i;
						$param['emp_class_3'.$i] = $arr_class[$i];
						array_push($type,'integer');
					}
				}
				$sql .= "   ) ";
			}

			if ($class_division != '' && $class_division != 0) {
				$sql .= "   AND EXISTS ( ";
				$sql .= "    SELECT ";
				$sql .= "     loginemp.emp_id ";
				$sql .= "    FROM ";
				$sql .= "     empmst loginemp ";
				$sql .= "    WHERE ";
				$sql .= "     loginemp.emp_id = :login_emp_id_mst_3 ";

				switch($class_division) {
					case 1:
						$sql .= "     AND loginemp.emp_class = emp.emp_class ";
						break;
					case 2:
						$sql .= "     AND loginemp.emp_attribute = emp.emp_attribute ";
						break;
					case 3:
						$sql .= "     AND loginemp.emp_dept = emp.emp_dept ";
						break;
					case 4:
						$sql .= "     AND loginemp.emp_room = emp.emp_room ";
						break;
				}

				$sql .= "    UNION ALL ";
				$sql .= "    SELECT ";
				$sql .= "     logincon.emp_id ";
				$sql .= "    FROM ";
				$sql .= "     concurrent logincon ";
				$sql .= "    WHERE ";
				$sql .= "     logincon.emp_id = :login_emp_id_con_3 ";

				switch($class_division) {
					case 1:
						$sql .= "     AND logincon.emp_class = emp.emp_class ";
						break;
					case 2:
						$sql .= "     AND logincon.emp_attribute = emp.emp_attribute ";
						break;
					case 3:
						$sql .= "     AND logincon.emp_dept = emp.emp_dept ";
						break;
					case 4:
						$sql .= "     AND logincon.emp_room = emp.emp_room ";
						break;
				}

				$sql .= "   ) ";

				$param['login_emp_id_mst_3'] = $this->user;
				array_push($type,'text');
				$param['login_emp_id_con_3'] = $this->user;
				array_push($type,'text');
			}

			// 2012/08/30 Yamagawa del(s)
			/*
			$sql .= "   AND EXISTS( ";
			$sql .= "    SELECT ";
			$sql .= "     auth.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     authmst auth ";
			$sql .= "    WHERE ";
			$sql .= "     auth.emp_id = emp.emp_id ";
			$sql .= "     AND auth.emp_del_flg = FALSE ";
			$sql .= "   ) ";
			*/
			// 2012/08/30 Yamagawa del(e)
			$sql .= "  ) wk ";
			$sql .= " GROUP BY ";
			$sql .= "  level ";
			$sql .= "  ,levelup_evaluation_id ";
			$sql .= "  ,emp_dept ";
			$sql .= " ) wk2 ";
			$sql .= "GROUP BY ";
			$sql .= " level ";
			$sql .= " ,emp_dept ";

			// レベル
			$sql .= "UNION ALL ";
			$sql .= "SELECT ";
			$sql .= " level ";
			$sql .= " ,'total' AS category_id ";
			$sql .= " ,'total' AS emp_dept ";
			$sql .= " ,round(AVG(COALESCE(value,0))) AS value ";
			$sql .= "FROM ";
			$sql .= " (SELECT ";
			$sql .= "  level ";
			$sql .= "  ,levelup_evaluation_id ";
			$sql .= "  ,SUM(COALESCE(value,0)) AS value ";
			$sql .= " FROM ";
			$sql .= "  (SELECT ";
			$sql .= "   reco.level ";
			$sql .= "   ,eval.levelup_evaluation_id ";
			$sql .= "   ,eval.value ";
			$sql .= "  FROM ";
			$sql .= "   empmst emp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    cl_personal_profile pro ";
			$sql .= "   ON ";
			$sql .= "    pro.emp_id = emp.emp_id ";
			$sql .= "   INNER JOIN ";
			$sql .= "    cl_apl_levelup_evaluation_value eval ";
			$sql .= "   ON ";
			$sql .= "    eval.levelup_emp_id = emp.emp_id ";
			$sql .= "    AND eval.evaluation_emp_division = 2 ";
			$sql .= "    AND eval.delete_flg = 0 ";
			$sql .= "   INNER JOIN ";
			$sql .= "    cl_apl_levelup_recognize reco ";
			$sql .= "   ON ";
			$sql .= "    reco.levelup_apply_id = eval.levelup_apply_id ";
			$sql .= "    AND reco.delete_flg = 0 ";

			if ($level != '') {
				$sql .= "  AND reco.level = :level_4 ";
				$param['level_4'] = $level;
				array_push($type,'integer');
			}

			$sql .= "    AND CASE ";
			$sql .= "         WHEN extract(MONTH FROM reco.recognize_date) BETWEEN 1 AND 3 ";
			$sql .= "         THEN extract(YEAR FROM reco.recognize_date) - 1 ";
			$sql .= "         ELSE extract(YEAR FROM reco.recognize_date) ";
			$sql .= "        END = :year_from_4 ";

			$param['year_from_4'] = $year_from;
			array_push($type,'integer');

			// 2012/08/31 Yamagawa add(s)
			$sql .= "   INNER JOIN ";
			$sql .= "    jobmst job ";
			$sql .= "   ON ";
			$sql .= "    job.job_id = emp.emp_job ";
			$sql .= "    AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
			// 2012/08/31 Yamagawa add(e)

			$sql .= "  WHERE ";
			$sql .= "   1 = 1 ";

			if ($job != '') {
				$sql .= "    AND emp.emp_job = :emp_job_4 ";
				$param['emp_job_4'] = $job;
				array_push($type,'integer');
			}

			if (count($arr_class) > 0) {
				$sql .= "   AND ( ";
				for ($i = 0; $i < count($arr_class); $i++) {
					if ($i > 0) {
						$sql .= "    OR ";
					}
					if ($arr_room[$i] != '') {
						$sql .= "    emp.emp_room = :emp_room_4".$i;
						$param['emp_room_4'.$i] = $arr_room[$i];
						array_push($type,'integer');
					} else if ($arr_dept[$i] != '') {
						$sql .= "    emp.emp_dept = :emp_dept_4".$i;
						$param['emp_dept_4'.$i] = $arr_dept[$i];
						array_push($type,'integer');
					} else if ($arr_atrb[$i] != '') {
						$sql .= "    emp.emp_attribute = :emp_attribute_4".$i;
						$param['emp_attribute_4'.$i] = $arr_atrb[$i];
						array_push($type,'integer');
					} else if ($arr_class[$i] != '') {
						$sql .= "    emp.emp_class = :emp_class_4".$i;
						$param['emp_class_4'.$i] = $arr_class[$i];
						array_push($type,'integer');
					}
				}
				$sql .= "   ) ";
			}

			if ($class_division != '' && $class_division != 0) {
				$sql .= "   AND EXISTS ( ";
				$sql .= "    SELECT ";
				$sql .= "     loginemp.emp_id ";
				$sql .= "    FROM ";
				$sql .= "     empmst loginemp ";
				$sql .= "    WHERE ";
				$sql .= "     loginemp.emp_id = :login_emp_id_mst_4 ";

				switch($class_division) {
					case 1:
						$sql .= "     AND loginemp.emp_class = emp.emp_class ";
						break;
					case 2:
						$sql .= "     AND loginemp.emp_attribute = emp.emp_attribute ";
						break;
					case 3:
						$sql .= "     AND loginemp.emp_dept = emp.emp_dept ";
						break;
					case 4:
						$sql .= "     AND loginemp.emp_room = emp.emp_room ";
						break;
				}

				$sql .= "    UNION ALL ";
				$sql .= "    SELECT ";
				$sql .= "     logincon.emp_id ";
				$sql .= "    FROM ";
				$sql .= "     concurrent logincon ";
				$sql .= "    WHERE ";
				$sql .= "     logincon.emp_id = :login_emp_id_con_4 ";

				switch($class_division) {
					case 1:
						$sql .= "     AND logincon.emp_class = emp.emp_class ";
						break;
					case 2:
						$sql .= "     AND logincon.emp_attribute = emp.emp_attribute ";
						break;
					case 3:
						$sql .= "     AND logincon.emp_dept = emp.emp_dept ";
						break;
					case 4:
						$sql .= "     AND logincon.emp_room = emp.emp_room ";
						break;
				}

				$sql .= "   ) ";

				$param['login_emp_id_mst_4'] = $this->user;
				array_push($type,'text');
				$param['login_emp_id_con_4'] = $this->user;
				array_push($type,'text');
			}

			// 2012/08/30 Yamagawa del(s)
			/*
			$sql .= "   AND EXISTS( ";
			$sql .= "    SELECT ";
			$sql .= "     auth.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     authmst auth ";
			$sql .= "    WHERE ";
			$sql .= "     auth.emp_id = emp.emp_id ";
			$sql .= "     AND auth.emp_del_flg = FALSE ";
			$sql .= "   ) ";
			*/
			// 2012/08/30 Yamagawa del(e)
			$sql .= "  ) wk ";
			$sql .= " GROUP BY ";
			$sql .= "  level ";
			$sql .= "  ,levelup_evaluation_id ";
			$sql .= " ) wk2 ";
			$sql .= "GROUP BY ";
			$sql .= " level ";
		}

		$data=parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_item_unit_average_evaluation_point() END count(data):".count($data));
		return $data;

	}

	/**
	 * 年度別平均評価点数取得
	 */
	function get_item_year_average_evaluation_point(
		$job
		, $level
		, $arr_class
		, $arr_atrb
		, $arr_dept
		, $arr_room
		, $year_from
		, $year_to
		, $class_division
		, $mode
		, $row_1_key
		, $row_2_key
		, $column_1_key
		, $column_2_key
	){

		$this->log->info("get_item_year_average_evaluation_point() START");
		$param = array();
		$type = array();

		$sql  = "";
		$sql .= "SELECT ";

		if ($mode == 'analysis') {
			$sql .= " level ";
			$sql .= " ,CAST(category_id AS VARCHAR) AS category_id ";
			$sql .= " ,CAST(eval_year AS VARCHAR) AS eval_year ";
			$sql .= " ,round(AVG(COALESCE(value,0))) AS value ";
			$sql .= "FROM ";
			$sql .= " (SELECT ";
			$sql .= "  level ";
			$sql .= "  ,levelup_evaluation_id ";
			$sql .= "  ,category_id ";
			$sql .= "  ,eval_year ";
			$sql .= "  ,SUM(COALESCE(value,0)) AS value ";
		} else if ($mode == 'emp_list') {
			$sql .= " emp.emp_lt_nm ";
			$sql .= " ,emp.emp_ft_nm ";
			$sql .= " ,class.class_nm ";
			$sql .= " ,atrb.atrb_nm ";
			$sql .= " ,dept.dept_nm ";
			$sql .= " ,room.room_nm ";
			$sql .= " FROM ";
			$sql .= "  (SELECT ";
			$sql .= "   wk.emp_id ";
		}

		$sql .= " FROM ";
		$sql .= "  (SELECT ";
		$sql .= "   reco.level ";
		$sql .= "   ,eval.levelup_evaluation_id ";
		$sql .= "   ,eval.category_id ";
		$sql .= "   ,CASE ";
		$sql .= "     WHEN extract(MONTH FROM reco.recognize_date) BETWEEN 1 AND 3 ";
		$sql .= "     THEN extract(YEAR FROM reco.recognize_date) - 1 ";
		$sql .= "     ELSE extract(YEAR FROM reco.recognize_date) ";
		$sql .= "    END AS eval_year ";
		$sql .= "   ,eval.value ";
		$sql .= "   ,emp.emp_class ";
		$sql .= "   ,emp.emp_attribute ";
		$sql .= "   ,emp.emp_dept ";
		$sql .= "   ,emp.emp_room ";
		$sql .= "   ,emp.emp_id ";
		$sql .= "   ,emp.emp_lt_nm ";
		$sql .= "   ,emp.emp_ft_nm ";
		$sql .= "  FROM ";
		$sql .= "   empmst emp ";
		$sql .= "   INNER JOIN ";
		$sql .= "    cl_apl_levelup_evaluation_value eval ";
		$sql .= "   ON ";
		$sql .= "    eval.levelup_emp_id = emp.emp_id ";
		$sql .= "    AND eval.evaluation_emp_division = 2 ";
		$sql .= "    AND eval.delete_flg = 0 ";
		$sql .= "   INNER JOIN ";
		$sql .= "    cl_apl_levelup_recognize reco ";
		$sql .= "   ON ";
		$sql .= "    reco.levelup_apply_id = eval.levelup_apply_id ";
		$sql .= "    AND reco.delete_flg = 0 ";

		if ($level != '') {
			$sql .= "  AND reco.level = :level ";
			$param['level'] = $level;
			array_push($type,'integer');
		}

		$sql .= "    AND CASE ";
		$sql .= "         WHEN extract(MONTH FROM reco.recognize_date) BETWEEN 1 AND 3 ";
		$sql .= "         THEN extract(YEAR FROM reco.recognize_date) - 1 ";
		$sql .= "         ELSE extract(YEAR FROM reco.recognize_date) ";
		$sql .= "        END BETWEEN :year_from AND :year_to ";

		$param['year_from'] = $year_from;
		array_push($type,'integer');
		$param['year_to'] = $year_to;
		array_push($type,'integer');

		// 2012/08/31 Yamagawa add(s)
		$sql .= "   INNER JOIN ";
		$sql .= "    jobmst job ";
		$sql .= "   ON ";
		$sql .= "    job.job_id = emp.emp_job ";
		$sql .= "    AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/31 Yamagawa add(e)

		$sql .= "  WHERE ";
		$sql .= "   1 = 1 ";

		if ($job != '') {
			$sql .= "    AND emp.emp_job = :emp_job ";
			$param['emp_job'] = $job;
			array_push($type,'integer');
		}

		if (count($arr_class) > 0) {
			$sql .= "   AND ( ";
			for ($i = 0; $i < count($arr_class); $i++) {
				if ($i > 0) {
					$sql .= "    OR ";
				}
				if ($arr_room[$i] != '') {
					$sql .= "    emp.emp_room = :emp_room".$i;
					$param['emp_room'.$i] = $arr_room[$i];
					array_push($type,'integer');
				} else if ($arr_dept[$i] != '') {
					$sql .= "    emp.emp_dept = :emp_dept".$i;
					$param['emp_dept'.$i] = $arr_dept[$i];
					array_push($type,'integer');
				} else if ($arr_atrb[$i] != '') {
					$sql .= "    emp.emp_attribute = :emp_attribute".$i;
					$param['emp_attribute'.$i] = $arr_atrb[$i];
					array_push($type,'integer');
				} else if ($arr_class[$i] != '') {
					$sql .= "    emp.emp_class = :emp_class".$i;
					$param['emp_class'.$i] = $arr_class[$i];
					array_push($type,'integer');
				}
			}
			$sql .= "   ) ";
		}

		if ($class_division != '' && $class_division != 0) {
			$sql .= "   AND EXISTS ( ";
			$sql .= "    SELECT ";
			$sql .= "     loginemp.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     empmst loginemp ";
			$sql .= "    WHERE ";
			$sql .= "     loginemp.emp_id = :login_emp_id_mst ";

			switch($class_division) {
				case 1:
					$sql .= "     AND loginemp.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND loginemp.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND loginemp.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND loginemp.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "    UNION ALL ";
			$sql .= "    SELECT ";
			$sql .= "     logincon.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     concurrent logincon ";
			$sql .= "    WHERE ";
			$sql .= "     logincon.emp_id = :login_emp_id_con ";

			switch($class_division) {
				case 1:
					$sql .= "     AND logincon.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND logincon.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND logincon.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND logincon.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "   ) ";

			$param['login_emp_id_mst'] = $this->user;
			array_push($type,'text');
			$param['login_emp_id_con'] = $this->user;
			array_push($type,'text');
		}

		// 2012/08/30 Yamagawa del(s)
		/*
		$sql .= "   AND EXISTS( ";
		$sql .= "    SELECT ";
		$sql .= "     auth.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     authmst auth ";
		$sql .= "    WHERE ";
		$sql .= "     auth.emp_id = emp.emp_id ";
		$sql .= "     AND auth.emp_del_flg = FALSE ";
		$sql .= "   ) ";
		*/
		// 2012/08/30 Yamagawa del(e)
		$sql .= "  ) wk ";

		if ($mode == 'analysis') {
			$sql .= " GROUP BY ";
			$sql .= "  level ";
			$sql .= "  ,levelup_evaluation_id ";
			$sql .= "  ,category_id ";
			$sql .= "  ,eval_year ";
			$sql .= " ) wk2 ";
			$sql .= "GROUP BY ";
			$sql .= " level ";
			$sql .= " ,category_id ";
			$sql .= " ,eval_year ";
		} else if ($mode == 'emp_list') {
			$sql .= "WHERE ";
			$sql .= " 1 = 1 ";

			if ($row_1_key != 'total') {
				$sql .= " AND wk.level = :row_1_key ";
				$param['row_1_key'] = $row_1_key;
				array_push($type,'integer');
			}

			if ($row_2_key != 'total') {
				$sql .= " AND wk.category_id = :row_2_key ";
				$param['row_2_key'] = $row_2_key;
				array_push($type,'integer');
			}

			if ($column_1_key != 'total') {
				$sql .= " AND wk.eval_year = :column_1_key ";
				$param['column_1_key'] = $column_1_key;
				array_push($type,'integer');
			}

			if ($column_2_key != 'total') {
				// 未使用
			}

			$sql .= " GROUP BY ";
			$sql .= "  wk.emp_id ";
			$sql .= " ) wk2 ";
			$sql .= " INNER JOIN ";
			$sql .= "  empmst emp ";
			$sql .= " ON ";
			$sql .= "  emp.emp_id = wk2.emp_id ";
			$sql .= " LEFT JOIN ";
			$sql .= "  classmst class ";
			$sql .= " ON ";
			$sql .= "  class.class_id = emp.emp_class ";
			$sql .= " LEFT JOIN ";
			$sql .= "  atrbmst atrb ";
			$sql .= " ON ";
			$sql .= "  atrb.atrb_id = emp.emp_attribute ";
			$sql .= " LEFT JOIN ";
			$sql .= "  deptmst dept ";
			$sql .= " ON ";
			$sql .= "  dept.dept_id = emp.emp_dept ";
			$sql .= " LEFT JOIN ";
			$sql .= "  classroom room ";
			$sql .= " ON ";
			$sql .= "  room.room_id = emp.emp_room ";

			$sql .= "ORDER BY ";
			$sql .= " emp.emp_class ";
			$sql .= " ,emp.emp_attribute ";
			$sql .= " ,emp.emp_dept ";
			$sql .= " ,emp.emp_room ";
			$sql .= " ,emp.emp_id ";
		}

		if ($mode == 'analysis') {
			// レベル＞カテゴリ
			$sql .= "UNION ALL ";
			$sql .= "SELECT ";
			$sql .= " level ";
			$sql .= " ,'total' AS category_id ";
			$sql .= " ,CAST(eval_year AS VARCHAR) AS eval_year ";
			$sql .= " ,round(AVG(COALESCE(value,0))) AS value ";
			$sql .= "FROM ";
			$sql .= " (SELECT ";
			$sql .= "  level ";
			$sql .= "  ,levelup_evaluation_id ";
			$sql .= "  ,eval_year ";
			$sql .= "  ,SUM(COALESCE(value,0)) AS value ";
			$sql .= " FROM ";
			$sql .= "  (SELECT ";
			$sql .= "   reco.level ";
			$sql .= "   ,eval.levelup_evaluation_id ";
			$sql .= "   ,CASE ";
			$sql .= "     WHEN extract(MONTH FROM reco.recognize_date) BETWEEN 1 AND 3 ";
			$sql .= "     THEN extract(YEAR FROM reco.recognize_date) - 1 ";
			$sql .= "     ELSE extract(YEAR FROM reco.recognize_date) ";
			$sql .= "    END AS eval_year ";
			$sql .= "   ,eval.value ";
			$sql .= "  FROM ";
			$sql .= "   empmst emp ";
			$sql .= "   INNER JOIN ";
			$sql .= "    cl_apl_levelup_evaluation_value eval ";
			$sql .= "   ON ";
			$sql .= "    eval.levelup_emp_id = emp.emp_id ";
			$sql .= "    AND eval.evaluation_emp_division = 2 ";
			$sql .= "    AND eval.delete_flg = 0 ";
			$sql .= "   INNER JOIN ";
			$sql .= "    cl_apl_levelup_recognize reco ";
			$sql .= "   ON ";
			$sql .= "    reco.levelup_apply_id = eval.levelup_apply_id ";
			$sql .= "    AND reco.delete_flg = 0 ";

			if ($level != '') {
				$sql .= "  AND reco.level = :level_2 ";
				$param['level_2'] = $level;
				array_push($type,'integer');
			}

			$sql .= "    AND CASE ";
			$sql .= "         WHEN extract(MONTH FROM reco.recognize_date) BETWEEN 1 AND 3 ";
			$sql .= "         THEN extract(YEAR FROM reco.recognize_date) - 1 ";
			$sql .= "         ELSE extract(YEAR FROM reco.recognize_date) ";
			$sql .= "        END BETWEEN :year_from_2 AND :year_to_2 ";

			$param['year_from_2'] = $year_from;
			array_push($type,'integer');
			$param['year_to_2'] = $year_to;
			array_push($type,'integer');

			// 2012/08/31 Yamagawa add(s)
			$sql .= "   INNER JOIN ";
			$sql .= "    jobmst job ";
			$sql .= "   ON ";
			$sql .= "    job.job_id = emp.emp_job ";
			$sql .= "    AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
			// 2012/08/31 Yamagawa add(e)

			$sql .= "  WHERE ";
			$sql .= "   1 = 1 ";

			if ($job != '') {
				$sql .= "    AND emp.emp_job = :emp_job_2 ";
				$param['emp_job_2'] = $job;
				array_push($type,'integer');
			}

			if (count($arr_class) > 0) {
				$sql .= "   AND ( ";
				for ($i = 0; $i < count($arr_class); $i++) {
					if ($i > 0) {
						$sql .= "    OR ";
					}
					if ($arr_room[$i] != '') {
						$sql .= "    emp.emp_room = :emp_room_2".$i;
						$param['emp_room_2'.$i] = $arr_room[$i];
						array_push($type,'integer');
					} else if ($arr_dept[$i] != '') {
						$sql .= "    emp.emp_dept = :emp_dept_2".$i;
						$param['emp_dept_2'.$i] = $arr_dept[$i];
						array_push($type,'integer');
					} else if ($arr_atrb[$i] != '') {
						$sql .= "    emp.emp_attribute = :emp_attribute_2".$i;
						$param['emp_attribute_2'.$i] = $arr_atrb[$i];
						array_push($type,'integer');
					} else if ($arr_class[$i] != '') {
						$sql .= "    emp.emp_class = :emp_class_2".$i;
						$param['emp_class_2'.$i] = $arr_class[$i];
						array_push($type,'integer');
					}
				}
				$sql .= "   ) ";
			}

			if ($class_division != '' && $class_division != 0) {
				$sql .= "   AND EXISTS ( ";
				$sql .= "    SELECT ";
				$sql .= "     loginemp.emp_id ";
				$sql .= "    FROM ";
				$sql .= "     empmst loginemp ";
				$sql .= "    WHERE ";
				$sql .= "     loginemp.emp_id = :login_emp_id_mst_2 ";

				switch($class_division) {
					case 1:
						$sql .= "     AND loginemp.emp_class = emp.emp_class ";
						break;
					case 2:
						$sql .= "     AND loginemp.emp_attribute = emp.emp_attribute ";
						break;
					case 3:
						$sql .= "     AND loginemp.emp_dept = emp.emp_dept ";
						break;
					case 4:
						$sql .= "     AND loginemp.emp_room = emp.emp_room ";
						break;
				}

				$sql .= "    UNION ALL ";
				$sql .= "    SELECT ";
				$sql .= "     logincon.emp_id ";
				$sql .= "    FROM ";
				$sql .= "     concurrent logincon ";
				$sql .= "    WHERE ";
				$sql .= "     logincon.emp_id = :login_emp_id_con_2 ";

				switch($class_division) {
					case 1:
						$sql .= "     AND logincon.emp_class = emp.emp_class ";
						break;
					case 2:
						$sql .= "     AND logincon.emp_attribute = emp.emp_attribute ";
						break;
					case 3:
						$sql .= "     AND logincon.emp_dept = emp.emp_dept ";
						break;
					case 4:
						$sql .= "     AND logincon.emp_room = emp.emp_room ";
						break;
				}

				$sql .= "   ) ";

				$param['login_emp_id_mst_2'] = $this->user;
				array_push($type,'text');
				$param['login_emp_id_con_2'] = $this->user;
				array_push($type,'text');

			}

			// 2012/08/30 Yamagawa add(s)
			/*
			$sql .= "   AND EXISTS( ";
			$sql .= "    SELECT ";
			$sql .= "     auth.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     authmst auth ";
			$sql .= "    WHERE ";
			$sql .= "     auth.emp_id = emp.emp_id ";
			$sql .= "     AND auth.emp_del_flg = FALSE ";
			$sql .= "   ) ";
			*/
			// 2012/08/30 Yamagawa add(e)
			$sql .= "  ) wk ";
			$sql .= " GROUP BY ";
			$sql .= "  level ";
			$sql .= "  ,levelup_evaluation_id ";
			$sql .= "  ,eval_year ";
			$sql .= " ) wk2 ";
			$sql .= "GROUP BY ";
			$sql .= " level ";
			$sql .= " ,eval_year ";
		}

		$data=parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_item_year_average_evaluation_point() END count(data):".count($data));
		return $data;

	}

	/**
	 * 看護師免許取得経過年人数取得
	 */
	function get_item_license_elapsed_year_emp_cnt(
		$job
		, $level
		, $arr_class
		, $arr_atrb
		, $arr_dept
		, $arr_room
		, $year_from
		, $year_to
		, $class_division
		, $mode
		, $row_1_key
		, $row_2_key
		, $column_1_key
		, $column_2_key
	){

		$this->log->info("get_item_license_elapsed_year_emp_cnt() START");
		$param = array();
		$type = array();

		$sql  = "";
		$sql .= "SELECT ";

		if ($mode == 'analysis') {
			$sql .= " emp_dept ";
			$sql .= " ,now_level ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 0 THEN cnt ELSE 0 END) AS year_0_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 1 THEN cnt ELSE 0 END) AS year_1_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 2 THEN cnt ELSE 0 END) AS year_2_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 3 THEN cnt ELSE 0 END) AS year_3_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 4 THEN cnt ELSE 0 END) AS year_4_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt BETWEEN 5 AND 7 THEN cnt ELSE 0 END) AS year_5_7_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt BETWEEN 8 AND 9 THEN cnt ELSE 0 END) AS year_8_9_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt >= 10 THEN cnt ELSE 0 END) AS year_10_cnt ";
			$sql .= "FROM ";
			$sql .= " (SELECT ";
			$sql .= "  emp_dept ";
			$sql .= "  ,now_level ";
			$sql .= "  ,year_cnt ";
			$sql .= "  ,count(emp_attribute) AS cnt ";
		} else if ($mode == 'emp_list') {
			$sql .= " wk.emp_lt_nm ";
			$sql .= " ,wk.emp_ft_nm ";
			$sql .= " ,class.class_nm ";
			$sql .= " ,atrb.atrb_nm ";
			$sql .= " ,dept.dept_nm ";
			$sql .= " ,room.room_nm ";
		}

		$sql .= " FROM ";
		$sql .= "  (SELECT ";
		$sql .= "   emp.emp_class ";
		$sql .= "   ,emp.emp_attribute ";
		$sql .= "   ,emp.emp_dept ";
		$sql .= "   ,emp.emp_room ";
		$sql .= "   ,emp.emp_id ";
		$sql .= "   ,emp.emp_lt_nm ";
		$sql .= "   ,emp.emp_ft_nm ";
		$sql .= "   ,COALESCE(pro.now_level,0) AS now_level ";
		$sql .= "   ,date_part( ";
		$sql .= "     'year' ";
		$sql .= "     ,age( ";
		$sql .= "       CURRENT_TIMESTAMP ";
		$sql .= "       ,to_date(lic.license_date,'YYYYMMDD') ";
		$sql .= "      ) ";
		$sql .= "    ) AS year_cnt ";
		$sql .= "  FROM ";
		$sql .= "   empmst emp ";

		// 削除されている部署に所属する職員は対象外
		$sql .= "   INNER JOIN ";
		$sql .= "    deptmst dept ";
		$sql .= "   ON ";
		$sql .= "    dept.dept_id = emp.emp_dept ";
		$sql .= "    AND dept.dept_del_flg = FALSE ";

		// 2012/08/31 Yamagawa add(s)
		$sql .= "   INNER JOIN ";
		$sql .= "    jobmst job ";
		$sql .= "   ON ";
		$sql .= "    job.job_id = emp.emp_job ";
		$sql .= "    AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/31 Yamagawa add(e)

		$sql .= "   LEFT JOIN ";
		$sql .= "    cl_personal_profile pro ";
		$sql .= "   ON ";
		$sql .= "    pro.emp_id = emp.emp_id ";

		if ($level != '') {
			$sql .= "  AND pro.now_level = :now_level ";
			$param['now_level'] = $level;
			array_push($type,'integer');
		}

		$sql .= "   INNER JOIN ";
		$sql .= "    jinji_relation rel ";
		$sql .= "   ON ";
		$sql .= "    rel.emp_id = emp.emp_id ";
		$sql .= "   INNER JOIN ";
		$sql .= "    jinji_license lic ";
		$sql .= "   ON ";
		$sql .= "    lic.jinji_id = rel.jinji_id ";
		$sql .= "    AND lic.item_id = 15 ";
		$sql .= "  WHERE ";
		$sql .= "   1 = 1 ";

		if ($job != '') {
			$sql .= " AND emp.emp_job = :emp_job ";
			$param['emp_job'] = $job;
			array_push($type,'integer');
		}

		if (count($arr_class) > 0) {
			$sql .= "   AND ( ";
			for ($i = 0; $i < count($arr_class); $i++) {
				if ($i > 0) {
					$sql .= "    OR ";
				}
				if ($arr_room[$i] != '') {
					$sql .= "    emp.emp_room = :emp_room".$i;
					$param['emp_room'.$i] = $arr_room[$i];
					array_push($type,'integer');
				} else if ($arr_dept[$i] != '') {
					$sql .= "    emp.emp_dept = :emp_dept".$i;
					$param['emp_dept'.$i] = $arr_dept[$i];
					array_push($type,'integer');
				} else if ($arr_atrb[$i] != '') {
					$sql .= "    emp.emp_attribute = :emp_attribute".$i;
					$param['emp_attribute'.$i] = $arr_atrb[$i];
					array_push($type,'integer');
				} else if ($arr_class[$i] != '') {
					$sql .= "    emp.emp_class = :emp_class".$i;
					$param['emp_class'.$i] = $arr_class[$i];
					array_push($type,'integer');
				}
			}
			$sql .= "   ) ";
		}

		if ($class_division != '' && $class_division != 0) {
			$sql .= "   AND EXISTS ( ";
			$sql .= "    SELECT ";
			$sql .= "     loginemp.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     empmst loginemp ";
			$sql .= "    WHERE ";
			$sql .= "     loginemp.emp_id = :login_emp_id_mst ";

			switch($class_division) {
				case 1:
					$sql .= "     AND loginemp.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND loginemp.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND loginemp.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND loginemp.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "    UNION ALL ";
			$sql .= "    SELECT ";
			$sql .= "     logincon.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     concurrent logincon ";
			$sql .= "    WHERE ";
			$sql .= "     logincon.emp_id = :login_emp_id_con ";

			switch($class_division) {
				case 1:
					$sql .= "     AND logincon.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND logincon.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND logincon.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND logincon.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "   ) ";

			$param['login_emp_id_mst'] = $this->user;
			array_push($type,'text');
			$param['login_emp_id_con'] = $this->user;
			array_push($type,'text');
		}

		// 2012/08/30 Yamagawa upd(s)
		/*
		$sql .= "   AND EXISTS( ";
		$sql .= "    SELECT ";
		$sql .= "     auth.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     authmst auth ";
		$sql .= "    WHERE ";
		$sql .= "     auth.emp_id = emp.emp_id ";
		$sql .= "     AND auth.emp_del_flg = FALSE ";
		$sql .= "   ) ";
		*/
		$sql .= "   AND ( ";
		$sql .= "    emp.emp_retire is null ";
		$sql .= "    OR trim(emp.emp_retire) = '' ";
		$sql .= "    OR to_date(trim(emp.emp_retire),'yyyymmdd') >= current_date ";
		$sql .= "   ) ";
		// 2012/08/30 Yamagawa upd(e)
		$sql .= "  ) wk ";

		if ($mode == 'analysis') {
			$sql .= " GROUP BY ";
			$sql .= "  emp_dept ";
			$sql .= "  ,now_level ";
			$sql .= "  ,year_cnt ";
			$sql .= " ) wk2 ";
			$sql .= "GROUP BY ";
			$sql .= " emp_dept ";
			$sql .= " ,now_level ";
		} else if ($mode == 'emp_list') {
			$sql .= " LEFT JOIN ";
			$sql .= "  classmst class ";
			$sql .= " ON ";
			$sql .= "  class.class_id = wk.emp_class ";
			$sql .= " LEFT JOIN ";
			$sql .= "  atrbmst atrb ";
			$sql .= " ON ";
			$sql .= "  atrb.atrb_id = wk.emp_attribute ";
			$sql .= " LEFT JOIN ";
			$sql .= "  deptmst dept ";
			$sql .= " ON ";
			$sql .= "  dept.dept_id = wk.emp_dept ";
			$sql .= " LEFT JOIN ";
			$sql .= "  classroom room ";
			$sql .= " ON ";
			$sql .= "  room.room_id = wk.emp_room ";

			$sql .= "WHERE ";
			$sql .= " 1 = 1 ";

			if ($row_1_key != 'total') {
				$sql .= " AND wk.emp_dept = :row_1_key ";
				$param['row_1_key'] = $row_1_key;
				array_push($type,'integer');
			}

			if ($row_2_key != 'total') {
				$sql .= " AND wk.now_level = :row_2_key ";
				$param['row_2_key'] = $row_2_key;
				array_push($type,'integer');
			}

			if ($column_1_key != 'total') {
				switch($column_1_key){
					case 0:
						$sql .= "  AND wk.year_cnt = 0 ";
						break;
					case 1:
						$sql .= "  AND wk.year_cnt = 1 ";
						break;
					case 2:
						$sql .= "  AND wk.year_cnt = 2 ";
						break;
					case 3:
						$sql .= "  AND wk.year_cnt = 3 ";
						break;
					case 4:
						$sql .= "  AND wk.year_cnt = 4 ";
						break;
					case 5:
						$sql .= "  AND wk.year_cnt BETWEEN 5 AND 7 ";
						break;
					case 6:
						$sql .= "  AND wk.year_cnt BETWEEN 8 AND 9 ";
						break;
					case 7:
						$sql .= "  AND wk.year_cnt >= 10 ";
						break;
				}
			}

			if ($column_2_key != 'total') {
				// 未使用
			}

			$sql .= "ORDER BY ";
			$sql .= " wk.emp_class ";
			$sql .= " ,wk.emp_attribute ";
			$sql .= " ,wk.emp_dept ";
			$sql .= " ,wk.emp_room ";
			$sql .= " ,wk.emp_id ";
		}

		$data=parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_item_license_elapsed_year_emp_cnt() END count(data):".count($data));
		return $data;

	}

	/**
	 * 在職年数別人数取得
	 */
	function get_item_tenure_emp_cnt(
		$job
		, $level
		, $arr_class
		, $arr_atrb
		, $arr_dept
		, $arr_room
		, $year_from
		, $year_to
		, $class_division
		, $mode
		, $row_1_key
		, $row_2_key
		, $column_1_key
		, $column_2_key
	){

		$this->log->info("get_item_tenure_emp_cnt() START");
		$param = array();
		$type = array();

		$sql  = "";
		$sql .= "SELECT ";

		if ($mode == 'analysis') {
			$sql .= " emp_dept ";
			$sql .= " ,now_level ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 0 THEN cnt ELSE 0 END) AS year_0_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 1 THEN cnt ELSE 0 END) AS year_1_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 2 THEN cnt ELSE 0 END) AS year_2_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 3 THEN cnt ELSE 0 END) AS year_3_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 4 THEN cnt ELSE 0 END) AS year_4_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt BETWEEN 5 AND 7 THEN cnt ELSE 0 END) AS year_5_7_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt BETWEEN 8 AND 9 THEN cnt ELSE 0 END) AS year_8_9_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt >= 10 THEN cnt ELSE 0 END) AS year_10_cnt ";
			$sql .= "FROM ";
			$sql .= " (SELECT ";
			$sql .= "  emp_dept ";
			$sql .= "  ,now_level ";
			$sql .= "  ,year_cnt ";
			$sql .= "  ,count(emp_attribute) AS cnt ";
		} else if ($mode == 'emp_list') {
			$sql .= " wk.emp_lt_nm ";
			$sql .= " ,wk.emp_ft_nm ";
			$sql .= " ,class.class_nm ";
			$sql .= " ,atrb.atrb_nm ";
			$sql .= " ,dept.dept_nm ";
			$sql .= " ,room.room_nm ";
		}

		$sql .= " FROM ";
		$sql .= "  (SELECT ";
		$sql .= "   emp.emp_class ";
		$sql .= "   ,emp.emp_attribute ";
		$sql .= "   ,emp.emp_dept ";
		$sql .= "   ,emp.emp_room ";
		$sql .= "   ,emp.emp_id ";
		$sql .= "   ,emp.emp_lt_nm ";
		$sql .= "   ,emp.emp_ft_nm ";
		$sql .= "   ,COALESCE(pro.now_level,0) AS now_level ";
		$sql .= "   ,date_part( ";
		$sql .= "     'year' ";
		$sql .= "     ,age( ";
		$sql .= "       CURRENT_TIMESTAMP ";
		$sql .= "       ,to_date(emp.emp_join,'YYYYMMDD') ";
		$sql .= "      ) ";
		$sql .= "    ) AS year_cnt ";
		$sql .= "  FROM ";
		$sql .= "   empmst emp ";

		// 削除されている部署に所属する職員は対象外
		$sql .= "   INNER JOIN ";
		$sql .= "    deptmst dept ";
		$sql .= "   ON ";
		$sql .= "    dept.dept_id = emp.emp_dept ";
		$sql .= "    AND dept.dept_del_flg = FALSE ";

		// 2012/08/31 Yamagawa add(s)
		$sql .= "   INNER JOIN ";
		$sql .= "    jobmst job ";
		$sql .= "   ON ";
		$sql .= "    job.job_id = emp.emp_job ";
		$sql .= "    AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/31 Yamagawa add(e)

		$sql .= "   LEFT JOIN ";
		$sql .= "    cl_personal_profile pro ";
		$sql .= "   ON ";
		$sql .= "    pro.emp_id = emp.emp_id ";

		if ($level != '') {
			$sql .= "  AND pro.now_level = :now_level ";
			$param['now_level'] = $level;
			array_push($type,'integer');
		}

		$sql .= "  WHERE ";
		$sql .= "   trim(emp.emp_join) != ''  ";

		if ($job != '') {
			$sql .= " AND emp.emp_job = :emp_job ";
			$param['emp_job'] = $job;
			array_push($type,'integer');
		}

		if (count($arr_class) > 0) {
			$sql .= "   AND ( ";
			for ($i = 0; $i < count($arr_class); $i++) {
				if ($i > 0) {
					$sql .= "    OR ";
				}
				if ($arr_room[$i] != '') {
					$sql .= "    emp.emp_room = :emp_room".$i;
					$param['emp_room'.$i] = $arr_room[$i];
					array_push($type,'integer');
				} else if ($arr_dept[$i] != '') {
					$sql .= "    emp.emp_dept = :emp_dept".$i;
					$param['emp_dept'.$i] = $arr_dept[$i];
					array_push($type,'integer');
				} else if ($arr_atrb[$i] != '') {
					$sql .= "    emp.emp_attribute = :emp_attribute".$i;
					$param['emp_attribute'.$i] = $arr_atrb[$i];
					array_push($type,'integer');
				} else if ($arr_class[$i] != '') {
					$sql .= "    emp.emp_class = :emp_class".$i;
					$param['emp_class'.$i] = $arr_class[$i];
					array_push($type,'integer');
				}
			}
			$sql .= "   ) ";
		}

		if ($class_division != '' && $class_division != 0) {
			$sql .= "   AND EXISTS ( ";
			$sql .= "    SELECT ";
			$sql .= "     loginemp.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     empmst loginemp ";
			$sql .= "    WHERE ";
			$sql .= "     loginemp.emp_id = :login_emp_id_mst ";

			switch($class_division) {
				case 1:
					$sql .= "     AND loginemp.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND loginemp.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND loginemp.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND loginemp.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "    UNION ALL ";
			$sql .= "    SELECT ";
			$sql .= "     logincon.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     concurrent logincon ";
			$sql .= "    WHERE ";
			$sql .= "     logincon.emp_id = :login_emp_id_con ";

			switch($class_division) {
				case 1:
					$sql .= "     AND logincon.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND logincon.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND logincon.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND logincon.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "   ) ";

			$param['login_emp_id_mst'] = $this->user;
			array_push($type,'text');
			$param['login_emp_id_con'] = $this->user;
			array_push($type,'text');
		}

		// 2012/08/30 Yamagawa upd(s)
		/*
		$sql .= "   AND EXISTS( ";
		$sql .= "    SELECT ";
		$sql .= "     auth.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     authmst auth ";
		$sql .= "    WHERE ";
		$sql .= "     auth.emp_id = emp.emp_id ";
		$sql .= "     AND auth.emp_del_flg = FALSE ";
		$sql .= "   ) ";
		*/
		$sql .= "   AND ( ";
		$sql .= "    emp.emp_retire is null ";
		$sql .= "    OR trim(emp.emp_retire) = '' ";
		$sql .= "    OR to_date(trim(emp.emp_retire),'yyyymmdd') >= current_date ";
		$sql .= "   ) ";
		// 2012/08/30 Yamagawa upd(e)
		$sql .= "  ) wk ";

		if ($mode == 'analysis') {
			$sql .= " GROUP BY ";
			$sql .= "  emp_dept ";
			$sql .= "  ,now_level ";
			$sql .= "  ,year_cnt ";
			$sql .= " ) wk2 ";
			$sql .= "GROUP BY ";
			$sql .= " emp_dept ";
			$sql .= " ,now_level ";
		} else if ($mode == 'emp_list') {
			$sql .= " LEFT JOIN ";
			$sql .= "  classmst class ";
			$sql .= " ON ";
			$sql .= "  class.class_id = wk.emp_class ";
			$sql .= " LEFT JOIN ";
			$sql .= "  atrbmst atrb ";
			$sql .= " ON ";
			$sql .= "  atrb.atrb_id = wk.emp_attribute ";
			$sql .= " LEFT JOIN ";
			$sql .= "  deptmst dept ";
			$sql .= " ON ";
			$sql .= "  dept.dept_id = wk.emp_dept ";
			$sql .= " LEFT JOIN ";
			$sql .= "  classroom room ";
			$sql .= " ON ";
			$sql .= "  room.room_id = wk.emp_room ";

			$sql .= "WHERE ";
			$sql .= " 1 = 1 ";

			if ($row_1_key != 'total') {
				$sql .= " AND wk.emp_dept = :row_1_key ";
				$param['row_1_key'] = $row_1_key;
				array_push($type,'integer');
			}

			if ($row_2_key != 'total') {
				$sql .= " AND wk.now_level = :row_2_key ";
				$param['row_2_key'] = $row_2_key;
				array_push($type,'integer');
			}

			if ($column_1_key != 'total') {
				switch($column_1_key){
					case 0:
						$sql .= "  AND wk.year_cnt = 0 ";
						break;
					case 1:
						$sql .= "  AND wk.year_cnt = 1 ";
						break;
					case 2:
						$sql .= "  AND wk.year_cnt = 2 ";
						break;
					case 3:
						$sql .= "  AND wk.year_cnt = 3 ";
						break;
					case 4:
						$sql .= "  AND wk.year_cnt = 4 ";
						break;
					case 5:
						$sql .= "  AND wk.year_cnt BETWEEN 5 AND 7 ";
						break;
					case 6:
						$sql .= "  AND wk.year_cnt BETWEEN 8 AND 9 ";
						break;
					case 7:
						$sql .= "  AND wk.year_cnt >= 10 ";
						break;
				}
			}

			if ($column_2_key != 'total') {
				// 未使用
			}

			$sql .= "ORDER BY ";
			$sql .= " wk.emp_class ";
			$sql .= " ,wk.emp_attribute ";
			$sql .= " ,wk.emp_dept ";
			$sql .= " ,wk.emp_room ";
			$sql .= " ,wk.emp_id ";
		}

		$data=parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_item_tenure_emp_cnt() END count(data):".count($data));
		return $data;

	}

	/**
	 * 配属年数別人数取得
	 */
	function get_item_assignment_year_emp_cnt(
		$job
		, $level
		, $arr_class
		, $arr_atrb
		, $arr_dept
		, $arr_room
		, $year_from
		, $year_to
		, $class_division
		, $mode
		, $row_1_key
		, $row_2_key
		, $column_1_key
		, $column_2_key
	){

		$this->log->info("get_item_assignment_year_emp_cnt() START");
		$param = array();
		$type = array();

		$sql  = "";
		$sql .= "SELECT ";

		if ($mode == 'analysis') {
			$sql .= " emp_dept ";
			$sql .= " ,now_level ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 0 THEN cnt ELSE 0 END) AS year_0_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 1 THEN cnt ELSE 0 END) AS year_1_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 2 THEN cnt ELSE 0 END) AS year_2_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 3 THEN cnt ELSE 0 END) AS year_3_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 4 THEN cnt ELSE 0 END) AS year_4_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt BETWEEN 5 AND 7 THEN cnt ELSE 0 END) AS year_5_7_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt BETWEEN 8 AND 9 THEN cnt ELSE 0 END) AS year_8_9_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt >= 10 THEN cnt ELSE 0 END) AS year_10_cnt ";
			$sql .= "FROM ";
			$sql .= " (SELECT ";
			$sql .= "  emp_dept ";
			$sql .= "  ,now_level ";
			$sql .= "  ,year_cnt ";
			$sql .= "  ,count(emp_dept) AS cnt ";
		} else if ($mode == 'emp_list') {
			$sql .= " emp.emp_lt_nm ";
			$sql .= " ,emp.emp_ft_nm ";
			$sql .= " ,class.class_nm ";
			$sql .= " ,atrb.atrb_nm ";
			$sql .= " ,dept.dept_nm ";
			$sql .= " ,room.room_nm ";
		}

		$sql .= " FROM ";
		$sql .= "  (SELECT ";
		$sql .= "   wk2.emp_id ";
		$sql .= "   ,wk2.emp_dept ";
		$sql .= "   ,COALESCE(pro.now_level,0) AS now_level ";
		$sql .= "   ,date_part( ";
		$sql .= "     'year' ";
		$sql .= "     ,age( ";
		$sql .= "       CURRENT_TIMESTAMP ";
		$sql .= "       ,wk2.emp_join ";
		$sql .= "      ) ";
		$sql .= "    ) AS year_cnt ";
		$sql .= "  FROM ";
		$sql .= "   (SELECT ";
		$sql .= "    wk.emp_dept ";
		$sql .= "    ,wk.emp_id ";
		$sql .= "    ,max(wk.emp_join) AS emp_join ";
		$sql .= "   FROM ";
		$sql .= "    (SELECT ";
		$sql .= "     emp.emp_dept ";
		$sql .= "     ,emp.emp_id ";
		$sql .= "     ,CASE ";
		$sql .= "       WHEN ";
		$sql .= "        his.histdate IS NULL ";
		$sql .= "       THEN ";
		$sql .= "        to_date(trim(emp.emp_join),'YYYYMMDD') ";
		$sql .= "       WHEN ";
		$sql .= "        trim(emp.emp_join) = '' ";
		$sql .= "       THEN ";
		$sql .= "        his.histdate ";
		$sql .= "       WHEN ";
		$sql .= "        to_date(trim(emp.emp_join),'YYYYMMDD') >= his.histdate ";
		$sql .= "       THEN ";
		$sql .= "        to_date(trim(emp.emp_join),'YYYYMMDD') ";
		$sql .= "       ELSE ";
		$sql .= "        his.histdate ";
		$sql .= "      END AS emp_join ";
		$sql .= "     ,trim(emp.emp_join) AS def_join ";
		$sql .= "     ,his.histdate ";
		$sql .= "    FROM ";
		$sql .= "     empmst emp ";

		// 削除されている部署に所属する職員は対象外
		$sql .= "   INNER JOIN ";
		$sql .= "    deptmst dept ";
		$sql .= "   ON ";
		$sql .= "    dept.dept_id = emp.emp_dept ";
		$sql .= "    AND dept.dept_del_flg = FALSE ";

		// 2012/08/31 Yamagawa add(s)
		$sql .= "   INNER JOIN ";
		$sql .= "    jobmst job ";
		$sql .= "   ON ";
		$sql .= "    job.job_id = emp.emp_job ";
		$sql .= "    AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/31 Yamagawa add(e)

		$sql .= "     LEFT JOIN ";
		$sql .= "      class_history his ";
		$sql .= "     ON ";
		$sql .= "      his.emp_id = emp.emp_id ";
		$sql .= "    WHERE ";
		$sql .= "     1 = 1 ";

		if ($job != '') {
			$sql .= " AND emp.emp_job = :emp_job ";
			$param['emp_job'] = $job;
			array_push($type,'integer');
		}

		if (count($arr_class) > 0) {
			$sql .= "   AND ( ";
			for ($i = 0; $i < count($arr_class); $i++) {
				if ($i > 0) {
					$sql .= "    OR ";
				}
				if ($arr_room[$i] != '') {
					$sql .= "    emp.emp_room = :emp_room".$i;
					$param['emp_room'.$i] = $arr_room[$i];
					array_push($type,'integer');
				} else if ($arr_dept[$i] != '') {
					$sql .= "    emp.emp_dept = :emp_dept".$i;
					$param['emp_dept'.$i] = $arr_dept[$i];
					array_push($type,'integer');
				} else if ($arr_atrb[$i] != '') {
					$sql .= "    emp.emp_attribute = :emp_attribute".$i;
					$param['emp_attribute'.$i] = $arr_atrb[$i];
					array_push($type,'integer');
				} else if ($arr_class[$i] != '') {
					$sql .= "    emp.emp_class = :emp_class".$i;
					$param['emp_class'.$i] = $arr_class[$i];
					array_push($type,'integer');
				}
			}
			$sql .= "   ) ";
		}

		if ($class_division != '' && $class_division != 0) {
			$sql .= "   AND EXISTS ( ";
			$sql .= "    SELECT ";
			$sql .= "     loginemp.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     empmst loginemp ";
			$sql .= "    WHERE ";
			$sql .= "     loginemp.emp_id = :login_emp_id_mst ";

			switch($class_division) {
				case 1:
					$sql .= "     AND loginemp.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND loginemp.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND loginemp.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND loginemp.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "    UNION ALL ";
			$sql .= "    SELECT ";
			$sql .= "     logincon.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     concurrent logincon ";
			$sql .= "    WHERE ";
			$sql .= "     logincon.emp_id = :login_emp_id_con ";

			switch($class_division) {
				case 1:
					$sql .= "     AND logincon.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND logincon.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND logincon.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND logincon.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "   ) ";

			$param['login_emp_id_mst'] = $this->user;
			array_push($type,'text');
			$param['login_emp_id_con'] = $this->user;
			array_push($type,'text');
		}

		// 2012/08/30 Yamagawa add(s)
		$sql .= "   AND ( ";
		$sql .= "    emp.emp_retire is null ";
		$sql .= "    OR trim(emp.emp_retire) = '' ";
		$sql .= "    OR to_date(trim(emp.emp_retire),'yyyymmdd') >= current_date ";
		$sql .= "   ) ";
		// 2012/08/30 Yamagawa add(e)

		$sql .= "    ) wk ";
		$sql .= "   WHERE ";
		$sql .= "    ( ";
		$sql .= "     wk.def_join != '' ";
		$sql .= "     OR wk.histdate IS NOT NULL ";
		$sql .= "    ) ";
		$sql .= "   GROUP BY ";
		$sql .= "    wk.emp_dept ";
		$sql .= "    ,wk.emp_id ";
		$sql .= "   ) wk2 ";
		$sql .= "   LEFT JOIN ";
		$sql .= "    cl_personal_profile pro ";
		$sql .= "   ON ";
		$sql .= "    pro.emp_id = wk2.emp_id ";

		if ($level != '') {
			$sql .= "  AND pro.now_level = :now_level ";
			$param['now_level'] = $level;
			array_push($type,'integer');
		}

		$sql .= "  ) wk3 ";

		if ($mode == 'analysis') {
			$sql .= " GROUP BY ";
			$sql .= "  emp_dept ";
			$sql .= "  ,now_level ";
			$sql .= "  ,year_cnt ";
			$sql .= " ) wk4 ";
			$sql .= "GROUP BY ";
			$sql .= " emp_dept ";
			$sql .= " ,now_level ";
		} else if ($mode == 'emp_list') {
			$sql .= " INNER JOIN ";
			$sql .= "  empmst emp ";
			$sql .= " ON ";
			$sql .= "  emp.emp_id = wk3.emp_id ";
			$sql .= " LEFT JOIN ";
			$sql .= "  classmst class ";
			$sql .= " ON ";
			$sql .= "  class.class_id = emp.emp_class ";
			$sql .= " LEFT JOIN ";
			$sql .= "  atrbmst atrb ";
			$sql .= " ON ";
			$sql .= "  atrb.atrb_id = emp.emp_attribute ";
			$sql .= " LEFT JOIN ";
			$sql .= "  deptmst dept ";
			$sql .= " ON ";
			$sql .= "  dept.dept_id = emp.emp_dept ";
			$sql .= " LEFT JOIN ";
			$sql .= "  classroom room ";
			$sql .= " ON ";
			$sql .= "  room.room_id = emp.emp_room ";

			$sql .= "WHERE ";
			$sql .= " 1 = 1 ";

			if ($row_1_key != 'total') {
				$sql .= " AND wk3.emp_dept = :row_1_key ";
				$param['row_1_key'] = $row_1_key;
				array_push($type,'integer');
			}

			if ($row_2_key != 'total') {
				$sql .= " AND wk3.now_level = :row_2_key ";
				$param['row_2_key'] = $row_2_key;
				array_push($type,'integer');
			}

			if ($column_1_key != 'total') {
				switch($column_1_key){
					case 0:
						$sql .= "  AND wk3.year_cnt = 0 ";
						break;
					case 1:
						$sql .= "  AND wk3.year_cnt = 1 ";
						break;
					case 2:
						$sql .= "  AND wk3.year_cnt = 2 ";
						break;
					case 3:
						$sql .= "  AND wk3.year_cnt = 3 ";
						break;
					case 4:
						$sql .= "  AND wk3.year_cnt = 4 ";
						break;
					case 5:
						$sql .= "  AND wk3.year_cnt BETWEEN 5 AND 7 ";
						break;
					case 6:
						$sql .= "  AND wk3.year_cnt BETWEEN 8 AND 9 ";
						break;
					case 7:
						$sql .= "  AND wk3.year_cnt >= 10 ";
						break;
				}
			}

			if ($column_2_key != 'total') {
				// 未使用
			}

			$sql .= "ORDER BY ";
			$sql .= " emp.emp_class ";
			$sql .= " ,emp.emp_attribute ";
			$sql .= " ,emp.emp_dept ";
			$sql .= " ,emp.emp_room ";
			$sql .= " ,emp.emp_id ";
		}

		$data=parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_item_assignment_year_emp_cnt() END count(data):".count($data));
		return $data;

	}

	/**
	 * 卒業学校別人数取得
	 */
	function get_item_graduation_emp_cnt(
		$job
		, $level
		, $arr_class
		, $arr_atrb
		, $arr_dept
		, $arr_room
		, $year_from
		, $year_to
		, $class_division
		, $mode
		, $row_1_key
		, $row_2_key
		, $column_1_key
		, $column_2_key
	){

		$this->log->info("get_item_graduation_emp_cnt() START");
		$param = array();
		$type = array();

		$sql  = "";
		$sql .= "SELECT ";

		if ($mode == 'analysis') {
			$sql .= " edu.school_name ";
			$sql .= " ,COALESCE(pro.now_level, 0) AS now_level ";
			$sql .= " ,count(emp.emp_id) AS emp_cnt ";
		} else if ($mode == 'emp_list') {
			$sql .= " emp.emp_lt_nm ";
			$sql .= " ,emp.emp_ft_nm ";
			$sql .= " ,class.class_nm ";
			$sql .= " ,atrb.atrb_nm ";
			$sql .= " ,dept.dept_nm ";
			$sql .= " ,room.room_nm ";
		}

		$sql .= "FROM ";
		$sql .= " empmst emp ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_personal_profile pro ";
		$sql .= " ON ";
		$sql .= "  pro.emp_id = emp.emp_id ";

		if ($level != '') {
			$sql .= "  AND pro.now_level = :now_level ";
			$param['now_level'] = $level;
			array_push($type,'integer');
		}

		$sql .= "  INNER JOIN ";
		$sql .= "  jinji_relation rel ";
		$sql .= " ON ";
		$sql .= "  rel.emp_id = emp.emp_id ";
		$sql .= " INNER JOIN ";
		$sql .= "  jinji_educational_bg edu ";
		$sql .= " ON ";
		$sql .= "  edu.jinji_id = rel.jinji_id ";

		// 2012/08/31 Yamagawa add(s)
		$sql .= "   INNER JOIN ";
		$sql .= "    jobmst job ";
		$sql .= "   ON ";
		$sql .= "    job.job_id = emp.emp_job ";
		$sql .= "    AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/31 Yamagawa add(e)

		if ($mode == 'emp_list') {
			$sql .= " LEFT JOIN ";
			$sql .= "  classmst class ";
			$sql .= " ON ";
			$sql .= "  class.class_id = emp.emp_class ";
			$sql .= " LEFT JOIN ";
			$sql .= "  atrbmst atrb ";
			$sql .= " ON ";
			$sql .= "  atrb.atrb_id = emp.emp_attribute ";
			$sql .= " LEFT JOIN ";
			$sql .= "  deptmst dept ";
			$sql .= " ON ";
			$sql .= "  dept.dept_id = emp.emp_dept ";
			$sql .= " LEFT JOIN ";
			$sql .= "  classroom room ";
			$sql .= " ON ";
			$sql .= "  room.room_id = emp.emp_room ";
		}

		$sql .= "WHERE ";
		$sql .= " 1 = 1 ";

		if ($job != '') {
			$sql .= " AND emp.emp_job = :emp_job ";
			$param['emp_job'] = $job;
			array_push($type,'integer');
		}

		if (count($arr_class) > 0) {
			$sql .= "   AND ( ";
			for ($i = 0; $i < count($arr_class); $i++) {
				if ($i > 0) {
					$sql .= "    OR ";
				}
				if ($arr_room[$i] != '') {
					$sql .= "    emp.emp_room = :emp_room".$i;
					$param['emp_room'.$i] = $arr_room[$i];
					array_push($type,'integer');
				} else if ($arr_dept[$i] != '') {
					$sql .= "    emp.emp_dept = :emp_dept".$i;
					$param['emp_dept'.$i] = $arr_dept[$i];
					array_push($type,'integer');
				} else if ($arr_atrb[$i] != '') {
					$sql .= "    emp.emp_attribute = :emp_attribute".$i;
					$param['emp_attribute'.$i] = $arr_atrb[$i];
					array_push($type,'integer');
				} else if ($arr_class[$i] != '') {
					$sql .= "    emp.emp_class = :emp_class".$i;
					$param['emp_class'.$i] = $arr_class[$i];
					array_push($type,'integer');
				}
			}
			$sql .= "   ) ";
		}

		if ($class_division != '' && $class_division != 0) {
			$sql .= "   AND EXISTS ( ";
			$sql .= "    SELECT ";
			$sql .= "     loginemp.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     empmst loginemp ";
			$sql .= "    WHERE ";
			$sql .= "     loginemp.emp_id = :login_emp_id_mst ";

			switch($class_division) {
				case 1:
					$sql .= "     AND loginemp.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND loginemp.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND loginemp.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND loginemp.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "    UNION ALL ";
			$sql .= "    SELECT ";
			$sql .= "     logincon.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     concurrent logincon ";
			$sql .= "    WHERE ";
			$sql .= "     logincon.emp_id = :login_emp_id_con ";

			switch($class_division) {
				case 1:
					$sql .= "     AND logincon.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND logincon.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND logincon.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND logincon.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "   ) ";

			$param['login_emp_id_mst'] = $this->user;
			array_push($type,'text');
			$param['login_emp_id_con'] = $this->user;
			array_push($type,'text');
		}

		// 2012/08/30 Yamagawa upd(s)
		/*
		$sql .= " AND EXISTS( ";
		$sql .= "  SELECT ";
		$sql .= "   auth.emp_id ";
		$sql .= "  FROM ";
		$sql .= "   authmst auth ";
		$sql .= "  WHERE ";
		$sql .= "   auth.emp_id = emp.emp_id ";
		$sql .= "   AND auth.emp_del_flg = FALSE ";
		$sql .= " ) ";
		*/
		$sql .= "   AND ( ";
		$sql .= "    emp.emp_retire is null ";
		$sql .= "    OR trim(emp.emp_retire) = '' ";
		$sql .= "    OR to_date(trim(emp.emp_retire),'yyyymmdd') >= current_date ";
		$sql .= "   ) ";
		// 2012/08/30 Yamagawa upd(e)

		if ($mode == 'analysis') {
			$sql .= "GROUP BY ";
			$sql .= " edu.school_name ";
			$sql .= " ,COALESCE(pro.now_level, 0) ";
		} else if ($mode == 'emp_list') {
			if ($row_1_key != 'total') {
				$sql .= " AND edu.school_name = :row_1_key ";
				$param['row_1_key'] = $row_1_key;
				array_push($type,'text');
			}

			if ($row_2_key != 'total') {
				// 未使用
			}

			if ($column_1_key != 'total') {
				$sql .= " AND COALESCE(pro.now_level, 0) = :column_1_key ";
				$param['column_1_key'] = $column_1_key;
				array_push($type,'integer');
			}

			if ($column_2_key != 'total') {
				// 未使用
			}

			$sql .= "ORDER BY ";
			$sql .= " emp.emp_class ";
			$sql .= " ,emp.emp_attribute ";
			$sql .= " ,emp.emp_dept ";
			$sql .= " ,emp.emp_room ";
			$sql .= " ,emp.emp_id ";
		}

		$data=parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_item_graduation_emp_cnt() END count(data):".count($data));
		return $data;

	}

	/**
	 * 卒業経過年数別人数取得
	 */
	function get_item_graduation_elapsed_year_emp_cnt(
		$job
		, $level
		, $arr_class
		, $arr_atrb
		, $arr_dept
		, $arr_room
		, $year_from
		, $year_to
		, $class_division
		, $mode
		, $row_1_key
		, $row_2_key
		, $column_1_key
		, $column_2_key
	){

		$this->log->info("get_item_graduation_elapsed_year_emp_cnt() START");
		$param = array();
		$type = array();

		$sql  = "";
		$sql .= "SELECT ";

		if ($mode == 'analysis') {
			$sql .= " school_name ";
			$sql .= " ,now_level ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 0 THEN cnt ELSE 0 END) AS year_0_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 1 THEN cnt ELSE 0 END) AS year_1_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 2 THEN cnt ELSE 0 END) AS year_2_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 3 THEN cnt ELSE 0 END) AS year_3_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt = 4 THEN cnt ELSE 0 END) AS year_4_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt BETWEEN 5 AND 7 THEN cnt ELSE 0 END) AS year_5_7_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt BETWEEN 8 AND 9 THEN cnt ELSE 0 END) AS year_8_9_cnt ";
			$sql .= " ,SUM(CASE WHEN year_cnt >= 10 THEN cnt ELSE 0 END) AS year_10_cnt ";
			$sql .= "FROM ";
			$sql .= " (SELECT ";
			$sql .= "  school_name ";
			$sql .= "  ,now_level ";
			$sql .= "  ,year_cnt ";
			$sql .= "  ,count(school_name) AS cnt ";
		} else if ($mode == 'emp_list') {
			$sql .= " wk.emp_lt_nm ";
			$sql .= " ,wk.emp_ft_nm ";
			$sql .= " ,class.class_nm ";
			$sql .= " ,atrb.atrb_nm ";
			$sql .= " ,dept.dept_nm ";
			$sql .= " ,room.room_nm ";
		}

		$sql .= " FROM ";
		$sql .= "  (SELECT ";
		$sql .= "   edu.school_name ";
		$sql .= "   ,COALESCE(pro.now_level,0) AS now_level ";
		$sql .= "   ,date_part( ";
		$sql .= "     'year' ";
		$sql .= "     ,age( ";
		$sql .= "       CURRENT_TIMESTAMP ";
		$sql .= "       ,to_date(edu.graduation_date,'YYYYMMDD') ";
		$sql .= "      ) ";
		$sql .= "    ) AS year_cnt ";
		$sql .= "   ,emp.emp_class ";
		$sql .= "   ,emp.emp_attribute ";
		$sql .= "   ,emp.emp_dept ";
		$sql .= "   ,emp.emp_room ";
		$sql .= "   ,emp.emp_id ";
		$sql .= "   ,emp.emp_lt_nm ";
		$sql .= "   ,emp.emp_ft_nm ";
		$sql .= "  FROM ";
		$sql .= "   empmst emp ";
		$sql .= "   LEFT JOIN ";
		$sql .= "    cl_personal_profile pro ";
		$sql .= "   ON ";
		$sql .= "    pro.emp_id = emp.emp_id ";

		if ($level != '') {
			$sql .= "  AND pro.now_level = :now_level ";
			$param['now_level'] = $level;
			array_push($type,'integer');
		}

		$sql .= "   INNER JOIN ";
		$sql .= "    jinji_relation rel ";
		$sql .= "   ON ";
		$sql .= "    rel.emp_id = emp.emp_id ";
		$sql .= "   INNER JOIN ";
		$sql .= "    jinji_educational_bg edu ";
		$sql .= "   ON ";
		$sql .= "    edu.jinji_id = rel.jinji_id ";

		// 2012/08/31 Yamagawa add(s)
		$sql .= "   INNER JOIN ";
		$sql .= "    jobmst job ";
		$sql .= "   ON ";
		$sql .= "    job.job_id = emp.emp_job ";
		$sql .= "    AND job.job_nm in ('看護師','助産師','保健師','准看護師') ";
		// 2012/08/31 Yamagawa add(e)

		$sql .= "  WHERE ";
		$sql .= "   1 = 1 ";

		if ($job != '') {
			$sql .= " AND emp.emp_job = :emp_job ";
			$param['emp_job'] = $job;
			array_push($type,'integer');
		}

		if (count($arr_class) > 0) {
			$sql .= "   AND ( ";
			for ($i = 0; $i < count($arr_class); $i++) {
				if ($i > 0) {
					$sql .= "    OR ";
				}
				if ($arr_room[$i] != '') {
					$sql .= "    emp.emp_room = :emp_room".$i;
					$param['emp_room'.$i] = $arr_room[$i];
					array_push($type,'integer');
				} else if ($arr_dept[$i] != '') {
					$sql .= "    emp.emp_dept = :emp_dept".$i;
					$param['emp_dept'.$i] = $arr_dept[$i];
					array_push($type,'integer');
				} else if ($arr_atrb[$i] != '') {
					$sql .= "    emp.emp_attribute = :emp_attribute".$i;
					$param['emp_attribute'.$i] = $arr_atrb[$i];
					array_push($type,'integer');
				} else if ($arr_class[$i] != '') {
					$sql .= "    emp.emp_class = :emp_class".$i;
					$param['emp_class'.$i] = $arr_class[$i];
					array_push($type,'integer');
				}
			}
			$sql .= "   ) ";
		}

		if ($class_division != '' && $class_division != 0) {
			$sql .= "   AND EXISTS ( ";
			$sql .= "    SELECT ";
			$sql .= "     loginemp.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     empmst loginemp ";
			$sql .= "    WHERE ";
			$sql .= "     loginemp.emp_id = :login_emp_id_mst ";

			switch($class_division) {
				case 1:
					$sql .= "     AND loginemp.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND loginemp.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND loginemp.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND loginemp.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "    UNION ALL ";
			$sql .= "    SELECT ";
			$sql .= "     logincon.emp_id ";
			$sql .= "    FROM ";
			$sql .= "     concurrent logincon ";
			$sql .= "    WHERE ";
			$sql .= "     logincon.emp_id = :login_emp_id_con ";

			switch($class_division) {
				case 1:
					$sql .= "     AND logincon.emp_class = emp.emp_class ";
					break;
				case 2:
					$sql .= "     AND logincon.emp_attribute = emp.emp_attribute ";
					break;
				case 3:
					$sql .= "     AND logincon.emp_dept = emp.emp_dept ";
					break;
				case 4:
					$sql .= "     AND logincon.emp_room = emp.emp_room ";
					break;
			}

			$sql .= "   ) ";

			$param['login_emp_id_mst'] = $this->user;
			array_push($type,'text');
			$param['login_emp_id_con'] = $this->user;
			array_push($type,'text');
		}

		// 2012/08/30 Yamagawa upd(s)
		/*
		$sql .= "   AND EXISTS( ";
		$sql .= "    SELECT ";
		$sql .= "     auth.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     authmst auth ";
		$sql .= "    WHERE ";
		$sql .= "     auth.emp_id = emp.emp_id ";
		$sql .= "     AND auth.emp_del_flg = FALSE ";
		$sql .= "   ) ";
		*/
		$sql .= "   AND ( ";
		$sql .= "    emp.emp_retire is null ";
		$sql .= "    OR trim(emp.emp_retire) = '' ";
		$sql .= "    OR to_date(trim(emp.emp_retire),'yyyymmdd') >= current_date ";
		$sql .= "   ) ";
		// 2012/08/30 Yamagawa upd(e)

		$sql .= "  ) wk ";

		if ($mode == 'analysis') {
			$sql .= " GROUP BY ";
			$sql .= "  school_name ";
			$sql .= "  ,now_level ";
			$sql .= "  ,year_cnt ";
			$sql .= " ) wk2 ";
			$sql .= "GROUP BY ";
			$sql .= " school_name ";
			$sql .= " ,now_level ";
		} else if ($mode == 'emp_list') {
			$sql .= " LEFT JOIN ";
			$sql .= "  classmst class ";
			$sql .= " ON ";
			$sql .= "  class.class_id = wk.emp_class ";
			$sql .= " LEFT JOIN ";
			$sql .= "  atrbmst atrb ";
			$sql .= " ON ";
			$sql .= "  atrb.atrb_id = wk.emp_attribute ";
			$sql .= " LEFT JOIN ";
			$sql .= "  deptmst dept ";
			$sql .= " ON ";
			$sql .= "  dept.dept_id = wk.emp_dept ";
			$sql .= " LEFT JOIN ";
			$sql .= "  classroom room ";
			$sql .= " ON ";
			$sql .= "  room.room_id = wk.emp_room ";

			$sql .= "WHERE ";
			$sql .= " 1 = 1 ";

			if ($row_1_key != 'total') {
				$sql .= " AND wk.school_name = :row_1_key ";
				$param['row_1_key'] = $row_1_key;
				array_push($type,'text');
			}

			if ($row_2_key != 'total') {
				$sql .= " AND wk.now_level = :row_2_key ";
				$param['row_2_key'] = $row_2_key;
				array_push($type,'integer');
			}

			if ($column_1_key != 'total') {
				switch($column_1_key){
					case 0:
						$sql .= "  AND wk.year_cnt = 0 ";
						break;
					case 1:
						$sql .= "  AND wk.year_cnt = 1 ";
						break;
					case 2:
						$sql .= "  AND wk.year_cnt = 2 ";
						break;
					case 3:
						$sql .= "  AND wk.year_cnt = 3 ";
						break;
					case 4:
						$sql .= "  AND wk.year_cnt = 4 ";
						break;
					case 5:
						$sql .= "  AND wk.year_cnt BETWEEN 5 AND 7 ";
						break;
					case 6:
						$sql .= "  AND wk.year_cnt BETWEEN 8 AND 9 ";
						break;
					case 7:
						$sql .= "  AND wk.year_cnt >= 10 ";
						break;
				}
			}

			if ($column_2_key != 'total') {
				// 未使用
			}

			$sql .= "ORDER BY ";
			$sql .= " wk.emp_class ";
			$sql .= " ,wk.emp_attribute ";
			$sql .= " ,wk.emp_dept ";
			$sql .= " ,wk.emp_room ";
			$sql .= " ,wk.emp_id ";
		}

		$data=parent::select($sql,$type,$param,MDB2_FETCHMODE_ASSOC);

		$this->log->info("get_item_graduation_elapsed_year_emp_cnt() END count(data):".count($data));
		return $data;

	}
}
