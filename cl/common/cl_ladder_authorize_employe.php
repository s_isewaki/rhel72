<?
//職員ごとのラダー認定状況画面


//ini_set("display_errors","1");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cl_application_list.inc");
require_once("cl_yui_calendar_util.ini");
require_once("cl_application_common.ini");
require_once("cl_common.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_title_name.ini");
require_once("cl_common_log_class.inc");
require_once("cl_smarty_setting.ini");
require_once("Cmx.php");
require_once("MDB2.php");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

foreach( $_REQUEST as $key => $value ) {
	$log->debug('$_REQUEST['.$key.']:'.$_REQUEST[$key],__FILE__,__LINE__);
}


$smarty = cl_get_smarty_obj();
$smarty->template_dir = dirname(__FILE__) . "/cl/view";

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);

$cl_title = cl_title_name();

/**
 * YUIカレンダーに必要な外部ファイルの読み込みを行います。
 */
$yui_cal_part=get_yui_cal_part();
$calendar = '';

/**
 * アプリケーションメニューHTML取得
 */
$aplyctn_mnitm_str=get_applycation_menuitem($session,$fname);

/**
 * 志願申請、評価申請用の志願者取得（申請者名）
 */
$arr_emp = $obj->get_empmst($session);
$emp_id  = $arr_emp["0"]["emp_id"];
$lt_nm  = $arr_emp["0"]["emp_lt_nm"];
$ft_nm  = $arr_emp["0"]["emp_ft_nm"];

$applicant_name = "$lt_nm"." "."$ft_nm";


/**
 * Javascriptコード生成（動的オプション項目変更用）
 */
$js_str = get_js_str($arr_wkfwcatemst,$workflow);

/**
 * データベースクローズ
 */
pg_close($con);

/**
 *
 */
if (
	$_REQUEST['mode'] == 'detail'
	|| $_POST['mode'] == 'detail'
) {
	$mode = 'detail';
} else {
	$mode = '';
}


if ($_REQUEST['emp_id'] != ''){
	$target_emp_id = $_REQUEST['emp_id'];
} else if ($_POST['target_emp_id'] != '') {
	$target_emp_id = $_POST['target_emp_id'];
} else {
	$target_emp_id = $emp_id;
}


//##########################################################################
// データベース接続オブジェクト取得
//##########################################################################
$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
}
$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

require_once("cl/model/search/cl_ladder_authorize_employe_model.php");
$progress_model = new cl_ladder_authorize_employe_model($mdb2,$emp_id);
$log->debug("研修受講進捗用DBA取得",__FILE__,__LINE__);

$log->debug('院外研修モデル読込　開始',__FILE__,__LINE__);
require_once("cl/model/ladder/cl_personal_profile_model.php");
$profile_model = new cl_personal_profile_model($mdb2, $emp_id);
$log->debug('院外研修モデル読込　終了',__FILE__,__LINE__);






//========================================
// 権限判定(認定項目の編集可否・認定証発行可否)
//========================================
//※当画面は看護教育委員会、審議会、看護部長、所属長がアクセス可能

// 2012/08/21 Yamagawa del(s)
//require_once("cl/model/master/cl_mst_nurse_education_committee_model.php");
//$education_committee_model = new cl_mst_nurse_education_committee_model($mdb2,$emp_id);
//$log->debug("看護教育委員会DBA取得",__FILE__,__LINE__);
// 2012/08/21 Yamagawa del(e)

require_once("cl/model/master/cl_mst_setting_council_model.php");
$council_model = new cl_mst_setting_council_model($mdb2,$emp_id);
$log->debug("審議会DBA取得",__FILE__,__LINE__);

// 2012/08/21 Yamagawa del(s)
/*
require_once("cl/model/master/cl_mst_nurse_manager_st_model.php");
$manager_st_model = new cl_mst_nurse_manager_st_model($mdb2,$emp_id);
$log->debug("所属長DBA取得",__FILE__,__LINE__);

require_once("cl/model/master/cl_mst_supervisor_st_model.php");
$supervisor_st_model = new cl_mst_supervisor_st_model($mdb2,$emp_id);
$log->debug("看護部長DBA取得",__FILE__,__LINE__);

// 看護教育委員会チェック
$education_committee = $education_committee_model->check_education_committee($emp_id);
*/
// 2012/08/21 Yamagawa del(e)

// 審議会チェック
$council = $council_model->check_council($emp_id);

// 2012/08/21 Yamagawa del(s)
/*
// 所属長(役職)チェック
$supervisor = $supervisor_st_model->check_supervisor($emp_id);

// 看護部長(役職)チェック
$nurse_manager = $manager_st_model->check_nurse_manager($emp_id);

//看護教育委員会、審議会、看護部長の場合
if (count($education_committee) > 0 || count($council) > 0 ||  count($nurse_manager) > 0)
{
	//レベルV認定の編集が可能
	$level5_edit_flg = true;
}
//所属長の場合(看護教育委員会、審議会、看護部長と兼務していない場合)
else
{
	//レベルV認定の編集が不可能
	$level5_edit_flg = false;
}
*/
// 2012/08/21 Yamagawa del(e)

// 2012/08/21 Yamagawa add(s)
if (count($council) > 0) {
	$council_flg = true;
} else {
	$council_flg = false;
}
// 2012/08/21 Yamagawa add(e)

if ($_REQUEST['print_flg'] == 'update') {
	// トランザクション開始
	$mdb2->beginTransaction();
	$log->debug("トランザクション開始",__FILE__,__LINE__);

	$param = array(
		"ladder_data"		=> $_REQUEST['ladder_data']
	);

	$res = $profile_model->update_print($param);

	// コミット
	$mdb2->commit();
	$log->debug("コミット",__FILE__,__LINE__);

}

if ($_REQUEST['get_level_date_regist'] == 'true') {

	// トランザクション開始
	$mdb2->beginTransaction();
	$log->debug("トランザクション開始",__FILE__,__LINE__);

	$data=$profile_model->select($target_emp_id);

	if (count($data) == 0) {
		$get_level1_date_db = '';
		$get_level2_date_db = '';
		$get_level3_date_db = '';
		$get_level4_date_db = '';
		$get_level5_date_db = '';
	} else {
		$get_level1_date_db = $data['get_level1_date'];
		$get_level2_date_db = $data['get_level2_date'];
		$get_level3_date_db = $data['get_level3_date'];
		$get_level4_date_db = $data['get_level4_date'];
		$get_level5_date_db = $data['get_level5_date'];
	}

	$set_now_level = 0;

	if ($_POST['ladderlevel'] == '1')
	{
		if (
			$_POST['auth_year_level1'] == ''
			|| $_POST['auth_month_level1'] == ''
			|| $_POST['auth_day_level1'] == ''
			|| $_POST['auth_year_level1'] == '-'
			|| $_POST['auth_month_level1'] == '-'
			|| $_POST['auth_day_level1'] == '-'
		) {
			$set_level_date = '';
		} else {
			$set_level_date = $_POST['auth_year_level1'].'-'. $_POST['auth_month_level1'].'-'.$_POST['auth_day_level1'];
		}
		$get_level1_date_db = $set_level_date;

		if (
			$_POST['certificate_level1_y'] == ''
			|| $_POST['certificate_level1_m'] == ''
			|| $_POST['certificate_level1_d'] == ''
			|| $_POST['certificate_level1_y'] == '-'
			|| $_POST['certificate_level1_m'] == '-'
			|| $_POST['certificate_level1_d'] == '-'
		){
			$set_certificate_level_date = null;
		} else {
			$set_certificate_level_date = $_POST['certificate_level1_y'].'-'. $_POST['certificate_level1_m'].'-'.$_POST['certificate_level1_d'];
		}

		if ($_REQUEST['level1_total_value'] == '')
		{
			$set_level_total_value = null;
		} else {
			$set_level_total_value = $_REQUEST['level1_total_value'];
		}
	}

	if ($_POST['ladderlevel'] == '2')
	{
		if (
			$_POST['auth_year_level2'] == ''
			|| $_POST['auth_month_level2'] == ''
			|| $_POST['auth_day_level2'] == ''
			|| $_POST['auth_year_level2'] == '-'
			|| $_POST['auth_month_level2'] == '-'
			|| $_POST['auth_day_level2'] == '-'
		) {
			$set_level_date = '';
		} else {
			$set_level_date = $_POST['auth_year_level2'].'-'. $_POST['auth_month_level2'].'-'.$_POST['auth_day_level2'];
		}
		$get_level2_date_db = $set_level_date;

		if (
			$_POST['certificate_level2_y'] == ''
			|| $_POST['certificate_level2_m'] == ''
			|| $_POST['certificate_level2_d'] == ''
			|| $_POST['certificate_level2_y'] == '-'
			|| $_POST['certificate_level2_m'] == '-'
			|| $_POST['certificate_level2_d'] == '-'
		){
			$set_certificate_level_date = '';
		} else {
			$set_certificate_level_date = $_POST['certificate_level2_y'].'-'. $_POST['certificate_level2_m'].'-'.$_POST['certificate_level2_d'];
		}

		if ($_REQUEST['level2_total_value'] == '')
		{
			$set_level_total_value = '';
		} else {
			$set_level_total_value = $_REQUEST['level2_total_value'];
		}
	}

	if ($_POST['ladderlevel'] == '3')
	{
		if (
			$_POST['auth_year_level3'] == ''
			|| $_POST['auth_month_level3'] == ''
			|| $_POST['auth_day_level3'] == ''
			|| $_POST['auth_year_level3'] == '-'
			|| $_POST['auth_month_level3'] == '-'
			|| $_POST['auth_day_level3'] == '-'
		) {
			$set_level_date = '';
		} else {
			$set_level_date = $_POST['auth_year_level3'].'-'. $_POST['auth_month_level3'].'-'.$_POST['auth_day_level3'];
		}
		$get_level3_date_db = $set_level_date;

		if (
			$_POST['certificate_level3_y'] == ''
			|| $_POST['certificate_level3_m'] == ''
			|| $_POST['certificate_level3_d'] == ''
			|| $_POST['certificate_level3_y'] == '-'
			|| $_POST['certificate_level3_m'] == '-'
			|| $_POST['certificate_level3_d'] == '-'
		){
			$set_certificate_level_date = '';
		} else {
			$set_certificate_level_date = $_POST['certificate_level3_y'].'-'. $_POST['certificate_level3_m'].'-'.$_POST['certificate_level3_d'];
		}

		if ($_REQUEST['level3_total_value'] == '')
		{
			$set_level_total_value = '';
		} else {
			$set_level_total_value = $_REQUEST['level3_total_value'];
		}
	}

	if ($_POST['ladderlevel'] == '4')
	{
		if (
			$_POST['auth_year_level4'] == ''
			|| $_POST['auth_month_level4'] == ''
			|| $_POST['auth_day_level4'] == ''
			|| $_POST['auth_year_level4'] == '-'
			|| $_POST['auth_month_level4'] == '-'
			|| $_POST['auth_day_level4'] == '-'
		) {
			$set_level_date = '';
		} else {
			$set_level_date = $_POST['auth_year_level4'].'-'. $_POST['auth_month_level4'].'-'.$_POST['auth_day_level4'];
		}
		$get_level4_date_db = $set_level_date;

		if (
			$_POST['certificate_level4_y'] == ''
			|| $_POST['certificate_level4_m'] == ''
			|| $_POST['certificate_level4_d'] == ''
			|| $_POST['certificate_level4_y'] == '-'
			|| $_POST['certificate_level4_m'] == '-'
			|| $_POST['certificate_level4_d'] == '-'
		){
			$set_certificate_level_date = '';
		} else {
			$set_certificate_level_date = $_POST['certificate_level4_y'].'-'. $_POST['certificate_level4_m'].'-'.$_POST['certificate_level4_d'];
		}

		if ($_REQUEST['level4_total_value'] == '')
		{
			$set_level_total_value = '';
		} else {
			$set_level_total_value = $_REQUEST['level4_total_value'];
		}
	}

	if ($_POST['ladderlevel'] == '5')
	{
		if (
			$_POST['auth_year_level5'] == ''
			|| $_POST['auth_month_level5'] == ''
			|| $_POST['auth_day_level5'] == ''
			|| $_POST['auth_year_level5'] == '-'
			|| $_POST['auth_month_level5'] == '-'
			|| $_POST['auth_day_level5'] == '-'
		) {
			$set_level_date = '';
		} else {
			$set_level_date = $_POST['auth_year_level5'].'-'. $_POST['auth_month_level5'].'-'.$_POST['auth_day_level5'];
		}
		$get_level5_date_db = $set_level_date;

		if (
			$_POST['certificate_level5_y'] == ''
			|| $_POST['certificate_level5_m'] == ''
			|| $_POST['certificate_level5_d'] == ''
			|| $_POST['certificate_level5_y'] == '-'
			|| $_POST['certificate_level5_m'] == '-'
			|| $_POST['certificate_level5_d'] == '-'
		){
			$set_certificate_level_date = '';
		} else {
			$set_certificate_level_date = $_POST['certificate_level5_y'].'-'. $_POST['certificate_level5_m'].'-'.$_POST['certificate_level5_d'];
		}

		if ($_REQUEST['level5_total_value'] == '')
		{
			$set_level_total_value = '';
		} else {
			$set_level_total_value = $_REQUEST['level5_total_value'];
		}
	}

	if ($get_level5_date_db != '') {
		$set_now_level = '5';
	} elseif ($get_level4_date_db != '') {
		$set_now_level = '4';
	} elseif ($get_level3_date_db != '') {
		$set_now_level = '3';
	} elseif ($get_level2_date_db != '') {
		$set_now_level = '2';
	} elseif ($get_level1_date_db != '') {
		$set_now_level = '1';
	} else {
		$set_now_level = '0';
	}

	//2012/05/16 K.Fujii ins(s)
	//職員情報取得
	$log->debug("require START cl_emp_class_history_list_model.php",__FILE__,__LINE__);
	require_once("cl/model/search/cl_emp_class_history_list_model.php");
	$log->debug("require END cl_emp_class_history_list_model.php",__FILE__,__LINE__);

	$log->debug("cl_emp_class_history_list_model インスタンス作成開始",__FILE__,__LINE__);
	$cl_emp_class_history_list_model = new cl_emp_class_history_list_model($mdb2, $user);
	$log->debug("cl_emp_class_history_list_model インスタンス作成終了",__FILE__,__LINE__);

	$log->debug("職員情報取得開始",__FILE__,__LINE__);
	$log->debug("emp_id:".$target_emp_id,__FILE__,__LINE__);
	$emp_data = $cl_emp_class_history_list_model->get_emp_data_history($target_emp_id);
	$log->debug("職員情報取得終了",__FILE__,__LINE__);
	//認定時の所属設定
	if(count($emp_data) == 1){
		if ($set_level_date != ""){
			if($emp_data[0]["histdate"] == "" || $set_level_date >= $emp_data[0]["histdate"]){
				$level_class = $emp_data[0]["emp_class"];
				$level_attribute = $emp_data[0]["emp_attribute"];
				$level_dept = $emp_data[0]["emp_dept"];
				$level_room = $emp_data[0]["emp_room"];
			}else{
				$level_class = $emp_data[0]["his_class"];
				$level_attribute = $emp_data[0]["his_atrib"];
				$level_dept = $emp_data[0]["his_dept"];
				$level_room = $emp_data[0]["his_room"];
			}
		}
	}else{
		$last_his_date = "";
		for($i=0;$i<count($emp_data);$i++){
			if($set_level_date != ""){
				if($i == 0 && $set_level_date < $emp_data[$i]["histdate"]){
					$level_class = $emp_data[$i]["his_class"];
					$level_attribute = $emp_data[$i]["his_atrib"];
					$level_dept = $emp_data[$i]["his_dept"];
					$level_room = $emp_data[$i]["his_room"];
				}
				if($last_his_date <= $set_level_date && $set_level_date < $emp_data[$i]["histdate"]){
					$level_class = $emp_data[$i]["his_class"];
					$level_attribute = $emp_data[$i]["his_atrib"];
					$level_dept = $emp_data[$i]["his_dept"];
					$level_room = $emp_data[$i]["his_room"];
				}
			}
			$last_his_date = $emp_data[$i]["histdate"];
		}
		if($set_level_date != "" && $level_class == ""){
			$level_class = $emp_data[0]["emp_class"];
			$level_attribute = $emp_data[0]["emp_attribute"];
			$level_dept = $emp_data[0]["emp_dept"];
			$level_room = $emp_data[0]["emp_room"];
		}
	}

	$level_class != "" ? $level_class : null;
	$level_attribute != "" ? $level_attribute : null;
	$level_dept != "" ? $level_dept : null;
	$level_room != "" ? $level_room : null;
	//2012/05/16 K.Fujii ins(e)

	$param = array(
		"emp_id"						=> $target_emp_id
		,"now_level"					=> $set_now_level
		,"get_level_total_value"		=> $set_level_total_value
		,"get_level_date"				=> $set_level_date
		,"get_level_certificate_date"	=> $set_certificate_level_date
		,"ladderlevel"					=> $ladderlevel
		//2012/05/16 K.Fujii ins(s)
		,"level_class"					=> $level_class
		,"level_attribute"				=> $level_attribute
		,"level_dept"					=> $level_dept
		,"level_room"					=> $level_room
		//2012/05/16 K.Fujii ins(e)
	);

	if (count($data) == 0) {
		$res = $profile_model->insert_ladderlevel($param);
	} else {
		$res = $profile_model->update_ladderlevel($param);
	}

	// コミット
	$mdb2->commit();
	$log->debug("コミット",__FILE__,__LINE__);

}

/**
 * ラダーレベル取得
 */
//2012/05/15 K.Fujii upd(s)
//$data_level = $progress_model->get_level_list($target_emp_id);
$data_level = $progress_model->get_emp_level_list($target_emp_id);
//2012/05/15 K.Fujii upd(e)
$emp_full_nm = $data_level[0]['emp_lt_nm'].' '.$data_level[0]['emp_ft_nm'];

$class_nm  = $data_level[0]['class_nm'];
$class_nm .= ' > ';
$class_nm .= $data_level[0]['atrb_nm'];
$class_nm .= ' > ';
$class_nm .= $data_level[0]['dept_nm'];
if ($data_level[0]['room_nm'] != '') {
	$class_nm .= ' > ';
	$class_nm .= $data_level[0]['class_nm'];
}

//2012/05/15 K.Fujii del(s)
/*if ($data_level[0]['get_level1_date'] != ''){
	$level1_year = substr($data_level[0]['get_level1_date'],0,4);
	$level1_month = substr($data_level[0]['get_level1_date'],5,2);
	$level1_day = substr($data_level[0]['get_level1_date'],8,2);
	$level1_date = $level1_year.'/'.$level1_month.'/'.$level1_day;
} else {
	$level1_year = '';
	$level1_month = '';
	$level1_day = '';
	$level1_date = '';
}

if ($mode == 'detail') {
	$option_level1_year  = cl_get_select_years($level1_year);
	$option_level1_month = cl_get_select_months($level1_month);
	$option_level1_day = cl_get_select_days($level1_day);
	$calendar .= write_calendar('level1', 'auth_year_level1', 'auth_month_level1', 'auth_day_level1');
}

if ($data_level[0]['level1_certificate_date'] != ''){
	$certificate_level1_y = substr($data_level[0]['level1_certificate_date'],0,4);
	$certificate_level1_m = substr($data_level[0]['level1_certificate_date'],5,2);
	$certificate_level1_d = substr($data_level[0]['level1_certificate_date'],8,2);
	$certificate_level1_date = $certificate_level1_y.'/'.$certificate_level1_m.'/'.$certificate_level1_d;
} else {
	$certificate_level1_y = '';
	$certificate_level1_m = '';
	$certificate_level1_d = '';
	$certificate_level1_date = '';
}

if ($mode == 'detail') {
	$option_certificate_level1_y  = cl_get_select_years($certificate_level1_y);
	$option_certificate_level1_m = cl_get_select_months($certificate_level1_m);
	$option_certificate_level1_d = cl_get_select_days($certificate_level1_d);
	$calendar .= write_calendar('certificate_level1', 'certificate_level1_y', 'certificate_level1_m', 'certificate_level1_d');
}

if ($data_level[0]['get_level2_date'] != ''){
	$level2_year = substr($data_level[0]['get_level2_date'],0,4);
	$level2_month = substr($data_level[0]['get_level2_date'],5,2);
	$level2_day = substr($data_level[0]['get_level2_date'],8,2);
	$level2_date = $level2_year.'/'.$level2_month.'/'.$level2_day;
} else {
	$level2_year = '';
	$level2_month = '';
	$level2_day = '';
	$level2_date = '';
}

if ($mode == 'detail') {
	$option_level2_year  = cl_get_select_years($level2_year);
	$option_level2_month = cl_get_select_months($level2_month);
	$option_level2_day = cl_get_select_days($level2_day);
	$calendar .= write_calendar('level2', 'auth_year_level2', 'auth_month_level2', 'auth_day_level2');
}

if ($data_level[0]['level2_certificate_date'] != ''){
	$certificate_level2_y = substr($data_level[0]['level2_certificate_date'],0,4);
	$certificate_level2_m = substr($data_level[0]['level2_certificate_date'],5,2);
	$certificate_level2_d = substr($data_level[0]['level2_certificate_date'],8,2);
	$certificate_level2_date = $certificate_level2_y.'/'.$certificate_level2_m.'/'.$certificate_level2_d;
} else {
	$certificate_level2_y = '';
	$certificate_level2_m = '';
	$certificate_level2_d = '';
	$certificate_level2_date = '';
}

if ($mode == 'detail') {
	$option_certificate_level2_y  = cl_get_select_years($certificate_level2_y);
	$option_certificate_level2_m = cl_get_select_months($certificate_level2_m);
	$option_certificate_level2_d = cl_get_select_days($certificate_level2_d);
	$calendar .= write_calendar('certificate_level2', 'certificate_level2_y', 'certificate_level2_m', 'certificate_level2_d');
}

if ($data_level[0]['get_level3_date'] != ''){
	$level3_year = substr($data_level[0]['get_level3_date'],0,4);
	$level3_month = substr($data_level[0]['get_level3_date'],5,2);
	$level3_day = substr($data_level[0]['get_level3_date'],8,2);
	$level3_date = $level3_year.'/'.$level3_month.'/'.$level3_day;
} else {
	$level3_year = '';
	$level3_month = '';
	$level3_day = '';
	$level3_date = '';
}

if ($mode == 'detail') {
	$option_level3_year  = cl_get_select_years($level3_year);
	$option_level3_month = cl_get_select_months($level3_month);
	$option_level3_day = cl_get_select_days($level3_day);
	$calendar .= write_calendar('level3', 'auth_year_level3', 'auth_month_level3', 'auth_day_level3');
}

if ($data_level[0]['level3_certificate_date'] != ''){
	$certificate_level3_y = substr($data_level[0]['level3_certificate_date'],0,4);
	$certificate_level3_m = substr($data_level[0]['level3_certificate_date'],5,2);
	$certificate_level3_d = substr($data_level[0]['level3_certificate_date'],8,2);
	$certificate_level3_date = $certificate_level3_y.'/'.$certificate_level3_m.'/'.$certificate_level3_d;
} else {
	$certificate_level3_y = '';
	$certificate_level3_m = '';
	$certificate_level3_d = '';
	$certificate_level3_date = '';
}

if ($mode == 'detail') {
	$option_certificate_level3_y  = cl_get_select_years($certificate_level3_y);
	$option_certificate_level3_m = cl_get_select_months($certificate_level3_m);
	$option_certificate_level3_d = cl_get_select_days($certificate_level3_d);
	$calendar .= write_calendar('certificate_level3', 'certificate_level3_y', 'certificate_level3_m', 'certificate_level3_d');
}

if ($data_level[0]['get_level4_date'] != ''){
	$level4_year = substr($data_level[0]['get_level4_date'],0,4);
	$level4_month = substr($data_level[0]['get_level4_date'],5,2);
	$level4_day = substr($data_level[0]['get_level4_date'],8,2);
	$level4_date = $level4_year.'/'.$level4_month.'/'.$level4_day;
} else {
	$level4_year = '';
	$level4_month = '';
	$level4_day = '';
	$level4_date = '';
}

if ($mode == 'detail') {
	$option_level4_year  = cl_get_select_years($level4_year);
	$option_level4_month = cl_get_select_months($level4_month);
	$option_level4_day = cl_get_select_days($level4_day);
	$calendar .= write_calendar('level4', 'auth_year_level4', 'auth_month_level4', 'auth_day_level4');
}

if ($data_level[0]['level4_certificate_date'] != ''){
	$certificate_level4_y = substr($data_level[0]['level4_certificate_date'],0,4);
	$certificate_level4_m = substr($data_level[0]['level4_certificate_date'],5,2);
	$certificate_level4_d = substr($data_level[0]['level4_certificate_date'],8,2);
	$certificate_level4_date = $certificate_level4_y.'/'.$certificate_level4_m.'/'.$certificate_level4_d;
} else {
	$certificate_level4_y = '';
	$certificate_level4_m = '';
	$certificate_level4_d = '';
	$certificate_level4_date = '';
}

if ($mode == 'detail') {
	$option_certificate_level4_y  = cl_get_select_years($certificate_level4_y);
	$option_certificate_level4_m = cl_get_select_months($certificate_level4_m);
	$option_certificate_level4_d = cl_get_select_days($certificate_level4_d);
	$calendar .= write_calendar('certificate_level4', 'certificate_level4_y', 'certificate_level4_m', 'certificate_level4_d');
}

if ($data_level[0]['get_level5_date'] != ''){
	$level5_year = substr($data_level[0]['get_level5_date'],0,4);
	$level5_month = substr($data_level[0]['get_level5_date'],5,2);
	$level5_day = substr($data_level[0]['get_level5_date'],8,2);
	$level5_date = $level5_year.'/'.$level5_month.'/'.$level5_day;
} else {
	$level5_year = '';
	$level5_month = '';
	$level5_day = '';
	$level5_date = '';
}

if ($mode == 'detail') {
	$option_level5_year  = cl_get_select_years($level5_year);
	$option_level5_month = cl_get_select_months($level5_month);
	$option_level5_day = cl_get_select_days($level5_day);
	$calendar .= write_calendar('level5', 'auth_year_level5', 'auth_month_level5', 'auth_day_level5');
}

if ($data_level[0]['level5_certificate_date'] != ''){
	$certificate_level5_y = substr($data_level[0]['level5_certificate_date'],0,4);
	$certificate_level5_m = substr($data_level[0]['level5_certificate_date'],5,2);
	$certificate_level5_d = substr($data_level[0]['level5_certificate_date'],8,2);
	$certificate_level5_date = $certificate_level5_y.'/'.$certificate_level5_m.'/'.$certificate_level5_d;
} else {
	$certificate_level5_y = '';
	$certificate_level5_m = '';
	$certificate_level5_d = '';
	$certificate_level5_date = '';
}

if ($mode == 'detail') {
	$option_certificate_level5_y  = cl_get_select_years($certificate_level5_y);
	$option_certificate_level5_m = cl_get_select_months($certificate_level5_m);
	$option_certificate_level5_d = cl_get_select_days($certificate_level5_d);
	$calendar .= write_calendar('certificate_level5', 'certificate_level5_y', 'certificate_level5_m', 'certificate_level5_d');
}

if ($data_level[0]['level1_total_value'] != ''){
	$level1_total_value = $data_level[0]['level1_total_value'];
} else {
	$level1_total_value = '';
}

if ($data_level[0]['level2_total_value'] != ''){
	$level2_total_value = $data_level[0]['level2_total_value'];
} else {
	$level2_total_value = '';
}

if ($data_level[0]['level3_total_value'] != ''){
	$level3_total_value = $data_level[0]['level3_total_value'];
} else {
	$level3_total_value = '';
}

if ($data_level[0]['level4_total_value'] != ''){
	$level4_total_value = $data_level[0]['level4_total_value'];
} else {
	$level4_total_value = '';
}

if ($data_level[0]['level5_total_value'] != ''){
	$level5_total_value = $data_level[0]['level5_total_value'];
} else {
	$level5_total_value = '';
}*/
//2012/05/15 K.Fujii del(e)

//2012/05/15 K.Fujii ins(s)
$level1_cat_cnt = 0;
$level2_cat_cnt = 0;
$level3_cat_cnt = 0;
$level4_cat_cnt = 0;
$level5_cat_cnt = 0;
for($i=0;$i<count($data_level);$i++){

	if($data_level[$i]['level'] == "1" && $level1_class_nm == "" && $data_level[$i]['class_nm_levelup'] != ""){
		$level1_class_nm  = $data_level[$i]['class_nm_levelup'];
		$level1_class_nm .= ' > ';
		$level1_class_nm .= $data_level[$i]['atrb_nm_levelup'];
		$level1_class_nm .= ' > ';
		$level1_class_nm .= $data_level[$i]['dept_nm_levelup'];
		if ($data_level[0]['room_nm'] != '') {
			$level1_class_nm .= ' > ';
			$level1_class_nm .= $data_level[$i]['class_nm_levelup'];
		}
	}

	if($data_level[$i]['level'] == "2" && $level2_class_nm == "" && $data_level[$i]['class_nm_levelup'] != ""){
		$level2_class_nm  = $data_level[$i]['class_nm_levelup'];
		$level2_class_nm .= ' > ';
		$level2_class_nm .= $data_level[$i]['atrb_nm_levelup'];
		$level2_class_nm .= ' > ';
		$level2_class_nm .= $data_level[$i]['dept_nm_levelup'];
		if ($data_level[$i]['room_nm'] != '') {
			$level2_class_nm .= ' > ';
			$level2_class_nm .= $data_level[$i]['class_nm_levelup'];
		}
	}

	if($data_level[$i]['level'] == "3" && $level3_class_nm == "" && $data_level[$i]['class_nm_levelup'] != ""){
		$level3_class_nm  = $data_level[$i]['class_nm_levelup'];
		$level3_class_nm .= ' > ';
		$level3_class_nm .= $data_level[$i]['atrb_nm_levelup'];
		$level3_class_nm .= ' > ';
		$level3_class_nm .= $data_level[$i]['dept_nm_levelup'];
		if ($data_level[0]['room_nm'] != '') {
			$level3_class_nm .= ' > ';
			$level3_class_nm .= $data_level[$i]['class_nm_levelup'];
		}
	}

	if($data_level[$i]['level'] == "4" && $level4_class_nm == "" && $data_level[$i]['class_nm_levelup'] != ""){
		$level4_class_nm  = $data_level[$i]['class_nm_levelup'];
		$level4_class_nm .= ' > ';
		$level4_class_nm .= $data_level[$i]['atrb_nm_levelup'];
		$level4_class_nm .= ' > ';
		$level4_class_nm .= $data_level[$i]['dept_nm_levelup'];
		if ($data_level[0]['room_nm'] != '') {
			$level4_class_nm .= ' > ';
			$level4_class_nm .= $data_level[$i]['class_nm_levelup'];
		}
	}

	if($data_level[$i]['level'] == "5" && $level5_class_nm == "" && $data_level[$i]['class_nm_levelup'] != ""){
		$level5_class_nm  = $data_level[$i]['class_nm_levelup'];
		$level5_class_nm .= ' > ';
		$level5_class_nm .= $data_level[$i]['atrb_nm_levelup'];
		$level5_class_nm .= ' > ';
		$level5_class_nm .= $data_level[$i]['dept_nm_levelup'];
		if ($data_level[0]['room_nm'] != '') {
			$level5_class_nm .= ' > ';
			$level5_class_nm .= $data_level[$i]['class_nm_levelup'];
		}
	}

	if($data_level[$i]['level'] == "1" && $level1_year == ""){
		if ($data_level[$i]['get_level_date'] != ''){
			$level1_year = substr($data_level[$i]['get_level_date'],0,4);
			$level1_month = substr($data_level[$i]['get_level_date'],5,2);
			$level1_day = substr($data_level[$i]['get_level_date'],8,2);
			$level1_date = $level1_year.'/'.$level1_month.'/'.$level1_day;
		} else {
			$level1_year = '';
			$level1_month = '';
			$level1_day = '';
			$level1_date = '';
		}
	}

	// 2012/07/13 Yamagawa upd(s)
	/*
	if($data_level[$i]['level'] == "1" && $certificate_level1_y == ""){
		if ($data_level[$i]['certificate_date'] != ''){
			$certificate_level1_y = substr($data_level[$i]['certificate_date'],0,4);
			$certificate_level1_m = substr($data_level[$i]['certificate_date'],5,2);
			$certificate_level1_d = substr($data_level[$i]['certificate_date'],8,2);
			$certificate_level1_date = $certificate_level1_y.'/'.$certificate_level1_m.'/'.$certificate_level1_d;
		} else {
			$certificate_level1_y = '';
			$certificate_level1_m = '';
			$certificate_level1_d = '';
			$certificate_level1_date = '';
		}
	}
	*/
	if($data_level[$i]['level'] == "1" && $wk_certificate_level1_y == ""){
		if ($data_level[$i]['certificate_date'] != ''){
			$wk_certificate_level1_y = substr($data_level[$i]['certificate_date'],0,4);
			$wk_certificate_level1_m = substr($data_level[$i]['certificate_date'],5,2);
			$wk_certificate_level1_d = substr($data_level[$i]['certificate_date'],8,2);
		} else {
			$wk_certificate_level1_y = '';
			$wk_certificate_level1_m = '';
			$wk_certificate_level1_d = '';
		}
	}
	// 2012/07/13 Yamagawa upd(e)

	if($data_level[$i]['level'] == "2" && $level2_year == ""){
		if ($data_level[$i]['get_level_date'] != ''){
			$level2_year = substr($data_level[$i]['get_level_date'],0,4);
			$level2_month = substr($data_level[$i]['get_level_date'],5,2);
			$level2_day = substr($data_level[$i]['get_level_date'],8,2);
			$level2_date = $level2_year.'/'.$level2_month.'/'.$level2_day;
		} else {
			$level2_year = '';
			$level2_month = '';
			$level2_day = '';
			$level2_date = '';
		}
	}

	// 2012/07/13 Yamagawa upd(s)
	/*
	if($data_level[$i]['level'] == "2" && $certificate_level2_y == ""){
		if ($data_level[$i]['certificate_date'] != ''){
			$certificate_level2_y = substr($data_level[$i]['certificate_date'],0,4);
			$certificate_level2_m = substr($data_level[$i]['certificate_date'],5,2);
			$certificate_level2_d = substr($data_level[$i]['certificate_date'],8,2);
			$certificate_level2_date = $certificate_level2_y.'/'.$certificate_level2_m.'/'.$certificate_level2_d;
		} else {
			$certificate_level2_y = '';
			$certificate_level2_m = '';
			$certificate_level2_d = '';
			$certificate_level2_date = '';
		}
	}
	*/
	if($data_level[$i]['level'] == "2" && $wk_certificate_level2_y == ""){
		if ($data_level[$i]['certificate_date'] != ''){
			$wk_certificate_level2_y = substr($data_level[$i]['certificate_date'],0,4);
			$wk_certificate_level2_m = substr($data_level[$i]['certificate_date'],5,2);
			$wk_certificate_level2_d = substr($data_level[$i]['certificate_date'],8,2);
		} else {
			$wk_certificate_level2_y = '';
			$wk_certificate_level2_m = '';
			$wk_certificate_level2_d = '';
		}
	}
	// 2012/07/13 Yamagawa upd(e)

	if($data_level[$i]['level'] == "3" && $level3_year == ""){
		if ($data_level[$i]['get_level_date'] != ''){
			$level3_year = substr($data_level[$i]['get_level_date'],0,4);
			$level3_month = substr($data_level[$i]['get_level_date'],5,2);
			$level3_day = substr($data_level[$i]['get_level_date'],8,2);
			$level3_date = $level3_year.'/'.$level3_month.'/'.$level3_day;
		} else {
			$level3_year = '';
			$level3_month = '';
			$level3_day = '';
			$level3_date = '';
		}
	}

	// 2012/07/13 Yamagawa upd(s)
	/*
	if($data_level[$i]['level'] == "3" && $certificate_level3_y == ""){
		if ($data_level[$i]['certificate_date'] != ''){
			$certificate_level3_y = substr($data_level[$i]['certificate_date'],0,4);
			$certificate_level3_m = substr($data_level[$i]['certificate_date'],5,2);
			$certificate_level3_d = substr($data_level[$i]['certificate_date'],8,2);
			$certificate_level3_date = $certificate_level3_y.'/'.$certificate_level3_m.'/'.$certificate_level3_d;
		} else {
			$certificate_level3_y = '';
			$certificate_level3_m = '';
			$certificate_level3_d = '';
			$certificate_level3_date = '';
		}
	}
	*/
	if($data_level[$i]['level'] == "3" && $wk_certificate_level3_y == ""){
		if ($data_level[$i]['certificate_date'] != ''){
			$wk_certificate_level3_y = substr($data_level[$i]['certificate_date'],0,4);
			$wk_certificate_level3_m = substr($data_level[$i]['certificate_date'],5,2);
			$wk_certificate_level3_d = substr($data_level[$i]['certificate_date'],8,2);
		} else {
			$wk_certificate_level3_y = '';
			$wk_certificate_level3_m = '';
			$wk_certificate_level3_d = '';
		}
	}
	// 2012/07/13 Yamagawa upd(e)

	if($data_level[$i]['level'] == "4" && $level4_year == ""){
		if ($data_level[$i]['get_level_date'] != ''){
			$level4_year = substr($data_level[$i]['get_level_date'],0,4);
			$level4_month = substr($data_level[$i]['get_level_date'],5,2);
			$level4_day = substr($data_level[$i]['get_level_date'],8,2);
			$level4_date = $level4_year.'/'.$level4_month.'/'.$level4_day;
		} else {
			$level4_year = '';
			$level4_month = '';
			$level4_day = '';
			$level4_date = '';
		}
	}

	// 2012/07/13 Yamagawa upd(s)
	/*
	if($data_level[$i]['level'] == "4" && $certificate_level4_y == ""){
		if ($data_level[$i]['certificate_date'] != ''){
			$certificate_level4_y = substr($data_level[$i]['certificate_date'],0,4);
			$certificate_level4_m = substr($data_level[$i]['certificate_date'],5,2);
			$certificate_level4_d = substr($data_level[$i]['certificate_date'],8,2);
			$certificate_level4_date = $certificate_level4_y.'/'.$certificate_level4_m.'/'.$certificate_level4_d;
		} else {
			$certificate_level4_y = '';
			$certificate_level4_m = '';
			$certificate_level4_d = '';
			$certificate_level4_date = '';
		}
	}
	*/
	if($data_level[$i]['level'] == "4" && $wk_certificate_level4_y == ""){
		if ($data_level[$i]['certificate_date'] != ''){
			$wk_certificate_level4_y = substr($data_level[$i]['certificate_date'],0,4);
			$wk_certificate_level4_m = substr($data_level[$i]['certificate_date'],5,2);
			$wk_certificate_level4_d = substr($data_level[$i]['certificate_date'],8,2);
		} else {
			$wk_certificate_level4_y = '';
			$wk_certificate_level4_m = '';
			$wk_certificate_level4_d = '';
		}
	}
	// 2012/07/13 Yamagawa upd(e)

	if($data_level[$i]['level'] == "5" && $level5_year == ""){
		if ($data_level[$i]['get_level_date'] != ''){
			$level5_year = substr($data_level[$i]['get_level_date'],0,4);
			$level5_month = substr($data_level[$i]['get_level_date'],5,2);
			$level5_day = substr($data_level[$i]['get_level_date'],8,2);
			$level5_date = $level5_year.'/'.$level5_month.'/'.$level5_day;
		} else {
			$level5_year = '';
			$level5_month = '';
			$level5_day = '';
			$level5_date = '';
		}
	}

	// 2012/07/13 Yamagawa upd(s)
	/*
	if($data_level[$i]['level'] == "5" && $certificate_level5_y == ""){
		if ($data_level[$i]['certificate_date'] != ''){
			$certificate_level5_y = substr($data_level[$i]['certificate_date'],0,4);
			$certificate_level5_m = substr($data_level[$i]['certificate_date'],5,2);
			$certificate_level5_d = substr($data_level[$i]['certificate_date'],8,2);
			$certificate_level5_date = $certificate_level5_y.'/'.$certificate_level5_m.'/'.$certificate_level5_d;
		} else {
			$certificate_level5_y = '';
			$certificate_level5_m = '';
			$certificate_level5_d = '';
			$certificate_level5_date = '';
		}
	}
	*/
	if($data_level[$i]['level'] == "5" && $wk_certificate_level5_y == ""){
		if ($data_level[$i]['certificate_date'] != ''){
			$wk_certificate_level5_y = substr($data_level[$i]['certificate_date'],0,4);
			$wk_certificate_level5_m = substr($data_level[$i]['certificate_date'],5,2);
			$wk_certificate_level5_d = substr($data_level[$i]['certificate_date'],8,2);
		} else {
			$wk_certificate_level5_y = '';
			$wk_certificate_level5_m = '';
			$wk_certificate_level5_d = '';
		}
	}
	// 2012/07/13 Yamagawa upd(e)

	if($data_level[$i]['level'] == "1" && $level1_total_value == ""){
		if ($data_level[$i]['total_value'] != ''){
			$level1_total_value = $data_level[$i]['total_value'];
		} else {
			$level1_total_value = '';
		}
	}

	if($data_level[$i]['level'] == "2" && $level2_total_value == ""){
		if ($data_level[$i]['total_value'] != ''){
			$level2_total_value = $data_level[$i]['total_value'];
		} else {
			$level2_total_value = '';
		}
	}

	if($data_level[$i]['level'] == "3" && $level3_total_value == ""){
		if ($data_level[$i]['total_value'] != ''){
			$level3_total_value = $data_level[$i]['total_value'];
		} else {
			$level3_total_value = '';
		}
	}

	if($data_level[$i]['level'] == "4" && $level4_total_value == ""){
		if ($data_level[$i]['total_value'] != ''){
			$level4_total_value = $data_level[$i]['total_value'];
		} else {
			$level4_total_value = '';
		}
	}

	if($data_level[$i]['level'] == "5" && $level5_total_value == ""){
		if ($data_level[$i]['total_value'] != ''){
			$level5_total_value = $data_level[$i]['total_value'];
		} else {
			$level5_total_value = '';
		}
	}

	if($data_level[$i]['level'] == "1"){
		$level1_cat_cnt += 1;
	}else if($data_level[$i]['level'] == "2"){
		$level2_cat_cnt += 1;
	}else if($data_level[$i]['level'] == "3"){
		$level3_cat_cnt += 1;
	}else if($data_level[$i]['level'] == "4"){
		$level4_cat_cnt += 1;
	}else{
		$level5_cat_cnt += 1;
	}
}

if ($mode == 'detail') {
	$option_level1_year  = cl_get_select_years($level1_year);
	$option_level1_month = cl_get_select_months($level1_month);
	$option_level1_day = cl_get_select_days($level1_day);
	$calendar .= write_calendar('level1', 'auth_year_level1', 'auth_month_level1', 'auth_day_level1');
	// 2012/07/13 Yamagawa upd(s)
	/*
	$option_certificate_level1_y = cl_get_select_years($certificate_level1_y);
	$option_certificate_level1_m = cl_get_select_months($certificate_level1_m);
	$option_certificate_level1_d = cl_get_select_days($certificate_level1_d);
	*/
	$option_certificate_level1_y = cl_get_select_years($wk_certificate_level1_y);
	$option_certificate_level1_m = cl_get_select_months($wk_certificate_level1_m);
	$option_certificate_level1_d = cl_get_select_days($wk_certificate_level1_d);
	// 2012/07/13 Yamagawa upd(e)
	$calendar .= write_calendar('certificate_level1', 'certificate_level1_y', 'certificate_level1_m', 'certificate_level1_d');
	$option_level2_year  = cl_get_select_years($level2_year);
	$option_level2_month = cl_get_select_months($level2_month);
	$option_level2_day = cl_get_select_days($level2_day);
	$calendar .= write_calendar('level2', 'auth_year_level2', 'auth_month_level2', 'auth_day_level2');
	// 2012/07/13 Yamagawa upd(s)
	/*
	$option_certificate_level2_y = cl_get_select_years($certificate_level2_y);
	$option_certificate_level2_m = cl_get_select_months($certificate_level2_m);
	$option_certificate_level2_d = cl_get_select_days($certificate_level2_d);
	*/
	$option_certificate_level2_y = cl_get_select_years($wk_certificate_level2_y);
	$option_certificate_level2_m = cl_get_select_months($wk_certificate_level2_m);
	$option_certificate_level2_d = cl_get_select_days($wk_certificate_level2_d);
	// 2012/07/13 Yamagawa upd(e)
	$calendar .= write_calendar('certificate_level2', 'certificate_level2_y', 'certificate_level2_m', 'certificate_level2_d');
	$option_level3_year  = cl_get_select_years($level3_year);
	$option_level3_month = cl_get_select_months($level3_month);
	$option_level3_day = cl_get_select_days($level3_day);
	$calendar .= write_calendar('level3', 'auth_year_level3', 'auth_month_level3', 'auth_day_level3');
	// 2012/07/13 Yamagawa upd(s)
	/*
	$option_certificate_level3_y = cl_get_select_years($certificate_level3_y);
	$option_certificate_level3_m = cl_get_select_months($certificate_level3_m);
	$option_certificate_level3_d = cl_get_select_days($certificate_level3_d);
	*/
	$option_certificate_level3_y = cl_get_select_years($wk_certificate_level3_y);
	$option_certificate_level3_m = cl_get_select_months($wk_certificate_level3_m);
	$option_certificate_level3_d = cl_get_select_days($wk_certificate_level3_d);
	// 2012/07/13 Yamagawa upd(e)
	$calendar .= write_calendar('certificate_level3', 'certificate_level3_y', 'certificate_level3_m', 'certificate_level3_d');
	$option_level4_year  = cl_get_select_years($level4_year);
	$option_level4_month = cl_get_select_months($level4_month);
	$option_level4_day = cl_get_select_days($level4_day);
	$calendar .= write_calendar('level4', 'auth_year_level4', 'auth_month_level4', 'auth_day_level4');
	// 2012/07/13 Yamagawa upd(s)
	/*
	$option_certificate_level4_y = cl_get_select_years($certificate_level4_y);
	$option_certificate_level4_m = cl_get_select_months($certificate_level4_m);
	$option_certificate_level4_d = cl_get_select_days($certificate_level4_d);
	*/
	$option_certificate_level4_y = cl_get_select_years($wk_certificate_level4_y);
	$option_certificate_level4_m = cl_get_select_months($wk_certificate_level4_m);
	$option_certificate_level4_d = cl_get_select_days($wk_certificate_level4_d);
	// 2012/07/13 Yamagawa upd(e)
	$calendar .= write_calendar('certificate_level4', 'certificate_level4_y', 'certificate_level4_m', 'certificate_level4_d');
	$option_level5_year  = cl_get_select_years($level5_year);
	$option_level5_month = cl_get_select_months($level5_month);
	$option_level5_day = cl_get_select_days($level5_day);
	$calendar .= write_calendar('level5', 'auth_year_level5', 'auth_month_level5', 'auth_day_level5');
	// 2012/07/13 Yamagawa upd(s)
	/*
	$option_certificate_level5_y = cl_get_select_years($certificate_level5_y);
	$option_certificate_level5_m = cl_get_select_months($certificate_level5_m);
	$option_certificate_level5_d = cl_get_select_days($certificate_level5_d);
	*/
	$option_certificate_level5_y = cl_get_select_years($wk_certificate_level5_y);
	$option_certificate_level5_m = cl_get_select_months($wk_certificate_level5_m);
	$option_certificate_level5_d = cl_get_select_days($wk_certificate_level5_d);
	// 2012/07/13 Yamagawa upd(e)
	$calendar .= write_calendar('certificate_level5', 'certificate_level5_y', 'certificate_level5_m', 'certificate_level5_d');
}
//2012/05/15 K.Fujii ins(s)

if ($data_level[0]['now_level'] != ''){
	$now_level = $data_level[0]['now_level'];
} else {
	$now_level = '';
}

// DB切断
$mdb2->disconnect();
$log->debug("DB切断",__FILE__,__LINE__);

$log->debug("テンプレートマッピング",__FILE__,__LINE__);

	//##########################################################################
	// 画面表示
	//##########################################################################
	$smarty = new Smarty();
	$smarty->template_dir = dirname(__FILE__) . "/../view";
	$smarty->compile_dir  = dirname(__FILE__) . "/../../templates_c";

	/**
	 * テンプレートマッピング
	 */
	$smarty->assign( 'cl_title'						, $cl_title						);
	$smarty->assign( 'session'						, $session						);
	$smarty->assign( 'target_emp_id'				, $target_emp_id				);
	$smarty->assign( 'mode'							, $mode							);
	$smarty->assign( 'yui_cal_part'					, $yui_cal_part					);
	$smarty->assign( 'calendar'						, $calendar						);
	$smarty->assign( 'workflow_auth'				, $workflow_auth				);
	$smarty->assign( 'aplyctn_mnitm_str'			, $aplyctn_mnitm_str			);
	$smarty->assign( 'js_str'						, $js_str						);

	$smarty->assign( 'emp_full_nm'					, $emp_full_nm					);
	$smarty->assign( 'class_nm'						, $class_nm						);
	$smarty->assign( 'option_level1_year'			, $option_level1_year			);
	$smarty->assign( 'option_level1_month'			, $option_level1_month			);
	$smarty->assign( 'option_level1_day'			, $option_level1_day			);
	$smarty->assign( 'now_level'					, $now_level					);
	$smarty->assign( 'level1_date'					, $level1_date					);
	$smarty->assign( 'option_certificate_level1_y'	, $option_certificate_level1_y	);
	$smarty->assign( 'option_certificate_level1_m'	, $option_certificate_level1_m	);
	$smarty->assign( 'option_certificate_level1_d'	, $option_certificate_level1_d	);
	// 2012/07/13 Yamagawa del(s)
	//$smarty->assign( 'certificate_level1_date'		, $certificate_level1_date		);
	// 2012/07/13 Yamagawa del(e)
	$smarty->assign( 'option_level2_year'			, $option_level2_year			);
	$smarty->assign( 'option_level2_month'			, $option_level2_month			);
	$smarty->assign( 'option_level2_day'			, $option_level2_day			);
	$smarty->assign( 'level2_date'					, $level2_date					);
	$smarty->assign( 'option_certificate_level2_y'	, $option_certificate_level2_y	);
	$smarty->assign( 'option_certificate_level2_m'	, $option_certificate_level2_m	);
	$smarty->assign( 'option_certificate_level2_d'	, $option_certificate_level2_d	);
	// 2012/07/13 Yamagawa del(s)
	//$smarty->assign( 'certificate_level2_date'		, $certificate_level2_date		);
	// 2012/07/13 Yamagawa del(e)
	$smarty->assign( 'option_level3_year'			, $option_level3_year			);
	$smarty->assign( 'option_level3_month'			, $option_level3_month			);
	$smarty->assign( 'option_level3_day'			, $option_level3_day			);
	$smarty->assign( 'level3_date'					, $level3_date					);
	$smarty->assign( 'option_certificate_level3_y'	, $option_certificate_level3_y	);
	$smarty->assign( 'option_certificate_level3_m'	, $option_certificate_level3_m	);
	$smarty->assign( 'option_certificate_level3_d'	, $option_certificate_level3_d	);
	// 2012/07/13 Yamagawa del(s)
	//$smarty->assign( 'certificate_level3_date'		, $certificate_level3_date		);
	// 2012/07/13 Yamagawa del(e)
	$smarty->assign( 'option_level4_year'			, $option_level4_year			);
	$smarty->assign( 'option_level4_month'			, $option_level4_month			);
	$smarty->assign( 'option_level4_day'			, $option_level4_day			);
	$smarty->assign( 'level4_date'					, $level4_date					);
	$smarty->assign( 'option_certificate_level4_y'	, $option_certificate_level4_y	);
	$smarty->assign( 'option_certificate_level4_m'	, $option_certificate_level4_m	);
	$smarty->assign( 'option_certificate_level4_d'	, $option_certificate_level4_d	);
	// 2012/07/13 Yamagawa del(s)
	//$smarty->assign( 'certificate_level4_date'		, $certificate_level4_date		);
	// 2012/07/13 Yamagawa del(e)
	$smarty->assign( 'option_level5_year'			, $option_level5_year			);
	$smarty->assign( 'option_level5_month'			, $option_level5_month			);
	$smarty->assign( 'option_level5_day'			, $option_level5_day			);
	$smarty->assign( 'level5_date'					, $level5_date					);
	$smarty->assign( 'option_certificate_level5_y'	, $option_certificate_level5_y	);
	$smarty->assign( 'option_certificate_level5_m'	, $option_certificate_level5_m	);
	$smarty->assign( 'option_certificate_level5_d'	, $option_certificate_level5_d	);
	// 2012/07/13 Yamagawa del(s)
	//$smarty->assign( 'certificate_level5_date'		, $certificate_level5_date		);
	// 2012/07/13 Yamagawa del(e)

	$smarty->assign( 'level1_total_value'			, $level1_total_value			);
	$smarty->assign( 'level2_total_value'			, $level2_total_value			);
	$smarty->assign( 'level3_total_value'			, $level3_total_value			);
	$smarty->assign( 'level4_total_value'			, $level4_total_value			);
	$smarty->assign( 'level5_total_value'			, $level5_total_value			);

	$smarty->assign( 'title'						, $training_name.'&nbsp;&nbsp;職員ごとのラダー認定状況画面'		);
	$smarty->assign( 'data'							, $data							);
	$smarty->assign( 'login_user'					, $login_user					);

	$smarty->assign( 'common_module'				, $common_module				);

	$smarty->assign( 'ladder_data'					, $_REQUEST['ladder_data']		);

	//2012/05/15 K.Fujii ins(s)
	$smarty->assign( 'data_level'					, $data_level					);
	$smarty->assign( 'level1_class_nm'				, $level1_class_nm				);
	$smarty->assign( 'level2_class_nm'				, $level2_class_nm				);
	$smarty->assign( 'level3_class_nm'				, $level3_class_nm				);
	$smarty->assign( 'level4_class_nm'				, $level4_class_nm				);
	$smarty->assign( 'level5_class_nm'				, $level5_class_nm				);
	$smarty->assign( 'level1_cat_cnt'				, $level1_cat_cnt				);
	$smarty->assign( 'level2_cat_cnt'				, $level2_cat_cnt				);
	$smarty->assign( 'level3_cat_cnt'				, $level3_cat_cnt				);
	$smarty->assign( 'level4_cat_cnt'				, $level4_cat_cnt				);
	$smarty->assign( 'level5_cat_cnt'				, $level5_cat_cnt				);
	//2012/05/15 K.Fujii ins(e)

	// 2012/08/21 Yamagawa upd(s)
	//$smarty->assign( 'level5_edit_flg'				, $level5_edit_flg				);// レベルV認定項目の変更可否(true:変更可能、false:変更不可能)
	$smarty->assign( 'council_flg'				, $council_flg				);// 認定項目の変更可否(true:変更可能、false:変更不可能)
	// 2012/08/21 Yamagawa upd(e)

	$smarty->display(basename(__FILE__,'.php').".tpl");

if ($_REQUEST['print_flg'] == 'update') {
	echo("<script language=\"javascript\">output_to_print();</script>");
	//echo("<script type=\"text/javascript\">window.opener.location.reload();</script>");
	echo("<script type=\"text/javascript\">window.opener.research();</script>");
}
if ($_REQUEST['get_level_date_regist'] == 'true') {
	//echo("<script type=\"text/javascript\">window.opener.location.reload();</script>");
	echo("<script type=\"text/javascript\">window.opener.research();</script>");
}

/**
 * Javascript文字列取得
 */
function get_js_str($arr_wkfwcatemst,$workflow)
{
	$str_buff[]  = "";
	foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) {
		$str_buff[] .= "\n\t\tif (cate_id == '".$tmp_cate_id."') {\n";


		 foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm){
			$tmp_wkfw_nm = htmlspecialchars($tmp_wkfw_nm, ENT_QUOTES);
			$tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
			$tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);

			$str_buff[] .= "\t\t\taddOption(obj_wkfw, '".$tmp_wkfw_id."', '".$tmp_wkfw_nm."',  '".$workflow."');\n";

		}

		$str_buff[] .= "\t\t}\n";
	}

	return implode('', $str_buff);
}

/**
 * 年オプションHTML取得
 */
function get_select_around_years($date)
{
	// 表示開始年は1990年なので、差分の90年分を引く
	if (date(n) <= 3){
		$year  = date(Y) - 1 - 1990;
	} else {
		$year  = date(Y) - 1990;
	}

	ob_start();
	show_years_around($year, 1, $date);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 年オプションHTML取得(報告日用)
 */
function cl_get_select_years($date)
{
	ob_start();
	show_select_years(10, $date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 月オプションHTML取得
 */
function cl_get_select_months($date)
{

	ob_start();
	show_select_months($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 日オプションHTML取得
 */
function cl_get_select_days($date)
{

	ob_start();
	show_select_days($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * アプリケーションメニューHTML取得
 */
function get_applycation_menuitem($session,$fname)
{
	ob_start();
	show_applycation_menuitem($session,$fname,"");
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * YUIカレンダー用スクリプトHTML取得
 */
function get_yui_cal_part()
{

	ob_start();
	/**
	 * YUIカレンダーに必要な外部ファイルの読み込みを行います。
	 */
	write_yui_calendar_use_file_read_0_12_2();
	$yui_cal_part=ob_get_contents();
	ob_end_clean();
	return $yui_cal_part;
}

function write_calendar($suffix, $year_control_id, $month_control_id, $date_control_id){

	ob_start();
	write_cl_calendar($suffix, $year_control_id, $month_control_id, $date_control_id);
	$str_buff = ob_get_contents();
	ob_end_clean();
	return $str_buff;

}

function show_years_around($before_count, $after_count, $num){

	if (date(n) <= 3){
		$now  = date(Y) - 1;
	} else {
		$now  = date(Y);
	}
	if ($num == ''){
		$num = $now;
	}

	// 当年まで
	for($i=0; $i<=$before_count;$i++){
		$yr = $now - $before_count + $i;
		echo("<option value=\"".$yr."\"");
		if($num == $yr){
			echo(" selected");
		}
		echo(">".$yr."</option>\n");
	}

	// 翌年
	for($i=1; $i<=$after_count;$i++){
		$yr = $now + $i;
		echo("<option value=\"".$yr."\"");
		if($num == $yr){
			echo(" selected");
		}
		echo(">".$yr."</option>\n");
	}

}

$log->info(basename(__FILE__)." END");
$log->shutdown();
