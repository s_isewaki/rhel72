<?php
require_once(dirname(__FILE__) . "/../../Cmx.php");

/**
 * log4phpライブラリパス定義
 */
define('LOG4PHP_DIR', dirname(__FILE__).'/../../libs/log4php');

/**
 * log4phpプロパティファイル定義
 */
define('LOG4PHP_CONFIGURATION', dirname(__FILE__).'/../conf/cl_log.properties');

/**
 * log4phpロガーマネージャロード
 */
require_once( LOG4PHP_DIR . '/LoggerManager.php' );

class cl_log_class
{
	/** ロガーオブジェクト */
	var $log;
	/** ディレクトリパス出力抑制フラグ */
	var $without_dir;

	var $pid;

	/**
	 * コンストラクタ
	 * @param string $fname 画面名
	 * @param boolean $without_dir 画面名ディレクトリパス除去
	 */
	function cl_log_class($fname,$without_dir=true)
	{
		$this->pid = getmypid();
		$this->without_dir = $without_dir;
		if($this->without_dir){
			$fname=basename($fname);
		}
		$this->log = & Logger::getLogger($this->pid);

	}

	/**
	 * ロガーシャットダウン
	 */
	function shutdown()
	{
                /**
                * シャットダウン
                */
                LoggerManager::shutdown();
	}

	/**
	 * infoメッセージ出力
	 * @param string $msg メッセージ
	 * @param string $file ファイル名
	 * @param boolean $line 行番号
	 * @param boolean $function 関数名
	 */
	function info($msg,$file=null,$line=null,$function=null)
	{
		$msg=$this->editmsg($msg,$file,$line);
		$this->log->info($msg);
	}
	/**
	 * debugメッセージ出力
	 * @param string $msg メッセージ
	 * @param string $file ファイル名
	 * @param boolean $line 行番号
	 * @param boolean $function 関数名
	 */
	function debug($msg,$file=null,$line=null,$function=null)
	{
		$msg=$this->editmsg($msg,$file,$line);
		$this->log->debug($msg);
	}
	/**
	 * errorメッセージ出力
	 * @param string $msg メッセージ
	 * @param string $file ファイル名
	 * @param boolean $line 行番号
	 * @param boolean $function 関数名
	 */
	function error($msg,$file=null,$line=null,$function=null)
	{
		$msg=$this->editmsg($msg,$file,$line);
		$this->log->error($msg);
	}
	/**
	 * warnメッセージ出力
	 * @param string $msg メッセージ
	 * @param string $file ファイル名
	 * @param boolean $line 行番号
	 * @param boolean $function 関数名
	 */
	function warn($msg,$file=null,$line=null,$function=null)
	{
		$msg=$this->editmsg($msg,$file,$line);
		$this->log->warn($msg);
	}
	/**
	 * fatalメッセージ出力
	 * @param string $msg メッセージ
	 * @param string $file ファイル名
	 * @param boolean $line 行番号
	 * @param boolean $function 関数名
	 */
	function fatal($msg,$file=null,$line=null,$function=null)
	{
		$msg=$this->editmsg($msg,$file,$line);
		$this->log->fatal($msg);
	}

	/**
	 * メッセージ編集
	 * @param string $msg メッセージ
	 * @param string $file ファイル名
	 * @param boolean $line 行番号
	 * @param boolean $function 関数名
	 */
	function editmsg($msg,$file=null,$line=null,$function=null)
	{
		if($this->without_dir){
			$file=basename($file);
		}
		$prestr="";
		if($file!=null){
			$prestr="[".$file."]";
		}
		if($line!=null){
			$prestr=$prestr."[".$line."]";
		}
		if($function!=null){
			$prestr=$prestr."[".$function."]";
		}

		return $prestr.$msg;
	}
}