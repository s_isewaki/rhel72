<?php

	//##########################################################################
	// 各種モジュールの読み込み
	//##########################################################################
	//require_once(dirname(__FILE__) . "/../../about_session.php");
	//require_once(dirname(__FILE__) . "/../../about_authority.php");
	//require_once(dirname(__FILE__) . "/../../about_postgres.php");
	//require_once(dirname(__FILE__) . "/../../show_select_values.ini");
	//require_once(dirname(__FILE__) . "/../../cl_application_list.inc");
	//require_once(dirname(__FILE__) . "/../../cl_application_common.ini");
	//require_once(dirname(__FILE__) . "/../../cl_common.ini");
	//require_once(dirname(__FILE__) . "/../../cl_title_name.ini");
	require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
	require_once(dirname(__FILE__) . "/../../Cmx.php");
	//require_once("Smarty/Smarty.class.php");
	require_once("MDB2.php");

	//##########################################################################
	// 初期処理
	//##########################################################################

	//ロガーをロード
	$log = new cl_common_log_class(basename(__FILE__));
	$log->info(basename(__FILE__)." START");

	//画面名
	$fname = $PHP_SELF;


	//##########################################################################
	// パラメータ取得
	//##########################################################################
foreach( $_REQUEST as $key => $value ) {
	$log->debug('$_REQUEST - ['.$key.']:'.$_REQUEST[$key],__FILE__,__LINE__);
}
	$session		= $_POST["session"];
	$login_user		= $_POST["login_user"];
	$plan_id		= $_POST["plan_id"];
	$p_plan_id		= $_POST["plan_id"];
	$training_id	= $_POST["training_id"];


	$log->debug('■plan_id   :'.$plan_id	,__FILE__,__LINE__);
	$log->debug('■session   :'.$session	,__FILE__,__LINE__);
	$log->debug('■training_id   :'.$training_id	,__FILE__,__LINE__);

	// ファイル名の先頭に研修日程ID(plan_id)を追加
	$file_name = $plan_id."_reception_list.csv";
	$log->debug('■file_name   :'.$file_name	,__FILE__,__LINE__);

//====================================
//セッションのチェック
//====================================
//$session = qualify_session($session,$fname);
//if($session == "0"){
//	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
//	echo("<script language='javascript'>showLoginPage(window);</script>");
//	exit;
//}
//	$log->debug('■session2   :'.$session	,__FILE__,__LINE__);
//====================================
//権限チェック
//====================================
//$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
//if($wkfw=="0"){
//	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
//	echo("<script language='javascript'>showLoginPage(window);</script>");
//	exit;
//}


//	// ワークフロー権限の取得
//	$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

//	$log->debug('■$workflow_auth   :'.$workflow_auth	,__FILE__,__LINE__);

	//##########################################################################
	// データベース接続
	//##########################################################################
	$mdb2 = MDB2::connect(CMX_DB_DSN);

	// セッションよりログインユーザー取得
	require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php");
	$empmst_model = new empmst_model($mdb2,$emp_id);
	$arr_empmst = $empmst_model->select_login_emp($session);

	$emp_id = $arr_empmst["emp_id"];

	$log->debug('■$emp_id2   :'.$emp_id	,__FILE__,__LINE__);

	// ヘッダーデータ取得
	require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_model.php");
	$cl_mst_inside_training_model = new cl_mst_inside_training_model($mdb2,$emp_id);
	$arr_training = $cl_mst_inside_training_model->select($training_id);

foreach($arr_training as $tmp){
//	foreach($tmp as $tmpp){
	$log->debug('■$arr_training2   :'.$tmp	,__FILE__,__LINE__);
//	}
}

	require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_schedule_model.php");
	$cl_mst_inside_training_schedule_model = new cl_mst_inside_training_schedule_model($mdb2,$emp_id);
	$arr_schedule = $cl_mst_inside_training_schedule_model->select($p_plan_id);
	$arr_date = explode("-",$arr_schedule['plan_date']);
	$plan_date_year = $arr_date[0];
	$plan_date_month = $arr_date[1];
	$plan_date_day = $arr_date[2];

	$log->debug('■$plan_date2   :'.$arr_schedule['plan_date']	,__FILE__,__LINE__);

	// 一覧データ取得
//	require_once(dirname(__FILE__) . "/../model/workflow/cl_apl_inside_training_apply_model.php");
//	$cl_apl_inside_training_apply_model = new cl_apl_inside_training_apply_model($mdb2, $emp_id);
	require_once(dirname(__FILE__) . "/../model/workflow/cl_inside_training_roll_model.php");
	$cl_inside_training_roll_model = new cl_inside_training_roll_model($mdb2, $emp_id);


//	$param = array(
//		"inside_training_id"	=> $training_id,
//		"plan_id"				=> $plan_id
//	);

	$param = array(
		"training_id"	=> $training_id,
		"plan_id"		=> $p_plan_id
	);

//	$data = $cl_apl_inside_training_apply_model->getReceptionList($param);
	$data = $cl_inside_training_roll_model->getReceptionList($param);

	// 行番号を追加
	$rowcnt = count($data);
	for ($index = 0; $index < $rowcnt; $index++){
		$data[$index]['no'] = $index + 1;
	}

	//##########################################################################
	// データベース接続を閉じる
	//##########################################################################
	$log->debug("データベース切断 START",__FILE__,__LINE__);
	$mdb2->disconnect();
	$log->debug("データベース切断 END",__FILE__,__LINE__);


	//##########################################################################
	// CSV出力
	//##########################################################################
	if ($_POST['csv_flg'] == 'true'){

		// ヘッダー行
		// 項目配列作成
		$array_csv_header = array();
		$array_csv_header[] = "No";
		$array_csv_header[] = "職員ID";
		$array_csv_header[] = "職員氏名";
		$array_csv_header[] = "所属";
		$array_csv_header[] = "申込日時";


		// 文字コード変換
		$csv_header = array();
		foreach($array_csv_header as $tmp_csv_header){
			$csv_header[] = format_column_for_csv($tmp_csv_header);
		}

		// 配列から文字列化
		$csv_header = implode(",", $csv_header);

		$log->debug('■csv_header   :'.$csv_header	,__FILE__,__LINE__);

		// 明細行
		foreach ($data as $row){
			$csv_body .= format_column_for_csv($row['no']);
			$csv_body .= ",". format_column_for_csv($row['emp_id']);
			$csv_body .= ",". format_column_for_csv($row['emp_name']);
			$csv_body .= ",". format_column_for_csv($row['affiliation']);
			$csv_body .= ",". format_column_for_csv($row['inside_training_date'] );
			$csv_body .= "\r\n";
		}

		$log->debug('■csv_body   :'.$csv_body	,__FILE__,__LINE__);

		$csv = $csv_header ."\r\n". $csv_body;

		// CSVを出力
//		$file_name = $plan_id."_reception_list.csv";
		header("Content-Type: application/octet-stream");
		header("Content-Disposition: attachment; filename=$file_name");
		header("Content-Length: " . strlen($csv) );
		echo($csv);

	}


	//====================================
	// 文字コード変換
	//====================================
	function format_column_for_csv($value, $from_encoding = "EUC-JP") {
		$buf = str_replace("\r", "", $value);
		$buf = str_replace("\n", "", $buf);
		if (strpos($buf, ",") !== false)  {
			$buf = '"' . $buf . '"';
		}
		return mb_convert_encoding($buf, "Shift_JIS", $from_encoding);
	}


	$log->info(basename(__FILE__)." END");
	$log->shutdown();

?>