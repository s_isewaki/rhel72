<?php
//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");
require_once('MDB2.php');

require_once(dirname(__FILE__) . "/../../cl_common.ini");

//define("SQL_SUCCESS",0);
//define("SQL_ERROR",9);

require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_schedule_model_sub.php");
require_once(dirname(__FILE__) . "/../model/search/cl_mst_inside_training_schedule_util.php");

require_once(dirname(__FILE__) . "/../model/sequence/cl_inside_training_schedule_id_seq_model.php");

$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

//------------------------------------------------------------------------------
// パラメータ取得
//------------------------------------------------------------------------------
//					"login_user"		: document.form.login_user.value					,	// ログインユーザID
//					"training_id"		: document.form.training_id.value					,	// 院内研修ID
//					"plan_id_str"		: plan_id_str											// 開催日ID文字列
$login_user				=$_POST["login_user"		];	// ログインユーザID
$training_id			=$_POST["training_id"		];	// 院内研修ID
$plan_id_str			=$_POST["plan_id_str"		];	// 開催日ID文字列

$log->debug('$login_user  ：'.$login_user,__FILE__,__LINE__);
$log->debug('$training_id ：'.$training_id,__FILE__,__LINE__);
$log->debug('$plan_id_str ：'.$plan_id_str,__FILE__,__LINE__);

// 開催日ID文字列再作成
//$plan_id_array			=split(',',$plan_id_str);
//$plan_id_str=implode( ',', $plan_id_array );
$plan_id_str=rtrim($plan_id_str,',');

$log->debug('$plan_id_str ：'.$plan_id_str,__FILE__,__LINE__);

// URLデコード
//$place					=urldecode($place);
//$remarks				=urldecode($remarks);

// 文字コード変換
//$place					=mb_convert_encoding($place, "EUC-JP", "UTF-8");
//$remarks				=mb_convert_encoding($remarks, "EUC-JP", "UTF-8");

//$log->debug('$place  ：'.$place,__FILE__,__LINE__);
//$log->debug('$remarks：'.$remarks,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 院内研修日程ID取得
//------------------------------------------------------------------------------
//$log->debug("院内研修日程IDSEQモデルインスタンス作成開始",__FILE__,__LINE__);
//$cl_inside_training_schedule_id_seq_model = new cl_inside_training_schedule_id_seq_model($mdb2, $login_user);
//$log->debug("院内研修日程IDSEQモデルインスタンス作成終了",__FILE__,__LINE__);

//$plan_id=$cl_inside_training_schedule_id_seq_model->getId();

//$log->debug('院内研修日程ID:'.$plan_id,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 日程番号の最大値を取得
//------------------------------------------------------------------------------
//$cl_mst_inside_training_schedule_model = new cl_mst_inside_training_schedule_model($mdb2, $login_user);
//$cl_mst_inside_training_schedule_util = new cl_mst_inside_training_schedule_util($mdb2, $login_user);
//$plan_no = $cl_mst_inside_training_schedule_util->numberingPlanNo($training_id);

//$log->debug('training_id	:'.$training_id		,__FILE__,__LINE__);
//$log->debug('plan_no		:'.$plan_no			,__FILE__,__LINE__);
//$log->debug('plan_id_str	:'.$plan_id_str		,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// パラメータ作成
//------------------------------------------------------------------------------
//$plan_date	= $plan_date_yyyy."/".$plan_date_mm."/".$plan_date_dd;

$param["update_user"]=$login_user;
$param=array(
	"update_user"			=> $login_user
);

$plan_id_array			=split(',',$plan_id_str);

$ph_str="";
$type=array();
for($i=0;$i<count($plan_id_array);$i++){
	$param["plan_id_".$i]=$plan_id_array[$i];
	$ph_str = $ph_str.":plan_id_".$i.",";
	$type[]="text";
}
$ph_str=rtrim($ph_str,',');

//------------------------------------------------------------------------------
// 論理削除
//------------------------------------------------------------------------------
//$rtn=$cl_mst_inside_training_schedule_model->insert($param);
        /**
         * Parameter
         */
        //$param=array();
        //$param["plan_id"]=$plan_id;

        /* SQL */
        $sql = "UPDATE cl_mst_inside_training_schedule SET delete_flg = 1 , update_date = current_timestamp , update_user = :update_user WHERE plan_id IN ( $ph_str )";
        $log->debug('$sql:'.$sql,__FILE__,__LINE__);

        /* カラムタイプ */
        //$type = array('text','text');

        /**
         * 実行
         */
//        $rtn=parent::execute($sql,$type,$param);
        //$this->log->info("Statement START");
        $stmt = $mdb2->prepare($sql, $type, MDB2_PREPARE_MANIP);
        //$stmt = $this->con->prepare($sql, null, MDB2_PREPARE_MANIP);
        //$this->log->info("Statement END");

        //$this->log->debug("execute start");
        $res = $stmt->execute($param);
        if(PEAR::isError($res)){
            $log->error("エラー詳細：".$res->getDebugInfo(),__FILE__,__LINE__);
            //$log->error("execute error");
            return null;
       	}
        //$this->log->debug("execute end ".$res,__FILE__,__LINE__);
        $stmt->free();

        //$this->log->info(__FUNCTION__." END");

$log->debug("開催日削除処理：".$rtn,__FILE__,__LINE__);
if($rtn==SQL_ERROR){
	$log->debug("SQL_ERROR",__FILE__,__LINE__);
	$json="{success:\"false\"}";
}
else{
	$log->debug("SQL_SUCCSESS",__FILE__,__LINE__);
	$json='{"success":"true"}';
}


$log->debug('$json:'.$json,__FILE__,__LINE__);


//------------------------------------------------------------------------------
// データベース接続を閉じる
//------------------------------------------------------------------------------
$log->debug("データベース切断 START",__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);

header("Content-Type: application/json; charset=EUC-JP");
echo $json;

