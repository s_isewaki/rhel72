<?php
//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");
require_once('MDB2.php');
require_once(dirname(__FILE__) . "/../model/template/cl_apl_inside_training_apply_model.php");

$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

$apply_id=$_GET["apply_id"];

$log->debug('apply_id:'.$apply_id,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 申請書データ取得処理を呼び出す
//------------------------------------------------------------------------------
$json=getJsonApplyData($mdb2, $apply_id);


$log->debug('$json:'.$json,__FILE__,__LINE__);


//------------------------------------------------------------------------------
// データベース接続を閉じる
//------------------------------------------------------------------------------
$log->debug("データベース切断 START",__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);

header("Content-Type: application/json; charset=EUC-JP");
echo $json;

/**
 * 変更申請書データ取得
 */
function getJsonApplyData($mdb2, $apply_id){
	global $log;
	$log->debug(__FUNCTION__." START");

	//--------------------------------------------------------------------------
	// 院内研修申請テーブルモデルインスタンス生成
	//--------------------------------------------------------------------------
	$log->debug("apply modelインスタンス作成開始",__FILE__,__LINE__);
	$model = new cl_apl_inside_training_apply_model($mdb2, $user);
	$log->debug("apply modelインスタンス作成終了",__FILE__,__LINE__);

	//--------------------------------------------------------------------------
	// 院内研修申請テーブルデータ取得
	//--------------------------------------------------------------------------
	$log->debug("getRecJson開始",__FILE__,__LINE__);
	$data=$model->select_by_apply_id($apply_id);
	$data["status_division"] = $data["update_division"];
	$log->debug("getRecJson終了",__FILE__,__LINE__);


	$data["change_reason"]     = str_replace("\r\n",   "\\n",$data["change_reason"]);			// 改行
	$data["learn_memo"]        = str_replace("\r\n",   "\\n",$data["learn_memo"]);			// 改行
	$data["training_slogan"]   = str_replace("\r\n",   "\\n",$data["training_slogan"]);		// 改行
	$data["training_purpose"]  = str_replace("\r\n",   "\\n",$data["training_purpose"]);
	$data["training_contents"] = str_replace("\r\n",   "\\n",$data["training_contents"]);

	//2012/05/11 K.Fujii ins(s)
	$data["training_require"] = str_replace("\r\n",   "\\n",$data["training_require"]);
	//2012/05/11 K.Fujii ins(e)

	// 変更前のデータを取得
	$log->debug("getBeforeData 開始　before_rec_id = ". $data['before_apply_id'],__FILE__,__LINE__);
	$before_data = $model->select($data['before_apply_id']);
	$before_data["status_division"] = $before_data["update_division"];
	$log->debug("getBeforeData終了",__FILE__,__LINE__);

	$before_data["learn_memo"]        = str_replace("\r\n",   "\\n",$before_data["learn_memo"]);			// 改行
	$before_data["training_slogan"]   = str_replace("\r\n",   "\\n",$before_data["training_slogan"]);		// 改行
	$before_data["training_purpose"]  = str_replace("\r\n",   "\\n",$before_data["training_purpose"]);
	$before_data["training_contents"] = str_replace("\r\n",   "\\n",$before_data["training_contents"]);
	$before_data["training_action"]   = str_replace("\r\n",   "\\n",$before_data["training_action"]);
	$before_data["training_support"]  = str_replace("\r\n",   "\\n",$before_data["training_support"]);

	//2012/05/11 K.Fujii ins(s)
	$before_data["training_require"] = str_replace("\r\n",   "\\n",$before_data["training_require"]);
	$before_data["change_reason"]     = str_replace("\r\n",   "\\n",$before_data["change_reason"]);			// 改行
	//2012/05/11 K.Fujii ins(e)

	//--------------------------------------------------------------------------
	// 日程情報データ取得
	//--------------------------------------------------------------------------
	$log->debug("schedule modelインスタンス作成開始",__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_schedule_model.php");
	$cl_mst_inside_training_schedule_model = new cl_mst_inside_training_schedule_model($mdb2, $user);
	$log->debug("schedule modelインスタンス作成終了",__FILE__,__LINE__);

	// 変更前のスケジュールデータを取得
	$log->debug("before_plan_id = ".$before_data["plan_id"],__FILE__,__LINE__);
	$arr_before_plan = $cl_mst_inside_training_schedule_model->getList($before_data["plan_id"]);
	$log->debug("before plan count = ". count($arr_before_plan),__FILE__,__LINE__);
	$log->debug("$arr_before_plan = ". $arr__before_plan,__FILE__,__LINE__);
	// 2012/04/12 Yamagawa add(s)
	// 「23:59」は「24:00」に変換（暫定対応）
	for($i = 0; $i < count($arr_before_plan); $i++) {
		if (substr($arr_before_plan[$i]['from_time'],0,5) == "23:59") {
			$arr_before_plan[$i]['from_time'] = "24:00";
		}
		if (substr($arr_before_plan[$i]['to_time'],0,5) == "23:59") {
			$arr_before_plan[$i]['to_time'] = "24:00";
		}
	}
	// 2012/04/12 Yamagawa add(e)

	// 変更後のスケジュールデータを取得
	$log->debug("plan_id = ".$data["plan_id"],__FILE__,__LINE__);
	$arr_plan = $cl_mst_inside_training_schedule_model->getList($data["plan_id"]);
	$log->debug("plan count = ". count($arr_plan),__FILE__,__LINE__);
	$log->debug("$arr_plan = ". $arr_plan,__FILE__,__LINE__);
	// 2012/04/12 Yamagawa add(s)
	// 「23:59」は「24:00」に変換（暫定対応）
	for($i = 0; $i < count($arr_plan); $i++) {
		if (substr($arr_plan[$i]['from_time'],0,5) == "23:59") {
			$arr_plan[$i]['from_time'] = "24:00";
		}
		if (substr($arr_plan[$i]['to_time'],0,5) == "23:59") {
			$arr_plan[$i]['to_time'] = "24:00";
		}
	}
	// 2012/04/12 Yamagawa add(e)

	// 2012/11/05 Yamagawa add(s)
	require_once(dirname(__FILE__) . "/../model/template/cl_apl_inside_training_report_model.php");
	$cl_apl_inside_training_report_model = new cl_apl_inside_training_report_model($mdb2, $user);

	$arr_reported_id = $cl_apl_inside_training_report_model->reported_apl_inside_training_rec($before_data["first_apply_id"]);

	$reported_id = "";
	for($i = 0; $i < count($arr_reported_id); $i++) {
		if ($reported_id != "") {
			$reported_id .= ",";
		}
		$reported_id .= $arr_reported_id[$i]["plan_id"];
	}
	$data["reported_id"] = $reported_id;
	// 2012/11/05 Yamagawa add(e)

//	$before_data["schedule_list"] = $arr_before_plan;
//	$data["schedule_list"] = $arr_plan;

//	$all_data['before_data'] = $before_data;
//	$all_data['now_data'] = $data;

//	$log->debug("all_data = ". print_r($arr_plan),__FILE__,__LINE__);

	$data["schedule_list"] = $arr_plan;
	$data["before_schedule_list"] = $arr_before_plan;
	$data["before_data"] = $before_data;
	//$all_data['now_data'] = $data;

//	$json=create_json($all_data);
	$json=create_json($data);

	$log->debug(__FUNCTION__." END");

	return $json;
}
