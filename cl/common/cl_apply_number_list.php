<?
/*
研修講座一覧
・院内研修申込を行う際に、申し込む研修及びその日程を選択する画面


【呼び出しパラメータ】
session　　　　　　　　　　セッションID。
gamen_mode 　　　　　　　　画面モード。"training_select":講座選択、"plan_select":日程選択(研修IDの指定が必須)
head_nurse_recommend_flg 　師長推薦フラグ。1:師長推薦、0:通常。(省略時は1)
emp_id 　　　　　　　　　　研修受講者の職員ID。
training_id　　　　　　　　初期選択させる研修ID。(省略可)
plan_id_csv　　　　　　　　初期選択させる日程IDをカンマ区切りで指定。例)"CLTSC00000001,CLTSC00000002,CLTSC00000003" (省略可)

【戻り値】
training_id　　　　　　　　研修ID
training_name　　　　　　　研修名
training_purpose 　　　　　研修目的
schedule_list　　　　　　　研修日程情報の配列(key=選択順,value=日程情報オブジェクト)

※日程情報オブジェクト
plan_id　　　　　　　　　　日程ID
training_time　　　　　　　研修回数
final_flg　　　　　　　　　最終フラグ
plan_date　　　　　　　　　開催日付(書式:"YYYY年MM月dd日(曜日)")
from_time　　　　　　　　　開催時刻From(書式:"HH:mm")
to_time　　　　　　　　　　開催時刻To(書式:"HH:mm")
apply_status 　　　　　　　申込受付

*/

//TODO 主催者などの表示で役職？所属？も表示させる必要があるかもしれない。
//TODO 一覧表をDIVでスクロールさせているが、ヘッダー固定のスクロールかページングにしたい。
//TODO 一覧表をの内容を制限する必要があるのでは？例えば過去の年度の日程は表示しないなど。

require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cl_application_list.inc");
require_once("cl_application_common.ini");
require_once("cl_common.ini");
require_once("cl_title_name.ini");
require_once("cl_smarty_setting.ini");
require_once("cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once('MDB2.php');



//====================================
//リクエストパラメータ取得
//====================================
$common_module = $_POST["common_module"];
$session = $_POST["session"];
$gamen_mode = $_POST["gamen_mode"];
$head_nurse_recommend_flg = $_POST["head_nurse_recommend_flg"];
$emp_id = $_POST["emp_id"];
$training_id = $_POST["training_id"];
$plan_id_csv = $_POST["plan_id_csv"];

$plan_id_arr = array();
if($plan_id_csv != "")
{
	$plan_id_arr = explode(",",$plan_id_csv);
}

if($head_nurse_recommend_flg != 1)
{
	$head_nurse_recommend_flg = 0;
}

//====================================
//初期処理
//====================================

//ロガーをロード
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");


//画面名
$fname = $PHP_SELF;


//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//権限チェック
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}



//====================================
// データベース接続
//====================================
$mdb2 = MDB2::connect(CMX_DB_DSN);



//====================================
// モデルをロード
//====================================

require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php");
$empmst_model = new empmst_model($mdb2);

// セッションよりログインユーザーIDを取得
$arr_empmst = $empmst_model->select_login_emp($session);
$login_emp_id = $arr_empmst[0]["emp_id"];

require_once(dirname(__FILE__) . "/../model/search/cl_training_select_list_model.php");
$cl_training_select_list_model = new cl_training_select_list_model($mdb2,$login_emp_id);

require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_model.php");
$cl_mst_inside_training_model = new cl_mst_inside_training_model($mdb2,$login_emp_id);

require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_theme_model.php");
$cl_mst_inside_training_theme_model = new cl_mst_inside_training_theme_model($mdb2,$login_emp_id);

require_once(dirname(__FILE__) . "/../model/ladder/cl_personal_profile_model.php");
$cl_personal_profile_model = new cl_personal_profile_model($mdb2,$login_emp_id);



//====================================
// データ取得
//====================================

//研修受講者の現在のレベルを取得
$arr_personal_profile_info = $cl_personal_profile_model->select($emp_id);
$now_level = $arr_personal_profile_info['now_level'];
//プロフィールが登録されていない場合はレベル０として扱う。
if($now_level == "")
{
	$now_level = 0;
}

//現在のレベル＋１を次目標とするラダーレベルとする。
$target_level = $now_level + 1;


//対象レベル　表示用
$target_level_disp = "";
switch ($target_level)
{
	case 1:
		$target_level_disp = "I";
		break;
	case 2:
		$target_level_disp = "II";
		break;
	case 3:
		$target_level_disp = "III";
		break;
	case 4:
		$target_level_disp = "IV";
		break;
	case 5:
		$target_level_disp = "V";
		break;
	case 6:
		$target_level_disp = "";
		break;
}


//研修講座一覧を取得
$arr_training_list = $cl_training_select_list_model->get_training_list($target_level,$head_nurse_recommend_flg);


//研修講座が選択済みの場合
if($training_id != "")
{
	//日程一覧を取得
	if($gamen_mode == "plan_select")
	{
		$arr_plan_list = $cl_training_select_list_model->get_plan_list_for_modify($training_id,$plan_id_csv);
	}
	else
	{
		$arr_plan_list = $cl_training_select_list_model->get_plan_list($training_id);
	}

	//研修の詳細情報を取得
	$arr_training_info = $cl_mst_inside_training_model->select($training_id);

	//研修に対する研修課題を取得(研修前課題、研修中課題、研修後課題の最大3レコード)
	$arr_theme_list = $cl_mst_inside_training_theme_model->select_from_training_id($training_id);
	for ($i_theme_division = 0; $i_theme_division < 3; $i_theme_division++)
	{
		$l_theme_theme_present = $arr_theme_list[$i_theme_division]['theme_present'];
		 $arr_theme_list[$i_theme_division]['theme_present_disp'] = get_emp_disp_string($l_theme_theme_present, $empmst_model);
	}

	//主催者を取得
	$training_opener = $arr_training_info['training_opener'];
	$training_opener_disp = get_emp_disp_string($training_opener, $empmst_model);
	//講師１を取得
	$training_teacher1 = $arr_training_info['training_teacher1'];
	$training_teacher1_disp = get_emp_disp_string($training_teacher1, $empmst_model);
	//講師２を取得
	$training_teacher2 = $arr_training_info['training_teacher2'];
	$training_teacher2_disp = get_emp_disp_string($training_teacher2, $empmst_model);
	//講師３を取得
	$training_teacher3 = $arr_training_info['training_teacher3'];
	$training_teacher3_disp = get_emp_disp_string($training_teacher3, $empmst_model);
	//講師４を取得
	$training_teacher4 = $arr_training_info['training_teacher4'];
	$training_teacher4_disp = get_emp_disp_string($training_teacher4, $empmst_model);
	//講師５を取得
	$training_teacher5 = $arr_training_info['training_teacher5'];
	$training_teacher5_disp = get_emp_disp_string($training_teacher5, $empmst_model);

	//研修の情報を展開
	$training_name = $arr_training_info['training_name'];
	$max_training_time = $arr_training_info['max_training_time'];
	$training_purpose = $arr_training_info['training_purpose'];
	$training_slogan = $arr_training_info['training_slogan'];
	$training_contents = $arr_training_info['training_contents'];
	$training_action = $arr_training_info['training_action'];
	$training_support = $arr_training_info['training_support'];

	//研修回数が全て登録されているかをチェック(登録されていないリスト作成)
	$no_time_list = "";
	for($i_time = 1; $i_time <= $max_training_time; $i_time++)
	{
		$time_have_flg = false;
		for($i_plan = 0; $i_plan < count($arr_plan_list); $i_plan++)
		{
			$l_training_time = $arr_plan_list[$i_plan]['training_time'];
			if($i_time == $l_training_time)
			{
				$time_have_flg = true;
			}
		}
		if(!$time_have_flg)
		{
			if($no_time_list != "")
			{
				$no_time_list = $no_time_list . "、";
			}
			$no_time_list = $no_time_list . get_training_time_disp_string($i_time, $i_time == $max_training_time);
		}
	}
}



//====================================
// データベース切断
//====================================
$mdb2->disconnect();



//====================================================================================================
// HTML出力
//====================================================================================================

$page_title = "研修講座一覧";
if($head_nurse_recommend_flg == 1)
{
	$page_title = "研修講座一覧(師長推薦)";
}


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cl_title?>｜<?=$page_title?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<script type="text/javascript">

//onload()時の処理
function onload_action()
{
	<?
	if($no_time_list != "")
	{
	?>
	alert("<?=$no_time_list?>の日程が登録されていません。");
	<?
	}
	?>
}

//====================================================================================================
//■イベント処理

//研修講座一覧から研修講座を選択した時の処理
function training_selected(training_id)
{
<?
//「日程選択モード」の場合は研修講座を変更不可能
if($gamen_mode == "plan_select")
{
	//日程変更の場合は他の研修自体を表示しなくしたため、警告不要。
	?>
	//alert("研修講座は変更できません。\n別の研修講座に変更したい場合は現在の研修講座をキャンセルし、\n新たに研修申込を行って下さい。");
	return;
	<?
}
?>

	//研修講座の情報をセットしてポストバック
	document.postback_form.training_id.value = training_id;
	document.postback_form.plan_id_csv.value = "";
	document.postback_form.submit();
}//function training_selected(training_id)  END



<?
//研修講座が選択されている場合のみ表示
if ($training_id != "")
{
?>


//「日程選択」ボタン押下時の処理
function select_button_clicked()
{
	//呼び出し元画面にインターフェース関数が定義されている場合
	if(window.opener && !window.opener.closed && window.opener.cl_training_select_return)
	{
		//インターフェース関数を介して親画面に値を返却します。
		var result = new Object();
		//研修ID
		result.training_id = "<?=to_javascript_string($training_id)?>";
		//研修名
		result.training_name = "<?=to_javascript_string($training_name)?>";
		//研修目的
		result.training_purpose = "<?=to_javascript_string($training_purpose)?>";

		//研修日程情報の配列
		result.schedule_list = new Array();

		//日程一覧のチェックボックスにチェックが付いている場合
		var input_tags = document.getElementsByTagName("input");
		var i_result_index = 0;
		var apply_status_ng_flg = false;
		var delete_plan_info = "";
		for(var i = 0; i < input_tags.length; i++)
		{
			if(input_tags[i].type == "checkbox" && input_tags[i].id.substr(0,11) == "entry_plan_" && input_tags[i].checked == true)
			{
				//日程情報をセット
				result.schedule_list[i_result_index] = m_schedule_list[input_tags[i].value];

				//申込受付が受付中では無い場合
				if(result.schedule_list[i_result_index].apply_status != 1)
				{
					apply_status_ng_flg = true;
				}

				//削除済みの日程の場合
				if(result.schedule_list[i_result_index].delete_flg == 1)
				{
					var i_time = result.schedule_list[i_result_index].training_time;
					var i_time_string = get_training_time_disp_string(i_time);
					if(delete_plan_info != "")
					{
						delete_plan_info = delete_plan_info + "、";
					}
					delete_plan_info = delete_plan_info + i_time_string;
				}

				i_result_index = i_result_index + 1;
			}
		}

		//申込受付がNGな日程が選択されている場合
		if(apply_status_ng_flg)
		{
			if(!confirm('現在受付中ではない日程が選択されています。よろしいでしょうか？'))
			{
				return;
			}
		}

		//削除されている日程が選択されている場合
		if(delete_plan_info != "")
		{
			alert("選択されている日程は取り下げとなりました。(" + delete_plan_info + ")\n別の日程を選択してください。");
			return;
		}

		//日程一覧のチェックボックスにチェックが１つも無い場合
		if(i_result_index == 0)
		{
			alert("日程を選択してください。");
			return;
		}

		//回数毎に１つずつ選択されているかのチェック
		for(i_time =1; i_time <= max_training_time; i_time++)
		{
			var selected_time_count = 0;
			for(var i = 0; i < result.schedule_list.length; i++)
			{
				if (result.schedule_list[i].training_time == i_time)
				{
					selected_time_count++;
				}
			}
			if(selected_time_count == 0)
			{
				var i_time_string = get_training_time_disp_string(i_time);
				alert(i_time_string + "が選択されていません。回数毎に１つずつ選択してください。");
				return;
			}
			if(selected_time_count >= 2)
			{
				var i_time_string = get_training_time_disp_string(i_time);
				alert(i_time_string + "が重複しています。回数毎に１つずつ選択してください。");
				return;
			}
		}

		window.opener.cl_training_select_return(result);
	}
	//自画面を終了します。
	window.close();
}


//日程情報
var m_schedule_list = new Array();
<?
for($i = 0; $i < count($arr_plan_list); $i++)
{
	//日程ID
	$l_plan_id   = $arr_plan_list[$i]['plan_id'];
	//研修回数
	$l_training_time = $arr_plan_list[$i]['training_time'];
	//最終フラグ
	$l_final_flg = $arr_plan_list[$i]['final_flg'];
	//開催日付
	$l_plan_date = $arr_plan_list[$i]['plan_date'];
	//開催時刻From
	$l_from_time = $arr_plan_list[$i]['from_time'];
	//開催時刻To
	$l_to_time   = $arr_plan_list[$i]['to_time'];
	//申込受付
	$l_apply_status = $arr_plan_list[$i]['apply_status'];
	//日付をフォーマット
	$l_plan_date_disp = get_date_disp_string($l_plan_date);
	$l_from_time_disp = get_time_disp_string($l_from_time);
	$l_to_time_disp   = get_time_disp_string($l_to_time);
	$l_delete_flg = $arr_plan_list[$i]['delete_flg'];
	?>
	m_schedule_list["<?=$l_plan_id?>"] = new Object();
	m_schedule_list["<?=$l_plan_id?>"].plan_id   = "<?=to_javascript_string($l_plan_id)?>";
	m_schedule_list["<?=$l_plan_id?>"].training_time   = "<?=to_javascript_string($l_training_time)?>";
	m_schedule_list["<?=$l_plan_id?>"].final_flg   = "<?=to_javascript_string($l_final_flg)?>";
	m_schedule_list["<?=$l_plan_id?>"].plan_date = "<?=to_javascript_string($l_plan_date_disp)?>";
	m_schedule_list["<?=$l_plan_id?>"].from_time = "<?=to_javascript_string($l_from_time_disp)?>";
	m_schedule_list["<?=$l_plan_id?>"].to_time   = "<?=to_javascript_string($l_to_time_disp)?>";
	m_schedule_list["<?=$l_plan_id?>"].apply_status = "<?=to_javascript_string($l_apply_status)?>";
	m_schedule_list["<?=$l_plan_id?>"].delete_flg = "<?=to_javascript_string($l_delete_flg)?>";
	<?
}
?>


var max_training_time = <?=$max_training_time?>;

function get_training_time_disp_string(training_time)
{
	var ret = "最終回";
	if(training_time != max_training_time)
	{
		ret = "第" + training_time + "回";
	}
	return ret;
}







<?
}//if ($training_id != "")  END
?>

//====================================================================================================
//■行のハイライト制御

//class属性が指定されたclass_nameのTDタグをハイライトします。
function highlightCells(class_name)
{
	changeCellsColor(class_name, '#ffff66');
}

//class属性が指定されたclass_nameのTDタグのハイライトを解除します。
function dehighlightCells(class_name)
{
	changeCellsColor(class_name, '');
}

//highlightCells()、dehighlightCells()専用関数
function changeCellsColor(class_name, color)
{
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++)
	{
		if (cells[i].className != class_name)
		{
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}



</script>
<style type="text/css">

table.list {border-collapse:collapse;}
table.list td {border:#A0D25A solid 1px;}

table.layout {border-collapse:collapse;}
table.layout td {border:0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="onload_action()">

<form name="postback_form" method="post" action="cl_common_call.php" target="_self">
<input type="hidden" id="common_module"            name="common_module"            value="<?=$common_module ?>">
<input type="hidden" id="session"                  name="session"                  value="<?=$session ?>">
<input type="hidden" id="gamen_mode"               name="gamen_mode"               value="<?=$gamen_mode ?>">
<input type="hidden" id="head_nurse_recommend_flg" name="head_nurse_recommend_flg" value="<?=$head_nurse_recommend_flg ?>">
<input type="hidden" id="emp_id"                   name="emp_id"                   value="<?=$emp_id ?>">
<input type="hidden" id="training_id"              name="training_id"              value="<?=$training_id ?>">
<input type="hidden" id="plan_id_csv"              name="plan_id_csv"              value="<?=$plan_id_csv ?>">


<center>


<!-- ヘッダー START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$page_title?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<!-- ヘッダー END -->






<!-- -------------------------------------------------------------------------------------------------------------- -->
<!-- ■研修講座一覧 -->
<!-- -------------------------------------------------------------------------------------------------------------- -->

<img src="img/spacer.gif" alt="" width="1" height="5"><br>

<div style="width:100%;height:200;overflow:scroll;">
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">

<!-- 研修一覧 ヘッダー START  -->
<tr bgcolor="#E5F6CD">
<td width="120" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">研修番号</font></td>
<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">研修名</font></td>
<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">目的</font></td>
<td width="70" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象者</font></td>
<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受講要件</font></td>
<td width="90" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">就業時間区分</font></td>
</tr>
<!-- 研修一覧 ヘッダー END -->




<!-- 研修一覧 データ START -->
<?
for($i = 0; $i < count($arr_training_list); $i++)//
{
	//配列情報を展開
	$l_training_id = $arr_training_list[$i]["training_id"];
	$l_training_name = $arr_training_list[$i]["training_name"];
	$l_training_purpose = $arr_training_list[$i]["training_purpose"];
	$l_target_level_required = $arr_training_list[$i]["target_level_required"];
	$l_training_require = $arr_training_list[$i]["training_require"];
	$l_time_division = $arr_training_list[$i]["time_division"];

	//対象レベルの必須　表示用
	$l_target_level_required_disp = "";
	switch ($l_target_level_required)
	{
		case 0:
			$l_target_level_required_disp = " ";
			break;
		case 1:
			$l_target_level_required_disp = $target_level_disp." (必須)";
			break;
		case 2:
			$l_target_level_required_disp =  $target_level_disp." (任意)";
			break;
	}

	//就業時間区分　表示用
	$l_time_division_disp = "";
	switch ($l_time_division)
	{
		case 1:
			$l_time_division_disp = "就業時間内";
			break;
		case 2:
			$l_time_division_disp = "就業時間外";
			break;
	}

	//ハイライトするセルのクラス属性値
	$l_highlight_class_name = "highlight_class_".$i;

	//日程変更であり、かつ研修IDが選択されていないものの場合は非表示
	if(!($gamen_mode == "plan_select" and $l_training_id !=$training_id))
	{
?>
<tr>
<td align="center" class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="training_selected('<?=$l_training_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_training_id))?></font></td>
<td                class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="training_selected('<?=$l_training_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_training_name))?></font></td>
<td                class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="training_selected('<?=$l_training_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_training_purpose))?></font></td>
<td align="center" class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="training_selected('<?=$l_training_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_target_level_required_disp))?></font></td>
<td                class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="training_selected('<?=$l_training_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_training_require))?></font></td>
<td align="center" class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="training_selected('<?=$l_training_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_time_division_disp))?></font></td>
</tr>
<?
	}

}
?>
<!-- 研修一覧 データ END -->

</table>
</div>

<img src="img/spacer.gif" alt="" width="1" height="5"><br>











<!-- -------------------------------------------------------------------------------------------------------------- -->
<!-- ■研修講座内容 -->
<!-- -------------------------------------------------------------------------------------------------------------- -->



<!-- 研修詳細の外枠 START -->
<?
//研修講座が選択されている場合のみ表示
if ($training_id != "")
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="list">
<tr bgcolor="#A0D25A">
<td height="22" class="spacing" ><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><?=$training_id?>:<?=$training_name?></font></b></td>
</tr>
<tr>
<td>




<!-- -------------------------------------------------------------------------------------------------------------- -->
<!-- ■日程一覧 -->
<!-- -------------------------------------------------------------------------------------------------------------- -->


<div style="width:100%;height:200;overflow:scroll;">
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">

<!-- 日程一覧 ヘッダー START  -->
<tr bgcolor="#E5F6CD">
<td width="30" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;</font></td>
<td width="250" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日程</font></td>
<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会場</font></td>
<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備考</font></td>
<td width="40" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最大<BR>人数</font></td>
<td width="40"  align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">現在<BR>人数</font></td>
<td width="70"  align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申込<BR>受付</font></td>
</tr>
<!-- 研修一覧 ヘッダー END -->



<!-- 日程一覧 データ START -->
<?



for($i = 0; $i < count($arr_plan_list); $i++)
{
	//配列情報を展開
	$l_plan_id = $arr_plan_list[$i]['plan_id'];
	$l_training_time = $arr_plan_list[$i]['training_time'];
	$l_final_flg = $arr_plan_list[$i]['final_flg'];
	$l_plan_date = $arr_plan_list[$i]['plan_date'];
	$l_from_time = $arr_plan_list[$i]['from_time'];
	$l_to_time = $arr_plan_list[$i]['to_time'];
	$l_max_people = $arr_plan_list[$i]['max_people'];
	$l_apply_status = $arr_plan_list[$i]['apply_status'];
	$l_place = $arr_plan_list[$i]['place'];
	$l_remarks = $arr_plan_list[$i]['remarks'];
	$l_training_roll_count = $arr_plan_list[$i]['training_roll_count'];
	$l_delete_flg = $arr_plan_list[$i]['delete_flg'];

	// 会場・備考　エスケープ
	$l_place = str_replace('&quot;', '"', $l_place);
	$l_place = str_replace('&lt;',   '<', $l_place);
	$l_place = str_replace('&gt;',   '>', $l_place);
	$l_remarks = str_replace('&quot;', '"', $l_remarks);
	$l_remarks = str_replace('&lt;',   '<', $l_remarks);
	$l_remarks = str_replace('&gt;',   '>', $l_remarks);


	//日時　表示用
	$l_training_time_disp = get_training_time_disp_string($l_training_time,$l_final_flg);
	$l_plan_date_disp = get_date_disp_string($l_plan_date);
	$l_from_time_disp = get_time_disp_string($l_from_time);
	$l_to_time_disp   = get_time_disp_string($l_to_time);
	$l_plan_date_time_disp = $l_training_time_disp . " " . $l_plan_date_disp . " " . $l_from_time_disp . " - " . $l_to_time_disp;

	//申込受付　表示用
	$l_apply_status_disp = "";
	switch ($l_apply_status)
	{
		case 0:
			$l_apply_status_disp = "準備中";
			break;
		case 1:
			$l_apply_status_disp = "受付中";
			break;
		case 2:
			$l_apply_status_disp = "終了";
			break;
		case 3:
			$l_apply_status_disp = "中止";
			break;
		case 4:
			$l_apply_status_disp = "日程変更";
			break;
	}

	//日程指定のチェックボックスのチェック状態
	$entry_plan_checked = "";
	if(in_array($l_plan_id, $plan_id_arr))
	{
		$entry_plan_checked = "checked";
	}

	$line_color_element = '';
	if($l_delete_flg == 1)
	{
		$line_color_element = ' bgcolor="#E0E0E0"';
	}
?>
<tr <?=$line_color_element?>>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" id="entry_plan_<?=$i?>" name="entry_plan_<?=$i?>" value="<?=$l_plan_id;?>" <?=$entry_plan_checked?> ></font></td>
<td               ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_plan_date_time_disp))?></font></td>
<td               ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_place))?></font></td>
<td               ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_remarks))?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_max_people))?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_training_roll_count))?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_apply_status_disp))?></font></td>
</tr>
<?
}//for($i = 0; $i < count($arr_plan_list); $i++)  END
?>
<!-- 研修一覧 データ END -->



</table>
</div>


<img src="img/spacer.gif" alt="" width="1" height="5"><br>





<!-- -------------------------------------------------------------------------------------------------------------- -->
<!-- ■選択ボタン -->
<!-- -------------------------------------------------------------------------------------------------------------- -->


<!-- 日程選択ボタン START -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="layout">
	<tr>
		<td align="left">
			<input type="button" id="select_button" name="select_button" value="日程選択" onclick="select_button_clicked()">
		</td>
	</tr>
</table>
<!-- 日程選択ボタン END -->

<img src="img/spacer.gif" alt="" width="1" height="10"><br>





<!-- -------------------------------------------------------------------------------------------------------------- -->
<!-- ■研修講座詳細 -->
<!-- -------------------------------------------------------------------------------------------------------------- -->





<!-- 研修講座詳細START -->

<!-- 研修内容 -->
<?
	//講師　表示用
	$all_teacher_disp = "";
	if($training_teacher1_disp != "")
	{
		$all_teacher_disp = $training_teacher1_disp;
	}
	if($training_teacher2_disp != "")
	{
		$all_teacher_disp .= "、";
		$all_teacher_disp .= $training_teacher2_disp;
	}
	if($training_teacher3_disp != "")
	{
		$all_teacher_disp .= "、";
		$all_teacher_disp .= $training_teacher3_disp;
	}
		if($training_teacher4_disp != "")
	{
		$all_teacher_disp .= "、";
		$all_teacher_disp .= $training_teacher4_disp;
	}
		if($training_teacher5_disp != "")
	{
		$all_teacher_disp .= "、";
		$all_teacher_disp .= $training_teacher5_disp;
	}

?>
<table width="800" border="0" cellspacing="0" cellpadding="1" class="layout">

	<tr>
		<td width="160" align="right">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">主催者</font>
		</td>
		<td width="200" align="left">
			<div style="width:180; height:1.2em; border:#5279a5 solid 1px;">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($training_opener_disp))?></font>
			</div>
		</td>
		<td width="40" align="right">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">講師</font>
		</td>
		<td width="*" align="left">
			<div style="width:380; border:#5279a5 solid 1px;">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($all_teacher_disp))?>&nbsp;</font>
			</div>
		</td>
	</tr>

	<tr>
		<td align="right">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">目標</font>
		</td>
		<td colspan="3">
			<div style="width:500; border:#5279a5 solid 1px;">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($training_slogan))?>&nbsp;</font>
			</div>
		</td>
	</tr>

	<tr>
		<td align="right">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font>
		</td>
		<td colspan="3">
			<div style="width:500; border:#5279a5 solid 1px;">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($training_contents))?>&nbsp;</font>
			</div>
		</td>
	</tr>


	<tr>
		<td align="right">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署での研修生の取り組み</font>
		</td>
		<td colspan="3">
			<div style="width:500; border:#5279a5 solid 1px;">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($training_action))?>&nbsp;</font>
			</div>
		</td>
	</tr>

	<tr>
		<td align="right">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">OJTでの支援内容</font>
		</td>
		<td colspan="3">
			<div style="width:500; border:#5279a5 solid 1px;">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($training_support))?>&nbsp;</font>
			</div>
		</td>
	</tr>


</table>



<img src="img/spacer.gif" alt="" width="1" height="10"><br>


<!-- 課題情報 -->
<?
for ($i_theme_division = 0; $i_theme_division < 3; $i_theme_division++)
{
	//現在のループで使用する課題情報を決定
	$arr_theme_info = "";
	for($i = 0; $i < count($arr_theme_list); $i++)
	{
		if( $arr_theme_list[$i]['theme_division'] == $i_theme_division )
		{
			$arr_theme_info = $arr_theme_list[$i];
			break;
		}
	}

	//配列情報を展開
	$theme_contents = "";
	$theme_method = "";
	$theme_deadline = "";
	$theme_theme_present = "";
	if($arr_theme_info != "")
	{
		$theme_contents = $arr_theme_info['theme_contents'];
		$theme_method = $arr_theme_info['theme_method'];
		$theme_deadline = $arr_theme_info['theme_deadline'];
		$theme_theme_present = $arr_theme_info['theme_present_disp'];
	}

	//課題区分　表示用
	$theme_division_disp = "";
	switch ($i_theme_division)
	{
		case 0:
			$theme_division_disp = "研修前課題";
			break;
		case 1:
			$theme_division_disp = "研修中課題";
			break;
		case 2:
			$theme_division_disp = "研修後課題";
			break;
	}


?>
<span style="font-weight:bold;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><?=$theme_division_disp?></font></span>
<table width="800" border="0" cellspacing="0" cellpadding="1" class="layout">
	<tr>
		<td width="160" align="right">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font><br />
		</td>
		<td width="*" align="left">
			<div style="width:500; border:#5279a5 solid 1px;">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($theme_contents))?>&nbsp;</font>
			</div>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">提出方法等</font><br />
		</td>
		<td align="left">
			<div style="width:500; border:#5279a5 solid 1px;">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($theme_method))?>&nbsp;</font>
			</div>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">提出期限</font><br />
		</td>
		<td align="left">
			<div style="width:300; border:#5279a5 solid 1px;">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($theme_deadline))?>&nbsp;</font>
			</div>
		</td>
	</tr>
	<tr>
		<td align="right">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">提出先</font><br />
		</td>
		<td align="left">
			<div style="width:300; border:#5279a5 solid 1px;">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($theme_theme_present))?>&nbsp;</font>
			</div>
		</td>
	</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<?
}//for ($i_theme_division = 0; $i_theme_division < 3; $i_theme_division++)  END
?>





</td>
</tr>
</table>
<?
}
?>
<!-- 研修詳細の外枠 END -->



</center>
</form>



</body>
</html>
<?

//====================================
// 終了処理
//====================================
$log->info(basename(__FILE__)." END");
$log->shutdown();




//====================================================================================================
// ローカル関数
//====================================================================================================


/**
 * 「開催者」、「講師」、「課題提出先」といった職員を表示文字列に変換します。
 *
 * @param $teacher_emp_id 表示する職員のemp_id
 * @param $empmst_model   empmstのモデル
 * @return 職員の表示文字列
 */
function get_emp_disp_string($teacher_emp_id, $empmst_model)
{
	$teacher_disp_string = "";
	if($teacher_emp_id != "")
	{
		$arrteacher_info = $empmst_model->select($teacher_emp_id);
		$teacher_disp_string = $arrteacher_info['emp_lt_nm']." ".$arrteacher_info['emp_ft_nm'];
	}
	return $teacher_disp_string;
}


/**
 * 研修回数を表示用文字列に変換します。
 *
 * @param $training_time DBより取得した日付データ。「YYYY-MM-DD」形式
 * @param $final_flg DBより取得した日付データ。「YYYY-MM-DD」形式
 * @return 日付の表示用文字列
 */
function get_training_time_disp_string($training_time,$final_flg)
{
	$ret = "最終回";
	if($final_flg != 1)
	{
		$ret = "第".$training_time."回";
	}
	return $ret;
}


/**
 * 日付を表示用文字列に変換します。「YYYY年MM月DD日(曜日)」
 *
 * @param $db_format_date DBより取得した日付データ。「YYYY-MM-DD」形式
 * @return 日付の表示用文字列
 */
function get_date_disp_string($db_format_date)
{
	$i_date = strtotime($db_format_date);
	$weekday = array( "日", "月", "火", "水", "木", "金", "土" );
	$ret = date("Y",$i_date)."年".date("m",$i_date)."月".date("d",$i_date)."日(".$weekday[date("w", $i_date)].")";
	return $ret;
}

/**
 * 時間を表示用文字列に変換します。「HH:mm」形式
 *
 * @param $db_format_time DBより取得した時間データ。「HH:mm:ss」形式
 * @return 時間の表示用文字列
 */
function get_time_disp_string($db_format_time)
{
	$ret = substr($db_format_time, 0,5);
	return $ret;
}

/**
 * JavaScript出力用に文字列変換します。
 *
 * @param $str 出力する文字列
 */
function to_javascript_string($str)
{
	$ret = $str;
	$ret = str_replace("\"","\\\"",$ret);
	$ret = str_replace("\r\n","\\n",$ret);
	$ret = str_replace("\r","\\n",$ret);
	$ret = str_replace("\n","\\n",$ret);
	return $ret;
}


?>