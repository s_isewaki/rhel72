<?php
/*
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cl_application_list.inc");
require_once("cl_yui_calendar_util.ini");
require_once("cl_application_common.ini");
require_once("cl_common.ini");
require_once("cl_title_name.ini");
require_once("cl_smarty_setting.ini");
require_once("cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once('MDB2.php');


$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

$smarty = cl_get_smarty_obj();
*/

	//##########################################################################
	// 各種モジュールの読み込み
	//##########################################################################
	require_once(dirname(__FILE__) . "/../../about_session.php");
	require_once(dirname(__FILE__) . "/../../about_authority.php");
	require_once(dirname(__FILE__) . "/../../about_postgres.php");
//	require_once(dirname(__FILE__) . "/../../show_select_values.ini");
	require_once(dirname(__FILE__) . "/../../cl_application_list.inc");
	require_once(dirname(__FILE__) . "/../../cl_application_common.ini");
	require_once(dirname(__FILE__) . "/../../cl_common.ini");
	require_once(dirname(__FILE__) . "/../../cl_title_name.ini");
	require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
	require_once(dirname(__FILE__) . "/../../Cmx.php");
	require_once("Smarty/Smarty.class.php");
	require_once("MDB2.php");

	//##########################################################################
	// 初期処理
	//##########################################################################

	//ロガーをロード
	$log = new cl_common_log_class(basename(__FILE__));
	$log->info(basename(__FILE__)." START");

	//画面名
	$fname = $PHP_SELF;


	//##########################################################################
	// パラメータ取得
	//##########################################################################
foreach( $_REQUEST as $key => $value ) {
	$log->debug('$_REQUEST['.$key.']:'.$_REQUEST[$key],__FILE__,__LINE__);
}
	$plan_id	= $_POST["plan_id"];
	//$session	= '21e6f8b67320018d487a90b29c802263';
	$training_id	= $_POST["training_id"];
	$p_session		= $_POST["p_session"];

//$_POST :: $key:plan_id $val:CLITS00000009
//$_POST :: $key:session $val:21e6f8b67320018d487a90b29c802263
//$_POST :: $key:training_id $val:CLTRA00000009

	$log->debug('■plan_id   :'.$plan_id	,__FILE__,__LINE__);
	$log->debug('■session   :'.$session	,__FILE__,__LINE__);
	$log->debug('■p_session   :'.$p_session	,__FILE__,__LINE__);
	$log->debug('■training_id   :'.$training_id	,__FILE__,__LINE__);

	$log->debug('■fname   :'.$fname	,__FILE__,__LINE__);

//====================================
//セッションのチェック
//====================================
//$session = qualify_session($session,$fname);
//if($session == "0"){
//	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
//	echo("<script language='javascript'>showLoginPage(window);</script>");
//	exit;
//}
	$log->debug('■session2   :'.$session	,__FILE__,__LINE__);
//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

	$log->debug('■$workflow_auth   :'.$workflow_auth	,__FILE__,__LINE__);

	//##########################################################################
	// データベース接続
	//##########################################################################
	$mdb2 = MDB2::connect(CMX_DB_DSN);

	//require_once(dirname(__FILE__) . "/../model/search/cl_mst_inside_training_schedule_util.php");
//	require_once(dirname(__FILE__) . "/../model/search/cl_mst_inside_training_schedule_util.php");
//	$model = new cl_mst_inside_training_schedule_util($mdb2, $login_user);

//	$data = $model->getList($training_id);

//foreach($data as $tmp){
//	foreach($tmp as $tmpp){
//		$log->debug('■$data0   :'.$tmpp	,__FILE__,__LINE__);
//	}
//}

	// セッションよりログインユーザー取得
	require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php");
	$empmst_model = new empmst_model($mdb2,$emp_id);
	$arr_empmst = $empmst_model->select_login_emp($session);

	$emp_id = $arr_empmst["emp_id"];

	$log->debug('■$emp_id2   :'.$emp_id	,__FILE__,__LINE__);

	// ヘッダーデータ取得
	require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_model.php");
	$cl_mst_inside_training_model = new cl_mst_inside_training_model($mdb2,$emp_id);
	$arr_training = $cl_mst_inside_training_model->select($training_id);

foreach($arr_training as $tmp){
//	foreach($tmp as $tmpp){
	$log->debug('■$arr_training   :'.$tmp	,__FILE__,__LINE__);
//	}
}

	require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_schedule_model.php");
	$cl_mst_inside_training_schedule_model = new cl_mst_inside_training_schedule_model($mdb2,$emp_id);
	$arr_schedule = $cl_mst_inside_training_schedule_model->select($plan_id);
	$arr_date = explode("-",$arr_schedule['plan_date']);
	$plan_date_year = $arr_date[0];
	$plan_date_month = $arr_date[1];
	$plan_date_day = $arr_date[2];
	// 2012/04/12 Yamagawa add(s)
	// 「23:59」は「24:00」に変換（暫定対応）
	if (substr($arr_schedule['from_time'],0,5) == "23:59") {
		$arr_schedule['from_time'] = "24:00";
	}
	if (substr($arr_schedule['to_time'],0,5) == "23:59") {
		$arr_schedule['to_time'] = "24:00";
	}
	// 2012/04/12 Yamagawa add(e)

	$log->debug('■$plan_date   :'.$arr_schedule['plan_date']	,__FILE__,__LINE__);


	// 一覧データ取得
	require_once(dirname(__FILE__) . "/../model/workflow/cl_inside_training_roll_model.php");
	$cl_inside_training_roll_model = new cl_inside_training_roll_model($mdb2, $emp_id);

	$param = array(
		"training_id"	=> $training_id,
		"plan_id"		=> $plan_id
	);

	$data = $cl_inside_training_roll_model->getReceptionList($param);

	// 取得したデータでループ
	$rowcnt = count($data);
	for ($index = 0; $index < count($data); $index++){
		$data[$index]['no'] = $index + 1;

	//	// 行番号・出欠フラグを追加
	//	switch($data[$index]['emp_roll']){
	//		case 0:
	//			$data[$index]['emp_roll'] = '';
	//			break;
	//		case 1:
	//			$data[$index]['emp_roll'] = 'checked';
	//			break;
	//		case 2:
	//			$data[$index]['emp_roll'] = 'disabled';
	//			break;
	//		default:
	//			$data[$index]['emp_roll'] = '';
	//			break;
	//	}
	}

	$log->debug('■$data   :'.$data	,__FILE__,__LINE__);


foreach($data as $tmp){
	foreach($tmp as $tmpp){
		$log->debug('■$data0   :'.$tmpp	,__FILE__,__LINE__);
	}
}

	// 行番号を追加
	$rowcnt = count($data);
	for ($index = 0; $index < $rowcnt; $index++){
		$data[$index]['no'] = $index + 1;
	}

	// CSV出力
	if ($_POST['csv_flg'] == 'true'){

	$log->debug('■ここきてる？   :'	,__FILE__,__LINE__);


//		// ヘッダー行
//		$csv  = format_column_for_csv('No');
//		$csv .= format_column_for_csv(',職員ID');
//		$csv .= format_column_for_csv(',職員氏名');
//		$csv .= format_column_for_csv(',所属');
//		$csv .= format_column_for_csv(',申込日時');

//		// 明細行
//		foreach ($data as $row){
//			$csv .= format_column_for_csv($row['no']);
//			$csv .= format_column_for_csv(",".$row['emp_id']);
//			$csv .= format_column_for_csv(",".$row['emp_name']);
//			$csv .= format_column_for_csv(",".$row['affiliation']);
//			$csv .= format_column_for_csv(",".$row['inside_training_date']);
//		}


		// ヘッダー行
		// 項目配列作成
		$aray_csv_header = array();
		$aray_csv_header[] = "No";
		$aray_csv_header[] = "職員ID";
		$aray_csv_header[] = "職員氏名";
		$aray_csv_header[] = "所属";
		$aray_csv_header[] = "申込日時";


		// 文字コード変換
		$csv_header = array();
		foreach($aray_csv_header as $tmp_csv_header){
			$csv_header[] = format_column_for_csv($tmp_csv_header);
		}

		// 配列から文字列化
		$csv_header = implode(",", $csv_header);


		$log->debug('■csv_header   :'.$csv_header	,__FILE__,__LINE__);

		// 明細行
		foreach ($data as $row){
			$csv_body .= "\r\n";
			$csv_body .= format_column_for_csv($row['no']);
			$csv_body .= format_column_for_csv(",".$row['emp_id']);
			$csv_body .= format_column_for_csv(",".$row['emp_name']);
			$csv_body .= format_column_for_csv(",".$row['affiliation']);
			$csv_body .= (",".$row['inside_training_date'] );
		$log->debug('■inside_training_date   :'.$row['inside_training_date']	,__FILE__,__LINE__);
		}

		// ヘッダーと明細を結合
		$csv = $csv_header . $csv_body;


$file_name = "reception_list.csv";
//$fp = fopen( $file_name, "w" );
		// CSVを出力
		ob_clean();
		header("Content-Type: application/octet-stream");
		header("Content-Disposition: attachment; filename=$file_name");
		header("Content-Length: " . strlen($csv) );

//		echo($csv);

//$fp = fopen( $file_name, "w" );
//fputs($fp, $csv);
//fclose( $fp );


	}

	//------------------------------------------------------------------------------
	// データベース接続を閉じる
	//------------------------------------------------------------------------------
	$log->debug("データベース切断 START",__FILE__,__LINE__);
	$mdb2->disconnect();
	$log->debug("データベース切断 END",__FILE__,__LINE__);

	//##########################################################################
	// 画面表示
	//##########################################################################
	$smarty = new Smarty();
	$smarty->template_dir = dirname(__FILE__) . "/../view";
	$smarty->compile_dir  = dirname(__FILE__) . "/../../templates_c";

	$smarty->assign( 'cl_title'			, $cl_title							);
	$smarty->assign( 'session'			, $session							);
	$smarty->assign( 'training_id'		, $training_id						);
	$smarty->assign( 'plan_id'			, $plan_id							);
	$smarty->assign( 'training_name'	, $arr_training['training_name']	);
	$smarty->assign( 'plan_date_year'	, $plan_date_year					);
	$smarty->assign( 'plan_date_month'	, $plan_date_month					);
	$smarty->assign( 'plan_date_day'	, $plan_date_day					);
//	$smarty->assign( 'plan_time_from'	, $arr_schedule['from_time']		);
//	$smarty->assign( 'plan_time_to'		, $arr_schedule['to_time']			);
	$smarty->assign( 'plan_time_from'	, substr($arr_schedule['from_time'],0,5)	);
	$smarty->assign( 'plan_time_to'		, substr($arr_schedule['to_time'],0,5)		);
	$smarty->assign( 'data'				, $data								);


	//テンプレート出力
	$smarty->display(basename(__FILE__,'.php').".tpl");

	function format_column_for_csv($value, $from_encoding = "EUC-JP") {
		$buf = str_replace("\r", "", $value);
		$buf = str_replace("\n", "", $buf);
		if (strpos($buf, ",") !== false)  {
			$buf = '"' . $buf . '"';
		}
		return mb_convert_encoding($buf, "Shift_JIS", $from_encoding);
	}


	$log->info(basename(__FILE__)." END");
	$log->shutdown();


// セッションよりログインユーザー取得
//require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php");
//$empmst_model = new empmst_model($mdb2,$emp_id);
//$arr_empmst = $empmst_model->select_login_emp($session);
//$emp_id = $arr_empmst[0]["emp_id"];

// ヘッダーデータ取得
//require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_model.php");
//$cl_mst_inside_training_model = new cl_mst_inside_training_model($mdb2,$emp_id);
//$arr_training = $cl_mst_inside_training_model->select($training_id);

//require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_schedule_model.php");
//$cl_mst_inside_training_schedule_model = new cl_mst_inside_training_schedule_model($mdb2,$emp_id);
//$arr_schedule = $cl_mst_inside_training_schedule_model->select($plan_id);
//$arr_date = explode("/",$arr_schedule['plan_date']);
//$plan_date_year = $arr_date[0];
//$plan_date_month = $arr_date[1];
//$plan_date_day = $arr_date[2];

// 一覧データ取得
//require_once(dirname(__FILE__) . "/../model/workflow/cl_apl_inside_training_apply_model.php");
//$cl_apl_inside_training_apply_model = new cl_apl_inside_training_apply_model($mdb2, $emp_id);

//$param = array(
//	"inside_training_id"	=> $training_id,
//	"plan_id"				=> $plan_id
//);
//$data = $cl_apl_inside_training_apply_model->getReceptionList($param);

// 行番号を追加
//$rowcnt = count($data);
//for ($index = 0; $index <= $rowcnt; $index++){
//	$data[$index]['no'] = $index + 1;
//}

// CSV出力
//if ($_POST['csv_flg'] == 'true'){

//	// ヘッダー行
//	$csv  = format_column_for_csv('No');
//	$csv .= format_column_for_csv(',職員ID');
//	$csv .= format_column_for_csv(',職員氏名');
//	$csv .= format_column_for_csv(',所属');
//	$csv .= format_column_for_csv(',申込日時');

//	// 明細行
//	foreach ($data as $row){
//		$csv .= format_column_for_csv($row['no']);
//		$csv .= format_column_for_csv(",".$row['emp_id']);
//		$csv .= format_column_for_csv(",".$row['emp_name']);
//		$csv .= format_column_for_csv(",".$row['affiliation']);
//		$csv .= format_column_for_csv(",".$row['inside_training_date']);
//	}

//	$file_name = "reception_list.csv";
//	header("Content-Disposition: attachment; filename=$file_name");
//	header("Content-Type: application/octet-stream; name=$file_name");
//	header("Content-Length: " . strlen($csv));
//	echo($csv);

//}

// データベース切断
//$mdb2->disconnect();



?>