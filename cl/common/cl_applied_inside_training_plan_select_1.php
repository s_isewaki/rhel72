<?
/*
院内研修日程一覧
・院内研修報告を行う際に、報告する研修の日程を選択する画面


【呼び出しパラメータ】
session　　　　　　　　　　セッションID。
emp_id 　　　　　　　　　　研修受講者の職員ID。

【戻り値】
training_apply_id　　　　　研修申込の院内研修ID
training_id　　　　　　　　研修ID
training_name　　　　　　　研修名
training_purpose 　　　　　目的
training_slogan　　　　　　目標
training_contents　　　　　内容
plan_id　　　　　　　　　　日程ID
training_time　　　　　　　研修回数
final_flg　　　　　　　　　最終フラグ
plan_date　　　　　　　　　開催日付(書式:"YYYY年MM月dd日(曜日)")
from_time　　　　　　　　　開催時刻From(書式:"HH:mm")
to_time　　　　　　　　　　開催時刻To(書式:"HH:mm")
*/


//TODO 戻り値に「研修から学んだこと（前回まで）」を含めるかは要検討

require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cl_application_list.inc");
require_once("cl_application_common.ini");
require_once("cl_common.ini");
require_once("cl_title_name.ini");
require_once("cl_smarty_setting.ini");
require_once("cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once('MDB2.php');



//====================================
//リクエストパラメータ取得
//====================================

//呼び出しパラメータ
$common_module = $_POST["common_module"];
$session = $_POST["session"];
$emp_id = $_POST["emp_id"];

//画面上部の研修一覧で選択された院内研修申請ID
$training_apply_id = $_POST["training_apply_id"];


//====================================
//初期処理
//====================================

//ロガーをロード
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");


//画面名
$fname = $PHP_SELF;


//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//権限チェック
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}



//====================================
// データベース接続
//====================================
$log->debug('データベース接続 START',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
$log->debug('データベース接続 END',__FILE__,__LINE__);



//====================================
// モデルをロード
//====================================

$log->debug('require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php"); START',__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php");
$empmst_model = new empmst_model($mdb2);
$log->debug('require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php"); END',__FILE__,__LINE__);

// セッションよりログインユーザーIDを取得
$arr_empmst = $empmst_model->select_login_emp($session);
$login_emp_id = $arr_empmst[0]["emp_id"];

$log->debug('require_once(dirname(__FILE__) . "/../model/search/cl_applied_inside_training_plan_list_model.php"); START',__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/search/cl_applied_inside_training_plan_list_model.php");
$cl_applied_inside_training_plan_list_model = new cl_applied_inside_training_plan_list_model($mdb2,$login_emp_id);
$log->debug('require_once(dirname(__FILE__) . "/../model/search/cl_applied_inside_training_plan_list_model.php"); END',__FILE__,__LINE__);

//====================================
// データ取得
//====================================

//院内研修申込一覧を取得
$log->debug('$cl_applied_inside_training_plan_list_model->get_applied_training_plan_list($emp_id); START',__FILE__,__LINE__);
$arr_training_list = $cl_applied_inside_training_plan_list_model->get_applied_training_plan_list($emp_id);
$log->debug('$cl_applied_inside_training_plan_list_model->get_applied_training_plan_list($emp_id); END',__FILE__,__LINE__);



//====================================
// データベース切断
//====================================
$log->debug('データベース切断 START',__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug('データベース切断 END',__FILE__,__LINE__);



//====================================================================================================
// HTML出力
//====================================================================================================

$page_title = "院内研修日程一覧";

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cl_title?>｜<?=$page_title?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<script type="text/javascript">


//====================================================================================================
//■イベント処理

//研修一覧の行クリックにて研修が選択された時の処理
function training_apply_id_selected(training_apply_id)
{
	//院内研修申請IDををセットしてポストバック
	document.postback_form.training_apply_id.value = training_apply_id;
	document.postback_form.submit();

}

//報告ボタン押下時の処理
function do_return()
{
	//研修申し込みID
	var training_apply_id = "<?=$training_apply_id?>";

	//日程ID
	var plan_id = "";
	var plan_radio_list = document.getElementsByName("selected_plan_id");
	for(var i = 0; i < plan_radio_list.length; i++)
	{
		if(plan_radio_list[i].checked)
		{
			plan_id = plan_radio_list[i].value;
		}
	}

	if(plan_id == "")
	{
		alert("選択された研修は全て報告済みです。");
		return;
	}

	//呼び出し元画面にインターフェース関数が定義されている場合
	if(window.opener && !window.opener.closed && window.opener.cl_applied_inside_training_plan_select_return)
	{
		//返却値を取得
		var array_key = training_apply_id + '_' + plan_id
		var result = m_return_data_list[array_key];
		//親画面へ通知
		window.opener.cl_applied_inside_training_plan_select_return(result);
	}
	//自画面を終了します。
	window.close();
}



//	//研修講座一覧から研修講座を選択した時の処理
//	function return_selected(training_apply_id,plan_id)
//	{
//		//呼び出し元画面にインターフェース関数が定義されている場合
//		if(window.opener && !window.opener.closed && window.opener.cl_applied_inside_training_plan_select_return)
//		{
//			//返却値を取得
//			var array_key = training_apply_id + '_' + plan_id
//			var result = m_return_data_list[array_key];
//			//親画面へ通知
//			window.opener.cl_applied_inside_training_plan_select_return(result);
//		}
//		//自画面を終了します。
//		window.close();
//	}//function return_selected(training_apply_id)  END



//返却値配列(key:研修申込の院内研修ID_日程ID、val:返却値)
var m_return_data_list = new Array();
<?
for($i = 0; $i < count($arr_training_list); $i++)
{
	//研修申込の院内研修ID
	$l_training_apply_id = $arr_training_list[$i]['training_apply_id'];
	//研修ID
	$l_training_id = $arr_training_list[$i]['training_id'];
	//研修名
	$l_training_name = $arr_training_list[$i]['training_name'];
	//目的
	$l_training_purpose = $arr_training_list[$i]['training_purpose'];
	//目標
	$l_training_slogan = $arr_training_list[$i]['training_slogan'];
	//研修内容
	$l_training_contents = $arr_training_list[$i]['training_contents'];

	//研修日程情報
	$l_plan_list = $arr_training_list[$i]['plan_list'];

	for($i_plan = 0; $i_plan < count($l_plan_list); $i_plan++)
	{
		//日程ID
		$l_plan_id   = $l_plan_list[$i_plan]['plan_id'];
		//研修回数
		$l_training_time = $l_plan_list[$i_plan]['training_time'];
		//最終フラグ
		$l_final_flg = $l_plan_list[$i_plan]['final_flg'];
		//開催日付
		$l_plan_date = $l_plan_list[$i_plan]['plan_date'];
		//開催時刻From
		$l_from_time = $l_plan_list[$i_plan]['from_time'];
		//開催時刻To
		$l_to_time   = $l_plan_list[$i_plan]['to_time'];

		//日付をフォーマット
		$l_plan_date_disp = get_date_disp_string($l_plan_date);
		$l_from_time_disp = get_time_disp_string($l_from_time);
		$l_to_time_disp   = get_time_disp_string($l_to_time);
		// 2012/04/12 Yamagawa add(s)
		// 「23:59」は「24:00」に変換（暫定対応）
		if ($l_from_time_disp == "23:59") {
			$l_from_time_disp = "24:00";
		}
		if ($l_to_time_disp == "23:59") {
			$l_to_time_disp = "24:00";
		}
		// 2012/04/12 Yamagawa add(e)

		//配列キー
		$array_key = $l_training_apply_id.'_'.$l_plan_id
?>
	m_return_data_list["<?=$array_key?>"] = new Object();
	m_return_data_list["<?=$array_key?>"].training_apply_id   = "<?=to_javascript_string($l_training_apply_id)?>";
	m_return_data_list["<?=$array_key?>"].training_id         = "<?=to_javascript_string($l_training_id)?>";
	m_return_data_list["<?=$array_key?>"].training_name       = "<?=to_javascript_string($l_training_name)?>";
	m_return_data_list["<?=$array_key?>"].training_purpose    = "<?=to_javascript_string($l_training_purpose)?>";
	m_return_data_list["<?=$array_key?>"].training_slogan     = "<?=to_javascript_string($l_training_slogan)?>";
	m_return_data_list["<?=$array_key?>"].training_contents   = "<?=to_javascript_string($l_training_contents)?>";
	m_return_data_list["<?=$array_key?>"].plan_id             = "<?=to_javascript_string($l_plan_id)?>";
	m_return_data_list["<?=$array_key?>"].training_time       = "<?=to_javascript_string($l_training_time)?>";
	m_return_data_list["<?=$array_key?>"].final_flg           = "<?=to_javascript_string($l_final_flg)?>";
	m_return_data_list["<?=$array_key?>"].plan_date           = "<?=to_javascript_string($l_plan_date_disp)?>";
	m_return_data_list["<?=$array_key?>"].from_time           = "<?=to_javascript_string($l_from_time_disp)?>";
	m_return_data_list["<?=$array_key?>"].to_time             = "<?=to_javascript_string($l_to_time_disp)?>";
<?
	}
}//for($i = 0; $i < count($arr_training_list); $i++) END
?>



//====================================================================================================
//■行のハイライト制御

//class属性が指定されたclass_nameのTDタグをハイライトします。
function highlightCells(class_name)
{
	changeCellsColor(class_name, '#ffff66');
}

//class属性が指定されたclass_nameのTDタグのハイライトを解除します。
function dehighlightCells(class_name)
{
	changeCellsColor(class_name, '');
}

//highlightCells()、dehighlightCells()専用関数
function changeCellsColor(class_name, color)
{
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++)
	{
		if (cells[i].className != class_name)
		{
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}



</script>
<style type="text/css">

table.list {border-collapse:collapse;}
table.list td {border:#A0D25A solid 1px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">

<form name="postback_form" method="post" action="cl_common_call.php" target="_self">
<input type="hidden" id="common_module"            name="common_module"            value="<?=$common_module ?>">
<input type="hidden" id="session"                  name="session"                  value="<?=$session ?>">
<input type="hidden" id="emp_id"                   name="emp_id"                   value="<?=$emp_id ?>">
<input type="hidden" id="training_apply_id"        name="training_apply_id"        value="<?=$training_apply_id ?>">


<center>


<!-- ヘッダー START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$page_title?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<!-- ヘッダー END -->


<img src="img/spacer.gif" alt="" width="1" height="5"><br>




<!-- -------------------------------------------------------------------------------------------------------------- -->
<!-- ■研修一覧 -->
<!-- -------------------------------------------------------------------------------------------------------------- -->


<!-- 研修一覧 START  -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">

<!-- 研修一覧 ヘッダー START  -->
<tr bgcolor="#E5F6CD">
<td width="140" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
<td width="110" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">院内研修番号</font></td>
<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">院内研修名</font></td>
<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">目的</font></td>
</tr>
<!-- 研修一覧 ヘッダー END -->


<!-- 研修一覧 データ START -->
<?
for($i = 0; $i < count($arr_training_list); $i++)
{
	//配列情報を展開
	$l_training_apply_id = $arr_training_list[$i]['training_apply_id'];
	$l_inside_training_date = $arr_training_list[$i]['inside_training_date'];
	$l_training_id = $arr_training_list[$i]['training_id'];
	$l_training_name = $arr_training_list[$i]['training_name'];
	$l_training_purpose = $arr_training_list[$i]['training_purpose'];
	$l_plan_list = $arr_training_list[$i]['plan_list'];

	//申請日　表示用
	$l_inside_training_date_disp = get_date_disp_string($l_inside_training_date);
	$l_training_id_disp = $l_training_id;
	$l_training_name_disp = $l_training_name;
	//2012/05/11 K.Fujii ins(s)
	$l_training_name_disp = str_replace('\'\'',   '&#039;', $l_training_name_disp);
	$l_training_name_disp = str_replace('"',      '&quot;', $l_training_name_disp);
	$l_training_name_disp = str_replace('\\\\',   '\\',     $l_training_name_disp);
	$l_training_name_disp = str_replace('<',      '&lt;',   $l_training_name_disp);
	$l_training_name_disp = str_replace('>',      '&gt;',   $l_training_name_disp);
	//2012/05/11 K.Fujii ins(s)
	$l_training_purpose_disp = $l_training_purpose;
	//2012/05/11 K.Fujii ins(s)
	$l_training_purpose_disp = str_replace('\'\'',   '&#039;', $l_training_purpose_disp);
	$l_training_purpose_disp = str_replace('"',      '&quot;', $l_training_purpose_disp);
	$l_training_purpose_disp = str_replace('\\\\',   '\\',     $l_training_purpose_disp);
	$l_training_purpose_disp = str_replace('<',      '&lt;',   $l_training_purpose_disp);
	$l_training_purpose_disp = str_replace('>',      '&gt;',   $l_training_purpose_disp);
	//2012/05/11 K.Fujii ins(s)


	//ハイライトするセルのクラス属性値
	$l_highlight_class_name = "highlight_class_".$i."_".$l_training_apply_id;

	//実行JavaScript関数
	$l_line_click_javascript = "training_apply_id_selected('$l_training_apply_id');";
?>
<tr>
<td align="center" class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="<?=$l_line_click_javascript?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_inside_training_date_disp))?></font></td>
<td align="center" class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="<?=$l_line_click_javascript?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_training_id_disp))?></font></td>
<!-- 2012/05/11 K.Fujii upd(s) -->
<!--<td                class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="<?=$l_line_click_javascript?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_training_name_disp))?></font></td>
<td                class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="<?=$l_line_click_javascript?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_training_purpose_disp))?></font></td>-->
<td                class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="<?=$l_line_click_javascript?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br($l_training_name_disp)?></font></td>
<td                class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="<?=$l_line_click_javascript?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br($l_training_purpose_disp)?></font></td>
<!-- 2012/05/11 K.Fujii upd(e) -->
</tr>
<?
}
?>
<!-- 研修一覧 データ END -->

</table>
<!-- 研修一覧 END  -->




<br>


<!-- -------------------------------------------------------------------------------------------------------------- -->
<!-- ■日程一覧 -->
<!-- -------------------------------------------------------------------------------------------------------------- -->
<?
if($training_apply_id != "")
{
?>

<!-- 日程一覧 データ START -->
<?
for($i = 0; $i < count($arr_training_list); $i++)
{
	//配列情報を展開
	$l_training_apply_id = $arr_training_list[$i]['training_apply_id'];
	$l_inside_training_date = $arr_training_list[$i]['inside_training_date'];
	$l_training_id = $arr_training_list[$i]['training_id'];
	$l_training_name = $arr_training_list[$i]['training_name'];
	$l_training_purpose = $arr_training_list[$i]['training_purpose'];
	$l_plan_list = $arr_training_list[$i]['plan_list'];
	// 2012/11/02 Yamagawa add(s)
	// 院内研修変更申込中フラグ
	$l_change_apply_flg = $arr_training_list[$i]['change_apply_flg'];
	// 2012/11/02 Yamagawa add(e)

	if($l_training_apply_id == $training_apply_id)
	{
		// 2012/11/02 Yamagawa add(s)
		if ($l_change_apply_flg == '0') {
		// 2012/11/02 Yamagawa add(e)
?>
			<!-- 日程一覧 START  -->
			<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">

			<!-- 日程一覧 ヘッダー START  -->
				<tr bgcolor="#E5F6CD">
					<td width="20" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;</font></td>
					<td width="140" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
					<td width="110" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">院内研修番号</font></td>
					<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">院内研修名</font></td>
					<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">目的</font></td>
					<td width="250" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日程</font></td>
					<td width="50" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出欠</font></td>
				</tr>
			<!-- 日程一覧 ヘッダー END -->
<?
			//申請日　表示用
			$l_inside_training_date_disp = get_date_disp_string($l_inside_training_date);
			$l_training_id_disp = $l_training_id;
			$l_training_name_disp = $l_training_name;
			//2012/05/11 K.Fujii ins(s)
			$l_training_name_disp = str_replace('\'\'',   '&#039;', $l_training_name_disp);
			$l_training_name_disp = str_replace('"',      '&quot;', $l_training_name_disp);
			$l_training_name_disp = str_replace('\\\\',   '\\',     $l_training_name_disp);
			$l_training_name_disp = str_replace('<',      '&lt;',   $l_training_name_disp);
			$l_training_name_disp = str_replace('>',      '&gt;',   $l_training_name_disp);
			//2012/05/11 K.Fujii ins(s)
			$l_training_purpose_disp = $l_training_purpose;
			//2012/05/11 K.Fujii ins(s)
			$l_training_purpose_disp = str_replace('\'\'',   '&#039;', $l_training_purpose_disp);
			$l_training_purpose_disp = str_replace('"',      '&quot;', $l_training_purpose_disp);
			$l_training_purpose_disp = str_replace('\\\\',   '\\',     $l_training_purpose_disp);
			$l_training_purpose_disp = str_replace('<',      '&lt;',   $l_training_purpose_disp);
			$l_training_purpose_disp = str_replace('>',      '&gt;',   $l_training_purpose_disp);
			//2012/05/11 K.Fujii ins(s)

			for($i_plan = 0; $i_plan < count($l_plan_list); $i_plan++)//
			{
				$l_plan_id = $l_plan_list[$i_plan]['plan_id'];
				$l_training_time = $l_plan_list[$i_plan]['training_time'];
				$l_final_flg = $l_plan_list[$i_plan]['final_flg'];
				$l_plan_date = $l_plan_list[$i_plan]['plan_date'];
				$l_from_time = $l_plan_list[$i_plan]['from_time'];
				$l_to_time = $l_plan_list[$i_plan]['to_time'];
				$l_emp_roll = $l_plan_list[$i_plan]['emp_roll'];

				//研修報告済みフラグ
				$l_reported_flg     = $l_plan_list[$i_plan]['reported_flg'];
				//過去の研修報告済みフラグ
				$l_old_reported_flg  = $l_plan_list[$i_plan]['old_reported_flg'];

				//日程　表示用
				$l_training_time_disp = get_training_time_disp_string($l_training_time, $l_final_flg);
				$l_plan_date_disp = get_date_disp_string($l_plan_date);
				$l_from_time_disp = get_time_disp_string($l_from_time);
				$l_to_time_disp   = get_time_disp_string($l_to_time);
				// 2012/04/12 Yamagawa add(s)
				// 「23:59」は「24:00」に変換（暫定対応）
				if ($l_from_time_disp == "23:59") {
					$l_from_time_disp = "24:00";
				}
				if ($l_to_time_disp == "23:59") {
					$l_to_time_disp = "24:00";
				}
				// 2012/04/12 Yamagawa add(e)
				$l_plan_date_time_disp =  $l_training_time_disp . " " . $l_plan_date_disp . " " . $l_from_time_disp . " - " . $l_to_time_disp;

				$l_emp_roll_disp = "";
				switch ($l_emp_roll)
				{
					case 0:
						$l_emp_roll_disp = "欠席";
						break;
					case 1:
						$l_emp_roll_disp = "出席";
						break;
					case 2:
						$l_emp_roll_disp = "キャンセル";//TODO キャンセルを廃止するか要相談
						break;
				}

	//			if($i_plan != 0)
	//			{
	//				$l_inside_training_date_disp = "";
	//				$l_training_id_disp = "";
	//				$l_training_name_disp = "";
	//				$l_training_purpose_disp = "";
	//			}

				//ハイライトするセルのクラス属性値
				$l_highlight_class_name = "highlight_class_".$i."_".$i_plan;

				//チェックボックスのチェック状態
				//未報告で前回までの報告が報告済みの場合に選択状態にする。
				$plan_checked_html = "";
				if($l_reported_flg != 1 and $l_old_reported_flg == 1)
				{
					$plan_checked_html = "checked";
				}
?>
				<tr <?=$default_line_color?>>
					<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="selected_plan_id" value="<?=$l_plan_id?>" <?=$plan_checked_html?> disabled></font></td>
					<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_inside_training_date_disp))?></font></td>
					<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_training_id_disp))?></font></td>
					<!-- 2012/05/11 K.Fujii upd(e) -->
					<!--<td               ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_training_name_disp))?></font></td>
					<td               ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_training_purpose_disp))?></font></td>-->
					<td               ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br($l_training_name_disp)?></font></td>
					<td               ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br($l_training_purpose_disp)?></font></td>
					<!-- 2012/05/11 K.Fujii upd(e) -->
					<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_plan_date_time_disp))?></font></td>
					<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_emp_roll_disp))?></font></td>
				</tr>
<?
			}
?>
			<!-- 日程一覧 データ END -->

			</table>
			<!-- 日程一覧 END  -->


			<!-- 報告ボタン START -->
			<table width="100%" border="0" cellspacing="0" cellpadding="1">
				<tr>
					<td align="left">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
							<input type="button" value="報告" onclick="do_return()">
						</font>
					</td>
				</tr>
			</table>
			<!-- 報告ボタン END -->
<?
		// 2012/11/02 Yamagawa add(s)
		} else {
?>
			<script language="javascript">
				alert('申請中の院内研修変更申込が存在する為、選択できません。');
			</script>
<?
		}
		// 2012/11/02 Yamagawa add(e)
	}//if($l_training_apply_id == $training_apply_id)
}
?>

<?
}//if($training_apply_id != "")
?>

</center>
</form>

</body>
</html>
<?

//====================================
// 終了処理
//====================================
$log->info(basename(__FILE__)." END");
$log->shutdown();

//====================================================================================================
// ローカル関数
//====================================================================================================

/**
 * 研修回数を表示用文字列に変換します。
 *
 * @param $training_time DBより取得した日付データ。「YYYY-MM-DD」形式
 * @param $final_flg DBより取得した日付データ。「YYYY-MM-DD」形式
 * @return 日付の表示用文字列
 */
function get_training_time_disp_string($training_time,$final_flg)
{
	$ret = "最終回";
	if($final_flg != 1)
	{
		$ret = "第".$training_time."回";
	}
	return $ret;
}


/**
 * 日付を表示用文字列に変換します。「YYYY年MM月DD日(曜日)」
 *
 * @param $db_format_date DBより取得した日付データ。「YYYY-MM-DD」形式
 * @return 日付の表示用文字列
 */
function get_date_disp_string($db_format_date)
{
	$i_date = strtotime($db_format_date);
	$weekday = array( "日", "月", "火", "水", "木", "金", "土" );
	$ret = date("Y",$i_date)."年".date("m",$i_date)."月".date("d",$i_date)."日(".$weekday[date("w", $i_date)].")";
	return $ret;
}

/**
 * 時間を表示用文字列に変換します。「HH:mm」形式
 *
 * @param $db_format_time DBより取得した時間データ。「HH:mm:ss」形式
 * @return 時間の表示用文字列
 */
function get_time_disp_string($db_format_time)
{
	$ret = substr($db_format_time, 0,5);
	return $ret;
}

/**
 * JavaScript出力用に文字列変換します。
 *
 * @param $str 出力する文字列
 */
function to_javascript_string($str)
{
	$ret = $str;
	$ret = str_replace("\"","\\\"",$ret);
	$ret = str_replace("\r\n","\\n",$ret);
	$ret = str_replace("\r","\\n",$ret);
	$ret = str_replace("\n","\\n",$ret);
	//2012/05/11 K.Fujii ins(s)
	$ret = str_replace("''","'",$ret);
	//2012/05/11 K.Fujii ins(e)
	return $ret;
}


?>