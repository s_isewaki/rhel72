<?php
//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");
require_once('MDB2.php');
require_once(dirname(__FILE__) . "/../model/search/cl_apl_inside_training_report_training_learned_model.php");

$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");


//抽出元
//　テーブル：cl_apl_inside_training_report
//　カラム　：training_learned
//
//抽出条件
//　training_apply_id　：今回報告する申し込み済み研修のID
//　plan_id　 ：今回報告する日程のID(この日程よりも前の研修報告が対象)

$training_apply_id = $_GET["training_apply_id"];
$plan_id = $_GET["plan_id"];


$log->debug('$training_apply_id:'.$training_apply_id,__FILE__,__LINE__);
$log->debug('$plan_id          :'.$plan_id,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 申請書データ取得処理を呼び出す
//------------------------------------------------------------------------------

$json = getJsonData($mdb2, $training_apply_id, $plan_id);

$log->debug('$json:'.$json,__FILE__,__LINE__);



//------------------------------------------------------------------------------
// データベース接続を閉じる
//------------------------------------------------------------------------------
$log->debug("データベース切断 START",__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);

header("Content-Type: application/json; charset=EUC-JP");
echo $json;

/**
 * 変更申請書データ取得
 */
function getJsonData($mdb2, $training_apply_id, $plan_id)
{
	global $log;
	$log->debug(__FUNCTION__." START");

	//--------------------------------------------------------------------------
	// 院内研修申請テーブルモデルインスタンス生成
	//--------------------------------------------------------------------------
	$log->debug("apply modelインスタンス作成開始",__FILE__,__LINE__);
	$model = new cl_apl_inside_training_report_training_learned_model($mdb2, $user);
	$log->debug("apply modelインスタンス作成終了",__FILE__,__LINE__);

	//--------------------------------------------------------------------------
	// 院内研修申請テーブルデータ取得
	//--------------------------------------------------------------------------
	$log->debug("getRecJson開始",__FILE__,__LINE__);
	$learned_data = $model->getList($training_apply_id, $plan_id);
	$log->debug("getRecJson終了",__FILE__,__LINE__);

	$data["learned_data"] = $learned_data;
	$json = create_json($data);

	$json = str_replace("\r\n","\n",$json);
	$json = str_replace("\r","\n",$json);
	$json = str_replace("\n","\\n",$json);

	$log->debug(__FUNCTION__." END");

	return $json;
}
