<?php
//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");
require_once('MDB2.php');
require_once(dirname(__FILE__) . "/../model/search/cl_levelup_recognize_apply_model.php");

$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

//レベルアップ申請ID
$levelup_apply_id=$_GET["levelup_apply_id"];
$log->debug('levelup_apply_id:'.$levelup_apply_id,__FILE__,__LINE__);

//対象レベル
$target_level =$_GET["target_level"];
$log->debug('target_level:'.$target_level,__FILE__,__LINE__);


//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

if($levelup_apply_id != "" && $apply_id == ""){
	//------------------------------------------------------------------------------
	// レベルアップ申請データ取得処理を呼び出す
	//------------------------------------------------------------------------------
	$json=getJsonLevelupApplyData($mdb2, $levelup_apply_id);
}else{
	//------------------------------------------------------------------------------
	// レベルアップ認定データ取得処理を呼び出す
	//------------------------------------------------------------------------------
	$json=getJsonLevelupRecognizeData($mdb2, $apply_id, $target_level);

}

$log->debug('$json:'.$json,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベース接続を閉じる
//------------------------------------------------------------------------------
$log->debug("データベース切断 START",__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);

header("Content-Type: application/json; charset=EUC-JP");
echo $json;

/**
 * レベルアップ申請データ取得
 */
function getJsonLevelupApplyData($mdb2, $levelup_apply_id){
	global $log;
	$log->debug(__FUNCTION__." START");

	$result_data = array();
	$log->debug("レベルアップ申請データ読込み START",__FILE__,__LINE__);
	$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
	$cl_levelup_recognize_apply_model = new cl_levelup_recognize_apply_model($mdb2, $user);
	$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

	//--------------------------------------------------------------------------
	// レベルアップ申請 レベルアップ申請者データ取得
	//--------------------------------------------------------------------------
	$log->debug("レベルアップ申請者データ取得読込み Start",__FILE__,__LINE__);
	$data=$cl_levelup_recognize_apply_model->get_levelup_emp_data($levelup_apply_id);
	$log->debug("レベルアップ申請者取得読込み End",__FILE__,__LINE__);

	$result_data["levelup_recognize_emp_data"] = $data;

	//--------------------------------------------------------------------------
	// レベルアップ申請 研修受講データ取得
	//--------------------------------------------------------------------------
	$log->debug("研修受講データ取得読込み Start",__FILE__,__LINE__);
	$data=$cl_levelup_recognize_apply_model->get_levelup_apply_training_list($levelup_apply_id);
	$log->debug("研修受講データ取得読込み End",__FILE__,__LINE__);

	$result_data["training_data"] = $data;

	//--------------------------------------------------------------------------
	// レベルアップ申請 評価データ取得
	//--------------------------------------------------------------------------
	$log->debug("評価データ読込み Start　　levelup_apply_id = ".$levelup_apply_id,__FILE__,__LINE__);
	$evaluation_data= $cl_levelup_recognize_apply_model->get_evaluation_data_list($levelup_apply_id);
	$log->debug("評価データ読込み End   count(evaluation_data) = ".count($evaluation_data),__FILE__,__LINE__);

	$result_data["evaluation_data"] = $evaluation_data;

	//--------------------------------------------------------------------------
	// レベルアップ申請 合格条件情報取得
	//--------------------------------------------------------------------------
	$log->debug("合格条件データ読込み Start　　levelup_apply_id = ".$levelup_apply_id,__FILE__,__LINE__);
	$get_pass_conditions= $cl_levelup_recognize_apply_model->get_pass_conditions($levelup_apply_id);
	$log->debug("合格条件データ読込み End   count(evaluation_data) = ".count($evaluation_data),__FILE__,__LINE__);

	$result_data["get_pass_conditions"] = $get_pass_conditions;


	$log->debug("レベルアップ申請データ読込み END",__FILE__,__LINE__);

	$json=create_json($result_data);

	$log->debug(__FUNCTION__." END");

	return $json;
}


/**
 * レベルアップ認定データ取得
 */
function getJsonLevelupRecognizeData($mdb2, $apply_id, $target_level){
	global $log;
	$log->debug(__FUNCTION__." START");

	$result_data = array();
	$log->debug("レベルアップ認定データ読込み START",__FILE__,__LINE__);
	$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
	$cl_levelup_recognize_apply_model = new cl_levelup_recognize_apply_model($mdb2, $user);
	$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

	//--------------------------------------------------------------------------
	// レベルアップ申請データ取得
	//--------------------------------------------------------------------------
	$log->debug("レベルアップ認定 対象職員データ取得読込み Start",__FILE__,__LINE__);
	$data=$cl_levelup_recognize_apply_model->get_levelup_recognize_emp_data($apply_id);
	$log->debug("レベルアップ認定 対象職員データ取得読込み End",__FILE__,__LINE__);

	//2012/05/18 K.Fujii ins(s)
	$data[0]["reason"]     = str_replace("\r\n",   "\\n",$data[0]["reason"]);			// 改行
	//2012/05/18 K.Fujii ins(e)

	$result_data["levelup_recognize_emp_data"] = $data;

	$levelup_apply_id = $data[0]["levelup_apply_id"];

	if($levelup_apply_id != ""){
		//--------------------------------------------------------------------------
		// レベルアップ申請 研修受講データ取得
		//--------------------------------------------------------------------------
		$log->debug("研修受講データ取得読込み Start",__FILE__,__LINE__);
		$data=$cl_levelup_recognize_apply_model->get_levelup_apply_training_list($levelup_apply_id);
		$log->debug("研修受講データ取得読込み End",__FILE__,__LINE__);

		$result_data["training_data"] = $data;

		//--------------------------------------------------------------------------
		// レベルアップ申請 評価データ取得
		//--------------------------------------------------------------------------
		$log->debug("評価データ読込み Start　　levelup_apply_id = ".$levelup_apply_id,__FILE__,__LINE__);
		$evaluation_data= $cl_levelup_recognize_apply_model->get_evaluation_data_list($levelup_apply_id);
		$log->debug("評価データ読込み End   count(evaluation_data) = ".count($evaluation_data),__FILE__,__LINE__);

		$result_data["evaluation_data"] = $evaluation_data;

		//--------------------------------------------------------------------------
		// レベルアップ申請 合格条件情報取得
		//--------------------------------------------------------------------------
		$log->debug("合格条件データ読込み Start　　levelup_apply_id = ".$levelup_apply_id,__FILE__,__LINE__);
		$get_pass_conditions= $cl_levelup_recognize_apply_model->get_pass_conditions($levelup_apply_id);
		$log->debug("合格条件データ読込み End   count(evaluation_data) = ".count($evaluation_data),__FILE__,__LINE__);

		$result_data["get_pass_conditions"] = $get_pass_conditions;
	}else{
		//--------------------------------------------------------------------------
		// 評価マスタ 評価マスタデータ取得
		//--------------------------------------------------------------------------
		$log->debug("評価マスタデータ取得読込み Start",__FILE__,__LINE__);
		$data=$cl_levelup_recognize_apply_model->get_mst_evaluation_category_list($target_level);
		$log->debug("評価マスタデータ取得読込み End",__FILE__,__LINE__);

		$result_data["evaluation_mst_list"] = $data;
	}

	$log->debug("レベルアップ申請データ読込み END",__FILE__,__LINE__);

	$json=create_json($result_data);

	$log->debug(__FUNCTION__." END");

	return $json;
}

