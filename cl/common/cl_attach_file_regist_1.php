<?
require_once(dirname(__FILE__) . "/../../cl_common.ini");
require_once(dirname(__FILE__) . "/../../cl_title_name.ini");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
require_once("cl_smarty_setting.ini");

//ロガーをロード
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

$smarty = new Smarty();
$smarty->template_dir = dirname(__FILE__) . "/../view";
$smarty->compile_dir  = dirname(__FILE__) . "/../../templates_c";

$fname = $PHP_SELF;

// 2012/09/24 Yamagawa upd(s)
/*
if ($_POST['file_kbn'] == '1') {
	$title = '添付ファイル（個人目標管理表）';
	$target_parent_div = 'clp_attach_Kojin';
} else if($_POST['file_kbn'] == '2') {
	$title = '添付ファイル（エピソード）';
	$target_parent_div = 'clp_attach_Episode';
}
*/
switch ($_POST['file_kbn']) {
	case '1':
		$title = '添付ファイル（個人目標管理表１）';
		$target_parent_div = 'clp_attach_Kojin';
		break;
	case '2':
		$title = '添付ファイル（エピソード）';
		$target_parent_div = 'clp_attach_Episode';
		break;
	case '3':
		$title = '添付ファイル（個人目標管理表２）';
		$target_parent_div = 'clp_attach_Kojin2';
		break;
	default:
		break;
}
// 2012/09/24 Yamagawa upd(e)
$session = $_POST['session'];
$common_module = $_POST['common_module'];
$file_kbn = $_POST['file_kbn'];
$upload_max_filesize = ini_get("upload_max_filesize");
$log->debug("file_kbn：".$_POST['file_kbn']);

// ファイルアップロードチェック
define(UPLOAD_ERR_OK, 0);
define(UPLOAD_ERR_INI_SIZE, 1);
define(UPLOAD_ERR_FORM_SIZE, 2);
define(UPLOAD_ERR_PARTIAL, 3);
define(UPLOAD_ERR_NO_FILE, 4);



// テンプレートマッピング
$smarty->assign( 'title'				, $title				);
$smarty->assign( 'cl_title'				, $cl_title				);
$smarty->assign( 'session'				, $session				);
$smarty->assign( 'upload_max_filesize'	, $upload_max_filesize	);
$smarty->assign( 'common_module'		, $common_module		);
$smarty->assign( 'file_kbn'				, $file_kbn				);

// テンプレート出力
$smarty->display(basename(__FILE__,'.php').".tpl");

if ($_POST['upload_flg'] == 'true') {

	$filename = $_FILES["file"]["name"];
	if ($filename == "") {
		echo("<script language=\"javascript\">alert('ファイルを選択してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}

	switch ($_FILES["file"]["error"]) {
		case UPLOAD_ERR_INI_SIZE:
		case UPLOAD_ERR_FORM_SIZE:
			echo("<script language=\"javascript\">alert('ファイルサイズが大きすぎます。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		case UPLOAD_ERR_PARTIAL:
		case UPLOAD_ERR_NO_FILE:
			echo("<script language=\"javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
	}

	// ファイル保存用ディレクトリがなければ作成
	if (!is_dir("cl/attach")) {
		mkdir("cl/attach", 0755);
	}
	if (!is_dir("cl/attach/tmp")) {
		mkdir("cl/attach/tmp", 0755);
	}

	// 30分以上前に保存されたファイルを削除
	foreach (glob("cl/attach/tmp/*.*") as $tmpfile) {
		if (time() - filemtime($tmpfile) >= 30 * 60) {
			unlink($tmpfile);
		}
	}

	// 添付ファイルIDの最大値を取得
	$max_id = 0;
	foreach (glob("cl/attach/tmp/{$session}_*.*") as $tmpfile) {
		preg_match("/{$session}_(\d*)\./", $tmpfile, $matches);
		$tmp_id = $matches[1];
		if ($tmp_id > $max_id) {
			$max_id = $tmp_id;
		}
	}
	$file_id = $max_id + 1;

	// アップロードされたファイルを保存
	$ext = strrchr($filename, ".");
	copy($_FILES["file"]["tmp_name"], "cl/attach/tmp/{$session}_{$file_id}{$ext}");
?>
	<script type="text/javascript">
		// ファイルリンク
		var fname = '<? echo($filename); ?>';

		var a = opener.document.createElement('a');
		a.href = 'cl/attach/tmp/<? echo("{$session}_{$file_id}{$ext}"); ?>';
		a.target = '_blank';
		a.appendChild(opener.document.createTextNode(fname));

		// 削除ボタン
		// 2012/06/12 Yamagawa del(s)
		//var inputA = opener.document.createElement('input');
		//inputA.type = 'button';
		//inputA.name = 'btn_<? echo($file_id); ?>';
		//inputA.value = '削除';
		//inputA.id = 'btn_<? echo($file_id); ?>';
		//if (inputA.onclick === null) {  // IE, Safari
		//	inputA.setAttribute('onclick', 'clp_detachFile("<?=$file_id?>");');
		//}
		//if (typeof inputA.onclick == 'string') {  // IE
		//	inputA.setAttribute('clp_detachFile("<?=$file_id?>")', null);
		//}
		//if (inputA.onclick == null) {  // IE, FireFox
		//	inputA.onclick = opener.clp_detachFile("<?=$file_id?>");
		//}
		// 2012/06/12 Yamagawa del(e)

		// ファイル名
		var inputB = opener.document.createElement('input');
		inputB.type = 'hidden';
		inputB.name = 'clp_file_name[]';
		inputB.value = fname;

		// ファイルＩＤ
		var inputC = opener.document.createElement('input');
		inputC.type = 'hidden';
		inputC.name = 'clp_file_id[]';
		inputC.value = '<? echo($file_id); ?>';

		// ファイル区分
		var inputD = opener.document.createElement('input');
		inputD.type = 'hidden';
		inputD.name = 'clp_file_division[]';
		inputD.value = '<? echo($file_kbn); ?>';

		// ファイルパス
		var inputE = opener.document.createElement('input');
		inputE.type = 'hidden';
		inputE.name = 'clp_file_path[]';
		inputE.value = 'cl/attach/tmp/<? echo("{$session}_{$file_id}{$ext}"); ?>';

		// 呼び出し元に反映
		// 2012/06/12 Yamagawa del(s)
		//var p = opener.document.createElement('p');
		//p.id = 'p_<? echo($file_id); ?>';
		//p.appendChild(a);
		//p.appendChild(opener.document.createTextNode(' '));
		//p.appendChild(inputA);
		//p.appendChild(inputB);
		//p.appendChild(inputC);
		//p.appendChild(inputD);
		//p.appendChild(inputE);
		// 2012/06/12 Yamagawa del(e)

		var div = opener.document.getElementById('<?=$target_parent_div?>');
		// 2012/06/12 Yamagawa upd(s)
		//div.appendChild(p);
		div.appendChild(a);
		div.appendChild(inputB);
		div.appendChild(inputC);
		div.appendChild(inputD);
		div.appendChild(inputE);
		// 2012/06/12 Yamagawa upd(e)

		//2012/05/16 K.Fujii ins(s)
		// 2012/06/12 Yamagawa del(s)
		//var file_kbn = '<?=$file_kbn?>';
		//if (file_kbn == '1') {
		//	opener.document.getElementById('hdn_attach_Kojin').value = p.innerHTML;
		//} else if(file_kbn == '2') {
		//	opener.document.getElementById('hdn_attach_Episode').value = p.innerHTML;
		//}
		// 2012/06/12 Yamagawa del(e)
		//2012/05/16 K.Fujii ins(e)

		// 2012/06/12 Yamagawa upd(s)
		//close_attach();
		close_attach('<?=$file_kbn?>');
		// 2012/06/12 Yamagawa upd(e)

	</script>
<?
}

