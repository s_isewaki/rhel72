<?php
//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");
require_once('MDB2.php');
require_once(dirname(__FILE__) . "/../model/template/cl_apl_inside_training_theme_present_model.php");

$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

$apply_id=$_GET["apply_id"];

$log->debug('apply_id:'.$apply_id,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 申請書データ取得処理を呼び出す
//------------------------------------------------------------------------------
$json=getJsonApplyData($mdb2, $apply_id);
//$json= ereg_replace("\r|\n","\\\\n",$json);


$log->debug('★★★★★★$json:'.$json,__FILE__,__LINE__);


//------------------------------------------------------------------------------
// データベース接続を閉じる
//------------------------------------------------------------------------------
$log->debug("データベース切断 START",__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);

header("Content-Type: application/json; charset=EUC-JP");
echo $json;

/**
 * 申請書データ取得
 */
function getJsonApplyData($mdb2, $apply_id){
	global $log;
	$log->debug(__FUNCTION__." START");

	//--------------------------------------------------------------------------
	// 院内研修課題提出テーブルモデルインスタンス生成
	//--------------------------------------------------------------------------
	$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
	$model = new cl_apl_inside_training_theme_present_model($mdb2, $user);
	$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

	//--------------------------------------------------------------------------
	// 院内研修課題提出テーブルデータ取得
	//--------------------------------------------------------------------------
	$log->debug("getRecJson開始",__FILE__,__LINE__);
	$data=$model->select($apply_id);
	$log->debug("getRecJson終了",__FILE__,__LINE__);

$data["present_answer"]  = str_replace("\r\n",   "\\n",$data["present_answer"]);		// 改行
$data["boss_comment"]    = str_replace("\r\n",   "\\n",$data["boss_comment"]);			// 改行
$data["sponsor_comment"] = str_replace("\r\n",   "\\n",$data["sponsor_comment"]);		// 改行
$data["teacher_comment"] = str_replace("\r\n",   "\\n",$data["teacher_comment"]);		// 改行

/*
 * "SELECT theme_apply_id,
 emp_id,
 apply_id,
 training_apply_id,
 training_id,
 theme_id,
 present_date,
 present_answer,
 boss_comment,
 sponsor_comment,
 teacher_comment,
 boss_id,
 sponsor_id,
 teacher_id,
 delete_flg,
 create_date,
 create_user,
 update_date,
 update_user FROM cl_apl_inside_training_theme_present WHERE apply_id = :apply_id";
 */
//$data["present_answer"	] = ereg_replace("\r|\n","<br/>",$data["present_answer"	]);
//$data["boss_comment"	] = ereg_replace("\r|\n","<br/>",$data["boss_comment"	]);
//$data["sponsor_comment"	] = ereg_replace("\r|\n","<br/>",$data["sponsor_comment"	]);
//$data["teacher_comment"	] = ereg_replace("\r|\n","<br/>",$data["teacher_comment"	]);

//$log->debug('■■■$data["present_answer"	]:'.$data["present_answer"	],__FILE__,__LINE__);
//$log->debug('■■■$data["boss_comment"	]:'.$data["boss_comment"	],__FILE__,__LINE__);
//$log->debug('■■■$data["sponsor_comment"	]:'.$data["sponsor_comment"	],__FILE__,__LINE__);
//$log->debug('■■■$data["teacher_comment"	]:'.$data["teacher_comment"	],__FILE__,__LINE__);

	//--------------------------------------------------------------------------
	// 院内研修マスタデータ取得
	//--------------------------------------------------------------------------
	$log->debug("研修マスタ Start　　training_id = ".$data['training_id'] ,__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_model.php");
	$log->debug("研修マスタ require End",__FILE__,__LINE__);

	$cl_mst_inside_training_model = new cl_mst_inside_training_model($mdb2, $user);
	$log->debug("研修マスタ インスタンス OK",__FILE__,__LINE__);

	$log->debug("研修マスタデータ読込み Start　　training_id = ".$data['training_id'],__FILE__,__LINE__);
	$inside_training_data= $cl_mst_inside_training_model->select($data['training_id']);

$inside_training_data["training_purpose"]  = str_replace("\r\n",   "\\n",$inside_training_data["training_purpose"]);		// 改行
$inside_training_data["training_slogan"]   = str_replace("\r\n",   "\\n",$inside_training_data["training_slogan"]);		// 改行
$inside_training_data["training_contents"] = str_replace("\r\n",   "\\n",$inside_training_data["training_contents"]);		// 改行
$inside_training_data["training_action"]   = str_replace("\r\n",   "\\n",$inside_training_data["training_action"]);		// 改行
$inside_training_data["training_support"]  = str_replace("\r\n",   "\\n",$inside_training_data["training_support"]);		// 改行

//2012/05/11 K.Fujii ins(s)
$inside_training_data["training_require"] = str_replace("\r\n",   "\\n",$inside_training_data["training_require"]);			// 改行
$inside_training_data["change_reason"]     = str_replace("\r\n",   "\\n",$inside_training_data["change_reason"]);			// 改行
//2012/05/11 K.Fujii ins(e)

/*
 * "SELECT training_id,
 training_name,
 target_level1,
 target_level2,
 target_level3,
 target_level4,
 target_level5,
 training_require,
 time_division,
 training_opener,
 training_teacher1,
 training_teacher2,
 training_teacher3,
 training_teacher4,
 training_teacher5,
 training_purpose,
 training_slogan,
 training_contents,
 training_action,
 training_support,
 abolition_flag,
 delete_flg,
 create_date,
 create_user,
 update_date,
 update_user FROM cl_mst_inside_training WHERE training_id = :training_id";

 */

/*

$inside_training_data["training_contents"	] = ereg_replace("\r|\n","<br>",$inside_training_data["training_contents"	]);
$inside_training_data["training_slogan"		] = ereg_replace("\r|\n","<br>",$inside_training_data["training_slogan"		]);
$inside_training_data["training_purpose"	] = ereg_replace("\r|\n","<br>",$inside_training_data["training_purpose"	]);
$inside_training_data["training_action"		] = ereg_replace("\r|\n","<br>",$inside_training_data["training_action"		]);
$inside_training_data["training_support"	] = ereg_replace("\r|\n","<br>",$inside_training_data["training_support"	]);

$log->debug('■■■$inside_training_data["training_contents"	]:'.$inside_training_data["training_contents"	],__FILE__,__LINE__);
$log->debug('■■■$inside_training_data["training_slogan"	]:'.$inside_training_data["training_slogan"	],__FILE__,__LINE__);
$log->debug('■■■$inside_training_data["training_purpose"	]:'.$inside_training_data["training_purpose"	],__FILE__,__LINE__);
$log->debug('■■■$inside_training_data["training_action"	]:'.$inside_training_data["training_action"	],__FILE__,__LINE__);
$log->debug('■■■$inside_training_data["training_support"	]:'.$inside_training_data["training_support"	],__FILE__,__LINE__);
*/

$log->debug("研修マスタデータ読込み End   training_id = ". $data['training_id'],__FILE__,__LINE__);

	//--------------------------------------------------------------------------
	// 申請済み研修テーブルデータ取得　(申請済み研修　に対する　日程　を取得するため)
	//--------------------------------------------------------------------------
	$log->debug("申請済み研修 modelインスタンス作成開始",__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/ladder/cl_applied_inside_training_model.php");
	$log->debug("申請済み研修 require End",__FILE__,__LINE__);

	$cl_applied_inside_training_model = new cl_applied_inside_training_model($mdb2, $user);
	$log->debug("申請済み研修 インスタンス OK",__FILE__,__LINE__);

	$arr_applied_training = $cl_applied_inside_training_model->select($data['training_apply_id']);
	$log->debug("arr_applied_training count = ". count($arr_plan),__FILE__,__LINE__);


	//--------------------------------------------------------------------------
	// 院内研修日程テーブルデータ取得
	//--------------------------------------------------------------------------
	$log->debug("schedule modelインスタンス作成開始",__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_schedule_model.php");
	$log->debug("研修日程 require End",__FILE__,__LINE__);

	$cl_mst_inside_training_schedule_model = new cl_mst_inside_training_schedule_model($mdb2, $user);
	$log->debug("研修日程 インスタンス OK",__FILE__,__LINE__);

	$arr_plan = $cl_mst_inside_training_schedule_model->getList($arr_applied_training['plan_id']);
	$log->debug("plan count = ". count($arr_plan),__FILE__,__LINE__);
	// 2012/04/12 Yamagawa add(s)
	// 「23:59」は「24:00」に変換（暫定対応）
	for($i = 0; $i < count($arr_plan); $i++) {
		if (substr($arr_plan[$i]['from_time'],0,5) == "23:59") {
			$arr_plan[$i]['from_time'] = "24:00";
		}
		if (substr($arr_plan[$i]['to_time'],0,5) == "23:59") {
			$arr_plan[$i]['to_time'] = "24:00";
		}
	}
	// 2012/04/12 Yamagawa add(e)


	//--------------------------------------------------------------------------
	// 院内研修課題マスタデータ取得
	//--------------------------------------------------------------------------
	$log->debug("schedule modelインスタンス作成開始",__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_theme_model.php");
	$log->debug("課題マスタ require End",__FILE__,__LINE__);

	$cl_mst_inside_training_theme_model = new cl_mst_inside_training_theme_model($mdb2, $user);
	$log->debug("課題マスタ インスタンス OK",__FILE__,__LINE__);


	$arr_theme = $cl_mst_inside_training_theme_model->select($data['theme_id']);

$arr_theme["theme_contents"] = str_replace("\r\n",   "\\n",$arr_theme["theme_contents"]);		// 改行
$arr_theme["theme_method"]   = str_replace("\r\n",   "\\n",$arr_theme["theme_method"]);			// 改行
$arr_theme["theme_deadline"] = str_replace("\r\n",   "\\n",$arr_theme["theme_deadline"]);		// 改行
/*
 * "SELECT theme_id,
 training_id,
 theme_division,
 theme_contents,
 theme_method,
 theme_deadline,
 theme_present,
 delete_flg,
 create_date,
 create_user,
 update_date,
 update_user FROM cl_mst_inside_training_theme WHERE theme_id = :theme_id";

theme_contents	theme_method

 */
//$arr_theme["theme_contents"	] = ereg_replace("\r|\n","<br>",$arr_theme["theme_contents"	]);
//$arr_theme["theme_method"	] = ereg_replace("\r|\n","<br>",$arr_theme["theme_method"	]);

$log->debug('■■■$arr_theme["theme_contents"	]:'.$arr_theme["theme_contents"	],__FILE__,__LINE__);
$log->debug('■■■$arr_theme["theme_method"	]:'.$arr_theme["theme_method"	],__FILE__,__LINE__);


	$log->debug("theme count = ". count($arr_theme),__FILE__,__LINE__);

	//--------------------------------------------------------------------------
	// 承認情報取得
	//--------------------------------------------------------------------------

	$log->debug("apply_id *= ". $data['apply_id'],__FILE__,__LINE__);
	$apv_data = $model->apv_data_select_rec($data['apply_id']);
	$log->debug("$apv_data = ". $apv_data,__FILE__,__LINE__);

for($i=0; $i<count($apv_data); $i++){
	$apv_data[$i]["present_answer"]  = str_replace("\r\n",   "\\n",$apv_data[$i]["present_answer"]);		// 改行
	$apv_data[$i]["boss_comment"]    = str_replace("\r\n",   "\\n",$apv_data[$i]["boss_comment"]);			// 改行
	$apv_data[$i]["sponsor_comment"] = str_replace("\r\n",   "\\n",$apv_data[$i]["sponsor_comment"]);		// 改行
	$apv_data[$i]["teacher_comment"] = str_replace("\r\n",   "\\n",$apv_data[$i]["teacher_comment"]);		// 改行
}
/*
 *  "SELECT theme_present.*,
 apv.apply_id,
 apv.apv_order,
 apv.emp_id FROM cl_apl_inside_training_theme_present AS theme_present LEFT JOIN cl_applyapv AS apv ON theme_present.apply_id = apv.apply_id WHERE apv.apply_id = :apply_id AND theme_present.delete_flg=0 ORDER BY theme_present.apply_id";

 */

	/*
	for($k=0;$k<count($apv_data);$k++){

		foreach( $apv_data[$k] as $key => $value ) {
			$log->debug('★★★★★★$apv_data['.$k.']['.$key.']:'.$apv_data[$k][$key],__FILE__,__LINE__);
			//$apv_data[$k]["present_answer"	] = ereg_replace("\r|\n","<br>",$apv_data[$k]["present_answer"	]);
			//$apv_data[$k]["theme_present"	] = ereg_replace("\r|\n","<br>",$apv_data[$k]["theme_present"	]);
			//$apv_data[$k]["boss_comment"	] = ereg_replace("\r|\n","<br>",$apv_data[$k]["boss_comment"	]);
			//$apv_data[$k]["sponsor_comment"	] = ereg_replace("\r|\n","<br>",$apv_data[$k]["sponsor_comment"	]);
			//$apv_data[$k]["teacher_comment"	] = ereg_replace("\r|\n","<br>",$apv_data[$k]["teacher_comment"	]);

			$apv_data[$k]["present_answer"	] = ereg_replace("\r|\n","\\\\n",$apv_data[$k]["present_answer"	]);
			$apv_data[$k]["theme_present"	] = ereg_replace("\r|\n","\\\\n",$apv_data[$k]["theme_present"	]);
			$apv_data[$k]["boss_comment"	] = ereg_replace("\r|\n","\\\\n",$apv_data[$k]["boss_comment"	]);
			$apv_data[$k]["sponsor_comment"	] = ereg_replace("\r|\n","\\\\n",$apv_data[$k]["sponsor_comment"	]);
			$apv_data[$k]["teacher_comment"	] = ereg_replace("\r|\n","\\\\n",$apv_data[$k]["teacher_comment"	]);

			$log->debug('■■■$apv_data['.$k.']["present_answer"	]:'.$apv_data[$k]["present_answer"	],__FILE__,__LINE__);
			$log->debug('■■■$apv_data['.$k.']["theme_present"	]:'.$apv_data[$k]["theme_present"	],__FILE__,__LINE__);
			$log->debug('■■■$apv_data['.$k.']["boss_comment"		]:'.$apv_data[$k]["boss_comment"	],__FILE__,__LINE__);
			$log->debug('■■■$apv_data['.$k.']["sponsor_comment"	]:'.$apv_data[$k]["sponsor_comment"	],__FILE__,__LINE__);
			$log->debug('■■■$apv_data['.$k.']["teacher_comment"	]:'.$apv_data[$k]["teacher_comment"	],__FILE__,__LINE__);
		}
	}
	*/


//--------------------------------------------------------------------------
	// 院内研修課題テーブルデータセット
	//--------------------------------------------------------------------------
	/*
	for($k=0;$k<count($inside_training_data);$k++){

		foreach( $inside_training_data[$k] as $key => $value ) {
			$log->debug('★★★★★★$inside_training_data['.$k.']['.$key.']:'.$inside_training_data[$k][$key],__FILE__,__LINE__);
		}
	}
	for($k=0;$k<count($arr_plan);$k++){

		foreach( $arr_plan[$k] as $key => $value ) {
			$log->debug('★★★★★★$arr_plan['.$k.']['.$key.']:'.$arr_plan[$k][$key],__FILE__,__LINE__);
		}
	}
	for($k=0;$k<count($arr_theme);$k++){

		foreach( $arr_theme[$k] as $key => $value ) {
			$log->debug('★★★★★★$arr_theme['.$k.']['.$key.']:'.$arr_theme[$k][$key],__FILE__,__LINE__);
		}
	}
	for($k=0;$k<count($apv_data);$k++){

		foreach( $apv_data[$k] as $key => $value ) {
			$log->debug('★★★★★★$apv_data['.$k.']['.$key.']:'.$apv_data[$k][$key],__FILE__,__LINE__);
		}
	}
	*/
	$data["training_list"] = $inside_training_data;
	$data["schedule_list"] = $arr_plan;
	$data["theme_list"]    = $arr_theme;
	$data["apv_data"] = $apv_data;

	//$json=urlencode(create_json($data));
	$json=create_json($data);


	$log->debug(__FUNCTION__." END");


	return $json;
}
