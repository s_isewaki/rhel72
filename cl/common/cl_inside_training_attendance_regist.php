<?php

	//##########################################################################
	// 各種モジュールの読み込み
	//##########################################################################
	require_once(dirname(__FILE__) . "/../../about_session.php");
	require_once(dirname(__FILE__) . "/../../about_authority.php");
	require_once(dirname(__FILE__) . "/../../about_postgres.php");
	require_once(dirname(__FILE__) . "/../../show_select_values.ini");
	require_once(dirname(__FILE__) . "/../../cl_application_list.inc");
	require_once(dirname(__FILE__) . "/../../cl_application_common.ini");
	require_once(dirname(__FILE__) . "/../../cl_common.ini");
	require_once(dirname(__FILE__) . "/../../cl_title_name.ini");
	require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
	require_once(dirname(__FILE__) . "/../../Cmx.php");
	require_once("Smarty/Smarty.class.php");
	require_once("MDB2.php");

	//##########################################################################
	// 初期処理
	//##########################################################################

	//ロガーをロード
	$log = new cl_common_log_class(basename(__FILE__));
	$log->info(basename(__FILE__)." START");

	//画面名
	$fname = $PHP_SELF;

	//##########################################################################
	// パラメータ取得
	//##########################################################################
	foreach( $_REQUEST as $key => $value ) {
		$log->debug('$_REQUEST['.$key.']:'.$_REQUEST[$key],__FILE__,__LINE__);
	}
	$p_session		= $_POST["p_session"];
	$plan_id		= $_POST["plan_id"];
	$training_id	= $_POST["training_id"];
	$login_user		= $_POST["login_user"];

	//$_POST :: $key:plan_id $val:CLITS00000009
	//$_POST :: $key:session $val:21e6f8b67320018d487a90b29c802263
	//$_POST :: $key:training_id $val:CLTRA00000009

	$log->debug('■plan_id   :'.$plan_id	,__FILE__,__LINE__);
	$log->debug('■session   :'.$session	,__FILE__,__LINE__);
	$log->debug('■training_id   :'.$training_id	,__FILE__,__LINE__);

	$log->debug('■fname   :'.$fname	,__FILE__,__LINE__);

	//====================================
	//セッションのチェック
	//====================================
	$session = qualify_session($session,$fname);
	//if($session == "0"){
	//	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	//	echo("<script language='javascript'>showLoginPage(window);</script>");
	//	exit;
	//}

	$log->debug('■session2   :'.$session	,__FILE__,__LINE__);

	//====================================
	//権限チェック
	//====================================
	$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
	if($wkfw=="0"){
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showLoginPage(window);</script>");
		exit;
	}

	// ワークフロー権限の取得
	$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

	$log->debug('■$workflow_auth   :'.$workflow_auth	,__FILE__,__LINE__);

	//##########################################################################
	// データベース接続
	//##########################################################################
	$mdb2 = MDB2::connect(CMX_DB_DSN);

	// セッションよりログインユーザー取得
	require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php");
	$empmst_model = new empmst_model($mdb2,$emp_id);
	$arr_empmst = $empmst_model->select_login_emp($session);

	$login_user = $arr_empmst["emp_id"];
	$emp_id = $login_user;
	// ヘッダーデータ取得
	require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_model.php");
	$cl_mst_inside_training_model = new cl_mst_inside_training_model($mdb2,$emp_id);
	$arr_training = $cl_mst_inside_training_model->select($training_id);

	require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_schedule_model.php");
	$cl_mst_inside_training_schedule_model = new cl_mst_inside_training_schedule_model($mdb2,$emp_id);
	$arr_schedule = $cl_mst_inside_training_schedule_model->select($plan_id);
	// 2012/04/12 Yamagawa add(s)
	// 「23:59」は「24:00」に変換（暫定対応）
	if (substr($arr_schedule['from_time'],0,5) == "23:59") {
		$arr_schedule['from_time'] = "24:00";
	}
	if (substr($arr_schedule['to_time'],0,5) == "23:59") {
		$arr_schedule['to_time'] = "24:00";
	}
	// 2012/04/12 Yamagawa add(e)

	// 年月日分解
	$arr_date = explode("-",$arr_schedule['plan_date']);
	$plan_date_year = $arr_date[0];
	$plan_date_month = $arr_date[1];
	$plan_date_day = $arr_date[2];


	// 一覧データ取得
//	require_once(dirname(__FILE__) . "/../model/workflow/cl_apl_inside_training_apply_model.php");
//	$cl_apl_inside_training_apply_model = new cl_apl_inside_training_apply_model($mdb2, $emp_id);
//	$param = array(
//		"inside_training_id"	=> $training_id,
//		"plan_id"				=> $plan_id
//	);
//	$data = $cl_apl_inside_training_apply_model->getReceptionList($param);

	// 一覧データ取得
	require_once(dirname(__FILE__) . "/../model/workflow/cl_inside_training_roll_model.php");
	$cl_inside_training_roll_model = new cl_inside_training_roll_model($mdb2, $emp_id);

	$param = array(
		"training_id"	=> $training_id,
		"plan_id"				=> $plan_id
	);

	$upd_data = $cl_inside_training_roll_model->getReceptionList($param);

	$koushin_flg = "f";
	// インポート処理
	if ($_POST['import_flg'] == 'true'){
		$log->debug('■インポートモード開始　count($fileData)：'.count($fileData)	,__FILE__,__LINE__);
		if (is_uploaded_file($_FILES["inputfile"]["tmp_name"])) {
			$fd = @fopen($_FILES['inputfile']['tmp_name'], "r");
			if ($fd) {
				$row_idx = 0;
				while (!feof($fd)) {
					$row_idx += 1;
					// 2012/08/31 Yamagawa del(s)
					/*
					if($row_idx == 1){
						continue;
					}
					*/
					// 2012/08/31 Yamagawa del(e)
					// 2012/08/21 Yamagawa upd(s)
					//$buffer = fgets($fd);
					$buffer = preg_replace(array('/\r\n/','/\r/','/\n/'), '', mb_convert_encoding(fgets($fd),"eucJP-win","sjis-win"));
					// 2012/08/21 Yamagawa upd(e)
					$tmp = preg_split('/,/', $buffer);
					// 2012/08/31 Yamagawa upd(s)
					/*
					//2012/05/09 K.Fujii upd(s)
					//if(count($tmp) == 6){
					//	if($tmp[5] < 3){
					//		$fileData[] = array("emp_id"=>$tmp[1], "attendance"=>$tmp[5]);
					if(count($tmp) == 5){
						if($tmp[4] < 3){
							$fileData[] = array("emp_id"=>$tmp[1], "attendance"=>$tmp[4]);
					//2012/05/09 K.Fujii upd(e)
			    		}
			    	}
			    	*/
					if (strlen($tmp[0]) > 0) {
						$fileData[] = array("emp_id"=>$tmp[0], "attendance"=>"1");
					}
					// 2012/08/31 Yamagawa upd(e)
			    }
			    if (!feof($fd)) {
			        echo "Error: unexpected fgets() fail\n";
			    }
			}
			fclose($fd);
		}
		if(count($upd_data)>0){
			// 取得したデータでループ
			$rowcnt = count($upd_data);
			for ($index = 0; $index < count($upd_data); $index++){
				for($i = 0;$i<count($fileData);$i++){
					if($upd_data[$index]["emp_id"] == $fileData[$i]["emp_id"]){
						$log->debug('■出欠の変更'	,__FILE__,__LINE__);
						// 出欠の変更
						$log->debug('■更新前：'.$upd_data[$index]["emp_roll"]	,__FILE__,__LINE__);
						// 2012/08/21 Yamagawa upd(s)
						//$upd_data[$index]["emp_roll"] = $fileData[$i]["attendance"];
						// 元ファイルの表示と合わせる
						$upd_data[$index]["emp_roll"] = (is_numeric($fileData[$i]["attendance"])) ? $fileData[$i]["attendance"] : ($fileData[$i]["attendance"] === "出席") ? "1" : "0";
						// 2012/08/21 Yamagawa upd(e)
						$log->debug('■更新後：'.$upd_data[$index]["emp_roll"]	,__FILE__,__LINE__);
					}
				}
			}
		}
		//更新処理
		if(count($upd_data)>0){
			$koushin_flg = "t";
		}
		$log->debug('■インポートモード終了'	,__FILE__,__LINE__);
	}

	// 更新ボタン押下処理
	if ($_POST['update_flg'] == 'true'){
		$log->debug('■更新モード開始'	,__FILE__,__LINE__);
		$roll_data = $_POST['roll_data'];
		$ary_roll = preg_split('/,/', $roll_data);

		if(count($ary_roll) == count($upd_data)){
			if(count($upd_data)>0){
				// 取得したデータでループ
				for ($index = 0; $index < count($upd_data); $index++){
						if($upd_data[$index]["emp_roll"] != 2){
							// 出欠の変更
							$upd_data[$index]["emp_roll"] = $ary_roll[$index];
						}
				}
			}
			//更新処理
			if(count($upd_data)>0){
				$koushin_flg = "t";
			}
		}
		$log->debug('■更新モード終了'	,__FILE__,__LINE__);
	}

	//データ更新処理
	if($koushin_flg == "t"){
		$log->debug('■更新処理開始'	,__FILE__,__LINE__);
		// トランザクション開始
		$mdb2->beginTransaction();
		//データ件数ループ
		foreach ($upd_data as $idx => $row){
			//パラメータ作成
			$upd_param = array(
				"emp_roll"		=> (int)$row["emp_roll"],
				"update_user"	=> $login_user,
				"plan_id"		=> $plan_id,
				"training_id"	=> $training_id,
				"emp_id"		=> $row["emp_id"]
			);

			// 出欠テーブル更新
			$log->debug('出欠テーブル 更新 Start'	,__FILE__,__LINE__);
			$result = $cl_inside_training_roll_model->setAttendanse($upd_param);
			$log->debug('出欠テーブル 更新 End'	,__FILE__,__LINE__);

		}
		// コミット
		$mdb2->commit();
		$log->debug('■更新処理終了'	,__FILE__,__LINE__);
	}


//	// 一覧データ取得
//	$param = array(
//		"inside_training_id"	=> $training_id,
//		"plan_id"				=> $plan_id
//	);
//
//	$data = $cl_applied_inside_training_model->getReceptionList($plan_id);

	// 一覧データ取得
	$cl_inside_training_roll_model = new cl_inside_training_roll_model($mdb2, $emp_id);

	$param = array(
		"training_id"	=> $training_id,
		"plan_id"		=> $plan_id
	);

	$data = $cl_inside_training_roll_model->getReceptionList($param);

	// 取得したデータでループ
	for ($index = 0; $index < count($data); $index++){
		$data[$index]['no'] = $index + 1;

	//	// 行番号・出欠フラグを追加
	//	switch($data[$index]['emp_roll']){
	//		case 0:
	//			$data[$index]['emp_roll'] = '';
	//			break;
	//		case 1:
	//			$data[$index]['emp_roll'] = 'checked';
	//			break;
	//		case 2:
	//			$data[$index]['emp_roll'] = 'disabled';
	//			break;
	//		default:
	//			$data[$index]['emp_roll'] = '';
	//			break;
	//	}
	}

	if($data[0]["emp_id"] != ""){
		$data_count = count($data);
	}else{
		$data_count = 0;
	}

	//------------------------------------------------------------------------------
	// データベース接続を閉じる
	//------------------------------------------------------------------------------
	$log->debug("データベース切断 START",__FILE__,__LINE__);
	$mdb2->disconnect();
	$log->debug("データベース切断 END",__FILE__,__LINE__);

	//##########################################################################
	// 画面表示
	//##########################################################################
	$smarty = new Smarty();
	$smarty->template_dir = dirname(__FILE__) . "/../view";
	$smarty->compile_dir  = dirname(__FILE__) . "/../../templates_c";

	$smarty->assign( 'cl_title'			, $cl_title							);
	$smarty->assign( 'session'			, $session							);
	$smarty->assign( 'common_module'	, $common_module					);
	$smarty->assign( 'training_id'		, $training_id						);
	$smarty->assign( 'plan_id'			, $plan_id							);
	$smarty->assign( 'training_name'	, $arr_training['training_name']	);
	$smarty->assign( 'plan_date_year'	, $plan_date_year					);
	$smarty->assign( 'plan_date_month'	, $plan_date_month					);
	$smarty->assign( 'plan_date_day'	, $plan_date_day					);
	$smarty->assign( 'plan_time_from'	, substr($arr_schedule['from_time'], 0,5)	);
	$smarty->assign( 'plan_time_to'		, substr($arr_schedule['to_time'],   0,5)	);
	$smarty->assign( 'data'				, $data								);
	$smarty->assign( 'login_user'		, $emp_id							);
	$smarty->assign( 'data_count'		, $data_count							);

	//テンプレート出力
	$smarty->display(basename(__FILE__,'.php').".tpl");

	function format_column_for_csv($value, $from_encoding = "EUC-JP") {
		$buf = str_replace("\r", "", $value);
		$buf = str_replace("\n", "", $buf);
		if (strpos($buf, ",") !== false)  {
			$buf = '"' . $buf . '"';
		}
		return mb_convert_encoding($buf, "Shift_JIS", $from_encoding);
	}

	$log->info(basename(__FILE__)." END");
	$log->shutdown();


?>