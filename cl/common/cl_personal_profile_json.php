<?php
//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");
require_once('MDB2.php');
require_once(dirname(__FILE__) . "/../model/ladder/cl_personal_profile_model.php");

$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

$login_user_id=$_GET["login_user_id"];
$log->debug('login_user_id:'.$login_user_id,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// Profileデータ取得処理を呼び出す
//------------------------------------------------------------------------------
$json=getJsonProfileData($mdb2, $login_user_id);

$log->debug('$json:'.$json,__FILE__,__LINE__);


//------------------------------------------------------------------------------
// データベース接続を閉じる
//------------------------------------------------------------------------------
$log->debug("データベース切断 START",__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);

header("Content-Type: application/json; charset=EUC-JP");
echo $json;

/**
 * Profileデータ取得
 * @param        $mb2 DB接続情報
 * @param string $login_user_id　LoginユーザID
 */
function getJsonProfileData($mdb2, $login_user_id){
	global $log;
	$log->debug(__FUNCTION__." START");

	//--------------------------------------------------------------------------
	// Personal Profile テーブルモデルインスタンス生成
	//--------------------------------------------------------------------------
	$log->debug("profile modelインスタンス作成開始",__FILE__,__LINE__);
	$model = new cl_personal_profile_model($mdb2, $user);
	$log->debug("profile modelインスタンス作成終了",__FILE__,__LINE__);

	//--------------------------------------------------------------------------
	// Personal Profile テーブルデータ取得
	//--------------------------------------------------------------------------
	$log->debug("getRecJson開始  login_user_id = ". $login_user_id,__FILE__,__LINE__);
	$data=$model->select($login_user_id);
	$log->debug("getRecJson終了",__FILE__,__LINE__);

	if( $data['now_level'] == null || $data['now_level'] == "" ){
		$data['now_level']="0";
	}


	$json=create_json($data);

	$log->debug(__FUNCTION__." END");

	return $json;
}
