<?
	// 一時的にロケールを変更
	setlocale(LC_ALL,'ja_JP.eucJP');

	ini_set("display_errors","1");

	//////////////////////////////////////////////////////////////////////////////////////////
	//  モジュールのロード
	//////////////////////////////////////////////////////////////////////////////////////////
	require_once( dirname(__FILE__) . "/../../Cmx.php");
	require_once( dirname(__FILE__) . "/cl_log_class.inc");
	require_once( 'MDB2.php' );

	require_once( dirname(__FILE__) . "/../../fpdf153/mbfpdf.php" );
	require_once( dirname(__FILE__) . "/../model/workflow/empmst_model.php" );

	$log = new cl_common_log_class(basename(__FILE__));

	$log->info(basename(__FILE__)." START");

	//////////////////////////////////////////////////////////////////////////////////////////
	//  リクエストパラメータのロギング
	//////////////////////////////////////////////////////////////////////////////////////////
	foreach( $_REQUEST as $key => $value ) {
		$log->debug('$_REQUEST['.$key.']:'.$_REQUEST[$key],__FILE__,__LINE__);
	}

	$ladder_data = $_POST["emp_date"];
	$ladder_data_array =split('@',$ladder_data);

	for($i=0;$i<count($ladder_data_array);$i++){

		$ladder_update_array =split(',',$ladder_data_array[$i]);

		if ($ladder_update_array[0] != '') {
			$emp_ids[] = $ladder_update_array[0];
			$emp_names[] = $ladder_update_array[1];
			$levels[] = $ladder_update_array[3];
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//  各種定義
	//////////////////////////////////////////////////////////////////////////////////////////
	define( 'ERAMAP'	, '/var/www/html/comedix/cl/common/eramap.csv'						);
	define( 'PDFCONF'	, '/var/www/html/comedix/cl/common/authorization_doc.conf'			);
	define( 'CSVFILE'	, '/var/www/html/comedix/cl/common/authorization_doc_template.csv'	);
	define( 'DELIMITER'	, ','																);

	//////////////////////////////////////////////////////////////////////////////////////////
	//  元号マップ取得
	//////////////////////////////////////////////////////////////////////////////////////////
	$era_array = getEraMap( ERAMAP );

	//////////////////////////////////////////////////////////////////////////////////////////
	//  元号名マップ取得
	//////////////////////////////////////////////////////////////////////////////////////////
	$era_name_array = getEraNameMap( ERAMAP );

	//////////////////////////////////////////////////////////////////////////////////////////
	//  システム日付取得
	//////////////////////////////////////////////////////////////////////////////////////////
	$yyyy		= date('Y');
	$mm			= date('m');
	$dd			= date('d');
	$yy			= $era_array[$yyyy];
	$eraname	= $era_name_array[$yyyy];

	if( $mm < 10 ){
		$mm = str_replace('0', ' ', $mm);
	}
	if( $dd < 10 ){
		$dd = str_replace('0', ' ', $dd);
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	// PDF設定取得
	//////////////////////////////////////////////////////////////////////////////////////////
	$hash = parse_ini_file(PDFCONF);

	$orientation			= $hash["orientation"			];
	$unit					= $hash["unit"					];
	$format					= $hash["format"				];
	$cell_width				= $hash["cell_width"			];

	$bg_img_path			= $hash["bg_img_path"			];
	$bg_img_start_point_x	= $hash["bg_img_start_point_x"	];
	$bg_img_start_point_y	= $hash["bg_img_start_point_y"	];
	$bg_img_width			= $hash["bg_img_width"			];
	$bg_img_hight			= $hash["bg_img_hight"			];
	$bg_img_type			= $hash["bg_img_type"			];

	$cell_border			= $hash["cell_border"			];
	$cell_ln				= $hash["cell_ln"				];
	$cell_fill				= $hash["cell_fill"				];

	//////////////////////////////////////////////////////////////////////////////////////////
	// PDFインスタンス生成
	//////////////////////////////////////////////////////////////////////////////////////////
	$pdf=new MBFPDF( $orientation , $unit , $format );

	//////////////////////////////////////////////////////////////////////////////////////////
	// フォント設定
	//////////////////////////////////////////////////////////////////////////////////////////
	$pdf->SetAutoPageBreak(true, 5);
	$pdf->AddMBFont(MINCHO,'EUC-JP');
	$pdf->AddMBFont(PMINCHO,'EUC-JP');
	$pdf->AddMBFont(GOTHIC,'EUC-JP');

	//////////////////////////////////////////////////////////////////////////////////////////
	// データベース接続
	//////////////////////////////////////////////////////////////////////////////////////////
	$mdb2 = MDB2::connect(CMX_DB_DSN);

	//////////////////////////////////////////////////////////////////////////////////////////
	// ユーザ毎にページ作成
	//////////////////////////////////////////////////////////////////////////////////////////
	$empmst_model = new empmst_model( $mdb2 , $login_emp_id );
	for($i=0;$i<count($emp_ids);$i++){

		//////////////////////////////////////////////////////////////////////////////////////
		// ユーザ名取得
		//////////////////////////////////////////////////////////////////////////////////////
		$log->debug('ユーザ名取得 START('.$emp_ids[$i].')',__FILE__,__LINE__);
		$empmst_data = $empmst_model->select($emp_ids[$i]);

		$emp_lt_nm = $empmst_data['emp_lt_nm'];
		$emp_ft_nm = $empmst_data['emp_ft_nm'];

		$emp_name = $emp_lt_nm.' '.$emp_ft_nm;

		$log->debug('$emp_name:'.$emp_name,__FILE__,__LINE__);

		$level = $levels[$i];
		$log->debug('ユーザ名取得 END  ('.$emp_ids[$i].')',__FILE__,__LINE__);

		//////////////////////////////////////////////////////////////////////////////////////
		// ページ追加
		//////////////////////////////////////////////////////////////////////////////////////
		$pdf->AddPage();

		//////////////////////////////////////////////////////////////////////////////////////
		// 背景画像（認定書フレーム）追加
		//////////////////////////////////////////////////////////////////////////////////////
		// 2012/08/21 Yamagawa del(s)
		//$pdf->Image( $bg_img_path , $bg_img_start_point_x , $bg_img_start_point_y , $bg_img_width , $bg_img_hight , $bg_img_type ) ;
		// 2012/08/21 Yamagawa del(e)

		//////////////////////////////////////////////////////////////////////////////////////
		// 認定証テンプレートＣＳＶファイルを開く
		//////////////////////////////////////////////////////////////////////////////////////
		$fp = fopen(CSVFILE, 'r') or die('ファイルが開けません');

		$first_line_flg=true;
		while ($field_array = fgetcsv($fp, 4096, DELIMITER, ENCLOSURE)) {
			$log->debug("csvfile：".print_r($field_array,true));

			//////////////////////////////////////////////////////////////////////////////////
			// 先頭行はヘッダなのでスキップする。
			//////////////////////////////////////////////////////////////////////////////////
			if( $first_line_flg ){
				$first_line_flg=false;
				continue;
			}

			//////////////////////////////////////////////////////////////////////////////////
			// フォントサイズ、ボールド、セル幅、セル高、アライメント、テキストを取得
			//////////////////////////////////////////////////////////////////////////////////
			$font_size	= $field_array[0];
			$bold		= $field_array[1];
			$cell_width	= $field_array[2];
			$cell_hight	= $field_array[3];
			$align		= $field_array[4];
			$text		= $field_array[5];

			//////////////////////////////////////////////////////////////////////////////////
			// テキストの置き換えを行う。
			//////////////////////////////////////////////////////////////////////////////////
			$text = str_replace('{$yy}', $yy, $text);
			$text = str_replace('{$mm}', $mm, $text);
			$text = str_replace('{$dd}', $dd, $text);
			$text = str_replace('{$level}', $level, $text);
			$text = str_replace('{$emp_name}', $emp_name, $text);
			$text = str_replace('{$eraname}', $eraname, $text);

			if( $font_size <> '00' ){
				$pdf->SetFont( MINCHO ,$bold,$font_size);
			}
			$pdf->Cell( $cell_width , $cell_hight , $text , $cell_border , 1 , $align , 0 , null );
			$log->debug("text：".$text);

		}
		//ファイルを閉じる
		fclose($fp);
	}

	//////////////////////////////////////////////////////////////////////////////////
	// PDF出力
	//////////////////////////////////////////////////////////////////////////////////
	$pdf->Output();

	$log->info(basename(__FILE__)." END");
	$log->shutdown();

	/**
	 * 元号マップ
	 * Enter description here ...
	 * @param unknown_type $map
	 */
	function getEraMap( $map ){
		$fp = fopen(ERAMAP, 'r') or die('ファイルが開けません');

		$line=0;
		$era_array = array();
		$era_name_array = array();
		while ($field_array = fgetcsv($fp, 4096, DELIMITER, ENCLOSURE)) {
			$line=$line+1;
			if( $line == 1 ){
				continue;
			}
			$yyyy		= $field_array[0];
			$era		= $field_array[1];
			$eraname	= $field_array[2];
			$era_array[$yyyy]=$era;
			$era_name_array[$yyyy]=$eraname;

		}

		//ファイルを閉じる
		fclose($fp);
		return $era_array;
	}

	/**
	 * 元号名マップ
	 * Enter description here ...
	 * @param unknown_type $map
	 */
	function getEraNameMap( $map ){
		$fp = fopen(ERAMAP, 'r') or die('ファイルが開けません');

		$line=0;
		$era_array = array();
		$era_name_array = array();
		while ($field_array = fgetcsv($fp, 4096, DELIMITER, ENCLOSURE)) {
			$line=$line+1;
			if( $line == 1 ){
				continue;
			}
			$yyyy		= $field_array[0];
			$era		= $field_array[1];
			$eraname	= $field_array[2];
			$era_array[$yyyy]=$era;
			$era_name_array[$yyyy]=$eraname;

		}

		//ファイルを閉じる
		fclose($fp);
		return $era_name_array;
	}
