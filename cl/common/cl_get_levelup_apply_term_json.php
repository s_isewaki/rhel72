<?php
//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");
require_once('MDB2.php');
require_once(dirname(__FILE__) . "/../model/master/cl_mst_levelup_apply_term_model.php");

$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

$user=$_GET["user"];

//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// レベルアップ申請期間取得処理を呼び出す
//------------------------------------------------------------------------------
$json=getJsonTermData($mdb2,$user);

$log->debug('$json:'.$json,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベース接続を閉じる
//------------------------------------------------------------------------------
$log->debug("データベース切断 START",__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);

header("Content-Type: application/json; charset=EUC-JP");
echo $json;

/**
 * レベルアップ申請ID取得
 */
function getJsonTermData($mdb2, $user){
	global $log;
	$log->debug(__FUNCTION__." START");

	$month = date('n');
	if ($month >= 4 && $month <= 9) {
		$term_div = 1;
	} else {
		$term_div = 2;
	}

	//--------------------------------------------------------------------------
	// レベルアップ申請ID取得
	//--------------------------------------------------------------------------
	$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
	$levelup_apply_term_model = new cl_mst_levelup_apply_term_model($mdb2, $user);
	$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

	$log->debug("レベルアップ申請期間読込み Start",__FILE__,__LINE__);
	$data=$levelup_apply_term_model->getRecordByDiv($term_div);
	$log->debug("レベルアップ申請期間読込み End",__FILE__,__LINE__);

	$json=create_json($data);

	$log->debug(__FUNCTION__." END");

	return $json;
}


