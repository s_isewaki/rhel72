//----------------------------------------------------------------------------------------------
// 開催日修正
//----------------------------------------------------------------------------------------------
function ladder_level_update(ladderlevel){

	// 2012/07/13 Yamagawa del(s)
	/*
	var moduleName = "cl_ladder_authorize_employe";

	//リクエストパラメータをオブジェクトで作成
	var params = new Object();
	params.emp_id = document.ladder_form.emp_id.value;
	params.now_level = document.ladder_form.now_level.value;
	params.year = document.ladder_form.year.value;
	params.mode = document.ladder_form.mode.value;
	params.p_session = document.ladder_form.session.value;
	params.get_level_date_regist = 'true';

	if (ladderlevel == 1) {

		params.ladderlevel = 1;

		if ((document.ladder_form.auth_year_level1.value == "" ||
			document.ladder_form.auth_month_level1.value == "" ||
			document.ladder_form.auth_day_level1.value == "" ||
			document.ladder_form.auth_year_level1.value == "-" ||
			document.ladder_form.auth_month_level1.value == "-" ||
			document.ladder_form.auth_day_level1.value == "-") &&
			(document.ladder_form.certificate_level1_y.value != "" &&
			document.ladder_form.certificate_level1_m.value != "" &&
			document.ladder_form.certificate_level1_d.value != "" &&
			document.ladder_form.certificate_level1_y.value != "-" &&
			document.ladder_form.certificate_level1_m.value != "-" &&
			document.ladder_form.certificate_level1_d.value != "-"))
		{
			alert('認定証発行日を入れる場合は認定日を設定してください。');
			return false;
		}

		if (document.ladder_form.certificate_level1_y.value != "")
		{
			if (compareDate(
				document.ladder_form.certificate_level1_y.value,
				document.ladder_form.certificate_level1_m.value,
				document.ladder_form.certificate_level1_d.value,
				document.ladder_form.auth_year_level1.value,
				document.ladder_form.auth_month_level1.value,
				document.ladder_form.auth_day_level1.value
			) < 1) {
				alert('認定日が正しく選択されていません。');
				return false;
			}
		}

		params.level1_total_value = document.ladder_form.level1_total_value.value;

	 	params.auth_year_level1 = document.ladder_form.auth_year_level1.value;
	 	params.auth_month_level1 = document.ladder_form.auth_month_level1.value;
	 	params.auth_day_level1 = document.ladder_form.auth_day_level1.value;

	 	params.certificate_level1_y = document.ladder_form.certificate_level1_y.value;
	 	params.certificate_level1_m = document.ladder_form.certificate_level1_m.value;
	 	params.certificate_level1_d = document.ladder_form.certificate_level1_d.value;

	} else {
		params.level1_total_value = "";
		params.auth_year_level1 = "";
		params.auth_month_level1 = "";
		params.auth_day_level1 = "";
		params.certificate_level1_y = "";
		params.certificate_level1_m = "";
		params.certificate_level1_d = "";
	}

	if (ladderlevel == 2) {

		params.ladderlevel = 2;

		if ((document.ladder_form.auth_year_level2.value == "" ||
			document.ladder_form.auth_month_level2.value == "" ||
			document.ladder_form.auth_day_level2.value == "" ||
			document.ladder_form.auth_year_level2.value == "-" ||
			document.ladder_form.auth_month_level2.value == "-" ||
			document.ladder_form.auth_day_level2.value == "-") &&
			(document.ladder_form.certificate_level2_y.value != "" &&
			document.ladder_form.certificate_level2_m.value != "" &&
			document.ladder_form.certificate_level2_d.value != "" &&
			document.ladder_form.certificate_level2_y.value != "-" &&
			document.ladder_form.certificate_level2_m.value != "-" &&
			document.ladder_form.certificate_level2_d.value != "-"))
		{
			alert('認定証発行日を入れる場合は認定日を設定してください。');
			return false;
		}

		if (document.ladder_form.certificate_level2_y.value != "")
		{
			if (compareDate(
				document.ladder_form.certificate_level2_y.value,
				document.ladder_form.certificate_level2_m.value,
				document.ladder_form.certificate_level2_d.value,
				document.ladder_form.auth_year_level2.value,
				document.ladder_form.auth_month_level2.value,
				document.ladder_form.auth_day_level2.value
			) < 1) {
				alert('認定日が正しく選択されていません。');
				return false;
			}
		}

		params.level2_total_value = document.ladder_form.level2_total_value.value;

	 	params.auth_year_level2 = document.ladder_form.auth_year_level2.value;
	 	params.auth_month_level2 = document.ladder_form.auth_month_level2.value;
	 	params.auth_day_level2 = document.ladder_form.auth_day_level2.value;

	 	params.certificate_level2_y = document.ladder_form.certificate_level2_y.value;
	 	params.certificate_level2_m = document.ladder_form.certificate_level2_m.value;
	 	params.certificate_level2_d = document.ladder_form.certificate_level2_d.value;

	} else {
		params.level2_total_value = "";
		params.auth_year_level2 = "";
		params.auth_month_level2 = "";
		params.auth_day_level2 = "";
		params.certificate_level2_y = "";
		params.certificate_level2_m = "";
		params.certificate_level2_d = "";
	}

	if (ladderlevel == 3) {

		params.ladderlevel = 3;

		if ((document.ladder_form.auth_year_level3.value == "" ||
			document.ladder_form.auth_month_level3.value == "" ||
			document.ladder_form.auth_day_level3.value == "" ||
			document.ladder_form.auth_year_level3.value == "-" ||
			document.ladder_form.auth_month_level3.value == "-" ||
			document.ladder_form.auth_day_level3.value == "-") &&
			(document.ladder_form.certificate_level3_y.value != "" &&
			document.ladder_form.certificate_level3_m.value != "" &&
			document.ladder_form.certificate_level3_d.value != "" &&
			document.ladder_form.certificate_level3_y.value != "-" &&
			document.ladder_form.certificate_level3_m.value != "-" &&
			document.ladder_form.certificate_level3_d.value != "-"))
		{
			alert('認定証発行日を入れる場合は認定日を設定してください。');
			return false;
		}

		if (document.ladder_form.certificate_level3_y.value != "")
		{
			if (compareDate(
				document.ladder_form.certificate_level3_y.value,
				document.ladder_form.certificate_level3_m.value,
				document.ladder_form.certificate_level3_d.value,
				document.ladder_form.auth_year_level3.value,
				document.ladder_form.auth_month_level3.value,
				document.ladder_form.auth_day_level3.value
			) < 1) {
				alert('認定日が正しく選択されていません。');
				return false;
			}
		}

		params.level3_total_value = document.ladder_form.level3_total_value.value;

	 	params.auth_year_level3 = document.ladder_form.auth_year_level3.value;
	 	params.auth_month_level3 = document.ladder_form.auth_month_level3.value;
	 	params.auth_day_level3 = document.ladder_form.auth_day_level3.value;

	 	params.certificate_level3_y = document.ladder_form.certificate_level3_y.value;
	 	params.certificate_level3_m = document.ladder_form.certificate_level3_m.value;
	 	params.certificate_level3_d = document.ladder_form.certificate_level3_d.value;

	} else {
		params.level3_total_value = "";
		params.auth_year_level3 = "";
		params.auth_month_level3 = "";
		params.auth_day_level3 = "";
		params.certificate_level3_y = "";
		params.certificate_level3_m = "";
		params.certificate_level3_d = "";
	}

	if (ladderlevel == 4) {

		params.ladderlevel = 4;

		if ((document.ladder_form.auth_year_level4.value == "" ||
			document.ladder_form.auth_month_level4.value == "" ||
			document.ladder_form.auth_day_level4.value == "" ||
			document.ladder_form.auth_year_level4.value == "-" ||
			document.ladder_form.auth_month_level4.value == "-" ||
			document.ladder_form.auth_day_level4.value == "-") &&
			(document.ladder_form.certificate_level4_y.value != "" &&
			document.ladder_form.certificate_level4_m.value != "" &&
			document.ladder_form.certificate_level4_d.value != "" &&
			document.ladder_form.certificate_level4_y.value != "-" &&
			document.ladder_form.certificate_level4_m.value != "-" &&
			document.ladder_form.certificate_level4_d.value != "-"))
		{
			alert('認定証発行日を入れる場合は認定日を設定してください。');
			return false;
		}

		if (document.ladder_form.certificate_level4_y.value != "")
		{
			if (compareDate(
				document.ladder_form.certificate_level4_y.value,
				document.ladder_form.certificate_level4_m.value,
				document.ladder_form.certificate_level4_d.value,
				document.ladder_form.auth_year_level4.value,
				document.ladder_form.auth_month_level4.value,
				document.ladder_form.auth_day_level4.value
			) < 1) {
				alert('認定日が正しく選択されていません。');
				return false;
			}
		}

		params.level4_total_value = document.ladder_form.level4_total_value.value;

	 	params.auth_year_level4 = document.ladder_form.auth_year_level4.value;
	 	params.auth_month_level4 = document.ladder_form.auth_month_level4.value;
	 	params.auth_day_level4 = document.ladder_form.auth_day_level4.value;

	 	params.certificate_level4_y = document.ladder_form.certificate_level4_y.value;
	 	params.certificate_level4_m = document.ladder_form.certificate_level4_m.value;
	 	params.certificate_level4_d = document.ladder_form.certificate_level4_d.value;

	} else {
		params.level4_total_value = "";
		params.auth_year_level4 = "";
		params.auth_month_level4 = "";
		params.auth_day_level4 = "";
		params.certificate_level4_y = "";
		params.certificate_level4_m = "";
		params.certificate_level4_d = "";
	}

	if (ladderlevel == 5) {

		params.ladderlevel = 5;

		if ((document.ladder_form.auth_year_level5.value == "" ||
			document.ladder_form.auth_month_level5.value == "" ||
			document.ladder_form.auth_day_level5.value == "" ||
			document.ladder_form.auth_year_level5.value == "-" ||
			document.ladder_form.auth_month_level5.value == "-" ||
			document.ladder_form.auth_day_level5.value == "-") &&
			(document.ladder_form.certificate_level5_y.value != "" &&
			document.ladder_form.certificate_level5_m.value != "" &&
			document.ladder_form.certificate_level5_d.value != "" &&
			document.ladder_form.certificate_level5_y.value != "-" &&
			document.ladder_form.certificate_level5_m.value != "-" &&
			document.ladder_form.certificate_level5_d.value != "-"))
		{
			alert('認定証発行日を入れる場合は認定日を設定してください。');
			return false;
		}

		if (document.ladder_form.certificate_level5_y.value != "")
		{
			if (compareDate(
				document.ladder_form.certificate_level5_y.value,
				document.ladder_form.certificate_level5_m.value,
				document.ladder_form.certificate_level5_d.value,
				document.ladder_form.auth_year_level5.value,
				document.ladder_form.auth_month_level5.value,
				document.ladder_form.auth_day_level5.value
			) < 1) {
				alert('認定日が正しく選択されていません。');
				return false;
			}
		}

		params.level5_total_value = document.ladder_form.level5_total_value.value;

	 	params.auth_year_level5 = document.ladder_form.auth_year_level5.value;
	 	params.auth_month_level5 = document.ladder_form.auth_month_level5.value;
	 	params.auth_day_level5 = document.ladder_form.auth_day_level5.value;

	 	params.certificate_level5_y = document.ladder_form.certificate_level5_y.value;
	 	params.certificate_level5_m = document.ladder_form.certificate_level5_m.value;
	 	params.certificate_level5_d = document.ladder_form.certificate_level5_d.value;

	} else {
		params.level5_total_value = "";
		params.auth_year_level5 = "";
		params.auth_month_level5 = "";
		params.auth_day_level5 = "";
		params.certificate_level5_y = "";
		params.certificate_level5_m = "";
		params.certificate_level5_d = "";
	}

	//------------------------------------------------------------------------------------------
	//ウィンドウサイズなどのオプション
	var w = 880;
	var h = 700;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

	//------------------------------------------------------------------------------------------
	//子画面を開く
	//------------------------------------------------------------------------------------------
	commonOpen(moduleName,params,option,'cl_ladder_authorize_employe',params.p_session);
	*/
	// 2012/07/13 Yamagawa del(e)

	// 2012/07/13 Yamagawa add(s)
	if (
		(
			document.getElementById('auth_year_level' + ladderlevel).value == ""
			|| document.getElementById('auth_month_level' + ladderlevel).value == ""
			|| document.getElementById('auth_day_level' + ladderlevel).value == ""
			|| document.getElementById('auth_year_level' + ladderlevel).value == "-"
			|| document.getElementById('auth_month_level' + ladderlevel).value == "-"
			|| document.getElementById('auth_day_level' + ladderlevel).value == "-"
		)
		&& (
			document.getElementById('certificate_level' + ladderlevel + '_y').value != ""
			&& document.getElementById('certificate_level' + ladderlevel + '_m').value != ""
			&& document.getElementById('certificate_level' + ladderlevel + '_d').value != ""
			&& document.getElementById('certificate_level' + ladderlevel + '_y').value != "-"
			&& document.getElementById('certificate_level' + ladderlevel + '_m').value != "-"
			&& document.getElementById('certificate_level' + ladderlevel + '_d').value != "-"
		)
	){
		alert('認定証発行日を入れる場合は認定日を設定してください。');
		return false;
	}

	if (document.getElementById('certificate_level' + ladderlevel + '_y').value != ""){
		if (
			compareDate(
				document.getElementById('certificate_level' + ladderlevel + '_y').value,
				document.getElementById('certificate_level' + ladderlevel + '_m').value,
				document.getElementById('certificate_level' + ladderlevel + '_d').value,
				document.getElementById('auth_year_level' + ladderlevel).value,
				document.getElementById('auth_month_level' + ladderlevel).value,
				document.getElementById('auth_day_level' + ladderlevel).value
			) < 1
		) {
			alert('認定日が正しく選択されていません。');
			return false;
		}
	}

	document.ladder_form.ladderlevel.value = ladderlevel;
	document.ladder_form.get_level_date_regist.value = 'true';
	document.ladder_form.target = "_self";
	document.ladder_form.action = "./cl_common_call.php";
	document.ladder_form.submit();
	// 2012/07/13 Yamagawa add(e)

}

// 2つの日付の差（何日間あるか）を求める
function compareDate(year1, month1, day1, year2, month2, day2) {
    var dt1 = new Date(year1, month1 - 1, day1);
    var dt2 = new Date(year2, month2 - 1, day2);
    var diff = dt1 - dt2;
    var diffDay = diff / 86400000;//1日は86400000ミリ秒
    return diffDay+1;
}

function update_certificate_date()
{
	// 2012/07/13 Yamagawa upd(s)
	/*
	var moduleName = "cl_ladder_authorize_employe";

	//リクエストパラメータをオブジェクトで作成
	var params = new Object();
	params.emp_id = document.ladder_form.emp_id.value;
	params.now_level = document.ladder_form.now_level.value;
	params.year = document.ladder_form.year.value;
	params.mode = document.ladder_form.mode.value;
	params.p_session = document.ladder_form.session.value;
	*/

	var update_chk_count = 0;
	var emp_id = "";
	var emp_id_str = "";
	for (i=1; i < 6; i++){
		if( document.getElementById('level' + i).checked ){
			if (document.getElementById('auth_year_level' + i).value == "" ||
				document.getElementById('auth_month_level' + i).value == "" ||
				document.getElementById('auth_day_level' + i).value == "" ||
				document.getElementById('auth_year_level' + i).value == "-" ||
				document.getElementById('auth_month_level' + i).value == "-" ||
				document.getElementById('auth_day_level' + i).value == "-") {
				alert("認定証が出力できません。");
				return false;
			}
			update_chk_count++;
			emp_id = document.ladder_form.emp_id.value;
			emp_id = emp_id + ','+ document.ladder_form.emp_full_nm.value;
			emp_id = emp_id + ',' + document.ladder_form.class_nm.value;
			if ( i==1 ){
				emp_id = emp_id + ',' + 'I';
			} else if (i==2){
				emp_id = emp_id + ',' + 'II';
			} else if (i==3){
				emp_id = emp_id + ',' + 'III';
			} else if (i==4){
				emp_id = emp_id + ',' + 'IV';
			} else if (i==5){
				emp_id = emp_id + ',' + 'V';
			}
			emp_id = emp_id + ',' + document.getElementById('level' + i + '_total_value').value;
			emp_id = emp_id + ',' + document.getElementById('auth_year_level' + i).value + '/' + document.getElementById('auth_month_level' + i).value + '/' + document.getElementById('auth_day_level' + i).value;
			emp_id = emp_id + ',' + document.getElementById('certificate_level' + i + '_y').value + '/' + document.getElementById('certificate_level' + i + '_m').value + '/' + document.getElementById('certificate_level' + i + '_d').value;
			emp_id_str = emp_id_str + emp_id + '@';
		}
	}

	if( update_chk_count == 0 ){
		alert("認定証発行できるものがありません。");
		return;
	}

	// 2012/07/13 Yamagawa del(s)
	/*
	params.print_flg = 'update';
	params.ladder_data = emp_id_str;

	//------------------------------------------------------------------------------------------
	//ウィンドウサイズなどのオプション
	var w = 880;
	var h = 700;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

	//------------------------------------------------------------------------------------------
	//子画面を開く
	//------------------------------------------------------------------------------------------
	commonOpen(moduleName,params,option,'cl_ladder_authorize_employe',params.p_session);
	*/
	// 2012/07/13 Yamagawa del(e)

	// 2012/07/13 Yamagawa add(s)
	document.ladder_form.print_flg.value = 'update';
	document.ladder_form.ladder_data.value = emp_id_str;
	document.ladder_form.target = "_self";
	document.ladder_form.action = "./cl_common_call.php";
	document.ladder_form.submit();
	// 2012/07/13 Yamagawa add(e)

}

function output_to_print()
{

	var moduleName = "cl_ladder_authorize_print_pdf";

	//リクエストパラメータをオブジェクトで作成
	var params = new Object();
	params.emp_date = document.ladder_form.ladder_data.value;
	params.p_session = document.ladder_form.session.value;

	//ウィンドウサイズなどのオプション
	var w = 500;
	var h = 700;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

	//子画面を開く
	commonOpen(moduleName,params,option,'cl_ladder_authorize_print_pdf',params.p_session);

}
