<?php

	ini_set( 'display_errors', 1 );

	//##########################################################################
	// 各種モジュールの読み込み
	//##########################################################################
	require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
	require_once(dirname(__FILE__) . "/../../Cmx.php");
	require_once("MDB2.php");

	//##########################################################################
	// 初期処理
	//##########################################################################

	//ロガーをロード
	$log = new cl_common_log_class(basename(__FILE__));
	$log->info(basename(__FILE__)." START");

	//====================================
	//画面名
	//====================================
	$fname = $PHP_SELF;

	//##########################################################################
	// パラメータ取得
	//##########################################################################
	foreach( $_REQUEST as $key => $value ) {
		$log->debug('$_REQUEST * ['.$key.']:'.$_REQUEST[$key],__FILE__,__LINE__);
	}
	$training_id	= $_POST["training_id"];
	$session		= $_POST["session"];
	$emp_id			= $_POST["login_user"];

	$log->debug('■$training_id   :'.$training_id	,__FILE__,__LINE__);

	// データベース接続
	$mdb2 = MDB2::connect(CMX_DB_DSN);
	$log->debug('データベース接続 OK'	,__FILE__,__LINE__);

	// セッションよりログインユーザー取得
	require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php");
	$empmst_model = new empmst_model($mdb2,$emp_id);
	$arr_empmst = $empmst_model->select_login_emp($session);
	$emp_id = $arr_empmst["emp_id"];
	$log->debug('■$emp_id2   :'.$emp_id	,__FILE__,__LINE__);

	$log->debug('アンケートCSV出力 ',__FILE__,__LINE__);

	// アンケートデータ取得
	require_once(dirname(__FILE__) . "/../model/template/cl_apl_inside_training_answer_model.php");
	$cl_apl_inside_training_answer_model = new cl_apl_inside_training_answer_model($mdb2, $emp_id);
	// 2012/11/16 Yamagawa upd(s)
	//$arr_answer = $cl_apl_inside_training_answer_model->select_by_training_id($training_id);
	$arr_answer = $cl_apl_inside_training_answer_model->select_by_training_id($training_id, $_POST['year']);
	// 2012/11/16 Yamagawa upd(e)

	// データベース切断
	$mdb2->disconnect();

	// ヘッダー行
	// 項目配列作成
	$array_csv_header = array();
	$array_csv_header[] = "研修の目的、目標は達成できましたか？";
	$array_csv_header[] = "ご自身の学習課題は達成できましたか？";
	$array_csv_header[] = "研修の日時は適切だと思いましたか？";
	$array_csv_header[] = "研修の日時は適切だと思いましたか？ 理由";
	$array_csv_header[] = "研修の方法（進め方）は適切だと思いましたか？";
	$array_csv_header[] = "研修の方法（進め方）は適切だと思いましたか？ 理由";
	$array_csv_header[] = "研修の内容は分かりやすいものでしたか？";
	$array_csv_header[] = "研修の内容は分かりやすいものでしたか？ 理由";
	$array_csv_header[] = "研修内容は今後の看護実践に役立ちますか？";
	$array_csv_header[] = "研修内容は今後の看護実践に役立ちますか？ 理由";
	$array_csv_header[] = "研修全体を通して気づいた点をご記入ください";
	// 文字コード変換
	$csv_header = array();
	foreach($array_csv_header as $tmp_csv_header){
		$csv_header[] = format_column_for_csv($tmp_csv_header);
	}
	// 配列から文字列化
	$csv_header = implode(",", $csv_header);

	// 明細行
	$ans_cnt=0;
	foreach ($arr_answer as $row){
		$ans_cnt++;
		$csv_body .=      format_column_for_csv($row['achievement_degree']);
		$csv_body .= ",". format_column_for_csv($row['mine_learned']);
		$csv_body .= ",". format_column_for_csv($row['training_plan_degree']);
		$csv_body .= ",". format_column_for_csv($row['training_plan_degree_reason']);
		$csv_body .= ",". format_column_for_csv($row['training_method']);
		$csv_body .= ",". format_column_for_csv($row['training_method_reason']);
		$csv_body .= ",". format_column_for_csv($row['training_contents']);
		$csv_body .= ",". format_column_for_csv($row['training_contents_reason']);
		$csv_body .= ",". format_column_for_csv($row['training_practice']);
		$csv_body .= ",". format_column_for_csv($row['training_practice_reason']);
		$csv_body .= ",". format_column_for_csv($row['point_notice']);
		$csv_body .= "\r\n";
	}

	$log->debug('■csv_body   '.$csv_body,__FILE__,__LINE__);

	// ヘッダーと本文を結合
	$csv = $csv_header ."\r\n". $csv_body;
	// ファイル名の先頭に研修id(training_id)を付与
	$file_name = $training_id."_answer_list.csv";

	// CSV出力
	header("Content-Disposition: attachment; filename=$file_name");
	header("Content-Type: application/octet-stream; name=$file_name");
	header("Content-Length: " . strlen($csv));
	echo($csv);

	// 文字コード変換
	function format_column_for_csv($value, $from_encoding = "EUC-JP") {
		$buf = str_replace("\r", "", $value);
		$buf = str_replace("\n", "", $buf);
		if (strpos($buf, ",") !== false)  {
			$buf = '"' . $buf . '"';
		}
		return mb_convert_encoding($buf, "Shift_JIS", $from_encoding);
	}

	$log->info(basename(__FILE__)." END");
	$log->shutdown();

?>