/**
 * 日程情報取得処理
 */
function checkMode(){

	// 2012/11/19 Yamagawa upd(s)
	/*
	if( document.form.mode.value == 2 ){
		getPlanInfo();
	}
	else{
		// 何もしない
	}
	*/
	switch (document.form.mode.value) {
		case '2':
			getPlanInfo();
			break;
		case '3':
			document.form.year.value = document.form.target_year.value;
			break;
		default:
			break;
	}
	// 2012/11/19 Yamagawa upd(e)

}

/**
 * 日程情報取得処理
 */
function getPlanInfo(){

	jQuery.ajax({
		contentType: 'application/x-www-form-urlencoded; charset=EUC-JP',
		type: 'post',
		url: 'cl/common/cl_mst_inside_training_schedule_json.php',
		dataType: 'text',
		data:{ "plan_id"	: document.form.plan_id.value , "ts" : new Date().getTime() } ,
		success: function(data, dataType){

			param = eval('(' + data + ')');
			setViewData( param );

		},
		error: function(XMLHttpRequest, textStatus, errorThrown){
			alert('開催日登録処理の起動に失敗しました。');
		}
	});

}

/**
 * 表示項目データ設定
 */
function setViewData( param ) {

	/**
	 * データ設定
	 */
	/* plan_id */
	document.form.plan_id.value = param.plan_id;
	/* 年度 */
	document.form.year.value = param.year;

	// 2012/11/19 Yamagawa del(s)
	///* 期区分 */
	//setSelectValue("term_div" , param.term_div );
	// 2012/11/19 Yamagawa del(e)

	/* 開催回数 */
	setSelectValue("training_time" , param.training_time );
	/* 期区分 */
	setSelectValue("plan_date_yyyy" , param.plan_date_yyyy );
	setSelectValue("plan_date_mm" , param.plan_date_mm );
	setSelectValue("plan_date_dd" , param.plan_date_dd );

	// 2012/04/12 Yamagawa upd(s)
	// 「23:59」は「24:00」に変換（暫定対応）
	if (
		String(param.from_time_hh) == "23"
		&& String(param.from_time_mi) == "59"
	) {
		setSelectValue("from_time_hh" , "24" );
		setSelectValue("from_time_mi" , "00" );
	} else {
		setSelectValue("from_time_hh" , param.from_time_hh );
		setSelectValue("from_time_mi" , param.from_time_mi );
	}
	if (
		String(param.to_time_hh) == "23"
		&& String(param.to_time_mi) == "59"
	) {
		setSelectValue("to_time_hh" , "24" );
		setSelectValue("to_time_mi" , "00" );
	} else {
		setSelectValue("to_time_hh" , param.to_time_hh );
		setSelectValue("to_time_mi" , param.to_time_mi );
	}
	// 2012/04/12 Yamagawa upd(e)

	document.form.max_people.value = param.max_people;

	document.form.place.value = to_javascript_string(param.place);

	document.form.remarks.value = to_javascript_string(param.remarks_br);

	// 2012/07/26 Yamagawa upd(s)
	//setSelectValue("apply_status" , param.apply_status );
	document.form.apply_status.value = param.apply_status;
	// 2012/07/26 Yamagawa upd(e)

	/* 初回登録時の日程ID */
	document.form.first_plan_id.value = param.first_plan_id;

}

/**
 * 日程更新処理
 */
function update(){
	//------------------------------------------------------------------------------------------
	// 報告区分取得
	//------------------------------------------------------------------------------------------
	var report_division;

	//------------------------------------------------------------------------------------------
	// アンケート区分
	//------------------------------------------------------------------------------------------
	var answer_division;

	//------------------------------------------------------------------------------------------
	// 入力チェック
	//------------------------------------------------------------------------------------------
	vldt_rtn = Apl_Validate();
	if( vldt_rtn == false ){
		return;
	}

	//------------------------------------------------------------------------------------------
	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	//------------------------------------------------------------------------------------------
	if(confirm('開催日情報を更新しますか？')){
		jQuery.ajax({
			contentType: 'application/x-www-form-urlencoded; charset=EUC-JP',
			type: 'post',
			url: 'cl/common/cl_mst_inside_training_schedule_update.php',
			dataType: 'json',
			data:{
					"plan_id"			: document.form.plan_id.value						,	// PLAN_ID
					"first_plan_id"		: document.form.first_plan_id.value					,	// 初回登録時の日程ID
					"login_user"		: document.form.login_user.value					,	// ログインユーザID
					"training_id"		: document.form.training_id.value					,	// 院内研修ID
					"year"				: document.form.year.value							,	// 年度
					// 2012/11/19 Yamagawa upd(s)
					//"term_div"			: document.form.term_div.value						,	// 期区分
					"term_div"			: ''												,	// 期区分
					// 2012/11/19 Yamagawa upd(e)
					"training_time"		: document.form.training_time.value					,	// 開催回数
					"max_training_time"	: document.form.max_training_time.value				,	// 研修回数
					"plan_date_yyyy"	: document.form.plan_date_yyyy.value				,	// 開催日付年
					"plan_date_mm"		: document.form.plan_date_mm.value					,	// 開催日付月
					"plan_date_dd"		: document.form.plan_date_dd.value					,	// 開催日付日
					"from_time_hh"		: document.form.from_time_hh.value					,	// 開催時刻　From 時
					"from_time_mi"		: document.form.from_time_mi.value					,	// 開催時刻　From 分
					"to_time_hh"		: document.form.to_time_hh.value					,	// 開催時刻　To 時
					"to_time_mi"		: document.form.to_time_mi.value					,	// 開催時刻　To 分
					"report_division"	: report_division									,	// 報告区分
					"answer_division"	: answer_division									,	// アンケート区分
					"max_people"		: document.form.max_people.value					,	// 最大人数
					"apply_status"		: document.form.apply_status.value					,	// 申込ステータス
					"place"				: encodeURIComponent(document.form.place.value)		,	// 会場
					"remarks"			: encodeURIComponent(document.form.remarks.value)	,	// 備考
					// 2012/08/27 Yamagawa add(s)
					"mode"				: document.form.mode.value								// 区分
					// 2012/08/27 Yamagawa add(e)
			},
			success: function(data, dataType){

				if( data.success == 'true' ){
					// 何もしない
				}
				else{
					alert('開催日更新に失敗しました。');
				}

				//呼び出し元画面にインターフェース関数が定義されている場合
				if(window.opener && !window.opener.closed && window.opener.plan_regist_return)
				{
					//親画面へ通知
					window.opener.plan_regist_return();
				}
				//自画面を終了します。
				window.close();

			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				alert('開催日更新処理の起動に失敗しました。');
				alert('XMLHttpRequest:' + XMLHttpRequest);
				alert('textStatus:' + textStatus);
				alert('errorThrown:' + errorThrown);
				window.opener.plan_regist_return();
				window.close();
			}
		});
	}
	else{
		// 何もしない
	}
}

/**
 * 日程登録処理
 */
function regist(){

	//------------------------------------------------------------------------------------------
	// 報告区分取得
	//------------------------------------------------------------------------------------------
	var report_division;

	//------------------------------------------------------------------------------------------
	// アンケート区分
	//------------------------------------------------------------------------------------------
	var answer_division;

	//------------------------------------------------------------------------------------------
	// 入力チェック
	//------------------------------------------------------------------------------------------
	vldt_rtn = Apl_Validate();
	if( vldt_rtn == false ){
		return;
	}

	//------------------------------------------------------------------------------------------
	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	//------------------------------------------------------------------------------------------
	if(confirm('開催日情報を登録しますか？')){
		jQuery.ajax({
			contentType: 'application/x-www-form-urlencoded; charset=EUC-JP',
			type: 'post',
			url: 'cl/common/cl_mst_inside_training_schedule_regist.php',
			dataType: 'json',
			data:{
					"login_user"		: document.form.login_user.value					,	// ログインユーザID
					"training_id"		: document.form.training_id.value					,	// 院内研修ID
					"year"				: document.form.year.value							,	// 年度
					// 2012/11/19 Yamagawa upd(s)
					//"term_div"			: document.form.term_div.value						,	// 期区分
					"term_div"			: ''												,	// 期区分
					// 2012/11/19 Yamagawa upd(e)
					"training_time"		: document.form.training_time.value					,	// 開催回数
					"max_training_time"	: document.form.max_training_time.value				,	// 研修回数
					"plan_date_yyyy"	: document.form.plan_date_yyyy.value				,	// 開催日付年
					"plan_date_mm"		: document.form.plan_date_mm.value					,	// 開催日付月
					"plan_date_dd"		: document.form.plan_date_dd.value					,	// 開催日付日
					"from_time_hh"		: document.form.from_time_hh.value					,	// 開催時刻　From 時
					"from_time_mi"		: document.form.from_time_mi.value					,	// 開催時刻　From 分
					"to_time_hh"		: document.form.to_time_hh.value					,	// 開催時刻　To 時
					"to_time_mi"		: document.form.to_time_mi.value					,	// 開催時刻　To 分
					"report_division"	: report_division									,	// 報告区分
					"answer_division"	: answer_division									,	// アンケート区分
					"max_people"		: document.form.max_people.value					,	// 最大人数
					"apply_status"		: document.form.apply_status.value					,	// 申込ステータス
					"place"				: encodeURIComponent(document.form.place.value)		,	// 会場
					"remarks"			: encodeURIComponent(document.form.remarks.value)		// 備考
			},
			success: function(data, dataType){

				if( data.success == 'true' ){
					// 何もしない
				}
				else{
					alert('開催日登録に失敗しました。');
				}

				//呼び出し元画面にインターフェース関数が定義されている場合
				if(window.opener && !window.opener.closed && window.opener.plan_regist_return)
				{
					//親画面へ通知
					window.opener.plan_regist_return();
				}
				//自画面を終了します。
				window.close();

			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				alert('開催日登録処理の起動に失敗しました。');
				window.opener.plan_regist_return();
				window.close();
			}
		});
	}
	else{
		// 何もしない
	}

}

/**
 * 申請入力チェック
 */
function Apl_Validate(){

	var errinfo = new Array();
	var err_idx=0;
	var re = new RegExp("[^0-9]+");

	// 2012/08/27 Yamagawa add(s)
	// 2012/11/28 Yamagawa upd(s)
	//if (document.form.mode.value == "2") {
	if (document.form.mode.value != "3") {
	// 2012/11/28 Yamagawa upd(e)
	// 2012/08/27 yamagawa add(e)

		/* 開催回数 */
		if( document.form.training_time.value == "" ){
			errinfo[err_idx++]="開催回数が未選択です。";
			document.form.training_time.style.backgroundColor="FFB6C1";
		}
		else{
			document.form.training_time.style.backgroundColor="";
		}

		/* 開催時間 */
		var from_h = document.form.from_time_hh.value;
		var from_m = document.form.from_time_mi.value;
		var to_h = document.form.to_time_hh.value;
		var to_m = document.form.to_time_mi.value;

		// 2012/04/12 Yamagawa add(s)
		if(from_h == "24" && from_m != "00"){
			errinfo[err_idx++]="開始時間に24時以降が設定されています。";
			document.form.from_time_hh.style.backgroundColor="FFB6C1";
			document.form.from_time_mi.style.backgroundColor="FFB6C1";
		} else {
			document.form.from_time_hh.style.backgroundColor="";
			document.form.from_time_mi.style.backgroundColor="";
		}
		// 2012/04/12 Yamagawa add(e)

		if(from_h > to_h){
			errinfo[err_idx++]="終了時間に開始時間以前が設定されています。";
			document.form.to_time_hh.style.backgroundColor="FFB6C1";
			document.form.to_time_mi.style.backgroundColor="FFB6C1";
		// 2012/04/12 Yamagawa add(s)
		}else if(to_h == "24" && to_m != "00"){
			errinfo[err_idx++]="終了時間に24時以降が設定されています。";
			document.form.to_time_hh.style.backgroundColor="FFB6C1";
			document.form.to_time_mi.style.backgroundColor="FFB6C1";
		// 2012/04/12 Yamagawa add(e)
		}else if(from_h == to_h){
			if(from_m >= to_m){
				errinfo[err_idx++]="終了時間に開始時間以前が設定されています。";
				document.form.to_time_hh.style.backgroundColor="FFB6C1";
				document.form.to_time_mi.style.backgroundColor="FFB6C1";
			}else{
				document.form.to_time_hh.style.backgroundColor="";
				document.form.to_time_mi.style.backgroundColor="";
			}
		}else{
			document.form.to_time_hh.style.backgroundColor="";
			document.form.to_time_mi.style.backgroundColor="";
		}

		/* 最大人数 */
		if( document.form.max_people.value == "" ){
			errinfo[err_idx++]="最大人数が未入力です。";
			document.form.max_people.style.backgroundColor="FFB6C1";
		}
		else if (re.test(document.form.max_people.value)) {
			errinfo[err_idx++] = '最大人数に半角数値以外が含まれています。';
			document.form.max_people.style.backgroundColor="FFB6C1";
		}
		else if (parseInt(document.form.max_people.value) < 1) {
			errinfo[err_idx++] = '最大人数が1未満です。';
			document.form.max_people.style.backgroundColor="FFB6C1";
		}
		else {
			document.form.max_people.style.backgroundColor="";
		}

		/* 会場 */
		if( document.form.place.value == "" ){
			errinfo[err_idx++]="会場が未入力です。";
			document.form.place.style.backgroundColor="FFB6C1";
		}
		else{
			document.form.place.style.backgroundColor="";
		}

	// 2012/08/27 Yamagawa add(s)
	}
	// 2012/08/27 Yamagawa add(e)

	var msg="";
	for (i = 0; i < errinfo.length; i++) {
		msg = msg + "・" + errinfo[i] + "\n";
	}

	// 入力チェック結果メッセージの表示
	if( errinfo.length > 0 ){
		msg="申請内容に誤りがあります。入力項目を確認してください。\n"+msg;
		alert(msg);
		return false;
	}
	else{
		return true;
	}

}

function to_javascript_string(str)
{
	var ret = str;
	ret = ret.replace(/\\n/g,   '\n');
	ret = ret.replace(/&amp;/g,   '&');
	ret = ret.replace(/&quot;/g,  '"');
	ret = ret.replace(/&lt;/g,    '<');
	ret = ret.replace(/&gt;/g,    '>');
	ret = ret.replace(/&amp;/g,   '&');
	//2012/05/11 K.Fujii ins(s)
	ret = ret.replace(/\''/g,     '\'');
	ret = ret.replace(/\\\\/g,     '\\');
	//2012/05/11 K.Fujii ins(e)
	return ret;
}