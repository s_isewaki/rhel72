<?
/*
レベルアップ申請書番号一覧
・評価を行う際に、申請書番号を選択する画面


【呼び出しパラメータ】
session　　　　　　　　　　セッションID。
emp_id 　　　　　　　　　　職員ID。

【戻り値】
levelup_apply_id　　　　　レベルアップ申請ID

*/


require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cl_application_list.inc");
require_once("cl_application_common.ini");
require_once("cl_common.ini");
require_once("cl_title_name.ini");
require_once("cl_smarty_setting.ini");
require_once("cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once('MDB2.php');



//====================================
//リクエストパラメータ取得
//====================================
$common_module = $_POST["common_module"];
$session = $_POST["session"];
$emp_id = $_POST["emp_id"];
$level = $_POST["level"];



//====================================
//初期処理
//====================================

//ロガーをロード
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");


//画面名
$fname = $PHP_SELF;


//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//権限チェック
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}



//====================================
// データベース接続
//====================================
$log->debug('データベース接続 START',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
$log->debug('データベース接続 END',__FILE__,__LINE__);



//====================================
// モデルをロード
//====================================

$log->debug('require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php"); START',__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php");
$empmst_model = new empmst_model($mdb2);
$log->debug('require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php"); END',__FILE__,__LINE__);

// セッションよりログインユーザーID(職員ID)を取得
$arr_empmst = $empmst_model->select_login_emp($session);
$login_emp_id = $arr_empmst[0]["emp_id"];

// 個人プロフィールテーブルより申請レベルを取得
require_once(dirname(__FILE__) . "/../model/ladder/cl_personal_profile_model.php");
$cl_personal_profile_model = new cl_personal_profile_model($mdb2,$login_emp_id);

$log->debug('require_once(dirname(__FILE__) . "/../model/search/cl_applied_inside_training_plan_list_model.php"); START',__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/search/cl_applied_inside_training_plan_list_model.php");
$cl_applied_inside_training_plan_list_model = new cl_applied_inside_training_plan_list_model($mdb2,$login_emp_id);
$log->debug('require_once(dirname(__FILE__) . "/../model/search/cl_applied_inside_training_plan_list_model.php"); END',__FILE__,__LINE__);

// レベルアップ申請テーブルより一覧用の項目を取得
$log->debug('require_once(dirname(__FILE__) . "/../model/search/cl_apl_levelup_apply_list_model.php"); START',__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/search/cl_apl_levelup_apply_list_model.php");
$cl_apl_levelup_apply_list_model = new cl_apl_levelup_apply_list_model($mdb2,$login_emp_id);
$log->debug('require_once(dirname(__FILE__) . "/../model/search/cl_apl_levelup_apply_list_model.php"); END',__FILE__,__LINE__);


//====================================
// データ取得
//====================================

//対象レベル　表示用
$target_level_disp = "";
switch ($target_level)
{
	case 1:
		$target_level_disp = "I";
		break;
	case 2:
		$target_level_disp = "II";
		break;
	case 3:
		$target_level_disp = "III";
		break;
	case 4:
		$target_level_disp = "IV";
		break;
	case 5:
		$target_level_disp = "V";
		break;
	case 6:
		$target_level_disp = "";
		break;
}


//研修申込一覧を取得
$log->debug('$cl_apl_levelup_apply_list_model->getLevelupApplyList($emp_id, $level); START',__FILE__,__LINE__);
$arr_training_list = $cl_apl_levelup_apply_list_model->getLevelupApplyList_forNewEval($emp_id, $level);
$log->debug('$cl_apl_levelup_apply_list_model->getLevelupApplyList($emp_id, $level); END',__FILE__,__LINE__);



//====================================
// データベース切断
//====================================
$log->debug('データベース切断 START',__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug('データベース切断 END',__FILE__,__LINE__);



//====================================================================================================
// HTML出力
//====================================================================================================

$page_title = "レベルアップ申請書番号一覧";

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cl_title?>｜<?=$page_title?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<script type="text/javascript">


//====================================================================================================
//■イベント処理

//研修講座一覧から研修講座を選択した時の処理
function return_selected(levelup_apply_id)
{
	//呼び出し元画面にインターフェース関数が定義されている場合
	if(window.opener && !window.opener.closed && window.opener.cl_apl_levelup_apply_list_return)
	{
		//返却値を取得
		var array_key = levelup_apply_id
		var result = m_return_data_list[array_key];
		//親画面へ通知
		window.opener.cl_apl_levelup_apply_list_return(result);
	}
	//自画面を終了します。
	window.close();
}//function return_selected(training_apply_id)  END



//返却値配列(key:研修申込の院内研修ID_日程ID、val:返却値)
var m_return_data_list = new Array();
<?
for($i = 0; $i < count($arr_training_list); $i++)
{
	//研修申込の院内研修ID
	$levelup_apply_id = $arr_training_list[$i]['levelup_apply_id'];
	//研修ID
	$level = $arr_training_list[$i]['level'];
	//研修名
	$apply_date = $arr_training_list[$i]['apply_date'];

	$levelup_emp_id = $arr_training_list[$i]['levelup_emp_id'];

	$levelup_apply_no = $arr_training_list[$i]['levelup_apply_no'];


	//配列キー
	$array_key = $levelup_apply_id
?>
	m_return_data_list["<?=$array_key?>"] = new Object();
	m_return_data_list["<?=$array_key?>"].levelup_apply_id   = "<?=to_javascript_string($levelup_apply_id)?>";
	m_return_data_list["<?=$array_key?>"].levelup_apply_no   = "<?=to_javascript_string($levelup_apply_no)?>";
	m_return_data_list["<?=$array_key?>"].level              = "<?=to_javascript_string($level)?>";
	m_return_data_list["<?=$array_key?>"].apply_date         = "<?=to_javascript_string($apply_date)?>";
	m_return_data_list["<?=$array_key?>"].levelup_emp_id      = "<?=to_javascript_string($levelup_emp_id)?>";
<?
//	}
}//for($i = 0; $i < count($arr_training_list); $i++) END
?>



//====================================================================================================
//■行のハイライト制御

//class属性が指定されたclass_nameのTDタグをハイライトします。

function highlightCells(class_name)
{
	changeCellsColor(class_name, '#ffff66');
}

//class属性が指定されたclass_nameのTDタグのハイライトを解除します。
function dehighlightCells(class_name)
{
	changeCellsColor(class_name, '');
}

//highlightCells()、dehighlightCells()専用関数
function changeCellsColor(class_name, color)
{
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++)
	{
		if (cells[i].className != class_name)
		{
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}



</script>
<style type="text/css">


table.list {border-collapse:collapse;}
table.list td {border:#A0D25A solid 1px;}


</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">


<form name="postback_form" method="post" action="cl_common_call.php" target="_self">
<input type="hidden" id="common_module"            name="common_module"            value="<?=$common_module ?>">
<input type="hidden" id="session"                  name="session"                  value="<?=$session ?>">
<input type="hidden" id="emp_id"                   name="emp_id"                   value="<?=$emp_id ?>">


<center>


<!-- ヘッダー START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$page_title?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<!-- ヘッダー END -->


<img src="img/spacer.gif" alt="" width="1" height="5"><br>

<!-- -------------------------------------------------------------------------------------------------------------- -->
<!-- ■研修申込一覧 -->
<!-- -------------------------------------------------------------------------------------------------------------- -->


<!-- 研修申込一覧表 START  -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">

<!-- 研修申込一覧 ヘッダー START  -->
<tr bgcolor="#E5F6CD">
<!-- <td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">レベルアップ申請ID</font></td> -->
<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">レベルアップ申請申請番号</font></td>
<td width="250" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請レベル</font></td>
<td width="250" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
<tr bgcolor="#E5F6CD">
</tr>
<!-- 研修申込一覧 ヘッダー END -->



<!-- 研修申込一覧 データ START -->
<?
	//for($i = 0; $i < ($arr_training_list.length); $i++){
	for($i = 0; $i < count($arr_training_list); $i++){

		$levelup_apply_id = $arr_training_list[$i]['levelup_apply_id'];
		$level = $arr_training_list[$i]['level'];
		$apply_date = $arr_training_list[$i]['apply_date'];
		$levelup_apply_no = $arr_training_list[$i]['levelup_apply_no'];


		//ハイライトするセルのクラス属性値
		$l_highlight_class_name = "highlight_class_".$i."_".$i_plan;

		//実行JavaScript関数
		$l_line_click_javascript = "return_selected('$levelup_apply_id');";


?>

<tr <?=$default_line_color?>>

<td align="center" class="<?=$l_highlight_class_name ?>"
                   onmouseover="highlightCells(this.className);this.style.cursor='pointer';"
                   onmouseout="dehighlightCells(this.className);this.style.cursor = '';"
                   onclick="<?=$l_line_click_javascript?>">
                   <!-- <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($levelup_apply_id))?></font> -->
                   <!-- <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($arr_training_list[$i]['levelup_apply_id']))?></font> -->
                   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($levelup_apply_no))?></font>
</td>


<td align="center" class="<?=$l_highlight_class_name ?>"
                   onmouseover="highlightCells(this.className);this.style.cursor='pointer';"
                   onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="<?=$l_line_click_javascript?>">
                   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($arr_training_list[$i]['level']))?></font>
                   <!-- <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($level))?></font> -->
</td>


<td align="center" class="<?=$l_highlight_class_name ?>"
                   onmouseover="highlightCells(this.className);this.style.cursor='pointer';"
                   onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="<?=$l_line_click_javascript?>">
                   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($apply_date))?></font>
                   <!-- <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($arr_training_list[$i]['apply_date']))?></font> -->
</td>

<?
	}
?>

<!-- 研修申込一覧 データ END -->

</table>
<!-- 研修申込一覧表 END  -->

</center>
</form>

</body>
</html>
<?

//====================================
// 終了処理
//====================================
$log->info(basename(__FILE__)." END");
$log->shutdown();



//====================================================================================================
// ローカル関数
//====================================================================================================

/**
 * JavaScript出力用に文字列変換します。
 *
 * @param $str 出力する文字列
 */
function to_javascript_string($str){
	$ret = $str;
	$ret = str_replace("\"","\\\"",$ret);
	$ret = str_replace("\r\n","\\n",$ret);
	$ret = str_replace("\r","\\n",$ret);
	$ret = str_replace("\n","\\n",$ret);
	return $ret;
}

?>