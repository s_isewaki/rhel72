<?
//==================================
//評価項目画面PHP（ラダー評価マスタ）
//==================================
require_once("about_session.php");
require_once("about_authority.php");
require_once("cl_workflow_common.ini");
require_once("cl_title_name.ini");
require_once("cl_common_log_class.inc");
require_once("cl_smarty_setting.ini");
require_once("Cmx.php");
require_once("MDB2.php");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

$smarty = cl_get_smarty_obj();
$smarty->template_dir = dirname(__FILE__) . "/../view";
$smarty->compile_dir  = dirname(__FILE__) . "/../../templates_c";

$session		= $_POST["session"];
$target_level	= $_POST["target_level"];
$master_kind_id	= $_POST["master_kind_id"];
$category_id	= $_POST["category_id"];
$group_id		= $_POST["group_id"];
$mode			= $_POST["mode"];

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
//if($session == "0"){
//	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
//	echo("<script language='javascript'>showLoginPage(window);</script>");
//	exit;
//}
//====================================
//権限チェック
//====================================
//$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
//if($wkfw=="0"){
//	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
//	echo("<script language='javascript'>showLoginPage(window);</script>");
//	exit;
//}
// ワークフロー権限の取得
//$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

//$cl_title = cl_title_name();

//====================================
// アプリケーションメニューHTML取得
//====================================
//$wkfwctn_mnitm_str=get_wkfw_menuitem($session,$fname);

//====================================
// データベース接続オブジェクト取得
//====================================
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
}
//====================================
// ログインユーザ情報取得
//====================================
require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php");
$empmst_model = new empmst_model($mdb2);
$arr_login = $empmst_model->select_login_emp($session);
$login_emp_id = $arr_login["emp_id"];


//====================================
// SQL用PHP設定
//====================================
// 評価項目
require_once(dirname(__FILE__) . "/../model/master/cl_mst_ludder_eval_model.php");
$cl_mst_ludder_eval = new cl_mst_ludder_eval_model($mdb2,$login_emp_id);

require_once(dirname(__FILE__) . "/../model/master/cl_mst_evaluation_item_model.php");
$cl_mst_evaluation = new cl_mst_evaluation_item_model($mdb2,$login_emp_id);

//====================================
// IdSequence取得
//====================================
require_once("cl/model/sequence/cl_evaluation_item_id_seq_model.php");
$id_model = new cl_evaluation_item_id_seq_model($mdb2,$login_emp_id);

//========================================================================
//Submit処理（Submit処理で分岐）
//========================================================================
	// 更新ボタン押下時
	if ($mode == 'UPDATA'){

				for($i=0; $i<$dataCount; $i++) {

					$no = $$i;
					$id = $i."_ID";
					$txt = $i."_TXT";
					$txt_mut = $i."_TXT_MUT";
					$level1 = $i."_LEVEL_1";
					$level2 = $i."_LEVEL_2";
					$level3 = $i."_LEVEL_3";
					$level4 = $i."_LEVEL_4";

					// 存在チェック
					$result = $cl_mst_evaluation->getRecord($$id);

						$param["level"]=$target_level;
						$param["evaluation_id"]=$master_kind_id;
						$param["category_id"]=$category_id;
						$param["group_id"]=$group_id;
						$param["item_name"]=$$txt;
						$param["must_value"]=$$txt_mut;
						$param["disp_order"]=$i;
						//2012/05/07 K.Fujii upd(s)
						//$param["disp_flg"]='0';
						$param["disp_flg"]='1';
						//2012/05/07 K.Fujii upd(e)

						if ($$level1 == '1')
						{
							$param["eval_disp_1_flg"]=$$level1;
						} else {
							$param["eval_disp_1_flg"]='0';
						}

						if ($$level2 == '1')
						{
							$param["eval_disp_2_flg"]=$$level2;
						} else {
							$param["eval_disp_2_flg"]='0';
						}

						if ($$level3 == '1')
						{
							$param["eval_disp_3_flg"]=$$level3;
						} else {
							$param["eval_disp_3_flg"]='0';
						}

						if ($$level4 == '1')
						{
							$param["eval_disp_4_flg"]=$$level4;
						} else {
							$param["eval_disp_4_flg"]='0';
						}
					if (count($result) == 0){
						$setid = $id_model->getSeqId();
						$param["item_id"]=(int)substr($setid,-8);
						$result = $cl_mst_evaluation->insert($param);
					}else{
						$param["item_id"]=($$id);
						$result = $cl_mst_evaluation->update($param);
					}
				}
	}

	// 評価項目情報取得
	$arr_stmst = $cl_mst_ludder_eval->select_mst_item($target_level,$master_kind_id,$category_id,$group_id);

	// 存在チェック用フラグ
	for ($i = 0; $i < count($arr_stmst); $i++){
		$arr_stmst[$i]['exe'] = '1';
	}

	if($mode == 'rank_change')
	{
		for($i=0; $i<$dataCount; $i++) {

			$no = $i;
			$txt = $i."_TXT";
			$txt_mut = $i."_TXT_MUT";
			$id = $i."_ID";
			$level1 = $i."_LEVEL_1";
			$level2 = $i."_LEVEL_2";
			$level3 = $i."_LEVEL_3";
			$level4 = $i."_LEVEL_4";

			$arr_stmst[$i] = array('disp_order' => $$no , 'item_name' => $$txt , 'must_value' => $$txt_mut , 'item_name' => $$txt , 'item_id' => $$id, 'eval_disp_1_flg' => $$level1 , 'eval_disp_2_flg' => $$level2, 'eval_disp_3_flg' => $$level3 , 'eval_disp_4_flg' => $$level4);
		}

	// 行追加ボタン押下時
	} else if ($mode == 'ADD_LINE'){
		$j=0;
		$arr_stmst = array();

		for($i=0; $i<$dataCount; $i++) {

			$no = $i;
			$txt = $i."_TXT";
			$txt_mut = $i."_TXT_MUT";
			$id = $i."_ID";
			$level1 = $i."_LEVEL_1";
			$level2 = $i."_LEVEL_2";
			$level3 = $i."_LEVEL_3";
			$level4 = $i."_LEVEL_4";

			$arr_stmst[$j] = array('disp_order' => $$no , 'item_name' => $$txt , 'must_value' => $$txt_mut , 'item_name' => $$txt , 'item_id' => $$id, 'eval_disp_1_flg' => $$level1 , 'eval_disp_2_flg' => $$level2, 'eval_disp_3_flg' => $$level3 , 'eval_disp_4_flg' => $$level4);
			$j++;

			// 追加行
			if($i == $hdnrec)
			{
				//$setid = $id_model->getSeqId();
				$arr_stmst[$j] = array('disp_order' => $$no,'item_name' => "",  'must_value' => "" , 'item_id' => "", 'eval_disp_1_flg' => '1' , 'eval_disp_2_flg' => '1' , 'eval_disp_3_flg' => '1','eval_disp_4_flg' => '1');
				$j++;
			}
		}

	// 行削除ボタン押下時
	}else if ($mode == 'DEL_LINE'){
		$j=0;
		$arr_stmst = array();
		for($i=0; $i<$dataCount; $i++) {

			$no = $i;
			$txt = $i."_TXT";
			$txt_mut = $i."_TXT_MUT";
			$id = $i."_ID";
			$level1 = $i."_LEVEL_1";
			$level2 = $i."_LEVEL_2";
			$level3 = $i."_LEVEL_3";
			$level4 = $i."_LEVEL_4";
		
			if($i == $hdnrec)
			{
				// 論理削除
				$result = $cl_mst_evaluation->logical_delete($$id);
				continue;
			}

			$arr_stmst[$j] = array('disp_order' => $$no,'item_name' => $$txt, 'must_value' => $$txt_mut , 'item_id' => $$id, 'eval_disp_1_flg' => $$level1 ,'eval_disp_2_flg' => $$level2,'eval_disp_3_flg' => $$level3,'eval_disp_4_flg' => $$level4);
			$j++;
		}
	}

	// 空ﾚｺｰﾄﾞ表示(検索結果が０件の場合)
	if (count($arr_stmst) == 0){
			//$setid = $id_model->getSeqId();
			$arr_stmst[0] = array('disp_order' => '1' , 'item_name' => "",  'must_value' => "" , 'item_id' => "", 'eval_disp_1_flg' => '1' ,'eval_disp_2_flg' => '1' , 'eval_disp_3_flg' => '1' , 'eval_disp_4_flg' => '1');
	}


// DB切断
$mdb2->disconnect();
//========================================================================
//========================================================================

//====================================
// テンプレートマッピング
//====================================
$log->debug("テンプレートマッピング開始",__FILE__,__LINE__);

	$smarty->assign( 'target_level'			, $target_level					);
	$smarty->assign( 'master_kind_id'		, $master_kind_id				);
	$smarty->assign( 'category_id'			, $category_id					);
	$smarty->assign( 'group_id'				, $group_id						);

	$smarty->assign( 'common_module'	, $common_module					);
	$smarty->assign( 'moduleName'			, $moduleName					);
	$smarty->assign( 'hdnlevel'				, $hdnlevel						);
	$smarty->assign( 'hdnmst'				, $hdnmst						);
	$smarty->assign( 'cl_title'				, $cl_title						);
	$smarty->assign( 'session'				, $session						);
	$smarty->assign( 'regist_flg'			, $regist_flg					);
	$smarty->assign( 'wkfwctn_mnitm_str'	, $wkfwctn_mnitm_str			);
	$smarty->assign( 'data'					, $arr_stmst					);

	$smarty->assign( 'cmb_cate'				, $cmb_cate						);
	$smarty->assign( 'cmb_mst'				, $cmb_mst						);
	$smarty->assign( 'cmb_ctg'				, $cmb_ctg						);

	$smarty->assign( 'get_tpl_arr'			, $get_tpl_arr					);
	$smarty->assign( 'get_tpl'				, $get_tpl						);

$log->debug("テンプレートマッピング終了",__FILE__,__LINE__);

//====================================
// テンプレート出力
//====================================
$smarty->display(basename(__FILE__,'.php').".tpl");
$log->debug("テンプレート出力終了",__FILE__,__LINE__);

//====================================
// アプリケーションメニューHTML取得
//====================================
function get_wkfw_menuitem($session,$fname)
{
	ob_start();
	show_wkfw_menuitem($session,$fname,"");
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

$log->info(basename(__FILE__)." END");
$log->shutdown();
