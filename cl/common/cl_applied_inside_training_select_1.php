<?
/*
院内研修申込一覧
・院内研修申込変更を行う際に、申込変更を行う研修申請を選択する画面

【呼び出しパラメータ】
session　　　　　　　　　　セッションID。
emp_id 　　　　　　　　　　研修受講者の職員ID。

【戻り値】
training_apply_id　　　　　研修申込の院内研修ID
last_training_apply_id 　　有効な研修情報を持つ院内研修申請ID
inside_training_date 　　　研修申し込みの申請日
learn_memo 　　　　　　　　学びたいこと
head_nurse_recommend_flg 　師長推薦フラグ。1:師長推薦、0:通常。(省略時は0)
training_id　　　　　　　　研修ID
training_name　　　　　　　研修名
change_apply_date　　　　　変更申請日
change_reason　　　　　　　変更理由

schedule_list　　　　　　　研修日程情報の配列(key=表示順,value=日程情報オブジェクト)

※日程情報オブジェクト
plan_id　　　　　　　　　　日程ID
splan_date　　　　　　　　　開催日付(書式:"YYYY年MM月dd日(曜日)")
from_time　　　　　　　　　開催時刻From(書式:"HH:mm")
to_time　　　　　　　　　　開催時刻To(書式:"HH:mm")

*/
//TODO 変更申込申請中の場合は更に変更申込できない形にする必要がある。しかし、そのチェックは申請処理のときではないと完全ではない？
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cl_application_list.inc");
require_once("cl_application_common.ini");
require_once("cl_common.ini");
require_once("cl_title_name.ini");
require_once("cl_smarty_setting.ini");
require_once("cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once('MDB2.php');

//====================================
//リクエストパラメータ取得
//====================================
$common_module = $_POST["common_module"];
$session = $_POST["session"];
$emp_id = $_POST["emp_id"];

//====================================
//初期処理
//====================================
//ロガーをロード
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// データベース接続
//====================================
$mdb2 = MDB2::connect(CMX_DB_DSN);

//====================================
// モデルをロード
//====================================
require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php");
$empmst_model = new empmst_model($mdb2);

// セッションよりログインユーザーIDを取得
$arr_empmst = $empmst_model->select_login_emp($session);
$login_emp_id = $arr_empmst[0]["emp_id"];

require_once(dirname(__FILE__) . "/../model/search/cl_applied_inside_training_list_model.php");
$cl_applied_inside_training_list_model = new cl_applied_inside_training_list_model($mdb2,$login_emp_id);

//====================================
// データ取得
//====================================
//院内研修申込一覧を取得
$arr_training_list = $cl_applied_inside_training_list_model->get_applied_training_list($emp_id);

//====================================
// データベース切断
//====================================
$mdb2->disconnect();

//====================================================================================================
// HTML出力
//====================================================================================================
$page_title = "院内研修申請一覧";

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cl_title?>｜<?=$page_title?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<script type="text/javascript">

//====================================================================================================
//■イベント処理

//研修講座一覧から研修講座を選択した時の処理
function return_selected(training_apply_id)
{
	//呼び出し元画面にインターフェース関数が定義されている場合
	if(window.opener && !window.opener.closed && window.opener.cl_applied_inside_training_select_return)
	{
		//返却値を取得
		var result = m_return_data_list[training_apply_id];
		//親画面へ通知
		window.opener.cl_applied_inside_training_select_return(result);
	}
	//自画面を終了します。
	window.close();
}//function return_selected(training_apply_id)  END

//返却値配列(key:研修申込の院内研修ID、val:返却値)
var m_return_data_list = new Array();
<?
for($i = 0; $i < count($arr_training_list); $i++)
{
	//研修申込の院内研修ID
	$l_training_apply_id = $arr_training_list[$i]['training_apply_id'];
	//有効な研修情報を持つ院内研修申請ID
	$l_last_training_apply_id = $arr_training_list[$i]['last_training_apply_id'];
	//研修申し込みの申請日
	$l_inside_training_date = $arr_training_list[$i]['inside_training_date'];
	$l_inside_training_date_disp = get_date_disp_string($l_inside_training_date);
	//学びたいこと
	$l_learn_memo = $arr_training_list[$i]['learn_memo'];
	//師長推薦フラグ。1:師長推薦、0:通常。(省略時は0)
	$l_recommend_flag = $arr_training_list[$i]['recommend_flag'];

	//変更申請日
	$l_change_apply_date = $arr_training_list[$i]['change_apply_date'];
	//変更理由
	$l_change_reason = $arr_training_list[$i]['change_reason'];

	//研修ID
	$l_training_id = $arr_training_list[$i]['training_id'];
	//研修名
	$l_training_name = $arr_training_list[$i]['training_name'];
	//研修日程情報
	$l_plan_list = $arr_training_list[$i]['plan_list'];
	// 2012/11/20 Yamagawa add(s)
	// 年度
	$l_year = $arr_training_list[$i]['year'];
	// 2012/11/20 Yamagawa add(e)
?>
	m_return_data_list["<?=$l_training_apply_id?>"] = new Object();
	m_return_data_list["<?=$l_training_apply_id?>"].training_apply_id        = "<?=to_javascript_string($l_training_apply_id)?>";
	m_return_data_list["<?=$l_training_apply_id?>"].last_training_apply_id   = "<?=to_javascript_string($l_last_training_apply_id)?>";
	m_return_data_list["<?=$l_training_apply_id?>"].inside_training_date     = "<?=to_javascript_string($l_inside_training_date_disp)?>";
	m_return_data_list["<?=$l_training_apply_id?>"].learn_memo               = "<?=to_javascript_string($l_learn_memo)?>";
	m_return_data_list["<?=$l_training_apply_id?>"].change_apply_date        = "<?=to_javascript_string($l_change_apply_date)?>";
	m_return_data_list["<?=$l_training_apply_id?>"].change_reason            = "<?=to_javascript_string($l_change_reason)?>";
	m_return_data_list["<?=$l_training_apply_id?>"].head_nurse_recommend_flg = "<?=to_javascript_string($l_recommend_flag)?>";
	m_return_data_list["<?=$l_training_apply_id?>"].training_id              = "<?=to_javascript_string($l_training_id)?>";
	m_return_data_list["<?=$l_training_apply_id?>"].training_name            = "<?=to_javascript_string($l_training_name)?>";
	// 2012/11/20 Yamagawa add(s)
	m_return_data_list["<?=$l_training_apply_id?>"].year                     = "<?=to_javascript_string($l_year)?>";
	// 2012/11/20 Yamagawa add(e)
	m_return_data_list["<?=$l_training_apply_id?>"].schedule_list            = new Array();
<?

	// 2012/11/05 Yamagawa add(s)
	$reported_plan_ids = '';
	// 2012/11/05 Yamagawa add(e)

	for($i_plan = 0; $i_plan < count($l_plan_list); $i_plan++)
	{
		//日程ID
		$l_plan_id   = $l_plan_list[$i_plan]['plan_id'];
		//研修回数
		$l_training_time = $l_plan_list[$i_plan]['training_time'];
		//最終フラグ
		$l_final_flg = $l_plan_list[$i_plan]['final_flg'];
		//開催日付
		$l_plan_date = $l_plan_list[$i_plan]['plan_date'];
		//開催時刻From
		$l_from_time = $l_plan_list[$i_plan]['from_time'];
		//開催時刻To
		$l_to_time   = $l_plan_list[$i_plan]['to_time'];
		// 2012/11/05 Yamagawa add(s)
		if ($l_plan_list[$i_plan]['reported_flg'] == '1') {
			$reported_plan_id[] = $l_plan_id;
		}
		// 2012/11/05 Yamagawa add(e)
		//日付をフォーマット
		$l_plan_date_disp = get_date_disp_string($l_plan_date);
		$l_from_time_disp = get_time_disp_string($l_from_time);
		$l_to_time_disp   = get_time_disp_string($l_to_time);
		// 2012/04/12 Yamagawa add(s)
		// 「23:59」は「24:00」に変換（暫定対応）
		if ($l_from_time_disp == "23:59") {
			$l_from_time_disp = "24:00";
		}
		if ($l_to_time_disp == "23:59") {
			$l_to_time_disp = "24:00";
		}
		// 2012/04/12 Yamagawa add(e)
?>
		m_return_data_list["<?=$l_training_apply_id?>"].schedule_list[<?=$i_plan?>] = new Object();
		m_return_data_list["<?=$l_training_apply_id?>"].schedule_list[<?=$i_plan?>].plan_id   = "<?=to_javascript_string($l_plan_id)?>";
		m_return_data_list["<?=$l_training_apply_id?>"].schedule_list[<?=$i_plan?>].training_time = "<?=to_javascript_string($l_training_time)?>";
		m_return_data_list["<?=$l_training_apply_id?>"].schedule_list[<?=$i_plan?>].final_flg = "<?=to_javascript_string($l_final_flg)?>";
		m_return_data_list["<?=$l_training_apply_id?>"].schedule_list[<?=$i_plan?>].plan_date = "<?=to_javascript_string($l_plan_date_disp)?>";
		m_return_data_list["<?=$l_training_apply_id?>"].schedule_list[<?=$i_plan?>].from_time = "<?=to_javascript_string($l_from_time_disp)?>";
		m_return_data_list["<?=$l_training_apply_id?>"].schedule_list[<?=$i_plan?>].to_time   = "<?=to_javascript_string($l_to_time_disp)?>";
<?
	}
	// 2012/11/05 Yamagawa add(s)
	$reported_plan_ids = implode(',', $reported_plan_id);
?>
	m_return_data_list["<?=$l_training_apply_id?>"].reported_id = "<?=to_javascript_string($reported_plan_ids)?>";
<?
	// 2012/11/05 Yamagawa add(e)
}//for($i = 0; $i < count($arr_training_list); $i++) END
?>

//====================================================================================================
//■行のハイライト制御

//class属性が指定されたclass_nameのTDタグをハイライトします。
function highlightCells(class_name)
{
	changeCellsColor(class_name, '#ffff66');
}

//class属性が指定されたclass_nameのTDタグのハイライトを解除します。
function dehighlightCells(class_name)
{
	changeCellsColor(class_name, '');
}

//highlightCells()、dehighlightCells()専用関数
function changeCellsColor(class_name, color)
{
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++)
	{
		if (cells[i].className != class_name)
		{
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}

</script>
<style type="text/css">

table.list {border-collapse:collapse;}
table.list td {border:#A0D25A solid 1px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">

<form name="postback_form" method="post" action="cl_common_call.php" target="_self">
<input type="hidden" id="common_module"            name="common_module"            value="<?=$common_module ?>">
<input type="hidden" id="session"                  name="session"                  value="<?=$session ?>">
<input type="hidden" id="emp_id"                   name="emp_id"                   value="<?=$emp_id ?>">

<center>

<!-- ヘッダー START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$page_title?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<!-- ヘッダー END -->

<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<!-- -------------------------------------------------------------------------------------------------------------- -->
<!-- ■研修申込一覧 -->
<!-- -------------------------------------------------------------------------------------------------------------- -->

<!-- 研修申込一覧表 START  -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">

<!-- 研修申込一覧 ヘッダー START  -->
<tr bgcolor="#E5F6CD">
<td width="140" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
<td width="110" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">院内研修番号</font></td>
<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">院内研修名</font></td>
<td width="250" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日程</font></td>
</tr>
<!-- 研修申込一覧 ヘッダー END -->

<!-- 研修申込一覧 データ START -->
<?
for($i = 0; $i < count($arr_training_list); $i++)
{
	//配列情報を展開
	$l_training_apply_id = $arr_training_list[$i]['training_apply_id'];
	$l_last_training_apply_id = $arr_training_list[$i]['last_training_apply_id'];
	$l_training_id = $arr_training_list[$i]['training_id'];
	$l_inside_training_date = $arr_training_list[$i]['inside_training_date'];
	$l_training_name = $arr_training_list[$i]['training_name'];
	//2012/05/10 K.Fujii ins(s)
	$l_training_name = str_replace('\'\'',   '\'', $l_training_name);
	$l_training_name = str_replace('\\\\',   '\\',     $l_training_name);
	$l_training_name = str_replace('&lt;',   '<',      $l_training_name);
	$l_training_name = str_replace('&gt;',   '>',      $l_training_name);
	//2012/05/10 K.Fujii ins(e)
	$l_recommend_flag = $arr_training_list[$i]['recommend_flag'];
	$l_plan_list = $arr_training_list[$i]['plan_list'];

	$l_plan_date_time_disp = "";
	for($i_plan = 0; $i_plan < count($l_plan_list); $i_plan++)//
	{
		$l_plan_id = $l_plan_list[$i_plan]['plan_id'];
		$l_training_time = $l_plan_list[$i_plan]['training_time'];
		$l_final_flg = $l_plan_list[$i_plan]['final_flg'];
		$l_plan_date = $l_plan_list[$i_plan]['plan_date'];
		$l_from_time = $l_plan_list[$i_plan]['from_time'];
		$l_to_time = $l_plan_list[$i_plan]['to_time'];

		//	//日時　表示用
		$l_training_time_disp = get_training_time_disp_string($l_training_time, $l_final_flg);
		$l_plan_date_disp = get_date_disp_string($l_plan_date);
		$l_from_time_disp = get_time_disp_string($l_from_time);
		$l_to_time_disp   = get_time_disp_string($l_to_time);
		// 2012/04/12 Yamagawa add(s)
		// 「23:59」は「24:00」に変換（暫定対応）
		if ($l_from_time_disp == "23:59") {
			$l_from_time_disp = "24:00";
		}
		if ($l_to_time_disp == "23:59") {
			$l_to_time_disp = "24:00";
		}
		// 2012/04/12 Yamagawa add(e)
		if($l_plan_date_time_disp != "")
		{
			$l_plan_date_time_disp = $l_plan_date_time_disp . "\n";
		}
		$l_plan_date_time_disp = $l_plan_date_time_disp . $l_training_time_disp . " " . $l_plan_date_disp . " " . $l_from_time_disp . " - " . $l_to_time_disp;
	}

	$l_inside_training_date_disp = get_date_disp_string($l_inside_training_date);

	//ハイライトするセルのクラス属性値
	$l_highlight_class_name = "highlight_class_".$i;

?>
<tr>
<td align="center" class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="return_selected('<?=$l_training_apply_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_inside_training_date_disp))?></font></td>
<td align="center" class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="return_selected('<?=$l_training_apply_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_training_id))?></font></td>
<td                class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="return_selected('<?=$l_training_apply_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br($l_training_name)?></font></td>
<td align="center" class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="return_selected('<?=$l_training_apply_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_plan_date_time_disp))?></font></td>
</tr>
<?
}
?>
<!-- 研修申込一覧 データ END -->

</table>
<!-- 研修申込一覧表 END  -->

</center>
</form>
</body>
</html>
<?

//====================================
// 終了処理
//====================================
$log->info(basename(__FILE__)." END");
$log->shutdown();

//====================================================================================================
// ローカル関数
//====================================================================================================

/**
 * 研修回数を表示用文字列に変換します。
 *
 * @param $training_time DBより取得した日付データ。「YYYY-MM-DD」形式
 * @param $final_flg DBより取得した日付データ。「YYYY-MM-DD」形式
 * @return 日付の表示用文字列
 */
function get_training_time_disp_string($training_time,$final_flg)
{
	$ret = "最終回";
	if($final_flg != 1)
	{
		$ret = "第".$training_time."回";
	}
	return $ret;
}

/**
 * 日付を表示用文字列に変換します。「YYYY年MM月DD日(曜日)」
 *
 * @param $db_format_date DBより取得した日付データ。「YYYY-MM-DD」形式
 * @return 日付の表示用文字列
 */
function get_date_disp_string($db_format_date)
{
	$i_date = strtotime($db_format_date);
	$weekday = array( "日", "月", "火", "水", "木", "金", "土" );
	$ret = date("Y",$i_date)."年".date("m",$i_date)."月".date("d",$i_date)."日(".$weekday[date("w", $i_date)].")";
	return $ret;
}

/**
 * 時間を表示用文字列に変換します。「HH:mm」形式
 *
 * @param $db_format_time DBより取得した時間データ。「HH:mm:ss」形式
 * @return 時間の表示用文字列
 */
function get_time_disp_string($db_format_time)
{
	$ret = substr($db_format_time, 0,5);
	return $ret;
}

/**
 * JavaScript出力用に文字列変換します。
 *
 * @param $str 出力する文字列
 */
function to_javascript_string($str)
{
	$ret = $str;
	$ret = str_replace("\"","\\\"",$ret);
	$ret = str_replace("\r\n","\\n",$ret);
	$ret = str_replace("\r","\\n",$ret);
	$ret = str_replace("\n","\\n",$ret);
	//2012/05/10 K.Fujii ins(s)
	$ret = str_replace("''","'",$ret);
	//2012/05/10 K.fujii ins(e)
	return $ret;
}

?>