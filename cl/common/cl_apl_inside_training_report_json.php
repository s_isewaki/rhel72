<?php
//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");
require_once('MDB2.php');
require_once(dirname(__FILE__) . "/../model/template/cl_apl_inside_training_report_model.php");

$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

$apply_id=$_GET["apply_id"];

$log->debug('apply_id:'.$apply_id,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 申請書データ取得処理を呼び出す
//------------------------------------------------------------------------------
$json=getJsonApplyData($mdb2, $apply_id);


$log->debug('$json:'.$json,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベース接続を閉じる
//------------------------------------------------------------------------------
$log->debug("データベース切断 START",__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);

header("Content-Type: application/json; charset=EUC-JP");
echo $json;

/**
 * 申請書データ取得
 */
function getJsonApplyData($mdb2, $apply_id){
	global $log;
	$log->debug(__FUNCTION__." START");

	//--------------------------------------------------------------------------
	// 院内研修報告テーブルデータ取得
	//--------------------------------------------------------------------------
	$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
	$model = new cl_apl_inside_training_report_model($mdb2, $user);
	$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

	$log->debug("研修報告データ読込み Start",__FILE__,__LINE__);
	$data=$model->select($apply_id);
	$log->debug("研修報告データ読込み End",__FILE__,__LINE__);
//$data["teacher_comment"	] = ereg_replace("\r|\n","\\\\n",$data["teacher_comment"	]);

	//$data["training_learned"] = ereg_replace("\r|\n","\\\\n",$data["training_learned"	]);
	//$data["boss_comment"	] = ereg_replace("\r|\n","\\\\n",$data["boss_comment"	]);
	//$data["chief_comment"	] = ereg_replace("\r|\n","\\\\n",$data["chief_comment"	]);
	//$data["sponsor_comment"	] = ereg_replace("\r|\n","\\\\n",$data["sponsor_comment"	]);
	//$data["teacher_comment"	] = ereg_replace("\r|\n","\\\\n",$data["teacher_comment"	]);


	// 2012/05/24 M.Yamagawa upd(s)
	/*
	$data["training_learned"]  = str_replace("\r\n",   "\\n",$data["training_learned"]);	// 答案
	$data["boss_comment"]      = str_replace("\r\n",   "\\n",$data["boss_comment"]);		// 所属長コメント
	$data["sponsor_comment"]   = str_replace("\r\n",   "\\n",$data["sponsor_comment"]);		// 主催者コメント
	$data["teacher_comment"]   = str_replace("\r\n",   "\\n",$data["teacher_comment"]);		// 講師コメント
	//2012/05/11 K.Fujii upd(s)
	//$data["remarks"]   = str_replace("\n",   "\\n",$data["remarks"]);						// 開催日一覧・備考
	$data["remarks"]   = str_replace("\r\n",   "\\n",$data["remarks"]);						// 開催日一覧・備考
	//2012/05/11 K.Fujii upd(e)
	*/
	$data["training_learned"] = str_replace("\r\n" , "\n"  , $data["training_learned"]);	// 答案
	$data["training_learned"] = str_replace("\r"   , "\n"  , $data["training_learned"]);
	$data["training_learned"] = str_replace("\n"   , "\\n" , $data["training_learned"]);

	$data["boss_comment"]     = str_replace("\r\n" , "\n"  , $data["boss_comment"]    );	// 所属長コメント
	$data["boss_comment"]     = str_replace("\r"   , "\n"  , $data["boss_comment"]    );
	$data["boss_comment"]     = str_replace("\n"   , "\\n" , $data["boss_comment"]    );

	$data["sponsor_comment"]  = str_replace("\r\n" , "\n"  , $data["sponsor_comment"] );	// 主催者コメント
	$data["sponsor_comment"]  = str_replace("\r"   , "\n"  , $data["sponsor_comment"] );
	$data["sponsor_comment"]  = str_replace("\n"   , "\\n" , $data["sponsor_comment"] );

	$data["teacher_comment"]  = str_replace("\r\n" , "\n"  , $data["teacher_comment"] );	// 講師コメント
	$data["teacher_comment"]  = str_replace("\r"   , "\n"  , $data["teacher_comment"] );
	$data["teacher_comment"]  = str_replace("\n"   , "\\n" , $data["teacher_comment"] );

	$data["remarks"]          = str_replace("\r\n" , "\n"  , $data["remarks"]         );	// 開催日一覧・備考
	$data["remarks"]          = str_replace("\r"   , "\n"  , $data["remarks"]         );
	$data["remarks"]          = str_replace("\n"   , "\\n" , $data["remarks"]         );
	// 2012/05/24 M.Yamagawa upd(e)

	/*

	for($k=0;$k<count($report_data);$k++){

		//foreach( $report_data[$k] as $key => $value ) {
		//	$log->debug('★★★★★★$report_data['.$k.']['.$key.']:'.$report_data[$k][$key],__FILE__,__LINE__);

			$report_data[$k]["training_learned"	] = ereg_replace("\r|\n","\\\\n",$report_data[$k]["training_learned"	]);
			$report_data[$k]["boss_comment"		] = ereg_replace("\r|\n","\\\\n",$report_data[$k]["boss_comment"	]);
			$report_data[$k]["chief_comment"	] = ereg_replace("\r|\n","\\\\n",$report_data[$k]["chief_comment"	]);
			$report_data[$k]["sponsor_comment"	] = ereg_replace("\r|\n","\\\\n",$report_data[$k]["sponsor_comment"	]);
			$report_data[$k]["teacher_comment"	] = ereg_replace("\r|\n","\\\\n",$report_data[$k]["teacher_comment"	]);


			//$log->debug('■■■$report_data['.$k.']["training_learned"	]:'.$report_data[$k]["training_learned"	],__FILE__,__LINE__);
			//$log->debug('■■■$report_data['.$k.']["boss_comment"	]:'.$report_data[$k]["boss_comment"	],__FILE__,__LINE__);
			//$log->debug('■■■$report_data['.$k.']["chief_comment"		]:'.$report_data[$k]["chief_comment"	],__FILE__,__LINE__);
			//$log->debug('■■■$report_data['.$k.']["sponsor_comment"	]:'.$report_data[$k]["sponsor_comment"	],__FILE__,__LINE__);
			//$log->debug('■■■$report_data['.$k.']["teacher_comment"	]:'.$report_data[$k]["teacher_comment"	],__FILE__,__LINE__);
		//}
	}
	*/



	$log->debug("研修申請報告データ読込み End",__FILE__,__LINE__);

	//--------------------------------------------------------------------------
	// 院内研修申請テーブルデータ取得
	//--------------------------------------------------------------------------
	$log->debug("研修申請データ読込み Start　　training_apply_id = ".$data['training_apply_id'] ,__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/template/cl_apl_inside_training_apply_model.php");
	$log->debug("研修申請 require End",__FILE__,__LINE__);

	$cl_apl_inside_training_apply_model = new cl_apl_inside_training_apply_model($mdb2, $user);
	$log->debug("研修申請 インスタンス OK",__FILE__,__LINE__);

	$log->debug("研修申請データ読込み Start　　training_apply_id = ".$data['training_apply_id'],__FILE__,__LINE__);
	$training_apply_data= $cl_apl_inside_training_apply_model->select($data['training_apply_id']);


	//$training_apply_data["change_reason"		] = ereg_replace("\r|\n","\\\\n",$training_apply_data["change_reason"	]);
	//$training_apply_data["training_name"		] = ereg_replace("\r|\n","\\\\n",$training_apply_data["training_name"	]);
	//$training_apply_data["inside_training_id"	] = ereg_replace("\r|\n","\\\\n",$training_apply_data["inside_training_id"	]);
	//$training_apply_data["training_plan_id"		] = ereg_replace("\r|\n","\\\\n",$training_apply_data["training_plan_id"	]);
	//$training_apply_data["training_purpose"		] = ereg_replace("\r|\n","\\\\n",$training_apply_data["training_purpose"	]);
	//$training_apply_data["training_slogan"		] = ereg_replace("\r|\n","\\\\n",$training_apply_data["training_slogan"	]);
	//$training_apply_data["training_contents"	] = ereg_replace("\r|\n","\\\\n",$training_apply_data["training_contents"	]);

	$log->debug("研修申請データ読込み End   plan_id = ". $training_apply_data['plan_id'],__FILE__,__LINE__);

	//--------------------------------------------------------------------------
	// 日程情報データ取得
	//--------------------------------------------------------------------------
	$log->debug("schedule modelインスタンス作成開始",__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_schedule_model.php");
	$log->debug("研修日程 require End",__FILE__,__LINE__);

	$cl_mst_inside_training_schedule_model = new cl_mst_inside_training_schedule_model($mdb2, $user);
	$log->debug("研修日程 インスタンス OK",__FILE__,__LINE__);

	$log->debug("plan_id = ".$training_apply_data['plan_id'],__FILE__,__LINE__);
	$arr_plan = $cl_mst_inside_training_schedule_model->getList($data['plan_id']);
	// 2012/04/12 Yamagawa add(s)
	// 「23:59」は「24:00」に変換（暫定対応）
	for($i = 0; $i < count($arr_plan); $i++) {
		if (substr($arr_plan[$i]['from_time'],0,5) == "23:59") {
			$arr_plan[$i]['from_time'] = "24:00";
		}
		if (substr($arr_plan[$i]['to_time'],0,5) == "23:59") {
			$arr_plan[$i]['to_time'] = "24:00";
		}
	}
	// 2012/04/12 Yamagawa add(e)

	//$training_apply_data["change_reason"	] = ereg_replace("\r|\n","\\\\n",$training_apply_data["change_reason"	]);


	$log->debug("plan count = ". count($arr_plan),__FILE__,__LINE__);

	$log->debug("$arr_plan = ". $arr_plan,__FILE__,__LINE__);


	//--------------------------------------------------------------------------
	// 承認情報取得
	//--------------------------------------------------------------------------

	$log->debug("apply_id *= ". $data['apply_id'],__FILE__,__LINE__);
	$apv_data = $model->apv_data_select_report_rec($data['apply_id']);
	$log->debug("$apv_data = ". $apv_data,__FILE__,__LINE__);

	//--------------------------------------------------------------------------
	// 院内研修申請テーブルデータセット
	//--------------------------------------------------------------------------
	//	$data["training_apply_data"] = $training_apply_data;
	//$data["training_apply_id"]	 = $training_apply_data["training_apply_id"];
	$data["training_name"]       = $training_apply_data["training_name"];
	$data["inside_training_id"]  = $training_apply_data["inside_training_id"];
	$data["training_plan_id"]    = $training_apply_data["training_plan_id"];
	$data["training_purpose"]    = str_replace("\r\n",   "\\n",$training_apply_data["training_purpose"]);
	$data["training_slogan"]     = str_replace("\r\n",   "\\n",$training_apply_data["training_slogan"]);
	$data["training_contents"]   = str_replace("\r\n",   "\\n",$training_apply_data["training_contents"]);

	$data["schedule_list"] = $arr_plan;
	$data["apv_data"] = $apv_data;

	// 2012/11/02 Yamagawa add(s)
	//--------------------------------------------------------------------------
	// 申請中院内研修変更申込件数取得
	//--------------------------------------------------------------------------
	$data['apply_count'] = $cl_apl_inside_training_apply_model->get_count_by_first_apply_id($data['training_apply_id']);
	// 2012/11/02 Yamagawa add(e)

	//--------------------------------------------------------------------------
	// 院内研修アンケートテーブルデータセット
	//--------------------------------------------------------------------------
	if($data["final_flg"] == "1"){
		$log->debug("院内研修アンケートデータ読込み Start",__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/template/cl_apl_inside_training_answer_model.php");
		$log->debug("院内研修アンケート require End",__FILE__,__LINE__);

		$cl_apl_inside_training_answer_model = new cl_apl_inside_training_answer_model($mdb2, $user);
		$log->debug("院内研修アンケート インスタンス OK",__FILE__,__LINE__);

		$log->debug("院内研修アンケートデータ読込み Start　　apply_id = ".$apply_id,__FILE__,__LINE__);
		$answer_data= $cl_apl_inside_training_answer_model->select_by_apply_id($apply_id);
		$log->debug("院内研修アンケートデータ読込み End",__FILE__,__LINE__);


		$answer_data["training_plan_degree_reason"	] = str_replace("\r\n","\\n",$answer_data["training_plan_degree_reason"	]);
		$answer_data["training_method_reason"		] = str_replace("\r\n","\\n",$answer_data["training_method_reason"		]);
		$answer_data["training_contents_reason"		] = str_replace("\r\n","\\n",$answer_data["training_contents_reason"	]);
		$answer_data["training_practice_reason"		] = str_replace("\r\n","\\n",$answer_data["training_practice_reason"	]);
		$answer_data["point_notice"					] = str_replace("\r\n","\\n",$answer_data["point_notice"				]);


		/*
		for($l=0;$l<count($answer_data);$l++){

			//foreach( $answer_data[$l] as $key => $value ) {
				//$log->debug('★★★★★★$answer_data['.$l.']['.$key.']:'.$answer_data[$l][$key],__FILE__,__LINE__);

				$answer_data_tmp = $answer_data[$l];
				$answer_data_tmp["training_plan_degree_reason"	] = ereg_replace("\r|\n","\\\\n",$answer_data_tmp["training_plan_degree_reason"	]);
				$answer_data_tmp["training_method_reason"		] = ereg_replace("\r|\n","\\\\n",$answer_data_tmp["training_method_reason"		]);
				$answer_data_tmp["training_contents_reason"		] = ereg_replace("\r|\n","\\\\n",$answer_data_tmp["training_contents_reason"	]);
				$answer_data_tmp["training_practice_reason"		] = ereg_replace("\r|\n","\\\\n",$answer_data_tmp["training_practice_reason"	]);
				$answer_data_tmp["point_notice"					] = ereg_replace("\r|\n","\\\\n",$answer_data_tmp["point_notice"				]);


				//$log->debug('■■■$report_data['.$k.']["training_learned"	]:'.$report_data[$k]["training_learned"	],__FILE__,__LINE__);
				//$log->debug('■■■$report_data['.$k.']["boss_comment"	]:'.$report_data[$k]["boss_comment"	],__FILE__,__LINE__);
				//$log->debug('■■■$report_data['.$k.']["chief_comment"		]:'.$report_data[$k]["chief_comment"	],__FILE__,__LINE__);
				//$log->debug('■■■$report_data['.$k.']["sponsor_comment"	]:'.$report_data[$k]["sponsor_comment"	],__FILE__,__LINE__);
				//$log->debug('■■■$report_data['.$k.']["teacher_comment"	]:'.$report_data[$k]["teacher_comment"	],__FILE__,__LINE__);
			//}
		}
		*/



		$data["answer_data"] = $answer_data;
	}

	//$json=urlencode(create_json($data));
	$json=create_json($data);

	$log->debug(__FUNCTION__." END");

	return $json;
}

