<?
/*
院内研修課題一覧
・院内研修課題提出を行う際に、提出する研修課題を選択する画面


【呼び出しパラメータ】
session　　　　　　　　　　セッションID。
emp_id 　　　　　　　　　　研修受講者の職員ID。

【戻り値】
training_apply_id　　　　　研修申込の院内研修ID
training_id　　　　　　　　研修ID
training_name　　　　　　　研修名
schedule_list　　　　　　　研修日程情報の配列(key=表示順,value=日程情報オブジェクト)
theme_id 　　　　　　　　　課題ID
theme_division 　　　　　　課題区分
theme_contents 　　　　　　課題内容

※日程情報オブジェクト
plan_id　　　　　　　　　　日程ID
training_time　　　　　　　研修回数
final_flg　　　　　　　　　最終フラグ
plan_date　　　　　　　　　開催日付(書式:"YYYY年MM月dd日(曜日)")
from_time　　　　　　　　　開催時刻From(書式:"HH:mm")
to_time　　　　　　　　　　開催時刻To(書式:"HH:mm")
*/


//TODO 既に提出ずみの場合に対しては特に制御を行っていない。運用でカバーでOK？

require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cl_application_list.inc");
require_once("cl_application_common.ini");
require_once("cl_common.ini");
require_once("cl_title_name.ini");
require_once("cl_smarty_setting.ini");
require_once("cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once('MDB2.php');



//====================================
//リクエストパラメータ取得
//====================================
$common_module = $_POST["common_module"];
$session = $_POST["session"];
$emp_id = $_POST["emp_id"];



//====================================
//初期処理
//====================================

//ロガーをロード
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");


//画面名
$fname = $PHP_SELF;


//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//権限チェック
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}



//====================================
// データベース接続
//====================================
$mdb2 = MDB2::connect(CMX_DB_DSN);



//====================================
// モデルをロード
//====================================

require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php");
$empmst_model = new empmst_model($mdb2);

// セッションよりログインユーザーIDを取得
$arr_empmst = $empmst_model->select_login_emp($session);
$login_emp_id = $arr_empmst[0]["emp_id"];

require_once(dirname(__FILE__) . "/../model/search/cl_applied_inside_training_theme_list_model.php");
$cl_applied_inside_training_theme_list_model = new cl_applied_inside_training_theme_list_model($mdb2,$login_emp_id);



//====================================
// データ取得
//====================================

//院内研修申込一覧を取得
$arr_training_list = $cl_applied_inside_training_theme_list_model->get_applied_training_theme_list($emp_id);



//====================================
// データベース切断
//====================================
$mdb2->disconnect();



//====================================================================================================
// HTML出力
//====================================================================================================

$page_title = "院内研修課題一覧";

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cl_title?>｜<?=$page_title?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<script type="text/javascript">


//====================================================================================================
//■イベント処理

//研修講座一覧から研修講座を選択した時の処理
function return_selected(training_apply_id,theme_id)
{
	//呼び出し元画面にインターフェース関数が定義されている場合
	if(window.opener && !window.opener.closed && window.opener.cl_applied_inside_training_theme_select_return)
	{
		//返却値を取得
		var array_key = training_apply_id + '_' + theme_id
		var result = m_return_data_list[array_key];
		//親画面へ通知
		window.opener.cl_applied_inside_training_theme_select_return(result);
	}
	//自画面を終了します。
	window.close();
}//function return_selected(training_apply_id)  END


//返却値配列(key:研修申込の院内研修ID_課題ID、val:返却値)
var m_return_data_list = new Array();
<?
for($i = 0; $i < count($arr_training_list); $i++)
{
	//研修申込の院内研修ID
	$l_training_apply_id = $arr_training_list[$i]['training_apply_id'];
	//研修ID
	$l_training_id = $arr_training_list[$i]['training_id'];
	//研修名
	$l_training_name = $arr_training_list[$i]['training_name'];
	//研修課題情報
	$l_theme_list = $arr_training_list[$i]['theme_list'];
	//研修日程情報
	$l_plan_list = $arr_training_list[$i]['plan_list'];

	for($i_theme = 0; $i_theme < count($l_theme_list); $i_theme++)
	{
		//課題ID
		$l_theme_id   = $l_theme_list[$i_theme]['theme_id'];
		//課題区分
		$l_theme_division   = $l_theme_list[$i_theme]['theme_division'];
		//課題内容
		$l_theme_contents   = $l_theme_list[$i_theme]['theme_contents'];
		//配列キー
		$l_array_key = $l_training_apply_id.'_'.$l_theme_id;
?>
		m_return_data_list["<?=$l_array_key?>"] = new Object();
		m_return_data_list["<?=$l_array_key?>"].training_apply_id        = "<?=to_javascript_string($l_training_apply_id)?>";
		m_return_data_list["<?=$l_array_key?>"].training_id              = "<?=to_javascript_string($l_training_id)?>";
		m_return_data_list["<?=$l_array_key?>"].training_name            = "<?=to_javascript_string($l_training_name)?>";
		m_return_data_list["<?=$l_array_key?>"].theme_id                 = "<?=to_javascript_string($l_theme_id)?>";
		m_return_data_list["<?=$l_array_key?>"].theme_division           = "<?=to_javascript_string($l_theme_division)?>";
		m_return_data_list["<?=$l_array_key?>"].theme_contents           = "<?=to_javascript_string($l_theme_contents)?>";
		m_return_data_list["<?=$l_array_key?>"].schedule_list            = new Array();
<?
		for($i_plan = 0; $i_plan < count($l_plan_list); $i_plan++)
		{
			//日程ID
			$l_plan_id   = $l_plan_list[$i_plan]['plan_id'];
			//研修回数
			$l_training_time = $l_plan_list[$i_plan]['training_time'];
			//最終フラグ
			$l_final_flg = $l_plan_list[$i_plan]['final_flg'];
			//開催日付
			$l_plan_date = $l_plan_list[$i_plan]['plan_date'];
			//開催時刻From
			$l_from_time = $l_plan_list[$i_plan]['from_time'];
			//開催時刻To
			$l_to_time   = $l_plan_list[$i_plan]['to_time'];
			//日付をフォーマット
			$l_plan_date_disp = get_date_disp_string($l_plan_date);
			$l_from_time_disp = get_time_disp_string($l_from_time);
			$l_to_time_disp   = get_time_disp_string($l_to_time);
			// 2012/04/12 Yamagawa add(s)
			// 「23:59」は「24:00」に変換（暫定対応）
			if ($l_from_time_disp == "23:59") {
				$l_from_time_disp = "24:00";
			}
			if ($l_to_time_disp == "23:59") {
				$l_to_time_disp = "24:00";
			}
			// 2012/04/12 Yamagawa add(e)
?>
			m_return_data_list["<?=$l_array_key?>"].schedule_list[<?=$i_plan?>] = new Object();
			m_return_data_list["<?=$l_array_key?>"].schedule_list[<?=$i_plan?>].plan_id       = "<?=to_javascript_string($l_plan_id)?>";
			m_return_data_list["<?=$l_array_key?>"].schedule_list[<?=$i_plan?>].training_time = "<?=to_javascript_string($l_training_time)?>";
			m_return_data_list["<?=$l_array_key?>"].schedule_list[<?=$i_plan?>].final_flg     = "<?=to_javascript_string($l_final_flg)?>";
			m_return_data_list["<?=$l_array_key?>"].schedule_list[<?=$i_plan?>].plan_date     = "<?=to_javascript_string($l_plan_date_disp)?>";
			m_return_data_list["<?=$l_array_key?>"].schedule_list[<?=$i_plan?>].from_time     = "<?=to_javascript_string($l_from_time_disp)?>";
			m_return_data_list["<?=$l_array_key?>"].schedule_list[<?=$i_plan?>].to_time       = "<?=to_javascript_string($l_to_time_disp)?>";
<?
		}
	}
}//for($i = 0; $i < count($arr_training_list); $i++) END
?>



//====================================================================================================
//■行のハイライト制御

//class属性が指定されたclass_nameのTDタグをハイライトします。
function highlightCells(class_name)
{
	changeCellsColor(class_name, '#ffff66');
}

//class属性が指定されたclass_nameのTDタグのハイライトを解除します。
function dehighlightCells(class_name)
{
	changeCellsColor(class_name, '');
}

//highlightCells()、dehighlightCells()専用関数
function changeCellsColor(class_name, color)
{
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++)
	{
		if (cells[i].className != class_name)
		{
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}



</script>
<style type="text/css">

table.list {border-collapse:collapse;}
table.list td {border:#A0D25A solid 1px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">

<form name="postback_form" method="post" action="cl_common_call.php" target="_self">
<input type="hidden" id="common_module"            name="common_module"            value="<?=$common_module ?>">
<input type="hidden" id="session"                  name="session"                  value="<?=$session ?>">
<input type="hidden" id="emp_id"                   name="emp_id"                   value="<?=$emp_id ?>">


<center>


<!-- ヘッダー START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$page_title?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<!-- ヘッダー END -->


<img src="img/spacer.gif" alt="" width="1" height="5"><br>

<!-- -------------------------------------------------------------------------------------------------------------- -->
<!-- ■研修申込一覧 -->
<!-- -------------------------------------------------------------------------------------------------------------- -->


<!-- 研修申込一覧表 START  -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">

<!-- 研修申込一覧 ヘッダー START  -->
<tr bgcolor="#E5F6CD">
<td width="140" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
<td width="110" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">院内研修番号</font></td>
<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">院内研修名</font></td>
<td width="90" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">課題区分</font></td>
<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">課題内容</font></td>
<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">提出方法等</font></td>
<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">提出期限</font></td>
<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">提出先</font></td>
</tr>
<!-- 研修申込一覧 ヘッダー END -->


<!-- 研修申込一覧 データ START -->
<?
for($i = 0; $i < count($arr_training_list); $i++)
{
	//配列情報を展開
	$l_training_apply_id = $arr_training_list[$i]['training_apply_id'];
	$l_inside_training_date = $arr_training_list[$i]['inside_training_date'];
	$l_training_id = $arr_training_list[$i]['training_id'];
	$l_training_name = $arr_training_list[$i]['training_name'];
	$l_theme_list = $arr_training_list[$i]['theme_list'];

	$l_inside_training_date_disp = get_date_disp_string($l_inside_training_date);
	$l_training_id_disp = $l_training_id;
	$l_training_name_disp = $l_training_name;

	for($i_theme = 0; $i_theme < count($l_theme_list); $i_theme++)
	{
		$l_theme_id         = $l_theme_list[$i_theme]['theme_id'];
		$l_theme_division   = $l_theme_list[$i_theme]['theme_division'];
		$l_theme_contents   = $l_theme_list[$i_theme]['theme_contents'];
		$l_theme_method     = $l_theme_list[$i_theme]['theme_method'];
		$l_theme_deadline   = $l_theme_list[$i_theme]['theme_deadline'];
		$l_theme_present    = $l_theme_list[$i_theme]['theme_present'];

		if($i_theme != 0)
		{
			$l_inside_training_date_disp = "";
			$l_training_id_disp = "";
			$l_training_name_disp = "";
		}

		$l_theme_division_disp = "";
		switch ($l_theme_division)
		{
			case 0:
				$l_theme_division_disp = "研修前課題";
				break;
			case 1:
				$l_theme_division_disp = "研修中課題";
				break;
			case 2:
				$l_theme_division_disp = "研修後課題";
				break;
		}

		//課題提出先　表示用
		$l_theme_present_disp = get_emp_disp_string($l_theme_present, $empmst_model);

		//ハイライトするセルのクラス属性値
		$l_highlight_class_name = "highlight_class_".$i."_".$i_theme;
?>
<tr>
<td align="center" class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="return_selected('<?=$l_training_apply_id?>','<?=$l_theme_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_inside_training_date_disp))?></font></td>
<td align="center" class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="return_selected('<?=$l_training_apply_id?>','<?=$l_theme_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_training_id_disp))?></font></td>
<td                class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="return_selected('<?=$l_training_apply_id?>','<?=$l_theme_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_training_name_disp))?></font></td>
<td align="center" class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="return_selected('<?=$l_training_apply_id?>','<?=$l_theme_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_theme_division_disp))?></font></td>
<td                class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="return_selected('<?=$l_training_apply_id?>','<?=$l_theme_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_theme_contents))?></font></td>
<td align="center" class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="return_selected('<?=$l_training_apply_id?>','<?=$l_theme_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_theme_method))?></font></td>
<td align="center" class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="return_selected('<?=$l_training_apply_id?>','<?=$l_theme_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_theme_deadline))?></font></td>
<td align="center" class="<?=$l_highlight_class_name ?>" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="return_selected('<?=$l_training_apply_id?>','<?=$l_theme_id?>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=nl2br(htmlspecialchars($l_theme_present_disp))?></font></td>
</tr>
<?
	}
}


?>
<!-- 研修申込一覧 データ END -->

</table>
<!-- 研修申込一覧表 END  -->


</center>
</form>

</body>
</html>
<?

//====================================
// 終了処理
//====================================
$log->info(basename(__FILE__)." END");
$log->shutdown();




//====================================================================================================
// ローカル関数
//====================================================================================================

/**
 * 「課題提出先」などの職員を表示文字列に変換します。
 *
 * @param $teacher_emp_id 表示する職員のemp_id
 * @param $empmst_model   empmstのモデル
 * @return 職員の表示文字列
 */
function get_emp_disp_string($teacher_emp_id, $empmst_model)
{
	$teacher_disp_string = "";
	if($teacher_emp_id != "")
	{
		$arrteacher_info = $empmst_model->select($teacher_emp_id);
		$teacher_disp_string = $arrteacher_info['emp_lt_nm']." ".$arrteacher_info['emp_ft_nm'];
	}
	return $teacher_disp_string;
}


/**
 * 研修回数を表示用文字列に変換します。
 *
 * @param $training_time DBより取得した日付データ。「YYYY-MM-DD」形式
 * @param $final_flg DBより取得した日付データ。「YYYY-MM-DD」形式
 * @return 日付の表示用文字列
 */
function get_training_time_disp_string($training_time,$final_flg)
{
	$ret = "最終回";
	if($final_flg != 1)
	{
		$ret = "第".$training_time."回";
	}
	return $ret;
}


/**
 * 日付を表示用文字列に変換します。「YYYY年MM月DD日(曜日)」
 *
 * @param $db_format_date DBより取得した日付データ。「YYYY-MM-DD」形式
 * @return 日付の表示用文字列
 */
function get_date_disp_string($db_format_date)
{
	$i_date = strtotime($db_format_date);
	$weekday = array( "日", "月", "火", "水", "木", "金", "土" );
	$ret = date("Y",$i_date)."年".date("m",$i_date)."月".date("d",$i_date)."日(".$weekday[date("w", $i_date)].")";
	return $ret;
}

/**
 * 時間を表示用文字列に変換します。「HH:mm」形式
 *
 * @param $db_format_time DBより取得した時間データ。「HH:mm:ss」形式
 * @return 時間の表示用文字列
 */
function get_time_disp_string($db_format_time)
{
	$ret = substr($db_format_time, 0,5);
	return $ret;
}

/**
 * JavaScript出力用に文字列変換します。
 *
 * @param $str 出力する文字列
 */
function to_javascript_string($str)
{
	$ret = $str;
	$ret = str_replace("\"","\\\"",$ret);
	$ret = str_replace("\r\n","\\n",$ret);
	$ret = str_replace("\r","\\n",$ret);
	$ret = str_replace("\n","\\n",$ret);
	return $ret;
}


?>