<?php
//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");
require_once('MDB2.php');
require_once(dirname(__FILE__) . "/../model/search/cl_levelup_evaluation_model.php");
require_once(dirname(__FILE__) . "/../model/search/cl_apl_levelup_apply_list_model.php");
require_once(dirname(__FILE__) . "/../model/search/cl_empmst_nm_model.php");
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

$levelup_apply_id = $_GET["levelup_apply_id"];
$log->debug('levelup_apply_id:'.$levelup_apply_id,__FILE__,__LINE__);

$apply_id=$_GET["apply_id"];
$log->debug('apply_id:'.$apply_id,__FILE__,__LINE__);

$evaluation_division=$_GET["evaluation_division"];
$log->debug('evaluation_division:'.$evaluation_division,__FILE__,__LINE__);

$login_user = $_GET["login_user"];
$log->debug('login_user:'.$login_user,__FILE__,__LINE__);

$level = $_GET["level"];
$log->debug('level:'.$level,__FILE__,__LINE__);

$mode = $_GET["mode"];
$log->debug('mode:'.$mode,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 評価表データ取得処理を呼び出す
//------------------------------------------------------------------------------
$json=getJsonEvaluationListData($mdb2, $levelup_apply_id, $apply_id, $evaluation_division, $login_user, $level);

$log->debug('$json:'.$json,__FILE__,__LINE__);


//------------------------------------------------------------------------------
// データベース接続を閉じる
//------------------------------------------------------------------------------
$log->debug("データベース切断 START",__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);

header("Content-Type: application/json; charset=EUC-JP");
echo $json;

/**
 * データ取得
 * @param        $mb2 DB接続情報
 * @param string $login_user_id　LoginユーザID
 * @param string $apply_id　     申請ID
 */
function getJsonEvaluationListData($mdb2, $levelup_apply_id, $apply_id, $evaluation_division, $login_user, $level){
	global $log;
	$log->debug(__FUNCTION__." START");


	//--------------------------------------------------------------------------
	//  テーブルモデルインスタンス生成
	//--------------------------------------------------------------------------
	$log->debug("cl_apl_levelup_apply_list_modelインスタンス作成開始",__FILE__,__LINE__);
	$cl_apl_levelup_apply_list_model = new cl_apl_levelup_apply_list_model($mdb2, $login_user);
	$log->debug("cl_apl_levelup_apply_list_modelインスタンス作成終了",__FILE__,__LINE__);

	if($evaluation_division == "1"){
		//--------------------------------------------------------------------------
		//  レベルアップ申請テーブルデータ取得
		//--------------------------------------------------------------------------
		$log->debug("getLevelupApplyList_forNewEval開始  login_user = ". $login_user,"level = ". $level,__FILE__,__LINE__);
		$leveup_apply_data=$cl_apl_levelup_apply_list_model->getLevelupApplyList_forNewEval($login_user,$level);
		$log->debug("getLevelupApplyList_forNewEval終了",__FILE__,__LINE__);

		if($leveup_apply_data[0]["levelup_apply_id"]==""){
			$result['leveup_apply_count'] = 0;
		}else{
			$result['leveup_apply_count'] = count($leveup_apply_data);
		}
	}else{
		$result['leveup_apply_count'] = 0;
	}

	if($levelup_apply_id == "" && $apply_id == ""){
		//新規申請時　初期表示

		// 対象データが１件の場合、レベルアップ申請IDを保持する
		if ($result['leveup_apply_count'] == 1) {
			$result['levelup_apply_id'] = $leveup_apply_data[0]['levelup_apply_id'];
			$levelup_evaluation_id = "";
			$result['levelup_evaluation_id'] = $levelup_evaluation_id;
			$result['chief_emp_name'] = $leveup_apply_data[0]['chief_emp_name'];
			$result['levelup_emp_id'] = $leveup_apply_data[0]['levelup_emp_id'];
			$result['colleague_emp_name'] = $leveup_apply_data[0]['colleague_emp_name'];
			$result['colleague_division'] = "";
			$levelup_emp_id = $login_user;
			$result['levelup_apply_no'] = $leveup_apply_data[0]['levelup_apply_no'];
		} else {
			$result['levelup_apply_id'] = '';
			$levelup_emp_id = $login_user;
			$result['levelup_apply_no'] = '';
		}
	}elseif($levelup_apply_id == "" && $apply_id != ""){
		//新規申請以外 初期表示
		//--------------------------------------------------------------------------
		//  レベルアップ申請テーブルデータ取得
		//--------------------------------------------------------------------------
		$log->debug("getLevelupApplyList_forEdit開始  apply_id = ". $apply_id."evaluation_division = ". $evaluation_division,__FILE__,__LINE__);
		$data=$cl_apl_levelup_apply_list_model->getLevelupApplyList_forEdit($apply_id,$evaluation_division);
		$log->debug("getLevelupApplyList_forEdit終了",__FILE__,__LINE__);

		$result['levelup_apply_id'] = $data['levelup_apply_id'];
		$levelup_evaluation_id = $data['levelup_evaluation_id'];
		$result['levelup_evaluation_id'] = $levelup_evaluation_id;
		$result['chief_emp_name'] = $data['chief_emp_name'];
		$result['levelup_emp_id'] = $data['levelup_emp_id'];
		$result['colleague_emp_name'] = $data['colleague_emp_name'];
		$result['colleague_division'] = $data['colleague_division'];
		if($data['levelup_emp_id'] == ""){
			$levelup_emp_id = $login_user;
		}else{
			$levelup_emp_id = $data['levelup_emp_id'];
		}
		$result['self_evaluation_date'] = $data['self_evaluation_date'];
		$result['supervisor_emp_id'] = $data['supervisor_emp_id'];
		$result['supervisor_evalution_date'] = $data['supervisor_evalution_date'];
		$result['colleague_division'] = $data['colleague_division'];
		$result['levelup_evaluation_id'] = $data['levelup_evaluation_id'];
		$result['levelup_apply_no'] = $data['levelup_apply_no'];
	}else{
		//申請番号選択時
		//--------------------------------------------------------------------------
		//  レベルアップ申請テーブルデータ取得
		//--------------------------------------------------------------------------
		$log->debug("getLevelupApplyList_forNewEval2開始  levelup_apply_id = ". $levelup_apply_id,__FILE__,__LINE__);
		$data=$cl_apl_levelup_apply_list_model->getLevelupApplyList_forNewEval2($levelup_apply_id);
		$log->debug("getLevelupApplyList_forNewEval2終了",__FILE__,__LINE__);

		$result['levelup_apply_id'] = $levelup_apply_id;
		$levelup_evaluation_id = "";
		$result['levelup_evaluation_id'] = $levelup_evaluation_id;
		$result['chief_emp_name'] = $data[0]['chief_emp_name'];
		$result['levelup_emp_id'] = $data[0]['levelup_emp_id'];
		$levelup_emp_id = $data[0]['levelup_emp_id'];
		$result['colleague_emp_name'] = $data[0]['colleague_emp_name'];
		$result['colleague_division'] = "";
		$result['levelup_apply_no'] = $data[0]['levelup_apply_no'];
	}

	$log->debug("levelup_apply_id:".$result['levelup_apply_id'],__FILE__,__LINE__);
	if ($result['levelup_apply_id'] != '') {

		//--------------------------------------------------------------------------
		//  テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("cl_levelup_evaluation_modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_levelup_evaluation_model($mdb2, $login_user);
		$log->debug("cl_levelup_evaluation_modelインスタンス作成終了",__FILE__,__LINE__);

		if($apply_id == ''){
			//新規申請時
			//--------------------------------------------------------------------------
			//  レベルアップ評価表データ取得
			//--------------------------------------------------------------------------
			$log->debug("getEvaluationList_forNewEval開始  levelup_apply_id = ". $levelup_apply_id,__FILE__,__LINE__);
			$data=$model->getEvaluationList_forNewEval($result['levelup_apply_id']);
			$log->debug("getEvaluationList_forNewEval終了",__FILE__,__LINE__);

			$result["evaluation_list"] = $data;
		}else if($levelup_apply_id == ""){
			//新規申請以外　初期表示
			if($evaluation_division == "1"){
				//自己評価
				//--------------------------------------------------------------------------
				//  レベルアップ評価表データ取得
				//--------------------------------------------------------------------------
				$log->debug("getEvaluationList_forEdit_div1開始  levelup_evaluation_id = ". $levelup_evaluation_id,__FILE__,__LINE__);
				$data=$model->getEvaluationList_forEdit_div1($levelup_evaluation_id);
				$log->debug("getEvaluationList_forEdit_div1終了",__FILE__,__LINE__);

				$result["evaluation_list"] = $data;
			}else{
				//同僚評価
				//--------------------------------------------------------------------------
				//  レベルアップ評価表データ取得
				//--------------------------------------------------------------------------
				$log->debug("getEvaluationList_forEdit_div2開始  levelup_evaluation_id = ". $levelup_evaluation_id ." emp_id = " .$login_user,__FILE__,__LINE__);
				$data=$model->getEvaluationList_forEdit_div2($levelup_evaluation_id);
				$log->debug("getEvaluationList_forEdit_div2終了",__FILE__,__LINE__);

				$result["evaluation_list"] = $data;
			}
		}else{
			//申請番号選択時
			//--------------------------------------------------------------------------
			//  レベルアップ評価表データ取得
			//--------------------------------------------------------------------------
			$log->debug("getEvaluationList_forNewEval開始  levelup_apply_id = ". $levelup_apply_id,__FILE__,__LINE__);
			$data=$model->getEvaluationList_forNewEval($result['levelup_apply_id']);
			$log->debug("getEvaluationList_forNewEval終了",__FILE__,__LINE__);

			$result["evaluation_list"] = $data;
		}
	}
	//--------------------------------------------------------------------------
	//  職員マスタテーブルモデルインスタンス生成
	//--------------------------------------------------------------------------
	$log->debug("cl_empmst_nm_modelインスタンス作成開始",__FILE__,__LINE__);
	$cl_empmst_nm_model = new cl_empmst_nm_model($mdb2, $login_user);
	$log->debug("cl_empmst_nm_modelインスタンス作成終了",__FILE__,__LINE__);

	//--------------------------------------------------------------------------
	//  テーブルデータ取得
	//--------------------------------------------------------------------------
	// 所属長　対象部署取得
	$log->debug("所属長　対象部署取得 START",__FILE__,__LINE__);
	$class_division = $cl_empmst_nm_model->get_supervisor_class();
	$log->debug("所属長　対象部署取得 END class_division = ".$class_division,__FILE__,__LINE__);

	if ($class_division == "0") {
		// 所属長(部署指定無)取得
		$log->debug("所属長(部署指定無)取得 START levelup_emp_id = ".$levelup_emp_id,__FILE__,__LINE__);
		$arr_wkfw_supervisor_apv = $cl_empmst_nm_model->get_supervisor_apv_class_none($levelup_emp_id);
		$log->debug("所属長(部署指定無)取得 END",__FILE__,__LINE__);
	} else {
		// 所属長(部署指定有)取得
		$log->debug("所属長(部署指定有)取得 START levelup_emp_id = " . $levelup_emp_id . " class_division = " . $class_division,__FILE__,__LINE__);
		$arr_wkfw_supervisor_apv = $cl_empmst_nm_model->get_supervisor_apv($levelup_emp_id,$class_division);
		$log->debug("所属長(部署指定有)取得 END",__FILE__,__LINE__);
	}

	// 2012/09/24 Yamagawa add(s)
	$log->debug("志願者取得 START levelup_emp_id = ".$levelup_emp_id,__FILE__,__LINE__);
	$arr_applicant = $cl_empmst_nm_model->get_applicant_name_affiliation($levelup_emp_id);
	$log->debug("志願者取得 END",__FILE__,__LINE__);

	$result["applicant_nm"] = $arr_applicant["emp_lt_nm"]." ".$arr_applicant["emp_ft_nm"];

	$result["applicant_affiliation"] = $arr_applicant["class_nm"];

	if ($arr_applicant["atrb_nm"] != "") {
		$result["applicant_affiliation"] .= " > ".$arr_applicant["atrb_nm"];
	}

	if ($arr_applicant["dept_nm"] != "") {
		$result["applicant_affiliation"] .= " > ".$arr_applicant["dept_nm"];
	}

	if ($arr_applicant["room_nm"] != "") {
		$result["applicant_affiliation"] .= " > ".$arr_applicant["room_nm"];
	}
	// 2012/09/24 Yamagawa add(e)

	for($i=0; $i<count($arr_wkfw_supervisor_apv); $i++)
	{
		$arr_wkfw_supervisor_apv[$i]["apv_div"] = "8";
	}

	$result["supervisor_nm"] = $arr_wkfw_supervisor_apv;
	$log->debug("arr_wkfw_supervisor_apv:".count($arr_wkfw_supervisor_apv),__FILE__,__LINE__);

	$result["evaluation_division"] = $evaluation_division;

	$json=create_json($result);

	$log->debug(__FUNCTION__." END");

	return $json;
}
