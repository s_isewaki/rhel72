<?
	/*
	研修講座一覧

	【呼び出しパラメータ】
	session　　　　　　　　　　セッションID。
	gamen_mode 　　　　　　　　画面モード。"training_select":講座選択、"plan_select":日程選択(研修IDの指定が必須)
	head_nurse_recommend_flg 　師長推薦フラグ。true:師長推薦、false:通常。(省略時はfalse)
	emp_id 　　　　　　　　　　研修受講者の職員ID。
	training_id　　　　　　　　初期選択させる研修ID。(省略可)
	plan_id_csv　　　　　　　　初期選択させる日程IDをカンマ区切りで指定。例)"TSC00000001,TSC00000002,TSC00000003" (省略可)

	【戻り値】
	training_id　　　　　　　　研修ID
	training_name　　　　　　　研修名
	training_purpose 　　　　　研修目的
	schedule_list　　　　　　　研修日程情報の配列(key=選択順,value=日程情報オブジェクト)

	※日程情報オブジェクト
	plan_id　　　　　　　　　　日程ID
	plan_date　　　　　　　　　開催日付(書式:"yyyy/MM/dd")
	from_time　　　　　　　　　開催時刻From(書式:"HH:mm")
	to_time　　　　　　　　　　開催時刻To(書式:"HH:mm")

	*/

	//##########################################################################
	// 各種モジュールの読み込み
	//##########################################################################
	ini_set("display_errors","1");
	require_once( "about_session.php");
	require_once( "about_authority.php");
	require_once( "about_postgres.php");
	require_once( "show_select_values.ini");
	require_once( "cl_application_list.inc");
	require_once( "cl_yui_calendar_util.ini");
	require_once( "cl_application_common.ini");
	require_once( "cl_common.ini");
	require_once( "cl_common_apply.inc");

	require_once( "cl_title_name.ini");
	require_once( "cl_common_log_class.inc");
	require_once( "Cmx.php");
	require_once("Smarty/Smarty.class.php");
	require_once('MDB2.php');


	//##########################################################################
	// 初期処理
	//##########################################################################

	//ロガーをロード
	$log = new cl_common_log_class(basename(__FILE__));
	$log->info(basename(__FILE__)." START");

	//画面名
	$fname = $PHP_SELF;

	//##########################################################################
	// パラメータ取得
	//##########################################################################
	$training_id	= $_POST["training_id"];
	$login_user		= $_POST["login_user"];
	$mode			= $_POST["mode"];
	$plan_id		= $_POST["plan_id"];
	// 2012/11/19 Yamagawa add(s)
	$target_year	= $_POST["target_year"];
	// 2012/11/19 Yamagawa add(e)

	$log->debug( '■$training_id:'.$training_id,__FILE__,__LINE__);
	$log->debug( '■$login_user :'.$login_user,__FILE__,__LINE__);
	$log->debug( '■$mode       :'.$mode,__FILE__,__LINE__);
	$log->debug( '■$plan_id    :'.$plan_id,__FILE__,__LINE__);

	//##########################################################################
	//セッションのチェック
	//##########################################################################
	$session = qualify_session($session,$fname);
	//if($session == "0"){
	//	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	//	echo("<script language='javascript'>showLoginPage(window);</script>");
	//	exit;
	//}

	//##########################################################################
	//権限チェック
	//##########################################################################
	//$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
	//if($wkfw=="0"){
	//	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	//	echo("<script language='javascript'>showLoginPage(window);</script>");
	//	exit;
	//}

	//##########################################################################
	// データベース接続
	//##########################################################################
	$mdb2 = MDB2::connect(CMX_DB_DSN);

	//##########################################################################
	// パラメータ取得
	//##########################################################################
	$today = date("Y/m/d", strtotime("today"));

	$arr_today = split("/", $today);
	$yyyy = $arr_today[0];
	$mm   = $arr_today[1];
	$dd   = $arr_today[2];
	$log->debug( 'yyyy:'.$yyyy,__FILE__,__LINE__);
	$log->debug( 'mm:'.$mm,__FILE__,__LINE__);
	$log->debug( 'dd:'.$dd,__FILE__,__LINE__);

	$calendar = '';
	$calendar .= write_calendar('plan', 'plan_date_yyyy', 'plan_date_mm', 'plan_date_dd');

	// 年オプションHTML取得
	//$option_date_yyyy=cl_get_select_years($yyyy);
	$option_date_yyyy=get_select_around_years($yyyy);

	// 月オプションHTML取得
	$option_date_mm=cl_get_select_months($mm);

	// 日オプションHTML取得
	$option_date_dd=cl_get_select_days($dd);

	//##########################################################################
	// 現在年度設定
	//##########################################################################
	if( $mm >=1 && $mm <=3) {
		$now_yyyy=$yyyy-1;
	} else {
		$now_yyyy=$yyyy;
	}

	//##########################################################################
	// 年度リスト作成
	//##########################################################################
	// 2012/11/20 Yamagwa upd(s)
	/*
	$nendo_ary[]=$now_yyyy-2;
	$nendo_ary[]=$now_yyyy-1;
	$nendo_ary[]=$now_yyyy;
	$nendo_ary[]=$now_yyyy+1;
	$nendo_ary[]=$now_yyyy+2;
	*/
	$nendo_ary[]=$now_yyyy+2;
	$nendo_ary[]=$now_yyyy+1;
	$nendo_ary[]=$now_yyyy;
	$nendo_ary[]=$now_yyyy-1;
	$nendo_ary[]=$now_yyyy-2;
	$nendo_ary[]=$now_yyyy-3;
	$nendo_ary[]=$now_yyyy-4;
	$nendo_ary[]=$now_yyyy-5;
	$nendo_ary[]=$now_yyyy-6;
	$nendo_ary[]=$now_yyyy-7;
	$nendo_ary[]=$now_yyyy-8;
	$nendo_ary[]=$now_yyyy-9;
	// 2012/11/20 Yamagawa upd(e)

	//##########################################################################
	// 時分配列
	//##########################################################################
	//for($h=0;$h<=23;$h++) $h_ary[]=str_pad($h,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(0,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(1,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(2,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(3,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(4,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(5,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(6,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(7,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(8,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(9,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(10,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(11,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(12,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(13,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(14,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(15,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(16,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(17,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(18,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(19,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(20,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(21,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(22,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(23,2, "0", STR_PAD_LEFT);
	$h_ary[]=str_pad(24,2, "0", STR_PAD_LEFT);

	//for($m=0;$m<=59;$m++) $m_ary[]=str_pad($m,2, "0", STR_PAD_LEFT);
	$m_ary[]=str_pad(0,2, "0", STR_PAD_LEFT);
	$m_ary[]=str_pad(5,2, "0", STR_PAD_LEFT);
	$m_ary[]=str_pad(10,2, "0", STR_PAD_LEFT);
	$m_ary[]=str_pad(15,2, "0", STR_PAD_LEFT);
	$m_ary[]=str_pad(20,2, "0", STR_PAD_LEFT);
	$m_ary[]=str_pad(25,2, "0", STR_PAD_LEFT);
	$m_ary[]=str_pad(30,2, "0", STR_PAD_LEFT);
	$m_ary[]=str_pad(35,2, "0", STR_PAD_LEFT);
	$m_ary[]=str_pad(40,2, "0", STR_PAD_LEFT);
	$m_ary[]=str_pad(45,2, "0", STR_PAD_LEFT);
	$m_ary[]=str_pad(50,2, "0", STR_PAD_LEFT);
	$m_ary[]=str_pad(55,2, "0", STR_PAD_LEFT);

	//##########################################################################
	// 院内研修取得
	//##########################################################################
	require_once("cl/model/master/cl_mst_inside_training_model.php");
	$training_model = new cl_mst_inside_training_model($mdb2 ,$login_user);
	$training_data = $training_model->select($training_id);

	//##########################################################################
	// 開催回数リスト作成
	//##########################################################################
	if ($training_data['max_training_time'] == ''){
		$max_training_time = 0;
	} else {
		$max_training_time = $training_data['max_training_time'];
	}

	$option_training_time = '';

	//##########################################################################
	// ブランク行生成
	//##########################################################################
	if (
		$max_training_time > 1
		|| $mode == 2
	) {
		$option_training_time .= '<option value=""></option>';
	}

	//##########################################################################
	// 回数リスト生成
	//##########################################################################
	for($i = 1; $i <= $max_training_time; $i++){
		$option_training_time .= '<option value="';
		$option_training_time .= $i;
		$option_training_time .= '">';
		if ($i == $max_training_time) {
			$option_training_time .= '最終回';
		} else {
			$option_training_time .= '第';
			$option_training_time .= $i;
			$option_training_time .= '回';
		}
		$option_training_time .= '</option>';
	}

	//##########################################################################
	// 画面表示
	//##########################################################################
	$smarty = new Smarty();
	$smarty->template_dir = dirname(__FILE__) . "/../view";
	$smarty->compile_dir  = dirname(__FILE__) . "/../../templates_c";

	$smarty->assign( 'session'				, $session					);
	$smarty->assign( 'title'				, '開催日登録・修正'		);
	$smarty->assign( 'option_training_time'	, $option_training_time		);
	$smarty->assign( 'option_date_yyyy'		, $option_date_yyyy			);
	$smarty->assign( 'option_date_mm'		, $option_date_mm			);
	$smarty->assign( 'option_date_dd'		, $option_date_dd			);
	$smarty->assign( 'training_id'			, $training_id				);
	$smarty->assign( 'login_user'			, $login_user				);
	$smarty->assign( 'mode'					, $mode						);
	$smarty->assign( 'plan_id'				, $plan_id					);
	$smarty->assign( 'max_training_time'	, $max_training_time		);

	$smarty->assign( 'nendo_ary'			, $nendo_ary				);
	// 2012/11/22 Yamagawa del(s)
	//$smarty->assign( 'now_yyyy'				, $now_yyyy					);
	// 2012/11/22 Yamagawa del(e)

	$smarty->assign( 'h_ary'				, $h_ary					);
	$smarty->assign( 'm_ary'				, $m_ary					);

	$smarty->assign( 'calendar'				, $calendar					);

	// 2012/07/25 Yamagawa add(s)
	$smarty->assign('auto_control_flg', $training_data['auto_control_flg']);
	// 2012/07/25 Yamagawa add(e)

	// 2012/11/19 Yamagawa add(s)
	$smarty->assign('target_year'			, $target_year				);
	// 2012/11/19 Yamagawa add(e)

	$smarty->display(basename(__FILE__,'.php').".tpl");

	$log->info(basename(__FILE__)." END");



	$log->shutdown();

	/**
	 * 年オプションHTML取得
	 */
	function get_select_around_years($date)
	{
		ob_start();
		show_years_around(9, 2, $date);
		$str_buff=ob_get_contents();
		ob_end_clean();
		return $str_buff;
	}

	function show_years_around($before_count, $after_count, $num){

		$now  = date(Y);
		if ($num == ''){
			$num = $now;
		}

		// ブランク行
		echo("<option value=\"\"></option>");


		// 翌年以降
		for($i=$after_count; $i>0; $i--){
			$yr = $now + $i;
			echo("<option value=\"".$yr."\"");
			if($num == $yr){
				echo(" selected");
			}
			echo(">".$yr."</option>\n");
		}

		// 当年まで
		for($i=0; $i<=$before_count; $i++){
			$yr = $now - $i;
			echo("<option value=\"".$yr."\"");
			if($num == $yr){
				echo(" selected");
			}
			echo(">".$yr."</option>\n");
		}

	}

	function write_calendar($suffix, $year_control_id, $month_control_id, $date_control_id){

		ob_start();
		write_cl_calendar($suffix, $year_control_id, $month_control_id, $date_control_id);
		$str_buff = ob_get_contents();
		ob_end_clean();
		return $str_buff;

	}

