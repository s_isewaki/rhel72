<?php
//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");
require_once('MDB2.php');
require_once(dirname(__FILE__) . "/../model/template/cl_apl_inside_training_apply_model.php");

$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

$apply_id=$_GET["apply_id"];

$log->debug('apply_id:'.$apply_id,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 申請書データ取得処理を呼び出す
//------------------------------------------------------------------------------
$json=getJsonApplyData($mdb2, $apply_id);

$log->debug('$json:'.$json,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベース接続を閉じる
//------------------------------------------------------------------------------
$log->debug("データベース切断 START",__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);

header("Content-Type: application/json; charset=EUC-JP");
echo $json;

/**
 * 申請書データ取得
 */
function getJsonApplyData($mdb2, $apply_id){
	global $log;
	$log->debug(__FUNCTION__." START");

	//--------------------------------------------------------------------------
	// 院内研修申請テーブルモデルインスタンス生成
	//--------------------------------------------------------------------------
	$log->debug("apply modelインスタンス作成開始",__FILE__,__LINE__);
	$model = new cl_apl_inside_training_apply_model($mdb2, $user);
	$log->debug("apply modelインスタンス作成終了",__FILE__,__LINE__);

	//--------------------------------------------------------------------------
	// 院内研修申請テーブルデータ取得
	//--------------------------------------------------------------------------
	$log->debug("getRecJson開始",__FILE__,__LINE__);
	$data=$model->select_by_apply_id($apply_id);
	$data["status_division"] = $data["update_division"];
	$log->debug("getRecJson終了",__FILE__,__LINE__);

	foreach( $data as $key => $value ) {
		$log->debug('★★★★★★$data['.$key.']:'.$data[$key],__FILE__,__LINE__);
	}

	$data["learn_memo"]      = str_replace("\r\n",   "\\n",$data["learn_memo"]);			// 改行
	$data["training_slogan"] = str_replace("\r\n",   "\\n",$data["training_slogan"]);		// 改行

	//2012/05/11 K.Fujii ins(s)
	$data["training_require"] = str_replace("\r\n",   "\\n",$data["training_require"]);		// 改行
	//2012/05/11 K.Fujii ins(e)

	//--------------------------------------------------------------------------
	// 日程情報データ取得
	//--------------------------------------------------------------------------
	$log->debug("schedule modelインスタンス作成開始",__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_schedule_model.php");
	$cl_mst_inside_training_schedule_model = new cl_mst_inside_training_schedule_model($mdb2, $user);
	$log->debug("schedule modelインスタンス作成終了",__FILE__,__LINE__);

	$log->debug("plan_id = ".$data["plan_id"],__FILE__,__LINE__);
	$arr_plan = $cl_mst_inside_training_schedule_model->getList($data["plan_id"]);

	// 2012/04/12 Yamagawa add(s)
	// 「23:59」は「24:00」に変換（暫定対応）
	for($i = 0; $i < count($arr_plan); $i++) {
		if (substr($arr_plan[$i]['from_time'],0,5) == "23:59") {
			$arr_plan[$i]['from_time'] = "24:00";
		}
		if (substr($arr_plan[$i]['to_time'],0,5) == "23:59") {
			$arr_plan[$i]['to_time'] = "24:00";
		}
	}
	// 2012/04/12 Yamagawa add(e)

	$log->debug("plan count = ". count($arr_plan),__FILE__,__LINE__);

	$log->debug("$arr_plan = ". $arr_plan,__FILE__,__LINE__);

	$data["schedule_list"] = $arr_plan;

	$json=create_json($data);

	$log->debug(__FUNCTION__." END");

	return $json;
}
