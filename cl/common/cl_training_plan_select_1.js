//----------------------------------------------------------------------------------------------
// 開催日登録
//----------------------------------------------------------------------------------------------
//function plan_regist(){
//	plan_edit_call(1);
//}
//----------------------------------------------------------------------------------------------
// 開催日修正
//----------------------------------------------------------------------------------------------
function plan_update(){
	//------------------------------------------------------------------------------------------
	//子画面のモジュール名
	//------------------------------------------------------------------------------------------
	var moduleName = "cl_training_plan_regist_1";

	//------------------------------------------------------------------------------------------
	//選択研修日程件数取得
	//------------------------------------------------------------------------------------------
	//alert("document.training_form.plan_chkbox.length=" + document.training_form.plan_chkbox.length);
	var plan_count = document.training_form.plan_chkbox.length;
	var plan_chk_count = 0;
	var plan_id = "";
	if (plan_count > 1) {
		for (i=0; i < plan_count; i++){
			if( document.training_form.plan_chkbox[i].checked ){
				plan_chk_count++;
				plan_id = document.training_form.plan_chkbox[i].value;
			}
		}
	} else {
		if( document.training_form.plan_chkbox.checked ){
			plan_chk_count++;
			plan_id = document.training_form.plan_chkbox.value;
		}
	}
	if( plan_chk_count == 0 ){
		alert("開催日情報が選択されていません。");
		return;
	}

	//------------------------------------------------------------------------------------------
	//リクエストパラメータをオブジェクトで作成
	//------------------------------------------------------------------------------------------
	if( plan_chk_count > 1 ){
		alert("開催日情報は１件のみ選択してください。");
		return;
	}
	//alert("plan_id:" + plan_id);
	//------------------------------------------------------------------------------------------
	//リクエストパラメータをオブジェクトで作成
	//------------------------------------------------------------------------------------------
	var params = new Object();
	params.training_id	= document.training_form.training_id.value;	// ログインユーザID
	params.login_user	= document.training_form.login_user.value;	// 院内研修ID
	params.mode			= "2";										// 動作モード 1:登録、2:更新
	params.plan_id		= plan_id;									// 研修日程ID

	//------------------------------------------------------------------------------------------
	//ウィンドウサイズなどのオプション
	//var h = window.screen.availHeight;
	//var w = window.screen.availWidth;
	//w = 1024;
	//------------------------------------------------------------------------------------------
	var w = 1090;
	var h = 600;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

	//------------------------------------------------------------------------------------------
	//子画面を開く
	//------------------------------------------------------------------------------------------
	commonOpen(moduleName,params,option,'cl_training_plan_regist_1','{/literal}{$session}{literal}');
}
//----------------------------------------------------------------------------------------------
// 開催日削除
//----------------------------------------------------------------------------------------------
function plan_delete(){

	//plan_edit_call(3);
	//------------------------------------------------------------------------------------------
	//選択研修日程件数取得
	//------------------------------------------------------------------------------------------
	//alert(document.training_form.plan.length);
	var plan_count = document.training_form.plan_chkbox.length;
	var plan_chk_count = 0;
	var plan_id = "";
	var plan_id_str = "";
	if (plan_count > 1) {
		for (i=0; i < plan_count; i++){
			if( document.training_form.plan_chkbox[i].checked ){
				plan_chk_count++;
				plan_id = document.training_form.plan_chkbox[i].value;
				plan_id_str = plan_id_str + plan_id + ',';
			}
		}
	} else {
		if( document.training_form.plan_chkbox.checked ){
			plan_chk_count++;
			plan_id = document.training_form.plan_chkbox.value;
			plan_id_str = plan_id_str + plan_id + ',';
		}
	}

	if( plan_chk_count == 0 ){
		alert("削除する開催日情報が選択してください。");
		return;
	}


	if(confirm('選択した開催日情報を削除しますか？')){
		jQuery.ajax({
			contentType: 'application/x-www-form-urlencoded; charset=EUC-JP',
			type: 'post',
			url: 'cl/common/cl_mst_inside_training_schedule_delete.php',
			dataType: 'json',
			data:{
					"login_user"		: document.training_form.login_user.value	,	// ログインユーザID
					"training_id"		: document.training_form.training_id.value	,	// 院内研修ID
					"plan_id_str"		: plan_id_str									// 開催日ID文字列

			},
			success: function(data, dataType){
				//alert('success');

				//alert('data.success:' + data.success);


				//success

				if( data.success == 'true' ){
					alert('選択された開催日を削除しました。');

				}
				else{
					alert('開催日削除に失敗しました。');

				}

				//呼び出し元画面にインターフェース関数が定義されている場合
				//if(window.opener && !window.opener.closed && window.opener.plan_regist_return)
				//{
					//返却値を取得
					//var result = m_return_data_list[training_apply_id];
					//親画面へ通知
					plan_regist_return();

				//}
				//自画面を終了します。
				//window.close();

			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				alert('開催日削除処理の起動に失敗しました。');
				//alert('textStatus:' + textStatus);
				plan_regist_return();
				//window.close();
			}
		});
	}
	else{
		// 何もしない
	}

}
//----------------------------------------------------------------------------------------------
// 開催日登録・修正画面表示
//----------------------------------------------------------------------------------------------
function plan_regist()
{
	//------------------------------------------------------------------------------------------
	//子画面のモジュール名
	//------------------------------------------------------------------------------------------
	var moduleName = "cl_training_plan_regist_1";

	//------------------------------------------------------------------------------------------
	//リクエストパラメータをオブジェクトで作成
	//------------------------------------------------------------------------------------------
	var params = new Object();
	params.training_id	= document.training_form.training_id.value;	// ログインユーザID
	params.login_user	= document.training_form.login_user.value;	// 院内研修ID
	params.mode			= "1";										// 動作モード 1:登録、2:更新
	// 2012/11/16 Yamagawa add(s)
	params.target_year	= document.training_form.year.value;		// 年度
	// 2012/11/16 Yamagawa add(e)

	//------------------------------------------------------------------------------------------
	//ウィンドウサイズなどのオプション
	//------------------------------------------------------------------------------------------
	var w = 1090;
	var h = 600;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

	//------------------------------------------------------------------------------------------
	//子画面を開く
	//------------------------------------------------------------------------------------------
	commonOpen(moduleName,params,option,'cl_training_plan_regist_1','{/literal}{$session}{literal}');
}

//子画面の実行結果受け取り関数(インターフェース関数)
//  result ・・・ 子画面で選択された情報
function plan_regist_return()
{

	window.focus();
	// 2012/08/27 Yamagawa add(s)
	document.training_form.action = "cl_common_call.php";
	// 2012/08/27 Yamagawa add(e)
	document.training_form.submit();

}

// 出欠登録
function open_attendance_regist_org(login_user,plan_id,training_id){

	var moduleName = "cl_inside_training_attendance_regist";

	//リクエストパラメータをオブジェクトで作成
	var params = new Object();
	params.login_user =login_user;
	params.plan_id = plan_id;
	params.training_id = training_id;
	params.p_session = document.training_form.session.value;

	//ウィンドウサイズなどのオプション
	//var h = window.screen.availHeight;
	//var w = window.screen.availWidth;
	var w = 1090;
	var h = 600;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

	//子画面を開く
	commonOpen(moduleName,params,option,'cl_inside_training_attendance_regist');
}

// 一括更新
function plan_update_all(){

	//------------------------------------------------------------------------------------------
	//子画面のモジュール名
	//------------------------------------------------------------------------------------------
	var moduleName = "cl_training_plan_regist_1";

	//------------------------------------------------------------------------------------------
	//リクエストパラメータをオブジェクトで作成
	//------------------------------------------------------------------------------------------
	var params = new Object();
	params.training_id	= document.training_form.training_id.value;	// ログインユーザID
	params.login_user	= document.training_form.login_user.value;	// 院内研修ID
	params.mode			= "3";										// 動作モード 1:登録、2:更新
	// 2012/11/16 Yamagawa add(s)
	params.target_year			= document.training_form.year.value;		// 年度
	// 2012/11/16 Yamagawa add(e)

	//------------------------------------------------------------------------------------------
	//ウィンドウサイズなどのオプション
	//var h = window.screen.availHeight;
	//var w = window.screen.availWidth;
	//w = 1024;
	//------------------------------------------------------------------------------------------
	var w = 1090;
	var h = 600;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

	//------------------------------------------------------------------------------------------
	//子画面を開く
	//------------------------------------------------------------------------------------------
	commonOpen(moduleName,params,option,'cl_training_plan_regist_1','{/literal}{$session}{literal}');

}