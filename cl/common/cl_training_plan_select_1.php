<?
	//ini_set( 'display_errors', 1 );

	//##########################################################################
	// 各種モジュールの読み込み
	//##########################################################################
	require_once(dirname(__FILE__) . "/../../about_session.php");
	require_once(dirname(__FILE__) . "/../../about_authority.php");
	require_once(dirname(__FILE__) . "/../../about_postgres.php");
	require_once(dirname(__FILE__) . "/../../show_select_values.ini");
	require_once(dirname(__FILE__) . "/../../cl_application_list.inc");
	require_once(dirname(__FILE__) . "/../../cl_yui_calendar_util.ini");
	require_once(dirname(__FILE__) . "/../../cl_application_common.ini");
	require_once(dirname(__FILE__) . "/../../cl_common.ini");
	require_once(dirname(__FILE__) . "/../../cl_title_name.ini");
	require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
	require_once(dirname(__FILE__) . "/../../Cmx.php");
	require_once("Smarty/Smarty.class.php");
	require_once("MDB2.php");
	// 2012/08/22 Yamagawa add(s)
	require_once(dirname(__FILE__) . "/cl_common_define.inc");
	// 2012/08/22 Yamagawa add(e)

	//##########################################################################
	// 初期処理
	//##########################################################################

	//ロガーをロード
	$log = new cl_common_log_class(basename(__FILE__));
	$log->info(basename(__FILE__)." START");

	//画面名
	$fname = $PHP_SELF;

	// 2012/11/16 Yamagawa add(s)
	// 年度リスト設定
	if ($_POST['year'] == '') {
		$year = date(Y);
		if (date(n) <= 3) {
			$year -= 1;
		}
	} else {
		$year = $_POST['year'];
	}
	$option_year = get_select_around_years($year);
	// 2012/11/16 Yamagawa add(e)

	//##########################################################################
	// パラメータ取得
	//##########################################################################
foreach( $_REQUEST as $key => $value ) {
	$log->debug('$_REQUEST['.$key.']:'.$_REQUEST[$key],__FILE__,__LINE__);
}
	$training_id	= $_POST["training_id"];
	$login_user		= $_POST["login_user"];
	$common_module	= $_POST["common_module"];
	$session		= $_POST["session"];
	$mode			= $_POST["mode"];

	$log->debug('■$training_id   :'.$training_id	,__FILE__,__LINE__);
	$log->debug('■$login_user    :'.$login_user	,__FILE__,__LINE__);
	$log->debug('■$common_module :'.$common_module	,__FILE__,__LINE__);
	$log->debug('■$session       :'.$session		,__FILE__,__LINE__);
	$log->debug('■$mode          :'.$mode			,__FILE__,__LINE__);

	//##########################################################################
	// データベース接続
	//##########################################################################
	$mdb2 = MDB2::connect(CMX_DB_DSN);

	//require_once(dirname(__FILE__) . "/../model/search/cl_mst_inside_training_schedule_util.php");
	require_once(dirname(__FILE__) . "/../model/search/cl_mst_inside_training_schedule_util.php");
	$model = new cl_mst_inside_training_schedule_util($mdb2, $login_user);
	// 2012/11/16 Yamagawa upd(s)
	//$data = $model->getList($training_id);
	$data = $model->getList($training_id ,$year);
	// 2012/11/16 Yamagawa upd(e)

	require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_model.php");
	$cl_mst_inside_training_model = new cl_mst_inside_training_model($mdb2, $login_user);
	$data2=$cl_mst_inside_training_model->select($training_id);

	$training_name=$data2["training_name"];
	$log->debug("研修名：".$training_name,__FILE__,__LINE__);

	$log->debug("ログインユーザ：".$login_user						,__FILE__,__LINE__);
	$log->debug("主催者　　　　：".$data2["training_opener"		]	,__FILE__,__LINE__);
	$log->debug("講師１　　　　：".$data2["training_teacher1"	]	,__FILE__,__LINE__);
	$log->debug("講師２　　　　：".$data2["training_teacher2"	]	,__FILE__,__LINE__);
	$log->debug("講師３　　　　：".$data2["training_teacher3"	]	,__FILE__,__LINE__);
	$log->debug("講師４　　　　：".$data2["training_teacher4"	]	,__FILE__,__LINE__);
	$log->debug("講師５　　　　：".$data2["training_teacher5"	]	,__FILE__,__LINE__);

	$training_opener_flg=false;
	if( strcmp( $login_user , $data2["training_opener"]   ) == 0 ) $training_opener_flg=true;
	if( strcmp( $login_user , $data2["training_teacher1"] ) == 0 ) $training_opener_flg=true;
	if( strcmp( $login_user , $data2["training_teacher2"] ) == 0 ) $training_opener_flg=true;
	if( strcmp( $login_user , $data2["training_teacher3"] ) == 0 ) $training_opener_flg=true;
	if( strcmp( $login_user , $data2["training_teacher4"] ) == 0 ) $training_opener_flg=true;
	if( strcmp( $login_user , $data2["training_teacher5"] ) == 0 ) $training_opener_flg=true;

	// 2012/07/26 Yamagawa add(s)
	$control_date = '';
	$status = '';
	$term_from = '';
	$term_to = '';
	// 2012/07/26 Yamagawa add(e)

	// 2012/04/12 Yamagawa add(s)
	// 「23:59」は「24:00」に変換（暫定対応）
	for($i = 0; $i < count($data); $i++) {
		if (
			$data[$i]['from_time_hh24'] == "23"
			&& $data[$i]['from_time_mi'] == "59"
		) {
			$data[$i]['from_time_hh24'] = "24";
			$data[$i]['from_time_mi'] = "00";
		}
		if (
			$data[$i]['to_time_hh24'] == "23"
			&& $data[$i]['to_time_mi'] == "59"
		) {
			$data[$i]['to_time_hh24'] = "24";
			$data[$i]['to_time_mi'] = "00";
		}

		//2012/05/11 K.Fujii ins(s)
		$data[$i]['place'] = str_replace('\'\'',   '&#039;', $data[$i]['place']);
		$data[$i]['place'] = str_replace('\\\\',   '\\',     $data[$i]['place']);

		$data[$i]['remarks_br'] = str_replace('\'\'',   '&#039;', $data[$i]['remarks_br']);
		$data[$i]['remarks_br'] = str_replace('"',      '&quot;', $data[$i]['remarks_br']);
		$data[$i]['remarks_br'] = str_replace('\\\\',   '\\',     $data[$i]['remarks_br']);
		$data[$i]['remarks_br'] = str_replace('&lt;',   '<',      $data[$i]['remarks_br']);
		$data[$i]['remarks_br'] = str_replace('&gt;',   '>',      $data[$i]['remarks_br']);
		//2012/05/11 K.Fujii ins(e)

		// 2012/07/26 Yamagawa add(s)
		if ($control_date == '' || $control_date >= $data[$i]['plan_date_str']) {
			$control_date = $data[$i]['plan_date_str'];
		}
		// 2012/07/26 Yamagawa add(e)

	}
	$control_date = substr($control_date,5,2).substr($control_date,8);
	$log->debug("control_date：".$control_date,__FILE__,__LINE__);
	// 2012/04/12 Yamagawa add(e)

	// 2012/07/26 Yamagawa add(s)
	if ($data2['auto_control_flg'] == "t") {

		$log->debug("期間設定マスタモデル呼出開始",__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/master/cl_mst_reception_term_model.php");
		$cl_mst_reception_term_model = new cl_mst_reception_term_model($mdb2, $login_user);
		$log->debug("期間設定マスタモデル呼出終了",__FILE__,__LINE__);

		// 2012/11/20 Yamagawa upd(s)
		/*
		// 設定マスタ（上期）取得
		$log->debug("設定マスタ（上期）取得開始",__FILE__,__LINE__);
		$first_term = $cl_mst_reception_term_model->getRecordByDiv(1);
		$log->debug("設定マスタ（上期）取得終了",__FILE__,__LINE__);

		// 設定マスタ（下期）取得
		$log->debug("設定マスタ（下期）取得開始",__FILE__,__LINE__);
		$second_term = $cl_mst_reception_term_model->getRecordByDiv(2);
		$log->debug("設定マスタ（下期）取得終了",__FILE__,__LINE__);

		// 上期・下期を判断する
		if (
				'0401' <= $control_date
			&&	$control_date <= '0930'
		) {
			$term_from = $first_term['term_from_date'];
			$term_to = $first_term['term_to_date'];
		} else {
			$term_from = $second_term['term_from_date'];
			$term_to = $second_term['term_to_date'];
		}
		*/
		// 設定マスタ取得
		$log->debug("設定マスタ取得開始",__FILE__,__LINE__);
		$term = $cl_mst_reception_term_model->getRecordByDiv($data2['term_div']);
		$log->debug("設定マスタ取得終了",__FILE__,__LINE__);

		$term_from = $term['term_from_date'];
		$term_to = $term['term_to_date'];
		// 2012/11/20 Yamagawa upd(e)

		$log->debug("term_from:".$term_from);
		$log->debug("term_to:".$term_to);
		if ($term_from != '' && $term_to != '') {
			// ステータスを判断する
			if (
					$term_from <= $term_to
				&&	$term_from <= date("md")
				&&	date("md") <= $term_to
			) {
				$status = '受付中';
			} else if (
					$term_from > $term_to
				&&	'00'.$term_from <= '01'.date("md")
				&&	'01'.date("md") <= '01'.$term_to
			) {
				$status = '受付中';
			} else if ($term_from > date("md")){
				$status = '準備中';
			} else if (date("md") > $term_to) {
				$status = '受付完了';
			}

			// ステータスを書き換え
			for($i = 0; $i < count($data); $i++) {
				$data[$i]['apply_status_str'] = $status;
			}
		}
	}
	// 2012/07/26 Yamagawa add(e)

	// 2012/08/22 Yamagawa add(s)
	require_once(dirname(__FILE__) . "/../model/template/cl_apl_inside_training_answer_model.php");
	$cl_apl_inside_training_answer_model = new cl_apl_inside_training_answer_model($mdb2, $emp_id);
	// 2012/11/16 Yamagawa upd(s)
	//$data_ans = $cl_apl_inside_training_answer_model->totalization_by_training_id($training_id);
	$data_ans = $cl_apl_inside_training_answer_model->totalization_by_training_id($training_id ,$year);
	// 2012/11/16 Yamagawa upd(e)
	$data_ans["achievement_best_name"] = ACHIEVEMENT_BEST_NM;
	$data_ans["achievement_good_name"] = ACHIEVEMENT_GOOD_NM;
	$data_ans["achievement_not_name"] = ACHIEVEMENT_NOT_NM;
	$data_ans["achievement_bad_name"] = ACHIEVEMENT_BAD_NM;
	$data_ans["learned_best_name"] = LEARNED_BEST_NM;
	$data_ans["learned_good_name"] = LEARNED_GOOD_NM;
	$data_ans["learned_not_name"] = LEARNED_NOT_NM;
	$data_ans["learned_bad_name"] = LEARNED_BAD_NM;
	$data_ans["plan_best_name"] = PLAN_BEST_NM;
	$data_ans["plan_bad_name"] = PLAN_BAD_NM;
	$data_ans["method_best_name"] = METHOD_BEST_NM;
	$data_ans["method_good_name"] = METHOD_GOOD_NM;
	$data_ans["method_not_name"] = METHOD_NOT_NM;
	$data_ans["contents_best_name"] = CONTENTS_BEST_NM;
	$data_ans["contents_good_name"] = CONTENTS_GOOD_NM;
	$data_ans["contents_not_name"] = CONTENTS_NOT_NM;
	$data_ans["contents_bad_name"] = CONTENTS_BAD_NM;
	$data_ans["practice_best_name"] = PRACTICE_BEST_NM;
	$data_ans["practice_good_name"] = PRACTICE_GOOD_NM;
	$data_ans["practice_not_name"] = PRACTICE_NOT_NM;
	$data_ans["practice_bad_name"] = PRACTICE_BAD_NM;
	// 2012/08/22 Yamagawa add(e)

	//------------------------------------------------------------------------------
	// データベース接続を閉じる
	//------------------------------------------------------------------------------
	$log->debug("データベース切断 START",__FILE__,__LINE__);
	$mdb2->disconnect();
	$log->debug("データベース切断 END",__FILE__,__LINE__);

	//##########################################################################
	// 画面表示
	//##########################################################################
	$smarty = new Smarty();
	$smarty->template_dir = dirname(__FILE__) . "/../view";
	$smarty->compile_dir  = dirname(__FILE__) . "/../../templates_c";

	$smarty->assign( 'session'			, $session			);
	$smarty->assign( 'title'			, $training_name.'&nbsp;&nbsp;開催日一覧'		);
	$smarty->assign( 'data'				, $data				);
	$smarty->assign( 'training_id'		, $training_id		);
	$smarty->assign( 'login_user'		, $login_user		);

	$smarty->assign( 'common_module'	, $common_module		);
	$smarty->assign( 'session'			, $session		);
	// 2012/08/22 Yamagawa add(s)
	$smarty->assign( 'answer'			, $data_ans		);
	// 2012/08/22 Yamagawa add(e)

	// 2012/11/16 Yamagawa add(s)
	$smarty->assign( 'option_year'		, $option_year);
	// 2012/11/16 Yamagawa add(e)

	/*
	 * 主催者・講師の場合
	 */
	if( $training_opener_flg == true ){
		$log->debug("編集モード(主催者、講師)",__FILE__,__LINE__);
		$smarty->display(basename(__FILE__,'.php').".tpl");

	}
	/*
	 * 主催者・講師以外の場合
	 */
	else{

		if( $mode == "ro"){
			$log->debug("参照モード",__FILE__,__LINE__);
			$smarty->display(basename(__FILE__,'.php')."_ro.tpl");
		}
		else{
			$log->debug("編集モード",__FILE__,__LINE__);
			$smarty->display(basename(__FILE__,'.php').".tpl");
		}
	}

	$log->info(basename(__FILE__)." END");

	$log->shutdown();

// 2012/11/16 Yamagawa add(s)
	/**
	 * 年オプションHTML取得
	 */
	function get_select_around_years($date)
	{
		ob_start();
		show_years_around(9, 2, $date);
		$str_buff=ob_get_contents();
		ob_end_clean();
		return $str_buff;
	}

	function show_years_around($before_count, $after_count, $num){

		$now  = date(Y);
		if (date(n) <= 3){
			$now -= 1;
		}

		// 翌年以降
		for($i=$after_count; $i>0; $i--){
			$yr = $now + $i;
			echo("<option value=\"".$yr."\"");
			if($num == $yr){
				echo(" selected");
			}
			echo(">".$yr."</option>\n");
		}

		// 当年まで
		for($i=0; $i<=$before_count; $i++){
			$yr = $now - $i;
			echo("<option value=\"".$yr."\"");
			if($num == $yr){
				echo(" selected");
			}
			echo(">".$yr."</option>\n");
		}

	}
// 2012/11/16 Yamagawa add(e)