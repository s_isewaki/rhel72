<?php

//ini_set( 'display_errors', 1 );

	//##########################################################################
	// 各種モジュールの読み込み
	//##########################################################################
	//require_once(dirname(__FILE__) . "/../../about_session.php");
	//require_once(dirname(__FILE__) . "/../../about_authority.php");
	//require_once(dirname(__FILE__) . "/../../about_postgres.php");
	//require_once(dirname(__FILE__) . "/../../show_select_values.ini");
	//require_once(dirname(__FILE__) . "/../../cl_application_list.inc");
	//require_once(dirname(__FILE__) . "/../../cl_yui_calendar_util.ini");
	//require_once(dirname(__FILE__) . "/../../cl_application_common.ini");
	//require_once(dirname(__FILE__) . "/../../cl_common.ini");
	//require_once(dirname(__FILE__) . "/../../cl_title_name.ini");
	require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
	require_once(dirname(__FILE__) . "/../../Cmx.php");
	require_once("MDB2.php");

	//##########################################################################
	// 初期処理
	//##########################################################################

	//ロガーをロード
	$log = new cl_common_log_class(basename(__FILE__));
	$log->info(basename(__FILE__)." START");


	//====================================
	//画面名
	//====================================
	$fname = $PHP_SELF;

	//##########################################################################
	// パラメータ取得
	//##########################################################################
	foreach( $_REQUEST as $key => $value ) {
		$log->debug('$_REQUEST * ['.$key.']:'.$_REQUEST[$key],__FILE__,__LINE__);
	}
		$training_id	= $_POST["training_id"];
		$plan_id		= $_POST["plan_id"];
		$login_user		= $_POST["login_user"];
		$common_module	= $_POST["common_module"];
		$session		= $_POST["session"];
		$login_user		= $_POST["login_user"];
		$emp_id			= $_POST["login_user"];
		$p_plan_id		= $_POST["plan_id"];


		$log->debug('■$training_id0   :'.$training_id	,__FILE__,__LINE__);
		$log->debug('■$plan_id*1  	  :'.$plan_id		,__FILE__,__LINE__);
		$log->debug('■$login_user 0   :'.$login_user	,__FILE__,__LINE__);
		$log->debug('■$common_module0 :'.$common_module	,__FILE__,__LINE__);
		$log->debug('■$session0       :'.$session		,__FILE__,__LINE__);

/*
	//====================================
	//セッションのチェック
	//====================================
	$session = qualify_session($session,$fname);
	if($session == "0"){
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showLoginPage(window);</script>");
		exit;
	}

	//====================================
	//権限チェック
	//====================================
	$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
	if($wkfw=="0"){
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showLoginPage(window);</script>");
		exit;
	}
	*/

	// ワークフロー権限の取得
	//$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);
	$log->debug('ワークフロー権限の取得 OK'	,__FILE__,__LINE__);

	// データベース接続
	$mdb2 = MDB2::connect(CMX_DB_DSN);
	$log->debug('データベース接続 OK'	,__FILE__,__LINE__);


	// セッションよりログインユーザー取得
	require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php");
	$empmst_model = new empmst_model($mdb2,$emp_id);
	$arr_empmst = $empmst_model->select_login_emp($session);

	$emp_id = $arr_empmst["emp_id"];

	$log->debug('■$emp_id2   :'.$emp_id	,__FILE__,__LINE__);

	// ヘッダーデータ取得
	require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_model.php");
	$cl_mst_inside_training_model = new cl_mst_inside_training_model($mdb2,$emp_id);
	$arr_training = $cl_mst_inside_training_model->select($training_id);

	foreach($arr_training as $tmp){
	//	foreach($tmp as $tmpp){
		$log->debug('■$arr_training   :'.$tmp	,__FILE__,__LINE__);
	//	}
	}

	require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_schedule_model.php");
	$cl_mst_inside_training_schedule_model = new cl_mst_inside_training_schedule_model($mdb2,$emp_id);
	$arr_schedule = $cl_mst_inside_training_schedule_model->select($p_plan_id);
	$arr_date = explode("-",$arr_schedule['plan_date']);
	$plan_date_year = $arr_date[0];
	$plan_date_month = $arr_date[1];
	$plan_date_day = $arr_date[2];

	$log->debug('■$plan_date   :'.$arr_schedule['plan_date']	,__FILE__,__LINE__);

	// 一覧データ取得
//	require_once(dirname(__FILE__) . "/../model/workflow/cl_apl_inside_training_apply_model.php");
//	$cl_apl_inside_training_apply_model = new cl_apl_inside_training_apply_model($mdb2, $emp_id);
	require_once(dirname(__FILE__) . "/../model/workflow/cl_inside_training_roll_model.php");
	$cl_inside_training_roll_model = new cl_inside_training_roll_model($mdb2, $emp_id);

//	$param = array(
//		"inside_training_id"	=> $training_id,
//		"plan_id"				=> $plan_id
//	);

	$param = array(
		"training_id"	=> $training_id,
		"plan_id"		=> $p_plan_id
	);

//	$data = $cl_apl_inside_training_apply_model->getReceptionList($param);
	$data = $cl_inside_training_roll_model->getReceptionList($param);

	// 取得したデータでループ
	$rowcnt = count($data);
	for ($index = 0; $index < $rowcnt; $index++){
		$data[$index]['no'] = $index + 1;

		// 行番号・出欠フラグ
		switch($data[$index]['emp_roll']){
			case 0:
				$data[$index]['emp_roll'] = '';
				break;
			case 1:
				$data[$index]['emp_roll'] = '出席';
				break;
			case 2:
				$data[$index]['emp_roll'] = '欠席';
				break;
			default:
				$data[$index]['emp_roll'] = '';
				break;
		}
	}


	// 出席者一覧CSV出力
	if ($_POST['attendance_csv_flg'] == 'true'){
	$log->debug('出席者一覧CSV出力 ',__FILE__,__LINE__);

		// ヘッダー行
		$csv_header  =     format_column_for_csv('No');
		$csv_header .= ",".format_column_for_csv('職員ID');
		$csv_header .= ",".format_column_for_csv('職員氏名');
		$csv_header .= ",".format_column_for_csv('所属');
		$csv_header .= ",".format_column_for_csv('出欠');
		// 2012/09/03 Yamagawa add(s)
		$csv_header .= ",".format_column_for_csv('報告状況');
		// 2012/09/03 Yamagawa add(e)

		// 明細行
		foreach ($data as $row){
//			if($row['emp_roll'] !=1) continue;

			$csv_body .= format_column_for_csv($row['no']);
			$csv_body .= ",". format_column_for_csv($row['emp_id']);
			$csv_body .= ",". format_column_for_csv($row['emp_name']);
			$csv_body .= ",". format_column_for_csv($row['affiliation']);
			$csv_body .= ",". format_column_for_csv($row['emp_roll'] );
			// 2012/09/03 Yamagawa add(s)
			$csv_body .= ",". format_column_for_csv($row['report_status']);
			// 2012/09/03 Yamagawa add(e)
			$csv_body .= "\r\n";
		}

		$csv = $csv_header ."\r\n". $csv_body;


		// ファイル名の先頭に研修日程id(plan_id)を付与
		$file_name = $p_plan_id."_attendance.csv";


		// CSV出力
		header("Content-Disposition: attachment; filename=$file_name");
		header("Content-Type: application/octet-stream; name=$file_name");
		header("Content-Length: " . strlen($csv));
		echo($csv);

	}



	// アンケートCSV出力
	if ($_POST['answer_csv_flg'] == 'true') {

		$log->debug('アンケートCSV出力 ',__FILE__,__LINE__);

		// アンケートデータ取得
		require_once(dirname(__FILE__) . "/../model/template/cl_apl_inside_training_answer_model.php");
		$cl_apl_inside_training_answer_model = new cl_apl_inside_training_answer_model($mdb2, $emp_id);

		$arr_answer = $cl_apl_inside_training_answer_model->select_by_plan_id($p_plan_id);


		// ヘッダー行
		// 項目配列作成
		$array_csv_header = array();
		$array_csv_header[] = "研修の目的、目標は達成できましたか？";
		$array_csv_header[] = "ご自身の学習課題は達成できましたか？";
		$array_csv_header[] = "研修の日時は適切だと思いましたか？";
		$array_csv_header[] = "研修の日時は適切だと思いましたか？ 理由";
		$array_csv_header[] = "研修の方法（進め方）は適切だと思いましたか？";
		$array_csv_header[] = "研修の方法（進め方）は適切だと思いましたか？ 理由";
		$array_csv_header[] = "研修の内容は分かりやすいものでしたか？";
		$array_csv_header[] = "研修の内容は分かりやすいものでしたか？ 理由";
		$array_csv_header[] = "研修内容は今後の看護実践に役立ちますか？";
		$array_csv_header[] = "研修内容は今後の看護実践に役立ちますか？ 理由";
		$array_csv_header[] = "研修全体を通して気づいた点をご記入ください";

		// 文字コード変換
		$csv_header = array();
		foreach($array_csv_header as $tmp_csv_header){
			$csv_header[] = format_column_for_csv($tmp_csv_header);
		}

		// 配列から文字列化
		$csv_header = implode(",", $csv_header);


		// 明細行
			$ans_cnt=0;
		foreach ($arr_answer as $row){
			$ans_cnt++;
			$csv_body .=      format_column_for_csv($row['achievement_degree']);
			$csv_body .= ",". format_column_for_csv($row['mine_learned']);
			$csv_body .= ",". format_column_for_csv($row['training_plan_degree']);
			$csv_body .= ",". format_column_for_csv($row['training_plan_degree_reason']);
			$csv_body .= ",". format_column_for_csv($row['training_method']);
			$csv_body .= ",". format_column_for_csv($row['training_method_reason']);
			$csv_body .= ",". format_column_for_csv($row['training_contents']);
			$csv_body .= ",". format_column_for_csv($row['training_contents_reason']);
			$csv_body .= ",". format_column_for_csv($row['training_practice']);
			$csv_body .= ",". format_column_for_csv($row['training_practice_reason']);
			$csv_body .= ",". format_column_for_csv($row['point_notice']);
			$csv_body .= "\r\n";
		}

		$log->debug('■csv_body   '.$csv_body,__FILE__,__LINE__);


	//	$csv_body = implode(",", $csv_body);

		// ヘッダーと本文を結合
		$csv = $csv_header ."\r\n". $csv_body;

		// ファイル名の先頭に研修日程id(plan_id)を付与
		$file_name = $p_plan_id."_answer_list.csv";

		// CSV出力
		header("Content-Disposition: attachment; filename=$file_name");
		header("Content-Type: application/octet-stream; name=$file_name");
		header("Content-Length: " . strlen($csv));
		echo($csv);

	}


	// データベース切断
	$mdb2->disconnect();


	// 文字コード変換
	function format_column_for_csv($value, $from_encoding = "EUC-JP") {
		$buf = str_replace("\r", "", $value);
		$buf = str_replace("\n", "", $buf);
		if (strpos($buf, ",") !== false)  {
			$buf = '"' . $buf . '"';
		}
		return mb_convert_encoding($buf, "Shift_JIS", $from_encoding);
	}


$log->info(basename(__FILE__)." END");
$log->shutdown();

?>