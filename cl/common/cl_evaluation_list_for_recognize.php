<?
	//ini_set( 'display_errors', 1 );

	//##########################################################################
	// 各種モジュールの読み込み
	//##########################################################################
	require_once(dirname(__FILE__) . "/../../about_session.php");
	require_once(dirname(__FILE__) . "/../../about_authority.php");
	require_once(dirname(__FILE__) . "/../../about_postgres.php");
	require_once(dirname(__FILE__) . "/../../show_select_values.ini");
	require_once(dirname(__FILE__) . "/../../cl_application_list.inc");
	require_once(dirname(__FILE__) . "/../../cl_yui_calendar_util.ini");
	require_once(dirname(__FILE__) . "/../../cl_application_common.ini");
	require_once(dirname(__FILE__) . "/../../cl_common.ini");
	require_once(dirname(__FILE__) . "/../../cl_title_name.ini");
	require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
	require_once(dirname(__FILE__) . "/../../Cmx.php");
	require_once("Smarty/Smarty.class.php");
	require_once("MDB2.php");

	//##########################################################################
	// 初期処理
	//##########################################################################
	//ロガーをロード
	$log = new cl_common_log_class(basename(__FILE__));
	$log->info(basename(__FILE__)." START");

	//画面名
	$fname = $PHP_SELF;

	// パラメータ取得
	//##########################################################################
	foreach( $_REQUEST as $key => $value ) {
		$log->debug('$_REQUEST['.$key.']:'.$_REQUEST[$key],__FILE__,__LINE__);
	}

	$session		= $_POST["p_session"];
	$login_user		= $_POST["login_user"];
	$target_level	= $_POST["target_level"];
	$evaluation_emp_division = $_POST["evaluation_emp_division"];
	$levelup_apply_id = $_POST["levelup_apply_id"];
	$must_check = $_POST["must_check"];

	$log->debug('■$session       :'.$session		,__FILE__,__LINE__);
	$log->debug('■$login_user    :'.$login_user	,__FILE__,__LINE__);
	$log->debug('■$target_level  :'.$target_level	,__FILE__,__LINE__);
	$log->debug('■$evaluation_emp_division  :'.$evaluation_emp_division		,__FILE__,__LINE__);
	$log->debug('■$levelup_apply_id    :'.$levelup_apply_id	,__FILE__,__LINE__);
	$log->debug('■$must_check    :'.$must_check	,__FILE__,__LINE__);

	//====================================
	// データベース接続
	//====================================
	$mdb2 = MDB2::connect(CMX_DB_DSN);

	//====================================
	// モデルをロード
	//====================================
	$log->debug('レベルアップ評価表（認定用）モデル START',__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/search/cl_evaluation_list_for_recognize_model.php");
	$cl_evaluation_list_for_recognize_model = new cl_evaluation_list_for_recognize_model($mdb2,$login_user);
	$log->debug('レベルアップ評価表（認定用）モデル END',__FILE__,__LINE__);

	//====================================
	// データ取得
	//====================================
	$data = $cl_evaluation_list_for_recognize_model->get_evaluation_list($evaluation_emp_division,$levelup_apply_id);

	//====================================
	// データベース切断
	//====================================
	$log->debug('データベース切断 START',__FILE__,__LINE__);
	$mdb2->disconnect();
	$log->debug('データベース切断 END',__FILE__,__LINE__);

	//##########################################################################
	// 画面表示
	//##########################################################################
	$smarty = new Smarty();
	$smarty->template_dir = dirname(__FILE__) . "/../view";
	$smarty->compile_dir  = dirname(__FILE__) . "/../../templates_c";

	$smarty->assign( 'session'			, $session			);
	$smarty->assign( 'login_user'		, $login_user		);
	$smarty->assign( 'target_level'		, $target_level		);
	$smarty->assign( 'evaluation_emp_division'		, $evaluation_emp_division		);
	$smarty->assign( 'must_check'		, $must_check		);
	$smarty->assign( 'data'				, $data				);

	$smarty->display(basename(__FILE__,'.php').".tpl");


	$log->info(basename(__FILE__)." END");

	$log->shutdown();

