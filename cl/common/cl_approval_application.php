<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_application_category_list.ini");
require_once("./cl/common/cl_approval_application_template.ini");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("get_values.ini");
require_once("application_draft_template.ini");
require_once("application_workflow_common_class.php");
require_once("application_common.ini");
require_once("get_values_for_template.ini");
require_once("library_common.php");
require_once("MDB2.php");

//##########################################################################
// パラメータ取得
//##########################################################################
foreach( $_REQUEST as $key => $value ) {
	$log->debug('$_REQUEST['.$key.']:'.$_REQUEST[$key],__FILE__,__LINE__);
}

	$apply_mode		= $_POST["apply_mode"];
	$trip_division		= $_POST["trip_division"];
	$short_wkfw_name	= $_POST["short_wkfw_name"];
	$seminar_apply_id	= $_POST["seminar_apply_id"];
	$session		= $_POST["session"];

	$log->debug('■$apply_mode   :'.$apply_mode	,__FILE__,__LINE__);
	$log->debug('■$trip_division  	  :'.$trip_division		,__FILE__,__LINE__);
	$log->debug('■$short_wkfw_name    :'.$short_wkfw_name	,__FILE__,__LINE__);
	$log->debug('■$seminar_apply_id    :'.$sseminar_apply_id	,__FILE__,__LINE__);
	$log->debug('■$session :'.$session	,__FILE__,__LINE__);

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);

if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,7,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, 3, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//------------------------------------------------------------------------------
// データベース接続
//------------------------------------------------------------------------------
$mdb2 = MDB2::connect(CMX_DB_DSN);

// 院外研修データ取得
require_once(dirname(__FILE__) . "/../model/search/cl_approval_application_model.php");
$cl_approval_application_model = new cl_approval_application_model($mdb2, $emp_id);

$outside_seminar_data = $cl_approval_application_model->get_outside_seminar($seminar_apply_id);

//------------------------------------------------------------------------------
// データベース接続を閉じる
//------------------------------------------------------------------------------
$mdb2->disconnect();

$obj = new application_workflow_common_class($con, $fname);

require_once("cl/common/cl_approval_application_config.php");

if($apply_mode == '0'){
	//出張届の場合
	if($trip_division == "1"){
		//日帰り
		if($short_wkfw_name == "c140"){
			//主任以下
			$wkfw_id = mode_0_trip_1_c140_wkfw_id;
			$template_name = "business_trip_application_1d_general";
		}elseif($short_wkfw_name == "c150"){
			//師長
			$wkfw_id = mode_0_trip_1_c150_wkfw_id;
			$template_name = "business_trip_application_1d_chief";
		}
	}elseif($trip_division == "2"){
		//２日以上４日以内
		if($short_wkfw_name == "c140"){
			$wkfw_id = mode_0_trip_2_c140_wkfw_id;
			$template_name = "business_trip_application_4d_general";
		}elseif($short_wkfw_name == "c150"){
			$wkfw_id = mode_0_trip_2_c150_wkfw_id;
			$template_name = "business_trip_application_4d_chief";
		}
	}elseif($trip_division == "3"){
		//５日以上
		if($short_wkfw_name == "c140"){
			$wkfw_id = mode_0_trip_3_c140_wkfw_id;
			$template_name = "business_trip_application_over_5d_general";
		}elseif($short_wkfw_name == "c150"){
			$wkfw_id = mode_0_trip_3_c150_wkfw_id;
			$template_name = "business_trip_application_over_5d_chief";
		}
	}else{
		//長期出張
		if($short_wkfw_name == "c140"){
			$wkfw_id = mode_0_trip_4_c140_wkfw_id;
			$template_name = "business_trip_application_long_general";
		}elseif($short_wkfw_name == "c150"){
			$wkfw_id = mode_0_trip_4_c150_wkfw_id;
			$template_name = "business_trip_application_long_chief";
		}
	}
}else{
	//出張報告の場合
	if($trip_division == "1"){
		//日帰り
		if($short_wkfw_name == "c140"){
			//主任以下
			$wkfw_id = mode_1_trip_1_c140_wkfw_id;
			$shinse_wkfw_id = mode_0_trip_1_c140_wkfw_id;
			$template_name = "business_trip_report_1d_general";
		}elseif($short_wkfw_name == "c150"){
			//師長
			$wkfw_id = mode_1_trip_1_c150_wkfw_id;
			$shinse_wkfw_id = mode_0_trip_1_c150_wkfw_id;
			$template_name = "business_trip_report_1d_chief";
		}
	}elseif($trip_division == "2"){
		//２日以上４日以内
		if($short_wkfw_name == "c140"){
			$wkfw_id = mode_1_trip_2_c140_wkfw_id;
			$shinse_wkfw_id = mode_0_trip_2_c140_wkfw_id;
			$template_name = "business_trip_report_4d_general";
		}elseif($short_wkfw_name == "c150"){
			$wkfw_id = mode_1_trip_2_c150_wkfw_id;
			$shinse_wkfw_id = mode_0_trip_2_c150_wkfw_id;
			$template_name = "business_trip_report_1d_chief";
		}
	}elseif($trip_division == "3"){
		//５日以上
		if($short_wkfw_name == "c140"){
			$wkfw_id = mode_1_trip_3_c140_wkfw_id;
			$shinse_wkfw_id = mode_0_trip_3_c140_wkfw_id;
			$template_name = "business_trip_report_over_5d_general";
		}elseif($short_wkfw_name == "c150"){
			$wkfw_id = mode_1_trip_3_c150_wkfw_id;
			$shinse_wkfw_id = mode_0_trip_3_c150_wkfw_id;
			$template_name = "business_trip_report_over_5d_chief";
		}
	}else{
		//長期出張
		if($short_wkfw_name == "c140"){
			$wkfw_id = mode_1_trip_4_c140_wkfw_id;
			$shinse_wkfw_id = mode_0_trip_4_c140_wkfw_id;
			$template_name = "business_trip_report_long_general";
		}elseif($short_wkfw_name == "c150"){
			$wkfw_id = mode_1_trip_4_c150_wkfw_id;
			$shinse_wkfw_id = mode_0_trip_4_c150_wkfw_id;
			$template_name = "business_trip_report_long_chief";
		}
	}
}

$page_title ="決裁・申請";

$sql = " SELECT a.apply_id, a.wkfw_id ";
$sql .= " FROM apply a ";
$sql .= " LEFT JOIN session s USING (emp_id) ";
$sql .= " LEFT JOIN wkfwmst USING (wkfw_id) ";
$sql .= " LEFT JOIN trip_link tl ON a.apply_id=tl.trip1_id ";
$sql .= " LEFT JOIN apply t2 ON tl.trip2_id=t2.apply_id AND NOT t2.delete_flg ";
$sql .= " WHERE s.session_id='$session' AND a.apply_stat=1 AND (t2.apply_stat IS NULL OR t2.apply_stat=2) AND not a.draft_flg AND not a.delete_flg AND a.re_apply_id IS NULL AND a.wkfw_id IN (SELECT wkfw_id FROM trip_workflow WHERE type=1) ";
$sql .= " ORDER BY a.apply_date DESC ";
$sql .= " LIMIT 20 ";

$sel = select_from_table($con,$sql,"",$fname);
if ($sel != 0) {
	$todoke = pg_fetch_all($sel);
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>決裁・申請 | 申請</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript" src="js/domready.js"></script>
<?

	// ワークフロー情報取得
	$arr_wkfwmst			= $obj->get_wkfwmst($wkfw_id);
	$wkfw_content			= $arr_wkfwmst[0]["wkfw_content"];
	$wkfw_content_type		= $arr_wkfwmst[0]["wkfw_content_type"];
	$approve_label			= $arr_wkfwmst[0]["approve_label"];
	$wkfw_type			= $arr_wkfwmst[0]["wkfw_type"];


	// 本文形式タイプのデフォルトを「テキスト」とする
	if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

	// 本文形式タイプのデフォルトを「テンプレート」の場合
	if($wkfw_content_type == "2")
	{
		$wkfw_history_no = $obj->get_max_wkfw_history_no($wkfw_id);
	}

	$num = 0;
	$pos = 0;
	while (1) {
		$pos = strpos($wkfw_content, 'show_cal', $pos);
		if ($pos === false) {
			break;
		} else {
			$num++;
		}
		$pos++;
	}

	// 外部ファイルを読み込む（$num==0でも読み込んでください）
	write_yui_calendar_use_file_read_0_12_2();
	// カレンダー作成、関数出力
	write_yui_calendar_script2($num);


$approve_label = ($approve_label != "2") ? "承認" : "確認";

print_r($todoke->rows);
?>
<script type="text/javascript">
//2012/05/07 K.Fujii ins(s)
Event.domReady.add(init);
//2012/05/07 K.Fujii ins(e)

function init() {
	if('<?=$back?>' == 't') {
	<?

		$approve_num = $_POST[approve_num];
		for($i=1; $i<=$approve_num;$i++) {

			$regist_emp_id = "regist_emp_id$i";
			$radio_emp_id = "radio_emp_id$i";
			$approve_name = "approve_name$i";

	?>
			var radio_emp_id = 'radio_emp_id' + <?=$i?>;
			if(document.getElementById('<?=$radio_emp_id?>')) {
				for(j=0; j<document.apply.elements['<?=$radio_emp_id?>'].length; j++) {
					if(document.apply.elements['<?=$radio_emp_id?>'][j].value == '<?=$_POST[$regist_emp_id];?>') {
						document.apply.elements['<?=$radio_emp_id?>'][j].checked = true;
					}
				}
				document.apply.elements['<?=$regist_emp_id?>'].value = '<?=$_POST[$regist_emp_id];?>';
			}
			if(document.getElementById('<?=$approve_name?>')) {
				document.apply.elements['<?=$approve_name?>'].value = '<?=$_POST[$approve_name];?>';
				document.apply.elements['<?=$regist_emp_id?>'].value = '<?=$_POST[$regist_emp_id];?>';
			}
	<?
		}
	?>
	}

	<? if($apply_mode == '0'){ ?>
		//出張届用初期処理
		//種別の設定
		<?
		if($outside_seminar_data["expense_division"] == "2"){
			if($outside_seminar_data["trip_division"]=="1"){
				echo("document.getElementById('kubun6').checked = true;");
			}else if($outside_seminar_data["trip_division"]=="2"){
				echo("document.getElementById('kubun7').checked = true;");
			}else{
				echo("document.getElementById('kubun8').checked = true;");
			}
		}else{
			if($outside_seminar_data["trip_division"]=="1"){
				echo("document.getElementById('kubun1').checked = true;");
			}else if($outside_seminar_data["trip_division"]=="2"){
				echo("document.getElementById('kubun2').checked = true;");
			}else if($outside_seminar_data["trip_division"]=="3"){
				echo("document.getElementById('kubun3').checked = true;");
			}else{
				echo("document.getElementById('kubun4').checked = true;");
			}
		}
		?>
		//期間の設定
		document.getElementById('date_y1').value = "<?=substr($outside_seminar_data["from_date"],0,4); ?>";
		document.getElementById('date_m1').value = "<?=substr($outside_seminar_data["from_date"],5,2); ?>";
		document.getElementById('date_d1').value = "<?=substr($outside_seminar_data["from_date"],8,2); ?>";
		document.getElementById('date_y2').value = "<?=substr($outside_seminar_data["to_date"],0,4); ?>";
		document.getElementById('date_m2').value = "<?=substr($outside_seminar_data["to_date"],5,2); ?>";
		document.getElementById('date_d2').value = "<?=substr($outside_seminar_data["to_date"],8,2); ?>";

		//出張場所設定
		document.getElementById('youken1').checked = true;
		document.getElementById('place').value = "<?=preg_replace('/\"/','\\\"',$outside_seminar_data["open_place"])?>";
		document.getElementById('gakkai_name').value = "<?=preg_replace('/\"/','\\\"',$outside_seminar_data["seminar_name"])?>";
	<? }else{ ?>
		//出張報告用初期処理
		var date_y1 = "<?=substr($outside_seminar_data["from_date"],0,4); ?>";
		var date_m1 = "<?=substr($outside_seminar_data["from_date"],5,2); ?>";
		var date_d1 = "<?=substr($outside_seminar_data["from_date"],8,2); ?>";
		var date_y2 = "<?=substr($outside_seminar_data["to_date"],0,4); ?>";
		var date_m2 = "<?=substr($outside_seminar_data["to_date"],5,2); ?>";
		var date_d2 = "<?=substr($outside_seminar_data["to_date"],8,2); ?>";

		document.getElementById("application_mode").value = "menu";

		<? foreach($todoke as $idx=>$row){
			if($row['wkfw_id'] == $shinse_wkfw_id){
		?>
				if(trip1["<?=$row['apply_id']?>"]["date_y1"] == date_y1 && trip1["<?=$row['apply_id']?>"]["date_m1"] == date_m1 && trip1["<?=$row['apply_id']?>"]["date_d1"] == date_d1){
					if(trip1["<?=$row['apply_id']?>"]["date_y2"] == date_y2 && trip1["<?=$row['apply_id']?>"]["date_m2"] == date_m2 && trip1["<?=$row['apply_id']?>"]["date_d2"] == date_d2){
						document.getElementById('trip1select').value = <?=$row['apply_id']?>;
						//期間の設定
						document.getElementById('date_y1').value = "<?=substr($outside_seminar_data["from_date"],0,4); ?>";
						document.getElementById('date_m1').value = "<?=substr($outside_seminar_data["from_date"],5,2); ?>";
						document.getElementById('date_d1').value = "<?=substr($outside_seminar_data["from_date"],8,2); ?>";
						document.getElementById('date_y2').value = "<?=substr($outside_seminar_data["to_date"],0,4); ?>";
						document.getElementById('date_m2').value = "<?=substr($outside_seminar_data["to_date"],5,2); ?>";
						document.getElementById('date_d2').value = "<?=substr($outside_seminar_data["to_date"],8,2); ?>";
						return false;
					}
				}
		<?
			}
		 }
		?>

	<? } ?>

}

//添付ファイル追加
function attachFile() {
	window.open('apply_attach.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

//添付ファイル削除
function detachFile(e) {
	if (e == undefined) {
		e = window.event;
	}

	var btn_id;
	if (e.target) {
		btn_id = e.target.getAttribute('id');
	} else {
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_', '');

	var p = document.getElementById('p_' + id);
	document.getElementById('attach').removeChild(p);
}

// 申請処理
function apply_regist(apv_num, precond_num) {

	// 未入力チェック
	for(i=1; i<=precond_num; i++)
	{
		obj = 'precond_apply_id' + i;
		apply_id = document.apply.elements[obj].value;
		if(apply_id == "") {
			alert('<? echo($approve_label); ?>確定の申請書を設定してくだい。');
			return;
		}
	}

	var apv_selected = 0;
	var apv_all_selected = true;
	for(i=1; i<=apv_num; i++) {
        var obj = 'regist_emp_id' + i;
        var radio_obj = 'radio_emp_id' + i;
        var emp_id = document.apply.elements[obj].value;
        var radio = document.apply.elements[radio_obj];
		if (emp_id == "") {
			apv_all_selected = false;
		}
        if (emp_id || (emp_id == "" && radio == null)) {
            apv_selected++;
        }
	}
    if (apv_selected < apv_num) {
		alert('<? echo($approve_label); ?>者を選択してください。');
		return;
	}
	if (!apv_all_selected && !confirm('指定されていない<? echo($approve_label); ?>者がありますが、送信しますか？')) {
		return;
	}

	if (window.InputCheck) {
		if (!InputCheck()) {
			return;
		}
	}

	// 重複チェック
	dpl_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj_src = 'regist_emp_id' + i;
		emp_id_src = document.apply.elements[obj_src].value;
		if (emp_id_src == '') continue;

		for(j=i+1; j<=apv_num; j++) {
			obj_dist = 'regist_emp_id' + j;
			emp_id_dist = document.apply.elements[obj_dist].value;
			if(emp_id_src == emp_id_dist) {
				dpl_flg = true;
			}
		}
	}

	if(dpl_flg)
	{
		if (!confirm('<? echo($approve_label); ?>者が重複していますが、申請しますか？'))
		{
				return;
		}
	}
	alert("申請しました。");
	document.apply.action="application_submit.php";
	document.apply.submit();

	self.close();

}

// 下書き保存
function draft_regist()
{
	document.apply.action="application_submit.php?draft=on";
	document.apply.submit();
	alert("下書き保存しました。");
	self.close();

}

// 印刷
function apply_printwin(id) {
	self.print();
}

// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];
		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">


.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}

table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}


p {margin:0;}
</style>
<style  type="text/css" media="print">
.noprint {display:none}
</style>

<style  type="text/css" media="print">
.noprint {display:none}
</style>

</head>
<!-- 2012/05/07 K.Fujii upd(s) -->
<!--<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init();initcal();
if (window.OnloadSub) { OnloadSub(); };resizeAllTextArea();if (window.refreshApproveOrders) {refreshApproveOrders();}">-->
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initcal();
if (window.OnloadSub) { OnloadSub(); };resizeAllTextArea();if (window.refreshApproveOrders) {refreshApproveOrders();}">
<!-- 2012/05/07 K.Fujii upd(e) -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<div class="noprint">
<!-- ヘッダー START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$page_title?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<!-- ヘッダー END -->
</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<!-- 2012/05/07 K.Fujii upd(s) -->
<!--<form name="apply" action="#" method="post" onload="init();">-->
<form name="apply" action="#" method="post" onload="">
<!-- 2012/05/07 K.Fujii upd(e) -->
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="wkfw_id" value="<?=$wkfw_id?>">
<input type="hidden" name="wkfw_type" value="<?=$wkfw_type?>">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">

<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td valign="top" width="100%">
<?
//テンプレートの表示
show_application_template($con, $session, $fname, $wkfw_id, $apply_title, $content, $file_id, $filename, $back, $mode, $template_name);
?>
</td>
</tr>
</table>
</form>

</td>
</tr>
</table>
<iframe name="printframe" width="0" height="0" frameborder="0"></iframe>
</body>
</html>
<?
pg_close($con);
?>

