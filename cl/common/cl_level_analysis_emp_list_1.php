<?php

//##########################################################################
// 各種モジュールの読み込み
//##########################################################################
require_once(dirname(__FILE__) . "/../../about_session.php");
require_once(dirname(__FILE__) . "/../../about_authority.php");
require_once(dirname(__FILE__) . "/../../about_postgres.php");
require_once(dirname(__FILE__) . "/../../cl_common.ini");
require_once(dirname(__FILE__) . "/../../cl_title_name.ini");
require_once(dirname(__FILE__) . "/../../cl_level_analysis_util.php");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once("Smarty/Smarty.class.php");
require_once("MDB2.php");

//##########################################################################
// 初期処理
//##########################################################################

//ロガーをロード
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

//画面名
$fname = $PHP_SELF;

//##########################################################################
// パラメータ取得
//##########################################################################
foreach( $_REQUEST as $key => $value ) {
	$log->debug('$_REQUEST['.$key.']:'.$_REQUEST[$key],__FILE__,__LINE__);
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

$log->debug('■$workflow_auth   :'.$workflow_auth	,__FILE__,__LINE__);

//##########################################################################
// データベース接続
//##########################################################################
$mdb2 = MDB2::connect(CMX_DB_DSN);

$data = get_emp_list($mdb2, $_POST['login_emp_id']);

//------------------------------------------------------------------------------
// データベース接続を閉じる
//------------------------------------------------------------------------------
$log->debug("データベース切断 START",__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);

//##########################################################################
// 画面表示
//##########################################################################
$smarty = new Smarty();
$smarty->template_dir = dirname(__FILE__) . "/../view";
$smarty->compile_dir  = dirname(__FILE__) . "/../../templates_c";

$smarty->assign( 'cl_title'			, $cl_title							);
$smarty->assign( 'session'			, $session							);
$smarty->assign( 'data'				, $data								);

//テンプレート出力
$smarty->display(basename(__FILE__,'.php').".tpl");

function format_column_for_csv($value, $from_encoding = "EUC-JP") {
	$buf = str_replace("\r", "", $value);
	$buf = str_replace("\n", "", $buf);
	if (strpos($buf, ",") !== false)  {
		$buf = '"' . $buf . '"';
	}
	return mb_convert_encoding($buf, "Shift_JIS", $from_encoding);
}

$log->info(basename(__FILE__)." END");
$log->shutdown();

?>