<?php
//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");
require_once('MDB2.php');
require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_schedule_model_sub.php");



$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

$log->debug('$_GET パラメータ debug START',__FILE__,__LINE__);
foreach( $_GET as $key => $value ) {
	$log->debug('$_GET['.$key.']:'.$_GET[$key],__FILE__,__LINE__);
}
$log->debug('$_GET パラメータ debug END',__FILE__,__LINE__);

$log->debug('$_POST パラメータ debug START',__FILE__,__LINE__);
foreach( $_POST as $key => $value ) {
	$log->debug('$_POST['.$key.']:'.$_POST[$key],__FILE__,__LINE__);
}
$log->debug('$_POST パラメータ debug END',__FILE__,__LINE__);



//$plan_id=$_GET["plan_id"];
$plan_id=$_POST["plan_id"];




$log->debug('plan_id:'.$plan_id,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 申請書データ取得処理を呼び出す
//------------------------------------------------------------------------------
$json=getJsonApplyData($mdb2, $plan_id);

$log->debug('$json:'.$json,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベース接続を閉じる
//------------------------------------------------------------------------------
$log->debug("データベース切断 START",__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);

header("Content-Type: application/json; charset=EUC-JP");
echo $json;

/**
 * 申請書データ取得
 */
function getJsonApplyData($mdb2, $plan_id){
	global $log;
	$log->debug(__FUNCTION__." START");

	//--------------------------------------------------------------------------
	// 院内研修報告テーブルモデルインスタンス生成
	//--------------------------------------------------------------------------
	$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
	$model = new cl_mst_inside_training_schedule_model($mdb2, $user);
	$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

	//--------------------------------------------------------------------------
	// 院内研修報告テーブルデータ取得
	//--------------------------------------------------------------------------
	$log->debug("getRecJson開始",__FILE__,__LINE__);

	$sql_buff[] = "SELECT plan_id,";
	$sql_buff[] = "training_id,";
	$sql_buff[] = "first_plan_id,";
	$sql_buff[] = "plan_no,";
	$sql_buff[] = "year,";
	$sql_buff[] = "term_div,";
	$sql_buff[] = "training_time,";
	$sql_buff[] = "to_char(plan_date,'yyyy') as plan_date_yyyy,";
	$sql_buff[] = "to_char(plan_date,'mm') as plan_date_mm,";
	$sql_buff[] = "to_char(plan_date,'dd') as plan_date_dd,";
	$sql_buff[] = "to_char(from_time,'hh24') as from_time_hh,";
	$sql_buff[] = "to_char(from_time,'mi') as from_time_mi,";
	$sql_buff[] = "to_char(to_time,'hh24') as to_time_hh,";
	$sql_buff[] = "to_char(to_time,'mi') as to_time_mi,";
	$sql_buff[] = "report_division,";
	$sql_buff[] = "answer_division,";
	$sql_buff[] = "max_people,";
	$sql_buff[] = "apply_status,";
	$sql_buff[] = "place,";
	$sql_buff[] = "replace(replace(replace(remarks,chr(13)||chr(10),'\\\\n'),chr(13),'\\\\n'),chr(10),'\\\\n') as remarks_br , ";
	$sql_buff[] = "delete_flg,";
	$sql_buff[] = "create_date,";
	$sql_buff[] = "create_user,";
	$sql_buff[] = "update_date,";
	$sql_buff[] = "update_user ";
	$sql_buff[] = "FROM cl_mst_inside_training_schedule ";
	$sql_buff[] = "WHERE plan_id = :plan_id";
	$sql=implode('', $sql_buff);
	$data=$model->getRecordBySql($sql,$plan_id);

	$log->debug("getRecJson終了",__FILE__,__LINE__);

	$data["place"]      = str_replace("\\",   "\\\\",$data["place"]);
	//2012/05/11 K.Fujii upd(s)
	//$data["remarks_br"] = str_replace('\\\\',   '\\n', $data["remarks_br"]);
	$data["remarks_br"] = str_replace('\\\\n',   '\\n', $data["remarks_br"]);
	//2012/05/11 K.Fujii upd(e)


	$json=create_json($data);

	//$json=htmlspecialchars($json);


	$log->debug(__FUNCTION__." END");


	return $json;
}
