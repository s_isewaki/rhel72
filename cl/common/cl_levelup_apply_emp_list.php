<?
	//ini_set( 'display_errors', 1 );

	//##########################################################################
	// 各種モジュールの読み込み
	//##########################################################################
	require_once(dirname(__FILE__) . "/../../about_session.php");
	require_once(dirname(__FILE__) . "/../../about_authority.php");
	require_once(dirname(__FILE__) . "/../../about_postgres.php");
	require_once(dirname(__FILE__) . "/../../show_select_values.ini");
	require_once(dirname(__FILE__) . "/../../cl_application_list.inc");
	require_once(dirname(__FILE__) . "/../../cl_yui_calendar_util.ini");
	require_once(dirname(__FILE__) . "/../../cl_application_common.ini");
	require_once(dirname(__FILE__) . "/../../cl_common.ini");
	require_once(dirname(__FILE__) . "/../../cl_title_name.ini");
	require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
	require_once(dirname(__FILE__) . "/../../Cmx.php");
	require_once("Smarty/Smarty.class.php");
	require_once("MDB2.php");

	//##########################################################################
	// 初期処理
	//##########################################################################
	//ロガーをロード
	$log = new cl_common_log_class(basename(__FILE__));
	$log->info(basename(__FILE__)." START");

	//画面名
	$fname = $PHP_SELF;

	// パラメータ取得
	//##########################################################################
	foreach( $_REQUEST as $key => $value ) {
		$log->debug('$_REQUEST['.$key.']:'.$_REQUEST[$key],__FILE__,__LINE__);
	}

	$session		= $_POST["p_session"];
	$login_user		= $_POST["login_user"];
	$target_level	= $_POST["target_level"];

	$log->debug('■$session       :'.$session		,__FILE__,__LINE__);
	$log->debug('■$login_user    :'.$login_user	,__FILE__,__LINE__);
	$log->debug('■$target_level  :'.$target_level	,__FILE__,__LINE__);

	//====================================
	// データベース接続
	//====================================
	$mdb2 = MDB2::connect(CMX_DB_DSN);

	//====================================
	// モデルをロード
	//====================================
	$log->debug('cl_mst_supervisor_class_model START',__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/master/cl_mst_supervisor_class_model.php");
	$cl_mst_supervisor_class_model = new cl_mst_supervisor_class_model($mdb2,$login_user);
	$log->debug('cl_mst_supervisor_class_model END',__FILE__,__LINE__);

	$supervisor_data = $cl_mst_supervisor_class_model->select();

	$class_division = $supervisor_data["class_division"];

	//====================================
	// モデルをロード
	//====================================
	$log->debug('レベルアップ対象一覧モデル START',__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/search/cl_levelup_apply_emp_list_model.php");
	$cl_levelup_apply_emp_list_model = new cl_levelup_apply_emp_list_model($mdb2,$login_user);
	$log->debug('レベルアップ対象一覧モデル END',__FILE__,__LINE__);

	//====================================
	// データ取得
	//====================================
	$data = $cl_levelup_apply_emp_list_model->get_levelup_apply_emp_list($target_level,$login_user,$class_division);

	// 2012/08/14 Yamagawa add(s)
	for ($idx = 0; $idx < count($data); $idx++) {
		// 当院での経験年数
		$data[$idx]['exp_years']
			= exp_months_calculation(
				$data[$idx]['exp_dates']
				,0
				,$data[$idx]['mat_exp_dates']
				,$data[$idx]['par_exp_dates']
			);

		// 経験年数
		$data[$idx]['total_exp_years']
			= exp_months_calculation(
				$data[$idx]['exp_dates']
				,$data[$idx]['car_exp_dates']
				,$data[$idx]['mat_exp_dates']
				,$data[$idx]['par_exp_dates']
			);
	}
	// 2012/08/14 Yamagawa add(e)

	//====================================
	// データベース切断
	//====================================
	$log->debug('データベース切断 START',__FILE__,__LINE__);
	$mdb2->disconnect();
	$log->debug('データベース切断 END',__FILE__,__LINE__);

	//##########################################################################
	// 画面表示
	//##########################################################################
	$smarty = new Smarty();
	$smarty->template_dir = dirname(__FILE__) . "/../view";
	$smarty->compile_dir  = dirname(__FILE__) . "/../../templates_c";

	$smarty->assign( 'session'			, $session			);
	$smarty->assign( 'login_user'		, $login_user		);
	$smarty->assign( 'target_level'		, $target_level		);
	$smarty->assign( 'data'				, $data				);

	$smarty->display(basename(__FILE__,'.php').".tpl");


	$log->info(basename(__FILE__)." END");

	$log->shutdown();

// 2012/08/14 Yamagawa add(s)
	/**
	 * exp_months_calculation：経験月数算出
	 * @param $exp_dates：当院での経験日数
	 * @param $car_dates：職歴日数
	 * @param $mat_dates：産休日数
	 * @param $par_dates：育休日数
	 */
	function exp_months_calculation($exp_dates, $car_dates, $mat_dates, $par_dates){

		$result = '';
		$cal_years = 0;
		$cal_months = 0;
		$cal_dates = 0;

		if (strlen($exp_dates) > 0) {
			$cal_dates = $exp_dates + $car_dates - $mat_dates - $par_dates;
		}

		if ($cal_dates > 0) {
			$cal_years += floor($cal_dates / 365);
			$cal_months += floor(($cal_dates % 365) / 30);
			if ($cal_years > 0) {
				$result .= $cal_years;
				$result .= "年";
			}
			if ($cal_months > 11) {
				$result .= 11;
				$result .= "ヶ月";
			} else if ($cal_months > 0) {
				$result .= $cal_months;
				$result .= "ヶ月";
			}
		}

		return $result;

	}

// 2012/08/14 Yamagawa add(e)
