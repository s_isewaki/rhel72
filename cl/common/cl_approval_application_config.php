<?php
//出張届 日帰り 主任以下
define("mode_0_trip_1_c140_wkfw_id",	"215");

//出張届 日帰り 師長
define("mode_0_trip_1_c150_wkfw_id",	"172");

//出張届 ２日以上４日以内 主任以下
define("mode_0_trip_2_c140_wkfw_id",	"216");

//出張届 ２日以上４日以内 師長
define("mode_0_trip_2_c150_wkfw_id",	"173");

//出張届 ５日以上 主任以下 主任以下
define("mode_0_trip_3_c140_wkfw_id",	"217");

//出張届 ５日以上 師長
define("mode_0_trip_3_c150_wkfw_id",	"174");

//出張届 長期出張 主任以下
define("mode_0_trip_4_c140_wkfw_id",	"218");

//出張届 長期出張 師長
define("mode_0_trip_4_c150_wkfw_id",	"175");

//出張報告 日帰り 主任以下
define("mode_1_trip_1_c140_wkfw_id",	"219");

//出張報告 日帰り 師長
define("mode_1_trip_1_c150_wkfw_id",	"176");

//出張報告 ２日以上４日以内 主任以下
define("mode_1_trip_2_c140_wkfw_id",	"220");

//出張報告 ２日以上４日以内 師長
define("mode_1_trip_2_c150_wkfw_id",	"177");

//出張報告 ５日以上 主任以下 主任以下
define("mode_1_trip_3_c140_wkfw_id",	"221");

//出張報告 ５日以上 師長
define("mode_1_trip_3_c150_wkfw_id",	"178");

//出張報告 長期出張 主任以下
define("mode_1_trip_4_c140_wkfw_id",	"222");

//出張報告 長期出張 師長
define("mode_1_trip_4_c150_wkfw_id",	"179");

?>