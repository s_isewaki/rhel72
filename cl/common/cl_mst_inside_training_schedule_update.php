<?php
//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");
require_once('MDB2.php');
require_once(dirname(__FILE__) . "/../../cl_common.ini");

//define("SQL_SUCCESS",0);
//define("SQL_ERROR",9);

require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_schedule_model_sub.php");
require_once(dirname(__FILE__) . "/../model/search/cl_mst_inside_training_schedule_util.php");

require_once(dirname(__FILE__) . "/../model/sequence/cl_inside_training_schedule_id_seq_model.php");

$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

//------------------------------------------------------------------------------
// パラメータ取得
//------------------------------------------------------------------------------
$plan_id				=$_POST["plan_id"			];	// 日程ID
$first_plan_id			=$_POST["first_plan_id"		];	// 初回登録時の日程ID
$login_user				=$_POST["login_user"		];	// ログインユーザID
$training_id			=$_POST["training_id"		];	// 院内研修ID
$year					=$_POST["year"				];	// 年度
$term_div				=$_POST["term_div"			];	// 期区分
$training_time			=$_POST["training_time"		];	// 開催回数
$plan_date_yyyy			=$_POST["plan_date_yyyy"	];	// 開催日付年
$plan_date_mm			=$_POST["plan_date_mm"		];	// 開催日付月
$plan_date_dd			=$_POST["plan_date_dd"		];	// 開催日付日
$from_time_hh			=$_POST["from_time_hh"		];	// 開催時刻　From
$from_time_mi			=$_POST["from_time_mi"		];	// 開催時刻　From
$to_time_hh				=$_POST["to_time_hh"		];	// 開催時刻　To
$to_time_mi				=$_POST["to_time_mi"		];	// 開催時刻　To
$report_division		=$_POST["report_division"	];	// 報告区分
$answer_division		=$_POST["answer_division"	];	// アンケート区分
$max_people				=$_POST["max_people"		];	// 最大人数
$apply_status			=$_POST["apply_status"		];	// 申込ステータス
$place					=$_POST["place"				];	// 会場
$remarks				=$_POST["remarks"			];	// 備考
// 2012/08/27 Yamagawa add(s)
$mode					=$_POST["mode"				];
// 2012/08/27 Yamagawa add(e)

// 最終フラグ
if ($_POST["training_time"] == $_POST["max_training_time"]){
	$final_flg = 1;
} else {
	$final_flg = 0;
}

// URLデコード
$place					=urldecode($place);
$remarks				=urldecode($remarks);

// 文字コード変換
//$place					=mb_convert_encoding($place, "EUC-JP", "UTF-8");
//$remarks				=mb_convert_encoding($remarks, "EUC-JP", "UTF-8");
$place					=mb_convert_encoding($place, "eucJP-win", "UTF-8");
$remarks				=mb_convert_encoding($remarks, "eucJP-win", "UTF-8");

$log->debug('$place  ：'.$place,__FILE__,__LINE__);
$log->debug('$remarks：'.$remarks,__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

$cl_mst_inside_training_schedule_model = new cl_mst_inside_training_schedule_model($mdb2, $login_user);
$log->debug("院内研修日程モデルインスタンス作成終了",__FILE__,__LINE__);

//------------------------------------------------------------------------------
// 日程番号の最大値を取得
//------------------------------------------------------------------------------
$cl_mst_inside_training_schedule_util = new cl_mst_inside_training_schedule_util($mdb2, $login_user);
$plan_no = $cl_mst_inside_training_schedule_util->numberingPlanNo($training_id);

//------------------------------------------------------------------------------
// 修正前データ取得
//------------------------------------------------------------------------------
$before_data = $cl_mst_inside_training_schedule_model->getRecord($plan_id);

$log->debug('training_time 前：'.$before_data['training_time']." 後：".$training_time);
$log->debug('plan_date 前：'.$before_data['plan_date']." 後：".$plan_date_yyyy."-".$plan_date_mm."-".$plan_date_dd);
$log->debug('from_time 前：'.$before_data['from_time']." 後：".$from_time_hh.":".$from_time_mi.":00");
$log->debug('to_time 前：'.$before_data['to_time']." 後：".$to_time_hh.":".$to_time_mi.":00");

// 2012/04/12 Yamagawa add(s)
// 「24:00」は「23:59」に変換（暫定対応）
if (
	$from_time_hh == "24"
	&& $from_time_mi == "00"
) {
	$from_time_hh = "23";
	$from_time_mi = "59";
}
if (
	$to_time_hh == "24"
	&& $to_time_mi == "00"
) {
	$to_time_hh = "23";
	$to_time_mi = "59";
}
// 2012/04/12 Yamagawa add(e)

// 2012/08/27 Yamagawa add(s)
if ($mode == "2") {
// 2012/08/27 Yamagawa add(e)

	if (
		$before_data['training_time'] == $training_time
		&& $before_data['plan_date'] == $plan_date_yyyy."-".$plan_date_mm."-".$plan_date_dd
		&& $before_data['from_time'] == $from_time_hh.":".$from_time_mi.":00"
		&& $before_data['to_time'] == $to_time_hh.":".$to_time_mi.":00"
	){
		$log->debug('training_id	:'.$training_id		,__FILE__,__LINE__);
		$log->debug('plan_no		:'.$plan_no			,__FILE__,__LINE__);
		$log->debug('year			:'.$year			,__FILE__,__LINE__);
		$log->debug('term_div		:'.$term_div		,__FILE__,__LINE__);
		$log->debug('training_time	:'.$training_time	,__FILE__,__LINE__);
		$log->debug('final_flg		:'.$final_flg		,__FILE__,__LINE__);
		$log->debug('plan_date_yyyy	:'.$plan_date_yyyy	,__FILE__,__LINE__);
		$log->debug('plan_date_mm	:'.$plan_date_mm	,__FILE__,__LINE__);
		$log->debug('plan_date_dd	:'.$plan_date_dd	,__FILE__,__LINE__);
		$log->debug('from_time_hh	:'.$from_time_hh	,__FILE__,__LINE__);
		$log->debug('from_time_mi	:'.$from_time_mi	,__FILE__,__LINE__);
		$log->debug('to_time_hh		:'.$to_time_hh		,__FILE__,__LINE__);
		$log->debug('to_time_mi		:'.$to_time_mi		,__FILE__,__LINE__);
		$log->debug('report_division:'.$report_division	,__FILE__,__LINE__);
		$log->debug('answer_division:'.$answer_division	,__FILE__,__LINE__);
		$log->debug('max_people		:'.$max_people		,__FILE__,__LINE__);
		$log->debug('apply_status	:'.$apply_status	,__FILE__,__LINE__);
		$log->debug('place			:'.$place			,__FILE__,__LINE__);
		$log->debug('remarks		:'.$remarks			,__FILE__,__LINE__);

		//------------------------------------------------------------------------------
		// パラメータ作成
		//------------------------------------------------------------------------------
		$plan_date	= $plan_date_yyyy."/".$plan_date_mm."/".$plan_date_dd;
		$param=array(
			"plan_id"			=> $plan_id				,
			"first_plan_id"		=> $first_plan_id		,
			"training_id"		=> $training_id			,
			"plan_no"			=> $plan_no				,
			"year"				=> $year				,
			"term_div"			=> $term_div			,
			"training_time"		=> $training_time		,
			"final_flg"			=> $final_flg			,
			"plan_date"			=> $plan_date	,
			"from_time"			=> $from_time_hh.":".$from_time_mi.":00"	,
			"to_time"			=> $to_time_hh.":".$to_time_mi.":00"		,
			"report_division"	=> $report_division	,
			"answer_division"	=> $answer_division	,
			"max_people"		=> $max_people		,
			"apply_status"		=> $apply_status	,
			//2012/05/11 K.Fujii upd(s)
			//"place"				=> htmlspecialchars($place)			,
			//"remarks"			=> htmlspecialchars($remarks)
			"place"				=> pg_escape_string($place)			,
			"remarks"			=> pg_escape_string($remarks)
			//2012/05/11 K.Fujii upd(e)
		);
		//------------------------------------------------------------------------------
		// 更新
		//------------------------------------------------------------------------------
		$rtn=$cl_mst_inside_training_schedule_model->update($param);

	} else {
		//------------------------------------------------------------------------------
		// 修正前データ論理削除
		//------------------------------------------------------------------------------
		$rtn = $cl_mst_inside_training_schedule_model->logical_delete($plan_id);
		$log->debug("院内研修日程論理削除終了",__FILE__,__LINE__);

		//------------------------------------------------------------------------------
		// 院内研修日程ID取得
		//------------------------------------------------------------------------------
		$log->debug("院内研修日程IDSEQモデルインスタンス作成開始",__FILE__,__LINE__);
		$cl_inside_training_schedule_id_seq_model = new cl_inside_training_schedule_id_seq_model($mdb2, $login_user);
		$log->debug("院内研修日程IDSEQモデルインスタンス作成終了",__FILE__,__LINE__);
		$plan_id=$cl_inside_training_schedule_id_seq_model->getId();
		$log->debug('院内研修日程ID:'.$plan_id,__FILE__,__LINE__);

		$log->debug('training_id	:'.$training_id		,__FILE__,__LINE__);
		$log->debug('plan_no		:'.$plan_no			,__FILE__,__LINE__);
		$log->debug('year			:'.$year			,__FILE__,__LINE__);
		$log->debug('term_div		:'.$term_div		,__FILE__,__LINE__);
		$log->debug('training_time	:'.$training_time	,__FILE__,__LINE__);
		$log->debug('final_flg		:'.$final_flg		,__FILE__,__LINE__);
		$log->debug('plan_date_yyyy	:'.$plan_date_yyyy	,__FILE__,__LINE__);
		$log->debug('plan_date_mm	:'.$plan_date_mm	,__FILE__,__LINE__);
		$log->debug('plan_date_dd	:'.$plan_date_dd	,__FILE__,__LINE__);
		$log->debug('from_time_hh	:'.$from_time_hh	,__FILE__,__LINE__);
		$log->debug('from_time_mi	:'.$from_time_mi	,__FILE__,__LINE__);
		$log->debug('to_time_hh		:'.$to_time_hh		,__FILE__,__LINE__);
		$log->debug('to_time_mi		:'.$to_time_mi		,__FILE__,__LINE__);
		$log->debug('report_division:'.$report_division	,__FILE__,__LINE__);
		$log->debug('answer_division:'.$answer_division	,__FILE__,__LINE__);
		$log->debug('max_people		:'.$max_people		,__FILE__,__LINE__);
		$log->debug('apply_status	:'.$apply_status	,__FILE__,__LINE__);
		$log->debug('place			:'.$place			,__FILE__,__LINE__);
		$log->debug('remarks		:'.$remarks			,__FILE__,__LINE__);

		//------------------------------------------------------------------------------
		// パラメータ作成
		//------------------------------------------------------------------------------
		$plan_date	= $plan_date_yyyy."/".$plan_date_mm."/".$plan_date_dd;
		$param=array(
			"plan_id"			=> $plan_id				,
			"first_plan_id"		=> $first_plan_id		,
			"training_id"		=> $training_id			,
			"plan_no"			=> $plan_no				,
			"year"				=> $year				,
			"term_div"			=> $term_div			,
			"training_time"		=> $training_time		,
			"final_flg"			=> $final_flg			,
			"plan_date"			=> $plan_date	,
			"from_time"			=> $from_time_hh.":".$from_time_mi.":00"	,
			"to_time"			=> $to_time_hh.":".$to_time_mi.":00"		,
			"report_division"	=> $report_division	,
			"answer_division"	=> $answer_division	,
			"max_people"		=> $max_people		,
			"apply_status"		=> $apply_status	,
			//2012/05/11 K.Fujii upd(s)
			//"place"				=> htmlspecialchars($place)			,
			//"remarks"			=> htmlspecialchars($remarks)
			"place"				=> pg_escape_string($place)			,
			"remarks"			=> pg_escape_string($remarks)
			//2012/05/11 K.Fujii upd(e)
		);
		//------------------------------------------------------------------------------
		// 登録
		//------------------------------------------------------------------------------
		$rtn=$cl_mst_inside_training_schedule_model->insert($param);
	}
// 2012/08/27 Yamagawa add(s)
} else if ($mode == "3") {

	$log->debug('training_id	:'.$training_id		,__FILE__,__LINE__);
	$log->debug('apply_status	:'.$apply_status	,__FILE__,__LINE__);
	$log->debug('remarks		:'.$remarks			,__FILE__,__LINE__);

	//------------------------------------------------------------------------------
	// パラメータ作成
	//------------------------------------------------------------------------------
	$param = array(
		 "training_id"	=>	$training_id
		,"apply_status"	=>	$apply_status
		,"remarks"		=>	$remarks
		// 2012/11/27 Yamagawa add(s)
		,"year"			=>	$year
		// 2012/11/27 Yamagawa add(e)
	);

	//------------------------------------------------------------------------------
	// 更新
	//------------------------------------------------------------------------------
	$rtn=$cl_mst_inside_training_schedule_model->update_for_training_id($param);

}
// 2012/08/27 Yamagawa add(e)

//------------------------------------------------------------------------------
// データベース接続を閉じる
//------------------------------------------------------------------------------
$log->debug("データベース切断 START",__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);

$log->debug("開催日更新処理：".$rtn,__FILE__,__LINE__);
if($rtn==SQL_ERROR){
	$log->debug("SQL_ERROR",__FILE__,__LINE__);
	$json="{success:\"false\"}";
}
else{
	$log->debug("SQL_SUCCSESS",__FILE__,__LINE__);
	$json='{"success":"true"}';
}

$log->debug('$json:'.$json,__FILE__,__LINE__);


header("Content-Type: application/json; charset=EUC-JP");
echo $json;

