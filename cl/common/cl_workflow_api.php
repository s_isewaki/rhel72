<?php
require_once(dirname(__FILE__) . "/../../Cmx.php");
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");

class cl_workflow_api{

    var $con;
    var $log;
    var $user;
    //var $arr_empmst;

	//function cl_workflow_api(&$p_con,$p_login_user){
	//function cl_workflow_api(&$p_con,$p_session){
	function cl_workflow_api($p_con){

		$this->con = $p_con;
		$this->log = new cl_common_log_class(basename(__FILE__,'.php'));
		//$this->user = $p_login_user;
		//$arr_empmst = $this->get_emp_info($p_session);
		//$this->user = $arr_empmst['emp_id'];

	}

	/**
	 * データアクセスユーザー設定
	 * @param session：セッション
	 * @return なし
	 */
	function setArrEmpmstBySession($p_session){
		$arr_empmst_tmp = $this->get_emp_info($p_session);
		$this->user = $arr_empmst_tmp['emp_id'];
	}

	/**
	 * データアクセスユーザー設定
	 * @param p_db_user：ユーザID
	 * @return なし
	 */
	function setArrEmpmstByEmpId($p_db_user){
		$this->user = $p_db_user;
	}

	/**
	 * ユーザー情報取得
	 * @param session：セッション
	 * @return ユーザー情報
	 */
	function get_emp_info($session){

		$this->log->info(__FUNCTION__." START");

		// セッションより職員情報を取得する
		require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php");
		$empmst_model = new empmst_model($this->con);
		$arr_empmst = $empmst_model->select_login_emp($session);

		$this->log->info(__FUNCTION__." END");
		return $arr_empmst;

	}

	/**
	 * ユーザー情報取得
	 * @param emp_id：職員ＩＤ
	 * @return ユーザー情報
	 */
	function get_emp_info_by_id($emp_id){

		$this->log->info(__FUNCTION__." START");

		// セッションより職員情報を取得する
		require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php");
		$empmst_model = new empmst_model($this->con, $this->user);
		$arr_empmst = $empmst_model->select($emp_id);

		$this->log->info(__FUNCTION__." END");
		return $arr_empmst;

	}

	/**
	 * 申請ＩＤ取得
	 * @param
	 * @return 申請ＩＤ
	 */
	function get_apply_id(){

		$this->log->info(__FUNCTION__." START");

		// シーケンスより申請ＩＤを取得
		require_once(dirname(__FILE__) . "/../model/sequence/cl_apply_id_seq_model.php");
		$cl_apply_id_model = new cl_apply_id_seq_model($this->con,$this->user);
		$apply_id = $cl_apply_id_model->getApplyId();
		$this->log->debug("■NEW apply_id:".$apply_id,__FILE__,__LINE__);

		$this->log->info(__FUNCTION__." END");
		return $apply_id;

	}

	/**
	 * 申請番号取得
	 * @return
	 */
	function get_apply_no()
	{

		$this->log->info(__FUNCTION__." START");

		require_once(dirname(__FILE__) . "/../model/workflow/cl_apply_model.php");
		$cl_apply_model = new cl_apply_model($this->con,$this->user);
		$apply_no = $cl_apply_model->get_apply_no();

//		$date = date("Ymd");
//		$year = substr($date, 0, 4);
//		$md   = substr($date, 4, 4);
//		if($md >= "0101" and $md <= "0331")
//		{
//			$year = $year - 1;
//		}
//
//		$arr_apply_cnt_per_year = $cl_apply_model->get_apply_cnt_per_year($year);
//		$max_cnt = $arr_apply_cnt_per_year['cnt'];
//		$apply_no = $max_cnt + 1;



		$this->log->info(__FUNCTION__." END");

		return $apply_no;

	}

	/**
	 * ワークフロー情報取得
	 * @param $req：リクエストパラメーター
	 * @return
	 */
	function get_wkfw_info_by_req($req){

		$this->log->info(__FUNCTION__."START");

		$wkfw_info = array();

		$wkfw_info['wkfw_appr']				= $req['wkfw_appr'];
		$wkfw_info['wkfw_content_type']		= $req['wkfw_content_type'];
		$wkfw_info['apply_title_disp_flg']	= $req['apply_title_disp_flg'];
		$wkfw_info['short_wkfw_name']		= $req['short_wkfw_name'];

		$this->log->info(__FUNCTION__."END");

		return $wkfw_info;

	}

	/**
	 * ワークフロー情報取得
	 * @param $wkfw_id：ワークフローＩＤ
	 * @return
	 */
	function get_wkfw_info_by_wkfw_id($wkfw_id){

		$this->log->info(__FUNCTION__."START");

		require_once(dirname(__FILE__) . "/../model/workflow/cl_wkfwmst_model.php");
		$cl_wkfwmst_model = new cl_wkfwmst_model($this->con,$this->user);
		$cl_wkfwmst=$cl_wkfwmst_model->select($wkfw_id);


		$wkfw_info = array();

		$wkfw_info['wkfw_appr']				= $cl_wkfwmst['wkfw_appr'];
		$wkfw_info['wkfw_content_type']		= $cl_wkfwmst['wkfw_content_type'];
		$wkfw_info['apply_title_disp_flg']	= $cl_wkfwmst['apply_title_disp_flg'];
		$wkfw_info['short_wkfw_name']		= $cl_wkfwmst['short_wkfw_name'];
		$wkfw_info['wkfw_id']				= $cl_wkfwmst['wkfw_id'];
		$wkfw_info['wkfw_type']				= $cl_wkfwmst['wkfw_type'];
		$wkfw_info['wkfw_title']			= $cl_wkfwmst['wkfw_title'];
		$wkfw_info['wkfw_content']			= $cl_wkfwmst['wkfw_content'];
		$wkfw_info['wkfw_del_flg']			= $cl_wkfwmst['wkfw_del_flg'];
		$wkfw_info['wkfw_start_date']		= $cl_wkfwmst['wkfw_start_date'];
		$wkfw_info['wkfw_end_date']			= $cl_wkfwmst['wkfw_end_date'];
		$wkfw_info['wkfw_folder_id']		= $cl_wkfwmst['wkfw_folder_id'];
		$wkfw_info['ref_dept_st_flg']		= $cl_wkfwmst['ref_dept_st_flg'];
		$wkfw_info['ref_dept_flg']			= $cl_wkfwmst['ref_dept_flg'];
		$wkfw_info['ref_st_flg']			= $cl_wkfwmst['ref_st_flg'];
		$wkfw_info['create_date']			= $cl_wkfwmst['create_date'];
		$wkfw_info['create_user']			= $cl_wkfwmst['create_user'];
		$wkfw_info['update_date']			= $cl_wkfwmst['update_date'];
		$wkfw_info['update_user']			= $cl_wkfwmst['update_user'];

		$this->log->info(__FUNCTION__."END");

		return $wkfw_info;

	}

	/**
	 * 申請登録
	 * @param $apply_id：申請ＩＤ
	 * @param $wkfw_id：ワークフローＩＤ
	 * @param $content：
	 * @param $apply_title：表題
	 * @param $draft：下書きフラグ
	 * @param $apply_no：申請番号
	 * @param $notice_sel_flg：通知者選択フラグ
	 * @param $wkfw_history_no：ワークフロー履歴番号
	 * @param $wkfwfile_history_no：ワークフロー履歴番号
	 * @param $arr_empmst：職員情報
	 * @param $arr_wkfw：ワークフロー情報
	 * @return
	 */
	function regist_apply(
		$apply_id
		, $wkfw_id
		, $content
		, $apply_title
		, $draft
		, $apply_no
		, $notice_sel_flg
		, $wkfw_history_no
		, $wkfwfile_history_no
		, $arr_empmst
		, $arr_wkfw
	){

		$this->log->info(__FUNCTION__." START");

		// パラメータセット
		$arr = array(
				"apply_id"				=> $apply_id,
				"wkfw_id"				=> $wkfw_id,
				"apply_content"			=> $content,
				"emp_id"				=> $arr_empmst['emp_id'],
				"apply_stat"			=> "0",
				"apply_date"			=> date("YmdHi"),
				"apply_title"			=> $apply_title,
				"re_apply_id"			=> null,
				"apv_fix_show_flg"		=> "t",
				"apv_bak_show_flg"		=> "t",
				"emp_class"				=> $arr_empmst['emp_class'],
				"emp_attribute"			=> $arr_empmst['emp_attribute'],
				"emp_dept"				=> $arr_empmst['emp_dept'],
				"apv_ng_show_flg"		=> "t",
				"emp_room"				=> $arr_empmst['emp_room'],
				"draft_flg"				=> ($draft == "on") ? "t" : "f",
				"wkfw_appr"				=> $arr_wkfw['wkfw_appr'],
				"wkfw_content_type"		=> $arr_wkfw['wkfw_content_type'],
				"apply_title_disp_flg"	=> $arr_wkfw['apply_title_disp_flg'],
				"apply_no"				=> $apply_no,
				"notice_sel_flg"		=> $notice_sel_flg,
				"wkfw_history_no"		=> ($wkfw_history_no == "") ? null : $wkfw_history_no,
				"wkfwfile_history_no"	=> ($wkfwfile_history_no == "") ? null : $wkfwfile_history_no
			 );


		// 申請登録
		require_once(dirname(__FILE__) . "/../model/workflow/cl_apply_model.php");
		$cl_apply_model = new cl_apply_model($this->con,$this->user);
		$rtn = $cl_apply_model->insert($arr);

		$this->log->info(__FUNCTION__." END");

		return $rtn;

	}

	/**
	 * 承認者候補取得
	 * @param $req：リクエストパラメーター
	 * @return
	 */
	function get_applyapvemp_by_req($req){

		$this->log->info(__FUNCTION__." START");

		$arr_applyapvemp = array();
		$approve_num = $req["approve_num"];

		// 件数分ループ
		for($i=1; $i <= $approve_num; $i++)
		{

			// 承認階層
			$apv_order = $req["apv_order".$i];
			$this->log->debug('apv_order:'.$apv_order,__FILE__,__LINE__);

			// 承認階層毎の人数
			$pst_approve_num = $req["pst_approve_num".$apv_order];
			$this->log->debug('pst_approve_num:'.$pst_approve_num,__FILE__,__LINE__);

			for($j=1; $j<=$pst_approve_num; $j++)
			{
				$arr = array();

				// 職員ＩＤ
				$pst_emp_id = $req["pst_emp_id".$apv_order."_".$j];
				$this->log->debug('pst_emp_id:'.$pst_emp_id,__FILE__,__LINE__);

				// 承認者区分
				$pst_st_div = $req["pst_st_div".$apv_order."_".$j];
				$this->log->debug('pst_st_div:'.$pst_st_div,__FILE__,__LINE__);

				// 委員会コード
				$pst_parent_pjt_id = $req["pst_parent_pjt_id".$apv_order."_".$j];
			    $pst_parent_pjt_id = ($pst_parent_pjt_id == "") ? null : $pst_parent_pjt_id;
				$this->log->debug('pst_parent_pjt_id:'.$pst_parent_pjt_id,__FILE__,__LINE__);

				// ＷＧコード
				$pst_child_pjt_id = $req["pst_child_pjt_id".$apv_order."_".$j];
				$pst_child_pjt_id = ($pst_child_pjt_id == "") ? null : $pst_child_pjt_id;
				$this->log->debug('pst_child_pjt_id:'.$pst_child_pjt_id,__FILE__,__LINE__);

				// パラメータセット
				$arr = array(
					"apply_id"			 => $apply_id			,
					"apv_order"			 => $apv_order			,
					"person_order"		 => $j					,
					"emp_id"			 => $pst_emp_id			,
					"st_div"			 => $pst_st_div			,
					"parent_pjt_id"		 => $pst_parent_pjt_id	,
					"child_pjt_id"		 => $pst_child_pjt_id
				);

				array_push($arr_applyapvemp, $arr);

			}
		}

		$this->log->info(__FUNCTION__." END");

		return $arr_applyapvemp;

	}

	/**
	 * 承認者候補登録
	 * @param $apply_id：申請ＩＤ
	 * @param $arr_applyapvemp：承認者候補
	 * @return
	 */
	function regist_applyapvemp($apply_id, $arr_applyapvemp){

		$this->log->info(__FUNCTION__." START");

		require_once(dirname(__FILE__) . "/../model/workflow/cl_applyapvemp_model.php");
		$cl_applyapvemp_model = new cl_applyapvemp_model($this->con,$this->user);

		$i = 1;

		// 件数分ループ
		foreach($arr_applyapvemp as $applyapvemp)
		{

			$arr = array(
				"apply_id"			 => $applyapvemp['apply_id']			,
				"apv_order"			 => $applyapvemp['apv_order']			,
				"person_order"		 => $i					,
				"emp_id"			 => $applyapvemp['pst_emp_id']			,
				"st_div"			 => $applyapvemp['pst_st_div']			,
				"parent_pjt_id"		 => $applyapvemp['pst_parent_pjt_id']	,
				"child_pjt_id"		 => $applyapvemp['pst_child_pjt_id']
			);

			// 承認者候補登録
			$cl_applyapvemp_model->insert($arr);

			// 番号をインクリメント
			$i++;

		}

		$this->log->info(__FUNCTION__." END");

	}

	/**
	 * 非同期・同期受信取得
	 * @param $apvteacher_num：講師人数
	 * @param $req：リクエストパラメーター
	 * @param $council_num：審議会人数
	 * @return
	 */
	// 2012/07/19 Yamagawa upd(s)
	//function get_applyasyncrecv_info($apvteacher_num, $req){
	function get_applyasyncrecv_info($apvteacher_num, $req, $council_num){
	// 2012/07/19 Yamagawa upd(e)

		$this->log->info(__FUNCTION__." START");

		$previous_apv_order = "";
		$arr_apv = array();
		$arr_apv_sub_order = array();

		// 対象研修に対する講師人数
		$apvteacher_num = $apvteacher_num;

		// 2012/07/19 Yamagawa add(s)
		// ワークフローマスタ取得
		$wkfw_info = $this->get_wkfw_info_by_req($req);
		// 2012/07/19 Yamagawa add(e)

		for($i=1; $i <= $req["approve_num"]; $i++){

			// 承認階層
			$apv_order = $req["apv_order".$i];

			// 承認者区分
			$st_div = $req["st_div".$i];

			// 複数承認者の指定
			$multi_apv_flg = ($req["multi_apv_flg".$i] == "") ? "f" : $req["multi_apv_flg".$i];

			$next_notice_div = ($req["next_notice_div".$i] == "") ? null : $req["next_notice_div".$i];

			// 承認サブ階層
			if($previous_apv_order != $apv_order)
			{
				$arr_apv_sub_order = array();
				$apv_sub_order_wk = 1;
			}

			// 承認者が講師　かつ
			// 複数承認可　かつ
			// 研修に対する講師が複数人設定されている場合
			// 選択した講師分登録する
			if ($st_div == 6 && $multi_apv_flg == "t" && $apvteacher_num > 1){
				$varname = "check_emp_id".$i."_";
				$arr_field_name = array_keys($req);
				foreach($arr_field_name as $field_name){
					$pos = strpos($field_name,$varname);
					if ($pos === 0){
						$arr_apv_sub_order[] = $apv_sub_order_wk;
						$apv_sub_order_wk++;
					}
				}
			// 2012/07/18 Yamagawa add(s)
			// レベルＶ　レベルアップ申請　かつ　承認者が審議会の場合
			} else if ($st_div == 11 && $wkfw_info["short_wkfw_name"] == "c440" && $council_num > 1) {
				$varname = "check_council".$i."_";
				$arr_field_name = array_keys($req);
				foreach($arr_field_name as $field_name){
					$pos = strpos($field_name,$varname);
					if ($pos === 0){
						$arr_apv_sub_order[] = $apv_sub_order_wk;
						$apv_sub_order_wk++;
					}
				}
			// 2012/07/18 Yamagawa add(e)
			} else {
				$arr_apv_sub_order[] = $apv_sub_order_wk;
				$apv_sub_order_wk++;
			}

			$arr_apv[$apv_order] = array("multi_apv_flg" => $multi_apv_flg, "next_notice_div" => $next_notice_div, "apv_sub_order" => $arr_apv_sub_order);

			$previous_apv_order = $apv_order;
		}

		$this->log->info(__FUNCTION__." END");
		return $arr_apv;

	}

	/**
	 * 非同期・同期受信登録
	 * @param $apply_id：申請ＩＤ
	 * @param $applyasyncrecv_info：非同期・同期情報
	 * @return
	 */
	function regist_applyasyncrecv($apply_id, $applyasyncrecv_info){

		$this->log->info(__FUNCTION__." START");

		require_once(dirname(__FILE__) . "/../model/workflow/cl_applyasyncrecv_model.php");
		$cl_applyasyncrecv_model = new cl_applyasyncrecv_model($this->con,$this->user);

		$arr_apv_sub_order = array();

		if(count($applyasyncrecv_info) > 0){

			$arr_send_apv_sub_order = array();
			foreach($applyasyncrecv_info as $apv_order => $apv_info)
			{
				$multi_apv_flg = $apv_info["multi_apv_flg"];
				$next_notice_div = $apv_info["next_notice_div"];
				$arr_apv_sub_order = $apv_info["apv_sub_order"];

				foreach($arr_send_apv_sub_order as $send_apv_order => $arr_send_apv_sub)
				{
					// 非同期通知
					if($arr_send_apv_sub != null)
					{
						foreach($arr_send_apv_sub as $send_apv_sub_order)
						{
							foreach($arr_apv_sub_order as $recv_apv_sub_order)
							{
								$param=array(
										"apply_id"				=>	$apply_id	,
										"send_apv_order"		=>	$send_apv_order		,
										"send_apv_sub_order"	=>	$send_apv_sub_order	,
										"recv_apv_order"		=>	$apv_order			,
										"recv_apv_sub_order"	=>	$recv_apv_sub_order	,
										"send_apved_order"		=>	null				,
										"apv_show_flg"			=>	"f"
								);
								$cl_applyasyncrecv_model->insert($param);
							}
						}
					}
					// 同期通知
					else
					{
						foreach($arr_apv_sub_order as $recv_apv_sub_order)
						{
							$param=array(
									"apply_id"				=>	$apply_id	,
									"send_apv_order"		=>	$send_apv_order		,
									"send_apv_sub_order"	=>	null				,
									"recv_apv_order"		=>	$apv_order			,
									"recv_apv_sub_order"	=>	$recv_apv_sub_order	,
									"send_apved_order"		=>	null				,
									"apv_show_flg"			=>	"f"
							);
							$cl_applyasyncrecv_model->insert($param);
						}
					}

				}
				$arr_send_apv_sub_order = array();

				// 非同期通知の場合
				if($multi_apv_flg == "t" && $next_notice_div == "1" && count($arr_apv_sub_order) > 1)
				{
					$arr_send_apv_sub_order[$apv_order] = $arr_apv_sub_order;
				}
				// 同期通知または権限並列通知の場合
				else if($multi_apv_flg == "t" && $next_notice_div != "1" && count($arr_apv_sub_order) > 1)
				{
					$arr_send_apv_sub_order[$apv_order] = null;
				}
			}
		}

		$this->log->info(__FUNCTION__." END");

	}

	/**
	 * 申請結果通知登録
	 * @param $apply_id：申請ＩＤ
	 * @param $arr_notice_emp_id：通知者職員ＩＤ
	 * @param $arr_rslt_ntc_div：
	 * @return
	 */
	function regist_applynotice($apply_id, $arr_notice_emp_id, $arr_rslt_ntc_div){

		$this->log->info(__FUNCTION__." START");

		require_once(dirname(__FILE__) . "/../model/workflow/cl_applynotice_model.php");
		$cl_applynotice_model = new cl_applynotice_model($this->con,$this->user);

		for($i=0; $i<count($arr_notice_emp_id); $i++){

			$param=array(
				"apply_id"		=>	$apply_id				,
				"recv_emp_id"	=>	$arr_notice_emp_id[$i]	,
				"confirmed_flg"	=>	"f"						,
				"send_emp_id"	=>	null					,
				"send_date"		=>	null					,
				"rslt_ntc_div"	=>	$arr_rslt_ntc_div[$i]
			);

			$cl_applynotice_model->insert($param);

		}

		$this->log->info(__FUNCTION__." END");

	}

	/**
	 * 前提とする申請書（申請用）取得
	 * @param $req：リクエストパラメーター
	 * @return
	 */
	function get_applyprecond_info($req){

		$this->log->info(__FUNCTION__." START");

		$applyprecond_info = array();

		for($i=1; $i<=$req["precond_num"]; $i++){

			$param = array();

			// 前提とする申請書　ワークフローＩＤ
			$precond_wkfw_id = $req["precond_wkfw_id".$i];

			// 前提とする申請書　申請ＩＤ
			$precond_apply_id = $req["precond_apply_id".$i];

			$param = array(
				"apply_id"			=> $apply_id			,
				"precond_wkfw_id"	=> $precond_wkfw_id		,
				"precond_order"		=> $i					,
				"precond_apply_id"	=> $precond_apply_id
			);

			array_push($applyprecond_info, $param);

		}

		$this->log->info(__FUNCTION__." END");

		return $applyprecond_info;

	}

	/**
	 * 前提とする申請書（申請用）登録
	 * @param $apply_id：申請ＩＤ
	 * @param $arr_applyprecond：前提とする申請書（申請用）情報
	 * @return
	 */
	function regist_applyprecond($apply_id, $arr_applyprecond){

		$this->log->info(__FUNCTION__." START");

		require_once(dirname(__FILE__) . "/../model/workflow/cl_applyprecond_model.php");
		$cl_applyprecond_model = new cl_applyprecond_model($this->con,$this->user);

		foreach($arr_applyprecond as $applyprecond){

			$cl_applyprecond_model->insert($applyprecond);

		}

		$this->log->info(__FUNCTION__." END");

	}

	/**
	 * 添付ファイル移動
	 * @param $apply_id：申請ＩＤ
	 * @param $arr_filename：ファイル名
	 * @param $arr_file_id：ファイルＩＤ
	 * @param $session：セッション
	 * @return
	 */
	function move_applyfile($apply_id, $arr_filename, $arr_file_id, $session){

		$this->log->info(__FUNCTION__." START");

		// 一時ファイルをコピー
		for ($i = 0; $i < count($arr_filename); $i++) {

			$tmp_file_id = $arr_file_id[$i];
			$tmp_filename = $arr_filename[$i];
			$tmp_fileno = $i + 1;
			$ext = strrchr($tmp_filename, ".");
			$tmp_filename = dirname(__FILE__) . "/../apply/tmp/{$session}_{$tmp_file_id}{$ext}";
			copy($tmp_filename, dirname(__FILE__) . "/../apply/{$apply_id}_{$tmp_fileno}{$ext}");

		}

		// コピー元ファイルを削除
		foreach (glob(dirname(__FILE__) . "/../apply/tmp/{$session}_*.*") as $tmpfile) {

			unlink($tmpfile);

		}

		$this->log->info(__FUNCTION__." END");

	}

	/**
	 * テンプレート項目取得
	 * @param $req：リクエストパラメータ
	 * @return
	 */
	function get_template_param($req){

		$this->log->info(__FUNCTION__." START");

		//------------------------------------------------------------------------------
		// テンプレートの項目のみをパラメータに設定
		//------------------------------------------------------------------------------
		require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");
		$param = template_parameter_pre_proccess($req);
		$this->log->debug('param：'.print_r($param, true));

		$this->log->info(__FUNCTION__." END");

		return $param;

	}

	/**
	 * ハンドラー呼出し
	 * @param $mode：処理モード
	 * @param $apply_id：申請ＩＤ
	 * @param $wkfw_id：ワークフローＩＤ
	 * @param $short_wkfw_name：管理ＣＤ
	 * @param $param：テンプレートパラメーター
	 * @return
	 */
	function load_handler($mode, $apply_id, $wkfw_id, $short_wkfw_name, $param){

		$this->log->info(__FUNCTION__." START");

		//------------------------------------------------------------------------------
		// パラメータに申請ＩＤ・職員ＩＤを追加
		//------------------------------------------------------------------------------
		$param['apply_id'] = $apply_id;
		$param['emp_id'] = $this->user;

		//------------------------------------------------------------------------------
		// テンプレート独自の申請処理を呼び出す
		//------------------------------------------------------------------------------
		//ハンドラーモジュールロード
		$this->log->debug('ハンドラーモジュールロード　開始　管理番号：'.$short_wkfw_name,__FILE__,__LINE__);
		$handler_script=dirname(__FILE__) . "/../handler/".$short_wkfw_name."_handler.php";
		$this->log->debug('$handler_script：'.$handler_script,__FILE__,__LINE__);
		require_once($handler_script);
		$this->log->debug('ハンドラーモジュールロード　終了',__FILE__,__LINE__);

		// ハンドラー　クラス化対応
		$this->log->debug('ハンドラークラスインスタンス化　開始',__FILE__,__LINE__);
		$handler_name = $short_wkfw_name."_handler";
		$handler = new $handler_name();
		$this->log->debug('ハンドラークラスインスタンス化　終了',__FILE__,__LINE__);

		switch($mode) {

			//------------------------------------------------------------------------------
			// 新規申請　申請の場合
			//------------------------------------------------------------------------------
			case "apply_regist":
				$this->log->debug('■ハンドラー新規申請　申請処理　開始',__FILE__,__LINE__);
				$handler->NewApl_Regist($this->con, $this->user, $param);
				$this->log->debug('■ハンドラー新規申請　申請処理　終了',__FILE__,__LINE__);
				break;

			//------------------------------------------------------------------------------
			// 新規申請　一時保存の場合
			//------------------------------------------------------------------------------
			case "draft_regist":
				$this->log->debug('■ハンドラー新規申請　一時保存処理　開始',__FILE__,__LINE__);
				$handler->NewApl_Draft($this->con, $this->user, $param);
				$this->log->debug('■ハンドラー新規申請　一時保存処理　終了',__FILE__,__LINE__);
				break;

			//------------------------------------------------------------------------------
			// 新規申請（一時保存から）　申請の場合
			//------------------------------------------------------------------------------
			case "apply_draft":
				$this->log->debug('■ハンドラー新規申請（一時保存から）　申請処理　開始',__FILE__,__LINE__);
				$handler->DraftApl_Regist($this->con, $this->user, $param);
				$this->log->debug('■ハンドラー新規申請（一時保存から）　申請処理　終了',__FILE__,__LINE__);
				break;

			//------------------------------------------------------------------------------
			// 新規申請（一時保存から）　一時保存の場合
			//------------------------------------------------------------------------------
			case "draft_update":
				$this->log->debug('■ハンドラー新規申請（一時保存から）　一時保存処理　開始',__FILE__,__LINE__);
				$handler->DraftApl_Draft($this->con, $this->user, $param);
				$this->log->debug('■ハンドラー新規申請（一時保存から）　一時保存処理　終了',__FILE__,__LINE__);
				break;

			//------------------------------------------------------------------------------
			// 申請詳細　更新の場合
			//------------------------------------------------------------------------------
			case "apply_update":
				$this->log->debug('■ハンドラー申請詳細　更新　開始',__FILE__,__LINE__);
				$handler->AplDetail_Update($this->con, $this->user, $param);
				$this->log->debug('■ハンドラー申請詳細　更新　終了',__FILE__,__LINE__);
				break;

			//------------------------------------------------------------------------------
			// その他
			//------------------------------------------------------------------------------
			default:
				break;
		}

		$this->log->info(__FUNCTION__." END");

	}

	/**
	 * 承認者情報取得
	 * @param $req：リクエストパラメーター
	 * @return
	 */
	function get_apv_info($req){

		$this->log->info(__FUNCTION__." START");

		$approve_num=$req["approve_num"];

		$apv_info = array();

		for ($i = 1; $i <= $approve_num; $i++) {

			/**
			 * 承認者情報配列に、承認者情報を格納する。
			 */
			$apv_info["regist_emp_id$i"		] = $req["regist_emp_id$i"		];
			$apv_info["st_div$i"			] = $req["st_div$i"				];
			$apv_info["parent_pjt_id$i"		] = $req["parent_pjt_id$i"		];
			$apv_info["child_pjt_id$i"		] = $req["child_pjt_id$i"		];
			$apv_info["apv_order$i"			] = $req["apv_order$i"			];
			$apv_info["apv_sub_order$i"		] = $req["apv_sub_order$i"		];
			$apv_info["multi_apv_flg$i"		] = $req["multi_apv_flg$i"		];
			$apv_info["next_notice_div$i"	] = $req["next_notice_div$i"	];

		}
		$apv_info["approve_num"	] = $approve_num;

		$this->log->info(__FUNCTION__." END");

		return $apv_info;

	}

	/**
	 * 承認者情報編集
	 * @param $apv_info：承認者情報
	 * @return
	 */
	function edit_apv_info($apv_info){

		$this->log->info(__FUNCTION__." START");

		$approve_num = $apv_info["approve_num"	];

		$tmp_arr_apv = array();
		$tmp_apv_order = 0;
		$tmp_pre_apv_order = 0;

		for ($i = 1; $i <= $approve_num; $i++) {

			/**
			 * 承認者が設定されている場合
			 */
			if ($apv_info["regist_emp_id$i"] != "") {

				/**
				 * 今回処理する承認者オーダーと前回処理した承認者オーダーが異なる場合
				 */
				if ($apv_info["apv_order$i"] != $tmp_pre_apv_order) {

					/**
					 * 作業用承認者オーダーをインクリメントし、作業用承認者サブオーダーに１を設定する。
					 * また、前回処理した承認者オーダーに今回処理する承認者オーダーを格納する。
					 */
					$tmp_apv_order++;
					$tmp_apv_sub_order = 1;
					$tmp_pre_apv_order = $apv_info["apv_order$i"];
				}
				/**
				 * 今回処理する承認者オーダーと前回処理した承認者オーダーが一致する場合
				 */
				else {
					/**
					 * 作業用承認者サブオーダーをインクリメントする。
					 */
					$tmp_apv_sub_order += 1;
				}

				/**
				 * TODO 仕様の確認
				 */
				if ($tmp_apv_sub_order == 2) {
					$tmp_arr_apv[count($tmp_arr_apv) - 1]["apv_sub_order"] = 1;
				}

				/**
				 * 承認者情報配列に、承認者情報を格納する。
				 */
				$tmp_arr_apv[] = array(
					"regist_emp_id"			=> $apv_info["regist_emp_id$i"],
					"st_div"				=> $apv_info["st_div$i"],
					"parent_pjt_id"			=> $apv_info["parent_pjt_id$i"],
					"child_pjt_id"			=> $apv_info["child_pjt_id$i"],
					"apv_order"				=> $tmp_apv_order,
					"apv_sub_order"			=> (($tmp_apv_sub_order == 1) ? null : $tmp_apv_sub_order),
					"multi_apv_flg"			=> $apv_info["multi_apv_flg$i"],
					"next_notice_div"		=> $apv_info["next_notice_div$i"]
				);
			}

		}

		$i = 1;
		$apv_info_edit = array();
		$apv_info_edit["approve_num"	] = count($tmp_arr_apv);
		foreach ($tmp_arr_apv as $tmp_apv) {

			$apv_info_edit["regist_emp_id$i"	]	= $tmp_apv[ "regist_emp_id"		];
			$apv_info_edit["st_div$i"			]	= $tmp_apv[ "st_div"			];
			$apv_info_edit["parent_pjt_id$i"	]	= $tmp_apv[ "parent_pjt_id"		];
			$apv_info_edit["child_pjt_id$i"		]	= $tmp_apv[ "child_pjt_id"		];
			$apv_info_edit["apv_order$i"		]	= $tmp_apv[ "apv_order"			];
			$apv_info_edit["apv_sub_order$i"	]	= $tmp_apv[ "apv_sub_order"		];
			$apv_info_edit["multi_apv_flg$i"	]	= $tmp_apv[ "multi_apv_flg"		];
			$apv_info_edit["next_notice_div$i"	]	= $tmp_apv[ "next_notice_div"	];

			$i++;
		}
		unset($tmp_arr_apv);

		$this->log->info(__FUNCTION__." END");

		return $apv_info_edit;

	}

	/**
	 * 選択済み講師の職員ID配列取得
	 * @param $req：リクエストパラメーター
	 * @return
	 */
	function get_apvteacher_info($req){

		$this->log->info(__FUNCTION__." START");

		$apvteacher_info = array();

		$arr_field_name = array_keys($req);
		foreach($arr_field_name as $field_name){
			$pos = strpos($field_name,"check_emp_id");
			if ($pos === 0){
				$apv_emp_id = $req[$field_name];
				$apvteacher_info[$field_name]=$apv_emp_id;
			}
		}

		$this->log->info(__FUNCTION__." END");

		return $apvteacher_info;

	}

	// 2012/07/19 Yamagawa add(s)
	/**
	 * 選択済み審議会の職員ID配列取得
	 * @param $req：リクエストパラメーター
	 * @return
	 */
	function get_council_info($req) {

		$this->log->info(__FUNCTION__." START");

		$council_info = array();

		$arr_field_name = array_keys($req);

		foreach ($arr_field_name as $field_name) {
			$pos = strpos($field_name,"check_council");
			if ($pos === 0) {
				$apv_emp_id = $req[$field_name];
				$council_info[$field_name] = $apv_emp_id;
			}
		}

		$this->log->info(__FUNCTION__." END");

		return $council_info;

	}
	// 2012/07/19 Yamagawa add(e)

	/**
	 * 承認登録
	 * @param $wkfw_id ワークフローID
	 * @param $apply_id 申請ID
	 * @param $apv_info 承認者情報
	 * @param $draft 下書きフラグ
	 * @param $apvteacher_num 講師選択人数
	 * @param $apvteacher_info 講師情報
	 * @param $council_num 審議会選択人数
	 * @param $council_info 審議会情報
	 */
	// 2012/07/19 Yamagawa upd(s)
	//function regist_approval_data($wkfw_id,$apply_id,$draft,$apv_info,$apvteacher_num,$apvteacher_info){
	function regist_approval_data($wkfw_id,$apply_id,$draft,$apv_info,$apvteacher_num,$apvteacher_info,$council_num,$council_info){
	// 2012/07/19 Yamagawa upd(e)

		$this->log->info(__FUNCTION__." START");

		//----------------------------------------------------------------------
		// 承認者件数取得
		//----------------------------------------------------------------------
		$approve_num = $apv_info["approve_num"	];

		//----------------------------------------------------------------------
		// 承認データテーブルモデルインスタンス作成
		//----------------------------------------------------------------------
		//$this->log->debug('★３　●承認登録前　$emp_id:'.$emp_id,__FILE__,__LINE__);
		$this->log->debug('approve_num：'.$approve_num,__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/workflow/cl_applyapv_model.php");
		$cl_applyapv_model = new cl_applyapv_model($this->con,$this->user);

		// 2012/07/19 Yamagawa add(s)
		//----------------------------------------------------------------------
		// ワークフローマスタ取得
		//----------------------------------------------------------------------
		$wkfw_info = $this->get_wkfw_info_by_wkfw_id($wkfw_id);
		// 2012/07/19 Yamagawa add(e)

		//----------------------------------------------------------------------
		// 承認者情報毎処理
		//----------------------------------------------------------------------
		$apv_order_key="";
		for($i=1; $i<=$approve_num; $i++){

			//------------------------------------------------------------------
			// 承認者情報項目補正
			//------------------------------------------------------------------
			$st_div				= $apv_info["st_div$i"];
			$apv_order			= ( $apv_info["apv_order$i"			] == "" ) ? null : $apv_info[ "apv_order$i"			];
			$multi_apv_flg		= ( $apv_info["multi_apv_flg$i"		] == "" ) ? "f"  : $apv_info[ "multi_apv_flg$i"		];
			$next_notice_div	= ( $apv_info["next_notice_div$i"	] == "" ) ? null : $apv_info[ "next_notice_div$i"	];
			$parent_pjt_id		= ( $apv_info["parent_pjt_id$i"		] == "" ) ? null : $apv_info[ "parent_pjt_id$i"		];
			$child_pjt_id		= ( $apv_info["child_pjt_id$i"		] == "" ) ? null : $apv_info[ "child_pjt_id$i"		];

			$this->log->debug('st_div：'.$st_div,__FILE__,__LINE__);

			if ($apv_order_key != $apv_order){
				$apv_sub_order_wk = 1;
				$apv_order_key = $apv_order;
			}

			//------------------------------------------------------------------
			// 承認データ登録
			//------------------------------------------------------------------

			/*
			 * 講師、かつ複数承認可能、かつ講師が二人以上の場合
			 */
			if ($st_div == 6 && $multi_apv_flg == "t" && $apvteacher_num > 1){

				foreach($apvteacher_info as $field_name => $apv_emp_id){
					$pos = strpos($field_name,"check_emp_id".$i."_");
					if ($pos === 0){
						//$apv_emp_id = $apvteacher_info[$field_name];

						// 職員情報取得
						$infos = $this->get_emp_info_by_id($apv_emp_id);

						// 承認データ登録用パラメータ設定
						$arr = array(
							"wkfw_id"			 =>	$wkfw_id,
							"apply_id"			 =>	$apply_id,
							"apv_order"			 =>	$apv_order,
							"emp_id"			 =>	$apv_emp_id,
							"apv_stat"			 =>	"0",
							"apv_date"			 =>	"",
							"apv_comment"		 =>	"",
							"st_div"			 =>	$st_div,
							"deci_flg"			 =>	"t",
							"emp_class"			 =>	$infos["emp_class"],
							"emp_attribute"		 =>	$infos["emp_attribute"],
							"emp_dept"			 =>	$infos["emp_dept"],
							"emp_st"			 =>	$infos["emp_st"],
							"apv_fix_show_flg"	 =>	"t",
							"emp_room"			 =>	$infos["emp_room"],
							"apv_sub_order"		 =>	$apv_sub_order_wk,
							"multi_apv_flg"		 =>	$multi_apv_flg,
							"next_notice_div"	 =>	$next_notice_div,
							"parent_pjt_id"		 =>	$parent_pjt_id,
							"child_pjt_id"		 =>	$child_pjt_id,
							"other_apv_flg"		 =>	"f",
							"draft_flg"			 =>	($draft == "on") ? "t" : "f",
							"eval_content"		 =>	null
						);

						// 承認データ登録
						$cl_applyapv_model->insert($arr);

						$apv_sub_order_wk++;
					}
				}

			// 2012/07/18 Yamagawa add(s)
			/*
			 * レベルＶ　レベルアップ申請　かつ　承認者が審議会　かつ　審議会が二人以上の場合
			 */
			}
			else if ($st_div == 11 && $wkfw_info["short_wkfw_name"] == "c440" && $council_num > 1) {
				foreach($council_info as $field_name => $apv_emp_id){
					$pos = strpos($field_name,"check_council".$i."_");
					if ($pos === 0){

						// 職員情報取得
						$infos = $this->get_emp_info_by_id($apv_emp_id);

						// 承認データ登録用パラメータ設定
						$arr = array(
							"wkfw_id"			 =>	$wkfw_id,
							"apply_id"			 =>	$apply_id,
							"apv_order"			 =>	$apv_order,
							"emp_id"			 =>	$apv_emp_id,
							"apv_stat"			 =>	"0",
							"apv_date"			 =>	"",
							"apv_comment"		 =>	"",
							"st_div"			 =>	$st_div,
							"deci_flg"			 =>	"t",
							"emp_class"			 =>	$infos["emp_class"],
							"emp_attribute"		 =>	$infos["emp_attribute"],
							"emp_dept"			 =>	$infos["emp_dept"],
							"emp_st"			 =>	$infos["emp_st"],
							"apv_fix_show_flg"	 =>	"t",
							"emp_room"			 =>	$infos["emp_room"],
							"apv_sub_order"		 =>	$apv_sub_order_wk,
							"multi_apv_flg"		 =>	$multi_apv_flg,
							"next_notice_div"	 =>	$next_notice_div,
							"parent_pjt_id"		 =>	$parent_pjt_id,
							"child_pjt_id"		 =>	$child_pjt_id,
							"other_apv_flg"		 =>	"f",
							"draft_flg"			 =>	($draft == "on") ? "t" : "f",
							"eval_content"		 =>	null
						);

						// 承認データ登録
						$cl_applyapv_model->insert($arr);

						$apv_sub_order_wk++;
					}
				}
			// 2012/07/18 Yamagawa add(e)

			}
			/*
			 * 講師、かつ複数承認可能、かつ講師が二人以上の場合以外
			 */
			else {

				$apv_emp_id = ($apv_info["regist_emp_id$i"] == "") ? null : $apv_info["regist_emp_id$i"];

				// 職員情報取得
				$infos = $this->get_emp_info_by_id($apv_emp_id);

				// 承認データ登録用パラメータ設定
				$arr = array(
					"wkfw_id"			 =>	$wkfw_id,
					"apply_id"			 =>	$apply_id,
					"apv_order"			 =>	$apv_order,
					"emp_id"			 =>	$apv_emp_id,
					"apv_stat"			 =>	"0",
					"apv_date"			 =>	"",
					"apv_comment"		 =>	"",
					"st_div"			 =>	$st_div,
					"deci_flg"			 =>	"t",
					"emp_class"			 =>	$infos["emp_class"],
					"emp_attribute"		 =>	$infos["emp_attribute"],
					"emp_dept"			 =>	$infos["emp_dept"],
					"emp_st"			 =>	$infos["emp_st"],
					"apv_fix_show_flg"	 =>	"t",
					"emp_room"			 =>	$infos["emp_room"],
					"apv_sub_order"		 =>	$apv_sub_order_wk,
					"multi_apv_flg"		 =>	$multi_apv_flg,
					"next_notice_div"	 =>	$next_notice_div,
					"parent_pjt_id"		 =>	$parent_pjt_id,
					"child_pjt_id"		 =>	$child_pjt_id,
					"other_apv_flg"		 =>	"f",
					"draft_flg"			 =>	($draft == "on") ? "t" : "f",
					"eval_content"		 => null
				);

				// 承認データ登録
				$cl_applyapv_model->insert($arr);

				$apv_sub_order_wk++;
			}


		}
		//$this->log->debug('★３　●承認登録後　$emp_id:'.$emp_id,__FILE__,__LINE__);

		$this->log->info(__FUNCTION__." END");

		return;

	}

	/**
	 * 添付ファイル登録
	 * @param $apply_id；申請ＩＤ
	 * @param $filename：添付ファイル情報
	 * @return
	 */
	function regist_applyfile_data($apply_id, $filename){

		$this->log->info(__FUNCTION__." START");

		//------------------------------------------------------------------------------
		// ●添付ファイル登録
		//------------------------------------------------------------------------------
		$this->log->debug('添付ファイル登録　開始',__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/workflow/cl_applyfile_model.php");
		$cl_applyfile_model = new cl_applyfile_model($this->con,$this->user);

		$no = 1;
		foreach ($filename as $tmp_filename)
		{

			$param=array(
					"apply_id"			=>	$apply_id		,
					"applyfile_no"		=>	$no				,
					"applyfile_name"	=>	$tmp_filename
			);
			$cl_applyfile_model->insert($param);

			$no++;
		}
		$this->log->debug('添付ファイル登録　終了',__FILE__,__LINE__);

		$this->log->info(__FUNCTION__." END");

		return;

	}

}

?>