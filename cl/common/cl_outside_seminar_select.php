<?
	//ini_set( 'display_errors', 1 );

	//##########################################################################
	// 各種モジュールの読み込み
	//##########################################################################
	require_once(dirname(__FILE__) . "/../../about_session.php");
	require_once(dirname(__FILE__) . "/../../about_authority.php");
	require_once(dirname(__FILE__) . "/../../about_postgres.php");
	require_once(dirname(__FILE__) . "/../../show_select_values.ini");
	require_once(dirname(__FILE__) . "/../../cl_application_list.inc");
	require_once(dirname(__FILE__) . "/../../cl_yui_calendar_util.ini");
	require_once(dirname(__FILE__) . "/../../cl_application_common.ini");
	require_once(dirname(__FILE__) . "/../../cl_common.ini");
	require_once(dirname(__FILE__) . "/../../cl_title_name.ini");
	require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
	require_once(dirname(__FILE__) . "/../../Cmx.php");
	require_once("Smarty/Smarty.class.php");
	require_once("MDB2.php");

	//##########################################################################
	// 初期処理
	//##########################################################################

	//ロガーをロード
	$log = new cl_common_log_class(basename(__FILE__));
	$log->info(basename(__FILE__)." START");

	//画面名
	$fname = $PHP_SELF;

	// パラメータ取得
	//##########################################################################
	foreach( $_REQUEST as $key => $value ) {
		$log->debug('$_REQUEST['.$key.']:'.$_REQUEST[$key],__FILE__,__LINE__);
	}
	$session		= $_POST["p_session"];
	$login_user		= $_POST["login_user"];

	$log->debug('■$login_user    :'.$login_user	,__FILE__,__LINE__);
	$log->debug('■$session       :'.$session		,__FILE__,__LINE__);

	//====================================
	// データベース接続
	//====================================
	$mdb2 = MDB2::connect(CMX_DB_DSN);

	//====================================
	// モデルをロード
	//====================================
	$log->debug('院外研修一覧モデル START',__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/../model/search/cl_outside_seminar_select_model.php");
	$cl_outside_seminar_select_model = new cl_outside_seminar_select_model($mdb2);
	$log->debug('院外研修一覧モデル END',__FILE__,__LINE__);

	//====================================
	// データ取得
	//====================================
	$data = $cl_outside_seminar_select_model->get_outside_training_list($login_user);

	//====================================
	// データベース切断
	//====================================
	$log->debug('データベース切断 START',__FILE__,__LINE__);
	$mdb2->disconnect();
	$log->debug('データベース切断 END',__FILE__,__LINE__);


	//##########################################################################
	// 画面表示
	//##########################################################################
	$smarty = new Smarty();
	$smarty->template_dir = dirname(__FILE__) . "/../view";
	$smarty->compile_dir  = dirname(__FILE__) . "/../../templates_c";

	$smarty->assign( 'session'			, $session			);
	$smarty->assign( 'login_user'		, $login_user		);
	$smarty->assign( 'data'				, $data				);

	$smarty->display(basename(__FILE__,'.php').".tpl");


	$log->info(basename(__FILE__)." END");

	$log->shutdown();

