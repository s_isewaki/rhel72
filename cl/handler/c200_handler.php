<?php
	//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");

	/*********************************************************************************
	 * 申請、承認時の　DBへの各処理   【レベルアップ申請 I】
	 *********************************************************************************/
//	require_once(dirname(__FILE__) . "../model/cl_levelup_apply_model.php");

	$log->debug("require START cl_apl_levelup_apply_model.php");
	require_once(dirname(__FILE__) . "/../model/workflow/cl_apl_levelup_apply_model.php");
	$log->debug("require END cl_apl_levelup_apply_model.php");

	$log->debug("require START cl_apl_levelup_apply_inside_training_model.php");
	require_once(dirname(__FILE__) . "/../model/workflow/cl_apl_levelup_apply_inside_training_model.php");
	$log->debug("require END cl_apl_levelup_apply_inside_training_model.php");

	$log->debug("require START cl_apl_levelup_apply_outside_seminar_model.php");
	require_once(dirname(__FILE__) . "/../model/workflow/cl_apl_levelup_apply_outside_seminar_model.php");
	$log->debug("require END cl_apl_levelup_apply_outside_seminar_model.php");

	$log->debug("require START cl_apl_levelup_apply_attached_file_model.php");
	require_once(dirname(__FILE__) . "/../model/workflow/cl_apl_levelup_apply_attached_file_model.php");
	$log->debug("require END cl_apl_levelup_apply_attached_file_model.php");

	$log->debug("require START cl_");
	require_once(dirname(__FILE__) . "/../model/workflow/cl_apl_levelup_apply_attached_file_model.php");
	$log->debug("require START ");

	$log->debug("require START cl_");
	require_once(dirname(__FILE__) . "/../model/template/cl_mst_evaluation_model.php");
	$log->debug("require START ");

class c200_handler {
	/*********************************************************************************
	 * クリニカルラダーコントロールから呼ばれる関数   【レベルアップ申請 I】
	 *********************************************************************************/
	/**
	 * @param unknown_type $mdb2
	 * @param unknown_type $user Loginユーザ情報
	 * @param unknown_type $param
	 */

	//-----------------------------------------------------
	// 新規申請
	//-----------------------------------------------------
	/**
	 * 申請時 登録処理
	 */
	function NewApl_Regist($mdb2, $user, $param){
		global $log;
		$log->info("NewApl_Regist　Start",__FILE__,__LINE__);
		$this->dataApplyAdd($mdb2, $user, $param);
	}

	/**
	 * 申請時 下書き保存
	 */
	function NewApl_Draft($mdb2, $user, $param){
		global $log;
		$log->info("NewApl_Draft　Start",__FILE__,__LINE__);
		$this->dataApplyAdd($mdb2, $user, $param);
	}

	/**
	 * 申請時 印刷
	 */
	function NewApl_Print($mdb2,$user,$param){
		global $log;
		$log->info("NewApl_Print　Start",__FILE__,__LINE__);
	}

	//-----------------------------------------------------
	// 下書き保存フォルダ
	//-----------------------------------------------------
	/**
	 * 下書き 申請
	 */
	function DraftApl_Regist($mdb2,$user,$param){
		global $log;
		$log->info("DraftApl_Regist　Start",__FILE__,__LINE__);
		$this->dataApplyEdit($mdb2, $user, $param);
	}

	/**
	 * 下書き 下書き保存
	 */
	function DraftApl_Draft($mdb2,$user,$param){
		global $log;
		$log->info("DraftApl_Draft　Start",__FILE__,__LINE__);
		$this->dataApplyEdit($mdb2, $user, $param);
	}

	/**
	 * 下書き 削除
	 */
	function DraftApl_Delete($mdb2,$user,$param){
		global $log;
		$log->info("DraftApl_Delete　Start",__FILE__,__LINE__);
		$this->dataApplyDelete($mdb2, $user, $param);
	}

	/**
	 * 下書き 印刷
	 */
	function DraftApl_Print($mdb2,$user,$param){
		global $log;
		$log->info("DraftApl_Print　Start",__FILE__,__LINE__);
	}

	//-----------------------------------------------------
	// 申請詳細
	//-----------------------------------------------------
	/**
	 * 申請詳細 更新
	 */
	function AplDetail_Update($mdb2,$user,$param){
		global $log;
		$log->info("AplDetail_Update　Start",__FILE__,__LINE__);
		$this->dataApplyEdit($mdb2, $user, $param);
	}

	/**
	 * 申請詳細 申請取消
	 */
	function AplDetail_Cancel($mdb2,$user,$param){
		global $log;
		$log->info("AplDetail_Cancel　Start",__FILE__,__LINE__);
		$this->dataApplyDelete($mdb2, $user, $param);
	}

	/**
	 * 申請詳細 再申請
	 */
	function AplDetail_ReRegist($mdb2,$user,$param){
		global $log;
		$log->info("AplDetail_ReRegist　Start",__FILE__,__LINE__);
		$this->dataApplyAdd($mdb2, $user, $param);
	}

	/**
	 * 申請詳細 印刷
	 */
	function AplDetail_Print($mdb2,$user,$param){
		global $log;
		$log->info("AplDetail_Print　Start",__FILE__,__LINE__);
	}


	//-----------------------------------------------------
	// 承認詳細
	//-----------------------------------------------------
	/**
	 * 承認詳細 登録
	 */
	function ApvDetail_Regist($mdb2 ,$user, $param){
		global $log;
		$log->info("ApvDetail_Regist　Start",__FILE__,__LINE__);
		//2012/05/28 K.Fujii upd(s)
		//$this->dataApvEdit($mdb2, $user, $param);
		$this->dataApvEdit($mdb2, $user, $param, "Edit");
		//2012/05/28 K.Fujii upd(e)
	}

	/**
	 * 承認詳細 下書き保存
	 */
	function ApvDetail_Draft($mdb2,$user,$param){
		global $log;
		$log->info("ApvDetail_Draft　Start",__FILE__,__LINE__);
		//2012/05/28 K.Fujii upd(s)
		//$this->dataApvEdit($mdb2, $user, $param);
		$this->dataApvEdit($mdb2, $user, $param, "Draft");
		//2012/05/28 K.Fujii upd(e)
	}

	/**
	 * 承認詳細 印刷
	 */
	function ApvDetail_Print($mdb2,$user,$param){
		global $log;
		$log->info("ApvDetail_Print　Start",__FILE__,__LINE__);
	}

	/**
	 * 自動承認
	 */
	function AutoApv_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info("AutoApv_Regist　Start",__FILE__,__LINE__);
	}

	/*********************************************************************************
	 * 上記から呼ばれる関数   【レベルアップ申請 I】
	 *********************************************************************************/
	/**
	 * 申請時　登録処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApplyAdd($mdb2, $user, $param){
		global $log;
		// レベルアップ申請テーブルデータのセット
		$levelup_apply_id = $this->getAplSequence_forAdd($mdb2, $user, $param);
		//$db_param = $this->setAplDbParam_fromParam(1, $mdb2, $user, $param, $levelup_apply_id);
		$Apldb_param = $this->setAplDbParam_fromParam(1, $mdb2, $user, $param, $levelup_apply_id);
		//$model = new cl_apl_levelup_apply_model($mdb2, $user);
		$cl_apl_levelup_apply_model = new cl_apl_levelup_apply_model($mdb2, $user);
		// 登録処理
		//$model->insert($db_param);
		$cl_apl_levelup_apply_model->insert($Apldb_param);

/*
		// レベルアップ申請_院内必修研修データのセット
		$model = new cl_apl_levelup_apply_inside_training_model($mdb2, $user);
		for($i = 0; $i < $param['must_hdn_data_length']; $i++){
			//レベルアップ申請_院内必修研修データセット
			$db_param = $this->setIntrDbParam_fromParam(1, $mdb2, $user, $param, $i, $levelup_apply_id);
			// 登録処理
			$model->insert($db_param);
		}

		// レベルアップ申請_院内任意研修データのセット
		$model = new cl_apl_levelup_apply_inside_training_model($mdb2, $user);
		for($i = 0; $i < $param['opt_hdn_data_length']; $i++){
			$db_param = $this->setIntrDbParam_fromParam(3, $mdb2, $user, $param, $i, $levelup_apply_id);
			// 登録処理
			$model->insert($db_param);
		}
*/
		// レベルアップ申請_院内研修データのセット
		//$model = new cl_apl_levelup_apply_inside_training_model($mdb2, $user);
		$cl_apl_levelup_apply_inside_training_model = new cl_apl_levelup_apply_inside_training_model($mdb2, $user);
		for($i = 0; $i < count($param["training_id"]); $i++){
			//レベルアップ申請_院内研修データセット
			//$db_param = $this->setIntrDbParam_fromParam(1, $mdb2, $user, $param, $i, $levelup_apply_id);
			$Intrdb_param = $this->setIntrDbParam_fromParam(1, $mdb2, $user, $param, $i, $levelup_apply_id);
			// 登録処理
			//$model->insert($db_param);
			$cl_apl_levelup_apply_inside_training_model->insert($Intrdb_param);
		}

		// レベルアップ申請_院外研修データのセット
		//$model = new cl_apl_levelup_apply_outside_seminar_model($mdb2, $user);
		$cl_apl_levelup_apply_outside_seminar_model = new cl_apl_levelup_apply_outside_seminar_model($mdb2, $user);
		for($i = 1; $i <= count($param['seminar_apply_id']); $i++){
			//$db_param = $this->setOutrDbParam_fromParam(1, $mdb2, $user, $param , $i , $levelup_apply_id);
			$Outrdb_param = $this->setOutrDbParam_fromParam(1, $mdb2, $user, $param , $i , $levelup_apply_id);
			// 登録処理
			//$model->insert($db_param);
			$cl_apl_levelup_apply_outside_seminar_model->insert($Outrdb_param);
		}

		// レベルアップ申請_添付ファイルデータのセット
		//$model = new cl_apl_levelup_apply_attached_file_model($mdb2, $user);
		$cl_apl_levelup_apply_attached_file_model = new cl_apl_levelup_apply_attached_file_model($mdb2, $user);
		for($i = 0; $i < count($param['file_division']); $i++){
			//$db_param = $this->setAttachDbParam_fromParam(1, $mdb2, $user, $param , $i , $levelup_apply_id);
			$Attachdb_param = $this->setAttachDbParam_fromParam(1, $mdb2, $user, $param , $i , $levelup_apply_id);
			// 登録処理３
			//$model->insert($db_param);
			$cl_apl_levelup_apply_attached_file_model->insert($Attachdb_param);
		}

		// マスタ履歴登録処理
		//$model = new cl_mst_evaluation_model($mdb2, $user);
		$cl_mst_evaluation_model = new cl_mst_evaluation_model($mdb2, $user);
		// パラメータ取得
		//$db_param = $this->setMstParam($levelup_apply_id);
		$Mstdb_param = $this->setMstParam($levelup_apply_id);
		// 登録処理 評価項目
		//$model->insert_evaluation_item($db_param);
		$cl_mst_evaluation_model->insert_evaluation_item($Mstdb_param);
		// 登録処理 評価基準
		//$model->insert_evaluation_group($db_param);
		$cl_mst_evaluation_model->insert_evaluation_group($Mstdb_param);
		// 登録処理 カテゴリ
		//$model->insert_evaluation_category($db_param);
		$cl_mst_evaluation_model->insert_evaluation_category($Mstdb_param);
		// 登録処理 マスタ種類
		//$model->insert_evaluation($db_param);
		$cl_mst_evaluation_model->insert_evaluation($Mstdb_param);

	}

	/**
	 * 申請一時保存時　登録処理(更新)
	 */
	function dataApplyEdit($mdb2, $user, $param){
		global $log;

		// レベルアップ申請テーブルデータのセット
		$levelup_apply_id = $param["levelup_apply_id"];
		//$db_param = $this->setAplDbParam_fromParam(2, $mdb2, $user, $param, $levelup_apply_id);
		$Apldb_param = $this->setAplDbParam_fromParam(2, $mdb2, $user, $param, $levelup_apply_id);
		//$model = new cl_apl_levelup_apply_model($mdb2, $user);
		$cl_apl_levelup_apply_model = new cl_apl_levelup_apply_model($mdb2, $user);
		// 更新処理
		//$model->update($db_param);
		$cl_apl_levelup_apply_model->update($Apldb_param);

/*
		// レベルアップ申請_院内必修研修データのセット
		$model = new cl_apl_levelup_apply_inside_training_model($mdb2, $user);
		for($i = 0; $i < $param['must_hdn_data_length']; $i++){
			//レベルアップ申請_院内必修研修データセット
			$db_param = $this->setIntrDbParam_fromParam(2, $mdb2, $user, $param, $i, $levelup_apply_id);
			// 更新処理
			$model->update($db_param);
		}

		// レベルアップ申請_院内任意研修データのセット
		$model = new cl_apl_levelup_apply_inside_training_model($mdb2, $user);
		for($i = 0; $i < $param['opt_hdn_data_length']; $i++){
			//レベルアップ申請_院内任意研修データセット
			$db_param = $this->setIntrDbParam_fromParam(2, $mdb2, $user, $param, $i, $levelup_apply_id);
			// 更新処理
			$model->update($db_param);
		}
*/
		// レベルアップ申請_院内研修データのセット
		//$model = new cl_apl_levelup_apply_inside_training_model($mdb2, $user);
		$cl_apl_levelup_apply_inside_training_model = new cl_apl_levelup_apply_inside_training_model($mdb2, $user);
		for($i = 0; $i < count($param["training_id"]); $i++){
			//レベルアップ申請_院内研修データセット
			//$db_param = $this->setIntrDbParam_fromParam(2, $mdb2, $user, $param, $i, $levelup_apply_id);
			$Intrdb_param = $this->setIntrDbParam_fromParam(2, $mdb2, $user, $param, $i, $levelup_apply_id);
			// 更新処理
			//$model->update($db_param);
			$cl_apl_levelup_apply_inside_training_model->update($Intrdb_param);
		}

		// レベルアップ申請_院外研修データのセット
		$arr_levelup_apply_outside_seminar_id = array();
		//$model = new cl_apl_levelup_apply_outside_seminar_model($mdb2, $user);
		$cl_apl_levelup_apply_outside_seminar_model = new cl_apl_levelup_apply_outside_seminar_model($mdb2, $user);
		for($i = 1; $i <= count($param['seminar_apply_id']); $i++){
			if ($param['levelup_apply_outside_seminar_id'][$i] == "") {
				//$db_param = $this->setOutrDbParam_fromParam(1, $mdb2, $user, $param , $i , $levelup_apply_id);
				$Outrdb_param = $this->setOutrDbParam_fromParam(1, $mdb2, $user, $param , $i , $levelup_apply_id);
				// 登録処理
				//$model->insert($db_param);
				$cl_apl_levelup_apply_outside_seminar_model->insert($Outrdb_param);
			} else {
				//$db_param = $this->setOutrDbParam_fromParam(2, $mdb2, $user, $param , $i , $levelup_apply_id);
				$Outrdb_param = $this->setOutrDbParam_fromParam(2, $mdb2, $user, $param , $i , $levelup_apply_id);
				// 更新処理
				//$model->update($db_param);
				$cl_apl_levelup_apply_outside_seminar_model->update($Outrdb_param);
			}
			//array_push($arr_levelup_apply_outside_seminar_id ,$db_param['levelup_apply_outside_seminar_id']);
			array_push($arr_levelup_apply_outside_seminar_id ,$Outrdb_param['levelup_apply_outside_seminar_id']);
		}
		// 削除処理
		//$model->logical_delete_from_update($param['levelup_apply_id'],$arr_levelup_apply_outside_seminar_id);
		$cl_apl_levelup_apply_outside_seminar_model->logical_delete_from_update($param['levelup_apply_id'],$arr_levelup_apply_outside_seminar_id);

		// レベルアップ申請_添付ファイルデータのセット
		$arr_levelup_apply_attached_file_id = array();
		//$model = new cl_apl_levelup_apply_attached_file_model($mdb2, $user);
		$cl_apl_levelup_apply_attached_file_model = new cl_apl_levelup_apply_attached_file_model($mdb2, $user);
		for($i = 0; $i < count($param['file_name']); $i++){
			if ($param['levelup_apply_attached_file_id'][$i] == "") {
				//$db_param = $this->setAttachDbParam_fromParam(1, $mdb2, $user, $param , $i , $levelup_apply_id);
				$Attachdb_param = $this->setAttachDbParam_fromParam(1, $mdb2, $user, $param , $i , $levelup_apply_id);
				// 登録処理
				//$model->insert($db_param);
				$cl_apl_levelup_apply_attached_file_model->insert($Attachdb_param);
			} else {
				//$db_param = $this->setAttachDbParam_fromParam(2, $mdb2, $user, $param , $i , $levelup_apply_id);
				$Attachdb_param = $this->setAttachDbParam_fromParam(2, $mdb2, $user, $param , $i , $levelup_apply_id);
				// 更新処理
				//$model->update($db_param);
				$cl_apl_levelup_apply_attached_file_model->update($Attachdb_param);
			}
			//array_push($arr_levelup_apply_attached_file_id ,$db_param['levelup_apply_attached_file_id']);
			array_push($arr_levelup_apply_attached_file_id ,$Attachdb_param['levelup_apply_attached_file_id']);

		}
		// 削除処理
		//$model->logical_delete_from_update($param['levelup_apply_id'],$arr_levelup_apply_attached_file_id);
		$cl_apl_levelup_apply_attached_file_model->logical_delete_from_update($param['levelup_apply_id'],$arr_levelup_apply_attached_file_id);
	}

	/**
	 * 削除処理
	 */
	function dataApplyDelete($mdb2,$user,$param){
		global $log;

		// レベルアップ申請テーブルデータのデリート
		//$model = new cl_apl_levelup_apply_model($mdb2, $user);
		$cl_apl_levelup_apply_model = new cl_apl_levelup_apply_model($mdb2, $user);
		// 論理削除処理
		//$model->logical_delete($param["levelup_apply_id"]);
		$cl_apl_levelup_apply_model->logical_delete($param["levelup_apply_id"]);

		// レベルアップ申請_院内研修データのデリート
		//$model = new cl_apl_levelup_apply_inside_training_model($mdb2, $user);
		$cl_apl_levelup_apply_inside_training_model = new cl_apl_levelup_apply_inside_training_model($mdb2, $user);
		// 論理削除処理
		//$model->logical_all_delete($param["levelup_apply_id"]);
		$cl_apl_levelup_apply_inside_training_model->logical_all_delete($param["levelup_apply_id"]);

		// レベルアップ申請_院外研修データのデリート
		//$model = new cl_apl_levelup_apply_outside_seminar_model($mdb2, $user);
		$cl_apl_levelup_apply_outside_seminar_model = new cl_apl_levelup_apply_outside_seminar_model($mdb2, $user);
		// 論理削除処理
		//$model->logical_all_delete($param["levelup_apply_id"]);
		$cl_apl_levelup_apply_outside_seminar_model->logical_all_delete($param["levelup_apply_id"]);

		// レベルアップ申請_添付ファイルデータのデリート
		//$model = new cl_apl_levelup_apply_attached_file_model($mdb2, $user);
		$cl_apl_levelup_apply_attached_file_model = new cl_apl_levelup_apply_attached_file_model($mdb2, $user);
		// 論理削除処理
		//$model->logical_all_delete($param["levelup_apply_id"]);
		$cl_apl_levelup_apply_attached_file_model->logical_all_delete($param["levelup_apply_id"]);

		// マスタ履歴登録処理
		//$model = new cl_mst_evaluation_model($mdb2, $user);
		$cl_mst_evaluation_model = new cl_mst_evaluation_model($mdb2, $user);

		// 論理削除処理(マスタ種類)
		//$model->delete_evaluation($param["levelup_apply_id"]);
		$cl_mst_evaluation_model->delete_evaluation($param["levelup_apply_id"]);
		// 論理削除処理(カテゴリ種類)
		//$model->delete_evaluation_category($param["levelup_apply_id"]);
		$cl_mst_evaluation_model->delete_evaluation_category($param["levelup_apply_id"]);
		// 論理削除処理(評価基準)
		//$model->delete_evaluation_group($param["levelup_apply_id"]);
		$cl_mst_evaluation_model->delete_evaluation_group($param["levelup_apply_id"]);
		// 論理削除処理(評価項目)
		//$model->delete_evaluation_item($param["levelup_apply_id"]);
		$cl_mst_evaluation_model->delete_evaluation_item($param["levelup_apply_id"]);
	}

	/**
	 * 承認時　更新処理
	 */
	function dataApvEdit($mdb2 ,$user, $param, $kbn){
		global $log;
		// レベルアップ申請テーブルデータのセット
		//$db_param = $this->setAplDbParam_fromParam(2, $mdb2, $user, $param , '');
		$Apldb_param = $this->setAplDbParam_fromParam(2, $mdb2, $user, $param , '');
		//$model = new cl_apl_levelup_apply_model($mdb2, $user);
		$cl_apl_levelup_apply_model = new cl_apl_levelup_apply_model($mdb2, $user);
		// 更新処理
		//$model->update_emp_id($db_param);
		$cl_apl_levelup_apply_model->update_emp_id($Apldb_param);

/*
		// レベルアップ申請_院内必修研修データのセット
		$model = new cl_apl_levelup_apply_inside_training_model($mdb2, $user);
		for($i = 0; $i < $param['must_hdn_data_length']; $i++){
			$db_param = $this->setIntrDbParam_fromParam(2, $mdb2, $user, $param, $i ,'');
			// 更新処理
			$model->update($db_param);
		}

		// レベルアップ申請_院内任意研修データのセット
		$model = new cl_apl_levelup_apply_inside_training_model($mdb2, $user);
		for($i = 0; $i < $param['opt_hdn_data_length']; $i++){
			// レベルアップ申請_院内任意研修データセット
			$db_param = $this->setIntrDbParam_fromParam(4, $mdb2, $user, $param, $i, '');
			// 更新処理
			$model->update($db_param);
		}

		// レベルアップ申請_院外研修データのセット
		$model = new cl_apl_levelup_apply_outside_seminar_model($mdb2, $user);
		for($i = 1; $i <= count($param['seminar_apply_id']); $i++){
			$db_param = $this->setOutrDbParam_fromParam(2, $mdb2, $user, $param , $i , '');
			// 更新処理
			$model->update($db_param);
		}

		// レベルアップ申請_添付ファイルデータのセット
		$model = new cl_apl_levelup_apply_attached_file_model($mdb2, $user);
		for($i = 0; $i < count($param['file_division']); $i++){
			$db_param = $this->setAttachDbParam_fromParam(2, $mdb2, $user, $param , $i , '');
			// 更新処理
			$model->update($db_param);
		}
*/

		//2012/05/22 K.Fujii ins(s)
		if($kbn == "Edit"){
			$levelup_apply_id = $param["levelup_apply_id"];
			$levelup_apply_data = $cl_apl_levelup_apply_model->getRecordByApplyId($param["apply_id"]);

			$log->debug("require START cl_applyapv_list.php",__FILE__,__LINE__);
			require_once(dirname(__FILE__) . "/../model/search/cl_applyapv_list.php");
			$log->debug("require END cl_applyapv_list.php",__FILE__,__LINE__);
			$log->debug("cl_applyapv_list インスタンス作成開始",__FILE__,__LINE__);
			$applyapv_model = new cl_applyapv_list($mdb2,$user);
			$log->debug("cl_applyapv_list インスタンス作成終了",__FILE__,__LINE__);
			// 承認者データ取得
			$log->debug("承認者データ取得開始 apply_id:".$param["apply_id"],__FILE__,__LINE__);
			$applyapv_data = $applyapv_model->getListByApplyId($param["apply_id"]);
			$log->debug("承認者データ取得終了",__FILE__,__LINE__);
			$j = 0;
			for($i=0;$i<count($applyapv_data);$i++){
				// 2012/06/14 Yamagawa upd(s)
				//if($applyapv_data[$i]["st_div"] != "11"){
				if($applyapv_data[$i]["st_div"] != "11" && $applyapv_data[$i]["apv_stat"] == "1"){
				// 2012/06/14 Yamagawa upd(e)
					$ary_apv_emp_id[$j] = $applyapv_data[$i]["emp_id"];
					$j++;
				}
			}

			$cwrkr_emp_id1 = $param["chief_emp_id"];
			$cwrkr_emp_id2 = $param["colleague_emp_id"];
			$from_emp_id = $user;
			$cnddt_emp_id = $levelup_apply_data["levelup_emp_id"];


			//職員データ取得
			$log->debug("require START empmst_model.php",__FILE__,__LINE__);
			require_once(dirname(__FILE__) . "/../model/workflow/empmst_model.php");
			$log->debug("require END empmst_model.php",__FILE__,__LINE__);
			$empmst_model = new empmst_model($mdb2,$user);
			$cwrkr_emp_data1 = $empmst_model->select($cwrkr_emp_id1);
			$cwrkr_emp_data2 = $empmst_model->select($cwrkr_emp_id2);
			$from_emp_data = $empmst_model->select($from_emp_id);
			$cnddt_emp_data = $empmst_model->select($cnddt_emp_id);
			for($i=0;$i<count($ary_apv_emp_id);$i++){
				$ary_apv_emp_data[$i] = $empmst_model->select($ary_apv_emp_id[$i]);
			}

			$log->debug("require START cl_ldr_cwrkr_asmnt_auto_apply_manage_model");
			require_once(dirname(__FILE__) . "/../model/ladder/cl_ldr_cwrkr_asmnt_auto_apply_manage_model.php");
			$log->debug("require END cl_ldr_cwrkr_asmnt_auto_apply_manage_model");
			$log->debug("cl_ldr_cwrkr_asmnt_auto_apply_manage_model インスタンス作成開始",__FILE__,__LINE__);
			$auto_apply_manage_model = new cl_ldr_cwrkr_asmnt_auto_apply_manage_model($mdb2,$user);
			$log->debug("cl_ldr_cwrkr_asmnt_auto_apply_manage_model インスタンス作成終了",__FILE__,__LINE__);

			$log->debug("require START cl_ldr_cwrkr_assmnt_acknowledger_manage_model");
			require_once(dirname(__FILE__) . "/../model/ladder/cl_ldr_cwrkr_assmnt_acknowledger_manage_model.php");
			$log->debug("require END cl_ldr_cwrkr_assmnt_acknowledger_manage_model");
			$log->debug("cl_ldr_cwrkr_assmnt_acknowledger_manage_model インスタンス作成開始",__FILE__,__LINE__);
			$assmnt_acknowledger_manage_model = new cl_ldr_cwrkr_assmnt_acknowledger_manage_model($mdb2,$user);
			$log->debug("cl_ldr_cwrkr_assmnt_acknowledger_manage_model インスタンス作成終了",__FILE__,__LINE__);


			for($i=0;$i<count($ary_apv_emp_id);$i++){
				if($ary_apv_emp_id[$i] == $user){
					//同僚評価自動申請管理テーブルデータ作成
					//ID採番
					$cwrkr_asmnt_auto_apply_manage_id1 = $this->getAutoApplySequence_forAdd($mdb2, $user, $param);
					//パラメータ作成
					$colleague_division = 1;
					$emp1_param = $this->set_auto_apply_manage_param_forIns($cwrkr_asmnt_auto_apply_manage_id1,$cwrkr_emp_data1,$from_emp_data,$cnddt_emp_data,$param,$colleague_division);
					//ID採番
					$cwrkr_asmnt_auto_apply_manage_id2 = $this->getAutoApplySequence_forAdd($mdb2, $user, $param);
					//パラメータ作成
					$colleague_division = 2;
					$emp2_param = $this->set_auto_apply_manage_param_forIns($cwrkr_asmnt_auto_apply_manage_id2,$cwrkr_emp_data2,$from_emp_data,$cnddt_emp_data,$param,$colleague_division);

					//データ登録
					$auto_apply_manage_model->insert($emp1_param);
					$auto_apply_manage_model->insert($emp2_param);

					//同僚評価承認者管理テーブルデータ作成
					//パラメータ作成
					$apv_order = 1;
					$apv_sub_order = 1;
					$emp1_acknowledger_manage_param = $this->set_acknowledger_manage_param($cwrkr_asmnt_auto_apply_manage_id1,$cwrkr_emp_data1,$param,$apv_order,$apv_sub_order);
//2012/07/31 第二階層廃止
//					for($i=0;$i<count($ary_apv_emp_data);$i++){
//						//パラメータ作成
//						$apv_order = 2;
//						$apv_sub_order = $i+1;
//						$ary_emp1_acknowledger_manage_param[$i] = $this->set_acknowledger_manage_param($cwrkr_asmnt_auto_apply_manage_id1,$ary_apv_emp_data[$i],$param,$apv_order,$apv_sub_order);
//					}
					//パラメータ作成
					$apv_order = 1;
					$apv_sub_order = 1;
					$emp2_acknowledger_manage_param = $this->set_acknowledger_manage_param($cwrkr_asmnt_auto_apply_manage_id2,$cwrkr_emp_data2,$param,$apv_order,$apv_sub_order);
//2012/07/31 第二階層廃止
//					for($i=0;$i<count($ary_apv_emp_data);$i++){
//						//パラメータ作成
//						$apv_order = 2;
//						$apv_sub_order = $i+1;
//						$ary_emp2_acknowledger_manage_param[$i] = $this->set_acknowledger_manage_param($cwrkr_asmnt_auto_apply_manage_id2,$ary_apv_emp_data[$i],$param,$apv_order,$apv_sub_order);
//					}

					//データ登録
					$assmnt_acknowledger_manage_model->insert($emp1_acknowledger_manage_param);
					$assmnt_acknowledger_manage_model->insert($emp2_acknowledger_manage_param);
//2012/07/31 第二階層廃止
//					for($i=0;$i<count($ary_apv_emp_data);$i++){
//						$assmnt_acknowledger_manage_model->insert($ary_emp1_acknowledger_manage_param[$i]);
//						$assmnt_acknowledger_manage_model->insert($ary_emp2_acknowledger_manage_param[$i]);
//					}
					break;
				}
			}
			$log->debug("require START cl_apply_model");
			require_once(dirname(__FILE__) . "/../model/workflow/cl_apply_model.php");
			$log->debug("require END cl_apply_model");
			$log->debug("cl_apply_model インスタンス作成開始",__FILE__,__LINE__);
			$cl_apply_model = new cl_apply_model($mdb2,$user);
			$log->debug("cl_apply_model インスタンス作成終了",__FILE__,__LINE__);
			//申請テーブルデータ検索
			$apply_data = $cl_apply_model->select($param["apply_id"]);
			if($apply_data["apply_stat"] == "2"){
				$proc_flg = 7;
			}else if($apply_data["apply_stat"] == "3"){
				$proc_flg = 8;
			}else if($apply_data["apply_stat"] == "1"){
				$proc_flg = 1;
			}else{
				$proc_flg = 0;
			}

			if($proc_flg != 0){
				$log->debug("require START cl_ldr_cwrkr_asmnt_select_model");
				require_once(dirname(__FILE__) . "/../model/search/cl_ldr_cwrkr_asmnt_select_model.php");
				$log->debug("require END cl_ldr_cwrkr_asmnt_select_model");
				$log->debug("cl_ldr_cwrkr_asmnt_select_model インスタンス作成開始",__FILE__,__LINE__);
				$cl_ldr_cwrkr_asmnt_select_model = new cl_ldr_cwrkr_asmnt_select_model($mdb2,$user);
				$log->debug("cl_ldr_cwrkr_asmnt_select_model インスタンス作成終了",__FILE__,__LINE__);
				//同僚評価自動申請管理テーブルデータ検索
				$auto_apply_manage_data = $cl_ldr_cwrkr_asmnt_select_model->getAutoApplyManageRecordByLevelupWkfwApplyId($param["apply_id"]);

				$log->debug("require START cl_ldr_cwrkr_asmnt_auto_apply_manage_model");
				require_once(dirname(__FILE__) . "/../model/ladder/cl_ldr_cwrkr_asmnt_auto_apply_manage_model.php");
				$log->debug("require END cl_ldr_cwrkr_asmnt_auto_apply_manage_model");
				$log->debug("cl_ldr_cwrkr_asmnt_auto_apply_manage_model インスタンス作成開始",__FILE__,__LINE__);
				$auto_apply_manage_model = new cl_ldr_cwrkr_asmnt_auto_apply_manage_model($mdb2,$user);
				$log->debug("cl_ldr_cwrkr_asmnt_auto_apply_manage_model インスタンス作成終了",__FILE__,__LINE__);
				for($i=0;$i<count($auto_apply_manage_data);$i++){
					//更新パラメータ作成
					$param = $this->set_auto_apply_manage_param_forUpd($auto_apply_manage_data[$i],$proc_flg);
					//データ登録
					$auto_apply_manage_model->update($param);
				}
			}
		}
		//2012/05/22 K.Fujii ins(e)
	}

	/**
	 * パラメータからレベルアップ申請テーブルを登録・更新するための配列にセットする
	 * @param int　$iAdd　0:Edit　1:Add
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @return array　$db_param　DBを登録・更新するための配列
	 */
	function setAplDbParam_fromParam($iAdd, $mdb2, $user, $param ,$levelup_apply_id){
		global $log;
		global $date;
		$log->debug("setAplDbParam_fromParam　Start　iAdd = " . $iAdd,__FILE__,__LINE__);


		if($iAdd == 1){
			$log->debug("setAplDbParam_fromParam　シーケンスCall Start",__FILE__,__LINE__);
		//	$levelup_apply_id = $this->getAplSequence_forAdd($mdb2, $user, $param);
			$db_param = array(
				"levelup_apply_id"		=> 					$levelup_apply_id					,
				"apply_id"				=> 					$param["apply_id"					],
				"levelup_emp_id"		=> 					$param["emp_id"						],
				"apply_date"			=> 					$param["apply_date"					],
				"level"					=> 					1									,
				"attached_target_flg"	=> 					($param['ChkKojin'] == 'on') ? 1 : 0,
				"attached_episode_flg"	=> 					($param['ChkEpisode'] == 'on') ? 1 : 0,
				"chief_emp_id"			=> 					null								,
				"colleague_emp_id"		=> 					null
			);
		}else{
			$db_param = array(
				"levelup_apply_id"		=> 					$param["levelup_apply_id"			],
				"apply_id"				=> 					$param["apply_id"					],
				"levelup_emp_id"		=> 					$param["emp_id"						],
				"apply_date"			=> 					$param["apply_date"					],
				"level"					=> 					1									,
				"attached_target_flg"	=> 					($param['ChkKojin'] == 'on') ? 1 : 0,
				"attached_episode_flg"	=> 					($param['ChkEpisode'] == 'on') ? 1 : 0,
				"chief_emp_id"			=> 					$param["chief_emp_id"				],
				"colleague_emp_id"		=> 					$param["colleague_emp_id"			]
			);
		}
		return $db_param;
	}

	/**
	 * パラメータからレベルアップ申請_院内研修を登録・更新するための配列にセットする
	 * @param int　$iAdd　0:Edit　1:Add
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @return array　$db_param　DBを登録・更新するための配列
	 */
	function setIntrDbParam_fromParam($Kbn, $mdb2, $user, $param, $rawno,$levelup_apply_id){
		global $log;
		$date = time();
		$log->debug("setIntrDbParam_fromParam　Start　iAdd = " . $iAdd,__FILE__,__LINE__);

		// insert時ID採番
//		if (($Kbn == 1) || ($Kbn == 3)){
		if ($Kbn == 1) {
			$levelup_apply_inside_training_id = $this->getIntrSequence_forAdd($mdb2, $user, $param);
		} else {
			$levelup_apply_inside_training_id = $param["levelup_apply_inside_training_id"][$rawno];
		}

//		if($param["pass_flg"][$rawno] == 'on')
//		{
			$db_param = array(
				"levelup_apply_inside_training_id"		=> 	$levelup_apply_inside_training_id		,
				"levelup_apply_id"						=> 	$levelup_apply_id						,
				"training_id"							=> 	$param["training_id"][$rawno]	,
				"must_division"							=> 	$param["must_division"][$rawno]	,
				"pass_flg"								=> 	($param["pass_flg"][$rawno] == 'on') ? 1 : 0
			);
/*
		} else {

			$db_param = array(
				"levelup_apply_inside_training_id"		=> 	$levelup_apply_inside_training_id		,
				"levelup_apply_id"						=> 	$levelup_apply_id						,
				"training_id"							=> 	$param["training_id"][$rawno]		,
				"must_division"							=> 	$param["must_division"][$rawno]	,
				"pass_flg"								=> 	0
			);
		}
*/

		return $db_param;
	}

	/**
	 * パラメータからレベルアップ申請_院外研修を登録・更新するための配列にセットする
	 * @param int　$iAdd　0:Edit　1:Add
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @return array　$db_param　DBを登録・更新するための配列
	 */
	function setOutrDbParam_fromParam($iAdd, $mdb2, $user, $param , $rowno , $levelup_apply_id){
		global $log;
		$date = time();
		$log->debug("setOutrDbParam_fromParam　Start　iAdd = " . $iAdd,__FILE__,__LINE__);

		if($iAdd == 1){
			$log->debug("setOutrDbParam_fromParam　シーケンスCall Start",__FILE__,__LINE__);
			$levelup_apply_outside_seminar_id = $this->getOutrSequence_forAdd($mdb2, $user, $param);
			$db_param = array(
				"levelup_apply_outside_seminar_id"		=> 					$levelup_apply_outside_seminar_id,
				"levelup_apply_id"						=> 					$levelup_apply_id				,
				"disp_order"							=> 					$param["disp_order"				][$rowno],
				"seminar_apply_id"						=> 					$param["seminar_apply_id"		][$rowno],
				"seminar_name"							=> 					$param["seminar_name"			][$rowno],
				"participation_division"				=> 					$param["participation_division"	][$rowno]
			);
		}else{
			$db_param = array(
				"levelup_apply_outside_seminar_id"		=> 					$param["levelup_apply_outside_seminar_id"	][$rowno],
				"levelup_apply_id"						=> 					$param["levelup_apply_id"					],
				"disp_order"							=> 					$param["disp_order"							][$rowno],
				"seminar_apply_id"						=> 					$param["seminar_apply_id"					][$rowno],
				"seminar_name"							=> 					$param["seminar_name"						][$rowno],
				"participation_division"				=> 					$param["participation_division"				][$rowno]
			);
		}

	//	$log->debug("db_param = " .print_r($db_param),__FILE__,__LINE__);
		return $db_param;
	}

	/**
	 * パラメータからレベルアップ申請_添付ファイルを登録・更新するための配列にセットする
	 * @param int　$iAdd　0:Edit　1:Add
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @return array　$db_param　DBを登録・更新するための配列
	 */
	function setAttachDbParam_fromParam($iAdd, $mdb2, $user, $param , $rowno ,$levelup_apply_id){
		global $log;
		$date = time();
		$log->debug("setAttachDbParam_fromParam　Start　iAdd = " . $iAdd,__FILE__,__LINE__);

		if($iAdd == 1){
			$log->debug("setAttachDbParam_fromParam　シーケンスCall Start",__FILE__,__LINE__);
			$levelup_apply_attached_file_id = $this->getAttachSequence_forAdd($mdb2, $user, $param);
			$db_param = array(
				"levelup_apply_attached_file_id"		=> 					$levelup_apply_attached_file_id	,
				"levelup_apply_id"						=> 					$levelup_apply_id				,
				"disp_order"							=> 					$rowno,
				"file_division"							=> 					$param["file_division"			][$rowno],
				"file_name"								=> 					$param["file_name"				][$rowno]
			);
			$log->debug("添付ファイル（元）：".$param["file_path"][$rowno],__FILE__,__LINE__);
			$log->debug("添付ファイル（新）：cl/attach/".$levelup_apply_attached_file_id,__FILE__,__LINE__);
			$ext = strrchr($param["file_path"][$rowno], ".");
			copy($param["file_path"][$rowno],"cl/attach/".$levelup_apply_attached_file_id.$ext);

		}else{
			$db_param = array(
				"levelup_apply_attached_file_id"		=> 					$param["levelup_apply_attached_file_id"	][$rowno],
				"levelup_apply_id"						=> 					$param["levelup_apply_id"				],
				"disp_order"							=> 					$rowno,
				"file_division"							=> 					$param["file_division"					][$rowno],
				"file_name"								=> 					$param["file_name"						][$rowno]
			);
		}
		return $db_param;
	}

	/**
	 * パラメータからレベルアップ申請_添付ファイルを登録・更新するための配列にセットする
	 * levelup_apply_id　$param
	 * @return array　$db_param　DBを登録・更新するための配列
	 */
	function setMstParam($levelup_apply_id){
		global $log;
		$db_param = array(
			"levelup_apply_id"		=> 	$levelup_apply_id	,
			"apply_short_wkfw_name"	=> 	"c200"				,
			"level"					=> 	1
		);
		return $db_param;
	}

	//2012/05/22 K.Fujii ins(s)
	function set_auto_apply_manage_param_forIns($auto_apply_manage_id, $cwrkr_emp_data, $from_emp_data,$cnddt_emp_data, $param, $colleague_division){
		global $log;
		$db_param = array(
			"cwrkr_asmnt_auto_apply_manage_id"	=>$auto_apply_manage_id,
			"levelup_wkfw_apply_id"				=>$param["apply_id"],
			"levelup_apply_apply_id"			=>$param["levelup_apply_id"],
			"level"								=>1,
			"cnddt_emp_id"						=>$cnddt_emp_data["emp_id"],
			"cnddt_emp_class"					=>$cnddt_emp_data["emp_class"],
			"cnddt_emp_attr"					=>$cnddt_emp_data["emp_attribute"],
			"cnddt_emp_dept"					=>$cnddt_emp_data["emp_dept"],
			"cnddt_emp_room"					=>$cnddt_emp_data["emp_room"],
			"colleague_division"				=>$colleague_division,
			"cwrkr_emp_id"						=>$cwrkr_emp_data["emp_id"],
			"cwrkr_emp_class"					=>$cwrkr_emp_data["emp_class"],
			"cwrkr_emp_attr"					=>$cwrkr_emp_data["emp_attribute"],
			"cwrkr_emp_dept"					=>$cwrkr_emp_data["emp_dept"],
			"cwrkr_emp_room"					=>$cwrkr_emp_data["emp_room"],
			"from_emp_id"						=>$from_emp_data["emp_id"],
			"from_emp_class"					=>$from_emp_data["emp_class"],
			"from_emp_attr"						=>$from_emp_data["emp_attribute"],
			"from_emp_dept"						=>$from_emp_data["emp_dept"],
			"from_emp_room"						=>$from_emp_data["emp_room"],
			"cwrkr_asmnt_wkfw_apply_id"			=>null,
			"cwrkr_asmnt_apply_apply_id"		=>null,
			"proc_flg"							=>0
		);
		return $db_param;
	}
	function set_auto_apply_manage_param_forUpd($auto_apply_manage_data,$proc_flg){
		global $log;
		$db_param = array(
			"cwrkr_asmnt_auto_apply_manage_id"	=>$auto_apply_manage_data["cwrkr_asmnt_auto_apply_manage_id"],
			"levelup_wkfw_apply_id"				=>$auto_apply_manage_data["levelup_wkfw_apply_id"],
			"levelup_apply_apply_id"			=>$auto_apply_manage_data["levelup_apply_apply_id"],
			"level"								=>$auto_apply_manage_data["level"],
			"cnddt_emp_id"						=>$auto_apply_manage_data["cnddt_emp_id"],
			"cnddt_emp_class"					=>$auto_apply_manage_data["cnddt_emp_class"],
			"cnddt_emp_attr"					=>$auto_apply_manage_data["cnddt_emp_attr"],
			"cnddt_emp_dept"					=>$auto_apply_manage_data["cnddt_emp_dept"],
			"cnddt_emp_room"					=>$auto_apply_manage_data["emp_room"],
			"colleague_division"				=>$auto_apply_manage_data["colleague_division"],
			"cwrkr_emp_id"						=>$auto_apply_manage_data["cwrkr_emp_id"],
			"cwrkr_emp_class"					=>$auto_apply_manage_data["cwrkr_emp_class"],
			"cwrkr_emp_attr"					=>$auto_apply_manage_data["cwrkr_emp_attr"],
			"cwrkr_emp_dept"					=>$auto_apply_manage_data["cwrkr_emp_dept"],
			"cwrkr_emp_room"					=>$auto_apply_manage_data["cwrkr_emp_room"],
			"from_emp_id"						=>$auto_apply_manage_data["from_emp_id"],
			"from_emp_class"					=>$auto_apply_manage_data["from_emp_class"],
			// 2012/08/07 Yamagawa upd(s)
			//"from_emp_attr"						=>$auto_apply_manage_data["from_emp_dept"],
			"from_emp_attr"						=>$auto_apply_manage_data["from_emp_attr"],
			// 2012/08/07 Yamagawa upd(e)
			"from_emp_dept"						=>$auto_apply_manage_data["from_emp_dept"],
			"from_emp_room"						=>$auto_apply_manage_data["from_emp_room"],
			"cwrkr_asmnt_wkfw_apply_id"			=>$auto_apply_manage_data["cwrkr_asmnt_wkfw_apply_id"],
			"cwrkr_asmnt_apply_apply_id"		=>$auto_apply_manage_data["cwrkr_asmnt_apply_apply_id"],
			"proc_flg"							=>$proc_flg
		);
		return $db_param;
	}

	function set_acknowledger_manage_param($auto_apply_manage_id,$emp_data,$param,$apv_order,$apv_sub_order){
		global $log;
		$db_param = array(
			"cwrkr_asmnt_auto_apply_manage_id"	=>$auto_apply_manage_id,
			"cwrkr_asmnt_wkfw_apply_id"			=>null,
			"cwrkr_asmnt_apply_apply_id"		=>null,
			"acknowledger_emp_id"				=>$emp_data["emp_id"],
			"acknowledger_emp_class"			=>$emp_data["emp_class"],
			"acknowledger_emp_attr"				=>$emp_data["emp_attribute"],
			"acknowledger_emp_dept"				=>$emp_data["emp_dept"],
			"acknowledger_emp_room"				=>$emp_data["emp_room"],
			"acknowledger_emp_st"				=>$emp_data["emp_st"],
			"st_div"							=>2,
			"apv_order"							=>$apv_order,
			"apv_sub_order"						=>$apv_sub_order
		);
		return $db_param;
	}
	//2012/05/22 K.Fujii ins(e)

	/**
	 * レベルアップ申請IDを取得する
	 * @param unknown_type　$param パラメータ
	 */
	function getAplSequence_forAdd($mdb2, $user, $param){
		global $log;
		require_once(dirname(__FILE__) . "/../model/sequence/cl_levelup_apply_id_seq_model.php");
		$model = new cl_levelup_apply_id_seq_model($mdb2, $user);
		$levelup_apply_id= $model->getSeqId();
		return $levelup_apply_id;
	}


	/**
	 * レベルアップ申請_院内研修IDを取得する
	 * @param unknown_type　$param パラメータ
	 */
	function getIntrSequence_forAdd($mdb2, $user, $param){
		global $log;
		require_once(dirname(__FILE__) . "/../model/sequence/cl_levelup_apply_inside_training_id_seq_model.php");
		$model = new cl_levelup_apply_inside_training_id_seq_model($mdb2, $user);
		$levelup_apply_inside_training_id= $model->getSeqId();
		return $levelup_apply_inside_training_id;
	}

	/**
	 * レベルアップ申請_院外研修IDを取得する
	 * @param unknown_type　$param パラメータ
	 */
	function getOutrSequence_forAdd($mdb2, $user, $param){
		global $log;
		require_once(dirname(__FILE__) . "/../model/sequence/cl_levelup_apply_outside_seminar_id_seq_model.php");
		$model = new cl_levelup_apply_outside_seminar_id_seq_model($mdb2, $user);
		$levelup_apply_outside_seminar_id= $model->getSeqId();
		return $levelup_apply_outside_seminar_id;
	}

	/**
	 * レベルアップ申請_添付ファイルIDを取得する
	 * @param unknown_type　$param パラメータ
	 */
	function getAttachSequence_forAdd($mdb2, $user, $param){
		global $log;
		require_once(dirname(__FILE__) . "/../model/sequence/cl_levelup_apply_attached_file_id_seq_model.php");
		$model = new cl_levelup_apply_attached_file_id_seq_model($mdb2, $user);
		$levelup_apply_attached_file_id= $model->getSeqId();
		return $levelup_apply_attached_file_id;
	}

	//2012/05/22 K.Fujii ins(s)
	/**
	 * 同僚評価自動申請管理IDを取得する
	 * @param unknown_type　$param パラメータ
	 */
	function getAutoApplySequence_forAdd($mdb2, $user, $param){
		global $log;
		require_once(dirname(__FILE__) . "/../model/sequence/cl_cwrkr_asmnt_auto_apply_manage_seq_model.php");
		$model = new cl_cwrkr_asmnt_auto_apply_manage_seq_model($mdb2, $user);
		$auto_apply_manage_id= $model->getSeqId();
		return $auto_apply_manage_id;
	}
	//2012/05/22 K.Fujii ins(e)

}

