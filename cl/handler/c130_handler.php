<?php
//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");

/*********************************************************************************
 * 申請、承認時の　DBへの各処理   【院内研修報告】
 *********************************************************************************/
$log->debug("require START cl_apl_inside_training_report_model.php");
require_once(dirname(__FILE__) . "/../model/template/cl_apl_inside_training_report_model.php");
$log->debug("require END cl_apl_inside_training_report_model.php");

$log->debug("require START cl_apl_inside_training_answer_model.php");
require_once(dirname(__FILE__) . "/../model/template/cl_apl_inside_training_answer_model.php");
$log->debug("require END cl_apl_inside_training_answer_model.php");

// 2012/04/21 Yamagawa add(s)
class c130_handler {
// 2012/04/21 Yamagawa add(e)

	/*********************************************************************************
	 * クリニカルラダーコントロールから呼ばれる関数   【院内研修報告】
	 *********************************************************************************/
	/**
	 * @param unknown_type $mdb2
	 * @param unknown_type $user Loginユーザ情報
	 * @param unknown_type $param
	 */

	//-----------------------------------------------------
	// 新規申請
	//-----------------------------------------------------
	/**
	 * 申請時 登録処理
	 */
	function NewApl_Regist(&$mdb2, $user, $param){
		global $log;
		$this->dataApplyAdd(&$mdb2, $user, $param);
	}

	/**
	 * 申請時 下書き保存
	 */
	function NewApl_Draft(&$mdb2, $user, $param){
		global $log;
		$this->dataApplyAdd(&$mdb2, $user, $param);
	}

	/**
	 * 申請時 印刷
	 */
	function NewApl_Print(&$mdb2,$user,$param){
	}

	//-----------------------------------------------------
	// 下書き保存フォルダ
	//-----------------------------------------------------
	/**
	 * 下書き 申請
	 */
	function DraftApl_Regist(&$mdb2,$user,$param){
		global $log;
		$this->dataApplyEdit(&$mdb2, $user, $param);
	}

	/**
	 * 下書き 下書き保存
	 */
	function DraftApl_Draft(&$mdb2,$user,$param){
		global $log;
		$this->dataApplyEdit(&$mdb2, $user, $param);
	}

	/**
	 * 下書き 削除
	 */
	function DraftApl_Delete(&$mdb2,$user,$param){
		global $log;
		$this->dataApplyDelete(&$mdb2, $user, $param);
	}

	/**
	 * 下書き 印刷
	 */
	function DraftApl_Print(&$mdb2,$user,$param){
	}

	//-----------------------------------------------------
	// 申請詳細
	//-----------------------------------------------------
	/**
	 * 申請詳細 更新
	 */
	function AplDetail_Update(&$mdb2,$user,$param){
		global $log;
		$this->dataApplyEdit(&$mdb2, $user, $param);
	}

	/**
	 * 申請詳細 申請取消
	 */
	function AplDetail_Cancel(&$mdb2,$user,$param){
		global $log;
		$this->dataApplyDelete(&$mdb2, $user, $param);
	}

	/**
	 * 申請詳細 再申請
	 */
	function AplDetail_ReRegist(&$mdb2,$user,$param){
		global $log;
		$this->dataApplyEdit(&$mdb2, $user, $param);
		//2012/03/26 hino add
	}

	/**
	 * 申請詳細 印刷
	 */
	function AplDetail_Print(&$mdb2,$user,$param){
	}


	//-----------------------------------------------------
	// 承認詳細
	//-----------------------------------------------------
	/**
	 * 承認詳細 登録
	 */
	function ApvDetail_Regist(&$mdb2 ,$user, $param){
		global $log;
		$log->info("ApvDetail_Regist　Start",__FILE__,__LINE__);
		$this->dataApvEdit(&$mdb2, $user, $param);
	}

	/**
	 * 承認詳細 下書き保存
	 */
	function ApvDetail_Draft(&$mdb2,$user,$param){
		global $log;
		$log->info("ApvDetail_Draft　Start",__FILE__,__LINE__);
		$this->dataApvEdit(&$mdb2, $user, $param);
	}

	/**
	 * 承認詳細 印刷
	 */
	function ApvDetail_Print(&$mdb2,$user,$param){
	}

	function AutoApv_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info("AutoApv_Regist　Start",__FILE__,__LINE__);
	}

	/*********************************************************************************
	 * 上記から呼ばれる関数   【院内研修申請】
	 *********************************************************************************/
	/**
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */

	/**
	 * 申請時　登録処理
	 */
	function dataApplyAdd($mdb2, $user, $param){
		global $log;


		// 研修データのセット
		$log->debug("dataApplyAdd　DBデータセットStart",__FILE__,__LINE__);
		$db_param = $this->setDbParam_fromParam(1, $mdb2, $user, $param);
		$log->debug("dataApplyAdd　DBデータセットEnd",__FILE__,__LINE__);

		$model_report = new cl_apl_inside_training_report_model($mdb2, $user);
		$log->debug("院内研修報告インスタンス取得 OK",__FILE__,__LINE__);


		// 院内研修報告登録
		$log->debug("院内研修報告 登録 Start",__FILE__,__LINE__);
		$model_report->insert($db_param);
		$log->debug("院内研修報告 登録 End",__FILE__,__LINE__);


		// 最終報告の場合
		if($param['final_flg'] == 1){

			$db_param_ans = $this->setAnswerDbParam_fromParam(1, $mdb2, $user, $param);
			$model_answer = new cl_apl_inside_training_answer_model($mdb2, $user);

			// アンケート登録
			$db_param_ans["report_id"] = $db_param["training_report_id"];
			$model_answer->insert($db_param_ans);

		}
	}

	/**
	 * 申請時一時保存処理（申請詳細更新、下書きフォルダ下書き保存）
	 */
	function dataApplyEdit($mdb2, $user, $param){
		global $log;
		// 研修データのセット
		$log->debug("dataApplyEdit　DBデータセットStart",__FILE__,__LINE__);

		$db_param = $this->setDbParam_fromParam(0, $mdb2, $user, $param);
		$log->debug("dataApplyEdit　DBデータセットEnd",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// 院内研修報告インスタンス
		//--------------------------------------------------------------------------
		$model_report = new cl_apl_inside_training_report_model($mdb2, $user);
		$log->debug("院内研修報告インスタンス取得 OK",__FILE__,__LINE__);

		// hdnのデータを使う
		$db_param["boss_comment"]    = pg_escape_string($param["hdn_boss_comment"]);
		$db_param["chief_comment"]   = pg_escape_string($param["hdn_chief_comment"]);
		$db_param["teacher_comment"] = pg_escape_string($param["hdn_teacher_comment"]);
		$db_param["sponsor_comment"] = pg_escape_string($param["hdn_sponsor_comment"]);

		//--------------------------------------------------------------------------
		// 院内研修報告更新
		//--------------------------------------------------------------------------
		$log->debug("院内研修報告 更新 Start",__FILE__,__LINE__);

		$model_report->update($db_param);
		$log->debug("院内研修報告 更新 End",__FILE__,__LINE__);

		// 最終報告の場合
		if($param['final_flg'] == 1){
			$log->debug("院内研修報告 アンケート パラメータセット Start",__FILE__,__LINE__);

			$db_param = $this->setAnswerDbParam_fromParam(0, $mdb2, $user, $param);
			$log->debug("院内研修報告 アンケート パラメータセット End",__FILE__,__LINE__);

			$model_answer = new cl_apl_inside_training_answer_model($mdb2, $user);
			$log->debug("院内研修報告 アンケートインスタンスセット　OK",__FILE__,__LINE__);

			// アンケート更新
			$model_answer->update($db_param);
			$log->debug("院内研修報告 アンケート更新　OK",__FILE__,__LINE__);
		}
	}

	/**
	 * 申請時　削除処理（論理削除）
	 */
	function dataApplyDelete($mdb2, $user, $param){
		global $log;
		$log->debug("dataApplyDelete Start",__FILE__,__LINE__);

		$model_report = new cl_apl_inside_training_report_model($mdb2, $user);
		$log->debug("院内研修報告インスタンス取得 OK",__FILE__,__LINE__);

		// 院内研修報告削除
		$model_report->logical_delete($param["apply_id"]);
	//	$log->debug("院内研修報告 論理削除 OK",__FILE__,__LINE__);

		// 最終報告の場合
		if($param['final_flg'] == 1){
			$log->debug("院内研修報告 アンケート Start",__FILE__,__LINE__);

			$model_answer = new cl_apl_inside_training_answer_model($mdb2, $user);
			$log->debug("院内研修報告 アンケート インスタンス　OK",__FILE__,__LINE__);

			// アンケート削除
			$model_answer->logical_delete($param["apply_id"]);
			$log->debug("院内研修報告 アンケート 削除　End",__FILE__,__LINE__);
		}
	}

	/**
	 * 承認時一時保存処理（承認詳細下書き保存、承認登録）
	 */
	function dataApvEdit($mdb2, $user, $param){
		global $log;
		$log->debug('$param='.str_replace("\n",",",print_r($param,true)));

		$log->debug("dataApvEdit　DBデータセットStart",__FILE__,__LINE__);

	//	$db_param = setDbParam_fromParam(0, $mdb2, $user, $param);
		$db_param = array(
			"update_user"			=>					 $user	,
			"training_report_id"	=>					 $param['training_report_id']	,

			"apply_id"				=>					 $param["apply_id"			]	,
			"training_apply_id"		=>					 $param["training_apply_id"	]	,
			"training_id"			=>					 $param["inside_training_id"]	,
			"plan_id"				=>					 $param["plan_id"			]	,
			"training_learned"		=>	pg_escape_string($param["training_learned"	])	,

			"boss_comment"			=>	pg_escape_string($param["boss_comment"		])	,
			"chief_comment"			=>	""	,
			"sponsor_comment"		=>	pg_escape_string($param["sponsor_comment"	])	,
			"teacher_comment"		=>	pg_escape_string($param["teacher_comment"	])	,
			"boss_id"				=>					 $param["boss_id"			]	,
			"chief_id"				=>					 $param["chief_id"			]	,
			"sponsor_id"			=>					 $param["sponsor_id"		]	,
			"teacher_id"			=>					 $param["teacher_id"		]
		);

		$log->debug("dataApvEdit　DBデータセットEnd",__FILE__,__LINE__);

		$log->debug("hdn_training_learned".$param["hdn_training_learned"],__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// 院内研修報告インスタンス
		//--------------------------------------------------------------------------
		$log->debug("院内研修報告インスタンス取得 start",__FILE__,__LINE__);
		$model_report = new cl_apl_inside_training_report_model($mdb2, $user);
		$log->debug("院内研修報告インスタンス取得 OK",__FILE__,__LINE__);

		// 承認詳細のときはhdn_training_learnedを使う
		//2012/05/11 K.Fujii upd(s)
		//$db_param["training_learned"] = $param["hdn_training_learned"];
		$db_param["training_learned"] = pg_escape_string($param["hdn_training_learned"]);
		//2012/05/11 K.Fujii upd(e)
		//--------------------------------------------------------------------------
		// 院内研修報告更新
		//--------------------------------------------------------------------------
		$log->debug("院内研修報告 更新 Start",__FILE__,__LINE__);
		$model_report->update($db_param);
		$log->debug("院内研修報告 更新 End",__FILE__,__LINE__);

		// 承認時のアンケートは参照のみ

	}

	/********************************************************************************
	 * パラメータからDBを登録・更新するための配列にセットする
	 * @param int　$iAdd　0:Edit　1:Add
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @return array　$db_param　DBを登録・更新するための配列
	 */
	function setDbParam_fromParam($iAdd, $mdb2, $user, $param){
		global $log;
		$log->debug("setDbParam_fromParam　Start　iAdd = " . $iAdd,__FILE__,__LINE__);

		if($iAdd == 1)
		{
			// 登録
			$log->debug("setDbParam_fromParam　シーケンスCall Start",__FILE__,__LINE__);
			$db_param1 = array(
				"training_report_id"	     => $this->getSequence_forAdd($mdb2, $user, $param)
			);

			$db_param2 = array(
				"emp_id"				=>					 $param["emp_id"			]	,

				"apply_id"				=>					 $param["apply_id"			]	,
				"training_apply_id"		=>					 $param["training_apply_id"	]	,
				"training_id"			=>					 $param["inside_training_id"]	,
				"plan_id"				=>					 $param["plan_id"			]	,
				"training_learned"		=>	pg_escape_string($param["training_learned"	])	,

				"boss_comment"			=>	pg_escape_string($param["boss_comment"		])	,
				"chief_comment"			=>	""	,
				"sponsor_comment"		=>	pg_escape_string($param["sponsor_comment"	])	,
				"teacher_comment"		=>	pg_escape_string($param["teacher_comment"	])	,
				"boss_id"				=>					 $param["boss_id"			]	,
				"chief_id"				=>					 $param["chief_id"			]	,
				"sponsor_id"			=>					 $param["sponsor_id"		]	,
				"teacher_id"			=>					 $param["teacher_id"		]
			);

		}
		else
		{
			// 更新
			$db_param1 = array(
				"update_user"				=>	$user	,
				"training_report_id"	     => $param['training_report_id']
			);

			$db_param2 = array(
				"apply_id"				=>					 $param["apply_id"			]	,
				"training_apply_id"		=>					 $param["training_apply_id"	]	,
				"training_id"			=>					 $param["inside_training_id"]	,
				"plan_id"				=>					 $param["plan_id"			]	,
				"training_learned"		=>	pg_escape_string($param["training_learned"	])	,

				"boss_comment"			=>	pg_escape_string($param["boss_comment"		])	,
				"chief_comment"			=>	""	,
				"sponsor_comment"		=>	pg_escape_string($param["sponsor_comment"	])	,
				"teacher_comment"		=>	pg_escape_string($param["teacher_comment"	])	,
				"boss_id"				=>					 $param["boss_id"			]	,
				"chief_id"				=>					 $param["chief_id"			]	,
				"sponsor_id"			=>					 $param["sponsor_id"		]	,
				"teacher_id"			=>					 $param["teacher_id"		]
			);
		}

		$db_param = array_merge($db_param1, $db_param2);
		return $db_param;
	}

	/**
	 * パラメータからアンケートDBを登録・更新するための配列にセットする
	 * @param int　$iAdd　0:Edit　1:Add
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @return array　$db_param　DBを登録・更新するための配列
	 */
	function setAnswerDbParam_fromParam($iAdd, $mdb2, $user, $param){
		global $log;
		$log->debug("setAnswerDbParam_fromParam　Start　iAdd = " . $iAdd,__FILE__,__LINE__);
		if($iAdd == 1){
			// 登録
			$log->debug("setAnswerDbParam_fromParam　シーケンスCall Start",__FILE__,__LINE__);
			$db_param1 = array(
				"report_answer_id"		=>	$this->getAnswerSequence_forAdd($mdb2, $user, $param)
			);
		}else{
			// 登録以外
			$db_param1 = array(
				"report_answer_id"			=>	$param['report_answer_id'			]
			);
		}


		$db_param2 = array(
			"emp_id"							=>					 $param["emp_id"						]	,
			"answer_id"							=>					 ""										,
			"report_id"							=>					 $param["training_report_id"			]	,
			"apply_id"							=>					 $param["apply_id"						]	,
			"training_id"						=>					 $param["inside_training_id"			]	,
			"plan_id"							=>					 $param["plan_id"						]	,
			"achievement_degree"				=>					 $param["achievment_degree"				]	,
			"mine_learned"						=>					 $param["mine_learned"					]	,
			"training_plan_degree"				=>					 $param["training_plan_degree"			]	,
			"training_plan_degree_reason"		=>	pg_escape_string($param["training_plan_degree_reason"	])	,
			"training_method"					=>					 $param["training_method"				]	,
			"training_method_reason"			=>	pg_escape_string($param["training_method_reason"		])	,
			"training_contents"					=>					 $param["training_contents"				]	,
			"training_contents_reason"			=>	pg_escape_string($param["training_contents_reason"		])	,
			"training_practice"					=>					 $param["training_practice"				]	,
			"training_practice_reason"			=>	pg_escape_string($param["training_practice_reason"		])	,
			"point_notice"						=>	pg_escape_string($param["point_notice"					])
		);

		$db_param = array_merge($db_param1, $db_param2);
		return $db_param;
	}

	/**
	 * 院内研修報告IDを取得する
	 * @param unknown_type　$param パラメータ
	 */
	function getSequence_forAdd($mdb2, $user, $param){
		global $log;
		// 院内研修報告IDの採番
		$log->debug("院内研修報告ID取得 Start",__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/sequence/cl_inside_training_report_id_seq_model.php");
		$log->debug("院内研修報告 require_once OK",__FILE__,__LINE__);

		$cl_inside_training_report_id_seq_model = new cl_inside_training_report_id_seq_model($mdb2,$user);
		$log->debug("院内研修報告 インスタンス OK",__FILE__,__LINE__);

		$training_report_id = $cl_inside_training_report_id_seq_model->getInTraReportId();
		$log->debug("院内研修報告ID取得 End   院内研修報告ID：".$training_report_id,__FILE__,__LINE__);

		return $training_report_id;
	}

	/**
	 * 院内研修アンケートIDを取得する
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @return string　$report_answer_id report_answer_id
	 */
	function getAnswerSequence_forAdd($mdb2, $user, $param){
		global $log;
		// 院内研修アンケートIDの採番
		$log->debug("院内研修報告 アンケート採番 Start",__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/sequence/cl_inside_training_enquete_id_seq_model.php");
		$log->debug("院内研修報告 アンケート require OK",__FILE__,__LINE__);

		$cl_inside_training_enquete_id_seq_model = new cl_inside_training_enquete_id_seq_model($mdb2,$user);
		$log->debug("院内研修報告 アンケート インスタンス OK",__FILE__,__LINE__);

		$report_answer_id=$cl_inside_training_enquete_id_seq_model->getId();
		$log->debug("院内研修アンケートID：".$report_answer_id,__FILE__,__LINE__);

		return $report_answer_id;
	}
// 2012/04/21 Yamagawa add(s)
}
// 2012/04/21 Yamagawa add(e)
?>