<?php
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");

/*********************************************************************************
 * ��������ǧ���Ρ�DB�ؤγƽ���   �ڱ��⸦��������С�
 *********************************************************************************/
$log->debug("require START cl_apl_inside_training_theme_present_model.php");
require_once(dirname(__FILE__) . "/../model/template/cl_apl_inside_training_theme_present_model.php");
$log->debug("require END cl_apl_inside_training_theme_present_model.php");

// 2012/04/21 Yamagawa add(s)
class c120_handler {
// 2012/04/21 Yamagawa add(e)

	//-------------------------------------------------------
	// �ڿ��������ۿ�����������¸������
	//-------------------------------------------------------

	// �ڿ��������ۿ���
	function NewApl_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		$this->AplRegist(&$mdb2, $user, $param);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}


	// �ڿ��������۲�����¸�ʢ����񤭥ե������
	function NewApl_Draft(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		$this->AplRegist(&$mdb2, $user, $param);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}


	// �ڿ��������۰���
	function NewApl_Print(&$mdb2, $user, $param){
		global $log;
		$log->debug("��������=>����",__FILE__,__LINE__);
	}

	//-------------------------------------------------------
	// �ڲ��񤭥ե�����ʰ����¸�ˡۿ�����������¸�����������
	//-------------------------------------------------------

	// �ڰ����¸�ʲ��񤭥ե�����ˡۿ���
	function DraftApl_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		$this->AplUpdate(&$mdb2, $user, $param);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}

	// �ڰ����¸�ʲ��񤭥ե�����ˡ۲�����¸
	function DraftApl_Draft(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		$this->AplUpdate(&$mdb2, $user, $param);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}

	// �ڰ����¸�ʲ��񤭥ե�����ˡۺ��
	function DraftApl_Delete(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// ���⸦��������п����ơ��֥��ǥ륤�󥹥�������
		//--------------------------------------------------------------------------
		$log->debug("model���󥹥��󥹺�������",__FILE__,__LINE__);
		$model = new cl_apl_inside_training_theme_present_model(&$mdb2, $user);
		$log->debug("model���󥹥��󥹺�����λ",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// ���⸦��������п����ơ��֥빹��
		//--------------------------------------------------------------------------
		$log->debug("logical_delete����",__FILE__,__LINE__);
		$model->logical_delete($param["apply_id"]);
		$log->debug("logical_delete��λ",__FILE__,__LINE__);
	}

	// �ڰ����¸�ʲ��񤭥ե�����ˡ۰��� ������
	function DraftApl_Print(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}



	//-------------------------------------------------------
	// �ڿ����ܺ١۹�����������á��ƿ���������
	//-------------------------------------------------------

	// �ڿ����ܺ١۹���
	function AplDetail_Update(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		$this->AplUpdate(&$mdb2, $user, $param);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}

	// �ڿ����ܺ١ۿ������
	function AplDetail_Cancel(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// ���⸦��������п����ơ��֥��ǥ륤�󥹥�������
		//--------------------------------------------------------------------------
		$log->debug("model���󥹥��󥹺�������",__FILE__,__LINE__);
		$model = new cl_apl_inside_training_theme_present_model(&$mdb2, $user);
		$log->debug("model���󥹥��󥹺�����λ",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// ���⸦��������п����ơ��֥빹��
		//--------------------------------------------------------------------------
		$log->debug("logical_delete����",__FILE__,__LINE__);
		$model->logical_delete($param["apply_id"]);
		$log->debug("logical_delete��λ",__FILE__,__LINE__);
	}


	// �ڿ����ܺ١ۺƿ��� ������
	function AplDetail_ReRegist(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);
		$log->debug("insert����(�����ܺ�=>�ƿ���)",__FILE__,__LINE__);

		$this->AplRegist(&$mdb2, $user, $param);

		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}


	// �ڿ����ܺ١۰��� ������
	function AplDetail_Print(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}




	//-------------------------------------------------------
	// �ھ�ǧ�ܺ١۲�����¸����Ͽ������
	//-------------------------------------------------------
	// �ھ�ǧ�ܺ١۲�����¸
	function ApvDetail_Draft(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		$this->ApvUpdate(&$mdb2, $user, $param);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}

	// �ھ�ǧ�ܺ١���Ͽ
	function ApvDetail_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		$this->ApvUpdate(&$mdb2, $user, $param);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}

	// �ھ�ǧ�ܺ١۰���
	function ApvDetail_Print(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}

	function AutoApv_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info("AutoApv_Regist��Start",__FILE__,__LINE__);
	}

	//**************************************************************
	// ���̽���
	//**************************************************************
	//
	// �ڿ�����Ͽ�ۡʿ���������������¸��
	function AplRegist($mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// ���⸦��������п���ID�κ���
		//--------------------------------------------------------------------------
		require_once(dirname(__FILE__) . "/../model/sequence/cl_inside_training_task_id_seq_model.php");
		$cl_inside_training_task_id_seq_model = new cl_inside_training_task_id_seq_model($mdb2,$param["emp_id"]);
		$theme_apply_id=$cl_inside_training_task_id_seq_model->getId();
		$log->debug("������п���ID��".$theme_apply_id,__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// ���⸦��������п����ơ��֥�ǡ�������
		//--------------------------------------------------------------------------
		$param_edit=array(
			"theme_apply_id"			=>					 $theme_apply_id					,
			"emp_id"					=>					 $param["emp_id"					],		// ����ID
			"apply_id"					=>					 $param["apply_id"					],		// ����ID
			"training_apply_id"			=>					 $param["training_apply_id"			],		// ���⸦������ID
			"training_id"				=>					 $param["training_id"				],		// ���⸦��ID
			"theme_id"					=>					 $param["theme_id"					],		// ���⸦������ID
			"present_date"				=>					 $param["date_y1"]."/".$param["date_m1"]."/".$param["date_d1"],
			"present_answer"			=>	pg_escape_string($param["present_answer"			]),		// ����
			"boss_comment"				=>					 "",
			"sponsor_comment"			=>					 "",
			"teacher_comment"			=>					 "",
			"boss_id"					=>					 $param["boss_id"					],
			"sponsor_id"				=>					 $param["sponsor_id"				],
			"teacher_id"				=>					 $param["teacher_id"				]
		);

		//--------------------------------------------------------------------------
		// ���⸦��������п����ơ��֥��ǥ륤�󥹥�������
		//--------------------------------------------------------------------------
		$log->debug("model���󥹥��󥹺�������",__FILE__,__LINE__);
		$model = new cl_apl_inside_training_theme_present_model($mdb2, $user);
		$log->debug("model���󥹥��󥹺�����λ",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// ���⸦��������п����ơ��֥���Ͽ
		//--------------------------------------------------------------------------
		$log->debug("insert����",__FILE__,__LINE__);
		$model->insert($param_edit);
		$log->debug("insert��λ",__FILE__,__LINE__);

		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}



	// �ڿ����������¸�� �ʿ����ܺٹ��������񤭥ե����������¸��
	function AplUpdate($mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// ���⸦��������п����ơ��֥�ǡ�������
		//--------------------------------------------------------------------------
		$param_edit=array(
			"theme_apply_id"			=>					 $param["theme_apply_id"			],
			"apply_id"					=>					 $param["apply_id"					],		// ����ID
			"training_apply_id"			=>					 $param["training_apply_id"			],		// ���⸦������ID
			"training_id"				=>					 $param["training_id"				],		// ���⸦��ID
			"theme_id"					=>					 $param["theme_id"					],		// ���⸦������ID
			"present_date"				=>					 $param["date_y1"]."/".$param["date_m1"]."/".$param["date_d1"],
			"present_answer"			=>	pg_escape_string($param["present_answer"			]),		// ����

			"boss_comment"				=>	pg_escape_string($param["hdn_boss_comment"			]),
			"sponsor_comment"			=>	pg_escape_string($param["hdn_sponsor_comment"		]),
			"teacher_comment"			=>	pg_escape_string($param["hdn_teacher_comment"		]),

			"boss_id"					=>					 $param["boss_id"					],
			"sponsor_id"				=>					 $param["sponsor_id"				],
			"teacher_id"				=>					 $param["teacher_id"				]
		);

		//--------------------------------------------------------------------------
		// ���⸦��������п����ơ��֥��ǥ륤�󥹥�������
		//--------------------------------------------------------------------------
		$log->debug("model���󥹥��󥹺������ϡʹ�����",__FILE__,__LINE__);
		$model = new cl_apl_inside_training_theme_present_model($mdb2, $user);
		$log->debug("model���󥹥��󥹺�����λ�ʹ�����",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// ���⸦��������п����ơ��֥빹��
		//--------------------------------------------------------------------------
		$log->debug("update����(����)",__FILE__,__LINE__);
		$model->update($param_edit);
		$log->debug("update��λ(����)",__FILE__,__LINE__);
	}



	// �ھ�ǧ�ܺ١۲�����¸����Ͽ
	function ApvUpdate($mdb2,$user,$param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// ���⸦��������п����ơ��֥�ǡ�������
		//--------------------------------------------------------------------------
		$param_edit=array(
			"theme_apply_id"			=>					 $param["hdn_theme_apply_id"		],
			"apply_id"					=>					 $param["hdn_apply_id"				],		// ����ID
			"training_apply_id"			=>					 $param["hdn_training_apply_id"		],		// ���⸦������ID
			"training_id"				=>					 $param["hdn_training_id"			],		// ���⸦��ID
			"theme_id"					=>					 $param["hdn_theme_id"				],		// ���⸦������ID
			"present_date"				=>					 $param["hdn_date_y1"]."/".$param["hdn_date_m1"]."/".$param["hdn_date_d1"],	//
			"present_answer"			=>	pg_escape_string($param["hdn_present_answer"		]),		// ����

			"boss_comment"				=>	pg_escape_string($param["boss_comment"				]),
			"sponsor_comment"			=>	pg_escape_string($param["sponsor_comment"			]),
			"teacher_comment"			=>	pg_escape_string($param["teacher_comment"			]),
			"boss_id"					=>					 $param["boss_id"					],
			"sponsor_id"				=>					 $param["sponsor_id"				],
			"teacher_id"				=>					 $param["teacher_id"				]
		);

		//--------------------------------------------------------------------------
		// ���⸦��������п����ơ��֥��ǥ륤�󥹥�������
		//--------------------------------------------------------------------------
		$log->debug("model���󥹥��󥹺�������",__FILE__,__LINE__);
		$model = new cl_apl_inside_training_theme_present_model($mdb2, $user);
		$log->debug("model���󥹥��󥹺�����λ",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// ���⸦��������п����ơ��֥���Ͽ
		//--------------------------------------------------------------------------
		$log->debug("update����(��ǧ�ܺ�=>������¸)",__FILE__,__LINE__);
		$model->update($param_edit);
		$log->debug("update��λ(��ǧ�ܺ�=>������¸)",__FILE__,__LINE__);

		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}
// 2012/04/21 Yamagawa add(s)
}
// 2012/04/21 Yamagawa add(e)

$log->info(basename(__FILE__)." END");
