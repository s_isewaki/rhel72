<?php
	//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");

/*********************************************************************************
 * レベルアップ認定テーブルの require
 *********************************************************************************/
$log->debug("require START cl_apl_levelup_recognize_model.php",__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/template/cl_apl_levelup_recognize_model.php");
$log->debug("require END cl_apl_levelup_recognize_model.php",__FILE__,__LINE__);

/*********************************************************************************
 * 申請テーブルの require
 *********************************************************************************/
$log->debug("require START cl_apply_model.php",__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/workflow/cl_apply_model.php");
$log->debug("require END cl_apply_model.php",__FILE__,__LINE__);

/*********************************************************************************
 * 個人プロフィールテーブルの require
 *********************************************************************************/
$log->debug("require START cl_levelup_recognize_auto_apv_model.php",__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/workflow/cl_levelup_recognize_auto_apv_model.php");
$log->debug("require END cl_levelup_recognize_auto_apv_model.php",__FILE__,__LINE__);

class c400_handler {

	/*********************************************************************************
	 * クリニカルラダーコントロールから呼ばれる関数   【院内研修申請】
	 *********************************************************************************/
	/**
	 * 【新規申請】【申請】呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function NewApl_Regist($mdb2, $user, $param){
		global $log;
		$log->info("NewApl_Regist　Start",__FILE__,__LINE__);
		$this->dataApplyAdd($mdb2, $user, $param, false);
	}

	/**
	 * 【新規申請】【下書き保存】呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function NewApl_Draft($mdb2, $user, $param){
		global $log;
		$log->info("NewApl_Draft　Start",__FILE__,__LINE__);
		$this->dataApplyAdd($mdb2, $user, $param, true);
	}

	/**
	 * 【新規申請】印刷
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function NewApl_Print($mdb2, $user, $param){
		global $log;
		$log->info("NewApl_Print　Start",__FILE__,__LINE__);
	}

	/**
	 * 【新規申請】（下書保存から）　申請　呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Regist($mdb2, $user, $param){
		global $log;
		$log->info("DraftApl_Regist　Start",__FILE__,__LINE__);
		$this->dataApplyEdit($mdb2, $user, $param, false);
	}

	/**
	 * 【新規申請】（下書保存から）　下書き保存　呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Draft($mdb2, $user, $param){
		global $log;
		$log->info("DraftApl_Draft　Start",__FILE__,__LINE__);
		$this->dataApplyEdit($mdb2, $user, $param, true);
	}

	/**
	 * 【新規申請】（下書保存から）　削除　呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Delete($mdb2, $user, $param){
		global $log;
		$log->info("DraftApl_Delete　Start",__FILE__,__LINE__);
		$this->dataApplyDelete($mdb2, $user, $param);
	}

	/**
	 * 【新規申請】（下書き保存から）　印刷
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Print($mdb2, $user, $param){
		global $log;
		$log->info("DraftApl_Print　Start",__FILE__,__LINE__);
	}

	/**
	 * 【申請詳細】更新　呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_Update($mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_Update　Start",__FILE__,__LINE__);

	}

	/**
	 * 【申請詳細】申請取消
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_Cancel($mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_Cancel　Start",__FILE__,__LINE__);

	}

	/**
	 * 【申請詳細】申請 再申請
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_ReRegist($mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_ReRegist　Start",__FILE__,__LINE__);

	}

	/**
	 * 【申請詳細】　印刷
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_Print($mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_Print　Start",__FILE__,__LINE__);
	}

	/**
	 * 【承認詳細】承認 登録
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function ApvDetail_Regist($mdb2, $user, $param){
		global $log;
		$log->info("ApvDetail_Regist　Start",__FILE__,__LINE__);
		$this->dataApproveAdd($mdb2, $user, $param);
	}

	/**
	 * 【承認詳細】承認 下書き保存
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function ApvDetail_Draft($mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_ReRegist　Start",__FILE__,__LINE__);
	}

	/**
	 * 【承認詳細】　印刷
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function ApvDetail_Print($mdb2, $user, $param){
		global $log;
		$log->info("ApvDetail_Print　Start",__FILE__,__LINE__);
	}


	/*********************************************************************************
	 * 上記から呼ばれる関数   【レベルアップ認定申請】
	 *********************************************************************************/
	/**
	 * 申請時　登録処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 * @param boolean $draft_flg 下書きの場合にtrue,それ以外はfalse
	 */
	function dataApplyAdd($mdb2, $user, $param, $draft_flg){
		global $log;
		// 研修データのセット
		$log->debug("dataApplyAdd　DBデータセットStart",__FILE__,__LINE__);
		$db_param = $this->setDbParam_fromParam(1, $mdb2, $user, $param);
		$log->debug("dataApplyAdd　DBデータセットEnd",__FILE__,__LINE__);

		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_apl_levelup_recognize_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		// 登録処理
		$log->debug("insert開始",__FILE__,__LINE__);
		$model->insert($db_param);
		$log->debug("insert終了",__FILE__,__LINE__);

		//下書きではない場合、自動承認依頼を行う。
		if($draft_flg == false)
		{
			//2012/05/22 K.Fujii ins(s)
			require_once(dirname(__FILE__) . "/../model/search/cl_applyapv_list.php");
			$cl_applyapv_list = new cl_applyapv_list($mdb2,$login_emp_id);
			// 承認者データ取得
			$log->debug("承認者データ取得開始 apply_id:".$param["apply_id"],__FILE__,__LINE__);
			$applyapv_data = $cl_applyapv_list->getListByApplyId($param["apply_id"]);
			$log->debug("承認者データ取得終了",__FILE__,__LINE__);

			//自動承認管理テーブルモデルインスタンス作成
			$log->debug("require START cl_ldr_auto_approve_manage_model");
			require_once(dirname(__FILE__) . "/../model/ladder/cl_ldr_auto_approve_manage_model.php");
			$log->debug("require END cl_ldr_auto_approve_manage_model");
			$log->debug("cl_ldr_auto_approve_manage_model インスタンス作成開始",__FILE__,__LINE__);
			$cl_ldr_auto_approve_manage_model = new cl_ldr_auto_approve_manage_model($mdb2,$user);
			$log->debug("cl_ldr_auto_approve_manage_model インスタンス作成終了",__FILE__,__LINE__);

			//自動承認管理テーブルデータ登録
			for($i=0;$i<count($applyapv_data);$i++){
				//ID採番
				$auto_approve_manage_id = $this->getAutoApproveSequence_forAdd($mdb2, $user, $param);
				//パラメータ作成
				$AutoApproveDBparam = $this->setDbParam_forAutoApproveManage($auto_approve_manage_id,$param,$applyapv_data[$i]);
				//データ登録
				$cl_ldr_auto_approve_manage_model->insert($AutoApproveDBparam);
			}
			//2012/05/22 K.Fujii ins(e)
		}

	}

	/**
	 * 申請時　更新処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 * @param boolean $draft_flg 下書きの場合にtrue,それ以外はfalse
	 */
	function dataApplyEdit($mdb2, $user, $param, $draft_flg){
		global $log;
		// 研修データのセット
		$log->debug("dataApplyEdit　DBデータセットStart",__FILE__,__LINE__);
		$db_param = $this->setDbParam_fromParam(0, $mdb2, $user, $param);
		$log->debug("dataApplyEdit　DBデータセットEnd",__FILE__,__LINE__);

		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_apl_levelup_recognize_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		// 更新処理
		$log->debug("update開始",__FILE__,__LINE__);
		$model->update($db_param);
		$log->debug("update終了",__FILE__,__LINE__);

		//下書きではない場合、自動承認依頼を行う。
		if($draft_flg == false)
		{
			require_once(dirname(__FILE__) . "/../model/search/cl_applyapv_list.php");
			$cl_applyapv_list = new cl_applyapv_list($mdb2,$login_emp_id);
			// 承認者データ取得
			$log->debug("承認者データ取得開始 apply_id:".$param["apply_id"],__FILE__,__LINE__);
			$applyapv_data = $cl_applyapv_list->getListByApplyId($param["apply_id"]);
			$log->debug("承認者データ取得終了",__FILE__,__LINE__);

			//自動承認管理テーブルモデルインスタンス作成
			$log->debug("require START cl_ldr_auto_approve_manage_model");
			require_once(dirname(__FILE__) . "/../model/ladder/cl_ldr_auto_approve_manage_model.php");
			$log->debug("require END cl_ldr_auto_approve_manage_model");
			$log->debug("cl_ldr_auto_approve_manage_model インスタンス作成開始",__FILE__,__LINE__);
			$cl_ldr_auto_approve_manage_model = new cl_ldr_auto_approve_manage_model($mdb2,$user);
			$log->debug("cl_ldr_auto_approve_manage_model インスタンス作成終了",__FILE__,__LINE__);

			//自動承認管理テーブルデータ登録
			for($i=0;$i<count($applyapv_data);$i++){
				//ID採番
				$auto_approve_manage_id = $this->getAutoApproveSequence_forAdd($mdb2, $user, $param);
				//パラメータ作成
				$AutoApproveDBparam = $this->setDbParam_forAutoApproveManage($auto_approve_manage_id,$param,$applyapv_data[$i]);
				//データ登録
				$cl_ldr_auto_approve_manage_model->insert($AutoApproveDBparam);
			}
		}

	}

	/**
	 * 申請時　削除処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApplyDelete($mdb2, $user, $param){
		global $log;

		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_apl_levelup_recognize_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		// 削除処理
		$log->debug("delete開始",__FILE__,__LINE__);
		$model->logical_delete($param["levelup_recognize_id"]);
		$log->debug("delete終了",__FILE__,__LINE__);
	}

	/**
	 * 承認時　登録処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApproveAdd($mdb2, $user, $param){
		global $log;
		$this->AutoApv_Regist($mdb2, $user, $param);
	}

	/**
	 * 【自動承認】　
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AutoApv_Regist($mdb2, $user, $param){
		global $log;

		//レベルアップ認定データ取得
		$log->debug("require START cl_levelup_recognize_list_model.php",__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/search/cl_levelup_recognize_list_model.php");
		$log->debug("require END cl_levelup_recognize_list_model.php",__FILE__,__LINE__);

		$log->debug("cl_levelup_recognize_list_model インスタンス作成開始",__FILE__,__LINE__);
		$cl_levelup_recognize_list_model = new cl_levelup_recognize_list_model($mdb2, $user);
		$log->debug("cl_levelup_recognize_list_model インスタンス作成終了",__FILE__,__LINE__);

		$log->debug("get_levelup_recognize_data 開始 apply_id = ".$param['apply_id'],__FILE__,__LINE__);
		$recognize_data = $cl_levelup_recognize_list_model->get_levelup_recognize_data($param['apply_id']);
		$log->debug("get_levelup_recognize_data 終了",__FILE__,__LINE__);

		//個人プロフィール情報取得
		$log->debug("cl_levelup_recognize_auto_apv_model インスタンス作成開始",__FILE__,__LINE__);
		$cl_levelup_recognize_auto_apv_model = new cl_levelup_recognize_auto_apv_model($mdb2, $user);
		$log->debug("cl_levelup_recognize_auto_apv_model インスタンス作成終了",__FILE__,__LINE__);

		$log->debug("個人プロフィール取得開始",__FILE__,__LINE__);
		$log->debug("emp_id:".$recognize_data['levelup_emp_id'],__FILE__,__LINE__);
		$personal_profile = $cl_levelup_recognize_auto_apv_model->select($recognize_data['levelup_emp_id']);
		$log->debug("個人プロフィール取得終了",__FILE__,__LINE__);

		//職員情報取得
		//2012/05/14 K.Fujii ins(s)
		$log->debug("require START cl_emp_class_history_list_model.php",__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/search/cl_emp_class_history_list_model.php");
		$log->debug("require END cl_emp_class_history_list_model.php",__FILE__,__LINE__);

		$log->debug("cl_emp_class_history_list_model インスタンス作成開始",__FILE__,__LINE__);
		$cl_emp_class_history_list_model = new cl_emp_class_history_list_model($mdb2, $user);
		$log->debug("cl_emp_class_history_list_model インスタンス作成終了",__FILE__,__LINE__);

		$log->debug("職員情報取得開始",__FILE__,__LINE__);
		$log->debug("emp_id:".$recognize_data['levelup_emp_id'],__FILE__,__LINE__);
		$emp_data = $cl_emp_class_history_list_model->get_emp_data($recognize_data['levelup_emp_id']);
		$log->debug("職員情報取得終了",__FILE__,__LINE__);
		//2012/05/14 K.Fujii ins(e)

		//個人プロフィール追加・更新
		$log->debug("recognize_flg:".$recognize_data['recognize_flg'],__FILE__,__LINE__);
		if($recognize_data['recognize_flg'] == "1"){
			$log->debug("個人プロフィール追加・更新開始",__FILE__,__LINE__);
			if($personal_profile["emp_id"] == ""){
				$params["emp_id"] = $recognize_data['levelup_emp_id'];
				$params["now_level"] = $recognize_data['level'];
				$params["get_level1_date"] = date("Y-m-d");
				$params["get_level2_date"] = null;
				$params["get_level3_date"] = null;
				$params["get_level4_date"] = null;
				$params["get_level5_date"] = null;
				$params["level1_total_value"] = $recognize_data['total_value'];
				$params["level2_total_value"] = null;
				$params["level3_total_value"] = null;
				$params["level4_total_value"] = null;
				$params["level5_total_value"] = null;
				$params["level1_certificate_date"] = null;
				$params["level2_certificate_date"] = null;
				$params["level3_certificate_date"] = null;
				$params["level4_certificate_date"] = null;
				$params["level5_certificate_date"] = null;
				$params["level1_levelup_recognize_id"] = $recognize_data['levelup_recognize_id'];
				$params["level2_levelup_recognize_id"] = null;
				$params["level3_levelup_recognize_id"] = null;
				$params["level4_levelup_recognize_id"] = null;
				$params["level5_levelup_recognize_id"] = null;
				//2012/05/14 K.Fujii ins(s)
				$params["get_level1_class"] = $emp_data[0]["emp_class"];
				$params["get_level2_class"] = null;
				$params["get_level3_class"] = null;
				$params["get_level4_class"] = null;
				$params["get_level5_class"] = null;
				$params["get_level1_atrb"] = $emp_data[0]["emp_attribute"];
				$params["get_level2_atrb"] = null;
				$params["get_level3_atrb"] = null;
				$params["get_level4_atrb"] = null;
				$params["get_level5_atrb"] = null;
				$params["get_level1_dept"] = $emp_data[0]["emp_dept"];
				$params["get_level2_dept"] = null;
				$params["get_level3_dept"] = null;
				$params["get_level4_dept"] = null;
				$params["get_level5_dept"] = null;
				$params["get_level1_room"] = $emp_data[0]["emp_room"];
				$params["get_level2_room"] = null;
				$params["get_level3_room"] = null;
				$params["get_level4_room"] = null;
				$params["get_level5_room"] = null;
				//2012/05/14 K.Fujii ins(e)

				$ins_upd_mode = "INS";
			}else{
				$params["emp_id"] = $personal_profile["emp_id"];
				// 2012/08/07 Yamagawa upd(s)
				//$params["now_level"] = $recognize_data['level'];
				if($personal_profile['get_level5_date'] != ""){
					$params["now_level"] = 5;
				} else if($personal_profile['get_level4_date'] != ""){
					$params["now_level"] = 4;
				} else if($personal_profile['get_level3_date'] != ""){
					$params["now_level"] = 3;
				} else if($personal_profile['get_level2_date'] != ""){
					$params["now_level"] = 2;
				} else {
					$params["now_level"] = 1;
				}
				// 2012/08/07 Yamagawa upd(e)
				$params["get_level1_date"] = date("Y-m-d");
				if($personal_profile['get_level2_date'] == ""){
					$params["get_level2_date"] = null;
				}else{
					$params["get_level2_date"] = $personal_profile['get_level2_date'];
				}
				if($personal_profile['get_level3_date'] == ""){
					$params["get_level3_date"] = null;
				}else{
					$params["get_level3_date"] = $personal_profile['get_level3_date'];
				}
				if($personal_profile['get_level4_date'] == ""){
					$params["get_level4_date"] = null;
				}else{
					$params["get_level4_date"] = $personal_profile['get_level4_date'];
				}
				if($personal_profile['get_level5_date'] == ""){
					$params["get_level5_date"] = null;
				}else{
					$params["get_level5_date"] = $personal_profile['get_level5_date'];
				}
				// 2012/08/07 Yamagawa upd(s)
				//$params["level1_total_value"] = $param['total_value'];
				$params["level1_total_value"] = $recognize_data['total_value'];
				// 2012/08/07 Yamagawa upd(e)
				if($personal_profile['level2_total_value'] == ""){
					$params["level2_total_value"] = null;
				}else{
					$params["level2_total_value"] = $personal_profile['level2_total_value'];
				}
				if($personal_profile['level3_total_value'] == ""){
					$params["level3_total_value"] = null;
				}else{
					$params["level3_total_value"] = $personal_profile['level3_total_value'];
				}
				if($personal_profile['level4_total_value'] == ""){
					$params["level4_total_value"] = null;
				}else{
					$params["level4_total_value"] = $personal_profile['level4_total_value'];
				}
				if($personal_profile['level5_total_value'] == ""){
					$params["level5_total_value"] = null;
				}else{
					$params["level5_total_value"] = $personal_profile['level5_total_value'];
				}
				if($personal_profile['level1_certificate_date'] == ""){
					$params["level1_certificate_date"] = null;
				}else{
					$params["level1_certificate_date"] = $personal_profile['level1_certificate_date'];
				}
				if($personal_profile['level2_certificate_date'] == ""){
					$params["level2_certificate_date"] = null;
				}else{
					$params["level2_certificate_date"] = $personal_profile['level2_certificate_date'];
				}
				if($personal_profile['level3_certificate_date'] == ""){
					$params["level3_certificate_date"] = null;
				}else{
					$params["level3_certificate_date"] = $personal_profile['level3_certificate_date'];
				}
				if($personal_profile['level4_certificate_date'] == ""){
					$params["level4_certificate_date"] = null;
				}else{
					$params["level4_certificate_date"] = $personal_profile['level4_certificate_date'];
				}
				if($personal_profile['level5_certificate_date'] == ""){
					$params["level5_certificate_date"] = null;
				}else{
					$params["level5_certificate_date"] = $personal_profile['level5_certificate_date'];
				}
				$params["level1_levelup_recognize_id"] = $recognize_data['levelup_recognize_id'];
				if($personal_profile['level2_levelup_recognize_id'] == ""){
					$params["level2_levelup_recognize_id"] = null;
				}else{
					$params["level2_levelup_recognize_id"] = $personal_profile['level2_levelup_recognize_id'];
				}
				if($personal_profile['level3_levelup_recognize_id'] == ""){
					$params["level3_levelup_recognize_id"] = null;
				}else{
					$params["level3_levelup_recognize_id"] = $personal_profile['level3_levelup_recognize_id'];
				}
				if($personal_profile['level4_levelup_recognize_id'] == ""){
					$params["level4_levelup_recognize_id"] = null;
				}else{
					$params["level4_levelup_recognize_id"] = $personal_profile['level4_levelup_recognize_id'];
				}
				if($personal_profile['level5_levelup_recognize_id'] == ""){
					$params["level5_levelup_recognize_id"] = null;
				}else{
					$params["level5_levelup_recognize_id"] = $personal_profile['level5_levelup_recognize_id'];
				}
				//2012/05/14 K.Fujii ins(s)
				$params["get_level1_class"] = $emp_data[0]["emp_class"];
				if($personal_profile['get_level2_class'] == ""){
					$params["get_level2_class"] = null;
				}else{
					$params["get_level2_class"] = $personal_profile['get_level2_class'];
				}
				if($personal_profile['get_level3_class'] == ""){
					$params["get_level3_class"] = null;
				}else{
					$params["get_level3_class"] = $personal_profile['get_level3_class'];
				}
				if($personal_profile['get_level4_class'] == ""){
					$params["get_level4_class"] = null;
				}else{
					$params["get_level4_class"] = $personal_profile['get_level4_class'];
				}
				if($personal_profile['get_level5_class'] == ""){
					$params["get_level5_class"] = null;
				}else{
					$params["get_level5_class"] = $personal_profile['get_level5_class'];
				}
				$params["get_level1_atrb"] = $emp_data[0]["emp_attribute"];
				if($personal_profile['get_level2_atrb'] == ""){
					$params["get_level2_atrb"] = null;
				}else{
					$params["get_level2_atrb"] = $personal_profile['get_level2_atrb'];
				}
				if($personal_profile['get_level3_atrb'] == ""){
					$params["get_level3_atrb"] = null;
				}else{
					$params["get_level3_atrb"] = $personal_profile['get_level3_atrb'];
				}
				if($personal_profile['get_level4_atrb'] == ""){
					$params["get_level4_atrb"] = null;
				}else{
					$params["get_level4_atrb"] = $personal_profile['get_level4_atrb'];
				}
				if($personal_profile['get_level5_atrb'] == ""){
					$params["get_level5_atrb"] = null;
				}else{
					$params["get_level5_atrb"] = $personal_profile['get_level5_atrb'];
				}
				$params["get_level1_dept"] = $emp_data[0]["emp_dept"];
				if($personal_profile['get_level2_dept'] == ""){
					$params["get_level2_dept"] = null;
				}else{
					$params["get_level2_dept"] = $personal_profile['get_level2_dept'];
				}
				if($personal_profile['get_level3_dept'] == ""){
					$params["get_level3_dept"] = null;
				}else{
					$params["get_level3_dept"] = $personal_profile['get_level3_dept'];
				}
				if($personal_profile['get_level4_dept'] == ""){
					$params["get_level4_dept"] = null;
				}else{
					$params["get_level4_dept"] = $personal_profile['get_level4_dept'];
				}
				if($personal_profile['get_level5_dept'] == ""){
					$params["get_level5_dept"] = null;
				}else{
					$params["get_level5_dept"] = $personal_profile['get_level5_dept'];
				}
				$params["get_level1_room"] = $emp_data[0]["emp_room"];
				if($personal_profile['get_level2_room'] == ""){
					$params["get_level2_room"] = null;
				}else{
					$params["get_level2_room"] = $personal_profile['get_level2_room'];
				}
				if($personal_profile['get_level3_room'] == ""){
					$params["get_level3_room"] = null;
				}else{
					$params["get_level3_room"] = $personal_profile['get_level3_room'];
				}
				if($personal_profile['get_level4_room'] == ""){
					$params["get_level4_room"] = null;
				}else{
					$params["get_level4_room"] = $personal_profile['get_level4_room'];
				}
				if($personal_profile['get_level5_room'] == ""){
					$params["get_level5_room"] = null;
				}else{
					$params["get_level5_room"] = $personal_profile['get_level5_room'];
				}
				//2012/05/14 K.Fujii ins(e)

				$ins_upd_mode = "UPD";
			}

			if($ins_upd_mode == "INS"){
				$log->debug("insert_level() Start",__FILE__,__LINE__);
				$rsult = $cl_levelup_recognize_auto_apv_model->insert_level($params);
				$log->debug("insert_level() End",__FILE__,__LINE__);
			}else{
				$log->debug("update_level() Start",__FILE__,__LINE__);
				$rsult = $cl_levelup_recognize_auto_apv_model->update_level($params);
				$log->debug("update_level() End",__FILE__,__LINE__);
			}

			$log->debug("個人プロフィール追加・更新終了",__FILE__,__LINE__);

			// 2012/07/26 Yamagawa add(s)
			// レベルアップ認定　認定日更新
			$log->debug("require START cl_apl_levelup_recognize_model.php",__FILE__,__LINE__);
			require_once(dirname(__FILE__) . "/../model/template/cl_apl_levelup_recognize_model.php");
			$log->debug("require END cl_apl_levelup_recognize_model.php",__FILE__,__LINE__);

			$log->debug("cl_apl_levelup_recognize_model インスタンス作成開始",__FILE__,__LINE__);
			$cl_apl_levelup_recognize_model = new cl_apl_levelup_recognize_model($mdb2, $user);
			$log->debug("cl_apl_levelup_recognize_model インスタンス作成終了",__FILE__,__LINE__);

			$recognize_param = array(
				"levelup_recognize_id"	=>	$params["level1_levelup_recognize_id"]
				,"recognize_date"		=>	$params["get_level1_date"]
			);

			$log->debug("レベルアップ認定　認定日更新開始",__FILE__,__LINE__);
			$rsult = $cl_apl_levelup_recognize_model->update($recognize_param);
			$log->debug("レベルアップ認定　認定日更新終了",__FILE__,__LINE__);
			// 2012/07/26 Yamagawa add(e)

		}
	}

	/**
	 * パラメータからDBを登録・更新するための配列にセットする
	 * @param int　$iAdd　0:Edit　1:Add
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @return array　$db_param　DBを登録・更新するための配列
	 */
	function setDbParam_fromParam($iAdd, $mdb2, $user, $param){
		global $log;
		$log->debug("setDbParam_fromParam　Start　iAdd = " . $iAdd,__FILE__,__LINE__);
		if($iAdd == 1){
			$log->debug("setDbParam_fromParam　シーケンスCall Start",__FILE__,__LINE__);
			$levelup_recognize_id = $this->getSequence_forAdd($mdb2, $user, $param);
			$db_param = array(
				"levelup_recognize_id"	=> 					$levelup_recognize_id			,
				"apply_id"				=> 					$param['apply_id'				],
				"levelup_apply_id"		=> 					$param['levelup_apply_id'		],
				"levelup_emp_id"		=> 					$param['levelup_emp_id'			],
				"level"					=> 					$param['level'					],
				"recognize_emp_id"		=> 					$param['recognize_emp_id'		],
				"recognize_date"		=> 					$param['recognize_date'			],
				"recognize_flg"			=> 					$param['recognize_flg'			],
				"reason"				=> pg_escape_string($param['reason'					]),
				"total_value"			=> 					$param['total_value'			]
			);
		}else{
			$db_param = array(
				"levelup_recognize_id"	=> 					$param['levelup_recognize_id'	],
				"apply_id"				=> 					$param['apply_id'				],
				"levelup_apply_id"		=> 					$param['levelup_apply_id'		],
				"levelup_emp_id"		=> 					$param['levelup_emp_id'			],
				"level"					=> 					$param['level'					],
				"recognize_emp_id"		=> 					$param['recognize_emp_id'		],
				"recognize_date"		=> 					$param['recognize_date'			],
				"recognize_flg"			=> 					$param['recognize_flg'			],
				"reason"				=> pg_escape_string($param['reason'					]),
				"total_value"			=> 					$param['total_value'			],
				"update_user" 			=> 					$user
			);
		}

		//$log->debug("db_param = " .print_r($db_param),__FILE__,__LINE__);
		return $db_param;
	}

	//2012/05/22 K.Fujii ins(s)
	//自動承認管理テーブル登録用パラメータ作成
	function setDbParam_forAutoApproveManage($auto_approve_manage_id,$param,$applyapv_data){
		global $log;

		$log->debug("setDbParam_forAutoApproveManage　Start",__FILE__,__LINE__);
		$db_param = array(
			"auto_approve_manage_id"		=> $auto_approve_manage_id,
			"apply_id"						=> $param["apply_id"],
			"short_wkfw_name"				=> "c400",
			"level"							=> "1",
			"apv_order"						=> $applyapv_data["apv_order"],
			"apv_sub_order"					=> $applyapv_data["apv_sub_order"],
			"last_approve_flg"				=> "1",
			"proc_flg"						=> "0"
		);

		$log->debug("setDbParam_forAutoApproveManage　End",__FILE__,__LINE__);
		return $db_param;
	}
	//2012/05/22 K.Fujii ins(e)

	/**
	 * レベルアップ認定IDを取得する
	 * @param unknown_type　$param パラメータ
	 */
	function getSequence_forAdd($mdb2, $user, $param){
		global $log;
		$log->debug("getSequence_forAdd　Sequence require Start",__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/sequence/cl_levelup_recognize_id_seq_model.php");
		$log->debug("getSequence_forAdd　Sequence require End",__FILE__,__LINE__);

		$log->debug("getSequence_forAdd　Sequence インスタンス作成 Start",__FILE__,__LINE__);
		$model = new cl_levelup_recognize_id_seq_model($mdb2, $user);
		$log->debug("getSequence_forAdd　Sequence インスタンス作成 End",__FILE__,__LINE__);

		$log->debug("getSequence_forAdd　Sequence getID Start",__FILE__,__LINE__);
		$levelup_recognize_id= $model->getSeqId();
		$log->debug("レベルアップ認定ID：".$levelup_recognize_id,__FILE__,__LINE__);
		$log->debug("getSequence_forAdd　Sequence getID End",__FILE__,__LINE__);
		return $levelup_recognize_id;

	}

	//2012/05/22 K.Fujii ins(s)
	/**
	 * 自動承認管理IDを取得する
	 * @param unknown_type　$param パラメータ
	 */
	function getAutoApproveSequence_forAdd($mdb2, $user, $param){
		global $log;
		require_once(dirname(__FILE__) . "/../model/sequence/cl_auto_approve_manage_seq_model.php");
		$model = new cl_auto_approve_manage_seq_model($mdb2, $user);
		$auto_approve_manage_id= $model->getSeqId();
		return $auto_approve_manage_id;
	}
	//2012/05/22 K.Fujii ins(e)

}

