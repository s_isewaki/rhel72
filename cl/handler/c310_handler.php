<?php
	//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");



/*********************************************************************************
 * 申請、承認時の　DBへの各処理   【評価表 II】
 *********************************************************************************/
//model呼び出し
$log->debug("require START cl_apl_levelup_evaluation_value_model.php",__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/template/cl_apl_levelup_evaluation_value_model.php");
$log->debug("require END cl_apl_levelup_evaluation_value_model.php",__FILE__,__LINE__);

$log->debug("cl_levelup_evaluation_model Start",__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/search/cl_levelup_evaluation_model.php");
$log->debug("cl_levelup_evaluation_model End",__FILE__,__LINE__);

$log->debug("require START cl_apl_levelup_evaluation_model.php",__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/template/cl_apl_levelup_evaluation_model.php");
$log->debug("require END cl_apl_levelup_evaluation_model.php",__FILE__,__LINE__);

class c310_handler {

	/*********************************************************************************
	 * クリニカルラダーコントロールから呼ばれる関数   【レベルII評価表】
	 *********************************************************************************/
	//******************************************************************************
	// 「新規申請」画面用ハンドラー
	//******************************************************************************
	/**
	 * 【新規申請】【申請】呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function NewApl_Regist($mdb2, $user, $param){
		global $log;
		$log->info("NewApl_Regist　Start",__FILE__,__LINE__);
		$this->dataApplyAdd($mdb2, $user, $param);
		$log->info("NewApl_Regist　End",__FILE__,__LINE__);
	}

	/**
	 * 【新規申請】【下書き保存】呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */

	function NewApl_Draft($mdb2, $user, $param){
		global $log;
		$log->info("NewApl_Draft　Start",__FILE__,__LINE__);
		$this->dataApplyAdd($mdb2, $user, $param);
		$log->info("NewApl_Draft　End",__FILE__,__LINE__);
	}

	/**
	 * 【新規申請】印刷
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function NewApl_Print($mdb2, $user, $param){
		global $log;
	}

	//******************************************************************************
	// 「新規申請（下書保存から）」画面用ハンドラー
	//******************************************************************************
	/**
	 * 【新規申請】（下書保存から）　申請　呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Regist($mdb2, $user, $param){
		global $log;
		$log->info("DraftApl_Regist　Start",__FILE__,__LINE__);
		$this->dataApplyEdit($mdb2, $user, $param);
		$log->info("DraftApl_Regist　End",__FILE__,__LINE__);
	}

	/**
	 * 【新規申請】（下書保存から）　下書き保存　呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Draft($mdb2, $user, $param){
		global $log;
		$log->info("DraftApl_Draft　Start",__FILE__,__LINE__);
		$this->dataApplyEdit($mdb2, $user, $param);
		$log->info("DraftApl_Draft　End",__FILE__,__LINE__);
	}

	/**
	 * 【新規申請】（下書保存から）　削除　呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Delete($mdb2, $user, $param){
		global $log;
		$log->info("DraftApl_Delete　Start",__FILE__,__LINE__);
		$this->dataApplyDelete($mdb2, $user, $param);
		$log->info("DraftApl_Delete　End",__FILE__,__LINE__);
	}

	/**
	 * 【新規申請】（下書き保存から）　印刷
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Print($mdb2, $user, $param){
		global $log;
	}

	//******************************************************************************
	// 「申請詳細」画面用ハンドラー
	//******************************************************************************
	/**
	 * 【申請詳細】更新　呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_Update($mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_Update　Start",__FILE__,__LINE__);
		$this->dataApplyEdit($mdb2, $user, $param);
		$log->info("AplDetail_Update　End",__FILE__,__LINE__);
	}

	/**
	 * 【申請詳細】申請取消
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_Cancel($mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_Cancel　Start",__FILE__,__LINE__);
		$this->dataApplyDelete($mdb2, $user, $param);
		$log->info("AplDetail_Cancel　End",__FILE__,__LINE__);
	}

	/**
	 * 【申請詳細】申請 再申請
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_ReRegist($mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_ReRegist　Start",__FILE__,__LINE__);
		$this->dataApplyAdd($mdb2, $user, $param);
		$log->info("AplDetail_ReRegist　End",__FILE__,__LINE__);
	}

	/**
	 * 【申請詳細】　印刷
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_Print($mdb2, $user, $param){
		global $log;
	}

	//******************************************************************************
	// 「承認詳細」画面用ハンドラー
	//******************************************************************************
	/**
	 * 【承認詳細】承認 登録
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function ApvDetail_Regist($mdb2, $user, $param){
		global $log;
		$log->info("ApvDetail_Regist　Start",__FILE__,__LINE__);
		$this->dataApproveAdd($mdb2, $user, $param);
		$log->info("ApvDetail_Regist　End",__FILE__,__LINE__);

		//承認処理後の画面遷移を変更します。
		//TODO 現状は以下の関数にてopenerの画面遷移を制御していますが、将来的にはフレームワークにて対応予定です。
		// 2012/10/25 Yamagawa add(s)
		// 承認時のみ遷移を有効にする
		if ($param['approve'] == '1') {
		// 2012/10/25 Yamagawa add(e)
			$log->info("approvePageMoveChange()　Start",__FILE__,__LINE__);
			$this->approvePageMoveChange($mdb2, $user, $param);
			$log->info("approvePageMoveChange()　End",__FILE__,__LINE__);
		// 2012/10/25 Yamagawa add(s)
		}
		// 2012/10/25 Yamagawa add(e)
	}

	/**
	 * 【承認詳細】承認 下書き保存
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function ApvDetail_Draft($mdb2, $user, $param){
		global $log;
		$log->info("ApvDetail_Regist　Start",__FILE__,__LINE__);
		$this->dataApproveAdd($mdb2, $user, $param);
		$log->info("ApvDetail_Regist　End",__FILE__,__LINE__);
	}

	/**
	 * 【承認詳細】　印刷
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function ApvDetail_Print($mdb2, $user, $param){
		global $log;
	}

	/**
	 * 【自動承認】　
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AutoApv_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		// 自動承認連携テーブル登録

		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}


	/*********************************************************************************
	 * 上記から呼ばれる関数   【レベルII評価表】
	 *********************************************************************************/
	/**
	 * 申請時　登録処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApplyAdd($mdb2, $user, $param, $kbn){
		global $log;

		$iAdd = 1; // 登録
		$evaluation_emp_division = 1; // 自己評価

		//新規levelup_evaluation_id採番
		$levelup_evaluation_id = $this->getSequence_forAddMain($mdb2, $user);

		//--------------------------------------------------------------------------
		//  レベルアップ評価表テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("cl_apl_levelup_evaluation_modelインスタンス作成開始",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_model = new cl_apl_levelup_evaluation_model($mdb2, $user);
		$log->debug("cl_apl_levelup_evaluation_modelインスタンス作成終了",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		//  レベルアップ評価表登録データのセット
		//--------------------------------------------------------------------------
		$log->debug("dataApplyAddMain　DBデータセットStart",__FILE__,__LINE__);
		$db_param_main = $this->setDbParam_froEvaluation($iAdd, $mdb2, $user, $param, $evaluation_emp_division, $levelup_evaluation_id);
		$log->debug("dataApplyAddMain　DBデータセットEnd",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		//  レベルアップ評価表登録
		//--------------------------------------------------------------------------
		$log->debug("レベルアップ評価表 登録 Start",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_model->insert($db_param_main);
		$log->debug("レベルアップ評価表 登録 End",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		//  レベルアップ評価点テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("cl_apl_levelup_evaluation_value_modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_apl_levelup_evaluation_value_model($mdb2, $user);
		$log->debug("cl_apl_levelup_evaluation_value_modelインスタンス作成終了",__FILE__,__LINE__);

		//自己評価点登録
		for($i = 0; $i < (int)$param["item_cnt"]; $i++){

			//--------------------------------------------------------------------------
			//  データのセット
			//--------------------------------------------------------------------------
			$log->debug("dataApplyAdd　DBデータセットStart",__FILE__,__LINE__);
			$db_param = $this->setDbParam_fromParam($iAdd, $mdb2, $user, $param, $evaluation_emp_division, $i, $levelup_evaluation_id);
			$log->debug("dataApplyAdd　DBデータセットEnd",__FILE__,__LINE__);

			//--------------------------------------------------------------------------
			//  レベルアップ評価点登録
			//--------------------------------------------------------------------------
			$log->debug("レベルアップ評価点 登録 Start",__FILE__,__LINE__);
			$model->insert($db_param);
			$log->debug("レベルアップ評価点 登録 End",__FILE__,__LINE__);

		}

	}

	/**
	 * 申請時　更新処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApplyEdit($mdb2, $user, $param, $kbn){
		global $log;

		$iAdd = 2;// 更新
		$evaluation_emp_division = "1";// 自己評価

		$levelup_evaluation_id = $param["levelup_evaluation_id"];
		//--------------------------------------------------------------------------
		//  データのセット
		//--------------------------------------------------------------------------
		$log->debug("dataApplyAddMain　DBデータセットStart",__FILE__,__LINE__);
		$db_param_main = $this->setDbParam_froEvaluation($iAdd, $mdb2, $user, $param, $evaluation_emp_division, $levelup_evaluation_id);
		$log->debug("dataApplyAddMain　DBデータセットEnd",__FILE__,__LINE__);


		//--------------------------------------------------------------------------
		//  レベルアップ評価表テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_model = new cl_apl_levelup_evaluation_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);


		//--------------------------------------------------------------------------
		//  レベルアップ評価表更新
		//--------------------------------------------------------------------------
		$log->debug("レベルアップ評価表 更新 Start",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_model->update($db_param_main);
		$log->debug("レベルアップ評価表 更新 End",__FILE__,__LINE__);


		//--------------------------------------------------------------------------
		//  レベルアップ評価点テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_value_model = new cl_apl_levelup_evaluation_value_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		for($i = 0; $i < (int)$param["item_cnt"]; $i++){

			// データのセット
			$log->debug("dataApplyEdit　DBデータセットStart",__FILE__,__LINE__);
			$db_param = $this->setDbParam_fromParam($iAdd, $mdb2, $user, $param, $evaluation_emp_division, $i, $levelup_evaluation_id);
			$log->debug("dataApplyEdit　DBデータセットEnd",__FILE__,__LINE__);


			// 更新処理
			$log->debug("update開始",__FILE__,__LINE__);
			$cl_apl_levelup_evaluation_value_model->update($db_param);
			$log->debug("update終了",__FILE__,__LINE__);
		}

	}

	/**
	 * 申請時　論理削除処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApplyDelete($mdb2, $user, $param){
		global $log;

		//--------------------------------------------------------------------------
		//評価表データの削除（論理削除）
		//--------------------------------------------------------------------------
		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_model = new cl_apl_levelup_evaluation_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		$log->debug("delete開始　levelup_evaluation_value_id:".$param["levelup_evaluation_id"],__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_model->logical_delete($param["levelup_evaluation_id"]);
		$log->debug("delete開始　levelup_evaluation_value_id:".$param["levelup_evaluation_id"],__FILE__,__LINE__);

		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_value_model = new cl_apl_levelup_evaluation_value_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		//評価点データの削除
		for($i = 0;$i<(int)$param["item_cnt"];$i++){
			// 論理削除処理
			$log->debug("delete開始　levelup_evaluation_value_id:".$param["levelup_evaluation_value_id_".$i],__FILE__,__LINE__);
			$cl_apl_levelup_evaluation_value_model->logical_delete($param["levelup_evaluation_value_id_".$i]);
			$log->debug("delete終了",__FILE__,__LINE__);
		}
	}

	/**
	 * 承認時　登録処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApproveAdd($mdb2, $user, $param){
		global $log;

		$iAdd = 2;//更新
		$evaluation_emp_division = "2";//所属長評価

		$levelup_evaluation_id = $param["levelup_evaluation_id"];
		//--------------------------------------------------------------------------
		//  データのセット
		//--------------------------------------------------------------------------
		$log->debug("dataApplyAddMain　DBデータセットStart",__FILE__,__LINE__);
		$db_param_main = $this->setDbParam_froEvaluation($iAdd, $mdb2, $user, $param, $evaluation_emp_division, $levelup_evaluation_id);
		$log->debug("dataApplyAddMain　DBデータセットEnd",__FILE__,__LINE__);


		//--------------------------------------------------------------------------
		//  テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_model = new cl_apl_levelup_evaluation_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);


		//--------------------------------------------------------------------------
		//  レベルアップ評価表更新
		//--------------------------------------------------------------------------
		$log->debug("レベルアップ評価表 更新 Start",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_model->update($db_param_main);
		$log->debug("レベルアップ評価表 更新 End",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		//  テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_value_model = new cl_apl_levelup_evaluation_value_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		// 2012/10/04 Yamagawa upd(s)
		//if($param["clp_levelup_evaluation_value_id2_0"] != ""){
		if($param["clp_levelup_evaluation_value_id2_0"] != ""){
		// 2012/10/04 Yamagawa upd(e)

			$iAdd = 0;//更新

			for($i = 0; $i < (int)$param["item_cnt"]; $i++){

				//--------------------------------------------------------------------------
				// 更新データのセット
				//--------------------------------------------------------------------------
				$log->debug("dataApplyEdit　DBデータセットStart",__FILE__,__LINE__);
				$db_param = $this->setDbParam_fromParam($iAdd, $mdb2, $user, $param, $evaluation_emp_division, $i, $levelup_evaluation_id);
				$log->debug("dataApplyEdit　DBデータセットEnd",__FILE__,__LINE__);

				//--------------------------------------------------------------------------
				// 評価点テーブルデータ更新
				//--------------------------------------------------------------------------
				$log->debug("update開始",__FILE__,__LINE__);
				$cl_apl_levelup_evaluation_value_model->update($db_param);
				$log->debug("update終了",__FILE__,__LINE__);
			}

		}else{
			$iAdd = 1;//登録

			$levelup_evaluation_id = $param["levelup_evaluation_id"];

			for($i = 0;$i < (int)$param["item_cnt"];$i++){
				//--------------------------------------------------------------------------
				// 登録データのセット
				//--------------------------------------------------------------------------
				$log->debug("setDbParam_fromParam　DBデータセットStart",__FILE__,__LINE__);
				$db_param = $this->setDbParam_fromParam($iAdd, $mdb2, $user, $param, $evaluation_emp_division, $i, $levelup_evaluation_id);
				$log->debug("setDbParam_fromParam　DBデータセットEnd",__FILE__,__LINE__);

				// 更新処理
				$log->debug("insert開始",__FILE__,__LINE__);
				$cl_apl_levelup_evaluation_value_model->insert($db_param);
				$log->debug("insert終了",__FILE__,__LINE__);
			}
		}
	}

	/********************************************************************************
	 * パラメータからDBを登録・更新するための配列にセットする
	 * @param int　$iAdd　0:Edit　1:Add
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @return array　$db_param_main　DBを登録・更新するための配列
	 */
	function setDbParam_froEvaluation($iAdd, $mdb2, $user, $param, $evaluation_emp_division, $levelup_evaluation_id){
		global $log;

		$log->debug("setDbParam_froEvaluation　Start　iAdd = " . $iAdd,__FILE__,__LINE__);
		if($iAdd == 1){// 登録
			$db_param_main = array(
				"levelup_evaluation_id"					=> $levelup_evaluation_id,
				"apply_id"								=> $param["apply_id"],
				"levelup_apply_id"						=> $param["levelup_apply_id"],
				"levelup_emp_id"						=> $param["levelup_emp_id"],
				"level"									=> $param["level"],
				"self_evaluation_date"					=> date("Y-m-d"),
				"supervisor_emp_id"						=> null,
				"supervisor_evalution_date"				=> null
			);
		}else{// 更新
			if($evaluation_emp_division == 1){
				// 自己評価
				$db_param_main = array(
					"levelup_evaluation_id"				=> $param["levelup_evaluation_id"],
					"apply_id"							=> $param["apply_id"],
					"levelup_apply_id"					=> $param["levelup_apply_id"],
					"levelup_emp_id"					=> $param["levelup_emp_id"],
					"level"								=> $param["level"],
					"self_evaluation_date"				=> date("Y-m-d"),
					"supervisor_emp_id"					=> $param["supervisor_emp_id"],
					"supervisor_evalution_date"			=> $param["supervisor_evalution_date"]
				);
			}else{
				// 所属長評価
				$db_param_main = array(
					"levelup_evaluation_id"				=> $param["levelup_evaluation_id"],
					"apply_id"							=> $param["apply_id"],
					"levelup_apply_id"					=> $param["levelup_apply_id"],
					"levelup_emp_id"					=> $param["levelup_emp_id"],
					"level"								=> $param["level"],
					"self_evaluation_date"				=> $param["self_evaluation_date"],
					"supervisor_emp_id"					=> $user,
					"supervisor_evalution_date"			=> date("Y-m-d")
				);
			}

		}
		$log->debug("setDbParam_froEvaluation　End　iAdd = " . $iAdd,__FILE__,__LINE__);
		return $db_param_main;
	}

	/********************************************************************************
	 * パラメータからDBを登録・更新するための配列にセットする
	 * @param int　$iAdd　0:Edit　1:Add
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @return array　$db_param　DBを登録・更新するための配列
	 */
	function setDbParam_fromParam($iAdd, $mdb2, $user, $param, $evaluation_emp_division, $cnt_point, $levelup_evaluation_id){
		global $log;

		$log->debug("setDbParam_fromParam　Start　iAdd = " . $iAdd,__FILE__,__LINE__);
		if($iAdd == 1){// 登録
			if($evaluation_emp_division == 1){
				// 自己評価
				$db_param = array(
					"levelup_evaluation_value_id"		=> $this->getSequence_forAdd($mdb2, $user),
					"levelup_evaluation_id"				=> $levelup_evaluation_id,
					"levelup_evaluation_colleague_id"	=> null,
					"levelup_apply_id"					=> $param["levelup_apply_id"],
					"levelup_emp_id"					=> $param["levelup_emp_id"],
					"evaluation_emp_id"					=> $user,
					"evaluation_id"						=> $param["evaluation_id_".$cnt_point],
					"category_id"						=> $param["category_id_".$cnt_point],
					"group_id"							=> $param["group_id_".$cnt_point],
					"item_id"							=> $param["item_id_".$cnt_point],
					"value"								=> $param["cmb_eval_value1_".$cnt_point],
					"evaluation_emp_division"			=> $evaluation_emp_division
				);
			}else{
				// 所属長評価
				$db_param = array(
					"levelup_evaluation_value_id"		=> $this->getSequence_forAdd($mdb2, $user),
					"levelup_evaluation_id"				=> $levelup_evaluation_id,
					"levelup_evaluation_colleague_id"	=> null,
					"levelup_apply_id"					=> $param["levelup_apply_id"],
					"levelup_emp_id"					=> $param["levelup_emp_id"],
					"evaluation_emp_id"					=> $user,
					"evaluation_id"						=> $param["evaluation_id_".$cnt_point],
					"category_id"						=> $param["category_id_".$cnt_point],
					"group_id"							=> $param["group_id_".$cnt_point],
					"item_id"							=> $param["item_id_".$cnt_point],
					"value"								=> $param["cmb_eval_value2_".$cnt_point],
					"evaluation_emp_division"			=> $evaluation_emp_division
				);
			}
		}else{// 更新
			if($evaluation_emp_division == "1"){
				// 自己評価
				$db_param = array(
					"evaluation_emp_division"			=> $evaluation_emp_division,
					"levelup_evaluation_id"				=> $param["levelup_evaluation_id"],
					"levelup_evaluation_colleague_id"	=> null,
					"levelup_apply_id"					=> $param["levelup_apply_id"],
					"levelup_emp_id"					=> $param["levelup_emp_id"],
					"evaluation_emp_id"					=> $user,
					"evaluation_id"						=> $param["evaluation_id_".$cnt_point],
					"category_id"						=> $param["category_id_".$cnt_point],
					"group_id"							=> $param["group_id_".$cnt_point],
					"item_id"							=> $param["item_id_".$cnt_point],
					"value"								=> $param["cmb_eval_value1_".$cnt_point],
					"levelup_evaluation_value_id"		=> $param["levelup_evaluation_value_id1_".$cnt_point]
				);
			}else{
				// 所属長評価
				$db_param = array(
					"evaluation_emp_division"			=> $evaluation_emp_division,
					"levelup_evaluation_id"				=> $param["levelup_evaluation_id"],
					"levelup_evaluation_colleague_id"	=> null,
					"levelup_apply_id"					=> $param["levelup_apply_id"],
					"levelup_emp_id"					=> $param["levelup_emp_id"],
					"evaluation_emp_id"					=> $user,
					"evaluation_id"						=> $param["evaluation_id_".$cnt_point],
					"category_id"						=> $param["category_id_".$cnt_point],
					"group_id"							=> $param["group_id_".$cnt_point],
					"item_id"							=> $param["item_id_".$cnt_point],
					"value"								=> $param["cmb_eval_value2_".$cnt_point],
					"levelup_evaluation_value_id"		=> $param["levelup_evaluation_value_id2_".$cnt_point]
				);
			}


		}
		$log->debug("setDbParam_fromParam　End　iAdd = " . $iAdd,__FILE__,__LINE__);
		return $db_param;
	}


	/**
	 * レベルアップ評価表のシーケンスを取得する
	 * @param unknown_type　$param パラメータ
	 */
	function getSequence_forAddMain($mdb2, $user, $param){

		global $log;
		$log->debug("getSequence_forAddMain　Sequence require Start",__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/sequence/cl_levelup_evaluation_id_seq_model.php");
		$log->debug("getSequence_forAddMain　Sequence require End",__FILE__,__LINE__);

		$log->debug("getSequence_forAddMain　Sequence インスタンス作成 Start",__FILE__,__LINE__);
		$model = new cl_levelup_evaluation_id_seq_model($mdb2, $user);
		$log->debug("getSequence_forAddMain　Sequence インスタンス作成 End",__FILE__,__LINE__);

		$log->debug("getSequence_forAddMain　Sequence getID Start",__FILE__,__LINE__);
		$levelup_evaluation_id= $model->getSeqId();
		$log->debug("レベルアップ評価ID：".$levelup_evaluation_id,__FILE__,__LINE__);
		$log->debug("getSequence_forAddMain　Sequence getID End",__FILE__,__LINE__);

		return $levelup_evaluation_id;

	}

	/**
	 * レベルアップ評価点のシーケンスを取得する
	 * @param unknown_type　$param パラメータ
	 */
	function getSequence_forAdd($mdb2, $user, $param){

		global $log;
		$log->debug("getSequence_forAdd　Sequence require Start",__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/sequence/cl_levelup_evaluation_value_id_seq_model.php");
		$log->debug("getSequence_forAdd　Sequence require End",__FILE__,__LINE__);

		$log->debug("getSequence_forAdd　Sequence インスタンス作成 Start",__FILE__,__LINE__);
		$model = new cl_levelup_evaluation_value_id_seq_model($mdb2, $user);
		$log->debug("getSequence_forAdd　Sequence インスタンス作成 End",__FILE__,__LINE__);

		$log->debug("getSequence_forAdd　Sequence getID Start",__FILE__,__LINE__);
		$levelup_evaluation_value_id= $model->getSeqId();
		$log->debug("レベルアップ評価ID：".$levelup_evaluation_value_id,__FILE__,__LINE__);
		$log->debug("getSequence_forAdd　Sequence getID End",__FILE__,__LINE__);

		return $levelup_evaluation_value_id;

	}

	/**
	 * 承認処理後の画面遷移を変更します。
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function approvePageMoveChange($mdb2, $user, $param)
	{

		//openerのreload_page()を無効にするJavaScriptを出力する。
		echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page=\"\";}</script>");

		//openerの遷移先のURLを作成する。
		$opener_url = $this->getApprovePageMoveURL($mdb2, $user, $param);

		//openerを画面遷移させるJavaScriptを出力する。
		echo("<script language=\"javascript\">if(window.opener && !window.opener.closed){window.opener.location.href=\"$opener_url\";}</script>");
	}

	/**
	 * 承認処理後の画面遷移先URLを取得します。
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function getApprovePageMoveURL($mdb2, $user, $param)
	{
		global $log;

		//============================================================
		//レベルアップ認定の管理コードからwkfw_id、wkfw_typeを取得
		//============================================================

		$recognize_short_wkfw_name = "c410";

		$log->debug("cl_wkfwmst_model require Start",__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/workflow/cl_wkfwmst_model.php");
		$log->debug("cl_wkfwmst_model require End",__FILE__,__LINE__);

		$log->debug("cl_wkfwmst_model インスタンス作成 Start",__FILE__,__LINE__);
		$cl_wkfwmst_model = new cl_wkfwmst_model($mdb2, $user);
		$log->debug("cl_wkfwmst_model インスタンス作成 End",__FILE__,__LINE__);

		$log->debug("cl_wkfwmst_model　検索 Start",__FILE__,__LINE__);
		$wkfw_data= $cl_wkfwmst_model->select_from_short_wkfw_name($recognize_short_wkfw_name);
		$log->debug("cl_wkfwmst_model　検索 End",__FILE__,__LINE__);

		$wkfw_id   = $wkfw_data["wkfw_id"];
		$wkfw_type = $wkfw_data["wkfw_type"];

		//============================================================
		//URL作成
		//============================================================

		$ts = date('YmdHis');
		$arr_millsec = explode(" ",microtime());
		$ts .= substr($arr_millsec[0],2);

		$url  = "ladder_menu.php";
		$url .= "?session=".$_POST["session"];
		$url .= "&wkfw_id=".$wkfw_id;
		$url .= "&wkfw_type=".$wkfw_type;
		$url .= "&levelup_apply_id=".$param["levelup_apply_id"];
		$url .= "&ts=".$ts;

		$log->debug("getApprovePageMoveURL:".$url,__FILE__,__LINE__);
		return $url;
	}

}

?>