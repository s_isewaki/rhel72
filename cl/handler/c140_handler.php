<?php
	//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");

/*********************************************************************************
 * 申請、承認時の　DBへの各処理   【院外研修】（計画）
 *********************************************************************************/
$log->debug("require START cl_apl_outside_seminar_model.php");
require_once(dirname(__FILE__) . "/../model/template/cl_apl_outside_seminar_model.php");
$log->debug("require END cl_apl_outside_seminar_model.php");

// 2012/04/21 Yamagawa add(s)
class c140_handler {
// 2012/04/21 Yamagawa add(e)

	//******************************************************************************
	// 「新規申請」画面用ハンドラー
	//******************************************************************************

	/**
	 * 【新規申請】【申請】
	 */
	function NewApl_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		$this->Regist(&$mdb2, $user, $param);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}
	/**
	 * 【新規申請】【下書き保存】
	 */
	function NewApl_Draft(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		$this->Regist(&$mdb2, $user, $param);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}
	/**
	 * 【新規申請】【印刷】
	 */
	function NewApl_Print(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}

	//******************************************************************************
	// 「新規申請（下書保存から）」画面用ハンドラー
	//******************************************************************************

	/**
	 * 【新規申請（下書保存から）】【申請】
	 */
	function DraftApl_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		$this->Update(&$mdb2 ,$user, $param);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}
	/**
	 * 【新規申請（下書保存から）】【下書き保存】
	 */
	function DraftApl_Draft(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		$this->Update(&$mdb2 ,$user, $param);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}
	/**
	 * 【新規申請（下書保存から）】【削除】
	 */
	function DraftApl_Delete(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// 院外研修（計画）申請テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_apl_outside_seminar_model(&$mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// 院外研修（計画）申請テーブル登録
		//--------------------------------------------------------------------------
		$log->debug("logical_delete開始",__FILE__,__LINE__);
		$model->logical_delete($param["apply_id"]);
		$log->debug("logical_delete終了",__FILE__,__LINE__);

		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}
	/**
	 * 【新規申請（下書保存から）】【印刷】
	 */
	function DraftApl_Print(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}

	//******************************************************************************
	// 「申請詳細」画面用ハンドラー
	//******************************************************************************

	/**
	 * 【申請詳細】【更新】
	 */
	function AplDetail_Update(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		$this->Update(&$mdb2 ,$user, $param);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}
	/**
	 * 【申請詳細】【申請取消】
	 */
	function AplDetail_Cancel(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// 院外研修（計画）申請テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_apl_outside_seminar_model(&$mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// 院外研修（計画）申請テーブル登録
		//--------------------------------------------------------------------------
		$log->debug("logical_delete開始",__FILE__,__LINE__);
		$model->logical_delete($param["apply_id"]);
		$log->debug("logical_delete終了",__FILE__,__LINE__);

		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}
	/**
	 * 【申請詳細】【再申請】
	 */
	function AplDetail_ReRegist(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		$this->Regist(&$mdb2, $user, $param);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}
	/**
	 * 【申請詳細】【印刷】
	 */
	function AplDetail_Print(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}
	//******************************************************************************
	// 「承認詳細」画面用ハンドラー
	//******************************************************************************
	/**
	 * 【承認詳細】【下書き保存】
	 */
	function ApvDetail_Draft(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		$this->ApvUpdate(&$mdb2, $user, $param);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}
	/**
	 * 【承認詳細】【登録】
	 */
	function ApvDetail_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		$this->ApvUpdate(&$mdb2, $user, $param);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}
	/**
	 * 【承認詳細】【印刷】
	 */
	function ApvDetail_Print(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);
		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}

	// 2012/04/21 Yamagawa add(s)
	/**
	 * 【自動承認】　
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AutoApv_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info("AutoApv_Regist　Start",__FILE__,__LINE__);
	}
	// 2012/04/21 Yamagawa add(e)

	//******************************************************************************
	// 「院外研修」用ハンドラー共通処理
	//******************************************************************************
	/**
	 * 申請データ登録
	 */
	function Regist($mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// 院外研修（計画）申請IDの採番
		//--------------------------------------------------------------------------
		require_once(dirname(__FILE__) . "/../model/sequence/cl_outside_seminar_id_seq_model.php");
		//require_once(dirname(__FILE__) . "/../model/auto/test_model.php");

		$cl_outside_seminar_id_seq_model = new cl_outside_seminar_id_seq_model($mdb2,$param["emp_id"]);
		$seminar_apply_id=$cl_outside_seminar_id_seq_model->getId();
		$log->debug("院外研修申請ID：".$seminar_apply_id,__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// 院外研修（計画）申請テーブルデータ作成
		//--------------------------------------------------------------------------

		// 届出区分が事後報告の場合は申請日を報告日として扱う
		if($param["report_division"]=="2"){
			$report_date_tmp = date('Y/m/d');
		}else{
			$report_date_tmp = null;
		}
		$log->debug("report_date_tmp：".$report_date_tmp,__FILE__,__LINE__);

		$param_edit=array(
			"seminar_apply_id"			=>					 $seminar_apply_id					,
			"seminar_id"				=>					 "",
			"apply_id"					=>					 $param["apply_id"					],		// 不要？
			"emp_id"					=>					 $param["emp_id"					],		// 職員ID
			"seminar_name"				=>	pg_escape_string($param["seminar_name"				]),		// 院外研修名称
			"report_date"				=>					 $report_date_tmp,							// 報告日
			"from_date"					=>					 $param["date_y1"]."/".$param["date_m1"]."/".$param["date_d1"],
			"to_date"					=>					 $param["date_y2"]."/".$param["date_m2"]."/".$param["date_d2"],
			"from_time"					=>					 null,
			"to_time"					=>					 null,
			"open_place"				=>	pg_escape_string($param["open_place"				]),		// open_place
			"teacher_name"				=>	pg_escape_string($param["teacher_name"				]),		// teacher_name
			"trip_division"				=>					 $param["trip_division"				],		// trip_division
			"participation_division"	=>					 $param["participation_division"	],		// participation_division
			"expense_division"			=>					 $param["expense_division"			],		// expense_division
			"report_division"			=>					 $param["report_division"			]		// report_division
		);

		//--------------------------------------------------------------------------
		// 院外研修（計画）申請テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_apl_outside_seminar_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// 院外研修（計画）申請テーブル登録
		//--------------------------------------------------------------------------
		$log->debug("insert開始",__FILE__,__LINE__);
		$model->insert($param_edit);
		$log->debug("insert終了",__FILE__,__LINE__);

		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}

	/**
	 * 更新処理
	 */
	function Update($mdb2 ,$user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// 院外研修（計画）申請テーブルデータ作成
		//--------------------------------------------------------------------------

		// 届出区分が事後報告の場合は申請日を報告日として扱う
		if($param["report_division"]=="2"){
			$report_date_tmp = date('Y/m/d');
		}else{
			$report_date_tmp = null;
		}
		$log->debug("report_date_tmp：".$report_date_tmp,__FILE__,__LINE__);

		$param_edit=array(
			"apply_id"					=>					 $param["apply_id"					],		// 申請ID
			"seminar_name"				=>	pg_escape_string($param["seminar_name"				]),		// 院外研修名称
			"report_date"				=>					 $report_date_tmp,							// 報告日
			"from_date"					=>					 $param["date_y1"]."/".$param["date_m1"]."/".$param["date_d1"],
			"to_date"					=>					 $param["date_y2"]."/".$param["date_m2"]."/".$param["date_d2"],
			"from_time"					=>					 null,
			"to_time"					=>					 null,
			"open_place"				=>	pg_escape_string($param["open_place"				]),		// open_place
			"teacher_name"				=>	pg_escape_string($param["teacher_name"				]),		// teacher_name
			"trip_division"				=>					 $param["trip_division"				],		// trip_division
			"participation_division"	=>					 $param["participation_division"	],		// participation_division
			"expense_division"			=>					 $param["expense_division"			],		// expense_division
			"report_division"			=>					 $param["report_division"			]		// report_division
		);

		//--------------------------------------------------------------------------
		// 院外研修（計画）申請テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_apl_outside_seminar_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// 院外研修（計画）申請テーブル更新
		//--------------------------------------------------------------------------
		$log->debug("update開始",__FILE__,__LINE__);
		$model->update($param_edit);
		$log->debug("update終了",__FILE__,__LINE__);

		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}

	/**
	 * 承認更新
	 */
	function ApvUpdate($mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		// 費用負担権限なし（expense_division=""） はhdnデータを使う
		// 費用負担権限なし（expense_division=1/2）は生データを使う
		$updata_expense_division = null;
		if($param["expense_division"]==""){
			$updata_expense_division = $param["hdn_expense_division"];
		}else{
			$updata_expense_division = $param["expense_division"];
		}

		$log->debug("updata_expense_division：".$updata_expense_division,__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// 院外研修（計画）申請テーブルデータ作成
		//--------------------------------------------------------------------------
	 	$param_edit=array(
			"apply_id"					=>					 $param["apply_id"					],		// 申請ID
			"seminar_name"				=>	pg_escape_string($param["hdn_seminar_name"			]),		// 院外研修名称
			"report_date"				=>					 $param["hdn_report_date"			],		// 報告日
			"from_date"					=>					 $param["hdn_date_y1"]."/".$param["hdn_date_m1"]."/".$param["hdn_date_d1"],
			"to_date"					=>					 $param["hdn_date_y2"]."/".$param["hdn_date_m2"]."/".$param["hdn_date_d2"],
			"from_time"					=>					 null,
			"to_time"					=>					 null,
			"open_place"				=>	pg_escape_string($param["hdn_open_place"			]),		// 開催地
			"teacher_name"				=>	pg_escape_string($param["hdn_teacher_name"			]),		// 講師名
			"trip_division"				=>					 $param["hdn_trip_division"			],		// 出張届け
			"participation_division"	=>					 $param["hdn_participation_division"],		// 参加区分

			"expense_division"			=>					 $updata_expense_division,					// 費用負担

			"report_division"			=>					 $param["hdn_report_division"		]		// 届出区分
		);

		//--------------------------------------------------------------------------
		// 院外研修（計画）申請テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_apl_outside_seminar_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		// 院外研修（計画）申請テーブル更新
		//--------------------------------------------------------------------------
		$log->debug("update開始",__FILE__,__LINE__);
		$model->update($param_edit);
		$log->debug("update終了",__FILE__,__LINE__);


		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}

}



$log->info(basename(__FILE__)." END");
