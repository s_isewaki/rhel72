<?php
	//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");

/*********************************************************************************
 * 申請、承認時の　DBへの各処理   【院内研修変更申請】
 *********************************************************************************/
$log->debug("require START cl_apl_inside_training_apply_model.php",__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/template/cl_apl_inside_training_apply_model.php");
$log->debug("require END cl_apl_inside_training_apply_model.php",__FILE__,__LINE__);

/*********************************************************************************
 * 院内研修申込済テーブルの require
 *********************************************************************************/
$log->debug("require START cl_applied_inside_training_model.php",__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/ladder/cl_applied_inside_training_model.php");
$log->debug("require END cl_applied_inside_training_model.php",__FILE__,__LINE__);

/*********************************************************************************
 * 申請テーブルの require
 *********************************************************************************/
$log->debug("require START cl_apply_model.php",__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/workflow/cl_apply_model.php");
$log->debug("require END cl_apply_model.php",__FILE__,__LINE__);

// 2012/04/21 Yamagawa add(s)
class c110_handler {
// 2012/04/21 Yamagawa add(e)

	/*********************************************************************************
	 * クリニカルラダーコントロールから呼ばれる関数   【院内研修変更申請】
	 *********************************************************************************/
	/**
	 * 【新規申請】【申請】呼び出し
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function NewApl_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info("NewApl_Regist　Start",__FILE__,__LINE__);
		$this->dataApplyAdd(&$mdb2, $user, $param);
	}

	/**
	 * 【新規申請】【下書き保存】呼び出し
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function NewApl_Draft(&$mdb2, $user, $param){
		global $log;
		$log->info("NewApl_Draft　Start",__FILE__,__LINE__);
		$this->dataApplyAdd(&$mdb2, $user, $param);
	}

	/**
	 * 【新規申請】印刷
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function NewApl_Print(&$mdb2, $user, $param){
		global $log;
		$log->info("NewApl_Print　Start",__FILE__,__LINE__);
	}

	/**
	 * 【新規申請】（下書保存から）　申請　呼び出し
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info("DraftApl_Regist　Start",__FILE__,__LINE__);
		$this->dataApplyEdit(&$mdb2, $user, $param);
	}

	/**
	 * 【新規申請】（下書保存から）　下書き保存　呼び出し
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Draft(&$mdb2, $user, $param){
		global $log;
		$log->info("DraftApl_Draft　Start",__FILE__,__LINE__);
		$this->dataApplyEdit(&$mdb2, $user, $param);
	}

	/**
	 * 【新規申請】（下書保存から）　削除　呼び出し
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Delete(&$mdb2, $user, $param){
		global $log;
		$log->info("DraftApl_Delete　Start",__FILE__,__LINE__);
		$this->dataApplyDelete(&$mdb2, $user, $param);
	}

	/**
	 * 【新規申請】（下書き保存から）　印刷
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Print(&$mdb2, $user, $param){
		global $log;
		$log->info("DraftApl_Print　Start",__FILE__,__LINE__);
	}

	/**
	 * 【申請詳細】更新　呼び出し
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_Update(&$mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_Update　Start",__FILE__,__LINE__);
		$this->dataApplyEdit(&$mdb2, $user, $param);
	}

	/**
	 * 【申請詳細】申請取消
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_Cancel(&$mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_Cancel　Start",__FILE__,__LINE__);
		$this->dataApplyDelete(&$mdb2, $user, $param);

	}

	/**
	 * 【申請詳細】申請 再申請
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_ReRegist(&$mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_ReRegist　Start",__FILE__,__LINE__);
		$this->dataApplyAdd(&$mdb2, $user, $param);
	}

	/**
	 * 【申請詳細】　印刷
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_Print(&$mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_Print　Start",__FILE__,__LINE__);
	}

	/**
	 * 【承認詳細】承認 登録
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function ApvDetail_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info("ApvDetail_Regist　Start",__FILE__,__LINE__);
		$this->dataApproveEdit(&$mdb2, $user, $param);
	}

	/**
	 * 【承認詳細】承認 下書き保存
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function ApvDetail_Draft(&$mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_ReRegist　Start",__FILE__,__LINE__);
	}

	/**
	 * 【承認詳細】　印刷
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function ApvDetail_Print(&$mdb2, $user, $param){
		global $log;
		$log->info("ApvDetail_Print　Start",__FILE__,__LINE__);
	}

	function AutoApv_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info("AutoApv_Regist　Start",__FILE__,__LINE__);
	}

	/*********************************************************************************
	 * 上記から呼ばれる関数   【院内研修変更申請】
	 *********************************************************************************/
	/**
	 * 申請時　登録処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApplyAdd($mdb2, $user, $param){
		global $log;
		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_apl_inside_training_apply_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		// 変更後のデータで登録する
		$log->debug("dataApplyAdd　変更後DBデータセットStart",__FILE__,__LINE__);
		$db_param = $this->setDbParam_fromParam(1, $mdb2, $user, $param);
		$log->debug("dataApplyAdd　変更後DBデータセットEnd",__FILE__,__LINE__);

		// 登録処理
		$log->debug("insert開始",__FILE__,__LINE__);
		$model->insert($db_param);
		$log->debug("insert終了",__FILE__,__LINE__);
	}

	/**
	 * 申請時　更新処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApplyEdit($mdb2, $user, $param){
		global $log;

		$log->debug("---------- param ------------------------------",__FILE__,__LINE__);
		$log->debug("training_apply_id = ". $param["training_apply_id"],__FILE__,__LINE__);
		$log->debug("training_id = ". $param["inside_training_id"],__FILE__,__LINE__);
		$log->debug("apply_id = ". $param["apply_id"],__FILE__,__LINE__);
		$log->debug("emp_id = ". $param["emp_id"],__FILE__,__LINE__);
		$log->debug("plan_id = ". $param["plan_id"],__FILE__,__LINE__);
		$log->debug("change_reason = ". $param["change_reason"],__FILE__,__LINE__);

		// 研修データのセット
		$log->debug("dataApplyEdit　DBデータセットStart",__FILE__,__LINE__);
		$db_param = $this->setDbParam_fromParam(0, $mdb2, $user, $param);
		$log->debug("dataApplyEdit　DBデータセットEnd",__FILE__,__LINE__);

		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_apl_inside_training_apply_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		$log->debug("---------- db_param ------------------------------",__FILE__,__LINE__);
		$log->debug("training_apply_id = ". $db_param["training_apply_id"],__FILE__,__LINE__);
		$log->debug("training_id = ". $db_param["inside_training_id"],__FILE__,__LINE__);
		$log->debug("apply_id = ". $db_param["apply_id"],__FILE__,__LINE__);
		$log->debug("emp_id = ". $db_param["emp_id"],__FILE__,__LINE__);
		$log->debug("plan_id = ". $db_param["plan_id"],__FILE__,__LINE__);
		$log->debug("change_reason = ". $db_param["change_reason"],__FILE__,__LINE__);

		// 更新処理
		$log->debug("update開始",__FILE__,__LINE__);
		//2012/05/10 K.Fujii upd(s)
		//$model->update($db_param);
		$model->update_by_apply_id($db_param);
		//2012/05/10 K.Fujii upd(e)
		$log->debug("update終了",__FILE__,__LINE__);
	}

	/**
	 * 申請時　削除処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApplyDelete($mdb2, $user, $param){
		global $log;

		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_apl_inside_training_apply_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		// 削除処理
		$log->debug("delete開始",__FILE__,__LINE__);
		$model->logical_delete($param["training_apply_id"]);
		$log->debug("delete終了",__FILE__,__LINE__);
	}

	/**
	 * 承認時　更新処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApproveEdit($mdb2, $user, $param){
		global $log;

		$log->debug("cl_apply_model インスタンス作成開始",__FILE__,__LINE__);
		$apply_model = new cl_apply_model($mdb2,$user);
		$log->debug("cl_apply_model インスタンス作成終了",__FILE__,__LINE__);

		// 申請データ取得
		$log->debug("申請データ取得開始",__FILE__,__LINE__);
		$apply_data = $apply_model->select($param['apply_id']);
		$log->debug("申請データ取得終了",__FILE__,__LINE__);

		$log->debug("cl_applied_inside_training_model インスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_applied_inside_training_model($mdb2, $user);
		$log->debug("cl_applied_inside_training_model インスタンス作成終了",__FILE__,__LINE__);

		if ($apply_data['apply_stat'] == 1){
			// 研修データのセット
			$log->debug("dataApproveEdit　DBデータセットStart",__FILE__,__LINE__);
			$db_param = $this->setAppliedDbParam_fromParam(0, $mdb2, $user, $param);
			$log->debug("dataApproveEdit　DBデータセットEnd",__FILE__,__LINE__);

			// 更新処理
			//$log->debug("update開始",__FILE__,__LINE__);
			//$model->update($db_param);
			//$log->debug("update終了",__FILE__,__LINE__);

			//このログが正しく出れば、↑のロジックをコメントアウトして、↓の処理を有効にしてください。
			$log->debug("■■■■■実装状況確認：この値が取れれば申請者に承認者が入る問題のテストが可能！  training_apply_id=".$param['training_apply_id'],__FILE__,__LINE__);

			// 更新処理()
			$log->debug("update開始",__FILE__,__LINE__);
			$model->update_from_apply_modify_template($param['training_apply_id'], $user);
			$log->debug("update終了",__FILE__,__LINE__);

			// 出欠テーブル更新
			$param["emp_id"]=$apply_data['emp_id'];
			$this->dataRollEdit($mdb2, $user, $param);
			//dataRollEdit($mdb2, $user, $param,$apply_data['emp_id']);

		}
	}

	/**
	 * パラメータからDBを登録・更新するための配列にセットする
	 * @param int　$iAdd　0:Edit　1:Add
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @return array　$db_param　DBを登録・更新するための配列
	 */
	function setDbParam_fromParam($iAdd, $mdb2, $user, $param){
		global $log;
		$log->debug("setDbParam_fromParam　Start　iAdd = " . $iAdd,__FILE__,__LINE__);
		if($iAdd == 1){
			$log->debug("setDbParam_fromParam　シーケンスCall Start",__FILE__,__LINE__);
			$db_param = array(
				"emp_id"				=> 					$param['emp_id'					],
				"training_apply_id"		=> 					$this->getSequence_forAdd($mdb2, $user, $param),
				"apply_id"				=> 					$param['apply_id'				],
				"charange_level"		=> 					$param['charange_level'			],
				"inside_training_id"	=> 					$param['inside_training_id'		],
				"plan_id"				=> 					$param['plan_id'				],
				"inside_training_date"	=> 					$param['inside_training_date'	],
				"change_apply_date"		=> 					$param['change_apply_date'		],
				"first_apply_id"		=> 					$param['first_apply_id'			],
				"before_apply_id"		=> 					$param['before_apply_id'		],
				"update_division"		=> 					$param['status_division'		],
				"change_reason"			=> pg_escape_string($param['change_reason'			]),
				"learn_memo"			=> pg_escape_string($param['learn_memo'				]),
				"recommend_flag"		=> 					$param['hdn_recommend_flag'		],
				"delete_flg"			=> 0,
				"create_user"			=> $user
				// 2012/11/20 Yamagawa add(s)
				,"year"					=>					$param['year']
				// 2012/11/20 Yamagawa add(e)
			);
		}else{
			$db_param = array(
				"emp_id"				=> 					$param['emp_id'					],
//			"training_apply_id"		=> 					$param['training_apply_id'		],
				"apply_id"				=> 					$param['apply_id'				],
				"charange_level"		=> 					$param['charange_level'			],
				"inside_training_id"	=> 					$param['inside_training_id'		],
				"plan_id"				=> 					$param['plan_id'				],
				"inside_training_date"	=> 					$param['inside_training_date'	],
				"change_apply_date"		=> 					$param['change_apply_date'		],
				"first_apply_id"		=> 					$param['first_apply_id'			],
				"before_apply_id"		=> 					$param['before_apply_id'		],
				"update_division"		=> 					$param['status_division'		],
				"change_reason"			=> pg_escape_string($param['change_reason'			]),
				"learn_memo"			=> pg_escape_string($param['learn_memo'				]),
				"recommend_flag"		=> 					$param['hdn_recommend_flag'		],
				"update_user"			=> 					$user
				// 2012/11/20 Yamagawa add(s)
				,"year"					=>					$param['year']
				// 2012/11/20 Yamagawa add(e)
			);
		}

	//	$log->debug("db_param = " .print_r($db_param),__FILE__,__LINE__);
		return $db_param;
	}

	/**
	 * 院内研修申請IDを取得する
	 * @param unknown_type　$param パラメータ
	 */
	function getSequence_forAdd($mdb2, $user, $param){
		global $log;
		$log->debug("getSequence_forAdd　Sequence require Start",__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/sequence/cl_inside_training_apply_id_seq_model.php");
		$log->debug("getSequence_forAdd　Sequence require End",__FILE__,__LINE__);

		$log->debug("getSequence_forAdd　Sequence インスタンス作成 Start",__FILE__,__LINE__);
		$model = new cl_inside_training_apply_id_seq_model($mdb2, $user);
		$log->debug("getSequence_forAdd　Sequence インスタンス作成 End",__FILE__,__LINE__);

		$log->debug("getSequence_forAdd　Sequence getID Start",__FILE__,__LINE__);
		$training_apply_id= $model->getInsideTrainingApplyId();
		$log->debug("院内研修申請ID：".$training_apply_id,__FILE__,__LINE__);
		$log->debug("getSequence_forAdd　Sequence getID End",__FILE__,__LINE__);
		return $training_apply_id;
	}

	/**
	 * パラメータから申請済みDBを登録・更新するための配列にセットする
	 * @param int　$iAdd　0:Edit　1:Add
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @return array　$db_param　DBを登録・更新するための配列
	 */
	function setAppliedDbParam_fromParam($iAdd, $mdb2, $user, $param){
		global $log;
		$log->debug("setAppliedDbParam_fromParam　Start　iAdd = " . $iAdd,__FILE__,__LINE__);
		if($iAdd == 1){
			$db_param = array(
				"training_apply_id"			=> 					$param["training_apply_id"		],
				"last_training_apply_id"	=> 					$param["training_apply_id"		],
				"emp_id"					=> 					$param['emp_id'					],
				"charange_level"			=> 					$param['charange_level'			],
				"inside_training_id"		=> 					$param['inside_training_id'		],
				"plan_id"					=> 					$param['plan_id'				],
				"inside_training_date"		=> 					$param['inside_training_date'	],
				"change_reason"				=> pg_escape_string($param['change_reason'			]),
				"learn_memo"				=> pg_escape_string($param['learn_memo'				]),
				"recommend_flag"			=> 					$param['hdn_recommend_flag'		],
				"delete_flg"				=> 					0,
				"create_user"				=> 					$user
			);
		}else{
			$del_flg = 0;
			// 変更区分が「キャンセル」のとき
			if($param['status_division'] == "2"){
				$del_flg = 1;
			}
			$db_param = array(
				"training_apply_id"			=> 					$param["first_apply_id"			],
				"last_training_apply_id"	=> 					$param["training_apply_id"		],
				"emp_id"					=> 					$param['emp_id'					],
				"charange_level"			=> 					$param['charange_level'			],
				"inside_training_id"		=> 					$param['inside_training_id'		],
				"plan_id"					=> 					$param['plan_id'				],
				"inside_training_date"		=> 					$param['inside_training_date'	],
				"change_reason"				=> pg_escape_string($param['change_reason'			]),
				"learn_memo"				=> pg_escape_string($param['learn_memo'				]),
				"recommend_flag"			=> 					$param['hdn_recommend_flag'		],
				"delete_flg"				=> 					$del_flg,
				"update_user"				=> 					$user,
			);
		}

	//	$log->debug("db_param = " .print_r($db_param),__FILE__,__LINE__);
		return $db_param;
	}

	//***************************************************************************************************
	//   院内研修出欠関連
	//***************************************************************************************************
	/**
	 * 院内研修出欠更新
	 * @param unknown_type $mdb2
	 * @param string $user login User
	 * @param unknown_type $param
	 */
	function dataRollEdit($mdb2, $user, $param){
	//function dataRollEdit($mdb2, $user, $param,$apply_emp_id){
		global $log;
		$log->debug("*****************************************************",__FILE__,__LINE__);
		$log->debug("　　　出欠更新処理開始　　　　",__FILE__,__LINE__);
		$log->debug("*****************************************************",__FILE__,__LINE__);

		//==================================================
		//使用するパラメータ
		//==================================================
		$log->debug('使用するパラメータ START'	,__FILE__,__LINE__);

		//更新対象の研修申し込みの申請ID
		$target_apply_id = $param["first_apply_id"];

		//更新対象の職員ID
		$target_emp_id = $param["emp_id"];

		//変更前の日程ID(CSV)
		$before_plan_id_csv = $param["before_plan_id"];
		$before_plan_id_array = explode (',', $before_plan_id_csv);

		//変更前の日程ID(CSV)
		$after_plan_id_csv = $param["plan_id"];
		$after_plan_id_array = explode (',', $after_plan_id_csv);

		//処理区分(1:内容変更、2:キャンセル)
		$hdn_status_division = $param["hdn_status_division"];

		$log->debug('使用するパラメータ END'	,__FILE__,__LINE__);

		//==================================================
		//物理削除する日程IDを決定
		//==================================================
		$log->debug('物理削除する日程IDを決定 START'	,__FILE__,__LINE__);

		$delete_target_plan_id_array = "";

		//キャンセルの場合
		if($hdn_status_division == 2)
		{
			//変更前の申請書に紐付く全日程IDが対象
			$delete_target_plan_id_array = $before_plan_id_array;
		}
		//内容変更の場合
		else
		{
			//変更前の日程IDのうち、変更後の日程IDに存在しないものが対象
			for($i = 0; $i < count($before_plan_id_array); $i++)
			{
				if(! in_array($before_plan_id_array[$i],$after_plan_id_array))
				{
					$delete_target_plan_id_array[] = $before_plan_id_array[$i];
				}
			}
		}

		$log->debug(str_replace("\n","", print_r($delete_target_plan_id_array,true)),__FILE__,__LINE__);
		$log->debug('物理削除する日程IDを決定 END'	,__FILE__,__LINE__);

		//==================================================
		//追加する日程IDを決定
		//==================================================
		$log->debug('追加する日程IDを決定 START'	,__FILE__,__LINE__);

		$insert_target_plan_id_array = "";

		//キャンセルの場合
		if($hdn_status_division == 2)
		{
			//追加しない。
			$insert_target_plan_id_array = "";
		}
		//内容変更の場合
		else
		{
			//変更後の日程IDのうち、変更前の日程IDに存在しないものが対象
			for($i = 0; $i < count($after_plan_id_array); $i++)
			{
				if(! in_array($after_plan_id_array[$i],$before_plan_id_array))
				{
					$insert_target_plan_id_array[] = $after_plan_id_array[$i];
				}
			}
		}

		$log->debug(str_replace("\n","", print_r($insert_target_plan_id_array,true)),__FILE__,__LINE__);
		$log->debug('追加する日程IDを決定 END'	,__FILE__,__LINE__);

		//==================================================
		//出欠テーブルの物理削除処理
		//==================================================
		$log->debug('出欠テーブルの物理削除処理 START'	,__FILE__,__LINE__);

		if($delete_target_plan_id_array != "")
		{
			for($i = 0; $i < count($delete_target_plan_id_array); $i++)
			{
				$target_plan_id = $delete_target_plan_id_array[$i];
				$this->deleteInsideTrainingRoll($mdb2, $user,$target_apply_id,$target_emp_id,$target_plan_id);
			}
		}

		$log->debug('出欠テーブルの物理削除処理 END'	,__FILE__,__LINE__);


		//==================================================
		//出欠テーブルの追加処理
		//==================================================
		$log->debug('出欠テーブルの追加処理 START'	,__FILE__,__LINE__);

		if($insert_target_plan_id_array != "")
		{
			for($i = 0; $i < count($insert_target_plan_id_array); $i++)
			{
				$target_plan_id = $insert_target_plan_id_array[$i];
				$this->insertInsideTrainingRoll($mdb2, $user,$target_apply_id,$target_emp_id,$target_plan_id);
			}
		}

		$log->debug('出欠テーブルの追加処理 END'	,__FILE__,__LINE__);


		$log->debug("*****************************************************",__FILE__,__LINE__);
		$log->debug("　　　出欠更新処理終了　　　　",__FILE__,__LINE__);
		$log->debug("*****************************************************",__FILE__,__LINE__);
	}



	/**
	 * 出欠テーブルに対する物理削除処理を行います。
	 *
	 * @param unknown_type $mdb2
	 * @param unknown_type $user
	 * @param unknown_type $apply_id
	 * @param unknown_type $emp_id
	 * @param unknown_type $plan_id
	 */
	function deleteInsideTrainingRoll($mdb2, $user,$apply_id,$emp_id,$plan_id)
	{
		global $log;
		$log->debug('deleteInsideTrainingRoll($mdb2, $user,$apply_id,$emp_id,$plan_id) START'	,__FILE__,__LINE__);

		//==================================================
		//ＤＢ処理用データモデルをロード
		//==================================================
		$log->debug('load cl_inside_training_roll_model.php START'	,__FILE__,__LINE__);

		require_once(dirname(__FILE__) . "/../model/workflow/cl_inside_training_roll_model.php");
		$model = new cl_inside_training_roll_model($mdb2, $user);

		$log->debug('load cl_inside_training_roll_model.php END'	,__FILE__,__LINE__);

		//==================================================
		//物理削除実行
		//==================================================

		// 物理削除処理
		$log->debug("物理削除実行 START",__FILE__,__LINE__);
		$log->debug("apply_id = ".$apply_id." plan_id=".$plan_id,__FILE__,__LINE__);
		$model->physical_delete_from_plan_id($apply_id,$plan_id);
		$log->debug("物理削除実行 END",__FILE__,__LINE__);

		$log->debug('deleteInsideTrainingRoll($mdb2, $user,$apply_id,$emp_id,$plan_id) END'	,__FILE__,__LINE__);
	}


	/**
	 * 出欠テーブルに対する追加処理を行います。
	 * Enter description here ...
	 * @param unknown_type $mdb2
	 * @param unknown_type $user
	 * @param unknown_type $apply_id
	 * @param unknown_type $emp_id
	 * @param unknown_type $plan_id
	 */
	function insertInsideTrainingRoll($mdb2, $user,$apply_id,$emp_id,$plan_id)
	{
		global $log;
		$log->debug('insertInsideTrainingRoll($mdb2, $user,$apply_id,$emp_id,$plan_id) START'	,__FILE__,__LINE__);

		//==================================================
		//ＤＢ処理用データモデルをロード
		//==================================================
		$log->debug('load cl_inside_training_roll_model.php START'	,__FILE__,__LINE__);

		require_once(dirname(__FILE__) . "/../model/workflow/cl_inside_training_roll_model.php");
		$model = new cl_inside_training_roll_model($mdb2, $user);

		$log->debug('load cl_inside_training_roll_model.php END'	,__FILE__,__LINE__);

		//==================================================
		//日程テーブルより日程情報を取得
		//==================================================
		$log->debug('日程テーブルより日程情報を取得 START'	,__FILE__,__LINE__);

		$schedule_info = $this->getScheduleInfo($mdb2, $plan_id);

		$log->debug('日程テーブルより日程情報を取得 END'	,__FILE__,__LINE__);

		//==================================================
		//出欠IDを採番
		//==================================================
		$log->debug('出欠IDを採番 START'	,__FILE__,__LINE__);

		$training_roll_id = $this->getInsideTrainingRollID($mdb2, $user);

		$log->debug('出欠IDを採番 END'	,__FILE__,__LINE__);

		//==================================================
		//ＤＢインサート
		//==================================================
		$log->debug('ＤＢインサート START'	,__FILE__,__LINE__);

		$db_param = array(
			"training_roll_id"		=> $training_roll_id,
			"plan_id"				=> $plan_id,
			"training_id"			=> $schedule_info['training_id'],
			"plan_no"				=> $schedule_info['plan_no'],
			"emp_id"				=> $emp_id,
			"apply_id"				=> $apply_id,
			"emp_roll"				=> 0,
			"delete_flg"			=> 0,
			"create_user"			=> $user
			);

		$log->debug(str_replace("\n","", print_r($db_param,true)),__FILE__,__LINE__);
		$model->insert($db_param);
		$log->debug('ＤＢインサート END'	,__FILE__,__LINE__);

		$log->debug('insertInsideTrainingRoll($mdb2, $user,$apply_id,$emp_id,$plan_id) END'	,__FILE__,__LINE__);
	}



	/**
	 * 院内研修出欠シーケンスID取得
	 */
	function getInsideTrainingRollID(&$mdb2, $user){
		global $log;
		$log->debug("getInsideTrainingRollID　Sequence require Start",__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/sequence/cl_inside_training_roll_id_seq_model.php");
		$log->debug("getInsideTrainingRollID　Sequence require End",__FILE__,__LINE__);

		$log->debug("getInsideTrainingRollID　インスタンス作成　Start",__FILE__,__LINE__);
		$model = new cl_inside_training_roll_id_seq_model($mdb2, $user);
		$log->debug("getInsideTrainingRollID　インスタンス作成　End",__FILE__,__LINE__);

		$log->debug("getInsideTrainingRollID　getID　Start",__FILE__,__LINE__);
		$training_roll_id= $model->getInsideTrainingRollID();
		$log->debug("getInsideTrainingRollID　getID　End    roll_id = ". $training_roll_id,__FILE__,__LINE__);
		return $training_roll_id;
	}



	/**
	 * 日程情報の検索
	 * @param string $plan_id
	 */
	function getScheduleInfo(&$mdb2, $plan_id){
		global $log;
		$log->debug('getScheduleInfo Require Start'	,__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_schedule_model.php");
		$log->debug('getScheduleInfo Require End'	,__FILE__,__LINE__);

		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_mst_inside_training_schedule_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		// 検索処理
		$log->debug("select開始　　plan_id = ". $plan_id,__FILE__,__LINE__);
		$sel_plan = $model->select($plan_id);
		$log->debug("select終了",__FILE__,__LINE__);
		return $sel_plan;
	}
// 2012/04/21 Yamagawa add(s)
}
// 2012/04/21 Yamagawa add(e)

?>
