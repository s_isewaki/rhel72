<?php
	//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");



/*********************************************************************************
 * 申請、承認時の　DBへの各処理   【同僚評価表 2】
 *********************************************************************************/
//model呼び出し
$log->debug("require START cl_apl_levelup_evaluation_value_model.php",__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/template/cl_apl_levelup_evaluation_value_model.php");
$log->debug("require END cl_apl_levelup_evaluation_value_model.php",__FILE__,__LINE__);

$log->debug("cl_levelup_evaluation_colleague_model Start",__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/search/cl_levelup_evaluation_colleague_model.php");
$log->debug("cl_levelup_evaluation_colleague_model End",__FILE__,__LINE__);

$log->debug("require START cl_apl_levelup_evaluation_colleague_model.php",__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/template/cl_apl_levelup_evaluation_colleague_model.php");
$log->debug("require END cl_apl_levelup_evaluation_colleague_model.php",__FILE__,__LINE__);

class c360_handler {

	/*********************************************************************************
	 * クリニカルラダーコントロールから呼ばれる関数   【レベル2同僚評価表】
	 *********************************************************************************/
	//******************************************************************************
	// 「新規申請」画面用ハンドラー
	//******************************************************************************
	/**
	 * 【新規申請】【申請】呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function NewApl_Regist($mdb2, $user, $param){
		global $log;
		$log->info("NewApl_Regist　Start",__FILE__,__LINE__);
		$this->dataApplyAdd($mdb2, $user, $param);
		$log->info("NewApl_Regist　End",__FILE__,__LINE__);
	}

	/**
	 * 【新規申請】【下書き保存】呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */

	function NewApl_Draft($mdb2, $user, $param){
		global $log;

	}

	/**
	 * 【新規申請】印刷
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function NewApl_Print($mdb2, $user, $param){
		global $log;
	}

	//******************************************************************************
	// 「新規申請（下書保存から）」画面用ハンドラー
	//******************************************************************************
	/**
	 * 【新規申請】（下書保存から）　申請　呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Regist($mdb2, $user, $param){
		global $log;

	}

	/**
	 * 【新規申請】（下書保存から）　下書き保存　呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Draft($mdb2, $user, $param){
		global $log;

	}

	/**
	 * 【新規申請】（下書保存から）　削除　呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Delete($mdb2, $user, $param){
		global $log;

	}

	/**
	 * 【新規申請】（下書き保存から）　印刷
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Print($mdb2, $user, $param){
		global $log;
	}

	//******************************************************************************
	// 「申請詳細」画面用ハンドラー
	//******************************************************************************
	/**
	 * 【申請詳細】更新　呼び出し
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_Update($mdb2, $user, $param){
		global $log;
		$log->info("dataApplyEdit　Start",__FILE__,__LINE__);
		//2012/05/28 K.Fujii del(s)
		//$this->dataApplyEdit($mdb2, $user, $param, "AplEdit");
		//2012/05/28 K.Fujii del(e)
		$log->info("dataApplyEdit　End",__FILE__,__LINE__);
	}

	/**
	 * 【申請詳細】申請取消
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_Cancel($mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_Cancel　Start",__FILE__,__LINE__);
		$this->dataApplyDelete($mdb2, $user, $param);
		$log->info("AplDetail_Cancel　End",__FILE__,__LINE__);
	}

	/**
	 * 【申請詳細】申請 再申請
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_ReRegist($mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_ReRegist　Start",__FILE__,__LINE__);
		//2012/05/28 K.Fujii del(s)
		//$this->dataApplyAdd($mdb2, $user, $param);
		//2012/05/28 K.Fujii del(e)
		$log->info("AplDetail_ReRegist　End",__FILE__,__LINE__);
	}

	/**
	 * 【申請詳細】　印刷
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_Print($mdb2, $user, $param){
		global $log;
	}

	//******************************************************************************
	// 「承認詳細」画面用ハンドラー
	//******************************************************************************
	/**
	 * 【承認詳細】承認 登録
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function ApvDetail_Regist($mdb2, $user, $param){
		global $log;
		$log->info("ApvDetail_Regist　Start",__FILE__,__LINE__);
		$this->dataApproveAdd($mdb2, $user, $param);
		$log->info("ApvDetail_Regist　End",__FILE__,__LINE__);
	}

	/**
	 * 【承認詳細】承認 下書き保存
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function ApvDetail_Draft($mdb2, $user, $param){
		global $log;
		$log->info("ApvDetail_Regist　Start",__FILE__,__LINE__);
		$this->dataApplyEdit($mdb2, $user, $param, "ApvEdit");
		$log->info("ApvDetail_Regist　End",__FILE__,__LINE__);
	}

	/**
	 * 【承認詳細】　印刷
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function ApvDetail_Print($mdb2, $user, $param){
		global $log;
	}

	/**
	 * 【自動承認】　
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AutoApv_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info(__FUNCTION__." START",__FILE__,__LINE__);

		$log->info(__FUNCTION__." END",__FILE__,__LINE__);
	}


	/**
	 * 申請時　登録処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApplyAdd($mdb2, $user, $param){
		global $log;

		$iAdd = 1; // 登録

		//2012/05/28 K.Fujii del(s)
		////新規levelup_evaluation_colleague_id採番
		//$levelup_evaluation_colleague_id = $this->getSequence_forAddMain($mdb2, $user);
		//2012/05/28 K.Fujii del(e)

		//2012/05/28 K.Fujii del(s)
		////--------------------------------------------------------------------------
		////  レベルアップ申請データ取得
		////--------------------------------------------------------------------------
		//$log->debug("cl_levelup_evaluation_colleague_modelインスタンス作成開始",__FILE__,__LINE__);
		//$cl_levelup_evaluation_colleague_model = new cl_levelup_evaluation_colleague_model($mdb2, $user);
		//$log->debug("cl_levelup_evaluation_colleague_modelインスタンス作成終了",__FILE__,__LINE__);
		//
		//$levelup_apply_data = $cl_levelup_evaluation_colleague_model->getLevelup_Apply_List_byLevelupApplyId($param["levelup_apply_id"]);
		//2012/05/28 K.Fujii del(e)

		//--------------------------------------------------------------------------
		//  レベルアップ同僚評価表テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("cl_apl_levelup_evaluation_colleague_modelインスタンス作成開始",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_colleague_model = new cl_apl_levelup_evaluation_colleague_model($mdb2, $user);
		$log->debug("cl_apl_levelup_evaluation_colleague_modelインスタンス作成終了",__FILE__,__LINE__);

		//2012/05/28 K.Fujii ins(s)
		//--------------------------------------------------------------------------
		//  レベルアップ評価点テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("cl_apl_levelup_evaluation_value_modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_apl_levelup_evaluation_value_model($mdb2, $user);
		$log->debug("cl_apl_levelup_evaluation_value_modelインスタンス作成終了",__FILE__,__LINE__);
		//2012/05/28 K.Fujii ins(e)

		//--------------------------------------------------------------------------
		//  レベルアップ同僚評価表登録データのセット
		//--------------------------------------------------------------------------
		//パラメータからIDを取得
		$levelup_evaluation_colleague_id = $param["levelup_evaluation_id"];
		$log->debug("setDbParam_froEvaluation　DBデータセットStart",__FILE__,__LINE__);
		//2012/05/28 K.Fujii upd(s)
		//$db_param_main = $this->setDbParam_froEvaluation($iAdd, $mdb2, $user, $param, $levelup_apply_data[0], $levelup_evaluation_colleague_id, $mode);
		$db_param_main = $this->setDbParam_froEvaluation($iAdd, $mdb2, $user, $param, $levelup_evaluation_colleague_id);
		//2012/05/28 K.Fujii upd(e)
		$log->debug("setDbParam_froEvaluation　DBデータセットEnd",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		//  レベルアップ同僚評価表登録
		//--------------------------------------------------------------------------
		$log->debug("レベルアップ同僚評価表 登録 Start",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_colleague_model->insert($db_param_main);
		$log->debug("レベルアップ同僚評価表 登録 End",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		//  評価マスタテーブルデータ取得
		//--------------------------------------------------------------------------
		$log->debug("cl_levelup_evaluation_colleague_modelインスタンス作成開始",__FILE__,__LINE__);
		$cl_levelup_evaluation_colleague_model = new cl_levelup_evaluation_colleague_model($mdb2, $user);
		$log->debug("cl_levelup_evaluation_colleague_modelインスタンス作成終了",__FILE__,__LINE__);
		$log->debug("getEvaluationItemList_byLevelupApplyId開始 levelup_apply_id = ". $param['levelup_apply_id'],__FILE__,__LINE__);
		$evaluation_item_list=$cl_levelup_evaluation_colleague_model->getEvaluationItemList_byLevelupApplyId($param['levelup_apply_id']);
		$log->debug("getEvaluationItemList_byLevelupApplyId終了 levelup_apply_id = ". $param['levelup_apply_id'],__FILE__,__LINE__);

		//2012/05/28 K.Fujii del(s)
		////--------------------------------------------------------------------------
		////  レベルアップ評価点テーブルモデルインスタンス生成
		////--------------------------------------------------------------------------
		//$log->debug("cl_apl_levelup_evaluation_value_modelインスタンス作成開始",__FILE__,__LINE__);
		//$model = new cl_apl_levelup_evaluation_value_model($mdb2, $user);
		//$log->debug("cl_apl_levelup_evaluation_value_modelインスタンス作成終了",__FILE__,__LINE__);
		//2012/05/28 K.Fujii del(e)

		if($param["colleague_division"] == "1"){
			$evaluation_emp_division = "3";
		}else{
			$evaluation_emp_division = "4";
		}

		for($i = 0; $i < count($evaluation_item_list); $i++){

			//--------------------------------------------------------------------------
			//  データのセット
			//--------------------------------------------------------------------------
			$log->debug("dataApplyAdd　DBデータセットStart",__FILE__,__LINE__);
			$db_param = $this->setDbParam_fromParam($iAdd, $mdb2, $user, $param, $evaluation_item_list[$i], $evaluation_emp_division, $i, $levelup_evaluation_colleague_id);
			$log->debug("dataApplyAdd　DBデータセットEnd",__FILE__,__LINE__);

			//--------------------------------------------------------------------------
			//  レベルアップ評価点登録
			//--------------------------------------------------------------------------
			$log->debug("レベルアップ評価点 登録 Start",__FILE__,__LINE__);
			$model->insert($db_param);
			$log->debug("レベルアップ評価点 登録 End",__FILE__,__LINE__);

		}

	}

	/**
	 * 申請時　更新処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApplyEdit($mdb2, $user, $param, $kbn){
		global $log;

		$levelup_evaluation_colleague_id = $param["levelup_evaluation_id"];

		$iAdd = 0; //更新
		//--------------------------------------------------------------------------
		//  レベルアップ同僚評価表登録データのセット
		//--------------------------------------------------------------------------
		$log->debug("setDbParam_froEvaluation　DBデータセットStart",__FILE__,__LINE__);
		$db_param_main = $this->setDbParam_froEvaluation($iAdd, $mdb2, $user, $param, $levelup_evaluation_colleague_id);
		$log->debug("setDbParam_froEvaluation　DBデータセットEnd",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		//  レベルアップ同僚評価表テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("cl_apl_levelup_evaluation_colleague_modelインスタンス作成開始",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_colleague_model = new cl_apl_levelup_evaluation_colleague_model($mdb2, $user);
		$log->debug("cl_apl_levelup_evaluation_colleague_modelインスタンス作成終了",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		//  レベルアップ同僚評価表更新
		//--------------------------------------------------------------------------
		$log->debug("レベルアップ同僚評価表 更新 Start",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_colleague_model->update($db_param_main);
		$log->debug("レベルアップ同僚評価表 更新 End",__FILE__,__LINE__);


		//--------------------------------------------------------------------------
		//  テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_value_model = new cl_apl_levelup_evaluation_value_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		$iAdd = 0;
		if($param["colleague_division"] == "1"){
			$evaluation_emp_division = "3";
		}else{
			$evaluation_emp_division = "4";
		}

		for($i = 0; $i < (int)$param["item_cnt"]; $i++){

			// データのセット
			$log->debug("setDbParam_fromParam　DBデータセットStart",__FILE__,__LINE__);
			//2012/05/28 K.Fujii upd(s)
			//$db_param = $this->setDbParam_fromParam($iAdd, $mdb2, $user, $param, $evaluation_item_list[$i], $evaluation_emp_division, $i, $levelup_evaluation_colleague_id);
			$db_param = $this->setDbParam_fromParam($iAdd, $mdb2, $user, $param, array(), $evaluation_emp_division, $i, $levelup_evaluation_colleague_id);
			//2012/05/28 K.Fujii upd(e)
			$log->debug("setDbParam_fromParam　DBデータセットEnd",__FILE__,__LINE__);


			// 更新処理
			$log->debug("update開始",__FILE__,__LINE__);
			$cl_apl_levelup_evaluation_value_model->update($db_param);
			$log->debug("update終了",__FILE__,__LINE__);
		}

	}

	/**
	 * 申請時　論理削除処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApplyDelete($mdb2, $user, $param){
		global $log;

		$levelup_evaluation_colleague_id = $param["levelup_evaluation_id"];

		//--------------------------------------------------------------------------
		//評価表データの削除（論理削除）
		//--------------------------------------------------------------------------
		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_colleague_model = new cl_apl_levelup_evaluation_colleague_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		$log->debug("delete開始　levelup_evaluation_colleague_id:".$levelup_evaluation_colleague_id,__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_colleague_model->logical_delete($levelup_evaluation_colleague_id);
		$log->debug("delete開始　levelup_evaluation_colleague_id:".$levelup_evaluation_colleague_id,__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		//  テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_value_model = new cl_apl_levelup_evaluation_value_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		//評価点データの削除
		for($i = 0;$i<(int)$param["item_cnt"];$i++){
			// 論理削除処理
			$log->debug("delete開始　levelup_evaluation_value_id:".$evaluation_item_list[$i]["levelup_evaluation_value_id"],__FILE__,__LINE__);
			$cl_apl_levelup_evaluation_value_model->logical_delete($evaluation_item_list[$i]["levelup_evaluation_value_id"]);
			$log->debug("delete終了",__FILE__,__LINE__);
		}
	}

	/**
	 * 承認時　登録処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApproveAdd($mdb2, $user, $param){
		global $log;

		$levelup_evaluation_colleague_id = $param["levelup_evaluation_id"];

		if($levelup_evaluation_colleague_id != ""){

		$iAdd = 0; //更新
		//--------------------------------------------------------------------------
		//  レベルアップ同僚評価表登録データのセット
		//--------------------------------------------------------------------------
		$log->debug("setDbParam_froEvaluation　DBデータセットStart",__FILE__,__LINE__);
			$db_param_main = $this->setDbParam_froEvaluation($iAdd, $mdb2, $user, $param, $levelup_evaluation_colleague_id);
		$log->debug("setDbParam_froEvaluation　DBデータセットEnd",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		//  レベルアップ同僚評価表テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("cl_apl_levelup_evaluation_colleague_modelインスタンス作成開始",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_colleague_model = new cl_apl_levelup_evaluation_colleague_model($mdb2, $user);
		$log->debug("cl_apl_levelup_evaluation_colleague_modelインスタンス作成終了",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		//  レベルアップ同僚評価表更新
		//--------------------------------------------------------------------------
		$log->debug("レベルアップ同僚評価表 更新 Start",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_colleague_model->update($db_param_main);
		$log->debug("レベルアップ同僚評価表 更新 End",__FILE__,__LINE__);

		//--------------------------------------------------------------------------
		//  テーブルモデルインスタンス生成
		//--------------------------------------------------------------------------
		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$cl_apl_levelup_evaluation_value_model = new cl_apl_levelup_evaluation_value_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		$iAdd = 0;
			if($param["colleague_division"] == "1"){
			$evaluation_emp_division = "3";
		}else{
			$evaluation_emp_division = "4";
		}

			for($i = 0; $i < (int)$param["item_cnt"]; $i++){

			// データのセット
			$log->debug("dataApplyEdit　DBデータセットStart",__FILE__,__LINE__);
				//2012/05/28 K.Fujii upd(s)
				//$db_param = $this->setDbParam_fromParam($iAdd, $mdb2, $user, $param, $evaluation_item_list[$i], $evaluation_emp_division, $i, $levelup_evaluation_colleague_id);
				$db_param = $this->setDbParam_fromParam($iAdd, $mdb2, $user, $param, array(), $evaluation_emp_division, $i, $levelup_evaluation_colleague_id);
				//2012/05/28 K.Fujii upd(e)
			$log->debug("dataApplyEdit　DBデータセットEnd",__FILE__,__LINE__);


			// 更新処理
			$log->debug("update開始",__FILE__,__LINE__);
			$cl_apl_levelup_evaluation_value_model->update($db_param);
			$log->debug("update終了",__FILE__,__LINE__);
		}

//2012/07/31 同僚評価の第二階層廃止
//		//2012/05/22 K.Fujii ins(s)
//		//--------------------------------------------------------------------------
//		//自動承認管理テーブルデータ作成（処理区分 0：未処理）
//		//--------------------------------------------------------------------------
//		//同僚評価承認者管理テーブルデータ検索
//		$log->debug("require START cl_ldr_cwrkr_asmnt_select_model");
//		require_once(dirname(__FILE__) . "/../model/search/cl_ldr_cwrkr_asmnt_select_model.php");
//		$log->debug("require END cl_ldr_cwrkr_asmnt_select_model");
//		$log->debug("cl_ldr_cwrkr_asmnt_select_model インスタンス作成開始",__FILE__,__LINE__);
//		$cl_ldr_cwrkr_asmnt_select_model = new cl_ldr_cwrkr_asmnt_select_model($mdb2,$user);
//		$log->debug("cl_ldr_cwrkr_asmnt_select_model インスタンス作成終了",__FILE__,__LINE__);
//			$log->debug("getAcknowledgerByApplyId Start apply_id = ".$param["apply_id"],__FILE__,__LINE__);
//			$acknowledger_manage = $cl_ldr_cwrkr_asmnt_select_model->getAcknowledgerByApplyId($param["apply_id"]);
//			$log->debug("getAcknowledgerByApplyId End",__FILE__,__LINE__);
//
//		$log->debug("require START cl_ldr_auto_approve_manage_model");
//		require_once(dirname(__FILE__) . "/../model/ladder/cl_ldr_auto_approve_manage_model.php");
//		$log->debug("require END cl_ldr_auto_approve_manage_model");
//		$log->debug("cl_ldr_auto_approve_manage_model インスタンス作成開始",__FILE__,__LINE__);
//		$cl_ldr_auto_approve_manage_model = new cl_ldr_auto_approve_manage_model($mdb2,$user);
//		$log->debug("cl_ldr_auto_approve_manage_model インスタンス作成終了",__FILE__,__LINE__);
//
//		for($i=0;$i<count($acknowledger_manage);$i++){
//			//ID採番
//				$auto_approve_manage_id = $this->getAutoApproveSequence_forAdd($mdb2, $user);
//			//パラメータ作成
//			$param = $this->setDbParam_forAutoApproveManage($auto_approve_manage_id, $acknowledger_manage[$i], $param);
//			//データ登録
//			$cl_ldr_auto_approve_manage_model->insert($param);
//		}
//		//2012/05/22 K.Fujii ins(e)
	}
	}

	/********************************************************************************
	 * パラメータからDBを登録・更新するための配列にセットする
	 * @param int　$iAdd　0:Edit　1:Add
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @return array　$db_param_main　DBを登録・更新するための配列
	 */
	function setDbParam_froEvaluation($iAdd, $mdb2, $user, $param, $levelup_evaluation_colleague_id){
		global $log;

		$log->debug("setDbParam_froEvaluation　Start　iAdd = " . $iAdd,__FILE__,__LINE__);
		if($iAdd == 1){
			//2012/05/28 K.Fujii del(s)
			//if($param["colleague_division"] == "1"){
			//	//主任評価者
			//	$db_param_main = array(
			//		"levelup_evaluation_colleague_id"		=> $levelup_evaluation_colleague_id,
			//		"apply_id"								=> $param["apply_id"],
			//		"levelup_apply_id"						=> $param["levelup_apply_id"],
			//		"levelup_emp_id"						=> $data["levelup_emp_id"],
			//		"level"									=> $data["level"],
			//		"colleague_division"					=> $param["colleague_division"],
			//		"colleague_emp_id"						=> $param["hdn_apply_chief_id"],
			//		"colleague_evaluation_date"				=> null
			//	);
			//}else{
			//	//同僚評価者
			//	$db_param_main = array(
			//		"levelup_evaluation_colleague_id"		=> $levelup_evaluation_colleague_id,
			//		"apply_id"								=> $param["apply_id"],
			//		"levelup_apply_id"						=> $param["levelup_apply_id"],
			//		"levelup_emp_id"						=> $data["levelup_emp_id"],
			//		"level"									=> $data["level"],
			//		"colleague_division"					=> $param["colleague_division"],
			//		"colleague_emp_id"						=> $param["hdn_apply_general_id"],
			//		"colleague_evaluation_date"				=> null
			//	);
			//}
			//2012/05/28 K.Fujii del(e)
			//2012/05/28 K.Fujii ins(s)
			//登録パラメータ
				$db_param_main = array(
					"levelup_evaluation_colleague_id"		=> $levelup_evaluation_colleague_id,
					"apply_id"								=> $param["apply_id"],
					"levelup_apply_id"						=> $param["levelup_apply_id"],
				"levelup_emp_id"						=> $param["cnddt_emp_id"],
				"level"									=> $param["level"],
					"colleague_division"					=> $param["colleague_division"],
				"colleague_emp_id"						=> $param["cwrkr_emp_id"],
					"colleague_evaluation_date"				=> null
				);
			//2012/05/28 K.Fujii ins(e)
			}else{
			//更新
			//承認詳細更新
				$db_param_main = array(
				"levelup_evaluation_colleague_id"		=> $param["levelup_evaluation_id"],
					"apply_id"								=> $param["apply_id"],
					"levelup_apply_id"						=> $param["levelup_apply_id"],
				"levelup_emp_id"						=> $param["levelup_emp_id"],
				"level"									=> $param["level"],
					"colleague_division"					=> $param["colleague_division"],
				"colleague_emp_id"						=> $user,
				"colleague_evaluation_date"				=> date("Y-m-d")
			);
		}
		$log->debug("setDbParam_froEvaluation　End　iAdd = " . $iAdd,__FILE__,__LINE__);
		return $db_param_main;
	}

	/********************************************************************************
	 * パラメータからDBを登録・更新するための配列にセットする
	 * @param int　$iAdd　0:Edit　1:Add
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @return array　$db_param　DBを登録・更新するための配列
	 */
	//2012/05/28 K.Fujii upd(s)
	//function setDbParam_fromParam($iAdd, $mdb2, $user, $param, $data, $evaluation_emp_division, $cnt_point, $levelup_evaluation_colleague_id){]
	function setDbParam_fromParam($iAdd, $mdb2, $user, $param, $evaluation_value_data, $evaluation_emp_division, $cnt_point, $levelup_evaluation_colleague_id){
		//2012/05/28 K.Fujii upd(e)
		global $log;

		$log->debug("setDbParam_fromParam　Start　iAdd = " . $iAdd,__FILE__,__LINE__);
		if($iAdd == 1){
			//2012/05/28 K.Fujii del(s)
			//if($evaluation_emp_division == "3"){
			//	$db_param = array(
			//		"levelup_evaluation_value_id"		=> $this->getSequence_forAdd($mdb2, $user),
			//		"levelup_evaluation_id"				=> null,
			//		"levelup_evaluation_colleague_id"	=> $levelup_evaluation_colleague_id,
			//		"levelup_apply_id"					=> $param["levelup_apply_id"],
			//		"levelup_emp_id"					=> $data["levelup_emp_id"],
			//		"evaluation_emp_id"					=> $param["hdn_apply_chief_id"],
			//		"evaluation_id"						=> $data["evaluation_id"],
			//		"category_id"						=> $data["category_id"],
			//		"group_id"							=> $data["group_id"],
			//		"item_id"							=> $data["item_id"],
			//		"value"								=> null,
			//		"evaluation_emp_division"			=> $evaluation_emp_division
			//	);
			//}else{
			//	$db_param = array(
			//		"levelup_evaluation_value_id"		=> $this->getSequence_forAdd($mdb2, $user),
			//		"levelup_evaluation_id"				=> null,
			//		"levelup_evaluation_colleague_id"	=> $levelup_evaluation_colleague_id,
			//		"levelup_apply_id"					=> $param["levelup_apply_id"],
			//		"levelup_emp_id"					=> $data["levelup_emp_id"],
			//		"evaluation_emp_id"					=> $param["hdn_apply_general_id"],
			//		"evaluation_id"						=> $data["evaluation_id"],
			//		"category_id"						=> $data["category_id"],
			//		"group_id"							=> $data["group_id"],
			//		"item_id"							=> $data["item_id"],
			//		"value"								=> null,
			//		"evaluation_emp_division"			=> $evaluation_emp_division
			//	);
			//}
			//2012/05/28 K.Fujii del(e)
			//2012/05/28 K.Fujii ins(s)
			//登録時
				$db_param = array(
					"levelup_evaluation_value_id"		=> $this->getSequence_forAdd($mdb2, $user),
					"levelup_evaluation_id"				=> null,
					"levelup_evaluation_colleague_id"	=> $levelup_evaluation_colleague_id,
					"levelup_apply_id"					=> $param["levelup_apply_id"],
					"levelup_emp_id"					=> $param["cnddt_emp_id"],
					"evaluation_emp_id"					=> $param["cwrkr_emp_id"],
					"evaluation_id"						=> $evaluation_value_data["evaluation_id"],
					"category_id"						=> $evaluation_value_data["category_id"],
					"group_id"							=> $evaluation_value_data["group_id"],
					"item_id"							=> $evaluation_value_data["item_id"],
					"value"								=> null,
					"evaluation_emp_division"			=> $evaluation_emp_division
				);
			//2012/05/28 K.Fujii ins(e)
			}else{
			//更新時
				$db_param = array(
				"evaluation_emp_division"			=> $evaluation_emp_division,
					"levelup_evaluation_id"				=> null,
				"levelup_evaluation_colleague_id"	=> $param["levelup_evaluation_id"],
					"levelup_apply_id"					=> $param["levelup_apply_id"],
				"levelup_emp_id"					=> $param["levelup_emp_id"],
				"evaluation_emp_id"					=> $user,
				"evaluation_id"						=> $param["evaluation_id_".$cnt_point],
				"category_id"						=> $param["category_id_".$cnt_point],
				"group_id"							=> $param["group_idv_".$cnt_point],
				"item_id"							=> $param["item_id_".$cnt_point],
				"value"								=> $param["cmb_eval_value1_".$cnt_point],
				"levelup_evaluation_value_id"		=> $param["levelup_evaluation_value_id1_".$cnt_point]
			);
		}

		$log->debug("setDbParam_fromParam　End　iAdd = " . $iAdd,__FILE__,__LINE__);
		return $db_param;
	}

	//2012/05/22 K.Fujii ins(s)
	//自動承認管理テーブル登録用パラメータ作成
	function setDbParam_forAutoApproveManage($auto_approve_manage_id, $data, $param){
		global $log;

		$log->debug("setDbParam_forAutoApproveManage　Start",__FILE__,__LINE__);
		$db_param = array(
			"auto_approve_manage_id"		=> $auto_approve_manage_id,
			"apply_id"						=> $param["apply_id"],
			"short_wkfw_name"				=> "c360",
			"level"							=> "2",
			"apv_order"						=> $data["apv_order"],
			"apv_sub_order"					=> $data["apv_sub_order"],
			"last_approve_flg"				=> "1",
			"proc_flg"						=> "0"
		);

		$log->debug("setDbParam_forAutoApproveManage　End",__FILE__,__LINE__);
		return $db_param;
	}
	//2012/05/22 K.Fujii ins(e)

	/**
	 * レベルアップ評価点のシーケンスを取得する
	 * @param unknown_type　$param パラメータ
	 */
	//function getSequence_forAdd($mdb2, $user, $param){
	function getSequence_forAdd($mdb2, $user){
		global $log;
		$log->debug("getSequence_forAdd　Sequence require Start",__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/sequence/cl_levelup_evaluation_value_id_seq_model.php");
		$log->debug("getSequence_forAdd　Sequence require End",__FILE__,__LINE__);

		$log->debug("getSequence_forAdd　Sequence インスタンス作成 Start",__FILE__,__LINE__);
		$model = new cl_levelup_evaluation_value_id_seq_model($mdb2, $user);
		$log->debug("getSequence_forAdd　Sequence インスタンス作成 End",__FILE__,__LINE__);

		$log->debug("getSequence_forAdd　Sequence getID Start",__FILE__,__LINE__);
		$levelup_evaluation_value_id= $model->getSeqId();
		$log->debug("レベルアップ評価点ID：".$levelup_evaluation_value_id,__FILE__,__LINE__);
		$log->debug("getSequence_forAdd　Sequence getID End",__FILE__,__LINE__);

		return $levelup_evaluation_value_id;

	}

	//2012/05/22 K.Fujii ins(s)
	/**
	 * 自動承認管理IDを取得する
	 * @param unknown_type　$param パラメータ
	 */
//	function getAutoApproveSequence_forAdd($mdb2, $user, $param){
	function getAutoApproveSequence_forAdd($mdb2, $user){
		global $log;
		require_once(dirname(__FILE__) . "/../model/sequence/cl_auto_approve_manage_seq_model.php");
		$model = new cl_auto_approve_manage_seq_model($mdb2, $user);
		$auto_approve_manage_id= $model->getSeqId();
		return $auto_approve_manage_id;
	}
	//2012/05/22 K.Fujii ins(e)

}

?>