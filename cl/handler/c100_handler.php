<?php
	//ini_set( 'display_errors', 1 );
require_once(dirname(__FILE__) . "/../../cl_common_log_class.inc");
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

require_once(dirname(__FILE__) . "/../../cl_common_apply.inc");

/*********************************************************************************
 * 院内研修申請テーブルの require
 *********************************************************************************/
$log->debug("require START cl_apl_inside_training_apply_model.php",__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/template/cl_apl_inside_training_apply_model.php");
$log->debug("require END cl_apl_inside_training_apply_model.php",__FILE__,__LINE__);

/*********************************************************************************
 * 院内研修申込済テーブルの require
 *********************************************************************************/
$log->debug("require START cl_applied_inside_training_model.php",__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/ladder/cl_applied_inside_training_model.php");
$log->debug("require END cl_applied_inside_training_model.php",__FILE__,__LINE__);

/*********************************************************************************
 * 申請テーブルの require
 *********************************************************************************/
$log->debug("require START cl_apply_model.php",__FILE__,__LINE__);
require_once(dirname(__FILE__) . "/../model/workflow/cl_apply_model.php");
$log->debug("require END cl_apply_model.php",__FILE__,__LINE__);

// 2012/04/21 Yamagawa add(s)
class c100_handler {
// 2012/04/21 Yamagawa add(e)

	/*********************************************************************************
	 * クリニカルラダーコントロールから呼ばれる関数   【院内研修申請】
	 *********************************************************************************/
	/**
	 * 【新規申請】【申請】呼び出し
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function NewApl_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info("NewApl_Regist　Start",__FILE__,__LINE__);
		$this->dataApplyAdd(&$mdb2, $user, $param);
	}

	/**
	 * 【新規申請】【下書き保存】呼び出し
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function NewApl_Draft(&$mdb2, $user, $param){
		global $log;
		$log->info("NewApl_Draft　Start",__FILE__,__LINE__);
		$this->dataApplyAdd(&$mdb2, $user, $param);
	}

	/**
	 * 【新規申請】印刷
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function NewApl_Print(&$mdb2, $user, $param){
		global $log;
		$log->info("NewApl_Print　Start",__FILE__,__LINE__);
	}

	/**
	 * 【新規申請】（下書保存から）　申請　呼び出し
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info("DraftApl_Regist　Start",__FILE__,__LINE__);
		$this->dataApplyEdit(&$mdb2, $user, $param);
	}

	/**
	 * 【新規申請】（下書保存から）　下書き保存　呼び出し
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Draft(&$mdb2, $user, $param){
		global $log;
		$log->info("DraftApl_Draft　Start",__FILE__,__LINE__);
		$this->dataApplyEdit(&$mdb2, $user, $param);
	}

	/**
	 * 【新規申請】（下書保存から）　削除　呼び出し
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Delete(&$mdb2, $user, $param){
		global $log;
		$log->info("DraftApl_Delete　Start",__FILE__,__LINE__);
		$this->dataApplyDelete(&$mdb2, $user, $param);
	}

	/**
	 * 【新規申請】（下書き保存から）　印刷
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function DraftApl_Print(&$mdb2, $user, $param){
		global $log;
		$log->info("DraftApl_Print　Start",__FILE__,__LINE__);
	}

	/**
	 * 【申請詳細】更新　呼び出し
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_Update(&$mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_Update　Start",__FILE__,__LINE__);
		$this->dataApplyEdit(&$mdb2, $user, $param);
	}

	/**
	 * 【申請詳細】申請取消
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_Cancel(&$mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_Cancel　Start",__FILE__,__LINE__);
		$this->dataApplyDelete(&$mdb2, $user, $param);
	}

	/**
	 * 【申請詳細】申請 再申請
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_ReRegist(&$mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_ReRegist　Start",__FILE__,__LINE__);
		$this->dataApplyAdd(&$mdb2, $user, $param);
	}

	/**
	 * 【申請詳細】　印刷
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AplDetail_Print(&$mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_Print　Start",__FILE__,__LINE__);
	}

	/**
	 * 【承認詳細】承認 登録
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function ApvDetail_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info("ApvDetail_Regist　Start",__FILE__,__LINE__);
		$this->dataApproveAdd(&$mdb2, $user, $param);
	}

	/**
	 * 【承認詳細】承認 下書き保存
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function ApvDetail_Draft(&$mdb2, $user, $param){
		global $log;
		$log->info("AplDetail_ReRegist　Start",__FILE__,__LINE__);
	}

	/**
	 * 【承認詳細】　印刷
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function ApvDetail_Print(&$mdb2, $user, $param){
		global $log;
		$log->info("ApvDetail_Print　Start",__FILE__,__LINE__);
	}

	// 2012/04/21 Yamagawa add(s)
	/**
	 * 【自動承認】　
	 * @param unknown_type &$mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function AutoApv_Regist(&$mdb2, $user, $param){
		global $log;
		$log->info("AutoApv_Regist　Start",__FILE__,__LINE__);
	}
	// 2012/04/21 Yamagawa add(e)

	/*********************************************************************************
	 * 上記から呼ばれる関数   【院内研修申請】
	 *********************************************************************************/
	/**
	 * 申請時　登録処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApplyAdd($mdb2, $user, $param){
		global $log;
		// 研修データのセット
		$log->debug("dataApplyAdd　DBデータセットStart",__FILE__,__LINE__);
		$db_param = $this->setDbParam_fromParam(1, $mdb2, $user, $param);
		$log->debug("dataApplyAdd　DBデータセットEnd",__FILE__,__LINE__);

		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_apl_inside_training_apply_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		// 登録処理
		$log->debug("insert開始",__FILE__,__LINE__);
		$model->insert($db_param);
		$log->debug("insert終了",__FILE__,__LINE__);
	}

	/**
	 * 申請時　更新処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApplyEdit($mdb2, $user, $param){
		global $log;
		// 研修データのセット
		$log->debug("dataApplyEdit　DBデータセットStart",__FILE__,__LINE__);
		$db_param = $this->setDbParam_fromParam(0, $mdb2, $user, $param);
		$log->debug("dataApplyEdit　DBデータセットEnd",__FILE__,__LINE__);

		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_apl_inside_training_apply_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		// 更新処理
		$log->debug("update開始",__FILE__,__LINE__);
		$model->update($db_param);
		$log->debug("update終了",__FILE__,__LINE__);
	}

	/**
	 * 申請時　削除処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApplyDelete($mdb2, $user, $param){
		global $log;

		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_apl_inside_training_apply_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		// 削除処理
		$log->debug("delete開始",__FILE__,__LINE__);
		$model->logical_delete($param["training_apply_id"]);
		$log->debug("delete終了",__FILE__,__LINE__);
	}

	/**
	 * 承認時　登録処理
	 * @param unknown_type $mdb2
	 * @param String $user Loginユーザ
	 * @param unknown_type $param 画面からのパラメータ
	 */
	function dataApproveAdd($mdb2, $user, $param){
		global $log;

		$log->debug("cl_apply_model インスタンス作成開始",__FILE__,__LINE__);
		$apply_model = new cl_apply_model($mdb2,$user);
		$log->debug("cl_apply_model インスタンス作成終了",__FILE__,__LINE__);

		// 申請データ取得
		$log->debug("申請データ取得開始",__FILE__,__LINE__);
		$apply_data = $apply_model->select($param['apply_id']);
		$log->debug("申請データ取得終了",__FILE__,__LINE__);

		$log->debug("cl_applied_inside_training_model インスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_applied_inside_training_model($mdb2, $user);
		$log->debug("cl_applied_inside_training_model インスタンス作成終了",__FILE__,__LINE__);

		$log->debug('★★★★$apply_data[\'apply_stat\']:'.$apply_data['apply_stat'],__FILE__,__LINE__);
		if ($apply_data['apply_stat'] == 1){
			// 研修データのセット
			$log->debug("dataApploveAdd　DBデータセットStart",__FILE__,__LINE__);
			$db_param = $this->setAppliedDbParam_fromParam(1, $mdb2, $user, $param);
			$log->debug("dataApploveAdd　DBデータセットEnd",__FILE__,__LINE__);

			$log->debug('★★★★$apply_data[\'emp_id\']:'.$apply_data['emp_id'],__FILE__,__LINE__);

			// 登録処理
			//$log->debug("insert開始",__FILE__,__LINE__);
			//$model->insert($db_param);
			//$log->debug("insert終了",__FILE__,__LINE__);

			//このログが正しく出れば、↑のロジックをコメントアウトして、↓の処理を有効にしてください。
			$log->debug("■■■■■実装状況確認：この値が取れれば申請者に承認者が入る問題のテストが可能！  training_apply_id=".$param['training_apply_id'],__FILE__,__LINE__);

			// 登録処理
			$log->debug("insert開始",__FILE__,__LINE__);
			$model->insert_from_apply_template($param['training_apply_id'], $user);
			$log->debug("insert終了",__FILE__,__LINE__);

			// 出欠テーブル登録
			$param["emp_id"]=$apply_data['emp_id'];
			//dataRollAdd($mdb2, $user, $param,$apply_data['emp_id']);
			$this->dataRollAdd($mdb2, $user, $param);
		}
	}

	/**
	 * パラメータからDBを登録・更新するための配列にセットする
	 * @param int　$iAdd　0:Edit　1:Add
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @return array　$db_param　DBを登録・更新するための配列
	 */
	function setDbParam_fromParam($iAdd, $mdb2, $user, $param){
		global $log;
		$log->debug("setDbParam_fromParam　Start　iAdd = " . $iAdd,__FILE__,__LINE__);
		// 2012/11/20 Yamagawa add(s)
		if ($param['year'] == "") {
			$year = date(Y);
			if (date(n) <= 3) {
				$yaer -= 1;
			}
		} else {
			$year = $param['year'];
		}
		// 2012/11/20 Yamagawa add(e)
		if($iAdd == 1){
			$log->debug("setDbParam_fromParam　シーケンスCall Start",__FILE__,__LINE__);
			$training_apply_id = $this->getSequence_forAdd($mdb2, $user, $param);
			$db_param = array(
				"emp_id"				=> 					$param['emp_id'					],
				"training_apply_id"		=> 					$training_apply_id				,
				"apply_id"				=> 					$param['apply_id'				],
				"charange_level"		=> 					$param['charange_level'			],
				"inside_training_id"	=> 					$param['inside_training_id'		],
				"plan_id"				=> 					$param['plan_id'				],
				"inside_training_date"	=> 					$param['inside_training_date'	],
				"change_apply_date"		=> 					null							,
				"first_apply_id"		=> 					$training_apply_id				,
				"before_apply_id"		=> 					null							,
				"update_division"		=> 					0								,
				"change_reason"			=> 					null							,
				"learn_memo"			=> pg_escape_string($param['learn_memo'				]),
				"recommend_flag"		=> 					$param['recommend_flag'			],
				"delete_flg"			=> 					0								,
				"create_user"			=> 					$user
				// 2012/11/20 Yamagawa add(s)
				,"year"					=>					$year
				// 2012/11/20 Yamagawa add(e)
			);
		}else{
			$db_param = array(
				"emp_id"				=> 					$param['emp_id'					],
				"training_apply_id"		=> 					$param['training_apply_id'		],
				"apply_id"				=> 					$param['apply_id'				],
				"charange_level"		=> 					$param['charange_level'			],
				"inside_training_id"	=> 					$param['inside_training_id'		],
				"plan_id"				=> 					$param['plan_id'				],
				"inside_training_date"	=> 					$param['inside_training_date'	],
				"change_apply_date"		=> 					null							,
				"first_apply_id"		=> 					$param['first_apply_id'			],
				"before_apply_id"		=> 					null							,
				"update_division"		=> 					0								,
				"change_reason"			=> 					null							,
				"learn_memo"			=> pg_escape_string($param['learn_memo'				]),
				"recommend_flag"		=> 					$param['recommend_flag'			],
				"update_user" 			=> 					$user
				// 2012/11/20 Yamagawa add(s)
				,"year"					=>					$year
				// 2012/11/20 Yamagawa add(e)
			);
		}

	//	$log->debug("db_param = " .print_r($db_param),__FILE__,__LINE__);
		return $db_param;
	}

	/**
	 * 院内研修申請IDを取得する
	 * @param unknown_type　$param パラメータ
	 */
	function getSequence_forAdd($mdb2, $user, $param){
		global $log;
		$log->debug("getSequence_forAdd　Sequence require Start",__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/sequence/cl_inside_training_apply_id_seq_model.php");
		$log->debug("getSequence_forAdd　Sequence require End",__FILE__,__LINE__);

		$log->debug("getSequence_forAdd　Sequence インスタンス作成 Start",__FILE__,__LINE__);
		$model = new cl_inside_training_apply_id_seq_model($mdb2, $user);
		$log->debug("getSequence_forAdd　Sequence インスタンス作成 End",__FILE__,__LINE__);

		$log->debug("getSequence_forAdd　Sequence getID Start",__FILE__,__LINE__);
		$training_apply_id= $model->getInsideTrainingApplyId();
		$log->debug("院内研修申請ID：".$training_apply_id,__FILE__,__LINE__);
		$log->debug("getSequence_forAdd　Sequence getID End",__FILE__,__LINE__);
		return $training_apply_id;
	}

	/**
	 * パラメータから申請済みDBを登録・更新するための配列にセットする
	 * @param int　$iAdd　0:Edit　1:Add
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @return array　$db_param　DBを登録・更新するための配列
	 */
	function setAppliedDbParam_fromParam($iAdd, $mdb2, $user, $param){
		global $log;
		$log->debug("setAppliedDbParam_fromParam　Start　iAdd = " . $iAdd . "   recommend_flag = ". $param['hdn_recommend_flag'],__FILE__,__LINE__);
		if($iAdd == 1){
			$db_param = array(
				"training_apply_id"			=> 					$param["training_apply_id"		],
				"last_training_apply_id"	=> 					$param["training_apply_id"		],
				"emp_id"					=> 					$param['emp_id'					],
				"charange_level"			=> 					$param['charange_level'			],
				"inside_training_id"		=> 					$param['inside_training_id'		],
				"plan_id"					=> 					$param['plan_id'				],
				"inside_training_date"		=> 					$param['inside_training_date'	],
				"change_reason"				=> 					$param['change_reason'			],
				"learn_memo"				=> pg_escape_string($param['learn_memo'				]),
				"recommend_flag"			=> 					$param['hdn_recommend_flag'		],
				"delete_flg"				=> 					0,
				"create_user"				=> 					$user
			);
		}else{
			$db_param = array(
				"training_apply_id"			=> 					$param["training_apply_id"		],
				"last_training_apply_id"	=> 					$param["training_apply_id"		],
				"emp_id"					=> 					$param['emp_id'					],
				"charange_level"			=> 					$param['charange_level'			],
				"inside_training_id"		=> 					$param['inside_training_id'		],
				"plan_id"					=> 					$param['plan_id'				],
				"inside_training_date"		=> 					$param['inside_training_date'	],
				"change_reason"				=> 					$param['change_reason'			],
				"learn_memo"				=> pg_escape_string($param['learn_memo'				]),
				"recommend_flag"			=> 					$param['hdn_recommend_flag'		],
				"update_user"				=> 					$user
			);
		}

	//	$log->debug("db_param = " .print_r($db_param),__FILE__,__LINE__);
		return $db_param;
	}

	//***************************************************************************************************
	//   院内研修出欠関連
	//***************************************************************************************************
	/**
	 * 院内研修出欠登録
	 * @param unknown_type $mdb2
	 * @param string $user login User
	 * @param unknown_type $param
	 */
	function dataRollAdd($mdb2, $user, $param){
	//function dataRollAdd($mdb2, $user, $param,$apply_emp_id){
		global $log;
		$log->debug("*****************************************************",__FILE__,__LINE__);
		$log->debug("　　　出欠登録処理開始　　　　",__FILE__,__LINE__);
		$log->debug("*****************************************************",__FILE__,__LINE__);
		$log->debug('dataRoll Require Start'	,__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/workflow/cl_inside_training_roll_model.php");
		$log->debug('dataRoll Require End'	,__FILE__,__LINE__);

		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_inside_training_roll_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		$log->debug("plan_id = ". $param["plan_id"],__FILE__,__LINE__);
		$arr_plan = array();
		$arr_plan = split(",", $param["plan_id"]);
		$log->debug("arr_plan = " . count($arr_plan), __FILE__,__LINE__);

		for($i = 0; $i < count($arr_plan); $i++){
			$log->debug("****************************" . $i + 1 . "件目 plan_id = ". $arr_plan[$i],__FILE__,__LINE__);
			// 日程データの検索
			$log->debug("日程データ検索 Start",__FILE__,__LINE__);
			$sel_plan = $this->getScheduleInfo($mdb2, $arr_plan[$i]);
			$log->debug("日程データ検索 End",__FILE__,__LINE__);
			$param['plan_no'] = $sel_plan['plan_no'];

			// 出欠データ検索
			$log->debug("出欠データ検索 Start  plan_id = ". $arr_plan[$i] . "   emp_id = " . $param["emp_id"],__FILE__,__LINE__);
			//$log->debug("出欠データ検索 Start  plan_id = ". $arr_plan[$i] . "   emp_id = " . $apply_emp_id,__FILE__,__LINE__);
			$sel_roll = $model->searchRec($arr_plan[$i], $param["emp_id"]);
			//$sel_roll = $model->searchRec($arr_plan[$i], $apply_emp_id);

			$log->debug("出欠データ検索 End  count sel_roll = ". count($sel_roll),__FILE__,__LINE__);

			if(count($sel_roll) <= 0){
				// 研修出欠データのセット
				$log->debug("dataRollAdd　DBデータ登録 セットStart",__FILE__,__LINE__);
				$db_param = $this->setRollDbParam_fromParam(1, $mdb2, $user, $param, $arr_plan[$i]);
				$log->debug("dataRollAdd　DBデータ登録 セットStart",__FILE__,__LINE__);

				// 登録処理
				$log->debug("insert開始",__FILE__,__LINE__);
				$model->insert($db_param);
				$log->debug("insert終了",__FILE__,__LINE__);
			}else{
				// 研修出欠データのセット
				$log->debug("dataRollAdd　DBデータ更新 セットStart",__FILE__,__LINE__);
				$db_param = $this->setRollDbParam_fromParam(0, $mdb2, $user, $sel_roll, $arr_plan[$i]);
				$log->debug("dataRollAdd　DBデータ更新 セットStart",__FILE__,__LINE__);

				// 更新処理
				$log->debug("update開始",__FILE__,__LINE__);
				$model->update($db_param);
				$log->debug("update終了",__FILE__,__LINE__);
			}
		}
	}

	/**
	 * パラメータから出欠DBを登録・更新するための配列にセットする
	 * @param int　$iAdd　0:Edit　1:Add
	 * @param unknown_type　$mdb2
	 * @param String　$user   Loginユーザ
	 * @param unknown_type　$param
	 * @param array　$db_plan
	 * @param string　$plan_id
	 * @return array　$db_param　DBを登録・更新するための配列
	 */
	function setRollDbParam_fromParam($iAdd, $mdb2, $user, $param, $plan_id){
		global $log;
		$log->debug("setRollDbParam_fromParam　Start　iAdd = " . $iAdd,__FILE__,__LINE__);
		if($iAdd == 1){
			$db_param = array(
				"training_roll_id"		=> $this->getInsideTrainingRollID($mdb2, $user),
				"plan_id"				=> $plan_id,
				"training_id"			=> $param['inside_training_id'],
				"plan_no"				=> $param['plan_no'],
				"emp_id"				=> $param['emp_id'],
				"apply_id"				=> $param["training_apply_id"],
			//	"emp_roll"				=> 1,
				"emp_roll"				=> 0,
				"delete_flg"			=> 0,
				"create_user"			=> $user
			);
		}else{
			$db_param = array(
				"training_roll_id"		=> $param['training_roll_id'	],
				"plan_id"				=> $plan_id						,
				"training_id"			=> $param['training_id'			],
				"plan_no"				=> $param['plan_no'				],
				"emp_id"				=> $param['emp_id'				],
				"apply_id"				=> $param["apply_id"			],
				"emp_roll"				=> $param["emp_roll"			],
				"update_user"			=> $user
			);
		}
		return $db_param;
	}

	/**
	 * 院内研修出欠シーケンスID取得
	 */
	function getInsideTrainingRollID($mdb2, $user){
		global $log;
		$log->debug("getInsideTrainingRollID　Sequence require Start",__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/sequence/cl_inside_training_roll_id_seq_model.php");
		$log->debug("getInsideTrainingRollID　Sequence require End",__FILE__,__LINE__);

		$log->debug("getInsideTrainingRollID　インスタンス作成　Start",__FILE__,__LINE__);
		$model = new cl_inside_training_roll_id_seq_model($mdb2, $user);
		$log->debug("getInsideTrainingRollID　インスタンス作成　End",__FILE__,__LINE__);

		$log->debug("getInsideTrainingRollID　getID　Start",__FILE__,__LINE__);
		$training_roll_id= $model->getInsideTrainingRollID();
		$log->debug("getInsideTrainingRollID　getID　End    roll_id = ". $training_roll_id,__FILE__,__LINE__);
		return $training_roll_id;
	}

	/**
	 * 日程情報の検索
	 * @param string $plan_id
	 */
	function getScheduleInfo($mdb2, $plan_id){
		global $log;
		$log->debug('getScheduleInfo Require Start'	,__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/../model/master/cl_mst_inside_training_schedule_model.php");
		$log->debug('getScheduleInfo Require End'	,__FILE__,__LINE__);

		$log->debug("modelインスタンス作成開始",__FILE__,__LINE__);
		$model = new cl_mst_inside_training_schedule_model($mdb2, $user);
		$log->debug("modelインスタンス作成終了",__FILE__,__LINE__);

		// 検索処理
		$log->debug("select開始　　plan_id = ". $plan_id,__FILE__,__LINE__);
		$sel_plan = $model->select($plan_id);
		$log->debug("select終了",__FILE__,__LINE__);
		return $sel_plan;
	}
// 2012/04/21 Yamagawa add(s)
}
// 2012/04/21 Yamagawa add(e)
?>
