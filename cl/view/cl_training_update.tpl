<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<title>CoMedix {$cl_title} | �����ֺ´���</title>
	<script type="text/javascript" src="js/fontsize.js"></script>
	<script type="text/javascript" src="js/cl_training_update.js"></script>
	{$yui_cal_part}
	{literal}
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<style type="text/css">
		.list {border-collapse:collapse;}
		.list td {border:#A0D25A solid 1px;}

		table.block_in {border-collapse:collapse;}
		table.block_in td {border:#A0D25A solid 0px;}
	</style>
	{/literal}
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr height="32" bgcolor="#35B341">
			<td class="spacing"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j14" color="#ffffff"><b>�����ֺ´���{if $mode == "readOnly"}(�ܺ�){else}(����){/if}</b></font></td>
			<td>&nbsp;</td>
			<td width="10">&nbsp;</td>
			<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="�Ĥ���" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<img src="img/spacer.gif" width="1" height="10" alt=""><br>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<form id="training_form" name="training_form" action="#" method="post">
					<input type="hidden" id="session" name="session" value="{$session}">
					<input type="hidden" id="mode" name="mode" value="{$mode}">
					<input type="hidden" name="updateflg" id="updateflg" value="{$updateflg}">
					<input type="hidden" id="input_div" name="input_div">
					<input type="hidden" id="training_id" name="training_id" value="{$training_id}">
					<input type="hidden" id="before_max_training_time" name="before_max_training_time" value="{$max_training_time}">
					<input type="hidden" id="schedule_count" name="schedule_count" value="{$schedule_count}">
					<input type="hidden" id="theme_id_before" name="theme_id_before" value="{$theme_id_before}">
					<input type="hidden" id="theme_id_now" name="theme_id_now" value="{$theme_id_now}">
					<input type="hidden" id="theme_id_after" name="theme_id_after" value="{$theme_id_after}">
					{if $mode != "readOnly"}
					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td width="*" align="right">
								<input type="button" value="����" onclick="training_update();">
								&nbsp;&nbsp;
							</td>
						</tr>
					</table>
					{/if}
					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����̾</font></td>
							<td width="120px"><input type="text" id="training_name" name="training_name" value="{$training_name}" size="50" {$mode}></td>
							<td width="90px" align="right">
								<input type="checkbox" id="abolition_flag" name="abolition_flag" value="t" {$abolition_flag} {if $mode == "readOnly"}disabled{/if}>
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ѻ߶�ʬ</font>
							</td>
							<!-- 2012/07/25 Yamagawa add(s) -->
							<td width="150px" align="right">
								<input type="checkbox" id="auto_control_flg" name="auto_control_flg" value="t" {$auto_control_flg} {if $mode == "readOnly"}disabled{/if}>
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�������ռ�ư����</font>
							</td>
							<!-- 2012/11/19 Yamagawa add(s) -->
							<td width="130px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��������</font>
								<select id="term_div" name="term_div" {if $mode == "readOnly"}disabled{/if}>{$term_div_options_str}</select>
							</td>
							<!-- 2012/11/19 Yamagawa add(e) -->
							<td width="*"></td>
							<!-- 2012/07/25 Yamagawa add(e) -->
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��Ū</font></td>
							<td><textarea id="training_purpose" name="training_purpose" rows="5" cols="100" {$mode}>{$training_purpose}</textarea></td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td width="120px" align="right">
								{if $mode != "readOnly"}
								<a href="javascript:void(0)" onclick="clearEmployee('training_opener')">
								{/if}
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��ż�</font>
								{if $mode != "readOnly"}
								</a>
								{/if}
							</td>
							<td width="210px">
								<input type="text" id="training_opener_nm" name="training_opener_nm" readOnly value="{$training_opener_nm}">
								<input type="hidden" id="training_opener_id" name="training_opener_id" value="{$training_opener_id}">
								{if $mode != "readOnly"}
								<input type="button" onclick="openEmployeeList('training_opener')" value="����̾��">
								{/if}
							</td>
							<td width="40px" align="right">
								{if $mode != "readOnly"}
								<a href="javascript:void(0)" onclick="clearEmployee('training_teacher1')">
								{/if}
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ֻգ�</font>
								{if $mode != "readOnly"}
								</a>
								{/if}
							</td>
							<td width="210px">
								<input type="text" id="training_teacher1_nm" name="training_teacher1_nm" readOnly value="{$training_teacher1_nm}">
								<input type="hidden" id="training_teacher1_id" name="training_teacher1_id" value="{$training_teacher1_id}">
								{if $mode != "readOnly"}
								<input type="button" onclick="openEmployeeList('training_teacher1')" value="����̾��">
								{/if}
							</td>
							<td width="40px" align="right">
								{if $mode != "readOnly"}
								<a href="javascript:void(0)" onclick="clearEmployee('training_teacher4')">
								{/if}
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ֻգ�</font>
								{if $mode != "readOnly"}
								</a>
								{/if}
							</td>
							<td width="210px">
								<input type="text" id="training_teacher4_nm" name="training_teacher4_nm" readOnly value="{$training_teacher4_nm}">
								<input type="hidden" id="training_teacher4_id" name="training_teacher4_id" value="{$training_teacher4_id}">
								{if $mode != "readOnly"}
								<input type="button" onclick="openEmployeeList('training_teacher4')" value="����̾��">
								{/if}
							</td>
							<td width="*"></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td align="right">
								{if $mode != "readOnly"}
								<a href="javascript:void(0)" onclick="clearEmployee('training_teacher2')">
								{/if}
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ֻգ�</font>
								{if $mode != "readOnly"}
								</a>
								{/if}
							</td>
							<td>
								<input type="text" id="training_teacher2_nm" name="training_teacher2_nm" readOnly value="{$training_teacher2_nm}">
								<input type="hidden" id="training_teacher2_id" name="training_teacher2_id" value="{$training_teacher2_id}">
								{if $mode != "readOnly"}
								<input type="button" onclick="openEmployeeList('training_teacher2')" value="����̾��">
								{/if}
							</td>
							<td align="right">
								{if $mode != "readOnly"}
								<a href="javascript:void(0)" onclick="clearEmployee('training_teacher5')">
								{/if}
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ֻգ�</font>
								{if $mode != "readOnly"}
								</a>
								{/if}
							</td>
							<td>
								<input type="text" id="training_teacher5_nm" name="training_teacher5_nm" readOnly value="{$training_teacher5_nm}">
								<input type="hidden" id="training_teacher5_id" name="training_teacher5_id" value="{$training_teacher5_id}">
								{if $mode != "readOnly"}
								<input type="button" onclick="openEmployeeList('training_teacher5')" value="����̾��">
								{/if}
							</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td align="right">
								{if $mode != "readOnly"}
								<a href="javascript:void(0)" onclick="clearEmployee('training_teacher3')">
								{/if}
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ֻգ�</font>
								{if $mode != "readOnly"}
								</a>
								{/if}
							</td>
							<td>
								<input type="text" id="training_teacher3_nm" name="training_teacher3_nm" readOnly value="{$training_teacher3_nm}">
								<input type="hidden" id="training_teacher3_id" name="training_teacher3_id" value="{$training_teacher3_id}">
								{if $mode != "readOnly"}
								<input type="button"onclick="openEmployeeList('training_teacher3')" value="����̾��">
								{/if}
							</td>
							<td></td>
							<td></td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�оݼ�</font></td>
							<td width="50px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��٥�I</font></td>
							<td width="50px"><select id="target_level1" name="target_level1" {if $mode == "readOnly"}disabled{/if}>{$level1_options_str}</select></td>
							<td width="50px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��٥�II</font></td>
							<td width="50px"><select id="target_level1" name="target_level2" {if $mode == "readOnly"}disabled{/if}>{$level2_options_str}</select></td>
							<td width="50px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��٥�III</font></td>
							<td width="50px"><select id="target_level3" name="target_level3" {if $mode == "readOnly"}disabled{/if}>{$level3_options_str}</select></td>
							<td width="50px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��٥�IV</font></td>
							<td width="50px"><select id="target_level4" name="target_level4" {if $mode == "readOnly"}disabled{/if}>{$level4_options_str}</select></td>
							<td width="50px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��٥�V</font></td>
							<td width="50px"><select id="target_level5" name="target_level5" {if $mode == "readOnly"}disabled{/if}>{$level5_options_str}</select></td>

						</tr>
					</table>
					<img src="img/spacer.gif" width="1" height="20" alt=""><br>

					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���Ȼ��ֶ�ʬ</font></td>
							<td width="80px"><select id="time_division" name="time_division" {if $mode == "readOnly"}disabled{/if}>{$time_division_options_str}</select></td>
							<td width="60px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�������</font></td>
							<td><input type="text" id="max_training_time" name="max_training_time" value="{$max_training_time}" maxlength="2" style="ime-mode:disabled;text-align:right;" size="2" {$mode}></td>
							<td width="*"></td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�����׷�</font></td>
							<td>
								<textarea id="training_require" name="training_require" cols="100" rows="5" {$mode}>{$training_require}</textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��ɸ</font></td>
							<td>
								<textarea id="training_slogan" name="training_slogan" cols="100" rows="5" {$mode}>{$training_slogan}</textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
							<td>
								<textarea id="training_contents" name="training_contents" cols="100" rows="5" {$mode}>{$training_contents}</textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����Ǥθ�����<br>�μ���Ȥ�</font></td>
							<td>
								<textarea id="training_action" name="training_action" cols="100" rows="5" {$mode}>{$training_action}</textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">OJT�Ǥλٱ�����</font></td>
							<td>
								<textarea id="training_support" name="training_support" cols="100" rows="5" {$mode}>{$training_support}</textarea>
							</td>
						</tr>
					</table>

					<img src="img/spacer.gif" width="1" height="20" alt=""><br>

					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><b>�������������</b></font>
							</td>
							<td></td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[����]</font>
							</td>
							<td>
								<textarea id="theme_contents_before" name="theme_contents_before" rows="5" cols="100" {$mode}>{$theme_contents_before}</textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[�����ˡ��]</font>
							</td>
							<td>
								<textarea id="theme_method_before" name="theme_method_before" rows="5" cols="100" {$mode}>{$theme_method_before}</textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[��д���]</font>
							</td>
							<td>
								<input type="text" id="theme_deadline_before" name="theme_deadline_before" value="{$theme_deadline_before}" {$mode}>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								{if $mode != "readOnly"}
								<a href="javascript:void(0)" onclick="clearEmployee('theme_present_before')">
								{/if}
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[�����]</font>
								{if $mode != "readOnly"}
								</a>
								{/if}
							</td>
							<td>
								<input type="text" id="theme_present_before_nm" name="theme_present_before_nm" readOnly value="{$theme_present_before_nm}">
								<input type="hidden" id="theme_present_before_id" name="theme_present_before_id" value="{$theme_present_before_id}">
								{if $mode != "readOnly"}
								<input type="button" onclick="openEmployeeList('theme_present_before')" value="����̾��">
								{/if}
							</td>
						</tr>
						<tr>
							<td>
								<img src="img/spacer.gif" width="1" height="10" alt=""><br>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><b>������������</b></font>
							</td>
							<td></td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[����]</font>
							</td>
							<td>
								<textarea id="theme_contents_now" name="theme_contents_now" rows="5" cols="100" {$mode}>{$theme_contents_now}</textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[�����ˡ��]</font>
							</td>
							<td>
								<textarea id="theme_method_now" name="theme_method_now" rows="5" cols="100" {$mode}>{$theme_method_now}</textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[��д���]</font>
							</td>
							<td>
								<input type="text" id="theme_deadline_now" name="theme_deadline_now" value="{$theme_deadline_now}"  {$mode}>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								{if $mode != "readOnly"}
								<a href="javascript:void(0)" onclick="clearEmployee('theme_present_now')">
								{/if}
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[�����]</font>
								{if $mode != "readOnly"}
								</a>
								{/if}
							</td>
							<td>
								<input type="text" id="theme_present_now_nm" name="theme_present_now_nm" readOnly value="{$theme_present_now_nm}">
								<input type="hidden" id="theme_present_now_id" name="theme_present_now_id" value="{$theme_present_now_id}">
								{if $mode != "readOnly"}
								<input type="button" onclick="openEmployeeList('theme_present_now')" value="����̾��">
								{/if}
							</td>
						</tr>
						<tr>
							<td>
								<img src="img/spacer.gif" width="1" height="10" alt=""><br>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><b>��������ʸ��</b></font>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[����]</font>
							</td>
							<td>
								<textarea id="theme_contents_after" name="theme_contents_after" rows="5" cols="100"  {$mode}>{$theme_contents_after}</textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[�����ˡ��]</font>
							</td>
							<td>
								<textarea id="theme_method_after" name="theme_method_after" rows="5" cols="100"  {$mode}>{$theme_method_after}</textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[��д���]</font>
							</td>
							<td>
								<input type="text" id="theme_deadline_after" name="theme_deadline_after" value="{$theme_deadline_after}" {$mode}>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								{if $mode != "readOnly"}
								<a href="javascript:void(0)" onclick="clearEmployee('theme_present_after')">
								{/if}
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[�����]</font>
								{if $mode != "readOnly"}
								</a>
								{/if}
							</td>
							<td>
								<input type="text" id="theme_present_after_nm" name="theme_present_after_nm" readOnly value="{$theme_present_after_nm}">
								<input type="hidden" id="theme_present_after_id" name="theme_present_after_id" value="{$theme_present_after_id}">
								{if $mode != "readOnly"}
								<input type="button" onclick="openEmployeeList('theme_present_after')" value="����̾��">
								{/if}
							</td>
						</tr>
						{if $mode != "readOnly"}
						<tr>
							<td colspan="2" align="right">
								<input type="button" value="����" onclick="training_update();">
								&nbsp;&nbsp;
							</td>
						</tr>
						{/if}
					</table>
				</form>
			</td>
		</tr>
	</table>
	<img src="img/spacer.gif" alt="" width="1" height="5"><br>

</body>
</html>