{* ################################################################################################################################################################ *}
{* 子画面フッター *}
{* $title：インクルード元画面にて定義する *}
{* ################################################################################################################################################################ *}

{literal}
<!-- 子画面(共通モジュール)を開くためのフォーム -->
<form id="common_module_call_form" name="common_module_call_form" method="post" action="cl_common_call.php" target="common_module_window"></form>


<!-- 子画面(共通モジュール)を開くためのスクリプト START -->
<script type="text/javascript">
//子画面(共通モジュール)を開きます。
//
//moduleName 子画面のモジュール名を指定します。
//params     子画面へ送信する専用パラメータを設定します。Objectを指定してください。Objectの属性名が送信パラメータ名となり、値がパラメータとなります。
//option     子画面を開くときのオプションを指定します。(JavaScriptのopen関数のoptionと同じです。)
//wid        子画面用ウィンドウID
//session    CoMedixセッションID
function commonOpen(moduleName,params,option,wid,session)
{
	//alert("tpl commonOpen call");
	//子画面(共通モジュール)送信用フォームタグ
	var f = document.getElementById("common_module_call_form");

	f.target=wid;

	//共通パラメータ用のhiddenを作成、値を設定。
	var html = "";
	html = html + '<input type="hidden" name="common_module" value="' + moduleName + '">';
	html = html + '<input type="hidden" name="session"       value="' + session    + '">';

	//専用パラメータ用のhiddenをparamsから作成。
	for(var key in params)
	{
		html = html + '<input type="hidden" name="' + key + '" value="">';
	}
	f.innerHTML = html;
	//alert("tpl html:" + html);

	//専用パラメータ用のhiddenにparamsから値を設定。
	for(var key in params)
	{
		var hidden = eval("f." + key);
		hidden.value = eval("params." + key);
	}

	//子画面を開く
	window.open('', wid, option);

	//子画面へ送信
	f.submit();
}
</script>
{/literal}
<!-- 子画面(共通モジュール)を開くためのフォーム及びスクリプト END -->
