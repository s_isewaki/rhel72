{literal}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<title>CoMedix クリニカルラダー | {/literal}{$title}{literal}</title>
	<script type="text/javascript" src="js/fontsize.js"></script>
	<script type="text/javascript" src="js/yui_0.12.2/build/yahoo/yahoo-min.js"></script>
	<script type="text/javascript" src="js/yui_0.12.2/build/event/event-min.js" ></script>
	<script type="text/javascript" src="js/yui_0.12.2/build/dom/dom-min.js" ></script>
	<script type="text/javascript" src="js/yui_0.12.2/build/calendar/calendar-min.js"></script>
	<link type="text/css" rel="stylesheet" href="js/yui_0.12.2/build/calendar/assets/calendar.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/cl_common.js"></script>

	<link rel="stylesheet" type="text/css" href="css/main.css">
	<style type="text/css">
		.list {border-collapse:collapse;}
		.list td {border:#A0D25A solid 0px;}

		table.block_in {border-collapse:collapse;}
		table.block_in td {border:#A0D25A solid 0px;}
	</style>
</head>
{/literal}
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

	{* ############################################################################################################################################################ *}
	{* 子画面ヘッダー *}
	{* ############################################################################################################################################################ *}

	<img src="img/spacer.gif" alt="" width="1" height="5"><br>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<form id="ladder_form" name="ladder_form" action="cl_common_call.php" target="_self" method="post">
					<input type="hidden" id="emp_date" name="emp_date" value="{$emp_date}">
					{section name=cell loop=$data}
						<!-- 一覧 -->
						<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
							<!-- ヘッダー -->
							<tr>
								<td align="left" width="30" height="15"></td>
								<td align="left" width="*" height="15"></td>
								<td align="left" width="30" height="15"></td>
							</tr>
							<tr>
								<td align="left" width="30"></td>
								<td align="center" width="*"><font size="10" face="ＭＳ Ｐゴシック, Osaka">認定証</font></td>
								<td align="left" width="30"></td>
							</tr>
							<tr>
								<td align="left" width="30" height="15"></td>
								<td align="left" width="*" height="15"></td>
								<td align="left" width="30" height="15"></td>
							</tr>
							<tr>
								<td align="left" width="30"></td>
								<td align="right" width="*"><font size="8" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$data[cell][1]}　殿</font></td>
								<td align="left" width="30"></td>
							</tr>
							<tr>
								<td align="left" width="30" height="15"></td>
								<td align="left" width="*" height="15"></td>
								<td align="left" width="30" height="15"></td>
							</tr>
							<tr>
								<td align="left" width="30"></td>
								<td align="left" width="*"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">　あなたを 能力レベル{$data[cell][3]}段階の看護師として認定いたします。</font></td>
								<td align="left" width="30"></td>
							</tr>
							<tr>
								<td align="left" width="30" height="30"></td>
								<td align="left" width="*" height="30"></td>
								<td align="left" width="30" height="30"></td>
							</tr>
							<tr>
								<td align="left" width="30"></td>
								<td align="center" width="*"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">認定年月日　{$data[cell][5]}</font></td>
								<td align="left" width="30"></td>
							</tr>
							<tr>
								<td align="left" width="30" height="15"></td>
								<td align="left" width="*" height="15"></td>
								<td align="left" width="30" height="15"></td>
							</tr>
							<tr>
								<td align="left" width="30"></td>
								<td align="center" width="*"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">○○病院　　　　院長　○○　○○</font></td>
								<td align="left" width="30"></td>
							</tr>
						</table>
						<img src="img/spacer.gif" alt="" width="1" height="50"><br>
					{/section}
					{* ############################################################################################################################################ *}
					{* 隠し項目 *}
					{* ############################################################################################################################################ *}
					<input type="hidden" name="common_module" value="{$common_module}">
					<input type="hidden" name="session"       value="{$session}">
				</form>
			</td>
		</tr>
	</table>
	<img src="img/spacer.gif" alt="" width="1" height="5"><br>

	{* ############################################################################################################################################################ *}
	{* 子画面フッター *}
	{* ############################################################################################################################################################ *}
	{include file='cl_sub_screen_footer.tpl'}

	<script type="text/javascript">
		YAHOO.namespace("ui.calendar");
		{$calendar}
	</script>


</body>
</html>