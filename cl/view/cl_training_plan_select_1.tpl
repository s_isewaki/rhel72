{literal}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<title>CoMedix クリニカルラダー | {/literal}{$title}{literal}</title>
	<script type="text/javascript" src="js/fontsize.js"></script>
	<script type="text/javascript" src="js/cl_training_insert.js"></script>
	<script type="text/javascript" src="js/yui_0.12.2/build/yahoo/yahoo-min.js"></script>
	<script type="text/javascript" src="js/yui_0.12.2/build/event/event-min.js" ></script>
	<script type="text/javascript" src="js/yui_0.12.2/build/dom/dom-min.js" ></script>
	<script type="text/javascript" src="js/yui_0.12.2/build/calendar/calendar-min.js"></script>
	<link type="text/css" rel="stylesheet" href="js/yui_0.12.2/build/calendar/assets/calendar.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/cl_common.js"></script>

	<link rel="stylesheet" type="text/css" href="css/main.css">
	<style type="text/css">
		.list {border-collapse:collapse;}
		.list td {border:#A0D25A solid 1px;}

		table.block_in {border-collapse:collapse;}
		table.block_in td {border:#A0D25A solid 0px;}
	</style>

	<script type="text/javascript" src="cl/common/cl_training_plan_select_1.js"></script>
	<script type="text/javascript" >
	//受付一覧
	function open_reception_list(login_user,plan_id,training_id){

		//alert('open_reception_list() login_user:' + login_user);
		//alert('open_reception_list() plan_id:' + plan_id);
		//alert('open_reception_list() training_id:' + training_id);

		var moduleName = "cl_inside_training_reception_list";

		//リクエストパラメータをオブジェクトで作成
		var params = new Object();
		params.login_user =login_user;
		params.plan_id = plan_id;
		params.training_id = training_id;
		params.p_session = document.training_form.session.value;

		//alert('open_reception_list() document.training_form.session.value:' + document.training_form.session.value);
		//alert('open_reception_list() params.p_session                    :' + params.p_session);

		//ウィンドウサイズなどのオプション
		//var h = window.screen.availHeight;
		//var w = window.screen.availWidth;
		var w = 800;
		var h = 600;
		var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

		//子画面を開く
		commonOpen(moduleName,params,option,'cl_inside_training_reception_list',params.p_session);
	}
	// 出欠登録
	function open_attendance_regist(login_user,plan_id,training_id){

		var moduleName = "cl_inside_training_attendance_regist";

		//リクエストパラメータをオブジェクトで作成
		var params = new Object();
		params.login_user =login_user;
		params.plan_id = plan_id;
		params.training_id = training_id;
		params.p_session = document.training_form.session.value;

		//ウィンドウサイズなどのオプション
		//var h = window.screen.availHeight;
		//var w = window.screen.availWidth;
		var w = 800;
		var h = 600;
		var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

		//子画面を開く
		commonOpen(moduleName,params,option,'cl_inside_training_attendance_regist',params.p_session);
	}

	// 2012/08/22 Yamagawa add(s)
	function output_to_csv(){
		document.training_form.action = "./cl/common/cl_training_plan_select_1_csv.php";
		document.training_form.submit();
	}
	// 2012/08/22 Yamagawa add(e)

	// 2012/11/16 Yamagawa upd(s)
	function plan_reload() {
		document.training_form.action = "cl_common_call.php";
		document.training_form.submit();
	}
	// 2012/11/16 Yamagawa upd(e)

	</script>


</head>
{/literal}

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

	{* ############################################################################################################################################################ *}
	{* 子画面ヘッダー *}
	{* ############################################################################################################################################################ *}
	{include file='cl_sub_screen_header.tpl'}

	<img src="img/spacer.gif" width="1" height="10" alt=""><br>

	<table width="100%" border="0" cellspacing="5" cellpadding="0">
		<tr>

			<td>
				<form id="training_form" name="training_form" action="cl_common_call.php" target="_self" method="post">

					{* ############################################################################################################################################ *}
					{* 開催日登録ボタン *}
					{* ############################################################################################################################################ *}

					<table width="1024" border="0" cellspacing="0" cellpadding="3" >
						<tr>
							<!-- 2012/11/16 Yamagawa add(s) -->
							<td align="left">
								<table class="list">
									<tr>
										<td width="30" bgcolor="#E5F6CD">
											<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年度</font>
										</td>
										<td>
											<select id="year" name="year" onchange="plan_reload();">{$option_year}</select>
										</td>
									</tr>
								</table>
							</td>
							<!-- 2012/11/16 Yamagawa add(e) -->
							<td align="right">
								<!-- 2012/08/27 Yamagawa add(s) -->
								{if count($data) == 0}
									<input type="button" value="一括修正" disabled>&nbsp;
									<input type="button" value="削除" disabled>&nbsp;
									<input type="button" value="修正" disabled>&nbsp;
								{else}
									<input type="button" value="一括修正" onclick="plan_update_all();">&nbsp;
									<input type="button" value="削除" onclick="plan_delete();">&nbsp;
									<input type="button" value="修正" onclick="plan_update();">&nbsp;
								{/if}
								<!-- 2012/08/27 Yamagawa add(e) -->
								<input type="button" value="登録" onclick="plan_regist();">&nbsp;
							</td>
						</tr>
					</table>

					{* ############################################################################################################################################ *}
					{* 開催日一覧 *}
					{* ############################################################################################################################################ *}

					<table width="1024" border="0" cellspacing="0" cellpadding="1" class="list">

						{* ######################################################################################################################################## *}
						{* 開催日一覧ヘッダー *}
						{* ######################################################################################################################################## *}

						<tr bgcolor="#E5F6CD">
							<td width="30"   align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">選択         </font></td>
							<!-- 2012/11/16 Yamagawa del(s) -->
							<!--
							<td width="120"  align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年度         </font></td>
							 -->
							<!-- 2012/11/16 Yamagawa del(e) -->
							<td width="60"   align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開催回数     </font></td>
							<td width="200"  align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開催日時     </font></td>
							<td width="150"  align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会場         </font></td>
<!--
							<td width="60"   align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告         </font></td>
							<td width="60"   align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アンケート   </font></td>
-->
							<td width="350"  align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備考         </font></td>
							<td width="40"   align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最大<br/>人数</font></td>
							<td width="40"   align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受付<br/>人数</font></td>
							<td width="40"   align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出席<br/>人数</font></td>
							<td width="80"   align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申込受付     </font></td>
						</tr>

						{* ######################################################################################################################################## *}
						{* 開催日一覧データ *}
						{* ######################################################################################################################################## *}
						{foreach from=$data item=var}
						<tr>
							<td align="center" ><input type="checkbox" name="plan_chkbox" value="{$var.plan_id}"></td>
							<!-- 2012/11/16 Yamagawa del(s) -->
							<!--
							<td align="left"   ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;{$var.year}&nbsp;-&nbsp;{$var.term_div_str}</font></td>
							 -->
							<!-- 2012/11/16 Yamagawa del(e) -->
							<td align="center" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;{$var.training_time}</font></td>
							<td align="center" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;{$var.plan_date_str}&nbsp;&nbsp;&nbsp;&nbsp;{$var.from_time_hh24}:{$var.from_time_mi}&nbsp;-&nbsp;{$var.to_time_hh24}:{$var.to_time_mi}</font></td>
							<td align="center" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;{$var.place}</font></td>
<!--
							<td align="center" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$var.report_division_str}</font></td>
							<td align="center" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$var.answer_division_str}</font></td>
-->
							<td align="left"   ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$var.remarks_br}</font></td>
							<td align="center" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$var.max_people}</font></td>
							<td align="center" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="#" onclick="open_reception_list('{$login_user}','{$var.plan_id}','{$var.training_id}');">{$var.u_cnt}</a></font></td>
							<td align="center" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="#" onclick="open_attendance_regist('{$login_user}','{$var.plan_id}','{$var.training_id}');">{$var.s_cnt}</a></font></td>
							<td align="center" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$var.apply_status_str}</font></td>
						</tr>
						{/foreach}

					<!--
							<td align="center" class="highlight_class_0" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="training_selected('CLTRA00000063');"><input type="checkbox" name="plan" value="{$var.plan_id}"></td>
					 -->
					</table>

					<!-- 2012/08/22 Yamagawa add(s) -->
					<img src="img/spacer.gif" width="0" height="20" alt="">
					{* ############################################################################################################################################ *}
					{* アンケート出力ボタン *}
					{* ############################################################################################################################################ *}

					<table width="1024" border="0" cellspacing="0" cellpadding="3" >
						<tr>
							<td align="left">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									<b>アンケート結果</b>
								</font>
							</td>
							<td align="right">
								<input type="button" value="アンケートCSV出力" onclick="output_to_csv();">&nbsp;
							</td>
						</tr>
					</table>

					{* ############################################################################################################################################ *}
					{* アンケート集計結果 *}
					{* ############################################################################################################################################ *}

					<table width="1024" border="0" cellspacing="0" cellpadding="1" class="list">

						{* ######################################################################################################################################## *}
						{* アンケート集計ヘッダー *}
						{* ######################################################################################################################################## *}

						<tr bgcolor="#E5F6CD">
							<td align="center" width="*"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設問</font></td>
							<td align="center" width="200"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回答結果</font></td>
							<td align="center" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回答数</font></td>
							<td align="center" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">割合</font></td>
						</tr>

						{* ######################################################################################################################################## *}
						{* アンケート集計データ *}
						{* ######################################################################################################################################## *}

						<tr>
							<td rowspan="4">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									研修の目的、目標は達成できましたか？
								</font>
							</td>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.achievement_best_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.achievement_best_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.achievement_best_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.achievement_good_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.achievement_good_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.achievement_good_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.achievement_not_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.achievement_not_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.achievement_not_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.achievement_bad_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.achievement_bad_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.achievement_bad_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td rowspan="4">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									ご自身の学習課題は達成できましたか？
								</font>
							</td>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.learned_best_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.learned_best_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.learned_best_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.learned_good_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.learned_good_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.learned_good_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.learned_not_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.learned_not_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.learned_not_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.learned_bad_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.learned_bad_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.learned_bad_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td rowspan="2">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									研修の日時は適切だと思いましたか？
								</font>
							</td>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.plan_best_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.plan_best_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.plan_best_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.plan_bad_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.plan_bad_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.plan_bad_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td rowspan="3">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									研修の方法（進め方）は適切だと思いましたか？
								</font>
							</td>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.method_best_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.method_best_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.method_best_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.method_good_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.method_good_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.method_good_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.method_not_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.method_not_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.method_not_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td rowspan="4">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									研修の内容は分かりやすいものでしたか？
								</font>
							</td>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.contents_best_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.contents_best_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.contents_best_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.contents_good_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.contents_good_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.contents_good_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.contents_not_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.contents_not_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.contents_not_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.contents_bad_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.contents_bad_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.contents_bad_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td rowspan="4">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									研修内容は今後の看護実践に役立ちますか？
								</font>
							</td>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.practice_best_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.practice_best_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.practice_best_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.practice_good_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.practice_good_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.practice_good_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.practice_not_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.practice_not_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.practice_not_percentage}
								</font>
							</td>
						</tr>

						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.practice_bad_name}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.practice_bad_count}
								</font>
							</td>
							<td align="right">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									{$answer.practice_bad_percentage}
								</font>
							</td>
						</tr>

					</table>
					<!-- 2012/08/22 Yamagawa add(e) -->

					{* ############################################################################################################################################ *}
					{* 隠し項目 *}
					{* ############################################################################################################################################ *}
					<input type="hidden" name="training_id" value="{$training_id}">
					<input type="hidden" name="login_user" value="{$login_user}">
					<input type="hidden" name="common_module" value="{$common_module}">
					<input type="hidden" name="session"       value="{$session}">

				</form>
			</td>

		</tr>
	</table>
	<img src="img/spacer.gif" alt="" width="1" height="5"><br>

	{* ############################################################################################################################################################ *}
	{* 子画面フッター *}
	{* ############################################################################################################################################################ *}
	{include file='cl_sub_screen_footer.tpl'}

</body>
</html>
