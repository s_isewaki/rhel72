<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<title>CoMedix {$cl_title} | ������������</title>
	<script type="text/javascript" src="js/fontsize.js"></script>
	<script type="text/javascript" src="js/cl_outside_seminar_select.js"></script>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript">
		var m_return_data_list = new Array();
	{section name=cell loop=$data}
		m_return_data_list["{$data[cell].seminar_apply_id}"] = new Object();
		m_return_data_list["{$data[cell].seminar_apply_id}"].seminar_apply_id			= "{$data[cell].seminar_apply_id}";
		m_return_data_list["{$data[cell].seminar_apply_id}"].seminar_name				= "{$data[cell].seminar_name|cat:$data[cell].participation_comment|escape:'javascript'}";
		m_return_data_list["{$data[cell].seminar_apply_id}"].participation_division		= "{$data[cell].participation_division}";
		m_return_data_list["{$data[cell].seminar_apply_id}"].report_date				= "{$data[cell].report_date}";
	{/section}
	</script>

	{$yui_cal_part}
	{literal}
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<style type="text/css">
		.list {border-collapse:collapse;}
		.list td {border:#A0D25A solid 1px;}

		table.block_in {border-collapse:collapse;}
		table.block_in td {border:#A0D25A solid 0px;}
	</style>
	{/literal}
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr height="32" bgcolor="#35B341">
			<td class="spacing"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j14" color="#ffffff"><b>������������</b></font></td>
			<td>&nbsp;</td>
			<td width="10">&nbsp;</td>
			<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="�Ĥ���" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<img src="img/spacer.gif" alt="" width="1" height="5"><br>

				<form id="training_form" name="training_form" action="#" method="post">
					<input type="hidden" id="session" name="session" value="{$session}">

					<img src="img/spacer.gif" alt="" width="1" height="20"><br>

					<table border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td align="left">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��������</font>
							</td>
						</tr>
					</table>

					<!-- ����ɽ�� -->
					<img src="img/spacer.gif" alt="" width="1" height="5"><br>
					<table border="0" cellspacing="0" cellpadding="1" class="list">
						<!-- �إå��� -->
						<tr bgcolor="#E5F6CD">
							<td align="left" width="200px"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����̾</font></td>
							<td align="left" width="80px"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">������</font></td>
							<td align="left" width="100px"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">������ô</font></td>
							<td align="left" width="200px"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">������</font></td>
							<td align="left" width="80px"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�����</font></td>
						</tr>
						<!-- ���� -->
						{section name=cell loop=$data}
						<input type="hidden" id="seminar_apply_id{$data[cell].seminar_apply_id}" name="seminar_apply_id{$data[cell].seminar_apply_id}" value="{$data[cell].seminar_apply_id}">
						<tr>
							<td align="left" width="200px" class="highlight_class_name{$data[cell].seminar_apply_id}" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick='return_selected(m_return_data_list["{$data[cell].seminar_apply_id}"]);'><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].seminar_name|cat:$data[cell].participation_comment}</font></td>
							<td align="left" width="80px" class="highlight_class_name{$data[cell].seminar_apply_id}" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick='return_selected(m_return_data_list["{$data[cell].seminar_apply_id}"]);'><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].apply_date}</font></td>
							<td align="left" width="100px" class="highlight_class_name{$data[cell].seminar_apply_id}" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick='return_selected(m_return_data_list["{$data[cell].seminar_apply_id}"]);'><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].expense}</font></td>
							<td align="left" width="200px" class="highlight_class_name{$data[cell].seminar_apply_id}" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick='return_selected(m_return_data_list["{$data[cell].seminar_apply_id}"]);'><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].from_date}&nbsp;-&nbsp;{$data[cell].to_date}</font></td>
							<td align="left" width="80px" class="highlight_class_name{$data[cell].seminar_apply_id}" onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick='return_selected(m_return_data_list["{$data[cell].seminar_apply_id}"]);'><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].report_date}</font></td>
						</tr>
						{/section}
					</table>
				</form>
			</td>
		</tr>
	</table>
	<img src="img/spacer.gif" alt="" width="1" height="5"><br>

	{* ############################################################################################################################################################ *}
	{* �Ҳ��̥եå��� *}
	{* ############################################################################################################################################################ *}
	{include file='cl_sub_screen_footer.tpl'}

</body>
</html>