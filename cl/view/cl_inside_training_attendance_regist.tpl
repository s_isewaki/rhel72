<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<title>CoMedix {$cl_title} | 研修講座管理(出欠登録)</title>
	<script type="text/javascript" src="./js/fontsize.js"></script>
	<script type="text/javascript" src="./js/cl_inside_training_attendance_regist.js"></script>
	{$yui_cal_part}
	{literal}
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<style type="text/css">
		.list {border-collapse:collapse;}
		.list td {border:#A0D25A solid 1px;}

		table.block_in {border-collapse:collapse;}
		table.block_in td {border:#A0D25A solid 0px;}
	</style>
	<script type="text/javascript" >
	/**
	 * output_to_csv
	 */
	/*function output_to_csv(type){

		switch (type){
			case 1:
				document.attendance_list.reception_csv_flg.value = 'true';
				document.attendance_list.answer_csv_flg.value = 'false';
				break;

			case 2:
				document.attendance_list.answer_csv_flg.value = 'true';
				document.attendance_list.reception_csv_flg.value = 'false';
				break;


			default:
				document.attendance_list.reception_csv_flg.value = 'false';
				document.attendance_list.answer_csv_flg.value = 'false';
			}

		document.attendance_list.import_flg.value = 'false';
		document.attendance_list.action = "./cl/common/cl_inside_training_attendance_regist_csv.php";
		document.attendance_list.submit();
	}

	function inport_csv(){
		if(document.getElementById("inputfile").value == ""){
			alert("インポートするファイルを選択してください");
			return false;
		}
		document.attendance_list.reception_csv_flg.value = 'false';
		document.attendance_list.answer_csv_flg.value = 'false';
		document.attendance_list.import_flg.value = 'true';
		document.attendance_list.submit();
	}*/
	</script>


	{/literal}
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr height="32" bgcolor="#35B341">
			<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>研修講座管理(出欠登録)</b></font></td>
			<td>&nbsp;</td>
			<td width="10">&nbsp;</td>
			<td width="32" align="center"><a href="javascript:void(0);" onclick="window_close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<img src="img/spacer.gif" width="1" height="10" alt=""><br>

	<!-- 画面表示用フォーム -->
	<form id="attendance_list" name="attendance_list" action="cl_common_call.php" target="_self" method="post" enctype="multipart/form-data">
		<input type="hidden" id="session" name="session" value="{$session}">
		<input type="hidden" id="login_user" name="login_user" value="{$login_user}">
		<input type="hidden" id="training_id" name="training_id" value="{$training_id}">
		<input type="hidden" id="plan_id" name="plan_id" value="{$plan_id}">
		<input type="hidden" name="common_module" value="{$common_module}">
		<input type="hidden" name="data_count" value="{$data_count}">
		<input type="hidden" name="roll_data" value="">
		<input type="hidden" id="import_flg" name="import_flg" value="false">
		<input type="hidden" id="attendance_csv_flg" name="attendance_csv_flg" value="false">
		<input type="hidden" id="answer_csv_flg" name="answer_csv_flg" value="false">
		<input type="hidden" id="update_flg" name="update_flg"  value="false">
		<img src="img/spacer.gif" alt="" width="1" height="2"><br>

		<!-- ヘッダー情報 -->
		<table width="100%" border="0" cellspacing="0" cellpadding="1">
			<tr>
				<td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
					院内研修番号：{$training_id}
					&nbsp;&nbsp;
					研修名：{$training_name}
				</font></td>
			</tr>
				<td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
					開催日時：{$plan_date_year}年{$plan_date_month}月{$plan_date_day}日  {$plan_time_from}&nbsp;&nbsp;-&nbsp;&nbsp;{$plan_time_to}
				</font></td>
			<tr>
			</tr>
			<tr>
				<td align="right">
					<input type="file" id="inputfile" name="inputfile" size="30">
					<input type="button" value="出欠インポート" onclick="inport_csv();" >
					<input type="button" value="出席者一覧CSV出力" onclick="output_to_csv(1);">
					<input type="button" value="アンケートCSV出力" onclick="output_to_csv(2);">
				</td>
			</tr>
		</table>

		<img src="img/spacer.gif" alt="" width="1" height="5"><br>

		<!-- 一覧表示 -->
	<table width="100%" border="0" cellspacing="5" cellpadding="1">
	<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
			<!-- ヘッダー -->
			<tr bgcolor="#E5F6CD">
				<td align="center" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No</font></td>
				<td align="center" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
				<td align="center" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員氏名</font></td>
				<td align="center" width="300"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属</font></td>
				<td align="center" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出席</font></td>
				<!-- 2012/09/03 Yamagawa add(s) -->
				<td align="center" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告<br>状況</font></td>
				<!-- 2012/09/03 Yamagawa add(e) -->
			</tr>
			<!-- 一覧 -->
			{section name=cell loop=$data}
			<tr onclick="">
				<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$data[cell].no}</font></td>
				<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$data[cell].emp_id}</font></td>
				<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$data[cell].emp_name}</font></td>
				<td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$data[cell].affiliation}</font></td>
				{if $data[cell].emp_roll == 1 }
				<td align="center"><input type="checkbox" id="roll{$data[cell].no}" value="t" {$data[cell].emp_roll} checked></td>
				{elseif $data[cell].emp_roll == 2 }
				<td align="center"><input type="checkbox" id="roll{$data[cell].no}" value="t" {$data[cell].emp_roll} disabled></td>
				{else}
				<td align="center"><input type="checkbox" id="roll{$data[cell].no}" value="t" {$data[cell].emp_roll} ></td>
				{/if}
				<!-- 2012/09/03 Yamagawa add(s) -->
				<td align="center">{$data[cell].report_status}</td>
				<!-- 2012/09/03 Yamagawa add(e) -->
			</tr>
			{/section}
		</table>
	</td>
	</tr>
	<tr>

	<td align="right"><input type="button" name="btn_upd" value="更新"	onclick="syukketu_update();"></td>

	</tr>
	</table>
	</form>

	<img src="img/spacer.gif" alt="" width="1" height="5"><br>
</body>

</html>