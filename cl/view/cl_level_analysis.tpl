<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
		<title>CoMedix {$cl_title} | ��٥�ʬ��</title>
		<script type="text/javascript" src="js/fontsize.js"></script>
		<script type="text/javascript" src="js/cl_level_analysis.js"></script>
		<script type="text/javascript">
			{$js_str}
		</script>
		{literal}
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<style type="text/css">
			.list {border-collapse:collapse;}
			.list td {border:#A0D25A solid 1px;}

			table.block_in {border-collapse:collapse;}
			table.block_in td {border:#A0D25A solid 0px;}
		</style>
		{/literal}
	</head>

	<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr bgcolor="#f6f9ff">
							<td width="32" height="32" class="spacing"><a href="ladder_menu.php?session={$session}"><img src="img/icon/s42.gif" width="32" height="32" border="0" alt="{$cl_title}"></a></td>
							<td>
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j16">
									&nbsp;<a href="ladder_menu.php?session={$session}">
										<b>{$cl_title}</b>
									</a>
									 &gt;
									 <a href="cl_level_analysis.php?session={$session}">
									 	<b>�����ʬ��</b>
									 </a>
								</font>
							</td>
							<!-- �������̥��ɽ�� -->
							{if $workflow_auth == 1}
							<td align="right" style="padding-right:6px;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j16">&nbsp;<a href="cl_workflow_menu.php?session={$session}&workflow=1"><b>�������̤�</b></a></font></td>
							{else}
							{/if}
						</tr>
					</table>

					<!-- ���֥�˥塼ɽ�� -->
					{$aplyctn_mnitm_str}

					<img src="img/spacer.gif" alt="" width="1" height="6"><br>

					<form id="terms" name="terms" action="#" method="post">
						<input type="hidden" id="session" name="session" value="{$session}">
						<input type="hidden" id="emp_date" name="emp_date" value="{$emp_date}">
						<input type="hidden" id="affiliation_row_count" name="affiliation_row_count" value="{$affiliation_row_count}">
						<input type="hidden" id="affiliation_row_max_count" name="affiliation_row_max_count" value="{$affiliation_row_max_count}">
						<input type="hidden" id="login_emp_id" name="login_emp_id" value="{$login_emp_id}">
						<input type="hidden" id="mode" name="mode">

						<!-- Excel������ -->
						<input type="hidden" id="job_view" name="job_view" value="{$job_view}">
						<input type="hidden" id="level_view" name="level_view" value="{$level_view}">
						<input type="hidden" id="analysis_category_view" name="analysis_category_view" value="{$analysis_category_view}">
						<input type="hidden" id="analysis_items_view" name="analysis_items_view" value="{$analysis_items_view}">
						<input type="hidden" id="year_from_view" name="year_from_view" value="{$year_from_view}">
						<input type="hidden" id="year_to_view" name="year_to_view" value="{$year_to_view}">

						<!-- ������� -->
						<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
							<tr>
								<td width="80" align="left" bgcolor="#E5F6CD">
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font>
								</td>
								<td width="200">
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
										<select id="job" name="job" onchange="selected_text_save(this.id, 'job_view');">{$option_job}</select>
									</font>
								</td>
								<td width="80" align="left" bgcolor="#E5F6CD">
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�������٥�</font>
								</td>
								<td width="*">
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
										<select id="level" name="level" onchange="selected_text_save(this.id, 'level_view');">{$option_level}</select>
									</font>
								</td>
							</tr>
							<tr>
								<!-- TODO �ɲá�����ܥ���ˤĤ��� -->
								<td width="80" align="left" bgcolor="#E5F6CD">
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��°</font>
								</td>
								<td colspan="3">
									{section name=cell loop=$affiliation}
										<div id="affiliation[{$affiliation[cell].rowno}]" style="border: solid 1px #A0D25A;">
											{if $class_cnt >= 4}
												<select id="class_id[{$affiliation[cell].rowno}]" name="class_id[{$affiliation[cell].rowno}]" onchange="selected_text_save(this.id, 'class_id_view[{$affiliation[cell].rowno}]');">
												{$affiliation[cell].option_class}
												</select>
												&nbsp;&gt;&nbsp;
												<select id="atrb_id[{$affiliation[cell].rowno}]" name="atrb_id[{$affiliation[cell].rowno}]" onchange="selected_text_save(this.id, 'atrb_id_view[{$affiliation[cell].rowno}]');">
												{$affiliation[cell].option_atrb}
												</select>
												&nbsp;&gt;&nbsp;
												<select id="dept_id[{$affiliation[cell].rowno}]" name="dept_id[{$affiliation[cell].rowno}]" onchange="selected_text_save(this.id, 'dept_id_view[{$affiliation[cell].rowno}]');">
												{$affiliation[cell].option_dept}
												</select>
												&nbsp;&gt;&nbsp;
												<select id="room_id[{$affiliation[cell].rowno}]" name="room_id[{$affiliation[cell].rowno}]" onchange="selected_text_save(this.id, 'room_id_view[{$affiliation[cell].rowno}]');">
												{$affiliation[cell].option_room}
												</select>
												<input type="hidden" id="class_id_view[{$affiliation[cell].rowno}]" name="class_id_view[{$affiliation[cell].rowno}]" value="{$affiliation[cell].class_id_view}">
												<input type="hidden" id="atrb_id_view[{$affiliation[cell].rowno}]" name="atrb_id_view[{$affiliation[cell].rowno}]" value="{$affiliation[cell].atrb_id_view}">
												<input type="hidden" id="dept_id_view[{$affiliation[cell].rowno}]" name="dept_id_view[{$affiliation[cell].rowno}]" value="{$affiliation[cell].dept_id_view}">
												<input type="hidden" id="room_id_view[{$affiliation[cell].rowno}]" name="room_id_view[{$affiliation[cell].rowno}]" value="{$affiliation[cell].room_id_view}">
											{else}
												<select id="class_id[{$affiliation[cell].rowno}]" name="class_id[{$affiliation[cell].rowno}]" onchange="selected_text_save(this.id, 'class_id_view[{$affiliation[cell].rowno}]');">
												{$affiliation[cell].option_class}
												</select>
												&nbsp;&gt;&nbsp;
												<select id="atrb_id[{$affiliation[cell].rowno}]" name="atrb_id[{$affiliation[cell].rowno}]" onchange="selected_text_save(this.id, 'atrb_id_view[{$affiliation[cell].rowno}]');">
												{$affiliation[cell].option_atrb}
												</select>
												&nbsp;&gt;&nbsp;
												<select id="dept_id[{$affiliation[cell].rowno}]" name="dept_id[{$affiliation[cell].rowno}]" onchange="selected_text_save(this.id, 'dept_id_view[{$affiliation[cell].rowno}]');">
												{$affiliation[cell].option_dept}
												</select>
												<input type="hidden" id="class_id_view[{$affiliation[cell].rowno}]" name="class_id_view[{$affiliation[cell].rowno}]" value="{$affiliation[cell].class_id_view}">
												<input type="hidden" id="atrb_id_view[{$affiliation[cell].rowno}]" name="atrb_id_view[{$affiliation[cell].rowno}]" value="{$affiliation[cell].atrb_id_view}">
												<input type="hidden" id="dept_id_view[{$affiliation[cell].rowno}]" name="dept_id_view[{$affiliation[cell].rowno}]" value="{$affiliation[cell].dept_id_view}">
											{/if}
											<input type="button" value="�ɲ�" onclick="add_affiliation();">
											{if $affiliation[cell].rowno > 0}
											<input type="button" value="���" onclick="del_affiliation(document.getElementById('affiliation[{$affiliation[cell].rowno}]'));">
											{/if}
										</div>
									{/section}
								</td>
							</tr>
							<tr>
								<td width="80" align="left" bgcolor="#E5F6CD">
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ʬ�Ϲ���</font>
								</td>
								<td colspan="3">
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
										<select id="analysis_category" name="analysis_category" onchange="selected_text_save(this.id, 'analysis_category_view');">{$option_analysis_category}</select>
										{if $item_display}
										<select id="analysis_items" name="analysis_items" onchange="selected_text_save(this.id, 'analysis_items_view');">{$option_analysis_items}</select>
										{/if}
										{if $year_from_display}
										<select id="year_from" name="year_from" onchange="selected_text_save(this.id, 'year_from_view');">{$option_year_from}</select>ǯ��
										{/if}
										{if $year_to_display}
										&nbsp;��&nbsp;
										<select id="year_to" name="year_to" onchange="selected_text_save(this.id, 'year_to_view');">{$option_year_to}</select>ǯ��
										{/if}
										<input type="button" value="ʬ��" onclick="out_analysis();">
										<input type="button" value="����" onclick="self.print();">
										<input type="button" value="EXCEL����" onclick="out_excel();">
										<input type="button" value="�����" onclick="out_graph();">
									</font>
								</td>
							</tr>
						</table>

						<!-- ����ɽ�� -->
						<img src="img/spacer.gif" alt="" width="1" height="5">
						<br>
						<table border="0" cellspacing="0" cellpadding="1" class="list">
							{section name=row loop=$data}
							<tr>
								{section name=cell loop=$data[row]}
								<td
									 {if $data[row][cell].colspan != ''}colspan="{$data[row][cell].colspan}"{/if}
									 {if $data[row][cell].rowspan != ''}rowspan="{$data[row][cell].rowspan}"{/if}
									 {if $data[row][cell].bolheader}
										bgcolor="#E5F6CD"
									 {/if}
									 {if $data[row][cell].bolemplist}
										onmouseover="this.style.backgroundColor='#ffff66';this.style.cursor='pointer';"
										onmouseout="this.style.backgroundColor='';this.style.cursor = '';"
										onclick="show_emp_list('{$data[row][cell].row_1_key}','{$data[row][cell].row_2_key}','{$data[row][cell].column_1_key}','{$data[row][cell].column_2_key}');"
									 {/if}
									 align="{$data[row][cell].align}"
									 width="120"
								>
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
										{$data[row][cell].value}
									</font>
								</td>
								{/section}
							</tr>
							{/section}
							<!-- ���� -->
						</table>
					</form>
				</td>
			</tr>
		</table>
		<img src="img/spacer.gif" alt="" width="1" height="5">
		<br>

		<!-- Excel�����ѥե����� -->
		<iframe id="download" name="download" width="0" height="0" frameborder="0"></iframe>

		{* ############################################################################################################################################################ *}
		{* �Ҳ��̥եå��� *}
		{* ############################################################################################################################################################ *}
		{include file='cl_sub_screen_footer.tpl'}

	</body>
</html>