<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<title>CoMedix {$cl_title} | �����ֺ´���</title>
	<script type="text/javascript" src="js/fontsize.js"></script>
	<script type="text/javascript" src="js/cl_training_insert.js"></script>
	{$yui_cal_part}
	{literal}
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<style type="text/css">
		.list {border-collapse:collapse;}
		.list td {border:#A0D25A solid 1px;}

		table.block_in {border-collapse:collapse;}
		table.block_in td {border:#A0D25A solid 0px;}
	</style>
	{/literal}
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr height="32" bgcolor="#35B341">
			<td class="spacing"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j14" color="#ffffff"><b>�����ֺ´���(������Ͽ)</b></font></td>
			<td>&nbsp;</td>
			<td width="10">&nbsp;</td>
			<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="�Ĥ���" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<img src="img/spacer.gif" width="1" height="10" alt=""><br>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<form id="training_form" name="training_form" action="#" method="post">
					<input type="hidden" id="session" name="session" value="{$session}">
					<input type="hidden" name="insertflg" id="insertflg" value="{$insertflg}">
					<input type="hidden" id="input_div" name="input_div">
					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td width="*" align="right">
								<input type="button" value="��Ͽ" onclick="training_insert();">
								&nbsp;&nbsp;
							</td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����̾</font></td>
							<td width="120px"><input type="text" id="training_name" name="training_name" size="50"></td>
							<td width="90px" align="right">
								<input type="checkbox" id="abolition_flag" name="abolition_flag" value="t">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ѻ߶�ʬ</font>
							</td>
							<!-- 2012/07/25 Yamagawa add(s) -->
							<td width="150px" align="right">
								<input type="checkbox" id="auto_control_flg" name="auto_control_flg" value="t" checked>
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�������ռ�ư����</font>
							</td>
							<!-- 2012/11/19 Yamagawa add(s) -->
							<td width="130px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��������</font>
								<select id="term_div" name="term_div">{$term_div_options_str}</select>
							</td>
							<!-- 2012/11/19 Yamagawa add(e) -->
							<td width="*"></td>
							<!-- 2012/07/25 Yamagawa add(e) -->
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��Ū</font></td>
							<td><textarea id="training_purpose" name="training_purpose" rows="5" cols="100"></textarea></td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td width="120px" align="right">
								<a href="javascript:void(0)" onclick="clearEmployee('training_opener')">
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��ż�</font>
								</a>
							</td>
							<td width="210px">
								<input type="text" id="training_opener_nm" name="training_opener_nm" readOnly>
								<input type="hidden" id="training_opener_id" name="training_opener_id">
								<input type="button" onclick="openEmployeeList('training_opener')" value="����̾��">
							</td>
							<td width="40px" align="right">
								<a href="javascript:void(0)" onclick="clearEmployee('training_teacher1')">
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ֻգ�</font>
								</a>
							</td>
							<td width="210px">
								<input type="text" id="training_teacher1_nm" name="training_teacher1_nm" readOnly>
								<input type="hidden" id="training_teacher1_id" name="training_teacher1_id">
								<input type="button" onclick="openEmployeeList('training_teacher1')" value="����̾��">
							</td>
							<td width="40px" align="right">
								<a href="javascript:void(0)" onclick="clearEmployee('training_teacher4')">
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ֻգ�</font>
								</a>
							</td>
							<td width="210px">
								<input type="text" id="training_teacher4_nm" name="training_teacher4_nm" readOnly>
								<input type="hidden" id="training_teacher4_id" name="training_teacher4_id">
								<input type="button" onclick="openEmployeeList('training_teacher4')" value="����̾��">
							</td>
							<td width="*"></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td align="right">
								<a href="javascript:void(0)" onclick="clearEmployee('training_teacher2')">
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ֻգ�</font>
								</a>
							</td>
							<td>
								<input type="text" id="training_teacher2_nm" name="training_teacher2_nm" readOnly>
								<input type="hidden" id="training_teacher2_id" name="training_teacher2_id">
								<input type="button" onclick="openEmployeeList('training_teacher2')" value="����̾��">
							</td>
							<td align="right">
								<a href="javascript:void(0)" onclick="clearEmployee('training_teacher5')">
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ֻգ�</font>
								</a>
							</td>
							<td>
								<input type="text" id="training_teacher5_nm" name="training_teacher5_nm" readOnly>
								<input type="hidden" id="training_teacher5_id" name="training_teacher5_id">
								<input type="button" onclick="openEmployeeList('training_teacher5')" value="����̾��">
							</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td align="right">
								<a href="javascript:void(0)" onclick="clearEmployee('training_teacher3')">
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ֻգ�</font>
								</a>
							</td>
							<td>
								<input type="text" id="training_teacher3_nm" name="training_teacher3_nm" readOnly>
								<input type="hidden" id="training_teacher3_id" name="training_teacher3_id">
								<input type="button"onclick="openEmployeeList('training_teacher3')" value="����̾��">
							</td>
							<td></td>
							<td></td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�оݼ�</font></td>
							<td width="50px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��٥�I</font></td>
							<td width="50px"><select id="target_level1" name="target_level1">{$level1_options_str}</select></td>
							<td width="50px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��٥�II</font></td>
							<td width="50px"><select id="target_level1" name="target_level2">{$level2_options_str}</select></td>
							<td width="50px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��٥�III</font></td>
							<td width="50px"><select id="target_level3" name="target_level3">{$level3_options_str}</select></td>
							<td width="50px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��٥�IV</font></td>
							<td width="50px"><select id="target_level4" name="target_level4">{$level4_options_str}</select></td>
							<td width="50px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��٥�V</font></td>
							<td width="50px"><select id="target_level5" name="target_level5">{$level5_options_str}</select></td>
							<td width="*"></td>
						</tr>
					</table>
					<img src="img/spacer.gif" width="1" height="20" alt=""><br>

					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���Ȼ��ֶ�ʬ</font></td>
							<td width="80px"><select id="time_division" name="time_division">{$time_division_options_str}</select></td>
							<td width="60px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�������</font></td>
							<td><input type="text" id="max_training_time" name="max_training_time" maxlength="2" style="ime-mode:disabled;text-align:right;" size="2"></td>
							<td width="*"></td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�����׷�</font></td>
							<td>
								<textarea id="training_require" name="training_require" cols="100" rows="5"></textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��ɸ</font></td>
							<td>
								<textarea id="training_slogan" name="training_slogan" cols="100" rows="5"></textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
							<td>
								<textarea id="training_contents" name="training_contents" cols="100" rows="5"></textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����Ǥθ�����<br>�μ���Ȥ�</font></td>
							<td>
								<textarea id="training_action" name="training_action" cols="100" rows="5"></textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">OJT�Ǥλٱ�����</font></td>
							<td>
								<textarea id="training_support" name="training_support" cols="100" rows="5"></textarea>
							</td>
						</tr>
					</table>

					<img src="img/spacer.gif" width="1" height="20" alt=""><br>

					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><b>�������������</b></font>
							</td>
							<td></td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[����]</font>
							</td>
							<td>
								<textarea id="theme_contents_before" name="theme_contents_before" rows="5" cols="100"></textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[�����ˡ��]</font>
							</td>
							<td>
								<textarea id="theme_method_before" name="theme_method_before" rows="5" cols="100"></textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[��д���]</font>
							</td>
							<td>
								<input type="text" id="theme_deadline_before" name="theme_deadline_before">
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<a href="javascript:void(0)" onclick="clearEmployee('theme_present_before')">
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[�����]</font>
								</a>
							</td>
							<td>
								<input type="text" id="theme_present_before_nm" name="theme_present_before_nm" readOnly>
								<input type="hidden" id="theme_present_before_id" name="theme_present_before_id">
								<input type="button" onclick="openEmployeeList('theme_present_before')" value="����̾��">
							</td>
						</tr>
						<tr>
							<td>
								<img src="img/spacer.gif" width="1" height="10" alt=""><br>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><b>������������</b></font>
							</td>
							<td></td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[����]</font>
							</td>
							<td>
								<textarea id="theme_contents_now" name="theme_contents_now" rows="5" cols="100"></textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[�����ˡ��]</font>
							</td>
							<td>
								<textarea id="theme_method_now" name="theme_method_now" rows="5" cols="100"></textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[��д���]</font>
							</td>
							<td>
								<input type="text" id="theme_deadline_now" name="theme_deadline_now">
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<a href="javascript:void(0)" onclick="clearEmployee('theme_present_now')">
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[�����]</font>
								</a>
							</td>
							<td>
								<input type="text" id="theme_present_now_nm" name="theme_present_now_nm" readOnly>
								<input type="hidden" id="theme_present_now_id" name="theme_present_now_id">
								<input type="button" onclick="openEmployeeList('theme_present_now')" value="����̾��">
							</td>
						</tr>
						<tr>
							<td>
								<img src="img/spacer.gif" width="1" height="10" alt=""><br>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><b>��������ʸ��</b></font>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[����]</font>
							</td>
							<td>
								<textarea id="theme_contents_after" name="theme_contents_after" rows="5" cols="100"></textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[�����ˡ��]</font>
							</td>
							<td>
								<textarea id="theme_method_after" name="theme_method_after" rows="5" cols="100"></textarea>
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[��д���]</font>
							</td>
							<td>
								<input type="text" id="theme_deadline_after" name="theme_deadline_after">
							</td>
						</tr>
						<tr>
							<td width="120px" align="right">
								<a href="javascript:void(0)" onclick="clearEmployee('theme_present_after')">
									<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">[�����]</font>
								</a>
							</td>
							<td>
								<input type="text" id="theme_present_after_nm" name="theme_present_after_nm" readOnly>
								<input type="hidden" id="theme_present_after_id" name="theme_present_after_id">
								<input type="button" onclick="openEmployeeList('theme_present_after')" value="����̾��">
							</td>
						</tr>
						<tr>
							<td colspan="2" align="right">
								<input type="button" value="��Ͽ" onclick="training_insert();">
								&nbsp;&nbsp;
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table>
	<img src="img/spacer.gif" alt="" width="1" height="5"><br>

</body>
</html>