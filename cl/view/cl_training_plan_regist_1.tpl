{literal}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<title>CoMedix ����˥������� | {/literal}{$title}{literal}</title>
	<script type="text/javascript" src="js/fontsize.js"></script>
	<script type="text/javascript" src="js/cl_training_insert.js"></script>
	<script type="text/javascript" src="js/yui_0.12.2/build/yahoo/yahoo-min.js"></script>
	<script type="text/javascript" src="js/yui_0.12.2/build/event/event-min.js" ></script>
	<script type="text/javascript" src="js/yui_0.12.2/build/dom/dom-min.js" ></script>
	<script type="text/javascript" src="js/yui_0.12.2/build/calendar/calendar-min.js"></script>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/cl_common.js"></script>

	<link type="text/css" rel="stylesheet" href="js/yui_0.12.2/build/calendar/assets/calendar.css">

	<link rel="stylesheet" type="text/css" href="css/main.css">
	<style type="text/css">
		.list {border-collapse:collapse;}
		.list td {border:#A0D25A solid 1px;}

		table.block_in {border-collapse:collapse;}
		table.block_in td {border:#A0D25A solid 0px;}
	</style>

	<script type="text/javascript" src="cl/common/cl_training_plan_regist_1.js"></script>

</head>
{/literal}

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="checkMode();">

	{* ############################################################################################################################################################ *}
	{* �Ҳ��̥إå��� *}
	{* ############################################################################################################################################################ *}
	{include file='cl_sub_screen_header.tpl'}

	<img src="img/spacer.gif" width="1" height="10" alt=""><br>

	<table width="100%" border="0" cellspacing="5" cellpadding="0">
		<tr>

			<td>
				<form id="form" name="form" action="#" method="post">

					{* ############################################################################################################################################ *}
					{* ���������� *}
					{* ############################################################################################################################################ *}

					<table width="780" border="0" cellspacing="0" cellpadding="1" class="list">

						{* ######################################################################################################################################## *}
						{* �����������إå��� *}
						{* ######################################################################################################################################## *}

						{* ######################################################################################################################################## *}
						{* ǯ�� *}
						{* ######################################################################################################################################## *}
						<tr>
							<td bgcolor="#E5F6CD" width="100"  align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ǯ��         </font></td>
							<td                   width="500"   align="left">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
									{if $mode eq "3"}
										<input type="text" id="year" name="value" readOnly style="border: none">
									{else}
										<select id="year" name="year" >
										{section name=idx loop=$nendo_ary}
											<option value="{$nendo_ary[idx]}" {if $target_year == $nendo_ary[idx]} selected{/if} >{$nendo_ary[idx]}</option>
										{/section}
										</select>
									{/if}
								</font>
							</td>
						</tr>
						<!-- 2012/08/27 Yamagawa add(s) -->
						{if $mode eq "3"}
							<input type="hidden" id="term_div" name="term_div">
							<input type="hidden" id="training_time" name="training_time">
							<input type="hidden" id="plan_date_yyyy" name="plan_date_yyyy">
							<input type="hidden" id="plan_date_mm" name="plan_date_mm">
							<input type="hidden" id="plan_date_dd" name="plan_date_dd">
							<input type="hidden" id="from_time_hh" name="from_time_hh">
							<input type="hidden" id="from_time_mi" name="from_time_mi">
							<input type="hidden" id="to_time_hh" name="to_time_hh">
							<input type="hidden" id="to_time_mi" name="to_time_mi">
							<input type="hidden" id="max_people" name="max_people">
							<input type="hidden" id="place" name="place">
						{else}
						<!-- 2012/08/27 Yamagawa add(e) -->

						{* ######################################################################################################################################## *}
						{* ���Ų�� *}
						{* ######################################################################################################################################## *}
						<tr>
							<td bgcolor="#E5F6CD" width="100"  align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���Ų��     </font></td>
							<td                   width="500"   align="left">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
									<select id="training_time" name="training_time">
										{$option_training_time}
									</select>
								</font>
							</td>
						</tr>

						{* ######################################################################################################################################## *}
						{* �������� *}
						{* ######################################################################################################################################## *}
						<tr>
							<td bgcolor="#E5F6CD" width="100"  align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��������     </font></td>
							<td                   width="500"  align="left">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
									<select id="plan_date_yyyy" name="plan_date_yyyy">
										{$option_date_yyyy}
									</select>ǯ
								</font>
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
									<select id="plan_date_mm" name="plan_date_mm">
										{$option_date_mm}
									</select>��
								</font>
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
									<select id="plan_date_dd" name="plan_date_dd">
										{$option_date_dd}
									</select>��
								</font>
								<img id="calplan"src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_calplan()"/>
								<font size='3' face='�ͣ� �Х����å�, Osaka' class='j12'>
									<div id="calplanContainer" style="position:absolute;display:none;z-index:10000;"></div>
								</font>
								&nbsp;&nbsp;&nbsp;&nbsp;

						{* ######################################################################################################################################## *}
						{* ���Ż��� *}
						{* ######################################################################################################################################## *}
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
									<select id="from_time_hh" name="from_time_hh" >
									{section name=idx loop=$h_ary}
										<option value="{$h_ary[idx]}" {if $h_ary[idx]=="00" } selected{/if} >{$h_ary[idx]}</option>
									{/section}
									</select>��
								</font>

								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
									<select id="from_time_mi" name="from_time_mi" >
									{section name=idx loop=$m_ary}
										<option value="{$m_ary[idx]}" {if $m_ary[idx]=="00" } selected{/if} >{$m_ary[idx]}</option>
									{/section}
									</select>ʬ
								</font>
								&nbsp;&nbsp;-&nbsp;&nbsp;
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
									<select id="to_time_hh" name="to_time_hh" >
									{section name=idx loop=$h_ary}
										<option value="{$h_ary[idx]}" {if $h_ary[idx]=="00" } selected{/if} >{$h_ary[idx]}</option>
									{/section}
									</select>��
								</font>

								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
									<select id="to_time_mi" name="to_time_mi" >
									{section name=idx loop=$m_ary}
										<option value="{$m_ary[idx]}" {if $m_ary[idx]=="00" } selected{/if} >{$m_ary[idx]}</option>
									{/section}
									</select>ʬ
								</font>

							</td>
						</tr>

						{* ######################################################################################################################################## *}
						{* ���̵ͭ *}
						{* ######################################################################################################################################## *}
<!--
						<tr>
							<td bgcolor="#E5F6CD" width="100"  align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���̵ͭ</font></td>
							<td                   width="500"  align="left" >
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
									<input name="report_division" type="radio" value="1" checked>����&nbsp;<input name="report_division" type="radio" value="0" >�ʤ�
								</font>
							</td>
						</tr>
-->
						{* ######################################################################################################################################## *}
						{* ���󥱡���̵ͭ *}
						{* ######################################################################################################################################## *}
<!--
						<tr>
							<td bgcolor="#E5F6CD" width="100"  align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���󥱡���̵ͭ</font></td>
							<td                   width="500"  align="left">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
									<input name="answer_division" type="radio" value="1" checked>����&nbsp;<input name="answer_division" type="radio" value="0" >�ʤ�
								</font>
							</td>
						</tr>
-->
						{* ######################################################################################################################################## *}
						{* ����Ϳ� *}
						{* ######################################################################################################################################## *}
						<tr>
							<td bgcolor="#E5F6CD" width="100"  align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����Ϳ�</font></td>
							<td                   width="500"  align="left">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
									<input name="max_people" type="text" value="" size="2" style="ime-mode:disabled;">&nbsp;��
								</font>
							</td>
						</tr>

						{* ######################################################################################################################################## *}
						{* ��� *}
						{* ######################################################################################################################################## *}
						<tr>
							<td bgcolor="#E5F6CD" width="100"  align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���     </font></td>
							<td                   width="500"   align="left">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
									<input name="place" type="text" value="" size="50">
								</font>
							</td>
						</tr>
						<!-- 2012/08/27 Yamagawa add(s) -->
						{/if}
						<!-- 2012/08/27 Yamagawa add(e) -->

						{* ######################################################################################################################################## *}
						{* �������� *}
						{* ######################################################################################################################################## *}
						<tr>
							<td bgcolor="#E5F6CD" width="100"  align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��������     </font></td>
							<td                   width="500"   align="left">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
									{if $auto_control_flg eq "t"}
									��ư������
									<input type="hidden" id="apply_status" name="apply_status" value="0">
									{else}
									<select id="apply_status" name="apply_status">
										<option value="0" selected>������  </option>
										<option value="1"         >������  </option>
										<option value="2"         >���մ�λ</option>
										<option value="3"         >���    </option>
										<option value="4"         >�����ѹ�</option>
									</select>
									{/if}
								</font>
							</td>
							<!-- ������=1����λ=2�����=3�������ѹ�=4			 -->
						</tr>

						{* ######################################################################################################################################## *}
						{* ���� *}
						{* ######################################################################################################################################## *}
						<tr>
							<td bgcolor="#E5F6CD" width="100" align="right"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����         </font></td>
							<td                   width="500"  align="left">
								<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
									<textarea name="remarks" rows="5" cols="60"></textarea>
								</font>
							</td>
						</tr>

					</table>

					{* ############################################################################################################################################ *}
					{* ��Ͽ�������ܥ��� *}
					{* ############################################################################################################################################ *}
					<table width="780" border="0" cellspacing="0" cellpadding="1" >
						<tr>
						{if $mode eq "1"}
							<td align="right"><input name="" type="button" value="��Ͽ" onClick="regist();"></td>
						{else}
							<td align="right"><input name="" type="button" value="����" onClick="update();"></td>
						{/if}
						</tr>
					</table>

					{* ############################################################################################################################################ *}
					{* �������� *}
					{* ############################################################################################################################################ *}
					<input type="hidden" id="training_id"       name="training_id"       value="{$training_id}" >
					<input type="hidden" id="login_user"        name="login_user"        value="{$login_user}" >
					<input type="hidden" id="mode"              name="mode"              value="{$mode}" >
					<input type="hidden" id="plan_id"           name="plan_id"           value="{$plan_id}" >
					<input type="hidden" id="max_training_time" name="max_training_time" value="{$max_training_time}" >
					<input type="hidden" id="first_plan_id"     name="first_plan_id"     >
					<!-- 2012/11/19 Yamagawa add(s) -->
					<input type="hidden" id="target_year"       name="target_year"       value="{$target_year}">
					<!-- 2012/11/19 Yamagawa add(e) -->

				</form>
			</td>

		</tr>
	</table>
	<img src="img/spacer.gif" alt="" width="1" height="5"><br>

	{* ############################################################################################################################################################ *}
	{* �Ҳ��̥եå��� *}
	{* ############################################################################################################################################################ *}
	{include file='cl_sub_screen_footer.tpl'}
	<!-- 2012/11/19 Yamagawa add(s) -->
	{if $mode eq "3"}
	{else}
	<!-- 2012/11/19 Yamagawa add(e) -->
		<script type="text/javascript">
			YAHOO.namespace("ui.calendar");
			{$calendar}
		</script>
	<!-- 2012/11/19 Yamagawa add(s) -->
	{/if}
	<!-- 2012/11/19 Yamagawa add(e) -->
</body>
</html>
