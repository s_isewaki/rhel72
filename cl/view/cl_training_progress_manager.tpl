<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<title>CoMedix {$cl_title} | �����ֺ´���</title>
	<script type="text/javascript" src="js/fontsize.js"></script>
	<script type="text/javascript" src="js/cl_training_progress_manager.js"></script>
	{$yui_cal_part}
	{literal}
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<style type="text/css">
		.list {border-collapse:collapse;}
		.list td {border:#A0D25A solid 1px;}

		table.block_in {border-collapse:collapse;}
		table.block_in td {border:#A0D25A solid 0px;}
	</style>
	{/literal}
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr bgcolor="#f6f9ff">
						<td width="32" height="32" class="spacing"><a href="ladder_menu.php?session={$session}"><img src="img/icon/s42.gif" width="32" height="32" border="0" alt="{$cl_title}"></a></td>
						<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j16">&nbsp;<a href="ladder_menu.php?session={$session}"><b>{$cl_title}</b></a> &gt; <a href="cl_training_progress_manager.php?session={$session}"><b>�������ֿ�Ľ(������)</b></a></font></td>

						<!-- �������̥��ɽ�� -->
						{if $workflow_auth == "1"}
						<td align="right" style="padding-right:6px;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j16">&nbsp;<a href="cl_workflow_menu.php?session={$session}&workflow=1"><b>�������̤�</b></a></font></td>
						{else}
						{/if}

					</tr>
				</table>

				<!-- ���֥�˥塼ɽ�� -->
				{$aplyctn_mnitm_str}

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
					</tr>
				</table>

				<img src="img/spacer.gif" alt="" width="1" height="2"><br>

				<form id="training_form" name="training_form" action="#" method="post">
					<input type="hidden" id="session" name="session" value="{$session}">
					<input type="hidden" id="training_id" name="training_id">
					<input type="hidden" id="user_csv_flg" name="user_csv_flg">
					<input type="hidden" id="training_csv_flg" name="training_csv_flg">
					<!-- 2012/07/04 Yamagawa add(s) -->
					<input type="hidden" id="research_flg" name="research_flg">
					<!-- 2012/07/04 Yamagawa add(e) -->
					<img src="img/spacer.gif" alt="" width="1" height="2"><br>

					<!-- ������� -->
					<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
						<tr>
							<td width="80" align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�������٥�</font></td>
							<td width="90"><select id="now_level" name="now_level">{$option_level}</select></td>
							<td width="80" align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">������̾</font></td>
							<td><input type="text" id="emp_nm" name="emp_nm" value="{$emp_nm}" size="20"></td>
							<td width="80" align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�࿦��ɽ��</font></td>
							<td colspan="3" align="left"><input type="checkbox" id="retire_disp_flg" name="retire_disp_flg" value="t" {$retire_disp_flg}></td>
						</tr>
						<tr>
							<td align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��°</font></td>
							<td colspan="7">{$pst_slct_bx_strct_str}</td>
						</tr>
						<tr>

							<td align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">������ʬ</font></td>
							<td ><select id="training_div" name="training_div">{$training_div_options_str}</select></td>
							<td width="80" align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�����ֹ�</font></td>
							<td width="140"><input type="text" id="training_id" name="training_id" value="{$training_id}"></td>
							<td width="80" align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����̾</font></td>
							<td width="250"><input type="text" id="training_name" name="training_name" value="{$training_name}" size="40"></td>
							<td width="60" align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ֻ�</font></td>
							<td><input type="text" id="teacher_name" name="teacher_name" value="{$teacher_name}" size="20"></td>
						</tr>
						<tr>
							<td align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">������</font></td>
							<td align="left" colspan="7">
								<table border="0" cellspacing="0" cellpadding="0" class="block_in">
									<tr>
										<td>
											<select id="date_apply_from_y" name="date_apply_from_y">{$option_date_apply_from_y}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_apply_from_m" name="date_apply_from_m">{$option_date_apply_from_m}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_apply_from_d" name="date_apply_from_d">{$option_date_apply_from_d}</select>
										</td>
										<td>
											<img id="cal_apply_from" src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal_apply_from();"/>
											<font size='3' face='�ͣ� �Х����å�, Osaka' class='j12'>
												<div id="cal_apply_fromContainer" style="position:absolute;display:none;z-index:10000;"></div>
											</font>
										</td>
										<td>
											&nbsp;<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��</font>&nbsp;
											<select id="date_apply_to_y" name="date_apply_to_y">{$option_date_apply_to_y}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_apply_to_m" name="date_apply_to_m">{$option_date_apply_to_m}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_apply_to_d" name="date_apply_to_d">{$option_date_apply_to_d}</select>
										</td>
										<td>
											<img id="cal_apply_to" src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal_apply_to()"/>
											<font size='3' face='�ͣ� �Х����å�, Osaka' class='j12'>
												<div id="cal_apply_toContainer" style="position:absolute;display:none;z-index:10000;"></div>
											</font>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">������</font></td>
							<td align="left" colspan="7">
								<table border="0" cellspacing="0" cellpadding="0" class="block_in">
									<tr>
										<td>
											<select id="date_students_from_y" name="date_students_from_y">{$option_date_students_from_y}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_students_from_m" name="date_students_from_m">{$option_date_students_from_m}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_students_from_d" name="date_students_from_d">{$option_date_students_from_d}</select>
										</td>
										<td>
											<img id="cal_students_from" src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal_students_from();"/>
											<font size='3' face='�ͣ� �Х����å�, Osaka' class='j12'>
												<div id="cal_students_fromContainer" style="position:absolute;display:none;z-index:10000;"></div>
											</font>
										</td>
										<td>
											&nbsp;<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��</font>&nbsp;
											<select id="date_students_to_y" name="date_students_to_y">{$option_date_students_to_y}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_students_to_m" name="date_students_to_m">{$option_date_students_to_m}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_students_to_d" name="date_students_to_d">{$option_date_students_to_d}</select>
										</td>
										<td>
											<img id="cal_students_to" src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal_students_to()"/>
											<font size='3' face='�ͣ� �Х����å�, Osaka' class='j12'>
												<div id="cal_students_toContainer" style="position:absolute;display:none;z-index:10000;"></div>
											</font>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr style="display:none">
							<td align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ɾ��ͽ����</font></td>
							<td align="left" colspan="7">
								<table border="0" cellspacing="0" cellpadding="0" class="block_in">
									<tr>
										<td>
											<select id="date_eval_plan_from_y" name="date_eval_plan_from_y">{$option_date_eval_plan_from_y}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_eval_plan_from_m" name="date_eval_plan_from_m">{$option_date_eval_plan_from_m}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_eval_plan_from_d" name="date_eval_plan_from_d">{$option_date_eval_plan_from_d}</select>
										</td>
										<td>
											<img id="cal_eval_plan_from" src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal_eval_plan_from();"/>
											<font size='3' face='�ͣ� �Х����å�, Osaka' class='j12'>
												<div id="cal_eval_plan_fromContainer" style="position:absolute;display:none;z-index:10000;"></div>
											</font>
										</td>
										<td>
											&nbsp;<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��</font>&nbsp;
											<select id="date_eval_plan_to_y" name="date_eval_plan_to_y">{$option_date_eval_plan_to_y}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_eval_plan_to_m" name="date_eval_plan_to_m">{$option_date_eval_plan_to_m}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_eval_plan_to_d" name="date_eval_plan_to_d">{$option_date_eval_plan_to_d}</select>
										</td>
										<td>
											<img id="cal_eval_plan_to" src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal_eval_plan_to()"/>
											<font size='3' face='�ͣ� �Х����å�, Osaka' class='j12'>
												<div id="cal_eval_plan_toContainer" style="position:absolute;display:none;z-index:10000;"></div>
											</font>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�����</font></td>
							<td align="left" colspan="6">
								<table border="0" cellspacing="0" cellpadding="0" class="block_in">
									<tr>
										<td>
											<select id="date_report_from_y" name="date_report_from_y">{$option_date_report_from_y}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_report_from_m" name="date_report_from_m">{$option_date_report_from_m}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_report_from_d" name="date_report_from_d">{$option_date_report_from_d}</select>
										</td>
										<td>
											<img id="cal_report_from" src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal_report_from();"/>
											<font size='3' face='�ͣ� �Х����å�, Osaka' class='j12'>
												<div id="cal_report_fromContainer" style="position:absolute;display:none;z-index:10000;"></div>
											</font>
										</td>
										<td>
											&nbsp;<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��</font>&nbsp;
											<select id="date_report_to_y" name="date_report_to_y">{$option_date_report_to_y}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_report_to_m" name="date_report_to_m">{$option_date_report_to_m}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_report_to_d" name="date_report_to_d">{$option_date_report_to_d}</select>
										</td>
										<td>
											<img id="cal_report_to" src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal_report_to()"/>
											<font size='3' face='�ͣ� �Х����å�, Osaka' class='j12'>
												<div id="cal_report_toContainer" style="position:absolute;display:none;z-index:10000;"></div>
											</font>
										</td>
									</tr>
								</table>
							</td>
							<td align="right"><input type="button" value="����" onclick="research();"></td>
						</tr>

						<!--
						<tr style="display:none">
							<td align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ɾ����</font></td>
							<td align="left" colspan="7">
								<table border="0" cellspacing="0" cellpadding="0" class="block_in">
									<tr>
										<td>
											<select id="date_eval_from_y" name="date_eval_from_y">{$option_date_eval_from_y}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_eval_from_m" name="date_eval_from_m">{$option_date_eval_from_m}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_eval_from_d" name="date_eval_from_d">{$option_date_eval_from_d}</select>
										</td>
										<td>
											<img id="cal_eval_from" src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal_eval_from();"/>
											<font size='3' face='�ͣ� �Х����å�, Osaka' class='j12'>
												<div id="cal_eval_fromContainer" style="position:absolute;display:none;z-index:10000;"></div>
											</font>
										</td>
										<td>
											&nbsp;<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��</font>&nbsp;
											<select id="date_eval_to_y" name="date_eval_to_y">{$option_date_eval_to_y}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_eval_to_m" name="date_eval_to_m">{$option_date_eval_to_m}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_eval_to_d" name="date_eval_to_d">{$option_date_eval_to_d}</select>
										</td>
										<td>
											<img id="cal_eval_to" src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal_eval_to()"/>
											<font size='3' face='�ͣ� �Х����å�, Osaka' class='j12'>
												<div id="cal_eval_toContainer" style="position:absolute;display:none;z-index:10000;"></div>
											</font>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						-->


					</table>

					<img src="img/spacer.gif" alt="" width="1" height="1">

					<!-- ��Ͽ������ܥ��� -->
					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td align="right">
								<input type="button" value="̾������" onclick="output_to_csv(1);">
								<input type="button" value="�ãӣֽ���" onclick="output_to_csv(2);">
							</td>
						</tr>
					</table>

					<!-- ����ɽ�� -->
					<img src="img/spacer.gif" alt="" width="1" height="5"><br>
					<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
						<!-- �إå��� -->
						<tr bgcolor="#E5F6CD">
							<td align="center" width="85" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����ID</font></td>
							<td align="left" width="70" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��̾</font></td>
							<td align="left" width="*"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��°</font></td>
							<td align="center" width="20" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">Lv</font></td>
							<td align="center" width="100" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�����ֹ�</font></td>
							<td align="left" width="120" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����̾</font></td>
							<td align="left" width="70" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�ֻ�̾</font></td>
							<td align="center" width="70" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">������</font></td>
							<td align="center" width="70" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">������</font></td>
							<td align="center" width="70" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�����</font></td>
						</tr>
						<!-- ���� -->
						{section name=cell loop=$data}
						<tr onmouseover="this.style.cursor='pointer'" onmouseout="this.style.cursor=''" onclick="show_detail('{$data[cell].emp_id}','{$data[cell].year}');">
							<td align="center" width="85" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].emp_personal_id}</font></td>
							<td align="left" width="70" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].emp_full_nm}</font></td>
							<td align="left" width="*"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].affiliation}</font></td>
							<td align="center" width="20" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].now_level}</font></td>
							<td align="center" width="100" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].training_id}</font></td>
							<td align="left" width="120" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].training_name}</font></td>
							<td align="left" width="70" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].teacher_name}</font></td>
							<td align="center" width="70" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].apply_date}</font></td>
							<td align="center" width="70" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].plan_date}</font></td>
							<td align="center" width="70" style="word-break:break-all;word-wrap:break-word;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].report_date}</font></td>
						</tr>
						{/section}
					</table>
				</form>
			</td>
		</tr>
	</table>
	<img src="img/spacer.gif" alt="" width="1" height="5"><br>

	{* ############################################################################################################################################################ *}
	{* �Ҳ��̥եå��� *}
	{* ############################################################################################################################################################ *}
	{include file='cl_sub_screen_footer.tpl'}

	<script type="text/javascript">
		YAHOO.namespace("ui.calendar");
		{$calendar}
	</script>

</body>
</html>