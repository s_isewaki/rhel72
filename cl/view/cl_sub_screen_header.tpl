{* ################################################################################################################################################################ *}
{* 子画面ヘッダー *}
{* $title：インクルード元画面にて定義する *}
{* ################################################################################################################################################################ *}

{literal}
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr height="32" bgcolor="#35B341">
			<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>{/literal}{$title}{literal}</b></font></td>

			<td>&nbsp;</td>
			<td width="10">&nbsp;</td>
			<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>
{/literal}
