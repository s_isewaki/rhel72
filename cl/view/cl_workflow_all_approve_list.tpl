<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix {$cl_title} | 受付一覧</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/cl_application_approve_list.js"></script>
{$yui_cal_part}
{literal}
<script type="text/javascript">
<!--

function cateOnChange() {

	var obj_cate = document.approve.category;
	var obj_wkfw = document.approve.workflow;

	var cate_id = getSelectedValue(obj_cate);
	// 申請書セレクトボックスのオプションを全削除
	deleteAllOptions(obj_wkfw);

	// 申請書セレクトボックスのオプションを作成
	addOption(obj_wkfw, '-', 'すべて');

	{/literal}{$js_str}{literal}

}

/**
 * 「職員名簿」ボタン押下時の処理
 */
function onclick_emp_list_btn(){
	var input_div = "1";
	var childwin = null;
	var url = 'cl_emp_list.php';

	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;

	url += '?session=' + document.getElementById('session').value + '&input_div=' + input_div;
	childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

/**
 * 職員名簿から選択された職員
 * @param string item_id
 * @param string emp_id
 * @param string emp_name
 */
function add_target_list(item_id, emp_id, emp_name){
	document.getElementById('approve_person').value = emp_name;
	document.getElementById('approve_emp').value = emp_id;

}

/**
 * 承認者クリア
 */
function clear_approve_emp(){
	if (confirm('「承認者」をクリアします。よろしいですか？')){
		document.getElementById('approve_person').value = '';
		document.getElementById('approve_emp').value = '';
	}
}

//-->
</script>
{/literal}

<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/cl_application_approve_list.css">
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();initcal();setEventHandler();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr bgcolor="#f6f9ff">
			<td width="32" height="32" class="spacing"><a href="ladder_menu.php?session={$session}"><img src="img/icon/s42.gif" width="32" height="32" border="0" alt="{$cl_title}"></a></td>
			<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="ladder_menu.php?session={$session}"><b>{$cl_title}</b></a> &gt; <a href="cl_workflow_menu.php?session={$session}"><b>管理画面</b></a></font></td>
			<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="ladder_menu.php?session={$session}"><b>ユーザ画面へ</b></a></font></td>
		</tr>
	</table>

{$aplyctn_mnitm_str}

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<form name="approve" action="#" method="post">
<input type="hidden" name="session" id="session" value="{$session}">
<input type="hidden" name="mode"	id="mode"	 value="">
<input type="hidden" name="approve" id="approve" value="">

<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
	<tr>
		<td bgcolor="#E5F6CD">
			<a href="javascript:void(0)" onclick="clear_approve_emp()">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認者</font>
			</a>
		</td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<input type="text" name="approve_person" id="approve_person" value="{$approve_person}" readonly="readonly" >
				<input type="hidden" name="approve_emp" id="approve_emp" value="{$approve_emp}">
				<input type="button" name="emp_list_btn" id="emp_list_btn" value="職員名簿" onclick="onclick_emp_list_btn()">
			</font>
		</td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">

<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
<tr>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font></td>
<td><select name="category" onchange="cateOnChange();">{$cate_options_str}</select></td>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請書名</font>
<td><select name="workflow">{$cate_options_str}</select></td>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">研修区分</font>
<td ><select id="training_div" name="training_div">{$training_div_options_str}</select></td>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">研修名</font>
<td><input type="text" size="25" name="training_theme" value="{$training_theme}"></td>
</tr>

<tr>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織</font></td>
<td colspan="3">{$pst_slct_bx_strct_str}</td>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者</font></td>
<td colspan="3"><input type="text" size="25" maxlength="30" name="apply_emp_nm" value="{$apply_emp_nm}"></td>
</tr>

<tr>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
<td colspan="3">

<table border="0" cellspacing="0" cellpadding="0" class="block_in">
<tr><td>
<select id="date_y1" name="date_y1">{$option_date_y1}</select>/<select id="date_m1" name="date_m1">{$option_date_m1}</select>/<select id="date_d1" name="date_d1">{$option_date_d1}</select>
</td><td>
	<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>

<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
</font>
</td><td>
&nbsp;〜<select id="date_y2" name="date_y2">{$option_date_y2}</select>/<select id="date_m2" name="date_m2">{$option_date_m2}</select>/<select id="date_d2" name="date_d2">{$option_date_d2}</select>

</td><td>
	<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal2()"/><br>
<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
</font>
</td></tr>
</table>

</td>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認状況</font></td>
<td colspan="3">
<select name="apv_stat">{$apv_stat_options_str}</select>
</td>
</tr>

<tr>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開催日</font></td>
<td colspan="3">
<table border="0" cellspacing="0" cellpadding="0" class="block_in">
<tr>
<td><select id="date_y3" name="date_y3">{$option_date_y3}</select>/<select id="date_m3" name="date_m3">{$option_date_m3}</select>/<select id="date_d3" name="date_d3">{$option_date_d3}</select></td>
<td><img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal3();"/><br><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><div id="cal3Container" style="position:absolute;display:none;z-index:10000;"></div></font></td>
<td>&nbsp;〜<select id="date_y4" name="date_y4">{$option_date_y4}</select>/<select id="date_m4" name="date_m4">{$option_date_m4}</select>/<select id="date_d4" name="date_d4">{$option_date_d4}</select></td>
<td><img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal4()"/><br><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><div id="cal4Container" style="position:absolute;display:none;z-index:10000;"></div></font></td>
</tr>
</table>

</td>
<td colspan="4">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block_in">
<tr align="right">
<td><input type="button" value="検索" onclick="all_approve_search();"></td>
</tr>
</table>
</td>
</tr>

</table>
</td>
</tr>
</table>


<img src="img/spacer.gif" alt="" width="1" height="1">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<!-- 2012/03/12 Yamagawa del(s) 一括処理復活時に復活させる -->
<!--<tr>
<td align="right">
<input type="button" value="承認" onclick="approve_regist('1');" id="approve_ok">
<input type="button" value="差戻し" onclick="approve_regist('3');" id="approve_back">
<input type="button" value="否認" onclick="approve_regist('2');" id="approve_ng">
</td>
</tr>-->
<!-- 2012/03/12 Yamagawa del(e) 一括処理復活時に復活させる -->
</table>

<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block2">
<tr bgcolor="#E5F6CD">
<!-- 2012/03/12 Yamagawa del(s) 一括処理復活時に復活させる -->
<!-- <td width="20" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;</font></td> -->
<!-- 2012/03/12 Yamagawa del(e) 一括処理復活時に復活させる -->
<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請番号<br>(管理ＣＤ-連番)</font></td>
<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請書名</font></td>
<td width="*" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">研修名</font></td>
<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">志願者</font></td>
<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">志願者所属</font></td>
<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者</font></td>
<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者所属</font></td>
<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
<td width="60" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認<br>状況</font></td>
</tr>
{$srch_aprv_lst_str}

</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
</td>
</tr>
</table>


</td>
</tr>
</table>
</form>


</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>

</body>
</html>


