<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<title>CoMedix {$cl_title} | ��٥륢�å�ɾ��ɽ</title>
	<script type="text/javascript" src="js/fontsize.js"></script>
	<script type="text/javascript" src="js/cl_levelup_apply_emp_list.js"></script>
	<script type="text/javascript" src="js/jquery.js"></script>

	{$yui_cal_part}
	{literal}
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<style type="text/css">
		.list {border-collapse:collapse;}
		.list td {border:#A0D25A solid 1px;}

		table.block_in {border-collapse:collapse;}
		table.block_in td {border:#A0D25A solid 0px;}
	</style>
	{/literal}
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr height="32" bgcolor="#35B341">
			<td class="spacing"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j14" color="#ffffff"><b>��٥륢�å�ɾ��ɽ</b></font></td>
			<td>&nbsp;</td>
			<td width="10">&nbsp;</td>
			<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="�Ĥ���" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
	<tr>
		<td width="50" height="22" align="center" valign="top" >
			<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><b>����</b></font>
		</td>

		<td width="630" align="center" valign="top"  colspan="2">
			<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><b>ɾ�����</b></font>
		</td>

		{if $evaluation_emp_division == 2 && $must_check != 1}
			<td width="60" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><b>
				Ʊν<br>ɾ����
			</b></font></td>
			<td width="60" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><b>
				<!-- 2012/09/21 Yamagawa upd(s) -->
				<!-- ��Ǥ<br>ɾ�� -->
				Ʊν<br>ɾ����
				<!-- 2012/09/21 Yamagawa upd(e) -->
			</b></font></td>
			<td width="60" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><b>
				����<br>ɾ��
			</b></font></td>
		{/if}

		<td width="60" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><b>
			{if $evaluation_emp_division == 1}
				����<br>ɾ��
			{elseif $evaluation_emp_division == 2}
				��°Ĺ<br>ɾ��
			{elseif $evaluation_emp_division == 3}
				<!-- 2012/09/21 Yamagawa upd(s) -->
				<!-- ��Ǥ<br>ɾ�� -->
				Ʊν<br>ɾ����
				<!-- 2012/09/21 Yamagawa upd(e) -->
			{elseif $evaluation_emp_division == 4}
				Ʊν<br>ɾ����
			{/if}
		</b></font></td>

		{if $must_check == 1}
		<td width="60" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><b>
			ɬã<br>ɾ��
		</b></font></td>
		{/if}
	</tr>
	{assign var="last_cat_id" value=""}
	{assign var="last_grp_id" value=""}
	{section name=cell loop=$data}
		{if $last_cat_id != $data[cell].category_id}
			<tr>
				<td width="50" style=" align:right; writing-mode:tb-rl" rowspan="{$data[cell].cat_rowspan}"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
					{$data[cell].category_name}
				</font></td>

				<td width="40" align="center" valign="top" rowspan="{$data[cell].grp_rowspan}"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
					{$data[cell].grp_order}
				</font></td>

				<td width="590" align="left"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
					{$data[cell].group_name}
				</font></td>

				{if $evaluation_emp_division == 2 && $must_check != 1}
					<td width="60"></td>
					<td width="60"></td>
					<td width="60"></td>
				{/if}

				<td width="60" align="center" align="left"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
				</font></td>
				{if $must_check == 1}
				<td width="60" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
				</font></td>
				{/if}
			</tr>
		{elseif $last_grp_id != $data[cell].group_id}
			<tr>
				<td width="40" align="center" valign="top" rowspan="{$data[cell].grp_rowspan}"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
					{$data[cell].grp_order}
				</font></td>

				<td width="590" align="left"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
					{$data[cell].group_name}
				</font></td>

				{if $evaluation_emp_division == 2 && $must_check != 1}
					<td width="60"></td>
					<td width="60"></td>
					<td width="60"></td>
				{/if}

				<td width="60" align="center">
				</td>

				{if $must_check == 1}
				<td width="60" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
				</font></td>
				{/if}
			</tr>
		{/if}

		<tr>
			{if $data[cell].must_value == ''}
				<td width="590" align="left">
			{else}
				<td width="590" align="left" bgcolor="#ffff66">
			{/if}
				<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
					{$data[cell].item_name}
				</font>
			</td>

			{if $evaluation_emp_division == 2 && $must_check != 1}
				{if $data[cell].must_value == ''}
					<td width="60" align="center">
				{else}
					<td width="60" align="center" bgcolor="#ffff66">
				{/if}
					<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
						{$data[cell].div3_value}
					</font>
				</td>
				{if $data[cell].must_value == ''}
					<td width="60" align="center">
				{else}
					<td width="60" align="center" bgcolor="#ffff66">
				{/if}
					<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
						{$data[cell].div4_value}
					</font>
				</td>
				{if $data[cell].must_value == ''}
					<td width="60" align="center">
				{else}
					<td width="60" align="center" bgcolor="#ffff66">
				{/if}
					<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
						{$data[cell].div1_value}
					</font>
				</td>
			{/if}

			{if $data[cell].must_value == ''}
				<td width="60" align="center">
			{else}
				<td width="60" align="center" bgcolor="#ffff66">
			{/if}
				<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
					{if $evaluation_emp_division == 1}
						{$data[cell].div1_value}
					{elseif $evaluation_emp_division == 2}
						{$data[cell].div2_value}
					{elseif $evaluation_emp_division == 3}
						{$data[cell].div3_value}
					{elseif $evaluation_emp_division == 4}
						{$data[cell].div4_value}
					{/if}
				</font>
			</td>

			{if $must_check == 1}
				{if $data[cell].must_value == ''}
					<td width="60" align="center">
				{else}
					<td width="60" align="center" bgcolor="#ffff66">
				{/if}
					<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
						{$data[cell].must_value}
					</font>
				</td>
			{/if}
		</tr>
		{assign var="last_cat_id" value=$data[cell].category_id}
		{assign var="last_grp_id" value=$data[cell].group_id}
	{/section}
	</table>

	<img src="img/spacer.gif" alt="" width="1" height="5"><br>

	{* ############################################################################################################################################################ *}
	{* �Ҳ��̥եå��� *}
	{* ############################################################################################################################################################ *}
	{include file='cl_sub_screen_footer.tpl'}

</body>
</html>