<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<title>CoMedix {$cl_title} | 研修受講進捗</title>
	<script type="text/javascript" src="js/fontsize.js"></script>
	<script type="text/javascript" src="js/cl_training_progress.js"></script>
	<script type="text/javascript" src="js/jquery.js"></script>
	{$yui_cal_part}
	{literal}
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<style type="text/css">
		.list {border-collapse:collapse;}
		.list td {border:#A0D25A solid 1px;}

		table.block_in {border-collapse:collapse;}
		table.block_in td {border:#A0D25A solid 0px;}
	</style>
	{/literal}
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr height="32" bgcolor="#35B341">
			<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>研修受講進捗</b></font></td>
			<td>&nbsp;</td>
			<td width="10">&nbsp;</td>
			<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<img src="img/spacer.gif" width="1" height="10" alt=""><br>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<img src="img/spacer.gif" alt="" width="1" height="5"><br>

				<form id="training_form" name="training_form" action="#" target="_self" method="post">
					<input type="hidden" id="session" name="session" value="{$session}">
					<input type="hidden" id="target_emp_id" name="target_emp_id" value="{$target_emp_id}">
					<input type="hidden" id="mode" name="mode" value="{$mode}">
					<input type="hidden" id="common_module" name="common_module" value="{$common_module}">
					<input type="hidden" id="emp_id" name="emp_id" value="{$emp_id}">
					<input type="hidden" id="year" name="year" value="{$year}">
					<input type="hidden" id="p_session" name="p_session" value="{$p_session}">
					<input type="hidden" id="level_date_regist" name="level_date_regist">
					<!-- 出張届・報告へのパラメータ項目はここで宣言  -->

					<!-- 検索条件 -->
					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									職員氏名：{$emp_full_nm}&nbsp;&nbsp;&nbsp;&nbsp;所属：{$class_nm}
								</font>
							</td>
							{if $council_flg}
								<td align="right">
									<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
										<input type="button" value="追加" onclick="get_level_date_regist();" />
									</font>
								</td>
							{/if}
						</tr>
					</table>

					<img src="img/spacer.gif" alt="" width="1" height="5"><br>

					<table border="0" cellspacing="0" cellpadding="1" class="list">
						<tr bgcolor="#E5F6CD">
							<td align="center" width="80px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ラダーレベル</font></td>
							<td align="center" width="200px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">認定日</font></td>
							<td align="center" width="80px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ラダーレベル</font></td>
							<td align="center" width="200px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">認定日</font></td>
							<td align="center" width="80px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ラダーレベル</font></td>
							<td align="center" width="200px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">認定日</font></td>
						</tr>
						<tr>
							<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">Ｉ</font></td>
							<td align="left">
								<table border="0" cellspacing="0" cellpadding="0" class="block_in">
									<tr>
										<td>
											{if $council_flg}
												<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
													<div id="callevel1Container" style="position:absolute;display:none;z-index:10000;"></div>
												</font>
												<select id="auth_year_level1" name="auth_year_level1">{$option_level1_year}</select>/
												<select id="auth_month_level1" name="auth_month_level1">{$option_level1_month}</select>/
												<select id="auth_day_level1" name="auth_day_level1">{$option_level1_day}</select>
												<img id="callevel1"src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_callevel1();"/>
											{else}
												<select id="auth_year_level1" name="auth_year_level1" disabled>{$option_level1_year}</select>/
												<select id="auth_month_level1" name="auth_month_level1" disabled>{$option_level1_month}</select>/
												<select id="auth_day_level1" name="auth_day_level1" disabled>{$option_level1_day}</select>
											{/if}
										</td>
									</tr>
								</table>
							</td>
							<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">III</font></td>
							<td align="left">
								<table border="0" cellspacing="0" cellpadding="0" class="block_in">
									<tr>
										<td>
											{if $council_flg}
												<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
													<div id="callevel3Container" style="position:absolute;display:none;z-index:10000;"></div>
												</font>
												<select id="auth_year_level3" name="auth_year_level3">{$option_level3_year}</select>/
												<select id="auth_month_level3" name="auth_month_level3">{$option_level3_month}</select>/
												<select id="auth_day_level3" name="auth_day_level3">{$option_level3_day}</select>
												<img id="callevel3"src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_callevel3();"/>
											{else}
												<select id="auth_year_level3" name="auth_year_level3" disabled>{$option_level3_year}</select>/
												<select id="auth_month_level3" name="auth_month_level3" disabled>{$option_level3_month}</select>/
												<select id="auth_day_level3" name="auth_day_level3" disabled>{$option_level3_day}</select>
											{/if}
										</td>
									</tr>
								</table>
							</td>
							<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">Ｖ</font></td>
							<td align="left">
								<table border="0" cellspacing="0" cellpadding="0" class="block_in">
									<tr>
										<td>
											{if $council_flg}
												<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
													<div id="callevel5Container" style="position:absolute;display:none;z-index:10000;"></div>
												</font>
												<select id="auth_year_level5" name="auth_year_level5">{$option_level5_year}</select>/
												<select id="auth_month_level5" name="auth_month_level5">{$option_level5_month}</select>/
												<select id="auth_day_level5" name="auth_day_level5">{$option_level5_day}</select>
												<img id="callevel5"src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_callevel5();"/>
											{else}
												<select id="auth_year_level5" name="auth_year_level5" disabled>{$option_level5_year}</select>/
												<select id="auth_month_level5" name="auth_month_level5" disabled>{$option_level5_month}</select>/
												<select id="auth_day_level5" name="auth_day_level5" disabled>{$option_level5_day}</select>
											{/if}
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">II</font></td>
							<td align="left">
								<table border="0" cellspacing="0" cellpadding="0" class="block_in">
									<tr>
										<td>
											{if $council_flg}
												<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
													<div id="callevel2Container" style="position:absolute;display:none;z-index:10000;"></div>
												</font>
												<select id="auth_year_level2" name="auth_year_level2">{$option_level2_year}</select>/
												<select id="auth_month_level2" name="auth_month_level2">{$option_level2_month}</select>/
												<select id="auth_day_level2" name="auth_day_level2">{$option_level2_day}</select>
												<img id="callevel2"src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_callevel2();"/>
											{else}
												<select id="auth_year_level2" name="auth_year_level2" disabled>{$option_level2_year}</select>/
												<select id="auth_month_level2" name="auth_month_level2" disabled>{$option_level2_month}</select>/
												<select id="auth_day_level2" name="auth_day_level2" disabled>{$option_level2_day}</select>
											{/if}
										</td>
									</tr>
								</table>
							</td>
							<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">IV</font></td>
							<td align="left">
								<table border="0" cellspacing="0" cellpadding="0" class="block_in">
									<tr>
										<td>
											{if $council_flg}
												<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
													<div id="callevel4Container" style="position:absolute;display:none;z-index:10000;"></div>
												</font>
												<select id="auth_year_level4" name="auth_year_level4">{$option_level4_year}</select>/
												<select id="auth_month_level4" name="auth_month_level4">{$option_level4_month}</select>/
												<select id="auth_day_level4" name="auth_day_level4">{$option_level4_day}</select>
												<img id="callevel4"src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_callevel4();"/>
											{else}
												<select id="auth_year_level4" name="auth_year_level4" disabled>{$option_level4_year}</select>/
												<select id="auth_month_level4" name="auth_month_level4" disabled>{$option_level4_month}</select>/
												<select id="auth_day_level4" name="auth_day_level4" disabled>{$option_level4_day}</select>
											{/if}
										</td>
									</tr>
								</table>
							</td>
							<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
							<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
						</tr>
					</table>

					<img src="img/spacer.gif" alt="" width="1" height="20"><br>

					<table border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td>
								<input type="hidden" id="search_year" name="search_year" value="{$option_search_year}">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$option_search_year}年度</font>
							</td>
						</tr>
					</table>

					<img src="img/spacer.gif" alt="" width="1" height="5"><br>

					<table border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">院内研修</font>
							</td>
						</tr>
					</table>

					<img src="img/spacer.gif" alt="" width="1" height="5"><br>

					<table border="0" cellspacing="0" cellpadding="1"  class="list">
						<tr bgcolor="#E5F6CD">
							<td align="center" width="120px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">番号</font></td>
							<td align="center" width="200px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">研修名／課題</font></td>
							<td align="center" width="80px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
							<td align="center" width="50px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回数</font></td>
							<td align="center" width="80px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受講日</font></td>
							<td align="center" width="80px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告日</font></td>
						</tr>
						{section name=cell loop=$data_inside}
						<tr>
							<td align="center" width="120px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$data_inside[cell].no_id}</font></td>
							<td align="left" width="200px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$data_inside[cell].training_name}</font></td>
							<td align="center" width="80px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$data_inside[cell].inside_training_date}</font></td>
							<td align="center" width="50px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$data_inside[cell].count}</font></td>
							<td align="center" width="80px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$data_inside[cell].plan_date}</font></td>
							<td align="center" width="80px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$data_inside[cell].report_date}</font></td>
						</tr>
						{/section}
					</table>

					<img src="img/spacer.gif" alt="" width="1" height="10">

					<!-- 登録・削除ボタン -->
					<table border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td align="left">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">院外研修</font>
							</td>
						</tr>
					</table>

					<!-- 一覧表示 -->
					<img src="img/spacer.gif" alt="" width="1" height="5"><br>
					<table border="0" cellspacing="0" cellpadding="1" class="list">
						<!-- ヘッダー -->
						<tr bgcolor="#E5F6CD">
							<td align="left" width="200px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">研修名</font></td>
							<td align="left" width="80px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
							<td align="left" width="100px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">費用負担</font></td>
							<td align="left" width="200px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開催日</font></td>
							<td align="left" width="80px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告日</font></td>
						</tr>
						<!-- 一覧 -->
						{section name=cell loop=$data_outside}
						<input type="hidden" id="seminar_apply_id{$data_outside[cell].seminar_apply_id}" name="seminar_apply_id{$data_outside[cell].seminar_apply_id}" value="{$data_outside[cell].seminar_apply_id}">
						<tr>
							<td align="left" width="200px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$data_outside[cell].seminar_name}</font></td>
							<td align="left" width="80px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$data_outside[cell].apply_date}</font></td>
							<td align="left" width="100px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$data_outside[cell].expense}</font></td>
							<td align="left" width="200px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$data_outside[cell].open_date}</font></td>
							<td align="left" width="80px"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$data_outside[cell].report_date}</font></td>
						</tr>
						{/section}
					</table>
				</form>
			</td>
		</tr>
	</table>
	<img src="img/spacer.gif" alt="" width="1" height="5"><br>

	{* ############################################################################################################################################################ *}
	{* 子画面フッター *}
	{* ############################################################################################################################################################ *}
	{include file='cl_sub_screen_footer.tpl'}

{if $council_flg}
	<script type="text/javascript">
		YAHOO.namespace("ui.calendar");
		{$calendar}
	</script>
{/if}

</body>
</html>