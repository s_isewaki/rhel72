<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
		<title>CoMedix {$cl_title} |ファイル添付</title>
		<script type="text/javascript" src="js/cl_attach_file_regist_1.js"></script>
		<script type="text/javascript" src="js/fontsize.js"></script>
		<script type="text/javascript" src="js/cl_common.js"></script>
		{literal}
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<style type="text/css">
			.list {border-collapse:collapse;}
			.list td {border:#A0D25A solid 1px;}
		</style>
		{/literal}
	</head>
	<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">

		{* ############################################################################################################################################################ *}
		{* 子画面ヘッダー *}
		{* ############################################################################################################################################################ *}
		{include file='cl_sub_screen_header.tpl'}

		<img src="img/spacer.gif" alt="" width="1" height="2"><br>
		<form id="attach" name="attach" action="#" method="post" target="_self" enctype="multipart/form-data">
			<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
				<tr>
				<td height="22" width="160" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">添付ファイル</font></td>
				<td width="440">
					<input type="file" id="file" name="file" size="50">
					<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※{$upload_max_filesize}まで</font>
				</td>
				</tr>
			</table>
			<table width="600" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td height="22" align="right"><input type="button" onclick="upload();" value="添付"></td>
				</tr>
			</table>
			<input type="hidden" id="session" name="session" value="{$session}">
			<input type="hidden" id="common_module" name="common_module" value="{$common_module}">
			<input type="hidden" id="file_kbn" name="file_kbn" value="{$file_kbn}">
			<input type="hidden" id="upload_flg" name="upload_flg">
		</form>

		{* ############################################################################################################################################################ *}
		{* 子画面フッター *}
		{* ############################################################################################################################################################ *}
		{include file='cl_sub_screen_footer.tpl'}

	</body>
</html>
