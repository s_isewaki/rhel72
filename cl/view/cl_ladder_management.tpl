<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
	<title>CoMedix {$cl_title} | ���������</title>
	<script type="text/javascript" src="js/fontsize.js"></script>
	<script type="text/javascript" src="js/cl_ladder_management.js"></script>
	{$yui_cal_part}
	{literal}
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<style type="text/css">
		.list {border-collapse:collapse;}
		.list td {border:#A0D25A solid 1px;}

		table.block_in {border-collapse:collapse;}
		table.block_in td {border:#A0D25A solid 0px;}
	</style>
	{/literal}
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr bgcolor="#f6f9ff">
						<td width="32" height="32" class="spacing"><a href="ladder_menu.php?session={$session}"><img src="img/icon/s42.gif" width="32" height="32" border="0" alt="{$cl_title}"></a></td>
						<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j16">&nbsp;<a href="ladder_menu.php?session={$session}"><b>{$cl_title}</b></a> &gt; <a href="cl_ladder_management.php?session={$session}"><b>���������</b></a></font></td>

						<!-- �������̥��ɽ�� -->
						{if $workflow_auth == "1"}
						<td align="right" style="padding-right:6px;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j16">&nbsp;<a href="cl_workflow_menu.php?session={$session}&workflow=1"><b>�������̤�</b></a></font></td>
						{else}
						{/if}

					</tr>
				</table>

				<!-- ���֥�˥塼ɽ�� -->
				{$aplyctn_mnitm_str}

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
					</tr>
				</table>

				<img src="img/spacer.gif" alt="" width="1" height="2"><br>

				<form id="ladder_form" name="ladder_form" action="#" method="post">
					<input type="hidden" id="session" name="session" value="{$session}">
					<input type="hidden" id="training_id" name="training_id">
					<input type="hidden" id="date_update_flg" name="date_update_flg" value="{$date_update_flg}">
					<input type="hidden" id="emp_date" name="emp_date" value="{$emp_date}">
					<img src="img/spacer.gif" alt="" width="1" height="2"><br>

					<!-- ������� -->
					<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
						<tr>
							<td width="80" align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��°</font></td>
							<td colspan="3">{$pst_slct_bx_strct_str}</td>
							<td width="80" align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�������٥�</font></td>
							<td width="50"><select id="now_level" name="now_level">{$option_level}</select></td>
							<!-- 2012/08/24 Yamagawa add(s) -->
							<td width="80" align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�࿦��ɽ��</font></td>
							<td width="30" align="center"><input type="checkbox" id="retire_disp_flg" name="retire_disp_flg" value="t" {$retire_disp_flg}></td>
							<!-- 2012/08/24 Yamagawa add(e) -->
						</tr>
						<tr>
							<td width="80" align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ǧ����</font></td>
							<td align="left" colspan="3">
								<table border="0" cellspacing="0" cellpadding="0" class="block_in">
									<tr>
										<td>
											<select id="date_authorize_from_y" name="date_authorize_from_y">{$option_date_authorize_from_y}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_authorize_from_m" name="date_authorize_from_m">{$option_date_authorize_from_m}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_authorize_from_d" name="date_authorize_from_d">{$option_date_authorize_from_d}</select>
										</td>
										<td>
											<img id="cal_authorize_from" src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal_authorize_from();"/>
											<font size='3' face='�ͣ� �Х����å�, Osaka' class='j12'>
												<div id="cal_authorize_fromContainer" style="position:absolute;display:none;z-index:10000;"></div>
											</font>
										</td>
										<td>
											&nbsp;<font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��</font>&nbsp;
											<select id="date_authorize_to_y" name="date_authorize_to_y">{$option_date_authorize_to_y}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_authorize_to_m" name="date_authorize_to_m">{$option_date_authorize_to_m}</select><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">/</font>
											<select id="date_authorize_to_d" name="date_authorize_to_d">{$option_date_authorize_to_d}</select>
										</td>
										<td>
											<img id="cal_authorize_to" src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal_authorize_to()"/>
											<font size='3' face='�ͣ� �Х����å�, Osaka' class='j12'>
												<div id="cal_authorize_toContainer" style="position:absolute;display:none;z-index:10000;"></div>
											</font>
										</td>
									</tr>
								</table>
							</td>
							<td width="80" align="left" bgcolor="#E5F6CD"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">������̾</font></td>
							<td colspan="3"><input type="text" id="emp_nm" name="emp_nm" value="{$emp_nm}" size="30"></td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td align="right">
								<input type="button" value="����" onclick="research();">
							</td>
						</tr>
					</table>
					<img src="img/spacer.gif" alt="" width="1" height="10">
					<!-- �ܥ��� -->
					{if $council_flg}
						<table width="100%" border="0" cellspacing="0" cellpadding="1">
							<tr>
								<td align="right">
									<input type="button" value="ǧ���ȯ��" onclick="update_certificate_date();">
								</td>
							</tr>
						</table>
					{/if}
					<!-- ����ɽ�� -->
					<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
						<!-- �إå��� -->
						<tr bgcolor="#E5F6CD">
							{if $council_flg}
								<td align="left" width="50"></td>
							{/if}
							<td align="left" width="*"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��̾</font></td>
							<td align="left" width="*"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��°</font></td>
							<td align="center" width="40"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�������٥�</font></td>
							<td align="center" width="50"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">������</font></td>
							<td align="center" width="80"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ǧ����</font></td>
							<td align="center" width="80"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ǧ���ȯ����</font></td>
						</tr>
						<!-- ���� -->
						{section name=cell loop=$data}
						<tr>
							{if $council_flg}
								<td align="center"><input type="checkbox" name="ladder_chkbox" value="{$data[cell].emp_id},{$data[cell].emp_full_nm},{$data[cell].affiliation},{$data[cell].now_level},{$data[cell].level_total_value},{$data[cell].get_level_date},{$data[cell].level_certificate_date}"></td>
							{/if}
							<td align="left" onmouseover="this.style.cursor='pointer'" onmouseout="this.style.cursor=''" onclick="show_detail('{$data[cell].emp_id}','{$data[cell].year}');"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].emp_full_nm}</font></td>
							<td align="left" onmouseover="this.style.cursor='pointer'" onmouseout="this.style.cursor=''" onclick="show_detail('{$data[cell].emp_id}','{$data[cell].year}');"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].affiliation}</font></td>
							<td align="center" onmouseover="this.style.cursor='pointer'" onmouseout="this.style.cursor=''" onclick="show_detail('{$data[cell].emp_id}','{$data[cell].year}');"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].now_level}</font></td>
							<td align="center" onmouseover="this.style.cursor='pointer'" onmouseout="this.style.cursor=''" onclick="show_detail('{$data[cell].emp_id}','{$data[cell].year}');"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].level_total_value}</font></td>
							<td align="center" onmouseover="this.style.cursor='pointer'" onmouseout="this.style.cursor=''" onclick="show_detail('{$data[cell].emp_id}','{$data[cell].year}');"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].get_level_date}</font></td>
							<td align="center" onmouseover="this.style.cursor='pointer'" onmouseout="this.style.cursor=''" onclick="show_detail('{$data[cell].emp_id}','{$data[cell].year}');"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">{$data[cell].level_certificate_date}</font></td>
						</tr>
						{/section}
					</table>
				</form>
			</td>
		</tr>
	</table>
	<img src="img/spacer.gif" alt="" width="1" height="5"><br>

	{* ############################################################################################################################################################ *}
	{* �Ҳ��̥եå��� *}
	{* ############################################################################################################################################################ *}
	{include file='cl_sub_screen_footer.tpl'}

	<script type="text/javascript">
		YAHOO.namespace("ui.calendar");
		{$calendar}
	</script>

</body>
</html>