<body>

<?
require_once("about_comedix.php");
require_once("settings_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

//時間有休表示フラグ、休暇申請テンプレートで使用する場合"t" opt/settings.phpで設定
//$paid_hol_hour_flg = get_settings_value("kintai_paid_hol_hour_flg", "f"); //常に表示のためコメント化する 20141217

// トランザクションを開始
pg_query($con, "begin transaction");

// 事由マスタ更新
$reason_count = count($reason_id);
for ($i = 0; $i < $reason_count; $i++) {
	//チェックボックスの項目名生成
	$work_display_flag = "display_flag_".$reason_id[$i];
    $work_time_hol_flag = "time_hol_flag_".$reason_id[$i];
    $work_half_hol_flag = "half_hol_flag_".$reason_id[$i];
    
	//非表示チェックボックスの値変換
	if (empty($$work_display_flag)){
		//チェックがなかったら表示
		$work_display_flag = "TRUE";
	}
	else{
		//チェックが有りで非表示
		$work_display_flag = "FALSE";
	}

    //時間有休チェックボックスの値変換
    if (empty($$work_time_hol_flag)){
        $work_time_hol_flag = "FALSE";
    }
    else{
        $work_time_hol_flag = "TRUE";
    }
    //半休/半休チェックボックスの値変換
    if (empty($$work_half_hol_flag)){
        $work_half_hol_flag = "FALSE";
    }
    else{
        $work_half_hol_flag = "TRUE";
    }
        
	$sql = "update atdbk_reason_mst set";
	$set = array("display_name", "display_flag", "recital", "day_count", "holiday_count", "kind_flag", "time_hol_flag", "half_hol_flag");
	$setvalue = array($display_name[$i], $work_display_flag, $recital[$i], $day_count[$i], $holiday_count[$i], $kind_flag[$i], $work_time_hol_flag, $work_half_hol_flag);
    $cond = "where reason_id = '$reason_id[$i]'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}


//細目名称マスタを更新する　20140620
$details = $_REQUEST["details_info"];

$sql = "delete from atdbk_reason_detail_mst";
$cond = "";
$del = delete_from_table($con, $sql, "", $fname);
if ($del == 0){
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

foreach ($details as $reason_id => $list){
	for ($j = 0; $j < count($list); $j++){
		$no = $j + 1;
		$full_day = (empty($list[$j]["full_day"])) ? null : (int)$list[$j]["full_day"];
		$part_day = (empty($list[$j]["part_day"])) ? null : (int)$list[$j]["part_day"];
	
		$sql = "insert into atdbk_reason_detail_mst(reason_id, reason_detail_id, reason_detail_name, reason_fulltime_day, reason_parttime_day";
		$sql .= ") values (";
		$content = array($reason_id, $no, pg_escape_string($list[$j]["detail_name"]), $full_day, $part_day);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// 休暇を選択した場合は事由に自動的に「公休」が入る 20130129
$reason_value = (isset($_POST["chkVacation"])) ? 24 : NULL;

$sql = "update atdptn set";
$set = array("reason");
$setvalue = array($reason_value);
$cond = "where atdptn_id = '10'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}


// トランザクションをコミット
pg_query("commit");

// データベース接続を閉じる
pg_close($con);

// 画面を再表示
echo("<script type=\"text/javascript\">location.href = 'atdbk_reason_mst_config.php?session=$session';</script>");
?>
</body>
