<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once('Cmx/View/Smarty.php');

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$auth_id = ($path != "3") ? 47 : 48;
if (check_authority($session, $auth_id, $fname) == "0")
{
    showLoginPage();
    exit;
}

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}


//==============================
// ログインユーザの職員ID・所属部署IDを取得
//==============================
$sql = "select emp_id, emp_class, emp_attribute, emp_dept, emp_st, (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0)
{
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_class = pg_fetch_result($sel, 0, "emp_class");
$emp_atrb = pg_fetch_result($sel, 0, "emp_attribute");
$emp_dept = pg_fetch_result($sel, 0, "emp_dept");
$emp_st = pg_fetch_result($sel, 0, "emp_st");
$emp_name = pg_fetch_result($sel, 0, "emp_name");


//==============================
//所属・役職一覧の取得
//==============================

// 部門一覧検索
$sql = "select class_id, class_nm from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0)
{
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 課一覧検索
$sql = "select classmst.class_id, atrbmst.atrb_id, classmst.class_nm, atrbmst.atrb_nm from atrbmst inner join classmst on atrbmst.class_id = classmst.class_id";
$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' order by classmst.class_id, atrbmst.order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);
if ($sel_atrb == 0)
{
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 科一覧検索
$sql = "select classmst.class_id, atrbmst.atrb_id, deptmst.dept_id, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm from deptmst inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id inner join classmst on atrbmst.class_id = classmst.class_id";
$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' and deptmst.dept_del_flg = 'f' order by classmst.class_id, atrbmst.atrb_id, deptmst.order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
if ($sel_dept == 0)
{
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 役職一覧検索
$sql = "select st_id, st_nm from stmst";
$cond = "where st_del_flg = 'f' order by order_no";
$sel_st = select_from_table($con, $sql, $cond, $fname);
if ($sel_st == 0)
{
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

//科一覧配列作成
$dept_names = Array();
while ($row = pg_fetch_array($sel_dept))
{
    $tmp_class_id = $row["class_id"];
    $tmp_atrb_id = $row["atrb_id"];
    $tmp_dept_id = $row["dept_id"];
    $tmp_dept_nm = $row["dept_nm"];
    $dept_names["{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"] = $tmp_dept_nm;
}
pg_result_seek($sel_dept, 0);



//==============================
// 組織名を取得
//==============================
$arr_class_name = get_class_name_array($con, $fname);



//==============================
// 初期表示時の設定
//==============================

// 権限情報
if (!is_array($ref_dept)) {$ref_dept = array();}
if (!is_array($ref_st)) {$ref_st = array();}
if (!is_array($upd_dept)) {$upd_dept = array();}
if (!is_array($upd_st)) {$upd_st = array();}



//==============================
// 親フォルダIDを取得
//==============================

// フォルダが指定されている場合
$parent = "";
if ($folder_id != "")
{
    // 親フォルダのフォルダパスを取得
    $sql = "select parent_id from el_tree";
    $cond = "where child_id = $folder_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) > 0)
    {
        $parent = pg_fetch_result($sel, 0, "parent_id");
    }
}


//==============================
//初期表示時の処理
//==============================
if ($back != "t")
{
    // fフォルダの更新の場合
    if ($folder_id != "")
    {
        // フォルダ情報を取得
        $sql = "select el_folder.*, el_cate.lib_link_id from el_folder inner join el_cate on el_cate.lib_cate_id = el_folder.lib_cate_id";
        $cond = "where folder_id = $folder_id";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0)
        {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $folder_name = pg_fetch_result($sel, 0, "folder_name");
        $lib_link_id = pg_fetch_result($sel, 0, "lib_link_id");

    }
    // カテゴリの更新の場合
    else
    {
        $sql = "select * from el_cate";
        $cond = "where lib_cate_id = $cate_id";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0)
        {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $folder_name = pg_fetch_result($sel, 0, "lib_cate_nm");
        $lib_link_id = pg_fetch_result($sel, 0, "lib_link_id");
    }

    // 権限情報を取得
    $ref_dept_st_flg = pg_fetch_result($sel, 0, "ref_dept_st_flg");
    $ref_dept_flg = pg_fetch_result($sel, 0, "ref_dept_flg");
    $ref_st_flg = pg_fetch_result($sel, 0, "ref_st_flg");
    $upd_dept_st_flg = pg_fetch_result($sel, 0, "upd_dept_st_flg");
    $upd_dept_flg = pg_fetch_result($sel, 0, "upd_dept_flg");
    $upd_st_flg = pg_fetch_result($sel, 0, "upd_st_flg");


    // フォルダが指定されている場合
    if ($folder_id != "")
    {
        $refdept_tblname = "el_folder_ref_dept";
        $refst_tblname = "el_folder_ref_st";
        $refemp_tblname = "el_folder_ref_emp";
        $upddept_tblname = "el_folder_upd_dept";
        $updst_tblname = "el_folder_upd_st";
        $updemp_tblname = "el_folder_upd_emp";
        $id_name = "folder_id";
        $lib_id = $folder_id;
    }
    else
    {
        $refdept_tblname = "el_cate_ref_dept";
        $refst_tblname = "el_cate_ref_st";
        $refemp_tblname = "el_cate_ref_emp";
        $upddept_tblname = "el_cate_upd_dept";
        $updst_tblname = "el_cate_upd_st";
        $updemp_tblname = "el_cate_upd_emp";
        $id_name = "lib_cate_id";
        $lib_id = $cate_id;
    }

    $sql = "select e.class_id, e.atrb_id, e.dept_id from $refdept_tblname e left join classmst c on e.class_id = c.class_id left join atrbmst a on e.atrb_id = a.atrb_id left join deptmst d on e.dept_id = d.dept_id";
    $cond = "where e.$id_name = $lib_id order by c.order_no, a.order_no, d.order_no";
    $sel_ref_dept = select_from_table($con, $sql, $cond, $fname);
    if ($sel_ref_dept == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel_ref_dept))
    {
        $tmp_class_id = $row["class_id"];
        $tmp_atrb_id = $row["atrb_id"];
        $tmp_dept_id = $row["dept_id"];
        $ref_dept[] = "$tmp_class_id-$tmp_atrb_id-$tmp_dept_id";
    }

    $sql = "select st_id from $refst_tblname";
    $cond = "where $id_name = $lib_id order by st_id";
    $sel_ref_st = select_from_table($con, $sql, $cond, $fname);
    if ($sel_ref_st == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel_ref_st))
    {
        $tmp_st_id = $row["st_id"];
        $ref_st[] = $tmp_st_id;
    }

    $sql = "select emp_id from $refemp_tblname";
    $cond = "where $id_name = $lib_id order by emp_id";
    $sel_ref_emp = select_from_table($con, $sql, $cond, $fname);
    if ($sel_ref_emp == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $target_id_list1 = "";
    $is_first_flg = true;
    while ($row = pg_fetch_array($sel_ref_emp))
    {
        $tmp_emp_id = $row["emp_id"];
        if ($is_first_flg)
        {
            $is_first_flg = false;
        }
        else
        {
            $target_id_list1 .= ",";
        }
        $target_id_list1 .= $tmp_emp_id;
    }

    $sql = "select e.class_id, e.atrb_id, e.dept_id from $upddept_tblname e left join classmst c on e.class_id = c.class_id left join atrbmst a on e.atrb_id = a.atrb_id left join deptmst d on e.dept_id = d.dept_id";
    $cond = "where e.$id_name = $lib_id order by c.order_no, a.order_no, d.order_no";
    $sel_upd_dept = select_from_table($con, $sql, $cond, $fname);
    if ($sel_upd_dept == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel_upd_dept))
    {
        $tmp_class_id = $row["class_id"];
        $tmp_atrb_id = $row["atrb_id"];
        $tmp_dept_id = $row["dept_id"];
        $upd_dept[] = "$tmp_class_id-$tmp_atrb_id-$tmp_dept_id";
    }

    $sql = "select st_id from $updst_tblname";
    $cond = "where $id_name = $lib_id order by st_id";
    $sel_upd_st = select_from_table($con, $sql, $cond, $fname);
    if ($sel_upd_st == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel_upd_st))
    {
        $tmp_st_id = $row["st_id"];
        $upd_st[] = $tmp_st_id;
    }

    $sql = "select emp_id from $updemp_tblname";
    $cond = "where $id_name = $lib_id order by emp_id";
    $sel_upd_emp = select_from_table($con, $sql, $cond, $fname);
    if ($sel_upd_emp == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $target_id_list2 = "";
    $is_first_flg = true;
    while ($row = pg_fetch_array($sel_upd_emp))
    {
        $tmp_emp_id = $row["emp_id"];
        if ($is_first_flg)
        {
            $is_first_flg = false;
        }
        else
        {
            $target_id_list2 .= ",";
        }
        $target_id_list2 .= $tmp_emp_id;
    }
}


//==============================
//権限部分の表示有無情報
//==============================
if ($ref_toggle_mode == "")
{
    $ref_toggle_mode = "▲";
}
$ref_toggle_display = ($ref_toggle_mode == "▼") ? "none" : "";
if ($upd_toggle_mode == "")
{
    $upd_toggle_mode = "▲";
}
$upd_toggle_display = ($upd_toggle_mode == "▼") ? "none" : "";

//==============================
// メンバー情報を配列に格納
//==============================

//参照権限の対象職員
$arr_target['1'] = array();
if ($target_id_list1 != "")
{
    $arr_target_id = split(",", $target_id_list1);
    for ($i = 0; $i < count($arr_target_id); $i++)
    {
        $tmp_emp_id = $arr_target_id[$i];
        $sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
        $cond = "where emp_id = '$tmp_emp_id'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0)
        {
            pg_close($con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
        }
        $tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
        array_push($arr_target['1'], array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
    }
}

//更新権限の対象職員
$arr_target['2'] = array();
if ($target_id_list2 != "")
{
    $arr_target_id = split(",", $target_id_list2);
    for ($i = 0; $i < count($arr_target_id); $i++)
    {
        $tmp_emp_id = $arr_target_id[$i];
        $sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
        $cond = "where emp_id = '$tmp_emp_id'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0)
        {
            pg_close($con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
        }
        $tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
        array_push($arr_target['2'], array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
    }
}


//==============================
// 更新権限確認
//==============================

//管理画面の場合
if ($path == "3")
{
    $edit_button_disabled = false;
}
//通常画面の場合
else
{
    $upd_flg = get_upd_flg($con, $fname, $archive, $cate_id, $folder_id, $emp_id, $emp_class, $emp_atrb, $emp_dept, $emp_st);
    $edit_button_disabled = !$upd_flg;
}

//====================================
//表示
//====================================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("PAGE_TITLE", "フォルダ更新");
$smarty->assign("session", $session);

$smarty->assign("is_update", true);

$smarty->assign("archive", $archive);
$smarty->assign("parent", $parent);
$smarty->assign("path", $path);

//親フォルダ
$smarty->assign("cate_id", $cate_id);
$smarty->assign("folder_id", $folder_id);
if ($cate_id != "" && $folder_id != ""){
    $smarty->assign("cate_nm", lib_get_category_name($con, $cate_id, $fname));
}
$smarty->assign("folder_path", lib_get_folder_path($con, $parent, $fname));

//フォルダ
$smarty->assign("folder_name", h($folder_name));

//------------------------------------
//参照可能範囲・更新可能範囲共通
//------------------------------------
//部署
$smarty->assign("sel_class", pg_fetch_all($sel_class));

$tmp_atrb = array();
foreach(pg_fetch_all($sel_atrb) as $atrb){
    $tmp_atrb[ $atrb['class_id'] ][ $atrb['atrb_id'] ] = $atrb['atrb_nm'];
}
$smarty->assign("sel_atrb", $tmp_atrb);

$tmp_dept = array();
foreach(pg_fetch_all($sel_dept) as $dept){
    $tmp_dept[ $dept['class_id'] ][ $dept['atrb_id'] ][ $dept['dept_id'] ] = $dept['dept_nm'];
}
$smarty->assign("sel_dept", $tmp_dept);

$smarty->assign("arr_class_name", $arr_class_name);
$smarty->assign("dept_names", $dept_names);

//役職
$smarty->assign("sel_st", pg_fetch_all($sel_st));

//職員
$smarty->assign("emp_id", $emp_id);
$smarty->assign("emp_name", $emp_name);
$smarty->assign("arr_target", $arr_target);

//------------------------------------
//参照可能範囲
//------------------------------------
$smarty->assign("ref_toggle_mode", $ref_toggle_mode);
$smarty->assign("ref_toggle_display", $ref_toggle_display);

//部署
$smarty->assign("ref_class_src", $ref_class_src);
$smarty->assign("ref_atrb_src", $ref_atrb_src);
$smarty->assign("ref_dept_st_flg", $ref_dept_st_flg);
$smarty->assign("ref_dept_flg", $ref_dept_flg);
$smarty->assign("ref_dept", $ref_dept);

//役職
$smarty->assign("ref_st_flg", $ref_st_flg);
$smarty->assign("ref_st", $ref_st);

//------------------------------------
//更新可能範囲
//------------------------------------
$smarty->assign("upd_toggle_mode", $upd_toggle_mode);
$smarty->assign("upd_toggle_display", $upd_toggle_display);

//部署
$smarty->assign("upd_class_src", $upd_class_src);
$smarty->assign("upd_atrb_src", $upd_atrb_src);
$smarty->assign("upd_dept_st_flg", $upd_dept_st_flg);
$smarty->assign("upd_dept_flg", $upd_dept_flg);
$smarty->assign("upd_dept", $upd_dept);

//役職
$smarty->assign("upd_st_flg", $upd_st_flg);
$smarty->assign("upd_st", $upd_st);

//更新ボタン
$smarty->assign("edit_button_disabled", $edit_button_disabled);

if ($design_mode == 1){
    $smarty->display("hiyari_el_folder1.tpl");
}
else{
    $smarty->display("hiyari_el_folder2.tpl");
}

pg_close($con);
