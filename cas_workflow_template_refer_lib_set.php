<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<?
require("./about_session.php");
require("./about_authority.php");
require_once("cas_common.ini");


$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"./js/showpage.js\"></script>");
	echo("<script language=\"javascript\">showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$checkauth = check_authority($session, $CAS_MANAGE_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"./js/showpage.js\"></script>");
	echo("<script language=\"javascript\">showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);

$sql = "select * from cas_wkfwlib ";
$cond = "where wkfwlib_id = '$wkfwlib_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$wkfw_title = pg_fetch_result($sel, 0, "wkfwlib_name");
$wkfw_content = pg_fetch_result($sel, 0, "wkfwlib_content");

// wkfw_content 保存
$ext = ".php";
$savefilename = "cas_workflow/tmp/{$session}_t{$ext}";

// 内容書き込み
$fp = fopen($savefilename, "w");
fwrite($fp, $wkfw_content, 2000000);

fclose($fp);


$content = str_replace("\\","\\\\", $wkfw_content);
$content = str_replace("\r\n","\n", $content);
$content = str_replace("\n","\\n", $content);
$content = str_replace("\"","\\\"", $content);
$content = eregi_replace("/(script)", "_\\1", $content);

?>
<script type="text/javascript">
opener.document.wkfw.wkfw_content.value = "<? echo($content); ?>";
if (opener.document.wkfw.wkfw_title.value == "") {
	opener.document.wkfw.wkfw_title.value = "<? echo($wkfw_title); ?>";
}
opener.document.wkfw.wkfw_content.value = "<? echo($content); ?>";

setTimeout("self.close();", 200);
</script>

</body>
<? pg_close($con); ?>
</html>
