<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="project_wg_register.php" method="post">
<input type="hidden" name="project_parent_id" value="<? echo($project_parent_id); ?>">
<input type="hidden" name="project_name" value="<? echo($project_name); ?>">
<input type="hidden" name="project_name_abbrev" value="<? echo($project_name_abbrev); ?>">
<input type="hidden" name="public_flag" value="<? echo($public_flag); ?>">
<input type="hidden" name="s_dt1" value="<? echo($s_dt1); ?>">
<input type="hidden" name="s_dt2" value="<? echo($s_dt2); ?>">
<input type="hidden" name="s_dt3" value="<? echo($s_dt3); ?>">
<input type="hidden" name="e_dt1" value="<? echo($e_dt1); ?>">
<input type="hidden" name="e_dt2" value="<? echo($e_dt2); ?>">
<input type="hidden" name="e_dt3" value="<? echo($e_dt3); ?>">
<input type="hidden" name="real_e_dt1" value="<? echo($real_e_dt1); ?>">
<input type="hidden" name="real_e_dt2" value="<? echo($real_e_dt2); ?>">
<input type="hidden" name="real_e_dt3" value="<? echo($real_e_dt3); ?>">
<input type="hidden" name="incharge" value="<? echo($incharge); ?>">
<input type="hidden" name="incharge_id" value="<? echo($incharge_id); ?>">
<input type="hidden" name="mygroup" value="<? echo($mygroup); ?>">
<input type="hidden" name="target_id_list1" value="<? echo($target_id_list1); ?>">
<input type="hidden" name="target_id_list2" value="<? echo($target_id_list2); ?>">
<input type="hidden" name="project_content" value="<? echo($project_content); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="schd_emp_id" value="<? echo($schd_emp_id); ?>">
<input type="hidden" name="adm_flg" value="<? echo($adm_flg); ?>">
<input type="hidden" name="ovtm_approve_num" value="<? echo($ovtm_approve_num); ?>">
<?
for ($i = 0; $i < $ovtm_approve_num; $i++) {
	$j = $i + 1;
	
	
	$str_emp_id = "ovtm_emp_id".$j;
	$str_emp_nm = "ovtm_emp_nm".$j;
	$str_content = "ovtm_approve_content".$j;
	$str_notice = "ovtm_next_notice_div".$j;
	
	echo("<input type=\"hidden\" name=\"ovtm_approve_content" . $j . "\" value=\"".$$str_content."\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_emp_id" . $j . "\" value=\"".$$str_emp_id."\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_emp_nm" . $j . "\" value=\"".$$str_emp_nm."\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_next_notice_div" . $j . "\" value=\"".$$str_notice."\">\n");
}
?>



</form>
<?
require("about_session.php");
require("about_authority.php");
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("library_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 委員会・WG権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック
if ($project_parent_id == "-") {
	echo("<script language=\"javascript\">alert(\"委員会を選択してください。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}
if ($project_name == "") {
	echo("<script language=\"javascript\">alert(\"WG名が入力されていません。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($project_name) > 50) {
	echo("<script language=\"javascript\">alert(\"WG名が長すぎます。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}
if ($project_name_abbrev == "") {
	echo("<script language=\"javascript\">alert(\"略称が入力されていません。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($project_name_abbrev) > 50) {
	echo("<script language=\"javascript\">alert(\"略称が長すぎます。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}
if ($public_flag == "") {
	echo("<script language=\"javascript\">alert(\"公開または非公開を選択してください。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}
if (!checkdate($s_dt2, $s_dt3, $s_dt1)) {
	echo("<script language=\"javascript\">alert(\"開始日が不正です。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}
if ($e_dt1 != "-" || $e_dt2 != "-" || $e_dt3 != "-") {
	if (!checkdate($e_dt2, $e_dt3, $e_dt1)) {
		echo("<script language=\"javascript\">alert(\"終了予定日が不正です。\");</script>");
		echo("<script language=\"javascript\">document.items.submit();</script>");
		exit;
	}
}
if ($real_e_dt1 != "-" || $real_e_dt2 != "-" || $real_e_dt3 != "-") {
	if (!checkdate($real_e_dt2, $real_e_dt3, $real_e_dt1)) {
		echo("<script language=\"javascript\">alert(\"終了日が不正です。\");</script>");
		echo("<script language=\"javascript\">document.items.submit();</script>");
		exit;
	}
}
$s_date = "$s_dt1$s_dt2$s_dt3";
if ($e_dt1 != "-" || $e_dt2 != "-" || $e_dt3 != "-") {
	$e_date = "$e_dt1$e_dt2$e_dt3";
	if ($s_date > $e_date) {
		echo("<script language=\"javascript\">alert(\"終了予定日は開始日より後にしてください。\");</script>");
		echo("<script language=\"javascript\">document.items.submit();</script>");
		exit;
	}
} else {
	$e_date = null;
}
if ($real_e_dt1 != "-" || $real_e_dt2 != "-" || $real_e_dt3 != "-") {
	$real_e_date = "$real_e_dt1$real_e_dt2$real_e_dt3";
	if ($s_date > $real_e_date) {
		echo("<script language=\"javascript\">alert(\"終了日は開始日より後にしてください。\");</script>");
		echo("<script language=\"javascript\">document.items.submit();</script>");
		exit;
	}
} else {
	$real_e_date = "";
}
if ($incharge_id == "") {
	echo("<script language=\"javascript\">alert(\"責任者が選択されていません。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}
$arr_target = array();
if ($target_id_list1 != "") {
	$arr_target_id = split(",", $target_id_list1);

	for ($i = 0; $i < count($arr_target_id); $i++) {
		array_push($arr_target, array("id" => $arr_target_id[$i], "kind" => "1"));
	}

}

if ($target_id_list2 != "") {
	$arr_target_id = split(",", $target_id_list2);

	for ($i = 0; $i < count($arr_target_id); $i++) {
		array_push($arr_target, array("id" => $arr_target_id[$i], "kind" => "2"));
	}
}

if (strlen($project_content) > 400) {
	echo("<script language=\"javascript\">alert(\"内容が長すぎます。\");</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
	exit;
}

$chk_apv_exist_flg = true;
for ($i = 0; $i < $ovtm_approve_num; $i++) 
{
	$idx = $i + 1;
	
	$approve = "ovtm_emp_id$idx";
	if($$approve == "") 
	{
		$chk_apv_exist_flg = false;	 	
	}	
	
}
if(!$chk_apv_exist_flg) {
	echo("<script type=\"text/javascript\">alert('承認階層に承認者を設定してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// トランザクションの開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// WGのIDを採番
$sql = "select max(pjt_id) from project";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$max = pg_result($sel, 0, 0);
if ($max == "") {
	$pjt_id = 1;
} else {
	$pjt_id = $max + 1;
}

// 登録値の編集
if ($public_flag != "t") {
	$public_flag = "f";
	$private_flag = "t";
} else {
    // 委員会デフォルトチェックフラグを取得
    $pjt_private_flg = lib_get_pjt_private_flg($con, $fname);
    if ($pjt_private_flg == 't') {
        $private_flag = "t";
    } else {
        $private_flag = "f";
    }
}


//委員会のソート番号(pjt_sort_id)を採番する
$sql = "select max(wg_sort_id) from project";
$cond = " where pjt_parent_id =".$project_parent_id." and pjt_delete_flag = 'f' ";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$max = pg_result($sel, 0, 0);
if ($max == "") {
	$sort_id = 1;
} else {
	$sort_id = $max + 1;
}


// WG情報を作成
$sql = "insert into project (pjt_id, pjt_name, pjt_start_date, pjt_end_date, pjt_real_end_date, pjt_response, 
		pjt_public_flag, pjt_content, pjt_delete_flag, pjt_name_abbrev, pjt_reg_emp_id, pjt_parent_id, wg_sort_id) values(";
$content = array($pjt_id, $project_name, $s_date, $e_date, $real_e_date, $incharge_id, $public_flag, 
		$project_content, "f", $project_name_abbrev, $emp_id, $project_parent_id, $sort_id);
$in = insert_into_table($con, $sql, $content, $fname);
if ($in == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// メンバー情報を作成
$mem_id = 1;
foreach ($arr_target as $row) {
	$sql = "insert into promember (pjt_id, pjt_member_id, emp_id, member_kind) values(";
	$content = array($pjt_id, $mem_id, $row["id"], $row["kind"]);
	$in = insert_into_table($con, $sql, $content, $fname);
	if ($in == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$mem_id++;
}

// 文書カテゴリIDの採番
$sql = "select max(lib_cate_id) from libcate";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$max = pg_result($sel, 0, 0);
if ($max == "") {
	$lib_cate_id = 1;
} else {
	$lib_cate_id = $max + 1;
}

// 文書カテゴリ情報の作成
$sql = "insert into libcate (lib_archive, lib_cate_id, lib_link_id, lib_cate_nm, private_flg) values (";
$content = array("4", $lib_cate_id, $pjt_id, $project_name, $private_flag);
$in = insert_into_table($con, $sql, $content, $fname);
if ($in == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

/*
// デフォルトで設定する承認者を更新
$arr_target3 = array();
if ($target_id_list3 != "") {
	$arr_target_id3 = split(",", $target_id_list3);
	
	for ($i = 0; $i < count($arr_target_id3); $i++) {
		array_push($arr_target3, array("id" => $arr_target_id3[$i]));
	}
	
}
//→承認者を登録する
foreach ($arr_target3 as $row) {
	$sql = "insert into prcdaprv_default (pjt_id, prcdaprv_emp_id) values(";
	$content = array($pjt_id, $row["id"]);
	$in = insert_into_table($con, $sql, $content, $fname);
	if ($in == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}
*/

//→承認者を登録する
for ($i = 1; $i <= $ovtm_approve_num; $i++) 
{
	
	$str_ap_emp_id = "ovtm_emp_id".$i;
	$str_ap_notice_id = "ovtm_next_notice_div".$i;
	
	
	//承認階層の種別を登録
	$sql = "insert into prcdaprv_mng_default (pjt_id, prcdaprv_order, next_notice_div) values(";
	$content = array($pjt_id, $i, $$str_ap_notice_id);
	$in = insert_into_table($con, $sql, $content, $fname);
	if ($in == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	
	$arr_app_id = split(",", $$str_ap_emp_id);
	$arr_app = array();
	$set_arr_app = array();
	
	for ($cnt = 0; $cnt < count($arr_app_id); $cnt++) 
	{
		array_push($set_arr_app, array("prcdaprv_emp_id" => $arr_app_id[$cnt]));
	}
	
	//→承認者を登録する
	$no_cnt = 1;
	foreach ($set_arr_app as $row) 
	{
		$sql = "insert into prcdaprv_default (pjt_id, prcdaprv_no, prcdaprv_emp_id, prcdaprv_order) values(";
		$content = array($pjt_id, $no_cnt, $row["prcdaprv_emp_id"], $i);
		$in = insert_into_table($con, $sql, $content, $fname);
		if ($in == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$no_cnt++;
	}
	
}



// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 文書管理用ディレクトリを作成
if (!is_dir("docArchive")) {
	mkdir("docArchive", 0755);
}
if (!is_dir("docArchive/project")) {
	mkdir("docArchive/project", 0755);
}
mkdir("docArchive/project/cate$lib_cate_id", 0755);

// 委員会・WG一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'project_menu_adm.php?session=$session';</script>");
?>
</body>
