<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("duty_shift_common_class.php");
require_once("show_class_name.ini");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);
$obj = new duty_shift_common_class($con, $fname);

// 権限のチェック
if ($obj->check_authority_Management($session, $fname) == "") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

pg_query($con, "begin");

$arr_class_name = get_class_name_array($con, $fname);
$class_cnt = $arr_class_name["class_cnt"];

// リンク設定の取得
$sql = "select * from duty_shift_sync_link";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$links = array();
while ($row = pg_fetch_array($sel)) {
	$links[$row["group_id"]] = array(
		"class_id" => $row["class_id"],
		"atrb_id" => $row["atrb_id"],
		"dept_id" => $row["dept_id"],
		"room_id" => ($row["room_id"] == "" || $class_cnt == 3) ? null : $row["room_id"]
	);
}

// 勤務シフト → 職員所属
if ($mode == 1) {
	foreach ($links as $tmp_group_id => $tmp_link) {
		$sql = "update empmst set";
		$set = array("emp_class", "emp_attribute", "emp_dept", "emp_room");
		$setvalue = array($tmp_link["class_id"], $tmp_link["atrb_id"], $tmp_link["dept_id"], $tmp_link["room_id"]);
		$cond = "where exists (select * from duty_shift_staff s where s.emp_id = empmst.emp_id and s.group_id = '$tmp_group_id') and exists (select * from authmst a where a.emp_id = empmst.emp_id and a.emp_del_flg = false)";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

// 職員所属 → 勤務シフト
} else if ($mode == 2) {

	// シフトグループが変わる職員の情報を取得（シフトグループからの削除は考慮しない）
	$sql = "select e.emp_id, s.group_id as cur_group_id, cg.pattern_id as cur_pattern_id, l.group_id as new_group_id, ng.pattern_id as new_pattern_id from empmst e inner join authmst a on e.emp_id = a.emp_id left join duty_shift_staff s on e.emp_id = s.emp_id left join duty_shift_group cg on s.group_id = cg.group_id left join duty_shift_sync_link l on e.emp_class = l.class_id and e.emp_attribute = l.atrb_id and e.emp_dept = l.dept_id and ((e.emp_room is null and l.room_id is null) or (e.emp_room = l.room_id)) left join duty_shift_group ng on l.group_id = ng.group_id";
	$cond = "where (not a.emp_del_flg) and l.group_id is not null and l.group_id <> '' and ((s.group_id is null or s.group_id = '') or (s.group_id <> l.group_id))";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	while ($row = pg_fetch_array($sel)) {
		$tmp_emp_id = $row["emp_id"];
		$tmp_cur_group_id = $row["cur_group_id"];
		$tmp_cur_pattern_id = $row["cur_pattern_id"];
		$tmp_new_group_id = $row["new_group_id"];
		$tmp_new_pattern_id = $row["new_pattern_id"];

		// 別のグループに移動
		$sql = "delete from duty_shift_staff";
		$cond = "where emp_id = '$tmp_emp_id'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
        // シフトグループ履歴作成
        if ($tmp_cur_group_id != "") {
            $sql = "insert into duty_shift_staff_history (emp_id, group_id, histdate) values(";
            $content = array($tmp_emp_id, $tmp_cur_group_id, date('Y-m-d H:i:s'));
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_query($con,"rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }

        $sql = "select count(*) from duty_shift_staff";
		$cond = "where group_id = '$tmp_new_group_id'";
		$sel_count = select_from_table($con, $sql, $cond, $fname);
		if ($sel_count == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_no = intval(pg_fetch_result($sel_count, 0, 0)) + 1;
		$sql = "insert into duty_shift_staff (group_id, emp_id, no) values (";
		$content = array($tmp_new_group_id, $tmp_emp_id, $tmp_no);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 勤務パターングループが変わらない場合、勤務条件をコピー
		if ($tmp_cur_pattern_id == $tmp_new_pattern_id) {
			$sql = "update duty_shift_staff_employment set";
			$set = array("group_id");
			$setvalue = array($tmp_new_group_id);
			$cond = "where group_id = '$tmp_cur_group_id' and emp_id = '$tmp_emp_id'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$sql = "update duty_shift_staff_employment_atdptn set";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

		// 勤務パターングループが変わる場合、勤務条件を削除
		} else {
			$sql = "delete from duty_shift_staff_employment";
			$cond = "where emp_id = '$tmp_emp_id'";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$sql = "delete from duty_shift_staff_employment_atdptn";
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
}

pg_query($con, "commit");
pg_close($con);

echo("<script type=\"text/javascript\">location.href = 'duty_shift_sync.php?session=$session&mode=$mode&result=success';</script>");
