<?
require_once("about_comedix.php");
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="config_register.php" method="post">
<input type="hidden" name="back" value="t">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="login_page_reflesh" value="<? echo($login_page_reflesh); ?>">
<input type="hidden" name="interval" value="<? echo($interval); ?>">
<input type="hidden" name="login_color" value="<? echo($login_color); ?>">
<input type="hidden" name="login_pos" value="<? echo($login_pos); ?>">
<input type="hidden" name="timecard_use" value="<? echo($timecard_use); ?>">
<input type="hidden" name="title" value="<? echo($title); ?>">
<input type="hidden" name="tmcd_chg_flg" value="<? echo($tmcd_chg_flg); ?>">
<input type="hidden" name="ret_btn_flg" value="<? echo($ret_btn_flg); ?>">
<input type="hidden" name="login_focus" value="<? echo($login_focus); ?>">
<input type="hidden" name="ic_read_flg" value="<? echo($ic_read_flg); ?>">
<input type="hidden" name="text_show_flg" value="<? echo($text_show_flg); ?>">
<input type="hidden" name="free_text" value="<? echo(h($free_text)); ?>">
<input type="hidden" name="event_show_flg" value="<? echo($event_show_flg); ?>">
<input type="hidden" name="area_show_flg" value="<? echo($area_show_flg); ?>">
<input type="hidden" name="allot_show_flg" value="<? echo($allot_show_flg); ?>">
<input type="hidden" name="welfare_show_flg" value="<? echo($welfare_show_flg); ?>">
<input type="hidden" name="lib_show_flg" value="<? echo($lib_show_flg); ?>">
<input type="hidden" name="qa_show_flg" value="<? echo($qa_show_flg); ?>">
<input type="hidden" name="user_intra_label_qa" value="<? echo($user_intra_label_qa); ?>">
<input type="hidden" name="stat_show_flg" value="<? echo($stat_show_flg); ?>">
<input type="hidden" name="rsv_show_flg" value="<? echo($rsv_show_flg); ?>">
<input type="hidden" name="info_show_flg" value="<? echo($info_show_flg); ?>">
<input type="hidden" name="cal_show_flg" value="<? echo($cal_show_flg); ?>">
<input type="hidden" name="weather_show_flg" value="<? echo($weather_show_flg); ?>">
<input type="hidden" name="wic_show_flg" value="<? echo($wic_show_flg); ?>">
<input type="hidden" name="ext_search_show_flg" value="<? echo($ext_search_show_flg); ?>">
<input type="hidden" name="link_show_flg" value="<? echo($link_show_flg); ?>">
<input type="hidden" name="extension_show_flg" value="<? echo($extension_show_flg); ?>">
<input type="hidden" name="user_intra_label_ext" value="<? echo($user_intra_label_ext); ?>">
<input type="hidden" name="news_new_days" value="<? echo($news_new_days); ?>">
<input type="hidden" name="cal_start_wd" value="<? echo($cal_start_wd); ?>">
<input type="hidden" name="weather_area" value="<? echo($weather_area); ?>">
<?
foreach ($block_id as $tmp_block_id) {
	$varname = "order_no_$tmp_block_id";
?>
<input type="hidden" name="block_id[]" value="<? echo($tmp_block_id); ?>">
<input type="hidden" name="<? echo($varname); ?>" value="<? echo($$varname); ?>">
<?
}
?>
</form>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 20, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($interval == "t") {
	if ($login_page_reflesh == "") {
		echo("<script type=\"text/javascript\">alert('更新間隔を入力してください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}

	if (preg_match("/^\d{1,3}$/", $login_page_reflesh) == 0) {
		echo("<script type=\"text/javascript\">alert('更新間隔は3桁以下の半角数字で入力してください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}

	if ($login_page_reflesh < 30) {
		echo("<script type=\"text/javascript\">alert('更新間隔は30秒以上で入力してください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}
}
if ($login_pos == "1") {
	if ($timecard_use == "f") {
		if ($title == "") {
			echo("<script type=\"text/javascript\">alert('タイトルを入力してください。');</script>");
			echo("<script type=\"text/javascript\">document.items.submit();</script>");
			exit;
		}

		if (strlen($title) > 50) {
			echo("<script type=\"text/javascript\">alert('タイトルが長すぎます。');</script>");
			echo("<script type=\"text/javascript\">document.items.submit();</script>");
			exit;
		}
	}
}
foreach ($block_id as $tmp_block_id) {
	$varname = "order_no_$tmp_block_id";
	$$varname = intval($$varname);
	if ($$varname == 0) {
		$$varname = null;
	}
	if ($$varname > 99) {
		echo("<script type=\"text/javascript\">alert('表示順は2桁までにしてください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}
}

// 登録値の編集
if ($login_page_reflesh == "") {$login_page_reflesh = "0";}
if ($timecard_use == "") {$timecard_use = "f";}
if ($tmcd_chg_flg == "") {$tmcd_chg_flg = "f";}
if ($ret_btn_flg == "") {$ret_btn_flg = "f";}
if ($text_show_flg == "") {$text_show_flg = "f";}
if ($event_show_flg == "") {$event_show_flg = "f";}
if ($area_show_flg == "") {$area_show_flg = "f";}
if ($allot_show_flg == "") {$allot_show_flg = "f";}
if ($welfare_show_flg == "") {$welfare_show_flg = "f";}
if ($lib_show_flg == "") {$lib_show_flg = "f";}
if ($qa_show_flg == "") {$qa_show_flg = "f";}
if ($user_intra_label_qa == "") {$user_intra_label_qa = "f";}
if ($stat_show_flg == "") {$stat_show_flg = "f";}
if ($rsv_show_flg == "") {$rsv_show_flg = "f";}
if ($info_show_flg == "") {$info_show_flg = "f";}
if ($cal_show_flg == "") {$cal_show_flg = "f";}
if ($weather_show_flg == "") {$weather_show_flg = "f";}
if ($wic_show_flg == "") {$wic_show_flg = "f";}
if ($ext_search_show_flg == "") {$ext_search_show_flg = "f";}
if ($link_show_flg == "") {$link_show_flg = "f";}
if ($extension_show_flg == "") {$extension_show_flg = "f";}
if ($user_intra_label_ext == "") {$user_intra_label_ext = "f";}
if ($news_new_days == "") {$news_new_days = null;}
if ($cal_start_wd == "") {$cal_start_wd = "1";}
if ($weather_area == "") {$weather_area = "4410";}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 環境設定情報を更新
$sql = "update config set";
$set = array("login_page_reflesh", "interval", "login_color", "login_pos", "timecard_use", "title", "tmcd_chg_flg", "ret_btn_flg", "login_focus", "ic_read_flg", "text_show_flg", "free_text", "event_show_flg", "area_show_flg", "allot_show_flg", "welfare_show_flg", "lib_show_flg", "qa_show_flg", "stat_show_flg", "rsv_show_flg", "info_show_flg", "cal_show_flg", "weather_show_flg", "wic_show_flg", "ext_search_show_flg", "link_show_flg", "extension_show_flg", "news_new_days", "cal_start_wd", "weather_area", "user_intra_label_qa", "user_intra_label_ext");
$setvalue = array($login_page_reflesh, $interval, $login_color, $login_pos, $timecard_use, $title, $tmcd_chg_flg, $ret_btn_flg, $login_focus, $ic_read_flg, $text_show_flg, p($free_text), $event_show_flg, $area_show_flg, $allot_show_flg, $welfare_show_flg, $lib_show_flg, $qa_show_flg, $stat_show_flg, $rsv_show_flg, $info_show_flg, $cal_show_flg, $weather_show_flg, $wic_show_flg, $ext_search_show_flg, $link_show_flg, $extension_show_flg, $news_new_days, $cal_start_wd, $weather_area, $user_intra_label_qa, $user_intra_label_ext);
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// ログイン画面ブロック情報をDELETE〜INSERT
$sql = "delete from loginblock";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($block_id as $tmp_block_id) {
	$varname = "order_no_$tmp_block_id";
	$sql = "insert into loginblock (block_id, order_no) values (";
	$content = array($tmp_block_id, $$varname);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'config_register.php?session=$session';</script>");
?>
</body>
