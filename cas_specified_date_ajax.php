<?
ob_start();
require("about_authority.php");
require("about_session.php");
ob_end_clean();

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
/*
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	ajax_server_erorr();
	exit;
}
*/
/**
 * 
 * 指定日付取得
 * @param $specified_date 指定日付 
 * 
 */

// 6ヵ月後の一日前
$half_year_later = date("Y/m/d",strtotime("+6 month" ,strtotime($specified_date)));
$half_year_later = date("Y/m/d",strtotime("-1 day" ,strtotime($half_year_later)));

// 6ヵ月後の一年前から1日後
$one_year_ago    = date("Y/m/d",strtotime("-1 year" ,strtotime($half_year_later)));
$one_year_ago    = date("Y/m/d",strtotime("+1 day" ,strtotime($one_year_ago)));

// 6ヵ月後の一日前から21日後
$three_weeks_later = date("Y/m/d",strtotime("+21 day" ,strtotime($half_year_later)));



// 6ヵ月後
$arr_date1 = split("/", $half_year_later);
$year1 = $arr_date1[0];
$month1 = $arr_date1[1];
$day1 = $arr_date1[2];


// 1年前
$arr_date2 = split("/", $one_year_ago);
$year2 = $arr_date2[0];
$month2 = $arr_date2[1];
$day2 = $arr_date2[2];

// 6ヵ月後の一日前から21日後
$arr_date3 = split("/", $three_weeks_later);
$year3 = $arr_date3[0];
$month3 = $arr_date3[1];
$day3 = $arr_date3[2];


//レスポンスデータ作成

$response_data = "ajax=success\n";
	
$p_key = "year1";
$p_value = $year1;
$response_data .= "$p_key=$p_value\n";

$p_key = "month1";
$p_value = $month1;
$response_data .= "$p_key=$p_value\n";

$p_key = "day1";
$p_value = $day1;
$response_data .= "$p_key=$p_value\n";

$p_key = "year2";
$p_value = $year2;
$response_data .= "$p_key=$p_value\n";

$p_key = "month2";
$p_value = $month2;
$response_data .= "$p_key=$p_value\n";

$p_key = "day2";
$p_value = $day2;
$response_data .= "$p_key=$p_value\n";

$p_key = "year3";
$p_value = $year3;
$response_data .= "$p_key=$p_value\n";

$p_key = "month3";
$p_value = $month3;
$response_data .= "$p_key=$p_value\n";

$p_key = "day3";
$p_value = $day3;
$response_data .= "$p_key=$p_value\n";

	
print $response_data;






function ajax_server_erorr()
{
	print "ajax=error\n";
	exit;
}

?>
