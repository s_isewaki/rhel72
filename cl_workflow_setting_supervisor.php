<?
//ini_set( 'display_errors', 1 );
require_once("about_session.php");
require_once("about_authority.php");
require_once("cl_workflow_common.ini");
require_once("cl_title_name.ini");
require_once("cl_common_log_class.inc");
require_once("cl_smarty_setting.ini");
require_once("Cmx.php");
require_once("MDB2.php");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

$smarty = cl_get_smarty_obj();
$smarty->template_dir = dirname(__FILE__) . "/cl/view";

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

$cl_title = cl_title_name();

/**
 * アプリケーションメニューHTML取得
 */
$wkfwctn_mnitm_str=get_wkfw_menuitem($session,$fname);

/**
 * Javascriptコード生成（動的オプション項目変更用）
 */
$js_str = get_js_str($arr_wkfwcatemst,$workflow);

/**
 * データベース接続オブジェクト取得
 */
$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
}
$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

/**
 * ログインユーザ情報取得
 */
require_once(dirname(__FILE__) . "/cl/model/workflow/empmst_model.php");
$empmst_model = new empmst_model($mdb2);
$arr_login = $empmst_model->select_login_emp($session);
$login_emp_id = $arr_login["emp_id"];

/**
 * 所属長部署
 */
require_once(dirname(__FILE__) . "/cl/model/master/cl_mst_supervisor_class_model.php");
$supervisor_class_model = new cl_mst_supervisor_class_model($mdb2, $login_emp_id);

if ($_POST['regist_flg'] == 'true') {

	/**
	 * 所属長役職
	 */
	require_once(dirname(__FILE__) . "/cl/model/master/cl_mst_supervisor_st_model.php");
	$supervisor_st_model = new cl_mst_supervisor_st_model($mdb2, $login_emp_id);

	/**
	 * トランザクション開始
	 */
	$mdb2->beginTransaction();
	$log->debug("トランザクション開始",__FILE__,__LINE__);

	/**
	 * 所属長部署削除
	 */
	$supervisor_class_model->physical_delete();

	/**
	 * 所属長役職削除
	 */
	$supervisor_st_model->physical_delete();

	/**
	 * 所属長部署登録
	 */
	$supervisor_class_model->insert($_POST['target_class_div']);

	/**
	 * 所属長役職登録
	 */
	$arr_field_name = array_keys($_POST);
	foreach($arr_field_name as $field_name){
		$pos = strpos($field_name,'target_st');
		if ($pos === 0) {
			$supervisor_st_model->insert($_POST[$field_name]);
		}
	}

	/**
	 * コミット処理
	 */
	$mdb2->commit();
	$log->debug("コミット",__FILE__,__LINE__);

}

/**
 * 登録フラグを初期化
 */
$regist_flg = '';

/**
 * 組織階層情報取得
 */
require_once(dirname(__FILE__) . "/cl/model/search/classname_model.php");
$classname_model = new classname_model($mdb2, $login_emp_id);
$arr_classname = $classname_model->getClassname();

/**
 * 役職マスタ取得
 */
require_once(dirname(__FILE__) . "/cl/model/search/stmst_model.php");
$stmst_model = new stmst_model($mdb2, $login_emp_id);
$arr_stmst = $stmst_model->getList_supervisor();

/**
 * 所属長部署取得
 */
$arr_supervisor_class = $supervisor_class_model->select();

$target_class = '';
$target_atrb = '';
$target_dept = '';
$target_room = '';
$target_none = '';

switch ($arr_supervisor_class['class_division']){
	case 1:
		$target_class = 'checked';
		break;
	case 2:
		$target_atrb = 'checked';
		break;
	case 3:
		$target_dept = 'checked';
		break;
	case 4:
		$target_room = 'checked';
		break;
	case 0:
	default:
		$target_none = 'checked';
		break;
}

// DB切断
$mdb2->disconnect();

/**
 * テンプレートマッピング
 */
$log->debug("テンプレートマッピング開始",__FILE__,__LINE__);

$smarty->assign( 'cl_title'				, $cl_title						);
$smarty->assign( 'session'				, $session						);
$smarty->assign( 'regist_flg'			, $regist_flg					);
$smarty->assign( 'wkfwctn_mnitm_str'	, $wkfwctn_mnitm_str			);
$smarty->assign( 'js_str'				, $js_str						);
$smarty->assign( 'level_cnt'			, $arr_classname['class_cnt']	);
$smarty->assign( 'class_nm'				, $arr_classname['class_nm'	]	);
$smarty->assign( 'atrb_nm'				, $arr_classname['atrb_nm'	]	);
$smarty->assign( 'dept_nm'				, $arr_classname['dept_nm'	]	);
$smarty->assign( 'room_nm'				, $arr_classname['room_nm'	]	);
$smarty->assign( 'target_class'			, $target_class					);
$smarty->assign( 'target_atrb'			, $target_atrb					);
$smarty->assign( 'target_dept'			, $target_dept					);
$smarty->assign( 'target_room'			, $target_room					);
$smarty->assign( 'target_none'			, $target_none					);
$smarty->assign( 'data'					, $arr_stmst					);

$log->debug("テンプレートマッピング終了",__FILE__,__LINE__);

/**
 * テンプレート出力
 */
$log->debug("テンプレート出力開始",__FILE__,__LINE__);

$smarty->display(basename($_SERVER['PHP_SELF'],'.php').".tpl");

$log->debug("テンプレート出力終了",__FILE__,__LINE__);

/**
 * アプリケーションメニューHTML取得
 */
function get_wkfw_menuitem($session,$fname)
{
	ob_start();
	show_wkfw_menuitem($session,$fname,"");
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * Javascript文字列取得
 */
function get_js_str($arr_wkfwcatemst,$workflow)
{
	$str_buff[]  = "";
	foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) {
		$str_buff[] .= "\n\t\tif (cate_id == '".$tmp_cate_id."') {\n";


		 foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm){
			$tmp_wkfw_nm = htmlspecialchars($tmp_wkfw_nm, ENT_QUOTES);
			$tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
			$tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);

			$str_buff[] .= "\t\t\taddOption(obj_wkfw, '".$tmp_wkfw_id."', '".$tmp_wkfw_nm."',  '".$workflow."');\n";

		}

		$str_buff[] .= "\t\t}\n";
	}

	return implode('', $str_buff);
}

$log->info(basename(__FILE__)." END");
$log->shutdown();
