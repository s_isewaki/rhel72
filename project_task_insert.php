<?
require("about_authority.php");
require("about_session.php");
?>
<body>
<form name="items" action="project_task_register.php" method="post">
<input type="hidden" name="back" value="t">
<input type="hidden" name="work_name" value="<? echo($work_name); ?>">
<input type="hidden" name="start_date_y" value="<? echo($start_date_y); ?>">
<input type="hidden" name="start_date_m" value="<? echo($start_date_m); ?>">
<input type="hidden" name="start_date_d" value="<? echo($start_date_d); ?>">
<input type="hidden" name="end_date_y" value="<? echo($end_date_y); ?>">
<input type="hidden" name="end_date_m" value="<? echo($end_date_m); ?>">
<input type="hidden" name="end_date_d" value="<? echo($end_date_d); ?>">
<input type="hidden" name="detail" value="<? echo($detail); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pjt_id" value="<? echo($pjt_id); ?>">
<input type="hidden" name="target_id_list" value="<? echo($target_id_list); ?>">
<input type="hidden" name="adm_flg" value="<? echo($adm_flg); ?>">
</form>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 委員会・WG権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($ward == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($work_name == "") {
	echo("<script type=\"text/javascript\">alert('作業名を入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($work_name) > 80) {
	echo("<script type=\"text/javascript\">alert('作業名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (!checkdate($start_date_m, $start_date_d, $start_date_y)) {
	echo("<script type=\"text/javascript\">alert('開始日が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (!checkdate($end_date_m, $end_date_d, $end_date_y)) {
	echo("<script type=\"text/javascript\">alert('終了予定日が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
$start_date = "$start_date_y$start_date_m$start_date_d";
$end_date = "$end_date_y$end_date_m$end_date_d";
if ($end_date < $start_date) {
	echo("<script type=\"text/javascript\">alert('終了予定日は開始日以降にしてください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($target_id_list == "") {
	echo("<script type=\"text/javascript\">alert('担当者を選択してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// タスクIDを採番
$sql = "select max(work_id) from prowork";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$work_id = pg_fetch_result($sel, 0, 0) + 1;

// タスク情報を登録
$sql = "insert into prowork (work_id, pjt_id, emp_id, work_name, work_status, work_start_date, work_expire_date, work_priority, work_detail, work_update) values (";
$content = array($work_id, $pjt_id, $emp_id, $work_name, "0", $start_date, $end_date, $priority, $detail, date("Ymd"));
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// タスク担当者情報を登録
$target_ids = split(",", $target_id_list);
foreach ($target_ids as $target_id) {
	$sql = "insert into proworkmem (work_id, emp_id) values (";
	$content = array($work_id, $target_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// タスク一覧画面を再表示
echo("<script type=\"text/javascript\">location.href = 'project_task_menu.php?session=$session&pjt_id=$pjt_id&adm_flg=$adm_flg';</script>");
?>
</body>
