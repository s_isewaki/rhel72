<?php
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("get_values.ini");
require_once("show_attendance_pattern.ini");
require_once("atdbk_common_class.php");
require_once("webmail_alias_functions.php");
require_once("settings_common.php");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 職員情報権限チェック
$empif = check_authority($session, 18, $fname);
if ($empif == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 職員登録権限を取得
$reg_auth = check_authority($session, 19, $fname);

// データベースに接続
$con = connect2db($fname);

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);
//javascript出力
$atdbk_common_class->setJavascript();

// 給与支給区分
$list_wage = array(
    "1" => "月給制",
//    "2" => "日給月給制",
    "3" => "日給制",
    "4" => "時間給制",
    "5" => "年俸制",
);

// 雇用・勤務形態
$list_duty_form = array(
        "1" => "常勤",
        "3" => "短時間正職員",
        "2" => "非常勤"
        );

// 雇用区分
$employment_working_other_flg = get_settings_value("employment_working_other_flg", "f");
if ($employment_working_other_flg == "t"){
	$list_duty_form_type = array(
		"1" => "正職員",
		"2" => "再雇用職員",
		"3" => "嘱託職員",
		"4" => "臨時職員",
		"5" => "パート職員"
	);
	
}

// 他部署兼務
$list_other_post_flg = array(
        "1" => "有り",
        "" => "無し"
        );


// 職員情報を取得
$infos = get_empmst($con, $emp_id, $fname);

if ($back != "t") {

    // 勤務条件情報を取得
    $sql = "select * from empcond";
    $cond = "where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) > 0) {
        $wage = pg_fetch_result($sel, 0, "wage");
        $tmcd_group_id = pg_fetch_result($sel, 0, "tmcd_group_id");
        $unit_price1 = pg_fetch_result($sel, 0, "unit_price1");
        $unit_price2 = pg_fetch_result($sel, 0, "unit_price2");
        $unit_price3 = pg_fetch_result($sel, 0, "unit_price3");
        $unit_price4 = pg_fetch_result($sel, 0, "unit_price4");
        $unit_price5 = pg_fetch_result($sel, 0, "unit_price5"); //交通費単価
        $unit_price6 = pg_fetch_result($sel, 0, "unit_price6"); //自転車等の交通費1日単価
        $unit_price7 = pg_fetch_result($sel, 0, "unit_price7"); //定期代
        $commuting_distance = pg_fetch_result($sel, 0, "commuting_distance"); //片道通勤距離
        $commuting_type = pg_fetch_result($sel, 0, "commuting_type"); //交通用具
        $irrg_type = pg_fetch_result($sel, 0, "irrg_type");
        $hol_minus = pg_fetch_result($sel, 0, "hol_minus");
        $duty_form = pg_fetch_result($sel, 0, "duty_form");  //勤務形態
        $atdptn_id = pg_fetch_result($sel, 0, "atdptn_id");
        $no_overtime = pg_fetch_result($sel, 0, "no_overtime");
        $day1_time = pg_fetch_result($sel, 0, "specified_time");
        $day1_hour = substr($day1_time, 0, 2);
        $day1_min = substr($day1_time, 2, 2);
        $paid_hol_tbl_id = pg_fetch_result($sel, 0, "paid_hol_tbl_id");
        $csv_layout_id = pg_fetch_result($sel, 0, "csv_layout_id");
        $paid_hol_start_date = pg_fetch_result($sel, 0, "paid_hol_start_date");
        $am_time = pg_fetch_result($sel, 0, "am_time");
        $am_hour = substr($am_time, 0, 2);
        $am_min = substr($am_time, 2, 2);
        $pm_time = pg_fetch_result($sel, 0, "pm_time");
        $pm_hour = substr($pm_time, 0, 2);
        $pm_min = substr($pm_time, 2, 2);
        $updated_on = pg_fetch_result($sel, 0, "updated_on");
        $other_post_flg = pg_fetch_result($sel, 0, "other_post_flg");  //他部署兼務
        $week_time = pg_fetch_result($sel, 0, "week_time");  			//週所定労働時間 20131111
        $week_hour = substr($week_time, 0, 2);
        $week_min = substr($week_time, 2, 2);
        $salary_id = pg_fetch_result($sel, 0, "salary_id"); //給与ID
        $duty_form_type = pg_fetch_result($sel, 0, "duty_form_type"); //雇用区分
        $closing = pg_fetch_result($sel, 0, "closing"); //締め日
        $org_corp_cd = pg_fetch_result($sel, 0, "org_corp_cd"); //所属元法人コード
    } else {
        $wage = "";
        $tmcd_group_id = "";
        $unit_price1 = "";
        $unit_price2 = "";
        $unit_price3 = "";
        $unit_price4 = "";
        $unit_price5 = "";
        $unit_price6 = "";
        $unit_price7 = "";
        $commuting_distance = "";
        $commuting_type = "";
        $irrg_type = "";
        $hol_minus = "";
        $duty_form = "";    //勤務形態
        $atdptn_id = null;
        $no_overtime = "f";
        $day1_hour = "";
        $day1_min = "";
        $paid_hol_tbl_id = "1";
        $csv_layout_id = "01";
        $paid_hol_start_date = "";
        $am_hour = "";
        $am_min = "";
        $pm_hour = "";
        $pm_min = "";
        $other_post_flg = "";  //他部署兼務
        $week_time = "";  			//週所定労働時間 20131111
		$week_hour = "";
		$week_min = "";
        $salary_id = "";
        $duty_form_type = "";
        $closing = "";
        $org_corp_cd = "";
    }
    $irrg_app = ($irrg_type == "") ? "1" : "2";

    $paid_hol_start_yr = substr($paid_hol_start_date, 0, 4);
    $paid_hol_start_mon = substr($paid_hol_start_date, 4, 2);
    $paid_hol_start_day = substr($paid_hol_start_date, 6, 2);

    // 個人別勤務時間帯追加 20120308
    $sql = "select * from empcond_officehours";
    $cond = "where emp_id = '$emp_id' order by weekday";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $num = pg_num_rows($sel);
    if ($num > 0) {

        for ($i=0; $i<$num; $i++) {
            $weekday = pg_fetch_result($sel, $i, "weekday");
            $officehours2_start = pg_fetch_result($sel, $i, "officehours2_start");
            $officehours2_end = pg_fetch_result($sel, $i, "officehours2_end");
            $officehours4_start = pg_fetch_result($sel, $i, "officehours4_start");
            $officehours4_end = pg_fetch_result($sel, $i, "officehours4_end");

            $varname1 = "start_hour".$weekday."_2";
            $varname2 = "start_min".$weekday."_2";
            $varname3 = "end_hour".$weekday."_2";
            $varname4 = "end_min".$weekday."_2";
            $varname5 = "start_hour".$weekday."_4";
            $varname6 = "start_min".$weekday."_4";
            $varname7 = "end_hour".$weekday."_4";
            $varname8 = "end_min".$weekday."_4";

            list($$varname1, $$varname2) = split(":", $officehours2_start);
            list($$varname3, $$varname4) = split(":", $officehours2_end);
            if ($officehours4_start != "") {
                list($$varname5, $$varname6) = split(":", $officehours4_start);
            }
            if ($officehours4_end != "") {
                list($$varname7, $$varname8) = split(":", $officehours4_end);
            }
        }
    }
}

// 給与支給区分 変更履歴を取得
$sql = "SELECT * FROM empcond_wage_history WHERE emp_id='$emp_id' ORDER BY histdate DESC";
$sel_wghist = select_from_table($con, $sql, "", $fname);
if ($sel_wghist == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 雇用・勤務形態変更履歴を取得
$sql = "SELECT * FROM empcond_duty_form_history WHERE emp_id='$emp_id' ORDER BY histdate DESC";
$sel_dfhist = select_from_table($con, $sql, "", $fname);
if ($sel_dfhist == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 他部署兼務履歴を取得 20120418
$sql = "SELECT * FROM empcond_other_post_flg_history WHERE emp_id='$emp_id' ORDER BY histdate DESC";
$sel_ophist = select_from_table($con, $sql, "", $fname);
if ($sel_ophist == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 勤務時間帯履歴を取得 20130206
$sql = "SELECT * FROM empcond_officehours_history WHERE emp_id='$emp_id' ORDER BY histdate DESC";
$sel_hrhist = select_from_table($con, $sql, "", $fname);
if ($sel_hrhist == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

//有休付与日数表の読み込み
$sql = "select * from timecard_paid_hol order by order_no";
$cond = "";
$paid_sel = select_from_table($con, $sql, $cond, $fname);
if ($paid_sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
//配列にデータ格納
$idx = 1;
$arr_paid_hol_info = array();
while($row = pg_fetch_array($paid_sel)){
    $arr_paid_hol_info[$idx] = $row;
    $wk_days = 0;
    for ($i=1; $i<=8; $i++) {
        $varname = "add_day".$i;
        $wk = ($row["$varname"] != "") ? $row["$varname"] : 0;
        $wk_days += $wk;
    }
    $arr_paid_hol_info[$idx]["disp_flg"] = ($wk_days != 0);
    $idx++;
}
// グループ名を取得
$group_names = get_timecard_group_names($con, $fname);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | 勤務条件</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript">
function onChangeIrrgApp() {
    var disabled = (document.employee.irrg_app[0].checked);
    for (var i = 0, j = document.employee.irrg_type.length; i < j; i++) {
        document.employee.irrg_type[i].disabled = disabled;
    }
    document.employee.hol_minus.disabled = disabled;
}

/* 給与支給区分 履歴 */
function wg_history_delete(id) {
    if (confirm('削除してよろしいでしょうか?')) {
        document.wghist.action = 'employee_cond_wg_history_delete.php';
        document.wghist.empcond_wage_history_id.value = id;
        document.wghist.submit();
    }
}
function wg_history_edit(id) {
    if (id) {
        document.wghist.empcond_wage_history_id.value = id;
    }
    document.wghist.submit();
}

/* 雇用・勤務形態 履歴 */
function df_history_delete(id) {
    if (confirm('削除してよろしいでしょうか?')) {
        document.dfhist.action = 'employee_cond_df_history_delete.php';
        document.dfhist.empcond_duty_form_history_id.value = id;
        document.dfhist.submit();
    }
}
function df_history_edit(id) {
    if (id) {
        document.dfhist.empcond_duty_form_history_id.value = id;
    }
    document.dfhist.submit();
}

/* 他部署兼務 履歴 */
function op_history_delete(id) {
    if (confirm('削除してよろしいでしょうか?')) {
        document.ophist.action = 'employee_cond_op_history_delete.php';
        document.ophist.empcond_other_post_flg_history_id.value = id;
        document.ophist.submit();
    }
}
function op_history_edit(id) {
    if (id) {
        document.ophist.empcond_other_post_flg_history_id.value = id;
    }
    document.ophist.submit();
}

/* 勤務時間帯 履歴 */
function hr_history_delete(id) {
    if (confirm('削除してよろしいでしょうか?')) {
        document.hrhist.action = 'employee_cond_hr_history_delete.php';
        document.hrhist.empcond_officehours_history_id.value = id;
        document.hrhist.submit();
    }
}
function hr_history_edit(id) {
    if (id) {
        document.hrhist.empcond_officehours_history_id.value = id;
    }
    document.hrhist.submit();
}

/* 履歴タブ */
$(document).ready(function(){
    $('div.hist').hide();
    $('#menu_tab>li').removeClass('selected');

    var hist = $.cookie('_employee_condition_history');
    switch (hist) {
        case 'df':
            $('#dfhist').show();
            $('#dfhist_tab').addClass('selected');
            break;
        case 'op':
            $('#ophist').show();
            $('#ophist_tab').addClass('selected');
            break;
        case 'hr':
            $('#hrhist').show();
            $('#hrhist_tab').addClass('selected');
            break;
        case 'wg':
        default:
            $('#wghist').show();
            $('#wghist_tab').addClass('selected');
            break;
    }
});

function history_tab(tab) {
    $('div.hist').hide();
    $('#menu_tab>li').removeClass('selected');
    $('#'+tab+'hist').show();
    $('#'+tab+'hist_tab').addClass('selected');
    $.cookie('_employee_condition_history', tab)
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/employee/employee.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="onChangeIrrgApp();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>&key1=<? echo(urlencode($key1)); ?>&key2=<? echo(urlencode($key2)); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_detail.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="employee_contact_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="employee_condition_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>勤務条件</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_auth_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_display_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー設定</font></a></td>
<? if (webmail_alias_enabled()) { ?>
<td width="5">&nbsp;</td>
<td width="130" align="center" bgcolor="#bdd1e7"><a href="employee_alias_setting.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&page=<? echo($page); ?>&view=<? echo($view); ?>&emp_o=<? echo($emp_o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メール転送設定</font></a></td>
<? } ?>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="employee" action="employee_condition_setting_confirm.php" method="post">
<? if ($reg_auth == 1) { ?>
<table width="800" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<? } ?>
<table width="800" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($infos[1]); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名前</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($infos[7]); ?>&nbsp;<? echo($infos[8]); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">給与ID</font></td>
<td><input type="text" name="salary_id" value="<? echo($salary_id); ?>" size="12" maxlength="10" style="ime-mode:inactive;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属元法人コード</font></td>
<td><input type="text" name="org_corp_cd" value="<? echo($org_corp_cd); ?>" size="14" maxlength="12" style="ime-mode:inactive;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">給与支給区分</font></td>
<td>
<select name="wage">
<option value="0"></option>
<?php
foreach ($list_wage as $item_id => $item_name) {
    printf('<option value="%s" %s>%s</option>', $item_id, ($item_id == $wage ? 'selected' : ''), $item_name);
}
?>
</select>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤パターンのグループ</font></td>
<td>
<?
    //グループコンボボックス出力
    $selected_group_id = $atdbk_common_class->setGroupOption("tmcd_group_id", "", $tmcd_group_id, "atdptn_id");
?>
</td>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤予定未登録時の標準の勤務パターン</font></td>
<td>
<select name="atdptn_id" id="atdptn_id">
    <option value="">
<?
    //グループID未設定時は、グループIDリストに表示されるIDを使用する
    if (empty($tmcd_group_id)){
        $tmcd_group_id = $selected_group_id;
    }
    // 出勤パターン名を取得
    $arr_attendance_pattern = $atdbk_common_class->get_pattern_array($tmcd_group_id);

    foreach ($arr_attendance_pattern as $list_atdptn_id => $list_atdptn_nm) {
        echo("<option value=\"$list_atdptn_id\"");
        if ($list_atdptn_id == $atdptn_id) {
            echo(" selected");
        }
        echo(">$list_atdptn_nm\n");
    }
?>
</select>
</td>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通常時間単価</font></td>
<td><input type="text" name="unit_price1" value="<? echo($unit_price1); ?>" size="10" maxlength="7" style="ime-mode:inactive;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">円</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">早出残業時間単価</font></td>
<td><input type="text" name="unit_price2" value="<? echo($unit_price2); ?>" size="10" maxlength="7" style="ime-mode:inactive;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">円</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">普通残業時間単価</font></td>
<td><input type="text" name="unit_price3" value="<? echo($unit_price3); ?>" size="10" maxlength="7" style="ime-mode:inactive;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">円</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">深夜残業時間単価</font></td>
<td><input type="text" name="unit_price4" value="<? echo($unit_price4); ?>" size="10" maxlength="7" style="ime-mode:inactive;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">円</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">交通費単価</font></td>
<td><input type="text" name="unit_price5" value="<? echo($unit_price5); ?>" size="10" maxlength="7" style="ime-mode:inactive;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">円</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">自転車等の交通費1日単価</font></td>
<td><input type="text" name="unit_price6" value="<? echo($unit_price6); ?>" size="10" maxlength="7" style="ime-mode:inactive;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">円</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定期代</font></td>
<td><input type="text" name="unit_price7" value="<? echo($unit_price7); ?>" size="10" maxlength="7" style="ime-mode:inactive;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">円</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">片道通勤距離</font></td>
<td><input type="text" name="commuting_distance" value="<? echo($commuting_distance); ?>" size="10" maxlength="7" style="ime-mode:inactive;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">交通用具</font></td>
<td><input type="text" name="commuting_type" value="<? echo($commuting_type); ?>" size="20" maxlength="20" style="ime-mode:active;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">変則労働適用</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="irrg_app" value="1"<? if ($irrg_app == "1") {echo(" checked");} ?> onclick="onChangeIrrgApp();">無<input type="radio" name="irrg_app" value="2"<? if ($irrg_app == "2") {echo(" checked");} ?> onclick="onChangeIrrgApp();">有</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">期間</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="irrg_type" value="1"<? if ($irrg_type == "1") {echo(" checked");} ?>>週<input type="radio" name="irrg_type" value="2"<? if ($irrg_type == "2") {echo(" checked");} ?>>月<img src="img/spacer.gif" alt="" height="1" width="20"><input type="checkbox" name="hol_minus" value="t"<? if ($hol_minus == "t") {echo(" checked");} ?>>所定労働時間から祝日分（8時間）を減算する</font></td>
</tr>

<!-- ************ oose add start ********* -->
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">雇用・勤務形態</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?php
foreach ($list_duty_form as $item_id => $item_name) {
    printf('<label><input type="radio" name="duty_form" value="%s" %s onclick="onChangeIrrgApp();">%s</label>', $item_id, ($item_id == $duty_form ? 'checked' : ''), $item_name);
}
?>
&nbsp;&nbsp;様式９の判定に利用される
</font></td>
</tr>

<?php if ($employment_working_other_flg == "t"){ ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">雇用区分</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?php 
	foreach ($list_duty_form_type as $item_id => $item_name){
		printf('<label><input type="radio" name="duty_form_type" value="%s" %s onclick="onChangeIrrgApp();">%s</label>', $item_id, ($item_id == $duty_form_type ? 'checked' : ''), $item_name);
	}
?>
</font></td>
</tr>
<?php } ?>

<? /* 他部署兼務追加 20120418 */ ?>
<tr height="22">
<td width="22%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">他部署兼務</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="other_post_flg" value="1"<? if ($other_post_flg == "1") {echo(" checked");} ?> >有り
<input type="radio" name="other_post_flg" value="" <? if ($other_post_flg == "") {echo(" checked");} ?> >無し&nbsp;&nbsp;様式９の判定に利用される
</font></td>
</tr>
<!---------
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="duty_day_flg" value="1"<? if ($duty_day_flg == "1") {echo(" checked");} ?> onclick="onChangeIrrgApp();">月
<input type="radio" name="duty_day_flg" value="2"<? if ($duty_day_flg == "2") {echo(" checked");} ?> onclick="onChangeIrrgApp();">火
<input type="radio" name="duty_day_flg" value="3"<? if ($duty_day_flg == "3") {echo(" checked");} ?> onclick="onChangeIrrgApp();">水
<input type="radio" name="duty_day_flg" value="4"<? if ($duty_day_flg == "4") {echo(" checked");} ?> onclick="onChangeIrrgApp();">木
<input type="radio" name="duty_day_flg" value="5"<? if ($duty_day_flg == "5") {echo(" checked");} ?> onclick="onChangeIrrgApp();">金
<input type="radio" name="duty_day_flg" value="6"<? if ($duty_day_flg == "6") {echo(" checked");} ?> onclick="onChangeIrrgApp();">土
<input type="radio" name="duty_day_flg" value="7"<? if ($duty_day_flg == "7") {echo(" checked");} ?> onclick="onChangeIrrgApp();">日
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">夜勤の有無</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="night_duty_flg" value="1"<? if ($night_duty_flg == "1") {echo(" checked");} ?> onclick="onChangeIrrgApp();">有り
<input type="radio" name="night_duty_flg" value="2"<? if ($night_duty_flg == "2") {echo(" checked");} ?> onclick="onChangeIrrgApp();">無し
<input type="radio" name="night_duty_flg" value="3"<? if ($night_duty_flg == "3") {echo(" checked");} ?> onclick="onChangeIrrgApp();">夜専
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">夜勤従事者への計上</font></td>
<td><input type="text" name="night_staff_cnt" value="<? echo($night_staff_cnt); ?>" size="10" maxlength="4" style="ime-mode:inactive;">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">人</font></td>
</tr>
-------->
<!-- ************ oose add end   ********* -->

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業管理</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="no_overtime" value="t"<? if ($no_overtime == "t") {echo(" checked");} ?>>残業管理をしない</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所定労働時間</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="day1_hour"><? show_hour_options_0_23($day1_hour); ?></select>時間<select name="day1_min">
<?
//青森県中様用、1分単位とする 20130327
$opt_aomori_flag = file_exists("opt/aomori_flag");
if ($opt_aomori_flag) {
    show_min_options_00_59($day1_min);
}
else {
    show_min_options_5($day1_min);
}
?>
</select>分</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">半日午前所定時間</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="am_hour"><? show_hour_options_0_23($am_hour); ?></select>時間<select name="am_min">
<?
if ($opt_aomori_flag) {
    show_min_options_00_59($am_min);
}
else {
    show_min_options_5($am_min);
}
?>
</select>分</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">半日午後所定時間</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="pm_hour"><? show_hour_options_0_23($pm_hour); ?></select>時間<select name="pm_min">
<?
if ($opt_aomori_flag) {
    show_min_options_00_59($pm_min);
}
else {
    show_min_options_5($pm_min);
}
?>
</select>分</font></td>
</tr>
<!-- 週所定労働時間 20131111 YOKOBORI -->
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週所定労働時間<br>（勤務シフト作成で非常勤の週所定労働時間をチェックする際に使用します）</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="week_hour"><? show_hour_options_1_40($week_hour); ?></select>時間
<select name="week_min"><? show_min_options_5($week_min); ?>
</select>分</font></td>
</tr>


<? /* 個人別勤務時間帯追加 20120203 */ ?>
<tr height="22">
<td align="right" valign="top" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務時間帯<br>
（勤務管理、<br>時間帯の設定で、<br>個人勤務条件を優先する<br>設定の場合に有効です）</font></td>
<td>

<table width="" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td align="center"></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所定労働</font></td>
<td align="center">
</td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩時間</font></td>
</tr>
<tr height="22">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本時間</font></td>
<td align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="start_hour8_2"><? show_hour_options_0_23($start_hour8_2); ?></select>：<select name="start_min8_2"><? show_min_options_5($start_min8_2); ?></select>〜<select name="end_hour8_2"><? show_hour_options_0_23($end_hour8_2); ?></select>：<select name="end_min8_2"><? show_min_options_5($end_min8_2); ?></select></font>
</td>
<td align="center">&nbsp;</td>
<td align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="start_hour8_4"><? show_hour_options_0_23($start_hour8_4); ?></select>：<select name="start_min8_4"><? show_min_options_5($start_min8_4); ?></select>〜<select name="end_hour8_4"><? show_hour_options_0_23($end_hour8_4); ?></select>：<select name="end_min8_4"><? show_min_options_5($end_min8_4); ?></select></font>

</td>
</tr>

<tr height="22">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">曜日別時間&nbsp;</font></td>
<td align="center"></td>
<td align="center"></td>
<td align="center"></td>
</tr>

<? /* 繰り返す
1:月曜日-6:土曜日
0:日曜日
7:祝日
 */
$arr_weekday_name = array("月曜日", "火曜日", "水曜日", "木曜日", "金曜日", "土曜日", "日曜日", "祝日");
$arr_weekday_id = array("1", "2", "3", "4", "5", "6", "0", "7");
for ($i=0; $i<8; $i++) {
    $varname1 = "start_hour".$arr_weekday_id[$i]."_2";
    $varname2 = "start_min".$arr_weekday_id[$i]."_2";
    $varname3 = "end_hour".$arr_weekday_id[$i]."_2";
    $varname4 = "end_min".$arr_weekday_id[$i]."_2";
    $varname5 = "start_hour".$arr_weekday_id[$i]."_4";
    $varname6 = "start_min".$arr_weekday_id[$i]."_4";
    $varname7 = "end_hour".$arr_weekday_id[$i]."_4";
    $varname8 = "end_min".$arr_weekday_id[$i]."_4";
 ?>
<tr height="22">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_weekday_name[$i]);?></font></td>
<td align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="<? echo($varname1);?>"><? show_hour_options_0_23($$varname1); ?></select>：<select name="<? echo($varname2);?>"><? show_min_options_5($$varname2); ?></select>〜<select name="<? echo($varname3);?>"><? show_hour_options_0_23($$varname3); ?></select>：<select name="<? echo($varname4);?>"><? show_min_options_5($$varname4); ?></select></font>
</td>
<td align="center">&nbsp;&nbsp;</td>
<td align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="<? echo($varname5);?>"><? show_hour_options_0_23($$varname5); ?></select>：<select name="<? echo($varname6);?>"><? show_min_options_5($$varname6); ?></select>〜<select name="<? echo($varname7);?>"><? show_hour_options_0_23($$varname7); ?></select>：<select name="<? echo($varname8);?>"><? show_min_options_5($$varname8); ?></select></font>

</td>
</tr>
<? } ?>
</table>

</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">有給付与起算日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="paid_hol_start_yr"><? show_select_years(100, $paid_hol_start_yr, true, true); ?></select>/<select name="paid_hol_start_mon"><? show_select_months($paid_hol_start_mon, true) ?></select>/<select name="paid_hol_start_day"><? show_select_days($paid_hol_start_day, true); ?></select></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">有給付与日数表</font></td>
<td>
<select name="paid_hol_tbl_id">
<?
//while($row = pg_fetch_array($paid_sel)){
for ($i=1; $i<=8; $i++) {
    if ($arr_paid_hol_info[$i]["disp_flg"] == false) {
        continue;
    }
    echo("<option value=\"{$arr_paid_hol_info[$i]["paid_hol_tbl_id"]}\"");
    if ($paid_hol_tbl_id == $arr_paid_hol_info[$i]["paid_hol_tbl_id"]) {
        echo(" selected");
    }
    //名称変更 常勤 週ｎ日
    $wk_tbl_id_name = $atdbk_common_class->get_paid_hol_tbl_id_name($arr_paid_hol_info[$i]["paid_hol_tbl_id"]);
    echo(">{$wk_tbl_id_name}");
    echo("</option>");
}

//選択肢に「なし」を追加
$selected = ($paid_hol_tbl_id == "") ? " selected" : "";
echo("<option value=\"\" $selected>なし</option>");
?>
</select>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月集計CSVレイアウト</font></td>
<td>
<select name="csv_layout_id">
<?
for ($i=1; $i<=20; $i++) {
    $wk_id = sprintf("%02d", $i);
    echo("<option value=\"$wk_id\"");
    if ($csv_layout_id == $wk_id) {
        echo(" selected");
    }
    echo(">レイアウト{$wk_id}");
    echo("</option>");
}
?>
</select>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">締め日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="closing"><option value="--"><option value="1"<? if ($closing == "1") {echo(" selected");} ?>>1<option value="2"<? if ($closing == "2") {echo(" selected");} ?>>5<option value="3"<? if ($closing == "3") {echo(" selected");} ?>>10<option value="4"<? if ($closing == "4") {echo(" selected");} ?>>15<option value="5"<? if ($closing == "5") {echo(" selected");} ?>>20<option value="6"<? if ($closing == "6") {echo(" selected");} ?>>末</select>日</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新日時</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php eh(substr($updated_on,0,19))?></font></td>
</tr>
</table>
<? if ($reg_auth == 1) { ?>
<table width="800" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<? } ?>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="key1" value="<? echo($key1); ?>">
<input type="hidden" name="key2" value="<? echo($key2); ?>">
<input type="hidden" name="class_cond" value="<? echo($class_cond); ?>">
<input type="hidden" name="atrb_cond" value="<? echo($atrb_cond); ?>">
<input type="hidden" name="dept_cond" value="<? echo($dept_cond); ?>">
<input type="hidden" name="room_cond" value="<? echo($room_cond); ?>">
<input type="hidden" name="job_cond" value="<? echo($job_cond); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="view" value="<? echo($view); ?>">
<input type="hidden" name="emp_o" value="<? echo($emp_o); ?>">
<input type="hidden" name="employment_working_other_flg" value="<? echo($employment_working_other_flg); ?>">
</form>
</td>
</tr>
</table>

<ul id="menu_tab">
<li id="wghist_tab"><a href="javascript:void(0);" onclick="history_tab('wg');">給与支給区分履歴</a></li>
<li id="dfhist_tab"><a href="javascript:void(0);" onclick="history_tab('df');">雇用・勤務形態履歴</a></li>
<li id="ophist_tab"><a href="javascript:void(0);" onclick="history_tab('op');">他部署兼務履歴</a></li>
<li id="hrhist_tab"><a href="javascript:void(0);" onclick="history_tab('hr');">勤務時間帯履歴</a></li>
</ul>

<div class="hist" id="wghist">
<form name="wghist" action="employee_cond_wg_history_form.php" method="post">
<button type="button" onclick="wg_history_edit()">給与支給区分の履歴を手動で追加する</button>
<? if (pg_num_rows($sel_wghist) > 0) {?>
<table class="hist">
<thead>
<tr>
    <td class="j12">給与支給区分</td>
    <td class="j12">変更日時</td>
    <td class="j12">修正</td>
    <td class="j12">削除</td>
</tr>
</thead>
<tbody>
<? while ($r = pg_fetch_assoc($sel_wghist)) {?>
<tr>
    <td class="j12"><?php eh($list_wage[$r["wage"]]);?></td>
    <td class="j12"><?php eh(substr($r["histdate"],0,19))?></td>
    <td><button type="button" onclick="wg_history_edit(<?php eh($r["empcond_wage_history_id"]);?>)">修正</button></td>
    <td><button type="button" onclick="wg_history_delete(<?php eh($r["empcond_wage_history_id"]);?>)">削除</button></td>
</tr>
<?}?>
</tbody>
</table>
<?}?>
<input type="hidden" name="session" value="<?php eh($session); ?>">
<input type="hidden" name="emp_id" value="<?php eh($emp_id); ?>">
<input type="hidden" name="empcond_wage_history_id" value="">
</form>
</div>

<div class="hist" id="dfhist">
<form name="dfhist" action="employee_cond_df_history_form.php" method="post">
<button type="button" onclick="df_history_edit()">雇用・勤務形態の履歴を手動で追加する</button>
<? if (pg_num_rows($sel_dfhist) > 0) {?>
<table class="hist">
<thead>
<tr>
    <td class="j12">雇用・勤務形態</td>
    <td class="j12">変更日時</td>
    <td class="j12">修正</td>
    <td class="j12">削除</td>
</tr>
</thead>
<tbody>
<? while ($r = pg_fetch_assoc($sel_dfhist)) {?>
<tr>
    <td class="j12"><?php eh($list_duty_form[$r["duty_form"]]);?></td>
    <td class="j12"><?php eh(substr($r["histdate"],0,19))?></td>
    <td><button type="button" onclick="df_history_edit(<?php eh($r["empcond_duty_form_history_id"]);?>)">修正</button></td>
    <td><button type="button" onclick="df_history_delete(<?php eh($r["empcond_duty_form_history_id"]);?>)">削除</button></td>
</tr>
<?}?>
</tbody>
</table>
<?}?>
<input type="hidden" name="session" value="<?php eh($session); ?>">
<input type="hidden" name="emp_id" value="<?php eh($emp_id); ?>">
<input type="hidden" name="empcond_duty_form_history_id" value="">
</form>
</div>

<div class="hist" id="ophist">
<form name="ophist" action="employee_cond_op_history_form.php" method="post">
<button type="button" onclick="op_history_edit()">他部署兼務の履歴を手動で追加する</button>
<? if (pg_num_rows($sel_ophist) > 0) {?>
<table class="hist">
<thead>
<tr>
    <td class="j12">他部署兼務</td>
    <td class="j12">変更日時</td>
    <td class="j12">修正</td>
    <td class="j12">削除</td>
</tr>
</thead>
<tbody>
<? while ($r = pg_fetch_assoc($sel_ophist)) {?>
<tr>
    <td class="j12"><?php eh($list_other_post_flg[$r["other_post_flg"]]);?></td>
    <td class="j12"><?php eh(substr($r["histdate"],0,19))?></td>
    <td><button type="button" onclick="op_history_edit(<?php eh($r["empcond_other_post_flg_history_id"]);?>)">修正</button></td>
    <td><button type="button" onclick="op_history_delete(<?php eh($r["empcond_other_post_flg_history_id"]);?>)">削除</button></td>
</tr>
<?}?>
</tbody>
</table>
<?}?>
<input type="hidden" name="session" value="<?php eh($session); ?>">
<input type="hidden" name="emp_id" value="<?php eh($emp_id); ?>">
<input type="hidden" name="empcond_other_post_flg_history_id" value="">
</form>
</div>

<div class="hist" id="hrhist">
<form name="hrhist" action="employee_cond_hr_history_form.php" method="post">
<button type="button" onclick="hr_history_edit()">勤務時間帯の履歴を手動で追加する</button>
<? if (pg_num_rows($sel_hrhist) > 0) {?>
<table class="hist">
<thead>
<tr>
    <td class="j12" colspan="3">勤務時間帯</td>
    <td class="j12" rowspan="2">変更日時</td>
    <td class="j12" rowspan="2">修正</td>
    <td class="j12" rowspan="2">削除</td>
</tr>
<tr>
    <td class="j12">基本</td>
    <td class="j12">休憩</td>
    <td class="j12">曜日別</td>
</tr>
</thead>
<tbody>
<? while ($r = pg_fetch_assoc($sel_hrhist)) {?>
<tr>
    <td class="j12" align="center">
<?php
        if ($r["officehours2_start"] != "") {
            eh($r["officehours2_start"]."〜".$r["officehours2_end"]);
        }
        else {
            e(" 未設定 ");
        }
        e("</td>\n");

        e("<td class=\"j12\" align=\"center\">");
        if ($r["officehours4_start"] != "") {
            eh($r["officehours4_start"]."〜".$r["officehours4_end"]);
        }
        else {
            e(" 未設定 ");
        }
        e("</td>\n");
        e("<td class=\"j12\" align=\"center\">");
        e($r["sub_rec_cnt"]."件");
?>
    </td>
    <td class="j12"><?php eh(substr($r["histdate"],0,19))?></td>
    <td><button type="button" onclick="hr_history_edit(<?php eh($r["empcond_officehours_history_id"]);?>)">修正</button></td>
    <td><button type="button" onclick="hr_history_delete(<?php eh($r["empcond_officehours_history_id"]);?>)">削除</button></td>
</tr>
<?}?>
</tbody>
</table>
<?}?>
<input type="hidden" name="session" value="<?php eh($session); ?>">
<input type="hidden" name="emp_id" value="<?php eh($emp_id); ?>">
<input type="hidden" name="empcond_officehours_history_id" value="">
</form>
</div>


</body>
<? pg_close($con); ?>
</html>
