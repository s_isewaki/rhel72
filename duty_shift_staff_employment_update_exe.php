<!-- ************************************************************************ -->
<!-- 勤務シフト作成｜スタッフ設定（個人勤務条件） -->
<!-- ************************************************************************ -->

<?
require_once("about_session.php");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);

///-----------------------------------------------------------------------------
// 指定職員ＩＤのスタッフ情報（個人勤務条件）を全削除
///-----------------------------------------------------------------------------
// のスタッフ情報（個人勤務条件
$sql = "delete from duty_shift_staff_employment";
//職員をグループ移動した場合に、新旧グループの2レコードになる不具合対応 20100223
//$cond = "where group_id = '$group_id' ";
//$cond .= "and emp_id = '$staff_id' ";
$cond = "where emp_id = '$staff_id' ";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
// スタッフ情報（個人勤務条件＿出勤パターン）
$sql = "delete from duty_shift_staff_employment_atdptn";
//職員をグループ移動した場合に、新旧グループの2レコードになる不具合対応 20100223
//$cond = "where group_id = '$group_id' ";
//$cond .= "and emp_id = '$staff_id' ";
$cond = "where emp_id = '$staff_id' ";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// スタッフ情報（個人勤務条件）の登録
///-----------------------------------------------------------------------------
if (($group_id != "") && ($staff_id != "")) {
	//未設定時処理（null設定）
	if ($other_post_flg == "") { $other_post_flg = null; }
	if ($night_duty_flg == "") { $night_duty_flg = null; }
	//ＳＱＬ
	$sql = "insert into duty_shift_staff_employment (";
	$sql .= "group_id, emp_id,";
	$sql .= "duty_mon_day_flg,";
	$sql .= "duty_tue_day_flg,";
	$sql .= "duty_wed_day_flg,";
	$sql .= "duty_thurs_day_flg,";
	$sql .= "duty_fri_day_flg,";
	$sql .= "duty_sat_day_flg,";
	$sql .= "duty_sun_day_flg,";
	$sql .= "duty_hol_day_flg,";
	$sql .= "night_duty_flg, night_staff_cnt";
	$sql .= ") values (";
	$content = array(
				$group_id, $staff_id,
				$duty_mon_day_flg,
				$duty_tue_day_flg,
				$duty_wed_day_flg,
				$duty_thurs_day_flg,
				$duty_fri_day_flg,
				$duty_sat_day_flg,
				$duty_sun_day_flg,
				$duty_hol_day_flg,
				$night_duty_flg, pg_escape_string($night_staff_cnt)
			);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

///-----------------------------------------------------------------------------
// スタッフ情報（個人勤務条件＿出勤パターン）の登録
///-----------------------------------------------------------------------------
if (($group_id != "") && ($staff_id != "")) {
	for ($i=0; $i<4; $i++) {
	for ($k=0; $k<$atdptn_cnt; $k++) {
		if ($i == 0) { $use_flg = $week_flg[$k]; }		//月〜金
		if ($i == 1) { $use_flg = $sat_flg[$k]; }		//土
		if ($i == 2) { $use_flg = $sun_flg[$k]; }		//日
		if ($i == 3) { $use_flg = $holiday_flg[$k]; }	//祝日
		//ＳＱＬ
		$sql = "insert into duty_shift_staff_employment_atdptn (";
		$sql .= "group_id, emp_id, week_flg, atdptn_ptn_id, use_flg ";
		$sql .= ") values (";
		$content = array(
					$group_id, $staff_id,
					$i + 1,
					$atdptn_ptn_id[$k],
					$use_flg
				);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	}
}

///-----------------------------------------------------------------------------
// ＤＢ(empcond)の更新
///-----------------------------------------------------------------------------
//存在チェック
$sql = "select * from empcond";
$cond = "where emp_id = '$staff_id' ";
$cond .= "order by emp_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$num = pg_numrows($sel);
if ($num > 0) {

	//更新
	$sql = "update empcond set";
    $set = array("duty_form", "other_post_flg");
	$setvalue = array($duty_form, $other_post_flg);
	$cond = "where emp_id = '$staff_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

}
// 勤務条件レコードがない場合は作成 20120322
else {

    $data = array(
            'emp_id'                => $staff_id,
            'wage'                  => 1,
            'duty_form'             => $duty_form,
            'other_post_flg'             => $other_post_flg
            );
    $sql = "INSERT INTO empcond (". implode(",",array_keys($data)) .") VALUES (";
    $ins = insert_into_table($con, $sql, array_values($data), $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

}


///-----------------------------------------------------------------------------
// 勤務シフト日数上限数削除
///-----------------------------------------------------------------------------
$sql = "delete from emp_shift_limit";
$cond = "where emp_id = '$staff_id' ";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

///-----------------------------------------------------------------------------
// 勤務シフト日数上限数登録
///-----------------------------------------------------------------------------
$sql = "insert into emp_shift_limit (";
$sql .= "emp_id, atdptn_id,limit_day_count";
$sql .= ") values (";
for($i=0;$i<$shift_limit_count; $i++){
	$content = array($staff_id,$select_atdptn[$i],$limit_day_count[$i]);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

///-----------------------------------------------------------------------------
// 個人定型勤務条件
///-----------------------------------------------------------------------------
$sql = "delete from duty_shift_staff_fixed_atdptn ";
$cond = "where emp_id = '$staff_id' ";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
for($i=0; $i<8; $i++){
	if( $fixed_week[$i] == "" ){
		$fixed_week[$i] = 0 ;	// 識別コードを４桁に変更 20140125
		$fixed_week[$i] = "0000" ;
	}
}

// 識別コードを４桁に変更 まとめた4桁コードを分解する 20140125

$week1 = (int)mb_substr($fixed_week[1], 0, 2, "UTF-8");		// 勤務シフトパターン
$week2 = (int)mb_substr($fixed_week[2], 0, 2, "UTF-8");
$week3 = (int)mb_substr($fixed_week[3], 0, 2, "UTF-8");
$week4 = (int)mb_substr($fixed_week[4], 0, 2, "UTF-8");
$week5 = (int)mb_substr($fixed_week[5], 0, 2, "UTF-8");
$week6 = (int)mb_substr($fixed_week[6], 0, 2, "UTF-8");
$week0 = (int)mb_substr($fixed_week[0], 0, 2, "UTF-8");
$week7 = (int)mb_substr($fixed_week[7], 0, 2, "UTF-8");

$jiyu1 = (int)mb_substr($fixed_week[1], 2, 2, "UTF-8");		// 事由コード
$jiyu2 = (int)mb_substr($fixed_week[2], 2, 2, "UTF-8");
$jiyu3 = (int)mb_substr($fixed_week[3], 2, 2, "UTF-8");
$jiyu4 = (int)mb_substr($fixed_week[4], 2, 2, "UTF-8");
$jiyu5 = (int)mb_substr($fixed_week[5], 2, 2, "UTF-8");
$jiyu6 = (int)mb_substr($fixed_week[6], 2, 2, "UTF-8");
$jiyu0 = (int)mb_substr($fixed_week[0], 2, 2, "UTF-8");
$jiyu7 = (int)mb_substr($fixed_week[7], 2, 2, "UTF-8");



$sql = "insert into duty_shift_staff_fixed_atdptn (";
$sql .= "emp_id, atdptn_1,atdptn_2,atdptn_3,atdptn_4,atdptn_5,atdptn_6,atdptn_0,atdptn_7 ";
$sql .= ",reason_1,reason_2,reason_3,reason_4,reason_5,reason_6,reason_0,reason_7 ";
$sql .= ") values ( ";

//$content = array($staff_id,$fixed_week[1],$fixed_week[2],$fixed_week[3],$fixed_week[4],$fixed_week[5],$fixed_week[6],$fixed_week[0],$fixed_week[7]);
$content = array($staff_id,$week1,$week2,$week3,$week4,$week5,$week6,$week0,$week7,$jiyu1,$jiyu2,$jiyu3,$jiyu4,$jiyu5,$jiyu6,$jiyu0,$jiyu7);

$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query($con, "commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);
///-----------------------------------------------------------------------------
// 遷移元画面を再表示
///-----------------------------------------------------------------------------
$back_show = "<script type=\"text/javascript\">location.href = 'duty_shift_staff_employment.php?session=$session&group_id=$group_id&staff_id=$staff_id'</script>";
echo($back_show);
?>

<!-- ************************************************************************ -->
<!-- デバック用 -->
<!-- ************************************************************************ -->
<HTML>
<!--
<? echo("group_id=$group_id \n"); ?>
<? print_r($other_staff_array); ?>
<? echo("emp_id_array=$emp_id_array"); ?>
<? echo("group_name=$group_name"); ?>
-->
</HTML>

