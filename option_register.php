<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("label_by_profile_type.ini");
require_once("show_display_setting_common.ini");
require_once("get_cas_title_name.ini");
require_once("webmail_alias_functions.php");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限の取得
$authorities = get_authority($session, $fname);
$webmail_auth = ($authorities[0] == "t") ? "1" : "0";  // ウェブメール
$extension_auth = ($authorities[49] == "t") ? "1" : "0";  // 内線電話帳
$inci_auth = ($authorities[47] == "t") ? "1" : "0";  // ファントルくん
$schedule_auth = ($authorities[2] == "t") ? "1" : "0";  // スケジュール
$newsuser_auth = ($authorities[46] == "t") ? "1" : "0";  // お知らせ
$task_auth = ($authorities[30] == "t") ? "1" : "0";  // タスク
$msg_auth = ($authorities[4] == "t") ? "1" : "0";  // 伝言メモ
$aprv_auth = ($authorities[7] == "t") ? "1" : "0";  // 決裁・申請
$fcl_auth = ($authorities[11] == "t") ? "1" : "0";  // 設備予約
$lib_auth = ($authorities[32] == "t") ? "1" : "0";  // 文書管理
$bbs_auth = ($authorities[1] == "t") ? "1" : "0";  // 掲示板
$memo_auth = ($authorities[45] == "t") ? "1" : "0";  // 備忘録
$link_auth = ($authorities[60] == "t") ? "1" : "0";  // リンクライブラリ
$intra_auth = ($authorities[55] == "t") ? "1" : "0";  // イントラネット
$bed_auth = ($authorities[14] == "t") ? "1" : "0";  // 病床管理
$cas_auth = ($authorities[67] == "t") ? "1" : "0";  // CAS(クリニカルラダー)
$manabu_auth = ($authorities[75] == "t") ? "1" : "0";  // バリテス（旧e-ラーニング）
$fplus_auth = ($authorities[78] == "t") ? "1" : "0";  // ファントルくん＋
$jnl_auth = ($authorities[80] == "t") ? "1" : "0";  // 日報月報
$ladder_auth = ($authorities[87] == "t") ? "1" : "0";  // クリニカルラダー
$attdcd_auth = ($authorities[5] == "t") ? "1" : "0"; // 出勤表
$ccusr1_auth = ($authorities[95] == "t") ? "1" : "0"; // スタッフ・ポートフォリオ

// データベースに接続
$con = connect2db($fname);

// 職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// 業務機能の使用可否を取得
$sql = "select * from license";
$cond = "";
$sel_func = select_from_table($con, $sql, $cond, $fname);
if ($sel_func == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$lcs_func = array();
for ($i = 1; $i <= 60; $i++) {
    $lcs_func[$i] = pg_fetch_result($sel_func, 0, "lcs_func$i");
}

// 環境設定情報を取得
$sql = "select use_flash_logo, use_web from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$use_flash_logo = pg_fetch_result($sel, 0, "use_flash_logo");
$use_web = pg_fetch_result($sel, 0, "use_web");

// オプション情報を取得
$sql = "select * from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
if (pg_num_rows($sel) > 0) {
    $default_page = pg_fetch_result($sel, 0, "default_page");
    $logo_type = pg_fetch_result($sel, 0, "logo_type");
    $font_size = pg_fetch_result($sel, 0, "font_size");
    $mail_flg = pg_fetch_result($sel, 0, "top_mail_flg");
    $ext_flg = pg_fetch_result($sel, 0, "top_ext_flg");
    $inci_flg = pg_fetch_result($sel, 0, "top_inci_flg");
    $fplus_flg = pg_fetch_result($sel, 0, "top_fplus_flg");
    $manabu_flg = pg_fetch_result($sel, 0, "top_manabu_flg");
    $cas_flg = pg_fetch_result($sel, 0, "top_cas_flg");
    $event_flg = pg_fetch_result($sel, 0, "top_event_flg");
    $schd_flg = pg_fetch_result($sel, 0, "top_schd_flg");
    $info_flg = pg_fetch_result($sel, 0, "top_info_flg");
    $task_flg = pg_fetch_result($sel, 0, "top_task_flg");
    $schdsrch_flg = pg_fetch_result($sel, 0, "top_schdsrch_flg");
    $msg_flg = pg_fetch_result($sel, 0, "top_msg_flg");
    $aprv_flg = pg_fetch_result($sel, 0, "top_aprv_flg");
    $fcl_flg = pg_fetch_result($sel, 0, "top_fcl_flg");
    $lib_flg = pg_fetch_result($sel, 0, "top_lib_flg");
    $bbs_flg = pg_fetch_result($sel, 0, "top_bbs_flg");
    $memo_flg = pg_fetch_result($sel, 0, "top_memo_flg");
    $link_flg = pg_fetch_result($sel, 0, "top_link_flg");
    $intra_flg = pg_fetch_result($sel, 0, "top_intra_flg");
    $bedinfo = pg_fetch_result($sel, 0, "bed_info");
    $wic_flg = pg_fetch_result($sel, 0, "top_wic_flg");
    $schd_type = pg_fetch_result($sel, 0, "schd_type");
    $jnl_flg = pg_fetch_result($sel, 0, "top_jnl_flg");
    $top_free_text_flg = pg_fetch_result($sel, 0, "top_free_text_flg");
    $ladder_flg = pg_fetch_result($sel, 0, "top_ladder_flg");
    $top_timecard_error_flg = pg_fetch_result($sel, 0, "top_timecard_error_flg");
    $top_workflow_flg = pg_fetch_result($sel, 0, "top_workflow_flg");
    $top_ccusr1_flg = pg_fetch_result($sel, 0, "top_ccusr1_flg");
} else {
    $default_page = "01";
    $logo_type = "1";
    $font_size = "1";
    $mail_flg = "t";
    $ext_flg = "t";
    $inci_flg = "t";
    $fplus_flg = "t";
    $manabu_flg = "f";
    $cas_flg = "t";
    $event_flg = "t";
    $schd_flg = "t";
    $info_flg = "t";
    $task_flg = "t";
    $schdsrch_flg = "t";
    $msg_flg = "t";
    $aprv_flg = "t";
    $fcl_flg = "t";
    $lib_flg = "t";
    $bbs_flg = "t";
    $memo_flg = "t";
    $link_flg = "t";
    $intra_flg = "t";
    $bedinfo = "f";
    $wic_flg = "f";
    $schd_type = "f";
    $jnl_flg = "f";
    $top_free_text_flg = "f";
    $ladder_flg = "t";
    $top_timecard_error_flg = "f";
    $top_workflow_flg = "f";
    $top_ccusr1_flg = "f";
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix オプション設定 | 表示設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="option_register.php?session=<? echo($session); ?>"><img src="img/icon/b15.gif" width="32" height="32" border="0" alt="オプション設定"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="option_register.php?session=<? echo($session); ?>"><b>オプション設定</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#5279a5"><a href="option_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>表示設定</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="option_menu_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="option_mygroup.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイグループ</font></a></td>
<? if (webmail_alias_enabled()) { ?>
<td width="5">&nbsp;</td>
<td width="130" align="center" bgcolor="#bdd1e7"><a href="option_alias.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メール転送設定</font></a></td>
<? } ?>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<!--<img src="img/spacer.gif" alt="" width="1" height="2"><br>-->
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>一般設定</b></font></td>
</tr>
</table>
<form action="option_insert.php" method="post">
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="180" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログイン時の表示ページ</font></td>
<td><? show_page_list($con, $default_page, $authorities, $lcs_func, $fname, $profile_type, $emp_id); ?></td>
</tr>
<input type="hidden" name="logo_type" value="1" /><!-- Flashロゴ設定のなごり -->
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>マイページ</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="180" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文字サイズ</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="font_size" value="1"<? if ($font_size == "1") {echo(" checked");} ?>>標準
<input type="radio" name="font_size" value="2"<? if ($font_size == "2") {echo(" checked");} ?>>大
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイページに表示</font></td>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="mail_flg" value="t"<? if ($mail_flg == "t") {echo(" checked");} ?><? if ($webmail_auth != 1) {echo(" disabled");} ?>>ウェブメール</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="ext_flg" value="t"<? if ($ext_flg == "t") {echo(" checked");} ?><? if ($extension_auth != 1) {echo(" disabled");} ?>>一発電話検索</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="inci_flg" value="t"<? if ($inci_flg == "t") {echo(" checked");} ?><? if ($inci_auth != 1 || $lcs_func["5"] != "t") {echo(" disabled");} ?>>ファントルくん</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="fplus_flg" value="t"<? if ($fplus_flg == "t") {echo(" checked");} ?><? if ($fplus_auth != 1 || $lcs_func["13"] != "t") {echo(" disabled");} ?>>ファントルくん＋</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="jnl_flg" value="t"<? if ($jnl_flg == "t") {echo(" checked");} ?><? if ($jnl_auth != 1 || $lcs_func["14"] != "t") {echo(" disabled");} ?>>日報・月報</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="manabu_flg" value="t"<? if ($manabu_flg == "t") {echo(" checked");} ?><? if ($manabu_auth != 1 || $lcs_func["12"] != "t") {echo(" disabled");} ?>>バリテス</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="cas_flg" value="t"<? if ($cas_flg == "t") {echo(" checked");} ?><? if ($cas_auth != 1 || $lcs_func["8"] != "t") {echo(" disabled");} ?>><?echo(get_cas_title_name());?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="event_flg" value="t"<? if ($event_flg == "t") {echo(" checked");} ?>><? echo $_label_by_profile["EVENT"][$profile_type]; ?></font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="schd_flg" value="t"<? if ($schd_flg == "t") {echo(" checked");} ?><? if ($schedule_auth != 1) {echo(" disabled");} ?>>スケジュール</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="info_flg" value="t"<? if ($info_flg == "t") {echo(" checked");} ?><? if ($newsuser_auth != 1) {echo(" disabled");} ?>>お知らせ・回覧板</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="task_flg" value="t"<? if ($task_flg == "t") {echo(" checked");} ?><? if ($task_auth != 1) {echo(" disabled");} ?>>タスク</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="whatsnew_flg" value="t"<? if ($msg_flg == "t" || $aprv_flg == "t") {echo(" checked");} ?><? if ($msg_auth != 1 && $aprv_auth != 1 && $lib_auth != 1 && $bbs_auth != 1) {echo(" disabled");} ?>>最新情報</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="schdsrch_flg" value="t"<? if ($schdsrch_flg == "t") {echo(" checked");} ?><? if ($schedule_auth != 1) {echo(" disabled");} ?>>スケジュール検索</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="fcl_flg" value="t"<? if ($fcl_flg == "t") {echo(" checked");} ?><? if ($fcl_auth != 1) {echo(" disabled");} ?>>設備予約</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="lib_flg" value="t"<? if ($lib_flg == "t") {echo(" checked");} ?><? if ($lib_auth != 1) {echo(" disabled");} ?>>文書管理</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="bbs_flg" value="t"<? if ($bbs_flg == "t") {echo(" checked");} ?><? if ($bbs_auth != 1) {echo(" disabled");} ?>>掲示板</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="memo_flg" value="t"<? if ($memo_flg == "t") {echo(" checked");} ?><? if ($memo_auth != 1) {echo(" disabled");} ?>>備忘録</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="link_flg" value="t"<? if ($link_flg == "t") {echo(" checked");} ?><? if ($link_auth != 1) {echo(" disabled");} ?>>リンクライブラリ</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="intra_flg" value="t"<? if ($intra_flg == "t") {echo(" checked");} ?><? if ($intra_auth != 1) {echo(" disabled");} ?>>イントラネット</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="bedinfo" value="t"<? if ($bedinfo == "t") {echo(" checked");} ?><? if ($bed_auth != 1 || $lcs_func["2"] != "t") {echo(" disabled");} ?>><?
// 入退院カレンダー/入退所カレンダー
echo $_label_by_profile["BED_INFO"][$profile_type];
?>
</font></td>
</tr>
<tr height="22">
<? if ($use_web == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="wic_flg" value="t"<? if ($wic_flg == "t") {echo(" checked");} ?>>WICレポート</font></td>
<? } ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="top_free_text_flg" value="t"<? if ($top_free_text_flg == "t") {echo(" checked");} ?>>フリーテキスト</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" id="ladder_flg" name="ladder_flg" value="t"<? if ($ladder_flg == "t") {echo(" checked");} ?><? if ($ladder_auth != 1 || $lcs_func["17"] != "t") {echo(" disabled");} ?>>クリニカルラダー</font></td>
<!-- 打刻エラー表示　出勤表の権限を参照する START -->
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" id="top_timecard_error_flg" name="top_timecard_error_flg" value="t"<? if ($top_timecard_error_flg == "t") {echo(" checked");} ?><? if ($attdcd_auth != 1) {echo(" disabled");} ?>>打刻エラー件数</font></td>
<!-- 打刻エラー表示　出勤表の権限を参照する END -->
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="top_workflow_flg" value="t"<? if ($top_workflow_flg === "t") {echo(" checked");} if($aprv_auth != 1){echo(" disabled");} ?>>決裁・申請</font></td>
<? if ($lcs_func[31]=="t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="top_ccusr1_flg" value="t"<? if ($top_ccusr1_flg === "t") {echo(" checked");}?>>スタッフ・ポートフォリオ</font></td>
<? } else { ?>
<td></td>
<? } ?>
<td></td>
<td></td>
</tr>
</table>
</td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>スケジュール</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<?
//------------------------------------------------------------------------------
// 出勤予定
//------------------------------------------------------------------------------
if ($schedule_auth == 1) {
    $disabled = "";
    if ($schd_type == "t") {
        $checked1 = " checked";
        $checked2 = "";
    } else {
        $checked1 = "";
        $checked2 = " checked";
    }
} else {
    $disabled = " disabled";
    $checked1 = "";
    $checked2 = "";
}
?>
<tr height="22">
<td width="180" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤予定を表示</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="schd_type" value="t"<? echo("$checked1$disabled"); ?>>する
<input type="radio" name="schd_type" value="f"<? echo("$checked2$disabled"); ?>>しない
</font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
