<?php
//更新処理
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 5, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

require_once './kintai/common/mdb_wrapper.php';
require_once './kintai/common/mdb.php';

//更新モード
$up_mode           = $_REQUEST["up_mode"];

switch ($up_mode){
	case "chk_update":	//チェック済みフラグ更新

		//フォームデータ取得
		$checked_flg       = $_REQUEST["checked_flg"];
		$emp_id            = $_REQUEST["emp_id"];
		$view              = $_REQUEST["view"];
		$yyyymm            = $_REQUEST["yyyymm"];
		$year              = $_REQUEST["year"];
		$month             = $_REQUEST["month"];
		$srch_name         = $_REQUEST["srch_name"];
		$cls               = $_REQUEST["cls"];
		$atrb              = $_REQUEST["atrb"];
		$dept              = $_REQUEST["dept"];
		$page              = $_REQUEST["page"];
		$group_id          = $_REQUEST["group_id"];
		$shift_group_id    = $_REQUEST["shift_group_id"];
		$csv_layout_id     = $_REQUEST["csv_layout_id"];
		$duty_form_jokin   = $_REQUEST["duty_form_jokin"];
		$duty_form_hijokin = $_REQUEST["duty_form_hijokin"];
		$staff_ids         = $_REQUEST["staff_ids"];
		$data_cnt          = $_REQUEST["data_cnt"];
		$check_flg         = $_REQUEST["check_flg"];
		$emp_move_flg      = $_REQUEST["emp_move_flg"];
		$emp_personal_id   = $_REQUEST["emp_personal_id"];
		//チェック済みフラグ
		if($checked_flg == "t"){
			$blnCHK = true;
		}else{
			$blnCHK = false;
		}
		
		//データ更新
		try {
		
			MDBWrapper::init(); // おまじない
			$log = new common_log_class(basename(__FILE__));
			$db = new MDB($log); // ログはnullでも可。指定するとどのPHPからMDBにアクセスした際のエラーかわかる
			$db->beginTransaction(); // トランザクション開始
		
			$types = array("boolean", 
						   "text", 
						   "integer", 
						   "integer");
			$param = array("check_flag" => $blnCHK, 
						   "emp_id"     => $emp_id, 
						   "duty_yyyy"  => $year, 
						   "duty_mm"    => $month);
		
			$sql  = "UPDATE";
			$sql .= " duty_shift_plan_staff ";
			$sql .= "SET";
			$sql .= " check_flag = :check_flag ";
			$sql .= "WHERE";
			$sql .= " emp_id = :emp_id AND ";
			$sql .= " duty_yyyy = :duty_yyyy AND";
			$sql .= " duty_mm = :duty_mm ";
			$db->update($sql, $types, $param);
		
			$db->endTransaction(); // トランザクション終了
		} catch (BusinessException $bex) {
			$message = $bex->getMessage();
			echo("<script type='text/javascript'>alert('".$message."');</script>");
			echo("<script language='javascript'>history.back();</script>");
			exit;
		} catch (FatalException $fex) { 
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		//HTML出力（自動遷移）
?>		
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
		<script language="javascript">
		function page_submit(){
			document.frmUP.submit();
		}
		</script>
		</head>
		<body onLoad="page_submit();">
		<form name="frmUP" method="post" action="./kintai_ovtmlist_adm.php">
			<input type="hidden" name="session"           value="<?php echo $session; ?>">
			<input type="hidden" name="emp_id"            value="<?php echo $emp_id; ?>">
			<input type="hidden" name="view"              value="<?php echo $view; ?>">
			<input type="hidden" name="yyyymm"            value="<?php echo $yyyymm; ?>">
			<input type="hidden" name="srch_name"         value="<?php echo $srch_name; ?>">
			<input type="hidden" name="cls"               value="<?php echo $cls; ?>">
			<input type="hidden" name="atrb"              value="<?php echo $atrb; ?>">
			<input type="hidden" name="dept"              value="<?php echo $dept; ?>">
			<input type="hidden" name="page"              value="<?php echo $page; ?>">
			<input type="hidden" name="group_id"          value="<?php echo $group_id; ?>">
			<input type="hidden" name="shift_group_id"    value="<?php echo $shift_group_id; ?>">
			<input type="hidden" name="csv_layout_id"     value="<?php echo $csv_layout_id; ?>">
			<input type="hidden" name="duty_form_jokin"   value="<?php echo $duty_form_jokin; ?>">
			<input type="hidden" name="duty_form_hijokin" value="<?php echo $duty_form_hijokin; ?>">
			<input type="hidden" name="staff_ids"         value="<?php echo $staff_ids; ?>">
			<input type="hidden" name="data_cnt"          value="<?php echo $data_cnt; ?>">
			<input type="hidden" name="check_flg"         value="<?php echo $check_flg; ?>">
			<input type="hidden" name="emp_move_flg"      value="<?php echo $emp_move_flg; ?>">
			<input type="hidden" name="emp_personal_id"   value="<?php echo $emp_personal_id; ?>">
			<input type="hidden" name="wherefrom"         value="<?php echo $wherefrom; ?>">
		</form>
		</body>
		</html>
<?php		
		break;
		
		
		
	default:	
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
}
?>
