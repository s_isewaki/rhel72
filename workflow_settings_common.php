<?php
//workflow_settings_common.php
//ワークフロー、決裁･申請設定
require_once("workflow_proxy_common_class.php");
require_once("settings_common.php");

function get_workflow_settings($con, $fname, $current_user_id) {
    
    $proxy_class = new workflow_proxy_common_class($con, $fname);
    
    $workflow_settings = array();
    
    $workflow_settings["kintai_paid_hol_hour_flg"] = get_settings_value("kintai_paid_hol_hour_flg", "t"); //初期値を"t"表示するに変更 20141217
    $workflow_settings["proxy_flg"] = ($proxy_class->is_workflow_proxy_staff($current_user_id)) ? "t" : "f";
    $workflow_settings["kintai_paid_hol_btn_disp_flg"] = get_settings_value("kintai_paid_hol_btn_disp_flg", "t");
    $workflow_settings["kintai_paid_hol_title"] = get_settings_value("kintai_vacation_paid_hol_title", "年次休暇簿");
    $workflow_settings["kintai_paid_remain_disp_flg"] = get_settings_value("kintai_paid_remain_disp_flg", "f");
    $workflow_settings["kintai_paid_remain_title"] = get_settings_value("kintai_paid_remain_title", "年次休暇残日数");
    $workflow_settings["kintai_calc_days_flg"] = get_settings_value("kintai_calc_days_flg", "f");
    $workflow_settings["kintai_vacation_disp_flg"] = get_settings_value("kintai_vacation_disp_flg", "f");
    
    $workflow_settings["summer_start_date"] = get_settings_value("summer_start_date", "");
    $workflow_settings["summer_end_date"] = get_settings_value("summer_end_date", "");
    
    return $workflow_settings;
    
}
