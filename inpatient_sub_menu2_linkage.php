<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>�¾���������Ͽ��������</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("show_select_years_wareki.ini");

$fname = $PHP_SELF;

// ���å����Υ����å�
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ���¤Υ����å�
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ����������Υ�����Хå�����URL������
$callbacker_url_prefix = ($_SERVER["SERVER_PORT"] == 443) ? "https://" : "http://";
$collbacker_url_dir = str_replace("inpatient_sub_menu2_linkage.php", "", $_SERVER["SCRIPT_NAME"]);
$callbacker_url = $callbacker_url_prefix . $_SERVER["HTTP_HOST"] . $collbacker_url_dir . "inpatient_sub_menu2_linkage_callbacker.php?session=$session&bldg_cd=$bldg_cd&ward_cd=$ward_cd&room_no=$room_no&bed_no=$bed_no";

// �ǡ����١�������³
$con = connect2db($fname);

// ���Ը���URL�����
$sql = "select linkage_url from ptadm";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$linkage_url = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "linkage_url") : "";

// ����̾�����
$sql = "select ward_name from wdmst";
$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ward_name = pg_fetch_result($sel, 0, "ward_name");

// �¼�̾�����
$sql = "select ptrm_name from ptrmmst";
$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd and ptrm_room_no = $room_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ptrm_name = pg_fetch_result($sel, 0, "ptrm_name");

// �����¾��ؤ�����ͽ������
$sql = "select ptif_id, inpt_lt_kj_nm, inpt_ft_kj_nm, inpt_in_res_dt, inpt_in_res_tm from inptres";
$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd and ptrm_room_no = $room_no and inpt_bed_no = '$bed_no' and inpt_in_res_dt_flg = 't'";
$sel_in = select_from_table($con, $sql, $cond, $fname);
if ($sel_in == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// �����¾��ؤΰ�žͽ������
$sql = "select inptmove.ptif_id, ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm, inptmove.move_dt, inptmove.move_tm from inptmove inner join ptifmst on inptmove.ptif_id = ptifmst.ptif_id";
$cond = "where inptmove.to_bldg_cd = $bldg_cd and inptmove.to_ward_cd = $ward_cd and inptmove.to_ptrm_room_no = $room_no and inptmove.to_bed_no = '$bed_no' and inptmove.move_cfm_flg = 'f' and inptmove.move_del_flg = 'f'";
$sel_mv = select_from_table($con, $sql, $cond, $fname);
if ($sel_mv == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// ͽ������ޡ������������ǥ�����
$reserve_list = array();
while ($row = pg_fetch_array($sel_in)) {
	$reserve_info = array();
	$reserve_info["ptif_id"] = $row["ptif_id"];
	$reserve_info["pt_nm"] = $row["inpt_lt_kj_nm"] . " " . $row["inpt_ft_kj_nm"];
	$reserve_info["type"] = "����ͽ��";
	$reserve_info["datetime"] = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $row["inpt_in_res_dt"]) . " " . preg_replace("/(\d{2})(\d{2})/", "$1:$2", $row["inpt_in_res_tm"]);
	$reserve_info["button"] = "����";
	$reserve_info["js"] = "registerIn";
	$reserve_list[] = $reserve_info;
}
while ($row = pg_fetch_array($sel_mv)) {
	$reserve_info = array();
	$reserve_info["ptif_id"] = $row["ptif_id"];
	$reserve_info["pt_nm"] = $row["ptif_lt_kaj_nm"] . " " . $row["ptif_ft_kaj_nm"];
	$reserve_info["type"] = "ž��ͽ��";
	$reserve_info["datetime"] = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $row["move_dt"]) . " " . preg_replace("/(\d{2})(\d{2})/", "$1:$2", $row["move_tm"]);
	$reserve_info["button"] = "ž��";
	$reserve_info["js"] = "registerMove";
	$reserve_list[] = $reserve_info;
}
usort($reserve_list, "sort_reserve_list");

function sort_reserve_list($reserve_info1, $reserve_info2) {
	return strcmp($reserve_info1["datetime"], $reserve_info2["datetime"]);
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function registerIn(ptif_id) {
	opener.location.href = 'inpatient_register.php?session=<? echo($session); ?>&pt_id=' + ptif_id + '&ward=<? echo("{$bldg_cd}-{$ward_cd}"); ?>&ptrm=<? echo($room_no); ?>&bed=<? echo($bed_no); ?>&bedundec1=f&bedundec2=f&bedundec3=f&path=3';
	window.close();
}

function reserveIn(ptif_id) {
	opener.location.href = 'inpatient_reserve_register.php?session=<? echo($session); ?>&pt_id=' + ptif_id + '&ward=<? echo("{$bldg_cd}-{$ward_cd}"); ?>&ptrm=<? echo($room_no); ?>&bed=<? echo($bed_no); ?>&bedundec1=f&bedundec2=f&bedundec3=f&path=1';
	window.close();
}

function registerMove(ptif_id) {
	location.href = 'inpatient_move_register.php?session=<? echo($session); ?>&pt_id=' + ptif_id;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j14" color="#ffffff"><b>��Ͽ��������</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="�Ĥ���" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="4" class="list">
<tr height="22">
<td width="" align="right" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18">����</font></td>
<td width=""><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18"><? echo($ward_name); ?></font></td>
<td width="" align="right" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18">�¼�</font></td>
<td width=""><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18"><? echo($ptrm_name); ?></font></td>
<td width="" align="right" bgcolor="#f6f9ff"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18">�٥å�No</font></td>
<td width=""><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18"><? echo($bed_no); ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<? if (count($reserve_list) > 0) { ?>
<table width="600" border="0" cellspacing="0" cellpadding="4" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="30%"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18">���Ի�̾</font></td>
<td width="20%"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18">����</font></td>
<td width="40%"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18">ͽ������</font></td>
<td width="10%" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18">��Ͽ</font></td>
</tr>
<? foreach ($reserve_list as $reserve_info) { ?>
<tr height="22">
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18"><? echo($reserve_info["pt_nm"]); ?></font></td>
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18"><? echo($reserve_info["type"]); ?></font></td>
<td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18"><? echo($reserve_info["datetime"]); ?></font></td>
<td align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18"><input type="button" value="<? echo($reserve_info["button"]); ?>" onclick="<? echo($reserve_info["js"]); ?>('<? echo($reserve_info["ptif_id"]); ?>');"></font></td>
</tr>
<? } ?>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<? } ?>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="160" align="center" bgcolor="#5279a5"><a href="inpatient_sub_menu2_linkage.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>&room_no=<? echo($room_no); ?>&bed_no=<? echo($bed_no); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18" color="#ffffff"><b>����ͽ�ꡦ����</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="inpatient_sub_menu2_patient.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>&room_no=<? echo($room_no); ?>&bed_no=<? echo($bed_no); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18">������Ͽ</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="inpatient_sub_menu2_temp.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>&room_no=<? echo($room_no); ?>&bed_no=<? echo($bed_no); ?>"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j18">���ޤ�</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<iframe width="600" height="500" src="<? echo($linkage_url); ?>?callbacker_url=<? echo(urlencode($callbacker_url)); ?>" frameborder="0"></iframe>
</center>
</body>
<? pg_close($con); ?>
</html>
