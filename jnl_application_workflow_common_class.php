<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

class jnl_application_workflow_common_class
{

	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション

	/**
	 * コンストラクタ
	 * @param object $con DBコネクション
	 * @param string $fname 画面名
	 */
	function jnl_application_workflow_common_class($con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		// 全項目リストを初期化
	}


//-------------------------------------------------------------------------------------------------------------------------
// フォルダツリー関連
//-------------------------------------------------------------------------------------------------------------------------

	/**
	 * ワークフローカテゴリ/フォルダ情報取得
	 * @return   array  ワークフローカテゴリ/フォルダ情報配列
	 */
	function get_workflow_folder_list(&$wkfw_counts)
	{
		$category_list = $this->get_workflow_category();

		foreach($category_list as $i => $category)
		{
			$wkfw_type = $category["wkfw_type"];
			$category["type"] = "category";
			$category["folders"] = $this->get_workflow_tree("", $wkfw_type);

			$category_list[$i] = $category;
		}

		// フォルダごとのワークフロー数を算出
		$wkfw_counts = $this->calc_workflow_for_count($category_list, "workflow", array(), "");

		return $category_list;
	}


	/**
	 * ワークフローカテゴリ情報取得
	 * @return   array  ワークフローカテゴリ情報配列
	 */
	function get_workflow_category()
	{
		$sql = "select wkfw_type, wkfw_nm from jnl_wkfwcatemst";
		$cond = "where not wkfwcate_del_flg order by wkfw_type asc";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "wkfw_nm" => $row["wkfw_nm"]);
		}
		return $arr;
	}

	/**
	 * ワークフローフォルダ情報取得
	 * @param    string  $parent_id   親フォルダID
	 * @param    string  $category_id カテゴリID
	 * @return   array  ワークフローフォルダ情報配列
	 */
	function get_workflow_tree($parent_id, $category_id)
	{

		$sql  = "select a.wkfw_folder_id as id, a.wkfw_type as cate, a.wkfw_folder_name as name ";
		$sql .= "from jnl_wkfwfolder a ";
		$sql .= "left join (select * from jnl_wkfwtree where not wkfw_tree_del_flg) b on a.wkfw_folder_id = b.wkfw_child_id ";
		$sql .= "where not a.wkfw_folder_del_flg ";

		if($parent_id == "")
		{
			$sql .= "and b.wkfw_parent_id is null ";
		}
		else
		{
			$sql .= "and b.wkfw_parent_id = $parent_id ";
		}

		if ($category_id != "") {
			$sql .= " and a.wkfw_type = $category_id ";
		}
		$sql .= "order by a.wkfw_folder_id ";

		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);

		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while ($row = pg_fetch_array($sel))
		{
			$id      = $row["id"];
			$cate    = $row["cate"];
			$name    = $row["name"];

			$arr[] = array(
				"type"      => "folder",
				"cate"      => $cate,
				"parent_id" => $parent_id,
				"id"        => $id,
				"name"      => $name,
				"folders"   => $this->get_workflow_tree($id, $cate)
			);
		}

		return $arr;
	}

	// ワークフローフォルダ一覧取得(修正あり)
	function get_cate_workflow_list($category_id, $folder_id, $parent_id)
	{

		if($category_id != "" && $folder_id == "")
		{
			$sql  = "select 1 as num, varchar(6) 'folder' as type, a.wkfw_folder_id as id, a.wkfw_type as cate, a.wkfw_folder_name as name, b.wkfw_parent_id as parent_id ";
			$sql .= "from jnl_wkfwfolder a ";
			$sql .= "left join (select * from jnl_wkfwtree where not wkfw_tree_del_flg) b on a.wkfw_folder_id = b.wkfw_child_id ";
			$sql .= "where b.wkfw_parent_id is null and a.wkfw_type = $category_id and not a.wkfw_folder_del_flg ";
		}
		else if($category_id != "" && $folder_id != "")
		{
			$sql  = "select 1 as num, varchar(6) 'folder' as type, a.wkfw_folder_id as id, a.wkfw_type as cate, a.wkfw_folder_name as name, b.wkfw_parent_id as parent_id ";
			$sql .= "from jnl_wkfwfolder a ";
			$sql .= "inner join (SELECT * FROM jnl_wkfwtree WHERE wkfw_parent_id = $folder_id and not wkfw_tree_del_flg) b on a.wkfw_folder_id = b.wkfw_child_id ";
			$sql .= "where not a.wkfw_folder_del_flg ";
		}
		$sql .= "union all ";

		$sql .= "select 2 as num, varchar(6) 'file' as type, a.wkfw_id as id, a.wkfw_type as cate, a.wkfw_title as name, null as parent_id ";
		$sql .= "from jnl_wkfwmst a ";
		$sql .= "where a.wkfw_type = $category_id and not a.wkfw_del_flg ";
		if($folder_id == "")
		{
			$sql .= "and a.wkfw_folder_id is null ";
		}
		else if($folder_id != "")
		{
			$sql .= "and a.wkfw_folder_id = $folder_id ";
		}

		$sql .= "order by num, id asc ";

		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);

		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while ($row = pg_fetch_array($sel))
		{
			$type               = $row["type"];
			$tmp_folder_id      = $row["id"];
			$tmp_category_type  = $row["cate"];
			$wkfw_folder_name   = $row["name"];
			$tmp_parent_id      = $row["parent_id"];

			if($type == "folder")
			{
				$arr[] = array(
					"type" => $type,
					"cate" => $tmp_category_type,
					"parent_id" => $tmp_parent_id,
					"id" => $tmp_folder_id,
					"name" => $wkfw_folder_name,
				);
			}
			else if($type == "file")
			{
				$arr[] = array(
					"type" => $type,
					"cate" => $tmp_category_type,
					"parent_id" => null,
					"id" => $tmp_folder_id,
					"name" => $wkfw_folder_name,
				);
			}
		}
		return $arr;
	}

	// フォルダパス取得
	function get_folder_path($folder_id)
	{

		// 当該フォルダ情報を取得
		$sql = "select wkfw_folder_id, wkfw_folder_name from jnl_wkfwfolder";
		$cond = "where wkfw_folder_id = $folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$folder_name = pg_fetch_result($sel, 0, "wkfw_folder_name");
		$arr = array("id" => $folder_id, "name" => $folder_name);

		// 親フォルダ情報を取得
		$sql = "select wkfw_parent_id from jnl_wkfwtree";
		$cond = "where wkfw_child_id = $folder_id and not wkfw_tree_del_flg";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 親フォルダが存在しない場合は自分の情報のみ返す
		if (pg_num_rows($sel) == 0) {
			return array($arr);
		}

		// 親フォルダが存在した場合は先祖の情報も含めて返す
		$parent_id = pg_fetch_result($sel, 0, "wkfw_parent_id");
		$ancestor = $this->get_folder_path($parent_id);
		$ancestor[] = $arr;
		return $ancestor;
	}

	// カテゴリ取得
	function get_wkfwcate_mst($wkfw_type)
	{
		$sql = "select wkfw_type, wkfw_nm, ref_dept_st_flg, ref_dept_flg, ref_st_flg from jnl_wkfwcatemst";
		$cond = "where not wkfwcate_del_flg and wkfw_type = $wkfw_type";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$wkfw_type       = pg_fetch_result($sel, 0, "wkfw_type");
		$wkfw_nm         = pg_fetch_result($sel, 0, "wkfw_nm");
		$ref_dept_st_flg = pg_fetch_result($sel, 0, "ref_dept_st_flg");
		$ref_dept_flg    = pg_fetch_result($sel, 0, "ref_dept_flg");
		$ref_st_flg      = pg_fetch_result($sel, 0, "ref_st_flg");

		$arr  = array("wkfw_type" => $wkfw_type,
		                "wkfw_nm" => $wkfw_nm,
                        "ref_dept_st_flg" => $ref_dept_st_flg,
                        "ref_dept_flg" => $ref_dept_flg,
                        "ref_st_flg" => $ref_st_flg);
		return $arr;
	}

	// フォルダ取得
	function get_wkfwfolder_mst($wkfw_folder_id)
	{
		$sql = "select wkfw_folder_id, wkfw_folder_name, wkfw_folder_del_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg from jnl_wkfwfolder";
		$cond = "where not wkfw_folder_del_flg and wkfw_folder_id = $wkfw_folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$foder_id        = pg_fetch_result($sel, 0, "wkfw_folder_id");
		$folder_name     = pg_fetch_result($sel, 0, "wkfw_folder_name");
		$ref_dept_st_flg = pg_fetch_result($sel, 0, "ref_dept_st_flg");
		$ref_dept_flg    = pg_fetch_result($sel, 0, "ref_dept_flg");
		$ref_st_flg      = pg_fetch_result($sel, 0, "ref_st_flg");

		$arr = array("wkfw_folder_id" => $foder_id,
                      "wkfw_folder_name" => $folder_name,
                      "ref_dept_st_flg" => $ref_dept_st_flg,
                      "ref_dept_flg" => $ref_dept_flg,
                      "ref_st_flg" => $ref_st_flg);

		return $arr;
	}


	// ワークフロー取得(カウント用)
	function get_workflow_for_count($srceen_mode, $arr_emp_info, $emp_id)
	{
		$sql = "select wkfw_type, wkfw_folder_id from jnl_wkfwmst";
		$cond = "where not wkfw_del_flg";

		if($srceen_mode == "apply")
		{
			$today = date("Ymd");
			$cond .= " and ((wkfw_start_date <= '$today' and wkfw_end_date >= '$today') or ";
			$cond .= "(wkfw_start_date is null and wkfw_end_date is null) or ";
			$cond .= "(wkfw_start_date <= '$today' and wkfw_end_date is null) or ";
			$cond .= "(wkfw_start_date is null and wkfw_end_date >= '$today')) ";


			if($arr_emp_info != "")
			{
				$cond .= "and (";

				for($i=0; $i<count($arr_emp_info); $i++)
				{
					$emp_class     = $arr_emp_info[$i]["emp_class"];
					$emp_attribute = $arr_emp_info[$i]["emp_attribute"];
					$emp_dept      = $arr_emp_info[$i]["emp_dept"];
					$emp_st        = $arr_emp_info[$i]["emp_st"];

					if($i > 0)
					{
						$cond .= "or ";
					}

					$cond .= "(";

					$cond .= "(jnl_wkfwmst.ref_dept_flg = '1' or (jnl_wkfwmst.ref_dept_flg = '2' and ";
					$cond .= "exists (select * from jnl_wkfw_refdept where jnl_wkfw_refdept.wkfw_id = jnl_wkfwmst.wkfw_id ";
					$cond .= "and jnl_wkfw_refdept.class_id = $emp_class and jnl_wkfw_refdept.atrb_id = $emp_attribute and jnl_wkfw_refdept.dept_id = $emp_dept))) ";

					$cond .= "and ";

					$cond .= "(jnl_wkfwmst.ref_st_flg = '1' or (jnl_wkfwmst.ref_st_flg = '2' and exists (select * from jnl_wkfw_refst where jnl_wkfw_refst.wkfw_id = jnl_wkfwmst.wkfw_id ";
					$cond .= "and jnl_wkfw_refst.st_id = $emp_st)))";

					$cond .= ") ";
				}

				$cond .= "or exists (select * from jnl_wkfw_refemp where jnl_wkfw_refemp.wkfw_id = jnl_wkfwmst.wkfw_id and jnl_wkfw_refemp.emp_id = '$emp_id')";
				$cond .= ") ";
			}
		}

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "wkfw_folder_id" => $row["wkfw_folder_id"]);
		}

		return $arr;
	}

	// ワークフロー数を算出
	function calc_workflow_for_count($folder_list, $screen_mode, $arr_emp_info, $emp_id)
	{

		$wkfw_list = $this->get_workflow_for_count($screen_mode, $arr_emp_info, $emp_id);
		$wkfw_count_per_folder = array();
		if (!is_null($wkfw_list))
		{
			foreach ($wkfw_list as $wkfw) {
				$cate_id = $wkfw["wkfw_type"];
				$folder_id = ($wkfw["wkfw_folder_id"] != "") ? $wkfw["wkfw_folder_id"] : "-";
				$doc_count_per_folder[$cate_id][$folder_id]++;
			}
		}
		if (!is_null($wkfw_list))
		{
			$wkfw_counts = array();
			foreach ($folder_list as $folder) {
				$this->calc_wkfw_counts_per_folder($folder, $doc_count_per_folder, $wkfw_counts);
			}
		}
		return $wkfw_counts;
	}

	// 指定フォルダ配下のワークフロー数を算出
	function calc_wkfw_counts_per_folder($folder, $doc_count_per_folder, &$wkfw_counts) {

		if ($folder["type"] == "category")
		{
			$cate_id = $folder["wkfw_type"];
			$folder_id = "-";
		}
		else if($folder["type"] == "folder")
		{
			$cate_id = $folder["cate"];
			$folder_id = $folder["id"];
		}

		$wkfw_counts[$cate_id][$folder_id] = intval($doc_count_per_folder[$cate_id][$folder_id]);

		foreach ($folder["folders"] as $folder)
		{
			$wkfw_counts[$cate_id][$folder_id] += $this->calc_wkfw_counts_per_folder($folder, $doc_count_per_folder, $wkfw_counts);
		}

		return $wkfw_counts[$cate_id][$folder_id];
	}


	// ワークフローカテゴリ/フォルダ情報取得
	function get_workflow_folder_list_for_apply($emp_id, &$wkfw_counts)
	{

		$today = date("Ymd");

		// 職員の部署・役職取得
		if($emp_id != "")
		{
			$arr_emp_info = $this->get_emp_info($emp_id);
		}
		else
		{
			$arr_emp_info = "";
		}

		// カテゴリ取得
		$category_list = $this->get_workflow_category_for_apply($arr_emp_info, $emp_id);

		// フォルダ/ワークフロー取得
		foreach($category_list as $i => $category)
		{
			$wkfw_type = $category["wkfw_type"];
			$category["type"] = "category";
			$category["folders"] = $this->get_workflow_tree_for_apply("", $wkfw_type, $today, $arr_emp_info, $emp_id);

			$category_list[$i] = $category;
		}

		// フォルダごとのワークフロー数を算出
		$wkfw_counts = $this->calc_workflow_for_count($category_list, "apply", $arr_emp_info, $emp_id);

		return $category_list;
	}

	// ワークフローカテゴリ/フォルダ情報取得(申請参照一覧用)
	function get_workflow_folder_list_for_apply_refer(&$apply_counts)
	{

		// カテゴリ取得
		$category_list = $this->get_workflow_category_for_apply_refer();

		// フォルダ/ワークフロー取得
		foreach($category_list as $i => $category)
		{
			$wkfw_type = $category["wkfw_type"];
			$category["type"] = "category";
			$category["folders"] = $this->get_workflow_tree_for_apply_refer("", $wkfw_type);

			$category_list[$i] = $category;
		}

		// フォルダごとの申請数を算出
		$apply_counts = $this->calc_apply_for_count($category_list);

		return $category_list;
	}

    // ワークフローカテゴリ情報取得(申請参照一覧用)
	function get_workflow_category_for_apply_refer()
	{
		// カテゴリ情報取得
		$sql   = "select wkfw_type, wkfw_nm from jnl_wkfwcatemst";
		$cond  = "where not wkfwcate_del_flg ";
		$cond .= "order by wkfw_type asc ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "wkfw_nm" => $row["wkfw_nm"]);
		}
		return $arr;
	}

	// フォルダ/ワークフロー情報取得(申請参照一覧用)
	function get_workflow_tree_for_apply_refer($parent_id, $category_id)
	{
		// フォルダ
		$sql  = "select varchar(6) 'folder' as type, jnl_wkfwfolder.wkfw_folder_id as id, jnl_wkfwfolder.wkfw_type as cate, jnl_wkfwfolder.wkfw_folder_name as name from jnl_wkfwfolder ";
		$sql .= "left join (select * from jnl_wkfwtree where not wkfw_tree_del_flg) jnl_wkfwtree on jnl_wkfwfolder.wkfw_folder_id = jnl_wkfwtree.wkfw_child_id ";
		$sql .= "where not jnl_wkfwfolder.wkfw_folder_del_flg ";

		if($parent_id == "")
		{
			$sql .= "and jnl_wkfwtree.wkfw_parent_id is null ";
		}
		else
		{
			$sql .= "and jnl_wkfwtree.wkfw_parent_id = $parent_id ";
		}
		if ($category_id != "") {
			$sql .= " and jnl_wkfwfolder.wkfw_type = $category_id ";
		}

		$sql .= "union all ";

		// ワークフロー
		$sql .= "select varchar(6) 'file' as type, jnl_wkfwmst.wkfw_id as id, jnl_wkfwmst.wkfw_type as cate, jnl_wkfwmst.wkfw_title as name from jnl_wkfwmst ";
		$sql .= "where wkfw_type = $category_id and not wkfw_del_flg ";

		if($parent_id == "")
		{
			$sql .= "and wkfw_folder_id is null ";
		}
		else if($parent_id != "")
		{
			$sql .= "and wkfw_folder_id = $parent_id ";
		}

		$sql .= "order by type desc, id asc ";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$tree = array();
		while ($row = pg_fetch_array($sel))
		{
			$type               = $row["type"];
			$folder_id          = $row["id"];
			$category_type      = $row["cate"];
			$wkfw_folder_name   = $row["name"];

			if($type == "folder")
			{
				$tree[] = array(
					"type" => $type,
					"cate" => $category_type,
					"parent_id" => $parent_id,
					"id" => $folder_id,
					"name" => $wkfw_folder_name,
					"folders" => $this->get_workflow_tree_for_apply($folder_id, $category_type)
				);
			}
			else if($type == "file")
			{
				$tree[] = array(
					"type" => $type,
					"cate" => $category_type,
					"parent_id" => $parent_id,
					"id" => $folder_id,
					"name" => $wkfw_folder_name,
					"folders" => array()
				);
			}
		}
		return $tree;
	}

	// 申請数を算出
	function calc_apply_for_count($folder_list)
	{

		$wkfw_list = $this->get_apply_for_count();
		$wkfw_count_per_folder = array();
		if (!is_null($wkfw_list))
		{
			foreach ($wkfw_list as $wkfw) {
				$cate_id = $wkfw["wkfw_type"];
				$folder_id = ($wkfw["wkfw_folder_id"] != "") ? $wkfw["wkfw_folder_id"] : "-";
				$doc_count_per_folder[$cate_id][$folder_id]++;
			}
		}

		if (!is_null($wkfw_list))
		{
			$wkfw_counts = array();
			foreach ($folder_list as $folder) {
				$this->calc_apply_counts_per_folder($folder, $doc_count_per_folder, $wkfw_counts);
			}
		}
		return $wkfw_counts;
	}

	// 申請数取得(カウント用)
	function get_apply_for_count()
	{
		$sql  = "select a.wkfw_type, a.wkfw_folder_id, a.wkfw_id, b.apply_id ";
		$sql .= "from jnl_wkfwmst a ";
		$sql .= "inner join jnl_apply b on a.wkfw_id = b.wkfw_id ";
		$cond = "where not wkfw_del_flg and not b.draft_flg and not b.delete_flg ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "wkfw_folder_id" => $row["wkfw_folder_id"], "wkfwr_id" => $row["wkfw_id"]);
		}

		return $arr;
	}

	// 指定フォルダ配下の申請数を算出
	function calc_apply_counts_per_folder($folder, $doc_count_per_folder, &$wkfw_counts) {

		if ($folder["type"] == "category")
		{
			$cate_id = $folder["wkfw_type"];
			$folder_id = "-";
			$wkfw_id = "-";
		}
		else if($folder["type"] == "folder")
		{
			$cate_id = $folder["cate"];
			$folder_id = $folder["id"];
			$wkfw_id = "-";
		}

		$wkfw_counts[$cate_id][$folder_id] = intval($doc_count_per_folder[$cate_id][$folder_id]);

		foreach ($folder["folders"] as $folder)
		{
			$wkfw_counts[$cate_id][$folder_id] += $this->calc_apply_counts_per_folder($folder, $doc_count_per_folder, $wkfw_counts);
		}

		return $wkfw_counts[$cate_id][$folder_id];
	}

	function get_apply_count($wkfw_id)
	{
		$sql  = "select count(*) as cnt from jnl_apply ";
		$cond = "where not draft_flg and not delete_flg and wkfw_id = $wkfw_id";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "cnt");
	}


	// フォルダ/ワークフロー情報取得
	function get_workflow_tree_for_apply($parent_id, $category_id, $today, $arr_emp_info, $emp_id)
	{

		// フォルダ
		$sql  = "select varchar(6) 'folder' as type, jnl_wkfwfolder.wkfw_folder_id as id, jnl_wkfwfolder.wkfw_type as cate, jnl_wkfwfolder.wkfw_folder_name as name from jnl_wkfwfolder ";
		$sql .= "left join (select * from jnl_wkfwtree where not wkfw_tree_del_flg) jnl_wkfwtree on jnl_wkfwfolder.wkfw_folder_id = jnl_wkfwtree.wkfw_child_id ";
		$sql .= "where not jnl_wkfwfolder.wkfw_folder_del_flg ";

		if($parent_id == "")
		{
			$sql .= "and jnl_wkfwtree.wkfw_parent_id is null ";
		}
		else
		{
			$sql .= "and jnl_wkfwtree.wkfw_parent_id = $parent_id ";
		}
		if ($category_id != "") {
			$sql .= " and jnl_wkfwfolder.wkfw_type = $category_id ";
		}

		if($arr_emp_info != "")
		{
			$sql .= "and (";

			for($i=0; $i<count($arr_emp_info); $i++)
			{
				$emp_class     = $arr_emp_info[$i]["emp_class"];
				$emp_attribute = $arr_emp_info[$i]["emp_attribute"];
				$emp_dept      = $arr_emp_info[$i]["emp_dept"];
				$emp_st        = $arr_emp_info[$i]["emp_st"];

				if($i > 0)
				{
					$sql .= "or ";
				}

				$sql .= "(";

				$sql .= "(jnl_wkfwfolder.ref_dept_flg = '1' or (jnl_wkfwfolder.ref_dept_flg = '2' and ";
				$sql .= "exists (select * from jnl_wkfwfolder_refdept where jnl_wkfwfolder_refdept.wkfw_folder_id = jnl_wkfwfolder.wkfw_folder_id ";
				$sql .= "and jnl_wkfwfolder_refdept.class_id = $emp_class and jnl_wkfwfolder_refdept.atrb_id = $emp_attribute and jnl_wkfwfolder_refdept.dept_id = $emp_dept))) ";

				$sql .= "and ";

				$sql .= "(jnl_wkfwfolder.ref_st_flg = '1' or (jnl_wkfwfolder.ref_st_flg = '2' and exists (select * from jnl_wkfwfolder_refst where jnl_wkfwfolder_refst.wkfw_folder_id = jnl_wkfwfolder.wkfw_folder_id ";
				$sql .= "and jnl_wkfwfolder_refst.st_id = $emp_st)))";

				$sql .= ") ";
			}
			$sql .= "or exists (select * from jnl_wkfwfolder_refemp where jnl_wkfwfolder_refemp.wkfw_folder_id = jnl_wkfwfolder.wkfw_folder_id and jnl_wkfwfolder_refemp.emp_id = '$emp_id')";
			$sql .= ") ";
		}

		$sql .= "union all ";


		// ワークフロー
		$sql .= "select varchar(6) 'file' as type, jnl_wkfwmst.wkfw_id as id, jnl_wkfwmst.wkfw_type as cate, jnl_wkfwmst.wkfw_title as name from jnl_wkfwmst ";
		$sql .= "where wkfw_type = $category_id and not wkfw_del_flg ";

		$sql .= "and ((wkfw_start_date <= '$today' and wkfw_end_date >= '$today') or ";
		$sql .= "(wkfw_start_date is null and wkfw_end_date is null) or ";
		$sql .= "(wkfw_start_date <= '$today' and wkfw_end_date is null) or ";
		$sql .= "(wkfw_start_date is null and wkfw_end_date >= '$today')) ";

		if($parent_id == "")
		{
			$sql .= "and wkfw_folder_id is null ";
		}
		else if($parent_id != "")
		{
			$sql .= "and wkfw_folder_id = $parent_id ";
		}

		if($arr_emp_info != "")
		{
			$sql .= "and (";

			for($i=0; $i<count($arr_emp_info); $i++)
			{
				$emp_class     = $arr_emp_info[$i]["emp_class"];
				$emp_attribute = $arr_emp_info[$i]["emp_attribute"];
				$emp_dept      = $arr_emp_info[$i]["emp_dept"];
				$emp_st        = $arr_emp_info[$i]["emp_st"];

				if($i > 0)
				{
					$sql .= "or ";
				}

				$sql .= "(";

				$sql .= "(jnl_wkfwmst.ref_dept_flg = '1' or (jnl_wkfwmst.ref_dept_flg = '2' and ";
				$sql .= "exists (select * from jnl_wkfw_refdept where jnl_wkfw_refdept.wkfw_id = jnl_wkfwmst.wkfw_id ";
				$sql .= "and jnl_wkfw_refdept.class_id = $emp_class and jnl_wkfw_refdept.atrb_id = $emp_attribute and jnl_wkfw_refdept.dept_id = $emp_dept))) ";

				$sql .= "and ";

				$sql .= "(jnl_wkfwmst.ref_st_flg = '1' or (jnl_wkfwmst.ref_st_flg = '2' and exists (select * from jnl_wkfw_refst where jnl_wkfw_refst.wkfw_id = jnl_wkfwmst.wkfw_id ";
				$sql .= "and jnl_wkfw_refst.st_id = $emp_st)))";

				$sql .= ") ";
			}

			$sql .= "or exists (select * from jnl_wkfw_refemp where jnl_wkfw_refemp.wkfw_id = jnl_wkfwmst.wkfw_id and jnl_wkfw_refemp.emp_id = '$emp_id')";
			$sql .= ") ";
		}

		$sql .= "order by type desc, id asc ";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$tree = array();
		while ($row = pg_fetch_array($sel))
		{
			$type               = $row["type"];
			$folder_id          = $row["id"];
			$category_type      = $row["cate"];
			$wkfw_folder_name   = $row["name"];

			if($type == "folder")
			{
				$tree[] = array(
					"type" => $type,
					"cate" => $category_type,
					"parent_id" => $parent_id,
					"id" => $folder_id,
					"name" => $wkfw_folder_name,
					"folders" => $this->get_workflow_tree_for_apply($folder_id, $category_type, $today, $arr_emp_info, $emp_id)
				);
			}
			else if($type == "file")
			{
				$tree[] = array(
					"type" => $type,
					"cate" => $category_type,
					"parent_id" => $parent_id,
					"id" => $folder_id,
					"name" => $wkfw_folder_name,
					"folders" => array()
				);
			}
		}
		return $tree;
	}

	// カテゴリ名取得
	function get_category_from_folder_id($folder_id)
	{
		$sql  = "select wkfw_type, wkfw_nm from jnl_wkfwcatemst ";
		$cond = "where wkfw_type in (select wkfw_type from jnl_wkfwfolder where wkfw_folder_id = $folder_id) and not wkfwcate_del_flg ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$wkfw_type        = pg_fetch_result($sel, 0, "wkfw_type");
		$wkfw_nm = pg_fetch_result($sel, 0, "wkfw_nm");

		$arr[] = array("wkfw_type" => $wkfw_type, "wkfw_nm" => $wkfw_nm);

		return $arr;
	}

    // ワークフローカテゴリ情報取得(申請画面用)
	function get_workflow_category_for_apply($arr_emp_info, $emp_id)
	{
		// カテゴリ情報取得
		$sql   = "select wkfw_type, wkfw_nm from jnl_wkfwcatemst";
		$cond  = "where not wkfwcate_del_flg ";

		if($arr_emp_info !=  "")
		{
			$cond .= "and (";

			for($i=0; $i<count($arr_emp_info); $i++)
			{
				$emp_class     = $arr_emp_info[$i]["emp_class"];
				$emp_attribute = $arr_emp_info[$i]["emp_attribute"];
				$emp_dept      = $arr_emp_info[$i]["emp_dept"];
				$emp_st        = $arr_emp_info[$i]["emp_st"];

				if($i > 0)
				{
					$cond .= "or ";
				}

				$cond .= "(";

				$cond .= "(ref_dept_flg = '1' or (ref_dept_flg = '2' and ";
				$cond .= "exists (select * from jnl_wkfwcate_refdept where jnl_wkfwcate_refdept.wkfw_type = jnl_wkfwcatemst.wkfw_type ";
				$cond .= "and jnl_wkfwcate_refdept.class_id = $emp_class and jnl_wkfwcate_refdept.atrb_id = $emp_attribute and jnl_wkfwcate_refdept.dept_id = $emp_dept))) ";

				$cond .= "and ";

				$cond .= "(ref_st_flg = '1' or (ref_st_flg = '2' and exists (select * from jnl_wkfwcate_refst where jnl_wkfwcate_refst.wkfw_type = jnl_wkfwcatemst.wkfw_type ";
				$cond .= "and jnl_wkfwcate_refst.st_id = $emp_st)))";

				$cond .= ") ";
			}

			$cond .= "or exists (select * from jnl_wkfwcate_refemp where jnl_wkfwcate_refemp.wkfw_type = jnl_wkfwcatemst.wkfw_type and jnl_wkfwcate_refemp.emp_id = '$emp_id')";
			$cond .= ") ";
		}
		$cond .= "order by wkfw_type asc ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "wkfw_nm" => $row["wkfw_nm"]);
		}
		return $arr;
	}


	// 職員の部署役職取得
	function get_emp_info($emp_id)
	{
		// 職員情報取得(複数部署役職対応)
		$sql  = "select emp_class, emp_attribute, emp_dept, emp_room, emp_st from empmst where emp_id = '$emp_id' ";
		$sql .= "union ";
		$sql .= "select emp_class, emp_attribute, emp_dept, emp_room, emp_st from concurrent where emp_id = '$emp_id'";

		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("emp_class" => $row["emp_class"], "emp_attribute" => $row["emp_attribute"], "emp_dept" => $row["emp_dept"], "emp_room" => $row["emp_room"], "emp_st" => $row["emp_st"]);
		}

		return $arr;
	}

//-------------------------------------------------------------------------------------------------------------------------
// アクセス権限関連
//-------------------------------------------------------------------------------------------------------------------------

	// 部門一覧取得
	function get_classmst()
	{
		$sql = "select class_id, class_nm from classmst";
		$cond = "where class_del_flg = 'f' order by order_no";
		$sel_class = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_class == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_class;
	}

	// 課一覧取得
	function get_atrbmst()
	{
		$sql = "select classmst.class_id, atrbmst.atrb_id, classmst.class_nm, atrbmst.atrb_nm from atrbmst inner join classmst on atrbmst.class_id = classmst.class_id";
		$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' order by classmst.order_no, atrbmst.order_no";
		$sel_atrb = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_atrb == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_atrb;
	}

	// 科一覧取得
	function get_deptmst()
	{
		$sql = "select classmst.class_id, atrbmst.atrb_id, deptmst.dept_id, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm from deptmst inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id inner join classmst on atrbmst.class_id = classmst.class_id";
		$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' and deptmst.dept_del_flg = 'f' order by classmst.order_no, atrbmst.order_no, deptmst.order_no";
		$sel_dept = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_dept == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_dept;
	}

	// 役職一覧取得
	function get_stmst()
	{
		$sql = "select st_id, st_nm from stmst";
		$cond = "where st_del_flg = 'f' order by order_no";
		$sel_st = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_st == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_st;
	}

	// カテゴリ用アクセス権（科）登録
	function delete_regist_wkfwcate_refdept($wkfw_type, $arr_ref_dept, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from jnl_wkfwcate_refdept";
			$cond = "where wkfw_type = $wkfw_type";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}

		foreach ($arr_ref_dept as $ref_dept)
		{
			list($class_id, $atrb_id, $dept_id) = split("-", $ref_dept);
			$sql = "insert into jnl_wkfwcate_refdept (wkfw_type, class_id, atrb_id, dept_id) values (";
			$content = array($wkfw_type, $class_id, $atrb_id, $dept_id);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// カテゴリ用アクセス権（役職）登録
	function delete_regist_wkfwcate_refst($wkfw_type, $arr_ref_st, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from jnl_wkfwcate_refst";
			$cond = "where wkfw_type = $wkfw_type";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		foreach ($arr_ref_st as $ref_st)
		{
			$sql = "insert into jnl_wkfwcate_refst (wkfw_type, st_id) values (";
			$content = array($wkfw_type, $ref_st);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// カテゴリ用アクセス権（職員）登録
	function delete_regist_wkfwcate_refemp($wkfw_type, $arr_ref_emp, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from jnl_wkfwcate_refemp";
			$cond = "where wkfw_type = $wkfw_type";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		foreach ($arr_ref_emp as $ref_emp) {
			$sql = "insert into jnl_wkfwcate_refemp (wkfw_type, emp_id) values (";
			$content = array($wkfw_type, $ref_emp);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}


	// フォルダ用アクセス権（科）登録
	function delete_regist_wkfwfolder_refdept($wkfw_folder_id, $arr_ref_dept, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from jnl_wkfwfolder_refdept";
			$cond = "where wkfw_folder_id = $wkfw_folder_id";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		foreach ($arr_ref_dept as $ref_dept)
		{
			list($class_id, $atrb_id, $dept_id) = split("-", $ref_dept);
			$sql = "insert into jnl_wkfwfolder_refdept (wkfw_folder_id, class_id, atrb_id, dept_id) values (";
			$content = array($wkfw_folder_id, $class_id, $atrb_id, $dept_id);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// フォルダ用アクセス権（役職）登録
	function delete_regist_wkfwfolder_refst($wkfw_folder_id, $arr_ref_st, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from jnl_wkfwfolder_refst";
			$cond = "where wkfw_folder_id = $wkfw_folder_id";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		foreach ($arr_ref_st as $ref_st)
		{
			$sql = "insert into jnl_wkfwfolder_refst (wkfw_folder_id, st_id) values (";
			$content = array($wkfw_folder_id, $ref_st);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// フォルダ用アクセス権（職員）登録
	function delete_regist_wkfwfolder_refemp($wkfw_folder_id, $arr_ref_emp, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from jnl_wkfwfolder_refemp";
			$cond = "where wkfw_folder_id = $wkfw_folder_id";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		foreach ($arr_ref_emp as $ref_emp) {
			$sql = "insert into jnl_wkfwfolder_refemp (wkfw_folder_id, emp_id) values (";
			$content = array($wkfw_folder_id, $ref_emp);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// ワークフロー用アクセス権（科）登録
	function regist_wkfw_refdept($wkfw_id, $arr_ref_dept, $mode)
	{
		foreach ($arr_ref_dept as $ref_dept)
		{
			list($class_id, $atrb_id, $dept_id) = split("-", $ref_dept);

			if($mode == "ALIAS")
			{
				$sql = "insert into jnl_wkfw_refdept ";
			}
			else
			{
				$sql = "insert into jnl_wkfw_refdept_real ";
			}
			$sql .= "(wkfw_id, class_id, atrb_id, dept_id) values (";
			$content = array($wkfw_id, $class_id, $atrb_id, $dept_id);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// ワークフロー用アクセス権（科）削除
	function delete_wkfw_refdept($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from jnl_wkfw_refdept";
		}
		else
		{
			$sql = "delete from jnl_wkfw_refdept_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー用アクセス権（役職）登録
	function regist_wkfw_refst($wkfw_id, $arr_ref_st, $mode)
	{
		foreach ($arr_ref_st as $ref_st)
		{
			if($mode == "ALIAS")
			{
				$sql = "insert into jnl_wkfw_refst ";
			}
			else
			{
				$sql = "insert into jnl_wkfw_refst_real ";
			}

			$sql .= "(wkfw_id, st_id) values (";
			$content = array($wkfw_id, $ref_st);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// ワークフロー用アクセス権（役職）削除
	function delete_wkfw_refst($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from jnl_wkfw_refst";
		}
		else
		{
			$sql = "delete from jnl_wkfw_refst_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー用アクセス権（職員）登録
	function regist_wkfw_refemp($wkfw_id, $arr_ref_emp, $mode)
	{
		foreach ($arr_ref_emp as $ref_emp)
		{
			if($mode == "ALIAS")
			{
				$sql = "insert into jnl_wkfw_refemp ";
			}
			else
			{
				$sql = "insert into jnl_wkfw_refemp_real ";
			}

			$sql .= "(wkfw_id, emp_id) values (";
			$content = array($wkfw_id, $ref_emp);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// ワークフロー用アクセス権（職員）削除
	function delete_wkfw_refemp($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from jnl_wkfw_refemp";
		}
		else
		{
			$sql = "delete from jnl_wkfw_refemp_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// カテゴリ用アクセス権（科）取得
	function get_wkfwcate_refdept($wkfw_type)
	{
		$sql = "select wkfw_type, class_id, atrb_id, dept_id from jnl_wkfwcate_refdept";
		$cond = "where wkfw_type = $wkfw_type";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "class_id" => $row["class_id"], "atrb_id" => $row["atrb_id"], "dept_id" => $row["dept_id"]);
		}
		return $arr;
	}

	// カテゴリ用アクセス権（役職）取得
	function get_wkfwcate_refst($wkfw_type)
	{
		$sql = "select wkfw_type, st_id from jnl_wkfwcate_refst";
		$cond = "where wkfw_type = $wkfw_type";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "st_id" => $row["st_id"]);
		}
		return $arr;
	}

    // カテゴリ用アクセス権（職員）取得
	function get_wkfwcate_refemp($wkfw_type)
	{
		$sql = "select wkfw_type, emp_id from jnl_wkfwcate_refemp";
		$cond = "where wkfw_type = $wkfw_type";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "emp_id" => $row["emp_id"]);
		}
		return $arr;
	}

	// フォルダ用アクセス権（科）取得
	function get_wkfwfolder_refdept($wkfw_folder_id)
	{
		$sql = "select wkfw_folder_id, class_id, atrb_id, dept_id from jnl_wkfwfolder_refdept";
		$cond = "where wkfw_folder_id = $wkfw_folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_folder_id" => $row["wkfw_folder_id"], "class_id" => $row["class_id"], "atrb_id" => $row["atrb_id"], "dept_id" => $row["dept_id"]);
		}
		return $arr;
	}

	// フォルダ用アクセス権（役職）取得
	function get_wkfwfolder_refst($wkfw_folder_id)
	{
		$sql = "select wkfw_folder_id, st_id from jnl_wkfwfolder_refst";
		$cond = "where wkfw_folder_id = $wkfw_folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_folder_id" => $row["wkfw_folder_id"], "st_id" => $row["st_id"]);
		}
		return $arr;
	}

    // フォルダ用アクセス権（職員）取得
	function get_wkfwfolder_refemp($wkfw_folder_id)
	{
		$sql = "select wkfw_folder_id, emp_id from jnl_wkfwfolder_refemp";
		$cond = "where wkfw_folder_id = $wkfw_folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_folder_id" => $row["wkfw_folder_id"], "emp_id" => $row["emp_id"]);
		}
		return $arr;
	}

	// ワークフロー用アクセス権（科）取得
	function get_wkfw_refdept($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "select wkfw_id, class_id, atrb_id, dept_id from jnl_wkfw_refdept";
		}
		else
		{
			$sql = "select wkfw_id, class_id, atrb_id, dept_id from jnl_wkfw_refdept_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_id" => $row["wkfw_id"], "class_id" => $row["class_id"], "atrb_id" => $row["atrb_id"], "dept_id" => $row["dept_id"]);
		}
		return $arr;
	}

	// ワークフロー用アクセス権（役職）取得
	function get_wkfw_refst($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "select wkfw_id, st_id from jnl_wkfw_refst";
		}
		else
		{
			$sql = "select wkfw_id, st_id from jnl_wkfw_refst_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_id" => $row["wkfw_id"], "st_id" => $row["st_id"]);
		}
		return $arr;
	}

    // ワークフロー用アクセス権（職員）取得
	function get_wkfw_refemp($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "select wkfw_id, emp_id from jnl_wkfw_refemp";
		}
		else
		{
			$sql = "select wkfw_id, emp_id from jnl_wkfw_refemp_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_id" => $row["wkfw_id"], "emp_id" => $row["emp_id"]);
		}
		return $arr;
	}


	// 結果通知管理情報取得
	function get_wkfwnoticemng($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql  = "select * from jnl_wkfwnoticemng";
		}
		else
		{
			$sql  = "select * from jnl_wkfwnoticemng_real";
		}
		$cond = "where wkfw_id = $wkfw_id";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$notice_target_class_div = pg_fetch_result($sel, 0, "target_class_div");
        $rslt_ntc_div0_flg = pg_fetch_result($sel, 0, "rslt_ntc_div0_flg");
        $rslt_ntc_div1_flg = pg_fetch_result($sel, 0, "rslt_ntc_div1_flg");
        $rslt_ntc_div2_flg = pg_fetch_result($sel, 0, "rslt_ntc_div2_flg");
        $rslt_ntc_div3_flg = pg_fetch_result($sel, 0, "rslt_ntc_div3_flg");
        $rslt_ntc_div4_flg = pg_fetch_result($sel, 0, "rslt_ntc_div4_flg");

		return array(
                       "notice_target_class_div" => $notice_target_class_div,
                       "rslt_ntc_div0_flg" => $rslt_ntc_div0_flg,
                       "rslt_ntc_div1_flg" => $rslt_ntc_div1_flg,
                       "rslt_ntc_div2_flg" => $rslt_ntc_div2_flg,
                       "rslt_ntc_div3_flg" => $rslt_ntc_div3_flg,
                       "rslt_ntc_div4_flg" => $rslt_ntc_div4_flg
		              );
	}

	// 部署役職指定(結果通知)情報取得
	function get_wkfwnoticestdtl($wkfw_id, $st_div, $mode)
	{
		$sql   = "select a.*, b.st_nm ";
		if($mode == "ALIAS")
		{
			$sql  .= "from jnl_wkfwnoticestdtl a ";
		}
		else
		{
			$sql  .= "from jnl_wkfwnoticestdtl_real a ";
		}
		$sql  .= "inner join stmst b on a.st_id = b.st_id ";
		$cond  = "where a.wkfw_id = $wkfw_id and st_div = $st_div";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("st_id" => $row["st_id"], "st_nm" => $row["st_nm"]);

		}
		return $arr;
	}

	// 職員指定(結果通知)情報取得
	function get_wkfwnoticedtl($wkfw_id, $mode)
	{
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm ";
		if($mode == "ALIAS")
		{
			$sql .= "from jnl_wkfwnoticedtl a ";
		}
		else
		{
			$sql .= "from jnl_wkfwnoticedtl_real a ";
		}
		$sql .= "inner join empmst b on a.emp_id = b.emp_id ";
		$cond = "where a.wkfw_id = $wkfw_id ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("emp_id" => $row["emp_id"], "emp_lt_nm" => $row["emp_lt_nm"], "emp_ft_nm" => $row["emp_ft_nm"]);

		}
		return $arr;
	}

	// 委員会・ＷＧ指定(結果通知)情報取得
	function get_wkfwnoticepjtdtl($wkfw_id, $mode)
	{
        $sql  = "select pjt_id, pjt_name from project ";
		if($mode == "ALIAS")
		{
	        $sql .= "where pjt_id in (select parent_pjt_id from jnl_wkfwnoticepjtdtl where wkfw_id = $wkfw_id) ";
		}
		else
		{
	        $sql .= "where pjt_id in (select parent_pjt_id from jnl_wkfwnoticepjtdtl_real where wkfw_id = $wkfw_id) ";
		}
		$sql .= "union all ";

		$sql .= "select pjt_id, pjt_name from project ";

		if($mode == "ALIAS")
		{
			$sql .= "where pjt_id in (select child_pjt_id from jnl_wkfwnoticepjtdtl where wkfw_id = $wkfw_id)";
		}
		else
		{
			$sql .= "where pjt_id in (select child_pjt_id from jnl_wkfwnoticepjtdtl_real where wkfw_id = $wkfw_id)";
		}

		$cond = "";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		if(pg_numrows($sel) == 1)
		{
			$pjt_parent_id = pg_fetch_result($sel, 0, "pjt_id");
			$pjt_parent_nm = pg_fetch_result($sel, 0, "pjt_name");

			$arr[] = array("pjt_parent_id" => $pjt_parent_id, "pjt_parent_nm" => $pjt_parent_nm);

		}
		else if(pg_numrows($sel) == 2)
		{
			$pjt_parent_id = pg_fetch_result($sel, 0, "pjt_id");
			$pjt_parent_nm = pg_fetch_result($sel, 0, "pjt_name");
			$pjt_child_id = pg_fetch_result($sel, 1, "pjt_id");
			$pjt_child_nm = pg_fetch_result($sel, 1, "pjt_name");

			$arr[] = array("pjt_parent_id" => $pjt_parent_id, "pjt_parent_nm" => $pjt_parent_nm, "pjt_child_id" => $pjt_child_id, "pjt_child_nm" => $pjt_child_nm);
		}
		return $arr;
	}

	// 部署役職指定(結果通知)(部署指定)情報取得
	function get_wkfwnoticesectdtl($wkfw_id, $mode)
	{
		$sql  = "select a.*, b.class_nm, c.atrb_nm, d.dept_nm, e.room_nm ";
		if($mode == "ALIAS")
		{
			$sql .= "from jnl_wkfwnoticesectdtl a ";
		}
		else
		{
			$sql .= "from jnl_wkfwnoticesectdtl_real a ";
		}
		$sql .= "left join classmst b on a.class_id = b.class_id and not b.class_del_flg ";
		$sql .= "left join atrbmst c on a.atrb_id = c.atrb_id and not c.atrb_del_flg ";
		$sql .= "left join deptmst d on a.dept_id = d.dept_id and not d.dept_del_flg ";
		$sql .= "left join classroom e on a.room_id = e.room_id and not e.room_del_flg ";
		$cond = "where a.wkfw_id = $wkfw_id ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$class_id = pg_fetch_result($sel, 0, "class_id");
		$atrb_id = pg_fetch_result($sel, 0, "atrb_id");
		$dept_id = pg_fetch_result($sel, 0, "dept_id");
		$room_id = pg_fetch_result($sel, 0, "room_id");

		$class_nm = pg_fetch_result($sel, 0, "class_nm");
		$atrb_nm = pg_fetch_result($sel, 0, "atrb_nm");
		$dept_nm = pg_fetch_result($sel, 0, "dept_nm");
		$room_nm = pg_fetch_result($sel, 0, "room_nm");

		return array("class_id" => $class_id,
         			   "atrb_id" => $atrb_id,
				       "dept_id" => $dept_id,
				       "room_id" => $room_id,
         			   "class_nm" => $class_nm,
         			   "atrb_nm" => $atrb_nm,
				       "dept_nm" => $dept_nm,
				       "room_nm" => $room_nm);

	}


//-------------------------------------------------------------------------------------------------------------------------
// ワークフロー登録・更新・削除関連
//-------------------------------------------------------------------------------------------------------------------------

	// 承認者管理登録
	function regist_wkfwapvmng($wkfw_id, $apv_order, $approve_mng, $mode)
	{

		$deci_flg         = $approve_mng["deci_flg"];
		$target_class_div = $approve_mng["target_class_div"];
		$multi_apv_flg    = $approve_mng["multi_apv_flg"];
		$next_notice_div  = $approve_mng["next_notice_div"];
		$apv_div0_flg     = $approve_mng["apv_div0_flg"];
		$apv_div1_flg     = $approve_mng["apv_div1_flg"];
		$apv_div2_flg     = $approve_mng["apv_div2_flg"];
		$apv_div3_flg     = $approve_mng["apv_div3_flg"];
		$apv_div4_flg     = $approve_mng["apv_div4_flg"];
		$apv_num          = $approve_mng["apv_num"];

		if($mode == "ALIAS")
		{
			$sql = "insert into jnl_wkfwapvmng ";
		}
		else
		{
			$sql = "insert into jnl_wkfwapvmng_real ";
		}

		$sql .= "(wkfw_id, apv_order, deci_flg, target_class_div, multi_apv_flg, next_notice_div, apv_div0_flg, apv_div1_flg, apv_div2_flg, apv_div3_flg, apv_div4_flg, apv_num) values(";
		$content = array($wkfw_id, $apv_order, $deci_flg, $target_class_div, $multi_apv_flg, $next_notice_div, $apv_div0_flg, $apv_div1_flg, $apv_div2_flg, $apv_div3_flg, $apv_div4_flg, $apv_num);

		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定登録
	function regist_wkfwapvpstdtl($wkfw_id, $apv_order, $st_id, $st_div, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "insert into jnl_wkfwapvpstdtl ";
		}
		else
		{
			$sql = "insert into jnl_wkfwapvpstdtl_real ";
		}
		$sql .= "(wkfw_id, apv_order, st_id, st_div) values(";
		$content = array($wkfw_id, $apv_order, $st_id, $st_div);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定登録
	function regist_wkfwapvdtl($wkfw_id, $apv_order, $emp_id, $apv_sub_order, $mode)
	{

		if($mode == "ALIAS")
		{
			$sql = "insert into jnl_wkfwapvdtl ";
		}
		else
		{
			$sql = "insert into jnl_wkfwapvdtl_real ";
		}

		$sql .= "(wkfw_id, apv_order, emp_id, apv_sub_order) values(";
		$content = array($wkfw_id, $apv_order, $emp_id, $apv_sub_order);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ登録
	function regist_wkfwpjtdtl($wkfw_id, $apv_order, $pjt_parent_id, $pjt_child_id, $mode)
	{
		$pjt_child_id = ($pjt_child_id == "") ? null : $pjt_child_id;

		if($mode == "ALIAS")
		{
			$sql = "insert into jnl_wkfwpjtdtl ";
		}
		else
		{
			$sql = "insert into jnl_wkfwpjtdtl_real ";
		}

		$sql .= "(wkfw_id, apv_order, parent_pjt_id, child_pjt_id) values(";
		$content = array($wkfw_id, $apv_order, $pjt_parent_id, $pjt_child_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職(部署指定)
	function regist_wkfwapvsectdtl($wkfw_id, $apv_order, $class_id, $atrb_id, $dept_id, $room_id, $mode)
	{

		$atrb_id = ($atrb_id == "") ? null : $atrb_id;
		$dept_id = ($dept_id == "") ? null : $dept_id;
		$room_id = ($room_id == "") ? null : $room_id;

		if($mode == "ALIAS")
		{
			$sql = "insert into jnl_wkfwapvsectdtl ";
		}
		else
		{
			$sql = "insert into jnl_wkfwapvsectdtl_real ";
		}

		$sql .= "(wkfw_id, apv_order, class_id, atrb_id, dept_id, room_id) values(";
		$content = array($wkfw_id, $apv_order, $class_id, $atrb_id, $dept_id, $room_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 添付ファイル登録
	function regist_wkfwfile($wkfw_id, $wkfwfile_no, $wkfwfile_name, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "insert into jnl_wkfwfile ";
		}
		else
		{
			$sql = "insert into jnl_wkfwfile_real ";
		}

		$sql .= "(wkfw_id, wkfwfile_no, wkfwfile_name) values (";
		$content = array($wkfw_id, $wkfwfile_no, $wkfwfile_name);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 結果通知管理登録
	function regist_wkfwnoticemng($arr, $mode)
	{
		$wkfw_id           = $arr["wkfw_id"];
		$target_class_div  = $arr["target_class_div"];
		$rslt_ntc_div0_flg = $arr["rslt_ntc_div0_flg"];
		$rslt_ntc_div1_flg = $arr["rslt_ntc_div1_flg"];
		$rslt_ntc_div2_flg = $arr["rslt_ntc_div2_flg"];
		$rslt_ntc_div3_flg = $arr["rslt_ntc_div3_flg"];
		$rslt_ntc_div4_flg = $arr["rslt_ntc_div4_flg"];

		if($mode == "ALIAS")
		{
			$sql = "insert into jnl_wkfwnoticemng ";
		}
		else
		{
			$sql = "insert into jnl_wkfwnoticemng_real ";
		}
		$sql .= "(wkfw_id, target_class_div, rslt_ntc_div0_flg, rslt_ntc_div1_flg, rslt_ntc_div2_flg, rslt_ntc_div3_flg, rslt_ntc_div4_flg) values(";
		$content = array($wkfw_id, $target_class_div, $rslt_ntc_div0_flg, $rslt_ntc_div1_flg, $rslt_ntc_div2_flg, $rslt_ntc_div3_flg, $rslt_ntc_div4_flg);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)登録
	function regist_wkfwnoticestdtl($wkfw_id, $st_id, $st_div, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "insert into jnl_wkfwnoticestdtl ";
		}
		else
		{
			$sql = "insert into jnl_wkfwnoticestdtl_real ";
		}
		$sql .= "(wkfw_id, st_id, st_div) values(";
		$content = array($wkfw_id, $st_id, $st_div);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定(結果通知)登録
	function regist_wkfwnoticedtl($wkfw_id, $emp_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "insert into jnl_wkfwnoticedtl ";
		}
		else
		{
			$sql = "insert into jnl_wkfwnoticedtl_real ";
		}
		$sql .= "(wkfw_id, emp_id) values(";
		$content = array($wkfw_id, $emp_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ指定(結果通知)登録
	function regist_wkfwnoticepjtdtl($wkfw_id, $pjt_parent_id, $pjt_child_id, $mode)
	{
		$pjt_child_id = ($pjt_child_id == "") ? null : $pjt_child_id;

		if($mode == "ALIAS")
		{
			$sql = "insert into jnl_wkfwnoticepjtdtl ";
		}
		else
		{
			$sql = "insert into jnl_wkfwnoticepjtdtl_real ";
		}
		$sql .= "(wkfw_id, parent_pjt_id, child_pjt_id) values(";
		$content = array($wkfw_id, $pjt_parent_id, $pjt_child_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)(部署指定)登録
	function regist_wkfwnoticesectdtl($wkfw_id, $class_id, $atrb_id, $dept_id, $room_id, $mode)
	{
		$atrb_id = ($atrb_id == "") ? null : $atrb_id;
		$dept_id = ($dept_id == "") ? null : $dept_id;
		$room_id = ($room_id == "") ? null : $room_id;

		if($mode == "ALIAS")
		{
			$sql = "insert into jnl_wkfwnoticesectdtl ";
		}
		else
		{
			$sql = "insert into jnl_wkfwnoticesectdtl_real ";
		}
		$sql .= "(wkfw_id, class_id, atrb_id, dept_id, room_id) values(";
		$content = array($wkfw_id, $class_id, $atrb_id, $dept_id, $room_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 前提とする申請書(ワークフロー用)登録
	function regist_wkfwfprecond($wkfw_id, $precond_order, $precond_wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "insert into jnl_wkfwfprecond ";
		}
		else
		{
			$sql = "insert into jnl_wkfwfprecond_real ";
		}

		$sql .= "(wkfw_id, precond_order, precond_wkfw_id) values(";
		$content = array($wkfw_id, $precond_order, $precond_wkfw_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}



	// 承認者管理削除
	function delete_wkfwapvmng($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from jnl_wkfwapvmng";
		}
		else
		{
			$sql = "delete from jnl_wkfwapvmng_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職削除
	function delete_wkfwapvpstdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from jnl_wkfwapvpstdtl";
		}
		else
		{
			$sql = "delete from jnl_wkfwapvpstdtl_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定削除
	function delete_wkfwapvdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from jnl_wkfwapvdtl";
		}
		else
		{
			$sql = "delete from jnl_wkfwapvdtl_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ削除
	function delete_wkfwpjtdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from jnl_wkfwpjtdtl";
		}
		else
		{
			$sql = "delete from jnl_wkfwpjtdtl_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職(部署指定)
	function delete_wkfwapvsectdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from jnl_wkfwapvsectdtl";
		}
		else
		{
			$sql = "delete from jnl_wkfwapvsectdtl_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 結果通知管理削除
	function delete_wkfwnoticemng($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from jnl_wkfwnoticemng";
		}
		else
		{
			$sql = "delete from jnl_wkfwnoticemng_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)削除
	function delete_wkfwnoticestdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from jnl_wkfwnoticestdtl";
		}
		else
		{
			$sql = "delete from jnl_wkfwnoticestdtl_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定(結果通知)削除
	function delete_wkfwnoticedtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from jnl_wkfwnoticedtl";
		}
		else
		{
			$sql = "delete from jnl_wkfwnoticedtl_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ指定(結果通知)削除
	function delete_wkfwnoticepjtdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from jnl_wkfwnoticepjtdtl";
		}
		else
		{
			$sql = "delete from jnl_wkfwnoticepjtdtl_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)(部署指定)削除
	function delete_wkfwnoticesectdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from jnl_wkfwnoticesectdtl";
		}
		else
		{
			$sql = "delete from jnl_wkfwnoticesectdtl_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 添付ファイル削除
	function delete_wkfwfile($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from jnl_wkfwfile";
		}
		else
		{
			$sql = "delete from jnl_wkfwfile_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(ワークフロー用)
	function delete_wkfwfprecond($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from jnl_wkfwfprecond";
		}
		else
		{
			$sql = "delete from jnl_wkfwfprecond_real";
		}
		$cond = "where wkfw_id = $wkfw_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}



	// ワークフロー登録
	function regist_wkfwmst($arr, $mode)
	{
		$wkfw_id = $arr["wkfw_id"];
		$wkfw_type = $arr["wkfw_type"];
		$wkfw_title = $arr["wkfw_title"];
		$wkfw_content = $arr["wkfw_content"];
		$start_date = $arr["start_date"];
		$end_date = $arr["end_date"];
		$wkfw_appr = $arr["wkfw_appr"];
		$wkfw_content_type = $arr["wkfw_content_type"];
		$wkfw_folder_id = $arr["wkfw_folder_id"];
		$ref_dept_st_flg = $arr["ref_dept_st_flg"];
		$ref_dept_flg = $arr["ref_dept_flg"];
		$ref_st_flg = $arr["ref_st_flg"];
		$short_wkfw_name = $arr["short_wkfw_name"];
		$apply_title_disp_flg = $arr["apply_title_disp_flg"];
		$send_mail_flg = $arr["send_mail_flg"];
		$lib_reg_flg = $arr["lib_reg_flg"];
		$lib_keyword = $arr["lib_keyword"];
		$lib_no = $arr["lib_no"];
		$lib_summary = $arr["lib_summary"];
		$lib_archive = $arr["lib_archive"];
		$lib_cate_id = $arr["lib_cate_id"];
		$lib_folder_id = $arr["lib_folder_id"];
		$lib_show_login_flg = $arr["lib_show_login_flg"];
		$lib_show_login_begin = $arr["lib_show_login_begin"];
		$lib_show_login_end = $arr["lib_show_login_end"];
		$lib_private_flg = $arr["lib_private_flg"];
		$lib_ref_dept_st_flg = $arr["lib_ref_dept_st_flg"];
		$lib_ref_dept_flg = $arr["lib_ref_dept_flg"];
		$lib_ref_st_flg = $arr["lib_ref_st_flg"];
		$lib_upd_dept_st_flg = $arr["lib_upd_dept_st_flg"];
		$lib_upd_dept_flg = $arr["lib_upd_dept_flg"];
		$lib_upd_st_flg = $arr["lib_upd_st_flg"];
		$approve_label = $arr["approve_label"];

		if($mode == "ALIAS")
		{
			$sql = "insert into jnl_wkfwmst ";
		}
		else
		{
			$sql = "insert into jnl_wkfwmst_real ";
		}

		$sql .= "(wkfw_id, wkfw_type, wkfw_title, wkfw_content, wkfw_start_date, wkfw_end_date, wkfw_appr, wkfw_content_type, wkfw_folder_id, ref_dept_st_flg, ref_dept_flg, ref_st_flg, short_wkfw_name, apply_title_disp_flg, send_mail_flg, lib_reg_flg, lib_keyword, lib_no, lib_summary, lib_archive, lib_cate_id, lib_folder_id, lib_show_login_flg, lib_show_login_begin, lib_show_login_end, lib_private_flg, lib_ref_dept_st_flg, lib_ref_dept_flg, lib_ref_st_flg, lib_upd_dept_st_flg, lib_upd_dept_flg, lib_upd_st_flg, approve_label) values (";
		$content = array($wkfw_id, $wkfw_type, pg_escape_string($wkfw_title), pg_escape_string($wkfw_content), $start_date, $end_date, $wkfw_appr, $wkfw_content_type, $wkfw_folder_id, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, pg_escape_string($short_wkfw_name), $apply_title_disp_flg, $send_mail_flg, $lib_reg_flg, $lib_keyword, $lib_no, $lib_summary, $lib_archive, $lib_cate_id, $lib_folder_id, $lib_show_login_flg, $lib_show_login_begin, $lib_show_login_end, $lib_private_flg, $lib_ref_dept_st_flg, $lib_ref_dept_flg, $lib_ref_st_flg, $lib_upd_dept_st_flg, $lib_upd_dept_flg, $lib_upd_st_flg, $approve_label);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー更新
	function update_wkfwmst($arr, $mode)
	{
		$wkfw_id = $arr["wkfw_id"];
		$wkfw_type = $arr["wkfw_type"];
		$wkfw_title = $arr["wkfw_title"];
		$wkfw_content = $arr["wkfw_content"];
		$start_date = $arr["start_date"];
		$end_date = $arr["end_date"];
		$wkfw_appr = $arr["wkfw_appr"];
		$wkfw_content_type = $arr["wkfw_content_type"];
		$wkfw_folder_id = $arr["wkfw_folder_id"];
		$ref_dept_st_flg = $arr["ref_dept_st_flg"];
		$ref_dept_flg = $arr["ref_dept_flg"];
		$ref_st_flg = $arr["ref_st_flg"];
		$short_wkfw_name = $arr["short_wkfw_name"];
		$apply_title_disp_flg = $arr["apply_title_disp_flg"];
		$send_mail_flg = $arr["send_mail_flg"];
		$lib_reg_flg = $arr["lib_reg_flg"];
		$lib_keyword = $arr["lib_keyword"];
		$lib_no = $arr["lib_no"];
		$lib_summary = $arr["lib_summary"];
		$lib_archive = $arr["lib_archive"];
		$lib_cate_id = $arr["lib_cate_id"];
		$lib_folder_id = $arr["lib_folder_id"];
		$lib_show_login_flg = $arr["lib_show_login_flg"];
		$lib_show_login_begin = $arr["lib_show_login_begin"];
		$lib_show_login_end = $arr["lib_show_login_end"];
		$lib_private_flg = $arr["lib_private_flg"];
		$lib_ref_dept_st_flg = $arr["lib_ref_dept_st_flg"];
		$lib_ref_dept_flg = $arr["lib_ref_dept_flg"];
		$lib_ref_st_flg = $arr["lib_ref_st_flg"];
		$lib_upd_dept_st_flg = $arr["lib_upd_dept_st_flg"];
		$lib_upd_dept_flg = $arr["lib_upd_dept_flg"];
		$lib_upd_st_flg = $arr["lib_upd_st_flg"];
		$approve_label = $arr["approve_label"];

		if($mode == "ALIAS")
		{
			$sql = "update jnl_wkfwmst set";
		}
		else
		{
			$sql = "update jnl_wkfwmst_real set";
		}
		$set = array("wkfw_type", "wkfw_title", "wkfw_content", "wkfw_start_date", "wkfw_end_date", "wkfw_appr", "wkfw_content_type", "wkfw_folder_id", "ref_dept_st_flg", "ref_dept_flg", "ref_st_flg", "short_wkfw_name", "apply_title_disp_flg", "send_mail_flg", "lib_reg_flg", "lib_keyword", "lib_no", "lib_summary", "lib_archive", "lib_cate_id", "lib_folder_id", "lib_show_login_flg", "lib_show_login_begin", "lib_show_login_end", "lib_private_flg", "lib_ref_dept_st_flg", "lib_ref_dept_flg", "lib_ref_st_flg", "lib_upd_dept_st_flg", "lib_upd_dept_flg", "lib_upd_st_flg", "approve_label");
		$setvalue = array($wkfw_type, pg_escape_string($wkfw_title), pg_escape_string($wkfw_content), $start_date, $end_date, $wkfw_appr, $wkfw_content_type, $wkfw_folder_id, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, pg_escape_string($short_wkfw_name), $apply_title_disp_flg, $send_mail_flg, $lib_reg_flg, $lib_keyword, $lib_no, $lib_summary, $lib_archive, $lib_cate_id, $lib_folder_id, $lib_show_login_flg, $lib_show_login_begin, $lib_show_login_end, $lib_private_flg, $lib_ref_dept_st_flg, $lib_ref_dept_flg, $lib_ref_st_flg, $lib_upd_dept_st_flg, $lib_upd_dept_flg, $lib_upd_st_flg, $approve_label);
		$cond = "where wkfw_id = $wkfw_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 略称取得
	function get_short_wkfw_name_used_cnt($short_wkfw_name, $wkfw_id)
	{
		$sql  .= "select count(*) as cnt from jnl_wkfwmst_real";
		$cond .= "where short_wkfw_name = '$short_wkfw_name'";

		if($wkfw_id != "")
		{
			$cond .= "and wkfw_id <> $wkfw_id";
		}

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}
//-------------------------------------------------------------------------------------------------------------------------
// 申請関連
//-------------------------------------------------------------------------------------------------------------------------

	// 承認者管理取得
	function get_wkfwapvmng($wkfw_id)
	{
		$sql  = "select wkfw_id, apv_order, deci_flg, target_class_div, multi_apv_flg, next_notice_div, apv_div0_flg, apv_div1_flg, apv_div2_flg, apv_div3_flg, apv_div4_flg, apv_num ";
		$sql .= "from jnl_wkfwapvmng ";
		$cond = "where wkfw_id = $wkfw_id order by apv_order";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_id" => $row["wkfw_id"],
                            "apv_order" => $row["apv_order"],
                            "deci_flg" => $row["deci_flg"],
                            "target_class_div" => $row["target_class_div"],
                            "multi_apv_flg" => $row["multi_apv_flg"],
                            "next_notice_div" => $row["next_notice_div"],
							"apv_div0_flg" => $row["apv_div0_flg"],
							"apv_div1_flg" => $row["apv_div1_flg"],
							"apv_div2_flg" => $row["apv_div2_flg"],
							"apv_div3_flg" => $row["apv_div3_flg"],
							"apv_div4_flg" => $row["apv_div4_flg"],
							"apv_num" => $row["apv_num"]
                           );
		}
		return $arr;
	}

	// 部署役職指定情報取得
	function get_wkfwapvpstdtl($wkfw_id, $apv_order, $target_class_div, $emp_id)
	{
		$arr = array();

		// 申請者の部署役職取得
		$arr_emp_info = $this->get_emp_info($emp_id);

		if($target_class_div == "4")
		{
			$fourth_post_exist_flg = false;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_room  = $emp_info["emp_room"];
				if($emp_room != "")
				{
					$fourth_post_exist_flg = true;
					break;
				}
			}

			if(!$fourth_post_exist_flg)
			{
				return $arr;
			}
		}

		$sql  = "select ";
		$sql .= "emp.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_class, empmst.emp_attribute, empmst.emp_dept, empmst.emp_room, empmst.emp_st, stmst.st_nm ";
		$sql .= "from ";
    	$sql .= "(";
    	$sql .= "select emp_id from empmst where ";

		// 「部署指定しない」以外
		if($target_class_div != 0)
		{
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_class     = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept      = $emp_info["emp_dept"];
				$emp_room      = $emp_info["emp_room"];

				if($target_class_div == "4")
				{
					if($emp_room == "")
					{
						continue;
					}
				}

				if($idx > 0)
				{
					$sql .= "or ";
				}

				if($target_class_div == "1")
				{
					$sql .= "emp_class = $emp_class ";
				}
				else if($target_class_div == "2")
				{
					$sql .= "emp_attribute = $emp_attribute ";
				}
				else if($target_class_div == "3")
				{
					$sql .= "emp_dept = $emp_dept ";
				}
				else if($target_class_div == "4")
				{
					$sql .= "emp_room = $emp_room ";
				}
				$idx++;
			}
			$sql .= ") ";
			$sql .= "and ";
		}
		$sql .= "exists (select st_id from jnl_wkfwapvpstdtl where empmst.emp_st = jnl_wkfwapvpstdtl.st_id and wkfw_id = $wkfw_id and apv_order = $apv_order and st_div = 0) ";
		$sql .= "union ";
		$sql .= "select emp_id from concurrent where ";

		// 「部署指定しない」以外
		if($target_class_div != 0)
		{
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_class     = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept      = $emp_info["emp_dept"];
				$emp_room      = $emp_info["emp_room"];

				if($target_class_div == "4")
				{
					if($emp_room == "")
					{
						continue;
					}
				}

				if($idx > 0)
				{
					$sql .= "or ";
				}

				if($target_class_div == "1")
				{
					$sql .= "emp_class = $emp_class ";
				}
				else if($target_class_div == "2")
				{
					$sql .= "emp_attribute = $emp_attribute ";
				}
				else if($target_class_div == "3")
				{
					$sql .= "emp_dept = $emp_dept ";
				}
				else if($target_class_div == "4")
				{
					$sql .= "emp_room = $emp_room ";
				}
				$idx++;
			}
			$sql .= ") ";
			$sql .= "and ";
		}
		$sql .= "exists (select st_id from jnl_wkfwapvpstdtl where concurrent.emp_st = jnl_wkfwapvpstdtl.st_id and wkfw_id = $wkfw_id and apv_order = $apv_order and st_div = 0) ";
		$sql .= ") emp ";
		$sql .= "inner join empmst on emp.emp_id = empmst.emp_id ";
		$sql .= "inner join authmst on emp.emp_id = authmst.emp_id and not authmst.emp_del_flg ";
		$sql .= "inner join stmst on empmst.emp_st = stmst.st_id and not stmst.st_del_flg ";
		$cond = "order by emp.emp_id asc ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$idx = 1;
		while($row = pg_fetch_array($sel))
		{
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
			                "emp_full_nm" => $emp_full_nm,
			                "st_nm" => $row["st_nm"],
   			                "apv_sub_order" => $idx
			               );
			$idx++;
		}
		return $arr;
	}

	// 職員指定情報取得
	function get_wkfwapvdtl($wkfw_id, $apv_order)
	{
		$sql   = "select a.emp_id, a.apv_sub_order, b.emp_lt_nm, b.emp_ft_nm, d.st_nm from jnl_wkfwapvdtl a ";
		$sql  .= "inner join empmst b on a.emp_id = b.emp_id ";
		$sql  .= "inner join authmst c on a.emp_id = c.emp_id ";
		$sql  .= "and c.emp_del_flg = 'f' ";
		$sql  .= "inner join stmst d on b.emp_st = d.st_id ";
		$cond  = "where a.wkfw_id = $wkfw_id and a.apv_order = $apv_order ";
		$cond .= "order by apv_sub_order asc";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
			                "emp_full_nm" => $emp_full_nm,
			                "st_nm" => $row["st_nm"],
			                "apv_sub_order" => $row["apv_sub_order"]
			               );
		}
		return $arr;
	}

	// 委員会・ＷＧ指定情報取得
	function get_wkfwpjtdtl($wkfw_id, $apv_order)
	{
		$sql  = "select parent_pjt_id, child_pjt_id from jnl_wkfwpjtdtl ";
		$cond = "where wkfw_id = $wkfw_id and apv_order = $apv_order";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$parent_pjt_id = pg_fetch_result($sel, 0, "parent_pjt_id");
		$child_pjt_id  = pg_fetch_result($sel, 0, "child_pjt_id");

		return array("parent_pjt_id" => $parent_pjt_id, "child_pjt_id" => $child_pjt_id);

	}



	// 委員会・ＷＧメンバー取得
	function get_project_member($parent_pjt_id, $child_pjt_id)
	{
		$sql  = "select emp.project_no, emp.pjt_response as emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, stmst.st_nm ";
		$sql .= "from ";
		$sql .= "( ";

		$sql .= "select varchar(1) '1' as project_no, project.pjt_id, project.pjt_response ";
		$sql .= "from project ";
		$sql .= "where project.pjt_public_flag and not project.pjt_delete_flag and project.pjt_parent_id is null and project.pjt_id = $parent_pjt_id ";

	 	if($child_pjt_id != "")
		{
			$sql .= "union all ";
			$sql .= "select varchar(1) '3' as project_no, project.pjt_id, project.pjt_response ";
			$sql .= "from project ";
			$sql .= "where project.pjt_public_flag and not project.pjt_delete_flag and project.pjt_parent_id = $parent_pjt_id and project.pjt_id = $child_pjt_id ";
		}

		$sql .= ") emp ";


		$sql .= "inner join empmst on emp.pjt_response = empmst.emp_id ";
		$sql .= "inner join authmst on emp.pjt_response = authmst.emp_id and not emp_del_flg ";
		$sql .= "inner join stmst on empmst.emp_st = stmst.st_id and not stmst.st_del_flg ";

		$sql .= "union all ";

		$sql .= "select emp.project_no, emp.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, stmst.st_nm ";
		$sql .= "from ";
		$sql .= "( ";

		$sql .= "select varchar(1) '2' as project_no, project.pjt_id, promember.emp_id, promember.pjt_member_id ";
		$sql .= "from project ";
		$sql .= "inner join promember on project.pjt_id = promember.pjt_id ";
		$sql .= "where project.pjt_public_flag and not project.pjt_delete_flag and project.pjt_parent_id is null and project.pjt_id = $parent_pjt_id ";

	 	if($child_pjt_id != "")
		{
			$sql .= "union all ";
			$sql .= "select varchar(1) '4' as project_no, project.pjt_id, promember.emp_id, promember.pjt_member_id ";
			$sql .= "from project ";
			$sql .= "inner join promember on project.pjt_id = promember.pjt_id ";
			$sql .= "where project.pjt_public_flag and not project.pjt_delete_flag and project.pjt_parent_id = $parent_pjt_id and project.pjt_id = $child_pjt_id ";
		}

		$sql .= ") emp ";

		$sql .= "inner join empmst on emp.emp_id = empmst.emp_id ";
		$sql .= "inner join authmst on emp.emp_id = authmst.emp_id and not emp_del_flg ";
		$sql .= "inner join stmst on empmst.emp_st = stmst.st_id and not stmst.st_del_flg ";
		$cond .= "order by project_no ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		$idx = 1;
		while($row = pg_fetch_array($sel))
		{
			$emp_id      = $row["emp_id"];
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$st_nm       = $row["st_nm"];

			$dpl_flg = false;

			// 重複があればセットしない。
			foreach($arr as $arr_apv)
			{
				if($emp_id == $arr_apv["emp_id"])
				{
					$dpl_flg = true;
				}
			}

			if(!$dpl_flg)
			{
				$arr[] = array("emp_id" => $row["emp_id"],
				                "emp_full_nm" => $emp_full_nm,
				                "st_nm" => $row["st_nm"],
				                "apv_sub_order" => $idx
				               );
				$idx++;
			}
		}
		return $arr;
	}


	// 申請者以外の結果通知者取得
	function get_wkfw_notice_for_apply($wkfw_id,
	                                    $notice_target_class_div,
                                        $rslt_ntc_div0_flg,
                                        $rslt_ntc_div1_flg,
                                        $rslt_ntc_div2_flg,
                                        $rslt_ntc_div3_flg,
                                        $rslt_ntc_div4_flg,
	                                    $emp_id)
	{
		$arr_tmp = array();

		// 部署役職指定(申請書所属)
		if($rslt_ntc_div0_flg == "t")
		{
			$arr_wkfwnoticestdtl = $this->get_wkfwnoticestdtl($wkfw_id, "0", "ALIAS");
			$notice_st_id = "";
			foreach($arr_wkfwnoticestdtl as $wkfwnoticestdtl)
			{
				if($notice_st_id != "")
				{
					$notice_st_id .= ",";
				}
				$notice_st_id .= $wkfwnoticestdtl["st_id"];
			}
			$arr_apvpstdtl = $this->get_apvpstdtl_for_wkfwpreview($notice_target_class_div, $notice_st_id, $emp_id);

			for($i=0; $i<count($arr_apvpstdtl); $i++)
			{
				$arr_apvpstdtl[$i]["rslt_ntc_div"] = "0";
			}

			// マージ処理
			$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_apvpstdtl);
		}

		// 部署役職指定(部署指定)
		if($rslt_ntc_div4_flg == "t")
		{
			$arrr_wkfwnoticesectdtl = $this->get_wkfwnoticesectdtl($wkfw_id, "ALIAS");
			$arr_wkfwnoticestdtl = $this->get_wkfwnoticestdtl($wkfw_id, "4", "ALIAS");
			$notice_st_id = "";
			foreach($arr_wkfwnoticestdtl as $wkfwnoticestdtl)
			{
				if($notice_st_id != "")
				{
					$notice_st_id .= ",";
				}
				$notice_st_id .= $wkfwnoticestdtl["st_id"];
			}
			$arr_post_sect = $this->get_emp_info_for_post_sect($arrr_wkfwnoticesectdtl["class_id"],
			                                        $arrr_wkfwnoticesectdtl["atrb_id"],
			                                        $arrr_wkfwnoticesectdtl["dept_id"],
			                                        $arrr_wkfwnoticesectdtl["room_id"],
			                                        $notice_st_id);

			for($i=0; $i<count($arr_post_sect); $i++)
			{
				$arr_post_sect[$i]["rslt_ntc_div"] = "4";
			}

			// マージ処理
			$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_post_sect);
		}

		// 職員指定
		if($rslt_ntc_div1_flg == "t")
		{
			$arr_emp_info = array();
			$arr_wkfwnoticedtl = $this->get_wkfwnoticedtl($wkfw_id, "ALIAS");
			foreach($arr_wkfwnoticedtl as $wkfwnoticedtl)
			{
				$arr_empmst_detail = $this->get_empmst_detail($wkfwnoticedtl["emp_id"]);
				$emp_lt_nm = $arr_empmst_detail[0]["emp_lt_nm"];
				$emp_ft_nm = $arr_empmst_detail[0]["emp_ft_nm"];
                $emp_full_nm = $emp_lt_nm." ".$emp_ft_nm;
				$arr_emp_info[] = array(
								 "emp_id" => $arr_empmst_detail[0]["emp_id"],
								 "emp_full_nm" => $emp_full_nm,
								 "st_nm" => $arr_empmst_detail[0]["st_nm"]
								 );
			}

			for($i=0; $i<count($arr_emp_info); $i++)
			{
				$arr_emp_info[$i]["rslt_ntc_div"] = "1";
			}

			// マージ処理
			$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_emp_info);
		}

		// 委員会・ＷＧ指定
		if($rslt_ntc_div3_flg == "t")
		{
			$arr_wkfwnoticepjtdtl = $this->get_wkfwnoticepjtdtl($wkfw_id, "ALIAS");
			$arr_project_member = $this->get_project_member($arr_wkfwnoticepjtdtl[0]["pjt_parent_id"], $arr_wkfwnoticepjtdtl[0]["pjt_child_id"]);

			for($i=0; $i<count($arr_project_member); $i++)
			{
				$arr_project_member[$i]["rslt_ntc_div"] = "3";
			}

			// マージ処理
			$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_project_member);
		}

		// 申請者を除去する
		$arr = array();
		foreach($arr_tmp as $tmp)
		{
			$tmp_emp_id = $tmp["emp_id"];
			if($tmp_emp_id == $emp_id)
			{
				continue;
			}
			$arr[] = $tmp;
		}

		return $arr;
	}



	// 承認者情報取得
	function get_wkfwapv_info($wkfw_id, $emp_id)
	{
		$arr_wkfwapvmng = $this->get_wkfwapvmng($wkfw_id);

		$arr = array();
		foreach($arr_wkfwapvmng as $apvmng)
		{
			$apvmng["apv_setting_flg"] = "f";

			$arr_apv = array();
			$multi_apv_flg = $apvmng["multi_apv_flg"];

			$apv_div0_flg  = $apvmng["apv_div0_flg"];
			$apv_div1_flg  = $apvmng["apv_div1_flg"];
			$apv_div2_flg  = $apvmng["apv_div2_flg"];
			$apv_div3_flg  = $apvmng["apv_div3_flg"];
			$apv_div4_flg  = $apvmng["apv_div4_flg"];

			if($apv_div0_flg == "f" && $apv_div1_flg == "f" && $apv_div2_flg == "t" && $apv_div3_flg == "f" && $apv_div4_flg == "f")
			{
				$apvmng["apv_setting_flg"] = "t";
			}

			// 部署役職(申請者所属)指定
			if($apv_div0_flg == "t")
			{
				$arr_wkfwapvpstdtl = $this->get_wkfwapvpstdtl($wkfw_id, $apvmng["apv_order"], $apvmng["target_class_div"], $emp_id);

				for($i=0; $i<count($arr_wkfwapvpstdtl); $i++)
				{
					$arr_wkfwapvpstdtl[$i]["apv_div"] = "0";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_wkfwapvpstdtl);

			}
			// 部署役職(部署指定)指定
			if($apv_div4_flg == "t")
			{

  				$arr_apvpstdtl = $this->get_apvpstdtl($wkfw_id, $apvmng["apv_order"], "4");
				$st_sect_ids = "";
				foreach($arr_apvpstdtl as $apvpstdtl)
				{
					if($st_sect_ids != "")
					{
						$st_sect_ids .= ",";
					}
					$st_sect_ids .= $apvpstdtl["st_id"];
				}
				$arr_sect_dtl = $this->get_apvsectdtl($wkfw_id, $apvmng["apv_order"]);
				$arr_post_sect = $this->get_emp_info_for_post_sect($arr_sect_dtl["class_id"], $arr_sect_dtl["atrb_id"], $arr_sect_dtl["dept_id"], $arr_sect_dtl["room_id"], $st_sect_ids);

				for($i=0; $i<count($arr_post_sect); $i++)
				{
					$arr_post_sect[$i]["apv_div"] = "4";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_post_sect);
			}
			// 職員指定
			if($apv_div1_flg == "t")
			{
				$arr_wkfwapvdtl = $this->get_wkfwapvdtl($wkfw_id, $apvmng["apv_order"]);
				for($i=0; $i<count($arr_wkfwapvdtl); $i++)
				{
					$arr_wkfwapvdtl[$i]["apv_div"] = "1";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_wkfwapvdtl);
			}
			// 委員会・ＷＧ
			if($apv_div3_flg == "t")
			{
				$arr_pjt = $this->get_wkfwpjtdtl($wkfw_id, $apvmng["apv_order"]);
				$arr_project_member = $this->get_project_member($arr_pjt["parent_pjt_id"], $arr_pjt["child_pjt_id"]);

				$pjt_nm = $this->get_pjt_nm($arr_pjt["parent_pjt_id"]);
				if($arr_pjt["child_pjt_id"] != "")
				{
					$pjt_nm .= " > ";
					$pjt_nm .= $this->get_pjt_nm($arr_pjt["child_pjt_id"]);
				}

				for($i=0; $i<count($arr_project_member); $i++)
				{
					$arr_project_member[$i]["apv_div"] = "3";
					$arr_project_member[$i]["pjt_nm"] = $pjt_nm;
					$arr_project_member[$i]["parent_pjt_id"] = $arr_pjt["parent_pjt_id"];
					$arr_project_member[$i]["child_pjt_id"]  = $arr_pjt["child_pjt_id"];
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_project_member);
			}
			// その他
			if($apv_div2_flg == "t")
			{
				$arr_setting_apv = "";
				for($i=0; $i<$apvmng["apv_num"]; $i++)
				{
					$arr_setting_apv[] = array();
				}

				for($i=0; $i<count($arr_setting_apv); $i++)
				{
					$arr_setting_apv[$i]["apv_div"] = "2";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_setting_apv);

			}

			// 複数承認者「許可する」
			if($multi_apv_flg == "t")
			{
				if(count($arr_apv) > 0)
				{
					if(count($arr_apv) > 1)
					{
						$apv_sub_order = 1;
						foreach($arr_apv as $apv)
						{
							$arr_emp = array();
							$apvmng["apv_sub_order"] = $apv_sub_order;
							$arr_emp[] = array(
                                               "emp_id" => $apv["emp_id"],
                                               "emp_full_nm" => $apv["emp_full_nm"],
                                               "st_nm" => $apv["st_nm"],
                                               "apv_div" => $apv["apv_div"],
                                               "pjt_nm" => $apv["pjt_nm"],
                                               "parent_pjt_id" => $apv["parent_pjt_id"],
                                               "child_pjt_id" => $apv["child_pjt_id"]
                                               );
							$apvmng["emp_infos"] = $arr_emp;

							if($apv["apv_div"] == "2")
							{
								$apvmng["apv_setting_flg"] = "t";
							}

							$arr[] = $apvmng;
							$apv_sub_order++;
						}
					}
					else
					{
						$arr_emp = array();
						$apvmng["apv_sub_order"] = "";
						$arr_emp[] = array(
                                           "emp_id" => $arr_apv[0]["emp_id"],
                                           "emp_full_nm" => $arr_apv[0]["emp_full_nm"],
                                           "st_nm" => $arr_apv[0]["st_nm"],
                                           "apv_div" => $arr_apv[0]["apv_div"],
                                           "pjt_nm" => $arr_apv[0]["pjt_nm"],
                                           "parent_pjt_id" => $arr_apv[0]["parent_pjt_id"],
                                           "child_pjt_id" => $arr_apv[0]["child_pjt_id"]
                                           );
						$apvmng["emp_infos"] = $arr_emp;

						if($apv["apv_div"] == "2")
						{
							$apvmng["apv_setting_flg"] = "t";
						}

						$arr[] = $apvmng;
					}
				}
				else
				{
					$apvmng["emp_infos"] = array();
					$arr[] = $apvmng;
				}
			}
			// 複数承認者「許可しない」
			else
			{
				$apvmng["apv_sub_order"] = "";
				$apvmng["emp_infos"] = $arr_apv;
				$arr[] = $apvmng;
			}
		}

		return $arr;
	}

	// ワークフロー情報取得
	function get_wkfwmst($wkfw_id)
	{
		$sql  = "select * from jnl_wkfwmst";
		$cond ="where wkfw_id = $wkfw_id";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}

	// フォーマットファイル情報取得
	function get_wkfwfile($wkfw_id)
	{
		$sql  = "select wkfwfile_no, wkfwfile_name from jnl_wkfwfile";
		$cond = "where wkfw_id = $wkfw_id order by wkfwfile_no";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfwfile_no" => $row["wkfwfile_no"], "wkfwfile_name" => $row["wkfwfile_name"]);
		}
		return $arr;
	}

	// 前提とする申請書情報取得
	function get_wkfwfprecond($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql  = "select a.precond_wkfw_id, b.wkfw_title from jnl_wkfwfprecond a ";
		}
		else
		{
			$sql  = "select a.precond_wkfw_id, b.wkfw_title from jnl_wkfwfprecond_real a ";
		}

		$sql .= "inner join jnl_wkfwmst b on a.precond_wkfw_id = b.wkfw_id ";
		$cond = "where a.wkfw_id = $wkfw_id order by a.precond_order asc ";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}

	// 承認確定申請情報取得
	function get_approved_apply_list($session, $wkfw_id, $page, $max_page)
	{
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$page = ($page - 1) * 15;

		$sql   = "select ";
		$sql  .= "a.*, ";
		$sql  .= "b.wkfw_title, b.short_wkfw_name ";
		$sql  .= "from jnl_apply a ";
		$sql  .= "inner join jnl_wkfwmst b on a.wkfw_id = b.wkfw_id ";
		$cond .= "where not a.delete_flg and a.apply_stat = '1' and a.emp_id = '$emp_id' and b.wkfw_id = $wkfw_id ";
		$cond .= "order by a.apply_date desc, a.apply_no desc ";
		$cond .= "offset $page limit $max_page ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}

	// 承認確定申請件数情報取得
	function get_approved_apply_list_count($session, $wkfw_id)
	{
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$sql   = "select ";
		$sql  .= "count(a.apply_id) as cnt ";
		$sql  .= "from jnl_apply a ";
		$sql  .= "inner join jnl_wkfwmst b on a.wkfw_id = b.wkfw_id ";
		$cond .= "where not a.delete_flg and a.apply_stat = '1' and a.emp_id = '$emp_id' and b.wkfw_id = $wkfw_id ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}


	// 申請登録
	function regist_apply($arr)
	{
		$apply_id          = $arr["apply_id"];
		$wkfw_id           = $arr["wkfw_id"];
		$apply_content     = $arr["apply_content"];
		$emp_id            = $arr["emp_id"];
		$apply_stat        = $arr["apply_stat"];
		$apply_date        = $arr["apply_date"];
		$delete_flg        = $arr["delete_flg"];
		$apply_title       = $arr["apply_title"];
		$re_apply_id       = $arr["re_apply_id"];
		$apv_fix_show_flg  = $arr["apv_fix_show_flg"];
		$apv_bak_show_flg  = $arr["apv_bak_show_flg"];
		$emp_class         = $arr["emp_class"];
		$emp_attribute     = $arr["emp_attribute"];
		$emp_dept          = $arr["emp_dept"];
		$apv_ng_show_flg   = $arr["apv_ng_show_flg"];
		$emp_room          = $arr["emp_room"];
		$draft_flg         = $arr["draft_flg"];
		$wkfw_appr         = $arr["wkfw_appr"];
		$wkfw_content_type = $arr["wkfw_content_type"];
		$apply_title_disp_flg = $arr["apply_title_disp_flg"];
		$apply_no          = $arr["apply_no"];
        $notice_sel_flg    = $arr["notice_sel_flg"];
        $wkfw_history_no   = $arr["wkfw_history_no"];
		$wkfwfile_history_no = $arr["wkfwfile_history_no"];

		$sql  = "insert into jnl_apply ";
		$sql .= "(apply_id, ";
		$sql .= "wkfw_id, ";
		$sql .= "apply_content, ";
		$sql .= "emp_id, ";
		$sql .= "apply_stat, ";
		$sql .= "apply_date, ";
		$sql .= "delete_flg, ";
		$sql .= "apply_title, ";
		$sql .= "re_apply_id, ";
		$sql .= "apv_fix_show_flg, ";
		$sql .= "apv_bak_show_flg, ";
		$sql .= "emp_class, ";
		$sql .= "emp_attribute, ";
		$sql .= "emp_dept, ";
		$sql .= "apv_ng_show_flg, ";
		$sql .= "emp_room, ";
		$sql .= "draft_flg, ";
		$sql .= "wkfw_appr, ";
		$sql .= "wkfw_content_type, ";
		$sql .= "apply_title_disp_flg, ";
		$sql .= "apply_no, ";
		$sql .= "notice_sel_flg, ";
		$sql .= "wkfw_history_no, ";
		$sql .= "wkfwfile_history_no) ";
		$sql .= "values (";

		$content = array(
							$apply_id,
							$wkfw_id,
							pg_escape_string($apply_content),
							$emp_id,
							$apply_stat,
							$apply_date,
							$delete_flg,
							pg_escape_string($apply_title),
							$re_apply_id,
							$apv_fix_show_flg,
							$apv_bak_show_flg,
							$emp_class,
							$emp_attribute,
							$emp_dept,
							$apv_ng_show_flg,
							$emp_room,
							$draft_flg,
							$wkfw_appr,
							$wkfw_content_type,
							$apply_title_disp_flg,
							$apply_no,
							$notice_sel_flg,
							$wkfw_history_no,
							$wkfwfile_history_no
						);


		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}



//登録直前に入力されているカンマを削除する
function del_comma_func($total)
{
	$arr = array();

	for($i=0; $i<count($total); $i++)
	{
		$arr[$i] = str_replace(',','',$total[$i]);

		if($arr[$i] == "")
		{
			$arr[$i] = null;
		}


	}

	return $arr;
}



// 集計用データの登録処理(病院日報)
function regist_add_up_apply_report1($total)
{

	//登録直前に入力されているカンマを削除する
	$total = $this->del_comma_func($total);

	$sql=	"insert into jnl_hospital_day (";
	$sql .=	"	emp_id , ";
	$sql .=	"	jnl_facility_id , ";
	$sql .=	"	newest_apply_id , ";
	$sql .=	"	jnl_status , ";
	$sql .=	"	regist_date , ";
	$sql .=	"	naika_ga	  , ";
	$sql .=	"	naika_ip	  , ";
	$sql .=	"	naika_shog	  , ";
	$sql .=	"	naika_seip	  , ";
	$sql .=	"	naika_ir	  , ";
	$sql .=	"	naika_kaig	  , ";
	$sql .=	"	naika_kaif	  , ";
	$sql .=	"	naika_ak	  , ";
	$sql .=	"	naika_kan	  , ";
	$sql .=	"	naika_icu	  , ";
	$sql .=	"	naika_shoni	  , ";
	$sql .=	"	naika_seir	  , ";
	$sql .=	"	naika_tok	  , ";
	$sql .=	"	naika_nin	  , ";
	$sql .=	"	sinkei_ga	  , ";
	$sql .=	"	sinkei_ip	  , ";
	$sql .=	"	sinkei_shog	  , ";
	$sql .=	"	sinkei_seip	  , ";
	$sql .=	"	sinkei_ir	  , ";
	$sql .=	"	sinkei_kaig	  , ";
	$sql .=	"	sinkei_kaif	  , ";
	$sql .=	"	sinkei_ak	  , ";
	$sql .=	"	sinkei_kan	  , ";
	$sql .=	"	sinkei_icu	  , ";
	$sql .=	"	sinkei_shoni  , ";
	$sql .=	"	sinkei_seir	  , ";
	$sql .=	"	sinkei_tok	  , ";
	$sql .=	"	sinkei_nin	  , ";
	$sql .=	"	kokyu_ga	  , ";
	$sql .=	"	kokyu_ip	  , ";
	$sql .=	"	kokyu_shog	  , ";
	$sql .=	"	kokyu_seip	  , ";
	$sql .=	"	kokyu_ir	  , ";
	$sql .=	"	kokyu_kaig	  , ";
	$sql .=	"	kokyu_kaif	  , ";
	$sql .=	"	kokyu_ak	  , ";
	$sql .=	"	kokyu_kan	  , ";
	$sql .=	"	kokyu_icu	  , ";
	$sql .=	"	kokyu_shoni	  , ";
	$sql .=	"	kokyu_seir	  , ";
	$sql .=	"	kokyu_tok	  , ";
	$sql .=	"	kokyu_nin	  , ";
	$sql .=	"	shoka_ga	  , ";
	$sql .=	"	shoka_ip	  , ";
	$sql .=	"	shoka_shog	  , ";
	$sql .=	"	shoka_seip	  , ";
	$sql .=	"	shoka_ir	  , ";
	$sql .=	"	shoka_kaig	  , ";
	$sql .=	"	shoka_kaif	  , ";
	$sql .=	"	shoka_ak	  , ";
	$sql .=	"	shoka_kan	  , ";
	$sql .=	"	shoka_icu	  , ";
	$sql .=	"	shoka_shoni	  , ";
	$sql .=	"	shoka_seir	  , ";
	$sql .=	"	shoka_tok	  , ";
	$sql .=	"	shoka_nin	  , ";
	$sql .=	"	junkanki_ga	  , ";
	$sql .=	"	junkanki_ip	  , ";
	$sql .=	"	junkanki_shog , ";
	$sql .=	"	junkanki_seip , ";
	$sql .=	"	junkanki_ir	  , ";
	$sql .=	"	junkanki_kaig , ";
	$sql .=	"	junkanki_kaif , ";
	$sql .=	"	junkanki_ak	  , ";
	$sql .=	"	junkanki_kan  , ";
	$sql .=	"	junkanki_icu  , ";
	$sql .=	"	junkanki_shoni, ";
	$sql .=	"	junkanki_seir , ";
	$sql .=	"	junkanki_tok  , ";
	$sql .=	"	junkanki_nin  , ";
	$sql .=	"	shouni_ga	  , ";
	$sql .=	"	shouni_ip	  , ";
	$sql .=	"	shouni_shog	  , ";
	$sql .=	"	shouni_seip	  , ";
	$sql .=	"	shouni_ir	  , ";
	$sql .=	"	shouni_kaig	  , ";
	$sql .=	"	shouni_kaif	  , ";
	$sql .=	"	shouni_ak	  , ";
	$sql .=	"	shouni_kan	  , ";
	$sql .=	"	shouni_icu	  , ";
	$sql .=	"	shouni_shoni  , ";
	$sql .=	"	shouni_seir	  , ";
	$sql .=	"	shouni_tok	  , ";
	$sql .=	"	shouni_nin	  , ";
	$sql .=	"	geka_ga		  , ";
	$sql .=	"	geka_ip			, ";
	$sql .=	"	geka_shog	  , ";
	$sql .=	"	geka_seip	  , ";
	$sql .=	"	geka_ir	  , ";
	$sql .=	"	geka_kaig	  , ";
	$sql .=	"	geka_kaif	  , ";
	$sql .=	"	geka_ak	  , ";
	$sql .=	"	geka_kan	  , ";
	$sql .=	"	geka_icu	  , ";
	$sql .=	"	geka_shoni	  , ";
	$sql .=	"	geka_seir	  , ";
	$sql .=	"	geka_tok	  , ";
	$sql .=	"	geka_nin	  , ";
	$sql .=	"	seikei_ga	  , ";
	$sql .=	"	seikei_ip	  , ";
	$sql .=	"	seikei_shog	  , ";
	$sql .=	"	seikei_seip	  , ";
	$sql .=	"	seikei_ir	  , ";
	$sql .=	"	seikei_kaig	  , ";
	$sql .=	"	seikei_kaif	  , ";
	$sql .=	"	seikei_ak	  , ";
	$sql .=	"	seikei_kan	  , ";
	$sql .=	"	seikei_icu	  , ";
	$sql .=	"	seikei_shoni	  , ";
	$sql .=	"	seikei_seir	  , ";
	$sql .=	"	seikei_tok	  , ";
	$sql .=	"	seikei_nin	  , ";
	$sql .=	"	keiseibi_ga	  , ";
	$sql .=	"	keiseibi_ip	  , ";
	$sql .=	"	keiseibi_shog	  , ";
	$sql .=	"	keiseibi_seip	  , ";
	$sql .=	"	keiseibi_ir	  , ";
	$sql .=	"	keiseibi_kaig	  , ";
	$sql .=	"	keiseibi_kaif	  , ";
	$sql .=	"	keiseibi_ak	  , ";
	$sql .=	"	keiseibi_kan	  , ";
	$sql .=	"	keiseibi_icu	  , ";
	$sql .=	"	keiseibi_shoni	  , ";
	$sql .=	"	keiseibi_seir	  , ";
	$sql .=	"	keiseibi_tok	  , ";
	$sql .=	"	keiseibi_nin	  , ";
	$sql .=	"	nou_ga	  , ";
	$sql .=	"	nou_ip	  , ";
	$sql .=	"	nou_shog	  , ";
	$sql .=	"	nou_seip	  , ";
	$sql .=	"	nou_ir	  , ";
	$sql .=	"	nou_kaig	  , ";
	$sql .=	"	nou_kaif	  , ";
	$sql .=	"	nou_ak	  , ";
	$sql .=	"	nou_kan	  , ";
	$sql .=	"	nou_icu	  , ";
	$sql .=	"	nou_shoni	  , ";
	$sql .=	"	nou_seir	  , ";
	$sql .=	"	nou_tok	  , ";
	$sql .=	"	nou_nin	  , ";
	$sql .=	"	hifu_ga	  , ";
	$sql .=	"	hifu_ip	  , ";
	$sql .=	"	hifu_shog	  , ";
	$sql .=	"	hifu_seip	  , ";
	$sql .=	"	hifu_ir	  , ";
	$sql .=	"	hifu_kaig	  , ";
	$sql .=	"	hifu_kaif	  , ";
	$sql .=	"	hifu_ak	  , ";
	$sql .=	"	hifu_kan	  , ";
	$sql .=	"	hifu_icu	  , ";
	$sql .=	"	hifu_shoni	  , ";
	$sql .=	"	hifu_seir	  , ";
	$sql .=	"	hifu_tok	  , ";
	$sql .=	"	hifu_nin	  , ";
	$sql .=	"	hinyo_ga	  , ";
	$sql .=	"	hinyo_ip	  , ";
	$sql .=	"	hinyo_shog	  , ";
	$sql .=	"	hinyo_seip	  , ";
	$sql .=	"	hinyo_ir	  , ";
	$sql .=	"	hinyo_kaig	  , ";
	$sql .=	"	hinyo_kaif	  , ";
	$sql .=	"	hinyo_ak	  , ";
	$sql .=	"	hinyo_kan	  , ";
	$sql .=	"	hinyo_icu	  , ";
	$sql .=	"	hinyo_shoni	  , ";
	$sql .=	"	hinyo_seir	  , ";
	$sql .=	"	hinyo_tok	  , ";
	$sql .=	"	hinyo_nin	  , ";
	$sql .=	"	sanfu_ga	  , ";
	$sql .=	"	sanfu_ip	  , ";
	$sql .=	"	sanfu_shog	  , ";
	$sql .=	"	sanfu_seip	  , ";
	$sql .=	"	sanfu_ir	  , ";
	$sql .=	"	sanfu_kaig	  , ";
	$sql .=	"	sanfu_kaif	  , ";
	$sql .=	"	sanfu_ak	  , ";
	$sql .=	"	sanfu_kan	  , ";
	$sql .=	"	sanfu_icu	  , ";
	$sql .=	"	sanfu_shoni	  , ";
	$sql .=	"	sanfu_seir	  , ";
	$sql .=	"	sanfu_tok	  , ";
	$sql .=	"	sanfu_nin	  , ";
	$sql .=	"	ganka_ga	  , ";
	$sql .=	"	ganka_ip	  , ";
	$sql .=	"	ganka_shog	  , ";
	$sql .=	"	ganka_seip	  , ";
	$sql .=	"	ganka_ir	  , ";
	$sql .=	"	ganka_kaig	  , ";
	$sql .=	"	ganka_kaif	  , ";
	$sql .=	"	ganka_ak	  , ";
	$sql .=	"	ganka_kan	  , ";
	$sql .=	"	ganka_icu	  , ";
	$sql .=	"	ganka_shoni	  , ";
	$sql .=	"	ganka_seir	  , ";
	$sql .=	"	ganka_tok	  , ";
	$sql .=	"	ganka_nin	  , ";
	$sql .=	"	jibi_ga	  , ";
	$sql .=	"	jibi_ip	  , ";
	$sql .=	"	jibi_shog	  , ";
	$sql .=	"	jibi_seip	  , ";
	$sql .=	"	jibi_ir	  , ";
	$sql .=	"	jibi_kaig	  , ";
	$sql .=	"	jibi_kaif	  , ";
	$sql .=	"	jibi_ak	  , ";
	$sql .=	"	jibi_kan	  , ";
	$sql .=	"	jibi_icu	  , ";
	$sql .=	"	jibi_shoni	  , ";
	$sql .=	"	jibi_seir	  , ";
	$sql .=	"	jibi_tok	  , ";
	$sql .=	"	jibi_nin	  , ";
	$sql .=	"	touseki_ga	  , ";
	$sql .=	"	touseki_ip	  , ";
	$sql .=	"	touseki_shog	  , ";
	$sql .=	"	touseki_seip	  , ";
	$sql .=	"	touseki_ir	  , ";
	$sql .=	"	touseki_kaig	  , ";
	$sql .=	"	touseki_kaif	  , ";
	$sql .=	"	touseki_ak	  , ";
	$sql .=	"	touseki_kan	  , ";
	$sql .=	"	touseki_icu	  , ";
	$sql .=	"	touseki_shoni	  , ";
	$sql .=	"	touseki_seir	  , ";
	$sql .=	"	touseki_tok	  , ";
	$sql .=	"	touseki_nin	  , ";
	$sql .=	"	seisin_ga	  , ";
	$sql .=	"	seisin_ip	  , ";
	$sql .=	"	seisin_shog	  , ";
	$sql .=	"	seisin_seip	  , ";
	$sql .=	"	seisin_ir	  , ";
	$sql .=	"	seisin_kaig	  , ";
	$sql .=	"	seisin_kaif	  , ";
	$sql .=	"	seisin_ak	  , ";
	$sql .=	"	seisin_kan	  , ";
	$sql .=	"	seisin_icu	  , ";
	$sql .=	"	seisin_shoni	  , ";
	$sql .=	"	seisin_seir	  , ";
	$sql .=	"	seisin_tok	  , ";
	$sql .=	"	seisin_nin	  , ";
	$sql .=	"	sika_ga	  , ";
	$sql .=	"	sika_ip	  , ";
	$sql .=	"	sika_shog	  , ";
	$sql .=	"	sika_seip	  , ";
	$sql .=	"	sika_ir	  , ";
	$sql .=	"	sika_kaig	  , ";
	$sql .=	"	sika_kaif	  , ";
	$sql .=	"	sika_ak	  , ";
	$sql .=	"	sika_kan	  , ";
	$sql .=	"	sika_icu	  , ";
	$sql .=	"	sika_shoni	  , ";
	$sql .=	"	sika_seir	  , ";
	$sql .=	"	sika_tok	  , ";
	$sql .=	"	sika_nin	  , ";
	$sql .=	"	hoshasen_ga	  , ";
	$sql .=	"	hoshasen_ip	  , ";
	$sql .=	"	hoshasen_shog	  , ";
	$sql .=	"	hoshasen_seip	  , ";
	$sql .=	"	hoshasen_ir	  , ";
	$sql .=	"	hoshasen_kaig	  , ";
	$sql .=	"	hoshasen_kaif	  , ";
	$sql .=	"	hoshasen_ak	  , ";
	$sql .=	"	hoshasen_kan	  , ";
	$sql .=	"	hoshasen_icu	  , ";
	$sql .=	"	hoshasen_shoni	  , ";
	$sql .=	"	hoshasen_seir	  , ";
	$sql .=	"	hoshasen_tok	  , ";
	$sql .=	"	hoshasen_nin	  , ";
	$sql .=	"	masui_ga	  , ";
	$sql .=	"	masui_ip	  , ";
	$sql .=	"	masui_shog	  , ";
	$sql .=	"	masui_seip	  , ";
	$sql .=	"	masui_ir	  , ";
	$sql .=	"	masui_kaig	  , ";
	$sql .=	"	masui_kaif	  , ";
	$sql .=	"	masui_ak	  , ";
	$sql .=	"	masui_kan	  , ";
	$sql .=	"	masui_icu	  , ";
	$sql .=	"	masui_shoni	  , ";
	$sql .=	"	masui_seir	  , ";
	$sql .=	"	masui_tok	  , ";
	$sql .=	"	masui_nin	  , ";
	$sql .=	"	gokei_ga	 ,";
	$sql .=	"	gokei_ip	 ,";
	$sql .=	"	gokei_shog	 ,";
	$sql .=	"	gokei_seip	 ,";
	$sql .=	"	gokei_ir	 ,";
	$sql .=	"	gokei_kaig	 ,";
	$sql .=	"	gokei_kaif	 ,";
	$sql .=	"	gokei_ak	 ,";
	$sql .=	"	gokei_kan	 ,";
	$sql .=	"	gokei_icu	 ,";
	$sql .=	"	gokei_shoni	 ,";
	$sql .=	"	gokei_seir	 ,";
	$sql .=	"	gokei_tok	 ,";
	$sql .=	"	gokei_nin	 ,";
	$sql .=	"	nyuin	  , ";
	$sql .=	"	taiin	  , ";
	$sql .=	"	del_flg ";
	$sql .=	") values (";

	$content = $total;

	$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
	if($ins == 0)
	{
		pg_query($this->_db_con, "rollback");
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}


	// 集計用データの登録処理(病院日報)
	function update_add_up_apply_report1($total,$apply_id)
	{
		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_hospital_day set";
		$set =	array("newest_apply_id");
		array_push($set,"emp_id");
		array_push($set,"jnl_status");
		array_push($set,"naika_ga");
		array_push($set,"naika_ip");
		 array_push($set,"naika_shog");
		 array_push($set,"naika_seip");
		 array_push($set,"naika_ir");
		 array_push($set,"naika_kaig");
		 array_push($set,"naika_kaif");
		 array_push($set,"naika_ak");
		 array_push($set,"naika_kan");
		 array_push($set,"naika_icu");
		 array_push($set,"naika_shoni");
		 array_push($set,"naika_seir");
		 array_push($set,"naika_tok");
		 array_push($set,"naika_nin");
		 array_push($set,"sinkei_ga");
		 array_push($set,"sinkei_ip");
		 array_push($set,"sinkei_shog");
		 array_push($set,"sinkei_seip");
		 array_push($set,"sinkei_ir");
		 array_push($set,"sinkei_kaig");
		 array_push($set,"sinkei_kaif");
		 array_push($set,"sinkei_ak");
		 array_push($set,"sinkei_kan");
		 array_push($set,"sinkei_icu");
		 array_push($set,"sinkei_shoni");
		 array_push($set,"sinkei_seir");
		 array_push($set,"sinkei_tok");
		 array_push($set,"sinkei_nin");
		 array_push($set,"kokyu_ga");
		 array_push($set,"kokyu_ip");
		 array_push($set,"kokyu_shog");
		 array_push($set,"kokyu_seip");
		 array_push($set,"kokyu_ir");
		 array_push($set,"kokyu_kaig");
		 array_push($set,"kokyu_kaif");
		 array_push($set,"kokyu_ak");
		 array_push($set,"kokyu_kan");
		 array_push($set,"kokyu_icu");
		 array_push($set,"kokyu_shoni");
		 array_push($set,"kokyu_seir");
		 array_push($set,"kokyu_tok");
		 array_push($set,"kokyu_nin");
		 array_push($set,"shoka_ga");
		 array_push($set,"shoka_ip");
		 array_push($set,"shoka_shog");
		 array_push($set,"shoka_seip");
		 array_push($set,"shoka_ir");
		 array_push($set,"shoka_kaig");
		 array_push($set,"shoka_kaif");
		 array_push($set,"shoka_ak");
		 array_push($set,"shoka_kan");
		 array_push($set,"shoka_icu");
		 array_push($set,"shoka_shoni");
		 array_push($set,"shoka_seir");
		 array_push($set,"shoka_tok");
		 array_push($set,"shoka_nin");
		 array_push($set,"junkanki_ga");
		 array_push($set,"junkanki_ip");
		 array_push($set,"junkanki_shog");
		 array_push($set,"junkanki_seip");
		 array_push($set,"junkanki_ir");
		 array_push($set,"junkanki_kaig");
		 array_push($set,"junkanki_kaif");
		 array_push($set,"junkanki_ak");
		 array_push($set,"junkanki_kan");
		 array_push($set,"junkanki_icu");
		 array_push($set,"junkanki_shoni");
		 array_push($set,"junkanki_seir");
		 array_push($set,"junkanki_tok");
		 array_push($set,"junkanki_nin");
		 array_push($set,"shouni_ga");
		 array_push($set,"shouni_ip");
		 array_push($set,"shouni_shog");
		 array_push($set,"shouni_seip");
		 array_push($set,"shouni_ir");
		 array_push($set,"shouni_kaig");
		 array_push($set,"shouni_kaif");
		 array_push($set,"shouni_ak");
		 array_push($set,"shouni_kan");
		 array_push($set,"shouni_icu");
		 array_push($set,"shouni_shoni");
		 array_push($set,"shouni_seir");
		 array_push($set,"shouni_tok");
		 array_push($set,"shouni_nin");
		 array_push($set,"geka_ga");
		 array_push($set,"geka_ip");
		 array_push($set,"geka_shog");
		 array_push($set,"geka_seip");
		 array_push($set,"geka_ir");
		 array_push($set,"geka_kaig");
		 array_push($set,"geka_kaif");
		 array_push($set,"geka_ak");
		 array_push($set,"geka_kan");
		 array_push($set,"geka_icu");
		 array_push($set,"geka_shoni");
		 array_push($set,"geka_seir");
		 array_push($set,"geka_tok");
		 array_push($set,"geka_nin");
		 array_push($set,"seikei_ga");
		 array_push($set,"seikei_ip");
		 array_push($set,"seikei_shog");
		 array_push($set,"seikei_seip");
		 array_push($set,"seikei_ir");
		 array_push($set,"seikei_kaig");
		 array_push($set,"seikei_kaif");
		 array_push($set,"seikei_ak");
		 array_push($set,"seikei_kan");
		 array_push($set,"seikei_icu");
		 array_push($set,"seikei_shoni");
		 array_push($set,"seikei_seir");
		 array_push($set,"seikei_tok");
		 array_push($set,"seikei_nin");
		 array_push($set,"keiseibi_ga");
		 array_push($set,"keiseibi_ip");
		 array_push($set,"keiseibi_shog");
		 array_push($set,"keiseibi_seip");
		 array_push($set,"keiseibi_ir");
		 array_push($set,"keiseibi_kaig");
		 array_push($set,"keiseibi_kaif");
		 array_push($set,"keiseibi_ak");
		 array_push($set,"keiseibi_kan");
		 array_push($set,"keiseibi_icu");
		 array_push($set,"keiseibi_shoni");
		 array_push($set,"keiseibi_seir");
		 array_push($set,"keiseibi_tok");
		 array_push($set,"keiseibi_nin");
		 array_push($set,"nou_ga");
		 array_push($set,"nou_ip");
		 array_push($set,"nou_shog");
		 array_push($set,"nou_seip");
		 array_push($set,"nou_ir");
		 array_push($set,"nou_kaig");
		 array_push($set,"nou_kaif");
		 array_push($set,"nou_ak");
		 array_push($set,"nou_kan");
		 array_push($set,"nou_icu");
		 array_push($set,"nou_shoni");
		 array_push($set,"nou_seir");
		 array_push($set,"nou_tok");
		 array_push($set,"nou_nin");
		 array_push($set,"hifu_ga");
		 array_push($set,"hifu_ip");
		 array_push($set,"hifu_shog");
		 array_push($set,"hifu_seip");
		 array_push($set,"hifu_ir");
		 array_push($set,"hifu_kaig");
		 array_push($set,"hifu_kaif");
		 array_push($set,"hifu_ak");
		 array_push($set,"hifu_kan");
		 array_push($set,"hifu_icu");
		 array_push($set,"hifu_shoni");
		 array_push($set,"hifu_seir");
		 array_push($set,"hifu_tok");
		 array_push($set,"hifu_nin");
		 array_push($set,"hinyo_ga");
		 array_push($set,"hinyo_ip");
		 array_push($set,"hinyo_shog");
		 array_push($set,"hinyo_seip");
		 array_push($set,"hinyo_ir");
		 array_push($set,"hinyo_kaig");
		 array_push($set,"hinyo_kaif");
		 array_push($set,"hinyo_ak");
		 array_push($set,"hinyo_kan");
		 array_push($set,"hinyo_icu");
		 array_push($set,"hinyo_shoni");
		 array_push($set,"hinyo_seir");
		 array_push($set,"hinyo_tok");
		 array_push($set,"hinyo_nin");
		 array_push($set,"sanfu_ga");
		 array_push($set,"sanfu_ip");
		 array_push($set,"sanfu_shog");
		 array_push($set,"sanfu_seip");
		 array_push($set,"sanfu_ir");
		 array_push($set,"sanfu_kaig");
		 array_push($set,"sanfu_kaif");
		 array_push($set,"sanfu_ak");
		 array_push($set,"sanfu_kan");
		 array_push($set,"sanfu_icu");
		 array_push($set,"sanfu_shoni");
		 array_push($set,"sanfu_seir");
		 array_push($set,"sanfu_tok");
		 array_push($set,"sanfu_nin");
		 array_push($set,"ganka_ga");
		 array_push($set,"ganka_ip");
		 array_push($set,"ganka_shog");
		 array_push($set,"ganka_seip");
		 array_push($set,"ganka_ir");
		 array_push($set,"ganka_kaig");
		 array_push($set,"ganka_kaif");
		 array_push($set,"ganka_ak");
		 array_push($set,"ganka_kan");
		 array_push($set,"ganka_icu");
		 array_push($set,"ganka_shoni");
		 array_push($set,"ganka_seir");
		 array_push($set,"ganka_tok");
		 array_push($set,"ganka_nin");
		 array_push($set,"jibi_ga");
		 array_push($set,"jibi_ip");
		 array_push($set,"jibi_shog");
		 array_push($set,"jibi_seip");
		 array_push($set,"jibi_ir");
		 array_push($set,"jibi_kaig");
		 array_push($set,"jibi_kaif");
		 array_push($set,"jibi_ak");
		 array_push($set,"jibi_kan");
		 array_push($set,"jibi_icu");
		 array_push($set,"jibi_shoni");
		 array_push($set,"jibi_seir");
		 array_push($set,"jibi_tok");
		 array_push($set,"jibi_nin");
		 array_push($set,"touseki_ga");
		 array_push($set,"touseki_ip");
		 array_push($set,"touseki_shog");
		 array_push($set,"touseki_seip");
		 array_push($set,"touseki_ir");
		 array_push($set,"touseki_kaig");
		 array_push($set,"touseki_kaif");
		 array_push($set,"touseki_ak");
		 array_push($set,"touseki_kan");
		 array_push($set,"touseki_icu");
		 array_push($set,"touseki_shoni");
		 array_push($set,"touseki_seir");
		 array_push($set,"touseki_tok");
		 array_push($set,"touseki_nin");
		 array_push($set,"seisin_ga");
		 array_push($set,"seisin_ip");
		 array_push($set,"seisin_shog");
		 array_push($set,"seisin_seip");
		 array_push($set,"seisin_ir");
		 array_push($set,"seisin_kaig");
		 array_push($set,"seisin_kaif");
		 array_push($set,"seisin_ak");
		 array_push($set,"seisin_kan");
		 array_push($set,"seisin_icu");
		 array_push($set,"seisin_shoni");
		 array_push($set,"seisin_seir");
		 array_push($set,"seisin_tok");
		 array_push($set,"seisin_nin");
		 array_push($set,"sika_ga");
		 array_push($set,"sika_ip");
		 array_push($set,"sika_shog");
		 array_push($set,"sika_seip");
		 array_push($set,"sika_ir");
		 array_push($set,"sika_kaig");
		 array_push($set,"sika_kaif");
		 array_push($set,"sika_ak");
		 array_push($set,"sika_kan");
		 array_push($set,"sika_icu");
		 array_push($set,"sika_shoni");
		 array_push($set,"sika_seir");
		 array_push($set,"sika_tok");
		 array_push($set,"sika_nin");
		 array_push($set,"hoshasen_ga");
		 array_push($set,"hoshasen_ip");
		 array_push($set,"hoshasen_shog");
		 array_push($set,"hoshasen_seip");
		 array_push($set,"hoshasen_ir");
		 array_push($set,"hoshasen_kaig");
		 array_push($set,"hoshasen_kaif");
		 array_push($set,"hoshasen_ak");
		 array_push($set,"hoshasen_kan");
		 array_push($set,"hoshasen_icu");
		 array_push($set,"hoshasen_shoni");
		 array_push($set,"hoshasen_seir");
		 array_push($set,"hoshasen_tok");
		 array_push($set,"hoshasen_nin");
		 array_push($set,"masui_ga");
		 array_push($set,"masui_ip");
		 array_push($set,"masui_shog");
		 array_push($set,"masui_seip");
		 array_push($set,"masui_ir");
		 array_push($set,"masui_kaig");
		 array_push($set,"masui_kaif");
		 array_push($set,"masui_ak");
		 array_push($set,"masui_kan");
		 array_push($set,"masui_icu");
		 array_push($set,"masui_shoni");
		 array_push($set,"masui_seir");
		 array_push($set,"masui_tok");
		 array_push($set,"masui_nin");
		 array_push($set,"gokei_ga");
		 array_push($set,"gokei_ip");
		 array_push($set,"gokei_shog");
		 array_push($set,"gokei_seip");
		 array_push($set,"gokei_ir");
		 array_push($set,"gokei_kaig");
		 array_push($set,"gokei_kaif");
		 array_push($set,"gokei_ak");
		 array_push($set,"gokei_kan");
		 array_push($set,"gokei_icu");
		 array_push($set,"gokei_shoni");
		 array_push($set,"gokei_seir");
		 array_push($set,"gokei_tok");
		 array_push($set,"gokei_nin");
		 array_push($set,"nyuin");
		 array_push($set,"taiin");

		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}




	// 集計用データの登録処理(病院月報1)
	function regist_add_up_apply_report2($total)
	{
		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_hospital_month_action (";
		$sql .=	"emp_id , ";
		$sql .=	"jnl_facility_id , ";
		$sql .=	"newest_apply_id , ";
		$sql .=	"jnl_status , ";
		$sql .=	"regist_date , ";
		$sql .="ope_all_anes, ";
		$sql .="ope_waist_anes, ";
		$sql .="ope_part_anes, ";
		$sql .="ope_scope, ";
		$sql .="ope_cataract, ";
		$sql .="scope_stomach, ";
		$sql .="scope_intestine, ";
		$sql .="scope_etc, ";
		$sql .="insp_ekg, ";
		$sql .="insp_diogram, ";
		$sql .="insp_treadmill, ";
		$sql .="insp_heart, ";
		$sql .="insp_stomach, ";
		$sql .="insp_carotid, ";
		$sql .="insp_lungs, ";
		$sql .="insp_bonesolt, ";
		$sql .="insp_gus, ";
		$sql .="radi_gnr_photo, ";
		$sql .="radi_man_mole, ";
		$sql .="radi_mdl, ";
		$sql .="radi_bem, ";
		$sql .="radi_dip, ";
		$sql .="radi_dic, ";
		$sql .="radi_secial, ";
		$sql .="radi_etc, ";
		$sql .="radi_ct_brain, ";
		$sql .="radi_ct_body, ";
		$sql .="radi_ct_etc, ";
		$sql .="radi_ct_add, ";
		$sql .="radi_mri_brain, ";
		$sql .="radi_mri_body, ";
		$sql .="radi_mri_etc, ";
		$sql .="radi_mri_add, ";
		$sql .="radi_angio, ";
		$sql .="fee_kid, ";
		$sql .="fee_sprt_nrt, ";
		$sql .="fee_care_breath, ";
		$sql .="fee_plan_security, ";
		$sql .="fee_plan_infection, ";
		$sql .="fee_mng_sec_instrmnt, ";
		$sql .="fee_guide_mng_md_one, ";
		$sql .="fee_guide_mng_md_two, ";
		$sql .="fee_guide_mng_md_three, ";
		$sql .="fee_orgnz_mng_sec_inf_md, ";
		$sql .="empty_one, ";
		$sql .="empty_two, ";
		$sql .="empty_thee, ";
		$sql .="empty_four, ";
		$sql .="empty_five, ";
		$sql .="empty_six, ";
		$sql .="empty_seven, ";
		$sql .=	"del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(病院月報1)
	function update_add_up_apply_report2($total,$apply_id)
	{
		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_hospital_month_action set";
		$set =	array("newest_apply_id");
		array_push($set,"emp_id");
		array_push($set,"jnl_status");
		array_push($set,"ope_all_anes");
		array_push($set,"ope_waist_anes");
		array_push($set,"ope_part_anes");
		array_push($set,"ope_scope");
		array_push($set,"ope_cataract");
		array_push($set,"scope_stomach");
		array_push($set,"scope_intestine");
		array_push($set,"scope_etc");
		array_push($set,"insp_ekg");
		array_push($set,"insp_diogram");
		array_push($set,"insp_treadmill");
		array_push($set,"insp_heart");
		array_push($set,"insp_stomach");
		array_push($set,"insp_carotid");
		array_push($set,"insp_lungs");
		array_push($set,"insp_bonesolt");
		array_push($set,"insp_gus");
		array_push($set,"radi_gnr_photo");
		array_push($set,"radi_man_mole");
		array_push($set,"radi_mdl");
		array_push($set,"radi_bem");
		array_push($set,"radi_dip");
		array_push($set,"radi_dic");
		array_push($set,"radi_secial");
		array_push($set,"radi_etc");
		array_push($set,"radi_ct_brain");
		array_push($set,"radi_ct_body");
		array_push($set,"radi_ct_etc");
		array_push($set,"radi_ct_add");
		array_push($set,"radi_mri_brain");
		array_push($set,"radi_mri_body");
		array_push($set,"radi_mri_etc");
		array_push($set,"radi_mri_add");
		array_push($set,"radi_angio");
		array_push($set,"fee_kid");
		array_push($set,"fee_sprt_nrt");
		array_push($set,"fee_care_breath");
		array_push($set,"fee_plan_security");
		array_push($set,"fee_plan_infection");
		array_push($set,"fee_mng_sec_instrmnt");
		array_push($set,"fee_guide_mng_md_one");
		array_push($set,"fee_guide_mng_md_two");
		array_push($set,"fee_guide_mng_md_three");
		array_push($set,"fee_orgnz_mng_sec_inf_md");
		array_push($set,"empty_one");
		array_push($set,"empty_two");
		array_push($set,"empty_thee");
		array_push($set,"empty_four");
		array_push($set,"empty_five");
		array_push($set,"empty_six");
		array_push($set,"empty_seven");

		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(病院月報2)
	function regist_add_up_apply_report3($total)
	{
		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_hospital_month_patient (";
		$sql .=	"emp_id , ";
		$sql .=	"jnl_facility_id , ";
		$sql .=	"newest_apply_id , ";
		$sql .=	"jnl_status , ";
		$sql .=	"regist_date , ";
		$sql .="gnr_ave_enter , ";
		$sql .="gnr_new_enter_gnr , ";
		$sql .="gnr_dpc_total , ";
		$sql .="gnr_ave_need_care , ";
		$sql .="gnr_one_fourteen , ";
		$sql .="gnr_fifteen_thirty , ";
		$sql .="gnr_emgc_mng , ";
		$sql .="gnr_over_ninety , ";
		$sql .="gnr_sp_ent_total , ";
		$sql .="hdc_one_fourteen , ";
		$sql .="hdc_fifteen_thirty , ";
		$sql .="hdc_over_ninety , ";
		$sql .="hdc_sp_ent_total , ";
		$sql .="rhb_ave_seriuos , ";
		$sql .="rhb_ave_return , ";
		$sql .="trt_a , ";
		$sql .="trt_b , ";
		$sql .="trt_c , ";
		$sql .="trt_d , ";
		$sql .="trt_e , ";
		$sql .="trt_f , ";
		$sql .="trt_g , ";
		$sql .="trt_h , ";
		$sql .="trt_i , ";
		$sql .="trt_one_fourteen , ";
		$sql .="trt_care_five , ";
		$sql .="trt_care_four , ";
		$sql .="trt_care_three , ";
		$sql .="trt_care_two , ";
		$sql .="trt_care_one , ";
		$sql .="new_comer , ";
		$sql .="first_comer , ";
		$sql .="intro_comer , ";
		$sql .="f_aid_req , ";
		$sql .="f_aid_rev , ";
		$sql .="f_aid_enter , ";
		$sql .="patient_overtime , ";
		$sql .="enter_overtime , ";
		$sql .="empty_one , ";
		$sql .="empty_two , ";
		$sql .="empty_thee , ";
		$sql .="empty_four , ";
		$sql .="empty_five , ";
		$sql .="empty_six , ";
		$sql .="empty_seven , ";
		$sql .=	"del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(病院月報2)
	function update_add_up_apply_report3($total,$apply_id)
	{
		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_hospital_month_patient set";
		$set =	array("newest_apply_id");
		array_push($set,"emp_id");
		array_push($set,"jnl_status");
		array_push($set,"gnr_ave_enter");
		array_push($set,"gnr_new_enter_gnr");
		array_push($set,"gnr_dpc_total");
		array_push($set,"gnr_ave_need_care");
		array_push($set,"gnr_one_fourteen");
		array_push($set,"gnr_fifteen_thirty");
		array_push($set,"gnr_emgc_mng");
		array_push($set,"gnr_over_ninety");
		array_push($set,"gnr_sp_ent_total");
		array_push($set,"hdc_one_fourteen");
		array_push($set,"hdc_fifteen_thirty");
		array_push($set,"hdc_over_ninety");
		array_push($set,"hdc_sp_ent_total");
		array_push($set,"rhb_ave_seriuos");
		array_push($set,"rhb_ave_return");
		array_push($set,"trt_a");
		array_push($set,"trt_b");
		array_push($set,"trt_c");
		array_push($set,"trt_d");
		array_push($set,"trt_e");
		array_push($set,"trt_f");
		array_push($set,"trt_g");
		array_push($set,"trt_h");
		array_push($set,"trt_i");
		array_push($set,"trt_one_fourteen");
		array_push($set,"trt_care_five");
		array_push($set,"trt_care_four");
		array_push($set,"trt_care_three");
		array_push($set,"trt_care_two");
		array_push($set,"trt_care_one");
		array_push($set,"new_comer");
		array_push($set,"first_comer");
		array_push($set,"intro_comer");
		array_push($set,"f_aid_req");
		array_push($set,"f_aid_rev");
		array_push($set,"f_aid_enter");
		array_push($set,"patient_overtime");
		array_push($set,"enter_overtime");
		array_push($set,"empty_one");
		array_push($set,"empty_two");
		array_push($set,"empty_thee");
		array_push($set,"empty_four");
		array_push($set,"empty_five");
		array_push($set,"empty_six");
		array_push($set,"empty_seven");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}



	// 集計用データの登録処理(老健 main)
	function regist_add_up_apply_nurse_report_main($total)
	{

		$sql=	"insert into jnl_nurse_home_main (";
		$sql .=	"newest_apply_id , ";
		$sql .=	"emp_id , ";
		$sql .=	"jnl_facility_id , ";
		$sql .=	"jnl_status , ";
		$sql .=	"jnl_report_flg , ";
		$sql .=	"regist_date , ";
		$sql .=	"del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(老健 main)
	function update_add_up_apply_nurse_report_main($total,$apply_id)
	{

		$sql=	"update  jnl_nurse_home_main set";
		$set =	array("newest_apply_id");
		array_push($set,"emp_id");
		array_push($set,"jnl_status");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの登録処理(老健 jnl_nurse_home_day_visit)
	function regist_add_up_apply_nurse_report_day1($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_nurse_home_day_visit (";
		$sql .="newest_apply_id , ";
		$sql .="time_area , ";
		$sql .="care_five , ";
		$sql .="care_four , ";
		$sql .="care_three , ";
		$sql .="care_two , ";
		$sql .="care_one , ";
		$sql .="care_etc , ";
		$sql .="del_flg  ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの更新処理(老健 jnl_nurse_home_day_visit)
	function update_add_up_apply_nurse_report_day1($total,$apply_id,$time_area)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_nurse_home_day_visit set";
		$set =	array("newest_apply_id");
		array_push($set,"time_area");
		array_push($set,"care_five");
		array_push($set,"care_four");
		array_push($set,"care_three");
		array_push($set,"care_two");
		array_push($set,"care_one");
		array_push($set,"care_etc");
		$cond = "where newest_apply_id = $apply_id and time_area=$time_area";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの登録処理(老健 jnl_nurse_home_day_visit_prevention)
	function regist_add_up_apply_nurse_report_day2($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_nurse_home_day_visit_prevention (";
		$sql .="newest_apply_id , ";
		$sql .="support_two , ";
		$sql .="support_one , ";
		$sql .="support_etc , ";
		$sql .="del_flg  ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(老健 jnl_nurse_home_day_visit)
	function update_add_up_apply_nurse_report_day2($total,$apply_id)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_nurse_home_day_visit_prevention set";
		$set =	array("newest_apply_id");
		array_push($set,"support_two");
		array_push($set,"support_one");
		array_push($set,"support_etc");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの登録処理(老健 jnl_nurse_home_day_enter)
	function regist_add_up_apply_nurse_report_day3($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_nurse_home_day_enter (";
		$sql .="newest_apply_id , ";
		$sql .="enter_level , ";
		$sql .="care_five , ";
		$sql .="care_four , ";
		$sql .="care_three , ";
		$sql .="care_two , ";
		$sql .="care_one , ";
		$sql .="care_etc , ";
		$sql .="del_flg  ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの更新処理(老健 jnl_nurse_home_day_enter)
	function update_add_up_apply_nurse_report_day3($total,$apply_id,$enter_level)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_nurse_home_day_enter set";
		$set =	array("newest_apply_id");
		array_push($set,"enter_level");
		array_push($set,"care_five");
		array_push($set,"care_four");
		array_push($set,"care_three");
		array_push($set,"care_two");
		array_push($set,"care_one");
		array_push($set,"care_etc");
		$cond = "where newest_apply_id = $apply_id and enter_level=$enter_level";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(老健 jnl_nurse_home_day_short)
	function regist_add_up_apply_nurse_report_day4($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_nurse_home_day_short (";
		$sql .="newest_apply_id , ";
		$sql .="short_level , ";
		$sql .="care_five , ";
		$sql .="care_four , ";
		$sql .="care_three , ";
		$sql .="care_two , ";
		$sql .="care_one , ";
		$sql .="care_etc , ";
		$sql .="del_flg  ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(老健 jnl_nurse_home_day_short)
	function update_add_up_apply_nurse_report_day4($total,$apply_id,$short_level)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_nurse_home_day_short set";
		$set =	array("newest_apply_id");
		array_push($set,"short_level");
		array_push($set,"care_five");
		array_push($set,"care_four");
		array_push($set,"care_three");
		array_push($set,"care_two");
		array_push($set,"care_one");
		array_push($set,"care_etc");
		$cond = "where newest_apply_id = $apply_id and short_level=$short_level";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(老健 jnl_nurse_home_day_short_prevention)
	function regist_add_up_apply_nurse_report_day5($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_nurse_home_day_short_prevention (";
		$sql .="newest_apply_id , ";
		$sql .="prevention_level , ";
		$sql .="support_two , ";
		$sql .="support_one , ";
		$sql .="support_etc , ";
		$sql .="del_flg  ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの更新処理(老健 jnl_nurse_home_day_short_prevention)
	function update_add_up_apply_nurse_report_day5($total,$apply_id,$prevention_level)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_nurse_home_day_short_prevention set";
		$set =	array("newest_apply_id");
		array_push($set,"prevention_level");
		array_push($set,"support_two");
		array_push($set,"support_one");
		array_push($set,"support_etc");
		$cond = "where newest_apply_id = $apply_id and prevention_level=$prevention_level";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの登録処理(老健 jnl_nurse_home_day_rehab)
	function regist_add_up_apply_nurse_report_day6($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_nurse_home_day_rehab (";
		$sql .="newest_apply_id , ";
		$sql .="care_five , ";
		$sql .="care_four , ";
		$sql .="care_three , ";
		$sql .="care_two , ";
		$sql .="care_one , ";
		$sql .="care_etc , ";
		$sql .="del_flg  ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの更新処理(老健 jnl_nurse_home_day_rehab)
	function update_add_up_apply_nurse_report_day6($total,$apply_id)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_nurse_home_day_rehab set";
		$set =	array("newest_apply_id");
		array_push($set,"care_five");
		array_push($set,"care_four");
		array_push($set,"care_three");
		array_push($set,"care_two");
		array_push($set,"care_one");
		array_push($set,"care_etc");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(老健 jnl_nurse_home_day_rehab_prevention)
	function regist_add_up_apply_nurse_report_day7($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_nurse_home_day_rehab_prevention (";
		$sql .="newest_apply_id , ";
		$sql .="support_two , ";
		$sql .="support_one , ";
		$sql .="support_etc , ";
		$sql .="del_flg  ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの更新処理(老健 jnl_nurse_home_day_rehab_prevention)
	function update_add_up_apply_nurse_report_day7($total,$apply_id)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_nurse_home_day_rehab_prevention set";
		$set =	array("newest_apply_id");
		array_push($set,"support_two");
		array_push($set,"support_one");
		array_push($set,"support_etc");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(老健 jnl_nurse_home_day_cnt)
	function regist_add_up_apply_nurse_report_day8($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_nurse_home_day_cnt (";
		$sql .="newest_apply_id , ";
		$sql .="cnt_in , ";
		$sql .="cnt_out , ";
		$sql .="out_return , ";
		$sql .="short_in , ";
		$sql .="short_out , ";
		$sql .="judge_ok , ";
		$sql .="judge_ng , ";
		$sql .="cnt_visit , ";
		$sql .="del_flg  ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの更新処理(老健 jnl_nurse_home_day_cnt)
	function update_add_up_apply_nurse_report_day8($total,$apply_id)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_nurse_home_day_cnt set";
		$set =	array("newest_apply_id");
		array_push($set,"cnt_in");
		array_push($set,"cnt_out");
		array_push($set,"out_return");
		array_push($set,"short_in");
		array_push($set,"short_out");
		array_push($set,"judge_ok");
		array_push($set,"judge_ng");
		array_push($set,"cnt_visit");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}



	// 集計用データの登録処理(老健 jnl_nurse_home_mon_enter)
	function regist_add_up_apply_nurse_report_mon1($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_nurse_home_mon_enter (";
		$sql .="newest_apply_id, ";
		$sql .="night_assigned, ";
		$sql .="short_term_rehab, ";
		$sql .="sterm_dementia, ";
		$sql .="care_dementia, ";
		$sql .="young_dementia, ";
		$sql .="terminal_thirty, ";
		$sql .="terminal_thirty_one, ";
		$sql .="keep_treat, ";
		$sql .="init_add, ";
		$sql .="out_preguidance, ";
		$sql .="out_guidance, ";
		$sql .="out_information, ";
		$sql .="out_presession, ";
		$sql .="visit_care, ";
		$sql .="management_nutrition, ";
		$sql .="move_oral, ";
		$sql .="keep_oral_one, ";
		$sql .="keep_oral_two, ";
		$sql .="keep_mouth_func, ";
		$sql .="treat_food, ";
		$sql .="support_return_home_one, ";
		$sql .="support_return_home_two, ";
		$sql .="urgent_treat, ";
		$sql .="care_dementia_one, ";
		$sql .="care_dementia_two, ";
		$sql .="dementia_information, ";
		$sql .="service_one, ";
		$sql .="service_two, ";
		$sql .="service_three, ";
		$sql .="del_flg ";
		$sql .=	") values (";
		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}



	// 集計用データの登録処理(老健 jnl_nurse_home_mon_short)
	function regist_add_up_apply_nurse_report_mon2($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_nurse_home_mon_short (";
		$sql .="newest_apply_id, ";
		$sql .="night_assigned, ";
		$sql .="enforce_rehab, ";
		$sql .="personal_rehab, ";
		$sql .="care_dementia, ";
		$sql .="urgent_support_dementia, ";
		$sql .="young_dementia, ";
		$sql .="send_return, ";
		$sql .="keep_treat, ";
		$sql .="treat_food, ";
		$sql .="urgent_network, ";
		$sql .="urgent_treat, ";
		$sql .="service_one, ";
		$sql .="service_two, ";
		$sql .="service_three, ";
		$sql .="del_flg ";
		$sql .=	") values (";
		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(老健 jnl_nurse_home_mon_commute)
	function regist_add_up_apply_nurse_report_mon3($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_nurse_home_mon_commute (";
		$sql .="newest_apply_id, ";
		$sql .="serial_care_a, ";
		$sql .="serial_care_b, ";
		$sql .="bathe_care, ";
		$sql .="plan_rehab, ";
		$sql .="management_rehab, ";
		$sql .="sterm_rehab_one, ";
		$sql .="sterm_rehab_three, ";
		$sql .="personal_rehab, ";
		$sql .="sterm_dementia, ";
		$sql .="young_dementia, ";
		$sql .="improve_food, ";
		$sql .="keep_mouth_func, ";
		$sql .="service_one, ";
		$sql .="service_two, ";
		$sql .="del_flg ";
		$sql .=	") values (";
		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}



	// 集計用データの登録処理(老健 jnl_nurse_home_mon_commute_pre)
	function regist_add_up_apply_nurse_report_mon4($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_nurse_home_mon_commute_pre (";
		$sql .="newest_apply_id, ";
		$sql .="young_dementia, ";
		$sql .="improve_motion_func, ";
		$sql .="improve_food, ";
		$sql .="improve_mouth_func, ";
		$sql .="value_office, ";
		$sql .="service_one_one, ";
		$sql .="service_one_two, ";
		$sql .="service_two_one, ";
		$sql .="service_two_two, ";
		$sql .="del_flg ";
		$sql .=	") values (";
		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの登録処理(老健 jnl_nurse_home_mon_visit)
	function regist_add_up_apply_nurse_report_mon5($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_nurse_home_mon_visit (";
		$sql .="newest_apply_id, ";
		$sql .="short_term_rehab_one, ";
		$sql .="short_term_rehab_three, ";
		$sql .="service, ";
		$sql .="del_flg ";
		$sql .=	") values (";
		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの登録処理(老健 jnl_nurse_home_mon_visit_pre)
	function regist_add_up_apply_nurse_report_mon6($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_nurse_home_mon_visit_pre (";
		$sql .="newest_apply_id, ";
		$sql .="short_term_rehab_three, ";
		$sql .="service, ";
		$sql .="del_flg ";
		$sql .=	") values (";
		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの更新処理(老健 jnl_nurse_home_mon_enter)
	function update_add_up_apply_nurse_report_mon1($total,$apply_id)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_nurse_home_mon_enter set";
		$set =	array("newest_apply_id");
		array_push($set,"night_assigned");
		array_push($set,"short_term_rehab");
		array_push($set,"sterm_dementia");
		array_push($set,"care_dementia");
		array_push($set,"young_dementia");
		array_push($set,"terminal_thirty");
		array_push($set,"terminal_thirty_one");
		array_push($set,"keep_treat");
		array_push($set,"init_add");
		array_push($set,"out_preguidance");
		array_push($set,"out_guidance");
		array_push($set,"out_information");
		array_push($set,"out_presession");
		array_push($set,"visit_care");
		array_push($set,"management_nutrition");
		array_push($set,"move_oral");
		array_push($set,"keep_oral_one");
		array_push($set,"keep_oral_two");
		array_push($set,"keep_mouth_func");
		array_push($set,"treat_food");
		array_push($set,"support_return_home_one");
		array_push($set,"support_return_home_two");
		array_push($set,"urgent_treat");
		array_push($set,"care_dementia_one");
		array_push($set,"care_dementia_two");
		array_push($set,"dementia_information");
		array_push($set,"service_one");
		array_push($set,"service_two");
		array_push($set,"service_three");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの更新処理(老健 jnl_nurse_home_mon_short)
	function update_add_up_apply_nurse_report_mon2($total,$apply_id)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_nurse_home_mon_short set";
		$set =	array("newest_apply_id");
		array_push($set,"night_assigned");
		array_push($set,"enforce_rehab");
		array_push($set,"personal_rehab");
		array_push($set,"care_dementia");
		array_push($set,"urgent_support_dementia");
		array_push($set,"young_dementia");
		array_push($set,"send_return");
		array_push($set,"keep_treat");
		array_push($set,"treat_food");
		array_push($set,"urgent_network");
		array_push($set,"urgent_treat");
		array_push($set,"service_one");
		array_push($set,"service_two");
		array_push($set,"service_three");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの更新処理(老健 jnl_nurse_home_mon_commute)
	function update_add_up_apply_nurse_report_mon3($total,$apply_id)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_nurse_home_mon_commute set";
		$set =	array("newest_apply_id");
		array_push($set,"serial_care_a");
		array_push($set,"serial_care_b");
		array_push($set,"bathe_care");
		array_push($set,"plan_rehab");
		array_push($set,"management_rehab");
		array_push($set,"sterm_rehab_one");
		array_push($set,"sterm_rehab_three");
		array_push($set,"personal_rehab");
		array_push($set,"sterm_dementia");
		array_push($set,"young_dementia");
		array_push($set,"improve_food");
		array_push($set,"keep_mouth_func");
		array_push($set,"service_one");
		array_push($set,"service_two");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの更新処理(老健 jnl_nurse_home_mon_commute_pre)
	function update_add_up_apply_nurse_report_mon4($total,$apply_id)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_nurse_home_mon_commute_pre set";
		$set =	array("newest_apply_id");
		array_push($set,"young_dementia");
		array_push($set,"improve_motion_func");
		array_push($set,"improve_food");
		array_push($set,"improve_mouth_func");
		array_push($set,"value_office");
		array_push($set,"service_one_one");
		array_push($set,"service_one_two");
		array_push($set,"service_two_one");
		array_push($set,"service_two_two");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(老健 jnl_nurse_home_mon_visit)
	function update_add_up_apply_nurse_report_mon5($total,$apply_id)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_nurse_home_mon_visit set";
		$set =	array("newest_apply_id");
		array_push($set,"short_term_rehab_one");
		array_push($set,"short_term_rehab_three");
		array_push($set,"service");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(老健 jnl_nurse_home_mon_visit_pre)
	function update_add_up_apply_nurse_report_mon6($total,$apply_id)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_nurse_home_mon_visit_pre set";
		$set =	array("newest_apply_id");
		array_push($set,"short_term_rehab_three");
		array_push($set,"service");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}




	// 集計用データの登録処理(返戻月報 main)
	function regist_add_up_apply_ret_report1($total)
	{

		$sql=	"insert into jnl_return_main (";
		$sql .=	"newest_apply_id , ";
		$sql .=	"emp_id , ";
		$sql .=	"jnl_facility_id , ";
		$sql .=	"jnl_status , ";
		$sql .=	"regist_date , ";
		$sql .=	"del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(返戻月報 main)
	function update_add_up_apply_ret_report1($total,$apply_id)
	{

		$sql=	"update  jnl_return_main set";
		$set =	array("newest_apply_id");
		array_push($set,"emp_id");
		array_push($set,"jnl_status");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}



	// 集計用データの登録処理(返戻月報 total)
	function regist_add_up_apply_ret_report2($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_return_total (";
		$sql .=	"newest_apply_id , ";
		$sql .=	"total_account , ";
		$sql .=	"total_point , ";
		$sql .=	"del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(返戻月報 total)
	function update_add_up_apply_ret_report2($total,$apply_id)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_return_total set";
		$set =	array("newest_apply_id");
		array_push($set,"total_account");
		array_push($set,"total_point");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(返戻月報 factor)
	function regist_add_up_apply_ret_report3($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_return_factor (";
		$sql .="newest_apply_id , ";
		$sql .="kind_factor , ";
		$sql .="kind_consult , ";
		$sql .="account_si , ";
		$sql .="point_si , ";
		$sql .="money_si , ";
		$sql .="account_ni , ";
		$sql .="point_ni , ";
		$sql .="money_ni , ";
		$sql .="reject_account_si , ";
		$sql .="reject_point_si , ";
		$sql .="reject_money_si , ";
		$sql .="reject_account_ni , ";
		$sql .="reject_point_ni , ";
		$sql .="reject_money_ni , ";
		$sql .=	"del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(返戻月報 factor)
	function update_add_up_apply_ret_report3($total,$apply_id,$kind_factor,$kind_consult)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_return_factor set";
		$set =	array("newest_apply_id");
		array_push($set,"kind_factor");
		array_push($set,"kind_consult");
		array_push($set,"account_si");
		array_push($set,"point_si");
		array_push($set,"money_si");
		array_push($set,"account_ni");
		array_push($set,"point_ni");
		array_push($set,"money_ni");
		array_push($set,"reject_account_si");
		array_push($set,"reject_point_si");
		array_push($set,"reject_money_si");
		array_push($set,"reject_account_ni");
		array_push($set,"reject_point_ni");
		array_push($set,"reject_money_ni");
		$cond = "where newest_apply_id = $apply_id and kind_factor=$kind_factor and kind_consult=$kind_consult";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}





	// 集計用データの登録処理(返戻月報 assessment)
	function regist_add_up_apply_ret_report4($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_return_assessment (";
		$sql .="newest_apply_id , ";
		$sql .="kind_factor , ";
		$sql .="kind_consult , ";
		$sql .="account_si , ";
		$sql .="point_si , ";
		$sql .="money_si , ";
		$sql .="account_ni , ";
		$sql .="point_ni , ";
		$sql .="money_ni , ";
		$sql .="reject_account_si , ";
		$sql .="reject_point_si , ";
		$sql .="reject_money_si , ";
		$sql .="reject_account_ni , ";
		$sql .="reject_point_ni , ";
		$sql .="reject_money_ni , ";
		$sql .="del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(返戻月報 assessment)
	function update_add_up_apply_ret_report4($total,$apply_id,$kind_factor,$kind_consult)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_return_assessment set";
		$set =	array("newest_apply_id");
		array_push($set,"kind_factor");
		array_push($set,"kind_consult");
		array_push($set,"account_si");
		array_push($set,"point_si");
		array_push($set,"money_si");
		array_push($set,"account_ni");
		array_push($set,"point_ni");
		array_push($set,"money_ni");
		array_push($set,"reject_account_si");
		array_push($set,"reject_point_si");
		array_push($set,"reject_money_si");
		array_push($set,"reject_account_ni");
		array_push($set,"reject_point_ni");
		array_push($set,"reject_money_ni");
		$cond = "where newest_apply_id = $apply_id and kind_factor=$kind_factor and kind_consult=$kind_consult";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(返戻月報 re_charge)
	function regist_add_up_apply_ret_report5($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_return_re_charge (";
		$sql .="newest_apply_id , ";
		$sql .="kind_factor , ";
		$sql .="kind_consult , ";
		$sql .="account_si , ";
		$sql .="day_si , ";
		$sql .="point_si , ";
		$sql .="money_si , ";
		$sql .="account_ni , ";
		$sql .="day_ni , ";
		$sql .="point_ni , ";
		$sql .="money_ni , ";
		$sql .="account_di , ";
		$sql .="day_di , ";
		$sql .="point_di , ";
		$sql .="money_di , ";
		$sql .="del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(返戻月報 re_charge)
	function update_add_up_apply_ret_report5($total,$apply_id,$kind_factor,$kind_consult)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_return_re_charge set";
		$set =	array("newest_apply_id");
		array_push($set,"kind_factor");
		array_push($set,"kind_consult");
		array_push($set,"account_si");
		array_push($set,"day_si");
		array_push($set,"point_si");
		array_push($set,"money_si");
		array_push($set,"account_ni");
		array_push($set,"day_ni");
		array_push($set,"point_ni");
		array_push($set,"money_ni");
		array_push($set,"account_di");
		array_push($set,"day_di");
		array_push($set,"point_di");
		array_push($set,"money_di");
		$cond = "where newest_apply_id = $apply_id and kind_factor=$kind_factor and kind_consult=$kind_consult";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}




	// 集計用データの登録処理(返戻月報 comment)
	function regist_add_up_apply_ret_report6($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_return_comment (";
		$sql .="newest_apply_id , ";
		$sql .="comment_return_si , ";
		$sql .="comment_return_ni , ";
		$sql .="comment_judge_si , ";
		$sql .="comment_judge_ni , ";
		$sql .="del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(返戻月報 comment)
	function update_add_up_apply_ret_report6($total,$apply_id)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_return_comment set";
		$set =	array("newest_apply_id");
		array_push($set,"comment_return_si");
		array_push($set,"comment_return_ni");
		array_push($set,"comment_judge_si");
		array_push($set,"comment_judge_ni");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(診療報酬総括表（医科） main)
	function regist_add_up_apply_synthesis_medi_main($total)
	{

		$sql=	"insert into jnl_reward_synthesis_medi_main (";
		$sql .=	"newest_apply_id , ";
		$sql .=	"emp_id , ";
		$sql .=	"jnl_facility_id , ";
		$sql .=	"jnl_status , ";
		$sql .=	"regist_date , ";
		$sql .=	"del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(診療報酬総括表（医科） main)
	function update_add_up_apply_synthesis_medi_main($total,$apply_id)
	{

		$sql=	"update  jnl_reward_synthesis_medi_main set";
		$set =	array("newest_apply_id");
		array_push($set,"emp_id");
		array_push($set,"jnl_status");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(診療報酬総括表（医科）)
	function regist_add_up_apply_synthesis_report1($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_reward_synthesis_medi (";
		$sql .="newest_apply_id , ";
		$sql .="type_section , ";
		$sql .="type_factor , ";
		$sql .="ent1_account , ";
		$sql .="ent1_days , ";
		$sql .="ent1_points , ";
		$sql .="ent1_money , ";
		$sql .="visit1_account , ";
		$sql .="visit1_days , ";
		$sql .="visit1_points , ";
		$sql .="visit1_money , ";
		$sql .="total1_account , ";
		$sql .="total1_days , ";
		$sql .="total1_points , ";
		$sql .="total1_money , ";
		$sql .="ent2_account , ";
		$sql .="ent2_days , ";
		$sql .="ent2_points , ";
		$sql .="ent2_money , ";
		$sql .="visit2_account , ";
		$sql .="visit2_days , ";
		$sql .="visit2_points , ";
		$sql .="visit2_money , ";
		$sql .="total2_account , ";
		$sql .="total2_days , ";
		$sql .="total2_points , ";
		$sql .="total2_money , ";
		$sql .="ent3_account , ";
		$sql .="ent3_days , ";
		$sql .="ent3_points , ";
		$sql .="ent3_money , ";
		$sql .="visit3_account , ";
		$sql .="visit3_days , ";
		$sql .="visit3_points , ";
		$sql .="visit3_money , ";
		$sql .="total3_account , ";
		$sql .="total3_days , ";
		$sql .="total3_points , ";
		$sql .="total3_money , ";
		$sql .="ent4_account , ";
		$sql .="ent4_days , ";
		$sql .="ent4_points , ";
		$sql .="ent4_money , ";
		$sql .="visit4_account , ";
		$sql .="visit4_days , ";
		$sql .="visit4_points , ";
		$sql .="visit4_money , ";
		$sql .="total4_account , ";
		$sql .="total4_days , ";
		$sql .="total4_points , ";
		$sql .="total4_money , ";
		$sql .="ent5_account , ";
		$sql .="ent5_days , ";
		$sql .="ent5_points , ";
		$sql .="ent5_money , ";
		$sql .="visit5_account , ";
		$sql .="visit5_days , ";
		$sql .="visit5_points , ";
		$sql .="visit5_money , ";
		$sql .="total5_account , ";
		$sql .="total5_days , ";
		$sql .="total5_points , ";
		$sql .="total5_money , ";
		$sql .="ent6_account , ";
		$sql .="ent6_days , ";
		$sql .="ent6_points , ";
		$sql .="ent6_money , ";
		$sql .="visit6_account , ";
		$sql .="visit6_days , ";
		$sql .="visit6_points , ";
		$sql .="visit6_money , ";
		$sql .="total6_account , ";
		$sql .="total6_days , ";
		$sql .="total6_points , ";
		$sql .="total6_money , ";
		$sql .=	"del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(診療報酬総括表（医科）)
	function update_add_up_apply_synthesis_report1($total,$apply_id,$type_factor,$type_section)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_reward_synthesis_medi set";
		$set =	array("newest_apply_id");
		array_push($set,"ent1_account	");
		array_push($set,"ent1_days	");
		array_push($set,"ent1_points	");
		array_push($set,"ent1_money	");
		array_push($set,"visit1_account	");
		array_push($set,"visit1_days	");
		array_push($set,"visit1_points	");
		array_push($set,"visit1_money	");
		array_push($set,"total1_account	");
		array_push($set,"total1_days	");
		array_push($set,"total1_points	");
		array_push($set,"total1_money	");
		array_push($set,"ent2_account	");
		array_push($set,"ent2_days	");
		array_push($set,"ent2_points	");
		array_push($set,"ent2_money	");
		array_push($set,"visit2_account	");
		array_push($set,"visit2_days	");
		array_push($set,"visit2_points	");
		array_push($set,"visit2_money	");
		array_push($set,"total2_account	");
		array_push($set,"total2_days	");
		array_push($set,"total2_points	");
		array_push($set,"total2_money	");
		array_push($set,"ent3_account	");
		array_push($set,"ent3_days	");
		array_push($set,"ent3_points	");
		array_push($set,"ent3_money	");
		array_push($set,"visit3_account	");
		array_push($set,"visit3_days	");
		array_push($set,"visit3_points	");
		array_push($set,"visit3_money	");
		array_push($set,"total3_account	");
		array_push($set,"total3_days	");
		array_push($set,"total3_points	");
		array_push($set,"total3_money	");
		array_push($set,"ent4_account	");
		array_push($set,"ent4_days	");
		array_push($set,"ent4_points	");
		array_push($set,"ent4_money	");
		array_push($set,"visit4_account	");
		array_push($set,"visit4_days	");
		array_push($set,"visit4_points	");
		array_push($set,"visit4_money	");
		array_push($set,"total4_account	");
		array_push($set,"total4_days	");
		array_push($set,"total4_points	");
		array_push($set,"total4_money	");
		array_push($set,"ent5_account	");
		array_push($set,"ent5_days	");
		array_push($set,"ent5_points	");
		array_push($set,"ent5_money	");
		array_push($set,"visit5_account	");
		array_push($set,"visit5_days	");
		array_push($set,"visit5_points	");
		array_push($set,"visit5_money	");
		array_push($set,"total5_account	");
		array_push($set,"total5_days	");
		array_push($set,"total5_points	");
		array_push($set,"total5_money	");
		array_push($set,"ent6_account	");
		array_push($set,"ent6_days	");
		array_push($set,"ent6_points	");
		array_push($set,"ent6_money	");
		array_push($set,"visit6_account	");
		array_push($set,"visit6_days	");
		array_push($set,"visit6_points	");
		array_push($set,"visit6_money	");
		array_push($set,"total6_account	");
		array_push($set,"total6_days	");
		array_push($set,"total6_points	");
		array_push($set,"total6_money	");
		$cond = "where newest_apply_id = $apply_id and type_factor=$type_factor and type_section=$type_section";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}




	// 集計用データの登録処理(診療報酬総括表（歯科） main)
	function regist_add_up_apply_synthesis_dental_main($total)
	{

		$sql=	"insert into jnl_reward_synthesis_dental_main (";
		$sql .=	"newest_apply_id , ";
		$sql .=	"emp_id , ";
		$sql .=	"jnl_facility_id , ";
		$sql .=	"jnl_status , ";
		$sql .=	"regist_date , ";
		$sql .=	"del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(診療報酬総括表（歯科） main)
	function update_add_up_apply_synthesis_dental_main($total,$apply_id)
	{

		$sql=	"update  jnl_reward_synthesis_dental_main set";
		$set =	array("newest_apply_id");
		array_push($set,"emp_id");
		array_push($set,"jnl_status");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(診療報酬総括表（歯科）)
	function regist_add_up_apply_synthesis_report2($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_reward_synthesis_dental (";
		$sql .="newest_apply_id , ";
		$sql .="type_section , ";
		$sql .="type_factor , ";
		$sql .="ent1_account , ";
		$sql .="ent1_days , ";
		$sql .="ent1_points , ";
		$sql .="ent1_money , ";
		$sql .="visit1_account , ";
		$sql .="visit1_days , ";
		$sql .="visit1_points , ";
		$sql .="visit1_money , ";
		$sql .="total1_account , ";
		$sql .="total1_days , ";
		$sql .="total1_points , ";
		$sql .="total1_money , ";
		$sql .="ent2_account , ";
		$sql .="ent2_days , ";
		$sql .="ent2_points , ";
		$sql .="ent2_money , ";
		$sql .="visit2_account , ";
		$sql .="visit2_days , ";
		$sql .="visit2_points , ";
		$sql .="visit2_money , ";
		$sql .="total2_account , ";
		$sql .="total2_days , ";
		$sql .="total2_points , ";
		$sql .="total2_money , ";
		$sql .="ent3_account , ";
		$sql .="ent3_days , ";
		$sql .="ent3_points , ";
		$sql .="ent3_money , ";
		$sql .="visit3_account , ";
		$sql .="visit3_days , ";
		$sql .="visit3_points , ";
		$sql .="visit3_money , ";
		$sql .="total3_account , ";
		$sql .="total3_days , ";
		$sql .="total3_points , ";
		$sql .="total3_money , ";
		$sql .="ent4_account , ";
		$sql .="ent4_days , ";
		$sql .="ent4_points , ";
		$sql .="ent4_money , ";
		$sql .="visit4_account , ";
		$sql .="visit4_days , ";
		$sql .="visit4_points , ";
		$sql .="visit4_money , ";
		$sql .="total4_account , ";
		$sql .="total4_days , ";
		$sql .="total4_points , ";
		$sql .="total4_money , ";
		$sql .="ent5_account , ";
		$sql .="ent5_days , ";
		$sql .="ent5_points , ";
		$sql .="ent5_money , ";
		$sql .="visit5_account , ";
		$sql .="visit5_days , ";
		$sql .="visit5_points , ";
		$sql .="visit5_money , ";
		$sql .="total5_account , ";
		$sql .="total5_days , ";
		$sql .="total5_points , ";
		$sql .="total5_money , ";
		$sql .="ent6_account , ";
		$sql .="ent6_days , ";
		$sql .="ent6_points , ";
		$sql .="ent6_money , ";
		$sql .="visit6_account , ";
		$sql .="visit6_days , ";
		$sql .="visit6_points , ";
		$sql .="visit6_money , ";
		$sql .="total6_account , ";
		$sql .="total6_days , ";
		$sql .="total6_points , ";
		$sql .="total6_money , ";
		$sql .=	"del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(診療報酬総括表（歯科）)
	function update_add_up_apply_synthesis_report2($total,$apply_id,$type_factor,$type_section)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_reward_synthesis_dental set";
		$set =	array("newest_apply_id");
		array_push($set,"ent1_account	");
		array_push($set,"ent1_days	");
		array_push($set,"ent1_points	");
		array_push($set,"ent1_money	");
		array_push($set,"visit1_account	");
		array_push($set,"visit1_days	");
		array_push($set,"visit1_points	");
		array_push($set,"visit1_money	");
		array_push($set,"total1_account	");
		array_push($set,"total1_days	");
		array_push($set,"total1_points	");
		array_push($set,"total1_money	");
		array_push($set,"ent2_account	");
		array_push($set,"ent2_days	");
		array_push($set,"ent2_points	");
		array_push($set,"ent2_money	");
		array_push($set,"visit2_account	");
		array_push($set,"visit2_days	");
		array_push($set,"visit2_points	");
		array_push($set,"visit2_money	");
		array_push($set,"total2_account	");
		array_push($set,"total2_days	");
		array_push($set,"total2_points	");
		array_push($set,"total2_money	");
		array_push($set,"ent3_account	");
		array_push($set,"ent3_days	");
		array_push($set,"ent3_points	");
		array_push($set,"ent3_money	");
		array_push($set,"visit3_account	");
		array_push($set,"visit3_days	");
		array_push($set,"visit3_points	");
		array_push($set,"visit3_money	");
		array_push($set,"total3_account	");
		array_push($set,"total3_days	");
		array_push($set,"total3_points	");
		array_push($set,"total3_money	");
		array_push($set,"ent4_account	");
		array_push($set,"ent4_days	");
		array_push($set,"ent4_points	");
		array_push($set,"ent4_money	");
		array_push($set,"visit4_account	");
		array_push($set,"visit4_days	");
		array_push($set,"visit4_points	");
		array_push($set,"visit4_money	");
		array_push($set,"total4_account	");
		array_push($set,"total4_days	");
		array_push($set,"total4_points	");
		array_push($set,"total4_money	");
		array_push($set,"ent5_account	");
		array_push($set,"ent5_days	");
		array_push($set,"ent5_points	");
		array_push($set,"ent5_money	");
		array_push($set,"visit5_account	");
		array_push($set,"visit5_days	");
		array_push($set,"visit5_points	");
		array_push($set,"visit5_money	");
		array_push($set,"total5_account	");
		array_push($set,"total5_days	");
		array_push($set,"total5_points	");
		array_push($set,"total5_money	");
		array_push($set,"ent6_account	");
		array_push($set,"ent6_days	");
		array_push($set,"ent6_points	");
		array_push($set,"ent6_money	");
		array_push($set,"visit6_account	");
		array_push($set,"visit6_days	");
		array_push($set,"visit6_points	");
		array_push($set,"visit6_money	");
		array_push($set,"total6_account	");
		array_push($set,"total6_days	");
		array_push($set,"total6_points	");
		array_push($set,"total6_money	");
		$cond = "where newest_apply_id = $apply_id and type_factor=$type_factor and type_section=$type_section";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(入院診療行為別明細 main)
	function regist_add_up_apply_diagnosis_enter_main($total)
	{

		$sql=	"insert into jnl_bill_diagnosis_enter_main (";
		$sql .=	"newest_apply_id , ";
		$sql .=	"emp_id , ";
		$sql .=	"jnl_facility_id , ";
		$sql .=	"jnl_status , ";
		$sql .=	"regist_date , ";
		$sql .=	"del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(入院診療行為別明細 main)
	function update_add_up_apply_diagnosis_enter_main($total,$apply_id)
	{

		$sql=	"update  jnl_bill_diagnosis_enter_main set";
		$set =	array("newest_apply_id");
		array_push($set,"emp_id");
		array_push($set,"jnl_status");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}



	// 集計用データの登録処理(入院診療行為別明細)
	function regist_add_up_apply_diagnosis_enter_report1($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_bill_diagnosis_enter (";
		$sql .="newest_apply_id, ";
		$sql .="type_section , ";
		$sql .="account , ";
		$sql .="days , ";
		$sql .="first_diagnosis, ";
		$sql .="guidance, ";
		$sql .="medicine, ";
		$sql .="injection, ";
		$sql .="treatment, ";
		$sql .="anesthesia, ";
		$sql .="inspection, ";
		$sql .="x_ray, ";
		$sql .="rehab, ";
		$sql .="fee_etc, ";
		$sql .="hospitalization, ";
		$sql .="dpc_all_assess, ";
		$sql .="meal_treat, ";
		$sql .="fee_total, ";
		$sql .="del_flg ";
		$sql .=	") values (";
		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(入院診療行為別明細)
	function update_add_up_apply_diagnosis_enter_report1($total,$apply_id,$type_section)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_bill_diagnosis_enter set";
		$set =	array("newest_apply_id");
		array_push($set,"account");
		array_push($set,"days");
		array_push($set,"first_diagnosis");
		array_push($set,"guidance");
		array_push($set,"medicine");
		array_push($set,"injection");
		array_push($set,"treatment");
		array_push($set,"anesthesia");
		array_push($set,"inspection");
		array_push($set,"x_ray");
		array_push($set,"rehab");
		array_push($set,"fee_etc");
		array_push($set,"hospitalization");
		array_push($set,"dpc_all_assess");
		array_push($set,"meal_treat");
		array_push($set,"fee_total");
		$cond = "where newest_apply_id = $apply_id and type_section=$type_section";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの登録処理(外来診療行為別明細 main)
	function regist_add_up_apply_diagnosis_visit_main($total)
	{

		$sql=	"insert into jnl_bill_diagnosis_visit_main (";
		$sql .=	"newest_apply_id , ";
		$sql .=	"emp_id , ";
		$sql .=	"jnl_facility_id , ";
		$sql .=	"jnl_status , ";
		$sql .=	"regist_date , ";
		$sql .=	"del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(外来診療行為別明細 main)
	function update_add_up_apply_diagnosis_visit_main($total,$apply_id)
	{

		$sql=	"update  jnl_bill_diagnosis_visit_main set";
		$set =	array("newest_apply_id");
		array_push($set,"emp_id");
		array_push($set,"jnl_status");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}



	// 集計用データの登録処理(外来診療行為別明細)
	function regist_add_up_apply_diagnosis_visit_report1($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_bill_diagnosis_visit (";
		$sql .="newest_apply_id, ";
		$sql .="type_section , ";
		$sql .="account , ";
		$sql .="days , ";
		$sql .="first_diagnosis , ";
		$sql .="re_diagnosis, ";
		$sql .="guidance , ";
		$sql .="being_in , ";
		$sql .="in_prescription , ";
		$sql .="medication , ";
		$sql .="injection , ";
		$sql .="treatment , ";
		$sql .="anesthesia , ";
		$sql .="inspection , ";
		$sql .="x_ray , ";
		$sql .="rehab , ";
		$sql .="prescription , ";
		$sql .="fee_etc	, ";
		$sql .="fee_total , ";
		$sql .=	"del_flg ";
		$sql .=	") values (";
		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(外来診療行為別明細)
	function update_add_up_apply_diagnosis_visit_report1($total,$apply_id,$type_section)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_bill_diagnosis_visit set";
		$set =	array("newest_apply_id");
		array_push($set,"account");
		array_push($set,"days");
		array_push($set,"first_diagnosis");
		array_push($set,"re_diagnosis");
		array_push($set,"guidance");
		array_push($set,"being_in");
		array_push($set,"in_prescription");
		array_push($set,"medication	");
		array_push($set,"injection");
		array_push($set,"treatment");
		array_push($set,"anesthesia");
		array_push($set,"inspection");
		array_push($set,"x_ray");
		array_push($set,"rehab");
		array_push($set,"prescription");
		array_push($set,"fee_etc");
		array_push($set,"fee_total");
		$cond = "where newest_apply_id = $apply_id and type_section=$type_section";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(療養費明細書 main)
	function regist_add_up_apply_bill_recuperation_main($total)
	{

		$sql=	"insert into jnl_bill_recuperation_main (";
		$sql .=	"newest_apply_id , ";
		$sql .=	"emp_id , ";
		$sql .=	"jnl_facility_id , ";
		$sql .=	"jnl_status , ";
		$sql .=	"regist_date , ";
		$sql .=	"del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(療養費明細書 main)
	function update_add_up_apply_bill_recuperation_main($total,$apply_id)
	{

		$sql=	"update jnl_bill_recuperation_main set";
		$set =	array("newest_apply_id");
		array_push($set,"emp_id");
		array_push($set,"jnl_status");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(療養費明細書-保険請求分、訪問リハビリ)
	function regist_add_up_apply_bill_recuperation_report1($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_bill_recuperation_insure_rehab (";
		$sql .="newest_apply_id, ";
		$sql .="type_section, ";
		$sql .="total_account, ";
		$sql .="total_days, ";
		$sql .="total_price, ";
		$sql .="total_ins_charge, ";
		$sql .="total_pblc_charge, ";
		$sql .="total_own_charge, ";
		$sql .="total_cost, ";
		$sql .="one_account, ";
		$sql .="one_days, ";
		$sql .="one_price, ";
		$sql .="one_ins_charge, ";
		$sql .="one_pblc_charge, ";
		$sql .="one_own_charge, ";
		$sql .="one_cost, ";
		$sql .="two_account, ";
		$sql .="two_days, ";
		$sql .="two_price, ";
		$sql .="two_ins_charge, ";
		$sql .="two_pblc_charge, ";
		$sql .="two_own_charge, ";
		$sql .="two_cost, ";
		$sql .="three_account, ";
		$sql .="three_days, ";
		$sql .="three_price, ";
		$sql .="three_ins_charge, ";
		$sql .="three_pblc_charge, ";
		$sql .="three_own_charge, ";
		$sql .="three_cost, ";
		$sql .="empty_one_one, ";
		$sql .="empty_one_two, ";
		$sql .="four_account, ";
		$sql .="four_days, ";
		$sql .="four_price, ";
		$sql .="four_ins_charge, ";
		$sql .="four_pblc_charge, ";
		$sql .="four_own_charge, ";
		$sql .="four_cost, ";
		$sql .="empty_two_one, ";
		$sql .="empty_two_two, ";
		$sql .="del_flg ";
		$sql .=	") values (";
		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(療養費明細書-保険請求分、訪問リハビリ)
	function update_add_up_apply_bill_recuperation_report1($total,$apply_id,$type_section)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update  jnl_bill_recuperation_insure_rehab set";
		$set =	array("newest_apply_id");
		array_push($set,"total_account");
		array_push($set,"total_days");
		array_push($set,"total_price");
		array_push($set,"total_ins_charge");
		array_push($set,"total_pblc_charge");
		array_push($set,"total_own_charge");
		array_push($set,"total_cost");
		array_push($set,"one_account");
		array_push($set,"one_days");
		array_push($set,"one_price");
		array_push($set,"one_ins_charge");
		array_push($set,"one_pblc_charge");
		array_push($set,"one_own_charge");
		array_push($set,"one_cost");
		array_push($set,"two_account");
		array_push($set,"two_days");
		array_push($set,"two_price");
		array_push($set,"two_ins_charge");
		array_push($set,"two_pblc_charge");
		array_push($set,"two_own_charge");
		array_push($set,"two_cost");
		array_push($set,"three_account");
		array_push($set,"three_days");
		array_push($set,"three_price");
		array_push($set,"three_ins_charge");
		array_push($set,"three_pblc_charge");
		array_push($set,"three_own_charge");
		array_push($set,"three_cost");
		array_push($set,"empty_one_one");
		array_push($set,"empty_one_two");
		array_push($set,"four_account");
		array_push($set,"four_days");
		array_push($set,"four_price");
		array_push($set,"four_ins_charge");
		array_push($set,"four_pblc_charge");
		array_push($set,"four_own_charge");
		array_push($set,"four_cost");
		array_push($set,"empty_two_one");
		array_push($set,"empty_two_two");
		$cond = "where newest_apply_id = $apply_id and type_section=$type_section";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(療養費明細書-特定入所者介護サービス費)
	function regist_add_up_apply_bill_recuperation_report2($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_bill_recuperation_service (";
		$sql .="newest_apply_id, ";
		$sql .="type_section, ";
		$sql .="type_kind, ";
		$sql .="short_house_acount, ";
		$sql .="short_house_days, ";
		$sql .="short_house_cost, ";
		$sql .="short_house_ins, ";
		$sql .="short_meal_acount, ";
		$sql .="short_meal_days, ";
		$sql .="short_meal_cost, ";
		$sql .="short_meal_ins, ";
		$sql .="enter_house_acount, ";
		$sql .="enter_house_days, ";
		$sql .="enter_house_cost, ";
		$sql .="enter_house_ins, ";
		$sql .="enter_meal_acount, ";
		$sql .="enter_meal_days, ";
		$sql .="enter_meal_cost, ";
		$sql .="enter_meal_ins, ";
		$sql .="total_house_acount, ";
		$sql .="total_house_days, ";
		$sql .="total_house_cost, ";
		$sql .="total_house_ins, ";
		$sql .="total_meal_acount, ";
		$sql .="total_meal_days, ";
		$sql .="total_meal_cost, ";
		$sql .="total_meal_ins, ";
		$sql .="del_flg ";
		$sql .=	") values (";
		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(療養費明細書-特定入所者介護サービス費)
	function update_add_up_apply_bill_recuperation_report2($total,$apply_id,$type_section,$type_kind)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update jnl_bill_recuperation_service set";
		$set =	array("newest_apply_id");
		array_push($set,"short_house_acount");
		array_push($set,"short_house_days");
		array_push($set,"short_house_cost");
		array_push($set,"short_house_ins");
		array_push($set,"short_meal_acount");
		array_push($set,"short_meal_days");
		array_push($set,"short_meal_cost");
		array_push($set,"short_meal_ins");
		array_push($set,"enter_house_acount");
		array_push($set,"enter_house_days");
		array_push($set,"enter_house_cost");
		array_push($set,"enter_house_ins");
		array_push($set,"enter_meal_acount");
		array_push($set,"enter_meal_days");
		array_push($set,"enter_meal_cost");
		array_push($set,"enter_meal_ins");
		array_push($set,"total_house_acount");
		array_push($set,"total_house_days");
		array_push($set,"total_house_cost");
		array_push($set,"total_house_ins");
		array_push($set,"total_meal_acount");
		array_push($set,"total_meal_days");
		array_push($set,"total_meal_cost");
		array_push($set,"total_meal_ins");
		$cond = "where newest_apply_id = $apply_id and type_section=$type_section and type_kind=$type_kind";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(療養費明細書-自己負担分明細書)
	function regist_add_up_apply_bill_recuperation_report3($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_bill_recuperation_own_payment (";
		$sql .="newest_apply_id, ";
		$sql .="type_section, ";
		$sql .="type_kind, ";
		$sql .="acount, ";
		$sql .="days, ";
		$sql .="money, ";
		$sql .="del_flg ";
		$sql .=	") values (";
		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの更新処理(療養費明細書-自己負担分明細書)
	function update_add_up_apply_bill_recuperation_report3($total,$apply_id,$type_section,$type_kind)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update jnl_bill_recuperation_own_payment set";
		$set =	array("newest_apply_id");
		array_push($set,"acount");
		array_push($set,"days");
		array_push($set,"money");
		$cond = "where newest_apply_id = $apply_id and type_section=$type_section and type_kind=$type_kind";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの登録処理(療養費明細書-その他)
	function regist_add_up_apply_bill_recuperation_report4($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_bill_recuperation_etc (";
		$sql .="newest_apply_id, ";
		$sql .="support_total_acount, ";
		$sql .="support_total_price, ";
		$sql .="support_total_amount, ";
		$sql .="support_now_acount, ";
		$sql .="support_now_price, ";
		$sql .="support_now_amount, ";
		$sql .="support_delay_acount, ";
		$sql .="support_delay_price, ";
		$sql .="support_delay_amount, ";
		$sql .="support_return_acount, ";
		$sql .="support_return_price, ";
		$sql .="support_return_amount, ";
		$sql .="rate_date_total_low_acount, ";
		$sql .="rate_date_total_low_days, ";
		$sql .="rate_date_total_total_days, ";
		$sql .="rate_date_total_rate, ";
		$sql .="rate_date_enter_low_acount, ";
		$sql .="rate_date_enter_low_days, ";
		$sql .="rate_date_enter_total_days, ";
		$sql .="rate_date_enter_rate, ";
		$sql .="rate_date_short_low_acount, ";
		$sql .="rate_date_short_low_days, ";
		$sql .="rate_date_short_total_days, ";
		$sql .="rate_date_short_rate, ";
		$sql .="all_total_charge, ";
		$sql .="ins_ins_total_charge, ";
		$sql .="specific_enter_total_charge, ";
		$sql .="ins_rehab_total_charge, ";
		$sql .="improvement_grant, ";
		$sql .="del_flg ";
		$sql .=	") values (";
		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(療養費明細書-その他)
	function update_add_up_apply_bill_recuperation_report4($total,$apply_id)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update jnl_bill_recuperation_etc set";
		$set =	array("newest_apply_id");
		array_push($set,"support_total_acount");
		array_push($set,"support_total_price");
		array_push($set,"support_total_amount");
		array_push($set,"support_now_acount");
		array_push($set,"support_now_price");
		array_push($set,"support_now_amount");
		array_push($set,"support_delay_acount");
		array_push($set,"support_delay_price");
		array_push($set,"support_delay_amount");
		array_push($set,"support_return_acount");
		array_push($set,"support_return_price");
		array_push($set,"support_return_amount");
		array_push($set,"rate_date_total_low_acount");
		array_push($set,"rate_date_total_low_days");
		array_push($set,"rate_date_total_total_days");
		array_push($set,"rate_date_total_rate");
		array_push($set,"rate_date_enter_low_acount");
		array_push($set,"rate_date_enter_low_days");
		array_push($set,"rate_date_enter_total_days");
		array_push($set,"rate_date_enter_rate");
		array_push($set,"rate_date_short_low_acount");
		array_push($set,"rate_date_short_low_days");
		array_push($set,"rate_date_short_total_days");
		array_push($set,"rate_date_short_rate");
		array_push($set,"all_total_charge");
		array_push($set,"ins_ins_total_charge");
		array_push($set,"specific_enter_total_charge");
		array_push($set,"ins_rehab_total_charge");
		array_push($set,"improvement_grant");
		$cond = "where newest_apply_id = $apply_id ";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(予防給付療養費明細書 main)regist_add_up_apply_bill_recuperation_repay_main
	function regist_add_up_apply_bill_recuperation_prepay_main($total)
	{

		$sql=	"insert into jnl_bill_recuperation_prepay_main (";
		$sql .=	"newest_apply_id , ";
		$sql .=	"emp_id , ";
		$sql .=	"jnl_facility_id , ";
		$sql .=	"jnl_status , ";
		$sql .=	"regist_date , ";
		$sql .=	"del_flg ";
		$sql .=	") values (";

		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(予防給付療養費明細書 main)
	function update_add_up_apply_bill_recuperation_prepay_main($total,$apply_id)
	{

		$sql=	"update jnl_bill_recuperation_prepay_main set";
		$set =	array("newest_apply_id");
		array_push($set,"emp_id");
		array_push($set,"jnl_status");
		$cond = "where newest_apply_id = $apply_id";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(予防給付療養費明細書-保険請求分、訪問リハビリ)
	function regist_add_up_apply_bill_recuperation_prepay_report1($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_bill_recuperation_insure_rehab_prepay (";
		$sql .="newest_apply_id, ";
		$sql .="type_section, ";
		$sql .="total_account, ";
		$sql .="total_days, ";
		$sql .="total_price, ";
		$sql .="total_ins_charge, ";
		$sql .="total_pblc_charge, ";
		$sql .="total_own_charge, ";
		$sql .="total_cost, ";
		$sql .="one_account, ";
		$sql .="one_days, ";
		$sql .="one_price, ";
		$sql .="one_ins_charge, ";
		$sql .="one_pblc_charge, ";
		$sql .="one_own_charge, ";
		$sql .="one_cost, ";
		$sql .="two_account, ";
		$sql .="two_days, ";
		$sql .="two_price, ";
		$sql .="two_ins_charge, ";
		$sql .="two_pblc_charge, ";
		$sql .="two_own_charge, ";
		$sql .="two_cost, ";
		$sql .="three_account, ";
		$sql .="three_days, ";
		$sql .="three_price, ";
		$sql .="three_ins_charge, ";
		$sql .="three_pblc_charge, ";
		$sql .="three_own_charge, ";
		$sql .="three_cost, ";
		$sql .="empty_one_one, ";
		$sql .="empty_one_two, ";
		$sql .="four_account, ";
		$sql .="four_days, ";
		$sql .="four_price, ";
		$sql .="four_ins_charge, ";
		$sql .="four_pblc_charge, ";
		$sql .="four_own_charge, ";
		$sql .="four_cost, ";
		$sql .="empty_two_one, ";
		$sql .="empty_two_two, ";
		$sql .="del_flg ";
		$sql .=	") values (";
		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(予防給付療養費明細書-保険請求分、訪問リハビリ)
	function update_add_up_apply_bill_recuperation_prepay_report1($total,$apply_id,$type_section)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update jnl_bill_recuperation_insure_rehab_prepay set";
		$set =	array("newest_apply_id");
		array_push($set,"total_account");
		array_push($set,"total_days");
		array_push($set,"total_price");
		array_push($set,"total_ins_charge");
		array_push($set,"total_pblc_charge");
		array_push($set,"total_own_charge");
		array_push($set,"total_cost");
		array_push($set,"one_account");
		array_push($set,"one_days");
		array_push($set,"one_price");
		array_push($set,"one_ins_charge");
		array_push($set,"one_pblc_charge");
		array_push($set,"one_own_charge");
		array_push($set,"one_cost");
		array_push($set,"two_account");
		array_push($set,"two_days");
		array_push($set,"two_price");
		array_push($set,"two_ins_charge");
		array_push($set,"two_pblc_charge");
		array_push($set,"two_own_charge");
		array_push($set,"two_cost");
		array_push($set,"three_account");
		array_push($set,"three_days");
		array_push($set,"three_price");
		array_push($set,"three_ins_charge");
		array_push($set,"three_pblc_charge");
		array_push($set,"three_own_charge");
		array_push($set,"three_cost");
		array_push($set,"empty_one_one");
		array_push($set,"empty_one_two");
		array_push($set,"four_account");
		array_push($set,"four_days");
		array_push($set,"four_price");
		array_push($set,"four_ins_charge");
		array_push($set,"four_pblc_charge");
		array_push($set,"four_own_charge");
		array_push($set,"four_cost");
		array_push($set,"empty_two_one");
		array_push($set,"empty_two_two");
		$cond = "where newest_apply_id = $apply_id and type_section=$type_section";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(予防給付療養費明細書-特定入所者介護サービス費)
	function regist_add_up_apply_bill_recuperation_prepay_report2($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_bill_recuperation_service_prepay (";
		$sql .="newest_apply_id, ";
		$sql .="type_section, ";
		$sql .="type_kind, ";
		$sql .="house_acount, ";
		$sql .="house_days, ";
		$sql .="house_cost, ";
		$sql .="house_ins, ";
		$sql .="meal_acount, ";
		$sql .="meal_days, ";
		$sql .="meal_cost, ";
		$sql .="meal_ins, ";
		$sql .="del_flg ";
		$sql .=	") values (";
		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(予防給付療養費明細書-特定入所者介護サービス費)
	function update_add_up_apply_bill_recuperation_prepay_report2($total,$apply_id,$type_section,$type_kind)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update jnl_bill_recuperation_service_prepay set";
		$set =	array("newest_apply_id");
		array_push($set,"house_acount");
		array_push($set,"house_days");
		array_push($set,"house_cost");
		array_push($set,"house_ins");
		array_push($set,"meal_acount");
		array_push($set,"meal_days");
		array_push($set,"meal_cost");
		array_push($set,"meal_ins");
		$cond = "where newest_apply_id = $apply_id and type_section=$type_section and type_kind=$type_kind";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの登録処理(予防給付療養費明細書-自己負担分明細書)
	function regist_add_up_apply_bill_recuperation_prepay_report3($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_bill_recuperation_own_payment_prepay (";
		$sql .="newest_apply_id, ";
		$sql .="type_section, ";
		$sql .="type_kind, ";
		$sql .="acount, ";
		$sql .="days, ";
		$sql .="money, ";
		$sql .="del_flg ";
		$sql .=	") values (";
		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの更新処理(予防給付療養費明細書-自己負担分明細書)
	function update_add_up_apply_bill_recuperation_prepay_report3($total,$apply_id,$type_section,$type_kind)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update jnl_bill_recuperation_own_payment_prepay set";
		$set =	array("newest_apply_id");
		array_push($set,"acount");
		array_push($set,"days");
		array_push($set,"money");
		$cond = "where newest_apply_id = $apply_id and type_section=$type_section and type_kind=$type_kind";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 集計用データの登録処理(予防給付療養費明細書-その他)
	function regist_add_up_apply_bill_recuperation_prepay_report4($total)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"insert into jnl_bill_recuperation_etc_prepay (";
		$sql .="newest_apply_id, ";
		$sql .="support_total_acount, ";
		$sql .="support_total_price, ";
		$sql .="support_total_amount, ";
		$sql .="support_now_acount, ";
		$sql .="support_now_price, ";
		$sql .="support_now_amount, ";
		$sql .="support_delay_acount, ";
		$sql .="support_delay_price, ";
		$sql .="support_delay_amount, ";
		$sql .="support_return_acount, ";
		$sql .="support_return_price, ";
		$sql .="support_return_amount, ";
		$sql .="rate_date_total_low_acount, ";
		$sql .="rate_date_total_low_days, ";
		$sql .="rate_date_total_total_days, ";
		$sql .="rate_date_total_rate, ";
		$sql .="rate_date_short_low_acount, ";
		$sql .="rate_date_short_low_days, ";
		$sql .="rate_date_short_total_days, ";
		$sql .="rate_date_short_rate, ";
		$sql .="all_total_charge, ";
		$sql .="ins_ins_total_charge, ";
		$sql .="specific_enter_total_charge, ";
		$sql .="ins_rehab_total_charge, ";
		$sql .="improvement_grant, ";
		$sql .="del_flg ";
		$sql .=	") values (";
		$content = $total;

		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 集計用データの更新処理(予防給付療養費明細書-その他)
	function update_add_up_apply_bill_recuperation_prepay_report4($total,$apply_id)
	{

		//登録直前に入力されているカンマを削除する
		$total = $this->del_comma_func($total);

		$sql=	"update jnl_bill_recuperation_etc_prepay set";
		$set =	array("newest_apply_id");
		array_push($set,"support_total_acount");
		array_push($set,"support_total_price");
		array_push($set,"support_total_amount");
		array_push($set,"support_now_acount");
		array_push($set,"support_now_price");
		array_push($set,"support_now_amount");
		array_push($set,"support_delay_acount");
		array_push($set,"support_delay_price");
		array_push($set,"support_delay_amount");
		array_push($set,"support_return_acount");
		array_push($set,"support_return_price");
		array_push($set,"support_return_amount");
		array_push($set,"rate_date_total_low_acount");
		array_push($set,"rate_date_total_low_days");
		array_push($set,"rate_date_total_total_days");
		array_push($set,"rate_date_total_rate");
		array_push($set,"rate_date_short_low_acount");
		array_push($set,"rate_date_short_low_days");
		array_push($set,"rate_date_short_total_days");
		array_push($set,"rate_date_short_rate");
		array_push($set,"all_total_charge");
		array_push($set,"ins_ins_total_charge");
		array_push($set,"specific_enter_total_charge");
		array_push($set,"ins_rehab_total_charge");
		array_push($set,"improvement_grant");
		$cond = "where newest_apply_id = $apply_id ";
		$setvalue = $total;
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}





























	// 承認登録
	function regist_applyapv($arr)
	{
		$wkfw_id          = $arr["wkfw_id"];
		$apply_id         = $arr["apply_id"];
		$apv_order        = $arr["apv_order"];
		$emp_id           = $arr["emp_id"];
		$apv_stat         = $arr["apv_stat"];
		$apv_date         = $arr["apv_date"];
		$delete_flg       = $arr["delete_flg"];
		$apv_comment      = $arr["apv_comment"];
		$st_div           = $arr["st_div"];
		$deci_flg         = $arr["deci_flg"];
		$emp_class        = $arr["emp_class"];
		$emp_attribute    = $arr["emp_attribute"];
		$emp_dept         = $arr["emp_dept"];
		$emp_st           = $arr["emp_st"];
		$apv_fix_show_flg = $arr["apv_fix_show_flg"];
		$emp_room         = $arr["emp_room"];
		$apv_sub_order    = $arr["apv_sub_order"];
		$multi_apv_flg    = $arr["multi_apv_flg"];
		$next_notice_div  = $arr["next_notice_div"];
		$parent_pjt_id    = $arr["parent_pjt_id"];
		$child_pjt_id     = $arr["child_pjt_id"];
		$other_apv_flg    = $arr["other_apv_flg"];

		$sql  = "insert into jnl_applyapv ";
		$sql .= "(wkfw_id, ";
		$sql .= "apply_id, ";
		$sql .= "apv_order, ";
		$sql .= "emp_id, ";
		$sql .= "apv_stat, ";
		$sql .= "apv_date, ";
		$sql .= "delete_flg, ";
		$sql .= "apv_comment, ";
		$sql .= "st_div, ";
		$sql .= "deci_flg, ";
		$sql .= "emp_class, ";
		$sql .= "emp_attribute, ";
		$sql .= "emp_dept, ";
		$sql .= "emp_st, ";
		$sql .= "apv_fix_show_flg, ";
		$sql .= "emp_room, ";
		$sql .= "apv_sub_order, ";
		$sql .= "multi_apv_flg, ";
		$sql .= "next_notice_div, ";
		$sql .= "parent_pjt_id, ";
		$sql .= "child_pjt_id, ";
		$sql .= "other_apv_flg) ";

		$sql .= "values (";

		$content = array(
							$wkfw_id,
							$apply_id,
							$apv_order,
							$emp_id,
							$apv_stat,
							$apv_date,
							$delete_flg,
							$apv_comment,
							$st_div,
							$deci_flg,
							$emp_class,
							$emp_attribute,
							$emp_dept,
							$emp_st,
							$apv_fix_show_flg,
							$emp_room,
							$apv_sub_order,
							$multi_apv_flg,
							$next_notice_div,
							$parent_pjt_id,
							$child_pjt_id,
							$other_apv_flg
						);

		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認者候補登録
	function regist_applyapvemp($apply_id, $apv_order, $person_order, $emp_id, $st_div, $parent_pjt_id, $child_pjt_id)
	{
		$sql = "insert into jnl_applyapvemp (apply_id, apv_order, person_order, emp_id, delete_flg, st_div, parent_pjt_id, child_pjt_id) values (";
		$content = array($apply_id, $apv_order, $person_order, $emp_id, "f", $st_div, $parent_pjt_id, $child_pjt_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 添付ファイル登録
	function regist_applyfile($apply_id, $applyfile_no, $applyfile_name)
	{
		$sql = "insert into jnl_applyfile (apply_id, applyfile_no, applyfile_name, delete_flg) values (";
		$content = array($apply_id, $applyfile_no, $applyfile_name, "f");
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(申請用)登録
	function regist_applyprecond($apply_id, $precond_wkfw_id, $precond_order, $precond_apply_id)
	{

		$precond_apply_id = ($precond_apply_id == "") ? null : $precond_apply_id;

		$sql = "insert into jnl_applyprecond (apply_id, precond_wkfw_id, precond_order, precond_apply_id, delete_flg) values (";
		$content = array($apply_id, $precond_wkfw_id, $precond_order, $precond_apply_id, "f");
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請結果通知登録
	function regist_applynotice($apply_id, $recv_emp_id, $rslt_ntc_div)
	{
		$sql = "insert into jnl_applynotice (apply_id, recv_emp_id, confirmed_flg, send_emp_id, send_date, delete_flg, rslt_ntc_div) values (";
		$content = array($apply_id, $recv_emp_id, "f", null, null, "f", $rslt_ntc_div);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 年度の申請件数取得
	function get_apply_cnt_per_year($year)
	{
		$this_ymd = $year."0401";
		$next_year = $year + 1;
		$next_ymd = $next_year."0331";

		$sql  = "select count(*) as cnt from jnl_apply ";
		$cond = "where substring(apply_date from 1 for 8) between '$this_ymd' and '$next_ymd' and not draft_flg";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "cnt");
	}

    function delete_applyapv($apply_id)
    {
		$sql = "delete from jnl_applyapv";
		$cond = "where apply_id = $apply_id and (emp_id is null or emp_id = '')";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
    }
//-------------------------------------------------------------------------------------------------------------------------
// 下書き・更新関連
//-------------------------------------------------------------------------------------------------------------------------

	// 申請・ワークフロー情報取得
	function get_apply_wkfwmst($apply_id)
	{
		$sql  = "select jnl_apply.*, empmst.emp_lt_nm, empmst.emp_ft_nm, ";
		$sql .= "jnl_wkfwmst.wkfw_title, jnl_wkfwmst.wkfw_folder_id, jnl_wkfwcatemst.wkfw_nm, jnl_wkfwmst.wkfw_content, short_wkfw_name, jnl_wkfwmst.approve_label, ";
		$sql .= "classmst.class_nm as apply_class_nm, atrbmst.atrb_nm as apply_atrb_nm, deptmst.dept_nm as apply_dept_nm, classroom.room_nm as apply_room_nm ";
		$sql .= "from jnl_apply ";
		$sql .= "inner join jnl_wkfwmst on jnl_apply.wkfw_id = jnl_wkfwmst.wkfw_id ";
		$sql .= "inner join jnl_wkfwcatemst on jnl_wkfwmst.wkfw_type = jnl_wkfwcatemst.wkfw_type ";
		$sql .= "inner join empmst on jnl_apply.emp_id = empmst.emp_id ";
		$sql .= "left join classmst on jnl_apply.emp_class = classmst.class_id ";
		$sql .= "left join atrbmst on jnl_apply.emp_attribute = atrbmst.atrb_id ";
		$sql .= "left join deptmst on jnl_apply.emp_dept = deptmst.dept_id ";
		$sql .= "left join classroom on jnl_apply.emp_room = classroom.room_id ";
		$cond = "where jnl_apply.apply_id = $apply_id ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}


	// 添付ファイル情報取得
	function get_applyfile($apply_id)
	{
		$sql  = "select applyfile_no, applyfile_name from jnl_applyfile";
		$cond = "where apply_id = $apply_id";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("applyfile_no" => $row["applyfile_no"], "applyfile_name" => $row["applyfile_name"]);

		}
		return $arr;
	}

	// 承認情報取得
	function get_applyapv($apply_id)
	{
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm, c.st_nm,  ";
		$sql .= "d.pjt_name as parent_pjt_name, f.pjt_name as child_pjt_name ";
		$sql .= "from jnl_applyapv a ";
		$sql .= "left join empmst b on a.emp_id = b.emp_id ";
		$sql .= "left join stmst c on a.emp_st = c.st_id ";
		$sql .= "left join project d on a.parent_pjt_id = d.pjt_id ";
		$sql .= "left join project f on a.child_pjt_id = f.pjt_id ";
		$cond = "where a.apply_id = $apply_id order by a.apv_order, a.apv_sub_order asc";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}

	// 承認者候補情報取得
	function get_applyapvemp($apply_id, $apv_order)
	{
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm, d.st_nm, c.emp_del_flg, ";
		$sql .= "f.pjt_name as parent_pjt_name, g.pjt_name as child_pjt_name ";
		$sql .= "from jnl_applyapvemp a ";
		$sql .= "inner join empmst b on a.emp_id = b.emp_id ";
		$sql .= "inner join authmst c on b.emp_id = c.emp_id ";
		$sql .= "left join stmst d on b.emp_st = d.st_id ";
		$sql .= "left join project f on a.parent_pjt_id = f.pjt_id ";
		$sql .= "left join project g on a.child_pjt_id = g.pjt_id ";
		$cond = "where a.apply_id = $apply_id and a.apv_order = $apv_order order by a.person_order asc";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}

	// 申請結果通知情報取得
	function get_applynotice($apply_id)
	{
		$sql   = "select a.*, b.emp_lt_nm, b.emp_ft_nm ";
		$sql  .= "from jnl_applynotice a ";
		$sql  .= "left join empmst b on a.recv_emp_id = b.emp_id ";
		$cond  = "where a.apply_id = $apply_id order by a.oid ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
                            "apply_id" => $row["apply_id"],
                            "recv_emp_id" => $row["recv_emp_id"],
                            "confirmed_flg" => $row["confirmed_flg"],
                            "send_emp_id" => $row["send_emp_id"],
                            "send_date" => $row["send_date"],
                            "delete_flg" => $row["delete_flg"],
                            "rslt_ntc_div" => $row["rslt_ntc_div"],
                            "emp_lt_nm" => $row["emp_lt_nm"],
                            "emp_ft_nm" => $row["emp_ft_nm"]
                           );
		}
		return $arr;
	}

	// 下書き申請取得
	function get_draft_apply($emp_id)
	{
		$sql   = "select a.apply_id, a.apply_title, a.wkfw_id, b.wkfw_title ";
		$sql  .= "from jnl_apply a ";
		$sql  .= "left join jnl_wkfwmst b on a.wkfw_id = b.wkfw_id ";
		$cond  = "where not a.delete_flg and a.draft_flg = 't' and a.emp_id = '$emp_id' order by a.apply_id";




		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("apply_id" => $row["apply_id"], "apply_title" => $row["apply_title"], "wkfw_id" => $row["wkfw_id"], "wkfw_title" => $row["wkfw_title"]);
		}

		return $arr;
	}

	// 前提とする申請書(申請用)取得
	function get_applyprecond($apply_id)
	{
		$sql   = "select ";
		$sql  .= "a.precond_wkfw_id, ";
		$sql  .= "a.precond_order, ";
		$sql  .= "a.precond_apply_id, ";
		$sql  .= "b.wkfw_title, ";
		$sql  .= "b.short_wkfw_name, ";
		$sql  .= "c.apply_title, ";
		$sql  .= "c.apply_date, ";
		$sql  .= "c.apply_no ";
		$sql  .= "from jnl_applyprecond a ";
		$sql  .= "inner join jnl_wkfwmst b on a.precond_wkfw_id = b.wkfw_id ";
		$sql  .= "left join jnl_apply c on a.precond_apply_id = c.apply_id ";
		$cond .= "where a.apply_id = $apply_id ";
		$cond .= "order by a.precond_order asc ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("precond_wkfw_id" => $row["precond_wkfw_id"],
                            "precond_order" => $row["precond_wkfw_id"],
                            "precond_apply_id" => $row["precond_apply_id"],
                            "wkfw_title" => $row["wkfw_title"],
                            "short_wkfw_name" => $row["short_wkfw_name"],
                            "apply_title" => $row["apply_title"],
                            "apply_date" => $row["apply_date"],
                            "apply_no" => $row["apply_no"]
			               );
		}

		return $arr;
	}

	// 申請更新(下書き用)
	function update_apply_draft($apply_id, $apply_content, $apply_date, $apply_title, $draft_flg, $apply_no)
	{
		$sql = "update jnl_apply set";

		if($apply_no != "")
		{
			$set = array("apply_content", "apply_date", "apply_title", "draft_flg", "apply_no");
			$setvalue = array(pg_escape_string($apply_content), $apply_date, pg_escape_string($apply_title), $draft_flg, $apply_no);
		}
		else
		{
			$set = array("apply_content", "apply_date", "apply_title", "draft_flg");
			$setvalue = array(pg_escape_string($apply_content), $apply_date, pg_escape_string($apply_title), $draft_flg);
		}
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請更新
	function update_apply($apply_id, $apply_content, $apply_title,$emp_id)
	{
		$sql = "update jnl_apply set";
		$set = array("apply_content", "apply_title","emp_id");
		$setvalue = array(pg_escape_string($apply_content), pg_escape_string($apply_title),$emp_id);
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認更新
	function update_applyapv($arr)
	{
		$apply_id       = $arr["apply_id"];
		$apv_order      = $arr["apv_order"];
		$apv_sub_order  = $arr["apv_sub_order"];
		$emp_id         = $arr["emp_id"];
		$st_div         = $arr["st_div"];
		$emp_class      = $arr["emp_class"];
		$emp_attribute  = $arr["emp_attribute"];
		$emp_dept       = $arr["emp_dept"];
		$emp_st         = $arr["emp_st"];
		$emp_room       = $arr["emp_room"];
		$parent_pjt_id  = $arr["parent_pjt_id"];
		$child_pjt_id   = $arr["child_pjt_id"];

		$sql = "update jnl_applyapv set";
		$set = array("emp_id", "st_div", "emp_class", "emp_attribute", "emp_dept", "emp_st", "emp_room", "parent_pjt_id", "child_pjt_id");
		$setvalue = array($emp_id, $st_div, $emp_class, $emp_attribute, $emp_dept, $emp_st, $emp_room, $parent_pjt_id, $child_pjt_id);
		$cond = "where apply_id = $apply_id and apv_order = $apv_order ";

		if($apv_sub_order != "")
		{
			$cond .= "and apv_sub_order = $apv_sub_order";
		}

		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 添付ファイル削除
	function delete_applyfile($apply_id)
	{
		$sql = "delete from jnl_applyfile";
		$cond = "where apply_id = $apply_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(申請用)更新
	function update_applyprecond($apply_id, $precond_wkfw_id, $precond_order, $precond_apply_id)
	{
		$precond_apply_id = ($precond_apply_id == "") ? null : $precond_apply_id;

		$sql = "update jnl_applyprecond set";
		$set = array("precond_apply_id");
		$setvalue = array($precond_apply_id);
		$cond = "where apply_id = $apply_id and precond_wkfw_id = $precond_wkfw_id and precond_order = $precond_order ";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請結果通知削除
	function delete_applynotice($apply_id)
	{
		$sql = "delete from jnl_applynotice";
		$cond = "where apply_id = $apply_id";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// 申請承認関連(論理削除)
//-------------------------------------------------------------------------------------------------------------------------
	// 申請論理削除
	function update_delflg_apply($apply_id, $delete_flg)
	{
		$sql = "update jnl_apply set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認論理削除
	function update_delflg_applyapv($apply_id, $delete_flg)
	{
		$sql = "update jnl_applyapv set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認候補論理削除
	function update_delflg_applyapvemp($apply_id, $delete_flg)
	{
		$sql = "update jnl_applyapvemp set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 添付ファイル論理削除
	function update_delflg_applyfile($apply_id, $delete_flg)
	{
		$sql = "update jnl_applyfile set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 非同期・同期受信論理削除
	function update_delflg_applyasyncrecv($apply_id, $delete_flg)
	{
		$sql = "update jnl_applyasyncrecv set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請結果通知論理削除
	function update_delflg_applynotice($apply_id, $delete_flg)
	{
		$sql = "update jnl_applynotice set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(申請用)論理削除
	function update_delflg_applyprecond($apply_id, $delete_flg)
	{
		$sql = "update jnl_applyprecond set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = $apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}




	// 集計データの各種テーブルのうちどのテーブルに対象データが入っているか調査してそのテーブル名を返却する
	function get_update_report_table($apply_id)
	{

		//返却する集計データのテーブル名
		$return_table_name = "";

		$sql   = "select a.short_wkfw_name from jnl_wkfwmst a, jnl_apply b ";
		$cond .= "where b.apply_id = $apply_id and a.wkfw_id = b.wkfw_id  ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$wkfw_name = pg_fetch_result($sel, 0, "short_wkfw_name");


		if($wkfw_name == "0001")
		{
			//病院日報
			$return_table_name = "jnl_hospital_day";
			//処理終了
			return $return_table_name;
		}
		elseif($wkfw_name == "0002")
		{
			//病院月報（行為件数）
			$return_table_name = "jnl_hospital_month_action";
			//処理終了
			return $return_table_name;
		}
		elseif($wkfw_name == "0003")
		{
			//病院月報（患者データ）
			$return_table_name = "jnl_hospital_month_patient";
			//処理終了
			return $return_table_name;
		}
		elseif($wkfw_name == "0004")
		{
			//老健日報
			$return_table_name = "jnl_nurse_home_main";
			//処理終了
			return $return_table_name;
		}
		elseif($wkfw_name == "0005")
		{
			//老健月報
			$return_table_name = "jnl_nurse_home_main";
			//処理終了
			return $return_table_name;
		}
		elseif($wkfw_name == "0006")
		{
			//返戻月報
			$return_table_name = "jnl_return_main";
			//処理終了
			return $return_table_name;
		}
		elseif($wkfw_name == "0007")
		{
			//診療報酬総括表（医科）
			$return_table_name = "jnl_reward_synthesis_medi_main";
			//処理終了
			return $return_table_name;
		}
		elseif($wkfw_name == "0008")
		{
			//診療報酬総括表（歯科）
			$return_table_name = "jnl_reward_synthesis_dental_main";
			//処理終了
			return $return_table_name;
		}
		elseif($wkfw_name == "0009")
		{
			//入院診療行為別明細
			$return_table_name = "jnl_bill_diagnosis_enter_main";
			//処理終了
			return $return_table_name;
		}
		elseif($wkfw_name == "0010")
		{
			//外来診療行為別明細
			$return_table_name = "jnl_bill_diagnosis_visit_main";
			//処理終了
			return $return_table_name;
		}
		elseif($wkfw_name == "0011")
		{
			//療養費明細書
			$return_table_name = "jnl_bill_recuperation_main";
			//処理終了
			return $return_table_name;
		}
		elseif($wkfw_name == "0012")
		{
			//療養費明細書
			$return_table_name = "jnl_bill_recuperation_prepay_main";
			//処理終了
			return $return_table_name;
		}
		else
		{
			//対象テーブルが見つからなかった場合は空文字を送る（ＤＢエラーになるはず）
			//処理終了
			return $return_table_name;
		}

	}



	// 集計データのステータスを更新する
	function update_report_status($selected_table,$apply_stat,$apply_id)
	{

		//集計用データ更新処理↓

		$sql = "update $selected_table set ";
		$set = array("jnl_status");
		$setvalue = array($apply_stat);
		$cond = "where newest_apply_id = $apply_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		//集計用データ更新処理↑
	}





	// 集計データのステータスを更新する
	function re_apply_report_status($selected_table,$apply_stat,$new_apply_id,$apply_id,$emp_id)
	{

		//集計用データ更新処理↓

		//ステータスが変化する際にapply_idが新規作成されることがあるので（再申請）newest_apply_idも更新する
		$sql = "update $selected_table set ";
		$set = array("jnl_status","newest_apply_id","emp_id");
		$setvalue = array($apply_stat,$new_apply_id,$emp_id);
		$cond = "where newest_apply_id = $apply_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		//集計用データ更新処理↑
	}

	// 集計データを論理削除
	function update_delflg_report($apply_id, $delete_flg)
	{

		//第一にどの病院、施設のデータか発見する
		$selected_table = $this->get_update_report_table($apply_id);

		if($selected_table == "jnl_return_main")
		{
			//返戻の場合はサブテーブルも論理削除する

			$del_table_list = array();
			$del_table_list[0] = "jnl_return_main";
			$del_table_list[1] = "jnl_return_total";
			$del_table_list[2] = "jnl_return_factor";
			$del_table_list[3] = "jnl_return_assessment";
			$del_table_list[4] = "jnl_return_re_charge";
			$del_table_list[5] = "jnl_return_comment";

			for($wcnt1=0;$wcnt1<=5;$wcnt1++)
			{

				//対象テーブルのデータを論理削除
				$sql = "update ".$del_table_list[$wcnt1]." set ";
				$set = array("del_flg");
				$setvalue = array($delete_flg);
				$cond = "where newest_apply_id = $apply_id";
				$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
				if ($upd == 0)
				{
					pg_query($this->_db_con, "rollback");
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}

		}
		elseif($selected_table == "jnl_nurse_home_main")
		{

			//老健ヘッダテーブルからステータスを取得し、1であったら「老健日報」であり、それ以外は「老健月報」↓
			$sql   = "select jnl_report_flg ";
			$sql  .= "from jnl_nurse_home_main ";
			$cond .= "where newest_apply_id = $apply_id ";

			$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			$report_status = pg_fetch_result($sel, 0, "jnl_report_flg");

			if($report_status == 1)
			{
				//老健日報の場合はサブテーブルも論理削除する

				$del_table_list = array();
				$del_table_list[0] = "jnl_nurse_home_main";
				$del_table_list[1] = "jnl_nurse_home_day_cnt";
				$del_table_list[2] = "jnl_nurse_home_day_enter";
				$del_table_list[3] = "jnl_nurse_home_day_visit";
				$del_table_list[4] = "jnl_nurse_home_day_visit_prevention";
				$del_table_list[5] = "jnl_nurse_home_day_short";
				$del_table_list[6] = "jnl_nurse_home_day_short_prevention";
				$del_table_list[7] = "jnl_nurse_home_day_rehab";
				$del_table_list[8] = "jnl_nurse_home_day_rehab_prevention";

				for($wcnt1=0;$wcnt1<=8;$wcnt1++)
				{

					//対象テーブルのデータを論理削除
					$sql = "update ".$del_table_list[$wcnt1]." set ";
					$set = array("del_flg");
					$setvalue = array($delete_flg);
					$cond = "where newest_apply_id = $apply_id";
					$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
					if ($upd == 0)
					{
						pg_query($this->_db_con, "rollback");
						pg_close($this->_db_con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}
			}
			else
			{
				//老健月報の場合はサブテーブルも論理削除する

				$del_table_list = array();
				$del_table_list[0] = "jnl_nurse_home_main";
				$del_table_list[1] = "jnl_nurse_home_mon_enter";
				$del_table_list[2] = "jnl_nurse_home_mon_short";
				$del_table_list[3] = "jnl_nurse_home_mon_commute";
				$del_table_list[4] = "jnl_nurse_home_mon_commute_pre";
				$del_table_list[5] = "jnl_nurse_home_mon_visit";
				$del_table_list[6] = "jnl_nurse_home_mon_visit_pre";

				for($wcnt1=0;$wcnt1<=6;$wcnt1++)
				{

					//対象テーブルのデータを論理削除
					$sql = "update ".$del_table_list[$wcnt1]." set ";
					$set = array("del_flg");
					$setvalue = array($delete_flg);
					$cond = "where newest_apply_id = $apply_id";
					$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
					if ($upd == 0)
					{
						pg_query($this->_db_con, "rollback");
						pg_close($this->_db_con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}

			}

		}
		elseif($selected_table == "jnl_reward_synthesis_medi_main")
		{
			//診療報酬総括表（医科）の場合はサブテーブルも論理削除する

			$del_table_list = array();
			$del_table_list[0] = "jnl_reward_synthesis_medi_main";
			$del_table_list[1] = "jnl_reward_synthesis_medi";

			for($wcnt1=0;$wcnt1<=1;$wcnt1++)
			{

				//対象テーブルのデータを論理削除
				$sql = "update ".$del_table_list[$wcnt1]." set ";
				$set = array("del_flg");
				$setvalue = array($delete_flg);
				$cond = "where newest_apply_id = $apply_id";
				$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
				if ($upd == 0)
				{
					pg_query($this->_db_con, "rollback");
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}

		}
		elseif($selected_table == "jnl_reward_synthesis_dental_main")
		{
			//診療報酬総括表（歯科）の場合はサブテーブルも論理削除する

			$del_table_list = array();
			$del_table_list[0] = "jnl_reward_synthesis_dental_main";
			$del_table_list[1] = "jnl_reward_synthesis_dental";

			for($wcnt1=0;$wcnt1<=1;$wcnt1++)
			{

				//対象テーブルのデータを論理削除
				$sql = "update ".$del_table_list[$wcnt1]." set ";
				$set = array("del_flg");
				$setvalue = array($delete_flg);
				$cond = "where newest_apply_id = $apply_id";
				$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
				if ($upd == 0)
				{
					pg_query($this->_db_con, "rollback");
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}

		}
		elseif($selected_table == "jnl_bill_diagnosis_enter_main")
		{
			//入院診療行為別明細の場合はサブテーブルも論理削除する

			$del_table_list = array();
			$del_table_list[0] = "jnl_bill_diagnosis_enter_main";
			$del_table_list[1] = "jnl_bill_diagnosis_enter";

			for($wcnt1=0;$wcnt1<=1;$wcnt1++)
			{

				//対象テーブルのデータを論理削除
				$sql = "update ".$del_table_list[$wcnt1]." set ";
				$set = array("del_flg");
				$setvalue = array($delete_flg);
				$cond = "where newest_apply_id = $apply_id";
				$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
				if ($upd == 0)
				{
					pg_query($this->_db_con, "rollback");
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}

		}
		elseif($selected_table == "jnl_bill_diagnosis_visit_main")
		{
			//外来診療行為別明細の場合はサブテーブルも論理削除する

			$del_table_list = array();
			$del_table_list[0] = "jnl_bill_diagnosis_visit_main";
			$del_table_list[1] = "jnl_bill_diagnosis_visit";

			for($wcnt1=0;$wcnt1<=1;$wcnt1++)
			{

				//対象テーブルのデータを論理削除
				$sql = "update ".$del_table_list[$wcnt1]." set ";
				$set = array("del_flg");
				$setvalue = array($delete_flg);
				$cond = "where newest_apply_id = $apply_id";
				$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
				if ($upd == 0)
				{
					pg_query($this->_db_con, "rollback");
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}

		}
		elseif($selected_table == "jnl_bill_recuperation_main")
		{
			//療養費明細書の場合はサブテーブルも論理削除する

			$del_table_list = array();
			$del_table_list[0] = "jnl_bill_recuperation_main";
			$del_table_list[1] = "jnl_bill_recuperation_insure_rehab";
			$del_table_list[2] = "jnl_bill_recuperation_service";
			$del_table_list[3] = "jnl_bill_recuperation_own_payment";
			$del_table_list[4] = "jnl_bill_recuperation_etc";

			for($wcnt1=0;$wcnt1<=4;$wcnt1++)
			{

				//対象テーブルのデータを論理削除
				$sql = "update ".$del_table_list[$wcnt1]." set ";
				$set = array("del_flg");
				$setvalue = array($delete_flg);
				$cond = "where newest_apply_id = $apply_id";
				$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
				if ($upd == 0)
				{
					pg_query($this->_db_con, "rollback");
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}

		}
		elseif($selected_table == "jnl_bill_recuperation_prepay_main")
		{
			//療養費明細書の場合はサブテーブルも論理削除する

			$del_table_list = array();
			$del_table_list[0] = "jnl_bill_recuperation_prepay_main";
			$del_table_list[1] = "jnl_bill_recuperation_insure_rehab_prepay";
			$del_table_list[2] = "jnl_bill_recuperation_service_prepay";
			$del_table_list[3] = "jnl_bill_recuperation_own_payment_prepay";
			$del_table_list[4] = "jnl_bill_recuperation_etc_prepay";

			for($wcnt1=0;$wcnt1<=4;$wcnt1++)
			{

				//対象テーブルのデータを論理削除
				$sql = "update ".$del_table_list[$wcnt1]." set ";
				$set = array("del_flg");
				$setvalue = array($delete_flg);
				$cond = "where newest_apply_id = $apply_id";
				$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
				if ($upd == 0)
				{
					pg_query($this->_db_con, "rollback");
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}

		}
		else
		{
			//対象テーブルのデータを論理削除
			$sql = "update $selected_table set ";
			$set = array("del_flg");
			$setvalue = array($delete_flg);
			$cond = "where newest_apply_id = $apply_id";
			$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($upd == 0)
			{
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}



	// 申請論理削除更新(全部)
	function update_delflg_all_apply($apply_id, $delete_flg)
	{
		// 申請情報を論理削除
		$this->update_delflg_apply($apply_id, $delete_flg);

		// 承認情報を論理削除
		$this->update_delflg_applyapv($apply_id, $delete_flg);

		// 承認者候補情報を論理削除
		$this->update_delflg_applyapvemp($apply_id, $delete_flg);

		// 添付ファイル情報論理削除
		$this->update_delflg_applyfile($apply_id, $delete_flg);

		// 非同期・同期受信論理削除
		$this->update_delflg_applyasyncrecv($apply_id, $delete_flg);

		// 申請結果通知論理削除
		$this->update_delflg_applynotice($apply_id, $delete_flg);

		// 前提とする申請書(申請用)論理削除
		$this->update_delflg_applyprecond($apply_id, $delete_flg);

		//集計データを論理削除
		$this->update_delflg_report($apply_id, $delete_flg);


	}

//-------------------------------------------------------------------------------------------------------------------------
// 再申請関連
//-------------------------------------------------------------------------------------------------------------------------

	// 申請登録
	function regist_re_apply($new_apply_id, $apply_id, $apply_content, $apply_title, $sceen_div,$emp_id)
	{

		$date = date("YmdHi");

		$year = substr($date, 0, 4);
		$md   = substr($date, 4, 4);
		if($md >= "0101" and $md <= "0331")
		{
			$year = $year - 1;
		}
		$max_cnt = $this->get_apply_cnt_per_year($year);
		$apply_no = $max_cnt + 1;


		$sql  = "insert into jnl_apply ";
		$sql .= "(select ";
		$sql .= "$new_apply_id, ";
		$sql .= "a.wkfw_id, ";

		if($sceen_div == "DETAIL")
		{
			$sql .= "'$apply_content', ";
		}
		else
		{
			$sql .= "a.apply_content, ";
		}

		$sql .= "'$emp_id', ";
		$sql .= "'0', ";
		$sql .= "'$date', ";
		$sql .= "'f', ";

		if($sceen_div == "DETAIL")
		{
			$sql .= "'$apply_title', ";
		}
		else
		{
			$sql .= "a.apply_title, ";
		}

		$sql .= "null, ";
		$sql .= "'t', ";
		$sql .= "'t', ";
		$sql .= "b.emp_class, ";
		$sql .= "b.emp_attribute, ";
		$sql .= "b.emp_dept, ";
		$sql .= "'t', ";
		$sql .= "b.emp_room, ";
		$sql .= "'f', ";
		$sql .= "a.wkfw_appr, ";
		$sql .= "a.wkfw_content_type, ";
		$sql .= "apply_title_disp_flg, ";
        $sql .= "$apply_no, ";
		$sql .= "a.notice_sel_flg, ";
		$sql .= "a.wkfw_history_no ";
		$sql .= "from jnl_apply a ";
		$sql .= "left join empmst b on a.emp_id = b.emp_id ";
		$sql .= "where a.apply_id = $apply_id";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認登録
	function regist_re_applyapv($new_apply_id, $apply_id)
	{
		$sql  = "insert into jnl_applyapv ";
		$sql .= "(select ";
		$sql .= "a.wkfw_id, ";
		$sql .= "$new_apply_id, ";
		$sql .= "a.apv_order, ";
		$sql .= "a.emp_id, ";
		$sql .= "'0', ";
		$sql .= "null, ";
		$sql .= "'f', ";
		$sql .= "'', ";
		$sql .= "a.st_div, ";
		$sql .= "a.deci_flg, ";
		$sql .= "b.emp_class, ";
		$sql .= "b.emp_attribute, ";
		$sql .= "b.emp_dept, ";
		$sql .= "b.emp_st, ";
		$sql .= "'t', ";
		$sql .= "b.emp_room, ";
		$sql .= "a.apv_sub_order, ";
		$sql .= "a.multi_apv_flg, ";
		$sql .= "a.next_notice_div, ";
		$sql .= "a.parent_pjt_id, ";
		$sql .= "a.child_pjt_id, ";
		$sql .= "'f'  ";
		$sql .= "from jnl_applyapv a ";
		$sql .= "left join empmst b on a.emp_id = b.emp_id ";
		$sql .= "where a.apply_id = $apply_id";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}

	// 承認者候補登録
	function regist_re_applyapvemp($new_apply_id, $apply_id)
	{
		$sql  = "insert into jnl_applyapvemp ";
		$sql .= "(select ";
		$sql .= "$new_apply_id, ";
		$sql .= "apv_order, ";
		$sql .= "person_order, ";
		$sql .= "emp_id, ";
		$sql .= "'f', ";
		$sql .= "st_div, ";
		$sql .= "parent_pjt_id, ";
		$sql .= "child_pjt_id ";
		$sql .= "from jnl_applyapvemp ";
		$sql .= "where apply_id = $apply_id";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 添付ファイル登録
	function regist_re_applyfile($new_apply_id, $apply_id)
	{

		$sql  = "insert into jnl_applyfile ";
		$sql .= "(select ";
		$sql .= "$new_apply_id, ";
		$sql .= "applyfile_no, ";
		$sql .= "applyfile_name, ";
		$sql .= "'f' ";
		$sql .= "from jnl_applyfile ";
		$sql .= "where apply_id = $apply_id";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 非同期・同期受信登録
	function regist_re_applyasyncrecv($new_apply_id, $apply_id)
	{
		$sql  = "insert into jnl_applyasyncrecv ";
		$sql .= "(select ";
		$sql .= "$new_apply_id, ";
		$sql .= "send_apv_order, ";
		$sql .= "send_apv_sub_order, ";
		$sql .= "recv_apv_order, ";
		$sql .= "recv_apv_sub_order, ";
		$sql .= "null, ";
		$sql .= "'f', ";
		$sql .= "'f' ";
		$sql .= "from jnl_applyasyncrecv ";
		$sql .= "where apply_id = $apply_id";


		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請結果通知登録
	function regist_re_applynotice($new_apply_id, $apply_id)
	{
		$sql  = "insert into jnl_applynotice ";
		$sql .= "(select ";
		$sql .= "$new_apply_id, ";
		$sql .= "recv_emp_id, ";
		$sql .= "'f', ";
		$sql .= "null, ";
		$sql .= "null, ";
		$sql .= "'f', ";
		$sql .= "rslt_ntc_div ";
		$sql .= "from jnl_applynotice ";
		$sql .= "where apply_id = $apply_id";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(申請用)登録
	function regist_re_applyprecond($new_apply_id, $apply_id)
	{
		$sql  = "insert into jnl_applyprecond ";
		$sql .= "(select ";
		$sql .= "$new_apply_id, ";
		$sql .= "precond_wkfw_id, ";
		$sql .= "precond_order, ";
		$sql .= "precond_apply_id, ";
		$sql .= "'f' ";
		$sql .= "from jnl_applyprecond ";
		$sql .= "where apply_id = $apply_id";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 再申請ＩＤ更新
	function update_re_apply_id($apply_id, $next_apply_id)
	{
		$sql = "update jnl_apply set";
		$set = array("re_apply_id");
		$setvalue = array($next_apply_id);
		$cond = "where apply_id = $apply_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// 非同期・同期通知関連
//-------------------------------------------------------------------------------------------------------------------------
	// 非同期・同期受信登録
	function regist_applyasyncrecv($apply_id, $send_apv_order, $send_apv_sub_order, $recv_apv_order, $recv_apv_sub_order)
	{
		$sql = "insert into jnl_applyasyncrecv (apply_id, send_apv_order, send_apv_sub_order, recv_apv_order, recv_apv_sub_order, send_apved_order, apv_show_flg, delete_flg) values (";
		$content = array($apply_id, $send_apv_order, $send_apv_sub_order, $recv_apv_order, $recv_apv_sub_order, null, "f", "f");
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// 承認
//-------------------------------------------------------------------------------------------------------------------------
	// 同一階層の承認者数取得
	function get_same_hierarchy_apvcnt($apply_id, $apv_order)
	{
		$sql  = "select count(*) as cnt from jnl_applyapv";
		$cond ="where apply_id = $apply_id and apv_order = $apv_order";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "cnt");
	}

	// 最終承認階層番号取得
	function get_last_apv_order($apply_id)
	{
		$sql  = "select max(apv_order) as max from jnl_applyapv";
		$cond ="where apply_id = $apply_id";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}


	// 承認情報更新
	function update_apvstat($apv_stat, $apv_date, $apv_comment, $apply_id, $apv_order, $apv_sub_order, $screen_div)
	{
		$sql = "update jnl_applyapv set";

		if($screen_div == "DETAIL")
		{
			$set = array("apv_stat", "apv_date", "apv_comment");
			$setvalue = array($apv_stat, $apv_date, pg_escape_string($apv_comment));
		}
		else if($screen_div == "LIST")
		{
			$set = array("apv_stat", "apv_date");
			$setvalue = array($apv_stat, $apv_date);
		}

		if($apv_sub_order != "")
		{
			$cond = "where apply_id = $apply_id and apv_order = $apv_order and apv_sub_order = $apv_sub_order";
		}
		else
		{
			$cond = "where apply_id = $apply_id and apv_order = $apv_order and apv_sub_order is null";
		}

		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 指定した承認ステータス数取得
	function get_apvstatcnt($apply_id, $apv_stat)
	{
		$sql = "select count(*) as cnt from jnl_applyapv ";
		$cond = "where apply_id = $apply_id and apv_stat = '$apv_stat'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}

	// 全承認者数取得
	function get_allapvcnt($apply_id)
	{
		$sql = "select count(*) as cnt from jnl_applyapv ";
		$cond = "where apply_id = $apply_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}

	// 申請ステータス更新
	function update_applystat($apply_id, $apply_stat, $session)
	{

		//集計用データのステータスを指定された値にに更新する////////////////////////////////////////////////
		//第一にどの病院、施設のデータか発見する
		$selected_table = $this->get_update_report_table($apply_id);

		// 集計データのステータスを更新する
		$this->update_report_status($selected_table,$apply_stat,$apply_id);


		$sql = "update jnl_apply set";
		$set = array("apply_stat");
		$setvalue = array($apply_stat);
		$cond = "where apply_id = $apply_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		// 申請結果通知更新
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];
		$this->update_send_applynotice($apply_id, $emp_id);
	}

	// 否認フラグ更新
	function update_ng_show_flg($apply_id, $apv_ng_show_flg)
	{
		$sql = "update jnl_apply set";
		$set = array("apv_ng_show_flg");
		$setvalue = array($apv_ng_show_flg);
		$cond = "where apply_id = $apply_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// 差戻しフラグ更新
	function update_bak_show_flg($apply_id, $apv_bak_show_flg)
	{
		$sql = "update jnl_apply set";
		$set = array("apv_bak_show_flg");
		$setvalue = array($apv_bak_show_flg);
		$cond = "where apply_id = $apply_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// 非同期・同期受信更新
	function update_apv_show_flg($apply_id, $send_apv_order, $send_apv_sub_order, $send_apved_order)
	{
		$sql = "update jnl_applyasyncrecv set";
		$set = array("apv_show_flg", "send_apved_order");
		$setvalue = array("t", $send_apved_order);

		if($send_apv_sub_order != "")
		{
			$cond = "where apply_id = $apply_id and send_apv_order = $send_apv_order and send_apv_sub_order = $send_apv_sub_order";
		}
		else
		{
			$cond = "where apply_id = $apply_id and send_apv_order = $send_apv_order and send_apv_sub_order is null";
		}
		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認更新番号取得
	function get_max_send_apved_order($apply_id, $send_apv_order)
	{
		$sql  = "select max(send_apved_order) as max from jnl_applyasyncrecv ";
		$cond = "where apply_id = $apply_id and send_apv_order = $send_apv_order";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}

	// 同一階層で指定した承認ステータス数取得
	function get_same_hierarchy_apvstatcnt($apply_id, $apv_order, $apv_stat)
	{
		$sql = "select count(*) as cnt from jnl_applyapv ";
		$cond = "where apply_id = $apply_id and apv_order = $apv_order and apv_stat = '$apv_stat'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}


	// 階層ごとの承認者情報取得
	function get_applyapv_per_hierarchy($apply_id, $apv_order)
	{
		$sql = "select * from jnl_applyapv ";
		$cond = "where apply_id = $apply_id and apv_order = $apv_order";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}

	function get_apply_stat($apply_id)
	{
		$sql = "select apply_stat from jnl_apply ";
		$cond = "where apply_id = $apply_id ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "apply_stat");
	}


	// 承認処理
	function  approve_application($apply_id, $wkfw_appr, $apv_order, $apv_sub_order, $approve, $apv_comment, $next_notice_div, $session, $screen_div)
	{

		// 承認ステータス更新
		$apv_date = date("YmdHi");
		$this->update_apvstat($approve, $apv_date, $apv_comment, $apply_id, $apv_order, $apv_sub_order, $screen_div);

		// 同報タイプ
		if($wkfw_appr == "1")
		{
			// 全承認者数取得
			$allapvcnt = $this->get_allapvcnt($apply_id);
			switch($approve)
			{
				case "1":   // 承認
					$apvstatcnt = $this->get_apvstatcnt($apply_id, "1");
					if($allapvcnt == $apvstatcnt)
					{
						// 申請ステータス更新
						$this->update_applystat($apply_id, "1", $session);
					}
					break;
				case "2":   // 否認
					// 申請ステータス更新
					$this->update_applystat($apply_id, "2", $session);
					break;

				case "3":   // 差戻し
					// 申請ステータス更新
					$this->update_applystat($apply_id, "3", $session);
					break;
				default:
					break;
			}
		}
		// 稟議タイプ
		else if($wkfw_appr == "2")
		{
			$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);

			// 該当承認者の同一階層に他の承認者がいる場合(複数承認者)
			if($same_hierarchy_apvcnt > 1)
			{
				// 最終承認階層取得
				$last_apv_order = $this->get_last_apv_order($apply_id);

				// 承認者の階層より後につづく階層がある場合
				if($apv_order < $last_apv_order)
				{
					// 承認者の階層が非同期指定の場合
					if($next_notice_div == "1")
					{
						switch($approve)
						{
							case "1":   // 承認
								// 非同期・同期受信テーブル更新
								$apved_order = $this->get_max_send_apved_order($apply_id, $apv_order);
								$this->update_apv_show_flg($apply_id, $apv_order, $apv_sub_order, $apved_order+1);
								break;
							case "2":   // 否認

								$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);
								$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "2");

								// 全員が否認の場合
								if($same_hierarchy_apvcnt == $same_hierarchy_apvstatcnt)
								{
									// 申請ステータス更新
									$this->update_applystat($apply_id, "2", $session);
								}
								// 同一階層で先に「承認」した人がいた場合
								$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
								if($same_hierarchy_apvstatcnt > 0)
								{
									// 非同期・同期受信テーブル更新
									$apved_order = $this->get_max_send_apved_order($apply_id, $apv_order);
									$this->update_apv_show_flg($apply_id, $apv_order, $apv_sub_order, $apved_order+1);
								}
								break;
							case "3":   // 差戻し

								// 同一階層で先に「承認」した人がいた場合
								$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
								if($same_hierarchy_apvstatcnt > 0)
								{
									// 非同期・同期受信テーブル更新
									$apved_order = $this->get_max_send_apved_order($apply_id, $apv_order);
									$this->update_apv_show_flg($apply_id, $apv_order, $apv_sub_order, $apved_order+1);
								}

								break;
							default:
								break;
						}

						// 未承認者がいない ＡＮＤ 承認が一人もいない ＡＮＤ 差戻しが一人でもいる
						$non_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");
						$ok_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
						$bak_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "3");
						if($non_apvstatcnt == 0 && $ok_apvstatcnt == 0 && $bak_apvstatcnt > 0)
						{
							// 申請ステータス更新
							$this->update_applystat($apply_id, "3", $session);
						}
					}
					// 承認者の階層が同期指定の場合
					else if($next_notice_div == "2")
					{
						$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");
						// 同一階層で未承認者がいない場合
						if($same_hierarchy_apvstatcnt == 0)
						{
							$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);
							$ok_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
							$no_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "2");
							$bak_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "3");

							// 全員が承認の場合
							if($same_hierarchy_apvcnt == $ok_approvecnt)
							{
								// 非同期・同期受信テーブル更新
								$this->update_apv_show_flg($apply_id, $apv_order, "", 1);
							}

							// 否認があって差戻しがない場合、否認にする。
							if($no_approvecnt > 0 && $bak_approvecnt == 0)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "2", $session);
							}

							// 差戻しが１つでもある場合
							if($bak_approvecnt > 0)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "3", $session);

							}
						}
					}
					// 権限並列指定の場合
					else if($next_notice_div == "3")
					{
						$non_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");

						// 未承認者がいた場合
						if($non_apvstatcnt > 0)
						{
							switch($approve)
							{
								case "1":   // 承認

									// 非同期・同期受信テーブル更新
									$this->update_apv_show_flg($apply_id, $apv_order, "", 1);

									// 他の承認者の承認ステータスが「未承認」の場合、「承認」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "1");

									break;
								case "2":   // 否認

									// 他の承認者の承認ステータスが「未承認」の場合、「否認」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "2");
									// 申請ステータス更新
									$this->update_applystat($apply_id, "2", $session);
									break;
								case "3":   // 差戻し

									// 他の承認者の承認ステータスが「未承認」の場合、「差戻し」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "3");
									// 申請ステータス更新
									$this->update_applystat($apply_id, "3", $session);

									break;
								default:
									break;
							}
						}
					}
				}
				// 承認者の階層より後につづく階層がない場合（最終階層）
				else if($apv_order == $last_apv_order)
				{
					// 承認者の階層が非同期指定の場合
					if($next_notice_div == "1")
					{
						$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");
						// 同一階層で未承認者がいない場合
						if($same_hierarchy_apvstatcnt == 0)
						{
							// 承認が１つでもある場合
							$approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
							if($approvecnt > 0)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "1", $session);
							}

							// 全員が否認の場合
							$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);
							$no_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "2");
							if($same_hierarchy_apvcnt == $no_approvecnt)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "2", $session);
							}

							// 承認がなく差戻しがある場合
							if($approvecnt == 0)
							{
								$bak_cnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "3");
								if($bak_cnt > 0)
								{
									// 申請ステータス更新
									$this->update_applystat($apply_id, "3", $session);
								}
							}
						}
					}
					// 承認者の階層が同期指定の場合
					else if($next_notice_div == "2")
					{
						$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");
						// 同一階層で未承認者がいない場合
						if($same_hierarchy_apvstatcnt == 0)
						{
							$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);
							$ok_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
							$no_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "2");
							$bak_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "3");

							// 全員が承認の場合
							if($same_hierarchy_apvcnt == $ok_approvecnt)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "1", $session);
							}

							// 否認があって差戻しがない場合、否認にする。
							if($no_approvecnt > 0 && $bak_approvecnt == 0)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "2", $session);
							}

							// 差戻しが１つでもある場合
							if($bak_approvecnt > 0)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "3", $session);

							}
						}
					}
					// 権限並列指定の場合
					else if($next_notice_div == "3")
					{
						$non_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");

						// 未承認者がいた場合
						if($non_apvstatcnt > 0)
						{
							switch($approve)
							{
								case "1":   // 承認

									// 他の承認者の承認ステータスが「未承認」の場合、「承認」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "1");
									// 申請ステータス更新
									$this->update_applystat($apply_id, "1", $session);
									break;
								case "2":   // 否認

									// 他の承認者の承認ステータスが「未承認」の場合、「否認」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "2");
									// 申請ステータス更新
									$this->update_applystat($apply_id, "2", $session);
									break;
								case "3":   // 差戻し

									// 他の承認者の承認ステータスが「未承認」の場合、「差戻し」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "3");
									// 申請ステータス更新
									$this->update_applystat($apply_id, "3", $session);

									break;
								default:
									break;
							}
						}
					}
				}
			}
			// 該当承認者の同一階層に他の承認者がいない場合(承認者一人)
			else if($same_hierarchy_apvcnt == 1)
			{
				switch($approve)
				{
					case "1":   // 承認
						// 最終承認階層取得
						$last_apv_order = $this->get_last_apv_order($apply_id);
						if($apv_order == $last_apv_order)
						{
							// 申請ステータス更新
							$this->update_applystat($apply_id, "1", $session);
						}
						break;
					case "2":   // 否認
						// 申請ステータス更新
						$this->update_applystat($apply_id, "2", $session);
						break;
					case "3":   // 差戻し
						// 申請ステータス更新
						$this->update_applystat($apply_id, "3", $session);
						break;
					default:
						break;

				}
			}
		}
	}

	// 非同期・同期受信情報取得
	function get_applyasyncrecv($apply_id, $recv_apv_order, $recv_apv_sub_order, $send_apved_order)
	{
		$sql   = "select * from jnl_applyasyncrecv ";
		$cond .= "where apply_id = $apply_id and ";
		$cond .= "recv_apv_order = $recv_apv_order and ";
        if($recv_apv_sub_order != "")
		{
			$cond .= "recv_apv_sub_order = $recv_apv_sub_order and ";
		}
		else
		{
			$cond .= "recv_apv_sub_order is null and ";
		}
		$cond .= "send_apved_order <= $send_apved_order ";
		$cond .= "order by send_apved_order asc ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("apply_id" => $row["apply_id"],
                            "send_apv_order" =>  $row["send_apv_order"],
                            "send_apv_sub_order" =>  $row["send_apv_sub_order"],
                            "recv_apv_order" =>  $row["recv_apv_order"],
                            "recv_apv_sub_order" =>  $row["recv_apv_sub_order"],
                            "send_apved_order" =>  $row["send_apved_order"]
			               );
		}
		return $arr;
	}

	// 申請結果通知・送信者更新
	function update_send_applynotice($apply_id, $send_emp_id)
	{
		$date = date("YmdHi");

		$sql = "update jnl_applynotice set";
		$set = array("send_emp_id", "send_date");
		$setvalue = array($send_emp_id, $date);
		$cond = "where apply_id = $apply_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認テーブル更新処理（権限並列用）
	function update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, $apv_stat)
	{
		$sql = "update jnl_applyapv set";
		$set = array("apv_stat", "other_apv_flg", "apv_date");
		$setvalue = array($apv_stat, "t", $apv_date);
		$cond = "where apply_id = $apply_id and apv_order = $apv_order and apv_stat = '0'";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// ワークフロープレビュー
//-------------------------------------------------------------------------------------------------------------------------

	// 承認者管理情報取得(プレビュー)
	function get_approve_mng_for_wkfwpreview($data)
	{
		// 承認者階層数取得
		$approve_num = $data["approve_num"];

		$arr = array();

		for($i=0; $i<$approve_num; $i++)
		{
			$j = $i + 1;

			$tmp_apv_div0_flg = "apv_div0_flg".$j;
			$apv_div0_flg = $data[$tmp_apv_div0_flg];

			$tmp_apv_div1_flg = "apv_div1_flg".$j;
			$apv_div1_flg = $data[$tmp_apv_div1_flg];

			$tmp_apv_div2_flg = "apv_div2_flg".$j;
			$apv_div2_flg = $data[$tmp_apv_div2_flg];

			$tmp_apv_div3_flg = "apv_div3_flg".$j;
			$apv_div3_flg = $data[$tmp_apv_div3_flg];

			$tmp_apv_div4_flg = "apv_div4_flg".$j;
			$apv_div4_flg = $data[$tmp_apv_div4_flg];

			$tmp_target_class_div = "target_class_div".$j;
			$target_class_div = $data[$tmp_target_class_div];

			$tmp_st_id = "st_id".$j;
			$st_id = $data[$tmp_st_id];

			$tmp_multi_apv_flg = "multi_apv_flg".$j;
			$multi_apv_flg = $data[$tmp_multi_apv_flg];

			$tmp_apv_num = "apv_num".$j;
			$apv_num = $data[$tmp_apv_num];

			$tmp_emp_id = "emp_id".$j;
			$emp_id = $data[$tmp_emp_id];

			$tmp_pjt_parent_id = "pjt_parent_id".$j;
			$pjt_parent_id = $data[$tmp_pjt_parent_id];

			$tmp_pjt_child_id = "pjt_child_id".$j;
			$pjt_child_id = $data[$tmp_pjt_child_id];

			$tmp_class_sect_id = "class_sect_id".$j;
			$class_sect_id = $data[$tmp_class_sect_id];

			$tmp_atrb_sect_id = "atrb_sect_id".$j;
			$atrb_sect_id = $data[$tmp_atrb_sect_id];

			$tmp_dept_sect_id = "dept_sect_id".$j;
			$dept_sect_id = $data[$tmp_dept_sect_id];

			$tmp_room_sect_id = "room_sect_id".$j;
			$room_sect_id = $data[$tmp_room_sect_id];

			$tmp_st_sect_id = "st_sect_id".$j;
			$st_sect_id = $data[$tmp_st_sect_id];


			$apv_setting_flg = "f";
			if($apv_div0_flg == "f" && $apv_div1_flg == "f" && $apv_div2_flg == "t" && $apv_div3_flg == "f" && $apv_div4_flg == "f")
			{
				$apv_setting_flg = "t";
			}

			$arr[] = array("apv_order" => $j,
                            "apv_div0_flg" => $apv_div0_flg,
                            "apv_div1_flg" => $apv_div1_flg,
                            "apv_div2_flg" => $apv_div2_flg,
                            "apv_div3_flg" => $apv_div3_flg,
                            "apv_div4_flg" => $apv_div4_flg,
                            "target_class_div" => $target_class_div,
                            "st_id" => $st_id,
                            "multi_apv_flg" => $multi_apv_flg,
                            "emp_id" => $emp_id,
                            "pjt_parent_id" => $pjt_parent_id,
                            "pjt_child_id" => $pjt_child_id,
                            "class_sect_id" => $class_sect_id,
                            "atrb_sect_id" => $atrb_sect_id,
                            "dept_sect_id" => $dept_sect_id,
                            "room_sect_id" => $room_sect_id,
                            "st_sect_id" => $st_sect_id,
                            "apv_num" => $apv_num,
                            "apv_setting_flg" => $apv_setting_flg);
		}
		return $arr;
	}

	// 承認者詳細情報取得(プレビュー)
	function get_approve_dtl_for_wkfwpreview($arr_wkfwapvmng, $emp_id)
	{
		$arr = array();
		foreach($arr_wkfwapvmng as $apvmng)
		{
			$arr_apv = array();
			$multi_apv_flg = $apvmng["multi_apv_flg"];

			$apv_div0_flg = $apvmng["apv_div0_flg"];
			$apv_div1_flg = $apvmng["apv_div1_flg"];
			$apv_div2_flg = $apvmng["apv_div2_flg"];
			$apv_div3_flg = $apvmng["apv_div3_flg"];
			$apv_div4_flg = $apvmng["apv_div4_flg"];

			// 部署役職(申請者所属)指定
			if($apv_div0_flg == "t")
			{
				$arr_apvpstdtl = $this->get_apvpstdtl_for_wkfwpreview($apvmng["target_class_div"], $apvmng["st_id"], $emp_id);
				for($i=0; $i<count($arr_apvpstdtl); $i++)
				{
					$arr_apvpstdtl[$i]["apv_div"] = "0";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_apvpstdtl);

			}

			// 部署役職(部署指定)指定
			if($apv_div4_flg == "t")
			{
				$arr_emp_info_sect = $this->get_emp_info_for_post_sect($apvmng["class_sect_id"], $apvmng["atrb_sect_id"], $apvmng["dept_sect_id"], $apvmng["room_sect_id"], $apvmng["st_sect_id"]);

				for($i=0; $i<count($arr_emp_info_sect); $i++)
				{
					$arr_emp_info_sect[$i]["apv_div"] = "4";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_emp_info_sect);
			}

			// 職員指定
			if($apv_div1_flg == "t")
			{
				$arr_emp_id = split(",", $apvmng["emp_id"]);
				$arr_emp_info = array();
				foreach($arr_emp_id as $emp_id)
				{
					$arr_empmst_detail = $this->get_empmst_detail($emp_id);
					$emp_lt_nm = $arr_empmst_detail[0]["emp_lt_nm"];
					$emp_ft_nm = $arr_empmst_detail[0]["emp_ft_nm"];
                    $emp_full_nm = $emp_lt_nm." ".$emp_ft_nm;
					$arr_emp_info[] = array(
									 "emp_id" => $arr_empmst_detail[0]["emp_id"],
									 "emp_full_nm" => $emp_full_nm,
									 "st_nm" => $arr_empmst_detail[0]["st_nm"]
 									   );
				}
				$apvmng["emp_id"] = "";

				for($i=0; $i<count($arr_emp_info); $i++)
				{
					$arr_emp_info[$i]["apv_div"] = "1";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_emp_info);
			}

			// 委員会・ＷＧ
			if($apv_div3_flg == "t")
			{
				$arr_project_member = $this->get_project_member($apvmng["pjt_parent_id"], $apvmng["pjt_child_id"]);

				$pjt_nm = $this->get_pjt_nm($apvmng["pjt_parent_id"]);
				if($apvmng["pjt_child_id"] != "")
				{
					$pjt_nm .= " > ";
					$pjt_nm .= $this->get_pjt_nm($apvmng["pjt_child_id"]);
				}

				for($i=0; $i<count($arr_project_member); $i++)
				{
					$arr_project_member[$i]["apv_div"] = "3";
					$arr_project_member[$i]["pjt_nm"] = $pjt_nm;
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_project_member);
			}

			// その他
			if($apv_div2_flg == "t")
			{
				$apv_num = $apvmng["apv_num"];
				$arr_setting_apv = "";
				for($i=0; $i<$apv_num; $i++)
				{
					$arr_setting_apv[] = array();
				}

				for($i=0; $i<count($arr_setting_apv); $i++)
				{
					$arr_setting_apv[$i]["apv_div"] = "2";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_setting_apv);

			}

			// 複数承認者「許可する」
			if($multi_apv_flg == "t")
			{
				if(count($arr_apv) > 0)
				{
					if(count($arr_apv) > 1)
					{
						$apv_sub_order = 1;
						foreach($arr_apv as $apv)
						{
							$arr_emp = array();
							$apvmng["apv_sub_order"] = $apv_sub_order;
							$arr_emp[] = array(
                                                "emp_id" => $apv["emp_id"],
                                                "emp_full_nm" => $apv["emp_full_nm"],
                                                "st_nm" => $apv["st_nm"],
                                                "apv_div" => $apv["apv_div"],
                                                "pjt_nm" => $apv["pjt_nm"]
                                               );
							$apvmng["emp_infos"] = $arr_emp;

							if($apv["apv_div"] == "2")
							{
								$apvmng["apv_setting_flg"] = "t";
							}

							$arr[] = $apvmng;
							$apv_sub_order++;
						}
					}
					else
					{
						$arr_emp = array();
						$apvmng["apv_sub_order"] = "";
						$arr_emp[] = array(
                                            "emp_id" => $arr_apv[0]["emp_id"],
                                            "emp_full_nm" => $arr_apv[0]["emp_full_nm"],
                                            "st_nm" => $arr_apv[0]["st_nm"],
                                            "apv_div" => $arr_apv[0]["apv_div"],
                                            "pjt_nm" => $arr_apv[0]["pjt_nm"]
                                           );
						$apvmng["emp_infos"] = $arr_emp;

						if($arr_apv[0]["apv_div"] == "2")
						{
							$apvmng["apv_setting_flg"] = "t";
						}

						$arr[] = $apvmng;
					}
				}
				else
				{
					$apvmng["emp_infos"] = array();
					$arr[] = $apvmng;
				}
			}
			// 複数承認者「許可しない」
			else
			{
				$apvmng["apv_sub_order"] = "";
				$apvmng["emp_infos"] = $arr_apv;
				$arr[] = $apvmng;
			}
		}

		return $arr;

	}


	// 部署役職指定情報取得(プレビュー用)
	function get_apvpstdtl_for_wkfwpreview($target_class_div, $st_id, $emp_id)
	{
		$arr = array();

		// 申請者の部署役職取得
		$arr_emp_info = $this->get_emp_info($emp_id);

		if($target_class_div == "4")
		{
			$fourth_post_exist_flg = false;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_room  = $emp_info["emp_room"];
				if($emp_room != "")
				{
					$fourth_post_exist_flg = true;
					break;
				}
			}

			if(!$fourth_post_exist_flg)
			{
				return $arr;
			}
		}


		$sql  = "select ";
		$sql .= "emp.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_class, empmst.emp_attribute, empmst.emp_dept, empmst.emp_room, empmst.emp_st, stmst.st_nm ";
		$sql .= "from ";
    	$sql .= "(";
    	$sql .= "select emp_id from empmst where ";
		// 「部署指定しない」以外
		if($target_class_div != 0)
		{
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_class     = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept      = $emp_info["emp_dept"];
				$emp_room      = $emp_info["emp_room"];

				if($target_class_div == "4")
				{
					if($emp_room == "")
					{
						continue;
					}
				}

				if($idx > 0)
				{
					$sql .= "or ";
				}

				if($target_class_div == "1")
				{
					$sql .= "emp_class = $emp_class ";
				}
				else if($target_class_div == "2")
				{
					$sql .= "emp_attribute = $emp_attribute ";
				}
				else if($target_class_div == "3")
				{
					$sql .= "emp_dept = $emp_dept ";
				}
				else if($target_class_div == "4")
				{
					$sql .= "emp_room = $emp_room ";
				}
				$idx++;
			}
			$sql .= ") ";
			$sql .= "and ";
		}

		$sql .= "empmst.emp_st in ($st_id) ";
		$sql .= "union ";
		$sql .= "select emp_id from concurrent where ";

		// 「部署指定しない」以外
		if($target_class_div != 0)
		{
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_class     = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept      = $emp_info["emp_dept"];
				$emp_room      = $emp_info["emp_room"];

				if($target_class_div == "4")
				{
					if($emp_room == "")
					{
						continue;
					}
				}

				if($idx > 0)
				{
					$sql .= "or ";
				}

				if($target_class_div == "1")
				{
					$sql .= "emp_class = $emp_class ";
				}
				else if($target_class_div == "2")
				{
					$sql .= "emp_attribute = $emp_attribute ";
				}
				else if($target_class_div == "3")
				{
					$sql .= "emp_dept = $emp_dept ";
				}
				else if($target_class_div == "4")
				{
					$sql .= "emp_room = $emp_room ";
				}
				$idx++;
			}
			$sql .= ") ";
			$sql .= "and ";
		}
		$sql .= "concurrent.emp_st in ($st_id) ";
		$sql .= ") emp ";
		$sql .= "inner join empmst on emp.emp_id = empmst.emp_id ";
		$sql .= "inner join authmst on emp.emp_id = authmst.emp_id and not authmst.emp_del_flg ";
		$sql .= "inner join stmst on empmst.emp_st = stmst.st_id and not stmst.st_del_flg ";
		$cond = "order by emp.emp_id asc ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$idx = 1;
		while($row = pg_fetch_array($sel))
		{
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
			                "emp_full_nm" => $emp_full_nm,
			                "st_nm" => $row["st_nm"]
			               );
			$idx++;
		}
		return $arr;
	}

	// 申請者以外の結果通知者取得(プレビュー用)
	function get_wkfw_notice_for_wkfwpreview($data, $emp_id)
	{
		$arr_tmp = array();
		$notice = $data["notice"];

		if($notice != "")
		{
			$rslt_ntc_div0_flg = $data["rslt_ntc_div0_flg"];
			$rslt_ntc_div1_flg = $data["rslt_ntc_div1_flg"];
			$rslt_ntc_div2_flg = $data["rslt_ntc_div2_flg"];
			$rslt_ntc_div3_flg = $data["rslt_ntc_div3_flg"];
			$rslt_ntc_div4_flg = $data["rslt_ntc_div4_flg"];

			// 部署役職指定(申請者所属)
			if($rslt_ntc_div0_flg == "t")
			{
				$notice_target_class_div = $data["notice_target_class_div"];
				$notice_st_id = $data["notice_st_id"];
				$arr_apvpstdtl = $this->get_apvpstdtl_for_wkfwpreview($notice_target_class_div, $notice_st_id, $emp_id);

				// マージ処理
				$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_apvpstdtl);
			}

			// 部署役職指定
			if($rslt_ntc_div4_flg == "t")
			{
				$arr_post_sect = $this->get_emp_info_for_post_sect($data["notice_class_sect_id"], $data["notice_atrb_sect_id"], $data["notice_dept_sect_id"], $data["notice_room_sect_id"], $data["notice_st_sect_id"]);

				// マージ処理
				$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_post_sect);
			}

			// 職員指定
			if($rslt_ntc_div1_flg == "t")
			{
				$notice_emp_id = $data["notice_emp_id"];
				$arr_notice_emp_id = split(",", $notice_emp_id);
				foreach($arr_notice_emp_id as $notice_emp_id)
				{
					$arr_empmst_detail = $this->get_empmst_detail($notice_emp_id);
					$emp_lt_nm = $arr_empmst_detail[0]["emp_lt_nm"];
					$emp_ft_nm = $arr_empmst_detail[0]["emp_ft_nm"];
                    $emp_full_nm = $emp_lt_nm." ".$emp_ft_nm;
					$arr_emp_info[] = array(
									 "emp_id" => $arr_empmst_detail[0]["emp_id"],
									 "emp_full_nm" => $emp_full_nm,
									 "st_nm" => $arr_empmst_detail[0]["st_nm"]
									 );
				}

				// マージ処理
				$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_emp_info);
			}

			// 委員会・ＷＧ指定
			if($rslt_ntc_div3_flg == "t")
			{
				$notice_pjt_parent_id = $data["notice_pjt_parent_id"];
				$notice_pjt_child_id = $data["notice_pjt_child_id"];
				$arr_project_member = $this->get_project_member($notice_pjt_parent_id, $notice_pjt_child_id);

				// マージ処理
				$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_project_member);
			}
		}

		// 申請者を除去する
		$arr = array();
		foreach($arr_tmp as $tmp)
		{
			$tmp_emp_id = $tmp["emp_id"];
			if($tmp_emp_id == $emp_id)
			{
				continue;
			}
			$arr[] = $tmp;
		}
		return $arr;
	}

	// 部署役職(部署指定)情報取得
	function get_apvsectdtl($wkfw_id, $apv_order)
	{
		$sql   = "select a.class_id, a.atrb_id, a.dept_id, a.room_id ";
		$sql  .= "from jnl_wkfwapvsectdtl a ";
		$cond  = "where wkfw_id = $wkfw_id and apv_order = $apv_order";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$class_id = pg_fetch_result($sel, 0, "class_id");
		$atrb_id = pg_fetch_result($sel, 0, "atrb_id");
		$dept_id = pg_fetch_result($sel, 0, "dept_id");
		$room_id = pg_fetch_result($sel, 0, "room_id");

		return array("class_id" => $class_id, "atrb_id" => $atrb_id, "dept_id" => $dept_id, "room_id" => $room_id);
	}

	// 役職情報取得
	function get_apvpstdtl($wkfw_id, $apv_order, $st_div)
	{
		$sql   = "select st_id from jnl_wkfwapvpstdtl ";
		$cond  = "where wkfw_id = $wkfw_id and apv_order = $apv_order and st_div = $st_div order by st_id";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("st_id" => $row["st_id"]);
		}

		return $arr;
	}

	// 職員情報取得(部署役職指定用)
	function get_emp_info_for_post_sect($class_id, $attribute_id, $dept_id, $room_id, $st_sect_id)
	{

		$sql  = "select a.emp_id, a.emp_lt_nm, a.emp_ft_nm, c.st_nm ";
		$sql .= "from ( ";
		$sql .= "select varchar(1) '1' as type, emp_id, emp_lt_nm, emp_ft_nm, emp_st from empmst ";

		if($class_id != "")
		{
			$sql .= "where emp_class = $class_id ";
		}

		if($attribute_id != "")
		{
			$sql .= "and emp_attribute = $attribute_id ";
		}

		if($dept_id != "")
		{
			$sql .= "and emp_dept = $dept_id ";
		}

		if($room_id != "")
		{
			$sql .= "and emp_room = $room_id ";
		}

		if($st_sect_id)
		{
			$sql .= "and emp_st in ($st_sect_id) ";
		}

		$sql .= "union ";
		$sql .= "select varchar(1) '2' as type, sub_a.emp_id, sub_b.emp_lt_nm, sub_b.emp_ft_nm, sub_a.emp_st from concurrent sub_a ";
		$sql .= "inner join empmst sub_b on sub_a.emp_id = sub_b.emp_id ";

		if($class_id != "")
		{
			$sql .= "where sub_a.emp_class = $class_id ";
		}

		if($attribute_id != "")
		{
			$sql .= "and sub_a.emp_attribute = $attribute_id ";
		}

		if($dept_id != "")
		{
			$sql .= "and sub_a.emp_dept = $dept_id ";
		}

		if($room_id != "")
		{
			$sql .= "and sub_a.emp_room = $room_id ";
		}

		if($st_sect_id)
		{
			$sql .= "and sub_a.emp_st in ($st_sect_id) ";
		}

		$sql .= ") a ";
		$sql .= "inner join authmst b on a.emp_id = b.emp_id and not b.emp_del_flg ";
		$sql .= "left join stmst c on a.emp_st = c.st_id and not c.st_del_flg ";
		$sql .= "order by a.emp_id asc, a.type asc";

		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		$tmp_emp_id = "";
		$idx = 1;
		while($row = pg_fetch_array($sel))
		{
			if($tmp_emp_id == $row["emp_id"])
			{
				continue;
			}
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
			                "emp_full_nm" => $emp_full_nm,
			                "st_nm" => $row["st_nm"],
			                "apv_sub_order" => $idx);

			$tmp_emp_id = $row["emp_id"];
			$idx++;
		}
		return $arr;
	}
//-------------------------------------------------------------------------------------------------------------------------
// 申請結果通知関連
//-------------------------------------------------------------------------------------------------------------------------

	// 申請結果通知一覧取得
	function get_applynotice_list($arr)
	{

		$session      = $arr["session"];
		$apply_emp_nm = $arr["apply_emp_nm"];
		$apply_stat   = $arr["apply_stat"];
		$class        = $arr["class"];
		$attribute    = $arr["attribute"];
		$dept         = $arr["dept"];
		$room         = $arr["room"];
		$page         = $arr["page"];
		$max_page     = $arr["max_page"];

		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$page = ($page - 1) * 15;

		$sql   = "select ";
		$sql  .= "b.*, ";
		$sql  .= "d.emp_lt_nm as apply_lt_nm, ";
		$sql  .= "d.emp_ft_nm as apply_ft_nm, ";
		$sql  .= "a.confirmed_flg, ";
		$sql  .= "a.send_date, ";
		$sql  .= "c.emp_lt_nm as send_lt_nm, ";
		$sql  .= "c.emp_ft_nm as send_ft_nm, ";
		$sql  .= "e.wkfw_nm, ";
		$sql  .= "e.wkfw_title, ";
		$sql  .= "e.short_wkfw_name, ";
		$sql  .= "e.wkfw_folder_id ";
		$sql  .= "from jnl_applynotice a ";
		$sql  .= "inner join jnl_apply b on a.apply_id = b.apply_id ";
		$sql  .= "inner join empmst c on a.send_emp_id = c.emp_id ";
		$sql  .= "inner join empmst d on b.emp_id = d.emp_id ";
		$sql  .= "inner join ";
		$sql  .= "(select a.wkfw_id, a.wkfw_type, a.wkfw_title, b.wkfw_nm, a.short_wkfw_name, a.wkfw_folder_id ";
		$sql  .= "from jnl_wkfwmst a ";
		$sql  .= "inner join jnl_wkfwcatemst b on ";
		$sql  .= "a.wkfw_type = b.wkfw_type) e on ";
		$sql  .= "b.wkfw_id = e.wkfw_id ";

		$cond .= "where not a.delete_flg and a.send_emp_id is not null and not b.draft_flg ";
		$cond .= "and a.recv_emp_id = '$emp_id' ";

		// 申請者名
		if($apply_emp_nm != "")
		{
			$apply_emp_nm = pg_escape_string($apply_emp_nm);
			$cond .= "and ";
			$cond .= "(d.emp_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_kn_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_kn_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or (d.emp_lt_nm || d.emp_ft_nm) like '%$apply_emp_nm%' ";
			$cond .= "or (d.emp_kn_lt_nm || d.emp_kn_ft_nm) like '%$apply_emp_nm%') ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-")
		{
			$cond .= "and b.apply_stat = '$apply_stat' ";
		}

		// 部署
		if($class != "")
		{
			$cond .= "and b.emp_class = $class ";
		}

		if($attribute != "")
		{
			$cond .= "and b.emp_attribute = $attribute ";
		}

		if($dept != "")
		{
			$cond .= "and b.emp_dept = $dept ";
		}

		if($room != "")
		{
			$cond .= "and b.emp_room = $room ";
		}

		$cond .= "order by a.send_date desc, b.apply_date desc ";
		$cond .= "offset $page limit $max_page ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}


	// 申請結果通知一覧件数取得
	function get_applynotice_list_count($arr)
	{

		$session      = $arr["session"];
		$apply_emp_nm = $arr["apply_emp_nm"];
		$apply_stat   = $arr["apply_stat"];
		$class        = $arr["class"];
		$attribute    = $arr["attribute"];
		$dept         = $arr["dept"];
		$room         = $arr["room"];

		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];


		$sql   = "select ";
		$sql  .= "count(a.apply_id) as cnt ";
		$sql  .= "from jnl_applynotice a ";
		$sql  .= "inner join jnl_apply b on a.apply_id = b.apply_id ";
		$sql  .= "inner join empmst c on a.send_emp_id = c.emp_id ";
		$sql  .= "inner join empmst d on b.emp_id = d.emp_id ";
		$sql  .= "inner join jnl_wkfwmst e on b.wkfw_id = e.wkfw_id ";

		$cond .= "where not a.delete_flg and a.send_emp_id is not null and not b.draft_flg ";
		$cond .= "and a.recv_emp_id = '$emp_id' ";

		// 申請者名
		if($apply_emp_nm != "")
		{
			$apply_emp_nm = pg_escape_string($apply_emp_nm);
			$cond .= "and ";
			$cond .= "(d.emp_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_kn_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_kn_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or (d.emp_lt_nm || d.emp_ft_nm) like '%$apply_emp_nm%' ";
			$cond .= "or (d.emp_kn_lt_nm || d.emp_kn_ft_nm) like '%$apply_emp_nm%') ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-")
		{
			$cond .= "and b.apply_stat = '$apply_stat' ";
		}

		// 部署
		if($class != "")
		{
			$cond .= "and b.emp_class = $class ";
		}

		if($attribute != "")
		{
			$cond .= "and b.emp_attribute = $attribute ";
		}

		if($dept != "")
		{
			$cond .= "and b.emp_dept = $dept ";
		}

		if($room != "")
		{
			$cond .= "and b.emp_room = $room ";
		}

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}

	// 申請結果通知一覧未確認件数取得
	function get_non_confirmed_notice_count($session)
	{
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$sql   = "select ";
		$sql  .= "count(a.apply_id) as cnt ";
		$sql  .= "from jnl_applynotice a ";
		$cond .= "where not a.delete_flg and not a.confirmed_flg ";
		$cond .= "and a.send_emp_id is not null and a.recv_emp_id = '$emp_id' ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}

	// 確認済みフラグ更新
	function update_confirmed_flg($apply_id, $session)
	{
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$sql = "update jnl_applynotice set";
		$set = array("confirmed_flg");
		$setvalue = array("t");
		$cond = "where not confirmed_flg and apply_id = $apply_id and recv_emp_id = '$emp_id' ";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

	}

//-------------------------------------------------------------------------------------------------------------------------
// 申請参照一覧関連
//-------------------------------------------------------------------------------------------------------------------------
	// 申請書件数取得(下書きは除く)
	function get_all_apply_count($delete_flg)
	{
		$sql  = "select count(*) as cnt from jnl_apply ";
		$cond = "where not draft_flg and delete_flg = '$delete_flg' ";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}

	// 申請書取得(下書きは除く)
	function get_apply_list($delete_flg, $page, $max_page, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition)
	{

		$page = ($page - 1) * 20;

		$category           = $arr_condition["category"];
		$workflow           = $arr_condition["workflow"];
		$apply_title        = $arr_condition["apply_title"];
		$apply_emp_nm       = $arr_condition["apply_emp_nm"];
		$apply_yyyy_from    = $arr_condition["date_y1"];
		$apply_mm_from      = $arr_condition["date_m1"];
		$apply_dd_from      = $arr_condition["date_d1"];
		$apply_yyyy_to      = $arr_condition["date_y2"];
		$apply_mm_to        = $arr_condition["date_m2"];
		$apply_dd_to        = $arr_condition["date_d2"];
		$apply_stat         = $arr_condition["apply_stat"];
		$class              = $arr_condition["class"];
		$attribute          = $arr_condition["attribute"];
		$dept               = $arr_condition["dept"];
		$room               = $arr_condition["room"];
		$apply_content      = $arr_condition["apply_content"];

		$sql   = "select ";
		$sql  .= "a.*, ";
		$sql  .= "b.wkfw_title, ";
		$sql  .= "b.short_wkfw_name, ";
		$sql  .= "c.emp_lt_nm, ";
		$sql  .= "c.emp_ft_nm ";
		$sql  .= "from jnl_apply a ";
		$sql  .= "left join jnl_wkfwmst b on a.wkfw_id = b.wkfw_id ";
		$sql  .= "inner join empmst c on a.emp_id = c.emp_id ";
		$cond .= "where not a.draft_flg and a.delete_flg = '$delete_flg' ";

		if($selected_cate != "")
		{
			$cond .= "and b.wkfw_type = $selected_cate ";
		}

		if($selected_folder != "")
		{
			$cond .= "and b.wkfw_folder_id = $selected_folder ";
		}

		if($selected_wkfw_id != "")
		{
			$cond .= "and b.wkfw_id = $selected_wkfw_id ";
		}


		// カテゴリ
		if($category != "" && $category != "-")
		{
			$cond .= "and b.wkfw_type = $category ";
		}

		// 申請書名
		if($workflow != "" &&  $workflow != "-")
		{
			$cond .= "and b.wkfw_id = $workflow ";
		}

		// 表題
		if($apply_title != "")
		{
			$apply_title = pg_escape_string($apply_title);
			$cond .= "and a.apply_title like '%$apply_title%' ";
		}

		// 申請者名
		if($apply_emp_nm != "")
		{
			$apply_emp_nm = pg_escape_string($apply_emp_nm);
			$cond .= "and ";
			$cond .= "(c.emp_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_kn_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_kn_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or (c.emp_lt_nm || c.emp_ft_nm) like '%$apply_emp_nm%' ";
			$cond .= "or (c.emp_kn_lt_nm || c.emp_kn_ft_nm) like '%$apply_emp_nm%') ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-")
		{
			$cond .= "and a.apply_stat = '$apply_stat' ";
		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) <= '$date_str' ";
		}


		// 部署
		if($class != "")
		{
			$cond .= "and a.emp_class = $class ";
		}

		if($attribute != "")
		{
			$cond .= "and a.emp_attribute = $attribute ";
		}

		if($dept != "")
		{
			$cond .= "and a.emp_dept = $dept ";
		}

		if($room != "")
		{
			$cond .= "and a.emp_room = $room ";
		}

		// 文字列検索
		if ($apply_content != "") {
			$search_keys = split(" ", $apply_content);
			for ($i = 0, $j = count($search_keys); $i < $j; $i++) {
				$tmp_cond[] = "a.apply_content like '%{$search_keys[$i]}%'";
			}
			$cond .= "and (" . join(" or ", $tmp_cond) . ")";
		}

		$cond .= "order by a.apply_date desc, a.apply_no desc ";
		$cond .= "offset $page limit $max_page ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}

	// 申請書件数取得(下書きは除く)
	function get_apply_list_count($delete_flg, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition)
	{

		$category           = $arr_condition["category"];
		$workflow           = $arr_condition["workflow"];
		$apply_title        = $arr_condition["apply_title"];
		$apply_emp_nm       = $arr_condition["apply_emp_nm"];
		$apply_yyyy_from    = $arr_condition["date_y1"];
		$apply_mm_from      = $arr_condition["date_m1"];
		$apply_dd_from      = $arr_condition["date_d1"];
		$apply_yyyy_to      = $arr_condition["date_y2"];
		$apply_mm_to        = $arr_condition["date_m2"];
		$apply_dd_to        = $arr_condition["date_d2"];
		$apply_stat         = $arr_condition["apply_stat"];
		$class              = $arr_condition["class"];
		$attribute          = $arr_condition["attribute"];
		$dept               = $arr_condition["dept"];
		$room               = $arr_condition["room"];
		$apply_content      = $arr_condition["apply_content"];

		$sql   = "select ";
		$sql  .= "count(a.apply_id) as cnt ";
		$sql  .= "from jnl_apply a ";
		$sql  .= "left join jnl_wkfwmst b on a.wkfw_id = b.wkfw_id ";
		$sql  .= "inner join empmst c on a.emp_id = c.emp_id ";
		$cond .= "where not a.draft_flg and a.delete_flg = '$delete_flg' ";

		if($selected_cate != "")
		{
			$cond .= "and b.wkfw_type = $selected_cate ";
		}

		if($selected_folder != "")
		{
			$cond .= "and b.wkfw_folder_id = $selected_folder ";
		}

		if($selected_wkfw_id != "")
		{
			$cond .= "and b.wkfw_id = $selected_wkfw_id ";
		}

		// カテゴリ
		if($category != "" && $category != "-")
		{
			$cond .= "and b.wkfw_type = $category ";
		}

		// 申請書名
		if($workflow != "" &&  $workflow != "-")
		{
			$cond .= "and b.wkfw_id = $workflow ";
		}

		// 表題
		if($apply_title != "")
		{
			$apply_title = pg_escape_string($apply_title);
			$cond .= "and a.apply_title like '%$apply_title%' ";
		}

		// 申請者名
		if($apply_emp_nm != "")
		{
			$apply_emp_nm = pg_escape_string($apply_emp_nm);
			$cond .= "and ";
			$cond .= "(c.emp_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_kn_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_kn_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or (c.emp_lt_nm || c.emp_ft_nm) like '%$apply_emp_nm%' ";
			$cond .= "or (c.emp_kn_lt_nm || c.emp_kn_ft_nm) like '%$apply_emp_nm%') ";
		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) <= '$date_str' ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-")
		{
			$cond .= "and a.apply_stat = '$apply_stat' ";
		}

		// 部署
		if($class != "")
		{
			$cond .= "and a.emp_class = $class ";
		}

		if($attribute != "")
		{
			$cond .= "and a.emp_attribute = $attribute ";
		}

		if($dept != "")
		{
			$cond .= "and a.emp_dept = $dept ";
		}

		if($room != "")
		{
			$cond .= "and a.emp_room = $room ";
		}

		// 文字列検索
		if ($apply_content != "") {
			$search_keys = split(" ", $apply_content);
			for ($i = 0, $j = count($search_keys); $i < $j; $i++) {
				$tmp_cond[] = "a.apply_content like '%{$search_keys[$i]}%'";
			}
			$cond .= "and (" . join(" or ", $tmp_cond) . ")";
		}

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0,"cnt");
	}


//-------------------------------------------------------------------------------------------------------------------------
// テンプレート履歴およびワークファイル履歴関連
//-------------------------------------------------------------------------------------------------------------------------

	// テンプレート履歴登録
	function regist_wkfw_template_history($wkfw_id, $wkfw_history_no, $wkfw_content)
	{
		$sql = "insert into jnl_wkfw_template_history (wkfw_id, wkfw_history_no, wkfw_content) values (";
		$content = array($wkfw_id, $wkfw_history_no, pg_escape_string($wkfw_content));
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// テンプレート履歴取得
	function get_wkfw_template_history($wkfw_id, $wkfw_history_no)
	{
		$sql  = "select * from jnl_wkfw_template_history ";
		$cond = "where wkfw_id = $wkfw_id and wkfw_history_no = $wkfw_history_no";
		$sel  = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}


		$wkfw_history_no = pg_fetch_result($sel, 0, "wkfw_history_no");
		$wkfw_content    = pg_fetch_result($sel, 0, "wkfw_content");

		$arr = array("wkfw_history_no" => $wkfw_history_no, "wkfw_content" => $wkfw_content);
		return $arr;
	}

	// テンプレート履歴No(ＭＡＸ値)取得
	function get_max_wkfw_history_no($wkfw_id)
	{
		$sql  = "select max(wkfw_history_no) as max from jnl_wkfw_template_history ";
		$cond = "where wkfw_id = $wkfw_id ";
		$sel  = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}

	// ワークファイル履歴登録
	function regist_wkfwfile_history($wkfw_id, $wkfwfile_no, $wkfwfile_history_no, $wkfwfile_name)
	{
		$sql = "insert into jnl_wkfwfile_history (wkfw_id, wkfwfile_no, wkfwfile_history_no, wkfwfile_name) values (";
		$content = array($wkfw_id, $wkfwfile_no, $wkfwfile_history_no, pg_escape_string($wkfwfile_name));
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークファイル履歴取得
	function get_wkfwfile_history($wkfw_id, $wkfwfile_history_no)
	{
		$sql  = "select * from jnl_wkfwfile_history ";
		$cond = "where wkfw_id = $wkfw_id and jnl_wkfwfile_history_no = $wkfwfile_history_no order by wkfwfile_no ";
		$sel  = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfwfile_history_no" => $row["wkfwfile_history_no"], "wkfwfile_no" => $row["wkfwfile_no"], "wkfwfile_name" => $row["wkfwfile_name"]);
		}
		return $arr;
	}

	// ワークファイル履歴No(ＭＡＸ値)取得
	function get_max_wkfwfile_history_no($wkfw_id)
	{
		$sql  = "select max(wkfwfile_history_no) as max from jnl_wkfwfile_history ";
		$cond = "where wkfw_id = $wkfw_id ";
		$sel  = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}

	// ワークファイルNo(ＭＡＸ値)取得
	function get_max_wkfwfile_no($wkfw_id)
	{
		$sql  = "select max(wkfwfile_no) as max from jnl_wkfwfile_history ";
		$cond = "where wkfw_id = $wkfw_id ";
		$sel  = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}

//-------------------------------------------------------------------------------------------------------------------------
// ワークフロー・本体情報関連
//-------------------------------------------------------------------------------------------------------------------------

	// 削除済みワークフロー取得
	function get_deleted_workflow_real()
	{
		$sql  = "select wkfw_id, wkfw_title from jnl_wkfwmst_real";
		$cond = "where wkfw_del_flg order by wkfw_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_id" => $row["wkfw_id"], "wkfw_title" => $row["wkfw_title"]);
		}
		return $arr;
	}

	// 削除済みワークフロー数取得
	function get_deleted_workflow_real_cnt()
	{
		$sql  = "select count(*) as cnt from jnl_wkfwmst_real";
		$cond = "where wkfw_del_flg";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$num = pg_fetch_result($sel, 0, "cnt");
		return $num;
	}

	// すべてフォルダ用ワークフロー取得
	function get_wkfwmst_real()
	{
		$sql  = "select wkfw_id, wkfw_title from jnl_wkfwmst_real";
		$cond = "where not wkfw_del_flg order by wkfw_id";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_id" => $row["wkfw_id"], "wkfw_title" => $row["wkfw_title"]);
		}
		return $arr;
	}

	// すべてフォルダ用ワークフロー数取得
	function get_wkfwmst_real_cnt()
	{
		$sql  = "select count(*) as cnt from jnl_wkfwmst_real";
		$cond = "where not wkfw_del_flg";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$num = pg_fetch_result($sel, 0, "cnt");
		return $num;
	}

	// ワークフローＩＤのＭＡＸ値取得
	function get_max_wkfw_id($mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "select max(wkfw_id) as max from jnl_wkfwmst";
		}
		else
		{
			$sql = "select max(wkfw_id) as max from jnl_wkfwmst_real";
		}
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);

		if ($sel == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "max");
	}

	// エイリアスワークフローＩＤ取得
	function get_alias_wkfw_id($real_wkfw_id)
	{
		$sql  = "select alias_wkfw_id from jnl_wkfwaliasmng";
		$cond = "where real_wkfw_id = $real_wkfw_id order by alias_wkfw_no asc";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("alias_wkfw_id" => $row["alias_wkfw_id"]);
		}
		return $arr;
	}

	// エイリアス管理登録
	function regist_wkfwaliasmng($real_wkfw_id, $alias_wkfw_id, $alias_wkfw_no)
	{
		$sql = "insert into jnl_wkfwaliasmng (real_wkfw_id, alias_wkfw_id, alias_wkfw_no, delete_flg) values (";
		$content = array($real_wkfw_id, $alias_wkfw_id, $alias_wkfw_no, "f");
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// エイリアスワークフローＮｏ(ＭＡＸ)取得
	function get_max_alias_wkfw_no($real_wkfw_id)
	{
		$sql  = "select max(alias_wkfw_no) from jnl_wkfwaliasmng";
		$cond = "where real_wkfw_id = $real_wkfw_id";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}

//-------------------------------------------------------------------------------------------------------------------------
// エイリアスワークフローコピー処理
//-------------------------------------------------------------------------------------------------------------------------

	// ワークフロー情報登録
	function regist_copy_wkfwmst($real_wkfw_id, $alias_wkfw_id, $wkfw_type, $wkfw_folder_id)
	{
		$wkfw_folder_id = ($wkfw_folder_id == "") ? "null" : $wkfw_folder_id;

		$sql  = "insert into jnl_wkfwmst( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "$wkfw_type, ";
		$sql .= "wkfw_title, ";
		$sql .= "wkfw_content, ";
		$sql .= "'f', ";
		$sql .= "wkfw_start_date, ";
		$sql .= "wkfw_end_date, ";
		$sql .= "wkfw_appr, ";
		$sql .= "wkfw_content_type, ";
		$sql .= "$wkfw_folder_id, ";
		$sql .= "ref_dept_st_flg, ";
		$sql .= "ref_dept_flg, ";
		$sql .= "ref_st_flg, ";
		$sql .= "short_wkfw_name, ";
		$sql .= "apply_title_disp_flg, ";
		$sql .= "send_mail_flg, ";
		$sql .= "approve_label, ";
		$sql .= "lib_reg_flg, ";
		$sql .= "lib_keyword, ";
		$sql .= "lib_no, ";
		$sql .= "lib_summary, ";
		$sql .= "lib_archive, ";
		$sql .= "lib_cate_id, ";
		$sql .= "lib_folder_id, ";
		$sql .= "lib_show_login_flg, ";
		$sql .= "lib_show_login_begin, ";
		$sql .= "lib_show_login_end, ";
		$sql .= "lib_private_flg, ";
		$sql .= "lib_ref_dept_st_flg, ";
		$sql .= "lib_ref_dept_flg, ";
		$sql .= "lib_ref_st_flg, ";
		$sql .= "lib_upd_dept_st_flg, ";
		$sql .= "lib_upd_dept_flg, ";
		$sql .= "lib_upd_st_flg ";
		$sql .= "from jnl_wkfwmst_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";
		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認者管理登録
	function regist_copy_wkfwapvmng($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into jnl_wkfwapvmng( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "apv_order, ";
		$sql .= "deci_flg, ";
		$sql .= "target_class_div, ";
		$sql .= "multi_apv_flg, ";
		$sql .= "next_notice_div, ";
		$sql .= "apv_div0_flg, ";
		$sql .= "apv_div1_flg, ";
		$sql .= "apv_div2_flg, ";
		$sql .= "apv_div3_flg, ";
		$sql .= "apv_div4_flg, ";
		$sql .= "apv_num ";
		$sql .= "from jnl_wkfwapvmng_real ";
		$sql .= "where wkfw_id = $real_wkfw_id order by apv_order";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定登録
	function regist_copy_wkfwapvdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into jnl_wkfwapvdtl( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "apv_order, ";
		$sql .= "emp_id, ";
		$sql .= "apv_sub_order ";
		$sql .= "from jnl_wkfwapvdtl_real ";
		$sql .= "where wkfw_id = $real_wkfw_id order by apv_order, apv_sub_order";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職(申請者所属)指定登録
	function regist_copy_wkfwapvpstdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into jnl_wkfwapvpstdtl( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "apv_order, ";
		$sql .= "st_id, ";
		$sql .= "st_div ";
		$sql .= "from jnl_wkfwapvpstdtl_real ";
		$sql .= "where wkfw_id = $real_wkfw_id order by apv_order ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ指定登録
	function regist_copy_wkfwpjtdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into jnl_wkfwpjtdtl( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "apv_order, ";
		$sql .= "parent_pjt_id, ";
		$sql .= "child_pjt_id ";
		$sql .= "from jnl_wkfwpjtdtl_real ";
		$sql .= "where wkfw_id = $real_wkfw_id order by apv_order ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職(部署指定)指定登録
	function regist_copy_wkfwapvsectdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into jnl_wkfwapvsectdtl( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "apv_order, ";
		$sql .= "class_id, ";
		$sql .= "atrb_id, ";
		$sql .= "dept_id, ";
		$sql .= "room_id ";
		$sql .= "from jnl_wkfwapvsectdtl_real ";
		$sql .= "where wkfw_id = $real_wkfw_id order by apv_order ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 結果通知管理登録
	function regist_copy_wkfwnoticemng($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into jnl_wkfwnoticemng( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "target_class_div, ";
		$sql .= "rslt_ntc_div0_flg, ";
		$sql .= "rslt_ntc_div1_flg, ";
		$sql .= "rslt_ntc_div2_flg, ";
		$sql .= "rslt_ntc_div3_flg, ";
		$sql .= "rslt_ntc_div4_flg ";
		$sql .= "from jnl_wkfwnoticemng_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定(結果通知)登録
	function regist_copy_wkfwnoticedtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into jnl_wkfwnoticedtl( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "emp_id ";
		$sql .= "from jnl_wkfwnoticedtl_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)登録
	function regist_copy_wkfwnoticestdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into jnl_wkfwnoticestdtl( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "st_id, ";
		$sql .= "st_div ";
		$sql .= "from jnl_wkfwnoticestdtl_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ指定(結果通知)登録
	function regist_copy_wkfwnoticepjtdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into jnl_wkfwnoticepjtdtl( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "parent_pjt_id, ";
		$sql .= "child_pjt_id ";
		$sql .= "from jnl_wkfwnoticepjtdtl_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)(部署指定)登録
	function regist_copy_wkfwnoticesectdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into jnl_wkfwnoticesectdtl( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "class_id, ";
		$sql .= "atrb_id, ";
		$sql .= "dept_id, ";
		$sql .= "room_id ";
		$sql .= "from jnl_wkfwnoticesectdtl_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(ワークフロー用)登録
	function regist_copy_wkfwfprecond($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into jnl_wkfwfprecond( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "precond_order, ";
		$sql .= "precond_wkfw_id ";
		$sql .= "from jnl_wkfwfprecond_real ";
		$sql .= "where wkfw_id = $real_wkfw_id order by precond_order";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー用アクセス権（科）登録
	function regist_copy_wkfw_refdept($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into jnl_wkfw_refdept( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "class_id, ";
		$sql .= "atrb_id, ";
		$sql .= "dept_id ";
		$sql .= "from jnl_wkfw_refdept_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー用アクセス権（役職）登録
	function regist_copy_wkfw_refst($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into jnl_wkfw_refst( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "st_id ";
		$sql .= "from jnl_wkfw_refst_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー用アクセス権（職員）登録
	function regist_copy_wkfw_refemp($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into jnl_wkfw_refemp( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "emp_id ";
		$sql .= "from jnl_wkfw_refemp_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// フォーマットファイル登録
	function regist_copy_wkfwfile($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into jnl_wkfwfile( ";
		$sql .= "select $alias_wkfw_id, ";
		$sql .= "wkfwfile_no, ";
		$sql .= "wkfwfile_name ";
		$sql .= "from jnl_wkfwfile_real ";
		$sql .= "where wkfw_id = $real_wkfw_id ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// ワークフロー・ドラッグアンドドロップ処理
//-------------------------------------------------------------------------------------------------------------------------

	// 指定フォルダの子フォルダ取得
	function get_child_folder($wkfw_parent_id, &$res)
	{
		$arr_wkfw_child_folder = $this->get_wkfw_child_folder($wkfw_parent_id);
		foreach($arr_wkfw_child_folder as $wkfw_child_folder)
		{
			$res[] = $wkfw_child_folder;

			$this->get_child_folder($wkfw_child_folder, $res);
		}
	}

	// 子フォルダ取得
	function get_wkfw_child_folder($wkfw_parent_id)
	{
		$sql  = "select wkfw_child_id from jnl_wkfwtree ";
		$cond = "where wkfw_parent_id = $wkfw_parent_id and not wkfw_tree_del_flg ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = $row["wkfw_child_id"];
		}
		return $arr;
	}

	// 指定フォルダの親フォルダ取得
	function get_parent_folder($wkfw_child_id)
	{
		// 親フォルダ情報を取得
		$sql = "select wkfw_parent_id from jnl_wkfwtree";
		$cond = "where wkfw_child_id = $wkfw_child_id and not wkfw_tree_del_flg ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "wkfw_parent_id");
	}

	// カテゴリ論理削除
	function update_wkfwcatemst_delflg($wkfw_type, $wkfwcate_del_flg)
	{
		$sql = "update jnl_wkfwcatemst set";
		$set = array("wkfwcate_del_flg");
		$setvalue = array($wkfwcate_del_flg);
		$cond = "where wkfw_type = $wkfw_type";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダ取得(wkfw_folder_idの最大値取得)
	function get_max_wkfw_folder_id()
	{
		$sql = "select max(wkfw_folder_id) as max from jnl_wkfwfolder";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "max");
	}

	// カテゴリ取得(wkfw_typeの最大値取得)
	function get_max_wkfw_type()
	{
		$sql = "select max(wkfw_type) as max from jnl_wkfwcatemst";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "max");
	}

	// ワークフローフォルダ登録
	function regist_wkfwfolder($arr)
	{
		$wkfw_folder_id      = $arr["wkfw_folder_id"];
		$wkfw_type           = $arr["wkfw_type"];
		$wkfw_folder_name    = $arr["wkfw_folder_name"];
		$ref_dept_st_flg     = $arr["ref_dept_st_flg"];
		$ref_dept_flg        = $arr["ref_dept_flg"];
		$ref_st_flg          = $arr["ref_st_flg"];

		$sql = "insert into jnl_wkfwfolder (wkfw_folder_id, wkfw_type, wkfw_folder_name, wkfw_folder_del_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg) values (";
		$content = array($wkfw_folder_id, $wkfw_type, pg_escape_string($wkfw_folder_name), "f", $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg);
		$result = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($result == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダ用アクセス権（科）登録
	function regist_wkfwfolder_refdept($wkfw_folder_id, $class_id, $atrb_id, $dept_id)
	{
		$sql = "insert into jnl_wkfwfolder_refdept (wkfw_folder_id, class_id, atrb_id, dept_id) values (";
		$content = array($wkfw_folder_id, $class_id, $atrb_id, $dept_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダ用アクセス権（役職）登録
	function regist_wkfwfolder_refst($wkfw_folder_id, $st_id)
	{
		$sql = "insert into jnl_wkfwfolder_refst (wkfw_folder_id, st_id) values (";
		$content = array($wkfw_folder_id, $st_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダ用アクセス権（職員）登録
	function regist_wkfwfolder_refemp($wkfw_folder_id, $emp_id)
	{
		$sql = "insert into jnl_wkfwfolder_refemp (wkfw_folder_id, emp_id) values (";
		$content = array($wkfw_folder_id, $emp_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー申請書・ワークフロータイプ更新
	function update_wkfwmst_wkfw_type($src_wkfw_type, $dest_wkfw_type)
	{
		$sql = "update jnl_wkfwmst set";
		$set = array("wkfw_type");
		$setvalue = array($dest_wkfw_type);
		$cond = "where wkfw_type = $src_wkfw_type";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー申請書・ファルダＩＤ更新
	function update_wkfwmst_wkfw_folder_id($new_wkfw_folder_id, $wkfw_id)
	{
		$sql = "update jnl_wkfwmst set";
		$set = array("wkfw_folder_id");
		$setvalue = array($new_wkfw_folder_id);
		$cond = "where wkfw_id = $wkfw_id and wkfw_folder_id is null";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフローＩ取得
	function get_wkfw_id_for_categoryDD($dest_wkfw_type)
	{
		$sql  = "select wkfw_id from jnl_wkfwmst ";
		$cond = "where wkfw_type = $dest_wkfw_type and wkfw_folder_id is null and not wkfw_del_flg ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = $row["wkfw_id"];
		}
		return $arr;

	}

	// ワークフローフォルダ・ワークフロータイプ更新
	function update_wkfwfolder_wkfw_type($src_wkfw_type, $dest_wkfw_type)
	{
		$sql = "update jnl_wkfwfolder set";
		$set = array("wkfw_type");
		$setvalue = array($dest_wkfw_type);
		$cond = "where wkfw_type = $src_wkfw_type";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダツリー登録
	function regist_wkfwtree($wkfw_parent_id, $wkfw_child_id)
	{
		$sql = "insert into jnl_wkfwtree (wkfw_parent_id, wkfw_child_id, wkfw_tree_del_flg) values (";
		$content = array($wkfw_parent_id, $wkfw_child_id, "f");
		$result = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($result == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダツリー論理削除
	function update_wkfwtree_del_flg($wkfw_child_id, $wkfw_tree_del_flg)
	{
		$sql = "update jnl_wkfwtree set";
		$set = array("wkfw_tree_del_flg");
		$setvalue = array($wkfw_tree_del_flg);
		$cond = "where wkfw_child_id = $wkfw_child_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダツリー論理削除
	function update_wkfwtree_del_flg_for_parent_id($wkfw_parent_id, $wkfw_tree_del_flg)
	{
		$sql = "update jnl_wkfwtree set";
		$set = array("wkfw_tree_del_flg");
		$setvalue = array($wkfw_tree_del_flg);
		$cond = "where wkfw_parent_id = $wkfw_parent_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダツリー・親フォルダＩＤ更新
	function update_wkfwtree_wkfw_parent_id($wkfw_parent_id, $wkfw_child_id)
	{
		$sql = "update jnl_wkfwtree set";
		$set = array("wkfw_parent_id");
		$setvalue = array($wkfw_parent_id);
		$cond = "where wkfw_child_id = $wkfw_child_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダツリー・件数取得
	function get_wkfwtree_count($wkfw_child_id)
	{
		$sql  = "select count(wkfw_parent_id) as cnt from jnl_wkfwtree";
		$cond = "where not wkfw_tree_del_flg and wkfw_child_id = $wkfw_child_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "cnt");
	}

	// ワークフローフォルダＩＤ取得
	function get_wkfwfolder_from_wkfw_type($wkfw_type)
	{
		$sql  = "select wkfw_folder_id from jnl_wkfwfolder";
		$cond = "where not wkfw_folder_del_flg and wkfw_type = $wkfw_type order by wkfw_folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = $row["wkfw_folder_id"];
		}
		return $arr;
	}


	// ワークフローフォルダ・ワークフロータイプ更新
	function update_wkfwfolder_wkfw_type_from_folder_id($wkfw_folder_id, $dest_wkfw_type)
	{
		$sql = "update jnl_wkfwfolder set";
		$set = array("wkfw_type");
		$setvalue = array($dest_wkfw_type);
		$cond = "where wkfw_folder_id = $wkfw_folder_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー申請書・ワークフロータイプ更新
	function update_wkfwmst_wkfw_type_from_folder_id($src_wkfw_type, $wkfw_folder_id, $dest_wkfw_type)
	{
		$sql = "update jnl_wkfwmst set";
		$set = array("wkfw_type");
		$setvalue = array($dest_wkfw_type);
		$cond = "where wkfw_type = $src_wkfw_type and wkfw_folder_id = $wkfw_folder_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// カテゴリ登録
	function regist_wkfwcatemst($wkfw_type, $category_name, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg)
	{
		$sql = "insert into jnl_wkfwcatemst (wkfw_type, wkfw_nm, wkfwcate_del_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg) values (";
		$content = array($wkfw_type, pg_escape_string($category_name), "f", $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg);
		$result = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($result == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// カテゴリ用アクセス権（科）登録
	function regist_wkfwcate_refdept($wkfw_type, $class_id, $atrb_id, $dept_id)
	{
		$sql = "insert into jnl_wkfwcate_refdept (wkfw_type, class_id, atrb_id, dept_id) values (";
		$content = array($wkfw_type, $class_id, $atrb_id, $dept_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// カテゴリ用アクセス権（役職）登録
	function regist_wkfwcate_refst($wkfw_type, $st_id)
	{
		$sql = "insert into jnl_wkfwcate_refst (wkfw_type, st_id) values (";
		$content = array($wkfw_type, $st_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

    // カテゴリ用アクセス権（職員）登録
	function regist_wkfwcate_refemp($wkfw_type, $emp_id)
	{
		$sql = "insert into jnl_wkfwcate_refemp (wkfw_type, emp_id) values (";
		$content = array($wkfw_type, $emp_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// カテゴリフォルダ論理削除
	function update_wkfwfolder_del_flg($wkfw_folder_id, $del_flg)
	{
		$sql = "update jnl_wkfwfolder set";
		$set = array("wkfw_folder_del_flg");
		$setvalue = array($del_flg);
		$cond = "where wkfw_folder_id = $wkfw_folder_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー申請書・ワークフロータイプ、フォルダーＩＤ更新
	function update_wkfwmst_wkfw_type_folder_id($src_wkfw_type, $wkfw_folder_id, $dest_wkfw_type)
	{
		$sql = "update jnl_wkfwmst set";
		$set = array("wkfw_type", "wkfw_folder_id");
		$setvalue = array($dest_wkfw_type, null);
		$cond = "where wkfw_type = $src_wkfw_type and wkfw_folder_id = $wkfw_folder_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}


	// カテゴリ移動
	function move_category($src_wkfw_type, $dest_wkfw_type, $dest_wkfw_folder_id)
	{
		// カテゴリ情報取得
	    $arr_wkfwcate_mst = $this->get_wkfwcate_mst($src_wkfw_type);

		// カテゴリ論理削除
		$this->update_wkfwcatemst_delflg($src_wkfw_type, "t");

		// フォルダ新規作成
		$max_wkfw_folder_id = $this->get_max_wkfw_folder_id();
		$new_wkfw_folder_id = intval($max_wkfw_folder_id) + 1;

		$arr = array
		(
			"wkfw_folder_id"     => $new_wkfw_folder_id,
			"wkfw_type"          => $dest_wkfw_type,
			"wkfw_folder_name"   => $arr_wkfwcate_mst["wkfw_nm"],
			"ref_dept_st_flg"    => $arr_wkfwcate_mst["ref_dept_st_flg"],
			"ref_dept_flg"       => $arr_wkfwcate_mst["ref_dept_flg"],
			"ref_st_flg"         => $arr_wkfwcate_mst["ref_st_flg"]
		);
		$this->regist_wkfwfolder($arr);

		// フォルダのアクセス権登録
		// カテゴリ用アクセス権（科）取得
		$arr_wkfwcate_refdept = $this->get_wkfwcate_refdept($src_wkfw_type);
		foreach($arr_wkfwcate_refdept as $wkfwcate_refdept)
		{
			$this->regist_wkfwfolder_refdept($new_wkfw_folder_id, $wkfwcate_refdept["class_id"], $wkfwcate_refdept["atrb_id"], $wkfwcate_refdept["dept_id"]);
		}

		// カテゴリ用アクセス権（役職）取得
		$arr_wkfwcate_refst = $this->get_wkfwcate_refst($src_wkfw_type);
		foreach($arr_wkfwcate_refst as $wkfwcate_refst)
		{
			$this->regist_wkfwfolder_refst($new_wkfw_folder_id, $wkfwcate_refst["st_id"]);
		}

	    // カテゴリ用アクセス権（職員）取得
		$arr_wkfwcate_refemp = $this->get_wkfwcate_refemp($src_wkfw_type);
		foreach($arr_wkfwcate_refemp as $wkfwcate_refemp)
		{
			$this->regist_wkfwfolder_refemp($new_wkfw_folder_id, $wkfwcate_refemp["emp_id"]);
		}

		// 遷移元カテゴリの直下にあるフォルダを取得。
		$arr_wkfwfolder = $this->get_wkfwfolder_from_wkfw_type($src_wkfw_type);
		foreach($arr_wkfwfolder as $wkfw_folder_id)
		{
			$wkfwtree_count = $this->get_wkfwtree_count($wkfw_folder_id);

			if($wkfwtree_count == 0)
			{
				// ワークフローツリー登録
				$this->regist_wkfwtree($new_wkfw_folder_id, $wkfw_folder_id);
			}
		}

		// 遷移先がフォルダの場合
		if($dest_wkfw_folder_id != "")
		{
			$this->regist_wkfwtree($dest_wkfw_folder_id, $new_wkfw_folder_id);
		}

		// ワークフローフォルダ・ワークフロータイプ更新
		$this->update_wkfwfolder_wkfw_type($src_wkfw_type, $dest_wkfw_type);

		// ワークフローＩＤ取得
		$arr_wkfw_id = $this->get_wkfw_id_for_categoryDD($src_wkfw_type);

		// ワークフロー申請書・ワークフロータイプ更新
		$this->update_wkfwmst_wkfw_type($src_wkfw_type, $dest_wkfw_type);

		// ワークフロー申請書・ファルダＩＤ	更新
		foreach($arr_wkfw_id as $wkfw_id)
		{
			$this->update_wkfwmst_wkfw_folder_id($new_wkfw_folder_id, $wkfw_id);
		}
	}




	// フォルダ移動
	function move_folder($src_wkfw_type, $src_wkfw_folder_id, $dest_wkfw_type, $dest_wkfw_folder_id, $root_flg)
	{
		// 遷移先がルートの場合
		if($root_flg)
		{
			// 遷移元フォルダ情報取得
			$arr_wkfwfolder_mst = $this->get_wkfwfolder_mst($src_wkfw_folder_id);

			// 遷移先カテゴリ新規作成
			$max_wkfw_type = $this->get_max_wkfw_type();
			$new_wkfw_type = intval($max_wkfw_type) + 1;

			$this->regist_wkfwcatemst($new_wkfw_type,
			                          $arr_wkfwfolder_mst["wkfw_folder_name"],
			                          $arr_wkfwfolder_mst["ref_dept_st_flg"],
			                          $arr_wkfwfolder_mst["ref_dept_flg"],
			                          $arr_wkfwfolder_mst["ref_st_flg"]);

			// 遷移先カテゴリアクセス権作成
			// 遷移元のフォルダ用アクセス権（科）取得、カテゴリ用アクセス権（科）登録
			$arr_wkfwfolder_refdept = $this->get_wkfwfolder_refdept($src_wkfw_folder_id);
			foreach($arr_wkfwfolder_refdept as $wkfwfolder_refdept)
			{
				$this->regist_wkfwcate_refdept($new_wkfw_type, $wkfwfolder_refdept["class_id"], $wkfwfolder_refdept["atrb_id"], $wkfwfolder_refdept["dept_id"]);
			}

			// フォルダ用アクセス権（役職）取得、カテゴリ用アクセス権（役職）登録
			$arr_wkfwfolder_refst = $this->get_wkfwfolder_refst($src_wkfw_folder_id);
			foreach($arr_wkfwfolder_refst as $wkfwfolder_refst)
			{
				$this->regist_wkfwcate_refst($new_wkfw_type, $wkfwfolder_refst["st_id"]);
			}
			// フォルダ用アクセス権（職員）取得、カテゴリ用アクセス権（職員）登録
			$arr_wkfwfolder_refemp = $this->get_wkfwfolder_refemp($src_wkfw_folder_id);
			foreach($arr_wkfwfolder_refemp as $wkfwfolder_refemp)
			{
				$this->regist_wkfwcate_refemp($new_wkfw_type, $wkfwfolder_refemp["emp_id"]);
			}

			// 遷移元フォルダ論理削除
			$this->update_wkfwfolder_del_flg($src_wkfw_folder_id, "t");

			// 遷移元フォルダの子フォルダを取得
			$arr_child_folder = array();
			$this->get_child_folder($src_wkfw_folder_id, $arr_child_folder);

			// 遷移元フォルダが親または子フォルダをもっている場合、ワークフローツリー論理削除
			$this->update_wkfwtree_del_flg($src_wkfw_folder_id, "t");
			$this->update_wkfwtree_del_flg_for_parent_id($src_wkfw_folder_id, "t");

			// ワークフローフォルダ・ワークフロータイプ更新(子フォルダ)
			foreach($arr_child_folder as $child_folder_id)
			{
				$this->update_wkfwfolder_wkfw_type_from_folder_id($child_folder_id, $new_wkfw_type);
			}

			// ワークフロー申請書・ワークフロータイプ更新
			$this->update_wkfwmst_wkfw_type_folder_id($src_wkfw_type, $src_wkfw_folder_id, $new_wkfw_type);
			foreach($arr_child_folder as $child_folder_id)
			{
				$this->update_wkfwmst_wkfw_type_from_folder_id($src_wkfw_type, $child_folder_id, $new_wkfw_type);
			}

		}
		else
		{
			// 遷移先がカテゴリの場合
			if($dest_wkfw_folder_id == "")
			{
				// 遷移元フォルダが親フォルダをもっている場合、ワークフローツリー論理削除
				$this->update_wkfwtree_del_flg($src_wkfw_folder_id, "t");
			}
			// 遷移先がフォルダの場合
			else
			{
				$wkfwtree_count = $this->get_wkfwtree_count($src_wkfw_folder_id);
				if($wkfwtree_count > 0)
				{
					// ワークフローツリー更新
					$this->update_wkfwtree_wkfw_parent_id($dest_wkfw_folder_id, $src_wkfw_folder_id);
				}
				else
				{
					// ワークフローツリー登録
					$this->regist_wkfwtree($dest_wkfw_folder_id, $src_wkfw_folder_id);
				}
			}

			// 遷移元フォルダの子フォルダを取得
			$arr_child_folder = array();
			$this->get_child_folder($src_wkfw_folder_id, $arr_child_folder);

			// ワークフローフォルダ・ワークフロータイプ更新
			$this->update_wkfwfolder_wkfw_type_from_folder_id($src_wkfw_folder_id, $dest_wkfw_type);
			foreach($arr_child_folder as $child_folder_id)
			{
				$this->update_wkfwfolder_wkfw_type_from_folder_id($child_folder_id, $dest_wkfw_type);
			}

			// ワークフロー申請書・ワークフロータイプ更新
			$this->update_wkfwmst_wkfw_type_from_folder_id($src_wkfw_type, $src_wkfw_folder_id, $dest_wkfw_type);
			foreach($arr_child_folder as $child_folder_id)
			{
				$this->update_wkfwmst_wkfw_type_from_folder_id($src_wkfw_type, $child_folder_id, $dest_wkfw_type);
			}
		}
	}


//-------------------------------------------------------------------------------------------------------------------------
// 申請一覧用
//-------------------------------------------------------------------------------------------------------------------------

	// 通常申請のＳＱＬ取得
	function get_normal_apply_sql($arr_cond)
	{
		$emp_id          = $arr_cond["emp_id"];
		$wkfw_type       = $arr_cond["wkfw_type"];
		$wkfw_id         = $arr_cond["wkfw_id"];
		$apply_title     = $arr_cond["apply_title"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apply_stat      = $arr_cond["apply_stat"];

        $sql  = "select ";
        $sql .= "varchar(1) '1' as apply_type, ";
        $sql .= "A.apply_id, ";
        $sql .= "A.apply_title, ";
        $sql .= "A.apply_date, ";
        $sql .= "A.apply_stat, ";
        $sql .= "A.apply_no, ";
        $sql .= "A.apv_fix_show_flg, ";
        $sql .= "A.apv_bak_show_flg, ";
        $sql .= "A.apv_ng_show_flg, ";
        $sql .= "B.wkfw_title, ";
        $sql .= "B.wkfw_nm, ";
        $sql .= "B.short_wkfw_name, ";
        $sql .= "B.wkfw_folder_id, ";
		$sql .= "B.wkfw_id, ";
		$sql .= "null as pjt_schd_id ";
        $sql .= "from jnl_apply A ";

        $sql .= "inner join ";
        $sql .= "(select WM.wkfw_id, WM.wkfw_type, WM.wkfw_title, CATE.wkfw_nm, WM.short_wkfw_name, WM.wkfw_folder_id ";
        $sql .= "from jnl_wkfwmst WM ";
        $sql .= "inner join jnl_wkfwcatemst CATE on ";
        $sql .= "WM.wkfw_type = CATE.wkfw_type) B on A.wkfw_id = B.wkfw_id ";


		//集計用データの報告書（各日報、月報）に対して
		//入力許可（入力対象者）のある報告書はすべて表示する
		//↓
		$sql .= " , (SELECT  *  FROM ";
		$sql .= " ( SELECT emp_id, jnl_facility_id, newest_apply_id FROM jnl_hospital_day ";
		$sql .= " UNION ALL SELECT emp_id,jnl_facility_id, newest_apply_id FROM jnl_hospital_month_action ";
		$sql .= " UNION ALL SELECT emp_id,jnl_facility_id, newest_apply_id FROM jnl_hospital_month_patient ";
		$sql .= " UNION ALL SELECT emp_id,jnl_facility_id, newest_apply_id FROM jnl_nurse_home_main";
		$sql .= " UNION ALL SELECT emp_id,jnl_facility_id, newest_apply_id FROM jnl_return_main";
		$sql .= " UNION ALL SELECT emp_id,jnl_facility_id, newest_apply_id FROM jnl_reward_synthesis_medi_main";
		$sql .= " UNION ALL SELECT emp_id,jnl_facility_id, newest_apply_id FROM jnl_reward_synthesis_dental_main";
		$sql .= " UNION ALL SELECT emp_id,jnl_facility_id, newest_apply_id FROM jnl_bill_diagnosis_enter_main";
		$sql .= " UNION ALL SELECT emp_id,jnl_facility_id, newest_apply_id FROM jnl_bill_diagnosis_visit_main";
		$sql .= " UNION ALL SELECT emp_id,jnl_facility_id, newest_apply_id FROM jnl_bill_recuperation_main";
		$sql .= " UNION ALL SELECT emp_id,jnl_facility_id, newest_apply_id FROM jnl_bill_recuperation_prepay_main";
		$sql .= " ) as wjre , ";
		$sql .= " (select jnl_facility_id as jr_fac_id from jnl_facility_register where emp_id ='$emp_id') as jr";
		$sql .= " WHERE wjre.jnl_facility_id = jr.jr_fac_id ) jnl_report ";
		//↑
		//集計用データの報告書（各日報、月報）に対して
		//入力許可（入力対象者）のある報告書はすべて表示する



//		$sql .= "where A.emp_id = '$emp_id' ";
//		$sql .= "and A.re_apply_id is null ";
		$sql .= "where A.re_apply_id is null ";
		$sql .= "and jnl_report.newest_apply_id = A.apply_id ";
		$sql .= "and not A.delete_flg ";
        $sql .= "and not A.draft_flg ";

		// カテゴリ
		if($wkfw_type != "" && $wkfw_type != "-") {
			$sql .= "and B.wkfw_type = $wkfw_type ";
		}

		// 申請書名
		if($wkfw_id != "" &&  $wkfw_id != "-") {
			$sql .= "and B.wkfw_id = $wkfw_id ";
		}

		// 表題
		if($apply_title != "") {
			$sql .= "and A.apply_title like '%$apply_title%' ";
		}

		// 承認者
		if($emp_nm != "") {

            $sql .= "and exists ";
            $sql .= "(select APV.apply_id from jnl_applyapv APV ";
            $sql .= "where exists (select * from empmst EMP where APV.emp_id = EMP.emp_id and ";
            $sql .= "(EMP.emp_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_ft_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_ft_nm like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_lt_nm || EMP.emp_ft_nm) like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_kn_lt_nm || EMP.emp_kn_ft_nm) like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_kn_lt_nm || EMP.emp_kn_ft_nm) like '%$emp_nm%')) ";
            $sql .= "and A.apply_id = APV.apply_id ";
            $sql .= "group by APV.apply_id) ";

		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_date from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_date from 1 for $date_len) <= '$date_str' ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-") {
			$sql .= "and A.apply_stat = '$apply_stat' ";
		}

		return $sql;
	}

    // 議事録公開申請（委員会・WG）のＳＱＬ取得
	function get_proceeding_apply_sql($arr_cond)
    {

		$emp_id          = $arr_cond["emp_id"];
		$apply_title     = $arr_cond["apply_title"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apply_stat      = $arr_cond["apply_stat"];

		$sql  = "select ";
		$sql .= "varchar(1) '2' as apply_type, ";
		$sql .= "null as apply_id, ";
        $sql .= "A.prcd_subject as apply_title, ";
        $sql .= "A.prcd_create_time as apply_date, ";
        $sql .= "A.prcd_status as apply_stat, ";
        $sql .= "null as apply_no, ";
        $sql .= "A.apv_fix_show_flg, ";
        $sql .= "null as apv_bak_show_flg, ";
        $sql .= "null as apv_ng_show_flg, ";
        $sql .= "'議事録公開申請（委員会・WG）' as wkfw_title, ";
        $sql .= "'CoMedix' as wkfw_nm, ";
        $sql .= "null as short_wkfw_name,";
        $sql .= "null as wkfw_folder_id, ";
        $sql .= "A.pjt_schd_id ";

        $sql .= "from proceeding A ";
        $sql .= "where A.prcd_create_emp_id = '$emp_id' ";

		// 表題
		if($apply_title != "") {
			$sql .= "and A.prcd_subject like '%$apply_title%' ";
		}

		// 承認者
		if($emp_nm != "") {

            $sql .= "and exists ";
            $sql .= "(select APV.pjt_schd_id from prcdaprv APV ";
            $sql .= "where exists (select * from empmst EMP where APV.prcdaprv_emp_id = EMP.emp_id and ";
            $sql .= "(EMP.emp_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_ft_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_ft_nm like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_lt_nm || EMP.emp_ft_nm) like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_kn_lt_nm || EMP.emp_kn_ft_nm) like '%$emp_nm%')) ";
            $sql .= "and A.pjt_schd_id = APV.pjt_schd_id ";
            $sql .= "group by APV.pjt_schd_id) ";
		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(A.prcd_create_time from 1 for $date_len) >= '$date_str' ";
			}
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(A.prcd_create_time from 1 for $date_len) <= '$date_str' ";
			}
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-") {
			$tmp_apply_stat = $apply_stat + 1;
			$sql .= "and A.prcd_status = '$tmp_apply_stat' ";
		}

		return $sql;
    }


	// 残業申請のＳＱＬ取得
	function get_ovtm_apply_sql($arr_cond)
	{
		$emp_id          = $arr_cond["emp_id"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apply_stat      = $arr_cond["apply_stat"];

		$sql  = "select ";
		$sql .= "varchar(1) '3' as apply_type, ";
		$sql .= "A.apply_id, ";
		$sql .= "null as apply_title, ";
		$sql .= "A.apply_time as apply_date, ";
		$sql .= "A.apply_status as apply_stat, ";
		$sql .= "null as apply_no, ";
		$sql .= "A.apv_fix_show_flg, ";
		$sql .= "A.apv_bak_show_flg, ";
		$sql .= "A.apv_ng_show_flg, ";
		$sql .= "'残業申請' as wkfw_title, ";
		$sql .= "'CoMedix' as wkfw_nm, ";
		$sql .= "null as short_wkfw_name, ";
		$sql .= "null as wkfw_folder_id, ";
		$sql .= "null as pjt_schd_id ";
		$sql .= "from ovtmapply A ";

		$sql .= "where A.emp_id = '$emp_id' ";
        $sql .= "and A.re_apply_id is null ";
        $sql .= "and not A.delete_flg ";


		// 承認者
		if($emp_nm != "") {

            $sql .= "and exists ";
            $sql .= "(select APV.apply_id from ovtmaprv APV ";
            $sql .= "where exists (select * from empmst EMP where APV.aprv_emp_id = EMP.emp_id and ";
            $sql .= "(EMP.emp_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_ft_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_ft_nm like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_lt_nm || EMP.emp_ft_nm) like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_kn_lt_nm || EMP.emp_kn_ft_nm) like '%$emp_nm%')) ";
            $sql .= "and A.apply_id = APV.apply_id ";
            $sql .= "group by APV.apply_id) ";
		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_time from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_time from 1 for $date_len) <= '$date_str' ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-") {
			$sql .= "and A.apply_status = '$apply_stat' ";
		}

		return $sql;
	}

	// 勤務時間修正申請のＳＱＬ取得
	function get_tmmd_apply_sql($arr_cond)
	{
		$emp_id          = $arr_cond["emp_id"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apply_stat      = $arr_cond["apply_stat"];

		$sql  = "select ";
		$sql .= "varchar(1) '4' as apply_type, ";
		$sql .= "A.apply_id, ";
		$sql .= "null as apply_title, ";
		$sql .= "A.apply_time as apply_date, ";
		$sql .= "A.apply_status as apply_stat, ";
		$sql .= "null as apply_no, ";
		$sql .= "A.apv_fix_show_flg, ";
		$sql .= "A.apv_bak_show_flg, ";
		$sql .= "A.apv_ng_show_flg, ";
		$sql .= "'勤務時間修正申請' as wkfw_title, ";
		$sql .= "'CoMedix' as wkfw_nm, ";
		$sql .= "null as short_wkfw_name, ";
		$sql .= "null as wkfw_folder_id, ";
		$sql .= "null as pjt_schd_id ";
		$sql .= "from tmmdapply A ";

		$sql .= "where A.emp_id = '$emp_id' ";
        $sql .= "and A.re_apply_id is null ";
        $sql .= "and not A.delete_flg ";

		// 承認者
		if($emp_nm != "") {

            $sql .= "and exists ";
            $sql .= "(select APV.apply_id from tmmdaprv APV ";
            $sql .= "where exists (select * from empmst EMP where APV.aprv_emp_id = EMP.emp_id and ";
            $sql .= "(EMP.emp_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_ft_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_ft_nm like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_lt_nm || EMP.emp_ft_nm) like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_kn_lt_nm || EMP.emp_kn_ft_nm) like '%$emp_nm%')) ";
            $sql .= "and A.apply_id = APV.apply_id ";
            $sql .= "group by APV.apply_id) ";
		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_time from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_time from 1 for $date_len) <= '$date_str' ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-") {
			$sql .= "and A.apply_status = '$apply_stat' ";
		}

		return $sql;
	}

    // 退勤後復帰申請のＳＱＬ取得
	function get_rtn_apply_sql($arr_cond)
	{
		$emp_id          = $arr_cond["emp_id"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apply_stat      = $arr_cond["apply_stat"];

		$sql  = "select ";
		$sql .= "varchar(1) '5' as apply_type, ";
		$sql .= "A.apply_id, ";
		$sql .= "null as apply_title, ";
		$sql .= "A.apply_time as apply_date, ";
		$sql .= "A.apply_status as apply_stat, ";
		$sql .= "null as apply_no, ";
		$sql .= "A.apv_fix_show_flg, ";
		$sql .= "A.apv_bak_show_flg, ";
		$sql .= "A.apv_ng_show_flg, ";
		$sql .= "'退勤後復帰申請' as wkfw_title, ";
		$sql .= "'CoMedix' as wkfw_nm, ";
		$sql .= "null as short_wkfw_name, ";
		$sql .= "null as wkfw_folder_id, ";
		$sql .= "null as pjt_schd_id ";
		$sql .= "from rtnapply A ";

		$sql .= "where A.emp_id = '$emp_id' ";
        $sql .= "and A.re_apply_id is null ";
        $sql .= "and not A.delete_flg ";

		// 承認者
		if($emp_nm != "") {

            $sql .= "and exists ";
            $sql .= "(select APV.apply_id from rtnaprv APV ";
            $sql .= "where exists (select * from empmst EMP where APV.aprv_emp_id = EMP.emp_id and ";
            $sql .= "(EMP.emp_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_ft_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_ft_nm like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_lt_nm || EMP.emp_ft_nm) like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_kn_lt_nm || EMP.emp_kn_ft_nm) like '%$emp_nm%')) ";
            $sql .= "and A.apply_id = APV.apply_id ";
            $sql .= "group by APV.apply_id) ";
		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_time from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_time from 1 for $date_len) <= '$date_str' ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-") {
			$sql .= "and A.apply_status = '$apply_stat' ";
		}

		return $sql;


	}

    // 通常申請の承認情報取得
	function get_applyapv_for_applylist($apply_id)
    {
		$sql   = "select A.apply_id, A.apv_stat, A.apv_fix_show_flg, B.emp_lt_nm, B.emp_ft_nm from jnl_applyapv A ";
        $sql  .= "left join empmst B on A.emp_id = B.emp_id ";
        $cond  = "where A.apply_id = $apply_id ";
        $cond .= "order by A.apv_order, A.apv_sub_order asc ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("apply_id" => $row["apply_id"], "apv_stat" => $row["apv_stat"], "apv_fix_show_flg" => $row["apv_fix_show_flg"], "emp_lt_nm" => $row["emp_lt_nm"], "emp_ft_nm" => $row["emp_ft_nm"]);
		}
		return $arr;
    }

	// 申請一覧取得ＳＱＬ
    function get_applylist_sql($arr_cond)
    {
		$wkfw_type       = $arr_cond["wkfw_type"];
		$wkfw_id         = $arr_cond["wkfw_id"];
		$apply_title     = $arr_cond["apply_title"];
		$apply_stat      = $arr_cond["apply_stat"];

		$sql = "";

		//-------------------------------------------
		// ■通常の申請
		// ・「カテゴリ」がCoMedix(0)以外
		//-------------------------------------------
		if($wkfw_type != "0")
		{
			$sql .= $this->get_normal_apply_sql($arr_cond);
		}


		return $sql;
    }

//-------------------------------------------------------------------------------------------------------------------------
// 承認一覧
//-------------------------------------------------------------------------------------------------------------------------
	// 通常申請のＳＱＬ取得
	function get_normal_approve_sql($arr_cond)
    {
		$emp_id          = $arr_cond["emp_id"];
		$wkfw_type       = $arr_cond["wkfw_type"];
		$wkfw_id         = $arr_cond["wkfw_id"];
		$apply_title     = $arr_cond["apply_title"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apv_stat        = $arr_cond["apv_stat"];

		$sql  = "select ";
		$sql .= "varchar(1) '1' as apply_type, ";
		$sql .= "a.apply_id, ";
		$sql .= "a.apply_stat, ";
		$sql .= "a.apply_date, ";
		$sql .= "a.apply_title, ";
		$sql .= "d.wkfw_title, ";
		$sql .= "d.wkfw_nm, ";
		$sql .= "e.emp_lt_nm, ";
		$sql .= "e.emp_ft_nm, ";
		$sql .= "b.apv_stat, ";
		$sql .= "null as pjt_schd_id, ";
		$sql .= "b.apv_order, ";
		$sql .= "b.apv_sub_order, ";
		$sql .= "c.send_apved_order, ";
		$sql .= "a.apply_no, ";
		$sql .= "d.short_wkfw_name, ";
		$sql .= "d.wkfw_id, ";
		$sql .= "d.wkfw_folder_id ";
		$sql .= "from jnl_apply a ";
		$sql .= "inner join jnl_applyapv b on a.apply_id = b.apply_id ";
		$sql .= "left join jnl_applyasyncrecv c on b.apply_id = c.apply_id ";
		$sql .= "and b.apv_order = c.recv_apv_order ";
		$sql .= "and ((b.apv_sub_order = c.recv_apv_sub_order) or (b.apv_sub_order is null and c.recv_apv_sub_order is null)) ";
		$sql .= "inner join ";
		$sql .= "(select a.wkfw_id, a.wkfw_type, a.wkfw_title, b.wkfw_nm, a.short_wkfw_name, a.wkfw_folder_id from jnl_wkfwmst a ";
		$sql .= "inner join jnl_wkfwcatemst b on a.wkfw_type = b.wkfw_type) d on a.wkfw_id = d.wkfw_id ";
		$sql .= "inner join empmst e on a.emp_id = e.emp_id ";

		//集計用データの報告書（各jnl 日報、月報）の施設ＩＤを取得する(order by で使用)
		//↓
		$sql .= " , ( SELECT jnl_facility_id, newest_apply_id FROM jnl_hospital_day ";
		$sql .= " UNION ALL SELECT jnl_facility_id, newest_apply_id FROM jnl_hospital_month_action ";
		$sql .= " UNION ALL SELECT jnl_facility_id, newest_apply_id FROM jnl_hospital_month_patient ";
		$sql .= " UNION ALL SELECT jnl_facility_id, newest_apply_id FROM jnl_nurse_home_main";
		$sql .= " UNION ALL SELECT jnl_facility_id, newest_apply_id FROM jnl_return_main";
		$sql .= " UNION ALL SELECT jnl_facility_id, newest_apply_id FROM jnl_reward_synthesis_medi_main";
		$sql .= " UNION ALL SELECT jnl_facility_id, newest_apply_id FROM jnl_reward_synthesis_dental_main";
		$sql .= " UNION ALL SELECT jnl_facility_id, newest_apply_id FROM jnl_bill_diagnosis_enter_main";
		$sql .= " UNION ALL SELECT jnl_facility_id, newest_apply_id FROM jnl_bill_diagnosis_visit_main";
		$sql .= " UNION ALL SELECT jnl_facility_id, newest_apply_id FROM jnl_bill_recuperation_main";
		$sql .= " UNION ALL SELECT jnl_facility_id, newest_apply_id FROM jnl_bill_recuperation_prepay_main";
		$sql .= " ) as wjre ";

		//↑
		//集計用データの報告書（各jnl 日報、月報）の施設ＩＤを取得する(order by で使用)

		$sql .= "where a.re_apply_id is null ";
		$sql .= "and not a.delete_flg and not a.draft_flg ";
		$sql .= "and wjre.newest_apply_id = a.apply_id  ";
		$sql .= "and b.emp_id = '$emp_id' ";
		$sql .= "and (c.apv_show_flg or c.apv_show_flg is null) ";

		// 承認状況
		if($apv_stat == "0")
		{
			$sql .= "and b.apv_stat = '0' ";

			$sql .= "and (";
			$sql .= "(a.wkfw_appr = '1' and ";
			$sql .= "exists (select apply_id from jnl_applyapv apv1 where a.apply_id = apv1.apply_id and apv1.emp_id = '$emp_id' and apv1.apv_stat = '0')) ";

			$sql .= "or ";
			$sql .= "(a.wkfw_appr = '2' and a.apply_stat = '0' and ";
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, apv_order from jnl_applyapv where emp_id = '$emp_id' and apv_stat = '0' ";
			$sql .= "and apv_order = (select min(apv_order) from jnl_applyapv minapplyapv where minapplyapv.apply_id = a.apply_id and apv_stat = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.apv_order = apv2.apv_order)) ";

			$sql .= "or ";
			$sql .= "(a.wkfw_appr = '2' and c.apv_show_flg and ";
			$sql .= "exists (select apply_id from jnl_applyapv apv3 where a.apply_id = apv3.apply_id and apv3.emp_id = '$emp_id' and apv3.apv_stat = '0'))";

			$sql .= ") ";

		}
		else if($apv_stat == "" || $apv_stat == "-")
		{
			$sql .= "and (";
			$sql .= "(a.wkfw_appr = '1' and ";
			$sql .= "exists (select apply_id from jnl_applyapv apv1 where a.apply_id = apv1.apply_id and apv1.emp_id = '$emp_id')) ";

			$sql .= "or ";
			$sql .= "(a.wkfw_appr = '2' and a.apply_stat = '0' and ";
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, apv_order from jnl_applyapv where emp_id = '$emp_id' and apv_stat = '0' ";
			$sql .= "and apv_order = (select min(apv_order) from jnl_applyapv minapplyapv where minapplyapv.apply_id = a.apply_id and apv_stat = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.apv_order = apv2.apv_order)) ";

			$sql .= "or ";
			$sql .= "(a.wkfw_appr = '2' and c.apv_show_flg and ";
			$sql .= "exists (select apply_id from jnl_applyapv apv3 where a.apply_id = apv3.apply_id and apv3.emp_id = '$emp_id' and apv3.apv_stat = '0'))";

			$sql .= "or ";
			$sql .= "(a.wkfw_appr = '2' and ";
			$sql .= "exists (select apv4.apply_id, apv4.apv_order from jnl_applyapv apv4 where a.apply_id = apv4.apply_id and b.apv_order = apv4.apv_order and apv4.emp_id = '$emp_id' and apv4.apv_stat <> '0'))";

			$sql .= ") ";
		}
		else
		{
			if($apv_stat != "" && $apv_stat != "-")
			{
				$sql .= "and b.apv_stat = '$apv_stat' ";
			}
		}

		// カテゴリ
		if($wkfw_type != "" && $wkfw_type != "-")
		{
			$sql .= "and d.wkfw_type = $wkfw_type ";
		}
		// 申請書名
		if($wkfw_id != "" &&  $wkfw_id != "-")
		{
			$sql .= "and d.wkfw_id = $wkfw_id ";
		}
		// 表題
		if($apply_title != "")
		{
			$sql .= "and a.apply_title like '%$apply_title%' ";
		}

		// 申請者
		if($emp_nm != "") {

			$sql .= "and ";
			$sql .= "(e.emp_lt_nm like '%$emp_nm%' ";
			$sql .= "or e.emp_ft_nm like '%$emp_nm%' ";
			$sql .= "or e.emp_kn_lt_nm like '%$emp_nm%' ";
			$sql .= "or e.emp_kn_ft_nm like '%$emp_nm%' ";
			$sql .= "or (e.emp_lt_nm || e.emp_ft_nm) like '%$emp_nm%' ";
			$sql .= "or (e.emp_kn_lt_nm || e.emp_kn_ft_nm) like '%$emp_nm%') ";

		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) <= '$date_str' ";
		}

		return $sql;
    }

	// 議事録公開申請（委員会・WG）のＳＱＬ取得
    function get_proceeding_approve_sql($arr_cond)
    {

		$emp_id          = $arr_cond["emp_id"];
		$apply_title     = $arr_cond["apply_title"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apv_stat        = $arr_cond["apv_stat"];

    	$sql = "select ";
		$sql .= "varchar(1) '2' as apply_type, ";
		$sql .= "null as apply_id, ";
		$sql .= "a.prcd_status as apply_stat, ";
		$sql .= "a.prcd_create_time as apply_date, ";
		$sql .= "a.prcd_subject as apply_title, ";
		$sql .= "'議事録公開申請（委員会・WG）' as wkfw_title, ";
		$sql .= "'CoMedix' as wkfw_nm, ";
		$sql .= "c.emp_lt_nm, ";
		$sql .= "c.emp_ft_nm, ";
		$sql .= "b.prcdaprv_date as apv_stat, ";
		$sql .= "a.pjt_schd_id, ";
		$sql .= "null as apv_order, ";
		$sql .= "null as apv_sub_order, ";
		$sql .= "null as send_apved_order, ";
		$sql .= "null as apply_no, ";
		$sql .= "null as short_wkfw_name, ";
		$sql .= "null as wkfw_folder_id ";
		$sql .= "from proceeding a ";
		$sql .= "inner join prcdaprv b on a.pjt_schd_id = b.pjt_schd_id ";
		$sql .= "inner join empmst c on a.prcd_create_emp_id = c.emp_id ";
	    $sql .= "where b.prcdaprv_emp_id = '$emp_id' ";

		// 表題
		if($apply_title != "")
		{
			$sql .= "and a.prcd_subject like '%$apply_title%' ";
		}

		// 申請者
		if($emp_nm != "")
		{
			$sql .= "and ";
			$sql .= "(c.emp_lt_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_ft_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_kn_lt_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_kn_ft_nm like '%$emp_nm%' ";
			$sql .= "or (c.emp_lt_nm || c.emp_ft_nm) like '%$emp_nm%' ";
			$sql .= "or (c.emp_kn_lt_nm || c.emp_kn_ft_nm) like '%$emp_nm%') ";

		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(a.prcd_create_time from 1 for $date_len) >= '$date_str' ";
			}
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(a.prcd_create_time from 1 for $date_len) <= '$date_str' ";
			}
		}

		// 承認状況
		if($apv_stat != "" && $apv_stat != "-")
		{
			if($apv_stat == "0") {
				$sql .= "and b.prcdaprv_date is null ";
			} else if($apv_stat == "1") {
			    $sql .= "and b.prcdaprv_date is not null ";
			}
		}
		return $sql;
    }

    // 残業申請のＳＱＬ取得
    function get_ovtm_approve_sql($arr_cond)
    {
		$emp_id          = $arr_cond["emp_id"];
		$apply_title     = $arr_cond["apply_title"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apv_stat        = $arr_cond["apv_stat"];

		$sql = "select ";
		$sql .= "varchar(1) '3' as apply_type, ";
		$sql .= "a.apply_id, ";
		$sql .= "a.apply_status as apply_stat, ";
		$sql .= "a.apply_time as apply_date, ";
		$sql .= "null as apply_title, ";
		$sql .= "'残業申請' as wkfw_title, ";
		$sql .= "'CoMedix' as wkfw_nm, ";
		$sql .= "emp_lt_nm, ";
		$sql .= "emp_ft_nm, ";
		$sql .= "b.aprv_status as apv_stat, ";
		$sql .= "null as pjt_schd_id, ";
		$sql .= "b.aprv_no as apv_order, ";
		$sql .= "b.aprv_sub_no as apv_sub_order, ";
		$sql .= "d.send_aprved_no as send_apved_order, ";
		$sql .= "null as apply_no, ";
		$sql .= "null as short_wkfw_name, ";
		$sql .= "null as wkfw_folder_id ";
		$sql .= "from ovtmapply a ";
		$sql .= "inner join ovtmaprv b on a.apply_id = b.apply_id ";
		$sql .= "left join ovtm_async_recv d on b.apply_id = d.apply_id ";
		$sql .= "and b.aprv_no = d.recv_aprv_no ";
		$sql .= "and ((b.aprv_sub_no = d.recv_aprv_sub_no) or (b.aprv_sub_no is null and d.recv_aprv_sub_no is null)) ";
		$sql .= "inner join empmst c on a.emp_id = c.emp_id ";

		$sql .= "where a.re_apply_id is null ";
		$sql .= "and not a.delete_flg ";
		$sql .= "and b.aprv_emp_id = '$emp_id' ";
		$sql .= "and (d.apv_show_flg or d.apv_show_flg is null) ";

		// 申請者
		if($emp_nm != "") {

			$sql .= "and ";
			$sql .= "(c.emp_lt_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_ft_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_kn_lt_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_kn_ft_nm like '%$emp_nm%' ";
			$sql .= "or (c.emp_lt_nm || c.emp_ft_nm) like '%$emp_nm%' ";
			$sql .= "or (c.emp_kn_lt_nm || c.emp_kn_ft_nm) like '%$emp_nm%') ";

		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(a.apply_time from 1 for $date_len) >= '$date_str' ";
			}
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(a.apply_time from 1 for $date_len) <= '$date_str' ";
			}
		}

		// 承認状況
		if($apv_stat == "0")
		{
			$sql .= "and b.aprv_status = '0' ";

			$sql .= "and (";

			$sql .= "(((a.apply_status = '0' or a.apply_status = '1') and ";
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, aprv_no from ovtmaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' ";
			$sql .= "and aprv_no = (select min(aprv_no) from ovtmaprv minovtmaprv where minovtmaprv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no))) ";

			$sql .= "or ";
			$sql .= "(d.apv_show_flg and ";
			$sql .= "exists (select apply_id from ovtmaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0'))";

			$sql .= ") ";

		}
		else if($apv_stat == "" || $apv_stat == "-")
		{
			$sql .= "and (";

			$sql .= "((a.apply_status = '0' or a.apply_status = '1') and ";
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, aprv_no from ovtmaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' ";
			$sql .= "and aprv_no = (select min(aprv_no) from ovtmaprv minovtmaprv where minovtmaprv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no)) ";


			$sql .= "or ";
			$sql .= "(d.apv_show_flg and ";
			$sql .= "exists (select apply_id from ovtmaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0'))";

			$sql .= "or ";
			$sql .= "(";
			$sql .= "exists (select apv4.apply_id, apv4.aprv_no from ovtmaprv apv4 where a.apply_id = apv4.apply_id and b.aprv_no = apv4.aprv_no and apv4.aprv_emp_id = '$emp_id' and apv4.aprv_status <> '0'))";

			$sql .= ") ";
		}
		else
		{
			if($apv_stat != "" && $apv_stat != "-")
			{
				$sql .= "and b.aprv_status = '$apv_stat' ";
			}
		}

		return $sql;
    }

	// 勤務時間修正申請のＳＱＬ取得
	function get_tmmd_approve_sql($arr_cond)
    {
		$emp_id          = $arr_cond["emp_id"];
		$apply_title     = $arr_cond["apply_title"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apv_stat        = $arr_cond["apv_stat"];

		$sql = "select ";
		$sql .= "varchar(1) '4' as apply_type, ";
		$sql .= "a.apply_id, ";
		$sql .= "a.apply_status as apply_stat, ";
		$sql .= "a.apply_time as apply_date, ";
		$sql .= "null as apply_title, ";
		$sql .= "'勤務時間修正申請' as wkfw_title, ";
		$sql .= "'CoMedix' as wkfw_nm, ";
		$sql .= "emp_lt_nm, ";
		$sql .= "emp_ft_nm, ";
		$sql .= "b.aprv_status as apv_stat, ";
		$sql .= "null as pjt_schd_id, ";
		$sql .= "b.aprv_no as apv_order, ";
		$sql .= "b.aprv_sub_no as apv_sub_order, ";
		$sql .= "d.send_aprved_no as send_apved_order, ";
		$sql .= "null as apply_no, ";
		$sql .= "null as short_wkfw_name, ";
		$sql .= "null as wkfw_folder_id ";
		$sql .= "from tmmdapply a ";
		$sql .= "inner join tmmdaprv b on a.apply_id = b.apply_id ";
		$sql .= "left join tmmd_async_recv d on b.apply_id = d.apply_id ";
		$sql .= "and b.aprv_no = d.recv_aprv_no ";
		$sql .= "and ((b.aprv_sub_no = d.recv_aprv_sub_no) or (b.aprv_sub_no is null and d.recv_aprv_sub_no is null)) ";
		$sql .= "inner join empmst c on a.emp_id = c.emp_id ";

		$sql .= "where a.re_apply_id is null ";
		$sql .= "and not a.delete_flg ";
		$sql .= "and b.aprv_emp_id = '$emp_id' ";
		$sql .= "and (d.apv_show_flg or d.apv_show_flg is null) ";

		// 申請者
		if($emp_nm != "") {

			$sql .= "and ";
			$sql .= "(c.emp_lt_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_ft_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_kn_lt_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_kn_ft_nm like '%$emp_nm%' ";
			$sql .= "or (c.emp_lt_nm || c.emp_ft_nm) like '%$emp_nm%' ";
			$sql .= "or (c.emp_kn_lt_nm || c.emp_kn_ft_nm) like '%$emp_nm%') ";

		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(a.apply_time from 1 for $date_len) >= '$date_str' ";
			}
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(a.apply_time from 1 for $date_len) <= '$date_str' ";
			}
		}

		// 承認状況
		if($apv_stat == "0")
		{
			$sql .= "and b.aprv_status = '0' ";

			$sql .= "and (";

			$sql .= "(((a.apply_status = '0' or a.apply_status = '1') and ";
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, aprv_no from tmmdaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' ";
			$sql .= "and aprv_no = (select min(aprv_no) from tmmdaprv mintmmdaprv where mintmmdaprv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no))) ";

			$sql .= "or ";
			$sql .= "(d.apv_show_flg and ";
			$sql .= "exists (select apply_id from tmmdaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0'))";

			$sql .= ") ";

		}
		else if($apv_stat == "" || $apv_stat == "-")
		{
			$sql .= "and (";

			$sql .= "((a.apply_status = '0' or a.apply_status = '1') and ";
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, aprv_no from tmmdaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' ";
			$sql .= "and aprv_no = (select min(aprv_no) from tmmdaprv mintmmdaprv where mintmmdaprv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no)) ";


			$sql .= "or ";
			$sql .= "(d.apv_show_flg and ";
			$sql .= "exists (select apply_id from tmmdaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0'))";

			$sql .= "or ";
			$sql .= "(";
			$sql .= "exists (select apv4.apply_id, apv4.aprv_no from tmmdaprv apv4 where a.apply_id = apv4.apply_id and b.aprv_no = apv4.aprv_no and apv4.aprv_emp_id = '$emp_id' and apv4.aprv_status <> '0'))";

			$sql .= ") ";
		}
		else
		{
			if($apv_stat != "" && $apv_stat != "-")
			{
				$sql .= "and b.aprv_status = '$apv_stat' ";
			}
		}

		return $sql;
    }

	// 退勤後復帰申請のＳＱＬ取得
	function get_rtn_approve_sql($arr_cond)
    {
		$emp_id          = $arr_cond["emp_id"];
		$apply_title     = $arr_cond["apply_title"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$apv_stat        = $arr_cond["apv_stat"];

		$sql = "select ";
		$sql .= "varchar(1) '5' as apply_type, ";
		$sql .= "a.apply_id, ";
		$sql .= "a.apply_status as apply_stat, ";
		$sql .= "a.apply_time as apply_date, ";
		$sql .= "null as apply_title, ";
		$sql .= "'退勤後復帰申請' as wkfw_title, ";
		$sql .= "'CoMedix' as wkfw_nm, ";
		$sql .= "emp_lt_nm, ";
		$sql .= "emp_ft_nm, ";
		$sql .= "b.aprv_status as apv_stat, ";
		$sql .= "null as pjt_schd_id, ";
		$sql .= "b.aprv_no as apv_order, ";
		$sql .= "b.aprv_sub_no as apv_sub_order, ";
		$sql .= "d.send_aprved_no as send_apved_order, ";
		$sql .= "null as apply_no, ";
		$sql .= "null as short_wkfw_name, ";
		$sql .= "null as wkfw_folder_id ";
		$sql .= "from rtnapply a ";
		$sql .= "inner join rtnaprv b on a.apply_id = b.apply_id ";
		$sql .= "left join rtn_async_recv d on b.apply_id = d.apply_id ";
		$sql .= "and b.aprv_no = d.recv_aprv_no ";
		$sql .= "and ((b.aprv_sub_no = d.recv_aprv_sub_no) or (b.aprv_sub_no is null and d.recv_aprv_sub_no is null)) ";
		$sql .= "inner join empmst c on a.emp_id = c.emp_id ";

		$sql .= "where a.re_apply_id is null ";
		$sql .= "and not a.delete_flg ";
		$sql .= "and b.aprv_emp_id = '$emp_id' ";
		$sql .= "and (d.apv_show_flg or d.apv_show_flg is null) ";

		// 申請者
		if($emp_nm != "") {

			$sql .= "and ";
			$sql .= "(c.emp_lt_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_ft_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_kn_lt_nm like '%$emp_nm%' ";
			$sql .= "or c.emp_kn_ft_nm like '%$emp_nm%' ";
			$sql .= "or (c.emp_lt_nm || c.emp_ft_nm) like '%$emp_nm%' ";
			$sql .= "or (c.emp_kn_lt_nm || c.emp_kn_ft_nm) like '%$emp_nm%') ";

		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(a.apply_time from 1 for $date_len) >= '$date_str' ";
			}
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			if($date_len > 0) {
				$sql .= "and substring(a.apply_time from 1 for $date_len) <= '$date_str' ";
			}
		}

		// 承認状況
		if($apv_stat == "0")
		{
			$sql .= "and b.aprv_status = '0' ";

			$sql .= "and (";

			$sql .= "(((a.apply_status = '0' or a.apply_status = '1') and ";
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, aprv_no from rtnaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' ";
			$sql .= "and aprv_no = (select min(aprv_no) from rtnaprv minrtnaprv where minrtnaprv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no))) ";

			$sql .= "or ";
			$sql .= "(d.apv_show_flg and ";
			$sql .= "exists (select apply_id from rtnaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0'))";

			$sql .= ") ";

		}
		else if($apv_stat == "" || $apv_stat == "-")
		{
			$sql .= "and (";

			$sql .= "((a.apply_status = '0' or a.apply_status = '1') and ";
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, aprv_no from rtnaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' ";
			$sql .= "and aprv_no = (select min(aprv_no) from rtnaprv minrtnaprv where minrtnaprv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no)) ";


			$sql .= "or ";
			$sql .= "(d.apv_show_flg and ";
			$sql .= "exists (select apply_id from rtnaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0'))";

			$sql .= "or ";
			$sql .= "(";
			$sql .= "exists (select apv4.apply_id, apv4.aprv_no from rtnaprv apv4 where a.apply_id = apv4.apply_id and b.aprv_no = apv4.aprv_no and apv4.aprv_emp_id = '$emp_id' and apv4.aprv_status <> '0'))";

			$sql .= ") ";
		}
		else
		{
			if($apv_stat != "" && $apv_stat != "-")
			{
				$sql .= "and b.aprv_status = '$apv_stat' ";
			}
		}

		return $sql;
    }

	// 承認一覧取得ＳＱＬ
    function get_approvelist_sql($arr_cond)
    {
		$wkfw_type       = $arr_cond["wkfw_type"];
		$wkfw_id         = $arr_cond["wkfw_id"];
		$apply_title     = $arr_cond["apply_title"];
		$apv_stat        = $arr_cond["apv_stat"];

		$sql = "";

		//-------------------------------------------
		// ■通常の申請
		// ・「カテゴリ」がCoMedix(0)以外
		//-------------------------------------------
		if($wkfw_type != "0")
		{
			$sql .= $this->get_normal_approve_sql($arr_cond);
		}

		return $sql;
    }

//-------------------------------------------------------------------------------------------------------------------------
// 通知件数取得
//-------------------------------------------------------------------------------------------------------------------------
    // 承認済み情報取得
    function get_apply_fix($emp_id)
    {
        $sql  = "select varchar(1) '1' as apply_type, apply_id, null as pjt_schd_id from jnl_apply ";
        $sql .= "where apply_stat = '1' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_fix_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '2' as apply_type, null, a.pjt_schd_id from proceeding a ";
        $sql .= "inner join (select pjt_schd_id from prcdaprv group by pjt_schd_id) b on a.pjt_schd_id = b.pjt_schd_id ";
        $sql .= "where a.prcd_create_emp_id = '$emp_id' and a.prcd_status = '2' and a.apv_fix_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '3' as apply_type, apply_id, null as pjt_schd_id from ovtmapply ";
        $sql .= "where apply_status = '1' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_fix_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '4' as apply_type, apply_id, null as pjt_schd_id from tmmdapply ";
        $sql .= "where apply_status = '1' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_fix_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '5' as apply_type, apply_id, null as pjt_schd_id from rtnapply ";
        $sql .= "where apply_status = '1' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_fix_show_flg ";

	    $cond = "";
	    $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
	    if ($sel == 0) {
		    pg_close($this->_db_con);
		    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		    exit;
        }

        $num = pg_numrows($sel);
        return $num;
    }


    // 差戻し情報取得
    function get_apply_bak($emp_id)
    {

        $sql  = "select varchar(1) '1' as apply_type, apply_id from jnl_apply ";
        $sql .= "where apply_stat = '3' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_bak_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '3' as apply_type, apply_id from ovtmapply ";
        $sql .= "where apply_status = '3' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_bak_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '4' as apply_type, apply_id from tmmdapply ";
        $sql .= "where apply_status = '3' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_bak_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '5' as apply_type, apply_id from rtnapply ";
        $sql .= "where apply_status = '3' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_bak_show_flg ";

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_numrows($sel);
        return $num;
    }


    // 承認待ち情報情報
    function get_approve_wait($emp_id)
    {
        $sql  = "select varchar(1) '1' as apply_type, a.apply_id, a.wkfw_id, a.emp_id, a.apply_date, a.apply_title, null as pjt_schd_id, b.apv_order, b.apv_sub_order ";
        $sql .= "from jnl_apply a ";
        $sql .= "inner join jnl_applyapv b on a.apply_id = b.apply_id ";
        $sql .= "left join jnl_applyasyncrecv c on b.apply_id = c.apply_id and b.apv_order = c.recv_apv_order and ";
        $sql .= "((b.apv_sub_order = c.recv_apv_sub_order) or (b.apv_sub_order is null and c.recv_apv_sub_order is null)) ";
        $sql .= "where ";
        $sql .= "not a.delete_flg and ";
        $sql .= "not a.draft_flg and ";
        $sql .= "(c.apv_show_flg or c.apv_show_flg is null) and ";
        $sql .= "b.emp_id = '$emp_id' and ";
        $sql .= "a.re_apply_id is null and ";
        $sql .= "b.apv_stat = '0' and ";
        $sql .= "(";
        $sql .= "(a.wkfw_appr = '1' and ";
        $sql .= "exists (select apply_id from jnl_applyapv apv1 where a.apply_id = apv1.apply_id and apv1.emp_id = '$emp_id' and apv1.apv_stat = '0') ";
        $sql .= ") ";
        $sql .= "or ";
        $sql .= "(a.wkfw_appr = '2' and a.apply_stat = '0' and ";
        $sql .= "exists (select apv2.* from (select apply_id, apv_order from jnl_applyapv where emp_id = '$emp_id' and apv_stat = '0' and ";
        $sql .= "apv_order = (select min(apv_order) from jnl_applyapv minapplyapv where minapplyapv.apply_id = a.apply_id and apv_stat = '0')) apv2 ";
        $sql .= "where a.apply_id = apv2.apply_id and b.apv_order = apv2.apv_order) ";
        $sql .= ") ";
        $sql .= "or ";
        $sql .= "(a.wkfw_appr = '2' and c.apv_show_flg and ";
        $sql .= "exists (select apply_id from jnl_applyapv apv3 where a.apply_id = apv3.apply_id and apv3.emp_id = '$emp_id' and apv3.apv_stat = '0') ";
        $sql .= ") ";
        $sql .= ") ";

        $sql .= "union ";
        $sql .= "select varchar(1) '2' as apply_type, null, null, prcd_create_emp_id, prcd_create_time, prcd_subject, pjt_schd_id, null, null ";
        $sql .= "from proceeding ";
        $sql .= "where prcd_status = '1' and pjt_schd_id in (select pjt_schd_id from prcdaprv where prcdaprv_emp_id = '$emp_id' and prcdaprv_date is null) ";

        $sql .= "union ";
        $sql .= "select varchar(1) '3' as apply_type, a.apply_id, null, a.emp_id, a.apply_time, null, null as pjt_schd_id, b.aprv_no, b.aprv_sub_no ";
        $sql .= "from ovtmapply a ";
        $sql .= "inner join ovtmaprv b on a.apply_id = b.apply_id ";
        $sql .= "left join ovtm_async_recv c on b.apply_id = c.apply_id and b.aprv_no = c.recv_aprv_no and ";
        $sql .= "((b.aprv_sub_no = c.recv_aprv_sub_no) or (b.aprv_sub_no is null and c.recv_aprv_sub_no is null)) ";
        $sql .= "where ";
        $sql .= "not a.delete_flg and ";
        $sql .= "(c.apv_show_flg or c.apv_show_flg is null) and ";
        $sql .= "b.aprv_emp_id = '$emp_id' and ";
        $sql .= "a.re_apply_id is null and ";
        $sql .= "b.aprv_status = '0' and ";
        $sql .= "( ";
        $sql .= "( ";
        $sql .= "( ";
        $sql .= "(a.apply_status = '0' or a.apply_status = '1') and ";
        $sql .= "exists (select apv2.* from (select apply_id, aprv_no from ovtmaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' and ";
        $sql .= "aprv_no = (select min(aprv_no) from ovtmaprv minapplyapv where minapplyapv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
        $sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no) ";
        $sql .= ") ";
        $sql .= ") ";
        $sql .= "or ";
        $sql .= "(c.apv_show_flg and ";
        $sql .= "exists (select apply_id from ovtmaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0') ";
        $sql .= ") ";
        $sql .= ") ";

        $sql .= "union ";
        $sql .= "select varchar(1) '4' as apply_type, a.apply_id, null, a.emp_id, a.apply_time, null, null as pjt_schd_id, b.aprv_no, b.aprv_sub_no ";
        $sql .= "from tmmdapply a ";
        $sql .= "inner join tmmdaprv b on a.apply_id = b.apply_id ";
        $sql .= "left join tmmd_async_recv c on b.apply_id = c.apply_id and b.aprv_no = c.recv_aprv_no and ";
        $sql .= "((b.aprv_sub_no = c.recv_aprv_sub_no) or (b.aprv_sub_no is null and c.recv_aprv_sub_no is null)) ";
        $sql .= "where ";
        $sql .= "not a.delete_flg and ";
        $sql .= "(c.apv_show_flg or c.apv_show_flg is null) and ";
        $sql .= "b.aprv_emp_id = '$emp_id' and ";
        $sql .= "a.re_apply_id is null and ";
        $sql .= "b.aprv_status = '0' and ";
        $sql .= "( ";
        $sql .= "( ";
        $sql .= "( ";
        $sql .= "(a.apply_status = '0' or a.apply_status = '1') and ";
        $sql .= "exists (select apv2.* from (select apply_id, aprv_no from tmmdaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' and ";
        $sql .= "aprv_no = (select min(aprv_no) from tmmdaprv minapplyapv where minapplyapv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
        $sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no) ";
        $sql .= ") ";
        $sql .= ") ";
        $sql .= "or ";
        $sql .= "(c.apv_show_flg and ";
        $sql .= "exists (select apply_id from tmmdaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0') ";
        $sql .= ") ";
        $sql .= ") ";

        $sql .= "union ";
        $sql .= "select varchar(1) '5' as apply_type, a.apply_id, null, a.emp_id, a.apply_time, null, null as pjt_schd_id, b.aprv_no, b.aprv_sub_no ";
        $sql .= "from rtnapply a ";
        $sql .= "inner join rtnaprv b on a.apply_id = b.apply_id ";
        $sql .= "left join rtn_async_recv c on b.apply_id = c.apply_id and b.aprv_no = c.recv_aprv_no and ";
        $sql .= "((b.aprv_sub_no = c.recv_aprv_sub_no) or (b.aprv_sub_no is null and c.recv_aprv_sub_no is null)) ";
        $sql .= "where ";
        $sql .= "not a.delete_flg and ";
        $sql .= "(c.apv_show_flg or c.apv_show_flg is null) and ";
        $sql .= "b.aprv_emp_id = '$emp_id' and ";
        $sql .= "a.re_apply_id is null and ";
        $sql .= "b.aprv_status = '0' and ";
        $sql .= "( ";
        $sql .= "( ";
        $sql .= "( ";
        $sql .= "(a.apply_status = '0' or a.apply_status = '1') and ";
        $sql .= "exists (select apv2.* from (select apply_id, aprv_no from rtnaprv where aprv_emp_id = '$emp_id' and aprv_status = '0' and ";
        $sql .= "aprv_no = (select min(aprv_no) from rtnaprv minapplyapv where minapplyapv.apply_id = a.apply_id and aprv_status = '0')) apv2 ";
        $sql .= "where a.apply_id = apv2.apply_id and b.aprv_no = apv2.aprv_no) ";
        $sql .= ") ";
        $sql .= ") ";
        $sql .= "or ";
        $sql .= "(c.apv_show_flg and ";
        $sql .= "exists (select apply_id from rtnaprv apv3 where a.apply_id = apv3.apply_id and apv3.aprv_emp_id = '$emp_id' and apv3.aprv_status = '0') ";
        $sql .= ") ";
        $sql .= ") ";


        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        $num = pg_numrows($sel);
        return $num;
    }


    // 否認件数取得
    function get_apply_ng($emp_id)
    {

        $sql  = "select varchar(1) '1' as apply_type, apply_id from jnl_apply ";
        $sql .= "where apply_stat = '2' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_ng_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '3' as apply_type, apply_id from ovtmapply ";
        $sql .= "where apply_status = '2' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_ng_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '4' as apply_type, apply_id from tmmdapply ";
        $sql .= "where apply_status = '2' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_ng_show_flg ";
        $sql .= "union ";
        $sql .= "select varchar(1) '5' as apply_type, apply_id from rtnapply ";
        $sql .= "where apply_status = '2' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_ng_show_flg ";

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_numrows($sel);
        return $num;
    }


    // 一部承認件数取得
    function get_apply_fix_1($emp_id)
    {

        // 申請ステータスが0:申請中のデータで、
        // 承認ステータスが1:承認の件数を承認テーブルから申請毎にカウントする。
        // 1人以上いる申請を計算し一部承認の件数を取得する。
        // 削除は除く。未読フラグapv_fix_show_flgはtであること。
        // 議事録分も同様に追加。
	    //$sql = "select sum(case when b.apv_1 > 0 then 1 else 0 end) as apv_1_cnt from (select count(a.apply_id) as apv_1 from jnl_applyapv a where a.apply_id in (select apply_id from jnl_apply where emp_id = '$emp_id' and delete_flg = 'f' and apply_stat = '0') and a.apv_stat = '1' and a.delete_flg = 'f' and a.apv_fix_show_flg = 't' group by a.apply_id) b";

        $sql  = "select sum(case when b.apv_1 > 0 then 1 else 0 end) as apv_1_cnt ";
        $sql .= "from ";
        $sql .= "(select count(a.apply_id) as apv_1 from jnl_applyapv a ";
        $sql .= "where a.apply_id in (select apply_id from jnl_apply where emp_id = '$emp_id' and not delete_flg and apply_stat = '0') and ";
        $sql .= "a.apv_stat = '1' and ";
        $sql .= "not a.delete_flg and ";
        $sql .= "a.apv_fix_show_flg ";
        $sql .= "group by a.apply_id ";
        $sql .= ") b ";

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_fetch_result($sel, 0, "apv_1_cnt");
        if ($num == "") {
            $num = 0;
        }
        $sql  = "select sum(case when b.apv_1 > 0 then 1 else 0 end) as apv_1_cnt ";
        $sql .= "from ";
        $sql .= "(select count(a.pjt_schd_id) as apv_1 from prcdaprv a ";
        $sql .= "where a.pjt_schd_id in (select pjt_schd_id from proceeding where prcd_create_emp_id = '$emp_id' and prcd_status = '1') and ";
        $sql .= "a.prcdaprv_date is not null and ";
        $sql .= "a.apv_fix_show_flg ";
        $sql .= "group by a.pjt_schd_id ";
        $sql .= ") b ";
        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num2 = pg_fetch_result($sel, 0, "apv_1_cnt");
        if ($num2 == "") {
            $num2 = 0;
        }

        //残業申請一部承認件数取得
        //$sql = "select sum(case when b.apv_1 > 0 then 1 else 0 end) as apv_1_cnt from (select count(a.apply_id) as apv_1 from ovtmaprv a where a.apply_id in (select apply_id from ovtmapply where emp_id = '$emp_id' and delete_flg = 'f' and apply_status = '0') and a.aprv_status = '1' and a.delete_flg = 'f' and a.apv_fix_show_flg = 't' group by a.apply_id) b";
        $sql  = "select sum(case when b.apv_1 > 0 then 1 else 0 end) as apv_1_cnt ";
        $sql .= "from ";
        $sql .= "(select count(a.apply_id) as apv_1 from ovtmaprv a ";
        $sql .= "where a.apply_id in (select apply_id from ovtmapply where emp_id = '$emp_id' and not delete_flg and apply_status = '0') and ";
        $sql .= "a.aprv_status = '1' and ";
        $sql .= "not a.delete_flg and ";
        $sql .= "a.apv_fix_show_flg ";
        $sql .= "group by a.apply_id ";
        $sql .= ") b ";

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num3 = pg_fetch_result($sel, 0, "apv_1_cnt");
        if ($num3 == "") {
            $num3 = 0;
        }

        //勤務時間修正申請一部承認件数取得
        $sql  = "select sum(case when b.apv_1 > 0 then 1 else 0 end) as apv_1_cnt ";
        $sql .= "from ";
        $sql .= "(select count(a.apply_id) as apv_1 from tmmdaprv a ";
        $sql .= "where a.apply_id in (select apply_id from tmmdapply where emp_id = '$emp_id' and not delete_flg and apply_status = '0') and ";
        $sql .= "a.aprv_status = '1' and ";
        $sql .= "not a.delete_flg and ";
        $sql .= "a.apv_fix_show_flg ";
        $sql .= "group by a.apply_id ";
        $sql .= ") b ";

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num4 = pg_fetch_result($sel, 0, "apv_1_cnt");
        if ($num4 == "") {
            $num4 = 0;
        }

        //退勤後復帰申請一部承認件数取得
        $sql  = "select sum(case when b.apv_1 > 0 then 1 else 0 end) as apv_1_cnt ";
        $sql .= "from ";
        $sql .= "(select count(a.apply_id) as apv_1 from rtnaprv a ";
        $sql .= "where a.apply_id in (select apply_id from rtnapply where emp_id = '$emp_id' and not delete_flg and apply_status = '0') and ";
        $sql .= "a.aprv_status = '1' and ";
        $sql .= "not a.delete_flg and ";
        $sql .= "a.apv_fix_show_flg ";
        $sql .= "group by a.apply_id ";
        $sql .= ") b ";

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num5 = pg_fetch_result($sel, 0, "apv_1_cnt");
        if ($num5 == "") {
            $num5 = 0;
        }

        return $num+$num2+$num3+$num4+$num5;
    }




//-------------------------------------------------------------------------------------------------------------------------
// 共通
//-------------------------------------------------------------------------------------------------------------------------

	// 職員情報取得
	function get_empmst($session)
	{
		$sql  = "select * from empmst";
		$cond = "where emp_id in (select emp_id from session where session_id='$session')";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}

	// 職員詳細情報取得
	function get_empmst_detail($emp_id)
	{
		$sql  = "select empmst.*, stmst.st_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm ";
		$sql .= "from empmst ";
		$sql .= "inner join authmst on empmst.emp_id = authmst.emp_id and not authmst.emp_del_flg ";
		$sql .= "left join stmst on empmst.emp_st = stmst.st_id ";
		$sql .= "left join classmst on empmst.emp_class = classmst.class_id ";
		$sql .= "left join atrbmst on empmst.emp_attribute = atrbmst.atrb_id ";
		$sql .= "left join deptmst on empmst.emp_dept = deptmst.dept_id ";
		$sql .= "left join classroom on empmst.emp_room = classroom.room_id ";
		$cond = "where empmst.emp_id = '$emp_id'";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);

	}

	// 室名称取得
	function get_room_nm($room_id)
	{
		$sql  = "select * from classroom";
		$cond = "where room_id = $room_id";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$room_nm = pg_result($sel,0,"room_nm");
		return $room_nm;
	}

	// 部署情報取得
	function get_classname()
	{
		$sql  = "select * from classname";
		$cond = "";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}

	// 部門マスタ取得
	function get_class_mst() {
		$sql = "select class_id, class_nm from classmst";
		$cond = "where class_del_flg = 'f' order by order_no";
		$sel_class = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_class == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_class;
	}

	// 課マスタ取得
	function get_atrb_mst() {
		$sql = "select class_id, atrb_id, atrb_nm from atrbmst";
		$cond = "where atrb_del_flg = 'f' order by class_id, order_no";
		$sel_atrb = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_atrb == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_atrb;
	}

	// 科マスタ取得
	function get_dept_mst() {
		$sql = "select atrb_id, dept_id, dept_nm from deptmst";
		$cond = "where dept_del_flg = 'f' order by atrb_id, order_no";
		$sel_dept = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_dept == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_dept;
	}

	// 室マスタ取得
	function get_room_mst() {
		$sql = "select dept_id, room_id, room_nm from classroom";
		$cond = "where room_del_flg = 'f' order by dept_id, order_no";
		$sel_room = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_room == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_room;
	}

//-------------------------------------------------------------------------------------------------------------------------
// その他
//-------------------------------------------------------------------------------------------------------------------------

	// 委員会ＷＧ名取得
	function get_pjt_nm($pjt_id)
	{
		$sql  = "select pjt_name from project";
		$cond = "where pjt_id = $pjt_id ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "pjt_name");
	}

	// 未承認以外をカウントする
	function get_applyapv_cnt($apply_id)
	{
		$sql  = "select count(*) as cnt from jnl_applyapv";
		$cond = "where apply_id = $apply_id and apv_stat <> '0'";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "cnt");
	}

	// マージ処理
	function merge_arr_emp_info($arr_all_emp_info, $arr_target_emp_info)
	{
		$arr_tmp_apv = array();
		foreach($arr_target_emp_info as $target_emp_info)
		{
			$dpl_flg = false;
			foreach($arr_all_emp_info as $all_emp_info)
			{
				if($target_emp_info["emp_id"] == $all_emp_info["emp_id"])
				{
					$dpl_flg = true;
				}
			}
			if(!$dpl_flg)
			{
				$arr_tmp_apv[] = $target_emp_info;
			}
		}

		for($i=0; $i<count($arr_tmp_apv); $i++)
		{
			array_push($arr_all_emp_info, $arr_tmp_apv[$i]);
		}

		return $arr_all_emp_info;
	}

	// 本体用ワークフローディレクトリ作成
	function create_wkfwreal_directory()
	{
		if (!is_dir("jnl_workflow/real"))
		{
			mkdir("jnl_workflow/real", 0755);
			// フォーマットファイルコピー
			foreach (glob("jnl_workflow/*.*") as $file)
			{
				$tmp_file = substr($file, 9);
				copy($file, "jnl_workflow/real/$tmp_file");
			}
		}
	}

	// ワークフロー用ディレクトリ作成
	function create_wkfwtmp_directory()
	{
        if(!is_dir("jnl_workflow"))
		{
			mkdir("jnl_workflow", 0755);
		}
        if (!is_dir("jnl_workflow/tmp"))
        {
            mkdir("jnl_workflow/tmp", 0755);
        }
	}

	// 申請用ディレクトリ作成
	function create_applytmp_directory()
	{
        if(!is_dir("jnl_apply"))
		{
			mkdir("jnl_apply", 0755);
		}
        if (!is_dir("jnl_apply/tmp"))
        {
            mkdir("jnl_apply/tmp", 0755);
        }
	}
}


function jnl_get_wkfw_send_mail_flg($con, $wkfw_id, $fname) {
	$sql = "select send_mail_flg from jnl_wkfwmst";
	$cond="where wkfw_id = $wkfw_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return pg_fetch_result($sel, 0, "send_mail_flg");
}

function jnl_must_send_mail($wkfw_send_mail_flg, $emp_email, $wkfw_appr, $apv_order) {
	return ($wkfw_send_mail_flg == "t" && $emp_email != "" && ($wkfw_appr == "1" || ($wkfw_appr == "2" && $apv_order == 1)));
}

function jnl_format_emp_nm($emp_detail) {
	return $emp_detail["emp_lt_nm"] . " " . $emp_detail["emp_ft_nm"];
}

function jnl_format_emp_mail($emp_detail) {
	if ($emp_detail["emp_email2"] != "") {
		return $emp_detail["emp_email2"];
	}
	require("webmail/config/config.php");
	return "noreply@" . $domain;
}

function jnl_format_emp_pos($emp_detail) {
	$labels = array($emp_detail["class_nm"], $emp_detail["dept_nm"], $emp_detail["atrb_nm"]);
	if ($emp_detail["room_nm"] != "") {
		$labels[] = $emp_detail["room_nm"];
	}
	return join(" > ", $labels);
}

function jnl_format_mail_content($con, $wkfw_content_type, $content, $fname) {
	$base_content = ($wkfw_content_type == "2") ? "" : $content;

	$sql = "select mail_content from jnl_wkfwnotice";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == "0") {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$additional_content = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "mail_content") : "";

	if ($additional_content != "") {
		$base_content .= "\n\n$additional_content";
	}

	return $base_content;
}

function jnl_get_wkfw_id_by_apply_id($con, $apply_id, $fname) {
	$sql = "select wkfw_id from jnl_apply";
	$cond="where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return pg_fetch_result($sel, 0, "wkfw_id");
}

function jnl_get_apply_by_apply_id($con, $apply_id, $fname) {
	$sql = "select * from jnl_apply";
	$cond="where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return pg_fetch_array($sel);
}

function jnl_must_send_mail_to_next_approvers($con, $obj, $apply_id, $wkfw_appr, $apv_order, $approve, $next_notice_div, $fname) {

	// 稟議でない場合はメールを送らない
	if ($wkfw_appr != "2") {
		return false;
	}

	// 「承認」でない場合はメールを送らない
	if ($approve != "1") {
		return false;
	}

	// ワークフローが通知メールなし設定の場合はメールを送らない
	$wkfw_id = jnl_get_wkfw_id_by_apply_id($con, $apply_id, $fname);
	$wkfw_send_mail_flg = jnl_get_wkfw_send_mail_flg($con, $wkfw_id, $fname);
	if ($wkfw_send_mail_flg != "t") {
		return false;
	}

	// 次の階層がなければメール送信不要
	if ($apv_order == $obj->get_last_apv_order($apply_id)) {
		return false;
	}

	switch ($next_notice_div) {

	case "1":  // 非同期の場合

		// 同階層において初めての承認ならメール
		return ($obj->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1") == 1);

	case "2":  // 同期の場合

		// 同階層において全員が「承認」ならメール
		$same_hierarchy_apvcnt = $obj->get_same_hierarchy_apvcnt($apply_id, $apv_order);
		$ok_approvecnt = $obj->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
		return ($same_hierarchy_apvcnt == $ok_approvecnt);

	case "3":  // 権限並列の場合

		// 無条件でメール（同階層において初めての承認のはずなので）
		return true;
	}
}

function jnl_register_library($con, $approve, $obj, $apply_id, $fname) {
	$files = array();

	// 「承認」でない場合は文書登録不要
	if ($approve != "1") {
		return $files;
	}

	// 承認確定でない場合は文書登録不要
	if ($obj->get_apply_stat($apply_id) != "1") {
		return $files;
	}

	// 添付ファイルがない場合は文書登録不要
	$apply_files = $obj->get_applyfile($apply_id);
	if (count($apply_files) == 0) {
		return $files;
	}

	// ワークフロー情報を取得
	$wkfw_id = jnl_get_wkfw_id_by_apply_id($con, $apply_id, $fname);
	$wkfwmsts = $obj->get_wkfwmst($wkfw_id);
	$wkfwmst = $wkfwmsts[0];

	// 文書登録と連動しない場合は文書登録不要
	if ($wkfwmst["lib_reg_flg"] != "t") {
		return $files;
	}

	// 申請者の職員IDを取得
	$apply = jnl_get_apply_by_apply_id($con, $apply_id, $fname);
	$apply_emp_id = $apply["emp_id"];

	foreach ($apply_files as $tmp_apply_file) {
		$tmp_file_no = $tmp_apply_file["applyfile_no"];
		$tmp_file_name = $tmp_apply_file["applyfile_name"];

		// 拡張子の設定
		$tmp_lib_extension = (strpos($tmp_file_name, ".")) ? preg_replace("/^.*\.([^.]*)$/", "$1", $tmp_file_name) : "txt";

		// 文書タイプの決定
		switch ($tmp_lib_extension) {
		case "doc":
		case "docx":
			$tmp_lib_type = "1";
			break;
		case "xls":
		case "xlsx":
			$tmp_lib_type = "2";
			break;
		case "ppt":
		case "pptx":
			$tmp_lib_type = "3";
			break;
		case "pdf":
			$tmp_lib_type = "4";
			break;
		case "txt":
			$tmp_lib_type = "5";
			break;
		case "jpg":
		case "jpeg":
			$tmp_lib_type = "6";
			break;
		case "gif":
			$tmp_lib_type = "7";
			break;
		default:
			$tmp_lib_type = "99";
			break;
		}

		// 文書名の設定
		$tmp_lib_nm = preg_replace("/^(.*)\.($tmp_lib_extension)$/", "$1", $tmp_file_name);

		// 文書IDの採番
		$sql = "select max(lib_id) from libinfo";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_lib_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

		// 文書情報を登録
		$sql = "insert into libinfo (lib_archive, lib_id, lib_cate_id, lib_extension, lib_keyword, lib_summary, emp_id, lib_delete_flag, show_login_flg, show_login_begin, show_login_end, folder_id, lib_type, private_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg, lib_nm, lib_up_date, lib_no) values (";
		$content = array($wkfwmst["lib_archive"], $tmp_lib_id, $wkfwmst["lib_cate_id"], $tmp_lib_extension, $wkfwmst["lib_keyword"], $wkfwmst["lib_summary"], $apply_emp_id, "f", $wkfwmst["lib_show_login_flg"], $wkfwmst["lib_show_login_begin"], $wkfwmst["lib_show_login_end"], $wkfwmst["lib_folder_id"], $tmp_lib_type, $wkfwmst["lib_private_flg"], $wkfwmst["lib_ref_dept_st_flg"], $wkfwmst["lib_ref_dept_flg"], $wkfwmst["lib_ref_st_flg"], $wkfwmst["lib_upd_dept_st_flg"], $wkfwmst["lib_upd_dept_flg"], $wkfwmst["lib_upd_st_flg"], $tmp_lib_nm, date("YmdHis"), $wkfwmst["lib_no"]);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書参照ログを削除（念のため）
		$sql = "delete from libreflog";
		$cond = "where lib_id = $tmp_lib_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 版情報を登録
		$sql = "insert into libedition values (";
		$content = array($tmp_lib_id, 1, $tmp_lib_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書参照可能部署情報を登録
        $sql = "delete from librefdept where lib_id = $tmp_lib_id";
        $del = delete_from_table($con, $sql, "", $fname);
        if ($del == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
		$sql = "insert into librefdept select cast($tmp_lib_id as int), class_id, atrb_id, dept_id from jnl_wkfwlibrefdept where wkfw_mode = 2 and wkfw_id = $wkfw_id";
        $ins = insert_into_table_no_content($con, $sql, $fname);
        if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書参照可能役職情報を登録
        $sql = "delete from librefst where lib_id = $tmp_lib_id";
        $del = delete_from_table($con, $sql, "", $fname);
        if ($del == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
		$sql = "insert into librefst select cast($tmp_lib_id as int), st_id from jnl_wkfwlibrefst where wkfw_mode = 2 and wkfw_id = $wkfw_id";
        $ins = insert_into_table_no_content($con, $sql, $fname);
        if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書参照可能職員情報を登録
        $sql = "delete from librefemp where lib_id = $tmp_lib_id";
        $del = delete_from_table($con, $sql, "", $fname);
        if ($del == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
		$sql = "insert into librefemp select cast($tmp_lib_id as int), emp_id from jnl_wkfwlibrefemp where wkfw_mode = 2 and wkfw_id = $wkfw_id";
        $ins = insert_into_table_no_content($con, $sql, $fname);
        if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書更新可能部署情報を登録
        $sql = "delete from libupddept where lib_id = $tmp_lib_id";
        $del = delete_from_table($con, $sql, "", $fname);
        if ($del == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
		$sql = "insert into libupddept select cast($tmp_lib_id as int), class_id, atrb_id, dept_id from jnl_wkfwlibupddept where wkfw_mode = 2 and wkfw_id = $wkfw_id";
        $ins = insert_into_table_no_content($con, $sql, $fname);
        if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書更新可能役職情報を登録
        $sql = "delete from libupdst where lib_id = $tmp_lib_id";
        $del = delete_from_table($con, $sql, "", $fname);
        if ($del == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
		$sql = "insert into libupdst select cast($tmp_lib_id as int), st_id from jnl_wkfwlibupdst where wkfw_mode = 2 and wkfw_id = $wkfw_id";
        $ins = insert_into_table_no_content($con, $sql, $fname);
        if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 文書更新可能職員情報を登録
        $sql = "delete from libupdemp where lib_id = $tmp_lib_id";
        $del = delete_from_table($con, $sql, "", $fname);
        if ($del == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
		$sql = "insert into libupdemp select cast($tmp_lib_id as int), emp_id from jnl_wkfwlibupdemp where wkfw_mode = 2 and wkfw_id = $wkfw_id";
        $ins = insert_into_table_no_content($con, $sql, $fname);
        if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// ファイル情報を戻り配列に追加
		$files[] = array(
			"apply_id" => $apply_id,
			"apply_file_no" => $tmp_file_no,
			"extension" => $tmp_lib_extension,
			"lib_archive" => $wkfwmst["lib_archive"],
			"lib_cate_id" => $wkfwmst["lib_cate_id"],
			"lib_id" => $tmp_lib_id
		);
	}

	return $files;
}

function jnl_copy_apply_file_to_library($file) {

	// ディレクトリ名を決定
	switch ($file["lib_archive"]) {
	case "1":
		$dir_name = "private";
		break;
	case "2":
		$dir_name = "all";
		break;
	case "3":
		$dir_name = "section";
		break;
	case "4":
		$dir_name = "project";
		break;
	}

	// 添付ファイル保存用ディレクトリがなければ作成
	if (!is_dir("docArchive")) {
		mkdir("docArchive", 0755);
	}
	if (!is_dir("docArchive/{$dir_name}")) {
		mkdir("docArchive/{$dir_name}", 0755);
	}
	if (!is_dir("docArchive/{$dir_name}/cate{$file["lib_cate_id"]}")) {
		mkdir("docArchive/{$dir_name}/cate{$file["lib_cate_id"]}", 0755);
	}

	// 文書管理フォルダへコピー
	$apply_file = "jnl_apply/{$file["apply_id"]}_{$file["apply_file_no"]}";
	if (!file_exists($apply_file)) {
		$apply_file .= ".{$file["extension"]}";
	}

	copy($apply_file, "docArchive/{$dir_name}/cate{$file["lib_cate_id"]}/document{$file["lib_id"]}.{$file["extension"]}");
}
