<?
require_once("about_comedix.php");
require_once("get_values.php");
require_once("fplus_common_class.php");

//==============================
//初期処理
//==============================
//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);


//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix  報告書印刷</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="javascript">

function load_action() 
{
	//印刷して閉じる
	self.print();
//	self.close();
	setTimeout('self.close()', 1000);
}	

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#E6B3D4 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#E6B3D4 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#E6B3D4 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#E6B3D4 solid 0px;}
table.block3 td {border-width:0;}


p {margin:0;}

</style>

</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="load_action()">
<form name="mainform" action="hiyari_rp_classification_folder_input.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="mode" value="<?=$mode?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="folder_id" value="<?=$folder_id?>">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block2">


<!-- 本体 START -->
<!--
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
		<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
		<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
	</tr>
	<tr>
		<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
		<td bgcolor="#F5FFE5">
-->
<?

$obj = new fplus_common_class($con, $fname);


// 申請・ワークフロー情報取得
$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);

$wkfw_folder_id = $arr_apply_wkfwmst[0]["wkfw_folder_id"];
$emp_class_nm      = $arr_apply_wkfwmst[0]["apply_class_nm"];
$emp_attribute_nm  = $arr_apply_wkfwmst[0]["apply_atrb_nm"];
$emp_dept_nm       = $arr_apply_wkfwmst[0]["apply_dept_nm"];
$emp_room_nm       = $arr_apply_wkfwmst[0]["apply_room_nm"];
$class_nm  = $emp_class_nm;
$class_nm .= " > ";
$class_nm .= $emp_attribute_nm;
$class_nm .= " > ";
$class_nm .= $emp_dept_nm;

if ($emp_room_nm != "")
{
	$class_nm .= " > ";
	$class_nm .= $emp_room_nm;
}
$apply_lt_name  = $arr_apply_wkfwmst[0]["emp_lt_nm"];
$apply_ft_name  = $arr_apply_wkfwmst[0]["emp_ft_nm"];
$apply_full_nm  = "$apply_lt_name $apply_ft_name";
$wkfw_nm        = $arr_apply_wkfwmst[0]["wkfw_nm"];
$wkfw_title     = $arr_apply_wkfwmst[0]["wkfw_title"];
$apply_date       = $arr_apply_wkfwmst[0]["apply_date"];
$year = substr($apply_date, 0, 4);
$apply_date     = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $apply_date);

$apply_no         = $arr_apply_wkfwmst[0]["apply_no"];
$short_wkfw_name  = $arr_apply_wkfwmst[0]["short_wkfw_name"];

// カテゴリ名(フォルダパス)取得
$folder_path = $wkfw_nm;
if($wkfw_folder_id != "")
{
	// フォルダ名
	$folder_list = $obj->get_folder_path($wkfw_folder_id);
	foreach($folder_list as $folder)
	{
		if($folder_path != "")
		{
			$folder_path .= " > ";
		}
		$folder_path .= $folder["name"];
	}
}


$apply_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);


//$patient_non_print2 = $patient_non_print == "true";
//$report_html = get_report_html_for_print($con,$fname,$report_id,$eis_id,$patient_non_print2,$session);//$eis_idは省略可能。
?>
	<tr>
		<td width="10%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告書様式</font>
		</td>
		<td width="40%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($wkfw_title)?></font>
		</td>
		<td width="20%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告日</font>
		</td>
		<td width="30%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$apply_date?></font>
		</td>
	</tr>

	<tr>
		<td width="10%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告書番号</font>
		</td>
		<td width="40%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($apply_no)?></font>
		</td>
		<td width="20%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font>
		</td>
		<td width="30%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($folder_path)?></font>
		</td>
	</tr>

	<tr>
		<td width="10%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告者</font>
		</td>
		<td width="40%">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($apply_full_nm)?></font>
		</td>
		<td width="20%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属</font>
		</td>
		<td width="30%" align="left">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($class_nm)?></font>
		</td>
	</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="5"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block2">
	<tr align="left">
		<td colspan="4"  bgcolor="E3E3FF"><font class="j12">1.報告者</font></td>
	</tr>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■氏名：</font></td>
		<td><font class="j12"><?=$data_1_1_name?></font></td>

		<td bgcolor="E3E3FF"><font class="j12">■ふりがな：</font></td>
		<td><font class="j12"><?=$data_1_3_name_kana?></font></td>
	</tr>

	<tr align="left">
		<td  bgcolor="E3E3FF"><font class="j12">■職員番号：</font></td>
		<td><font class="j12"><?=$data_1_5_emp_code?></font></td>
		<td  bgcolor="E3E3FF"><font class="j12">■カルテ番号：</font></td>
		<td><font class="j12"><?=$data_1_6_clinical_records?></font></td>
	</tr>
	
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■性別/年齢/経験年数：</font></td>
		<?
			if($data_1_7_sex == "01")
			{$str_sex = "男性";}
			else
			{$str_sex = "女性";}
		?>
		<td colspan="3"><font class="j12"><?=$str_sex?>/<?=$data_1_8_years?>歳/<?=$data_1_9_exp_year?>年</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■所属部門：</font></td>
		<?
		$sql = "select item_name from tmplitem where mst_cd='A410' and item_cd='$data_1_2_belong_dept'";
		$cond = "";

		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
		$get_data = pg_fetch_result($sel, 0, "item_name");
		?>
		<td><font class="j12"><?=$get_data?></font></td>
		<td  bgcolor="E3E3FF"><font class="j12">その他の部門（記載）</font></td>
		<td><font class="j12"><?=$data_1_4_dept_etc?></font></td>
	</tr>
	
	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">2.発生日時</font></td>
	</tr>
	
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">発生日</font></td>
		<td><font class="j12"><?=$data_2_1_arise_date?></font></td>
		<td bgcolor="E3E3FF"><font class="j12">発生時間（24時間制）</font></td>
		<td><font class="j12"><?=$data_2_2_arise_hour?>時<?=$data_2_3_arise_minute?>分</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">3.職種</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">職種</font></td>
<?
$sql = "select item_name from tmplitem where mst_cd='A401' and item_cd='$data_3_1_job'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "item_name");
?>
		<td><font class="j12"><?=$get_data?></font></td>
		<td bgcolor="E3E3FF"><font class="j12">診療科</font></td>
<?
$sql = "select item_name from tmplitem where mst_cd='A402' and item_cd='$data_3_2_hospital_dept'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "item_name");
?>
		<td><font class="j12"><?=$get_data?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">診療科記載</font></td>
		<td><font class="j12"><?=$data_3_3_hospital_dept_etc?></font></td>
		<td bgcolor="E3E3FF"><font class="j12">職業記載</font></td>
		<td><font class="j12"><?=$data_3_4_job_etc?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">4.発生場所</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">発生場所</font></td>
<?
$sql = "select item_name from tmplitem where mst_cd='A403' and item_cd='$data_4_1_arise_area'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "item_name");
?>

		<td><font class="j12"><?=$get_data?></font></td>
		<td bgcolor="E3E3FF"><font class="j12">病棟名</font></td>
<?
$sql = "select item_name from tmplitem where mst_cd='A404' and item_cd='$data_4_2_arise_ward'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "item_name");
?>
		
		<td><font class="j12"><?=$get_data?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">科目名</font></td>
<?
$sql = "select item_name from tmplitem where mst_cd='A402' and item_cd='$data_4_3_arise_dept'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "item_name");
?>
		
		<td><font class="j12"><?=$get_data?></font></td>
		<td bgcolor="E3E3FF"><font class="j12">科目名記載</font></td>
		<td><font class="j12"><?=$data_4_4_arise_dept_etc?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">場所記載</font></td>
		<td colspan="3"><font class="j12"><?=$data_4_5_arise_etc?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">5.患者の確定</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">汚染源の患者が誰か分かっていますか？</font></td>

<?
$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_5_1_patient_decision' and record_id='$data_5_1_patient_decision'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "record_caption");
$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>

		<td colspan="3"><font class="j12"><?=$get_data?><?=$get_data2?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">詳細内容</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">患者氏名又はイニシャル</font></td>
		<td><font class="j12"><?=$data_5_2_patient_name?></font></td>
		<td bgcolor="E3E3FF"><font class="j12">患者カルテ番号</font></td>
		<td><font class="j12"><?=$data_5_3_patient_cl_rec?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">入院・外来</font></td>
<?
$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_5_4_patient_in_out' and record_id='$data_5_4_patient_in_out'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "record_caption");
$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td colspan="3"><font class="j12"><?=$get_data?><?=$get_data2?></font></td>
	</tr>


<?
$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_5_5_patient_hiv' and record_id='$data_5_5_patient_hiv'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "record_caption");
$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">患者の検査結果-HIV</font></td>
		<td><font class="j12"><?=$get_data?><?=$get_data2?></font></td>


<?
$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_5_5_patient_hiv' and record_id='$data_5_9_patient_syphilis'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "record_caption");
$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td bgcolor="E3E3FF"><font class="j12">患者の検査結果-梅毒</font></td>
		<td><font class="j12"><?=$get_data?><?=$get_data2?></font></td>
	</tr>

<?
$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_5_5_patient_hiv' and record_id='$data_5_8_patient_hbe'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "record_caption");
$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">患者の検査結果-HBe抗原</font></td>
		<td><font class="j12"><?=$get_data?><?=$get_data2?></font></td>
		
<?
$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_5_5_patient_hiv' and record_id='$data_5_7_patient_hbs'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "record_caption");
$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td bgcolor="E3E3FF"><font class="j12">患者の検査結果-HBs抗原</font></td>
		<td><font class="j12"><?=$get_data?><?=$get_data2?></font></td>
	</tr>

<?
$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_5_5_patient_hiv' and record_id='$data_5_6_patient_hcv'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "record_caption");
$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">患者の検査結果-HCV</font></td>
		<td><font class="j12"><?=$get_data?><?=$get_data2?></font></td>

	
<?
$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_5_5_patient_hiv' and record_id='$data_5_10_patient_atla'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
$get_data = pg_fetch_result($sel, 0, "record_caption");
$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td bgcolor="E3E3FF"><font class="j12">患者の検査結果-ATLA(HTLV-1)</font></td>
		<td><font class="j12"><?=$get_data?><?=$get_data2?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">その他</font></td>
		<td colspan="3"><font class="j12"><?=$data_5_11_patient_etc?></font></td>
	</tr>




<?
	if($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107A")
{
	//針刺しの印刷項目
	
?>




	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">6.器材の選択・使用者</font></td>
	</tr>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_5_1_patient_decision' and record_id='$data_6_1_user_select'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="2"><font class="j12">あなた自身がこの原因器材を選択して患者に使用したのですか？</font></td>
		<td colspan="2"><font class="j12"><?=$get_data?></font></td>
	</tr>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">記載</font></td>
		<td colspan="3"><font class="j12"><?=$data_6_2_user_select_memo?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">6-A.器材の所持者</font></td>

	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_6_1_user_select' and record_id='$data_6_3_user_owner'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		
		<td bgcolor="E3E3FF"><font class="j12">他者が持っていた原因器材で受傷したのですか？</font></td>
		<td colspan="2"><font class="j12"><?=$get_data?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">7.器材の汚染</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">器材は血液・体液などで汚染されていましたか？</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_7_1_pollution' and record_id='$data_7_1_pollution'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td colspan="3"><font class="j12"><?=$get_data?></font></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">8.使用目的</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">原因器材はどのような目的で使用されていましたか？</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_8_1_use_view' and record_id='$data_8_1_use_view'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td colspan="3"><font class="j12"><?=$get_data?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">静脈採血</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_8_2_use_view_vein' and record_id='$data_8_2_use_view_vein'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td><font class="j12"><?=$get_data?></font></td>
		<td bgcolor="E3E3FF"><font class="j12">動脈採血</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_8_3_use_view_artery' and record_id='$data_8_3_use_view_artery'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td><font class="j12"><?=$get_data?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="2"><font class="j12">記載</font></td>
		<td colspan="3"><font class="j12"><?=$data_8_4_use_view_etc?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">9.事例発生状況</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">どのような状況で針刺し・切創が生じましたか？</font></td>

	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_9_1_situation' and record_id='$data_9_1_situation'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td colspan="3"><font class="j12"><?=$get_data?><?=$get_data2?></font></td>
	</tr>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">記載</font></td>
		<td colspan="3"><font class="j12"><?=$data_9_2_situation_memo?></font></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">10.器材（器材項目・器材名・ゲージ数）</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">針刺し・切創の原因となった器材は？</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_10_1_tools' and record_id='$data_10_1_tools'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td colspan="3"><font class="j12"><?=$get_data?><?=$get_data2?></font></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■針（中空針）</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_10_2_tools_needle' and record_id='$data_10_2_tools_needle'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td colspan="3"><font class="j12"><?=$get_data?><?=$get_data2?></font></td>
	</tr>
		<td bgcolor="E3E3FF"><font class="j12">記載</font></td>
		<td colspan="3"><font class="j12"><?=$data_10_3_tools_needle_etc?></font></td>
	</tr>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">原因器材のゲージ数</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_10_4_tools_needle_gauge' and record_id='$data_10_4_tools_needle_gauge'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td><font class="j12"><?=$get_data?><?=$get_data2?></font></td>
		<td bgcolor="E3E3FF"><font class="j12">記載</font></td>
		<td><font class="j12"><?=$data_10_5_tools_needle_gauge_etc?></font></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■手術器材等</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_10_6_tools_ope' and record_id='$data_10_6_tools_ope'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td colspan="3"><font class="j12"><?=$get_data?><?=$get_data2?></font></td>
	</tr>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">記載1</font></td>
		<td colspan="3"><font class="j12"><?=$data_10_7_tools_ope_memo?></font></td>
	</tr>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">その他の場合</font></td>
		<td colspan="3"><font class="j12"><?=$data_10_8_tools_ope_etc?></font></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">10.器材（器材項目・器材名・ゲージ数）</font></td>
	</tr>



	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">■ガラス製器材</font></td>
		
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_10_9_tools_glass' and record_id='$data_10_9_tools_glass'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td colspan="3"><font class="j12"><?=$get_data?><?=$get_data2?></font></td>
	</tr>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">記載</font></td>
		<td colspan="3"><font class="j12"><?=$data_10_10_tools_glass_etc?></font></td>
	</tr>




	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">11.安全器材</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">受傷原因の器材は安全器材でしたか？<br>（上記設問で"はい"とお答えの方のみ11-A・11-B・11-Cに記載してください）</font></td>
	</tr>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_6_1_user_select' and record_id='$data_11_1_safe_tls'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
	<tr align="left">
		<td colspan="4"><font class="j12"><?=$get_data?></font></td>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">安全器材名</font></td>
		<td colspan="3"><font class="j12"><?=$data_11_2_safe_tls_name?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">11-A.安全装置作動の有無</font></td>
		<td bgcolor="E3E3FF"><font class="j12">安全装置を作動させましたか？</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_11_3_safe_tls_precaution' and record_id='$data_11_3_safe_tls_precaution'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>

		<td colspan="2"><font class="j12"><?=$get_data?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">11-B.受傷の時期</font></td>
		<td bgcolor="E3E3FF"><font class="j12">受傷の時期</font></td>
		
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_11_4_safe_period' and record_id='$data_11_4_safe_period'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td colspan="2"><font class="j12"><?=$get_data?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">11-C.安全機能の可否</font></td>
		<td bgcolor="E3E3FF"><font class="j12">安全器材による受傷の場合、この装置・機構で受傷を防ぐことができると考えますか？</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_6_1_user_select' and record_id='$data_11_5_safe_func'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td colspan="2"><font class="j12"><?=$get_data?></font></td>
	</tr>

	<tr align="left">
		<td colspan="4" bgcolor="E3E3FF"><font class="j12">意見を記載して下さい</font></td>
	</tr>
	<tr align="left">
		<td colspan="4"><font class="j12"><?=nl2br($data_11_6_safe_tls_memo)?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">12.受傷部位</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="2"><font class="j12">針刺し・切創部位を選択して下さい。複数の受傷部位の場合、傷の深い順に3個まで選択可</font></td>
		<td colspan="2"><font class="j12"><?=$data_12_1_injury_deep?> <?=$data_12_2_injury_middle?> <?=$data_12_3_injury_superficial?></font></td>
	</tr>
	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><img border="0" src="images/epinet.GIF" alt="イラスト1"></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">13.受傷の程度</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">針刺し・切創の程度</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_13_1_injury_degree' and record_id='$data_13_1_injury_degree'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td colspan="3"><font class="j12"><?=$get_data?><?=$get_data2?></font></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">14.手袋の着用</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">針刺し・切創の程度</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_14_1_injury_glove' and record_id='$data_14_1_injury_glove'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td colspan="3"><font class="j12"><?=$get_data?><?=$get_data2?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">15.HBs抗体</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">あなた自身はHBs抗体陽性ですか？</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_15_1_hbs_positivity' and record_id='$data_15_1_hbs_positivity'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td colspan="3"><font class="j12"><?=$get_data?><?=$get_data2?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">16.緊急処置受傷</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">緊急処置時（蘇生時を含む）の受傷でしたか？</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_6_1_user_select' and record_id='$data_16_1_emergency_injury'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td colspan="3"><font class="j12"><?=$get_data?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">17.針刺し・切創発生時の状況及び背景について</font></td>
	</tr>

	<tr align="left">
		<td colspan="4" bgcolor="E3E3FF"><font class="j12">下記の<font class="j12" color="red">(1)</font>〜<font class="j12" color="red">(6)</font>を含めて詳しく記載して下さい。<br>
					<font class="j12" color="red">(1)</font>具体的な発生現場（階、病棟、ナースステーション等）<font class="j12" color="red">(2)</font>発生時にどのような仕事、行為をしていたか？<br>
					<font class="j12" color="red">(3)</font>受傷原因に器材<font class="j12" color="red">(4)</font>どのようにして発生したか？<font class="j12" color="red">(5)</font>特別な事情・状況・背景等<font class="j12" color="red">(6)</font>受傷時の処置・対応
					</font></td>
	</tr>

	<tr align="left">
		<td colspan="4"><font class="j12"><?=nl2br($data_17_1_situation_background)?></font></td>
	</tr>

	<tr align="left">
		<td colspan="4" bgcolor="E3E3FF"><font class="j12">18.あなたはどのようにすればこの事例を防ぐことができたと思いますか？（簡単に記載して下さい）</font></td>
	</tr>

	<tr align="left">
		<td colspan="4"><font class="j12"><?=nl2br($data_18_1_how_prevention)?></font></td>
	</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="5"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block2">

	<tr align="left">
		<td colspan="4" bgcolor="8EFFA8"><font class="j12">以下管理者記載</font></td>
	</tr>
	
	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">総計</font></td>
		<td colspan="2"><font class="j12"><?=$data_19_1_all_total_fee?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="4" bgcolor="8EFFA8"><font class="j12">■検査費用（HBV、HCV、肝機能など）</font></td>
	</tr>
	
	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">1）該当患者に実施した検査</font></td>
		<td colspan="2"><font class="j12"><?=$data_19_2_patient_exam?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">2）受傷者に実施した検査</font></td>
		<td colspan="2"><font class="j12"><?=$data_19_3_injury_exam?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">小計</font></td>
		<td colspan="2"><font class="j12"><?=$data_19_4_shokei?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">■業務中断/職場離脱</font></td>
		<td colspan="2"><font class="j12"><?=$data_19_5_job_break?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">■代務採用経費</font></td>
		<td colspan="2"><font class="j12"><?=$data_19_6_replace_employ?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">合計</font></td>
		<td colspan="2"><font class="j12"><?=$data_19_7_gokei?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">■感染・発症予防措置の費用（HB免疫グロプリン、抗HIV予防投薬など）</font></td>
		<td colspan="2"><font class="j12"><?=$data_19_8_infection_fee?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">■発症後の治療費用</font></td>
		<td colspan="2"><font class="j12"><?=$data_19_9_cure_fee?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="4" bgcolor="8EFFA8"><font class="j12">(治療内容)</font></td>
	</tr>

	<tr align="left">
		<td colspan="4"><font class="j12"><?=nl2br($data_19_10_cure_memo)?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="8EFFA8"><font class="j12">公務・労務災害補償を申請しましたか？</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_6_1_user_select' and record_id='$data_19_11_insurance_apply'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td><font class="j12"><?=$get_data?></font></td>
		<td bgcolor="8EFFA8"><font class="j12">はいの場合、認定されましたか？</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_6_1_user_select' and record_id='$data_19_12_apply_certif'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td><font class="j12"><?=$get_data?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="8EFFA8"><font class="j12">認定年月日/病休日数/就業制限</font></td>
		<td colspan="3"><font class="j12"><?=$data_19_13_certif_date?> / <?=$data_19_14_break_days?>日 / <?=$data_19_15_restraint_days?>日</font></td>
	</tr>

</table>

<?
}
else
{
	//粘膜汚染の印刷項目
?>	
	
	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">6.汚染した体液</font></td>
	</tr>
	<?
	$str_chk_name = "";
	$cond_cnt = 0;
	$cond_sql = "";
	if($data_6_1_fluid_blood[0] == "01")
	{
		$cond_sql = " and (record_id = '01' "; 
		$cond_cnt++;
	}
		
	if($data_6_2_fluid_vomit[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '02' ";}
		else
			{$cond_sql .= " or record_id = '02' ";}
		$cond_cnt++;
	}
	
	if($data_6_2_fluid_vomit[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '02' ";}
		else
			{$cond_sql .= " or record_id = '02' ";}
		$cond_cnt++;
	}
	
	if($data_6_3_fluid_phlegm[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '03' ";}
		else
			{$cond_sql .= " or record_id = '03' ";}
		$cond_cnt++;
	}
	
	if($data_6_4_fluid_saliva[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '04' ";}
		else
			{$cond_sql .= " or record_id = '04' ";}
		$cond_cnt++;
	}
	
	if($data_6_5_fluid_spinal_cord[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '05' ";}
		else
			{$cond_sql .= " or record_id = '05' ";}
		$cond_cnt++;
	}
	
	if($data_6_6_fluid_ascites[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '06' ";}
		else
			{$cond_sql .= " or record_id = '06' ";}
		$cond_cnt++;
	}
	
	if($data_6_7_fluid_pleural[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '07' ";}
		else
			{$cond_sql .= " or record_id = '07' ";}
		$cond_cnt++;
	}
	
	if($data_6_8_fluid_amniotic[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '08' ";}
		else
			{$cond_sql .= " or record_id = '08' ";}
		$cond_cnt++;
	}
	
	if($data_6_9_fluid_urine[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '09' ";}
		else
			{$cond_sql .= " or record_id = '09' ";}
		$cond_cnt++;
	}
	
	if($data_6_10_fluid_etc[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '99' ";}
		else
			{$cond_sql .= " or record_id = '99' ";}
		$cond_cnt++;
	}
	
	if($cond_sql != "")
	{
		$cond_sql .= ")";

		$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_6_1_fluid_blood' ".$cond_sql;
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
		
		$str_chk_name = "";
		while($row = pg_fetch_array($sel))
		{
			$str_chk_name .= "「".$row["record_caption"].$row["record_sub_caption"]."」  ";
		}
	}
?>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">皮膚・粘膜はどの体液で汚染しましたか？</font></td>
		<td colspan="3"><font class="j12"><?=$str_chk_name?></font></td>
	</tr>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">その他記載</font></td>
		<td colspan="3"><font class="j12"><?=$data_6_11_fluid_etc_memo?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">7.汚染組織・状態</font></td>
	</tr>
	
	<?
	$str_chk_name = "";
	$cond_cnt = 0;
	$cond_sql = "";
	if($data_7_1_part_clean_skin[0] == "01")
	{
		$cond_sql = " and (record_id = '01' "; 
		$cond_cnt++;
	}
	
	if($data_7_2_part_blemished_skin[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '02' ";}
		else
			{$cond_sql .= " or record_id = '02' ";}
		$cond_cnt++;
	}

	if($data_7_3_part_eye[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '03' ";}
		else
			{$cond_sql .= " or record_id = '03' ";}
		$cond_cnt++;
	}
	
	if($data_7_4_part_nose[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '04' ";}
		else
			{$cond_sql .= " or record_id = '04' ";}
		$cond_cnt++;
	}
	
	if($data_7_5_part_mouth[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '05' ";}
		else
			{$cond_sql .= " or record_id = '05' ";}
		$cond_cnt++;
	}
	
	if($data_7_6_part_etc[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '99' ";}
		else
			{$cond_sql .= " or record_id = '99' ";}
		$cond_cnt++;
	}
	
	if($cond_sql != "")
	{
		$cond_sql .= ")";
		
		$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_7_1_part_clean_skin' ".$cond_sql;
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
		
		$str_chk_name = "";
		while($row = pg_fetch_array($sel))
		{
			$str_chk_name .= "「".$row["record_caption"].$row["record_sub_caption"]."」  ";
		}
	}	
?>	

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">皮膚・粘膜はどの体液で汚染しましたか？</font></td>
		<td colspan="3"><font class="j12"><?=$str_chk_name?></font></td>
	</tr>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">その他記載</font></td>
		<td colspan="3"><font class="j12"><?=$data_7_7_part_etc_memo?></font></td>
	</tr>



	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">8.汚染時の状況</font></td>
	</tr>
	
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">血液・体液はどのようにして接触しましたか？</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_8_1_touch' and record_id='$data_8_1_touch'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td colspan="3"><font class="j12"><?=$get_data?><?=$get_data2?></font></td>
	</tr>



	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">9.汚染時の防衣・防具</font></td>
	</tr>
	

	<tr align="left">
		<td bgcolor="E3E3FF" rowspan="7"><font class="j12">汚染したときどのような防衣・防具をつけていましたか？<br>（使用していた防衣・防具について該当項目を全てチェック、手袋の場合はブランド名を明記）</font></td>
	</tr>
	

	<tr align="left">
	<?
	$cond_sql = "";
	$str_chk_name = "　";
	if($data_9_1_cloth_no_glove[0] == "01")
	{
		$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_9_1_cloth_no_glove' and record_id='01'";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
		
		$str_chk_name = "";
		while($row = pg_fetch_array($sel))
		{
			$str_chk_name = "「".$row["record_caption"].$row["record_sub_caption"]."」  ";
		}
	}
?>
		<td colspan="3"><font class="j12"><?=$str_chk_name?></font></td>
	</tr>
	

	<tr align="left">
	<?
	$cond_sql = "";
	$str_chk_name = "　";
	if($data_9_2_cloth_one_glove[0] == "01")
	{
		$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_9_1_cloth_no_glove' and record_id='02'";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
		
		$str_chk_name = "";
		while($row = pg_fetch_array($sel))
		{
			$str_chk_name = "「".$row["record_caption"].$row["record_sub_caption"]."」  ";
		}
	}
	
?>
		<td colspan="3"><font class="j12"><?=$str_chk_name?>(ブランド名：<?=$data_9_3_cloth_one_glove_memo?>)</font></td>
	</tr>
	

	<tr align="left">
	<?
	$cond_sql = "";
	$str_chk_name = "　";
	if($data_9_4_cloth_double_glove[0] == "01")
	{
		$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_9_1_cloth_no_glove' and record_id='03'";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
		
		$str_chk_name = "";
		while($row = pg_fetch_array($sel))
		{
			$str_chk_name = "「".$row["record_caption"].$row["record_sub_caption"]."」  ";
		}
	}
	
?>
		<td colspan="3"><font class="j12"><?=$str_chk_name?>(ブランド名：<?=$data_9_5_cloth_double_glove_memo?>)</font></td>

	</tr>
	

	<tr align="left">
	<?
	$str_chk_name = "";
	$cond_cnt = 0;
	$cond_sql = "";
	if($data_9_6_cloth_goggle[0] == "01")
	{
		$cond_sql = " and (record_id = '04' "; 
		$cond_cnt++;
	}
	
	if($data_9_7_cloth_glass[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '05' ";}
		else
			{$cond_sql .= " or record_id = '05' ";}
		$cond_cnt++;
	}
	
	if($data_9_8_cloth_glass_aside[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '06' ";}
		else
			{$cond_sql .= " or record_id = '06' ";}
		$cond_cnt++;
	}
	
	if($data_9_9_cloth_face_shield[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '07' ";}
		else
			{$cond_sql .= " or record_id = '07' ";}
		$cond_cnt++;
	}
	
	if($data_9_10_cloth_ope_mask[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '08' ";}
		else
			{$cond_sql .= " or record_id = '08' ";}
		$cond_cnt++;
	}
	
	if($data_9_11_cloth_glass_mask[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '09' ";}
		else
			{$cond_sql .= " or record_id = '09' ";}
		$cond_cnt++;
	}
	
	if($data_9_12_cloth_ope_gown[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '10' ";}
		else
			{$cond_sql .= " or record_id = '10' ";}
		$cond_cnt++;
	}
	
	if($data_9_13_cloth_gown[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '11' ";}
		else
			{$cond_sql .= " or record_id = '11' ";}
		$cond_cnt++;
	}
	
	if($data_9_14_cloth_experiment[0] == "01")
	{
		if($cond_cnt < 1)
			{$cond_sql = " and (record_id = '12' ";}
		else
			{$cond_sql .= " or record_id = '12' ";}
		$cond_cnt++;
	}
	
	
	if($cond_sql != "")
	{
		$cond_sql .= ")";
		
		$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_9_1_cloth_no_glove' ".$cond_sql;
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
		
		$str_chk_name = "";
		while($row = pg_fetch_array($sel))
		{
			$str_chk_name .= "「".$row["record_caption"].$row["record_sub_caption"]."」  ";
		}
		
	}	
	
?>	
		<td colspan="3"><font class="j12"><?=$str_chk_name?></font></td>

	</tr>
	
	
	

	<tr align="left">
	<?
	$cond_sql = "";
	$str_chk_name = "　";
	if($data_9_15_cloth_exp_etc[0] == "01")
	{
		$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_9_1_cloth_no_glove' and record_id='13'";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
		
		$str_chk_name = "";
		while($row = pg_fetch_array($sel))
		{
			$str_chk_name = "「".$row["record_caption"].$row["record_sub_caption"]."」  ";
		}
	}
	
?>
		<td colspan="3"><font class="j12"><?=$str_chk_name?>(ブランド名：<?=$data_9_16_cloth_exp_etc_memo?>)</font></td>
	</tr>
	

	<tr align="left">
	<?
	$cond_sql = "";
	$str_chk_name = "　";
	if($data_9_17_cloth_etc[0] == "01")
	{
		$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_9_1_cloth_no_glove' and record_id='99'";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
		
		$str_chk_name = "";
		while($row = pg_fetch_array($sel))
		{
			$str_chk_name = "「".$row["record_caption"].$row["record_sub_caption"]."」  ";
		}
	}
	
?>
		<td colspan="3"><font class="j12"><?=$str_chk_name?>(ブランド名：<?=$data_9_18_cloth_etc_memo?>)</font></td>
	</tr>
	

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">10.汚染理由</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">汚染はどのようにして起こりましたか？</font></td>
	<?
	$cond_sql = "";
	$str_chk_name = "　";
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_10_1_how_arise' and record_id='$data_10_1_how_arise'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	
	$str_chk_name = "";
	while($row = pg_fetch_array($sel))
	{
		$str_chk_name = $row["record_caption"].$row["record_sub_caption"];
	}
?>
		<td colspan="3"><font class="j12"><?=$str_chk_name?></font></td>
	</tr>
	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">チューブ記載</font></td>
		<td colspan="3"><font class="j12"><?=$data_10_2_how_arise_tube?></font></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">11.汚染時間</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">皮膚・粘膜はどの位のあいだ血液・体液に接触していましたか？</font></td>
	<?
	$cond_sql = "";
	$str_chk_name = "　";
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_11_1_how_long' and record_id='$data_11_1_how_long'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	
	$str_chk_name = "";
	while($row = pg_fetch_array($sel))
	{
		$str_chk_name = $row["record_caption"].$row["record_sub_caption"];
	}
?>
		<td colspan="3"><font class="j12"><?=$str_chk_name?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">12.接触量</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">皮膚・粘膜に接触した血液・体液の量はどの位でしたか？</font></td>
	<?
	$cond_sql = "";
	$str_chk_name = "　";
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_12_1_how_much' and record_id='$data_12_1_how_much'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	
	$str_chk_name = "";
	while($row = pg_fetch_array($sel))
	{
		$str_chk_name = $row["record_caption"].$row["record_sub_caption"];
	}
?>
		<td colspan="3"><font class="j12"><?=$str_chk_name?></font></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">13.汚染部位</font></td>
	</tr>

	<tr align="left">
		<td colspan="2" rowspan="4" bgcolor="E3E3FF"><font class="j12">汚染部位を選択して下さい。身体部位番号域を越えた広範囲の汚染部位の場合、汚染の著しい順に選択して下さい</font></td>
	</tr>

	<tr align="left">
		<td colspan="2"><font class="j12">「<?=$data_13_1_injury_deep?>」<?=$data_13_11_injury_deep_name?></font></td>
	</tr>
	<tr align="left">
		<td colspan="2"><font class="j12">「<?=$data_13_2_injury_middle?>」<?=$data_13_21_injury_middle_name?></font></td>
	</tr>
	<tr align="left">
		<td colspan="2"><font class="j12">「<?=$data_13_3_injury_superficial?>」<?=$data_13_31_injury_superficial_name?></font></td>
	</tr>
	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><img border="0" src="images/epinetB.GIF" alt="イラスト1"></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">14.HBs抗体</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">あなた自身はHBs抗体陽性ですか？</font></td>
	<?
	$cond_sql = "";
	$str_chk_name = "　";
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_15_1_hbs_positivity' and record_id='$data_14_1_hbs_positivity'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	
	$str_chk_name = "";
	while($row = pg_fetch_array($sel))
	{
		$str_chk_name = $row["record_caption"].$row["record_sub_caption"];
	}
?>
		<td colspan="3"><font class="j12"><?=$str_chk_name?></font></td>
	</tr>


	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">15.緊急処置受傷</font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF"><font class="j12">緊急処置時（蘇生時を含む）の受傷でしたか？</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_6_1_user_select' and record_id='$data_15_1_emergency_hurt'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		
		<td colspan="3"><font class="j12"><?=$get_data?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">16.汚染時の状況及び背景について</font></td>
	</tr>

	<tr align="left">
		<td colspan="4" bgcolor="E3E3FF"><font class="j12">下記の<font class="j12" color="red">(1)</font>〜<font class="j12" color="red">(6)</font>を含めて詳しく記載して下さい。<br>
					<font class="j12" color="red">(1)</font>具体的な発生現場（階、病棟、ナースステーション等）
					<font class="j12" color="red">(2)</font>発生時にどのような仕事、行為をしていたか？
					<font class="j12" color="red">(3)</font>汚染原因の器材
					<font class="j12" color="red">(4)</font>どのようにして発生したか？
					<font class="j12" color="red">(5)</font>特別な事情・状況・背景等
					<font class="j12" color="red">(6)</font>汚染後の処置・対応
					</font></td>
	</tr>
	<tr align="left">
		<td colspan="4"><font class="j12"><?=nl2br($data_16_1_situation_background)?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="E3E3FF" colspan="4"><font class="j12">17.あなたはどのようにすればこの事例を防ぐことができたと思いますか？<br>（簡単に記載して下さい）</font></td>
	</tr>

	<tr align="left">
		<td colspan="4"><font class="j12"><?=nl2br($data_17_1_how_prevention)?></font></td>
	</tr>

</table>

<img src="img/spacer.gif" alt="" width="1" height="5"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block2">

	<tr align="left">
		<td colspan="4" bgcolor="8EFFA8"><font class="j12">以下管理者記載</font></td>
	</tr>
	
	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">総計</font></td>
		<td colspan="2"><font class="j12"><?=$data_18_1_all_total_fee?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="4" bgcolor="8EFFA8"><font class="j12">■検査費用（HBV、HCV、肝機能など）</font></td>
	</tr>
	
	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">1）該当患者に実施した検査</font></td>
		<td colspan="2"><font class="j12"><?=$data_18_2_patient_exam?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">2）受傷者に実施した検査</font></td>
		<td colspan="2"><font class="j12"><?=$data_18_3_injury_exam?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">小計</font></td>
		<td colspan="2"><font class="j12"><?=$data_18_4_shokei?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">■業務中断/職場離脱</font></td>
		<td colspan="2"><font class="j12"><?=$data_18_5_job_break?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">■代務採用経費</font></td>
		<td colspan="2"><font class="j12"><?=$data_18_6_replace_employ?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">合計</font></td>
		<td colspan="2"><font class="j12"><?=$data_18_7_gokei?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">■感染・発症予防措置の費用（HB免疫グロプリン、抗HIV予防投薬など）</font></td>
		<td colspan="2"><font class="j12"><?=$data_18_8_infection_fee?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="2" bgcolor="8EFFA8"><font class="j12">■発症後の治療費用</font></td>
		<td colspan="2"><font class="j12"><?=$data_18_9_cure_fee?>円</font></td>
	</tr>

	<tr align="left">
		<td colspan="4" bgcolor="8EFFA8"><font class="j12">(治療内容)</font></td>
	</tr>

	<tr align="left">
		<td colspan="4"><font class="j12"><?=nl2br($data_18_10_cure_memo)?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="8EFFA8"><font class="j12">公務・労務災害補償を申請しましたか？</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_6_1_user_select' and record_id='$data_18_11_insurance_apply'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td><font class="j12"><?=$get_data?></font></td>
		<td bgcolor="8EFFA8"><font class="j12">はいの場合、認定されましたか？</font></td>
	<?
	$sql = "select record_caption,record_sub_caption from fplus_epi_master where field_name='data_6_1_user_select' and record_id='$data_18_12_apply_certif'";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {pg_close($con);echo("<script type='text/javascript' src='./js/showpage.js'></script>");echo("<script language='javascript'>showErrorPage(window);</script>");	exit;}
	$get_data = pg_fetch_result($sel, 0, "record_caption");
	$get_data2 = pg_fetch_result($sel, 0, "record_sub_caption");
?>
		<td><font class="j12"><?=$get_data?></font></td>
	</tr>

	<tr align="left">
		<td bgcolor="8EFFA8"><font class="j12">認定年月日/病休日数/就業制限</font></td>
		<td colspan="3"><font class="j12"><?=$data_18_13_certif_date?> / <?=$data_18_14_break_days?>日 / <?=$data_18_15_restraint_days?>日</font></td>
	</tr>

</table>

<?	
}
?>


<!-- BODY全体 END -->

</form>
</body>
</html>
<?
//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>

