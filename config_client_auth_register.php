<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 環境設定｜端末認証履歴</title>
<?
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 20, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$auth_key = $_COOKIE["caid"];

// データベースに接続
$con = connect2db($fname);

// 認証キーが未設定の場合
if ($auth_key == "") {
	$message = "本端末は認証キー未発行またはクリアーされています。";

	// 発行IDの仮採番（現在の最大値を取得し、1を足す）
	$sql = "select max(issue_id) from client_auth";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$issue_id = (pg_num_rows($sel) == 0) ? 1 : intval(pg_fetch_result($sel, 0, 0)) + 1;

// 認証キーが設定されている場合
} else {

	// 認証キーが有効かどうか確認する
	$sql = "select issue_id from client_auth";
	$cond = "where auth_key = '$auth_key' and not disabled";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$message = "本端末は認証キー発行済みです。";
	} else {
		$message = "本端末の認証キーは無効です。有効にするためには、管理者に連絡してください。";
	}
}

// 発行者＝ログイン者の情報を取得
$sql = "select e.emp_lt_nm, e.emp_ft_nm, c.class_nm, a.atrb_nm, d.dept_nm, r.room_nm from empmst e inner join classmst c on e.emp_class = c.class_id inner join atrbmst a on e.emp_attribute = a.atrb_id inner join deptmst d on e.emp_dept = d.dept_id left join classroom r on e.emp_room = r.room_id";
$cond = "where e.emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$issuer_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
$dept = format_issuer_dept(
	pg_fetch_result($sel, 0, "class_nm"),
	pg_fetch_result($sel, 0, "atrb_nm"),
	pg_fetch_result($sel, 0, "dept_nm"),
	pg_fetch_result($sel, 0, "room_nm"),
	$room_nm
);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
.list td .inner {border-collapse:collapse;}
.list td .inner td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>端末認証キー発行</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="6"><br>

<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($message); ?></font></td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="4"><br>

<? if ($auth_key == "") { ?>
<form action="config_client_auth_insert.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="20%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発行No</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($issue_id); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="20%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">端末番号</font></td>
<td><input type="text" name="client_no" value="" style="ime-mode:inactive;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="20%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備考</font></td>
<td><input type="text" name="note" value="" size="50" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発行者</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($issuer_name); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($dept); ?></font></td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="4"><br>

<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td align="right"><input type="submit" value="認証キー発行"></td>
</tr>
</table>

<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<? } ?>

</center>
</body>
<? pg_close($con); ?>
</html>
<?
function format_issuer_dept($class_nm, $atrb_nm, $dept_nm, $room_nm) {
	$issuer_dept = $class_nm . "＞" . $atrb_nm . "＞" . $dept_nm;
	if (strlen($room_nm) > 0) $issuer_dept .= "＞" . $room_nm;
	return $issuer_dept;
}
