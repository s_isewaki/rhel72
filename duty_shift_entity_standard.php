<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成｜管理｜施設基準登録</title>

<?
// ini_set("display_errors","1");
require_once("about_comedix.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_manage_tab_common.ini");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//管理画面用
$chk_flg = $obj->check_authority_Management($session, $fname);
if ($chk_flg == "") {
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
//ユーザ画面用
$section_admin_auth = $obj->check_authority_user($session, $fname);
///-----------------------------------------------------------------------------
//初期値設定
///-----------------------------------------------------------------------------
    ///-----------------------------------------------------------------------------
    //施設基準
    ///-----------------------------------------------------------------------------
    if ($standard_id == "") {
        $standard_id = "1";
    }
    ///-----------------------------------------------------------------------------
    //届出区分情報の取得
    ///-----------------------------------------------------------------------------
    $report_kbn_array = $obj->get_report_kbn_array();
    ///-----------------------------------------------------------------------------
    //施設基準情報の取得
    ///-----------------------------------------------------------------------------
    if ($reload_flg == "") {
        //施設基準情報の取得
        $data_array = $obj->get_duty_shift_institution_standard_array($standard_id);
        if (count($data_array) > 0) {
            $ward_cnt = $data_array[0]["ward_cnt"];                     //病棟数
            $sickbed_cnt = $data_array[0]["sickbed_cnt"];               //病床数
            $report_kbn = $data_array[0]["report_kbn"];                 //届出区分
            $report_patient_cnt = $data_array[0]["report_patient_cnt"]; //届出時入院患者数
            $hosp_patient_cnt = $data_array[0]["hosp_patient_cnt"];     //１日平均入院患者数
            $nursing_assistance_flg = $data_array[0]["nursing_assistance_flg"]; //看護補助加算の有無
            $hospitalization_cnt = $data_array[0]["hospitalization_cnt"];       //平均在院日数 20130402項目復活

            //夜勤時間帯
            $wk = $data_array[0]["prov_start_hhmm"];    //開始時分
            $prov_start_hh = substr($wk, 0, 2);
            $prov_start_mm = substr($wk, 2, 2);
            $wk = $data_array[0]["prov_end_hhmm"];      //終了時分
            $prov_end_hh = substr($wk, 0, 2);
            $prov_end_mm = substr($wk, 2, 2);

            $limit_time = $data_array[0]["limit_time"]; //月平均夜勤時間上限
            $shift_flg = $data_array[0]["shift_flg"];   //交代制

            //申し送り時間
            for($i=0; $i<3; $i++) {
                $k = $i + 1;
                $wk = $data_array[0]["send_start_hhmm_$k"]; //開始時分
                $send_start_hh[$i] = substr($wk, 0, 2);
                $send_start_mm[$i] = substr($wk, 2, 2);
                $wk = $data_array[0]["send_end_hhmm_$k"];   //終了時分
                $send_end_hh[$i] = substr($wk, 0, 2);
                $send_end_mm[$i] = substr($wk, 2, 2);
            }
            $labor_cnt = $data_array[0]["labor_cnt"];   //所定労働時間

            $nurse_staffing_flg = $data_array[0]["nurse_staffing_flg"]; //看護配置加算の有無
            $acute_nursing_flg = $data_array[0]["acute_nursing_flg"];   //急性期看護補助加算の有無
            $acute_nursing_cnt = $data_array[0]["acute_nursing_cnt"];   //急性期看護補助加算の配置比率
            $nursing_assistance_cnt = $data_array[0]["nursing_assistance_cnt"]; //看護補助加算の配置比率
            //様式９平成２４年度改訂対応 20120406
            $acute_assistance_flg = $data_array[0]["acute_assistance_flg"]; //急性期看護補助体制加算の届出区分
            $acute_assistance_cnt = $data_array[0]["acute_assistance_cnt"]; //急性期看護補助体制加算の配置比率
            $night_acute_assistance_flg = $data_array[0]["night_acute_assistance_flg"]; //夜間急性期看護補助体制加算の届出区分
            $night_acute_assistance_cnt = $data_array[0]["night_acute_assistance_cnt"]; //夜間急性期看護補助体制加算の配置比率
            $night_shift_add_flg = $data_array[0]["night_shift_add_flg"];   //看護職員夜間配置加算の有無
        }
    }
    ///-----------------------------------------------------------------------------
    //交代制（２：２交代、３：３交代）
    ///-----------------------------------------------------------------------------
    if ($shift_flg == "") {
        $shift_flg = 2;
    }

/**
 * 届出区分が「特定入院料」又は「特定入院料（看護職員＋看護補助者）」の場合のみ表示する詳細設定を作成する
 * 
 * @param array  $data       施設基準(duty_shift_institution_standard)データ
 * @param string $report_kbn 届出区分
 * 
 * @return void
 */
function createReportKbnSpecificDetail($data, $report_kbn)
{
    // 表示する為に特殊文字をHTMLエンティティに変換
    $specific_type = h($data["specific_type"]);
    $nurse_staffing_cnt = h($data["nurse_staffing_cnt"]);
    $assistance_staffing_cnt = h($data["assistance_staffing_cnt"]);
    
    echo <<<_HTML_
    <tr height="22">
      <td bgcolor="#f6f9ff" align="right">特定入院料種類</td>
      <td colspan="3">
        <input name="specific_type" type="text" value="{$specific_type}" size="55" maxlength="30" style="ime-mode: active;">
      </td>
    </tr>
_HTML_;

    if ($report_kbn == REPORT_KBN_SPECIFIC) {    // 届出区分が特定入院料
        echo <<<_HTML_
    <tr height="22">
      <td bgcolor="#f6f9ff" align="right">看護配置</td>
      <td>
        <input name="nurse_staffing_cnt" type="text" value="{$nurse_staffing_cnt}" size="5" maxlength="2" style="ime-mode: disabled;">&nbsp;対1
      </td>
      <td bgcolor="#f6f9ff" align="right">看護補助配置</td>
      <td>
        <input name="assistance_staffing_cnt" type="text" value="{$assistance_staffing_cnt}" size="5" maxlength="2" style="ime-mode: disabled;">&nbsp;対1
      </td>
    </tr>
_HTML_;
    } elseif ($report_kbn == REPORT_KBN_SPECIFIC_PLUS) {    // 届出区分が特定入院料（看護職員＋看護補助者）
        echo <<<_HTML_
    <tr height="22">
      <td bgcolor="#f6f9ff" align="right">看護配置・看護補助配置</td>
      <td colspan="3">
        <input name="nurse_staffing_cnt" type="text" value="{$nurse_staffing_cnt}" size="5" maxlength="2" style="ime-mode: disabled;">&nbsp;対1
      </td>
    </tr>
_HTML_;
    }
}
?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
    ///-----------------------------------------------------------------------------
    // 交代制変更
    ///-----------------------------------------------------------------------------
    function shiftChg()
    {
        document.mainform.reload_flg.value = "1";
        document.mainform.action ="duty_shift_entity_standard.php";
        document.mainform.submit();
    }
    ///-----------------------------------------------------------------------------
    // 登録
    ///-----------------------------------------------------------------------------
    function insertData()
    {
        var shift_cnt = document.mainform.shift_cnt.value;
        var i=0;
        var wk="";
        var wk_cnt1=0;
        var wk_cnt2=0;
        var wk1="";
        var wk2="";
        var wk3="";
        var wk4="";
        var start_hhmm = new Array(shift_cnt);
        var end_hhmm =  new Array(shift_cnt);

        ///-----------------------------------------------------------------------------
        // 数値チェック
        ///-----------------------------------------------------------------------------
        //病棟数
        wk = document.mainform.ward_cnt.value;
        if (wk.match(/[^0-9]/g)) {
            alert("病棟数に数値以外が設定されています。");
            return;
        }
        //病床数
        wk = document.mainform.sickbed_cnt.value;
        if (wk.match(/[^0-9]/g)) {
            alert("病床数に数値以外が設定されています。");
            return;
        }
        //届出時入院患者数
        wk = document.mainform.report_patient_cnt.value;
        if (wk.match(/[^0-9]/g)) {
            alert("届出時入院患者数に数値以外が設定されています。");
            return;
        }
        //１日平均入院患者数
        wk = document.mainform.hosp_patient_cnt.value;
        if (wk.match(/[^0-9]/g)) {
            alert("１日平均入院患者数に数値以外が設定されています。");
            return;
        }
        //平均在院日数
        wk = document.mainform.hospitalization_cnt.value;
        if (wk.match(/[^0-9]/g)) {
            alert("平均在院日数に数値以外が設定されています。");
            return;
        }
        //月平均夜勤時間上限
        wk = document.mainform.limit_time.value;
        if (wk.match(/[^0-9]/g)) {
            alert("月平均夜勤時間上限に数値以外が設定されています。");
            return;
        }
        //常勤職員の所定労働時間
        wk = document.mainform.labor_cnt.value;
        if (!wk.match(/^\d$|^\d+\.?\d+$/g)) {
            alert("常勤職員の週所定労働時間に数値以外が設定されています。");
            return;
        }
        ///-----------------------------------------------------------------------------
        // 夜勤時間帯
        ///-----------------------------------------------------------------------------
        wk1 = document.mainform.prov_start_hh.value;
        wk2 = document.mainform.prov_start_mm.value;
        wk3 = document.mainform.prov_end_hh.value;
        wk4 = document.mainform.prov_end_mm.value;
        if ((wk1 != "") || (wk2 != "") || (wk3 != "") || (wk4 != "")) {
            ///-----------------------------------------------------------------------------
            // 未入力チェック（夜勤時間帯）
            ///-----------------------------------------------------------------------------
            if (((wk1 !="") && (wk2 == "")) ||
                ((wk1 =="") && (wk2 != ""))) {
                alert("夜勤時間帯の開始時間が正しくありません。");
                return;
            }
            if (((wk3 !="") && (wk4 == "")) ||
                ((wk3 =="") && (wk4 != ""))) {
                alert("夜勤時間帯の終了時間が正しくありません。");
                return;
            }
            ///-----------------------------------------------------------------------------
            // 時間チェック（夜勤時間帯）
            ///-----------------------------------------------------------------------------
            var prov_start_hh = 0;
            var prov_start_mm = 0;
            var prov_end_hh = 0;
            var prov_end_mm = 0;
            prov_start_hh = document.mainform.prov_start_hh.value;
            prov_start_mm = document.mainform.prov_start_mm.value;
            prov_end_hh = document.mainform.prov_end_hh.value;
            prov_end_mm = document.mainform.prov_end_mm.value;
            if (prov_start_hh > prov_end_hh) {
                prov_end_hh = parseInt(prov_end_hh,10) + 24;
            }
            var prov_start_hhmm = parseInt(prov_start_hh,10) * 60 + parseInt(prov_start_mm,10);
            var prov_end_hhmm = parseInt(prov_end_hh,10) * 60 + parseInt(prov_end_mm,10);
            wk_cnt1 = 16 * 60;
            wk_cnt2 = parseInt(prov_end_hhmm,10) - parseInt(prov_start_hhmm,10);

//var wk = "prov_start_hh=" +  parseInt(prov_start_hh,10) +  " prov_end_hh=" +  parseInt(prov_end_hh,10);
//alert(wk);
            //１６時間以内か
            //if (wk_cnt2 > wk_cnt1) {
            //  alert("夜勤時間帯が１６時間以内ではありません。");
            //  return;
            //}
            //２２時から翌朝５時までを含むか
            prov_start_hh = document.mainform.prov_start_hh.value;
            prov_start_mm = document.mainform.prov_start_mm.value;
            prov_end_hh = document.mainform.prov_end_hh.value;
            prov_end_mm = document.mainform.prov_end_mm.value;
            if (prov_start_hh > prov_end_hh) {
                wk_cnt1 = 22 * 60;
                wk_cnt2 = 29 * 60;
                if (((prov_start_hhmm < wk_cnt1) && (prov_end_hhmm < wk_cnt1)) ||
                    ((prov_start_hhmm > wk_cnt2) && (prov_end_hhmm > wk_cnt2))) {
                    alert("夜勤時間帯が２２時から翌朝５時までを含む時間帯ではありません。");
                    return;
                }
            } else {
                var wk_flg = "";
                prov_start_hhmm = parseInt(prov_start_hh,10) * 60 + parseInt(prov_start_mm,10);
                prov_end_hhmm = parseInt(prov_end_hh,10) * 60 + parseInt(prov_end_mm,10);
                wk_cnt1 = 22 * 60;
                wk_cnt2 = 24 * 60;
                if ((prov_start_hhmm >= wk_cnt1) && (prov_start_hhmm <= wk_cnt2)) { wk_flg = "1"; }
                if ((prov_end_hhmm >= wk_cnt1) && (prov_end_hhmm <= wk_cnt2)) { wk_flg = "1"; }
                wk_cnt1 = 0;
                wk_cnt2 = 5 * 60;
                if ((prov_start_hhmm >= wk_cnt1) && (prov_start_hhmm <= wk_cnt2)) { wk_flg = "1"; }
                if ((prov_end_hhmm >= wk_cnt1) && (prov_end_hhmm <= wk_cnt2)) { wk_flg = "1"; }
                if (wk_flg == "") {
                    alert("夜勤時間帯が２２時から翌朝５時までを含む時間帯ではありません。");
                    return;
                }
            }
        }
        ///-----------------------------------------------------------------------------
        // 時間チェック（申し送り)
        ///-----------------------------------------------------------------------------
        for(i=0;i<shift_cnt;i++){
            k = i + 1;
            wk1 = document.mainform.elements['send_start_hh[]'][i].value;
            wk2 = document.mainform.elements['send_start_mm[]'][i].value;
            if (((wk1 !="") && (wk2 == "")) ||
                ((wk1 =="") && (wk2 != ""))) {
                alert("申し送り時刻" + k + "の開始時間が正しくありません。");
                return;
            }
            start_hhmm[i] = wk1 + wk2;
            wk1 = document.mainform.elements['send_end_hh[]'][i].value;
            wk2 = document.mainform.elements['send_end_mm[]'][i].value;
            if (((wk1 !="") && (wk2 == "")) ||
                ((wk1 =="") && (wk2 != ""))) {
                alert("申し送り時刻" + k + "の終了時間が正しくありません。");
                return;
            }
            end_hhmm[i] = wk1 + wk2;
            if (start_hhmm[i] > end_hhmm[i]) {
                alert("申し送り時間が" + k + "　開始時間　＞　終了時間　です。");
                return;
            }
        }
        ///-----------------------------------------------------------------------------
        // 重複チェック（申し送り）
        ///-----------------------------------------------------------------------------
        for(i=0;i<shift_cnt;i++){
        for(k=0;k<shift_cnt;k++){
            if (i != k) {
                wk = "";
                if ((start_hhmm[i] <= start_hhmm[k]) &&
                    (start_hhmm[i] <= end_hhmm[k])) {
                    wk = "1";
                }
                if ((start_hhmm[i] >= start_hhmm[k]) &&
                    (start_hhmm[i] >= end_hhmm[k])) {
                    wk = "1";
                }
                if (wk == "") {
                    alert("申し送り時間が重複しています。");
                    return;
                }
            }
        }
        }

        ///-----------------------------------------------------------------------------
        // 登録
        ///-----------------------------------------------------------------------------
        document.mainform.reload_flg.value = "";
        document.mainform.action = "duty_shift_entity_standard_update_exe.php";
        document.mainform.submit();
    }
    ///-----------------------------------------------------------------------------
    // 届出区分変更
    ///-----------------------------------------------------------------------------
    function reportKbnChg()
    {
        document.mainform.reload_flg.value = "1";
        document.mainform.action ="duty_shift_entity_standard.php";
        document.mainform.submit();
    }
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse; width: 950px;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<?
		// 画面遷移
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_manage_title($session, $section_admin_auth);			//duty_shift_common.ini
		echo("</table>\n");

		// タブ
		$arr_option = "";
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_manage_tab_menuitem($session, $fname, $arr_option);	//duty_shift_manage_tab_common.ini
		echo("</table>\n");

		// 下線
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 入力エリア -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="mainform" method="post" style="margin: 0;">
		<!-- ------------------------------------------------------------------------ -->
		<!-- メニュー（施設基準１〜２０）-->
		<!-- ------------------------------------------------------------------------ -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="j12" style="font-family: ＭＳ Ｐゴシック, Osaka;">
		<tr height="22" >
		<td>&nbsp;&nbsp;
		<?
			for ($i=1;$i<=20;$i++){ //10件から20件へ変更 20120209
				$wk = "施設基準" . $i ."&nbsp;&nbsp;";
				if ($standard_id == $i) { echo($wk); } else {
					echo("<a href=\"duty_shift_entity_standard.php?session=$session&standard_id=$i\">$wk</a>&nbsp;&nbsp;");
				}
				if ($i == 10) {
					echo("<br>\n&nbsp;&nbsp;&nbsp;");
				}
			}
		?>
		</td>
		</tr>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 施設基準入力（１） -->
		<!-- ------------------------------------------------------------------------ -->
		<table border="0" cellspacing="0" cellpadding="2" class="list j12" style="font-family: ＭＳ Ｐゴシック, Osaka;">
		<tr height="22">
			<!-- 病棟数 -->
			<td width="28%" bgcolor="#f6f9ff" align="right">病棟数</td>
			<td width="27%">
			<input name="ward_cnt" type="text" value="<? echo(h($ward_cnt)); ?>" size= "5" maxlength="5" style="ime-mode:disabled ;">
			</td>
			<!-- 病床数 -->
			<td width="22%" bgcolor="#f6f9ff" align="right">病床数</td>
			<td width="23%">
			<input name="sickbed_cnt" type="text" value="<? echo(h($sickbed_cnt)); ?>" size="5" maxlength="5" style="ime-mode:disabled ;">
			</td>
		</tr>
		<tr height="22">
			<!-- 届出区分 -->
			<td bgcolor="#f6f9ff" align="right">届出区分</td>
			<td align="left">
			<select name="report_kbn" onChange="reportKbnChg();">
			<? 
				for($i=0; $i<count($report_kbn_array); $i++) {
					$wk_id = $report_kbn_array[$i]["id"];
					$wk_name = $report_kbn_array[$i]["name"];
					echo("<option value=\"$wk_id\"");
					if ($wk_id == $report_kbn) { echo(" selected"); }
					echo(">$wk_name \n");
				}
			?>
			</select>
			</td>
			<!-- 届出時入院患者数 -->
			<td bgcolor="#f6f9ff" align="right">届出時入院患者数</td>
			<td align="left">
			<input name="report_patient_cnt" type="text" value="<? echo(h($report_patient_cnt)); ?>" size="5" maxlength="5" style="ime-mode:disabled ;">
			</td>
		</tr>
<?
// 届出区分が「特定入院料」又は「特定入院料（看護職員＋看護補助者）」の場合のみ表示 20121011
if (($report_kbn == REPORT_KBN_SPECIFIC) || ($report_kbn == REPORT_KBN_SPECIFIC_PLUS)) {
    createReportKbnSpecificDetail($data_array[0], $report_kbn);
}
?>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 施設基準入力（２） -->
		<!-- ------------------------------------------------------------------------ -->
		<table border="0" cellspacing="0" cellpadding="2" class="list j12" style="font-family: ＭＳ Ｐゴシック, Osaka;">
		<tr height="22">
			<!-- １日平均入院患者数 -->
			<td width="28%" bgcolor="#f6f9ff" align="right">１日平均入院患者数</td>
			<td width="27%" colspan="1">
			<input name="hosp_patient_cnt" type="text" value="<? echo(h($hosp_patient_cnt)); ?>" size="5" maxlength="5" style="ime-mode:disabled;">
			<span style="color: red;">※端数切り上げ整数入力</span>
			</td>
			<!-- 平均在院日数 -->
			<td width="22%" bgcolor="#f6f9ff" align="right">平均在院日数</td>
			<td width="23%">
			<input name="hospitalization_cnt" type="text" value="<? echo(h($hospitalization_cnt)); ?>" size="5" maxlength="5" style="ime-mode:disabled;">
			<span style="color: red;">※端数切り上げ整数入力</span>
			</td>
		</tr>
<? if ($report_kbn != REPORT_KBN_SPECIFIC && $report_kbn != REPORT_KBN_SPECIFIC_PLUS) { // 届出区分による表示切替 20121011 start ?>
		<tr height="22">
			<td bgcolor="#f6f9ff" align="right">
    <? //様式９平成２６年度改訂対応 20140324
    //地域包括ケア病棟
    if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
         $wk_title = "看護職員配置加算の有無";
    }
    else {
        $wk_title = "看護配置加算の有無";
    }
    echo($wk_title);
    ?>
            </td>
			<td colspan="3">
			<label><input type="radio" name="nurse_staffing_flg" value="0"<? if ($nurse_staffing_flg != "1") {echo " checked ";} ?>>
			無</label>&nbsp;&nbsp;
			<label><input type="radio" name="nurse_staffing_flg" value="1"<? if ($nurse_staffing_flg == "1") {echo " checked ";} ?>>
			有</label>
    <? //様式９平成２６年度改訂対応 20140324
    //地域包括ケア病棟
    if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
        echo("&nbsp;（50対1）");
    }
    ?>
			</td>
		</tr>
        
        <? /* 様式９平成２４年度改訂対応 20120406 */ ?>
    <? // 様式９平成２６年度改訂対応 20140324
    //   文言削除「平成２４年度改訂対応<br>」
    //地域包括ケア病棟以外の場合
    if ($report_kbn != REPORT_KBN_COMMUNITY_CARE && $report_kbn != REPORT_KBN_COMMUNITY_CARE2) {
          ?>
		<tr height="22">
			<td bgcolor="#f6f9ff" align="right" style="border-bottom:none;">　急性期看護補助体制加算の届出区分</td>
			<td colspan="3" valign="bottom" style="border-bottom:none;">
			<label><input type="radio" name="acute_assistance_flg" value="0"<? if ($acute_assistance_flg != "1") {echo " checked ";} ?>>
			無</label>&nbsp;&nbsp;
			<label><input type="radio" name="acute_assistance_flg" value="1"<? if ($acute_assistance_flg == "1") {echo " checked ";} ?>>
			有</label>&nbsp;
			（
			<select id="acute_assistance_cnt" name="acute_assistance_cnt">
			<option value=""></option>
			<option value="25" <? if ($acute_assistance_cnt == "25") {echo(" selected");}?>>25</option>
			<option value="50" <? if ($acute_assistance_cnt == "50") {echo(" selected");}?>>50</option>
			<option value="75" <? if ($acute_assistance_cnt == "75") {echo(" selected");}?>>75</option>
			</select>
			対1）
			</td>
		</tr>
		<tr height="22">
			<td bgcolor="#f6f9ff" align="right" style="border-top:none;border-bottom:none;">　夜間急性期看護補助体制加算の届出区分</td>
			<td colspan="3" style="border-top:none;border-bottom:none;">
			<label><input type="radio" name="night_acute_assistance_flg" value="0"<? if ($night_acute_assistance_flg != "1") {echo " checked ";} ?>>
			無</label>&nbsp;&nbsp;
			<label><input type="radio" name="night_acute_assistance_flg" value="1"<? if ($night_acute_assistance_flg == "1") {echo " checked ";} ?>>
			有</label>&nbsp;
			（
			<select id="night_acute_assistance_cnt" name="night_acute_assistance_cnt">
			<option value=""></option>
			<option value="25" <? if ($night_acute_assistance_cnt == "25") {echo(" selected");}?>>25</option>
			<option value="50" <? if ($night_acute_assistance_cnt == "50") {echo(" selected");}?>>50</option>
			<option value="100" <? if ($night_acute_assistance_cnt == "100") {echo(" selected");}?>>100</option>
			</select>
			対1）
			</td>
		</tr>
		<tr height="22">
			<td bgcolor="#f6f9ff" align="right" style="border-top:none;">　看護職員夜間配置加算の有無</td>
			<td colspan="3" style="border-top:none;">
			<label><input type="radio" name="night_shift_add_flg" value="0"<? if ($night_shift_add_flg != "1") {echo " checked ";} ?>>
			無</label>&nbsp;&nbsp;
			<label><input type="radio" name="night_shift_add_flg" value="1"<? if ($night_shift_add_flg == "1") {echo " checked ";} ?>>
			有</label>&nbsp;
			</td>
		</tr>
        <? } ?>
		<!-- 看護補助加算の有無 -->
		<tr height="22">
			<td bgcolor="#f6f9ff" align="right">
    <? //様式９平成２６年度改訂対応 20140324
    //地域包括ケア病棟
    if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
        $wk_title = "看護補助者配置加算の届出区分";
    }
    else {
        $wk_title = "看護補助加算の届出区分";
    }
    echo($wk_title);
    ?>
            </td>
			<td colspan="3">
			<label><input type="radio" name="nursing_assistance_flg" value="0"<? if ($nursing_assistance_flg != "1") {echo " checked ";} ?>>
			無</label>&nbsp;&nbsp;
			<label><input type="radio" name="nursing_assistance_flg" value="1"<? if ($nursing_assistance_flg == "1") {echo " checked ";} ?>>
			有</label>&nbsp;
    <? //様式９平成２６年度改訂対応 20140324
    //地域包括ケア病棟
    if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
        echo("（25対1）");
    }
    else {
    ?>
			（
			<select id="nursing_assistance_cnt" name="nursing_assistance_cnt">
			<option value=""></option>
			<option value="30" <? if ($nursing_assistance_cnt == "30") {echo(" selected");}?>>30</option>
			<option value="50" <? if ($nursing_assistance_cnt == "50") {echo(" selected");}?>>50</option>
			<option value="75" <? if ($nursing_assistance_cnt == "75") {echo(" selected");}?>>75</option>
			</select>
			対1）
            
            <? } ?>
			</td>
		</tr>
<? } // 届出区分による表示切替 20121011 end ?>
		<!-- 夜勤時間帯 -->
		<tr height="22">
			<td bgcolor="#f6f9ff" align="right">夜勤時間帯</td>
			<td colspan="3">
		<?
		    // 開始時間〜終了時間
			$obj->show_hour_options_0_23("prov_start_hh", $prov_start_hh);
			$obj->show_min_options_00_59("prov_start_mm", $prov_start_mm);
			echo("〜");
			$obj->show_hour_options_0_23("prov_end_hh", $prov_end_hh);
			$obj->show_min_options_00_59("prov_end_mm", $prov_end_mm);
		?>
			</td>
		</tr>
		<!-- 月平均夜勤時間上限 -->
		<tr height="22">
			<td bgcolor="#f6f9ff" align="right">月平均夜勤時間上限</td>
			<td colspan="3">
			<input type="text" name="limit_time" value="<? echo(h($limit_time)); ?>" size="3" maxlength="3" style="ime-mode:disabled ;">
			</td>
		</tr>
		<!-- 交代制 -->
		<tr height="22">
			<td bgcolor="#f6f9ff" align="right">交代制</td>
			<td colspan="3">
			<label><input type="radio" name="shift_flg" value="2"<? if ($shift_flg == "2") {echo " checked ";} ?> onClick="shiftChg()">
			２交代</label>&nbsp;&nbsp;
			<label><input type="radio" name="shift_flg" value="3"<? if ($shift_flg == "3") {echo " checked ";} ?> onClick="shiftChg()">
			３交代</label>
			</td>
		</tr>
		<!-- 申し送り時間（１〜３） -->
		<? for ($i=0; $i<$shift_flg; $i++) { ?>
			<tr height="22">
				<? $wk = "申し送り時間" . ($i + 1) ?>
				<td bgcolor="#f6f9ff" align="right"><? echo($wk); ?></td>
				<td colspan="3">
			<?
			    // 開始時間〜終了時間
				$obj->show_hour_options_0_23("send_start_hh[]", $send_start_hh[$i]);
				$obj->show_min_options_00_59("send_start_mm[]", $send_start_mm[$i]);
				echo("〜");
				$obj->show_hour_options_0_23("send_end_hh[]", $send_end_hh[$i]);
				$obj->show_min_options_00_59("send_end_mm[]", $send_end_mm[$i]);
			?>
				</td>
			</tr>
		<? } ?>
		<!-- 常勤職員の所定労働時間 -->
		<tr height="22">
			<td bgcolor="#f6f9ff" align="right" nowrap>常勤職員の週所定労働時間</td>
			<td colspan="3">
			<input type="text" name="labor_cnt" value="<? echo(h($labor_cnt)); ?>" size="5" maxlength="5" style="ime-mode:disabled ;">時間
			&nbsp;<span style="color: red;">（「月の稼動日数×週所定労働時間÷７」を月間所定労働時間とします）</span>
			</td>
		</tr>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 更新ボタン -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="950" border="0" cellspacing="0" cellpadding="2">
		<tr height="22">
		<td align="right"><input type="button" value="更新" onclick="insertData()"></td>
		</tr>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="standard_id" value="<? echo($standard_id); ?>">
		<input type="hidden" name="reload_flg" value="<? echo($reload_flg); ?>">
		<input type="hidden" name="shift_cnt" value="<? echo($shift_flg); ?>">
	</form>

</td>
</tr>
</table>
</body>

<? pg_close($con); ?>

</html>

