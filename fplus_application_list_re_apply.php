<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="fplus_apply_list.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="category" value="<?echo($category)?>">
<input type="hidden" name="workflow" value="<?echo($workflow)?>">
<input type="hidden" name="apply_title" value="<?echo($apply_title)?>">
<input type="hidden" name="approve_emp_nm" value="<?echo($approve_emp_nm)?>">
<input type="hidden" name="date_y1" value="<?echo($date_y1)?>">
<input type="hidden" name="date_m1" value="<?echo($date_m1)?>">
<input type="hidden" name="date_d1" value="<?echo($date_d1)?>">
<input type="hidden" name="date_y2" value="<?echo($date_y2)?>">
<input type="hidden" name="date_m2" value="<?echo($date_m2)?>">
<input type="hidden" name="date_d2" value="<?echo($date_d2)?>">
<input type="hidden" name="apply_stat" value="<?echo($apply_stat)?>">
</form>


<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("fplus_common_class.php");


$fname=$PHP_SELF;


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 78, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}


// データベースに接続
$con = connect2db($fname);

$obj = new fplus_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

$arr_next_appry_id = array();
$must_send_mail_applys = array();
for ($i = 0, $count = count($re_apply_chk); $i < $count; $i++) {
	if ($re_apply_chk[$i] != "") {

		$apply_id = $re_apply_chk[$i];

		// 再申請チェック
		$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
		$apply_stat = $arr_apply_wkfwmst[0]["apply_stat"];
		if($apply_stat != "3") {
			echo("<script type=\"text/javascript\">alert(\"報告状況が変更されたため、再報告できません。\");</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}

		// 新規申請(apply_id)採番
		$sql = "select max(apply_id) from fplusapply";
		$cond = "";
		$apply_max_sel = select_from_table($con,$sql,$cond,$fname);
		$max = pg_result($apply_max_sel,0,"max");
		if($max == ""){
			$new_apply_id = "1";
		}else{
			$new_apply_id = $max + 1;
		}


/////////////エピネット、感染症報告用再申請の際のデータ(apply_idのみ)更新処理ここから

		$sql = "select 1 as id from  fplus_epi_a where apply_id = $apply_id
				union 
				select 2 as id from  fplus_epi_b where apply_id = $apply_id
				union
				select 3 as id from fplus_infection_mrsa where apply_id = $apply_id";
		

		// apply_idの場所検索
//		$sql = "select count(apply_id) as cnt from fplus_epi_a where apply_id = $apply_id";
		$cond = "";
		$sel = select_from_table($con,$sql,$cond,$fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		$epi_id = pg_fetch_result($sel, 0, "id");
		
		if($epi_id == "1")
		{
			//エピネット針刺し
			
			$set_data = array();
			$set_key = array();
			$set_data[0] = $new_apply_id;
			$set_key[0] = "apply_id";
			
			//Ａ報告書（針刺し・切創）テーブルにデータが存在しているのでfplus_epi_aに登録
			//集計用データの登録処理
			$obj->update_Episys107A($apply_id,$set_key,$set_data);
		}
		else if($epi_id == "2")
		{
			//エピネット感染
			
			$set_data = array();
			$set_key = array();
			$set_data[0] = $new_apply_id;
			$set_key[0] = "apply_id";
				
			//Ｂ報告書（感染）テーブルにデータが存在しているのでfplus_epi_bに登録
			//集計用データの登録処理
			$obj->update_Episys107B($apply_id,$set_key,$set_data);
		}
		else if($epi_id == "3")
		{
			//感染症報告書
		
			$set_data = array();
			$set_key = array();
			$set_data[0] = $new_apply_id;
			$set_key[0] = "apply_id";
					
			//感染症報告書テーブルにデータが存在しているのでfplus_infection_mrsaに登録
			//集計用データの登録処理(MRSA等耐性菌検出報告書)
			$obj->update_MRSA($apply_id,$set_key,$set_data);
		}
		
		// k-fujii 医療事故報告用ここから
		$sql = "select 1 as id from  fplus_immediate_report where apply_id = $apply_id
				union
				select 2 as id from  fplus_first_report where apply_id = $apply_id
				union
				select 3 as id from fplus_second_report where apply_id = $apply_id
				union
				select 4 as id from fplus_consultation_request where apply_id = $apply_id
				union
				select 5 as id from fplus_consultation_report where apply_id = $apply_id
				union
				select 6 as id from fplus_medical_apparatus_fault_report where apply_id = $apply_id
				union
				select 7 as id from fplus_number_of_occurrences where apply_id = $apply_id";
		

		// apply_idの場所検索
//		$sql = "select count(apply_id) as cnt from fplus_epi_a where apply_id = $apply_id";
		$cond = "";
		$sel = select_from_table($con,$sql,$cond,$fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		$epi_id = pg_fetch_result($sel, 0, "id");

		// 新規報告�盧糧�
		$date = date("YmdHi");

		$year = substr($date, 0, 4);
		$md   = substr($date, 4, 4);
		if($md >= "0101" and $md <= "0331")
		{
			$year = $year - 1;
		}
		$max_cnt = $obj->get_apply_cnt_per_year($year);
		$apply_no = $max_cnt + 1;

		$sql = " SELECT a.short_wkfw_name ".
			" FROM ".
			" 	fpluswkfwmst a ".
			" 	,fplusapply app ".
			" WHERE ".
			" 	a.wkfw_id = app.wkfw_id ".
			" 	AND app.apply_id = '$apply_id' ";
		$cond = "";
		$report_no_sel = select_from_table($con,$sql,$cond,$fname);
		$short_wkfw_name = pg_result($report_no_sel,0,"short_wkfw_name");
		$report_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

		if($epi_id == "1")
		{
			//緊急報告受付
			
			$set_data = array();
			$set_key = array();
			$set_data[0] = $new_apply_id;
			$set_key[0] = "apply_id";
			$set_data[1] = $report_no;
			$set_key[1] = "report_no";
			$set_data[2] = date("Y-m-d H:i:s");
			$set_key[2] = "update_time";
			
			//緊急報告受付テーブルにデータが存在しているのでfplus_epi_immediate_repoに登録
			//集計用データの登録処理
			$obj->update_immediate_repo($apply_id,$set_key,$set_data);
		}
		else if($epi_id == "2")
		{
			//第一報
			
			$set_data = array();
			$set_key = array();
			$set_data[0] = $new_apply_id;
			$set_key[0] = "apply_id";
			$set_data[1] = $report_no;
			$set_key[1] = "report_no";
			$set_data[2] = date("Y-m-d H:i:s");
			$set_key[2] = "update_time";
				
			//第一報テーブルにデータが存在しているのでfplus_epi_first_repoに登録
			//集計用データの登録処理
			$obj->update_first_repo($apply_id,$set_key,$set_data);
		}
		else if($epi_id == "3")
		{
			//第二報

			$set_data = array();
			$set_key = array();
			$set_data[0] = $new_apply_id;
			$set_key[0] = "apply_id";
			$set_data[1] = $report_no;
			$set_key[1] = "report_no";
			$set_data[2] = date("Y-m-d H:i:s");
			$set_key[2] = "update_time";
					
			//第二報テーブルにデータが存在しているのでfplus_epi_second_repoに登録
			//集計用データの登録処理
			$obj->update_second_repo($apply_id,$set_key,$set_data);
		}
		else if($epi_id == "4")
		{
			//コンサルテーション依頼書

			$set_data = array();
			$set_key = array();
			$set_data[0] = $new_apply_id;
			$set_key[0] = "apply_id";
			$set_data[1] = $report_no;
			$set_key[1] = "report_no";
			$set_data[2] = date("Y-m-d H:i:s");
			$set_key[2] = "update_time";
					
					
			//コンサルテーション依頼書テーブルにデータが存在しているのでfplus_epi_cons_reqに登録
			//集計用データの登録処理
			$obj->update_cons_req($apply_id,$set_key,$set_data);
		}
		else if($epi_id == "5")
		{
			//コンサルテーション報告書

			$set_data = array();
			$set_key = array();
			$set_data[0] = $new_apply_id;
			$set_key[0] = "apply_id";
			$set_data[1] = $report_no;
			$set_key[1] = "report_no";
			$set_data[2] = date("Y-m-d H:i:s");
			$set_key[2] = "update_time";

			//コンサルテーション報告書テーブルにデータが存在しているのでfplus_epi_cons_reportに登録
			//集計用データの登録処理
			$obj->update_cons_report($apply_id,$set_key,$set_data);
		}
		else if($epi_id == "6")
		{
			//医療機器不具合報告

			$set_data = array();
			$set_key = array();
			$set_data[0] = $new_apply_id;
			$set_key[0] = "apply_id";
			$set_data[1] = $report_no;
			$set_key[1] = "report_no";
			$set_data[2] = date("Y-m-d H:i:s");
			$set_key[2] = "update_time";

			//医療機器不具合報告テーブルにデータが存在しているのでfplus_medical_apparatus_fault_reportに登録
			//データの登録処理
			$obj->update_medical_apparatus_fault_report($apply_id,$set_key,$set_data);
		}
		else if($epi_id == "6")
		{
			//レベル別発生件数データ
			$set_data = array();
			$set_key = array();
			$set_data[0] = $new_apply_id;
			$set_key[0] = "apply_id";
			$set_data[1] = $report_no;
			$set_key[1] = "report_no";
			$set_data[2] = date("Y-m-d H:i:s");
			$set_key[2] = "update_time";

			//発生件数データテーブルにデータが存在しているのでfplus_number_of_occurrencesに登録
			//データの登録処理
			$obj->update_number_of_occurrences_level($apply_id,$set_key,$set_data);

			//職種別発生件数データ
			$set_data = array();
			$set_key = array();
			$set_data[0] = $new_apply_id;
			$set_key[0] = "apply_id";
			$set_data[1] = $report_no;
			$set_key[1] = "report_no";
			$set_data[2] = date("Y-m-d H:i:s");
			$set_key[2] = "update_time";

			//発生件数データテーブルにデータが存在しているのでfplus_number_of_occurrencesに登録
			//データの登録処理
			$obj->update_number_of_occurrences($apply_id,$set_key,$set_data);

			//転落・転倒発生件数データ
			$set_data = array();
			$set_key = array();
			$set_data[0] = $new_apply_id;
			$set_key[0] = "apply_id";
			$set_data[1] = $report_no;
			$set_key[1] = "report_no";
			$set_data[2] = date("Y-m-d H:i:s");
			$set_key[2] = "update_time";

			//転落・転倒発生件数データテーブルにデータが存在しているのでfplus_number_of_occurrences_fall_accidentに登録
			//データの登録処理
			$obj->update_number_of_occurrences_fall_accident($apply_id,$set_key,$set_data);

		}
		// k-fujii 医療事故報告用ここまで

/////////////エピネット、感染症報告用再申請の際のデータ(apply_idのみ)更新処理ここまで
		


		// 申請登録
		$obj->regist_re_apply($new_apply_id, $apply_id, "", "", "LIST");

		// 承認登録
		$obj->regist_re_applyapv($new_apply_id, $apply_id);

		// 承認者候補登録
		$obj->regist_re_applyapvemp($new_apply_id, $apply_id);

		// 添付ファイル登録
		$obj->regist_re_applyfile($new_apply_id, $apply_id);

		// 非同期・同期受信登録
		$obj->regist_re_applyasyncrecv($new_apply_id, $apply_id);

		// 申請結果通知登録
		$obj->regist_re_applynotice($new_apply_id, $apply_id);

		// 前提とする申請書(申請用)登録
		$obj->regist_re_applyprecond($new_apply_id, $apply_id);

		// 元の申請書に再申請ＩＤを更新
		$obj->update_re_apply_id($apply_id, $new_apply_id);

		array_push($arr_next_appry_id, $new_apply_id);

		// メール送信不要であれば次の申請書へ
		$wkfw_id = fplus_get_wkfw_id_by_apply_id($con, $new_apply_id, $fname);
		$wkfw_send_mail_flg = fplus_get_wkfw_send_mail_flg($con, $wkfw_id, $fname);
		if ($wkfw_send_mail_flg != "t") {
			continue;
		}

		// メール送信に必要な情報を配列に格納
		$sql = "select e.emp_email2, a.apv_order from fplusapplyapv a inner join empmst e on e.emp_id = a.emp_id";
		$cond = "where a.apply_id = $new_apply_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$to_addresses = array();
		while ($row = pg_fetch_array($sel)) {
			if (fplus_must_send_mail($wkfw_send_mail_flg, $row["emp_email2"], $arr_apply_wkfwmst[0]["wkfw_appr"], $row["apv_order"])) {
				$to_addresses[] = $row["emp_email2"];
			}
		}

		// 送信先がなければ次の申請書へ
		if (count($to_addresses) == 0) {
			continue;
		}

		// メール送信に必要な情報を配列に格納
		$must_send_mail_applys[] = array(
			"to_addresses" => $to_addresses,
			"wkfw_content_type" => $arr_apply_wkfwmst[0]["wkfw_content_type"],
			"content" => $arr_apply_wkfwmst[0]["apply_content"],
			"apply_title" => $arr_apply_wkfwmst[0]["apply_title"],
			"approve_label" => ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認"
		);
	}
}



// 添付ファイルをコピー
// 添付ファイルの確認
if (!is_dir("fplus")) {
	mkdir("fplus", 0755);
}
if (!is_dir("fplus/apply")) {
	mkdir("fplus/apply", 0755);
}
for ($i = 0; $i < $count; $i++) {
	if ($re_apply_chk[$i] != "") {

		$apply_id = $re_apply_chk[$i];
		$sql = "select * from fplusapplyfile";
		$cond = "where apply_id = $apply_id order by apply_id asc";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if($sel==0){
			pg_exec($con,"rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {

			$fine_no = $row["applyfile_no"];
			$file_name = $row["applyfile_name"];
			$ext = strrchr($file_name, ".");

			$tmp_filename = "fplus/apply/{$apply_id}_{$fine_no}{$ext}";
			copy($tmp_filename, "fplus/apply/{$arr_next_appry_id[$i]}_{$fine_no}{$ext}");
		}
	}
}

// メール送信準備
if (count($must_send_mail_applys) > 0) {
	$emp_id = get_emp_id($con, $session, $fname);
	$emp_detail = $obj->get_empmst_detail($emp_id);
	$emp_nm = fplus_format_emp_nm($emp_detail[0]);
	$emp_mail = fplus_format_emp_mail($emp_detail[0]);
	$emp_pos = fplus_format_emp_pos($emp_detail[0]);
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// メール送信
if (count($must_send_mail_applys) > 0) {
	mb_internal_encoding("EUC-JP");
	mb_language("Japanese");

	foreach ($must_send_mail_applys as $data) {
		$mail_subject = "[CoMedix] 受付依頼のお知らせ";
		$mail_content = fplus_format_mail_content($data["wkfw_content_type"], $data["content"]);
		$mail_separator = str_repeat("-", 60) . "\n";
		$additional_headers = "From: {$emp_mail}";
		$additional_parameter = "-f{$emp_mail}";

		foreach ($data["to_addresses"] as $to_address) {
			$mail_body = "以下の受付依頼がありました。\n\n";
			$mail_body .= "報告者：{$emp_nm}\n";
			$mail_body .= "所属：{$emp_pos}\n";
			$mail_body .= "表題：{$data["apply_title"]}\n";
			if ($mail_content != "") {
				$mail_body .= $mail_separator;
				$mail_body .= "{$mail_content}\n";
			}

			mb_send_mail($to_address, $mail_subject, $mail_body, $additional_headers, $additional_parameter);
		}
	}
}

// 一覧画面に遷移
echo("<script type=\"text/javascript\">document.items.submit();</script>");
?>
</body>
