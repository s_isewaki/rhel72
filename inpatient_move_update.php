<?
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
$new_date = "$year$month$day";
$new_time = "$hour$min";
if (!checkdate($month, $day, $year)) {
	echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($new_date == $date && $new_time == $time) {
	echo("<script type=\"text/javascript\">alert('日時が同じです。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

$con = connect2db($fname);
pg_query($con, "begin");

// 日時が直前の転床以前になる場合はエラー
$sql = "select move_dt, move_tm from inptmove";
$cond = "where ptif_id = '$pt_id' and move_del_flg = 'f' and (move_dt < '$date' or (move_dt = '$date' and move_tm < '$time')) order by move_dt desc, move_tm desc limit 1";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$pre_date = pg_fetch_result($sel, 0, "move_dt");
	$pre_time = pg_fetch_result($sel, 0, "move_tm");
	if ($new_date < $pre_date || ($new_date == $pre_date && $new_time <= $pre_time)) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定の日時が直前の転床以前のため更新できません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}

// 直後の転床と日時が逆転する場合はエラー
$sql = "select move_dt, move_tm from inptmove";
$cond = "where ptif_id = '$pt_id' and move_del_flg = 'f' and (move_dt > '$date' or (move_dt = '$date' and move_tm > '$time')) order by move_dt, move_tm limit 1";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$post_date = pg_fetch_result($sel, 0, "move_dt");
	$post_time = pg_fetch_result($sel, 0, "move_tm");
	if ($new_date > $post_date || ($new_date == $post_date && $new_time >= $post_time)) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定の日時が直後の転床以降のため更新できません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}

// 入院期間外であればエラー
$terms = array();
$sql = "select inpt_in_dt, inpt_in_tm from inptmst";
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$terms[] = array("start" => pg_fetch_result($sel, 0, "inpt_in_dt") . pg_fetch_result($sel, 0, "inpt_in_tm"), "end" => "999999999999");
}
$sql = "select inpt_in_dt, inpt_in_tm, inpt_out_dt, inpt_out_tm from inpthist";
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$terms[] = array("start" => $row["inpt_in_dt"] . $row["inpt_in_tm"], "end" => $row["inpt_out_dt"] . $row["inpt_out_tm"]);
}
$term_error = true;
foreach ($terms as $term) {
	if ($term["start"] < "$date$time" && "$date$time" < $term["end"]) {
		$term_error = false;
		break;
	}
}
if ($term_error) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('指定の日時が入院期間外のため更新できません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 転床日時を更新
$sql = "update inptmove set";
$set = array("move_dt", "move_tm");
$setvalue = array($new_date, $new_time);
$cond = "where ptif_id = '$pt_id' and move_dt = '$date' and move_tm = '$time' and move_del_flg = 'f'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

pg_query($con, "commit");
pg_close($con);

echo("<script type=\"text/javascript\">if (opener) opener.location.reload();</script>");
echo("<script type=\"text/javascript\">self.close();</script>");
