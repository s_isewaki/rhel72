<?
// セッションの開始
session_start();

ob_start();
require_once("about_comedix.php");
require_once("yui_calendar_util.ini");
require_once("fplus_common.ini");
require_once("fplus_common_class.php");
require_once("fplus_yui_calendar_text.ini");
require_once("fplus_headquarters_total_list_sub.php");
require_once("fplus_headquarters_total_list_make_table.php");
ob_end_clean();

$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 79, $fname);
$con = @connect2db($fname);

$obj = new fplus_common_class($con, $fname);

if(!$session || !$fplusadm_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}

//一覧用職種取得
$job = get_inci_job_mst();

//レベル取得
$level_mst = get_inci_level_mst();

// listタイプ "a" 〜 "h" が入る
$list_type = (@$_REQUEST["list_type"] == "" ? "a" : @$_REQUEST["list_type"]);

//今回使わないレベルの削除
for($i=0;$i<count($level_mst);$i++){
	if($list_type == "b"){
		if("{$level_mst[$i]['easy_code']}" == 'L3b' or "{$level_mst[$i]['easy_code']}" != 'LV4' or "{$level_mst[$i]['easy_code']}" == 'LV5'){
			$level_ary[] = array("code"=> "{$level_mst[$i]['easy_code']}","name"=>"{$level_mst[$i]['easy_name']}","short_name" => "{$level_mst[$i]['short_name']}");
		}
	}else{
		if("{$level_mst[$i]['easy_code']}" != 'LVh' && "{$level_mst[$i]['easy_code']}" != 'LV3'&& "{$level_mst[$i]['easy_code']}" != 'L4a' && "{$level_mst[$i]['easy_code']}" != 'L4b' && "{$level_mst[$i]['easy_code']}" != 'LVo'){
			$level_ary[] = array("code"=> "{$level_mst[$i]['easy_code']}","name"=>"{$level_mst[$i]['easy_name']}","short_name" => "{$level_mst[$i]['short_name']}");
		}
	}
}

if(date("m")>="04"){
	$date_this_y = (@$_REQUEST["target_year"]=="" ? date("Y") : @$_REQUEST["target_year"]);
	$date_now_y = date("Y");
}else{
	$date_this_y = (@$_REQUEST["target_year"]=="" ? date("Y", strtotime(date("Y/m/d")." -1 year")) : @$_REQUEST["target_year"]);
	$date_now_y = date("Y", strtotime(date("Y/m/d")." -1 year"));
}

$date_next_y = (string)((int)$date_this_y + 1);
$date_last_y = (string)((int)$date_this_y - 1);
$date_2y_befor = (string)((int)$date_last_y - 1);
$date_3y_befor = (string)((int)$date_2y_befor - 1);
$date_4y_befor = (string)((int)$date_3y_befor - 1);

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
$list_title_a = "ヒヤリハット含む事故発生件数";
$list_title_b = "転倒・転落による事故発生件数";
$list_title_c = "ヒヤリハット件数（職種別）";
$list_title_d = "病床規模別発生件数";
$list_title_e = "入院施設基準別発生件数";
$list_title_f = "事故報告件数（要件別）";
$list_title_g = "概要別事故報告件数";
$list_title_h = "年度別事故報告件数";

//------------------
// 横列フィールドの定義
//------------------
$horz_field_definision =array(
	"a"=>array(
		array("dbkey"=> "hospital_name",		"text" => "病院名",			"colwidth" => "150",	"data_align" => "left",		"cellwidth" => "19.50"),
		array("dbkey"=> "occurrences_year",		"text" => "年度",			"colwidth" => "60",	"data_align" => "center",	"cellwidth" => "6.13"),
		array("dbkey"=> "hospital_bed",			"text" => "病床数（実稼働）",		"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "15.50"),
		array("dbkey"=> "report_per_hospital_bed",	"text" => "１ベッド当たりの提出数",	"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "21.25"),
		array("dbkey"=> "total_report_count",		"text" => "総件数",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level0",			"text" => "レベル０",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level1",			"text" => "レベル１",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level2",			"text" => "レベル２",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level3a",			"text" => "レベル３a",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level3b",			"text" => "レベル３b",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level4",			"text" => "レベル４",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level5",			"text" => "レベル５",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75")
	),
	"b"=>array(
		array("dbkey"=> "hospital_name",		"text" => "病院名",			"colwidth" => "150",	"data_align" => "left",		"cellwidth" => "19.50"),
		array("dbkey"=> "occurrences_year",		"text" => "年度",			"colwidth" => "60",	"data_align" => "center",	"cellwidth" => "6.13"),
		array("dbkey"=> "level3b",			"text" => "レベル３b",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level4",			"text" => "レベル４",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level5",			"text" => "レベル５",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75")
	),
	"c"=>array(
		array("dbkey"=> "hospital_name",		"text" => "病院名",			"colwidth" => "150",	"data_align" => "left",		"cellwidth" => "19.50"),
		array("dbkey"=> "occurrences_year",		"text" => "年度",			"colwidth" => "60",	"data_align" => "center",	"cellwidth" => "6.13"),
		array("dbkey"=> "hospital_bed",			"text" => "病床数（実稼働）",		"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "15.50"),
		array("dbkey"=> "report_per_hospital_bed",	"text" => "１ベッド当たりの提出数",	"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "21.25"),
		array("dbkey"=> "total_report_count",		"text" => "総件数",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75")
	),
	"d"=>array(
		array("dbkey"=> "bed_kbn",			"text" => "病床数",			"colwidth" => "150",	"data_align" => "left",		"cellwidth" => "19.50"),
		array("dbkey"=> "occurrences_year",		"text" => "年度",			"colwidth" => "60",	"data_align" => "center",	"cellwidth" => "6.13"),
		array("dbkey"=> "total_report_count",		"text" => "総件数",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level0sum",			"text" => "レベル０",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level1sum",			"text" => "レベル１",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level2sum",			"text" => "レベル２",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level3asum",			"text" => "レベル３a",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level3bsum",			"text" => "レベル３b",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level4sum",			"text" => "レベル４",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level5sum",			"text" => "レベル５",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75")
	),
	"e"=>array(
		array("dbkey"=> "nursing_standard",		"text" => "入院看護基準",		"colwidth" => "150",	"data_align" => "left",		"cellwidth" => "19.50"),
		array("dbkey"=> "occurrences_year",		"text" => "年度",			"colwidth" => "60",	"data_align" => "center",	"cellwidth" => "6.13"),
		array("dbkey"=> "total_report_count",		"text" => "総件数",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level0sum",			"text" => "レベル０",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level1sum",			"text" => "レベル１",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level2sum",			"text" => "レベル２",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level3asum",			"text" => "レベル３a",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level3bsum",			"text" => "レベル３b",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level4sum",			"text" => "レベル４",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "level5sum",			"text" => "レベル５",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75")
	),
	"f"=>array(
		array("dbkey"=> "hospital_name",		"text" => "病院名",			"colwidth" => "150",	"data_align" => "left",		"cellwidth" => "19.50"),
		array("dbkey"=> "occurrences_year",		"text" => "年度",			"colwidth" => "60",	"data_align" => "center",	"cellwidth" => "6.13"),
		array("dbkey"=> "hospital_bed",			"text" => "病床数（実稼働）",		"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "15.50"),
		array("dbkey"=> "sum_over_3b",			"text" => "事故発生総件数（3b以上）",	"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "13.63"),
		array("dbkey"=> "total_report_count",		"text" => "事故報告総件数",		"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "requirements1sum",		"text" => "要件��",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "requirements2sum",		"text" => "要件��",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "requirements3sum",		"text" => "要件��",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "outline1sum",			"text" => "薬剤",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "outline2sum",			"text" => "輸血",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "outline3sum",			"text" => "治療・処置",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "9.88"),
		array("dbkey"=> "outline4sum",			"text" => "医療機器等",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "9.88"),
		array("dbkey"=> "outline5sum",			"text" => "ドレーンチューブ",		"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "outline6sum",			"text" => "検査",			"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75"),
		array("dbkey"=> "outline7sum",			"text" => "療養上の世話",		"colwidth" => "60",	"data_align" => "right",	"cellwidth" => "8.75")
	),
	"g"=>array(
		array("dbkey"=> "outline",		"text" => "",			"colwidth" => "36",		"data_align" => "center"),
		array("dbkey"=> "require1",		"text" => "要件��",		"colwidth" => "16",		"data_align" => "right"),
		array("dbkey"=> "require2",		"text" => "要件��",		"colwidth" => "16",		"data_align" => "right"),
		array("dbkey"=> "require3",		"text" => "要件��",		"colwidth" => "16",		"data_align" => "right"),
		array("dbkey"=> "total",		"text" => "合計",		"colwidth" => "16",		"data_align" => "right")
	),
	"h"=>array(
		array("dbkey"=> "report_hospital_count",	"text" => "",				"colwidth" => "100",		"data_align" => "center"),
		array("dbkey"=> "$date_4y_befor",		"text" => $date_4y_befor."年度",	"colwidth" => "80",		"data_align" => "right"),
		array("dbkey"=> "$date_3y_befor",		"text" => $date_3y_befor."年度",	"colwidth" => "80",		"data_align" => "right"),
		array("dbkey"=> "$date_2y_befor",		"text" => $date_2y_befor."年度",	"colwidth" => "80",		"data_align" => "right"),
		array("dbkey"=> "$date_last_y",			"text" => $date_last_y."年度",		"colwidth" => "80",		"data_align" => "right"),
		array("dbkey"=> "$date_this_y",			"text" => $date_this_y."年度",		"colwidth" => "80",		"data_align" => "right")
	)
);
if($list_type == "c"){
	//カラム追加
	$col_cnt = 0;
	for($i=0;$i<count($job);$i++){
		$col_cnt = $i + 5;
		$horz_field_definision["c"][$col_cnt] = array("dbkey"=> "jobcategory{$job[$i]['def_code']}sum", "text" => "{$job[$i]['def_name']}", "colwidth" => "60", "data_align" => "right", "cellwidth" => "8.75");
	}
	$col_cnt += 1;
	$horz_field_definision["c"][$col_cnt] = array("dbkey"=> "total_report_count_level0", "text" => "総数", "colwidth" => "60", "data_align" => "right", "cellwidth" => "8.75");
	$col_cnt += 1;
	for($i=0;$i<count($job);$i++){
		$horz_field_definision["c"][$col_cnt] = array("dbkey"=> "level_0_jobcategory{$job[$i]['def_code']}", "text" => "{$job[$i]['def_name']}", "colwidth" => "60", "data_align" => "right", "cellwidth" => "8.75");
		$col_cnt += 1;
	}
}
//====================================================================
// パラメータ取得
//====================================================================
if($list_type == "a"){
	$list_title = $list_title_a;
	$xlstitle = $list_title_a."（レベル別）";
}else if($list_type=="b"){
	$list_title = $list_title_b;
	$xlstitle = "転倒・転落による事故（レベル3b以上）発生件数";
}else if($list_type=="c"){
	$list_title = $list_title_c;
	$xlstitle = "ヒヤリハット件数（職種別）";
}else if($list_type=="d"){
	$list_title = $list_title_d;
	$xlstitle = "病床規模別発生件数";
}else if($list_type=="e"){
	$list_title = $list_title_e;
	$xlstitle = "入院看護基準別発生件数";
}else if($list_type=="f"){
	$list_title = $list_title_f;
	$xlstitle = "事故報告件数（要件別）";
}else if($list_type=="g"){
	$list_title = $list_title_g;
	$xlstitle = "事故報告件数（概要・要件別）";
}else{
	$list_title = $list_title_h;
	$xlstitle = "事故報告件数（年度別）";
}

//Excelボタン押下時に"1"が入る
$is_print_mode = @$_REQUEST["print_mode"];


//====================================================================
// DB関係
//====================================================================
if($list_type == "a"){
	//事故件数検索
	$result = getAccidentCount4Level($con,$fname,$level_ary,$date_this_y,$date_last_y);

	if($result[0]["hospital_id"]!=""){
		//病床数取得
		$hospital_bed_list = getHospitalbedList($con,$fname,$result,$date_this_y,$date_last_y);
		//データマトリクスを作成する
		$data_list = getDatalist_a($result,$horz_field_definision[$list_type],$hospital_bed_list,$date_this_y,$date_last_y);
		$data_count = count($data_list);
	}else{
		$data_count = 0;
	}
}elseif($list_type == "b"){
	//事故件数検索
	$result = getFallAccidentCount($con,$fname,$date_this_y,$date_last_y);
	if($result[0]["hospital_id"]!=""){
		//データマトリクスを作成する
		$data_list = getDatalist_b($result,$horz_field_definision[$list_type],$date_this_y,$date_last_y);
		$data_count = count($data_list);
	}else{
		$data_count = 0;
	}
}elseif($list_type == "c"){
	//事故件数検索
	$result = getAccidentCount4Job($con,$fname,$level_ary,$job,$date_this_y,$date_last_y);


	if($result[0]["hospital_id"]!=""){
		//病床数取得
		$hospital_bed_list = getHospitalbedList($con,$fname,$result,$date_this_y,$date_last_y);
		//１ベッド当たりの提出数取得
		$report_per_hospital_bed = getReportPerHospitalBed4Job($result);
		//データマトリクスを作成する
		$data_list = getDatalist_c($result,$horz_field_definision[$list_type],$report_per_hospital_bed,$hospital_bed_list,$date_this_y,$date_last_y);
		$data_count = count($data_list);
	}else{
		$data_count = 0;
	}

}elseif($list_type == "d"){
	//事故件数検索
	$result = getAccidentCount4bed($con,$fname,$level_ary,$date_this_y,$date_last_y);


	if($result[0]["bed_kbn"]!=""){
		//データマトリクスを作成する
		$data_list = getDatalist_d($result,$horz_field_definision[$list_type],$date_this_y,$date_last_y);
		$data_count = count($data_list);
	}else{
		$data_count = 0;
	}
}elseif($list_type == "e"){
	//事故件数検索
	$result = getAccidentCount4nursing($con,$fname,$level_ary,$date_this_y,$date_last_y);

	if($result[0]["nursing_standard_id"]!=""){
		//データマトリクスを作成する
		$data_list = getDatalist_e($result,$horz_field_definision[$list_type],$date_this_y,$date_last_y);
		$data_count = count($data_list);
	}else{
		$data_count = 0;
	}
}elseif($list_type == "f"){
	//事故件数検索
	$result = getAccidentCount4requirement($con,$fname,$date_this_y,$date_last_y);

	if($result[0]["hospital_id"]!=""){
		//病床数取得
		$hospital_bed_list = getHospitalbedList($con,$fname,$result,$date_this_y,$date_last_y);
		//データマトリクスを作成する
		$data_list = getDatalist_f($result,$horz_field_definision[$list_type],$hospital_bed_list,$date_this_y,$date_last_y);
		$data_count = count($data_list);
	}else{
		$data_count = 0;
	}
}else if ($list_type == "g") {
	$vert_fields_definision = array();
	$vert_fields_definision = array(
		"0" => array("code" => "1", "name" => "薬剤")
		, "1" => array("code" => "2", "name" => "輸血")
		, "2" => array("code" => "3", "name" => "治療・処置")
		, "3" => array("code" => "4", "name" => "医療機器等")
		, "4" => array("code" => "5", "name" => "ドレーン・チューブ")
		, "5" => array("code" => "6", "name" => "検査")
		, "6" => array("code" => "7", "name" => "療養上の世話")
		, "7" => array("code" => "8", "name" => "合計")
	);
	
	$result = getTotalReportCountOutline($con, $fname, $date_this_y);
	$data_list = getDatalist_g($result, $vert_fields_definision, $horz_field_definision[$list_type]);
	
	if ($result == false) {
		$data_count = 0;
	} else {
		$data_count = 1;
	}
	
	$_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["g"]["data_list"] = $data_list;
	$_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["g"]["horz_list"] = $horz_field_definision[$list_type];
	$_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["g"]["data_count"] = $data_count;
}else if ($list_type == "h") {
	$vert_fields_definision = array();
	$vert_fields_definision = array(
		"0" => array("code" => "1", "name" => "報告病院数")
	);
	
	$result_HospitalCount = getReportHospitalCount($con,$fname,$date_next_y,$date_this_y,$date_last_y,$date_2y_befor,$date_3y_befor,$date_4y_befor);
	$data_list_HospitalCount = getDatalist_h($result_HospitalCount);
	
	if ($data_list_HospitalCount == false) {
		$data_count1 = 0;
	} else {
		$data_count1 = 1;
	}
	
	$_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["h"]["data_list_HospitalCount"] = $data_list_HospitalCount;
	$_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["h"]["horz_list_HospitalCount"] = $horz_field_definision[$list_type];
	$_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["h"]["data_count_HospitalCount"] = $data_count1;

	$vert_fields_definision = array();
	$vert_fields_definision = array(
		"0" => array("code" => "occurrences_year", "name" => "")
		, "1" => array("code" => "total_requirements1_count", "name" => "要件��")
		, "2" => array("code" => "total_requirements2_count", "name" => "要件��")
		, "3" => array("code" => "total_requirements3_count", "name" => "要件��")
	);

	$result_RequirementCount = getReportRequirementCount($con,$fname,$date_next_y,$date_this_y,$date_last_y,$date_2y_befor,$date_3y_befor,$date_4y_befor);
	$data_list_RequirementCount = getDatalist_h_2($result_RequirementCount,$vert_fields_definision);

	if ($data_list_HospitalCount == false) {
		$data_count2 = 0;
	} else {
		$data_count2 = 1;
	}
	if($data_count1 != 0 or $data_count2 != 0){
		$data_count = 1;
	}
	$_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["h"]["data_list_RequirementCount"] = $data_list_RequirementCount;
	$_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["h"]["vert_list_RequirementCount"] = $vert_fields_definision;
	$_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["h"]["data_count_RequirementCount"] = $data_count2;

}

//====================================================================
// 印刷（XLSエクセル出力）の場合。内容を出力してdieする。
// もし失敗すれば、ブラウザ上に内容が表示されるであろう。
//====================================================================
if ($is_print_mode){

	require_once("fplus_headquarters_total_list_print.ini");

	$templ_dir = "./fplus/excel_templates/";
	// 作成するディレクトリパス
	$data_dir = "./fplus/excel_tmp";
	// ディレクトリが存在するかチェックし、なければ作成
	if(!is_dir($data_dir)){
		umask(0);
		$rc = mkdir($data_dir, 0777);
	}

	if($list_type == "g"){

		$templ_name = "fplus_headquarters_total_list_template1.xlsx";
		$data_file_name = fplus_medical_accident_report_list_print_execute_g($templ_dir,$templ_name,$data_dir,$date_this_y);
		updateOriginalFile($templ_dir,$templ_name,$data_dir, $data_file_name);

	}elseif($list_type == "h"){

		$templ_name = "fplus_headquarters_total_list_template2.xlsx";
		$data_file_name = fplus_medical_accident_report_list_print_execute_h($templ_dir,$templ_name,$data_dir,$date_this_y);
		updateOriginalFile($templ_dir,$templ_name,$data_dir, $data_file_name);

	}else{
		fplus_medical_accident_report_list_print_execute(
			$horz_field_definision[$list_type], // 横列フィールドの定義
			$data_list, // 検索データ
			$xlstitle, // タイトル
			$date_this_y, //年度
			$list_type //表タイプ
		);
	}

	die;
}

//====================================================================
// HTMLヘッダ
//====================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 医療事故報告管理表</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td {border:#E6B3D4 solid 1px; padding:3px; }
.list2 {border-collapse:collapse;}
.list2 td {border:#dfd95e solid 1px; padding:3px; }
</style>

<? //-------------------------------------------------------------------------?>
<? // JavaScript                                                              ?>
<? //-------------------------------------------------------------------------?>
<script type="text/javascript">

	<? // ************* エクセル出力開始 ************* ?>
	function startPrint(){
		if (document.frm.hdn_data_count.value == ""){
			alert("対象データが存在しません。");
			return;
		}
		document.frm.action = 'fplus_headquarters_total_list.php';
		document.frm.print_mode.value = "1";
		document.frm.list_type.value = "<?=$list_type?>";
		document.frm.target = "print_window";
		document.frm.submit();

		return false;

	}
	
	<? // ************* 画面の幅の取得 ************* ?>
	function getClientWidth(){
		if (document.body.clientWidth) return document.body.clientWidth;
		if (window.innerWidth) return window.innerWidth;
		if (document.documentElement && document.documentElement.clientWidth) {
			return document.documentElement.clientWidth;
		}
		return 0;
	}


	<? // ************* 上部スクロールバー制御 ************* ?>
	var scroll_div = null;
	var scrollbar_div = null;
	var scroll_image_gauge = null;
	var scroll_initialized = false;
	function listScrolled(id){
		var newW = getClientWidth();
		if (!newW) return;
		if (scroll_div==null) scroll_div = document.getElementById("scroll_div");
		if (!scroll_div) return;
		if (scrollbar_div==null) scrollbar_div = document.getElementById("scrollbar_div");
			scroll_div.style.overflowX = "scroll";

			if (scroll_div.scrollWidth > 0){
				if (scroll_image_gauge==null) scroll_image_gauge = document.getElementById("scroll_image_gauge");
				scroll_image_gauge.style.height = "1px";
				scroll_image_gauge.style.width = scroll_div.scrollWidth + "px";
			}

		scrollbar_div.style.width = (newW-2) + "px";
		scroll_div.style.width = (newW-2) + "px";

		scroll_initialized = true;
		if(!id) return;
		if (scrollbar_div.scrollLeft != scroll_div.scrollLeft){
			if (id == scroll_div.id){
				scrollbar_div.scrollLeft = scroll_div.scrollLeft;
			} else {
				scroll_div.scrollLeft = scrollbar_div.scrollLeft;
			}
		}
	}

	<? // ************* 集計ボタン押下時 ************* ?>
	function clickTotal(){
		document.frm.action = 'fplus_headquarters_total_list.php';
		document.frm.print_mode.value = "";
		document.frm.target = "_self";
		document.frm.submit();
		return false;
	}

	<? // ************* 切替ボタン押下 ************* ?>
	function changeListAndstartSearch(list_type){
		document.frm.action = 'fplus_headquarters_total_list.php';
		document.frm.list_type.value = list_type;
		return clickTotal();
	}

</script>

</head>
<body style="margin:0" onload="listScrolled();" onresize="listScrolled();">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>

<? //-------------------------------------------------------------------------?>
<? // ぱんくず、管理画面へのリンク                                            ?>
<? //-------------------------------------------------------------------------?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr bgcolor="#f6f9ff">
	<td width="32" height="32" class="spacing">
	  <a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a>
	</td>
	<td>
	  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplus_headquarters_total_list.php?session=<? echo($session); ?>&list_type=<?=$list_type?>"><b>本部集計表</b></a></font>
	</td>
	<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplusadm_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font>
	</td>
  </tr>
</table>


<? //-------------------------------------------------------------------------?>
<? // メインメニュータブと、ボトムマージン描画                                ?>
<? //-------------------------------------------------------------------------?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><? show_applycation_menuitem($session,$fname,""); ?></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><td bgcolor="#e6b3d4"><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>


<? //-------------------------------------------------------------------------?>
<? // 入力フォーム描画開始                                                    ?>
<? //-------------------------------------------------------------------------?>
<form name="frm" id="frm" action="fplus_headquarters_total_list.php" method="post" enctype="multipart/form-data">
	<input type="hidden" name="session" id="session" value="<? echo($session); ?>" />
	<input type="hidden" name="print_mode" id="print_mode" value="" />
	<input type="hidden" name="list_type" id="list_type" value="<?=$list_type?>" />
	<input type="hidden" name="hdn_data_count" id="hdn_data_count" value="<?=$result[0][$horz_field_definision[$list_type][0]["dbkey"]] == "" ? "0" :$data_count?>" />

	<? //-------------------------------------------------------------------------?>
	<? // サブメニューリンクボタン描画                                            ?>
	<? //-------------------------------------------------------------------------?>
	<table border="0" cellspacing="0" cellpadding="0">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="4"></td>
		</tr>
		<tr>
			<td style="padding:3px 6px; float:left; background-color:#<?=$list_type=="a"?"ff99cc":"f4f4f4"?>; border:1px solid #ccc;">
				<?=$font?><a href="javascript:void(0)" onclick="return changeListAndstartSearch('a');" style="color:#<?=$list_type=="a"?"fff":"00f"?>"><?=$list_title_a?></a></font>
			</td>
			<td width="6"></td>
			<td style="padding:3px 6px; float:left; background-color:#<?=$list_type=="b"?"ff99cc":"f4f4f4"?>; border:1px solid #ccc;">
				<?=$font?><a href="javascript:void(0)" onclick="return changeListAndstartSearch('b');" style="color:#<?=$list_type=="b"?"fff":"00f"?>"><?=$list_title_b?></a></font>
			</td>
			<td width="6"></td>
			<td style="padding:3px 6px; float:left; background-color:#<?=$list_type=="c"?"ff99cc":"f4f4f4"?>; border:1px solid #ccc;">
				<?=$font?><a href="javascript:void(0)" onclick="return changeListAndstartSearch('c');" style="color:#<?=$list_type=="c"?"fff":"00f"?>"><?=$list_title_c?></a></font>
			</td>
			<td width="6"></td>
			<td style="padding:3px 6px; float:left; background-color:#<?=$list_type=="d"?"ff99cc":"f4f4f4"?>; border:1px solid #ccc;">
				<?=$font?><a href="javascript:void(0)" onclick="return changeListAndstartSearch('d');" style="color:#<?=$list_type=="d"?"fff":"00f"?>"><?=$list_title_d?></a></font>
			</td>
		</tr>
	</table>
	<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="6"></td>
		</tr>
		<tr>
			<td style="padding:3px 6px; float:left; background-color:#<?=$list_type=="e"?"ff99cc":"f4f4f4"?>; border:1px solid #ccc;">
				<?=$font?><a href="javascript:void(0)" onclick="return changeListAndstartSearch('e');" style="color:#<?=$list_type=="e"?"fff":"00f"?>"><?=$list_title_e?></a></font>
			</td>
			<td width="6"></td>
			<td style="padding:3px 6px; float:left; background-color:#<?=$list_type=="f"?"ff99cc":"f4f4f4"?>; border:1px solid #ccc;">
				<?=$font?><a href="javascript:void(0)" onclick="return changeListAndstartSearch('f');" style="color:#<?=$list_type=="f"?"fff":"00f"?>"><?=$list_title_f?></a></font>
			</td>
			<td width="6"></td>
			<td style="padding:3px 6px; float:left; background-color:#<?=$list_type=="g"?"ff99cc":"f4f4f4"?>; border:1px solid #ccc;">
				<?=$font?><a href="javascript:void(0)" onclick="return changeListAndstartSearch('g');" style="color:#<?=$list_type=="g"?"fff":"00f"?>"><?=$list_title_g?></a></font>
			</td>
			<td width="6"></td>
			<td style="padding:3px 6px; float:left; background-color:#<?=$list_type=="h"?"ff99cc":"f4f4f4"?>; border:1px solid #ccc;">
				<?=$font?><a href="javascript:void(0)" onclick="return changeListAndstartSearch('h');" style="color:#<?=$list_type=="h"?"fff":"00f"?>"><?=$list_title_h?></a></font>
			</td>
		</tr>
		<tr>
			<td height="6"></td>
		</tr>
	</table>

	<? //-------------------------------------------------------------------------?>
	<? // 集計条件                                                                ?>
	<? //-------------------------------------------------------------------------?>
	<div style="float:left;">
	<table border="0" cellspacing="0" cellpadding="0" class="list1">
		<tr>
			<td align="center" width="70" bgcolor="#feeae9"><?=$font?>集計期間</font></td>
			<td bgcolor="#fff8f7"><?=$font?>
				<select id="target_year" name="target_year">
					<? for($i=(int)$date_now_y -4;$i<=(int)$date_now_y;$i++){ ?>
					<option value="<?=$i?>" <?=($i==$date_this_y ? "selected='selected'" : "")?>><?=$i?></option>
					<? } ?>
				</select> 年度
				<input type="button" onclick="clickTotal();" value="集計"  /></font>
			</td>
		</tr>
	</table>
	</div>

	<div><?=$font?>
		<? // ************* ボタン ******************** ?>
		<div style="float:right;">
			<table>
				<tr>
					<td>
						<?=$font?><input type="button" onclick="startPrint();" value="EXCEL出力"  <?= $data_count == 0 ? "disabled" : "" ?> /></font>
					</td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
		<br style="clear:both"/>
		</font>
	</div>

	<? //-------------------------------------------------------------------------?>
	<? // 一覧表                                                                  ?>
	<? //-------------------------------------------------------------------------?>
	<?
		foreach ($horz_field_definision[$list_type] as $idx => $row){
			$colwidth_sum += (int)$row["colwidth"];
		}
	?>
	<? if($colwidth_sum > 1024){ ?>
	<div width="100%" style="margin-top:6px; border:1px solid #E6B3D4; background-color:#eee">
	<div id="scrollbar_div" style="line-height:0; font-size:1px; height:18px; overflow-x:scroll;" onscroll="listScrolled(this.id);">
		<img src="img/spacer.gif" id="scroll_image_gauge"/>
	</div>
	<div width="100%" id="scroll_div" onscroll="listScrolled(this.id);" style="overflow-x:scroll;">
	<? } ?>
		<? // テーブル描画 ?>
		<?
		if($list_type == "a"){
			gettable_a($data_list,$horz_field_definision[$list_type],$date_this_y,$data_count,$xlstitle);
		}elseif($list_type == "b"){
			gettable_a($data_list,$horz_field_definision[$list_type],$date_this_y,$data_count,$xlstitle);
		}elseif($list_type == "c"){
			gettable_c($data_list,$horz_field_definision[$list_type],$job,$date_this_y,$data_count,$xlstitle);
		}elseif($list_type == "d"){
			gettable_a($data_list,$horz_field_definision[$list_type],$date_this_y,$data_count,$xlstitle);
		}elseif($list_type == "e"){
			gettable_a($data_list,$horz_field_definision[$list_type],$date_this_y,$data_count,$xlstitle);
		}elseif($list_type == "f"){
			gettable_f($data_list,$horz_field_definision[$list_type],$date_this_y,$data_count,$xlstitle);
		} else if ($list_type == "g") {
		?>
			<style type="text/css">
			.graph {
				clear:both;
				margin: 5px 0;
			}
			</style>
			
			<table border="0" cellspacing="0" cellpadding="4" style="background-color: #ffffff">
				<tr>
					<td align="center">
						<?
						gettable_g($data_list, $horz_field_definision[$list_type], $date_this_y, $data_count, $xlstitle);
						?>
					</td>
					<td>
						<div class="graph">
							<img
							alt="グラフ(描画には時間がかかります。しばらくお待ちください。)"
							src="fplus_headquarters_total_list_make_graph.php?type=pie1"
							/>
						</div>
					</td>
				</tr>
				<tr>
					<td width="1">
						<div class="graph">
							<img
							alt="グラフ(描画には時間がかかります。しばらくお待ちください。)"
							src="fplus_headquarters_total_list_make_graph.php?type=bar"
							/>
						</div>
					</td>
					<td>
						<div class="graph">
							<img
							alt="グラフ(描画には時間がかかります。しばらくお待ちください。)"
							src="fplus_headquarters_total_list_make_graph.php?type=pie2"
							/>
						</div>
					</td>
				</tr>
			</table>
		<?
		} else if ($list_type == "h") {
		?>
			<style type="text/css">
			.graph {
				clear:both;
				margin: 5px 0;
			}
			</style>
			
			<table border="0" cellspacing="0" cellpadding="4" style="background-color: #ffffff">
				<tr>
					<td align="left" colspan="2">
						<?
						gettable_h($data_list_HospitalCount,$horz_field_definision[$list_type],$data_count1,$xlstitle,$date_this_y);
						?>
					</td>
				</tr>
				<tr>
					<td width="1">
						<div class="graph">
							<img
							alt="グラフ(描画には時間がかかります。しばらくお待ちください。)"
							src="fplus_headquarters_total_list_make_graph.php?type=bar2"
							/>
						</div>
					</td>
					<td>
						<div class="graph">
							<img
							alt="グラフ(描画には時間がかかります。しばらくお待ちください。)"
							src="fplus_headquarters_total_list_make_graph.php?type=bar3"
							/>
						</div>
					</td>
				</tr>
			</table>
		<?
		}
		?>
	<? if($colwidth_sum > 1024){ ?>
	</div>
	</div>
	<? } ?>
</form>
</td>
</tr>
</table>
</body>
</html>
