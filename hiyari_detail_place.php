<?
//==============================
//本処理
//==============================

require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_db_class.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$hiyari = check_authority($session, 48, $fname);
if ($hiyari == "0")
{
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

$db = new hiyari_db_class($con, $fname);
// $edit_item_list = get_super_item($con, $fname);
//==============================
//選択項目情報の取得
//==============================
if($is_postback != "true")
{
    if($_GET['menu']) {
        $easy_item_code = $_GET['menu'];
    }

    $edit_item_list = get_sub_item($db, $easy_item_code);
    $line_count = count($edit_item_list);

    if($line_count == 0) {
        $line_count = 1;
        $easy_name = get_easy_name($db, $easy_item_code);
        $item_count = judge_easyinput_item($db, $easy_item_code);
        $type_count = judge_easyinput_type($db, $easy_item_code);

        if($item_count == 0) {
            $sql = "INSERT INTO inci_easyinput_item_mst(grp_code, easy_item_code, easy_item_name) VALUES( ";
            $data = array(115, $easy_item_code, "「{$easy_name}」の詳細");
            $db->insert($sql, $data, true);
        }

        if($type_count == 0) {
            $sql = "INSERT INTO inci_easyinput_item_type(grp_code, easy_item_code, item_type, item_must, item_must_able) VALUES( ";
            $data = array(115, $easy_item_code, "select", 1, 1);
            $db->insert($sql, $data, true);
        }
    }
}

//==============================
//ポストバック時の処理
//==============================
if($is_postback == "true")
{
    //==============================
    //ポストバックデータより現在の表示内容を作成
    //==============================
    $edit_item_list = "";
    for($line_no=0;$line_no<$line_count;$line_no++)
    {
        $p_easy_item_code = "easy_item_code_".$line_no;
        $p_easy_name      = "easy_name_".$line_no;
        $p_easy_code      = "easy_code_".$line_no;
        $p_easy_flag      = "easy_flag_".$line_no;
        $p_easy_num       = "easy_num_".$line_no;
        $p_inci_name      = "inci_name_".$line_no;
        $p_inci_code      = "inci_code_".$line_no;

        $tmp_array = array();

        $tmp_array["easy_item_code"] = $$p_easy_item_code;
        $tmp_array["easy_name"]      = $$p_easy_name;
        $tmp_array["easy_code"]      = $$p_easy_code;
        $tmp_array["easy_flag"]      = ($$p_easy_flag == true) ? 0 : 1;
        $tmp_array["easy_num"]       = $line_no+1;
        $tmp_array["inci_name"]      = $$p_inci_name;
        $tmp_array["inci_code"]      = $$p_inci_code;

        if(empty($tmp_array["easy_item_code"])) {
            $tmp_array["easy_item_code"] = $_GET['easy_item_code'];//$_GET['menu'];
        }

        $edit_item_list[$line_no] = $tmp_array;
    }

    //==============================
    //行追加
    //==============================
    if($postback_mode == "add_line")
    {
        $tmp_array = array();

        $tmp_array["easy_item_code"] = $easy_item_code;
        $tmp_array["easy_name"]      = '';
        $tmp_array["easy_code"]      = '';
        $tmp_array["easy_flag"]      = '1';
        $tmp_array["easy_num"]       = '';
        $tmp_array["inci_name"]      = '';
        $tmp_array["inci_code"]      = '';

        $new_line_no = 0;
        $tmp_edit_item_list = array();
        for($line_no=0;$line_no<$line_count;$line_no++)
        {
            //既存行に割り込み追加の場合
            if($add_line_no == $line_no)
            {
                $tmp_edit_item_list[$new_line_no] = $tmp_array;
                $new_line_no++;
            }

            $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
            $new_line_no++;
        }

        //最終行に追加の場合
        if($add_line_no == $line_count)
        {
            $tmp_edit_item_list[$add_line_no] = $tmp_array;
            $new_line_no++;
        }

        $edit_item_list = $tmp_edit_item_list;
        $line_count++;

    }
    //==============================
    //行削除
    //==============================
    elseif($postback_mode == "delete_line")
    {
        $new_line_no = 0;
        $tmp_edit_item_list = array();
        for($line_no=0;$line_no<$line_count;$line_no++)
        {
            if($delete_line_no != $line_no)
            {
                $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
                $new_line_no++;
            }
        }
        $edit_item_list = $tmp_edit_item_list;
        $line_count--;
    }
    //==============================
    //順番変更
    //==============================
    elseif($postback_mode == "change_line_no")
    {
        $tmp_edit_item_list = array();
        for($line_no=0;$line_no<$line_count;$line_no++)
        {
            //移動するデータを先に割り当てる。
            $tmp_edit_item_list[$change_line_no_after_line_no] = $edit_item_list[$change_line_no_target_line_no];

            //移動するデータ以外を割り当てる
            if($line_no != $change_line_no_target_line_no)
            {
                //先頭方向に移動する場合
                if($change_line_no_target_line_no > $change_line_no_after_line_no)
                {
                    //移動元-移動先間は末尾方向へ１移動。それ以外は移動なし。
                    $new_line_no = $line_no;
                    if($change_line_no_target_line_no > $line_no && $change_line_no_after_line_no <= $line_no)
                    {
                        $new_line_no++;
                    }
                    $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
                }
                //末尾方向に移動する場合
                elseif($change_line_no_target_line_no < $change_line_no_after_line_no)
                {
                    //移動元-移動先間は先頭方向へ１移動。それ以外は移動なし。
                    $new_line_no = $line_no;
                    if($change_line_no_target_line_no < $line_no && $change_line_no_after_line_no >= $line_no)
                    {
                        $new_line_no--;
                    }
                    $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
                }
                //移動しない場合(ありえない)
                else
                {
                    //移動しない。
                    $new_line_no = $line_no;
                    $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
                }

            }
        }
        $edit_item_list = $tmp_edit_item_list;
    }
    //==============================
    //ＤＢ更新
    //==============================
    elseif($postback_mode == "update")
    {
        $update_item = $edit_item_list[0];
        if(!empty($update_item['easy_item_code'])) $easy_item_code = $update_item['easy_item_code'];

        // 更新する前に、一度情報を無効化する
        $sql = "UPDATE inci_report_materials SET ";
        $set = array('available', 'easy_flag');
        $setvalue = array('f', 0);
        $cond = "WHERE grp_code = 115 AND easy_item_code = '{$easy_item_code}'";

        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        for($line_no=0;$line_no<$line_count;$line_no++)
        {
            // データを取得
            $update_item = $edit_item_list[$line_no];
            // データを変数に挿入
            $easy_item_code = $update_item['easy_item_code'];
            $easy_name      = $update_item['easy_name'];
            $easy_code      = $update_item['easy_code'];
            $easy_flag      = empty($update_item['easy_flag']) ? 0 : 1;
            $easy_num       = $line_no+1;
            $inci_code      = $update_item['inci_code'];

            // 同じデータがあればUPDATE
            if($easy_code > 0) {
                $sql = "UPDATE inci_report_materials SET ";
                $set = array('easy_name', 'easy_flag', 'easy_num', 'inci_name', 'inci_code', 'available');
                $setvalue = array(pg_escape_string($easy_name), $easy_flag, $easy_num, pg_escape_string($easy_name), pg_escape_string($inci_code), 't');
                $cond = "WHERE grp_code = 115 AND easy_item_code = {$easy_item_code} AND easy_code = '{$easy_code}'";

                $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                if ($upd == 0) {
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
            // 新規はINSERT
            } else {
                // item_idのMAX値を取得する
                $sql = "SELECT max(to_number(easy_code, '9999')) FROM inci_report_materials where grp_code = 115";
                $db->query($sql);
                $max = $db->getOne();
                if($max <= 1000){
                    $easy_code = "1001";
                }else{
                    $easy_code = $max + 1;
                }

                $sql = "INSERT INTO inci_report_materials(cate_code, grp_code, easy_item_code, easy_name, easy_code, easy_flag, easy_num, acci_item_code, acci_flag, acci_num, inci_item_code, inci_name, inci_code, inci_flag, inci_num, available) VALUES(";
                $cond = array(1, 115, $easy_item_code, pg_escape_string($easy_name), $easy_code, $easy_flag, $easy_num, 0, 0, 0, 0, pg_escape_string($easy_name), $inci_code, 1, 0, 't');

                $ins = insert_into_table($con, $sql, $cond, $fname);
                if ($ins == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
            }
        }
    }
}

//========================================================================================================================
//HTML出力
//========================================================================================================================

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 管理項目変更</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

//起動時の処理
function loadaction() {
<?php if ( $error_message != "" ) { //更新時エラーがある場合 ?>
	alert("<?=$error_message?>");
<?php } ?>

<?php if ( $postback_mode == "add_line" ) { ?>
	var focus_obj_name = "easy_name_<?=$add_line_no?>";
	var focus_obj = document.getElementsByName(focus_obj_name)[0];
	focus_obj.focus();
<?php } ?>
}

//ＤＢ更新処理
function update_data() {
    if ( update_check() ) {
        document.form1.postback_mode.value = "update";
        document.form1.submit();
    }
}

//順番変更処理
function list_change_line_no(change_line_no_target_line_no, change_line_no_after_line_no ) {
    document.form1.postback_mode.value = "change_line_no";
    document.form1.change_line_no_target_line_no.value = change_line_no_target_line_no;
    document.form1.change_line_no_after_line_no.value = change_line_no_after_line_no;
    document.form1.submit();
}

//行削除処理
function list_delete_line(line_no) {
    document.form1.postback_mode.value = "delete_line";
    document.form1.delete_line_no.value = line_no;
    document.form1.submit();
}

//行追加処理
function list_add_line(line_no) {
    document.form1.postback_mode.value = "add_line";
    document.form1.add_line_no.value = line_no;
    document.form1.submit();
}

//削除-使用済みチェック/全非表示チェック
function delete_check(line_no, used_item_flg) {
    if ( used_item_flg ) {
        alert("既に使用されている項目は削除できません。");
        return false;
    }
    return true;
}
function update_check() {
<?php for ( $line_no = 0; $line_no < $line_count; $line_no++ ) { ?>
    if ( document.form1.easy_name_<?=$line_no?>.value == "" ) {
        alert("表示内容が未入力です。");
        return false;
    }
<?php } ?>
    return true;
}


</script>

<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="loadaction()">
<form id="form1" name="form1" action="" method="post" onsubmit="return false;">

<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="easy_item_code" value="<?=$easy_item_code?>">

<input type="hidden" name="line_count" value="<?=$line_count?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">
<input type="hidden" name="change_line_no_after_line_no" value="">
<input type="hidden" name="change_line_no_target_line_no" value="">
<input type="hidden" name="delete_line_no" value="">
<input type="hidden" name="add_line_no" value="">



<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<?
    $PAGE_TITLE = "項目管理";
    show_hiyari_header_for_sub_window($PAGE_TITLE);
?>
</table>
<!-- ヘッダー END -->


<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<? // ヘッダータブ表示
show_item_edit_header_tab($session, $fname, $grp_code, $easy_item_code);
?>

<!-- メイン START -->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td>

<!--// 表示系 START -->

<table border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
    <td bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="hiyari_item_edit.php?session=<?=$session?>&grp_code=110&easy_item_code=60"><b>発生場所</b></a></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="hiyari_detail_place.php?session=<?=$session?>&grp_code=115&easy_item_code=1"><b>発生場所詳細</b></a></font>
    </td>
</tr>
<tr height="22">
    <td bgcolor="#DFFFDC" nowrap>
        <select name="menu" onChange="location.href='hiyari_detail_place.php?session=<?=$session?>&menu='+this.form.menu.value">
        <?
        $item = get_item($db);
        foreach($item as $value) {
            $checked_flg = ($easy_item_code == $value['easy_code']) ? ' selected' : '';
            echo "<option value='{$value['easy_code']}'{$checked_flg}>{$value['easy_name']}</option>\n";
        }
        ?>
        </select>
    </td>
</tr>
</table>
<!--// 表示系 END -->

<!-- 外の枠 START -->
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="800" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">


<!-- 上部更新ボタン等 START-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="left" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b><?=htmlspecialchars($easy_item_name)?></b></font>
</td>
<td>
    &nbsp
</td>
<td align="right">
    <input type="button" value="更新" onclick="update_data();">
</td>
</tr>
</table>
<!-- 上部更新ボタン等 START-->

<!-- 一覧 START-->
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22">
    <td width="300" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示内容</font>
    </td>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非表示</font>
    </td>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示順</font>
    </td>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行削除</font>
    </td>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行追加</font>
    </td>
</tr>

<?
for($line_no=0;$line_no<$line_count;$line_no++)
{
    $edit_item      = $edit_item_list[$line_no];
    $easy_item_code = empty($edit_item["easy_item_code"]) ? $_GET['menu'] : $edit_item["easy_item_code"];
    $easy_name      = $edit_item["easy_name"];
    $easy_code      = $edit_item["easy_code"];
    $easy_flag      = $edit_item["easy_flag"];
    $easy_num       = $edit_item["easy_num"];
    $inci_name      = $edit_item["inci_name"];
    $inci_code      = $edit_item["inci_code"];

    $used_item_flg     = in_array($super_item_id,$used_super_item_id_list);

    //非表示欄のチェック状態のHTML
    $no_disp_checked_html = "";
    if(!$easy_flag)
    {
        $no_disp_checked_html = "checked";
    }

    //初期項目フラグの値のHTML
    $default_item_flg_value_html = "false";
    if($default_item_flg)
    {
        $default_item_flg_value_html = "true";
    }

    $used_item_flg_script = "false";
    if($used_item_flg)
    {
        $used_item_flg_script = "true";
    }
?>

<input type="hidden" name="easy_item_code_<?=$line_no?>" value="<?=$easy_item_code?>">
<!--//<input type="hidden" name="easy_name_<?=$line_no?>" value="<?=$easy_name?>">-->
<input type="hidden" name="easy_code_<?=$line_no?>" value="<?=$easy_code?>">
<!--//<input type="hidden" name="easy_flag_<?=$line_no?>" value="<?=$easy_flag?>">-->
<input type="hidden" name="easy_num_<?=$line_no?>" value="<?=$easy_num?>">
<input type="hidden" name="inci_name_<?=$line_no?>" value="<?=$inci_name?>">
<!--//<input type="hidden" name="inci_code_<?=$line_no?>" value="<?=$inci_code?>">-->
<!--//<input type="hidden" name="default_item_flg_<?=$line_no?>" value="<?=$default_item_flg_value_html?>">-->
<tr height="22">

<td align="left" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <?php
    if($easy_code > 1000 || empty($easy_code)) {
    ?>
    <input type="text" name="easy_name_<?=$line_no?>" value="<?=htmlspecialchars($easy_name)?>" style="width:290px" maxlength="50">
    <?php
    } else {
    ?>
    <input type="hidden" name="easy_name_<?=$line_no?>" value="<?=htmlspecialchars($easy_name)?>">
    <?=htmlspecialchars($easy_name)?>
    <?php
    }
    ?>
    </font>
</td>

<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <input type="checkbox" name="easy_flag_<?=$line_no?>" value="true" <?=$no_disp_checked_html?> >
    </font>
</td>

<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if($line_no-1 > -1)
{
?>
    <img src="img/up.gif" onclick="list_change_line_no('<?=$line_no?>','<?=$line_no-1?>');" style="cursor: pointer;">
<?
}
?>
<?
if($line_no+1 < $line_count)
{
?>
    <img src="img/down.gif" onclick="list_change_line_no('<?=$line_no?>','<?=$line_no+1?>');" style="cursor: pointer;">
<?
}
?>
    </font>
</td>


<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if($easy_code > 1000)
{
?>
    <input type="button" value="行削除" onclick="if(delete_check(<?=$line_no?>,<?=$used_item_flg_script?>)){list_delete_line('<?=$line_no?>');}" >
<?
}
?>
    </font>
</td>

<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <input type="button" name="add_line" value="行追加" onclick="list_add_line('<?=$line_no+1?>');" >
    </font>
</td>

</tr>
<?
}
?>


</table>
<!-- 一覧 END -->


<!-- 下部更新ボタン等 START-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="right">
    <input type="button" value="更新" onclick="update_data();">
</td>
</tr>
</table>
<!-- 下部更新ボタン等 START-->


</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<!-- 外の枠 END -->



</td>
</tr>
</table>
<!-- メイン END -->


</td>
</tr>
</table>
<!-- BODY全体 END -->

</form>
</body>
</html>
<?

//==============================
//DBコネクション終了
//==============================
pg_close($con);

function get_sub_item($db, $id) {
    $sql = "SELECT * FROM inci_report_materials WHERE grp_code = 115 AND easy_item_code = {$id} AND available = 't' order by easy_num";
    $db->query($sql);

    return $db->getAll();
}

function get_item($db) {
    $sql = "SELECT * FROM inci_report_materials WHERE grp_code = 110 AND easy_item_code = 60  AND available = 't' order by easy_num";
    $db->query($sql);

    return $db->getAll();
}

function get_easy_name($db, $easy_code) {
    $sql = "SELECT easy_name FROM inci_report_materials WHERE grp_code = 110 AND easy_item_code = 60 AND easy_code = '{$easy_code}' AND available = 't'";
    $db->query($sql);

    return $db->getOne();
}

function judge_easyinput_item($db, $easy_item_code) {
    $sql = "SELECT count(*) FROM inci_easyinput_item_mst WHERE grp_code = 115 AND easy_item_code = {$easy_item_code}";
    $db->query($sql);

    return $db->getOne();
}

function judge_easyinput_type($db, $easy_item_code) {
    $sql = "SELECT count(*) FROM inci_easyinput_item_type WHERE grp_code = 115 AND easy_item_code = {$easy_item_code}";
    $db->query($sql);

    return $db->getOne();
}

?>
