<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix タスク | タスク一覧</title>
<?
require_once("Cmx.php");
require_once("aclg_set.php");
require("about_authority.php");
require("about_session.php");
require("show_to_do.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// タスク権限のチェック
$checkauth = check_authority($session, 30, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// デフォルト日付をシステム日付とする
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}

// データベースに接続
$con = connect2db($fname);
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteTask(){
	if (confirm("削除してよろしいですか？")) {
		document.task.submit();
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" colspan="2" class="spacing"><a href="work_menu.php?session=<? echo($session); ?>"><img src="img/icon/b04.gif" width="32" height="32" border="0" alt="タスク"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_menu.php?session=<? echo($session); ?>"><b>タスク</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#5279a5"><a href="work_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>タスク一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="to_do_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td align="right">
<? if ($mode == "full") { ?>
<input type="button" value="戻る" onclick="location.href = 'work_menu.php?session=<? echo($session); ?>&date=<? echo($date); ?>';" style="width:12em;">
<? } else { ?>
<input type="button" value="全てのタスクの表示" onclick="location.href = 'work_menu.php?session=<? echo($session); ?>&date=<? echo($date); ?>&mode=full';" style="width:12em;">
<? } ?>
<input type="button" value="削除" onclick="deleteTask();">
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? show_task_list_navigation($session, $date, $mode); ?>
<form name="task" action="work_delete.php" method="post">
<? show_task_list($con, $date, $mode, $session, $fname); ?>
<input type="hidden" name="session" value="<? echo($session);?>">
<input type="hidden" name="date" value="<? echo($date);?>">
<input type="hidden" name="mode" value="<? echo($mode);?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
