<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理｜有休繰越・付与修正</title>
<?
require_once("about_comedix.php");
require_once("show_timecard_common.ini");
require_once("timecard_bean.php");
require_once("atdbk_common_class.php");
require_once("show_select_values.ini");
require_once("work_admin_paid_holiday_common.php");
require_once("timecard_common_class.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("date_utils.php");
require_once("calendar_name_class.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
//カレンダー名、所定労働時間履歴用
$calendar_name_class = new calendar_name_class($con, $fname);


//更新ボタン
if ($update_flg == "1" && $del_flg != "1") {

	$wk_mmdd = $select_mon.$select_day;
	if ($service_year == "-") {
		$service_year = "";
	}
	if ($service_mon == "-") {
		$service_mon = "";
	} else {
		$service_mon = intval($service_mon);
	}
	// トランザクションを開始
	pg_query($con, "begin transaction");
	//データ追加
	if ($data_cnt == "0" || $data_cnt == "") {
        $sql = "insert into emppaid (emp_id, year, days1, days2, adjust_day, paid_hol_tbl_id, paid_hol_add_mmdd, service_year, service_mon, carry_time_minute) values (";
        $wk_days1_minute = round($days1_hour * 60); //20141014 分対応
        $content = array($emp_id, $select_year, $days1, $days2, $adjust_day, $paid_hol_tbl_id, $wk_mmdd, $service_year, $service_mon, $wk_days1_minute);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	} else {
        $wk_days1_minute = round($days1_hour * 60); //20141014 分対応
        //データ更新
		$sql = "update emppaid set";
        $set = array("paid_hol_tbl_id", "year", "paid_hol_add_mmdd", "service_year", "service_mon", "days1", "days2", "adjust_day", "carry_time_minute");
        $setvalue = array($paid_hol_tbl_id, $select_year, $wk_mmdd, $service_year, $service_mon, $days1, $days2, $adjust_day, $wk_days1_minute);
		$cond = "where emp_id = '$emp_id' and year = '$year' ";
		if ($paid_hol_add_mmdd != "") {
			$cond .= " and paid_hol_add_mmdd = '$paid_hol_add_mmdd' ";
		}
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	//追加分
	if ($add_flg == "1") {
        $wk_paid_hol_add_mmdd = $select_mon2.$select_day2;
        $sql = "select count(*) as cnt from emppaid ";
        $cond = "where emp_id = '$emp_id' and year = '$select_year2' and paid_hol_add_mmdd = '$wk_paid_hol_add_mmdd' ";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        $cnt = pg_result($sel,0,"cnt");
        if ($cnt == "0") {

            $sql = "insert into emppaid (emp_id, year, days1, days2, adjust_day, paid_hol_tbl_id, paid_hol_add_mmdd, service_year, service_mon, carry_time_minute) values (";

            if ($service_year2 == "-") {
                $service_year2 = "";
            }
            if ($service_mon2 == "-") {
                $service_mon2 = "";
            } else {
                $service_mon2 = intval($service_mon2);
            }
            $wk_days1_2_minute = round($days1_2_hour * 60); //20141014 分対応
            $content = array($emp_id, $select_year2, $days1_2, $days2_2, $adjust_day2, $paid_hol_tbl_id2, $wk_paid_hol_add_mmdd, $service_year2, $service_mon2, $wk_days1_2_minute);
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
                exit;
            }
        }
	}

	// トランザクションをコミット
	pg_query($con, "commit");

	$update_flg = "";

//呼出元一覧を再表示 20111222
	echo("<script type=\"text/javascript\">opener.document.mainform.submit();</script>");

}

//削除
if ($del_flg == "1") {
	// トランザクションを開始
	pg_query($con, "begin transaction");

	$sql = "delete from emppaid";
	$cond = "where emp_id = '$emp_id' and year = '$year' ";
	if ($paid_hol_add_mmdd != "") {
		$cond .= " and paid_hol_add_mmdd = '$paid_hol_add_mmdd' ";
	}
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	// トランザクションをコミット
	pg_query($con, "commit");

	$update_flg = "";
	$del_flg = "";

	//呼出元一覧を再表示 20111222
	echo("<script type=\"text/javascript\">opener.document.mainform.submit();</script>");
}

// 職員ID・氏名を取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm, emp_personal_id, emp_join from empmst";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
$emp_personal_id = pg_fetch_result($sel, 0, "emp_personal_id");
$emp_join = pg_fetch_result($sel, 0, "emp_join");
$emp_join_str = substr($emp_join, 0, 4)."年".intval(substr($emp_join, 4, 2))."月".intval(substr($emp_join, 6, 2))."日";

$sql = "select specified_time from empcond";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
    $paid_specified_time = pg_fetch_result($sel, 0, "specified_time");
}
else {
    $paid_specified_time = "";
}

//有休付与日数表の読み込み
$sql = "select * from timecard_paid_hol order by order_no";
$cond = "";
$paid_sel = select_from_table($con, $sql, $cond, $fname);
$paid_sel2 = select_from_table($con, $sql, $cond, $fname);
if ($paid_sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$arr_paid_hol = array();
$arr_tbl_id_disp_flg = array(); //日数を合計し0ならば非表示、0以外は表示 20120329
$arr_paid_hol_tbl_id = array(); //有休表ID
while($row = pg_fetch_array($paid_sel)){
	$paid_hol_tbl_id      = $row["paid_hol_tbl_id"];
    $arr_paid_hol_tbl_id[] = $paid_hol_tbl_id;
    $wk_days = 0;
    for ($i=1; $i<=8; $i++) {
		$varname = "add_day".$i;
        //未設定時の対応、""は0とする、8番目が""の時は7番目の数値を使用 20120821
        $wk_add_day = $row["$varname"];
        if ($i == 8 && $wk_add_day == "") {
            $wk_add_day = $arr_paid_hol[$paid_hol_tbl_id][7];
        }
        if ($wk_add_day == "") {
            $wk_add_day = "0";
        }

        $arr_paid_hol[$paid_hol_tbl_id][$i] = $wk_add_day;
        $wk = ($row["$varname"] != "") ? $row["$varname"] : 0;
        $wk_days += $wk;
	}
    $arr_tbl_id_disp_flg[$paid_hol_tbl_id] = ($wk_days != 0);
}

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);

$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, date("Ymd"), "");

//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();

$duty_form = $obj_hol_hour->get_duty_form($emp_id);

$paid_hol_hour_flag = "f";
if ($obj_hol_hour->paid_hol_hour_flag == "t" &&
    (($duty_form != "2" && $obj_hol_hour->paid_hol_hour_full_flag == "t") ||
    ($duty_form == "2" && $obj_hol_hour->paid_hol_hour_part_flag == "t"))){
    $paid_hol_hour_flag = "t";
}

//所属表示
$str_orgname = get_orgname($con, $fname, $emp_id, $timecard_bean);
$str_orgname = substr($str_orgname, 1);
$str_orgname = str_replace(" ", "＞", $str_orgname);

//有休情報を取得
$sql = "select * from emppaid";
$cond = "where emp_id = '$emp_id' order by year, paid_hol_add_mmdd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$num = pg_num_rows($sel);

//同一年月日のデータを除く 20110404
$arr_paid_info =  array();
if ($num > 0) {
	$i = 0;
	$old_key = "";
	while ($row = pg_fetch_array($sel)) {
		$wk_key = $row["year"].$row["paid_hol_add_mmdd"];
		$wk_mmdd = $row["paid_hol_add_mmdd"];
		if ($old_key != $wk_key) {
			$arr_paid_info[$i] = $row;
			$i++;
		}
		$old_key = $wk_key;
	}
	$num = count($arr_paid_info);
	$data_cnt = $num;
}

//一件もない場合
if ($num == 0) {
	$arr_paid_info[0]["paid_hol_tbl_id"] = $arg_tbl_id;

	//有休付与情報を設定、日数、付与日、勤続
	//勤続1年以上の場合
	$year_before = date("Ymd", strtotime("-1 year"));
	$emp_join_year = substr($emp_join, 0, 4);
	$emp_join_mmdd = substr($emp_join, 4, 4);
	if ($emp_join > 0 && $emp_join <= $year_before) {
		$wk_year = date("Y");
		//4月1日基準の場合
		if ($timecard_bean->closing_paid_holiday == "1") {
			$arr_paid_info[0]["paid_hol_add_mmdd"] = "0401";
		}
		//1月1日基準の場合
		elseif ($timecard_bean->closing_paid_holiday == "2") {
			$arr_paid_info[0]["paid_hol_add_mmdd"] = "0101";
		}
		//採用日
		else {
			$arr_paid_info[0]["paid_hol_add_mmdd"] = $emp_join_mmdd;
			//当日より後の月日の場合、前年データとする
			if ($emp_join_mmdd > date("md")) {
				$wk_year--;
			}
		}
		$arr_paid_info[0]["year"] = $wk_year;
		$wk_end_date = $wk_year.$arr_paid_info[0]["paid_hol_add_mmdd"];
		$len_service = get_length_of_service($emp_join, $wk_end_date);
		list($wk_yy, $wk_mm) = split("/", $len_service);
		$arr_paid_info[0]["service_year"] = $wk_yy;
		$arr_paid_info[0]["service_mon"] = 0;

	} else{
		$shigatsu_flg = false;
		//4月1日基準の場合
		if ($timecard_bean->closing_paid_holiday == "1") {
			//入職月
			$emp_join_mon = substr($emp_join, 4, 2);
			//3ヶ月基準、1から3月
			if ($timecard_bean->criteria_months == "1") {
				if ($emp_join_mon >= "01" && $emp_join_mon <= "03") {
					$shigatsu_flg = true;
				}
			}
			//6ヶ月基準、10から3月
			else {
				if (($emp_join_mon >= "01" && $emp_join_mon <= "03") ||
						($emp_join_mon >= "10" && $emp_join_mon <= "12")) {
					$shigatsu_flg = true;
				}
			}
		}

		//4月1日基準で入職日が1-3月か10-3月の場合
		if ($shigatsu_flg) {
			$arr_paid_info[0]["year"] = date("Y");
			$arr_paid_info[0]["paid_hol_add_mmdd"] = "0401";
			$wk_yy = 1;
			$wk_mm = 0;
			$arr_paid_info[0]["service_year"] = $wk_yy;
			$arr_paid_info[0]["service_mon"] = $wk_mm;
		}
		else {
			//4月1日基準、採用日基準
			if ($timecard_bean->closing_paid_holiday == "1" ||
					$timecard_bean->closing_paid_holiday == "3" ) {
				$wk_month = ($timecard_bean->criteria_months == "1") ? 3 : 6;
				//入職日のｎヵ月後
				$wk_join_year = substr($emp_join, 0, 4);
				$emp_join_mon = substr($emp_join, 4, 2);
				$wk_join_mon = intval($emp_join_mon) + $wk_month;
				$wk_join_day = intval(substr($emp_join, 6, 2));

				if ($wk_join_mon > 12) {
					$wk_join_year++;
					$wk_join_mon -= 12;
				}
				//月末確認
				if ($wk_join_day > 28) {
					$wk_max_day = days_in_month($wk_join_year, $wk_join_mon);
					if ($wk_join_day > $wk_max_day) {
						$wk_join_day = $wk_max_day;
					}
				}

				$arr_paid_info[0]["year"] = $wk_join_year;
				$arr_paid_info[0]["paid_hol_add_mmdd"] = sprintf("%02d%02d", $wk_join_mon, $wk_join_day);;

				$wk_yy = 0;
				$wk_mm = $wk_month;
			} else {
				//1月1日基準
				//検索の有休付与日が未設定の場合
				if (substr($arg_add_date, 0, 3) == "---") {
					$wk_year = date("Y");
					$arr_paid_info[0]["paid_hol_add_mmdd"] = substr($arg_add_date, 3, 4);
				} else {
					$wk_year = substr($arg_add_date, 0, 4);
					$arr_paid_info[0]["paid_hol_add_mmdd"] = substr($arg_add_date, 4, 4);
				}
				$arr_paid_info[0]["year"] = $wk_year;

				$wk_yy = "";
				$wk_mm = "";
				if ($emp_join > 0) {
					$wk_end_date = $wk_year.$arr_paid_info[0]["paid_hol_add_mmdd"];
					$len_service = get_length_of_service($emp_join, $wk_end_date);
					list($wk_yy, $wk_mm) = split("/", $len_service);
				}
			}
			$arr_paid_info[0]["service_year"] = $wk_yy;
			$arr_paid_info[0]["service_mon"] = $wk_mm;
		}
	}
	$wk_add_days = get_add_day($con, $fname, $arr_paid_hol, $arg_tbl_id, $wk_yy, $wk_mm, $timecard_bean);
	$arr_paid_info[0]["days2"] = $wk_add_days;
	$num = 1;
	$data_cnt = 0;
}

//移行データがある場合、繰越数等を設定
$sql = "select a.* from timecard_paid_hol_import a";
$cond = "where a.emp_personal_id = '$emp_personal_id' ";
$sel=select_from_table($con,$sql,$cond,$fname);
if($sel==0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$import_cnt = pg_num_rows($sel);
if ($import_cnt > 0) {
	$tmp_last_add_date = pg_fetch_result($sel, 0, "last_add_date"); //移行データの前回付与日 y/m/d形式
	list($wk_import_add_year, $wk_month, $wk_day) = split("/", $tmp_last_add_date);
	$last_add_date = sprintf("%04d%02d%02d", $wk_import_add_year, $wk_month, $wk_day); //yyyymmdd形式
	$tmp_last_use = pg_fetch_result($sel, 0, "last_use"); //前年使用
	$tmp_last_add_date = pg_fetch_result($sel, 0, "last_add_date"); //前回付与日
	$tmp_last_carry = pg_fetch_result($sel, 0, "last_carry"); //前年繰越
	$tmp_last_add = pg_fetch_result($sel, 0, "last_add"); //前年付与
	$tmp_curr_use = pg_fetch_result($sel, 0, "curr_use"); //当年使用
	$tmp_curr_carry = pg_fetch_result($sel, 0, "curr_carry"); //当年繰越
	$tmp_curr_add = pg_fetch_result($sel, 0, "curr_add"); //当年付与
	$tmp_curr_remain = pg_fetch_result($sel, 0, "curr_remain"); //当年残
	$tmp_data_migration_date = pg_fetch_result($sel, 0, "data_migration_date"); //移行日
	$tmp_tbl_kind = pg_fetch_result($sel, 0, "tbl_kind"); //テーブル種別
}
//時間有休移行データ確認
$import_hour_cnt = 0;
if ($paid_hol_hour_flag == "t") {
    //時間有休移行データがある場合、繰越数等を設定
    $sql = "select a.* from timecard_paid_hol_hour_import a";
    $cond = "where a.emp_personal_id = '$emp_personal_id' ";
    $sel=select_from_table($con,$sql,$cond,$fname);
    if($sel==0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    $import_hour_cnt = pg_num_rows($sel);
    if ($import_hour_cnt > 0) {
        $tmp_hol_hour_data_migration_date = pg_fetch_result($sel, 0, "data_migration_date"); //移行日
        $tmp_hol_hour_last_use = pg_fetch_result($sel, 0, "last_use"); //前年使用
        $tmp_hol_hour_last_carry = pg_fetch_result($sel, 0, "last_carry"); //前年繰越
        $tmp_hol_hour_curr_use = pg_fetch_result($sel, 0, "curr_use"); //当年使用
        $tmp_hol_hour_curr_carry = pg_fetch_result($sel, 0, "curr_carry"); //当年繰越
    }
}
//付与年月日を配列に設定し、有休使用日数をカウントする
for ($i=0; $i<$num; $i++) {
	$wk_year = $arr_paid_info[$i]["year"];
	$hidden_paid_hol_add_mmdd = $arr_paid_info[$i]["paid_hol_add_mmdd"];
	//移行データがある場合
	if ($import_cnt > 0 && $wk_year == $wk_import_add_year) {
		//付与データの付与月日がない場合、移行データの付与年月日を使用
		if ($arr_paid_info[$i]["paid_hol_add_mmdd"] == "") {
			$arr_paid_info[$i]["paid_hol_add_mmdd"] = substr($last_add_date, 4, 4);
		}
		//表名称が未設定の場合、移行データのテーブル種別から変換
		if ($arr_paid_info[$i]["paid_hol_tbl_id"] == "") {
			$tbl_id_name = mb_substr($tmp_tbl_kind, 1);
			$wk_tbl_id = "";
			for ($j=0; $j<8; $j++) {
				$wk_id = $j+1;
				$chk_name = $atdbk_common_class->get_paid_hol_tbl_id_name($wk_id);
				if ($tbl_id_name == $chk_name) {
					$wk_tbl_id = $wk_id;
					break;
				}
			}
			$arr_paid_info[$i]["paid_hol_tbl_id"] = $wk_tbl_id;
		}
	}
	$wk_paid_hol_add_mmdd = $arr_paid_info[$i]["paid_hol_add_mmdd"];
	if ($wk_paid_hol_add_mmdd > 0) {
		$wk_end_date = $wk_year.$wk_paid_hol_add_mmdd;
	} else {
		//設定の有休付与日1月1日
		if ($timecard_bean->closing_paid_holiday == "2") {
			$wk_end_date = $wk_year."0101";
		} else {
			$wk_end_date = $wk_year."0401";
		}
	}
	$arr_paid_info[$i]["paid_hol_add_date"] = $wk_end_date;
}
//所定労働時間
//$day1_time = $timecard_common_class->get_calendarname_day1_time();
//$specified_time = $obj_hol_hour->get_specified_time($emp_id, $day1_time);
//$specified_hour = $specified_time / 60;
for ($i=0; $i<$num; $i++) {
	$start_date = $arr_paid_info[$i]["paid_hol_add_date"];
	//最終行
	if ($i == $num - 1) {
		$wk_yyyy = intval(substr($start_date, 0, 4))+1;
		$end_date = $wk_yyyy.substr($start_date, 4, 4);
	} else {
		$end_date = $arr_paid_info[$i+1]["paid_hol_add_date"];
	}
	$end_date = last_date($end_date);
	//期間終了を設定
	$arr_paid_info[$i]["paid_hol_end_date"] = $end_date;
	//最終行は呼出元の有休繰越・付与日数更画面の新有休残締め日までで年休使用数を計算。付与年月日指定時（新有休残締め日未設定）は1年間で計算。20110406
	//if ($i == $num - 1) {
	//	if ($select_close_date != "" && $select_close_date != "---") {
	//		$end_date = $select_close_date;
	//	}
	//}
	$wk_cnt = $timecard_common_class->get_nenkyu_siyo($emp_id, $start_date, $end_date);
    if ($paid_hol_hour_flag == "t") {
        //時間有休合計取得
        $paid_hol_hour_total = $obj_hol_hour->get_paid_hol_day($emp_id, $start_date, $end_date);
        $arr_paid_info[$i]["nenkyu_siyo_hour"] = $paid_hol_hour_total / 60;
        $arr_paid_info[$i]["nenkyu_siyo_minute"] = $paid_hol_hour_total;
    }
	$arr_paid_info[$i]["nenkyu_siyo"] = $wk_cnt;
}

//移行データがある場合、繰越数等を設定
if ($import_cnt > 0) {
	for ($i=0; $i<$num; $i++) {
		$start_date = $arr_paid_info[$i]["paid_hol_add_date"];
		$end_date = $arr_paid_info[$i]["paid_hol_end_date"];
		//当年範囲確認
		if ($start_date <= $last_add_date &&
				$last_add_date <= $end_date) {
			//当年繰越が未設定の場合、移行データから設定
			if ($arr_paid_info[$i]["days1"] == "") {
				$arr_paid_info[$i]["days1"] = $tmp_curr_carry; //当年繰越
			}
			//当年付与が未設定の場合、移行データから設定
			if ($arr_paid_info[$i]["days2"] == "") {
				$arr_paid_info[$i]["days2"] = $tmp_curr_add; //当年付与
			}
			//移行日の後の使用数
			$tmp_data_migration_date = next_date($tmp_data_migration_date); //20110414 移行日の翌日からとする
			$wk_use = $timecard_common_class->get_nenkyu_siyo($emp_id, $tmp_data_migration_date, $end_date);
			if ($paid_hol_hour_flag == "t") {
				//時間有休合計取得
                //時間有休移行データがない場合
                if ($import_hour_cnt == 0) {
                    $paid_hol_hour_total = $obj_hol_hour->get_paid_hol_day($emp_id, $tmp_data_migration_date, $end_date);
                    $arr_paid_info[$i]["nenkyu_siyo_hour"] = $paid_hol_hour_total / 60;
                    $arr_paid_info[$i]["nenkyu_siyo_minute"] = $paid_hol_hour_total;
                }
            }
			$tmp_curr_use += $wk_use;
			$arr_paid_info[$i]["nenkyu_siyo"] = $tmp_curr_use; //当年使用

			//前年がある場合
			if ($i>0) {
				$arr_paid_info[$i-1]["days1"] = $tmp_last_carry; //前年繰越
				$arr_paid_info[$i-1]["days2"] = $tmp_last_add; //前年付与
				$arr_paid_info[$i-1]["nenkyu_siyo"] = $tmp_last_use; //前年使用
			}
            break;
        }
	}
}

//時間有休移行データがある場合、繰越数等を設定
if ($import_hour_cnt > 0) {
    for ($i=0; $i<$num; $i++) {
		$start_date = $arr_paid_info[$i]["paid_hol_add_date"];
		$end_date = $arr_paid_info[$i]["paid_hol_end_date"];
		//当年範囲確認
        if ($start_date <= $tmp_hol_hour_data_migration_date &&
                $tmp_hol_hour_data_migration_date <= $end_date) {
            //当年繰越が未設定の場合、移行データから設定
            if ($arr_paid_info[$i]["carry_time_minute"] == "") {
                $arr_paid_info[$i]["carry_time_minute"] = $obj_hol_hour->hh_or_hhmm_to_minute($tmp_hol_hour_curr_carry); //当年繰越 20141014 HH:MM対応
            }
            //移行日の後の使用数
            $tmp_hol_hour_data_migration_date = next_date($tmp_hol_hour_data_migration_date); //移行日の翌日からとする
            $wk_use = $obj_hol_hour->get_paid_hol_day($emp_id, $tmp_hol_hour_data_migration_date, $end_date, "");
            //20141014 HH:MM対応
            $tmp_hol_hour_curr_use_minute = $obj_hol_hour->hh_or_hhmm_to_minute($tmp_hol_hour_curr_use);
            $wk_nenkyu_siyo_minute = $tmp_hol_hour_curr_use_minute + $wk_use;
            $tmp_hol_hour_curr_use += ($wk_use / 60);
            $arr_paid_info[$i]["nenkyu_siyo_hour"] = $tmp_hol_hour_curr_use; //当年使用
            $arr_paid_info[$i]["nenkyu_siyo_minute"] = $wk_nenkyu_siyo_minute; //当年使用（分）
            //前年がある場合
            if ($i>0) {
                if ($arr_paid_info[$i-1]["carry_time_minute"] == "") {
                    $arr_paid_info[$i-1]["carry_time_minute"] = $obj_hol_hour->hh_or_hhmm_to_minute($tmp_hol_hour_last_carry); //前年繰越
                    $arr_paid_info[$i-1]["nenkyu_siyo_hour"] = $tmp_hol_hour_last_use; //前年使用
                    $arr_paid_info[$i-1]["nenkyu_siyo_minute"] = $obj_hol_hour->hh_or_hhmm_to_minute($tmp_hol_hour_last_use); //前年使用（分）
                }
            }
            break;
        }
    }
    //追加行の年に移行データがある場合
    //1年後の範囲を使用
    $wk_start_date = date("Ymd", strtotime("1 year",date_utils::to_timestamp_from_ymd($start_date)));
    $wk_end_date = date("Ymd", strtotime("1 year",date_utils::to_timestamp_from_ymd($end_date)));
    $add_hol_hour_migration_flg = false;
    if ($wk_start_date <= $tmp_hol_hour_data_migration_date &&
            $tmp_hol_hour_data_migration_date <= $wk_end_date) {
        if ($i>0) {
            if ($arr_paid_info[$i-1]["carry_time_minute"] == "") {
                $arr_paid_info[$i-1]["carry_time_minute"] = $obj_hol_hour->hh_or_hhmm_to_minute($tmp_hol_hour_last_carry); //前年繰越
                $arr_paid_info[$i-1]["nenkyu_siyo_hour"] = $tmp_hol_hour_last_use; //前年使用
                $arr_paid_info[$i-1]["nenkyu_siyo_minute"] = $obj_hol_hour->hh_or_hhmm_to_minute($tmp_hol_hour_last_use); //前年使用（分）
            }
        }
        $add_hol_hour_migration_flg = true;
    }
}
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript">
<!--
window.resizeTo(1120, 600);
//行追加
function addLine() {
	document.getElementById("add_line").style.display = "";
	document.getElementById("add_btn").disabled = true;
	document.mainform.add_flg.value = "1";
}
//行削除
function delLine() {
	document.getElementById("add_line").style.display = "none";
	document.getElementById("add_btn").disabled = false;
	document.mainform.add_flg.value = "";
}
//データ削除
function delData() {
	if (confirm('この行の有休付与情報を削除します。よろしいですか？')) {
		document.mainform.del_flg.value = "1";
		document.mainform.submit();
	}
}
//入力チェック
function checkInput() {


	days1 = document.getElementById("days1").value;
	days2 = document.getElementById("days2").value;
	if (days1 != "" && !days1.match(/^\d$|^\d+\.?\d+$/g)) {
		alert('当年繰越には半角数字を入力してください。');
		return false;
	}
	if (days2 != "" && !days2.match(/^\d$|^\d+\.?\d+$/g)) {
		alert('当年付与には半角数字を入力してください。');
		return false;
	}
	adjust_day = document.getElementById("adjust_day").value;
	if (adjust_day != "" && !adjust_day.match(/^-?\d$|^-?\d+\.?\d+$/g)) {
		alert('調整日数には半角数字を入力してください。');
		return false;
	}

	//行が追加されている場合
	if (document.getElementById("add_flg").value == "1") {

		if (document.getElementById("select_year").value == document.getElementById("select_year2").value &&
			document.getElementById("select_mon").value == document.getElementById("select_mon2").value &&
			document.getElementById("select_day").value == document.getElementById("select_day2").value) {
			alert('付与年月日には同じ日を指定できません。');
			return false;
		}

		days1_2 = document.getElementById("days1_2").value;
		days2_2 = document.getElementById("days2_2").value;
		if (days1_2 != "" && !days1_2.match(/^\d$|^\d+\.?\d+$/g)) {
			alert('当年繰越には半角数字を入力してください。');
			return false;
		}
		if (days2_2 != "" && !days2_2.match(/^\d$|^\d+\.?\d+$/g)) {
			alert('当年付与には半角数字を入力してください。');
			return false;
		}
		adjust_day2 = document.getElementById("adjust_day2").value;
		if (adjust_day2 != "" && !adjust_day2.match(/^-?\d$|^-?\d+\.?\d+$/g)) {
			alert('調整日数には半角数字を入力してください。');
			return false;
		}
	}
	return true;
}
//数値変更再計算
function changeDay() {
	ret = checkInput();
	if (ret == false) {
		return;
	}

	days1 = document.getElementById("days1").value;
	days2 = document.getElementById("days2").value;
	prev_use = document.getElementById("prev_use").value;

	document.getElementById("cell_days1").innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">'+days1+'</font>';
	document.getElementById("cell_days2").innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">'+days2+'</font>';

	if (days1 == "") days1 = "0";
	if (days2 == "") days2 = "0";
	if (prev_use == "") prev_use = "0";
	adjust = document.getElementById("adjust_day").value;
	document.getElementById("cell_adjust_day").innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">'+adjust+'</font>';
	if (adjust == "") adjust = "0";

	prev_remain = eval(days1) + eval(days2) + eval(adjust) - eval(prev_use);

	document.getElementById("cell_remain").innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">'+prev_remain+'</font>';

	//当年繰越、 前年残<=前年付与の場合前年残、それ以外は前年付与
	days1_2 = (prev_remain <= days2) ? prev_remain : days2;
	document.getElementById("days1_2").value = days1_2;
}

<?
//配列、付与情報
echo("var arr_paid_hol = new Array(9);\n");
for ($i=1; $i<=8; $i++) {
	echo("arr_paid_hol[$i] = new Array(9);\n");
	for ($j=1; $j<=8; $j++) {
        //未設定時の対応、""は0とする、8番目が""の時は7番目の数値を使用 20120329
        $wk_days = $arr_paid_hol[$i][$j];
        if ($j == 8 && $wk_days == "") {
            $wk_days = $arr_paid_hol[$i][7];
        }
        if ($wk_days == "") {
            $wk_days = "0";
        }
        echo("arr_paid_hol[$i][$j] = {$wk_days};\n");
	}
}

//有休付与日が採用日の場合
if ($timecard_bean->closing_paid_holiday == "3") {
	//期間確認 yymm形式
	if ($timecard_bean->criteria_months == "2") {
        echo("var arr_hikaku_tbl = new Array('0706', '0606', '0506', '0406', '0306', '0206', '0106', '0006');\n");
	} else {
        echo("var arr_hikaku_tbl = new Array('0700', '0600', '0500', '0400', '0300', '0200', '0100', '0003');\n");
	}
}

echo ("var closing_paid_holiday = '{$timecard_bean->closing_paid_holiday}';\n");
echo ("var criteria_months = '{$timecard_bean->criteria_months}';\n");
?>

//有休表、勤続年数変更
function changeIdYear(line) {

	varname = (line == 1) ? 'paid_hol_tbl_id' : 'paid_hol_tbl_id2';
	tbl_id = document.getElementById(varname).value;

	if (tbl_id == "") {
		return;
	}

	varname = (line == 1) ? 'service_year' : 'service_year2';
	service_year = document.getElementById(varname).value;
	varname = (line == 1) ? 'service_mon' : 'service_mon2';
	service_mon = document.getElementById(varname).value;

	if (service_year == "-") {
		service_year = 0;
	}
	if (service_mon == "-") {
		service_mon = '00';
	}
	if (closing_paid_holiday == '3') {
		service_year = (service_year < 10) ? '0'+service_year : service_year;
		service_yymm = ""+service_year+service_mon;
		if (criteria_months == '1' && service_yymm < '0003') {
			varname = (line == 1) ? 'days2' : 'days2_2';
			document.getElementById(varname).value = '';
			return;
		}
		if (criteria_months == '2' && service_yymm < '0006') {
			varname = (line == 1) ? 'days2' : 'days2_2';
			document.getElementById(varname).value = '';
			return;
		}
		//7年後から6ヶ月まで勤続の期間を確認
		for (wk_idx=0; wk_idx<8; wk_idx++) {

			wk_hikaku_kikan = arr_hikaku_tbl[wk_idx];
			if (service_yymm >= wk_hikaku_kikan) {
				wk_kinzoku_id = 8 - wk_idx;
				break;
			}
		}
		if (wk_kinzoku_id <= 0) {
			wk_kinzoku_id = 1;
		}

	} else {
		wk_kinzoku_id = eval(service_year) + 1;
	}
	if (wk_kinzoku_id > 8) {
		wk_kinzoku_id = 8;
	}
	add_cnt = arr_paid_hol[tbl_id][wk_kinzoku_id];
	varname = (line == 1) ? 'days2' : 'days2_2';
	document.getElementById(varname).value = add_cnt;

	changeDay();
}

//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
		<tr height="32" bgcolor="#5279a5">
		<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>有休繰越・付与修正</b></font></td>
		<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

<table width="500" border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="100" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
職員ID
</font></td>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? echo($emp_personal_id); ?>
</font></td>
<td width="100" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
氏名
</font></td>
<td width="200"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? echo($emp_name); ?>
</font></td>
</tr>
<tr>
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
所属
</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? echo($str_orgname); ?>
</font></td>
</tr>
<tr>
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
採用年月日
</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($emp_join > 0) {
	echo($emp_join_str);
}
 ?>
</font></td>
</tr>
</table>
<br>
<form name="mainform" action="work_admin_paid_edit.php" method="post">
<table width="100%" border="0" cellspacing="1" cellpadding="1" class="list">
<?
//時間有休時、見出しを2段表示
if ($paid_hol_hour_flag == "t") {
    echo("<tr bgcolor=\"#f6f9ff\">");
    echo("<td align=\"center\" colspan=\"3\"></td>\n");
    echo("<td align=\"center\" colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">前年繰越</font></td>\n");
    echo("<td align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">前年<br>付与</font></td>\n");
    echo("<td align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">前年<br>調整</font></td>\n");
    echo("<td align=\"center\" colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">前年取得</font></td>\n");
    echo("<td align=\"center\" colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">前年残</font></td>\n");
    echo("<td align=\"center\" colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">当年繰越</font></td>\n");
    echo("<td align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">当年<br>付与</font></td>\n");
    echo("<td align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">当年<br>調整</font></td>\n");
    echo("<td align=\"center\" colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">当年取得</font></td>\n");
    echo("<td align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"></font></td>\n");
    echo("</tr>\n");
}
echo("<tr bgcolor=\"#f6f9ff\">");
echo("<td align=\"center\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">有休表</font></td>\n");
echo("<td align=\"center\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">付与年月日</font></td>\n");
echo("<td align=\"center\" nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">勤続年数</font></td>\n");
$wk_str = ($paid_hol_hour_flag == "t") ? "日数" : "前年<br>繰越";
echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_str</font></td>\n");
if ($paid_hol_hour_flag == "t") {
    echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">時間</font></td>\n");
}
if ($paid_hol_hour_flag == "f") {
    echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">前年<br>付与</font></td>\n");
}
if ($paid_hol_hour_flag == "f") {
    echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">前年<br>調整</font></td>\n");
}
$wk_str = ($paid_hol_hour_flag == "t") ? "日数" : "前年<br>取得";
echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_str</font></td>\n");
if ($paid_hol_hour_flag == "t") {
    echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">時間</font></td>\n");
}
$wk_str = ($paid_hol_hour_flag == "t") ? "日数" : "前年<br>残";
echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_str</font></td>\n");
if ($paid_hol_hour_flag == "t") {
    echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">時間</font></td>\n");
}
$wk_str = ($paid_hol_hour_flag == "t") ? "日数" : "当年<br>繰越";
echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_str</font></td>\n");
if ($paid_hol_hour_flag == "t") {
    echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">時間</font></td>\n");
}
if ($paid_hol_hour_flag == "f") {
    echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">当年<br>付与</font></td>\n");
}
if ($paid_hol_hour_flag == "f") {
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">調整<br>日数</font></td>\n");
}
$wk_str = ($paid_hol_hour_flag == "t") ? "日数" : "当年<br>取得";
echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_str</font></td>\n");
if ($paid_hol_hour_flag == "t") {
    echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">時間</font></td>\n");
}
if ($paid_hol_hour_flag == "f") {
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"></font></td>\n");
}
echo("</tr>\n");


//繰返し
for ($i=0; $i<$num; $i++) {
    $start_date = $arr_paid_info[$i]["paid_hol_add_date"];
    // 所定労働時間履歴 20121129
    $arr_timehist = $calendar_name_class->get_calendarname_time($start_date);
    $day1_time = $arr_timehist["day1_time"];
    $specified_time = $obj_hol_hour->get_specified_time2($paid_specified_time, $day1_time);
    $specified_hour = $specified_time / 60;

    //表示
	$wk_paid_hol_tbl_id = $arr_paid_info[$i]["paid_hol_tbl_id"];
	$wk_tbl_id_name = $atdbk_common_class->get_paid_hol_tbl_id_name($wk_paid_hol_tbl_id);

	$wk_year = $arr_paid_info[$i]["year"];
	$wk_paid_hol_add_mmdd = $arr_paid_info[$i]["paid_hol_add_mmdd"];
	$wk_add_ymd = $wk_year."年";
	if ($wk_paid_hol_add_mmdd > 0) {
		$wk_add_ymd .= intval(substr($wk_paid_hol_add_mmdd, 0, 2))."月".intval(substr($wk_paid_hol_add_mmdd, 2, 2))."日";
		$wk_end_date = $wk_year.$wk_paid_hol_add_mmdd;
	} else {
		//登録データの有休付与日が未設定（20101129の改修以前のデータ）
		//設定の有休付与日1月1日
		if ($timecard_bean->closing_paid_holiday == "2") {
			$wk_add_ymd .= "1月1日";
			$wk_end_date = $wk_year."0101";
		} else {
			$wk_add_ymd .= "4月1日";
			$wk_end_date = $wk_year."0401";
		}
	}
	$wk_yy = $arr_paid_info[$i]["service_year"];
	$wk_mm = $arr_paid_info[$i]["service_mon"];
	$wk_service = "";
	//勤続年数が未設定
	if ($wk_yy == 0 && $wk_mm == 0) {
		if ($emp_join > 0) {
			$len_service = get_length_of_service($emp_join, $wk_end_date);
			list($wk_yy, $wk_mm) = split("/", $len_service);
		}
	}
	if ($wk_yy > 0) {
		$wk_service .= $wk_yy."年";
	}
	if ($wk_mm > 0) {
		$wk_service .= $wk_mm."ヶ月";
	}

	$wk_days1 = $arr_paid_info[$i]["days1"];
	$wk_days2 = $arr_paid_info[$i]["days2"];
	$wk_adjust_day = $arr_paid_info[$i]["adjust_day"];
	//取得
	$wk_curr_use = $arr_paid_info[$i]["nenkyu_siyo"];
	//時間有休
    $wk_carry_time_minute = $arr_paid_info[$i]["carry_time_minute"];
    if ($wk_carry_time_minute == "") {
        $wk_days1_hour = "";
    }
    else {
        $wk_days1_hour = $wk_carry_time_minute / 60;
    }
    //時間が大きい場合、日数換算
    //日数換算
    if ($paid_hol_hour_flag == "t" && $wk_days1_hour > $specified_hour) {
        $wk_day = intval($wk_days1_hour / $specified_hour);
        $wk_days1 += $wk_day;
        $wk_days1_hour -= ($specified_hour * $wk_day);
    }
    //繰越方法
    if ($wk_days1_hour > 0) {
        switch ($obj_hol_hour->paid_hol_hour_carry_flg) {
            case "2": //そのまま繰越
                break;
            case "3": //切捨て
                $wk_days1_hour = 0;
                //追加行の表示のため、情報更新
                $arr_paid_info[$i]["carry_time_minute"] = 0;
                break;
            case "4": //半日切上げ
                $half_time = $specified_time / 2;
                //半日時間（分）
                if ($wk_days1_hour * 60 > $half_time) {
                    $wk_days1 += 1;
                    $wk_days1_hour = 0;
                }
                else if ($wk_days1_hour * 60 > 0) {
                    $wk_days1 += 0.5;
                    $wk_days1_hour = 0;
                }
                $arr_paid_info[$i]["days1"] = $wk_days1;
                $arr_paid_info[$i]["carry_time_minute"] = 0;
                break;
            case "1": //切上げ
            default:
                $wk_days1 += 1;
                $wk_days1_hour = 0;
                //追加行の表示のため、情報更新
                $arr_paid_info[$i]["days1"] = $wk_days1;
                $arr_paid_info[$i]["carry_time_minute"] = 0;
                break;

        }
    }
    $wk_curr_use_hour = $arr_paid_info[$i]["nenkyu_siyo_hour"];
    //1分単位で時間有休取得の場合 20140805
    $wk_curr_use_hhmm = minute_to_hmm($arr_paid_info[$i]["nenkyu_siyo_minute"]);


	echo("<tr>\n");
	//最新行の場合
	if ($i == $num-1) {
		//初回
		if ($update_flg == "") {
			$paid_hol_tbl_id = $wk_paid_hol_tbl_id;
			$select_year = $wk_year;
			$select_mon = intval(substr($wk_end_date, 4, 2));
			$select_day = intval(substr($wk_end_date, 6, 2));
			$service_year = $wk_yy;
			$service_mon = $wk_mm;
			$days1 = $wk_days1;
			$days2 = $wk_days2;
			$adjust_day = $wk_adjust_day;
		}
		echo("<td align=\"center\">");
		echo("<select name=\"paid_hol_tbl_id\" id=\"paid_hol_tbl_id\" onChange=\"changeIdYear(1);\">");

        for ($idx = 0; $idx < 8; $idx++) {
            //日数合計を確認し0ならば非表示、0以外は表示 20120329
            $wk_paid_hol_tbl_id = $arr_paid_hol_tbl_id[$idx];
            if ($arr_tbl_id_disp_flg[$wk_paid_hol_tbl_id] == false) {
                continue;
            }
            echo("<option value=\"{$wk_paid_hol_tbl_id}\"");
            if ($paid_hol_tbl_id == $wk_paid_hol_tbl_id) {
                echo(" selected");
            }
            //名称変更 常勤 週ｎ日
            $wk_tbl_id_name = $atdbk_common_class->get_paid_hol_tbl_id_name($wk_paid_hol_tbl_id);
            echo(">{$wk_tbl_id_name}");
            echo("</option>");
        }

		//選択肢に「なし」を追加
		$selected = ($paid_hol_tbl_id == "") ? " selected" : "";
		echo("<option value=\"\" $selected>なし</option>");
		echo("</select>");
		echo("</td>");
		//付与年月日
		if ($wk_year == "") {
			$wk_year = date("Y");
		}
		$wk_start = min($wk_year, date("Y")-1);
		$wk_end = max($wk_year+1, date("Y")+1);
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("<select name=\"select_year\" id=\"select_year\">");
		show_select_years_span($wk_start, $wk_end, $select_year, $asc = false);
		echo("</select>");
		echo("年");
		echo("<select name=\"select_mon\" id=\"select_mon\">");
		show_select_months($select_mon, false);
		echo("</select>");
		echo("月");
		echo("<select name=\"select_day\" id=\"select_day\">");
		show_select_days($select_day, false);
		echo("</select>");
		echo("日");
		echo("</font></td>\n");
		//勤続年数
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("<select name=\"service_year\" id=\"service_year\" onChange=\"changeIdYear(1);\">");
		echo("<option value=\"-\">\n");
		show_select_years_span(1, 50, $service_year, $asc = true);
		echo("</select>");
		echo("年");
		echo("<select name=\"service_mon\" id=\"service_mon\" onChange=\"changeIdYear(1);\">");
		show_select_months($service_mon, true);
		echo("</select>");
		echo("ヶ月");

		echo("</font></td>\n");
	} else {
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_tbl_id_name</font></td>\n");
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_add_ymd</font></td>\n");
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_service</font></td>\n");
	}
	//前年繰越、付与、調整
	if ($i > 0) { //1行目以外
		$prev_days1 =	$arr_paid_info[$i-1]["days1"];
		$prev_days2 =	$arr_paid_info[$i-1]["days2"];
		$prev_adjust_day = $arr_paid_info[$i-1]["adjust_day"];
		$prev_use = $arr_paid_info[$i-1]["nenkyu_siyo"];
		$prev_remain = $prev_days1+$prev_days2+$prev_adjust_day-$prev_use;
        //時間有休
        $wk_carry_time_minute = $arr_paid_info[$i-1]["carry_time_minute"];
        $prev_days1_hour = $wk_carry_time_minute / 60;

        $prev_use_hour = $arr_paid_info[$i-1]["nenkyu_siyo_hour"];
        $prev_use_hhmm = minute_to_hmm($arr_paid_info[$i-1]["nenkyu_siyo_minute"]);
        $wk_prev_use_hour = $obj_hol_hour->hh_or_hhmm_to_minute($prev_use_hhmm) / 60; //HH:MM形式対応 20141024
        $kurikosi_fuyo_day = $prev_days1 + $prev_days2 + $prev_adjust_day;
        //前年残
        list($prev_remain, $prev_remain_hour, $wk_day_kansan ) = day_hour_subtract($kurikosi_fuyo_day, $prev_days1_hour, $prev_use, $wk_prev_use_hour, $specified_hour);

    } else {
		//1行目で移行データがある場合
		if ($last_add_date == $arr_paid_info[$i]["year"].$arr_paid_info[$i]["paid_hol_add_mmdd"]) {
			$prev_days1 = $tmp_last_carry;
			$prev_days2 = $tmp_last_add;
			$prev_use = $tmp_last_use;
			$prev_remain = $prev_days1+$prev_days2-$prev_use;
			$prev_adjust_day = "";

        } else {
			$prev_days1 = "";
			$prev_days2 = "";
			$prev_use = "";
			$prev_remain = "";
			$prev_adjust_day = "";

        }
     	$start_date = $arr_paid_info[$i]["paid_hol_add_date"];
		$end_date = $arr_paid_info[$i]["paid_hol_end_date"];
		//当年範囲確認
        if ($start_date <= $tmp_hol_hour_data_migration_date &&
                $tmp_hol_hour_data_migration_date <= $end_date) {
            $prev_days1_hour = $tmp_hol_hour_last_carry;
            $prev_use_hour = $tmp_hol_hour_last_use;
            $wk_prev_days1_hour = $obj_hol_hour->hh_or_hhmm_to_minute($tmp_hol_hour_last_carry) / 60; //HH:MM形式対応 20141024
            $wk_prev_use_hour = $obj_hol_hour->hh_or_hhmm_to_minute($tmp_hol_hour_last_use) / 60; //HH:MM形式対応 20141024
        }
        else {
            $prev_days1_hour = "";
            $prev_use_hour = "";
            $wk_prev_days1_hour = 0;
            $wk_prev_use_hour = 0;
        }
        if ($prev_days1 != "" || $prev_days2 != "" || $prev_days1_hour != "" || $prev_use != "" || $prev_use_hour != "") {
            $kurikosi_fuyo_day = $prev_days1 + $prev_days2;
            //前年残
            list($prev_remain, $prev_remain_hour, $wk_day_kansan ) = day_hour_subtract($kurikosi_fuyo_day, $wk_prev_days1_hour, $prev_use, $wk_prev_use_hour, $specified_hour);
        }
    }
    //前年繰越
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$prev_days1</font></td>\n");
    //前年繰越時間
    if ($paid_hol_hour_flag == "t") {
        echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
            $prev_days1_hhmm = minute_to_hmm(round($obj_hol_hour->hh_or_hhmm_to_minute($prev_days1_hour)));
            echo($prev_days1_hhmm);
        }
        else {
            echo($prev_days1_hour);
        }
        echo("</font></td>\n");
    }
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$prev_days2</font></td>\n");
    //前年調整
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$prev_adjust_day</font></td>\n");
	echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$prev_use</font></td>\n");
    //前年取得時間
    if ($paid_hol_hour_flag == "t") {
        echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
            echo($prev_use_hhmm);
        }
        else {
            echo($prev_use_hour);
        }
        echo("</font></td>\n");
    }
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$prev_remain</font></td>\n");
    //前年残時間
    if ($paid_hol_hour_flag == "t") {
        echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
            $prev_remain_hhmm = minute_to_hmm(round($prev_remain_hour * 60));
            echo($prev_remain_hhmm);
        }
        else {
            echo($prev_remain_hour);
        }
        echo("</font></td>\n");
    }
    //当年繰越、付与、調整
	//最新行の場合
	if ($i == $num-1) {
		echo("<td align=\"center\"><input type=\"text\" name=\"days1\" id=\"days1\" value=\"$days1\" size=\"5\" maxlength=\"5\" style=\"text-align:right\" style=\"ime-mode:inactive;\" onchange=\"changeDay();\"></td>\n");
        //当年繰越時間
        if ($paid_hol_hour_flag == "t") {
            echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
            if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
                $wk_days1_hhmm = minute_to_hmm(round($wk_days1_hour * 60));
                echo($wk_days1_hhmm);
            }
            else {
                echo($wk_days1_hour);
            }
            echo("</font></td>\n");

            echo("<input type=\"hidden\" name=\"days1_hour\" id=\"days1_hour\" value=\"$wk_days1_hour\">\n");
        }
        echo("<td align=\"center\"><input type=\"text\" name=\"days2\" id=\"days2\" value=\"$days2\" size=\"5\" maxlength=\"5\" style=\"text-align:right\" style=\"ime-mode:inactive;\" onchange=\"changeDay();\"></td>\n");
        //当年調整
        echo("<td align=\"center\"><input type=\"text\" name=\"adjust_day\" id=\"adjust_day\" value=\"$adjust_day\" size=\"5\" maxlength=\"5\" style=\"text-align:right\" style=\"ime-mode:inactive;\" onchange=\"changeDay();\"></td>\n");
		echo("<input type=\"hidden\" name=\"year\" id=\"year\" value=\"$wk_year\">\n");
		echo("<input type=\"hidden\" name=\"paid_hol_add_mmdd\" id=\"paid_hol_add_mmdd\" value=\"$hidden_paid_hol_add_mmdd\">\n");
	} else {
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_days1</font></td>\n");
        //当年繰越時間
        if ($paid_hol_hour_flag == "t") {
            echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
            if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
                $wk_days1_hhmm = minute_to_hmm(round($wk_days1_hour * 60));
                echo($wk_days1_hhmm);
            }
            else {
                echo($wk_days1_hour);
            }
            echo("</font></td>\n");
        }
        echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_days2</font></td>\n");
        //当年調整
//        if ($paid_hol_hour_flag == "f") {
            echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_adjust_day</font></td>\n");
//        }
	}
	//当年取得
	echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_curr_use</font></td>\n");
    //当年取得時間
    if ($paid_hol_hour_flag == "t") {
        echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
            echo($wk_curr_use_hhmm);
        }
        else {
            echo($wk_curr_use_hour);
        }
        echo("</font></td>\n");
    }
    echo("<td align=\"left\">");
	if ($i == $num-1) {
		echo("<input type=\"button\" id=\"add_btn\" value=\"追加\" onclick=\"addLine();\">");
		echo("&nbsp;<input type=\"button\" id=\"del2_btn\" value=\"削除\" onclick=\"delData();\">");
	}
	echo("</td>\n");
	echo("</tr>\n");
}
//初期表示時、追加行の情報設定
if ($update_flg == "") {
	$paid_hol_tbl_id2 = $paid_hol_tbl_id;
	//設定の有休付与日が4/1基準、
	//勤続年数が3ヶ月か6ヶ月前回付与データがある職員
	if ($timecard_bean->closing_paid_holiday == "1" &&
			$wk_yy == 0 &&
			($wk_mm == 3 || $wk_mm == 6)
		) {
		$select_year2 = $wk_year;
		$select_mon2 = intval(substr($wk_paid_hol_add_mmdd, 0, 2));
		if ($select_mon2 > 4) {
			$select_year2++;
		}
		$select_mon2 = 4;
		$select_day2 = intval(substr($wk_paid_hol_add_mmdd, 2, 2));

		$service_year2 = 1;
		$service_mon2 = 0;
	} else {
		//前回が3ヶ月なら1年にする
		if ($wk_yy == 0 && $wk_mm == 3) {
			$select_mon2 = intval(substr($wk_paid_hol_add_mmdd, 0, 2));
			$select_day2 = intval(substr($wk_paid_hol_add_mmdd, 2, 2));

			$select_mon2 += 9;
			if ($select_mon2 > 12) {
				$select_mon2 -= 12;
				$select_year2 = $wk_year+1;
			} else {
				$select_year2 = $wk_year;
			}

			$service_year2 = 1;
			$service_mon2 = 0;
		} else {
			$select_year2 = $wk_year+1;
			$select_mon2 = intval(substr($wk_paid_hol_add_mmdd, 0, 2));
			$select_day2 = intval(substr($wk_paid_hol_add_mmdd, 2, 2));
			$service_year2 = $wk_yy+1;
			$service_mon2 = $wk_mm;
		}
	}

	$days2_2 = get_add_day($con, $fname, $arr_paid_hol, $paid_hol_tbl_id2, $service_year2, $service_mon2, $timecard_bean);
	$adjust_day2 = "";
}

echo("<tr id=\"add_line\" style=\"display:none;\">\n");
echo("<td align=\"center\">");
echo("<select name=\"paid_hol_tbl_id2\" id=\"paid_hol_tbl_id2\" onChange=\"changeIdYear(2);\">");

for ($idx = 0; $idx < 8; $idx++) {
    $wk_paid_hol_tbl_id = $arr_paid_hol_tbl_id[$idx];
    if ($arr_tbl_id_disp_flg[$wk_paid_hol_tbl_id] == false) {
        continue;
    }
    echo("<option value=\"$wk_paid_hol_tbl_id\"");
    if ($paid_hol_tbl_id2 == $wk_paid_hol_tbl_id) {
		echo(" selected");
	}
	//名称変更 常勤 週ｎ日 20101117
    $wk_tbl_id_name = $atdbk_common_class->get_paid_hol_tbl_id_name($wk_paid_hol_tbl_id);
	echo(">{$wk_tbl_id_name}");
	echo("</option>");
}
//選択肢に「なし」を追加
$selected = ($paid_hol_tbl_id2 == "") ? " selected" : "";
echo("<option value=\"\" $selected>なし</option>");
echo("</select>");
echo("</td>");
//付与年月日
if ($wk_year == "") {
	$wk_year = date("Y");
}
$wk_start = min($wk_year, date("Y")-1);
$wk_end = max($wk_year+1, date("Y")+1);
echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("<select name=\"select_year2\" id=\"select_year2\">");
show_select_years_span($wk_start, $wk_end, $select_year2, $asc = false);
echo("</select>");
echo("年");
echo("<select name=\"select_mon2\" id=\"select_mon2\">");
show_select_months($select_mon2, false);
echo("</select>");
echo("月");
echo("<select name=\"select_day2\" id=\"select_day2\">");
show_select_days($select_day2, false);
echo("</select>");
echo("日");
echo("</font></td>\n");
//勤続年数
echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("<select name=\"service_year2\" id=\"service_year2\" onChange=\"changeIdYear(2);\">");
echo("<option value=\"-\">\n");
show_select_years_span(1, 50, $service_year2, $asc = true);
echo("</select>");
echo("年");
echo("<select name=\"service_mon2\" id=\"service_mon2\" onChange=\"changeIdYear(2);\">");
show_select_months($service_mon2, true);
echo("</select>");
echo("ヶ月");

echo("</font></td>\n");
//前年繰越、付与、調整
$prev_days1 =	$arr_paid_info[$i-1]["days1"];
$prev_days2 =	$arr_paid_info[$i-1]["days2"];
$prev_adjust_day = $arr_paid_info[$i-1]["adjust_day"];
$prev_use = $arr_paid_info[$i-1]["nenkyu_siyo"];
//$prev_remain = $prev_days1+$prev_days2+$prev_adjust_day-$prev_use;
//時間有休
$wk_carry_time_minute = $arr_paid_info[$i-1]["carry_time_minute"];
$prev_days1_hour = $wk_carry_time_minute / 60;
//日数換算
if ($paid_hol_hour_flag == "t" && $prev_days1_hour > $specified_hour) {
    $wk_day = intval($prev_days1_hour / $specified_hour);
    $prev_days1 += $wk_day;
    $prev_days1_hour -= ($specified_hour * $wk_day);
}

$prev_use_hour = $arr_paid_info[$i-1]["nenkyu_siyo_hour"];
$prev_use_hhmm = minute_to_hmm($arr_paid_info[$i-1]["nenkyu_siyo_minute"]);
$wk_prev_use_hour = $obj_hol_hour->hh_or_hhmm_to_minute($prev_use_hhmm) / 60; //HH:MM形式対応 20141024

$kurikosi_fuyo_day = $prev_days1 + $prev_days2 + $prev_adjust_day; //調整日数 20121106
//前年残
list($prev_remain, $prev_remain_hour, $wk_day_kansan ) = day_hour_subtract($kurikosi_fuyo_day, $prev_days1_hour, $prev_use, $wk_prev_use_hour, $specified_hour);

echo("<input type=\"hidden\" id=\"prev_days1\" name=\"prev_days1\" value=\"{$prev_days1}\">\n");
echo("<input type=\"hidden\" id=\"prev_days2\" name=\"prev_days2\" value=\"{$prev_days2}\">\n");
echo("<input type=\"hidden\" id=\"prev_adjust_day\" name=\"prev_adjust_day\" value=\"{$prev_adjust_day}\">\n");
echo("<input type=\"hidden\" id=\"prev_use\" name=\"prev_use\" value=\"{$prev_use}\">\n");
echo("<input type=\"hidden\" id=\"prev_remain\" name=\"prev_remain\" value=\"{$prev_remain}\">\n");

//前年繰越
echo("<td align=\"center\" id=\"cell_days1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$prev_days1</font></td>\n");
//前年繰越時間
if ($paid_hol_hour_flag == "t") {
        echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
    if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
        $prev_days1_hhmm = minute_to_hmm(round($prev_days1_hour * 60));
        echo($prev_days1_hhmm);
    }
    else {
        echo($prev_days1_hour);
    }
    echo("</font></td>\n");
}
//前年付与
echo("<td align=\"center\" id=\"cell_days2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$prev_days2</font></td>\n");
//前年調整
echo("<td align=\"center\" id=\"cell_adjust_day\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$prev_adjust_day</font></td>\n");

//前年取得
echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$prev_use</font></td>\n");
//前年取得時間
if ($paid_hol_hour_flag == "t") {
    echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
    if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
        echo($prev_use_hhmm);
    }
    else {
        echo($prev_use_hour);
    }
    echo("</font></td>\n");

}
//前年残
echo("<td align=\"center\" id=\"cell_remain\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$prev_remain</font></td>\n");
//前年残時間
if ($paid_hol_hour_flag == "t") {
        echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
    if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
        $wk_min = round($prev_remain_hour * 60);
        $prev_remain_hhmm = minute_to_hmm($wk_min);
        echo($prev_remain_hhmm);
    }
    else {
        echo($prev_remain_hour);
    }
    echo("</font></td>\n");
}
//繰越計算
//前年残<前年付与の場合、前年残
//時間有休残を考慮 20141120
$wk_day_kansan = $prev_remain + ($prev_remain_hour / $specified_hour);
if ($wk_day_kansan < $prev_days2) {
    $days1_2 = $prev_remain;
    $wk_carry_time = $prev_remain_hour;
}
//上記以外は前年付与（前々年分を除く）
else {
    $days1_2 = $prev_days2;
    $wk_carry_time = "";
}

//移行データがある場合
if ($add_hol_hour_migration_flg) {
    $wk_carry_time = $obj_hol_hour->hh_or_hhmm_to_minute($tmp_hol_hour_curr_carry) / 60; //20141015 HH:MM形式対応
    //日数換算
    if ($wk_carry_time > $specified_hour) {
        $wk_day = intval($wk_carry_time / $specified_hour);
        $days1_2 += $wk_day;
        $wk_carry_time -= ($specified_hour * $wk_day);
    }
    //前々年分は繰り越さない 20150501
	if ($days1_2 >= $prev_days2) {
	    $days1_2 = $prev_days2;
	    $wk_carry_time = "";
	}

//    $wk_use_time = $tmp_hol_hour_curr_use;
}
//else {
//    $wk_use_time = "";
//}
//繰越方法
if ($wk_carry_time > 0) {
    switch ($obj_hol_hour->paid_hol_hour_carry_flg) {
        case "2": //そのまま繰越
            break;
        case "3": //切捨て
            $wk_carry_time = 0;
            break;
        case "4": //半日切上げ
            //半日時間（分）
            $half_time = $specified_time / 2;
            //$wk_carry_time（時間換算）*60
            if ($wk_carry_time * 60 > $half_time) {
                $days1_2 += 1;
                $wk_carry_time = 0;
            }
            else if ($wk_carry_time * 60 > 0) {
                $days1_2 += 0.5;
                $wk_carry_time = 0;
            }
            break;
        case "1": //切上げ
        default:
            $days1_2 += 1;
            $wk_carry_time = 0;
            break;

    }
}
//当年繰越
echo("<td align=\"center\"><input type=\"text\" name=\"days1_2\" id=\"days1_2\" value=\"$days1_2\" size=\"5\" maxlength=\"5\" style=\"text-align:right\" style=\"ime-mode:inactive;\"></td>\n");
//当年繰越時間
if ($paid_hol_hour_flag == "t") {
    echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");

    if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
        $wk_carry_hhmm = minute_to_hmm(round($wk_carry_time * 60));
        echo($wk_carry_hhmm);
    }
    else {
        echo($wk_carry_time);
    }
    echo("</font></td>\n");
    echo("<input type=\"hidden\" name=\"days1_2_hour\" id=\"days1_2_hour\" value=\"$wk_carry_time\">\n");
}
//当年付与
echo("<td align=\"center\"><input type=\"text\" name=\"days2_2\" id=\"days2_2\" value=\"$days2_2\" size=\"5\" maxlength=\"5\" style=\"text-align:right\" style=\"ime-mode:inactive;\" onchange=\"changeDay();\"></td>\n");
//当年調整
echo("<td align=\"center\"><input type=\"text\" name=\"adjust_day2\" id=\"adjust_day2\" value=\"$adjust_day2\" size=\"5\" maxlength=\"5\" style=\"text-align:right\" style=\"ime-mode:inactive;\" onchange=\"changeDay();\"></td>\n");
//当年取得
//期間確認後、使用数取得
$wk_start_date = date("Ymd", strtotime("1 year",date_utils::to_timestamp_from_ymd($arr_paid_info[$num-1]["paid_hol_add_date"])));

//if ($select_close_date != "" && $select_close_date != "---") {
//    $wk_end_date = $select_close_date;
//}
//else {
    $wk_end_date = date("Ymd", strtotime("1 year",date_utils::to_timestamp_from_ymd($arr_paid_info[$num-1]["paid_hol_end_date"])));
//}
$wk_use = $timecard_common_class->get_nenkyu_siyo($emp_id, $wk_start_date, $wk_end_date);

echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_use</font></td>\n");
//当年取得時間
if ($paid_hol_hour_flag == "t") {
	$wk_use_time = $obj_hol_hour->get_paid_hol_day($emp_id, $wk_start_date, $wk_end_date, "");
    if ($obj_hol_hour->paid_hol_hour_unit_time == "1") {
		$wk_use_time = minute_to_hmm($wk_use_time);
	}
	else {
		$wk_use_time = $wk_use_time / 60;
	}
    echo("<td align=\"center\" width=\"40\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_use_time</font></td>\n");
}
echo("<td align=\"left\">");
echo("<input type=\"button\" value=\"削除\" onclick=\"delLine();\">");
echo("</td>\n");

echo("</tr>\n");

?>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="1">
<tr>
<td align="right">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="data_cnt" value="<? echo($data_cnt); ?>">
<input type="hidden" name="update_flg" value="1">
<input type="hidden" name="add_flg" id="add_flg" value="">
<input type="hidden" name="del_flg" value="">
<input type="hidden" name="select_close_date" value="<? echo($select_close_date); ?>">
<input type="hidden" name="arg_tbl_id" value="<? echo($arg_tbl_id); ?>">
<input type="submit" value="更新" onclick="return checkInput();">
</td>
</tr>
</table>
</form>


<?
pg_close($con);


/**
 * 付与日数取得
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $arr_paid_hol 有給休暇付与日数表
 * @param mixed $paid_hol_tbl_id 表名称
 * @param mixed $service_year 勤続年数
 * @param mixed $service_mon 勤続月数
 * @param mixed $timecard_bean タイムカード設定情報
 * @return mixed 付与日数
 *
 */
function get_add_day($con, $fname, $arr_paid_hol, $paid_hol_tbl_id, $service_year, $service_mon, $timecard_bean) {

	$ret_day = "";
	$wk_month = ($timecard_bean->criteria_months == "1") ? "3" : "6";

	//有休付与日が採用日の場合
	if ($timecard_bean->closing_paid_holiday == "3") {
		//期間確認 yymm形式
		if ($timecard_bean->criteria_months == "2") {
            $arr_hikaku_tbl = array("0706", "0606", "0506", "0406", "0306", "0206", "0106", "0006");
		} else {
            $arr_hikaku_tbl = array("0700", "0600", "0500", "0400", "0300", "0200", "0100", "0003");
		}
		$service_yymm = sprintf("%02d%02d", $service_year, $service_mon);
		//7年後から6ヶ月まで勤続の期間を確認
		for ($wk_idx=0; $wk_idx<8; $wk_idx++) {

			$wk_hikaku_kikan = $arr_hikaku_tbl[$wk_idx];
			if ($service_yymm >= $wk_hikaku_kikan) {
				$wk_kinzoku_id = 8 - $wk_idx;
				break;
			}
		}
		if ($wk_kinzoku_id <= 0) {
			$wk_kinzoku_id = 1;
		}

	} else {
		$wk_kinzoku_id = $service_year + 1;
		if ($wk_kinzoku_id > 8) {
			$wk_kinzoku_id = 8;
		}
	}
	$ret_day = $arr_paid_hol[$paid_hol_tbl_id][$wk_kinzoku_id];
	return $ret_day;
}

?>