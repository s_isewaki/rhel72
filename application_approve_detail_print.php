<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_application_apply_history.ini");
require_once("show_application_approve_detail.ini");
require_once("application_imprint_common.ini");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("get_values.ini");
require_once("application_workflow_common_class.php");
require_once("get_values_for_template.ini");
require_once("library_common.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,7,$fname);
if($wkfw=="0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$obj = new application_workflow_common_class($con, $fname);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript" src="js/domready.js"></script>
<script type="text/javascript">
// 全テキストエリアの行数変更
function resizeAllTextArea() {
    var objs = document.getElementsByTagName('textarea');
    for (var i = 0, j = objs.length; i < j; i++) {
        var t = objs[i];
        if (t.scrollHeight > t.clientHeight) {
            t.style.height = (t.scrollHeight + 5) + 'px';
        }
    }
}
</script>
<?
// 履歴フラグ true:履歴なので登録ボタン等を無効化する
$history_flg = ($history_flg == "1") ? true : false;
if($target_apply_id == "") {
    $target_apply_id = $apply_id;
}


// 申請・ワークフロー情報取得
$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($target_apply_id);
$wkfw_content      = $arr_apply_wkfwmst[0]["wkfw_content"];
$wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
$apply_content     = $arr_apply_wkfwmst[0]["apply_content"];
$wkfw_id           = $arr_apply_wkfwmst[0]["wkfw_id"];
$wkfw_history_no   = $arr_apply_wkfwmst[0]["wkfw_history_no"];
$approve_label     = ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認";

/*
$sel_apply_wkfwmst = search_apply_wkfwmst($con, $fname, $target_apply_id);
$wkfw_content = pg_fetch_result($sel_apply_wkfwmst, 0, "wkfw_content");
$wkfw_content_type = pg_fetch_result($sel_apply_wkfwmst, 0, "wkfw_content_type");
$apply_content = pg_fetch_result($sel_apply_wkfwmst, 0, "apply_content");
*/
// 本文形式タイプのデフォルトを「テキスト」とする
if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

// 形式をテキストからテンプレートに変更した場合の古いデータ対応
// wkfw_content_typeは2:テンプレートだが、登録データがXMLでない場合、1:テキストとして処理する
if ($wkfw_content_type == "2") {
    if (strpos($apply_content, "<?xml") === false) {
        $wkfw_content_type = "1";
    }
}

if($wkfw_history_no != "")
{
    $arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
    $wkfw_content = $arr_wkfw_template_history["wkfw_content"];
}

$num = 0;
if ($wkfw_content_type == "2") {
    $pos = 0;
    while (1) {
        $pos = strpos($wkfw_content, 'show_cal', $pos);
        if ($pos === false) {
            break;
        } else {
            $num++;
        }
        $pos++;
    }
}

// 外部ファイルを読み込む（$num==0でも読み込んでください）
write_yui_calendar_use_file_read_0_12_2();
// カレンダー作成、関数出力
write_yui_calendar_script2($num);
?>
<title>CoMedix 決裁・申請 | <? echo($approve_label); ?>詳細印刷</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}

p {margin:0;}
textarea {overflow:auto;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initcal();if (window.OnloadSub) { OnloadSub(); };resizeAllTextArea();copyApproveOrders();">
<form name="apply" action="#" method="post">
<? show_application_approve_detail($con, $session, $fname, $target_apply_id, $mode, $apv_print_comment, $approve, $drag_flg, 0, $history_flg, $apv_order_fix, $apv_sub_order_fix, $send_apved_order_fix); ?>
    <table id="approve_orders" width="100%" border="0" cellspacing="0" cellpadding="2" class="block2">
    </table>
</form>
</body>
<? pg_close($con); ?>
<script type="text/javascript">
function copyApproveOrders() {
    opener.updateDOM();

    var self_table = document.getElementById('approve_orders');
    var opener_rows = opener.document.getElementById('approve_orders').rows;
    for (var i = 0, j = opener_rows.length; i < j; i++) {
        var row = self_table.insertRow(-1);
        row.style.backgroundColor = opener_rows[i].style.backgroundColor;
        row.style.height = opener_rows[i].style.height;
        row.style.textAlign = 'center';

        var opener_cells = opener_rows[i].cells;
        for (var m = 0, n = opener_cells.length; m < n; m++) {
            var cell = row.insertCell(-1);
            cell.style.width = '20%';
            cell.innerHTML = opener_cells[m].innerHTML;
        }
    }
}

setTimeout("self.print();self.close();", 100);
</script>
</html>
