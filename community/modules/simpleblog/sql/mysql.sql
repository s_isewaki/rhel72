-- $Id: mysql.sql,v 1.1 2004/05/15 06:41:32 kousuke Exp $

-- blog information table
-- uid :
--    xoops uid
-- blog_permission:
--    0: anyone can read, anyone can write comment
--    1: anyone can read, user can write comment
--    3: user can read, user can write comment
-- last_update:
--    update time
CREATE TABLE simpleblog_info (
	uid int(5) unsigned NOT NULL default '0',
	title varchar(200) binary,
	blog_permission tinyint(1) NOT NULL default '0',
	last_update TIMESTAMP NOT NULL ,
	PRIMARY KEY (uid)
) TYPE=MyISAM;

-- blog data table
-- uid :
--     xoops uid
-- blog_date :
--     date of blog
-- title :
--     title of blog
-- post_text :
--     blog data
-- alter table xoops_simpleblog add last_update timestamp not null;
CREATE TABLE simpleblog (
	uid int(5) unsigned NOT NULL default '0',
	blog_date DATE not null,
	title varchar(200),
	post_text text,
	last_update TIMESTAMP NOT NULL,
	PRIMARY KEY  (uid, blog_date)
) TYPE=MyISAM;


-- blog comment table
-- uid :
--     xoops uid
-- blog_date :
--     date of blog
-- comment_id :
--     sequential comment id. 
-- comment_uid :
--     uid of comment user. set value to 0 if guest user
-- comment_name :
--     guest user name. 
-- post_text :
--     comment data
CREATE TABLE simpleblog_comment (
	uid int(5) unsigned NOT NULL default '0',
	blog_date DATE not null,
	comment_id int(8) unsigned NOT NULL auto_increment,
	comment_uid int(5) unsigned NOT NULL default '0',
	comment_name varchar(200),
	post_text text,
	create_date TIMESTAMP NOT NULL,
	KEY (uid, blog_date),
	PRIMARY KEY (comment_id)
) TYPE=MyISAM;

CREATE TABLE simpleblog_application (
	uid int(5) unsigned NOT NULL,
	title varchar(200) binary,
	permission tinyint(1) NOT NULL,
	create_date int(10) NOT NULL ,
	PRIMARY KEY (uid)
) TYPE = MyISAM;


CREATE TABLE simpleblog_trackback (
	uid int(5) unsigned NOT NULL,
	t_date DATE not null,
	count int(8) unsigned,
	title varchar(250),
	url text,
	KEY(uid, t_date)
) TYPE = MyISAM;