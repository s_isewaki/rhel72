<?php
// $Id: application.php,v 1.1 2004/05/15 06:37:34 kousuke Exp $
require('header.php');
	
	if(!$xoopsUser){
		redirect_header(XOOPS_URL.'/',1,_MD_SIMPLEBLOG_NORIGHTTOACCESS.'(1.1)');
		exit();
	}
	if(!xoops_refcheck()){
		redirect_header(XOOPS_URL.'/modules/simpleblog/',2,'Referer Check Failed');
		exit();
	}
	if((!empty($xoopsModuleConfig['SIMPLEBLOG_APPL'])) && ($xoopsModuleConfig['SIMPLEBLOG_APPL'] == 1) ){
		redirect_header(XOOPS_URL.'/',1,_MD_SIMPLEBLOG_NORIGHTTOACCESS.'(1.2)');
		exit();
	}
	
	$blog = new SimpleBlog($xoopsUser->uid());
	$result = SimpleBlogUtils::newApplication($HTTP_POST_VARS['title'], ($HTTP_POST_VARS['permission']));
	
	if($result == ''){
		redirect_header(XOOPS_URL.'/',1,_MD_SIMPLEBLOG_APPLICATION_APPLIED);
		exit();
	}else{
		redirect_header(XOOPS_URL.'/', 2, $result);
		exit();
	}
require('footer.php');
?>