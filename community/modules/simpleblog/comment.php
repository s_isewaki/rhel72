<?php
// $Id: comment.php,v 1.1 2004/05/15 06:37:34 kousuke Exp $
require('header.php');
	if(!xoops_refcheck()){
		redirect_header(XOOPS_URL.'/modules/simpleblog/',2,'Referer Check Failed');
		exit();
	}
	$params = SimpleBlogUtils::getDateFromHttpParams();
	
	$comment = isset($HTTP_POST_VARS['comment']) ? $HTTP_POST_VARS['comment'] : '';
	$name = isset($HTTP_POST_VARS['name']) ? $HTTP_POST_VARS['name'] : "";
	if($params['uid'] <= 0){
		redirect_header(XOOPS_URL.'/',3,_MD_SIMPLEBLOG_NORIGHTTOACCESS.'(0)');
		exit();
	}else if($comment == ''){
		redirect_header(SimpleBlogUtils::createUrl($params['uid']),3,_MD_SIMPLEBLOG_COMMENT_NO_COMMENT);
		exit();
	}
	if(!$xoopsUser){
		if($name == ''){
			$name = _MD_SIMPLEBLOG_FORM_ANONYMOUS_NAME;
		}else if(strlen($name) > 200){
			redirect_header(SimpleBlogUtils::createUrl($params['uid']), 3, _MD_SIMPLEBLOG_COMMENT_NAME_TOO_LONG);
			exit();
		}
	}
	$blog = new SimpleBlog($params['uid']);
	$dates = $params;
	if(!$dates || !SimpleBlogUtils::isCompleteDate($dates)){
		redirect_header(XOOPS_URL.'/',3,_MD_SIMPLEBLOG_INVALID_DATE.'(1.0)');
		exit();
	}
	
	if(!$blog->canComment()){
		redirect_header(XOOPS_URL.'/',3,_MD_SIMPLEBLOG_NORIGHTTOACCESS.'(1)');
		exit();
	}
	
	if($blog->insertComment($dates, $name,$comment)){
		redirect_header(SimpleBlogUtils::createUrl($params['uid']) ,2,_MD_SIMPLEBLOG_THANKS_COMMENT);
		exit();
	}
require('footer.php');
?>