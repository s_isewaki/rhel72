<?php
// $Id: comments.php,v 1.1 2004/09/09 10:03:23 onokazu Exp $
//%%%%%% Comment Manager %%%%%
define('_MD_AM_COMMMAN','コメントマネジャー');

define('_MD_AM_LISTCOMM','コメント一覧');
define('_MD_AM_ALLMODS','全てのモジュール');
define('_MD_AM_ALLSTATUS','全てのステータス');
define('_MD_AM_MODULE','モジュール');
define('_MD_AM_COMFOUND','%s 件のコメントが見つかりました。');
?>