<?php
// $Id: modinfo.php,v 1.1 2004/09/09 10:03:28 onokazu Exp $
// Module Info

// The name of this module
define("_MI_HEADLINES_NAME","ヘッドライン");

// A brief description of this module
define("_MI_HEADLINES_DESC","RSS/XML形式のニュース記事をブロック内に表示します");

// Names of blocks for this module (Not all module has blocks)
define("_MI_HEADLINES_BNAME","ヘッドラインブロック");

// Names of admin menu items
define("_MI_HEADLINES_ADMENU1","ヘッドライン一覧");
?>