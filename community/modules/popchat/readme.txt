/*******************************************************************************
XooPopChat - XOOPS2 Popup Chat module
Copyright (c) Y.Sakai @ Bluemoon inc. (www.bluemooninc.biz)
*******************************************************************************/

This is a Popup Chat modules for XOOPS2.

Future:
1.Open a pop window for chat. You can operate XOOPS separately.
2.Call from chat window with private message.
3.Leave bihind and call anybody a recent message with marquee block in XOOPS top menu.
4.Event notification supported. You can get a notice via email.
5.Replaceable user name to real name. 

v0.00 2004/11/13 Alpha release.
v0.01 2004/11/17 Log and Input are divide to Flame. Add French by Outch.
v0.02 2004/11/26 Add Who's Online and send Private Message from chat window.
v0.03 2005/01/10 Add Marquee,Admin menu,SQL Table. Delete log folder.
rev.a 2005/01/11 Bugix for auto link and single quotation. fix multi-language.
rev.b 2005/01/11 Bugix for MySQL install error of some version of MySQL. change prameter name in modinfo.php.
v0.04 2005/01/18 Add Event Notification. Add a parameter of replace user name to real name.
rev.a 2005/01/29 Fix about the form collapses when the pop window opened was corrected. 
rev.b 2005/01/30 Update a French language by Outch.
rev.c 2005/01/31 Bugfix about double display chat message to single.
rev.d 2005/02/28 Bugfix about it couldn't use a slash char.
rev.e 2005/03/04 Bugfix about notification.

<Usage>

--- PLEASE UNINSTALL BEFORE INSTALL NEW VERSION. ---
        (revision update are unneccecsally)

Edit a config.php, if you want modify.