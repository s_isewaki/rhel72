<?php
// $Id: xoopsobject.php,v 1.1 2004/09/09 05:14:50 onokazu Exp $
if (!defined('XOOPS_ROOT_PATH')) {
	exit();
}
/**
 * this file is for backward compatibility only
 * @package kernel
 **/
/**
 * Load the new object class 
 **/
require_once XOOPS_ROOT_PATH.'/kernel/object.php';
?>