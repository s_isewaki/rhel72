<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 52, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($name == "") {
	echo("<script type=\"text/javascript\">alert('定数の名前が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($name) > 80) {
	echo("<script type=\"text/javascript\">alert('定数の名前が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($value == "") {
	echo("<script type=\"text/javascript\">alert('値が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($value) > 10) {
	echo("<script type=\"text/javascript\">alert('値が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (!preg_match("/^-?[0-9.]+$/", $value)) {
	echo("<script type=\"text/javascript\">alert('値には数値を入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースへ接続
$con = connect2db($fname);

// 定数情報を更新
$sql = "update statconst set";
$set = array("statconst_name", "statconst_value");
$setvalue = array($name, $value);
$cond = "where statconst_id = $id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 管理項目一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'stat_master_const_list.php?session=$session'</script>");
?>
