<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 63, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 登録値を編集
if ($section_use == "") {$section_use = "f";}
if ($project_use == "") {$project_use = "f";}
if ($private_use == "") {$private_use = "f";}
if ($show_login_flg == "") {$show_login_flg = "f";}
if ($show_tree_flg == "") {$show_tree_flg = "t";}
if ($pjt_private_flg == "") {$pjt_private_flg = "f";}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// オプション情報をDELETE〜INSERT
$sql = "delete from libconfig";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$lock_show_all_flg = "t"; // 全体は隠さない
if ($lock_show_flg=="all") { // 全体を隠す場合
    $lock_show_all_flg = "f"; // 全体を隠す
    $lock_show_flg = "f"; // 部署も隠す
}
$sql = "insert into libconfig (section_archive_use, project_archive_use, private_archive_use, lock_show_flg, lock_show_all_flg, count_flg, dragdrop_flg, real_name_flg, show_login_flg, stick_folder,show_tree_flg,pjt_private_flg,show_newlist_flg,all_createfolder_flg,newlist_chk_flg) values (";
$content = array($section_use, $project_use, $private_use, $lock_show_flg, $lock_show_all_flg, $count_flg, $dragdrop_flg, $real_name_flg, $show_login_flg, $stick_folder,$show_tree_flg,$pjt_private_flg,$show_newlist_flg,$all_createfolder_flg,$newlist_chk_flg);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// データベース接続を閉じる
pg_close($con);

// オプション画面を再表示
echo("<script type=\"text/javascript\">location.href = 'library_config.php?session=$session';</script>");
?>
