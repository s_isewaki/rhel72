<?
ob_start();
require_once("about_comedix.php");
require_once("date_utils.php");
require_once("timecard_common_class.php");
ob_end_clean();

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    pq_close($con);
    ajax_server_erorr();
    exit;
}

// データベースに接続
$con = connect2db($fname);

$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $date, $date);
$arr_officetime_info = $timecard_common_class->get_officetime_info($emp_id, $tmcd_group_id, $pattern, $date);

//レスポンスデータ作成
$response_data = "ajax=success\n";

$p_key = "office_start_time";
$p_value = $arr_officetime_info["$p_key"];
$response_data .= "$p_key=$p_value\n";

$p_key = "office_end_time";
$p_value = $arr_officetime_info["$p_key"];
$response_data .= "$p_key=$p_value\n";

$p_key = "rest_start_time";
$p_value = $arr_officetime_info["$p_key"];
$response_data .= "$p_key=$p_value\n";

$p_key = "rest_end_time";
$p_value = $arr_officetime_info["$p_key"];
$response_data .= "$p_key=$p_value\n";

$p_key = "office_time_min";
$p_value = $arr_officetime_info["$p_key"];
$response_data .= "$p_key=$p_value\n";

$p_key = "rest_time_min";
$p_value = $arr_officetime_info["$p_key"];
$response_data .= "$p_key=$p_value\n";

$p_key = "base_time";
$p_value = $arr_officetime_info["$p_key"];
$response_data .= "$p_key=$p_value\n";


print $response_data;
// データベース切断
pg_close($con);

//サーバーエラー
function ajax_server_erorr()
{
    print "ajax=error\n";
    exit;
}

?>
