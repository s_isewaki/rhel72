<?
ob_start();
require("about_authority.php");
require("about_session.php");
ob_end_clean();

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pq_close($con);
	ajax_server_erorr();
	exit;
}

// データベースに接続
$con = connect2db($fname);

$group_nm = get_group_nm_ajax($con, $fname, $emp_id, $group_id);

//レスポンスデータ作成
if($group_nm == "")
{
	//検索にヒットしない。
	$response_data = "ajax=0hit\n";
}
else
{
	
	$response_data = "ajax=success\n";
	
	$p_key = "group_nm";
	$p_value = $group_nm;
	$response_data .= "$p_key=$p_value\n";
	
}	

print $response_data;
// データベース切断
pg_close($con);


// グループ名

/**
 * グループ名取得、指定職員が他グループに登録されている場合にグループ名を返す
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $emp_id 職員ID
 * @param mixed $group_id 登録対象グループID
 * @return mixed グループ名
 *
 */
function get_group_nm_ajax($con, $fname, $emp_id, $group_id)
{
	$sql  = "select b.group_name from duty_shift_staff a ";
	$sql .= "inner join duty_shift_group b on a.group_id = b.group_id ";
	$cond = "where a.emp_id = '$emp_id' and a.group_id != '$group_id' ";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pq_close($con);
		ajax_server_erorr();
		exit;
	}
	
	$group_nm = pg_fetch_result($sel, 0, "group_name");
	
	return $group_nm;
}	

//サーバーエラー
function ajax_server_erorr()
{
	print "ajax=error\n";
	exit;
}

?>
