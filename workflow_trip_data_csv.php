<?php
// 出張旅費
// 旅費CSV出力
require_once("about_comedix.php");
ob_start();
require_once('workflow_trip_common.php');
require_once("application_workflow_common_class.php");
require_once('Cmx/Model/SystemConfig.php');

$fname = $_SERVER["PHP_SELF"];

// セッションと権限のチェック
$session = qualify_session($_REQUEST["session"], $fname);
$checkauth = check_authority($session, 3, $fname);
if ($session == "0" or $checkauth == "0") {
    js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// obj
$obj = new application_workflow_common_class($con, $fname);

// 期間チェック
if ($_REQUEST["date_y1"] == '-' or $_REQUEST["date_m1"] == '-' or $_REQUEST["date_d1"] == '-') {
    js_alert_exit('期間が選択されていません。');
}
if ($_REQUEST["date_y2"] == '-' or $_REQUEST["date_m2"] == '-' or $_REQUEST["date_d2"] == '-') {
    js_alert_exit('期間が選択されていません。');
}
$st_ymd = sprintf("%04d%02d%02d000000", $_REQUEST["date_y1"], $_REQUEST["date_m1"], $_REQUEST["date_d1"]);
$ed_ymd = sprintf("%04d%02d%02d235959", $_REQUEST["date_y2"], $_REQUEST["date_m2"], $_REQUEST["date_d2"]);
if ($st_ymd > $ed_ymd) {
    js_alert_exit('期間が正しくありません。');
}
$date = sprintf("%04d%02d%02d-%04d%02d%02d", $_REQUEST["date_y1"], $_REQUEST["date_m1"], $_REQUEST["date_d1"], $_REQUEST["date_y2"], $_REQUEST["date_m2"], $_REQUEST["date_d2"]);

// typeチェック
if (empty($_REQUEST["type"])) {
    js_alert_exit('出力するCSVが選択されていません。');
}

// 所属
$class = @$_REQUEST["search_emp_class"];
$atrb = @$_REQUEST["search_emp_attribute"];
$dept = @$_REQUEST["search_emp_dept"];
$room = @$_REQUEST["search_emp_room"];

// 申請状況
$stat = @$_REQUEST["search_apply_stat"];

// ワークフロー
$type = @$_REQUEST["type"];

// 出力する項目
$items = (array)@$_REQUEST["item"];
if ($type == 1) {
    $items = array_merge($items, (array)@$_REQUEST["item1"]);
}
else {
    $items = array_merge($items, (array)@$_REQUEST["item2"]);
}
if (count($items) <= 0) {
    js_alert_exit('出力する項目が一つもチェックされていません。');
}

// 新幹線
$trip_station = get_trip_station();

// 飛行機
$trip_airport = get_trip_airport();

// 報告書ステータス
$array_apply_stat2 = array("" => '[未報告]', "0" => '[報告済(申請中)]', "1" => '[報告済(承認確定)]', "2" => '[報告済(否認)]', "3" => '[報告済(差戻し)]');

// 項目表題
$conf = new Cmx_SystemConfig();
$title_label = ($conf->get('apply.view.title_label') == "2") ? "標題" : "表題";
$item_title = array(
        "apply_no" => "申請番号",
        "wkfw_title" => "申請書名",
        "apply_title" => $title_label,
        "emp_id" => "職員ID",
        "emp_name" => "職員名",
        "date" => "申請日",
        "status" => "申請状況",
        "emp_dept" => "所属",
        "class_id" => "部署1コード",
        "atrb_id" => "部署2コード",
        "dept_id" => "部署3コード",
        "comment" => "承認者コメント",

        "kubun" => "種別",
        "t1_start_date" => "出張期間(開始)",
        "t1_end_date" => "出張期間(終了)",
        "t1_kikan" => "出張期間(日数)",
        "pref" => "出張先(都道府県)",
        "city" => "出張先(市区町村)",
        "place" => "出張先(出張場所)",
        "youken" => "出張要件",
        "gakkai_name" => "学会名",
        "gakkai_organizer" => "主催者",
        "gakkai_start_date" => "学会期間(開始)",
        "gakkai_end_date" => "学会期間(終了)",
        "presentation" => "演題発表",
        "act" => "主演/客演",
        "presentation_titie" => "演題名",
        "gakkai_igai" => "学会以外",
        "transit1" => "公共交通機関",
        "transit2" => "社用車",
        "transit3" => "タクシー",
        "transit4" => "航空機",
        "transit5" => "ビジネスパック",
        "transit6" => "私有車",
        "reason_transit6" => "私有車使用理由",
        "sharei" => "謝礼",
        "t1_prepayment" => "前渡金",
        "prepayment_value" => "前渡金額",
        "preticket" => "前渡チケット",
        "preticket21" => "新幹線",
        "preticket22" => "国内航空券",
        "station1_from" => "新幹線行き：発",
        "station1_to" => "新幹線行き：着",
        "station1_shitei" => "新幹線行き：列車指定",
        "station2_from" => "新幹線帰り：発",
        "station2_to" => "新幹線帰り：着",
        "station2_shitei" => "新幹線帰り：列車指定",
        "airport1_from" => "航空機行き：発",
        "airport1_to" => "航空機行き：着",
        "airport1_shitei" => "航空機行き：便指定",
        "airport2_from" => "航空機帰り：発",
        "airport2_to" => "航空機帰り：着",
        "airport2_shitei" => "航空機帰り：便指定",
        "t2_apply_stat" => "出張報告書",

        "report" => "報告内容",
        "t2_start_date" => "出発時間",
        "t2_end_date" => "到着時間",
        "t2_kikan" => "出張実働日数",
        "t2_kikan2" => "出張実働日数 (B)",
        "transit_flg" => "交通費不要",
        "stay_flg" => "宿泊料不要",
        "stay1" => "宿泊地(東京23区内)",
        "stay1_days" => "宿泊日数(東京23区内)",
        "stay1_days2" => "宿泊日数(東京23区内) (B)",
        "stay2" => "宿泊地(東京23区以外)",
        "stay2_days" => "宿泊日数(東京23区以外)",
        "stay2_days2" => "宿泊日数(東京23区以外) (B)",
        "stay2_city" => "宿泊場所(東京23区以外)",
        "tt1_fee_total" => "交通費(公共交通機関)",
        "tt2_fee_total" => "交通費(ビジネスパック)",
        "tt3_fee_total" => "交通費(私有車)",
        "tt3_reason" => "私有車使用理由",
        "entrance_fee" => "参加費",

        "daily_fee" => "日当(単価)",
        "daily_fee_days" => "日当(日数)",
        "daily_fee_percentage" => "日当(割合)",
        "daily_fee_total" => "日当(金額)",

        "daily_fee2" => "日当(単価) (B)",
        "daily_fee_days2" => "日当(日数) (B)",
        "daily_fee_percentage2" => "日当(割合) (B)",
        "daily_fee_total2" => "日当(金額) (B)",

        "tokyo_fee" => "23区宿泊(単価)",
        "tokyo_fee_days" => "23区宿泊(日数)",
        "tokyo_fee_percentage" => "23区宿泊(割合)",
        "tokyo_fee_total" => "23区宿泊(金額)",

        "tokyo_fee2" => "23区宿泊(単価) (B)",
        "tokyo_fee_days2" => "23区宿泊(日数) (B)",
        "tokyo_fee_percentage2" => "23区宿泊(割合) (B)",
        "tokyo_fee_total2" => "23区宿泊(金額) (B)",

        "hotel_fee" => "宿泊(単価)",
        "hotel_fee_days" => "宿泊(日数)",
        "hotel_fee_percentage" => "宿泊(割合)",
        "hotel_fee_total" => "宿泊(金額)",

        "hotel_fee2" => "宿泊(単価) (B)",
        "hotel_fee_days2" => "宿泊(日数) (B)",
        "hotel_fee_percentage2" => "宿泊(割合) (B)",
        "hotel_fee_total2" => "宿泊(金額) (B)",

        "t2_prepayment" => "前渡金",
        "total_fee" => "精算金額",

        "t1_apply_no" => "出張・自己研修届 申請番号"
        );

// データ取得SQL
$sql = <<<__SQL_END__
SELECT
    a.*,
    w.short_wkfw_name,
    w.wkfw_title,
    w.approve_label,
    e.emp_personal_id,
    e.emp_lt_nm,
    e.emp_ft_nm,
    b1.class_nm,
    b1.link_key as b1_id,
    b2.atrb_nm,
    b2.link_key as b2_id,
    b3.dept_nm,
    b3.link_key1 as b3_id,
    t2.apply_stat as t2_apply_stat
FROM apply a
    JOIN empmst e USING (emp_id)
    JOIN wkfwmst w USING (wkfw_id)
    LEFT JOIN classmst b1 ON a.emp_class=b1.class_id
    LEFT JOIN atrbmst b2 ON a.emp_attribute=b2.atrb_id
    LEFT JOIN deptmst b3 ON a.emp_dept=b3.dept_id
    LEFT JOIN trip_link tl ON a.apply_id=tl.trip1_id
    LEFT JOIN apply t2 ON tl.trip2_id=t2.apply_id
__SQL_END__;

$cond = <<<__SQL_END__
WHERE not a.delete_flg
  AND not a.draft_flg
  AND a.apply_date >= '{$st_ymd}'
  AND a.apply_date <= '{$ed_ymd}'
  AND a.wkfw_id IN (SELECT wkfw_id FROM trip_workflow WHERE type={$type})
__SQL_END__;

if ($stat != '-') {
    $cond .=" AND a.apply_stat='{$stat}'";
}
if ($class) {
    $cond .=" AND a.emp_class={$class}";
}
if ($atrb) {
    $cond .=" AND a.emp_attribute={$atrb}";
}
if ($dept) {
    $cond .=" AND a.emp_dept={$dept}";
}
if ($room) {
    $cond .=" AND a.emp_room={$room}";
}
$cond .=" ORDER BY a.apply_id";

// SQL実行
$sel = select_from_table($con,$sql,$cond,$fname);
if ($sel == 0) {
    js_error_exit();
}

// データ集計
$csv = "";
while ($r = pg_fetch_assoc($sel)) {
    $xml = get_array_apply_content($r["apply_content"]);

    $comment = "";
    $applyapv = $obj->get_applyapv($r["apply_id"]);
    foreach ($applyapv as $apv) {
        if ($apv["apv_comment"]) {
            $comment = $apv["apv_comment"];
        }
    }

    $fields = array(
            "apply_no"      => format_apply_no($r["short_wkfw_name"], $r["apply_date"], $r["apply_no"]),
            "wkfw_title"    => $r["wkfw_title"],
            "apply_title"   => $r["apply_title"],
            "emp_id"        => $r["emp_personal_id"],
            "emp_name"      => $r["emp_lt_nm"]." ".$r["emp_ft_nm"],
            "date"          => preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2}).*$/", "$1/$2/$3 $4:$5", $r["apply_date"]),
            "status"        => $array_apply_stat[ $r["apply_stat"] ],
            "emp_dept"      => format_emp_dept($r["class_nm"],$r["atrb_nm"],$r["dept_nm"]),
            "class_id"      => $r["b1_id"],
            "atrb_id"       => $r["b2_id"],
            "dept_id"       => $r["b3_id"],
            "comment"       => $comment
    );

    if ($type == 1) {
        $presentation = $array_presentation[ $xml["presentation"] ];
        if ($xml["zacho"]) {
            $presentation .= '、座長';
        }
        if ($xml["shikai"]) {
            $presentation .= '、司会者';
        }

        $preticket21 = check_checkbox($xml["preticket2"], 1);
        $preticket22 = check_checkbox($xml["preticket2"], 2);
        $fields = array_merge($fields, array(
                    "kubun"                 => $array_kubun[ $xml["kubun"] ],
                    "t1_start_date"         => sprintf("%04d/%02d/%02d", $xml["date_y1"],$xml["date_m1"],$xml["date_d1"]),
                    "t1_end_date"           => sprintf("%04d/%02d/%02d", $xml["date_y2"],$xml["date_m2"],$xml["date_d2"]),
                    "t1_kikan"              => $xml["kikan"],
                    "pref"                  => $array_pref[ $xml["pref"] ],
                    "city"                  => $xml["city"],
                    "place"                 => $xml["place"],
                    "youken"                => $array_youken[ $xml["youken"] ],
                    "gakkai_name"           => $xml["youken"] == "1" ? $xml["gakkai_name"] : "",
                    "gakkai_organizer"      => $xml["youken"] == "1" ? $xml["gakkai_organizer"] : "",
                    "gakkai_start_date"     => $xml["youken"] == "1" ? sprintf("%04d/%02d/%02d", $xml["date_y3"],$xml["date_m3"],$xml["date_d3"]) : "",
                    "gakkai_end_date"       => $xml["youken"] == "1" ? sprintf("%04d/%02d/%02d", $xml["date_y4"],$xml["date_m4"],$xml["date_d4"]) : "",
                    "presentation"          => $xml["youken"] == "1" ? $presentation : "",
                    "presentation_titie"    => $xml["youken"] == "1" ? $xml["presentation_titie"] : "",
                    "act"                   => $xml["youken"] == "1" ? $array_act[ $xml["act"] ] : "",
                    "gakkai_igai"           => $xml["youken"] == "2" ? $xml["gakkai_igai"] : "",
                    "transit1"              => check_checkbox($xml["transit"], 1) ? '1' : '0',
                    "transit2"              => check_checkbox($xml["transit"], 2) ? '1' : '0',
                    "transit3"              => check_checkbox($xml["transit"], 3) ? '1' : '0',
                    "transit4"              => check_checkbox($xml["transit"], 4) ? '1' : '0',
                    "transit5"              => check_checkbox($xml["transit"], 5) ? '1' : '0',
                    "transit6"              => check_checkbox($xml["transit"], 6) ? '1' : '0',
                    "reason_transit6"       => $xml["reason_transit6"],
                    "sharei"                => $array_sharei[ $xml["sharei"] ],
                    "t1_prepayment"         => $array_prepayment[ $xml["prepayment"] ],
                    "prepayment_value"      => $xml["prepayment_value"],
                    "preticket"             => $array_preticket[ $xml["preticket"] ],
                    "preticket21"           => $preticket21 ? '1' : '0',
                    "preticket22"           => $preticket22 ? '1' : '0',
                    "station1_from"         => $preticket21 ? $trip_station[ $xml["station1_from"] ] : "",
                    "station1_to"           => $preticket21 ? $trip_station[ $xml["station1_to"] ] : "",
                    "station1_shitei"       => $preticket21 ? $xml["station1_shitei"] : "",
                    "station2_from"         => $preticket21 ? $trip_station[ $xml["station2_from"] ] : "",
                    "station2_to"           => $preticket21 ? $trip_station[ $xml["station2_to"] ] : "",
                    "station2_shitei"       => $preticket21 ? $xml["station2_shitei"] : "",
                    "airport1_from"         => $preticket22 ? $trip_airport[ $xml["airport1_from"] ] : "",
                    "airport1_to"           => $preticket22 ? $trip_airport[ $xml["airport1_to"] ] : "",
                    "airport1_shitei"       => $preticket22 ? $xml["airport1_shitei"] : "",
                    "airport2_from"         => $preticket22 ? $trip_airport[ $xml["airport2_from"] ] : "",
                    "airport2_to"           => $preticket22 ? $trip_airport[ $xml["airport2_to"] ] : "",
                    "airport2_shitei"       => $preticket22 ? $xml["airport2_shitei"] : "",
                    "t2_apply_stat"         => $array_apply_stat2[ $r["t2_apply_stat"] ]
                    ));
    }
    elseif ($type == 2) {
        $fields = array_merge($fields, array(
                    "report"                => $xml["report"],
                    "t2_start_date"         => sprintf("%04d/%02d/%02d %02d:%02d", $xml["date_y1"],$xml["date_m1"],$xml["date_d1"],$xml["date_h1"],$xml["date_i1"]),
                    "t2_end_date"           => sprintf("%04d/%02d/%02d %02d:%02d", $xml["date_y2"],$xml["date_m2"],$xml["date_d2"],$xml["date_h2"],$xml["date_i2"]),
                    "t2_kikan"              => $xml["kikan"],
                    "t2_kikan2"             => $xml["kikan2"],
                    "transit_flg"           => $xml["transit_flg"][0],
                    "stay_flg"              => $xml["stay_flg"][0],
                    "stay1"                 => check_checkbox($xml["stay"], 1) ? '1' : '0',
                    "stay1_days"            => $xml["stay1_days"],
                    "stay1_days2"           => $xml["stay1_days2"],
                    "stay2"                 => check_checkbox($xml["stay"], 2) ? '1' : '0',
                    "stay2_days"            => $xml["stay2_days"],
                    "stay2_days2"           => $xml["stay2_days2"],
                    "stay2_city"            => $xml["stay2_city"],
                    "tt1_fee_total"         => $xml["tt1_fee_total"],
                    "tt2_fee_total"         => $xml["tt2_fee_total"],
                    "tt3_fee_total"         => $xml["tt3_fee_total"],
                    "tt3_reason"            => $xml["tt3_reason"],
                    "entrance_fee"          => $xml["entrance_fee"],
                    "daily_fee"             => $xml["daily_fee"],
                    "daily_fee_days"        => $xml["daily_fee_days"],
                    "daily_fee_percentage"  => $xml["daily_fee_percentage"],
                    "daily_fee_total"       => $xml["daily_fee_total"],

                    "daily_fee2"            => $xml["daily_fee2"],
                    "daily_fee_days2"       => $xml["daily_fee_days2"],
                    "daily_fee_percentage2" => $xml["daily_fee_percentage2"],
                    "daily_fee_total2"      => $xml["daily_fee_total2"],

                    "tokyo_fee"             => $xml["tokyo_fee"],
                    "tokyo_fee_days"        => $xml["tokyo_fee_days"],
                    "tokyo_fee_percentage"  => $xml["tokyo_fee_percentage"],
                    "tokyo_fee_total"       => $xml["tokyo_fee_total"],

                    "tokyo_fee2"            => $xml["tokyo_fee2"],
                    "tokyo_fee_days2"       => $xml["tokyo_fee_days2"],
                    "tokyo_fee_percentage2" => $xml["tokyo_fee_percentage2"],
                    "tokyo_fee_total2"      => $xml["tokyo_fee_total2"],

                    "hotel_fee"             => $xml["hotel_fee"],
                    "hotel_fee_days"        => $xml["hotel_fee_days"],
                    "hotel_fee_percentage"  => $xml["hotel_fee_percentage"],
                    "hotel_fee_total"       => $xml["hotel_fee_total"],

                    "hotel_fee2"            => $xml["hotel_fee2"],
                    "hotel_fee_days2"       => $xml["hotel_fee_days2"],
                    "hotel_fee_percentage2" => $xml["hotel_fee_percentage2"],
                    "hotel_fee_total2"      => $xml["hotel_fee_total2"],

                    "t2_prepayment"         => $xml["prepayment"],
                    "total_fee"             => $xml["total_fee"],
                    "t1_apply_no"           => t1_apply_no($xml["trip1"])
            ));
    }

    $new_fields = array();
    foreach ($items as $item) {
        $value = str_replace('"', '""', $fields[$item]);
        if (preg_match('/[,"\s]/', $value)) {
            $value = '"' . $value . '"';
        }
        $new_fields[] = $value;
    }
    $csv .= implode(',', $new_fields) . "\n";

}

$top = array();
foreach ($items as $item) {
    $top[] = $item_title[$item];
}
$csvtop .= implode(',', $top) . "\n";

$csv = mb_convert_encoding($csvtop.$csv, "Shift_JIS", "EUC-JP");

pg_close($con);
ob_end_clean();

// CSV出力
$file_name = "data_$date.csv";
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);

//-----------------------------------------------
// format_apply_no_for_csv
//-----------------------------------------------
function format_apply_no($short_wkfw_name, $apply_date, $apply_no) {
    $y  = substr($apply_date, 0, 4);
    $md = substr($apply_date, 4, 4);
    if ($md >= "0101" && $md <= "0331") {
        $y = $y - 1;
    }
    return sprintf("%s-%s%04d", $short_wkfw_name, $y, $apply_no);
}

//-----------------------------------------------
// format_emp_dept_for_csv
//-----------------------------------------------
function format_emp_dept($class_nm, $atrb_nm, $dept_nm) {
    if ($class_nm == "" || $atrb_nm == "" || $dept_nm == "") {
        return "";
    }
    $emp_dept = "$class_nm > $atrb_nm > $dept_nm";
    return $emp_dept;
}

//-----------------------------------------------
// check_checkbox
//-----------------------------------------------
function check_checkbox($checkbox,$value) {
    foreach ($checkbox as $id) {
        if ($id == $value) {
            return true;
        }
    }
    return false;
}

//-----------------------------------------------
// t1_apply_no
//-----------------------------------------------
function t1_apply_no($trip1_id) {
    global $con, $fname;
    $sql = "SELECT apply_date, apply_no, w.short_wkfw_name FROM apply JOIN wkfwmst w USING (wkfw_id) WHERE apply_id={$trip1_id}";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        js_error_exit();
    }
    $r = pg_fetch_assoc($sel);
    return format_apply_no($r["short_wkfw_name"], $r["apply_date"], $r["apply_no"]);
}