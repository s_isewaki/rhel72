<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 23, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$arr_class_name = get_class_name_array($con, $fname);

// 入力チェック
if ($class_name == "") {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('{$arr_class_name[0]}名を入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($class_name) > 60) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('{$arr_class_name[0]}名が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($link_key) > 12) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('外部連携キーが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// 部門IDを採番
$sql = "select max(class_id) from classmst";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$class_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 表示順の決定
$sql = "select max(order_no) from classmst";
$cond = "where not class_del_flg";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$order_no = intval(pg_fetch_result($sel, 0, 0)) + 1;

// トランザクションの開始
pg_query($con, "begin");

// 部門情報を登録
$area_id = ($area_id == "") ? null : intval($area_id);
$sql = "insert into classmst (class_id, class_nm, class_del_flg, area_id, class_show_flg, link_key, order_no) values (";
$content = array($class_id, $class_name, "f", $area_id, $class_show_flg, $link_key, $order_no);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 文書カテゴリIDを採番
$sql = "select max(lib_cate_id) from libcate";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$lib_cate_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 文書カテゴリ情報を登録
$sql = "insert into libcate (lib_archive, lib_cate_id, lib_link_id, lib_cate_nm) values (";
$content = array(3, $lib_cate_id, $class_id, $class_name);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_query($con, "commit");
pg_close($con);

// 情報共有用のディレクトリを作成
if (!is_dir("docArchive")) {
	mkdir("docArchive", 0755);
}
if (!is_dir("docArchive/section")) {
	mkdir("docArchive/section", 0755);
}
mkdir("docArchive/section/cate$lib_cate_id", 0755);

// 一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'class_menu.php?session=$session';</script>\n");