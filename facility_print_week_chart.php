<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 設備予約 | 週（印刷）</title>
<?
$fname = $PHP_SELF;

require_once("about_comedix.php");
require("show_facility.ini");
require("facility_common.ini");
require("facility_week_chart_common.ini");
require("holiday.php");
require("show_calendar_memo.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 施設・設備登録権限を取得
$fcl_admin_auth = check_authority($session, 41, $fname);

// 表示範囲が渡されていない場合は「全て」に設定
if ($range == "") {
    $range = "all";
}

// タイムスタンプが渡されていない場合は当日日付のタイムスタンプを取得
if ($date == "") {
    $date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}

// 日付の設定
$day = date("d", $date);
$month = date("m", $date);
$year = date("Y", $date);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 参照／予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_referable_categories($con, $emp_id, $fname);

// カテゴリ未選択の場合、最初のカテゴリが選択されたものとする
if ($cate_id == "") {
    if ($range != "all") {
        list($cate_id, $fcl_id) = split("-", $range);
    }
    if ($cate_id == "") {
        $cate_id = (count($categories) > 0) ? strval(key($categories)) : "";
    }
}

// 設備未選択の場合、最初の設備が選択されたものとする
if ($cate_id != "" && $fcl_id == "") {
    $facilities = $categories[$cate_id]["facilities"];
    $fcl_id = (count($facilities) > 0) ? strval(key($facilities)) : "";
}

// 選択されたカテゴリの患者情報連携フラグを取得
if ($cate_id != "") {
    $pt_flg = ($categories[$cate_id]["pt_flg"] == "t");
} else {
    $pt_flg = false;
}

// 選択された設備の管理者権限を取得
if ($fcl_id != "") {
    $fcl_admin_flg = $categories[$cate_id]["facilities"][$fcl_id]["admin_flg"];
} else {
    $fcl_admin_flg = false;
}

// 選択されたカテゴリの週表示に予定内容を表示するを取得
if ($cate_id != "") {
    $show_content = $categories[$cate_id]["show_content"];
} else {
    $show_content = "f";
}

// 他画面の表示範囲に選択内容を反映
if ($range = "all") {
    if ($cate_id != "") {
        $range = $cate_id;
    }
    if ($fcl_id != "") {
        $range .= "-{$fcl_id}";
    }
}

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];
$calendar_start = $default_info["calendar_start"];

// 表示対象週のスタート日のタイムスタンプを取得
$start_date = get_start_date_timestamp($con, $date, $emp_id, $fname);

// 表示対象週の7日分の日付を配列で取得
$dates = get_dates_in_week($start_date);

// 時間枠の設定
$base_min = 30;
if ($cate_id != "") {
    $base_min = $categories[$cate_id]["base_min"];
}

// 時刻指定なしの予約情報を配列で取得
$timeless_reservations = get_timeless_reservations($con, $cate_id, $fcl_id, $dates, $base_min, $fcl_admin_flg, $emp_id, $pt_flg, $fname);

// 予約情報を配列で取得
$reservations = get_reservations($con, $cate_id, $fcl_id, $dates, $base_min, $fcl_admin_flg, $emp_id, $pt_flg, $fname);

// スタート曜日の設定
$start_wd = ($calendar_start == "1") ? 0 : 1;  // 0：日曜、1：月曜

// 処理週のスタート日のタイムスタンプを求める
$start_day = get_start_day($date, $start_wd);

// 前月・前週・翌週・翌月のタイプスタンプを変数に格納
$last_month = get_last_month($date);
$last_week = strtotime("-7 days", $date);
$next_week = strtotime("+7 days", $date);
$next_month = get_next_month($date, $start_wd);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
@media print {
    .noprint {visibility:hidden;}
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="self.print();self.close();">
<center>
<form name="view" action="facility_week_chart.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="30"><span class="noprint"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示範囲&nbsp;&nbsp;<? show_category_string($categories, $cate_id); ?></font></span><img src="img/spacer.gif" width="180" height="1" alt=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><? show_date_string($date, $start_day, $session, $range); ?></font></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="range" value="<? echo($range); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
</form>
<? show_facilities($categories, $cate_id, $fcl_id, $range, $date, $session); ?>
<?
if ($fcl_id != "") {
    echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">\n");

    // 日毎の最大重複数（列数）
    $max_col_of_days = get_max_col_of_days($reservations);

    $start_date = date("Ymd", $start_day);
    $end_date = date("Ymd", strtotime("+6 days", $start_day));
    // カレンダーのメモを取得
    $arr_calendar_memo = get_calendar_memo($con, $fname, $start_date, $end_date);
    // 日付行を表示
    show_dates($con, $dates, $range, $session, $fname, $arr_calendar_memo, 40, $reservations);

    // 時刻指定なしの予約行を表示
    show_timeless_reservations($timeless_reservations, $base_min, $cate_id, $fcl_id, $range, $pt_flg, $session, false, true, $max_col_of_days, $reservations);
    // 予約行を表示
    show_reservations($reservations, $base_min, $cate_id, $fcl_id, $range, $pt_flg, $session, false, true, $max_col_of_days);

    echo("</table>\n");
} else {
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
    echo("<tr height=\"22\">\n");
    echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">表示可能な設備がありません。</font></td>\n");
    echo("</tr>\n");
    echo("</table>\n");
}
?>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// 表示範囲を出力
function show_category_string($categories, $cate_id) {
    if (count($categories) == 0) {
        echo("（未登録）");
        return;
    }

    foreach ($categories as $tmp_category_id => $tmp_category) {
        if ($tmp_category_id != $cate_id) {
            continue;
        }
        echo("【{$tmp_category["name"]}】");
        break;
    }
}

// 選択日付を出力
function show_date_string($date, $start_day, $session, $range) {
    $tmp_start_day = date("Y/m/d", $start_day);
    $tmp_end_day = date("Y/m/d", strtotime("+6 days", $start_day));
    echo("【{$tmp_start_day}〜{$tmp_end_day}】");
}

// 設備リストを出力
function show_facilities($categories, $cate_id, $fcl_id, $range, $date, $session) {
    if (count($categories) == 0) {
        return;
    }
    if (count($categories[$cate_id]["facilities"]) == 0) {
        return;
    }

    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"margin-top:5px;\">\n");
    echo("<tr>\n");

    foreach ($categories[$cate_id]["facilities"] as $tmp_fcl_id => $tmp_facility) {
        if ($tmp_fcl_id != $fcl_id) {
            continue;
        }
        echo("<td style=\"padding-right:15px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        echo("<b>");
        echo($tmp_facility["name"]);
        echo("</b>");
        echo("</font></td>\n");
        break;
    }

    echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
    $time_str = date("Y")."/".date("m")."/".date("d")." ".date("H").":".date("i");
    echo("作成日時：$time_str\n");
    echo("</font></td>\n");
    echo("</tr>\n");
    echo("</table>\n");
}
?>
