<?php
$fname = $_SERVER["PHP_SELF"];

require_once("about_comedix.php");
require_once("show_schedule_chart.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// スケジュール管理権限を取得
$schd_place_auth = check_authority($session, 13, $fname);

// 日付の設定
if ($date == "") {
    $date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}
$year = date("Y", $date);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "SELECT emp_id FROM session WHERE session_id='{$session}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// スケジュール機能の初期画面情報を取得
$default_view = get_default_view($con, $emp_id, $fname);

// オプション設定情報を取得
$sql = "SELECT * FROM schdical WHERE emp_id='{$emp_id}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
if (pg_num_rows($sel) > 0) {
    $ical = pg_fetch_assoc($sel);
}
else {
    $ical = array(
        "active"        => "f",
        "before_days"   => 30,
        "after_days"    => 60,
        "type"          => 1,
        "timeguide"     => "f",
        "project"       => "f",
        "type_in_title" => "f",
    );
}

// URL
$script_names = explode("/",$_SERVER["SCRIPT_NAME"]);
$url = sprintf("webcal://%s/%s/", $_SERVER["SERVER_NAME"], $script_names[1]) ;

pg_close($con);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | カレンダー連携</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
form {padding:0 10px;}
fieldset {margin:10px 0;padding:5px;}
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
<script type="text/javascript">
function public_str() {
    var r = '';
    var str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    for (var i = 0; i < 64; i++) {
        r += str.charAt(Math.floor(Math.random() * 62));
    }
    return r;
}
function set_public_str() {
    $('#public_str').val(public_str());
}
function load_public_str() {
    if (!$('#public_str').val()) {
        set_public_str();
    }
}

</script>
</head>

<body style="color:#000; background-color:#fff; margin:0;" onload="load_public_str();">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr bgcolor="#f6f9ff">
    <td width="32" height="32" class="spacing"><a href="<?php eh($default_view); ?>?session=<?php eh($session); ?>"><img src="img/icon/b02.gif" width="32" height="32" border="0" alt="スケジュール"></a></td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php eh($default_view); ?>?session=<?php eh($session); ?>"><b>スケジュール</b></a></font></td>
    <? if ($schd_place_auth == "1") { ?>
    <td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="schedule_place_list.php?session=<?php eh($session); ?>"><b>管理画面へ</b></a></font></td>
    <? } ?>
    </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #5279a5; margin-bottom:5px;">
    <tr height="22">
    <td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
    <td width="5"></td>
    <td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週</font></a></td>
    <td width="5"></td>
    <td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_month_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
    <td width="5"></td>
    <td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_year_menu.php?session=<?php eh($session); ?>&year=<?php eh($year); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
    <td width="5"></td>
    <td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_list_upcoming.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
    <td width="5"></td>
    <td width="160" align="center" bgcolor="#bdd1e7"><a href="schedule_search.php?session=<?php eh($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公開スケジュール検索</font></a></td>
    <td width="5"></td>
    <td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_mygroup.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイグループ</font></a></td>
    <td width="5"></td>
    <td width="120" align="center" bgcolor="#5279a5"><a href="schedule_ical.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>カレンダー連携</b></font></a></td>
    <td width="5"></td>
    <td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_option.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
    <td>&nbsp;</td>
    </tr>
</table>

<form action="schedule_ical_insert.php" name="mainform">
    <p class="j16"><b>カレンダー連携設定</b></p>

    <div style="background-color:#f8e58c;padding:3px;font-size:11px;cursor:pointer;" onclick="$('#setsumei').toggle();">
    <span style="color:red;font-weight:bold;">（※注）</span>
    カレンダー連携をご利用いただくためには一定の条件が必要です。
    詳しくはココをクリック。
    </div>
    <div id="setsumei" style="display:none;margin-top:5px;border:1px solid #000;padding:5px;font-size:13px;">
        <b>iPhone(スマートフォン)と連携するための条件</b>
        <ul>
            <li>WiFiを経由して院内のネットワークにアクセスでき、CoMedixに接続することができる。</li>
            <li>VPNを経由して院外から院内のネットワークにアクセスでき、CoMedixに接続することができる。</li>
            <li>インターネットから、CoMedixに接続することができる。</li>
        </ul>
        <hr />
        <b>Googleカレンダーと連携するするための条件</b>
        <ul>
            <li>インターネットから、CoMedixに接続することができる。</li>
        </ul>
        <hr />
        <b>注意事項</b>
        <ul>
            <li>連携はCoMedix→連携先(iPhone,Googleカレンダー等)への一方向となります。連携先→CoMedixへの連携はできません。</li>
            <li>連携先(iPhone,Googleカレンダー等)でスケジュールを編集したり、削除することはできません。参照のみ可能です。</li>
            <li style="color:red;font-weight:bold;">連携したiPhone(スマートフォン)を紛失された際には、必ず公開用文字列の再生成を行ってください。</li>
        </ul>
    </div>

<?php if ($ical["active"] == 't') {?>
    <fieldset class="j12">
    <legend>カレンダー連携URL</legend>
    <p><a href="<?php eh($url);?>schedule_list_ics.php?emp_id=<?php eh($emp_id);?>&str=<?php eh($ical["public_str"]);?>" target="_blank">iPhone(iPad)から、このリンクをクリックするとカレンダー登録が簡単にできます。</a></p>
    <input type="text" id="ical_url" size="100" readonly style="font-size:11px;font-family:monospace;" value="<?php eh($url);?>schedule_list_ics.php?emp_id=<?php eh($emp_id);?>&str=<?php eh($ical["public_str"]);?>" /><br />
    <button type="button" onclick="javascript:clipboardData.setData('Text', document.getElementById('ical_url').value);">クリップボードにコピー(IEのみ)</button><br />
    </fieldset>
    <hr />
<?php }?>

    <fieldset class="j12">
    <legend>カレンダー連携を利用する</legend>
    <label><input type="radio" name="active" value="1" <?php echo $ical["active"] == "t" ? 'checked' : '';?> />利用する</label>
    <label><input type="radio" name="active" value="0" <?php echo $ical["active"] == "f" ? 'checked' : '';?> />利用しない</label>
    </fieldset>

    <fieldset class="j12">
    <legend>期間</legend>
    <div>過去：<input type="text" name="before_days" value="<?php eh($ical["before_days"]);?>" size="4" style="text-align:right;" />日前のスケジュールを連携する</div>
    <div>予定：<input type="text" name="after_days"  value="<?php eh($ical["after_days"]);?>"  size="4" style="text-align:right;" />日先のスケジュールを連携する</div>
    </fieldset>

    <fieldset class="j12">
    <legend>連携スケジュール</legend>
    <label><input type="radio" name="type" value="1" <?php echo $ical["type"] == "1" ? 'checked' : '';?> />公開スケジュールのみ</label>
    <label><input type="radio" name="type" value="2" <?php echo $ical["type"] == "2" ? 'checked' : '';?> />私用は「予定あり」とする</label>
    <label><input type="radio" name="type" value="3" <?php echo $ical["type"] == "3" ? 'checked' : '';?> />私用も含む全て</label>
    </fieldset>

    <fieldset class="j12">
    <legend>追加連携</legend>
    <label><input type="checkbox" name="timeguide" value="1" <?php echo $ical["timeguide"] == "t" ? 'checked' : '';?> />院内行事</label><br />
    <label><input type="checkbox" name="project" value="1" <?php echo $ical["project"] == "t" ? 'checked' : '';?> />委員会・WG</label> (<a href="javascript:void(0);" onclick="window.open('schedule_project_setting.php?session=<?php eh($session); ?>', 'newwin', 'width=640, height=700, scrollbars=yes');">表示させる委員会・WGの設定</a>)<br />
    </fieldset>

    <fieldset class="j12">
    <legend>その他</legend>
    <label><input type="checkbox" name="type_in_title" value="1" <?php echo $ical["type_in_title"] == "t" ? 'checked' : '';?> />種別をタイトルに含める</label><br />
    </fieldset>

    <fieldset class="j12">
    <legend>公開用文字列</legend>
    <input type="text" id="public_str" name="public_str" size="64" readonly style="font-family:monospace;color:grey;" value="<?php eh($ical["public_str"]);?>" />
    <button type="button" onclick="set_public_str();">再生成</button><br />
    <div style="margin-top:3px;color:red;">※再生成を行うと、iPhoneやGoogleカレンダーと連携しているURLは無効になります。再度iPhoneやGoogleカレンダーと連携の設定をしていただく必要があります。</div>
    </fieldset>

    <button class="j12" type="submit">設定する</button>
    <input type="hidden" name="session" value="<?php eh($session);?>" />
    <input type="hidden" name="emp_id" value="<?php eh($emp_id);?>" />
    <input type="hidden" name="date" value="<?php eh($date);?>" />
</form>

</body>
</html>
