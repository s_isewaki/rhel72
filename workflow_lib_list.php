<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_workflow_lib_list.ini");
require_once("referer_common.ini");
require_once("workflow_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$checkauth = check_authority($session, 3, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の設定
if ($workflow != "") {
	set_referer($con, $session, "workflow", $workflow, $fname);
	$referer = $workflow;
} else {
	$referer = get_referer($con, $session, "workflow", $fname);
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>ワークフロー | テンプレート一覧</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteWorkflow() {
	if (document.wkfw.elements['del_lib[]'] == undefined) {
		alert('削除可能なデータが存在しません。');
		return;
	}

	var checked = false;

	if (document.wkfw.elements['del_lib[]'] != undefined) {
		if (document.wkfw.elements['del_lib[]'].length == undefined) {
			if (document.wkfw.elements['del_lib[]'].checked) {
				checked = true;
			}
		} else {
			for (var i = 0, j = document.wkfw.elements['del_lib[]'].length; i < j; i++) {
				if (document.wkfw.elements['del_lib[]'][i].checked) {
					checked = true;
					break;
				}
			}
		}
	}

	if (!checked) {
		alert('削除対象が選択されていません。');
		return;
	}

	if (confirm('削除します。よろしいですか？')) {
		document.wkfw.submit();
	}
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/workflow/workflow.css">
<style type="text/css">
.list {border-collapse:collapse;border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table id="header">
<tr>
<td class="icon"><a href="workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><a href="workflow_menu.php?session=<?=$session?>"><b>ワークフロー</b></a></td>
</tr>
</table>
<? } else { ?>
<table id="header">
<tr>
<td class="icon"><a href="application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b08.gif" width="32" height="32" border="0" alt="決裁・申請"></a></td>
<td>
	<a href="application_menu.php?session=<? echo($session); ?>"><b>決裁・申請</b></a> &gt; 
	<a href="workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; 
	<a href="workflow_lib_list.php?session=<? echo($session); ?>"><b>テンプレート</b></a>
</td>
<td class="kanri"><a href="application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></td>
</tr>
</table>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="120"><?show_wkfw_sidemenu($session);?></td>
<td width="5"><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top">

<?
show_wkfw_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="right"><input type="button" value="削除" onclick="deleteWorkflow();"></td></tr>
</table>
<form name="wkfw" action="workflow_lib_delete.php" method="post">
<? show_workflow_lib_list($con,$session,$fname); ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>