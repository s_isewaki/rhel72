<?php
/**
 *  【ファントルくん項目概要・種類・場面・内容（2010版）用関数郡】
 * 
 * 
 *  作成者 ： システム開発部 神野 真彦
 *  作成日 ： 2011/08/26
 */

require_once("./about_postgres.php");
require_once("./hiyari_common.ini");
require_once("./get_values.php");
require_once("./hiyari_report_class.php");

/**
 *  概要の種類・場面・内容が存在するか確認するメソッド
 *  @param Object      $con                DBコネクション
 *  @param String      $super_item_code    概要のコード
 * 
 *  @return   Array    $item_code_flg_list 種類、場面、内容の選択肢があるか確認
 */
function getItemCodeFlgList($con, $super_item_code){
    
    if ($super_item_code != "" ) {
        // *** 種類・場面・内容の存在フラグリスト ***
        $item_code_flg_list = array (
                'kind'      =>  false ,
                'scene'     =>  false ,
                'content'   =>  false ,
        );
        
        // *** SQL設定 ***    
        $sql  = "SELECT DISTINCT item_code FROM inci_item_2010 ";
        $cond = " WHERE super_item_id = {$super_item_code} and disp_flg = true and available = true";
        
        // SQL実行    
        $sel = select_from_table($con, $sql, $cond, $fname);
        
        // SQL検証
        if ($sel == 0)
        {
            pg_close($con);
            echo("<script type='text/javascript' src='js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        
        // レコードの数分ループをまわす
        while ($row = pg_fetch_assoc($sel)) {
            
            // 存在する item_code は true にする ※半角及び全角スペースは除去する。
            $item_code_flg_list[ mb_ereg_replace("(　| )","",$row['item_code']) ] = true ;
        }
           
        // 戻り値設定
        return $item_code_flg_list;
    }
}
