<?php
// 出張旅費
// ワークフロー設定・更新
require_once("about_comedix.php");

$fname = $_SERVER["PHP_SELF"];

// セッションと権限のチェック
$session = qualify_session($_REQUEST["session"], $fname);
$checkauth = check_authority($session, 3, $fname);
if ($session == "0" or $checkauth == "0") {
	js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

foreach ($_REQUEST["wkfw_id"] as $id) {
	// 登録されているかチェック
	$sql = "SELECT * FROM trip_workflow WHERE wkfw_id={$id}";
	$sel = select_from_table($con,$sql,"",$fname);
	if ($sel == 0) {
		js_error_exit();
	}

	// UPDATE	
	if (pg_num_rows($sel) > 0) {
		$set = array(
				"type" => !empty($_REQUEST["type"][$id]) ? p($_REQUEST["type"][$id]) : 0,
				"updated_on" => date('Y-m-d H:i:s')
				);
		$sql = "UPDATE trip_workflow SET ";
		$cond = "WHERE wkfw_id={$id}";
		$upd = update_set_table($con, $sql, array_keys($set), array_values($set), $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			js_error_exit();
		}
	}

	// INSERT
	else {
		$sql = "INSERT INTO trip_workflow (wkfw_id,type,updated_on) values (";
		$content = array(
				$id,
				!empty($_REQUEST["type"][$id]) ? p($_REQUEST["type"][$id]) : 0,
				date('Y-m-d H:i:s')
				);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			js_error_exit();
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

?>
<script type="text/javascript">
location.href = 'workflow_trip_workflow.php?session=<?=$session?>';
</script>