<?
require("about_session.php");
require("about_authority.php");
require("inpatient_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (preg_match("/^\d{8}$/", $date) == 0) {
	echo("<script type=\"text/javascript\">alert('日付は数字8桁で入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
$year = substr($date, 0, 4);
$month = substr($date, 4, 2);
$day = substr($date, 6, 2);
if (!checkdate($month, $day, $year)) {
	echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 入院状況レコードを登録
update_inpatient_condition($con, $pt_id, $date, null, $status, $session, $fname);

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面遷移
if ($mode == "vacant") {
	echo("<script type=\"text/javascript\">location.href = 'bed_info_detail.php?session=$session&mode=vacant&bldg_cd=$bldg_cd&ward_cd=$ward_cd&ptrm_room_no=$ptrm_room_no&dt=$dt&bldgwd1=$bldgwd1&in_yr=$in_yr&in_mon=$in_mon&in_day=$in_day&in_hr=$in_hr&in_min=$in_min&bk_bldg_cd=$bk_bldg_cd';</script>\n");
} else {
	echo("<script type=\"text/javascript\">location.href = 'bed_info_detail.php?session=$session&mode=bldg&bldg_cd=$bldg_cd&ward_cd=$ward_cd&ptrm_room_no=$ptrm_room_no&dt=$dt&bk_bldg_cd=$bk_bldg_cd';</script>\n");
}
?>
