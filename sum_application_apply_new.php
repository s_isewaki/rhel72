<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("sum_application_template.ini");
require_once("yui_calendar_util.ini");
require_once("show_select_values.ini");
require_once("sum_application_imprint_common.ini");
require_once("get_values.ini");
require_once("sum_application_workflow_common_class.php");
//require_once("library_common.php");
require_once("summary_common.ini");
require_once("get_menu_label.ini");

function qurt_sql($v, $def = "null"){
	if (trim($v) == "") return $def;
	return "'" . pg_escape_string($v) . "'";
}

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$summary_check=check_authority($session,57,$fname);
if($summary_check=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$emp_id = get_emp_id($con, $session, $fname);
$ptif_id = $_REQUEST["ptif_id"];
$tmpl_id = $_REQUEST["tmpl_id"];
$ymd =     $_REQUEST["ymd"];
$date_y = substr($ymd, 0, 4);
$date_m = substr($ymd, 4, 2);
$date_d = substr($ymd, 6, 2);

// 承認連番IDと日付が設定されていれば、「新規」ボタンがクリックされているので、該当テンプレートの内容をコピーする必要がある
if (isset($_REQUEST["apply_id"]) && isset($_REQUEST["worksheet_date"]))
{
    $apply_id = $_REQUEST["apply_id"]; // 承認連番ID
    $worksheet_date = $_REQUEST["worksheet_date"];    // 日付

    // 申請テーブルを取得
    $row_apply = getSumApplyTable($con, $apply_id, $fname);
    $summary_id = $row_apply["summary_id"];
    $apply_date_ymd = substr($row_apply["apply_date"], 0, 8);
    
    // サマリテーブルを取得
    $row_summary = getSummaryTable($con, $summary_id, $ptif_id, $fname);
    $summary_seq = $row_summary["summary_seq"];
    
    // XMLファイル名を取得
    $xml_file_name = getXmlFileName($con, $emp_id, $ptif_id, $summary_id, $summary_seq, $apply_id, $fname);
}

// テンプレートマスタの取得
$tmplmst = select_from_table($con, "select * from tmplmst where tmpl_id = ".(int)@$tmpl_id, "", $fname);
$tmpl_name = @pg_fetch_result($tmplmst, 0, "tmpl_name");

// ワークフローマスタの取得
$wkfwmst = select_from_table($con, "select * from sum_wkfwmst where tmpl_id = ".(int)@$tmpl_id, "", $fname);
$wkfw_id = @pg_fetch_result($wkfwmst, 0, "wkfw_id");
$wkfw_enabled = @pg_fetch_result($wkfwmst, 0, "wkfw_enabled");

// 送信先職員リストの取得
$obj = new application_workflow_common_class($con, $fname);
if($wkfw_enabled) $arr_wkfwapv = $obj->get_wkfwapv_info($wkfw_id, $emp_id, $ptif_id);
$approve_num = @count($arr_wkfwapv);   // 受信者数

// テンプレートから、診療区分の取得
$sel_div = select_from_table($con, "select div_id from tmpl_div_link where tmpl_id = ".(int)@$tmpl_id, "", $fname);
$div_id = pg_fetch_result($sel_div, 0, "div_id");

// 診療科一覧を取得
// ログインユーザが担当医の場合、所属している診療科を取得
$sql =
	" select sect_id, sect_nm from (".
	"   select * from (".
	"     select *, case when sect_sort_order = '' then null else sect_sort_order end as sort_order from sectmst".
	"   ) d".
	" ) d".
	" where sect_del_flg = 'f' and (sect_hidden_flg is null or sect_hidden_flg <> 1) order by sort_order, sect_id";
$sel_sect = select_from_table($con, $sql, "", $fname);
$sects = pg_fetch_all($sel_sect);
if (@$sect_id == "") {
	$sel = select_from_table($con, "select sect_id from drmst where dr_emp_id = '$emp_id' and dr_del_flg = 'f'", "", $fname);
	if (pg_num_rows($sel) > 0) $sect_id = pg_fetch_result($sel, 0, "sect_id");
}




?>
<title>CoMedix <?=get_report_menu_label($con, $fname)?> | 送信オーダ作成</title>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>

<? write_yui_calendar_use_file_read_0_12_2(); ?>
<? write_yui_calendar_script2(1); ?>
<script type="text/javascript">

function set_regist_emp_id(obj, idx) {
	var emp_id = 'regist_emp_id' + idx;
	document.apply.elements[emp_id].value = obj.value;
}

function attachFile() {
	window.open('sum_apply_attach.php?session=<? echo($session); ?>', 'newwin2', 'width=640,height=480,scrollbars=yes');
}

function detachFile(e) {
	if (e == undefined) e = window.event;
	var btn_id = (e.target ? e.target.getAttribute('id') : e.srcElement.id);
	var id = btn_id.replace('btn_', '');
	var p = document.getElementById('p_' + id);
	document.getElementById('attach').removeChild(p);
}

function init() {
}

function sinryoka_ymd_changed(id){
	if (confirm('診療科/年月日を変更する場合、入力したテンプレートの内容をクリアしますがよろしいですか？')) {
		var y = document.getElementById("date_y1").value;
		var m = document.getElementById("date_m1").value;
		var d = document.getElementById("date_d1").value;
		document.touroku.ymd.value = (y*10000 + m*100 + d*1);
		document.touroku.action = "sum_application_apply_new.php";
		document.touroku.method = "get";
		document.touroku.submit();
	}	else {
		for(i=0; i<document.touroku.sel_diag.options.length; i++)	{
			if(document.touroku.sel_diag.options[i].text == document.touroku.diag.value) {
				document.touroku.sel_diag.selectedIndex = i;
				break;
			}
		}
	}
}

function callback_template_registed(){
	document.touroku.submit();
}

// 送信処理
function apply_regist(apv_num) {

	// 受信者が全く存在しない場合
  var regist_flg = false;
	for(i=1; i<=apv_num; i++) {
		emp_id = document.touroku.elements["regist_emp_id"+i].value;
		if(emp_id != "") {
			regist_flg = true;
			break;
		}
	}
	if(!regist_flg) {
		alert('受信者が存在しないため、送信できません。');
		return;
	}

	// 重複チェック
	dpl_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj_src = 'regist_emp_id' + i;
		emp_id_src = document.touroku.elements[obj_src].value;
		if(emp_id_src == "") {
			continue;
		}
		for(j=i+1; j<=apv_num; j++) {
			obj_dist = 'regist_emp_id' + j;
			emp_id_dist = document.touroku.elements[obj_dist].value;
			if(emp_id_src == emp_id_dist) {
				dpl_flg = true;
			}
		}
	}

	if(dpl_flg) {
		if (!confirm('受信者が重複していますが、送信しますか？')) {
			return;
		}
	}
	var y = document.getElementById("date_y1").value;
	var m = document.getElementById("date_m1").value;
	var d = document.getElementById("date_d1").value;
	document.touroku.cre_date.value = (y*10000 + m*100 + d*1);
	document.touroku.action = "summary_insert.php";
	document.touroku.method = "post";
	document.touroku.is_regist_and_send.value = "1";

	// テンプレートの中のフォームをサブミット
	iframe_template.document.tmplform.action = "create_tmpl_xml.php?session=<?=$session?>";
	iframe_template.document.tmplform.submit();
//	document.touroku.submit();
}
</script>
</head>
<body onload="init();initcal();
if (window.OnloadSub) { OnloadSub(); };if (window.refreshApproveOrders) {refreshApproveOrders();}">
<form name="touroku" action="#" method="get">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="tmpl_id" value="<?=$tmpl_id?>">
<input type="hidden" name="ptif_id" value="<?=$ptif_id?>">
<input type="hidden" name="tmpl_id" value="<?=$tmpl_id?>">
<input type="hidden" name="sel_diag" value="<?=$div_id?>">
<input type="hidden" name="wkfw_id" value="<?=$wkfw_id?>">
<input type="hidden" name="xml_file" value="wk_<?=$emp_id?>.xml">
<input type="hidden" name="pt_id" value="<?=$ptif_id?>">
<input type="hidden" name="cre_date" value="<?=$ymd?>">
<input type="hidden" name="ymd" value="<?=$ymd?>">
<input type="hidden" name="direct_registration" value="1">
<input type="hidden" name="is_regist_and_send" value="1">
<input type="hidden" name="summary_naiyo" value="">
<input type="hidden" name="priority" value="1">
<input type="hidden" name="outreg" value="">
<input type="hidden" id="summary_problem" name="summary_problem" value="">
<input type="hidden" name="seiki_emp_id" value="<?=$emp_id?>">
<input type="hidden" name="dairi_emp_id" value="">

<div style="padding:4px">


<!-- ヘッダエリア -->
<?= summary_common_show_dialog_header("送信オーダ作成"); ?>
<div style="margin-top:4px; text-align:right">
	<input type="button" onclick="apply_regist('<?=$approve_num?>');" value="登録＋送信"/>
</div>





<table class="prop_table" style="margin-top:4px">
	<?//------------------------------------?>
	<?// 診療科                             ?>
	<?//------------------------------------?>
	<tr>
		<th>診療科</th>
		<td>
			<select name="sect_id" id="sect_id" onchange="sinryoka_ymd_changed(this.id)">
			<? foreach ($sects as $tmp_sect) { ?>
				<option value="<?=$tmp_sect["sect_id"]?>" <?= $tmp_sect["sect_id"] == @$sect_id ? "selected" : ""?>><?=$tmp_sect["sect_nm"]?>
			<? } ?>
			</select>
		</td>
	</tr>
	<?//------------------------------------?>
	<?// プロブレム                         ?>
	<?//------------------------------------?>
	<tr>
		<th>プロブレム</t>
		<td>
			<select id='problem_id' name='problem_id' onChange="set_summary_problem();">
			<option value="0">#0 未選択</option>
			<?
			//==============================
			//プロブレムリスト取得
			//==============================
			$sql = "select summary_problem_list.* from summary_problem_list ";
			$cond = "where summary_problem_list.ptif_id = '$ptif_id'";
			$cond .= " order by summary_problem_list.problem_list_no";
			$sel_plist = select_from_table($con, $sql, $cond, $fname);
			if ($sel_plist == 0) {
				pg_close($con);
				require_once("summary_common.ini");
				summary_common_show_error_page_and_die();
			}
			$num_plist = pg_numrows($sel_plist);
			for($i=0;$i<$num_plist;$i++) {
				$pl_problem_id = pg_result($sel_plist,$i,"problem_id");
				$pl_problem_list_no = pg_result($sel_plist,$i,"problem_list_no");
				$pl_problem = pg_result($sel_plist,$i,"problem");
				$pl_problem_value = "#$pl_problem_list_no $pl_problem";
				$pl_selected = "";
				if(@$problem_id == @$pl_problem_id)
				{
					$pl_selected = "selected";
				}
				?>
				<option value="<?=$pl_problem_id?>" <?=$pl_selected?>><?=$pl_problem_value?></option>
				<?
			}
			?>
			</select>
			<script type="text/javascript">
			set_summary_problem();
			function set_summary_problem() {
				var pl_index = document.touroku.problem_id.selectedIndex;
				var pl_value = document.touroku.problem_id.options[pl_index].text;
				document.touroku.summary_problem.value = pl_value;
			}
			</script>
		</td>
	</tr>
	<?//------------------------------------?>
	<?// 年月日                             ?>
	<?//------------------------------------?>
	<tr>
		<th>年月日</td>
		<td>
			<div style="float:left">
				<select id='date_y1' name='sel_yr' onchange="sinryoka_ymd_changed(this.id)"><? show_select_years(15, $date_y); ?></select> 年
				<select id='date_m1' name='sel_mon' onchange="sinryoka_ymd_changed(this.id)"><? show_select_months($date_m); ?></select> 月
				<select id='date_d1' name='sel_day' onchange="sinryoka_ymd_changed(this.id)"><? show_select_days($date_d); ?></select> 日
			</div>
			<div style="float:left; font-size:12px">
				<img src="img/calendar_link.gif" style="cursor:pointer;" onclick="show_cal1();"/>
				<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
		  </div>
			<div style="clear:both"></div>
		</td>
	</tr>
</table>

<?//------------------------------------?>
<?// テンプレート                       ?>
<?//------------------------------------?>
<div style="padding:3px; border-top:1px solid #d2eafd; background-color:#bee0fc; margin-top:4px"><?=$tmpl_name?></div>


<table class="prop_table" style="width:100%;"><tr><td>
<?
$tmpl_title =urlencode($tmpl_name);
if (!empty($xml_file_name)) {    // XMLファイル名がセットされていれば、テンプレート内容をコピーして新規登録画面を開く
    $html = "<iframe src=\"summary_tmpl_read.php"
          . "?session=$session"
          . "&pt_id=$ptif_id"
          . "&emp_id=$emp_id"
          . "&tmpl_copy_xml_file=$xml_file_name"
          . "&tmpl_id=$tmpl_id"
          . "&div_id=$div_id"
          . "&cre_date=$ymd"
          . "&direct_registration=1"
          . "&tmpl_title=$tmpl_title"
          . "&iframe=iframe_template"
          . "&update_flg=new"
          . "&auto_parent_resize=1\""
          . " name=\"iframe_template\" id=\"iframe_template\" style=\"width:100%; height:300px;\" frameborder=\"0\" scrolling=\"no\"></iframe>";
} else {
    $html = "<iframe src=\"summary_tmpl_read.php"
          . "?session=$session"
          . "&pt_id=$ptif_id"
          . "&emp_id=$emp_id"
          . "&tmpl_id=$tmpl_id"
          . "&div_id=$div_id"
          . "&cre_date=$ymd"
          . "&update_flg=new"
          . "&direct_registration=1"
          . "&tmpl_title=$tmpl_title"
          . "&iframe=iframe_template"
          . "&auto_parent_resize=1\""
          . " name=\"iframe_template\" id=\"iframe_template\" style=\"width:100%; height:300px;\" frameborder=\"0\" scrolling=\"no\"></iframe>";
}
echo $html;
?>
</td></tr></table>

<?//------------------------------------?>
<?// 添付ファイルと送信先の選択         ?>
<?//------------------------------------?>
<div style="margin-top:4px">
<? show_application_template($con, $session, $fname, @$arr_wkfwapv, $wkfw_id, @$apply_title, @$content, @$file_id, @$filename, @$back, @$mode, "", 1); ?></div>

<?//------------------------------------?>
<?// 送信ボタン                         ?>
<?//------------------------------------?>
<div style="margin-top:4px; text-align:right">
	<input type="button" onclick="apply_regist('<?=$approve_num?>');" value="登録＋送信"/>
</div>

</div>
</form>
</body>
</html>
<?
/**
 * 申請テーブルを取得
 * 
 * @param resource $con      データベース接続リソース
 * @param string   $apply_id 承認連番ID
 * @param string   $fname    画面名
 * 
 * @return array 取得したレコード
 */
function getSumApplyTable($con, $apply_id, $fname)
{
    $rs_apply = select_from_table($con, "select * from sum_apply where apply_id = " . $apply_id, "", $fname);
    $row_apply = pg_fetch_array($rs_apply);
    return $row_apply;
}

/**
 * サマリテーブルを取得
 * 
 * @param resource $con        データベース接続リソース
 * @param integer  $summary_id サマリID
 * @param string   $ptif_id    患者ID
 * @param string   $fname      画面名
 * 
 * @return array 取得したレコード
 */
function getSummaryTable($con, $summary_id, $ptif_id, $fname)
{
    $rs_summary = select_from_table($con, "select * from summary where summary_id = ".$summary_id." and ptif_id = '".$ptif_id."'", "", $fname);
    $row_summary = pg_fetch_array($rs_summary);
    return $row_summary;
}

/**
 * XMLファイル名を取得
 * 
 * @param resource $con         データベース接続リソース
 * @param string   $emp_id      登録者ID
 * @param string   $ptif_id     患者ID
 * @param integer  $summary_id  サマリID
 * @param integer  $summary_seq サマリ連番
 * @param string   $apply_id    承認連番ID
 * @param string   $fname       画面名
 * 
 * @return string XMLファイル名
 */
function getXmlFileName($con, $emp_id, $ptif_id, $summary_id, $summary_seq, $apply_id, $fname)
{
    $xml_file_name = $emp_id . "_" . $ptif_id . "_" . $summary_id . ".xml";    // XMLファイル取得(2010/1まで)
    // XMLファイル取得(2010/2から)
    if ($summary_seq != "") {
        $sql = "select xml_file_name from sum_xml where xml_file_name = '".$summary_seq."_".$apply_id.".xml'";
        $rs = select_from_table($con, $sql, "", $fname);
        $tmp_xml_file_name = @pg_fetch_result($rs, 0, "xml_file_name");
        if ($tmp_xml_file_name != "") {
            $xml_file_name = $tmp_xml_file_name;
        }
    }
    return $xml_file_name;
}
?>
