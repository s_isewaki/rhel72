<?
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_comedix.php");

$fname=$PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$auth_id = ($frompage == "1") ? 46 : 24;
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ユーザ画面の一覧へ移動
if ($frompage == "1" && $sele_newscate != "") {
	echo("<script type=\"text/javascript\">location.href = 'newsuser_menu.php?session=$session&sele_newscate=$sele_newscate';</script>");
}

// 入力チェック
if (!is_array($news_ids)) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('削除対象が選択されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションを開始
pg_query($con, "begin");

// チェックされたお知らせをループ
foreach ($news_ids as $tmp_news_id) {

	// ユーザ画面の自己登録分の場合
	if ($frompage == "1") {

		// お知らせ情報を削除
		$sql = "delete from news";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// お知らせ配信先部署情報を削除
		$sql = "delete from newsdept";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// お知らせ職種情報を削除
		$sql = "delete from newsjob";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// お知らせ役職情報を削除
		$sql = "delete from newsst";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// お知らせ参照情報を削除
		$sql = "delete from newsref";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// お知らせアンケート情報を削除
		$sql = "delete from newsenquet_q";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$sql = "delete from newsenquet_a";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$sql = "delete from newsanswer";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// お知らせ督促メール情報を削除
		$sql = "delete from newsmail";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 添付ファイル情報を削除
		$sql = "delete from newsfile";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	} else {
		// 管理画面ではお知らせ情報の削除フラグをtrueに更新
		$sql = "update news set";
		$set = array("news_del_flg");
		$setvalue = array('t');
		$cond = "where news_id = $tmp_news_id";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// ユーザ画面の自己登録分の場合
if ($frompage == "1") {
	// 添付ファイルの削除
	foreach ($news_ids as $tmp_news_id) {
		foreach (glob("news/{$tmp_news_id}_*.*") as $tmpfile) {
			unlink($tmpfile);
		}
	}
}
foreach (glob("news/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}

// 30分以上前に保存された一時ファイルを削除
foreach (glob("news/tmp/*.*") as $tmpfile) {
	if (time() - filemtime($tmpfile) >= 30 * 60) {
		unlink($tmpfile);
	}
}

// ユーザ画面の自己登録分からの遷移時
if ($frompage == "1") {
	echo("<script type=\"text/javascript\">location.href = 'newsuser_list.php?session=$session&page=$page';</script>");

// 管理画面のお知らせ検索結果からの遷移時
} else if ($frompage == "2") {
?>
<body>
<form name="mainform" action="news_search.php" method="post">
<input type="hidden" name="session" value="<? eh($session); ?>">
<input type="hidden" name="srch_flg" value="t">
<input type="hidden" name="srch_news_title" value="<? eh($srch_news_title); ?>">
<input type="hidden" name="srch_news" value="<? eh($srch_news); ?>">
<input type="hidden" name="srch_news_category" value="<? eh($srch_news_category); ?>">
<input type="hidden" name="srch_class_id" value="<? eh($srch_class_id); ?>">
<input type="hidden" name="srch_job_id" value="<? eh($srch_job_id); ?>">
<input type="hidden" name="srch_st_id" value="<? eh($srch_st_id); ?>">
<input type="hidden" name="srch_notice_id" value="<? eh($srch_notice_id); ?>">
<input type="hidden" name="srch_notice2_id" value="<? eh($srch_notice2_id); ?>">
<input type="hidden" name="srch_src_class_id" value="<? eh($srch_src_class_id); ?>">
<input type="hidden" name="srch_src_atrb_id" value="<? eh($srch_src_atrb_id); ?>">
<input type="hidden" name="srch_src_dept_id" value="<? eh($srch_src_dept_id); ?>">
<input type="hidden" name="srch_src_room_id" value="<? eh($srch_src_room_id); ?>">
<input type="hidden" name="src_emp_id" value="<? eh($src_emp_id); ?>">
<input type="hidden" name="srch_news_date_from1" value="<? eh($srch_news_date_from1); ?>">
<input type="hidden" name="srch_news_date_from2" value="<? eh($srch_news_date_from2); ?>">
<input type="hidden" name="srch_news_date_from3" value="<? eh($srch_news_date_from3); ?>">
<input type="hidden" name="srch_news_date_to1" value="<? eh($srch_news_date_to1); ?>">
<input type="hidden" name="srch_news_date_to2" value="<? eh($srch_news_date_to2); ?>">
<input type="hidden" name="srch_news_date_to3" value="<? eh($srch_news_date_to3); ?>">
<input type="hidden" name="srch_news_date_login_from1" value="<? eh($srch_news_date_login_from1); ?>">
<input type="hidden" name="srch_news_date_login_from2" value="<? eh($srch_news_date_login_from2); ?>">
<input type="hidden" name="srch_news_date_login_from3" value="<? eh($srch_news_date_login_from3); ?>">
<input type="hidden" name="srch_news_date_login_to1" value="<? eh($srch_news_date_login_to1); ?>">
<input type="hidden" name="srch_news_date_login_to2" value="<? eh($srch_news_date_login_to2); ?>">
<input type="hidden" name="srch_news_date_login_to3" value="<? eh($srch_news_date_login_to3); ?>">
<input type="hidden" name="srch_emp_id" value="<? eh($srch_emp_id); ?>">
<input type="hidden" name="srch_news_date1" value="<? eh($srch_news_date1); ?>">
<input type="hidden" name="srch_news_date2" value="<? eh($srch_news_date2); ?>">
<input type="hidden" name="srch_news_date3" value="<? eh($srch_news_date3); ?>">
</form>
<script type="text/javascript">
document.mainform.submit();
</script>
<body>
<?
// 管理画面のお知らせ一覧からの遷移時
} else {
	echo("<script type=\"text/javascript\">location.href = 'news_menu.php?session=$session&page=$page';</script>");
}
