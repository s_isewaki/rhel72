<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ | お知らせ対象者</title>
<?
require_once("about_comedix.php");
require_once("show_class_name.ini");
require_once("label_by_profile_type.ini");
require_once("get_cas_title_name.ini");
require_once("get_menu_label.ini");

$fname = $PHP_SELF;

$detail = "お知らせ対象者";

$session = ($tmp_session != '') ? $tmp_session : $session[0];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 46, $fname);
if ($checkauth == "0") {
	$checkauth = check_authority($session, 24, $fname);
	if ($checkauth == "0") {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
}

// 並び順のデフォルトは職員IDの昇順
if ($emp_o == "") {$emp_o = "1";}

// データベースに接続
$con = connect2db($fname);
// 組織階層情報を取得
$arr_class_name = get_class_name_array($con, $fname);

// 職種一覧を取得
$sql = "select job_id, job_nm from jobmst";
$cond = "where job_del_flg = 'f' order by order_no";
$sel_job = select_from_table($con, $sql, $cond, $fname);
if ($sel_job == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// array_filter()のための空配列チェック
function fTrim($var){
	return($var != '');
}

// $st_idから空の配列だけ除外
$st_id = array_filter($st_id, "fTrim");

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function clearOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function postSend(emp_o, page) {
	if (typeof page !== 'undefined') {
		document.list.page.value = page;
	}
	document.list.emp_o.value = emp_o;
	document.list.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list th {border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">

<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?php echo $detail; ?></b></font></td>

<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<div style="background-color: white;">
<form name="list" action="news_employee_list.php" method="post">
<input type="hidden" name="tmp_session" value="<? echo $session; ?>">
<input type="hidden" name="emp_o">
<input type="hidden" name="page">
<input type="hidden" name="category" value="<? echo $category[0]; ?>">
<? foreach ($st_id as $tmp_st_id) { ?>
<input type="hidden" name="st_id[]" value="<? echo $tmp_st_id; ?>">
<? } ?>
<? if ($category[0] == "2") { ?>
<? foreach ($class_id as $tmp_class_id) { ?>
<input type="hidden" name="class_id[]" value="<? echo $tmp_class_id; ?>">
<? } ?>
<? foreach ($atrb_id as $tmp_atrb_id) { ?>
<input type="hidden" name="atrb_id[]" value="<? echo $tmp_atrb_id; ?>">
<? } ?>
<? foreach ($dept_id as $tmp_dept_id) { ?>
<input type="hidden" name="dept_id[]" value="<? echo $tmp_dept_id; ?>">
<? } ?>
<? } else if ($category[0] == "3") { ?>
<? foreach ($job_id as $tmp_job_id) { ?>
<input type="hidden" name="job_id[]" value="<? echo $tmp_job_id; ?>">
<? } ?>
<? } ?>
</form>
<? show_next($category[0], $class_id, $atrb_id, $dept_id, $job_id, $st_id, $emp_o, $page, $fname, $con); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<th><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
職員ID
</font></th>
<th><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($emp_o != "1") { ?>
<span style="cursor: pointer" onclick="postSend(1<? if ($page != '') {echo ", ".$page; } ?>)"><u style="color : blue">職員名</u></span>
<!--a href="news_employee_list.php?session=<?php echo $session; ?>&job_id=<?php echo $job_id; ?>&page=<?php echo $page; ?>&emp_o=1">職員名</a-->
<? } else { ?>
<span style="cursor: pointer" onclick="postSend(2<? if ($page != '') {echo ", ".$page; } ?>)"><u style="color : blue">職員名</u></span>
<? } ?>
</font></th>
<th><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($emp_o != "3") { ?>
<span style="cursor: pointer" onclick="postSend(3<? if ($page != '') {echo ", ".$page; } ?>)"><u style="color : blue">所属</u></span>
<? } else { ?>
<span style="cursor: pointer" onclick="postSend(4<? if ($page != '') {echo ", ".$page; } ?>)"><u style="color : blue">所属</u></span>
<? } ?>
</font></th>
<th><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($emp_o != "5") { ?>
<span style="cursor: pointer" onclick="postSend(5<? if ($page != '') {echo ", ".$page; } ?>)"><u style="color : blue">職種</u></span>
<? } else { ?>
<span style="cursor: pointer" onclick="postSend(6<? if ($page != '') {echo ", ".$page; } ?>)"><u style="color : blue">職種</u></span>
<? } ?>
</font></th>
<th><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($emp_o != "7") { ?>
<span style="cursor: pointer" onclick="postSend(7<? if ($page != '') {echo ", ".$page; } ?>)"><u style="color : blue">役職</u></span>
<? } else { ?>
<span style="cursor: pointer" onclick="postSend(8<? if ($page != '') {echo ", ".$page; } ?>)"><u style="color : blue">役職</u></span>
<? } ?>
</font></th>
</tr>
<?
show_emp_list($category[0], $class_id, $atrb_id, $dept_id, $job_id, $st_id, $emp_o, $page, $fname, $con);
?>
<? pg_close($con); ?>
</table>
<br />
</div>
</body>
</html>
<?

// [姓][名][第一階層][第二階層][第三階層][第四階層][権限グループID][権限グループ名][利用停止フラグ]を取得
function show_emp_list($category, $class_id, $atrb_id, $dept_id, $job_id, $st_id, $emp_o, $page, $fname, $con) {
	$limit = 20;

	$sql = "select empmst.emp_personal_id,emp_lt_nm,emp_ft_nm,classmst.class_nm,atrbmst.atrb_nm,deptmst.dept_nm,stmst.st_nm,jobmst.job_nm from empmst";
	$sql .= " left join classmst on classmst.class_id = empmst.emp_class";
	$sql .= " left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute";
	$sql .= " left join deptmst on deptmst.dept_id = empmst.emp_dept";
	$sql .= " left join stmst on stmst.st_id = empmst.emp_st";
	$sql .= " left join jobmst on jobmst.job_id = empmst.emp_job";
	$sql .= " left join authmst on authmst.emp_id = empmst.emp_id";

	$cond = "where ";

	if ($category == '2') {
		// 各階層を判定
		for ($i = 0; $i < count($class_id); $i++) {
			if ($i != 0) {
				$cond .= " or ";
			}
			$cond .= "(authmst.emp_del_flg = 'f' and (authmst.emp_newsuser_flg = 't' or authmst.emp_news_flg = 't') and empmst.emp_class = '{$class_id[$i]}'";
			if ($atrb_id[$i] != '0') {
				$cond .= " and empmst.emp_attribute = '{$atrb_id[$i]}'";
			}
			if ($dept_id[$i] != '0') {
				$cond .= " and empmst.emp_dept = '{$dept_id[$i]}'";
			}
			$cond .= " and empmst.emp_st in(";
			$cond .= implode(",", $st_id);
			$cond .= "))";
		}
	} else if ($category == '3') {
		// 職種を判定
		$cond .= "authmst.emp_del_flg = 'f' and (authmst.emp_newsuser_flg = 't' or authmst.emp_news_flg = 't')";
		$cond .= " and empmst.emp_job in(";
		$cond .= implode(",", $job_id);
		$cond .= ") ";

		$cond .= " and empmst.emp_st in(";
		$cond .= implode(",", $st_id);
		$cond .= ")";
	}

	if ($emp_o == "1") {
		$cond .= " order by emp_kn_lt_nm,emp_kn_ft_nm";
	} else if ($emp_o == "2") {
		$cond .= " order by emp_kn_lt_nm desc,emp_kn_ft_nm desc";
	} else if ($emp_o == "3") {
		$cond .= " order by classmst.order_no,atrbmst.order_no,deptmst.order_no";
	} else if ($emp_o == "4") {
		$cond .= " order by classmst.order_no desc,atrbmst.order_no desc,deptmst.order_no desc";
	} else if ($emp_o == "5") {
		$cond .= " order by job_id";
	} else if ($emp_o == "6") {
		$cond .= " order by job_id desc";
	} else if ($emp_o == "7") {
		$cond .= " order by st_id";
	} else if ($emp_o == "8") {
		$cond .= " order by st_id desc";
	} else if ($emp_o == "9") {
		$cond .= " order by group_id";
	} else {
		$cond .= " order by group_id desc";
	}

	$cond .= " limit $limit";
	if ($page > 0) {
		$offset = $page * $limit;
		$cond .= " offset $offset";
	}

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 職員表示
	while($row = pg_fetch_array($sel)){
		$emp_personal_id = $row["emp_personal_id"];
		$emp_lt_nm = $row["emp_lt_nm"];
		$emp_ft_nm = $row["emp_ft_nm"];
		$tmp_position = "{$row["class_nm"]}＞{$row["atrb_nm"]}＞{$row["dept_nm"]}";
		$job_nm = $row["job_nm"];
		$st_nm = $row["st_nm"];

		echo("<tr>");
		echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$emp_personal_id</font></td>");
		echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$emp_lt_nm $emp_ft_nm</font></td>");
		echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_position</font></td>");
		echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$job_nm</font></td>");
		echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$st_nm</font></td>");
		echo("</tr>");
	}
}

function show_next($category, $class_id, $atrb_id, $dept_id, $job_id, $st_id, $emp_o, $page, $fname, $con) {
	$limit = 20;

	$sql = "select empmst.* from empmst";
	$sql .= " left join classmst on classmst.class_id = empmst.emp_class";
	$sql .= " left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute";
	$sql .= " left join deptmst on deptmst.dept_id = empmst.emp_dept";
	$sql .= " left join classroom on classroom.room_id = empmst.emp_room";
	$sql .= " left join stmst on stmst.st_id = empmst.emp_st";
	$sql .= " left join jobmst on jobmst.job_id = empmst.emp_job";
	$sql .= " left join authmst on authmst.emp_id = empmst.emp_id";

	$cond = "where ";

	if ($category == 2) {
		// 各階層を判定
		for ($i = 0; $i < count($class_id); $i++) {
			if ($i != 0) {
				$cond .= " or ";
			}
			$cond .= "(authmst.emp_del_flg = 'f' and (authmst.emp_newsuser_flg = 't' or authmst.emp_news_flg = 't') and empmst.emp_class = '{$class_id[$i]}'";
			if ($atrb_id[$i] != '0') {
				$cond .= " and empmst.emp_attribute = '{$atrb_id[$i]}'";
			}
			if ($dept_id[$i] != '0') {
				$cond .= " and empmst.emp_dept = '{$dept_id[$i]}'";
			}
			$cond .= " and empmst.emp_st in(";
			$cond .= implode(",", $st_id);
			$cond .= "))";
		}
	} else if ($category == 3) {
		// 職種を判定
		$cond .= "authmst.emp_del_flg = 'f' and (authmst.emp_newsuser_flg = 't' or authmst.emp_news_flg = 't')";
		$cond .= " and empmst.emp_job in(";
		$cond .= implode(",", $job_id);
		$cond .= ")";

		$cond .= " and empmst.emp_st in(";
		$cond .= implode(",", $st_id);
		$cond .= ");";
	}

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$emp_count = intval(pg_numrows($sel));

	$total_page = ceil($emp_count / $limit);

	if ($total_page <= 1) {
		return;
	}

	echo("<table align=\"right\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">\n");
	echo("<tr>\n");
	echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
	echo("ページ&nbsp;");
	$t = 10;
	if ($page > 0) {
		echo "<span style=\"cursor: pointer\" onclick=\"postSend({$emp_o}, 0)\"><u style=\"color : blue\">&lt;&lt;|</u></span>&nbsp;\n";
		echo "<span style=\"cursor: pointer\" onclick=\"postSend({$emp_o}, ".($page - 1).")\"><u style=\"color : blue\">←</u></span>&nbsp;\n";
	} else {
		echo("&nbsp;");
	}
	$min = $page - 4;
	$max = $page + 4;
	if(($total_page - 1) < $max){
		$max = ($max - ($total_page - 1)) + 4;
	} else {
		$max = 4;
	}

	if(0 > $min){
		$min = (0 - $min) + 4;
	} else {
		$min = 4;
	}

	for($i = $max; $i > 0; $i--){
		if(($page - $i) >= 0){
			echo "<span style=\"cursor: pointer\" onclick=\"postSend({$emp_o}, ".($page - $i).")\"><u style=\"color : blue\">".(($page - $i) + 1)."</u></span>&nbsp;\n";
		}
	}

	echo("[".($page + 1)."]&nbsp;");

	for($i = 1; $i <= $min; $i++){
		if(($page + $i) <= ($total_page - 1)){
			echo "<span style=\"cursor: pointer\" onclick=\"postSend({$emp_o}, ".($page + $i).")\"><u style=\"color : blue\">".(($page + $i) + 1)."</u></span>&nbsp;\n";
		}
	}
	if ($page < ($total_page - 1)) {
		echo "<span style=\"cursor: pointer\" onclick=\"postSend({$emp_o}, ".($page + 1).")\"><u style=\"color : blue\">→</u></span>&nbsp;\n";
		echo "<span style=\"cursor: pointer\" onclick=\"postSend({$emp_o}, ".($total_page - 1).")\"><u style=\"color : blue\">&gt;&gt;|</u></span>&nbsp;\n";
	} else {
		echo("&nbsp;");
	}
	echo("</font></td>\n");
	echo("</tr>\n");
	echo("</table><br />\n");
}
?>
