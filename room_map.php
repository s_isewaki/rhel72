<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理 | 表示設定 | 病床マップ</title>
<?
require("about_authority.php");
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 21, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

$default_url = ($section_admin_auth == "1") ? "entity_menu.php" : "building_list.php";

// データベースに接続
$con = connect2db($fname);

// 棟一覧を取得
$sql = "select bldg_cd, bldg_name from bldgmst";
$cond = "where bldg_del_flg = 'f' order by bldg_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$buildings = array();
while ($row = pg_fetch_array($sel)) {
	$buildings[$row["bldg_cd"]] = $row["bldg_name"];
}
unset($sel);

// 指定棟の病棟一覧を取得
if ($bldg_cd != "") {
	$sql = "select ward_cd, ward_name from wdmst";
	$cond = "where bldg_cd = $bldg_cd and ward_del_flg = 'f' order by ward_cd";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$wards = array();
	while ($row = pg_fetch_array($sel)) {
		$wards[$row["ward_cd"]] = $row["ward_name"];
	}
	unset($sel);
}

// 病棟が選択されている場合
if ($ward_cd != "") {

	// 病室一覧を取得
	$sql = "select ptrm_room_no, ptrm_name from ptrmmst";
	$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd and ptrm_del_flg = 'f' order by ptrm_room_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$rooms = array();
	$rooms["ns"] = array("name" => "ナースステーション");
	$rooms["wc"] = array("name" => "トイレ");
	while ($row = pg_fetch_array($sel)) {
		$rooms[$row["ptrm_room_no"]] = array("name" => $row["ptrm_name"]);
	}
	unset($sel);

	// 列数・行数が未入力の場合
	if ($column_count == "" || $row_count == "") {

		// 列数・行数情報を取得
		$sql = "select column_count, row_count from ptrmmapboard";
		$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0) {
			$column_count = pg_fetch_result($sel, 0, "column_count");
			$row_count = pg_fetch_result($sel, 0, "row_count");

		// 未登録の場合
		} else {

			// 共通のレイアウト情報から1行あたりの病室数を取得
			$sql = "select room_count1 from roomlayout";
			$cond = "";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			if (pg_num_rows($sel) > 0) {
				$column_count = pg_fetch_result($sel, 0, "room_count1");
			} else {
				$column_count = "8";
			}

			// 行数を計算
			$row_count = ceil(count($rooms) / $column_count);
		}
		unset($sel);
	}

	// マップ配列を初期化
	$maps = array();
	for ($r = 1; $r <= $row_count; $r++) {
		for ($c = 1; $c <= $column_count; $c++) {
			$maps[$r][$c] = null;
		}
	}

	// 病床マップ情報を取得
	$sql = "select row_no, column_no, room_type, ptrm_room_no from ptrmmap";
	$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// マップ配列に反映（病室が削除されていない場合のみ）
	while ($row = pg_fetch_array($sel)) {
		$tmp_row_no = $row["row_no"];
		$tmp_column_no = $row["column_no"];
		$tmp_room_type = $row["room_type"];
		switch ($tmp_room_type) {
		case "1":
			$tmp_room_no = $row["ptrm_room_no"];
			break;
		case "2":
			$tmp_room_no = "ns";
			break;
		case "3":
			$tmp_room_no = "wc";
			break;
		}

		if (array_key_exists($tmp_room_no, $rooms) &&
					intval($tmp_row_no) <= intval($row_count) &&
					intval($tmp_column_no) <= intval($column_count)
		) {
			$maps[$tmp_row_no][$tmp_column_no] = array(
				"room_no" => $tmp_room_no,
				"name" => $rooms[$tmp_room_no]["name"]
			);
			$rooms[$tmp_room_no]["in_map"] = true;
		} else {
			$rooms[$tmp_room_no]["in_map"] = false;
		}
	}
	unset($sel);
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/yui/3.10.3/build/yui/yui-min.js"></script>
<script type="text/javascript">
function changeWardOptions() {
	var bldg_cd = document.condform.bldg_cd.value;
	location.href = 'room_map.php?session=<? echo($session); ?>&bldg_cd=' + bldg_cd;
}

function calculate() {
	var bldg_cd = document.condform.bldg_cd.value;
	var ward_cd = document.condform.ward_cd.value;
	location.href = 'room_map.php?session=<? echo($session); ?>&bldg_cd=' + bldg_cd + '&ward_cd=' + ward_cd;
}

function setCount() {
	var column_count = parseInt(document.condform.column_count.value, 10);
	var row_count = parseInt(document.condform.row_count.value, 10);

	if (isNaN(column_count) || column_count <= 0) {
		alert('列数が不正です。');
		document.condform.column_count.focus();
		return;
	}
	if (isNaN(row_count) || row_count <= 0) {
		alert('行数が不正です。');
		document.condform.row_count.focus();
		return;
	}

	location.href = 'room_map.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>&column_count=' + column_count + '&row_count=' + row_count;
}

function initMap() {
	YUI().use('dd-drag', 'dd-proxy', 'dd-drop', function(Y){
<? foreach ($rooms as $tmp_room_no => $tmp_room) { ?>
		var dd = new Y.DD.Drag({
			node: '#room_<? echo($tmp_room_no); ?>'
		}).plug(Y.Plugin.DDProxy, {
			moveOnEnd: false
		});
		dd.on('drag:drophit', function(e){
			var room = e.drag.get('node');
			var fromTarget = room.ancestor();
			var fromTargetId = fromTarget.getAttribute('id');
			var toTarget = e.drop.get('node');
			var toTargetId = toTarget.getAttribute('id');

			// 移動しない場合
			if (fromTargetId == toTargetId) {
				return;
			}

			// 表示候補に移動する場合
			if (toTargetId == 'target_out') {
				fromTarget.removeChild(room);
				room.replaceClass('room_in_map', 'room_out_map');
				toTarget.insert(room, toTarget.get('children').size() - 1);
				return;
			}

			// 空白セルに移動する場合
			if (toTarget.get('children').size() == 0) {
				fromTarget.removeChild(room);
				room.replaceClass('room_out_map', 'room_in_map');
				toTarget.appendChild(room);
				return;
			}

			// 病室設定済みセルに移動する場合
			fromTarget.removeChild(room);
			var rival_room = toTarget.get('children').item(0);
			toTarget.removeChild(rival_room);
			room.replaceClass('room_out_map', 'room_in_map');
			toTarget.appendChild(room);
			if (fromTargetId == 'target_out') {
				rival_room.replaceClass('room_in_map', 'room_out_map');
				fromTarget.insert(rival_room, fromTarget.get('children').size() - 1);
			} else {
				fromTarget.appendChild(rival_room);
			}
		});
<? } ?>

<? for ($r = 1; $r <= $row_count; $r++) { ?>
	<? for ($c = 1; $c <= $column_count; $c++) { ?>
		new Y.DD.Drop({node: '#target_<? echo($r); ?>_<? echo($c); ?>'});
	<? } ?>
<? } ?>
		if (document.getElementById('target_out')) {
			new Y.DD.Drop({node: '#target_out'});
		}
	});
}

function updateMap() {
<? foreach ($rooms as $tmp_room_no => $tmp_room) { ?>
	var room = document.getElementById('room_<? echo($tmp_room_no); ?>');
	if (room.parentNode.id != 'target_out') {
		var input = document.createElement('input');
		input.type = 'hidden';
		input.name = room.parentNode.id;
		input.value = '<? echo($tmp_room_no); ?>';
		document.mainform.appendChild(input);
	}
<? } ?>
	document.mainform.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.room_in_map {
	text-align:center;
	cursor:move;
	width:100%;
	margin:0 10px;
	padding:15px 2px;
	vertical-align:middle;
}
.room_out_map {
	border:#5279a5 solid 1px;
	padding:4px 10px;
	text-align:center;
	cursor:move;
	float:left;
	margin:5px;
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initMap();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a> &gt; <a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="bed_display_setting.php?session=<? echo($session); ?>"><b>表示設定</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<table width="118" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279A5">
<td height="22" class="spacing" colspan="2"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">管理項目</font></b></td>
</tr>
<? if ($section_admin_auth == "1") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="entity_menu.php?session=<? echo($session); ?>">診療科</a></font></td>
</tr>
<? } ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="building_list.php?session=<? echo($session); ?>">病棟</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_display_setting.php?session=<? echo($session); ?>">表示設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>">一括登録</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_master_menu.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>
</table>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="90" align="center" bgcolor="#bdd1e7"><a href="bed_display_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="room_layout.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟レイアウト</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#5279a5"><a href="room_map.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病床マップ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="bed_display_calendar.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院カレンダー</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="3"><br>
<form name="condform" onsubmit="setCount(); return false;">
<table width="500" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="15%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">棟</font></td>
<td><select name="bldg_cd" onchange="changeWardOptions();">
<option value="">　　　　　　　　</option>
<?
foreach ($buildings as $tmp_bldg_cd => $tmp_bldg_nm) {
	echo("<option value=\"$tmp_bldg_cd\"");
	if ($tmp_bldg_cd == $bldg_cd) {
		echo(" selected");
	}
	echo(">$tmp_bldg_nm\n");
}
?>
</select></td>
<td align="right" bgcolor="#f6f9ff" width="15%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟</font></td>
<td><select name="ward_cd" onchange="calculate();">
<option value="">　　　　　　　　</option>
<?
foreach ($wards as $tmp_ward_cd => $tmp_ward_nm) {
	echo("<option value=\"$tmp_ward_cd\"");
	if ($tmp_ward_cd == $ward_cd) {
		echo(" selected");
	}
	echo(">$tmp_ward_nm\n");
}
?>
</select></td>
</tr>
<? if ($ward_cd != "" && count($rooms) > 0) { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">列数</font></td>
<td><input type="text" name="column_count" value="<? echo($column_count); ?>" size="5" maxlength="3" style="ime-mode:inactive;"></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行数</font></td>
<td><input type="text" name="row_count" value="<? echo($row_count); ?>" size="5" maxlength="3" style="ime-mode:inactive;"></td>
</tr>
<? } ?>
</table>
<?
if ($bldg_cd != "" && count($wards) == 0) {
	$error = "選択された棟には病棟が登録されていません。";
} else if ($ward_cd != "" && count($rooms) == 0) {
	$error = "選択された病棟には病室が登録されていません。";
}
?>
<? if (count($rooms) > 0 || $error != "") { ?>
<table width="500" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<? if (count($rooms) > 0) { ?>
<td align="right"><input type="submit" value="設定"></td>
<? } else { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($error); ?></font></td>
<? } ?>
</tr>
</table>
<? } ?>
</form>
<? if (count($rooms) > 0 && $error == "") { ?>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" action="room_map_update.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="button" value="更新" onclick="updateMap();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
<? for ($r = 1; $r <= $row_count; $r++) { ?>
<tr height="80">
<? for ($c = 1; $c <= $column_count; $c++) { ?>
<td id="target_<? echo($r); ?>_<? echo($c); ?>" width="<? echo(round(100 / $column_count)); ?>%" align="center" valign="middle">
<? if (is_array($maps[$r][$c])) { ?>
<div id="room_<? echo($maps[$r][$c]["room_no"]); ?>" class="room_in_map"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($maps[$r][$c]["name"]); ?></font></div>
<? } ?>
</td>
<? } ?>
</tr>
<? } ?>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示候補</font></b></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
<tr>
<td id="target_out" height="80">
<?
foreach ($rooms as $tmp_room_no => $tmp_room) {
	if (!$tmp_room["in_map"]) {
?>
<div id="room_<? echo($tmp_room_no); ?>" class="room_out_map"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_room["name"]); ?></font></div>
<?
	}
}
?>
<br style="float:clear;">
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="bldg_cd" value="<? echo($bldg_cd); ?>">
<input type="hidden" name="ward_cd" value="<? echo($ward_cd); ?>">
<input type="hidden" name="column_count" value="<? echo($column_count); ?>">
<input type="hidden" name="row_count" value="<? echo($row_count); ?>">
</form>
<? } ?>
</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
