<?php
if ($IS_ADMIN_MENU) $_REQUEST["path"]= "3";

// 隠しIFRAME内処理
if (isset($_REQUEST["zz_pagehist"])) {
?>    <html>
        <head>
            <?php if(!$IS_LOGIN_TREE) { ?>
                <script type="text/javascript"> parent.callbackPageHist("<?=$_REQUEST["pagehist_number"]?>"); </script>
            <?php } ?>
        </head>
        <body><?=$_REQUEST["pagehist_number"]?></body>
    </html> <?php
    die;
}
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("referer_common.ini");
require_once("menu_common.ini");
require_once("library_common.php");
require_once("show_class_name.ini");
ob_clean();


// 管理画面と丸々ソース兼用。管理画面library_admin_menu.phpから呼ばれる場合あり。
if (!$fname) $fname = $PHP_SELF;

$ajax_action = $_REQUEST["ajax_action"];
$IS_MYPAGE = $_REQUEST["is_mypage"];

$a = $_REQUEST["a"];
$c = $_REQUEST["c"];
$f = $_REQUEST["f"];
$o = $_REQUEST["o"];

if ($c=="rootfolder") $c = ""; // 共有選択ならカテゴリはカラとしておく
if ($c!="" && !preg_match("/^[1-9][0-9]*$/", $c)) { echo "パラメータが不正です。(c)(".$c.")"; die; } // 数値でない
if ($f!="" && !preg_match("/^[1-9][0-9]*$/", $f)) { echo "パラメータが不正です。(f)(".$f.")"; die; } // 数値でない
if ($o!="" && !preg_match("/^[1-9][0-9]*$/", $o)) { echo "パラメータが不正です。(o)(".$o.")"; die; } // 数値でない
if (!$c && $f) { echo "パラメータが不正です。(カテゴリ無し、フォルダあり:".$f.")"; die; }

$exist_auth = 0;
$lib_admin_auth = 0;
if (!$IS_LOGIN_TREE) {
    $session = qualify_session($session, $fname);
    if ($session=="0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }
    $lib_admin_auth = check_authority($session, 63, $fname); // 文書管理管理者権限を取得
    if ($IS_ADMIN_MENU) {
        if ($lib_admin_auth) $exist_auth = 1;
    } else {
        // 一般権限のチェック
        $exist_auth = check_authority($session, 32, $fname);
    }
    if (!(int)$exist_auth) {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }
}


// データベースに接続
$con = connect2db($fname);
if (!$ajax_action || $ajax_action=="load_document") {
    // アクセスログ
    aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);
}



// 書庫４種のうち使用可能なものの配列を取得 all / section / project / private
// 書庫選択ボタンの非表示切替に利用しているが、ツリーやリンクから非表示書庫へ行くことはできる

$is_for_public = false;
if ($IS_LOGIN_TREE) $is_for_public = true;
$available_archives = lib_available_archives($con, $fname, $is_for_public, $clib);

// 書庫指定がなければ全体書庫を選択状態に。あるいは指定書庫に。
if ($a!="1" && $a!="2" && $a!="3" && $a!="4") {
    $sel = select_from_table($con, "select value from system_config where key = 'library.ListDefaultArchive'", "", $fname);
    if ($sel == 0) libcom_goto_error_page_and_die();
    $a = @pg_fetch_result($sel, 0, "value"); // 取得されるのは1,3,4のどれか。通常は「カラ文字」である
    $syoko_ary = array("1"=>"private", "3"=>"section", "4"=>"project"); // あり得る書庫のコンバーター配列
    if (!in_array($syoko_ary[$a], $available_archives)) $a = "2"; // 書庫判別不明なら、全体共有($a=2)とする。
    $_REQUEST["a"] = $a; // REQUESTにも再代入。後続プログラムでは$_REQUEST["a"]を参照されるかもしれない。
}



if ($IS_LOGIN_TREE) {
    $referer = "3";
} else if ($IS_ADMIN_MENU) {
    $referer = get_referer($con, $session, "library", $fname);
} else {
    // 遷移元の設定
    if ($library != "") {
        set_referer($con, $session, "library", $library, $fname);
        $referer = $library;
    } else {
        $referer = get_referer($con, $session, "library", $fname);
    }
}


// ログインユーザの職員情報を取得
$emp_id = "";
if (!$IS_LOGIN_TREE) {
    $sql = "select emp_id, emp_class, emp_attribute, emp_dept, emp_st from empmst";
    $cond = "where emp_id = (select emp_id from session where session_id = '$session')";
    $sel_emp = select_from_table($con, $sql, $cond, $fname);
    if ($sel_emp == 0) libcom_goto_error_page_and_die();
    $empmstRow = pg_fetch_assoc($sel_emp);
    $emp_id = $empmstRow["emp_id"];
}

$IS_OWNERS = "";
if (defined("IS_OWNERS_CLUB")) {
    $IS_OWNERS = "1";
    if ($empmstRow["emp_class"]=="1") $IS_OWNERS = "";
}

// オプション設定情報からフォントサイズを取得（マイページ時のみ）
$font_size = "";
if ($IS_MYPAGE) {
    $sql = "select font_size from option where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) libcom_goto_error_page_and_die();
    $font_size = @pg_fetch_result($sel, 0, "font_size");
}
$font_class = ($font_size == "2") ? "j16" : "j12";

//------------------------------------------------------------------------------
// サンプルデータ機械生成コーナー
// おおよそ1000個ずつ位作成してください。
// 再リクエストすればプラスされてゆきます。
// 一旦消す場合は$delete_and_insertフラグを1に指定してください。
// delete系が走ります。重要な環境へ作成するときは注意。自己責任で。
//
// libtreeテーブルは最後のほうで実行されます。
// 完全に処理が終わるまでは、カテゴリに並列にフォルダ作成されている状態です。
// 完全に処理が終わるのを待ったほうがよいです。
//------------------------------------------------------------------------------
/*
if ($_REQUEST["make_test_data"]) {
    set_time_limit (1000);
    $folder_count = array(1, 10, 10, 10); // どの階層に何個のフォルダを作成するか。1x10x10x10で、おおよそ1000個のフォルダ作成。
    $file_count = array(1, 1, 1, 1); // それぞれの階層に何個ずつのファイルを作成するか。1x1x1x2の設定で、おおよそ2000個のファイル作成。
    $ref_folder_id = 12515; // どのフォルダの権限設定を引継ぐか（カテゴリでなくて）
    $ref_lib_id = 137471; // どのファイルの設定と実物を引継ぐか（物理ファイルはダミーを作成）
    $ref_cate_id = 256; // どのカテゴリの下に作成するか
    $delete_and_insert = 0; // 削除してからサンプルを作成する場合1
    $sample_name_prefix = "◆機械登録権限多2";


    $sel = select_from_table($con, "select lib_archive from libfolder where folder_id = ".$ref_folder_id, "", $fname);
    if ($sel == 0) echo pg_last_error($con);
    $lib_archive = pg_fetch_result($sel, 0, 0);
    if (!$lib_archive) {
        echo '$ref_folder_idのフォルダは存在しません。';
        die;
    }

    $sel = select_from_table($con, "select lib_cate_id from libcate where lib_cate_id = ".$ref_cate_id, "", $fname);
    if ($sel == 0) echo pg_last_error($con);
    $dummy = pg_fetch_result($sel, 0, 0);
    if (!$dummy) {
        echo '$ref_cate_idのカテゴリは存在しません。';
        die;
    }

    $sel = select_from_table($con, "select lib_id from libinfo where lib_archive = '".$lib_archive."' and lib_id = ".$ref_lib_id, "", $fname);
    if ($sel == 0) echo pg_last_error($con);
    $dummy = pg_fetch_result($sel, 0, 0);
    if (!$dummy) {
        echo '$ref_lib_idの文書は存在しません。';
        die;
    }

    $dest_dir = "";
    if ($lib_archive=="1") $dest_dir = "private";
    if ($lib_archive=="2") $dest_dir = "all";
    if ($lib_archive=="3") $dest_dir = "section";
    if ($lib_archive=="4") $dest_dir = "project";

    if ($delete_and_insert) {
        $sql = "delete from libtree where child_id in (select folder_id from libfolder where folder_name like '".$sample_name_prefix."%')";
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

        $sql = "delete from libedition where base_lib_id in (select folder_id from libfolder where folder_name like '".$sample_name_prefix."%')";
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

        $sql = "select lib_id from libinfo where lib_nm like '".$sample_name_prefix."%'";
        $sel = select_from_table($con, $sql, "", $fname);
        while ($row = pg_fetch_assoc($sel)) {
            $file_path = "docArchive/".$dest_dir."/cate".$ref_cate_id."/document".$row["lib_id"].".txt";
            @unlink($file_path);
        }

        $sql = "delete from libinfo where lib_nm like '".$sample_name_prefix."%'";
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

        $sql = "delete from libfolder where folder_name like '".$sample_name_prefix."%'";
        $upd = update_set_table($con, $sql, array(), null, "", $fname);
    }

    $sel = select_from_table($con, "select max(folder_id) from libfolder", "", $fname);
    $max_folder_id = intval(pg_fetch_result($sel, 0, 0));

    $all_node = array("");
    for ($depth=1; $depth<=count($folder_count); $depth++) {
        $all_node_keys = array_keys($all_node);
        foreach ($all_node_keys as $pkey) {
            $_depth = count(explode("@", $pkey)) - 1;
            if ($_depth+1 != $depth) continue;
            for ($idx=1; $idx<=$folder_count[$depth-1]; $idx++) {
                $max_folder_id++;
                $all_node[$pkey."@".$depth."_".$idx] = $max_folder_id;
            }
        }
    }

    $ymdhms = date("YmdHis");
    // サンプル機械生成１
    foreach ($all_node as $pkey => $new_folder_id) {
        if ($pkey=="0") continue;

        $nodes = explode("@", $pkey);
        $depth = count($nodes)-2;

        $sql =
        " insert into libfolder (".
        "     folder_id".
        "   , lib_archive, lib_cate_id, folder_name, folder_no".
        "   , reg_emp_id, upd_time, private_flg, ref_dept_st_flg, ref_dept_flg".
        "   , ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg".
        " ) select".
        " ".$new_folder_id.
        "   , lib_archive, ".$ref_cate_id.", '".$sample_name_prefix."".$new_folder_id."', folder_no".
        "   , reg_emp_id, '".$ymdhms."', private_flg, ref_dept_st_flg, ref_dept_flg".
        "   , ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg".
        " from libfolder where folder_id = ".$ref_folder_id;
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

        $sql =
        " insert into libfolderrefdept (folder_id, class_id, atrb_id, dept_id)".
        " select ".$new_folder_id.", class_id, atrb_id, dept_id".
        " from libfolderrefdept where folder_id = ".$ref_folder_id;
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

        $sql =
        " insert into libfolderrefst (folder_id, st_id)".
        " select ".$new_folder_id.", st_id".
        " from libfolderrefst where folder_id = ".$ref_folder_id;
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

        $sql =
        " insert into libfolderrefemp (folder_id, emp_id)".
        " select ".$new_folder_id.", emp_id".
        " from libfolderrefemp where folder_id = ".$ref_folder_id;
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

        $sql =
        " insert into libfolderupddept (folder_id, class_id, atrb_id, dept_id)".
        " select ".$new_folder_id.", class_id, atrb_id, dept_id".
        " from libfolderupddept where folder_id = ".$ref_folder_id;
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

        $sql =
        " insert into libfolderupdst (folder_id, st_id)".
        " select ".$new_folder_id.", st_id".
        " from libfolderupdst where folder_id = ".$ref_folder_id;
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

        $sql =
        " insert into libfolderupdemp (folder_id, emp_id)".
        " select ".$new_folder_id.", emp_id".
        " from libfolderupdemp where folder_id = ".$ref_folder_id;
        $upd = update_set_table($con, $sql, array(), null, "", $fname);




        for ($fidx=1; $fidx<=(int)@$file_count[$depth]; $fidx++) {

            $sel = select_from_table($con, "select nextval('library_lib_id_seq') as lib_id", "", $fname);
            $new_lib_id = intval(pg_fetch_result($sel, 0, 0));

            $sql =
            " insert into libinfo (".
            " lib_archive, lib_id, lib_cate_id, lib_extension, lib_keyword".
            ",lib_summary, emp_id, lib_delete_flag, show_login_flg, show_login_begin".
            ", show_login_end, folder_id, lib_type, private_flg, ref_dept_st_flg".
            ", ref_dept_flg, ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg".
            ", lib_nm, lib_up_date, ref_count, lib_no, apply_id, show_mypage_flg".
            " ) select".
            " lib_archive, ".$new_lib_id.", ".$ref_cate_id.", lib_extension, lib_keyword".
            ",lib_summary, emp_id, lib_delete_flag, show_login_flg, show_login_begin".
            ", show_login_end, ".$new_folder_id.", lib_type, private_flg, ref_dept_st_flg".
            ", ref_dept_flg, ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg".
            ", '".$sample_name_prefix."ファイル".$new_lib_id."', '".$ymdhms."', ref_count, lib_no, apply_id, show_mypage_flg".
            " from libinfo where lib_id = ".$ref_lib_id;
            $upd = update_set_table($con, $sql, array(), null, "", $fname);

            $sql =
            " insert into libedition (base_lib_id, edition_no, lib_id) values (".
            " ".$new_lib_id.", 1, ".$new_lib_id.
            " )";
            $upd = update_set_table($con, $sql, array(), null, "", $fname);

        $sql =
        " insert into librefdept (lib_id, class_id, atrb_id, dept_id)".
        " select ".$new_lib_id.", class_id, atrb_id, dept_id".
        " from librefdept where lib_id = ".$ref_lib_id;
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

        $sql =
        " insert into librefst (lib_id, st_id)".
        " select ".$new_lib_id.", st_id".
        " from librefst where lib_id = ".$ref_lib_id;
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

        $sql =
        " insert into librefemp (lib_id, emp_id)".
        " select ".$new_lib_id.", emp_id".
        " from librefemp where lib_id = ".$ref_lib_id;
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

        $sql =
        " insert into libupddept (lib_id, class_id, atrb_id, dept_id)".
        " select ".$new_lib_id.", class_id, atrb_id, dept_id".
        " from libupddept where lib_id = ".$ref_lib_id;
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

        $sql =
        " insert into libupdst (lib_id, st_id)".
        " select ".$new_lib_id.", st_id".
        " from libupdst where lib_id = ".$ref_lib_id;
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

        $sql =
        " insert into libupdemp (lib_id, emp_id)".
        " select ".$new_lib_id.", emp_id".
        " from libupdemp where lib_id = ".$ref_lib_id;
        $upd = update_set_table($con, $sql, array(), null, "", $fname);

            $file_path = "docArchive/".$dest_dir."/cate".$ref_cate_id."/document".$new_lib_id.".txt";
            //echo $file_path."<br>";
            $fp = @fopen($file_path, 'w');
            if ($fp) {
                fwrite($fp, "1234567890");
                fclose($fp);
            }
            chmod($file_path, 0666);
        }
    }
    echo "end1.";
    // サンプル機械生成２
    foreach ($all_node as $pkey => $new_folder_id) {
        if ($pkey=="0") continue;
        $nodes = explode("@", $pkey);
        array_pop($nodes); // 親を示すハッシュは、ひとつ末尾要素を除いたものである。
        $parent_key = implode("@", $nodes);
        $parent_folder_id = $all_node[$parent_key];

        if ($parent_folder_id) {
            $sql = "delete from libtree where child_id = ".$new_folder_id; // いったん破棄
            $upd = update_set_table($con, $sql, array(), null, "", $fname);
            $sql = "insert into libtree (parent_id, child_id) values (".$parent_folder_id.", ".$new_folder_id.")"; // 新しい親子関係
            $upd = update_set_table($con, $sql, array(), null, "", $fname);
        }
    }
    echo "end2.";

    $sel = select_from_table($con, "select count(*) as cnt from libfolder", "", $fname);
    if ($sel == 0) echo pg_last_error($con);
    echo "<br>folders=".pg_fetch_result($sel, 0, 0);
    $sel = select_from_table($con, "select count(*) as cnt from libinfo", "", $fname);
    if ($sel == 0) echo pg_last_error($con);
    echo "<br>docs=".pg_fetch_result($sel, 0, 0);
    die;
} // サンプル機械生成ここまで
*/

$stick_folder = lib_get_stick_folder($con, $fname);
// デフォルトの並び順を設定
if ($o == "") $o = "7";  // 番号の昇順


//**************************************************************************************************
// 委員会・WG書庫の場合のみ 組織プルダウン制御（ログイン画面では不要）
//**************************************************************************************************
if (!$IS_LOGIN_TREE && ($a=="4" || $ajax_action=="load_class_atrb" || $_REQUEST["load_defalut_class_atrb"])) {
    $class_id = $_REQUEST["class_id"];
    $atrb_id = $_REQUEST["atrb_id"];
    // 初期表示時はデフォルトの組織情報があれば取得
    if (!isset($_REQUEST["class_id"]) || $_REQUEST["load_defalut_class_atrb"]) {
        $sel = select_from_table($con, "select class_id, atrb_id from prodfltcls where emp_id = '$emp_id'", "", $fname);
        if ($sel == 0) libcom_goto_error_page_and_die();
        $class_id = @pg_fetch_result($sel, 0, "class_id");
        $atrb_id = @pg_fetch_result($sel, 0, "atrb_id");
    }
    // 初期表示でなければリクエスト値をデフォルト選択肢として登録しておく
    else {
        $del = delete_from_table($con, "delete from prodfltcls where emp_id = '$emp_id'", "", $fname);
        if ($del == 0) libcom_goto_error_page_and_die();

        $sql =
        " insert into prodfltcls (emp_id, class_id, atrb_id) values (".
        " '".$emp_id."', ".($class_id=="" ? "null" : (int)$class_id).", ".($atrb_id=="" ? "null" : (int)$atrb_id).")";
        if (update_set_table($con, $sql, array(), null, "", $fname) == 0) libcom_goto_error_page_and_die();
    }
    if ((int)$class_id>0) $class_id = (int)$class_id;
    else $class_id = "";

    if ((int)$atrb_id>0)  $atrb_id = (int)$atrb_id;
    else $atrb_id = "";

    //■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
    // Ajax: classmstとatrbmstのコンボボックス
    //■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
    // ajaxの場合
    if ($ajax_action=="load_class_atrb") {
        ob_clean();

        // 組織1階層目の一覧を取得
        $sql = "select class_id, class_nm from classmst where class_del_flg = 'f' order by order_no";
        $sel = select_from_table($con, $sql, "", $fname);
        if ($sel == 0) libcom_goto_error_page_and_die();
        $classes = array();
        while ($row = pg_fetch_assoc($sel)) {
            $classes[$row["class_id"]] = $row["class_nm"];
        }

        // 組織1階層目が選択されている場合
        $attributes = array();
        if ($class_id) {
            // 組織2階層目の一覧を取得
            $sql = "select atrb_id, atrb_nm from atrbmst where class_id = $class_id and atrb_del_flg = 'f' order by order_no";
            $sel = select_from_table($con, $sql, "", $fname);
            if ($sel == 0) libcom_goto_error_page_and_die();
            while ($row = pg_fetch_assoc($sel)) {
                $attributes[$row["atrb_id"]] = $row["atrb_nm"];
            }
        }

        $arr_class_name = get_class_name_array($con, $fname);
        ?>
        <span class="j12"><?=$arr_class_name[0]?></span>
        <select name="class_id" id="class_id" onchange="changeClassAtrb('class');">
            <option value="">　　　　　</option>
            <?php foreach ($classes as $_class_id => $_class_nm) { ?>
                <option value="<?=$_class_id?>"<?=($_class_id == $class_id?" selected":"")?>><?=$_class_nm?></option>
            <?php } ?>
        </select><select name="atrb_id" id="atrb_id" onchange="changeClassAtrb('atrb');">
            <option value="">　　　　　</option>
            <?php foreach ($attributes as $_atrb_id => $_atrb_nm) { ?>
                <option value="<?=$_atrb_id?>"<?=($_atrb_id == $atrb_id ? " selected":"")?>><?=$_atrb_nm?></option>
            <?php } ?>
        </select>
        <?php
        die;
    }
}


//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
// Ajax：指定ノードの親リスト
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
if ($ajax_action=="load_parents") {
    $ret = array();
    $p = (int)$f;
    if ($p) $ret[]= $c."_".$p; // フォルダ指定があれば追加する

    for (;;) {
        $p = lib_get_one($con, $fname, "select parent_id from libtree where child_id = ".$p);
        if (!$p) break;
        $c_f = $c."_".$p;
        if (in_array($c_f, $ret)) { echo "循環参照エラー：".implode(",",$ret); die; }
        array_unshift($ret, $c_f); // その親を追加する
    }
    array_unshift($ret, ($c ? $c : "rootfolder")."_"); // カテゴリ直下の階層を追加する
    if ($c) array_unshift($ret, "rootfolder_"); // カテゴリ指定されている場合は、先頭に共有階層を持ってくる
    ob_clean();
    echo implode(",", $ret);
    die;
}



//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
// Ajax:ツリー表示
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
if ($ajax_action=="load_tree") {
    ob_clean();

    $lock_show_flg = lib_get_lock_show_flg($con, $fname);
    $lock_show_all_flg = lib_get_lock_show_all_flg($con, $fname);
    lib_cre_tmp_concurrent_table($con, $fname, $emp_id);
    if ($IS_ADMIN_MENU || $IS_LOGIN_TREE) { $class_id = ""; $atrb_id =""; }

    //========================================================================================================
    // 全て開く場合
    //========================================================================================================
    if ($_REQUEST["isGetAllChild"]) {

        //------------------------------------
        // 先頭カテゴリから要素群を作成する
        //------------------------------------
        $opt = array("is_admin"=>$IS_ADMIN_MENU, "is_login"=>$IS_LOGIN_TREE, "is_mypage"=>$IS_MYPAGE);
        if ($IS_FOLDER_SELECT) $opt["is_ignore_class_atrb"] = 1;
        $sql = lib_get_libcate_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $a, "", $class_id, $atrb_id, $opt);
        $sel = select_from_table($con, $sql, "", $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql);
        $tree_list = array();
        while ($row = pg_fetch_assoc($sel)) {
            $row["hier"] = "a"; // 全ノードをいったんa階層マークしておく
            $tree_list[]= $row;
        }
        if (!count($tree_list)) {
            echo '<div style="padding:2px" class="j12">フォルダは登録されていません。</div>';
            die;
        }
        if (count($tree_list)) $tree_list[count($tree_list)-1]["hier"] = "b"; // 最後のノードはノードアイコンが違う。b階層としてマークしておく。

        //------------------------------------
        // 全フォルダを取得しておく
        //------------------------------------
        $opt = array("is_admin"=>$IS_ADMIN_MENU, "is_login"=>$IS_LOGIN_TREE);
        $sql = lib_get_libfolder_sql($emp_id, $lock_show_flg, $lock_show_all_flg, "", "", "", $opt);
        $sel = select_from_table($con, $sql, "", $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql);
        $libfolder_c = array();
        $libfolder_f = array();
        $libfolder_a = array();
        while ($row = pg_fetch_assoc($sel)) {
            $libfolder_a[$row["folder_id"]] = $row;
            // フォルダIDが子でない場合（つまり、親フォルダIGが取得できなかった場合）のみ、カテゴリ別一覧へ
            if (!$row["parent_id"]) $libfolder_c[$row["lib_cate_id"]][] = $row;
            // 上記とは反対に、親フォルダが取得できたなら、親フォルダ別の子一覧を作成しておく
            else $libfolder_f[$row["parent_id"]][] = $row;
        }

        //------------------------------------
        // カテゴリをループ出力
        //------------------------------------
        $drawn_nodes = 0;
        $last_hier = "";
        $drawn_ttl = 0;
        for (;;) {
            $ttlcnt++;
            if (!count($tree_list)) break;
            $row = array_shift($tree_list);
            if (!$row) continue;

            $hier = $row["hier"];
            for ($idx=strlen($hier); $idx<=strlen($last_hier); $idx++) {
                $drawn_nodes--; echo "</div>";
            } // 子ノードの閉じタグ

            //------------------------------------
            // 管理画面ならこのノードの子を全部取得
            // そうでなければ、プラスアイコンを出すために、このノードに子ノードが存在するか確認したい
            //------------------------------------
            $_a = $row["lib_archive"];
            $_c = $row["lib_cate_id"];
            $_f = $row["folder_id"];

            $is_exist_child = 0;
            $append_pos = -1;
            if ($_f) $children = $libfolder_f[$_f];
            else $children = $libfolder_c[$_c];
            if (!is_array($children)) $children = array();
            foreach ($children as $idx => $cRow) {
                $ra = $cRow["lib_archive"];
                $rc = $cRow["lib_cate_id"];
                $rf = $cRow["folder_id"];
                if ($cRow["link_id"]) {
                    $rf = $cRow["link_id"];
                    $link_row = $libfolder_a[$rf];
                    if (!$link_row) continue;
                    $ra = $link_row["lib_archive"];
                    $rc = $link_row["lib_cate_id"];
                    $rf = $link_row["folder_id"];
                    if (!$ra) continue;
                }
                $is_exist_child = 1;
                $append_pos++;
                $cRow["hier"] = $hier."a"; // この子をひとまずa階層としてセット
                array_splice($tree_list, $append_pos, 0, array($cRow)); // さらに下層の検証リストへ追加
            }
            if ($append_pos>=0) $tree_list[$append_pos]["hier"] = $hier."b"; // 子を追加したら、末尾の子はb階層とする

            //------------------------------------
            // ツリーノードを描画。
            // 子要素を格納する<div>の開始タグまでを含む。
            //------------------------------------
            $last_hier = $hier;
            lib_write_node_html($row, $_a, $_c, $_f, $o, $hier, 1, $is_exist_child);
            $drawn_ttl++;
            $c_f = $cate_id."_".$folder_id;
            $eid = ($folder_id ? "f".$folder_id : "c".$cate_id);
            $drawn_nodes++;
        }
        for($idx=0; $idx<$drawn_nodes; $idx++) echo "</div>";
        die;
    }



    //========================================================================================================
    // 「全て開く」ではない場合
    // open_eid_listは現在画面側で「開いてマイナスアイコンになっているフォルダ」のIDカンマ配列である
    //========================================================================================================
    $tree_list = array();
    $open_eid_list = array();
    if ($_REQUEST["open_eid_list"]) $open_eid_list = explode(",", $_REQUEST["open_eid_list"]);
    $seed = array();
    foreach ($open_eid_list as $eid) {
        if (substr($eid,0,1)!="f") $seed[]= substr($eid,1);
    }

    //------------------------------------
    // 開いているフォルダの全親を集積する。通常は渡ってこない。
    // これらはそれぞれの子は出力に含める。
    // さらに、プラスアイコン判定のため、「閲覧可能な孫フォルダが１つ以上存在するか」まで確認する必要がある
    //------------------------------------
    while (true) {
        if (!count($seed)) break;
        $_f = array_shift($seed);
        if (!$_f) continue;
        $open_eid_list[] = "f".$_f;
        // ひとつ上にさかのぼる。親がいなければ終了。親なしの親は、カテゴリである。
        $sql = "select parent_id from libtree where child_id = ".$_f;
        $_f = lib_get_one($con, $fname, $sql);
        if ($_f) $seed[]= $_f;
    }

    //------------------------------------
    // 共有クリック時：カテゴリ一覧となる
    //------------------------------------
    if (!$c) {
        $opt = array("is_admin"=>$IS_ADMIN_MENU, "is_login"=>$IS_LOGIN_TREE, "is_mypage"=>$IS_MYPAGE);
        if ($IS_FOLDER_SELECT) $opt["is_ignore_class_atrb"] = 1;
        $sql = lib_get_libcate_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $a, "", $class_id, $atrb_id, $opt);

        $sel = select_from_table($con, $sql, "", $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql);
        $tree_list = array();
        while ($row = pg_fetch_assoc($sel)) {
            $row["hier"] = "a"; // 全ノードをいったんa階層マークしておく
            $tree_list[]= $row;
        }
        if (!count($tree_list)) {
            echo '<div style="padding:2px" class="j12">参照できるフォルダが存在しないか、登録されていません。</div>';
            die;
        }
        if (count($tree_list)) $tree_list[count($tree_list)-1]["hier"] = "b"; // 最後のノードはノードアイコンが違う。b階層としてマークしておく。
    }
    //------------------------------------
    // ツリークリック時：フォルダ一覧となる。指定したカテゴリまたはフォルダの子要素群を作成する
    //------------------------------------
    else {
        // リンクなら子ツリーは存在しない
        if ($c && $f) {
            $link_id = lib_get_one($con, $fname, "select link_id from libfolder where folder_id = ".$f);
            if ($link_id) die;
        }

        $hier = $_REQUEST["hier"];
        $tree_list = array();
        $exist_self = 0;
        if ($a) {
            // 自身フォルダを除いた子フォルダの一覧。自身フォルダの権限は確認省略
            $opt = array("is_admin"=>$IS_ADMIN_MENU, "is_login"=>$IS_LOGIN_TREE, "is_narrow_mode"=>1, "with_child"=>1, "exclude_self"=>1);
            $sql = lib_get_libfolder_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $a, $c, $f, $opt);
            $sel = select_from_table($con, $sql, "", $fname);
            if ($sel == 0) libcom_goto_error_page_and_die($sql);
            while ($row = pg_fetch_assoc($sel)) {
                // リンクなら実体の権限を確認する
                if ($row["link_id"]) {
                    $link_row = lib_get_top_row($con, $fname, "select * from libfolder where folder_id = ".$row["link_id"]);
                    if (!$link_row) continue;
                    $ra = $link_row["lib_archive"];
                    $rc = $link_row["lib_cate_id"];
                    $rf = $link_row["folder_id"];
                    if (!$ra) continue;
                    $opt = array("is_admin"=>$IS_ADMIN_MENU, "is_login"=>$IS_LOGIN_TREE, "is_narrow_mode"=>1, "top_only"=>1);
                    $sql = lib_get_libfolder_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $ra, $rc, $rf, $opt);
                    $real_row = lib_get_top_row($con, $fname, $sql);
                    if (!$real_row["folder_id"]) continue;
                }
                $row["hier"] = $hier."a";
                $tree_list[] = $row;
            }
        }
        if (count($tree_list)) $tree_list[count($tree_list)-1]["hier"] = $hier."b"; // 最後のノードはノードアイコンが違う。b階層としてマークしておく。
    }
    //------------------------------------
    // カテゴリまたはフォルダをループ出力
    //------------------------------------
    $drawn_nodes = 0;
    $last_hier = "";
    $drawn_ttl = 0;
    $open_parents = array();
    for (;;) {
        $ttlcnt++;
        if (!count($tree_list)) break;
        $row = array_shift($tree_list);
        if (!$row) continue;

        $hier = $row["hier"];
        for ($idx=strlen($hier); $idx<=strlen($last_hier); $idx++) {
            $drawn_nodes--; echo "</div>";
        } // 子ノードの閉じタグ

        //------------------------------------
        // このノードがカテゴリでなく、リンクなら実体を取得
        //------------------------------------
        $_ra = $row["lib_archive"];
        $_rc = $row["lib_cate_id"];
        $_rf = $row["folder_id"]; // カテゴリなら空値
        if ($row["link_id"]) {
            $link_row = lib_get_top_row($con, $fname, "select * from libfolder where folder_id = ".$row["folder_id"]);
            if ($link_row) {
                $_ra = $link_row["lib_archive"];
                $_rc = $link_row["lib_cate_id"];
                $_rf = $link_row["folder_id"];
            }
        }

        //------------------------------------
        // プラスアイコンを出すために、このノードに子ノードが存在するか確認したい
        //------------------------------------
        $is_exist_child = 0;
        $append_pos = -1;
        $default_open = 0;
        // デフォルトで開くアイコンの場合
        if (in_array("f".$_rf, $open_eid_list) || (!$_rf && in_array("c".$_rc, $open_eid_list))) {
            $default_open = 1;
            // 子のみの一覧。子はすべて表示することになる
            $opt = array("is_admin"=>$IS_ADMIN_MENU, "is_login"=>$IS_LOGIN_TREE, "is_narrow_mode"=>1, "with_child"=>1, "exclude_self"=>1);
            $sql = lib_get_libfolder_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $_ra, $_rc, $_rf, $opt);
            $sel2 = select_from_table($con, $sql, "", $fname);
            if ($sel2 == 0) libcom_goto_error_page_and_die($sql);
            $children = array();
            while ($row2 = pg_fetch_assoc($sel2)) $children[] = $row2;
            foreach ($children as $idx => $cRow) {
                // リンクだったら実体の権限を確認
                if ($cRow["link_id"]) {
                    $opt = array("is_admin"=>$IS_ADMIN_MENU, "is_login"=>$IS_LOGIN_TREE, "is_narrow_mode"=>1, "top_only"=>1);
                    $sql = lib_get_libfolder_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $cRow["lib_archive"], $cRow["lib_cate_id"], $cRow["folder_id"], $opt);
                    $check_row = lib_get_top_row($con, $fname, $sql);
                    if (!is_array($check_row)) continue; // 実体の権限がないので飛ばす
                }
                $is_exist_child = 1;
                $append_pos++;
                $cRow["hier"] = $hier."a"; // この子をひとまずa階層としてセット
                array_splice($tree_list, $append_pos, 0, array($cRow)); // さらに下層の検証リストへ追加
            }
            if ($append_pos>=0) $tree_list[$append_pos]["hier"] = $hier."b"; // 子を追加したら、末尾の子はb階層とする
        }
        else {
            // リンクだったら実体の権限を確認。トップオンリーオプションで、リンクは後回しで取得される。
            $opt = array("is_admin"=>$IS_ADMIN_MENU, "is_login"=>$IS_LOGIN_TREE, "is_narrow_mode"=>1, "with_child"=>1, "top_only"=>1, "exclude_self"=>1);
            $sql = lib_get_libfolder_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $_ra, $_rc, $_rf, $opt);
            $top_row = lib_get_top_row($con, $fname, $sql);
            if ($top_row["folder_id"]) {
                // リンクだったら実体の権限を確認。このリンクが駄目なら次のリンクを確認する必要があるのだが。
                if ($top_row["link_id"]) {
                    $top_row = lib_get_top_row($con, $fname, "select * from libfolder where folder_id = ".$top_row["link_id"]); // リンク先実体
                    $opt = array("is_admin"=>$IS_ADMIN_MENU, "is_login"=>$IS_LOGIN_TREE, "is_narrow_mode"=>1, "top_only"=>1);
                    $sql = lib_get_libfolder_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $top_row["lib_archive"], $top_row["lib_cate_id"], $top_row["folder_id"], $opt);
                    $top_row = lib_get_top_row($con, $fname, $sql); // リンク先実体の権限レコード
                }
                if ($top_row["folder_id"]) $is_exist_child = 1;
            }
        }

        //------------------------------------
        // カテゴリHTMLノードを描画。
        // 子要素を格納する<div>の開始タグまでを含む。
        //------------------------------------
        $last_hier = $hier;
        lib_write_node_html($row, $a, $c, $f, $o, $hier, $default_open, $is_exist_child);
        $drawn_ttl++;
        $c_f = $cate_id."_".$folder_id;
        $eid = ($folder_id ? "f".$folder_id : "c".$cate_id);
        $drawn_nodes++;
    }
    for($idx=0; $idx<$drawn_nodes; $idx++) echo "</div>";
    die;
}
// フォルダツリー表示JavaScriptを出力。画面にはすべてのデータを出力。
function lib_write_node_html($node, $a, $c, $f, $o, $hier, $default_open, $is_exist_child) {
    global $IS_ADMIN_MENU;
    global $IS_LOGIN_TREE;
    global $IS_FOLDER_SELECT;
    global $IS_MYPAGE;
    //$writable = ($node["writable"]) ? "1" : "";
    // if ($IS_ADMIN_MENU) $writable = "1"; // 管理画面ならフル操作可能
    // if ($a=="1") $writable = "1"; // 個人フォルダならフル操作可能

    $cate_id = $node["lib_cate_id"];
    $folder_id = $node["folder_id"];

    $nm = str_replace('"', '\"', ($folder_id ? $node["folder_name"] : $node["lib_cate_nm"]));

    $ygtvicon = ($is_exist_child ? "ygtvtp" : "ygtvtn");
    if (substr($hier, -1)=="b") $ygtvicon = ($is_exist_child ? "ygtvlp" : "ygtvln"); // 最下ノードならアイコン様態を変更

    $org_ygtvicon = $ygtvicon;
    if ($default_open && $is_exist_child) $ygtvicon = substr($ygtvicon, 0, 5)."m"; // 全開指定で子がいる。開いた状態にする

    $c_f = $cate_id."_".$folder_id;
    $eid = ($folder_id ? "f".$folder_id : "c".$cate_id);

    echo '<div class="the_node ygtvitem" id="item_'.$c_f.'" hier="'.$hier.'" writable="'.($node["writable"] ? "1" : "").'"';
    echo ' link_id="'.$node["link_id"].'" a="'.$node["lib_archive"].'" eid="'.$eid.'" item_ab="'.substr($hier,-1).'"';
    if ($node["link_id"]) echo ' title="リンクフォルダ"';
    echo '>';
    echo '<table cellspacing="0" cellpadding="0"><tbody><tr>';

    // マージン
    for ($idx=0; $idx<strlen($hier)-1; $idx++) {
        echo '<td class="'.($hier[$idx]=="a" ? 'ygtvdepthcell' : 'ygtvblankdepthcell').'"><div class="ygtvspacer"></div></td>';
    }

    // 開閉部分
    echo '<th id="icon_'.$eid.'" class="'.$ygtvicon.'" org_class="'.$org_ygtvicon.'"';
    echo ' onclick="loadTreeNextChild(this)"';
    echo ' onmouseout="hoverIcon(this)"';
    echo ' onmouseover="hoverIcon(this,1)"';
    echo '><div class="ygtvspacer'.($node["link_id"]?' is_link_folder':"").'"></div>';
    echo '</th>';

    // 名称と詳細一覧リンク表示部分
    $clickable = 1;
    if ($IS_FOLDER_SELECT) {
        if (!$node["writable"]) $clickable = 0;
        if ($node["link_id"]) $clickable = 0;
        if ($_REQUEST["type"]=="link" && !$folder_id) $clickable = 0;
    }
    $fs_cls = (!$clickable ? ' class="fs_disabled"' : "");
    echo '<td'.$fs_cls.'><a class="ygtvlabel" href="javascript:void(0)"';
    if ($IS_FOLDER_SELECT) {
        if (!$clickable) {
            echo ' style="cursor:default; color:#808080"';
        } else {
            echo " onclick=\"selectNode('".$cate_id."', '".$folder_id."', '".$a."')\"";
        }
    } else {
        echo " onclick=\"pickupNode('".$cate_id."', '".$folder_id."', '".$a."', '', '".$node["link_id"]."')\"";
    }
    echo '>';
    echo '<div class="droppable_target"><span class="j12 node_span'.($node["writable"] || $IS_LOGIN_TREE ?"":" lock_icon").'">'.$nm.'</span></div>';
    echo '</a></td>';

    echo '</tr></tbody></table>';
    echo '</div>';
    // 子階層のコンテナ開始タグ。終了タグは呼び元で管理すること。
    echo '<div class="ygtvitemchildren" id="itemchildren_'.$eid.'"'.(!$default_open ? ' style="display:none"' : '').'>';

    ob_flush();
}



//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
// Ajax:右側一覧
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
if ($ajax_action == "load_document") {
    show_document_list($a, $c, $f, $o, $session, $con, $fname, $emp_id, $available_archives, $class_id, $atrb_id);
    die;
}



//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
// ここから通常表示
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
// 最新文書一覧表示フラグを取得
$newlist_flg = lib_get_show_newlist_flg($con, $fname, $clib);
// ツリー表示フラグを取得
$tree_flg = lib_get_show_tree_flg($con, $fname, $clib);
if ($IS_MYPAGE) $tree_flg = "t"; // マイページは常にツリー表示
if ($IS_FOLDER_SELECT) $tree_flg = "t"; // 保存先選択は常にツリー表示

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">

<?php
    $_title = "文書一覧";
    if ($IS_FOLDER_SELECT) {
        $_title = '保存先選択';
        if ($_REQUEST["type"] == "link") $_title = 'リンク先選択';
         else if ($_REQUEST["type"] == "parent") $_title = '親フォルダ選択';
    }
?>
<title>CoMedix 文書管理 | <?=$_title?></title>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px; }
.list td td {border-width:0; }
#main_list label { display:block }
div.selectedLabel table td a div { font-weight:bold; color:black; }
div.selectedLabel table td a div div { font-weight:normal; color:black; }
.ygtvlabel {padding-right:6px;}
table#tbl_archive_buttons { border-collapse:collapse; }
table#tbl_archive_buttons td { width:100px; height:20px; vertical-align:middle; border:1px solid #5279A5; background:#ffffff; font-size:14px; }
table#tbl_archive_buttons a { display:block; height:20px; width:100px; line-height:20px; text-decoration:none; text-align:center;  color:#5279a5 }
table#tbl_archive_buttons a.selected { background:#5279A5; color:#FFFFFF }
.lock_icon { background:url(img/icon/lock.gif) no-repeat 0 2px; padding-left:12px }

#next_archive_label .s1 { padding-right:10px; font-size:12px }
#next_archive_label .s2 { font-weight:bold }
.with_icon { background-repeat:no-repeat; background-position:2px 0 }
.with_icon a { display:block; }
tr.tr_document_row .with_icon a:hover { background-color:#c4d7ff; }
tr.tr_document_row:hover a { background-color:#eef5ff }

.with_icon div { padding-left:22px; }
div.with_icon div.is_link_folder { background:url(img/icon/timeless.gif) no-repeat 2px 8px; }
div.the_node div.is_link_folder { height:22px; background:url(img/icon/timeless.gif) no-repeat 16px 10px; }
div.rightside_icon div { position:relative }
div.rightside_icon div a.legacy_link { position:absolute; width:20px; height:20px; left:0; top:0; cursor:default }
div.rightside_icon div a.legacy_link:hover { background:transparent !important }
tr.tr_document_row:hover a.legacy_link { background:transparent }

div.ygtvitem table { width:100% }
div.ygtvitem table a { display:block }
div.ygtvitem table a:hover { background-color:#c4d7ff }

div.ygtvitem table td.fs_disabled a:hover { background-color:#eeeeee }

div#itemchildren_rootfolder a.droppable_active { backgroundColor:#ffffcc }
div#itemchildren_rootfolder a.droppable_hover { backgroundColor:#ffffcc }

</style>
<script type="text/javascript" src="js/showpage.js"></script>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.11.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="js/yui_0.12.2/build/treeview/assets/folders/tree.css">

<script type="text/javascript">
$.ajaxSettings.type = "POST";
$.ajaxSettings.async = false;
var ee = function(id) { return document.getElementById(id); };


var currentC = "<?=$c?>";
var currentF = "<?=$f?>";
var currentA = "<?=$a?>";
var currentO = "<?=$o?>";

var archiveNames = {
    "1":"<?=get_lib_archive_label(1)?>",
    "2":"<?=get_lib_archive_label(2)?>",
    "3":"<?=get_lib_archive_label(3)?>",
    "4":"<?=get_lib_archive_label(4)?>"
}

var lastClickedCF = "rootfolder_";
var dragDiv = 0;
var dragInfo = 0;

function clearView() {
    ee("itemchildren_rootfolder").innerHTML = "";
    var ml = ee("main_list");
    if (ml) ml.parentNode.removeChild(ml);
    collapseAll();
}

function prepareDroppable() {
    $("div.droppable_target").each(function(){
        this.className = "droppable_prepared";
        $(this).droppable({
            tolerance:"pointer",
            over:function(){
                var clr = "#ccffff"; // 水色：ここへのドロップ可能
                var div = this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
                var obj = dropCheck(getCF(div.id));
                if (obj.errmsg) clr = "#ffcccc"; // 赤：ここへのドロップ不可
                this.style.backgroundColor = clr;
            },
            out:function(){
                this.style.backgroundColor = "";
            },
            drop:function(){
                dropFinish(this);
            }
        });
    });
}


// ドラッグ内容チェック
function dragCheck(srcDiv) {
    var c = srcDiv.getAttribute("c"); // 左ツリーで選択したc
    var f = srcDiv.getAttribute("f"); // 左ツリーで選択したf
    var link_id = srcDiv.getAttribute("link_id"); // フォルダならlink_idが格納されている
    var drag_type = srcDiv.getAttribute("drag_type");
    var content_value = srcDiv.getAttribute("content_value");
    var content_type = srcDiv.getAttribute("content_type");
    var is_wkfw = srcDiv.getAttribute("is_wkfw");
    var jp = "文書";
    if (content_type=="category" || content_type=="folder") jp = "フォルダ";

    if (!content_value) return {bgColor:"#ffcccc", errmsg:"この"+jp+"は移動できません。(b2)"}; // PGエラー
    if (content_type!="document" && content_type!="category" && content_type!="folder") {
        return {bgColor:"#ffcccc", errmsg:"この"+jp+"は移動できません。(b3)"}; // PGエラー
    }
    if (ee("next_archive_label").innerHTML !="") return {bgColor:"#ffcccc", errmsg:"異なる書庫のフォルダは移動できません。"};
    if (content_type=="category") {
        if (currentA=="3") return {bgColor:"#ffcccc", errmsg:"最上位フォルダはマスターメンテナンス機能で管理されています。移動できません。"};
        if (currentA=="4") return {bgColor:"#ffcccc", errmsg:"最上位フォルダは委員会・WG機能で管理されています。移動できません。"};
    }
    <?php if (!$lib_admin_auth) { ?>
        if (content_type=="category" || content_type=="folder") {
            return {bgColor:"#ffcccc", errmsg:"フォルダの移動は管理者のみが行えます。"};
        }
    <?php } ?>

    var bgColor = ""; // 対象外
    var errmsg = "この"+jp+"は移動できません。(b4)";
    if (drag_type=="is_readonly_doc")    { bgColor = "#ffcccc"; errmsg = "更新不可の"+jp+"です。移動できません。"; } // 赤：ドラッグ継続不可
    else if (drag_type=="movable_doc")   { bgColor = "#ccffff"; errmsg = ""; } // 水色：ドラッグ可能

    var folderCF = "";
    if (content_type=="category") folderCF = content_value+"_";
    if (content_type=="folder") folderCF = c+"_"+content_value;

    return {
        errmsg:errmsg, bgColor:bgColor, content_value:content_value, content_type:content_type,
        c:c, f:f, folderCF:folderCF, link_id:link_id, is_wkfw:is_wkfw
    };
}

function getParent(elem) {
    if (elem && elem.getAttribute("eid")=="rootfolder") return "";
    var childrenNode = 0;
    for (;;) {
        var elem = elem.parentNode;
        if (!elem) return "";
        if (elem.id=="left_tree_container") return ee("item_rootfolder_");
        if (elem.className.indexOf("itemchildren")>=0) { childrenNode = elem; break; }
    }
    if (childrenNode) {
        for(var idx=0; idx<childrenNode.parentNode.childNodes.length; idx++) {
            var node = childrenNode.parentNode.childNodes[idx];
            if (node.getAttribute && "itemchildren_"+node.getAttribute("eid")==childrenNode.id) return node;
        }
    }
    return ee("item_rootfolder_");
}

// ドラッグ先チェック
function dropCheck(c_f, isDoDrop) {
    if (!c_f) return {errmsg:"ここへは移動できません。(a2)"}; // PGエラー
    var acf = c_f.split("_");
    if (acf.length!=2) return {errmsg:"ここへは移動できません。(a3)"}; // PGエラー
    if (!acf[0]) return {errmsg:"ここへは移動できません。(a4)"}; // PGエラー
    <?php if ($IS_ADMIN_MENU) { ?>
    if (acf[0]=="rootfolder" && currentA=="1") return {errmsg:"共有へは移動できません。"};
    <?php } ?>
    if (acf[0]=="rootfolder" && currentA=="3") {
        return {errmsg:"共有直下のフォルダはマスターメンテナンス機能で管理されています。移動できません。"};
    }
    if (acf[0]=="rootfolder" && currentA=="4") {
        return {errmsg:"共有直下のフォルダは委員会・WG機能で管理されています。移動できません。"};
    }
    if (acf[0]=="rootfolder" && dragInfo.link_id) return {errmsg:"共有へはリンクフォルダを移動できません。"};
    if (dragInfo.is_wkfw) {
        if (acf[0]=="rootfolder" && dragInfo.content_type=="folder") {
            return {errmsg:"ワークフローとして指定されているフォルダは、最上位階層への昇格移動はできません。"};
        }
        if (acf[0]!="rootfolder" && dragInfo.content_type=="category") {
            return {errmsg:"ワークフローとして指定されているフォルダは、最上位階層からの降格移動はできません。"};
        }
    }
    var divItem = ee("item_"+c_f);
    if (!divItem.getAttribute("writable")) return {errmsg:"ロックされているフォルダへは移動できません。"};
    if (divItem.getAttribute("link_id")) return {errmsg:"リンクフォルダへは移動できません。"};
    if (acf[0]==dragInfo.c && acf[1]==dragInfo.f) return {errmsg:"現在の格納先です。移動できません。"};
    if (dragInfo.content_type=="document") {
        if (acf[0]=="rootfolder") return {errmsg:"共有には文書を移動できません。"};
    }
    var elem = ee("item_"+c_f);
    var parentCF = "";

    if (dragInfo.content_type=="folder" || dragInfo.content_type=="category") {
        if (c_f==dragInfo.folderCF) return {errmsg:"自フォルダへは移動できません。"};
        for (;;) {
            var elem = getParent(elem);
            if (!elem) break;
            var pcf = getCF(elem.id);
            if (pcf==dragInfo.folderCF) return {errmsg:"移動元の配下のフォルダへは移動できません。"};
            parentCF = pcf;
        }
    }

    if (isDoDrop) {
        if (dragInfo.content_type=="category" || dragInfo.content_type=="folder") {
            if (acf[0]=="rootfolder") {
                if (!confirm("共有へ移動しようとしています。\n配下の文書数によっては相当時間がかかります。\n\n移動を実行してよろしいですか？")) return false;
            }
            else if (acf[0] != dragInfo.c) {
                if (!confirm("別の最上位フォルダ配下へ移動しようとしています。\n配下の文書数によっては相当時間がかかります。\n\n移動を実行してよろしいですか？")) return false;
            }
        }
    }

    return {c:acf[0], f:acf[1], errmsg:"", parentCF:parentCF}; // 移動先a, 移動先c, エラーメッセ、移動先の親c_f
}

function logout() {
    alert("長時間利用されませんでしたのでタイムアウトしました。\n再度ログインしてください。");
    showLoginPage(window);
    return 0;
}

function dropFinish(dropDiv) {
    var infoDiv = dropDiv.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
    $(dragDiv).css({backgroundColor:"", zIndex:0});
    if (!dragInfo.bgColor) return; // チェックの結果、背景色指定が不明の場合はドラッグ仕込みを行わないでスルー
    if (dragInfo.errmsg) { dropDiv.style.backgroundColor = ""; return alert(dragInfo.errmsg); }

    // ドラッグ先
    var dropCheckInfo = dropCheck(getCF(infoDiv.id), 1);
    if (!dropCheckInfo) {
        dropDiv.style.backgroundColor = "";
        return;
    }
    if (dropCheckInfo.errmsg) { dropDiv.style.backgroundColor = ""; return alert(dropCheckInfo.errmsg); }
    var c = dropCheckInfo.c; // 移動先c
    var f = dropCheckInfo.f; // 移動先f


    dropDiv.style.backgroundColor = "";
    var url =
        'library_move.php?session=<?=$session?>&ajax_action=move&c=' + c + '&f=' + f + '&a=' + currentA + '&o='+currentO+"&from_c="+currentC+
        '&content_type=' + dragInfo.content_type + "&content_value=" + dragInfo.content_value;
    $.ajax({ url:url, success:function(msg) {
        msg = $.trim(msg);
        if (msg=="SESSION_EXPIRED") {
            return logout();
        }
        if (msg=="DATABASE_SELECT_ERROR") msg = "データベース検索に失敗しました。";
        else if (msg=="DATABASE_UPDATE_ERROR") msg = "データベース更新に失敗しました。";
//        else if (msg!="ok") msg = "システムエラーが発生しました。";
        if (msg!="ok") {
            alert(msg);
            return;
        }
        // 文書の場合：現在のフォルダを変更しつつ、フォルダをリロード
        if (dragInfo.content_type=="document") {
            pickupNode(c, f, currentA);
            return;
        }
        // フォルダをフォルダまたはカテゴリまたは共有へ移動する場合：ツリーを一部組み換え
        // サーバに読み込ませなおす
        // 多くのノードのclass指定を短時間で完了させるのは難しいため。
        // eidやhierアトリビュートの設定をしてまわるのはしんどいため
        // ソートを手動で行うとズレる可能性もあるため
        else {
            // 自分と自分の配下を探し出して抹消する
            if (dragInfo.content_type=="folder") {
                var elem = ee("item_"+dragInfo.c+"_"+dragInfo.content_value); // 移動するDIVタグ
            } else { // カテゴリなら
                var elem = ee("item_"+dragInfo.content_value+"_"); // 移動するDIVタグ
            }
            if (elem) {
                var eid = elem.getAttribute("eid"); // 移動内容
                var elemChild = ee("itemchildren_"+eid); // 移動内容の子要素
                elem.parentNode.removeChild(elem); // 自分は消す。
                elemChild.parentNode.removeChild(elemChild); // 子配列は消す。多くのノードのclass指定を短時間で完了させるのは難しいため。
                delete elem;
                delete elemChild;
            }

            var open_eid_list = []; // 移動先の全小要素の開閉状態
            $("#itemchildren_rootfolder div.the_node").each(function(){ // 表示中の全アイテム
                var _c_f = this.id.substring(5); // 「item_c_f」形式
                var eid = getEid(_c_f);
                var thIcon = ee("icon_"+eid);
                if (thIcon && thIcon.className.substring(5,6)=="m") {
                    open_eid_list.push(eid); // 開いているならeid追記
                }
            });

            var destElem = ee("item_"+c+"_"+f); // 移動先のDIVタグ
            var destEid = destElem.getAttribute("eid"); //移動先のDIVタグの、子要素格納領域
            var destChildren = ee("itemchildren_"+destEid); // 実際の移動先

            destChildren.innerHTML = ""; // 格納先の要素をばっさり全削除
            var postData = "open_eid_list="+encodeURIComponent(open_eid_list.join(","));
            pickupNode(c, f, currentA, postData); // 移動先フォルダを選択状態にする
            ee("itemchildren_"+destEid).style.display = ""; // 移動先を開く
            if (ee("icon_"+destEid)) openNodeIcon(ee("icon_"+destEid)); // rootfolderでなければ移動先フォルダのアイコンを開き状態に
        }
    }});
}


function loadClassAtrb(isInit, a) {
    <?php if ($IS_ADMIN_MENU) { ?> return; <?php } ?>
    if (parseInt(a, 10)!=4) {
        if (ee("class_atrb_container")) ee("class_atrb_container").innerHTML = "";
        return;
    }
    if (!ee("class_id")) {
        var class_id = <?=(int)$class_id?>;
        var atrb_id = <?=(int)$atrb_id?>;
    } else {
        var class_id = $("#class_id").val();
        var atrb_id = $("#atrb_id").val();
    }
    if (!class_id) class_id = "";
    if (!atrb_id) atrb_id = "";
    var params = "&class_id="+class_id+"&atrb_id="+atrb_id;
    <?php if (!$IS_LOGIN_TREE) { ?>
    if (isInit) params += "&load_defalut_class_atrb=1";
    $.ajax({ url:"<?=$fname?>?session=<?=$session?><?=($IS_MYPAGE?'&is_mypage=1':'')?>&ajax_action=load_class_atrb"+params, data:"", success:function(msg) {
        if (ee("class_atrb_container")) ee("class_atrb_container").innerHTML = msg;
    }});
    <?php } ?>
    return;
}

function changeClassAtrb(which) {
    if (which=="class") $("#atrb_id").get(0).selectedIndex = 0;
    if (which=="class") {
        loadClassAtrb("", currentA);
    }
    loadDocument("tree", currentA, "", "", currentO);
    loadDocument("document", currentA, "", "", currentO);
}


function getCF(divId){
    return divId.replace("item_", "");
}
function getEid(c_f) {
    var acf = c_f.split("_");
    if (acf[0]=="rootfolder") return acf[0];
    if (acf[1]) return "f"+acf[1];
    return "c"+acf[0];
}
function loadDocument(what, a, c, f, o, isInit, postData) {
    var class_id = $("#class_id").val();
    var atrb_id = $("#atrb_id").val();
    if (!a) a = "";
    if (!c) c = "";
    if (!f) f = "";
    if (!o) o = "";
    if (!class_id) class_id = "";
    if (!atrb_id) atrb_id = "";
    if (!postData) postData = "";
    if (c=="rootfolder") c = "";
    var elem = ee("item_"+c+"_"+f);
    var other = "";
    if (elem) {
        other =
        "&hier="+elem.getAttribute("hier")+
        "&writable="+elem.getAttribute("writable");
    }
    <?php if ($_REQUEST["path"]) { ?>other += "&path=<?=$_REQUEST["path"]?>";<?php } ?>
    <?php if ($_REQUEST["type"]) { ?>other += "&type=<?=$_REQUEST["type"]?>";<?php } ?>

    var params = "&a="+a+"&c="+c+"&f="+f+"&o="+o+"&class_id="+class_id+"&atrb_id="+atrb_id+other;

    if (!c) c = "rootfolder";
    currentA = a;
    currentC = c;
    currentF = f
    currentO = o;
    if (what == "tree") {
        $.ajax({ url:"<?=$fname?>?session=<?=$session?><?=($IS_MYPAGE?'&is_mypage=1':'')?>&ajax_action=load_tree"+params, data:postData, error:function(XMLHttpRequest, textStatus, errorThrown){ alert("error:"+errorThrown.message+ " " +textStatus+" "+XMLHttpRequest.status);}, success:function(msg) {
            if (!ee("itemchildren_rootfolder")) return alert("ツリーデータ読み込みに失敗しました。(root)");
            if (c == "") {
                ee("itemchildren_rootfolder").innerHTML = msg;
            } else {
                var eid = getEid(c+"_"+f);
                if (!ee("itemchildren_"+eid)) { alert("指定された場所はツリー表示できません。"); return; }
                ee("itemchildren_"+eid).innerHTML = msg;
            }
            checkArchiveNames();
        }});
    }

    if (what == "document") {
        $.ajax({ url:"<?=$fname?>?session=<?=$session?><?=($IS_MYPAGE?'&is_mypage=1':'')?>&ajax_action=load_document"+params, data:"", success:function(msg) {
            if (!ee("document_container")) return alert("document_containerがありません。");
            $("#document_container").html(msg);
            <?php if (!$IS_MYPAGE) { ?>
            $("#btnToUpNode").get(0).disabled = ($("#hidden_btnToUpNode_Enabled").val()?false:true);
            <?php } ?>
            <?php if (!$IS_LOGIN_TREE && !$IS_MYPAGE) { ?>
            $("#btnMakeFolder").get(0).disabled = ($("#hidden_btnMakeFolder_Enabled").val()?false:true);
            $("#btnBunsyo").get(0).disabled = ($("#hidden_btnBunsyo_Enabled").val()?false:true);
            $("#btnChangeName").get(0).disabled = ($("#hidden_btnChangeName_Enabled").val()?false:true);
            $("#btnDelete").get(0).disabled = ($("#hidden_btnDelete_Enabled").val()?false:true);
            <?php } ?>
            checkArchiveNames();
            if (ee("item_" + c + "_" + f)) resetIcon(c + "_" + f);
        }});

        // 「戻る」ボタンが利くようにフォワード
        // データは親画面の隅っこにでも格納しておく
        <?php if (!$IS_LOGIN_TREE && !$IS_MYPAGE) { ?>
        if (window.parent && !avoidHistSave) {
            if (!window.parent.libPageHist) {
                window.parent.libPageHist = {};
                window.parent.libPageHistNumber = -1;
            }
            // ５０個程度以上蓄積されたら削除する
            // ちなみに削除された履歴には戻れないということである。
            // マルチ画面のときは混在するだろうが、不問とする。
            var aHid = [];
            var histLen = 0;
            for (var hid in window.parent.libPageHist) {
                aHid.push(parseInt(hid.substring(1)));
            }
            aHid.sort(function(a, b) { return (parseInt(a) > parseInt(b)) ? 1 : -1; });
            var delLen = aHid.length - 50;
            for (var idx=0; idx<= delLen; idx++) {
                delete window.parent.libPageHist["h"+aHid[idx]];
            }

            var open_eid_list = []; // 移動先の全小要素の開閉状態
            $("#itemchildren_rootfolder div.the_node").each(function(){ // 表示中の全アイテム
                var _c_f = this.id.substring(5); // 「item_c_f」形式
                var eid = getEid(_c_f);
                var thIcon = ee("icon_"+eid);
                if (thIcon && thIcon.className.substring(5,6)=="m") {
                    open_eid_list.push(eid); // 開いているならeid追記
                }
            });
            var hist = {
                open_eid_list:open_eid_list.join(","),
                a:currentA, c:currentC, f:currentF, o:currentO,
                class_id:class_id, atrb_id:atrb_id
            };
            window.parent.libPageHistNumber++;
            window.parent.libPageHist["h"+window.parent.libPageHistNumber] = hist;
            hist_return_check["h"+window.parent.libPageHistNumber] = 1;
            document.frm_pagehist.pagehist_number.value = window.parent.libPageHistNumber;
            document.frm_pagehist.a.value = currentA;
            document.frm_pagehist.c.value = currentC;
            document.frm_pagehist.f.value = currentF;
            document.frm_pagehist.o.value = currentO;
            document.frm_pagehist.class_id.value = class_id;
            document.frm_pagehist.atrb_id.value = atrb_id;
            document.frm_pagehist.submit();
        }
        <?php } ?>
    }
}

function checkArchiveNames() {
    ee("rootfolder_label").innerHTML = archiveNames[currentA];
    if (!ee("available_archives")) {
        alert("エラーが発生しました。");
    } else {
        var aa = ee("available_archives").value;
        <?php if (!$IS_MYPAGE) { ?>
            ee("submenu1").style.display = (aa.indexOf("private")>=0 ? "" : "none");
            ee("submenu3").style.display = (aa.indexOf("section")>=0 ? "" : "none");
            ee("submenu4").style.display = (aa.indexOf("project")>=0 ? "" : "none");
            for (var idx=1; idx<=4; idx++) {
                $("#submenu"+idx+" a").get(0).className = (currentA==idx?"selected":"unselected");
            }
        <?php } else { ?>
            var html = [];
            html.push("書庫：&nbsp;");
            html.push('<select onchange="clearView(); pickupNode(\'\',\'\',this.value);">');
            html.push('<option value="2"'+(currentA=="2"?" selcted":"")+'>全体共有</option>');
            var fok = "";
            if (aa.indexOf("section")>=0) { fok=1; html.push('<option value="3"'+(currentA=="3"?" selected":"")+'>部署共有</option>'); }
            if (aa.indexOf("project")>=0) { fok=1; html.push('<option value="4"'+(currentA=="4"?" selected":"")+'>委員会・WG共有</option>'); }
            if (aa.indexOf("private")>=0) { fok=1; html.push('<option value="1"'+(currentA=="1"?" selected":"")+'>個人共有</option>'); }
            html.push("</select>");
            ee("submenu_selector_container").innerHTML = html.join("");
            ee("submenu_selector_container").style.visibility = (fok ? "visible" : "hidden");
        <?php } ?>
    }
}

function showRightList2(aTag) {
    var div = aTag.parentNode.parentNode.parentNode.parentNode.parentNode;
    var c_f = getCF(div.id);
    showRightList(c_f);
}
function showRightList(c_f) {
    if (lastClickedCF) $("#item_"+lastClickedCF).removeClass('selectedLabel');
    $("#item_"+c_f).addClass('selectedLabel');
    lastClickedCF = c_f;
    var acf = c_f.split("_");
    loadDocument("document", currentA, acf[0], acf[1], currentO);
}
function selectNode(c, f, a, postData) {
    location.href = 'library_folder_select_exe.php?session=<?=$session?>&a='+a+'&c='+c+'&f='+f+'&path=<?=$_REQUEST["path"]?>&type=<?=$_REQUEST["type"]?>';
    return false;
}

function pickupNode(c, f, a, postData, link_id) {
    if (!c) c = "rootfolder";
    if (!f) f = "";
    if (!a) a = currentA;
    var c_f = c+"_"+f;
    if (!postData) postData = "";
    // 書庫が違えば書き直し、移動先ノードがなければ書き足し
    if (a!=currentA) {
        loadClassAtrb(1, a);
        ee("itemchildren_rootfolder").innerHTML = "";
    }
    if (!link_id) {
        loadTreeChild(a, c_f, postData);
    }
    if (lastClickedCF) $("#item_"+lastClickedCF).removeClass('selectedLabel');
    $("#item_"+c_f).addClass('selectedLabel'); // 左側文字をボールドに

    <?php if ($IS_FOLDER_SELECT) { ?>
    return;
    <?php } ?>
    // 右側を読み込む
    loadDocument("document", a, c, f, currentO);
    lastClickedCF = c_f;
}

// 描画が必要なノードを全部書き足す
function loadTreeChild(a, c_f, postData) {
    // 表示したいノード
    if (!postData) postData = "";
    if (c_f=="_" || c_f == "") c_f = "rootfolder_";
    var acf = c_f.split("_");

    // ノードがあって、子リストが１件以上存在するなら描画しない
    var eid = getEid(c_f);
    var divItem = document.getElementById("item_"+c_f);
    var oneChild = $("#itemchildren_"+eid+" div.the_node").get(0);
    if (divItem && oneChild) {
        var pNode = divItem;
        for (;;) {
            pNode = getParent(pNode); // ひとつ上
            if (!pNode) return;
            var eid = pNode.getAttribute("eid");
            ee("itemchildren_"+eid).style.display = ""; // ひとつ上の小リストを表示
            if (eid!="rootfolder") openNodeIcon(ee("icon_"+eid)); // アイコンを開く
        }
    }

    // 表示したいノードの親階層一覧を読み込む
    $.ajax({ url:"<?=$fname?>?session=<?=$session?><?=($IS_MYPAGE?'&is_mypage=1':'')?>&ajax_action=load_parents&c="+acf[0]+"&f="+acf[1], success:function(parentCsv) {
        if (parentCsv=="") return; // 親も自分も居ない。描画不要
        parentCsv = $.trim(parentCsv);
        var parents = parentCsv.split(",");
        var prevEid = "";
        for (var idx=0; idx<parents.length; idx++) { // 上位階層から呼び出す
            if (prevEid) {
                ee("itemchildren_"+prevEid).style.display = ""; // ひとつ上の小リストを表示
                if (prevEid!="rootfolder") openNodeIcon(ee("icon_"+prevEid)); // アイコンを開く
            }
            var _c_f = parents[idx];
            var _acf = _c_f.split("_");
            if (_acf[0].indexOf("rootfolder")>=0) _acf[0]= "rootfolder";
            if (_acf.length!=2) {
                if (parentCsv.indexOf("showLoginPage")>=0) return logout();
                alert("階層情報の取得に失敗しました。(1)("+parentCsv+")");
                return;
            }
            if (_acf[0]!="rootfolder" && !_acf[0].match(/^[1-9][0-9]*$/)) {
                if (parentCsv.indexOf("showLoginPage")>=0) return logout();
                alert("階層情報の取得に失敗しました。(2)("+parentCsv+")");
                return;
            }
            if (_acf[1]!="" && !_acf[1].match(/^[1-9][0-9]*$/)) {
                if (parentCsv.indexOf("showLoginPage")>=0) return logout();
                alert("階層情報の取得に失敗しました。(3)("+parentCsv+")");
                return;
            }

            var eid = getEid(_c_f);

            var divItem = document.getElementById("item_"+_c_f);
            var oneChild = $("#itemchildren_"+eid+" div.the_node").get(0);
            var divChildren = document.getElementById("itemchildren_"+eid);
            if (!divItem || !oneChild) { // ノードが無いか、子リストがゼロ件なら描画トライ
                loadDocument("tree", a, _acf[0], _acf[1], currentO, "", postData);
            }
            if (!ee("itemchildren_"+eid)) return; // 権限がない等で描画できなかった。終了
            prevEid = eid;
        }
    }});
}
function moveToParent() {

    var elem = ee("item_"+currentC+"_"+currentF);
    elem = getParent(elem);
    if (!elem) return;

    // 親がいなければ全部リロード
    if (!elem) {
        loadDocument("document", currentA, "", "", currentO);
        if (lastClickedCF) $("#item_"+lastClickedCF).removeClass('selectedLabel');
        return;
    }
    if (lastClickedCF) $("#item_"+lastClickedCF).removeClass('selectedLabel');
    lastClickedCF = getCF(elem.id);
    $("#item_"+lastClickedCF).addClass('selectedLabel');
    var acf = lastClickedCF.split("_");
    pickupNode(acf[0], acf[1], currentA);
}

function createFolder() {
    var class_id;
    if (typeof $("#class_id").val() !== 'undefined') {
        class_id = $("#class_id").val();
    }
    else {
        class_id = 0;
    }

    var atrb_id;
    if (typeof $("#atrb_id").val() !== 'undefined') {
        atrb_id = $("#atrb_id").val();
    }
    else {
        atrb_id = 0;
    }

    var c = (currentC=="rootfolder" ? "" : currentC);
    location.href =
    'library_folder_register.php?session=<?=$session?><?=($_REQUEST["path"]?"&path=".$_REQUEST["path"]:"")?><?=($_REQUEST["type"]?"&type=".$_REQUEST["type"]:"")?>'+
    '&archive=' + currentA + '&cate_id=' + c + '&parent=' + currentF + '&o=' + currentO + '&class_id=' + class_id + '&atrb_id=' + atrb_id;
}

function registDocument() {
    location.href =
    'library_register.php?session=<?=$session?><?=($_REQUEST["path"]?"&path=".$_REQUEST["path"]:"")?><?=($_REQUEST["type"]?"&type=".$_REQUEST["type"]:"")?>'+
    '&archive='+currentA+'&category='+currentC+'&folder_id='+currentF+'&o='+currentO;
}

function updateFolder() {
    location.href =
    'library_folder_update.php?session=<?=$session?><?=($_REQUEST["path"]?"&path=".$_REQUEST["path"]:"")?><?=($_REQUEST["type"]?"&type=".$_REQUEST["type"]:"")?>'+
    '&arch_id='+currentA+'&cate_id='+currentC+'&folder_id='+currentF+'&o='+currentO;
}

function deleteDocument() {
    var checked_cid = getCheckedIds(document.mainform.elements['cid[]']);
    var checked_did = getCheckedIds(document.mainform.elements['did[]']);
    var checked_fid = getCheckedIds(document.mainform.elements['fid[]']);

    if (checked_cid.length == 0 && checked_did.length == 0 && checked_fid.length == 0) {
        alert('削除対象が選択されていません。');
        return;
    }

    if (!confirm('選択されたデータを削除します。よろしいですか？')) {
        return;
    }

    document.mainform.action =
    'library_delete.php?session=<?=$session?><?=($_REQUEST["path"]?"&path=".$_REQUEST["path"]:"")?><?=($_REQUEST["type"]?"&type=".$_REQUEST["type"]:"")?>'+
    '&archive='+currentA+'&category='+currentC+'&folder_id='+currentF+'&o='+currentO;
    document.mainform.submit();
}

function collapseAll() {
    var btn = ee("btn_expand_collapse");
    btn.setAttribute("isExpanding", "1");
    expandCollapseAll(btn);
}
function expandCollapseAll(btn) {
    btn.disabled = true;
    var isExpanding = (btn.getAttribute("isExpanding") ? "" : "1");

    // 全て開く場合
    if (isExpanding) {
        loadDocument("tree", currentA, "", "", currentO, "", "&isGetAllChild=1");
    } else {
        // 子ノードを開閉する（実際は閉じる場合のみ）
        $("#itemchildren_rootfolder div.ygtvitemchildren").css({display:(isExpanding?"":"none")});
        // アイコン開閉マーク変更
        $("#itemchildren_rootfolder th").each(function() {
            var cls = this.getAttribute("org_class");
            var No6 = cls.substring(5,1);
            this.className = (isExpanding && No6!="n" ? cls.substring(0,5)+"m" : cls); // 開いているなら閉じるアイコンに
        });
    }
    btn.value = (isExpanding ? "全て閉じる" : "全て開く");
    btn.setAttribute("isExpanding", isExpanding);
    btn.disabled = false;

    if (lastClickedCF) $("#item_"+lastClickedCF).addClass('selectedLabel'); // 左側文字をボールドに
}

function getCheckedIds(boxes) {
    var checked_ids = new Array();
    if (boxes) {
        if (!boxes.length) {
            if (boxes.checked) {
                checked_ids.push(boxes.value);
            }
        } else {
            for (var i = 0, j = boxes.length; i < j; i++) {
                if (boxes[i].checked) {
                    checked_ids.push(boxes[i].value);
                }
            }
        }
    }
    return checked_ids;
}

function addHiddenElement(frm, name, value) {
    var input = document.createElement('input');
    input.type = 'hidden';
    input.name = name;
    input.value = value;
    frm.appendChild(input);
}

function referDocument(path) {
    window.open(path);
}

function hoverIcon(div, isMouseOver) {
    if (div.className.substring(5,6)=="n") return;
    if (isMouseOver) div.className = div.className.substring(0,6) + "h";
    else div.className = div.className.substring(0,6);
}
function loadTreeNextChild(aTag) {
    var div = aTag.parentNode.parentNode.parentNode.parentNode;
    var c_f = getCF(div.id);
    var a = div.getAttribute("a");
    acf = c_f.split("_");
    loadTreeChild(a, c_f, "");
    var divItem = document.getElementById("item_"+c_f);
    var child = document.getElementById("itemchildren_"+getEid(c_f));
    var divIcon = document.getElementById("icon_"+getEid(c_f));
    if (child.innerHTML=="") {
        child.style.display = "none";
        divIcon.className = divIcon.getAttribute("org_class");
    } else {
        if (child.style.display=="none" || c_f == "rootfolder_") {
            child.style.display = "";
            divIcon.className = divIcon.className.substring(0,5)+"m";
        } else {
            child.style.display = "none";
            divIcon.className = divIcon.getAttribute("org_class");
        }
    }
}
function resetIcon(c_f) {
    if (c_f=="rootfolder_") return;
    var child = document.getElementById("itemchildren_"+getEid(c_f));
    var divIcon = document.getElementById("icon_"+getEid(c_f));

    var no5 = (ee("item_"+c_f).getAttribute("item_ab")=="a" ? "t" : "l");
    var no6 = (child.innerHTML!="" ? "p" : "n");
    var org_class = "ygtv" + no5 + no6;
    divIcon.setAttribute("org_class", org_class);
    if (child.innerHTML=="") {
        child.style.display = "none";
        divIcon.className = divIcon.getAttribute("org_class");
    } else {
        if (child.style.display=="none" || c_f == "rootfolder_") {
            divIcon.className = org_class;
        } else {
            divIcon.className = org_class.substring(0,5)+"m";
        }
    }
}
function openNodeIcon(divIcon, isClose) {
    if (isClose) divIcon.className = divIcon.getAttribute("org_class");
    divIcon.className = divIcon.className.substring(0,5)+"m";
}

function resizeIframeEx() {
    var win = window.parent;
    <?php if ($IS_LOGIN_TREE) { ?>
    var screenHeight = window.document.body.clientHeight;
    var hh = ee("mypage_content_area").offsetHeight;
    ee("left_tree_container").style.height = (screenHeight - 100 - 38) + "px";
    ee("document_container").style.height  = (screenHeight - 100 - 12) + "px";
    return;
    <?php } ?>
    if (!win) return;
    var floatingpage = win.document.getElementById("floatingpage");
    if (floatingpage) {
        var screenHeight = window.document.body.clientHeight;
        var contentTop = 0;
        if (win.floatingpage) {
            var screenHeight = win.document.body.clientHeight;
            var dom = floatingpage;
            for(;;) {
                contentTop += dom.offsetTop;
                dom = dom.parentNode;
                if (dom && dom.tagName.toLowerCase() == "body") break;
            }
        }
        var mainTable = ee("content_table");
        if (!mainTable) return;
        var iframeMargin = 18;
        var ttlHeight = screenHeight - contentTop - iframeMargin;
        if (ttlHeight < mainTable.offsetTop) return;

        if (ttlHeight - mainTable.offsetTop - 38 >= 0) ee("left_tree_container").style.height = (ttlHeight - mainTable.offsetTop - 38) + "px";
        if (ttlHeight - mainTable.offsetTop - 12 >= 0) ee("document_container").style.height  = (ttlHeight - mainTable.offsetTop - 12) + "px";
        if (ttlHeight >= 0) floatingpage.style.height = ttlHeight + "px";
        return;
    }
    var library_area = win.document.getElementById("library_area");
    if (library_area) {
        var oh = 60;
        if (ee("main_list") && ee("left_tree_inner")) {
            var h1 = ee("left_tree_inner").offsetHeight;
            var h2 = ee("main_list").offsetHeight;
            var oh = Math.max(h1, h2);
            oh = Math.min(500, oh+40);

            ee("left_tree_container").style.height = (oh-38) + "px";
            ee("document_container").style.height  = (oh-12) + "px";
        }
        oh = document.getElementById("mypage_content_area").offsetHeight;
        win.libraryResize(oh);
    }
}

var initPageType = 0;

// 「戻る」ボタンで戻った場合
// データを引き出して画面を復元
var avoidHistSave = 0;
var hist_return_check = {};
function callbackPageHist(pagehistNumber) {
    var libPageHist = window.parent.libPageHist;
    if (!libPageHist) return;
    if (hist_return_check["h"+pagehistNumber]) {
        delete hist_return_check["h"+pagehistNumber];
        return;
    }
    if (!window.parent) return;
    var libPageHist = window.parent.libPageHist;
    if (!libPageHist) return;
    var hist = libPageHist["h"+pagehistNumber];
    if (!hist) return;
    $("#mainform").css({display:"none"});
    currentA = hist.a;
    currentC = hist.c;
    currentF = hist.f;
    currentO = hist.o;
    clearView();
    initPage(2, "open_eid_list="+encodeURIComponent(hist.open_eid_list));
    $("#class_id").val(hist.class_id);
    $("#atrb_id").val(hist.atrb_id);
    avoidHistSave = 1;
    pickupNode(hist.c, hist.f, currentA, "open_eid_list="+encodeURIComponent(hist.open_eid_list));
    avoidHistSave = 0;

    if (lastClickedCF) $("#item_"+lastClickedCF).removeClass('selectedLabel');
    lastClickedCF = hist.lastClickedCF;
    lastClickedCF = currentC + "_" + currentF;
    $("#item_"+lastClickedCF).addClass('selectedLabel');
    $("#mainform").css({display:""});
}


function initPage(initType, postData) {
    if (initPageType && initType==1) return;
    initPageType = initType;
    try{ parent.scrollTo(0,0); } catch(ex){}
    if (currentA=="") {
        var div = document.getElementById('itemchildren_rootfolder');
        div.style.padding = '2px';
        div.innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">書庫を選択してください。</font>';
        return;
    }
    var c = currentC;
    var f = currentF;
    loadClassAtrb(1, currentA);
    avoidHistSave = 1;
    collapseAll();
    pickupNode(c, f, currentA, postData);
    avoidHistSave = 0;
    setInterval("resizeIframeEx()", 500);
}

function locationPage(url) {
    location.href = url + "?session=<?=$session?>&a="+currentA;
}

</script>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
    onload="initPage(1); $('#mainform').css({display:''});resizeIframeEx();" onresize="resizeIframeEx()">
<div id="mypage_content_area"<?=($IS_MYPAGE ?' style="padding-top:5px"':'')?>>



<?php if ($IS_LOGIN_TREE || $IS_FOLDER_SELECT) { ?><div style="padding:5px"><?php } ?>



<form name="mainform" method="post" id="mainform" style="display:none">

<div class="j12" id="drag_container" style="position:absolute; white-space:nowrap; z-index:10000;"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>






<?php
//=================================================================================================================
// ロゴ、ぱんくず１：ログインツリーの場合はダイアログヘッダ
//=================================================================================================================
?>
<?php if ($IS_LOGIN_TREE) { ?>
<div style="padding-bottom:5px">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr height="32" bgcolor="#5279a5">
            <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>文書一覧</b></font></td>
            <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"
                ><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0"
                    onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a>
            </td>
        </tr>
    </table>
</div>
<?php } ?>


<?php if ($IS_FOLDER_SELECT) { ?>
<div style="padding-bottom:5px">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr height="32" bgcolor="#5279a5">
            <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$_title?></b></font></td>
            <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"
                ><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0"
                    onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
        </tr>
    </table>
</div>
<?php } ?>








<?php
    if ($referer == "2") {
        // イントラメニュー情報を取得
        $sel = select_from_table($con, "select * from intramenu", "", $fname);
        if ($sel == 0) libcom_goto_error_page_and_die();
        $intra_menu2 = pg_fetch_result($sel, 0, "menu2");
        $intra_menu2_3 = pg_fetch_result($sel, 0, "menu2_3");
    }
?>






<?php if (!$IS_LOGIN_TREE && !$IS_MYPAGE && !$IS_FOLDER_SELECT) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<?php } ?>




<?php
//=================================================================================================================
// ロゴ、ぱんくず（referer==2） イントラネット＋管理画面
//=================================================================================================================
?>
<?php if (!$IS_FOLDER_SELECT) { ?>
<?php if (!$IS_LOGIN_TREE) { ?>
<?php if (!$IS_MYPAGE) { ?>
<?php if ($referer == "2") { ?>
<?php if ($IS_ADMIN_MENU) { ?>
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<?=$session?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a><span id="navigation_container" style="display:none"></span></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<?=$session?>"><b>イントラネット</b></a> &gt; <a href="intra_info.php?session=<?=$session?>"><b><?php echo($intra_menu2); ?></b></a> &gt; <a href="library_list_all.php?session=<?=$session?>"><b><?php echo($intra_menu2_3); ?></b></a> &gt; <a href="javascript:void(0)" onclick="locationPage('library_admin_menu.php')"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="javascript:void(0)" onclick="locationPage('library_list_all.php')"><b>ユーザ画面へ</b></a></font></td>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>



<?php
//=================================================================================================================
// ロゴ、ぱんくず（referer==2） イントラネット＋ユーザ画面
//=================================================================================================================
?>
<?php if (!$IS_FOLDER_SELECT) { ?>
<?php if (!$IS_LOGIN_TREE) { ?>
<?php if ($referer == "2") { ?>
<?php if (!$IS_ADMIN_MENU) { ?>
<?php if (!$IS_MYPAGE) { ?>
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<?=$session?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<?=$session?>"><b>イントラネット</b></a> &gt;
<a href="intra_info.php?session=<?=$session?>"><b><?php echo($intra_menu2); ?></b></a> &gt;
<a href="<?=$fname?>?session=<?=$session?>"><b><?php echo($intra_menu2_3); ?></b></a>
<span id="navigation_container"></span>
</font>
</td>
    <?php if ($lib_admin_auth == "1") { ?>
    <td align="right" style="padding:0 6px;" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="javascript:void(0)" onclick="locationPage('library_admin_menu.php')"><b>管理画面へ</b></a></font></td>
    <?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>


<?php
//=================================================================================================================
// ロゴ、ぱんくず（referer2以外）イントラ以外＋管理画面
//=================================================================================================================
?>
<?php if (!$IS_FOLDER_SELECT) { ?>
<?php if (!$IS_LOGIN_TREE) { ?>
<?php if ($referer != "2") { ?>
<?php if ($IS_ADMIN_MENU) { ?>
<?php if (!$IS_MYPAGE) { ?>
    <td width="32" height="32" class="spacing"><a href="library_list_all.php?session=<?=$session?>"><img src="img/icon/b12.gif" width="32" height="32" border="0" alt="文書管理"></a></td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="library_list_all.php?session=<?=$session?>"><b>文書管理</b></a> &gt; <a href="library_admin_menu.php?session=<?=$session?>"><b>管理画面</b></a></font><span id="navigation_container" style="display:none"></span></td>
    <td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="javascript:void(0)" onclick="locationPage('library_list_all.php')"><b>ユーザ画面へ</b></a></font></td>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>



<?php
//=================================================================================================================
// ロゴ、ぱんくず（referer2以外）イントラ以外＋ユーザ画面
//=================================================================================================================
?>
<?php if (!$IS_FOLDER_SELECT) { ?>
<?php if (!$IS_LOGIN_TREE) { ?>
<?php if ($referer != "2") { ?>
<?php if (!$IS_ADMIN_MENU) { ?>
<?php if (!$IS_MYPAGE) { ?>
    <td width="32" height="32" class="spacing" style="text-align:left"><a href="library_list_all.php?session=<?=$session?>"><img src="img/icon/b12.gif" width="32" height="32" border="0" alt="文書管理"></a></td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?=$fname?>?session=<?=$session?>"><b>文書管理</b></a>
        <span id="navigation_container"></span>
    </font></td>
    <?php if ($lib_admin_auth == "1") { ?>
        <td align="right" style="padding:0 6px;" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="javascript:void(0)" onclick="locationPage('library_admin_menu.php')"><b>管理画面へ</b></a></font></td>
    <?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>




<?php if (!$IS_LOGIN_TREE && !$IS_MYPAGE && !$IS_FOLDER_SELECT) { ?>
</tr>
</table>
<?php } ?>












<?php if (!$IS_MYPAGE && !$IS_FOLDER_SELECT) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<?php } ?>



<?php
//=================================================================================================================
// メニューボタン（referer==2） イントラネットx管理メニュー
//=================================================================================================================
?>
<?php if ($referer == "2") { ?>
<?php if ($IS_ADMIN_MENU) { ?>
<?php if (!$IS_FOLDER_SELECT){ ?>
<?php if (!$IS_LOGIN_TREE){ ?>
<?php if (!$IS_MYPAGE) { ?>
<td width="75" align="center" bgcolor="#5279a5"><a href="library_admin_menu.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>文書一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_admin_refer_log.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照履歴</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_config.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td style="width:auto">&nbsp;</td>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>



<?php
//=================================================================================================================
// メニューボタン（referer==2） イントラネットxユーザメニュー
//=================================================================================================================
?>
<?php if ($referer == "2") { ?>
<?php if (!$IS_FOLDER_SELECT){ ?>
<?php if (!$IS_ADMIN_MENU) { ?>
<?php if (!$IS_LOGIN_TREE){ ?>
<?php if (!$IS_MYPAGE) { ?>
<td width="105" align="center" bgcolor="#bdd1e7"><a href="intra_menu.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メインメニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="<?php echo(get_tab_width($intra_menu2)); ?>" align="center" bgcolor="#bdd1e7"><a href="intra_info.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo($intra_menu2); ?></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#5279a5"><a href="<?=$fname?>?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>文書一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_register.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_search.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_refer_log.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照履歴</font></a></td>
<td width="5">&nbsp;</td>
    <?php if ($newlist_flg == 't') { ?>
    <td width="75" align="center" bgcolor="#bdd1e7"><a href="library_list_new.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>最新文書一覧</nobr></font></a></td>
    <td width="5">&nbsp;</td>
    <td width="75" align="center" bgcolor="#bdd1e7"><a href="library_option.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
    <?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>


<?php
//=================================================================================================================
// メニューボタン（referer==2） イントラ以外x管理メニュー
//=================================================================================================================
?>
<?php if ($referer != "2") { ?>
<?php if (!$IS_FOLDER_SELECT){ ?>
<?php if ($IS_ADMIN_MENU) { ?>
<?php if (!$IS_LOGIN_TREE){ ?>
<?php if (!$IS_MYPAGE) { ?>
<td width="75" align="center" bgcolor="#5279a5"><a href="<?=$fname?>?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>文書一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_admin_refer_log.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照履歴</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_config.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>



<?php
//=================================================================================================================
// メニューボタン（referer==2） イントラ以外xユーザメニュー
//=================================================================================================================
?>
<?php if ($referer != "2") { ?>
<?php if (!$IS_FOLDER_SELECT){ ?>
<?php if (!$IS_ADMIN_MENU) { ?>
<?php if (!$IS_LOGIN_TREE){ ?>
<?php if (!$IS_MYPAGE) { ?>
<td width="75" align="center" bgcolor="#5279a5"><a href="<?=$fname?>?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>文書一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_register.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書登録</font></a></td>
<td width="5">&nbsp;</td>

<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_search.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書検索</font></a></td>
<td width="5">&nbsp;</td>

<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_refer_log.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照履歴</font></a></td>
<td width="5">&nbsp;</td>

    <?php if ($newlist_flg == 't') { ?>
    <td width="75" align="center" bgcolor="#bdd1e7"><a href="library_list_new.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>最新文書一覧</nobr></font></a></td>
    <td width="5">&nbsp;</td>
    <td width="75" align="center" bgcolor="#bdd1e7"><a href="library_option.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
    <?php } ?>

<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>




<?php
//=================================================================================================================
// メニューボタン ログイン画面
//=================================================================================================================
?>
<?php if ($IS_LOGIN_TREE){ ?>
<td width="75" align="center" bgcolor="#5279a5"><a href="<?=$fname?>?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>文書一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_search_from_login.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書検索</font></a></td>
<td width="5">&nbsp;</td>
<?php } ?>








<?php if (!$IS_MYPAGE && !$IS_FOLDER_SELECT) { ?>
<td style="width:auto">&nbsp;</td>
<td align="right" id="class_atrb_container">

<?php // ここにclassmst・atrbmst選択プルダウンが表示される ?>

</td>
<?php } ?>




<?php if (!$IS_MYPAGE && !$IS_FOLDER_SELECT) { ?>
</tr>
</table>
<?php } ?>







<?php
//=================================================
// 上下スペース
//=================================================
?>
<?php if (!$IS_MYPAGE && !$IS_FOLDER_SELECT) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<?php } ?>





<?php
//=================================================================================================================
// 画面左上  全体・部署などのグループ選択リンク
//=================================================================================================================
?>
<?php if (!$IS_MYPAGE) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<!-- 書庫 -->
<td style="width:330px" id="archive_buttons_container"><?php // 80px かける 4つ ?>
    <table cellspacing="0" cellpadding="0" id="tbl_archive_buttons"><tr>
    	<?php if ($IS_LOGIN_TREE) { ?>
        <td id="submenu2"><a href="library_tree_from_login.php?a=2" onclick="clearView(); pickupNode('','','2'); return false;">全体</a></td>
        <td id="submenu3"><a href="library_tree_from_login.php?a=3" onclick="clearView(); pickupNode('','','3'); return false;">部署</a></td>
        <td id="submenu4"><a href="library_tree_from_login.php?a=4" onclick="clearView(); pickupNode('','','4'); return false;"><nobr>委員会・WG</nobr></a></td>
        <td id="submenu1"><a href="library_tree_from_login.php?a=1" onclick="clearView(); pickupNode('','','1'); return false;">個人</a></td>
        <?php } else { ?>
        <td id="submenu2"><a href="library_list_all.php?session=<?=$session?>&a=2" onclick="clearView(); pickupNode('','','2'); return false;">全体</a></td>
        <td id="submenu3"><a href="library_list_all.php?session=<?=$session?>&a=3" onclick="clearView(); pickupNode('','','3'); return false;">部署</a></td>
        <td id="submenu4"><a href="library_list_all.php?session=<?=$session?>&a=4" onclick="clearView(); pickupNode('','','4'); return false;"><nobr>委員会・WG</nobr></a></td>
        <td id="submenu1"><a href="library_list_all.php?session=<?=$session?>&a=1" onclick="clearView(); pickupNode('','','1'); return false;">個人</a></td>
        <?php } ?>
    </tr></table>
</td>

<td width="4"></td>


<?php
//=================================================================================================================
// 画面右上  登録系などのボタン
//=================================================================================================================
?>
<!-- ボタン -->
<td align="right">

<nobr>
<span id="next_archive_label" style="color:#999900"></span>
<?php if (!$IS_MYPAGE && !$IS_FOLDER_SELECT) { ?>
<input type="button" id="btnToUpNode" value="上の階層へ" disabled onclick="moveToParent();">
<?php } ?>
<?php if (!$IS_LOGIN_TREE && !$IS_MYPAGE && !$IS_FOLDER_SELECT) { ?>
<input type="button" id="btnMakeFolder" value="フォルダ作成" disabled onclick="createFolder();">
<input type="button" id="btnBunsyo" value="文書登録" disabled onclick="registDocument();">
<input type="button" id="btnChangeName" value="フォルダ名更新" disabled onclick="updateFolder();">
<input type="button" id="btnDelete" value="削除" disabled onclick="deleteDocument();">
<?php } ?>
</nobr></td>
</tr>
<tr height="2">
<td colspan="3"></td>
</tr>
</table>
<?php } ?>






<?php
//=================================================================================================================
// 左右メインコンテンツ開始  マイページならタイトルバー
//=================================================================================================================
?>
<?php if ($tree_flg=="t") { ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="content_table"<?=($IS_MYPAGE?' class="list"':'')?> style="border-collapse:collapse">




<?php if ($IS_MYPAGE) { ?>
    <tr><td colspan="3" style="background-color:#bdd1e7; width:100%; border:1px solid #5279a5">
    <table id="doc" cellspacing="0" cellpadding="0" style="width:100%"><tr>

    <?php if ($font_size == "1") { ?>
        <td width="20" height="22" class="spacing"><a href="library_list_all.php?session=<?=$session?>"><img src="img/icon/s12.gif" width="20" height="20" border="0" alt="文書管理"></a></td>
    <?php } else { ?>
        <td width="32" height="32" class="spacing"><a href="library_list_all.php?session=<?=$session?>"><img src="img/icon/b12.gif" width="32" height="32" border="0" alt="文書管理"></a></td>
    <?php } ?>
    <td style="text-align:left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>"><a href="library_list_all.php?session=<?=$session?>" style="padding-left:3px"<?=($IS_MYPAGE?' target="_parent"':'')?>><b>文書管理</b></a></font>
    <span class="<?=$font_class?>" id="submenu_selector_container" style="padding-left:50px"></span>
<span id="next_archive_label" style="padding-left:50px;"></span>
    </td>
    </tr></table>
    </td></tr>
<?php } ?>


<tr valign="top">


<?php
//=================================================================================================================
// 左側  ツリー
//=================================================================================================================
?>
<?php if ($IS_FOLDER_SELECT) { ?>
<td>
<?php } else { ?>
<td width="30%">
<?php } ?>



<?php } else { ?>
    <div style="display:none">
<?php } ?>




<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
<tr><td style="border-bottom:<?=(!$IS_MYPAGE?"0":"1px solid #5279a5")?>">
    <table width="100%" border="0" cellspacing="0" cellpadding="2"><tr height="22" bgcolor="#f6f9ff">
        <td class="spacing j12">フォルダ</td>
        <td align="right"><input type="button" value="全て開く" onclick="expandCollapseAll(this);" isExpanding="" id="btn_expand_collapse"></td>
    </tr></table>
</td></tr></table>





<?php // ★★★★★★ ツリーコンテンツ ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0"<?=(!$IS_MYPAGE?' class="list"':'')?>>
<tr><td>
    <div style="<?=($IS_FOLDER_SELECT?"":"overflow-y:scroll;")?>" id="left_tree_container" tabIndex="0">
    <?php // 共有ノード HTMLは yahoo ui の treeviewの方式に従う ?>
    <div id="left_tree_inner" style="position:relative">
    <div id="item_rootfolder_" class="the_node ygtvitem selectedLabel" hier="" writable="1" link_id="" a="" eid="rootfolder"
    <?=($IS_FOLDER_SELECT ? ' style="display:none"' : "")?>>
        <table border="0" cellpadding="0" cellspacing="0"><tbody><tr>
            <td class="ygtvtn" style="background-position:-16px 0; width:20px"><div class="ygtvspacer"></div></td>
            <td><a class="ygtvlabel" href="javascript:void(0)" onclick="showRightList('rootfolder_')">
                <div class="droppable_target">
                    <span class="j12 node_span" id="rootfolder_label"></span>
                </div>
            </a></td>
        </tr></tbody></table>
    </div>
    <?php // 子ノード一覧   HTMLは yahoo ui の treeviewの方式に従う ?>
    <div class="ygtvitemchildren" id="itemchildren_rootfolder"></div>
    </div>
    </div>
</td></tr></table>







<?php if ($tree_flg=="t") { ?>
    </td>
    <?php if (!$IS_MYPAGE) { ?>
    <td width="4"></td>
    <?php } ?>
<?php } else { ?>
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="content_table">
    <tr valign="top">
<?php } ?>






<?php
//=================================================================================================================
// 右側  文書一覧
//=================================================================================================================
?>
<!-- 文書一覧 -->
<?php if (!$IS_FOLDER_SELECT) { ?>
<td>
    <div style="overflow-y:scroll;border:1px solid #cccccc; position:relative;" id="document_container">
    </div>
</td>
<?php } ?>
</tr>
</table>
<input type="hidden" id="available_archives" value="<?=implode(",",$available_archives)?>" />
</td>
</tr>
</table>



<input type="hidden" name="session" value="<?=$session?>">
</form>

<?php if ($IS_LOGIN_TREE||$IS_FOLDER_SELECT) { ?></div><?php } ?>
</div><!-- // mypage_content_area -->



<div style="display:none">
<form name="frm_pagehist" action="<?=$fname?>?session=<?=$session?>" target="iframe_pagehist" method="get">
<input type="hidden" name="pagehist_number" />
<input type="hidden" name="zz_pagehist" value="1" />
<input type="hidden" name="a" value="" />
<input type="hidden" name="c" value="" />
<input type="hidden" name="f" value="" />
<input type="hidden" name="o" value="" />
<input type="hidden" name="class_id" value="" />
<input type="hidden" name="atrb_id" value="" />
</form>

<iframe id="iframe_pagehist" name="iframe_pagehist" src="img/spacer.gif"></iframe>
</div>
</body>
<?php pg_close($con); ?>
</html>












<?php

//**********************************************************************************************************************
// 文書一覧を出力
//**********************************************************************************************************************
function show_document_list($a, $c, $f, $o, $session, &$con, $fname, $emp_id, $available_archives, $class_id, $atrb_id) {
    ob_clean();
    global $IS_ADMIN_MENU;
    global $IS_LOGIN_TREE;
    global $IS_MYPAGE;
    global $IS_OWNERS;
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2"<?=(!$IS_MYPAGE?' class="list"':'') ?> id="main_list">
<tr height="22" bgcolor="#f6f9ff">

<?php
//=======================
// 削除ヘッダ
//=======================
?>
<?php if (!$IS_LOGIN_TREE && !$IS_MYPAGE) { ?>
<td align="center" width="40" class="j12" style="<?=($IS_MYPAGE?';border-bottom:1px solid #5279a5':'') ?>">削除</td>
<?php } ?>

<?php
//=======================
// 番号ヘッダ
//=======================
?>
<?php if (!$IS_MYPAGE) { ?>
<td style="width:100px<?=($IS_MYPAGE?';border-bottom:1px solid #5279a5':'') ?>" class="j12">
<?php $img_option = 'alt="" width="17" height="17" border="0" style="vertical-align:middle;"'; ?>
<?php $onclick = "loadDocument('document', currentA, currentC, currentF, ".($o=="7"?"8":"7").")"; ?>
<?php echo('<a href="javascript:void(0)" onclick="'.$onclick.'"><img src="img/'.($o=="7"?"up":"down").'.gif" '.$img_option.'>番号</a>'); ?>
</td>
<?php } ?>

<?php
//=======================
// 文書名ヘッダ
//=======================
?>
<td class="j12" style="<?=($IS_MYPAGE?';border-bottom:1px solid #5279a5':'') ?>">
<?php $onclick = "loadDocument('document', currentA, currentC, currentF, ".($o=="1"?"2":"1").")"; ?>
<?php echo('<a href="javascript:void(0)" onclick="'.$onclick.'"><img src="img/'.($o=="1"?"up":"down").'.gif" '.$img_option.'>文書名</a>'); ?>
</td>

<?php
//=======================
// 更新日ヘッダ
//=======================
?>
<td style="width:80px<?=($IS_MYPAGE?';border-bottom:1px solid #5279a5':'') ?>" class="j12">
<?php $onclick = "loadDocument('document', currentA, currentC, currentF, ".($o=="3"?"4":"3").")"; ?>
<?php echo('<a href="javascript:void(0)" onclick="'.$onclick.'"><img src="img/'.($o=="3"?"up":"down").'.gif" '.$img_option.'>更新日</a>'); ?>
</td>

<?php
//=======================
// サイズヘッダ
//=======================
?>
<td style="width:70px<?=($IS_MYPAGE?';border-bottom:1px solid #5279a5':'') ?>" class="j12">
<?php $onclick = "loadDocument('document', currentA, currentC, currentF, ".($o=="5"?"6":"5").")"; ?>
<?php echo('<a href="javascript:void(0)" onclick="'.$onclick.'"><img src="img/'.($o=="5"?"up":"down").'.gif" '.$img_option.'>サイズ</a>'); ?>
</td>

<?php
//=======================
// 権限ヘッダ
//=======================
?>
<?php if (!$IS_LOGIN_TREE) { ?>
<td align="center" style="width:36px<?=($IS_MYPAGE?';border-bottom:1px solid #5279a5':'') ?>" class="j12">権限</td>
<?php } ?>

</tr>


<?php
//===================================
// 右側一覧
//===================================

    //--------------------------------------------
    // フォルダが指定された場合は、それがリンクかどうかを確認する。
    // リンクだった場合、リンクが指し示す実態とする。
    //--------------------------------------------
    $link_id = "";
    $raw_a = $a;
    $raw_c = $c;
    $raw_f = $f;
    if ($c && $f) {
        $link_id = lib_get_one($con, $fname, "select link_id from libfolder where folder_id = ".$f);
        if ($link_id) {
            $f = $link_id;
            $link_row = lib_get_top_row($con, $fname, "select * from libfolder where folder_id = ".$f);
            if ($link_row) {
                $a = $link_row["lib_archive"];
                $c = $link_row["lib_cate_id"];
            }
        }
    }

    $lock_show_flg = lib_get_lock_show_flg($con, $fname, $clib);
    $lock_show_all_flg = lib_get_lock_show_all_flg($con, $fname);
    $category_list = array();
    $libfolders = array();
    lib_cre_tmp_concurrent_table($con, $fname, $emp_id);

    $docs = array();
    $self_row = 0;

    //==============================================================================================
    // フォルダ未指定なら、カテゴリ一覧が必要
    //==============================================================================================
    if (!$f) {
        $opt = array("is_admin"=>$IS_ADMIN_MENU, "is_login"=>$IS_LOGIN_TREE, "is_mypage"=>$IS_MYPAGE);
        if ($IS_FOLDER_SELECT) $opt["is_ignore_class_atrb"] = 1;
        $sql = lib_get_libcate_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $a, "", $class_id, $atrb_id, $opt);
        $sel = select_from_table($con, $sql, "", $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql);
        while ($row = pg_fetch_assoc($sel)) {
            if ($c && $c==$row["lib_cate_id"]) $self_row = $row; // 選択中カテゴリ行
            $category_list[$row["lib_cate_id"]] = $row;
        }
    }

    //==============================================================================================
    // カテゴリ未指定なら、右リストにカテゴリ一覧を出力する。共有フォルダ選択状態。
    //==============================================================================================
    if ($c == "") {
        //-------------------------------------
        // ワークフローの保存先として使われているか確認したい
        //-------------------------------------
        $exist_workflow_list = array();
        if (!$IS_LOGIN_TREE && !$IS_MYPAGE && count($category_list)) {
            $sql =
            " select lib_cate_id from wkfwmst".
            " where lib_archive = '".$a."'".
            " and wkfw_del_flg = 'f'";
            $sel = select_from_table($con, $sql, "", $fname);
            if ($sel == 0) libcom_goto_error_page_and_die($sql);
            while ($row = pg_fetch_assoc($sel)) $exist_workflow_list[$row["lib_cate_id"]] = 1;
        }

        //-------------------------------------
        // リスト化
        //-------------------------------------
        foreach ($category_list as $row) {
            // ソート用に組み直しが必要
            $_cate_id = $row["lib_cate_id"];

            $folder_exist = "";
            $doc_exist = "";
            if (!$IS_LOGIN_TREE && !$IS_MYPAGE) {
                // 表示するカテゴリに、フォルダが存在するか確認したい。
                $sql = " select lib_cate_id from libfolder where lib_archive = '".$a."' and lib_cate_id = ".$_cate_id." limit 1";
                $folder_exist = lib_get_one($con, $fname, $sql);

                // 表示するカテゴリに、ファイルが存在するか確認したい。
                $sql =
                " select lib_cate_id from libinfo".
                " where lib_archive = '".$a."' and lib_delete_flag = 'f' and lib_cate_id = ".$_cate_id." limit 1";
                $doc_exist = lib_get_one($con, $fname, $sql);
            }

            $docs[] = array(
                "id" => $row["lib_cate_id"],
                "lib_cate_id" => $row["lib_cate_id"],
                "archive_id" => $row["lib_archive"],
                "cate_id" => $row["id"],
                "name" => $row["lib_cate_nm"],
                "no" => $row["lib_cate_no"],
                "type" => "category",
                "file_type" => "フォルダ",
                "size" => 0,
                "modify_flg" => ($row["writable"] ? true : false),
                "folder_exist" =>  ($folder_exist ? 1 : 0),
                "doc_exist" => ($doc_exist ? 1 : 0),
                "path" => "",
                "modified" => $row["upd_time"],
                "is_wkfw" => ($exist_workflow_list[$_cate_id] ? 1 : "")
            );
        }
    //==============================================================================================
    // カテゴリあるいはフォルダ指定があれば、子フォルダ内容を出力する
    //==============================================================================================
    } else {
        // フォルダの一覧、自分を含める
        $opt = array("is_admin"=>$IS_ADMIN_MENU, "is_login"=>$IS_LOGIN_TREE, "is_narrow_mode"=>1, "with_child"=>1, "check_hilink"=>1);
        $sql = lib_get_libfolder_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $a, $c, $f, $opt);
        $sel = select_from_table($con, $sql, "", $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql);
        while ($row = pg_fetch_assoc($sel)) {
            if ($row["folder_id"]==$f) $self_row = $row; // 自分はここで除外
            else $libfolders[$row["folder_id"]] = $row;
        }

        //-------------------------------------
        // ワークフローの保存先として使われているか確認したい
        //-------------------------------------
        $exist_workflow_list = array();
        if (!$IS_LOGIN_TREE && !$IS_MYPAGE && count($libfolders)) {
            $sql =
            " select lib_folder_id from wkfwmst".
            " where lib_archive = '".$row["archive_id"]."'".
            " and lib_folder_id in (".implode(",", array_keys($libfolders)).")".
            " and wkfw_del_flg = 'f'";
            $sel = select_from_table($con, $sql, "", $fname);
            if ($sel == 0) libcom_goto_error_page_and_die($sql);
            while ($row = pg_fetch_assoc($sel)) $exist_workflow_list[$row["lib_folder_id"]] = 1;
        }

        foreach ($libfolders as $row) {
            // リンク先の権限がなければ表示しない
            if ($row["link_id"]) {
                $link_row = lib_get_top_row($con, $fname, "select * from libfolder where folder_id = ".$row["link_id"]);
                if (!$link_row) continue;
                $ra = $link_row["lib_archive"];
                $rc = $link_row["lib_cate_id"];
                $rf = $link_row["folder_id"];
                if (!$ra) continue;
                $opt = array("is_admin"=>$IS_ADMIN_MENU, "is_login"=>$IS_LOGIN_TREE, "is_narrow_mode"=>1, "top_only"=>1);
                $sql = lib_get_libfolder_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $ra, $rc, $rf, $opt);
                $real_row = lib_get_top_row($con, $fname, $sql);
                if (!$real_row["folder_id"]) continue;
            }
            $_f = $row["folder_id"];

            $folder_exist = "";
            $doc_exist = "";
            if (!$IS_LOGIN_TREE && !$IS_MYPAGE) {
                // 表示するフォルダに、フォルダが存在するか確認したい。
                $sql = "select child_id from libtree where parent_id = ".$_f;
                $folder_exist = lib_get_one($con, $fname, $sql);

                // 表示するフォルダに、ファイルが存在するか確認したい。
                //XX $sql = "select lib_cate_id from libinfo where folder_id = '".$_f."' and lib_delete_flag = 'f' limit 1";

                // 2015-12-09 最新の版番号であるlib_idで確認する。
                $sql = "select lib_id from libinfo
                        where folder_id = {$_f} and lib_delete_flag = 'f' and
                        lib_id in (
                            select lib_id from libedition le_a
                            where not exists (
                                select base_lib_id from libedition le_b
                                where
                                    le_a.base_lib_id = le_b.base_lib_id
                                and le_a.edition_no < le_b.edition_no
                            )
                        ) limit 1";
                $doc_exist = lib_get_one($con, $fname, $sql);
            }

            $docs[] = array(
                "id" => $_f,
                "archive_id"     => $row["lib_archive"],
                "cate_id"        => $row["lib_cate_id"],
                "name"           => $row["folder_name"],
                "no"             => $row["folder_no"],
                "type"           => "folder",
                "file_type"      => "フォルダ",
                "size"           => 0,
                "modify_flg"     => $row["writable"],
                "folder_exist"   => ($folder_exist ? 1 : 0),// 直下の子フォルダ存在確認
                "doc_exist"      => ($doc_exist ? 1 : 0), // 文書が直下に存在。（直下のみ確認する。子孫まで再帰的に確認しない）
                "path"           => "",
                "modified"       => $row["upd_time"],
                "link_id"        => $row["link_id"],
                "is_wkfw"        => ($exist_workflow_list[$_f] ? 1 : ""),
                "is_link_target" => ($row["is_hilink"] ? 1 : "")
            );
        }
        // 文書一覧情報
        $opt = array("is_admin"=>$IS_ADMIN_MENU, "is_login"=>$IS_LOGIN_TREE);
        $sql = lib_get_libinfo_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $a, $c, $f, 0, $opt);
        $sel = select_from_table($con, $sql, "", $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql);
        while ($row = pg_fetch_assoc($sel)) {
            if (!lib_check_edition($con, $fname, $row["lib_id"])) continue;
            $tmp_path = get_document_path($row["lib_archive"], $row["lib_cate_id"], $row["lib_id"], $row["lib_extension"]);
            $docs[]= array(
                "lib_id"    => $row["lib_id"],
                "id"        => $row["lib_id"],
                "name"      => $row["lib_nm"],
                "no"        => $row["lib_no"],
                "type"      => "document",
                "file_type" => lib_get_file_type_name($row["lib_type"]),
                "size"      => @filesize($tmp_path),
                "modify_flg"=> ($row["writable"] ? true : false), // boolean
                "path"      => $tmp_path,
                "modified"  => $row["lib_up_date"],
                "cate_id"   => $row["lib_cate_id"],
                "folder_id" => $row["folder_id"]
            );
        }
    }

    // ドラッグ＆ドロップ使用フラグを取得
    $dragdrop_flg = lib_get_dragdrop_flg($con, $fname, $clib);
    if ($IS_MYPAGE) $dragdrop_flg = false;
    if ($IS_LOGIN_TREE) $dragdrop_flg = false;
    //---------------------------------
    // 画面指定フォルダを更新できるかの判断。各種ボタン状態を決める
    //---------------------------------
    $btnDelete = ""; // 削除ボタン押下不可
    if ($self_row["writable"]) $btnDelete = "1"; // 更新可能であれば削除も可能
    if (!$self_row) $btnDelete = ($a=="3" || $a=="4" ? "" : "1"); // 全体共有と個人は、カテゴリも削除可能、部署・委員会のトップなら削除不可
    if (!count($docs)) $btnDelete = ""; // 子がいなければ削除チェックボックスが存在しない。削除不可
    if (!$IS_ADMIN_MENU && $link_id) $btnDelete = ""; // リンクは管理者のみ削除可能
    if ($IS_OWNERS && $a=="2") $btnDelete = "";

    $all_createfolder_flg = lib_get_all_createfolder_flg($con, $fname, $clib); // 全体共有の最上位でフォルダの作成を許可するフラグを取得
    $btnMakeFolder = ""; // フォルダ作成可能かどうか
    if ($self_row["writable"]) $btnMakeFolder = "1"; // 更新可能でリンクでない。作成可能
    if (!$self_row) $btnMakeFolder = ($a=="3" || $a=="4" ? "" : "1"); // 全体共有と個人は、カテゴリ作成可能、部署・委員会のトップなら作成不可
    if (!$IS_ADMIN_MENU && !$self_row && $a=="2" && $all_createfolder_flg=="f") $btnMakeFolder = ""; // 全体共有カテゴリが作成できない場合
    if ($link_id) $btnMakeFolder = ""; // リンクへはフォルダ作成できない
    if ($IS_OWNERS && $a=="2") $btnMakeFolder = "";
    if ($IS_OWNERS && $a=="3") $btnMakeFolder = "";

    $btnBunsyo = ""; // 文書登録可能かどうか
    if ($self_row["writable"]) $btnBunsyo = "1"; // 更新可能でリンクでない。登録可能
    if (!$self_row) $btnBunsyo = ""; // 共有直下には文書を作成できない
    if ($link_id) $btnBunsyo = ""; // リンクへは文書登録できない
    if ($IS_OWNERS && $a=="2") $btnBunsyo = "";

    $btnChangeName = ""; //名前変更できるかどうか。self_rowが無い場合は共有を示す。共有は名前変更できない。部署委員会のトップでも名称変更はできないが押せる。
    if ($self_row["writable"]) $btnChangeName = "1"; // カテゴリまたはフォルダが更新可能なら名称変更可能
    if (!$IS_ADMIN_MENU && $link_id) $btnChangeName = ""; // リンクは管理者のみ名称変更可能
    if ($IS_OWNERS && $a=="2") $btnChangeName = "";
    if ($IS_OWNERS && $a=="3") $btnChangeName = "";

    if ($o=="1") usort($docs, "sort_docs_by_name"); // 名前の昇順
    if ($o=="2") usort($docs, "sort_docs_by_name_desc"); // 名前の降順
    if ($o=="3") usort($docs, "sort_docs_by_modified"); // 更新日時の昇順
    if ($o=="4") usort($docs, "sort_docs_by_modified_desc"); // 更新日時の降順
    if ($o=="5") usort($docs, "sort_docs_by_size"); // サイズの昇順
    if ($o=="6") usort($docs, "sort_docs_by_size_desc"); // サイズの降順
    if ($o=="7") usort($docs, "sort_docs_by_no"); // 番号の昇順
    if ($o=="8") usort($docs, "sort_docs_by_no_desc"); // 番号の降順

    $filename_flg = lib_get_filename_flg();
    foreach ($docs as $tmp_doc) {
        $_did = $tmp_doc["id"];
        $_type = $tmp_doc["type"];
        $_link_id = (int)$tmp_doc["link_id"];
        $_file_type = $tmp_doc["file_type"];
        $_modify_flg = ($tmp_doc["modify_flg"]) ? 1 : 0;
        if ($IS_ADMIN_MENU) $_modify_flg = 1; // 管理画面なら常に編集可能。
        if ($IS_LOGIN_TREE) $_modify_flg = 0; // ログイン画面なら常に編集不可。

        $is_disabled = "";
        if ($IS_OWNERS) {
            if ($a=="2") $is_disabled = 1;
            if ($a=="3" && ($_type=="folder" || $_type=="category")) $is_disabled = 1;
            if ($a=="3" && $_type=="document") {
                $sql =
                " select li.lib_id from libinfo li".
                " inner join empmst em on (em.emp_class = 1 and em.emp_id = li.emp_id)".
                " where li.lib_id = ".$_did." limit 1";
                if ((int)lib_get_one($con, $fname, $sql)) $is_disabled = "1";
            }
        }

        //-------------------------------
        // 行描画開始
        //-------------------------------
        echo '<tr height="22" class="tr_document_row">';

        //-------------------------------
        // 削除チェックボックス
        // 個人、全体は削除可（部署、委員会は削除不可）
        // 文書が存在する場合も、削除不可
        //-------------------------------
        if (!$IS_LOGIN_TREE && !$IS_MYPAGE) {
            if ($is_disabled) {
                echo '<td align="center">-</td>';
            } else {
                $_del_reason = array();
                if ($_type == "category" || $_type == "folder") {
                    if (!$_link_id && $tmp_doc["doc_exist"])    $_del_reason[] = "文書が存在します。削除できません。";
                    if (!$_link_id && $tmp_doc["folder_exist"]) $_del_reason[] = "配下フォルダが存在します。削除できません。";
                    if (!$_link_id && $tmp_doc["is_wkfw"])      $_del_reason[] = "ワークフローの保存先に指定されています。削除できません。";
                    if (!$_modify_flg)                          $_del_reason[]= "更新不可のフォルダは削除できません。";
                    if ($tmp_doc["is_link_target"])             $_del_reason[]= "リンクされています。削除できません。";
                    if ($_type=="category") {
                        if ($a=="3") $_del_reason[]= "最上位フォルダはマスターメンテナンス機能で管理されています。削除できません。";
                        if ($a=="4") $_del_reason[]= "最上位フォルダは委員会・WG機能で管理されています。削除できません。";
                    }
                }
                else {
                    if (!$_modify_flg) $_del_reason[]= "更新不可のフォルダは削除できません。";
                }

                $cbox = "";
                if (!count($_del_reason)) {
                    $cbox = "<input type=\"checkbox\" name=\"did[]\" value=\"$_did\">";
                    if ($_type == "category") $cbox = "<input type=\"checkbox\" name=\"cid[]\" value=\"$_did\">";
                    if ($_type == "folder") $cbox = "<input type=\"checkbox\" name=\"fid[]\" value=\"$_did\">"; // リンクでないフォルダなら
                } else {
                    $cbox = '<input type="checkbox" title="'.implode("\n", $_del_reason).'" disabled>';
                }
                echo '<td align="center"><label>'.$cbox.'</label></td>';
            }
        }

        //-------------------------------
        // 番号
        //-------------------------------
        if (!$IS_MYPAGE) {
        echo '<td class="j12">'.$tmp_doc["no"].'</td>';
        }

        //-------------------------------
        // 文書名
        //-------------------------------

        // アイコン
        $img = "folder.gif";
        if ($_type != "category" && $_type != "folder") {
            $img = "other.jpg";
            if ($_file_type=="Word")       $img = "word.jpg";
            if ($_file_type=="Excel")      $img = "excel.jpg";
            if ($_file_type=="PowerPoint") $img = "powerpoint.jpg";
            if ($_file_type=="PDF")        $img = "pdf.jpg";
            if ($_file_type=="テキスト")   $img = "text.jpg";
            if ($_file_type=="JPEG")       $img = "jpeg.jpg";
            if ($_file_type=="GIF")        $img = "gif.jpg";
        }

        // 名前のリンク先
        $href = 'javascript:void(0)';
        $onclick = "";
        $target = "";
        $_aid = $tmp_doc["archive_id"];
        if ($_type == "category") {
            $onclick = " onclick=\"pickupNode('$_did', '', '".$tmp_doc["archive_id"]."'); return false;\"";
	        if ($IS_LOGIN_TREE) $href = 'library_tree_from_login.php?a='.$tmp_doc["archive_id"].'&c='.$_did;
	        else if (!$IS_MYPAGE) $href = 'library_list_all.php?session='.$session.'&a='.$tmp_doc["archive_id"].'&c='.$_did;
        } else if ($_type == "folder") {
            $onclick = " onclick=\"pickupNode('".$tmp_doc["cate_id"]."', '$_did', '".$tmp_doc["archive_id"]."'); return false;\"";
	        if ($IS_LOGIN_TREE) $href = 'library_tree_from_login.php?a='.$tmp_doc["archive_id"].'&c='.$tmp_doc["cate_id"]."&f=".$_did;
	        else if (!$IS_MYPAGE) $href = 'library_list_all.php?session='.$session.'&a='.$tmp_doc["archive_id"].'&c='.$tmp_doc["cate_id"]."&f=".$_did;
        } else {
            $href = 'library_refer.php?s='.$session.'&i='.$_did.'&u='.urlencode($tmp_doc["path"]);
            $target = ' target="_blank"';
        }

        echo '<td style="padding:0;" class="j12">';
        echo '  <div id="doc'.$_did.'" class="with_icon rightside_icon" content_type="'.$_type.'" content_value="'.$_did.'"';
        echo '    drag_type="'.($_modify_flg ? "movable_doc" : "is_readonly_doc").'"';
        echo '    is_wkfw="'.$tmp_doc["is_wkfw"].'"';
        echo '    link_id="'.$tmp_doc["link_id"].'" c="'.($c=="" ? "rootfolder" :$c).'" f="'.$f.'"';
        echo '    style="background-image:url(img/icon/'.$img.');'.($dragdrop_flg=="t" ? ' cursor:move;':'').'">';
        echo '    <div'.($_type == "folder" && $_link_id ? ' class="is_link_folder" title="リンクフォルダ"' : "").'>';
        echo '      <a '.($_modify_flg || $IS_LOGIN_TREE ? '' : ' class="lock_icon"').' href="'.$href.'" '.$onclick.$target.'>'.$tmp_doc["name"].'</a>';
        echo '    </div>';
        echo '  </div>';
        echo '</td>';

        //-------------------------------
        // 更新日
        //-------------------------------
        $_modified_str = "-";
        if (strlen($tmp_doc["modified"]) >= 8) {
            $_modified_str = preg_replace("/^(\d{4})(\d{2})(\d{2})(.*)$/", "$1/$2/$3", $tmp_doc["modified"]);
        }
        echo '<td align="center" class="j12">'.$_modified_str.'</td>';

        //-------------------------------
        // サイズ
        //-------------------------------
        echo '<td class="j12">'.format_size($tmp_doc["size"]).'</td>';

        //-------------------------------
        // 権限
        //-------------------------------
        if (!$IS_LOGIN_TREE) {
            if ($is_disabled) {
                echo '<td align="center" class="j12">-</td>';
            } else if (!$IS_ADMIN_MENU && ($_type == "folder" && $_link_id)) { // ユーザ画面で、リンクなら押せない
                echo '<td align="center" class="j12">-</td>';
            }
            else {
                $href = '';
                $path = ($_REQUEST["path"]?"&path=".$_REQUEST["path"]:"");
                $type = ($_REQUEST["type"]?"&type=".$_REQUEST["type"]:"");
                if ($_type == "category") {
                    $href = "library_folder_update.php?session=$session&arch_id=$a&cate_id=$_did&folder_id=&o=$o$path$type";
                } else if ($_type == "folder") {
                    $href = "library_folder_update.php?session=$session&arch_id=$a&cate_id=$c&folder_id=$_did&o=$o$path$type";
                } else {
                    $href = "library_detail.php?session=$session&a=$a&c=$c&f=$f&o=$o&lib_id=$_did&listtype=1$path$type";
                }
                echo '<td align="center" class="j12"><a href="'.$href.'"'.($IS_MYPAGE?' target="_parent"':'').'>'.($_modify_flg ? "更新" : "参照").'</td>';
            }
        }

        echo '</tr>';
    }
    echo '</table>';
    if (!count($docs)) {
        echo '<div style="padding:20px 0 0 10px" class="j12">参照できる文書／フォルダが存在しないか、登録されていません。</div>';
    }
    $_name = ($c ? lib_get_category_name($con, $c, $fname) : "");
    $_fpath_list = ($a && $f ? lib_get_folder_path($con, $f, $fname, $a, $clib) : array());
?>
<input type="hidden" id="available_archives" value="<?=implode(",",$available_archives)?>" />
<input type="hidden" id="hidden_btnToUpNode_Enabled" value="<?=$c ? 1:''?>"><?php // カテゴリがあれば上階層へ行ける ?>
<input type="hidden" id="hidden_btnMakeFolder_Enabled" value="<?=$btnMakeFolder?>">
<input type="hidden" id="hidden_btnBunsyo_Enabled" value="<?=$btnBunsyo?>">
<input type="hidden" id="hidden_btnChangeName_Enabled" value="<?=$btnChangeName?>">
<input type="hidden" id="hidden_btnDelete_Enabled" value="<?=$btnDelete?>">
<script type="text/javascript">
    var nLink = [];
    nLink.push(' &gt; <a href="" onclick="pickupNode(0, 0, \'<?=$a?>\'); return false;"><b>文書一覧</b></a>');
    <?php if (count($available_archives) > 1) { ?>
    <?php     $archive_name = lib_get_archive_name($a); ?>
           nLink.push(' &gt; <a href="library_list_all.php?session=<?=$session?>&a=<?=$a?>" onclick="pickupNode(0, 0, \'<?=$a?>\'); return false;"><b><?=$archive_name?></b></a>');
    <?php } ?>
    <?php if ($c) { ?>
           nLink.push(' &gt; <a href="library_list_all.php?session=<?=$session?>&a=<?=$a?>&c=<?=$c?>" onclick="pickupNode(\'<?=$c?>\', 0, \'<?=$a?>\'); return false;"><b><?=$_name?></b></a>');
    <?php     if (is_array($_fpath_list)) { ?>
    <?php         foreach ($_fpath_list as $_fpath) { ?>
                   nLink.push(' &gt; <a href="library_list_all.php?session=<?=$session?>&a=<?=$a?>&c=<?=$c?>&f=<?=$_fpath["id"]?>" onclick="pickupNode(\'<?=$c?>\', \'<?=$_fpath["id"]?>\', \'<?=$a?>\'); return false;"><b><?=$_fpath["name"]?></b></a>');
    <?php         } ?>
    <?php     } ?>
    <?php } ?>
    <?php if (!$IS_LOGIN_TREE && !$IS_MYPAGE) { ?>
    if (ee("navigation_container")) ee("navigation_container").innerHTML = nLink.join("");
    <?php } ?>
    <?php
    // リンクフォルダならa,c,fを元のフォルダのものに変更
        $a_html ="";
        if ($link_id && (!$a || $raw_a!=$a)) {
            if ($a) $a_html = '<span class="s1"><span class="s2">【'.get_lib_archive_label2($a).'書庫内】</span></span>';
            else $a_html = '<span class="s1"><span class="s2">【リンク先不明】</span></span>';
        }
    ?>
    ee("next_archive_label").innerHTML = '<?=$a_html?>';
    <?php if ($dragdrop_flg == "t") { ?>
    var isIE6 = (typeof document.documentElement.style.maxHeight != "undefined" ? false : true);
    $("table#main_list div.with_icon").draggable({
        revert:true,
        appendTo:"#drag_container",
        scroll: true,
        helper: (isIE6 ? 'original' : 'clone'),
        revertDuration:0,
        refreshPositions:true,
        start:function(){
            dragDiv = this;
            dragInfo = dragCheck(this);
            $(this).css({backgroundColor:dragInfo.bgColor, zIndex:1000});
            this.setAttribute("title", dragInfo.errmsg);
            prepareDroppable();
            ee("left_tree_container").focus();
        },
        stop:function() {
            $(this).css({backgroundColor:"", zIndex:0});
        },
        cancel:"a"
    });
    <?php } ?>
</script>
<?php
}