<?
require_once("about_comedix.php");
require_once("kango_common.ini");
require_once("holiday.php");

//==============================
//前処理
//==============================

// ページ名
$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, $AUTH_NO_KANGO_USER, $fname);
if($checkauth == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// パラメータ
$is_postback = isset($_REQUEST["is_postback"]) ? $_REQUEST["is_postback"] : null;
$time = isset($_REQUEST["time"]) ? $_REQUEST["time"] : null;
$pt_id = isset($_REQUEST["pt_id"]) ? $_REQUEST["pt_id"] : null;
$cal_y = isset($_REQUEST["cal_y"]) ? $_REQUEST["cal_y"] : null;
$cal_m = isset($_REQUEST["cal_m"]) ? $_REQUEST["cal_m"] : null;
$input_date = isset($_REQUEST["input_date"]) ? $_REQUEST["input_date"] : null;
$input_time = isset($_REQUEST["input_time"]) ? $_REQUEST["input_time"] : null;
$renzoku_copy = isset($_REQUEST["renzoku_copy"]) ? $_REQUEST["renzoku_copy"] : null;

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//ユーザーID
//==============================
$emp_id = get_emp_id($con,$session,$fname);


//==============================
//現在の患者の入棟情報
//==============================
$pt_all_inpt_info =  get_all_inpt_info($con,$fname,"","",$pt_id);

//==============================
//ポストバック時の処理
//==============================
if($is_postback == "true")
{
	//==============================
	//保存処理
	//==============================
	if($postback_mode == "save")
	{
		//クリア以外の場合
		if($input_state != 0)
		{
			//==============================
			//パラメータ解析
			//==============================
			$input_value = null;
			$i = -1;
			while(true)
			{
				$i++;
				$param_name = "input{$i}";
				$input_param = isset($$param_name) ? $$param_name : null;
				if(empty($input_param))
				{
					break;
				}

				$input_value1 = null;
				$input_param_list = explode("_",$input_param);
				$input_value1['eval_form_id'] = $input_param_list[0];
				$input_value1['eval_item_id'] = $input_param_list[1];
				$input_value1['eval_cate_no'] = $input_param_list[2];
				$input_value[] = $input_value1;
			}

			//==============================
			//トランザクション開始
			//==============================
			pg_query($con,"begin transaction");

			//==============================
			//評価記録保存
			//==============================
			save_eval_record($con,$fname,$pt_id,$input_date,$input_time,$bldg_cd,$ward_cd,$ptrm_room_no,$inpt_bed_no,$emp_id,$input_state,$input_value);

			//==============================
			//コミット
			//==============================
			pg_query($con, "commit");
		}
		//クリアの場合
		else
		{
			//==============================
			//トランザクション開始
			//==============================
			pg_query($con,"begin transaction");

			//==============================
			//評価記録論理削除
			//==============================
			delete_eval_record($con,$fname,$pt_id,$input_date,$input_time);

			//==============================
			//コミット
			//==============================
			pg_query($con, "commit");
		}
	}
	//==============================
	//コピー処理
	//==============================
	elseif($postback_mode == "copy_prev" || $postback_mode == "copy_next")
	{
		//==============================
		//トランザクション開始
		//==============================
		pg_query($con,"begin transaction");

		//==============================
		//コピー元データの入力日付取得
		//==============================
		if($postback_mode == "copy_prev")
		{
			$sql = "select max(input_date) as input_date from nlcs_eval_record where pt_id = '{$pt_id}' and input_date < '{$input_date}' and input_time = {$input_time} and not delete_flg";
		}
		else
		{
			$sql = "select min(input_date) as input_date from nlcs_eval_record where pt_id = '{$pt_id}' and input_date > '{$input_date}' and input_time = {$input_time} and not delete_flg";
		}
		$sel = select_from_table($con, $sql, "", $fname);
		if ($sel == 0)
		{
			pg_query($con,"rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$copy_target_input_date = pg_fetch_result($sel,0,'input_date');

		//==============================
		//コピー元がない場合
		//==============================
		if($copy_target_input_date == "")
		{
			//==============================
			//コピー元がないことを通知
			//==============================
			?>
			<script type="text/javascript">
			alert("現在の時間帯におけるコピー元データがありません。");
			</script>
			<?

			//==============================
			//トランザクション終了
			//==============================
			pg_query($con,"rollback");
		}
		//==============================
		//コピー元がある場合
		//==============================
		else
		{
			//==============================
			//コピー元データの入力値を取得
			//==============================
			$sql = "select input_state,eval_data_id from nlcs_eval_record where pt_id = '{$pt_id}' and input_date = '{$copy_target_input_date}' and input_time = {$input_time} and not delete_flg";
			$sel = select_from_table($con, $sql, "", $fname);
			if ($sel == 0)
			{
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$input_state = pg_fetch_result($sel,0,'input_state');
			$copy_target_eval_data_id = pg_fetch_result($sel,0,'eval_data_id');

			$sql = "select eval_form_id,eval_item_id,eval_cate_no from nlcs_eval_record_data where eval_data_id = {$copy_target_eval_data_id}";
			$sel = select_from_table($con, $sql, "", $fname);
			if ($sel == 0)
			{
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$input_value = pg_fetch_all($sel);

			//==============================
			//単体コピーの場合
			//==============================
			if($renzoku_copy != "t")
			{
				//==============================
				//評価記録保存
				//==============================
				save_eval_record($con,$fname,$pt_id,$input_date,$input_time,$bldg_cd,$ward_cd,$ptrm_room_no,$inpt_bed_no,$emp_id,$input_state,$input_value);
			}
			//==============================
			//連続コピーの場合
			//==============================
			else
			{
				$copy_target_input_date_y = substr($copy_target_input_date,0,4);
				$copy_target_input_date_m = substr($copy_target_input_date,4,2);
				$copy_target_input_date_d = substr($copy_target_input_date,6,2);

				$i = 0;
				while(true)
				{
					if($postback_mode == "copy_prev")
					{
						$i++;
					}
					else
					{
						$i--;
					}
					$tmp_datetime = mktime(0, 0, 0, $copy_target_input_date_m, $copy_target_input_date_d + $i, $copy_target_input_date_y);
					$tmp_date = date("Ymd",$tmp_datetime);
					if($postback_mode == "copy_prev")
					{
						if($input_date < $tmp_date)
						{
							break;
						}
					}
					else
					{
						if($input_date > $tmp_date)
						{
							break;
						}
					}

					//==============================
					//評価記録保存
					//==============================
					save_eval_record($con,$fname,$pt_id,$tmp_date,$input_time,$bldg_cd,$ward_cd,$ptrm_room_no,$inpt_bed_no,$emp_id,$input_state,$input_value);


				}
			}

			//==============================
			//コミット
			//==============================
			pg_query($con, "commit");
		}

	}
}



//==============================
//対象:日時情報の取得
//==============================

//初期アクセス時　※他画面から指定されて呼び出されるケースあり。
if($input_date == "")
{
	$input_date = date("Ymd");
}
if($input_time == "")
{
	$input_time = get_now_time();
}

$input_date_y = substr($input_date,0,4);
$input_date_m = substr($input_date,4,2);
$input_date_d = substr($input_date,6,2);


//時間帯に対する時刻
$time_info = get_time_info($time);
$input_time_st_hhss = $time_info['st_hhss'];
$input_time_ed_hhss = $time_info['ed_hhss'];


//==============================
//対象:患者情報の取得
//==============================
$sql = "select * from ptifmst where ptif_id = '{$pt_id}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if(pg_num_rows($sel) == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//氏名
$pt_name = pg_fetch_result($sel,0,'ptif_lt_kaj_nm').' '.pg_fetch_result($sel,0,'ptif_ft_kaj_nm');

//性別
$pt_sex = get_sex_name(pg_fetch_result($sel,0,'ptif_sex'));


//==============================
//対象:場所情報の取得(入院中判定/入院患者氏名)
//==============================



//初期アクセス時
if($is_postback != "true")
{
	//「患者」「日時」を指定して入棟患者を検索(「場所」は指定しない。必ずどこかに入院している。)
	$wk_data = null;
	foreach($pt_all_inpt_info as $all_inpt_info1)
	{
		if(
			   $all_inpt_info1['inpt_in_dt']  <= $input_date
			&& $all_inpt_info1['inpt_out_dt']  >= $input_date
			)
		{
			$wk_data[] = $all_inpt_info1;
		}
	}

	//「病棟レイアウト」から遷移した場合、除外患者が指定されるケースがある。
	//この場合、「患者」「日時」の条件でヒットしない。
	if($wk_data == null)
	{
		?>
		<script type="text/javascript">
		alert("指定された患者は対象外に指定されている可能性があります。");
		history.back();
		</script>
		<?
		exit;
	}

	//初期アクセス時は「場所」条件を取得しておく。
	$bldg_cd = $wk_data[0]['bldg_cd'];
	$ward_cd = $wk_data[0]['ward_cd'];
	$ptrm_room_no = $wk_data[0]['ptrm_room_no'];
	$inpt_bed_no = $wk_data[0]['inpt_bed_no'];

	//初期アクセスの場合は必ず患者は存在する。
	$is_target_inpt = true;
}
//ポストバックの場合
else
{
	//「患者」「日時」「場所」を指定して入院患者を検索
	$wk_data = null;
	foreach($pt_all_inpt_info as $all_inpt_info1)
	{
		if(
			   $all_inpt_info1['inpt_in_dt']  <= $input_date
			&& $all_inpt_info1['inpt_out_dt']  >= $input_date
			&& $all_inpt_info1['bldg_cd']  >= $bldg_cd
			&& $all_inpt_info1['ward_cd']  >= $ward_cd
			&& $all_inpt_info1['ptrm_room_no']  >= $ptrm_room_no
			&& $all_inpt_info1['inpt_bed_no']  >= $inpt_bed_no
			)
		{
			$wk_data[] = $all_inpt_info1;
		}
	}

	//患者の存在判定
	if($wk_data != null)
	{
		$is_target_inpt = true;
	}
	else
	{
		$is_target_inpt = false;
	}

}

//==============================
//病室名の取得
//==============================

//病室名
$sql  = " select ptrm_name from";
$sql .= " (";
$sql .= get_bldg_ward_room_sql();
$sql .= " ) bldg_ward_room_sql";
$sql .= " where bldg_cd = {$bldg_cd}";
$sql .= " and ward_cd = {$ward_cd}";
$sql .= " and ptrm_room_no = {$ptrm_room_no}";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if(pg_num_rows($sel) == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ptrm_name = pg_fetch_result($sel,0,'ptrm_name');


//==============================
//前患者/次患者情報(患者の存在するベッド情報)
//==============================
//ルール：ベッド番号、病室、病棟の順位で次に存在する患者が対象。(別の棟には移動しない。)


//	//「日時」「場所(棟のみ)」条件での「患者」「場所」情報を「場所」順で検索
//	$wk_data = null;
//	foreach($all_inpt_info as $all_inpt_info1)
//	{
//		if(
//			   $all_inpt_info1['bldg_cd']  == $bldg_cd
//			&& $all_inpt_info1['inpt_in_dt']  <= $input_date
//			&& $all_inpt_info1['inpt_out_dt']  >= $input_date
//			)
//		{
//			$wk_data[] = $all_inpt_info1;
//		}
//	}
//	$wk_data = usort_pg_fetch_all($wk_data,array("ward_cd","ptrm_room_no","inpt_bed_no"));


//「日時」「場所(棟のみ)」条件での「患者」「場所」情報を「場所」順で検索
$all_inpt_info =  get_all_inpt_info($con,$fname,$input_date,$input_date);
$wk_data = null;
foreach($all_inpt_info as $all_inpt_info1)
{
	if(
		   $all_inpt_info1['bldg_cd']  == $bldg_cd
		)
	{
		$wk_data[] = $all_inpt_info1;
	}
}
$wk_data = usort_pg_fetch_all($wk_data,array("ward_cd","ptrm_room_no","inpt_bed_no"));




//前患者/次患者情報作成
$next_pt_info = null;
$prev_pt_info = null;
$tmp_target_pt_hit = false;;
foreach($wk_data as $wk_data1)
{
	//場所情報で前後判定(患者は日時によっては入院していない場合がある為、場所情報で判定)
	if(
	     $ward_cd > $wk_data1['ward_cd']
	  || $ward_cd == $wk_data1['ward_cd'] && $ptrm_room_no > $wk_data1['ptrm_room_no']
	  || $ward_cd == $wk_data1['ward_cd'] && $ptrm_room_no == $wk_data1['ptrm_room_no'] && $inpt_bed_no > $wk_data1['inpt_bed_no']
	)
	{
		$prev_pt_info = $wk_data1;
	}
	if(
	     $ward_cd < $wk_data1['ward_cd']
	  || $ward_cd == $wk_data1['ward_cd'] && $ptrm_room_no < $wk_data1['ptrm_room_no']
	  || $ward_cd == $wk_data1['ward_cd'] && $ptrm_room_no == $wk_data1['ptrm_room_no'] && $inpt_bed_no < $wk_data1['inpt_bed_no']
	)
	{
		$next_pt_info = $wk_data1;
		break;
	}
}


//==============================
//記録状態を取得
//==============================

$sql = "select input_state from nlcs_eval_record where pt_id = '{$pt_id}' and input_date = '{$input_date}' and input_time = {$input_time} and not delete_flg";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//0:未入力
if(pg_num_rows($sel) == 0)
{
	$input_state = 0;
}
//1:確定,2:保留,3:不在
else
{
	$input_state = pg_fetch_result($sel,0,'input_state');
}

//==============================
//評価票情報を取得
//==============================



$form_info = get_used_form_info($con,$fname,$pt_id,$input_date,$input_time);


//==============================
//カレンダー情報
//==============================

//初期アクセス時　※他画面から指定されて呼び出されるケースあり。
if($cal_y == "")
{
	$cal_y = date("Y");
}
if($cal_m == "")
{
	$cal_m = date("m");
}

//カレンダーの月の患者の評価結果
$sql  = " select * from nlcs_eval_record";
$sql .= " where pt_id = '{$pt_id}'";
$sql .= " and input_date like '{$cal_y}{$cal_m}%'";
$sql .= " and not delete_flg";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$cal_eval_record_data = pg_fetch_all_in_array($sel);
$cal_eval_record = array();
foreach($cal_eval_record_data as $cal_eval_record_data1)
{
	$wk = null;
	$wk['input_state'] = $cal_eval_record_data1['input_state'];
	$wk['pt_class_level'] = $cal_eval_record_data1['pt_class_level'];
	$cal_eval_record[ $cal_eval_record_data1['input_date'] ][ $cal_eval_record_data1['input_time'] ] = $wk;
}


//「患者」「場所」を指定して入院患者を検索
$wk_data = array();
foreach($pt_all_inpt_info as $all_inpt_info1)
{
	if(
		   $all_inpt_info1['bldg_cd']  == $bldg_cd
		&& $all_inpt_info1['ward_cd']  == $ward_cd
		&& $all_inpt_info1['ptrm_room_no']  == $ptrm_room_no
		&& $all_inpt_info1['inpt_bed_no']  == $inpt_bed_no
		)
	{
		$wk_data[] = $all_inpt_info1;
	}
}
$cal_all_inpt_info = $wk_data;


//カレンダー情報作成
$cal_info = null;
$cal_d_max = 31;
$week_day_list = array('日','月','火','水','木','金','土');
for($cal_d=1;$cal_d<=$cal_d_max;$cal_d++)
{
	$tmp_date = mktime(0, 0, 0, $cal_m, $cal_d, $cal_y);

	//末日超え
	if(date("m", $tmp_date) !=  $cal_m)
	{
		$cal_d_max = $cal_d -1;
		break;
	}

	$tmp_date_info = null;
	$tmp_date_info['week_day'] = $week_day_list[date("w", $tmp_date)];
	$tmp_date_info['is_holiday'] = (get_holiday_name(date("Ymd", $tmp_date)) != "");

	//日付セルの色
	//今日
	if(date("Ymd", $tmp_date) == date("Ymd"))
	{
		$tmp_date_info['color'] = "#ccffcc";
	}
	//日祝
	elseif($tmp_date_info['is_holiday'] || $tmp_date_info['week_day'] == '日')
	{
		$tmp_date_info['color'] = "#fadede";
	}
	//土
	elseif($tmp_date_info['week_day'] == '土')
	{
		$tmp_date_info['color'] = "#defafa";
	}
	//平日
	else
	{
		$tmp_date_info['color'] = "#fefcdf";
	}

	//入棟中
	$cal_is_inpt_date = false;
	foreach($cal_all_inpt_info as $cal_all_inpt_info1)
	{
		if(
			   $cal_all_inpt_info1['inpt_in_dt'] <= date("Ymd", $tmp_date)
			&& $cal_all_inpt_info1['inpt_out_dt'] >= date("Ymd", $tmp_date)
			)
		{
			$cal_is_inpt_date = true;
			break;
		}
	}
	$tmp_date_info['is_inpt_date'] = $cal_is_inpt_date;


	$tmp_date_info['time'] = null;

	$tmp_time_info = null;
	for($tmp_time = 1;$tmp_time <= 3; $tmp_time++)
	{
		$tmp_time_info = null;

		//セルのリンク
		//未入棟の場合
		if(!$cal_is_inpt_date)
		{
			$tmp_time_info['has_link'] = false;
		}
		//未来の場合
		elseif(date("Ymd", $tmp_date) > date("Ymd"))
		{
			$tmp_time_info['has_link'] = false;
		}
		else
		{
			$tmp_time_info['has_link'] = true;
		}

		//セルの記号
		//リンクがない場合
		if(!$tmp_time_info['has_link'])
		{
			$tmp_time_info['mark'] = '　';
		}
		//未入力の場合
		elseif(!isset($cal_eval_record[date("Ymd", $tmp_date)][$tmp_time]) or $cal_eval_record[date("Ymd", $tmp_date)][$tmp_time] == "")
		{
			$tmp_time_info['mark'] = '■';
		}
		//保留の場合
		elseif($cal_eval_record[date("Ymd", $tmp_date)][$tmp_time]['input_state'] == 2)
		{
			$tmp_time_info['mark'] = '▲';
		}
		//不在の場合
		elseif($cal_eval_record[date("Ymd", $tmp_date)][$tmp_time]['input_state'] == 3)
		{
			$tmp_time_info['mark'] = '×';
		}
		//確定の場合
		else
		{
			$tmp_time_info['mark'] = $cal_eval_record[date("Ymd", $tmp_date)][$tmp_time]['pt_class_level'];
		}

		//セルの色
		//選択している時間帯
		if($input_date == date("Ymd", $tmp_date) && $input_time == $tmp_time)
		{
			$tmp_time_info['color'] = '#88FFFF';
		}
		//未入棟の場合
		elseif(!$cal_is_inpt_date)
		{
			$tmp_time_info['color'] = '#888888';
		}
		//それ以外
		else
		{
			$tmp_time_info['color'] = '#FFFFFF';
		}

		$tmp_date_info['time'][$tmp_time] = $tmp_time_info;
	}
	$cal_info[$cal_d] = $tmp_date_info;
}

//先月翌月
$tmp_date = mktime(0, 0, 0, $cal_m, -1, $cal_y);
$cal_prev_y = date("Y", $tmp_date);
$cal_prev_m = date("m", $tmp_date);
$tmp_date = mktime(0, 0, 0, $cal_m, 32, $cal_y);
$cal_next_y = date("Y", $tmp_date);
$cal_next_m = date("m", $tmp_date);

//2008/04/01以前は評価票の定義がないため、アクセス禁止
if($cal_prev_y <= "2008" && $cal_prev_m < "04")
{
	$cal_prev_y = $cal_y;
	$cal_prev_m = $cal_m;
}


//==============================
//その他
//==============================

//画面名
$PAGE_TITLE = "看護観察記録";

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$KANGO_TITLE?> | <?=$PAGE_TITLE?></title>


<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">


function initPage()
{
	input_item_changed_all();
}

//全体ての入力項目変更時の処理を実効
function input_item_changed_all()
{
	<?
	for($ab_i=0;$ab_i<=1;$ab_i++)
	{
		foreach($form_info[$ab_i]['eval_item_info'] as $item_info)
		{
			?>
			input_item_changed(<?=$item_info['eval_form_id']?>,<?=$item_info['eval_item_id']?>);
			<?
		}
	}
	?>
}

//入力項目変更時の処理：色変更
function input_item_changed(eval_form_id,eval_item_id)
{
	var td_id = "eval_item_input_td_" + eval_form_id + "_" + eval_item_id;
	var td_obj = document.getElementById(td_id);

	var cate1_radio_id = "input_" + eval_form_id + "_" + eval_item_id + "_1";
	var cate1_radio = document.getElementById(cate1_radio_id);

	if(cate1_radio.checked)
	{
		td_obj.style.backgroundColor = "#FFFFFF";
	}
	else
	{
		td_obj.style.backgroundColor = "#FFFF00";
	}

	sum_item_point();
}

//評価表得点の合計計算
function sum_item_point() {
	var a1 = 0;
	var b1 = 0;

	//評価表A
	<?
	foreach($form_info[0]['eval_item_info'] as $item_info)
	{
		foreach($item_info['eval_cate_info'] as $cate_info)
		{
		?>
			if(document.getElementById("input_<?=$cate_info['eval_form_id']?>_<?=$cate_info['eval_item_id']?>_<?=$cate_info['eval_cate_no']?>").checked) {
				a1 += parseInt(document.getElementById("point1_<?=$cate_info['eval_form_id']?>_<?=$cate_info['eval_item_id']?>_<?=$cate_info['eval_cate_no']?>").value);
			}
		<?
		}
	}
	?>
	//評価表B
	<?
	foreach($form_info[1]['eval_item_info'] as $item_info)
	{
		foreach($item_info['eval_cate_info'] as $cate_info)
		{
		?>
			if(document.getElementById("input_<?=$cate_info['eval_form_id']?>_<?=$cate_info['eval_item_id']?>_<?=$cate_info['eval_cate_no']?>").checked) {
				b1 += parseInt(document.getElementById("point1_<?=$cate_info['eval_form_id']?>_<?=$cate_info['eval_item_id']?>_<?=$cate_info['eval_cate_no']?>").value);
			}
		<?
		}
	}
	?>

	document.getElementById("point_A1").innerHTML = a1;
	document.getElementById("point_B1").innerHTML = b1;
}

//月間評価表
function go_report()
{
	location.href = 'kango_eval_report.php?session=<?=$session?>&pt_id=<?=$pt_id?>&cal_y=<?=$cal_y?>&cal_m=<?=$cal_m?>';
}

//前患者に移動
function go_prev_pt()
{
	<?
	if($prev_pt_info == null)
	{
		?>
		alert("棟内に前患者はいません。");
		<?
	}
	else
	{
		?>
		document.mainform.ward_cd.value = "<?=$prev_pt_info['ward_cd']?>";
		document.mainform.ptrm_room_no.value = "<?=$prev_pt_info['ptrm_room_no']?>";
		document.mainform.inpt_bed_no.value = "<?=$prev_pt_info['inpt_bed_no']?>";
		document.mainform.pt_id.value = "<?=$prev_pt_info['ptif_id']?>";
		document.mainform.postback_mode.value = "change_pt";
		document.mainform.submit();
		<?
	}
	?>
}
//次患者に移動
function go_next_pt()
{
	<?
	if($next_pt_info == null)
	{
		?>
		alert("棟内に次患者はいません。");
		<?
	}
	else
	{
		?>
		document.mainform.ward_cd.value = "<?=$next_pt_info['ward_cd']?>";
		document.mainform.ptrm_room_no.value = "<?=$next_pt_info['ptrm_room_no']?>";
		document.mainform.inpt_bed_no.value = "<?=$next_pt_info['inpt_bed_no']?>";
		document.mainform.pt_id.value = "<?=$next_pt_info['ptif_id']?>";
		document.mainform.postback_mode.value = "change_pt";
		document.mainform.submit();
		<?
	}
	?>
}

//カレンダー先月
function go_cal_prev()
{
	document.mainform.cal_y.value = "<?=$cal_prev_y?>";
	document.mainform.cal_m.value = "<?=$cal_prev_m?>";
	document.mainform.postback_mode.value = "change_cal";
	document.mainform.submit();
}
//カレンダー翌月
function go_cal_next()
{
	document.mainform.cal_y.value = "<?=$cal_next_y?>";
	document.mainform.cal_m.value = "<?=$cal_next_m?>";
	document.mainform.postback_mode.value = "change_cal";
	document.mainform.submit();
}


//日時変更
function change_date_time(p_date,p_time)
{
	document.mainform.input_date.value = p_date;
	document.mainform.input_time.value = p_time;
	document.mainform.postback_mode.value = "change_date_time";
	document.mainform.submit();
}


//現在の情報を保存します。
//0:クリア
//1:確定
//2:保留
//3:不在
function save(save_mode)
{

	//既存データに対してクリアを行う場合の意思確認
	//※看護記録後に入退院日が変更された場合はクリアにより不正記録を削除頂く必要あり。
	<?
	//データが保存されている場合(未入力以外)
	if($input_state != 0)
	{
		?>
		if (save_mode == 0 && !confirm("記録内容を削除します。よろしいですか？"))
		{
			return;
		}
		<?
	}
	?>
	document.mainform.postback_mode.value = "save";
	document.mainform.input_state.value = save_mode;
	document.mainform.submit();
}



//コピー(前)
function copy_prev()
{
	document.mainform.postback_mode.value = "copy_prev";
	document.mainform.submit();
}

//コピー(後)
function copy_next()
{
	document.mainform.postback_mode.value = "copy_next";
	document.mainform.submit();
}




</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

.list_in {border-collapse:collapse;}
.list_in td {border:#FFFFFF solid 0px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<!-- ヘッダー START -->
<?
$show_kango_header_option = null;
$show_kango_header_option['input_date'] = $input_date;
$show_kango_header_option['input_time'] = $input_time;
$show_kango_header_option['bldg_cd'] = $bldg_cd;
$show_kango_header_option['ward_cd'] = $ward_cd;
$show_kango_header_option['ptrm_room_no'] = $ptrm_room_no;
$show_kango_header_option['inpt_bed_no'] = $inpt_bed_no;
$show_kango_header_option['pt_id'] = $pt_id;
show_kango_header($session,$fname,false,$show_kango_header_option);
?>
<!-- ヘッダー END -->
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>

<!-- 全体 START -->
<form name="mainform" action="<?=$fname?>" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">
<!-- 表示条件：いつ -->
<input type="hidden" name="input_date" value="<?=$input_date?>">
<input type="hidden" name="input_time" value="<?=$input_time?>">
<!-- 表示条件：どこで -->
<input type="hidden" name="bldg_cd" value="<?=$bldg_cd?>">
<input type="hidden" name="ward_cd" value="<?=$ward_cd?>">
<input type="hidden" name="ptrm_room_no" value="<?=$ptrm_room_no?>">
<input type="hidden" name="inpt_bed_no" value="<?=$inpt_bed_no?>">
<!-- 表示条件：だれが -->
<input type="hidden" name="pt_id" value="<?=$pt_id?>">

<!-- カレンダー情報 -->
<input type="hidden" name="cal_y" value="<?=$cal_y?>">
<input type="hidden" name="cal_m" value="<?=$cal_m?>">

<!-- 保存時の記録状態 -->
<input type="hidden" name="input_state" value="">






<!-- 機能タイトル START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="250">
<b>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
看護必要度
</font>
</b>
</td>
<td>
&nbsp;
</td>
</tr>
</table>
<!-- 機能タイトル END -->



<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
</table>


<!-- 患者情報表示領域 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- 患者情報 START -->
<table width='100%' border='0' cellspacing='0' cellpadding='1' class="list">
<tr>
<td bgcolor='#bdd1e7' width="45" align="right" nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>患者ID</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$pt_id?></font></td>
<td bgcolor='#bdd1e7' width="60" align="right" nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>患者氏名</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($pt_name)?></font></td>
<td bgcolor='#bdd1e7' width="35" align="right" nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>性別</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$pt_sex?></font></td>
<td bgcolor='#bdd1e7' width="35" align="right" nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>部屋</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($ptrm_name)?></font></td>
<td bgcolor='#bdd1e7' width="65" align="right" nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>ベッド番号</font></td>
<td width="20"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$inpt_bed_no?></font></td>
</tr>
</table>
<!-- 患者情報 END -->


</td>
<td width="200" align="right" nowrap>

<!-- 患者切り替え START -->
<input type="button" value="前患者" onclick="go_prev_pt()">
<input type="button" value="次患者" onclick="go_next_pt()">
<!-- 患者切り替え END -->

</td>
</tr>
</table>
<!-- 患者情報表示領域 END -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
</table>














<!-- 看護記録履歴表示領域 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>


<!-- 看護記録履歴_全体枠 START -->
<table width='100%' border='0' cellspacing='0' cellpadding='1' class="list">
<tr>
<td>

<!-- 看護記録履歴_月切り替え START -->
<table width='100%' border='0' cellspacing='0' cellpadding='1' class="list_in">
<tr bgcolor='#bdd1e7'>
<td width="120" align="center">
	<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
	<a href="javascript:go_cal_prev()">＜＜</a>
	<?=$cal_y."/".$cal_m?>
	<a href="javascript:go_cal_next()">＞＞</a>
	</font>
	</td>
<td width="30">&nbsp;</td>
<td align="right" nowrap><button type="button" onclick="go_report();">月間評価表</button></td>
</tr>
</table>
<!-- 看護記録履歴_月切り替え END -->

</td>
</tr>
<tr>
<td>


<!-- 看護記録履歴_カレンダー START -->
<table border='0' cellspacing='0' cellpadding='1' class="list">
	<tr>
	<td bgcolor='#bdd1e7' width="20" align="center" valign="middle"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>&nbsp</font></td>
	<?
	for($cal_d=1;$cal_d<=$cal_d_max;$cal_d++)
	{
		?>
		<td bgcolor='<?=$cal_info[$cal_d]['color']?>' width="20" align="center" valign="middle"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$cal_info[$cal_d]['week_day']?></font></td>
		<?
	}
	?>
	</tr>
	<tr>
	<td bgcolor='#bdd1e7' width="20" align="center" valign="middle"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>日</font></td>
	<?
	for($cal_d=1;$cal_d<=$cal_d_max;$cal_d++)
	{
		?>
		<td bgcolor='<?=$cal_info[$cal_d]['color']?>' width="20" align="center" valign="middle"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$cal_d?></font></td>
		<?
	}
	?>
	</tr>
<?
$cal_time_name_list = null;
$cal_time_name_list[1] = '深';
$cal_time_name_list[2] = '日';
$cal_time_name_list[3] = '準';

for($cal_time = 1;$cal_time<=3;$cal_time++)
{
	$cal_time_name = $cal_time_name_list[$cal_time];

	?>
	<tr>
	<td bgcolor='#bdd1e7' align="center" valign="middle"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$cal_time_name?></font></td>
	<?
	for($cal_d=1;$cal_d<=$cal_d_max;$cal_d++)
	{
		$cal_ymd = date("Ymd", mktime(0, 0, 0, $cal_m, $cal_d, $cal_y));
		?>
		<td align="center" valign="middle" bgcolor="<?=$cal_info[$cal_d]['time'][$cal_time]['color']?>">
			<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
			<?
			if($cal_info[$cal_d]['time'][$cal_time]['has_link'])
			{
				?>
				<a href="javascript:change_date_time('<?=$cal_ymd?>',<?=$cal_time?>)">
				<?=$cal_info[$cal_d]['time'][$cal_time]['mark']?>
				</a>
				<?
			}
			else
			{
				?>
				<?=$cal_info[$cal_d]['time'][$cal_time]['mark']?>
				<?
			}
			?>
			</font>
			</td>
		<?
	}
	?>
	</tr>
	<?
}
?>

</table>
<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>※表示記号&nbsp;[数字]=患者レベル(確定)&nbsp[▲]=保留&nbsp;[■]=未入力&nbsp;[×]=不在</font>
<!-- 看護記録履歴_カレンダー END -->
</td>
</tr>
</table>
<!-- 看護記録履歴_全体枠 END -->

</td>
</tr>
</table>
<!-- 看護記録履歴表示領域 END -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
</table>














<!-- 実効ボタン表示領域 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="75" nowrap>
	<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?="{$input_date_y}/{$input_date_m}/{$input_date_d}"?></font>
	</td>
<td width="50" <?if($input_time == 1){echo(" bgcolor='#88FFFF'");}?> align="center" nowrap>
	<a href="javascript:change_date_time(<?=$input_date?>,1)">
	<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>深夜帯</font>
	</a>
	</td>
<td width="50" <?if($input_time == 2){echo(" bgcolor='#88FFFF'");}?> align="center" nowrap>
	<a href="javascript:change_date_time(<?=$input_date?>,2)">
	<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>日勤帯</font>
	</a>
	</td>
<td width="50" <?if($input_time == 3){echo(" bgcolor='#88FFFF'");}?> align="center" nowrap>
	<a href="javascript:change_date_time(<?=$input_date?>,3)">
	<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>準夜帯</font>
	</a>
	</td>
<td>
</td>
<?
if($is_target_inpt)
{
?>
<td align="right" nowrap>
	<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
	<input type="checkbox" id="renzoku_copy" name="renzoku_copy" value="t">
	<label for="renzoku_copy">連続コピー</label>
	<input type="button" value="コピー(前)" style="width:80px" onclick="copy_prev()">
	<input type="button" value="コピー(後)" style="width:80px" onclick="copy_next()">
	<input type="button" value="保留"       style="width:80px" onclick="save(2)">
	<input type="button" value="確定"       style="width:80px" onclick="save(1)">
	<input type="button" value="不在"       style="width:80px" onclick="save(3)">
	<input type="button" value="クリア"     style="width:80px" onclick="save(0)">
	</font>
	</td>
<?
}
else
{
?>
<td width="550">
	<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
	<b>この時は入院していません。</b>
	</font>
	</td>
<?
}
?>
</tr>
</table>

</td>
</tr>
</table>
<!-- 実効ボタン表示領域 END -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
</table>

















<!-- 看護記録入力領域 START -->
<?
if($is_target_inpt)
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>

<?
$ab_list = null;
$ab_list[0] = "A";
$ab_list[1] = "B";

$input_index = -1;

for($ab_i=0;$ab_i<=1;$ab_i++)
{
	$ab_big = $ab_list[$ab_i];

	?>
	<td width="50%" valign="top">


	<!-- 評価票_外枠 START -->
	<table width='100%' border='0' cellspacing='0' cellpadding='1' class="list">
	<tr>
	<td bgcolor='#bdd1e7'>
	<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>評価票<?=$ab_big?></font>
	</td>
	</tr>
	<tr>
	<td>

	<table width='100%' border='0' cellspacing='0' cellpadding='1' class="list">
	<?
	foreach($form_info[$ab_i]['eval_item_info'] as $item_info)
	{
		$input_index++;
		?>
		<tr>
		<td bgcolor='#bdd1e7' width="50%"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=nl2br(h($item_info['eval_item_name']))?></font></td>
		<td id="eval_item_input_td_<?=$item_info['eval_form_id']?>_<?=$item_info['eval_item_id']?>" width="50%">
			<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
			<?
			foreach($item_info['eval_cate_info'] as $cate_info)
			{
				?>
				<input type="radio"
				       id="input_<?=$cate_info['eval_form_id']?>_<?=$cate_info['eval_item_id']?>_<?=$cate_info['eval_cate_no']?>"
				       name="input<?=$input_index?>"
				       value="<?=$cate_info['eval_form_id']?>_<?=$cate_info['eval_item_id']?>_<?=$cate_info['eval_cate_no']?>"
				       onclick="input_item_changed(<?=$cate_info['eval_form_id']?>,<?=$cate_info['eval_item_id']?>)"
				       <? if($cate_info['selected'] == 't'){echo('checked');} ?>
				       >
				<input type="hidden" id="point1_<?=$cate_info['eval_form_id']?>_<?=$cate_info['eval_item_id']?>_<?=$cate_info['eval_cate_no']?>" value="<?=$cate_info['eval_point1']?>">
				<label for="input_<?=$cate_info['eval_form_id']?>_<?=$cate_info['eval_item_id']?>_<?=$cate_info['eval_cate_no']?>"><?=h($cate_info['eval_cate_name'])?></label>
				<?
			}
			?>
			</font>
			</td>
		</tr>
		<?
	}
	?>
		<tr>
		<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><b>評価表合計点数</b></font></td>
		<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><span id="point_<?=$ab_big?>1">0</span>点</font></td>
		</tr>
	</table>


	</td>
	</tr>
	</table>
	<!-- 評価票_外枠 END -->

	</td>
	<?
}
?>

</tr>
</table>
<?
}
?>
<!-- 看護記録入力領域 END -->












</form>
<!-- 全体 END -->

</td>
</tr>
</table>
</body>
</html>
<?
pg_close($con);
?>
