<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理 | 患者基礎データ</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");
require("inpatient_list_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// 患者登録権限を取得
$pt_reg_auth = check_authority($session, 22, $fname);

// データベースに接続
$con = connect2db($fname);

// 患者情報を取得
$sql = "select * from ptifmst";
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");;
	exit;
}
$lt_nm = pg_fetch_result($sel, 0, "ptif_lt_kaj_nm");
$ft_nm = pg_fetch_result($sel, 0, "ptif_ft_kaj_nm");

// 患者詳細情報を取得
if ($back != "t") {
	$sql = "select * from ptsubif";
	$cond = "where ptif_id = '$pt_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");;
		exit;
	}

	if (pg_num_rows($sel) > 0) {
		list($height1, $height2) = explode(".", pg_fetch_result($sel, 0, "ptsubif_height"));
		if ($height2 == "") {$height2 = "0";}

		list($weight1, $weight2) = explode(".", pg_fetch_result($sel, 0, "ptsubif_weight"));
		if ($weight2 == "") {$weight2 = "0";}

		if (pg_fetch_result($sel, 0, "ptsubif_move_flg") == "t") {
			$ptsubif_move = "0";
		} else if (pg_fetch_result($sel, 0, "ptsubif_move1") == "t") {
			$ptsubif_move = "1";
		} else if (pg_fetch_result($sel, 0, "ptsubif_move2") == "t") {
			$ptsubif_move = "2";
		} else if (pg_fetch_result($sel, 0, "ptsubif_move3") == "t") {
			$ptsubif_move = "3";
		} else if (pg_fetch_result($sel, 0, "ptsubif_move4") == "t") {
			$ptsubif_move = "4";
		} else if (pg_fetch_result($sel, 0, "ptsubif_move5") == "t") {
			$ptsubif_move = "5";
		}

		$ptsubif_move6 = pg_fetch_result($sel, 0, "ptsubif_move6");

		$ptsubif_tool_flg = pg_fetch_result($sel, 0, "ptsubif_tool_flg");
		for ($i = 1; $i <= 7; $i++) {
			$var_name = "ptsubif_tool$i";
			$$var_name = pg_fetch_result($sel, 0, "ptsubif_tool$i");
		}

		$blood_abo = pg_fetch_result($sel, 0, "ptsubif_blood_abo");
		$blood_rh = pg_fetch_result($sel, 0, "ptsubif_blood_rh");
		$ventilator = pg_fetch_result($sel, 0, "ptsubif_ventilator");
		$bronchotomy = pg_fetch_result($sel, 0, "ptsubif_bronchotomy");
		$experiment = pg_fetch_result($sel, 0, "ptsubif_experiment_flg");
		$dialysis_flg = pg_fetch_result($sel, 0, "ptsubif_dialysis");

		$ptsubif_algy_flg = pg_fetch_result($sel, 0, "ptsubif_algy_flg");
		for ($i = 1; $i <= 7; $i++) {
			$var_name = "ptsubif_allergy$i";
			$$var_name = pg_fetch_result($sel, 0, "ptsubif_allergy$i");
		}

		$allergy_note = pg_fetch_result($sel, 0, "ptsubif_algy_com");
		$meal_note = pg_fetch_result($sel, 0, "ptsubif_meal_com");

		$ptsubif_infect_flg = pg_fetch_result($sel, 0, "ptsubif_infect_flg");
		for ($i = 1; $i <= 10; $i++) {
			$var_name = "ptsubif_infection$i";
			$$var_name = pg_fetch_result($sel, 0, "ptsubif_infection$i");
		}

		$epidemic_note = pg_fetch_result($sel, 0, "ptsubif_infection_com");
		$note1 = pg_fetch_result($sel, 0, "ptsubif_note");

		$medical_div = pg_fetch_result($sel, 0, "ptsubif_medical_div");
		$adl_div = pg_fetch_result($sel, 0, "ptsubif_adl_div");
	}
}

$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);

// 病棟一覧を取得
$sql = "select bldg_cd, ward_cd, ward_name from wdmst";
$cond = "where ward_del_flg = 'f' order by bldg_cd, ward_cd";
$sel_ward = select_from_table($con, $sql, $cond, $fname);
if ($sel_ward == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	setListDateDisabled();
}

<? show_inpatient_list_common_javascript(); ?>

function setItemsLabel() {
	var tools = new Array();
	if (document.mainform.ptsubif_tool_flg.value == 't') {
		tools.push('なし');
	}
	if (document.mainform.ptsubif_tool1.value == 't') {
		tools.push('上肢装具');
	}
	if (document.mainform.ptsubif_tool2.value == 't') {
		tools.push('下肢装具');
	}
	if (document.mainform.ptsubif_tool3.value == 't') {
		tools.push('義歯');
	}
	if (document.mainform.ptsubif_tool4.value == 't') {
		tools.push('義眼');
	}
	if (document.mainform.ptsubif_tool5.value == 't') {
		tools.push('補聴器');
	}
	if (document.mainform.ptsubif_tool6.value == 't') {
		tools.push('車椅子');
	}
	if (document.mainform.ptsubif_tool7.value == 't') {
		tools.push('歩行器');
	}

	var algys = new Array();
	if (document.mainform.ptsubif_algy_flg.value == 't') {
		algys.push('なし');
	}
	if (document.mainform.ptsubif_allergy1.value == 't') {
		algys.push('抗生物質');
	}
	if (document.mainform.ptsubif_allergy2.value == 't') {
		algys.push('解熱鎮静剤');
	}
	if (document.mainform.ptsubif_allergy3.value == 't') {
		algys.push('局所麻酔薬');
	}
	if (document.mainform.ptsubif_allergy4.value == 't') {
		algys.push('液製剤');
	}
	if (document.mainform.ptsubif_allergy5.value == 't') {
		algys.push('ワクチン');
	}
	if (document.mainform.ptsubif_allergy6.value == 't') {
		algys.push('造影剤');
	}
	if (document.mainform.ptsubif_allergy7.value == 't') {
		algys.push('その他');
	}

	var infects = new Array();
	if (document.mainform.ptsubif_infect_flg.value == 't') {
		infects.push('なし');
	}
	if (document.mainform.ptsubif_infection1.value == 't') {
		infects.push('未検査');
	}
	if (document.mainform.ptsubif_infection2.value == 't') {
		infects.push('梅毒');
	}
	if (document.mainform.ptsubif_infection3.value == 't') {
		infects.push('Ｂ型肝炎');
	}
	if (document.mainform.ptsubif_infection4.value == 't') {
		infects.push('Ｃ型肝炎');
	}
	if (document.mainform.ptsubif_infection5.value == 't') {
		infects.push('ＨＩＶ');
	}
	if (document.mainform.ptsubif_infection6.value == 't') {
		infects.push('ＭＲＳＡ');
	}
	if (document.mainform.ptsubif_infection7.value == 't') {
		infects.push('緑膿菌');
	}
	if (document.mainform.ptsubif_infection8.value == 't') {
		infects.push('疥癬');
	}
	if (document.mainform.ptsubif_infection9.value == 't') {
		infects.push('ＡＴＬ');
	}
	if (document.mainform.ptsubif_infection10.value == 't') {
		infects.push('その他');
	}

	document.getElementById('tool').innerHTML = tools.join(', ');
	document.getElementById('algy').innerHTML = algys.join(', ');
	document.getElementById('infect').innerHTML = infects.join(', ');
}

function openItemSetting() {
	window.open('patient_item_update.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>', 'newwin', 'width=640,height=600,scrollbars=yes');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="setItemsLabel();initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#bdd1e7"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床管理トップ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#5279a5"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>入退院登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr>
<td width="70%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>入院予定〜退院登録</b></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>検索条件設定</b></font></td>
</tr>
<tr>
<td valign="middle">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td align="center"><form action="inpatient_vacant_search.php" method="get"><input type="submit" value="空床検索"><input type="hidden" name="session" value="<? echo($session); ?>"></form><br><form action="inpatient_waiting_list.php" method="get"><input type="submit" value="待機患者"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_reserve_register_list.php" method="get"><input type="submit" value="入院予定"><input type="hidden" name="session" value="<? echo($session); ?>"><input type="hidden" name="mode" value="search"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_register_list.php" method="get"><input type="submit" value="入院"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_go_out_register_list.php" method="get"><input type="submit" value="外出・外泊"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_move_register_list.php" method="get"><input type="submit" value="転棟"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="out_inpatient_reserve_register_list.php" method="get">
<input type="submit" value="退院予定"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="out_inpatient_register_list.php" method="get"><input type="submit" value="退院"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
</tr>
</table>
</td>
<td valign="middle">
<? show_inpatient_list_form($con, $sel_ward, "", "", "", "", "", "", "", "", "", $session, $fname); ?>
</td>
</tr>
</table>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<? if ($path == "1") {  // 入院予定登録 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=1&key1=<? echo($url_key1) ?>&key2=<? echo($url_key2) ?>&ward=<? echo($ward); ?>&ptrm=<? echo($ptrm); ?>&bed=<? echo($bed); ?>&bedundec1=<? echo($bedundec1); ?>&bedundec2=<? echo($bedundec2); ?>&bedundec3=<? echo($bedundec3); ?>&ptwait_id=<? echo($ptwait_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="inpatient_patient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=1&key1=<? echo($url_key1) ?>&key2=<? echo($url_key2) ?>&ward=<? echo($ward); ?>&ptrm=<? echo($ptrm); ?>&bed=<? echo($bed); ?>&bedundec1=<? echo($bedundec1); ?>&bedundec2=<? echo($bedundec2); ?>&bedundec3=<? echo($bedundec3); ?>&ptwait_id=<? echo($ptwait_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>基礎データ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=1&key1=<? echo($url_key1) ?>&key2=<? echo($url_key2) ?>&ward=<? echo($ward); ?>&ptrm=<? echo($ptrm); ?>&bed=<? echo($bed); ?>&bedundec1=<? echo($bedundec1); ?>&bedundec2=<? echo($bedundec2); ?>&bedundec3=<? echo($bedundec3); ?>&ptwait_id=<? echo($ptwait_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=1&key1=<? echo($url_key1) ?>&key2=<? echo($url_key2) ?>&ward=<? echo($ward); ?>&ptrm=<? echo($ptrm); ?>&bed=<? echo($bed); ?>&bedundec1=<? echo($bedundec1); ?>&bedundec2=<? echo($bedundec2); ?>&bedundec3=<? echo($bedundec3); ?>&ptwait_id=<? echo($ptwait_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1) ?>&key2=<? echo($url_key2) ?>&ward=<? echo($ward); ?>&ptrm=<? echo($ptrm); ?>&bed=<? echo($bed); ?>&bedundec1=<? echo($bedundec1); ?>&bedundec2=<? echo($bedundec2); ?>&bedundec3=<? echo($bedundec3); ?>&ptwait_id=<? echo($ptwait_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院予定登録</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? } else if ($path == "2") {  // 入院予定情報 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=2&ptwait_id=<? echo($ptwait_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="inpatient_patient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=2&ptwait_id=<? echo($ptwait_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>基礎データ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=2&ptwait_id=<? echo($ptwait_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&ptwait_id=<? echo($ptwait_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院予定情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="inpatient_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&ptwait_id=<? echo($ptwait_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院登録</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? } else if ($path == "3") {  // 入院登録 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=3&key1=<? echo($url_key1) ?>&key2=<? echo($url_key2) ?>&ward=<? echo($ward); ?>&ptrm=<? echo($ptrm); ?>&bed=<? echo($bed); ?>&bedundec1=<? echo($bedundec1); ?>&bedundec2=<? echo($bedundec2); ?>&bedundec3=<? echo($bedundec3); ?>&ptwait_id=<? echo($ptwait_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="inpatient_patient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=3&key1=<? echo($url_key1) ?>&key2=<? echo($url_key2) ?>&ward=<? echo($ward); ?>&ptrm=<? echo($ptrm); ?>&bed=<? echo($bed); ?>&bedundec1=<? echo($bedundec1); ?>&bedundec2=<? echo($bedundec2); ?>&bedundec3=<? echo($bedundec3); ?>&ptwait_id=<? echo($ptwait_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>基礎データ</b></font></a>
</td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=3&key1=<? echo($url_key1) ?>&key2=<? echo($url_key2) ?>&ward=<? echo($ward); ?>&ptrm=<? echo($ptrm); ?>&bed=<? echo($bed); ?>&bedundec1=<? echo($bedundec1); ?>&bedundec2=<? echo($bedundec2); ?>&bedundec3=<? echo($bedundec3); ?>&ptwait_id=<? echo($ptwait_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=3&key1=<? echo($url_key1) ?>&key2=<? echo($url_key2) ?>&ward=<? echo($ward); ?>&ptrm=<? echo($ptrm); ?>&bed=<? echo($bed); ?>&bedundec1=<? echo($bedundec1); ?>&bedundec2=<? echo($bedundec2); ?>&bedundec3=<? echo($bedundec3); ?>&ptwait_id=<? echo($ptwait_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&ptwait_id=<? echo($ptwait_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院予定情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="inpatient_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1) ?>&key2=<? echo($url_key2) ?>&ward=<? echo($ward); ?>&ptrm=<? echo($ptrm); ?>&bed=<? echo($bed); ?>&bedundec1=<? echo($bedundec1); ?>&bedundec2=<? echo($bedundec2); ?>&bedundec3=<? echo($bedundec3); ?>&ptwait_id=<? echo($ptwait_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院登録</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? } else if ($path == "4") {  // 入院情報 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="inpatient_patient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>基礎データ</b></font></a>
</td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="inpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="inpatient_check_list.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">チェックリスト</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="inpatient_nutrition.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">栄養問診表</font></a></td>
<td width="5">&nbsp;</td>
<td width="170" align="center" bgcolor="#bdd1e7"><a href="inpatient_exception.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均在院日数除外指定</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? } else if ($path == "5") {  // 退院予定情報 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="inpatient_patient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>基礎データ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="inpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="inpatient_check_list.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">チェックリスト</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="inpatient_nutrition.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">栄養問診表</font></a></td>
<td width="5">&nbsp;</td>
<td width="170" align="center" bgcolor="#bdd1e7"><a href="inpatient_exception.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均在院日数除外指定</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="outpatient_reserve_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院予定情報</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? } else if ($path == "6") {  // 退院情報 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="inpatient_patient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>基礎データ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="inpatient_detail_ref.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="inpatient_check_list_ref.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">チェックリスト</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="inpatient_nutrition_ref.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">栄養問診表</font></a></td>
<td width="5">&nbsp;</td>
<td width="170" align="center" bgcolor="#bdd1e7"><a href="inpatient_exception.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均在院日数除外指定</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="outpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院情報</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? } else if ($path == "7") {  // キャンセル情報 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=7&ccl_id=<? echo($ccl_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="inpatient_patient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=7&ccl_id=<? echo($ccl_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>基礎データ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=7&ccl_id=<? echo($ccl_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=7&ccl_id=<? echo($ccl_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_detail_ref.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&ccl_id=<? echo($ccl_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院予定情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="inpatient_cancel_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&ccl_id=<? echo($ccl_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">キャンセル情報</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? } ?>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="mainform" action="inpatient_patient_sub_update_exe.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="24%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pt_id); ?></font></td>
<td width="16%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者氏名</font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$lt_nm $ft_nm"); ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="24%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">身長</font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="height1" value="<? echo($height1); ?>" size="3" maxlength="3" style="ime-mode:inactive;">.<input type="text" name="height2" value="<? echo($height2); ?>" size="3" maxlength="1" style="ime-mode:inactive;">cm</font></td>
<td width="16%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">体重</font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="weight1" value="<? echo($weight1); ?>" size="3" maxlength="3" style="ime-mode:inactive;">.<input type="text" name="weight2" value="<? echo($weight2); ?>" size="3" maxlength="1" style="ime-mode:inactive;">kg</font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">救護区分・移動手段</font></td>
<td><select name="ptsubif_move">
<option value="">
<option value="0"<? if ($ptsubif_move == "0") {echo(" selected");} ?>>独歩行
<option value="1"<? if ($ptsubif_move == "1") {echo(" selected");} ?>>杖歩行
<option value="2"<? if ($ptsubif_move == "2") {echo(" selected");} ?>>介助歩行
<option value="3"<? if ($ptsubif_move == "3") {echo(" selected");} ?>>車椅子（自己駆動）
<option value="4"<? if ($ptsubif_move == "4") {echo(" selected");} ?>>車椅子（介助）
<option value="5"<? if ($ptsubif_move == "5") {echo(" selected");} ?>>担送
</select></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">装具・補助具</font></td>
<td style="padding:0;">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="tool"></span></font>
<input type="hidden" name="ptsubif_tool_flg" value="<? echo($ptsubif_tool_flg); ?>">
<?
for ($i = 1; $i <= 7; $i++) {
	$var_name = "ptsubif_tool$i";
	echo("<input type=\"hidden\" name=\"ptsubif_tool$i\" value=\"{$$var_name}\">\n");
}
?>
</td>
<? if ($pt_reg_auth == 1) { ?>
<td align="right" valign="top"><a href="javascript:void(0);" onclick="openItemSetting();"><img src="img/pencil.gif" alt="項目選択" width="13" height="13" border="0"></a></td>
<? } ?>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">酸素必要</font></td>
<td><input type="checkbox" name="ptsubif_move6" value="t"<? if ($ptsubif_move6 == "t") {echo " checked";} ?>></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">人工呼吸器</font></td>
<td><input type="checkbox" name="ventilator" value="t"<? if ($ventilator == "t") {echo " checked";} ?>></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">血液型</font></td>
<td>
<select name="blood_abo">
<option value="0"<? if ($blood_abo == "0") {echo " selected";} ?>>不明
<option value="1"<? if ($blood_abo == "1") {echo " selected";} ?>>A型
<option value="2"<? if ($blood_abo == "2") {echo " selected";} ?>>B型
<option value="3"<? if ($blood_abo == "3") {echo " selected";} ?>>AB型
<option value="4"<? if ($blood_abo == "4") {echo " selected";} ?>>O型
</select>
<select name="blood_rh">
<option value="0"<? if ($blood_rh == "0") {echo " selected";} ?>>不明
<option value="1"<? if ($blood_rh == "1") {echo " selected";} ?>>Rh+
<option value="2"<? if ($blood_rh == "2") {echo " selected";} ?>>Rh-
</select>
</td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">気管切開</font></td>
<td><input type="checkbox" name="bronchotomy" value="t"<? if ($bronchotomy == "t") {echo " checked";} ?>></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">治験患者</font></td>
<td><input type="checkbox" name="experiment" value="t"<? if ($experiment == "t") {echo " checked";} ?>></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">透析患者</font></td>
<td><input type="checkbox" name="dialysis_flg" value="t"<? if ($dialysis_flg == "t") {echo " checked";} ?>></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アレルギー</font></td>
<td colspan="3" style="padding:0;">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="algy"></span></font>
<input type="hidden" name="ptsubif_algy_flg" value="<? echo($ptsubif_algy_flg); ?>">
<?
for ($i = 1; $i <= 7; $i++) {
	$var_name = "ptsubif_allergy$i";
	echo("<input type=\"hidden\" name=\"ptsubif_allergy$i\" value=\"{$$var_name}\">\n");
}
?>
</td>
<? if ($pt_reg_auth == 1) { ?>
<td align="right" valign="top"><a href="javascript:void(0);" onclick="openItemSetting();"><img src="img/pencil.gif" alt="項目選択" width="13" height="13" border="0"></a></td>
<? } ?>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アレルギーコメント</font></td>
<td colspan="3"><textarea cols="50" rows="5" name="allergy_note" style="ime-mode:active;"><? echo($allergy_note); ?></textarea></td>
</tr>
<!--
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">食事コメント</font></td>
<td colspan="3"><textarea cols="50" rows="5" name="meal_note" style="ime-mode:active;"><? echo($meal_note); ?></textarea></td>
</tr>
-->
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">感染症</font></td>
<td colspan="3" style="padding:0;">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="infect"></span></font>
<input type="hidden" name="ptsubif_infect_flg" value="<? echo($ptsubif_infect_flg); ?>">
<?
for ($i = 1; $i <= 10; $i++) {
	$var_name = "ptsubif_infection$i";
	echo("<input type=\"hidden\" name=\"ptsubif_infection$i\" value=\"{$$var_name}\">\n");
}
?>
</td>
<? if ($pt_reg_auth == 1) { ?>
<td align="right" valign="top"><a href="javascript:void(0);" onclick="openItemSetting();"><img src="img/pencil.gif" alt="項目選択" width="13" height="13" border="0"></a></td>
<? } ?>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">感染症コメント</font></td>
<td colspan="3"><textarea cols="50" rows="5" name="epidemic_note" style="ime-mode:active;"><? echo($epidemic_note); ?></textarea></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">特記事項</font></td>
<td colspan="3"><textarea rows="5" cols="50" name="note1" style="ime-mode:active;"><? echo($note1); ?></textarea></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">医療区分</font></td>
<td><select name="medical_div">
<option value="">
<option value="1"<? if ($medical_div == "1") {echo(" selected");} ?>>1
<option value="2"<? if ($medical_div == "2") {echo(" selected");} ?>>2
<option value="3"<? if ($medical_div == "3") {echo(" selected");} ?>>3
</select></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ADL区分</font></td>
<td><select name="adl_div">
<option value="">
<option value="1"<? if ($adl_div == "1") {echo(" selected");} ?>>1
<option value="2"<? if ($adl_div == "2") {echo(" selected");} ?>>2
<option value="3"<? if ($adl_div == "3") {echo(" selected");} ?>>3
</select></td>
</tr>
</table>
<? if ($pt_reg_auth == 1) { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<? } ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="key1" value="<? echo($key1); ?>">
<input type="hidden" name="key2" value="<? echo($key2); ?>">
<input type="hidden" name="ptwait_id" value="<? echo($ptwait_id); ?>">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="ward" value="<? echo($ward); ?>">
<input type="hidden" name="ptrm" value="<? echo($ptrm); ?>">
<input type="hidden" name="bed" value="<? echo($bed); ?>">
<input type="hidden" name="bedundec1" value="<? echo($bedundec1); ?>">
<input type="hidden" name="bedundec2" value="<? echo($bedundec2); ?>">
<input type="hidden" name="bedundec3" value="<? echo($bedundec3); ?>">
<input type="hidden" name="in_dt" value="<? echo($in_dt); ?>">
<input type="hidden" name="in_tm" value="<? echo($in_tm); ?>">
<input type="hidden" name="ccl_id" value="<? echo($ccl_id); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
