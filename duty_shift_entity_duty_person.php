<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成｜管理 |当直管理者設定</title>

<?
//ini_set("display_errors","1");
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common.ini");
require_once("duty_shift_manage_tab_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_duty_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_duty = new duty_shift_duty_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//管理画面用
$chk_flg = $obj->check_authority_Management($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//ユーザ画面用
$section_admin_auth = $obj->check_authority_user($session, $fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	//ＤＢ(stmst)より役職情報取得
	//ＤＢ(jobmst)より職種情報取得
	//ＤＢ(empmst)より職員情報を取得
	///-----------------------------------------------------------------------------
	$data_st = $obj->get_stmst_array();
	$data_job = $obj->get_jobmst_array();
	$data_emp = $obj->get_empmst_array("");
	///-----------------------------------------------------------------------------
	// スタッフ情報を取得
	///-----------------------------------------------------------------------------
	$set_array = array();
	if ($reload_flg == "1") {
		///-----------------------------------------------------------------------------
		//再表示の場合、元データ取得
		///-----------------------------------------------------------------------------
		$wk_array = array();
		$wk_emp_id_array = split(",", $emp_id_array);
		$m=0;
		foreach ($wk_emp_id_array as $emp_id) {
			$wk_array[$m]["id"] = $emp_id;
			$m++;
		}
		///-----------------------------------------------------------------------------
		//指定職員IDの追加／更新／削除
		///-----------------------------------------------------------------------------
		if ($up_idx != "") {
			//変更の場合
			$wk_array[$up_idx]["id"] = $up_emp_id;
			$set_array = $wk_array;
		} else if ($del_emp_id != "") {
			//削除の場合
			$m=0;
			for($i=0; $i<count($wk_array); $i++) {
				if ($wk_array[$i]["id"] != $del_emp_id) {
					$set_array[$m] = $wk_array[$i];
					$m++;
				}
			}
		} else {
			//追加データの場合
			$m=0;
			$wk_add_array = array();
			$wk_emp_id_array = split(",", $add_emp_id_array);
			foreach ($wk_emp_id_array as $emp_id) {
				$wk_add_array[$m]["id"] = $emp_id;
				$m++;
			}
			$m=0;
			if ($add_idx == -1) {
				for($k=0; $k<count($wk_add_array); $k++) {
					$set_array[$m] = $wk_add_array[$k];
					$m++;
				}
			}
			if ($emp_id_array != "") {
				for($i=0; $i<count($wk_array); $i++) {
					if ($i == $add_idx) {
						$set_array[$m] = $wk_array[$i];
						$m++;
						for($k=0; $k<count($wk_add_array); $k++) {
							$set_array[$m] = $wk_add_array[$k];
							$m++;
						}
					} else {
						$set_array[$m] = $wk_array[$i];
						$m++;
					}
				}
			}
		}
		///-----------------------------------------------------------------------------
		//データ再設定
		///-----------------------------------------------------------------------------
		$staff_array = array();
		for($i=0;$i<count($set_array);$i++){
			$wk_data = $obj->get_duty_shift_staff_one($set_array[$i]["id"], $data_st, $data_job, $data_emp);
			$staff_array[$i]["id"] = $set_array[$i]["id"];			//スタッフＩＤ（職員ＩＤ）
			$staff_array[$i]["no"] = $i + 1;						//表示順
			$staff_array[$i]["name"] = $wk_data["name"];			//スタッフ名称
			$staff_array[$i]["job_id"] = $wk_data["job_id"];		//職種ＩＤ
			$staff_array[$i]["job_name"] = $wk_data["job_name"];	//職種名
			$staff_array[$i]["status"] = $wk_data["status"];		//役職
		}
	} else {
		///-----------------------------------------------------------------------------
		//初期表示の場合
		///-----------------------------------------------------------------------------
		$staff_array = $obj_duty->get_duty_shift_duty_person_array($data_st, $data_job, $data_emp);
	}

?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

	var childwin = null;

	///-----------------------------------------------------------------------------
	// openEmployeeList
	///-----------------------------------------------------------------------------
	function openEmployeeList(item_id) {
		dx = screen.width;
		dy = screen.top;
		base = 0;
		wx = 720;
		wy = 600;
		var url = './emplist_popup.php';
		url += '?session=<?=$session?>';
		url += '&emp_id=<?=$emp_id?>';
		url += '&mode=19';
		url += '&item_id='+item_id;
		childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

		childwin.focus();
	}
	///-----------------------------------------------------------------------------
	// closeEmployeeList
	///-----------------------------------------------------------------------------
	function closeEmployeeList() {
		if (childwin != null && !childwin.closed) {
			childwin.close();
		}
		childwin = null;
	}
	///-----------------------------------------------------------------------------
	// add_target_list
	///-----------------------------------------------------------------------------
	function add_target_list(item_id, emp_id, emp_name)
	{
		var emp_ids = emp_id.split(", ");
		var emp_names = emp_name.split(", ");

		///-----------------------------------------------------------------------------
		//変更用（先頭の人で判定）
		///-----------------------------------------------------------------------------
		wk_emp_id = emp_ids[0];
		wk_emp_name = emp_names[0];
		///-----------------------------------------------------------------------------
		//重複チェック（重複データは対象外）
		///-----------------------------------------------------------------------------
		var wk = document.mainform.emp_id_array.value;
		var emp_ids2 = wk.split(",");
		var ok_emp_ids="";
		var err_flg = 0;
		for(i=0;i<emp_ids.length;i++){
			err_flg = 0;
			for(k=0;k<emp_ids2.length;k++){
				if (emp_ids[i] == emp_ids2[k]) {
					err_flg = 1;
					break;
				}
			}
			if (err_flg == 0){
				if (ok_emp_ids != "") {
					ok_emp_ids += ",";
				}
				ok_emp_ids += emp_ids[i];
			}
		}

		if (ok_emp_ids == "") {
			alert('職員の重複指定はできません。');
			return;
		}
		///-----------------------------------------------------------------------------
		//画面再表示
		///-----------------------------------------------------------------------------
		document.mainform.up_flg.value = "";
		document.mainform.down_flg.value = "";

		document.mainform.reload_flg.value = "1";
		document.mainform.del_emp_id.value = "";
		document.mainform.up_emp_id.value = wk_emp_id;
		document.mainform.add_emp_id_array.value = ok_emp_ids;
		document.mainform.action="duty_shift_entity_duty_person.php";
		document.mainform.submit();
	}
	///-----------------------------------------------------------------------------
	// 追加
	///-----------------------------------------------------------------------------
	function addEmp(idx)
	{
		//追加行
		document.mainform.up_flg.value = "";
		document.mainform.down_flg.value = "";

		document.mainform.add_idx.value = idx;
		document.mainform.up_idx.value = "";
		document.mainform.del_emp_id.value = "";
	//	document.mainform.submit();
		//職員名簿表示
		openEmployeeList('1');
	}
	///-----------------------------------------------------------------------------
	// 変更
	///-----------------------------------------------------------------------------
	function updateEmp(idx)
	{
		//変更行
		document.mainform.up_flg.value = "";
		document.mainform.down_flg.value = "";

		document.mainform.add_idx.value = "";
		document.mainform.up_idx.value = idx;
		document.mainform.del_emp_id.value = "";
	//	document.mainform.submit();
		//職員名簿表示
		openEmployeeList('1');
	}
	///-----------------------------------------------------------------------------
	// 削除
	///-----------------------------------------------------------------------------
	function delEmp(emp_id)
	{
		//画面再表示
		document.mainform.up_flg.value = "";
		document.mainform.down_flg.value = "";

		document.mainform.reload_flg.value = "1";
		document.mainform.add_idx.value = "";
		document.mainform.up_idx.value = "";
		document.mainform.up_emp_id.value = "";
		document.mainform.del_emp_id.value = emp_id;
		document.mainform.action="duty_shift_entity_duty_person.php";
		document.mainform.submit();
	}
	///-----------------------------------------------------------------------------
	// 更新
	///-----------------------------------------------------------------------------
	function editData() {
		//職員登録子画面をＣＬＯＳＥ
		closeEmployeeList();

		//更新処理
	//	if (confirm('「更新」します。よろしいですか？')) {
			document.mainform.action="duty_shift_entity_duty_person_update_exe.php"
			document.mainform.submit();
	//	}
	}

</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
	<?
		// 画面遷移
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_manage_title($session, $section_admin_auth);			//duty_shift_common.ini
		echo("</table>\n");

		// タブ
		$arr_option = "";
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_manage_tab_menuitem($session, $fname, $arr_option);	//duty_shift_manage_tab_common.ini
		echo("</table>\n");

		// 下線
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	?>
	</td></tr></table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 職員一覧 -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="mainform" method="post">
		<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
		<?
			///-----------------------------------------------------------------------------
			// 職員一覧（タイトル）
			///-----------------------------------------------------------------------------
			echo("<tr height=\"22\"> \n");
			echo("<td width=\"5%\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">No</font></td> \n");
			echo("<td width=\"30%\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">名称</font></td> \n");
			echo("<td width=\"20%\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">職種</font></td> \n");
			echo("<td width=\"20%\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">役職</font></td> \n");
			echo("<td width=\"25%\" align=\"left\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
			echo("<input type=\"button\" value=\"追加\" onclick=\"addEmp(-1);\"> \n");
			echo("</font></td> \n");
			echo("</tr> \n");
			///-----------------------------------------------------------------------------
			// 職員一覧（データ）
			///-----------------------------------------------------------------------------
			for($i=0; $i<count($staff_array); $i++) {
				echo("<tr height=\"22\"> \n");
				$wk_id = $staff_array[$i]["id"];

				// 表示順
				echo("<td width=\"5%\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				echo($i + 1 );
				echo("</font></td> \n");

				// 名称
				echo("<td width=\"30%\" align=\"center\" bgcolor=\"$wk_bgcolor\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				echo($staff_array[$i]["name"]);
				echo("</font></td> \n");

				// 職種
				echo("<td width=\"20%\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				echo($staff_array[$i]["job_name"]);
				echo("</font></td> \n");

				// 役職
				echo("<td width=\"20%\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				echo($staff_array[$i]["status"]);
				echo("</font></td> \n");

				// 追加／変更／削除
				echo("<td width=\"25%\" align=\"left\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				echo("<input type=\"button\" value=\"追加\" onclick=\"addEmp($i);\"> \n");
//				echo("<input type=\"button\" value=\"変更\" onclick=\"updateEmp($i);\"> \n");
				echo("<input type=\"button\" value=\"削除\" onclick=\"delEmp('$wk_id');\"> \n");
				echo("</font></td>\n");
				echo("</tr>\n");
			}
		?>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 「更新」ボタン -->
		<!-- ------------------------------------------------------------------------ -->
		<br>
		<table width="600" border="0" cellspacing="0" cellpadding="0">
		<tr height="22">
			<td align="right"><input type="button" value="更新" onclick="editData();" ></td>
		</tr>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="emp_id_array" value="<? show_emp_id_array($staff_array); ?>">

		<input type="hidden" name="up_flg" value="<? echo($up_flg); ?>">
		<input type="hidden" name="down_flg" value="<? echo($down_flg); ?>">

		<input type="hidden" name="add_idx" value="<? echo($add_idx); ?>">
		<input type="hidden" name="up_idx" value="<? echo($up_idx); ?>">
		<input type="hidden" name="up_emp_id" value="<? echo($up_emp_id); ?>">
		<input type="hidden" name="del_emp_id" value="<? echo($del_emp_id); ?>">
		<input type="hidden" name="add_emp_id_array" value="<? echo($add_emp_id_array); ?>">
		<input type="hidden" name="reload_flg" value="<? echo($reload_flg); ?>">
	</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>

<?
///-----------------------------------------------------------------------------
// show_emp_id_array
///-----------------------------------------------------------------------------
function show_emp_id_array($staff_array) {
	for ($i = 0; $i < count($staff_array); $i++) {
		if ($i > 0) {
			echo(",");
		}
		echo($staff_array[$i]["id"]);
	}
}
?>

