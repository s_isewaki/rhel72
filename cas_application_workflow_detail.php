<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_cas_application_workflow_detail.ini");
require_once("show_select_values.ini");
require_once("cas_application_imprint_common.ini");
require_once("cas_yui_calendar_util.ini");
require_once("get_values.ini");
require_once("cas_application_workflow_common_class.php");
require_once("cas_common.ini");
require_once("get_cas_title_name.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cas_application_workflow_common_class($con, $fname);

$cas_title = get_cas_title_name();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cas_title?> | 申請詳細</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<?

// 申請・ワークフロー情報取得
$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
$wkfw_content      = $arr_apply_wkfwmst[0]["wkfw_content"];
$wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
$apply_content     = $arr_apply_wkfwmst[0]["apply_content"];
$wkfw_id           = $arr_apply_wkfwmst[0]["wkfw_id"];
$wkfw_history_no   = $arr_apply_wkfwmst[0]["wkfw_history_no"];

// 本文形式タイプのデフォルトを「テキスト」とする
if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

// 形式をテキストからテンプレートに変更した場合の古いデータ対応
// wkfw_content_typeは2:テンプレートだが、登録データがXMLでない場合、1:テキストとして処理する
if ($wkfw_content_type == "2") {
	if (strpos($apply_content, "<?xml") === false) {
		$wkfw_content_type = "1";
	}
}

if($wkfw_history_no != "")
{
	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];
}

$num = 0;
if ($wkfw_content_type == "2") {
	$pos = 0;
	while (1) {
		$pos = strpos($wkfw_content, 'show_cal', $pos);
		if ($pos === false) {
			break;
		} else {
			$num++;
		}
		$pos++;
	}
}


if ($num > 0) {
	// 外部ファイルを読み込む
	write_yui_calendar_use_file_read_0_12_2();
}
// カレンダー作成、関数出力
write_yui_calendar_script2($num);

/*
if ($mode == "apply_printwin") {

//---------テンプレートの場合XML形式のテキスト$contentを作成---------

	if ($wkfw_content_type == "2") {
		// XML変換用コードの保存
		$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
		$savexmlfilename = "workflow/tmp/{$session}_x.php";
		$fp = fopen($savexmlfilename, "w");

		if (!$fp) {
			echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
			echo("<script language='javascript'>history.back();</script>");
		}
		if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
			fclose($fp);
			echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
			echo("<script language='javascript'>history.back();</script>");
		}

		fclose($fp);

		include( $savexmlfilename );

	}
	$savexmlfilename = "workflow/tmp/{$session}_d.php";
	$fp = fopen($savexmlfilename, "w");

	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	if(!fwrite($fp, $content, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	fclose($fp);
}
*/
?>
<script type="text/javascript">
function apply_printwin()
{
	document.apply.mode.value = "apply_printwin";
	document.apply.action="cas_application_workflow_detail.php?session=<?=$session?>";
	document.apply.submit();
}

function apply_simpleprintwin() {
	document.apply.simple.value = "t";
	apply_printwin();
}

// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];
		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}

table.block2 {border-collapse:collapse;}
table.block2 td {border:#A0D25A solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#A0D25A solid 0px;}
table.block3 td {border-width:0;}

p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initcal();if(window.cas_load){cas_load();};if(window.no_7_load){no_7_load();};
<? if ($mode == "apply_printwin") { ?>
window.open('cas_application_workflow_detail_print.php?session=<?=$session?>&fname=<?=$fname?>&apply_id=<?=$apply_id?>&mode=apply_print&simple=<?=$simple?>', 'apply_print', 'width=640,height=700,scrollbars=yes');
<? } ?>
if (window.OnloadSub) { OnloadSub(); };resizeAllTextArea();">
<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="simple" value="">

<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>申請詳細</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<?
$mode = "";
show_application_workflow_detail($con, $session, $fname, $apply_id, $mode);
?>
</center>
</form>
</body>
</html>

<?
// 申請結果通知画面から呼ばれた場合
if($screen == "NOTICE")
{
// 確認済みフラグ更新
$obj->update_confirmed_flg($apply_id, $session);
?>
<script language='javascript'>
if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}
</script>
<?
}

pg_close($con);
?>



