<?php

include_once("../opt/kanrinishi_config.php");
include_once(APP_BASE . "controller/Import.php");
include_once(APP_BASE . "controller/Classification.php");
include_once(APP_BASE . "controller/FileClean.php");
if (DEBUG == "1") {
    ini_set("display_errors", "On");
    ini_set("error_reporting", E_ALL);
}
/**
 * 取込
 */
$import = new Import();
$import->Main($argv);
/**
 * 分類
 */
$classification = new Classification();
$classification->Main($argv);
/**
 * 古いファイルの削除
 */
$file = new FileClean();
$file->DeleteOldFile();
