<?php

include_once(APP_BASE . "controller/AppController.php");

/**
 * DB SAVER
 */
class DutyShiftReportDbSaver extends AppController {

    /**
     * 画面項目名
     * @var type 
     */
    private $_BasicItems;
    private $_ChangeInformationItems;
    private $_SeriousIllnessItems;
    private $_SeriousInformationItems;
    private $_WorkersInformationItems;
    private $_NightHospitalizationItems;
    private $_NightInformationItems;
    private $_NightPatroltimeItems;

    /**
     * 画面データ
     * @var type 
     */
    private $_BasicData;
    private $_ChargeInformationData;
    private $_SeriousIllnessData;
    private $_SeriousInformationData;
    private $_WorkersInformationData;
    private $_NightHospitalizationData;
    private $_NightInformationData;
    private $_NightPatroltimeData;

    /**
     * エラーメッセージ
     * @var type 
     */
    private $_ErrorMessage;

    /**
     * コンストラクタ
     */
    public function __construct() {

        parent::__construct();

        $this->Uses(
                array(
                    "DutyShiftReportBasic",
                    "DutyShiftReportChangeInformation",
                    "DutyShiftReportSeriousIllness",
                    "DutyShiftReportSeriousInformation",
                    "DutyShiftReportWorkersInformation",
                    "DutyShiftReportNightHospitalization",
                    "DutyShiftReportNightInformation",
                    "DutyShiftReportNightPatroltime",
                    "DutyShiftReportHistory",
                )
        );
        $this->setInputItems();
    }

    /**
     * 基本項目の設定
     * @param type $value
     */
    public function SetBasicData($value) {

        $this->_BasicData = $value;
    }

    public function SetChargeInformationData($value) {

        $this->_ChargeInformationData = $value;
    }

    public function SetSeriousIllnessData($value) {

        $this->_SeriousIllnessData = $value;
    }

    public function SetSeriousInformationData($value) {

        $this->_SeriousInformationData = $value;
    }

    public function SetWorkersInformationData($value) {

        $this->_WorkersInformationData = $value;
    }

    public function SetNightHospitalizationData($value) {

        $this->_NightHospitalizationData = $value;
    }

    public function SetNightInformationData($value) {

        $this->_NightInformationData = $value;
    }

    public function SetNightPatroltimeData($value) {

        $this->_NightPatroltimeData = $value;
    }

    /**
     * エラーメッセージ
     * @return type
     */
    public function GetErrorMessage() {

        return $this->_ErrorMessage;
    }

    /**
     * 入力項目名設定
     */
    private function setInputItems() {

        $inputItems = new DutyShiftReportInputItems();

        $this->_BasicItems = $inputItems->GetBasicItems();
        $this->_ChangeInformationItems = $inputItems->GetChangeInformationItems();
        $this->_SeriousIllnessItems = $inputItems->GetSeriousIllnessItems();
        $this->_SeriousInformationItems = $inputItems->GetSeriousInformationItems();
        $this->_WorkersInformationItems = $inputItems->GetWorkersInformationItems();
        $this->_NightHospitalizationItems = $inputItems->GetNightHospitalizationItems();
        $this->_NightInformationItems = $inputItems->GetNightInformationItems();
        $this->_NightPatroltimeItems = $inputItems->GetNightPatroltimeItems();
    }

    /**
     * 保存処理
     * @return boolean
     */
    public function Save($category) {

        $this->BeginTransaction();

        if (!$this->saveAll()) {
            $this->Rollback();
            return false;
        }

//        switch ($category) {
//            case "A":
//                if (!$this->saveAll()) {
//                    $this->Rollback();
//                    return false;
//                }
//                break;
//            case "1-1":
//            case "1-2":
//                if (!$this->saveReportBasic($category)) {
//                    $this->Rollback();
//                    return false;
//                }
//                break;
//            case "2":
//                if (!$this->saveReportChangeInformation()) {
//                    $this->Rollback();
//                    return false;
//                }
//                break;
//            case "3":
//                if (!$this->saveReportSriousIllness()) {
//                    $this->Rollback();
//                    return false;
//                }
//                break;
//            case "4":
//                if (!$this->saveReportWorkersInformation()) {
//                    $this->Rollback();
//                    return false;
//                }
//                break;
//            case "5":
//                if (!$this->saveReportNightHospitalization()) {
//                    $this->Rollback();
//                    return false;
//                }
//
//                if (!$this->saveReportNightInformation()) {
//                    $this->Rollback();
//                    return false;
//                }
//
//                if (!$this->saveReportNightPatroltime()) {
//                    $this->Rollback();
//                    return false;
//                }
//                break;
//        }

        if (!$this->saveReportHistory($category)) {
            $this->Rollback();
            return false;
        }

        $this->Commit();
        return true;
    }

    /**
     * 全項目保存
     * @return boolean
     */
    private function saveAll() {

        if (!$this->saveReportBasic("A")) {
            return false;
        }

        if (!$this->saveReportChangeInformation()) {
            return false;
        }

        if (!$this->saveReportSriousIllness()) {
            return false;
        }

        if (!$this->saveReportSeriousInformation()) {
            return false;
        }

        if (!$this->saveReportWorkersInformation()) {
            return false;
        }

        if (!$this->saveReportNightHospitalization()) {
            return false;
        }

        if (!$this->saveReportNightInformation()) {
            return false;
        }

        if (!$this->saveReportNightPatroltime()) {
            return false;
        }

        return true;
    }

    /**
     * 基本情報保存
     */
    private function saveReportBasic($category) {

        $data = $this->_BasicData;

        switch ($category) {
            case "1-1":
                $cond = Array(
                    "data" => Array(
                        "entry_name" => $data[0]["entry_name"],
                    //"day_shift_name" => $data[0]["day_shift_name"],
                    //"night_shift_name" => $data[0]["night_shift_name"],
                    ),
                    "conditions" => Array(
                        "shift_group" => $this->_BasicData[0]["shift_group"],
                        "report_date" => $this->_BasicData[0]["report_date"],
                    ),
                );
                break;
            case "1-2":
                $cond = Array(
                    "data" => Array(
                        "entry_name" => $data[0]["entry_name"],
                        "constant" => $data[0]["constant"],
                        "before_num" => $data[0]["before_num"],
                        "current_num" => $data[0]["current_num"],
                        "hostpitaladmissions_num" => $data[0]["hostpitaladmissions_num"],
                        "transference_num" => $data[0]["transference_num"],
                        "discharge_num" => $data[0]["discharge_num"],
                        "moving_out_num" => $data[0]["moving_out_num"],
                        "sleepover_num" => $data[0]["sleepover_num"],
                        "goingout_num" => $data[0]["goingout_num"],
                        "attendant_num" => $data[0]["attendant_num"],
                        "course_name1" => $data[0]["course_name1"],
                        "course_num1" => $data[0]["course_num1"],
                        "course_name2" => $data[0]["course_name2"],
                        "course_num2" => $data[0]["course_num2"],
                        "course_name3" => $data[0]["course_name3"],
                        "course_num3" => $data[0]["course_num3"],
                        "course_name4" => $data[0]["course_name4"],
                        "course_num4" => $data[0]["course_num4"],
                        "course_name5" => $data[0]["course_name5"],
                        "course_num5" => $data[0]["course_num5"],
                        "course_name6" => $data[0]["course_name6"],
                        "course_num6" => $data[0]["course_num6"],
                        //"course_name7" => $data[0]["course_name7"],
                        //"course_num7" => $data[0]["course_num7"],
                        "palliativecare_name" => $data[0]["palliativecare_name"],
                        "palliativecare_num" => $data[0]["palliativecare_num"],
                        "stretcher_num" => $data[0]["stretcher_num"],
                        "convoy_num" => $data[0]["convoy_num"],
                        "independent_num" => $data[0]["independent_num"],
                        "nursing_degree_a1" => $data[0]["nursing_degree_a1"],
                        "nursing_degree_a2" => $data[0]["nursing_degree_a2"],
                        "nursing_degree_a3" => $data[0]["nursing_degree_a3"],
                        "nursing_degree_a4" => $data[0]["nursing_degree_a4"],
                        "nursing_degree_b1" => $data[0]["nursing_degree_b1"],
                        "nursing_degree_b2" => $data[0]["nursing_degree_b2"],
                        "nursing_degree_b3" => $data[0]["nursing_degree_b3"],
                        "nursing_degree_b4" => $data[0]["nursing_degree_b4"],
                        "nursing_degree_c1" => $data[0]["nursing_degree_c1"],
                        "nursing_degree_c2" => $data[0]["nursing_degree_c2"],
                        "nursing_degree_c3" => $data[0]["nursing_degree_c3"],
                        "nursing_degree_c4" => $data[0]["nursing_degree_c4"],
                        "patient_status_name1" => $data[0]["patient_status_name1"],
                        "patient_status_num1" => $data[0]["patient_status_num1"],
                        "patient_status_name2" => $data[0]["patient_status_name2"],
                        "patient_status_num2" => $data[0]["patient_status_num2"],
                        "patient_status_name3" => $data[0]["patient_status_name3"],
                        "patient_status_num3" => $data[0]["patient_status_num3"],
                        "patient_status_name4" => $data[0]["patient_status_name4"],
                        "patient_status_num4" => $data[0]["patient_status_num4"],
                        "inspection_name1" => $data[0]["inspection_name1"],
                        "inspection_latenight1" => $data[0]["inspection_latenight1"],
                        "inspection_dayshift1" => $data[0]["inspection_dayshift1"],
                        "inspection_quasinight1" => $data[0]["inspection_quasinight1"],
                        "inspection_name2" => $data[0]["inspection_name2"],
                        "inspection_latenight2" => $data[0]["inspection_latenight2"],
                        "inspection_dayshift2" => $data[0]["inspection_dayshift2"],
                        "inspection_quasinight2" => $data[0]["inspection_quasinight2"],
                        "inspection_name3" => $data[0]["inspection_name3"],
                        "inspection_latenight3" => $data[0]["inspection_latenight3"],
                        "inspection_dayshift3" => $data[0]["inspection_dayshift3"],
                        "inspection_quasinight3" => $data[0]["inspection_quasinight3"],
                        "inspection_name4" => $data[0]["inspection_name4"],
                        "inspection_latenight4" => $data[0]["inspection_latenight4"],
                        "inspection_dayshift4" => $data[0]["inspection_dayshift4"],
                        "inspection_quasinight4" => $data[0]["inspection_quasinight4"],
                        "inspection_name5" => $data[0]["inspection_name5"],
                        "inspection_latenight5" => $data[0]["inspection_latenight5"],
                        "inspection_dayshift5" => $data[0]["inspection_dayshift5"],
                        "inspection_quasinight5" => $data[0]["inspection_quasinight5"],
                        "inspection_name6" => $data[0]["inspection_name6"],
                        "inspection_latenight6" => $data[0]["inspection_latenight6"],
                        "inspection_dayshift6" => $data[0]["inspection_dayshift6"],
                        "inspection_quasinight6" => $data[0]["inspection_quasinight6"],
                        "inspection_name7" => $data[0]["inspection_name7"],
                        "inspection_latenight7" => $data[0]["inspection_latenight7"],
                        "inspection_dayshift7" => $data[0]["inspection_dayshift7"],
                        "inspection_quasinight7" => $data[0]["inspection_quasinight7"],
                        "inspection_name8" => $data[0]["inspection_name8"],
                        "inspection_latenight8" => $data[0]["inspection_latenight8"],
                        "inspection_dayshift8" => $data[0]["inspection_dayshift8"],
                        "inspection_quasinight8" => $data[0]["inspection_quasinight8"],
                    ),
                    "conditions" => Array(
                        "shift_group" => $this->_BasicData[0]["shift_group"],
                        "report_date" => $this->_BasicData[0]["report_date"],
                    ),
                );
                break;
            default:
                $cond = Array(
                    "data" => Array(
                        "entry_name" => $data[0]["entry_name"],
                        //"day_shift_name" => $data[0]["day_shift_name"],
                        //"night_shift_name" => $data[0]["night_shift_name"],
                        "constant" => $data[0]["constant"],
                        "before_num" => $data[0]["before_num"],
                        "current_num" => $data[0]["current_num"],
                        "hostpitaladmissions_num" => $data[0]["hostpitaladmissions_num"],
                        "transference_num" => $data[0]["transference_num"],
                        "discharge_num" => $data[0]["discharge_num"],
                        "moving_out_num" => $data[0]["moving_out_num"],
                        "sleepover_num" => $data[0]["sleepover_num"],
                        "goingout_num" => $data[0]["goingout_num"],
                        "attendant_num" => $data[0]["attendant_num"],
                        "course_name1" => $data[0]["course_name1"],
                        "course_num1" => $data[0]["course_num1"],
                        "course_name2" => $data[0]["course_name2"],
                        "course_num2" => $data[0]["course_num2"],
                        "course_name3" => $data[0]["course_name3"],
                        "course_num3" => $data[0]["course_num3"],
                        "course_name4" => $data[0]["course_name4"],
                        "course_num4" => $data[0]["course_num4"],
                        "course_name5" => $data[0]["course_name5"],
                        "course_num5" => $data[0]["course_num5"],
                        "course_name6" => $data[0]["course_name6"],
                        "course_num6" => $data[0]["course_num6"],
                        //"course_name7" => $data[0]["course_name7"],
                        //"course_num7" => $data[0]["course_num7"],
                        "palliativecare_name" => $data[0]["palliativecare_name"],
                        "palliativecare_num" => $data[0]["palliativecare_num"],
                        "stretcher_num" => $data[0]["stretcher_num"],
                        "convoy_num" => $data[0]["convoy_num"],
                        "independent_num" => $data[0]["independent_num"],
                        "nursing_degree_a1" => $data[0]["nursing_degree_a1"],
                        "nursing_degree_a2" => $data[0]["nursing_degree_a2"],
                        "nursing_degree_a3" => $data[0]["nursing_degree_a3"],
                        "nursing_degree_a4" => $data[0]["nursing_degree_a4"],
                        "nursing_degree_b1" => $data[0]["nursing_degree_b1"],
                        "nursing_degree_b2" => $data[0]["nursing_degree_b2"],
                        "nursing_degree_b3" => $data[0]["nursing_degree_b3"],
                        "nursing_degree_b4" => $data[0]["nursing_degree_b4"],
                        "nursing_degree_c1" => $data[0]["nursing_degree_c1"],
                        "nursing_degree_c2" => $data[0]["nursing_degree_c2"],
                        "nursing_degree_c3" => $data[0]["nursing_degree_c3"],
                        "nursing_degree_c4" => $data[0]["nursing_degree_c4"],
                        "patient_status_name1" => $data[0]["patient_status_name1"],
                        "patient_status_num1" => $data[0]["patient_status_num1"],
                        "patient_status_name2" => $data[0]["patient_status_name2"],
                        "patient_status_num2" => $data[0]["patient_status_num2"],
                        "patient_status_name3" => $data[0]["patient_status_name3"],
                        "patient_status_num3" => $data[0]["patient_status_num3"],
                        "patient_status_name4" => $data[0]["patient_status_name4"],
                        "patient_status_num4" => $data[0]["patient_status_num4"],
                        "inspection_name1" => $data[0]["inspection_name1"],
                        "inspection_latenight1" => $data[0]["inspection_latenight1"],
                        "inspection_dayshift1" => $data[0]["inspection_dayshift1"],
                        "inspection_quasinight1" => $data[0]["inspection_quasinight1"],
                        "inspection_name2" => $data[0]["inspection_name2"],
                        "inspection_latenight2" => $data[0]["inspection_latenight2"],
                        "inspection_dayshift2" => $data[0]["inspection_dayshift2"],
                        "inspection_quasinight2" => $data[0]["inspection_quasinight2"],
                        "inspection_name3" => $data[0]["inspection_name3"],
                        "inspection_latenight3" => $data[0]["inspection_latenight3"],
                        "inspection_dayshift3" => $data[0]["inspection_dayshift3"],
                        "inspection_quasinight3" => $data[0]["inspection_quasinight3"],
                        "inspection_name4" => $data[0]["inspection_name4"],
                        "inspection_latenight4" => $data[0]["inspection_latenight4"],
                        "inspection_dayshift4" => $data[0]["inspection_dayshift4"],
                        "inspection_quasinight4" => $data[0]["inspection_quasinight4"],
                        "inspection_name5" => $data[0]["inspection_name5"],
                        "inspection_latenight5" => $data[0]["inspection_latenight5"],
                        "inspection_dayshift5" => $data[0]["inspection_dayshift5"],
                        "inspection_quasinight5" => $data[0]["inspection_quasinight5"],
                        "inspection_name6" => $data[0]["inspection_name6"],
                        "inspection_latenight6" => $data[0]["inspection_latenight6"],
                        "inspection_dayshift6" => $data[0]["inspection_dayshift6"],
                        "inspection_quasinight6" => $data[0]["inspection_quasinight6"],
                        "inspection_name7" => $data[0]["inspection_name7"],
                        "inspection_latenight7" => $data[0]["inspection_latenight7"],
                        "inspection_dayshift7" => $data[0]["inspection_dayshift7"],
                        "inspection_quasinight7" => $data[0]["inspection_quasinight7"],
                        "inspection_name8" => $data[0]["inspection_name8"],
                        "inspection_latenight8" => $data[0]["inspection_latenight8"],
                        "inspection_dayshift8" => $data[0]["inspection_dayshift8"],
                        "inspection_quasinight8" => $data[0]["inspection_quasinight8"],
                    ),
                    "conditions" => Array(
                        "shift_group" => $this->_BasicData[0]["shift_group"],
                        "report_date" => $this->_BasicData[0]["report_date"],
                    ),
                );
                break;
        }

        $result = $this->_Model["DutyShiftReportBasic"]->Save($cond);
        if (!$result) {
            $this->_ErrorMessage = $this->_Model["DutyShiftReportBasic"]->GetErrorMessage();
            return false;
        }

        return true;
    }

    /**
     * 入院・転入・退院・転出の保存
     * @return boolean
     */
    private function saveReportChangeInformation() {

        $data = $this->_ChargeInformationData;

        for ($i = 0; $i < count($data); $i++) {

            $cond = Array(
                "data" => Array(
                    "data_class" => $data[$i]["data_class"],
                    "department_name" => $data[$i]["department_name"],
                    "hour" => $data[$i]["hour"],
                    "patient_name" => $data[$i]["patient_name"],
                    "age" => $data[$i]["age"],
                    "sex" => $data[$i]["sex"],
                    "disease_name" => $data[$i]["disease_name"],
                    "attending_physician" => $data[$i]["attending_physician"],
                    "articles" => $data[$i]["articles"],
                ),
                "conditions" => Array(
                    "shift_group" => $this->_BasicData[0]["shift_group"],
                    "report_date" => $this->_BasicData[0]["report_date"],
                    "detail_no" => $i,
                ),
            );

            $result = $this->_Model["DutyShiftReportChangeInformation"]->Save($cond);
            if (!$result) {
                $this->_ErrorMessage = $this->_Model["DutyShiftReportChangeInformation"]->GetErrorMessage();
                return false;
            }
        }

        return true;
    }

    /**
     * 重症・要観察者・手術の保存
     * @return boolean
     */
    private function saveReportSriousIllness() {

        $data = $this->_SeriousIllnessData;

        for ($i = 0; $i < count($data); $i++) {

            $cond = Array(
                "data" => Array(
                    "data_class" => $data[$i]["data_class"],
                    "department_name" => $data[$i]["department_name"],
                    "hour" => $data[$i]["hour"],
                    "patient_name" => $data[$i]["patient_name"],
                    "age" => $data[$i]["age"],
                    "sex" => $data[$i]["sex"],
                    "articles" => $data[$i]["articles"],
                ),
                "conditions" => Array(
                    "shift_group" => $this->_BasicData[0]["shift_group"],
                    "report_date" => $this->_BasicData[0]["report_date"],
                    "detail_no" => $i,
                ),
            );

            $result = $this->_Model["DutyShiftReportSeriousIllness"]->Save($cond);
            if (!$result) {
                $this->_ErrorMessage = $this->_Model["DutyShiftReportSeriousIllness"]->GetErrorMessage();
                return false;
            }
        }

        return true;
    }

    /**
     * 重症_日勤帯の特記事項
     * @return boolean
     */
    private function saveReportSeriousInformation() {

        $data = $this->_SeriousInformationData;

        $cond = Array(
            "data" => Array(
                "articles" => $data[0]["articles"],
            ),
            "conditions" => Array(
                "shift_group" => $this->_BasicData[0]["shift_group"],
                "report_date" => $this->_BasicData[0]["report_date"],
            ),
        );

        $result = $this->_Model["DutyShiftReportSeriousInformation"]->Save($cond);
        if (!$result) {
            $this->_ErrorMessage = $this->_Model["DutyShiftReportSeriousInformation"]->GetErrorMessage();
            return false;
        }

        return true;
    }

    /**
     * 勤務者情報の保存
     * @return boolean
     */
    private function saveReportWorkersInformation() {

        $data = $this->_WorkersInformationData;

        for ($i = 0; $i < count($data); $i++) {

            $cond = Array(
                "data" => Array(
                    "data_class" => $data[$i]["data_class"],
                    "job_category" => $data[$i]["job_category"],
                    "staff_name" => $data[$i]["staff_name"],
                ),
                "conditions" => Array(
                    "shift_group" => $this->_BasicData[0]["shift_group"],
                    "report_date" => $this->_BasicData[0]["report_date"],
                    "detail_no" => $i,
                ),
            );

            $result = $this->_Model["DutyShiftReportWorkersInformation"]->Save($cond);
            if (!$result) {
                $this->_ErrorMessage = $this->_Model["DutyShiftReportWorkersInformation"]->GetErrorMessage();
                return false;
            }
        }

        return true;
    }

    /**
     * 夜間入退院の保存
     * @return boolean
     */
    private function saveReportNightHospitalization() {

        $data = $this->_NightHospitalizationData;

        for ($i = 0; $i < count($data); $i++) {

            $cond = Array(
                "data" => Array(
                    "data_class" => $data[$i]["data_class"],
                    "department_name" => $data[$i]["department_name"],
                    "hour" => $data[$i]["hour"],
                    "patient_name" => $data[$i]["patient_name"],
                    "age" => $data[$i]["age"],
                    "disease_name" => $data[$i]["disease_name"],
                    "attending_physician" => $data[$i]["attending_physician"],
                ),
                "conditions" => Array(
                    "shift_group" => $this->_BasicData[0]["shift_group"],
                    "report_date" => $this->_BasicData[0]["report_date"],
                    "detail_no" => $i,
                ),
            );

            $result = $this->_Model["DutyShiftReportNightHospitalization"]->Save($cond);
            if (!$result) {
                $this->_ErrorMessage = $this->_Model["DutyShiftReportNightHospitalization"]->GetErrorMessage();
                return false;
            }
        }

        return true;
    }

    /**
     * 夜間の特記・管理事項の保存
     * @return boolean
     */
    private function saveReportNightInformation() {

        $data = $this->_NightInformationData;

        $cond = Array(
            "data" => Array(
                "articles" => $data[0]["articles"],
            ),
            "conditions" => Array(
                "shift_group" => $this->_BasicData[0]["shift_group"],
                "report_date" => $this->_BasicData[0]["report_date"],
            ),
        );

        $result = $this->_Model["DutyShiftReportNightInformation"]->Save($cond);
        if (!$result) {
            $this->_ErrorMessage = $this->_Model["DutyShiftReportNightInformation"]->GetErrorMessage();
            return false;
        }

        return true;
    }

    /**
     * 巡視時間の保存
     * @return boolean
     */
    private function saveReportNightPatroltime() {

        $data = $this->_NightPatroltimeData;

        for ($i = 0; $i < count($data); $i++) {

            $cond = Array(
                "data" => Array(
                    "data_class" => $data[$i]["data_class"],
                    "hour" => $data[$i]["hour"],
                    //"staff_name" => $data[$i]["staff_name"],
                    "abnormality" => $data[$i]["abnormality"],
                    "articles" => $data[$i]["articles"],
                ),
                "conditions" => Array(
                    "shift_group" => $this->_BasicData[0]["shift_group"],
                    "report_date" => $this->_BasicData[0]["report_date"],
                    "detail_no" => $i,
                ),
            );

            $result = $this->_Model["DutyShiftReportNightPatroltime"]->Save($cond);
            if (!$result) {
                $this->_ErrorMessage = $this->_Model["DutyShiftReportNightPatroltime"]->GetErrorMessage();
                return false;
            }
        }

        return true;
    }

    /**
     * 履歴テーブル保存
     * @param type $category
     * @return boolean
     */
    private function saveReportHistory($category) {

        $cond = Array(
            "data" => Array(
                "entry_name" => $this->_BasicData[0]["entry_name"],
            ),
            "conditions" => Array(
                "shift_group" => $this->_BasicData[0]["shift_group"],
                "report_date" => $this->_BasicData[0]["report_date"],
                "history_date" => date("Y/m/d H:i:s", time()),
            ),
        );

        $result = $this->_Model["DutyShiftReportHistory"]->Save($cond);
        if (!$result) {
            $this->_ErrorMessage = $this->_Model["DutyShiftReportHistory"]->GetErrorMessage();
            return false;
        }

        return true;
    }

}
