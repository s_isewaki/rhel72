<?php

include_once(APP_BASE . "controller/AppController.php");
include_once(APP_BASE . "controller/ClassificationTypeA.php");
include_once(APP_BASE . "controller/ClassificationTypeB.php");
include_once(APP_BASE . "controller/ClassificationTypeC.php");
include_once(APP_BASE . "controller/ClassificationTypeF.php");

/**
 * 分類
 */
class Classification extends AppController {

    private $_ResultHeader;
    private $_ClassificationTypeA;
    private $_ClassificationTypeB;
    private $_ClassificationTypeC;
    private $_ClassificationTypeF;

    /**
     * メイン
     */
    public function Main($argv) {

        $this->_Logger->PutInfo("処理を開始しました。");

        $this->initialize($argv);

        /**
         * import_header取得
         */
        if (!$this->getImportHeader()) {
            $this->_Logger->PutInfo("処理するデータがありませんでした。");
            return false;
        }

        /**
         * 更新処理
         */
        if (!$this->registDb()) {
            $this->_Logger->PutErr("更新に失敗しました。");
            return false;
        }

        $this->_Logger->PutInfo("処理を終了しました。");

        return true;
    }

    /**
     * 初期処理
     */
    private function initialize($argv) {

        $this->_ProgramName = $argv[0];
        $this->Uses(
                array(
                    "DutyShiftImportHeader",
                )
        );

        $this->_ClassificationTypeA = new ClassificationTypeA($argv[0]);
        $this->_ClassificationTypeB = new ClassificationTypeB($argv[0]);
        $this->_ClassificationTypeC = new ClassificationTypeC($argv[0]);
        $this->_ClassificationTypeF = new ClassificationTypeF($argv[0]);
    }

    /**
     * ヘッダの取得
     */
    private function getImportHeader() {

        $cond = Array(
            "fields" => Array(
                "file_name" => "file_name",
                "send_date" => "send_date",
                "send_time" => "send_time",
                "data_type" => "data_type",
                "proc_class" => "proc_class",
                "patient_cd" => "patient_cd",
                "status" => "status",
            ),
            "conditions" => Array(
                "status" => "0",
            ),
            "order" => Array(
                "file_name" => "ASC",
            )
        );
        $resultHeader = $this->_Model["DutyShiftImportHeader"]->find($cond);
        if (!$resultHeader) {
            return false;
        }

        $this->_ResultHeader = $resultHeader;

        return true;
    }

    /**
     * DB登録
     */
    private function registDb() {

        foreach ($this->_ResultHeader as $row) {
            switch ($row["proc_class"]) {
                case "1":
                    $this->addDb($row);
                    break;
                case "9":
                    $this->deleteDb($row);
                    break;
            }
        }

        return true;
    }

    /**
     * DB追加
     */
    private function addDb($row) {

        $this->_Logger->PutInfo("ADD:");
        $this->_Logger->PutInfo("file_name:" . $row["file_name"]);
        $this->_Logger->PutInfo("send_date:" . $row["send_date"]);
        $this->_Logger->PutInfo("send_time:" . $row["send_time"]);
        $this->_Logger->PutInfo("data_type:" . $row["data_type"]);

        switch ($row["data_type"]) {
            case "A":
                if (!$this->_ClassificationTypeA->AddTypeADb($row)) {
                    return false;
                }
                break;
            case "B":
                if (!$this->_ClassificationTypeB->AddTypeBDb($row)) {
                    return false;
                }
                break;
            case "C":
                if (!$this->_ClassificationTypeC->AddTypeCDb($row)) {
                    return false;
                }
                break;
            case "F":
                if (!$this->_ClassificationTypeF->AddTypeFDb($row)) {
                    return false;
                }
                break;
        }

        return true;
    }

    /**
     * DB削除
     */
    private function deleteDb($row) {

        $this->_Logger->PutInfo("DEL:");
        $this->_Logger->PutInfo("file_name:" . $row["file_name"]);
        $this->_Logger->PutInfo("send_date:" . $row["send_date"]);
        $this->_Logger->PutInfo("send_time:" . $row["send_time"]);
        $this->_Logger->PutInfo("data_type:" . $row["data_type"]);

        switch ($row["data_type"]) {
            case "A":
                if (!$this->_ClassificationTypeA->DeleteTypeADb($row)) {
                    return false;
                }
                break;
            case "B":
                if (!$this->_ClassificationTypeB->DeleteTypeBDb($row)) {
                    return false;
                }
                break;
            case "C":
                if (!$this->_ClassificationTypeC->DeleteTypeCDb($row)) {
                    return false;
                }
                break;
            case "F":
                if (!$this->_ClassificationTypeF->DeleteTypeFDb($row)) {
                    return false;
                }
                break;
        }

        return true;
    }

}
