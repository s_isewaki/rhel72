<?php

include_once(APP_BASE . "controller/AppController.php");

class DutyShiftReportImporterNightPatroltime extends AppController {

    private $_BasicData;                //基本情報データ
    private $_NightPatroltime;          //巡視時間データ
    private $_NightPatroltimeItems;     //巡視時間項目

    /**
     * コンストラクタ
     */

    public function __construct() {

        parent::__construct();

        $this->Uses(
                array(
                    "DutyShiftReportNightPatroltime",
                )
        );
        $inputItems = new DutyShiftReportInputItems();
        $this->_NightPatroltimeItems = $inputItems->GetNightPatroltimeItems();
    }

    /**
     * 基本情報の設定
     * @param type $value
     */
    public function SetBasicData($value) {

        $this->_BasicData = $value;
    }

    /**
     * インポート
     */
    public function Import() {

        $this->initializeData();
        $datas = $this->getData();
        if (!$datas) {
            return $this->_NightPatroltime;
        }

        $i = 0;
        foreach ($datas as $data) {
            $this->_NightPatroltime[$i]["data_class"] = $data["data_class"];
            $this->_NightPatroltime[$i]["hour"] = $data["hour"];
            //$this->_NightPatroltime[$i]["abnormality"] = $data["abnormality"];
            $i++;
        }

        return $this->_NightPatroltime;
    }

    /**
     * データの初期化
     */
    private function initializeData() {

        $this->_NightPatroltime = array();

        for ($i = 0; $i < LINE_NIGHT_PATROLTIME; $i++) {
            foreach ($this->_NightPatroltimeItems as $item) {
                $this->_NightPatroltime[$i][$item] = "";
            }
        }
    }

    /**
     * 
     */
    private function getData() {

        $cond = Array(
            "conditions" => Array(
                "shift_group" => $this->_BasicData[0]["shift_group"],
                "report_date" => $this->_BasicData[0]["before_date"],
            ),
            "order" => Array(
                "detail_no" => "ASC",
            )
        );

        foreach ($this->_NightPatroltimeItems as $item) {
            $cond["fields"][$item] = $item;
        }

        return $this->_Model["DutyShiftReportNightPatroltime"]->Find($cond);
    }

}
