<?php

include_once(APP_BASE . "controller/AppController.php");
include_once(APP_BASE . "util/FileSystem.php");

/**
 * ファイル整理クラス
 */
class FileClean extends AppController {

    /**
     * 古いファイルの削除
     */
    public function DeleteOldFile() {

        FileSystem::DeleteFileDay(SAVE_PATH, DELETE_DAY);
        FileSystem::DeleteFileDay(LOG_PATH, DELETE_DAY);
    }

}
