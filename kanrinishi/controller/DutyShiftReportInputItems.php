<?php

include_once(APP_BASE . "controller/AppController.php");

/**
 * 入力項目名
 */
class DutyShiftReportInputItems extends AppController {

    /**
     * コンストラクタ
     */
    public function __construct() {

        parent::__construct();
    }

    /**
     * 基本項目名取得
     */
    public function GetBasicItems() {

        return array(
            "shift_group",
            "report_date",
            "entry_name",
            "constant",
            "before_num",
            "current_num",
            "hostpitaladmissions_num",
            "transference_num",
            "discharge_num",
            "moving_out_num",
            "sleepover_num",
            "goingout_num",
            "attendant_num",
            "course_name1",
            "course_num1",
            "course_name2",
            "course_num2",
            "course_name3",
            "course_num3",
            "course_name4",
            "course_num4",
            "course_name5",
            "course_num5",
            "course_name6",
            "course_num6",
            "palliativecare_name",
            "palliativecare_num",
            "stretcher_num",
            "convoy_num",
            "independent_num",
            "nursing_degree_a1",
            "nursing_degree_a2",
            "nursing_degree_a3",
            "nursing_degree_a4",
            "nursing_degree_b1",
            "nursing_degree_b2",
            "nursing_degree_b3",
            "nursing_degree_b4",
            "nursing_degree_c1",
            "nursing_degree_c2",
            "nursing_degree_c3",
            "nursing_degree_c4",
            "patient_status_name1",
            "patient_status_num1",
            "patient_status_name2",
            "patient_status_num2",
            "patient_status_name3",
            "patient_status_num3",
            "patient_status_name4",
            "patient_status_num4",
            "inspection_name1",
            "inspection_latenight1",
            "inspection_dayshift1",
            "inspection_quasinight1",
            "inspection_name2",
            "inspection_latenight2",
            "inspection_dayshift2",
            "inspection_quasinight2",
            "inspection_name3",
            "inspection_latenight3",
            "inspection_dayshift3",
            "inspection_quasinight3",
            "inspection_name4",
            "inspection_latenight4",
            "inspection_dayshift4",
            "inspection_quasinight4",
            "inspection_name5",
            "inspection_latenight5",
            "inspection_dayshift5",
            "inspection_quasinight5",
            "inspection_name6",
            "inspection_latenight6",
            "inspection_dayshift6",
            "inspection_quasinight6",
            "inspection_name7",
            "inspection_latenight7",
            "inspection_dayshift7",
            "inspection_quasinight7",
            "inspection_name8",
            "inspection_latenight8",
            "inspection_dayshift8",
            "inspection_quasinight8",
        );
    }

    /**
     * 移動情報項目名取得
     */
    public function GetChangeInformationItems() {

        return array(
            "data_class",
            "department_name",
            "hour",
            "patient_name",
            "age",
            "sex",
            "disease_name",
            "attending_physician",
            "articles",
        );
    }

    /**
     * 移動情報項目名取得
     */
    public function GetChangeInformationSearchItems() {

        return array(
            "check",
            "data_class",
            "department_name",
            "hour",
            "patient_name",
            "age",
            "sex",
            "disease_name",
            "attending_physician",
            "articles",
        );
    }

      /**
     * 移動情報項目名取得 インポート時項目比較用
     */
    public function GetChangeInformationCompItems() {

        return array(
            "data_class",
            "hour",
            "patient_name",
            "age",
        );
    }
    
    /**
     * 重症情報項目名取得
     */
    public function GetSeriousIllnessItems() {

        return array(
            "data_class",
            "department_name",
            "hour",
            "patient_name",
            "age",
            "sex",
            "articles",
        );
    }
    
    /**
     * 重症情報 日勤帯の特記事項 項目名取得
     * @return type
     */
   public function GetSeriousInformationItems() {

        return array(
            "articles",
        );
    }
    /**
     * 重症情報項目名取得
     */
    public function GetSeriousIllnessSearchItems() {

        return array(
            "check",
            "data_class",
            "department_name",
            "hour",
            "patient_name",
            "age",
            "sex",
            "articles",
        );
    }

    /**
     * 重症情報項目名取得
     */
    public function GetSeriousIllnessKeyItems() {

        return array(
            "patient_cd",
            "patient_name",
        );
    }

    /**
     * 
     * @return type
     */
    public function GetSeriousIllnessResultItems() {

        return array(
            "patient_cd",
            "patient_name_kana",
            "patient_name",
            "age",
            "sex",
            "sex_name",
        );
    }

    /**
     * 勤務者情報項目名取得
     */
    public function GetWorkersInformationItems() {

        return array(
            "data_class",
            "job_category",
            "staff_name",
        );
    }

    /**
     * 夜間入退院情報
     */
    public function GetNightHospitalizationItems() {

        return array(
            "data_class",
            "department_name",
            "hour",
            "patient_name",
            "age",
            "disease_name",
            "attending_physician",
        );
    }

    /**
     * 夜間入退院情報
     */
    public function GetNightHospitalizationSearchItems() {

        return array(
            "check",
            "data_class",
            "department_name",
            "hour",
            "patient_name",
            "age",
            "disease_name",
            "attending_physician",
        );
    }

      /**
     * 夜間入退院情報
     */
    public function GetNightHospitalizationCompItems() {

        return array(
            "data_class",
            "hour",
            "patient_name",
            "age",
        );
    }
    /**
     * 夜間入退院情報
     */
    public function GetNightInformationItems() {

        return array(
            "articles",
        );
    }

    /**
     * 巡視時間情報
     */
    public function GetNightPatroltimeItems() {

        return array(
            "data_class",
            "hour",
            //"staff_name",
            "abnormality",
            "articles",
        );
    }

}
