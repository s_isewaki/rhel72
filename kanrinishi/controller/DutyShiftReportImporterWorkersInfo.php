<?php

include_once(APP_BASE . "controller/AppController.php");
include_once(APP_BASE . "controller/DutyShiftReportInputItems.php");

/**
 * 勤務者情報データ取込クラス
 */
class DutyShiftReportImporterWorkersInfo extends AppController {

    private $_BasicData;    //基本情報データ
    private $_ErrorMessage = "";

    /**
     * コンストラクタ
     */
    public function __construct() {

        parent::__construct();

        $this->Uses(
                array(
                    "DutyShiftPlanStaff",
                )
        );
    }

    /**
     * エラーメッセージの取得
     * @return type
     */
    public function GetErrorMessage() {

        return $this->_ErrorMessage;
    }

    /**
     * 基本情報の設定
     * @param type $value
     */
    public function SetBasicData($value) {

        $this->_BasicData = $value;
    }

    /**
     * 勤務者情報のインポート
     * [emp_id] => 120703157869
     * [emp_name] => 第一外科B子
     * [job_name] => 看護師
     * [st_name] => 部長
     * [ptn_name] => 日勤
     * ------------------------------
     * シフトグループ shift_group
     * 年月日 report_date
     * 明細番号 detail_no
     * 区分 data_class
     * 職種 job_category
     * 職員氏名 staff_name
     */
    public function Import() {

        $workersInfo = $this->_Model["DutyShiftPlanStaff"]->GetWorkersInfo(
                $this->_BasicData[0]["shift_group"], $this->_BasicData[0]["report_date_str"]);

        /**
         * 全体をクリア
         */
        $data = null;
        for ($j = 0; $j < LINE_WORKERS_INFORMATION; $j++) {
            $data[$j]["data_class"] = "";
            $data[$j]["job_category"] = "";
            $data[$j]["staff_name"] = "";
        }

        $i = 0;
        $j = 46;
        foreach ($workersInfo as $info) {
            //下記で勤務と休暇を分岐させる
            //ptn_id = "10"が休暇それ以外は勤務
            if ($info["ptn_id"] != "10") {
                if ($i >= 46) {
                    $i++;
                    continue;
                }
                $data[$i]["data_class"] = $info["ptn_name"];
                $data[$i]["job_category"] = $info["job_name"];
                $data[$i]["staff_name"] = $info["emp_name"];
                $i++;
            } else {
                if ($j >= 69) {
                    $j++;
                    continue;
                }
                $data[$j]["data_class"] = $info["ptn_name"];
                $data[$j]["job_category"] = $info["job_name"];
                $data[$j]["staff_name"] = $info["emp_name"];
                $j++;
            }
        }

//        echo "i=" . $i . "<br />";
//        echo "j=" . $j . "<br />";

        if ($i > 46) {
            $this->_ErrorMessage .= "勤務職員数が、46人を超えたため、取り込めない情報が発生しました。<br />";
        }
        if ($j > 69) {
            $this->_ErrorMessage .= "休暇職員数が、23人を超えたため、取り込めない情報が発生しました。<br />";
        }

        return $data;
    }

}
