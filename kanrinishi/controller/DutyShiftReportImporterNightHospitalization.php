<?php

include_once(APP_BASE . "controller/AppController.php");
include_once(APP_BASE . "util/DateConverter.php");

/**
 * データ取込クラス
 * 夜間入退院
 */
class DutyShiftReportImporterNightHospitalization extends AppController {

    private $_BasicData;                        //基本情報データ
    private $_NightHospitalization;             //夜間入退院データ
    private $_NightHospitalizationItems;        //夜間入退院項目
    private $_NightHospitalizationSearchItems;  //夜間入退院項目
    private $_ErrorMessage = "";
    private $_NightHospitalizationCompItems;

    /**
     * コンストラクタ
     * 
     */
    public function __construct() {

        parent::__construct();

        $this->Uses(
                array(
                    "DutyShiftImportPatientWard",
                )
        );
        $inputItems = new DutyShiftReportInputItems();
        $this->_NightHospitalizationItems = $inputItems->GetNightHospitalizationItems();
        $this->_NightHospitalizationSearchItems = $inputItems->GetNightHospitalizationSearchItems();
        $this->_NightHospitalizationCompItems = $inputItems->GetNightHospitalizationCompItems();
    }

    /**
     * エラーメッセージの取得
     * @return type
     */
    public function GetErrorMessage() {

        return $this->_ErrorMessage;
    }

    /**
     * 基本情報の設定
     * @param type $value
     */
    public function SetBasicData($value) {

        $this->_BasicData = $value;
    }

    /**
     * 基本情報の設定
     * @param type $value
     */
    public function SetNightHospitalizationData($value) {

        $this->_NightHospitalization = $value;
    }

    /**
     * インポート
     */
    public function Import() {

        $this->initializeData();
        $datas = $this->getData();
        if (!$datas) {
            return $this->_NightHospitalization;
        }

        $i = 0;
        foreach ($datas as $data) {
            $this->_NightHospitalization[$i]["data_class"] = $data["datatype_name"];
            $this->_NightHospitalization[$i]["department_name"] = $this->getDepatmentName($data["department_cd"]);
            $this->_NightHospitalization[$i]["hour"] = DateConverter::AddCol($data["move_time"]);
            $this->_NightHospitalization[$i]["patient_name"] = $data["name_kanji"];
            $this->_NightHospitalization[$i]["age"] = DateConverter::CalcAge($data["birthday"]);
            $this->_NightHospitalization[$i]["sex"] = $data["sex"];
            $this->_NightHospitalization[$i]["disease_name"] = $data["disease_name"];
            $this->_NightHospitalization[$i]["attending_physician"] = $data["attending_physician"];
            $this->_NightHospitalization[$i]["articles"] = "";
            $i++;
        }

        return $this->_NightHospitalization;
    }

    /**
     * 一括取込
     */
    public function ImportAll() {

        $data = array();
        for ($j = 0; $j < LINE_NIGHT_HOSPITALIZATION; $j++) {
            foreach ($this->_NightHospitalizationItems as $item) {
                $data[$j][$item] = "";
            }
        }

        $j = 0;
        foreach ($this->_NightHospitalization as $nhData) {
            $notEmpty = false;
            foreach ($this->_NightHospitalizationItems as $item) {
                if ($nhData[$item] != "") {
                    $notEmpty = true;
                }
            }
            if (!$notEmpty) {
                break;
            }
            foreach ($this->_NightHospitalizationItems as $item) {
                if (isset($nhData[$item])) {
                    $data[$j][$item] = $nhData[$item];
                }
            }
            $j++;
        }

        $importData = $this->Import();
        for ($i = 0; $i < count($importData); $i++) {

            $notEmpty = false;
            foreach ($this->_NightHospitalizationItems as $item) {
                if ($importData[$i][$item] != "") {
                    $notEmpty = true;
                }
            }
            if (!$notEmpty) {
                break;
            }

            foreach ($this->_NightHospitalizationItems as $item) {
                $data[$j][$item] = $importData[$i][$item];
            }
            $j++;
        }

        if (count($data) > LINE_NIGHT_HOSPITALIZATION) {
            $this->_ErrorMessage = "夜間入退院情報が10件を超えたため、取り込めない情報が発生しました";
        }

        return $data;
    }

    /**
     * 
     * @param type $searchData
     */
    public function ImportPreviewReg($searchData) {

        /**
         * 初期化を行う
         */
        $data = array();
        for ($j = 0; $j < LINE_NIGHT_HOSPITALIZATION; $j++) {
            foreach ($this->_NightHospitalizationSearchItems as $item) {
                $data[$j][$item] = "";
            }
        }

        /**
         * 既存の内容を読み込む
         */
        $j = 0;
        foreach ($this->_NightHospitalization as $nhData) {
//            $notEmpty = false;
//            foreach ($this->_NightHospitalizationItems as $item) {
//                if ($nhData[$item] != "") {
//                    $notEmpty = true;
//                }
//            }
//            if (!$notEmpty) {
//                break;
//            }
            foreach ($this->_NightHospitalizationSearchItems as $item) {
                if (isset($nhData[$item])) {
                    $data[$j][$item] = $nhData[$item];
                }
            }
            $j++;
        }

        for ($i = 0; $i < count($searchData); $i++) {
            if ($searchData[$i]["check"] == "1") {

                //対象とするものが空行であれば飛ばす
                $isEmpty = true;
                foreach ($this->_NightHospitalizationSearchItems as $item) {
                    if ($searchData[$i][$item] != "") {
                        $isEmpty = false;
                    }
                }
                if ($isEmpty) {
                    continue;
                }

                //対象とするものと同じデータがあれば飛ばす
                foreach ($this->_NightHospitalization as $nhData) {
                    $isSame = true;
                    foreach ($this->_NightHospitalizationCompItems as $item) {
                        if ($searchData[$i][$item] != $nhData[$item]) {
                            $isSame = false;
                        }
                    }
                    if ($isSame) {
                        continue 2;
                    }
                }

                //空白行を探す
                $j = 0;
                foreach ($data as $nhData) {
                    $isEmpty = true;
                    foreach ($this->_NightHospitalizationItems as $item) {
                        if ($nhData[$item] != "") {
                            $isEmpty = false;
                        }
                    }
                    if ($isEmpty) {
//                        echo $j;
                        break;
                    }
                    $j++;
                }

                foreach ($this->_NightHospitalizationSearchItems as $item) {
                    $data[$j][$item] = $searchData[$i][$item];
                }
                $j++;
            }
        }

        if (count($data) > LINE_NIGHT_HOSPITALIZATION) {
            $this->_ErrorMessage = "夜間入退院情報が10件を超えたため、取り込めない情報が発生しました";
        }

        return $data;
    }

    /**
     * データの初期化
     */
    private function initializeData() {

        $this->_NightHospitalization = array();
        for ($j = 0; $j < LINE_NIGHT_HOSPITALIZATION; $j++) {
            foreach ($this->_NightHospitalizationItems as $item)
                $this->_NightHospitalization[$j][$item] = "";
        }
    }

    /**
     * 診療科目名の取得
     * @param type $key
     */
    private function getDepatmentName($key) {

        $department = $GLOBALS['Department'];
        if (isset($department[$key])) {
            return $department[$key];
        }

        return false;
    }

    /**
     * 移動情報
     */
    private function getData() {

        $tagetDate = $this->_BasicData[0]["report_date_str"];
        $wardCd = $this->_BasicData[0]["sfc_group_cd"];
        return $this->_Model["DutyShiftImportPatientWard"]->ImportNightHospitalization($tagetDate, $wardCd);
    }

}
