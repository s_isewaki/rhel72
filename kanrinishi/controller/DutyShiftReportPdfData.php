<?php

require_once(APP_BASE . '../fpdf153/mbfpdf.php');

/**
 * REPORT PDF DATA
 *
 * @author master
 */
class DutyShiftReportPdfData {

    private $_Pdf;                      //PDFオブジェクト
    private $_LeftMargin = 5;           //左マージン
    private $_CellWidth = 9;            //セルの幅
    private $_Height = 3.9;             //セルの高さ
    private $_Border = "1";             //境界線あり
    private $_BodyFirstY = 22;          //明細部の開始位置
    private $_HeaderData;               //ヘッダデータ
    private $_BasicData;                //基本情報
    private $_ChangeInformationData;    //移動情報
    private $_SeriousIllnessData;       //重症者情報
    private $_SeriousInformationData;   //日勤帯の特記事項
    private $_WorkersInformationData;   //勤務者
    private $_NightHospitalizationData; //夜間入退院情報
    private $_NightInformationData;     //夜間情報
    private $_NightPatroltimeData;      //巡視情報

    public function __construct() {

        $this->_Pdf = new MBFPDF('L', 'mm', 'A4');
        $this->_Pdf->SetAutoPageBreak(true, 5);
        $this->_Pdf->AddMBFont(MINCHO, 'EUC-JP');
        $this->_Pdf->AddMBFont(GOTHIC, 'EUC-JP');
        $this->_Pdf->Open();
    }

    /**
     * 基本情報
     * @param type $data
     */
    public function setHeaderData($data) {

        $this->_HeaderData = $data;
    }

    /**
     * 基本情報
     * @param type $data
     */
    public function setBasicData($data) {

        $this->_BasicData = $data;
    }

    /**
     * 移動情報
     * @param type $data
     */
    public function setChangeInformationData($data) {

        $this->_ChangeInformationData = $data;
    }

    /**
     * 重症者情報
     * @param type $data
     */
    public function setSeriousIllnessData($data) {

        $this->_SeriousIllnessData = $data;
    }

    /**
     * 重症者情報 日勤帯の特記事項
     * @param type $data
     */
    public function setSeriousInformationData($data) {

        $this->_SeriousInformationData = $data;
    }

    /**
     * 勤務者情報
     * @param type $data
     */
    public function setWorkersInformationData($data) {

        $this->_WorkersInformationData = $data;
    }

    /**
     * 夜間入退院
     * @param type $data
     */
    public function setNightHospitalizationData($data) {

        $this->_NightHospitalizationData = $data;
    }

    /**
     * 夜間情報
     * @param type $data
     */
    public function setNightInformationData($data) {

        $this->_NightInformationData = $data;
    }

    /**
     * 巡視時間
     * @param type $data
     */
    public function setNightPatroltimeData($data) {

        $this->_NightPatroltimeData = $data;
    }

    /**
     * 作成
     */
    public function Make() {

        $this->makeHead();
        $this->makeBody();
        $this->output();
    }

    /**
     * ヘッダの描画
     */
    private function makeHead() {

        $this->_Pdf->AddPage();

        //ページヘッダ
        $this->makePageHead();

        //基本情報
        $this->makeBasicHead();

        //入院・転入　退院・転出
        $this->makeChangeInformationHead();

        //重症・要観察・手術
        $this->makeSeriousIllnessHead();

        //勤務者情報
        $this->makeWorkersInformationHead();

        //夜間入退院
        $this->makeNightHospitalizationHead();

        //管理事項
        $this->makeNightInformationHead();
    }

    /**
     * ページヘッダの描画
     */
    private function makePageHead() {

        //ページタイトル
        $this->_Pdf->setXY($this->_LeftMargin, 5);
        $this->_Pdf->SetFont(GOTHIC, '', 12); //GOTHIC,MINCHO
        $this->_Pdf->Cell(0, 9, '病棟管理日誌', '', 0, 'L');

        //ヘッダ部
//        $this->_Pdf->SetFont(GOTHIC, '', 9);
//        $this->_Pdf->SetXY($this->_LeftMargin, 17);
//        $this->_Pdf->Cell($this->_CellWidth * 3, 4, '病棟', '0', 0, 'L');
//        $this->_Pdf->Cell($this->_CellWidth * 4, 4, '年月日', '0', 0, 'L');
//        $this->_Pdf->Cell($this->_CellWidth * 4, 4, '日勤責任者：', '0', 0, 'L');
//        $this->_Pdf->Cell($this->_CellWidth * 4, 4, '夜勤責任者：', '0', 0, 'L');
        //印鑑部ヘッダ
        $this->_Pdf->SetFont(GOTHIC, '', 7);
        $this->_Pdf->SetXY(248, 10.3);
        $this->makeHeadCell(3, "総看護長");
        $this->makeHeadCell(2, "看護長");

        //印鑑部枠
        $this->_Pdf->SetXY(248, 14.2);
        $this->_Pdf->SetFillColor(255, 255, 255);
        $this->_Pdf->Cell($this->_CellWidth * 3, $this->_Height * 2, '', $this->_Border, 0, 'C', 1);
        $this->_Pdf->Cell($this->_CellWidth * 2, $this->_Height * 2, '', $this->_Border, 0, 'C', 1);
    }

    /**
     * 基本情報
     */
    private function makeBasicHead() {

        //患者数等
        $this->makeHeadVCell(1, 1, 0.5, 9, "");
        $this->makeHeadVCellStr("患　者　数　等", 1, 1);

        $headArr = array(
            array(
                'line' => 1,
                array('name' => '定数', 'width' => 1),
                array('name' => '前日数', 'width' => 1),
                array('name' => '現在数', 'width' => 1),
                array('name' => '入院', 'width' => 1),
                array('name' => '転入', 'width' => 1),
                array('name' => '退院', 'width' => 1),
                array('name' => '転出', 'width' => 1),
                array('name' => '外泊', 'width' => 1),
                array('name' => '外出', 'width' => 1),
                array('name' => '付添', 'width' => 1),
                array('name' => '物品点検', 'width' => 3),
                array('name' => '深夜', 'width' => 1),
                array('name' => '日勤', 'width' => 1),
                array('name' => '準夜', 'width' => 1),
            ),
            array(
                'line' => 3,
//                array('name' => '内科', 'width' => 1),
//                array('name' => '外科', 'width' => 1),
//                array('name' => '小児科', 'width' => 1),
//                array('name' => '精神科', 'width' => 1),
//                array('name' => '婦人科', 'width' => 1),
//                array('name' => 'ﾘﾊﾋﾞﾘ科', 'width' => 1),
//                array('name' => '亜急性期', 'width' => 1),
                array('name' => '', 'width' => 1),
                array('name' => '', 'width' => 1),
                array('name' => '', 'width' => 1),
                array('name' => '', 'width' => 1),
                array('name' => '', 'width' => 1),
                array('name' => '', 'width' => 1),
//                array('name' => '緩和ｹｱ', 'width' => 1),
                array('name' => '', 'width' => 1),
                array('name' => '坦送', 'width' => 1),
                array('name' => '護送', 'width' => 1),
                array('name' => '独歩', 'width' => 1),
            ),
            array(
                'line' => 5,
                array('name' => '看護度', 'width' => 5),
                array('name' => '患者状況', 'width' => 3),
            ),
            array(
                'line' => 6,
                array('name' => '', 'width' => 1),
                array('name' => 'I', 'width' => 1),
                array('name' => 'II', 'width' => 1),
                array('name' => 'III', 'width' => 1),
                array('name' => 'IV', 'width' => 1),
            ),
            array(
                'line' => 7,
                array('name' => 'A', 'width' => 1),
            ),
            array(
                'line' => 8,
                array('name' => 'B', 'width' => 1),
            ),
            array(
                'line' => 9,
                array('name' => 'C', 'width' => 1),
            ),
        );
        $this->makeHeadFromArr(false, $headArr);
    }

    /**
     * 移動情報
     */
    private function makeChangeInformationHead() {

        /**
         * 入院・転入・退院・転出
         */
        $this->makeHeadVCell(1, 10, 0.5, 24, "");
        $headStr = "入院・転入　退院・転出";
        $this->makeHeadVCellStr($headStr, 14, 1);

        $headArr = array(
            array(
                'line' => 10,
                array('name' => '区分', 'width' => 1),
                array('name' => '診療科', 'width' => 1),
                array('name' => '時間', 'width' => 1),
                array('name' => '患者氏名', 'width' => 3),
                array('name' => '年齢', 'width' => 1),
                array('name' => '性別', 'width' => 1),
                array('name' => '主病名', 'width' => 3),
                array('name' => '主治医', 'width' => 2),
                array('name' => '特記事項', 'width' => 3),
            )
        );
        $this->makeHeadFromArr(false, $headArr);
    }

    /**
     * 重症者情報
     */
    private function makeSeriousIllnessHead() {

        /**
         * 重症・要観察・手術
         */
        $this->makeHeadVCell(1, 34, 0.5, 13, "");
        $this->makeHeadVCellStr("重症・要観察・手術", 35, 1);

        $headArr = array(
            array(
                'line' => 34,
                array('name' => '区分', 'width' => 1),
                array('name' => '診療科', 'width' => 1),
                array('name' => '時間', 'width' => 1),
                array('name' => '患者氏名', 'width' => 3),
                array('name' => '年齢', 'width' => 1),
                array('name' => '性別', 'width' => 1),
                array('name' => '日勤帯の特記事項', 'width' => 8),
            )
        );
        $this->makeHeadFromArr(false, $headArr);
    }

    /**
     * 勤務者情報
     */
    private function makeWorkersInformationHead() {

        /**
         * 勤務者情報のセル
         */
        $this->makeHeadVCell(17.5, 1, 0.5, 24, "");
        $this->makeHeadVCellStr("勤務者情報", 10, 17.5);

        $headArr = array(
            array(
                'line' => 1,
                array('name' => '区分', 'width' => 1),
                array('name' => '職種', 'width' => 2),
                array('name' => '職員氏名', 'width' => 2),
                array('name' => '区分', 'width' => 1),
                array('name' => '職種', 'width' => 2),
                array('name' => '職員氏名', 'width' => 2),
                array('name' => '休暇', 'width' => 1),
                array('name' => '職種', 'width' => 2),
                array('name' => '職員氏名', 'width' => 2),
            )
        );
        $this->makeHeadFromArr(true, $headArr);
    }

    /**
     * 夜間入退院情報
     */
    private function makeNightHospitalizationHead() {

        /**
         * 夜間入退院のセル
         */
        $this->makeHeadVCell(17.5, 25, 0.5, 11, "");
        $this->makeHeadVCellStr("夜間入退院", 27, 17.5);

        $headArr = array(
            array(
                'line' => 25,
                array('name' => '区分', 'width' => 1),
                array('name' => '診療科', 'width' => 2),
                array('name' => '時間', 'width' => 1),
                array('name' => '患者氏名', 'width' => 3),
                array('name' => '年齢', 'width' => 1),
                array('name' => '主病名', 'width' => 3.5),
                array('name' => '主治医', 'width' => 3.5),
            )
        );
        $this->makeHeadFromArr(true, $headArr);
    }

    /**
     * 管理事項
     * 　夜間情報
     * 　巡視時間
     */
    private function makeNightInformationHead() {

        /**
         * 管理事項
         */
        $this->makeHeadVCell(17.5, 36, 0.5, 11, "");
        $this->makeHeadVCellStr("管理事項", 38, 17.5);

        /**
         * 巡視時間
         */
        $this->makeHeadVCell(18, 42, 0.5, 5, "");
        $this->makeHeadVCellStr("巡視時間", 41.5, 18);

        $headArr = array(
            array(
                'line' => 36,
                array('name' => '夜間の特記・管理事項', 'width' => 15),
            )
        );
        $this->makeHeadFromArr(true, $headArr);
    }

    /**
     * 配列からヘッダを作成
     */
    private function makeHeadFromArr($isRight, $headArr) {

        if ($isRight) {
            $left = $this->_LeftMargin + $this->_CellWidth * 17;
        } else {
            $left = $this->_LeftMargin + $this->_CellWidth * 0.5;
        }
        for ($i = 0; $i < count($headArr); $i++) {
            $this->_Pdf->setXY($left, $this->_BodyFirstY + ($headArr[$i]["line"] - 1) * $this->_Height);
            for ($j = 0; $j < count($headArr[$i]) - 1; $j++) {
                $this->makeHeadCell($headArr[$i][$j]["width"], $headArr[$i][$j]["name"]);
            }
        }
    }

    /**
     * ヘッダ用背景色付セルの描画
     */
    private function makeHeadCell($width, $text) {

        $this->_Pdf->SetFillColor(224, 224, 224);
        $this->_Pdf->Cell($this->_CellWidth * $width, $this->_Height, $text, $this->_Border, 0, 'C', 1);
        /**
         * w      セルの幅
         * h      セルの高さ
         * txt    表示する文字列
         * border 境界線
         * ln     メソッド実行後のカレント位置
         * align  テキストの位置
         * fill   背景色をセットするかどうか
         * link   URLかAddLink()メンバ関数によって返された識別子
         */
    }

    /**
     * ヘッダ用背景色付縦長セルの描画
     */
    private function makeHeadVCell($firstCol, $firstRow, $width, $height, $text) {

        $this->_Pdf->SetXY($this->_LeftMargin + ($firstCol - 1) * $this->_CellWidth, $this->_BodyFirstY + $this->_Height * ($firstRow - 1));
        $this->_Pdf->SetFillColor(224, 224, 224);
        $this->_Pdf->Cell($this->_CellWidth * $width, $this->_Height * $height, $text, $this->_Border, 0, 'C', 1);
    }

    /**
     * ヘッダ用背景色付縦長セル内文字列の描画
     */
    private function makeHeadVCellStr($headStr, $row, $col) {

        for ($i = 0; $i < mb_strlen($headStr); $i++) {
            $this->_Pdf->SetXY($this->_LeftMargin + ($col - 1) * $this->_CellWidth, $this->_BodyFirstY + $this->_Height * ($row + $i));
            $this->_Pdf->Write(4, mb_substr($headStr, $i, 1));
        }
    }

    /**
     * 明細部の描画
     */
    private function makeBody() {

        /**
         * ヘッダ部
         */
        $this->_Pdf->SetFont(GOTHIC, '', 8);
        $this->_Pdf->SetXY($this->_LeftMargin, 17);
        $this->_Pdf->Cell($this->_CellWidth * 3, 4, $this->_HeaderData["shift_group"], '0', 0, 'L');
        $this->_Pdf->Cell($this->_CellWidth * 4, 4, $this->_HeaderData["report_date"], '0', 0, 'L');
        //$this->_Pdf->Cell($this->_CellWidth * 5, 4, $this->_HeaderData["day_shift_name"], '0', 0, 'L');
        //$this->_Pdf->Cell($this->_CellWidth * 5, 4, $this->_HeaderData["night_shift_name"], '0', 0, 'L');

        $this->_Pdf->SetFont(GOTHIC, '', 7);
        $this->_Pdf->SetFillColor(255, 255, 255);
        $left = $this->_LeftMargin + $this->_CellWidth * 0.5;

        /**
         * 患者数２行目
         */
        $this->_Pdf->setXY($left, $this->_BodyFirstY + 1 * $this->_Height);
        for ($i = 0; $i < count($this->_BasicData["num"][0]); $i++) {
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_BasicData["num"][0][$i], $this->_Border, 0, 'R', 1);
        }

        /**
         * 患者数３行目
         */
        $this->_Pdf->SetFillColor(224, 224, 224);
        $this->_Pdf->SetFont(GOTHIC, '', 5);
        $this->_Pdf->setXY($left, $this->_BodyFirstY + 2 * $this->_Height);
        for ($i = 0; $i < count($this->_BasicData["name"][1]); $i++) {
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_BasicData["name"][1][$i], $this->_Border, 0, 'C', 1);
        }
        $this->_Pdf->SetFont(GOTHIC, '', 7);
        $this->_Pdf->SetFillColor(255, 255, 255);

        /**
         * 患者数４行目
         */
        $this->_Pdf->setXY($left, $this->_BodyFirstY + 3 * $this->_Height);
        for ($i = 0; $i < count($this->_BasicData["num"][1]); $i++) {
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_BasicData["num"][1][$i], $this->_Border, 0, 'R', 1);
        }
        for ($i = 0; $i < count($this->_BasicData["num"][2]); $i++) {
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_BasicData["num"][2][$i], $this->_Border, 0, 'R', 1);
        }

        /**
         * 物品点検
         */
        for ($i = 0; $i < count($this->_BasicData["num"][3]); $i++) {
            $this->_Pdf->setXY($left + $this->_CellWidth * 10, $this->_BodyFirstY + ($i + 1) * $this->_Height);
            $this->_Pdf->Cell($this->_CellWidth * 3, $this->_Height, $this->_BasicData["num"][3][$i][0], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_BasicData["num"][3][$i][1], $this->_Border, 0, 'R', 1);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_BasicData["num"][3][$i][2], $this->_Border, 0, 'R', 1);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_BasicData["num"][3][$i][3], $this->_Border, 0, 'R', 1);
        }

        /**
         * 看護度
         */
        for ($i = 0; $i < count($this->_BasicData["num"][4]); $i++) {
            $this->_Pdf->setXY($left + $this->_CellWidth, $this->_BodyFirstY + ($i + 6) * $this->_Height);
            for ($j = 0; $j < count($this->_BasicData["num"][4][$i]); $j++) {
                $this->_Pdf->Cell($this->_CellWidth * 1, $this->_Height, $this->_BasicData["num"][4][$i][$j], $this->_Border, 0, 'R', 1);
            }
        }

        /**
         * 患者状況
         */
        for ($i = 0; $i < count($this->_BasicData["num"][5]); $i++) {
            $this->_Pdf->setXY($left + $this->_CellWidth * 5, $this->_BodyFirstY + ($i + 5) * $this->_Height);
            $this->_Pdf->Cell($this->_CellWidth * 2, $this->_Height, $this->_BasicData["num"][5][$i][0], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_BasicData["num"][5][$i][1], $this->_Border, 0, 'R', 1);
        }

        /**
         * 入院、転入、退院、転出
         */
        for ($i = 0; $i < count($this->_ChangeInformationData); $i++) {
            $this->_Pdf->setXY($left, $this->_BodyFirstY + ($i + 10) * $this->_Height);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_ChangeInformationData[$i]["data_class"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_ChangeInformationData[$i]["department_name"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_ChangeInformationData[$i]["hour"], $this->_Border, 0, 'R', 1);
            $this->_Pdf->Cell($this->_CellWidth * 3, $this->_Height, $this->_ChangeInformationData[$i]["patient_name"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_ChangeInformationData[$i]["age"], $this->_Border, 0, 'C', 1);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_ChangeInformationData[$i]["sex"], $this->_Border, 0, 'C', 1);
            $this->_Pdf->Cell($this->_CellWidth * 3, $this->_Height, $this->_ChangeInformationData[$i]["disease_name"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth * 2, $this->_Height, $this->_ChangeInformationData[$i]["attending_physician"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth * 3, $this->_Height, $this->_ChangeInformationData[$i]["articles"], $this->_Border, 0, 'L', 1);
        }

        /**
         * 重症
         */
        for ($i = 0; $i < count($this->_SeriousIllnessData); $i++) {
            $this->_Pdf->setXY($left, $this->_BodyFirstY + ($i + 34) * $this->_Height);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_SeriousIllnessData[$i]["data_class"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_SeriousIllnessData[$i]["department_name"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_SeriousIllnessData[$i]["hour"], $this->_Border, 0, 'R', 1);
            $this->_Pdf->Cell($this->_CellWidth * 3, $this->_Height, $this->_SeriousIllnessData[$i]["patient_name"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_SeriousIllnessData[$i]["age"], $this->_Border, 0, 'C', 1);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_SeriousIllnessData[$i]["sex"], $this->_Border, 0, 'C', 1);
            //$this->_Pdf->Cell($this->_CellWidth * 8, $this->_Height, $this->_SeriousIllnessData[$i]["articles"], $this->_Border, 0, 'L', 1);
        }
        $this->_Pdf->setXY($left + $this->_CellWidth * 8, $this->_BodyFirstY + 34 * $this->_Height);
        $this->_Pdf->Cell($this->_CellWidth * 8, $this->_Height * 12, "", $this->_Border, 0, 'L', 1);

        $arr = explode("\r\n", $this->_SeriousInformationData[0]["articles"]);
        for ($i = 0; $i < count($arr); $i++) {
            if ($i >= 12) {
                break;
            }
            $this->_Pdf->setXY($left + $this->_CellWidth * 8, $this->_BodyFirstY + 34 * $this->_Height + ($this->_Height * $i));
            $this->_Pdf->Write(4, $arr[$i]);
        }

        /**
         * 勤務者情報 3段にわたる
         */
        for ($i = 0; $i < 23; $i++) {
            $this->_Pdf->setXY($left + $this->_CellWidth * 16.5, $this->_BodyFirstY + ($i + 1) * $this->_Height);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_WorkersInformationData[$i]["data_class"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth * 2, $this->_Height, $this->_WorkersInformationData[$i]["job_category"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth * 2, $this->_Height, $this->_WorkersInformationData[$i]["staff_name"], $this->_Border, 0, 'L', 1);
        }
        for ($i = 0; $i < 23; $i++) {
            $this->_Pdf->setXY($left + $this->_CellWidth * (16.5 + 5), $this->_BodyFirstY + ($i + 1) * $this->_Height);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_WorkersInformationData[$i + 23]["data_class"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth * 2, $this->_Height, $this->_WorkersInformationData[$i + 23]["job_category"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth * 2, $this->_Height, $this->_WorkersInformationData[$i + 23]["staff_name"], $this->_Border, 0, 'L', 1);
        }
        for ($i = 0; $i < 23; $i++) {
            $this->_Pdf->setXY($left + $this->_CellWidth * (16.5 + 5 + 5), $this->_BodyFirstY + ($i + 1) * $this->_Height);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_WorkersInformationData[$i + 23 + 23]["data_class"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth * 2, $this->_Height, $this->_WorkersInformationData[$i + 23 + 23]["job_category"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth * 2, $this->_Height, $this->_WorkersInformationData[$i + 23 + 23]["staff_name"], $this->_Border, 0, 'L', 1);
        }

        /**
         * 夜間入退院
         */
        for ($i = 0; $i < count($this->_NightHospitalizationData); $i++) {
            $this->_Pdf->setXY($left + $this->_CellWidth * 16.5, $this->_BodyFirstY + ($i + 25) * $this->_Height);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_NightHospitalizationData[$i]["data_class"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth * 2, $this->_Height, $this->_NightHospitalizationData[$i]["department_name"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_NightHospitalizationData[$i]["hour"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth * 3, $this->_Height, $this->_NightHospitalizationData[$i]["patient_name"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth, $this->_Height, $this->_NightHospitalizationData[$i]["age"], $this->_Border, 0, 'C', 1);
            $this->_Pdf->Cell($this->_CellWidth * 3.5, $this->_Height, $this->_NightHospitalizationData[$i]["disease_name"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth * 3.5, $this->_Height, $this->_NightHospitalizationData[$i]["attending_physician"], $this->_Border, 0, 'L', 1);
        }

        /**
         * 特記事項
         */
        $this->_Pdf->setXY($left + $this->_CellWidth * 16.5, $this->_BodyFirstY + 36 * $this->_Height);
        $this->_Pdf->Cell($this->_CellWidth * 15, $this->_Height * 5, "", $this->_Border, 0, 'L', 1);
        $arr = explode("\r\n", $this->_NightInformationData[0]["articles"]);
        for ($i = 0; $i < count($arr); $i++) {
            if ($i >= 5) {
                break;
            }
            $this->_Pdf->setXY($left + $this->_CellWidth * 16.5, $this->_BodyFirstY + 36 * $this->_Height + ($this->_Height * $i));
            $this->_Pdf->Write(4, $arr[$i]);
        }

        /**
         * 巡視時間
         */
        for ($i = 0; $i < count($this->_NightPatroltimeData); $i++) {
            $this->_Pdf->setXY($left + $this->_CellWidth * 17, $this->_BodyFirstY + ($i + 41) * $this->_Height);
            $this->_Pdf->Cell($this->_CellWidth * 3.5, $this->_Height, $this->_NightPatroltimeData[$i]["data_class"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth * 1, $this->_Height, $this->_NightPatroltimeData[$i]["hour"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth * 3, $this->_Height, $this->_NightPatroltimeData[$i]["abnormality"], $this->_Border, 0, 'L', 1);
            $this->_Pdf->Cell($this->_CellWidth * 7, $this->_Height, $this->_NightPatroltimeData[$i]["articles"], $this->_Border, 0, 'L', 1);
//            if ($this->_NightPatroltimeData[$i]["data_class"] != "") {
//                $str = $this->_NightPatroltimeData[$i]["hour"] . "(" . $this->_NightPatroltimeData[$i]["staff_name"] . ")";
//            } else {
//                $str = "";
//            }
//            $this->_Pdf->Cell($this->_CellWidth * 11, $this->_Height, $str, $this->_Border, 0, 'L', 1);
        }
    }

    /**
     * PDF出力
     */
    private function output() {

        header("Content-Type: application/octet-stream");
        header("Content-Disposition: inline;
        filename = \"ByotoDiary.pdf\"");
        $this->_Pdf->Output();
    }

}
