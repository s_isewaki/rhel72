<?php

include_once(APP_BASE . "controller/AppController.php");

/**
 * 種別A分類
 */
class ClassificationTypeA extends AppController {

    /**
     * コンストラクタ
     */
    public function __construct($programName) {

        parent::__construct();

        $this->_ProgramName = $programName;
        $this->Uses(
                array(
                    "DutyShiftImportHeader",
                    "DutyShiftImportTypea",
                    "DutyShiftImportPatient",
                    "DutyShiftImportPatientWard",
                )
        );
    }

    /**
     * TYPEAのデータの更新
     */
    public function AddTypeADb($rowHead) {

        /**
         * typeテーブルを読み込む
         */
        $cond = Array(
            "fields" => Array(
                "name_kana" => "name_kana",
                "name_kanji" => "name_kanji",
                "sex" => "sex",
                "birthday" => "birthday",
                "contact" => "contact",
                "status" => "status",
            ),
            "conditions" => Array(
                "file_name" => $rowHead["file_name"],
            ),
        );
        $resultTypeA = $this->_Model["DutyShiftImportTypea"]->find($cond);
        if (!$resultTypeA) {
            return false;
        }

        $this->BeginTransaction();
        if (!$this->updTypeADbDetail($rowHead, $resultTypeA[0])) {
            $this->_Logger->PutInfo("ロールバックしました" . $rowHead["file_name"]);
            $this->Rollback();
            return false;
        }

        if (!$this->updateStatus($rowHead, "1")) {
            $this->_Logger->PutInfo("ロールバックしました" . $rowHead["file_name"]);
            $this->Rollback();
            return false;
        }

        $this->Commit();
        return true;
    }

    /**
     * 
     * @param type $rowHead
     * @param type $status
     * @return boolean
     */
    private function updateStatus($rowHead, $status) {

        $cond = Array(
            "data" => Array(
                "status" => $status,
            ),
            "conditions" => Array(
                "file_name" => $rowHead["file_name"],
            ),
        );
        $result = $this->_Model["DutyShiftImportHeader"]->Save($cond);
        if (!$result) {
            return false;
        }

        $cond = Array(
            "data" => Array(
                "status" => $status,
            ),
            "conditions" => Array(
                "file_name" => $rowHead["file_name"],
            ),
        );
        $result = $this->_Model["DutyShiftImportTypea"]->Save($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * TYPEAのデータの削除
     */
    public function DeleteTypeADb($rowHead) {

        //typeテーブルを読み込む
        $cond = Array(
            "fields" => Array(
                "name_kana" => "name_kana",
                "name_kanji" => "name_kanji",
                "sex" => "sex",
                "birthday" => "birthday",
                "contact" => "contact",
                "status" => "status",
            ),
            "conditions" => Array(
                "status" => "1",
                "patient_cd" => $rowHead["patient_cd"],
            ),
            "order" => Array(
                "file_name" => "DESC",
            )
        );
        $resultTypeA = $this->_Model["DutyShiftImportTypea"]->find($cond);
        if (!$resultTypeA) {
            $this->_Logger->PutInfo("前のデータがないため削除できません");
            return false;
        }
        if (count($resultTypeA) < 2) {
            $this->_Logger->PutInfo("前のデータがないため削除できません");
            return false;
        }

        $this->BeginTransaction();
        if (!$this->updTypeADbDetail($rowHead, $resultTypeA[1])) {
            $this->Rollback();
            return false;
        }

        if (!$this->updateStatus($rowHead, "3")) {
            $this->Rollback();
            return false;
        }

        $this->Commit();

        return true;
    }

    /**
     * TYPEAのデータの更新詳細
     */
    private function updTypeADbDetail($rowHead, $rowBody) {

        $cond = Array(
            "data" => Array(
                "name_kana" => $rowBody["name_kana"],
                "name_kanji" => $rowBody["name_kanji"],
                "sex" => $rowBody["sex"],
                "birthday" => $rowBody["birthday"],
                "contact" => $rowBody["contact"],
                "del_flg" => "0",
            ),
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
            ),
        );
        $result = $this->_Model["DutyShiftImportPatient"]->Save($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

}
