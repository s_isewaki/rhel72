<?php

include_once(APP_BASE . "controller/AppController.php");

/**
 * DB LOADER
 */
class DutyShiftReportDbLoader extends AppController {

    /**
     * 画面項目名
     * @var type 
     */
    private $_BasicItems;
    private $_ChangeInformationItems;
    private $_SeriousIllnessItems;
    private $_SeriousInformationItems;
    private $_WorkersInformationItems;
    private $_NightHospitalizationItems;
    private $_NightInformationItems;
    private $_NightPatroltimeItems;

    /**
     * 画面データ
     * @var type 
     */
    private $_BasicData;

    /**
     * コンストラクタ
     */
    public function __construct() {

        parent::__construct();

        $this->Uses(
                array(
                    "DutyShiftReportBasic",
                    "DutyShiftReportChangeInformation",
                    "DutyShiftReportSeriousIllness",
                    "DutyShiftReportSeriousInformation",
                    "DutyShiftReportWorkersInformation",
                    "DutyShiftReportNightHospitalization",
                    "DutyShiftReportNightInformation",
                    "DutyShiftReportNightPatroltime",
                    "DutyShiftReportHistory",
                    "DutyShiftGroup",
                )
        );
        $this->setInputItems();
    }

    /**
     * 基本項目の設定
     * @param type $value
     */
    public function SetBasicData($value) {

        $this->_BasicData = $value;
    }

    /**
     * 入力項目名設定
     */
    private function setInputItems() {

        $inputItems = new DutyShiftReportInputItems();

        $this->_BasicItems = $inputItems->GetBasicItems();
        $this->_ChangeInformationItems = $inputItems->GetChangeInformationItems();
        $this->_SeriousIllnessItems = $inputItems->GetSeriousIllnessItems();
        $this->_SeriousInformationItems = $inputItems->GetSeriousInformationItems();
        $this->_WorkersInformationItems = $inputItems->GetWorkersInformationItems();
        $this->_NightHospitalizationItems = $inputItems->GetNightHospitalizationItems();
        $this->_NightInformationItems = $inputItems->GetNightInformationItems();
        $this->_NightPatroltimeItems = $inputItems->GetNightPatroltimeItems();
    }

    /**
     * 勤務シフトグループの取得
     * @param type $value
     */
    public function LoadDutyShiftGroup($value) {

//        $cond = Array(
//            "fields" => Array(
//                "group_id" => "group_id",
//                "group_name" => "group_name",
//            ),
//            "conditions" => Array(
//                "sfc_group_code" => "!= ",
//            ),
//            "order" => Array(
//                "group_id" => "ASC",
//            )
//        );
//
//        return $this->_Model["DutyShiftGroup"]->Find($cond);
        return $this->_Model["DutyShiftGroup"]->GetShiftGroupList($value);
    }

    /**
     * 基本情報の読み込み
     */
    public function LoadBasic() {

        $cond = Array(
            "conditions" => Array(
                "shift_group" => $this->_BasicData[0]["shift_group"],
                "report_date" => $this->_BasicData[0]["report_date"],
            ),
            "fields" => Array(
                "''" => "proc",
            )
        );

        foreach ($this->_BasicItems as $item) {
            $cond["fields"][$item] = $item;
        }

        $data = $this->_Model["DutyShiftReportBasic"]->Find($cond);
        if (!$data) {
            foreach ($this->_BasicItems as $item) {
                $data[0][$item] = "";
            }
        }

        $data[0]["shift_group"] = $this->_BasicData[0]["shift_group"];
        $data[0]["report_date"] = $this->_BasicData[0]["report_date"];
        $data[0]["proc"] = $this->_BasicData[0]["proc"];

        return $data;
    }

    /**
     * 移動情報の読み込み
     */
    public function LoadChangeInformation() {

        $cond = Array(
            "conditions" => Array(
                "shift_group" => $this->_BasicData[0]["shift_group"],
                "report_date" => $this->_BasicData[0]["report_date"],
            ),
            "order" => Array(
                "detail_no" => "ASC",
            )
        );

        foreach ($this->_ChangeInformationItems as $item) {
            $cond["fields"][$item] = $item;
        }

        return $this->_Model["DutyShiftReportChangeInformation"]->Find($cond);
    }

    /**
     * 重症・要観察者・手術の読み込み
     */
    public function LoadSeriousIllness() {

        $cond = Array(
            "conditions" => Array(
                "shift_group" => $this->_BasicData[0]["shift_group"],
                "report_date" => $this->_BasicData[0]["report_date"],
            ),
            "order" => Array(
                "detail_no" => "ASC",
            )
        );

        foreach ($this->_SeriousIllnessItems as $item) {
            $cond["fields"][$item] = $item;
        }

        return $this->_Model["DutyShiftReportSeriousIllness"]->Find($cond);
    }

    /**
     * 重症_日勤帯の特記事項
     * @return type
     */
    public function LoadSeriousInformation() {

        $cond = Array(
            "conditions" => Array(
                "shift_group" => $this->_BasicData[0]["shift_group"],
                "report_date" => $this->_BasicData[0]["report_date"],
            ),
        );

        foreach ($this->_SeriousInformationItems as $item) {
            $cond["fields"][$item] = $item;
        }

        return $this->_Model["DutyShiftReportSeriousInformation"]->Find($cond);
    }

    /**
     * 勤務者情報の読み込み
     */
    public function LoadWorkersInformation() {

        $cond = Array(
            "conditions" => Array(
                "shift_group" => $this->_BasicData[0]["shift_group"],
                "report_date" => $this->_BasicData[0]["report_date"],
            ),
            "order" => Array(
                "detail_no" => "ASC",
            )
        );

        foreach ($this->_WorkersInformationItems as $item) {
            $cond["fields"][$item] = $item;
        }

        return $this->_Model["DutyShiftReportWorkersInformation"]->Find($cond);
    }

    /**
     * 夜間入退院情報の読み込み
     */
    public function LoadNightHospitalization() {

        $cond = Array(
            "conditions" => Array(
                "shift_group" => $this->_BasicData[0]["shift_group"],
                "report_date" => $this->_BasicData[0]["report_date"],
            ),
            "order" => Array(
                "detail_no" => "ASC",
            )
        );

        foreach ($this->_NightHospitalizationItems as $item) {
            $cond["fields"][$item] = $item;
        }

        return $this->_Model["DutyShiftReportNightHospitalization"]->Find($cond);
    }

    /**
     * 夜間情報の読み込み
     */
    public function LoadNightInformation() {

        $cond = Array(
            "conditions" => Array(
                "shift_group" => $this->_BasicData[0]["shift_group"],
                "report_date" => $this->_BasicData[0]["report_date"],
            ),
        );

        foreach ($this->_NightInformationItems as $item) {
            $cond["fields"][$item] = $item;
        }

        return $this->_Model["DutyShiftReportNightInformation"]->Find($cond);
    }

    /**
     * 巡視時間の読み込み
     */
    public function LoadNightPatroltime() {

        $cond = Array(
            "conditions" => Array(
                "shift_group" => $this->_BasicData[0]["shift_group"],
                "report_date" => $this->_BasicData[0]["report_date"],
            ),
            "order" => Array(
                "detail_no" => "ASC",
            )
        );

        foreach ($this->_NightPatroltimeItems as $item) {
            $cond["fields"][$item] = $item;
        }

        return $this->_Model["DutyShiftReportNightPatroltime"]->Find($cond);
    }

    /**
     * 履歴情報の読み込み
     */
    public function LoadHistory() {

        $cond = Array(
            "conditions" => Array(
                "shift_group" => $this->_BasicData[0]["shift_group"],
                "report_date" => $this->_BasicData[0]["report_date"],
            ),
            "fields" => Array(
                "history_date" => "history_date",
                "entry_name" => "entry_name",
            ),
            "order" => Array(
                "history_date" => "DESC",
            )
        );

        return $this->_Model["DutyShiftReportHistory"]->Find($cond);
    }

}
