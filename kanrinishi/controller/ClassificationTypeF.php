<?php

include_once(APP_BASE . "controller/AppController.php");

/**
 * 種別F分類
 */
class ClassificationTypeF extends AppController {

    /**
     * コンストラクタ
     */
    public function __construct($programName) {

        parent::__construct();

        $this->_ProgramName = $programName;
        $this->Uses(
                array(
                    "DutyShiftImportHeader",
                    "DutyShiftImportTypef",
                    "DutyShiftImportPatientWard",
                    "DutyShiftImportNursingDegree",
                )
        );
    }

    /**
     * TYPEFの追加
     * @param type $rowHead
     * @return boolean
     */
    public function AddTypeFDb($rowHead) {

        $cond = Array(
            "fields" => Array(
                "data_type" => "data_type",
                "start_date" => "start_date",
                "discrimination" => "discrimination",
                "status" => "status",
            ),
            "conditions" => Array(
                "file_name" => $rowHead["file_name"],
            ),
        );

        $resultTypeF = $this->_Model["DutyShiftImportTypef"]->find($cond);
        if (!$resultTypeF) {
            return false;
        }

        $this->BeginTransaction();
        if (!$this->updTypeFDb1Detail($rowHead, $resultTypeF[0])) {
            $this->Rollback();
            $this->_Logger->PutInfo("ロールバックしました" . $rowHead["file_name"]);
            $this->updateStatus($rowHead, "9");
            return false;
        }

        if (!$this->updateStatus($rowHead, "1")) {
            $this->Rollback();
            $this->_Logger->PutInfo("ロールバックしました" . $rowHead["file_name"]);
            $this->updateStatus($rowHead, "9");
            return false;
        }

        $this->Commit();
        return true;
    }

    /**
     * 
     * @param type $rowHead
     * @param type $status
     * @return boolean
     */
    private function updateStatus($rowHead, $status) {

        $cond = Array(
            "data" => Array(
                "status" => $status,
            ),
            "conditions" => Array(
                "file_name" => $rowHead["file_name"],
            ),
        );
        $result = $this->_Model["DutyShiftImportHeader"]->Save($cond);
        if (!$result) {
            return false;
        }

        $cond = Array(
            "data" => Array(
                "status" => $status,
            ),
            "conditions" => Array(
                "file_name" => $rowHead["file_name"],
            ),
        );
        $result = $this->_Model["DutyShiftImportTypef"]->Save($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * TYPEFのデータの削除
     * @param type $rowHead
     * @return boolean
     */
    public function DeleteTypeFDb($rowHead) {

        $cond = Array(
            "fields" => Array(
                "data_type" => "data_type",
                "start_date" => "start_date",
                "status" => "status",
            ),
            "conditions" => Array(
                "file_name" => $rowHead["file_name"],
            ),
        );

        $resultTypeF = $this->_Model["DutyShiftImportTypef"]->find($cond);
        if (!$resultTypeF) {
            return false;
        }

        //typeFテーブルを読み込む 患者コードとデータ区分が同じで処理済のデータ
        $condBefore = Array(
            "fields" => Array(
                "data_type" => "data_type",
                "start_date" => "start_date",
                "discrimination" => "discrimination",
            ),
            "conditions" => Array(
                "status" => "1",
                "patient_cd" => $rowHead["patient_cd"],
                "data_type" => $resultTypeF[0]["data_type"],
            ),
            "order" => Array(
                "file_name" => "DESC",
            )
        );

        $resultBeforeTypeF = $this->_Model["DutyShiftImportTypef"]->find($condBefore);
        if (!$resultBeforeTypeF) {
            $this->_Logger->PutInfo("前のデータの取得でエラーが発生しました。");
            $this->updateStatus($rowHead, "9");
            return false;
        }

        $this->BeginTransaction();

        if (count($resultBeforeTypeF) < 2) {
            //前のデータがない場合は、削除する。
            if (!$this->delTypeFDb1Detail($rowHead, $resultTypeF[0])) {
                $this->Rollback();
                $this->updateStatus($rowHead, "9");
                return false;
            }
        } else {
            //前のデータがある場合は前のデータに戻す。
            if (!$this->updTypeFDb1Detail($rowHead, $resultBeforeTypeF[1])) {
                $this->Rollback();
                $this->updateStatus($rowHead, "9");
                return false;
            }
        }

        if (!$this->updateStatus($rowHead, "3")) {
            $this->Rollback();
            $this->updateStatus($rowHead, "9");
            return false;
        }

        $this->Commit();

        return true;
    }

    /**
     * 前の患者情報を取得
     */
    private function getBeforePatientInfo($rowHead, $rowBody) {

        $condBeforePatient = Array(
            "fields" => Array(
                "ward_cd" => "ward_cd",
                "in_move_date" => "in_move_date",
                "in_move_time" => "in_move_time",
            ),
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
            ),
            "order" => Array(
                "in_move_date" => "DESC",
            ),
        );
        $resultBefore = $this->_Model["DutyShiftImportPatientWard"]->Find($condBeforePatient);
        if (!$resultBefore) {
            return false;
        }

        return $resultBefore;
    }

    /**
     * 
     * @param type $rowHead
     * @param type $rowBody
     * @return boolean
     */
    private function updTypeFDb1Detail($rowHead, $rowBody) {

        $beforeResult = $this->getBeforePatientInfo($rowHead, $rowBody);
        if (count($beforeResult) > 0) {
            $wordCd = $beforeResult[0]["ward_cd"];
        } else {
            $this->_Logger->PutInfo("患者情報がないため更新出来ません。");
            return false;
        }
        $cond = Array(
            "data" => Array(
                "nursing_degree" => $rowBody["discrimination"],
                "ward_cd" => $wordCd,
                "del_flg" => "0",
            ),
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
                "data_type" => $rowBody["data_type"],
                "begin_date" => $rowBody["start_date"],
            ),
        );
        $result = $this->_Model["DutyShiftImportNursingDegree"]->Save($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * 
     * @param type $rowHead
     * @param type $rowBody
     * @return boolean
     */
    private function delTypeFDb1Detail($rowHead, $rowBody) {

        $cond = Array(
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
                "data_type" => $rowBody["data_type"],
                "begin_date" => $rowBody["start_date"],
            ),
        );
        $result = $this->_Model["DutyShiftImportNursingDegree"]->Delete($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

}
