<?php

include_once(APP_BASE . "controller/AppController.php");

/**
 * 重症・要観察者・手術セクションインポートクラス
 */
class DutyShiftReportImpoterSeriousIllness extends AppController {

    private $_BasicData;    //基本情報データ
    private $_SeriousIllnessItems;
    private $_SeriousIllnessData;
    private $_ErrorMessage = "";

    /**
     * コンストラクタ
     */
    public function __construct() {

        parent::__construct();

        $this->Uses(
                array(
                    "DutyShiftReportSeriousIllness",
                )
        );
        $inputItems = new DutyShiftReportInputItems();
        $this->_SeriousIllnessItems = $inputItems->GetSeriousIllnessItems();
    }

    /**
     * エラーメッセージの取得
     * @return type
     */
    public function GetErrorMessage() {

        return $this->_ErrorMessage;
    }

    /**
     * 基本情報の設定
     * @param type $value
     */
    public function SetBasicData($value) {

        $this->_BasicData = $value;
    }

    /**
     * 重症・要観察者・手術セクションデータの設定
     * @param type $value
     */
    public function SetSeriousIllnessData($value) {

        $this->_SeriousIllnessData = $value;
    }

    /**
     * 取込
     * @return type
     */
    public function Import() {

        $cond = Array(
            "conditions" => Array(
                "shift_group" => $this->_BasicData[0]["shift_group"],
                "report_date" => $this->_BasicData[0]["before_date"],
            ),
            "order" => Array(
                "detail_no" => "ASC",
            )
        );

        foreach ($this->_SeriousIllnessItems as $item) {
            $cond["fields"][$item] = $item;
        }

        return $this->_Model["DutyShiftReportSeriousIllness"]->Find($cond);
    }

    /**
     * 画面入力の続きにインポート
     * 一括取込用
     */
    public function ImportAll() {

        /**
         * 配列の初期化
         */
        $data = array();
        for ($j = 0; $j < LINE_SERIOUS_ILLNESS; $j++) {
            foreach ($this->_SeriousIllnessItems as $item) {
                $data[$j][$item] = "";
            }
        }

        /**
         * 現在のデータを保存
         */
        $j = 0;
        foreach ($this->_SeriousIllnessData as $nhData) {
            $notEmpty = false;
            foreach ($this->_SeriousIllnessItems as $item) {
                if ($nhData[$item] != "") {
                    $notEmpty = true;
                }
            }
            if (!$notEmpty) {
                break;
            }
            foreach ($this->_SeriousIllnessItems as $item) {
                if (isset($nhData[$item])) {
                    $data[$j][$item] = $nhData[$item];
                }
            }
            $j++;
        }

        /**
         * 取込データを追加
         */
        $importData = $this->Import();
        for ($i = 0; $i < count($importData); $i++) {
            $isEmpty = true;
            foreach ($this->_SeriousIllnessItems as $item) {
                if ($importData[$i][$item] != "") {
                    $isEmpty = false;
                }
            }
            if ($isEmpty) {
                continue;
            }
            foreach ($this->_SeriousIllnessItems as $item) {
                $data[$j][$item] = $importData[$i][$item];
            }
            $j++;
        }

        //if (count($data) > LINE_SERIOUS_ILLNESS) {
        if ($j > LINE_SERIOUS_ILLNESS) {
            $this->_ErrorMessage = "重症・要観察・手術情報が12件を超えたため、取り込めない情報が発生しました";
        }

        return $data;
    }

    /**
     * プレビュー分を取り込む
     * @param type $searchData
     */
    public function ImportPreviewReg($searchData) {

        $data = array();
        for ($j = 0; $j < LINE_SERIOUS_ILLNESS; $j++) {
            foreach ($this->_SeriousIllnessItems as $item) {
                $data[$j][$item] = "";
            }
        }

        $j = 0;
        foreach ($this->_SeriousIllnessData as $nhData) {
            $notEmpty = false;
            foreach ($this->_SeriousIllnessItems as $item) {
                if ($nhData[$item] != "") {
                    $notEmpty = true;
                }
            }
            if (!$notEmpty) {
                break;
            }
            foreach ($this->_SeriousIllnessItems as $item) {
                if (isset($nhData[$item])) {
                    $data[$j][$item] = $nhData[$item];
                }
            }
            $j++;
        }

        for ($i = 0; $i < count($searchData); $i++) {
            $isEmpty = true;
            foreach ($this->_SeriousIllnessItems as $item) {
                if ($searchData[$i][$item] != "") {
                    $isEmpty = false;
                }
            }
            if ($isEmpty) {
                continue;
            }
            if ($searchData[$i]["check"] == "1") {
                foreach ($this->_SeriousIllnessItems as $item) {
                    $data[$j][$item] = $searchData[$i][$item];
                }
                $j++;
            }
        }

        if (count($data) > LINE_SERIOUS_ILLNESS) {
            $this->_ErrorMessage = "重症・要観察・手術情報が12件を超えたため、取り込めない情報が発生しました";
        }

        return $data;
    }

}
