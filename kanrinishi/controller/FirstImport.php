<?php

include_once(APP_BASE . "controller/AppController.php");

/**
 * 初期インポートクラス
 * 
 * 導入時に使用する。何らかのトラブル発生時に最初からデータを取り込みなおすという場合
 * にも使用する可能性がある。
 */
class FirstImport extends AppController {

    private $_TargetFile;
    private $_ImportPath = IMPORT_PATH;
    private $_SavePath = SAVE_PATH;

    /**
     * メイン 
     * @param type $argv
     * @return boolean
     */
    public function Main($argv) {

        $this->_TargetFile = FIRST_PATIENT . "." . TARGET_EXT;

        $this->_Logger->PutInfo("処理を開始しました。");

        $this->_ProgramName = $argv[0];
        $this->Uses(
                array(
                    "DutyShiftImportPatient",
                    "DutyShiftImportPatientWard",
                    "DutyShiftImportNursingDegree",
                )
        );

        $this->BeginTransaction();
        if (!$this->getCsv()) {
            $this->_Logger->PutErr("処理が異常終了しました。");
            $this->Rollback();
            return false;
        }

        $this->moveCsvFile($this->_TargetFile);

        $this->Commit();
        $this->_Logger->PutInfo("処理を終了しました。");

        return true;
    }

    /**
     * CSV取得
     * @return boolean
     */
    private function getCsv() {

        if (!file_exists($this->_ImportPath . $this->_TargetFile)) {
            $this->_Logger->PutErr("対象ファイルが存在しません。");
            return false;
        }

        $fileDataSJis = file_get_contents($this->_ImportPath . $this->_TargetFile);
        //$fileData = mb_convert_encoding($fileDataSJis, "EUC-JP", "SJIS");
        //$fileData = mb_convert_encoding($fileDataSJis, "EUC-JP", "SJIS-WIN");
        $fileData = mb_convert_encoding($fileDataSJis, "cp51932", "cp932");
        $lines = explode("\r\n", $fileData);

        foreach ($lines as $line) {
            if ($line == "") {
                break;
            }
            $cols = explode(",", $line);
            if (!$this->registDb($cols)) {
                return false;
            }
        }

        return true;
    }

    /**
     * 登録
     * @param type $cols
     * @return boolean
     */
    private function registDb($cols) {

        /**
         * 患者マスタ
         */
        if (!$this->registPatient($cols)) {
            return false;
        }

        /**
         * 基本データのみであればここで終了
         * 2016/01/21追加
         */
        if ($this->isBasicOnly($cols)) {
            $this->_Logger->PutInfo(trim($cols[0]) . ":基本項目データ");
            return true;
        }

        $this->_Logger->PutInfo(trim($cols[0]) . ":通常データ");

        /**
         * 患者病棟情報
         */
        if (!$this->registPatientWard($cols)) {
            return false;
        }

        /**
         * 看護度・救護区分
         */
        if (!$this->registNursingDegree($cols)) {
            return false;
        }

        return true;
    }

    /**
     * 基本データのみかの判定
     * 
     * 2016/01/21追加 入院ではなく患者情報だけのデータも存在し、入院等の情報は通常の
     * 　　　　　　　　取込バッチの方へ来るというパターンもあるため。
     * 理由：患者基本情報は入院の時ではなく、外来等で初めて登録されたときにしか来ない
     * 　　　ため現在入院していなくとも、あらかじめ患者基本情報を保持しておく必要があ
     * 　　　るため、その判別を行う。
     * @param type $cols
     * @return boolean
     */
    private function isBasicOnly($cols) {

        for ($i = 5; $i < count($cols); $i++) {
            if (trim($cols[$i]) != "") {
                return false;
            }
        }
        return true;
    }

    private function registPatient($cols) {

        $cond = Array(
            "data" => Array(
                "name_kana" => trim($cols[1]),
                "name_kanji" => trim($cols[2]),
                "sex" => trim($cols[3]),
                "birthday" => trim($cols[4]),
                "contact" => trim($cols[12]),
                "del_flg" => "0",
            ),
            "conditions" => Array(
                "patient_cd" => trim($cols[0]),
            ),
        );

        $result = $this->_Model["DutyShiftImportPatient"]->Save($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * 患者病棟情報登録
     * @param type $cols
     */
    private function registPatientWard($cols) {

        if (trim($cols[6]) == "") {
            $wordCd = "*";
        } else {
            $wordCd = trim($cols[6]);
        }

        $cond = Array(
            "data" => Array(
                "out_move_date" => "",
                "out_move_time" => "",
                "data_type1" => "0",
                "data_type2" => "",
                "department_cd" => trim($cols[7]),
                "sickroom_cd" => trim($cols[8]),
                "sickbed_cd" => trim($cols[9]),
                "admission_form" => trim($cols[5]),
                "attending_physician" => trim($cols[10]),
                "disease_name" => trim($cols[11]),
                "articles" => "",
                "outcome_cd" => "",
                "del_flg" => "0",
            ),
            "conditions" => Array(
                "patient_cd" => trim($cols[0]),
                "ward_cd" => $wordCd,
                "in_move_date" => date("Ymd", time()),
                "in_move_time" => FIRST_HOUR,
            ),
        );

        $result = $this->_Model["DutyShiftImportPatientWard"]->Save($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * 看護度・救護情報
     * @param type $cols
     */
    private function registNursingDegree($cols) {

        if (trim($cols[6]) == "") {
			return true;
        //    $wardCd = "*";
        } else {
            $wardCd = trim($cols[6]);
        }

        $cond1 = Array(
            "data" => Array(
                "nursing_degree" => trim($cols[13]),
                "del_flg" => "0",
                "ward_cd" => $wardCd,
            ),
            "conditions" => Array(
                "patient_cd" => trim($cols[0]),
                "data_type" => "1",
                "begin_date" => date("Ymd", time()),
            ),
        );

        $result1 = $this->_Model["DutyShiftImportNursingDegree"]->Save($cond1);
        if (!$result1) {
            return false;
        }

        if (trim($cols[14]) == "") {
			return true;
            //$nursingDegree = "03";
        } else {
            $nursingDegree = trim($cols[14]);
        }

        $cond2 = Array(
            "data" => Array(
                "nursing_degree" => $nursingDegree,
                "del_flg" => "0",
                "ward_cd" => $wardCd,
            ),
            "conditions" => Array(
                "patient_cd" => trim($cols[0]),
                "data_type" => "2",
                "begin_date" => date("Ymd", time()),
            ),
        );

        $result2 = $this->_Model["DutyShiftImportNursingDegree"]->Save($cond2);
        if (!$result2) {
            return false;
        }

        return true;
    }

    /**
     * 終了したファイルを移動
     * @param type $fileName
     */
    private function moveCsvFile($fileName) {

        rename($this->_ImportPath . $fileName, $this->_SavePath . $fileName);
    }

}
