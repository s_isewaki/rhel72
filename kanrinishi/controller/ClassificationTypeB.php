<?php

include_once(APP_BASE . "controller/AppController.php");

/**
 * データ種別Bの分類クラス
 */
class ClassificationTypeB extends AppController {

    /**
     * コンストラクタ
     */
    public function __construct($programName) {

        parent::__construct();

        $this->_ProgramName = $programName;
        $this->Uses(
                array(
                    "DutyShiftImportHeader",
                    "DutyShiftImportTypeb",
                    "DutyShiftImportPatientWard",
                    "DutyShiftImportChangeInformation",
                )
        );
    }

    /**
     * TYPEBの更新
     */
    public function AddTypeBDb($rowHead) {

        /**
         * typeBテーブルを読み込む
         */
        $cond = Array(
            "fields" => Array(
                "data_type" => "data_type",
                "move_date" => "move_date",
                "move_time" => "move_time",
                "ward_cd" => "ward_cd",
                "department_cd" => "department_cd",
                "sickroom_cd" => "sickroom_cd",
                "sickbed_cd" => "sickbed_cd",
                "admission_form" => "admission_form",
                "attending_physician" => "attending_physician",
                "disease_name" => "disease_name",
                "articles" => "articles",
                "outcome_cd" => "outcome_cd",
                "status" => "status",
            ),
            "conditions" => Array(
                "file_name" => $rowHead["file_name"],
            ),
        );

        $resultTypeB = $this->_Model["DutyShiftImportTypeb"]->find($cond);
        if (!$resultTypeB) {
            return false;
        }

        $this->BeginTransaction();
        switch ($resultTypeB[0]["data_type"]) {
            case "1":
                if (!$this->updTypeBDb1Detail($rowHead, $resultTypeB[0])) {
                    $this->_Logger->PutInfo("ロールバックしました" . $rowHead["file_name"]);
                    $this->Rollback();
                    $this->updateStatus($rowHead, "9");
                    return false;
                }
                break;
            case "2"://転棟
                if (!$this->updTypeBDb2Detail($rowHead, $resultTypeB[0], true)) {
                    $this->_Logger->PutInfo("ロールバックしました" . $rowHead["file_name"]);
                    $this->Rollback();
                    $this->updateStatus($rowHead, "9");
                    return false;
                }
                break;
            case "3"://転科
                if (!$this->updTypeBDb3Detail($rowHead, $resultTypeB[0], true)) {
                    $this->_Logger->PutInfo("ロールバックしました" . $rowHead["file_name"]);
                    $this->Rollback();
                    $this->updateStatus($rowHead, "9");
                    return false;
                }
                break;
            case "5"://退院
                if (!$this->updTypeBDb5Detail($rowHead, $resultTypeB[0])) {
                    $this->_Logger->PutInfo("ロールバックしました" . $rowHead["file_name"]);
                    $this->Rollback();
                    $this->updateStatus($rowHead, "9");
                    return false;
                }
                break;
            case "6"://主治医変更
                //患者の現在の病棟コードを取得してから更新する
                if (!$this->updTypeBDb6Detail($rowHead, $resultTypeB[0])) {
                    $this->_Logger->PutInfo("ロールバックしました" . $rowHead["file_name"]);
                    $this->Rollback();
                    $this->updateStatus($rowHead, "9");
                    return false;
                }
                break;
            case "7"://入院病名変更
                if (!$this->updTypeBDb7Detail($rowHead, $resultTypeB[0])) {
                    $this->_Logger->PutInfo("ロールバックしました" . $rowHead["file_name"]);
                    $this->Rollback();
                    $this->updateStatus($rowHead, "9");
                    return false;
                }
                break;
        }

        if (!$this->updateStatus($rowHead, "1")) {
            $this->Rollback();
            return false;
        }

        $this->Commit();
        return true;
    }

    /**
     * 処理ステータスの更新
     * @param type $rowHead
     * @param type $status
     * @return boolean
     */
    private function updateStatus($rowHead, $status) {

        $cond = Array(
            "data" => Array(
                "status" => $status,
            ),
            "conditions" => Array(
                "file_name" => $rowHead["file_name"],
            ),
        );
        $result = $this->_Model["DutyShiftImportHeader"]->Save($cond);
        if (!$result) {
            return false;
        }

        $cond = Array(
            "data" => Array(
                "status" => $status,
            ),
            "conditions" => Array(
                "file_name" => $rowHead["file_name"],
            ),
        );
        $result = $this->_Model["DutyShiftImportTypeb"]->Save($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * TYPEBのデータの削除
     */
    public function DeleteTypeBDb($rowHead) {

        /**
         * typeBテーブルを読み込む
         */
        $cond = Array(
            "fields" => Array(
                "data_type" => "data_type",
                "move_date" => "move_date",
                "move_time" => "move_time",
                "ward_cd" => "ward_cd",
                "status" => "status",
            ),
            "conditions" => Array(
                "file_name" => $rowHead["file_name"],
            ),
        );

        $resultTypeB = $this->_Model["DutyShiftImportTypeb"]->find($cond);
        if (!$resultTypeB) {
            return false;
        }

        //特別処理
        switch ($resultTypeB[0]["data_type"]) {
            case "1"://入院の場合はデータを削除
                if (!$this->delTypeBDbDetail($rowHead, $resultTypeB[0])) {
                    $this->Rollback();
                    $this->updateStatus($rowHead, "9");
                    return false;
                }
                $this->updateStatus($rowHead, "3");
                return true;
            case "5"://退院の場合は退院日時は消去し、移動データは削除する。
                if (!$this->delTypeBDb5Detail($rowHead, $resultTypeB[0])) {
                    $this->Rollback();
                    $this->updateStatus($rowHead, "9");
                    return false;
                }
                $this->updateStatus($rowHead, "3");
                return true;
        }

        /**
         * typeBテーブルを読み込む
         * 患者コードとデータ区分が同じで処理済のデータ
         */
        if ($resultTypeB[0]["data_type"] == "3") {
            $dataType = "IN('1','2','3')";
        } else {
            $dataType = $resultTypeB[0]["data_type"];
        }
        $condBefore = Array(
            "fields" => Array(
                "data_type" => "data_type",
                "move_date" => "move_date",
                "move_time" => "move_time",
                "ward_cd" => "ward_cd",
                "department_cd" => "department_cd",
                "sickroom_cd" => "sickroom_cd",
                "attending_physician" => "attending_physician",
                "disease_name" => "disease_name",
                "status" => "status",
            ),
            "conditions" => Array(
                "status" => "1",
                "proc_Class" => "1",
                "patient_cd" => $rowHead["patient_cd"],
                "data_type" => $dataType,
            ),
            "order" => Array(
                "file_name" => "DESC",
            )
        );

        $resultBeforeTypeB = $this->_Model["DutyShiftImportTypeb"]->find($condBefore);
        if (!$resultBeforeTypeB) {
            $this->_Logger->PutInfo("前のデータの検索に失敗しました");
            $this->updateStatus($rowHead, "9");
            return false;
        }

        if (count($resultBeforeTypeB) < 2) {
            switch ($resultTypeB[0]["data_type"]) {
                case "6"://主治医変更で前のデータがない場合
                    if (!$this->delTypeBDb6Detail($rowHead, $resultTypeB[0])) {
                        $this->Rollback();
                        $this->updateStatus($rowHead, "9");
                        return false;
                    }
                    $this->updateStatus($rowHead, "3");
                    return true;
                default:
                    $this->_Logger->PutInfo("前のデータがないため削除できません");
                    $this->updateStatus($rowHead, "9");
                    return false;
            }
        }

        $this->BeginTransaction();

        //転棟の場合
        switch ($resultTypeB[0]["data_type"]) {
            case "3"://転棟
                if (!$this->updTypeBDb3Detail($rowHead, $resultBeforeTypeB[1])) {
                    $this->Rollback();
                    $this->updateStatus($rowHead, "9");
                    return false;
                }
                $this->Commit();
                return true;
        }

        /**
         * ファイル名の降順の2件目を取得
         */
        switch ($resultBeforeTypeB[1]["data_type"]) {
            case "2"://転棟の取り消し
                //新しいレコードを消して、古いレコードの退院転出日時欄を消す
                if (!$this->delTypeBDbDetail($rowHead, $resultTypeB[0])) {
                    $this->Rollback();
                    $this->updateStatus($rowHead, "9");
                    return false;
                }
                if (!$this->updTypeBDb2Detail($rowHead, $resultBeforeTypeB[1], false)) {
                    $this->Rollback();
                    $this->updateStatus($rowHead, "9");
                    return false;
                }
                break;
            case "6"://主治医変更　前のデータがある場合
                if (!$this->updTypeBDb6Detail($rowHead, $resultBeforeTypeB[1])) {
                    $this->Rollback();
                    $this->updateStatus($rowHead, "9");
                    return false;
                }
                break;
            case "7":                //入院病名変更
                if (!$this->updTypeBDb7Detail($rowHead, $resultBeforeTypeB[1])) {
                    $this->Rollback();
                    $this->updateStatus($rowHead, "9");
                    return false;
                }
                break;
        }

        if (!$this->updateStatus($rowHead, "3")) {
            $this->Rollback();
            return false;
        }

        $this->Commit();
        return true;
    }

    /**
     * TYPEBのデータの更新1詳細　入院
     */
    private function updTypeBDb1Detail($rowHead, $rowBody) {

        //前の病棟コードを取得
        $beforePatientData = $this->getBeforePatientInfo($rowHead, $rowBody);

        if ($rowBody["ward_cd"] == "") {
            $wardCd = "*";
        } else {
            $wardCd = $rowBody["ward_cd"];
        }

        $condPatient = Array(
            "data" => Array(
                "department_cd" => $rowBody["department_cd"],
                "sickroom_cd" => $rowBody["sickroom_cd"],
                "attending_physician" => $rowBody["attending_physician"],
                "data_type1" => $rowBody["data_type"],
                "del_flg" => "0",
            ),
            "conditions" => Array(
                "ward_cd" => $wardCd,
                "patient_cd" => $rowHead["patient_cd"],
                "in_move_date" => $rowBody["move_date"],
                "in_move_time" => $rowBody["move_time"],
            ),
        );

        //前のデータが順番違い出来ていたら
        if (trim($beforePatientData[0]["in_move_date"]) == "*") {
            $condPatient["data"]["sickroom_cd"] = $beforePatientData[0]["sickroom_cd"];
            $condPatient["data"]["sickbed_cd"] = $beforePatientData[0]["sickbed_cd"];
            $condPatient["data"]["admission_form"] = $beforePatientData[0]["admission_form"];
            $condPatient["data"]["attending_physician"] = $beforePatientData[0]["attending_physician"];
            $condPatient["data"]["disease_name"] = $beforePatientData[0]["disease_name"];
            $condPatient["data"]["articles"] = $beforePatientData[0]["articles"];
            $condPatient["data"]["outcome_cd"] = $beforePatientData[0]["outcome_cd"];

            //前の不要データを削除する
            $condDelete = Array(
                "conditions" => array(
                    "patient_cd" => $beforePatientData[0]["patient_cd"],
                    "ward_cd" => $beforePatientData[0]["ward_cd"],
                    "in_move_date" => $beforePatientData[0]["in_move_date"],
                    "in_move_time" => $beforePatientData[0]["in_move_time"],
                ),
            );
            $resultPatient = $this->_Model["DutyShiftImportPatientWard"]->Delete($condDelete);
        }

        $resultPatient = $this->_Model["DutyShiftImportPatientWard"]->Save($condPatient);
        if (!$resultPatient) {
            return false;
        }

        $cond = Array(
            "data" => Array(
                "ward_cd" => $rowBody["ward_cd"],
            ),
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
                "data_type" => $rowBody["data_type"],
                "move_date" => $rowBody["move_date"],
                "move_time" => $rowBody["move_time"],
            ),
        );
        $result = $this->_Model["DutyShiftImportChangeInformation"]->Save($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * 前の患者情報を取得
     */
    private function getBeforePatientInfo($rowHead, $rowBody) {

        $condBeforePatient = Array(
            "fields" => Array(
                "patient_cd" => "patient_cd",
                "ward_cd" => "ward_cd",
                "in_move_date" => "in_move_date",
                "in_move_time" => "in_move_time",
                "out_move_date" => "out_move_date",
                "out_move_time" => "out_move_time",
                "department_cd" => "department_cd",
                "sickroom_cd" => "sickroom_cd",
                "sickbed_cd" => "sickbed_cd",
                "admission_form" => "admission_form",
                "attending_physician" => "attending_physician",
                "disease_name" => "disease_name",
                "articles" => "articles",
                "outcome_cd" => "outcome_cd",
            ),
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
            ),
            "order" => Array(
                "in_move_date" => "DESC",
                "in_move_time" => "DESC",
            ),
        );
       
//        if ($rowBody["ward_cd"] != "") {
//            $condBeforePatient["conditions"]["ward_cd"] = $rowBody["ward_cd"];
//        }

        return $this->_Model["DutyShiftImportPatientWard"]->Find($condBeforePatient);
    }

    /**
     * TYPEBのデータの更新2詳細　転棟
     */
    private function updTypeBDb2Detail($rowHead, $rowBody, $isAdd) {

        $resultBefore = array();

        /**
         * 処理区分が追加時
         */
        if ($isAdd) {
            /**
             * 前の病棟コードを取得
             */
            $resultBefore = $this->getBeforePatientInfo($rowHead, $rowBody);
            if (!$resultBefore) {
                $this->_Logger->PutInfo("病棟が決定していないため転棟できません。" . $rowBody["patient_cd"]);
                return false;
            }

            /**
             * 前の病棟コードを転出にする
             */
            $condPatient = Array(
                "data" => Array(
                    "out_move_date" => $rowBody["move_date"],
                    "out_move_time" => $rowBody["move_time"],
                    "data_type2" => "2",
                    "del_flg" => "0",
                ),
                "conditions" => Array(
                    "patient_cd" => $rowHead["patient_cd"],
                    "ward_cd" => $resultBefore[0]["ward_cd"],
                    "in_move_date" => $resultBefore[0]["in_move_date"],
                    "in_move_time" => $resultBefore[0]["in_move_time"],
                ),
            );

            $resultPatient = $this->_Model["DutyShiftImportPatientWard"]->Save($condPatient);
            if (!$resultPatient) {
                return false;
            }
        }

        /**
         * 患者マスタを転入で登録
         */
        $condPatient = Array(
            "data" => Array(
                "out_move_date" => "",
                "out_move_time" => "",
                "department_cd" => $rowBody["department_cd"],
                "data_type1" => "2",
                "del_flg" => "0",
            ),
            "conditions" => Array(
                "ward_cd" => $rowBody["ward_cd"],
                "patient_cd" => $rowHead["patient_cd"],
                "in_move_date" => $rowBody["move_date"],
                "in_move_time" => $rowBody["move_time"],
            ),
        );

        /**
         * 前のデータがある場合は下記を引き継ぐ
         * 診療科コード	X(2)	department_cd
         * 病室コード	X(6)	sickroom_cd
         * 病床コード	X(6)	sickbed_cd
         * 入院形態	X(1)	admission_form
         * 主治医名	X(20)	attending_physician
         * 病名         X(40)	disease_name
         */
        if (count($resultBefore) > 0) {
            $condPatient["data"]["department_cd"] = $resultBefore[0]["department_cd"];
            $condPatient["data"]["sickroom_cd"] = $resultBefore[0]["sickroom_cd"];
            $condPatient["data"]["sickbed_cd"] = $resultBefore[0]["sickbed_cd"];
            $condPatient["data"]["admission_form"] = $resultBefore[0]["admission_form"];
            $condPatient["data"]["attending_physician"] = $resultBefore[0]["attending_physician"];
            $condPatient["data"]["disease_name"] = $resultBefore[0]["disease_name"];
        }

        $resultPatient = $this->_Model["DutyShiftImportPatientWard"]->Save($condPatient);
        if (!$resultPatient) {
            return false;
        }

        /**
         * 患者移動情報を更新
         */
        $cond = Array(
            "data" => Array(
                "ward_cd" => $rowBody["ward_cd"],
            ),
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
                "data_type" => $rowBody["data_type"],
                "move_date" => $rowBody["move_date"],
                "move_time" => $rowBody["move_time"],
            ),
        );
        $result = $this->_Model["DutyShiftImportChangeInformation"]->Save($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * TYPEBのデータの更新3詳細　転科
     */
    private function updTypeBDb3Detail($rowHead, $rowBody) {

        /**
         * DutyShiftImportPatientWardより前データを取得
         */
        $resultBefore = $this->getBeforePatientInfo($rowHead, $rowBody);

        if (!$resultBefore) {
            $this->_Logger->PutInfo("病棟が決定していないため転科できません。");
            return false;
        } else {
            $inMoveDate = $resultBefore[0]["in_move_date"];
            $inMoveTime = $resultBefore[0]["in_move_time"];
            $wardCd = $resultBefore[0]["ward_cd"];
            if (trim($resultBefore[0]["out_move_date"]) != "") {
                $this->_Logger->PutInfo("退院しているので転科できません。");
                return false;
            }
        }

        /**
         * 患者マスタを更新
         */
        $condPatient = Array(
            "data" => Array(
                "department_cd" => $rowBody["department_cd"],
                "del_flg" => "0",
            ),
            "conditions" => Array(
                "ward_cd" => $wardCd,
                "patient_cd" => $rowHead["patient_cd"],
                "in_move_date" => $inMoveDate,
                "in_move_time" => $inMoveTime,
            ),
        );

        $resultPatient = $this->_Model["DutyShiftImportPatientWard"]->Save($condPatient);
        if (!$resultPatient) {
            return false;
        }

        return true;
    }

    /**
     * TYPEBのデータの更新5詳細　退院
     */
    private function updTypeBDb5Detail($rowHead, $rowBody) {

        $resultBefore = $this->getBeforePatientInfo($rowHead, $rowBody);

        if ($resultBefore[0]["in_move_date"] . $resultBefore[0]["in_move_time"] > $rowBody["move_date"] . $rowBody["move_time"]) {
            $this->_Logger->PutInfo("入院日と退院日が逆転しているため退院できません。");
            return false;
        }

        /**
         * 患者マスタを更新
         */
        $condPatient = Array(
            "data" => Array(
                "out_move_date" => $rowBody["move_date"],
                "out_move_time" => $rowBody["move_time"],
                "data_type2" => "5",
            ),
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
                //"ward_cd" => $rowBody["ward_cd"],
                "ward_cd" => $resultBefore[0]["ward_cd"],
                "in_move_date" => $resultBefore[0]["in_move_date"],
                "in_move_time" => $resultBefore[0]["in_move_time"],
            ),
        );
        $resultPatient = $this->_Model["DutyShiftImportPatientWard"]->Save($condPatient);
        if (!$resultPatient) {
            return false;
        }

        /**
         * 患者移動情報を更新
         */
        $cond = Array(
            "data" => Array(
                "ward_cd" => $rowBody["ward_cd"],
            ),
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
                "data_type" => $rowBody["data_type"],
                "move_date" => $rowBody["move_date"],
                "move_time" => $rowBody["move_time"],
            ),
        );
        $result = $this->_Model["DutyShiftImportChangeInformation"]->Save($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * 退院
     */
    private function delTypeBDb5Detail($rowHead, $rowBody) {

        $resultBefore = $this->getBeforePatientInfo($rowHead, $rowBody);

        /**
         * 患者マスタを更新
         */
        $condPatient = Array(
            "data" => Array(
                "out_move_date" => "",
                "out_move_time" => "",
                "data_type2" => "8",
            ),
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
                "ward_cd" => $resultBefore[0]["ward_cd"],
                //"ward_cd" => $rowBody["ward_cd"],
                "in_move_date" => $resultBefore[0]["in_move_date"],
                "in_move_time" => $resultBefore[0]["in_move_time"],
            ),
        );
        $resultPatient = $this->_Model["DutyShiftImportPatientWard"]->Save($condPatient);
        if (!$resultPatient) {
            return false;
        }

        $cond = Array(
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
                "data_type" => "5",
                "move_date" => $rowBody["move_date"],
                "move_time" => $rowBody["move_time"],
            ),
        );
        $result = $this->_Model["DutyShiftImportChangeInformation"]->Delete($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * 主治医変更　前のデータがない場合
     * @param type $rowHead
     * @param type $rowBody
     * @return boolean
     */
    private function delTypeBDb6Detail($rowHead, $rowBody) {

        $resultBefore = $this->getBeforePatientInfo($rowHead, $rowBody);
        if (!$resultBefore) {
            $this->_Logger->PutInfo("病棟が決定していないため主治医変更できません。");
            return false;
        } else {
            $inMoveDate = $resultBefore[0]["in_move_date"];
            $inMoveTime = $resultBefore[0]["in_move_time"];
            $wardCd = $resultBefore[0]["ward_cd"];
            if (trim($resultBefore[0]["out_move_date"]) != "") {
                $this->_Logger->PutInfo("退院しているので主治医変更できません。");
                return false;
            }
        }

        /**
         * 患者マスタを更新
         */
        $condPatient = Array(
            "data" => Array(
                "attending_physician" => "", //主治医欄を消去
            ),
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
                "ward_cd" => $wardCd,
                "in_move_date" => $inMoveDate,
                "in_move_time" => $inMoveTime
            ),
        );
        $resultPatient = $this->_Model["DutyShiftImportPatientWard"]->Save($condPatient);
        if (!$resultPatient) {
            return false;
        }

        return true;
    }

    /**
     * TYPEBのデータの更新6詳細　主治医変更
     */
    private function updTypeBDb6Detail($rowHead, $rowBody) {

        /**
         * DutyShiftImportPatientWardより前データを取得
         */
        $resultBefore = $this->getBeforePatientInfo($rowHead, $rowBody);
        if (!$resultBefore) {
            $this->_Logger->PutInfo("病棟が決定していないため主治医変更できません。");
            return false;
        } else {
            $inMoveDate = $resultBefore[0]["in_move_date"];
            $inMoveTime = $resultBefore[0]["in_move_time"];
            $wardCd = $resultBefore[0]["ward_cd"];
            if (trim($resultBefore[0]["out_move_date"]) != "") {
                $this->_Logger->PutInfo("退院しているので主治医変更できません。");
                return false;
            }
        }

        /**
         * 患者マスタを更新
         */
        $condPatient = Array(
            "data" => Array(
                "attending_physician" => $rowBody["attending_physician"],
                "del_flg" => "0",
            ),
            "conditions" => Array(
                "ward_cd" => $wardCd,
                "patient_cd" => $rowHead["patient_cd"],
                "in_move_date" => $inMoveDate,
                "in_move_time" => $inMoveTime,
            ),
        );

        $resultPatient = $this->_Model["DutyShiftImportPatientWard"]->Save($condPatient);
        if (!$resultPatient) {
            return false;
        }

        return true;
    }

    /**
     * TYPEBのデータの更新7詳細　入院病名変更
     */
    private function updTypeBDb7Detail($rowHead, $rowBody) {

        $resultBefore = $this->getBeforePatientInfo($rowHead, $rowBody);
        if (!$resultBefore) {
            $this->_Logger->PutInfo("病棟が決定していないため入院病名変更できません。");
            return false;
        } else {
            $inMoveDate = $resultBefore[0]["in_move_date"];
            $inMoveTime = $resultBefore[0]["in_move_time"];
            $wardCd = $resultBefore[0]["ward_cd"];
        }

        /**
         * 患者マスタを更新
         */
        $condPatient = Array(
            "data" => Array(
                "disease_name" => $rowBody["disease_name"],
                "del_flg" => "0",
            ),
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
                "in_move_date" => $inMoveDate,
                "in_move_time" => $inMoveTime,
            ),
        );
        $resultPatient = $this->_Model["DutyShiftImportPatientWard"]->Save($condPatient);
        if (!$resultPatient) {
            return false;
        }

        return true;
    }

    /**
     * TYPEBのデータの更新7詳細　入院病名変更
     */
    private function delTypeBDbDetail($rowHead, $rowBody) {

        /**
         * 患者マスタを削除
         */
        $condPatient = Array(
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
                "ward_cd" => $rowBody["ward_cd"],
                "in_move_date" => $rowBody["move_date"],
                "in_move_time" => $rowBody["move_time"],
            ),
        );

        $resultPatient = $this->_Model["DutyShiftImportPatientWard"]->Delete($condPatient);
        if (!$resultPatient) {
            return false;
        }

        return true;
    }

}
