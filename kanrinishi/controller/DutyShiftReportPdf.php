<?php

include_once("../../opt/kanrinishi_config.php");
include_once(APP_BASE . "controller/AppController.php");
include_once(APP_BASE . "controller/DutyShiftReportPdfData.php");
include_once(APP_BASE . "controller/DutyShiftReportInputLoader.php");
include_once(APP_BASE . "controller/DutyShiftReportInputItems.php");
include_once(APP_BASE . "util/DateConverter.php");

/**
 * REPORT PDF
 */
class DutyShiftReportPdf extends AppController {

    /**
     * 項目名
     * @var type 
     */
    private $_BasicItems;
    private $_ChargeInformationItems;
    private $_SeriousIllnessItems;
    private $_SeriousInformationItems;
    private $_WorkersInformationItems;
    private $_NightHospitalizationItems;
    private $_NightInformationItems;
    private $_NightPatroltimeItems;

    /**
     * データ
     * @var type 
     */
    private $_BasicData;
    private $_ChargeInformationData;
    private $_SeriousIllnessData;
    private $_SeriousInformationData;
    private $_WorkersInformationData;
    private $_NightHospitalizationData;
    private $_NightInformationData;
    private $_NightPatroltimeData;

    /**
     * メイン
     */
    public function Main($post) {

//        ini_set("display_errors", "On");
//        ini_set("error_reporting", E_ALL);

        $this->setInputItems();
        $this->inputItem2DataArr($post);

        $pdfData = new DutyShiftReportPdfData();
        $pdfData->setHeaderData($this->getHeaderData());
        $pdfData->setBasicData($this->getBasicData());
        $pdfData->setChangeInformationData($this->getChangeInformationData());
        $pdfData->setSeriousIllnessData($this->getSeriousIllnessData());
        $pdfData->setSeriousInformationData($this->getSeriousInformationData());
        $pdfData->setWorkersInformationData($this->getWorkersInformationData());
        $pdfData->setNightHospitalizationData($this->getNightHospitalizationData());
        $pdfData->setNightInformationData($this->getNightInformationData());
        $pdfData->setNightPatroltimeData($this->getNightPatroltimeData());
        $pdfData->Make();
    }

    /**
     * 入力項目名設定
     */
    private function setInputItems() {

        $inputItems = new DutyShiftReportInputItems();

        $this->_BasicItems = $inputItems->GetBasicItems();
        $this->_ChargeInformationItems = $inputItems->GetChangeInformationItems();
        $this->_SeriousIllnessItems = $inputItems->GetSeriousIllnessItems();
        $this->_SeriousInformationItems = $inputItems->GetSeriousInformationItems();
        $this->_WorkersInformationItems = $inputItems->GetWorkersInformationItems();
        $this->_NightHospitalizationItems = $inputItems->GetNightHospitalizationItems();
        $this->_NightInformationItems = $inputItems->GetNightInformationItems();
        $this->_NightPatroltimeItems = $inputItems->GetNightPatroltimeItems();
    }

    /**
     * 変換
     * @param type $post
     */
    private function inputItem2DataArr($post) {

        $inputLoader = new DutyShiftReportInputLoader();
        $this->_BasicData = $inputLoader->InputItemBasic2DataArr($post);
        $this->_ChargeInformationData = $inputLoader->InputItemChangeInformation2DataArr($post);
        $this->_SeriousIllnessData = $inputLoader->InputItemSeriousIllness2DataArr($post);
        $this->_SeriousInformationData = $inputLoader->InputItemSeriousInformation2DataArr($post);
        $this->_WorkersInformationData = $inputLoader->InputItemWorkersInformation2DataArr($post);
        $this->_NightHospitalizationData = $inputLoader->InputItemNightHospitalization2DataArr($post);
        $this->_NightInformationData = $inputLoader->InputItemNightInformation2DataArr($post);
        $this->_NightPatroltimeData = $inputLoader->InputItemNightPatroltime2DataArr($post);

        return true;
    }

    /**
     * ヘッダデータの取得
     * 病棟名、指定年月日、日勤責任者、夜勤責任者
     */
    private function getHeaderData() {

        $data = null;

//        $data["shift_group"] = $this->_BasicData[0]["shift_group"];
        $data["shift_group"] = $this->_BasicData[0]["group_name"];
        $reportDate = $this->_BasicData[0]["report_date"];
        $data["report_date"] = DateConverter::ConvWarekiDate($reportDate) . " " .
                DateConverter::GetDayoftheweek($reportDate) . "曜日";
        //$data["day_shift_name"] = "日勤責任者：" . $this->_BasicData[0]["day_shift_name"];
        //$data["night_shift_name"] = "夜勤責任者：" . $this->_BasicData[0]["night_shift_name"];

        return $data;
    }

    /**
     * 基本情報の取得
     * @return type
     */
    private function getBasicData() {

        $data = null;

        /**
         * 定数~付添
         */
        $data["num"][0] = array(
            $this->_BasicData[0]["constant"],
            $this->_BasicData[0]["before_num"],
            $this->_BasicData[0]["current_num"],
            $this->_BasicData[0]["hostpitaladmissions_num"],
            $this->_BasicData[0]["transference_num"],
            $this->_BasicData[0]["discharge_num"],
            $this->_BasicData[0]["moving_out_num"],
            $this->_BasicData[0]["sleepover_num"],
            $this->_BasicData[0]["goingout_num"],
            $this->_BasicData[0]["attendant_num"],
        );

        /**
         * 診療科目名
         */
        $data["name"][1] = array(
            $this->_BasicData[0]["course_name1"],
            $this->_BasicData[0]["course_name2"],
            $this->_BasicData[0]["course_name3"],
            $this->_BasicData[0]["course_name4"],
            $this->_BasicData[0]["course_name5"],
            $this->_BasicData[0]["course_name6"],
            $this->_BasicData[0]["palliativecare_name"],
                //$this->_BasicData[0]["course_name7"],
        );

        /**
         * 診療科目別人数
         */
        $data["num"][1] = array(
            $this->_BasicData[0]["course_num1"],
            $this->_BasicData[0]["course_num2"],
            $this->_BasicData[0]["course_num3"],
            $this->_BasicData[0]["course_num4"],
            $this->_BasicData[0]["course_num5"],
            $this->_BasicData[0]["course_num6"],
            $this->_BasicData[0]["palliativecare_num"],
                //$this->_BasicData[0]["course_num7"],
        );

        /**
         * 担送~独歩
         */
        $data["num"][2] = array(
            $this->_BasicData[0]["stretcher_num"],
            $this->_BasicData[0]["convoy_num"],
            $this->_BasicData[0]["independent_num"],
        );

        /**
         * 物品点検
         */
        for ($i = 0; $i < 8; $i++) {
            $data["num"][3][$i] = array(
                $this->_BasicData[0]["inspection_name" . ($i + 1)],
                $this->_BasicData[0]["inspection_latenight" . ($i + 1)],
                $this->_BasicData[0]["inspection_dayshift" . ($i + 1)],
                $this->_BasicData[0]["inspection_quasinight" . ( $i + 1)],
            );
        }

        /**
         * 看護度
         */
        $arr = array("a", "b", "c");
        for ($i = 0; $i < count($arr); $i++) {
            $data["num"][4][$i] = array(
                $this->_BasicData[0]["nursing_degree_" . $arr[$i] . "1"],
                $this->_BasicData[0]["nursing_degree_" . $arr[$i] . "2"],
                $this->_BasicData[0]["nursing_degree_" . $arr[$i] . "3"],
                $this->_BasicData[0]["nursing_degree_" . $arr[$i] . "4"],
            );
        }

        /**
         * 患者状況
         */
        for ($i = 0; $i < 4; $i++) {
            $data["num"][5][$i] = array(
                $this->_BasicData[0]["patient_status_name" . ($i + 1)],
                $this->_BasicData[0]["patient_status_num" . ($i + 1)],
            );
        }

        return $data;
    }

    /**
     * 移動情報の取得
     * @return type
     */
    private function getChangeInformationData() {

        $data = null;

        for ($i = 0; $i < count($this->_ChargeInformationData); $i++) {
            foreach ($this->_ChargeInformationItems as $value) {
                if ($value == "patient_name" && $this->_ChargeInformationData[$i][$value] != "") {
                    $data[$i][$value] = $this->_ChargeInformationData[$i][$value] . " 様";
                } else {
                    $data[$i][$value] = $this->_ChargeInformationData[$i][$value];
                }
            }
        }
        return $data;
    }

    /**
     * 重症情報の取得
     * @return type
     */
    private function getSeriousIllnessData() {

        $data = null;

        for ($i = 0; $i < count($this->_SeriousIllnessData); $i++) {
            foreach ($this->_SeriousIllnessItems as $value) {
                if ($value == "patient_name" && $this->_SeriousIllnessData[$i][$value] != "") {
                    $data[$i][$value] = $this->_SeriousIllnessData[$i][$value] . " 様";
                } else {
                    $data[$i][$value] = $this->_SeriousIllnessData[$i][$value];
                }
            }
        }
        return $data;
    }

    /**
     * 重症_日勤帯特記事項
     * @return type
     */
    private function getSeriousInformationData() {

        $data = null;

        for ($i = 0; $i < count($this->_SeriousInformationData); $i++) {
            foreach ($this->_SeriousInformationItems as $value) {
                $data[$i][$value] = $this->_SeriousInformationData[$i][$value];
            }
        }

        return $data;
    }

    /**
     * 勤務者情報の取得
     * @return type
     */
    private function getWorkersInformationData() {

        $data = null;

        for ($i = 0; $i < count($this->_WorkersInformationData); $i++) {
            foreach ($this->_WorkersInformationItems as $value) {
                $data[$i][$value] = $this->_WorkersInformationData[$i][$value];
            }
        }
        return $data;
    }

    /**
     * 夜間入退院情報の取得
     * @return type
     */
    private function getNightHospitalizationData() {

        $data = null;

        for ($i = 0; $i < count($this->_NightHospitalizationData); $i++) {
            foreach ($this->_NightHospitalizationItems as $value) {
                if ($value == "patient_name" && $this->_NightHospitalizationData[$i][$value] != "") {
                    $data[$i][$value] = $this->_NightHospitalizationData[$i][$value] . " 様";
                } else {
                    $data[$i][$value] = $this->_NightHospitalizationData[$i][$value];
                }
            }
        }
        return $data;
    }

    /**
     * 夜間情報の取得
     * @return type
     */
    private function getNightInformationData() {

        $data = null;

        for ($i = 0; $i < count($this->_NightInformationData); $i++) {
            foreach ($this->_NightInformationItems as $value) {
                $data[$i][$value] = $this->_NightInformationData[$i][$value];
            }
        }

        return $data;
    }

    /**
     * 巡視時間の取得
     * @return type
     */
    private function getNightPatroltimeData() {

        $data = null;

        for ($i = 0; $i < count($this->_NightPatroltimeData); $i++) {
            foreach ($this->_NightPatroltimeItems as $value) {
                if ($value == "abnormality") {
                    switch ($this->_NightPatroltimeData[$i][$value]) {
                        case "1":
                            $data[$i][$value] = "異常なし";
                            break;
                        case "2":
                            $data[$i][$value] = "異常あり";
                            break;
                        default:
                            $data[$i][$value] = "";
                            break;
                    }
                } else {
                    $data[$i][$value] = $this->_NightPatroltimeData[$i][$value];
                }
            }
        }

        return $data;
    }

}

$pdf = new DutyShiftReportPdf();
$pdf->Main(filter_input_array(INPUT_POST));
