<?php

include_once(APP_BASE . "controller/AppController.php");

/**
 * データシフトクラス
 */
class DataShift extends AppController {

    /**
     * コンストラクタ
     */
    public function __construct($programName) {

        parent::__construct();

        $this->_ProgramName = $programName;
        $this->Uses(
                array(
                    "DutyShiftImportPatient",
                    "DutyShiftImportChangeInformation",
                    "DutyShiftImportGooutInformation",
                )
        );
    }

    /**
     * メイン
     */
    public function Main() {

        $this->_Logger->PutInfo("処理を開始しました。");

        $targetDate = $this->getTargetDate();

        $this->BeginTransaction();

        if (!$this->shiftPatient($targetDate)) {
            $this->Rollback();
            $this->_Logger->PutErr("shiftPatientで処理が異常終了しました。");
            return false;
        }

        if (!$this->shiftChargeInformation($targetDate)) {
            $this->Rollback();
            $this->_Logger->PutErr("shiftChargeInformationで処理が異常終了しました。");
            return false;
        }

        if (!$this->shiftGooutInformation($targetDate)) {
            $this->Rollback();
            $this->_Logger->PutErr("shiftGooutInformationで処理が異常終了しました。");
            return false;
        }

        $this->Commit();

        $this->_Logger->PutInfo("処理を終了しました。");

        return true;
    }

    /**
     * 前日の日付の取得
     */
    private function getTargetDate() {

        return date("Ymd", strtotime("-1 day"));
    }

    /**
     * 患者情報
     */
    private function shiftPatient($targetDate) {

        /**
         * Patientテーブルを読み込む
         */
        $cond = Array(
            "fields" => Array(
                "patient_cd" => "patient_cd",
                "name_kana" => "name_kana",
                "name_kanji" => "name_kanji",
                "sex" => "sex",
                "birthday" => "birthday",
                "admission_form" => "admission_form",
                "ward_cd" => "ward_cd",
                "before_ward_cd" => "before_ward_cd",
                "department_cd" => "department_cd",
                "sickroom_cd" => "sickroom_cd",
                "sickbed_cd" => "sickbed_cd",
                "attending_physician" => "attending_physician",
                "disease_name" => "disease_name",
                "contact" => "contact",
                "nursing_degree" => "nursing_degree",
                "aid_classification" => "aid_classification",
                "del_flg" => "del_flg",
            ),
            "conditions" => Array(
                "info_date" => "99999999",
            ),
        );
        $resultPatient = $this->_Model["DutyShiftImportPatient"]->Find($cond);
        if (!$resultPatient) {
            $this->_Logger->PutErr("Patientデータが取得できませんでした。");
            return false;
        }

        foreach ($resultPatient as $row) {
            $this->registPatient($targetDate, $row);
        }

        return true;
    }

    /**
     * 登録
     * @param type $targetDate
     * @param type $row
     * @return boolean
     */
    private function registPatient($targetDate, $row) {

        $cond = Array(
            "data" => Array(
                "name_kana" => $row["name_kana"],
                "name_kanji" => $row["name_kanji"],
                "sex" => $row["sex"],
                "birthday" => $row["birthday"],
                "admission_form" => $row["admission_form"],
                "ward_cd" => $row["ward_cd"],
                "before_ward_cd" => $row["before_ward_cd"],
                "department_cd" => $row["department_cd"],
                "sickroom_cd" => $row["sickroom_cd"],
                "sickbed_cd" => $row["sickbed_cd"],
                "attending_physician" => $row["attending_physician"],
                "disease_name" => $row["disease_name"],
                "contact" => $row["contact"],
                "nursing_degree" => $row["nursing_degree"],
                "aid_classification" => $row["aid_classification"],
                "del_flg" => "0",
            ),
            "conditions" => Array(
                "info_date" => $targetDate,
                "patient_cd" => $row["patient_cd"],
            ),
        );
        $result = $this->_Model["DutyShiftImportPatient"]->Save($cond);
        if (!$result) {
            $this->_Logger->PutErr("Patientの保存ができませんでした。");
            return false;
        }

        return true;
    }

    /**
     * 患者移動情報
     */
    private function shiftChargeInformation($targetDate) {

        /**
         * 患者移動情報テーブルを読み込む
         */
        $cond = Array(
            "fields" => Array(
                "patient_cd" => "patient_cd",
                "data_type" => "data_type",
                "move_date" => "move_date",
                "move_time" => "move_time",
            ),
            "conditions" => Array(
                "info_date" => "99999999",
            ),
        );
        $resultChangeInformation = $this->_Model["DutyShiftImportChangeInformation"]->find($cond);
        if (!$resultChangeInformation) {
            return false;
        }

        foreach ($resultChangeInformation as $row) {
            $this->registChangeInformation($targetDate, $row);
        }

        return true;
    }

    /**
     * 患者移動情報登録
     */
    private function registChangeInformation($targetDate, $row) {

        $cond = Array(
            "data" => Array(
                "data_type" => $row["data_type"],
                "move_date" => $row["move_date"],
                "move_time" => $row["move_time"],
            ),
            "conditions" => Array(
                "info_date" => $targetDate,
                "patient_cd" => $row["patient_cd"],
            ),
        );
        $result = $this->_Model["DutyShiftImportChangeInformation"]->Save($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * 外泊・外出情報
     */
    private function shiftGooutInformation($targetDate) {

        /**
         * 外泊・外出情報テーブルを読み込む
         */
        $cond = Array(
            "fields" => Array(
                "patient_cd" => "patient_cd",
                "data_type" => "data_type",
                "goout_date_from" => "goout_date_from",
                "goout_time_from" => "goout_time_from",
                "goout_date_to" => "goout_date_to",
                "goout_time_to" => "goout_time_to",
            ),
            "conditions" => Array(
                "info_date" => "99999999",
            ),
        );
        $resultChangeInformation = $this->_Model["DutyShiftImportGooutInformation"]->find($cond);
        if (!$resultChangeInformation) {
            return false;
        }

        foreach ($resultChangeInformation as $row) {
            $this->registGooutInformation($targetDate, $row);
        }

        return true;
    }

    /**
     * 外泊・外出情報登録
     */
    private function registGooutInformation($targetDate, $row) {

        $cond = Array(
            "data" => Array(
                "data_type" => $row["data_type"],
                "goout_date_from" => $row["goout_date_from"],
                "goout_time_from" => $row["goout_time_from"],
                "goout_date_to" => $row["goout_date_to"],
                "goout_time_to" => $row["goout_time_to"],
            ),
            "conditions" => Array(
                "info_date" => $targetDate,
                "patient_cd" => $row["patient_cd"],
            ),
        );
        $result = $this->_Model["DutyShiftImportGooutInformation"]->Save($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

}
