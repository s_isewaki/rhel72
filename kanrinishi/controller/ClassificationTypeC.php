<?php

include_once(APP_BASE . "controller/AppController.php");

/**
 * データ種別C分類クラス
 */
class ClassificationTypeC extends AppController {

    /**
     * コンストラクタ
     */
    public function __construct($programName) {

        parent::__construct();

        $this->_ProgramName = $programName;
        $this->Uses(
                array(
                    "DutyShiftImportHeader",
                    "DutyShiftImportTypec",
                    "DutyShiftImportGooutInformation",
                )
        );
    }

    /**
     * TYPE Cの処理
     */
    public function AddTypeCDb($rowHead) {

        /**
         * typeCテーブルを読み込む
         */
        $cond = Array(
            "fields" => Array(
                "data_type" => "data_type",
                "goout_date" => "goout_date",
                "goout_time" => "goout_time",
                "return_date" => "return_date",
                "return_time" => "return_time",
                "status" => "status",
            ),
            "conditions" => Array(
                "file_name" => $rowHead["file_name"],
            ),
        );

        $resultTypeC = $this->_Model["DutyShiftImportTypec"]->find($cond);
        if (!$resultTypeC) {
            return false;
        }

        $this->BeginTransaction();
        switch ($resultTypeC[0]["data_type"]) {
            case "1":
            case "2":
                if (!$this->updTypeCDb12Detail($rowHead, $resultTypeC[0])) {
                    $this->Rollback();
                    $this->_Logger->PutInfo("ロールバックしました" . $rowHead["file_name"]);

                    return false;
                }
                break;
            case "3":
                if (!$this->updTypeCDb3Detail($rowHead, $resultTypeC[0])) {
                    $this->Rollback();
                    $this->_Logger->PutInfo("ロールバックしました" . $rowHead["file_name"]);
                    return false;
                }
                break;
        }

        if (!$this->updateStatus($rowHead, "1")) {
            $this->Rollback();
            return false;
        }

        $this->Commit();
        return true;
    }

    /**
     * 処理ステータスの更新
     * @param type $rowHead
     * @param type $status
     * @return boolean
     */
    private function updateStatus($rowHead, $status) {

        $cond = Array(
            "data" => Array(
                "status" => $status,
            ),
            "conditions" => Array(
                "file_name" => $rowHead["file_name"],
            ),
        );
        $result = $this->_Model["DutyShiftImportHeader"]->Save($cond);
        if (!$result) {
            return false;
        }

        $cond = Array(
            "data" => Array(
                "status" => $status,
            ),
            "conditions" => Array(
                "file_name" => $rowHead["file_name"],
            ),
        );
        $result = $this->_Model["DutyShiftImportTypec"]->Save($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * TYPECのデータの削除
     */
    public function DeleteTypeCDb($rowHead) {

        /**
         * typeCテーブルを読み込む
         */
        $cond = Array(
            "fields" => Array(
                "data_type" => "data_type",
                "goout_date" => "goout_date",
                "status" => "status",
            ),
            "conditions" => Array(
                "file_name" => $rowHead["file_name"],
            ),
        );

        $resultTypeC = $this->_Model["DutyShiftImportTypec"]->find($cond);
        if (!$resultTypeC) {
            return false;
        }

        /**
         * typeCテーブルを読み込む
         * 患者コードとデータ区分が同じで処理済のデータ
         */
        $condBefore = Array(
            "fields" => Array(
                "data_type" => "data_type",
                "goout_date" => "goout_date",
                "goout_time" => "goout_time",
                "return_date" => "return_date",
                "return_time" => "return_time",
                "status" => "status",
            ),
            "conditions" => Array(
                "status" => "1",
                "patient_cd" => $rowHead["patient_cd"],
                "data_type" => $resultTypeC[0]["data_type"],
            ),
            "order" => Array(
                "file_name" => "DESC",
            )
        );

        $resultBeforeTypeC = $this->_Model["DutyShiftImportTypec"]->find($condBefore);

        if (!$resultBeforeTypeC) {
            $this->_Logger->PutInfo("前のデータがないため削除できません");
            return false;
        }

        if (count($resultBeforeTypeC) < 2) {
            switch ($resultTypeC[0]["data_type"]) {
                case "1":
                case "2":
                    //前のデータがない場合はデータを削除する
                    if (!$this->delTypeCDbDetail($rowHead, $resultTypeC[0])) {
                        $this->Rollback();
                        return false;
                    }
                    return true;
                default:
                    $this->_Logger->PutInfo("前のデータがないため削除できません");
                    return false;
            }
        }

        $this->BeginTransaction();
        switch ($resultBeforeTypeC[1]["data_type"]) {
            case "1":
            case "2":
                if (!$this->delTypeCDbDetail($rowHead, $resultBeforeTypeC[0])) {
                    $this->Rollback();
                    return false;
                }
                break;
            case "3":
                if (!$this->updTypeCDb3Detail($rowHead, $resultBeforeTypeC[1])) {
                    $this->Rollback();
                    return false;
                }
                break;
        }

        if (!$this->updateStatus($rowHead, "3")) {
            $this->Rollback();
            return false;
        }

        $this->Commit();

        return true;
    }

    /**
     * 外泊・外出
     * @param type $rowHead
     * @param type $rowBody
     * @return boolean
     */
    private function updTypeCDb12Detail($rowHead, $rowBody) {

        $cond = Array(
            "data" => Array(
                "goout_time_from" => $rowBody["goout_time"],
//                "goout_date_to" => "",
//                "goout_time_to" => "",
                "goout_date_to" => $rowBody["return_date"],
                "goout_time_to" => $rowBody["return_time"],
            ),
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
                "data_type" => $rowBody["data_type"],
                "goout_date_from" => $rowBody["goout_date"],
            ),
        );
        $result = $this->_Model["DutyShiftImportGooutInformation"]->Save($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * 帰院
     * 外泊のデータに対して帰院日時を設定
     * @param type $rowHead
     * @param type $rowBody
     * @return boolean
     */
    private function updTypeCDb3Detail($rowHead, $rowBody) {

        $cond = Array(
            "data" => Array(
                "goout_date_to" => $rowBody["return_date"],
                "goout_time_to" => $rowBody["return_time"],
            ),
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
                "data_type" => "1",
                "goout_date_from" => $rowBody["goout_date"],
            ),
        );
        $result = $this->_Model["DutyShiftImportGooutInformation"]->Save($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * 削除
     * @param type $rowHead
     * @param type $rowBody
     * @return boolean
     */
    private function delTypeCDbDetail($rowHead, $rowBody) {

        $cond = Array(
            "conditions" => Array(
                "patient_cd" => $rowHead["patient_cd"],
                "data_type" => $rowBody["data_type"],
                "goout_date_from" => $rowBody["goout_date"],
            ),
        );

        $result = $this->_Model["DutyShiftImportGooutInformation"]->Delete($cond);
        if (!$result) {
            return false;
        }

        return true;
    }

}
