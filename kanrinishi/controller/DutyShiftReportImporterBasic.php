<?php

include_once(APP_BASE . "controller/AppController.php");
include_once(APP_BASE . "util/CodeConverter.php");

/**
 * データ取込クラス
 * 患者数等
 */
class DutyShiftReportImporterBasic extends AppController {

    private $_BasicData;    //基本情報データ
    private $_ErrorMessage = "";

    /**
     * コンストラクタ
     */
    public function __construct() {

        parent::__construct();

        $this->Uses(
                array(
                    "DutyShiftReportBasic",
                    "DutyShiftImportPatientWard",
                    "DutyShiftPlanStaff",
                    "DutyShiftImportNursingDegree",
                    "DutyShiftImportGooutInformation",
                )
        );
        $inputItems = new DutyShiftReportInputItems();
        $this->_BasicItems = $inputItems->GetBasicItems();
    }

    /**
     * エラーメッセージの取得
     * @return type
     */
    public function GetErrorMessage() {

        return $this->_ErrorMessage;
    }

    /**
     * 基本情報の設定
     * @param type $value
     */
    public function SetBasicData($value) {

        $this->_BasicData = $value;
    }

    /**
     * 基本情報のインポート
     */
    public function Import() {

        $data = $this->initializeBasic();

        /**
         * 前日からのデータ取得
         */
        $beforeData = $this->getBeforeBasic();

        /**
         * 定数・前日数
         */
        $data[0]["constant"] = $beforeData[0]["constant"];
        $data[0]["before_num"] = $beforeData[0]["current_num"];

        /**
         * 付添数
         */
        //$data[0]["attendant_num"] = $beforeData[0]["attendant_num"];

        /**
         * 地域包括
         */
        $careNum = $this->getCareCount();
        if ($careNum[0]["num"] > 0) {
//            $data[0]["palliativecare_name"] = "地域包括";
            $data[0]["palliativecare_name"] = AREA_CARE_NAME;
            $data[0]["palliativecare_num"] = $careNum[0]["num"];
        } else {/**         * なければ前日より取得する         */
            $data[0]["palliativecare_name"] = $beforeData[0]["palliativecare_name"];
            $data[0]["palliativecare_num"] = $beforeData[0]["palliativecare_num"];
        }
        /**
         * 患者状況
         * 11/27 空の場合のみセットする
         */
        if ($this->_BasicData[0]["patient_status_num1"] == "" &&
                $this->_BasicData[0]["patient_status_name1"] == "" &&
                $this->_BasicData[0]["patient_status_num2"] == "" &&
                $this->_BasicData[0]["patient_status_name2"] == "" &&
                $this->_BasicData[0]["patient_status_num3"] == "" &&
                $this->_BasicData[0]["patient_status_name3"] == "" &&
                $this->_BasicData[0]["patient_status_num4"] == "" &&
                $this->_BasicData[0]["patient_status_name4"] == "") {
            for ($i = 1; $i <= 4; $i++) {
                $data[0]["patient_status_name" . $i] = $beforeData[0]["patient_status_name" . $i];
                $data[0]["patient_status_num" . $i] = $beforeData[0]["patient_status_num" . $i];
            }
        } else {
            for ($i = 1; $i <= 4; $i++) {
                $data[0]["patient_status_name" . $i] = $this->_BasicData[0]["patient_status_name" . $i];
                $data[0]["patient_status_num" . $i] = $this->_BasicData[0]["patient_status_num" . $i];
            }
        }

        /**
         * 物品点検
         *  11/27 空の場合のみセットする
         */
        if ($this->_BasicData[0]["inspection_name1"] == "" &&
                $this->_BasicData[0]["inspection_name2"] == "" &&
                $this->_BasicData[0]["inspection_name3"] == "" &&
                $this->_BasicData[0]["inspection_name4"] == "" &&
                $this->_BasicData[0]["inspection_name5"] == "" &&
                $this->_BasicData[0]["inspection_name6"] == "" &&
                $this->_BasicData[0]["inspection_name7"] == "" &&
                $this->_BasicData[0]["inspection_name8"] == "" &&
                $this->_BasicData[0]["inspection_latenight1"] == "" &&
                $this->_BasicData[0]["inspection_dayshift1"] == "" &&
                $this->_BasicData[0]["inspection_quasinight1"] == "" &&
                $this->_BasicData[0]["inspection_latenight2"] == "" &&
                $this->_BasicData[0]["inspection_dayshift2"] == "" &&
                $this->_BasicData[0]["inspection_quasinight2"] == "" &&
                $this->_BasicData[0]["inspection_latenight3"] == "" &&
                $this->_BasicData[0]["inspection_dayshift3"] == "" &&
                $this->_BasicData[0]["inspection_quasinight3"] == "" &&
                $this->_BasicData[0]["inspection_latenight4"] == "" &&
                $this->_BasicData[0]["inspection_dayshift4"] == "" &&
                $this->_BasicData[0]["inspection_quasinight4"] == "" &&
                $this->_BasicData[0]["inspection_latenight5"] == "" &&
                $this->_BasicData[0]["inspection_dayshift5"] == "" &&
                $this->_BasicData[0]["inspection_quasinight5"] == "" &&
                $this->_BasicData[0]["inspection_latenight6"] == "" &&
                $this->_BasicData[0]["inspection_dayshift6"] == "" &&
                $this->_BasicData[0]["inspection_quasinight6"] == "" &&
                $this->_BasicData[0]["inspection_latenight7"] == "" &&
                $this->_BasicData[0]["inspection_dayshift7"] == "" &&
                $this->_BasicData[0]["inspection_quasinight7"] == "" &&
                $this->_BasicData[0]["inspection_latenight8"] == "" &&
                $this->_BasicData[0]["inspection_dayshift8"] == "" &&
                $this->_BasicData[0]["inspection_quasinight8"] == "") {
            for ($i = 1; $i <= 8; $i++) {
                $data[0]["inspection_name" . $i] = $beforeData[0]["inspection_name" . $i];
            }
        } else {
            for ($i = 1; $i <= 8; $i++) {
                $data[0]["inspection_name" . $i] = $this->_BasicData[0]["inspection_name" . $i];
                $data[0]["inspection_latenight" . $i] = $this->_BasicData[0]["inspection_latenight" . $i];
                $data[0]["inspection_dayshift" . $i] = $this->_BasicData[0]["inspection_dayshift" . $i];
                $data[0]["inspection_quasinight" . $i] = $this->_BasicData[0]["inspection_quasinight" . $i];
            }
        }

        /**
         * 現在数
         */
        $currentData = $this->getCurrentCount();
        $data[0]["current_num"] = $currentData[0]["num"];

        /**
         * 入院数	hostpitaladmissions_num
         * 転入数	transference_num
         * 退院数	discharge_num
         * 転出数	moving_out_num
         */
        $countData = $this->getMoveCount();
        $data[0]["hostpitaladmissions_num"] = $countData[0]["hostpitaladmissions_num"];
        $data[0]["transference_num"] = $countData[0]["transference_num"];
        $data[0]["discharge_num"] = $countData[0]["discharge_num"];
        $data[0]["moving_out_num"] = $countData[0]["moving_out_num"];

        /**
         * 外泊数	sleepover_num
         * 外出数	goingout_num
         */
        $gooutCount = $this->getGooutCount();
        $data[0]["sleepover_num"] = $gooutCount[0]["sleepover_num"];
        $data[0]["goingout_num"] = $gooutCount[0]["goingout_num"];

        /**
         * 診療科別人数
         * ---
         * 診療科目名1	course_name1
         * 診療科目人数1	course_num1
         * 診療科目名2	course_name2
         * 診療科目人数2	course_num2
         * 診療科目名3	course_name3
         * 診療科目人数3	course_num3
         * 診療科目名4	course_name4
         * 診療科目人数4	course_num4
         * 診療科目名5	course_name5
         * 診療科目人数5	course_num5
         * 診療科目名6	course_name6
         * 診療科目人数6	course_num6
         */
        $departmentCount = $this->getDepartmentCount();

        if (count($departmentCount) > 6) {
            $this->_ErrorMessage = "診療科数オーバーのため、取り込めない診療科が発生しました。";
        }

        $j = 1;
        for ($i = 0; $i < count($departmentCount); $i++) {
            if (!CodeConverter::GetDepatmentName($departmentCount[$i]["department_cd"])) {
                continue;
            }
            $data[0]["course_name" . ($j)] = CodeConverter::GetDepatmentName($departmentCount[$i]["department_cd"]);
            $data[0]["course_num" . ($j)] = $departmentCount[$i]["num"];
            $j++;
        }

        /**
         * 担送、護送、独歩
         * 担送数	stretcher_num
         * 護送数	convoy_num
         * 独歩数	independent_num
         */
//        $aidClassificationCount = $this->getAidClassificationCount();
//        if (isset($aidClassificationCount[0]["num"])) {
//            $data[0]["stretcher_num"] = $aidClassificationCount[0]["num"];
//        } else {
//            $data[0]["stretcher_num"] = "0";
//        }
//        if (isset($aidClassificationCount[1]["num"])) {
//            $data[0]["convoy_num"] = $aidClassificationCount[1]["num"];
//        } else {
//            $data[0]["convoy_num"] = "0";
//        }
//        if (isset($aidClassificationCount[2]["num"])) {
//            $data[0]["independent_num"] = $aidClassificationCount[2]["num"];
//        } else {
//            $data[0]["independent_num"] = "0";
//        }
//        $data[0]["stretcher_num"] = "0";
//        $data[0]["convoy_num"] = "0";
//        $data[0]["independent_num"] = "0";
//
//        if (!$aidClassificationCount) {
//            
//        } else {
//            foreach ($aidClassificationCount as $acCount) {
//                switch ($acCount["nursing_degree"]) {
//                    case "01":
//                        $data[0]["stretcher_num"] = $acCount["num"];
//                        break;
//                    case "02":
//                        $data[0]["convoy_num"] = $acCount["num"];
//                        break;
//                    case "03":
//                        $data[0]["independent_num"] = $acCount["num"];
//                        break;
//                }
//            }
//        }

        $data[0]["stretcher_num"] = 0;
        $data[0]["convoy_num"] = 0;
        $data[0]["independent_num"] = 0;
        $aidClassificationCount = $this->importListAidClassification();
        if (is_array($aidClassificationCount)) {
            foreach ($aidClassificationCount as $acCount) {
                switch ($acCount["nursing_degree"]) {
                    case "01":
                        $data[0]["stretcher_num"] ++;
                        break;
                    case "02":
                        $data[0]["convoy_num"] ++;
                        break;
                    case "03":
                        $data[0]["independent_num"] ++;
                        break;
                }
            }
        }
        /**
         * 看護度
         */
        $nursingDegreeCount = $this->getNursingDegreeCount();
        $nursingDegreeTable = $GLOBALS['nursingDegreeTable'];
        for ($i = 0; $i < count($nursingDegreeCount); $i++) {
            foreach ($nursingDegreeTable as $key => $value) {
                if ($nursingDegreeCount[$i]["nursing_degree"] == $key) {
                    $data[0][$value] = $nursingDegreeCount[$i]["num"];
                }
            }
        }

        return $data;
    }

    //インポート一覧
    public function ImportList() {

        $data = array();

        /**
         * 現在入院患者一覧
         */
        $data["CurrentList"] = $this->importListCurrent();
        if (!$data["CurrentList"]) {
            $data["CurrentList"] = array();
        }
        foreach ($data["CurrentList"] as &$current) {
            $current["department_name"] = CodeConverter::GetDepatmentName($current["department_cd"]);
        }

        /**
         * 診療科別一覧
         */
        $data["DepartmentList"] = $this->importListDepartment();
        if (!$data["DepartmentList"]) {
            $data["DepartmentList"] = array();
        }
        foreach ($data["DepartmentList"] as &$department) {
            $department["department_name"] = CodeConverter::GetDepatmentName($department["department_cd"]);
        }

        /**
         * 救護区分
         * 担送、護送、独歩
         */
        $data["AidClassificationList"] = $this->importListAidClassification();
        if (!$data["AidClassificationList"]) {
            $data["AidClassificationList"] = array();
        }
        foreach ($data["AidClassificationList"] as &$aidClassification) {
            $aidClassification["department_name"] = CodeConverter::GetDepatmentName($aidClassification["department_cd"]);
            $aidClassification["nursing_degree_name"] = CodeConverter::NursingDegreeName($aidClassification["nursing_degree"]);
        }

        /**
         * 入院、転入、退院、転出
         */
        $data["ChangeList"] = $this->importListChangeInformation();
        if (!$data["ChangeList"]) {
            $data["ChangeList"] = array();
        }
        foreach ($data["ChangeList"] as &$change) {
            $change["department_name"] = CodeConverter::GetDepatmentName($change["department_cd"]);
        }

        /**
         * 外泊、外出
         */
        $data["GooutList"] = $this->importListGooutInformation();
        if (!$data["GooutList"]) {
            $data["GooutList"] = array();
        }
        foreach ($data["GooutList"] as &$change) {
            $change["department_name"] = CodeConverter::GetDepatmentName($change["department_cd"]);
        }

        return $data;
    }

    /**
     * 現在入院患者一覧
     */
    private function importListCurrent() {

        $sfcGroupCd = $this->_BasicData[0]["sfc_group_cd"];
        $reportDateStr = $this->_BasicData[0]["report_date_str"];

        return $this->_Model["DutyShiftImportPatientWard"]->ImportListCurrent($sfcGroupCd, $reportDateStr);
    }

    /**
     * 診療科別一覧
     * @return type
     */
    private function importListDepartment() {

        $sfcGroupCd = $this->_BasicData[0]["sfc_group_cd"];
        $reportDateStr = $this->_BasicData[0]["report_date_str"];

        return $this->_Model["DutyShiftImportPatientWard"]->ImportListDepartment($sfcGroupCd, $reportDateStr);
    }

    /**
     * 救護区分別一覧
     */
    private function importListAidClassification() {

        $sfcGroupCd = $this->_BasicData[0]["sfc_group_cd"];
        $reportDateStr = $this->_BasicData[0]["report_date_str"];

        return $this->_Model["DutyShiftImportNursingDegree"]->ImportListAidClassification($sfcGroupCd, $reportDateStr);
    }

    /**
     * 移動患者一覧
     * 入院、転入、退院、転出
     * @return type
     */
    private function importListChangeInformation() {

        $sfcGroupCd = $this->_BasicData[0]["sfc_group_cd"];
        $reportDateStr = $this->_BasicData[0]["report_date_str"];

        return $this->_Model["DutyShiftImportPatientWard"]->ImportListChangeInformation($sfcGroupCd, $reportDateStr);
    }

    /**
     * 移動患者一覧
     * 外泊、外出
     */
    private function importListGooutInformation() {

        $sfcGroupCd = $this->_BasicData[0]["sfc_group_cd"];
        $reportDateStr = $this->_BasicData[0]["report_date_str"];

        return $this->_Model["DutyShiftImportPatientWard"]->ImportListGooutInformation($sfcGroupCd, $reportDateStr);
    }

    /**
     * 基本情報の初期化
     */
    private function initializeBasic() {

        $data = null;
        foreach ($this->_BasicItems as $item) {
            $data[0][$item] = "";
        }

        /**
         * インポート対象外は元のデータを入れる
         */
        $data[0]["shift_group"] = $this->_BasicData[0]["shift_group"];
        $data[0]["report_date"] = $this->_BasicData[0]["report_date"];
        $data[0]["proc"] = $this->_BasicData[0]["proc"];

        return $data;
    }

    /**
     * 前日から取得するデータ
     * 定数、患者状況、物品点検の物品名
     * @return type
     */
    private function getBeforeBasic() {

        $shiftGroup = $this->_BasicData[0]["shift_group"];
        $beforeDate = $this->_BasicData[0]["before_date"];

        $cond = Array(
            "conditions" => Array(
                "shift_group" => $shiftGroup,
                "report_date" => $beforeDate,
            ),
            "fields" => Array(
                "constant" => "constant",
                "current_num" => "current_num",
                "patient_status_name1" => "patient_status_name1",
                "patient_status_num1" => "patient_status_num1",
                "patient_status_name2" => "patient_status_name2",
                "patient_status_num2" => "patient_status_num2",
                "patient_status_name3" => "patient_status_name3",
                "patient_status_num3" => "patient_status_num3",
                "patient_status_name4" => "patient_status_name4",
                "patient_status_num4" => "patient_status_num4",
                "inspection_name1" => "inspection_name1",
                "inspection_name2" => "inspection_name2",
                "inspection_name3" => "inspection_name3",
                "inspection_name4" => "inspection_name4",
                "inspection_name5" => "inspection_name5",
                "inspection_name6" => "inspection_name6",
                "inspection_name7" => "inspection_name7",
                "inspection_name8" => "inspection_name8",
                "palliativecare_name" => "palliativecare_name",
                "palliativecare_num" => "palliativecare_num",
                "attendant_num" => "attendant_num",
            )
        );

        return $this->_Model["DutyShiftReportBasic"]->Find($cond);
    }

    /**
     * 現在数
     * @return type
     */
    private function getCurrentCount() {

        $sfcGroupCd = $this->_BasicData[0]["sfc_group_cd"];
        $reportDateStr = $this->_BasicData[0]["report_date_str"];

        return $this->_Model["DutyShiftImportPatientWard"]->getCurrentCount($sfcGroupCd, $reportDateStr);
    }

    /**
     * 入院、転入、退院、転出
     */
    private function getMoveCount() {

        $sfcGroupCd = $this->_BasicData[0]["sfc_group_cd"];
        $reportDateStr = $this->_BasicData[0]["report_date_str"];

        return $this->_Model["DutyShiftImportPatientWard"]->
                        GetMoveCount($sfcGroupCd, $reportDateStr);
    }

    /**
     * 外泊、外出
     */
    private function getGooutCount() {

        return $this->_Model["DutyShiftImportGooutInformation"]->
                        getGooutCount($this->_BasicData[0]["report_date_str"], $this->_BasicData[0]["sfc_group_cd"]);
    }

    /**
     * 診療科別　名称、人数
     */
    private function getDepartmentCount() {

        return $this->_Model["DutyShiftImportPatientWard"]->
                        getDepartmentCount($this->_BasicData[0]["report_date_str"], $this->_BasicData[0]["sfc_group_cd"]);
    }

    /**
     * 包括ケア数の取得
     */
    private function getCareCount() {

        $sfcGroupCd = $this->_BasicData[0]["sfc_group_cd"];
        $reportDateStr = $this->_BasicData[0]["report_date_str"];

        return $this->_Model["DutyShiftImportPatientWard"]->getCareCount($sfcGroupCd, $reportDateStr);
    }

//    /**
//     * 担送、護送、独歩
//     */
//    private function getAidClassificationCount() {
//
//        return $this->_Model["DutyShiftImportNursingDegree"]->
//                        getAidClassificationCount($this->_BasicData[0]["report_date_str"], $this->_BasicData[0]["sfc_group_cd"]);
//    }

    /**
     * 看護度
     */
    private function getNursingDegreeCount() {

        return $this->_Model["DutyShiftImportNursingDegree"]->
                        getNursingDegreeCount($this->_BasicData[0]["report_date_str"]);
    }

}
