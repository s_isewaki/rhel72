<?php

include_once(APP_BASE . "controller/AppController.php");
include_once(APP_BASE . "controller/DutyShiftReportInputItems.php");

/**
 * INPUT LOADER
 */
class DutyShiftReportInputLoader extends AppController {

    /**
     * 画面項目名
     * @var type 
     */
    private $_BasicItems;
    private $_ChangeInformationItems;
    private $_ChangeInformationSearchItems;
    private $_SeriousIllnessItems;
    private $_SeriousInformationItems;
    private $_SeriousIllnessSearchItems;
    private $_SeriousIllnessKeyItems;
    private $_SeriousIllnessResultItems;
    private $_WorkersInformationItems;
    private $_NightHospitalizationItems;
    private $_NightHospitalizationSearchItems;
    private $_NightInformationItems;
    private $_NightPatroltimeItems;
    private $_EmplNameData = "";

    /**
     * コンストラクタ
     */
    public function __construct() {

        parent::__construct();

        $this->Uses(
                array(
                    "DutyShiftGroup",
                )
        );

        $this->setInputItems();
    }

    public function SetEmplData($value) {

        $this->_EmplNameData = $value;
    }

    /**
     * 入力項目名設定
     */
    private function setInputItems() {

        $inputItems = new DutyShiftReportInputItems();

        $this->_BasicItems = $inputItems->GetBasicItems();
        $this->_ChangeInformationItems = $inputItems->GetChangeInformationItems();
        $this->_ChangeInformationSearchItems = $inputItems->GetChangeInformationSearchItems();
        $this->_SeriousIllnessItems = $inputItems->GetSeriousIllnessItems();
        $this->_SeriousInformationItems = $inputItems->GetSeriousInformationItems();
        $this->_SeriousIllnessSearchItems = $inputItems->GetSeriousIllnessSearchItems();
        $this->_SeriousIllnessKeyItems = $inputItems->GetSeriousIllnessKeyItems();
        $this->_SeriousIllnessResultItems = $inputItems->GetSeriousIllnessResultItems();
        $this->_WorkersInformationItems = $inputItems->GetWorkersInformationItems();
        $this->_NightHospitalizationItems = $inputItems->GetNightHospitalizationItems();
        $this->_NightHospitalizationSearchItems = $inputItems->GetNightHospitalizationSearchItems();
        $this->_NightInformationItems = $inputItems->GetNightInformationItems();
        $this->_NightPatroltimeItems = $inputItems->GetNightPatroltimeItems();
    }

    /**
     * 入力項目を連想配列に変換
     * @param type $postItems
     * @return type
     */
    public function InputItemBasic2DataArr($postItems) {

        $data = null;

        /**
         * 画面とDBで一致しない項目
         */
        $data[0]["proc"] = $postItems["proc"];
        $data[0]["no"] = $postItems["no"];
        $data[0]["start_yr"] = $postItems["start_yr"];
        $data[0]["start_mon"] = $postItems["start_mon"];
        $data[0]["start_day"] = $postItems["start_day"];
        $data[0]["entry_name"] = $this->_EmplNameData;

        /**
         * 一致する項目
         */
        foreach ($this->_BasicItems as $item) {
            if (isset($postItems[$item])) {
                $data[0][$item] = $postItems[$item];
            }
        }

        /**
         * procをactionとcategoryに分割
         */
        $arr = explode("_", $data[0]["proc"]);
        $data[0]["action"] = $arr[0];
        $data[0]["category"] = $arr[1];

        /**
         * 日付を登録用に連結する
         */
        $data[0]["report_date"] = $data[0]["start_yr"] . "/" .
                $data[0]["start_mon"] . "/" .
                $data[0]["start_day"];

        /**
         * 日付を各種参照用に連続文字列形式に連結する
         */
        $data[0]["report_date_str"] = $data[0]["start_yr"] .
                $data[0]["start_mon"] .
                $data[0]["start_day"];

        /**
         * 病棟をコードから名称に返還
         */
        $data[0]["group_name"] = $this->setShiftGroupName($data);

        return $data;
    }

    /**
     * 
     */
    private function setShiftGroupName($data) {

        $cond = Array(
            "conditions" => Array(
                "group_id" => $data[0]["shift_group"],
            ),
            "fields" => Array(
                "group_name" => "group_name",
            )
        );

        $this->_BasicData[0]["group_name"] = "";
        $data = $this->_Model["DutyShiftGroup"]->Find($cond);
        if (isset($data[0]["group_name"])) {
            return $data[0]["group_name"];
        }

        return "";
    }

    /**
     * 入院・転入・退院・転出セクションの入力項目を連想配列に変換
     * @param type $postItems
     * @return type
     */
    public function InputItemChangeInformation2DataArr($postItems) {

        $data = null;
        $postData = $postItems["change_information"];

        foreach ($this->_ChangeInformationItems as $item) {
            for ($i = 0; $i < count($postData[$item]); $i++) {
                $data[$i][$item] = $postData[$item][$i];
            }
        }

        return $data;
    }

    /**
     * 入院・転入・退院・転出セクションの入力項目を連想配列に変換
     * @param type $postItems
     * @return type
     */
    public function InputItemChangeInformationSearch2DataArr($postItems) {

        $data = array();
        if (!isset($postItems["change_information_search"])) {
            return $data;
        }
        $postData = $postItems["change_information_search"];

        foreach ($this->_ChangeInformationSearchItems as $item) {
            for ($i = 0; $i < LINE_CHANGE_INFORMATION; $i++) {
                if ($item == "check") {
                    if (isset($postData[$item][$item . $i])) {
                        $data[$i][$item] = $postData[$item][$item . $i];
                    } else {
                        $data[$i][$item] = "0";
                    }
                } else {
                    $data[$i][$item] = $postData[$item][$i];
                }
            }
        }

        return $data;
    }

    /**
     * 重症・要観察・手術セクションの入力項目を連想配列に変換
     * @param type $postItems
     */
    public function InputItemSeriousIllness2DataArr($postItems) {

        $data = null;
        $postData = $postItems["serious_illness"];

        foreach ($this->_SeriousIllnessItems as $item) {
            for ($i = 0; $i < count($postData[$item]); $i++) {
                $data[$i][$item] = $postData[$item][$i];
            }
        }

        return $data;
    }

    /**
     * 夜間の特記・管理事項の入力項目を連想配列に変換
     * @param type $postItems
     * @return type
     */
    public function InputItemSeriousInformation2DataArr($postItems) {

        $data = null;
        $postData = $postItems["serious_information"];

        foreach ($this->_SeriousInformationItems as $item) {
            for ($i = 0; $i < count($postData[$item]); $i++) {
                $data[$i][$item] = $postData[$item][$i];
            }
        }

        return $data;
    }

    /**
     * 重症・要観察・手術セクションの入力項目を連想配列に変換
     * @param type $postItems
     */
    public function InputItemSeriousIllnessSearch2DataArr($postItems) {

        $data = array();
        if (!isset($postItems["serious_illness_search"])) {
            return $data;
        }
        $postData = $postItems["serious_illness_search"];

        foreach ($this->_SeriousIllnessSearchItems as $item) {
            for ($i = 0; $i < LINE_SERIOUS_ILLNESS; $i++) {
                if ($item == "check") {
                    if (isset($postData[$item][$item . $i])) {
                        $data[$i][$item] = $postData[$item][$item . $i];
                    } else {
                        $data[$i][$item] = "0";
                    }
                } else {
                    $data[$i][$item] = $postData[$item][$i];
                }
            }
        }

        return $data;
    }

    /**
     * 重症・要観察・手術セクションの入力項目を連想配列に変換
     * @param type $postItems
     */
    public function InputItemSeriousIllnessKey2DataArr($postItems) {

        $data = array();
        if (!isset($postItems["serious_illness_key"])) {
            return $data;
        }
        $postData = $postItems["serious_illness_key"];

        foreach ($this->_SeriousIllnessKeyItems as $item) {
            $data[$item] = $postData[$item];
        }

        return $data;
    }

    /**
     * 重症・要観察・手術セクションの入力項目を連想配列に変換
     * @param type $postItems
     */
    public function InputItemSeriousIllnessResult2DataArr($postItems) {

        $data = array();
        if (!isset($postItems["serious_illness_result"])) {
            return $data;
        }
        $postData = $postItems["serious_illness_result"];

        foreach ($this->_SeriousIllnessResultItems as $item) {
            if (isset($postData[$item])) {
                for ($i = 0; $i < count($postData[$item]); $i++) {
                    $data[$i][$item] = $postData[$item][$i];
                }
            }
        }

        return $data;
    }

    /**
     * 勤務者セクションの入力項目を連想配列に変換
     * @param type $postItems
     */
    public function InputItemWorkersInformation2DataArr($postItems) {

        $data = null;
        $postData = $postItems["workers_information"];

        foreach ($this->_WorkersInformationItems as $item) {
            for ($i = 0; $i < count($postData[$item]); $i++) {
                $data[$i][$item] = $postData[$item][$i];
            }
        }

        return $data;
    }

    /**
     * 夜間入退院セクションの入力項目を連想配列に変換
     * @param type $postItems
     */
    public function InputItemNightHospitalization2DataArr($postItems) {

        $data = null;
        $postData = $postItems["night_hospitalization"];

        foreach ($this->_NightHospitalizationItems as $item) {
            for ($i = 0; $i < count($postData[$item]); $i++) {
                $data[$i][$item] = $postData[$item][$i];
            }
        }

        return $data;
    }

    /**
     * 夜間入退院セクションの入力項目を連想配列に変換
     * @param type $postItems
     */
    public function InputItemNightHospitalizationSearch2DataArr($postItems) {

        $data = array();
        if (!isset($postItems["night_hospitalization_search"])) {
            return $data;
        }
        $postData = $postItems["night_hospitalization_search"];

        foreach ($this->_NightHospitalizationSearchItems as $item) {
            for ($i = 0; $i < LINE_NIGHT_HOSPITALIZATION; $i++) {
                if ($item == "check") {
                    if (isset($postData[$item][$item . $i])) {
                        $data[$i][$item] = $postData[$item][$item . $i];
                    } else {
                        $data[$i][$item] = "0";
                    }
                } else {
                    $data[$i][$item] = $postData[$item][$i];
                }
            }
        }

        return $data;
    }

    /**
     * 夜間の特記・管理事項の入力項目を連想配列に変換
     * @param type $postItems
     * @return type
     */
    public function InputItemNightInformation2DataArr($postItems) {

        $data = null;
        $postData = $postItems["night_information"];

        foreach ($this->_NightInformationItems as $item) {
            for ($i = 0; $i < count($postData[$item]); $i++) {
                $data[$i][$item] = $postData[$item][$i];
            }
        }

        return $data;
    }

    /**
     * 巡視時間の入力項目を連想配列に変換
     * @param type $postItems
     * @return type
     */
    public function InputItemNightPatroltime2DataArr($postItems) {

        $data = null;
        $postData = $postItems["night_patroltime"];
        foreach ($this->_NightPatroltimeItems as $item) {
            for ($i = 0; $i < count($postData[$item]); $i++) {
                $data[$i][$item] = $postData[$item][$i];
            }
        }

        return $data;
    }

}
