<?php

include_once(APP_BASE . "controller/AppController.php");

/**
 * インポートクラス
 */
class Import extends AppController {

    private $_FileNames;
    private $_ImportPath = IMPORT_PATH;
    private $_SavePath = SAVE_PATH;
    private $_TargetExt = TARGET_EXT;

    /**
     * メイン
     * @param type $argv
     */
    public function Main($argv) {

        $this->initialize($argv);

        $this->_Logger->PutInfo("処理を開始しました。");

        if (!$this->getFileList()) {
            $this->_Logger->PutErr("CSV取得で処理が異常終了しました。");
        }

        if (!$this->csv2Db()) {
            $this->_Logger->PutErr("DB登録で処理が異常終了しました。");
        }

        $this->finalize();

        $this->_Logger->PutInfo("処理を終了しました。");
    }

    /**
     * 初期処理
     * @param type $argv
     */
    private function initialize($argv) {

        $this->_FileNames = null;

        $this->_ProgramName = $argv[0];
        $this->Uses(
                array(
                    "DutyShiftImportHeader",
                    "DutyShiftImportTypea",
                    "DutyShiftImportTypeb",
                    "DutyShiftImportTypec",
                    "DutyShiftImportTypef"
                )
        );
    }

    /**
     * 終了時処理
     */
    private function finalize() {

        $this->Close();
    }

    /**
     * CSVファイル名一覧の取得（ファイル名の昇順）
     * @return boolean
     */
    private function getFileList() {

        $dirH = opendir($this->_ImportPath);
        while (false !== ($fileNames[] = readdir($dirH) )) {
            
        }
        closedir($dirH);
        sort($fileNames);
        foreach ($fileNames as $fileName) {
            if ($this->isTargetFile($fileName)) {
                $this->_FileNames[] = $fileName;
            }
        }
        return true;
    }

    /**
     * 対象ファイルかの判定
     * @param type $fileName
     * @return boolean
     */
    private function isTargetFile($fileName) {

        if (is_file($this->_ImportPath . $fileName)) {
            if (end(explode('.', $fileName)) == $this->_TargetExt) {
                return true;
            }
        }
        return false;
    }

    /**
     * CSVからDBへ
     * @return boolean
     */
    private function csv2Db() {

        if ($this->_FileNames == null) {
            return true;
        }
        foreach ($this->_FileNames as $fileName) {
            $this->BeginTransaction();
            if (!$this->registDb($fileName)) {
                $this->Rollback();
                continue;
            }
            $this->Commit();
            $this->moveCsvFile($fileName);
        }
        return true;
    }

    /**
     * DBへ登録
     * @param type $fileFullName
     * @return boolean
     */
    private function registDb($fileFullName) {

        $fileName = explode(".", $fileFullName);

        if ($this->isExsist($fileName[0])) {
            $this->_Logger->PutErr("既に登録されているデータなので除外しました。:" .
                    print_r($fileName[0], true));
            return true;
        }

        $fileDataSJis = file_get_contents($this->_ImportPath . $fileFullName);
        //$fileData = mb_convert_encoding($fileDataSJis, "EUC-JP", "SJIS");
        //$fileData = mb_convert_encoding($fileDataSJis, "EUC-JP", "SJIS-WIN");
        $fileData = mb_convert_encoding($fileDataSJis, "cp51932", "cp932");
        $lines = explode("\r\n", $fileData);
        $cols = explode(",", $lines[0]);

        if (count($cols) < 5) {
            $this->_Logger->PutErr("データの書式が正しくありません。");
            return false;
        }

        $headData = array_slice($cols, 0, 5);
        $bodyData = array_slice($cols, 5);
        if (count($bodyData) <= 0) {
            $this->_Logger->PutErr("データに明細部がありません。");
            return false;
        }
        $type = $this->getDataType($headData);

        if (!$this->registHeader($fileName[0], $headData)) {
            $this->_Logger->PutErr("HEADERの取り込みが異常終了しました。:" .
                    print_r($headData, true));
            return false;
        }

        if (!$this->registBody($fileName[0], $type, $headData, $bodyData)) {
            $this->_Logger->PutErr("明細の取り込みが異常終了しました。");
            return false;
        }

        return true;
    }

    /**
     * HEADER存在チェック
     * @param type $fileName
     * @return boolean
     */
    private function isExsist($fileName) {

        $cond = Array(
            "fields" => Array(
                "file_name" => "file_name"
            ),
            "conditions" => Array(
                "file_name" => $fileName,
            )
        );
        $resultHeader = $this->_Model["DutyShiftImportHeader"]->find($cond);
        if (!$resultHeader) {
            return false;
        }

        if (count($resultHeader) >= 1) {
            return true;
        }
        return false;
    }

    /**
     * HEADERの登録
     * @param type $fileName
     * @param type $lineHead
     * @return boolean
     */
    private function registHeader($fileName, $lineHead) {

        $registData = array(
            "file_name" => $fileName,
            "send_date" => trim($lineHead[0]),
            "send_time" => trim($lineHead[1]),
            "data_type" => trim($lineHead[2]),
            "proc_class" => trim($lineHead[3]),
            "patient_cd" => trim($lineHead[4]),
            "status" => "0",
        );

        if (!$this->_Model["DutyShiftImportHeader"]->SaveIns($registData)) {
            return false;
        }
        return true;
    }

    /**
     * 明細の登録
     * @param type $fileName
     * @param type $type
     * @param type $headData
     * @param type $bodyData
     * @return boolean
     */
    private function registBody($fileName, $type, $headData, $bodyData) {

        switch ($type) {
            case "A":
                $this->_Logger->PutInfo("種別Aを取り込みます");
                if (!$this->registTypea($fileName, $headData, $bodyData)) {
                    $this->_Logger->PutErr("種別Aの取り込みが異常終了しました。");
                    return false;
                }
                break;
            case "B":
                $this->_Logger->PutInfo("種別Bを取り込みます");
                if (!$this->registTypeb($fileName, $headData, $bodyData)) {
                    $this->_Logger->PutErr("種別Bの取り込みが異常終了しました。");
                    return false;
                }
                break;
            case "C":
                $this->_Logger->PutInfo("種別Cを取り込みます");
                if (!$this->registTypec($fileName, $headData, $bodyData)) {
                    $this->_Logger->PutErr("種別Cの取り込みが異常終了しました。");
                    return false;
                }
                break;
            case "F":
                $this->_Logger->PutInfo("種別Fを取り込みます");
                if (!$this->registTypef($fileName, $headData, $bodyData)) {
                    $this->_Logger->PutErr("種別Fの取り込みが異常終了しました。");
                    return false;
                }
                break;
            default:
                $this->_Logger->PutInfo("種別[" . $type .
                        "]は取り込み対象外なので除外します。");
                break;
        }

        return true;
    }

    /**
     * TYPE Aの登録
     * @param type $fileName
     * @param type $lineHead
     * @param type $lineBody
     * @return boolean
     */
    private function registTypeA($fileName, $lineHead, $lineBody) {

        $registData = array(
            "file_name" => $fileName,
            "send_date" => trim($lineHead[0]),
            "send_time" => trim($lineHead[1]),
            "data_class" => trim($lineHead[2]),
            "proc_class" => trim($lineHead[3]),
            "patient_cd" => trim($lineHead[4]),
            "name_kana" => trim($lineBody[0]),
            "name_kanji" => trim($lineBody[1]),
            "sex" => trim($lineBody[2]),
            "birthday" => trim($lineBody[3]),
            "contact" => trim($lineBody[4]),
            "status" => "0",
        );
        if (!$this->_Model["DutyShiftImportTypea"]->SaveIns($registData)) {
            return false;
        }
        return true;
    }

    /**
     * TYPE Bの登録
     * @param type $fileName
     * @param type $lineHead
     * @param type $lineBody
     * @return boolean
     */
    private function registTypeB($fileName, $lineHead, $lineBody) {

        $registData = array(
            "file_name" => $fileName,
            "send_date" => trim($lineHead[0]),
            "send_time" => trim($lineHead[1]),
            "data_class" => trim($lineHead[2]),
            "proc_class" => trim($lineHead[3]),
            "patient_cd" => trim($lineHead[4]),
            "data_type" => trim($lineBody[0]),
            "move_date" => trim($lineBody[1]),
            "move_time" => trim($lineBody[2]),
            "ward_cd" => trim($lineBody[3]),
            "department_cd" => trim($lineBody[4]),
            "sickroom_cd" => trim($lineBody[5]),
            "sickbed_cd" => trim($lineBody[6]),
            "admission_form" => trim($lineBody[7]),
            "attending_physician" => trim($lineBody[8]),
            "disease_name" => trim($lineBody[9]),
            "articles" => trim($lineBody[10]),
            "outcome_cd" => trim($lineBody[11]),
            "status" => "0",
        );
        if (!$this->_Model["DutyShiftImportTypeb"]->SaveIns($registData)) {
            return false;
        }
        return true;
    }

    /**
     * TYPE Cの登録
     * @param type $fileName
     * @param type $lineHead
     * @param type $lineBody
     * @return boolean
     */
    private function registTypeC($fileName, $lineHead, $lineBody) {

        $registData = array(
            "file_name" => $fileName,
            "send_date" => trim($lineHead[0]),
            "send_time" => trim($lineHead[1]),
            "data_class" => trim($lineHead[2]),
            "proc_class" => trim($lineHead[3]),
            "patient_cd" => trim($lineHead[4]),
            "data_type" => trim($lineBody[0]),
            "goout_date" => trim($lineBody[1]),
            "goout_time" => trim($lineBody[2]),
            "return_date" => trim($lineBody[3]),
            "return_time" => trim($lineBody[4]),
            "status" => "0",
        );
        if (!$this->_Model["DutyShiftImportTypec"]->SaveIns($registData)) {
            return false;
        }
        return true;
    }

    /**
     * TYPE Fの登録
     * @param type $fileName
     * @param type $lineHead
     * @param type $lineBody
     * @return boolean
     */
    private function registTypeF($fileName, $lineHead, $lineBody) {

        $registData = array(
            "file_name" => $fileName,
            "send_date" => trim($lineHead[0]),
            "send_time" => trim($lineHead[1]),
            "data_class" => trim($lineHead[2]),
            "proc_class" => trim($lineHead[3]),
            "patient_cd" => trim($lineHead[4]),
            "data_type" => trim($lineBody[0]),
            "start_date" => trim($lineBody[1]),
            "discrimination" => trim($lineBody[2]),
            "status" => "0",
        );
        if (!$this->_Model["DutyShiftImportTypef"]->SaveIns($registData)) {
            return false;
        }
        return true;
    }

    /**
     * ファイルタイプの判定
     * @param type $headData
     * @return type
     */
    private function getDataType($headData) {

        return $headData[2];
    }

    /**
     * 終了したファイルを移動
     * @param type $fileName
     */
    private function moveCsvFile($fileName) {

        rename($this->_ImportPath . $fileName, $this->_SavePath . $fileName);
    }

}
