<?php

include_once(APP_BASE . "controller/AppController.php");
include_once(APP_BASE . "util/DateConverter.php");

/**
 * 検索
 */
class DutyShiftReportSearcher extends AppController {

    private $_BasicData;    //基本情報データ
    private $_SeriousIllnessKeyData;

    /**
     * コンストラクタ
     */
    public function __construct() {

        parent::__construct();

        $this->Uses(
                array(
                    "DutyShiftImportPatientWard",
                    "DutyShiftGroup",
                )
        );
    }

    /**
     * 基本情報の設定
     * @param type $value
     */
    public function SetBasicData($value) {

        $this->_BasicData = $value;
        $this->setGroupSfcId();
    }

    /**
     * 病棟コードを連携用病棟コードに変換する
     */
    private function setGroupSfcId() {

        $cond = Array(
            "conditions" => Array(
                "group_id" => $this->_BasicData[0]["shift_group"],
            ),
            "fields" => Array(
                "sfc_group_code" => "sfc_group_code",
            )
        );

        $this->_BasicData[0]["sfc_group_cd"] = "";
        $data = $this->_Model["DutyShiftGroup"]->Find($cond);

        if (isset($data[0]["sfc_group_code"])) {
            $this->_BasicData[0]["sfc_group_cd"] = $data[0]["sfc_group_code"];
        }
    }

    /**
     * 検索データ
     * @param type $value
     */
    public function SetSeriousIllnessKeyData($value) {

        $this->_SeriousIllnessKeyData = $value;
    }

    /**
     * 検索
     */
    public function SearchPatient($keyData) {

        $datas = $this->_Model["DutyShiftImportPatientWard"]->SearchList(
                $this->_BasicData[0]["sfc_group_cd"], $keyData["patient_cd"], $keyData["patient_name"], $this->_BasicData[0]["report_date_str"]);
        if (!$datas) {
            return $datas;
        }

        foreach ($datas as &$data) {
            switch ($data["sex"]) {
                case "M":
                    $data["sex_name"] = "男";
                    break;
                case "F":
                    $data["sex_name"] = "女";
                    break;
                default:
                    $data["sex_name"] = "";
                    break;
            }
            $data["age"] = DateConverter::CalcAge($data["birthday"]);
        }

        return $datas;
    }

}
