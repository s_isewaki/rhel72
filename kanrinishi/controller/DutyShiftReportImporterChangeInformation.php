<?php

include_once(APP_BASE . "controller/AppController.php");
include_once(APP_BASE . "util/DateConverter.php");

/**
 * データ取込クラス
 * 移動情報
 */
class DutyShiftReportImporterChangeInformation extends AppController {

    private $_BasicData;                //基本情報データ
    private $_ChangeInformation;        //入退院データ
    private $_ChangeInformationData;    //入退院データ
    private $_ChangeInformationItems;   //入退院項目
    private $_ErrorMessage = "";
    private $_ChangeInformationCompItems;

    /**
     * コンストラクタ
     */
    public function __construct() {

        parent::__construct();

        $this->Uses(
                array(
                    "DutyShiftImportPatientWard",
                )
        );
        $inputItems = new DutyShiftReportInputItems();
        $this->_ChangeInformationItems = $inputItems->GetChangeInformationItems();
        $this->_ChangeInformationCompItems = $inputItems->GetChangeInformationCompItems();
    }

    /**
     * エラーメッセージの取得
     * @return type
     */
    public function GetErrorMessage() {

        return $this->_ErrorMessage;
    }

    /**
     * 基本情報の設定
     * @param type $value
     */
    public function SetBasicData($value) {

        $this->_BasicData = $value;
    }

    /**
     * 移動情報の設定
     * @param type $value
     */
    public function SetChangeInformationData($value) {

        $this->_ChangeInformationData = $value;
    }

    /**
     * 
     */
    public function ImportAll() {

        $data = array();
        for ($j = 0; $j < LINE_CHANGE_INFORMATION; $j++) {
            foreach ($this->_ChangeInformationItems as $item) {
                $data[$j][$item] = "";
            }
        }

        $j = 0;
        foreach ($this->_ChangeInformationData as $nhData) {
            $notEmpty = false;
            foreach ($this->_ChangeInformationItems as $item) {
                if ($nhData[$item] != "") {
                    $notEmpty = true;
                }
            }
            if (!$notEmpty) {
                break;
            }
            foreach ($this->_ChangeInformationItems as $item) {
                if (isset($nhData[$item])) {
                    $data[$j][$item] = $nhData[$item];
                }
            }
            $j++;
        }

        $importData = $this->Import();

        for ($i = 0; $i < count($importData); $i++) {
            foreach ($this->_ChangeInformationItems as $item) {
                $data[$j][$item] = $importData[$i][$item];
            }
            $j++;
        }

        if ($j > LINE_CHANGE_INFORMATION) {
            $this->_ErrorMessage .= "移動情報が、" . LINE_CHANGE_INFORMATION .
                    "人を超えたため、取り込めない情報が発生しました。";
        }

        return $data;
    }

    /**
     * 画面に入力されているデータとプレビュー画面で選択したデータを合わせて返す。
     * @param type $searchData
     */
    public function ImportPreviewReg($searchData) {

        /**
         * 初期化
         */
        $data = array();
        for ($j = 0; $j < LINE_CHANGE_INFORMATION; $j++) {
            foreach ($this->_ChangeInformationItems as $item)
                $data[$j][$item] = "";
        }

        /**
         * 既存の内容を取込む
         */
        $j = 0;
        foreach ($this->_ChangeInformationData as $nhData) {
//            $notEmpty = false;
//            foreach ($this->_ChangeInformationItems as $item) {
//                if ($nhData[$item] != "") {
//                    $notEmpty = true;
//                }
//            }
//            if (!$notEmpty) {
//                break;
//            }
            foreach ($this->_ChangeInformationItems as $item) {
                if (isset($nhData[$item])) {
                    $data[$j][$item] = $nhData[$item];
                }
            }
            $j++;
        }

        /**
         * 既存の内容の空白行に入れ込む
         */
        for ($i = 0; $i < count($searchData); $i++) {
            if ($searchData[$i]["check"] == "1") {

                //対象とするものが空行であれば飛ばす
                $isEmpty = true;
                foreach ($this->_ChangeInformationItems as $item) {
                    if ($searchData[$i][$item] != "") {
                        $isEmpty = false;
                    }
                }
                if ($isEmpty) {
                    continue;
                }

                //対象とするものと同じデータがあれば飛ばす
                foreach ($this->_ChangeInformationData as $nhData) {
                    $isSame = true;
//                    echo "\n\n";
                    foreach ($this->_ChangeInformationCompItems as $item) {
//echo $item . ":" . $searchData[$i][$item] . ":" . $nhData[$item] . "\n";
                        if ($searchData[$i][$item] != $nhData[$item]) {
                            $isSame = false;
                        }
                    }
                    if ($isSame) {
//                        echo "同じ\n";
                        continue 2;
                    }
                }

                //空白行を探す
                $j = 0;
                foreach ($data as $nhData) {
                    $isEmpty = true;
                    foreach ($this->_ChangeInformationItems as $item) {
                        if ($nhData[$item] != "") {
                            $isEmpty = false;
                        }
                    }
                    if ($isEmpty) {
                        echo $j;
                        break;
                    }
                    $j++;
                }

                //空白行に挿入
                foreach ($this->_ChangeInformationItems as $item) {
                    $data[$j][$item] = $searchData[$i][$item];
                }
            }
        }

        if (count($data) > LINE_CHANGE_INFORMATION) {
            $this->_ErrorMessage .= "移動情報が、" . LINE_CHANGE_INFORMATION .
                    "人を超えたため、取り込めない情報が発生しました。";
        }

        return $data;
    }

    /**
     * 移動情報のインポート
     */
    public function Import() {

        $this->initializeData();
        $datas = $this->getData();
        if (!$datas) {
            return $this->_ChangeInformation;
        }

        $i = 0;
        foreach ($datas as $data) {
            $this->_ChangeInformation[$i]["data_class"] = $data["datatype_name"];
            $this->_ChangeInformation[$i]["department_name"] = $this->getDepatmentName($data["department_cd"]);
            $this->_ChangeInformation[$i]["hour"] = DateConverter::AddCol($data["move_time"]);
            $this->_ChangeInformation[$i]["patient_name"] = $data["name_kanji"];
            $this->_ChangeInformation[$i]["age"] = DateConverter::CalcAge($data["birthday"]);
            $this->_ChangeInformation[$i]["sex"] = $data["sex"];
            $this->_ChangeInformation[$i]["disease_name"] = $data["disease_name"];
            $this->_ChangeInformation[$i]["attending_physician"] = $data["attending_physician"];
            $this->_ChangeInformation[$i]["articles"] = "";
            $i++;
        }

        if (count($this->_ChangeInformation) > 23) {
            $this->_ErrorMessage = "移動情報が23件を超えたため、取り込めない情報が発生しました。";
        }

        return $this->_ChangeInformation;
    }

    /**
     * データの初期化
     */
    private function initializeData() {

        $this->_ChangeInformation = array();

        for ($j = 0; $j < LINE_CHANGE_INFORMATION; $j++) {
            foreach ($this->_ChangeInformationItems as $item)
                $this->_ChangeInformation[$j][$item] = "";
        }
    }

    /**
     * 診療科目名の取得
     * @param type $key
     */
    private function getDepatmentName($key) {

        $department = $GLOBALS["Department"];
        if (isset($department[$key])) {
            return $department[$key];
        }

        return false;
    }

    /**
     * 移動情報
     */
    private function getData() {

        $tagetDate = $this->_BasicData[0]["report_date_str"];
        $wardCd = $this->_BasicData[0]["sfc_group_cd"];
        return $this->_Model["DutyShiftImportPatientWard"]->ImportChangeInfomation($tagetDate, $wardCd);
    }

}
