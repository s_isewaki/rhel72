<?php

include_once(APP_BASE . "controller/AppController.php");
include_once(APP_BASE . "controller/DutyShiftReportInputLoader.php");
include_once(APP_BASE . "controller/DutyShiftReportDbLoader.php");
include_once(APP_BASE . "controller/DutyShiftReportDbSaver.php");
include_once(APP_BASE . "controller/DutyShiftReportImporter.php");
include_once(APP_BASE . "controller/DutyShiftReportSearcher.php");

/**
 * 病棟管理日誌クラス
 */
class duty_shift_diary_bld_manage extends AppController {

    /**
     * データ
     * @var type 
     */
    private $_DutyShiftGroup;
    private $_BasicData;
    private $_BasicSearchData;          //患者数インポート確認データ
    private $_BasicListData;
    private $_ChangeInformationData;
    private $_ChangeInformationSearchData;
    private $_SeriousIllnessData;
    private $_SeriousInformationData;
    private $_SeriousIllnessSearchData;
    private $_SeriousIllnessKeyData;
    private $_SeriousIllnessResultData;
    private $_WorkersInformationData;
    private $_NightHospitalizationData;
    private $_NightHospitalizationSearchData;
    private $_NightInformationData;
    private $_NightPatroltimeData;
    private $_HistoryData;
    private $_EmplNameData;
    private $_EmplCd;
    private $_ProgramName;

    /**
     * エラーメッセージ
     * @var type 
     */
    private $_ErrorMessage;

    /**
     * コンストラクタ
     * @param type $programName
     */
    public function __construct($programName) {

        if (DEBUG == "1") {
            ini_set("display_errors", "On");
            ini_set("error_reporting", E_ALL);
        }

        parent::__construct();

        $this->_ProgramName = $programName;
    }

    /**
     * 制御
     * @param type $groupArray
     * @param type $session
     * @param type $post
     */
    public function Control($groupArray, $session, $post, $emplName, $emplCd) {

        $this->_EmplNameData = $emplName;
        $this->_EmplCd = $emplCd;

        $dbLoader = new DutyShiftReportDbLoader();
        $this->_DutyShiftGroup = $dbLoader->LoadDutyShiftGroup($this->_EmplCd);

        if (count($post) > 0) {
            if (!$this->inputItem2DataArr($post)) {
                return false;
            }

            switch ($this->_BasicData[0]["action"]) {
                case "LOAD":
                    if (!$this->load()) {
                        $this->_Logger->PutErr("LOADでエラーが発生しました。");
                    }
                    break;
                case "SAVE":
                    if (!$this->save()) {
                        $this->_Logger->PutErr("SAVEでエラーが発生しました。");
                    }
                    break;
                case "IMPORT":
                    if (!$this->import()) {
                        $this->_Logger->PutErr("IMPORTでエラーが発生しました。");
                    }
                    break;
                case "SEARCH":
                    if (!$this->search()) {
                        $this->_Logger->PutErr("SEARCHでエラーが発生しました。");
                    }
                    break;
                case "PDF":
                    break;
                default:
                    break;
            }
        //} else {
        //    if (!$this->load()) {
        //        $this->_Logger->PutErr("LOADでエラーが発生しました。");
        //    }
        }

        $this->index($groupArray, $session);

        return true;
    }

    /**
     * 変換
     * @param type $post
     */
    private function inputItem2DataArr($post) {

        $inputLoader = new DutyShiftReportInputLoader();
        $inputLoader->SetEmplData($this->_EmplNameData);
        $this->_BasicData = $inputLoader->InputItemBasic2DataArr($post);
        $this->_ChangeInformationData = $inputLoader->InputItemChangeInformation2DataArr($post);
        $this->_ChangeInformationSearchData = $inputLoader->InputItemChangeInformationSearch2DataArr($post);
        $this->_SeriousIllnessData = $inputLoader->InputItemSeriousIllness2DataArr($post);
        $this->_SeriousInformationData = $inputLoader->InputItemSeriousInformation2DataArr($post);
        $this->_SeriousIllnessSearchData = $inputLoader->InputItemSeriousIllnessSearch2DataArr($post);
        $this->_SeriousIllnessKeyData = $inputLoader->InputItemSeriousIllnessKey2DataArr($post);
        //$this->_SeriousIllnessResultData = $inputLoader->InputItemSeriousIllnessResult2DataArr($post);
        $this->_WorkersInformationData = $inputLoader->InputItemWorkersInformation2DataArr($post);
        $this->_NightHospitalizationData = $inputLoader->InputItemNightHospitalization2DataArr($post);
        $this->_NightHospitalizationSearchData = $inputLoader->InputItemNightHospitalizationSearch2DataArr($post);
        $this->_NightInformationData = $inputLoader->InputItemNightInformation2DataArr($post);
        $this->_NightPatroltimeData = $inputLoader->InputItemNightPatroltime2DataArr($post);

        return true;
    }

    /**
     * 表示処理
     */
    private function load() {

        $dbLoader = new DutyShiftReportDbLoader();
        $dbLoader->SetBasicData($this->_BasicData);
        $this->_BasicData = $dbLoader->LoadBasic();
        $this->_ChangeInformationData = $dbLoader->LoadChangeInformation();
        $this->_SeriousIllnessData = $dbLoader->LoadSeriousIllness();
        $this->_SeriousInformationData = $dbLoader->LoadSeriousInformation();
        $this->_WorkersInformationData = $dbLoader->LoadWorkersInformation();
        $this->_NightHospitalizationData = $dbLoader->LoadNightHospitalization();
        $this->_NightInformationData = $dbLoader->LoadNightInformation();
        $this->_NightPatroltimeData = $dbLoader->LoadNightPatroltime();
        $this->_HistoryData = $dbLoader->LoadHistory();

        if ($this->_BasicData[0]["entry_name"] == "") {
            $this->_ErrorMessage = "データが登録されていません。";
        }

        return true;
    }

    /**
     * 保存処理
     * @return boolean
     */
    private function save() {

        $dbSaver = new DutyShiftReportDbSaver();
        $dbSaver->SetBasicData($this->_BasicData);
        $dbSaver->SetChargeInformationData($this->_ChangeInformationData);
        $dbSaver->SetSeriousIllnessData($this->_SeriousIllnessData);
        $dbSaver->SetSeriousInformationData($this->_SeriousInformationData);
        $dbSaver->SetWorkersInformationData($this->_WorkersInformationData);
        $dbSaver->SetNightHospitalizationData($this->_NightHospitalizationData);
        $dbSaver->SetNightInformationData($this->_NightInformationData);
        $dbSaver->SetNightPatroltimeData($this->_NightPatroltimeData);
        if (!$dbSaver->Save($this->_BasicData[0]["category"])) {
            $this->_ErrorMessage = $dbSaver->GetErrorMessage();
            return false;
        }

        return true;
    }

    /**
     * インポート
     */
    private function import() {

        $import = new DutyShiftReportImporter();
        $import->SetBasicData($this->_BasicData);
        $import->SetChangeInformation($this->_ChangeInformationData);
        $import->SetSeriousIllnessData($this->_SeriousIllnessData);
        $import->SetNightHospitalizationData($this->_NightHospitalizationData);

        switch ($this->_BasicData[0]["category"]) {
            case "A":
                $this->_BasicData = $import->ImportBasic(); //ALL
                $this->_ChangeInformationData = $import->ImportChangeInformationAll();
                $this->_SeriousIllnessData = $import->ImportSeriousIllnessAll();
                $this->_WorkersInformationData = $import->ImportWorkersInfo();
                $this->_NightHospitalizationData = $import->ImportNightHospitalizationAll();
                $this->_NightPatroltimeData = $import->ImportNightPatroltime();
                break;
            case "1-2":     //基本情報（確認画面用データ取得）
                $this->_BasicSearchData = $import->ImportBasic();
                $this->_BasicListData = $import->ImportBasicList();
                break;
            case "1-2-2":   //基本情報（確認画面のOK）
                $this->_BasicData = $import->ImportBasic();
                break;
            case "2":       //移動情報（取込画面用データ取得）
                $this->_ChangeInformationSearchData = $import->ImportChangeInformation();
                break;
            case "2-1":     //移動情報（取込画面のOK)
                $this->_ChangeInformationData = $import->ImportChangeInformationPreviewReg(
                        $this->_ChangeInformationSearchData);
                break;
            case "3":       //重症情報
                $this->_SeriousIllnessSearchData = $import->ImportSeriousIllness();
                break;
            case "3-1":     //重症情報
                $this->_SeriousIllnessData = $import->ImportSeriousIllnessPreviewReg(
                        $this->_SeriousIllnessSearchData);
                break;
            case "4":       //勤務者情報
                $this->_WorkersInformationData = $import->ImportWorkersInfo();
                break;
            case "5":       //夜間入退院（取込画面用データ取得）
                $this->_NightHospitalizationSearchData = $import->ImportNightHospitalization();
                break;
            case "5-1":     //夜間入退院（取込画面のOK）
                $this->_NightHospitalizationData = $import->ImportNightHospitalizationPreviewReg(
                        $this->_NightHospitalizationSearchData);
                $this->_NightPatroltimeData = $import->ImportNightPatroltime();
                break;
        }

        return true;
    }

    /**
     * 検索
     */
    private function search() {

        $search = new DutyShiftReportSearcher();
        $search->SetBasicData($this->_BasicData);

        switch ($this->_BasicData[0]["category"]) {
            case "3-1": //重症・要観察者・手術セクション
                $this->_SeriousIllnessResultData = $search->SearchPatient($this->_SeriousIllnessKeyData);
                break;
        }

        return true;
    }

    /**
     * 一覧
     * @param type $groupArray
     * @param type $session
     */
    private function index($groupArray, $session) {

        if (isset($this->_BasicData[0]["report_date"])) {
            $date = new DateTime($this->_BasicData[0]["report_date"]);
            $start_yr = $date->format('Y');
            $start_mon = $date->format('m');
            $start_day = $date->format('d');
        } else {
//            $start_yr = date('Y', strtotime('-1 day'));
//            $start_mon = date('m', strtotime('-1 day'));
//            $start_day = date('d', strtotime('-1 day'));
            // 11/27 前日から当日に変更
            $start_yr = date('Y');
            $start_mon = date('m');
            $start_day = date('d');
        }

        include_once(APP_BASE . "view/DutyShiftDiaryBldManage.php");
    }

}
