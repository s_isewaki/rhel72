<?php

include_once(APP_BASE . "util/Logger.php");

/**
 * コントローラベースクラス
 */
class AppController {

    protected $_Model;
    protected $_Logger;
    private $_Models;
    private $_ProgramName;
    private $_LogPath = LOG_PATH;

    /**
     * コンストラクタ
     */
    function __construct() {

        $this->_Logger = new Logger($this->_LogPath);
    }

    /**
     * モデルの使用 
     * @param type $models
     */
    protected function Uses($models) {

        $this->_Models = $models;

        foreach ($this->_Models as $model) {
            include_once(APP_BASE . "model/" . $model . ".php");
            $this->_Model[$model] = new $model($this->_ProgramName);
        }
    }

    /**
     * 閉じる
     */
    protected function Close() {

        foreach ($this->_Model as $model) {
            $model->Close();
        }
    }

    /**
     * トランザクション開始 
     */
    protected function BeginTransaction() {

        foreach ($this->_Model as $model) {
            $model->BeginTransaction();
        }
    }

    /**
     * コミット
     */
    protected function Commit() {

        foreach ($this->_Model as $model) {
            $model->Commit();
        }
    }

    /**
     * ロールバック
     */
    protected function Rollback() {

        foreach ($this->_Model as $model) {
            $model->Rollback();
        }
    }

}
