<?php

include_once(APP_BASE . "controller/AppController.php");
include_once(APP_BASE . "controller/DutyShiftReportInputItems.php");
include_once(APP_BASE . "controller/DutyShiftReportImporterBasic.php");
include_once(APP_BASE . "controller/DutyShiftReportImporterWorkersInfo.php");
include_once(APP_BASE . "controller/DutyShiftReportImpoterSeriousIllness.php");
include_once(APP_BASE . "controller/DutyShiftReportImporterChangeInformation.php");
include_once(APP_BASE . "controller/DutyShiftReportImporterNightHospitalization.php");
include_once(APP_BASE . "controller/DutyShiftReportImporterNightPatroltime.php");

/**
 * データ取込クラス
 */
class DutyShiftReportImporter extends AppController {

    /**
     * 画面データ
     * @var type 
     */
    private $_BasicData;    //基本情報データ
    private $_SeriousIllnessData;
    private $_BasicItems;   //基本情報項目
    private $_DutyShiftReportImporterBasic;
    private $_DutyShiftReportImporterWorkersInfo;
    private $_DutyShiftReportImpoterSeriousIllness;
    private $_DutyShiftReportImporterChangeInformation;
    private $_DutyShiftReportImporterNightHospitalization;
    private $_DutyShiftReportImporterNightPatroltime;
    private $_NightHospitalization;
    private $_ChangeInformation;        //入退院データ

    /**
     * コンストラクタ
     */

    public function __construct() {

        parent::__construct();

        $this->Uses(
                array(
                    "DutyShiftGroup",
                )
        );
        $this->setInputItems();
        $this->_DutyShiftReportImporterBasic = new DutyShiftReportImporterBasic();
        $this->_DutyShiftReportImporterWorkersInfo = new DutyShiftReportImporterWorkersInfo();
        $this->_DutyShiftReportImpoterSeriousIllness = new DutyShiftReportImpoterSeriousIllness();
        $this->_DutyShiftReportImporterChangeInformation = new DutyShiftReportImporterChangeInformation();
        $this->_DutyShiftReportImporterNightHospitalization = new DutyShiftReportImporterNightHospitalization();
        $this->_DutyShiftReportImporterNightPatroltime = new DutyShiftReportImporterNightPatroltime();
    }

    /**
     * 基本情報の設定
     * @param type $value
     */
    public function SetBasicData($value) {

        $this->_BasicData = $value;
        $this->_BasicData[0]["before_date"] = date("Y/m/d", strtotime($this->_BasicData[0]["report_date"] . " -1 day"));
        $this->setGroupSfcId();
    }

    /**
     * 移動情報
     * @param type $value
     */
    public function SetChangeInformation($value) {

        $this->_ChangeInformation = $value;
    }

    /**
     * 重症データ
     * @param type $value
     */
    public function SetSeriousIllnessData($value) {

        $this->_SeriousIllnessData = $value;
    }

    /**
     * 夜間入退院
     * @param type $value
     */
    public function SetNightHospitalizationData($value) {

        $this->_NightHospitalization = $value;
    }

    /**
     * 病棟コードを連携用病棟コードに変換する
     */
    private function setGroupSfcId() {

        $cond = Array(
            "conditions" => Array(
                "group_id" => $this->_BasicData[0]["shift_group"],
            ),
            "fields" => Array(
                "sfc_group_code" => "sfc_group_code",
            )
        );

        $this->_BasicData[0]["sfc_group_cd"] = "";
        $data = $this->_Model["DutyShiftGroup"]->Find($cond);

        if (isset($data[0]["sfc_group_code"])) {
            $this->_BasicData[0]["sfc_group_cd"] = $data[0]["sfc_group_code"];
        }
    }

    /**
     * 入力項目名設定
     */
    private function setInputItems() {

        $inputItems = new DutyShiftReportInputItems();
        $this->_BasicItems = $inputItems->GetBasicItems();
    }

    /**
     * 基本情報のインポート
     */
    public function ImportBasic() {

        $this->_DutyShiftReportImporterBasic->SetBasicData($this->_BasicData);
        $data = $this->_DutyShiftReportImporterBasic->Import();
        $data["ErrorMessage"] = $this->_DutyShiftReportImporterBasic->GetErrorMessage();
        return $data;
    }

    /**
     * 基本情報一覧のインポート
     */
    public function ImportBasicList() {

        $this->_DutyShiftReportImporterBasic->SetBasicData($this->_BasicData);
        return $this->_DutyShiftReportImporterBasic->ImportList();
    }

    /**
     * 移動情報
     * インポートデータの配列を返す
     */
    public function ImportChangeInformation() {

        $this->_DutyShiftReportImporterChangeInformation->SetBasicData($this->_BasicData);
        $data = $this->_DutyShiftReportImporterChangeInformation->Import();
        $data["ErrorMessage"] = $this->_DutyShiftReportImporterChangeInformation->GetErrorMessage();
        return $data;
    }

    /**
     * 移動情報
     * もともと画面に設定されていたデータとインポートデータの配列を返す
     */
    public function ImportChangeInformationAll() {

        $this->_DutyShiftReportImporterChangeInformation->SetBasicData($this->_BasicData);
        $this->_DutyShiftReportImporterChangeInformation->SetChangeInformationData($this->_ChangeInformation);
        $data = $this->_DutyShiftReportImporterChangeInformation->ImportAll();
        $data["ErrorMessage"] = $this->_DutyShiftReportImporterChangeInformation->GetErrorMessage();
        return $data;
    }

    /**
     * 移動情報
     * もともと画面に設定されていたデータとインポートデータより選択したデータを合わせ
     * た配列を返す
     */
    public function ImportChangeInformationPreviewReg($searchData) {

        $this->_DutyShiftReportImporterChangeInformation->SetChangeInformationData($this->_ChangeInformation);
        $data = $this->_DutyShiftReportImporterChangeInformation->ImportPreviewReg($searchData);
        $data["ErrorMessage"] = $this->_DutyShiftReportImporterChangeInformation->GetErrorMessage();
        return $data;
    }

    /**
     * 重症情報のインポート
     * 前日のデータを取得
     */
    public function ImportSeriousIllness() {

        $this->_DutyShiftReportImpoterSeriousIllness->SetBasicData($this->_BasicData);
        $this->_DutyShiftReportImpoterSeriousIllness->SetSeriousIllnessData($this->_SeriousIllnessData);
        return $this->_DutyShiftReportImpoterSeriousIllness->Import();
    }

    /**
     * 
     */
    public function ImportSeriousIllnessAll() {

        $this->_DutyShiftReportImpoterSeriousIllness->SetBasicData($this->_BasicData);
        $this->_DutyShiftReportImpoterSeriousIllness->SetSeriousIllnessData($this->_SeriousIllnessData);
        return $this->_DutyShiftReportImpoterSeriousIllness->ImportAll();
    }

    /**
     * 重症情報
     * プレビューで選択したデータを返す
     * @param type $searchData
     * @return type
     */
    public function ImportSeriousIllnessPreviewReg($searchData) {

        $this->_DutyShiftReportImpoterSeriousIllness->SetSeriousIllnessData($this->_SeriousIllnessData);
        $data = $this->_DutyShiftReportImpoterSeriousIllness->ImportPreviewReg($searchData);
        $data["ErrorMessage"] = $this->_DutyShiftReportImpoterSeriousIllness->GetErrorMessage();
        return $data;
    }

    /**
     * 勤務者情報のインポート
     * インポートデータを返す
     */
    public function ImportWorkersInfo() {

        $this->_DutyShiftReportImporterWorkersInfo->SetBasicData($this->_BasicData);
        $data = $this->_DutyShiftReportImporterWorkersInfo->Import();
        $data["ErrorMessage"] = $this->_DutyShiftReportImporterWorkersInfo->GetErrorMessage();
        return $data;
    }

    /**
     * 夜間入退院情報のインポート
     */
    public function ImportNightHospitalization() {

        $this->_DutyShiftReportImporterNightHospitalization->SetBasicData($this->_BasicData);
        $data = $this->_DutyShiftReportImporterNightHospitalization->Import();
        $data["ErrorMessage"] = $this->_DutyShiftReportImporterNightHospitalization->GetErrorMessage();
        return $data;
    }

    /**
     * 
     * @return type
     */
    public function ImportNightHospitalizationAll() {

        $this->_DutyShiftReportImporterNightHospitalization->SetBasicData($this->_BasicData);
        $this->_DutyShiftReportImporterNightHospitalization->SetNightHospitalizationData($this->_NightHospitalization);
        $data = $this->_DutyShiftReportImporterNightHospitalization->ImportAll();
        $data["ErrorMessage"] = $this->_DutyShiftReportImporterNightHospitalization->GetErrorMessage();
        return $data;
    }

    /**
     * 
     * @param type $searchData
     * @return type
     */
    public
            function ImportNightHospitalizationPreviewReg($searchData) {

        $this->_DutyShiftReportImporterNightHospitalization->SetNightHospitalizationData($this->_NightHospitalization);
        $data = $this->_DutyShiftReportImporterNightHospitalization->ImportPreviewReg($searchData);
        $data["ErrorMessage"] = $this->_DutyShiftReportImporterNightHospitalization->GetErrorMessage();
        return $data;
    }

    /**
     * 夜間巡回のインポート
     */
    public function ImportNightPatroltime() {

        $this->_DutyShiftReportImporterNightPatroltime->SetBasicData($this->_BasicData);
        return $this->_DutyShiftReportImporterNightPatroltime->Import();
    }

}
