<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix 勤務シフト作成｜勤務日誌</title>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript" src="js/duty_shift/duty_shift_diary_bld_manage.js"></script>
  <script type="text/javascript">
      /**
       * インポート一覧の表示
       * @param {type} value
       * @returns {undefined}
       */
      function DispImportList(value) {

          obj = document.getElementById(value).style;
          obj.display = 'block';
      }

      /**
       * 履歴表示
       * @returns {undefined}
       */
      function DisplayHistory() {

          obj = document.getElementById("history_detail").style;
          if (obj.display === 'none') {
              obj.display = 'block';
          } else {
              obj.display = 'none';
          }
      }

      /**
       * インポート一覧を閉じる
       * @param {type} value
       * @returns {undefined}
       */
      function CloseImport(value) {

          obj = document.getElementById(value).style;
          obj.display = 'none';
      }

      /**
       * 
       * @param {type} category
       * @param {type} no
       * @returns {undefined}
       */
      function searchSubmitButton(category, no) {

          document.mainform.no.value = no;
          submitButton(category);
      }

      /**
       * 実行ボタン
       * @param {type} category
       * @returns {undefined}
       */
      function submitButton(category) {

          section = "";
          if (getUa() !== "chrome") {
              section = "#" + getSection(category);
          }

          document.mainform.proc.value = category;
          if (category === 'PDF_A') {
              document.mainform.action = 'kanrinishi/controller/DutyShiftReportPdf.php';
              document.mainform.target = "_blank";
              document.mainform.submit();
          }
          else {
              document.mainform.action = 'duty_shift_diary_bld_manage.php' + section;
              document.mainform.target = "floatingpage";
              document.mainform.submit();
          }
      }

      /**
       * セクション取得
       * @param {type} category
       * @returns {undefined|String|Boolean}
       */
      function getSection(category) {

          switch (category) {
              case 'IMPORT_1-2':
              case 'IMPORT_1-2-2':
              case 'SAVE_1-2':
                  return 's1';
              case 'IMPORT_2':
              case 'IMPORT_2-1':
              case 'SAVE_2':
                  return 's2';
              case 'IMPORT_3':
              case 'IMPORT_3-1':
              case 'SAVE_3':
              case 'SEARCH_3':
              case 'SEARCH_3-1':
                  return 's3';
              case 'IMPORT_4':
              case 'IMPORT_4-1':
              case 'SAVE_4':
                  return 's4';
              case 'IMPORT_5':
              case 'IMPORT_5-1':
              case 'SAVE_5':
                  return 's5';
              default:
                  return "";
          }
      }

      /**
       * ブラウザ判定
       * @returns {undefined}
       */
      function getUa() {
          var userAgent = window.navigator.userAgent.toLowerCase();

          if (userAgent.indexOf('opera') !== -1) {
              return 'opera';
          } else if (userAgent.indexOf('msie') !== -1) {
              return 'ie';
          } else if (userAgent.indexOf('chrome') !== -1) {
              return 'chrome';
          } else if (userAgent.indexOf('safari') !== -1) {
              return 'safari';
          } else if (userAgent.indexOf('gecko') !== -1) {
              return 'gecko';
          } else {
              return false;
          }
      }

      /**
       * 重症セルの設定
       * @param {type} no 
       * @param {type} name
       * @param {type} age
       * @param {type} sex
       * @returns {undefined}
       */
      function SetIllnessCell(no, name, age, sex) {

          obj = document.getElementById('serious_illness_button_' + no);
          target = obj.parentNode;
          for (i = 0; i < 7; i++) {
              target = target.nextElementSibling;
              switch (i) {
                  case 3://氏名
                      target.firstElementChild.value = name;
                      break;
                  case 4://年齢
                      target.firstElementChild.value = age;
                      break;
                  case 5://性別
                      target.firstElementChild.value = sex;
                      break;
              }
          }
          CloseImport('import_serious_illness_search');
      }

      /**
       * 重症・要観察・手術 全チェックボタン
       * @param {type} obj
       * @returns {undefined}
       */
      function seriousIllnessAllCheck(obj) {

          if (obj.checked) {
              check = true;
          } else {
              check = false;
          }

          for (i = 0; i < <?php echo LINE_SERIOUS_ILLNESS; ?>; i++) {
              element = document.getElementById('serious_illness_search_check' + i);
              element.checked = check;
          }
      }

      /**
       * 重症・要観察・手術
       * セルのクリア
       * @param {type} obj
       * @returns {undefined}
       */
      function ClearCell(obj) {

          var target = obj.parentNode;
          for (i = 0; i < 7; i++) {
              target = target.nextElementSibling;
              target.firstElementChild.value = '';
          }
      }

      /**
       * 検索ウィンドウのオープン
       * @returns {undefined}
       */
      function OpenSearchWindow() {

          window.open('search.php', 'win', 'width=500,height=400,menubar=yes,status=yes,scrollbars=yes');
          return false;
      }

      /**
       * ロードに時の処理
       * @returns {undefined}
       */
      function Load() {

          /**
           * カテゴリによる表示制御
           */
          switch ('<?php echo $this->_BasicData[0]["proc"]; ?>') {
              case 'IMPORT_1-2':
                  obj = document.getElementById("import_patient_num").style;
                  obj.display = 'block';
                  break;
              case 'IMPORT_2':
                  obj = document.getElementById("import_change_list").style;
                  obj.display = 'block';
                  break;
              case 'IMPORT_3':
                  obj = document.getElementById("import_serious_illness_list").style;
                  obj.display = 'block';
                  break;
              case 'SEARCH_3':
              case 'SEARCH_3-1':
                  obj = document.getElementById("import_serious_illness_search").style;
                  obj.display = 'block';
                  break;
              case 'IMPORT_5':
                  obj = document.getElementById("import_night_hospitalization_list").style;
                  obj.display = 'block';
                  break;
          }

          /**
           * エラーメッセージの表示
           */
          if ('<?php echo $this->_ErrorMessage; ?>' !== '') {
              alert('<?php echo $this->_ErrorMessage; ?>');
          }
      }

  </script>

  <link rel="stylesheet" type="text/css" href="css/main.css">
  <style type="text/css">
   .list {
       border-collapse:collapse;
   }
   .list td {
       border:#5279a5 solid 1px;
   }
   span.link{
       text-decoration: underline;
       cursor:pointer;
   }
   div.WinTitle{
       padding:10px;
   }
  </style>
 </head>
 <body onload="Load();" bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td>
     <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
       <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr bgcolor="#f6f9ff"> 
          <td width="32" height="32" class="spacing"> 
           <a href="duty_shift_results.php?session=<?php echo($session); ?>"> 
            <img src="img/icon/duty_shift.gif" width="32" height="32" border="0" alt="勤務シフト作成"> 
           </a></td> 
          <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp; 
            <a href="duty_shift_results.php?session=<?php echo($session); ?>"><b>勤務シフト作成</b></a> 
           </font></td> 
          <td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"> 
            <a href="duty_shift_entity_menu.php?session=<?php echo($session); ?>"><b>管理画面へ</b></a> 
           </font></td> 
         </tr> 
        </table>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
          <td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="duty_shift_menu.php?session=<?php echo($session); ?>&group_id=120900000023"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>シフト作成</nobr></font></a></td>
          <td width="5">&nbsp;</td>
          <td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="duty_shift_staff.php?session=<?php echo($session); ?>&group_id=120900000023"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>職員設定</nobr></font></a></td>
          <td width="5">&nbsp;</td>
          <td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="duty_shift_results.php?session=<?php echo($session); ?>&group_id=120900000023"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>勤務実績入力</nobr></font></a></td>
          <td width="5">&nbsp;</td>
          <td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="duty_shift_need_cnt_standard.php?session=<?php echo($session); ?>&group_id=120900000023"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>必要人数設定</nobr></font></a></td>
          <td width="5">&nbsp;</td>
          <td width="120" height="22" align="center" bgcolor="#5279a5"><a href="duty_shift_diary_bld_manage.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><nobr><b>勤務日誌</b></nobr></font></a></td>
          <td width="5">&nbsp;</td>
          <td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="duty_shift_report.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>届出書添付書類</nobr></font></a></td>
          <td width="5">&nbsp;</td>
          <td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="duty_shift_mprint.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>管理帳票</nobr></font></a></td>
          <td width="5">&nbsp;</td>
          <td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="duty_shift_auto.php?session=<?php echo($session); ?>&group_id=120900000023"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>シフトグループ設定</nobr></font></a></td>
          <td width="5">&nbsp;</td>
          <td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="duty_shift_duty.php?session=<?php echo($session); ?>&group_id=120900000023"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>当直予定表作成</nobr></font></a></td>
          <td width="5">&nbsp;</td>
          <td>&nbsp;</td>
         </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
          <td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
         </tr>
        </table>
        <img src="img/spacer.gif" width="1" height="5" alt=""><br />
       </td>
      </tr>
     </table>

     <form name="mainform" method="post" action="duty_shift_diary_bld_manage.php">
      <table width="960" border="0" cellspacing="0" cellpadding="2" class="">
       <tr>
        <td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>病棟管理日誌</b></font></td>
        <td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary.php?session=<?php echo($session); ?>">勤務データ出力</a></font></td>
        <td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_allotment.php?session=<?php echo($session); ?>">勤務分担表</a></font></td>
        <td align="left" width="140"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_sfc.php?session=<?php echo($session); ?>">管理日誌（SFC形式）</a></font></td>
        <td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_list.php?session=<?php echo($session); ?>">在院者一覧</a></font></td>
        <td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_duty.php?session=<?php echo($session); ?>">当直者一覧</a></font></td>
        <td align="left" width=""></td>
       </tr>
      </table>

      <div style="padding : 10px;">
       シフトグループ（病棟）の選択
       <select name="shift_group" onchange="submitButton('LOAD_A');">
        <!--        <option value="">&nbsp;</option>-->
        <?php
        for ($i = 0; $i < count($this->_DutyShiftGroup); $i++) {
            $wk_id = $this->_DutyShiftGroup[$i]["group_id"];
            $wk_name = $this->_DutyShiftGroup[$i]["group_name"];
            echo("<option value=\"$wk_id\"");
            if ($this->_BasicData[0]["shift_group"] == $wk_id) {
                echo(" selected");
            }
            echo(">" . $wk_name . "</option>\n");
        }
        ?>
       </select>
      </div>

      <div style="padding : 10px;">
       年月日の指定
       <select id='start_yr' name='start_yr' onchange="submitButton('LOAD_A');"><?php
           show_select_years(15, $start_yr, false);
           ?></select>年
       <select id='start_mon' name='start_mon' onchange="submitButton('LOAD_A');"><?php
           show_select_months($start_mon, false);
           ?></select>月
       <select id='start_day' name='start_day' onchange="submitButton('LOAD_A');"><?php
           show_select_days($start_day, false);
           ?></select>日

       <!-- 表示ボタン -->
       <input type="submit" name="actionDisp" value="表示" onclick="submitButton('LOAD_A');" >
       <!--<input type="submit" name="actionDisp" value="一括取込" onclick="submitButton('IMPORT_A');">-->
       <!--<input type="submit" name="actionDisp" value="一括保存" onclick="submitButton('SAVE_A');" >-->
       <input type="submit" name="actionDisp" value="印刷（PDF）" onclick="submitButton('PDF_A');" >
      </div>

      <!-- ------------------------------------------------------------------------ -->
      <!-- ＨＩＤＤＥＮ -->
      <!-- ------------------------------------------------------------------------ -->
      <input type="hidden" name="session" value="<?php echo($session); ?>">
      <input type="hidden" name="proc" value="Z_Z">
      <input type="hidden" name="report_date" value="">
      <input type="hidden" name="no" value="<?php
      if (isset($this->_BasicData[0]["no"])) {
          echo $this->_BasicData[0]["no"];
      };
      ?>">

      <div style="padding : 10px;">
       <div style="float:left;width:300px;">
        &nbsp;
        <!--日勤責任者
        <input type="text" name="day_shift_name" size="25" maxlength="20" value="<?php //echo $this->_BasicData[0]["day_shift_name"];                                                                                                 ?>">
        -->
       </div>
       <div style="float:left;width:320px;">
        &nbsp;
        <!--
        夜勤責任者
        <input type="text" name="night_shift_name" size="25" maxlength="20" value="<?php //echo $this->_BasicData[0]["night_shift_name"];                                                                                                 ?>">
        <input type="submit" name="actionDisp" value="保存" onclick="submitButton('SAVE_1-1');" >
        -->
       </div>

       <div style="float:left;width:320px;position: relative;">
           <?php
           if (count($this->_HistoryData) > 0) {
               ?>
            <span style="text-decoration: underline;cursor:pointer;" onclick="DisplayHistory();">記入者&nbsp;
             <?php echo $this->_HistoryData[0]["entry_name"]; ?>&nbsp;
             <?php echo $this->_HistoryData[0]["history_date"]; ?>
            </span>

            <?php include_once(APP_BASE . "view/Elements/DutyShiftHistoryDetail.php"); ?>

            <?php
        }
        ?>
       </div>

       <div style="clear:left;"></div>

       <a name="s1"></a>
       <div style="padding:10px;width:350px;float:left;">
        <span>患者数等</span> 
       </div>

       <?php include_once(APP_BASE . "view/Elements/DutyShiftImportPatientNum.php"); ?>

       <div style="padding:10px;">
        <input type="submit" name="actionDisp" value="データ取込" onclick="submitButton('IMPORT_1-2');
               ">
        <input type ="submit" name="actionDisp" value="保存"  onclick="submitButton('SAVE_1-2');">
        <hr style="text-align:left;width:500px;margin-left:0px" />
       </div>

       <?php
       if (isset($this->_BasicData["ErrorMessage"])) {
           ?>
           <span style="color:red"><?php echo $this->_BasicData["ErrorMessage"]; ?></span>
           <?php
       }
       ?>

       <div style="padding:10px;">
        <table class="list">
         <tr bgcolor="#f6f9ff">
          <td style = "width:100px;text-align:center;"><span class = "">定数</span></td>
          <td style = "width:100px;text-align:center;"><span class = "">前日数</span></td>
          <td style = "width:100px;text-align:center;"><span class = "">現在数</span></td>
          <td style = "width:100px;text-align:center;"><span class = "">入院</span></td>
          <td style = "width:100px;text-align:center;"><span class = "">転入</span></td>
          <td style = "width:100px;text-align:center;"><span class = "">退院</span></td>
          <td style = "width:100px;text-align:center;"><span class = "">転出</span></td>
          <td style = "width:100px;text-align:center;"><span class = "">外泊</span></td>
          <td style = "width:100px;text-align:center;"><span class = "">外出</span></td>
          <td style = "width:100px;text-align:center;"><span class = "">付添</span></td>
         </tr>
         <tr >
          <td style = "width:100px;text-align:center;"><input type = "text" name = "constant" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["constant"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "before_num" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["before_num"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "current_num" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["current_num"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "hostpitaladmissions_num" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["hostpitaladmissions_num"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "transference_num" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["transference_num"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "discharge_num" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["discharge_num"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "moving_out_num" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["moving_out_num"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "sleepover_num" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["sleepover_num"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "goingout_num" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["goingout_num"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "attendant_num" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["attendant_num"]; ?>"></td>
         </tr>
        </table>

        <table class = "list">
         <tr>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "course_name1" size = "8" maxlength = "10" value = "<?php echo $this->_BasicData[0]["course_name1"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "course_name2" size = "8" maxlength = "10" value = "<?php echo $this->_BasicData[0]["course_name2"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "course_name3" size = "8" maxlength = "10" value = "<?php echo $this->_BasicData[0]["course_name3"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "course_name4" size = "8" maxlength = "10" value = "<?php echo $this->_BasicData[0]["course_name4"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "course_name5" size = "8" maxlength = "10" value = "<?php echo $this->_BasicData[0]["course_name5"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "course_name6" size = "8" maxlength = "10" value = "<?php echo $this->_BasicData[0]["course_name6"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "palliativecare_name" size = "8" maxlength = "10" value = "<?php echo $this->_BasicData[0]["palliativecare_name"]; ?>"></td>
          <td style = "width:100px;text-align:center;" bgcolor = "#f6f9ff">担送</td>
          <td style = "width:100px;text-align:center;" bgcolor = "#f6f9ff">護送</td>
          <td style = "width:100px;text-align:center;" bgcolor = "#f6f9ff">独歩</td>
         </tr>
         <tr>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "course_num1" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["course_num1"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "course_num2" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["course_num2"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "course_num3" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["course_num3"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "course_num4" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["course_num4"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "course_num5" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["course_num5"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "course_num6" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["course_num6"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "palliativecare_num" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["palliativecare_num"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "stretcher_num" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["stretcher_num"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "convoy_num" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["convoy_num"]; ?>"></td>
          <td style = "width:100px;text-align:center;"><input type = "text" name = "independent_num" size = "2" maxlength = "2" value = "<?php echo $this->_BasicData[0]["independent_num"]; ?>"></td>
         </tr>
        </table>
       </div>

       <div>
        <div style = "padding:10px;float:left;">
         <span style = "">看護度</span>
         <table class = "list">
          <tr bgcolor = "#f6f9ff">
           <td style = "width:50px;text-align:center;"></td>
           <td style = "width:50px;text-align:center;">I</td>
           <td style = "width:50px;text-align:center;">II</td>
           <td style = "width:50px;text-align:center;">III</td>
           <td style = "width:50px;text-align:center;">IV</td>
          </tr>
          <tr >
           <td style = "width:50px;text-align:center;">A</td>
           <td style = "width:50px;text-align:center;"><input type = "text" name = "nursing_degree_a1" size = "2" maxlength = "2" value = "<?php echo h($this->_BasicData[0]["nursing_degree_a1"]); ?>"></td>
           <td style = "width:50px;text-align:center;"><input type = "text" name = "nursing_degree_a2" size = "2" maxlength = "2" value = "<?php echo h($this->_BasicData[0]["nursing_degree_a2"]); ?>"></td>
           <td style = "width:50px;text-align:center;"><input type = "text" name = "nursing_degree_a3" size = "2" maxlength = "2" value = "<?php echo h($this->_BasicData[0]["nursing_degree_a3"]); ?>"></td>
           <td style = "width:50px;text-align:center;"><input type = "text" name = "nursing_degree_a4" size = "2" maxlength = "2" value = "<?php echo h($this->_BasicData[0]["nursing_degree_a4"]); ?>"></td>
          </tr>
          <tr >
           <td style = "width:50px;text-align:center;">B</td>
           <td style = "width:50px;text-align:center;"><input type = "text" name = "nursing_degree_b1" size = "2" maxlength = "2" value = "<?php echo h($this->_BasicData[0]["nursing_degree_b1"]); ?>"></td>
           <td style = "width:50px;text-align:center;"><input type = "text" name = "nursing_degree_b2" size = "2" maxlength = "2" value = "<?php echo h($this->_BasicData[0]["nursing_degree_b2"]); ?>"></td>
           <td style = "width:50px;text-align:center;"><input type = "text" name = "nursing_degree_b3" size = "2" maxlength = "2" value = "<?php echo h($this->_BasicData[0]["nursing_degree_b3"]); ?>"></td>
           <td style = "width:50px;text-align:center;"><input type = "text" name = "nursing_degree_b4" size = "2" maxlength = "2" value = "<?php echo h($this->_BasicData[0]["nursing_degree_b4"]); ?>"></td>
          </tr>
          <tr >
           <td style = "width:50px;text-align:center;"><span class = "">C</span></td>
           <td style = "width:50px;text-align:center;"><input type = "text" name = "nursing_degree_c1" size = "2" maxlength = "2" value = "<?php echo h($this->_BasicData[0]["nursing_degree_c1"]); ?>"></td>
           <td style = "width:50px;text-align:center;"><input type = "text" name = "nursing_degree_c2" size = "2" maxlength = "2" value = "<?php echo h($this->_BasicData[0]["nursing_degree_c2"]); ?>"></td>
           <td style = "width:50px;text-align:center;"><input type = "text" name = "nursing_degree_c3" size = "2" maxlength = "2" value = "<?php echo h($this->_BasicData[0]["nursing_degree_c3"]); ?>"></td>
           <td style = "width:50px;text-align:center;"><input type = "text" name = "nursing_degree_c4" size = "2" maxlength = "2" value = "<?php echo h($this->_BasicData[0]["nursing_degree_c4"]); ?>"></td>
          </tr>
         </table>
        </div>

        <div style = "padding:10px;">
         <span style = "">患者状況</span>
         <table class = "list">
             <?php
             for ($i = 1; $i <= 4; $i++) {
                 ?>
              <tr bgcolor="">
               <td style="width:200px;text-align:center;">
                <input type="text" name="patient_status_name<?php echo $i; ?>" size="22" maxlength="12" value="<?php echo h($this->_BasicData[0]["patient_status_name" . $i]); ?>">
               </td>
               <td style="width:50px;text-align:center;">
                <input type="text" name="patient_status_num<?php echo $i; ?>" size="2" maxlength="2" value="<?php echo h($this->_BasicData[0]["patient_status_num" . $i]); ?>">
               </td>
              </tr>
              <?php
          }
          ?>
         </table>  
        </div>
       </div>

       <div style="padding : 10px;clear: left;">
        <span style="">物品点検</span>
        <table class="list">
         <tr bgcolor="#f6f9ff">
          <td style="width:50px;text-align:center;"><span class=""></span></td>
          <td style="width:50px;text-align:center;"><span class="">深夜</span></td>
          <td style="width:50px;text-align:center;"><span class="">日勤</span></td>
          <td style="width:50px;text-align:center;"><span class="">準夜</span></td>
         </tr>
         <?php
         for ($i = 1; $i <= 8; $i++) {
             ?>
             <tr >
              <td style="width:50px;text-align:center;">
               <input type="text" name="inspection_name<?php echo $i; ?>" size="22" maxlength="20" value="<?php echo h($this->_BasicData[0]["inspection_name" . $i]); ?>">
              </td>
              <td style="width:50px;text-align:center;">
               <input type="text" name="inspection_latenight<?php echo $i; ?>" size="8" maxlength="4" value="<?php echo h($this->_BasicData[0]["inspection_latenight" . $i]); ?>">
              </td>
              <td style="width:50px;text-align:center;">
               <input type="text" name="inspection_dayshift<?php echo $i; ?>" size="8" maxlength="4" value="<?php echo h($this->_BasicData[0]["inspection_dayshift" . $i]); ?>">
              </td>
              <td style="width:50px;text-align:center;">
               <input type="text" name="inspection_quasinight<?php echo $i; ?>" size="8" maxlength="4" value="<?php echo h($this->_BasicData[0]["inspection_quasinight" . $i]); ?>">
              </td>
             </tr>
             <?php
         }
         ?>

        </table>   
       </div>

       <a name="s2"></a>
       <div style="padding:10px;width:350px;float:left;">
        <span>入院・転入・退院・転出</span> 
       </div>

       <?php include_once(APP_BASE . "view/Elements/DutyShiftImportChangePreview.php"); ?>

       <div style="padding:10px;">
        <input type="submit" name="actionDisp" value="データ取込" onclick="submitButton('IMPORT_2');">
        <input type="submit" name="actionDisp" value="保存"  onclick="submitButton('SAVE_2');">
        <hr style="text-align:left; width:500px;; margin-left:0px"/>
       </div>

       <?php
       if (isset($this->_ChangeInformationData["ErrorMessage"])) {
           ?>
           <span style="color:red"><?php echo $this->_ChangeInformationData["ErrorMessage"]; ?></span>
           <?php
       }
       ?>

       <div style="padding:10px;">
        <table class="list">
         <tr bgcolor="#f6f9ff">
          <td style="text-align:center;"><span class="">区分</span></td>
          <td style="text-align:center;"><span class="">診療科</span></td>
          <td style="text-align:center;"><span class="">時間</span></td>
          <td style="text-align:center;"><span class="">患者氏名</span></td>
          <td style="text-align:center;"><span class="">年齢</span></td>
          <td style="text-align:center;"><span class="">性別</span></td>
          <td style="text-align:center;"><span class="">主病名</span></td>
          <td style="text-align:center;"><span class="">主治医</span></td>
          <td style="text-align:center;"><span class="">特記事項</span></td>
         </tr>
         <?php
         for ($i = 0; $i < LINE_CHANGE_INFORMATION; $i++) {
             ?>
             <tr>
              <td style="text-align:center;">
               <input type="text" name="change_information[data_class][]" size="12" maxlength="12" value="<?php echo $this->_ChangeInformationData[$i]["data_class"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" name="change_information[department_name][]" size="12" maxlength="12" value="<?php echo $this->_ChangeInformationData[$i]["department_name"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" name="change_information[hour][]" size="5" maxlength="5" value="<?php echo $this->_ChangeInformationData[$i]["hour"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" name="change_information[patient_name][]" size="22" maxlength="22" value="<?php echo $this->_ChangeInformationData[$i]["patient_name"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" name="change_information[age][]" size="3" maxlength="3" value="<?php echo $this->_ChangeInformationData[$i]["age"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" name="change_information[sex][]" size="1" maxlength="3" value="<?php echo $this->_ChangeInformationData[$i]["sex"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" name="change_information[disease_name][]" size="20" maxlength="40" value="<?php echo $this->_ChangeInformationData[$i]["disease_name"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" name="change_information[attending_physician][]" size="10" maxlength="20" value="<?php echo $this->_ChangeInformationData[$i]["attending_physician"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" name="change_information[articles][]" size="60" maxlength="255" value="<?php echo $this->_ChangeInformationData[$i]["articles"]; ?>">
              </td>
             </tr>
             <?php
         }
         ?>
        </table>   
       </div>

       <a name="s3"></a>
       <div style="padding:10px;width:350px;float:left;">
        <span>重症・要観察・手術</span> 
       </div>

       <?php include_once(APP_BASE . "view/Elements/DutyShiftImportSeriousIllnessPreview.php"); ?>
       <?php include_once(APP_BASE . "view/Elements/DutyShiftImportSeriousIllnessSearch.php"); ?>

       <div style="padding:10px;">
        <input type="submit" name="actionDisp" value="データ取込" onclick="submitButton('IMPORT_3');">
        <input type="submit" name="actionDisp" value="保存"  onclick="submitButton('SAVE_3');">
        <hr style="text-align:left; width:500px;; margin-left:0px"/>
       </div>

       <?php
       if (isset($this->_SeriousIllnessData["ErrorMessage"])) {
           ?>
           <span style="color:red"><?php echo $this->_SeriousIllnessData["ErrorMessage"]; ?></span>
           <?php
       }
       ?>

       <div style="padding:10px;">
        <table class="list">
         <tr bgcolor="#f6f9ff">
          <td style="text-align:center;"><span class="">&nbsp;</span></td>
          <td style="text-align:center;"><span class="">区分</span></td>
          <td style="text-align:center;"><span class="">診療科</span></td>
          <td style="text-align:center;"><span class="">時間</span></td>
          <td style="text-align:center;"><span class="">患者氏名</span></td>
          <td style="text-align:center;"><span class="">年齢</span></td>
          <td style="text-align:center;"><span class="">性別</span></td>
          <!--<td style="text-align:center;"><span class="">日勤帯の特記事項</span></td>-->
         </tr>
         <?php
         for ($i = 0; $i < LINE_SERIOUS_ILLNESS; $i++) {
             ?>
             <tr id="tr">
              <td id="td" style="text-align:center;">
               <!--<input type="button" name="search" value="検索"  onclick="OpenSearchWindow(this);">-->
               <input ID="serious_illness_button_<?php echo $i; ?>" type="submit" name="actionDisp" value="検索"  onclick="searchSubmitButton('SEARCH_3', '<?php echo $i; ?>');">
               <input type="button" name="clear" value="クリア"  onclick="ClearCell(this);">
              </td>
              <td id="td2" style="text-align:center;">
               <select name="serious_illness[data_class][]" id="data_class">
                <option value="" >&nbsp;</option>
                <option value="重症" <?php
                if ($this->_SeriousIllnessData[$i]["data_class"] == "重症") {
                    echo "selected";
                }
                ?>>重症</option>
                <option value="要観察" <?php
                if ($this->_SeriousIllnessData[$i]["data_class"] == "要観察") {
                    echo "selected";
                }
                ?>>要観察</option>
                <option value="手術" <?php
                if ($this->_SeriousIllnessData[$i]["data_class"] == "手術") {
                    echo "selected";
                }
                ?>>手術</option>
               </select>

              </td>
              <td style="text-align:center;">
               <input type="text" id="department_name" name="serious_illness[department_name][]" size="12" maxlength="12" value="<?php echo $this->_SeriousIllnessData[$i]["department_name"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" id="hour" name="serious_illness[hour][]" size="5" maxlength="5" value="<?php echo $this->_SeriousIllnessData[$i]["hour"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" id="patient_name" name="serious_illness[patient_name][]" size="22" maxlength="22" value="<?php echo $this->_SeriousIllnessData[$i]["patient_name"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" id="age" name="serious_illness[age][]" size="3" maxlength="3" value="<?php echo $this->_SeriousIllnessData[$i]["age"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" id="sex" name="serious_illness[sex][]" size="1" maxlength="3" value="<?php echo $this->_SeriousIllnessData[$i]["sex"]; ?>">
               <input type="hidden" id="articles" name="serious_illness[articles][]" size="60" maxlength="255" value="<?php echo $this->_SeriousIllnessData[$i]["articles"]; ?>">
              </td>
             </tr>
             <?php
         }
         ?>
        </table>   
       </div>

       <div style="padding : 10px;clear: left;">
        <span style="">日勤帯の特記事項</span>
        <div style="clear:left;"></div>
        <textarea name="serious_information[articles][]" cols="58" rows="5" style="border:solid 2px;"><?php echo $this->_SeriousInformationData[0]["articles"]; ?></textarea>
        <br />
        ※各行ごとに改行を入れてください。</div>

       <a name="s4"></a>
       <div style="padding:10px;width:350px;float:left;">
        <span>勤務者情報</span> 
       </div>

       <div style="padding:10px;">
        <input type="submit" name="actionDisp" value="データ取込" onclick="submitButton('IMPORT_4');">
        <input type="submit" name="actionDisp" value="保存" onclick="submitButton('SAVE_4');" >
        <hr style="text-align:left; width:500px; margin-left:0px" />
       </div>

       <?php
       if (isset($this->_WorkersInformationData["ErrorMessage"])) {
           ?>
           <span style="color:red"><?php echo $this->_WorkersInformationData["ErrorMessage"]; ?></span>
           <?php
       }
       ?>

       <div style="">
        <div style="padding:10px 0px 10px 10px;float:left;">
         <table class="list">
          <tr bgcolor="#f6f9ff">
           <td style="text-align:center;"><span class="">区分</span></td>
           <td style="text-align:center;"><span class="">職種</span></td>
           <td style="text-align:center;"><span class="">職員氏名</span></td>
          </tr>
          <?php
          for ($i = 0; $i < LINE_WORKERS_INFORMATION / 3; $i++) {
              ?>
              <tr >
               <td style="text-align:center;"><input type="text" name="workers_information[data_class][]" size="8" maxlength="12" value="<?php echo $this->_WorkersInformationData[$i]["data_class"]; ?>"></td>
               <td style="text-align:center;"><input type="text" name="workers_information[job_category][]" size="8" maxlength="12" value="<?php echo $this->_WorkersInformationData[$i]["job_category"]; ?>"></td>
               <td style="text-align:center;"><input type="text" name="workers_information[staff_name][]" size="18" maxlength="22" value="<?php echo $this->_WorkersInformationData[$i]["staff_name"]; ?>"></td>
              </tr>
              <?php
          }
          ?>
         </table>   
        </div>

        <div style="padding:10px 0px 10px 0px;float:left;">
         <table class="list">
          <tr bgcolor="#f6f9ff">
           <td style="text-align:center;"><span class="">区分</span></td>
           <td style="text-align:center;"><span class="">職種</span></td>
           <td style="text-align:center;"><span class="">職員氏名</span></td>
          </tr>
          <?php
          for ($i = LINE_WORKERS_INFORMATION / 3; $i < LINE_WORKERS_INFORMATION / 3 * 2; $i++) {
              ?>
              <tr >
               <td style="text-align:center;"><input type="text" name="workers_information[data_class][]" size="8" maxlength="12" value="<?php echo $this->_WorkersInformationData[$i]["data_class"]; ?>"></td>
               <td style="text-align:center;"><input type="text" name="workers_information[job_category][]" size="8" maxlength="12" value="<?php echo $this->_WorkersInformationData[$i]["job_category"]; ?>"></td>
               <td style="text-align:center;"><input type="text" name="workers_information[staff_name][]" size="18" maxlength="22" value="<?php echo $this->_WorkersInformationData[$i]["staff_name"]; ?>"></td>
              </tr>
              <?php
          }
          ?>
         </table>   
        </div>

        <div style="padding:10px 0px 10px 0px;float:left;">
         <table class="list">
          <tr bgcolor="#f6f9ff">
           <td style="text-align:center;"><span class="">休暇</span></td>
           <td style="text-align:center;"><span class="">職種</span></td>
           <td style="text-align:center;"><span class="">職員氏名</span></td>
          </tr>
          <?php
          for ($i = LINE_WORKERS_INFORMATION / 3 * 2; $i < LINE_WORKERS_INFORMATION; $i++) {
              ?>
              <tr >
               <td style="text-align:center;"><input type="text" name="workers_information[data_class][]" size="8" maxlength="12" value="<?php echo $this->_WorkersInformationData[$i]["data_class"]; ?>"></td>
               <td style="text-align:center;"><input type="text" name="workers_information[job_category][]" size="8" maxlength="12" value="<?php echo $this->_WorkersInformationData[$i]["job_category"]; ?>"></td>
               <td style="text-align:center;"><input type="text" name="workers_information[staff_name][]" size="18" maxlength="22" value="<?php echo $this->_WorkersInformationData[$i]["staff_name"]; ?>"></td>
              </tr>
              <?php
          }
          ?>
         </table>   
        </div>
       </div>

       <div style="clear:left;"></div>

       <a name="s5"></a>
       <div style="padding:10px;width:350px;float:left;">
        <span>夜間入退院</span> 
       </div>

       <?php include_once(APP_BASE . "view/Elements/DutyShiftImportNightHospitalizationPreview.php"); ?>

       <div style="padding:10px;">
        <input type="submit" name="actionDisp" value="データ取込" onclick="submitButton('IMPORT_5');">
        <input type="submit" name="actionDisp" value="保存"  onclick="submitButton('SAVE_5')
                        ;">
        <hr style="text-align:left; width:500px;; margin-left:0px"/>
       </div>

       <?php
       if (isset($this->_NightHospitalizationData["ErrorMessage"])) {
           ?>
           <span style="color:red"><?php echo $this->_NightHospitalizationData["ErrorMessage"]; ?></span>
           <?php
       }
       ?>

       <div style="padding:10px;">
        <table class="list">
         <tr bgcolor="#f6f9ff">
          <td style="text-align:center;"><span class="">区分</span></td>
          <td style="text-align:center;"><span class="">診療科</span></td>
          <td style="text-align:center;"><span class="">時間</span></td>
          <td style="text-align:center;"><span class="">患者氏名</span></td>
          <td style="text-align:center;"><span class="">年齢</span></td>
          <td style="text-align:center;"><span class="">主病名</span></td>
          <td style="text-align:center;"><span class="">主治医</span></td>
         </tr>
         <?php
         for ($i = 0; $i < LINE_NIGHT_HOSPITALIZATION; $i++) {
             ?>
             <tr >
              <td style="text-align:center;">
               <input type="text" name="night_hospitalization[data_class][]" size="12" maxlength="12" value="<?php echo $this->_NightHospitalizationData[$i]["data_class"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" name="night_hospitalization[department_name][]" size="12" maxlength="12" value="<?php echo $this->_NightHospitalizationData[$i]["department_name"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" name="night_hospitalization[hour][]" size="5" maxlength="5" value="<?php echo $this->_NightHospitalizationData[$i]["hour"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" name="night_hospitalization[patient_name][]" size="22" maxlength="22" value="<?php echo $this->_NightHospitalizationData[$i]["patient_name"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" name="night_hospitalization[age][]" size="3" maxlength="3" value="<?php echo $this->_NightHospitalizationData[$i]["age"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" name="night_hospitalization[disease_name][]" size="20" maxlength="40" value="<?php echo $this->_NightHospitalizationData[$i]["disease_name"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" name="night_hospitalization[attending_physician][]" size="10" maxlength="20" value="<?php echo $this->_NightHospitalizationData[$i]["attending_physician"]; ?>">
              </td>
             </tr>
             <?php
         }
         ?>
        </table>   
       </div>

       <div style="padding : 10px;clear: left;">
        <span style="">夜間の特記・管理事項</span>
        <div style="clear:left;"></div>
        <textarea name="night_information[articles][]" cols="104" rows="5" style="border:solid 2px;"><?php echo $this->_NightInformationData[0]["articles"]; ?></textarea><br />
        ※各行ごとに改行を入れてください。
       </div>

       <div style="padding : 10px;clear: left;">
        <span style="">巡視時間</span>
        <table class="list">
         <tr bgcolor="#f6f9ff">
          <td style="text-align:center;"><span class="">区分</span></td>
          <td style="text-align:center;"><span class="">時間</span></td>
          <td style="text-align:center;"><span class="">異常</span></td>
          <td style="text-align:center;"><span class="">内容</span></td>
         </tr>

         <?php
         for ($i = 0; $i < LINE_NIGHT_PATROLTIME; $i++) {
             ?>
             <tr>
              <td style="text-align:center;">
               <input type="text" name="night_patroltime[data_class][]" size="12" maxlength="12" value="<?php echo $this->_NightPatroltimeData[$i]["data_class"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="text" name="night_patroltime[hour][]" size="5" maxlength="5" value="<?php echo $this->_NightPatroltimeData[$i]["hour"]; ?>">
              </td>
              <td style="text-align:center;">
               <input type="radio" name="night_patroltime[abnormality][<?php echo $i; ?>]" value="0" <?php
               if ($this->_NightPatroltimeData[$i]["abnormality"] == 0) {
                   echo "checked";
               }
               ?>>未入力
               <input type="radio" name="night_patroltime[abnormality][<?php echo $i; ?>]" value="1" <?php
               if ($this->_NightPatroltimeData[$i]["abnormality"] == 1) {
                   echo "checked";
               }
               ?> >異常なし
               <input type="radio" name="night_patroltime[abnormality][<?php echo $i; ?>]" value="2" <?php
               if ($this->_NightPatroltimeData[$i]["abnormality"] == 2) {
                   echo "checked";
               }
               ?>>異常あり
              </td>
              <td style="text-align:center;">
               <input type="text" name="night_patroltime[articles][]" size="22" maxlength="22" value="<?php echo $this->_NightPatroltimeData[$i]["articles"]; ?>">
              </td>
             </tr>
             <?php
         }
         ?>
        </table>   
       </div>
     </form>
     <iframe name="download" width="0" height="0" frameborder="0"></iframe>
    </td>
   </tr>
  </table>
 </body>
</html>
