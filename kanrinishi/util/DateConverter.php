<?php

/**
 * 日付変換クラス
 */
class DateConverter {

    /**
     * 曜日取得
     * @param type $src
     * @return string
     */
    public static function GetDayoftheweek($src) {

        $datetime = new DateTime($src);
        $week = array("日", "月", "火", "水", "木", "金", "土");
        $w = (int) $datetime->format('w');

        return $week[$w];
    }

    /**
     * 西暦→和暦変換
     * @param type $src
     *          yyyy/mm/dd
     * @return boolean|string　
     *          年号yy年mm月dd日（1年の場合は元年）
     *          江戸時代等明治より前はfalseを返却
     */
    public static function ConvWarekiDate($src) {

        list($year, $month, $day) = explode("/", $src);
        if (!@checkdate($month, $day, $year) ||
                $year < 1869 ||
                strlen($year) !== 4 ||
                strlen($month) !== 2 ||
                strlen($day) !== 2)
            return false;
        $date = str_replace("/", "", $src);
        $gengo = "";
        $wayear = 0;
        if ($date >= 19890108) {
            $gengo = "平成";
            $wayear = $year - 1988;
        } elseif ($date >= 19261225) {
            $gengo = "昭和";
            $wayear = $year - 1925;
        } elseif ($date >= 19120730) {
            $gengo = "大正";
            $wayear = $year - 1911;
        } else {
            $gengo = "明治";
            $wayear = $year - 1868;
        }
        switch ($wayear) {
            case 1:
                $wadate = $gengo . "元年" . $month . "月" . $day . "日";
                break;
            default:
                $wadate = $gengo . sprintf("%02d", $wayear) . "年" . $month . "月" . $day . "日";
        }
        return $wadate;
    }

    /**
     * 時刻にコロンを付加   
     * HHMM → HH:MM
     * @param type $value
     * @return string
     */
    public static function AddCol($value) {

        if (strlen($value) != 4) {
            return "";
        }

        return substr($value, 0, 2) . ":" . substr($value, 2, 2);
    }

    /**
     * 生年月日より年齢計算
     * YYYYMMDD → NN
     * @param type $value
     * @return string
     */
    public static function CalcAge($value) {

        if ($value == "") {
            return "";
        }

        return (int) ((date('Ymd') - $value) / 10000);
    }

}
