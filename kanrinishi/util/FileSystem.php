<?php

/**
 * ファイルシステム
 */
class FileSystem {

    /**
     * 指定パスの指定日数経過後のファイルを削除
     * @param type $path
     * @param type $day
     */
    public static function DeleteFileDay($path, $day) {

        $deleteDay = date('Y-m-d', strtotime("- " . $day . " day"));
        $dir = opendir($path);
        while (false !== ($file = readdir($dir))) {
            if ($file[0] != ".") {
                $fileTime = date("Y-m-d", filemtime($path . $file));
                if ($deleteDay > $fileTime) {
                    unlink($path . $file);
                    echo "DELETE:" . $path . $file . "\n";
                }
            }
        }
        closedir($dir);
    }

}
