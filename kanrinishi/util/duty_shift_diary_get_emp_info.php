<?php
/**
 * 当モジュールは堰合さんに作成したいただいたもの。本来ならばmodelに入れるべき内容で
 * あるが、作りが違うため、utilに入れてある。
 */
require_once("about_comedix.php");
require_once("duty_shift_common_class.php");

/**
 * 勤務者情報取得
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $group_id シフトグループ（病棟）
 * @param mixed $date 対象日（YYYYMMDD形式）
 * @return array 区分、職種、役職、職員氏名、職員IDを配列で返す
 * 　$arr_emp_info[][ptn_name,job_name,st_name,emp_name,emp_id]
 */
function get_emp_info($con, $fname, $group_id, $date) {

    $obj = new duty_shift_common_class($con, $fname);
    //事由
    $arr_reason = $obj->get_reason_2("1");

    $arr_reasonnm = array();
    foreach ($arr_reason as $row) {
        $arr_reasonnm[$row["id"]] = $row["name"];
    }
    $arr_reasonnm[34] = "休暇";

    //職種
    $arr_jobnm = array();
    $arr_jobmst = $obj->get_jobmst_array();
    foreach ($arr_jobmst as $row) {
        $arr_jobnm[$row["id"]] = $row["name"];
    }

    //役職
    $arr_stnm = array();
    $arr_stmst = $obj->get_stmst_array();
    foreach ($arr_stmst as $row) {
        $arr_stnm[$row["id"]] = $row["name"];
    }

    $data_emp = array();
    //勤務シフトグループ
    $data_wktmgrp = $obj->get_wktmgrp_array();
    //シフトグループ情報
    $group_array = $obj->get_duty_shift_group_array($group_id, $data_emp, $data_wktmgrp);
    //グループID別の指定日が属する期間を取得
    $arr_date = $obj->get_term_from_group_date($group_id, $group_array, $date);
    //月またがり時の月度
    $target_yyyy = substr($date, 0, 4);
    $target_mm = (int) substr($date, 4, 2);
    $target_dd = substr($date, 6, 2);
    //前月から当月
    if ($group_array[0]["start_month_flg1"] == "0") {
        //対象日の日＞締め日 例 対象16>締め15
        if ($target_dd > $group_array[0]["end_day1"]) {
            //月を終了日の月とする
            $target_yyyy = substr($arr_date[1], 0, 4);
            $target_mm = (int) substr($arr_date[1], 4, 2);
        }
    }
    //当月から翌月
    if ($group_array[0]["month_flg1"] == "2") {
        //対象日の日＜＝締め日 例 対象15<=締め15
        if ($target_dd <= $group_array[0]["end_day1"]) {
            //月を開始日の月とする
            $target_yyyy = substr($arr_date[0], 0, 4);
            $target_mm = (int) substr($arr_date[0], 4, 2);
        }
    }
    //勤務情報取得、月ごとの勤務予定duty_shift_plan_staff、勤務実績atdbkrslt、勤務記号duty_shift_pattern、勤務パターンatdptn、職員マスタempmst、応援情報duty_shift_plan_assist
    $sql = "select a.emp_id, b.emp_lt_nm||b.emp_ft_nm as emp_name, c.pattern, c.reason, d.order_no, d.font_name, b.emp_job, b.emp_st, e.atdptn_nm, f.group_id as assist_group_id "
            . "from duty_shift_plan_staff a "
            . "left join empmst b on b.emp_id = a.emp_id "
            . "left join atdbkrslt c on c.emp_id = a.emp_id and c.date = '$date' "
            . "left join duty_shift_pattern d on d.pattern_id = c.tmcd_group_id and cast(d.atdptn_ptn_id as varchar) = c.pattern "
            //"and case when d.reason = ' ' then '' else d.reason end = c.reason "
            //START pattern='10'以外はreasonを見ないように修正
            . "and case when c.pattern = '10' then "
            . "case when d.reason = ' ' "
            . "	then ('' = c.reason or c.reason is null)  "
            . " else d.reason = c.reason "
            . "end "
            . " else 1 = 1 "
            . "end  "
            //END
            . "left join atdptn e on e.group_id = c.tmcd_group_id and cast(e.atdptn_id as varchar) = c.pattern "
            . "left join duty_shift_plan_assist f on f.emp_id = a.emp_id and f.duty_date = '$date' ";

    $cond = "where a.group_id = '$group_id' and a.duty_yyyy = $target_yyyy and a.duty_mm = $target_mm and c.pattern != '' "
            . "order by c.pattern = '10', d.order_no, a.no";

    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type = 'text/javascript' src = './js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    $i = 0;
    $arr_emp_info = array();
    while ($row = pg_fetch_array($sel)) {
//別病棟への応援を除く
        $assist_group_id = $row["assist_group_id"];
        if ($assist_group_id != "" &&
                $assist_group_id != $group_id) {
            continue;
        }
//職員ID
        $arr_emp_info[$i]["emp_id"] = $row["emp_id"];
//職員氏名
        $arr_emp_info[$i]["emp_name"] = $row["emp_name"];
//職種
        $job_id = $row["emp_job"];
        $arr_emp_info[$i]["job_name"] = $arr_jobnm[$job_id];
//役職
        $st_id = $row["emp_st"];
        $arr_emp_info[$i]["st_name"] = $arr_stnm[$st_id];

//区分（勤務パターン）
        if ($row["pattern"] != "10") {
//$ptn_name = $row["atdptn_nm"]; //出勤表の出勤パターン名
            $ptn_name = $row["font_name"]; //勤務シフト記号
        } else {
//勤務シフト記号にあれば使用
            if ($row["font_name"] != "") {
                $ptn_name = $row["font_name"];
            } else {
                $reason_id = $row["reason"];
                $ptn_name = $arr_reasonnm[$reason_id];
            }
        }
        $arr_emp_info[$i]["ptn_name"] = $ptn_name;
        $arr_emp_info[$i]["ptn_id"] = $row["pattern"]; //勤務パターンのID

        $i++;
    }

    return $arr_emp_info;
}
?>