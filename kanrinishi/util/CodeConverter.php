<?php

/**
 * コード変換クラス
 * 
 * 当クラスはマスタテーブルではなく、定数として定義されているコードと名称を変換するク
 * ラスである。
 */
class CodeConverter {

    /**
     * 診療科目名の取得
     * @param type $key
     */
    public static function GetDepatmentName($key) {

        $department = $GLOBALS['Department'];
        if (isset($department[$key])) {
            return $department[$key];
        }

        return false;
    }

    /**
     * 救護区分名の取得
     */
    public static function NursingDegreeName($key) {

        $nursingDegree = $GLOBALS['NursingDegree'];
        if (isset($nursingDegree[$key])) {
            return $nursingDegree[$key];
        }

        return false;
    }
}
