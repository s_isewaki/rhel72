<?php

/**
 * ログ出力クラス
 */
class Logger {

    const LOG_INFO = "Info";
    const LOG_ERR = "Err";

    private $_LogType;
    private $_LogPath = "";

    /**
     * コンストラクタ
     * @param type $logPath
     */
    public function __construct($logPath) {

        $this->_LogPath = $logPath;
        $this->_LogType = array(
            LOG_INFO => "[Info]",
            LOG_ERR => "[Err]"
        );
    }

    /**
     * 情報ログ
     * @param type $msg
     */
    public function PutInfo($msg) {

        $this->putLogDetail(LOG_INFO, $msg);
    }

    /**
     * エラーログ
     * @param type $msg
     */
    public function PutErr($msg) {

        $this->putLogDetail(LOG_ERR, $msg);
    }

    /**
     * ログ出力
     * @param type $logType
     * @param type $msg
     */
    private function putLogDetail($logType, $msg) {

        $trace = debug_backtrace();
        $className = $trace[2]["class"];

        $logMsg = date("Y/m/d H:i:s") . " " . $className . " " .
                $this->_LogType[$logType] . " " . $msg . "\n";
        $fileName = date("Ymd") . ".log";
        file_put_contents($this->_LogPath . $fileName, $logMsg, FILE_APPEND);
    }

}
