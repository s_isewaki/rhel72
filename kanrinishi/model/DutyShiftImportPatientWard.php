<?php

include_once(APP_BASE . "model/AppModel.php");

class DutyShiftImportPatientWard extends AppModel {

    /**
     * ����
     * @param type $sfcGroupCd
     */
    public function SearchList($sfcGroupCd, $patientCd, $patientName, $reportDateStr) {

        $sql = "SELECT " .
                "   P.patient_cd AS patient_cd, " .
                "   P.name_kana AS patient_name_kana, " .
                "   P.name_kanji AS patient_name, " .
                "   P.sex AS sex, " .
                "   P.birthday AS birthday " .
                "FROM " .
                "   duty_shift_import_patient P " .
                "   INNER JOIN duty_shift_import_patient_ward PW " .
                "   ON P.patient_cd = PW.patient_cd " .
                "WHERE " .
                "   PW.ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
                "	AND PW.in_move_date <= '" . $this->esc($reportDateStr) . "' AND ( " .
                "		(PW.out_move_date > '" . $this->esc($reportDateStr) . "' AND PW.out_move_time > '235959') " .
                "	OR PW.out_move_date = '' " .
                "	) " .
                "";

        if ($patientCd != "") {
            $sql.= "AND P.patient_cd = '" . $this->esc($patientCd) . "' ";
        } else if ($patientName != "") {
            $sql.= "AND P.name_kanji LIKE '%" . $this->esc($patientName) . "%' ";
        }

        return $this->Query($sql);
    }

    /**
     * �����������԰���
     */
    public function ImportListCurrent($sfcGroupCd, $reportDateStr) {

        $sql = "SELECT " .
                "	PW.patient_cd AS patient_cd, " .
                "	P.name_kanji AS patient_name, " .
                "	PW.department_cd AS department_cd, " .
                "	PW.sickroom_cd AS sickroom_cd " .
                "FROM " .
                "	duty_shift_import_patient_ward PW " .
                "	INNER JOIN duty_shift_import_patient P " .
                "	ON PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "	PW.in_move_date <= '" . $this->esc($reportDateStr) . "' AND ( " .
//                "		(PW.out_move_date > '" . $this->esc($reportDateStr) . "' AND PW.out_move_time > '235959') " .
                "		(PW.out_move_date > '" . $this->esc($reportDateStr) . "' ) " .
                "	OR (PW.out_move_date = '' OR PW.out_move_date IS NULL) " .
                "	) " .
                "	AND ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
                "ORDER BY " .
                "	PW.patient_cd " .
                "";

        return $this->Query($sql);
    }

    /**
     * ���Ų��̰���
     */
    public function ImportListDepartment($sfcGroupCd, $reportDateStr) {

        $sql = "SELECT " .
                "	PW.patient_cd AS patient_cd, " .
                "	P.name_kanji AS patient_name, " .
                "	PW.department_cd AS department_cd " .
                "FROM " .
                "	duty_shift_import_patient_ward PW " .
                "	INNER JOIN duty_shift_import_patient P " .
                "		ON PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "	in_move_date <= '" . $this->esc($reportDateStr) . "' " .
                "	AND (out_move_date > '" . $this->esc($reportDateStr) . "' " .
                "		OR (out_move_date = '' OR out_move_date IS NULL )) " .
                "	AND ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
                "ORDER BY " .
                "	PW.department_cd ASC " .
                "";

        return $this->Query($sql);
    }

    /**
     * ��ư���԰���
     * @param type $sfcGroupCd
     * @param type $reportDateStr
     */
    public function ImportListChangeInformation($sfcGroupCd, $reportDateStr) {

        $sql = "SELECT " .
                "	'1' AS datatype_cd, " .
                "	'����' AS datatype_name, " .
                "	P.patient_cd AS patient_cd, " .
                "	P.name_kanji AS patient_name, " .
                "	PW.department_cd AS department_cd " .
                "FROM " .
                "	duty_shift_import_patient_ward PW " .
                "	INNER JOIN duty_shift_import_patient P " .
                "		ON PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "	PW.ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
                "	AND PW.in_move_date = '" . $this->esc($reportDateStr) . "' " .
                "	AND PW.data_type1 = '1' " .
                "UNION ALL " .
                "SELECT " .
                "	'2' AS datatype_cd, " .
                "	'ž��' AS datatype_name, " .
                "	P.patient_cd AS patient_cd, " .
                "	P.name_kanji AS patient_name, " .
                "	PW.department_cd AS department_cd " .
                "FROM " .
                "	duty_shift_import_patient_ward PW " .
                "	INNER JOIN duty_shift_import_patient P " .
                "		on PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "	ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
                "	AND in_move_date = '" . $this->esc($reportDateStr) . "' " .
                "	AND data_type1 = '2' " .
                "UNION ALL " .
                "SELECT " .
                "	'3' AS datatype_cd, " .
                "	'�ౡ' AS datatype_name, " .
                "	P.patient_cd AS patient_cd, " .
                "	P.name_kanji AS patient_name, " .
                "	PW.department_cd AS department_cd " .
                "FROM " .
                "	duty_shift_import_patient_ward PW " .
                "	INNER JOIN duty_shift_import_patient P " .
                "		on PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "	ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
                "	AND out_move_date = '" . $this->esc($reportDateStr) . "' " .
                "	AND data_type2 = '5' " .
                "UNION ALL " .
                "SELECT " .
                "	'4' AS datatype_cd, " .
                "	'ž��' AS datatype_name, " .
                "	P.patient_cd AS patient_cd, " .
                "	P.name_kanji AS patient_name, " .
                "	PW.department_cd AS department_cd " .
                "FROM " .
                "	duty_shift_import_patient_ward PW " .
                "	INNER JOIN duty_shift_import_patient P " .
                "		on PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "	ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
                "	AND out_move_date = '" . $this->esc($reportDateStr) . "' " .
                "	AND data_type2 = '2' " .
                "";

        return $this->Query($sql);
    }

    /**
     * ��ư����
     * ���С�����
     * @param type $sfcGroupCd
     * @param type $reportDateStr
     * @return type
     */
    public function ImportListGooutInformation($sfcGroupCd, $reportDateStr) {

        $sql = "SELECT " .
                "	'1' AS datatype_cd, " .
                "	'����' AS datatype_name, " .
                "	P.patient_cd AS patient_cd, " .
                "	P.name_kanji AS patient_name, " .
                "	PW.department_cd AS department_cd " .
                "FROM " .
                "	duty_shift_import_patient P " .
                "	INNER JOIN duty_shift_import_patient_ward PW " .
                "		ON P.patient_cd = PW.patient_cd " .
                "	INNER JOIN duty_shift_import_goout_information GI " .
                "		ON GI.patient_cd = PW.patient_cd " .
                "WHERE " .
                "	GI.data_type = '1' " .
                " 	AND GI.goout_date_from <= '" . $this->esc($reportDateStr) . "' " .
                " 	AND (GI.goout_date_to > '" . $this->esc($reportDateStr) . "' " .
                //" 	AND (GI.goout_date_to >= '" . $this->esc($reportDateStr) . "' " .
                "		OR GI.goout_date_to = '') " .
                "	AND PW.ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
                "GROUP BY " .
                "	P.patient_cd, " .
                "	P.name_kanji, " .
                "	PW.department_cd " .
                "UNION " .
                "SELECT " .
                "	'2' AS datatype_cd, " .
                "	'����' AS datatype_name, " .
                "	P.patient_cd AS patient_cd, " .
                "	P.name_kanji AS patient_name, " .
                "	PW.department_cd AS department_cd " .
                "FROM " .
                "	duty_shift_import_patient P " .
                "	INNER JOIN duty_shift_import_patient_ward PW " .
                "		ON P.patient_cd = PW.patient_cd " .
                "	INNER JOIN duty_shift_import_goout_information GI " .
                "		ON PW.patient_cd = GI.patient_cd " .
                "WHERE " .
                "	GI.data_type = '2' " .
                " 	AND GI.goout_date_from = '" . $this->esc($reportDateStr) . "' " .
                "	AND PW.ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
                "GROUP BY " .
                "	P.patient_cd, " .
                "	P.name_kanji, " .
                "	PW.department_cd " .
                "";

        return $this->Query($sql);
    }

    /**
     * ���߿�
     * @param type $sfcGroupCd
     * @param type $reportDateStr
     */
    public function getCurrentCount($sfcGroupCd, $reportDateStr) {

        $sql = "SELECT " .
                " COUNT(*) as num " .
                " FROM " .
                " duty_shift_import_patient_ward " .
                " WHERE " .
                " in_move_date <= '" . $this->esc($reportDateStr) . "' AND ( " .
//                " (out_move_date > '" . $this->esc($reportDateStr) . "' AND out_move_time > '235959') " .
                " (out_move_date > '" . $this->esc($reportDateStr) . "') " .
                " OR (out_move_date = '' OR out_move_date IS NULL) " .
                " ) " .
                "	AND ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
                "";

        return $this->Query($sql);
    }

    /**
     * ��祱�����μ���
     * @param type $sfcGroupCd
     * @param type $reportDateStr
     */
    public function getCareCount($sfcGroupCd, $reportDateStr) {

        $sql = "SELECT " .
                " COUNT(*) as num " .
                " FROM " .
                " duty_shift_import_patient_ward " .
                " WHERE " .
                " in_move_date <= '" . $this->esc($reportDateStr) . "' AND ( " .
//                " (out_move_date > '" . $this->esc($reportDateStr) . "' AND out_move_time > '235959') " .
                " (out_move_date > '" . $this->esc($reportDateStr) . "') " .
                " OR (out_move_date = '' OR out_move_date IS NULL) " .
                " ) " .
                "	AND ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
//                "	AND sickroom_cd = '411' " .
                "	AND sickroom_cd IN(" . AREA_CARE_CD . ") " .
                "";

        return $this->Query($sql);
    }

    /**
     * ������ž�����ౡ��ž��
     */
    public function GetMoveCount($sfcGroupCd, $reportDateStr) {

        $sql = "SELECT " .
                "(SELECT" . //����
                "   COUNT(*) AS CountNum " .
                "FROM " .
                "   duty_shift_import_patient_ward " .
                "WHERE " .
                "   ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
                "   AND in_move_date = '" . $this->esc($reportDateStr) . "' " .
                "   AND data_type1 = '1') AS hostpitaladmissions_num, " .
                "(SELECT " .
                "	COUNT(*) " .
                "FROM " .
                "	duty_shift_import_patient_ward PW " .
                "	INNER JOIN duty_shift_import_patient P " .
                "		ON PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "	ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
                "	AND out_move_date = '" . $this->esc($reportDateStr) . "' " .
                "	AND data_type2 = '5') AS discharge_num, " .
                "(SELECT " . //ž��
                "   COUNT(*) AS CountNum " .
                "FROM " .
                "	duty_shift_import_patient_ward " .
                "WHERE " .
                "	ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
                "	AND in_move_date = '" . $this->esc($reportDateStr) . "' " .
                "	AND data_type1 = '2') AS transference_num, " .
                "(SELECT " .
                "	COUNT(*) " .
                "FROM " .
                "	duty_shift_import_patient_ward PW " .
                "	INNER JOIN duty_shift_import_patient P " .
                "		ON PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "	ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
                "	AND out_move_date = '" . $this->esc($reportDateStr) . "' " .
                "	AND data_type2 = '2') AS moving_out_num " .
                "";

        return $this->Query($sql);
    }

    /**
     * ���Ų��̡�̾�Ρ��Ϳ�
     */
    public function GetDepartmentCount($reportDateStr, $sfcGroupCd) {

        $sql = "SELECT " .
                "	department_cd as department_cd, " .
                "	count(*) as num " .
                "FROM " .
                "	duty_shift_import_patient_ward " .
                "WHERE " .
                "	in_move_date <= '" . $this->esc($reportDateStr) . "' " .
                "	AND (out_move_date > '" . $this->esc($reportDateStr) . "' " .
                "		OR (out_move_date = '' OR out_move_date IS NULL) ) " .
                "   AND ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
                "GROUP BY " .
                "	department_cd " .
                "ORDER BY " .
                "	department_cd " .
                "";

        return $this->Query($sql);
    }

    /**
     * ��ư���󥤥�ݡ���
     */
    public function ImportChangeInfomation($tagetDate, $wardCd) {

        $sql = "" .
                $this->gethostpitaladmissionsSql($tagetDate, $wardCd) .
                "UNION " .
                $this->getTransferenceSql($tagetDate, $wardCd) .
                "UNION " .
                $this->getDischargeSql($tagetDate, $wardCd) .
                "UNION " .
                $this->getMovingoutSql($tagetDate, $wardCd) .
                "ORDER BY move_time ASC ";

        return $this->Query($sql);
    }

    /**
     * ������ 
     */
    private function gethostpitaladmissionsSql($tagetDate, $wardCd) {

        return "SELECT " .
                "   '����' AS datatype_name, " .
                "   PW.department_cd AS department_cd, " .
                "   PW.in_move_time AS move_time, " .
                "   P.name_kanji AS name_kanji, " .
                "   P.birthday AS birthday, " .
                "   CASE P.sex  " .
                "   	WHEN 'M' THEN '��' " .
                "   	WHEN 'F' THEN '��' " .
                "   	ELSE '' " .
                "   END AS sex, " .
                "   PW.disease_name AS disease_name, " .
                "   PW.attending_physician AS attending_physician " .
                "FROM " .
                "	duty_shift_import_patient_ward PW " .
                "	inner join duty_shift_import_patient P " .
                "		on PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "	ward_cd = '" . $this->esc($wardCd) . "' " .
                "	AND in_move_date = '" . $this->esc($tagetDate) . "' " .
                "	AND data_type1 = '1' " .
                "";
    }

    /**
     * ž����	
     */
    private function getTransferenceSql($tagetDate, $wardCd) {

        return "SELECT " .
                "   'ž��' AS datatype_name, " .
                "   PW.department_cd AS department_cd, " .
                "   PW.in_move_time AS move_time, " .
                "   P.name_kanji AS name_kanji, " .
                "   P.birthday AS birthday, " .
                "   CASE P.sex " .
                "       WHEN 'M' THEN '��' " .
                "       WHEN 'F' THEN '��' " .
                "       ELSE '' " .
                "   END AS sex, " .
                "   PW.disease_name AS disease_name, " .
                "   PW.attending_physician AS attending_physician " .
                "FROM " .
                "   duty_shift_import_patient_ward PW " .
                "   INNER JOIN duty_shift_import_patient P " .
                "   ON PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "   ward_cd = '" . $this->esc($wardCd) . "' " .
                "   AND in_move_date = '" . $this->esc($tagetDate) . "' " .
                "   AND data_type1 = '2' " .
                "";
    }

    /**
     * �ౡ��	
     */
    private function getDischargeSql($tagetDate, $wardCd) {

        return "SELECT " .
                "   '�ౡ' AS datatype_name, " .
                "   PW.department_cd AS department_cd, " .
                "   PW.out_move_time AS move_time, " .
                "   P.name_kanji AS name_kanji, " .
                "   P.birthday AS birthday, " .
                "   CASE P.sex " .
                "       WHEN 'M' THEN '��' " .
                "       WHEN 'F' THEN '��' " .
                "       ELSE '' " .
                "   END AS sex, " .
                "   PW.disease_name AS disease_name, " .
                "   PW.attending_physician AS attending_physician " .
                "FROM " .
                "   duty_shift_import_patient_ward PW " .
                "   INNER JOIN duty_shift_import_patient P " .
                "   ON PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "   ward_cd = '" . $this->esc($wardCd) . "' " .
                "   AND out_move_date = '" . $this->esc($tagetDate) . "' " .
                "   AND data_type2 = '5' " .
                "";
    }

    /**
     * ž�п�	
     */
    private function getMovingoutSql($tagetDate, $wardCd) {

        return "SELECT " .
                "   'ž��' AS datatype_name, " .
                "   PW.department_cd AS department_cd, " .
                "   PW.out_move_time AS move_time, " .
                "   P.name_kanji AS name_kanji, " .
                "   P.birthday AS birthday, " .
                "   CASE P.sex " .
                "   WHEN 'M' THEN '��' " .
                "   WHEN 'F' THEN '��' " .
                "   ELSE '' " .
                "   END AS sex, " .
                "   PW.disease_name AS disease_name, " .
                "   PW.attending_physician AS attending_physician " .
                "FROM " .
                "   duty_shift_import_patient_ward PW " .
                "   INNER JOIN duty_shift_import_patient P " .
                "   ON PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "   ward_cd = '" . $this->esc($wardCd) . "' " .
                "   AND out_move_date = '" . $this->esc($tagetDate) . "' " .
                "   AND data_type2 = '2' " .
                "";
    }

    /**
     * ��ư����
     */
    public function ImportNightHospitalization($tagetDate, $wardCd) {

//        $tagetDate = $this->_BasicData[0]["report_date_str"];
//        $wardCd = $this->_BasicData[0]["sfc_group_cd"];

        $sql = "" .
                $this->getNightHostpitaladmissionsSql($tagetDate, $wardCd) .
                "UNION " .
                $this->getNightTransferenceSql($tagetDate, $wardCd) .
                "UNION " .
                $this->getNightDischargeSql($tagetDate, $wardCd) .
                "UNION " .
                $this->getNightMoving_outSql($tagetDate, $wardCd) .
                "ORDER BY move_time ASC ";

//        return $this->_Model["DutyShiftImportPatientWard"]->Query($sql);
        return $this->Query($sql);
    }

    /**
     * ������ 
     */
    private function getNightHostpitaladmissionsSql($tagetDate, $wardCd) {

        return "SELECT " .
                "	'����' AS datatype_name, " .
                "	PW.department_cd AS department_cd, " .
                "	PW.in_move_time AS move_time, " .
                "	P.name_kanji AS name_kanji, " .
                "	P.birthday AS birthday, " .
                "	CASE P.sex  " .
                "		WHEN 'M' THEN '��' " .
                "		WHEN 'F' THEN '��' " .
                "		ELSE '' " .
                "	END AS sex, " .
                "	PW.disease_name AS disease_name, " .
                "	PW.attending_physician AS attending_physician " .
                "FROM " .
                "	duty_shift_import_patient_ward PW " .
                "	INNER JOIN duty_shift_import_patient P " .
                "		ON PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "	ward_cd = '" . $this->esc($wardCd) . "' " .
                "	AND in_move_date = '" . $this->esc($tagetDate) . "' " .
                "	AND in_move_time >= '" . NIGHT_HOUR_FROM . "' " .
                "	AND in_move_time <= '" . NIGHT_HOUR_TO . "' " .
                "	AND data_type1 = '1' " .
                "";
    }

    /**
     * ž����	
     */
    private function getNightTransferenceSql($tagetDate, $wardCd) {

        return"SELECT " .
                "	'ž��' AS datatype_name, " .
                "	PW.department_cd AS department_cd, " .
                "	PW.in_move_time AS move_time, " .
                "	P.name_kanji AS name_kanji, " .
                "	P.birthday AS birthday, " .
                "	CASE P.sex " .
                "		WHEN 'M' THEN '��' " .
                "		WHEN 'F' THEN '��' " .
                "		ELSE '' " .
                "	END AS sex, " .
                "	PW.disease_name AS disease_name, " .
                "	PW.attending_physician AS attending_physician " .
                "FROM " .
                "	duty_shift_import_patient_ward PW " .
                "	INNER JOIN duty_shift_import_patient P " .
                "		ON PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "	ward_cd = '" . $this->esc($wardCd) . "' " .
                "	AND in_move_date = '" . $this->esc($tagetDate) . "' " .
                "	AND in_move_time >= '" . NIGHT_HOUR_FROM . "' " .
                "	AND in_move_time <= '" . NIGHT_HOUR_TO . "' " .
                "	AND data_type1 = '2' " .
                "";
    }

    /**
     * �ౡ��	
     */
    private function getNightDischargeSql($tagetDate, $wardCd) {

        return"SELECT " .
                "	'�ౡ' AS datatype_name, " .
                "	PW.department_cd AS department_cd, " .
                "	PW.out_move_time AS move_time, " .
                "	P.name_kanji AS name_kanji, " .
                "	P.birthday AS birthday, " .
                "	CASE P.sex " .
                "		WHEN 'M' THEN '��' " .
                "		WHEN 'F' THEN '��' " .
                "		ELSE '' " .
                "	END AS sex, " .
                "	PW.disease_name AS disease_name, " .
                "	PW.attending_physician AS attending_physician " .
                "FROM " .
                "	duty_shift_import_patient_ward PW " .
                "	INNER JOIN duty_shift_import_patient P " .
                "		ON PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "	ward_cd = '" . $this->esc($wardCd) . "' " .
                "	AND out_move_date = '" . $this->esc($tagetDate) . "' " .
                "	AND out_move_time >= '" . NIGHT_HOUR_FROM . "' " .
                "	AND out_move_time <= '" . NIGHT_HOUR_TO . "' " .
                "	AND data_type2 = '5' " .
                "";
    }

    /**
     * ž�п�	
     */
    private function getNightMoving_outSql($tagetDate, $wardCd) {

        return"SELECT " .
                "	'ž��' AS datatype_name, " .
                "	PW.department_cd AS department_cd, " .
                "	PW.out_move_time AS move_time, " .
                "	P.name_kanji AS name_kanji, " .
                "	P.birthday AS birthday, " .
                "	CASE P.sex " .
                "		WHEN 'M' THEN '��' " .
                "		WHEN 'F' THEN '��' " .
                "		ELSE '' " .
                "	END AS sex, " .
                "	PW.disease_name AS disease_name, " .
                "	PW.attending_physician AS attending_physician " .
                "FROM " .
                "	duty_shift_import_patient_ward PW " .
                "	inner join duty_shift_import_patient P " .
                "		on PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "	ward_cd = '" . $this->esc($wardCd) . "' " .
                "	AND out_move_date = '" . $this->esc($tagetDate) . "' " .
                "	AND out_move_time >= '" . NIGHT_HOUR_FROM . "' " .
                "	AND out_move_time <= '" . NIGHT_HOUR_TO . "' " .
                "	AND data_type2 = '5' " .
                "";
    }

}
