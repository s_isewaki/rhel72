<?php

include_once(APP_BASE . "model/AppModel.php");

class DutyShiftReportWorkersInformation extends AppModel {

    /**
     * コンストラクタ
     * @param type $programName
     */
    function __construct($programName) {

        parent::__construct($programName);

        $this->_Validate = array(
            'shift_group' => array(
                'rule' => 'notEmpty',
                'message' => 'シフトグループ（病棟）を選択してください。'
            ),
            'data_class' => array(
                'rule' => array('maxLength', 6),
                'message' => '区分を6文字以内で入力してください。'
            ),
            'job_category' => array(
                'rule' => array('maxLength', 12),
                'message' => '職種を12文字以内で入力してください。'
            ),
            'staff_name' => array(
                'rule' => array('maxLength', 22),
                'message' => '職員氏名を22文字以内で入力してください。'
            ),
        );
    }

}
