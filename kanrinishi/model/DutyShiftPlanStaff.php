<?php

include_once(APP_BASE . "model/AppModel.php");
include_once(APP_BASE . "util/duty_shift_diary_get_emp_info.php");

class DutyShiftPlanStaff extends AppModel {

    /**
     * 勤務者情報を取得
     * （堰合さんモジュールより取得）
     * @param type $groupId
     * @param type $targetDate
     * @return type
     */
    public function GetWorkersInfo($groupId, $targetDate) {

        return get_emp_info(
                $this->_Connection, $this->_ProgramName, $groupId, $targetDate);
    }

}
