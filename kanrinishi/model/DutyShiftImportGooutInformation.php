<?php

include_once(APP_BASE . "model/AppModel.php");

class DutyShiftImportGooutInformation extends AppModel {

    /**
     * 外泊、外出
     */
    public function GetGooutCount($reportDateStr, $wardCd) {

        $sql = "SELECT " .
                "(SELECT " . //外泊
                "	COUNT(*) " .
                "FROM " .
                "	duty_shift_import_goout_information GI " .
                "WHERE " .
                "	data_type = '1' " .
                " 	AND goout_date_from <= '" . $this->esc($reportDateStr) . "' " .
                " 	AND (goout_date_to > '" . $this->esc($reportDateStr) . "' " .
                //" 	AND (goout_date_to >= '" . $this->esc($reportDateStr) . "' " .
                "	OR goout_date_to = '') " . 
                 "  AND '" . $wardCd . "' = ( ".
                "       SELECT PW.ward_cd " .
                "       FROM duty_shift_import_patient_ward PW " .
                "       WHERE PW.patient_cd = GI.patient_cd " .
                "       ORDER BY PW.in_move_date DESC, PW.in_move_time DESC limit 1) " .
                ") AS sleepover_num," .
                "(SELECT " . //外出
                "	COUNT(*) " .
                "FROM " .
                "	duty_shift_import_goout_information GI " .
                "WHERE " .
                "	data_type = '2' " .
                " 	AND goout_date_from = '" . $this->esc($reportDateStr) . "' " .
                 "  AND '" . $wardCd . "' = ( ".
                "       SELECT PW.ward_cd " .
                "       FROM duty_shift_import_patient_ward PW " .
                "       WHERE PW.patient_cd = GI.patient_cd " .
                "       ORDER BY PW.in_move_date DESC, PW.in_move_time DESC limit 1) " .
                ") AS goingout_num " .
                "";

        return $this->Query($sql);
    }

}
