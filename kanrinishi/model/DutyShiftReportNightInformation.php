<?php

include_once(APP_BASE . "model/AppModel.php");

class DutyShiftReportNightInformation extends AppModel {

    /**
     * コンストラクタ
     * @param type $programName
     */
    function __construct($programName) {

        parent::__construct($programName);

        $this->_Validate = array(
            'shift_group' => array(
                'rule' => 'notEmpty',
                'message' => 'シフトグループ（病棟）を選択してください。'
            ),
            'articles' => array(
                'rule' => array('maxLength', 255),
                'message' => '夜間の特記・管理事項を255文字以内で入力してください。'
            ),
        );
    }

}
