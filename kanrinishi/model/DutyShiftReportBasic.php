<?php

include_once(APP_BASE . "model/AppModel.php");

/**
 * 
 */
class DutyShiftReportBasic extends AppModel {

    /**
     * コンストラクタ
     * @param type $programName
     */
    function __construct($programName) {

        parent::__construct($programName);

        $this->_Validate = array(
            'shift_group' => array(
                'rule' => 'notEmpty',
                'message' => 'シフトグループ（病棟）を選択してください。'
            ),
            'day_shift_name' => array(
                'rule' => array('maxLength', 20),
                'message' => '日勤責任者名を20文字以内で入力してください。'
            ),
            'night_shift_name' => array(
                'rule' => array('maxLength', 20),
                'message' => '夜勤責任者名を20文字以内で入力してください。'
            ),
            'constant' => array(
                'rule' => array('maxLength', 2),
                'message' => '定数を2文字以内で入力してください。'
            ),
            'before_num' => array(
                'rule' => array('maxLength', 2),
                'message' => '前日数を2文字以内で入力してください。'
            ),
            'current_num' => array(
                'rule' => array('maxLength', 2),
                'message' => '現在数を2文字以内で入力してください。'
            ),
            'hostpitaladmissions_num' => array(
                'rule' => array('maxLength', 2),
                'message' => '入院を2文字以内で入力してください。'
            ),
            'transference_num' => array(
                'rule' => array('maxLength', 2),
                'message' => '転入を2文字以内で入力してください。'
            ),
            'discharge_num' => array(
                'rule' => array('maxLength', 2),
                'message' => '退院を2文字以内で入力してください。'
            ),
            'moving_out_num' => array(
                'rule' => array('maxLength', 2),
                'message' => '転出を2文字以内で入力してください。'
            ),
            'sleepover_num' => array(
                'rule' => array('maxLength', 2),
                'message' => '外泊を2文字以内で入力してください。'
            ),
            'goingout_num' => array(
                'rule' => array('maxLength', 2),
                'message' => '外出を2文字以内で入力してください。'
            ),
            'attendant_num' => array(
                'rule' => array('maxLength', 2),
                'message' => '付添を2文字以内で入力してください。'
            ),
            'course_name1' => array(
                'rule' => array('maxLength', 10),
                'message' => '診療科目名(1番目)を10文字以内で入力してください。'
            ),
            'course_num1' => array(
                'rule' => array('maxLength', 2),
                'message' => '診療科目人数(1番目)を2文字以内で入力してください。'
            ),
            'course_name2' => array(
                'rule' => array('maxLength', 10),
                'message' => '診療科目名(2番目)を10文字以内で入力してください。'
            ),
            'course_num2' => array(
                'rule' => array('maxLength', 2),
                'message' => '診療科目人数(2番目)を2文字以内で入力してください。'
            ),
            'course_name3' => array(
                'rule' => array('maxLength', 10),
                'message' => '診療科目名(3番目)を10文字以内で入力してください。'
            ),
            'course_num3' => array(
                'rule' => array('maxLength', 2),
                'message' => '診療科目人数(3番目)を2文字以内で入力してください。'
            ),
            'course_name4' => array(
                'rule' => array('maxLength', 10),
                'message' => '診療科目名(4番目)を10文字以内で入力してください。'
            ),
            'course_num4' => array(
                'rule' => array('maxLength', 2),
                'message' => '診療科目人数(4番目)を2文字以内で入力してください。'
            ),
            'course_name5' => array(
                'rule' => array('maxLength', 10),
                'message' => '診療科目名(5番目)を10文字以内で入力してください。'
            ),
            'course_num5' => array(
                'rule' => array('maxLength', 2),
                'message' => '診療科目人数(5番目)を2文字以内で入力してください。'
            ),
            'course_name6' => array(
                'rule' => array('maxLength', 10),
                'message' => '診療科目名(6番目)を10文字以内で入力してください。'
            ),
            'course_num6' => array(
                'rule' => array('maxLength', 2),
                'message' => '診療科目人数(6番目)を2文字以内で入力してください。'
            ),
            'course_name7' => array(
                'rule' => array('maxLength', 10),
                'message' => '診療科目名(7番目)を10文字以内で入力してください。'
            ),
            'course_num7' => array(
                'rule' => array('maxLength', 2),
                'message' => '診療科目人数(7番目)を2文字以内で入力してください。'
            ),
            'stretcher_num' => array(
                'rule' => array('maxLength', 2),
                'message' => '担送を2文字以内で入力してください。'
            ),
            'convoy_num' => array(
                'rule' => array('maxLength', 2),
                'message' => '護送を2文字以内で入力してください。'
            ),
            'independent_num' => array(
                'rule' => array('maxLength', 2),
                'message' => '独歩を2文字以内で入力してください。'
            ),
            'nursing_degree_a1' => array(
                'rule' => array('maxLength', 2),
                'message' => '看護度A1を2文字以内で入力してください。'
            ),
            'nursing_degree_a2' => array(
                'rule' => array('maxLength', 2),
                'message' => '看護度A2を2文字以内で入力してください。'
            ),
            'nursing_degree_a3' => array(
                'rule' => array('maxLength', 2),
                'message' => '看護度A3を2文字以内で入力してください。'
            ),
            'nursing_degree_a4' => array(
                'rule' => array('maxLength', 2),
                'message' => '看護度A4を2文字以内で入力してください。'
            ),
            'nursing_degree_b1' => array(
                'rule' => array('maxLength', 2),
                'message' => '看護度B1を2文字以内で入力してください。'
            ),
            'nursing_degree_b2' => array(
                'rule' => array('maxLength', 2),
                'message' => '看護度B2を2文字以内で入力してください。'
            ),
            'nursing_degree_b3' => array(
                'rule' => array('maxLength', 2),
                'message' => '看護度B3を2文字以内で入力してください。'
            ),
            'nursing_degree_b4' => array(
                'rule' => array('maxLength', 2),
                'message' => '看護度B4を2文字以内で入力してください。'
            ),
            'nursing_degree_c1' => array(
                'rule' => array('maxLength', 2),
                'message' => '看護度C1を2文字以内で入力してください。'
            ),
            'nursing_degree_c2' => array(
                'rule' => array('maxLength', 2),
                'message' => '看護度C2を2文字以内で入力してください。'
            ),
            'nursing_degree_c3' => array(
                'rule' => array('maxLength', 2),
                'message' => '看護度C3を2文字以内で入力してください。'
            ),
            'nursing_degree_c4' => array(
                'rule' => array('maxLength', 2),
                'message' => '看護度C4を2文字以内で入力してください。'
            ),
            'patient_status_name1' => array(
                'rule' => array('maxLength', 12),
                'message' => '患者状況名1を12文字以内で入力してください。'
            ),
            'patient_status_name2' => array(
                'rule' => array('maxLength', 12),
                'message' => '患者状況名2を12文字以内で入力してください。'
            ),
            'patient_status_name3' => array(
                'rule' => array('maxLength', 12),
                'message' => '患者状況名3を12文字以内で入力してください。'
            ),
            'patient_status_name4' => array(
                'rule' => array('maxLength', 12),
                'message' => '患者状況名4を12文字以内で入力してください。'
            ),
            'patient_status_num1' => array(
                'rule' => array('maxLength', 2),
                'message' => '患者状況数1を2文字以内で入力してください。'
            ),
            'patient_status_num2' => array(
                'rule' => array('maxLength', 2),
                'message' => '患者状況数2を2文字以内で入力してください。'
            ),
            'patient_status_num3' => array(
                'rule' => array('maxLength', 2),
                'message' => '患者状況数3を2文字以内で入力してください。'
            ),
            'patient_status_num4' => array(
                'rule' => array('maxLength', 2),
                'message' => '患者状況数4を2文字以内で入力してください。'
            ),
            'inspection_name1' => array(
                'rule' => array('maxLength', 20),
                'message' => '物品点検名1を20文字以内で入力してください。'
            ),
            'inspection_name2' => array(
                'rule' => array('maxLength', 20),
                'message' => '物品点検名2を20文字以内で入力してください。'
            ),
            'inspection_name3' => array(
                'rule' => array('maxLength', 20),
                'message' => '物品点検名3を20文字以内で入力してください。'
            ),
            'inspection_name4' => array(
                'rule' => array('maxLength', 20),
                'message' => '物品点検名4を20文字以内で入力してください。'
            ),
            'inspection_name5' => array(
                'rule' => array('maxLength', 20),
                'message' => '物品点検名5を20文字以内で入力してください。'
            ),
            'inspection_name6' => array(
                'rule' => array('maxLength', 20),
                'message' => '物品点検名6を20文字以内で入力してください。'
            ),
            'inspection_name7' => array(
                'rule' => array('maxLength', 20),
                'message' => '物品点検名7を20文字以内で入力してください。'
            ),
            'inspection_name8' => array(
                'rule' => array('maxLength', 20),
                'message' => '物品点検名8を20文字以内で入力してください。'
            ),
            'inspection_latenight1' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検深夜数1を4文字以内で入力してください。'
            ),
            'inspection_latenight2' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検深夜数2を4文字以内で入力してください。'
            ),
            'inspection_latenight3' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検深夜数3を4文字以内で入力してください。'
            ),
            'inspection_latenight4' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検深夜数4を4文字以内で入力してください。'
            ),
            'inspection_latenight5' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検深夜数5を4文字以内で入力してください。'
            ),
            'inspection_latenight6' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検深夜数6を4文字以内で入力してください。'
            ),
            'inspection_latenight7' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検深夜数7を4文字以内で入力してください。'
            ),
            'inspection_latenight8' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検深夜数8を4文字以内で入力してください。'
            ),
            'inspection_dayshift1' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検日勤数1を4文字以内で入力してください。'
            ),
            'inspection_dayshift2' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検日勤数2を4文字以内で入力してください。'
            ),
            'inspection_dayshift3' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検日勤数3を4文字以内で入力してください。'
            ),
            'inspection_dayshift4' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検日勤数4を4文字以内で入力してください。'
            ),
            'inspection_dayshift5' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検日勤数5を4文字以内で入力してください。'
            ),
            'inspection_dayshift6' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検日勤数6を4文字以内で入力してください。'
            ),
            'inspection_dayshift7' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検日勤数7を4文字以内で入力してください。'
            ),
            'inspection_dayshift8' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検日勤数8を4文字以内で入力してください。'
            ),
            'inspection_quasinight1' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検準夜数1を4文字以内で入力してください。'
            ),
            'inspection_quasinight2' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検準夜数2を4文字以内で入力してください。'
            ),
            'inspection_quasinight3' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検準夜数3を4文字以内で入力してください。'
            ),
            'inspection_quasinight4' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検準夜数4を4文字以内で入力してください。'
            ),
            'inspection_quasinight5' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検準夜数5を4文字以内で入力してください。'
            ),
            'inspection_quasinight6' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検準夜数6を4文字以内で入力してください。'
            ),
            'inspection_quasinight7' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検準夜数7を4文字以内で入力してください。'
            ),
            'inspection_quasinight8' => array(
                'rule' => array('maxLength', 4),
                'message' => '物品点検準夜数8を4文字以内で入力してください。'
            ),
        );
    }

}
