<?php

include_once(APP_BASE . "model/AppModel.php");

class DutyShiftReportNightHospitalization extends AppModel {

    /**
     * コンストラクタ
     * @param type $programName
     */
    function __construct($programName) {

        parent::__construct($programName);

        $this->_Validate = array(
            'shift_group' => array(
                'rule' => 'notEmpty',
                'message' => 'シフトグループ（病棟）を選択してください。'
            ),
            'data_class' => array(
                'rule' => array('maxLength', 6),
                'message' => '区分を6文字以内で入力してください。'
            ),
            'department_name' => array(
                'rule' => array('maxLength', 12),
                'message' => '診療科を12文字以内で入力してください。'
            ),
            'hour' => array(
                'rule' => array('maxLength', 5),
                'message' => '時間を5文字以内で入力してください。'
            ),
            'patient_name' => array(
                'rule' => array('maxLength', 22),
                'message' => '患者氏名を22文字以内で入力してください。'
            ),
            'age' => array(
                'rule' => array('maxLength', 3),
                'message' => '年齢を3文字以内で入力してください。'
            ),
            'disease_name' => array(
                'rule' => array('maxLength', 40),
                'message' => '主病名を40文字以内で入力してください。'
            ),
            'attending_physician' => array(
                'rule' => array('maxLength', 20),
                'message' => '主治医を20文字以内で入力してください。'
            ),
        );
    }

}
