<?php

include_once(APP_BASE . "model/AppModel.php");

class DutyShiftReportNightPatroltime extends AppModel {

    /**
     * コンストラクタ
     * @param type $programName
     */
    function __construct($programName) {

        parent::__construct($programName);

        $this->_Validate = array(
            'shift_group' => array(
                'rule' => 'notEmpty',
                'message' => 'シフトグループ（病棟）を選択してください。'
            ),
            'data_class' => array(
                'rule' => array('maxLength', 6),
                'message' => '区分を6文字以内で入力してください。'
            ),
            'hour' => array(
                'rule' => array('maxLength', 5),
                'message' => '時間を5文字以内で入力してください。'
            ),
            'abnormality' => array(
                'rule' => array('maxLength', 1),
                'message' => '異常を設定してください。'
            ),
            'articles' => array(
                'rule' => array('maxLength', 22),
                'message' => '内容を255文字以内で入力してください。'
            ),
        );
    }

}
