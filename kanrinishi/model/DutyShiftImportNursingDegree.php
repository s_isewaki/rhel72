<?php

include_once(APP_BASE . "model/AppModel.php");

class DutyShiftImportNursingDegree extends AppModel {

    /**
     * �߸��ʬ�̰���
     * @param type $sfcGroupCd
     * @param type $reportDateStr
     */
    public function ImportListAidClassification($sfcGroupCd, $reportDateStr) {

//        $sql = "SELECT " .
//                "	P.patient_cd AS patient_cd, " .
//                "	P.name_kanji AS patient_name, " .
//                "	ND.ward_cd AS ward_cd, " .
////                "	ND.nursing_degree, " .
//                "(SELECT department_cd " .
//                "   FROM duty_shift_import_patient_ward PW " .
//                "   WHERE PW.patient_cd = ND.patient_cd " .
//                "   ORDER BY in_move_date DESC LIMIT 1 ) AS department_cd, " .
//                "(SELECT nursing_degree FROM duty_shift_import_nursing_degree ND2 " .
//                "WHERE ND.patient_cd = ND2.patient_cd  " .
//                "AND ND2.data_type = '2' " .
//                "ORDER BY begin_date DESC LIMIT 1 ) AS nursing_degree " .
//                "FROM " .
//                "	duty_shift_import_nursing_degree ND " .
//                "	INNER JOIN duty_shift_import_patient P " .
//                "		ON ND.patient_cd = P.patient_cd " .
//                "WHERE " .
//                "	ND.data_type = '2' " .
//                "	AND ND.begin_date <= '" . $this->esc($reportDateStr) . "' " .
//                   "	AND ND.ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
//                //add START
//                "	AND EXISTS(SELECT * FROM duty_shift_import_patient_ward PW2 " .
//                "       WHERE PW2.patient_cd = P.patient_cd AND PW2.in_move_date <= '" . $this->esc($reportDateStr) . "' AND ( " .
//                //"		(PW2.out_move_date > '" . $this->esc($reportDateStr) . "' AND PW2.out_move_time > '2359') " .
//                "		(PW2.out_move_date || PW2.out_move_time > '" . $this->esc($reportDateStr) . "2359" . "') " .
//                "	OR(PW2.out_move_date = '' OR PW2.out_move_date IS NULL ))) " .
//                //add END
//                "ORDER BY  " .
//                //"	ND.nursing_degree " .
//                "	nursing_degree ASC, ND.patient_cd ASC " .
//                "";
        
           $sql = "SELECT " .
                "	PW.patient_cd AS patient_cd, " .
                "	P.name_kanji AS patient_name, " .
                "	PW.department_cd AS department_cd, " .
                "(SELECT nursing_degree FROM duty_shift_import_nursing_degree ND2 " .
                "WHERE PW.patient_cd = ND2.patient_cd  " .
                "AND ND2.data_type = '2' " .
                "AND ND2.begin_date <= '" . $this->esc($reportDateStr) . "' " .
                "ORDER BY begin_date DESC LIMIT 1 ) AS nursing_degree " .
                "FROM " .
                "	duty_shift_import_patient_ward PW " .
                "	INNER JOIN duty_shift_import_patient P " .
                "	ON PW.patient_cd = P.patient_cd " .
                "WHERE " .
                "	PW.in_move_date <= '" . $this->esc($reportDateStr) . "' AND ( " .
//                "		(PW.out_move_date > '" . $this->esc($reportDateStr) . "' AND PW.out_move_time > '235959') " .
                "		(PW.out_move_date > '" . $this->esc($reportDateStr) . "' ) " .
                "	OR (PW.out_move_date = '' OR PW.out_move_date IS NULL) " .
                "	) " .
                "	AND ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
                "ORDER BY " .
                "	nursing_degree ASC, PW.patient_cd " .
                "";
//        echo "��".$sql;
        return $this->Query($sql);
    }

//    /**
//     * �߸��ʬ
//     * ô��������������
//     * @param type $reportDataStr
//     * @return type
//     */
//    public function GetAidClassificationCount($reportDateStr, $sfcGroupCd) {
//
//        $sql = "SELECT " .
//                "	ND.nursing_degree, " .
//                "	count(*) as num " .
//                "FROM  " .
//                "	duty_shift_import_nursing_degree ND " .
//                "WHERE " .
//                "	ND.data_type = '2' " .
//                "	AND ND.begin_date <= '" . $this->esc($reportDateStr) . "' " .
//                // "AND ND.begin_date=(SELECT begin_date from duty_shift_import_nursing_degree ND2 where ".
//                // "ND2.begin_date <= '" . $this->esc($reportDateStr) . "' ORDER BY begin_date DESC LIMIT 1) " .
//                //     "	AND ND.ward_cd = '" . $this->esc($sfcGroupCd) . "' " .
//                //add START
//                "	AND EXISTS(SELECT * FROM duty_shift_import_patient_ward PW2 " .
//                "       WHERE PW2.patient_cd = ND.patient_cd AND PW2.in_move_date <= '" . $this->esc($reportDateStr) . "' AND ( " .
//                "		(PW2.out_move_date || PW2.out_move_time > '" . $this->esc($reportDateStr) . "2359" . "') " .
////                "	OR PW2.out_move_date = '' )) " .
//                "	OR(PW2.out_move_date = '' OR PW2.out_move_date IS NULL ))) " .
//                //add END
//                "GROUP BY " .
//                "	nursing_degree " .
//                // "HAVING begin_date=MAX(begin_date)".
//                //"ORDER BY  " .
//                //"	ND.patient_cd " .
//                "";
////echo $sql;
//        return $this->Query($sql);
//    }

    /**
     * �Ǹ���
     * @param type $reportDataStr
     * @return type
     */
    public function GetNursingDegreeCount($reportDataStr) {

        $sql = "SELECT " .
                "	nursing_degree, " .
                "	count(*) as num " .
                "FROM  " .
                "	duty_shift_import_nursing_degree " .
                "WHERE " .
                "	data_type = '1' " .
                "	AND begin_date <= '" . $this->esc($reportDataStr) . "' " .
                "GROUP BY " .
                "	nursing_degree " .
                "ORDER BY  " .
                "	nursing_degree " .
                "";

        return $this->Query($sql);
    }

}
