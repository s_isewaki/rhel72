<?php

include_once(APP_BASE . "model/AppModel.php");
include_once(APP_BASE . "..//duty_shift_common_class.php");

/**
 * 
 */
class DutyShiftGroup extends AppModel {

    /**
     * 
     * @param type $empId
     * @return type
     */
    public function GetShiftGroupList($empId) {

        /**
         * 堰合さんモジュールの呼び出し
         */
        $data_emp = array();
        $obj = new duty_shift_common_class($this->_Connection, $this->_ProgramName);
        $dataWktmgrp = $obj->get_wktmgrp_array();
        $groupArray = $obj->get_duty_shift_group_array("", $data_emp, $dataWktmgrp);
        return $obj->get_valid_group_array_for_diary($groupArray, $empId, "1");
    }

}
