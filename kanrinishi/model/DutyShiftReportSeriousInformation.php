<?php

include_once(APP_BASE . "model/AppModel.php");

/**
 * Description of DutyShiftReportSeriousInformation
 *
 * @author master
 */
class DutyShiftReportSeriousInformation extends AppModel {

    /**
     * コンストラクタ
     * @param type $programName
     */
    function __construct($programName) {

        parent::__construct($programName);

        $this->_Validate = array(
            'shift_group' => array(
                'rule' => 'notEmpty',
                'message' => 'シフトグループ（病棟）を選択してください。'
            ),
            'articles' => array(
                'rule' => array('maxLength', 255),
                'message' => '日勤帯の特記事項を255文字以内で入力してください。'
            ),
        );
    }

}
