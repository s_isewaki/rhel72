<?php

include_once(APP_BASE . '../about_postgres.php');

/**
 * 
 */
class AppModel {

    private $_TableName;
    protected $_Connection;
    protected $_ProgramName = "AppModel";
    protected $_Validate;
    protected $_ErrorMessages = array();

    /**
     * コンストラクタ
     * @param type $programName
     */
    function __construct($programName) {

        if ($programName != "") {
            $this->_ProgramName = $programName;
        }

        $className = get_class($this);
        $classNameArr = str_split($className, 1);
        $name = "";
        foreach ($classNameArr as $str) {
            if (ctype_upper($str)) {
                $name .= "_" . mb_strtolower($str);
            } else {
                $name .= $str;
            }
        }
        $this->_TableName = substr($name, 1, strlen($name) - 1);
        $this->_Connection = connect2db($this->_ProgramName);
    }

    /**
     * エラーメッセージ返却
     * @return type
     */
    public function GetErrorMessage() {

        return implode("\\n", $this->_ErrorMessages);
    }

    /**
     * クローズ
     */
    public function Close() {

        pg_close($this->_Connection);
    }

    /**
     * データ検査
     * 
     * notEmpty
     * maxLength
     * @param type $datas
     */
    private function validation($datas) {

        $isSuccess = true;
        if (!isset($this->_Validate)) {
            return true;
        }

        foreach ($datas as $dataKeys) {
            foreach ($dataKeys as $key => $data) {
                if (array_key_exists($key, $this->_Validate)) {
                    if (is_array($this->_Validate[$key]["rule"])) {
                        if ($this->_Validate[$key]["rule"][0] = "maxLength") {
                            if (mb_strlen($data) > $this->_Validate[$key]["rule"][1]) {
                                $this->_ErrorMessages[] = $this->_Validate[$key]["message"];
                                $isSuccess = false;
                            }
                        }
                    } else {
                        if ($this->_Validate[$key]["rule"] == "notEmpty") {
                            if ($data == "") {
                                $this->_ErrorMessages[] = $this->_Validate[$key]["message"];
                                $isSuccess = false;
                            }
                        }
                    }
                }
            }
        }

        return $isSuccess;
    }

    /**
     * 検索
     * array(
     *  o  'conditions' => array('Model.field' => $thisValue), //検索条件の配列
     *  x  'recursive' => 1, //int
     *  o  'fields' => array('Model.field1', 'DISTINCT Model.field2'), //フィールド名の配列
     *  o  'order' => array('Model.created', 'Model.field3 DESC'), //並び順を文字列または配列で指定
     *  x  'group' => array('Model.field'), //GROUP BYのフィールド
     *  x  'limit' => n, //int
     *  x  'page' => n, //int
     *  x  'offset' => n, //int
     *  x  'callbacks' => true //falseの他に'before'、'after'を指定できます
     * )
     * @param type $datas
     * @return boolean
     */
    public function Find($datas) {

        $sqlSelect = "SELECT ";
        foreach ($datas["fields"] as $key => $value) {
            $sqlSelect.= $key . " AS " . $value . ", ";
        }
//        $sql = substr($sqlSelect, 0, -1);
        $sql = rtrim($sqlSelect, ", ");

        $sql .=" FROM " . $this->_TableName . " ";

        $where = "WHERE 1 = 1 ";
        foreach ($datas["conditions"] as $key => $value) {
            if (strpos($value, "!=") === 0 ||
                    strpos($value, "<=") === 0 ||
                    strpos($value, "<") === 0 ||
                    strpos($value, ">=") === 0 ||
                    strpos($value, ">") === 0 ||
                    strpos($value, "LIKE") === 0) {
                $valueArr = explode(" ", $value);
                $where.= "AND " . $key . " " . $valueArr[0] .
                        " '" . $this->esc(trim(ltrim($value, $valueArr[0]))) . "' ";
            } else if (strpos($value, "IN(") === 0) {
                $where.= "AND " . $key . " " . $value . " ";
            } else {
                $where.= "AND " . $key . " = '" . $this->esc($value) . "' ";
            }
        }
        $cond = $where;

        if (isset($datas["order"])) {
            $order = "ORDER BY ";
            foreach ($datas["order"] as $key => $value) {
                $order.= $key . " " . $value . ", ";
            }
            $cond = $where . rtrim($order, ", ");
        }

        $res = select_from_table($this->_Connection, $sql, $cond, $this->_ProgramName);
        if ($res == 0) {
            return false;
        }

        $result = false;
        $colNames = pg_num_fields($res);
        for ($rowCount = 0; $rowCount < pg_num_rows($res); $rowCount++) {
            for ($colCount = 0; $colCount < $colNames; ++$colCount) {
                $colName = pg_field_name($res, $colCount);
                $result[$rowCount][$colName] = pg_fetch_result($res, $rowCount, $colName);
            }
        }

        return $result;
    }

    /**
     * 保存あればUpdate,なければInsert
     * array(
     * o 'conditions' => array('Model.field' => $thisValue), //検索条件の配列
     * o 'data' => array(),     //追加、更新データ
     * x 'insdata' => array(),  //追加時のみのデータ
     * x 'upddata' => array(),  //更新時のみのデータ
     * @param type $datas
     * @return boolean
     */
    public function Save($datas) {

        if (!$this->validation($datas)) {
            return false;
        }

        $sql = "UPDATE " . $this->_TableName . " SET ";
        foreach ($datas["data"] as $key => $value) {
            $sql .= $key . " = '" . $this->esc($value) . "', ";
        }
        $sql .= "regist_date = '" . date("Y/m/d H:i:s", time()) . "' ";
        $sql.=" WHERE 1 = 1 ";
        foreach ($datas["conditions"] as $key => $value) {
            $sql .= "AND " . $key . " = '" . $this->esc($value) . "' ";
        }
        $sql .= ";";

        /**
         * 条件付INSERT文
         */
        $sql .= "INSERT INTO " . $this->_TableName . " (";
        foreach ($datas["conditions"] as $key => $value) {
            $sql.=$key . ", ";
        }
        foreach ($datas["data"] as $key => $value) {
            $sql.=$key . ", ";
        }
        $sql.="regist_date) SELECT ";
        foreach ($datas["conditions"] as $key => $value) {
            $sql.="'" . $this->esc($value) . "', ";
        }
        foreach ($datas["data"] as $key => $value) {
            $sql .= "'" . $this->esc($value) . "', ";
        }
        $sql .= "'" . date("Y/m/d H:i:s", time()) . "'";
        $sql.=" WHERE NOT EXISTS(";
        $sql .= "SELECT * ";
        $sql .=" FROM " . $this->_TableName . " ";

        $sql .= "WHERE 1 = 1 ";
        foreach ($datas["conditions"] as $key => $value) {
            $sql.= "AND " . $key . " = '" . $this->esc($value) . "' ";
        }
        $sql.=");";

        if (!pg_query($this->_Connection, $sql)) {
            return false;
        }

        return true;
    }

    /**
     * 保存　追加のみ対応
     * @param type $datas
     * @return boolean
     */
    public function SaveIns($datas) {

        $sql = "INSERT INTO " . $this->_TableName . " (";
        foreach ($datas as $key => $value) {
            $sql.=$key . ", ";
        }
        $sql.="regist_date)VALUES(";

        $content = null;
        foreach ($datas as $key => $value) {
            $content[] = $this->esc($value);
        }
        $content[] = date("Y/m/d H:i:s", time());

        $ins = insert_into_table($this->_Connection, $sql, $content, $this->_ProgramName);
        if ($ins == 0) {
            return false;
        }
        return true;
    }

    /**
     * 削除
     */
    public function Delete($datas) {

        $sql = "DELETE FROM " . $this->_TableName . " ";
        $sql .= "WHERE ";
        foreach ($datas["conditions"] as $key => $value) {
            if (strpos($value, "<=") === 0 ||
                    strpos($value, "<") === 0 ||
                    strpos($value, ">=") === 0 ||
                    strpos($value, ">") === 0) {
                $valueArr = explode(" ", $value);
                $sql.= " " . $key . " " . $valueArr[0] .
                        " '" . $this->esc(trim(ltrim($value, $valueArr[0]))) . "' AND";
            } else {
                $sql.= " " . $key . " = '" . $this->esc($value) . "' AND";
            }
        }

        $sql = rtrim($sql, "AND");

        if (!pg_query($this->_Connection, $sql)) {
            return false;
        }

        return true;
    }

    /**
     * 自由クエリー
     * @param type $sql
     */
    public function Query($sql) {

        $res = pg_query($this->_Connection, $sql);
        if (!$res) {
            return false;
        }

        $result = false;
        $colCount = pg_num_fields($res);
        for ($rowCount = 0; $rowCount < pg_num_rows($res); $rowCount++) {
            for ($i = 0; $i < $colCount; ++$i) {
                $colName = pg_field_name($res, $i);
                $result[$rowCount][$colName] = pg_fetch_result($res, $rowCount, $colName);
            }
        }

        return $result;
    }

    /**
     * トランザクション開始
     */
    public function BeginTransaction() {

        pg_query($this->_Connection, "BEGIN");
    }

    /**
     * コミット
     */
    public function Commit() {

        pg_query($this->_Connection, "COMMIT");
    }

    /**
     * ロールバック
     */
    public function Rollback() {

        pg_query($this->_Connection, "ROLLBACK");
    }

    /**
     * escape
     * @param type $str
     * @return type
     */
    protected function esc($str) {

        return pg_escape_string($str);
        //return pg_escape_literal($str);
        //return str_replace("'", "''", $str);
    }

}
