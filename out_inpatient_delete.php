<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$check_auth = check_authority($session, 14, $fname);
if ($check_auth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin transaction");

// チェックされた患者をループ
if (is_array($del_pt)) {
	foreach ($del_pt as $key) {
		list($ptif_id, $inpt_in_dt, $inpt_in_tm) = split("-", $key);

		// 退院情報の取得
		$sql = "select * from inpthist";
		$cond = "where ptif_id = '$ptif_id' and inpt_in_dt = '$inpt_in_dt' and inpt_in_tm = '$inpt_in_tm'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$inpt_out_dt = pg_fetch_result($sel, 0, "inpt_out_dt");
		$inpt_out_tm = pg_fetch_result($sel, 0, "inpt_out_tm");

		// 退院情報の削除
		$sql = "delete from inpthist";
		$cond = "where ptif_id = '$ptif_id' and inpt_in_dt = '$inpt_in_dt' and inpt_in_tm = '$inpt_in_tm'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		// チェックリスト情報の削除
		$sql = "delete from inptqhist";
		$cond = "where ptif_id = '$ptif_id' and inpt_in_dt = '$inpt_in_dt' and inpt_in_tm = '$inpt_in_tm'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		// 栄養問診表情報の削除
		$sql = "delete from inptnuthist";
		$cond = "where ptif_id = '$ptif_id' and inpt_in_dt = '$inpt_in_dt' and inpt_in_tm = '$inpt_in_tm'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		// 入院状況情報の削除
		$sql = "delete from inptcond";
		$cond = "where ptif_id = '$ptif_id' and to_date(date, 'YYYYMMDD') between to_date('$inpt_in_dt', 'YYYYMMDD') and to_date('$inpt_out_dt', 'YYYYMMDD') and hist_flg = 't'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		// 移転情報の削除
		$sql = "delete from inptmove";
		$cond = "where ptif_id = '$ptif_id' and to_timestamp(move_dt || move_tm, 'YYYYMMDDHH24MI') between to_timestamp('$inpt_in_dt$inpt_in_tm', 'YYYYMMDDHH24MI') and to_timestamp('$inpt_out_dt$inpt_out_tm', 'YYYYMMDDHH24MI')";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		// 外出情報の削除
		$sql = "delete from inptgoout";
		$cond = "where ptif_id = '$ptif_id' and out_type = '1' and to_timestamp(out_date || out_time, 'YYYYMMDDHH24MI') between to_timestamp('$inpt_in_dt$inpt_in_tm', 'YYYYMMDDHH24MI') and to_timestamp('$inpt_out_dt$inpt_out_tm', 'YYYYMMDDHH24MI')";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		// 外泊情報の削除
		$sql = "delete from inptgoout";
		$cond = "where ptif_id = '$ptif_id' and out_type = '2' and to_date(out_date, 'YYYYMMDD') between to_date('$inpt_in_dt', 'YYYYMMDD') and to_date('$inpt_out_dt', 'YYYYMMDD')";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		// 担当者情報の削除
		$sql = "delete from inptophist";
		$cond = "where ptif_id = '$ptif_id' and inpt_in_dt = '$inpt_in_dt' and inpt_in_tm = '$inpt_in_tm'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
}


// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 患者一覧画面を再表示
if ($inpt_3month_flg == "t") {
	$location = "out_inpatient_3month_list.php";
} else if ($inpt_out_date_flg == "t") {
	$location = "out_inpatient_date_list.php";
} else {
	$location = "out_inpatient_list.php";
}
echo("<script language=\"javascript\">location.href = '$location?session=$session&view=$view&initial=$initial&bldgwd=$bldgwd&order=$order&page=$page&list_sect_id=$list_sect_id&list_year=$list_year&list_month=$list_month&list_day=$list_day&list_year2=$list_year2&list_month2=$list_month2&list_day2=$list_day2'</script>");
?>
