<?php
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once("hiyari_header_class.php");
// CmxSystemConfigクラス
require_once('class/Cmx/Model/SystemConfig.php');

// CmxSystemConfigインスタンス生成
$sysConf = new Cmx_SystemConfig();


$file = get_file_name_from_php_self($fname);
$smarty = new Cmx_View();

$smarty->assign("session", $session);
$smarty->assign("file", $file);

//権限ユーザ
$is_inci_auth_emp = is_inci_auth_emp($session,$fname);
$smarty->assign("is_inci_auth_emp", $is_inci_auth_emp);

//更新権限
if ($file != "hiyari_rpt_shitagaki_db.php"){
    $smarty->assign("is_update", $is_update);
}

//評価予定期日の表示の有無
$predetermined_eval = $sysConf->get('fantol.predetermined.eval');
$smarty->assign("predetermined_eval", $predetermined_eval);

//レポートDB権限
$smarty->assign("is_report_db_update_able", $is_report_db_update_able);

//一覧表示項目情報
$last_item = array();
if ($design_mode == 2){
    if (isset($disp_flag)){
        if ($disp_flag['progress_disp']=='t' && count($auth_list) > 0){
            $last_item = 'progress';
        }
        elseif($disp_flag['level_disp']=='t'){
            $last_item = 'level';
        }
        elseif($disp_flag['read_count_disp']=='t'){
            $last_item = 'read_count';
        }
        elseif($disp_flag['send_date_disp']=='t'){
            $last_item = 'send_date';
        }
        elseif($disp_flag['recv_emp_disp']=='t'){
            $last_item = 'recv_emp';
        }
        elseif($disp_flag['send_emp_disp']=='t'){
            $last_item = 'send_emp';
        }
        elseif($disp_flag['send_emp_class_disp']=='t'){
            $last_item = 'send_emp_class';
        }
        else{
            $last_item = 'subject';
        }
    }
    else{
        if (count($auth_list) == 0){
            $last_item = 'level';
        }
    }
    $smarty->assign('last_item', $last_item);
}

//ステータスのアイコン数（評価機能を使わない場合は1つ減る）
if ($file != "hiyari_rpt_shitagaki_db.php"){
    if ($file == "hiyari_rpt_recv_torey.php"){
        $status_icon_cnt = $is_evaluation_use ? 5 : 4;
    }
    else{
        $status_icon_cnt = $is_evaluation_use ? 3 : 2;
    }
    $smarty->assign('status_icon_cnt', $status_icon_cnt);
}

//進捗
$disp_progress = false;
if (($file == "hiyari_rpt_recv_torey.php" && $disp_flag['progress_disp'] == "t") ||
    ($file == "hiyari_rpt_send_torey.php" && $disp_flag['progress_disp'] == "t") ||
     $file == "hiyari_rpt_report_classification.php"){

    $smarty->assign("progress_comment_use_flg", $progress_comment_use_flg);

    $disp_progress = true;
    $smarty->assign('disp_progress', $disp_progress);
    $smarty->assign("mail_id", $mail_id);
    if ($design_mode == 1){
        $auth_short_name = array();
        $disp_auth_count = 0;
        foreach($arr_inci as $arr_key => $arr){
            if(is_usable_auth($arr_key,$fname,$con)){
                $disp_auth_count++;
                $auth_short_name[] = h($arr['auth_short_name']);
            }
        }
        $smarty->assign("auth_short_name", $auth_short_name);

        $auth_key = array();
        $auth_key_list = array_keys($arr_inci);
        for($i = 0; $i < $disp_auth_count; $i++){
            $auth_key[] = $auth_key_list[$i];
        }
        $smarty->assign("auth_key", $auth_key);
    }
    else{
        $smarty->assign("auth_list", $auth_list);
    }
}

//------------------------------
//ヘッダ用データ
//------------------------------
$header = new hiyari_header_class($session, $fname, $con);
$smarty->assign("page_title", $page_title);
$smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
$smarty->assign("inci_auth_name", $header->get_inci_auth_name());
$smarty->assign("is_kanrisha", $header->is_kanrisha());
$smarty->assign("tab_info", $header->get_user_tab_info());

//------------------------------
//選択トレイ
//------------------------------
$torey_list = array();

$i=0;
$torey_list[$i]["name"] = "受信トレイ";
$torey_list[$i]["link"] = "hiyari_rpt_recv_torey.php";
$torey_list[$i]["is_active"] = ($file == "hiyari_rpt_recv_torey.php");
if ($design_mode == 2){
    $torey_list[$i]["class"] = "sub_nav_01";
}

$i++;
$torey_list[$i]["name"] = "送信済みトレイ";
$torey_list[$i]["link"] = "hiyari_rpt_send_torey.php";
$torey_list[$i]["is_active"] = ($file == "hiyari_rpt_send_torey.php");
if ($design_mode == 2){
    $torey_list[$i]["class"] = "sub_nav_02";
}

if (is_report_db_usable($session,$fname,$con)){
    $i++;
    $torey_list[$i]["name"] = "報告ファイル";
    $torey_list[$i]["link"] = "hiyari_rpt_report_classification.php";
    $torey_list[$i]["is_active"] = ($file == 'hiyari_rpt_report_classification.php');
    if ($design_mode == 2){
        $torey_list[$i]["class"] = "sub_nav_04";
    }
}

$i++;
$torey_list[$i]["name"] = "下書きファイル";
$torey_list[$i]["link"] = "hiyari_rpt_shitagaki_db.php";
$torey_list[$i]["is_active"] = ($file == "hiyari_rpt_shitagaki_db.php");
if ($design_mode == 2){
    $torey_list[$i]["class"] = "sub_nav_03 none";
}

$smarty->assign("torey_list", $torey_list);

//------------------------------
//日付条件
//------------------------------
$smarty->assign("search_date_mode", $search_date_mode);

//報告書の対象日付指定
if ($file == 'hiyari_rpt_report_classification.php') {
    $smarty->assign("search_date_target", $search_date_target);
}

//年月指定
$smarty->assign("search_date_year", $search_date_year);
$smarty->assign("search_date_month", $search_date_month);

//開始日・終了日指定
$smarty->assign("search_date_ymd_st", $search_date_ymd_st);
$smarty->assign("search_date_ymd_ed", $search_date_ymd_ed);

//------------------------------
//亀画像のURL
//------------------------------
$smarty->assign("kame_img", get_emp_kame_image_url($session,$fname,$con));

//------------------------------
//分類フォルダ
//------------------------------
if ($file == "hiyari_rpt_report_classification.php"){
    $smarty->assign("folder_id", $folder_id);
    $smarty->assign("is_report_classification_usable", $is_report_classification_usable);
    $smarty->assign("report_link_id", $report_link_id);
    $smarty->assign("report_link_status", $report_link_status);
    $smarty->assign("folders", $folders);
    $smarty->assign("search_default_date_start", $search_default_date_start);
    $smarty->assign("search_default_date_end", $search_default_date_end);
}

//------------------------------
//実行アイコン
//------------------------------
$smarty->assign("icon_disp", $icon_disp);
if ($icon_disp['excel']){
    $smarty->assign("excel_target_report_id_list", $excel_target_report_id_list);
}

if ($file == "hiyari_rpt_shitagaki_db.php"){
    $smarty->assign("is_other_shitagaki_readable", $is_other_shitagaki_readable);
    $smarty->assign("disp_mode", $disp_mode);
}

//返信、転送、レポート編集、進捗登録、分析・再発防止、メール内容表示（件名のクリック）に渡すメールIDモード
if ($file == "hiyari_rpt_recv_torey.php"){
    $smarty->assign("mail_id_mode", "recv");
}
else if ($file == "hiyari_rpt_send_torey.php"){
    $smarty->assign("mail_id_mode", "send");
}
else{
    $smarty->assign("mail_id_mode", "");
}

//list_idがmail_idであるかどうかのフラグ
if ($file != "hiyari_rpt_recv_torey.php" && $file != "hiyari_rpt_send_torey.php"){
    $smarty->assign("list_id_is_mail_id", false);
}
else{
    $smarty->assign("list_id_is_mail_id", true);
}

//------------------------------
//検索パラメータ
//------------------------------
$smarty->assign("search_toggle", $search_toggle);

if ($file != "hiyari_rpt_shitagaki_db.php" || ($file == "hiyari_rpt_shitagaki_db.php" && $disp_mode = "all")){
    $smarty->assign("search_name", get_search_name($file));

    //報告部署------------------------------
    //全てを表示フラグ
    if ($file == 'hiyari_rpt_report_classification.php' || $file == 'hiyari_analysis_search.php'){
        $smarty->assign("search_emp_all", $search_emp_all);
    }

    //選択部署
    if (is_array($search_emp_class)) {
        if (count($search_emp_class) > 0) {
            $smarty->assign("sel_class_id", $search_emp_class[0]);
        }
    }
    else {
        if (!empty($search_emp_class)) {
            $smarty->assign("sel_class_id", $search_emp_class);
        }
    }
    if (is_array($search_emp_attribute)) {
        if (count($search_emp_attribute) > 0) {
            $smarty->assign("sel_atrb_id", $search_emp_attribute[0]);
        }
    }
    else {
        if (!empty($search_emp_attribute)) {
            $smarty->assign("sel_atrb_id", $search_emp_attribute);
        }
    }
    if (is_array($search_emp_dept)) {
        if (count($search_emp_dept) > 0) {
            $smarty->assign("sel_dept_id", $search_emp_dept[0]);
        }
    }
    else {
        if (!empty($search_emp_dept)) {
            $smarty->assign("sel_dept_id", $search_emp_dept);
        }
    }
    if (is_array($search_emp_room)) {
        if (count($search_emp_room) > 0) {
            $smarty->assign("sel_room_id", $search_emp_room[0]);
        }
    }
    else {
        if (!empty($search_emp_room)) {
            $smarty->assign("sel_room_id", $search_emp_room);
        }
    }

    //部署の選択肢
    if($file == 'hiyari_rpt_report_classification.php' || $file == 'hiyari_analysis_search.php'){
        // ログインユーザーの報告書の所属制限
        $emp_id = get_emp_id($con,$session,$fname);
        $post_layer = get_report_db_read_div($session,$fname,$con);
        $read_auth_flg = get_report_db_read_auth_flg($session,$fname,$con);
    }
    $post_select_data = get_post_select_data($con, $fname, $emp_id, $post_layer, $emp_auth, $read_auth_flg);
    $smarty->assign("post_select_data", $post_select_data);

    //氏名------------------------------
    $smarty->assign("search_emp_name", $search_emp_name);

    //職種------------------------------
    $jobs = get_job_mst($con, $fname);
    $job_list = array();
    while ($row = pg_fetch_array($jobs)){
        $job_list[$row["job_id"]] =  $row["job_nm"];
    }
    $smarty->assign("job_list", $job_list);
    $smarty->assign("sel_job_id", $search_emp_job);

    if ($file != "hiyari_rpt_shitagaki_db.php"){
        //患者情報------------------------------
        $smarty->assign("search_patient_id", $search_patient_id);
        $smarty->assign("patient_year", get_patient_year($con, $fname));
        $smarty->assign("search_patient_year_from", $search_patient_year_from);
        $smarty->assign("search_patient_year_to", $search_patient_year_to);
        $smarty->assign("search_patient_name", $search_patient_name);

        //事案番号------------------------------
        $smarty->assign("search_report_no", $search_report_no);
    }

    if ($file == 'hiyari_rpt_report_classification.php' || $file == 'hiyari_analysis_search.php'){
        //評価予定日------------------------------
        $smarty->assign("search_evaluation_date", $search_evaluation_date);
    }

    if ($file == "hiyari_rpt_recv_torey.php" || $file == "hiyari_rpt_report_classification.php" || $file == "hiyari_analysis_search.php"){
        //表題へのキーワード------------------------------
        $smarty->assign("search_title", $search_title);
        $smarty->assign("title_and_or_div", $title_and_or_div);

        //インシデントの内容へのキーワード------------------------------
        $smarty->assign("search_incident", $search_incident);
        $smarty->assign("incident_and_or_div", $incident_and_or_div);
    }

    //送信日/報告日/作成日------------------------------
    if ($file == "hiyari_rpt_recv_torey.php" || $file == "hiyari_rpt_send_torey.php") {
        $search_torey_date_in_search_area_title_name = "送信日";
    }
    if ($file == 'hiyari_rpt_report_classification.php' || $file == 'hiyari_analysis_search.php'){
        $search_torey_date_in_search_area_title_name = "報告日";
    }
    if ($file == "hiyari_rpt_shitagaki_db.php") {
        $search_torey_date_in_search_area_title_name = "作成日";
    }
    $smarty->assign("search_torey_date_in_search_area_title_name", $search_torey_date_in_search_area_title_name);
    $smarty->assign("search_torey_date_in_search_area_start", $search_torey_date_in_search_area_start);
    $smarty->assign("search_torey_date_in_search_area_end", $search_torey_date_in_search_area_end);

    //発生日------------------------------
    if ($file != "hiyari_rpt_shitagaki_db.php") {
        $smarty->assign("search_incident_date_start", $search_incident_date_start);
        $smarty->assign("search_incident_date_end", $search_incident_date_end);
    }

    //自動分類フラグ------------------------------
    if($file == "hiyari_rpt_report_classification.php") {
        $smarty->assign("auto_group_flg", $auto_group_flg);
    }
}

//------------------------------
//一覧項目表示フラグ
//------------------------------
$smarty->assign("disp_flag", $disp_flag);

//------------------------------
//一覧タイトル
//------------------------------
//日付
if ($file == 'hiyari_rpt_report_classification.php'){
    $smarty->assign("date_type", $date_type);
    $smarty->assign("date_name", $date_name);
}

//------------------------------
//一覧内容
//------------------------------
$smarty->assign("list", $list);

//------------------------------
//ソート
//------------------------------
$smarty->assign("sort_item", $sort_item);
$smarty->assign("sort_div", $sort_div);

//------------------------------
//ページング用データ
//------------------------------
$smarty->assign("page", $page);
$smarty->assign("page_max", $page_max);
$page_list = array();
$page_stt = max(1, $page - 3);
$page_end = min($page_stt + 6, $page_max);
$page_stt = max(1, $page_end - 6);
for ($i=$page_stt; $i<=$page_end; $i++){
    $page_list[] = $i;
}
$smarty->assign("page_list", $page_list);

//------------------------------
//表示
//------------------------------
if ($design_mode == 1){
    $smarty->display("hiyari_report1.tpl");
}
else{
    $smarty->display("hiyari_report2.tpl");
}

