<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | スケジュール印刷（週）</title>
<?
$fname = $PHP_SELF;

require_once("about_comedix.php");
require("time_check.ini");
require("project_check.ini");
require("show_date_navigation_week.ini");
require("holiday.php");
require("show_schedule_common.ini");
require("show_schedule_chart.ini");
require("show_calendar_memo.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// スケジュール管理権限を取得
$schd_place_auth = check_authority($session, 13, $fname);

// 日付の設定
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}
$day = date("d", $date);
$month = date("m", $date);
$year = date("Y", $date);

// 「前月」「前日」「翌日」「翌月」のタイムスタンプを取得
$last_month = get_last_month($date);
$last_week = get_last_week($date);
$next_week = get_next_week($date);
$next_month = get_next_month($date);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 院内行事チェックボックスの処理
if ($timegd == "on" || $timegd == "off") {
	time_update($con, $emp_id, $timegd, $fname);
} else if ($timegd == "") {
	$timegd = time_check($con, $emp_id, $fname);
}

// 委員会・WGチェックボックスの処理
if ($pjtgd == "on" || $pjtgd == "off") {
	project_update($con, $emp_id, $pjtgd, $fname);
} else if ($pjtgd == "") {
	$pjtgd = project_check($con, $emp_id, $fname);
}

// スケジュール機能の初期画面情報を取得
$default_view = get_default_view($con, $emp_id, $fname);

// 表示対象週のスタート日のタイムスタンプを取得
$start_date = get_start_date_timestamp($con, $date, $emp_id, $fname);

// 表示対象週の7日分の日付を配列で取得
$dates = get_dates_in_week($start_date);
$start_ymd = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $dates[0]);
$end_ymd = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $dates[count($dates) - 1]);

// 時間指定のないスケジュールの情報を配列で取得
$timeless_schedules = get_timeless_schedules($con, $emp_id, $pjtgd, $timegd, $dates, $fname);

// 時間指定のあるスケジュール情報を配列で取得
$normal_schedules = get_normal_schedules($con, $emp_id, $pjtgd, $timegd, $dates, $fname);

// ログインユーザの職員名を取得
$sql = "select emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="self.print();self.close();">
<center>
<form name="timegd">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22" bgcolor="#f6f9ff">
<td width="194"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("{$start_ymd}〜{$end_ymd}");?></font></td>
<td width="154"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($emp_nm); ?></font></td>
<td></td>
</tr>
</table>
<?
echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">\n");

$start_ymd = date("Ymd", $start_date);
$end_ymd = date("Ymd", strtotime("+6 days", $start_date));
// カレンダーのメモを取得
$arr_calendar_memo = get_calendar_memo($con, $fname, $start_ymd, $end_ymd);

// 日付行を表示
show_dates($con, $dates, $emp_id, $normal_schedules, $session, $fname, $arr_calendar_memo);

// 時間指定なし行を表示
show_timeless_schedules($timeless_schedules, $normal_schedules, $session, false, true);

// 時間指定あり行を表示
show_normal_schedules($timeless_schedules, $normal_schedules, $session, false, true);

echo("</table>\n");
?>
</form>
<? /* 凡例非表示
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width=""><? show_schd_type_color_list($con, $fname); ?></td>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
*/ ?>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// 表示対象週のスタート日のタイムスタンプを取得
function get_start_date_timestamp($con, $date, $emp_id, $fname) {
	$sql = "select calendar_start1 from option";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$calendar_start = pg_fetch_result($sel, 0, "calendar_start1");

	$start_wd = ($calendar_start == 1) ? 0 : 1;  // 0：日曜、1：月曜

	return get_start_day($date, $start_wd);
}

// 表示対象週の7日分の日付を配列で取得
function get_dates_in_week($start_date) {
	$ret = array();
	for ($i = 0; $i < 7; $i++) {
		$ret[] = date("Ymd", strtotime("+$i days", $start_date));
	}
	return $ret;
}

// 日付行を表示
function show_dates($con, $dates, $emp_id, $normal_schedules, $session, $fname, $arr_calendar_memo) {
	$today = date("Ymd");

	echo("<tr height=\"22\">\n");
	echo("<td>&nbsp;</td>\n");
	foreach ($dates as $date) {
		$colspan = get_overlap_schedule_count($normal_schedules[$date]);
		$timestamp = to_timestamp($date);
		$month = date("m", $timestamp);
		$day = date("d", $timestamp);
		$weekday_info = get_weekday_info($timestamp);
		$holiday = ktHolidayName($timestamp);
		if ($date == $today) {
			$bgcolor = "#ccffcc";
		} elseif ($holiday != "") {
			$bgcolor = "#fadede";
		} else if ($arr_calendar_memo["{$date}_type"] == "5") {
			$bgcolor = "#defafa";
		} else {
			$bgcolor = $weekday_info["color"];
		}
		$schd_type_name = get_schd_name($con, $emp_id, $emp_id, $date, $fname);

		echo("<td width=\"14%\" align=\"center\" valign=\"top\" colspan=\"$colspan\" bgcolor=\"$bgcolor\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\"><b>");
//		echo("<a href=\"schedule_menu.php?session=$session&date=$timestamp\">");
		echo("{$month}/{$day}（{$weekday_info["label"]}）");
//		echo("</a>");
		echo("</b><br>");
		// カレンダーのメモがある場合は設定する
		if ($arr_calendar_memo["$date"] != "") {
			if ($holiday == "") {
				$holiday = $arr_calendar_memo["$date"];
			} else {
				$holiday .= "<br>".$arr_calendar_memo["$date"];
			}
		}
		if ($holiday != "") {
			echo("<font color=\"red\" class=\"j12\">$holiday</font>&nbsp;");
		}
		if ($schd_type_name != "") {
			echo("<font class=\"j12\">$schd_type_name</font><br>");
		}
		echo("</font></td>\n");
	}
	echo("</tr>\n");
}

// タイムスタンプから曜日情報を取得
function get_weekday_info($date) {
	$wd = date("w", $date);
	switch ($wd) {
	case "0":
		return array("label" => "日", "color" => "#fadede");
		break;
	case "1":
		return array("label" => "月", "color" => "#fefcdf");
		break;
	case "2":
		return array("label" => "火", "color" => "#fefcdf");
		break;
	case "3":
		return array("label" => "水", "color" => "#fefcdf");
		break;
	case "4":
		return array("label" => "木", "color" => "#fefcdf");
		break;
	case "5":
		return array("label" => "金", "color" => "#fefcdf");
		break;
	case "6":
		return array("label" => "土", "color" => "#defafa");
		break;
	}
}
?>
