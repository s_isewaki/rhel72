<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="application_apply_detail.php" method="post">
<input type="hidden" name="apply_title" value="<? echo($apply_title); ?>">
<input type="hidden" name="content" value="<? echo($content); ?>">
<?
foreach ($filename as $tmp_filename) {
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}

for ($i = 1; $i <= $approve_num; $i++) {
	$varname = "regist_emp_id$i";
	echo("<input type=\"hidden\" name=\"regist_emp_id$i\" value=\"{$$varname}\">\n");
}

?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="approve_num" value="<? echo($approve_num); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="notice_emp_id" id="notice_emp_id" value="<?=$notice_emp_id?>">
<input type="hidden" name="notice_emp_nm" id="notice_emp_nm" value="<?=$notice_emp_nm?>">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">

</form>

<?
$fname=$PHP_SELF;

require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("application_workflow_common_class.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$conf = new Cmx_SystemConfig();
$title_label = ($conf->get('apply.view.title_label') == "2") ? "標題" : "表題";

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 決裁・申請権限のチェック
$checkauth = check_authority($session, 7, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (mb_strlen($apply_title) > 80) {
	echo("<script type=\"text/javascript\">alert('{$title_label}が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");

	exit;
}

// 添付ファイルの確認
if (!is_dir("apply")) {
	mkdir("apply", 0755);
}
if (!is_dir("apply/tmp")) {
	mkdir("apply/tmp", 0755);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$ext = strrchr($filename[$i], ".");

	$tmp_filename = "apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	if (!is_file($tmp_filename)) {
		echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

$obj = new application_workflow_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

//テンプレートの場合XML形式のテキスト$contentを作成
if ($wkfw_content_type == "2") {
	$ext = ".php";
	$savexmlfilename = "workflow/tmp/{$session}_x{$ext}";

	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];

	$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
	$fp = fopen($savexmlfilename, "w");
	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、更新してください。$savexmlfilename');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、更新してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	fclose($fp);

	include( $savexmlfilename );




}

// 承認状況チェック
$wkfw = get_wkfwmst($con, $wkfw_id, $fname);
$approve_label = ($wkfw["approve_label"] != "2") ? "承認" : "確認";

$apv_cnt = $obj->get_applyapv_cnt($apply_id);
if($apv_cnt > 0) {
	echo("<script language=\"javascript\">alert('他の{$approve_label}状況が変更されたため、更新できません。');</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
}

// メールを送る設定の場合、データ更新前の宛先情報を取得
if ($wkfw["send_mail_flg"] == "t") {
	$sql = "select e.emp_email2 from empmst e";
	$cond = "where exists (select * from applyapv a where a.emp_id = e.emp_id and a.apply_id = $apply_id";
	if ($wkfw["wkfw_appr"] == "2") {
		$cond .= " and a.apv_order = 1";
	}
	$cond .= ")";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$before_updating_to_addresses = array();
	while ($row = pg_fetch_array($sel)) {
		if ($row["emp_email2"] != "") {
			$before_updating_to_addresses[] = $row["emp_email2"];
		}
	}
}

// 申請更新
$obj->update_apply($apply_id, $content, $apply_title);

if (phpversion() >= '5.1') {
    //----- ワークフロー新規追加処理 Start -----
    $template_type = $_POST["kintai_template_type"];
    if (!empty($template_type)) {
        // MDB初期化
        require_once 'kintai/common/mdb_wrapper.php';
        require_once 'kintai/common/mdb.php';
        MDBWrapper::init();
        $mdb = new MDB();
        $mdb->beginTransaction();

        $class = null;
        switch ($template_type) {
            case "ovtm": // 時間外勤務命令の場合
                if (module_check(WORKFLOW_OVTM_MODULE, WORKFLOW_OVTM_CLASS)) {
                    $class = WORKFLOW_OVTM_CLASS;
                }
                break;
            case "hol": // 休暇申請の場合
                // 総合届け使用フラグ
                $general_flag = file_exists("opt/general_flag");
                if ($general_flag) {
                    if (module_check(WORKFLOW_HOL_MODULE, WORKFLOW_HOL_CLASS)) {
                        $class = WORKFLOW_HOL_CLASS;
                    }
                }
                //休暇申請
                else {
                    if (module_check(WORKFLOW_HOL2_MODULE, WORKFLOW_HOL2_CLASS)) {
                        $class = WORKFLOW_HOL2_CLASS;
                    }
                }
                break;
            case "travel": // 旅行命令
                if (module_check(WORKFLOW_TRVL_MODULE, WORKFLOW_TRVL_CLASS)) {
                    $class = WORKFLOW_TRVL_CLASS;
                }
                break;
            case "late": // 遅刻早退の場合
            	if ($_POST["late_early_type"] === "new") {
            		if (module_check(WORKFLOW_LATE2_MODULE, WORKFLOW_LATE2_CLASS)) {
            			$class = WORKFLOW_LATE2_CLASS;
            		}
            	}else{
	                if (module_check(WORKFLOW_LATE_MODULE, WORKFLOW_LATE_CLASS)) {
	                    $class = WORKFLOW_LATE_CLASS;
	                }
            	}
                break;
        }
        if (!empty($class)) {
            $module = new $class;
            $module->update_apply($apply_id);
        }
    }
}
// モジュール・クラスの存在チェック
function module_check($module, $class) {
    if (file_exists(WORKFLOW_KINTAI_DIR."/".$module)) {
        require_once WORKFLOW_KINTAI_DIR."/".$module;
        return class_exists($class);
    }
    return false;
}
//----- ワークフロー新規追加処理 End -----

// 承認更新
$arr_applyapv = $obj->get_applyapv($apply_id);
$emp_infos = array();
for($i=1; $i<=$approve_num; $i++)
{
	$varname = "regist_emp_id$i";
	$emp_id = $$varname;

	$varname = "apv_order$i";
	$apv_order = $$varname;

	$varname = "apv_sub_order$i";
	$apv_sub_order = $$varname;

	$varname = "st_div$i";
	$st_div = $$varname;

	$varname = "parent_pjt_id$i";
	$parent_pjt_id = ($$varname == "") ? null : $$varname;

	$varname = "child_pjt_id$i";
	$child_pjt_id = ($$varname == "") ? null : $$varname;

	// 所属、役職も登録する
	$infos = get_empmst($con, $emp_id, $fname);
	$emp_class     = $infos[2];
	$emp_attribute = $infos[3];
	$emp_dept      = $infos[4];
	$emp_st        = $infos[6];
	$emp_room      = $infos[33];

	if ($st_div == "0" || $st_div == "4") {
		foreach ($arr_applyapv as $applyapv) {
			if ($applyapv["apv_order"] != $apv_order) {
				continue;
			}

			if ($apv_sub_order == "") {
				if (!isset($emp_infos[$apv_order])) {
					$emp_infos[$apv_order] = $obj->get_applyapvemp($apply_id, $apv_order);
				}
				foreach ($emp_infos[$apv_order] as $emp) {
					if ($emp["emp_id"] == $emp_id) {
						$emp_class     = $emp["class_id"];
						$emp_attribute = $emp["atrb_id"];
						$emp_dept      = $emp["dept_id"];
						$emp_room      = $emp["room_id"];
						$emp_st        = $emp["st_id"];
						break 2;
					}
				}
			} else if ($applyapv["apv_sub_order"] == $apv_sub_order && $applyapv["emp_id"] == $emp_id) {
				$emp_class     = $applyapv["emp_class"];
				$emp_attribute = $applyapv["emp_attribute"];
				$emp_dept      = $applyapv["emp_dept"];
				$emp_room      = $applyapv["emp_room"];
				$emp_st        = $applyapv["emp_st"];
				break;
			}
		}
	}

	$arr = array(
                  "apply_id" => $apply_id,
                  "apv_order" => $apv_order,
                  "apv_sub_order" => $apv_sub_order,
                  "emp_id" => $emp_id,
                  "st_div" => $st_div,
                  "emp_class" => $emp_class,
                  "emp_attribute" => $emp_attribute,
                  "emp_dept" => $emp_dept,
                  "emp_st" => $emp_st,
                  "emp_room" => $emp_room,
                  "parent_pjt_id" => $parent_pjt_id,
                  "child_pjt_id" => $child_pjt_id
                 );

	$obj->update_applyapv($arr);
}


// 添付ファイル削除・登録
$obj->delete_applyfile($apply_id);
$no = 1;
foreach ($filename as $tmp_filename)
{
	$obj->regist_applyfile($apply_id, $no, $tmp_filename);
	$no++;
}


// 申請結果通知削除登録
$obj->delete_applynotice($apply_id);
if($notice_emp_id != "")
{
	$arr_notice_emp_id = split(",", $notice_emp_id);
    $arr_rslt_ntc_div  = split(",", $rslt_ntc_div);
	for($i=0; $i<count($arr_notice_emp_id); $i++)
	{
		$obj->regist_applynotice($apply_id, $arr_notice_emp_id[$i], $arr_rslt_ntc_div[$i]);
	}
}

// メールを送る設定の場合
if ($wkfw["send_mail_flg"] == "t") {

	// データ更新後の宛先情報を取得
	$sql = "select e.emp_email2 from empmst e";
	$cond = "where exists (select * from applyapv a where a.emp_id = e.emp_id and a.apply_id = $apply_id";
	if ($wkfw["wkfw_appr"] == "2") {
		$cond .= " and a.apv_order = 1";
	}
	$cond .= ")";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$after_updating_to_addresses = array();
	while ($row = pg_fetch_array($sel)) {
		if ($row["emp_email2"] != "") {
			$after_updating_to_addresses[] = $row["emp_email2"];
		}
	}

	// 増えたメールアドレスを確認
	$to_addresses = array_diff($after_updating_to_addresses, $before_updating_to_addresses);

	// メール送信に必要な情報を取得
	if (count($to_addresses) > 0) {
		$emp_id = get_emp_id($con, $session, $fname);
		$emp_detail = $obj->get_empmst_detail($emp_id);
		$emp_nm = format_emp_nm($emp_detail[0]);
		$emp_mail_header = format_emp_mail_header($emp_detail[0]);
		$emp_mail = format_emp_mail($emp_detail[0]);
		$emp_pos = format_emp_pos($emp_detail[0]);
	}
}

//----- メディアテック Start  -----
if (phpversion() >= '5.1') {
    if (!empty($template_type)) {
        // 申請日を更新
        require_once 'kintai/common/workflow_common_util.php';
        WorkflowCommonUtil::apply_date_update($con, $apply_id);
        $mdb->endTransaction();
    }
}
//----- メディアテック End  -----

// トランザクションをコミット
pg_query($con, "commit");

// メール送信
if (count($to_addresses) > 0) {
	mb_internal_encoding("EUC-JP");
	mb_language("Japanese");

	$mail_subject = "[CoMedix] {$approve_label}依頼のお知らせ";
	$mail_content = format_mail_content($con, $wkfw_content_type, $content, $fname);
	$mail_separator = str_repeat("-", 60) . "\n";
	$additional_headers = "From: $emp_mail_header";
	$additional_parameter = "-f$emp_mail";

	foreach ($to_addresses as $to_address) {
		$mail_body = "以下の{$approve_label}依頼がありました。\n\n";
		$mail_body .= "申請者：{$emp_nm}\n";
		$mail_body .= "所属：{$emp_pos}\n";
		$mail_body .= "{$title_label}：{$apply_title}\n";
		if ($mail_content != "") {
			$mail_body .= $mail_separator;
			$mail_body .= "{$mail_content}\n";
		}

		mb_send_mail($to_address, $mail_subject, $mail_body, $additional_headers, $additional_parameter);
	}
}

// データベース接続を切断
pg_close($con);

// 添付ファイルの移動
foreach (glob("apply/{$apply_id}_*.*") as $tmpfile) {
	unlink($tmpfile);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$tmp_fileno = $i + 1;
	$ext = strrchr($tmp_filename, ".");

	$tmp_filename = "apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	copy($tmp_filename, "apply/{$apply_id}_{$tmp_fileno}{$ext}");
}
foreach (glob("apply/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}


echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
echo("<script language=\"javascript\">window.close();</script>\n");

?>
</body>
<?
function get_wkfwmst($con, $wkfw_id, $fname) {
	$sql = "select * from wkfwmst";
	$cond="where wkfw_id = '$wkfw_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return pg_fetch_array($sel);
}
