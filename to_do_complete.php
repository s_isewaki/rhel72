<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
if ($pjt_id == "") {
	$checkauth = check_authority($session, 30, $fname);
} else {
	$checkauth = check_authority($session, 31, $fname);
}
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 選択されたタスクの進捗を「完了」に更新
if ($pjt_id == "") {
	$sql = "update work set";
} else {
	$sql = "update prowork set";
}
$set  = array("work_status");
$setvalue = array("1");
$cond = "where work_id = '$work_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 再表示
if ($ret_url == "") {
	echo("<script type=\"text/javascript\">location.href = 'left.php?session=$session';</script>");
} else {
	echo("<script type=\"text/javascript\">location.href = '$ret_url';</script>");
}
?>
