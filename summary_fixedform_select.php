<?
/*

画面パラメータ
	$session
		セッションID
	$caller
		呼び出し元識別子
*/
if (@$caller=="") $caller = "";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require_once("about_comedix.php");
require("label_by_profile_type.ini");
require("./get_values.ini");
require_once("summary_common.ini");
require_once("get_menu_label.ini");

//ページ名
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 組織タイプを取得
//==============================
$profile_type = get_profile_type($con, $fname);


//利用者IDを取得
$emp_id = get_emp_id($con, $session, $fname);


//==============================
// 初回表示時はログインユーザの診療科を取得
//==============================
if (!@$_REQUEST["searching"] && !@$_REQUEST["sel_sect_id"]){
	$sql =
		" select sect_id from drmst".
		" where enti_id = 1".
		" and dr_del_flg = 'f'".
		" and dr_emp_id = '".pg_escape_string($emp_id)."'".
		" limit 1";
	$sel = select_from_table($con, $sql, "", $fname);
	$_REQUEST["sel_sect_id"] = pg_fetch_result($sel, 0, 0);
	if (!$_REQUEST["sel_sect_id"]){
		$sql =
			" select sect_id from numst".
			" where enti_id = 1".
			" and nurse_del_flg = 'f'".
			" and nurse_emp_id = '".pg_escape_string($emp_id)."'".
			" limit 1";
		$sel = select_from_table($con, $sql, "", $fname);
		$_REQUEST["sel_sect_id"] = pg_fetch_result($sel, 0, 0);
	}
}


//==============================
//検索処理
//==============================
$sql =
	" select".
	" a.emp_id, a.fixedform_id, a.fixedform_sentence".
	",b.fixedform_count".
	" from fixedform a".
	" left join fixedform_count b on (a.fixedform_id = b.fixedform_id and b.emp_id = '".pg_escape_string($emp_id)."')".
	" where (a.emp_id = '".pg_escape_string($emp_id)."' or a.emp_id is null or a.emp_id = '')".
	" ". (@$_REQUEST["sel_sect_id"] ? "and ((a.emp_id is not null and a.emp_id <> '') or a.sect_id = ".(int)$_REQUEST["sel_sect_id"].")" : "").
	" order by b.fixedform_count is null, b.fixedform_count desc";
$sel = select_from_table($con,$sql,"",$fname);
$sel_num = pg_numrows($sel);


//==========================================================================================
//HTML出力
//==========================================================================================
?>
<title>CoMedix <?=get_report_menu_label($con, $fname)?>｜定型文</title>
<script src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript">

function searchStart(sect_id){
	location.href = "summary_fixedform_select.php?session=<?=$session?>&caller=<?=@$caller?>&multi_flg=<?=@$multi_flg?>&target_id=<?=@$target_id?>&searching=1&sel_sect_id="+sect_id;
}



//行が選択されたときの処理を行います。
function select_item(fixedform_id, sentence) {
	var url = 'summary_fixedform_counter.php';
	var params = $H({'session':'<?=$session?>','fixedform_id':fixedform_id}).toQueryString();
	var myAjax = new Ajax.Request(
		url,
		{
			method: 'post',
			postBody: params
			<?
			if (@$multi_flg == "") {
				echo(",onComplete: set_wm_counter_response");
			}
			?>
		});

	if(window.opener && !window.opener.closed && window.opener.call_back_fixedform_select_2) {
		window.opener.call_back_fixedform_select_2('<?=@$_REQUEST["target_id"]?>', sentence);
		return;
	}

	//呼び出し元画面に通知
	if(window.opener && !window.opener.closed && window.opener.call_back_fixedform_select)
	{
		var result = new Object();

		var caller_type = "";
		if (window.opener.document.tmplform) {
			caller_type = window.opener.document.tmplform.<?=$caller?>.type;
		}
		//operaはtinymceを未使用のためテキストエリア
		if (window.opera) {
			if (window.opener.document.touroku) {
				caller_type = window.opener.document.touroku.<?=$caller?>.type;
			}
		}

		if (caller_type == 'text') { //テキストボックスは<br>を削除
			result.sentence = sentence.replace(/<br>/g, '');
		} else if (caller_type == 'textarea') { //テキストエリアは改行コードに
			result.sentence = sentence.replace(/<br>/g, '\r\n');
		} else { // tinymce
			result.sentence = sentence;
		}
		window.opener.call_back_fixedform_select('<?=$caller?>',result);

	}


}
<?
//multi_flgが一回のみの場合、AJAX完了時の通知先関数でクローズ処理。
if (@$multi_flg == "") {
?>
function set_wm_counter_response(oXMLHttpRequest) {
	window.close();
}
<?
}
?>

function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}

//定型文の登録画面
function show_regist_window() {
	var url = "summary_fixedform_regist.php?session=<?=$session?>&kind_flg=1";
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=400,top=10,width=600,height=400";
	window.open(url, 'fixedform_regist_window',option);

}
// 更新画面
function show_update_window(fixedform_id) {
	var url = "summary_fixedform_update.php?session=<?=$session?>&kind_flg=1&fixedform_id="+fixedform_id;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=400,top=10,width=600,height=400";
	window.open(url, 'fixedform_update_window',option);

}

//削除
function deleteFixedform() {
	if (document.main_form.elements['delete_id[]'] == undefined) {
		alert('削除可能なデータが存在しません');
		return;
	}

	if (document.main_form.elements['delete_id[]'].length == undefined) {
		if (!document.main_form.elements['delete_id[]'].checked) {
			alert('チェックボックスをオンにしてください。');
			return;
		}
	} else {
		var checked = false;
		for (var i = 0, j = document.main_form.elements['delete_id[]'].length; i < j; i++) {
			if (document.main_form.elements['delete_id[]'][i].checked) {
				checked = true;
				break;
			}
		}
		if (!checked) {
			alert('チェックボックスをオンにしてください。');
			return;
		}
	}

	if (confirm("削除してよろしいですか？")) {
		document.main_form.submit();
	}
}

</script>
</head>
<body>
<div style="padding:4px">

<!-- ヘッダ -->
<?= summary_common_show_dialog_header("定型文"); ?>

<?
//==============================
//入力フィールド 出力
//==============================
?>
<form name="main_form" action="summary_fixedform_delete.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="caller" value="<?=$caller?>">
<input type="hidden" name="caller_type" value="">
<input type="hidden" name="multi_flg" value="<?=@$multi_flg?>">

<div class="dialog_searchfield" style="padding:4px">
	<div style="padding-bottom:3px;">共用定型文の分類</div>
	<? $selected_sect_style = 'text-align:center; padding:2px;background-color:#f8fdb7; border:1px solid #c5d506'; ?>
	<? $selected_sect_html = (!@$_REQUEST["sel_sect_id"] ? $selected_sect_style : ""); ?>
	<span style="white-space:nowrap; <?=$selected_sect_html?>"><a href="#" onclick="searchStart(0);" class="always_blue">すべて</a></span>&nbsp;<wbr/>&nbsp;
	<?
		$sql =
				" select sect_id, sect_nm from (".
				"   select * from (".
				"     select *, case when sect_sort_order = '' then null else sect_sort_order end as sort_order from sectmst".
				"   ) d".
				" ) d".
				" where sect_del_flg = 'f' and (sect_hidden_flg is null or sect_hidden_flg <> 1) order by sort_order, sect_id";
		$sel_sect = select_from_table($con, $sql, "", $fname);
		while($row = pg_fetch_array($sel_sect)){
			$selected_sect_html = (@$_REQUEST["sel_sect_id"] == $row["sect_id"] ? $selected_sect_style : "");
			?><span style="white-space:nowrap; <?=$selected_sect_html?>"><a href="#" onclick="searchStart(<?=$row["sect_id"]?>);" class="always_blue"><?=$row["sect_nm"]?></a></span>&nbsp;<wbr/>&nbsp;<?
		}
	?>
</div>

<div class="search_button_div">
	<input type="button" onclick="show_regist_window();" value="追加"/>
	<input type="button" onclick="deleteFixedform();" value="削除"/>
</div>



<table class="listp_table">
	<!-- 一覧HEADER START -->
	<tr>
	<th style="width:20px;"></th>
	<th style="text-align:center">内容</th>
	<th style="width:80px; text-align:center">分類</th>
	<th style="width:80px; text-align:center">更新</th>
	</tr>
	<!-- 一覧HEADER END -->
	<!-- 一覧DATA START -->
	<?
	$is_disp_over = false;
	for($i = 0; $i < $sel_num; $i++)
	{
		$fixedform_id    = pg_fetch_result($sel,$i,"fixedform_id");
		$sentence  = pg_fetch_result($sel,$i,"fixedform_sentence");
		$emp_id = pg_fetch_result($sel,$i,"emp_id");

		$sentence_js  = str_replace("'","''",$sentence);
		$sentence_js  = str_replace("\r\n","<br>",$sentence_js);

		$sentence  = h($sentence);
		$sentence  = str_replace("\r\n","<br>",$sentence);

		$line_script = "";
		$line_script .= " class=\"list_line".$i."\"";
		$line_script .= " onmouseover=\"changeCellsColor(this.className, '#fefc8f'); this.style.cursor = 'pointer';\"";
		$line_script .= " onmouseout=\"changeCellsColor(this.className, ''); this.style.cursor = '';\"";
		$onclick_script = " onclick=\"select_item($fixedform_id,'$sentence_js')\"";

		$line_script2 = "";
		$line_script2 .= " class=\"list_line".$i."\"";
		$line_script2 .= " onmouseover=\"changeCellsColor(this.className, '#fefc8f'); this.style.cursor = '';\"";
		$line_script2 .= " onmouseout=\"changeCellsColor(this.className, ''); this.style.cursor = '';\"";

	?>
	<tr>
	<td <?=$line_script2?>><input type="checkbox" name="<? echo("delete_id[]"); ?>" value="<? echo("$fixedform_id"); ?>" <? if ($emp_id =="") { echo("disabled"); } ?>></td>
	<td <?=$line_script?> <?=$onclick_script?> align="left"  ><?=$sentence?></td>
	<td <?=$line_script?> <?=$onclick_script?> align="center">
	<?
	if ($emp_id == "") {
		echo("共用");
	} else {
		echo("個人");
	}
	?>
	</td>
	<td <?=$line_script2?> align="center">
	<? if ($emp_id != "") { ?>
		<input type="button" onclick="show_update_window(<?=$fixedform_id?>);" value="更新"/>
	<? } ?>
	</td>
	</tr>
	<?
	}
	?>
	<!-- 一覧DATA END -->
</table>

</form>
<?


//==============================
//終了HTML 出力
//==============================
?>

</div>
</body>
</html>
<?

pg_close($con);

?>
