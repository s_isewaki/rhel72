<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
switch ($wherefrom) {
	case "1":  // タイムカード入力画面より
		$auth_id = 5;
		break;
	case "2":  // タイムカード修正画面より
	case "3":  // A4横画面より
		$auth_id = 42;
		break;
	case "7":  // 勤務シフト作成より 
		$auth_id = 69;
		break;
	default:
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showLoginPage(window);</script>");
		exit;
		break;
}
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// 期間をループ
for ($i = 0; $i < count($date); $i++) {

	// 処理日付の勤務実績レコード数を取得
	$tmp_date = $date[$i];
	$sql = "select date, allow_count from atdbkrslt";
	$cond = "where emp_id = '$emp_id' and date = '$tmp_date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$rec_count = pg_numrows($sel);
	$allow_count = ($rec_count > 0) ? pg_fetch_result($sel, 0, "allow_count") : "";
	
	// 勤務実績レコードがなければ作成、あれば更新
	$tmcd_group_id = $list_tmcd_group_id[$i];
	if ($tmcd_group_id == "--" || $tmcd_group_id == "") {
		$tmcd_group_id = null;
	}
	$tmp_pattern = $pattern[$i];
	if ($tmp_pattern == "--") {
		$tmp_pattern = "";
	}
	$tmp_reason = $reason[$i];
	if ($tmp_reason == "--") {
		$tmp_reason = "";
	}
    $tmp_night_duty = $night_duty[$i];
	if ($tmp_night_duty == "--") {
		$tmp_night_duty = "";
	}

	$tmp_allow_id = ($allow_ids[$i] == "--") ? NULL : $allow_ids[$i];
	//手当回数、未設定の場合は1、設定済はその数値を使用
	$tmp_allow_count = ($allow_count == "") ? 1 : $allow_count;

	if ($rec_count == 0) {
		$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, tmcd_group_id";
		if ($wherefrom != "3") {//A4横画面以外
			$sql .= ", night_duty, allow_id, allow_count";
		}
		$sql .= ") values (";
		$content = array($emp_id, $tmp_date, $tmp_pattern, $tmp_reason, $tmcd_group_id);
		if ($wherefrom != "3") {//A4横画面以外
			array_push($content, $tmp_night_duty);
			array_push($content, $tmp_allow_id);
			array_push($content, $tmp_allow_count);
		}
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	} else {
		$sql = "update atdbkrslt set";
		$set = array("pattern", "reason", "tmcd_group_id");
		$setvalue = array($tmp_pattern, $tmp_reason, $tmcd_group_id);
		if ($wherefrom != "3") {//A4横画面以外
			array_push($set, "night_duty");
			array_push($set, "allow_id");
			array_push($set, "allow_count");
			array_push($setvalue, $tmp_night_duty);
			array_push($setvalue, $tmp_allow_id);
			array_push($setvalue, $tmp_allow_count);
		}
		$cond = "where emp_id = '$emp_id' and date = '$tmp_date'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 画面を再表示
$view = ($view == "1") ? "2": "1";
switch ($wherefrom) {
	case "1":  // タイムカード入力画面より
		echo("<script type=\"text/javascript\">location.href = 'atdbk_timecard.php?session=$session&yyyymm=$yyyymm&view=$view';</script>");
		break;
	case "2":  // タイムカード修正画面より
		$url_srch_name = urlencode($srch_name);
		echo("<script type=\"text/javascript\">location.href = 'work_admin_timecard.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&view=$view&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg';</script>");
		break;
	case "3":  // A4横画面より
		$url_srch_name = urlencode($srch_name);
        echo("<script type=\"text/javascript\">location.href = 'work_admin_timecard_a4.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&view=$view&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg';</script>");
		break;
	case "7";
		$duty_yyyy = substr($yyyymm, 0, 4);
		$duty_mm = intval(substr($yyyymm, 4, 2));
		echo("<script type=\"text/javascript\">opener.location.href = 'duty_shift_results.php?session=$session&group_id=$group_id&duty_yyyy=$duty_yyyy&duty_mm=$duty_mm';</script>");
        echo("<script type=\"text/javascript\">location.href = 'work_admin_timecard.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&view=$view&wherefrom=$wherefrom&group_id=$group_id&staff_ids=$staff_ids';</script>");
		break;
}
?>
