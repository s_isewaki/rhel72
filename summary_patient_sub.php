<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");
require("label_by_profile_type.ini");
require_once("summary_common.ini");

$fname = $PHP_SELF;

$height1 = @$_REQUEST["height1"];
$height2 = @$_REQUEST["height2"];
$weight1 = @$_REQUEST["weight1"];
$weight2 = @$_REQUEST["weight2"];


$ptsubif_move = @$_REQUEST["ptsubif_move"];
$ptsubif_tool_flg = @$_REQUEST["ptsubif_tool_flg"];
$ptsubif_move6 = @$_REQUEST["ptsubif_move6"];
$ventilator = @$_REQUEST["ventilator"];
$blood_abo = @$_REQUEST["blood_abo"];
$blood_rh = @$_REQUEST["blood_rh"];
$bronchotomy = @$_REQUEST["bronchotomy"];
$experiment = @$_REQUEST["experiment"];
$dialysis_flg = @$_REQUEST["dialysis_flg"];
$ptsubif_algy_flg = @$_REQUEST["ptsubif_algy_flg"];
$ptsubif_allergy1 = @$_REQUEST["ptsubif_allergy1"];
$ptsubif_allergy2 = @$_REQUEST["ptsubif_allergy2"];
$ptsubif_allergy3 = @$_REQUEST["ptsubif_allergy3"];
$ptsubif_allergy4 = @$_REQUEST["ptsubif_allergy4"];
$ptsubif_allergy5 = @$_REQUEST["ptsubif_allergy5"];
$ptsubif_allergy6 = @$_REQUEST["ptsubif_allergy6"];
$ptsubif_allergy7 = @$_REQUEST["ptsubif_allergy7"];
$allergy_note = @$_REQUEST["allergy_note"];
$meal_note = @$_REQUEST["meal_note"];
$ptsubif_infect_flg = @$_REQUEST["ptsubif_infect_flg"];
$ptsubif_infection1 = @$_REQUEST["ptsubif_infection1"];
$ptsubif_infection2 = @$_REQUEST["ptsubif_infection2"];
$ptsubif_infection3 = @$_REQUEST["ptsubif_infection3"];
$ptsubif_infection4 = @$_REQUEST["ptsubif_infection4"];
$ptsubif_infection5 = @$_REQUEST["ptsubif_infection5"];
$ptsubif_infection6 = @$_REQUEST["ptsubif_infection6"];
$ptsubif_infection7 = @$_REQUEST["ptsubif_infection7"];
$ptsubif_infection8 = @$_REQUEST["ptsubif_infection8"];
$ptsubif_infection9 = @$_REQUEST["ptsubif_infection9"];
$ptsubif_infection10 = @$_REQUEST["ptsubif_infection10"];
$epidemic_note = @$_REQUEST["epidemic_note"];
$note1 = @$_REQUEST["note1"];
$medical_div = @$_REQUEST["medical_div"];
$adl_div = @$_REQUEST["adl_div"];


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, 57, $fname);  // メドレポート
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$checkauth = check_authority($session, 15, $fname);  // 患者参照
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 患者登録権限・入院来院履歴権限・病床管理権限を取得
$pt_reg_auth = check_authority($session, 22, $fname);

// データベースに接続
$con = connect2db($fname);

//==============================
// ログ
//==============================
require_once("summary_common.ini");
summary_common_write_operate_log("access", $con, $fname, $session, "", "", $pt_id);


// 患者情報を取得
$sel = select_from_table($con, "select * from ptifmst where ptif_id = '$pt_id' and ptif_del_flg = 'f'", "", $fname);
if ($sel == 0) {
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
$lt_nm = pg_fetch_result($sel, 0, "ptif_lt_kaj_nm");
$ft_nm = pg_fetch_result($sel, 0, "ptif_ft_kaj_nm");

// 患者詳細情報を取得
if (@$back != "t") {
	$sel = select_from_table($con, "select * from ptsubif where ptif_id = '$pt_id'", "", $fname);
	if ($sel == 0) {
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}

	if (pg_num_rows($sel) > 0) {
		list($height1, $height2) = @explode(".", pg_fetch_result($sel, 0, "ptsubif_height").".");
		if ($height2 == "") {$height2 = "0";}

		list($weight1, $weight2) = @explode(".", pg_fetch_result($sel, 0, "ptsubif_weight").".");
		if ($weight2 == "") {$weight2 = "0";}

		if (pg_fetch_result($sel, 0, "ptsubif_move_flg") == "t") {
			$ptsubif_move = "0";
		} else if (pg_fetch_result($sel, 0, "ptsubif_move1") == "t") {
			$ptsubif_move = "1";
		} else if (pg_fetch_result($sel, 0, "ptsubif_move2") == "t") {
			$ptsubif_move = "2";
		} else if (pg_fetch_result($sel, 0, "ptsubif_move3") == "t") {
			$ptsubif_move = "3";
		} else if (pg_fetch_result($sel, 0, "ptsubif_move4") == "t") {
			$ptsubif_move = "4";
		} else if (pg_fetch_result($sel, 0, "ptsubif_move5") == "t") {
			$ptsubif_move = "5";
		}

		$ptsubif_move6 = pg_fetch_result($sel, 0, "ptsubif_move6");

		$ptsubif_tool_flg = pg_fetch_result($sel, 0, "ptsubif_tool_flg");
		for ($i = 1; $i <= 7; $i++) {
			$var_name = "ptsubif_tool$i";
			$$var_name = pg_fetch_result($sel, 0, "ptsubif_tool$i");
		}

		$blood_abo = pg_fetch_result($sel, 0, "ptsubif_blood_abo");
		$blood_rh = pg_fetch_result($sel, 0, "ptsubif_blood_rh");
		$bronchotomy = pg_fetch_result($sel, 0, "ptsubif_bronchotomy");
		$ventilator = pg_fetch_result($sel, 0, "ptsubif_ventilator");
		$experiment = pg_fetch_result($sel, 0, "ptsubif_experiment_flg");
		$dialysis_flg = pg_fetch_result($sel, 0, "ptsubif_dialysis");

		$ptsubif_algy_flg = pg_fetch_result($sel, 0, "ptsubif_algy_flg");
		for ($i = 1; $i <= 7; $i++) {
			$var_name = "ptsubif_allergy$i";
			$$var_name = pg_fetch_result($sel, 0, "ptsubif_allergy$i");
		}

		$allergy_note = pg_fetch_result($sel, 0, "ptsubif_algy_com");
		$meal_note = pg_fetch_result($sel, 0, "ptsubif_meal_com");

		$ptsubif_infect_flg = pg_fetch_result($sel, 0, "ptsubif_infect_flg");
		for ($i = 1; $i <= 10; $i++) {
			$var_name = "ptsubif_infection$i";
			$$var_name = pg_fetch_result($sel, 0, "ptsubif_infection$i");
		}

		$epidemic_note = pg_fetch_result($sel, 0, "ptsubif_infection_com");
		$note1 = pg_fetch_result($sel, 0, "ptsubif_note");

		$medical_div = pg_fetch_result($sel, 0, "ptsubif_medical_div");
		$adl_div = pg_fetch_result($sel, 0, "ptsubif_adl_div");
	}
}

// 当該患者が現在入院中か否かチェック
$sql = "select count(*) from inptmst";
$cond = "where ptif_id = '$pt_id' and (inpt_in_flg = 't' or inpt_out_res_flg = 't')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
$in_bed_flg = (pg_fetch_result($sel, 0, 0) > 0);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 患者/利用者
$patient_title = $_label_by_profile["PATIENT"][$profile_type];
?>
<title>CoMedix <?
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($_label_by_profile["MED_REPORT"][$profile_type]); ?> | <? echo ($patient_title); ?>基礎データ</title>
<script type="text/javascript">
function setItemsLabel() {
	var tools = new Array();
	if (document.mainform.ptsubif_tool_flg.value == 't') tools.push('なし');
	if (document.mainform.ptsubif_tool1.value == 't') tools.push('上肢装具');
	if (document.mainform.ptsubif_tool2.value == 't') tools.push('下肢装具');
	if (document.mainform.ptsubif_tool3.value == 't') tools.push('義歯');
	if (document.mainform.ptsubif_tool4.value == 't') tools.push('義眼');
	if (document.mainform.ptsubif_tool5.value == 't') tools.push('補聴器');
	if (document.mainform.ptsubif_tool6.value == 't') tools.push('車椅子');
	if (document.mainform.ptsubif_tool7.value == 't') tools.push('歩行器');

	var algys = new Array();
	if (document.mainform.ptsubif_algy_flg.value == 't') algys.push('なし');
	if (document.mainform.ptsubif_allergy1.value == 't') algys.push('抗生物質');
	if (document.mainform.ptsubif_allergy2.value == 't') algys.push('解熱鎮静剤');
	if (document.mainform.ptsubif_allergy3.value == 't') algys.push('局所麻酔薬');
	if (document.mainform.ptsubif_allergy4.value == 't') algys.push('液製剤');
	if (document.mainform.ptsubif_allergy5.value == 't') algys.push('ワクチン');
	if (document.mainform.ptsubif_allergy6.value == 't') algys.push('造影剤');
	if (document.mainform.ptsubif_allergy7.value == 't') algys.push('その他');

	var infects = new Array();
	if (document.mainform.ptsubif_infect_flg.value == 't') infects.push('なし');
	if (document.mainform.ptsubif_infection1.value == 't') infects.push('未検査');
	if (document.mainform.ptsubif_infection2.value == 't') infects.push('梅毒');
	if (document.mainform.ptsubif_infection3.value == 't') infects.push('Ｂ型肝炎');
	if (document.mainform.ptsubif_infection4.value == 't') infects.push('Ｃ型肝炎');
	if (document.mainform.ptsubif_infection5.value == 't') infects.push('ＨＩＶ');
	if (document.mainform.ptsubif_infection6.value == 't') infects.push('ＭＲＳＡ');
	if (document.mainform.ptsubif_infection7.value == 't') infects.push('緑膿菌');
	if (document.mainform.ptsubif_infection8.value == 't') infects.push('疥癬');
	if (document.mainform.ptsubif_infection9.value == 't') infects.push('ＡＴＬ');
	if (document.mainform.ptsubif_infection10.value == 't') infects.push('その他');

	document.getElementById('tool').innerHTML = tools.join(', ');
	document.getElementById('algy').innerHTML = algys.join(', ');
	document.getElementById('infect').innerHTML = infects.join(', ');
}

function openItemSetting() {
	window.open('patient_item_update.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>', 'newwin', 'width=640,height=600,scrollbars=yes');
}
</script>
</head>
<body onload="setItemsLabel();">

<? $pager_param_addition = "&page=".@$page."&key1=".@$key1."&key2=".@$key2; ?>

<? summary_common_show_sinryo_top_header($session, $fname, "基礎データ", "", $pt_id); ?>

<? summary_common_show_report_tab_menu($con, $fname, "基礎データ", 0, $pt_id, $pager_param_addition); ?>



<table class="prop_table" cellspacing="1">
	<tr height="22">
		<th width="100px"><? echo ($patient_title); ?>ID</th>
		<td width="100px" style="text-align:center"><? echo($pt_id); ?></td>
		<th width="100px"><? echo ($patient_title); ?>氏名</th>
		<td width=""><? echo("$lt_nm $ft_nm"); ?></td>
	</tr>
</table>

<div style="width:650px; padding-top:16px">

<form name="mainform" action="inpatient_patient_sub_update_exe.php" method="post">
<table class="prop_table" style="width:100%" cellspacing="1">
	<tr>
		<th width="150px">身長</th>
		<td width="200px"><input type="text" name="height1" value="<? echo($height1); ?>" size="3" maxlength="3" style="ime-mode:inactive;">&nbsp;．<input type="text" name="height2" value="<? echo($height2); ?>" size="3" maxlength="1" style="ime-mode:inactive;">&nbsp;cm</td>
		<th width="100px">体重</th>
		<td width="200px"><input type="text" name="weight1" value="<? echo($weight1); ?>" size="3" maxlength="3" style="ime-mode:inactive;">&nbsp;．<input type="text" name="weight2" value="<? echo($weight2); ?>" size="3" maxlength="1" style="ime-mode:inactive;">&nbsp;kg</td>
	</tr>
	<tr>
		<th>救護区分・移動手段</th>
		<td>
			<select name="ptsubif_move">
				<option value="">
				<option value="0"<? if ($ptsubif_move == "0") {echo(" selected");} ?>>独歩行
				<option value="1"<? if ($ptsubif_move == "1") {echo(" selected");} ?>>杖歩行
				<option value="2"<? if ($ptsubif_move == "2") {echo(" selected");} ?>>介助歩行
				<option value="3"<? if ($ptsubif_move == "3") {echo(" selected");} ?>>車椅子（自己駆動）
				<option value="4"<? if ($ptsubif_move == "4") {echo(" selected");} ?>>車椅子（介助）
				<option value="5"<? if ($ptsubif_move == "5") {echo(" selected");} ?>>担送
			</select>
		</td>
		<th>装具・補助具</th>
		<td>
			<input type="hidden" name="ptsubif_tool_flg" value="<? echo($ptsubif_tool_flg); ?>">
			<? for ($i = 1; $i <= 7; $i++) { ?>
			<?   $var_name = "ptsubif_tool".$i; ?>
			<input type="hidden" name="ptsubif_tool<?=$i?>" value="<?=$$var_name?>">
			<? } ?>
			<table style="width:100%"><tr>
				<td><span id="tool"></span></td>
				<? if ($pt_reg_auth == 1) { ?>
				<td valign="top"><a href="javascript:void(0);" onclick="openItemSetting();"><img src="img/pencil.gif" alt="項目選択" width="13" height="13" border="0"></a></td>
				<? } ?>
			</tr></table>
		</td>
	</tr>
	<tr>
		<th>酸素必要</th>
		<td><input type="checkbox" name="ptsubif_move6" value="t"<? if ($ptsubif_move6 == "t") {echo " checked";} ?>></td>
		<th>人工呼吸器</th>
		<td><input type="checkbox" name="ventilator" value="t"<? if ($ventilator == "t") {echo " checked";} ?>></td>
	</tr>
	<tr>
		<th>血液型</th>
		<td>
			<select name="blood_abo">
				<option value="0"<? if ($blood_abo == "0") {echo " selected";} ?>>不明
				<option value="1"<? if ($blood_abo == "1") {echo " selected";} ?>>A型
				<option value="2"<? if ($blood_abo == "2") {echo " selected";} ?>>B型
				<option value="3"<? if ($blood_abo == "3") {echo " selected";} ?>>AB型
				<option value="4"<? if ($blood_abo == "4") {echo " selected";} ?>>O型
			</select>
			<select name="blood_rh">
				<option value="0"<? if ($blood_rh == "0") {echo " selected";} ?>>不明
				<option value="1"<? if ($blood_rh == "1") {echo " selected";} ?>>Rh+
				<option value="2"<? if ($blood_rh == "2") {echo " selected";} ?>>Rh-
			</select>
		</td>
		<th>気管切開</th>
		<td><input type="checkbox" name="bronchotomy" value="t"<? if ($bronchotomy == "t") {echo " checked";} ?>></td>
	</tr>
	<tr>
		<th>治験患者</th>
		<td><input type="checkbox" name="experiment" value="t"<? if ($experiment == "t") {echo " checked";} ?>></td>
		<th>透析患者</th>
		<td><input type="checkbox" name="dialysis_flg" value="t"<? if ($dialysis_flg == "t") {echo " checked";} ?>></td>
	</tr>
	<tr>
		<th>アレルギー</th>
		<td colspan="3" style="padding:0;">
			<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td>
						<span id="algy"></span>
						<input type="hidden" name="ptsubif_algy_flg" value="<? echo($ptsubif_algy_flg); ?>">
						<? for ($i = 1; $i <= 7; $i++) { ?>
						<?   $var_name = "ptsubif_allergy$i"; ?>
						<input type="hidden" name="ptsubif_allergy<?=$i?>" value="<?=$$var_name?>">
						<? } ?>
					</td>
					<? if ($pt_reg_auth == 1) { ?>
					<td valign="top"><a href="javascript:void(0);" onclick="openItemSetting();"><img src="img/pencil.gif" alt="項目選択" width="13" height="13" border="0"></a></td>
					<? } ?>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<th>アレルギーコメント</th>
		<td colspan="3"><textarea cols="50" rows="5" name="allergy_note" style="ime-mode:active;"><? echo($allergy_note); ?></textarea></td>
	</tr>
<!--
	<tr>
		<th>食事コメント</th>
		<td colspan="3"><textarea cols="50" rows="5" name="meal_note" style="ime-mode:active;"><? echo($meal_note); ?></textarea></td>
	</tr>
-->
	<tr>
		<th>感染症</th>
		<td colspan="3" style="padding:0;">
			<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td>
					<span id="infect"></span>
					<input type="hidden" name="ptsubif_infect_flg" value="<? echo($ptsubif_infect_flg); ?>">
					<? for ($i = 1; $i <= 10; $i++) { ?>
					<?   $var_name = "ptsubif_infection$i"; ?>
					<input type="hidden" name="ptsubif_infection<?=$i?>" value="<?=$$var_name?>">
					<? } ?>
					</td>
					<? if ($pt_reg_auth == 1) { ?>
					<td valign="top"><a href="javascript:void(0);" onclick="openItemSetting();"><img src="img/pencil.gif" alt="項目選択" width="13" height="13" border="0"></a></td>
					<? } ?>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<th>感染症コメント</th>
		<td colspan="3"><textarea cols="50" rows="5" name="epidemic_note" style="ime-mode:active;"><? echo($epidemic_note); ?></textarea></td>
	</tr>
	<tr>
		<th>特記事項</th>
		<td colspan="3"><textarea rows="5" cols="50" name="note1" style="ime-mode:active;"><? echo($note1); ?></textarea></td>
	</tr>
	<tr>
		<th>医療区分</th>
		<td>
			<select name="medical_div">
				<option value="">
				<option value="1"<? if ($medical_div == "1") {echo(" selected");} ?>>1
				<option value="2"<? if ($medical_div == "2") {echo(" selected");} ?>>2
				<option value="3"<? if ($medical_div == "3") {echo(" selected");} ?>>3
			</select>
		</td>
		<th>ADL区分</th>
		<td>
			<select name="adl_div">
				<option value="">
				<option value="1"<? if ($adl_div == "1") {echo(" selected");} ?>>1
				<option value="2"<? if ($adl_div == "2") {echo(" selected");} ?>>2
				<option value="3"<? if ($adl_div == "3") {echo(" selected");} ?>>3
			</select>
		</td>
	</tr>
</table>


<? if ($pt_reg_auth == 1) { ?>
<div class="search_button_div">
	<input type="button" onclick="document.mainform.submit();" value="更新"/>
</div>
<? } ?>

</div>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="path" value="M">
</form>

</body>
<? pg_close($con); ?>
</html>
