<{*************************************************************************************************
報告書内容のデータ列
*************************************************************************************************}>
<{*<!--
---------------------------------------------------------------------------------------------------
発見・発生日時
---------------------------------------------------------------------------------------------------
-->*}>
<{if ($i==105 || $i==100) && $j==5}>
    <input type="text" id="id_<{$i}>_5" class="size06 calendar_text" name="_<{$i}>_5" value="<{$vals[$i][5][0]}>" readonly>
    <input type="button" class="button size04 mL10 radius_mini" value="今 日" onclick="set_<{$i}>_5_now()"　/>
    <a id="id_<{$i}>_5_btn" class="calendar_btn" href="javascript:void(0);">カレンダー</a>

    <input type="hidden" id="id_<{$i}>_10" name="_<{$i}>_10" value="<{$vals[$i][10][0]}>">
    <input type="hidden" id="id_<{$i}>_20" name="_<{$i}>_20" value="<{$vals[$i][20][0]}>">
    <input type="hidden" id="id_<{$i}>_30" name="_<{$i}>_30" value="<{$vals[$i][30][0]}>">

    <{*<!--タイムラグ区分-->*}>
    <{if $i==105}>
        <input type="hidden" id="id_105_70" name="_105_70" value="<{$vals[105][70][0]}>">
    <{/if}>

    <script type="text/javascript">
        function set_<{$i}>_5_now() {
            $("#id_<{$i}>_5").val('<{$this_ymd}>');
            $("#id_<{$i}>_10").val('<{$this_month}>');
            $("#id_<{$i}>_20").val('<{$day_week}>');
            $("#id_<{$i}>_30").val('<{$week_div}>');

            <{*<!--事例の詳細(時系列)があれば、一緒にセットする（1行目のみ & 日付が未設定の場合）-->*}>
            if (document.getElementById("id_4000_1")) {
                if (!document.getElementById("id_4000_1").value) {
                    $("#id_4000_1").val('<{$this_ymd}>');
                }
            }
        }
    </script>

<{*<!--
---------------------------------------------------------------------------------------------------
患者の数
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $i == 200 && $j == 10}>
    <ul>
        <{foreach from=$grps[$i].easy_item_list[$j].easy_list key=k item=data}>
            <{if $item_element_no_disp[$i][$j] == "" || !in_array($data.easy_code,$item_element_no_disp[$i][$j])}>
                <li class="item_set<{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>">
                    <label for="id_<{$i}>_<{$j}>_<{$k}>">
                        <input type='radio'
                          id="id_<{$i}>_<{$j}>_<{$k}>"
                          class="radio"
                          name="_<{$i}>_<{$j}>[]"
                          value="<{$data.easy_code}>"
                          onclick="radio_onclick(this)"
                          <{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>
                          >
                        <{$data.easy_name|escape:"html"}>
                    </label>

                    <{*<!--関連する患者情報の数-->*}>
                    <{if $data.easy_code==2}>
                        <{if array_intersect(array(310,320,330,340,350,360,370,380,390), $grp_flags)}>
                            &nbsp;&nbsp;：&nbsp;関連する患者数
                            <select id="id_200_30" name="_200_30" onchange="selectbox_onchange(this);">
                                <option value=""></option>
                                <{foreach from=$grps[200].easy_item_list[30].easy_list item=list}>
                                    <{if $item_element_no_disp[200][30] == "" || !in_array($list.easy_code,$item_element_no_disp[200][30])}>
                                        <option value="<{$list.easy_code}>" <{if in_array($list.easy_code, (array)@$vals[200][30])}>selected<{/if}>>
                                            <{$list.easy_name|escape:"html"}>
                                        </option>
                                    <{/if}>
                                <{/foreach}>
                            </select>
                        <{/if}>
                    <{/if}>

                    <{*<!--その他の入力欄-->*}>
                    <{if isset($data.other_code)}>
                        &nbsp;&nbsp;&nbsp;&nbsp;備考（<input type="text" id="id_<{$i}>_<{$data.other_code}>" class="text" name="_<{$i}>_<{$data.other_code}>" value="<{$vals[$i][$data.other_code][0]}>">）
                    <{/if}>

                </li>
            <{/if}>
        <{/foreach}>
    </ul>
    <a class="clear_btn cb_type01" href="javascript:void(0);" onclick="clear_item('radio','200','10')">クリア</a>

<{*<!--
---------------------------------------------------------------------------------------------------
アセスメント・患者の状態
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $i == 140 && $j == 10}>
    <{assign var="colnum" value=4}>
    <{assign var="td_width" value="25%"}>
    <table class="tabletype05">
    <{foreach from=$grps[140].easy_item_list[10].easy_list item=e140_item}>
        <{assign var="e140_code" value=$e140_item.easy_code}>
        <{assign var="v140" value=$vals[140][10]}>
        <{if in_array($e140_code, $asses_use)}>
            <tr>
                <td>
                    <dl class="clearfix<{if in_array($e140_code,(array)@$v140)}> checked<{/if}>">
                        <{*<!--メインチェックボックス-->*}>
                        <dt class="item_set">
                            <label for="140_10_<{$e140_code}>">
                                <span class="checkbox_wrap<{if in_array($e140_code,(array)@$v140)}> checked<{/if}>">
                                    <input type='checkbox'
                                      id="140_10_<{$e140_code}>"
                                      class="140_10 checkbox"
                                      name="_140_10[]"
                                      value="<{$e140_code}>"
                                      <{if in_array($e140_code,(array)@$v140)}>checked<{/if}>
                                    >
                                </span>
                                <{$e140_item.easy_name|escape:"html"}>：
                            </label>
                        </dt>

                        <{assign var="e150" value=$grps[150].easy_item_list[$e140_code]}>
                        <{assign var="v150" value=$vals[150][$e140_code]}>

                        <{*<!--詳細-->*}>
                        <dd>
                            <{*<!--selectの場合-->*}>
                            <{if $e150.easy_item_type == 'select'}>
                                <select name="_150_<{$e140_code}>" id="id_150_<{$e140_code}>_0" class="150_<{$e140_code}>"
                                    <{if !in_array($e140_code, (array)@$vals[140][10])}>disabled<{/if}>
                                >
                                    <option value=""></option>
                                        <{foreach from=$e150.easy_list item=e150_list}>
                                            <{if $item_element_no_disp[150][$e140_code] == "" || !in_array($e150_list.easy_code,$item_element_no_disp[150][$e140_code])}>
                                                <option value="<{$e150_list.easy_code}>"
                                                    <{if in_array($e150_list.easy_code, (array)@$vals[150][$e140_code])}>selected<{/if}>
                                                >
                                                <{$e150_list.easy_name|escape:"html"}>
                                            <{/if}>
                                        <{/foreach}>
                                </select>

                            <{*<!--radioの場合-->*}>
                            <{else}>
                                <{foreach from=$e150.easy_list item=e150_item}>
                                    <{assign var="e150_code" value=$e150_item.easy_code}>
                                    <{if $item_element_no_disp[150][$e140_code] == "" || !in_array($e150_code, $item_element_no_disp[150][$e140_code])}>
                                        <div class="item_set" style="margin-right: 3.5em; background-color: transparent !important;">
                                            <label for="id_150_<{$e140_code}>_<{$e150_code}>" >
                                                <input type="radio"
                                                    id="id_150_<{$e140_code}>_<{$e150_code}>"
                                                    class="150_<{$e140_code}>"
                                                    name="_150_<{$e140_code}>[]"
                                                    value="<{$e150_code}>"
                                                    <{if in_array($e150_code, (array)@$v150)}>checked<{/if}>
                                                    <{if !in_array($e140_code,(array)@$v140)}>disabled<{/if}>
                                                >
                                                <{$e150_item.easy_name|escape:"html"}>
                                            </label>
                                        </div>
                                    <{/if}>
                                <{/foreach}>
                            <{/if}>
                        </dd>
                    </dl>

                    <{*<!--以下、詳細の詳細-->*}>
                    <{if $e140_code == 6 || $e140_code == 7 || $e140_code == 8 || $e140_code == 20}>
                        <{*<!--リスク回避器具：有りの詳細 -->*}>
                        <{if $e140_code == 6}>
                            <{assign var="grp_code" value=170}>
                        <{*<!--拘束用具：有りの詳細-->*}>
                        <{elseif $e140_code == 7}>
                            <{assign var="grp_code" value=180}>
                        <{*<!--身体拘束行為：有りの詳細-->*}>
                        <{elseif $e140_code == 8}>
                            <{assign var="grp_code" value=190}>
                        <{*<!--ルート関連（自己抜去）：ミトンありの詳細-->*}>
                        <{elseif $e140_code == 20}>
                            <{assign var="grp_code" value=160}>
                        <{/if}>

                        <table class="box wrap_height disable">
                            <{assign var="cnt" value=$colnum}>

                            <{foreach from=$selection[$grp_code][10].list item=e_item}>
                                <{*<!--改行-->*}>
                                <{if $cnt == $colnum || $e_item.easy_code == 99}>
                                    <{*<!--改行前に空セルを必要な分だけ作成-->*}>
                                    <{if $cnt < $colnum}>
                                        <{section start=$cnt loop=$colnum name=n}>
                                            <td width="<{$td_width}>" <{if $smarty.section.n.loop == $colnum}>class="lineR"<{/if}>>
                                                <span></span>
                                            </td>
                                        <{/section}>
                                    <{/if}>

                                    </tr>
                                    <tr>

                                    <{if $e_item.easy_code == 99}>
                                        <{assign var="cnt" value=$colnum}>
                                    <{else}>
                                        <{assign var="cnt" value=1}>
                                    <{/if}>
                                <{else}>
                                    <{assign var="cnt" value=$cnt+1}>
                                <{/if}>

                                <{*<!--セル-->*}>
                                <td width="<{$td_width}>" <{if $e_item.easy_code == 99}>colspan="<{$colnum}>"<{/if}> class="<{if $cnt == $colnum}>lineR<{/if}>">
                                    <{*<!--リスク回避器具：有りの詳細 -->*}>
                                    <{if $e140_code == 6}>
                                        <span class="item_set<{if in_array($e_item.easy_code, (array)@$vals[170][10]) && in_array($e140_code,(array)@$v140)}> checked<{/if}>">
                                            <label for="150_6_10_<{$e_item.easy_code}>" >
                                                <span class="checkbox_wrap<{if in_array($e_item.easy_code, (array)@$vals[170][10]) && in_array($e140_code,(array)@$v140)}> checked<{/if}><{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10"}> disabled<{/if}>">
                                                    <input type='checkbox'
                                                        id="150_6_10_<{$e_item.easy_code}>"
                                                        class="150_6_10 checkbox"
                                                        name="_170_10[]"
                                                        value="<{$e_item.easy_code}>"
                                                        <{if in_array($e_item.easy_code, (array)@$vals[170][10]) && in_array($e140_code,(array)@$v140)}>checked<{/if}>
                                                        <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10"}>disabled<{/if}>
                                                    >
                                                </span>
                                                <{$e_item.easy_name|escape:"html"}>
                                            </label>
                                            <{if $e_item.easy_code == 99}>
                                                <input type="text" class="150_6_20" id="150_6_20" name="_170_20" value="<{if in_array($e_item.easy_code, (array)@$vals[170][10]) && in_array($e140_code,(array)@$v140)}><{$vals[170][20][0]}><{/if}>" <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10" || !in_array(99,(array)@$vals[170][10])}>disabled<{/if}> />
                                            <{/if}>
                                        </span>

                                    <{*<!--拘束用具：有りの詳細-->*}>
                                    <{elseif $e140_code == 7}>
                                        <span class="item_set<{if in_array($e_item.easy_code, (array)@$vals[180][10]) && in_array($e140_code,(array)@$v140)}> checked<{/if}>">
                                            <label for="150_7_10_<{$e_item.easy_code}>" >
                                                <span class="checkbox_wrap<{if in_array($e_item.easy_code, (array)@$vals[180][10]) && in_array($e140_code,(array)@$v140)}> checked<{/if}><{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10"}> disabled<{/if}>">
                                                    <input type="checkbox"
                                                        id="150_7_10_<{$e_item.easy_code}>"
                                                        class="150_7_10 checkbox"
                                                        name="_180_10[]"
                                                        value="<{$e_item.easy_code}>"
                                                        <{if in_array($e_item.easy_code, (array)@$vals[180][10]) && in_array($e140_code,(array)@$v140)}>checked<{/if}>
                                                        <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10"}>disabled<{/if}>
                                                    >
                                                </span>
                                                <{$e_item.easy_name|escape:"html"}>
                                            </label>
                                            <{if $e_item.easy_code == 99}>
                                                <input type="text" class="150_7_20" id="150_7_20" name="_180_20" value="<{if in_array($e_item.easy_code, (array)@$vals[180][10]) && in_array($e140_code,(array)@$v140)}><{$vals[180][20][0]}><{/if}>" <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10" || !in_array(99,(array)@$vals[180][10])}>disabled<{/if}> />
                                            <{/if}>
                                        </span>

                                    <{*<!--身体拘束行為：有りの詳細-->*}>
                                    <{elseif $e140_code == 8}>
                                        <span class="item_set<{if in_array($e_item.easy_code, (array)@$vals[190][10]) && in_array($e140_code,(array)@$v140)}> checked<{/if}>">
                                            <label for="150_8_10_<{$e_item.easy_code}>" >
                                                <span class="checkbox_wrap<{if in_array($e_item.easy_code, (array)@$vals[190][10]) && in_array($e140_code,(array)@$v140)}> checked<{/if}><{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10"}> disabled<{/if}>">
                                                    <input type='checkbox'
                                                        id="150_8_10_<{$e_item.easy_code}>"
                                                        class="150_8_10 checkbox"
                                                        name="_190_10[]"
                                                        value="<{$e_item.easy_code}>"
                                                        <{if in_array($e_item.easy_code, (array)@$vals[190][10]) && in_array($e140_code,(array)@$v140)}>checked<{/if}>
                                                        <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10"}>disabled<{/if}>
                                                    >
                                                </span>
                                                <{$e_item.easy_name|escape:"html"}>
                                            </label>
                                            <{if $e_item.easy_code == 99}>
                                                <input type="text" class="150_8_20" id="150_8_20" name="_190_20" value="<{if in_array($e_item.easy_code, (array)@$vals[190][10]) && in_array($e140_code,(array)@$v140)}><{$vals[190][20][0]}><{/if}>" <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10" || !in_array(99,(array)@$vals[190][10])}>disabled<{/if}>    />
                                            <{/if}>
                                        </span>

                                    <{*<!--ルート関連（自己抜去）：ミトンありの詳細-->*}>
                                    <{elseif $e140_code == 20}>
                                        <span class="item_set<{if in_array($e_item.easy_code, (array)@$vals[160][10])}> checked<{/if}>">
                                            <label for="150_20_10_<{$e_item.easy_code}>" >
                                                <span class="checkbox_wrap<{if in_array($e_item.easy_code, (array)@$vals[160][10])}> checked<{/if}><{if !in_array($e140_code,(array)@$v140) || $v150[0] != "20"}> disabled<{/if}>">
                                                    <input type='checkbox'
                                                        id="150_20_10_<{$e_item.easy_code}>"
                                                        class="150_20_10 checkbox"
                                                        name="_160_10[]"
                                                        value="<{$e_item.easy_code}>"
                                                        <{if in_array($e_item.easy_code, (array)@$vals[160][10])}>checked<{/if}>
                                                        <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "20"}>disabled<{/if}>
                                                    >
                                                </span>
                                                <{$e_item.easy_name|escape:"html"}>
                                            </label>
                                        </span>
                                    <{/if}>
                                </td>
                            <{/foreach}>

                            <{*<!--空セル-->*}>
                            <{if $cnt < $colnum}>
                                <{section start=$cnt loop=$colnum name=n}>
                                    <td width="<{$td_width}>" class="<{if $smarty.section.n.loop == $colnum}>lineR<{/if}>">
                                        <span></span>
                                    </td>
                                <{/section}>
                            <{/if}>

                            </tr>
                        </table>
                    <{/if}>
                </td>
            </tr>
        <{/if}>
    <{/foreach}>
    </table>

<{*<!--
---------------------------------------------------------------------------------------------------
影響への影響・患者への精神的影響・対応
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif ($i==1400 && $j==30) || ($i==1400 && $j==60) || ($i==1410 && $j==20)}>
    <{assign var="colnum" value=$selection[$i][$j].col_num}>
    <{assign var="td_width" value=$selection[$i][$j].item_width}>
    <table class="box">
        <{assign var="cnt" value=$colnum}>
        <{foreach from=$ic key=k item=data}>
            <{if $item_element_no_disp[$i][$j] == "" || !in_array($data.item_id,$item_element_no_disp[$i][$j])}>
                <{*<!--改行-->*}>
                <{if $cnt == $colnum || isset($data.other_code)}>
                    <{*<!--改行前に空セルを必要な分だけ作成-->*}>
                    <{if $cnt < $colnum}>
                        <{section start=$cnt loop=$colnum name=n}>
                            <td width="<{$td_width}>" <{if $smarty.section.n.loop == $colnum}>class="lineR"<{/if}>><span></span></td>
                        <{/section}>
                    <{/if}>

                    </tr>
                    <tr>

                    <{if isset($data.other_code)}>
                        <{assign var="cnt" value=$colnum}>
                    <{else}>
                        <{assign var="cnt" value=1}>
                    <{/if}>
                <{else}>
                    <{assign var="cnt" value=$cnt+1}>
                <{/if}>

                <{*<!--セル-->*}>
                <td width="<{$td_width}>" class="<{if $cnt == $colnum}>lineR<{/if}>" <{if isset($data.other_code)}>colspan="<{$colnum}>"<{/if}>>
                    <span class="item_set<{if in_array($data.item_id, (array)@$vals[$i][$j])}> checked<{/if}>">
                        <label for="id_<{$i}>_<{$j}>_<{$key}>_<{$k}>" >
                            <span class="checkbox_wrap<{if in_array($data.item_id, (array)@$vals[$i][$j])}> checked<{/if}>">
                                <input type='checkbox'
                                    id="id_<{$i}>_<{$j}>_<{$key}>_<{$k}>"
                                    class="checkbox"
                                    name="_<{$i}>_<{$j}>[]"
                                    value="<{$data.item_id}>"
                                    onclick="checkbox_onclick(this)"
                                    <{if in_array($data.item_id, (array)@$vals[$i][$j])}> checked<{/if}>
                                >
                            </span>
                            <{$data.item_name|escape:"html"}>
                        </label>

                        <{*<!--その他の入力欄-->*}>
                        <{if isset($data.other_code)}>
                            <span id="id_<{$i}>_<{$data.other_code}>_<{$key}>" class="other">
                                <{*<!--影響/対応区分毎に表示エリアが存在するが、other_codeは影響/対応に各1つのみ。複数の表示エリアに出力するとnameが重複してしまうため、区分が選択されている場合のみ出力-->*}>
                                <{if in_array($key, (array)@$vals[$i][10])}>
                                    (<input type="text" id="id_<{$i}>_<{$data.other_code}>" class="text" name="_<{$i}>_<{$data.other_code}>" value="<{$vals[$i][$data.other_code][0]}>">)
                                <{/if}>
                            </span>
                        <{/if}>
                    </span>
                </td>
            <{/if}>
        <{/foreach}>

        <{*<!--空セル-->*}>
        <{if $cnt < $colnum}>
            <{section start=$cnt loop=$colnum name=n}>
                <td width="<{$td_width}>" class="<{if $smarty.section.n.loop == $colnum}>lineR<{/if}>"><span></span></td>
            <{/section}>
        <{/if}>

        </tr>
    </table>
<{*<!--
---------------------------------------------------------------------------------------------------
対応詳細
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $i==1410 && $j==30}>
    <ul>
        <{foreach from=$ic key=k item=data}>
            <{if $item_element_no_disp[$i][$j] == "" || !in_array($data.item_id,$item_element_no_disp[$i][$j])}>
                <li class="item_set<{if in_array($data.sub_item_id, (array)@$vals[$i][$j])}> checked<{/if}>">
                    <label for="id_<{$i}>_<{$j}>_<{$key}>_<{$k}>" >
                        <span class="checkbox_wrap<{if in_array($data.sub_item_id, (array)@$vals[$i][$j])}> checked<{/if}>">
                            <input type='checkbox'
                                id="id_<{$i}>_<{$j}>_<{$key}>_<{$k}>"
                                class="checkbox"
                                name="_<{$i}>_<{$j}>[]"
                                value="<{$data.sub_item_id}>"
                                onclick="checkbox_onclick(this)"
                                <{if in_array($data.sub_item_id, (array)@$vals[$i][$j])}> checked<{/if}>
                            >
                        </span>
                        <{$data.sub_item_name|escape:"html"}>
                    </label>
                </li>
            <{/if}>
        <{/foreach}>
    </ul>

<{*<!--
---------------------------------------------------------------------------------------------------
患者影響レベル
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $i == 90 && $j == 10}>
    <ul id="level_info">
        <{foreach from=$level_infos key=k item=data}>
        	<{if empty($level_use) || in_array($data.easy_code, $level_use)}>
	            <{if $data.use_flg}>
	                <li <{if in_array($data.easy_code, (array)@$vals[$i][$j])}>class="checked"<{/if}>>
	                    <label class="item_set" for="id_<{$i}>_<{$j}>_<{$k}>">
	                        <input type='radio'
	                            id="id_<{$i}>_<{$j}>_<{$k}>"
	                            class="radio"
	                            name="_<{$i}>_<{$j}>[]"
	                            value="<{$data.easy_code}>"
	                            <{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>
	                        >
	                        <{$data.easy_name|escape:"html"}>&nbsp;：<{$data.message|escape:"html"}>
	                    </label>
	                </li>
	            <{/if}>
            <{/if}>
        <{/foreach}>
    </ul>
    <br><br>
    <a class="clear_btn cb_type01" href="javascript:void(0);" onclick="clear_item('radio','<{$i}>','<{$j}>')">
        クリア
    </a>

<{*<!--
---------------------------------------------------------------------------------------------------
評価予定日
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $i == 620 && $j == 33}>
    <input type="text" id="id_620_33" name="_620_33" class="size06 calendar_text" value="<{$vals[620][33][0]}>" readonly>
    <a id="id_620_33_btn" class="calendar_btn" href="javascript:void(0);" />カレンダー</a>
    <a class="mT3 clear_btn cb_type02" href="javascript:void(0);" onclick="clear_item('text','<{$i}>','<{$j}>')">クリア</a>

<{*<!--
---------------------------------------------------------------------------------------------------
テキストボックス
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'text'}>
    <input type="text" id="id_<{$i}>_<{$j}>"
        class="<{if !isset($txt_class) || $txt_class==""}>text size01<{else}><{$txt_class}><{/if}><{if ($i==246 && $j==76) || ($i>=380 && $i<=389 && $j==10)}> calendar_text<{/if}>"
        name="_<{$i}>_<{$j}>" value="<{$vals[$i][$j][0]}>" onchange="text_onchange(this);" >

    <{*<!--いつごろ(不明の時に記載) 、関連医薬品、医療材料・諸物品等、医療機器等-->*}>
    <{if ($i==105 && $j==50) || ($i==100 && $j==50) || ($i==1000)}>
        <input type="button" class="button size04 mL10 radius_mini" value="不明" onclick="unknown_item('<{$i}>','<{$j}>')">

    <{*<!--主治医-->*}>
    <{elseif ($i==243 && $j==73)}>
        <input type="button" class="button mL10 radius_mini" value="職員マスターからコピー" onclick="window.open('hiyari_doctor_search.php?session=<{$session}>', 'newwin', 'width=640,height=480,scrollbars=yes');">
        <span id="doctor_have_span"></span>

    <{*<!--入院日-->*}>
    <{elseif ($i==246 && $j==76) || ($i>=380 && $i<=389 && $j==10)}>
        <a type="button" id="id_<{$i}>_<{$j}>_btn" class="calendar_btn" href="javascript:void(0);" />カレンダー</a>
        <{if $hospital_ymd_flg == 't'}>
            <{if $iwata_flg || $patient_search_ex_flg || ($i>=380 && $i<=389 && $j==10)}>
                <!-- 入院年月日個別取得は行わない -->
            <{else}>
                <input type="button" class="button mL10 radius_mini" value="患者マスターからコピー" onclick="inpt_search_open();">
            <{/if}>
        <{/if}>

    <{*<!--患者ID-->*}>
    <{elseif ($i==210 && $j==10)}>
        <{if $patient_profile_flg == 't'}>
            <{if $kameda_flg}>
                <input type="button" class="button size04 mL10 radius_mini" value="コピー" onclick="getPtid();">
            <{elseif $cis_flg}>
                <input type="button" class="button size04 mL10 radius_mini" value="検索" onclick="cis_patient_search('report_input');">
            <{elseif $kinkyo_flg}>
                <input type="button" class="button size04 mL10 radius_mini" value="検索" onclick="kinkyo_patient_search('report_input');">
            <{elseif $nishiyodo_flg}>
                <input type="button" class="button size04 mL10 radius_mini" value="検索" onclick="nishiyodo_patient_search('report_input');">
            <{elseif $gifudaigaku_flg}>
                <input type="button" class="button size04 mL10 radius_mini" value="検索" onclick="gifudaigaku_patient_search('report_input')">
            <{elseif $boueiidai_flg}>
                <input type="button" class="button size04 mL10 radius_mini" value="検索" onclick="boueiidai_patient_search('report_input')">
            <{elseif $fukushima_flg}>
                <input type="button" class="button size04 mL10 radius_mini" value="検索" onclick="fukushima_patient_search('report_input')">
            <{elseif $patient_search_ex_flg}>
                <input type="button" class="button size04 mL10 radius_mini" value="検索" onclick="patient_search_ex('report_input')">
            <{elseif $iwata_flg}>
                <input type="button" class="button size04 mL10 radius_mini" value="検索" onclick="iwata_patient_search('report_input')">
            <{else}>
                <input type="button" class="button<{if $pt_auth != "1"}>_disabled<{/if}> size04 mL10 radius_mini" value="検索" onclick="patient_search();" <{if $pt_auth != "1"}> disabled<{/if}>>
            <{/if}>
        <{/if}>

        <{*<!--電子カルテ連携-->*}>
        <{if $emr_flg=='t' && $is_sm_emp_flg == true && $gamen_mode == "update"}>
            <input type="button" class="button size04 mL10 radius_mini" value="カルテ参照" onclick="call_emr(<{$i}>, <{$j}>)">
        <{/if}>

    <{*<!--関連する患者情報 患者ID-->*}>
    <{elseif ($i>=310 && $i<=319 && $j==10)}>
        <{*<!--電子カルテ連携-->*}>
        <{if $emr_flg=='t' && $is_sm_emp_flg == true && $gamen_mode == "update"}>
            <input type="button" class="button size04 mL10 radius_mini" value="カルテ参照" onclick="call_emr(<{$i}>, <{$j}>)">
        <{/if}>

    <{*<!--インシデントに直接関連する疾患名/関連する疾患名１〜３-->*}>
    <{elseif ($i==250 && $j==80) || ($i==260 && $j==90) || ($i==270 && $j==100) || ($i==280 && $j==110)}>
        <{if $byoumei_search_flg == 't'}>
            <input type="button" class="button size04 mL10 radius_mini" value="検索" onclick="popupByomeiSelect('id_<{$i}>_<{$j}>');">
        <{/if}>
    <{/if}>

    <{*<!--テキストクラスをクリアしておく-->*}>
    <{assign var="" value=$txt_class}>

<{*<!--
---------------------------------------------------------------------------------------------------
テキストエリア
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'textarea'}>


<{if $eis_item_number_of_character[$i]}>

<div class="form-group">
    <p class="counter pull-right text-muted">
    <font color=#ff0000>入力文字数に制限があります。</font>残り文字数:<a class="count"><{$eis_item_number_of_character[$i]}></a>/<{$eis_item_number_of_character[$i]}></p>
    <textarea class="form-control"id="id_<{$i}>_<{$j}>" rows="<{if isset($row_num)}><{$row_num}><{else}>10<{/if}>" name="_<{$i}>_<{$j}>" maxlength=<{$eis_item_number_of_character[$i]}>><{$vals[$i][$j][0]}></textarea>
</div>

<{else}>
  <textarea id="id_<{$i}>_<{$j}>" rows="<{if isset($row_num)}><{$row_num}><{else}>10<{/if}>" name="_<{$i}>_<{$j}>"><{$vals[$i][$j][0]}></textarea>

<{/if}>

<{*<!--
---------------------------------------------------------------------------------------------------
カレンダー
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'calendar'}>
    <input type="text" id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" class="size06 calendar_text" value="<{$vals[$i][$j][0]}>" readonly>
    <a id="id_<{$i}>_<{$j}>_btn" class="calendar_btn" href="javascript:void(0);" />カレンダー</a>
    <a class="mT3 clear_btn cb_type02" href="javascript:void(0);" onclick="clear_item('<{$grps[$i].easy_item_list[$j].easy_item_type}>','<{$i}>','<{$j}>')">クリア</a>
<{*<!--
---------------------------------------------------------------------------------------------------
セレクトボックス
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'select'}>
    <select id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" onchange="selectbox_onchange(this);">
        <option value=""></option>
        <{foreach from=$grps[$i].easy_item_list[$j].easy_list item=data}>
            <{if $item_element_no_disp[$i][$j] == "" || !in_array($data.easy_code,$item_element_no_disp[$i][$j])}>
                <option value="<{$data.easy_code}>" <{if in_array($data.easy_code, (array)@$vals[$i][$j])}>selected<{/if}>>
                    <{$data.easy_name|escape:"html"}>
                </option>
            <{/if}>
        <{/foreach}>
    </select>

<{*<!--
---------------------------------------------------------------------------------------------------
ラジオボタンとチェックボックス
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'radio' || $grps[$i].easy_item_list[$j].easy_item_type == 'checkbox'}>
    <{*<!--------------------------------------------------------------------------
    table表示
    --------------------------------------------------------------------------->*}>
    <{if $tabletype == 'tabletype02' || $tabletype == 'tabletype04'}>
        <{assign var="colnum" value=$selection[$i][$j].col_num}>
        <{assign var="td_width" value=$selection[$i][$j].item_width}>
        <{assign var="cnt" value=$colnum}>
        <table class="box">
            <{foreach from=$selection[$i][$j].list key=k item=data name=sel}>
                <{*<!--------------------------------------------------------------
                発生要因の区切り
                --------------------------------------------------------------->*}>
                <{if $data.pause_flg == "t"}>
                    <{if !$smarty.foreach.sel.first}>
                        </tr>
                    <{/if}>

                    <tr>
                        <th colspan="<{$colnum}>"><span class="strong"><{$data.easy_name|escape:"html"}></span></th>
                    </tr>

                    <{assign var="cnt" value=$colnum}>

                <{*<!--------------------------------------------------------------
                通常の選択肢
                --------------------------------------------------------------->*}>
                <{else}>
                    <{*<!--改行-->*}>
                    <{if $cnt == $colnum || isset($data.other_code)}>
                        <{*<!--改行前に空セルを必要な分だけ作成-->*}>
                        <{if $cnt < $colnum}>
                            <{section start=$cnt loop=$colnum name=n}>
                                <td width="<{$td_width}>" <{if $smarty.section.n.loop == $colnum}>class="lineR"<{/if}>><span></span></td>
                            <{/section}>
                        <{/if}>

                        <{if !$smarty.foreach.sel.first}>
                            </tr>
                        <{/if}>

                        <tr>
                        <{if isset($data.other_code)}>
                            <{assign var="cnt" value=$colnum}>
                        <{else}>
                            <{assign var="cnt" value=1}>
                        <{/if}>
                    <{else}>
                        <{assign var="cnt" value=$cnt+1}>
                    <{/if}>

                    <{*<!--セル-->*}>
                    <td width="<{$td_width}>" class="<{if $cnt == $colnum}>lineR<{/if}>" <{if isset($data.other_code)}>colspan="<{$colnum}>"<{/if}>>
                    <{if $grps[$i].easy_item_list[$j].easy_item_type == 'radio'}>
                        <span class="item_set<{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>">
                            <label for="id_<{$i}>_<{$j}>_<{$k}>">
                                <input type="radio"
                                    id="id_<{$i}>_<{$j}>_<{$k}>"
                                    class="radio"
                                    name="_<{$i}>_<{$j}>[]"
                                    value="<{$data.easy_code}>"
                                    onclick="radio_onclick(this)"
                                    <{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>
                                >
                    <{else}>
                        <span class="item_set<{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>">
                            <label for="id_<{$i}>_<{$j}>_<{$k}>">
                                <span class="checkbox_wrap<{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>">
                                    <input type="checkbox"
                                        id="id_<{$i}>_<{$j}>_<{$k}>"
                                        class="checkbox"
                                        name="_<{$i}>_<{$j}>[]"
                                        value="<{$data.easy_code}>"
                                        onclick="checkbox_onclick(this)"
                                        <{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>
                                    >
                                </span>
                    <{/if}>
                                <{$data.easy_name|escape:"html"}>
                            </label>

                            <{*<!--その他の入力欄-->*}>
                            <{if isset($data.other_code)}>
                                (<input type="text" id="id_<{$i}>_<{$data.other_code}>" class="text" name="_<{$i}>_<{$data.other_code}>" value="<{$vals[$i][$data.other_code][0]}>">)
                            <{/if}>
                        </span>

                        <{*<!--ヒヤリ・ハット分類-->*}>
                        <{if $i==125 && $j==85}>
                            <input type="hidden" id="item_125_85_<{$data.easy_code}>" value="<{$data.easy_name|escape:"html"}>">
                        <{/if}>

                        <{*<!--概要-->*}>
                        <{if $i==900 && $j==10}>
                            <{section name=k loop=$grps[900].easy_item_list[10].easy_list}>
                                <{if $item_element_no_disp[900][10] == "" || !in_array($grps[900].easy_item_list[10].easy_list[k].easy_code,$item_element_no_disp[900][10])}>
                                    <input type="hidden" name="_<{900}>_<{10}>_<{$grps[900].easy_item_list[10].easy_list[k].easy_code}>_hdn"
                                        value="<{$grps[900].easy_item_list[10].easy_list[k].export_code}>">
                                <{/if}>
                            <{/section}>
                        <{/if}>
                    </td>
                <{/if}>
            <{/foreach}>

            <{*<!--空セル-->*}>
            <{if $cnt < $colnum}>
                <{section start=$cnt loop=$colnum name=n}>
                    <td width="<{$td_width}>" class="<{if $smarty.section.n.loop == $colnum}>lineR<{/if}>"><span></span></td>
                <{/section}>
            <{/if}>

            </tr>

            <{*<!--クリアボタン-->*}>
            <{if $grps[$i].easy_item_list[$j].easy_item_type == 'radio'}>
                <tr>
                    <td colspan="<{$colnum}>" class="lineB lineR">
                        <span>
                            <a class="clear_btn cb_type02" href="javascript:void(0);" onclick="clear_item('<{$grps[$i].easy_item_list[$j].easy_item_type}>','<{$i}>','<{$j}>')">クリア</a>
                        </span>
                    </td>
                </tr>
            <{/if}>
        </table>

    <{*<!--------------------------------------------------------------------------
    横並び表示
    --------------------------------------------------------------------------->*}>
    <{elseif $tabletype == 'tabletype03'}>
        <ul>
            <{foreach from=$selection[$i][$j].list key=k item=data name=sel}>
            <{if $grps[$i].easy_item_list[$j].easy_item_type == 'radio'}>
                <li class="item_set<{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>">
                    <label for="id_<{$i}>_<{$j}>_<{$k}>">
                        <input type='radio'
                            id="id_<{$i}>_<{$j}>_<{$k}>"
                            class="radio"
                            name="_<{$i}>_<{$j}>[]"
                            value="<{$data.easy_code}>"
                            onclick="radio_onclick(this)"
                            <{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>
                        >
            <{else}>
                <li class="item_set<{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>">
                    <label for="id_<{$i}>_<{$j}>_<{$k}>">
                        <span class="checkbox_wrap<{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>">
                            <input type='checkbox'
                                id="id_<{$i}>_<{$j}>_<{$k}>"
                                class="checkbox"
                                name="_<{$i}>_<{$j}>[]"
                                value="<{$data.easy_code}>"
                                onclick="checkbox_onclick(this)"
                                <{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>
                            >
                        </span>
            <{/if}>
                        <{$data.easy_name|escape:"html"}>
                    </label>

                    <{*<!--その他の入力欄-->*}>
                    <{if isset($data.other_code)}>
                        (<input type="text" id="id_<{$i}>_<{$data.other_code}>" class="text" name="_<{$i}>_<{$data.other_code}>" value="<{$vals[$i][$data.other_code][0]}>">)
                    <{/if}>
                </li>
            <{/foreach}>
        </ul>

        <{*<!--クリアボタン-->*}>
        <{if $grps[$i].easy_item_list[$j].easy_item_type == 'radio'}>
            <a class="clear_btn cb_type03" href="javascript:void(0);" onclick="clear_item('<{$grps[$i].easy_item_list[$j].easy_item_type}>','<{$i}>','<{$j}>')">クリア</a>
        <{/if}>

    <{*<!--------------------------------------------------------------------------
    縦並び表示
    --------------------------------------------------------------------------->*}>
    <{else}>
        <ul>
            <{foreach from=$selection[$i][$j].list key=k item=data name=sel}>
            <{if $grps[$i].easy_item_list[$j].easy_item_type == 'radio'}>
                <li class="item_set<{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>">
                    <label for="id_<{$i}>_<{$j}>_<{$k}>">
                        <input type="radio"
                            id="id_<{$i}>_<{$j}>_<{$k}>"
                            class="radio"
                            name="_<{$i}>_<{$j}>[]"
                            value="<{$data.easy_code}>"
                            onclick="radio_onclick(this)"
                            <{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>
                        >
            <{else}>
                <li class="item_set<{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>">
                    <label for="id_<{$i}>_<{$j}>_<{$k}>">
                        <span class="checkbox_wrap<{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>">
                            <input type="checkbox"
                                id="id_<{$i}>_<{$j}>_<{$k}>"
                                class="checkbox"
                                name="_<{$i}>_<{$j}>[]"
                                value="<{$data.easy_code}>"
                                onclick="checkbox_onclick(this)"
                                <{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>
                            >
                        </span>
            <{/if}>
                        <{$data.easy_name|escape:"html"}>
                    </label>

                    <{*<!--その他の入力欄-->*}>
                    <{if isset($data.other_code)}>
                        (<input type="text" id="id_<{$i}>_<{$data.other_code}>" class="text" name="_<{$i}>_<{$data.other_code}>" value="<{$vals[$i][$data.other_code][0]}>">)
                    <{/if}>
                </li>
            <{/foreach}>
        </ul>
        <{*<!--クリアボタン-->*}>
        <{if $grps[$i].easy_item_list[$j].easy_item_type == 'radio'}>
            <a class="clear_btn cb_type01" href="javascript:void(0);" onclick="clear_item('<{$grps[$i].easy_item_list[$j].easy_item_type}>','<{$i}>','<{$j}>')">クリア</a>
        <{/if}>
    <{/if}>
<{/if}>
