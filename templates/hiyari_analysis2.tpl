<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$page_title}></title>
	<meta http-equiv="imagetoolbar" content="no" />
	<link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/analysis.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/date_search.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/pagination.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/smoothness/jquery-ui.custom.css">
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="js/jquery/jquery-ui-1.10.2.custom.min.js"></script>
  <script type="text/javascript" src="js/jquery/jquery.ui.datepicker-ja.min.js" charset="utf-8"></script>
  <script>
    jQuery(function($){
      //チェックボックスのチェックがある時のtr
      $('.checkbox').click(function(e) {
        $(this).closest('tr').toggleClass('checked');
      });	
      
      //カレンダー
      j$('.calendar_text').datepicker({showOn: 'focus', showOtherMonths: true, selectOtherMonths: true});
      j$('.calendar_btn').click(function(){
        j$(this).prev('input').datepicker( 'show' );
      });
    });
  </script>
  <script type="text/javascript" src="js/hiyari_analysis.js"></script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
</head>

<body id="top">
  <{*ヘッダー*}>
  <{include file="hiyari_header2.tpl"}>

	<div id="content" class="clearfix <{$tab_info.total_width_class}>">
    <form name="list_form" method="post" action="hiyari_analysis_disp.php">
      <input type="hidden" name="session" value="<{$session}>">

      <input type="hidden" name="is_postback" value="true">
      <input type="hidden" name="mode" value="search">

      <!-- 期間指定データ START -->
      <input type="hidden" name="search_date_mode" value="<{$search_date_mode}>">
      <input type="hidden" name="search_date_year" value="<{$search_date_year}>">
      <input type="hidden" name="search_date_month" value="<{$search_date_month}>">
      <!-- 期間指定データ END -->
      
      <input type="hidden" name="search_toggle" value="<{$search_toggle}>">

      <input type="hidden" name="page" value="<{$page}>">
      <input type="hidden" name="sort_item" value="<{$sort_item}>">
      <input type="hidden" name="sort_div" value="<{$sort_div}>">

      <div id="sub" class="clearfix">
        <{* 日付条件 *}>
        <{if $search_date_mode != "non"}>
          <{include file="hiyari_date_search2.tpl"}>
        <{/if}>
      </div>
      
      <div id="main" class="<{$tab_info.main_width_class}>">
        <{* 検索領域 *}>
        <div id="search_area" class="color02 radius">
          <table class="v_title">
            <tr>
              <th>分析番号</th>
              <td class="input_no">
                <input type="text" name="search_analysis_no" id="search_analysis_no" value="<{$search_analysis_no}>" maxlength="40">
              </td>
            </tr>
            <tr>
              <th>事案番号</th>
              <td class="input_no">
                <input type="text" name="search_problem_no" id="search_problem_no" value="<{$search_problem_no}>" maxlength="40">
              </td>
            </tr>
            <tr>
              <th>タイトル</th>
              <td class="input_str">
                <input type="text" name="search_analysis_title" id="search_analysis_title" value="<{$search_analysis_title}>" maxlength="40">
              </td>
            </tr>
            <tr>
              <th>概要</th>
              <td class="input_str">
                <input type="text" name="search_analysis_summary" id="search_analysis_summary" value="<{$search_analysis_summary}>" maxlength="40">
              </td>
            </tr>
            <tr>
              <th>分析手法</th>
              <td class="input_label">
                <label for="method_all"     ><input type="radio" name="search_analysis_method" id="method_all"     value=""        <{if $search_analysis_method==""       }>checked<{/if}>>全て</label>
                <label for="method_general" ><input type="radio" name="search_analysis_method" id="method_general" value="general" <{if $search_analysis_method=="general"}>checked<{/if}>>汎用</label>
                <label for="method_4M4E"    ><input type="radio" name="search_analysis_method" id="method_4M4E"    value="4M4E"    <{if $search_analysis_method=="4M4E"   }>checked<{/if}>>４Ｍ４Ｅ</label>
                <label for="method_SHEL"    ><input type="radio" name="search_analysis_method" id="method_SHEL"    value="SHEL"    <{if $search_analysis_method=="SHEL"   }>checked<{/if}>>ＳＨＥＬ</label>
                <label for="method_RCA"     ><input type="radio" name="search_analysis_method" id="method_RCA"     value="RCA"     <{if $search_analysis_method=="RCA"    }>checked<{/if}>>ＲＣＡ</label>
              </td>
            </tr>
            <tr>
              <th>更新年月日</th>
              <td class="input_date">
                開始年月日
                <input type="text" id="renewal_date_start_input" class="calendar_text" name="renewal_date_start_input" value="<{$renewal_date_start_input}>" maxlength="10">
                <a class="calendar_btn" href="#">カレンダー</a>

                終了年月日
                <input type="text" id="renewal_date_end_input" class="calendar_text" name="renewal_date_end_input" value="<{$renewal_date_end_input}>" maxlength="10">
                <a class="calendar_btn" href="#">カレンダー</a>
              </td>
            </tr>
            <tr>
              <th>評価予定期日</th>
              <td class="input_date">
                開始年月日
                <input type="text" id="assessment_date_start_input" class="calendar_text" name="assessment_date_start_input" value="<{$assessment_date_start_input}>" value="" maxlength="10">
                <a class="calendar_btn" href="#">カレンダー</a>

                終了年月日
                <input type="text" id="assessment_date_end_input" class="calendar_text" name="assessment_date_end_input" value="<{$assessment_date_end_input}>" value="" maxlength="10">
                <a class="calendar_btn" href="#">カレンダー</a>
              </td>
            </tr>
            <tr>
              <th class="none">進捗</th>
              <td class="none">
                <select name="search_analysis_progress">
                  <option value=""  <{if $search_analysis_progress=="" }>selected<{/if}>>全て</option>
                  <option value="1" <{if $search_analysis_progress=="1"}>selected<{/if}>>分析中</option>
                  <option value="2" <{if $search_analysis_progress=="2"}>selected<{/if}>>分析済</option>
                  <option value="3" <{if $search_analysis_progress=="3"}>selected<{/if}>>評価中</option>
                  <option value="4" <{if $search_analysis_progress=="4"}>selected<{/if}>>評価済</option>
                </select>
              </td>
            </tr>                        
          </table>
          <input type="button" class="button radius_mini" value="検索" onclick="search_torey();">
          <input type="button" class="button radius_mini" value="閉じる" onclick="show_search_area();" id="close_search_btn">
          <script type="text/javascript">
            <{if $search_toggle}>
              document.getElementById('search_area').style.display = '';
            <{else}>
              document.getElementById('search_area').style.display = 'none';
            <{/if}>
          </script>
        </div>
        
        <div id="list_area" class="color02 radius">
          <{* 実行アイコン *}>
          <ul id="main_nav" class="clearfix">
            <{if $is_analysis_update_usable}>
              <li class="main_nav_01"><a href="#" onclick="analysis_new('<{$session}>');">作成</a></li>
            <{/if}>
            <li class="main_nav_08"><a href="#" onclick="output_excel('<{$has_shel}>');">出力（SHEL）</a></li>
            <li class="main_nav_09"><a href="#" onclick="show_search_area();">検索</a></li>
            <{if $is_analysis_update_usable}>
              <li class="main_nav_90"><a href="#" onclick="analysis_action_delete();">削除</a></li>
            <{/if}>
          </ul>
          
          <{* 一覧 START *}>
          <table class="list">
            <tr>
              <th width="15"></th>
              <th width="70">分析番号</th>
              <th width="70">事案番号</th>
              <th>タイトル</th>
              <th width="70">更新年月日</th>
              <th width="65">評価期日</th>
              <th width="40">進捗</th>
              <th width="40" class="none">手法</th>
            </tr>
            <{foreach from=$list item=i}>
              <tr>
                <td>
                  <{if $is_sm || $login_user_id == $i.registed_user_id}>
                    <span class="checkbox_wrap">
                      <input type="checkbox" class="checkbox" name="delete_key[]" value="<{$i.id}>">
                    </span>
                  <{else}>
                    <span class="checkbox_wrap disabled">
                      <input type="checkbox" class="checkbox" name="delete_key[]" value="<{$i.id}>" disabled>
                    </span>
                  <{/if}>
                </td>
                <td>
                  <{if $is_analysis_update_usable}>
                      <a href="JavaScript:void(0);" onclick="analysis_new('<{$session}>', <{$i.id}>);return false;"><{$i.no}></a>
                  <{else}>
                      <{$i.no}>
                  <{/if}>
                </td>
                <td>
                  <{$i.problem}>
                </td>
                <td>
                  <a class="ellipsis" href="JavaScript:void(0);" onclick="analysis_method_new(<{$i.id}>, '<{$i.method}>', '<{$i.problem}>', '<{$session}>');return false;"><{$i.title}></a>
                </td>
                <td>
                  <{$i.renewal_date}>
                </td>
                <td>
                  <{$i.progress_date}>
                </td>
                <td>
                  <{$i.progress}>
                </td>
                <td class="none">
                  <{$i.method_name}>
                </td>
              </tr>
            <{/foreach}>
          </table>
          <{include file="hiyari_paging2.tpl"}>        
        </div>
      </div>
    </form>
  </div>
	<div id="footer">
		<p>Copyright Comedix all right reserved.</p>
	</div>
</body>