<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$page_title}></title>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/hiyari_analysis.js"></script>
  <script type="text/javascript" src="js/hiyari_calendar.js"></script>
  <style type="text/css">
    .list {border-collapse:collapse;}
    .list td {border:#35B341 solid 1px;}
    .list_2 {border-collapse:collapse;}
    .list_2 td {border:#999999 solid 1px;}
    .list_in {border-collapse:collapse;}
    .list_in td {border:#FFFFFF solid 0px;}
  </style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
  <!-- �إå��� START -->
  <{include file="hiyari_header1.tpl"}>
  <!-- �إå��� END -->

  <form name="list_form" method="post" action="hiyari_analysis_disp.php">
    <input type="hidden" name="session" value="<{$session}>">

    <input type="hidden" name="is_postback" value="true">
    <input type="hidden" name="mode" value="search">

    <!-- ���ֻ���ǡ��� START -->
    <input type="hidden" name="search_date_mode" value="<{$search_date_mode}>">
    <input type="hidden" name="search_date_year" value="<{$search_date_year}>">
    <input type="hidden" name="search_date_month" value="<{$search_date_month}>">
    <!-- ���ֻ���ǡ��� END -->
    
    <input type="hidden" name="search_toggle" value="<{$search_toggle}>">

    <input type="hidden" name="page" value="<{$page}>">
    <input type="hidden" name="sort_item" value="<{$sort_item}>">
    <input type="hidden" name="sort_div" value="<{$sort_div}>">

    <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <!-- ��¦ START -->
        <td width="200" valign="top" bgcolor="#D5F4D8">
          <!-- ���վ�� START -->
          <{if $search_date_mode != "non"}>
            <{include file="hiyari_date_search1.tpl"}>
          <{/if}>
          <!-- ���վ�� END -->
        </td>
        <!-- ��¦ END -->
        
        <td width="10"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        
        <!-- ��¦��START -->
        <td valign="top">
          <!-- �¹ԥ������� START -->
          <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
              <td>
                <table border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
                  <tr>
                    <{if $is_analysis_update_usable}>
                      <td align="center"><img src="img/hiyari_rp_new.gif" width="36" height="30" style="cursor: pointer;" onclick="analysis_new('<{$session}>');"></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                      <td align="center"><img src="img/hiyari_rp_delete.gif" style="cursor: pointer;" onclick="analysis_action_delete();"></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>
                    <td align="center"><img src="img/hiyari_rp_search.gif" style="cursor: pointer;" onclick="show_search_area();"></td>
                    <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <td align="center"><img src="img/hiyari_rp_excel.gif" style="cursor: pointer;" onclick="output_excel('<{$has_shel}>');"></td>
                  </tr>
                  <tr>
                    <{if $is_analysis_update_usable}>
                      <td align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12" color="#13781D">����</font></td>
                      <td><img src="img/spacer.gif" width="30" height="1" alt=""></td>
                      <td align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12" color="#13781D">���</font></td>
                      <td><img src="img/spacer.gif" width="30" height="1" alt=""></td>
                    <{/if}>
                    <td align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12" color="#13781D">����</font></td>
                    <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <td align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12" color="#13781D">���ϡ�SHEL��</font></td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <!-- �¹ԥ������� END -->
        
          <!-- �����ΰ� START -->
          <table border="0" cellspacing="0" cellpadding="0"  id="search_area" style="display:none;">
            <img src="img/spacer.gif" width="1" height="10" alt=""><br>
            <tr>
              <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
              <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
              <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
            </tr>
            <tr>
              <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
              <td bgcolor="#F5FFE5">
                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                  <tr>
                    <td rowspan="2">
                      <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                        <tr>
                          <td width="80" bgcolor="#DFFFDC"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ʬ���ֹ�</font></td>
                          <td width="150" bgcolor="#FFFFFF">
                            <input type="text" name="search_analysis_no" id="search_analysis_no" size="40" value="<{$search_analysis_no}>" maxlength="40" style="ime-mode:disable">
                          </td>
                          <td width="80" bgcolor="#DFFFDC"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�����ֹ�</font></td>
                          <td width="*" bgcolor="#FFFFFF">
                            <input type="text" name="search_problem_no" id="search_problem_no" size="40" value="<{$search_problem_no}>" maxlength="40" style="ime-mode:disable">
                          </td>
                        </tr>
                        <tr>
                          <td width="60" bgcolor="#DFFFDC"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�����ȥ�</font></td>
                          <td width="*" bgcolor="#FFFFFF" colspan="3"><input type="text" name="search_analysis_title" id="search_analysis_title" size="80" value="<{$search_analysis_title}>" maxlength="40" style="ime-mode:active"></td>
                        </tr>
                        <tr>
                          <td width="60" bgcolor="#DFFFDC"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
                          <td width="*" bgcolor="#FFFFFF" colspan="3"><input type="text" name="search_analysis_summary" id="search_analysis_summary" size="80" value="<{$search_analysis_summary}>" maxlength="40" style="ime-mode:active"></td>
                        </tr>
                        <tr>
                          <td width="60" bgcolor="#DFFFDC"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ʬ�ϼ�ˡ</font></td>
                          <td width="*" bgcolor="#FFFFFF" colspan="3">
                            <font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
                              <input type="radio" name="search_analysis_method" value=""        <{if $search_analysis_method==""       }>checked<{/if}>>����&nbsp;&nbsp;&nbsp;
                              <input type="radio" name="search_analysis_method" value="general" <{if $search_analysis_method=="general"}>checked<{/if}>>����&nbsp;&nbsp;&nbsp;
                              <input type="radio" name="search_analysis_method" value="4M4E"    <{if $search_analysis_method=="4M4E"   }>checked<{/if}>>���ͣ���&nbsp;&nbsp;&nbsp;
                              <input type="radio" name="search_analysis_method" value="SHEL"    <{if $search_analysis_method=="SHEL"   }>checked<{/if}>>�ӣȣţ�&nbsp;&nbsp;&nbsp;
                              <input type="radio" name="search_analysis_method" value="RCA"     <{if $search_analysis_method=="RCA"    }>checked<{/if}>>�ңã�&nbsp;&nbsp;&nbsp;
                            </font>
                          </td>
                        </tr>
                        <tr>
                          <td width="60" bgcolor="#DFFFDC"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����ǯ����</font></td>
                          <td width="*" bgcolor="#FFFFFF"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����ǯ����</font><input type="text" id="renewal_date_start_input" name="renewal_date_start_input" value="<{$renewal_date_start_input}>" style="width:80px" maxlength="10">
                            <img src="img/calendar_link.gif" style="cursor:pointer;" onclick="call_hiyari_calendar('renewal_search_st', '<{$session}>')"/>
                          </td>
                          <td width="*" bgcolor="#FFFFFF" colspan="2"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��λǯ����</font>
                            <input type="text" id="renewal_date_end_input" name="renewal_date_end_input" value="<{$renewal_date_end_input}>" style="width:80px" maxlength="10">
                            <img src="img/calendar_link.gif" style="cursor:pointer;" onclick="call_hiyari_calendar('renewal_search_ed', '<{$session}>')"/>
                          </td>
                        </tr>
                        <tr>
                          <td width="60" bgcolor="#DFFFDC"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ɾ��ͽ�����</font></td>
                          <td width="*" bgcolor="#FFFFFF"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����ǯ����</font>
                            <input type="text" id="assessment_date_start_input" name="assessment_date_start_input" value="<{$assessment_date_start_input}>" style="width:80px" value="" maxlength="10">
                            <img src="img/calendar_link.gif" style="cursor:pointer;" onclick="call_hiyari_calendar('assessment_search_st', '<{$session}>')"/>
                          </td>
                          <td width="*" bgcolor="#FFFFFF" colspan="2"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��λǯ����</font>
                            <input type="text" id="assessment_date_end_input" name="assessment_date_end_input" value="<{$assessment_date_end_input}>" style="width:80px" value="" maxlength="10">
                            <img src="img/calendar_link.gif" style="cursor:pointer;" onclick="call_hiyari_calendar('assessment_search_ed', '<{$session}>')"/>
                          </td>
                        </tr>
                        <tr>
                          <td width="60" bgcolor="#DFFFDC"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��Ľ</font></td>
                          <td width="*" bgcolor="#FFFFFF" colspan="3">
                            <select name="search_analysis_progress">
                              <option value=""  <{if $search_analysis_progress=="" }>selected<{/if}>>����</option>
                              <option value="1" <{if $search_analysis_progress=="1"}>selected<{/if}>>ʬ����</option>
                              <option value="2" <{if $search_analysis_progress=="2"}>selected<{/if}>>ʬ�Ϻ�</option>
                              <option value="3" <{if $search_analysis_progress=="3"}>selected<{/if}>>ɾ����</option>
                              <option value="4" <{if $search_analysis_progress=="4"}>selected<{/if}>>ɾ����</option>
                            </select>
                          </td>
                        </tr>                        
                      </table>
                    </td>
                    <td valign="top">
                      <a href="javascript:void(0);" onclick="show_search_area();return false;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�Ĥ���</font></a>
                    </td>
                  </tr>
                  <tr>
                    <td valign="bottom">
                      <input type="button" value="����" onclick="search_torey();">
                    </td>
                  </tr>
                </table>
              </td>
              <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
            </tr>
            <tr>
              <td><img src="img/r_3.gif" width="10" height="10"></td>
              <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
              <td><img src="img/r_4.gif" width="10" height="10"></td>
            </tr>
          </table>
          <img src="img/spacer.gif" width="1" height="10" alt=""><br>
          <script type="text/javascript">
            <{if $search_toggle}>
              document.getElementById('search_area').style.display = '';
            <{else}>
              document.getElementById('search_area').style.display = 'none';
            <{/if}>
          </script>
          <!-- �����ΰ� END -->
        
          <!-- ���� START -->
          <table border="0" cellspacing="0" cellpadding="2" width="100%" class="list">
            <tr>
              <td bgcolor="#DFFFDC"></td>
              <td bgcolor="#DFFFDC" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ʬ���ֹ�</font></td>
              <td bgcolor="#DFFFDC" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�����ֹ�</font></td>
              <td bgcolor="#DFFFDC" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">�����ȥ�</font></td>
              <td bgcolor="#DFFFDC" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����ǯ����</font></td>
              <td bgcolor="#DFFFDC" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">ɾ������</font></td>
              <td bgcolor="#DFFFDC" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��Ľ</font></td>
              <td bgcolor="#DFFFDC" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��ˡ</font></td>
            </tr>
            <{foreach from=$list item=i}>
              <tr>
                <{if $is_sm || $login_user_id == $i.registed_user_id}>
                  <td align="center" width="10px"><input type="checkbox" name="delete_key[]" value="<{$i.id}>"></td>
                <{else}>
                  <td align="center" width="10px"><input type="checkbox" name="delete_key[]" value="<{$i.id}>" disabled></td>
                <{/if}>
                <td align="center" width="120px">
                  <font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
                    <{if $is_analysis_update_usable}>
                        <a href="JavaScript:void(0);" onclick="analysis_new('<{$session}>', <{$i.id}>);return false;"><{$i.no}></a>
                    <{else}>
                        <{$i.no}>
                    <{/if}>
                  </font>
                </td>
                <td align="center" width="100px"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><{$i.problem}></font></td>
                <td>
                  <font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
                    <a href="JavaScript:void(0);" onclick="analysis_method_new(<{$i.id}>, '<{$i.method}>', '<{$i.problem}>', '<{$session}>'); return false;"><{$i.title}></a>
                  </font>
                </td>
                <td align="center" width="100px">
                  <font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><{$i.renewal_date}></font>
                </td>
                <td align="center" width="100px">
                  <font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><{$i.progress_date}></font>
                </td>
                <td align="center" width="60px">
                  <font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><{$i.progress}></font>
                </td>
                <td align="center" width="60px">
                  <font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><{$i.method_name}></font>
                </td>
              </tr>
            <{/foreach}>
          </table>
          <!-- ���� END -->
          
          <!-- �ڡ������� START -->
          <{include file="hiyari_paging1.tpl"}>
          <!-- �ڡ������� END -->                        
        </td>
        <!-- ��¦��END -->
      </tr>
    </table>
  </form>
</td>
</tr>
</table>
</body>