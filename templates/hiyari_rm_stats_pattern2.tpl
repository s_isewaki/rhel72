<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | 統計分析：定型集計</title>
	<meta http-equiv="imagetoolbar" content="no" />
	<link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/rm_stats.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/hiyari_rm_stats.js"></script>
  <{include file="hiyari_post_select_js.tpl"}>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
  <style type="text/css">
      body select {font-size: 13px;}
  </style>
  <script type="text/javascript">
    $(function($){
    	//集計ダウンロード表示
    	<{if $stats_download_mode == true}>
	       	var sub_menu_width = document.getElementById('sub_menu');
	       	sub_menu_width.style.width = "630px";
	    <{/if}>
		    
        //チェックボックス
        $('.checkbox').click(function(){
            $(this).closest('.checkbox_wrap').toggleClass('checked');
        });
    });

    function init_page()
    {
      <{* 集計部署の選択肢の初期化 *}>
      <{section name=cnt loop=$belong_cnt}>
          set_post_select_box_obj_key("belong_<{$smarty.section.cnt.index}>",
                                      "belong_class_<{$smarty.section.cnt.index}>",
                                      "belong_attribute_<{$smarty.section.cnt.index}>",
                                      "belong_dept_<{$smarty.section.cnt.index}>",
                                      "belong_room_<{$smarty.section.cnt.index}>");
          setClassOptions("belong_<{$smarty.section.cnt.index}>",
                          "<{$belong_class[$smarty.section.cnt.index]}>",
                          "<{$belong_attribute[$smarty.section.cnt.index]}>",
                          "<{$belong_dept[$smarty.section.cnt.index]}>",
                          "<{$belong_room[$smarty.section.cnt.index]}>");
      <{/section}>
    }
      
    function submit_func(mode)
    {
      document.main_form.mode.value = mode;
      document.main_form.submit();
    }

    //統計分析レポート
    function show_report_list_window(vertical_title, horizontal_title, report_id_csv)
    {
      document.sub_form.vertical_item_label.value = vertical_title;
      document.sub_form.horizontal_item_label.value = horizontal_title;
      document.sub_form.report_id_list.value = report_id_csv;
      
      var h = window.screen.availHeight - 30;
      var w = window.screen.availWidth - 10;
      var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
      window.open('', 'rm_stats_reports_window', option);
      
      document.sub_form.submit();
    }

    //EXCEL
    function excel_download()
    {
      document.xls.submit();
    }

    //部署
    function belong_add(obj) {
      obj.mode.value = "add_belong";
      obj.submit();
    }
    function belong_delete(obj, no) {
      obj.mode.value = "del_belong";
      obj.del_line_no.value = no;
      obj.submit();
    }
  </script>
</head>

<body id="top" onload="init_page()";>
  <{*ヘッダー*}>
  <{include file="hiyari_header2.tpl"}>

  <div id="content" class="<{$tab_info.total_width_class}>">
    <{* 上部ボタン *}>
    <div id="sub_menu" class="color01 radius">
      <table class="v_title">
        <tr>
          <th class="none">
            <img src="img/side_nav_txt01.gif" alt="MENU" />
          </th>
          <td class="none">
            <a href="hiyari_rm_stats.php?session=<{$session}>">クロス集計</a>
          </td>
          <td class="active none">
            <a href="hiyari_rm_stats_pattern.php?session=<{$session}>">定型集計</a>
          </td>
          <{if $stats_download_mode == true}>
	          <td class="none">
	            <a href="hiyari_rm_stats_download.php?session=<{$session}>">集計ダウンロード</a>
	          </td>
          <{/if}>
        </tr>
      </table>
    </div>
    
    <!----------------------------------------------------------------------------------- 
    集計条件領域 START
    ------------------------------------------------------------------------------------>
    <form name="main_form" method="post" action="hiyari_rm_stats_pattern.php?session=<{$session}>">
      <input type="hidden" name="mode" value="">
      <!-- 集計条件:所属 -->
      <input type="hidden" name="belong_cnt" value="<{$belong_cnt}>">
      <input type="hidden" name="del_line_no" value="">
      
      <div id="conditions1" class="color01 radius">
        <table class="v_title">
          <tr>
            <th width="100">集計モード</th>
            <td colspan="<{$post_select_data.class_cnt+1}>">
              <!-- 集計条件:集計モード -->
              <select name="date_mode">
                <option value="report_date" <{$report_date_str}>>報告日</option>
                <option value="incident_date" <{$incident_date_str}>>発生日</option>
              </select>
              <!-- 集計条件:副報告 -->
              <span class="checkbox_wrap<{if $sub_report_use == 'true'}> checked<{/if}>">
                <input type="checkbox" class="checkbox" id="sub_report_use" name="sub_report_use" value="true" <{if $sub_report_use=="true"}>checked<{/if}>>
              </span>
              <label for="sub_report_use">副報告を集計に含める</label>
            </td>
          </tr>
          
          <!-- 集計条件:集計年月 -->
          <tr>
            <th>集計年月</th>
            <td colspan="<{$post_select_data.class_cnt+1}>">
              <select name="date_year">
                <{foreach from=$year_list item=y}>
                  <option value="<{$y}>" <{if $date_year==$y}>selected<{/if}>><{$y}></option>
                <{/foreach}>
              </select>年
              
              <select name="date_month">
                <{foreach from=$month_list item=m}>
                  <option value="<{$m|string_format:'%02d'}>" <{if $date_month==$m}>selected<{/if}>><{$m}></option>
                <{/foreach}>
              </select>月〜
              
              <select name="date_term">
                <{foreach from=$month_list item=t}>
                  <option value="<{$t|string_format:'%02d'}>" <{if $date_term==$t}>selected<{/if}>><{$t}>ヶ月分</option>
                <{/foreach}>
              </select>
            </td>
          </tr>    
                              
          <!-- 集計条件:所属 -->
          <{section name=cnt loop=$belong_cnt}>
            <tr>
              <{if $smarty.section.cnt.index == 0}>
                <th rowspan="<{$belong_cnt}>">所属</th>
              <{/if}>              
            
              <td>
                <div id="busho_data">
                  <{$arr_class_name[0]|escape}>
                  <select id="belong_class_<{$smarty.section.cnt.index}>" name="belong_class[]" onchange="setAtrbOptions('belong_<{$smarty.section.cnt.index}>');"></select>

                  <{$arr_class_name[1]|escape}>
                  <select id="belong_attribute_<{$smarty.section.cnt.index}>" name="belong_attribute[]" onchange="setDeptOptions('belong_<{$smarty.section.cnt.index}>');"></select>

                  <{$arr_class_name[2]|escape}>
                  <select id="belong_dept_<{$smarty.section.cnt.index}>" name="belong_dept[]" onchange="setRoomOptions('belong_<{$smarty.section.cnt.index}>');"></select>
                  
                  <{if $arr_class_name.class_cnt == 4}>
                    <{$arr_class_name[3]|escape}>
                    <select id="belong_room_<{$smarty.section.cnt.index}>" name="belong_room[]"></select>
                  <{/if}>
                </div>
                <div id="busho_btn">
                  <input class="button radius_mini" type="button" value="追加" onClick="belong_add(this.form)">
                  <{if !$smarty.section.cnt.first}>
                    <input class="button radius_mini" type="button" value="削除" onClick="belong_delete(this.form, <{$smarty.section.cnt.index}>)">
                  <{/if}>
                </div>
              </td>
            </tr>
          <{/section}>

          <!-- 集計条件:軸 -->
          <tr>
            <th class="none">軸</th>
            <td class="none">
              縦軸組織
              <select name="vertical_axis">
                <{foreach from=$vertical_axis_arr key=value item=text}>
                  <option value="<{$value}>" <{if $vertical_axis == $value}>selected<{/if}>><{$text}></option>
                <{/foreach}>
              </select>
              
              横軸
              <select name="horizontal_axis">
                <{foreach from=$horizontal_axis_arr key=value item=text}>
                  <option value="<{$value}>" <{if $horizontal_axis==$value}>selected<{/if}>><{$text}></option>
                <{/foreach}>
              </select>
            </td>
          </tr>
        </table>
      </div>       
      <div id="main_btn">
        <input class="button radius_mini" type="button" value="集計" onClick="submit_func('sum');">
      </div>
    
      <!----------------------------------------------------------------------------------- 
      集計結果領域 START
      ------------------------------------------------------------------------------------>
      <{if $mode == "sum"}>
        <{if $show_excel_btn_flg}>
          <div class="section" id="action_btns">
            <div class="clearfix">
              <ul style="float:right;">
                <li><input class="button radius_mini" type="button" value="EXCEL出力" onclick="excel_download()"></li>
              </ul>
            </div>
          </div>
        <{/if}>
        
        <!-- 集計結果 START -->
        <div id="result_area" class="section">
          <div class="color02 radius">
            <table id="result" class="mT5">
              <!-- 縦軸組織ループ -->
              <{foreach from=$rows item=r}>
                <tr <{if $r.is_col_header}>class="col_title col_top"<{/if}>>
                  <!-- 横軸ループ -->
                  <{foreach from=$r.cells item=c}>
                    <!-- ブランク(左上) -->
                    <{if isset($c.isBlank)}>
                      <th colspan="2"></th>
                    <!-- 列ヘッダ -->
                    <{elseif isset($c.col_header)}>
                      <th width="<{$col_width}>%">
                        <{$c.col_header}>
                      </th>
                    <!-- 行ヘッダ -->
                    <{elseif isset($c.row_header)}>
                      <th rowspan="4" <{if $c.is_delimiter}>class="delimiter"<{/if}>>
                        <{$c.row_header}>
                      </th>
                    <!-- 行サブヘッダ -->
                    <{elseif isset($c.sub_header)}>
                      <th <{if $c.is_delimiter}>class="delimiter"<{/if}>>
                        <{$c.sub_header}>
                      </th>
                    <!-- 計 -->
                    <{elseif isset($c.sum)}>
                      <td <{if $c.is_delimiter}>class="delimiter"<{/if}>>
                        <{if isset($c.sum.v_title)}>
                          <a href="javascript:void(0)" onclick="show_report_list_window('<{$c.sum.v_title}>', '<{$c.sum.h_title}>', '<{$c.sum.report_ids}>'); return false;">
                            <{$c.sum.total}>
                          </a>                                                      
                        <{else}>
                          <{$c.sum.total}>
                        <{/if}>                                                        
                      </td>
                    <!-- データ -->
                    <{elseif isset($c.data)}>
                      <td <{if $c.is_delimiter}>class="delimiter"<{/if}>>
                        <{if isset($c.data.v_title)}>
                          <a href="javascript:void(0)" onclick="show_report_list_window('<{$c.data.v_title}>', '<{$c.data.h_title}>', '<{$c.data.report_ids}>');return false;">
                            <{$c.data.num}>
                          </a>
                        <{else}>
                          <{$c.data.num}>
                        <{/if}>
                      </td>
                    <{/if}>
                  <{/foreach}>
                </tr>
              <{/foreach}>
            </table>
          </div>
        </div>
      <{/if}>
    </form>
  </div>
  
  <form name="sub_form" method="post" action="hiyari_rm_stats_reports.php" target="rm_stats_reports_window">
    <input type="hidden" name="session" value="<{$session}>">
    <input type="hidden" name="vertical_label" value="<{$vertical_label}>">
    <input type="hidden" name="horizontal_label" value="<{$horizontal_label}>">
    <input type="hidden" name="vertical_item_label" value="">
    <input type="hidden" name="horizontal_item_label" value="">
    <input type="hidden" name="report_id_list" value="">
    <input type="hidden" name="count_date_mode" value="<{$date_mode}>">
  </form>

  <{* 集計が実行された場合 *}>
  <{if $mode == "sum"}>
    <form name="xls" method="post" action="hiyari_rm_stats_pattern_excel.php" target="download">
      <input type="hidden" name="date_mode" value="<{$date_mode}>">
      <input type="hidden" name="date_year" value="<{$date_year}>">
      <input type="hidden" name="date_month" value="<{$date_month}>">
      <input type="hidden" name="date_term" value="<{$date_term}>">
      <input type="hidden" name="sub_report_use" value="<{$sub_report_use}>">
      <input type="hidden" name="belong_cnt" value="<{$belong_cnt}>">
      <input type="hidden" name="session" value="<{$session}>">
      <{foreach from=$belong_list item=belong}>
        <input type="hidden" name="belong_class[]" value="<{$belong.class}>">
        <input type="hidden" name="belong_attribute[]" value="<{$belong.attribute}>">
        <input type="hidden" name="belong_dept[]" value="<{$belong.dept}>">
        <input type="hidden" name="belong_room[]" value="<{$belong.room}>">
        <input type="hidden" name="belong_selected[]" value="<{$belong.selected}>">
      <{/foreach}>
      <input type="hidden" name="vertical_axis" value="<{$vertical_axis}>">
      <input type="hidden" name="horizontal_axis" value="<{$horizontal_axis}>">
    </form>
    <iframe name="download" width="0" height="0" frameborder="0"></iframe>
  <{/if}>
	<div id="footer">
		<p>Copyright Comedix all right reserved.</p>
	</div>
</body>
</html>
