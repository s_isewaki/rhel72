<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <{$INCIDENT_TITLE}> | トップ</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
  .list {border-collapse:collapse;}
  .list td {border:#35B341 solid 1px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
  <!-- ヘッダー START -->
  <{include file="hiyari_header1.tpl"}>
  <!-- ヘッダー END -->
  <img src="img/spacer.gif" width="1" height="5" alt=""><br>

  <!-- 警告メッセージ START -->
  <{if $warning_flg}>
    <table border="0" cellspacing="0" cellpadding="0" style="margin-bottom:5px">
      <tbody>
        <tr>
          <td width="10"><img src="img/r2_1.gif" width="10" height="10"></td>
          <td width="100%" background="img/r2_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
          <td width="10"><img src="img/r2_2.gif" width="10" height="10"></td>
        </tr>
        <tr>
          <td background="img/r2_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td class="spacing" valign="middle" align="left" bgcolor="#FCEAD8">
                    <font size="4" face="ＭＳ Ｐゴシック, Osaka" class="j15"><b><{$warning_message}></b></font>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
          <td background="img/r2_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        </tr>
        <tr>
          <td><img src="img/r2_3.gif" width="10" height="10"></td>
          <td background="img/r2_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
          <td><img src="img/r2_4.gif" width="10" height="10"></td>
        </tr>
      </tbody>
    </table>
  <{/if}>
  <!-- 警告メッセージ END -->

  <!--未読の受信レポート件数 START-->
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
      <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
      <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
    </tr>
    <tr>
      <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
          <tr>
            <td class="spacing" valign="middle" align="left" bgcolor="#F5FFE5" >
              <font size="4" face="ＭＳ Ｐゴシック, Osaka" class="j15">
                <B>未読のメッセージが&nbsp;<{$no_recv_read_num}>&nbsp;件あります。</B>
              </font>
            </td>
          </tr>
        </table>        
      </td>
      <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    </tr>
    <tr>
      <td><img src="img/r_3.gif" width="10" height="10"></td>
      <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
      <td><img src="img/r_4.gif" width="10" height="10"></td>
    </tr>
  </table>
  <!--未読の受信レポート件数 END-->

  <!--お知らせ START-->
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:5px">
    <tr>
      <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
      <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
      <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
    </tr>
    <tr>
      <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top" style="background-color:#f5ffe5">
              <table width="100%" border="0" cellspacing="0" cellpadding="2" style="background-color:#fff; border-collapse:collapse;">
                <tr>
                  <td bgcolor="#dfffdc" style="border:1px solid #35B341; color:#13781d">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                      <b><{$news_title}></b>
                    </font>
                  </td>
                </tr>
                <tr>
                  <td bgcolor="#ffffff" style="border:1px solid #35B341">
                    <table width="100%" border="0" cellspacing="0" cellpadding="2">
                      <{if isset($news_list)}>                      
                        <{foreach from=$news_list item=data}>
                          <tr>
                            <td width="60%">
                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                              <a href="javascript:void(0)" onclick="window.open('hiyari_news_detail_from_top.php?session=<{$session}>&news_id=<{$data.id}>','newwin','scrollbars=yes,width=640,height=480');return false;" <{$data.style}>><{$data.title}></a>
                              </font>
                            </td>
                            <td>
                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">（<{$data.category}>）</font>
                            </td>
                            <td align="right">
                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$data.date}></font>
                            </td>
                          </tr>
                          <{if isset($data.news)}>
                            <tr>
                              <td colspan="3" style="padding-left:10px;">
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                                  <{$data.news}>
                                </font>
                              </td>
                            </tr>
                            <{if isset($data.img)}>
                              <tr><td colspan="3" style="padding-left:10px;"><{$data.img}></td></tr>
                            <{/if}>
                            <tr>
                              <td colspan="3" style="padding-bottom:10px;"></td>
                            </tr>
                          <{/if}>
                        <{/foreach}>
                      <{else}>
                        <tr><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">お知らせはありません。</font></td></tr>
                      <{/if}>
                    </table>
                  </td>
                </tr>
              </table>             
            </td>
          </tr>
        </table>
      </td>
      <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    </tr>
    <tr>
      <td><img src="img/r_3.gif" width="10" height="10"></td>
      <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
      <td><img src="img/r_4.gif" width="10" height="10"></td>
    </tr>
  </table>
  <!--お知らせ END-->

  <!-- 評価予定期日の報告書件数 START -->
  <{if $is_evaluation_date_auth_all_disp || $is_evaluation_date_auth_post_disp}>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:5px">
      <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
      </tr>
      <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="top" style="background-color:#f5ffe5">
                <div><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b>評価予定期日の報告書件数</b></font></div>
                <{foreach from=$report_cnt_list item=grp}>
                  <{if isset($grp.post_name)}>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr bgcolor="#F5FFE5"><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b><{$grp.post_name}></b></font></td></tr>
                    </table>
                  <{/if}>
                  
                  <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list" style="background-color:#fff">
                    <tr>
                      <td bgcolor="#DFFFDC" width="20%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価日が過ぎた報告書</font></td>
                      <td bgcolor="#DFFFDC" width="20%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">本日</font></td>
                      <td bgcolor="#DFFFDC" width="20%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">翌日</font></td>
                      <td bgcolor="#DFFFDC" width="20%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">1週間以内</font></td>
                      <td bgcolor="#DFFFDC" width="20%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">2週間以内</font></td>
                    </tr>
                    <tr>
                      <{foreach from=$grp.cnt key=type item=count}>
                        <td>
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                            <{if $is_usable}>
                              <a href="<{$classification_link}><{$type}><{$grp.post_param}>">
                            <{/if}>
                              <{$count}>
                            <{if $is_usable}>
                              </a>
                            <{/if}>
                          </font>
                        </td>                       
                      <{/foreach}>
                    </tr>
                  </table>
                <{/foreach}>
              </td>
            </tr>
          </table>
        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
      </tr>
      <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
      </tr>
    </table>
  <{/if}>
  <!-- 評価予定期日の報告書件数 END -->

  <!-- 出来事分析の評価予定期日一覧 START -->
  <{if $is_evaluation_date_auth_all_disp}>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:5px">
      <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
      </tr>
      <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="top" style="background-color:#f5ffe5">
                <div><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b>出来事分析の評価予定期日一覧</b></font></div>
                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list" style="background-color:#fff">
                  <tr>
                    <td bgcolor="#DFFFDC" width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">分析番号</font></td>
                    <td bgcolor="#DFFFDC" width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
                    <td bgcolor="#DFFFDC" width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価予定期日</font></td>
                    <td bgcolor="#DFFFDC" width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">進捗</font></td>
                    <td bgcolor="#DFFFDC" width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">RCA対策</font></td>
                    <td bgcolor="#DFFFDC" width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">優先順位</font></td>
                    <td bgcolor="#DFFFDC" width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価予定期日</font></td>
                    <td bgcolor="#DFFFDC" width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">進捗</font></td>
                    <td bgcolor="#DFFFDC" width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当者</font></td>
                  </tr>
                  <{foreach from=$analysis_list item=data}>
                    <tr>
                      <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$data.no}></font></td>
                      <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$data.title}></font></td>
                      <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$data.progress_date}></font></td>
                      <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$data.progress}></font></td>
                      <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$data.plan}></font></td>
                      <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$data.yusenjun}></font></td>
                      <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$data.kijitu}></font></td>
                      <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$data.sincyoku}></font></td>
                      <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$data.rca_emp_name_list}></font></td>
                    </tr>
                  <{/foreach}>
                </table>                
                <{include file="hiyari_paging1.tpl"}>
              </td>
            </tr>
          </table>
        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
      </tr>
      <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
      </tr>
    </table>
  <{/if}>
  <!-- 出来事分析の評価予定期日一覧 END -->

  <img src="img/spacer.gif" width="1" height="6" alt=""><br>
  <form name="list_form" method="post" action="hiyari_menu.php">
      <input type="hidden" name="session" value="<{$session}>">
      <input type="hidden" name="mode" value="page_change">
      <input type="hidden" name="page" value="1">
  </form>
  
</td>
</tr>
</table>
</body>
</html>