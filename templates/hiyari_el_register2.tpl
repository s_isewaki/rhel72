<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
  <script type="text/javascript" src="js/hiyari_el.js"></script>
  <script type="text/javascript">
    var w = 1024 + 37;
    var h = window.screen.availHeight;
    window.resizeTo(w, h);

    //--------------------------------------------------
    //登録対象者配列
    //--------------------------------------------------
    m_target_list = new Array();
    //参照権限の対象職員
    m_target_list[1] = new Array(
        <{foreach name=n from=$arr_target[1] item=i}>
          <{if !$smarty.foreach.n.first}>,<{/if}>
          new user_info('<{$i.id}>','<{$i.name}>')
        <{/foreach}>
      );
    //更新権限の対象職員
    m_target_list[2] = new Array(
        <{foreach name=n from=$arr_target[2] item=i}>
          <{if !$smarty.foreach.n.first}>,<{/if}>
          new user_info('<{$i.id}>','<{$i.name}>')
        <{/foreach}>
      );

    //--------------------------------------------------
    //部署情報配列
    //--------------------------------------------------
    //部
    var classes = [];
    <{foreach from=$sel_class item=i}>
      classes.push({id: '<{$i.class_id}>', name: '<{$i.class_nm}>'});
    <{/foreach}>

    //課
    var atrbs = {};
    <{foreach from=$sel_atrb key=class_id item=c}>
      atrbs['<{$class_id}>'] = [];
      <{foreach from=$c key=atrb_id item=a}>
        atrbs['<{$class_id}>'].push({id: '<{$atrb_id}>', name: '<{$a}>'});
      <{/foreach}>
    <{/foreach}>

    //科
    var depts = {};
    <{foreach from=$sel_dept key=class_id item=c}>
      depts['<{$class_id}>'] = {};
      <{foreach from=$c key=atrb_id item=a}>
        depts['<{$class_id}>']['<{$atrb_id}>'] = [];
        <{foreach from=$a key=dept_id item=d}>
          depts['<{$class_id}>']['<{$atrb_id}>'].push({id: '<{$dept_id}>', name: '<{$d}>'});
        <{/foreach}>
      <{/foreach}>
    <{/foreach}>

    //--------------------------------------------------
    //画面起動時の処理
    //--------------------------------------------------
    function initPage()
    {
        //登録対象者を設定する。
        update_target_html("1");
        update_target_html("2");

        //登録ファイル数に対する入力項目の制御
        setFileElementsDisplay2();

        //権限部分の初期制御
        onChangeArchive(true, '<{$ref_class_src}>', '<{$ref_atrb_src}>', '<{$upd_class_src}>', '<{$upd_atrb_src}>');
    }
  
    //----------------------------------------------------------------------------------------------------
    // 権限関係以外の画面処理
    //----------------------------------------------------------------------------------------------------
    //ファイルパスを解析し、コンテンツ名・コンテンツタイプをセットします。
    function setFileInfo(no, path)
    {
        //コンテンツ名にセット
        var fname;
        if (path.indexOf('\\') != -1)
        {
            fname = path.replace(/^.*\\/, '');
        }
        else if (path.indexOf('/') != -1)
        {
            fname = path.replace(/^.*\//, '');
        }
        else
        {
            fname = path;
        }
        var docname = fname.replace(/\.[^.]*$/, '');
        document.mainform.elements['document_name'.concat(no)].value = docname;

        //コンテンツタイプにセット
        var ext = fname.replace(/.*\./, '');
        switch (ext)
        {
            case 'doc':
            case 'docx':
                document.mainform.elements['document_type'.concat(no)].options[1].selected = true;
                break;
            case 'xls':
            case 'xlsx':
                document.mainform.elements['document_type'.concat(no)].options[2].selected = true;
                break;
            case 'ppt':
            case 'pptx':
                document.mainform.elements['document_type'.concat(no)].options[3].selected = true;
                break;
            case 'pdf':
                document.mainform.elements['document_type'.concat(no)].options[4].selected = true;
                break;
            case 'txt':
                document.mainform.elements['document_type'.concat(no)].options[5].selected = true;
                break;
            case 'jpg':
            case 'jpeg':
                document.mainform.elements['document_type'.concat(no)].options[6].selected = true;
                break;
            case 'gif':
                document.mainform.elements['document_type'.concat(no)].options[7].selected = true;
                break;
            default:
                document.mainform.elements['document_type'.concat(no)].options[8].selected = true;
                break;
        }
    }

    //登録ファイル数に対する入力項目の制御
    function setFileElementsDisplay2()
    {
        var file_count = document.mainform.file_count.value;

        for (var i = 1; i <= 5; i++){
            var display = (i <= file_count) ? '' : 'none';
            $('#file' + i).css('display', display);
        }
    }
  </script>
  <script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
  <script type="text/javascript" src="js/yui/build/connection/connection-min.js"></script>
</head>

<body id="el_reg" onload="initPage();">
  <!-------------------------------------------------------------------------
  ヘッダー 
  -------------------------------------------------------------------------->
	<div id="header" class="clearfix">
		<div class="inner" class="clearfix">
			<h1><{$PAGE_TITLE}></h1>
      <p class="notice">※<{$upload_max_filesize}>を超えるファイルは登録できません。</p>
      <p class="action_btn">
        <input type="button" class="button radius_mini" class="button radius_mini" value="登録" onclick="submitForm();">
      </p>
		</div>
	</div>

  <!-------------------------------------------------------------------------
  コンテンツ
  -------------------------------------------------------------------------->
  <div id="content" class="clearfix">
    <div class="section">
      <form name="mainform" action="hiyari_el_register_exe.php" method="post" enctype="multipart/form-data">
        <div class="color02 radius clearfix mB5">
          <table class="v_title">
            <!-- 登録ファイル数 START -->
            <tr>
              <th width="100">登録ファイル数</th>
              <td>
                <select name="file_count" onchange="setFileElementsDisplay2();">
                  <{foreach from=$contents key=k item=i}>
                    <option value="<{$k}>" <{if $file_count == $k}>selected<{/if}>><{$k}>
                  <{/foreach}>
                </select>
              </td>
            </tr>
            <!-- 登録ファイル数 END -->
            <!-- 保存先 START -->
            <tr>
              <th class="none">保存先</td>
              <td id="path" class="none" colspan="3">
                <span id="folder_path" style="padding-right:2px;">
                  <{if $category == ""}>
                    選択してください
                  <{else}>
                    <{$cate_nm}>
                    <{foreach name=n from=$folder_path item=fp}>
                      &gt;&nbsp;<{$fp.name}>
                    <{/foreach}>
                  <{/if}>
                </span>
                <input type="button" id="folder_path_btn" class="button radius_mini" class="button radius_mini" value="選択" onclick="selectFolder('<{$session}>', '<{$path}>');">
                <input type="hidden" name="archive" value="<{$archive}>">
                <input type="hidden" name="category" value="<{$category}>">
                <input type="hidden" name="folder_id" value="<{$folder_id}>">
              </td>
            </tr>
            <!-- 保存先 END -->
          </table>
        </div>
        
        <!-- ファイル名-説明x5 START -->
        <{foreach from=$contents key=k item=i}>
          <div class="color02 radius clearfix mB5" id="file<{$k}>" <{if $k > 1}>style="display:none;"<{/if}>>
            <table class="v_title">
              <!-- ファイル名 START -->
              <tr>
                <th width="100">ファイル</th>
                <td colspan="3">
                  <input type="file" name="upfile<{$k}>" size="50" value="" style="ime-mode:inactive;" onchange="setFileInfo(<{$k}>, this.value);">
                </td>
              </tr>
              <!-- ファイル名 END -->

              <!-- コンテンツ名-コンテンツタイプ START -->
              <tr>
                <th>コンテンツ名</th>
                <td>
                  <input name="document_name<{$k}>" type="text" size="50" maxlength="100" value="<{$i.document_name}>" style="ime-mode:active;">
                </td>
                <th>コンテンツタイプ</th>
                <td>
                  <select name="document_type<{$k}>">
                    <option value="0"<{if $i.document_type_var == "0"}> selected<{/if}>>
                    <option value="1"<{if $i.document_type_var == "1"}> selected<{/if}>>Word
                    <option value="2"<{if $i.document_type_var == "2"}> selected<{/if}>>Excel
                    <option value="3"<{if $i.document_type_var == "3"}> selected<{/if}>>PowerPoint
                    <option value="4"<{if $i.document_type_var == "4"}> selected<{/if}>>PDF
                    <option value="5"<{if $i.document_type_var == "5"}> selected<{/if}>>テキスト
                    <option value="6"<{if $i.document_type_var == "6"}> selected<{/if}>>JPEG
                    <option value="7"<{if $i.document_type_var == "7"}> selected<{/if}>>GIF
                    <option value="99"<{if $i.document_type_var == "99"}> selected<{/if}>>その他
                  </select>
                </td>
              </tr>
              <!-- コンテンツ名-コンテンツタイプ START -->

              <!-- キーワード-コンテンツ番号 START -->
              <tr>
                <th>キーワード</th>
                <td>
                  <input name="keywd<{$k}>" type="text" size="50" maxlength="50" value="<{$i.keyword}>" style="ime-mode:active;">
                </td>
                <th class="borderb">コンテンツ番号</th>
                <td>
                  <input type="text" name="lib_no<{$k}>" value="<{$i.lib_no}>" style="ime-mode:inactive;">
                </td>
              </tr>
              <!-- キーワード-コンテンツ番号 END -->

              <!-- 説明 START -->
              <tr>
                <th class="none">説明</th>
                <td class="none" colspan="3">
                  <textarea name="explain<{$k}>" rows="5" style="ime-mode:active;"><{$i.explain}></textarea>
                </td>
              </tr>
              <!-- 説明 END -->
            </table>
          </div>
        <{/foreach}>
        <!-- ファイル名-説明x5 END -->

        <!-- 参照・更新可能職員指定 -->
        <{include file="hiyari_el_emp2.tpl"}>

        <input type="hidden" name="session" value="<{$session}>">
        <input type="hidden" name="o" value="<{$o}>">
        <input type="hidden" name="path" value="<{$path}>">
        <input type="hidden" name="ref_toggle_mode" value="">
        <input type="hidden" name="upd_toggle_mode" value="">
        <input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
        <input type="hidden" id="target_name_list1" name="target_name_list1" value="">
        <input type="hidden" id="target_id_list2"   name="target_id_list2" value="">
        <input type="hidden" id="target_name_list2" name="target_name_list2" value="">
      </form>
    </div>
  </div>
  
  <!-----------------------------------------------------------------
  フッター
  ------------------------------------------------------------------>
  <div id="footer">
    <div class="inner">
      <p class="notice">※<{$upload_max_filesize}>を超えるファイルは登録できません。</p>
      <p class="action_btn">
        <input type="button" class="button radius_mini" class="button radius_mini" value="登録" onclick="submitForm();">
      </p>
    </div>
  </div>
</body>
</html>
