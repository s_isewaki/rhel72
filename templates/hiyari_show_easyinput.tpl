<{*
インターフェース

    $grps
        array   項目情報配列
    $vals
        array   値の配列
    $grp_flags
        array   レポートに使用する項目グループ
    $user_grp_flags
        array   レポートに使用するユーザー定義項目グループ

//  $mail_input_flg
//      boolean メールメッセージ入力を有効にするフラグ
//  $mail_message
//      string  メールメッセージ(表示値/TinyMCE対応)
//  $registration_date
//      string   報告日(表示値)
    $regist_date_y
        string 報告日(年)
    $regist_date_m
        string 報告日(月)
    $regist_date_d
        string 報告日(日)

    $report_title
        string   タイトル(表示値)
    $user_cate_name
        string  ユーザー定義カテゴリ名

    $size
        コメントアウト内でのみ使用。廃止可能。
    $by_self
        コメントアウト内でのみ使用。廃止可能。


使用するサブテンプレート
    hiyari_show_easyinput_item.tpl

*}>

<!-- 全体 -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center" style="padding-right:10px;padding-left:10px;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td>



<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- 報告書情報                                                                                           -->
<!-- ---------------------------------------------------------------------------------------------------- -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
            <tr height="22">
                <td bgcolor="#DFFFDC" width="250"><font size="3" class="j12">表題</font></td>
                <td ><font size="3" class="j12"><{$report_title}></font></td>
                <td width="150" bgcolor="#DFFFDC" width="25%"><font size="3" class="j12">報告日</font></td>
                <td width="150"><font size="3" class="j12"><{$regist_date_y}>/<{$regist_date_m}>/<{$regist_date_d}></font></td>
            </tr>
<!--
<{if $mail_input_flg}>
            <tr height="22">
                <td bgcolor="#DFFFDC"><font size="3" class="j12">セーフティマネージャへのメッセージ</font></td>
                <td colspan="3"><{$mail_message}></td>
            </tr>
<{/if}>
-->
        </table>
    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<br>





<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ８．医療事故情報収集事業の発生件数情報等                                                             -->
<!-- ---------------------------------------------------------------------------------------------------- -->


<{if in_array(1100, $grp_flags) || in_array(1110, $grp_flags) || in_array(1120, $grp_flags)}>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td><font size="3" class="j12"><b>医療事故情報収集事業の発生件数情報等</b></font></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>

        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
        <{if in_array(1100, $grp_flags)}>
                <tr height="22">
                    <td bgcolor="#DFFFDC" width="250px">
                        <font size="3" class="j12">医療の実施の有無</font>
                    </td>
                    <td>
                        <font size="3" class="j12"><{$text_vals[1100][10][0]}></font>
                    </td>
                </tr>
                <tr height="22">
                    <td bgcolor="#DFFFDC" width="250px">
                        <font size="3" class="j12">実施の程度</font>
                    </td>
                    <td>
                        <font size="3" class="j12"><{$text_vals[1100][20][0]}></font>
                    </td>
                </tr>
                <tr height="22">
                    <td bgcolor="#DFFFDC" width="250px">
                        <font size="3" class="j12">事故の程度</font>
                    </td>
                    <td>
                        <font size="3" class="j12"><{$text_vals[1100][30][0]}></font>
                    </td>
                </tr>
                <tr height="22">
                    <td bgcolor="#DFFFDC" width="250px">
                        <font size="3" class="j12">不明の場合</font>
                    </td>
                    <td>
                        <font size="3" class="j12"><{$text_vals[1100][40][0]}></font>
                    </td>
                </tr>
                <tr height="22">
                    <td bgcolor="#DFFFDC" width="250px">
                        <font size="3" class="j12">仮に実施された場合の影響度</font>
                    </td>
                    <td>
                        <font size="3" class="j12"><{$text_vals[1100][50][0]}></font>
                    </td>
                </tr>
        <{/if}>
        <{if in_array(1110, $grp_flags)}>
                <tr height="22">
                    <td bgcolor="#DFFFDC" width="250px">
                        <font size="3" class="j12">特に報告を求める事例</font>
                    </td>
                    <td>
                        <font size="3" class="j12"><{$text_vals[1110][10][0]}></font>
                    </td>
                </tr>
                <tr height="22">
                    <td bgcolor="#DFFFDC" width="250px">
                        <font size="3" class="j12">その他の場合</font>
                    </td>
                    <td>
                        <font size="3" class="j12"><{$text_vals[1110][20][0]}></font>
                    </td>
                </tr>
        <{/if}>
        <{if in_array(1120, $grp_flags)}>
                <tr height="22">
                    <td bgcolor="#DFFFDC" width="250px">
                        <font size="3" class="j12">発生件数集計再掲</font>
                    </td>
                    <td>
                        <font size="3" class="j12"><{$text_vals[1120][10][0]}></font>
                    </td>
                </tr>
        <{/if}>
        </table>

    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<br>
<{/if}>






<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- １．いつ、どこで                                                                                     -->
<!-- ---------------------------------------------------------------------------------------------------- -->


<{if in_array(100, $grp_flags) || in_array(110, $grp_flags) || in_array(105, $grp_flags) || in_array(116, $grp_flags)}>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td><font size="3" class="j12"><b>いつ、どこで</b></font></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>

        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">

            <{assign var="colspan_110_60_f" value="1"}>
            <{if in_array(100, $grp_flags)}>
                <{assign var="colspan_110_60_f" value="3"}>
            <{/if}>
            <{if in_array(105, $grp_flags)}>
                <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=105 j=5  td_title_width="95" td_field_width="220"}>
                    
					
					<{* 2時間単位選択表示の場合 *}>
					<{if $time_setting_flg == true }>
						<{include file="hiyari_show_easyinput_item.tpl" i=105 j=40 td_title_width="95" td_field_width="130"}>

					<{* 時刻表示の場合 *}>
					<{else}>
						<td width="150" bgcolor="#DFFFDC" >
							<font size="3" class="j12">発見時刻</font>
						</td>

						<td width="130">
							<{include file="hiyari_show_easyinput_item_field.tpl" i=105 j=60 }><font size="3" class="j12">時</font>
							<{include file="hiyari_show_easyinput_item_field.tpl" i=105 j=65 }><font size="3" class="j12">分</font>
						</td>

					<{/if}>

					<{include file="hiyari_show_easyinput_item.tpl" i=105 j=50 td_title_width="150" td_field_width=""}>

                </tr>
            <{/if}>
            <{if in_array(100, $grp_flags)}>
                <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=100 j=5  td_title_width="95" td_field_width="220"}>
                    

					<{* 2時間単位選択表示の場合 *}>
					<{if $time_setting_flg == true }>
						<{include file="hiyari_show_easyinput_item.tpl" i=100 j=40 td_title_width="95" td_field_width="130"}>

					<{* 時刻表示の場合 *}>
					<{else}>
						<td width="150" bgcolor="#DFFFDC" >
							<font size="3" class="j12">発生時刻</font>
						</td>

						<td width="130">
							<{include file="hiyari_show_easyinput_item_field.tpl" i=100 j=60 }><font size="3" class="j12">時</font>
							<{include file="hiyari_show_easyinput_item_field.tpl" i=100 j=65 }><font size="3" class="j12">分</font>
						</td>

					<{/if}>

					<{include file="hiyari_show_easyinput_item.tpl" i=100 j=50 td_title_width="150" td_field_width=""}>




                </tr>
            <{/if}>
            
            <{* 発生部署 *}>
            <{if in_array(116, $grp_flags)}>
                <tr height="22">
                    <td width="95" bgcolor="#DFFFDC">
                        <{include file="hiyari_show_easyinput_item_title.tpl" i=116 j=10}>
                    </td>
                    
                    <td width="450"   <{if $colspan_110_60_f > 1}>colspan="<{$colspan_110_60_f}>"<{/if}> >
                        <{include file="hiyari_show_easyinput_item_field.tpl" i=116 j=10 }>
                    </td>
                    
                    <{include file="hiyari_show_easyinput_item.tpl" i=116 j=15 td_title_width="150" td_field_width=""}>
                </tr>
            <{/if}>
            
            <{* 発生場所 *}>
            <{if in_array(110, $grp_flags)}>
                <tr height="22">
                    <td width="95" bgcolor="#DFFFDC">
                        <{include file="hiyari_show_easyinput_item_title.tpl" i=110 j=60}>
                    </td>
                    <td width="450"   <{if $colspan_110_60_f > 1}>colspan="<{$colspan_110_60_f}>"<{/if}> >
                        <{include file="hiyari_show_easyinput_item_field.tpl" i=110 j=60 }>
                        <{if $vals[110][65] != ""}>
                        (<{include file="hiyari_show_easyinput_item_field.tpl" i=110 j=65 }>)
                        <{/if}>
                    </td>
                    <{include file="hiyari_show_easyinput_item.tpl" i=110 j=70 td_title_width="150" td_field_width=""}>
                </tr>
            <{/if}>
        </table>

    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<br>
<{/if}>










<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ２．誰が                                                                                             -->
<!-- ---------------------------------------------------------------------------------------------------- -->

<{if in_array(3000, $grp_flags) || array_intersect(array(3050, 3100, 3150, 3200, 3250, 3300, 3350, 3400, 3450), $grp_flags) || in_array(3500, $grp_flags) || in_array(3020, $grp_flags) || array_intersect(array(3030, 3032, 3034, 3036, 3038), $grp_flags)}>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td><font size="3" class="j12"><b>誰が</b></font></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>

        <{if in_array(3000, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=3000 j=10 td_title_width="250" td_field_width="300"}>
                    <{include file="hiyari_show_easyinput_item.tpl" i=3000 j=20 td_title_width="150" td_field_width=""}>
                </tr>
            </table>
        <{/if}>

        <{if in_array(3020, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                  <{if $style_code == 1}>
                    <{include file="hiyari_show_easyinput_item.tpl" i=3020 j=10 td_title_width="150" td_field_width=""}>
                  <{else}>  
                    <{include file="hiyari_show_easyinput_item.tpl" i=3020 j=10 td_title_width="250" td_field_width="300"}>
                    <{include file="hiyari_show_easyinput_item.tpl" i=3020 j=20 td_title_width="150" td_field_width=""}>
                  <{/if}>
                </tr>
            </table>
        <{/if}>

        <{if array_intersect(array(3030, 3032, 3034, 3036, 3038), $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                            <{if in_array(3030, $grp_flags)}>
                            <tr height="22">
                                <{include file="hiyari_show_easyinput_item.tpl" i=3030 j=10}>
                            </tr>
                            <{/if}>
                            <{if in_array(3032, $grp_flags)}>
                            <tr height="22">
                                <{include file="hiyari_show_easyinput_item.tpl" i=3032 j=10}>
                            </tr>
                            <{/if}>
                            <{if in_array(3034, $grp_flags)}>
                            <tr height="22">
                                <{include file="hiyari_show_easyinput_item.tpl" i=3034 j=10}>
                            </tr>
                            <{/if}>
                            <{if in_array(3036, $grp_flags)}>
                            <tr height="22">
                                <{include file="hiyari_show_easyinput_item.tpl" i=3036 j=10}>
                            </tr>
                            <{/if}>
                            <{if in_array(3036, $grp_flags)}>
                            <tr height="22">
                                <{include file="hiyari_show_easyinput_item.tpl" i=3036 j=20}>
                            </tr>
                            <{/if}>
                            <{if in_array(3038, $grp_flags)}>
                            <tr height="22">
                                <{include file="hiyari_show_easyinput_item.tpl" i=3038 j=10}>
                            </tr>
                            <{/if}>
                            <{if in_array(3038, $grp_flags)}>
                            <tr height="22">
                                <{include file="hiyari_show_easyinput_item.tpl" i=3038 j=20}>
                            </tr>
                            <{/if}>
                        </table>
                    </td>
                </tr>
            </table>
        <{/if}>

        <{if in_array(3000, $grp_flags) && (array_intersect(array(3050, 3100, 3150, 3200, 3250, 3300, 3350, 3400, 3450), $grp_flags) || in_array(3500, $grp_flags))}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if array_intersect(array(3050, 3100, 3150, 3200, 3250, 3300, 3350, 3400, 3450), $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <{assign var="parties_count" value=10}>
                <{section name=parties_index loop=$parties_count}>
                    <{assign var="index" value=$smarty.section.parties_index.index}>

                    <{if array_intersect(array(3050 + $index, 3100 + $index, 3150 + $index, 3200 + $index, 3250 + $index, 3300 + $index, 3350 + $index, 3400 + $index, 3450 + $index), $grp_flags)}>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                                <tr height="22">
                                    <td bgcolor="#DFFFDC" colspan="2"><font size="3" class="j12">当事者<{$index+1}></font></td>
                                </tr>
                                <{if in_array(3050 + $index, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=$index+3050 j=30}>
                                </tr>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=$index+3050 j=35}>
                                </tr>
                                <{/if}>
                                <{if in_array(3100 + $index, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=$index+3100 j=40}>
                                </tr>
                                <{/if}>
                                <{if in_array(3150 + $index, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=$index+3150 j=50}>
                                </tr>
                                <{/if}>
                                <{if in_array(3200 + $index, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=$index+3200 j=60}>
                                </tr>
                                <{/if}>
                                <{if in_array(3250 + $index, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=$index+3250 j=70}>
                                </tr>
                                <{/if}>
                                <{if in_array(3250 + $index, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=$index+3250 j=80}>
                                </tr>
                                <{/if}>
                                <{if in_array(3300 + $index, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=$index+3300 j=90}>
                                </tr>
                                <{/if}>
                                <{if in_array(3300 + $index, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=$index+3300 j=100}>
                                </tr>
                                <{/if}>
                                <{if in_array(3350 + $index, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=$index+3350 j=110}>
                                </tr>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=$index+3350 j=115}>
                                </tr>
                                <{/if}>
                                <{if in_array(3400 + $index, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=$index+3400 j=120}>
                                </tr>
                                <{/if}>
                                <{if in_array(3400 + $index, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=$index+3400 j=130}>
                                </tr>
                                <{/if}>
                                <{if in_array(3450 + $index, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=$index+3450 j=140}>
                                </tr>
                                <{/if}>
                            </table>
                        </td>
                    </tr>
                    <{/if}>
                <{/section}>
            </table>
        <{/if}>

        <{if array_intersect(array(3050, 3100, 3150, 3200, 3250, 3300, 3350, 3400, 3450), $grp_flags) && in_array(3500, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(3500, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=3500 j=150}>
                </tr>
                <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=3500 j=160}>
                </tr>
            </table>
        <{/if}>

    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<br>
<{/if}>






<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ３．誰に                                                                                             -->
<!-- ---------------------------------------------------------------------------------------------------- -->


<{if in_array(200, $grp_flags) || in_array(210, $grp_flags) || in_array(230, $grp_flags) || in_array(240, $grp_flags) || in_array(243, $grp_flags) || in_array(246, $grp_flags) || in_array(250, $grp_flags) || in_array(260, $grp_flags) || in_array(270, $grp_flags) || in_array(280, $grp_flags) || in_array(290, $grp_flags) || in_array(1200, $grp_flags)}>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td><font size="3" class="j12"><b>誰に</b></font></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>

        <{if in_array(200, $grp_flags) || in_array(210, $grp_flags) || in_array(230, $grp_flags) || in_array(240, $grp_flags) || in_array(243, $grp_flags) || in_array(246, $grp_flags) || in_array(250, $grp_flags) || in_array(260, $grp_flags) || in_array(270, $grp_flags) || in_array(280, $grp_flags) || in_array(1200, $grp_flags)}>

            <{if in_array(210, $grp_flags)}>

                <{assign var="colspan_210_50_count"  value="0"}>
                <{if in_array(30, $patient_use) && in_array(40, $patient_use) && in_array(50, $patient_use)}>
                    <{assign var="colspan_210_50_count"  value="2"}>
                <{/if}>

                <{*患者の数*}>
                <{if in_array(200, $grp_flags)}>
                    <{assign var="colspan_200_10_f"  value="1"}>
                    <{assign var="colspan_200_20_f"  value=""}>
                    <{php}>$this->_tpl_vars['colspan_200_20_f'] = $this->_tpl_vars['colspan_210_50_count']+1;<{/php}>
                <{/if}>

                <{*患者プロフィール(患者ID、患者氏名)*}>
                <{assign var="colspan_210_10_f"  value="1"}>
                <{assign var="colspan_210_20_f"  value="1"}>
                <{if in_array(10, $patient_use) && in_array(20, $patient_use)}>

                    <{php}>$this->_tpl_vars['colspan_210_20_f'] = $this->_tpl_vars['colspan_210_50_count']+1;<{/php}>

                <{elseif in_array(10, $patient_use) && !in_array(20, $patient_use)}>

                    <{php}>$this->_tpl_vars['colspan_210_10_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>

                <{elseif !in_array(10, $patient_use) && in_array(20, $patient_use)}>

                    <{php}>$this->_tpl_vars['colspan_210_20_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>

                <{/if}>

                <{*患者プロフィール(患者の性別、患者の年数、患者の月数)*}>
                <{assign var="colspan_210_30_f"  value="1"}>
                <{assign var="colspan_210_40_f"  value="1"}>
                <{assign var="colspan_210_50_f"  value="1"}>

                <{if in_array(30, $patient_use) && !in_array(40, $patient_use) && !in_array(50, $patient_use)}>

                    <{php}>$this->_tpl_vars['colspan_210_30_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>

                <{elseif !in_array(30, $patient_use) && in_array(40, $patient_use) && !in_array(50, $patient_use)}>

                    <{php}>$this->_tpl_vars['colspan_210_40_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>

                <{elseif !in_array(30, $patient_use) && !in_array(40, $patient_use) && in_array(50, $patient_use)}>

                    <{php}>$this->_tpl_vars['colspan_210_50_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>

                <{/if}>

                <{*患者区分１、患者区分２*}>
                <{assign var="colspan_230_60_f"  value="1"}>
                <{assign var="colspan_240_70_f"  value="1"}>
                <{if in_array(230, $grp_flags) && in_array(240, $grp_flags)}>
                    <{assign var="colspan_230_60_f"  value="1"}>
                    <{php}>$this->_tpl_vars['colspan_240_70_f'] = $this->_tpl_vars['colspan_210_50_count']+1;<{/php}>
                <{elseif in_array(230, $grp_flags)}>
                    <{php}>$this->_tpl_vars['colspan_230_60_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>
                <{elseif in_array(240, $grp_flags)}>
                    <{php}>$this->_tpl_vars['colspan_240_70_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>
                <{/if}>

                <{*主治医*}>
                <{assign var="colspan_243_73_f"  value=""}>
                <{php}>$this->_tpl_vars['colspan_243_73_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>

                <{*入院日*}>
                <{assign var="colspan_246_76_f"  value=""}>
                <{php}>$this->_tpl_vars['colspan_246_76_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>


                <{*インシデントに直接関連する疾患名、関連する疾患名１*}>
                <{assign var="colspan_250_80_f"  value="1"}>
                <{assign var="colspan_260_90_f"  value="1"}>
                <{if in_array(250, $grp_flags) && in_array(260, $grp_flags)}>
                    <{php}>$this->_tpl_vars['colspan_260_90_f'] = $this->_tpl_vars['colspan_210_50_count']+1;<{/php}>
                <{elseif in_array(250, $grp_flags)}>
                    <{php}>$this->_tpl_vars['colspan_250_80_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>
                <{elseif in_array(260, $grp_flags)}>
                    <{php}>$this->_tpl_vars['colspan_260_90_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>
                <{/if}>

                <{*関連する疾患名２、関連する疾患名３*}>
                <{assign var="colspan_270_100_f" value="1"}>
                <{assign var="colspan_280_110_f" value="1"}>
                <{if in_array(270, $grp_flags) && in_array(280, $grp_flags)}>
                    <{php}>$this->_tpl_vars['colspan_280_110_f'] = $this->_tpl_vars['colspan_210_50_count']+1;<{/php}>
                <{elseif in_array(270, $grp_flags)}>
                    <{php}>$this->_tpl_vars['colspan_270_100_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>
                <{elseif in_array(280, $grp_flags)}>
                    <{php}>$this->_tpl_vars['colspan_280_110_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>
                <{/if}>

            <{elseif in_array(200, $grp_flags) || (in_array(230, $grp_flags) && in_array(240, $grp_flags)) || (in_array(250, $grp_flags) && in_array(260, $grp_flags)) || (in_array(270, $grp_flags) && in_array(280, $grp_flags))}>

                <{if in_array(200, $grp_flags)}>
                    <{assign var="colspan_200_10_f"  value="1"}>
                    <{assign var="colspan_200_20_f"  value="1"}>
                <{/if}>

                <{if in_array(230, $grp_flags) && in_array(240, $grp_flags)}>
                    <{assign var="colspan_230_60_f"  value="1"}>
                    <{assign var="colspan_240_70_f"  value="1"}>
                <{elseif in_array(230, $grp_flags)}>
                    <{assign var="colspan_230_60_f"  value="3"}>
                <{elseif in_array(240, $grp_flags)}>
                    <{assign var="colspan_240_70_f"  value="3"}>
                <{/if}>

                <{assign var="colspan_243_73_f"  value="3"}>

                <{assign var="colspan_246_76_f"  value="3"}>

                <{if in_array(250, $grp_flags) && in_array(260, $grp_flags)}>
                    <{assign var="colspan_250_80_f"  value="1"}>
                    <{assign var="colspan_260_90_f"  value="1"}>
                <{elseif in_array(250, $grp_flags)}>
                    <{assign var="colspan_250_80_f"  value="3"}>
                <{elseif in_array(260, $grp_flags)}>
                    <{assign var="colspan_260_90_f"  value="3"}>
                <{/if}>

                <{if in_array(270, $grp_flags) && in_array(280, $grp_flags)}>
                    <{assign var="colspan_270_100_f" value="1"}>
                    <{assign var="colspan_280_110_f" value="1"}>
                <{elseif in_array(270, $grp_flags)}>
                    <{assign var="colspan_270_100_f" value="3"}>
                <{elseif in_array(280, $grp_flags)}>
                    <{assign var="colspan_280_110_f" value="3"}>
                <{/if}>

            <{else}>
                <{assign var="colspan_230_60_f"  value="1"}>
                <{assign var="colspan_240_70_f"  value="1"}>

                <{assign var="colspan_243_73_f"  value="1"}>

                <{assign var="colspan_246_76_f"  value="1"}>

                <{assign var="colspan_250_80_f"  value="1"}>
                <{assign var="colspan_260_90_f"  value="1"}>

                <{assign var="colspan_270_100_f" value="1"}>
                <{assign var="colspan_280_110_f" value="1"}>
            <{/if}>

            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">

                <{if in_array(200, $grp_flags)}>
                    <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=200 j=10 td_title_width="200" td_field_width="250" colspan=$colspan_200_10_f}>
                        <{include file="hiyari_show_easyinput_item.tpl" i=200 j=20 td_title_width="150" td_field_width="200" colspan=$colspan_200_20_f}>
                    </tr>
                <{/if}>

                <{*関連する患者情報の数*}>
                <{if array_intersect(array(310,320,330,340,350,360,370,380,390), $grp_flags)}>
                    <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=200 j=30 colspan=$colspan_243_73_f}>
                    </tr>                
                <{/if}>
                
                <{if in_array(210, $grp_flags)}>
                    <{if in_array(10, $patient_use) || in_array(20, $patient_use)}>
                        <tr height="22">
                            <{if in_array(10, $patient_use)}>
                                <{include file="hiyari_show_easyinput_item.tpl" i=210 j=10 td_title_width="200" td_field_width="250" colspan=$colspan_210_10_f}>
                            <{/if}>
                            <{if in_array(20, $patient_use)}>
                                <{include file="hiyari_show_easyinput_item.tpl" i=210 j=20 td_title_width="150" td_field_width="200" colspan=$colspan_210_20_f}>
                            <{/if}>
                        </tr>
                    <{/if}>
                    <{if in_array(30, $patient_use) || in_array(40, $patient_use) || in_array(50, $patient_use)}>
                        <tr height="22">
                            <{if in_array(30, $patient_use)}>
                                <{include file="hiyari_show_easyinput_item.tpl" i=210 j=30 td_title_width="200" td_field_width="250" colspan=$colspan_210_30_f}>
                            <{/if}>
                            <{if in_array(40, $patient_use)}>
                                <{include file="hiyari_show_easyinput_item.tpl" i=210 j=40 td_title_width="150" td_field_width="75" colspan=$colspan_210_40_f}>
                            <{/if}>
                            <{if in_array(50, $patient_use)}>
                                <{include file="hiyari_show_easyinput_item.tpl" i=210 j=50 td_title_width="150" td_field_width="135" colspan=$colspan_210_50_f}>
                            <{/if}>
                        </tr>
                    <{/if}>
                <{/if}>

                <{if in_array(230, $grp_flags) || in_array(240, $grp_flags)}>
                    <tr height="22">
                        <{if in_array(230, $grp_flags) && in_array(240, $grp_flags)}>
                            <{include file="hiyari_show_easyinput_item.tpl" i=230 j=60 td_title_width="200" td_field_width="250" colspan=$colspan_230_60_f}>
                            <{include file="hiyari_show_easyinput_item.tpl" i=240 j=70 td_title_width="150" td_field_width="200" colspan=$colspan_240_70_f}>
                        <{elseif in_array(230, $grp_flags)}>
                            <{include file="hiyari_show_easyinput_item.tpl" i=230 j=60 td_title_width="200" td_field_width="200" colspan=$colspan_230_60_f}>
                        <{elseif in_array(240, $grp_flags)}>
                            <{include file="hiyari_show_easyinput_item.tpl" i=240 j=70 td_title_width="200" td_field_width="200" colspan=$colspan_240_70_f}>
                        <{/if}>
                    </tr>
                <{/if}>


                <{if in_array(243, $grp_flags)}>
                    <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=243 j=73 colspan=$colspan_243_73_f}>
                    </tr>
                <{/if}>


                <{if in_array(246, $grp_flags)}>
                    <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=246 j=76 colspan=$colspan_246_76_f}>
                    </tr>
                <{/if}>

                <{if in_array(1200, $grp_flags)}>
                    <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=1200 j=10}>
                    </tr>
                <{/if}>

                <{if in_array(250, $grp_flags) || in_array(260, $grp_flags)}>
                    <tr height="22">
                        <{if in_array(250, $grp_flags) && in_array(260, $grp_flags)}>
                            <{include file="hiyari_show_easyinput_item.tpl" i=250 j=80 td_title_width="200" td_field_width="250" colspan=$colspan_250_80_f}>
                            <{include file="hiyari_show_easyinput_item.tpl" i=260 j=90 td_title_width="150" td_field_width="200" colspan=$colspan_260_90_f}>
                        <{elseif in_array(250, $grp_flags)}>
                            <{include file="hiyari_show_easyinput_item.tpl" i=250 j=80 td_title_width="200" td_field_width="200" colspan=$colspan_250_80_f}>
                        <{elseif in_array(260, $grp_flags)}>
                            <{include file="hiyari_show_easyinput_item.tpl" i=260 j=90 td_title_width="200" td_field_width="200" colspan=$colspan_260_90_f}>
                        <{/if}>
                    </tr>
                <{/if}>

                <{if in_array(270, $grp_flags) || in_array(280, $grp_flags)}>
                    <tr height="22">
                        <{if in_array(270, $grp_flags) && in_array(280, $grp_flags)}>
                            <{include file="hiyari_show_easyinput_item.tpl" i=270 j=100 td_title_width="200" td_field_width="250" colspan=$colspan_270_100_f}>
                            <{include file="hiyari_show_easyinput_item.tpl" i=280 j=110 td_title_width="150" td_field_width="" colspan=$colspan_280_110_f}>
                        <{elseif in_array(270, $grp_flags)}>
                            <{include file="hiyari_show_easyinput_item.tpl" i=270 j=100 td_title_width="200" td_field_width="200" colspan=$colspan_270_100_f}>
                        <{elseif in_array(280, $grp_flags)}>
                            <{include file="hiyari_show_easyinput_item.tpl" i=280 j=110 td_title_width="200" td_field_width="200" colspan=$colspan_280_110_f}>
                        <{/if}>
                    </tr>
                <{/if}>
            </table>
            
            <{if array_intersect(array(310,320,330,340,350,360,370,380,390), $grp_flags)}>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
                </table>
            <{/if}>
        <{/if}>


        <{*関連する患者情報*}>
        <{section name=patients_index loop=10}>
          <{assign var="index" value=$smarty.section.patients_index.index}>
          <{if array_intersect(array(310+$index,320+$index,330+$index,340+$index,350+$index,360+$index,370+$index,380+$index,390+$index), $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">              
              <tbody>
                <tr height="22">
                  <td bgcolor="#DFFFDC" colspan="2"><font size="3" class="j12">関連する患者情報<{$index+1}></font></td>
                </tr>
                <{section name=groups start=310 step=10 loop=400}>                      
                  <{if in_array($smarty.section.groups.index+$index, $grp_flags)}>
                    <tr height="22">
                      <{include file="hiyari_show_easyinput_item.tpl" i=$smarty.section.groups.index+$index j=10}>
                    </tr>
                  <{/if}>                    
                <{/section}>                  
              </tbody>
            </table>
          <{/if}>
        <{/section}>
        
        <{if array_intersect(array(310,320,330,340,350,360,370,380,390,290), $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>
        
        <{if in_array(290, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=290 j=120}>
                </tr>
                <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=290 j=130}>
                </tr>
                <{if in_array(295, $grp_flags)}>
                    <{foreach from=$grps[295].easy_item_list key="easy_item_code_295" item="tmp_295_data"}>
                        <{if count($vals[295][$easy_item_code_295]) > 0 && count($grps[295].easy_item_list[$easy_item_code_295].easy_list) > 1}>
                            <tr height="22">
                                <{include file="hiyari_show_easyinput_item.tpl" i=295 j=$easy_item_code_295}>
                            </tr>
                        <{/if}>
                    <{/foreach}>
                <{/if}>
            </table>
        <{/if}>

    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<br>
<{/if}>











<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ４．何をどうした                                                               -->
<!-- ---------------------------------------------------------------------------------------------------- -->

<{if in_array(120, $grp_flags) || in_array(130, $grp_flags) || in_array(140, $grp_flags) || in_array(700, $grp_flags) || in_array(900, $grp_flags) || in_array(1000, $grp_flags) || in_array(1300, $grp_flags) || in_array(1310, $grp_flags) || in_array(1320, $grp_flags)}>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td><font size="3" class="j12"><b>何をどうした</b></font></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>

        <{if in_array(120, $grp_flags) || in_array(400, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">

            <{if in_array(120, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=120 j=80}>
                </tr>
                <{if in_array(400, $grp_flags)}>

                    <{if in_array(10, $eis_400_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=400 j=10}>
                        </tr>
                    <{/if}>

                    <{if in_array(20, $eis_400_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=400 j=20}>
                        </tr>
                    <{/if}>

                    <{if in_array(30, $eis_400_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=400 j=30}>
                        </tr>
                    <{/if}>

                <{/if}>
                <{if in_array(410, $grp_flags)}>

                    <{if in_array(10, $eis_410_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=410 j=10}>
                        </tr>
                    <{/if}>

                    <{if in_array(15, $eis_410_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=410 j=15}>
                        </tr>
                    <{/if}>

                    <{if in_array(20, $eis_410_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=410 j=20}>
                        </tr>
                    <{/if}>

                    <{if in_array(25, $eis_410_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=410 j=25}>
                        </tr>
                    <{/if}>

                    <{if in_array(30, $eis_410_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=410 j=30}>
                        </tr>
                    <{/if}>

                    <{if in_array(40, $eis_410_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=410 j=40}>
                        </tr>
                    <{/if}>

                    <{if in_array(50, $eis_410_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=410 j=50}>
                        </tr>
                    <{/if}>

                    <{if in_array(51, $eis_410_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=410 j=51}>
                        </tr>
                    <{/if}>

                    <{if in_array(52, $eis_410_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=410 j=52}>
                        </tr>
                    <{/if}>

                    <{if in_array(53, $eis_410_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=410 j=53}>
                        </tr>
                    <{/if}>

                    <{if in_array(60, $eis_410_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=410 j=60}>
                        </tr>
                    <{/if}>

                    <{if in_array(61, $eis_410_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=410 j=61}>
                        </tr>
                    <{/if}>

                    <{if in_array(62, $eis_410_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=410 j=62}>
                        </tr>
                    <{/if}>

                    <{if in_array(63, $eis_410_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=410 j=63}>
                        </tr>
                    <{/if}>

                <{/if}>
                <{if in_array(420, $grp_flags)}>

                    <{if in_array(10, $eis_420_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=420 j=10}>
                        </tr>
                    <{/if}>

                    <{if in_array(20, $eis_420_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=420 j=20}>
                        </tr>
                    <{/if}>

                    <{if in_array(30, $eis_420_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=420 j=30}>
                        </tr>
                    <{/if}>

                <{/if}>
                <{if in_array(430, $grp_flags)}>

                    <{if in_array(10, $eis_430_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=430 j=10}>
                        </tr>
                    <{/if}>

                    <{if in_array(20, $eis_430_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=430 j=20}>
                        </tr>
                    <{/if}>

                    <{if in_array(30, $eis_430_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=430 j=30}>
                        </tr>
                    <{/if}>

                    <{if in_array(40, $eis_430_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=430 j=40}>
                        </tr>
                    <{/if}>

                    <{if in_array(41, $eis_430_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=430 j=41}>
                        </tr>
                    <{/if}>

                    <{if in_array(42, $eis_430_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=430 j=42}>
                        </tr>
                    <{/if}>

                <{/if}>
                <{if in_array(440, $grp_flags)}>

                    <{if in_array(10, $eis_440_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=440 j=10}>
                        </tr>
                    <{/if}>

                    <{if in_array(20, $eis_440_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=440 j=20}>
                        </tr>
                    <{/if}>

                    <{if in_array(30, $eis_440_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=440 j=30}>
                        </tr>
                    <{/if}>

                    <{if in_array(40, $eis_440_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=440 j=40}>
                        </tr>
                    <{/if}>

                    <{if in_array(41, $eis_440_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=440 j=41}>
                        </tr>
                    <{/if}>

                    <{if in_array(42, $eis_440_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=440 j=42}>
                        </tr>
                    <{/if}>

                    <{if in_array(43, $eis_440_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=440 j=43}>
                        </tr>
                    <{/if}>

                    <{if in_array(44, $eis_440_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=440 j=44}>
                        </tr>
                    <{/if}>

                <{/if}>
                <{if in_array(450, $grp_flags)}>

                    <{if in_array(10, $eis_450_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=450 j=10}>
                        </tr>
                    <{/if}>

                    <{if in_array(20, $eis_450_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=450 j=20}>
                        </tr>
                    <{/if}>

                    <{if in_array(30, $eis_450_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=450 j=30}>
                        </tr>
                    <{/if}>

                    <{if in_array(40, $eis_450_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=450 j=40}>
                        </tr>
                    <{/if}>

                    <{if in_array(41, $eis_450_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=450 j=41}>
                        </tr>
                    <{/if}>

                    <{if in_array(42, $eis_450_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=450 j=42}>
                        </tr>
                    <{/if}>

                <{/if}>
                <{if in_array(460, $grp_flags)}>

                    <{if in_array(10, $eis_460_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=460 j=10}>
                        </tr>
                    <{/if}>

                    <{if in_array(20, $eis_460_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=460 j=20}>
                        </tr>
                    <{/if}>

                    <{if in_array(30, $eis_460_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=460 j=30}>
                        </tr>
                    <{/if}>

                    <{if in_array(40, $eis_460_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=460 j=40}>
                        </tr>
                    <{/if}>

                    <{if in_array(41, $eis_460_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=460 j=41}>
                        </tr>
                    <{/if}>

                    <{if in_array(42, $eis_460_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=460 j=42}>
                        </tr>
                    <{/if}>

                    <{if in_array(43, $eis_460_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=460 j=43}>
                        </tr>
                    <{/if}>

                    <{if in_array(44, $eis_460_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=460 j=44}>
                        </tr>
                    <{/if}>

                    <{if in_array(50, $eis_460_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=460 j=50}>
                        </tr>
                    <{/if}>

                    <{if in_array(51, $eis_460_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=460 j=51}>
                        </tr>
                    <{/if}>

                    <{if in_array(52, $eis_460_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=460 j=52}>
                        </tr>
                    <{/if}>

                <{/if}>
                <{if in_array(470, $grp_flags)}>

                    <{if in_array(10, $eis_470_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=470 j=10}>
                        </tr>
                    <{/if}>

                    <{if in_array(20, $eis_470_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=470 j=20}>
                        </tr>
                    <{/if}>

                    <{if in_array(30, $eis_470_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=470 j=30}>
                        </tr>
                    <{/if}>

                    <{if in_array(40, $eis_470_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=470 j=40}>
                        </tr>
                    <{/if}>

                    <{if in_array(41, $eis_470_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=470 j=41}>
                        </tr>
                    <{/if}>

                    <{if in_array(42, $eis_470_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=470 j=42}>
                        </tr>
                    <{/if}>

                    <{if in_array(43, $eis_470_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=470 j=43}>
                        </tr>
                    <{/if}>

                    <{if in_array(44, $eis_470_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=470 j=44}>
                        </tr>
                    <{/if}>

                    <{if in_array(50, $eis_470_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=470 j=50}>
                        </tr>
                    <{/if}>

                    <{if in_array(51, $eis_470_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=470 j=51}>
                        </tr>
                    <{/if}>

                    <{if in_array(52, $eis_470_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=470 j=52}>
                        </tr>
                    <{/if}>

                <{/if}>
                <{if in_array(480, $grp_flags)}>

                    <{if in_array(10, $eis_480_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=480 j=10}>
                        </tr>
                    <{/if}>

                    <{if in_array(20, $eis_480_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=480 j=20}>
                        </tr>
                    <{/if}>

                    <{if in_array(30, $eis_480_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=480 j=30}>
                        </tr>
                    <{/if}>

                    <{if in_array(40, $eis_480_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=480 j=40}>
                        </tr>
                    <{/if}>

                    <{if in_array(41, $eis_480_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=480 j=41}>
                        </tr>
                    <{/if}>

                    <{if in_array(42, $eis_480_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=480 j=42}>
                        </tr>
                    <{/if}>

                <{/if}>
                <{if in_array(490, $grp_flags)}>

                    <{if in_array(10, $eis_490_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=490 j=10}>
                        </tr>
                    <{/if}>

                    <{if in_array(20, $eis_490_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=490 j=20}>
                        </tr>
                    <{/if}>

                    <{if in_array(30, $eis_490_use)}>
                        <tr height="22">
                        <{include file="hiyari_show_easyinput_item.tpl" i=490 j=30}>
                        </tr>
                    <{/if}>
                <{/if}>
            <{/if}>
            </table>
        <{/if}>

        <{if in_array(700, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=700 j=10}>
                </tr>
        <{/if}>

        <{if in_array(710, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=710 j=10}>
                </tr>
        <{/if}>

        <{if in_array(720, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=720 j=10}>
                </tr>
        <{/if}>

        <{if in_array(730, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=730 j=10}>
                </tr>
        <{/if}>

        <{if in_array(740, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=740 j=10}>
                </tr>
        <{/if}>

        <{if in_array(750, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=750 j=10}>
                </tr>
        <{/if}>

        <{if in_array(760, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=760 j=10}>
                </tr>
        <{/if}>

        <{if in_array(770, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=770 j=10}>
                </tr>
        <{/if}>

        <{if in_array(780, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=780 j=10}>
                </tr>
        <{/if}>

        <{if in_array(790, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=790 j=10}>
                </tr>
        <{/if}>

        <{if in_array(700, $grp_flags)}>
            </table>
        <{/if}>

        <{if in_array(800, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <{if in_array(5, $item_show_flags)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=800 j=5}>
                    </tr>
                <{/if}>
                <{if in_array(10, $item_show_flags)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=800 j=10}>
                    </tr>
                <{/if}>
                <{if in_array(15, $item_show_flags)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=800 j=15}>
                    </tr>
                <{/if}>
                <{if in_array(20, $item_show_flags)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=800 j=20}>
                    </tr>
                <{/if}>
                <{if in_array(25, $item_show_flags)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=800 j=25}>
                    </tr>
                <{/if}>
                <{if in_array(30, $item_show_flags)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=800 j=30}>
                    </tr>
                <{/if}>
                <{if in_array(35, $item_show_flags)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=800 j=35}>
                    </tr>
                <{/if}>
                <{if in_array(40, $item_show_flags)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=800 j=40}>
                    </tr>
                <{/if}>
                <{if in_array(45, $item_show_flags)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=800 j=45}>
                    </tr>
                <{/if}>
                <{if in_array(50, $item_show_flags)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=800 j=50}>
                    </tr>
                <{/if}>
                <{if in_array(55, $item_show_flags)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=800 j=55}>
                    </tr>
                <{/if}>
                <{if in_array(60, $item_show_flags)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=800 j=60}>
                    </tr>
                <{/if}>
                <{if in_array(65, $item_show_flags)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=800 j=65}>
                    </tr>
                <{/if}>
                <{if in_array(70, $item_show_flags)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=800 j=70}>
                    </tr>
                <{/if}>
            </table>
        <{/if}>

        <{if in_array(900, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=900 j=10}>
                </tr>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=900 j=20}>
                </tr>
        <{/if}>

        <{if in_array(910, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=910 j=10}>
                </tr>
        <{/if}>

        <{if in_array(920, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=920 j=10}>
                </tr>
        <{/if}>

        <{if in_array(930, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=930 j=10}>
                </tr>
        <{/if}>

        <{if in_array(940, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=940 j=10}>
                </tr>
        <{/if}>

        <{if in_array(950, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=950 j=10}>
                </tr>
        <{/if}>

        <{if in_array(960, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=960 j=10}>
                </tr>
        <{/if}>

        <{if in_array(970, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=970 j=10}>
                </tr>
        <{/if}>

        <{if in_array(980, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=980 j=10}>
                </tr>
        <{/if}>

        <{if in_array(990, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=990 j=10}>
                </tr>
        <{/if}>

        <{if in_array(900, $grp_flags)}>
            </table>
        <{/if}>

        <{if in_array(900, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <{if in_array(5, $item_show_flags_2010)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1000 j=5}>
                    </tr>
                <{/if}>
                <{if in_array(10, $item_show_flags_2010)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1000 j=10}>
                    </tr>
                <{/if}>
                <{if in_array(15, $item_show_flags_2010)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1000 j=15}>
                    </tr>
                <{/if}>
                <{if in_array(20, $item_show_flags_2010)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1000 j=20}>
                    </tr>
                <{/if}>
                <{if in_array(25, $item_show_flags_2010)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1000 j=25}>
                    </tr>
                <{/if}>
                <{if in_array(30, $item_show_flags_2010)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1000 j=30}>
                    </tr>
                <{/if}>
                <{if in_array(35, $item_show_flags_2010)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1000 j=35}>
                    </tr>
                <{/if}>
                <{if in_array(40, $item_show_flags_2010)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1000 j=40}>
                    </tr>
                <{/if}>
                <{if in_array(45, $item_show_flags_2010)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1000 j=45}>
                    </tr>
                <{/if}>
                <{if in_array(50, $item_show_flags_2010)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1000 j=50}>
                    </tr>
                <{/if}>
                <{if in_array(55, $item_show_flags_2010)}>
                    <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1000 j=55}>
                    </tr>
                <{/if}>
            </table>
        <{/if}>

        <{if in_array(1300, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=1300 j=10}>
                </tr>
            </table>
        <{/if}>

        <{if in_array(1310, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="3" alt=""></td></tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=1310 j=10}>
                </tr>
            </table>
        <{/if}>

        <{if in_array(1320, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="3" alt=""></td></tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=1320 j=10}>
                </tr>
            </table>
        <{/if}>

        <{if in_array(120, $grp_flags) && in_array(140, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

<{* アセスメント・患者の状態 20090304 *}>
        <{if in_array(140, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=140 j=10}>
                </tr>
            </table>
        <{/if}>

        <{if (in_array(120, $grp_flags) || in_array(140, $grp_flags)) && in_array(130, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(130, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=130 j=90}>
                </tr>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=130 j=100}>
                </tr>
            </table>
        <{/if}>




    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<br>
<{/if}>
















<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ５．インシデントの内容                                                                               -->
<!-- ---------------------------------------------------------------------------------------------------- -->


<{if in_array(500, $grp_flags) || in_array(520, $grp_flags) || in_array(525, $grp_flags) || in_array(530, $grp_flags) || in_array(570, $grp_flags) || in_array(1400, $grp_flags) || in_array(4000, $grp_flags)}>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr><td><font size="3" class="j12"><b>インシデントの内容</b></font></td></tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
      </tr>
      <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td>
        <{if in_array(500, $grp_flags) || in_array(520, $grp_flags) || in_array(525, $grp_flags) || in_array(530, $grp_flags)}>

            <{assign var="ta1_array_list" value=""}>
            <{php}>
                $wk_arr = array();
                $wk_arr[] = array(500,10);
                $wk_arr[] = array(520,30);
                $wk_arr[] = array(525,10);
                $wk_arr[] = array(530,40);
                $this->_tpl_vars['ta1_array_list'] = $wk_arr;
            <{/php}>
            <{assign var="ta1_1_use" value=false}>
            <{assign var="ta1_2_use" value=false}>
            <{assign var="ta1_3_use" value=false}>
            <{assign var="ta1_4_use" value=false}>
            <{foreach item=ta1_array from=$ta1_array_list}>
                <{assign var="ta1_grp_work" value=$ta1_array[0]}>
                <{assign var="ta1_item_work" value=$ta1_array[1]}>
                <{if in_array($ta1_grp_work, $grp_flags)}>
                    <{if !$ta1_1_use}>
                        <{assign var="ta1_1_use" value=true}>
                        <{assign var="ta1_1_grp" value=$ta1_grp_work}>
                        <{assign var="ta1_1_item" value=$ta1_item_work}>
                    <{elseif !$ta1_2_use}>
                        <{assign var="ta1_2_use" value=true}>
                        <{assign var="ta1_2_grp" value=$ta1_grp_work}>
                        <{assign var="ta1_2_item" value=$ta1_item_work}>
                    <{elseif !$ta1_3_use}>
                        <{assign var="ta1_3_use" value=true}>
                        <{assign var="ta1_3_grp" value=$ta1_grp_work}>
                        <{assign var="ta1_3_item" value=$ta1_item_work}>
                    <{else}>
                        <{assign var="ta1_4_use" value=true}>
                        <{assign var="ta1_4_grp" value=$ta1_grp_work}>
                        <{assign var="ta1_4_item" value=$ta1_item_work}>
                    <{/if}>
                <{/if}>
            <{/foreach}>

            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">

            <{if $ta1_1_use || $ta1_2_use}>
                <tr>
                    <{include file="hiyari_show_easyinput_item.tpl" i=$ta1_1_grp j=$ta1_1_item td_title_width="10%" td_field_width="40%"}>
                <{if $ta1_2_use}>
                    <{include file="hiyari_show_easyinput_item.tpl" i=$ta1_2_grp j=$ta1_2_item td_title_width="10%" td_field_width="40%"}>
                <{else}>
                    <td colspan="2"></td>
                <{/if}>
                </tr>
            <{/if}>

            <{if $ta1_3_use}>
                <tr>
                    <{include file="hiyari_show_easyinput_item.tpl" i=$ta1_3_grp j=$ta1_3_item td_title_width="10%" td_field_width="40%"}>
                    <td colspan="2"></td>
                </tr>
            <{/if}>

            </table>
        <{/if}>


        <{if (in_array(500, $grp_flags) || in_array(520, $grp_flags) || in_array(525, $grp_flags) || in_array(530, $grp_flags)) && in_array(4000, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

<{* 事例の詳細（時系列）4000〜4049
   Smartyにロジック書かれすぎてわけわからないので、
       hiyari_show_easyinput_item.tpl
       hiyari_show_easyinput_item_field.tpl
       hiyari_show_easyinput_item_string.tpl
   とか使わないでやってます。
*}>

        <{if in_array(4000, $grp_flags)}>

            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <thead>
                <tr>
                    <td bgcolor="#DFFFDC">No</td>
                    <td bgcolor="#DFFFDC" width="250">
                        <{include file="hiyari_show_easyinput_item_title.tpl" i=4000 j=1}>
                    </td>
                    <td bgcolor="#DFFFDC" width="120">
                        <{include file="hiyari_show_easyinput_item_title.tpl" i=4000 j=4}>
                    </td>
                    <td bgcolor="#DFFFDC">
                        <{include file="hiyari_show_easyinput_item_title.tpl" i=4000 j=5}>
                    </td>
                </tr>
                </thead>
                <tbody id="tbody_4000">
                <{section name=tbody_4000 loop=50}>
                    <{assign var="index" value=$smarty.section.tbody_4000.index}>
                    <{if array_intersect(array(4000 + $index), $grp_flags) || array_intersect(array(4000 + $index), array_keys($vals))}>
                    <{math assign=i4000 equation="x + y" x=$index y=4000}>
                    <{math assign=i4000_no equation="xx + yy" xx=$index yy=1}>
                    <tr>
                        <td><{$i4000_no}></td>
                        <td><font size="3" class="j12"><{$vals[$i4000][1][0]|escape:"html"}> <{$vals[$i4000][2][0]|escape:"html"}>時 <{$vals[$i4000][3][0]|escape:"html"}>分</font></td>
                        <td><font size="3" class="j12"><{$vals[$i4000][4][0]|escape:"html"}> <{$vals[$i4000][6][0]|escape:"html"}></font></td>
                        <td><font size="3" class="j12"><{$vals[$i4000][5][0]}></font></td>
                    </tr>
                    <{/if}>
                <{/section}>
                </tbody>
            </table>

        <{/if}>

        <{if (in_array(500, $grp_flags) || in_array(520, $grp_flags) || in_array(525, $grp_flags) || in_array(530, $grp_flags) || in_array(4000, $grp_flags)) && in_array(570, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>


        <{if in_array(570, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr>
                    <{include file="hiyari_show_easyinput_item.tpl" i=570 j=5}>
                </tr>
            <{if $vals[570][11] != "" || $vals[570][12] != ""}>
                <tr>
                <{include file="hiyari_show_easyinput_item.tpl" i=570 j=11}>
                </tr>
                <tr>
                <{include file="hiyari_show_easyinput_item.tpl" i=570 j=12}>
                </tr>
            <{/if}>
            <{if $vals[570][21] != "" || $vals[570][22] != "" || $vals[570][23] != ""}>
                <tr>
                <{include file="hiyari_show_easyinput_item.tpl" i=570 j=21}>
                </tr>
                <tr height="22">
                    <td width=250 bgcolor="#DFFFDC">
                        <{include file="hiyari_show_easyinput_item_title.tpl" i=570 j=22}>
                    </td>
                    <td>
                        <font size="3" class="j12">
                        約
                        </font>
                        <{include file="hiyari_show_easyinput_item_field.tpl" i=570 j=22 text_field_width=50}>
                        <font size="3" class="j12">
                        cc
                        </font>
                    </td>
                </tr>
                <tr>
                <{include file="hiyari_show_easyinput_item.tpl" i=570 j=23}>
                </tr>
            <{/if}>
            <{if $vals[570][30] != "" || $vals[570][31] != "" || $vals[570][32] != ""}>
                <tr>
                <{include file="hiyari_show_easyinput_item.tpl" i=570 j=30}>
                </tr>
                <tr>
                <{include file="hiyari_show_easyinput_item.tpl" i=570 j=31}>
                </tr>
                <tr>
                <{include file="hiyari_show_easyinput_item.tpl" i=570 j=32}>
                </tr>
            <{/if}>
            <{if $vals[570][41] != "" || $vals[570][42] != "" || $vals[570][43] != "" || $vals[570][44] != ""}>
                <tr>
                <{include file="hiyari_show_easyinput_item.tpl" i=570 j=41}>
                </tr>
                <tr>
                <{include file="hiyari_show_easyinput_item.tpl" i=570 j=42}>
                </tr>
                <tr>
                <{include file="hiyari_show_easyinput_item.tpl" i=570 j=43}>
                </tr>
                <tr>
                <{include file="hiyari_show_easyinput_item.tpl" i=570 j=44}>
                </tr>
            <{/if}>
            </table>
        <{/if}>


        <{if in_array(1400, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=1400 j=10}>
                </tr>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=1400 j=20}>
                </tr>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=1400 j=30}>
                </tr>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=1400 j=40}>
                </tr>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=1400 j=50}>
                </tr>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=1400 j=60}>
                </tr>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=1400 j=70}>
                </tr>
            </table>
        <{/if}>


        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
      </tr>
      <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
      </tr>
    </table>
    <br>
<{/if}>





<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ６．インシデントへの対応                                                                             -->
<!-- ---------------------------------------------------------------------------------------------------- -->


<{if in_array(540, $grp_flags) || in_array(550, $grp_flags) || in_array(580, $grp_flags) || in_array(1410, $grp_flags)}>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td><font size="3" class="j12"><b>インシデントへの対応</b></font></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>

        <{if in_array(540, $grp_flags) || in_array(550, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">

                <tr>

                <{if in_array(540, $grp_flags) && in_array(550, $grp_flags)}>
                    <{include file="hiyari_show_easyinput_item.tpl" i=540 j=50 td_title_width="10%" td_field_width="40%" rowspan=2}>
                    <{include file="hiyari_show_easyinput_item.tpl" i=550 j=55 td_title_width="10%" td_field_width="40%"}>
                </tr><tr>
                    <{include file="hiyari_show_easyinput_item.tpl" i=550 j=60 td_title_width="10%" td_field_width="40%"}>
                <{elseif in_array(540, $grp_flags)}>
                    <{include file="hiyari_show_easyinput_item.tpl" i=540 j=50 td_title_width="10%" td_field_width="40%"}>
                    <td colspan="2"></td>
                <{else}>
                    <{include file="hiyari_show_easyinput_item.tpl" i=550 j=55 td_title_width="10%" td_field_width="40%"}>
                    <td colspan="2" rowspan="2"></td>
                </tr><tr>
                    <{include file="hiyari_show_easyinput_item.tpl" i=550 j=60 td_title_width="10%" td_field_width="40%"}>
                <{/if}>

                </tr>

            </table>
        <{/if}>


        <{if (in_array(540, $grp_flags) || in_array(550, $grp_flags)) && in_array(580, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>


        <{if in_array(580, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr>
                    <{include file="hiyari_show_easyinput_item.tpl" i=580 j=5}>
                </tr>
            <{if $vals[580][11] != "" || $vals[580][12] != "" || $vals[580][13] != ""}>
                <tr>
                    <{include file="hiyari_show_easyinput_item.tpl" i=580 j=11}>
                </tr>
                <tr>
                    <{include file="hiyari_show_easyinput_item.tpl" i=580 j=12}>
                </tr>
                <tr>
                    <{include file="hiyari_show_easyinput_item.tpl" i=580 j=13}>
                </tr>
            <{/if}>
            <{if $vals[580][21] != "" || $vals[580][22] != "" || $vals[580][23] != ""}>
                <tr>
                    <{include file="hiyari_show_easyinput_item.tpl" i=580 j=21}>
                </tr>
                <tr>
                    <{include file="hiyari_show_easyinput_item.tpl" i=580 j=22}>
                </tr>
                <tr>
                    <{include file="hiyari_show_easyinput_item.tpl" i=580 j=23}>
                </tr>
            <{/if}>
            <{if $vals[580][31] != "" || $vals[580][32] != "" || $vals[580][33] != ""}>
                <tr>
                    <{include file="hiyari_show_easyinput_item.tpl" i=580 j=31}>
                </tr>
                <tr>
                    <{include file="hiyari_show_easyinput_item.tpl" i=580 j=32}>
                </tr>
                <tr>
                    <{include file="hiyari_show_easyinput_item.tpl" i=580 j=33}>
                </tr>
            <{/if}>
            <{if $vals[580][41] != ""}>
                <tr>
                    <{include file="hiyari_show_easyinput_item.tpl" i=580 j=41}>
                </tr>
            <{/if}>
            </table>
        <{/if}>

        <{if in_array(1410, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=1410 j=10}>
                </tr>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=1410 j=20}>
                </tr>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=1410 j=30}>
                </tr>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=1410 j=40}>
                </tr>
            </table>
        <{/if}>


    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<br>
<{/if}>




<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ユーザー定義カテゴリ
<!-- ---------------------------------------------------------------------------------------------------- -->

<{foreach from=$user_grp_flags item=grp_code name=user_grp}>
  <{if $smarty.foreach.user_grp.first}>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><td><font size="3" class="j12"><b><{$user_cate_name}></b></font></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
  <{/if}>

        <tr height="22">
          <{include file="hiyari_show_easyinput_item.tpl" i=$grp_code j=10}>
        </tr>

  <{if $smarty.foreach.user_grp.last}>
      </table>
    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<br>
  <{/if}>
<{/foreach}>





<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ７．インシデントの分析                                                                               -->
<!-- ---------------------------------------------------------------------------------------------------- -->

<{if in_array(125, $grp_flags) || in_array(650, $grp_flags) || in_array(90, $grp_flags) || in_array(96, $grp_flags) || in_array(600, $grp_flags) || in_array(510, $grp_flags) || in_array(590, $grp_flags) || in_array(610, $grp_flags) || in_array(620, $grp_flags) || in_array(625, $grp_flags) || in_array(630, $grp_flags) || in_array(635, $grp_flags) || in_array(690, $grp_flags) || in_array(1500, $grp_flags) || in_array(1502, $grp_flags) || in_array(1504, $grp_flags) || in_array(1510, $grp_flags) || in_array(1520, $grp_flags) || in_array(1530, $grp_flags)}>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td><font size="3" class="j12"><b>インシデントの分析</b></font></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>
        <{if in_array(125, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=125 j=85}>
                </tr>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=125 j=86}>
                </tr>
            </table>
        <{/if}>
        <{if in_array(650, $grp_flags)}>
            <{if in_array(125, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
            <{/if}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=650 j=50}>
                </tr>
            </table>
        <{/if}>       

        <{if in_array(125, $grp_flags) && (in_array(650, $grp_flags) || in_array(90, $grp_flags) || in_array(96, $grp_flags) || in_array(600, $grp_flags) || in_array(510, $grp_flags) || in_array(590, $grp_flags) || in_array(610, $grp_flags) || in_array(620, $grp_flags) || in_array(625, $grp_flags) || in_array(630, $grp_flags) || in_array(635, $grp_flags) || in_array(690, $grp_flags))}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(90, $grp_flags) || in_array(96, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
            <{if in_array(90, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=90 j=10}>
                </tr>
            <{/if}>
            <{if in_array(96, $grp_flags)}>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=96 j=20}>
                </tr>
            <{/if}>
            </table>
        <{/if}>

        <{if (in_array(90, $grp_flags) || in_array(96, $grp_flags)) && (in_array(1500, $grp_flags) || in_array(1502, $grp_flags) || in_array(1504, $grp_flags) || in_array(1510, $grp_flags) || in_array(1520, $grp_flags) || in_array(1530, $grp_flags))}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(1500, $grp_flags) || in_array(1502, $grp_flags) || in_array(1504, $grp_flags) || in_array(1510, $grp_flags) || in_array(1520, $grp_flags) || in_array(1530, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
            <{* 1500: リスクの評価: 重要性 *}>
            <{if in_array(1500, $grp_flags)}>
                <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1500 j=10}>
                </tr>
            <{/if}>

            <{* 1502: リスクの評価: 緊急性 *}>
            <{if in_array(1502, $grp_flags)}>
                <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1502 j=10}>
                </tr>
            <{/if}>

            <{* 1504: リスクの評価: 頻度 *}>
            <{if in_array(1504, $grp_flags)}>
                <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1504 j=10}>
                </tr>
            <{/if}>

            <{if in_array(1510, $grp_flags)}>
                <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1510 j=10}>
                </tr>
            <{/if}>

            <{* 1520: システム改善の必要性 *}>
            <{if in_array(1520, $grp_flags)}>
                <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1520 j=10}>
                </tr>
            <{/if}>

            <{* 1530: 教育研修への活用 *}>
            <{if in_array(1530, $grp_flags)}>
                <tr height="22">
                    <{include file="hiyari_show_easyinput_item.tpl" i=1530 j=10}>
                </tr>
            <{/if}>
            </table>
        <{/if}>

        <{if (in_array(1500, $grp_flags) || in_array(1502, $grp_flags) || in_array(1504, $grp_flags) || in_array(1510, $grp_flags) || in_array(1520, $grp_flags) || in_array(1530, $grp_flags)) && (in_array(600, $grp_flags) || in_array(510, $grp_flags) || in_array(590, $grp_flags) || in_array(610, $grp_flags) || in_array(620, $grp_flags) || in_array(625, $grp_flags) || in_array(630, $grp_flags) || in_array(635, $grp_flags) || in_array(690, $grp_flags))}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(600, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=600 j=10}>
                </tr>
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=600 j=20}>
                </tr>

                <{if in_array(605, $grp_flags)}>
                    <{foreach from=$grps[605].easy_item_list key="easy_item_code_605" item="tmp_605_data"}>
                        <{if count($vals[605][$easy_item_code_605]) > 0 && count($grps[605].easy_item_list[$easy_item_code_605].easy_list) > 0}>
                            <tr height="22">
                                <{include file="hiyari_show_easyinput_item.tpl" i=605 j=$easy_item_code_605}>
                            </tr>
                        <{/if}>
                        <{if $easy_item_code_605 < "12" || $easy_item_code_605 > "18"}>
                            <{math equation="x + y" x=$easy_item_code_605 y=100 assign=eic_605}>
                            <{if $vals[605][$eic_605] && $vals[605][$eic_605][0] != "" && $grps[605].easy_item_list[$eic_605].easy_list}>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=605 j=$eic_605}>
                                </tr>
                            <{/if}>
                        <{elseif $easy_item_code_605 > "12"}>
                            <{math equation="x + y" x=$easy_item_code_605 y=99 assign=eic_605}>
                            <{if $vals[605][$eic_605] && $vals[605][$eic_605][0] != "" && $grps[605].easy_item_list[$eic_605].easy_list}>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=605 j=$eic_605}>
                                </tr>
                            <{/if}>
                        <{/if}>
                        <{if $easy_item_code_605 == "18"}>
                            <{math equation="x + y" x=$easy_item_code_605 y=100 assign=eic_605}>
                            <{if $vals[605][$eic_605] && $vals[605][$eic_605][0] != "" && $grps[605].easy_item_list[$eic_605].easy_list}>
                                <tr height="22">
                                    <{include file="hiyari_show_easyinput_item.tpl" i=605 j=$eic_605}>
                                </tr>
                            <{/if}>
                        <{/if}>
                    <{/foreach}>
                <{/if}>

            </table>
        <{/if}>

        <{if in_array(600, $grp_flags) && (in_array(510, $grp_flags) || in_array(590, $grp_flags) || in_array(610, $grp_flags) || in_array(620, $grp_flags) || in_array(625, $grp_flags) || in_array(630, $grp_flags) || in_array(635, $grp_flags) || in_array(690, $grp_flags) )}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(510, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=510 j=20}>
                </tr>
            </table>
        <{/if}>

        <{if in_array(510, $grp_flags) && (in_array(590, $grp_flags) || in_array(610, $grp_flags) || in_array(620, $grp_flags) || in_array(625, $grp_flags) || in_array(630, $grp_flags) || in_array(635, $grp_flags) || in_array(690, $grp_flags) )}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>


        <{if in_array(590, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                    <td width="10%" bgcolor="#DFFFDC">
                        <{include file="hiyari_show_easyinput_item_title.tpl" i=590 j=90}>
                    </td>
                    <td width="40%">
                        <{include file="hiyari_show_easyinput_item_field.tpl" i=590 j=90}>
                    </td>
                    <td width="10%" bgcolor="#DFFFDC">
                        <{include file="hiyari_show_easyinput_item_title.tpl" i=590 j=91}>
                    </td>
                    <td width="40%">
                        <{include file="hiyari_show_easyinput_item_field.tpl" i=590 j=91}>
                    </td>
                </tr>
            </table>
        <{/if}>


        <{if in_array(590, $grp_flags) && (in_array(610, $grp_flags) || in_array(620, $grp_flags) || in_array(625, $grp_flags) || in_array(630, $grp_flags) || in_array(635, $grp_flags) || in_array(690, $grp_flags) )}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>


        <{if in_array(610, $grp_flags) || in_array(620, $grp_flags) || in_array(625, $grp_flags) || in_array(630, $grp_flags) || in_array(635, $grp_flags)}>

                <{assign var="ta3_array_list" value=""}>
                <{php}>
                $wk_arr = array();
                $wk_arr[] = array(610,20);
                $wk_arr[] = array(620,30);
                $wk_arr[] = array(625,35);
                $wk_arr[] = array(630,40);
                $wk_arr[] = array(635,45);
                $this->_tpl_vars['ta3_array_list'] = $wk_arr;
                <{/php}>
                <{assign var="ta3_1_use" value=false}>
                <{assign var="ta3_2_use" value=false}>
                <{assign var="ta3_3_use" value=false}>
                <{assign var="ta3_4_use" value=false}>
                <{assign var="ta3_5_use" value=false}>
                <{foreach item=ta3_array from=$ta3_array_list}>
                    <{assign var="ta3_grp_work" value=$ta3_array[0]}>
                    <{assign var="ta3_item_work" value=$ta3_array[1]}>
                    <{if in_array($ta3_grp_work, $grp_flags)}>
                        <{if !$ta3_1_use}>
                            <{assign var="ta3_1_use" value=true}>
                            <{assign var="ta3_1_grp" value=$ta3_grp_work}>
                            <{assign var="ta3_1_item" value=$ta3_item_work}>
                        <{elseif !$ta3_2_use}>
                            <{assign var="ta3_2_use" value=true}>
                            <{assign var="ta3_2_grp" value=$ta3_grp_work}>
                            <{assign var="ta3_2_item" value=$ta3_item_work}>
                        <{elseif !$ta3_3_use}>
                            <{assign var="ta3_3_use" value=true}>
                            <{assign var="ta3_3_grp" value=$ta3_grp_work}>
                            <{assign var="ta3_3_item" value=$ta3_item_work}>
                        <{elseif !$ta3_4_use}>
                            <{assign var="ta3_4_use" value=true}>
                            <{assign var="ta3_4_grp" value=$ta3_grp_work}>
                            <{assign var="ta3_4_item" value=$ta3_item_work}>
                        <{else}>
                            <{assign var="ta3_5_use" value=true}>
                            <{assign var="ta3_5_grp" value=$ta3_grp_work}>
                            <{assign var="ta3_5_item" value=$ta3_item_work}>
                        <{/if}>
                    <{/if}>
                <{/foreach}>

                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">

                    <{if $ta3_1_use || $ta3_2_use}>
                        <tr>
                            <{if $ta3_1_grp == 620 && $ta3_1_item == 30}>
                            <td width="10%" bgcolor="#DFFFDC">
                                <{include file="hiyari_show_easyinput_item_title.tpl" i=620 j=30}>
                            </td>
                            <td width="40%">
                                <{include file="hiyari_show_easyinput_item_field.tpl" i=620 j=30}>
                                <{if $vals[620][33] != ""}>
                                <br>
                                <{include file="hiyari_show_easyinput_item_title.tpl" i=620 j=33}>
                                ：<{include file="hiyari_show_easyinput_item_field.tpl" i=620 j=33}>
                                <{/if}>
                            </td>
                            <{else}>
                                <{include file="hiyari_show_easyinput_item.tpl" i=$ta3_1_grp j=$ta3_1_item td_title_width="10%" td_field_width="40%"}>
                            <{/if}>
                        <{if $ta3_2_use}>
                            <{if $ta3_2_grp == 620 && $ta3_2_item == 30}>
                            <td width="10%" bgcolor="#DFFFDC">
                                <{include file="hiyari_show_easyinput_item_title.tpl" i=620 j=30}>
                            </td>
                            <td width="40%">
                                <{include file="hiyari_show_easyinput_item_field.tpl" i=620 j=30}>
                                <{if $vals[620][33] != ""}>
                                <br>
                                <{include file="hiyari_show_easyinput_item_title.tpl" i=620 j=33}>
                                ：<{include file="hiyari_show_easyinput_item_field.tpl" i=620 j=33}>
                                <{/if}>
                            </td>
                            <{else}>
                                <{include file="hiyari_show_easyinput_item.tpl" i=$ta3_2_grp j=$ta3_2_item td_title_width="10%" td_field_width="40%"}>
                            <{/if}>
                        <{else}>
                            <td colspan="2"></td>
                        <{/if}>
                        </tr>
                    <{/if}>

                    <{if $ta3_3_use || $ta3_4_use}>
                        <tr>
                            <{if $ta3_3_grp == 620 && $ta3_3_item == 30}>
                            <td width="10%" bgcolor="#DFFFDC">
                                <{include file="hiyari_show_easyinput_item_title.tpl" i=620 j=30}>
                            </td>
                            <td width="40%">
                                <{include file="hiyari_show_easyinput_item_field.tpl" i=620 j=30}>
                                <{if $vals[620][33] != ""}>
                                <br>
                                <{include file="hiyari_show_easyinput_item_title.tpl" i=620 j=33}>
                                ：<{include file="hiyari_show_easyinput_item_field.tpl" i=620 j=33}>
                                <{/if}>
                            </td>
                            <{else}>
                                <{include file="hiyari_show_easyinput_item.tpl" i=$ta3_3_grp j=$ta3_3_item td_title_width="10%" td_field_width="40%"}>
                            <{/if}>
                        <{if $ta3_4_use}>
                            <{if $ta3_4_grp == 620 && $ta3_4_item == 30}>
                            <td width="10%" bgcolor="#DFFFDC">
                                <{include file="hiyari_show_easyinput_item_title.tpl" i=620 j=30}>
                            </td>
                            <td width="40%">
                                <{include file="hiyari_show_easyinput_item_field.tpl" i=620 j=30}>
                                <{if $vals[620][33] != ""}>
                                <br>
                                <{include file="hiyari_show_easyinput_item_title.tpl" i=620 j=33}>
                                ：<{include file="hiyari_show_easyinput_item_field.tpl" i=620 j=33}>
                                <{/if}>
                            </td>
                            <{else}>
                                <{include file="hiyari_show_easyinput_item.tpl" i=$ta3_4_grp j=$ta3_4_item td_title_width="10%" td_field_width="40%"}>
                            <{/if}>
                        <{else}>
                            <td colspan="2"></td>
                        <{/if}>
                        </tr>
                    <{/if}>

                    <{if $ta3_5_use}>
                        <tr>
                            <{if $ta3_5_grp == 620 && $ta3_5_item == 30}>
                            <td width="10%" bgcolor="#DFFFDC">
                                <{include file="hiyari_show_easyinput_item_title.tpl" i=620 j=30}>
                            </td>
                            <td width="40%">
                                <{include file="hiyari_show_easyinput_item_field.tpl" i=620 j=30}>
                                <{if $vals[620][33] != ""}>
                                <br>
                                <{include file="hiyari_show_easyinput_item_title.tpl" i=620 j=33}>
                                ：<{include file="hiyari_show_easyinput_item_field.tpl" i=620 j=33}>
                                <{/if}>
                            </td>
                            <{else}>
                                <{include file="hiyari_show_easyinput_item.tpl" i=$ta3_5_grp j=$ta3_5_item td_title_width="10%" td_field_width="40%"}>
                            <{/if}>
                            <td colspan="2"></td>
                        </tr>
                    <{/if}>

                </table>
        <{/if}>

        <{if (in_array(610, $grp_flags) || in_array(620, $grp_flags) || in_array(625, $grp_flags) || in_array(630, $grp_flags) || in_array(635, $grp_flags)) && in_array(690, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(690, $grp_flags)}>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                <{include file="hiyari_show_easyinput_item.tpl" i=690 j=90}>
                </tr>
            </table>
        <{/if}>







    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<br>
<{/if}>

<!-- ファイル添付 START 	//20130630-->
<{if  $file_attach == "1"}>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
						<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
						<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
					</tr>
					<tr>
						<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
								<tr height="22">
									<td width="250" bgcolor="#DFFFDC"><font size="3" class="j12">ファイル添付</font></td>
									<td>
										<{foreach from=$arr_file  item=tmp_hy_file}>
											<div><font size="3" class="j12"><{$tmp_hy_file}></font></div>
										<{/foreach}>
									</td>
								</tr>



								<tr height="22">
									<td width="250" bgcolor="#DFFFDC"><font size="3" class="j12">【登録済】添付ファイル</font></td>
									<td>
									<font size="3" class="j12">

										<{foreach from=$file_list  item=tmp_hy_file}>
											<div><{$tmp_hy_file.file_name}></div>
										<{/foreach}>
									
									
									</font>
									</td>
								</tr>



							</table>
						</td>
						<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
					</tr>
					<tr>
						<td><img src="img/r_3.gif" width="10" height="10"></td>
						<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
						<td><img src="img/r_4.gif" width="10" height="10"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<{/if}>
<!-- ファイル添付 END //20130630-->

<!-- 全体 -->
</td></tr>
</table>
</td></tr>
</table>
