<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | 統計分析レポート表示</title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/smoothness/jquery-ui-1.10.3.custom.min.css">
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
  <script type="text/javascript" src="js/jquery/jquery-ui-1.10.2.custom.min.js"></script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
  <script type="text/javascript">    
    var w = 1024 + 37;
    var h = window.screen.availHeight;
    window.resizeTo(w, h);
  
    jQuery(function($){
        //匿名
        $('.anonymous').tooltip({
            position: {
                my: "left+20 center",
                at: "right center",
                using: function( position, feedback ) {
                    $( this ).css( position );
                    $( "<div>" )
                    .addClass( "arrow" )
                    .addClass( feedback.vertical )
                    .addClass( feedback.horizontal )
                    .appendTo( this );
                }
            }
        });
    });    
    
    //報告書表示(参照モード)
    function rp_action_update_from_report_id(report_id,mail_id) {
        var url = "hiyari_easyinput.php?session=<{$session}>&gamen_mode=update&report_id=" + report_id + "&callerpage=" + "<{$file}>" + "<{$non_update_flg}>"+"&mail_id=" + mail_id;
        show_rp_sub_window(url);
    }

    //レポーティング子画面を表示
    function show_rp_sub_window(url) {
        var h = window.screen.availHeight - 30;
        var w = window.screen.availWidth - 10;
        var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

        window.open(url, '_blank',option);
    }
    
    //分析登録
    function set_analysis_regist() {
        var oReportId = document.getElementsByName('report_id');
        var flg = false;
        for ( var i=0; i<oReportId.length; i++ ) {
            if(oReportId[i].checked == true) flg = true;
        }
        if(flg == false) {
            alert('事案番号を選択してください');
            return false;
        }

        var url = "hiyari_analysis_regist.php?session=<{$session}>";
        var h = 400;
        var w = 500;
        var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
        window.open(url, 'hiyari_analysis_regist', option);
        
        var OBJ = document.mainform;
        OBJ.action = "hiyari_analysis_regist.php?session=<{$session}>";
        OBJ.target = 'hiyari_analysis_regist';
        OBJ.mode.value = 'stats';
        OBJ.submit();
    }
    
    //EXCEL出力
    function excel_download(){
      document.mainform.action = 'hiyari_rm_stats_reports.php?session=<{$session}>';
      document.mainform.target = '';
      document.mainform.mode.value = 'excel'
      document.mainform.submit();
    }    
  </script>
</head>

<body id="rm_stats_report">
	<div id="header">
		<div class="inner">
			<h1>統計分析レポート表示</h1>
		</div>
	</div>
  
  <div id="content" class="clearfix">
    <form name="mainform" action="#" method="post">
      <input type="hidden" name="report_id_list" value="<{$report_id_list}>">
      <input type="hidden" name="session" value="<{$session}>">
      
      <div class="section">
        <div class="color01 radius clearfix mB20">
          <table id="axis_label" class="v_title">
            <tr>
              <th width="10%">縦軸</th>
              <td width="45%"><{$vertical_label}></td>
              <td width="45%" class="last"><{$vertical_item_label}></td>
            </tr>
            <tr>
              <th width="10%" class="none">横軸</th>
              <td width="45%" class="none"><{$horizontal_label}></td>
              <td width="45%" class="none last"><{$horizontal_item_label}></td>
            </tr>
          </table>
        </div>
        
        <div class="color02 radius clearfix">
          <input type="hidden" name="mode" value="">
          <ul id="main_nav" class="clearfix">
            <{if $is_analysis_use}>
              <li class="main_nav_10"><a href="#" onclick="set_analysis_regist();">分析</a></li>
            <{/if}>
            <li class="main_nav_08"><a href="#" onclick="excel_download();">出力</a></li>
          </ul>
            
          <table class="list">
            <tr>
              <{foreach name=lh from=$list_header item=h}>
                <th <{if $smarty.foreach.lh.last}>class="none"<{/if}>><{$h}></th>
              <{/foreach}>
            </tr>
              
            <{foreach from=$list_data item=ld}>
              <tr>
                <{if $is_analysis_use}>
                  <td><input type="radio" name="report_id" value="<{$ld.report_id}>"></td>
                <{else}>
                  <td>&nbsp;</td>
                <{/if}>
                <td><{$ld.report_no}></td>
                <td class="subject">
                  <span class="ellipsis">
                    <{if $ld.is_linkable}>
                      <a href="javascript:rp_action_update_from_report_id('<{$ld.report_id}>','<{$ld.mail_id}>')">
                    <{/if}>                    
                    <{$ld.title}>
                    <{if $ld.is_linkable}>
                      </a>
                    <{/if}>
                  </span>
                </td>
                <td class="class_nm">
                  <span class="ellipsis">
                    <{$ld.class_nm}>
                  </span>
                </td>
                <td class="emp_name">
                  <{if $ld.is_popup_name}>
                    <span class="anonymous">
                      <a href="#" title="<{$ld.real_name|escape}>"><{$ld.disp_emp_name}></a>
                    </span>
                  <{else}>
                    <span class="ellipsis">
                      <{$ld.disp_emp_name}>
                    </span>
                  <{/if}>
                </td>
                <td><{$ld.ymd}></td>
                <td class="count none"><{$ld.count}></td>
              </tr>
            <{/foreach}>
          </table>
        </div>
      </div>
      
      <input type="hidden" name="caller" value="hiyari_rm_stats_report.php" >
    </form>
  </div>
</body>
</html>
