<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
//--------------------------------------------------
//TinyMCE関連
//--------------------------------------------------
var m_use_tyny_mce = false;
if ( !tinyMCE.isOpera ) {
    m_use_tyny_mce = true;
    tinyMCE.init({
        mode : "textareas",
        theme : "advanced",
        plugins : "preview,table,emotions,fullscreen,layer",
        //language : "ja_euc-jp",
        language : "ja",
        width : "100%",
        height : "200",
        editor_selector : "mceEditor",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat,|,bullist,numlist,|,outdent,indent,|,hr,|,charmap,emotions,|,preview,|,undo,redo,|,fullscreen",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        content_css : "tinymce/tinymce_content.css",
        theme_advanced_statusbar_location : "none",
        force_br_newlines : true,
        forced_root_block : '',
        force_p_newlines : false
    });
}

function show_non_tag_message() {
    //Operaの場合はタグ抜きしたTEXTを表示
    document.form1.mail_message.value = "<{$mail_message_non_tag}>";
}

//--------------------------------------------------
//起動時の処理を行います。
//--------------------------------------------------
function loadaction() {
<{if $is_mail_sendable}>    //送信先を設定する。
    update_sendto_html();

    //TinyMCEを使用しない場合はタグ抜きメッセージを表示する。
    if ( !m_use_tyny_mce ) {
        show_non_tag_message();
    }
<{/if}>
}

//--------------------------------------------------
//送信可能かチェックします。
//--------------------------------------------------
function send_check() {
    //入力チェック
    var err_msg = "";
    var is_ok = true;
    //TO必須
    if(document.getElementById("to_emp_id_list").value == "") {
        is_ok = false;
        err_msg = err_msg + "宛先を入力してください。\n";
    }
    //件名必須
    if(document.getElementById("mail_subject").value == "") {
        is_ok = false;
        err_msg = err_msg + "件名を入力してください。\n";
    }

    if ( !is_ok ) {
        alert(err_msg);
        return false;
    }
    if ( !is_ok ) {
        alert(err_msg);
        return false;
    }

    //意思確認
    if ( !confirm("送信します。よろしいですか？") ) {
        return false;
    }

    return true;
}
// バックスペースキー押下による、ブラウザバック抑制
$(document).keydown(function(e) {
    if (   e.keyCode === 8
        || (e.altKey === true &&  e.keyCode === 37)) {
        var tag = e.target.nodeName.toLowerCase();
        var $target = $(e.target);
        if ((tag !== 'input' && tag !== 'textarea') || $target.attr('readonly') || $target.is(':disabled')) {
            document.FRM_MAIN.back.value='clicked';document.FRM_MAIN.submit();
            return false;
        }
    }
    return true;
});
</script>
<link rel="stylesheet" type="text/css" href="css/incident.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="loadaction();">
<a id="pagetop" name="pagetop"></a>

<!-- ヘッダー START -->
<{php}>
    require_once("hiyari_common.ini");
    show_hiyari_header_for_sub_window($this->_tpl_vars['PAGE_TITLE']);
<{/php}>
<!-- ヘッダー END -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td>
    <!-- 実行アイコン START -->
    <table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
        <td valign="middle" align="center"><img src="img/left.gif" width="36" height="30" style="cursor: pointer;" onclick="document.FRM_MAIN.back.value='clicked';document.FRM_MAIN.submit();"></td>
<{if $gamen_mode == 'new' || $gamen_mode == 'shitagaki'}>
        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
        <td valign="middle" align="center"><img src="img/hiyari_rp_shitagaki.gif" style="cursor: pointer;" onclick="document.FRM_MAIN.shitagaki.value='clicked';document.FRM_MAIN.submit();"></td>
        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
        <td valign="middle" align="center"><img src="img/hiyari_rp_soushin.gif" width="36" height="30" style="cursor: pointer;" onclick="if(send_check()){document.FRM_MAIN.send_create.value='clicked';document.FRM_MAIN.submit();}"></td>
<{else}>
    <{if $is_mail_not_sendable}>
        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
        <td valign="middle" align="center"><img src="img/hiyari_rp_shitagaki.gif" style="cursor: pointer;" onclick="document.FRM_MAIN.update.value='clicked';document.FRM_MAIN.submit();"></td>
    <{/if}>
    <{if $is_mail_sendable}>
        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
        <td valign="middle" align="center"><img src="img/hiyari_rp_soushin.gif" width="36" height="30" style="cursor: pointer;" onclick="if(send_check()){document.FRM_MAIN.send_update.value='clicked';document.FRM_MAIN.submit();}"></td>
    <{/if}>
<{/if}>
      </tr>
      <tr>
        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">戻る</font></td>
<{if $gamen_mode == 'new' || $gamen_mode == 'shitagaki'}>
        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">下書き保存</font></td>
        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">送信</font></td>
<{else}>
    <{if $is_mail_not_sendable}>
        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">更新</font></td>
    <{/if}>
    <{if $is_mail_sendable}>
        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">送信</font></td>
    <{/if}>
<{/if}>
      </tr>
    </table>
    <!-- 実行アイコン END -->
</td>
<td width="600">
    <!-- 画面進捗 START -->
    <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
        <tr>
            <td align="center" bgcolor="#FFDDFD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出来事報告の流れ</font></td>
        </tr>
        <tr>
            <td align="center">
                <nobr>
                &nbsp
                1.出来事の記入
                &nbsp;
                ⇒
                &nbsp;
                <b><span style="background-color:#35B341;color:#FFFFFF;padding:2px;">
                2.記入内容を確認
                </span></b>
                &nbsp;
                ⇒
                &nbsp;
                (送信)
                </font>
                </nobr>
            </td>
        </tr>
    </table>
    <!-- 画面進捗 END -->
</td></tr>
</table>






<form  name='FRM_MAIN' action='hiyari_easyinput_confirm.php' method='post'>

<!-- ポストバック情報 -->
<input type='hidden' name='is_postback' id='is_postback' value='ture'>
<input type='hidden' name='shitagaki'   id='shitagaki'   value=''>
<input type='hidden' name='update'      id='update'      value=''>
<input type='hidden' name='send_update' id='send_update' value=''>
<input type='hidden' name='send_create' id='send_create' value=''>
<input type='hidden' name='back'        id='back'        value=''>
<input type='hidden' name='eis_title_id' id='eis_title_id' value="<{$eis_title_id}>">

<!-- セッション情報 -->
<input type="hidden" name='session'     id='session'     value="<{$session}>">
<input type="hidden" name='hyr_sid'     id='hyr_sid'     value="<{$hyr_sid}>">




<!-- 所属情報 -->
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="right">

      <table border="0" cellspacing="0" cellpadding="0">
      <tr>
      <td>
<{if $report_no != ""}>
        <table border="0" cellspacing="0" cellpadding="2" class="list">
            <tr height="22">
                <td bgcolor="#DFFFDC">
                    <nobr>
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    事案番号
                    </font>
                    </nobr>
                </td>
                <td>
                    <nobr>
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <{$report_no}>
                    </font>
                    </nobr>
                </td>
            </tr>
        </table>
<{/if}>
      </td>
      <td width="10">
      <img src="img/spacer.gif" width="1" height="5" alt=""><br>
      </td>
      <td>
<{php}>
    require_once("./hiyari_post_select_box.ini");
    show_post_select_box_for_report_confirm(
        $this->_tpl_vars['con'],
        $this->_tpl_vars['fname'],
        $this->_tpl_vars['registrant_class'],
        $this->_tpl_vars['registrant_attribute'],
        $this->_tpl_vars['registrant_dept'],
        $this->_tpl_vars['registrant_room']
    );
<{/php}>
      </td>
      <tr>
      </table>

    </td>
  </tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>




<{if $is_mail_sendable}>

<!-- 大項目 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#66FFFF" align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<b>メール送信設定</b>
</font><br>
</td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- 大項目 END -->


<!-- メール情報 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center" style="padding-right:10px;padding-left:10px;">
<table width="1000" border="0" cellspacing="0" cellpadding="0">
<tr><td>

<!-- メールヘッダー -->
<{include file="hiyari_mail_header_js.tpl"}>
<{include file="hiyari_mail_header1.tpl"}>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<textarea id="mail_message" name="mail_message" style="width:100%; height:100;" rows="50" class="mceEditor"></textarea>

</td></tr>
</table>
</td></tr>
</table>
<!-- メール情報 END -->









<!-- 大項目 START -->
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#66FFFF" align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<b>報告書内容確認</b>
</font><br>
</td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- 大項目 END -->

<{/if}>

<!-- 入力フォーム START -->
<{include file="hiyari_show_easyinput.tpl"}>
<br />
<!-- 入力フォーム END -->
<br />
</form>


</td>
</tr>
</table>
&nbsp<a href="#pagetop">このページの先頭へ</a><br>
<br>
</body>
</html>
