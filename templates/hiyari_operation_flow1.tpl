<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><{$INCIDENT_TITLE}> | 運用フロー</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
  <{if $mode === "kanri"}>
    function init_page(){
      flow_url_file();
    }

    function flow_url_file(){
      rd_btn = document.mainform.elements['original_flow'];
      rd_btn_flg = false;
      if(rd_btn.length){
        for (var i = 0; i < rd_btn.length; i++){
          if(rd_btn[i].checked){
            if(rd_btn[i].value == 'f'){
              rd_btn_flg = true;
              break;
            }
          }
        }
      }

      obj = document.mainform.elements['flow_url'];
      obj.disabled = rd_btn_flg;
    }
  <{/if}>
  
  // 担当者設定画面
  function go_charge_register(mode, auth){
    if (mode == "kanri") {
      location.href = "hiyari_charge_register.php?session=<{$session}>&auth=" + auth;
    } else {
      var url = "hiyari_charge_disp.php?session=<{$session}>&auth=" + auth;
      show_rp_sub_window(url);
    }
  }

  function show_rp_sub_window(url){
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1000,height=700";
    window.open(url, '_blank',option);
  }
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
  .list {border-collapse:collapse;}
  .list td {border:#35B341 solid 1px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" <{if $mode == "kanri"}>onload="init_page();"<{/if}>>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
  <{include file="hiyari_header1.tpl"}>
  <img src="img/spacer.gif" width="1" height="5" alt=""><br>
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
      <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
      <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
    </tr>
    <tr>
      <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
      <td bgcolor="#F5FFE5">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <{if $mode == "kanri"}>
            <tr>
              <td>
                <form name="mainform" action="" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="postback_mode" value="send_change">
                  <input type="hidden" name="session" value="<{$session}>">
                  <input type="hidden" name="mode" value="<{$mode}>">
                  <table width="500" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>
                        <font size="4" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                          <nobr><input type="radio" name="original_flow" value="f" onclick="flow_url_file()" <{if !$is_original_flow}>checked<{/if}>>標準運用フローを利用する</nobr><br>
                          <nobr><input type="radio" name="original_flow" value="t" onclick="flow_url_file()" <{if $is_original_flow}>checked<{/if}>>独自運用フローを利用する</nobr><br>
                          <nobr>&nbsp;&nbsp;&nbsp;画像指定(gif,jpeg)&nbsp;<input type="file" name="flow_url" size="40"></nobr><br>
                        </font>
                      </td>
                      <td align="right"  valign="bottom">
                        <input type="submit" value="更新">
                      </td>
                    </tr>
                  </table>
                  <input type="hidden" name="flow_id" value="<{$flow_id}>">
                </form>
              </td>
            </tr>
          <{/if}>
          <{if $mode == "user"}>
            <tr>
              <td>
                <table width="600" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>
                      <table width="100%" border="0" cellspacing="0" cellpadding="3"  class="list">
                        <tr>
                          <td width="230" align="center" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当者名称</font></td>
                          <td width="130" align="center" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">略称</font></td>
                          <td align="center" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職務</font></td>
                        </tr>
                        <{*オペレーションフロー(ユーザー画面)の権限一覧*}>
                        <{foreach key=auth item=arr from=$arr_auth_list}>
                          <tr>
                            <td bgcolor="#FFFFFF">
                              <a href="javascript:go_charge_register('<{$mode}>', '<{$auth}>');">
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$arr.auth_name|escape}></font>
                              </a>
                            </td>
                            <td bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$arr.auth_short_name|escape}></font></td>
                            <td bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$arr.explanation|escape}></font></td>
                          </tr>
                        <{/foreach}>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          <{/if}>
          <tr>
            <td><img src="img/spacer.gif" width="1" height="20"></td>
          </tr>
          <!-- フロー画像 START -->
          <tr>
            <td>
              <div style="position: relative;">
                <{if $is_original_flow}>
                  <{if isset($img_url) && $img_url != ""}>
                    <img src="<{$img_url}>">
                  <{/if}>
                <{else}>
                  <img src="img/zu_1.png" width="886" height="592">
                  <div align="center" style="position:absolute; top:30px; left:65px; width:162px">
                    <nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">入力者</font></nobr>
                  </div>
                  <div align="center" style="position:absolute; top:234px; left:65px; width:162px">
                    <nobr>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                        <{if $isSM}>
                          <a href="javascript:go_charge_register('<{$mode}>','SM');"><{$arr_auth_name[0]|escape}></a>
                        <{else}>
                          <{$arr_auth_name[0]|escape}>
                        <{/if}>
                      </font>
                    </nobr>
                  </div>
                  <div align="center" style="position:absolute; top:236px; left:328px; width:162px">
                    <nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">医療安全検討部会</font></nobr>
                  </div>
                  <div align="center" style="position:absolute; top:224px; left:598px; width:162px">
                    <nobr>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                        <{if $isRM}>
                          <a href="javascript:go_charge_register('<{$mode}>','RM');"><{$arr_auth_name[1]|escape}></a>
                        <{else}>
                          <{$arr_auth_name[1]|escape}>
                        <{/if}>
                      </font>
                    </nobr>
                  </div>
                  <div align="center" style="position:absolute; top:244px; left:598px; width:162px">
                    <nobr>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                        <{if $isRA}>
                          <a href="javascript:go_charge_register('<{$mode}>','RA');"><{$arr_auth_name[2]|escape}></a>
                        <{else}>
                          <{$arr_auth_name[2]|escape}>
                        <{/if}>
                      </font>
                    </nobr>
                  </div>

                  <div align="center" style="position:absolute; top:332px; left:598px; width:162px">
                    <nobr>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                        <{if $isSD}>
                          <a href="javascript:go_charge_register('<{$mode}>','SD');"><{$arr_auth_name[3]|escape}></a>
                        <{else}>
                          <{$arr_auth_name[3]|escape}>
                        <{/if}>
                      </font>
                    </nobr>
                  </div>

                  <div align="center" style="position:absolute; top:476px; left:598px; width:162px">
                    <nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">医療安全管理委員会</font></nobr>
                  </div>
                <{/if}>
              </div>
            </td>
          </tr>
          <!-- フロー画像 END -->
        </table>
      </td>
      <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    </tr>
    <tr>
      <td><img src="img/r_3.gif" width="10" height="10"></td>
      <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
      <td><img src="img/r_4.gif" width="10" height="10"></td>
    </tr>
  </table>

  <{*ヘッダーに含まれるテーブルの終わり*}>
  <{if $mode == "kanri"}>
        </td>
      </tr>
    </table>
  <{/if}>
</td>
</tr>
</table>
</body>
</html>