<{*************************************************************************************************
ファイル名：hiyar_mail_header2.tpl
概要：hiyari_mail_input.iniにあったshow_hiyari_mail_header_input_form()のhtml部分をテンプレート化＆新デザインにしたもの
（js部分は、hiyar_mail_header_js.tpl）
*************************************************************************************************}>  
<input type="hidden" id="to_emp_id_list"   name="to_emp_id_list" value="">
<input type="hidden" id="cc_emp_id_list"   name="cc_emp_id_list" value="">
<input type="hidden" id="to_emp_name_list" name="to_emp_name_list" value="">
<input type="hidden" id="cc_emp_name_list" name="cc_emp_name_list" value="">
<input type="hidden" id="to_emp_anonymous_list" name="to_emp_anonymous_list" value="">
<input type="hidden" id="cc_emp_anonymous_list" name="cc_emp_anonymous_list" value="">

<table id="mail_header">
  <!-- TO領域 START -->
  <tr>                    
    <th>TO</th>
    <{if $is_to_editable && $address_search_flag}>
      <td>
        <table class="to_cc">
          <tr>
            <td id="to_disp_area"></td>
            <td>
              <ul>
                <li>
                  <input type="button" class="button radius_mini" value="職員検索" onclick="show_sendto_add_window('TO');">
                </li>
                <{foreach from=$usable_auth key=auth_key item=auth name=list}>
                  <li>
                    <input type="button" class="button radius_mini" value="<{$auth.auth_name}>" onclick="add_recv_list_from_auth('TO','<{$auth_key}>');">
                  </li>
                <{/foreach}>

                <{if $doctor_emp_id != ""}>
                  <li>
                    <input type="button"  class="button radius_mini" value="主治医" onclick="add_recv_list_from_emp('TO','<{$doctor_emp_id}>','<{$doctor_emp_name}>');">
                  </li>
                <{/if}>
              </ul>
            </td>
          </tr>
        </table>
      </td>
    <{else}>
      <td id="to_disp_area">&nbsp;</td>
    <{/if}>
  </tr>
  <!-- TO領域 END -->
        
  <!-- CC領域 START -->
  <tr id="mail_header_cc_tr">
    <th>
      <a href="#" onclick="switch_display('cc');">CC</a>
    </th>
    <{if $is_cc_editable && $address_search_flag}>
      <td>
        <table id="send_emp_list_cc" class="to_cc" style="display:none" >
          <tr>
            <td id="cc_disp_area"></td>
            <td>
              <ul>
                <li>
                  <input type="button" class="button radius_mini" value="職員検索" onclick="show_sendto_add_window('CC');">
                </li>
                <{foreach from=$usable_auth key=auth_key item=auth name=list}>
                  <li>
                    <input type="button" class="button radius_mini" value="<{$auth.auth_name}>" onclick="add_recv_list_from_auth('CC','<{$auth_key}>');">
                  </li>
                <{/foreach}>
                <{if $doctor_emp_id != ""}>
                  <li>
                    <input type="button" class="button radius_mini" value="主治医" onclick="add_recv_list_from_emp('CC','<{$doctor_emp_id}>','<{$doctor_emp_name}>');">
                  </li>
                <{/if}>
              </ul>
            </td>
          </tr>
        </table>
      </td>
    <{else}>
      <td id="cc_disp_area">&nbsp;</td>
    <{/if}>
  </tr>
  <!-- CC領域 END -->
  
  <!-- CC非表示の表示 START -->
  <tr id="mail_header_non_cc_tr">
    <th>CC</th>
    <td>
      <{if $is_cc_editable}>
        <{if $address_search_flag}>
          <a href="javascript:void(0);" onclick="is_cc_usable( true );return false;">CCを追加</a>
        <{/if}>
      <{else}>
        ※匿名からのメールを返信する場合、CCを指定することはできません。
      <{/if}>
    </td>
  </tr>
  <!-- CC非表示の表示 END -->
  
  <tr>
    <th class="none">件名</th>
    <td class="none">
      <input type="text" id="mail_subject" name="mail_subject" style="width:350" value="<{$default_subject}>">
    </td>
  </tr>
</table>

<{if $anonymous_mail_use_flg}>
  <label id="anonymous">
    <span class="checkbox_wrap<{if $is_anonymous_default}> checked<{/if}>">
      <input type="checkbox" class="checkbox" name="anonymous" value="true" <{if $is_anonymous_default}>checked<{/if}>>
    </span>
    匿名で送信する
  </label>
<{/if}>        
<!-- メールヘッダー情報 END -->