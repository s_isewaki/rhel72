<form method="post" enctype="multipart/form-data" action="hiyari_file_upload.php">
<script type="text/javascript" src="js/jquery/jquery-upload-1.0.2.min.js"></script>
<script type="text/javascript">
	jQuery(function($){
	 
		$(document).on('click', '.delF', function(){
			//alert(this.getAttribute("name"));
			$.ajax({
				context: self,
				url: 'hiyari_file_upload.php',
				type: 'post',
				data: {
					func_id: this.getAttribute("name"),
					StateTrns: 'delFile',
					Ids: '<{$report_id}>',
					fuld_session: '<{$session}>'
				},
				success:function(resDel){
					//alert('success');
					resDel = resDel.replace(/\r\n/g, "");
					var atchList = $('#atchList');
					var wwwTmp = $('#'+resDel);
					wwwTmp.remove();
				}
			});
		});

		$(document).on('click', '.delFR', function(){
			if(!confirm("ファイルが完全削除されます。よろしいですか？"))
			{
				return false;
			}
			
			$.ajax({
				context: self,
				url: 'hiyari_file_upload.php',
				type: 'post',
				data: {
					func_id: this.getAttribute("name"),
					StateTrns: 'delFileReg',
					Ids: '<{$report_id}>',
					fuld_session: '<{$session}>'
				},
				success:function(resDel){
					//alert('success');
					resDel = resDel.replace(/\r\n/g, "");
					var atchList = $('#atchList');
					var wwwTmp = $('#'+resDel);
					wwwTmp.remove();
				}
			});
		});
	 


	 
		$('#img').change(function(){
			//アップロード、表示
			var postD = $("form").serialize();
			
			//ボタン非表示
			$(this).hide();
			$(this).upload('hiyari_file_upload.php',
				postD,
				function(resHtml){
					var atchList = $('#atchList');
					atchList.html(resHtml);

					//ボタン再表示
					$(this).show();
					
				},'html'
			);
		});
	});

</script>



	<{if !$readonly_flg}>
	  <div class="section tabletype01">
		<h2 class="type02">ファイル</h2>
		<div class="color02 radius">
		  <table>
			<tr id="id_105_5_area">
				<th width="188" id="id_105_5_title" >ファイル添付</th>
				<td id="id_105_5_field" class="tabletype03">
					<div class="date_set1">
						<input type='hidden' name='fuld_session' id='fuld_session' value='<{$session}>'>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※<{$file_max_size}>まで</font>
						<input type="file" size="100" value="" id="img" name="img">
						<br><br>
						
						<div id="atchList">
							<{foreach from=$arr_file key=tmp_hy_key item=tmp_hy_file}>
								<div id="<{$tmp_hy_key}>"><input type="button" name="<{$tmp_hy_key}>" class="delF" value="削除"/> <{$tmp_hy_file}></div>
							<{/foreach}>
						</div>
					</div>
				</td>
			</tr>
		  </table>
	            
		</div>
	  </div>
	  <p class="pagetop"><a href="#top">ページの先頭へ</a></p>
	<{/if}>
	<{if $file_list|@count > 0}>
		<br>
		<div>
		  <table class="mT5">
		    <tr>
				<td VALIGN="bottom">
					<div id="hyr_preview">
					
						<{foreach from=$file_list item=tmp_hy_file}>
							<div  style="float: left; border : 10px  solid white" id="<{$tmp_hy_file.file_no}>">
								<a href="<{$tmp_hy_file.view}>=<{$session}>&r=<{$report_id}>&u=<{$tmp_hy_file.file_no}>" <{$tmp_hy_file.target}> >
									<img border=0 src="<{$tmp_hy_file.base}>" title="<{$tmp_hy_file.file_name}>"><br><{$tmp_hy_file.file_name}>
								</a>
									<{if !$readonly_flg}>
										<input type="button" name="<{$tmp_hy_file.file_no}>" class="delFR" value="削除"/>
									<{/if}>
							</div>
						<{/foreach}>
					</div>
				</td>
			</tr>
			</table>
		</div>
	<{/if}>


<!-- データが全て表示されたかのフラグ -->
<input type="hidden" name="data_exist_flg" value="1">