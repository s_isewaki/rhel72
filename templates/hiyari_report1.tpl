<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$page_title}></title>
  <script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
  <script type="text/javascript" src="js/yui/build/event/event-min.js"></script>
  <script type="text/javascript" src="js/yui/build/dom/dom-min.js"></script>
  <script type="text/javascript" src="js/yui/build/dragdrop/dragdrop-min.js"></script>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript" src="js/incident_popup.js"></script>
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript">
    jQuery.noConflict();
    var j$ = jQuery;
    
    var session = "<{$session}>";
    <{if $file == "hiyari_rpt_report_classification.php"}>
      var folder_id = "<{$folder_id}>";
    <{/if}>
    
    
    j$(document).ready(function() {
        j$('#all_select').click(function(){       
            allCheckOnOff();
	});
    });
	
    function allCheckOnOff()
    {        
        var list_select_objs = document.getElementsByName("list_select[]");
        var th_span_obj = document.getElementById("all_select");

        j$(list_select_objs).attr('checked', th_span_obj.checked);  
    } 
    
    
  </script>
  <script type="text/javascript" src="js/hiyari_report.js?v=201506"></script>
  <script type="text/javascript" src="js/hiyari_report1.js"></script>
  <script type="text/javascript" src="js/hiyari_calendar.js"></script>
  <{include file="hiyari_post_select_js.tpl"}>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <style type="text/css">
    .list {border-collapse:collapse;}
    .list td {border:#35B341 solid 1px;}

    .list_2 {border-collapse:collapse;}
    .list_2 td {border:#999999 solid 1px;}

    .list_in {border-collapse:collapse;}
    .list_in td {border:#FFFFFF solid 0px;}
  </style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
  <!-- ヘッダー START -->
  <{include file="hiyari_header1.tpl"}>
  <!-- ヘッダー END -->

  <!-- レポーティング本体 START -->
  <form name="list_form" method="post" action="<{$file}>">
    <input type="hidden" name="session" value="<{$session}>">
    <input type="hidden" name="search_evaluation_date_post" value="<{$search_evaluation_date}>">
    
    <input type="hidden" name="is_postback" value="true">
    <input type="hidden" name="mode" value="">
    
    <input type="hidden" name="search_toggle" value="<{$search_toggle}>">   
    
    <!-- 期間指定データ START -->
    <input type="hidden" name="search_date_mode" value="<{$search_date_mode}>">
    <input type="hidden" name="search_date_year" value="<{$search_date_year}>">
    <input type="hidden" name="search_date_month" value="<{$search_date_month}>">
    <!-- 期間指定データ END -->
    
    <input type="hidden" name="sort_item" value="<{$sort_item}>">
    <input type="hidden" name="sort_div" value="<{$sort_div}>">    
    <input type="hidden" name="page" value="<{$page}>">
    
    <{if $file == "hiyari_rpt_report_classification.php"}>
      <input type="hidden" name="is_report_classification_usable" value="<{$is_report_classification_usable}>">
    
      <!-- フォルダ分類関連 START -->
      <input type="hidden" name="folder_id" value="<{$folder_id}>">
      <input type="hidden" name="change_report_classification_report_id" value="">
      <input type="hidden" name="change_report_classification_folder_id" value="">            
      <!-- フォルダ分類関連 END -->
      
      <!-- Excel出力関連 START -->
      <input type="hidden" name="report_id" value="">
      <input type="hidden" name="report_id_list" value="<{$excel_target_report_id_list}>">
      <!-- Excel出力関連 END -->      
    <{/if}>   
    
    <{if $file == "hiyari_rpt_shitagaki_db.php"}>
      <input type="hidden" name="disp_mode" value="<{$disp_mode}>">
    <{/if}>
    
    <{if $file != "hiyari_rpt_shitagaki_db.php"}>
      <input type="hidden" id="is_report_db_update_able" name="is_report_db_update_able" value="<{if $is_report_db_update_able}>1<{else}>0<{/if}>">
      <input type="hidden" id="is_update" name="is_update" value="<{if $is_update}>1<{else}>0<{/if}>">
      <input type="hidden" id="is_report_file_page" name="is_report_file_page" value="<{if $file=='hiyari_rpt_report_classification.php'}>1<{else}>0<{/if}>">      
    <{/if}>
    
    <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <!-- 左側 START -->
        <td width="200" valign="top" bgcolor="#D5F4D8">        
          <table width="200" border="0" cellspacing="10" cellpadding="0">
            <tr>
              <td>
                <!-- トレイ選択 START -->
                <{foreach from=$torey_list item=torey}>
                  <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="5"><img src="img/i_corner1.gif" width="5" height="5"></td>
                      <td width="170" background="img/i_up.gif"><img src="img/spacer.gif" width="1" height="5" alt=""></td>
                      <td width="5"><img src="img/i_corner2.gif" width="5" height="5"></td>
                    </tr>
                    <tr>
                      <td background="img/i_left.gif"><img src="img/spacer.gif" width="5" height="1" alt=""></td>                      
                      <td bgcolor="#E5F8E1" align="center">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                          <a href="<{$torey.link}>?session=<{$session}>" <{if $torey.is_active}>style="background-color:#35B341;color:#FFFFFF;padding:2px;"<{/if}>><{$torey.name}></a>
                        </font>
                        <br>
                      </td>
                      <td background="img/i_right.gif"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
                    </tr>
                    <tr>
                      <td><img src="img/i_corner3.gif" width="5" height="5"></td>
                      <td background="img/i_down.gif"><img src="img/spacer.gif" width="1" height="5" alt=""></td>
                      <td><img src="img/i_corner4.gif" width="5" height="5"></td>
                    </tr>
                  </table>
                  <img src="img/spacer.gif" width="1" height="5" alt=""><br>
                <{/foreach}>
                <!-- トレイ選択 END -->
                
                <!-- 日付条件 START -->
                <{if $search_date_mode != "non"}>
                  <{include file="hiyari_date_search1.tpl"}>
                <{/if}>
                <!-- 日付条件 END -->
              
                <!-- 分類フォルダ START -->
                <{if $file == "hiyari_rpt_report_classification.php"}>
                  <table width="100%" border="0" cellspacing="0" cellpadding="3" class="list_2">
                    <tr>
                      <td bgcolor="#FFFBE7" align="center">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">分類フォルダ</font>
                      </td>
                    </tr>
                    <tr>
                      <td bgcolor="#FFFFFF">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list_in">
                          <tr>
                            <td height="100" valign="top">
                              <!-- フォルダ一覧 START -->
                              <table width="100%" border="0" cellspacing="0" cellpadding="1" class="list_in">
                                <{foreach from=$folders item=fld}>
                                  <tr>
                                    <td valign="top">
                                      <{if $fld.has_checkbox}>
                                        <input type="checkbox" value="<{$fld.id}>" id="folder_select[]" name="folder_select[]">
                                      <{else}>
                                        &nbsp;
                                      <{/if}>
                                    </td>
                                    <td valign="top" <{$fld.td_bgcolor}>>
                                      <div <{if !$fld.is_selected}> id="dd1_target_folder_<{$fld.id}>" class="dd1_target"<{/if}>>
                                        <img src="<{$fld.image}>" style="vertical-align:middle;cursor: pointer;" onclick="javascript:rp_action_change_classification_folder('<{$fld.id}>');">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                                          <a href="javascript:rp_action_change_classification_folder('<{$fld.id}>');" <{$fld.style}>><{$fld.name}></a>
                                        </font>
                                        <{if !$fld.has_checkbox or ($auto_group_flg == "1" || $fld.is_selected)}>
                                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">
                                            (<{$fld.count}>)
                                          </font>
                                        <{/if}>
                                      </div>
                                    </td>
                                  </tr>
                                <{/foreach}>
                              </table>
                              <!-- フォルダ一覧 END -->
                            </td>
                          </tr>
                          <!-- 分類オペレーション START -->
                          <tr>
                            <td>                            
                              <table width="100%" border="0" cellspacing="0" cellpadding="3" class="list_in">
                                <tr>
                                  <td width="33%" align="center">
                                    <input type="button" value="作成" onclick="rp_action_new_classification_folder();">
                                  </td>
                                  <td width="34%" align="center">
                                    <input type="button" value="変更" onclick="rp_action_update_classification_folder();">
                                  </td>
                                  <td width="33%" align="center">
                                    <input type="button" value="削除" onclick="rp_action_delete_classification_folder();">
                                  </td>
                                </tr>
                              </table>                              
                            </td>
                          </tr>
                          <!-- 分類オペレーション END -->
                        </table>
                      </td>
                    </tr>
                  </table>
                <{/if}>
                <!-- 分類フォルダ END -->
              
                <!-- 亀画像 -->
                <img src="<{$kame_img}>" width="180" alt="ファントルくん" style="cursor: pointer;" onclick="rp_action_image_select();">
              </td>
            </tr>
          </table>
        </td>
        <!-- 左側 END -->
      
        <td width="10"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
      
        <!-- 右側　START -->
        <td valign="top">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr><td colspan="2"><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
            <tr height="55">
              <!-- 実行アイコン START -->
              <td align="left" valign="top">
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <{if $icon_disp.create}>
                      <td align="center"><img src="img/hiyari_rp_new.gif" width="36" height="30" style="cursor: pointer;" onclick="rp_action_new();"></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.return}>
                      <td align="center"><img src="img/hiyari_rp_return.gif" width="32" height="29" style="cursor: pointer;" onclick="rp_action_return('<{$mail_id_mode}>');"></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>
                    
                    <{if $icon_disp.forward}>
                      <td align="center"><img src="img/hiyari_rp_forward.gif" width="32" height="29" style="cursor: pointer;" onclick="rp_action_forward('<{$mail_id_mode}>');"></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.delete1}>
                      <td align="center"><img src="img/hiyari_rp_delete.gif" width="24" height="34" style="cursor: pointer;" onclick="rp_action_delete();"></td>
                      <td><img src="img/spacer.gif" width="30" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.edit}>
                      <{if $file == "hiyari_rpt_shitagaki_db.php"}>
                        <td align="center"><img src="img/hiyari_rp_update.gif" width="36" height="30" style="cursor: pointer;" onclick="rp_action_shitagaki_update();"></td>
                        <td><img src="img/spacer.gif" width="30" height="1" alt=""></td>
                      <{else}>
                        <td align="center"><img src="img/hiyari_rp_update.gif" width="36" height="30" style="cursor: pointer;" onclick="rp_action_update('<{$mail_id_mode}>', '<{$list_id_is_mail_id}>', '<{$file}>');"></td>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                      <{/if}>
                    <{/if}>

                    <{if $icon_disp.read_flg}>
                      <td align="center"><img src="img/hiyari_rp_readflg.gif" width="44" height="31" style="cursor: pointer;" onclick="rp_action_readflg();"></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.progress}>
                      <td align="center"><img src="img/hiyari_rp_progress.gif" width="38" height="30" style="cursor: pointer;" onclick="rp_action_progress('<{$mail_id_mode}>', '<{$list_id_is_mail_id}>');"></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>
                    
                    <{if $icon_disp.analysis}>
                      <td align="center"><img src="img/hiyari_rp_analysis.gif" width="36" height="30" style="cursor: pointer;" onclick="rp_action_analysis('<{$mail_id_mode}>', '<{$list_id_is_mail_id}>');"></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>
                    
                    <{if $icon_disp.analysis_register}>
                      <td align="center"><img src="img/hiyari_rp_analysis.gif" width="36" height="30" style="cursor: pointer;" onclick="rp_action_analysis2();"></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>
                    
                    <{if $icon_disp.excel}>
                      <td align="center"><img src="img/hiyari_rp_excel.gif" style="cursor: pointer;" onclick="rp_action_report_excel();"></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.delete2}>
                      <td align="center"><img src="img/hiyari_rp_delete.gif" style="cursor: pointer;" onclick="rp_action_delete();"></td>
                      <td><img src="img/spacer.gif" width="30" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.undelete}>
                      <td align="center"><img src="img/hiyari_rp_delete_cancel.gif" style="cursor: pointer;" onclick="rp_action_delete_cancel();"></td>
                      <td><img src="img/spacer.gif" width="30" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.kill}>
                      <td align="center"><img src="img/hiyari_rp_delete.gif" style="cursor: pointer;" onclick="rp_action_kill();"></td>
                      <td><img src="img/spacer.gif" width="30" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.search}>
                      <td align="center"><img src="img/hiyari_rp_search.gif" style="cursor: pointer;" onclick="show_search_input_page('<{$file}>');"></td>
                    <{/if}>
                  </tr>
                  <tr>
                    <{if $icon_disp.create}>
                      <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">作成</font></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.return}>
                      <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">返信</font></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.forward}>
                      <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">転送</font></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.delete1}>
                      <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">削除</font></td>
                      <td><img src="img/spacer.gif" width="30" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.edit}>
                      <{if $file == "hiyari_rpt_report_classification.php"}>
                        <{if $is_inci_auth_emp && $is_report_db_update_able}>
                          <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">編集</font></td>
                        <{else}>
                          <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">参照</font></td>
                        <{/if}>                      
                      <{else}>
                        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">編集</font></td>                        
                      <{/if}>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.read_flg}>
                      <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">未読・既読</font></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>
                    
                    <{if $icon_disp.progress}>
                      <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">進捗登録</font></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.analysis}>
                      <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">分析・再発防止</font></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>
                    
                    <{if $icon_disp.analysis_register}>
                      <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">出来事分析</font></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>
                    
                    <{if $icon_disp.excel}>
                      <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">Excel出力</font></td>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.delete2}>
                      <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">削除</font></td>
                      <td><img src="img/spacer.gif" width="30" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.undelete}>
                      <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">削除取消</font></td>
                      <td><img src="img/spacer.gif" width="30" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.kill}>
                      <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">ゴミ箱を空にする</font></td>
                      <td><img src="img/spacer.gif" width="30" height="1" alt=""></td>
                    <{/if}>

                    <{if $icon_disp.search}>
                      <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">検索</font></td>
                    <{/if}>
                  </tr>
                </table>
              </td>
              <!-- 実行アイコン END -->
              
              <!-- 下書きタブ START -->
              <{if $is_other_shitagaki_readable}>
                <td valign="bottom" width="200">
                  <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <{if $disp_mode == "all"}>
                        <td bgcolor="#AEE1B3" style="border:#35B341 solid 1px;" width="90" align="center"><a href="hiyari_rpt_shitagaki_db.php?session=<{$session}>&disp_mode=self"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">本人分</font></a></td>
                        <td><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                        <td bgcolor="#35B341" style="border:#35B341 solid 1px;" width="90" align="center"><a href="hiyari_rpt_shitagaki_db.php?session=<{$session}>&disp_mode=all"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>すべて</b></font></a></td>
                      <{else}>
                        <td bgcolor="#35B341" style="border:#35B341 solid 1px;" width="90" align="center"><a href="hiyari_rpt_shitagaki_db.php?session=<{$session}>&disp_mode=self"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>本人分</b></font></a></td>
                        <td><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                        <td bgcolor="#AEE1B3" style="border:#35B341 solid 1px;" width="90" align="center"><a href="hiyari_rpt_shitagaki_db.php?session=<{$session}>&disp_mode=all"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">すべて</font></a></td>
                      <{/if}>
                    </tr>
                  </table>
                </td>
              <{/if}>
              <!-- 下書きタブ END -->
            </tr>
          </table>        
          
          <!-- 検索領域 START -->
          <img src="img/spacer.gif" width="1" height="10" alt=""><br>
          <table border="0" cellspacing="0" cellpadding="0"  id="search_area" style="display:none;">
            <tr>
              <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
              <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
              <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
            </tr>
            <tr>
              <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
              <td bgcolor="#F5FFE5">
                <table width="100%" border="0" cellspacing="1" cellpadding="2">
                  <tr>
                    <td rowspan="2">
                      <table width="100%" border="0" cellspacing="1" cellpadding="2" class="list">
                      	<{if $file=='hiyari_rpt_recv_torey.php'}>
                        <tr>
                          <td width="40" bgcolor="#DFFFDC">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告者</font>
                          </td>
                          <!-- 部署表示 -->
                          <td width="60" bgcolor="#DFFFDC">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告部署</font>
                          </td>
                          <td colspan="3"  bgcolor="#FFFFFF" nowrap>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                              <select id="search_emp_class" name="search_emp_class" onchange="setAtrbOptions('search_emp_post');"></select>
                              &nbsp;&nbsp;&gt;&nbsp;&nbsp;
                              <select id="search_emp_attribute" name="search_emp_attribute" onchange="setDeptOptions('search_emp_post');"></select>
                              &nbsp;&nbsp;&gt;&nbsp;&nbsp;
                              <select id="search_emp_dept" name="search_emp_dept" onchange="setRoomOptions('search_emp_post');"></select>
                              <{if $post_select_data.class_cnt == 4}>
                                &nbsp;&nbsp;&gt;&nbsp;&nbsp;
                                <select id="search_emp_room" name="search_emp_room"></select>
                              <{/if}>
                            </font>
                          </td>
                        </tr>
                          
                        <tr>
                          <td width="40" bgcolor="#DFFFDC">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$search_name}></font>
                          </td>
                          <td width="60" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名</font></td>
                          <td width="150" bgcolor="#FFFFFF">
                            <input type="text" name="search_emp_name" id="search_emp_name" size="40" value="<{$search_emp_name|escape}>" maxlength="40" style="ime-mode:active">
                          </td>
                          <td width="60" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
                          <td width="*" bgcolor="#FFFFFF">
                            <select name="search_emp_job" id="search_emp_job">
                              <option value="">
                              <{foreach from=$job_list key=k item=job_name}>
                                <option value="<{$k}>" <{if $k == $sel_job_id}> selected<{/if}>>
                                  <{$job_name}>
                                </option>
                              <{/foreach}>
                            </select>
                          </td>
                        </tr>
                        <{else}>
                        <tr>
                          <td rowspan="2" width="40" bgcolor="#DFFFDC">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$search_name}></font>
                          </td>
                          <!-- 部署表示 -->
                          <td width="60" bgcolor="#DFFFDC">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告部署</font>
                          </td>
                          <td colspan="3"  bgcolor="#FFFFFF" nowrap>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                              <{if $file=='hiyari_rpt_report_classification.php' || $file=='hiyari_analysis_search.php'}>
                                兼務部署の全てを表示
                                <input type="checkbox" value="true" id="search_emp_all" name="search_emp_all" onClick="hiyari_search_emp_all(this.checked, <{$post_select_data.class_cnt}>)" <{if $search_emp_all}>checked<{/if}>>
                                <br />
                              <{/if}>
                              <select id="search_emp_class" name="search_emp_class" onchange="setAtrbOptions('search_emp_post');"></select>
                              &nbsp;&nbsp;&gt;&nbsp;&nbsp;
                              <select id="search_emp_attribute" name="search_emp_attribute" onchange="setDeptOptions('search_emp_post');"></select>
                              &nbsp;&nbsp;&gt;&nbsp;&nbsp;
                              <select id="search_emp_dept" name="search_emp_dept" onchange="setRoomOptions('search_emp_post');"></select>
                              <{if $post_select_data.class_cnt == 4}>
                                &nbsp;&nbsp;&gt;&nbsp;&nbsp;
                                <select id="search_emp_room" name="search_emp_room"></select>
                              <{/if}>
                            </font>
                          </td>
                        </tr>
                          
                        <tr>
                          <td width="60" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名</font></td>
                          <td width="150" bgcolor="#FFFFFF">
                            <input type="text" name="search_emp_name" id="search_emp_name" size="40" value="<{$search_emp_name|escape}>" maxlength="40" style="ime-mode:active">
                          </td>
                          <td width="60" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
                          <td width="*" bgcolor="#FFFFFF">
                            <select name="search_emp_job" id="search_emp_job">
                              <option value="">
                              <{foreach from=$job_list key=k item=job_name}>
                                <option value="<{$k}>" <{if $k == $sel_job_id}> selected<{/if}>>
                                  <{$job_name}>
                                </option>
                              <{/foreach}>
                            </select>
                          </td>
                        </tr>
                        <{/if}>
                        
                        <{if $file != "hiyari_rpt_shitagaki_db.php"}>
                          <tr>
                            <td rowspan="2" width="40" bgcolor="#DFFFDC">
                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者</font>
                            </td>
                            <td width="60" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ＩＤ</font></td>
                            <td width="150" bgcolor="#FFFFFF">
                              <input type="text" name="search_patient_id" size="20" id="search_patient_id" value="<{$search_patient_id|escape}>" maxlength="30">
                            </td>
                            <td width="60" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者年齢</font></td>
                            <td width="*" bgcolor="#FFFFFF">
                              <select name="search_patient_year_from" id="search_patient_year_from" onchange="check_patient_year('from');">
                                <option value="">
                                <{foreach from=$patient_year key=k item=value}>
                                  <option value="<{$k}>" <{if $k == $search_patient_year_from}> selected<{/if}>>
                                    <{$value}>
                                  </option>
                                <{/foreach}>
                              </select>
                              〜
                              <select name="search_patient_year_to" id="search_patient_year_to" onchange="check_patient_year('to');">
                                <option value="">
                                <{foreach from=$patient_year key=k item=value}>
                                  <option value="<{$k}>" <{if $k == $search_patient_year_to}> selected<{/if}>>
                                    <{$value}>
                                  </option>
                                <{/foreach}>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td width="60" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名</font></td>
                            <td colspan="3" width="*" bgcolor="#FFFFFF">
                              <input type="text" name="search_patient_name" size="40" id="search_patient_name" value="<{$search_patient_name|escape}>" maxlength="40" style="ime-mode:active">
                            </td>
                          </tr>
                          <script type="text/javascript">
                            if(document.getElementById('search_patient_year_from').value == '900'){
                              document.getElementById('search_patient_year_to').disabled = true;
                            }
                            
                            function check_patient_year(div)
                            {
                              if(div == 'from'){
                                  var year_from = document.getElementById('search_patient_year_from').value;
                                  if(year_from == '900'){
                                      document.getElementById('search_patient_year_to').value = '';
                                      document.getElementById('search_patient_year_to').disabled = true;
                                  }
                                  else{
                                      document.getElementById('search_patient_year_to').disabled = false;
                                  }
                              }
                              else if(div == 'to'){
                                  var year_to = document.getElementById('search_patient_year_to').value;
                                  if(year_to == '900'){
                                      document.getElementById('search_patient_year_from').value = '900';
                                      document.getElementById('search_patient_year_to').value = '';
                                      document.getElementById('search_patient_year_to').disabled = true;
                                  }
                                  else{
                                      document.getElementById('search_patient_year_to').disabled = false;
                                  }
                              }
                            }
                          </script>
                        <{/if}>
                        
                        <{if $file == "hiyari_rpt_report_classification.php" || $file == "hiyari_analysis_search.php"}>
                          <tr>
                            <td bgcolor="#DFFFDC" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事案番号</font></td>
                            <td bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                              <input type="text" value="<{$search_report_no}>" id="search_report_no" name="search_report_no" maxlength="11">
                              </font>
                            </td>
                            <td bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価予定日</font></td>
                            <td bgcolor="#FFFFFF">
                              <select id="search_evaluation_date" name="search_evaluation_date">
                                <option value="">
                                <option value="today"    <{if $search_evaluation_date == "today"   }>selected<{/if}> >本日
                                <option value="tomorrow" <{if $search_evaluation_date == "tomorrow"}>selected<{/if}> >翌日
                                <option value="1week"    <{if $search_evaluation_date == "1week"   }>selected<{/if}> >1週間以内
                                <option value="2week"    <{if $search_evaluation_date == "2week"   }>selected<{/if}> >2週間以内
                              </select>
                            </td>
                          </tr>
                        <{/if}>

                        <{if $file == "hiyari_rpt_recv_torey.php" || $file == "hiyari_rpt_send_torey.php"}>
                          <tr>
                            <td bgcolor="#DFFFDC" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事案番号</font></td>
                            <td bgcolor="#FFFFFF" colspan="3">
                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                <input type="text" value="<{$search_report_no}>" id="search_report_no" name="search_report_no" maxlength="11">
                              </font>
                            </td>
                          </tr>
                        <{/if}>
                        
                        <{if $file == "hiyari_rpt_recv_torey.php" || $file == "hiyari_rpt_report_classification.php" || $file == "hiyari_analysis_search.php"}>
                          <tr>
                            <td bgcolor="#DFFFDC" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題へのキーワード</font></td>
                            <td bgcolor="#FFFFFF" colspan="3">
                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                <input type="text" value="<{$search_title}>" id="search_title" name="search_title" size="60">
                                空白で区切られたキーワードを
                                <input type="radio" name="title_and_or_div" value="1" <{if $title_and_or_div == 1 || $title_and_or_div == ""}>checked<{/if}>>
                                AND
                                <input type="radio" name="title_and_or_div" value="2" <{if $title_and_or_div == 2}>checked<{/if}>>
                                OR検索
                              </font>
                            </td>
                          </tr>
                          <tr>
                            <td bgcolor="#DFFFDC" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">インシデントの内容へのキーワード</font></td>
                            <td bgcolor="#FFFFFF" colspan="3">
                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                <input type="text" value="<{$search_incident}>" id="search_incident" name="search_incident" size="60">
                                空白で区切られたキーワードを
                                <input type="radio" name="incident_and_or_div" value="1" <{if $incident_and_or_div == 1 || $incident_and_or_div == ""}>checked<{/if}>>
                                AND
                                <input type="radio" name="incident_and_or_div" value="2" <{if $incident_and_or_div == 2}>checked<{/if}>>
                                OR検索
                              </font>
                            </td>
                          </tr>
                        <{/if}>
                        
                        <tr>
                          <td bgcolor="#DFFFDC" colspan="2">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$search_torey_date_in_search_area_title_name}></font>
                          </td>
                          <td bgcolor="#FFFFFF">
                            <table border="0" cellspacing="0" cellpadding="0" class="list_in">
                              <tr>
                                <td width="80">
                                  <nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始年月日</font></nobr>
                                </td>
                                <td width="90">
                                  <input type="text" id="search_torey_date_in_search_area_start" name="search_torey_date_in_search_area_start" style="width:80px" value="<{$search_torey_date_in_search_area_start}>" maxlength="10">
                                </td>
                                <td width="20">
                                  <img src="img/calendar_link.gif" style="cursor:pointer;" onclick="call_hiyari_calendar('torey_search_st_in_search_area', '<{$session}>')"/>
                                </td>
                              </tr>
                            </table>
                          </td>
                          <td bgcolor="#FFFFFF" colspan="2">
                            <table border="0" cellspacing="0" cellpadding="0" class="list_in">
                              <tr>
                                <td width="80"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">終了年月日</font></nobr></td>
                                <td width="90"><input type="text" id="search_torey_date_in_search_area_end" name="search_torey_date_in_search_area_end" style="width:80px" value="<{$search_torey_date_in_search_area_end}>" maxlength="10"></td>
                                <td width="20"><img src="img/calendar_link.gif" style="cursor:pointer;" onclick="call_hiyari_calendar('torey_search_ed_in_search_area', '<{$session}>')"/></td>
                              </tr>
                            </table>
                          </td>
                        </tr>

                        <{if $file != "hiyari_rpt_shitagaki_db.php"}>
                          <tr>
                            <td bgcolor="#DFFFDC" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生日</font></td>
                            <td bgcolor="#FFFFFF">
                              <table border="0" cellspacing="0" cellpadding="0" class="list_in">
                                <tr>
                                  <td width="80"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始年月日</font></nobr></td>
                                  <td width="90"><input type="text" id="search_incident_date_start" name="search_incident_date_start" style="width:80px" value="<{$search_incident_date_start}>" maxlength="10"></td>
                                  <td width="20"><img src="img/calendar_link.gif" style="cursor:pointer;" onclick="call_hiyari_calendar('torey_search_incident_date_st', '<{$session}>')"/></td>
                                </tr>
                              </table>
                            </td>
                            <td bgcolor="#FFFFFF" colspan="2">
                              <table border="0" cellspacing="0" cellpadding="0" class="list_in">
                                <tr>
                                  <td width="80"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">終了年月日</font></nobr></td>
                                  <td width="90"><input type="text" id="search_incident_date_end" name="search_incident_date_end" style="width:80px" value="<{$search_incident_date_end}>" maxlength="10"></td>
                                  <td width="20"><img src="img/calendar_link.gif" style="cursor:pointer;" onclick="call_hiyari_calendar('torey_search_incident_date_ed', '<{$session}>')"/></td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        <{/if}>
                      </table>
                    </td>
                    <td valign="top">
                      <a href="javascript:show_search_input_page('<{$file}>');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">閉じる</font></a>
                    </td>
                  </tr>
                  </tr>
                  <tr>
                    <td valign="bottom">
                      <{if $file == "hiyari_rpt_report_classification.php"}>
                          <input type="checkbox" id="auto_group_flg" name="auto_group_flg" value="1" <{if $auto_group_flg == "1"}>checked<{/if}>>
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                            自動<br>分類する
                          </font>
                          <br><br>
                      <{/if}>
                      <input type="button" value="検索" onclick="search_torey('<{$file}>', '<{$search_torey_date_in_search_area_title_name}>');">
                    </td>
                  </tr>
                </table>     
              </td>
              <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
            </tr>
            <tr>
              <td><img src="img/r_3.gif" width="10" height="10"></td>
              <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
              <td><img src="img/r_4.gif" width="10" height="10"></td>
            </tr>
          </table>
          <img src="img/spacer.gif" width="1" height="10" alt=""><br>

          <script type="text/javascript">
            j$("#search_area").css("display", ("<{$search_toggle}>" == "open") ? "" : "none");
            
            set_post_select_box_obj_key("search_emp_post","search_emp_class","search_emp_attribute","search_emp_dept","search_emp_room");

            <{if $file=='hiyari_rpt_report_classification.php' || $file=='hiyari_analysis_search.php'}>
              hiyari_search_emp_all(<{if $search_emp_all}>true<{else}>false<{/if}>, <{$post_select_data.class_cnt}>);
            <{/if}>
            
            <{if isset($sel_class_id)}>
              setClassOptions("search_emp_post",'<{$sel_class_id}>','<{$sel_atrb_id}>','<{$sel_dept_id}>','<{$sel_room_id}>');
            <{else}>
              setClassOptions("search_emp_post",'','','','');
            <{/if}>  
          </script>          
          <!-- 検索領域 END -->
          
          <!-- 一覧 START -->
          <table width="100%" border="0" cellspacing="0" cellpadding="3" class="list_2">
            <!-- タイトル START -->
            <tr>
                <td rowspan="2" bgcolor="#FFFBE7" width="25">
                
                    <input type="checkbox"  id="all_select" value="all">
         
                </td>
            <!--   <td rowspan="2" bgcolor="#FFFBE7" width="25">&nbsp;</td>-->
              <{if $file == "hiyari_rpt_report_classification.php"}>
                <!-- 報告ファイル -->
                <td rowspan="2" align="center" bgcolor="#FFFBE7" width="130"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a title="初期ソート順" href="javascript:set_report_list_sort('0');">○<span style="margin-right:0.5em;"></span><a href="javascript:set_report_list_sort('REPORT_NO');">事案番号</a></font></td>
                <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('REPORT_TITLE');">表題</a></font></td>
                <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('CLASS');">報告部署</a></font></td>
                <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('REGIST_NAME');">報告者</a></font></td>
                <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('<{$date_type}>');"><{$date_name}></a></font></td>
                <td rowspan="2" align="center" bgcolor="#FFDDFD" width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('LEVEL');">影<BR>響</a></font></td>
              <{elseif $file == "hiyari_rpt_shitagaki_db.php"}>
                <!-- 下書きファイル -->
                <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('REPORT_TITLE');">表題</a></font></td>
                <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('REGIST_NAME');">報告者</a></font></td>
                <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('REGIST_DATE');">作成日</a></font></td>
              <{else}>
                <{*$disp_flagを使用しているのは受信・送信トレイのみ*}>
                <{if $disp_flag.report_no_disp == "t"}>
                  <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('REPORT_NO');">事案番号</a></font></td>
                <{/if}>                
                <{if $disp_flag.subject_disp == "t"}>
                  <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('SUBJECT');">件名</a></font></td>
                <{/if}>                
                <{if $disp_flag.send_emp_class_disp == "t"}>
                  <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('SEND_CLASS');">報告部署</font></a></td>
                <{/if}>                
                <{if $disp_flag.send_emp_disp == "t"}>
                  <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('SEND_NAME');">送信者</font></a></td>
                <{/if}>
                <{if $disp_flag.recv_emp_disp == "t"}>
                  <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">受信者</font></td>
                <{/if}>      
                <{if $disp_flag.send_date_disp == "t"}>
                  <td rowspan="2" align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('SEND_DATE');">送信日</a></font></td>
                <{/if}>
                <{if $disp_flag.read_count_disp == "t"}>
                  <td rowspan="2" align="center" bgcolor="#FFDDFD" width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">既<br>読</font></td>
                <{/if}>
                <{if $disp_flag.read_flg_disp == "t"}>
                  <td rowspan="2" align="center" bgcolor="#FFDDFD" width="20"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('READ_REPORT');">未<br>読</a></font></td>
                <{/if}>
                <{if $disp_flag.level_disp == "t"}>
                  <td rowspan="2" align="center" bgcolor="#FFDDFD" width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('LEVEL');">影<BR>響</a></font></td>
                <{/if}>
              <{/if}>
              
              <{if $disp_progress}>
                <{foreach from=$auth_short_name item=i}>
                  <td colspan="2" align="center" bgcolor="#FFDDFD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i}></font></td>
                <{/foreach}>
              <{/if}>
            </tr>
            <tr>
              <{if $disp_progress}>
                <{foreach from=$auth_key item=i}>
                  <td bgcolor="#FFDDFD" align="center" width="20"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('<{$i}>_START');">受<br>付</a></font></td>
                  <td bgcolor="#FFDDFD" align="center" width="20"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('<{$i}>_END');">確<br>認</a></font></td>
                <{/foreach}>
              <{/if}>
            </tr>            
            <!-- タイトル END -->
            
            <!-- 一覧の内容 START -->
            <{foreach from=$list item=i}>
              <tr>
                <td align="center">
                  <input type="checkbox" name="list_select[]" value="<{$i.list_id}>">                                    
                  <input type="hidden" id="report_id_of_list_id_<{$i.list_id}>" value="<{$i.report_id}>">
                  <input type="hidden" id="report_torey_update_flg_id_<{$i.list_id}>" value="<{$i.report_torey_update_flg_value}>">
                  <input type="hidden" id="send_job_id_<{$i.list_id}>" value="<{$i.send_job_id}>">
                  <input type="hidden" id="eis_no_<{$i.list_id}>" value="<{$i.eis_no}>">
                </td>
                
                <{if $file == "hiyari_rpt_report_classification.php"}>
                  <!-- 報告ファイル -->
                  <input type="hidden" id="link_id_for_<{$i.report_id}>" value="<{$i.report_link_id}>">
                  <input type="hidden" id="link_status_for_<{$i.report_id}>" value="<{$i.report_link_status}>">
                  <input type="hidden" id="mail_id_of_list_id_<{$i.list_id}>" value="<{$i.mail_id}>">
                  <td align="<{$i.report_no_align}>">
                    <div id="dd1_target_report_<{$i.report_id}>" class="dd1_target">
                      <div id="dd1_item_report_<{$i.report_id}>" class="dd1_item">
                        <table border="0" cellspacing="0" cellpadding="0" class="list_in">
                          <tr>
                            <td width="20" align="center">
                              <div id="dd1_handle_report_<{$i.report_id}>" class="dd1_handle" style="cursor:move;">
                                <img src="<{$i.little_report_img}>" style="vertical-align:middle;">
                              </div>
                             </td>
                            <td width="100" align="left">
                              <nobr>
                              <{if $folder_id != "delete" && $is_report_classification_usable}>
                                <a href="javascript:rp_action_update_same_report_link('<{$i.report_id}>');">
                              <{/if}>
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.report_no}></font>
                              <{if $folder_id != "delete" && $is_report_classification_usable}>
                                </a>
                              <{/if}>
                              </nobr>
                            </td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </td>
                  <td>
                    <a href="javascript:rp_action_update_from_report_id('<{$i.report_id}>', '<{$file}>', '<{$i.mail_id}>');">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.title}></font>
                    </a>
                  </td>
                  <td>
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.class_nm}></font>
                  </td>                  
                  <td>
                    <{if $i.anonymous_flg && $i.mouseover_popup_flg}>                        
                      <span onmousemove="name_popup_disp('<{$i.real_name|escape}>', '報告者', event);" onmouseout="closeDetail();" style="cursor:pointer;">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="blue"><B><{$i.disp_emp_name}></B></font>
                      </span>
                    <{else}>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.disp_emp_name}></font>
                    <{/if}>
                  </td>                  
                  <td>
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.report_date}></font>
                  </td>                  
                  <td align="center">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.level}></font>
                  </td>
                
                <{elseif $file == "hiyari_rpt_shitagaki_db.php"}>
                  <!-- 下書きファイル -->
                  <td>
                    <a href="javascript:rp_action_shitagaki_update_from_report_id('<{$i.report_id}>');">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.title}></font>
                    </a>
                  </td>
                  <td>
                    <{if $i.anonymous_flg && $i.mouseover_popup_flg}>                        
                      <span onmousemove="name_popup_disp('<{$i.real_name|escape}>', '報告者', event);" onmouseout="closeDetail();" style="cursor:pointer;">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="blue"><B><{$i.disp_emp_name}></B></font>
                      </span>
                    <{else}>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.disp_emp_name}></font>
                    <{/if}>
                  </td>
                  <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.create_date}></font></td>
                
                <{else}>
                  <{*$disp_flagを使用しているのは受信・送信トレイのみ*}>
                  <{if $disp_flag.report_no_disp == "t"}>
                    <td nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.report_no}></font></td>
                  <{/if}>
                
                  <{if $disp_flag.subject_disp == "t"}>
                    <td <{$i.marker_style}>>
                      <a href="javascript:rp_action_mail_disp('<{$i.mail_id}>', '<{$mail_id_mode}>');" <{$i.marker_style}>>
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.subject}></font>
                      </a>
                    </td>
                  <{/if}>

                  <{if $disp_flag.send_emp_class_disp == "t"}>
                    <td <{$i.marker_style}>>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.class_nm}></font>
                    </td>
                  <{/if}>

                  <{if $disp_flag.send_emp_disp == "t"}>
                    <td <{$i.marker_style}>>
                      <{if $i.anonymous_flg && $i.mouseover_popup_flg}>
                        <span onmousemove="name_popup_disp('<{$i.real_name|escape}>', '送信者', event);" onmouseout="closeDetail();" style="cursor:pointer;">
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="blue"><B><{$i.disp_emp_name}></B></font>
                        </span>
                      <{else}>
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.disp_emp_name}></font>
                      <{/if}>
                    </td>
                  <{/if}>

                  <{if $disp_flag.recv_emp_disp == "t"}>
                    <td <{$i.marker_style}>>
                      <{if $i.anonymous_flg && $i.mouseover_popup_flg}>                        
                        <span onmousemove="name_popup_disp('<{$i.real_name|escape}>', '受信者', event);" onmouseout="closeDetail();" style="cursor:pointer;">
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="blue"><B><{$i.disp_emp_name}></B></font>
                        </span>
                      <{else}>
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.disp_emp_name}></font>
                      <{/if}>
                    </td>
                  <{/if}>
                  
                  <{if $disp_flag.send_date_disp == "t"}>
                    <td <{$i.marker_style}>>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.send_date}></font>
                    </td>
                  <{/if}>

                  <{if $disp_flag.read_count_disp == "t"}>
                    <td align="center">
                      <a href="javascript:rp_action_recv_emp_list_disp('<{$i.mail_id}>');">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.read_count}></font>
                      </a>
                    </td>
                  <{/if}>
                  
                  <{if $disp_flag.read_flg_disp == "t"}>
                    <td align="center">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.recv_not_read_mark}></font>
                    </td>
                  <{/if}>
                  
                  <{if $disp_flag.level_disp == "t"}>
                    <td align="center">
                      <a href="javascript:rp_action_analysis_disp('<{$i.report_id}>');">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$i.level}></font>
                      </a>
                    </td>
                  <{/if}>
                <{/if}>
                
                <{if $disp_progress}>
                  <{foreach from=$i.auth_info key=k item=val}>
                    <td align="center" <{if !$val.use}>style='background-color:#C0C0C0'<{/if}> >                      
                      <{if $val.accepted}>
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">○</font>
                      <{/if}>                      
                    </td>
                    <td align="center" <{if !$val.use}>style='background-color:#C0C0C0'<{/if}> >
                      <{if $val.confirmed}>
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                          <{if $progress_comment_use_flg}>
                            <a href="javascript:rp_action_progress_comment_list('<{$i.report_id}>', '<{$k}>');">○</a>
                          <{else}>
                            ○
                          <{/if}>
                        </font>
                      <{/if}>
                    </td>
                  <{/foreach}>
                <{/if}>
              </tr>
            <{/foreach}>
            <!-- 一覧の内容 END -->
          </table>  
          <!-- 一覧 END -->
          
          <!-- ページ選択 START -->
          <{include file="hiyari_paging1.tpl"}>
          <!-- ページ選択 END -->                        
        </td>
        <!-- 右側 END -->
      </tr>
    </table>
  </form>
  <!-- レポーティング本体 END -->
</td>
</tr>
</table>
<!-- BODY全体 END -->

</body>
</html>
