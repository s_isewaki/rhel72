<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
  <title><{$INCIDENT_TITLE}> | お知らせ管理</title>
	<meta http-equiv="imagetoolbar" content="no" />
	<link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/news_menu.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/pagination.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript">
    $(function(){
      //チェックボックスのチェックがある時のtr
      $('.checkbox').click(function(e) {
        $(this).closest('tr').toggleClass('checked');
      })
    })
    
    function deleteNews() {
      if (confirm('削除してよろしいですか？')) {
        document.list_form.action = "hiyari_news_delete.php";
        document.list_form.submit();
      }
    }
  </script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
</head>

<body id="top">
  <{*ヘッダー*}>
  <{include file="hiyari_header2.tpl"}>

	<div id="content" class="clearfix <{$tab_info.total_width_class}>">
		<div id="sub" class="clearfix">
			<div id="sub_menu" class="color01 radius">
				<h2><img src="img/side_nav_txt01.gif" alt="MENU" /></h2>
				<ul>
					<li class="active"><a href="#">お知らせ管理</a></li>
					<li class="none"><a href="hiyari_news_register.php?session=<{$session}>">お知らせ登録</a></li>
				</ul>
			</div>
    </div>
    
		<div id="main" class="<{$tab_info.main_width_class}>">
      <{* 一覧 *}>
      <div id="main_list" class="color02 radius">
        <form name="list_form" action="hiyari_news_menu.php" method="post">
          <input type="hidden" name="session" value="<{$session}>">
          <input type="hidden" name="page" value="<{$page}>">
          <input type="hidden" name="mode" value="">
          
          <{* 実行アイコン *}>
          <ul id="main_nav" class="clearfix">
            <li class="main_nav_90"><a href="#" onclick="deleteNews();">削除</a></li>
          </ul>
          
          <table class="list">
            <tr>
              <th width="11">&nbsp;</th>
              <th width="70">登録日</th>
              <th width="70">カテゴリ</th>
              <th>タイトル</td>
              <th width="160">掲載期間</th>
              <th width="80" class="none">参照者一覧</th>
            </tr>
            <{foreach from=$news_list key=news_id item=news}>
              <tr>
                <td>
                  <span class="checkbox_wrap">
                    <input name="news_ids[]" type="checkbox" class="checkbox" value="<{$news_id}>">
                  </span>
                </td>
                <td>
                  <{$news.news_date}>
                </td>
                <td>
                  <{$news.category}>
                </td>
                <td>
                  <a href="hiyari_news_update.php?session=<{$session}>&news_id=<{$news_id}>&page=<{$page}>">
                    <span class="ellipsis" <{$news.style}>>
                      <{$news.news_title}>
                    </span>
                  </a>
                </td>
                <td>
                  <{$news.news_begin}> 〜 <{$news.news_end}>
                </td>
                <td class="none">
                    <{if $news.record_flg == "t"}>
                      <a href="hiyari_news_refer_list.php?session=<{$session}>&news_id=<{$news_id}>&menu_page=<{$page}>">参照者一覧</a>
                    <{else}>
                      &nbsp;
                    <{/if}>
                </td>
              </tr>
            <{/foreach}>
          </table>
          <{include file="hiyari_paging2.tpl"}>
        </form>
      </div>
    </div>
  </div>
	<div id="footer">
		<p>Copyright Comedix all right reserved.</p>
	</div>
</body>
</html>