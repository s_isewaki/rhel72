<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
    jQuery.noConflict();
    var j$ = jQuery;
</script>
<script type="text/javascript">
<!--
// 返信
function rp_action_return() {
    location.href = "hiyari_rp_mail_input.php?session=<{$session}>&mode=return&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>";
}
// 転送
function rp_action_forward(){
    location.href = "hiyari_rp_mail_input.php?session=<{$session}>&mode=forward&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>";
}

// メッセージ表示
function rp_action_message_disp() {
    location.href = "hiyari_rp_mail_disp.php?session=<{$session}>&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>";
}

//進捗登録
function rp_action_progress() {
    location.href = "hiyari_rp_progress.php?session=<{$session}>&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>&report_id=<{$report_id}>";
}
//印刷
function rp_action_print()
{

<{if $patient_non_print_flg}>
    var patient_non_print_flg = true;
<{else}>
    var patient_non_print_flg = false;
<{/if}>
<{if $patient_non_print_select_flg}>
    patient_non_print_flg = !confirm("患者ID、氏名の印刷を有効にしますか？");
<{/if}>
    var param_patient_non_print = "false";
    if(patient_non_print_flg)
    {
        param_patient_non_print = "true";
    }


<{if $gamen_mode == "analysis"}>
    var url = "hiyari_report_print.php?session=<{$session}>&report_id=<{$report_id}>&patient_non_print=" + param_patient_non_print;
<{elseif $gamen_mode == "update"}>
    //var url = "hiyari_report_print.php?session=<{$session}>&report_id=<{$report_id}>&eis_id=<{$mail_eis_id}>&gamen_mode=<{$gamen_mode}>&patient_non_print=" + param_patient_non_print;
        <{if $sel_job != ""}>//指定様式の時
            var url = "hiyari_report_print.php?session=<{$session}>&report_id=<{$report_id}>&eis_id=<{$mail_eis_id}>&gamen_mode=<{$gamen_mode}>&patient_non_print=" + param_patient_non_print;
        <{else}>//全項目のとき
            var url = "hiyari_report_print.php?session=<{$session}>&report_id=<{$report_id}>&eis_id=&gamen_mode=<{$gamen_mode}>&patient_non_print=" + param_patient_non_print;
        <{/if}>
<{else}>
    var url = "hiyari_report_print.php?session=<{$session}>&report_id=<{$report_id}>&eis_id=<{$mail_eis_id}>&patient_non_print=" + param_patient_non_print;
<{/if}>
    window.open(url);
}

//起動時の処理
function loadaction()
{
<{if $readonly_flg == false && $eis_select_able}>
    set_eis_input_list();
    set_eis_no(<{$sel_eis_no}>);
<{/if}>
    disp_update_doctor_have();

    if(window.area_605_others_disp_change){area_605_others_disp_change();}
    if(window.area_605_disp_change){area_605_disp_change();}
    if(window.area_295_disp_change){area_295_disp_change();}
    if(window.area_570_disp_change){area_570_disp_change();}
    if(window.area_580_disp_change){area_580_disp_change();}
    if(window.area_1400_disp_change){area_1400_disp_change();}
    if(window.area_1410_disp_change){area_1410_disp_change();}
    if(window.area_1410_20_disp_change){area_1410_20_disp_change();}
    if(window.area_140_disp_change){area_140_disp_change(0);area_140_disp_change(1);area_140_disp_change(2);area_140_disp_change(3);area_140_disp_change(4);area_140_disp_change(5);}
    if(window.area_150_20_disp_change){area_150_20_disp_change();}

<{if $auto_reload}>
  start_auto_session_update();
<{/if}>

<{if $gamen_mode == 'analysis' }>
	if(window.opener && !window.opener.closed && window.opener.reload_page) {
		window.opener.reload_page();
    }
<{/if}>
}

function load_item() {
    cng_scene(document.FRM_MAIN._700_10.value);
    cng_kind(document.FRM_MAIN._700_10.value);
    cng_content(document.FRM_MAIN._700_10.value);
    cng_scene_item(document.FRM_MAIN._710_10.value);
    cng_kind_item(document.FRM_MAIN._740_10.value);
    cng_content_item(document.FRM_MAIN._770_10.value);

    disp_kind();
}

function load_item_2010() {
    cng_kind_2010(document.FRM_MAIN._900_10.value);
    cng_scene_2010(document.FRM_MAIN._900_10.value);
    cng_content_2010(document.FRM_MAIN._900_10.value);
    cng_kind_item_2010(document.FRM_MAIN._910_10.value);
    cng_scene_item_2010(document.FRM_MAIN._940_10.value);
    cng_content_item_2010(document.FRM_MAIN._970_10.value);

    disp_kind_2010();
}

function disp_kind() {
    var id   = document.FRM_MAIN._700_10.value;
    var disp = document.getElementById("kind_disp");

    if(id == 2 || id == 3) {
        disp.style.display = '';
    } else {
        disp.style.display = 'none';
    }
}

function disp_kind_2010() {
    var id   = document.FRM_MAIN._900_10.value;
    var disp = document.getElementById("kind_disp_2010");

    var kind = document.FRM_MAIN._910_10.options.length > 1 ? document.FRM_MAIN._910_10.options[1].value : "";
//  var kind = document.FRM_MAIN._910_10.options[1].value;

    if( kind == "") {
        disp.style.display = 'none';
    } else {
        disp.style.display = '';
    }
}

function undisp(id) {
    ele1 = document.getElementById("add_item_1");
    ele2 = document.getElementById("add_item_2");
    ele3 = document.getElementById("add_item_3");

    if(id != 3) ele1.style.display = 'none';
    if(id != 6) ele2.style.display = 'none';
    if(id != 8) ele3.style.display = 'none';
}

function disp_hide_1100(OBJ) {
    if(document.getElementById("id_1100_10_1").checked == true) {
        document.getElementById('info_1').style.display='';
        document.getElementById('info_2').style.display='none';
    } else if(document.getElementById("id_1100_10_2").checked == true) {
        document.getElementById('info_2').style.display='';
        document.getElementById('info_1').style.display='none';
    }
}

function disp_hide_scene_content_2010() {
    if(document.getElementById("id_900_10").value == "") {
        document.getElementById("scene_content_2010").style.display = 'none';
        document.getElementById("id_910_10").value = "";
        document.getElementById("id_920_10").value = "";
        document.getElementById("id_930_10").value = "";
        document.getElementById("id_940_10").value = "";
        document.getElementById("id_950_10").value = "";
        document.getElementById("id_960_10").value = "";
        document.getElementById("id_970_10").value = "";
        document.getElementById("id_980_10").value = "";
        document.getElementById("id_990_10").value = "";
        <{if in_array(1300, $grp_flags)}>
        document.getElementById("id_1000_5").value = "";
        //document.getElementById("id_1000_10").value = "";
        document.getElementById("id_1000_15").value = "";
        <{/if}>
        <{if in_array(1310, $grp_flags)}>
        document.getElementById("id_1000_20").value = "";
        document.getElementById("id_1000_25").value = "";
        document.getElementById("id_1000_30").value = "";
        <{/if}>
        <{if in_array(1320, $grp_flags)}>
        document.getElementById("id_1000_35").value = "";
        document.getElementById("id_1000_40").value = "";
        document.getElementById("id_1000_45").value = "";
        document.getElementById("id_1000_50").value = "";
        document.getElementById("id_1000_55").value = "";
        <{/if}>
            } else {
        document.getElementById("scene_content_2010").style.display = '';
    }
}

function disp_hide_scene_content() {
    if(document.getElementById("id_700_10").value == "") {
        document.getElementById("scene_content").style.display = 'none';
        document.getElementById("id_710_10").value = "";
        document.getElementById("id_720_10").value = "";
        document.getElementById("id_730_10").value = "";
        document.getElementById("id_740_10").value = "";
        document.getElementById("id_750_10").value = "";
        document.getElementById("id_760_10").value = "";
        document.getElementById("id_770_10").value = "";
        document.getElementById("id_780_10").value = "";
        document.getElementById("id_790_10").value = "";
        document.getElementById("id_800_5").value = "";
        document.getElementById("id_800_10").value = "";
        document.getElementById("id_800_15").value = "";
        document.getElementById("id_800_20").value = "";
        document.getElementById("id_800_25").value = "";
        document.getElementById("id_800_30").value = "";
        document.getElementById("id_800_35").value = "";
        document.getElementById("id_800_40").value = "";
        document.getElementById("id_800_45").value = "";
        document.getElementById("id_800_50").value = "";
        document.getElementById("id_800_55").value = "";
        document.getElementById("id_800_60").value = "";
        document.getElementById("id_800_65").value = "";
        document.getElementById("id_800_70").value = "";
    } else {
        document.getElementById("scene_content").style.display = '';
    }
}


// bodyタグのonloadイベントはこちら
j$(document).ready(function(){
    loadaction();
    <{if in_array(900, $grp_flags)}>hide_must_2010();<{/if}>
    <{if in_array(700, $grp_flags)}>disp_hide_scene_content();load_item();<{/if}>
    <{if in_array(800, $grp_flags)}>undisp(document.FRM_MAIN._700_10.value);<{/if}>
    <{if in_array(900, $grp_flags)}>disp_hide_scene_content_2010();load_item_2010();disp_item_2010(document.FRM_MAIN._900_10.value);<{/if}>
    <{if in_array(1100, $grp_flags)}>disp_hide_1100(this.form);<{/if}>
    <{if $subject == 1}>change_classification();<{/if}> 
});
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/incident.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
  font { font-family: ＭＳ Ｐゴシック, Osaka; }
  table { border:0; }
  .list {border-collapse:collapse;}
  .list td {border:#35B341 solid 1px; }
  .desc td {border-style:none;}
  
  td.item_selection{
    border-right:none;
  }
  td.item_btn{
    border-left:none;
    text-align:right;
    width:40px;
    vertical-align:top;
  }  
  .item_set{
      display:inline-block;
      margin-right:1em;
      white-space:nowrap;
  }
</style>
</HEAD>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
  <div id="all_contents">
    <script type="text/javascript">
      var all_contents = document.getElementById("all_contents");
    </script>
    <a id="pagetop" name="pagetop"></a>

    <!-- セッションタイムアウト防止 START -->
    <{if $auto_reload}>
      <script type="text/javascript">
        function start_auto_session_update()
        {
            //1分(60*1000ms)後に開始
            setTimeout(auto_sesson_update,60000);
        }
        function auto_sesson_update()
        {
            //セッション更新
            document.session_update_form.submit();

            //1分(60*1000ms)後に最呼び出し
            setTimeout(auto_sesson_update,60000);
        }
      </script>
      <form name="session_update_form" action="hiyari_session_update.php" method="post" target="session_update_frame">
        <input type="hidden" name="session" value="<{$session}>">
      </form>
      <iframe name="session_update_frame" width="0" height="0" frameborder="0"></iframe>
    <{/if}>
    <!-- セッションタイムアウト防止 END -->

    <!-- ヘッダー START -->
    <{php}>
      require_once("hiyari_common.ini");
      show_hiyari_header_for_sub_window($this->_tpl_vars['PAGE_TITLE']);
    <{/php}>
    <!-- ヘッダー END -->

    <table width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td>
          <!-- 上部 START -->
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>                
              <!-- 実行アイコン START -->
              <td>
                <table cellspacing="0" cellpadding="0">
                  <tr>
                    <{if !$readonly_flg}>
                      <{if $gamen_mode == "new" || $gamen_mode == "shitagaki"}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td valign="middle" align="center"><img src="img/hiyari_rp_shitagaki.gif" style="cursor: pointer;" onclick="if(chkShitagakiSubmit()){document.FRM_MAIN.shitagaki.value='clicked';document.FRM_MAIN.submit();};"></td>
                      <{/if}>
                      
                      <{if $gamen_mode != "cate_update"}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td valign="middle" align="center"><img src="img/right.gif" height="30" style="cursor: pointer;" onclick="if(chkSubmit()){document.FRM_MAIN.next.value='clicked';document.FRM_MAIN.submit();};"></td>
                      <{/if}>
                    <{/if}>
                        
                    <{if $gamen_mode == "cate_update"}>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                      <td valign="middle" align="center"><img src="img/hiyari_rp_shitagaki.gif" height="30" style="cursor: pointer;" onclick="if(chkSubmit()){document.FRM_MAIN.report_update.value='clicked';document.FRM_MAIN.submit();};"></td>
                    <{/if}>

                    <{if !$readonly_flg}>
                      <{if $mail_id != "" || $progres_edit_flg}>
                        <td rowspan="2" align="center" width="3">&nbsp</td>
                        <td rowspan="2" align="center" bgcolor="#35B341"><img src="img/spacer.gif" width="1" height="8" alt=""></td>
                      <{/if}>
                    <{/if}>
                    
                    <{if $mail_id != ""}>
                      <{if $mail_id_mode == "recv" && $return_btn_show_flg}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td align="center"><img src="img/hiyari_rp_return.gif" width="32" height="29" style="cursor: pointer;" onclick="rp_action_return();"></td>
                      <{/if}>
                      <{if $forward_btn_show_flg}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td align="center"><img src="img/hiyari_rp_forward.gif" width="32" height="29" style="cursor: pointer;" onclick="rp_action_forward();"></td>
                      <{/if}>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                      <td align="center"><img src="img/hiyari_rp_mail_disp.gif" height="30" style="cursor: pointer;" onclick="rp_action_message_disp();"></td>
                    <{/if}>
                    
                    <{if $progres_edit_flg}>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                      <td align="center"><img src="img/hiyari_rp_progress.gif" width="38" height="30" style="cursor: pointer;" onclick="rp_action_progress();"></td>
                    <{/if}>
                    
                    <{if $gamen_mode == "update" || $gamen_mode == "analysis" || $gamen_mode == "shitagaki"}>
                      <{if $print_btn_show_flg}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td align="center"><img src="img/hiyari_rp_print.gif" style="cursor: pointer;" onclick="rp_action_print();"></td>
                      <{/if}>
                    <{/if}>
                  </tr>
                  
                  <tr>
                    <{if !$readonly_flg}>
                      <{if $gamen_mode == "new" || $gamen_mode == "shitagaki"}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td align="center"><font class="j12" color="#13781D">下書き保存</font></td>
                      <{/if}>
                      
                      <{if $gamen_mode != "cate_update"}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td align="center"><font class="j12" color="#13781D">内容確認</font></td>
                      <{/if}>
                    <{/if}>
                    
                    <{if $gamen_mode == "cate_update"}>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                      <td align="center"><font class="j12" color="#13781D">更新</font></td>
                    <{/if}>

                    <{if $mail_id != ""}>
                      <{if $mail_id_mode == "recv" && $return_btn_show_flg}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td align="center"><font class="j12" color="#13781D">返信</font></td>
                      <{/if}>
                      <{if $forward_btn_show_flg}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td align="center"><font class="j12" color="#13781D">転送</font></td>
                      <{/if}>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                      <td align="center"><font class="j12" color="#13781D">表示</font></td>
                    <{/if}>

                    <{if $progres_edit_flg}>
                      <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                      <td align="center"><font class="j12" color="#13781D">進捗登録</font></td>
                    <{/if}>
                    
                    <{if $gamen_mode == "update" || $gamen_mode == "analysis" || $gamen_mode == "shitagaki"}>
                      <{if $print_btn_show_flg}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td align="center"><font class="j12" color="#13781D">印刷</font></td>
                      <{/if}>
                    <{/if}>
                  </tr>
                </table>
              </td>
              <!-- 実行アイコン END -->
              
              <{if !$readonly_flg}>
                <!-- 職種選択 START -->
                <td width="400">
                  <{if $eis_select_able}>
                    <table width="400" cellspacing="0" cellpadding="2" class="list">
                      <tr>
                        <td  align="center" bgcolor="#DFFFDC"><font class="j12">対象職種・報告書様式</font></td>
                      </tr>
                      <tr>
                        <td align="center">
                          <nobr>
                          <form name='job_change_form' action='' method='post' onSubmit="return false;">
                            <select name="sel_job_input" onchange="set_eis_input_list();change_job();">
                              <{section loop=$job_ids name=i}>
                              <option value="<{$job_ids[i]}>"<{if $job_ids[i] == $sel_job}> selected<{/if}>><{$job_names[i]}>
                              <{/section}>
                            </select>

                            <select name="sel_eis_input" onchange="change_job();">
                            </select>
                          </form>
                          </nobr>
                        </td>
                      </tr>
                    </table>
                    <script type="text/javascript">
                        function change_job()
                        {
                            //document.FRM_MAIN.sel_job.value = sel_job_input.value;
                            document.FRM_MAIN.sel_job.value = job_change_form.sel_job_input.options[job_change_form.sel_job_input.selectedIndex].value;
                            document.FRM_MAIN.sel_eis_no.value = job_change_form.sel_eis_input.options[job_change_form.sel_eis_input.selectedIndex].value;
                            document.FRM_MAIN.sel_eis_change.value='changed';
                            document.FRM_MAIN.submit();
                        }
                        function set_eis_input_list(){
                          var job_box = job_change_form.sel_job_input;//document.getElementById("sel_job_input");
                          var eis_box = job_change_form.sel_eis_input;//document.getElementById("sel_eis_input");
                          var job_id = job_box.options[job_box.selectedIndex].value;

                          deleteAllOptions(eis_box);
                          if (false) {

                          }
                          <{section loop=$job_ids name=i}>
                          else if(job_id == "<{$job_ids[i]}>") {
                          <{*//デフォルトの様式番号 *}>
                          <{section loop=$eis_job_link_info name=ii}>
                            <{if $eis_job_link_info[ii].job_id == $job_ids[i] && $eis_job_link_info[ii].default_flg == "t"}>
                            var default_eis_no = "<{$eis_job_link_info[ii].eis_no}>";
                            <{/if}>
                          <{/section}>
                          <{*//様式をリストに追加 *}>
                          <{section loop=$eis_job_link_info name=ii}>
                            <{if $eis_job_link_info[ii].job_id == $job_ids[i]}>
                            addOption(eis_box, "<{$eis_job_link_info[ii].eis_no}>", "<{$eis_job_link_info[ii].eis_name}>", default_eis_no);
                            <{/if}>
                          <{/section}>
                          }
                          <{/section}>
                          else {

                          }
                        }
                        

                        function deleteAllOptions(box)
                        {
                            for (var i = box.length - 1; i >= 0; i--)
                            {
                                box.options[i] = null;
                            }
                        }

                        function addOption(box, value, text, selected)
                        {
                            var opt = document.createElement("option");
                            opt.value = value;
                            opt.text = text;
                            if (selected == value)
                            {
                                opt.selected = true;
                            }
                            box.options[box.length] = opt;
                            try {box.style.fontSize = 'auto';} catch (e) {}
                            box.style.overflow = 'auto';
                        }


                        function set_eis_no(eis_no)
                        {
                            var obj = job_change_form.sel_eis_input;//document.getElementById("sel_eis_input");
                            set_select_ctl_from_value(obj,eis_no);
                        }

                        function set_select_ctl_from_value(obj,val)
                        {
                            var is_ok = false;
                            for(var i=0; i<obj.options.length; i++)
                            {
                                if(obj.options[i].value == val)
                                {
                                    obj.selectedIndex = i;
                                    is_ok = true;
                                    break;
                                }
                            }
                            return is_ok;
                        }
                    </script>
                  <{/if}>
                </td>
                <!-- 職種選択 END -->
                
                <td width="10">&nbsp;</td>

                <!-- 画面進捗 START -->
                <td width="400">
                  <table width="100%" cellspacing="0" cellpadding="2" class="list">
                    <tr>
                      <td align="center" bgcolor="#FFDDFD"><font class="j12">出来事報告の流れ</font></td>
                    </tr>
                    <tr>
                      <td align="center">
                        <{if $gamen_mode == "cate_update"}>
                          <nobr>
                          <font class="j14">
                          <b><span style="background-color:#35B341;color:#FFFFFF;padding:2px;">
                          1.出来事の記入
                          </span></b>
                          &nbsp;
                          ⇒
                          &nbsp;
                          (更新)
                          </font>
                          </nobr>
                        <{else}>
                          <nobr>
                          <font class="j14">
                          <b><span style="background-color:#35B341;color:#FFFFFF;padding:2px;">
                          1.出来事の記入
                          </span></b>
                          &nbsp;
                          ⇒
                          &nbsp;
                          2.記入内容を確認
                          &nbsp;
                          ⇒
                          &nbsp;
                          (送信)
                          </font>
                          </nobr>
                        <{/if}>
                      </td>
                    </tr>
                  </table>
                </td>
              <{/if}>
              <!-- 画面進捗 END -->
            </tr>
          </table>
          <!-- 上部 END -->
        

          <form name='FRM_MAIN' action='hiyari_easyinput.php?callerpage=hiyari_easyinput.php' method='post' onSubmit="return false;" >
            <{* 確認画面で様式を取得するためにデータを送る START *}>
            <input type="hidden" name="eis_title_id" value="<{$eis_title_id}>">
            <{* 確認画面で様式を取得するためにデータを送る END *}>

            <!-- ポストバック情報 -->
            <input type='hidden' name='is_postback'         id='is_postback'         value='ture'>
            <input type='hidden' name='next'                id='next'                value=''>
            <input type='hidden' name='shitagaki'           id='shitagaki'           value=''>
            <input type='hidden' name='report_update'       id='report_update'       value=''>
            <input type='hidden' name='sel_eis_change'      id='sel_job_change'      value=''>
            <input type='hidden' name='registrant_post_change' id='registrant_post_change' value=''>
			<input type='hidden' name='subject_mode'            id='subject_mode'        value='<{$subject_mode}>'>
<input type='hidden' name='callerpage'            id='callerpage'        value='<{$callerpage}>'>

            <!-- セッション情報 -->
            <input type='hidden' name='session'             id='session'             value='<{$session}>'>
            <input type='hidden' name='hyr_sid'             id='hyr_sid'             value='<{$hyr_sid}>'>

            <!-- 画面パラメータ -->
            <input type='hidden' name='gamen_mode'          id='gamen_mode'          value='<{$gamen_mode}>'>
            <input type='hidden' name='report_id'           id='report_id'           value='<{$report_id}>'>
            <input type='hidden' name='mail_id'             id='mail_id'             value='<{$mail_id}>'>
            <input type='hidden' name='mail_id_mode'        id='mail_id_mode'        value='<{$mail_id_mode}>'>
            <input type='hidden' name='cate'                id='cate'                value='<{$cate}>'>

            <!-- 職種／様式 (JavaScriptにより動的変更／ポストバック用) -->
            <input type='hidden' name='sel_job'             id='sel_job'             value='<{$sel_job}>'>
            <input type='hidden' name='sel_eis_no'          id='sel_eis_no'          value='<{$sel_eis_no}>'>

            <!-- 職種／様式 (JavaScriptによる変更なし／確認画面送信用) -->
            <input type='hidden' name='job_id'              id='job_id'              value='<{$sel_job}>'>
            <input type='hidden' name='eis_no'              id='eis_no'              value='<{$sel_eis_no}>'>

            <input type='hidden' name='style_code'          id='style_code'         value='<{$style_code}>'>

            <!-- 主治医 -->
            <input type='hidden' name='doctor_emp_id'       id='doctor_emp_id'       value='<{$doctor_emp_id}>'>

            <!-- 特殊様式情報 -->
            <input type="hidden" name="patient_use" value="<{$patient_use_csv}>">
            <input type="hidden" name="eis_400_use" value="<{$eis_400_use_csv}>">
            <input type="hidden" name="eis_410_use" value="<{$eis_410_use_csv}>">
            <input type="hidden" name="eis_420_use" value="<{$eis_420_use_csv}>">
            <input type="hidden" name="eis_430_use" value="<{$eis_430_use_csv}>">
            <input type="hidden" name="eis_440_use" value="<{$eis_440_use_csv}>">
            <input type="hidden" name="eis_450_use" value="<{$eis_450_use_csv}>">
            <input type="hidden" name="eis_460_use" value="<{$eis_460_use_csv}>">
            <input type="hidden" name="eis_470_use" value="<{$eis_470_use_csv}>">
            <input type="hidden" name="eis_480_use" value="<{$eis_480_use_csv}>">
            <input type="hidden" name="eis_490_use" value="<{$eis_490_use_csv}>">

            <!-- タイムラグ -->
            <input type="hidden" name="first_send_time" value="<{$first_send_time}>">
            <input type="hidden" id="_105_75" name="_105_75" value="<{$vals[105][75][0]}>">


            <!-- 所属情報 -->
            <img src="img/spacer.gif" width="1" height="5" alt=""><br>
            <table width="100%" cellspacing="0" cellpadding="0">
              <tr>

              <td align="left">
                <table cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="10">
                    <img src="img/spacer.gif" width="1" height="5" alt=""><br>
                    </td>
                    <td>
                    
                    <!-- タイムラグ -->
                    <{if $is_sm_emp_flg == true 
                     && $vals[105][75][0] != '' 
                     && $disp_taimelag_flg == true
                     && $gamen_mode != 'cate_update'
                     && $gamen_mode != 'shitagaki'
                     && $first_send_time != ''
                    }>
                      <table cellspacing="0" cellpadding="2" class="list">
                        <tr height="22">
                          <td bgcolor="#DFFFDC">
                            <nobr>
                            <font class="j12">
                            第一報送信時刻
                            </font>
                            </nobr>
                          </td>
                            
                          <td>
                            <nobr>
                            <font class="j12">
                            <{$first_send_time}>
                            </span>
                            </font>
                            </nobr>
                          </td>
                          
                          <td bgcolor="#DFFFDC">
                            <nobr>
                            <font class="j12">
                            発見時刻から第一報送信時刻までの時間
                            </font>
                            </nobr>
                          </td>
                          
                          <td>
                            <nobr>
                            <font class="j12">
                            <{$timelag_view}>
                            </font>
                            </nobr>
                          </td>							
                        </tr>
                      </table>
                    <{/if}>
                    </td>
                  </tr>
                </table>
              </td>

                <td align="right">
                <table cellspacing="0" cellpadding="0">
                  <tr>
                    <td>
                    <{if $report_no != ""}>
                      <table cellspacing="0" cellpadding="2" class="list">
                        <tr height="22">
                          <td bgcolor="#DFFFDC">
                            <nobr>
                            <font class="j12">
                            事案番号
                            </font>
                            </nobr>
                          </td>
                          <td>
                            <nobr>
                            <font class="j12">
                            <{$report_no}>
                            </font>
                            </nobr>
                          </td>
                        </tr>
                      </table>
                    <{/if}>
                    </td>
                    <td width="10">
                    <img src="img/spacer.gif" width="1" height="5" alt=""><br>
                    </td>
                    <td>
                    <{php}>
                      require_once("./hiyari_post_select_box.ini");
                      show_post_select_box_for_report_input(
                        $this->_tpl_vars['session'],
                        $this->_tpl_vars['con'],
                        $this->_tpl_vars['fname'],
                        $this->_tpl_vars['registrant_class'],
                        $this->_tpl_vars['registrant_attribute'],
                        $this->_tpl_vars['registrant_dept'],
                        $this->_tpl_vars['registrant_room'],
                        $this->_tpl_vars['readonly_flg']
                      );
                    <{/php}>
                    </td>

                    <td width="10">
                    <img src="img/spacer.gif" width="1" height="5" alt=""><br>
                    </td>
                  <tr>
                </table>

                </td>
              </tr>
            </table>
            <img src="img/spacer.gif" width="1" height="5" alt=""><br>
            <{include file="hiyari_edit_easyinput.tpl" input_mode=$gamen_mode}>
            <br>
          </form>

<!-- ファイル添付 START 	//20130630-->
            <{if  $file_attach == "1" && $style_code_for_tmp_file == "2" }>
            <br>
				<form method="post" enctype="multipart/form-data" action="hiyari_file_upload.php">

					<script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
					<script type="text/javascript" src="js/jquery/jquery-upload-1.0.2.min.js"></script>
					<script>
					jQuery(function($){
					 
						$(document).on('click', '.delF', function(){
							//alert(this.getAttribute("name"));
							$.ajax({
								context: self,
								url: 'hiyari_file_upload.php',
								type: 'post',
								data: {
									func_id: this.getAttribute("name"),
									StateTrns: 'delFile',
									Ids: '<{$report_id}>',
									fuld_session: '<{$session}>'
								},
								success:function(resDel){
									//alert('success');
									resDel = resDel.replace(/\r\n/g, "");
									var atchList = $('#atchList');
									var wwwTmp = $('#'+resDel);
									wwwTmp.remove();
								}
							});
						});

						$(document).on('click', '.delFR', function(){
							if(!confirm("ファイルが完全削除されます。よろしいですか？"))
							{
								return false;
							}
							
							$.ajax({
								context: self,
								url: 'hiyari_file_upload.php',
								type: 'post',
								data: {
									func_id: this.getAttribute("name"),
									StateTrns: 'delFileReg',
									Ids: '<{$report_id}>',
									fuld_session: '<{$session}>'
								},
								success:function(resDel){
									//alert('success');
									resDel = resDel.replace(/\r\n/g, "");
									var atchList = $('#atchList');
									var wwwTmp = $('#'+resDel);
									wwwTmp.remove();
								}
							});
						});
					 


					 
						$('#img').change(function(){
							//アップロード、表示
							var postD = $("form").serialize();
							$(this).upload('hiyari_file_upload.php',
								postD,
								function(resHtml){
									var atchList = $('#atchList');
									atchList.html(resHtml);
								},'html'
							);
						});
					});
					
					</script>

					<table border="0" width="100%" cellspacing="0" cellpadding="2" >
						<tr>
							<td valign="top" width="30%">
								<table width="100%" cellspacing="0" cellpadding="2" >
									<tr>
										<td>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">  
												<tr>
													<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
													<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
													<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
												</tr>
												<tr>
													<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
													<td class="list">
														<table width="100%">
															<tr>
																<td width="100" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイル添付</font></td>
																<td bgcolor="white">
																	<input type='hidden' name='fuld_session' id='fuld_session' value='<{$session}>'>
																	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※<{$file_max_size}>まで</font>
																	<input type="file" size="40" value="" id="img" name="img">
																	<br><br>
																	<div id="atchList">
																		<{foreach from=$arr_file key=tmp_hy_key item=tmp_hy_file}>
																			<div id="<{$tmp_hy_key}>"><{$tmp_hy_file}><input type="button" name="<{$tmp_hy_key}>" class="delF" value="削除"/></div>
																		<{/foreach}>
																	</div>
																</td>
															</tr>
														</table>
													</td>
													<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
												</tr>
												<tr>
													<td><img src="img/r_3.gif" width="10" height="10"></td>
													<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
													<td><img src="img/r_4.gif" width="10" height="10"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="70%">

								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">  
												<tr>
													<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
													<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
													<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
												</tr>
												<tr>
													<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
													<td class="list">
														<table width="100%">
															<tr>
																<td class="list">
																	<div id="hyr_preview">
																	
															<{foreach from=$file_list item=tmp_hy_file}>
																<div  style="float: left; border : 10px  solid white" id="<{$tmp_hy_file.file_no}>">
																	<a href="<{$tmp_hy_file.view}>=<{$session}>&r=<{$report_id}>&u=<{$tmp_hy_file.file_no}>&gF=<{$tmp_hy_file.gazou_flg}>" <{$tmp_hy_file.target}> >
																		<img border=0 src="<{$tmp_hy_file.base}>" title="<{$tmp_hy_file.file_name}>"  >
																		<br><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$tmp_hy_file.file_name}></font>
																	</a>
																	<{if !$readonly_flg}>
																		<input type="button" name="<{$tmp_hy_file.file_no}>" class="delFR" value="削除"/>
																	<{/if}>
																	
																</div>
																
															<{/foreach}>
																	
																	</div>
																</td>
															</tr>
														</table>
													</td>
													<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
												</tr>
												<tr>
													<td><img src="img/r_3.gif" width="10" height="10"></td>
													<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
													<td><img src="img/r_4.gif" width="10" height="10"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<br>
				</form>
            <{/if}>

<!-- ファイル添付 END 	//20130630-->




          <!-- iwata患者情報取得フォームSTART -->
          <form name='iwata_patient_search_form' action='' method='post'>
            <input type="hidden" id="caller"         name="caller">
            <input type="hidden" id="target_date"    name="target_date">
            <input type="hidden" id="patient_id"     name="patient_id">
            <input type="hidden" id="patient_name"   name="patient_name">
            <input type="hidden" id="page"           name="page">
            <input type="hidden" id="callbacker_url" name="callbacker_url">
          </form>
          <!-- iwata患者情報取得フォームEND -->

          <!-- 患者情報取得汎用フォームSTART -->
          <form name='patient_search_form' action='' method='post'>
            <input type="hidden" id="caller"         name="caller">
            <input type="hidden" id="target_date"    name="target_date">
            <input type="hidden" id="patient_id"     name="patient_id">
            <input type="hidden" id="patient_name"   name="patient_name">
            <input type="hidden" id="page"           name="page">
            <input type="hidden" id="callbacker_url" name="callbacker_url">
          </form>
          <!-- 患者情報取得汎用フォームEND -->
        </td>
      </tr>
    </table>

    <{if $gamen_mode != "analysis"}>
      <table cellspacing="0" cellpadding="0">
        <tr>
          <td>
            &nbsp<a href="#pagetop">このページの先頭へ</a><br>
          </td>
          <!-- 実行アイコン２ START -->
          <td>
            <{if $readonly_flg == false}>
              <table cellspacing="0" cellpadding="0">
                <tr>
                  <{if $gamen_mode != "cate_update"}>
                    <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <td valign="middle" align="center"><img src="img/right.gif" height="30" style="cursor: pointer;" onclick="if(chkSubmit()){document.FRM_MAIN.next.value='clicked';document.FRM_MAIN.submit();};"></td>
                  <{/if}>
                </tr>
                <tr>
                  <{if $gamen_mode != "cate_update"}>
                    <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <td align="center"><font class="j12" color="#13781D">内容確認</font></td>
                  <{/if}>
                </tr>
              </table>
            <{/if}>
          </td>
          <!-- 実行アイコン２ END -->
        </tr>
      </table>
    <{/if}>

    <br>
  </div>
</body>
</html>
