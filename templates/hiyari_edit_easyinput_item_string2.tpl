<!--タイトル-->
<{if $tr}>
  '<tr height="22">\n'+
<{/if}>
    '<td bgcolor="#DFFFDC" width="250">'+
      '<font size="3" class="j12">'+
        '<span <{if in_array($i|cat:'_'|cat:$j, $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>'+
          '<{$grps[$i].easy_item_list[$j].easy_item_name|escape:"html"}>'+
        '</span>'+
        '<{if in_array($i|cat:'_'|cat:$j, $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}>'+
      '</font>'+
    '</td>\n'+

<!--データ-->
    '<td>\n'+
      <{if $grps[$i].easy_item_list[$j].easy_item_type == 'select'}>
        '<select id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" onchange="selectbox_onchange(this);">'+
          '<option value="">'+
          <{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}>
            <{if $item_element_no_disp[$i][$j] == "" || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}>
              '<option value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>">'+
                '<{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}>'+
              '</option>'+
            <{/if}>
          <{/section}>
        '</select>'+
            
        <{if isset($j2)}>
          '<select id="id_<{$i}>_<{$j2}>" name="_<{$i}>_<{$j2}>" onchange="selectbox_onchange(this);">'+
            '<option value="">'+
            <{section name=k loop=$grps[$i].easy_item_list[$j2].easy_list}>
              <{if $item_element_no_disp[$i][$j2] == "" || !in_array($grps[$i].easy_item_list[$j2].easy_list[k].easy_code,$item_element_no_disp[$i][$j2])}>
                '<option value="<{$grps[$i].easy_item_list[$j2].easy_list[k].easy_code}>">'+
                  '<{$grps[$i].easy_item_list[$j2].easy_list[k].easy_name|escape:"html"}>'+
                '</option>'+
              <{/if}>
            <{/section}>
          '</select>'+
        <{/if}>

      <{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'radio'}>
        <{if $radio_line_count == ""}>
          <{assign var="radio_line_count" value="4"}>
        <{/if}>
        '<table width="100%" cellspacing="0" cellpadding="0" class="sellist">'+
          '<tr>'+
            '<td style="width:25%">'+
              '<input type="radio" id="id_<{$i}>_<{$j}>_NON" name="_<{$i}>_<{$j}>[]" value="">'+
              '<label id="id_<{$i}>_<{$j}>_NON_label" for="id_<{$i}>_<{$j}>_NON"><font class="j12">未入力</font></label>'+
            '</td>'+          
            <{php}>$radio_col_index = 1;<{/php}>      
            <{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}>
              <{if $item_element_no_disp[$i][$j] == "" || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}>
                <{if (isset($grps[$i].easy_item_list[$j].easy_list[k].other_code))}>  <!--不明やその他の入力欄がある場合-->
                  <{php}>$radio_col_index=$this->_tpl_vars['radio_line_count'];<{/php}>
                  '</tr><tr>'+
                  '<td colspan="<{$radio_line_count}>">'+
                <{else}>
                  <{php}>
                    $radio_col_index++;
                    if($radio_col_index == $this->_tpl_vars['radio_line_count']+1) 
                    {
                      $radio_col_index=1;
                  <{/php}>
                      '</tr><tr>'+
                  <{php}>
                    }
                  <{/php}>
                  '<td style="width:25%">'+                  
                <{/if}>
                  <{assign var="easy_title" value='$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}>
                  '<input type="radio" id="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>" name="_<{$i}>_<{$j}>[]"'+
                    ' value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>">'+
                  '<label for="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>">'+
                    '<font class="j12">'+
                      '<{$easy_title}>'+
                      <{if isset($grps[$i].easy_item_list[$j].easy_list[k].other_code)}>  <{*不明やその他の入力欄*}>
                        <{assign var="other_code" value=$grps[$i].easy_item_list[$j].easy_list[k].other_code}>
                        '&nbsp;('+
                        '<input type="text" id="id_<{$i}>_<{$other_code}>" name="_<{$i}>_<{$other_code}>"'+
                          'value="" style="width:250;ime-mode:active">'+
                        ')'+
                      <{/if}>
                    '</font>'+
                  '</label>'+
                '</td>'+
              <{/if}>
            <{/section}>
          '</tr>'+
        '</table>'+
      <{/if}>
    '</td>\n'+
<{if $tr}>
  '</tr>\n'+
<{/if}>
