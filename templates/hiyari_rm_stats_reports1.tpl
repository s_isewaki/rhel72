<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix <{$INCIDENT_TITLE}> | 統計分析レポート表示</title>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript">
    //報告書表示(参照モード)
    function rp_action_update_from_report_id(report_id,mail_id) {
        var url = "hiyari_easyinput.php?session=<{$session}>&gamen_mode=update&report_id=" + report_id + "&callerpage=" + "<{$file}>" + "<{$non_update_flg}>"+"&mail_id=" + mail_id;
        show_rp_sub_window(url);
    }

    //レポーティング子画面を表示
    function show_rp_sub_window(url) {
        var h = window.screen.availHeight - 30;
        var w = window.screen.availWidth - 10;
        var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

        window.open(url, '_blank',option);
    }
  </script>
  <{php}>
   show_anonymous_emp_name_popup_js();
  <{/php}>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <style type="text/css">
    .list {border-collapse:collapse;}
    .list td {border:#35B341 solid 1px;}
  </style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
  <form name="mainform" action="#" method="post">
    <input type="hidden" name="report_id_list" value="<{$report_id_list}>">
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr height="32" bgcolor="#35B341">
        <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>統計分析レポート表示</b></font></td>
        <td>&nbsp;</td>
        <td width="10">&nbsp;</td>
        <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
      </tr>
    </table>
    
    <img src="img/spacer.gif" width="10" height="10" alt=""><br>
    <img src="img/spacer.gif" width="1" height="10" alt=""><br>
  
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
      </tr>
      <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td bgcolor="#F5FFE5">
          <table>
            <tr>
              <td>
                <table width="650" border="0" cellspacing="1" cellpadding="2" class="list">
                  <tr>
                    <td width="50" bgcolor="#DFFFDC" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">縦軸</font></td>
                    <td width="300" bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$vertical_label}></font></td>
                    <td width="300" bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$vertical_item_label}></font></td>
                  </tr>
                </table>
              </td>
              <{if $is_analysis_use}>
                <td rowspan="2" style="width:120px;text-align:center;">
                  <script>
                    function set_analysis_regist() {
                        var oReportId = document.getElementsByName('report_id');
                        var flg = false;
                        for ( var i=0; i<oReportId.length; i++ ) {
                            if(oReportId[i].checked == true) flg = true;
                        }
                        if(flg == false) {
                            alert('事案番号を選択してください');
                            return false;
                        }

                        var url = "hiyari_analysis_regist.php?session=<{$session}>";
                        var h = 400;
                        var w = 500;
                        var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
                        window.open(url, 'hiyari_analysis_regist', option);

                        var OBJ = document.mainform;
                        OBJ.action = "hiyari_analysis_regist.php?session=<{$session}>";
                        OBJ.target = 'hiyari_analysis_regist';
                        OBJ.mode.value = 'stats';
                        OBJ.submit();
                    }
                  </script>
                  <input type="hidden" name="mode" value="">
                  <img src="img/hiyari_rp_new.gif" style="cursor: pointer;" onClick="set_analysis_regist();"><br>
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">出来事分析</font>
                </td>
              <{/if}>
            </tr>
            <tr>
              <td>
                <table width="650" border="0" cellspacing="1" cellpadding="2" class="list">
                  <tr>
                    <td width="50" bgcolor="#DFFFDC" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">横軸</font></td>
                    <td width="300" bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$horizontal_label}></font></td>
                    <td width="300" bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$horizontal_item_label}></font></td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          
          <img src="img/spacer.gif" width="1" height="10" alt=""><br>

          <table width="1050" border="0" cellspacing="1" cellpadding="2" class="list">
            <tr bgcolor="#DFFFDC">
              <td width="20"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;</font></td>
              <td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事案番号</font></td>
              <td width="330"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題</font></td>
              <td width="220"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告部署</font></td>
              <td width="150"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告者</font></td>
                <{if $count_date_mode == "incident_date"}>
                  <td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生年月日</font></td>
                <{else}>
                  <td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告年月日</font></td>
                <{/if}>
              <td width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計数</font></td>
            </tr>
            
            <{foreach from=$list_data item=ld}>
              <tr bgcolor="#FFFFFF">
                <{if $is_analysis_use}>
                  <td><input type="radio" name="report_id" value="<{$ld.report_id}>"></td>
                <{else}>
                  <td>&nbsp;</td>
                <{/if}>
                <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$ld.report_no}></font></td>
                <td>
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <{if $ld.is_linkable}>
                      <a href="javascript:rp_action_update_from_report_id('<{$ld.report_id}>','<{$ld.mail_id}>')">
                    <{/if}> 
                    <{$ld.title}>
                    <{if $ld.is_linkable}>
                      </a>
                    <{/if}> 
                  </font>
                </td>
                <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$ld.class_nm}></font></td>
                <td>
                  <{if $ld.is_popup_name}>
                    <span onmousemove="name_popup_disp('<{$ld.real_name}>', 'regist', event);" onmouseout="closeDetail();" style="cursor:pointer;">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="blue"><B><{$ld.disp_emp_name}></B></font>
                    </span>                    
                  <{else}>
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$ld.disp_emp_name}></font>
                  <{/if}>
                </td>
                <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$ld.ymd}></font></td>
                <td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$ld.count}></font></td>
              </tr>
            <{/foreach}>
          </table>
        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
      </tr>
      <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
      </tr>
    </table>
    
    <input type="hidden" name="caller" value="hiyari_rm_stats_report.php" >
  </form>
</body>
</html>
