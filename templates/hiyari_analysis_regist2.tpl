<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
	<title>CoMedix <{$INCIDENT_TITLE}> | �����ʬ��</title>
	<meta http-equiv="imagetoolbar" content="no" />
	<link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/smoothness/jquery-ui-1.10.3.custom.min.css">
	<link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/hiyari_analysis_regist.js"></script>
	<script type="text/javascript" src="js/jquery/jquery-ui-1.10.2.custom.min.js"></script>  
  <script type="text/javascript" src="js/jquery/jquery.ui.datepicker-ja.min.js" charset="utf-8"></script>
  <script type="text/javascript">
    var w = 560;    
    var h = window.screen.availHeight;
    if (h > 750){
      h = 750;
    }
    window.resizeTo(w, h);
    
    jQuery(function($){
        <{*ʬ�ϥ��С�*}>
        <{if isset($analysis_member_id)}>
            add_emp_list('<{$analysis_member_id}>', '<{$analysis_member}>');
        <{/if}>

        <{*ʬ�ϻ���*}>
        <{if isset($analysis_problem_id)}>
            add_analysis_list('<{$analysis_problem_id}>', '<{$analysis_problem}>', 1);
        <{/if}>

        <{*�������*}>
        <{if isset($analysis_resemble_id)}>
            add_analysis_list('<{$analysis_resemble_id}>', '<{$analysis_resemble}>', 2);
        <{/if}>    
        
        <{*��������*}>
        $('.calendar_text').datepicker({showOn: 'focus', showOtherMonths: true, selectOtherMonths: true});
        $('.calendar_btn').click(function(){
            $(this).prev('input').datepicker( 'show' );
        });     
    })
  </script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
</head>

<body id="analysis_regist">
	<div id="header">
    <div class="inner">
      <h1>ʬ����Ͽ</h1>
    </div>
	</div>

	<div id="content" class="clearfix">
    <form name="form_list" method="POST" action="hiyari_analysis_regist.php">
      <div class="section">      
        <div class="color02 radius">
          <table class="v_title">
            <tr>
              <th width="100">ʬ���ֹ�</th>
              <td><{$analysis_no}></td>
            </tr>
            <tr>
              <th>������</th>
              <td><{$renewer}></td>
            </tr>
            <tr>
              <th>����ǯ����</th>
              <td><{$renewal_date}></td>
            </tr>
            <tr>
              <th>�����ȥ�</th>
              <td>
                <input type="text" name="analysis_title" value="<{$analysis_title}>" size=60 style="ime-mode:active">
              </td>
            </tr>
            <tr>
              <th>ʬ�ϼ�ˡ</th>
              <td>
                <input type="radio" name="analysis_method" value="general"<{if $analysis_method == '' || $analysis_method=='general'}> checked<{/if}>>����
                <input type="radio" name="analysis_method" value="4M4E"<{if $analysis_method=='4M4E'}> checked<{/if}>>���ͣ���
                <input type="radio" name="analysis_method" value="SHEL"<{if $analysis_method=='SHEL'}> checked<{/if}>>�ӣȣţ�
                <input type="radio" name="analysis_method" value="RCA" <{if $analysis_method=='RCA' }> checked<{/if}>>�ңã�                    
              </td>
            </tr>
            <tr>
              <th>
                ʬ�ϥ��С�<br/>
                <input type="button" class="button radius_mini" value="����̾��" onclick="call_emp_search('<{$session}>');">
              </th>
              <td id="disp_area"></td>
            </tr>
            <tr>
              <th>ɾ��ͽ�����</th>
              <td>
                <input type="text" id="progress_date" class="calendar_text" name="progress_date" value="<{$progress_date}>" style="width:100;border:#35B341 solid 2px;" readonly>
                <a class="calendar_btn" href="#">��������</a>
                <input type="button" class="button radius_mini" value="���ꥢ" onClick="clear_progress_date();">
              </td>
            </tr>
            <tr>
              <th>��Ľ</th>
              <td>
                <select name="analysis_progress">
                  <option value="1"<{if $analysis_progress == '1'}> selected<{/if}>>ʬ����
                  <option value="2"<{if $analysis_progress == '2'}> selected<{/if}>>ʬ�Ϻ�
                  <option value="3"<{if $analysis_progress == '3'}> selected<{/if}>>ɾ����
                  <option value="4"<{if $analysis_progress == '4'}> selected<{/if}>>ɾ����
                </select>                    
              </td>
            </tr>
            <tr>
              <th>
                ʬ�ϻ���<br/>
                <input type="button" class="button radius_mini" value="����" onClick="call_analysis_search(1,'<{$session}>');">
              </th>
              <td id="disp_area_1"></td>
            </tr>
            <tr>
              <th class="none">
                �������<br/>
                <input type="button" class="button radius_mini" value="����" onClick="call_analysis_search(2,'<{$session}>');">
              </th>
              <td class="none" id="disp_area_2"></td>
            </tr>
          </div>
        </table>
      </div>
      <div id="main_btn" class="section">
        <{if $can_regist}>
          <input type="button" class="button radius_mini" value="��Ͽ" onclick="analysis_regist('<{$session}>','<{$moveto_analysis}>');">
        <{/if}>
      </div>
      
      <input type="hidden" name="session" value="<{$session}>">
      <input type="hidden" name="analysis_id" value="<{$analysis_id}>">
      <input type="hidden" name="analysis_no" value="<{$analysis_no}>">
      <input type="hidden" name="renewer" value="<{$renewer}>">
      <input type="hidden" name="renewal_date" value="<{$renewal_date}>">
      <input type="hidden" name="disp_area_ids" value="" id="disp_area_ids">
      <input type="hidden" name="disp_area_names" value="" id="disp_area_names">
      <input type="hidden" name="disp_area_ids_1" value="" id="disp_area_ids_1">
      <input type="hidden" name="disp_area_names_1" value="" id="disp_area_names_1">
      <input type="hidden" name="disp_area_ids_2" value="" id="disp_area_ids_2">
      <input type="hidden" name="disp_area_names_2" value="" id="disp_area_names_2">
      <input type="hidden" name="is_postback" value="true">
      <input type="hidden" name="mode" value="<{if $mode=='update'}>update<{else}>regist<{/if}>">
      <input type="hidden" name="caller" value="<{$caller}>">
       <{if $from_pastreport}>
          <input type="hidden" name="from_pastreport" value="from_pastreport">
       <{/if}>
    </form>
  </div>
</body>
</html>
