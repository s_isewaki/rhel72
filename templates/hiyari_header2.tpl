<div id="header">
  <div class="inner <{$tab_info.total_width_class}>" >
    <h1>
      <a href="hiyari_menu.php?session=<{$session}>">
        <img src="img/logo_fantol_01.gif" alt="FANTOL" />
      </a>
    </h1>
    <{if $mode == "kanri"}>
      <p id="admin"><a href="hiyari_menu.php?session=<{$session}>">ユーザ画面へ</a></p>
    <{else}>  
      <{if $is_kanrisha}>
        <p id="admin"><a href="hiyari_level_info_update.php?session=<{$session}>">管理画面へ</a></p>
      <{/if}>      
    <{/if}>
    <p id="hd_txt" class="radius"><{$inci_auth_name}></p>
    
    <{*メニュー*}>
    <ul id="global_nav" class="clearfix">
      <{foreach name=tab_loop from=$tab_info.tabs item=tab}>
        <{if $tab.is_active}><!--<{* 現在のタブ *}>-->
          <li class="<{$tab.class_name}> active"><a href="<{$tab.url}>"><{$tab.title}></a></li>
        <{elseif $tab.is_usable}><!--<{* 有効なタブ *}>-->
          <li class="<{$tab.class_name}>"><a href="<{$tab.url}>"><{$tab.title}></a></li>
        <{else}><!--<{* 無効なタブ *}>-->
          <li class="<{$tab.class_name}> none"><a href="javascript:void(0);"><{$tab.title}></a></li>
        <{/if}>
      <{/foreach}>
      <{if isset($tab_info.right_end_class)}>
          <li class="<{$tab_info.right_end_class}>"></li>
      <{/if}>
    </ul>
  </div>
</div>
