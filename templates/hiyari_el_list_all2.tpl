<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
	<title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
	<meta http-equiv="imagetoolbar" content="no" />
	<link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/el_list_all.css" />
  <script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
  <script type="text/javascript" src="js/yui/build/treeview/treeview-min.js"></script>
  <link rel="stylesheet" type="text/css" href="js/yui/build/treeview/css/folders/tree.css">
  <script type="text/javascript" src="js/yui/build/event/event-min.js"></script>
  <script type="text/javascript" src="js/yui/build/dom/dom-min.js"></script>
  <script type="text/javascript" src="js/yui/build/dragdrop/dragdrop-min.js"></script>
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="js/libs.js"></script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
  <script type="text/javascript">
    jQuery.noConflict();
    var j$ = jQuery;
    
    jQuery(function($){
      //チェックボックスのチェックがある時のtr
      $('.checkbox').click(function(e) {
        $(this).closest('tr').toggleClass('checked');
      });	
    });

    //起動時の処理
    function initPage(){
        //フォルダーツリー作成
        drawTree();

        <{if $dragdrop_flg}>
          //ドラッグアンドドロップ作成
          constructDragDrop();
        <{/if}>
        
        //フォルダエリアのスクロールバー設定
        j$('#b_scrollbar').tinyscrollbar();
        
        //フォルダ展開時のスクロールバー設定
        j$('#folders table td').live('click', function(){
          j$('#b_scrollbar').tinyscrollbar();
        });
    }

    //フォルダーツリーを生成
    var tree = null;
    function drawTree(){
        <{if $cnt_of_tree==0}>
            var div = document.getElementById('folders');
            div.style.padding = '2px';
            div.innerHTML = 'フォルダは登録されていません。';
            return;
        <{else}>
            tree = new YAHOO.widget.TreeView('folders');
            var root = tree.getRoot();
            <{php}>
              $tree = $this->_tpl_vars['tree'];
              $a = $this->_tpl_vars['a'];
              $c = $this->_tpl_vars['c'];
              $f = $this->_tpl_vars['f'];
              $o = $this->_tpl_vars['o'];
              $doc_counts = $this->_tpl_vars['doc_counts'];
              $session = $this->_tpl_vars['session'];
              $fname = $this->_tpl_vars['fname'];
              $parent_folder_id = $this->_tpl_vars['parent_folder_id'];
              $dragdrop_flg = $this->_tpl_vars['dragdrop_flg'];
              lib_write_tree_js($tree, $a, $c, $f, $o, $doc_counts, $session, $fname, $parent_folder_id, $dragdrop_flg);
              $this->_tpl_vars['parent_folder_id']=$parent_folder_id;
            <{/php}>
            tree.draw();
        <{/if}>
    }

    //ドラッグアンドドロップを生成
    function constructDragDrop(){
        var divs = document.getElementsByTagName('div');
        for (var i = 0, j = divs.length; i < j; i++)
        {
            if (divs[i].className == 'doc')
            {
                var doc = new YAHOO.util.DD(divs[i].id, 'docs');
                doc.setHandleElId('h_'.concat(divs[i].id));

                doc.startPos = YAHOO.util.Dom.getXY(divs[i]);

                doc.startDrag = function (x, y)
                {
                    var el = this.getDragEl();
                    el.style.backgroundColor = '#fefe83';
                    el.style.zIndex = 999;
                }

                doc.onDragDrop = function (e, id)
                {
                    var nodeIndex = id.match(/\d+$/);
                    tree.getNodeByIndex(nodeIndex).getNodeHtml().match(/(\d+),(\d*)<\/span>/);
                    var c = RegExp.$1;
                    var f = RegExp.$2;
                    var lib_id = this.getDragEl().id.match(/\d+$/);
                    var url = 'hiyari_el_move.php?session=<{$session}>&c='.concat(c).concat('&f=').concat(f).concat('&o=<{$o}>&lib_id=').concat(lib_id)<{if $path=="3"}>.concat('&path=3')<{/if}>;
                    location.href = url;
                }

                doc.endDrag = function (x, y)
                {
                    var el = this.getDragEl();
                    YAHOO.util.Dom.setXY(el, this.startPos);
                    el.style.backgroundColor = '';
                    el.style.zIndex = 0;
                }
            }
        }
    }

    function expandAncestor(node){
        if (node.parent){
            node.parent.expand();
            expandAncestor(node.parent);
        }
    }

    function expandAll(){
        if (tree){
            tree.expandAll();
            document.getElementById('expand').style.display = 'none';
            document.getElementById('collapse').style.display = '';
            
            j$('#b_scrollbar').tinyscrollbar();
        }
    }

    function collapseAll(){
        if (tree){
            tree.collapseAll();
            document.getElementById('collapse').style.display = 'none';
            document.getElementById('expand').style.display = '';
            
            j$('#b_scrollbar').tinyscrollbar();
        }
    }

    //コンテンツ、フォルダの削除
    function deleteDocument(){
        var checked_cid = getCheckedIds(document.mainform.elements['cid[]']);
        var checked_did = getCheckedIds(document.mainform.elements['did[]']);
        var checked_fid = getCheckedIds(document.mainform.elements['fid[]']);

        if (checked_cid.length == 0 && checked_did.length == 0 && checked_fid.length == 0)
        {
            alert('削除対象が選択されていません。');
            return;
        }

        if (!confirm('選択されたデータを削除します。よろしいですか？'))
        {
            return;
        }

        document.mainform.action = 'hiyari_el_delete.php';
        addHiddenElement(document.mainform, 'archive', '<{$a}>');
        addHiddenElement(document.mainform, 'category', '<{$c}>');
        addHiddenElement(document.mainform, 'folder_id', '<{$f}>');
        addHiddenElement(document.mainform, 'o', '<{$o}>');
        document.mainform.submit();
    }

    //※deleteDocument()用
    function getCheckedIds(boxes){
        var checked_ids = new Array();
        if (boxes)
        {
            if (!boxes.length)
            {
                if (boxes.checked)
                {
                    checked_ids.push(boxes.value);
                }
            }
            else
            {
                for (var i = 0, j = boxes.length; i < j; i++)
                {
                    if (boxes[i].checked)
                    {
                        checked_ids.push(boxes[i].value);
                    }
                }
            }
        }
        return checked_ids;
    }
    
    //※deleteDocument()用
    function addHiddenElement(frm, name, value){
        var input = document.createElement('input');
        input.type = 'hidden';
        input.name = name;
        input.value = value;
        frm.appendChild(input);
    }

    //「上の階層へ」
    function moveToParent(){
        <{if $f == ""}>
        //書庫へ移動
            location.href = '<{$fname}>?session=<{$session}>&a=<{$a}>&o=<{$o}>';
        <{elseif $parent_folder_id == ""}>
        //カテゴリへ移動
            location.href = '<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&o=<{$o}>';
        <{else}>
        //親フォルダへ移動
            location.href = '<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&f=<{$parent_folder_id}>&o=<{$o}>';
        <{/if}>
    }

    //コンテンツ登録画面を表示します。
    function registDocument(){
        var url = "hiyari_el_register.php?session=<{$session}>&archive=<{$a}>&category=<{$c}>&folder_id=<{$f}>&o=<{$o}>";
        show_sub_window(url);
    }

    //フォルダ作成画面を表示します。
    function createFolder(){
        var url = "hiyari_el_folder_register.php?session=<{$session}>&archive=<{$a}>&cate_id=<{$c}>&parent=<{$f}>";
        show_sub_window(url);
    }

    //フォルダ更新画面を表示します。
    function updateFolder(){
        var url = "hiyari_el_folder_update.php?session=<{$session}>&archive=<{$a}>&cate_id=<{$c}>&folder_id=<{$f}>";
        show_sub_window(url);
    }

    //検索画面を表示します。
    function show_search_page(){
        var url = "hiyari_el_search.php?session=<{$session}>";
        show_sub_window(url);
    }

    //参照履歴画面を表示します。
    function show_refer_log_page(){
        var url = "hiyari_el_refer_log.php?session=<{$session}>";
        show_sub_window(url);
    }

    //子画面を表示します。
    function show_sub_window(url){
        var h = window.screen.availHeight - 30;
        var w = window.screen.availWidth - 10;
        var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
        window.open(url, '_blank',option);
    }

    //リロードします。※注意：この関数は別画面から呼び出されます。
    function reload_page(){
        location.href = "<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&f=<{$f}>&o=<{$o}>";
    }
  </script>
</head>

<body id="top" onload="initPage();">
  <{*ヘッダー*}>
  <{include file="hiyari_header2.tpl"}>

  <div id="content" class="clearfix <{$tab_info.total_width_class}>">
    <form name="mainform" method="post">
      <input type="hidden" name="session" value="<{$session}>">
      
      <{* フォルダ一覧 *}>
      <div id="sub">
        <div id="sub_bunrui" class="color01 radius">
          <h2>フォルダ</h2>
          <div id="sub_nav" class="radius_mini">
            <span id="expand">
              <input type="button" class="nav_btn" onclick="expandAll();" value="全て開く">
            </span>
            <span id="collapse" style="display:none;">
              <input type="button" class="nav_btn" onclick="collapseAll();" value="全て閉じる">
            </span>
          </div>
          <div id="b_scrollbar">
            <div class="scrollbar"><div class="track radius"><div class="thumb radius"><div class="end"></div></div></div></div>
            <div class="viewport inner radius_mini">
              <div class="overview" id="folders"></div>
            </div>
          </div>
        </div>
      </div>

      <div id="main" class="<{$tab_info.main_width_class}>">
        <{* 上部ボタン *}>
        <ul id="main_nav" class="clearfix">
          <li><input type="button" class="button radius_mini" value="コンテンツ検索" onclick="show_search_page()" /></li>
          <li><input type="button" class="button radius_mini" value="参照履歴" onclick="show_refer_log_page()" /></li>
          <li><input type="button" class="<{$parent_button_class}> radius_mini" value="上の階層へ" <{$parent_button_disabled}> onclick="moveToParent();" /></li>
          <li><input type="button" class="<{$folder_button_class}> radius_mini" value="フォルダ作成" <{$folder_button_disabled}> onclick="createFolder();" /></li>
          <li><input type="button" class="<{$document_button_class}> radius_mini" value="コンテンツ登録" <{$document_button_disabled}> onclick="registDocument();" /></li>
          <li><input type="button" class="<{$folder_edit_button_class}> radius_mini" value="フォルダ名更新" <{$folder_edit_button_disabled}> onclick="updateFolder();" /></li>
          <li><input type="button" class="<{$delete_button_class}> radius_mini" value="削除" <{$delete_button_disabled}> onclick="deleteDocument();" /></li>
        </ul>
        
        <{* 一覧 *}>
        <div id="list_area" class="color02 radius">
          <table class="list">
            <tr>
              <th width="34">削除</th>
              <th>
                <{if $o=="1" }> <{*コンテンツ名：昇順*}>
                    <a href="<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&f=<{$f}>&o=2"><img src="img/up.gif" alt="" width="17" height="17" border="0" style="vertical-align:middle;">コンテンツ名</a>
                <{else}> <{*コンテンツ名：降順*}>
                    <a href="<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&f=<{$f}>&o=1"><img src="img/down.gif" alt="" width="17" height="17" border="0" style="vertical-align:middle;">コンテンツ名</a>
                <{/if}>
              </th>
              <th>感想</th>
              <th width="80">
                <{if $o=="3" }> <{*更新日時：昇順*}>
                  <a href="<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&f=<{$f}>&o=4"><img src="img/up.gif" alt="" width="17" height="17" border="0" style="vertical-align:middle;">更新日</a>
                <{else}><{*更新日時：降順*}>
                  <a href="<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&f=<{$f}>&o=3"><img src="img/down.gif" alt="" width="17" height="17" border="0" style="vertical-align:middle;">更新日</a>
                <{/if}>
              </th>
              <th width="70">
                <{if $o=="5"}><{*サイズ：昇順*}>
                  <a href="<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&f=<{$f}>&o=6"><img src="img/up.gif" alt="" width="17" height="17" border="0" style="vertical-align:middle;">サイズ</a>
                <{else}><{*サイズ：降順*}>
                  <a href="<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&f=<{$f}>&o=5"><img src="img/down.gif" alt="" width="17" height="17" border="0" style="vertical-align:middle;">サイズ</a>
                <{/if}>
              </th>
              <th class="none">権限</th>
            </tr>
            <{foreach from=$contents item=i}>
              <tr>
                <td>
                  <span class="checkbox_wrap<{if !$i.can_delete}> disabled<{/if}>">
                    <{$i.cbox}>
                  </span>
                </td>
                <td class="left">
                  <div id="doc<{$i.did}>" class="<{$i.div_class}>">
                    <{$i.icon}>
                    <{$i.anchor}>
                  </div>
                </td>
                <td class="left"><{$i.kansou}></td>
                <td><{$i.modified_str}></td>
                <td><{$i.size}></td>
                <td class="none">
                  <a href="javascript:show_sub_window('<{$i.auth_update_url}>')"><{$i.auth}></a>
                </td>
              </tr>
            <{/foreach}>
          </table>
        </div>
      </div>
    </form>    
  </div>
  
	<div id="footer">
		<p>Copyright Comedix all right reserved.</p>
	</div>
</body>
</html>
