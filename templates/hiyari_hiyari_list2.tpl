<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | ヒヤリハット提出</title>
	<meta http-equiv="imagetoolbar" content="no" />
	<link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/smoothness/jquery-ui-1.10.3.custom.min.css">
	<link type="text/css" rel="stylesheet" media="all" href="css/hiyari_hiyari.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/pagination.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
  <script type="text/javascript" src="js/hiyari_hiyari.js"></script>
  <script type="text/javascript" src="js/jquery/jquery-ui-1.10.2.custom.min.js"></script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
  <script type="text/javascript">
    jQuery(function($){
        //分類フォルダ
        $('#b_scrollbar').tinyscrollbar();
        $('#b_scrollbar input').click(function() {
            $(this).closest('li').toggleClass('checked');
            $(this).closest('.checkbox_wrap').toggleClass('checked');
        });

        //一覧のチェックボックス
        $('.checkbox').click(function() {
            //チェックボックスのチェックがある時のtr
            $(this).closest('tr').toggleClass('checked');
            //チェックボックスの見た目
            $(this).closest('.checkbox_wrap').toggleClass('checked');
        });
        
        //匿名ポップアップ
        $('.emp_name').tooltip({
            position: {
                my: "left+20 center",
                at: "right center",
                using: function( position, feedback ) {
                    $( this ).css( position );
                    $( "<div>" )
                    .addClass( "arrow" )
                    .addClass( feedback.vertical )
                    .addClass( feedback.horizontal )
                    .appendTo( this );
                }
            }
        });        
    });
  </script>
</head>

<body id="top">
  <{*ヘッダー*}>
  <{include file="hiyari_header2.tpl"}>

	<div id="content" class="clearfix <{$tab_info.total_width_class}>">
    <form name="list_form" method="post" action="hiyari_hiyari_list.php">
      <input type="hidden" name="is_postback" value="true">
      <input type="hidden" name="session" value="<{$session}>">
      <input type="hidden" name="mode" value="">
      <input type="hidden" name="change_output_target_in_out" value="">
      <input type="hidden" name="list_tab_mode" value="<{$list_tab_mode}>">
      <input type="hidden" name="page" value="<{$page}>">
      <input type="hidden" name="folder_id" value="<{$folder_id}>">
      <input type="hidden" name="sort_item" value="<{$sort_item}>">
      <input type="hidden" name="sort_div" value="<{$sort_div}>">
      
      <div id="sub" class="clearfix">
        <{* リンク *}>
        <div id="sub_menu" class="color01 radius">
          <h2><img src="img/side_nav_txt01.gif" alt="MENU" /></h2>
          <ul>
            <li><a href="hiyari_hiyari_info_2010.php?session=<{$session}>">発生件数情報</a></li>
            <li class="active"><a href="hiyari_hiyari_list.php?session=<{$session}>">データ提出(CSV)</a></li>
            <li><a href="hiyari_hiyari_xml_list.php?session=<{$session}>">データ提出(XML)</a></li>
          </ul>
        </div>
    
        <{* 分類 *}>
        <div id="sub_bunrui" class="color01 radius">
          <h2>ヒヤリ・ハット分類フォルダ</h2>
          
          <{* 分類年 *}>
          <div id="sel_years" class="inner radius_mini">
            <select name="hiyari_year" onchange="hiyari_year_changed(this.value);">
              <{foreach from=$cat_years item=i}>
                <option value="<{$i}>" <{if $i==$hiyari_year}>selected="selected"<{/if}>><{$i}></option>
              <{/foreach}>
            </select>年
          </div>
          
          <{* フォルダ一覧 *}>
          <div id="b_scrollbar">
            <div class="scrollbar"><div class="track radius"><div class="thumb radius"><div class="end"></div></div></div></div>
            <div class="viewport inner radius_mini">
              <div class="overview">
                <ul class="radius_mini">
                  <{foreach from=$folders item=i}>
                    <li <{if $i.folder_id == $folder_id}>class="active"<{/if}>>
                      <span class="checkbox_wrap">
                        <input type="checkbox" value="<{$i.folder_id}>" id="folder_select[]" name="folder_select[]">
                      </span>
                      <a href="javascript:selected_hiyari_folder_changed('<{$i.folder_id}>');" <{if $i.folder_id == $folder_id}>class="active"<{/if}>>
                        <{$i.folder_name}>
                      <{if $i.folder_id == $folder_id}>
                        (<{$i.report_count}>)
                      <{/if}>
                      </a>
                    </li>
                  <{/foreach}>
                </ul>
              </div>
            </div>
          </div>               
          <{* 分類オペレーション *}>          
          <div id="folder_nav">
            <ul>
              <li><input type="button" class="nav_btn radius_mini" value="作成" onclick="create_hiyari_folder('<{$session}>', '<{$hiyari_year}>', false);"></li>
              <li><input type="button" class="nav_btn radius_mini" value="変更" onclick="update_hiyari_folder('<{$session}>', '<{$hiyari_year}>', false);"></li>
              <li><input type="button" class="nav_btn radius_mini" value="削除" onclick="delete_hiyari_folder();"></li>
            </ul>
          </div>
        </div>
      </div>
          
      <div id="main" class="<{$tab_info.main_width_class}>">
        <{if $folder_id == ""}>
          <{if $all_folder_info_count == 0}>
            フォルダを作成してください。
          <{else}>
            フォルダを選択してください。
          <{/if}>
        <{else}>
          <ul id="action_btns" class="cleafix">
            <{if $list_tab_mode == "target_list" || $list_tab_mode == "target_list_ng"}>
              <li><input type="button" class="button radius_mini" value="形式チェック" onclick="check_ng_report()"></li>
              <li><input type="button" class="button radius_mini" value="提出除外" onclick="change_output_target('out')"></li>
            <{/if}>
            <{if $list_tab_mode == "target_out_list"}>
              <li><input type="button" class="button radius_mini" value="提出対象" onclick="change_output_target('in')"></li>
            <{/if}>
          </ul>
          
          <{* 一覧 *}>
          <div id="report_list" class="section clear">
            <div class="color02 radius">     
              <{* 実行アイコン *}>
              <ul id="main_nav" class="clearfix">
                <li class="main_nav_08"><a href="#" onclick="output_hiyari_csv();">CSV出力</a></li>
              </ul>
              
              <ul id="target_nav" class="clearfix">            
                <li <{if $list_tab_mode == "target_list"}>class="active"<{/if}>>
                  <a class="tab radius_mini" href="javascript:change_list_tab('target_list')">提出対象報告</a>
                </li>              
                <{if $list_tab_mode == "target_list_ng"}>              
                  <li <{if $list_tab_mode == "target_list_ng"}>class="active"<{/if}>>
                    <a class="tab radius_mini" href="javascript:change_list_tab('target_list_ng')">提出対象報告(NG)</a>
                  </li>
                <{/if}>                
                <li <{if $list_tab_mode == "target_out_list"}>class="active"<{/if}>>
                  <a class="tab radius_mini" href="javascript:change_list_tab('target_out_list')">提出対象外報告</a>
                </li>
              </ul>
              <table class="list">
                <tr>
                  <th width="20">&nbsp;</th>
                  <th width="70"><a href="javascript:set_report_list_sort('REPORT_NO');">事案番号</a></th>
                  <th class="title"><a href="javascript:set_report_list_sort('REPORT_TITLE');">表題</a></th>
                  <th class="class"><a href="javascript:set_report_list_sort('CLASS');">報告部署</a></th>
                  <th class="emp_name"><a href="javascript:set_report_list_sort('REGIST_NAME');">報告者</a></th>
                  <th width="70" class="none"><a href="javascript:set_report_list_sort('INCIDENT_DATE');">発生日</a></th>
                </tr>
                <{foreach from=$report_list item=row}>
                  <tr> 
                    <td>
                      <span class="checkbox_wrap">
                        <input type="checkbox" name="list_select[]" value="<{$row.id}>" class="checkbox">
                      </span>
                      <input type="hidden" id="report_id_of_list_id_<{$row.id}>" value="<{$row.id}>">
                    </td>
                    <td>
                      <nobr>
                      <a href="javascript:show_report_ng_info('<{$row.id}>', '<{$session}>', '<{$hiyari_data_type}>', false);">
                        <{$row.report_no}>
                      </a>
                      </nobr>
                    </td>  
                    <td>
                      <a href="javascript:open_report_update_from_report_id('<{$row.id}>', '<{$session}>','<{$row.mail_id}>');" class="left ellipsis title">
                        <{$row.title}>
                      </a>
                    </td>    
                    <td>
                      <span class="left ellipsis class">
                        <{$row.class_nm}>
                      </span>
                    </td>	
                    <td>
                      <span class="left ellipsis emp_name">
                        <{if $row.is_anonymous_report && $is_mouseover_popup}>
                          <a href="#" title="<{$row.real_name|escape}>">匿名</a>
                        <{else}>
                          <{$row.disp_emp_name}>
                        <{/if}>
                      </span>
                    </td>	
                    <td><{$row.incident_date}></td>
                  </tr>
                <{/foreach}>
              </table>
              <{include file="hiyari_paging2.tpl"}>
            </div>
          </div>
        <{/if}>
      </div>
      <!-- 画面右 END -->
    </form>
    <!-- 分類・一覧・ページ選択 END -->

    <!-- CSV出力用フォーム START -->
    <form name="csv_output_form" action="hiyari_hiyari_csv_output.php" method="post" target="download">
      <input type="hidden" name="session" value="<{$session}>">
      <input type="hidden" name="folder_id" value="<{$folder_id}>">
    </form>
    <iframe name="download" width="0" height="0" frameborder="0"></iframe>
    <!-- CSV出力用フォーム END -->
  </div>
  <!-- 本体 END -->
  
	<div id="footer">
		<p>Copyright Comedix all right reserved.</p>
	</div>
</body>
</html>
