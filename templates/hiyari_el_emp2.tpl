<!-- 参照部署役職の指定 START -->
<h2 class="type02">
  <span style="cursor:pointer;" onclick="toggle2(document.getElementById('ref_toggle'), 'ref_toggle_area');">参照可能範囲の指定&nbsp;</span>
  <span id="ref_toggle" style="cursor:pointer;" onclick="toggle2(this, 'ref_toggle_area');"><{$ref_toggle_mode}></span>
</h2>
<div id="ref_toggle_area" style="display:<{$ref_toggle_display}>;" class="color02 radius clearfix mB20">
  <table class="v_title">            
    <tr>
      <th width="100" rowspan="2">
        部署・役職<br>
        <input type="checkbox" class="mR5" name="ref_dept_st_flg" value="t" <{if $ref_dept_st_flg == "t"}>checked<{/if}> onclick="setDisabled();">許可しない
      </th>
      <th class="none">
        部署<br>
        <input type="radio" name="ref_dept_flg" value="1" <{if $ref_dept_flg == "1"}>checked<{/if}> onclick="setDisabled();">すべて
        <input type="radio" name="ref_dept_flg" value="2" <{if $ref_dept_flg == "2"}>checked<{/if}> onclick="setDisabled();">指定する
        <input type="button" class="ref_btn button radius_mini mL20" name="ref_dept_all" value="全画面" onclick="showDeptAll('ref', '<{$session}>');">
      </th>
      <th class="none">
        役職<br>
        <input type="radio" name="ref_st_flg" value="1" <{if $ref_st_flg == "1"}>checked<{/if}> onclick="setDisabled();">すべて
        <input type="radio" name="ref_st_flg" value="2" <{if $ref_st_flg == "2"}>checked<{/if}> onclick="setDisabled();">指定する
      </th>
    </tr>
    <tr>
      <td>
        <table class="busho">
          <td valign="bottom" class="busho1 none">
            <label>可能とする<{$arr_class_name[2]}></label>
            <select name="ref_dept" size="6" multiple>
              <{foreach from=$ref_dept item=dept_id}>
                <option value="<{$dept_id}>"><{$dept_names[$dept_id]}></option>
              <{/foreach}>
            </select><br>
            <input type="button" class="ref_btn button radius_mini mT5" name="delete_all_ref_dept" value="全て消去" onclick="deleteAllOptions(this.form.ref_dept);">
          </td>
          <td class="none arrow_area">
            <input type="button" class="ref_arrow arrow radius_mini" name="add_ref_dept" value=" &lt; " onclick="addSelectedOptions(this.form.ref_dept, this.form.ref_dept_src);"><br><br>
            <input type="button" class="ref_arrow arrow radius_mini" name="delete_ref_dept" value=" &gt; " onclick="deleteSelectedOptions(this.form.ref_dept);">
          </td>
          <td class="busho2 none">
            <select name="ref_class_src" class="mR5" onchange="setRefAtrbSrcOptions();"></select><{$arr_class_name[0]}><br>
            <select name="ref_atrb_src" class="mR5" onchange="setRefDeptSrcOptions();"></select><{$arr_class_name[1]}><br>
            <select name="ref_dept_src" size="6" multiple></select><br>
            <input type="button" class="ref_btn button radius_mini mT5" name="select_all_ref_dept" value="全て選択" onclick="selectAllOptions(this.form.ref_dept_src);">
          </td>
        </table>
      </td>
      <td>
        <select name="ref_st[]" size="10" multiple>
          <{foreach from=$sel_st item=i}>
              <option value="<{$i.st_id}>" <{if in_array($i.st_id, $ref_st)}>selected<{/if}>>
                <{$i.st_nm}>
              </option>
          <{/foreach}>
        </select>
      </td>
    </tr>
    <!-- 参照部署役職の指定 END -->

    <!-- 参照職員の指定 START -->
    <tr>
      <th class="none">
        職員
      </th>
      <td class="none">
        <span id="target_disp_area1"></span>
      </td>
      <td class="none">
        <input type="button" class="button radius_mini mB5" name="emplist1" value="職員名簿" onclick="openEmployeeList('1', '<{$session}>');"><br>
        <input type="button" class="button radius_mini" name="emplist1_clear" value="クリア" onclick="clear_target('1','<{$emp_id}>','<{$emp_name}>');"><br>
      </td>
    </tr>
  </table>
</div>
<!-- 参照職員の指定 END -->

<!-- 更新部署役職の指定 START -->
<h2 class="type02">
  <span style="cursor:pointer;" onclick="toggle2(document.getElementById('upd_toggle'), 'upd_toggle_area');">更新可能範囲の指定&nbsp;</span>
  <span id="upd_toggle" style="cursor:pointer;" onclick="toggle2(this, 'upd_toggle_area');"><{$upd_toggle_mode}></span>
</h2>
<div id="upd_toggle_area" style="display:<{$upd_toggle_display}>;" class="color02 radius clearfix">
  <table class="v_title">            
    <tr>
      <th width="100" rowspan="2">
        部署・役職<br>
        <input type="checkbox" class="mR5" name="upd_dept_st_flg" value="t" <{if $upd_dept_st_flg == "t"}>checked<{/if}> onclick="setDisabled();">許可しない
      </th>
      <th class="none">
        部署<br>
        <input type="radio" name="upd_dept_flg" value="1" <{if $upd_dept_flg == "1"}>checked<{/if}> onclick="setDisabled();">すべて
        <input type="radio" name="upd_dept_flg" value="2" <{if $upd_dept_flg == "2"}>checked<{/if}> onclick="setDisabled();">指定する
        <input type="button" class="upd_btn button radius_mini mL20" name="upd_dept_all" value="全画面" onclick="showDeptAll('upd', '<{$session}>');">
      </th>
      <th class="none">
        役職<br>
        <input type="radio" name="upd_st_flg" value="1" <{if $upd_st_flg == "1"}>checked<{/if}> onclick="setDisabled();">すべて
        <input type="radio" name="upd_st_flg" value="2" <{if $upd_st_flg == "2"}>checked<{/if}> onclick="setDisabled();">指定する
      </th>
    </tr>
    <tr>
      <td>
        <table class="busho">
          <tr>
            <td valign="bottom" class="busho1 none">
              <label>可能とする<{$arr_class_name[2]}></label>
              <select name="upd_dept" size="6" multiple>
                <{foreach from=$upd_dept item=dept_id}>
                  <option value="<{$dept_id}>"><{$dept_names[$dept_id]}></option>
                <{/foreach}>
              </select><br/>
              <input type="button" class="upd_btn button radius_mini mT5" name="delete_all_upd_dept" value="全て消去" onclick="deleteAllOptions(this.form.upd_dept);">
            </td>
            <td class="none arrow_area">
              <input type="button" class="upd_arrow arrow radius_mini" name="add_upd_dept" value=" &lt; " onclick="addSelectedOptions(this.form.upd_dept, this.form.upd_dept_src);"><br><br>
              <input type="button" class="upd_arrow arrow radius_mini" name="delete_upd_dept" value=" &gt; " onclick="deleteSelectedOptions(this.form.upd_dept);">
            </td>
            <td class="busho2 none">
              <select name="upd_class_src" class="mR5" onchange="setUpdAtrbSrcOptions();"></select><{$arr_class_name[0]}><br>
              <select name="upd_atrb_src" class="mR5" onchange="setUpdDeptSrcOptions();"></select><{$arr_class_name[1]}><br>
              <select name="upd_dept_src" size="6" multiple></select><br>
              <input type="button" class="upd_btn button radius_mini mT5" name="select_all_upd_dept" value="全て選択" onclick="selectAllOptions(this.form.upd_dept_src);">
            </td>
          </tr>
        </table>
      </td>
      <td>
        <select name="upd_st[]" size="10" multiple>
          <{foreach from=$sel_st item=i}>
            <option value="<{$i.st_id}>" <{if in_array($i.st_id, $upd_st)}>selected<{/if}>>
              <{$i.st_nm}>
            </option>
          <{/foreach}>
        </select>
      </td>
    </tr>
    <!-- 更新部署役職の指定 END -->

    <!-- 更新職員の指定 START -->
    <tr>
      <th class="none">
        職員
      </th>
      <td class="none">
        <span id="target_disp_area2"></span>
      </td>
      <td class="none">
        <input type="button" class="button radius_mini mB5" name="emplist2" value="職員名簿" onclick="openEmployeeList('2', '<{$session}>');"><br>
        <input type="button" class="button radius_mini" name="emplist2_clear" value="クリア" onclick="clear_target('2','<{$emp_id}>','<{$emp_name}>');"><br>
      </td>
    </tr>
  </table>
</div>
<!-- 更新職員の指定 END -->
