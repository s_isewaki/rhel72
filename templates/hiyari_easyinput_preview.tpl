<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <{$INCIDENT_TITLE}> | 報告書プレビュー</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
    jQuery.noConflict();
    var j$ = jQuery;
</script>
<link rel="stylesheet" type="text/css" href="css/incident.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
.desc td {border-style:none;}
</style>
<{if $style_code==2}>
  <link rel="stylesheet" type="text/css" href="css/easyinput.css">
<{/if}>
</HEAD>
<SCRIPT type="text/javascript">
//====================================================================================================
//起動時の処理
//====================================================================================================
<{if $style_code==2}>
  j$(document).ready(function(){
    <{if !$readonly_flg && $eis_select_able}>
      set_eis_input_list();
      set_eis_no(<{$sel_eis_no}>);
    <{/if}>

    <{if $auto_reload}>
      start_auto_session_update();
    <{/if}>  
  });
<{else}>
  function load_item() {
    cng_scene(document.FRM_MAIN._700_10.value);
    cng_kind(document.FRM_MAIN._700_10.value);
    cng_content(document.FRM_MAIN._700_10.value);
    cng_scene_item(document.FRM_MAIN._710_10.value);
    cng_kind_item(document.FRM_MAIN._740_10.value);
    cng_content_item(document.FRM_MAIN._770_10.value);

    disp_kind();
  }

  function disp_kind() {
    var id   = document.FRM_MAIN._700_10.value;
    var disp = document.getElementById("kind_disp");

    if(id == 2 || id == 3) {
        disp.style.display = '';
    } else {
        disp.style.display = 'none';
    }
  }

  function load_item_2010() {
    cng_kind_2010();
    cng_scene_2010();
    cng_content_2010();
    cng_kind_item_2010();
    cng_scene_item_2010();
    cng_content_item_2010();

    disp_kind_2010();
  }

  function disp_kind_2010() {
    var disp = document.getElementById("kind_disp_2010");

    if( j$("input:radio[name='_910_10[]']").length > 1 ) {
      disp.style.display = '';
    } else {
      disp.style.display = 'none';
    }
  }

  function undisp(id) {
    ele1 = document.getElementById("add_item_1");
    ele2 = document.getElementById("add_item_2");
    ele3 = document.getElementById("add_item_3");

    if(id != 3) ele1.style.display = 'none';
    if(id != 6) ele2.style.display = 'none';
    if(id != 8) ele3.style.display = 'none';
  }
<{/if}>
</script>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onLoad="<{if in_array(700, $grp_flags)}>load_item();<{/if}><{if in_array(800, $grp_flags)}>undisp(document.FRM_MAIN._700_10.value);<{/if}><{if in_array(900, $grp_flags)}>load_item_2010();disp_item_2010();<{/if}><{if in_array(110, $grp_flags)}>area_110_65_disp_change('<{$vals[110][60][0]}>', '<{$vals[110][65][0]}>');<{/if}>">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="tbl_layout">
<!-- ヘッダー START -->
<{php}>
    require_once("./hiyari_common.ini");
    show_hiyari_header_for_sub_window("報告書様式プレビュー");
<{/php}>
<!-- ヘッダー END -->
<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<form name='FRM_MAIN' action='hiyari_easyinput_preview.php' method='post'>
<input type='hidden' name='session' value='<{$session}>'>
<input type='hidden' name='hyr_sid' value='<{$hyr_sid}>'>

<{if $style_code==2}>
  <{include file="hiyari_edit_easyinput2.tpl"}>
<{else}>
  <{include file="hiyari_edit_easyinput.tpl"}>
<{/if}>

<div>
<input type="button" name="close" value="閉じる" alt="閉じる" onClick='window.close();'>
<!--
<input type="button" name="cancel" value="再入力" alt="再入力する" onClick='history.go(-1);'>
<input type="submit" name="register" value="登録" alt="登録する">
-->
</div>
<br>

</form>

</td>
</tr>
</table>
</body>
</html>
