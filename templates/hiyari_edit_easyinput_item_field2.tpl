<{*
インターフェース
	$grps
		array   項目情報配列
	$vals
		array   値の配列
	$grp_flags
		array   レポートに使用する項目グループ
	$i
		string  グループコード
	$j
		string  アイテムコード
	$style
		string  "textarea"に対する書式指定。"big"、"genmensoti"、"kaizensaku"
	$checkbox_line_count
		integer "checkbox"に対する一行辺りの表示項目数。省略時は4。
	$radio_line_count
		integer "radio"に対する一行辺りの表示項目数。省略時は4。
	$comp
		string  "select"の選択値を判定する際に項目名を判定するか項目コードを判定するかを決定する。"name"の場合は項目名判定。
		        $compが設定されるケースは現状ない。(廃止可能)
	$text_field_width
		string  "text"のwidth値。省略時は250。
	$level_infos
		array   インシレベルとメッセージの配列
	$item_element_no_disp
		array   項目表示情報配列
	$add_tag_element
		string  "text"のみ対応。追加するタグ属性。

*}>
<{if $text_field_width == ""}>
	<{assign var="text_field_width" value="250"}>
<{/if}>
<{php}>print_r($ic)<{/php}>

	<{if count($ic) == 1 && ($i==1400 || ($i==1410 && $j==20))}>

		<input type='checkbox' id="id_<{$i}>_<{$j}>_<{$key}>_0" name="_<{$i}>_<{$j}>[]" value="<{$ic[0].item_id}>" onclick="checkbox_onclick(this)"
		<{if in_array($ic[0].item_id, (array)@$vals[$i][$j])}> checked<{/if}>>
		<label for="id_<{$i}>_<{$j}>_<{$key}>_0"><font class="j12"><{$ic[0].item_name|escape:"html"}></font></label>

	<{elseif ($i==1400 || ($i==1410 && $j==20))}>

		<{if $checkbox_line_count == ""}>
			<{assign var="checkbox_line_count" value="4"}>
		<{/if}>
		<table width="100%" cellspacing="0" cellpadding="2" class="list">
		<tr height="22">
		<{php}>
			$checkbox_col_index = 0;
		
                <{/php}>
		<{section name=k loop=$ic}>
			<{if $item_element_no_disp[$i][$j] == "" || !in_array($ic[$smarty.section.k.index].item_id,$item_element_no_disp[$i][$j])}>
				<{php}>
					$checkbox_col_index++;
					if($checkbox_col_index == $this->_tpl_vars['checkbox_line_count']+1)
					{
						$checkbox_col_index=1;
				
                                <{/php}>
						</tr><tr>
				<{php}>
					}
				<{/php}>
	<td style="width:25%">
		
            <input type='checkbox' id="id_<{$i}>_<{$j}>_<{$key}>_<{$smarty.section.k.index}>" name="_<{$i}>_<{$j}>[]" value="<{$ic[$smarty.section.k.index].item_id}>"
onclick="checkbox_onclick(this)"
				<{if in_array($ic[$smarty.section.k.index].item_id, (array)@$vals[$i][$j])}> checked<{/if}>>
				<label for="id_<{$i}>_<{$j}>_<{$key}>_<{$smarty.section.k.index}>"><font class="j12"><{$ic[$smarty.section.k.index].item_name|escape:"html"}></font></label>
	</td>
			<{/if}>
		<{/section}>
		</tr>
		</table>

	<{elseif count($ic) == 1 && ($i==1410 && $j==30)}>

		<input type='checkbox' id="id_<{$i}>_<{$j}>_<{$key}>_0" name="_<{$i}>_<{$j}>[]" value="<{$ic[0].sub_item_id}>" onclick="checkbox_onclick(this)"
		<{if in_array($ic[0].sub_item_id, (array)@$vals[$i][$j])}> checked<{/if}>>
		<label for="id_<{$i}>_<{$j}>_<{$key}>_0"><font class="j12"><{$ic[0].sub_item_name|escape:"html"}></font></label>

	<{elseif ($i==1410 && $j==30)}>

		<{if $checkbox_line_count == ""}>
			<{assign var="checkbox_line_count" value="4"}>
		<{/if}>
		<table width="100%" cellspacing="0" cellpadding="2" class="list">
		<tr height="22">
		<{php}>
			$checkbox_col_index = 0;
		
                <{/php}>
		<{section name=k loop=$ic}>
			<{if $item_element_no_disp[$i][$j] == "" || !in_array($ic[$smarty.section.k.index].item_id,$item_element_no_disp[$i][$j])}>
				<{php}>
					$checkbox_col_index++;
					if($checkbox_col_index == $this->_tpl_vars['checkbox_line_count']+1)
					{
						$checkbox_col_index=1;
				
                                <{/php}>
						</tr><tr>
				<{php}>
					}
				<{/php}>
	<td style="width:25%">
		<input type='checkbox' id="id_<{$i}>_<{$j}>_<{$key}>_<{$smarty.section.k.index}>" name="_<{$i}>_<{$j}>[]" value="<{$ic[$smarty.section.k.index].sub_item_id}>" onclick="checkbox_onclick(this)"
				<{if in_array($ic[$smarty.section.k.index].sub_item_id, (array)@$vals[$i][$j])}> checked<{/if}>>
				<label for="id_<{$i}>_<{$j}>_<{$key}>_<{$smarty.section.k.index}>"><font class="j12"><{$ic[$smarty.section.k.index].sub_item_name|escape:"html"}></font></label>
	</td>
			<{/if}>
		<{/section}>
		</tr>
		</table>

	<{/if}>