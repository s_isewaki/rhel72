<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | 代行者設定</title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/agent_register.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/smoothness/jquery-ui.custom.css">
	<script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/hiyari_agent_register.js"></script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
	<script type="text/javascript" src="js/jquery/jquery-ui-1.10.2.custom.min.js"></script>
  <script type="text/javascript" src="js/jquery/jquery.ui.datepicker-ja.min.js" charset="utf-8"></script>
  <script>
    $(function() {
      $('.calendar_text').datepicker({showOn: 'focus', showOtherMonths: true, selectOtherMonths: true});
      $('.calendar_btn').click(function(){
        $(this).prev('input').datepicker( 'show' );
      });
    });
  </script>
</head>

<body id="top" onload="initPage('<{$agent_ids}>', '<{$agent_names}>');">
  <{* ヘッダー *}>
  <{include file="hiyari_header2.tpl"}>

  <{* メイン *}>
  <div id="content" class="<{$tab_info.total_width_class}>">
    <div class="color02 radius">
      <form name="mainform" action="hiyari_agent_register.php" method="post">
        <table class="v_title">
          <tr>
            <th>代行期間</th>
            <td id="term">						
              <input type="text" id="start_date" class="calendar_text" name="start_date" value="<{$start_date}>" readonly />
              <a class="calendar_btn" href="#">カレンダー</a>
              <a class="mT3 clear_btn cb_type02" href="#" onclick="date_clear('start_date');">クリア</a>
              <span>〜</span>				
              <input type="text" id="end_date" class="calendar_text" name="end_date" value="<{$end_date}>" readonly>
              <a class="calendar_btn" href="#">カレンダー</a>
              <a class="mT3 clear_btn cb_type02" href="#" onclick="date_clear('end_date');">クリア</a>
              <p>＊代行期間を設けたくない場合、入力は不要です。</p>
            </td>
          </tr>
          <tr>
            <th class="none">職員</td>
            <td id="emp" class="none">
              <div id="disp_area_1">&nbsp;</div>
              <div id="emp_btn">
                <input type="button" class="button radius_mini" value="職員名簿" onclick="call_emp_search('1', '<{$session}>');">
                <br>
                <a class="mT3 clear_btn cb_type02" onclick="emp_clear('1');" href="#">クリア</a>
              </div>
            </td>
          </tr>
        </table>
        <input type="hidden" name="disp_area_ids_1" value="" id="disp_area_ids_1">
        <input type="hidden" name="disp_area_names_1" value="" id="disp_area_names_1">
        <input type="hidden" name="emp_id" value="<{$emp_id}>" id="emp_id">
        <input type="hidden" name="emp_nm" value="<{$emp_nm}>" id="emp_nm">
      </form>
    </div>
    <div id="main_btn">
      <input type="button" class="button radius_mini" value="登録" onclick="regist('<{$session}>');">
    </div>    
  </div>
  
  <{*フッター*}>
  <{include file="hiyari_footer2.tpl"}>
</body>
</html>
