<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <{$INCIDENT_TITLE}> | <{$title}></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<{php}>
  // YUIファイルを読み込む
  read_yui_calendar_use_file_0_12_2();
<{/php}>
<script type="text/javascript" src="js/yui_0.12.2/build/dragdrop/dragdrop-min.js"></script>
<{php}>
  // カレンダー作成、関数出力
  $arr_id = array('start_date', 'end_date');
  read_yui_calendar_script($arr_id);
<{/php}>

<script language="javascript">
function initPage() {
    constructDragDrop();
    setAtrbOptions('<{$search_emp_attribute}>', '<{$search_emp_dept}>', '<{$search_emp_room}>');
}

function constructDragDrop() {
    var divs = document.getElementsByTagName('div');
    for (var i = 0, j = divs.length; i < j; i++) {
        if (divs[i].className == 'doc') {
            var doc = new YAHOO.util.DD(divs[i].id, 'docs');
            doc.setHandleElId('h_'.concat(divs[i].id));

            doc.startPos = YAHOO.util.Dom.getXY(divs[i]);

            doc.startDrag = function (x, y) {
                var el = this.getDragEl();
                el.style.backgroundColor = '#fefe83';
                el.style.zIndex = 999;
            }

            doc.onDragDrop = function (event,targetId) {
                id = this.id;
                entry = id.substring(0, 5);
                // ターゲット一覧から追加一覧へドラッグ&ドロップ
                if (entry == 'entry' && targetId == 'add_list') {
                    del_report_id = id.substring(5, id.length);
                    document.list_form.del_report_id.value = del_report_id;
                    document.list_form.action="hiyari_rp_same_report_link.php?session=<{$session}>";
                    document.list_form.submit();
                }

                add = id.substring(0, 3);
                // 追加一覧からターゲット一覧へドラッグ&ドロップ
                if (add == 'add' && targetId == 'entry_list') {
                    add_report_id = id.substring(3, id.length);
                    document.list_form.add_report_id.value = add_report_id;
                    document.list_form.action="hiyari_rp_same_report_link.php?session=<{$session}>";
                    document.list_form.submit();
                }
            }

            doc.endDrag = function (x, y) {
                var el = this.getDragEl();
                YAHOO.util.Dom.setXY(el, this.startPos);
                el.style.backgroundColor = '';
                el.style.zIndex = 0;
            }
        }
    }
}

// 日付クリア
function date_clear(id) {
    var obj = document.getElementById(id);
    obj.value = '';
}

// 検索処理
function search_report() {
    document.list_form.action="hiyari_rp_same_report_link.php?session=<{$session}>";
    document.list_form.submit();
}

// 部署マスタ設定
function setAtrbOptions(atrb_id, dept_id, room_id) {
    deleteAllOptions(document.list_form.search_emp_attribute);

    addOption(document.list_form.search_emp_attribute, '', '----------', atrb_id);

    var class_id = document.list_form.search_emp_class.value;
    switch (class_id) {
        <{foreach from=$atrb_list key=class_id item=atrb}>
            case '<{$class_id}>':
                <{foreach from=$atrb key=atrb_id item=atrb_name}>
                    addOption(document.list_form.search_emp_attribute, '<{$atrb_id}>', '<{$atrb_name}>', atrb_id);
                <{/foreach}>
                break;
        <{/foreach}>
    }

    setDeptOptions(dept_id, room_id);
}

function setDeptOptions(dept_id, room_id) {
    deleteAllOptions(document.list_form.search_emp_dept);

    addOption(document.list_form.search_emp_dept, '', '----------', dept_id);

    var atrb_id = document.list_form.search_emp_attribute.value;
    switch (atrb_id) {
        <{foreach from=$dept_list key=atrb_id item=dept}>
            case '<{$atrb_id}>':
                <{foreach from=$dept key=dept_id item=dept_name}>
                    addOption(document.list_form.search_emp_dept, '<{$dept_id}>', '<{$dept_name}>', dept_id);
                <{/foreach}>
            break;
        <{/foreach}>
    }

    <{if isset($room_list)}>
        setRoomOptions(room_id);
    <{/if}>
}

function setRoomOptions(room_id) {
    deleteAllOptions(document.list_form.search_emp_room);

    addOption(document.list_form.search_emp_room, '', '----------', room_id);

    var dept_id = document.list_form.search_emp_dept.value;
    switch (dept_id) {
        <{foreach from=$room_list key=dept_id item=room}>
            case '<{$dept_id}>':
                <{foreach from=$room key=room_id item=room_name}>
                    addOption(document.list_form.search_emp_room, '<{$room_id}>', '<{$room_name}>', room_id);
                <{/foreach}>
              break;
        <{/foreach}>
    }
}

function addOption(box, value, text, selected) {
    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;
    if (selected == value) {
        opt.setAttribute("selected", true);
    }
    box.options[box.length] = opt;
    try {box.style.fontSize = 'auto';} catch (e) {}
    box.style.overflow = 'auto';
}

function deleteAllOptions(box) {
    for (var i = box.length - 1; i >= 0; i--) {
        box.options[i] = null;
    }
}

function regist_report() {
    document.list_form.mode.value = "regist";
    document.list_form.action="hiyari_rp_same_report_link.php?session=<{$session}>";
    document.list_form.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
  .list {border-collapse:collapse;}
  .list td {border:#35B341 solid 1px;}

  table.block {border-collapse:collapse;}
  table.block td {border:#35B341 solid 1px;padding:1px;}
  table.block td td {border-width:0;}

  table.block_in {border-collapse:collapse;}
  table.block_in td {border:#35B341 solid 0px;}
  table.block_in td td {border-width:1;}

  #entry_list {
      background-color: #f3f3f3; border: 1px solid black;
      overflow: none; padding: 5px; font-size: 85%;
  }

  #add_list {
      background-color: #F5FFE5; border: 1px solid black;
      overflow: none; padding: 5px; font-size: 85%;
  }
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initcal();initPage();">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr height="32" bgcolor="#35B341">
      <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><{$title}></b></font></td>
      <td>&nbsp;</td>
      <td width="10">&nbsp;</td>
      <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
    </tr>
  </table>
  <img src="img/spacer.gif" width="10" height="10" alt=""><br>
  
  <form name="list_form" action="hiyari_rp_same_report_link.php" method="post">
    <input type="hidden" name="session" value="<{$session}>">
    <input type="hidden" name="page" value="<{$page}>">
    <input type="hidden" name="add_report_id" value="">
    <input type="hidden" name="entry_report_id" value="<{$entry_report_id}>">
    <input type="hidden" name="del_report_id" value="">
    <input type="hidden" name="target_report_id" value="<{$target_report_id}>">
    <input type="hidden" name="is_postback" value="true">
    <input type="hidden" name="mode" value="">
    <input type="hidden" name="color_target_report_id" value="<{$color_target_report_id}>">
    <input type="hidden" name="color_add_report_id" value="<{$color_add_report_id}>">

    <table border="0" cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
      </tr>
      <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td>
          <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
              <td width="25%" bgcolor="#F5FFE5">
                <table cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                  </tr>
                </table>
              </td>
              <td width="50%" bgcolor="#F5FFE5">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td width="320" valign="bottom">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">【事案番号関連付け】(主報告にはチェックをつけてください)</font>
                    </td>
                    <td>
                      <table border="0" cellspacing="0" cellpadding="1">
                        <tr>
                          <td width="20"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                          <td width="512" align="center">
                            <table border="0" cellspacing="1" cellpadding="1">
                              <tr>
                                <td colspan="4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">【検索条件指定】</font></td>
                              </tr>
                              <tr>
                                <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
                                <td nowrap>
                                  <table border="0" cellspacing="0" cellpadding="2">
                                    <tr>
                                      <td>
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                          <select name="search_emp_class" onchange="setAtrbOptions();">
                                            <option value="">----------</option>
                                            <{foreach from=$class_list key=class_id item=class_name}>
                                                <option value="<{$class_id}>" <{if $search_emp_class == $class_id}>selected<{/if}>>
                                                    <{$class_name}>
                                                </option>
                                            <{/foreach}>
                                          </select>
                                          <{$arr_class_name[0]}><BR>
                                          
                                          <select name="search_emp_attribute" onchange="setDeptOptions();"></select>
                                          <{$arr_class_name[1]}><BR>
                                          
                                          <select name="search_emp_dept" <{if isset($room_list)}>onchange="setRoomOptions();"<{/if}>></select>
                                          <{$arr_class_name[2]}><BR>
                                          
                                          <{if isset($room_list)}>
                                            <select name="search_emp_room"></select>
                                            <{$arr_class_name[3]}>
                                          <{/if}>
                                        </font>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                                <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告者名</font></td>
                                <td>
                                  <table border="0" cellspacing="0" cellpadding="2">
                                    <tr>
                                      <td><input type="text" name="search_emp_name" value="<{$search_emp_name}>" size="30" maxlength="30" style="ime-mode:active"></td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告日</font></td>
                                <td  colspan="3">
                                  <table border="0" cellspacing="0" cellpadding="2">
                                    <tr>
                                      <td>
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                          <input type="text" id="start_date" name="start_date" size="14" value="<{$start_date}>" readonly>
                                          <img src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
                                          <div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
                                        </font>
                                      </td>
                                      <td>
                                        <input type="button" value="クリア" onclick="date_clear('start_date');">
                                      </td>
                                      <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">〜</font></td>
                                      <td>
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                          <input type="text" id="end_date" name="end_date" size="14" value="<{$end_date}>" readonly>
                                          <img src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer;" onclick="show_cal2();"/><br>
                                          <div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
                                        </font>
                                      </td>
                                      <td>
                                        <input type="button" value="クリア" onclick="date_clear('end_date');">
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                                <td align="right"><input type="button" value="検索" onclick="search_report();"></td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  
                  <!-- エントリー一覧 start-->
                  <tr>
                    <td width="320" valign="top">
                      <div id="entry_list"  class="doc">
                        <table width="320" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="450" valign="top">
                              <table width="320" border="0" cellspacing="0" cellpadding="2" class="list">
                                <tr bgcolor="#FFFBE7">
                                  <td width="20" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">主</font></td>
                                  <td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事案番号</font></td>
                                  <td width="200"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題</font></td>
                                </tr>
                                <{foreach from=$linked_report_info item=info}>
                                  <tr bgcolor="<{if $info.is_add_report}>#F5FFE5<{elseif $info.is_target_report}>#FFDDFD<{else}>#FFFFFF<{/if}>">
                                    <td align="center">
                                      <input type="radio" name="main_report_id" value="<{$info.report_id}>" <{if $main_report_id == $info.report_id}>checked<{/if}>>
                                    </td>
                                    <td style="padding:0;">
                                    <div id="entry<{$info.report_id}>" class="doc">
                                      <table border="0" cellspacing="0" cellpadding="2" class="block_in">
                                        <tr valign="top">
                                          <td>
                                            <img id="h_entry<{$info.report_id}>" src="img/icon/file.gif" alt="" width="16" height="16" border="0" style="vertical-align:middle;cursor:move;">
                                          </td>
                                          <td nowrap>
                                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$info.report_no}></font>
                                          </td>
                                        </tr>
                                      </table>
                                    </div>
                                    </td>
                                    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$info.report_title|escape}></font></td>
                                  </tr>
                                <{/foreach}>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </div>

                      <table border="0" cellspacing="1" cellpadding="0" width="332">
                        <tr>
                          <td align="right"><input type="button" value="登録" onclick="regist_report();"></td>
                        </tr>
                      </table>
                    </td>
                    <!-- エントリー一覧 end-->

                    <td valign="top">
                      <table border="0" cellspacing="0" cellpadding="1">
                        <tr>
                          <td width="20" aling="center">&lt;==&gt;</td>
                          
                          <!-- 検索一覧 start-->
                          <td width="500">
                            <div id="add_list"  class="doc">
                              <table width="500" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td height="500" valign="top">
                                    <table width="500" border="0" cellspacing="0" cellpadding="2" class="list">
                                      <tr bgcolor="#FFFBE7">
                                        <td width="100" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事案番号</font></td>
                                        <td width="200"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題</font></td>
                                        <td width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告者</font></td>
                                        <td width="80" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告日</font></td>
                                      </tr>
                                      <{foreach from=$add_report_info item=info}>
                                        <tr bgcolor="<{if $info.is_add_report}>#F5FFE5<{elseif $info.is_target_report}>#FFDDFD<{else}>#FFFFFF<{/if}>">
                                          <td style="padding:0;">
                                            <div id="add<{$info.report_id}>" class="doc">
                                              <table border="0" cellspacing="0" cellpadding="2" class="block_in">
                                                <tr valign="top">
                                                  <td nowrap><img id="h_add<{$info.report_id}>" src="<{$info.little_report_img}>" alt="" width="16" height="16" border="0" style="vertical-align:middle;cursor:move;"></td>
                                                  <td nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$info.report_no}></font></td>
                                                </tr>
                                              </table>
                                            </div>
                                          </td>
                                          <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$info.report_title|escape}></font></td>
                                          <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$info.create_emp_name}></font></td>
                                          <td nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$info.registration_date}></font></td>
                                        </tr>
                                      <{/foreach}>
                                    </table>
                                    <{include file="hiyari_paging1.tpl"}>
                                  </td>
                                </tr>
                              </table>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </td>
                    <!-- 検索一覧 end-->
                    <td>
                      <table cellspacing="0" cellpadding="0" border="0">
                        <tr><td><img src="img/spacer.gif" width="1" height="1" alt=""></td></tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
              <td width="25%" bgcolor="#F5FFE5">
                <table cellspacing="0" cellpadding="0" border="0">
                  <tr><td><img src="img/spacer.gif" width="1" height="1" alt=""></td></tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
      </tr>
      <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
      </tr>
    </table>
    <img src="img/spacer.gif" width="1" height="5" alt=""><br>
  </form>
</body>
</html>
