<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
  <title><{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
	<meta http-equiv="imagetoolbar" content="no" />
	<link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/news_menu.css" />
	<style type="text/css">
		div#content table.v_title td #tinymce_div td { border-bottom-color: white; padding : 0px; }
		div#tinymce_div td table { width : 0px; }
		div#tinymce_div td table #news_fontsizeselect { width : 100%; }
		#news_fontsizeselect_text {width : 100%}

	</style>
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
  <script type="text/javascript" src="js/hiyari_news_edit.js"></script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
</head>

<body id="top" onload="init_page('<{$mode}>', '<{$news_non_tag}>')">
  <{*ヘッダー*}>
  <{include file="hiyari_header2.tpl"}>

	<div id="content" class="clearfix <{$tab_info.total_width_class}>">
		<div id="sub" class="clearfix">
			<div id="sub_menu" class="color01 radius">
				<h2><img src="img/side_nav_txt01.gif" alt="MENU" /></h2>
				<ul>
					<li><a href="hiyari_news_menu.php?session=<{$session}>&page=<{$page}>">お知らせ管理</a></li>
          <{if $mode=="register"}>
            <li class="active none"><a href="#">お知らせ登録</a></li>
          <{else}>
            <li><a href="hiyari_news_register.php?session=<{$session}>">お知らせ登録</a></li>
            <li class="active none"><a href="#">お知らせ詳細</a></li>
          <{/if}>
				</ul>
			</div>
    </div>
    
    <div id="main" class="<{$tab_info.main_width_class}> autoPadDiv">
      <form name="mainform" action="<{$action_file}>" method="post">
        <div id="news_edit" class="color02">
          <table id="news_edit_tbl" class="v_title">
            <tr>
              <th>タイトル</th>
              <td>
                <input type="text" name="news_title" value="<{$news_title}>" size="50" maxlength="100" style="ime-mode:active;">
              </td>
            </tr>
            <tr>
              <th>ラインマーカー</th>
              <td>
                <select name="marker" class="mL0">
                  <option value="0" style="background-color:white;color:black;"   <{if $marker == 0}>selected<{/if}>>なし</option>
                  <option value="1" style="background-color:red;color:white;"     <{if $marker == 1}>selected<{/if}>>赤</option>
                  <option value="2" style="background-color:aqua;color:blue;"     <{if $marker == 2}>selected<{/if}>>青</option>
                  <option value="3" style="background-color:yellow;color:blue;"   <{if $marker == 3}>selected<{/if}>>黄</option>
                  <option value="4" style="background-color:lime;color:blue;"     <{if $marker == 4}>selected<{/if}>>緑</option>
                  <option value="5" style="background-color:fuchsia;color:white;" <{if $marker == 5}>selected<{/if}>>ピンク</option>
                </select>
              </td>
            </tr>
            <tr>
              <th>カテゴリ</th>
              <td>
                <select name="news_category" onchange="showSubCategory();" class="mL0">
                  <{foreach from=$category_list key=id item=name}>
                    <option value="<{$id}>" <{if $id == $news_category}>selected<{/if}>><{$name}></option>
                  <{/foreach}>
                </select>&nbsp;
                <select name="class_id" style="display:none;">
                  <{foreach from=$class_list key=id item=name}>
                    <option value="<{$id}>" <{if $id == $class_id}>selected<{/if}>><{$name}></option>
                  <{/foreach}>
                </select>
                <select name="job_id" style="display:none;">
                  <{foreach from=$job_list key=id item=name}>
                    <option value="<{$id}>" <{if $id == $job_id}>selected<{/if}>><{$name}></option>
                  <{/foreach}>
                </select>
              </td>
            </tr>
            <tr>
              <th>内容</th>
              <td>
                <div id="tinymce_div">
                  <textarea name="news" cols="40" rows="12" style="ime-mode:active;"><{$news}></textarea>
                </div>
              </td>
            </tr>
            <tr>
              <th>添付ファイル</th>
              <td>
                <div id="attach">
                  <{foreach name=attach from=$file_list key=id item=file}>
                    <p id="p_<{$id}>" class="attach<{if $smarty.foreach.attach.last}>mB10<{/if}>">
                      <a href="<{$newsfile_folder_name}>/tmp/<{$session}>_<{$id}><{$file.ext}>" target="_blank"><{$file.name}></a>
                      <input type="button" id="btn_<{$id}>" class="button radius_mini" name="btn_<{$id}>" value="削除" onclick="detachFile(event);">
                      <input type="hidden" name="filename[]" value="<{$file.name}>">
                      <input type="hidden" name="file_id[]" value="<{$id}>">
                    </p>
                  <{/foreach}>                    
                </div>
                <input class="button radius_mini" type="button" value="追加" onclick="attachFile('<{$session}>');">                
              </td>
            </tr>
            <tr>
              <th>参照者を記録する</th>
              <td>
                <label for="record_yes"><input type="radio" class="radio" name="record_flg" id="record_yes" value="t" <{if $record_flg == "t"}>checked<{/if}>>する&nbsp;</label>
                <label for="record_no" ><input type="radio" class="radio" name="record_flg" id="record_no" value="f" <{if $record_flg == "f"}>checked<{/if}>>しない</label>   
              </td>
            </tr>
            <tr>
              <th>掲載期間</th>
              <td>
                <select name="news_begin1" class="mL0">
                  <{foreach from=$stt_years item=y}>
                    <option value="<{$y}>" <{if $y == $news_begin1}>selected<{/if}>><{$y}></option>
                  <{/foreach}>
                </select>/
                <select name="news_begin2">
                  <{foreach from=$months item=m}>
                    <option value="<{$m}>" <{if $m == $news_begin2}>selected<{/if}>><{$m}></option>
                  <{/foreach}>
                </select>/
                <select name="news_begin3">
                  <{foreach from=$days item=d}>
                    <option value="<{$d}>" <{if $d == $news_begin3}>selected<{/if}>><{$d}></option>
                  <{/foreach}>
                </select> 〜
                <select name="news_end1">
                  <{foreach from=$end_years item=y}>
                    <option value="<{$y}>" <{if $y == $news_end1}>selected<{/if}>><{$y}></option>
                  <{/foreach}>
                </select>/
                <select name="news_end2">
                  <{foreach from=$months item=m}>
                    <option value="<{$m}>" <{if $m == $news_end2}>selected<{/if}>><{$m}></option>
                  <{/foreach}>
                </select>/
                <select name="news_end3">
                  <{foreach from=$days item=d}>
                    <option value="<{$d}>" <{if $d == $news_end3}>selected<{/if}>><{$d}></option>
                  <{/foreach}>
                </select>                
              </td>
            </tr>
            <{if $mode=="update"}>
              <tr>
                <th>登録者</th>
                <td>
                  <{$emp_name}><input type="hidden" name="emp_name" value="<{$emp_name}>">
                </td>
              </tr>
            <{/if}>
            <tr>
              <th class="none">登録日</th>
              <td class="none">
                <select name="news_date1" class="mL0">
                  <{foreach from=$reg_years item=y}>
                    <option value="<{$y}>" <{if $y == $news_date1}>selected<{/if}>><{$y}></option>
                  <{/foreach}>
                </select>/
                <select name="news_date2">
                  <{foreach from=$months item=m}>
                    <option value="<{$m}>" <{if $m == $news_date2}>selected<{/if}>><{$m}></option>
                  <{/foreach}>
                </select>/
                <select name="news_date3">
                  <{foreach from=$days item=d}>
                    <option value="<{$d}>" <{if $d == $news_date3}>selected<{/if}>><{$d}></option>
                  <{/foreach}>
                </select>                
              </td>
            </tr>
          </table>
        </div>
        <div id="main_btn">
          <input class="button radius_mini" type="submit" value="<{$action_btn}>">
        </div>
        <input type="hidden" name="session" value="<{$session}>">
        <{if $mode=="update"}>
          <input type="hidden" name="news_id" value="<{$news_id}>">
          <input type="hidden" name="page" value="<{$page}>">
        <{/if}>
      </form>
    </div>
  </div>
	<div id="footer">
		<p>Copyright Comedix all right reserved.</p>
	</div>
</body>
</html>
