<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
  <script type="text/javascript" src="js/hiyari_el.js"></script>
  <script type="text/javascript">
    var w = 1024 + 37;
    var h = window.screen.availHeight;
    window.resizeTo(w, h);
    
    //--------------------------------------------------
    //登録対象者配列
    //--------------------------------------------------
    m_target_list = new Array();
    //参照権限の対象職員
    m_target_list[1] = new Array(
        <{foreach name=n from=$arr_target[1] item=i}>
          <{if !$smarty.foreach.n.first}>,<{/if}>
          new user_info('<{$i.id}>','<{$i.name}>')
        <{/foreach}>
      );
    //更新権限の対象職員
    m_target_list[2] = new Array(
        <{foreach name=n from=$arr_target[2] item=i}>
          <{if !$smarty.foreach.n.first}>,<{/if}>
          new user_info('<{$i.id}>','<{$i.name}>')
        <{/foreach}>
      );

    //--------------------------------------------------
    //部署情報配列
    //--------------------------------------------------
    //部
    var classes = [];
    <{foreach from=$sel_class item=i}>
      classes.push({id: '<{$i.class_id}>', name: '<{$i.class_nm}>'});
    <{/foreach}>

    //課
    var atrbs = {};
    <{foreach from=$sel_atrb key=class_id item=c}>
      atrbs['<{$class_id}>'] = [];
      <{foreach from=$c key=atrb_id item=a}>
        atrbs['<{$class_id}>'].push({id: '<{$atrb_id}>', name: '<{$a}>'});
      <{/foreach}>
    <{/foreach}>

    //科
    var depts = {};
    <{foreach from=$sel_dept key=class_id item=c}>
      depts['<{$class_id}>'] = {};
      <{foreach from=$c key=atrb_id item=a}>
        depts['<{$class_id}>']['<{$atrb_id}>'] = [];
        <{foreach from=$a key=dept_id item=d}>
          depts['<{$class_id}>']['<{$atrb_id}>'].push({id: '<{$dept_id}>', name: '<{$d}>'});
        <{/foreach}>
      <{/foreach}>
    <{/foreach}>

    //--------------------------------------------------
    //画面起動時の処理
    //--------------------------------------------------
    function initPage()
    {
        //登録対象者を設定する。
        update_target_html("1");
        update_target_html("2");

        //権限部分の初期制御
        onChangeArchive(true, '<{$ref_class_src}>', '<{$ref_atrb_src}>', '<{$upd_class_src}>', '<{$upd_atrb_src}>');
    }
  </script>
  <script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
  <script type="text/javascript" src="js/yui/build/connection/connection-min.js"></script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
</head>

<body id="el_reg" onload="initPage();">
  <!-------------------------------------------------------------------------
  ヘッダー 
  -------------------------------------------------------------------------->
	<div id="header">
		<div class="inner">
			<h1><{$PAGE_TITLE}></h1>
      <p class="action_btn">
        <{if $is_update}>
            <input type="button" class="button radius_mini" value="更新" <{if $edit_button_disabled}>disabled"<{/if}> onclick="submitForm();">
        <{else}>
            <input type="button" class="button radius_mini" value="作成" onclick="submitForm();">
        <{/if}>
      </p>
		</div>
	</div>

  <!-------------------------------------------------------------------------
  コンテンツ
  -------------------------------------------------------------------------->
  <div id="content" class="clearfix">
    <div class="section">
        <{if $is_update}>
          <form name="mainform" action="hiyari_el_folder_update_exe.php" method="post">
        <{else}>
          <form name="mainform" action="hiyari_el_folder_register_exe.php" method="post">
        <{/if}>
        <div class="color02 radius clearfix mB20">
          <table class="v_title">
            <!-- 作成先 START -->
            <tr>
              <th width="100">
                <{if $is_update}>
                  親フォルダ
                <{else}>
                  作成先
                <{/if}>                      
              </th>
              <td>
                <!-- 書庫移行のパス出力 -->
                <{if isset($cate_nm)}>
                    <{$cate_nm}>
                <{else}>
                  （最上位フォルダ）
                <{/if}>
                <{foreach name=n from=$folder_path item=fp}>
                  &gt;&nbsp;<{$fp.name}>
                <{/foreach}>
              </td>
            </tr>
            <!-- 作成先 END -->

            <!-- フォルダ名 START -->
            <tr>
              <th class="none" width="100">フォルダ名</th>
              <td class="none">
                <input type="text" name="folder_name" size="60" maxlength="60" value="<{$folder_name}>" style="ime-mode:active;">
              </td>
            </tr>
            <!-- フォルダ名 END -->
          </table>
        </div>
        
        <!-- 参照・更新可能職員指定 -->
        <{include file="hiyari_el_emp2.tpl"}>
                
        <input type="hidden" name="session" value="<{$session}>">
        <input type="hidden" name="archive" value="<{$archive}>">
        <input type="hidden" name="category" value="<{$cate_id}>">
        <input type="hidden" name="cate_id" value="<{$cate_id}>">
        <input type="hidden" name="parent" value="<{$parent}>">
        <input type="hidden" name="folder_id" value="<{$folder_id}>">
        <input type="hidden" name="path" value="<{$path}>">
        <input type="hidden" name="ref_toggle_mode" value="">
        <input type="hidden" name="upd_toggle_mode" value="">
        <input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
        <input type="hidden" id="target_name_list1" name="target_name_list1" value="">
        <input type="hidden" id="target_id_list2"   name="target_id_list2" value="">
        <input type="hidden" id="target_name_list2" name="target_name_list2" value="">
      </form>
    </div>
  </div>
  
  <!-----------------------------------------------------------------
  フッター
  ------------------------------------------------------------------>
  <div id="footer">
    <div class="inner">
      <p class="action_btn">
        <{if $is_update}>
            <input type="button" class="button radius_mini" value="更新" <{if $edit_button_disabled}>disabled"<{/if}> onclick="submitForm();">
        <{else}>
            <input type="button" class="button radius_mini" value="作成" onclick="submitForm();">
        <{/if}>
      </p>
    </div>
  </div>
</body>
</html>