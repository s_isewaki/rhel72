<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix ファントルくん | 集計条件読込</title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
  <script type="text/javascript">
    function load_stats_condition(id) { 
      opener.load_stats_condition(id);
      window.close();
    }
    
    function delete_condition(){
      var stats = document.getElementsByName('stats_delete[]');
      var flg = false;
      for ( var i=0; i < stats.length; i++ ) {
          if (stats[i].checked) flg = true;
      }
      if (flg == false) {
          alert('削除対象が選択されていません。');
          return;
      }
      document.main_form.postback_mode.value='delete';
      document.main_form.submit();
    }
  </script>
</head>

<body id="stats_condition">
  <!-------------------------------------------------------------------------
  ヘッダー 
  -------------------------------------------------------------------------->
	<div id="header" class="clearfix">
		<div class="inner" class="clearfix">
			<h1>集計条件一覧</h1>
		</div>
	</div>

  <!-------------------------------------------------------------------------
  コンテンツ
  -------------------------------------------------------------------------->
  <div id="content" class="clearfix">
    <div class="section">
      <form name="main_form" action="hiyari_rm_stats_condition_load.php" method="post">
        <input type="hidden" name="session" value="<{$session}>">
        <input type="hidden" name="postback_mode" value="">
        
        <div class="color02 radius clearfix">
          <ul id="main_nav">
            <li class="main_nav_90"><a href="#" onclick="delete_condition();">削除</a></li>
          </ul>
          
          <table class="list">
            <tr>
              <th></th>
              <th class="none">集計条件</th>
            </tr>
            <{if isset($stats_condition)}>
              <{foreach from=$stats_condition item=i}>
                <tr>
                  <td>
                    <input type="checkbox" name="stats_delete[]" value="<{$i.condition_id|escape}>">
                  </td>
                  <td width="99%" class="none">
                    <a name="selected_link" href="javascript:load_stats_condition('<{$i.condition_id|escape}>');"><{$i.stats_name}></a>
                  </td>
                </tr>
              <{/foreach}>
            <{/if}>
          </table>
        </div>
      </form>
    </div>
  </div>
</body>
</html>
