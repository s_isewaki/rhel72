<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
	<title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
	<meta http-equiv="imagetoolbar" content="no" />
	<link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/news.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/pagination.css" />
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
</head>

<body id="top">
  <{*ヘッダー*}>
  <{include file="hiyari_header2.tpl"}>
	
	<div id="content" class="<{$tab_info.total_width_class}>">
    <{*警告メッセージ*}>
    <{if $warning_flg}>
      <p class="notice radius"><{$warning_message}></p>
    <{/if}>
    
    <{*未読件数*}>
    <{if $no_recv_read_num > 0}>
      <p class="notice radius">未読のメッセージが&nbsp;<{$no_recv_read_num}>&nbsp;件あります。</p>
    <{/if}> 
    
    <{*下書き件数*}>
    <{if $no_shitagaki_num > 0}>
      <p class="notice radius">下書き報告書件数は&nbsp;<{$no_shitagaki_num}>&nbsp;件です。</p>
    <{/if}>
    
    <{*お知らせ*}>
		<h2 class="type01"><{$news_title}></h2>    
		<div class="color01 radius">
			<table id="news_list" class="h_title">
				<tr>
					<th width="10%">日 時</th>
					<th width="30%">場 所</th>
					<th width="60%">内 容</th>
				</tr>
        <{if isset($news_list)}>
          <{foreach from=$news_list item=data}>
            <tr class="<{$data.class}>">
              <td><span><{$data.date}></span></td>
              <td class="left"><span>（<{$data.category}>）</span></td>
              <td class="left">
                <span class="none">
                  <a href="javascript:void(0)" onclick="window.open('hiyari_news_detail_from_top.php?session=<{$session}>&news_id=<{$data.id}>','newwin','scrollbars=yes,width=640,height=480');return false;" <{$data.style}>><{$data.title}></a>
                </span>
              </td>
            </tr>
            <{if isset($data.news)}>
              <tr>
                <td class="detail left" colspan="3">
                  <span class="none">
                    <{$data.news}>
                    <{if isset($data.img)}>
                      <{$data.img}>
                    <{/if}>
                  </span>
                </td>
              </tr>
            <{/if}>
          <{/foreach}>
        <{else}>
          <tr>
            <td colspan="3"><span class="none">お知らせはありません。</span></td>
          </tr>
        <{/if}>
			</table>
		</div>
    
    <{*評価予定期日の報告書件数*}>
    <{if $is_evaluation_date_auth_all_disp || $is_evaluation_date_auth_post_disp}>
      <h2 class="type02 report_cnt">評価予定期日の報告書件数</h2>
      <div class="color02 radius">
        <table class="list">
          <tr>
            <th width="20%">評価日が過ぎた報告書</th>
            <th width="20%">本 日</th>
            <th width="20%">翌 日</th>
            <th width="20%">1週間以内</th>
            <th width="20%" class="none">2週間以内</th>
          </tr>
          <tr>
            <{foreach name=rpt_cnt from=$report_cnt_list key=type item=count}>
              <{if $smarty.foreach.rpt_cnt.first}>
                <td>
                  <span>
              <{elseif $smarty.foreach.rpt_cnt.last}>
                <td class="none">
                  <span>
              <{else}>
                <td>
                  <span>
              <{/if}>
                  <{if $is_usable}>
                    <a href="<{$classification_link}><{$type}>">
                  <{/if}>
                    <{$count}>
                  <{if $is_usable}>
                    </a>
                  <{/if}>
                </span>
              </td>
            <{/foreach}>
          </tr>
        </table>
      </div>
    <{/if}>
    
  </div>
  
  <{*フッター*}>
  <{include file="hiyari_footer2.tpl"}>
  
  <form name="list_form" method="post" action="hiyari_menu.php">
    <input type="hidden" name="session" value="<{$session}>">
    <input type="hidden" name="mode" value="page_change">
    <input type="hidden" name="page" value="1">
  </form>
</body>
</html>