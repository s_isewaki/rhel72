<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="javascript">
    function init_page()
    {
        //部署
        set_post_select_box_obj_key("search_emp_post","search_emp_class","search_emp_attribute","search_emp_dept","search_emp_room");
        setClassOptions("search_emp_post",'<{$class_id}>','<{$attribute_id}>','<{$dept_id}>','<{$room_id}>');
        
        //患者年齢
        if(document.getElementById('patient_year_from').value == '900'){
            document.getElementById('patient_year_to').disabled = true;
        }
        
        disp_control_120_400();
        disp_control_900_1000();
        set_disabled_enabled();
    }


    //患者年齢
    function check_patient_year(div)
    {
        if(div == 'from'){
            var year_from = document.getElementById('patient_year_from').value;
            if(year_from == '900'){
                document.getElementById('patient_year_to').value = '';
                document.getElementById('patient_year_to').disabled = true;
            }
            else{
                document.getElementById('patient_year_to').disabled = false;
            }
        }
        else if(div == 'to'){
            var year_to = document.getElementById('patient_year_to').value;
            if(year_to == '900'){
                document.getElementById('patient_year_from').value = '900';
                document.getElementById('patient_year_to').value = '';
                document.getElementById('patient_year_to').disabled = true;
            }
            else{
                document.getElementById('patient_year_to').disabled = false;
            }
        }
    }

    function set_disabled_enabled()
    {
        var inci_level_regist_flg = false;
        for(i=0; i<document.mainform.inci_level_regist_flg.length;i++) {
            if(document.mainform.inci_level_regist_flg[i].type == "checkbox") {
                inci_level_regist_flg = document.mainform.inci_level_regist_flg[i].checked;
            }
        }

        var happened_cause_regist_flg = false;
        for(i=0; i<document.mainform.happened_cause_regist_flg.length;i++) {
            if(document.mainform.happened_cause_regist_flg[i].type == "checkbox") {
                happened_cause_regist_flg = document.mainform.happened_cause_regist_flg[i].checked;
            }
        }

        var inci_background_regist_flg = false;
        for(i=0; i<document.mainform.inci_background_regist_flg.length;i++) {
            if(document.mainform.inci_background_regist_flg[i].type == "checkbox") {
                inci_background_regist_flg = document.mainform.inci_background_regist_flg[i].checked;
            }
        }

        var improve_plan_regist_flg = false;
        for(i=0; i<document.mainform.improve_plan_regist_flg.length;i++) {
            if(document.mainform.improve_plan_regist_flg[i].type == "checkbox") {
                improve_plan_regist_flg = document.mainform.improve_plan_regist_flg[i].checked;
            }
        }

        var effect_confirm_regist_flg = false;
        for(i=0; i<document.mainform.effect_confirm_regist_flg.length;i++) {
            if(document.mainform.effect_confirm_regist_flg[i].type == "checkbox") {
                effect_confirm_regist_flg = document.mainform.effect_confirm_regist_flg[i].checked;
            }
        }

        var warning_case_regist_flg = false;
        for(i=0; i<document.mainform.warning_case_regist_flg.length;i++) {
            if(document.mainform.warning_case_regist_flg[i].type == "checkbox") {
                warning_case_regist_flg = document.mainform.warning_case_regist_flg[i].checked;
            }
        }

        var analysis_target_flg = false;
        for(i=0; i<document.mainform.analysis_target_flg.length;i++) {
            if(document.mainform.analysis_target_flg[i].type == "checkbox") {
                analysis_target_flg = document.mainform.analysis_target_flg[i].checked;
            }
        }

        var title_keyword               = document.mainform.title_keyword.value;
        var incident_contents_keyword   = document.mainform.incident_contents_keyword.value;
        var search_emp_class            = document.mainform.search_emp_class.value;
        var search_emp_name             = document.mainform.search_emp_name.value;
        var search_emp_job              = document.mainform.search_emp_job.value;
        var patient_id                  = document.mainform.patient_id.value;
        var patient_year_from           = document.mainform.patient_year_from.value;
        var patient_year_to             = document.mainform.patient_year_to.value;
        var patient_name                = document.mainform.patient_name.value;
        var evaluation_date             = document.mainform.evaluation_date.value;

        var use_level_flg = false;
        var use_level_obj_list = document.getElementsByName("use_level[]");
        for(i=0;i<use_level_obj_list.length;i++){
            if(use_level_obj_list[i].checked){
                use_level_flg = true;
                break;
            }
        }

        var hiyari_summary_flg = false;
        var hiyari_summary_obj_list = document.getElementsByName("hiyari_summary[]");
        for(i=0;i<hiyari_summary_obj_list.length;i++){
            if(hiyari_summary_obj_list[i].checked){
                hiyari_summary_flg = true;
                break;
            }
        }


        var use_120_flg = false;
        var use_120_obj_list = document.getElementsByName("use_120[]");
        for(i=0;i<use_120_obj_list.length;i++){
            if(use_120_obj_list[i].checked){
                use_120_flg = true;
                break;
            }
        }

        var use_900_flg = false;
        var use_900_obj_list = document.getElementsByName("use_900[]");
        for(i=0;i<use_900_obj_list.length;i++){
            if(use_900_obj_list[i].checked){
                use_900_flg = true;
                break;
            }
        }


        var input_cnt = 0;

        if(inci_level_regist_flg == true) {
            input_cnt++;
        }
        if(happened_cause_regist_flg == true) {
            input_cnt++;
        }
        if(inci_background_regist_flg == true) {
            input_cnt++;
        }
        if(improve_plan_regist_flg == true) {
            input_cnt++;
        }
        if(effect_confirm_regist_flg == true) {
            input_cnt++;
        }
        if(warning_case_regist_flg == true) {
            input_cnt++;
        }
        if(analysis_target_flg == true) {
            input_cnt++;
        }
        if(analysis_target_flg == true) {
            input_cnt++;
        }

        if(title_keyword != "") {
            input_cnt++;
        }
        if(incident_contents_keyword != "") {
            input_cnt++;
        }
        if(search_emp_class != "") {
            input_cnt++;
        }
        if(search_emp_name != "") {
            input_cnt++;
        }
        if(search_emp_job != "") {
            input_cnt++;
        }
        if(patient_id != "") {
            input_cnt++;
        }
        if(patient_year_from != "" || patient_year_to != "") {
            input_cnt++;
        }
        if(patient_name != "") {
            input_cnt++;
        }
        if(evaluation_date != "") {
            input_cnt++;
        }
        if(use_level_flg){
            input_cnt++;
        }
        if(hiyari_summary_flg){
            input_cnt++;
        }
        if(use_120_flg){
            input_cnt++;
        }
        if(use_900_flg){
            input_cnt++;
        }

        //AND･ORのdisabledを判定
        var dis_enable_flg = false;
        if(input_cnt < 2) {
            document.mainform.and_or_div[0].checked = true;
            dis_enable_flg = true;
        }

        //AND･ORのdisabledを設定
        for(i=0; i<document.mainform.and_or_div.length; i++)
        {
            document.mainform.and_or_div[i].disabled = dis_enable_flg;
        }
    }

    //インシデントの概要の表示制御
    function disp_control_120_400()
    {
        //各インシデントの概要に対して
        for(var item_list_400_index = 0; item_list_400_index < 10; item_list_400_index++){
            //--------------------------------------------------
            //関連オブジェクトの取得
            //--------------------------------------------------

            //インシデントの概要checkboxオブジェクト
            var use_120_obj = document.getElementById("use_120_" + item_list_400_index);

            //場面・内容制限checkboxオブジェクト
            var use_400_bamen_obj = document.getElementById("use_400_bamen_" + item_list_400_index);
            var use_400_naiyo_obj = document.getElementById("use_400_naiyo_" + item_list_400_index);

            //場面・内容制限の表示領域divオブジェクト
            var area_400_use_obj = document.getElementById("area_400_use_" + item_list_400_index);

            //場面・内容の表示領域divオブジェクト
            var area_400_obj = document.getElementById("area_400_" + item_list_400_index);
            var area_400_bamen_obj = document.getElementById("area_400_bamen_" + item_list_400_index);
            var area_400_naiyo_obj = document.getElementById("area_400_naiyo_" + item_list_400_index);

            //場面・内容のcheckboxオブジェクト配列
            var bamen_obj_list = document.getElementsByName("bamen_" + item_list_400_index + "[]");
            var naiyo_obj_list = document.getElementsByName("naiyo_" + item_list_400_index + "[]");

            //--------------------------------------------------
            //制御?
            //--------------------------------------------------
            //インシデントの概要が選択されている場合
            if(use_120_obj.checked){
                //場面・内容制限を表示
                area_400_use_obj.style.display = "block";
            }
            //インシデントの概要が選択されていない場合
            else{
                //場面・内容制限を非表示
                area_400_use_obj.style.display = "none";

                //場面・内容制限は未選択
                use_400_bamen_obj.checked = false;
                use_400_naiyo_obj.checked = false;
            }

            //--------------------------------------------------
            //制御?
            //--------------------------------------------------
            //場面・内容制限を行う場合
            if(use_400_bamen_obj.checked || use_400_naiyo_obj.checked){
                //場面・内容領域を表示
                area_400_obj.style.display = "block";
            }
            //場面・内容制限を行わない場合
            else{
                //場面・内容領域を非表示
                area_400_obj.style.display = "none";
            }

            //場面制限を行う場合
            if(use_400_bamen_obj.checked){
                //場面領域を表示
                area_400_bamen_obj.style.display = "block";
            }
            //場面制限を行わない場合
            else{
                //場面領域を非表示
                area_400_bamen_obj.style.display = "none";

                //全ての場面の指定をクリア
                for(var i = 0; i < bamen_obj_list.length; i++){
                    bamen_obj_list[i].checked = false;
                }
            }

            //内容制限を行う場合
            if(use_400_naiyo_obj.checked){
                //内容領域を表示
                area_400_naiyo_obj.style.display = "block";
            }
            //内容制限を行わない場合
            else{
                //内容領域を非表示
                area_400_naiyo_obj.style.display = "none";

                //全ての内容の指定をクリア
                for(var i = 0; i < naiyo_obj_list.length; i++){
                    naiyo_obj_list[i].checked = false;
                }
            }
        }
    }

    //概要・場面・内容（2010年改訂）の表示制御
    function disp_control_900_1000()
    {
        //各インシデントの概要に対して
        <{foreach from=$gaiyo2010_list key=code_900 item=i}>
            disp_control_900_1000_item(<{$code_900}>);
        <{/foreach }>
    }

    function disp_control_900_1000_item(code_900)    
    {
        var item_list_1000_index = code_900 - 1;
        //--------------------------------------------------
        //関連オブジェクトの取得
        //--------------------------------------------------

        //インシデントの概要checkboxオブジェクト
        var use_900_obj = document.getElementById("use_900_" + item_list_1000_index);

        //場面・内容制限checkboxオブジェクト
        var use_1000_kind_obj    = document.getElementById("use_1000_kind_" + item_list_1000_index);
        var use_1000_scene_obj   = document.getElementById("use_1000_scene_" + item_list_1000_index);
        var use_1000_content_obj = document.getElementById("use_1000_content_" + item_list_1000_index);

        //場面・内容制限の表示領域divオブジェクト
        var area_1000_use_obj = document.getElementById("area_1000_use_" + item_list_1000_index);

        //場面・内容の表示領域divオブジェクト
        var area_1000_obj         = document.getElementById("area_1000_" + item_list_1000_index);
        var area_1000_kind_obj    = document.getElementById("area_1000_kind_" + item_list_1000_index);
        var area_1000_scene_obj   = document.getElementById("area_1000_scene_" + item_list_1000_index);
        var area_1000_content_obj = document.getElementById("area_1000_content_" + item_list_1000_index);

        //--------------------------------------------------
        //制御?
        //--------------------------------------------------

        //概要が選択されている場合
        if(use_900_obj.checked){
            //場面・内容制限を表示
            area_1000_use_obj.style.display = "block";
        }
        //概要が選択されていない場合
        else{
            //場面・内容制限を非表示
            area_1000_use_obj.style.display = "none";

            //場面・内容制限は未選択
            use_1000_kind_obj.checked = false;
            use_1000_scene_obj.checked = false;
            use_1000_content_obj.checked = false;
            //全ての種類の指定をクリア
            check_disappear_item('kind', item_list_1000_index);
            check_disappear_item('scene', item_list_1000_index);
            check_disappear_item('content', item_list_1000_index);
        }

        //--------------------------------------------------
        //制御?
        //--------------------------------------------------
        //場面・内容制限を行う場合
        if(use_1000_kind_obj.checked || use_1000_scene_obj.checked || use_1000_content_obj.checked){
            //場面・内容領域を表示
            area_1000_obj.style.display = "block";
        }
        //場面・内容制限を行わない場合
        else{
            //場面・内容領域を非表示
            area_1000_obj.style.display = "none";
            //全ての種類の指定をクリア
            check_disappear_item('kind', item_list_1000_index);
            check_disappear_item('scene', item_list_1000_index);
            check_disappear_item('content', item_list_1000_index);
        }

        //種類制限を行う場合
        if(use_1000_kind_obj.checked){
            //種類領域を表示
            area_1000_kind_obj.style.display = "block";
            check_appear_detail_item('kind', item_list_1000_index)
        }
        //種類制限を行わない場合
        else{
            //種類領域を非表示
            area_1000_kind_obj.style.display = "none";

            //全ての種類の指定をクリア
            check_disappear_item('kind', item_list_1000_index);
        }

        //場面制限を行う場合
        if(use_1000_scene_obj.checked){
            //場面領域を表示
            area_1000_scene_obj.style.display = "block";
            check_appear_detail_item('scene', item_list_1000_index)
        }
        //場面制限を行わない場合
        else{
            //場面領域を非表示
            area_1000_scene_obj.style.display = "none";

            //全ての場面の指定をクリア
            check_disappear_item('scene', item_list_1000_index);
        }

        //内容制限を行う場合
        if(use_1000_content_obj.checked){
            //内容領域を表示
            area_1000_content_obj.style.display = "block";
            check_appear_detail_item('content', item_list_1000_index)
        }
        //内容制限を行わない場合
        else{
            //内容領域を非表示
            area_1000_content_obj.style.display = "none";

            //全ての場面の指定をクリア
            check_disappear_item('content', item_list_1000_index);
        }
    }

    function check_disappear_item(code, item_list_1000_index) {
        //場面・内容のcheckboxオブジェクト配列
        var obj_list = document.getElementsByName(code + "_" + item_list_1000_index + "[]");
        // 絞込みのcheckboxオブジェクト配列
        var n_obj_list = document.getElementsByName("n_"+code+"_" + item_list_1000_index + "[]");
        // 詳細を取得
        var area_1000_detail_obj_list = document.getElementsByName("area_1000_"+code+"_detail_list_" + item_list_1000_index + "[]");

        //全ての種類の指定をクリア
        for(var i = 0; i < obj_list.length; i++){
            obj_list[i].checked = false;
        }
        for(var i = 0; i < n_obj_list.length; i++){
            n_obj_list[i].checked = false;
        }
        if(area_1000_detail_obj_list) {
            for(var i = 0; i < area_1000_detail_obj_list.length; i++) {
                var j = area_1000_detail_obj_list[i].value;
                document.getElementById("area_1000_"+code+"_detail_"+j).style.display = 'none';
            }
        }

        // 項目のcheckboxオブジェクト配列
        var detail_index = document.getElementsByName("area_1000_"+code+"_detail_list_"+item_list_1000_index+"[]");
        for(var i = 0; i < detail_index.length; i++) {
            var detail_obj_list = document.getElementsByName(code+"_detail_" + detail_index[i].value + "[]");
            for(var j = 0; j < detail_obj_list.length; j++){
                detail_obj_list[j].checked = false;
            }
        }

    }

    function check_appear_detail_item(code, item_list_1000_index) {
        var area_1000_detail_obj_list = document.getElementsByName("area_1000_"+code+"_detail_list_" + item_list_1000_index + "[]");

        if(area_1000_detail_obj_list) {
            for(var i = 0; i < area_1000_detail_obj_list.length; i++) {
                var j = area_1000_detail_obj_list[i].value;

                if(document.getElementById("id_n_"+j).checked == true) {
                    document.getElementById("area_1000_"+code+"_detail_"+j).style.display = 'block';
                }
            }
        }
    }
    // 詳細項目の表示・非表示
    function check_appear_item(OBJ, index, code, item_index) {
        var disp = document.getElementById("area_1000_"+code+"_detail_"+index);
        var item = document.getElementById("id_c_"+index);

        if(OBJ.checked == true) {
            disp.style.display = '';
            item.checked = true;
        } else {
            disp.style.display = 'none';
            // item.checked = false;
            var item_obj = document.getElementsByName(code+"_detail_"+index+"[]");
            for(var i=0; i<item_obj.length; i++) {
                item_obj[i].checked = false;
            }
        }
    }
</script>
<{include file="hiyari_post_select_js.tpl"}>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
  .list {border-collapse:collapse;}
  .list td {border:#35B341 solid 1px;}

  table.block_in {border-collapse:collapse;}
  table.block_in td {border:#35B341 solid 0px;}
  table.block_in td td {border-width:1;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init_page();">
  <form name="mainform" action="hiyari_rp_classification_folder_input.php" method="post">
    <input type="hidden" name="session" value="<{$session}>">
    <input type="hidden" name="mode" value="<{$mode}>">
    <input type="hidden" name="is_postback" value="true">
    <input type="hidden" name="folder_id" value="<{$folder_id}>">

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <!-- ヘッダー START -->
      <tr>
        <td>
          <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr height="32" bgcolor="#35B341">
              <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><{$PAGE_TITLE}></b></font></td>
              <td>&nbsp;</td>
              <td width="10">&nbsp;</td>
              <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
              </tr>
            </table>
            <img src="img/spacer.gif" width="10" height="10" alt=""><br>
          </table>
        </td>
      </tr>
      <!-- ヘッダー END -->
      
      <tr>
        <td>
          <table border="0" cellspacing="0" cellpadding="0" width="1000">
            <tr>
              <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
              <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
              <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
            </tr>
            <tr>
              <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
              <td bgcolor="#F5FFE5">
                <!--===========================================================
                フォルダ名とフォルダタイプ
                ============================================================-->
                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                  <tr>
                    <td align="right" width="150" bgcolor="#DFFFDC">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">分類フォルダ名</font>
                    </td>
                    <td align="left" bgcolor="#FFFFFF">
                      <input type="text" name="folder_name" value="<{$folder_name|escape}>" maxlength="50" style="width:500px">
                    </td>
                  </tr>
                  <tr>
                    <td align="right" width="150" bgcolor="#DFFFDC">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">共有／個人</font>
                    </td>
                    <td align="left" bgcolor="#FFFFFF">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                        <{if $mode == "new"}>
                          <select name="common_flg">
                            <option value="f">個人フォルダ</option>
                            <option value="t">共有フォルダ</option>
                          </select>
                        <{else}>
                          <{$folder_type}>
                        <{/if}>
                      </font>
                    </td>
                  </tr>
                </table>
              
                <!--===========================================================
                自動分類の条件設定
                ============================================================-->
                <img src="img/spacer.gif" width="1" height="10" alt="">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
                    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
                  </tr>
                  <tr>
                    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                    <td bgcolor="#F5FFE5">
                      <table border="0" cellspacing="0" cellpadding="2" class="list">
                        <tr>
                          <td width="140" align="center" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">自動分類用の条件設定</font></td>
                        </tr>
                      </table>
                      <table width="100%" border="0" cellspacing="0" cellpadding="2">
                        <tr>
                          <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10" color="red">＊以下の条件設定を行った場合、このフォルダに対して、その条件に該当するレポートを自動的に紐付けます。</font></td>
                        </tr>
                      </table>
                      
                      <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                        <!-----------------------------------------------------
                        報告者
                        ------------------------------------------------------>
                        <tr>
                          <td rowspan="2" width="60" bgcolor="#DFFFDC" nowrap>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告者</font>
                          </td>
                          <td width="60" bgcolor="#DFFFDC" nowrap>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告部署</font>
                          </td>
                          <td colspan="3"  bgcolor="#FFFFFF" nowrap>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                              <select id="search_emp_class" name="search_emp_class" onchange="setAtrbOptions('search_emp_post');set_disabled_enabled();"></select>
                              &nbsp;&gt;&nbsp;<select id="search_emp_attribute" name="search_emp_attribute" onchange="setDeptOptions('search_emp_post');"></select>
                              &nbsp;&gt;&nbsp;<select id="search_emp_dept" name="search_emp_dept" onchange="setRoomOptions('search_emp_post');"></select>
                              <{if $arr_class_name.class_cnt == 4}>
                                &nbsp;&gt;&nbsp;<select id="search_emp_room" name="search_emp_room"></select>
                              <{/if}>
                            </font>
                          </td>
                        </tr>
                        <tr>
                          <td width="70" bgcolor="#DFFFDC" nowrap>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名</font>
                          </td>
                          <td width="150" bgcolor="#FFFFFF">
                            <input type="text" name="search_emp_name" id="search_emp_name" size="40" value="<{$search_emp_name|escape}>" maxlength="40" style="ime-mode:active" onkeyup="set_disabled_enabled();">
                          </td>
                          <td width="60" bgcolor="#DFFFDC" nowrap>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font>
                          </td>
                          <td width="*" bgcolor="#FFFFFF">
                            <select name="search_emp_job" id="search_emp_job" onchange="set_disabled_enabled();">
                              <option value=""></option>
                              <{foreach from=$job_list key=job_id item=job_name}>
                                <option value="<{$job_id}>" <{if $job_id == $search_emp_job}>selected<{/if}>>
                                  <{$job_name}>
                                </option>
                              <{/foreach}>
                            </select>
                          </td>
                        </tr>
                
                        <!-----------------------------------------------------
                        患者
                        ------------------------------------------------------>
                        <tr>
                          <td rowspan="2" bgcolor="#DFFFDC" nowrap>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者</font>
                          </td>

                          <td bgcolor="#DFFFDC" nowrap>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ＩＤ</font>
                          </td>
                          <td bgcolor="#FFFFFF">
                            <input type="text" name="patient_id" size="20" id="patient_id" value="<{$patient_id|escape}>" onkeyup="set_disabled_enabled();" maxlength="30">
                          </td>
                          <td bgcolor="#DFFFDC" nowrap>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者年齢</font>
                          </td>
                          <td bgcolor="#FFFFFF" nowrap>
                          <select name="patient_year_from" id="patient_year_from" onchange="check_patient_year('from');"  onchange="set_disabled_enabled();">
                            <option value=""></option>
                            <{foreach from=$patient_year key=k item=year}>
                              <option value="<{$k}>" <{if $k == $patient_year_from}>selected<{/if}>>
                                <{$year}>
                              </option>
                            <{/foreach}>
                          </select>
                          〜
                          <select name="patient_year_to" id="patient_year_to" onchange="check_patient_year('to');" onchange="set_disabled_enabled();">
                            <option value="">
                            <{foreach from=$patient_year key=k item=year}>
                              <option value="<{$k}>" <{if $k == $patient_year_to}>selected<{/if}>>
                                <{$year}>
                              </option>
                            <{/foreach}>
                          </select>
                          </td>
                        </tr>
                        
                        <tr>
                          <td bgcolor="#DFFFDC" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名</font></td>
                          <td colspan="3" width="*" bgcolor="#FFFFFF">
                            <input type="text" name="patient_name" size="40" id="patient_name" value="<{$patient_name|escape}>" maxlength="40" style="ime-mode:active" onkeyup="set_disabled_enabled();"></td>
                        </tr>
              
                        <!-----------------------------------------------------
                        キーワード
                        ------------------------------------------------------>
                        <tr>
                          <td bgcolor="#DFFFDC" colspan="2">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題へのキーワード</font>
                          </td>
                          <td bgcolor="#FFFFFF" colspan="3">
                            <input type="text" name="title_keyword" size="50" maxlength="50" value="<{$title_keyword}>" onkeyup="set_disabled_enabled();">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">空白で区切られたキーワードを<input type="radio" name="title_and_or_div" value="1" <{if $title_and_or_div == 1}>checked<{/if}>>AND<input type="radio" name="title_and_or_div" value="2" <{if $title_and_or_div == 2 || $title_and_or_div == ""}>checked<{/if}>>OR検索</font>
                          </td>
                        </tr>
                        <tr>
                          <td bgcolor="#DFFFDC" colspan="2">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">インシデントの内容<BR>へのキーワード</font>
                          </td>
                          <td bgcolor="#FFFFFF" colspan="3">
                            <input type="text" name="incident_contents_keyword" size="50" maxlength="50" value="<{$incident_contents_keyword}>" onkeyup="set_disabled_enabled();">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">空白で区切られたキーワードを<input type="radio" name="incident_and_or_div" value="1" <{ if $incident_and_or_div == 1}>checked<{/if}>>AND<input type="radio" name="incident_and_or_div" value="2" <{if $incident_and_or_div == 2 || $incident_and_or_div == ""}>checked<{/if}>>OR検索</font>
                          </td>
                        </tr>
                        
                        <!-----------------------------------------------------
                        未入力チェック
                        ------------------------------------------------------>
                        <tr>
                          <td bgcolor="#DFFFDC" colspan="2">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">未入力チェック</font>
                          </td>
                          <td bgcolor="#FFFFFF" colspan="3">
                            <table border="0" cellspacing="0" cellpadding="1" class="block_in">
                              <tr>
                                <td width="200">
                                  <input type="hidden" name="inci_level_regist_flg" value="t">
                                  <input type="checkbox" name="inci_level_regist_flg" value="f" onclick="set_disabled_enabled();"<{if $inci_level_regist_flg == "f"}>checked<{/if}>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者影響レベル</font>
                                </td>
                                <td width="200">
                                  <input type="hidden" name="happened_cause_regist_flg" value="t">
                                  <input type="checkbox" name="happened_cause_regist_flg" value="f" onclick="set_disabled_enabled();"<{if $happened_cause_regist_flg == "f"}>checked<{/if}>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生要因</font>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <input type="hidden" name="inci_background_regist_flg" value="t">
                                  <input type="checkbox" name="inci_background_regist_flg" value="f" onclick="set_disabled_enabled();"<{if $inci_background_regist_flg == "f"}>checked<{/if}>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">インシデントの背景・要因</font>
                                </td>
                                <td>
                                  <input type="hidden" name="improve_plan_regist_flg" value="t">
                                  <input type="checkbox" name="improve_plan_regist_flg" value="f" onclick="set_disabled_enabled();"<{if $improve_plan_regist_flg == "f"}>checked<{/if}>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">改善策</font>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <input type="hidden" name="effect_confirm_regist_flg" value="t">
                                  <input type="checkbox" name="effect_confirm_regist_flg" value="f" onclick="set_disabled_enabled();"<{if $effect_confirm_regist_flg == "f"}>checked<{/if}>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">効果の確認</font>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        
                        <!-----------------------------------------------------
                        評価予定日
                        ------------------------------------------------------>
                        <tr>
                          <td bgcolor="#DFFFDC" colspan="2">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価予定日</font>
                          </td>
                          <td bgcolor="#FFFFFF" colspan="3">
                            <table border="0" cellspacing="0" cellpadding="1" class="block_in">
                              <tr>
                                <td width="100%">
                                  <select id="evaluation_date" name="evaluation_date" onchange="set_disabled_enabled();">
                                    <option value="">
                                    <option value="today"    <{if $evaluation_date == "today"   }>selected<{/if}> >本日
                                    <option value="tomorrow" <{if $evaluation_date == "tomorrow"}>selected<{/if}> >翌日
                                    <option value="1week"    <{if $evaluation_date == "1week"   }>selected<{/if}> >1週間以内
                                    <option value="2week"    <{if $evaluation_date == "2week"   }>selected<{/if}> >2週間以内
                                  </select>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        
                        <!-----------------------------------------------------
                        事例分析検討対象
                        ------------------------------------------------------>
                        <tr>
                          <td bgcolor="#DFFFDC" colspan="2">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事例分析検討対象</font>
                          </td>
                          <td bgcolor="#FFFFFF" colspan="3">
                            <table border="0" cellspacing="0" cellpadding="1" class="block_in">
                              <tr>
                                <td width="100%">
                                  <input type="hidden" name="analysis_target_flg" value="f">
                                  <input type="checkbox" name="analysis_target_flg" value="t" onclick="set_disabled_enabled();"<{if $analysis_target_flg == "t"}>checked<{/if}>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事例分析検討対象としての登録あり</font>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        
                        <!-----------------------------------------------------
                        警鐘事例
                        ------------------------------------------------------>
                        <tr>
                          <td bgcolor="#DFFFDC" colspan="2">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">警鐘事例</font>
                          </td>
                          <td bgcolor="#FFFFFF" colspan="3">
                            <table border="0" cellspacing="0" cellpadding="1" class="block_in">
                              <tr>
                                <td width="200">
                                  <input type="hidden" name="warning_case_regist_flg" value="f">
                                  <input type="checkbox" name="warning_case_regist_flg" value="t" onclick="set_disabled_enabled();" <{if $warning_case_regist_flg == "t"}>checked<{/if}>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">警鐘事例としての登録あり</font>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>

                        <!-----------------------------------------------------
                        患者影響レベル
                        ------------------------------------------------------>
                        <tr>
                          <td bgcolor="#DFFFDC" colspan="2">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者影響レベル</font>
                          </td>
                          <td bgcolor="#FFFFFF" colspan="3">
                            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                              <{foreach from=$level item=row}>
                                <tr>
                                  <{foreach from=$row key=code item=info}>
                                    <td width="25%">
                                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                        <nobr>
                                          <input type="checkbox" name="use_level[]" value="<{$code}>" <{if $info.is_checked}>checked<{/if}> onclick="set_disabled_enabled();"><{$info.name|escape}>
                                        </nobr>
                                      </font>
                                    </td>
                                  <{/foreach}>
                                </tr>
                              <{/foreach}>
                            </table>
                          </td>
                        </tr>

                        <!-----------------------------------------------------
                        ヒヤリ・ハット分類
                        ------------------------------------------------------>
                        <tr>
                          <td bgcolor="#DFFFDC" colspan="2">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">分類</font>
                          </td>
                          <td bgcolor="#FFFFFF" colspan="3">
                            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                              <{foreach name=hiyari from=$hiyari_list key=code item=i}>
                                <{if $smarty.foreach.hiyari.index % 4 == 0}>
                                  <tr>
                                <{/if}>
                                  <td width="25%">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <nobr>
                                      <input type="checkbox" name="hiyari_summary[]" value="<{$code}>" <{if $i.is_checked}>checked<{/if}> onclick="set_disabled_enabled();"><{$i.name|escape}>
                                      </nobr>
                                    </font>
                                  </td>
                                <{if $smarty.foreach.hiyari.index % 4 == 3 || $smarty.foreach.hiyari.last}>
                                  </tr>
                                <{/if}>
                              <{/foreach}>
                            </table>
                          </td>
                        </tr>
                        
                        <!-----------------------------------------------------
                        インシデントの概要
                        ------------------------------------------------------>
                        <tr>
                          <td bgcolor="#DFFFDC" colspan="2">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">インシデントの概要</font>
                          </td>
                          <td bgcolor="#FFFFFF" colspan="3">
                            <{foreach name=gaiyo from=$gaiyo_list key=code_120 item=item_120}>
                              <table width="100%" border="0" cellspacing="0" class="list">
                                <tr>
                                  <td bgcolor="#FFFBE7" colspan="2" <{if $smarty.foreach.gaiyo.index > 0}>style="border-top:0px;"<{/if}>>
                                    <table width="100%" border="0" cellspacing="0" class="block_in">
                                      <tr>
                                        <td bgcolor="#FFFBE7" width="150" nowrap>
                                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <input type="checkbox" id="use_120_<{$item_120.item_list_400_index}>" name="use_120[]" value="<{$code_120}>" <{if $item_120.is_checked}>checked<{/if}> onclick="disp_control_120_400();set_disabled_enabled();" ><{$item_120.name}>
                                          </font>
                                        </td>
                                        <td bgcolor="#FFFBE7">
                                          <div id="area_400_use_<{$item_120.item_list_400_index}>" style="display:none">
                                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" nowrap>
                                            （
                                            <input type="checkbox" id="use_400_bamen_<{$item_120.item_list_400_index}>" name="use_400_bamen[]" value="<{$item_120.item_list_400_index}>" <{if $item_120.is_bamen_checked}>checked<{/if}> onclick="disp_control_120_400();">場面で絞り込む
                                            &nbsp;&nbsp;
                                            <input type="checkbox" id="use_400_naiyo_<{$item_120.item_list_400_index}>" name="use_400_naiyo[]" value="<{$item_120.item_list_400_index}>" <{if $item_120.is_naiyo_checked}>checked<{/if}> onclick="disp_control_120_400();">内容で絞り込む
                                            ）
                                            </font>
                                          </div>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>

                              <div id="area_400_<{$item_120.item_list_400_index}>" style="display:none">
                                <table width="100%" border="0" cellspacing="0" class="list">
                                  <tr>
                                    <td bgcolor="#FFFBE7" colspan="2" style="border-top:0px;">
                                      <table width="100%" border="0" cellspacing="0" class="list">
                                        <tr>
                                          <td bgcolor="#FFFBE7" colspan="2" style="height:0px;border-top:0px;border-left:0px;border-right:0px;"></td>
                                        </tr>
                                      </table>

                                      <{foreach from=$item_120.detail key=detail_type item=detail}>
                                        <div id="area_400_<{$detail_type}>_<{$item_120.item_list_400_index}>" style="display:none">
                                          <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                                            <tr>
                                              <td bgcolor="#DFFFDC" align="center"style="border-top:0px;">
                                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                  <{$detail.title}>
                                                </font>
                                              </td>
                                              <td bgcolor="#FFFFFF" width="100%" style="border-top:0px;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                                                  <{foreach name=list from=$detail.list key=code item=i}>
                                                    <{if $smarty.foreach.list.index % 4 == 0}>
                                                      <tr>
                                                    <{/if}>
                                                      <td width="25%">
                                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                        <nobr>
                                                        <input type="checkbox" name="<{$i.obj_name}>[]" value="<{$code}>" <{if $i.is_checked}>checked<{/if}> onclick="set_disabled_enabled();"><{$i.name|escape}>
                                                        </nobr>
                                                        </font>
                                                      </td>
                                                    <{if $smarty.foreach.list.index % 4 == 3 || $smarty.foreach.list.last}>
                                                      </tr>
                                                    <{/if}>
                                                  <{/foreach}>
                                                </table>
                                              </td>
                                            </tr>
                                          </table>
                                        </div>
                                      <{/foreach}>
                                    </td>
                                  </tr>
                                </table>
                              </div>
                            <{/foreach}>  
                          </td>
                        </tr>
                          
                        <!-----------------------------------------------------
                        概要・場面・内容(2010年改訂)                        
                        ------------------------------------------------------>
                        <tr>
                          <td bgcolor="#DFFFDC" colspan="2">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">概要・場面・内容<br>(2010年改訂)</font>
                          </td>
                          <td bgcolor="#FFFFFF" colspan="3">
                            <{foreach name=gaiyo2010 from=$gaiyo2010_list key=code_900 item=item_900}>
                              <table width="100%" border="0" cellspacing="0" class="list">
                                <tr>
                                  <td bgcolor="#FFFBE7" colspan="2" <{if !$smarty.foreach.gaiyo2010.first}>style="border-top:0px;"<{/if}>>
                                    <table width="100%" border="0" cellspacing="0" class="block_in">
                                      <tr>
                                        <td bgcolor="#FFFBE7" width="150" nowrap>
                                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <input type="checkbox" id="use_900_<{$item_900.item_list_1000_index}>" name="use_900[]" value="<{$code_900}>" <{if $item_900.is_checked}>checked<{/if}> onclick="disp_control_900_1000();set_disabled_enabled();" ><{$item_900.name|escape}>
                                          </font>
                                        </td>
                                        <td bgcolor="#FFFBE7">
                                          <div id="area_1000_use_<{$item_900.item_list_1000_index}>" style="display:none">
                                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" nowrap>
                                              （
                                              <input type="checkbox" id="use_1000_kind_<{$item_900.item_list_1000_index}>" name="use_1000_kind[]" value="<{$item_900.item_list_1000_index}>" <{if $item_900.is_kind_checked}>checked<{/if}> onclick="disp_control_900_1000();">種類で絞り込む
                                              &nbsp;&nbsp;
                                              <input type="checkbox" id="use_1000_scene_<{$item_900.item_list_1000_index}>" name="use_1000_scene[]" value="<{$item_900.item_list_1000_index}>" <{if $item_900.is_scene_checked}>checked<{/if}> onclick="disp_control_900_1000();">場面で絞り込む
                                              &nbsp;&nbsp;
                                              <input type="checkbox" id="use_1000_content_<{$item_900.item_list_1000_index}>" name="use_1000_content[]" value="<{$item_900.item_list_1000_index}>" <{if $item_900.is_content_checked}>checked<{/if}> onclick="disp_control_900_1000();">内容で絞り込む
                                              ）
                                            </font>
                                          </div>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>

                              <div id="area_1000_<{$item_900.item_list_1000_index}>" style="display:none">
                                <table width="100%" border="0" cellspacing="0" class="list">
                                  <tr>
                                    <td bgcolor="#FFFBE7" colspan="2" style="border-top:0px;">
                                      <table width="100%" border="0" cellspacing="0" class="list">
                                        <tr>
                                          <td bgcolor="#FFFBE7" colspan="2" style="height:0px;border-top:0px;border-left:0px;border-right:0px;"></td>
                                        </tr>
                                      </table>
                                      
                                      <{foreach from=$item_900.sub key=sub_type item=sub}>
                                        <div id="area_1000_<{$sub_type}>_<{$item_900.item_list_1000_index}>" style="display:none">
                                          <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                                            <tr>
                                              <td bgcolor="#DFFFDC" align="center"style="border-top:0px;">
                                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                  <{$sub.title}>
                                                </font>
                                              </td>
                                              <td bgcolor="#FFFFFF" width="100%" style="border-top:0px;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                                                  <{foreach name=list from=$sub.list key=code item=i}>
                                                    <{if $smarty.foreach.list.index % 4 == 0}>
                                                      <tr>
                                                    <{/if}>
                                                      <td width="25%">
                                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                          <nobr>
                                                          <input type="checkbox" id="id_c_<{$code}>" name="<{$i.obj_name}>[]" value="<{$code}>" <{if $i.is_checked}>checked<{/if}> onclick="set_disabled_enabled();"><{$i.name|escape}>
                                                          （<input type="checkbox" id="id_n_<{$code}>" name="use_1000_detail_<{$sub_type}>[]" value="<{$code}>" <{if $i.is_detail_checked}>checked<{/if}> onClick="check_appear_item(document.getElementById('id_n_<{$code}>'), <{$code}>, '<{$sub_type}>', '<{$item_900.item_list_1000_index}>')">絞り込み）
                                                          </nobr>
                                                        </font>
                                                      </td>
                                                    <{if $smarty.foreach.list.index % 4 == 3 || $smarty.foreach.list.last}>
                                                      </tr>
                                                    <{/if}>
                                                  <{/foreach}>
                                                </table>
                                              </td>
                                            </tr>
                                          </table>
                                        </div>

                                        <{foreach name=list from=$sub.list key=code item=i}>
                                          <div id="area_1000_<{$sub_type}>_detail_<{$code}>" style="display:none">
                                            <input type="hidden" name="area_1000_<{$sub_type}>_detail_list_<{$item_900.item_list_1000_index}>[]" value="<{$code}>">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                                              <tr>
                                                <td bgcolor="#DFFFDC" align="center"style="border-top:0px;">
                                                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                    項<br>目
                                                  </font>
                                                </td>
                                                <td bgcolor="#FFFFFF" width="100%" style="border-top:0px;">
                                                  <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                                                    <{foreach name=detail from=$i.detail_list key=detail_code item=d}>
                                                      <{if $smarty.foreach.detail.index % 4 == 0}>
                                                        <tr>
                                                      <{/if}>
                                                        <td width="25%">
                                                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                          <nobr>
                                                          <input type="checkbox" name="<{$d.obj_name}>[]" value="<{$detail_code}>" <{if $d.is_checked}>checked<{/if}> onclick="set_disabled_enabled();"><{$d.name|escape}>
                                                          </nobr>
                                                          </font>
                                                        </td>
                                                      <{if $smarty.foreach.detail.index % 4 == 3 || $smarty.foreach.detail.last}>
                                                        </tr>
                                                      <{/if}>
                                                    <{/foreach}>
                                                  </table>
                                                </td>
                                              </tr>
                                            </table>
                                          </div>
                                        <{/foreach}>
                                      <{/foreach}>
                                    </td>
                                  </tr>
                                </table>
                              </div>
                            <{/foreach}>
                          </td>
                        </tr>
                        
                        <!-----------------------------------------------------
                        AND_OR条件
                        ------------------------------------------------------>
                        <tr>
                          <td bgcolor="#DFFFDC" colspan="2">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">AND_OR条件</font>
                          </td>
                          <td bgcolor="#FFFFFF" colspan="3">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                              <input type="radio" name="and_or_div" value="0" <{if $and_or_div == "0"}>checked<{/if}>>AND
                              <input type="radio" name="and_or_div" value="1" <{if $and_or_div == "1"}>checked<{/if}>>OR
                            </font>
                          </td>
                        </tr>
                      </table>
                    </td>
                    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                  </tr>
                  <tr>
                    <td><img src="img/r_3.gif" width="10" height="10"></td>
                    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                    <td><img src="img/r_4.gif" width="10" height="10"></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td align="right">
                      <input type="submit" value="<{$SUBMIT_BUTTON_TITLE}>">
                    </td>
                  </tr>
                </table>
              </td>
              <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
            </tr>
            <tr>
              <td><img src="img/r_3.gif" width="10" height="10"></td>
              <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
              <td><img src="img/r_4.gif" width="10" height="10"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </form>
</body>
</html>
