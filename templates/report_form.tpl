<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/smoothness/jquery-ui.custom.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <script type="text/javascript">
    var w = 1024 + 37;
    var h = window.screen.availHeight;
    window.resizeTo(w, h);
  </script>
  <script type="text/javascript" src="js/jquery/jquery-1.7.2.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
  <script type="text/javascript" src="js/report_form.js"></script>
  <script type="text/javascript" src="js/jquery/jquery-ui-1.10.2.custom.min.js"></script>
  <script type="text/javascript" src="js/jquery/jquery.ui.datepicker-ja.min.js" charset="utf-8"></script>
  <script type="text/javascript" src="js/jquery/jquery.bgiframe.js"></script>
  <script type="text/javascript">
    //====================================================================================================
    //起動時の処理
    //====================================================================================================
    $(function(){
      <{if !$readonly_flg && $eis_select_able}>
        set_eis_input_list();
        set_eis_no(<{$sel_eis_no}>);
      <{/if}>

      <{if $auto_reload}>
        start_auto_session_update();
      <{/if}>

	  <{if $gamen_mode == 'analysis' }>
	    if(window.opener && !window.opener.closed && window.opener.reload_page) {
		  window.opener.reload_page();
        }
	  <{/if}>      
    });
    
    //変更があったら残り文字数を表示
    $(document).on("change keyup",".form-group textarea", function () {
        var count, $parent, maxLength, $target;

        $parent = $(this).parent();
        $target = $("p.counter", $parent);
   
        count = $(this).val().length;
        maxLength = $(this).attr('maxlength');
        if (isFinite(maxLength)) {
            $("a.count", $target).text(parseInt(maxLength) - count);
        }
        //console.log(count);
    
    });



    //====================================================================================================
    //実行アイコン
    //====================================================================================================
    //下書き
    function rp_action_draft()
    {
      if (!chkShitagakiSubmit()) return;

      document.FRM_MAIN.shitagaki.value='clicked';
      document.FRM_MAIN.submit();
    }
    //一時保存
    function rp_action_temporary()
    {
      if (!chkShitagakiSubmit()) return;

      document.FRM_MAIN.report_update.value='clicked';
      document.FRM_MAIN.submit();
    }
 	// 確認
    function set_progress_end_date()
    {
	    location.href = "hiyari_rp_mail_disp.php?session=<{$session}>&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>&mode=update&callerpage=<{$callerpage}>";
    }
    //評価
    function rp_action_analysis(){
        var job_id = $('#sel_job_input').val();
        var eis_no = $('#sel_eis_input').val();
        location.href = "hiyari_easyinput.php?session=<{$session}>&gamen_mode=analysis&report_id=<{$report_id}>&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>&callerpage=<{$callerpage}>" + '&job_id=' + job_id + '&eis_no=' + eis_no;
    }
    //編集
    function rp_action_update()
    {
        location.href = "hiyari_easyinput.php?session=<{$session}>&gamen_mode=update&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>&report_id=<{$report_id}>&callerpage=<{$callerpage}>";
    }
    
    // 転送
    function rp_action_forward()
    {
        location.href = "hiyari_rp_mail_input.php?session=<{$session}>&mode=forward&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>";
    }
    
    // メッセージ表示
    function rp_action_message_disp()
    {
      location.href = "hiyari_rp_mail_disp.php?session=<{$session}>&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>";
    }
    //進捗登録
    function rp_action_progress()
    {
      location.href = "hiyari_rp_progress.php?session=<{$session}>&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>&report_id=<{$report_id}>";
    }
    //印刷
    function rp_action_print()
    {
      <{if $patient_non_print_flg}>
        var patient_non_print_flg = true;
      <{else}>
        var patient_non_print_flg = false;
      <{/if}>

      <{if $patient_non_print_select_flg}>
        patient_non_print_flg = !confirm("患者ID、氏名の印刷を有効にしますか？");
      <{/if}>

      var param_patient_non_print = "false";
      if(patient_non_print_flg){
          param_patient_non_print = "true";
      }

      <{if $gamen_mode == "analysis"}>
        var url = "hiyari_report_print.php?session=<{$session}>&report_id=<{$report_id}>&patient_non_print=" + param_patient_non_print;
      <{elseif $gamen_mode == "update"}>
        <{if $sel_job != ""}>//指定様式の時
            var url = "hiyari_report_print.php?session=<{$session}>&report_id=<{$report_id}>&eis_id=<{$mail_eis_id}>&gamen_mode=<{$gamen_mode}>&patient_non_print=" + param_patient_non_print;
        <{else}>//全項目のとき
            var url = "hiyari_report_print.php?session=<{$session}>&report_id=<{$report_id}>&eis_id=&gamen_mode=<{$gamen_mode}>&patient_non_print=" + param_patient_non_print;
        <{/if}>
      <{else}>
      
        var url = "hiyari_report_print.php?session=<{$session}>&report_id=<{$report_id}>&eis_id=<{$mail_eis_id}>&patient_non_print=" + param_patient_non_print;
      <{/if}>

      window.open(url);
    }

    //過去の提出報告書一覧リスト
    function rp_action_past_report()
    {
        location.href = "hiyari_pastreport_list.php?session=<{$session}>&gamen_mode=update&registrant_id=<{$registrant_id}>&report_id=<{$report_id}>&mail_id=<{$mail_id}>&callerpage=<{$callerpage}>";
  
    }
    //====================================================================================================
    //評価画面用
    //====================================================================================================
    function show_report_contents_in_another_window()
    {
      var childwin = null;

      var w = 1024 + 37;
      var h = window.screen.availHeight;
      var l = screen.width - w;

//      var url = "hiyari_easyinput.php?session=<{$session}>&gamen_mode=analysis&report_id=<{$report_id}>&report_contents=1";
      var url = "hiyari_easyinput.php?session=<{$session}>&gamen_mode=analysis&non_update_flg=true&report_id=<{$report_id}>&report_contents=1&job_id=<{$sel_job}>&eis_no=<{$sel_eis_no}>";
      childwin = window.open(url, null, 'left='+l+',top=0,width='+w+',height='+h+',scrollbars=yes,resizable=yes');
      childwin.focus();

      $('#window_switch_btn1').hide();
      $('#report_contents_for_analysis').hide();
    }

    function show_report_contents_in_this_window()
    {
      $('#window_switch_btn1').show();
      $('#report_contents_for_analysis').show();
    }

  	//公開
    function rp_action_open()
    {
    	var eis_no = $('#sel_eis_input').val();
        location.href = "hiyari_rp_open_input.php?session=<{$session}>&mode=open_report&report_id=<{$report_id}>&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>&eis_no=" + eis_no;
    }

  </script>
</head>

<body id="top" class="houkoku">
  <script type="text/javascript">
    var all_contents = document.getElementById("all_contents");
  </script>
  <a id="pagetop" name="pagetop"></a>

  <!-- セッションタイムアウト防止 START -->
  <{if $auto_reload}>
    <script type="text/javascript">
function start_auto_session_update()
    {
        //1分(60*1000ms)後に開始
        setTimeout(auto_sesson_update,60000);
    }
    function auto_sesson_update()
    {
        //セッション更新
        document.session_update_form.submit();

        //1分(60*1000ms)後に最呼び出し
        setTimeout(auto_sesson_update,60000);
    }
    </script>
    <form name="session_update_form" action="hiyari_session_update.php" method="post" target="session_update_frame">
      <input type="hidden" name="session" value="<{$session}>">
    </form>
    <iframe name="session_update_frame" width="0" height="0" frameborder="0"></iframe>
  <{/if}>
  <!-- セッションタイムアウト防止 END -->

  <!-- ヘッダー START -->
	<div id="header">
		<div class="inner">
			<h1><{$PAGE_TITLE}></h1>
      <!-- 実行アイコン START -->
      <ul class="sub_nav clearfix">
        <{if $gamen_mode == "update"}>
      <!--	<li class="sub_nav_edit"><a href="javascript:rp_action_open();">編集</a></li>-->
      	<{/if}>
        <{if !$readonly_flg}>
          <{if $gamen_mode == "new" || $gamen_mode == "shitagaki"}>
            <li class="sub_nav_draft"><a href="javascript:rp_action_draft();">下書き保存</a></li>
          <{/if}>

        <{if $gamen_mode == "analysis"}>
          <li class="sub_nav_temporary"><a href="javascript:rp_action_temporary();">一時保存</a></li>
        <{/if}>

        <{if $gamen_mode != "cate_update"}>
          <li class="sub_nav_next"><a href="javascript:if(chkSubmit()){document.FRM_MAIN.next.value='clicked';document.FRM_MAIN.submit();};">内容確認</a></li>
        <{/if}>
        <{/if}>
		
		<{if $report_progress_updatable_flg && $gamen_mode != "cate_update"}>
		  <li class="sub_nav_confirm">
            <a href="javascript:set_progress_end_date();">確認</a>
          </li>
        <{elseif $is_evaluation_use && $gamen_mode != "new" && $gamen_mode != "shitagaki" && $gamen_mode != "analysis" && $gamen_mode != "cate_update"}>
          <li class="sub_nav_analysis">
            <a href="javascript:rp_action_analysis();">評価</a>
          </li>
        <{/if}>

        <{if $gamen_mode == "analysis" && !$readonly_flg}>
          <li class="sub_nav_edit"><a href="javascript:rp_action_update();">編集</a></li>
        <{/if}>

        
              
        
        
        <{if $gamen_mode == "cate_update"}>
          <li class="sub_nav_update"><a href="javascript:if(chkSubmit()){document.FRM_MAIN.report_update.value='clicked';document.FRM_MAIN.submit();};">更新</a></li>
        <{/if}>
        
        <{if $callerpage != "hiyari_rpt_report_classification.php" && $gamen_mode != "new"}>
          <{if $mail_id != ""}>
            <li class="sub_nav_return"><a href="javascript:rp_action_message_disp();">戻る</a></li>
            <{/if}>
        <{/if}>

        <{if $progres_edit_flg}>
          <li class="sub_nav_progress"><a href="javascript:rp_action_progress();">進捗登録</a><li>
        <{/if}>

        <{if $label_use_flg && $gamen_mode != "new" && $gamen_mode != "shitagaki"}>
          <li id="label_header" class="sub_nav_label"><a href="javascript:rp_action_label(true);">ラベル登録</a></li>
        <{/if}>

        <{if $gamen_mode == "update" || $gamen_mode == "analysis" || $gamen_mode == "shitagaki"}>
          <{if $print_btn_show_flg}>
            <li class="sub_nav_print"><a href="javascript:rp_action_print();">印刷</a></li>
          <{/if}>
        <{/if}>
        
        <{if $gamen_mode == "update" && $list_btn_show_flg=="TRUE" && ($callerpage =="hiyari_rpt_report_classification.php"
        ||$callerpage == "hiyari_pastreport_list.php" ||$callerpage == "from_hiyari_rpt_report_classification.php"
        ||$callerpage == "hiyari_easyinput.php" )}>
            <li class="sub_nav_samereport"><a href="javascript:rp_action_past_report();">同一報告者</a></li>
        <{/if}>
        <{if $forward_btn_show_flg && $gamen_mode == "update"}>
        <li class="sub_nav_forward"><a href="javascript:rp_action_forward();">転送</a></li>
         <{/if}>
      </ul>
      <!-- 実行アイコン END -->
		</div>
	</div>
  <!-- ヘッダー END -->

  <div id="content" class="clearfix">
    <{if !$readonly_flg}>
      <div class="section">
        <!-- 画面進捗 -->
        <div id="flow" class="radius_mini ie_radius_mini mB30">
          <ul class="clearfix">
            <li class="flow_list01">出来事報告の流れ</li>
            <li class="flow_list02 active"><span class="wrap"><span class="number_bg">●</span><span class="number">1</span>出来事の記入</span></li>
            <{if $gamen_mode == "cate_update"}>
              <li class="flow_list04"><span class="wrap"><span class="number_bg">●</span><span class="number">2</span>更新</span></li>
            <{else}>
              <li class="flow_list03"><span class="wrap"><span class="number_bg">●</span><span class="number">2</span>記入内容を確認</span></li>
              <li class="flow_list04"><span class="wrap"><span class="number_bg">●</span><span class="number">3</span>送信</span></li>
            <{/if}>
          </ul>
        </div>

        <!-- 職種選択 START -->
        <{if $eis_select_able}>
          <div class="color01 radius mB20 clearfix">
            <form name='job_change_form' action='' method='post' onSubmit="return false;">
              <table>
                <tr>
                  <th>● 対象職種・報告書様式</th>
                </tr>
                <tr>
                  <td>
                    <select class="size01 mL0" id="sel_job_input" name="sel_job_input" onchange="set_eis_input_list();change_job();">
                      <{section loop=$job_ids name=i}>
                        <option value="<{$job_ids[i]}>"<{if $job_ids[i] == $sel_job}> selected<{/if}>><{$job_names[i]}>
                      <{/section}>
                    </select>
                    <select class="size01" id="sel_eis_input" name="sel_eis_input" onchange="change_job();">
                    </select>
                  </td>
                </tr>
              </table>
            </form>
          </div>
          <script type="text/javascript">
            function change_job()
            {
              //document.FRM_MAIN.sel_job.value = sel_job_input.value;
              document.FRM_MAIN.sel_job.value = job_change_form.sel_job_input.options[job_change_form.sel_job_input.selectedIndex].value;
              document.FRM_MAIN.sel_eis_no.value = job_change_form.sel_eis_input.options[job_change_form.sel_eis_input.selectedIndex].value;
              document.FRM_MAIN.sel_eis_change.value='changed';
              document.FRM_MAIN.submit();
            }

            function set_eis_input_list()
            {
              var job_box = job_change_form.sel_job_input;//document.getElementById("sel_job_input");
              var eis_box = job_change_form.sel_eis_input;//document.getElementById("sel_eis_input");
              var job_id = job_box.options[job_box.selectedIndex].value;

              deleteAllOptions(eis_box);
              if(false){
              }
              <{section loop=$job_ids name=i}>
                else if(job_id == "<{$job_ids[i]}>") { //デフォルトの様式番号
                  <{section loop=$eis_job_link_info name=ii}>
                    <{if $eis_job_link_info[ii].job_id == $job_ids[i] && $eis_job_link_info[ii].default_flg == "t"}>
                      var default_eis_no = "<{$eis_job_link_info[ii].eis_no}>";
                    <{/if}>
                  <{/section}>
                  //様式をリストに追加
                  <{section loop=$eis_job_link_info name=ii}>
                    <{if $eis_job_link_info[ii].job_id == $job_ids[i]}>
                      addOption(eis_box, "<{$eis_job_link_info[ii].eis_no}>", "<{$eis_job_link_info[ii].eis_name}>", default_eis_no);
                    <{/if}>
                  <{/section}>
                }
              <{/section}>
              else{
              }
            }

            function deleteAllOptions(box)
            {
              for (var i = box.length - 1; i >= 0; i--)
              {
                box.options[i] = null;
              }
            }

            function addOption(box, value, text, selected)
            {
              var opt = document.createElement("option");
              opt.value = value;
              opt.text = text;
              if (selected == value)
              {
                  opt.selected = true;
              }
              box.options[box.length] = opt;
              try {box.style.fontSize = 'auto';} catch (e) {}
              box.style.overflow = 'auto';
            }

            function set_eis_no(eis_no)
            {
              var obj = job_change_form.sel_eis_input;//document.getElementById("sel_eis_input");
              set_select_ctl_from_value(obj,eis_no);
            }

            function set_select_ctl_from_value(obj,val)
            {
              var is_ok = false;
              for(var i=0; i<obj.options.length; i++)
              {
                if(obj.options[i].value == val)
                {
                  obj.selectedIndex = i;
                  is_ok = true;
                  break;
                }
              }
              return is_ok;
            }
          </script>
        <{/if}>
        <!-- 職種選択 END -->
      </div>
    <{else}>
        <input type='hidden' id='sel_job_input' value='<{$sel_job|escape}>'>
        <input type='hidden' id='sel_eis_input' value='<{$sel_eis_no|escape}>'>
    <{/if}>

    <form name='FRM_MAIN' action='hiyari_easyinput.php?callerpage=hiyari_easyinput.php' method='post' onSubmit="return false;" >
      <{* 確認画面で様式を取得するためにデータを送る START *}>
      <input type="hidden" name="eis_title_id" value="<{$eis_title_id}>">
      <{* 確認画面で様式を取得するためにデータを送る END *}>

      <!-- ポストバック情報 -->
      <input type='hidden' name='is_postback'         id='is_postback'         value='ture'>
      <input type='hidden' name='next'                id='next'                value=''>
      <input type='hidden' name='shitagaki'           id='shitagaki'           value=''>
      <input type='hidden' name='report_update'       id='report_update'       value=''>
      <input type='hidden' name='sel_eis_change'      id='sel_job_change'      value=''>
      <input type='hidden' name='registrant_post_change' id='registrant_post_change' value=''>
      <input type='hidden' name='subject_mode'            id='subject_mode'        value='<{$subject_mode}>'>
       <input type='hidden' name='callerpage'            id='callerpage'        value='<{$callerpage}>'>
          
     
      <!-- セッション情報 -->
      <input type='hidden' name='session'             id='session'             value='<{$session}>'>
      <input type='hidden' name='hyr_sid'             id='hyr_sid'             value='<{$hyr_sid}>'>

      <!-- 画面パラメータ -->
      <input type='hidden' name='gamen_mode'          id='gamen_mode'          value='<{$gamen_mode}>'>
      <input type='hidden' name='report_id'           id='report_id'           value='<{$report_id}>'>
      <input type='hidden' name='mail_id'             id='mail_id'             value='<{$mail_id}>'>
      <input type='hidden' name='mail_id_mode'        id='mail_id_mode'        value='<{$mail_id_mode}>'>
      <input type='hidden' name='cate'                id='cate'                value='<{$cate}>'>

      <!-- 職種／様式 (JavaScriptにより動的変更／ポストバック用) -->
      <input type='hidden' name='sel_job'             id='sel_job'             value='<{$sel_job}>'>
      <input type='hidden' name='sel_eis_no'          id='sel_eis_no'          value='<{$sel_eis_no}>'>

      <!-- 職種／様式 (JavaScriptによる変更なし／確認画面送信用) -->
      <input type='hidden' name='job_id'              id='job_id'              value='<{$sel_job}>'>
      <input type='hidden' name='eis_no'              id='eis_no'              value='<{$sel_eis_no}>'>

      <!-- 様式スタイル -->
      <input type='hidden' name='style_code'          id='style_code'         value='<{$style_code}>'>

      <!-- 特殊様式情報 -->
      <input type="hidden" name="patient_use" value="<{$patient_use_csv}>">
      <input type="hidden" name="eis_400_use" value="<{$eis_400_use_csv}>">
      <input type="hidden" name="eis_410_use" value="<{$eis_410_use_csv}>">
      <input type="hidden" name="eis_420_use" value="<{$eis_420_use_csv}>">
      <input type="hidden" name="eis_430_use" value="<{$eis_430_use_csv}>">
      <input type="hidden" name="eis_440_use" value="<{$eis_440_use_csv}>">
      <input type="hidden" name="eis_450_use" value="<{$eis_450_use_csv}>">
      <input type="hidden" name="eis_460_use" value="<{$eis_460_use_csv}>">
      <input type="hidden" name="eis_470_use" value="<{$eis_470_use_csv}>">
      <input type="hidden" name="eis_480_use" value="<{$eis_480_use_csv}>">
      <input type="hidden" name="eis_490_use" value="<{$eis_490_use_csv}>">

      <!-- タイムラグ -->
      <input type="hidden" name="first_send_time" value="<{$first_send_time}>">
      <input type="hidden" id="_105_75" name="_105_75" value="<{$vals[105][75][0]}>">

      <div class="section">
        <div class="clearfix">
          <!-- 報告部署 -->
          <ul id="pankuzu">
            <li <{if !$readonly_flg}>class="first"<{/if}>>
              <{if !$readonly_flg}>
                <a class="radius_mini" href="javascript:show_registrant_post_edit('<{$session}>');">
              <{/if}>
                報告部署
              <{if !$readonly_flg}>
                </a>
              <{/if}>
            </li>
            <li>
              <span id="registrant_class_name"><{$class_name}></span>
            </li>
            <li>
              <span id="registrant_attribute_name">
                <{if isset($attribute_name)}>
                  <{$attribute_name}>
                <{/if}>
              </span>
            </li>
            <li <{if !isset($room_name)}>class="last"<{/if}>>
              <span id="registrant_dept_name">
                <{if isset($dept_name)}>
                  <{$dept_name}>
                <{/if}>
              </span>
            </li>
            <li class="last">
              <span id="registrant_room_name">
                <{if isset($room_name)}>
                  <{$room_name}>
                <{/if}>
              </span>
            </li>
          </ul>
          <input type="hidden" id="registrant_class" name="registrant_class" value="<{$registrant_class}>">
          <input type="hidden" id="registrant_attribute" name="registrant_attribute" value="<{$registrant_attribute}>">
          <input type="hidden" id="registrant_dept" name="registrant_dept" value="<{$registrant_dept}>">
          <input type="hidden" id="registrant_room" name="registrant_room" value="<{$registrant_room}>">

          <!-- 報告内容を別ウィンドウで表示 -->
          <{if $gamen_mode == "analysis"}>
            <div id="window_switch_btn1">
              <a id="show_in_another_window_btn1" class="radius_mini" href="javascript:show_report_contents_in_another_window();">報告内容を別ウィンドウで表示</a>
            </div>
          <{/if}>
        </div>

        <!-- 事案番号 -->
        <{if $report_no != ""}>
          <div class="color01 radius mB40 clearfix">
            <table class="wrap_height">
              <tr>
                <th width="188">
                  事案番号
                </th>
                <td>
                  <span class="bold"><{$report_no}></span>
                </td>
              </tr>
            </table>
          </div>
        <{/if}>

        <!-- タイムラグ -->
        <{if $is_sm_emp_flg == true
         && $vals[105][75][0] != ''
         && $disp_taimelag_flg == true
         && $gamen_mode != 'cate_update'
         && $gamen_mode != 'shitagaki'
         && $first_send_time != ''
        }>
          <div class="color01 radius mB40 clearfix">
            <table class="wrap_height">
              <tr>
                <td width="506" class="lineR">
                  <span class="bold">第一報送信時刻　　<{$first_send_time}></span>
                </td>
                <td width="506" class="lineNone">
                  <span class="bold">発見時刻から第一報送信時刻までの時間<{if isset($is_no_timelag)}><br/><{else}>　　<{/if}><{$timelag_view}></span>
                </td>
              </tr>
            </table>
          </div>
        <{/if}>

        <{include file="report_form_main.tpl" input_mode=$gamen_mode}>



      </div>
    </form>

  <!-- ファイル添付機能 -->
    <{if  $file_attach == "1"}>
       <{include file="report_form_file.tpl" input_mode=$gamen_mode}>
    <{/if}>

  </div>

  <!-- iwata患者情報取得フォームSTART -->
  <form name='iwata_patient_search_form' action='' method='post'>
    <input type="hidden" id="caller"         name="caller">
    <input type="hidden" id="target_date"    name="target_date">
    <input type="hidden" id="patient_id"     name="patient_id">
    <input type="hidden" id="patient_name"   name="patient_name">
    <input type="hidden" id="page"           name="page">
    <input type="hidden" id="callbacker_url" name="callbacker_url">
  </form>
  <!-- iwata患者情報取得フォームEND -->

  <!-- 患者情報取得汎用フォームSTART -->
  <form name='patient_search_form' action='' method='post'>
    <input type="hidden" id="regist_date"    name="regist_date">
    <input type="hidden" id="caller"         name="caller">
    <input type="hidden" id="target_date"    name="target_date">
    <input type="hidden" id="patient_id"     name="patient_id">
    <input type="hidden" id="patient_name"   name="patient_name">
    <input type="hidden" id="page"           name="page">
    <input type="hidden" id="callbacker_url" name="callbacker_url">
  </form>
  <!-- 患者情報取得汎用フォームEND -->

  <!-- 事例の詳細（時系列）の事象用フォームSTART -->
  <form name='phenomenon_form' action='hiyari_easyinput_phenomenon.php' method='post'>
    <input type="hidden" name="session">
    <input type="hidden" name="grp_code">
    <input type="hidden" name="phenomenon">
  </form>
  <!-- 事例の詳細（時系列）の事象用フォームEND -->

  <!-- 実行アイコン２ START -->
  <div id="footer">
    <div class="inner">
      <!-- 実行アイコン START -->
      <ul class="sub_nav clearfix">
        <{if $gamen_mode == "update"}>
      	 <!-- <li class="sub_nav_edit"><a href="javascript:rp_action_open();">編集</a></li>-->
      	<{/if}>
        <{if !$readonly_flg}>
          <{if $gamen_mode == "new" || $gamen_mode == "shitagaki"}>
            <li class="sub_nav_draft"><a href="javascript:rp_action_draft();">下書き保存</a></li>
          <{/if}>

        <{if $gamen_mode == "analysis"}>
          <li class="sub_nav_temporary"><a href="javascript:rp_action_temporary();">一時保存</a></li>
        <{/if}>

          <{if $gamen_mode != "cate_update"}>
            <li class="sub_nav_next"><a href="javascript:if(chkSubmit()){document.FRM_MAIN.next.value='clicked';document.FRM_MAIN.submit();};">内容確認</a></li>
          <{/if}>
        <{/if}>
        
		<{if $report_progress_updatable_flg && $gamen_mode != "cate_update"}>
		  <li class="sub_nav_confirm">
            <a href="javascript:set_progress_end_date();">確認</a>
          </li>
        <{elseif $is_evaluation_use && $gamen_mode != "new" && $gamen_mode != "shitagaki" && $gamen_mode != "analysis" && $gamen_mode != "cate_update"}>
          <li class="sub_nav_analysis">
            <a href="javascript:rp_action_analysis();">評価</a>
          </li>
        <{/if}>


        <{if $gamen_mode == "analysis" && !$readonly_flg}>
          <li class="sub_nav_edit"><a href="javascript:rp_action_update();">編集</a></li>
        <{/if}>

        <{if $gamen_mode == "cate_update"}>
          <li class="sub_nav_update"><a href="javascript:if(chkSubmit()){document.FRM_MAIN.report_update.value='clicked';document.FRM_MAIN.submit();};">更新</a></li>
        <{/if}>

        <{if $callerpage != "hiyari_rpt_report_classification.php" &&  $gamen_mode != "new"}>
          <{if $mail_id != ""}>
            <li class="sub_nav_return"><a href="javascript:rp_action_message_disp();">戻る</a></li>
          <{/if}>
        <{/if}>

        <{if $progres_edit_flg}>
          <li class="sub_nav_progress"><a href="javascript:rp_action_progress();">進捗登録</a><li>
        <{/if}>

        <{if $label_use_flg && $gamen_mode != "new" && $gamen_mode != "shitagaki"}>
          <li id="label_footer" class="sub_nav_label"><a href="javascript:rp_action_label(false);">ラベル登録</a></li>
        <{/if}>

        <{if $gamen_mode == "update" || $gamen_mode == "analysis" || $gamen_mode == "shitagaki"}>
          <{if $print_btn_show_flg}>
            <li class="sub_nav_print"><a href="javascript:rp_action_print();">印刷</a></li>
          <{/if}>
        <{/if}>
        
        <{if $gamen_mode == "update" && $list_btn_show_flg=="TRUE" && ($callerpage =="hiyari_rpt_report_classification.php"
        ||$callerpage == "hiyari_pastreport_list.php" ||$callerpage == "from_hiyari_rpt_report_classification.php"
        ||$callerpage == "hiyari_easyinput.php" )}>
            <li class="sub_nav_samereport"><a href="javascript:rp_action_past_report();">同一報告者</a></li>
        <{/if}>
        
        <{if $forward_btn_show_flg && $gamen_mode == "update"}>
            <li class="sub_nav_forward"><a href="javascript:rp_action_forward();">転送</a></li>
        <{/if}>
        
      </ul>
      <!-- 実行アイコン END -->
    </div>
  </div>

  <!-- ラベル登録のフォームSTART -->
  <{if $label_use_flg && $report_id > 0}>
    <form id="label_form">
      <input type="hidden" name="session" value="<{$session}>">
      <input type="hidden" name="mode" value="label">
      <input type="hidden" name="report_id" value="<{$report_id}>">
      <div id="label" class="color01 radius">
        <div id="b_scrollbar">
          <div class="scrollbar"><div class="track radius"><div class="thumb radius"><div class="end"></div></div></div></div>
          <div class="viewport inner radius_mini">
            <div class="overview">
              <ul class="radius_mini">
                <{foreach from=$folders item=fld}>
                  <li class="<{if !$fld.is_common}>person<{/if}><{if $fld.is_selected}> checked<{/if}>">
                    <span class="checkbox_wrap <{if $fld.is_selected}>checked<{/if}>">
                      <input type="checkbox" class="checkbox" value="<{$fld.id}>" name="selected_folder[]" <{if $fld.is_selected}>checked="checked"<{/if}>>
                    </span>
                    <{$fld.name}>
                  </li>
                <{/foreach}>
              </ul>
            </div>
          </div>
        </div>
        <div>
          <input type="button" class="button radius_mini" value="登録" onclick="regist_label();">
        </div>
      </div>
    </form>
  <{/if}>
  <!-- ラベル登録のフォームEND -->
</body>
</html>
