<{*************************************************************************************************
報告書内容のデータ列
*************************************************************************************************}>
<{*<!--
---------------------------------------------------------------------------------------------------
発見・発生日時
---------------------------------------------------------------------------------------------------
-->*}>
<{if ($i==105 || $i==100) && $j==5}>
  <input type="text" id="id_<{$i}>_5" class="text_date" name="_<{$i}>_5" value="<{$vals[$i][5][0]}>" readonly>
  <input type="button" value="今日" onclick="set_<{$i}>_5_now()">

  <{if $i==105}>
    <img id="start_date_caller" class="calendar_btn" src="img/calendar_link.gif" onclick="window.open('hiyari_calendar_discover.php?session=<{$session}>', 'newwin', 'width=640,height=480,scrollbars=yes');"/>
  <{else}>
    <img id="start_date_caller" class="calendar_btn" src="img/calendar_link.gif" onclick="window.open('hiyari_calendar.php?session=<{$session}>', 'newwin', 'width=640,height=480,scrollbars=yes');"/>
  <{/if}>

  <input type="hidden" id="id_<{$i}>_10" name="_<{$i}>_10" value="<{$vals[$i][10][0]}>">
  <input type="hidden" id="id_<{$i}>_20" name="_<{$i}>_20" value="<{$vals[$i][20][0]}>">
  <input type="hidden" id="id_<{$i}>_30" name="_<{$i}>_30" value="<{$vals[$i][30][0]}>">

  <{*<!--タイムラグ区分-->*}>
  <{if $i==105}>
    <input type="hidden" id="id_105_70" name="_105_70" value="<{$vals[105][70][0]}>">
  <{/if}>

  <script type="text/javascript">
    function set_<{$i}>_5_now() {
      j$("#id_<{$i}>_5").val('<{$this_ymd}>');
      j$("#id_<{$i}>_10").val('<{$this_month}>');
      j$("#id_<{$i}>_20").val('<{$day_week}>');
      j$("#id_<{$i}>_30").val('<{$week_div}>');

      <{*<!--事例の詳細(時系列)があれば、一緒にセットする（1行目のみ & 日付が未設定の場合）-->*}>
      if (document.getElementById("id_4000_1")) {
        if (!document.getElementById("id_4000_1").value) {
          j$("#id_4000_1").val('<{$this_ymd}>');
        }
      }
    }
  </script>

<{*<!--
---------------------------------------------------------------------------------------------------
患者の数
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $i == 200 && $j == 10}>
  <{foreach from=$grps[$i].easy_item_list[$j].easy_list key=k item=data}>
    <{if $item_element_no_disp[$i][$j] == "" || !in_array($data.easy_code,$item_element_no_disp[$i][$j])}>
      <span class="item_set">
        <label>
          <input type='radio' id="id_<{$i}>_<{$j}>_<{$k}>" name="_<{$i}>_<{$j}>[]" value="<{$data.easy_code}>" onclick="radio_onclick(this)"
            <{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked <{/if}>>
          <{$data.easy_name|escape:"html"}>
        </label>

        <{*<!--関連する患者情報の数-->*}>
        <{if $data.easy_code==2}>
          <{if array_intersect(array(310,320,330,340,350,360,370,380,390), $grp_flags)}>
            &nbsp;&nbsp;：&nbsp;関連する患者数
            <select id="id_200_30" name="_200_30" onchange="selectbox_onchange(this);">
              <option value=""></option>
              <{foreach from=$grps[200].easy_item_list[30].easy_list item=list}>
                <{if $item_element_no_disp[200][30] == "" || !in_array($list.easy_code,$item_element_no_disp[200][30])}>
                  <option value="<{$list.easy_code}>" <{if in_array($list.easy_code, (array)@$vals[200][30])}>selected<{/if}>>
                    <{$list.easy_name|escape:"html"}>
                  </option>
                <{/if}>
              <{/foreach}>
            </select>
          <{/if}>
        <{/if}>

        <{*<!--その他の入力欄-->*}>
        <{if isset($data.other_code)}>
          &nbsp;&nbsp;&nbsp;&nbsp;備考（<input type="text" class="text_normal" id="id_<{$i}>_<{$data.other_code}>" name="_<{$i}>_<{$data.other_code}>" value="<{$vals[$i][$data.other_code][0]}>">）
        <{/if}>

      </span>
    <{/if}>
  <{/foreach}>

<{*<!--
---------------------------------------------------------------------------------------------------
アセスメント・患者の状態
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $i == 140 && $j == 10}>
  <{*<!--アセスメント・患者の状態-->*}>
  <{foreach from=$grps[140].easy_item_list[10].easy_list item=e140_item}>
    <{assign var="e140_code" value=$e140_item.easy_code}>
    <{assign var="v140" value=$vals[140][10]}>
    <{if in_array($e140_code, $asses_use)}>
      <div>
          <span class="item_set">
            <label>
              <input  type='checkbox'
                id="140_10_<{$e140_code}>"
                class="140_10"
                name="_140_10[]" value="<{$e140_code}>"
                <{if in_array($e140_code,(array)@$v140)}>checked<{/if}>
              >
              <{$e140_item.easy_name|escape:"html"}>：
            </label>
         </span>

        <{*<!--アセスメント・患者の状態：詳細-->*}>
        <{assign var="e150" value=$grps[150].easy_item_list[$e140_code]}>
        <{assign var="v150" value=$vals[150][$e140_code]}>
        <{if $e150.easy_item_type != 'select'}>
          <{foreach from=$e150.easy_list item=e150_item}>
            <{assign var="e150_code" value=$e150_item.easy_code}>
            <{if $item_element_no_disp[150][$e140_code] == "" || !in_array($e150_code, $item_element_no_disp[150][$e140_code])}>
              <span class="item_set">
                <label>
                  <input  type="radio"
                    id="id_150_<{$e140_code}>_<{$e150_code}>"
                    class="150_<{$e140_code}>"
                    name="_150_<{$e140_code}>[]"
                    value="<{$e150_code}>"
                    <{if in_array($e150_code, (array)@$v150)}>checked<{/if}>
                    <{if !in_array($e140_code,(array)@$v140)}>disabled<{/if}>
                  >
                  <{$e150_item.easy_name|escape:"html"}>
                </label>
              </span>
            <{/if}>
          <{/foreach}>

          <{*<!--ミトンあり詳細-->*}>
          <{if $e140_code == 20}>
            <div id="detail_150_20" style="margin:3px 0 10px 20px;">
              <{foreach from=$grps[160].easy_item_list[10].easy_list item=e160_item}>
                <{if !in_array($e160_item.easy_code,$item_element_no_disp[160][10])}>
                  <span class="item_set">
                    <label>
                      <input  type='checkbox'
                        name="_160_10[]"
                        id="150_20_10_<{$e160_item.easy_code}>"
                        class="150_20_10"
                        value="<{$e160_item.easy_code}>"
                        <{if in_array($e160_item.easy_code, (array)@$vals[160][10])}>checked<{/if}>
                        <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "20"}>disabled<{/if}>
                      >
                      <{$e160_item.easy_name|escape:"html"}>
                    </label>
                  </span>
                <{/if}>
              <{/foreach}>
            </div>
          <{/if}>

          <{*<!--リスク回避用具-->*}>
          <{if $e140_code == 6}>
            <div id="detail_150_6" style="margin:3px 0 10px 20px;">
              <{foreach from=$grps[170].easy_item_list[10].easy_list item=e170_item}>
                <{if !in_array($e170_item.easy_code,$item_element_no_disp[170][10])}>
                  <span class="item_set">
                    <label>
                      <input  type='checkbox'
                        name="_170_10[]"
                        id="150_6_10_<{$e170_item.easy_code}>"
                        class="150_6_10"
                        value="<{$e170_item.easy_code}>"
                        <{if in_array($e170_item.easy_code, (array)@$vals[170][10])}>checked<{/if}>
                        <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10"}>disabled<{/if}>
                      >
                      <{$e170_item.easy_name|escape:"html"}>
                    </label>
                    <{if $e170_item.easy_code == 99}>
                      <input type="text" class="text_normal 150_6_20" id="150_6_20" name="_170_20" value="<{$vals[170][20][0]}>" <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10" || !in_array(99,(array)@$vals[170][10])}>disabled<{/if}> />
                    <{/if}>
                  </span>
                <{/if}>
              <{/foreach}>
            </div>
          <{/if}>

          <{*<!--拘束用具-->*}>
          <{if $e140_code == 7}>
            <div id="detail_150_7" style="margin:3px 0 10px 20px;">
              <{foreach from=$grps[180].easy_item_list[10].easy_list item=e180_item}>
                <{if !in_array($e180_item.easy_code,$item_element_no_disp[180][10])}>
                  <span class="item_set">
                    <label>
                      <input  type='checkbox'
                              name="_180_10[]"
                              id="150_7_10_<{$e180_item.easy_code}>"
                              class="150_7_10"
                              value="<{$e180_item.easy_code}>"
                              <{if in_array($e180_item.easy_code, (array)@$vals[180][10])}>checked<{/if}>
                              <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10"}>disabled<{/if}>
                      >
                      <{$e180_item.easy_name|escape:"html"}>
                    </label>
                    <{if $e180_item.easy_code == 99}>
                      <input type="text" class="text_normal 150_7_20" id="150_7_20" name="_180_20" value="<{$vals[180][20][0]}>" <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10" || !in_array(99,(array)@$vals[180][10])}>disabled<{/if}> />
                    <{/if}>
                  </span>
                <{/if}>
              <{/foreach}>
            </div>
          <{/if}>

          <{*<!--身体拘束行為-->*}>
          <{if $e140_code == 8}>
            <div id="detail_150_8" style="margin:3px 0 10px 20px;">
              <{foreach from=$grps[190].easy_item_list[10].easy_list item=e190_item}>
                <{if !in_array($e190_item.easy_code,$item_element_no_disp[190][10])}>
                  <span class="item_set">
                    <label>
                      <input  type='checkbox'
                              name="_190_10[]"
                              id="150_8_10_<{$e190_item.easy_code}>"
                              class="150_8_10"
                              value="<{$e190_item.easy_code}>"
                              <{if in_array($e190_item.easy_code, (array)@$vals[190][10])}>checked<{/if}>
                              <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10"}>disabled<{/if}>
                      >
                      <{$e190_item.easy_name|escape:"html"}>
                    </label>
                    <{if $e190_item.easy_code == 99}>
                      <input type="text" class="text_normal 150_8_20" id="150_8_20" name="_190_20" value="<{$vals[190][20][0]}>" <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10" || !in_array(99,(array)@$vals[190][10])}>disabled<{/if}>  />
                    <{/if}>
                  </span>
                <{/if}>
              <{/foreach}>
            </div>
          <{/if}>

        <{else}>
          <select name="_150_<{$e140_code}>"
            class="150_<{$e140_code}>"
            id="id_150_<{$e140_code}>_0"
            <{if !in_array($e140_code, (array)@$vals[140][10])}>disabled<{/if}>
          >
            <option value=""></option>
              <{foreach from=$e150.easy_list item=e150_list}>
                <{if $item_element_no_disp[150][$e140_code] == "" || !in_array($e150_list.easy_code,$item_element_no_disp[150][$e140_code])}>
                  <option value="<{$e150_list.easy_code}>"
                    <{if in_array($e150_list.easy_code, (array)@$vals[150][$e140_code])}>selected<{/if}>
                  >
                  <{$e150_list.easy_name|escape:"html"}>
                <{/if}>
              <{/foreach}>
          </select>
        <{/if}>
      </div>
    <{/if}>
  <{/foreach}>

<{*<!--
---------------------------------------------------------------------------------------------------
影響への影響・患者への精神的影響・対応
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif ($i==1400 && $j==30) || ($i==1400 && $j==60) || ($i==1410 && $j==20)}>
  <{foreach from=$ic key=k item=data}>
    <{if $item_element_no_disp[$i][$j] == "" || !in_array($data.item_id,$item_element_no_disp[$i][$j])}>
      <span class="item_set">
        <label>
          <input type='checkbox' id="id_<{$i}>_<{$j}>_<{$key}>_<{$k}>" name="_<{$i}>_<{$j}>[]"
            value="<{$data.item_id}>" onclick="checkbox_onclick(this)"
            <{if in_array($data.item_id, (array)@$vals[$i][$j])}> checked<{/if}>
          >
          <{$data.item_name|escape:"html"}>
        </label>

        <{*<!--その他の入力欄-->*}>
        <{if isset($data.other_code)}>
          <span id="id_<{$i}>_<{$data.other_code}>_<{$key}>">
            <{*<!--影響/対応区分毎に表示エリアが存在するが、other_codeは影響/対応に各1つのみ。複数の表示エリアに出力するとnameが重複してしまうため、区分が選択されている場合のみ出力-->*}>
            <{if in_array($key, (array)@$vals[$i][10])}>
              (<input type="text" class="text_normal" id="id_<{$i}>_<{$data.other_code}>" name="_<{$i}>_<{$data.other_code}>" value="<{$vals[$i][$data.other_code][0]}>">)
            <{/if}>
          </span>
        <{/if}>
      </span>
    <{/if}>
  <{/foreach}>

<{*<!--
---------------------------------------------------------------------------------------------------
対応詳細
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $i==1410 && $j==30}>
  <{foreach from=$ic key=k item=data}>
    <{if $item_element_no_disp[$i][$j] == "" || !in_array($data.item_id,$item_element_no_disp[$i][$j])}>
      <span class="item_set">
        <label>
          <input type='checkbox' id="id_<{$i}>_<{$j}>_<{$key}>_<{$k}>" name="_<{$i}>_<{$j}>[]"
            value="<{$data.sub_item_id}>" onclick="checkbox_onclick(this)"
            <{if in_array($data.sub_item_id, (array)@$vals[$i][$j])}> checked<{/if}>>
            <{$data.sub_item_name|escape:"html"}>
        </label>
      </span>
    <{/if}>
  <{/foreach}>

<{*<!--
---------------------------------------------------------------------------------------------------
患者影響レベル
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $i == 90 && $j == 10}>
  <table id="level_info">
    <{foreach from=$level_infos key=k item=data}>
	  <{if empty($level_use) || in_array($data.easy_code, $level_use)}>
        <{if $data.use_flg}>
          <tr>
            <td>
              <label class="item_set">
                <input type='radio' name="_<{$i}>_<{$j}>[]" id="id_<{$i}>_<{$j}>_<{$k}>" value="<{$data.easy_code}>"
                  <{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>>
                <{$data.easy_name|escape:"html"}>
              </label>
            </td>
            <td>
              ：<{$data.message|escape:"html"}>
            </td>
          </tr>
        <{/if}>
	  <{/if}>
    <{/foreach}>
  </table>

<{*<!--
---------------------------------------------------------------------------------------------------
評価予定日
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $i == 620 && $j == 33}>
  <input type="text" id="id_620_33" name="_620_33" class="text_date" value="<{$vals[620][33][0]}>" readonly>
  <img id="caller_620_33" class="calendar_btn"; src="img/calendar_link.gif" onclick="window.open('hiyari_calendar.php?session=<{$session}>&caller=620_33', 'newwin', 'width=640,height=480,scrollbars=yes');"/>

<{*<!--
---------------------------------------------------------------------------------------------------
テキストボックス
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'text'}>
  <input type="text" id="id_<{$i}>_<{$j}>"
    <{if !isset($txt_class) || $txt_class==""}>
      class="text_normal"
    <{else}>
      class="<{$txt_class}>"
    <{/if}>
    name="_<{$i}>_<{$j}>" value="<{$vals[$i][$j][0]}>" onchange="text_onchange(this);" >

  <{*<!--いつごろ(不明の時に記載) 、関連医薬品、医療材料・諸物品等、医療機器等-->*}>
  <{if ($i==105 && $j==50) || ($i==100 && $j==50) || ($i==1000)}>
    <input type="button" value="不明" onclick="unknown_item('<{$i}>','<{$j}>')">

  <{*<!--主治医-->*}>
  <{elseif ($i==243 && $j==73)}>
    <input type="button" value="職員マスターからコピー" onclick="window.open('hiyari_doctor_search.php?session=<{$session}>', 'newwin', 'width=640,height=480,scrollbars=yes');">
    <span id="doctor_have_span"></span>

  <{*<!--入院日-->*}>
  <{elseif ($i==246 && $j==76) || ($i>=380 && $i<=389 && $j==10)}>
    <img id="caller_<{$i}>_<{$j}>" class="calendar_btn" src="img/calendar_link.gif" onclick="window.open('hiyari_calendar.php?session=<{$session}>&caller=<{$i}>_<{$j}>', 'newwin', 'width=640,height=480,scrollbars=yes');"/>
    <{if $hospital_ymd_flg == 't'}>
      <{if $iwata_flg || $patient_search_ex_flg || ($i>=380 && $i<=389 && $j==10)}>
        <!-- 入院年月日個別取得は行わない -->
      <{else}>
        <input type="button" value="患者マスターからコピー" onclick="inpt_search_open();">
      <{/if}>
    <{/if}>

  <{*<!--患者ID-->*}>
  <{elseif ($i==210 && $j==10)}>
    <{if $patient_profile_flg == 't'}>
      <{if $kameda_flg}>
        <input type="button" value="コピー" onclick="getPtid();">
      <{elseif $cis_flg}>
        <input type="button" value="検索" onclick="cis_patient_search('report_input');">
      <{elseif $kinkyo_flg}>
        <input type="button" value="検索" onclick="kinkyo_patient_search('report_input');">
      <{elseif $nishiyodo_flg}>
        <input type="button" value="検索" onclick="nishiyodo_patient_search('report_input');">
      <{elseif $gifudaigaku_flg}>
        <input type="button" value="検索" onclick="gifudaigaku_patient_search('report_input')">
      <{elseif $boueiidai_flg}>
        <input type="button" value="検索" onclick="boueiidai_patient_search('report_input')">
      <{elseif $fukushima_flg}>
        <input type="button" value="検索" onclick="fukushima_patient_search('report_input')">
      <{elseif $patient_search_ex_flg}>
        <input type="button" value="検索" onclick="patient_search_ex('report_input')">
      <{elseif $iwata_flg}>
        <input type="button" value="検索" onclick="iwata_patient_search('report_input')">
      <{else}>
        <input type="button" value="検索" onclick="patient_search();" <{if $pt_auth != "1"}> disabled<{/if}>>
      <{/if}>
    <{/if}>

  <{*<!--インシデントに直接関連する疾患名/関連する疾患名１〜３-->*}>
  <{elseif ($i==250 && $j==80) || ($i==260 && $j==90) || ($i==270 && $j==100) || ($i==280 && $j==110)}>
    <{if $byoumei_search_flg == 't'}>
      <input type="button" value="検索" onclick="popupByomeiSelect('id_<{$i}>_<{$j}>');">
    <{/if}>
  <{/if}>

  <{*<!--テキストクラスをクリアしておく-->*}>
  <{assign var="" value=$txt_class}>

<{*<!--
---------------------------------------------------------------------------------------------------
テキストエリア
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'textarea'}>
  <textarea id="id_<{$i}>_<{$j}>" rows="<{if isset($row_num)}><{$row_num}><{else}>10<{/if}>" name="_<{$i}>_<{$j}>"><{$vals[$i][$j][0]}></textarea>

<{*<!--
---------------------------------------------------------------------------------------------------
カレンダー
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'calendar'}>
  <input type="text" id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" class="text_date" value="<{$vals[$i][$j][0]}>" readonly>
  <img id="caller_<{$i}>_<{$j}>" class="calendar_btn"; src="img/calendar_link.gif" onclick="window.open('hiyari_calendar.php?session=<{$session}>&caller=<{$i}>_<{$j}>', 'newwin', 'width=640,height=480,scrollbars=yes');"/>

<{*<!--
---------------------------------------------------------------------------------------------------
セレクトボックス
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'select'}>
  <select id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" onchange="selectbox_onchange(this);">
    <option value=""></option>
    <{foreach from=$grps[$i].easy_item_list[$j].easy_list item=data}>
      <{if $item_element_no_disp[$i][$j] == "" || !in_array($data.easy_code,$item_element_no_disp[$i][$j])}>
        <option value="<{$data.easy_code}>" <{if in_array($data.easy_code, (array)@$vals[$i][$j])}>selected<{/if}>>
          <{$data.easy_name|escape:"html"}>
        </option>
      <{/if}>
    <{/foreach}>
  </select>

<{*<!--
---------------------------------------------------------------------------------------------------
チェックボックス（pause_flgがあるのは、発生要因のみ）
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'checkbox'}>
  <{foreach name=n from=$grps[$i].easy_item_list[$j].easy_list key=k item=data}>
    <{if $data.pause_flg == "t"}>
      <{if $smarty.foreach.n.index != 0}>
        <br>
      <{/if}>
      <span class="pause_title">
        <{$data.easy_name|escape:"html"}>
      </span>
      <br>
    <{else}>
      <{if $item_element_no_disp[$i][$j] == "" || !in_array($data.easy_code,$item_element_no_disp[$i][$j])}>
        <span class="item_set">
          <label>
            <input type='checkbox' id="id_<{$i}>_<{$j}>_<{$k}>" name="_<{$i}>_<{$j}>[]" value="<{$data.easy_code}>" onclick="checkbox_onclick(this)"
              <{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked<{/if}>>
              <{$data.easy_name|escape:"html"}>
          </label>

          <{*<!--その他の入力欄-->*}>
          <{if isset($data.other_code)}>
            (<input type="text" class="text_normal" id="id_<{$i}>_<{$data.other_code}>" name="_<{$i}>_<{$data.other_code}>" value="<{$vals[$i][$data.other_code][0]}>">)
          <{/if}>
        </span>
      <{/if}>
    <{/if}>
  <{/foreach}>

<{*<!--
---------------------------------------------------------------------------------------------------
ラジオボタン
---------------------------------------------------------------------------------------------------
-->*}>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'radio'}>
  <{foreach from=$grps[$i].easy_item_list[$j].easy_list key=k item=data}>
    <{if $item_element_no_disp[$i][$j] == "" || !in_array($data.easy_code,$item_element_no_disp[$i][$j])}>
      <span class="item_set">
        <label>
          <input type='radio' id="id_<{$i}>_<{$j}>_<{$k}>" name="_<{$i}>_<{$j}>[]" value="<{$data.easy_code}>" onclick="radio_onclick(this)"
            <{if in_array($data.easy_code, (array)@$vals[$i][$j])}> checked <{/if}>>
          <{$data.easy_name|escape:"html"}>
        </label>

        <{*<!--その他の入力欄-->*}>
        <{if isset($data.other_code)}>
          (<input type="text" class="text_normal" id="id_<{$i}>_<{$data.other_code}>" name="_<{$i}>_<{$data.other_code}>" value="<{$vals[$i][$data.other_code][0]}>">)
        <{/if}>

        <{*<!--ヒヤリ・ハット分類-->*}>
        <{if $i==125 && $j==85}>
          <input type="hidden" id="item_125_85_<{$data.easy_code}>" value="<{$data.easy_name|escape:"html"}>">
        <{/if}>
      </span>

      <{*<!--ヒヤリ・ハットの影響度 / 再発防止に資する(警鐘的)事例-->*}>
      <{if ($i==96 && $j==20) || ($i==510 && $j==20)}>
        <br>
      <{/if}>

      <{*<!--概要-->*}>
      <{if $i==900 && $j==10}>
        <{section name=k loop=$grps[900].easy_item_list[10].easy_list}>
          <{if $item_element_no_disp[900][10] == "" || !in_array($grps[900].easy_item_list[10].easy_list[k].easy_code,$item_element_no_disp[900][10])}>
            <input type="hidden" name="_<{900}>_<{10}>_<{$grps[900].easy_item_list[10].easy_list[k].easy_code}>_hdn"
              value="<{$grps[900].easy_item_list[10].easy_list[k].export_code}>">
          <{/if}>
        <{/section}>
      <{/if}>
    <{/if}>
  <{/foreach}>
  
<{/if}>