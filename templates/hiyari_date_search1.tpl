<script type="text/javascript">
//報告ファイルの日付種類を変更し、日付検索を実行
function change_search_date_target(search_date_target)
{
    //ポストバック
    document.list_form.search_date_target.value = search_date_target;
    document.list_form.page.value=1;
    if(search_date_target == "report_date"){
        var sort_item_value = "REGIST_DATE";
    }
    if(search_date_target == "evalutioin_date"){
        var sort_item_value = "EVALUATION_DATE";
    }
    if(search_date_target == "incident_date"){
        var sort_item_value = "INCIDENT_DATE";
    }
    document.list_form.sort_item.value = search_date_target;
    document.list_form.sort_div.value = "0";//昇順
    document.list_form.submit();
}

//年を変更し、日付検索(月指定)を実行します。
function change_cal_year(cur_date_year, cur_date_month, add_year)
{
    //変更後の年
    var new_year = cur_date_year + add_year;

    //チェック
    if(new_year < 0 || new_year > 9999){
        return;
    }

    //ポストバック
    document.list_form.search_date_mode.value="month";
    document.list_form.search_date_year.value=new_year;
    document.list_form.search_date_month.value=cur_date_month;
    document.list_form.page.value=1;
    document.list_form.submit();
}

//月を変更し、日付検索(月指定)を実行します。
function change_cal_month(cur_date_year, month)
{
    //前0付加
    if(month <= 9){
        month = "0" + month;
    }

    //ポストバック
    document.list_form.search_date_mode.value="month";
    document.list_form.search_date_year.value=cur_date_year;
    document.list_form.search_date_month.value=month;
    document.list_form.page.value=1;
    document.list_form.submit();
}

//日付検索(開始/終了指定)を実行します。
function search_date_st_ed()
{
    //入力値取得
    var st = document.getElementById("torey_date_start_input").value;
    var ed = document.getElementById("torey_date_end_input").value;

    //入力値チェック
    if( (st != "" && !is_date_str(st)) || (ed != "" && !is_date_str(ed)) ){
        alert("日付が正しくありません。");
        return;
    }

    //ポストバック
    document.list_form.search_date_mode.value="ymd_st_ed";
    document.list_form.page.value=1;
    document.list_form.submit();
}

//カレンダーの月の色を選択色にします。
function changeCalMonthColor(month_td_obj)
{
    month_td_obj.style.backgroundColor = '#FFDDFD';
}

//カレンダーの月の色を非選択色にします。
function resetCalMonthColor(month_td_obj)
{
    month_td_obj.style.backgroundColor = '#E5F8E1';
}
</script>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>
  
<table border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td width="5"><img src="img/i_corner1.gif" width="5" height="5"></td>
    <td width="170" background="img/i_up.gif"><img src="img/spacer.gif" width="1" height="5" alt=""></td>
    <td width="5"><img src="img/i_corner2.gif" width="5" height="5"></td>
  </tr>
  <tr>
    <td background="img/i_left.gif"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
    <td bgcolor="#E5F8E1" align="center">
      <!-- 報告ファイルの日付種類指定 -->
      <{if $file == "hiyari_rpt_report_classification.php"}>
        <table border="0" cellspacing="0" cellpadding="1" width="100%">
          <tr>
            <td align="center" style="border:#E5F8E1 solid 1px;">
              <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                  <td align="center" nowrap>
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                      <select name="search_date_target" onchange="change_search_date_target(this.value)" style="width:90%">
                        <option value="report_date"     <{if $search_date_target == "report_date"    }>selected<{/if}> >報告日</option>
                        <option value="incident_date"   <{if $search_date_target == "incident_date"  }>selected<{/if}> >発生日</option>
                        <option value="evaluation_date" <{if $search_date_target == "evaluation_date"}>selected<{/if}> >評価予定日</option>
                      </select>
                    </font>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <img src="img/spacer.gif" width="1" height="2" alt=""><br>
      <{/if}>
    
      <!-- 年月指定 -->
      <table border="0" cellspacing="0" cellpadding="1" width="100%">
        <tr>
          <{if $search_date_mode == "month"}>
            <td align="center" style="border:#35B341 solid 1px;">
          <{else}>
            <td align="center" style="border:#CCCCCC solid 1px;">
          <{/if}>
              <table border="0" cellspacing="0" cellpadding="1" width="100%">
                <tr>
                  <td align="center" style="border:#E5F8E1 solid 1px;">
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                      <tr>
                        <td align="center">
                          <table border="0" cellspacing="0" cellpadding="0" width="162">
                            <td align="right" valign="middle" width="40%">
                              <img src="img/left.gif" alt="前の年へ" style="cursor: pointer;" onclick="change_cal_year(<{$search_date_year}>,'<{$search_date_month}>',-1);">
                            </td>
                            <td align="center" valign="middle" width="20%">
                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                <span id="torey_cal_year_disp"><{$search_date_year}></span>
                              </font>
                            </td>
                            <td align="left" valign="middle" width="40%">
                              <img src="img/right.gif" alt="次の年へ" style="cursor: pointer;" onclick="change_cal_year(<{$search_date_year}>,'<{$search_date_month}>',1);">
                            </td>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td align="center">
                          <table border="0" cellspacing="0" cellpadding="0" width="162" style="border-collapse:collapse;">
                            <tr>
                              <{section loop=12 name=m}>
                                <{if $smarty.section.m.iteration==7}>
                                  </tr>
                                  <tr>
                                <{/if}>
                                  <{if $search_date_mode == "month" && $search_date_month + 0 == $smarty.section.m.iteration}>
                                    <{*選択月*}>
                                    <td align="center" width="26" style="background-color:orange;cursor:pointer;border:#999999 solid 1px;" onclick="change_cal_month(<{$search_date_year}>, <{$smarty.section.m.iteration}>);">
                                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10"><{$smarty.section.m.iteration}>月</font>
                                    </td>
                                  <{else}>
                                    <{*非選択月*}>
                                    <td align="center" width="26" style="background-color:#E5F8E1;cursor:pointer;border:#999999 solid 1px;" onmouseover="changeCalMonthColor(this);" onmouseout="resetCalMonthColor(this);" onclick="change_cal_month(<{$search_date_year}>, <{$smarty.section.m.iteration}>);">
                                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10"><{$smarty.section.m.iteration}>月</font>
                                    </td>
                                  <{/if}>
                              <{/section}>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
          </td>
        </tr>
      </table>
      <img src="img/spacer.gif" width="1" height="2" alt=""><br>
      
      <!-- 開始日・終了日指定 -->
      <table border="0" cellspacing="0" cellpadding="1" width="100%">
        <tr>
          <{if $search_date_mode == "ymd_st_ed"}>
            <td align="center" style="border:#35B341 solid 1px;">
          <{else}>
            <td align="center" style="border:#CCCCCC solid 1px;">
          <{/if}>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
              <tr>
                <td><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始年月日</font></nobr></td>
                <td><input type="text" id="torey_date_start_input" name="search_date_ymd_st" style="width:80px" value="<{$search_date_ymd_st}>" maxlength="10"></td>
                <td><img src="img/calendar_link.gif" style="cursor:pointer;" onclick="call_hiyari_calendar('torey_search_st', '<{$session}>')"/></td>
              </tr>
              <tr>
                <td><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">終了年月日</font></nobr></td>
                <td><input type="text" id="torey_date_end_input" name="search_date_ymd_ed" style="width:80px" value="<{$search_date_ymd_ed}>" maxlength="10"></td>
                <td><img src="img/calendar_link.gif" style="cursor:pointer;" onclick="call_hiyari_calendar('torey_search_ed', '<{$session}>')"/></td>
              </tr>
              <tr>
                <td align="left" colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="button" value="設定" onclick="search_date_st_ed()"></font></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td background="img/i_right.gif"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/i_corner3.gif" width="5" height="5"></td>
    <td background="img/i_down.gif"><img src="img/spacer.gif" width="1" height="5" alt=""></td>
    <td><img src="img/i_corner4.gif" width="5" height="5"></td>
  </tr>
</table>
