<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
</head>

<body id="news_attach">
  <form action="hiyari_news_attach_save.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="session" value="<{$session}>">
    
    <!-------------------------------------------------------------------------
    ヘッダー 
    -------------------------------------------------------------------------->
    <div id="header" class="clearfix">
      <div class="inner" class="clearfix">
        <h1><{$PAGE_TITLE}></h1>
      </div>
    </div>

    <!-------------------------------------------------------------------------
    コンテンツ
    -------------------------------------------------------------------------->
    <div id="content" class="clearfix">
      <div class="section">
        <div class="color02 radius clearfix">
          <div>
            添付ファイル：
            <input type="file" name="file" size="50">
            ※<{$upload_max_filesize}>まで
          </div>
          <div id="main_btn">
            <input type="submit" class="button radius_mini" value="添付">
          </div>
        </div>
      </div>
    </div>
  </form>
</body>
</html>
