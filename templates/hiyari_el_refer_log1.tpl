<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <style type="text/css">
    table.list {border-collapse:collapse;}
    table.list td {border:#5279a5 solid 1px;}
    table.list td td {border-width:0;}
  </style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <!-- �إå��� START -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr height="32" bgcolor="#35B341">
            <td class="spacing"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j14" color="#ffffff"><b><{$PAGE_TITLE}></b></font></td>
            <td>&nbsp;</td>
            <td width="10">&nbsp;</td>
            <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="�Ĥ���" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
          </tr>
        </table>
        <img src="img/spacer.gif" width="10" height="10" alt=""><br>
        <!-- �إå��� END -->
      </td>
    </tr>
    
    <tr>
      <td>
        <!-- ���� START -->
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
            <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
          </tr>
          <tr>
            <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
            <td bgcolor="#F5FFE5">
              <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <{foreach from=$sort_type item=row}>
                  <tr height="22">
                    <{foreach from=$row item=s}>
                      <td width="33%" align="center" bgcolor="#FFFFFF">
                        <font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
                          <{if $s.is_selected}>
                            <b>
                          <{else}>
                            <a href="hiyari_el_refer_log.php?session=<{$session}>&sort=<{$s.sort_id}>">
                          <{/if}>
                          <{$s.title}>
                          <{if $s.is_selected}>
                            </b>
                          <{else}>
                            </a>
                          <{/if}>
                        </font>
                      </td>
                    <{/foreach}>
                  </tr>
                <{/foreach}>
              </table>

              <{if isset($list)}>
                <{if isset($page_count)}>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:5px 0 2px;">
                    <tr>
                      <td>
                        <font size="3" face="�ͣ� �Х����å�, Osaka" class="j14">
                          <{if $page > 1}>
                            <a href="<{$fname}>?session=<{$session}>&sort=<{$sort}>&page=<{$page-1}>">����</a>��
                          <{else}>
                            ������
                          <{/if}>

                          <{section name=page_list loop=$page_count}> 
                            <{if $smarty.section.page_list.iteration == $page}>
                              <b><{$smarty.section.page_list.iteration}></b>
                            <{else}>
                              <a href="<{$fname}>?session=<{$session}>&sort=<{$sort}>&page=<{$smarty.section.page_list.iteration}>"><{$smarty.section.page_list.iteration}></a>
                            <{/if}>
                          <{/section}>

                          <{if $page < $page_count}>
                            ��<a href="<{$fname}>?session=<{$session}>&sort=<{$sort}>&page=<{$page+1}>">����</a>
                          <{else}>
                            ������
                          <{/if}>
                        </font>
                      </td>
                    </tr>
                  </table>
                <{else}>
                  <img src="img/spacer.gif" alt="" width="1" height="5"><br>
                <{/if}>
                
                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                  <tr height="22" bgcolor="#DFFFDC">
                    <td width="23%"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����ƥ��̾</font></td>
                    <td width="8%" align="right" style="padding-right:4px;"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">���ȿ�</font></td>
                    <td width="13%"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��������</font></td>
                    <td width="12%"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��Ͽ��</font></td>
                    <td width="38%"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">��¸��</font></td>
                    <td width="6%" align="center"><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">����</font></td>
                  </tr>
                  <{foreach from=$list item=i}>
                    <tr height="22" valign="top" bgcolor="#FFFFFF">
                      <td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><{$i.lib_nm}></font></td>
                      <td align="right" style="padding-right:4px;">
                        <font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
                          <{$i.ref_count}>
                        </font>
                      </td>
                      <td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><{$i.lib_up_date}></font></td>
                      <td><font size="3" face="�ͣ� �Х����å�, Osaka" class="j12"><{$i.emp_nm}></font></td>
                      <td>
                        <font size="3" face="�ͣ� �Х����å�, Osaka" class="j12">
                          <{$i.lib_cate_nm}>
                          <{$i.path}>
                        </font>
                      </td>
                      <{if $lib_get_filename_flg > 0}>
                        <td align="center"><input type="button" value="����" onclick="location.href = '<{$i.url}>';"></td>
                      <{else}>
                        <td align="center"><input type="button" value="����" onclick="window.open('<{$i.url}>');"></td>
                      <{/if}>
                    </tr>
                  <{/foreach}>
                </table>
              <{/if}>
            </td>
            <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
          </tr>
          <tr>
            <td><img src="img/r_3.gif" width="10" height="10"></td>
            <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td><img src="img/r_4.gif" width="10" height="10"></td>
          </tr>
        </table>
        <!-- ���� END -->
      </td>
    </tr>
  </table>

  <form name="delform" action="hiyari_el_delete.php" method="post">
    <input type="hidden" name="session" value="<{$session}>">
    <input type="hidden" name="did[]" value="">
    <input type="hidden" name="path" value="2">
    <input type="hidden" name="sort" value="<{$sort}>">
    <input type="hidden" name="page" value="<{$page}>">
  </form>
  <iframe name="download" width="0" height="0" frameborder="0"></iframe>
</body>
</html>
