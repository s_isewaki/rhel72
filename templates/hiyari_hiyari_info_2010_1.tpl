<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <{$INCIDENT_TITLE}> | 医療事故等情報報告</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<SCRIPT type="text/javascript">
  //EXCEL出力実効
  function excel_download()
  {
    document.xls.submit();
  }

  //子画面起動実効
  function show_report_list_window(vertical_index,horizontal_index,report_id_list)
  {
    //報告書一覧データを設定
    document.report_id_list_form.report_id_list.value = report_id_list;

    //縦軸の項目名を設定
    var v_name = '合計';
    <{foreach from=$v_names item=i key=k}>
      if (vertical_index==<{$k}>){
          v_name = "<{$i.v_name}>";
      }
    <{/foreach}>
    document.report_id_list_form.vertical_item_label.value = v_name;

    //横軸の項目名を設定
    var h_name = '合計';
    <{foreach from=$h_names item=i key=k}>
      if (horizontal_index==<{$k}>){
          h_name = "<{$i.h_name}>";
      }
    <{/foreach}>
    document.report_id_list_form.horizontal_item_label.value = h_name;

    //子画面に対して送信
    var h = window.screen.availHeight - 30;
    var w = window.screen.availWidth - 10;
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
    window.open('', 'rm_stats_reports_window', option);
    document.report_id_list_form.submit();
  }
</SCRIPT>
<style type="text/css">
  .list {border-collapse:collapse;}
  .list td {border:#35B341 solid 1px;}
  .list_in {border-collapse:collapse;}
  .list_in td {border:#FFFFFF solid 0px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <{include file="hiyari_header1.tpl"}>
      <img src="img/spacer.gif" width="1" height="5" alt=""><br>
      
      <!-- リンク START -->
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td style="padding:3px 6px; float:left; background-color:#35B341; border:1px solid #ccc;">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
            <a href="hiyari_hiyari_info_2010.php?session=<{$session}>" style="color:#FFFFFF">発生件数情報</a>
            </font>
          </td>
          <td width="5"></td>
          <td style="padding:3px 6px; float:left; background-color:#f4f4f4; border:1px solid #ccc;">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
            <a href="hiyari_hiyari_list.php?session=<{$session}>" style="color:#00f">データ提出(CSV)</a>
            </font>
          </td>
          <td width="5"></td>
          <td style="padding:3px 6px; float:left; background-color:#f4f4f4; border:1px solid #ccc;">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
            <a href="hiyari_hiyari_xml_list.php?session=<{$session}>" style="color:#00f">データ提出(XML)</a>
            </font>
          </td>
        </tr>
      </table>
      <img src="img/spacer.gif" width="1" height="5" alt=""><br>     
      <!-- リンク END -->
      
      <!-- Excel出力 START -->
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" width="280">
            <table border="0" cellspacing="0" cellpadding="2" class="list">
              <tr>
              </tr>
            </table>
          </td>
          <td align="left" nowrap>
            <a href="JavaScript:void(0);" onclick="excel_download(); return false;"><img src="img/hiyari_rp_excel.gif" border="0"></a><br>
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">Excel出力</font>
          </td>
        </tr>
      </table>
      <!-- Excel出力 END -->
      
      <table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr>
          <!-- 年月 START -->
          <td width="280" valign="top">
            <table width="100%" border="1" cellspacing="0" cellpadding="2" class="list_2">
              <tr>
                <td>
                  <{foreach from=$years item=y}>
                    <span id="text<{$y}>" onClick="getElementById('num<{$y}>').style.display=(getElementById('num<{$y}>').style.display=='block'?'none':'block');if(getElementById('num<{$y}>').style.display=='block'){getElementById('text<{$y}>').innerHTML='<span style=\'cursor: pointer\'>−</span>';}else{getElementById('text<{$y}>').innerHTML='<span style=\'cursor: pointer\'>＋</span>';}">
                      <{if $year==$y && $mode=='quarter' || $y==$cur_year}>
                        <span style="cursor: pointer">−</span>
                      <{else}>
                        <span style="cursor: pointer">＋</span>
                      <{/if}>
                    </span>
                    <a href="hiyari_hiyari_info_2010.php?session=<{$session}>&year=<{$y}>&mode=all">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" <{if $year==$y && $mode=='all'}>style="background-color:#FFDDFD"<{/if}>>
                        <{$y}>年
                      </font>
                    </a>
                    <br>
                    <div id="num<{$y}>" style="display:<{if $y==$cur_year}>block<{elseif $year==$y && $mode=='quarter'}>block<{else}>none<{/if}>">
                      &nbsp;&nbsp;&nbsp;−<a href="hiyari_hiyari_info_2010.php?session=<{$session}>&year=<{$y}>&month=1&mode=quarter"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"<{if $year==$y && $month == 1 && $mode=='quarter'}> style="background-color:#FFDDFD"<{/if}>>1〜3月</font></a><br>
                      &nbsp;&nbsp;&nbsp;−<a href="hiyari_hiyari_info_2010.php?session=<{$session}>&year=<{$y}>&month=4&mode=quarter"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"<{if $year==$y && $month == 4 && $mode=='quarter'}> style="background-color:#FFDDFD"<{/if}>>4〜6月</font></a><br>
                      &nbsp;&nbsp;&nbsp;−<a href="hiyari_hiyari_info_2010.php?session=<{$session}>&year=<{$y}>&month=7&mode=quarter"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"<{if $year==$y && $month == 7 && $mode=='quarter'}> style="background-color:#FFDDFD"<{/if}>>7〜9月</font></a><br>
                      &nbsp;&nbsp;&nbsp;−<a href="hiyari_hiyari_info_2010.php?session=<{$session}>&year=<{$y}>&month=10&mode=quarter"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"<{if $year==$y && $month == 10 && $mode=='quarter'}> style="background-color:#FFDDFD"<{/if}>>10〜12月</font></a><br>
                    </div>
                  <{/foreach}>
                </td>
              </tr>
            </table>
          </td>
          <!-- 年月 END -->

          <!-- 集計結果表 START -->
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">           
              <!-- 集計結果タイトル行 START -->                     
                <!-- 表タイトル START -->
                <tr height="22" bgcolor="#DFFFDC" valign="top">              
                  <td width="18%" rowspan="5" style="text-align:center;vertical-align:middle;">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目</font>
                  </td>
                  <td width="64%" colspan="4" style="text-align:center">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">誤った医療の実施の有無</font>
                  </td>
                  <td width="18%" rowspan="5" style="text-align:center;vertical-align:middle;">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計</font>
                  </td>
                </tr>
                <tr bgcolor="#DFFFDC">
                  <td colspan="3" style="text-align:center;" nowrap>
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">実施なし</font>
                  </td>
                  <td rowspan="4" style="vertical-align:middle;text-align:center;">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">実施あり</font>
                  </td>
                </tr>
                <tr bgcolor="#DFFFDC">
                  <td colspan="3" style="text-align:center;" nowrap>
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">影響度</font>
                  </td>
                </tr>
                <tr bgcolor="#DFFFDC">
                  <td colspan="3" style="text-align:center;" nowrap>
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">当該事例の内容が仮に実施された場合、</font>
                  </td>
                </tr>
                <!-- 表タイトル END -->
                <!-- 列タイトル START -->
                <tr bgcolor="#DFFFDC">
                  <{foreach from=$col_title item=i}>
                    <td width="<{$col_width}>%">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$i}></font>
                    </td>
                  <{/foreach}>
                </tr>
                <!-- 列タイトル END -->                
              <!-- 集計結果タイトル行 END -->
              
              <!-- 集計結果セル行 START -->
                <{foreach from=$data1 item=v key=vkey}>
                  <tr height="22" valign="top">
                    <!-- 行タイトル START -->
                    <td bgcolor="#DFFFDC" nowrap>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list_in">
                        <tr>
                          <td align="left">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" nowrap>
                              <{$v.row_title}>
                            </font>
                          </td>
                        </tr>
                      </table>
                    </td>               
                    <!-- 行タイトル END -->
                  
                    <!-- セル START -->
                    <{foreach from=$v.items item=h key=hkey}>
                      <td align="right" bgcolor="#FFFFFF">
                        <{if $h.count > 0}>
                          <a href="javascript:void(0)" onclick="show_report_list_window(<{$vkey}>,<{$hkey}>,'<{$h.report_ids}>');return false;">
                        <{/if}>                          
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$h.count}></font>
                        <{if $h.count > 0}>
                          </a>
                        <{/if}>
                      </td>
                    <{/foreach}>
                    <!-- セル END -->
                    
                    <!-- 行合計 START -->
                    <td align="right" bgcolor="#FFFFFF">
                      <{if $v.sum > 0}>
                        <a href="javascript:void(0)" onclick="show_report_list_window(<{$vkey}>,-1,'<{$v.report_ids}>');return false;">
                      <{/if}>
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$v.sum}></font>
                      <{if $v.sum > 0}>
                        </a>
                      <{/if}>
                    </td>
                    <!-- 行合計 END -->
                  </tr>
                <{/foreach}>
              <!-- 集計結果セル行 END -->
              
              <!-- 集計結果合計行 START -->
                <tr height="22" valign="top">
                  <!-- 計タイトル START -->
                  <td bgcolor="#DFFFDC">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計</font>
                  </td>
                  <!-- 計タイトル END -->
                  
                  <!-- 列合計 START -->
                  <{foreach from=$data1_sums item=i name=sum}>
                    <td align="right" bgcolor="#FFFFFF">
                      <{if $i.sum > 0}>
                        <a href="javascript:void(0)" onclick="show_report_list_window(-1,<{$smarty.foreach.sum.index}>,'<{$i.report_ids}>');return false;">
                      <{/if}>
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$i.sum}></font>
                      <{if $i.sum > 0}>
                        </a>
                      <{/if}>
                    </td>
                  <{/foreach}>
                  <!-- 列合計 END -->
                  
                  <!-- 総合計 START -->
                  <td align="right" bgcolor="#FFFFFF">
                    <{if $sums_sum > 0}>
                      <a href="javascript:void(0)" onclick="show_report_list_window(-1,-1,'<{$report_ids}>');return false;">
                    <{/if}>
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$sums_sum}></font>
                    <{if $sums_sum > 0}>
                      </a>
                    <{/if}>
                  </td>
                  <!-- 総合計 END -->
                </tr>
              <!-- 集計結果合計行 END -->
              
              <!-- 行タイトル START -->
              <tr>
                <td bgcolor="#DFFFDC" nowrap colspan="6">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">再掲</font>
                </td>
              </tr>
              <!-- 行タイトル END -->
              
              <!-- 集計結果セル行 START -->
              <{foreach from=$data2 item=v key=vkey}>
                <tr height="22" valign="top">
                  <!-- 行タイトル START -->
                  <td bgcolor="#DFFFDC">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <{if $vkey==0}>��<{$v.row_title}><{/if}>
                    <{if $vkey==1}>��<{$v.row_title}><{/if}>
                    <{if $vkey==2}>��<{$v.row_title}><{/if}>
                    <{if $vkey==3}>��<{$v.row_title}><{/if}>
                    </font>
                  </td>
                  <!-- 行タイトル END -->
                
                  <!-- セル START -->
                  <{foreach from=$v.items item=h key=hkey}>
                    <td align="right" bgcolor="#FFFFFF">
                      <{if $h.count > 0}>
                        <a href="javascript:void(0)" onclick="show_report_list_window(<{$vkey}>,<{$hkey}>,'<{$h.report_ids}>');return false;">
                      <{/if}>
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$h.count}></font>
                      <{if $h.count > 0}>
                        </a>
                      <{/if}>
                    </td>                        
                  <{/foreach}>
                  <!-- セル END -->
                  
                  <!-- 行合計 START -->
                  <td align="right" bgcolor="#FFFFFF">
                    <{if $v.sum > 0}>
                      <a href="javascript:void(0)" onclick="show_report_list_window(<{$vkey}>,-1,'<{$v.report_ids}>');return false;">
                    <{/if}>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$v.sum}></font>
                    <{if $v.sum > 0}>
                      </a>
                    <{/if}>
                  </td>
                  <!-- 行合計 END -->
                </tr>
              <{/foreach}>                            
              <!-- 集計結果セル行 END -->              
            </table>           
          </td>
          
          <!-- 集計結果表 END -->         
        </tr>
      </table>
    </td>
  </tr>
</table>

<!--------------------------------------------------------------------------------------------------------------------------->
<!-- 他画面送信:子画面用フォーム -->
<!--------------------------------------------------------------------------------------------------------------------------->
<form name="report_id_list_form" action="hiyari_rm_stats_reports.php" method="post" target="rm_stats_reports_window">
  <input type="hidden" name="<{$session_name}>" value="<{$session_id}>">
  <!-- 送信条件 -->
  <input type="hidden" name="session" value="<{$session}>">
  <!-- 検索条件表示 -->
  <input type="hidden" name="vertical_item_label" value="">
  <input type="hidden" name="horizontal_item_label" value="">
  <!-- 集計データ -->
  <input type="hidden" name="report_id_list" value="">
</form>

<!--------------------------------------------------------------------------------------------------------------------------->
<!-- 他画面送信:Excel用フォーム -->
<!--------------------------------------------------------------------------------------------------------------------------->
<form name="xls" action="hiyari_hiyari_info_2010_excel.php" method="post" target="download">
  <input type="hidden" name="<{$session_name}>" value="<{$session_id}>">
  <!-- 送信条件 -->
  <input type="hidden" name="session" value="<{$session}>">
  <!-- 検索条件 -->
  <input type="hidden" name="year" value="<{$year}>">
  <input type="hidden" name="month" value="<{$month}>">
  <input type="hidden" name="mode" value="<{$mode}>">
  <!-- 結果 -->
  <input type="hidden" name="total_item_count" value="<{$total_item_count_list}>">
  <input type="hidden" name="total_vertical_sum_count" value="<{$total_vertical_sum_count_list}>">
  <input type="hidden" name="total_horizontal_sum_count" value="<{$total_horizontal_sum_count_list}>">
  <input type="hidden" name="total_sum_count" value="<{$total_sum_count_list}>">
  <input type="hidden" name="total_show_count" value="<{$total_show_count_list}>">
  <input type="hidden" name="total_vertical2_sum_count" value="<{$total_vertical2_sum_count_list}>">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
</body>
</html>
