<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <{$INCIDENT_TITLE}> | 報告書登録</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function checkForm() {
    var exp_year = document.FRM_MAIN.exp_Year.value;
    var exp_month = document.FRM_MAIN.exp_Month.value;
    var dept_year = document.FRM_MAIN.dept_Year.value;
    var dept_month = document.FRM_MAIN.dept_Month.value;

    if ((exp_year == '' && exp_month != '') || (exp_year != '' && exp_month == '') || (dept_year == '' && dept_month != '') || (dept_year != '' && dept_month == '')) {
        alert('年月は両方指定してください。');
        return;
    }
    <{if $can_belong_change}>
        if(document.FRM_MAIN.cls.value == "") {
            alert("所属を選択してください");
            return false;
        }

        if(document.FRM_MAIN.dept.value == "0") {
            alert("第三階層を選択してください");
            return false;
        }
    <{/if}>
    document.FRM_MAIN.submit();
}

function initPage() {
  <{if $can_belong_change}>
      setAttributeOptions(document.FRM_MAIN.cls, document.FRM_MAIN.atrb, document.FRM_MAIN.dept , document.FRM_MAIN.room , '<{$emp_atrb|escape:"html"}>', '<{$emp_dept|escape:"html"}>', '<{$emp_room|escape:"html"}>');
  <{/if}>
}

function setAttributeOptions(class_box, atrb_box, dept_box, room_box, atrb_id, dept_id, room_id) {
	delAllOptions(atrb_box);

	var class_id = class_box.value;
	<{foreach item=arr_atrb_list from=$atrb_list}>
	if (class_id == '<{$arr_atrb_list.class_id|escape:"html"}>') {
		addOption(atrb_box, '<{$arr_atrb_list.atrb_id|escape:"html"}>', '<{$arr_atrb_list.atrb_nm|escape:"html"}>', atrb_id);
	}
	<{/foreach}>
	if (atrb_box.options.length == 0) {
		addOption(atrb_box, '0', '（未登録）');
	}
	
	setDepartmentOptions(atrb_box, dept_box, room_box, dept_id, room_id);
}


function setDepartmentOptions(atrb_box, dept_box, room_box, dept_id, room_id) {
	delAllOptions(dept_box);

	var atrb_id = atrb_box.value;

	<{foreach item=arr_dept_list from=$dept_list}>
	if (atrb_id == '<{$arr_dept_list.atrb_id|escape:"html"}>') {
		addOption(dept_box, '<{$arr_dept_list.dept_id|escape:"html"}>', '<{$arr_dept_list.dept_nm|escape:"html"}>', dept_id);
	}
	<{/foreach}>
	if (dept_box.options.length == 0) {
		addOption(dept_box, '0', '（未登録）');
	}

	setRoomOptions(dept_box, room_box, room_id);
}

function setRoomOptions(dept_box, room_box, room_id) {
	if (!room_box) {
		return;
	}

	delAllOptions(room_box);

	var dept_id = dept_box.value;

	<{foreach item=arr_room_list from=$room_list}>
	if (dept_id == '<{$arr_room_list.dept_id|escape:"html"}>') {
		if (room_box.options.length == 0) {
			addOption(room_box, '0', '　　　　　');
		}
		addOption(room_box, '<{$arr_room_list.room_id|escape:"html"}>', '<{$arr_room_list.room_nm|escape:"html"}>', room_id);
	}
	<{/foreach}>
	if (room_box.options.length == 0) {
		addOption(room_box, '0', '（未登録）');
	}
}

function delAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <!-- ヘッダー START -->
        <{include file="hiyari_header1.tpl"}>
        <!-- ヘッダー END -->
        <form name='FRM_MAIN' action='hiyari_profile_register.php' method='post'>
          <table width="625" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
              <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
              <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
            </tr>
            <tr>
              <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
              <td>
                <table width="620" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td colspan="3"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                  </tr>
                  <tr>
                    <td width="1"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                    <td>
                      <table width="620" border="0" cellspacing="0" cellpadding="10" class="list">
                        <tr height="22">
                          <td class="spacing" width="180" bgcolor="#DFFFDC">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">専門医・認定医及びその他の医療従事者の専門・認定資格</font>
                          </td>
                          <td>
                            <textarea name="qualification" cols="45" rows="4"><{$qualification}></textarea>
                          </td>
                        </tr>
                        <tr height="22">
                          <td class="spacing" bgcolor="#DFFFDC">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font>
                          </td>
                          <td>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                              <select name="exp_code">
                                <option value="">　</option>
                                <{foreach item=exp1 from=$exp_list}>
                                  <{if $exp1.easy_code == $exp_code}>
                                    <{assign var="exp_code_selected" value="selected"}>
                                  <{else}>
                                    <{assign var="exp_code_selected" value=""}>
                                  <{/if}>
                                  <option value="<{$exp1.easy_code}>" <{$exp_code_selected}>><{$exp1.easy_name|escape:"html"}></option>
                                <{/foreach}>
                              </select>
                            </font>
                          </td>
                        </tr>
                        <tr height="22">
                          <td class="spacing" bgcolor="#DFFFDC">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種開始年月</font>
                          </td>
                          <td>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                              <select name="exp_Year">
                                <option value="">　</option>
                                <{foreach from=$exp_year_list item=year}>
                                  <option value="<{$year}>" <{if $exp_year==$year}>selected<{/if}>>
                                    <{$year}>
                                  </option>
                                <{/foreach}>
                              </select>年
                              <select name="exp_Month">
                                <option value="">　</option>
                                <{foreach from=$month_list item=month}>
                                  <option value="<{$month}>" <{if $exp_month==$month}>selected<{/if}>>
                                    <{$month}>
                                  </option>
                                <{/foreach}>
                              </select>月
                            </font>
                          </td>
                        </tr>
                        <tr height="22">
                          <td class="spacing" bgcolor="#DFFFDC">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署開始年月</font>
                          </td>
                          <td>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                              <select name="dept_Year">
                                <option value="">　</option>
                                <{foreach from=$dept_year_list item=year}>
                                  <option value="<{$year}>" <{if $dept_year==$year}>selected<{/if}>>
                                    <{$year}>
                                  </option>
                                <{/foreach}>
                              </select>年
                              <select name="dept_Month">
                                <option value="">　</option>
                                <{foreach from=$month_list item=month}>
                                  <option value="<{$month}>" <{if $dept_month==$month}>selected<{/if}>>
                                    <{$month}>
                                  </option>
                                <{/foreach}>
                              </select>月
                            </font>
                          </td>
                        </tr>
                      <{if $can_belong_change}>
                        <tr height="22">
                          <td class="spacing" bgcolor="#DFFFDC">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属部署</font>
                          </td>
                          <td>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                              <select name="cls" onchange="setAttributeOptions(this.form.cls, this.form.atrb, this.form.dept, this.form.room);">>
                                <option value="">　</option>
                                <{foreach item=arr_cls_list from=$cls_list}>
                                  <{if $arr_cls_list.class_id == $emp_cls}>
                                    <{assign var="arr_cls_list_selected" value="selected"}>
                                  <{else}>
                                    <{assign var="arr_cls_list_selected" value=""}>
                                  <{/if}>
                                  <option value="<{$arr_cls_list.class_id}>" <{$arr_cls_list_selected}>><{$arr_cls_list.class_nm|escape:"html"}></option>
                                <{/foreach}>
                              </select><{$belong1|escape:"html"}>
                            </font>
                                  
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                              <select name="atrb"  onchange="setDepartmentOptions(this.form.atrb, this.form.dept, this.form.room);">
                              </select><{$belong2|escape:"html"}>
                            </font>
                            
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                              <select name="dept" onchange="setRoomOptions(this.form.dept, this.form.room);"></select>
                              </select><{$belong3|escape:"html"}>
                            </font>

                          <{if $belong_num == "4"}>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                              <select name="room"></select>
                              </select><{$belong4|escape:"html"}>
                            </font>
                          <{/if}>
                          </td>
                        </tr>
                      <{/if}>
                      </table>
                  
                      <table border="0" cellspacing="0" cellpadding="0">
                        <tr><td bgcolor="#F5FFE5"><img src="img/spacer.gif" width="1" height="1" alt=""></td></tr>
                      </table>

                      <table width="620" border="0" cellspacing="0" cellpadding="0">
                        <tr height="22">
                          <td align="right" bgcolor="#F5FFE5">
                            <input type="button" value="更新" onclick="checkForm();">
                          </td>
                        </tr>
                      </table>
                    </td>
                    <td width="1"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                  </tr>
                  <tr>
                    <td colspan="3"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                  </tr>
                </table>
              </td>
              <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
            </tr>
            
            <tr>
              <td><img src="img/r_3.gif" width="10" height="10"></td>
              <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
              <td><img src="img/r_4.gif" width="10" height="10"></td>
            </tr>
          </table>
          
          <input type="hidden" name="session" value="<{$session}>">
          <input type="hidden" name="ok" value="更新">
        </form>
      </td>
    </tr>
  </table>
</body>
</html>
