<?xml version="1.0" encoding="EUC-JP"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
        <meta http-equiv="Content-Script-Type" content="text/javascript" />
        <meta http-equiv="Content-Style-Type" content="text/css" />
        <meta http-equiv="X-UA-Compatible" content="IE=<{$ie|default:'edge'}>" />
        <meta http-equiv="imagetoolbar" content="no" />
        <base href="<{$BASE_HREF}>/" />
        <link rel="stylesheet" type="text/css" href="css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="css/comedix.css" />
        <link rel="stylesheet" type="text/css" href="css/<{$prefix}>.css" />
        <link rel="stylesheet" type="text/css" href="css/smoothness/jquery-ui.custom.css" />
        <title>CoMedix <{$title}></title>
    </head>
