<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/hiyari_el.js"></script>
  <script type="text/javascript">
    //--------------------------------------------------
    //登録対象者配列
    //--------------------------------------------------
    m_target_list = new Array();
    //参照権限の対象職員
    m_target_list[1] = new Array(
        <{foreach name=n from=$arr_target[1] item=i}>
          <{if !$smarty.foreach.n.first}>,<{/if}>
          new user_info('<{$i.id}>','<{$i.name}>')
        <{/foreach}>
      );
    //更新権限の対象職員
    m_target_list[2] = new Array(
        <{foreach name=n from=$arr_target[2] item=i}>
          <{if !$smarty.foreach.n.first}>,<{/if}>
          new user_info('<{$i.id}>','<{$i.name}>')
        <{/foreach}>
      );

    //--------------------------------------------------
    //部署情報配列
    //--------------------------------------------------
    //部
    var classes = [];
    <{foreach from=$sel_class item=i}>
      classes.push({id: '<{$i.class_id}>', name: '<{$i.class_nm}>'});
    <{/foreach}>

    //課
    var atrbs = {};
    <{foreach from=$sel_atrb key=class_id item=c}>
      atrbs['<{$class_id}>'] = [];
      <{foreach from=$c key=atrb_id item=a}>
        atrbs['<{$class_id}>'].push({id: '<{$atrb_id}>', name: '<{$a}>'});
      <{/foreach}>
    <{/foreach}>

    //科
    var depts = {};
    <{foreach from=$sel_dept key=class_id item=c}>
      depts['<{$class_id}>'] = {};
      <{foreach from=$c key=atrb_id item=a}>
        depts['<{$class_id}>']['<{$atrb_id}>'] = [];
        <{foreach from=$a key=dept_id item=d}>
          depts['<{$class_id}>']['<{$atrb_id}>'].push({id: '<{$dept_id}>', name: '<{$d}>'});
        <{/foreach}>
      <{/foreach}>
    <{/foreach}>

    //--------------------------------------------------
    //画面起動時の処理
    //--------------------------------------------------
    function initPage()
    {
        //登録対象者を設定する。
        update_target_html("1");
        update_target_html("2");

        //履歴管理有無の有効制御
        setManageHistoryButtonDisabled();

        //権限部分の初期制御
        onChangeArchive(true, '<{$ref_class_src}>', '<{$ref_atrb_src}>', '<{$upd_class_src}>', '<{$upd_atrb_src}>');
    }
    
    //----------------------------------------------------------------------------------------------------
    // 権限関係以外の画面処理
    //----------------------------------------------------------------------------------------------------
    //ファイルパスを解析し、コンテンツ名・コンテンツタイプをセットします。
    function setFileInfo(path)
    {
        //コンテンツ名にセット
        var fname;
        if (path.indexOf('\\') != -1)
        {
            fname = path.replace(/^.*\\/, '');
        }
        else if (path.indexOf('/') != -1)
        {
            fname = path.replace(/^.*\//, '');
        }
        else
        {
            fname = path;
        }
        var docname = fname.replace(/\.[^.]*$/, '');
        document.mainform.document_name.value = docname;

        //コンテンツタイプにセット
        var ext = fname.replace(/.*\./, '');
        switch (ext)
        {
            case 'doc':
            case 'docx':
                document.mainform.document_type.options[1].selected = true;
                break;
            case 'xls':
            case 'xlsx':
                document.mainform.document_type.options[2].selected = true;
                break;
            case 'ppt':
            case 'pptx':
                document.mainform.document_type.options[3].selected = true;
                break;
            case 'pdf':
                document.mainform.document_type.options[4].selected = true;
                break;
            case 'txt':
                document.mainform.document_type.options[5].selected = true;
                break;
            case 'jpg':
            case 'jpeg':
                document.mainform.document_type.options[6].selected = true;
                break;
            case 'gif':
                document.mainform.document_type.options[7].selected = true;
                break;
            default:
                document.mainform.document_type.options[8].selected = true;
                break;
        }
    }

    //履歴管理有無の有効制御
    function setManageHistoryButtonDisabled()
    {
        //新たにファイルがアップロードされる場合のみ有効
        var disabled = (document.mainform.upfile.value == '');
        document.mainform.manage_history[0].disabled = disabled;
        document.mainform.manage_history[1].disabled = disabled;
    }
  </script>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
  <script type="text/javascript" src="js/yui/build/connection/connection-min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <style type="text/css">
    .list {border-collapse:collapse; border:#5279a5 solid 1px;}
    .list td {border:#5279a5 solid 1px;}
    .list td td {border-width:0;}
    .non_in_list {border-collapse:collapse;}
    .non_in_list td {border:0px;}
  </style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <!-- ヘッダー START -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr height="32" bgcolor="#35B341">
            <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><{$PAGE_TITLE}></b></font></td>
            <td>&nbsp;</td>
            <td width="10">&nbsp;</td>
            <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
          </tr>
        </table>
        <img src="img/spacer.gif" width="10" height="10" alt=""><br>
        <!-- ヘッダー END -->
      </td>
    </tr>
    <tr>
      <td>
        <!-- 本体 START -->
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
            <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
          </tr>
          <tr>
            <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
            <td bgcolor="#F5FFE5">
              <form name="mainform" action="hiyari_el_update_exe.php" method="post" enctype="multipart/form-data">
                <!-- 上部ボタン START -->
                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                  <tr height="22">
                    <td style="vertical-align:bottom;">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10" color="red">
                        ※ファイルを更新したい場合のみ、参照ボタンでファイルを選択してください。<{$upload_max_filesize}>を超えるファイルは登録できません。
                      </font>
                    </td>
                    <td align="right">
                      <input type="button" value="更新" onclick="submitForm();"<{if !$upd_flg}> disabled<{/if}>>
                      <input type="button" value="参照" onclick="<{$refer_onclick}>">
                    </td>
                  </tr>
                </table>
                <!-- 上部ボタン END -->

                <!-- 表全体 START -->
                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                  <!-- ファイル名-履歴管理 START -->
                  <tr height="22">
                    <td align="right" bgcolor="#DFFFDC">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイル</font>
                    </td>
                    <td bgcolor="#FFFFFF">
                      <input type="file" name="upfile" size="50" value="" style="ime-mode:inactive;" onchange="setFileInfo(this.value);setManageHistoryButtonDisabled();">
                    </td>
                    <td align="right" bgcolor="#DFFFDC">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">履歴管理</font>
                    </td>
                    <td bgcolor="#FFFFFF">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                        <input type="radio" name="manage_history" value="t"<{if $manage_history == "t"}> checked<{/if}>>する
                        <input type="radio" name="manage_history" value="f"<{if $manage_history == "f"}> checked<{/if}>>しない
                      </font>
                    </td>
                  </tr>
                  <!-- ファイル名-履歴管理 END -->
                  
                  <!-- コンテンツ名-コンテンツタイプ START -->
                  <tr height="22">
                    <td width="15%" align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コンテンツ名</font></td>
                    <td bgcolor="#FFFFFF"><input name="document_name" type="text" size="50" maxlength="100" value="<{$document_name}>" style="ime-mode:active;"></td>
                    <td width="14%" align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コンテンツタイプ</font></td>
                    <td width="17%" bgcolor="#FFFFFF">
                      <select name="document_type">
                        <option value="0"<{if $document_type == "0"}> selected<{/if}>>
                        <option value="1"<{if $document_type == "1"}> selected<{/if}>>Word
                        <option value="2"<{if $document_type == "2"}> selected<{/if}>>Excel
                        <option value="3"<{if $document_type == "3"}> selected<{/if}>>PowerPoint
                        <option value="4"<{if $document_type == "4"}> selected<{/if}>>PDF
                        <option value="5"<{if $document_type == "5"}> selected<{/if}>>テキスト
                        <option value="6"<{if $document_type == "6"}> selected<{/if}>>JPEG
                        <option value="7"<{if $document_type == "7"}> selected<{/if}>>GIF
                        <option value="99"<{if $document_type == "99"}> selected<{/if}>>その他
                      </select>
                    </td>
                  </tr>
                  <!-- コンテンツ名-コンテンツタイプ END -->

                  <!-- キーワード-コンテンツ番号 START -->
                  <tr height="22">
                    <td align="right" bgcolor="#DFFFDC">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">キーワード</font>
                    </td>
                    <td bgcolor="#FFFFFF">
                      <input name="keywd" type="text" size="50" maxlength="50" value="<{$keywd}>" style="ime-mode:active;">
                    </td>
                    <td align="right" bgcolor="#DFFFDC">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コンテンツ番号</font>
                    </td>
                    <td bgcolor="#FFFFFF">
                      <input type="text" name="lib_no" value="<{$lib_no}>" style="ime-mode:inactive;">
                    </td>
                  </tr>
                  <!-- キーワード-コンテンツ番号 END -->

                  <!-- 説明 START -->
                  <tr>
                    <td align="right" bgcolor="#DFFFDC">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">説明</font>
                    </td>
                    <td colspan="3" bgcolor="#FFFFFF">
                      <textarea name="explain" rows="5" cols="40" style="ime-mode:active;"><{$explain}></textarea>
                    </td>
                  </tr>
                  <!-- 説明 END -->

                  <!-- 保存先 START -->
                  <tr height="22">
                    <td align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">保存先</font></td>
                    <td colspan="3" bgcolor="#FFFFFF">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                        <span id="folder_path" style="padding-right:2px;">
                          <{$cate_nm}>
                          <{foreach name=n from=$folder_path item=fp}>
                            &gt;&nbsp;<{$fp.name}>
                          <{/foreach}>
                        </span>
                        <input type="button" value="選択" onclick="selectFolder('<{$session}>', '<{$path}>');">
                        <input type="hidden" name="archive" value="<{$archive}>">
                        <input type="hidden" name="category" value="<{$category}>">
                        <input type="hidden" name="folder_id" value="<{$folder_id}>">
                      </font>
                    </td>
                  </tr>
                  <!-- 保存先 END -->

                  <!-- 登録者-更新日時 START -->
                  <tr height="22">
                    <td align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録者</font></td>
                    <td bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$reg_emp_name}>（<{$reg_class_nm}>）</font></td>
                    <td align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新日時</font></td>
                    <td bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$upd_time}></font></td>
                  </tr>
                  <!-- 登録者-更新日時 END -->

                  <!-- 参照数-版番号 START -->
                  <tr height="22">
                    <td align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照数</font></td>
                    <td bgcolor="#FFFFFF">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                        <{if $has_admin_auth && $ref_count > 0}>
                          <a href="javascript:void(0);" onclick="window.open('hiyari_el_admin_refer_log_detail.php?session=<{$session}>&lib_id=<{$lib_id}>', 'newwin', 'scrollbars=yes,width=640,height=700');"><{$ref_count}></a>
                        <{else}>
                          <{$ref_count}>
                        <{/if}>
                      </font>
                    </td>
                    <td align="right" bgcolor="#DFFFDC">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">版番号</font>
                    </td>
                    <td bgcolor="#FFFFFF">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                        <{if $edition_no > 1}>
                          <a href="javascript:void(0);" onclick="window.open('hiyari_el_history.php?session=<{$session}>&lib_id=<{$lib_id}>&path=<{$path}>', 'libhist', 'width=640,height=480,scrollbars=yes');"><{$edition_no}></a>
                        <{else}>
                          <{$edition_no}>
                        <{/if}>
                      </font>
                    </td>
                  </tr>
                  <!-- 参照数-版番号 END -->

                  <!-- 参照・更新可能職員指定 -->
                  <{include file="hiyari_el_emp1.tpl"}>               
                </table>
                <!-- 表全体 END -->

                <!-- 下部ボタン START -->
                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                  <tr height="22">
                    <td align="right">
                      <input type="button" value="更新" onclick="submitForm();"<{if !$upd_flg}> disabled<{/if}>>
                      <input type="button" value="参照" onclick="<{$refer_onclick}>">
                    </td>
                  </tr>
                </table>
                <!-- 下部ボタン END -->

                <input type="hidden" name="session" value="<{$session}>">
                <input type="hidden" name="lib_id" value="<{$lib_id}>">
                <input type="hidden" name="a" value="<{$a}>">
                <input type="hidden" name="c" value="<{$c}>">
                <input type="hidden" name="f" value="<{$f}>">
                <input type="hidden" name="o" value="<{$o}>">
                <input type="hidden" name="path" value="<{$path}>">
                <input type="hidden" name="ref_toggle_mode" value="">
                <input type="hidden" name="upd_toggle_mode" value="">
                <input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
                <input type="hidden" id="target_name_list1" name="target_name_list1" value="">
                <input type="hidden" id="target_id_list2"   name="target_id_list2" value="">
                <input type="hidden" id="target_name_list2" name="target_name_list2" value="">
              </form>
            </td>
            <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
          </tr>
          <tr>
            <td><img src="img/r_3.gif" width="10" height="10"></td>
            <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td><img src="img/r_4.gif" width="10" height="10"></td>
          </tr>
        </table>
        <!-- 本体 END -->
      </td>
    </tr>
  </table>
</body>
</html>
