<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/pagination.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
  <script type="text/javascript">
    var w = 1024 + 37;
    var h = window.screen.availHeight;
    window.resizeTo(w, h);

    //感想ユーザーの変更
    function emp_change(emp_id)
    {
        document.list_form.mode.value = "emp_change";
        document.list_form.kansou_emp_id.value = emp_id;
        document.list_form.submit();
    }

    //感想を削除します。
    function delete_kansou()
    {
        if(!confirm('感想を削除します。よろしいですか？'))
        {
            return;
        }

        document.list_form.mode.value = "delete_kansou";
        document.list_form.submit();
    }

    //感想を更新(登録)します。
    function update_kansou()
    {
        if(document.list_form.kansou.value == "")
        {
            alert("感想が入力されていません。");
            return;
        }

        document.list_form.mode.value = "update_kansou";
        document.list_form.submit();
    }
  </script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
</head>

<body>
  <!-------------------------------------------------------------------------
  ヘッダー 
  -------------------------------------------------------------------------->
	<div id="header" class="clearfix">
		<div class="inner" class="clearfix">
			<h1><{$PAGE_TITLE}></h1>
      <p class="action_btn">
        <input type="button" class="button radius_mini"  value="削除" onclick="delete_kansou()">
        <input type="button" class="button radius_mini"  value="更新" onclick="update_kansou()">
      </p>
		</div>
	</div>

  <!-------------------------------------------------------------------------
  コンテンツ
  -------------------------------------------------------------------------->
  <div id="content" class="clearfix">
    <div class="section">
      <form name="list_form" method="post" action="hiyari_el_kansou.php">
        <input type="hidden" name="is_postback" value="true">
        <input type="hidden" name="session" value="<{$session}>">
        <input type="hidden" name="path" value="<{$path}>">
        <input type="hidden" name="base_lib_id" value="<{$base_lib_id}>">
        <input type="hidden" name="mode" value="">
        <input type="hidden" name="kansou_emp_id" value="<{$kansou_emp_id}>">
        <input type="hidden" name="page" value="<{$page}>">
        
        <!-- 入力エリア START -->
        <div class="color02 radius clearfix mB20">
          <!-- 実行ボタンエリア START -->
          <{if $kansou_emp_id != $emp_id}>
            <span id="kansou_emp">感想記入者</span>&nbsp;&nbsp;<{$kansou_emp_name}>
          <{/if}>
          <!-- 実行ボタンエリア END -->
          <textarea name="kansou" style="width:966px;height:200px"><{$kansou}></textarea>
        </div>
        <!-- 入力エリア END -->

        <!-- 一覧エリア START -->
        <{if $rec_count > 0}>
          <div class="color02 radius clearfix mB20">
            <!-- ページングエリア START -->
            <{if $page_max != 1}>
              <{include file="hiyari_paging2.tpl"}>
            <{/if}>
            <!-- ページングエリア END -->
            <table class="list">
              <tr>
                <th>更新日付</th>
                <th>部署</th>
                <th>氏名</th>
                <th class="none">感想</th>
              </tr>
              <{foreach name=l from=$list item=i}>
                <tr>
                  <td>
                    <nobr>
                      <{if $path == "3"}>
                        <a href="javascript:emp_change('<{$i.emp_id}>');">
                          <{$i.upd_time}>
                        </a>
                      <{else}>
                          <{$i.upd_time}>
                      <{/if}>
                    </nobr>
                  </td>
                  <td>
                    <nobr><{$i.classes_nm}></nobr>
                  </td>
                  <td>
                    <nobr><{$i.emp_nm}></nobr>
                  </td>
                  <td class="none">
                    <{$i.kansou}>
                  </td>
                </tr>
              <{/foreach}>
            </table>
          </div>
        <{/if}>
        <!-- 一覧エリア END -->
      </form>
    </div>
  </div>
</body>
</html>
