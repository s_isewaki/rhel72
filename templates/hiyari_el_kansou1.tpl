<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
  <script type="text/javascript" src="js/yui/build/treeview/treeview-min.js"></script>
  <link rel="stylesheet" type="text/css" href="js/yui/build/treeview/css/folders/tree.css">
  <script type="text/javascript">
    //感想ユーザーの変更
    function emp_change(emp_id)
    {
        document.list_form.mode.value = "emp_change";
        document.list_form.kansou_emp_id.value = emp_id;
        document.list_form.submit();
    }

    //感想を削除します。
    function delete_kansou()
    {
        if(!confirm('感想を削除します。よろしいですか？'))
        {
            return;
        }

        document.list_form.mode.value = "delete_kansou";
        document.list_form.submit();
    }

    //感想を更新(登録)します。
    function update_kansou()
    {
        if(document.list_form.kansou.value == "")
        {
            alert("感想が入力されていません。");
            return;
        }

        document.list_form.mode.value = "update_kansou";
        document.list_form.submit();
    }
  </script>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <style type="text/css">
    .list {border-collapse:collapse;}
    .list td {border:#5279a5 solid 1px;}
    .list_in {border-collapse:collapse;}
    .list_in td {border:#FFFFFF solid 0px;}
  </style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <!-- ヘッダー START -->
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr height="32" bgcolor="#35B341">
            <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><{$PAGE_TITLE}></b></font></td>
            <td>&nbsp;</td>
            <td width="10">&nbsp;</td>
            <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
          </tr>
        </table>
        <img src="img/spacer.gif" width="10" height="10" alt=""><br>
      </td>
    </tr>
    <!-- ヘッダー END -->
    
    <!-- 本体 START -->
    <tr>
      <td>
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
            <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
          </tr>
          <tr>
            <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
            <td bgcolor="#F5FFE5">
              <form name="list_form" method="post" action="hiyari_el_kansou.php">
                <input type="hidden" name="is_postback" value="true">
                <input type="hidden" name="session" value="<{$session}>">
                <input type="hidden" name="path" value="<{$path}>">
                <input type="hidden" name="base_lib_id" value="<{$base_lib_id}>">
                <input type="hidden" name="mode" value="">
                <input type="hidden" name="kansou_emp_id" value="<{$kansou_emp_id}>">
                <input type="hidden" name="page" value="<{$page}>">
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                  <!-- 実行ボタンエリア START -->
                  <tr>
                    <td>
                      <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                          <td align="left" nowrap>
                            <{if $kansou_emp_id != $emp_id}>
                              <table border="0" cellspacing="0" cellpadding="2" class="list">
                                <tr>
                                  <td bgcolor="#DFFFDC" width="80">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">感想記入者</font>
                                  </td>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$kansou_emp_name}></font>
                                  </td>
                                </tr>
                              </table>
                            <{else}>
                              &nbsp;
                            <{/if}>
                          </td>
                          <td align="right" nowrap>
                            <input type="button" value="削除" onclick="delete_kansou()">
                            <input type="button" value="更新" onclick="update_kansou()">
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <!-- 実行ボタンエリア END -->

                  <!-- 入力エリア START -->
                  <tr>
                    <td>
                      <textarea name="kansou" style="width:100%;height:200px"><{$kansou}></textarea>
                    </td>
                  </tr>
                  <!-- 入力エリア END -->

                  <!-- ページングエリア START -->
                  <tr>
                    <td>
                      <{if $page_max != 1}>
                        <{include file="hiyari_paging1.tpl"}>
                      <{else}>
                        <table width="100%">
                          <tr>
                            <td>
                              <table>
                                <tr>
                                  <td>
                                    &nbsp;
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      <{/if}>
                    </td>
                  </tr>
                  <!-- ページングエリア END -->

                  <!-- 一覧エリア START -->
                  <tr>
                    <td>
                      <{if $rec_count > 0}>
                        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                          <tr height="22" bgcolor="#DFFFDC">
                            <td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新日付</font></td>
                            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
                            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名</font></td>
                            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">感想</font></td>
                          </tr>
                          <{foreach from=$list item=i}>
                            <tr height="22" bgcolor="#FFFFFF">
                              <td>
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                  <{if $path == "3"}>
                                    <a href="javascript:emp_change('<{$i.emp_id}>');">
                                      <{$i.upd_time}>
                                    </a>
                                  <{else}>
                                      <{$i.upd_time}>
                                  <{/if}>
                                </font>
                              </td>
                              <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$i.classes_nm}></font></td>
                              <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$i.emp_nm}></font></td>
                              <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$i.kansou}></font></td>
                            </tr>
                          <{/foreach}>
                        </table>
                      <{/if}>
                    </td>
                  </tr>
                  <!-- 一覧エリア END -->
                </table>
              </form>
            </td>
            <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
          </tr>
          <tr>
            <td><img src="img/r_3.gif" width="10" height="10"></td>
            <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td><img src="img/r_4.gif" width="10" height="10"></td>
          </tr>
        </table>
      </td>
    </tr>
    <!-- 本体 END -->
  </table>
</body>
</html>
