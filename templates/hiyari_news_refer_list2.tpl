<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
  <title><{$INCIDENT_TITLE}> | お知らせ参照者一覧</title>
	<meta http-equiv="imagetoolbar" content="no" />
	<link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/news_menu.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/pagination.css" />
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
</head>

<body id="top">
  <{*ヘッダー*}>
  <{include file="hiyari_header2.tpl"}>
  
	<div id="content" class="clearfix <{$tab_info.total_width_class}>">
		<div id="sub" class="clearfix">
			<div id="sub_menu" class="color01 radius">
				<h2><img src="img/side_nav_txt01.gif" alt="MENU" /></h2>
				<ul>
					<li><a href="hiyari_news_menu.php?session=<{$session}>&page=<{$menu_page}>">お知らせ管理</a></li>
          <li><a href="hiyari_news_register.php?session=<{$session}>">お知らせ登録</a></li>
          <li class="active none"><a href="#">参照者一覧</a></li>
				</ul>
			</div>
    </div>
    
    <div id="main" class="<{$tab_info.main_width_class}>">
      <div id="ref_info" class="color03 radius">
        <table>
          <tr>
            <th>タイトル</th>
            <td colspan="5">
              <span><{$news_title}></span>
            </td>
          </tr>
          <tr>
            <th width="17%" class="none">お知らせ対象者数</th>
            <td width="16%" class="none">
              <span><{$count_all}>名</span>
            </td>
            <th width="17%" class="none">参照済み人数</th>
            <td width="16%" class="none">
              <span>
                <{if $is_referred}>
                  <b><{$count_referred}>名</b>
                <{else}>
                  <a href="hiyari_news_refer_list.php?session=<{$session}>&news_id=<{$news_id}>&=1&menu_page=<{$menu_page}>"><{$count_referred}>名</a>
                <{/if}>
              </span>
            </td>
            <th width="17%" class="none">残人数</th>
            <td width="16%" class="none">
              <span>            
                <{if $is_referred}>
                  <a href="hiyari_news_refer_list.php?session=<{$session}>&news_id=<{$news_id}>&ref_mode=2&menu_page=<{$menu_page}>"><{$count_unreferred}>名</a>
                <{else}>
                  <b><{$count_unreferred}>名</b>
                <{/if}>
              </span>
            </td>             
          </tr>
        </table>
      </div>
      
      <div id="main_list" class="color02 radius mT10">
        <table class="list">
          <tr>
            <th width="33%">職員ID</th>
            <th width="33%">職員氏名</th>
            <th width="33%" class="none">参照日時</th>
          </tr>
          <{foreach from=$referrer_list item=i}>
            <tr>
              <td><{$i.emp_id|escape:"html"}></td>
              <td>
                <span class="ellipsis">
                  <{$i.emp_name|escape:"html"}>
                </span>
              </td>
              <td class="none"><{$i.ref_time|escape:"html"}></td>
            </tr>
          <{/foreach}>
        </table>
        <form name="list_form" action="hiyari_news_refer_list.php" method="post">
          <{include file="hiyari_paging2.tpl"}>
          <input type="hidden" name="session" value="<{$session}>">
          <input type="hidden" name="ref_mode" value="<{$ref_mode}>">
          <input type="hidden" name="news_id" value="<{$news_id}>">
          <input type="hidden" name="menu_page" value="<{$menu_page}>">
          <input type="hidden" name="mode" value="<{$mode}>">
          <input type="hidden" name="page" value="<{$page}>">
        </form>
      </div>
    </div>
  </div>
	<div id="footer">
		<p>Copyright Comedix all right reserved.</p>
	</div>
</body>
</html>

