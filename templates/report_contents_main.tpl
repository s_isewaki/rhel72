  <div id="report_contents_for_analysis" class="section tabletype01">
    <div id="report_contents_header" class="clearfix">
      <h2 class="type03">報告内容</h2>
      <div id="window_switch_btn2">
        <a id="show_in_another_window_btn2" class="radius_mini" href="javascript:show_report_contents_in_another_window();">報告内容を別ウィンドウで表示</a>
        <a id="back_to_analysis_window_btn" class="radius_mini" href="javascript:back_to_analysis_window();">評価画面に表示</a>
      </div>
    </div>
    
    <{foreach from=$rpt_data item=cate}>
      <div class="color03 radius">
        <h3>● <span class="strong"><{$cate.name}></span></h3>
        <table class="v_title">
          <{foreach from=$cate.list item=i name=n}>
            <tr>
              <{if isset($i.grp_title)}>
                <th colspan="2"><{$i.grp_title}></th>
              <{else}>
                <th <{if $smarty.foreach.n.last}>class="last"<{/if}> width="188"><{$i.title}></th>
                <td <{if $smarty.foreach.n.last}>class="last"<{/if}>>
                  <{$i.content}>
                  <{foreach from=$i.others item=other_content}>
                    （<{$other_content}>）
                  <{/foreach}>
                </td>
              <{/if}>
            </tr>
          <{/foreach}>
        </table>
      </div>      
      <span class="lineWhite">　</span>
    <{/foreach}>
    <p class="pagetop"><a href="#top">ページの先頭へ</a></p>
  </div>
