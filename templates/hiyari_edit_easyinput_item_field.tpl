<{*
インターフェース
    $grps
        array   項目情報配列
    $vals
        array   値の配列
    $grp_flags
        array   レポートに使用する項目グループ
    $i
        string  グループコード
    $j
        string  アイテムコード
    $style
        string  "textarea"に対する書式指定。"big"、"genmensoti"、"kaizensaku"
    $checkbox_line_count
        integer "checkbox"に対する一行辺りの表示項目数。省略時は4。
    $radio_line_count
        integer "radio"に対する一行辺りの表示項目数。省略時は4。
    $comp
        string  "select"の選択値を判定する際に項目名を判定するか項目コードを判定するかを決定する。"name"の場合は項目名判定。
                $compが設定されるケースは現状ない。(廃止可能)
    $text_field_width
        string  "text"のwidth値。省略時は250。
    $level_infos
        array   インシレベルとメッセージの配列
    $item_element_no_disp
        array   項目表示情報配列
    $add_tag_element
        string  "text"のみ対応。追加するタグ属性。

*}>
<{if $text_field_width == ""}>
    <{assign var="text_field_width" value="250"}>
<{/if}>


<{if $i == 90 && $j == 10}>
    <select name="_<{$i}>_<{$j}>" onchange="selectbox_onchange(this);">
    <option value="">
    <{section name=k loop=$level_infos}><{if $level_infos[k].use_flg}><option value="<{$level_infos[k].easy_code}>" <{if in_array($level_infos[k].easy_code, (array)@$vals[$i][$j])}> selected<{/if}>><{$level_infos[k].easy_name|escape:"html"}><{/if}><{/section}>
    </select>

<{* 発生場所・詳細 *}>
<{elseif $i == 110 && $j == 65}>
    <script type="text/javascript">
        function area_110_65_disp_change(this_obj) {
            switch (this_obj.value) {
            <{foreach from=$grps_115 key=item_id item=list_115}>
            case '<{$item_id}>':
                document.getElementById('span_110_65').innerHTML = '<select name="_<{$i}>_<{$j}>"><option value=""></option><{foreach from=$list_115 key=code item=name}><option value="<{$code|escape:"javascript"}>"><{$name|escape:"javascript"}></option><{/foreach}></select>';
                break;
            <{if $item_id==$vals[110][60][0]}>
                <{assign var='item_list_115' value=$list_115}>
            <{/if}>
            <{/foreach}>
            default:
                document.getElementById('span_110_65').innerHTML =
                    '<input type="text" id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" style="width:<{$text_field_width}>;ime-mode:<{$ime}>" <{$add_tag_element}> >';
                break;
            }
        }
    </script>
	
    <span id="span_110_65">
    <{if $item_list_115}>
    <select name="_<{$i}>_<{$j}>">
        <option value=""></option>
        <{foreach from=$item_list_115 key=code item=name}>
            <option value="<{$code}>" <{if $code==$vals[110][65][0]}>selected<{/if}>><{$name|escape:"html"}></option>
        <{/foreach}>
    </select>
    <{else}>
    <input type="text" id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" value="<{$vals[$i][$j][0]}>" style="width:<{$text_field_width}>;ime-mode:<{$ime}>" <{$add_tag_element}> >
    <{/if}>
    </span>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'select' && $i == 700 && $j == 10}>
    <select id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" onchange="disp_hide_scene_content();cng_scene(this.form._700_10.value);cng_kind(this.form._700_10.value);cng_content(this.form._700_10.value);disp_item(this.form._700_10.value);reset_item();disp_kind();<{if in_array($grps[$i].easy_item_list[$j].relate_group, $grp_flags)}>relate_detail(this,'rel_<{$grps[$i].easy_item_list[$j].relate_group}>');<{/if}>"
       >
    <option value=""></option>
    <{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}><{if $item_element_no_disp[$i][$j] == "" || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}><option value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>"<{if ($comp != "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code, (array)@$vals[$i][$j])) || ($comp == "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_name, (array)@$vals[$i][$j]))}> selected<{/if}>><{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}><{/if}><{/section}>
    </select>

<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'select' && $i == 710 && $j == 10}>
    <select id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" onchange="cng_scene_item(this.form._710_10.value);<{if in_array($grps[$i].easy_item_list[$j].relate_group, $grp_flags)}>relate_detail(this,'rel_<{$grps[$i].easy_item_list[$j].relate_group}>');<{/if}>"
       >
    <option value=""></option>
    <{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}><{if $item_element_no_disp[$i][$j] == "" || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}><option value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>"<{if ($comp != "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code, (array)@$vals[$i][$j])) || ($comp == "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_name, (array)@$vals[$i][$j]))}> selected<{/if}>><{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}><{/if}><{/section}>
    </select>

<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'select' && $i == 740 && $j == 10}>
    <select id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" onchange="cng_kind_item(this.form._740_10.value);<{if in_array($grps[$i].easy_item_list[$j].relate_group, $grp_flags)}>relate_detail(this,'rel_<{$grps[$i].easy_item_list[$j].relate_group}>');<{/if}>"
       >
    <option value=""></option>
    <{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}><{if $item_element_no_disp[$i][$j] == "" || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}><option value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>"<{if ($comp != "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code, (array)@$vals[$i][$j])) || ($comp == "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_name, (array)@$vals[$i][$j]))}> selected<{/if}>><{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}><{/if}><{/section}>
    </select>

<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'select' && $i == 770 && $j == 10}>
    <select id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" onchange="cng_content_item(this.form._770_10.value);<{if in_array($grps[$i].easy_item_list[$j].relate_group, $grp_flags)}>relate_detail(this,'rel_<{$grps[$i].easy_item_list[$j].relate_group}>');<{/if}>"
       >
    <option value=""></option>
    <{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}><{if $item_element_no_disp[$i][$j] == "" || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}><option value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>"<{if ($comp != "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code, (array)@$vals[$i][$j])) || ($comp == "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_name, (array)@$vals[$i][$j]))}> selected<{/if}>><{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}><{/if}><{/section}>
    </select>

<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'select' && $i == 900 && $j == 10}>
    <select id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" onchange="reset_kind_2010();hide_must_2010();disp_hide_scene_content_2010();cng_scene_2010(this.form._900_10.value);cng_kind_2010(this.form._900_10.value);cng_content_2010(this.form._900_10.value);disp_item_2010(this.form._900_10.value);reset_item_2010();disp_kind_2010();delete_detail_item_2010(this.form);<{if in_array($grps[$i].easy_item_list[$j].relate_group, $grp_flags)}>relate_detail(this,'rel_<{$grps[$i].easy_item_list[$j].relate_group}>');<{/if}>"
       >
    <option value=""></option>
    <{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}><{if $item_element_no_disp[$i][$j] == "" || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}><option value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>"<{if ($comp != "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code, (array)@$vals[$i][$j])) || ($comp == "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_name, (array)@$vals[$i][$j]))}> selected<{/if}>><{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}><{/if}><{/section}>
    </select>




	<{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}>
		<{if $item_element_no_disp[$i][$j] == "" || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}>
			<input type="hidden" name="_<{$i}>_<{$j}>_<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>_hdn" value="<{$grps[$i].easy_item_list[$j].easy_list[k].export_code}>">
		<{/if}>
	<{/section}>




<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'select' && $i == 910 && $j == 10}>
    <select id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" onchange="cng_kind_item_2010(this.form._910_10.value);<{if in_array($grps[$i].easy_item_list[$j].relate_group, $grp_flags)}>relate_detail(this,'rel_<{$grps[$i].easy_item_list[$j].relate_group}>');<{/if}>"
       >
    <option value=""></option>
    <{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}><{if $item_element_no_disp[$i][$j] == "" || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}><option value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>"<{if ($comp != "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code, (array)@$vals[$i][$j])) || ($comp == "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_name, (array)@$vals[$i][$j]))}> selected<{/if}>><{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}><{/if}><{/section}>
    </select>

<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'select' && $i == 940 && $j == 10}>
    <select id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" onchange="cng_scene_item_2010(this.form._940_10.value);cng_item_2010(this.form);<{if in_array($grps[$i].easy_item_list[$j].relate_group, $grp_flags)}>relate_detail(this,'rel_<{$grps[$i].easy_item_list[$j].relate_group}>');<{/if}>"
       >
    <option value=""></option>
    <{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}><{if $item_element_no_disp[$i][$j] == "" || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}><option value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>"<{if ($comp != "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code, (array)@$vals[$i][$j])) || ($comp == "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_name, (array)@$vals[$i][$j]))}> selected<{/if}>><{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}><{/if}><{/section}>
    </select>

<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'select' && $i == 970 && $j == 10}>
    <select id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" onchange="cng_content_item_2010(this.form._970_10.value);<{if in_array($grps[$i].easy_item_list[$j].relate_group, $grp_flags)}>relate_detail(this,'rel_<{$grps[$i].easy_item_list[$j].relate_group}>');<{/if}>"
       >
    <option value=""></option>
    <{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}><{if $item_element_no_disp[$i][$j] == "" || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}><option value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>"<{if ($comp != "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code, (array)@$vals[$i][$j])) || ($comp == "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_name, (array)@$vals[$i][$j]))}> selected<{/if}>><{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}><{/if}><{/section}>
    </select>

<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'select' && $i == 3020 && $j == 10}>
    <select id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>">
    <option value=""></option>
    <{section name=k loop=$grps[3050].easy_item_list[30].easy_list}><{if $item_element_no_disp[3050][30] == "" || !in_array($grps[3050].easy_item_list[30].easy_list[k].easy_code,$item_element_no_disp[3050][30])}><option value="<{$grps[3050].easy_item_list[30].easy_list[k].easy_code}>"<{if (in_array($grps[3050].easy_item_list[30].easy_list[k].easy_code, (array)@$vals[3020][10]))}> selected<{/if}>><{$grps[3050].easy_item_list[30].easy_list[k].easy_name|escape:"html"}><{/if}><{/section}>
    </select>

<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'select'}>
    <select id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" onchange="selectbox_onchange(this);<{if in_array($grps[$i].easy_item_list[$j].relate_group, $grp_flags)}>relate_detail(this,'rel_<{$grps[$i].easy_item_list[$j].relate_group}>');<{/if}><{if $i==3000 && $j==10 && $gamen_mode != 'update'}>getDiscovererJob();<{/if}>"
       >
    <option value="">
    <{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}><{if $item_element_no_disp[$i][$j] == "" || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}><option value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>"<{if ($comp != "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code, (array)@$vals[$i][$j])) || ($comp == "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_name, (array)@$vals[$i][$j]))}> selected<{/if}>><{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}><{/if}><{/section}>
    </select>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'text'}>
    <input type="text" id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" value="<{$vals[$i][$j][0]}>" style="width:<{$text_field_width}>;ime-mode:<{$ime}>" <{$add_tag_element}> >
    <{if ($i == "440" && $j == "43") || ($i == "460" && $j == "43") || ($i == "470" && $j == "43") || ($i == "430" && $j == "42") || ($i == "450" && $j == "42") || ($i == "460" && $j == "52") || ($i == "470" && $j == "52") || ($i == "480" && $j == "42") }>
    <font class="j10" color="#FF0000">(西暦年４桁と月数字２桁を入力してください。例&nbsp;200805&nbsp;&nbsp;月が不明の場合&nbsp;例&nbsp;2008)</font>
    <{/if}>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'textarea'}>
    <textarea style="width:100%;ime-mode:<{$ime}>" rows="<{if $style == "big"}>12<{elseif $style == "genmensoti" || $style == "kaizensaku" }>10<{else}>4<{/if}>" name="_<{$i}>_<{$j}>"><{$vals[$i][$j][0]}></textarea>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'checkbox'}>
    <{if count($grps[$i].easy_item_list[$j].easy_list) == 1}>
        <input type='checkbox' id="id_<{$i}>_<{$j}>_0" name="_<{$i}>_<{$j}>[]" value="<{$grps[$i].easy_item_list[$j].easy_list[0].easy_code}>" onclick="checkbox_onclick(this)"
        <{if in_array($grps[$i].easy_item_list[$j].easy_list[0].easy_code, (array)@$vals[$i][$j])}> checked<{/if}>>
        <label for="id_<{$i}>_<{$j}>_0"><font class="j12"><{$grps[$i].easy_item_list[$j].easy_list[0].easy_name|escape:"html"}></font></label>
    <{else}>
        <{if $checkbox_line_count == ""}>
            <{assign var="checkbox_line_count" value="4"}>
        <{/if}>
        <table width="100%" cellspacing="0" cellpadding="2" class="list">
        <tr height="22">
        <{php}>
            $checkbox_col_index = 0;
        <{/php}>
        <{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}>
            <{if $item_element_no_disp[$i][$j] == "" || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}>
                <{php}>
                    $checkbox_col_index++;
                    if($checkbox_col_index == $this->_tpl_vars['checkbox_line_count']+1)
                    {
                        $checkbox_col_index=1;
                <{/php}>
                        </tr><tr>
                <{php}>
                    }
                
                <{/php}>
    <td style="width:25%">
        <input type='checkbox' id="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>" name="_<{$i}>_<{$j}>[]" value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>" onclick="checkbox_onclick(this)"
        <{if in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code, (array)@$vals[$i][$j])}> checked<{/if}>>
        <label for="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>"><font class="j12"><{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}></font></label>
    </td>
            <{/if}>
        <{/section}>
        </tr>
        </table>
    <{/if}>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'cb_1'}>
    <input type='checkbox' id="id_<{$i}>_<{$j}>_0" name="_<{$i}>_<{$j}>[]" value="<{$grps[$i].easy_item_list[$j].easy_list[0].easy_code}>"
    <{if in_array($grps[$i].easy_item_list[$j].easy_list[0].easy_code, (array)@$vals[$i][$j])}> checked<{/if}>>
    <label for="id_<{$i}>_<{$j}>_0"><font class="j12"><{$grps[$i].easy_item_list[$j].easy_list[0].easy_name|escape:"html"}></font></label>

<{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'radio'}>
    <{if $radio_line_count == ""}>
        <{assign var="radio_line_count" value="4"}>
    <{/if}>
    <table width="100%" cellspacing="0" cellpadding="2" class="list">
    <tr>
        <td style="width:25%">
        <input type='radio' id="id_<{$i}>_<{$j}>_NON" name="_<{$i}>_<{$j}>[]" value="" onclick="radio_onclick(this)"
        <{if (array)@$vals[$i][$j] == ""}> checked<{/if}>
        <{if in_array($grps[$i].easy_item_list[$j].relate_group, $grp_flags)}> onchange="relate_detail(this,'rel_<{$grps[$i].easy_item_list[$j].relate_group}>');"<{/if}>>
        <label for="id_<{$i}>_<{$j}>_NON"><font class="j12">未入力</font></label>
        </td>
    <{php}>
        $radio_col_index = 1;
    <{/php}>
    <{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}>
        <{if $item_element_no_disp[$i][$j] == "" || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}>
            <{php}>
                $radio_col_index++;
                if($radio_col_index == $this->_tpl_vars['radio_line_count']+1)
                {
                    $radio_col_index=1;
            <{/php}>
    </tr><tr>
            <{php}>
                }
            
            <{/php}>
            <{assign var="easy_title" value='$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}>

    <td style="width:25%">
        <input type='radio' id="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>" name="_<{$i}>_<{$j}>[]" value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>"
onclick="radio_onclick(this);<{*<{if $i==125 && $j==85}>change_title_125(this, '<{$easy_title}>')<{/if}>*}>"
            <{if in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code, (array)@$vals[$i][$j])}> checked<{/if}>
            <{if in_array($grps[$i].easy_item_list[$j].relate_group, $grp_flags)}> onchange="relate_detail(this,'rel_<{$grps[$i].easy_item_list[$j].relate_group}>');"<{/if}>>
            <label for="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>"><font class="j12"><{$easy_title}></font></label>
            <{if $i==125 && $j==85}><input type="hidden" id="item_125_85_<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>" value="<{$easy_title}>"><{/if}>
    </td>
        <{/if}>
    <{/section}>
    </tr>
    </table>
<{/if}>

