<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | メッセージ表示</title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <style>
    div#content table.tabletype08 span {display: inline;padding: 0px;}
  </style>
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
  <script type="text/javascript">    
    var w = 1024 + 37;
    var h = window.screen.availHeight;
    window.resizeTo(w, h);
  
    $(function(){
        //チェックボックス
        $('.checkbox').click(function() {
            var wrap = $(this).closest('.checkbox_wrap');
            wrap.toggleClass('checked');
            wrap.closest('li').toggleClass('checked');
        })
    })
    
    function openerReload() {
        if(window.opener && !window.opener.closed && window.opener.reload_page) {
            window.opener.reload_page();
        }
    }

    // 確認
    function set_progress_end_date()
    {
      <{if $progress_comment_use_flg && $is_evaluation_use == false }>
        var url = "hiyari_rp_progress_comment_input.php?session=<{$session}>&report_id=<{$report_id}>&mail_eis_id=<{$send_eis_id}>";

        var h = window.screen.availHeight - 30;
        var w = window.screen.availWidth - 10;
        var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

        window.open(url, '_blank',option);
      <{/if}>
	    location.href = "hiyari_rp_mail_disp.php?session=<{$session}>&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>&mode=update";
    }
    
    //評価
    function rp_action_analysis(){
      location.href = "hiyari_easyinput.php?session=<{$session}>&gamen_mode=analysis&report_id=<{$report_id}>&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>&job_id=<{$sel_job}>&eis_no=<{$sel_eis_no}>";
    }

    // 返信
    function rp_action_return()
    {
        location.href = "hiyari_rp_mail_input.php?session=<{$session}>&mode=return&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>";
    }
    //転送
    function rp_action_forward()
    {
        location.href = "hiyari_rp_mail_input.php?session=<{$session}>&mode=forward&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>";
    }
    //編集
    function rp_action_update()
    {
        location.href = "hiyari_easyinput.php?session=<{$session}>&gamen_mode=update&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>&report_id=<{$report_id}>";
    }
    //進捗登録
    function rp_action_progress()
    {
        location.href = "hiyari_rp_progress.php?session=<{$session}>&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>&report_id=<{$report_id}>";
    }
    //ラベル登録
    function rp_action_label(is_header)
    {
        <{if $label_edit_flg}>
            if (is_header){
              var pos = $('#label_header').offset();
              var x = pos.left - 50;
              var y = pos.top + 50;
              $('#label_form').css({'left': x + 'px', 'top': y + 'px'});
            }
            else{
              var pos = $('#label_footer').offset();
              var x = pos.left - 50;
              var y = pos.top - $('#label_form').height() - 5;
              $('#label_form').css({'left': x + 'px', 'top': y + 'px'});              
            }
            $('#label_form').toggle('fast');
            $('#b_scrollbar').tinyscrollbar();
        <{else}>
            alert('報告書がゴミ箱に入っているため、ラベルの登録はできません。');
        <{/if}>      
    }
    function regist_label()
    {
        $('#label_form').css('display', 'none');
        
        $.post(
            "hiyari_report_classification.php",
            $("#label_form").serialize(),
            function(data, status) {
                alert(data);
            }
        );
    }
    //印刷
    function rp_action_print()
    {
        var sh = window.screen.availHeight - 30;
        var sw = window.screen.availWidth - 10;
        var h = 100;
        var w = 260;
        var t = (sh-h)/2;
        var l = (sw-w)/2

        var url = "hiyari_print_select.php?session=<{$session}>&report_id=<{$report_id}>&mail_id=<{$mail_id}>&eis_id=<{$eis_id}>";
        var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,left=" + l + ",top=" + t + ",width=" + w + ",height=" + h;

        window.open(url, '_blank',option);
    }
    
    //公開
    function rp_action_open()
    {
        location.href = "hiyari_rp_open_input.php?session=<{$session}>&mode=open_mail&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>&eis_no=<{$sel_eis_no}>";
    }
    
    // メッセージ内の×ボタンクリックでメッセージを非表示にする
    $(function() {
        <{if $mode==='update'}>
            $('.alert').fadeIn(1000).delay(2000).fadeOut(2000);
        <{/if}>
        $('.alert .close').on('click', function() {
            $(this).parents('.alert').hide();
        });
    });
</script>
</head>

<body id="top" class="houkoku" onload="openerReload();">
  <!-- メッセージ -->
	<div id="header">
		<div class="inner">
			<h1><img src="img/txt_img03.gif" alt="メッセージ表示" /></h1>
      <div class="color03 alert alert-info">
          <p>
            報告書の進捗を確認済に変更しました。
            <button type="button" class="close" data-dismiss="alert">×</button>
          </p>          
      </div>
	<ul class="sub_nav clearfix">
        <!--<li class="sub_nav_edit"><a href="javascript:rp_action_open();">編集</a></li>-->
			
        <{if $report_progress_updatable_flg || (!$is_evaluation_use && $progress_comment_use_flg)}>
          <li class="<{if $has_comment}>sub_nav_edit_comment<{else}>sub_nav_confirm<{/if}>">
            <a href="javascript:set_progress_end_date();"><{$disp_comment}></a>
          </li>
        <{elseif $is_evaluation_use}>
          <li class="sub_nav_analysis">
            <a href="javascript:rp_action_analysis();">評価</a>
          </li>
        <{/if}>
        
        <{if $mail_id != ""}>
          <{if $mail_id_mode == "recv" && $arr_btn_flg.return_btn_show_flg == "t"}>
            <li class="sub_nav_replay"><a href="javascript:rp_action_return();">返信</a></li>
          <{/if}>

          <{if $arr_btn_flg.forward_btn_show_flg == "t"}>
            <li class="sub_nav_forward"><a href="javascript:rp_action_forward();">転送</a></li>
          <{/if}>
        <{/if}>
        
        <{if $report_edit_flg && $report_torey_update_flg}>
          <li class="sub_nav_edit"><a href="javascript:rp_action_update();">編集</a></li>
        <{/if}>

        <{if $progres_edit_flg}>
          <li class="sub_nav_progress"><a href="javascript:rp_action_progress();">進捗登録</a></li>
        <{/if}>

        <{if $label_use_flg}>
          <li id="label_header" class="sub_nav_label"><a href="javascript:rp_action_label(true);">ラベル登録</a></li>
        <{/if}>
        
        <{if $arr_btn_flg.print_btn_show_flg == "t"}>
          <li class="sub_nav_print"><a href="javascript:rp_action_print();">印刷</a></li>          
        <{/if}>
			</ul>
		</div>
	</div>
  
  <div id="content" class="clearfix">
    <form name="mainform" action="hiyari_mail_disp.php" method="post">
      <input type="hidden" name="mail_id" value="<{$mail_id}>">
      <input type="hidden" name="mail_id_mode" value="<{$mail_id_mode}>">
      <input type="hidden" name="report_id" value="<{$report_id}>">

    <!-- メッセージヘッダー -->
      <div class="section">
        <div class="color01 radius mB20 clearfix">
          <table class="tabletype08">
            <tr>
              <th width="188">件名</th>
              <td <{$marker_style}>><{$subject|escape}></td>
            </tr>
            <tr>
              <th>送信者</th>
              <td><{$send_emp_name}></td>
            </tr>
            <tr>
              <th>報告部署</th>
              <td><{$send_emp_class_name}></td>
            </tr>
            <tr>
              <th>日付</th>
              <td><{$send_date}></td>
            </tr>
            <tr>
              <th <{if isset($mail_message)}>class="last"<{/if}>>あて先</th>
              <td <{if isset($mail_message)}>class="last"<{/if}>><{$recv_name}></td>
            </tr>
            <{if !isset($mail_message)}>
              <tr>
                <th class="last">内容</th>
                <td class="last">
                  <{$last_message}>
                </td>
              </tr>
            <{/if}>
          </table>
				</div>
			</div>
 
    <{if isset($progress_comment_data)}>
    <div class="section tabletype01">
      <h2 class="type03">コメント</h2>
      <{foreach from=$progress_comment_data key="auth" item="info"}>
        <span class="auth_name"><{$info.auth_name|escape}></span>
        <div id="progress_comment" class="color03 radius">
          <table class="list">
            <tr>
              <th class="lineB" width="70">入力日付</th>
              <th class="lineB" width="104">担当者</th>
              <th class="none">コメント</th>
            </tr>
            <{foreach from=$info.data item="row" name="n"}>
              <tr>
                <td class="repeatY<{if $smarty.foreach.n.last}> lineB<{/if}>">
                  <{$row.date|escape}>
                </td>
                <td class="repeatY<{if $smarty.foreach.n.last}> lineB<{/if}>">
                  <{$row.emp_name|escape}>
                </td>
                <td class="none<{if $smarty.foreach.n.last}> lineB<{/if}>">
                  <{$row.comment|escape|nl2br}>
                </td>
              </tr>
            <{/foreach}>
          </table>
        </div>
      <{/foreach}>
      
      <p class="pagetop"><a href="#top">ページの先頭へ</a></p>
    </div>
  <{/if}>
      
      <{include file="hiyari_mail_contents.tpl"}>
      
    </form>
  </div>
  
	<div id="footer">
		<div class="inner">
			<ul class="sub_nav clearfix">
        <{if $report_progress_updatable_flg || (!$is_evaluation_use && $progress_comment_use_flg)}>
          <li class="<{if $has_comment}>sub_nav_edit_comment<{else}>sub_nav_confirm<{/if}>">
            <a href="javascript:set_progress_end_date();"><{$disp_comment}></a>
          </li>
        <{elseif $is_evaluation_use}>
          <li class="sub_nav_analysis">
            <a href="javascript:rp_action_analysis();">評価</a>
          </li>
        <{/if}>
        
        <{if $mail_id != ""}>
          <{if $mail_id_mode == "recv" && $arr_btn_flg.return_btn_show_flg == "t"}>
            <li class="sub_nav_replay"><a href="javascript:rp_action_return();">返信</a></li>
          <{/if}>

          <{if $arr_btn_flg.forward_btn_show_flg == "t"}>
            <li class="sub_nav_forward"><a href="javascript:rp_action_forward();">転送</a></li>
          <{/if}>
        <{/if}>
        
        <{if $report_edit_flg && $report_torey_update_flg}>
          <li class="sub_nav_edit"><a href="javascript:rp_action_update();">編集</a></li>
        <{/if}>

        <{if $progres_edit_flg}>
          <li class="sub_nav_progress"><a href="javascript:rp_action_progress();">進捗登録</a></li>
        <{/if}>

        <{if $label_use_flg}>
          <li id="label_footer" class="sub_nav_label"><a href="javascript:rp_action_label(false);">ラベル登録</a></li>
        <{/if}>
        <{if $arr_btn_flg.print_btn_show_flg == "t"}>
          <li class="sub_nav_print"><a href="javascript:rp_action_print();">印刷</a></li>          
        <{/if}>
			</ul>
    </div>
  </div>
  
  <{if $label_use_flg}>
    <form id="label_form">
      <input type="hidden" name="session" value="<{$session}>">
      <input type="hidden" name="mode" value="label"> 
      <input type="hidden" name="report_id" value="<{$report_id}>">
      <div id="label" class="color01 radius">
        <div id="b_scrollbar">
          <div class="scrollbar"><div class="track radius"><div class="thumb radius"><div class="end"></div></div></div></div>
          <div class="viewport inner radius_mini">
            <div class="overview">
              <ul class="radius_mini">
                <{foreach from=$folders item=fld}>
                  <li class="<{if !$fld.is_common}>person<{/if}><{if $fld.is_selected}> checked<{/if}>">
                    <span class="checkbox_wrap<{if $fld.is_selected}> checked<{/if}>">
                      <input type="checkbox" class="checkbox" value="<{$fld.id}>" name="selected_folder[]" <{if $fld.is_selected}>checked="checked"<{/if}>>
                    </span>
                    <{$fld.name}>
                  </li>
                <{/foreach}>
              </ul>            
            </div>
          </div>
        </div>
        <div>
          <input type="button" class="button radius_mini" value="登録" onclick="regist_label();">
        </div>
      </div>
    </form>  
  <{/if}>
</body>
</html>