<!-- 参照可能範囲の指定有無 START -->
<tr height="22">
  <td bgcolor="#DFFFDC" colspan="4" class="spacing">
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
      <span style="cursor:pointer;" onclick="toggle1(document.getElementById('ref_toggle'));">参照可能範囲の指定&nbsp;</span>
      <span id="ref_toggle" style="cursor:pointer;" onclick="toggle1(this);"><{$ref_toggle_mode}></span>
    </font>
  </td>
</tr>
<!-- 参照可能範囲の指定有無 END -->

<!-- 参照部署役職の指定 START -->
<tr height="22" id="ref_toggle1" style="display:<{$ref_toggle_display}>;">
  <td bgcolor="#DFFFDC" rowspan="2" align="center">
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署・役職<br>
      <input type="checkbox" name="ref_dept_st_flg" value="t" <{if $ref_dept_st_flg == "t"}>checked<{/if}> onclick="setDisabled();">許可しない
    </font>
  </td>
  <td bgcolor="#DFFFDC" class="spacing">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
        <td align="right"><input type="button" name="ref_dept_all" value="全画面" onclick="showDeptAll('ref', '<{$session}>');"></td>
      </tr>
    </table>
  </td>
  <td bgcolor="#DFFFDC" colspan="2" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
</tr>
<tr id="ref_toggle2" style="display:<{$ref_toggle_display}>;">
  <td bgcolor="#FFFFFF">
    <table cellspacing="0" cellpadding="0" border="0">
      <tr>
        <td style="padding-right:20px;">
          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
            <input type="radio" name="ref_dept_flg" value="1" <{if $ref_dept_flg == "1"}>checked<{/if}> onclick="setDisabled();">すべて<br>
            <input type="radio" name="ref_dept_flg" value="2" <{if $ref_dept_flg == "2"}>checked<{/if}> onclick="setDisabled();">指定する
          </font>
        </td>
        <td>
          <table cellspacing="2" cellpadding="0" border="0">
            <tr>
              <td valign="bottom" style="position:relative;top:3px;">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">可能とする<{$arr_class_name[2]}>
                </font>
              </td>
              <td></td>
              <td>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                  <select name="ref_class_src" onchange="setRefAtrbSrcOptions();">
                  </select><{$arr_class_name[0]}><br>
                  <select name="ref_atrb_src" onchange="setRefDeptSrcOptions();">
                  </select><{$arr_class_name[1]}>
                </font>
              </td>
            </tr>
            <tr>
              <td>
                <select name="ref_dept" size="6" multiple style="width:120px;">
                  <{foreach from=$ref_dept item=dept_id}>
                    <option value="<{$dept_id}>"><{$dept_names[$dept_id]}></option>
                  <{/foreach}>
                </select>
              </td>
              <td align="center">
                <input type="button" name="add_ref_dept" value=" &lt; " onclick="addSelectedOptions(this.form.ref_dept, this.form.ref_dept_src);"><br><br><input type="button" name="delete_ref_dept" value=" &gt; " onclick="deleteSelectedOptions(this.form.ref_dept);">
              </td>
              <td>
                <select name="ref_dept_src" size="6" multiple style="width:120px;"></select>
              </td>
            </tr>
            <tr>
              <td>
                <input type="button" name="delete_all_ref_dept" value="全て消去" onclick="deleteAllOptions(this.form.ref_dept);">
              </td>
              <td></td>
              <td><input type="button" name="select_all_ref_dept" value="全て選択" onclick="selectAllOptions(this.form.ref_dept_src);"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </td>
  <td colspan="2" bgcolor="#FFFFFF">
    <table cellspacing="0" cellpadding="0" border="0">
      <tr>
        <td style="padding-right:20px;">
          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
            <input type="radio" name="ref_st_flg" value="1" <{if $ref_st_flg == "1"}>checked<{/if}> onclick="setDisabled();">すべて<br>
            <input type="radio" name="ref_st_flg" value="2" <{if $ref_st_flg == "2"}>checked<{/if}> onclick="setDisabled();">指定する
          </font>
        </td>
        <td>
          <table cellspacing="2" cellpadding="0" border="0">
            <tr>
              <td>
                <select name="ref_st[]" size="10" multiple>
                  <{foreach from=$sel_st item=i}>
                      <option value="<{$i.st_id}>" <{if in_array($i.st_id, $ref_st)}>selected<{/if}>>
                        <{$i.st_nm}>
                      </option>
                  <{/foreach}>
                </select>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </td>
</tr>
<!-- 参照部署役職の指定 END -->

<!-- 参照職員の指定 START -->
<tr height="22" id="ref_toggle3" style="display:<{$ref_toggle_display}>;">
  <td bgcolor="#DFFFDC" align="center">
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
    <input type="button" name="emplist1" value="職員名簿" style="margin-left:2em;width: 5.5em;" onclick="openEmployeeList('1', '<{$session}>');"><br>
    <input type="button" name="emplist1_clear" value="クリア" style="margin-left:2em;width: 5.5em;" onclick="clear_target('1','<{$emp_id}>','<{$emp_name}>');"><br>
  </td>
  <td colspan="3" bgcolor="#FFFFFF">
    <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
      <tr>
        <td width="350" height="60" style="border:#5279a5 solid 1px;">
          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
            <span id="target_disp_area1"></span>
          </font>
        </td>
      </tr>
    </table>
  </td>
</tr>
<!-- 参照職員の指定 END -->

<!-- 更新可能範囲の指定 START -->
<tr height="24">
  <td bgcolor="#DFFFDC" colspan="4" class="spacing">
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
      <span style="cursor:pointer;" onclick="toggle1(document.getElementById('upd_toggle'));">更新可能範囲の指定&nbsp;</span>
      <span id="upd_toggle" style="cursor:pointer;" onclick="toggle1(this);"><{$upd_toggle_mode}></span>
    </font>
  </td>
</tr>
<!-- 更新可能範囲の指定有無 END -->

<!-- 更新部署役職の指定 START -->
<tr height="22" id="upd_toggle1" style="display:<{$upd_toggle_display}>;">
  <td bgcolor="#DFFFDC" rowspan="2" align="center">
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
      部署・役職<br>
      <input type="checkbox" name="upd_dept_st_flg" value="t" <{if $upd_dept_st_flg == "t"}>checked<{/if}> onclick="setDisabled();">許可しない
    </font>
  </td>
  <td bgcolor="#DFFFDC" class="spacing">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
        <td align="right"><input type="button" name="upd_dept_all" value="全画面" onclick="showDeptAll('upd', '<{$session}>');"></td>
      </tr>
    </table>
  </td>
  <td bgcolor="#DFFFDC" colspan="2" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
</tr>
<tr id="upd_toggle2" style="display:<{$upd_toggle_display}>;">
  <td bgcolor="#FFFFFF">
    <table cellspacing="0" cellpadding="0" border="0">
      <tr>
        <td style="padding-right:20px;">
          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
            <input type="radio" name="upd_dept_flg" value="1" <{if $upd_dept_flg == "1"}>checked<{/if}> onclick="setDisabled();">すべて<br>
            <input type="radio" name="upd_dept_flg" value="2" <{if $upd_dept_flg == "2"}>checked<{/if}> onclick="setDisabled();">指定する
          </font>
        </td>
        <td>
          <table cellspacing="2" cellpadding="0" border="0">
            <tr>
              <td valign="bottom" style="position:relative;top:3px;">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                  可能とする<{$arr_class_name[2]}>
                </font>
              </td>
              <td></td>
              <td>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                  <select name="upd_class_src" onchange="setUpdAtrbSrcOptions();">
                  </select><{$arr_class_name[0]}><br>
                  <select name="upd_atrb_src" onchange="setUpdDeptSrcOptions();">
                  </select><{$arr_class_name[1]}>
                </font>
              </td>
            </tr>
            <tr>
              <td>
                <select name="upd_dept" size="6" multiple style="width:120px;">
                  <{foreach from=$upd_dept item=dept_id}>
                    <option value="<{$dept_id}>"><{$dept_names[$dept_id]}>
                  <{/foreach}>
                </select>
              </td>
              <td align="center"><input type="button" name="add_upd_dept" value=" &lt; " onclick="addSelectedOptions(this.form.upd_dept, this.form.upd_dept_src);"><br><br><input type="button" name="delete_upd_dept" value=" &gt; " onclick="deleteSelectedOptions(this.form.upd_dept);"></td>
              <td><select name="upd_dept_src" size="6" multiple style="width:120px;"></select></td>
            </tr>
            <tr>
              <td><input type="button" name="delete_all_upd_dept" value="全て消去" onclick="deleteAllOptions(this.form.upd_dept);"></td>
              <td></td>
              <td><input type="button" name="select_all_upd_dept" value="全て選択" onclick="selectAllOptions(this.form.upd_dept_src);"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </td>
  <td colspan="2" bgcolor="#FFFFFF">
    <table cellspacing="0" cellpadding="0" border="0">
      <tr>
        <td style="padding-right:20px;">
          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
            <input type="radio" name="upd_st_flg" value="1" <{if $upd_st_flg == "1"}>checked<{/if}> onclick="setDisabled();">すべて<br>
            <input type="radio" name="upd_st_flg" value="2" <{if $upd_st_flg == "2"}>checked<{/if}> onclick="setDisabled();">指定する
          </font>
        </td>
        <td>
          <table cellspacing="2" cellpadding="0" border="0">
            <tr>
              <td>
                <select name="upd_st[]" size="10" multiple>
                  <{foreach from=$sel_st item=i}>
                    <option value="<{$i.st_id}>" <{if in_array($i.st_id, $upd_st)}>selected<{/if}>>
                      <{$i.st_nm}>
                    </option>
                  <{/foreach}>
                </select>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </td>
</tr>
<!-- 更新部署役職の指定 END -->

<!-- 更新職員の指定 START -->
<tr height="22" id="upd_toggle3" style="display:<{$upd_toggle_display}>;">
  <td bgcolor="#DFFFDC" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
    <input type="button" name="emplist2" value="職員名簿" style="margin-left:2em;width: 5.5em;" onclick="openEmployeeList('2', '<{$session}>');"><br>
    <input type="button" name="emplist2_clear" value="クリア" style="margin-left:2em;width: 5.5em;" onclick="clear_target('2','<{$emp_id}>','<{$emp_name}>');"><br></td>
      <td colspan="3" bgcolor="#FFFFFF">
    <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
      <tr>
        <td width="350" height="60" style="border:#5279a5 solid 1px;">
          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
            <span id="target_disp_area2"></span>
          </font>
        </td>
      </tr>
    </table>
  </td>
</tr>
<!-- 更新職員の指定 END -->
