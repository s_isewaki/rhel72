<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <{$INCIDENT_TITLE}> | 様式作成</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tablednd.js"></script>
<script type="text/javascript">
jQuery.noConflict();
var j$ = jQuery;
<!--
j$(document).ready(function(){
    on_change_style(<{$style_code}>);
    <{if $style_code == 1}>
        set_unable_checkbox();
    <{/if}>
    setMustUsableAll();
    set_method_2010(true);
});

//項目の並び替えを行うスクリプト
j$(function() {
	j$('.dragHandle').closest('tbody').tableDnD({
		dragHandle: "dragHandle"
	});

	j$('.dragHandle').hover(function() {
		j$(this).addClass('showDragHandle');
		j$(this).css("cursor","move");
		j$(this.parentNode.cells).css('background-color', 'gold');
	},
	function() {
		j$(this).removeClass('showDragHandle');
		j$(this).css("cursor","default");
		j$(this.parentNode.cells).css('background-color', 'white');
	});
});

function getUseObj(grp_code) {
    var use_obj_id = "id_" + grp_code;
    var use_obj = getFormObj(use_obj_id);
    return use_obj;
}

function getMustObj(grp_code) {
    var must_obj_id = "id_must_" + grp_code;
    var must_obj = getFormObj(must_obj_id);
    return must_obj;
}

function getFormObj(id) {
    var oForm;
    if(document.all) oForm = document.all(id);
    else if(document.getElementById) oForm = document.getElementById(id);

    return oForm;
}

function set_unable_checkbox() {
    var c_700 = getUseObj('700');
    var c_800 = getUseObj('800');
    var c_900 = getUseObj('900');

    var c_120 = getUseObj('120');
    var c_400_110 = getUseObj('400_110');
    var c_400_120 = getUseObj('400_120');
    var c_400_130 = getUseObj('400_130');

    if ( c_700.checked ) {
        c_120.disabled = true;
        c_400_110.disabled = true;
        c_400_120.disabled = true;
        c_400_130.disabled = true;
        c_900.disabled = true;
    }

    else if ( c_120.checked ) {
        c_700.disabled = true;
        c_800.disabled = true;
        c_900.disabled = true;
    }

    else if ( c_900.checked ) {
        c_120.disabled = true;
        c_400_110.disabled = true;
        c_400_120.disabled = true;
        c_400_130.disabled = true;
        c_700.disabled = true;
        c_800.disabled = true;
    }
}

function setRelateCheck( parent_grp_code, child_grp_code ) {
    if ( parent_grp_code==110 && j$("input[name='style_code']").val()==1 ){
      return;
    }

    var objParent = getUseObj( parent_grp_code );
    var objChild = getUseObj( child_grp_code );
    if ( objChild.checked ) {
        objParent.checked = true;
        setMustUsable(parent_grp_code);
    }
}

//チェックボックス連動 20140423 R.C
function setParentChildCheck( obj_target, mode ){
    if (mode == "check_parent"){
        j$(obj_target).parents("div.parent").find('ul li input.check-child').attr('checked',obj_target.checked);
    }

    if (mode == "check_child"){
        var checkNum = j$(obj_target).parents('ul').find('li input.check-child:checked').length;
        var check_flag = (checkNum > 0) ? 'checked' : '';

        j$(obj_target).parents("div.parent").find('input.check-parent:checkbox').attr('checked',check_flag);
    }
}

function setMustUsableAll() {
    var grp_code = null;

    <{foreach from=$cate_list item=cate}>
        <{section name=l loop=$cate.grp_list}>
            <{* アセスメント・患者の状態詳細、ミトンあり詳細の場合対象外 20090304 *}>
            <{if $cate.grp_list[l].grp_code != 150
              && $cate.grp_list[l].grp_code != 160
              && $cate.grp_list[l].grp_code != 170
              && $cate.grp_list[l].grp_code != 180
              && $cate.grp_list[l].grp_code != 190}>
                setMustUsable(<{$cate.grp_list[l].grp_code}>);
                <{if isset( $cate.grp_list[l].rel_grp)}>
                    setMustUsable(<{$cate.grp_list[l].rel_grp.grp_code}>);
                <{/if}>
            <{/if}>
        <{/section}>
    <{/foreach}>
    setMustUsable(700);
}
function setMustUsable(grp_code) {
    // 必須項目のチェックボックスが無いため、処理を行わない
    if(grp_code == 1130 || grp_code == 1000) return;

    <{if $style_code == 1}>
      if ( j$("#id_" + grp_code + "_area").css("display") == "none" ) return;
    <{/if}>

    if(grp_code == "140") {
        <{section name=m loop=$item_mst_asses}>
            use_obj = getUseObj('150_<{$item_mst_asses[m].easy_item_code}>');
            must_obj = getMustObj('150_<{$item_mst_asses[m].easy_item_code}>');
            must_obj.disabled = !use_obj.checked;
        <{/section}>
    }
    else if(grp_code == "210") {
        // 患者プロフィールの場合
        use_obj = getUseObj('210_10');
        must_obj = getMustObj('210_10');
        must_obj.disabled = !use_obj.checked;

        use_obj = getUseObj('210_20');
        must_obj = getMustObj('210_20');
        must_obj.disabled = !use_obj.checked;

        use_obj = getUseObj('210_30');
        must_obj = getMustObj('210_30');
        must_obj.disabled = !use_obj.checked;

        use_obj = getUseObj('210_40');
        must_obj = getMustObj('210_40');
        must_obj.disabled = !use_obj.checked;

        use_obj = getUseObj('210_50');
        must_obj = getMustObj('210_50');
        must_obj.disabled = !use_obj.checked;
    }
    else if(grp_code == "400" || grp_code == "120") {
        //インシデントの概要の項目グループの場合
        use_obj = getUseObj('400_110');
        must_obj = getMustObj('400_110');
        must_obj.disabled = !use_obj.checked;

        use_obj = getUseObj('400_120');
        must_obj = getMustObj('400_120');
        must_obj.disabled = !use_obj.checked;

        use_obj = getUseObj('400_130');
        must_obj = getMustObj('400_130');
        must_obj.disabled = !use_obj.checked;

        if(grp_code == "120")
        {
            use_obj = getUseObj(grp_code);
            must_obj = getMustObj(grp_code);
            must_obj.disabled = !use_obj.checked;
        }
    }
    else if(grp_code == "800" || grp_code == "700") {
        //インシデントの概要の項目グループの場合
        use_obj = getUseObj('800');
        must_obj = getMustObj('800');
        must_obj.disabled = !use_obj.checked;

        if(grp_code == "700")
        {
            use_obj = getUseObj(grp_code);
            must_obj = getMustObj(grp_code);
            must_obj.disabled = !use_obj.checked;
        }
    }
    else {
        use_obj = getUseObj(grp_code);
        must_obj = getMustObj(grp_code);

        //※disabledになるため、submit対象とならない。
        must_obj.disabled = !use_obj.checked;
    }
}

function setDisabled(grp_code) {
    if(grp_code == "700" || grp_code == "800") {
        use_obj_700 = getUseObj('700');
        use_obj_120 = getUseObj('120');
        use_obj_400_110 = getUseObj('400_110');
        use_obj_400_120 = getUseObj('400_120');
        use_obj_400_130 = getUseObj('400_130');
        use_obj_900 = getUseObj('900');
        must_obj_120 = getMustObj('120');
        must_obj_400_110 = getMustObj('400_110');
        must_obj_400_120 = getMustObj('400_120');
        must_obj_400_130 = getMustObj('400_130');
        must_obj_900 = getMustObj('900');

        if(use_obj_700.checked == true) {
            use_obj_120.disabled = true;
            use_obj_120.checked  = false;
            use_obj_400_110.disabled = true;
            use_obj_400_110.checked  = false;
            use_obj_400_120.disabled = true;
            use_obj_400_120.checked  = false;
            use_obj_400_130.disabled = true;
            use_obj_400_130.checked  = false;
            use_obj_900.disabled = true;
            use_obj_900.checked = false;
            must_obj_120.disabled = true;
            must_obj_400_110.disabled = true;
            must_obj_400_120.disabled = true;
            must_obj_400_130.disabled = true;
            must_obj_900.disabled = true;
        } else {
            use_obj_120.disabled = false;
            use_obj_400_110.disabled = false;
            use_obj_400_120.disabled = false;
            use_obj_400_130.disabled = false;
            use_obj_900.disabled = false;
        }
    } else if(grp_code == "120" || grp_code == "400") {
        use_obj_120 = getUseObj('120');
        use_obj_700 = getUseObj('700');
        use_obj_800 = getUseObj('800');
        use_obj_900 = getUseObj('900');
        must_obj_700 = getMustObj('700');
        must_obj_800 = getMustObj('800');
        must_obj_900 = getMustObj('900');

        if(use_obj_120.checked == true) {
            use_obj_700.disabled = true;
            use_obj_700.checked  = false;
            use_obj_800.disabled = true;
            use_obj_800.checked  = false;
            use_obj_900.disabled = true;
            use_obj_900.checked  = false;
        } else {
            use_obj_700.disabled = false;
            use_obj_800.disabled = false;
            use_obj_900.disabled = false;
        }
    } else if(grp_code == "900") {
        use_obj_900 = getUseObj('900');
        use_obj_120 = getUseObj('120');
        use_obj_400_110 = getUseObj('400_110');
        use_obj_400_120 = getUseObj('400_120');
        use_obj_400_130 = getUseObj('400_130');
        use_obj_700 = getUseObj('700');
        use_obj_800 = getUseObj('800');
        must_obj_120 = getMustObj('120');
        must_obj_400_110 = getMustObj('400_110');
        must_obj_400_120 = getMustObj('400_120');
        must_obj_400_130 = getMustObj('400_130');
        must_obj_700 = getMustObj('700');
        must_obj_800 = getMustObj('800');

        if(use_obj_900.checked == true) {
            use_obj_120.disabled = true;
            use_obj_120.checked  = false;
            use_obj_400_110.disabled = true;
            use_obj_400_110.checked  = false;
            use_obj_400_120.disabled = true;
            use_obj_400_120.checked  = false;
            use_obj_400_130.disabled = true;
            use_obj_400_130.checked  = false;
            use_obj_700.disabled = true;
            use_obj_700.checked = false;
            use_obj_800.disabled = true;
            use_obj_800.checked = false;
            must_obj_120.disabled = true;
            must_obj_400_110.disabled = true;
            must_obj_400_120.disabled = true;
            must_obj_400_130.disabled = true;
            must_obj_700.disabled = true;
            must_obj_800.disabled = true;
        } else {
            use_obj_120.disabled = false;
            use_obj_400_110.disabled = false;
            use_obj_400_120.disabled = false;
            use_obj_400_130.disabled = false;
            use_obj_700.disabled = false;
            use_obj_800.disabled = false;
        }
    }
}

function setCheckboxOnOffAll(target, is_on) {
    <{foreach from=$cate_list item=cate}>
        setCheckboxOnOff(target,'<{$cate.cate_code}>',is_on);
    <{/foreach}>
}

function setCheckboxOnOff(target, cate_code, is_on) {
    var checkbox_obj = null;

    <{foreach from=$cate_list item=cate}>
        if(cate_code == '<{$cate.cate_code}>'){
            <{section name=l loop=$cate.grp_list}>
                <{* アセスメント・患者の状態詳細、ミトンあり詳細の場合対象外 20090304 *}>
                <{if $cate.grp_list[l].grp_code != 150
                  && $cate.grp_list[l].grp_code != 160
                  && $cate.grp_list[l].grp_code != 170
                  && $cate.grp_list[l].grp_code != 180
                  && $cate.grp_list[l].grp_code != 190
                  && $cate.grp_list[l].grp_code != 1000
                  && $cate.grp_list[l].grp_code != 1130}>
                  if ( j$("#id_" + <{$cate.grp_list[l].grp_code}> + "_area").css("display") != "none" ){
                    if(target == "use"){
                        <{*アセスメント・患者の状態*}>
                        <{if $cate.grp_list[l].grp_code == 140}>
                            getUseObj('150_3').checked = is_on;
                            getUseObj('150_5').checked = is_on;
                            getUseObj('150_6').checked = is_on;
                            getUseObj('150_7').checked = is_on;
                            getUseObj('150_8').checked = is_on;
                            getUseObj('150_10').checked = is_on;
                            getUseObj('150_20').checked = is_on;
                            getUseObj('150_30').checked = is_on;
                            getUseObj('150_40').checked = is_on;
                            getUseObj('150_50').checked = is_on;
                            getUseObj('150_60').checked = is_on;
                            getUseObj('150_70').checked = is_on;
                            getUseObj('150_80').checked = is_on;

                        <{elseif $cate.grp_list[l].grp_code == 1130}>

                        <{*患者プロフィール*}>
                        <{elseif $cate.grp_list[l].grp_code == 210}>
                            checkbox_obj = getUseObj('210_10');
                            checkbox_obj.checked = is_on;

                            checkbox_obj = getUseObj('210_20');
                            checkbox_obj.checked = is_on;

                            checkbox_obj = getUseObj('210_30');
                            checkbox_obj.checked = is_on;

                            checkbox_obj = getUseObj('210_40');
                            checkbox_obj.checked = is_on;

                            checkbox_obj = getUseObj('210_50');
                            checkbox_obj.checked = is_on;

                        <{*通常項目の場合*}>
                        <{else}>
                            checkbox_obj = getUseObj(<{$cate.grp_list[l].grp_code}>);
                            checkbox_obj.checked = is_on;
                            checkbox_obj.disabled = false;

                            <{*インシデントの概要の項目グループの場合*}>
                            <{if $cate.grp_list[l].grp_code == 120}>
                                setDisabled('120');

                                checkbox_obj = getUseObj('400_110');
                                checkbox_obj.checked = is_on;
                                checkbox_obj.disabled = false;

                                checkbox_obj = getUseObj('400_120');
                                checkbox_obj.checked = is_on;
                                checkbox_obj.disabled = false;

                                checkbox_obj = getUseObj('400_130');
                                checkbox_obj.checked = is_on;
                                checkbox_obj.disabled = false;

                            <{*発生場所*}>
                            <{elseif $cate.grp_list[l].grp_code == 110}>
                                checkbox_obj = getUseObj('110_65');
                                checkbox_obj.checked = is_on;
                                checkbox_obj.disabled = false;
                            <{/if}>
                        <{/if}>

                        <{if $cate.grp_list[l].grp_code != 1130}>
                            setMustUsable(<{$cate.grp_list[l].grp_code}>);
                        <{/if}>
                    }
                    else{ //if(target=="must")
                        <{if $cate.grp_list[l].grp_code == 140}>
                            getMustObj('150_3').checked = is_on;
                            getMustObj('150_5').checked = is_on;
                            getMustObj('150_6').checked = is_on;
                            getMustObj('150_7').checked = is_on;
                            getMustObj('150_8').checked = is_on;
                            getMustObj('150_10').checked = is_on;
                            getMustObj('150_20').checked = is_on;
                            getMustObj('150_30').checked = is_on;
                            getMustObj('150_40').checked = is_on;
                            getMustObj('150_50').checked = is_on;
                            getMustObj('150_60').checked = is_on;
                            getMustObj('150_70').checked = is_on;
                            getMustObj('150_80').checked = is_on;

                        <{elseif $cate.grp_list[l].grp_code == 1130}>
                        <{elseif $cate.grp_list[l].grp_code == 210}> // 患者プロフィールの場合
                            checkbox_obj = getMustObj('210_10');
                            checkbox_obj.checked = is_on;

                            checkbox_obj = getMustObj('210_20');
                            checkbox_obj.checked = is_on;

                            checkbox_obj = getMustObj('210_30');
                            checkbox_obj.checked = is_on;

                            checkbox_obj = getMustObj('210_40');
                            checkbox_obj.checked = is_on;

                            checkbox_obj = getMustObj('210_50');
                            checkbox_obj.checked = is_on;
                        <{else}> //通常項目の場合
                            checkbox_obj = getMustObj(<{$cate.grp_list[l].grp_code}>);
                            checkbox_obj.checked = is_on;

                            <{if $cate.grp_list[l].grp_code == 120}> // インシデントの概要の項目グループの場合
                                checkbox_obj = getMustObj('400_110');
                                checkbox_obj.checked = is_on;

                                checkbox_obj = getMustObj('400_120');
                                checkbox_obj.checked = is_on;

                                checkbox_obj = getMustObj('400_130');
                                checkbox_obj.checked = is_on;

                                checkbox_obj = getMustObj('700');
                                checkbox_obj.checked = is_on;

                                checkbox_obj = getMustObj('800');
                                checkbox_obj.checked = is_on;
                            <{/if}>
                        <{/if}>
                    }
                  }
                <{/if}>
            <{/section}>
        }
    <{/foreach}>
}


function regist() {
    var match_flg = false;
    if(!match_flg){match_flg = check_number(document.FRM_MAIN.letter_500.value)};
    if(!match_flg){match_flg = check_number(document.FRM_MAIN.letter_520.value)};
    if(!match_flg){match_flg = check_number(document.FRM_MAIN.letter_525.value)};
    if(!match_flg){match_flg = check_number(document.FRM_MAIN.letter_530.value)};
    if(!match_flg){match_flg = check_number(document.FRM_MAIN.letter_540.value)};
    if(!match_flg){match_flg = check_number(document.FRM_MAIN.letter_550.value)};
    if(!match_flg){match_flg = check_number(document.FRM_MAIN.letter_610.value)};
    if(!match_flg){match_flg = check_number(document.FRM_MAIN.letter_620.value)};
    if(!match_flg){match_flg = check_number(document.FRM_MAIN.letter_625.value)};
    if(!match_flg){match_flg = check_number(document.FRM_MAIN.letter_630.value)};
    if(!match_flg){match_flg = check_number(document.FRM_MAIN.letter_635.value)};

    if(!match_flg){
        document.FRM_MAIN.action = "<{$post_back_url}>";
        document.FRM_MAIN.target = "_self";
        document.FRM_MAIN.submit();
    }
}
function check_number(value){
        //入力値に 0〜9 以外があれば
        if(value.match(/[^0-9]+/)){
            alert("最大文字数の欄は半角数字のみを入力してください。");
            return true;
        }
}



function show_preview() {
    //サイズ指定で空ウィンドウを開く
    var h = window.screen.availHeight;
    var w = window.screen.availWidth;
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
    var url = "";
    window.open(url, 'easyinput_set_preview_window',option);

    //開いたウィンドウにプレビュー呼び出しの送信
    document.FRM_MAIN.action = "hiyari_easyinput_preview.php?theme="+document.FRM_MAIN.this_term_theme.value;
    document.FRM_MAIN.target = "easyinput_set_preview_window";
    document.FRM_MAIN.submit();
}

// 収集事業様式でチェックが必要な項目
function setCheckboxOnInci() {
    document.getElementById("id_700").checked = false;
    document.getElementById("id_800").checked = false;
    document.getElementById("id_120").checked = false;
    document.getElementById("id_400_110").checked = false;
    document.getElementById("id_400_120").checked = false;
    document.getElementById("id_400_130").checked = false;
    document.getElementById("id_700").disabled = true;
    document.getElementById("id_800").disabled = true;
    document.getElementById("id_120").disabled = true;
    document.getElementById("id_400_110").disabled = true;
    document.getElementById("id_400_120").disabled = true;
    document.getElementById("id_400_130").disabled = true;
    document.getElementById("id_900").disabled = false;

    document.getElementById("id_1100").checked = true;
    //document.getElementById("id_1110").checked = true;
    document.getElementById("id_1120").checked = true;
    document.getElementById("id_100").checked = true;
    document.getElementById("id_110").checked = true;
    document.getElementById("id_3000").checked = true;
    document.getElementById("id_3050").checked = true;
    document.getElementById("id_3250").checked = true;
    document.getElementById("id_3300").checked = true;
    document.getElementById("id_3500").checked = true;
    document.getElementById("id_200").checked = true;
    document.getElementById("id_210_30").checked = true;
    document.getElementById("id_210_40").checked = true;
    document.getElementById("id_210_50").checked = true;
    document.getElementById("id_230").checked = true;
    document.getElementById("id_250").checked = true;
    document.getElementById("id_260").checked = true;
    document.getElementById("id_270").checked = true;
    document.getElementById("id_280").checked = true;
    document.getElementById("id_290").checked = true;
    document.getElementById("id_900").checked = true;
    document.getElementById("id_600").checked = true;
    document.getElementById("id_610").checked = true;
    document.getElementById("id_620").checked = true;
    //document.getElementById("id_130").checked = true;
    //document.getElementById("id_590").checked = true;
    document.getElementById("id_1300").checked = true;
    document.getElementById("id_1310").checked = true;
    document.getElementById("id_1320").checked = true;
    //document.getElementById("id_3350").checked = true;
    //document.getElementById("id_3400").checked = true;
    //document.getElementById("id_3450").checked = true;
    document.getElementById("id_90").checked = true;
    //document.getElementById("id_3200").checked = true;
    //document.getElementById("id_500").checked = true;
    document.getElementById("id_520").checked = true;
    setMustUsable(1100);
    //setMustUsable(1110);
    setMustUsable(1120);
    setMustUsable(100);
    setMustUsable(110);
    setMustUsable(3000);
    setMustUsable(3050);
    setMustUsable(3250);
    setMustUsable(3300);
    setMustUsable(3500);
    setMustUsable(200);
    setMustUsable("210_30");
    setMustUsable("210_40");
    setMustUsable("210_50");
    setMustUsable(230);
    setMustUsable(250);
    setMustUsable(260);
    setMustUsable(270);
    setMustUsable(280);
    setMustUsable(290);
    setMustUsable(900);
    setMustUsable(600);
    setMustUsable(610);
    setMustUsable(620);
    //setMustUsable(130);
    //setMustUsable(590);
    setMustUsable(1300);
    setMustUsable(1310);
    setMustUsable(1320);
    //setMustUsable(3350);
    //setMustUsable(3400);
    //setMustUsable(3450);
    setMustUsable(90);
    //setMustUsable(3200);
    //setMustUsable(500);
    setMustUsable(520);
    if(document.FRM_MAIN.name_290.value == "") {
        document.FRM_MAIN.name_290.value = "直前の患者の状態";
    }
    if(document.FRM_MAIN.name_250.value == "") {
        document.FRM_MAIN.name_250.value = "事例に直接関連する疾患名";
    }
    if(document.FRM_MAIN.name_610.value == "") {
        document.FRM_MAIN.name_610.value = "事例の背景要因の概要";
    }
    if(document.FRM_MAIN.name_520.value == "") {
        document.FRM_MAIN.name_520.value = "事例の内容";
    }
}

/////////////////////////////////////////////////
// デフォルト値
/////////////////////////////////////////////////
//概要・場面・内容（2010年改訂）
function set_method_2010(is_on_load){
    <{*ロード時にデフォルト値があれば設定*}>
    <{if isset($default[900][10])}>
    if ( is_on_load ) {
        j$("#method_2010").val(<{$default[900][10]}>);
    }
    <{/if}>
    <{*概要の値に合わせて種類・場面・内容を設定*}>
    var super_id = j$("#method_2010").val();

    set_kind_2010(super_id, is_on_load);
    set_scene_2010(super_id, is_on_load);
    set_content_2010(super_id, is_on_load);
}

function set_kind_2010(super_id, is_on_load){
    var obj = j$("#kind_2010");
    <{*概要の値に合わせて選択肢入れ替え*}>
    obj.html("<option value=''></option>");
    j$('#kind_2010').width();
    <{foreach from=$kind_2010 key=key item=val}>
        if (super_id == <{$key}>) {
        <{foreach name=list from=$val key=key2 item=val2}>
            obj.append("<option value='<{$val2.item_id}>'><{$val2.item_name}></option>");
            j$('#kind_2010').width();
        <{/foreach}>
        }
    <{/foreach}>

    <{*選択肢がなければ非表示*}>
    obj.css("display", (obj.children().length > 1) ? "block":"none");

    <{*ロード時にデフォルト値があれば設定*}>
    $kind_id="";
    <{if isset($default[910][10])}>
    if (is_on_load){
        $kind_id = <{$default[910][10]}>;
        j$("#kind_2010").val($kind_id);
    }
    <{/if}>
  <{*種類の値に合わせて項目を設定*}>
  set_kind_item_2010($kind_id, is_on_load);
}

function set_scene_2010(super_id, is_on_load){
    var obj = j$("#scene_2010");
    <{*概要の値に合わせて選択肢入れ替え*}>
    obj.html("<option value=''></option>");
    j$('#scene_2010').width();
    <{foreach from=$scene_2010 key=key item=val}>
        if (super_id == <{$key}>) {
        <{foreach name=list from=$val key=key2 item=val2}>
            obj.append("<option value='<{$val2.item_id}>'><{$val2.item_name}></option>");
            j$('#scene_2010').width();
        <{/foreach}>
        }
    <{/foreach}>

    <{*選択肢がなければ非表示*}>
    obj.css("display", (obj.children().length > 1) ? "block":"none");

    <{*ロード時にデフォルト値があれば設定*}>
    $scene_id="";
    <{if isset($default[940][10])}>
        if ( is_on_load ) {
            $scene_id = <{$default[940][10]}>;
            j$("#scene_2010").val($scene_id);
        }
    <{/if}>

    <{*場面の値に合わせて項目を設定*}>
    set_scene_item_2010($scene_id, is_on_load);
}

function set_content_2010(super_id, is_on_load){
    var obj = j$("#content_2010");

    <{*概要の値に合わせて選択肢入れ替え*}>
    obj.html("<option value=''></option>");
    j$('#content_2010').width();
    <{foreach from=$content_2010 key=key item=val}>
        if (super_id == <{$key}>) {
            <{foreach name=list from=$val key=key2 item=val2}>
                obj.append("<option value='<{$val2.item_id}>'><{$val2.item_name}></option>");
                j$('#content_2010').width();
            <{/foreach}>
        }
    <{/foreach}>

    <{*選択肢がなければ非表示*}>
    obj.css("display", (obj.children().length > 1) ? "block":"none");

    <{*ロード時にデフォルト値があれば設定*}>
    $content_id="";
    <{if isset($default[970][10])}>
        if (is_on_load){
        $content_id = <{$default[970][10]}>;
        j$("#content_2010").val($content_id);

        }
    <{/if}>

    <{*内容の値に合わせて項目を設定*}>
    set_content_item_2010($content_id, is_on_load);
}

function set_kind_item_2010(kind_id, is_on_load){
    var obj = j$("#kind_item_2010");
    <{*種類の値に合わせて選択肢入れ替え*}>
    obj.html("<option value=''></option>");
    j$('#kind_item_2010').width();
    <{foreach from=$kind_item_2010 key=key item=val}>
        if (kind_id == <{$key}>) {
            <{foreach name=list from=$val key=key2 item=val2}>
                obj.append("<option value='<{$val2.sub_item_id}>'><{$val2.sub_item_name}></option>");
                j$('#kind_item_2010').width();
            <{/foreach}>
        }
    <{/foreach}>

    <{*選択肢がなければ非表示*}>
    obj.css("display", (obj.children().length > 1) ? "block":"none");

    <{*ロード時にデフォルト値があれば設定*}>
    <{if isset($default[920][10])}>
        if (is_on_load){
            j$("#kind_item_2010").val(<{$default[920][10]}>);
        }
    <{/if}>

}

function set_scene_item_2010(scene_id, is_on_load){
    var obj = j$("#scene_item_2010");
    <{*場面の値に合わせて選択肢入れ替え*}>
    obj.html("<option value=''></option>");
    j$('#scene_item_2010').width();
    <{foreach from=$scene_item_2010 key=key item=val}>
        if (scene_id == <{$key}>) {
            <{foreach name=list from=$val key=key2 item=val2}>
                obj.append("<option value='<{$val2.sub_item_id}>'><{$val2.sub_item_name}></option>");
                j$('#scene_item_2010').width();
            <{/foreach}>
        }
    <{/foreach}>

  <{*選択肢がなければ非表示*}>
  obj.css("display", (obj.children().length > 1) ? "block":"none");

  <{*ロード時にデフォルト値があれば設定*}>
  <{if isset($default[950][10])}>
    if (is_on_load){
      j$("#scene_item_2010").val(<{$default[950][10]}>);

    }
  <{/if}>
}

function set_content_item_2010(content_id, is_on_load){
    var obj = j$("#content_item_2010");
    <{*内容の値に合わせて選択肢入れ替え*}>
    obj.html("<option value=''></option>");
    j$('#content_item_2010').width();
    <{foreach from=$content_item_2010 key=key item=val}>
        if (content_id == <{$key}>) {
            <{foreach name=list from=$val key=key2 item=val2}>
                obj.append("<option value='<{$val2.sub_item_id}>'><{$val2.sub_item_name}></option>");
                j$('#content_item_2010').width();
            <{/foreach}>
        }
    <{/foreach}>

    <{*選択肢がなければ非表示*}>
    obj.css("display", (obj.children().length > 1) ? "block":"none");

    <{*ロード時にデフォルト値があれば設定*}>
    <{if isset($default[980][10])}>
        if (is_on_load){
            j$("#content_item_2010").val(<{$default[980][10]}>);
        }
    <{/if}>
}

/////////////////////////////////////////////////
// スタイルの変更
/////////////////////////////////////////////////
function on_change_style(style_code){
  <{*発生場所詳細*}>
  j$("#id_110_65_area").css("display", style_code==2 ? "table-row":"none");

  <{*関連する患者情報*}>
  j$("#id_patients_line").css("display", style_code==1 ? "table-row":"none");
  j$("#id_patients_info").css("display", style_code==2 ? "table-row":"none");
  j$("#id_310_area").css("display", style_code==2 ? "table-row":"none");
  j$("#id_320_area").css("display", style_code==2 ? "table-row":"none");
  j$("#id_330_area").css("display", style_code==2 ? "table-row":"none");
  j$("#id_340_area").css("display", style_code==2 ? "table-row":"none");
  j$("#id_350_area").css("display", style_code==2 ? "table-row":"none");
  j$("#id_360_area").css("display", style_code==2 ? "table-row":"none");
  j$("#id_370_area").css("display", style_code==2 ? "table-row":"none");
  j$("#id_380_area").css("display", style_code==2 ? "table-row":"none");
  j$("#id_390_area").css("display", style_code==2 ? "table-row":"none");

  <{*発生した場面と内容に関する情報 *}>
  j$("#id_700_area").css("display", style_code==1 ? "table-row":"none");
  j$("#id_800_area").css("display", style_code==1 ? "table-row":"none");

  <{*インシデントの概要*}>
  j$("#id_120_area").css("display", style_code==1 ? "table-row":"none");
  j$("#id_400_110_area").css("display", style_code==1 ? "table-row":"none");
  j$("#id_400_120_area").css("display", style_code==1 ? "table-row":"none");
  j$("#id_400_130_area").css("display", style_code==1 ? "table-row":"none");

  <{*影響*}>
  j$("#id_570_area").css("display", style_code==1 ? "table-row":"none");

  <{*対応*}>
  j$("#id_580_area").css("display", style_code==1 ? "table-row":"none");

  if (style_code == 2){
      <{*2010年版の概要を有効にする*}>
      use_obj_900 = getUseObj('900');
      use_obj_900.disabled = false;
      must_obj_900 = getMustObj('900');
      must_obj_900.disabled = false;
      <{*2010年版以外の概要を無効にする*}>
      var temp = use_obj_900.checked;
      use_obj_900.checked = true;
      setDisabled('900');
      use_obj_900.checked = temp;
  }
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

caption {text-align:left;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>


<!-- ヘッダー START -->
<{php}>
    require_once("./hiyari_common.ini");
    $eis_no = $this->_tpl_vars['eis_no'];
    if($eis_no == "*")$eis_no="";
    show_hiyari_header($this->_tpl_vars['session'],$this->_tpl_vars['fname'],true,array('eis_no' => $eis_no));

<{/php}>
<!-- ヘッダー END -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>




<form name="FRM_MAIN" action="<{$post_back_url}>" method="post">
<input type="hidden" name="postback" value="true">
<input type='hidden' name='session' value='<{$session}>'>
<input type='hidden' name='hyr_sid' value='<{$hyr_sid}>'>
<input type='hidden' name='eis_no' value='<{$eis_no}>'>
<input type='hidden' name='eis_id' value='<{$eis_id}>'>
<input type='hidden' name='design_mode' value='<{$design_mode}>'>






<!-- 様式番号/名称の枠 START -->
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="845" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">

    <!-- 様式番号/名称 START-->
    <table width="845" border="0" cellspacing="0" cellpadding="2" class="list">
    <tr height="22">
    <td width="60" align="right" bgcolor="#DFFFDC">
        <nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">様式番号</font></nobr>
    </td>
    <td width="30" align="center" bgcolor="#FFFFFF">
        <nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$eis_no}></font></nobr>
    </td>
    <td width="60" align="right" bgcolor="#DFFFDC">
        <nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">様式名称</font></nobr>
    </td>
    <td width="450" align="right" bgcolor="#FFFFFF">
        <input type="text" name="eis_name" value="<{$eis_name|escape:"html"}>" maxlength="50" style="width:100%">
    </td>
    </tr>
    </table>
    <!-- 様式番号/名称 END -->

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<!-- 様式番号/名称の枠 END -->



<!-- 報告書スタイル設定 START -->
<{if $design_mode == 2}>
  <input type='hidden' name='style_code' id='style_code' value='<{$style_code}>'>
<{else}>
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
      <td width="845" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
      <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
    </tr>
    <tr>
      <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
      <td bgcolor="#F5FFE5">
        <table width="845" border="0" cellspacing="0" cellpadding="2" class="list">
          <tr height="22">
            <td bgcolor="#DFFFDC">
              <nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告書スタイル設定</font></nobr>
            </td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">
              <nobr>
                &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="style_code" value="2" <{if $style_code==2}>checked<{/if}> onclick="on_change_style(2);">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スタイル２：新しいスタイルです。選択肢が画面上に表示されているスタイルです。</font>
              </nobr>
            </td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">
              <nobr>
                &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="style_code" value="1" <{if $style_code==1}>checked<{/if}> onclick="on_change_style(1);">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スタイル１：旧バージョンのスタイルです。リストボックス主体のスタイルです。</font>
              </nobr>
            </td>
          </tr>
        </table>
      </td>
      <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    </tr>
    <tr>
      <td><img src="img/r_3.gif" width="10" height="10"></td>
      <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
      <td><img src="img/r_4.gif" width="10" height="10"></td>
    </tr>
  </table>
<{/if}>
<!-- 報告書スタイル設定 END -->


<!-- 表題入力設定 START -->
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="845" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">

    <table width="845" border="0" cellspacing="0" cellpadding="2" class="list">
    <tr height="22">
        <td bgcolor="#DFFFDC">
            <nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題入力設定</font></nobr>
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF">
            <nobr>
                &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="subject" value="0" <{if $subject==0 || $subject==""}>checked<{/if}>>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題に分類の項目を表示しない（未入力の場合は「分類」の選択肢、「インシデントの内容」の先頭１０文字の順で自動入力）</font>
            </nobr>
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF">
            <nobr>
                &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="subject" value="1" <{if $subject==1}>checked<{/if}>>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題に分類の項目を表示し、分類に登録する</font>
            </nobr>
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF">
            <nobr>
                &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="subject" value="2" <{if $subject==2}>checked<{/if}>>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題に分類の項目を表示し、分類に登録しない</font>
            </nobr>
        </td>
    </tr>
    </table>

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<!-- 表題入力設定 END -->















<!-- 一覧 START-->
<nobr>
<input type="button" value="全てON"  onclick="setCheckboxOnOffAll('use',true)">
<input type="button" value="全てOFF" onclick="setCheckboxOnOffAll('use',false)">
<input type="button" value="全て必須"  onclick="setCheckboxOnOffAll('must',true)">
<input type="button" value="全て非必須" onclick="setCheckboxOnOffAll('must',false)">
<input type="button" value="医療事故等収集様式(ヒヤリ)" onclick="setCheckboxOnInci()">
</nobr>
<br>
<{foreach from=$cate_list item=cate}>
    <img src="img/spacer.gif" alt="" width="1" height="6"><br>

    <nobr>
    <{if $cate.cate_code == 10}>
    <input type="text" name="user_cate_name" value="<{$cate.cate_name}>">
    <{else}>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><{$cate.cate_name}></b></font>
    <{/if}>
    <input type="button" value="全てON"  onclick="setCheckboxOnOff('use','<{$cate.cate_code}>',true)">
    <input type="button" value="全てOFF" onclick="setCheckboxOnOff('use','<{$cate.cate_code}>',false)">
    <input type="button" value="全て必須"  onclick="setCheckboxOnOff('must','<{$cate.cate_code}>',true)">
    <input type="button" value="全て非必須" onclick="setCheckboxOnOff('must','<{$cate.cate_code}>',false)">
    </nobr>

    <!-- 入力表の枠 START -->
    <table border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="845" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
    </tr>
    <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td bgcolor="#F5FFE5">

        <!-- 入力表 START -->
        <table width="845" border="0" cellspacing="0" cellpadding="2" class="list">
          <tr>
            <{if $cate.cate_code == 10}>
              <td bgcolor="#DFFFDC" width="10"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
            <{/if}>
            <{if $cate.cate_code >= 2 && $cate.cate_code <= 7 || $cate.cate_code == 10}>
              <td bgcolor="#DFFFDC" width="330"></td>
              <td bgcolor="#DFFFDC" width="110" align="center">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                文言変更
                </font>
              </td>
            <{else}>
              <td bgcolor="#DFFFDC" width="440"></td>
            <{/if}>
            <td bgcolor="#DFFFDC" width="30" align="center">
              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
              必須指定
              </font>
            </td>
            <td bgcolor="#DFFFDC" width="370" align="center">
              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
              デフォルト値
              </font>
            </td>
              <{if $cate.cate_code >= 5 && $cate.cate_code <= 7}>
                <td bgcolor="#DFFFDC" width="37" align="center">
                <font face="ＭＳ Ｐゴシック, Osaka" class="j12">
                最大文字数
                </font>
                </td>

            <{/if}>

          </tr>

          <{section name=l loop=$cate.grp_list}>


<{* アセスメント・患者の状態詳細、ミトンあり詳細の場合対象外 20090304 *}>
            <{if $cate.grp_list[l].grp_code == "150"}>
            <{elseif $cate.grp_list[l].grp_code == "160"}>
            <{elseif $cate.grp_list[l].grp_code == "170"}>
            <{elseif $cate.grp_list[l].grp_code == "180"}>
            <{elseif $cate.grp_list[l].grp_code == "190"}>
<{* 場面と内容の場合対象外 awaji *}>
            <{elseif $cate.grp_list[l].grp_code == "700"}>
            <{elseif $cate.grp_list[l].grp_code == "800"}>
<{* 概要2010年版 20090918 *}>
            <{elseif $cate.grp_list[l].grp_code == "900"}>
<{* 関連医薬品・医療機器等・医療材料・諸物品等 除外 *}>
            <{elseif $cate.grp_list[l].grp_code == "1000"}>
<{* 関連医薬品・医療材料・諸物品等・医療機器等 順番の影響により除外 *}>
            <{elseif $cate.grp_list[l].grp_code == "1300"}>
            <{elseif $cate.grp_list[l].grp_code == "1310"}>
            <{elseif $cate.grp_list[l].grp_code == "1320"}>
<{* アセスメント・患者の状態 20090309 *}>
            <{elseif $cate.grp_list[l].grp_code == "140"}>
              <tr>
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" style="margin-left:5px;"><{$cate.grp_list[l].grp_name}></font>
                </td>
                <td bgcolor="#FFFFFF">
                </td>
                <td bgcolor="#FFFFFF">
                </td>
                <td bgcolor="#FFFFFF">
                </td>
              </tr>

              <{section name=m loop=$item_mst_asses}>
                <tr id="id_<{$cate.grp_list[l].grp_code}>_area">
                  <td bgcolor="#FFFFFF">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                      <input type="checkbox"
                              id="id_<{$item_mst_asses[m].grp_code}>_<{$item_mst_asses[m].easy_item_code}>"
                              name="asses_use_easy_item_code[]" value="<{$item_mst_asses[m].easy_item_code}>"
                              <{if in_array($item_mst_asses[m].easy_item_code, $asses_use)}>
                              checked
                              <{/if}>
                              onclick="
                                      var this_grp_code = <{$cate.grp_list[l].grp_code}>;
                                      setMustUsable(this_grp_code);
                                      "
                              style="margin-left:20px;"
                              >
                      <label for="id_<{$item_mst_asses[m].grp_code}>_<{$item_mst_asses[m].easy_item_code}>"><{$item_mst_asses[m].easy_item_name}></label>
                    </font>
                  </td>
                  <td bgcolor="#FFFFFF"></td>
                  <td bgcolor="#FFFFFF" align="center">
                    <input type="checkbox"
                          id="id_must_<{$item_mst_asses[m].grp_code}>_<{$item_mst_asses[m].easy_item_code}>"
                          name="asses_must_easy_item_code[]"
                          value="<{$item_mst_asses[m].easy_item_code}>"
                          <{if in_array($item_mst_asses[m].easy_item_code, $asses_must)}>
                          checked
                          <{/if}>
                          >
                  </td>
                  <td bgcolor="#FFFFFF"></td>
                </tr>
              <{/section}>

            <{elseif $cate.grp_list[l].grp_code == "90"}>
              <!-- 患者影響レベル 20140423 R.C -->
              <tr id="id_<{$cate.grp_list[l].grp_code}>_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                      <div class="parent">
                        <input type="checkbox"
                               class="check-parent"
                               id="id_<{$cate.grp_list[l].grp_code}>"
                               name="use_grp_code[]" value="<{$cate.grp_list[l].grp_code}>"
                               <{if in_array( $cate.grp_list[l].grp_code, $flags)}>
                               checked
                               <{/if}>
                               onclick="
                                           setParentChildCheck(this, 'check_parent');
                                           var this_grp_code = <{$cate.grp_list[l].grp_code}>;
                                           setMustUsable(this_grp_code);
                                       "
                               >
                        <label for="id_<{$cate.grp_list[l].grp_code}>"><{$cate.grp_list[l].grp_name}></label>

                        <ul style="list-style-type:none;margin:0px 0px 0px 15px;">
                            <li>
                                <label>
                                    <font color="#FF0000">※表示しない項目はチェックを外してください。</font>
                                </label>
                            </li>
                            <{foreach from=$level_infos item=info}>
                                <li>
                                    <input type="checkbox"
                                           class="check-child"
                                           id="id_<{$cate.grp_list[l].grp_code}>_<{$info.easy_code}>"
                                           name="patient_level_names[]" value="<{$info.easy_code}>"
                                           <{if in_array( $cate.grp_list[l].grp_code, $flags)}>
                                               <{if empty($level_use) || in_array( $info.easy_code, $level_use)}>
                                               checked
                                               <{/if}>
                                           <{/if}>
                                           onclick="
                                                       setParentChildCheck(this, 'check_child');
                                                       var this_grp_code = <{$cate.grp_list[l].grp_code}>;
                                                       setMustUsable(this_grp_code);
                                                      "
                                    >
                                    <label for="id_<{$cate.grp_list[l].grp_code}>_<{$info.easy_code}>"><{$info.easy_name|escape:"html"}></label>
                                </li>
                            <{/foreach}>
                        </ul>
                    </div>
                  </font>
                </td>
                <td bgcolor="#FFFFFF" align="right"></td>
                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_<{$cate.grp_list[l].grp_code}>"
                         name="must_grp_code[]"
                         value="<{$cate.grp_list[l].grp_code}>"
                         <{if in_array( $cate.grp_list[l].grp_code, $must_flags)}>
                         checked
                         <{/if}>
                         >
                </td>

                <td bgcolor="#FFFFFF">
                  <!-- デフォルト値 -->
                    <select name="def_90_10">
                      <option value=""></option>
                      <{foreach from=$level_infos item=info}>
                        <option value="<{$info.easy_code}>"
                          <{if isset($default[90][10])}>
                            <{if $default[90][10] == $info.easy_code}>selected<{/if}>
                          <{/if}>
                        >
                          <{$info.easy_name|escape:"html"}>
                        </option>
                      <{/foreach}>
                    </select>
                </td>
              </tr>

            <{elseif $cate.grp_list[l].grp_code == "110"}>
              <!--発生場所-->
              <tr id="id_<{$cate.grp_list[l].grp_code}>_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_<{$cate.grp_list[l].grp_code}>"
                           name="place_use_easy_item_code[]" value="60"
                           <{if in_array( $cate.grp_list[l].grp_code, $flags)}>checked<{/if}>
                           onclick="
                                   var this_grp_code = <{$cate.grp_list[l].grp_code}>;
                                   setMustUsable(this_grp_code);
                                   setRelateCheck('110', '110_65');
                                   "
                           >
                    <label for="id_<{$cate.grp_list[l].grp_code}>"><{$cate.grp_list[l].grp_name}></label>
                  </font>
                </td>
                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_<{$cate.grp_list[l].grp_code}>"
                         name="must_grp_code[]"
                         value="<{$cate.grp_list[l].grp_code}>"
                         <{if in_array( $cate.grp_list[l].grp_code, $must_flags)}>checked<{/if}>
                         >
                </td>
                <td bgcolor="#FFFFFF"></td>
              </tr>

              <tr id="id_110_65_area">
                <td bgcolor="#FFFFFF" width="440">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_<{$cate.grp_list[l].grp_code}>_65"
                           name="place_use_easy_item_code[]" value="65"
                           <{if $is_place_detail_use == true}>
                           checked
                           <{/if}>
                           style="margin-left:20px;"
                           onclick="setRelateCheck('110', '110_65');"
                           >
                    <label for="id_<{$cate.grp_list[l].grp_code}>_65">発生場所詳細</label>
                  </font>
                </td>
                <td bgcolor="#FFFFFF" width="30"></td>
                <td bgcolor="#FFFFFF" width="370"></td>
              </tr>

            <{elseif $cate.grp_list[l].grp_code == "210"}>
            <!-- 患者プロフィール -->
              <tr>
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" style="margin-left:5px;"><{$cate.grp_list[l].grp_name}></font>
                </td>
                <td bgcolor="#FFFFFF">
                </td>
                <td bgcolor="#FFFFFF">
                </td>
                <td bgcolor="#FFFFFF">
                </td>
              </tr>

              <{section name=m loop=$item_mst_patient}>
                <tr id="id_<{$cate.grp_list[l].grp_code}>_area">
                  <td bgcolor="#FFFFFF">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                      <input type="checkbox"
                              id="id_<{$item_mst_patient[m].grp_code}>_<{$item_mst_patient[m].easy_item_code}>"
                              name="patient_use_easy_item_code[]" value="<{$item_mst_patient[m].easy_item_code}>"
                              <{if in_array($item_mst_patient[m].easy_item_code, $patient_use)}>
                              checked
                              <{/if}>
                              onclick="
                                      var this_grp_code = <{$cate.grp_list[l].grp_code}>;
                                      setMustUsable(this_grp_code);
                                      "
                              style="margin-left:20px;"
                              >
                      <label for="id_<{$item_mst_patient[m].grp_code}>_<{$item_mst_patient[m].easy_item_code}>"><{$item_mst_patient[m].easy_item_name}></label>
                    </font>
                  </td>
                  <td bgcolor="#FFFFFF"></td>
                  <td bgcolor="#FFFFFF" align="center">
                    <input type="checkbox"
                          id="id_must_<{$item_mst_patient[m].grp_code}>_<{$item_mst_patient[m].easy_item_code}>"
                          name="patient_must_easy_item_code[]"
                          value="<{$item_mst_patient[m].easy_item_code}>"
                          <{if in_array($item_mst_patient[m].easy_item_code, $patient_must)}>
                          checked
                          <{/if}>
                          >
                  </td>
                  <td bgcolor="#FFFFFF">
                  </td>
                </tr>
              <{/section}>

            <{elseif $cate.grp_list[l].grp_code >= "310" and $cate.grp_list[l].grp_code <= "390"}>
            <!-- 関連する患者情報 -->
              <{if $cate.grp_list[l].grp_code == "310"}>
                <tr id="id_patients_line"></tr>
                <tr id="id_patients_info">
                  <td bgcolor="#FFFFFF">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" style="margin-left:5px;">関連する患者情報</font>
                  </td>
                  <td bgcolor="#FFFFFF"></td>
                  <td bgcolor="#FFFFFF"></td>
                  <td bgcolor="#FFFFFF"></td>
                </tr>
              <{/if}>

              <tr id="id_<{$cate.grp_list[l].grp_code}>_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_<{$cate.grp_list[l].grp_code}>"
                           name="use_grp_code[]" value="<{$cate.grp_list[l].grp_code}>"
                           <{if in_array( $cate.grp_list[l].grp_code, $flags)}>
                           checked
                           <{/if}>
                           onclick="
                                   var this_grp_code = <{$cate.grp_list[l].grp_code}>;
                                   setMustUsable(this_grp_code);
                                   "
                           style="margin-left:20px;"
                           >
                    <label for="id_<{$cate.grp_list[l].grp_code}>"><{$cate.grp_list[l].grp_name}></label>
                  </font>
                </td>
                <td bgcolor="#FFFFFF"></td>
                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_<{$cate.grp_list[l].grp_code}>"
                         name="must_grp_code[]"
                         value="<{$cate.grp_list[l].grp_code}>"
                         <{if in_array( $cate.grp_list[l].grp_code, $must_flags)}>
                         checked
                         <{/if}>
                         >
                </td>
                <td bgcolor="#FFFFFF"></td>
              </tr>

            <{elseif $cate.grp_list[l].grp_code == "120"}>
            <!-- インシデントの概要の項目グループ -->
              <tr id="id_700_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                          id="id_700"
                          name="use_grp_code[]" value="700"
                          <{if in_array( 700, $flags)}>
                          checked
                          <{/if}>
                          onclick="
                                  var this_grp_code = 700;
                                  setMustUsable(this_grp_code);

                                  var parent_grp_code = this_grp_code;
                                  var child_grp_code = '800';
                                  setRelateCheck( parent_grp_code , child_grp_code );

                                  setDisabled(700);
                                  "
                          >
                    <label for="id_700">発生した場面と内容に関する情報</label>
                  </font>
                </td>
                <td bgcolor="#FFFFFF"></td>
                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_700"
                         name="info_must_easy_item_code[]"
                         value="10"
                         <{if in_array( 700, $must_flags)}>
                         checked
                         <{/if}>
                         >
                </td>
                <td bgcolor="#FFFFFF"></td>
              </tr>

              <tr id="id_800_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_800"
                           name="use_grp_code[]"
                           value="800"
                           <{if in_array( 800, $flags)}>
                           checked
                           <{/if}>
                           style="margin-left:20px;"
                           onclick="
                                   var this_grp_code = '800';
                                   setMustUsable(this_grp_code);

                                   var parent_grp_code = 700;
                                   var child_grp_code = this_grp_code;
                                   setRelateCheck( parent_grp_code , child_grp_code );

                                   setDisabled(800);
                                   "
                           >
                    <label for="id_800">医薬品、医療・歯科医療用具（機器）、諸物品が要因と考えられる事例に関する追加項目</label><br>
                  </font>
                </td>
                <td bgcolor="#FFFFFF"></td>
                <td bgcolor="#FFFFFF" align="center">
                <input type="checkbox"
                       id="id_must_800"
                       name="item_must_easy_item_code[]"
                       value="10"
                       <{if in_array( 800, $must_flags)}>
                       checked
                       <{/if}>
                       >
                </td>
                <td bgcolor="#FFFFFF"></td>
              </tr>

              <tr id="id_120_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_<{$cate.grp_list[l].grp_code}>"
                           name="use_grp_code[]" value="<{$cate.grp_list[l].grp_code}>"
                           <{if in_array( $cate.grp_list[l].grp_code, $flags)}>
                           checked
                           <{/if}>
                           onclick="
                                   var this_grp_code = 120;
                                   setMustUsable(this_grp_code);

                                   var parent_grp_code = this_grp_code;
                                   var child_grp_code = '400_110';
                                   setRelateCheck( parent_grp_code , child_grp_code );
                                   var child_grp_code = '400_120';
                                   setRelateCheck( parent_grp_code , child_grp_code );
                                   var child_grp_code = '400_130';
                                   setRelateCheck( parent_grp_code , child_grp_code );

                                   setDisabled(120);
                                   "
                           >
                    <label for="id_<{$cate.grp_list[l].grp_code}>"><{$cate.grp_list[l].grp_name}></label>
                  </font>
                </td>

                <{* 文言変更対応 START awaji *}>
                <td bgcolor="#FFFFFF" align="right">
                  <{assign var="gc" value=$cate.grp_list[l].grp_code}>
                  <input type="text" name="name_<{$gc}>" value="<{$eis_item_title.$gc}>">
                </td>
                <{* 文言変更対応 END awaji *}>

                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_<{$cate.grp_list[l].grp_code}>"
                         name="must_grp_code[]"
                         value="<{$cate.grp_list[l].grp_code}>"
                         <{if in_array( $cate.grp_list[l].grp_code, $must_flags)}>
                         checked
                         <{/if}>
                         >
                </td>
                <td bgcolor="#FFFFFF"></td>
              </tr>

              <tr id="id_400_110_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_400_110"
                           name="summary_use_item_grp_code[]"
                           value="110"
                           <{if in_array(110, $summary_use)}>
                           checked
                           <{/if}>
                           style="margin-left:20px;"
                           onclick="
                                   var this_grp_code = '400_110';
                                   setMustUsable(this_grp_code);

                                   var parent_grp_code = 120;
                                   var child_grp_code = this_grp_code;
                                   setRelateCheck( parent_grp_code , child_grp_code );

                                   setDisabled(400);
                                   "
                           >
                    <label for="id_400_110">発生場面</label><br>
                  </font>
                </td>
                <td bgcolor="#FFFFFF"></td>
                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_400_110"
                         name="summary_must_item_grp_code[]"
                         value="110"
                         <{if in_array(110, $summary_must)}>
                         checked
                         <{/if}>
                         >
                </td>
                <td bgcolor="#FFFFFF"></td>
              </tr>

              <tr id="id_400_120_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_400_120"
                           name="summary_use_item_grp_code[]"
                           value="120"
                           <{if in_array(120, $summary_use)}>
                           checked
                           <{/if}>
                           style="margin-left:20px;"
                           onclick="
                                   var this_grp_code = '400_120';
                                   setMustUsable(this_grp_code);

                                   var parent_grp_code = 120;
                                   var child_grp_code = this_grp_code;
                                   setRelateCheck( parent_grp_code , child_grp_code );

                                   setDisabled(400);
                                   "
                           >
                    <label for="id_400_120">内容</label><br>
                  </font>
                </td>
                <td bgcolor="#FFFFFF"></td>
                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_400_120"
                         name="summary_must_item_grp_code[]"
                         value="120"
                         <{if in_array(120, $summary_must)}>
                         checked
                         <{/if}>
                         >
                </td>
                <td bgcolor="#FFFFFF"></td>
              </tr>

              <tr id="id_400_130_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_400_130"
                           name="summary_use_item_grp_code[]"
                           value="130"
                           <{if in_array(130, $summary_use)}>
                           checked
                           <{/if}>
                           style="margin-left:20px;"
                           onclick="
                                   var this_grp_code = '400_130';
                                   setMustUsable(this_grp_code);

                                   var parent_grp_code = 120;
                                   var child_grp_code = this_grp_code;
                                   setRelateCheck( parent_grp_code , child_grp_code );

                                   setDisabled(400);
                                   "
                           >
                    <label for="id_400_130">内容の補足記載</label><br>
                  </font>
                </td>
                <td bgcolor="#FFFFFF"></td>
                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_400_130"
                         name="summary_must_item_grp_code[]"
                         value="130"
                         <{if in_array(130, $summary_must)}>
                         checked
                         <{/if}>
                         >
                </td>
                <td bgcolor="#FFFFFF"></td>
              </tr>

              <tr id="id_900_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_900"
                           name="use_grp_code[]" value="900"
                           <{if in_array( 900, $flags)}>
                           checked
                           <{/if}>
                           onclick="
                                   var this_grp_code = 900;
                                   setMustUsable(this_grp_code);
                                   setDisabled(900);
                                   "
                           >
                    <label for="id_900">事例の概要</label>
                  </font>
                </td>
                <td bgcolor="#FFFFFF" align="right">
                  <{assign var="gc" value="900"}>
                  <input type="text" name="name_<{$gc}>" value="<{$eis_item_title.$gc}>">
                </td>
                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_900"
                         name="item_must_2010_easy_item_code[]"
                         value="10"
                         <{if in_array( 900, $must_flags)}>
                         checked
                         <{/if}>
                         >
                </td>
                <td bgcolor="#FFFFFF">
                  <select id="method_2010" name="def_900_10" onchange="set_method_2010(false);">
                    <option value=""></option>
                    <{foreach from=$method item=m}>
                      <option value="<{$m.super_item_id}>">
                        <{$m.super_item_name|escape:"html"}>
                      </option>
                    <{/foreach}>
                  </select>
                </td>
              </tr>
              <tr id="id_910_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <p style="margin-left:25px;">種類</p>
                  </font>
                </td>
                <td bgcolor="#FFFFFF"></td>
                <td bgcolor="#FFFFFF" align="center"></td>
                <td bgcolor="#FFFFFF">
                  <select id="kind_2010" name="def_910_10" onchange="set_kind_item_2010(this.value, false);"></select>
                </td>
              </tr>

              <tr id="id_920_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <p style="margin-left:70px;">項目</p>
                  </font>
                </td>
                <td bgcolor="#FFFFFF"></td>
                <td bgcolor="#FFFFFF" align="center"></td>
                <td bgcolor="#FFFFFF">
                  <select id="kind_item_2010" name="def_920_10"></select>
                </td>
              </tr>

              <tr id="id_940_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <p style="margin-left:25px;">発生場面</p>
                  </font>
                </td>
                <td bgcolor="#FFFFFF"></td>
                <td bgcolor="#FFFFFF" align="center"></td>
                <td bgcolor="#FFFFFF">
                  <select id="scene_2010"  name="def_940_10" onchange="set_scene_item_2010(this.value, false);"></select>
                </td>
              </tr>

              <tr id="id_950_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <p style="margin-left:70px;">項目</p>
                  </font>
                </td>
                <td bgcolor="#FFFFFF"></td>
                <td bgcolor="#FFFFFF" align="center"></td>
                <td bgcolor="#FFFFFF">
                  <select id="scene_item_2010" name="def_950_10"></select>
                </td>
              </tr>

              <tr id="id_970_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <p style="margin-left:25px;">事例の内容</p>
                  </font>
                </td>
                <td bgcolor="#FFFFFF"></td>
                <td bgcolor="#FFFFFF" align="center"></td>
                <td bgcolor="#FFFFFF">
                  <select id="content_2010" name="def_970_10" onchange="set_content_item_2010(this.value, false);"></select>
                </td>
              </tr>

              <tr id="id_980_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <p style="margin-left:70px;">項目</p>
                  </font>
                </td>
                <td bgcolor="#FFFFFF"></td>
                <td bgcolor="#FFFFFF" align="center"></td>
                <td bgcolor="#FFFFFF">
                  <select id="content_item_2010" name="def_980_10"></select>
                </td>
              </tr>
              <tr id="id_1300_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_1300"
                           name="use_grp_code[]" value="1300"
                           <{if in_array( 1300, $flags)}>
                           checked
                           <{/if}>
                           onclick="
                                   var this_grp_code = 1300;
                                   setMustUsable(this_grp_code);

                                   setDisabled(1300);
                                   "
                           >
                    <label for="id_1300">関連医薬品</label>
                  </font>
                </td>
                <td bgcolor="#FFFFFF" align="right">
                </td>
                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_1300"
                         name="must_grp_code[]"
                         value="1300"
                         <{if in_array( 1300, $must_flags)}>
                         checked
                         <{/if}>
                         >
                </td>
                <td bgcolor="#FFFFFF"></td>
              </tr>

              <tr id="id_1310_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_1310"
                           name="use_grp_code[]" value="1310"
                           <{if in_array( 1310, $flags)}>
                           checked
                           <{/if}>
                           onclick="
                                   var this_grp_code = 1310;
                                   setMustUsable(this_grp_code);

                                   setDisabled(1310);
                                   "
                           >
                    <label for="id_1310">医療材料・諸物品等</label>
                  </font>
                </td>
                <td bgcolor="#FFFFFF" align="right">
                </td>
                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_1310"
                         name="must_grp_code[]"
                         value="1310"
                         <{if in_array( 1310, $must_flags)}>
                         checked
                         <{/if}>
                         >
                </td>
                <td bgcolor="#FFFFFF"></td>
              </tr>

              <tr id="id_1320_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_1320"
                           name="use_grp_code[]" value="1320"
                           <{if in_array( 1320, $flags)}>
                           checked
                           <{/if}>
                           onclick="
                                   var this_grp_code = 1320;
                                   setMustUsable(this_grp_code);

                                   setDisabled(1320);
                                   "
                           >
                    <label for="id_1320">医療機器等</label>
                  </font>
                </td>
                <td bgcolor="#FFFFFF" align="right">
                </td>
                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_1320"
                         name="must_grp_code[]"
                         value="1320"
                         <{if in_array( 1320, $must_flags)}>
                         checked
                         <{/if}>
                         >
                </td>
                <td bgcolor="#FFFFFF"></td>
              </tr>

            <{elseif $cate.grp_list[l].grp_code == "290"}>
            <!-- インシデント直前の患者の状態の項目グループ -->
              <tr id="id_290_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_<{$cate.grp_list[l].grp_code}>"
                           name="use_grp_code[]" value="<{$cate.grp_list[l].grp_code}>"
                           <{if in_array( $cate.grp_list[l].grp_code, $flags)}>
                           checked
                           <{/if}>
                           onclick="
                                   var this_grp_code = <{$cate.grp_list[l].grp_code}>;
                                   setMustUsable(this_grp_code);

                                   var parent_grp_code = this_grp_code;
                                   var child_grp_code = '295';
                                   setRelateCheck( parent_grp_code , child_grp_code );
                                   "
                           >
                    <label for="id_<{$cate.grp_list[l].grp_code}>"><{$cate.grp_list[l].grp_name}></label>
                  </font>
                </td>
                <td bgcolor="#FFFFFF" align="right">
                  <{assign var="gc" value=$cate.grp_list[l].grp_code}>
                  <input type="text" name="name_<{$gc}>" value="<{$eis_item_title.$gc}>">
                </td>
                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_<{$cate.grp_list[l].grp_code}>"
                         name="must_grp_code[]"
                         value="<{$cate.grp_list[l].grp_code}>"
                         <{if in_array( $cate.grp_list[l].grp_code, $must_flags)}>
                         checked
                         <{/if}>
                         >
                </td>
                <td bgcolor="#FFFFFF"></td>
              </tr>

            <{elseif $cate.grp_list[l].grp_code == "295"}>
            <!-- インシデント直前の患者の状態詳細の項目グループ -->
              <tr id="id_295_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_<{$cate.grp_list[l].grp_code}>"
                           name="use_grp_code[]" value="<{$cate.grp_list[l].grp_code}>"
                           <{if in_array( $cate.grp_list[l].grp_code, $flags)}>
                           checked
                           <{/if}>
                           style="margin-left:20px;"
                           onclick="
                                   var this_grp_code = <{$cate.grp_list[l].grp_code}>;
                                   setMustUsable(this_grp_code);

                                   var parent_grp_code = 290;
                                   var child_grp_code = this_grp_code;
                                   setRelateCheck( parent_grp_code , child_grp_code );

                                   "
                           >
                    <label for="id_<{$cate.grp_list[l].grp_code}>"><{$cate.grp_list[l].grp_name}></label>
                  </font>
                </td>
                <td bgcolor="#FFFFFF" align="right">
                  <{assign var="gc" value=$cate.grp_list[l].grp_code}>
                  <input type="text" name="name_<{$gc}>" value="<{$eis_item_title.$gc}>">
                </td>
                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_<{$cate.grp_list[l].grp_code}>"
                         name="must_grp_code[]"
                         value="<{$cate.grp_list[l].grp_code}>"
                         <{if in_array( $cate.grp_list[l].grp_code, $must_flags)}>
                         checked
                         <{/if}>
                         >
                </td>
                <td bgcolor="#FFFFFF"></td>
              </tr>

            <{elseif $cate.grp_list[l].grp_code == "600"}>
            <!-- 発生要因の項目グループ -->
              <tr id="id_600_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_<{$cate.grp_list[l].grp_code}>"
                           name="use_grp_code[]" value="<{$cate.grp_list[l].grp_code}>"
                           <{if in_array( $cate.grp_list[l].grp_code, $flags)}>
                           checked
                           <{/if}>
                           onclick="
                                   var this_grp_code = <{$cate.grp_list[l].grp_code}>;
                                   setMustUsable(this_grp_code);

                                   var parent_grp_code = this_grp_code;
                                   var child_grp_code = '605';
                                   setRelateCheck( parent_grp_code , child_grp_code );
                                   "
                           >
                    <label for="id_<{$cate.grp_list[l].grp_code}>"><{$cate.grp_list[l].grp_name}></label>
                  </font>
                </td>
                <td bgcolor="#FFFFFF"></td>
                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_<{$cate.grp_list[l].grp_code}>"
                         name="must_grp_code[]"
                         value="<{$cate.grp_list[l].grp_code}>"
                         <{if in_array( $cate.grp_list[l].grp_code, $must_flags)}>
                         checked
                         <{/if}>
                         >
                </td>
                <td bgcolor="#FFFFFF"></td>
              </tr>

            <{elseif $cate.grp_list[l].grp_code == "605"}>
            <!-- 発生要因詳細の項目グループ -->
              <tr id="id_605_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_<{$cate.grp_list[l].grp_code}>"
                           name="use_grp_code[]" value="<{$cate.grp_list[l].grp_code}>"
                           <{if in_array( $cate.grp_list[l].grp_code, $flags)}>
                           checked
                           <{/if}>
                           style="margin-left:20px;"
                           onclick="
                                   var this_grp_code = <{$cate.grp_list[l].grp_code}>;
                                   setMustUsable(this_grp_code);

                                   var parent_grp_code = 600;
                                   var child_grp_code = this_grp_code;
                                   setRelateCheck( parent_grp_code , child_grp_code );

                                   "
                           >
                    <label for="id_<{$cate.grp_list[l].grp_code}>"><{$cate.grp_list[l].grp_name}></label>
                  </font>
                </td>
                <td bgcolor="#FFFFFF"></td>
                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_<{$cate.grp_list[l].grp_code}>"
                         name="must_grp_code[]"
                         value="<{$cate.grp_list[l].grp_code}>"
                         <{if in_array( $cate.grp_list[l].grp_code, $must_flags)}>
                         checked
                         <{/if}>
                         >
                </td>
                <td bgcolor="#FFFFFF"></td>
              </tr>

            <{elseif $cate.grp_list[l].grp_code == "1130"}>
            <!-- 今期のテーマ -->
              <tr id="id_<{$cate.grp_list[l].grp_code}>_area">
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <label for="id_<{$cate.grp_list[l].grp_code}>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<{$cate.grp_list[l].grp_name}></label>
                  </font>
                  <input type="text" name="this_term_theme" value="<{$theme}>" size="60">
                </td>
                <td bgcolor="#FFFFFF"></td>
                <td bgcolor="#FFFFFF"></td>
              </tr>

            <{else}>
            <!-- 通常項目 -->
              <tr id="id_<{$cate.grp_list[l].grp_code}>_area">
                <{if $cate.cate_code == 10}>
                  <td class="dragHandle" bgcolor="#FFFFFF"></td>
                <{/if}>
                <td bgcolor="#FFFFFF">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="checkbox"
                           id="id_<{$cate.grp_list[l].grp_code}>"
                           name="use_grp_code[]" value="<{$cate.grp_list[l].grp_code}>"
                           <{if in_array( $cate.grp_list[l].grp_code, $flags)}>
                           checked
                           <{/if}>
                           onclick="
                                   var this_grp_code = <{$cate.grp_list[l].grp_code}>;
                                   setMustUsable(this_grp_code);
                                   "
                           >
                    <label for="id_<{$cate.grp_list[l].grp_code}>"><{$cate.grp_list[l].grp_name}></label>
                  </font>
                </td>

                <{* 文言変更対応 START awaji *}>
                <{if $cate.cate_code >= 2 && $cate.cate_code <= 7 || $cate.cate_code == 10}>
                  <td bgcolor="#FFFFFF" align="right">
                    <{if $cate.grp_list[l].grp_code == 125 ||
                        $cate.grp_list[l].grp_code == 3300 ||
                        $cate.grp_list[l].grp_code == 520 ||
                        $cate.grp_list[l].grp_code == 525 ||
                        $cate.grp_list[l].grp_code == 530 ||
                        $cate.grp_list[l].grp_code == 96 ||
                        $cate.grp_list[l].grp_code == 610 ||
                        $cate.grp_list[l].grp_code == 630 ||
                        $cate.grp_list[l].grp_code == 250 ||
                        $cate.grp_list[l].grp_code == 290 ||
                        $cate.grp_list[l].grp_code == 295 ||
                        $cate.grp_list[l].grp_code == 120 ||
                        $cate.grp_list[l].grp_code == 240 ||
                        $cate.grp_list[l].grp_code == 1400 ||
                        $cate.grp_list[l].grp_code == 1410 ||
                        $cate.grp_list[l].grp_code == 1500 ||
                        $cate.grp_list[l].grp_code == 1502 ||
                        $cate.grp_list[l].grp_code == 1504 ||
                        $cate.grp_list[l].grp_code == 1510 ||
                        $cate.grp_list[l].grp_code == 1520 ||
                        $cate.grp_list[l].grp_code == 1530 ||
                        $cate.grp_list[l].grp_code == 130 ||
                        $cate.grp_list[l].grp_code == 900 ||
                        $cate.grp_list[l].grp_code >= 10000}>
                      <{assign var="gc" value=$cate.grp_list[l].grp_code}>
                      <input type="text" name="name_<{$gc}>" value="<{$eis_item_title.$gc}>">
                    <{/if}>
                  </td>
                <{/if}>
                <{* 文言変更対応 END awaji *}>

                <td bgcolor="#FFFFFF" align="center">
                  <input type="checkbox"
                         id="id_must_<{$cate.grp_list[l].grp_code}>"
                         name="must_grp_code[]"
                         value="<{$cate.grp_list[l].grp_code}>"
                         <{if in_array( $cate.grp_list[l].grp_code, $must_flags)}>
                         checked
                         <{/if}>
                         >
                </td>

                <td bgcolor="#FFFFFF">
                  <{if $cate.grp_list[l].grp_code == "520"}>
                  <!-- インシデントの内容 -->
                    <textarea name="def_520_30" style="width:100%;" rows="12"><{if isset($default[520][30])}><{$default[520][30]}><{/if}></textarea>
                  <{/if}>
                </td>


            <{/if}>

                <{*最大文字数入力 *}>
                <{if $cate.cate_code == 5 || $cate.cate_code == 6 || $cate.cate_code == 7}>

                    <{if
                        $cate.grp_list[l].grp_code == 500 ||
                        $cate.grp_list[l].grp_code == 520 ||
                        $cate.grp_list[l].grp_code == 525 ||
                        $cate.grp_list[l].grp_code == 530 ||
                        $cate.grp_list[l].grp_code == 540 ||
                        $cate.grp_list[l].grp_code == 550 ||
                        $cate.grp_list[l].grp_code == 610 ||
                        $cate.grp_list[l].grp_code == 620 ||
                        $cate.grp_list[l].grp_code == 625 ||
                        $cate.grp_list[l].grp_code == 630 ||
                        $cate.grp_list[l].grp_code == 635 }>
                        <td bgcolor="#FFFFFF" align="right">
                      <{assign var="gc" value=$cate.grp_list[l].grp_code}>
                      <input type="text" name="letter_<{$gc}>" value="<{$eis_number_of_charcter.$gc}>">
                       </td>
                    <{/if}>

                <{/if}>
                <{* 最大文字数入力 END awaji *}>

                 </tr>

          <{/section}>

        </table>
        <!-- 入力表 END -->

    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    </tr>
    <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
    </tr>
    </table>
    <!-- 入力表の枠 END -->

<{/foreach}>
<!-- 一覧 END-->











<br>

<div>
<input type="button" name="preview" value="プレビュー" onclick="show_preview()">
<input type="button" name="register" value="登録" onclick="regist()">
</div>
<br>

</form>
<!-- フッター START -->
<{php}>
    show_hiyari_footer($this->_tpl_vars['session'],$this->_tpl_vars['fname'],true);

<{/php}>
<!-- フッター END -->

</td>
</tr>
</table>
</body>
</html>
