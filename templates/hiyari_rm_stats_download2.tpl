<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | 統計分析：定型集計</title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/rm_stats.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/hiyari_rm_stats.js"></script>
  <{include file="hiyari_post_select_js.tpl"}>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
  <script type="text/javascript">
  $(function($){
	  //集計ダウンロード表示
	  <{if $stats_download_mode == true}>
	     var sub_menu_width = document.getElementById('sub_menu');
	     sub_menu_width.style.width = "630px";
	  <{/if}>
	  
      //チェックボックス
      $('.checkbox').click(function(){
          $(this).closest('.checkbox_wrap').toggleClass('checked');
      });
  });
  
    function init_page()
    {    
      <{* 集計部署の選択肢の初期化 *}>
      <{section name=cnt loop=$belong_cnt}>
          set_post_select_box_obj_key("belong_<{$smarty.section.cnt.index}>",
                                      "belong_class_<{$smarty.section.cnt.index}>",
                                      "belong_attribute_<{$smarty.section.cnt.index}>",
                                      "belong_dept_<{$smarty.section.cnt.index}>",
                                      "belong_room_<{$smarty.section.cnt.index}>");
          setClassOptions("belong_<{$smarty.section.cnt.index}>",
                          "<{$belong_class[$smarty.section.cnt.index]}>",
                          "<{$belong_attribute[$smarty.section.cnt.index]}>",
                          "<{$belong_dept[$smarty.section.cnt.index]}>",
                          "<{$belong_room[$smarty.section.cnt.index]}>");
      <{/section}>
    }
      
    function submit_func(mode)
    {
      document.main_form.mode.value = mode;
      document.main_form.submit();
    }

    //EXCEL
    function excel_download()
    {
      document.main_form.mode.value = 'excel';
      document.main_form.submit();
    }

    //部署
    function belong_add(obj) {
      obj.mode.value = "add_belong";
      obj.submit();
    }
    function belong_delete(obj, no) {
      obj.mode.value = "del_belong";
      obj.del_line_no.value = no;
      obj.submit();
    }
  </script>
</head>

<body id="top" onload="init_page()";>
  <{*ヘッダー*}>
  <{include file="hiyari_header2.tpl"}>

  <div id="content" class="<{$tab_info.total_width_class}>">
    <{* 上部ボタン *}>
    <div id="sub_menu" class="color01 radius">
      <table class="v_title">
        <tr>
          <th class="none">
            <img src="img/side_nav_txt01.gif" alt="MENU" />
          </th>
          <td class="none">
            <a href="hiyari_rm_stats.php?session=<{$session}>">クロス集計</a>
          </td>
          <td class="none">
            <a href="hiyari_rm_stats_pattern.php?session=<{$session}>">定型集計</a>
          </td>
          <{if $stats_download_mode == true}>
	          <td class="active none">
	            <a href="hiyari_rm_stats_download.php?session=<{$session}>">集計ダウンロード</a>
	          </td>
	      <{/if}>
        </tr>
      </table>
    </div>
    
    <!----------------------------------------------------------------------------------- 
    集計条件領域 START
    ------------------------------------------------------------------------------------>
    <form name="main_form" method="post" action="hiyari_rm_stats_download.php?session=<{$session}>">
      <input type="hidden" name="mode" value="">
      <!-- 集計条件:所属 -->
      <input type="hidden" name="belong_cnt" value="<{$belong_cnt}>">
      <input type="hidden" name="del_line_no" value="">
      
      <div id="conditions1" class="color01 radius">
        <table class="v_title">
          <tr>
            <th width="100">集計モード</th>
            <td colspan="<{$post_select_data.class_cnt+1}>">
              <!-- 集計条件:集計モード -->
              <select name="date_mode">
                <option value="report_date" <{$report_date_str}>>報告日</option>
                <option value="incident_date" <{$incident_date_str}>>発生日</option>
              </select>
              <!-- 集計条件:副報告 -->
              <label for="sub_report_use">
                <span class="checkbox_wrap<{if $sub_report_use}> checked<{/if}>">
                  <input type="checkbox" class="mL10 checkbox" id="sub_report_use" name="sub_report_use" value="true" <{if $sub_report_use}>checked<{/if}> >
                </span>
              副報告を集計に含める
              </label>
            </td>
          </tr>
          
          <!-- 集計条件:集計年月 -->
          <tr>
<!-- [集計条件:所属]を暫定で非表示にすることに伴うレイアウト調整の為、変更([集計条件:所属]表示時は、class="none"を削除する) 2014/03/13         
            <th>集計年月</th>
            <td colspan="<{$post_select_data.class_cnt+1}>">
-->            
            <th class="none">集計年月</th>
            <td class="none" colspan="<{$post_select_data.class_cnt+1}>">
              <select name="date_year">
                <{foreach from=$year_list item=y}>
                  <option value="<{$y}>" <{if $date_year==$y}>selected<{/if}>><{$y}></option>
                <{/foreach}>
              </select>年
              
              <select name="date_month">
                <{foreach from=$month_list item=m}>
                  <option value="<{$m|string_format:'%02d'}>" <{if $date_month==$m}>selected<{/if}>><{$m}></option>
                <{/foreach}>
              </select>月〜
              
              <select name="date_term">
                <{foreach from=$month_list item=t}>
                  <option value="<{$t|string_format:'%02d'}>" <{if $date_term==$t}>selected<{/if}>><{$t}>ヶ月分</option>
                <{/foreach}>
              </select>
            </td>
          </tr>    
                              
          <!-- 集計条件:所属 -->
<!-- [集計条件:所属]は暫定で非表示 2014/03/13
          <{section name=cnt loop=$belong_cnt}>
            <tr>
              <{if $smarty.section.cnt.index == 0}>
                <th class="none" rowspan="<{$belong_cnt}>">所属</th>
              <{/if}>
            
              <td <{if $smarty.section.cnt.index == $belong_cnt-1}>class="none"<{/if}>>
                <div id="busho_data">
                  <{$arr_class_name[0]|escape}>
                  <select id="belong_class_<{$smarty.section.cnt.index}>" name="belong_class[]" onchange="setAtrbOptions('belong_<{$smarty.section.cnt.index}>');"></select>

                  <{$arr_class_name[1]|escape}>
                  <select id="belong_attribute_<{$smarty.section.cnt.index}>" name="belong_attribute[]" onchange="setDeptOptions('belong_<{$smarty.section.cnt.index}>');"></select>

                  <{$arr_class_name[2]|escape}>
                  <select id="belong_dept_<{$smarty.section.cnt.index}>" name="belong_dept[]" onchange="setRoomOptions('belong_<{$smarty.section.cnt.index}>');"></select>
                  
                  <{if $arr_class_name.class_cnt == 4}>
                    <{$arr_class_name[3]|escape}>
                    <select id="belong_room_<{$smarty.section.cnt.index}>" name="belong_room[]"></select>
                  <{/if}>
                </div>
                <div id="busho_btn">
                  <input class="button radius_mini" type="button" value="追加" onClick="belong_add(this.form)">
                  <{if !$smarty.section.cnt.first}>
                    <input class="button radius_mini" type="button" value="削除" onClick="belong_delete(this.form, <{$smarty.section.cnt.index}>)">
                  <{/if}>
                </div>
              </td>
            </tr>
          <{/section}>
-->          
        </table>
      </div>
      <div id="main_btn" style="text-align: left;">
        <input style="width: 200px;" class="button radius_mini" type="button" value="報告用EXCEL出力" onClick="excel_download()">
      </div>
    </form>
  </div>
  <div id="footer">
    <p>Copyright Comedix all right reserved.</p>
  </div>
</body>
</html>
