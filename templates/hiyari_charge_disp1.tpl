<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix <{$INCIDENT_TITLE}>｜担当者設定確認</title>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <style type="text/css">
    .list {border-collapse:collapse;}
    .list td {border:#35B341 solid 1px;}

    table.block {border-collapse:collapse;}
    table.block td {border:#35B341 solid 1px;padding:1px;}
    table.block td td {border-width:0;}

    table.block_in {border-collapse:collapse;}
    table.block_in td {border:#35B341 solid 0px;}
    table.block_in td td {border-width:1;}

    p.scroll {height:80px; overflow: scroll;}

    .non_in_list {border-collapse:collapse;}
    .non_in_list td {border:0px;}
  </style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr height="32" bgcolor="#35B341">
      <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>担当者設定確認</b></font></td>
      <td>&nbsp;</td>
      <td width="10">&nbsp;</td>
      <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
    </tr>
  </table>
  <img src="img/spacer.gif" width="10" height="10" alt=""><br>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
      <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
      <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
    </tr>
    <tr>
      <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
      <td>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td width="20%" bgcolor="#F5FFE5">
              <table cellspacing="0" cellpadding="0" border="0">
                <tr><td><img src="img/spacer.gif" width="1" height="1" alt=""></td></tr>
              </table>
            </td>
            <td width="60%">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#FFDDFD">
                  <td align="center" height="100%"><font size="4" face="ＭＳ Ｐゴシック, Osaka"><b><{$sel_auth_name}></b></font><td>
                </tr>
              </table>
            </td>
            <td width="20%" bgcolor="#F5FFE5">
              <table cellspacing="0" cellpadding="0" border="0">
                <tr><td><img src="img/spacer.gif" width="1" height="1" alt=""></td></tr>
              </table>
            </td>
          </tr>
        </table>

        <table width="100%" cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td width="20%" bgcolor="#F5FFE5">
              <table cellspacing="0" cellpadding="0" border="0">
                <tr><td><img src="img/spacer.gif" width="1" height="1" alt=""></td></tr>
              </table>
            </td>

            <td width="60%" bgcolor="#F5FFE5">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
              
                <!-- 決裁者の設定 -->
                <tr>
                  <td>
                    <table width="35%" border="0" cellspacing="0" cellpadding="0" class="list">
                      <tr height="22">
                        <td  bgcolor="#DFFFDC" align="center">
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">■決裁者の設定</font>
                        </td>
                      </tr>
                    </table>

                    <table cellspacing="0" cellpadding="0" border="0">
                      <tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
                    </table>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="block">
                      <{if $auth == 'RM' || $auth == 'RA' || $auth == 'SD' || $auth == 'MD'}>
                        <tr>
                          <td rowspan="2" width="20%" bgcolor="#DFFFDC" align="center">
                            <table border='0' cellspacing='0' cellpadding='0' class="block_in">
                              <tr>
                                <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署・役職</font>
                              </tr>
                            </table>
                          </td>
                          <td width="40%" bgcolor="#DFFFDC">
                            <table cellspacing="0" cellpadding="1" border="0">
                              <tr>
                                <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
                              </tr>
                            </table>
                          </td>
                          <td width="40%" bgcolor="#DFFFDC">
                            <table cellspacing="0" cellpadding="1" border="0">
                              <tr>
                                <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <{if $decide_use_flg == 't'}>
                            <td align="center" bgcolor="#FFFFFF">
                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者の所属する【<{$decide_class}>】</font>
                            </td>
                            <td bgcolor="#FFFFFF">
                              <table border="0" cellspacing="2" cellpadding="2">
                                <tr>
                                  <td>
                                    <table border="0" cellspacing="0" cellpadding="3" class="block_in">
                                      <tr valign="top" height="80">
                                        <td width="200">
                                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <{foreach name=n from=$decide_posts item=post}>
                                              <{if !$smarty.foreach.n.first}><br><{/if}>
                                              <{$post}>
                                            <{/foreach}>
                                          </font>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          <{elseif $decide_use_flg == 'f'}>
                            <td colspan="2" bgcolor="#FFFFFF">
                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">指定しない</font>
                            </td>
                          <{/if}>
                        </tr>
                      <{/if}>
                      
                      <{if $auth != 'SD'}>
                        <tr height="100">
                          <td width="20%" bgcolor="#DFFFDC" align="center">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
                          </td>
                          <td colspan="2" bgcolor="#FFFFFF">
                            <table width="100%" height="100%" border="0" cellspacing="3" cellpadding="0" class="non_in_list">
                              <tr>
                                <td style="border:#35B341 solid 1px;">
                                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$decide_target}></font>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      <{/if}>
                    </table>
                  </td>
                </tr>

                <tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
                
                <!-- 審議者の設定 -->
                <{if $auth == 'SM' || $auth == 'RM' || $auth == 'RA' || $auth == 'SD'}>
                  <tr>
                    <td>
                      <table width="35%" border="0" cellspacing="0" cellpadding="0" class="list">
                        <tr height="22">
                          <td  bgcolor="#DFFFDC" align="center">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">■審議者の設定</font>
                          </td>
                        </tr>
                      </table>

                      <table cellspacing="0" cellpadding="0" border="0">
                        <tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
                      </table>

                      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="block">
                        <{if $auth == 'RM' || $auth == 'RA' || $auth == 'SD'}>
                          <tr>
                            <td rowspan="2" width="20%" bgcolor="#DFFFDC" align="center">
                              <table border='0' cellspacing='0' cellpadding='0' class="block_in">
                                <tr>
                                  <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署・役職</font>
                                </tr>
                              </table>
                            </td>
                            <td width="40%" bgcolor="#DFFFDC">
                              <table cellspacing="0" cellpadding="1" border="0">
                                <tr>
                                  <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
                                </tr>
                              </table>
                            </td>
                            <td width="40%" bgcolor="#DFFFDC">
                              <table cellspacing="0" cellpadding="1" border="0">
                                <tr>
                                  <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                      
                          <tr>
                            <{if $discuss_use_flg == 't'}>
                              <td align="center" bgcolor="#FFFFFF">
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者の所属する【<{$discuss_class}>】</font>
                              </td>
                              <td bgcolor="#FFFFFF">
                                <table border="0" cellspacing="2" cellpadding="2">
                                  <tr>
                                    <td>
                                      <table border="0" cellspacing="0" cellpadding="3" class="block_in">
                                        <tr valign="top" height="80">
                                          <td width="200">
                                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                              <{foreach name=n from=$discuss_posts item=post}>
                                                <{if !$smarty.foreach.n.first}><br><{/if}>
                                                <{$post}>
                                              <{/foreach}>
                                            </font>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            <{elseif $discuss_use_flg == 'f'}>
                              <td colspan="2" bgcolor="#FFFFFF">
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">指定しない
                              </td>
                            <{/if}>
                          </tr>
                        <{/if}>
                        
                        <{if $auth != 'SD'}>
                          <tr height="100">
                            <td width="20%" bgcolor="#DFFFDC" align="center">
                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
                            </td>
                            <td colspan="2" bgcolor="#FFFFFF">
                              <table width="100%" height="100%" border="0" cellspacing="3" cellpadding="0" class="non_in_list">
                                <tr>
                                  <td style="border:#35B341 solid 1px;">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$discuss_target}></font>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        <{/if}>
                      </table>
                    </td>
                  </tr>
                <{/if}>
                
              </table>
            </td>
            
            <td width="20%" bgcolor="#F5FFE5">
              <table cellspacing="0" cellpadding="0" border="0">
                <tr><td><img src="img/spacer.gif" width="1" height="1" alt=""></td></tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    </tr>
    <tr>
      <td><img src="img/r_3.gif" width="10" height="10"></td>
      <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
      <td><img src="img/r_4.gif" width="10" height="10"></td>
    </tr>
  </table>
</body>
</html>
