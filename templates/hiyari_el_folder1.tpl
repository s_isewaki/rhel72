<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/hiyari_el.js"></script>
  <script type="text/javascript">
    //--------------------------------------------------
    //登録対象者配列
    //--------------------------------------------------
    m_target_list = new Array();
    //参照権限の対象職員
    m_target_list[1] = new Array(
        <{foreach name=n from=$arr_target[1] item=i}>
          <{if !$smarty.foreach.n.first}>,<{/if}>
          new user_info('<{$i.id}>','<{$i.name}>')
        <{/foreach}>
      );
    //更新権限の対象職員
    m_target_list[2] = new Array(
        <{foreach name=n from=$arr_target[2] item=i}>
          <{if !$smarty.foreach.n.first}>,<{/if}>
          new user_info('<{$i.id}>','<{$i.name}>')
        <{/foreach}>
      );

    //--------------------------------------------------
    //部署情報配列
    //--------------------------------------------------
    //部
    var classes = [];
    <{foreach from=$sel_class item=i}>
      classes.push({id: '<{$i.class_id}>', name: '<{$i.class_nm}>'});
    <{/foreach}>

    //課
    var atrbs = {};
    <{foreach from=$sel_atrb key=class_id item=c}>
      atrbs['<{$class_id}>'] = [];
      <{foreach from=$c key=atrb_id item=a}>
        atrbs['<{$class_id}>'].push({id: '<{$atrb_id}>', name: '<{$a}>'});
      <{/foreach}>
    <{/foreach}>

    //科
    var depts = {};
    <{foreach from=$sel_dept key=class_id item=c}>
      depts['<{$class_id}>'] = {};
      <{foreach from=$c key=atrb_id item=a}>
        depts['<{$class_id}>']['<{$atrb_id}>'] = [];
        <{foreach from=$a key=dept_id item=d}>
          depts['<{$class_id}>']['<{$atrb_id}>'].push({id: '<{$dept_id}>', name: '<{$d}>'});
        <{/foreach}>
      <{/foreach}>
    <{/foreach}>

    //--------------------------------------------------
    //画面起動時の処理
    //--------------------------------------------------
    function initPage()
    {
        //登録対象者を設定する。
        update_target_html("1");
        update_target_html("2");

        //権限部分の初期制御
        onChangeArchive(true, '<{$ref_class_src}>', '<{$ref_atrb_src}>', '<{$upd_class_src}>', '<{$upd_atrb_src}>');
    }
  </script>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
  <script type="text/javascript" src="js/yui/build/connection/connection-min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <style type="text/css">
    .list {border-collapse:collapse; border:#5279a5 solid 1px;}
    .list td {border:#5279a5 solid 1px;}
    .list td td {border-width:0;}
    .non_in_list {border-collapse:collapse;}
    .non_in_list td {border:0px;}
  </style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <!-- ヘッダー START -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr height="32" bgcolor="#35B341">
            <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><{$PAGE_TITLE}></b></font></td>
            <td>&nbsp;</td>
            <td width="10">&nbsp;</td>
            <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
          </tr>
        </table>
        <img src="img/spacer.gif" width="10" height="10" alt=""><br>
        <!-- ヘッダー END -->
      </td>
    </tr>
    <tr>
      <td>
        <!-- 本体 START -->
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
            <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
          </tr>
          <tr>
            <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
            <td bgcolor="#F5FFE5">
              <{if $is_update}>
                <form name="mainform" action="hiyari_el_folder_update_exe.php" method="post">
              <{else}>
                <form name="mainform" action="hiyari_el_folder_register_exe.php" method="post">
              <{/if}>
                <!-- 表全体 START -->
                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                  <!-- 作成先 START -->
                  <tr height="22">
                    <td width="16%" align="right" bgcolor="#DFFFDC">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                        <{if $is_update}>
                          親フォルダ
                        <{else}>
                          作成先
                        <{/if}>                      
                      </font>
                    </td>
                    <td colspan="3" width="84%" bgcolor="#FFFFFF">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                        <!-- 書庫移行のパス出力 -->
                        <{if isset($cate_nm)}>
                            <{$cate_nm}>
                        <{else}>
                          （最上位フォルダ）
                        <{/if}>
                        <{foreach name=n from=$folder_path item=fp}>
                          &gt;&nbsp;<{$fp.name}>
                        <{/foreach}>
                      </font>
                    </td>
                  </tr>
                  <!-- 作成先 END -->

                  <!-- フォルダ名 START -->
                  <tr height="22">
                    <td width="16%" align="right" bgcolor="#DFFFDC">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ名</font>
                    </td>
                    <td colspan="3" bgcolor="#FFFFFF">
                      <input type="text" name="folder_name" size="60" maxlength="60" value="<{$folder_name}>" style="ime-mode:active;">
                    </td>
                  </tr>
                  <!-- フォルダ名 END -->

                  <!-- 参照・更新可能職員指定 -->
                  <{include file="hiyari_el_emp1.tpl"}>
                </table>
                <!-- 表全体 END -->
                
                <!-- 下部ボタン START -->
                <table width="100%" border="0" cellspacing="0" cellpadding="1">
                  <tr height="22">
                    <{if $is_update}>
                      <td align="right">
                        <input type="button" value="更新" <{if $edit_button_disabled}>disabled"<{/if}> onclick="submitForm();">
                      </td>
                    <{else}>
                      <td colspan="3" align="right">
                        <input type="button" value="作成" onclick="submitForm();">
                      </td>
                    <{/if}>
                  </tr>
                </table>
                <!-- 下部ボタン END -->

                <input type="hidden" name="session" value="<{$session}>">
                <input type="hidden" name="archive" value="<{$archive}>">
                <input type="hidden" name="category" value="<{$cate_id}>">
                <input type="hidden" name="cate_id" value="<{$cate_id}>">
                <input type="hidden" name="parent" value="<{$parent}>">
                <input type="hidden" name="folder_id" value="<{$folder_id}>">
                <input type="hidden" name="path" value="<{$path}>">
                <input type="hidden" name="ref_toggle_mode" value="">
                <input type="hidden" name="upd_toggle_mode" value="">
                <input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
                <input type="hidden" id="target_name_list1" name="target_name_list1" value="">
                <input type="hidden" id="target_id_list2"   name="target_id_list2" value="">
                <input type="hidden" id="target_name_list2" name="target_name_list2" value="">
              </form>
            </td>
            <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
          </tr>
          <tr>
            <td><img src="img/r_3.gif" width="10" height="10"></td>
            <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td><img src="img/r_4.gif" width="10" height="10"></td>
          </tr>
        </table>
        <!-- 本体 END -->
      </td>
    </tr>
  </table>
</body>
</html>