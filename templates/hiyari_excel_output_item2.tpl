<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | EXCEL出力</title>
	<meta http-equiv="imagetoolbar" content="no" />
	<link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript">
      var w = 940;
      var h = window.screen.availHeight;
      window.resizeTo(w, h);
    
      jQuery(function($){
          //チェックボックス
          $('#sub input').change(function() {
              $(this).closest('ul').find('li').removeClass('checked');
              if ($(this).prop('checked')){
                  $(this).closest('li').addClass('checked');
              }
              else{
                  $(this).closest('li').removeClass('checked');
              }
          });	

          $('#main input').change(function() {
              $(this).closest('td').toggleClass('checked');
              
              var child_code = $(this).prop('class').replace('child_','');
              if (child_code.length > 0){
                  var child = $('#grp_' + child_code);
                  var child_span = child.closest('span');
                  if ($(this).prop('checked')){
                      child_span.removeClass('disabled');
                      child.prop('disabled', false);                    
                  }
                  else{
                      child_span.closest('td').removeClass('checked');
                      child.prop('checked', false);
                      
                      child_span.addClass('disabled');
                      child.prop('disabled', true);                      
                  } 
              }
          });	        
      });

      function setCheckboxOnOff(cate_code,is_on) {
          var ckbox = $('#cate_' + cate_code).find('input');
          if (is_on){
              ckbox.prop('checked', true);
              ckbox.closest('td').addClass('checked');
          }
          else{
              ckbox.prop('checked', false);
              ckbox.closest('td').removeClass('checked');
          }
          
          var child = $('#cate_' + cate_code).find('.child');
          if (child.length > 0){
              var child_span = child.closest('span');
              if (is_on){
                  child_span.removeClass('disabled');
                  child.prop('disabled', false);
              }
              else{
                  child_span.addClass('disabled');
                  child.prop('disabled', true);
              }
          }
      }

      function set_output_item() {
          var output_cnt = get_output_count();
          if(output_cnt > 256){
              alert("出力項目が多すぎます。項目を絞り込んでください。\n\nEXCEL列数:" + output_cnt + "/256 (" + (output_cnt-256) + "列超過)" );
              return;
          }
          
        var person_number = document.forms['mainform'].elements["person_num"].options;
        var output = [];
        for(var i = 0; i < person_number.length; i++) {
            if(person_number[i].selected) {
                document.mainform.person_number.value = person_number[i].value;
                
            }
        }
    
        var elm = document.forms['mainform'].elements["profile_num"].options;
        var output = [];
        for(var i = 0; i < elm.length; i++) {
            if(elm[i].selected) {
                document.mainform.profile_number.value = elm[i].value;
                
            }
        }
        
        var detail_num = document.forms['mainform'].elements["detail_num"].options;
        var output = [];
        for(var i = 0; i < detail_num.length; i++) {
            if(detail_num[i].selected) {
                document.mainform.detail_number.value = detail_num[i].value;
            }
        }
        
          
          document.mainform.action="hiyari_rp_report_list_excel.php";
          document.mainform.submit();
      }

      function set_output_item_csv() {
          
        var person_number = document.forms['mainform'].elements["person_num"].options;
        var output = [];
        for(var i = 0; i < person_number.length; i++) {
            if(person_number[i].selected) {
                document.mainform.person_number.value = person_number[i].value;
                
            }
        }
    
        var elm = document.forms['mainform'].elements["profile_num"].options;
        var output = [];
        for(var i = 0; i < elm.length; i++) {
            if(elm[i].selected) {
                document.mainform.profile_number.value = elm[i].value;
                
            }
        }
        
        var detail_num = document.forms['mainform'].elements["detail_num"].options;
        var output = [];
        for(var i = 0; i < detail_num.length; i++) {
            if(detail_num[i].selected) {
                document.mainform.detail_number.value = detail_num[i].value;
            }
        }

          
          
          document.mainform.action="hiyari_rp_report_list_csv.php";
          document.mainform.submit();
      }

      function set_output_item_pdf() {
          var h = window.screen.availHeight;
          var w = window.screen.availWidth;
          var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
          window.open("", "pdf_window", option);
          document.mainform.target = "pdf_window";
          document.mainform.action="hiyari_rp_report_list_pdf.php";
          
                  var person_number = document.forms['mainform'].elements["person_num"].options;
        var output = [];
        for(var i = 0; i < person_number.length; i++) {
            if(person_number[i].selected) {
                document.mainform.person_number.value = person_number[i].value;
                
            }
        }
    
        var elm = document.forms['mainform'].elements["profile_num"].options;
        var output = [];
        for(var i = 0; i < elm.length; i++) {
            if(elm[i].selected) {
                document.mainform.profile_number.value = elm[i].value;
                
            }
        }
        
        var detail_num = document.forms['mainform'].elements["detail_num"].options;
        var output = [];
        for(var i = 0; i < detail_num.length; i++) {
            if(detail_num[i].selected) {
                document.mainform.detail_number.value = detail_num[i].value;
            }
        }        var person_number = document.forms['mainform'].elements["person_num"].options;
        var output = [];
        for(var i = 0; i < person_number.length; i++) {
            if(person_number[i].selected) {
                document.mainform.person_number.value = person_number[i].value;
                
            }
        }
    
        var elm = document.forms['mainform'].elements["profile_num"].options;
        var output = [];
        for(var i = 0; i < elm.length; i++) {
            if(elm[i].selected) {
                document.mainform.profile_number.value = elm[i].value;
                
            }
        }
        
        var detail_num = document.forms['mainform'].elements["detail_num"].options;
        var output = [];
        for(var i = 0; i < detail_num.length; i++) {
            if(detail_num[i].selected) {
                document.mainform.detail_number.value = detail_num[i].value;
            }
        }
          
          document.mainform.submit();
      }

      function get_output_count() {
          //未指定の時の出力件数
          var cnt = 4;

          var input_object_list = document.getElementsByTagName("input");
          for (var i = 0; i < input_object_list.length; i++) {
              var obj = input_object_list[i];
              if (obj.type == "checkbox" && obj.checked) {
                  switch (obj.value) {
                      <{foreach from=$grp_excel_output_cnt key=grp_code item=cnt}>
                      case "<{$grp_code}>":cnt = cnt + <{$cnt}>;break;
                      <{/foreach}>
                  }
              }
          }
          return cnt;
      }

      function set_max_report_dl_cnt() {
          document.mainform.action="hiyari_excel_output_item.php";
          document.mainform.mode.value = "set_max_report_dl_cnt";
          document.mainform.submit();
      }
  </script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
</head>
 
<body id="excel_output_item">
  <form name="mainform" action="" method="post">
    <input type="hidden" name="session" value="<{$session}>">
    <input type="hidden" name="report_id_list" value="<{$report_id_list}>">
    <input type="hidden" name="emp_id" value="<{$emp_id}>">
    <input type="hidden" name="mode" value="">
    
    <input type="hidden" name="person_number" value="">
    <input type="hidden" name="detail_number" value=""> 
    <input type="hidden" name="profile_number" value="">
    <!-------------------------------------------------------------------------
    ヘッダー 
    -------------------------------------------------------------------------->
    <div id="header">
      <div class="inner">
        <h1><{$title}></h1>
        <p class="action_btn">
          <input type="button" class="<{if $max_report_dl_cnt==10}>button<{else}>button_disabled<{/if}> radius_mini" value="PDF出力" onclick="set_output_item_pdf()" <{if $max_report_dl_cnt!=10}>disabled<{/if}>>
          <input type="button" class="button radius_mini" value="CSV出力" onclick="set_output_item_csv()">
          <input type="button" class="button radius_mini" value="EXCEL出力" onclick="set_output_item()">
        </p>
        <p class="notice">
          環境によっては件数、項目数が多い場合にダウンロードが正しく動作しない場合があります。その場合は件数、項目数を減らして再度実行してください。
        </p>
      </div>
    </div>
    
    <!-------------------------------------------------------------------------
    コンテンツ
    -------------------------------------------------------------------------->
    <div id="content" class="clearfix">
      <!-----------------------------------------------------------------
      件数設定
      ------------------------------------------------------------------>
      <div id="sub" class="clearfix">
        <div class="color01 radius">
          <h2>件数設定</h2>
          <select name="max_report_dl_cnt" onchange="set_max_report_dl_cnt()">
            <option value="10"  <{if $max_report_dl_cnt == 10 }>selected<{/if}>>10件単位</option>
            <option value="100" <{if $max_report_dl_cnt == 100}>selected<{/if}>>100件単位</option>
            <option value="200" <{if $max_report_dl_cnt == 200}>selected<{/if}>>200件単位</option>
            <option value="300" <{if $max_report_dl_cnt == 300}>selected<{/if}>>300件単位</option>
            <option value="0"   <{if $max_report_dl_cnt == 0  }>selected<{/if}>>全件</option>
          </select>
          <ul class="radius_mini">
            <{foreach name=range from=$arr_start_end key=k item=i}>
              <li class="<{if $smarty.foreach.range.index == 0}> checked<{/if}>">
                <input id="cnt_<{$k}>" type="radio" name="report_idx"; value="<{$smarty.foreach.range.index}>" <{if $smarty.foreach.range.index == 0}> checked<{/if}>>
                <label for="cnt_<{$k}>"><{$i.start}>件から<{$i.end}>件まで出力</label>
              </li>
            <{/foreach}>
          </ul>
        </div>
      </div>  
      
      <!-----------------------------------------------------------------
      一覧
      ------------------------------------------------------------------>
      <div id="main" class="section">      
        <{foreach from=$cate_list item=cate}>
          <div class="color02 radius mB10">
            <table id="cate_<{$cate.cate_code}>" class="list">
              <tr>
                <th>
                  <{$cate.cate_name}>
                  <div class="grp_button">
                    <input type="button" class="button radius_mini" value="全てON"  onclick="setCheckboxOnOff('<{$cate.cate_code}>',true)">
                    <input type="button" class="button radius_mini" value="全てOFF" onclick="setCheckboxOnOff('<{$cate.cate_code}>',false)">
                  </div>
                </th>
              </tr>
              <{foreach from=$cate.grp_list item=grp}>

                <{* 発生日時 *}>
                <{if $grp.grp_code == 100}>
                  <tr>
                    <td>
                      &nbsp;発生日時
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("100_5", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_100_5" 
                               name="use_grp_item_code[]" 
                               value="100_5" 
                               <{if in_array("100_5", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_100_5">発生年月日</label>
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("100_40", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_100_40" 
                               name="use_grp_item_code[]" 
                               value="100_40"
                               <{if in_array("100_40", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_100_40">発生時間帯</label>
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("100_50", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_100_50" 
                               name="use_grp_item_code[]" 
                               value="100_50"
                               <{if in_array("100_50", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_100_50">いつごろ</label>
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("100_65", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_100_65" 
                               name="use_grp_item_code[]" 
                               value="100_65"
                               <{if in_array("100_65", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_100_65">発生時刻：時　と　分</label>
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("100_60", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_100_60" 
                               name="use_grp_item_code[]" 
                               value="100_60"
                               <{if in_array("100_60", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_100_60">発生時刻：時　のみ</label>
                    </td>
                  </tr>

                <{* 発生場所 *}>
                <{elseif $grp.grp_code == 110}>
                  <tr>
                    <td>
                      &nbsp;発生場所
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("110_60", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_110_60" 
                               name="use_grp_item_code[]" 
                               value="110_60"
                               <{if in_array("110_60", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_110_60">発生場所</label>
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("110_65", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_110_65" 
                               name="use_grp_item_code[]" 
                               value="110_65"
                               <{if in_array("110_65", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_110_65">発生場所詳細</label>
                    </td>
                  </tr>

                <{* 患者プロフィール *}>
                <{elseif $grp.grp_code == 210}>
                  <tr>
                    <td>
                      &nbsp;患者プロフィール
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("210_10", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_210_10" 
                               name="use_grp_item_code[]" 
                               value="210_10"
                               <{if in_array("210_10", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_210_10">患者ID</label>
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("210_20", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_210_20" 
                               name="use_grp_item_code[]" 
                               value="210_20"
                               <{if in_array("210_20", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_210_20">患者氏名</label>
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("210_30", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_210_30" 
                               name="use_grp_item_code[]" 
                               value="210_30"
                               <{if in_array("210_30", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_210_30">患者性別</label>
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("210_40", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_210_40" 
                               name="use_grp_item_code[]" 
                               value="210_40"
                               <{if in_array("210_40", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_210_40">患者年齢（年数）</label>
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("210_50", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_210_50" 
                               name="use_grp_item_code[]" 
                               value="210_50"
                               <{if in_array("210_50", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_210_50">患者年齢（月数）</label>
                    </td>
                  </tr>
                                
                <{* アセスメント・患者の状態は出力対象外。 20090304*}>
                <{elseif $grp.grp_code == 140}>
                  <tr>
                    <td>
                      &nbsp;アセスメント・患者の状態
                    </td>
                  </tr>

                <{* 改善策 *}>
                <{elseif $grp.grp_code == 620}>
                  <tr>
                    <td>
                      &nbsp;改善策
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("620_30", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_620_30" 
                               name="use_grp_item_code[]" 
                               value="620_30"
                               <{if in_array("620_30", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_620_30">改善策</label>
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("620_33", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_620_33" 
                               name="use_grp_item_code[]" 
                               value="620_33"
                               <{if in_array("620_33", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_620_33">評価予定期日</label>
                    </td>
                  </tr>

                <{* 概要・場面・内容（2010年改訂） *}>
                <{elseif $grp.grp_code == 900}>
                  <tr>
                    <td>
                      &nbsp;概要・場面・内容（2010年改訂）
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("900_10", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_900_10" 
                               name="use_grp_item_code[]" 
                               value="900_10"
                               <{if in_array("900_10", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_900_10">概要</label>
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("910_10", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_910_10" 
                               name="use_grp_item_code[]" 
                               value="910_10"
                               <{if in_array("910_10", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_910_10">種類</label>
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("920_10", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_920_10" 
                               name="use_grp_item_code[]" 
                               value="920_10"
                               <{if in_array("920_10", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_920_10">種類の項目</label>
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("940_10", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_940_10" 
                               name="use_grp_item_code[]" 
                               value="940_10"
                               <{if in_array("940_10", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_940_10">発生場面</label>
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("950_10", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_950_10" 
                               name="use_grp_item_code[]" 
                               value="950_10"
                               <{if in_array("950_10", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_950_10">発生場面の項目</label>
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("970_10", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_970_10" 
                               name="use_grp_item_code[]" 
                               value="970_10"
                               <{if in_array("970_10", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_970_10">事例の内容</label>
                    </td>
                  </tr>
                  <tr>
                    <td class="<{if in_array("980_10", $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap mL20">
                        <input type="checkbox" 
                               id="grp_980_10" 
                               name="use_grp_item_code[]" 
                               value="980_10"
                               <{if in_array("980_10", $flags)}>checked<{/if}>
                        >
                      </span>
                      <label for="grp_980_10">事例の内容の項目</label>
                    </td>
                  </tr>
                <{elseif $grp.grp_code > 900 && $grp.grp_code <= 990}>

                <{* 通常 *}>
                <{else}>
                  <{* 関連する患者情報 *}>
                  <{if $grp.grp_code == 310}>
                    <tr>
                      <td>
                        &nbsp;関連する患者情報&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 出力数
                     <SELECT name="profile_num">
                    <{section name="profile_num" start=1 loop=11}>
                    <OPTION value=<{$smarty.section.profile_num.index|escape}>><{$smarty.section.profile_num.index|escape}></OPTION>
                   <{/section}>
                  </SELECT>
                      </td>
                    </tr>
                    <{elseif $grp.grp_code == 3050}>
                  <tr>
                    <td>
                      &nbsp当事者情報出力                 
                    <SELECT name="person_num">
                    <{section name="person_num" start=1 loop=11}>
                    <OPTION value=<{$smarty.section.person_num.index|escape}>><{$smarty.section.person_num.index|escape}></OPTION>
                   <{/section}>
                  </SELECT>
                    </td>
                  </tr>
                  <{/if}>
                              
                  <tr>
                    <td class="<{if in_array($grp.grp_code, $flags)}>checked<{/if}>">
                      <span class="checkbox_wrap<{if ($grp.grp_code >= 15000 and $grp.grp_code < 15100) || $grp.grp_code >= 310 and $grp.grp_code < 390}> mL20<{/if}>">
                        <input type="checkbox"
                               id="grp_<{$grp.grp_code}>"
                               <{if isset($grp.rel_grp)}>class="child_<{$grp.rel_grp.grp_code}>"<{/if}>
                               name="use_grp_item_code[]"
                               value="<{$grp.grp_code}>"
                               <{if in_array($grp.grp_code, $flags)}>checked<{/if}>
                        >
                      </span>
                      <!--事例の詳細(時系列)  --> 
                      <{if $grp.grp_code == 4000}>
                      <label for="grp_<{$grp.grp_code}>"><{$grp.grp_name}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 出力数
                          <SELECT name="detail_num">
                    <{section name="detail_num" start=1 loop=51}>
                        <OPTION value=<{$smarty.section.detail_num.index|escape}>><{$smarty.section.detail_num.index|escape}></OPTION>
                   <{/section}>
                    </SELECT></label>
                      <{else}>
                      <label for="grp_<{$grp.grp_code}>"><{$grp.grp_name}></label>
                      <{/if}>
                      
                      
                    </td>
                  </tr>
                              
                  <{if isset($grp.rel_grp)}>
                    <tr>
                      <td class="<{if in_array($grp.grp_code, $flags)}><{if in_array($grp.rel_grp.grp_code, $flags)}>checked<{/if}><{/if}>">
                        <span class="checkbox_wrap mL20 <{if !in_array($grp.grp_code, $flags)}>disabled<{/if}>">
                          <input type="checkbox"
                                 id="grp_<{$grp.rel_grp.grp_code}>"
                                 class="child parent_<{$grp.grp_code}>"
                                 name="use_grp_item_code[]"
                                 value="<{$grp.rel_grp.grp_code}>"
                                 <{if in_array($grp.grp_code, $flags)}>
                                  <{if in_array($grp.rel_grp.grp_code, $flags)}>checked<{/if}>
                                 <{else}>
                                  disabled
                                 <{/if}>                                 
                          >
                        </span>
                        <label for="grp_<{$grp.rel_grp.grp_code}>"><{$grp.grp_name}></label><br>
                      </td>
                    </tr>
                  <{/if}>
                <{/if}>
              <{/foreach}>
            </table>
          </div>      
        <{/foreach}>
      </div>
      <p class="pagetop"><a href="#excel_output_item">ページの先頭へ</a></p>
    </div>
    
    <!-----------------------------------------------------------------
    フッター
    ------------------------------------------------------------------>
    <div id="footer">
      <div class="inner">
        <p class="action_btn">
            <input type="button" class="<{if $max_report_dl_cnt==10}>button<{else}>button_disabled<{/if}> radius_mini" value="PDF出力" onclick="set_output_item_pdf()" <{if $max_report_dl_cnt!=10}>disabled<{/if}> >
            <input type="button" class="button radius_mini" value="CSV出力" onclick="set_output_item_csv()">
            <input type="button" class="button radius_mini" value="EXCEL出力" onclick="set_output_item()">
        </p>
        <p class="notice">
          環境によっては件数、項目数が多い場合にダウンロードが正しく動作しない場合があります。その場合は件数、項目数を減らして再度実行してください。
        </p>
      </div>
    </div>
  </form>
</body>
</html>