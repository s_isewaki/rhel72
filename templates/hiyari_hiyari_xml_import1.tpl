<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix <{$INCIDENT_TITLE}> | XMLインポート</title>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript" src="js/incident_popup.js"></script>
  <{include file="hiyari_post_select_js.tpl"}>
  <script type="text/javascript">
    function load_action()
    {
      set_post_select_box_obj_key("default", "search_emp_class", "search_emp_attribute", "search_emp_dept", "search_emp_room");
      setClassOptions("default", "<{$registrant_class}>", "<{$registrant_attribute}>", "<{$registrant_dept}>", "<{$registrant_room}>");
      set_eis_list();
    }
    function import_xml() {
      document.import_form.page.value = 1;
      document.import_form.mode.value = "import";
      document.import_form.submit();
    }
    
    function page_change(page) {
      document.import_form.page.value = page;
      document.import_form.mode.value = "paging";
      document.import_form.submit();
    }
    
    function show_sub_window(url) {
      var h = window.screen.availHeight - 30;
      var w = window.screen.availWidth - 10;
      var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
      
      window.open(url, '_blank', option);
    }

    //職種と様式    
    function set_eis_list()
    {
      var job_box = document.getElementById("sel_job_id");
      var eis_box = document.getElementById("sel_eis_id");
      var job_id = job_box.options[job_box.selectedIndex].value;
      
      deleteAllOptions(eis_box);
      if (false) {}
      <{foreach from=$registrant_jobs item=job_data}>
        else if (job_id == "<{$job_data.job_id}>") {
          <{foreach from=$eis_sel_arr item=eis_data}>
            <{if $job_data.job_id == $eis_data.job_id && $eis_data.default_flg == "t"}>
              var default_eis_no = "<{$eis_data.eis_no}>";
            <{/if}>
          <{/foreach}>
          <{foreach from=$eis_sel_arr item=eis_data}>
            <{if $job_data.job_id == $eis_data.job_id}>
              addOption(eis_box, "<{$eis_data.eis_no}>", "<{$eis_data.eis_name}>", default_eis_no);
            <{/if}>
          <{/foreach}>
        }
      <{/foreach}>
    }
    
    function deleteAllOptions(box)
    {
      for (var i = box.length - 1; i >= 0; i--)
      {
        box.options[i] = null;
      }
    }
    
    function addOption(box, value, text, selected)
    {
      var opt = document.createElement("option");
      opt.value = value;
      opt.text = text;
      if (selected == value)
      {
      opt.selected = true;
      }
      box.options[box.length] = opt;
      try {box.style.fontSize = 'auto';} catch (e) {}
      box.style.overflow = 'auto';
    }
    
    // インシレベルポップアップ表示
    function popup_inci_level(content, e) {
        popupDetailGreen(
            new Array(
                "<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><{$title}></font>", content
            ), 1000, 100, e
        );
    }
  </script>
  <style type="text/css">
    .list {border-collapse:collapse;}
    .list td {border:#35B341 solid 1px;}

    .list_2 {border-collapse:collapse;}
    .list_2 td {border:#999999 solid 1px;}

    .list_in {border-collapse:collapse;}
    .list_in td {border:#FFFFFF solid 0px;}
  </style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="load_action()">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <{*ヘッダー*}>
        <{include file="hiyari_header1.tpl"}>
        <img src="img/spacer.gif" width="1" height="5" alt=""><br>
          
        <form name="import_form" method="post" action="hiyari_hiyari_xml_import.php?session=<{$session}>" enctype="multipart/form-data">
          <input type="hidden" name="mode" value="">
          <input type="hidden" name="page" value="<{$page}>">
          <input type="hidden" name="index" value="">
          <input type="hidden" name="target_xml_index" value="">
          <input type="hidden" name="target_db_index" value="">
          <input type="hidden" name="design_mode" value="<{$design_mode}>">
          
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="top">                
                <!-- 画面上部（検索） START -->
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td valign="top">
                      <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
                          <td background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                          <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
                        </tr>
                        <tr>
                          <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                          <td bgcolor="#F5FFE5">
                            <table border="0" cellspaing="0" cellpadding="0" style="border-collapse: collapse; border: #ffffff">
                              <tr>
                                <td>
                                  <table border="0" cellspacing="0" cellpadding="3" class="list">
                                    <tr height="25">
                                      <td align="center" rowspan="<{$row_num}>" bgcolor="#DFFFDC" width="140">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告部署</font>
                                      </td>
                                      <td width="60" align="center" bgcolor="#DFFFDC">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$arr_class_name[0]|escape}></font>
                                      </td>
                                      <td bgcolor="#ffffff" style="padding: 0px; padding-left: 2px; padding-right: 2px">
                                        <select id="search_emp_class" name="search_emp_class" onchange="setAtrbOptions('default');"></select>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td width="60" align="center" bgcolor="#DFFFDC">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$arr_class_name[1]|escape}></font>
                                      </td>
                                      <td bgcolor="#ffffff" style="padding: 0px; padding-left: 2px; padding-right: 2px">
                                        <select id="search_emp_attribute" name="search_emp_attribute" onchange="setDeptOptions('default');"></select>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td width="60" align="center" bgcolor="#DFFFDC">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$arr_class_name[2]|escape}></font>
                                      </td>
                                      <td bgcolor="#ffffff" style="padding: 0px; padding-left: 2px; padding-right: 2px">
                                        <select id="search_emp_dept" name="search_emp_dept" onchange="setRoomOptions('default');"></select>
                                      </td>
                                    </tr>
                                    <{if $arr_class_name.class_cnt == 4}>
                                      <tr>
                                        <td width="60" align="center" bgcolor="#DFFFDC">
                                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$arr_class_name[3]|escape}></font>
                                        </td>
                                        <td bgcolor="#ffffff" style="padding: 0px; padding-left: 2px; padding-right: 2px">
                                          <select id="search_emp_room" name="search_emp_room"></select>
                                        </td>
                                      </tr>
                                    <{/if}>
                                  </table>
                                  
                                  <img src="img/spacer.gif" width="1" height="5" alt=""><br>
                                  
                                  <table border="0" cellspacing="0" cellpadding="3" class="list">
                                    <tr height="25">
                                      <td align="center" bgcolor="#DFFFDC" width="140">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象職種・報告書様式</font>
                                      </td>
                                      <td bgcolor="#ffffff" style="padding: 0px; padding-left: 2px; padding-right: 2px">
                                        <select id="sel_job_id" name="sel_job_id" style="height: 20px" onchange="set_eis_list()">
                                          <{foreach from=$registrant_jobs item=job}>
                                            <option value="<{$job.job_id}>"<{$job.selected}>><{$job.job_nm}></option>
                                          <{/foreach}>
                                        </select>
                                        <select id="sel_eis_id" name="sel_eis_id" style="height: 20px">
                                        </select>
                                      </td>
                                    </tr>
                                  </table>
                                  
                                  <img src="img/spacer.gif" width="1" height="5" alt=""><br>
                                  
                                  <table border="0" cellspacing="0" cellpadding="3" class="list">
                                    <tr height="25">
                                      <td align="center" bgcolor="#DFFFDC" width="140">
                                        <span onmousemove="popup_inci_level('<{$popup_detail_message}>', event);" onmouseout="closeDetail();" style="cursor:pointer;">
                                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="blue">
                                            <B><{$title}></B>
                                            <img src="./img/hiyari_report_help_icon.gif" alt="説明" border="0" style="vertical-align: text-bottom;" />
                                          </font>
                                        </span>
                                      </td>
                                      <td bgcolor="#ffffff" style="padding: 0px; padding-left: 2px; padding-right: 2px">
                                        <select name="patient_level" style="height: 20px">
                                          <{foreach from=$level_infos item=level}>
                                            <{if $level.use_flg}>
                                              <option value="<{$level.easy_code}>"><{$level.easy_name}></option>
                                            <{/if}>
                                          <{/foreach}>
                                        </select>
                                      </td>
                                    </tr>
                                  </table>
                                  
                                  <img src="img/spacer.gif" width="1" height="5" alt=""><br>
                                  
                                  <table border="0" cellspacing="0" cellpadding="3" class="list">
                                    <tr height="25">
                                      <td align="center" bgcolor="#DFFFDC" width="140">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">重複チェック</font>
                                      </td>
                                      <td bgcolor="#ffffff">
                                        <table border="0" cellspacing="0" cellpadding="0" class="list_in">
                                          <tr>
                                            <td>
                                              <label for="repetition_rb_1">
                                                <table>
                                                  <tr>
                                                    <td rowspan="3" style="border-top: solid 1px #35B341; border-left: solid 1px #35B341; border-bottom: solid 1px #35B341">
                                                      <input type="radio" name="repetition_rb" id="repetition_rb_1" value="1" checked>
                                                    </td>
                                                    <td class="j12">
                                                      登録部署
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="j12">
                                                      登録者
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="j12">
                                                      シーケンス番号
                                                    </td>
                                                  </tr>
                                                </table>
                                              </label>
                                            </td>
                                            <td style="width: 10px"></td>
                                            <td class="j12">
                                              <label for="repetition_rb_2">
                                                <table>
                                                  <tr>
                                                    <td rowspan="3" style="border-top: solid 1px #35B341; border-left: solid 1px #35B341; border-bottom: solid 1px #35B341">
                                                      <input type="radio" name="repetition_rb" id="repetition_rb_2" value="2">
                                                    </td>
                                                    <td class="j12">
                                                      登録部署、登録者、シーケンス番号
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="j12">
                                                      発生年月、発生曜日、発生時間帯、事故(事例)の概要
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="j12">
                                                      患者の数、患者性別、患者の年齢(年)、患者の年齢(月)
                                                    </td>
                                                  </tr>
                                                </table>
                                              </label>
                                            </td>
                                            <td style="width: 5px"></td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                    <tr height="25">
                                      <td align="center" bgcolor="#DFFFDC" width="140">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">重複データの処理</font>
                                      </td>
                                      <td bgcolor="#ffffff">
                                        <table border="0" cellspacing="0" cellpadding="0" class="list_in">
                                          <tr>
                                            <td class="j12">
                                              <input type="radio" name="replace_rd" id="replace_rd_1" value="1" checked>
                                              <label for="replace_rd_1">インポートしない</label>
                                            </td>
                                            <td style="width: 10px"></td>
                                            <td class="j12">
                                              <input type="radio" name="replace_rd" id="replace_rd_2" value="2">
                                              <label for="replace_rd_2">インポートする(置換)</label>
                                            </td>
                                            <td style="width: 5px"></td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                  </table>
                                  
                                  <img src="img/spacer.gif" width="1" height="5" alt=""><br>
                                  
                                  <table border="0" cellspacing="0" cellpadding="3" class="list">
                                    <tr height="25">
                                      <td align="center" bgcolor="#DFFFDC" width="140">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">XMLファイル</font>
                                      </td>
                                      <td bgcolor="#ffffff" class="j12" style="padding: 0px; padding-left: 2px; padding-right: 2px">
                                        <input type="file" name="xml_file" value="">
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                          <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                        </tr>
                        <tr>
                          <td><img src="img/r_3.gif" width="10" height="10"></td>
                          <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                          <td><img src="img/r_4.gif" width="10" height="10"></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td align="right">
                      <img src="img/spacer.gif" width="1" height="4" alt=""><br>
                      <input type="button" value="登録" style="width: 80px" onclick="import_xml()">
                    </td>
                  </tr>
                </table>
                <!-- 画面上部（検索） END -->
                
                <!-- 画面下部（結果） START -->
                <{* メッセージ *}>
                <{if $under_disp_message != ""}>
                  <img src="img/spacer.gif" width="1" height="5" alt=""><br>                  
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td bgcolor="#35B341"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
                    </tr>
                  </table>                  
                  <img src="img/spacer.gif" width="1" height="5" alt=""><br>

                  <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
                      <td background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                      <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
                    </tr>
                    <tr>
                      <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                      <td bgcolor="#F5FFE5">
                        <table border="0" cellspaing="0" cellpadding="0" style="border-collapse: collapse; border: #ffffff">
                          <tr>
                            <td>
                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b><{$under_disp_message}></b></font>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                    </tr>
                    <tr>
                      <td><img src="img/r_3.gif" width="10" height="10"></td>
                      <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                      <td><img src="img/r_4.gif" width="10" height="10"></td>
                    </tr>
                  </table>
                <{/if}>

                <{* 処理内容 *}>
                <{if ($is_verified_ok && $num_indispensable == 0) || $mode == "paging"}>							
                  <img src="img/spacer.gif" width="1" height="5" alt=""><br>                  
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td bgcolor="#35B341"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
                    </tr>
                  </table>                  
                  <img src="img/spacer.gif" width="1" height="5" alt=""><br>
                  
                  <{* 処理結果（件数） *}>
                  <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
                      <td background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                      <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
                    </tr>
                    <tr>
                      <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                      <td bgcolor="#F5FFE5">
                        <table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border: #ffffff">
                          <tr>
                            <td>
                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b>処理結果</b></font>
                              <table border="0" cellspacing="0" cellpadding="3" class="list">
                                <tr height="25">
                                  <td align="center" bgcolor="#DFFFDC" width="80">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">処理件数</font>
                                  </td>
                                  <td align="center" bgcolor="#DFFFDC" width="80">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">インポート件数</font>
                                  </td>
                                  <td align="center" bgcolor="#DFFFDC" width="80">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">置換件数</font>
                                  </td>
                                  <td align="center" bgcolor="#DFFFDC" width="80">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">重複エラー</font>
                                  </td>
                                </tr>
                                <tr height="25">
                                  <td align="right" bgcolor="#ffffff" class="j12">
                                    <{$TOTAL_CNT}>件
                                  </td>
                                  <td align="right" bgcolor="#ffffff" class="j12">
                                    <{$IMPORT_CNT}>件
                                  </td>
                                  <td align="right" bgcolor="#ffffff" class="j12">
                                    <{$REPLACE_CNT}>件
                                  </td>
                                  <td align="right" bgcolor="#ffffff" class="j12">
                                    <{$ERROR_CNT}>件
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                    </tr>
                    <tr>
                      <td><img src="img/r_3.gif" width="10" height="10"></td>
                      <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                      <td><img src="img/r_4.gif" width="10" height="10"></td>
                    </tr>
                  </table>
                  
                  <{* 重複データ一覧 *}>
                  <{if $num_duplicated > 0}>
                    <img src="img/spacer.gif" width="1" height="5" alt=""><br>                    
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
                        <td background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
                      </tr>
                      <tr>
                        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                        <td bgcolor="#F5FFE5">
                          <table border="0" cellspaing="0" cellpadding="0" style="border-collapse: collapse; border: #ffffff">
                            <tr>
                              <td>
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b>重複データ内容</b></font>
                                <table border="0" cellspacing="0" cellpadding="3" class="list">
                                  <tr height="25">
                                    <td align="center" bgcolor="#DFFFDC" width="60">
                                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">SEQ</font>
                                    </td>
                                    <td align="center" bgcolor="#DFFFDC" width="90">
                                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生年月</font>
                                    </td>
                                    <td align="center" bgcolor="#DFFFDC" width="90">
                                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間帯</font>
                                    </td>
                                    <td align="center" bgcolor="#DFFFDC" width="110">
                                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生場所</font>
                                    </td>
                                    <td align="center" bgcolor="#DFFFDC" width="110">
                                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">概要</font>
                                    </td>
                                  </tr>
                                  <{foreach from=$duplicated item=result}>
                                    <tr height="25">
                                      <td align="center" bgcolor="#FFFFFF" class="j12">
                                        <{$result.seq}>
                                      </td>
                                      <td align="center" bgcolor="#FFFFFF" class="j12">
                                        <{$result.year}>年<{$result.month}>月
                                      </td>
                                      <td align="center" bgcolor="#FFFFFF" class="j12">
                                        <{$result.time}>
                                      </td>
                                      <td align="center" bgcolor="#FFFFFF" class="j12">
                                        <{$result.site}>
                                      </td>
                                      <td align="center" bgcolor="#FFFFFF" class="j12">
                                        <{$result.summary}>
                                      </td>
                                    </tr>
                                  <{/foreach}>
                                </table>
                              </td>
                            </tr>
                          </table>
                          <{if $page_max != 1}>
                            <table width="100%">
                              <tr>
                                <td>
                                  <table>
                                    <tr>
                                      <td>
                                        <{if $page == 1}>
                                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                                          <span style="color:silver">先頭へ</span>
                                          </font>
                                        <{else}>
                                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                                          <a href="javascript:void(0);" onclick="page_change(1);return false;">先頭へ</a>
                                          </font>
                                        <{/if}>
                                      </td>
                                      <td>
                                        <{if $page == 1}>
                                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                                          <span style="color:silver">前へ</span>
                                          </font>
                                        <{else}>
                                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                                          <a href="javascript:void(0);" onclick="page_change(<{$page - 1}>);return false;">前へ</a>
                                          </font>
                                        <{/if}>
                                      </td>
                                      <td>
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                                          <{section name=i loop=$page_max}>
                                            <{if $smarty.section.i.iteration == $page}>
                                              [<{$smarty.section.i.iteration}>]
                                            <{else}>
                                              <a href="javascript:void(0);" onclick="page_change(<{$smarty.section.i.iteration}>);return false;">[<{$smarty.section.i.iteration}>]</a>
                                            <{/if}>
                                          <{/section}>
                                        </font>
                                      </td>
                                      <td>
                                        <{if $page == $page_max}>
                                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                                          <span style="color:silver">次へ</span>
                                          </font>
                                        <{else}>
                                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                                          <a href="javascript:void(0);" onclick="page_change(<{$page + 1}>);return false;">次へ</a>
                                          </font>
                                        <{/if}>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          <{/if}>
                        </td>
                        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                      </tr>
                      <tr>
                        <td><img src="img/r_3.gif" width="10" height="10"></td>
                        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                        <td><img src="img/r_4.gif" width="10" height="10"></td>
                      </tr>
                    </table>
                  <{/if}>
                  
                  <{* 未処理データ一覧 *}>
                  <{if $num_repetition > 0}>
                    <img src="img/spacer.gif" width="1" height="5" alt=""><br>
                    
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
                        <td background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
                      </tr>
                      <tr>
                        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                        <td bgcolor="#F5FFE5">
                          <table border="0" cellspaing="0" cellpadding="0" style="border-collapse: collapse; border: #ffffff">
                            <tr>
                              <td>
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b>未処理データ内容</b></font>
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">（重複データが複数存在し、置換対象を選択できません）</font>
                                <table border="0" cellspacing="0" cellpadding="3" class="list">
                                  <tr height="25">
                                    <td align="center" bgcolor="#DFFFDC" width="60">
                                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">SEQ</font>
                                    </td>
                                    <td align="center" bgcolor="#DFFFDC" width="90">
                                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生年月</font>
                                    </td>
                                    <td align="center" bgcolor="#DFFFDC" width="90">
                                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間帯</font>
                                    </td>
                                    <td align="center" bgcolor="#DFFFDC" width="110">
                                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生場所</font>
                                    </td>
                                    <td align="center" bgcolor="#DFFFDC" width="110">
                                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">概要</font>
                                    </td>
                                    <td align="center" bgcolor="#DFFFDC"></td>
                                  </tr>
                                  <{foreach from=$repetition item=report key=k}>
                                    <tr height="25">
                                      <td align="center" bgcolor="#FFFFFF" class="j12">
                                        <{$report.seq}>
                                      </td>
                                      <td align="center" bgcolor="#FFFFFF" class="j12">
                                        <{$report.year}>年<{$report.month}>月
                                      </td>
                                      <td align="center" bgcolor="#FFFFFF" class="j12">
                                        <{$report.time}>
                                      </td>
                                      <td align="center" bgcolor="#FFFFFF" class="j12">
                                        <{$report.site}>
                                      </td>
                                      <td align="center" bgcolor="#FFFFFF" class="j12">
                                        <{$report.summary}>
                                      </td>
                                      <td align="center" bgcolor="#FFFFFF" class="j12" style="padding: 0px; padding-left: 2px; padding-right: 2px">
                                        <input type="button" value="置換対象選択" <{$report.button_str}> onclick="show_sub_window('hiyari_hiyari_xml_import_sub.php?session=<{$session}>&index=<{$k}>')">
                                      </td>
                                    </tr>
                                  <{/foreach}>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                      </tr>
                      <tr>
                        <td><img src="img/r_3.gif" width="10" height="10"></td>
                        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                        <td><img src="img/r_4.gif" width="10" height="10"></td>
                      </tr>
                    </table>
                  <{/if}>
                <{/if}>                
                <!-- 画面下部（結果） END -->
              </td>
            </tr>
          </table>
        </form>
      </td>
    </tr>
  </table>
</body>
</html>
