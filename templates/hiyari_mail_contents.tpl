<{*************************************************************************************************
ファイル名：hiyari_mail_contents.tpl
概要：送信されたメッセージと報告書内容を表示する
画面：メッセージ表示・返信・転送画面
*************************************************************************************************}>
<{if $emr_flg=='t'}>
<script language="JavaScript">
// 電子カルテ起動
function start_emr(ptid){
    var emr=document.getElementById('emr');
    var c=(emr.contentWindow || emr.contentDocument);
    c.location.replace("hiyari_exec_emr.php?session=<{$session}>&ptid="+ptid);
}
</script>
<iframe id="emr" src="about:blank" width="0" height="0" style="visibility:hidden;position:absolute;"></iframe>
<{/if}>
<div class="section tabletype01">
  <!--<{*************************************************************
  旧デザインで送信された場合
  **************************************************************}>-->
  <{if isset($mail_message)}>  
    <div class="color03 radius">
      <{if isset($old_message)}>
        <{$old_message}><br/><br/>
      <{/if}>
      <{if isset($send_mail)}>
        <{foreach from=$send_mail item=i name=n}>
          <div class="sender_msg<{if $smarty.foreach.n.last}> none<{/if}>">
            <p class="sender"><{$i.name}>さんの報告内容：</p>
            <{$i.message}>
          </div>
        <{/foreach}>
      <{/if}>
      <{$mail_message}>
    </div>
  <{else}>
  
  <!--<{*************************************************************
  新デザインで送信された場合
  **************************************************************}>-->
    <{if isset($send_mail)}>
      <div class="color03 radius">
        <{foreach from=$send_mail item=i name=n}>
          <div class="sender_msg<{if $smarty.foreach.n.last}> none<{/if}>">
            <p class="sender"><{$i.name}>さんの報告内容：</p>
            <{$i.message}>
          </div>
        <{/foreach}>
      </div>        
    <{/if}>
    
    <{foreach from=$rpt_data item=cate}>
      <h2 class="type03"><{$cate.name}></h2>
      <div class="color03 radius">
        <table>
          <{foreach from=$cate.list item=i}>
            <tr>
              <{if isset($i.grp_title)}>
                <th colspan="2"><{$i.grp_title}></th>
              <{else}>
                <th width="188">
                    <{$i.title}>
                </th>
                <td>
                  <{if $i.is_update}>
                    <table class="updated">
                      <tr>
                        <th width="50">修正前</th>
                        <td>
                          <{$i.content_old}>
                          <{if $emr_flg=='t' && $i.title=='患者ID' && $is_sm_emp_flg}>
                            <a href="javascript:void(0);" onclick="start_emr('<{$i.content_old}>')">[カルテ参照]</a>
                          <{/if}>
                        </td>
                      </tr>
                      <tr class="after_updated">
                        <th width="50">修正後</th>
                        <td>
                          <{$i.content}>
                          <{if $emr_flg=='t' && $i.title=='患者ID' && $is_sm_emp_flg}>
                            <a href="javascript:void(0);" onclick="start_emr('<{$i.content}>')">[カルテ参照]</a>
                          <{/if}>
                        </td>
                      </tr>
                    </table>
                  <{else}>
                    <{if $i.title=='添付ファイル'}>
                    <{foreach from=$file_list item=tmp_hy_file}>
                        <a href="<{$tmp_hy_file.view}>=<{$session}>&r=<{$report_id}>&u=<{$tmp_hy_file.file_no}>" <{$tmp_hy_file.target}> >
                            <{$tmp_hy_file.file_name}>
                        </a>
                    <{/foreach}>
                    <{else}>
                      <{$i.content}>
                      <{if $emr_flg=='t' && $i.title=='患者ID' && $is_sm_emp_flg}>
                        <a href="javascript:void(0);" onclick="start_emr('<{$i.content}>')">[カルテ参照]</a>
                      <{/if}>
                    <{/if}>
                  <{/if}>
                </td>
              <{/if}>
            </tr>
          <{/foreach}>
        </table>
      </div>
      <p class="pagetop"><a href="#top">ページの先頭へ</a></p>
    <{/foreach}>
  <{/if}>
</div>
