<{*************************************************************************************************
報告書登録（編集）画面の項目
*************************************************************************************************}>
<tr id="id_<{$i}>_<{$j}>_area">

<{*<!--
===================================================================================================
タイトルの列
===================================================================================================
-->*}> 
  <th width="188" id="id_<{$i}>_<{$j}>_title" <{if $last_item[$i][$j] || $is_last}>class="last"<{/if}>>
    <{include file="report_form_title.tpl"}>
  </th>

<{*<!--
===================================================================================================
内容の列
===================================================================================================
-->*}>
<td id="id_<{$i}>_<{$j}>_field" class="<{$tabletype}><{if $last_item[$i][$j] || $is_last}> last<{/if}>">
    <{*<!--発生場所詳細、概要の種類/発生場面/事例の内容-->*}>
    <{if ($i==110 && $j==65) || ($i==910 && $j==10) || ($i==920 && $j==10) || ($i==940 && $j==10) 
      || ($i==950 && $j==10) || ($i==970 && $j==10) || ($i==980 && $j==10)}>
      <{*JSで挿入するため何もしない*}>
      
    <{*<!--発見日時/発生日時-->*}>
    <{elseif ($i==105 || $i==100) && $j==5}>
      <div class="date_set1">
        <div class="date_time">
          <{*日付*}>
          <{include file="report_form_field.tpl"}>
          
          <{*時間*}>
          <{if !$time_setting_flg}>
            <{include file="report_form_field.tpl" j=60}>時
            <{include file="report_form_field.tpl" j=65}>分
            <input type="hidden" id="_<{$i}>_40" name="_<{$i}>_40" value="<{$vals[$i][40][0]}>">
          <{/if}>
        </div>
        
        <a href="javascript:void(0);" class="mT3 clear_btn cb_type02"
          <{if $time_setting_flg}>
            onclick="clear_item('text','<{$i}>','<{$j}>');">
          <{else}>
            onclick="clear_item('text','<{$i}>','<{$j}>');clear_item('select','<{$i}>','60');clear_item('select','<{$i}>','65');">
          <{/if}>
          クリア
        </a>
      </div>
      
      <{*時間（2時間単位の場合）*}>
      <{if $time_setting_flg}>
        <div class="date_set2">
          <{include file="report_form_field.tpl" j=40}>
        </div>
      <{/if}>
      
    <{*<!--発見者職種経験年数/部署配属年数-->*}>
    <{elseif $i==3036 || $i==3038}>
      <{include file="report_form_field.tpl"}>
      <{include file="report_form_field.tpl" j=20}>
    
    <{*<!--当事者職種経験年数-->*}>
    <{elseif $i>=3250 && $i<=3259}>
      <{include file="report_form_field.tpl"}>
      <{include file="report_form_field.tpl" j=80}>

    <{*<!--当事者部署配属年数-->*}>
    <{elseif $i>=3300 && $i<=3309}>
      <{include file="report_form_field.tpl"}>
      <{include file="report_form_field.tpl" j=100}>

    <{*<!--患者の年齢-->*}>
    <{elseif $i==210 && $j==40}>
      <{if in_array(40, $patient_use)}>
        <{include file="report_form_field.tpl"}>
      <{/if}>
      <{if in_array(50, $patient_use)}>
        <{include file="report_form_field.tpl" j=50}>
      <{/if}>      
      
    <{*<!--患者の年齢(関連する患者情報)-->*}>
    <{elseif ($i>=340 && $i<=349) && $j==10}>
      <{include file="report_form_field.tpl"}>
      <{include file="report_form_field.tpl" j=20}>
    
    <{*<!--影響区分：出血量-->*}>
    <{elseif $i==1400 && $j==40}>
      約<{include file="report_form_field.tpl"}>cc
      
    <{*<!--他-->*}>
    <{else}>
      <{include file="report_form_field.tpl"}>
    <{/if}>    
  </td>
</tr>