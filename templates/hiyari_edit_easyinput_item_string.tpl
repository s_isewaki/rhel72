<{*
インターフェース
	$grps
		array   項目情報配列
	$vals
		array   値の配列
	$grp_flags
		array   レポートに使用する項目グループ
	$item_must
		array   必須項目情報

	$tr
		boolean 全体を囲むTRタグが必要な場合はtrue
	$colspan
		int     全体を囲むTDタグの横結合数
	$i
		string  グループコード
	$j
		string  アイテムコード
	$style
		string  "textarea"に対する書式指定。"big"のみ可能。(ちなみに、"textarea"が指定されるケースはないため$styleも使用されない。)
	$default
		boolean 画面に値を反映させる場合にtrue。"select"及び"text"に対してのみ有効。


*}>

<{if $tr}>
    '<tr height="22">\n'+
<{/if}>

'<td bgcolor="#DFFFDC" width="250">'+
'<font size="3" class="j12">'+
'<span <{if in_array($i|cat:'_'|cat:$j, $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>'+
'<{$grps[$i].easy_item_list[$j].easy_item_name|escape:"html"}>'+
'</span>'+
'<{if in_array($i|cat:'_'|cat:$j, $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}>'+
'</font>'+
'</td>\n'+

<{if $colspan > 1}>
    '<td colspan="<{$colspan}>">\n'+
<{else}>
    '<td>\n'+
<{/if}>

<{if $grps[$i].easy_item_list[$j].easy_item_type == "select"}>
	'<select id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>"<{if in_array($grps[$i].easy_item_list[$j].relate_group, $grp_flags)}> onchange="relate_detail(this,\'rel_<{$grps[$i].easy_item_list[$j].relate_group}>\');"<{/if}>>\n'+
	'<option value="">\n'+
	<{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}>
		<{if $item_element_no_disp[$i][$j] == "" || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}>
		'<option value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>"<{if $default && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code, $vals[$i][$j])}> selected<{/if}>><{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}></option>\n'+
		<{/if}>
	<{/section}>
	'</select>\n'+

<{elseif $grps[$i].easy_item_list[$j].easy_item_type == "text"}>
	'<input type="text" name="_<{$i}>_<{$j}>" value="<{if $default}><{$vals[$i][$j][0]|replace:"\\":"\\\\"|replace:"'":"\'"}><{/if}>" size="35" style="ime-mode:active;">\n'+
	<{if ($i == "440" && $j == "43") || ($i == "460" && $j == "43") || ($i == "470" && $j == "43") || ($i == "430" && $j == "42") || ($i == "450" && $j == "42") || ($i == "460" && $j == "52") || ($i == "470" && $j == "52") || ($i == "480" && $j == "42") }>
            '<font size="3" class="j10" color="#FF0000">(西暦年４桁と月数字２桁を入力してください。例&nbsp;200805&nbsp;&nbsp;月が不明の場合&nbsp;例&nbsp;2008)</font>\n'+
	<{/if}>
<{elseif $grps[$i].easy_item_list[$j].easy_item_type == "textarea"}>
	'<textarea style="width:100%;ime-mode:active;" rows="<{if $style == "big"}>12<{else}>4<{/if}>" name="_<{$i}>_<{$j}>" ></textarea>\n'+

<{elseif $grps[$i].easy_item_list[$j].easy_item_type == "checkbox"}>
	'<table border="1">\n'+
	'<tr>\n'+
	<{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}>
		'<td>\n'+
		'<input type="checkbox" id="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>" name="_<{$i}>_<{$j}>[]" value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>">\n'+
		'<label for="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>"><{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}></label>\n'+
		'</td>\n'+
		<{if $smarty.section.k.iteration is div by 4 and not $smarty.section.k.last}>
			'</tr><tr>\n'+
		<{/if}>
	<{/section}>
	'</tr>\n'+
	'</table>\n'+

<{elseif $grps[$i].easy_item_list[$j].easy_item_type == "radio"}>
	'<table border="1">\n'+
	'<tr>\n'+
	<{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}>
	'<td>\n'+
	'<input type="radio" id="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>" name="_<{$i}>_<{$j}>[]" value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}><{if in_array($grps[$i].easy_item_list[$j].relate_group, $grp_flags)}> onchange="relate_detail(this,\'rel_<{$grps[$i].easy_item_list[$j].relate_group}>\');"<{/if}>">\n'+
	'<label for="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>"><{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}></label>\n'+
	'</td>\n'+
	<{if $smarty.section.k.iteration is div by 4 and not $smarty.section.k.last}>
		'</tr><tr>\n'+
	<{/if}>
	<{/section}>
	'</tr>\n'+
	'</table>\n'+
<{/if}>
'</td>\n'+
<{if $tr}>
	'</tr>\n'+
<{/if}>
