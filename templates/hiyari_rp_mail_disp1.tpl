<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix <{$INCIDENT_TITLE}> | メッセージ表示</title>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript">
    function openerReload() {
        if(window.opener && !window.opener.closed && window.opener.reload_page) {
            window.opener.reload_page();
        }
    }

    // 内容確認
    function set_progress_end_date()
    {
      <{if $progress_comment_use_flg}>
        var url = "hiyari_rp_progress_comment_input.php?session=<{$session}>&report_id=<{$report_id}>&mail_eis_id=<{$send_eis_id}>";

        var h = window.screen.availHeight - 30;
        var w = window.screen.availWidth - 10;
        var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

        window.open(url, '_blank',option);
      <{/if}>
        location.href = "hiyari_rp_mail_disp.php?session=<{$session}>&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>&mode=update";
    }

    // 返信
    function rp_action_return()
    {
        location.href = "hiyari_rp_mail_input.php?session=<{$session}>&mode=return&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>";
    }
    //転送
    function rp_action_forward()
    {
        location.href = "hiyari_rp_mail_input.php?session=<{$session}>&mode=forward&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>";
    }
    //編集
    function rp_action_update()
    {
        location.href = "hiyari_easyinput.php?session=<{$session}>&gamen_mode=update&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>&report_id=<{$report_id}>";
    }
    //進捗登録
    function rp_action_progress()
    {
        location.href = "hiyari_rp_progress.php?session=<{$session}>&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>&report_id=<{$report_id}>";
    }
    //印刷
    function rp_action_print()
    {
        var sh = window.screen.availHeight - 30;
        var sw = window.screen.availWidth - 10;
        var h = 100;
        var w = 260;
        var t = (sh-h)/2;
        var l = (sw-w)/2

        var url = "hiyari_print_select.php?session=<{$session}>&report_id=<{$report_id}>&mail_id=<{$mail_id}>&eis_id=<{$eis_id}>";
        var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,left=" + l + ",top=" + t + ",width=" + w + ",height=" + h;

        window.open(url, '_blank',option);
    }
    
    // メッセージ内の×ボタンクリックでメッセージを非表示にする
    $(function() {
        <{if $mode==='update'}>
            $('.alert').fadeIn(1000).delay(2000).fadeOut(2000);
        <{/if}>
        $('.alert .close').on('click', function() {
            $(this).parents('.alert').hide();
        });
    });
  </script>
  <link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
table.list_2 {border-collapse:collapse;}
table.list_2 td {border:#CCCCCC solid 1px;padding:1px;}
table.list {border-collapse:collapse;}
table.list td {border:#35B341 solid 1px;}
p.scroll {height:690px; overflow: scroll;border:#CCCCCC solid 1px;}

/* メッセージ用スタイル*/
.alert {
    display: none; /* 初期状態は非表示 */
    width: 400px;
    /* 画面上部に表示 */
    position: absolute;
    top: 5%;
    left: 50%;
    margin-left: -225px; /* (width + padding-left + padding-right + border-left + border-right) の半分 */
    padding: 12px 35px 8px 14px;
    margin-bottom: 18px;
    color: rgb(2, 2, 2);
    text-shadow: 0px 1px 0px rgba(255, 255, 255, 0.5);
    background-color: rgb(249, 237, 190);
    border: 1px solid rgb(240,195,109);
    border-radius: 4px 4px 4px 4px;
}

/* メッセージ削除ボタン用スタイル*/
.close {
    float: right;
    font-size: 20px;
    font-weight: bold;
    line-height: 18px;
    color: rgb(0, 0, 0);
    text-shadow: 0px 1px 0px rgb(255, 255, 255);
    opacity: 0.2;
    position: relative;
    top: -2px;
    right: -21px;
    line-height: 18px;
    padding: 0px;
    cursor: pointer;
    background: none repeat scroll 0% 0% transparent;
    border: 0px none;
}
// -->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="openerReload();">
  <!-- メッセージ -->
  <div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>報告書の進捗を確認済に変更しました。</strong>
  </div>
  <form name="mainform" action="hiyari_mail_disp.php" method="post">
    <input type="hidden" name="mail_id" value="<{$mail_id}>">
    <input type="hidden" name="mail_id_mode" value="<{$mail_id_mode}>">
    <input type="hidden" name="report_id" value="<{$report_id}>">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr height="32" bgcolor="#35B341">
        <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>メッセージ表示</b></font></td>
        <td>&nbsp</td>
        <td width="10">&nbsp</td>
        <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();return false;"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
      </tr>
    </table>
    <img src="img/spacer.gif" width="10" height="10" alt=""><br>

    <!-- 実行アイコン -->
    <table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <{if $report_progress_updatable_flg || $progress_comment_use_flg}>
          <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
          <td align="center"><img id="progress_end" src="img/hiyari_rp_progress_end.gif" style="cursor: pointer;" onclick="set_progress_end_date();"></td>
          <td rowspan="2"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
          <td rowspan="2" align="center" bgcolor="#35B341"><img src="img/spacer.gif" width="1" height="8" alt=""></td>
        <{/if}>

        <{if $mail_id != ""}>
          <{if $mail_id_mode == "recv" && $arr_btn_flg.return_btn_show_flg == "t"}>
            <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
            <td align="center"><img src="img/hiyari_rp_return.gif" width="32" height="29" style="cursor: pointer;" onclick="rp_action_return();"></td>
          <{/if}>

          <{if $arr_btn_flg.forward_btn_show_flg == "t"}>
            <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
            <td align="center"><img src="img/hiyari_rp_forward.gif" width="32" height="29" style="cursor: pointer;" onclick="rp_action_forward();"></td>
          <{/if}>
        <{/if}>

        <{if $report_edit_flg && $report_torey_update_flg === true}>
          <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
          <td align="center"><img src="img/hiyari_rp_update.gif" width="36" height="30" style="cursor: pointer;" onclick="rp_action_update();"></td>
        <{/if}>

        <{if $progres_edit_flg}>
          <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
          <td align="center"><img src="img/hiyari_rp_progress.gif" width="38" height="30" style="cursor: pointer;" onclick="rp_action_progress();"></td>
        <{/if}>

        <{if $arr_btn_flg.print_btn_show_flg == "t"}>
          <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
          <td align="center"><img src="img/hiyari_rp_print.gif" style="cursor: pointer;" onclick="rp_action_print();"></td>
        <{/if}>
      </tr>
      <tr>
        <{if $report_progress_updatable_flg || $progress_comment_use_flg}>
          <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
          <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D"><{$disp_comment}></font></td>
        <{/if}>

        <{if $mail_id != ""}>
          <{if $mail_id_mode == "recv" && $arr_btn_flg.return_btn_show_flg == "t"}>
            <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
            <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">返信</font></td>
          <{/if}>

          <{if $arr_btn_flg.forward_btn_show_flg == "t"}>
            <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
            <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">転送</font></td>
          <{/if}>
        <{/if}>

        <{if $report_edit_flg && $report_torey_update_flg}>
          <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
          <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">編集</font></td>
        <{/if}>

        <{if $progres_edit_flg}>
          <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
          <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">進捗登録</font></td>
        <{/if}>

        <{if $arr_btn_flg.print_btn_show_flg == "t"}>
          <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
          <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">印刷</font></td>
        <{/if}>
      </tr>
    </table>

    <!-- メッセージヘッダー -->
    <table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
      </tr>
      <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td bgcolor="#F5FFE5">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="7%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka">件名&nbsp;：&nbsp;</font></td>
              <td width="*%" align="left">
              <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td <{$marker_style}>>
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka"><{$subject|escape}></font>
                  </td>
                </tr>
              </table>
              </td>
            </tr>
            <tr>
              <td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka">送信者&nbsp;：&nbsp;</font></td>
              <td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka"><{$send_emp_name}></font></td>
            </tr>
            <tr>
              <td align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka">報告部署&nbsp;：&nbsp;</font></td>
              <td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka"><{$send_emp_class_name}></font></td>
            </tr>
            <tr>
              <td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka">日付&nbsp;：&nbsp;</font></td>
              <td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka"><{$send_date}></font></td>
            </tr>
            <tr>
              <td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka">あて先&nbsp;：&nbsp;</font></td>
              <td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka"><{$recv_name}></font></td>
            </tr>
          </table>
        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
      </tr>
      <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
      </tr>
    </table>

    <img src="img/spacer.gif" width="1" height="10" alt=""><br>

    <p class="scroll">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr valign="top" height="690">
          <td bgcolor="#FFFBE7">
            <{$mail_message}>
          </td>
        </tr>
      </table>
    </p>
  </form>
</body>
</html>
