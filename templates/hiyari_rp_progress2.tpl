<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
  <meta http-equiv="X-UA-Compatible" content="IE=8" />
  <title>CoMedix <{$INCIDENT_TITLE}> | 進捗管理</title>
	<meta http-equiv="imagetoolbar" content="no" />
	<link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/smoothness/jquery-ui-1.10.3.custom.min.css">
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
	<script type="text/javascript" src="js/jquery/jquery-ui-1.10.2.custom.min.js"></script>
  <script type="text/javascript" src="js/jquery/jquery.ui.datepicker-ja.min.js" charset="utf-8"></script>
  <script type="text/javascript">
    var w = 1024 + 37;
    var h = window.screen.availHeight;
    window.resizeTo(w, h);
    
    //===============================================
    //実行アイコン
    //===============================================
    // 返信
    function rp_action_return() {
        location.href = "hiyari_rp_mail_input.php?session=<{$session}>&mode=return&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>";
    }
    // 転送
    function rp_action_forward(){
        location.href = "hiyari_rp_mail_input.php?session=<{$session}>&mode=forward&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>";
    }

    // 編集
    function rp_action_update() {
        location.href = "hiyari_easyinput.php?session=<{$session}>&gamen_mode=update&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>&report_id=<{$report_id}>";
    }

    // メッセージ表示 return_list 20140508 R.C
    function rp_action_message_disp(return_list) {
    	if (return_list == "return_list"){
			window.close();
    	}else{	 
        	location.href = "hiyari_rp_mail_disp.php?session=<{$session}>&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>";
    	}
    }

	// 出来事報告編集 20140508 R.C
    function rp_action_edit(return_list) {
    	if (return_list == "return_list"){
    		window.close();
    	}else{
    		location.href = "hiyari_easyinput.php?session=<{$session}>&gamen_mode=update&report_id=<{$report_id}>&callerpage=hiyari_rpt_report_classification.php&frompage=hiyari_rp_progress.php";
    	}
    }
    
    //===============================================
    // 進捗設定
    //===============================================
    function fncSetUseFlg() {
      document.mainform.action="hiyari_rp_progress.php?session=<{$session}>&tab_switch=regist_tab1";
      document.mainform.submit();
    }

    jQuery(function($){
        //匿名
        $('.anonymous').tooltip({
            position: {
                my: "left+20 center",
                at: "right center",
                using: function( position, feedback ) {
                    $( this ).css( position );
                    $( "<div>" )
                    .addClass( "arrow" )
                    .addClass( feedback.vertical )
                    .addClass( feedback.horizontal )
                    .appendTo( this );
                }
            }
        });
        
        //患者影響レベル
        $('.radio').click(function(){
          $(this).closest('ul').find('li').removeClass('checked');
          $(this).closest('li').addClass('checked')
        });	
        
        //工程(担当) 設定
        $('.checkbox').live('click', function(){
          $(this).closest('td').toggleClass('checked')
        });	
    });    
    
    //患者影響レベルのクリア
    function clear_level(){
        var obj = $(".radio");
        obj.attr("checked", false);
        obj.closest('ul').find('li').removeClass('checked');
    }
    
    //===============================================
    // 進捗登録
    //===============================================
    function fncRegist(){
        if (chkDate() == true) {
            document.mainform.action="hiyari_rp_progress.php?session=<{$session}>&tab_switch=regist_tab2";
            document.mainform.submit();
        }
    }

    jQuery(function($){
      //カレンダー
      $('.calendar_text').datepicker({
          showOn: 'focus',
          showOtherMonths: true,
          selectOtherMonths: true
      });
      $('.calendar_btn').live('click', function(){
          $(this).prev('input').datepicker( 'show' );
      });
      
      //チェックボックスの見た目
      $('.checkbox').click(function(e) {
          $('.checkbox_wrap').toggleClass('checked');
      });	    
    });

    //クリア
    function clear_date(id){
        $("#" + id).val('');
    }    
    
    //入力チェック
    function chkDate() {
        <{foreach name=n from=$progress_info item=i}>
            <{if $i.regist_flg == '1'}>
                var from = $('#from<{$smarty.foreach.n.index}>');
                var to = $('#to<{$smarty.foreach.n.index}>');
                var eval = $('#eval<{$smarty.foreach.n.index}>');
                
                if (to.val() != '') {
                    if (from.val() == '') {
                        alert('受付日付を入力してください。');
                        return false;
                    }
                    if (to.val() < from.val()){
                        alert('期間の前後が合っていません。');
                        return false;
                    }
                }

                if (eval.length > 0){
                    if (eval.val() != '') {
                        if (to.val() == '' && eval.val() != '') {
                            alert('確認日付を入力してください。');
                            return false;
                        }
                        if (eval.val() < to.val()){
                            alert('期間の前後が合っていません。');
                            return false;
                        }
                    }
                }
            <{/if}>
        <{/foreach}>
        
        return true;
    }       
  </script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->  
</head>

<body id="top">
  <a id="pagetop" name="pagetop"></a>

  <{* ヘッダー *}>
	<div id="header">
		<div class="inner">
			<h1>進捗管理</h1>
      <{* 実行アイコン *}>
			<ul class="sub_nav clearfix">
        <{if $mail_id != ""}>
          <{if $mail_id_mode == "recv" && $arr_btn_flg.return_btn_show_flg == "t"}>
            <li class="sub_nav_replay"><a href="javascript:void(0);" onclick="rp_action_return();return false;">返信</a></li>
          <{/if}>
          <{if $arr_btn_flg.forward_btn_show_flg == "t"}>
            <li class="sub_nav_forward"><a href="javascript:void(0);" onclick="rp_action_forward();return false;">転送</a></li>
          <{/if}>
        <{/if}>
        
        <{* 編集ボタン 20140508 R.C *}>
        <{* <li class="sub_nav_edit"><a href="javascript:void(0);" onclick="rp_action_update();return false;">編集</a></li> *}>
        
        <{if $mail_id != ""}>
          <li class="sub_nav_return"><a href="javascript:void(0);" onclick="rp_action_message_disp('<{$return_list}>');return false;">表示</a></li>
        <{else}>
          <li class="sub_nav_return"><a href="javascript:void(0);" onclick="rp_action_edit('<{$return_list}>');return false;">表示</a></li>
        <{/if}>
      </ul>
		</div>    
	</div>  
  
  <div id="content">
    <{if $sm_auth_flg == "1"}>
      <div id="sub_menu" class="color01 radius mB10">
        <table class="v_title">
          <tr>
            <th class="none <{if $sm_auth_flg == "1"}>menu2<{else}>menu1<{/if}>">MENU</th>
            <td class="none<{if $tab_switch == "PRGRES_TAB_1"}> active<{/if}>">
              <a href="hiyari_rp_progress.php?session=<{$session}>&amp;tab_switch=PRGRES_TAB_1&amp;report_id=<{$report_id}>&amp;mail_id=<{$mail_id}>&amp;mail_id_mode=<{$mail_id_mode}>&amp;return_list=<{$return_list}>">
                進捗設定
              </a>
            </td>
            <td class="none<{if $tab_switch == "PRGRES_TAB_2"}> active<{/if}>">
              <a href="hiyari_rp_progress.php?session=<{$session}>&amp;tab_switch=PRGRES_TAB_2&amp;report_id=<{$report_id}>&amp;mail_id=<{$mail_id}>&amp;mail_id_mode=<{$mail_id_mode}>&amp;return_list=<{$return_list}>">
                進捗登録
              </a>
            </td>
          </tr>
        </table>
      </div>
    <{/if}>
    
    <div class="section">
      <div class="color01 radius mB10">
        <table>
          <tr>
            <th width="155">
              ● 表 題
            </th>
            <td>
              <{$report_title|escape}>
            </td>
          </tr>
        </table>
      </div>
    </div>      
    
    <form name="mainform" action="hiyari_progress_registration.php" method="post">
      <input type="hidden" name="emp_id" value="<{$emp_id}>">
      <input type="hidden" name="report_id" value="<{$report_id}>">    
      <input type="hidden" name="mail_id" value="<{$mail_id}>">
      <input type="hidden" name="mail_id_mode" value="<{$mail_id_mode}>">    
      <input type="hidden" name="tab_switch" value="<{$tab_switch}>">    
      <input type="hidden" name="sm_auth_flg" value="<{$sm_auth_flg}>">
      <input type="hidden" name="return_list" value="<{$return_list}>">
    
      <{********************************************************************************
      進捗設定画面 
      ********************************************************************************}>
      <{if $tab_switch == "PRGRES_TAB_1"}>
        <{*患者影響レベル*}>
        <div class="section tabletype01">
          <h2 class="type02"><{$level_title}></h2>
          <div class="color02 radius">
            <table>
              <tr>
                <th width="155" <{if !isset($level_rireki_info)}>class="last"<{/if}>>
                  <{$level_title}>
                </th>
                <td colspan="3" <{if !isset($level_rireki_info)}>class="last"<{/if}>>
                  <ul>
                    <{foreach from=$level_list key=k item=i}>
                      <li <{if $i.selected}>class="checked"<{/if}>>
                        <label for="lvl_<{$k}>">
                          <input type="radio" id="lvl_<{$k}>" class="radio" value="<{$i.key}>" name="inci_lvl[]" <{if $i.selected}>checked<{/if}>>
                          <{$i.val}>&nbsp;：<{$i.description}>  
                        </label>
                      </li>                      
                    <{/foreach}>                
                  </ul>
                  <a class="clear_btn cb_type01" href="javascript:void(0);" onclick="clear_level()">クリア</a>
                </td>
              </tr>
            </table>
                
            <{if isset($level_rireki_info)}>
              <table id="level_rireki" class="list">
                <{foreach name=lvl from=$level_rireki_info item=info}>
                  <tr>
                    <th width="155" class="none<{if $smarty.foreach.lvl.last}> last<{/if}>">
                      <{$info.index_name}>
                    </th>
                    <td <{if $smarty.foreach.lvl.last}>class="last"<{/if}>>
                      <{$info.date}>
                    </td>
                    <td <{if $smarty.foreach.lvl.last}>class="last"<{/if}>>
                      <{if isset($info.real_name)}>
                        <span class="anonymous">
                          <a href="#" title="<{$info.real_name}>">匿名</a>
                        </span>
                      <{else}>  
                        <{$info.disp_emp_name}>
                      <{/if}>  
                    </td>
                    <td class="none<{if $smarty.foreach.lvl.last}> last<{/if}>">
                      <{$info.level_name}>
                    </td>
                  </tr>
                <{/foreach}>
              </table>
            <{/if}>
          </div>
        </div>
        
        <{*進捗設定*}>
        <div class="section tabletype01">
          <h2 class="type02">工程(担当) 設定</h2>
          <div class="color02 radius">
            <table id="setting">
              <{foreach name=set from=$setting_info item=i}>
                <tr>
                  <th width="155" <{if $smarty.foreach.set.last}>class="last"<{/if}>>
                    <{$i.auth_name}>
                  </th>
                  <td class="<{if $smarty.foreach.set.last}>last<{/if}><{if $i.use_flg == "t"}> checked<{/if}>">
                    <input type="checkbox" class="checkbox" name="<{$i.auth}>" value="t" <{if $i.use_flg == "t"}> checked<{/if}>/>
                  </td>
                </tr>
              <{/foreach}>
            </table>
          </div>
        </div>
        
        <div id="main_btn">
          <input type="button" class="button radius_mini" value="登録" onclick="fncSetUseFlg();">
        </div>    
      
      <{********************************************************************************
      進捗登録画面 
      ********************************************************************************}>
      <{else}>
        <div class="section tabletype01">
          <div class="color02 radius">
            <table id="progress" class="list">
              <tr class="header">
                <th>工程(担当)</th>
                <th>あなたの<br/>権限</th>
                <th>受付日付</th>
                <th>確認日付</th>
                <th>評価日付</th>
                <th class="none">更新者</th>
              </tr>

              <{foreach name=n from=$progress_info item=i}>
                <tr>
                  <th width="155" class="none<{if $smarty.foreach.n.last}> last<{else}> auth_name<{/if}>"><{$i.auth_name}></th>
                  <td width="55" <{if $smarty.foreach.n.last}>class="last"<{/if}>><{$i.your_auth}></td>
                  <{foreach from=$i.dates key=date_type item=date}>
                    <td width="200" <{if $smarty.foreach.n.last}>class="last"<{/if}>>
                      <{if $date.is_disabled}>
                        <{$date.date}>
                      <{else}>
                        <input type="text" id="<{$date_type}><{$smarty.foreach.n.index}>" class="size04 calendar_text" name="<{$date_type}><{$smarty.foreach.n.index}>" value="<{$date.date}>" readonly />
                        <a class="calendar_btn" href="javascript:void(0);">カレンダー</a>
                        <a class="clear_btn" href="javascript:void(0);" onclick="clear_date('<{$date_type}><{$smarty.foreach.n.index}>');">
                          クリア
                        </a>
                      <{/if}>
                    </td>
                  <{/foreach}>
                  <td class="none<{if $smarty.foreach.n.last}> last<{/if}>">
                    <{$i.updater}>
                  </td>
                </tr>
                <input type="hidden" name="regist_flg<{$smarty.foreach.n.index}>" value="<{$i.regist_flg}>">
                <input type="hidden" name="auth<{$smarty.foreach.n.index}>"value="<{$i.auth}>">
              <{/foreach}>
            </table>
            <{if $sm_auth_flg == '1'}>
              <div id="edit_lock">
                <span class="checkbox_wrap<{if $edit_lock_flg == 't'}> checked<{/if}>">
                  <input type="checkbox" class="checkbox" name="edit_lock_flg" value="t" <{if $edit_lock_flg == 't'}>checked<{/if}>/>
                </span>
                <{$sm_short_name}>以外編集ロック
              </div>
            <{/if}>
          </div>
        </div>        
        <div id="main_btn">
          <input type="button" class="button radius_mini" value="登録" onclick="fncRegist();">
        </div>          
      <{/if}>  
    </form>
  </div>

  <div id="footer">
    <div class="inner">    
      <{* 実行アイコン *}>
			<ul class="sub_nav clearfix">
        <{if $mail_id != ""}>
          <{if $mail_id_mode == "recv" && $arr_btn_flg.return_btn_show_flg == "t"}>
            <li class="sub_nav_replay"><a href="javascript:void(0);" onclick="rp_action_return();return false;">返信</a></li>
          <{/if}>
          <{if $arr_btn_flg.forward_btn_show_flg == "t"}>
            <li class="sub_nav_forward"><a href="javascript:void(0);" onclick="rp_action_forward();return false;">転送</a></li>
          <{/if}>
        <{/if}>
        
        <{* 編集ボタン 20140508 R.C *}>
        <{*	<li class="sub_nav_edit"><a href="javascript:void(0);" onclick="rp_action_update();return false;">編集</a></li> *}>
        
        <{if $mail_id != ""}>
          <li class="sub_nav_return"><a href="javascript:void(0);" onclick="rp_action_message_disp('<{$return_list}>');return false;">表示</a></li>
		<{else}>
          <li class="sub_nav_return"><a href="javascript:void(0);" onclick="rp_action_edit('<{$return_list}>');return false;">表示</a></li>
        <{/if}>
      </ul>
    </div>
  </div>
</body>
</html>
