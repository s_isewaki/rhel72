<{*
インターフェース
    $grps
        array   項目情報配列
    $vals
        array   値の配列
    $grp_flags
        array   レポートに使用する項目グループ
    $rel_grps
        array   連動グループ
    $level_infos
        array   インシレベルとメッセージの配列
    $item_element_no_disp
        array   項目非表示情報配列

//  $mail_input_flg
//      boolean メールメッセージ入力を有効にするフラグ
//  $mail_message
//      string  メールメッセージ(表示値/TinyMCE対応)($mail_input_flg=trueの場合のみ必要。)
//  $registration_date
//      string  報告日(表示値)
    $regist_date_y
        string  報告日(年)
    $regist_date_m
        string  報告日(月)
    $regist_date_d
        string  報告日(非)
    $report_title
        string  タイトル(表示値)

    $experience
        object  ログインユーザーのプロフィール情報
    $input_nocheck
        int     入力チェック実行フラグ(1なら有効)
    $parties_count
        int     プロフィールループ数
    $party_cols
        int     TD列結合数
    $profile
        object  プロフィール情報
    $profile_rowspan
        int     プロフィールの行結合数
    $pt_auth
        int     "1"以外のときに患者マスターからコピーが使用不可
    $session
        string  セッションID
    $input_mode
        string  入力モード。"analysis"の場合は分析・再発防止用入力表示となる。省略可能。

使用するサブテンプレート
    hiyari_edit_easyinput_item_string.tpl
    hiyari_edit_easyinput_item.tpl
*}>
<{if array_intersect(array(3100, 3150, 3200, 3250, 3300), $grp_flags)}>
    <{assign var=party_cols value=3}>
<{else}>
    <{assign var=party_cols value=2}>
<{/if}>

<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
<!--
<{*
//  //====================================================================================================
//  //TinyMCE
//  //====================================================================================================
//
//  if(!tinyMCE.isOpera)
//  {
//      tinyMCE.init({
//          mode : "textareas",
//          theme : "advanced",
//          plugins : "preview,table,emotions,fullscreen,layer",
//          language : "ja_euc-jp",
//          width : "100%",
//          height : "400",
//          editor_selector : "mceEditor",
//          theme_advanced_toolbar_location : "top",
//          theme_advanced_toolbar_align : "left",
//          content_css : "tinymce/tinymce_content.css",
//          theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat,|,bullist,numlist,|,outdent,indent,|,hr,|,charmap,emotions,|,preview,|,undo,redo,|,fullscreen",
//          theme_advanced_buttons2 : "",
//          theme_advanced_buttons3 : ""
//      });
//  }
*}>
//====================================================================================================
//入力チェック
//====================================================================================================
var m_error_color = "#FF6666";//"red";

function checkInput(objId, objName){
    var obj = document.FRM_MAIN[ objId ];
    if ( !obj ) {
        return "";
    }

    //FireFox対応
    //FireFoxで、インシデント概要の必須エラー後別のインシデント概要に変更した場合に
    //上記のobj判定で正しく判定されないケースがある。(エラー対象となったオブジェクトが何故か取得できてしまうため)
    if( objId.substr(1,3) >= 400 && objId.substr(1,3) < 500 ) {
        var obj120s = document.getElementsByName("_120_80");
        var obj120  = obj120s[0];
        if ( obj120 && objId.substr(2, 1) != obj120.options[obj120.selectedIndex].value -1) {
            return "";
        }
    }

    //チェックボックス／ラジオボタン配列の場合
    if ( !obj.type ) {

        //エラー判定
        var is_ok = false;
        for ( var i = 0; i < obj.length; i++ ) {
            //※ラジオボタンの場合、未入力がチェックされているケースがあるため、値のチェックも必要。
            if ( obj[i].checked == true && obj[i].value != "" && obj[i].disabled != true ) {
                is_ok = true;
            }
        }

        //エラーカラー更新
        var color = (is_ok) ? "" :m_error_color;
        for ( var i = 0; i < obj.length; i++ ) {
            //obj[i].style.backgroundColor = color;//FireFoxでは何も起こらない。(color,borderColorも同様)
            obj[i].parentNode.style.backgroundColor = color;//親TDの背景色
        }

        // グループ項目入力チェック
        if (is_ok){
            is_ok = checkGroupItemInput(objId);
        }

        //エラーメッセージ返却
        if(is_ok) {
            return "";
        }
        else {
            return objName + " が未選択です\n";
        }

    }

    //ラジオボタン配列(配列数=1)の場合
    //チェックボックス配列(配列数=1)の場合(通常はitem_must_ableをfalseとし、必須対象としないのが適切。)
    else if ( obj.type == "checkbox" || obj.type == "radio" ) {

        //エラー判定
        var is_ok = obj.checked;

        //エラーカラー更新
        var color = (is_ok) ? "" :m_error_color;
        obj.parentNode.style.backgroundColor = color;//親TDの背景色

        //エラーメッセージ返却
        if ( is_ok ) {
            return "";
        }
        else {
            return objName + " が未選択です\n";
        }
    }

    //プルダウンメニューの場合
    else if ( obj.type == "select-one" ) {

        //エラー判定
        var is_ok = false;
        if ( obj.options[ obj.selectedIndex ].value != ""  && obj.disabled != true ) {
            is_ok = true;
        }

        //エラーカラー更新
        var color = (is_ok) ? "" :m_error_color;
        obj.style.backgroundColor = color;

        //エラーメッセージ返却
        if ( is_ok ) {
            return "";
        }

        else {
            return objName + " が未選択です\n";
        }
    }

    //それ以外の場合
    else {

        //エラー判定
        var is_ok = ( obj.value != "" );

        //エラーカラー更新
        var color = (is_ok) ? "" :m_error_color;
        obj.style.backgroundColor = color;

        //エラーメッセージ返却
        if(is_ok)
        {
            return "";
        }
        else
        {
            return objName + " が未入力です\n";
        }
    }
    return "";
}

//グループ項目入力チェック
function checkGroupItemInput(objId){
    var itemGroups = <{$item_groups}>;
    var itemGroup = itemGroups[objId];
    if (itemGroup == null){  // グループ指定なし
        return true;
    }

    var itemGroupOk = true;
    for (var grpIdx = 0; grpIdx < itemGroup.length; grpIdx++){
        var items = itemGroup[grpIdx];
        if (items.length >= 0){
            var is_ok = false;
            for (var itmIdx = 0; itmIdx < items.length; itmIdx++){
                var obj = document.getElementById(items[itmIdx]);
                if (obj && obj.checked && obj.value != ""){
                    is_ok = true;
                    break;
                }
            }
            if (!is_ok){
                itemGroupOk = false;
            }
            for (var itmIdx = 0; itmIdx < items.length; itmIdx++){
                var obj = document.getElementById(items[itmIdx]);
                if (obj){

                    //エラーカラー更新
                    var color = (is_ok) ? "" :m_error_color;
                    obj.parentNode.style.backgroundColor = color;//親TDの背景色

                }
            }
        }
    }

    return itemGroupOk;
}

<{*
//「なし」と入力されるケースがあるため、
// YYYYMM形式項目のチェックは行わない形に変更。
//
//  //YYYYMM形式項目のチェック
//  function checkInputYYYYMM(objId, objName)
//  {
//      if(
//           objId == "_440_43"
//        || objId == "_460_43"
//        || objId == "_470_43"
//        || objId == "_430_42"
//        || objId == "_450_42"
//        || objId == "_460_52"
//        || objId == "_470_52"
//        || objId == "_480_42"
//        )
//      {
//          var obj = document.FRM_MAIN[ objId ];
//          if ( !obj ) return "";
//
//          //FireFox対応
//          //FireFoxで、インシデント概要の必須エラー後別のインシデント概要に変更した場合に
//          //上記のobj判定で正しく判定されないケースがある。(エラー対象となったオブジェクトが何故か取得できてしまうため)
//          if( objId.substr(1,3) >= 400 && objId.substr(1,3) < 500 )
//          {
//              var obj120s = document.getElementsByName("_120_80");
//              var obj120  = obj120s[0];
//              if(objId.substr(2,1) != obj120.options[obj120.selectedIndex].value -1)
//              {
//                  return "";
//              }
//          }
//
//          //エラー判定
//          oRegExp = new RegExp("^[0-9][0-9][0-9][0-9][0-9][0-9]$");
//          var is_ok = ( obj.value == "" || obj.value.match(oRegExp) );
//
//          //エラーカラー更新
//          var color = (is_ok) ? "" :m_error_color;
//          obj.style.backgroundColor = color;
//
//          //エラーメッセージ返却
//          if(is_ok)
//          {
//              return "";
//          }
//          else
//          {
//              return objName + " はYYYYMM形式で入力して下さい。\n";
//          }
//      }
//      return "";
//  }


*}>

// 日付妥当性チェック
function dateCheck(year, month, day, msg){

    var obj_year = document.FRM_MAIN[ year ];
    var obj_month = document.FRM_MAIN[ month ];
    var obj_day = document.FRM_MAIN[ day ];

    year = parseInt(obj_year.value,10);
    month = parseInt(obj_month.value,10);
    day = parseInt(obj_day.value,10);

    //閏年
    if ((month == 2) && YearChk(year)) {
        month=13;
    }

    //末日チェック
    if (LastDayChk(month,day)) {
        return "";
    }
    else {
        obj_year.style.backgroundColor = m_error_color;
        obj_month.style.backgroundColor = m_error_color;
        obj_day.style.backgroundColor = m_error_color;

        return msg  + 'の日付が正しくありません';
    }
}


//末日チェック
function LastDayChk(in_Month,in_Day){
  lastDay = new Array(31,28,31,30,31,30,31,31,30,31,30,31,29);
  if (lastDay[in_Month-1] >= in_Day) {
    return true;
  }
   else {
    return false;
  }
}

//年チェック
function YearChk(in_Year){
  if ((in_Year % 4) == 0 && ((in_Year % 100) != 0 || (in_Year % 400))) {
    return true;
  }
  return false;
}



//連動元が指定された値であるとき、連動先が必須であることをチェックする。
//引数
// objId 連動元HTMLコントロールID
// objVal 連動対象となる値
// targetObjId 連動先HTMLコントロールID
// tergetName  連動先項目名
//戻値
// checkInput()の同様。
function checkRelateInput(objId, objVal, targetObjId, targetName){
    var obj = document.FRM_MAIN[ objId ];
    if ( !obj ) return "";

    //連動項目のエラークリア
    var t_obj = document.FRM_MAIN[ targetObjId ];
    if ( !t_obj ) return "";
    t_obj.style.backgroundColor = "";

    //通常のコントロールではない場合(チェックボックス配列とみなす。)
    if ( !obj.type ){
        for ( var j = 0; j < obj.length; j++ ){
            if ( obj[j].checked && obj[j].value == objVal ){
                return checkInput( targetObjId, targetName );
            }
        }
        return "";
    }
    //通常のコントロールの場合(プルダウンメニューとみなす。)
    else {
        if ( obj.options[ obj.selectedIndex ].value == objVal ){
            return checkInput( targetObjId, targetName );
        }
        else {
            return "";
        }
    }

    return "";
}



<{if $input_nocheck != 1}>
//タイトル未入力時は、インシデントの内容の先頭10文字を設定
function set_title_from_520_30() {
    var obj_title = document.FRM_MAIN[ 'report_title' ];
    if ( !obj_title ) return "";
    var obj_520_30= document.FRM_MAIN[ '_520_30' ];
    if ( !obj_520_30 ) return "";
    if(obj_title.value == "")
    {
        var t = obj_520_30.value;

        //改行コード等を取り除く
        t = t.replace(/(\n|\r|\t)+/g, "")

        //先頭10文字以外をカット
        if(t.length > 10)
        {
            t = t.substr(0,10);
        }

        obj_title.value = t;
    }
}

//タイトル未入力時は、ヒヤリ・ハット分類を設定
function set_title_from_125_85() {
    var obj_title = document.FRM_MAIN[ 'report_title' ];
    if ( !obj_title ) return "";
    var obj_125_85= document.getElementsByName("_125_85[]");

    if ( !obj_125_85 ) return "";
    if(obj_title.value == "")
    {
        for(i=1; i < obj_125_85.length; i++) {
            if(obj_125_85[i].checked) {
                t = document.getElementById("item_125_85_" + obj_125_85[i].value).value;
                obj_title.value = t;
                break;
            }
        }
    }
}

//下書き保存用のチェック
function chkShitagakiSubmit() {
    // データが全て表示されるまで、submit出来ないようにする 20100319
    if(!document.FRM_MAIN.data_exist_flg) {
        alert("データが全て出揃ってから行ってください");
        return false;
    }

    //タイトル未入力時は、ヒヤリ・ハット分類を優先して設定
    set_title_from_125_85();
    //タイトル未入力時は、インシデントの内容を設定

    <{if $subject_mode == 0}>
    //タイトル未入力時は、インシデントの内容を設定
    set_title_from_520_30();
    <{/if}>

    //タイトルだけ必須。
    var submitFlag = true;
    var errMsg = "";
    <{if $subject_mode == 1}>
    errMsg += checkInput('subject', '表題');
    <{else}>
    errMsg += checkInput('report_title', '表題');
    <{/if}>
    errMsg += dateCheck('regist_date_y', 'regist_date_m', 'regist_date_d', '報告日');

    if ( errMsg != "" ){
        alert( errMsg );
        submitFlag = false;
    }

    return submitFlag;
}

//患者IDのチェック
function chkInputPtid() {
    var submitFlag = true;
    var errMsg = "";
    errMsg += checkInput('_210_10', '患者ID');

    if ( errMsg != "" ){
        alert( errMsg );
        submitFlag = false;
    }

    return submitFlag;
}

//患者情報の取得
function getPtid() {
    if (chkInputPtid()) {
        var id_objs = document.getElementsByName("_210_10");
        var wk_ptid = id_objs[0].value;

        var url = "db2_hiyari_patient_get.php?session=<{$session}>&patient_id=" + wk_ptid;
        var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=200,top=200,width=420,height=100";
        window.open(url, '_blank',option);
    }
}


//全項目チェック
function chkSubmit() {
    // データが全て表示されるまで、submit出来ないようにする 20100319
    if(!document.FRM_MAIN.data_exist_flg) {
        alert("データが全て出揃ってから内容確認を行ってください");
        return false;
    }

    //タイトル未入力時は、ヒヤリ・ハット分類を優先して設定
    set_title_from_125_85();
    <{if $subject_mode == 0}>
    //タイトル未入力時は、インシデントの内容を設定
    set_title_from_520_30();
    <{/if}>
        var submitFlag = true;
    var errMsg = "";
    <{if $subject_mode == 1}>
    errMsg += checkInput('subject', '表題');
    <{else}>
    errMsg += checkInput('report_title', '表題');
    <{/if}>
    errMsg += dateCheck('regist_date_y', 'regist_date_m', 'regist_date_d', '報告日');

<{foreach key=grp_code item=grp_val from=$grps}>

<{foreach key=easy_item_code item=easy_item_val from=$grp_val.easy_item_list}>
    <{if ($grp_code == 100 && $easy_item_code == 10) ||($grp_code == 100 && $easy_item_code == 20) ||($grp_code == 100 && $easy_item_code == 30)}>
        <{* 非表示項目のため、チェック不要。 *}>
    <{elseif ($grp_code == 105 && $easy_item_code == 10) ||($grp_code == 105 && $easy_item_code == 20) ||($grp_code == 105 && $easy_item_code == 30) || ($grp_code == 105 && $easy_item_code == 70) ||($grp_code == 105 && $easy_item_code == 75)}>
        <{* 非表示項目のため、チェック不要。 *}>
    <{elseif ($grp_code >= 4000 && $grp_code <= 4049)}>
        <{* 非表示項目のため、チェック不要。 *}>
        <{elseif $grp_code == 295}>
        <{*発生要因詳細 *}>
        <{if in_array($grp_code|cat:'_'|cat:$easy_item_code, $item_must)}>
            var area_295_id = "area_295_<{$easy_item_code}>";
            var area_295_obj = document.getElementById(area_295_id);
            if(area_295_obj && area_295_obj.style.display == "block")  {                //対象の場合は通常のチェック
                errMsg += checkInput('_<{$grp_code}>_<{$easy_item_code}><{if $easy_item_val.easy_item_type == 'checkbox' or $easy_item_val.easy_item_type == 'radio'}>[]<{/if}>', '<{$easy_item_val.easy_item_name}>');
             }
                <{/if}>
    <{elseif $grp_code == 605}>
        <{*発生要因詳細 *}>
        <{if in_array($grp_code|cat:'_'|cat:$easy_item_code, $item_must)}>
            var area_605_id = "area_605_<{$easy_item_code}>";
            var area_605_obj = document.getElementById(area_605_id);
            if(area_605_obj && area_605_obj.style.display == "block")  {                //対象の場合は通常のチェック
                errMsg += checkInput('_<{$grp_code}>_<{$easy_item_code}><{if $easy_item_val.easy_item_type == 'checkbox' or $easy_item_val.easy_item_type == 'radio'}>[]<{/if}>', '<{$easy_item_val.easy_item_name}>');
             }
                <{/if}>
    <{elseif $grp_code == 110 && $easy_item_code == 65}>
        <{* 発生場所詳細 *}>
        <{if in_array('110_60', $item_must)}>
            if (document.getElementById('span_110_65').firstChild.tagName == 'SELECT') {
                errMsg += checkInput('_110_65', '発生場所詳細');
            }
        <{/if}>
    <{elseif $grp_code == 570 && $easy_item_code != 5}>
        <{* 患者への影響 *}>
        <{if in_array($grp_code|cat:'_'|cat:$easy_item_code, $item_must)}>
            <{if $easy_item_code >= 10 && $easy_item_code <= 19}>var area_570_id = "area_570_1";<{/if}>
            <{if $easy_item_code >= 20 && $easy_item_code <= 29}>var area_570_id = "area_570_2";<{/if}>
            <{if $easy_item_code >= 30 && $easy_item_code <= 39}>var area_570_id = "area_570_3";<{/if}>
            <{if $easy_item_code >= 40 && $easy_item_code <= 49}>var area_570_id = "area_570_4";<{/if}>

            var area_570_obj = document.getElementById(area_570_id);

            if(area_570_obj.style.display == "block") {             //対象の場合は通常のチェック
                errMsg += checkInput('_<{$grp_code}>_<{$easy_item_code}><{if $easy_item_val.easy_item_type == 'checkbox' or $easy_item_val.easy_item_type == 'radio'}>[]<{/if}>', '<{$easy_item_val.easy_item_name}>');

            }       <{/if}>
    <{elseif $grp_code == 580 && $easy_item_code != 5}>
        <{* 対応 *}>
        <{if in_array($grp_code|cat:'_'|cat:$easy_item_code, $item_must)}>
            <{if $easy_item_code >= 10 && $easy_item_code <= 19}>var area_580_id = "area_580_1";<{/if}>
            <{if $easy_item_code >= 20 && $easy_item_code <= 29}>var area_580_id = "area_580_2";<{/if}>
            <{if $easy_item_code >= 30 && $easy_item_code <= 39}>var area_580_id = "area_580_3";<{/if}>
            <{if $easy_item_code >= 40 && $easy_item_code <= 49}>var area_580_id = "area_580_4";<{/if}>
                var area_580_obj = document.getElementById(area_580_id);
                if(area_580_obj.style.display == "block") {                 //対象の場合は通常のチェック
                    errMsg += checkInput('_<{$grp_code}>_<{$easy_item_code}><{if $easy_item_val.easy_item_type == 'checkbox' or $easy_item_val.easy_item_type == 'radio'}>[]<{/if}>', '<{$easy_item_val.easy_item_name}>');
                }       <{/if}>
    <{elseif $grp_code == "1100" && $easy_item_code == "10"}>
        <{if in_array($grp_code|cat:'_'|cat:$easy_item_code, $item_must)}>
                            var obj1100_1 = document.getElementById("id_1100_10_1");
                var obj1100_2 = document.getElementById("id_1100_10_2");
                if( obj1100_1 && obj1100_2 ) {
                    if(obj1100_1.checked == false && obj1100_2.checked == false) {
                        obj1100_1.parentNode.style.backgroundColor = m_error_color;
                        obj1100_2.parentNode.style.backgroundColor = m_error_color;
                        errMsg += "誤った医療の実施の有無 が未選択です\n";
                    } else {
                        obj1100_1.parentNode.style.backgroundColor = '';
                        obj1100_2.parentNode.style.backgroundColor = '';
                    }
                }

                if(obj1100_1.checked == true) {
                    var obj1100_20_1 = document.getElementById("id_1100_20_1");
                    var obj1100_20_2 = document.getElementById("id_1100_20_2");
                    var obj1100_20_3 = document.getElementById("id_1100_20_3");
                    var obj1100_20_4 = document.getElementById("id_1100_20_4");
                    if( obj1100_20_1 && obj1100_20_2 && obj1100_20_3 && obj1100_20_4) {
                        if(obj1100_20_1.checked == false && obj1100_20_2.checked == false && obj1100_20_3.checked == false && obj1100_20_4.checked == false) {
                            obj1100_20_1.parentNode.style.backgroundColor = m_error_color;
                            obj1100_20_2.parentNode.style.backgroundColor = m_error_color;
                            obj1100_20_3.parentNode.style.backgroundColor = m_error_color;
                            obj1100_20_4.parentNode.style.backgroundColor = m_error_color;
                            errMsg += "実施の程度 が未選択です\n";
                        } else {
                            obj1100_20_1.parentNode.style.backgroundColor = '';
                            obj1100_20_2.parentNode.style.backgroundColor = '';
                            obj1100_20_3.parentNode.style.backgroundColor = '';
                            obj1100_20_4.parentNode.style.backgroundColor = '';
                        }
                    }
                }

                if (obj1100_2.checked == true){
                    var obj1100_50_1 = document.getElementById("id_1100_50_1");
                    var obj1100_50_2 = document.getElementById("id_1100_50_2");
                    var obj1100_50_3 = document.getElementById("id_1100_50_3");
                    if ( obj1100_50_1 && obj1100_50_2 && obj1100_50_3){
                      if (obj1100_50_1.checked == false && obj1100_50_2.checked == false && obj1100_50_3.checked == false){
                        obj1100_50_1.parentNode.style.backgroundColor = m_error_color;
                        obj1100_50_2.parentNode.style.backgroundColor = m_error_color;
                        obj1100_50_3.parentNode.style.backgroundColor = m_error_color;
                        errMsg += "仮に実施された場合の影響度 が未選択です\n";
                      } else {
                        obj1100_50_1.parentNode.style.backgroundColor = '';
                        obj1100_50_2.parentNode.style.backgroundColor = '';
                        obj1100_50_3.parentNode.style.backgroundColor = '';
                      }
                    }
                 }
                    <{/if}>
    <{elseif $grp_code == "900" && $easy_item_code == "10"}>
// 1000番台の2010年版は手動でエラーを確認する
        <{if in_array('900_10', $item_must)}>

    if( document.FRM_MAIN._900_10.value ) {
            document.FRM_MAIN._900_10.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._900_10.style.backgroundColor = m_error_color;
            errMsg += "概要 が未記入です\n";
        }
    if( document.FRM_MAIN._910_10.value || document.FRM_MAIN._900_10.value == 2 || document.FRM_MAIN._900_10.value == 8 || !document.FRM_MAIN._910_10[1] || document.FRM_MAIN._910_10[1].value == "") {
            document.FRM_MAIN._910_10.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._910_10.style.backgroundColor = m_error_color;
            errMsg += "種類 が未記入です\n";
        }
    if( document.FRM_MAIN._920_10.value || document.FRM_MAIN._900_10.value == 2 || document.FRM_MAIN._900_10.value == 8 || !document.FRM_MAIN._920_10[1] || document.FRM_MAIN._920_10[1].value == "") {
            document.FRM_MAIN._920_10.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._920_10.style.backgroundColor = m_error_color;
            errMsg += "種類の項目 が未記入です\n";
        }

    if( document.FRM_MAIN._930_10.value || document.FRM_MAIN._900_10.value != 8 || !document.FRM_MAIN._930_10[1] || document.FRM_MAIN._930_10[1].value == "") {
            document.FRM_MAIN._930_10.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._930_10.style.backgroundColor = m_error_color;
            errMsg += "種類のその他 が未記入です\n";
        }
    obj_940_10 = document.getElementById("id_940_10");
    obj_950_10 = document.getElementById("id_950_10");
    obj_970_10 = document.getElementById("id_970_10");
    obj_980_10 = document.getElementById("id_980_10");

    if( document.FRM_MAIN._940_10.value || document.FRM_MAIN._900_10.value == 8 || !obj_940_10.options[1] || obj_940_10.options[1].value == '') {
            document.FRM_MAIN._940_10.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._940_10.style.backgroundColor = m_error_color;
            errMsg += "発生場面 が未記入です\n";
        }

    if( document.FRM_MAIN._950_10.value || document.FRM_MAIN._900_10.value == 8 || !obj_950_10.options[1] || obj_950_10.options[1].value == '') {
            document.FRM_MAIN._950_10.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._950_10.style.backgroundColor = m_error_color;
            errMsg += "発生場面の項目 が未記入です\n";
        }

    if( document.FRM_MAIN._960_10.value || document.FRM_MAIN._900_10.value != 8) {
            document.FRM_MAIN._960_10.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._960_10.style.backgroundColor = m_error_color;
            errMsg += "発生場面のその他 が未記入です\n";
        }

    if( document.FRM_MAIN._970_10.value || document.FRM_MAIN._900_10.value == 8 || !obj_970_10.options[1] || obj_970_10.options[1].value == '') {
            document.FRM_MAIN._970_10.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._970_10.style.backgroundColor = m_error_color;
            errMsg += "事故(事例)の内容 が未記入です\n";
        }

    if( document.FRM_MAIN._980_10.value || document.FRM_MAIN._900_10.value == 8 || !obj_980_10.options[1] || obj_980_10.options[1].value == '') {
            document.FRM_MAIN._980_10.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._980_10.style.backgroundColor = m_error_color;
            errMsg += "事故(事例)の内容の項目 が未記入です\n";
        }

    if( document.FRM_MAIN._990_10.value || document.FRM_MAIN._900_10.value != 8) {
            document.FRM_MAIN._990_10.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._990_10.style.backgroundColor = m_error_color;
            errMsg += "事故(事例)の内容のその他 が未記入です\n";
        }
<{/if}>
<{if in_array("1300_10", $item_must) || in_array("1310_10", $item_must) || in_array("1320_10", $item_must)}>
if( obj1000 = document.getElementById("id_900_10") ) {
    if( obj1000.value == 1 ) {
    <{if in_array("1300_10", $item_must)}>
        if( document.FRM_MAIN._1000_5.value ) {
            document.FRM_MAIN._1000_5.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._1000_5.style.backgroundColor = m_error_color;
            errMsg += "販売名 が未記入です\n";
        }

        if( document.FRM_MAIN._1000_15.value ) {
            document.FRM_MAIN._1000_15.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._1000_15.style.backgroundColor = m_error_color;
            errMsg += "製造販売業者名 が未記入です\n";
        }
    <{/if}>    } else if( obj1000.value == 3 || obj1000.value == 5 || obj1000.value == 7 ) {
        <{if in_array("1310_10", $item_must)}>
            if( document.FRM_MAIN._1000_20.value || obj1000.value == 7) {
            document.FRM_MAIN._1000_20.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._1000_20.style.backgroundColor = m_error_color;
            errMsg += "販売名 が未記入です\n";
        }
        if( document.FRM_MAIN._1000_25.value || obj1000.value == 7) {
            document.FRM_MAIN._1000_25.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._1000_25.style.backgroundColor = m_error_color;
            errMsg += "製造販売業者名 が未記入です\n";
        }
        if( document.FRM_MAIN._1000_30.value || obj1000.value == 7) {
            document.FRM_MAIN._1000_30.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._1000_30.style.backgroundColor = m_error_color;
            errMsg += "購入年月 が未記入です\n";
        }
    <{/if}>    } else if( obj1000.value == 4 || obj1000.value == 6 ) {
        <{if in_array("1320_10", $item_must)}>
            if( document.FRM_MAIN._1000_35.value ) {
            document.FRM_MAIN._1000_35.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._1000_35.style.backgroundColor = m_error_color;
            errMsg += "販売名 が未記入です\n";
        }
        if( document.FRM_MAIN._1000_40.value ) {
            document.FRM_MAIN._1000_40.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._1000_40.style.backgroundColor = m_error_color;
            errMsg += "製造販売業者名 が未記入です\n";
        }
        if( document.FRM_MAIN._1000_45.value ) {
            document.FRM_MAIN._1000_45.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._1000_45.style.backgroundColor = m_error_color;
            errMsg += "製造年月 が未記入です\n";
        }
        if( document.FRM_MAIN._1000_50.value ) {
            document.FRM_MAIN._1000_50.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._1000_50.style.backgroundColor = m_error_color;
            errMsg += "購入年月 が未記入です\n";
        }
        if( document.FRM_MAIN._1000_55.value ) {
            document.FRM_MAIN._1000_55.style.backgroundColor = "";
        } else {
            document.FRM_MAIN._1000_55.style.backgroundColor = m_error_color;
            errMsg += "直近の保守・点検年月 が未記入です\n";
        }
    <{/if}>    }
}
<{  /if}>
<{ else}>
<{*
    <{if $easy_item_val.easy_item_must == 1}>
        errMsg += checkInput('_<{$grp_code}>_<{$easy_item_code}><{if $easy_item_val.easy_item_type == 'checkbox' or $easy_item_val.easy_item_type == 'radio'}>[]<{/if}>', '<{$easy_item_val.easy_item_name}>');
    <{/if}>
    <{if $easy_item_val.relate_other != ""}>
        errMsg += checkRelateInput('_<{$grp_code}>_<{$easy_item_code}><{if $easy_item_val.easy_item_type == 'checkbox' or $easy_item_val.easy_item_type == 'radio'}>[]<{/if}>', '<{$easy_item_val.relate_other.easy_code}>', '_<{$grp_code}>_<{$easy_item_val.relate_other.rel_item_code}>', '<{$easy_item_val.relate_other.rel_item_name}>');
    <{/if}>
*}>
    <{if in_array($grp_code|cat:'_'|cat:$easy_item_code, $item_must)}>
        <{if $time_setting_flg == true }>
            <{if !(($grp_code == 100 && $easy_item_code == 60) || ($grp_code == 100 && $easy_item_code == 65) || ($grp_code == 105 && $easy_item_code == 60) || ($grp_code == 105 && $easy_item_code == 65)) }>
                errMsg += checkInput('_<{$grp_code}>_<{$easy_item_code}><{if $easy_item_val.easy_item_type == 'checkbox' or $easy_item_val.easy_item_type == 'radio'}>[]<{/if}>', '<{$easy_item_val.easy_item_name}>');
            <{/if}>
        <{else}>
            <{if !(($grp_code == 100 && $easy_item_code == 40) || ($grp_code == 105 && $easy_item_code == 40)) }>
                errMsg += checkInput('_<{$grp_code}>_<{$easy_item_code}><{if $easy_item_val.easy_item_type == 'checkbox' or $easy_item_val.easy_item_type == 'radio'}>[]<{/if}>', '<{$easy_item_val.easy_item_name}>');
            <{/if}>
        <{/if}>
    <{/if}>
<{/if}>
<{/foreach}>
<{/foreach}>

    errMsg += multi_touzisha_check();
    errMsg += multi_jirei_check();

    if ( errMsg != "" ){
        alert( errMsg );
        submitFlag = false;
    }

    return submitFlag;
}


//複数当事者の入力チェック
//最下部の当事者に当事者情報が入力されている場合、上部の当事者の項目のいずれかは入力必須となる。
//但し、以下の場合はチェック対象外となる。
//・当事者が１人の場合
//・当事者項目に１つでも必須項目がある場合
//・最終表示当事者
function multi_touzisha_check() {
    //返却用エラーメッセージ
    var err_msg = "";

    //==================================================
    //オブジェクトID配列を初期化
    //==================================================
    var touzisha_obj_id_list = new Array(10);
    for(var i=0;i<10;i++)
    {
        touzisha_obj_id_list[i] = new Object();
    }

    //==================================================
    //オブジェクトID配列にID情報を格納
    //==================================================
    <{foreach key=grp_code item=grp_val from=$grps}>
<{if $grp_code > 3000 && $grp_code < 3500}>
<{foreach key=easy_item_code item=easy_item_val from=$grp_val.easy_item_list}>
<{if in_array($grp_code|cat:'_'|cat:$easy_item_code, $item_must)}>
                    //必須項目が存在するため、チェック不要。
                    return "";
<{/if}>
touzisha_obj_id_list[<{$grp_code}>%10].id_<{$grp_code}>_<{$easy_item_code}> = '_<{$grp_code}>_<{$easy_item_code}><{if $easy_item_val.easy_item_type == 'checkbox' or $easy_item_val.easy_item_type == 'radio'}>[]<{/if}>';
<{/foreach}>
<{/if}>
<{/foreach}>
        //==================================================
    //最終表示当事者を判定／エラークリア
    //==================================================
    var last_disp_touzisha = 0;
    //各担当者に対して
    for(var i=0;i<10;i++)
    {
        var is_no_obj = true;

        //各当事者入力項目に対して
        for(var object_field_name in touzisha_obj_id_list[i])
        {
            //入力項目オブジェクト
            var obj_id = eval('touzisha_obj_id_list[i].' + object_field_name);
            var obj = document.FRM_MAIN[ obj_id ];
            if ( obj )
            {

                //入力項目オブジェクトあり
                is_no_obj = false;

                //エラークリア
                obj.style.backgroundColor = "";

            }
        }

        //オブジェクトが１つも無い場合
        if(is_no_obj)
        {
            //以降、非表示状態のため、ループ終了
            break;
        }
        else
        {
            //最終表示当事者を更新
            last_disp_touzisha = i;
        }
    }

    //==================================================
    //当事者が１人の場合はチェック不要。
    //==================================================
    if(last_disp_touzisha == 0)
    {
        return "";
    }

    //==================================================
    //必須チェック＆エラー処理
    //==================================================
    //最終表示当事者以外の各表示当事者に対して
    for(var i=0;i<last_disp_touzisha;i++)
    {
        var is_no_input = true;

        //各当事者入力項目に対して
        for(var object_field_name in touzisha_obj_id_list[i])
        {
            //入力項目オブジェクト
            var obj_id = eval('touzisha_obj_id_list[i].' + object_field_name);
            var obj = document.FRM_MAIN[ obj_id ];
            if ( !obj )
            {
                continue;
            }

            //入力チェック
            if( obj.type == "select-one" )
            {
                if(obj.options[ obj.selectedIndex ].value != "")
                {
                    is_no_input = false
                    break;
                }
            }
            else
            {
                if(obj.value != "")
                {
                    is_no_input = false
                    break;
                }
            }
        }

        //まったく入力されていない場合
        if(is_no_input)
        {
            //エラーとみなす。
            err_msg += "当事者" + (i + 1) + "が未入力です\n";

            //各当事者入力項目に対して
            for(var object_field_name in touzisha_obj_id_list[i])
            {
                //入力項目オブジェクト
                var obj_id = eval('touzisha_obj_id_list[i].' + object_field_name);
                var obj = document.FRM_MAIN[ obj_id ];
                if ( obj )
                {
                    //エラーカラー設定
                    obj.style.backgroundColor = m_error_color;
                }
            }

        }
    }

    return err_msg;
}
//事例の詳細(時系列)の入力チェック
//最下部に情報が入力されている場合、上部の項目は入力必須となる。
//但し、以下の場合はチェック対象外となる。
//・当事者が１人の場合
//・当事者項目に１つでも必須項目がある場合
//・最終表示当事者
function multi_jirei_check() {
<{if in_array('4000_1', $item_must)}>
    //返却用エラーメッセージ
    var err_msg = "";
    var is_no_input = true;
    for(var grp_code = 4000; grp_code <= 4049; grp_code++) {
        if (document.FRM_MAIN[ '_'+grp_code+'_1']) {
            if (!document.FRM_MAIN[ '_'+grp_code+'_1'].value) {
                is_no_input = false;
                document.FRM_MAIN[ '_'+grp_code+'_1'].style.backgroundColor = m_error_color;
            }
            if (!document.FRM_MAIN[ '_'+grp_code+'_2'].value) {
                is_no_input = false;
                document.FRM_MAIN[ '_'+grp_code+'_2'].style.backgroundColor = m_error_color;
            }
            if (!document.FRM_MAIN[ '_'+grp_code+'_3'].value) {
                is_no_input = false;
                document.FRM_MAIN[ '_'+grp_code+'_3'].style.backgroundColor = m_error_color;
            }
            if (!document.FRM_MAIN[ '_'+grp_code+'_4'].value) {
                is_no_input = false;
                document.FRM_MAIN[ '_'+grp_code+'_4'].style.backgroundColor = m_error_color;
            }
            if (!document.FRM_MAIN[ '_'+grp_code+'_5'].value) {
                is_no_input = false;
                document.FRM_MAIN[ '_'+grp_code+'_5'].style.backgroundColor = m_error_color;
            }
        }
    }
    if (!is_no_input) {
        err_msg += "事例の詳細(時系列)が未入力です\n";
    }
    return err_msg;
<{else}>
    return "";
<{/if}>
}

<{else}>
    function chkSubmit(){
        return true;
    }
<{/if}>
//====================================================================================================
//ラジオボタンクリック時の動作
//====================================================================================================
function radio_onclick(this_obj) {
    var obj_id = this_obj.id;
    if(! obj_id)
    {
        return;
    }

    var obj_id_info_arr = obj_id.split("_");//0:固定文字｢id｣,1:グループコード,2:項目コード,3:項目インデックス(※選択項目コードではない。)
    if(obj_id_info_arr.length != 4)
    {
        return;
    }

    var grp_code = obj_id_info_arr[1];
    var easy_item_code = obj_id_info_arr[2];
    var item_index = obj_id_info_arr[3];

    //ヒヤリハット分類の場合
    if(grp_code == "125" && easy_item_code == "85")
    {
        auto_set_from_125(this_obj.value);
    }
}

// ヒヤリハット分類で選択された値をタイトルに起用する 20090909
//  function change_title_125(this_obj, title) {
//      var ttl = document.FRM_MAIN.report_title;
//      if(!ttl.value) {
//          ttl.value = title;
//      }
//  }

//ヒヤリハット分類よりインシデント概要以下を自動設定します。
function auto_set_from_125(value_125) {
    var obj_120 = document.getElementById("id_120_80");
    if(!obj_120)
    {
        //インシデントの概要が存在しない場合は何もしない。
        return;
    }
    if(obj_120.value != "")
    {
        //インシデントの概要が未選択ではない場合は何もしない。
        return;
    }

    switch (value_125) {
        case "05"://輸血
            set_select_ctl_from_value(obj_120,"3");
            relate_detail(obj_120,'rel_400');
            break;
        case "09"://リハビリテーション
            set_select_ctl_from_value(obj_120,"4");
            relate_detail(obj_120,'rel_400');
            var obj_430_10 = document.getElementById("id_430_10");
            if(obj_430_10) {
                set_select_ctl_from_value(obj_430_10,"25");
            }
            break;
        case "13"://ドレーン・チューブ類の使用・管理
            set_select_ctl_from_value(obj_120,"6");
            relate_detail(obj_120,'rel_400');
            break;
        case "14"://観察
            set_select_ctl_from_value(obj_120,"9");
            relate_detail(obj_120,'rel_400');
            var obj_480_10 = document.getElementById("id_480_10");
            if(obj_480_10) {
                set_select_ctl_from_value(obj_480_10,"14");
            }
            break;
        case "16"://排泄の介助
            set_select_ctl_from_value(obj_120,"9");
            relate_detail(obj_120,'rel_400');
            var obj_480_10 = document.getElementById("id_480_10");
            if(obj_480_10) {
                set_select_ctl_from_value(obj_480_10,"7");
            }
            break;
        case "18"://移送
            set_select_ctl_from_value(obj_120,"9");
            relate_detail(obj_120,'rel_400');
            var obj_480_10 = document.getElementById("id_480_10");
            if(obj_480_10) {
                set_select_ctl_from_value(obj_480_10,"10");
            }
            break;
        case "19"://転倒
            set_select_ctl_from_value(obj_120,"9");
            relate_detail(obj_120,'rel_400');
            var obj_480_20 = document.getElementById("id_480_20");
            if(obj_480_20) {
                set_select_ctl_from_value(obj_480_20,"1");
            }
            break;
        case "28"://転落
            set_select_ctl_from_value(obj_120,"9");
            relate_detail(obj_120,'rel_400');
            var obj_480_20 = document.getElementById("id_480_20");
            if(obj_480_20) {
                set_select_ctl_from_value(obj_480_20,"2");
            }
            break;
        case "23"://説明
            set_select_ctl_from_value(obj_120,"10");
            relate_detail(obj_120,'rel_400');
            var obj_490_10 = document.getElementById("id_490_10");
            if(obj_490_10) {
                set_select_ctl_from_value(obj_490_10,"11");
            }
            break;
        case "25"://歯科医療用具
            set_select_ctl_from_value(obj_120,"7");
            relate_detail(obj_120,'rel_400');
            break;
        default:
            //それ以外の場合は連動しない。
            break;
    }
}



//====================================================================================================
//チェックボックスクリック時の動作
//====================================================================================================
function checkbox_onclick(this_obj) {
    <{if in_array(605, $grp_flags) || in_array(140, $grp_flags) || in_array(295, $grp_flags) || in_array(1410, $grp_flags)}>
        var obj_id = this_obj.id;
        if(! obj_id)
        {
            return;
        }

        var obj_id_info_arr = obj_id.split("_");//0:固定文字｢id｣,1:グループコード,2:項目コード,3:項目インデックス(※選択項目コードではない。)
        if(obj_id_info_arr.length != 4 && obj_id_info_arr[1] != 1410)
        {
            return;
        }

        var grp_code = obj_id_info_arr[1];
        var easy_item_code = obj_id_info_arr[2];
        var item_index = obj_id_info_arr[3];

        // フォームのVALUEの値
        var serial_number = this_obj.value;

        <{if in_array(605, $grp_flags) }>
            //発生要因の場合
            if ( grp_code == "600" && easy_item_code == "10" ) {
                area_605_disp_change();
            }
        <{/if}>
        if(grp_code == "1410" && easy_item_code == "20") {
            var n = document.getElementsByName("_1410_20[]");
            for(i=0; n.length>i; i++) {
                if(this_obj.value == n[i].value && this_obj.checked == true) {
                    if(id_1410_30_ic_obj = document.getElementById("id_1410_30_ic_"+n[i].value)) {
                        id_1410_30_ic_obj.style.display = "block";
                    }
                } else if(this_obj.value == n[i].value){
                    if(id_1410_30_ic_obj = document.getElementById("id_1410_30_ic_"+n[i].value)) {
                        id_1410_30_ic_obj.style.display = "none";

                        var nn = document.getElementsByName("_1410_30[]");
                        for(j=0; nn.length>j; j++) {
                            obj_id_ic_arr = nn[j].id.split("_");
                            if(obj_id_ic_arr[3] == n[i].value) {
                                nn[j].checked = false;
                            }
                        }
                    }
                }
            }
        }

        // インシデント直前の患者の状態の場合
        if(grp_code == "290" && easy_item_code == "120")
        {
            area_295_disp_change();
        }
        <{if in_array(605, $grp_flags) }>
            //その他の内容の場合
            if((grp_code == "605" && easy_item_code == "1") || (grp_code == "600" && easy_item_code == "10" && serial_number == "1")) area_605_101_disp_change();
            if((grp_code == "605" && easy_item_code == "2") || (grp_code == "600" && easy_item_code == "10" && serial_number == "2")) area_605_102_disp_change();
            if((grp_code == "605" && easy_item_code == "3") || (grp_code == "600" && easy_item_code == "10" && serial_number == "3")) area_605_103_disp_change();
            if((grp_code == "605" && easy_item_code == "4") || (grp_code == "600" && easy_item_code == "10" && serial_number == "4")) area_605_104_disp_change();
            if((grp_code == "605" && easy_item_code == "5") || (grp_code == "600" && easy_item_code == "10" && serial_number == "5")) area_605_105_disp_change();
            if((grp_code == "605" && easy_item_code == "6") || (grp_code == "600" && easy_item_code == "10" && serial_number == "6")) area_605_106_disp_change();
            if((grp_code == "605" && easy_item_code == "7") || (grp_code == "600" && easy_item_code == "10" && serial_number == "7")) area_605_107_disp_change();
            if((grp_code == "605" && easy_item_code == "8") || (grp_code == "600" && easy_item_code == "10" && serial_number == "8")) area_605_108_disp_change();
            if((grp_code == "605" && easy_item_code == "9") || (grp_code == "600" && easy_item_code == "10" && serial_number == "9")) area_605_109_disp_change();
            if((grp_code == "605" && easy_item_code == "10") || (grp_code == "600" && easy_item_code == "10" && serial_number == "10")) area_605_110_disp_change();
            if((grp_code == "605" && easy_item_code == "11") || (grp_code == "600" && easy_item_code == "10" && serial_number == "11")) area_605_111_disp_change();
            if((grp_code == "605" && easy_item_code == "13") || (grp_code == "600" && easy_item_code == "10" && serial_number == "13")) area_605_112_disp_change();
            if((grp_code == "605" && easy_item_code == "14") || (grp_code == "600" && easy_item_code == "10" && serial_number == "14")) area_605_113_disp_change();
            if(grp_code == "600" && easy_item_code == "10" && serial_number == "15") area_605_114_disp_change();
            if(grp_code == "600" && easy_item_code == "10" && serial_number == "16") area_605_115_disp_change();
            if(grp_code == "600" && easy_item_code == "10" && serial_number == "17") area_605_116_disp_change();
            if((grp_code == "605" && easy_item_code == "18") || (grp_code == "600" && easy_item_code == "10" && serial_number == "18")) area_605_117_disp_change();
            if((grp_code == "605" && easy_item_code == "18") || (grp_code == "600" && easy_item_code == "10" && serial_number == "18")) area_605_118_disp_change();
            if((grp_code == "605" && easy_item_code == "19") || (grp_code == "600" && easy_item_code == "10" && serial_number == "19")) area_605_119_disp_change();
            if((grp_code == "605" && easy_item_code == "20") || (grp_code == "600" && easy_item_code == "10" && serial_number == "20")) area_605_120_disp_change();
            if(grp_code == "600" && easy_item_code == "10" && serial_number == "25") area_605_121_disp_change();
        <{/if}>
    <{/if}>
}

//発生要因詳細の表示を発生要因のチェック状態に対し正しい形に変更します。
//※grp_code=600のeasy_code が grp_code=605のeasy_item_code である仕様が前提のロジックになっています。拡張時は注意。
function area_605_disp_change() {
    <{if in_array(600, $grp_flags) && in_array(605, $grp_flags)}>
    <{* //発生要因の各チェックボックスに対して *}>
    <{section name=k loop=$grps[600].easy_item_list[10].easy_list}>
    <{assign var="easy_code_600" value=$grps[600].easy_item_list[10].easy_list[k].easy_code}>
    <{*//発生要因のチェックボックスオブジェクト *}>
    var checkbox_600_obj = document.getElementById("id_600_10_<{$smarty.section.k.index}>");
    <{* //発生要因詳細の表示領域オブジェクト *}>
    var area_605_obj     = document.getElementById("area_605_<{$easy_code_600}>");
    <{* //項目が非表示設定されていない場合 *}>
    if(checkbox_600_obj) {
        <{assign var="easy_item_code_605" value=$easy_code_600}>
        <{if array_key_exists($easy_item_code_605,$grps[605].easy_item_list)}>
        <{* //連動対象の発生要因の場合 *}>
        <{* //発生要因がチェックされている場合 *}>
        if(checkbox_600_obj.checked) {
            <{if count($grps[605].easy_item_list[$easy_item_code_605].easy_list) == 0}>
            <{* //詳細が0項目の場合(現状、このケースはない。) *}>
            <{* //詳細を非表示(非表示のままにする。) *}>
                area_605_obj.style.display = "none";
            <{else}>
            <{*//詳細が2項目以上の場合 詳細を表示 *}>
           area_605_obj.style.display = "block";
       <{/if}>
        <{*//発生要因がチェックされていない場合 *}>
    } else {
        <{*//詳細を非表示 *}>
        area_605_obj.style.display = "none";
        <{*//詳細の全項目のチェック状態を解除 *}>
        <{*//※非表示のままチェック状態になっているケースもあるため、必ず行う。 *}>
        <{if $grps[605].easy_item_list[$easy_item_code_605].easy_item_type == "checkbox"}>
            <{*//詳細項目がチェックボックスの場合 *}>
            <{section name=k2 loop=$grps[605].easy_item_list[$easy_item_code_605].easy_list}>
                document.getElementById("id_605_<{$easy_item_code_605}>_<{$smarty.section.k2.index}>").checked = false;
            <{/section}>
        <{else}>
        <{*//詳細項目がプルダウンメニューの場合 *}>
        document.getElementById("id_605_<{$easy_item_code_605}>").selectedIndex = 0;
        <{/if}>
    }
    <{/if}>
    }
    <{/section}>
<{/if}>


    }

function area_295_disp_change() {
        <{if in_array(290, $grp_flags) && in_array(295, $grp_flags)}>
<{*      //発生要因の各チェックボックスに対して *}>
        <{section name=k loop=$grps[290].easy_item_list[120].easy_list}>
        <{assign var="easy_code_290" value=$grps[290].easy_item_list[120].easy_list[k].easy_code}>
<{*              //発生要因のチェックボックスオブジェクト *}>
       var checkbox_290_obj = document.getElementById("id_290_120_<{$smarty.section.k.index}>");<{*              //発生要因詳細の表示領域オブジェクト *}>
       var area_295_obj     = document.getElementById("area_295_<{$easy_code_290}>");<{*              //項目が非表示設定されていない場合 *}>
       if(checkbox_290_obj) { <{        assign var="easy_item_code_295" value=$easy_code_290}>
<{          if array_key_exists($easy_item_code_295,$grps[295].easy_item_list)}>
<{*        //連動対象の発生要因の場合 *}>
<{*        //発生要因がチェックされている場合 *}>
           if(checkbox_290_obj.checked) { <{               if count($grps[295].easy_item_list[$easy_item_code_295].easy_list) == 0}>
<{*        //詳細が0項目の場合(現状、このケースはない。) *}>
<{*        //詳細を非表示(非表示のままにする。) *}>
                   area_295_obj.style.display = "none";<{               elseif count($grps[295].easy_item_list[$easy_item_code_295].easy_list) == 1}>
<{*        //詳細が1項目の場合(このケースは仕様上、チェックボックスに限定される。) *}>
<{*        //詳細を非表示(非表示のままにする。) *}>
<{*        //非表示のままチェック状態にする。 *}>
                   area_295_obj.style.display = "none";                   document.getElementById("id_295_<{$easy_item_code_295}>_0").checked = true;;<{              else}>
<{*        //詳細が2項目以上の場合 *}>
<{*        //詳細を表示 *}>
                   area_295_obj.style.display = "block";<{              /if}>
<{*        //発生要因がチェックされていない場合 *}>
       } else {<{*        //詳細を非表示 *}>
<{*        //詳細の全項目のチェック状態を解除 *}>
<{*        //※非表示のままチェック状態になっているケースもあるため、必ず行う。 *}>
                   area_295_obj.style.display = "none";<{              if $grps[295].easy_item_list[$easy_item_code_295].easy_item_type == "checkbox"}>
<{*        //詳細項目がチェックボックスの場合 *}>
<{                section name=k2 loop=$grps[295].easy_item_list[$easy_item_code_295].easy_list}>
                       document.getElementById("id_295_<{$easy_item_code_295}>_<{$smarty.section.k2.index}>").checked = false;<{                /section}>
<{              else}>
<{*        //詳細項目がプルダウンメニューの場合 *}>
                   var selectbox_295_obj = document.getElementById("id_295_<{$easy_item_code_295}>").selectedIndex = 0;<{              /if}>
       }<{           /if}>
   }<{     /section}>
    <{/if}>
    }



// その他にチェックされているときの初期値
function area_605_others_disp_change() {
        <{if in_array(600, $grp_flags) && in_array(605, $grp_flags)}>
        area_605_101_disp_change();
        area_605_102_disp_change();
        area_605_103_disp_change();
        area_605_104_disp_change();
        area_605_105_disp_change();
        area_605_106_disp_change();
        area_605_107_disp_change();
        area_605_108_disp_change();
        area_605_109_disp_change();
        area_605_110_disp_change();
        area_605_111_disp_change();
        area_605_112_disp_change();
        area_605_113_disp_change();
        area_605_114_disp_change();
        area_605_115_disp_change();
        area_605_116_disp_change();
        area_605_117_disp_change();
        area_605_118_disp_change();
        area_605_119_disp_change();
        area_605_120_disp_change();
        area_605_121_disp_change();
        <{/if}>
    }

//その他の内容を表示させるためのロジック
//※grp_code=605でeasy_item_codeが101以上のとき表示させるロジックです
function area_605_disp_common_change(checkbox_name, obj_id, is_value, formobj) {
    var chk = document.getElementsByName(checkbox_name);
    //発生要因詳細の表示領域オブジェクト
    var obj = document.getElementById(obj_id);
    if (obj) {
        for(var i=0; i<chk.length; i++) {
            if(chk.item(i).checked == true && chk.item(i).value == is_value) {
                //詳細を表示
                obj.style.display = "block";
                break;
            } else if(chk.item(i).checked == false && chk.item(i).value == is_value) {
                obj.style.display = "none";
                formobj.value = "";
            } else {
                obj.style.display = "none";
            }
        }
    }
}
function area_605_disp_common_change2(checkbox_id, obj_id, is_value, formobj) {
    var chk = document.getElementById(checkbox_id); //発生要因のチェックボックスオブジェクト
    var obj = document.getElementById(obj_id); //発生要因詳細の表示領域オブジェクト
    if(chk.value == is_value) { //発生要因がチェックされている場合は詳細を表示
        obj.style.display = "block";
    } else {
        obj.style.display = "none";
        formobj.value = "";
    }
}

function area_605_101_disp_change() { area_605_disp_common_change("_605_1[]",  "area_605_101", 2,  document.FRM_MAIN._605_101); }
function area_605_102_disp_change() { area_605_disp_common_change("_605_2[]",  "area_605_102", 4,  document.FRM_MAIN._605_102); }
function area_605_103_disp_change() { area_605_disp_common_change("_605_3[]",  "area_605_103", 6,  document.FRM_MAIN._605_103); }
function area_605_104_disp_change() { area_605_disp_common_change("_605_4[]",  "area_605_104", 9,  document.FRM_MAIN._605_104); }
function area_605_105_disp_change() { area_605_disp_common_change("_605_5[]",  "area_605_105", 12, document.FRM_MAIN._605_105); }
function area_605_106_disp_change() { area_605_disp_common_change("_605_6[]",  "area_605_106", 17, document.FRM_MAIN._605_106); }
function area_605_107_disp_change() { area_605_disp_common_change("_605_7[]",  "area_605_107", 21, document.FRM_MAIN._605_107); }
function area_605_108_disp_change() { area_605_disp_common_change("_605_8[]",  "area_605_108", 28, document.FRM_MAIN._605_108); }
function area_605_109_disp_change() { area_605_disp_common_change("_605_9[]",  "area_605_109", 33, document.FRM_MAIN._605_109); }
function area_605_110_disp_change() { area_605_disp_common_change("_605_10[]", "area_605_110", 42, document.FRM_MAIN._605_110); }
function area_605_111_disp_change() { area_605_disp_common_change("_605_11[]", "area_605_111", 48, document.FRM_MAIN._605_111); }
function area_605_112_disp_change() { area_605_disp_common_change("_605_13[]", "area_605_112", 57, document.FRM_MAIN._605_112); }
function area_605_113_disp_change() { area_605_disp_common_change("_605_14[]", "area_605_113", 63, document.FRM_MAIN._605_113); }
function area_605_114_disp_change() { area_605_disp_common_change2("id_605_16", "area_605_114", "82", document.FRM_MAIN._605_114); }
function area_605_115_disp_change() { area_605_disp_common_change2("id_605_15", "area_605_115", "73", document.FRM_MAIN._605_115); }
function area_605_116_disp_change() { area_605_disp_common_change2("id_605_17", "area_605_116", "90", document.FRM_MAIN._605_116); }
function area_605_117_disp_change() { area_605_disp_common_change("_605_18[]", "area_605_117", 96, document.FRM_MAIN._605_117); }
function area_605_118_disp_change() { area_605_disp_common_change("_605_18[]", "area_605_118", 98, document.FRM_MAIN._605_118); }
function area_605_119_disp_change() { area_605_disp_common_change("_605_19[]", "area_605_119", 102, document.FRM_MAIN._605_119); }
function area_605_120_disp_change() { area_605_disp_common_change("_605_20[]", "area_605_120", 106, document.FRM_MAIN._605_120); }
function area_605_121_disp_change() { area_605_disp_common_change("_605_10[]", "area_605_121", 25, document.FRM_MAIN._605_121); }
<{* アセスメント・患者の状態 20090304 *}>
<{* clickイベントを設定 *}>
j$(document).ready(function(){
    // アセスメント・患者の状態
    j$(".140_10").click(function() {
        if (this.checked) {
            j$(".150_"+this.value).attr("disabled", false);
            // ラジオ選択チェック
            if (j$(".150_"+this.value).attr("type") == 'radio') {
                var radio_flg = false;
                j$(".150_"+this.value).each(function(){
                    if (this.checked) {
                        j$(this).click();
                        radio_flg = true;
                    }
                },radio_flg);
                if (!radio_flg) {
                    j$(".150_"+this.value).each(function(){
                        j$(this).click();
                        this.checked = true;
                        return false;
                    });
                }
            }
        }
        else {
            j$(".150_"+this.value).attr("disabled", true);
            j$(".150_"+this.value+"_10").attr("disabled", true);
            j$(".150_"+this.value+"_20").attr("disabled", true);
        }
    });

    // リスク回避用具
    j$(".150_6").click(function() {
        j$(".150_6_10").attr("disabled", this.value != 10);
        if (this.value != 10) {
            j$(".150_6_20").attr("disabled", true);
        }
        j$(".150_6_20").attr("disabled", !j$("#150_6_10_99").attr('checked') || j$("#150_6_10_99").attr('disabled'));
    });
    // リスク回避器具：有り：その他
    j$("#150_6_10_99").click(function(){
        j$(".150_6_20").attr("disabled", !this.checked || this.disabled);
    });

    // 拘束用具
    j$(".150_7").click(function() {
        j$(".150_7_10").attr("disabled", this.value != 10);
        if (this.value != 10) {
            j$(".150_7_20").attr("disabled", true);
        }
        j$(".150_7_20").attr("disabled", !j$("#150_7_10_99").attr('checked') || j$("#150_7_10_99").attr('disabled'));
    });
    // リスク回避器具：有り：その他
    j$("#150_7_10_99").click(function(){
        j$(".150_7_20").attr("disabled", !this.checked || this.disabled);
    });

    // 身体拘束行為
    j$(".150_8").click(function() {
        j$(".150_8_10").attr("disabled", this.value != 10);
        if (this.value != 10) {
            j$(".150_8_20").attr("disabled", true);
        }
        j$(".150_8_20").attr("disabled", !j$("#150_8_10_99").attr('checked') || j$("#150_8_10_99").attr('disabled'));
    });
    // 身体拘束行為：有り：その他
    j$("#150_8_10_99").click(function(){
        j$(".150_8_20").attr("disabled", !this.checked || this.disabled);
    });

    // ミトン
    j$(".150_20").click(function() {
        j$(".150_20_10").attr("disabled", this.value != 20);
    });
});

//====================================================================================================
//セレクトボックス変更時の動作
//====================================================================================================
//これとは別にrelate_detailが呼ばれるケースがある。

function selectbox_onchange(this_obj) {
    var obj_id = this_obj.id;
    if(! obj_id)
    {
        return;
    }

    var obj_id_info_arr = obj_id.split("_");//0:固定文字｢id｣,1:グループコード,2:項目コード
    if(obj_id_info_arr.length != 3)
    {
        return;
    }

    var grp_code = obj_id_info_arr[1];
    var easy_item_code = obj_id_info_arr[2];

    //その他の内容の場合
    if(grp_code == "605" && easy_item_code == "15") area_605_115_disp_change();
    if(grp_code == "605" && easy_item_code == "16") area_605_114_disp_change();
    if(grp_code == "605" && easy_item_code == "17") area_605_116_disp_change();

    //影響区分の場合
    if(grp_code == "570" && easy_item_code == "5")
    {
        if(this_obj.value != "")
        {
            field_570_580_relate();
            area_570_disp_change();
            area_580_disp_change();
        }
        else
        {
            area_570_disp_change();
        }
    }
    //対応区分の場合
    if(grp_code == "580" && easy_item_code == "5")
    {
        if(this_obj.value != "")
        {
            field_570_580_relate();
            area_570_disp_change();
            area_580_disp_change();
        }
        else
        {
            area_580_disp_change();
        }
    }

    // 患者への影響（カスタマイズ用）
    if(grp_code == "1400" && easy_item_code == "10") {
        if(this_obj.value != "") {
            // 下位項目表示
            area_1400_change(this_obj);
            // 患者への対応と連動
            field_1400_1410_relate();
        }
        else {
            area_1400_change(this_obj);
        }
    }

    // 患者への対応（カスタマイズ用）
    if(grp_code == 1410 && easy_item_code == 10) {
        if(this_obj.value != "") {
            // 下位項目を表示
            area_1410_change(this_obj);
            // 患者への影響と連動
            field_1400_1410_relate();
        } else {
            area_1410_change(this_obj);
        }
    }

    // 発生場所詳細の場合
    if(grp_code == "110" && easy_item_code == "60") {
        area_110_65_disp_change(this_obj);
    }

        // 発見時刻の場合
    if (grp_code == "105" && easy_item_code == "60") {
        var value = parseInt(this_obj.value);
        var obj_time = document.getElementById("_105_40");

        switch (value) {
            case 0: case 1:
                obj_time.value = '1';
        break;
            case 2: case 3:
                obj_time.value = '2';
        break;
            case 4: case 5:
                obj_time.value = '3';
        break;
            case 6: case 7:
                obj_time.value = '4';
        break;
            case 8: case 9:
                obj_time.value = '5';
        break;
            case 10: case 11:
                obj_time.value = '6';
        break;
            case 12: case 13:
                obj_time.value = '7';
        break;
            case 14: case 15:
                obj_time.value = '8';
        break;
            case 16: case 17:
                obj_time.value = '9';
        break;
            case 18: case 19:
                obj_time.value = '10';
        break;
            case 20: case 21:
                obj_time.value = '11';
        break;
            case 22: case 23:
                obj_time.value = '12';
        break;
        }
    }

    // 発生時刻の場合
    if (grp_code == "100" && easy_item_code == "60") {
        var value = parseInt(this_obj.value);
        var obj_time = document.getElementById("_100_40");

        switch (value) {
            case 0: case 1:
                obj_time.value = '1';
        break;
            case 2: case 3:
                obj_time.value = '2';
        break;
            case 4: case 5:
                obj_time.value = '3';
        break;
            case 6: case 7:
                obj_time.value = '4';
        break;
            case 8: case 9:
                obj_time.value = '5';
        break;
            case 10: case 11:
                obj_time.value = '6';
        break;
            case 12: case 13:
                obj_time.value = '7';
        break;
            case 14: case 15:
                obj_time.value = '8';
        break;
            case 16: case 17:
                obj_time.value = '9';
        break;
            case 18: case 19:
                obj_time.value = '10';
        break;
            case 20: case 21:
                obj_time.value = '11';
        break;
            case 22: case 23:
                obj_time.value = '12';
        break;
        }
    }
}

function area_1400_change(this_obj) {
    var n = document.getElementsByName("_1400_30[]");

    for ( i = 0; n.length > i; i++ ) {
        n[i].checked = false;
    }

    var n = document.getElementsByName("_1400_60[]");

    for ( i = 0; n.length > i; i++ ) {
        n[i].checked = false;
    }

    document.getElementById("id_1400_20").value = "";
    document.getElementById("id_1400_40").value = "";
    document.getElementById("id_1400_50").value = "";
    document.getElementById("id_1400_70").value = "";

    if(this_obj.value != "") {
        if(id_1400_20_ic_obj = document.getElementById("id_1400_20_ic")) {
            if(this_obj.value == 3) {
                id_1400_20_ic_obj.style.display = "block";
            } else {
                id_1400_20_ic_obj.style.display = "none";
            }
        }

        if(id_1400_40_ic_obj = document.getElementById("id_1400_40_ic")) {
            if(this_obj.value == 2) {
                id_1400_40_ic_obj.style.display = "block";
            } else {
                id_1400_40_ic_obj.style.display = "none";
            }
        }

        if(id_1400_50_ic_obj = document.getElementById("id_1400_50_ic")) {
            if(this_obj.value != "") {
                id_1400_50_ic_obj.style.display = "block";
            } else {
                id_1400_50_ic_obj.style.display = "none";
            }
        }

        for (var idx = 0; idx < document.FRM_MAIN._1400_10.options.length; idx++) {
            var i = document.FRM_MAIN._1400_10.options[idx].value;
            if(this_obj.value == i) {
                if(id_1400_30_ic_obj = document.getElementById("id_1400_30_ic_"+i)) {
                    id_1400_30_ic_obj.style.display = "block";
                }

                if(id_1400_60_ic_obj = document.getElementById("id_1400_60_ic_"+i)) {
                    if(document.FRM_MAIN.elements['_1400_60[]'].length != 0 && document.getElementById("id_1400_60_"+i+"_0").value != "") {
                        id_1400_60_ic_obj.style.display = "block";
                        document.getElementById("id_1400_70_ic").style.display = "block";
                    } else {
                        document.getElementById("id_1400_70_ic").style.display = "none";

                    }
                }
                else {
                    document.getElementById("id_1400_70_ic").style.display = "none";
                }
            } else {
                if(id_1400_30_ic_obj = document.getElementById("id_1400_30_ic_"+i)) {
                    id_1400_30_ic_obj.style.display = "none";
                }
                if(id_1400_60_ic_obj = document.getElementById("id_1400_60_ic_"+i)) {
                    id_1400_60_ic_obj.style.display = "none";
                }
            }
        }
    }

    else {
        if ( id_1400_20_ic_obj = document.getElementById("id_1400_20_ic") ) {
            id_1400_20_ic_obj.style.display = "none";
        }

        if ( id_1400_40_ic_obj = document.getElementById("id_1400_40_ic") ) {
            id_1400_40_ic_obj.style.display = "none";
        }

        if ( id_1400_50_ic_obj = document.getElementById("id_1400_50_ic") ) {
            id_1400_50_ic_obj.style.display = "none";
        }

        if ( id_1400_70_ic_obj = document.getElementById("id_1400_70_ic") ) {
            id_1400_70_ic_obj.style.display = "none";
        }

        for (var idx = 0; idx < document.FRM_MAIN._1400_10.options.length; idx++) {
            var l = document.FRM_MAIN._1400_10.options.length;
            var i = document.FRM_MAIN._1400_10.options[idx].value;
            if ( id_1400_30_ic_obj = document.getElementById("id_1400_30_ic_"+i) ) {
                id_1400_30_ic_obj.style.display = "none";
            }
            if ( id_1400_60_ic_obj = document.getElementById("id_1400_60_ic_"+i) ) {
                id_1400_60_ic_obj.style.display = "none";
            }
        }
    }
}
function area_1410_change(this_obj){
    var n = document.getElementsByName("_1410_20[]");
    for(i=0; n.length>i; i++) {
        n[i].checked = false;
    }

    var n = document.getElementsByName("_1410_30[]");
    for(i=0; n.length>i; i++) {
        n[i].checked = false;
    }

    document.getElementById("id_1410_40").value = "";

    var n = document.getElementsByName("_1410_20[]");
    for(i=0; n.length>i; i++) {
        if (nn = document.getElementById("id_1410_30_ic_"+n[i].value)) {
            nn.style.display = "none";
        }
    }

    if ( this_obj.value != "" ) {
        if(id_1410_40_ic_obj = document.getElementById("id_1410_40_ic")) {
            if(this_obj.value != "") {
                id_1410_40_ic_obj.style.display = "block";
            } else {
                id_1410_40_ic_obj.style.display = "none";
            }
        }

        for (var idx = 0; idx < document.FRM_MAIN._1410_10.options.length; idx++) {
            var i = document.FRM_MAIN._1410_10.options[idx].value;
            if(this_obj.value == i) {
                if(id_1410_20_ic_obj = document.getElementById("id_1410_20_ic_"+i)) {
                    id_1410_20_ic_obj.style.display = "block";
                }

            } else {
                if(id_1410_20_ic_obj = document.getElementById("id_1410_20_ic_"+i)) {
                    id_1410_20_ic_obj.style.display = "none";
                }
            }
        }

    }
    else {
        if( id_1410_40_ic_obj = document.getElementById("id_1410_40_ic") ) {
            id_1410_40_ic_obj.style.display = "none";
        }

        for ( var idx = 0; idx < document.FRM_MAIN._1410_10.options.length; idx++ ) {
            var i = document.FRM_MAIN._1410_10.options[idx].value;
            if ( id_1410_20_ic_obj = document.getElementById("id_1410_20_ic_"+i) ) {
                id_1410_20_ic_obj.style.display = "none";
            }
        }
    }
}

function area_1400_disp_change() {
        <{if in_array(1400, $grp_flags)}>
            // 連動元オブジェクト
        var selectbox_1400_10_id = "id_1400_10";
        var selectbox_1400_10_obj = document.getElementById(selectbox_1400_10_id);

        if(selectbox_1400_10_obj) {
            if(id_1400_20_ic_obj = document.getElementById("id_1400_20_ic")) {
                if(selectbox_1400_10_obj.value == 3) {
                    id_1400_20_ic_obj.style.display = "block";
                } else {
                    id_1400_20_ic_obj.style.display = "none";
                }
            }
            if(id_1400_40_ic_obj = document.getElementById("id_1400_40_ic")) {
                if(selectbox_1400_10_obj.value == 2) {
                    id_1400_40_ic_obj.style.display = "block";
                } else {
                    id_1400_40_ic_obj.style.display = "none";
                }
            }

            if(id_1400_50_ic_obj = document.getElementById("id_1400_50_ic")) {
                if(selectbox_1400_10_obj.value != "") {
                    id_1400_50_ic_obj.style.display = "block";
                } else {
                    id_1400_50_ic_obj.style.display = "none";
                }
            }


            // リストで選択されているIDのチェックボックスを表示する
            // 例）区分で薬剤が選択されていたら、表示する詳細項目も薬剤の詳細項目になる
            var id = selectbox_1400_10_obj.value;

            if(id_1400_30_ic_obj = document.getElementById("id_1400_30_ic_"+id)) {
                id_1400_30_ic_obj.style.display = "block";
            }
            if(id_1400_60_ic_obj = document.getElementById("id_1400_60_ic_"+id)) {
                if(document.FRM_MAIN.elements['_1400_60[]'].length != 0 && document.getElementById("id_1400_60_"+id+"_0").value != "") {
                    id_1400_60_ic_obj.style.display = "block";
                    document.getElementById("id_1400_70_ic").style.display = "block";
                } else {
                    document.getElementById("id_1400_70_ic").style.display = "none";
                }
            }
        }
        <{/if}>
    }

function area_1410_disp_change() {
        <{if in_array(1410, $grp_flags)}>
            // 連動元オブジェクト
        var selectbox_1410_10_id = "id_1410_10";
        var selectbox_1410_10_obj = document.getElementById(selectbox_1410_10_id);

        if(selectbox_1410_10_obj) {
            if(id_1410_40_ic_obj = document.getElementById("id_1410_40_ic")) {
                if(selectbox_1410_10_obj.value != "") {
                    id_1410_40_ic_obj.style.display = "block";
                } else {
                    id_1410_40_ic_obj.style.display = "none";
                }
            }

            // リストで選択されているIDのチェックボックスを表示する
            // 例）区分で薬剤が選択されていたら、表示する詳細項目も薬剤の詳細項目になる
            var id = selectbox_1410_10_obj.value;

            if(id_1410_20_ic_obj = document.getElementById("id_1410_20_ic_"+id)) {
                id_1410_20_ic_obj.style.display = "block";
            }
        }
        <{/if}>
    }

function area_1410_20_disp_change() {
        <{if in_array(1410, $grp_flags)}>
            var n = document.getElementsByName("_1410_20[]");
        for(i=0; n.length>i; i++) {
            if(n[i].value && n[i].checked == true) {
                if(id_1410_30_ic_obj = document.getElementById("id_1410_30_ic_"+n[i].value)) {
                    id_1410_30_ic_obj.style.display = "block";
                }
            } else if(n[i].value){
                if(id_1410_30_ic_obj = document.getElementById("id_1410_30_ic_"+n[i].value)) {
                    id_1410_30_ic_obj.style.display = "none";

                    /*var nn = document.getElementsByName("_1410_30[]");
                    for(j=0; nn.length>j; j++) {
                        nn[j].checked = false;
                    }*/
                }
            }
        }
        <{/if}>
    }



function area_570_disp_change()
{
        <{if in_array(570, $grp_flags)}>
            //連動元オブジェクト
        var selectbox_570_5_id = "id_570_5";
        var selectbox_570_5_obj = document.getElementById(selectbox_570_5_id);

        if(selectbox_570_5_obj)
        {
            //まず、全ての連動先項目を非表示
            for(var i = 1; i <= 4; i++)
            {
                //連動先オブジェクト
                var area_570_id = "area_570_" + i;
                var area_570_obj = document.getElementById(area_570_id);

                //連動元に対応した連動先の場合
                if(selectbox_570_5_obj.value == i)
                {
                    if(area_570_obj)
                    {
                        //表示
                        area_570_obj.style.display = "block";
                    }
                }

                //連動元に対応していない連動先の場合
                else
                {
                    if(area_570_obj)
                    {
                        //表示されている場合は内容を破棄
                        if(area_570_obj.style.display == "block")
                        {
                            switch (i)
                            {
                                case 1:
                                    field_to_empty("_570_11[]","checkbox");
                                    field_to_empty("_570_12","text");
                                    break;
                                case 2:
                                    field_to_empty("_570_21[]","checkbox");
                                    field_to_empty("_570_22","text");
                                    field_to_empty("_570_23","text");
                                    break;
                                case 3:
                                    field_to_empty("_570_30","text");
                                    field_to_empty("_570_31[]","checkbox");
                                    field_to_empty("_570_32","text");
                                    break;
                                case 4:
                                    field_to_empty("_570_41[]","checkbox");
                                    field_to_empty("_570_42","text");
                                    field_to_empty("_570_43[]","checkbox");
                                    field_to_empty("_570_44","text");
                                    break;
                            }
                        }

                        //非表示
                        area_570_obj.style.display = "none";
                    }
                }

            }
        }

        <{/if}>
    }






function area_580_disp_change()
{
        <{if in_array(580, $grp_flags)}>
            //連動元オブジェクト
        var selectbox_580_5_id = "id_580_5";
        var selectbox_580_5_obj = document.getElementById(selectbox_580_5_id);

        if(selectbox_580_5_obj)
        {
            //まず、全ての連動先項目を非表示
            for(var i = 1; i <= 4; i++)
            {
                //連動先オブジェクト
                var area_580_id = "area_580_" + i;
                var area_580_obj = document.getElementById(area_580_id);

                //連動元に対応した連動先の場合
                if(selectbox_580_5_obj.value == i)
                {
                    if(area_580_obj)
                    {
                        //表示
                        area_580_obj.style.display = "block";
                    }
                }

                //連動元に対応していない連動先の場合
                else
                {
                    if(area_580_obj)
                    {
                        //表示されている場合は内容を破棄
                        if(area_580_obj.style.display == "block")
                        {
                            switch (i)
                            {
                                case 1:
                                    field_to_empty("_580_11[]","checkbox");
                                    field_to_empty("_580_12[]","checkbox");
                                    field_to_empty("_580_13","text");
                                    break;
                                case 2:
                                    field_to_empty("_580_21[]","checkbox");
                                    field_to_empty("_580_22[]","checkbox");
                                    field_to_empty("_580_23","text");
                                    break;
                                case 3:
                                    field_to_empty("_580_31[]","checkbox");
                                    field_to_empty("_580_32[]","checkbox");
                                    field_to_empty("_580_33","text");
                                    break;
                                case 4:
                                    field_to_empty("_580_41[]","checkbox");
                                    break;
                            }
                        }

                        //非表示
                        area_580_obj.style.display = "none";
                    }
                }
            }

        }

        <{/if}>
    }


function field_to_empty(obj_name,type)
{
    var c_objs = document.getElementsByName(obj_name);
    if(c_objs)
    {
        for(var i = 0; i < c_objs.length; i++)
        {
            switch(type)
            {
                case "text":
                    c_objs[i].value = "";
                    break;
                case "checkbox":
                    c_objs[i].checked = false;
                    break;
            }
        }
    }
}


function field_570_580_relate()
{
        <{if in_array(570, $grp_flags) && in_array(580, $grp_flags)}>
            //連動オブジェクト
        var selectbox_570_5_id = "id_570_5";
        var selectbox_570_5_obj = document.getElementById(selectbox_570_5_id);
        var selectbox_580_5_id = "id_580_5";
        var selectbox_580_5_obj = document.getElementById(selectbox_580_5_id);

        if(selectbox_570_5_obj && selectbox_580_5_obj)
        {
            //どちらか片方だけ空欄の場合、両方とも同じ値に設定する。

            if(selectbox_570_5_obj.value == "" && selectbox_580_5_obj.value != "")
            {
                set_select_ctl_from_value(selectbox_570_5_obj,selectbox_580_5_obj.value);
            }

            if(selectbox_570_5_obj.value != "" && selectbox_580_5_obj.value == "")
            {
                set_select_ctl_from_value(selectbox_580_5_obj,selectbox_570_5_obj.value);
            }
        }

        <{/if}>
    }
function field_1400_1410_relate() {
    <{if in_array(1400, $grp_flags) && in_array(1410, $grp_flags)}>
    //連動オブジェクト
    var selectbox_1400_10_id = "id_1400_10";
    var selectbox_1400_10_obj = document.getElementById(selectbox_1400_10_id);
    var selectbox_1410_10_id = "id_1410_10";
    var selectbox_1410_10_obj = document.getElementById(selectbox_1410_10_id);

    if ( selectbox_1400_10_obj && selectbox_1410_10_obj ) {
        //どちらか片方だけ空欄の場合、両方とも同じ値に設定する。
        if ( selectbox_1400_10_obj.value == "" && selectbox_1410_10_obj.value != "" ) {
            set_select_ctl_from_value(selectbox_1400_10_obj,selectbox_1410_10_obj.value);
            area_1400_change(document.getElementById("id_1400_10"));
        }

        if ( selectbox_1400_10_obj.value != "" && selectbox_1410_10_obj.value == "" ) {
            set_select_ctl_from_value(selectbox_1410_10_obj,selectbox_1400_10_obj.value);
            area_1410_change(document.getElementById("id_1410_10"));
        }
    }
    <{/if}>
}

//====================================================================================================
//共通
//====================================================================================================
//プルダウンメニューを指定された値で選択します。
//引数
//  obj:selectオブジェクト
//  val:設定値
//戻値
//  設定に成功した場合にTrueを、それ以外の場合はFalseを返します。
function set_select_ctl_from_value(obj,val) {
    var is_ok = false;
    for(var i=0; i<obj.options.length; i++)
    {
        if(obj.options[i].value == val)
        {
            obj.selectedIndex = i;
            is_ok = true;
            break;
        }
    }
    return is_ok;
}

//====================================================================================================
//連動項目１　変化項目
//====================================================================================================


<{if in_array(400, $grp_flags)}>
var rel_details = new Array;
var rel_code_map = new Array;

rel_details[400] =
<{if in_array(10, $eis_400_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=400 j=10 tr=true default=true}> <{/if}>
<{if in_array(20, $eis_400_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=400 j=20 tr=true default=true}> <{/if}>
<{if in_array(30, $eis_400_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=400 j=30 tr=true default=true}> <{/if}>
"";

rel_details[410] =
<{if in_array(10, $eis_410_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=410 j=10 tr=true default=true}> <{/if}>
<{if in_array(15, $eis_410_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=410 j=15 tr=true default=true}> <{/if}>
<{if in_array(20, $eis_410_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=410 j=20 tr=true default=true}> <{/if}>
<{if in_array(25, $eis_410_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=410 j=25 tr=true default=true}> <{/if}>
<{if in_array(30, $eis_410_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=410 j=30 tr=true default=true}> <{/if}>
<{if in_array(40, $eis_410_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=410 j=40 tr=true default=true}> <{/if}>
<{if in_array(50, $eis_410_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=410 j=50 tr=true default=true}> <{/if}>
<{if in_array(51, $eis_410_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=410 j=51 tr=true default=true}> <{/if}>
<{if in_array(52, $eis_410_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=410 j=52 tr=true default=true}> <{/if}>
<{if in_array(53, $eis_410_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=410 j=53 tr=true default=true}> <{/if}>
<{if in_array(60, $eis_410_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=410 j=60 tr=true default=true}> <{/if}>
<{if in_array(61, $eis_410_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=410 j=61 tr=true default=true}> <{/if}>
<{if in_array(62, $eis_410_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=410 j=62 tr=true default=true}> <{/if}>
<{if in_array(63, $eis_410_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=410 j=63 tr=true default=true}> <{/if}>
"";

rel_details[420] =
<{if in_array(10, $eis_420_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=420 j=10 tr=true default=true}> <{/if}>
<{if in_array(20, $eis_420_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=420 j=20 tr=true default=true}> <{/if}>
<{if in_array(30, $eis_420_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=420 j=30 tr=true default=true}> <{/if}>
"";

rel_details[430] =
<{if in_array(10, $eis_430_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=430 j=10 tr=true default=true}> <{/if}>
<{if in_array(20, $eis_430_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=430 j=20 tr=true default=true}> <{/if}>
<{if in_array(30, $eis_430_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=430 j=30 tr=true default=true}> <{/if}>
<{if in_array(40, $eis_430_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=430 j=40 tr=true default=true}> <{/if}>
<{if in_array(41, $eis_430_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=430 j=41 tr=true default=true}> <{/if}>
<{if in_array(42, $eis_430_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=430 j=42 tr=true default=true}> <{/if}>
"";

rel_details[440] =
<{if in_array(10, $eis_440_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=440 j=10 tr=true default=true}> <{/if}>
<{if in_array(20, $eis_440_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=440 j=20 tr=true default=true}> <{/if}>
<{if in_array(30, $eis_440_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=440 j=30 tr=true default=true}> <{/if}>
<{if in_array(40, $eis_440_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=440 j=40 tr=true default=true}> <{/if}>
<{if in_array(41, $eis_440_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=440 j=41 tr=true default=true}> <{/if}>
<{if in_array(42, $eis_440_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=440 j=42 tr=true default=true}> <{/if}>
<{if in_array(43, $eis_440_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=440 j=43 tr=true default=true}> <{/if}>
<{if in_array(44, $eis_440_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=440 j=44 tr=true default=true}> <{/if}>
"";

rel_details[450] =
<{if in_array(10, $eis_450_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=450 j=10 tr=true default=true}> <{/if}>
<{if in_array(20, $eis_450_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=450 j=20 tr=true default=true}> <{/if}>
<{if in_array(30, $eis_450_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=450 j=30 tr=true default=true}> <{/if}>
<{if in_array(40, $eis_450_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=450 j=40 tr=true default=true}> <{/if}>
<{if in_array(41, $eis_450_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=450 j=41 tr=true default=true}> <{/if}>
<{if in_array(42, $eis_450_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=450 j=42 tr=true default=true}> <{/if}>
"";

rel_details[460] =
<{if in_array(10, $eis_460_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=460 j=10 tr=true default=true}> <{/if}>
<{if in_array(20, $eis_460_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=460 j=20 tr=true default=true}> <{/if}>
<{if in_array(30, $eis_460_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=460 j=30 tr=true default=true}> <{/if}>
<{if in_array(40, $eis_460_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=460 j=40 tr=true default=true}> <{/if}>
<{if in_array(41, $eis_460_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=460 j=41 tr=true default=true}> <{/if}>
<{if in_array(42, $eis_460_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=460 j=42 tr=true default=true}> <{/if}>
<{if in_array(43, $eis_460_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=460 j=43 tr=true default=true}> <{/if}>
<{if in_array(44, $eis_460_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=460 j=44 tr=true default=true}> <{/if}>
<{if in_array(50, $eis_460_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=460 j=50 tr=true default=true}> <{/if}>
<{if in_array(51, $eis_460_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=460 j=51 tr=true default=true}> <{/if}>
<{if in_array(52, $eis_460_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=460 j=52 tr=true default=true}> <{/if}>
"";

rel_details[470] =
<{if in_array(10, $eis_470_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=470 j=10 tr=true default=true}> <{/if}>
<{if in_array(20, $eis_470_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=470 j=20 tr=true default=true}> <{/if}>
<{if in_array(30, $eis_470_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=470 j=30 tr=true default=true}> <{/if}>
<{if in_array(40, $eis_470_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=470 j=40 tr=true default=true}> <{/if}>
<{if in_array(41, $eis_470_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=470 j=41 tr=true default=true}> <{/if}>
<{if in_array(42, $eis_470_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=470 j=42 tr=true default=true}> <{/if}>
<{if in_array(43, $eis_470_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=470 j=43 tr=true default=true}> <{/if}>
<{if in_array(44, $eis_470_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=470 j=44 tr=true default=true}> <{/if}>
<{if in_array(50, $eis_470_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=470 j=50 tr=true default=true}> <{/if}>
<{if in_array(51, $eis_470_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=470 j=51 tr=true default=true}> <{/if}>
<{if in_array(52, $eis_470_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=470 j=52 tr=true default=true}> <{/if}>
""

rel_details[480] =
<{if in_array(10, $eis_480_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=480 j=10 tr=true default=true}> <{/if}>
<{if in_array(20, $eis_480_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=480 j=20 tr=true default=true}> <{/if}>
<{if in_array(30, $eis_480_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=480 j=30 tr=true default=true}> <{/if}>
<{if in_array(40, $eis_480_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=480 j=40 tr=true default=true}> <{/if}>
<{if in_array(41, $eis_480_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=480 j=41 tr=true default=true}> <{/if}>
<{if in_array(42, $eis_480_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=480 j=42 tr=true default=true}> <{/if}>
""

rel_details[490] =
<{if in_array(10, $eis_490_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=490 j=10 tr=true default=true}> <{/if}>
<{if in_array(20, $eis_490_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=490 j=20 tr=true default=true}> <{/if}>
<{if in_array(30, $eis_490_use)}> <{include file="hiyari_edit_easyinput_item_string.tpl" i=490 j=30 tr=true default=true}> <{/if}>
"";

<{section name=r loop=$rel_grps[120][80]}>
    rel_code_map[ '<{$rel_grps[120][80][r].easy_code}>' ] = <{$rel_grps[120][80][r].rel_grp_code}>;
<{/section}>
function relate_detail(myself,rel_id){

    //選択されている連動元コードを特定
    var mycode;
    if ( myself.type == "radio" ){
        mycode = myself.value;
    }
    else if ( myself.type == "select-one" ){
        mycode = myself.options[ myself.selectedIndex ].value;
    }

    //連動結果をHTMLを取得
    var chgHtml = "";
    if ( rel_details[ rel_code_map[mycode] ] ){
        chgHtml = rel_details[ rel_code_map[mycode] ];
    }

    //連動先オブジェクトにHTMLを設定
    var oRel = getRelateObj(rel_id);
    chgHtml = '<table width="100%" cellspacing="0" cellpadding="2" class="list" style="position:relative;top:-1px;">\n' + chgHtml + '</table>\n';
    if ( oRel ) oRel.innerHTML = chgHtml;

}

function getRelateObj(id){
    var oRel;
    if(document.all) oRel = document.all(id);
    else if(document.getElementById) oRel = document.getElementById(id);

    return oRel;
}

<{/if}>

<{*
//===================================================================================================
//4000 事例の詳細(時系列) jsパート
//====================================================================================================
*}>
<{if array_intersect(array(4000), $grp_flags)}>
// 事例の詳細(時系列)のテーブル行「追加」
function addTable_jirei()
{
    var table = document.getElementById("tbody_4000");

    if (table.rows.length >= 50) {
        return;
    }

    var row = table.insertRow(-1);
    var cell01 = row.insertCell(-1);
    var cell02 = row.insertCell(-1);
    var cell03 = row.insertCell(-1);
    var cell04 = row.insertCell(-1);

    var bef = 3998 + table.rows.length; // 4000 + length - 2
    var num = 3999 + table.rows.length; // 4000 + length - 1

    cell01.innerHTML = '<font size="3" class="j12">' + table.rows.length + '</font>';
    cell02.innerHTML = tr_4000_1.replace(/4000/g, num);
    cell03.innerHTML = tr_4000_4.replace(/4000/g, num);
    cell04.innerHTML = tr_4000_5.replace(/4000/g, num);

    document.getElementById('id_'+num+'_1').value = document.getElementById('id_'+bef+'_1').value;
}

// 事例の詳細(時系列)のテーブル行「削除」
function deleteTable_jirei()
{
    var table = document.getElementById("tbody_4000");

    if (table.rows.length <= 1) {
        return;
    }

    table.deleteRow(-1);
}

var tr_4000_1 = '<font size="3" class="j12"><input type="text" id="id_4000_1" name="_4000_1" value="" style="width:80px;border:#35B341 solid 2px;" readonly> <img id="caller_4000_1" src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer;" onclick="window.open(\'hiyari_calendar.php?session=<{$session}>&caller=4000_1\',\'newwin\',\'width=640,height=480,scrollbars=yes\');"/> <button type="button" onclick="document.getElementById(\'id_4000_1\').value = \'\';">クリア</button> <select id="id_4000_2" name="_4000_2" onchange="selectbox_onchange(this);">    <option value=""></option><{section name=k2 loop=$grps[4000].easy_item_list[2].easy_list}><{assign var="easy_4000_2" value=$grps[4000].easy_item_list[2].easy_list[k2]}><{if $item_element_no_disp[4000][2] == "" || !in_array($easy_4000_2.easy_code, $item_element_no_disp[4000][2])}><option value="<{$easy_4000_2.easy_code}>"><{$easy_4000_2.easy_name|escape:"javascript"}></option><{/if}><{/section}></select>時 <select id="id_4000_3" name="_4000_3" onchange="selectbox_onchange(this);">    <option value=""></option><{section name=k3 loop=$grps[4000].easy_item_list[3].easy_list}><{assign var="easy_4000_3" value=$grps[4000].easy_item_list[3].easy_list[k3]}><{if $item_element_no_disp[4000][3] == "" || !in_array($easy_4000_3.easy_code, $item_element_no_disp[4000][3])}><option value="<{$easy_4000_3.easy_code}>"><{$easy_4000_3.easy_name|escape:"javascript"}></option><{/if}><{/section}></select>分</font>';
var tr_4000_4 = '<select id="id_4000_4" name="_4000_4" onchange="selectbox_onchange(this);"><option value=""></option><{section name=k4 loop=$grps[4000].easy_item_list[4].easy_list}><{assign var="easy_4000_4" value=$grps[4000].easy_item_list[4].easy_list[k4]}><{if $item_element_no_disp[4000][4] == "" || !in_array($easy_4000_4.easy_code, $item_element_no_disp[4000][4])}><option value="<{$easy_4000_4.easy_code}>"><{$easy_4000_4.easy_name|escape:"javascript"}></option><{/if}><{/section}></select><input type="text" name="_4000_6" style="ime-mode:active;"/>';
var tr_4000_5 = '<textarea style="width:100%;height:50px;ime-mode:active;" rows="2" name="_4000_5"></textarea>';

<{/if}>

//====================================================================================================
//連動項目２　追加項目
//====================================================================================================
<{if array_intersect(array(3030, 3032, 3034, 3036, 3038, 3050, 3100, 3150, 3200, 3250, 3300, 3350, 3400, 3450), $grp_flags)}>

    <{section name=idx loop=1}>
    <{assign var="idx" value=$smarty.section.idx.index}>

        var parties_inputs =         <{if in_array(3050, $grp_flags)}>
            <{include file="hiyari_edit_easyinput_item_string.tpl" i=305$idx j=30 tr=true colspan=$party_cols-1}>
            <{include file="hiyari_edit_easyinput_item_string.tpl" i=305$idx j=35 tr=true colspan=$party_cols-1 ime="active"}>
        <{/if}>

        <{if in_array(3100, $grp_flags)}>
            '<tr height="22">\n'+
            <{include file="hiyari_edit_easyinput_item_string.tpl" i=310$idx j=40 tr=false}>

            <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
                '<td width="25%" rowspan="<{$profile_rowspan}>">\n'+

                <{if $self_button_flg == true }>
                    '<input type="button" value="本人ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="copyFromSelfProfile(_party_index);" style="width:200px;margin-bottom:2px;"><br />\n'+
                <{/if}>

                <{if $other_user_button_flg == true }>
                    '<input type="button" value="他の職員ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="other_profile_copy(_party_index);" style="width:200px;"><br />\n'+
                <{/if}>

                <{if $unknown_button_flg == true}>
                    '<input type="button" value="ﾌﾟﾛﾌｨｰﾙ不明" onclick="setUnknownProfile(_party_index);" style="width:200px;">\n'+
                <{/if}>

                '</td>\n'+
            <{/if}>

            '</tr>\n'+
        <{/if}>

        <{if in_array(3150, $grp_flags)}>
            '<tr height="22">\n'+
            <{include file="hiyari_edit_easyinput_item_string.tpl" i=315$idx j=50 tr=false}>
            <{if !in_array(3100, $grp_flags)}>

            <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
                '<td width="25%" rowspan="<{$profile_rowspan}>">\n'+

                <{if $self_button_flg == true }>
                    '<input type="button" value="本人ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="copyFromSelfProfile(_party_index);" style="width:200px;margin-bottom:2px;"><br />\n'+
                <{/if}>

                <{if $other_user_button_flg == true }>
                    '<input type="button" value="他の職員ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="other_profile_copy(_party_index);" style="width:200px;"><br />\n'+
                <{/if}>

                <{if $unknown_button_flg == true}>
                    '<input type="button" value="ﾌﾟﾛﾌｨｰﾙ不明" onclick="setUnknownProfile(_party_index);" style="width:200px;">\n'+
                <{/if}>

                '</td>\n'+
            <{/if}>

            <{/if}>
            '</tr>\n'+
        <{/if}>

        <{if in_array(3200, $grp_flags)}>
            '<tr height="22">\n'+
            <{include file="hiyari_edit_easyinput_item_string.tpl" i=320$idx j=60 tr=false}>
            <{if !in_array(3100, $grp_flags) && !in_array(3150, $grp_flags)}>

            <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
                '<td width="25%" rowspan="<{$profile_rowspan}>">\n'+

                <{if $self_button_flg == true }>
                    '<input type="button" value="本人ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="copyFromSelfProfile(_party_index);" style="width:200px;margin-bottom:2px;"><br />\n'+
                <{/if}>

                <{if $other_user_button_flg == true }>
                    '<input type="button" value="他の職員ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="other_profile_copy(_party_index);" style="width:200px;"><br />\n'+
                <{/if}>

                <{if $unknown_button_flg == true}>
                    '<input type="button" value="ﾌﾟﾛﾌｨｰﾙ不明" onclick="setUnknownProfile(_party_index);" style="width:200px;">\n'+
                <{/if}>

                '</td>\n'+
            <{/if}>

            <{/if}>
            '</tr>\n'+
        <{/if}>

        <{if in_array(3250, $grp_flags)}>
            '<tr height="22">\n'+
            <{include file="hiyari_edit_easyinput_item_string.tpl" i=325$idx j=70 tr=false}>
            <{if !in_array(3100, $grp_flags) && !in_array(3150, $grp_flags) && !in_array(3200, $grp_flags)}>

            <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
                '<td width="25%" rowspan="<{$profile_rowspan}>">\n'+

                <{if $self_button_flg == true }>
                    '<input type="button" value="本人ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="copyFromSelfProfile(_party_index);" style="width:200px;margin-bottom:2px;"><br />\n'+
                <{/if}>

                <{if $other_user_button_flg == true }>
                    '<input type="button" value="他の職員ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="other_profile_copy(_party_index);" style="width:200px;"><br />\n'+
                <{/if}>

                <{if $unknown_button_flg == true}>
                    '<input type="button" value="ﾌﾟﾛﾌｨｰﾙ不明" onclick="setUnknownProfile(_party_index);" style="width:200px;">\n'+
                <{/if}>

                '</td>\n'+
            <{/if}>

            <{/if}>
            '</tr>\n'+
            <{include file="hiyari_edit_easyinput_item_string.tpl" i=325$idx j=80 tr=true}>
        <{/if}>

        <{if in_array(3300, $grp_flags)}>
            '<tr height="22">\n'+
            <{include file="hiyari_edit_easyinput_item_string.tpl" i=330$idx j=90 tr=false}>
            <{if !in_array(3100, $grp_flags) && !in_array(3150, $grp_flags) && !in_array(3200, $grp_flags) && !in_array(3250, $grp_flags)}>

            <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
                '<td width="25%" rowspan="<{$profile_rowspan}>">\n'+

                <{if $self_button_flg == true }>
                    '<input type="button" value="本人ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="copyFromSelfProfile(_party_index);" style="width:200px;margin-bottom:2px;"><br />\n'+
                <{/if}>

                <{if $other_user_button_flg == true }>
                    '<input type="button" value="他の職員ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="other_profile_copy(_party_index);" style="width:200px;"><br />\n'+
                <{/if}>

                <{if $unknown_button_flg == true}>
                    '<input type="button" value="ﾌﾟﾛﾌｨｰﾙ不明" onclick="setUnknownProfile(_party_index);" style="width:200px;">\n'+
                <{/if}>

                '</td>\n'+
            <{/if}>

            <{/if}>
            '</tr>\n'+
            <{include file="hiyari_edit_easyinput_item_string.tpl" i=330$idx j=100 tr=true}>
        <{/if}>

        <{if in_array(3350, $grp_flags)}>
            <{include file="hiyari_edit_easyinput_item_string.tpl" i=335$idx j=110 tr=true colspan=$party_cols-1}>
            <{include file="hiyari_edit_easyinput_item_string.tpl" i=335$idx j=115 tr=true colspan=$party_cols-1 ime="active"}>
        <{/if}>

        <{if in_array(3400, $grp_flags)}>
            <{include file="hiyari_edit_easyinput_item_string.tpl" i=340$idx j=120 tr=true colspan=$party_cols-1}>
            <{include file="hiyari_edit_easyinput_item_string.tpl" i=340$idx j=130 tr=true colspan=$party_cols-1}>
        <{/if}>

        <{if in_array(3450, $grp_flags)}>
            <{include file="hiyari_edit_easyinput_item_string.tpl" i=345$idx j=140 tr=true colspan=$party_cols-1}>
        <{/if}>

        "";
    <{/section}>


    function getTBodyObj(id){
    var oTABLE,oTbodies,oTBODY;
    //if(document.all) oTABLE = document.all(id);
    //else if(document.getElementById)
    oTABLE = document.getElementById(id);

    if(oTABLE){
        oTbodies = oTABLE.tBodies;
        oTBODY = oTbodies.item(0);
    }

    return oTBODY;
}

function deleteTable(id){
    var oTBODY = getTBodyObj(id);
    if(oTBODY.rows.length < 2) return;
    var del_row = oTBODY.rows.length - 1;
    oTBODY.deleteRow(del_row);
}

function rewriteTable(id){
    var oTBODY = getTBodyObj(id);
    if(!oTBODY) return;
    if(oTBODY.rows.length >= 10) return;
    var oTR=oTBODY.insertRow(oTBODY.rows.length);
    var oTCell=oTR.insertCell(0);
    var num_row = oTBODY.rows.length;
    var num = num_row - 1;
    var html = parties_inputs.replace(/_(3[0-4][05])0_/g, "_" + "$1" + num + "_").replace(/_party_index/g, num);
    oTCell.innerHTML = '<table width="100%" cellspacing="0" cellpadding="2" class="list" >\n<tr height="22">\n<td bgcolor="#DFFFDC" colspan="<{$party_cols}>"><font class="j12">当事者' + num_row + '</font></td>\n</tr>\n' + html + '</table>\n';
}

// 発見者から発見者職種を取得する
function getDiscovererJob() {
    var obj = document.getElementById("id_3000_10");
    var obj2 = document.getElementById("id_3020_10");

    if(obj2) {
        if(obj.value == 1 || obj.value == 2) {

            for(i=0; i<obj2.options.length; i++) {
                if(obj2.options[i].value == '<{$profile.exp_code}>') {
                    obj2.options[i].selected = true;
                    break;
                }
            }
        }
    }
}
// 当事者の本人プロフィールコピー処理
function copyFromSelfProfile(party_index) {
    var emp_nm_box = document.FRM_MAIN.elements['_310' + party_index + '_40'];
    var class_nm_box = document.FRM_MAIN.elements['_315' + party_index + '_50'];
    var qualification_box = document.FRM_MAIN.elements['_320' + party_index + '_60'];
    var exp_year_box = document.FRM_MAIN.elements['_325' + party_index + '_70'];
    var exp_month_box = document.FRM_MAIN.elements['_325' + party_index + '_80'];
    var dept_year_box = document.FRM_MAIN.elements['_330' + party_index + '_90'];
    var dept_month_box = document.FRM_MAIN.elements['_330' + party_index + '_100'];
    var exp_code_box = document.FRM_MAIN.elements['_305' + party_index + '_30'];

    if (emp_nm_box) {
        emp_nm_box.value = '<{$profile.emp_nm}>';
    }
    if (class_nm_box) {
        class_nm_box.value = '<{$emp_class_nm}>';
    }
    if (qualification_box) {
        qualification_box.value = '<{$profile.qualification|regex_replace:"/\r?\n/":"<br>"|regex_replace:"/'/":"\'"}>'.replace(/<br>/g, '\n');
    }
    if (exp_year_box) {
        exp_year_box.value = '<{$experience.exp_year}>';
    }
    if (exp_month_box) {
        exp_month_box.value = '<{$experience.exp_month}>';
    }
    if (dept_year_box) {
        dept_year_box.value = '<{$experience.dept_year}>';
    }
    if (dept_month_box) {
        dept_month_box.value = '<{$experience.dept_month}>';
    }
    if (exp_code_box) {
        exp_code_box.value = '<{if !in_array($profile.exp_code, (array)@$item_element_no_disp[3050][30])}><{$profile.exp_code}><{/if}>';
    }
}

// 発見者の本人プロフィールコピー処理
function copyFromSelfProfile2() {
    var emp_nm_box = document.FRM_MAIN.elements['_3030_10'];
    var class_nm_box = document.FRM_MAIN.elements['_3032_10'];
    var qualification_box = document.FRM_MAIN.elements['_3034_10'];
    var exp_year_box = document.FRM_MAIN.elements['_3036_10'];
    var exp_month_box = document.FRM_MAIN.elements['_3036_20'];
    var dept_year_box = document.FRM_MAIN.elements['_3038_10'];
    var dept_month_box = document.FRM_MAIN.elements['_3038_20'];
    var exp_code_box        = document.FRM_MAIN.elements['_3020_10'];

    if (emp_nm_box) {
        emp_nm_box.value = '<{$profile.emp_nm}>';
    }
    if (class_nm_box) {
        class_nm_box.value = '<{$emp_class_nm}>';
    }
    if (qualification_box) {
        qualification_box.value = '<{$profile.qualification|regex_replace:"/\r?\n/":"<br>"|regex_replace:"/'/":"\'"}>'.replace(/<br>/g, '\n');
    }
    if (exp_year_box) {
        exp_year_box.value = '<{$experience.exp_year}>';
    }
    if (exp_month_box) {
        exp_month_box.value = '<{$experience.exp_month}>';
    }
    if (dept_year_box) {
        dept_year_box.value = '<{$experience.dept_year}>';
    }
    if (dept_month_box) {
        dept_month_box.value = '<{$experience.dept_month}>';
    }
    if (exp_code_box) {
        exp_code_box.value = '<{if !in_array($profile.exp_code, (array)@$item_element_no_disp[3020][10])}><{$profile.exp_code}><{/if}>';
    }
}

// 当事者のプロフィール不明コピー処理
function setUnknownProfile(party_index) {
    var exp_year_box = document.FRM_MAIN.elements['_325' + party_index + '_70'];
    var exp_month_box = document.FRM_MAIN.elements['_325' + party_index + '_80'];
    var dept_year_box = document.FRM_MAIN.elements['_330' + party_index + '_90'];
    var dept_month_box = document.FRM_MAIN.elements['_330' + party_index + '_100'];
    var exp_code_box = document.FRM_MAIN.elements['_305' + party_index + '_30'];

    if (exp_year_box) {
        exp_year_box.value = '900';     //当事者職種経験年数：年数不明
    }
    if (exp_month_box) {
        exp_month_box.value = '90';     //当事者職種経験年数：月数不明
    }
    if (dept_year_box) {
        dept_year_box.value = '900';    //当事者部署配置年数：年数不明
    }
    if (dept_month_box) {
        dept_month_box.value = '90';    //当事者部署配置年数：月数不明
    }
    if (exp_code_box) {
        //当事者職種：その他
        exp_code_box.value = '<{if !in_array(99, (array)@$item_element_no_disp[3050][30])}>99<{/if}>';
    }
}

// 発見者のプロフィール不明コピー処理
function setUnknownProfile2() {
    var exp_year_box = document.FRM_MAIN.elements['_3036_10'];
    var exp_month_box = document.FRM_MAIN.elements['_3036_20'];
    var dept_year_box = document.FRM_MAIN.elements['_3038_10'];
    var dept_month_box = document.FRM_MAIN.elements['_3038_20'];
    var exp_code_box    = document.FRM_MAIN.elements['_3020_10'];

    if (exp_year_box) {
        exp_year_box.value = '900';     //当事者職種経験年数：年数不明
    }
    if (exp_month_box) {
        exp_month_box.value = '90';     //当事者職種経験年数：月数不明
    }
    if (dept_year_box) {
        dept_year_box.value = '900';    //当事者部署配置年数：年数不明
    }
    if (dept_month_box) {
        dept_month_box.value = '90';    //当事者部署配置年数：月数不明
    }
    if (exp_code_box) {
        //当事者職種：その他
        exp_code_box.value = '<{if !in_array(99, (array)@$item_element_no_disp[3020][10])}>99<{/if}>';
    }
}

// 当事者の他職員プロフィールコピー処理
function copyFromChildWindowProfile(index, emp_nm, class_nm, qualification, exp_year, exp_month, dept_year, dept_month, exp_code) {
    var emp_nm_box = document.FRM_MAIN.elements['_310' + index + '_40'];
    var class_nm_box = document.FRM_MAIN.elements['_315' + index + '_50'];
    var qualification_box = document.FRM_MAIN.elements['_320' + index + '_60'];
    var exp_year_box = document.FRM_MAIN.elements['_325' + index + '_70'];
    var exp_month_box = document.FRM_MAIN.elements['_325' + index + '_80'];
    var dept_year_box = document.FRM_MAIN.elements['_330' + index + '_90'];
    var dept_month_box      = document.FRM_MAIN.elements['_330' + index + '_100'];
    var exp_code_box        = document.FRM_MAIN.elements['_305' + index + '_30'];

    if (emp_nm_box) {
        emp_nm_box.value = emp_nm;
    }
    if (class_nm_box) {
        class_nm_box.value = class_nm;
    }
    if (qualification_box) {
        qualification_box.value = qualification;
    }
    if (exp_year_box) {
        exp_year_box.value = exp_year;
    }
    if (exp_month_box) {
        exp_month_box.value = exp_month;
    }
    if (dept_year_box) {
        dept_year_box.value = dept_year;
    }
    if (dept_month_box) {
        dept_month_box.value = dept_month;
    }

    if (exp_code_box) {
        var no_disp_code_flg = false;
    <{if is_array(@$item_element_no_disp[3050][30])}>
        <{foreach from=$item_element_no_disp[3050][30] item=no_disp_code}>
        if( exp_code == <{$no_disp_code}>) {
            no_disp_code_flg = true;
        }
    <{/foreach}>
    <{/if}>
        exp_code_box.value = no_disp_code_flg ? '' : exp_code ;
    }
}

// 発見者の他職員プロフィールコピー処理
function copyFromChildWindowProfile2(emp_nm, class_nm, qualification, exp_year, exp_month, dept_year, dept_month, exp_code) {
    var emp_nm_box = document.FRM_MAIN.elements['_3030_10'];
    var class_nm_box = document.FRM_MAIN.elements['_3032_10'];
    var qualification_box = document.FRM_MAIN.elements['_3034_10'];
    var exp_year_box = document.FRM_MAIN.elements['_3036_10'];
    var exp_month_box = document.FRM_MAIN.elements['_3036_20'];
    var dept_year_box = document.FRM_MAIN.elements['_3038_10'];
    var dept_month_box = document.FRM_MAIN.elements['_3038_20'];
    var exp_code_box        = document.FRM_MAIN.elements['_3020_10'];

    if (emp_nm_box) {
        emp_nm_box.value = emp_nm;
    }
    if (class_nm_box) {
        class_nm_box.value = class_nm;
    }
    if (qualification_box) {
        qualification_box.value = qualification;
    }
    if (exp_year_box) {
        exp_year_box.value = exp_year;
    }
    if (exp_month_box) {
        exp_month_box.value = exp_month;
    }
    if (dept_year_box) {
        dept_year_box.value = dept_year;
    }
    if (dept_month_box) {
        dept_month_box.value = dept_month;
    }
    if (exp_code_box) {
        var no_disp_code_flg = false;
        <{if is_array(@$item_element_no_disp[3020][10])}>
        <{foreach from=$item_element_no_disp[3020][10] item=no_disp_code}>
        if( exp_code == <{$no_disp_code}>) {
            no_disp_code_flg = true;
        }
        <{/foreach}>
        <{/if}>
        exp_code_box.value = no_disp_code_flg ? '' : exp_code ;
    }
}

//他の人のプロフィールをコピーします。
function other_profile_copy(party_index) {
    call_emp_search(party_index);
}

function other_profile_copy2() {
    call_emp_search2();
}


//--------------------------------------------------
//職員名簿画面
//--------------------------------------------------
function call_emp_search(input_div) {
    var childwin = null;

    dx = screen.width;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = 'hiyari_emp_list.php';
    url += '?session=<{$session}>&call_back_mode=inci_profile&input_div=' + input_div;
    childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    childwin.focus();
}

function call_emp_search2() {
    var childwin = null;

    dx = screen.width;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = 'hiyari_emp_list.php';
    url += '?session=<{$session}>&call_back_mode=inci_profile&mode=2';
    childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    childwin.focus();
}

function add_emp_list(emp_id, emp_nm, input_div, other_info) {
    copyFromChildWindowProfile(input_div, emp_nm, other_info.class_nm, other_info.qualification, other_info.exp_year, other_info.exp_month, other_info.dept_year, other_info.dept_month, other_info.exp_code);
}

function add_emp_list2(emp_id, emp_nm, other_info) {
    copyFromChildWindowProfile2(emp_nm, other_info.class_nm, other_info.qualification, other_info.exp_year, other_info.exp_month, other_info.dept_year, other_info.dept_month, other_info.exp_code);
}
<{/if}>
//-->
</script>

<script type="text/javascript">
//患者検索を行います。
function patient_search()
{
    var id_objs = document.getElementsByName("_210_10");
    var pt_id = id_objs[0].value;

    var target_date = false;
    if(document.getElementById("_100_5")) {
        target_date = document.getElementById("_100_5").value;
    }
    if(target_date == "") {
        target_date = false;
    }

    if(pt_id == "")
    {
        //患者IDが未入力の場合は子画面検索を行う。
        window.open('hiyari_patient_search.php?session=<{$session}>&target_date='+target_date, 'newwin', 'width=640,height=480,scrollbars=yes');
    }
    else
    {
        //患者IDが入力されている場合はAjax患者検索を行う。
        patient_search_ajax(pt_id, target_date);
    }


}


//Ajax患者検索を開始します。
function patient_search_ajax(pt_id, target_date)
{

    var url = 'hiyari_patient_search_ajax.php';
    var params = $H({'session':'<{$session}>','pt_id':pt_id,'date':target_date}).toQueryString();
    var myAjax = new Ajax.Request(
        url,
        {
            method: 'post',
            postBody: params,
            onComplete: patient_search_ajax_response
        });
}
//Ajax患者検索結果に対する処理を行います。
function patient_search_ajax_response(oXMLHttpRequest)
{
    if(oXMLHttpRequest.status == 200)
    {
        //alert(oXMLHttpRequest.responseText);
        var response = new ajax_response_object(oXMLHttpRequest.responseText);
        if(response.ajax == "success")
        {
            //患者情報を反映
            copyPatientData(response.patient_id, response.patient_name, response.sex, response.age_year, response.age_month, response.in_dt, response.disease, response.class1, response.class2);
        }
        else if(response.ajax == "0hit")
        {
            alert("該当する患者が見つかりませんでした。");
        }
        else
        {
            //処理失敗(何もしない)
        }
    }
    else
    {
        //通信失敗(何もしない)
    }
}
//レスポンスデータよりレスポンスオブジェクトを生成します。(コンストラクタ)
function ajax_response_object(response_text)
{
    var a1 = response_text.split("\n");
    for (var i = 0; i < a1.length; i++)
    {
        var line = a1[i];
        var sep_index = line.indexOf("=");
        if(sep_index == -1)
        {
            break;//最後の改行行と判定
        }
        var key = line.substring(0,sep_index);
        var val = line.substring(sep_index+1);

        var eval_string = "this." + key + " = '" + val.replace("'", "\'") + "';";
        eval(eval_string);
    }
}

//患者情報を反映させます。
function copyPatientData(patient_id, patient_name, sex, age_year, age_month, in_dt, disease, class1, class2) {
    if (document.FRM_MAIN.elements['_210_10']) {
        document.FRM_MAIN.elements['_210_10'].value = patient_id;
    }
    if (document.FRM_MAIN.elements['_210_20']) {
        document.FRM_MAIN.elements['_210_20'].value = patient_name;
    }
    if (document.FRM_MAIN.elements['_210_30']) {
        document.FRM_MAIN.elements['_210_30'].value = sex;
    }
    if (document.FRM_MAIN.elements['_210_40']) {
        document.FRM_MAIN.elements['_210_40'].value = age_year;
    }
    if (document.FRM_MAIN.elements['_210_50']) {
        document.FRM_MAIN.elements['_210_50'].value = age_month;
    }
    if (document.FRM_MAIN.elements['_246_76']) {
        document.FRM_MAIN.elements['_246_76'].value = in_dt;
    }
    if (document.FRM_MAIN.elements['_250_80']) {
        document.FRM_MAIN.elements['_250_80'].value = disease;
    }
    if (document.FRM_MAIN.elements['_230_60']) {
        document.FRM_MAIN.elements['_230_60'].value = class1;
    }
    if (document.FRM_MAIN.elements['_240_70']) {
        document.FRM_MAIN.elements['_240_70'].value = class2;
    }
}

//nishiyodo環境患者情報取得
function nishiyodo_patient_search(caller)
{
    //発生日付
    var target_date = "";
    if(document.FRM_MAIN.elements['_100_5'])
    {
        target_date = document.FRM_MAIN.elements['_100_5'].value;
    }

    //患者ID
    var patient_id = "";
    if(document.FRM_MAIN.elements['_210_10'])
    {
        patient_id = document.FRM_MAIN.elements['_210_10'].value;
    }

    //取得結果連携URL
    var this_full_url = document.URL;
    var idx = this_full_url.indexOf("hiyari_easyinput.php");
    var callbacker_url = this_full_url.substr(0,idx) + "hiyari_nishiyodo_patient_search_callbacker.php";

    //呼び出しパラメータセット
    document.patient_search_form.caller.value = caller;
    document.patient_search_form.target_date.value = target_date;
    document.patient_search_form.patient_id.value = patient_id;
    document.patient_search_form.patient_name.value = "";
    document.patient_search_form.page.value = "1";
    document.patient_search_form.callbacker_url.value = callbacker_url;

    //子画面呼び出し
    window.open('', 'newwin', 'width=640,height=480,scrollbars=yes');
    document.patient_search_form.action = "hiyari_patient_search_ny.php";
    document.patient_search_form.target = "newwin";
    document.patient_search_form.submit();
}

//nishiyodo環境患者情報取得結果受信
function call_back_nishiyodo_patient_search(caller,result)
{
    //患者ID
    if(document.FRM_MAIN.elements['_210_10'])
    {
        document.FRM_MAIN.elements['_210_10'].value = result.patient_id;
    }
    //患者氏名
    if(document.FRM_MAIN.elements['_210_20'])
    {
        document.FRM_MAIN.elements['_210_20'].value = result.patient_name;
    }
    //患者の性別
    if(document.FRM_MAIN.elements['_210_30'])
    {
        document.FRM_MAIN.elements['_210_30'].value = result.sex;
    }
    //患者の主治医
    if(document.FRM_MAIN.elements['_243_73'])
    {
        document.FRM_MAIN.elements['_243_73'].value = result.syujii;
    }
    //発生日付があるときのみコピーする項目
    if(result.target_date != "")
    {
        //患者の年齢(年数)
        if(document.FRM_MAIN.elements['_210_40'])
        {
            document.FRM_MAIN.elements['_210_40'].value = result.age_year;
        }
        //患者の年齢(月数)
        if(document.FRM_MAIN.elements['_210_50'])
        {
            document.FRM_MAIN.elements['_210_50'].value = result.age_month;
        }
        //患者区分1
        if(document.FRM_MAIN.elements['_230_60'])
        {
            document.FRM_MAIN.elements['_230_60'].value = result.patient_class1;
        }
        //患者区分2
        if(document.FRM_MAIN.elements['_240_70'])
        {
            document.FRM_MAIN.elements['_240_70'].value = result.patient_class2;
        }
        //入院日
        if(document.FRM_MAIN.elements['_246_76'])
        {
            document.FRM_MAIN.elements['_246_76'].value = result.inpt_date;
        }
    }
}

//CIS環境パターン１患者情報取得結果受信
function call_back_cis1_patient_search(caller,result)
{
    //患者ID
    if(document.FRM_MAIN.elements['_210_10'])
    {
        document.FRM_MAIN.elements['_210_10'].value = result.patient_id;
    }
    //患者氏名
    if(document.FRM_MAIN.elements['_210_20'])
    {
        document.FRM_MAIN.elements['_210_20'].value = result.patient_name;
    }
    //患者の性別
    if(document.FRM_MAIN.elements['_210_30'])
    {
        document.FRM_MAIN.elements['_210_30'].value = result.sex;
    }
    //病棟
    if(document.FRM_MAIN.elements['_1200_10'])
    {
        document.FRM_MAIN.elements['_1200_10'].value = result.byoto;
    }
    //疾患名
    if(document.FRM_MAIN.elements['_250_80'])
    {
        document.FRM_MAIN.elements['_250_80'].value = result.byomei_name1;
    }

    //関連する疾患名１
    if(document.FRM_MAIN.elements['_260_90'])
    {
        document.FRM_MAIN.elements['_260_90'].value = result.byomei_name2;
    }

    //関連する疾患名２
    if(document.FRM_MAIN.elements['_270_100'])
    {
        document.FRM_MAIN.elements['_270_100'].value = result.byomei_name3;
    }

    //関連する疾患名３
    if(document.FRM_MAIN.elements['_280_110'])
    {
        document.FRM_MAIN.elements['_280_110'].value = result.byomei_name4;
    }


    // 関連診療所
    obj_130_90 = document.getElementsByName("_130_90[]");
    if(obj_130_90) {
        var bp = false;
        var j  = false;

        for(var i=0; obj_130_90.length > i; i++) {
            if(obj_130_90[i].value == result.ka_name) {
                obj_130_90[i].checked = true;
                bp = true;
            } else if(obj_130_90[i].value == '38') {
                j = i;
                obj_130_90[i].checked = false;
            } else {
                obj_130_90[i].checked = false;
            }
        }

        if(bp == false && j != false) {
            obj_130_90[j].checked = true;
        }
    }
    //発生日付があるときのみコピーする項目
    if(result.target_date != "")
    {
        //患者の年齢(年数)
        if(document.FRM_MAIN.elements['_210_40'])
        {
            document.FRM_MAIN.elements['_210_40'].value = result.age_year;
        }
        //患者の年齢(月数)
        if(document.FRM_MAIN.elements['_210_50'])
        {
            document.FRM_MAIN.elements['_210_50'].value = result.age_month;
        }
        //患者区分1
        if(document.FRM_MAIN.elements['_230_60'])
        {
            document.FRM_MAIN.elements['_230_60'].value = result.patient_class1;
        }
        //患者区分2
        if(document.FRM_MAIN.elements['_240_70'])
        {
            document.FRM_MAIN.elements['_240_70'].value = result.patient_class2;
        }
        //入院日
        if(document.FRM_MAIN.elements['_246_76'])
        {
            document.FRM_MAIN.elements['_246_76'].value = result.inpt_date;
        }
    }
}

//CIS環境パターン１患者情報取得
function cis_patient_search(caller)
{
    //発生日付
    var target_date = "";
    if(document.FRM_MAIN.elements['_100_5'])
    {
        target_date = document.FRM_MAIN.elements['_100_5'].value;
    }

    //患者ID
    var patient_id = "";
    if(document.FRM_MAIN.elements['_210_10'])
    {
        patient_id = document.FRM_MAIN.elements['_210_10'].value;
    }

    //取得結果連携URL
    var this_full_url = document.URL;
    var idx = this_full_url.indexOf("hiyari_easyinput.php");
    var callbacker_url = this_full_url.substr(0,idx) + "hiyari_cis_patient_search_callbacker.php";

    //呼び出しパラメータセット
    document.patient_search_form.caller.value = caller;
    document.patient_search_form.target_date.value = target_date;
    document.patient_search_form.patient_id.value = patient_id;
    document.patient_search_form.patient_name.value = "";
    document.patient_search_form.page.value = "1";
    document.patient_search_form.callbacker_url.value = callbacker_url;

    //子画面呼び出し
    window.open('', 'newwin', 'width=640,height=480,scrollbars=yes');
    document.patient_search_form.action = "hiyari_patient_search_cis.php";
    document.patient_search_form.target = "newwin";
    document.patient_search_form.submit();
}





//国立福島病院様環境患者情報取得
function fukushima_patient_search(caller)
{
    //発生日付
    var target_date = "";
    if(document.FRM_MAIN.elements['_100_5'])
    {
        target_date = document.FRM_MAIN.elements['_100_5'].value;
    }

    //患者ID
    var patient_id = "";
    if(document.FRM_MAIN.elements['_210_10'])
    {
        patient_id = document.FRM_MAIN.elements['_210_10'].value;
    }

    //取得結果連携URL
    var this_full_url = document.URL;
    var idx = this_full_url.indexOf("hiyari_easyinput.php");
    var callbacker_url = this_full_url.substr(0,idx) + "hiyari_fukushima_patient_search_callbacker.php";

    //呼び出しパラメータセット
    document.patient_search_form.caller.value = caller;
    document.patient_search_form.target_date.value = target_date;
    document.patient_search_form.patient_id.value = patient_id;
    document.patient_search_form.patient_name.value = "";
    document.patient_search_form.page.value = "1";
    document.patient_search_form.callbacker_url.value = callbacker_url;

    //子画面呼び出し
    window.open('', 'newwin', 'width=640,height=480,scrollbars=yes');
    document.patient_search_form.action = "hiyari_patient_search_kf.php";
    document.patient_search_form.target = "newwin";
    document.patient_search_form.submit();
}

//国立福島様環境患者情報取得結果受信
function call_back_fukushima_patient_search(caller,result)
{
    //患者ID
    if(document.FRM_MAIN.elements['_210_10'])
    {
        document.FRM_MAIN.elements['_210_10'].value = result.patient_id;
    }
    //患者氏名
    if(document.FRM_MAIN.elements['_210_20'])
    {
        document.FRM_MAIN.elements['_210_20'].value = result.patient_name;
    }
    //患者の性別
    if(document.FRM_MAIN.elements['_210_30'])
    {
        document.FRM_MAIN.elements['_210_30'].value = result.sex;
    }
    //発生日付があるときのみコピーする項目
    if(result.target_date != "")
    {
        //患者の年齢(年数)
        if(document.FRM_MAIN.elements['_210_40'])
        {
            document.FRM_MAIN.elements['_210_40'].value = result.age_year;
        }
        //患者の年齢(月数)
        if(document.FRM_MAIN.elements['_210_50'])
        {
            document.FRM_MAIN.elements['_210_50'].value = result.age_month;
        }
    }
}

//勤医協中央病院様環境患者情報取得
function kinkyo_patient_search(caller)
{
    //発生日付
    var target_date = "";
    if(document.FRM_MAIN.elements['_100_5'])
    {
        target_date = document.FRM_MAIN.elements['_100_5'].value;
    }

    //患者ID
    var patient_id = "";
    if(document.FRM_MAIN.elements['_210_10'])
    {
        patient_id = document.FRM_MAIN.elements['_210_10'].value;
    }

    //取得結果連携URL
    var this_full_url = document.URL;
    var idx = this_full_url.indexOf("hiyari_easyinput.php");
    var callbacker_url = this_full_url.substr(0,idx) + "hiyari_kinkyo_patient_search_callbacker.php";

    //呼び出しパラメータセット
    document.patient_search_form.caller.value = caller;
    document.patient_search_form.target_date.value = target_date;
    document.patient_search_form.patient_id.value = patient_id;
    document.patient_search_form.patient_name.value = "";
    document.patient_search_form.page.value = "1";
    document.patient_search_form.callbacker_url.value = callbacker_url;

    //子画面呼び出し
    window.open('', 'newwin', 'width=640,height=480,scrollbars=yes');
    document.patient_search_form.action = "hiyari_patient_search_kk.php";
    document.patient_search_form.target = "newwin";
    document.patient_search_form.submit();
}

//勤医協中央病院様環境患者情報取得結果受信
function call_back_kinkyo_patient_search(caller,result)
{
    //患者ID
    if(document.FRM_MAIN.elements['_210_10'])
    {
        document.FRM_MAIN.elements['_210_10'].value = result.patient_id;
    }
    //患者氏名
    if(document.FRM_MAIN.elements['_210_20'])
    {
        document.FRM_MAIN.elements['_210_20'].value = result.patient_name;
    }
    //患者の性別
    if(document.FRM_MAIN.elements['_210_30'])
    {
        document.FRM_MAIN.elements['_210_30'].value = result.sex;
    }
    //患者区分1
    if(document.FRM_MAIN.elements['_230_60'])
    {
        document.FRM_MAIN.elements['_230_60'].value = result.patient_class1;
    }
    //患者区分2
    if(document.FRM_MAIN.elements['_240_70'])
    {
        document.FRM_MAIN.elements['_240_70'].value = result.patient_class2;
    }
    /*
    //病棟
    if(document.FRM_MAIN.elements['_1200_10'])
    {
        document.FRM_MAIN.elements['_1200_10'].value = result.byoto;
    }
    //疾患名
    if(document.FRM_MAIN.elements['_250_80'])
    {
        document.FRM_MAIN.elements['_250_80'].value = result.byomei_name1;
    }
    // 関連診療所
    obj_130_90 = document.getElementsByName("_130_90[]");
    if(obj_130_90) {
        var bp = false;
        var j  = false;

        for(var i=0; obj_130_90.length > i; i++) {
            if(obj_130_90[i].value == result.ka_name) {
                obj_130_90[i].checked = true;
                bp = true;
            } else if(obj_130_90[i].value == '38') {
                j = i;
                obj_130_90[i].checked = false;
            } else {
                obj_130_90[i].checked = false;
            }
        }

        if(bp == false && j != false) {
            obj_130_90[j].checked = true;
        }
    }
    */
    //発生日付があるときのみコピーする項目
    if(result.target_date != "")
    {
        //患者の年齢(年数)
        if(document.FRM_MAIN.elements['_210_40'])
        {
            document.FRM_MAIN.elements['_210_40'].value = result.age_year;
        }
        //患者の年齢(月数)
        if(document.FRM_MAIN.elements['_210_50'])
        {
            document.FRM_MAIN.elements['_210_50'].value = result.age_month;
        }
        //入院日
        /*if(document.FRM_MAIN.elements['_246_76'])
        {
            document.FRM_MAIN.elements['_246_76'].value = result.inpt_date;
        }*/
    }
}

//iwata環境患者情報取得
function iwata_patient_search(caller)
{
    //発生日付
    var target_date = "";
    if(document.FRM_MAIN.elements['_100_5'])
    {
        target_date = document.FRM_MAIN.elements['_100_5'].value;
    }

    //患者ID
    var patient_id = "";
    if(document.FRM_MAIN.elements['_210_10'])
    {
        patient_id = document.FRM_MAIN.elements['_210_10'].value;
    }

    //取得結果連携URL
    var this_full_url = document.URL;
    var idx = this_full_url.indexOf("hiyari_easyinput.php");
    var callbacker_url = this_full_url.substr(0,idx) + "hiyari_iwata_patient_search_callbacker.php";

    //呼び出しパラメータセット
    document.iwata_patient_search_form.caller.value = caller;
    document.iwata_patient_search_form.target_date.value = target_date;
    document.iwata_patient_search_form.patient_id.value = patient_id;
    document.iwata_patient_search_form.patient_name.value = "";
    document.iwata_patient_search_form.page.value = "1";
    document.iwata_patient_search_form.callbacker_url.value = callbacker_url;

    //子画面呼び出し
    window.open('', 'newwin', 'width=640,height=480,scrollbars=yes');
    document.iwata_patient_search_form.action = "<{$iwata_patient_search_url}>";
    document.iwata_patient_search_form.target = "newwin";
    document.iwata_patient_search_form.submit();
}


//iwata環境患者情報取得結果受信
function call_back_iwata_patient_search(caller,result)
{
    //患者ID
    if(document.FRM_MAIN.elements['_210_10'])
    {
        document.FRM_MAIN.elements['_210_10'].value = result.patient_id;
    }
    //患者氏名
    if(document.FRM_MAIN.elements['_210_20'])
    {
        document.FRM_MAIN.elements['_210_20'].value = result.patient_name;
    }
    //患者の性別
    if(document.FRM_MAIN.elements['_210_30'])
    {
        document.FRM_MAIN.elements['_210_30'].value = result.sex;
    }
    //発生日付があるときのみコピーする項目
    if(result.target_date != "")
    {
        //患者の年齢(年数)
        if(document.FRM_MAIN.elements['_210_40'])
        {
            document.FRM_MAIN.elements['_210_40'].value = result.age_year;
        }
        //患者の年齢(月数)
        if(document.FRM_MAIN.elements['_210_50'])
        {
            document.FRM_MAIN.elements['_210_50'].value = result.age_month;
        }
        //患者区分1
        if(document.FRM_MAIN.elements['_230_60'])
        {
            document.FRM_MAIN.elements['_230_60'].value = result.patient_class1;
        }
        //患者区分2
        if(document.FRM_MAIN.elements['_240_70'])
        {
            document.FRM_MAIN.elements['_240_70'].value = result.patient_class2;
        }
        //入院日
        if(document.FRM_MAIN.elements['_246_76'])
        {
            document.FRM_MAIN.elements['_246_76'].value = result.inpt_date;
        }
    }
}

//患者情報取得汎用
function patient_search_ex(caller)
{
    //発生日付
    var target_date = "";
    if(document.FRM_MAIN.elements['_100_5'])
    {
        target_date = document.FRM_MAIN.elements['_100_5'].value;
    }

    //患者ID
    var patient_id = "";
    if(document.FRM_MAIN.elements['_210_10'])
    {
        patient_id = document.FRM_MAIN.elements['_210_10'].value;
    }

    //取得結果連携URL
    var this_full_url = document.URL;
    var idx = this_full_url.indexOf("hiyari_easyinput.php");
    var callbacker_url = this_full_url.substr(0,idx) + "hiyari_patient_search_callbacker.php";

    //呼び出しパラメータセット
    document.patient_search_form.caller.value = caller;
    document.patient_search_form.target_date.value = target_date;
    document.patient_search_form.patient_id.value = patient_id;
    document.patient_search_form.patient_name.value = "";
    document.patient_search_form.page.value = "1";
    document.patient_search_form.callbacker_url.value = callbacker_url;

    //子画面呼び出し
    window.open('', 'newwin', 'width=640,height=480,scrollbars=yes');
    document.patient_search_form.action = "<{$patient_search_ex_url}>";
    document.patient_search_form.target = "newwin";
    document.patient_search_form.submit();
}


//患者情報取得結果受信汎用
function call_back_patient_search(caller,result)
{
    //患者ID
    if(document.FRM_MAIN.elements['_210_10'])
    {
        document.FRM_MAIN.elements['_210_10'].value = result.patient_id;
    }
    //患者氏名
    if(document.FRM_MAIN.elements['_210_20'])
    {
        document.FRM_MAIN.elements['_210_20'].value = result.patient_name;
    }
    //患者の性別
    if(document.FRM_MAIN.elements['_210_30'])
    {
        document.FRM_MAIN.elements['_210_30'].value = result.sex;
    }
    //発生日付があるときのみコピーする項目
    if(result.target_date != "")
    {
        //患者の年齢(年数)
        if(document.FRM_MAIN.elements['_210_40'])
        {
            document.FRM_MAIN.elements['_210_40'].value = result.age_year;
        }
        //患者の年齢(月数)
        if(document.FRM_MAIN.elements['_210_50'])
        {
            document.FRM_MAIN.elements['_210_50'].value = result.age_month;
        }
        //患者区分1
        if(document.FRM_MAIN.elements['_230_60'])
        {
            document.FRM_MAIN.elements['_230_60'].value = result.patient_class1;
        }
        //患者区分2
        if(document.FRM_MAIN.elements['_240_70'])
        {
            document.FRM_MAIN.elements['_240_70'].value = result.patient_class2;
        }
        //入院日
        if(document.FRM_MAIN.elements['_246_76'])
        {
            document.FRM_MAIN.elements['_246_76'].value = result.inpt_date;
        }
    }
}


//カレンダーからの戻り処理を行います。
function call_back_hiyari_calender(caller,ymd)
{
    if(caller == "620_33")
    {
        document.FRM_MAIN.elements['_620_33'].value = ymd;
    }
    else if(caller == "246_76")
    {
        document.FRM_MAIN.elements['_246_76'].value = ymd;
    }
    else
    {
        document.FRM_MAIN.elements['_'+caller].value = ymd;
    }
}

function clear_620_33()
{
    document.FRM_MAIN.elements['_620_33'].value = "";
}

function clear_105_5() {
    document.FRM_MAIN.elements['_105_5'].value = "";
    document.FRM_MAIN.elements['_105_10'].value = "";
    document.FRM_MAIN.elements['_105_20'].value = "";
    document.FRM_MAIN.elements['_105_30'].value = "";
}

function clear_100_5() {
    document.FRM_MAIN.elements['_100_5'].value = "";
    document.FRM_MAIN.elements['_100_10'].value = "";
    document.FRM_MAIN.elements['_100_20'].value = "";
    document.FRM_MAIN.elements['_100_30'].value = "";

}

//主治医情報を保持しているかどうかを表示します。
function disp_update_doctor_have()
{
    var span = document.getElementById("doctor_have_span");
    if(span)
    {
        if(document.FRM_MAIN.doctor_emp_id.value != "")
        {
            span.innerHTML = "(職員マスタ連携あり)";
        }
        else
        {
            span.innerHTML = "";
        }
    }
}

//主治医情報を設定します。
function set_doctor_emp_id(doctor_emp_id)
{
    document.FRM_MAIN.doctor_emp_id.value = doctor_emp_id;
    disp_update_doctor_have();
}

//主治医情報をクリアします。
function clear_doctor_emp_id()
{
    document.FRM_MAIN.doctor_emp_id.value = "";
    disp_update_doctor_have();
}

function check_unset_1100_1() {
    document.getElementById('id_1100_50_1').checked = false;
    document.getElementById('id_1100_50_2').checked = false;
    document.getElementById('id_1100_50_3').checked = false;
}

function check_unset_1100_2() {
    document.getElementById('id_1100_20_1').checked = false;
    document.getElementById('id_1100_20_2').checked = false;
    document.getElementById('id_1100_20_3').checked = false;
    document.getElementById('id_1100_20_4').checked = false;
    document.getElementById('id_1100_30_1').checked = false;
    document.getElementById('id_1100_30_2').checked = false;
    document.getElementById('id_1100_30_3').checked = false;
    document.getElementById('id_1100_30_4').checked = false;
    document.getElementById('id_1100_30_5').checked = false;
    document.getElementById('id_1100_30_6').checked = false;
    check_unset_1100_3();
}

function check_unset_1100_3() {
    if(document.getElementById('id_1100_30_6').checked == false) {
        document.getElementById('id_1100_40').value = '';
    }
}

function change_classification() {
    var subject    = document.getElementById("subject");
    var obj_125_85 = document.getElementsByName("_125_85[]");

        <{if in_array(125, $grp_flags)}>
            for(var i=0; obj_125_85.length > i; i++) {
            if(obj_125_85[i].value == subject[subject.selectedIndex].value) {
                obj_125_85[i].checked = true;
                document.getElementById("report_title").value = subject[subject.selectedIndex].innerHTML;
                break;
            }
        }
        <{else}>
            document.getElementById("_125_85").value = subject[subject.selectedIndex].value;
        document.getElementById("report_title").value = subject[subject.selectedIndex].innerHTML;
        <{/if}>
    }

    //---------------------------------------------
    //クリアボタン
    //---------------------------------------------
    function clear_item(type, grp_code, easy_item_code){
        //値のクリア
        switch(type){
        case "radio":
        case "checkbox":
          j$("input:[name='_" + grp_code + "_" + easy_item_code + "[]']").attr("checked", false);
          break;
        case "select":
        case "text":
        case "textarea":
          j$("#id_" + grp_code + "_" + easy_item_code).val("");
          break;
        }

        //連動
        <{*誤った医療の実施の有無*}>
        if (grp_code==1100 && easy_item_code==10){
            document.getElementById('info_1').style.display='none';
            document.getElementById('info_2').style.display='none';
            check_unset_1100_1();
            check_unset_1100_2();
        }
    }
</script>



<!-- 全体 -->
<table width="100%" cellspacing="0" cellpadding="0">
<tr><td align="center" style="padding-right:10px;padding-left:10px;">
<table width="100%" cellspacing="0" cellpadding="0">
<tr><td>




<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- 報告書情報                                                                                           -->
<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ※TinyMCEのため(?)width指定は全て必要。 -->

<table width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>
        <table width="100%" cellspacing="0" cellpadding="2" class="list">
            <tr height="22">
                <td width="20%" bgcolor="#DFFFDC"><font class="j12"><span <{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}>>表題</span><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}></font></td>
                <{if $subject_mode == 0}>
                    <td width="55%"><input type="text" name="report_title" size="50" style="width:100%;ime-mode:active" value="<{$report_title}>"></td>
                <{elseif $subject_mode == 2}>
                    <td width="55%">
                        <select name="report_title">
                            <option value=""></option>
                        <{foreach item=item from=$classification}>
                            <{if !in_array($item.easy_code, (array)@$item_element_no_disp[125][85])}>
                                <option value="<{$item.easy_name}>" <{if $item.easy_name == $report_title}>selected<{/if}>><{$item.easy_name}></option>
                            <{/if}>
                        <{/foreach}>
                        </select>
                    </td>
                <{elseif $subject_mode == 1}>
                    <td width="55%">
                        <select name="subject" id="subject" onChange="change_classification()">
                            <option value=""></option>
                        <{foreach item=item from=$classification}>
                            <{if !in_array($item.easy_code, (array)@$item_element_no_disp[125][85])}>
                                <option value="<{$item.easy_code}>" <{if $item.easy_name == $report_title}>selected<{/if}>><{$item.easy_name}></option>
                            <{/if}>
                        <{/foreach}>
                        </select>
                        <input type="hidden" id="report_title" name="report_title" value="<{$report_title}>">
                        <{if !in_array(125, $grp_flags)}><input type="hidden" id="_125_85" name="_125_85[]" value="<{$vals[125][85][0]}>"><{/if}>
                    </td>
                <{/if}>
                <td width="10%" bgcolor="#DFFFDC"><font class="j12">報告日</font></td>
                <td width="15%" >
                    <font class="j12">
                    <nobr>
                    <{php}>require_once("show_select_values.ini"); <{/php}>
                    <select id="regist_date_y" name="regist_date_y"><{php}>show_select_years(5, $this->_tpl_vars['regist_date_y'], false);
                    <{/php}></select>年
                    <select id="regist_date_m" name="regist_date_m">
                    <{php}>show_select_months($this->_tpl_vars['regist_date_m'], false); <{/php}></select>月
                    <select id="regist_date_d" name="regist_date_d"><{php}>show_select_days($this->_tpl_vars['regist_date_d'], false);
                    <{/php}>
                    </select>日
                    </nobr>
                    </font>
                </td>

            </tr>
<!--
<{if $mail_input_flg}>
            <tr height="100">
                <td bgcolor="#DFFFDC"><font class="j12">セーフティマネージャへのメッセージ</font></td>
                <td colspan="3"><font class="j12"><textarea style="width:100%;height:100%" name="mail_message" class="mceEditor"><{$mail_message}></textarea></font></td>
            </tr>
<{/if}>
-->
        </table>
        <table width="100%" cellspacing="0" cellpadding="0">
            <tr><td><img src="img/spacer.gif" width="10" height="1" alt=""></td></tr>
        </table>

    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<br>






<{if $input_mode == "analysis"}>
<{include file="hiyari_edit_easyinput_analysis.tpl"}>
<{else}>


<!-- レポート内容START -->

<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ８．医療事故情報収集事業の発生件数情報等                                                             -->
<!-- ---------------------------------------------------------------------------------------------------- -->
<{if in_array(1100, $grp_flags) || in_array(1110, $grp_flags) || in_array(1120, $grp_flags)}>
<table width="100%" cellspacing="0" cellpadding="0">
  <tr><td><font class="j12"><b>医療事故情報収集事業の発生件数情報等</b></font></td></tr>
</table>
<table width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>
      <table width="100%" cellspacing="0" cellpadding="2" class="list">
        <{if in_array(1100, $grp_flags)}>
        <tr height="22">
          <td width=250 bgcolor="#DFFFDC">
            <font class="j12">
            <span <{if in_array('1100'|cat:'_'|cat:'10', $item_must) }><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>誤った医療の実施の有無<{if in_array('1100'|cat:'_'|cat:'10', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></span>
            </font>
            <font class="j12"></font>
          </td>
          <td class="item_selection">
            <span class="item_set">
              <input type="radio" id="id_1100_10_1" name="_1100_10[]" value="実施あり"
                onclick="document.getElementById('info_1').style.display='';document.getElementById('info_2').style.display='none';check_unset_1100_1();"
                <{if $vals[1100][10][0] == '実施あり'}>checked<{/if}>>
              <label for="id_1100_10_1">
                <font class="j12">実施あり</font>
              </label>
            </span>
            <span class="item_set">
              <input type="radio" id="id_1100_10_2" name="_1100_10[]" value="実施なし"
                onclick="document.getElementById('info_2').style.display='';document.getElementById('info_1').style.display='none';check_unset_1100_2();"
                <{if $vals[1100][10][0] == '実施なし'}>checked<{/if}>>
              <label for="id_1100_10_2">
                <font class="j12">実施なし</font>
              </label>
            </span>
          </td>
          <td class="item_btn">
            <a href="javascript:void(0);" onclick="clear_item('radio',1100,10);">
              <font class="j12">[クリア]</font>
            </a>
          </td>
        </tr>
      </table>

      <div id="info_1" style="display:none">
        <table width="100%" cellspacing="0" cellpadding="0">
          <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
        </table>
        <table width="100%" cellspacing="0" cellpadding="2" class="list">
          <tr height="22">
            <td width=250 bgcolor="#DFFFDC">
              <font class="j12">
              <span <{if in_array('1100'|cat:'_'|cat:'10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>実施の程度<{if in_array('1100'|cat:'_'|cat:'10', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></span>
              </font>
              <font class="j12"></font>
            </td>
            <td class="item_selection">
              <span class="item_set">
                <input type='radio' id="id_1100_20_1" name="_1100_20[]" value="濃厚な治療"
                <{if $vals[1100][20][0] == '濃厚な治療'}>checked<{/if}>>
                <label for="id_1100_20_1"><font class="j12">濃厚な治療</font></label>
              </span>
              <br>
              <span class="item_set">
                <input type='radio' id="id_1100_20_2" name="_1100_20[]" value="軽微な治療"
                <{if $vals[1100][20][0] == '軽微な治療'}>checked<{/if}>>
                <label for="id_1100_20_2"><font class="j12">軽微な治療</font></label>
              </span>
              <br>
              <span class="item_set">
                <input type='radio' id="id_1100_20_3" name="_1100_20[]" value="なし"
                <{if $vals[1100][20][0] == 'なし'}>checked<{/if}>>
                <label for="id_1100_20_3"><font class="j12">なし</font></label>
              </span>
              <br>
              <span class="item_set">
                <input type='radio' id="id_1100_20_4" name="_1100_20[]" value="不明"
                <{if $vals[1100][20][0] == '不明'}>checked<{/if}>>
                <label for="id_1100_20_4"><font class="j12">不明</font></label>
              </span>
            </td>
            <td class="item_btn">
              <a href="javascript:void(0);" onclick="clear_item('radio',1100,20);">
                <font class="j12">[クリア]</font>
              </a>
            </td>
          </tr>
          <tr height="22">
            <td width=250 bgcolor="#DFFFDC">
              <font class="j12">
              <span>事故の程度</span>
              </font>
              <font class="j12"></font>
            </td>
            <td class="item_selection">
              <span class="item_set">
                <input type='radio' id="id_1100_30_1" name="_1100_30[]" value="死亡"
                onclick="check_unset_1100_3()"
                <{if $vals[1100][30][0] == '死亡'}>checked<{/if}>>
                <label for="id_1100_30_1"><font class="j12">死亡</font></label>
              </span>
              <br>
              <span class="item_set">
                <input type='radio' id="id_1100_30_2" name="_1100_30[]" value="障害残存の可能性がある(高い)"
                onclick="check_unset_1100_3()"
                <{if $vals[1100][30][0] == '障害残存の可能性がある(高い)'}>checked<{/if}>>
                <label for="id_1100_30_2"><font class="j12">障害残存の可能性がある(高い)</font></label>
              </span>
              <br>
              <span class="item_set">
                <input type='radio' id="id_1100_30_3" name="_1100_30[]" value="障害残存の可能性がある(低い)"
                onclick="check_unset_1100_3()"
                <{if $vals[1100][30][0] == '障害残存の可能性がある(低い)'}>checked<{/if}>>
                <label for="id_1100_30_3"><font class="j12">障害残存の可能性がある(低い)</font></label>
              </span>
              <br>
              <span class="item_set">
                <input type='radio' id="id_1100_30_4" name="_1100_30[]" value="障害残存の可能性なし"
                onclick="check_unset_1100_3()"
                <{if $vals[1100][30][0] == '障害残存の可能性なし'}>checked<{/if}>>
                <label for="id_1100_30_4"><font class="j12">障害残存の可能性なし</font></label>
              </span>
              <br>
              <span class="item_set">
                <input type='radio' id="id_1100_30_5" name="_1100_30[]" value="障害なし"
                onclick="check_unset_1100_3()"
                <{if $vals[1100][30][0] == '障害なし'}>checked<{/if}>>
                <label for="id_1100_30_5"><font class="j12">障害なし</font></label>
              </span>
              <br>
              <span class="item_set">
                <input type='radio' id="id_1100_30_6" name="_1100_30[]" value="不明"
                <{if $vals[1100][30][0] == '不明'}>checked<{/if}>>
                <label for="id_1100_30_6"><font class="j12">不明</font></label>
                (
                <input type="text" id="id_1100_40" name="_1100_40" size="60" value="<{$vals[1100][40][0]}>" style="ime-mode:active">
                )
              </span>
            </td>
            <td class="item_btn">
              <a href="javascript:void(0);" onclick="clear_item('radio',1100,30)">
                <font class="j12">[クリア]</font>
              </a>
            </td>
          </tr>
        </table>
      </div>

      <div id="info_2" style="display:none">
        <table width="100%" cellspacing="0" cellpadding="0">
          <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
        </table>
        <table width="100%" cellspacing="0" cellpadding="2" class="list">
          <tr height="22">
            <td width=250 bgcolor="#DFFFDC">
              <font class="j12">
              <span <{if in_array('1100'|cat:'_'|cat:'10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>仮に実施された場合の影響度<{if in_array('1100'|cat:'_'|cat:'10', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></span>
              </font>
              <font class="j12"></font>
            </td>
            <td class="item_selection">
              <span class="item_set">
                <input type="radio" id="id_1100_50_1" name="_1100_50[]" value="死亡もしくは重篤な状況に至ったと考えられる"
                <{if $vals[1100][50][0] == '死亡もしくは重篤な状況に至ったと考えられる'}>checked<{/if}> >
                <label for="id_1100_50_1">
                  <font class="j12">死亡もしくは重篤な状況に至ったと考えられる</font>
                </label>
              </span>
              <br>
              <span class="item_set">
                <input type="radio" id="id_1100_50_2" name="_1100_50[]" value="濃厚な処置・治療が必要であると考えられる"
                <{if $vals[1100][50][0] == '濃厚な処置・治療が必要であると考えられる'}>checked<{/if}> >
                <label for="id_1100_50_2">
                  <font class="j12">濃厚な処置・治療が必要であると考えられる</font>
                </label>
              </span>
              <br>
              <span class="item_set">
                <input type="radio" id="id_1100_50_3" name="_1100_50[]" value="軽微な処置・治療が必要もしくは処置・治療が不要と考えられる"
                <{if $vals[1100][50][0] == '軽微な処置・治療が必要もしくは処置・治療が不要と考えられる'}>checked<{/if}> >
                <label for="id_1100_50_3">
                  <font class="j12">軽微な処置・治療が必要もしくは処置・治療が不要と考えられる</font>
                </label>
              </span>
            </td>
            <td class="item_btn">
              <a href="javascript:void(0);" onclick="clear_item('radio',1100,50);">
                <font class="j12">[クリア]</font>
              </a>
            </td>
          </tr>
        </table>
      </div>
      <{/if}>

      <table width="100%" cellspacing="0" cellpadding="0">
        <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
      </table>

      <table width="100%" cellspacing="0" cellpadding="2" class="list">
        <{if in_array(1110, $grp_flags)}>
        <tr height="22">
          <td width=250 bgcolor="#DFFFDC">
            <font class="j12">
            <span <{if in_array('1110'|cat:'_'|cat:'10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>特に報告を求める事例<{if in_array('1110'|cat:'_'|cat:'10', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></span>
            </font>
            <font class="j12"></font>
          </td>
          <td class="item_selection">
            <span class="item_set">
              <input type="radio" id="id_1110_10_0" name="_1110_10[]" value='院内感染による死亡や障害'
              onclick="clear_item('text', 1110, 20);"
              <{if $vals[1110][10][0] == '院内感染による死亡や障害'}> checked<{/if}>>
              <label for="id_1110_10_0">
                <font class="j12">院内感染による死亡や障害</font>
              </label>
            </span>
            <br>
            <span class="item_set">
              <input type="radio" id="id_1110_10_1" name="_1110_10[]" value='患者の自殺又は自殺企図'
              onclick="clear_item('text', 1110, 20);"
              <{if $vals[1110][10][0] == '患者の自殺又は自殺企図'}> checked<{/if}>>
              <label for="id_1110_10_1">
                <font class="j12">患者の自殺又は自殺企図</font>
              </label>
            </span>
            <br>
            <span class="item_set">
              <input type="radio" id="id_1110_10_2" name="_1110_10[]" value='入院患者の失踪'
              onclick="clear_item('text', 1110, 20);"
              <{if $vals[1110][10][0] == '入院患者の失踪'}> checked<{/if}>>
              <label for="id_1110_10_2">
                <font class="j12">入院患者の失踪</font>
              </label>
            </span>
            <br>
            <span class="item_set">
              <input type="radio" id="id_1110_10_3" name="_1110_10[]" value='入院中の熱傷'
              onclick="clear_item('text', 1110, 20);"
              <{if $vals[1110][10][0] == '入院中の熱傷'}> checked<{/if}>>
              <label for="id_1110_10_3">
                <font class="j12">入院中の熱傷</font>
              </label>
            </span>
            <br>
            <span class="item_set">
              <input type="radio" id="id_1110_10_4" name="_1110_10[]" value='入院中の感電'
              onclick="clear_item('text', 1110, 20);"
              <{if $vals[1110][10][0] == '入院中の感電'}> checked<{/if}>>
              <label for="id_1110_10_4">
                <font class="j12">入院中の感電</font>
              </label>
            </span>
            <br>
            <span class="item_set">
              <input type="radio" id="id_1110_10_5" name="_1110_10[]" value='医療施設内の火災による患者の死亡や障害'
              onclick="clear_item('text', 1110, 20);"
              <{if $vals[1110][10][0] == '医療施設内の火災による患者の死亡や障害'}> checked<{/if}>>
              <label for="id_1110_10_5">
                <font class="j12">医療施設内の火災による患者の死亡や障害</font>
              </label>
            </span>
            <br>
            <span class="item_set">
              <input type="radio" id="id_1110_10_6" name="_1110_10[]" value='間違った保護者の許への新生児の引渡し'
              onclick="clear_item('text', 1110, 20);"
              <{if $vals[1110][10][0] == '間違った保護者の許への新生児の引渡し'}> checked<{/if}>>
              <label for="id_1110_10_6">
                <font class="j12">間違った保護者の許への新生児の引渡し</font>
              </label>
            </span>
            <br>
            <span class="item_set">
              <input type="radio" id="id_1110_10_7" name="_1110_10[]" value='本事例は選択肢には該当しない（その他）'
              <{if $vals[1110][10][0] == '本事例は選択肢には該当しない（その他）'}> checked<{/if}>>
              <label for="id_1110_10_7">
                <font class="j12">本事例は選択肢には該当しない（その他）</font>
              </label>
              (
              <input type="text" id="id_1110_20" name="_1110_20" value="<{$vals[1110][20][0]}>" size="60" style="ime-mode:active">
              )
            </span>
          </td>
          <td class="item_btn">
            <a href="javascript:void(0);" onclick="clear_item('radio',1110,10);">
              <font class="j12">[クリア]</font>
            </a>
          </td>
        </tr>
      </table>
      <{/if}>

      <table width="100%" cellspacing="0" cellpadding="0">
        <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
      </table>

      <table width="100%" cellspacing="0" cellpadding="2" class="list">
        <{if in_array(1120, $grp_flags)}>
          <tr height="22">
            <td width=250 bgcolor="#DFFFDC">
              <font class="j12">
                <span <{if in_array('1120'|cat:'_'|cat:'10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>発生件数集計再掲<{if in_array('1120'|cat:'_'|cat:'10', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></span>
              </font>
              <font class="j12"></font>
            </td>
            <td class="item_selection">
              <label for="id_1120_10_1">
                <input type='radio' id="id_1120_10_1" name="_1120_10[]" value="薬剤の名称や形状に関連する事例" onclick="radio_onclick(this)" <{if $vals[1120][10][0] == '薬剤の名称や形状に関連する事例'}>checked<{/if}>>
                <font class="j12">薬剤の名称や形状に関連する事例</font>
              </label>
              <br>
              <label for="id_1120_10_2">
                <input type='radio' id="id_1120_10_2" name="_1120_10[]" value="薬剤に由来する事例" onclick="radio_onclick(this)" <{if $vals[1120][10][0] == '薬剤に由来する事例'}>checked<{/if}>>
                <font class="j12">薬剤に由来する事例</font>
              </label>
              <br>
              <label for="id_1120_10_3">
                <input type='radio' id="id_1120_10_3" name="_1120_10[]" value="医療機器等に由来する事例" onclick="radio_onclick(this)" <{if $vals[1120][10][0] == '医療機器等に由来する事例'}>checked<{/if}>>
                <font class="j12">医療機器等に由来する事例</font>
              </label>
              <br>
              <label for="id_1120_10_4">
                <input type='radio' id="id_1120_10_4" name="_1120_10[]" value="今期のテーマ(<{$theme}>)" onclick="radio_onclick(this)" <{if ereg("^今期のテーマ", $vals[1120][10][0])}>checked<{/if}>>
                <font class="j12">今期のテーマ(<{$theme}>)</font>
              </label>

              <br>
              <label for="id_1120_10_0">
                <input type='radio' id="id_1120_10_0" name="_1120_10[]" value="該当無し" onclick="radio_onclick(this)" <{if $vals[1120][10][0] == '該当無し'}>checked<{/if}>>
                <font class="j12">該当無し</font>
              </label>

            </td>
            <td class="item_btn">
              <a href="javascript:void(0);" onclick="clear_item('radio',1120,10);">
                <font class="j12">[クリア]</font>
              </a>
            </td>
          </tr>
        <{/if}>
      </table>
    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<br>
<{/if}>

<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- １．いつ、どこで                                                                                     -->
<!-- ---------------------------------------------------------------------------------------------------- -->

<{if in_array(100, $grp_flags) || in_array(110, $grp_flags) || in_array(105, $grp_flags) || in_array(116, $grp_flags)}>
<table width="100%" cellspacing="0" cellpadding="0">
    <tr><td><font class="j12"><b>いつ、どこで</b></font></td></tr>
</table>
<table width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>


        <table width="100%" cellspacing="0" cellpadding="2" class="list">

            <{assign var="colspan_110_60_f" value="1"}>
            <{if in_array(100, $grp_flags)}>
                <{assign var="colspan_110_60_f" value="3"}>
            <{/if}>


            <{* 発見日時 *}>
            <{if in_array(105, $grp_flags)}>
            <tr height="22">

                <td bgcolor="#DFFFDC" width="95">
                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=105 j=5}>
                </td>
                <td width="220">
                    <nobr>
                    <input type="text" id="_105_5" name="_105_5" value="<{$vals[105][5][0]}>" style="width:100;border:#35B341 solid 2px;" readonly>
                    <input type="button" value="今日" onclick="set_105_5_now()">
                    <img id="start_date_caller" src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer;" onclick="window.open('hiyari_calendar_discover.php?session=<{$session}>', 'newwin', 'width=640,height=480,scrollbars=yes');"/>
                    <input type="button" value="クリア" onclick="clear_105_5()">
                    </nobr>
                    <input type="hidden" id="_105_10" name="_105_10" value="<{$vals[105][10][0]}>">
                    <input type="hidden" id="_105_20" name="_105_20" value="<{$vals[105][20][0]}>">
                    <input type="hidden" id="_105_30" name="_105_30" value="<{$vals[105][30][0]}>">
                    <input type="hidden" id="_105_70" name="_105_70" value="<{$vals[105][70][0]}>">
                    <{* 確認画面で様式を取得するためにデータを送る START *}>
                    <{*<input type="hidden" name="eis_title_id" value="<{$eis_title_id}>">*}>
                    <{* 確認画面で様式を取得するためにデータを送る END *}>
                    <script type="text/javascript">
                        function set_105_5_now() {
                            var obj_ymd = document.getElementById("_105_5");
                            obj_ymd.value = '<{$this_ymd}>';

                            var obj_month = document.getElementById("_105_10");
                            obj_month.value = '<{$this_month}>';

                            var obj_day_week = document.getElementById("_105_20");
                            obj_day_week.value = '<{$day_week}>';

                            var obj_week_div = document.getElementById("_105_30");
                            obj_week_div.value = '<{$week_div}>';

                            <{* 事例の詳細(時系列)があれば、一緒にセットする *}>
                            <{* 1行目のみ & 日付が未設定の場合               *}>
                            if (document.getElementById("id_4000_1")) {
                                if (! document.getElementById("id_4000_1").value) {
                                    document.getElementById("id_4000_1").value = '<{$this_ymd}>';
                                }
                            }
                        }
                    </script>
                </td>

                <{* 2時間単位選択表示の場合 *}>
                <{if $time_setting_flg == true }>

                    <td bgcolor="#DFFFDC" width="95">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=105 j=40}>
                    </td>
                    <td width="130">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=105 j=40}>
                    </td>

                <{* 2時間単位選択表示の場合 *}>
                <{ else }>
                    <td bgcolor="#DFFFDC" width="85">
                        <font size="3" class="j12">発見時刻</font>
                        <font size="3" class="j12"><{if in_array('105'|cat:'_'|cat:'60', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></font>
                    </td>

                    <td width="140">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=105 j=60}><font size="3" class="j12">時</font>
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=105 j=65}><font size="3" class="j12">分</font>
                    </td>

                    <input type="hidden" id="_105_40" name="_105_40" value="<{$vals[105][40][0]}>">
                <{/if}>



                <td bgcolor="#DFFFDC" width="150">
                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=105 j=50}>
                </td>
                <td>
                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=105 j=50 ime="active"}>
                </td>
            </tr>
            <{/if}>

            <{* 発生日時 *}>
            <{if in_array(100, $grp_flags)}>
            <tr height="22">

                <td bgcolor="#DFFFDC" width="95">
                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=100 j=5}>
                </td>
                <td width="220">
                    <nobr>
                    <input type="text" id="_100_5" name="_100_5" value="<{$vals[100][5][0]}>" style="width:100;border:#35B341 solid 2px;" readonly>
                    <input type="button" value="今日" onclick="set_100_5_now()">
                    <img id="start_date_caller" src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer;" onclick="window.open('hiyari_calendar.php?session=<{$session}>', 'newwin', 'width=640,height=480,scrollbars=yes');"/>
                    <input type="button" value="クリア" onclick="clear_100_5()">
                    </nobr>
                    <input type="hidden" id="_100_10" name="_100_10" value="<{$vals[100][10][0]}>">
                    <input type="hidden" id="_100_20" name="_100_20" value="<{$vals[100][20][0]}>">
                    <input type="hidden" id="_100_30" name="_100_30" value="<{$vals[100][30][0]}>">
                    <{* 確認画面で様式を取得するためにデータを送る START *}>
                    <{*<input type="hidden" name="eis_title_id" value="<{$eis_title_id}>">*}>
                    <{* 確認画面で様式を取得するためにデータを送る END *}>
                    <script type="text/javascript">
                        function set_100_5_now() {
                            var obj_ymd = document.getElementById("_100_5");
                            obj_ymd.value = '<{$this_ymd}>';

                            var obj_month = document.getElementById("_100_10");
                            obj_month.value = '<{$this_month}>';

                            var obj_day_week = document.getElementById("_100_20");
                            obj_day_week.value = '<{$day_week}>';

                            var obj_week_div = document.getElementById("_100_30");
                            obj_week_div.value = '<{$week_div}>';

                            <{* 事例の詳細(時系列)があれば、一緒にセットする *}>
                            <{* 1行目のみ & 日付が未設定の場合               *}>
                            if (document.getElementById("id_4000_1")) {
                                if (! document.getElementById("id_4000_1").value) {
                                    document.getElementById("id_4000_1").value = '<{$this_ymd}>';
                                }
                            }
                        }
                    </script>
                </td>

                <{* 2時間単位選択表示の場合 *}>
                <{if $time_setting_flg == true }>
                    <td bgcolor="#DFFFDC" width="95">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=100 j=40}>
                    </td>
                    <td width="130">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=100 j=40}>
                    </td>

                <{* 2時間単位選択表示の場合 *}>
                                <{ else }>
                    <td bgcolor="#DFFFDC" width="85">
                        <font size="3" class="j12">発生時刻</font>
                        <font size="3" class="j12"><{if in_array('100'|cat:'_'|cat:'60', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></font>
                    </td>

                    <td width="140">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=100 j=60}><font size="3" class="j12">時</font>
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=100 j=65}><font size="3" class="j12">分</font>
                    </td>

                    <input type="hidden" id="_100_40" name="_100_40" value="<{$vals[100][40][0]}>">
                                <{/if}>

                <td bgcolor="#DFFFDC" width="150">
                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=100 j=50}>
                </td>
                <td>
                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=100 j=50 ime="active"}>
                </td>
            </tr>
            <{/if}>

            <{* 発生部署 *}>
            <{if in_array(116, $grp_flags)}>
                <tr height="22">
                    <td bgcolor="#DFFFDC" width="95">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=116 j=10}>
                    </td>
                    <td width="455" colspan="<{$colspan_110_60_f}>">
                        <nobr>
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=116 j=10}>
                        </nobr>
                    </td>

                    <td bgcolor="#DFFFDC" width="150">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=116 j=15}>
                    </td>
                    <td>
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=116 j=15 ime="active"}>
                    </td>
                </tr>
            <{/if}>

            <{* 発生場所 *}>
            <{if in_array(110, $grp_flags)}>
            <tr height="22">
                <td bgcolor="#DFFFDC" width="95">
                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=110 j=60}>
                </td>
                <td width="455" colspan="<{$colspan_110_60_f}>">
                    <nobr>
                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=110 j=60}>
                    (<{include file="hiyari_edit_easyinput_item_field.tpl" i=110 j=65 ime="active"}>)
                    </nobr>
                </td>

                <td bgcolor="#DFFFDC" width="150">
                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=110 j=70}>
                </td>
                <td>
                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=110 j=70 ime="active"}>
                </td>
            </tr>
            <{/if}>

        </table>



    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<br>
<{/if}>









<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ２．誰が                                                                                             -->
<!-- ---------------------------------------------------------------------------------------------------- -->

<{if   in_array(3000, $grp_flags)
     || array_intersect(array(3050, 3100, 3150, 3200, 3250, 3300, 3350, 3400, 3450), $grp_flags)
     || in_array(3500, $grp_flags)
     || in_array(3020, $grp_flags)
     || array_intersect(array(3030, 3032, 3034, 3036, 3038), $grp_flags)
}>
<table width="100%" cellspacing="0" cellpadding="0">
    <tr><td><font class="j12"><b>誰が</b></font></td></tr>
</table>

<table width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>

        <{if in_array(3000, $grp_flags) || in_array(3020, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <tr>
                <{if in_array(3000, $grp_flags)}>
                    <td bgcolor="#DFFFDC" width="250">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=3000 j=10}>
                    </td>
                    <td width="300">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=3000 j=10}>
                    </td>
                <{/if}>

                <{if in_array(3020, $grp_flags)}>
                    <td bgcolor="#DFFFDC" width="250">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=3020 j=10}>
                    </td>
                    <td width="300">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=3020 j=10}>
                    </td>
                <{/if}>

                <{if in_array(3000, $grp_flags)}>
                    <td bgcolor="#DFFFDC" width="150">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=3000 j=20}>
                    </td>
                    <td>
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=3000 j=20 ime="active"}>
                    </td>
                <{/if}>
                </tr>
            </table>
        <{/if}>

        <{if array_intersect(array(3030, 3032, 3034, 3036, 3038), $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <table width="100%" cellspacing="0" cellpadding="2" class="list">
                            <{if in_array(3030, $grp_flags)}>
                                <{include file="hiyari_edit_easyinput_item.tpl" i=3030 j=10 tr=false ime="active"}>
                                <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
                                    <td width="25%" rowspan="<{$discoverer_rowspan}>">

                                        <{if $self_button_flg == true }>
                                            <input type="button" value="本人ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="copyFromSelfProfile2();" style="width:200px;margin-bottom:2px;"><br />
                                        <{/if}>

                                        <{if $other_user_button_flg == true }>
                                            <input type="button" value="他の職員ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="other_profile_copy2();" style="width:200px;"><br />
                                        <{/if}>

                                        <{if $unknown_button_flg == true}>
                                            <input type="button" value="ﾌﾟﾛﾌｨｰﾙ不明" onclick="setUnknownProfile2();" style="width:200px;">
                                        <{/if}>

                                    </td>
                                <{/if}>
                            <{/if}>
                            <{if in_array(3032, $grp_flags)}>
                            <tr height="22">
                                <{include file="hiyari_edit_easyinput_item.tpl" i=3032 j=10 tr=false  ime="active"}>
                                <{if !in_array(3030, $grp_flags)}>

                                <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
                                    <td width="25%" rowspan="<{$discoverer_rowspan}>">

                                        <{if $self_button_flg == true }>
                                            <input type="button" value="本人ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="copyFromSelfProfile2();" style="width:200px;margin-bottom:2px;"><br />
                                        <{/if}>

                                        <{if $other_user_button_flg == true }>
                                            <input type="button" value="他の職員ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="other_profile_copy2();" style="width:200px;"><br />
                                        <{/if}>

                                        <{if $unknown_button_flg == true}>
                                            <input type="button" value="ﾌﾟﾛﾌｨｰﾙ不明" onclick="setUnknownProfile2();" style="width:200px;">
                                        <{/if}>

                                    </td>
                                <{/if}>

                                <{/if}>
                            </tr>
                            <{/if}>
                            <{if in_array(3034, $grp_flags)}>
                            <tr height="22">
                                <{include file="hiyari_edit_easyinput_item.tpl" i=3034 j=10 tr=false  ime="active"}>
                                <{if !in_array(3030, $grp_flags) && !in_array(3032, $grp_flags)}>

                                <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
                                    <td width="25%" rowspan="<{$discoverer_rowspan}>">

                                        <{if $self_button_flg == true }>
                                            <input type="button" value="本人ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="copyFromSelfProfile2();" style="width:200px;margin-bottom:2px;"><br />
                                        <{/if}>

                                        <{if $other_user_button_flg == true }>
                                            <input type="button" value="他の職員ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="other_profile_copy2();" style="width:200px;"><br />
                                        <{/if}>

                                        <{if $unknown_button_flg == true}>
                                            <input type="button" value="ﾌﾟﾛﾌｨｰﾙ不明" onclick="setUnknownProfile2();" style="width:200px;">
                                        <{/if}>

                                    </td>
                                <{/if}>
                                <{/if}>
                            </tr>
                            <{/if}>
                            <{if in_array(3036, $grp_flags)}>
                            <tr height="22">
                                <{include file="hiyari_edit_easyinput_item.tpl" i=3036 j=10 tr=false}>
                                <{if !in_array(3030, $grp_flags) && !in_array(3032, $grp_flags) && !in_array(3034, $grp_flags)}>

                                <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
                                    <td width="25%" rowspan="<{$discoverer_rowspan}>">

                                        <{if $self_button_flg == true }>
                                            <input type="button" value="本人ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="copyFromSelfProfile2();" style="width:200px;margin-bottom:2px;"><br />
                                        <{/if}>

                                        <{if $other_user_button_flg == true }>
                                            <input type="button" value="他の職員ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="other_profile_copy2();" style="width:200px;"><br />
                                        <{/if}>

                                        <{if $unknown_button_flg == true}>
                                            <input type="button" value="ﾌﾟﾛﾌｨｰﾙ不明" onclick="setUnknownProfile2();" style="width:200px;">
                                        <{/if}>

                                    </td>
                                <{/if}>

                                <{/if}>
                            </tr>
                                <{include file="hiyari_edit_easyinput_item.tpl" i=3036 j=20 tr=true}>
                            <{/if}>
                            <{if in_array(3038, $grp_flags)}>
                            <tr height="22">
                                <{include file="hiyari_edit_easyinput_item.tpl" i=3038 j=10 tr=false}>
                                <{if !in_array(3030, $grp_flags) && !in_array(3032, $grp_flags) && !in_array(3034, $grp_flags) && !in_array(3036, $grp_flags)}>

                                <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
                                    <td width="25%" rowspan="<{$discoverer_rowspan}>">

                                        <{if $self_button_flg == true }>
                                            <input type="button" value="本人ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="copyFromSelfProfile2();" style="width:200px;margin-bottom:2px;"><br />
                                        <{/if}>

                                        <{if $other_user_button_flg == true }>
                                            <input type="button" value="他の職員ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="other_profile_copy2();" style="width:200px;"><br />
                                        <{/if}>

                                        <{if $unknown_button_flg == true}>
                                            <input type="button" value="ﾌﾟﾛﾌｨｰﾙ不明" onclick="setUnknownProfile2();" style="width:200px;">
                                        <{/if}>

                                    </td>
                                <{/if}>
                                <{/if}>
                            </tr>
                                <{include file="hiyari_edit_easyinput_item.tpl" i=3038 j=20 tr=true}>
                            <{/if}>
                        </table>
                    </td>
                </tr>
            </table>
        <{/if}>




        <{if in_array(3000, $grp_flags) && (array_intersect(array(3050, 3100, 3150, 3200, 3250, 3300, 3350, 3400, 3450), $grp_flags) || in_array(3500, $grp_flags))}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>




        <{if array_intersect(array(3050, 3100, 3150, 3200, 3250, 3300, 3350, 3400, 3450), $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="0" id="parties">

                <{assign var="parties_count" value=10}>
                <{section name=parties_index loop=$parties_count}>
                    <{assign var="index" value=$smarty.section.parties_index.index}>

                    <{if array_intersect(array(3050 + $index, 3100 + $index, 3150 + $index, 3200 + $index, 3250 + $index, 3300 + $index, 3350 + $index, 3400 + $index, 3450 + $index), $grp_flags) || array_intersect(array(3050 + $index, 3100 + $index, 3150 + $index, 3200 + $index, 3250 + $index, 3300 + $index, 3350 + $index, 3400 + $index, 3450 + $index), array_keys($vals))}>
                    <tr>
                        <td>
                            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                                <tr height="22">
                                    <td bgcolor="#DFFFDC" colspan="<{$party_cols}>"><font class="j12">当事者<{$index+1}></font></td>
                                </tr>
                                <{if in_array(3050, $grp_flags)}>
                                    <{include file="hiyari_edit_easyinput_item.tpl" i=$index+3050 j=30 tr=true colspan=$party_cols-1}>
                                    <{include file="hiyari_edit_easyinput_item.tpl" i=$index+3050 j=35 tr=true colspan=$party_cols-1 ime="active"}>
                                <{/if}>
                                <{if in_array(3100, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_edit_easyinput_item.tpl" i=$index+3100 j=40 tr=false ime="active"}>

                                    <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
                                        <td width="25%" rowspan="<{$profile_rowspan}>">

                                            <{if $self_button_flg == true }>
                                                <input type="button" value="本人ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="copyFromSelfProfile('<{$index}>');" style="width:200px;margin-bottom:2px;"><br />
                                            <{/if}>

                                            <{if $other_user_button_flg == true }>
                                                <input type="button" value="他の職員ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="other_profile_copy(<{$index}>);" style="width:200px;"><br />
                                            <{/if}>

                                            <{if $unknown_button_flg == true}>
                                                <input type="button" value="ﾌﾟﾛﾌｨｰﾙ不明" onclick="setUnknownProfile('<{$index}>');" style="width:200px;">
                                            <{/if}>

                                        </td>
                                    <{/if}>

                                </tr>
                                <{/if}>

                                <{if in_array(3150, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_edit_easyinput_item.tpl" i=$index+3150 j=50 tr=false ime="active"}>
                                    <{if !in_array(3100, $grp_flags)}>

                                    <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
                                        <td width="25%" rowspan="<{$profile_rowspan}>">

                                            <{if $self_button_flg == true }>
                                                <input type="button" value="本人ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="copyFromSelfProfile('<{$index}>');" style="width:200px;margin-bottom:2px;"><br />
                                            <{/if}>

                                            <{if $other_user_button_flg == true }>
                                                <input type="button" value="他の職員ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="other_profile_copy(<{$index}>);" style="width:200px;"><br />
                                            <{/if}>

                                            <{if $unknown_button_flg == true}>
                                                <input type="button" value="ﾌﾟﾛﾌｨｰﾙ不明" onclick="setUnknownProfile('<{$index}>');" style="width:200px;">
                                            <{/if}>

                                        </td>
                                    <{/if}>

                                    <{/if}>
                                </tr>
                                <{/if}>
                                <{if in_array(3200, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_edit_easyinput_item.tpl" i=$index+3200 j=60 tr=false ime="active"}>
                                    <{if !in_array(3100, $grp_flags) && !in_array(3150, $grp_flags)}>

                                    <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
                                        <td width="25%" rowspan="<{$profile_rowspan}>">

                                            <{if $self_button_flg == true }>
                                                <input type="button" value="本人ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="copyFromSelfProfile('<{$index}>');" style="width:200px;margin-bottom:2px;"><br />
                                            <{/if}>

                                            <{if $other_user_button_flg == true }>
                                                <input type="button" value="他の職員ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="other_profile_copy(<{$index}>);" style="width:200px;"><br />
                                            <{/if}>

                                            <{if $unknown_button_flg == true}>
                                                <input type="button" value="ﾌﾟﾛﾌｨｰﾙ不明" onclick="setUnknownProfile('<{$index}>');" style="width:200px;">
                                            <{/if}>

                                        </td>
                                    <{/if}>

                                    <{/if}>
                                </tr>
                                <{/if}>
                                <{if in_array(3250, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_edit_easyinput_item.tpl" i=$index+3250 j=70 tr=false}>
                                    <{if !in_array(3100, $grp_flags) && !in_array(3150, $grp_flags) && !in_array(3200, $grp_flags)}>

                                    <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
                                        <td width="25%" rowspan="<{$profile_rowspan}>">

                                            <{if $self_button_flg == true }>
                                                <input type="button" value="本人ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="copyFromSelfProfile('<{$index}>');" style="width:200px;margin-bottom:2px;"><br />
                                            <{/if}>

                                            <{if $other_user_button_flg == true }>
                                                <input type="button" value="他の職員ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="other_profile_copy(<{$index}>);" style="width:200px;"><br />
                                            <{/if}>

                                            <{if $unknown_button_flg == true}>
                                                <input type="button" value="ﾌﾟﾛﾌｨｰﾙ不明" onclick="setUnknownProfile('<{$index}>');" style="width:200px;">
                                            <{/if}>

                                        </td>
                                    <{/if}>

                                    <{/if}>
                                </tr>
                                <{include file="hiyari_edit_easyinput_item.tpl" i=$index+3250 j=80 tr=true}>
                                <{/if}>
                                <{if in_array(3300, $grp_flags)}>
                                <tr height="22">
                                    <{include file="hiyari_edit_easyinput_item.tpl" i=$index+3300 j=90 tr=false}>
                                    <{if !in_array(3100, $grp_flags) && !in_array(3150, $grp_flags) && !in_array(3200, $grp_flags) && !in_array(3250, $grp_flags)}>

                                    <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
                                        <td width="25%" rowspan="<{$profile_rowspan}>">

                                            <{if $self_button_flg == true }>
                                                <input type="button" value="本人ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="copyFromSelfProfile('<{$index}>');" style="width:200px;margin-bottom:2px;"><br />
                                            <{/if}>

                                            <{if $other_user_button_flg == true }>
                                                <input type="button" value="他の職員ﾌﾟﾛﾌｨｰﾙからｺﾋﾟｰ" onclick="other_profile_copy(<{$index}>);" style="width:200px;"><br />
                                            <{/if}>

                                            <{if $unknown_button_flg == true}>
                                                <input type="button" value="ﾌﾟﾛﾌｨｰﾙ不明" onclick="setUnknownProfile('<{$index}>');" style="width:200px;">
                                            <{/if}>

                                        </td>
                                    <{/if}>

                                    <{/if}>
                                </tr>
                                <{include file="hiyari_edit_easyinput_item.tpl" i=$index+3300 j=100 tr=true}>
                                <{/if}>
                                <{if in_array(3350, $grp_flags)}>
                                    <{include file="hiyari_edit_easyinput_item.tpl" i=$index+3350 j=110 tr=true colspan=$party_cols-1}>
                                    <{include file="hiyari_edit_easyinput_item.tpl" i=$index+3350 j=115 tr=true colspan=$party_cols-1 ime="active"}>
                                <{/if}>
                                <{if in_array(3400, $grp_flags)}>
                                    <{include file="hiyari_edit_easyinput_item.tpl" i=$index+3400 j=120 tr=true colspan=$party_cols-1}>
                                    <{include file="hiyari_edit_easyinput_item.tpl" i=$index+3400 j=130 tr=true colspan=$party_cols-1 ime="active"}>
                                <{/if}>
                                <{if in_array(3450, $grp_flags)}>
                                    <{include file="hiyari_edit_easyinput_item.tpl" i=$index+3450 j=140 tr=true colspan=$party_cols-1}>
                                <{/if}>
                            </table>
                        </td>
                    </tr>
                    <{/if}>
                <{/section}>
            </table>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="3" alt=""></td></tr>
                <tr bgcolor="#F5FFE5">
                    <td>
                        <div><input type="button" value="  追加  " onclick="rewriteTable('parties')"> <input type="button" value="  削除  " onclick="deleteTable('parties')"></div>
                    </td>
                </tr>
            </table>
        <{/if}>



        <{if array_intersect(array(3050, 3100, 3150, 3200, 3250, 3300, 3350, 3400, 3450), $grp_flags) && in_array(3500, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>




        <{if in_array(3500, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=3500 j=150 tr=true}>
                <{include file="hiyari_edit_easyinput_item.tpl" i=3500 j=160 tr=true ime="active"}>
            </table>
        <{/if}>

    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<br>
<{/if}>


<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ３．誰に                                                                                             -->
<!-- ---------------------------------------------------------------------------------------------------- -->



<{if in_array(200, $grp_flags) || in_array(210, $grp_flags) || in_array(230, $grp_flags) || in_array(240, $grp_flags) || in_array(243, $grp_flags) || in_array(246, $grp_flags) || in_array(250, $grp_flags) || in_array(260, $grp_flags) || in_array(270, $grp_flags) || in_array(280, $grp_flags) || in_array(290, $grp_flags) || in_array(1200, $grp_flags)}>
<table width="100%" cellspacing="0" cellpadding="0">
    <tr><td><font class="j12"><b>誰に</b></font></td></tr>
</table>
<table width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>

    <{if in_array(200, $grp_flags) || in_array(210, $grp_flags)|| in_array(230, $grp_flags) || in_array(240, $grp_flags) || in_array(243, $grp_flags) || in_array(246, $grp_flags) || in_array(250, $grp_flags) || in_array(260, $grp_flags) || in_array(270, $grp_flags) || in_array(280, $grp_flags)}>

        <{assign var="colspan_200_10_t"  value=""}><{assign var="colspan_200_10_f"  value=""}><{assign var="colspan_200_20_t"  value=""}><{assign var="colspan_200_20_f"  value=""}>
        <{assign var="colspan_210_10_t"  value=""}><{assign var="colspan_210_10_f"  value=""}><{assign var="colspan_210_20_t"  value=""}><{assign var="colspan_210_20_f"  value=""}>
        <{assign var="colspan_210_30_t"  value=""}><{assign var="colspan_210_30_f"  value=""}><{assign var="colspan_210_40_t"  value=""}><{assign var="colspan_210_40_f"  value=""}><{assign var="colspan_210_50_t"  value=""}><{assign var="colspan_210_50_f"  value=""}>
        <{assign var="colspan_230_60_t"  value=""}><{assign var="colspan_230_60_f"  value=""}><{assign var="colspan_240_70_t"  value=""}><{assign var="colspan_240_70_f"  value=""}>
        <{assign var="colspan_243_73_t"  value=""}><{assign var="colspan_243_73_f"  value=""}><{assign var="colspan_243_73_e"  value=""}>
        <{assign var="colspan_246_76_t"  value=""}><{assign var="colspan_246_76_f"  value=""}><{assign var="colspan_246_76_e"  value=""}>
        <{assign var="colspan_250_80_t"  value=""}><{assign var="colspan_250_80_f"  value=""}><{assign var="colspan_260_90_t"  value=""}><{assign var="colspan_260_90_f"  value=""}>
        <{assign var="colspan_270_100_t" value=""}><{assign var="colspan_270_100_f" value=""}><{assign var="colspan_280_110_t" value=""}><{assign var="colspan_280_110_f" value=""}>


        <{if in_array(210, $grp_flags)}>

            <{assign var="colspan_210_50_count"  value="0"}>
            <{if in_array(30, $patient_use) && in_array(40, $patient_use) && in_array(50, $patient_use)}>
                <{assign var="colspan_210_50_count"  value="2"}>
            <{/if}>

            <{*患者の数*}>
            <{if in_array(200, $grp_flags)}>
                <{assign var="colspan_200_10_t"  value="1"}>
                <{assign var="colspan_200_10_f"  value="1"}>
                <{assign var="colspan_200_20_t"  value="1"}>
                <{php}>$this->_tpl_vars['colspan_200_20_f'] = $this->_tpl_vars['colspan_210_50_count']+1;<{/php}>
            <{/if}>

            <{*患者プロフィール(患者ID、患者氏名)*}>
            <{if in_array(10, $patient_use) && in_array(20, $patient_use)}>

                <{assign var="colspan_210_10_t"  value="1"}>
                <{assign var="colspan_210_10_f"  value="1"}>
                <{assign var="colspan_210_20_t"  value="1"}>
                <{php}>$this->_tpl_vars['colspan_210_20_f'] = $this->_tpl_vars['colspan_210_50_count']+1;<{/php}>

            <{elseif in_array(10, $patient_use) && !in_array(20, $patient_use)}>

                <{assign var="colspan_210_10_t"  value="1"}>
                <{php}>$this->_tpl_vars['colspan_210_10_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>

            <{elseif !in_array(10, $patient_use) && in_array(20, $patient_use)}>

                <{assign var="colspan_210_20_t"  value="1"}>
                <{php}>$this->_tpl_vars['colspan_210_20_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>

            <{/if}>

            <{*患者プロフィール(患者の性別、患者の年数、患者の月数)*}>
            <{assign var="colspan_210_30_t"  value="1"}>
            <{assign var="colspan_210_30_f"  value="1"}>
            <{assign var="colspan_210_40_t"  value="1"}>
            <{assign var="colspan_210_40_f"  value="1"}>
            <{assign var="colspan_210_50_t"  value="1"}>
            <{assign var="colspan_210_50_f"  value="1"}>

            <{if in_array(30, $patient_use) && !in_array(40, $patient_use) && !in_array(50, $patient_use)}>

                <{php}>$this->_tpl_vars['colspan_210_30_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>

            <{elseif !in_array(30, $patient_use) && in_array(40, $patient_use) && !in_array(50, $patient_use)}>

                <{php}>$this->_tpl_vars['colspan_210_40_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>

            <{elseif !in_array(30, $patient_use) && !in_array(40, $patient_use) && in_array(50, $patient_use)}>

                <{php}>$this->_tpl_vars['colspan_210_50_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>

            <{/if}>

            <{*患者区分１、患者区分２*}>
            <{if in_array(230, $grp_flags) && in_array(240, $grp_flags)}>
                <{assign var="colspan_230_60_t"  value="1"}>
                <{assign var="colspan_230_60_f"  value="1"}>
                <{assign var="colspan_240_70_t"  value="1"}>
                <{php}>$this->_tpl_vars['colspan_240_70_f'] = $this->_tpl_vars['colspan_210_50_count']+1;<{/php}>
            <{elseif in_array(230, $grp_flags)}>
                <{assign var="colspan_230_60_t"  value="1"}>
                <{php}>$this->_tpl_vars['colspan_230_60_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>
            <{elseif in_array(240, $grp_flags)}>
                <{assign var="colspan_240_70_t"  value="1"}>
                <{php}>$this->_tpl_vars['colspan_240_70_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>
            <{/if}>

            <{*主治医*}>
            <{assign var="colspan_243_73_t"  value="1"}>
            <{assign var="colspan_243_73_f"  value="1"}>
            <{php}>$this->_tpl_vars['colspan_243_73_e'] = $this->_tpl_vars['colspan_210_50_count']+2;<{/php}>

            <{*入院日*}>
            <{assign var="colspan_246_76_t"  value="1"}>
            <{assign var="colspan_246_76_f"  value="1"}>
            <{php}>$this->_tpl_vars['colspan_246_76_e'] = $this->_tpl_vars['colspan_210_50_count']+2;<{/php}>

            <{*インシデントに直接関連する疾患名、関連する疾患名１*}>
            <{if in_array(250, $grp_flags) && in_array(260, $grp_flags)}>
                <{assign var="colspan_250_80_t"  value="1"}>
                <{assign var="colspan_250_80_f"  value="1"}>
                <{assign var="colspan_260_90_t"  value="1"}>
                <{php}>$this->_tpl_vars['colspan_260_90_f'] = $this->_tpl_vars['colspan_210_50_count']+1;<{/php}>
            <{elseif in_array(250, $grp_flags)}>
                <{assign var="colspan_250_80_t"  value="1"}>
                <{php}>$this->_tpl_vars['colspan_250_80_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>
            <{elseif in_array(260, $grp_flags)}>
                <{assign var="colspan_260_90_t"  value="1"}>
                <{php}>$this->_tpl_vars['colspan_260_90_f'] = $this->_tpl_vars['colspan_210_50_count']+3;<{/php}>
            <{/if}>

            <{*関連する疾患名２、関連する疾患名３*}>
            <{if in_array(270, $grp_flags) && in_array(280, $grp_flags)}>
                <{assign var="colspan_270_100_t" value="1"}>
                <{assign var="colspan_270_100_f" value="1"}>
                <{assign var="colspan_280_110_t" value="1"}>
                <{php}>$this->_tpl_vars['colspan_280_110_f'] = $this->_tpl_vars['colspan_210_50_count']+1; <{/php}>
            <{elseif in_array(270, $grp_flags)}>
                <{assign var="colspan_270_100_t" value="1"}>
                <{php}>$this->_tpl_vars['colspan_270_100_f'] = $this->_tpl_vars['colspan_210_50_count']+3; <{/php}>
            <{elseif in_array(280, $grp_flags)}>
                <{assign var="colspan_280_110_t" value="1"}>
                <{php}>$this->_tpl_vars['colspan_280_110_f'] = $this->_tpl_vars['colspan_210_50_count']+3;
                <{/php}>
            <{/if}>

        <{elseif in_array(200, $grp_flags) || in_array(230, $grp_flags) || in_array(240, $grp_flags) || in_array(243, $grp_flags) || in_array(246, $grp_flags) || in_array(250, $grp_flags) || in_array(260, $grp_flags) || in_array(270, $grp_flags) || in_array(280, $grp_flags)}>

            <{assign var="colspan_200_10_t"  value=""}><{assign var="colspan_200_10_f"  value=""}><{assign var="colspan_200_20_t"  value=""}><{assign var="colspan_200_20_f"  value=""}>
            <{assign var="colspan_210_10_t"  value=""}><{assign var="colspan_210_10_f"  value=""}><{assign var="colspan_210_20_t"  value=""}><{assign var="colspan_210_20_f"  value=""}>
            <{assign var="colspan_210_30_t"  value=""}><{assign var="colspan_210_30_f"  value=""}><{assign var="colspan_210_40_t"  value=""}><{assign var="colspan_210_40_f"  value=""}><{assign var="colspan_210_50_t"  value=""}><{assign var="colspan_210_50_f"  value=""}>
            <{assign var="colspan_230_60_t"  value=""}><{assign var="colspan_230_60_f"  value=""}><{assign var="colspan_240_70_t"  value=""}><{assign var="colspan_240_70_f"  value=""}>
            <{assign var="colspan_243_73_t"  value=""}><{assign var="colspan_243_73_f"  value=""}><{assign var="colspan_243_73_e"  value=""}>
            <{assign var="colspan_246_76_t"  value=""}><{assign var="colspan_246_76_f"  value=""}><{assign var="colspan_246_76_e"  value=""}>
            <{assign var="colspan_250_80_t"  value=""}><{assign var="colspan_250_80_f"  value=""}><{assign var="colspan_260_90_t"  value=""}><{assign var="colspan_260_90_f"  value=""}>
            <{assign var="colspan_270_100_t" value=""}><{assign var="colspan_270_100_f" value=""}><{assign var="colspan_280_110_t" value=""}><{assign var="colspan_280_110_f" value=""}>

            <{if in_array(200, $grp_flags) || in_array(210, $grp_flags) || (in_array(230, $grp_flags) && in_array(240, $grp_flags)) || (in_array(250, $grp_flags) && in_array(260, $grp_flags)) || (in_array(270, $grp_flags) && in_array(280, $grp_flags))}>

                <{if in_array(200, $grp_flags)}>
                    <{assign var="colspan_200_10_t"  value="1"}>
                    <{assign var="colspan_200_10_f"  value="1"}>
                    <{assign var="colspan_200_20_t"  value="1"}>
                    <{assign var="colspan_200_20_f"  value="1"}>
                <{/if}>

                <{if in_array(230, $grp_flags) && in_array(240, $grp_flags)}>
                    <{assign var="colspan_230_60_t"  value="1"}>
                    <{assign var="colspan_230_60_f"  value="1"}>
                    <{assign var="colspan_240_70_t"  value="1"}>
                    <{assign var="colspan_240_70_f"  value="1"}>
                <{elseif in_array(230, $grp_flags)}>
                    <{assign var="colspan_230_60_t"  value="1"}>
                    <{assign var="colspan_230_60_f"  value="3"}>
                <{elseif in_array(240, $grp_flags)}>
                    <{assign var="colspan_240_70_t"  value="1"}>
                    <{assign var="colspan_240_70_f"  value="3"}>
                <{/if}>

                <{assign var="colspan_243_73_t"  value="1"}>
                <{assign var="colspan_243_73_f"  value="1"}>
                <{assign var="colspan_243_73_e"  value="2"}>

                <{assign var="colspan_246_76_t"  value="1"}>
                <{assign var="colspan_246_76_f"  value="1"}>
                <{assign var="colspan_246_76_e"  value="2"}>

                <{if in_array(250, $grp_flags) && in_array(260, $grp_flags)}>
                    <{assign var="colspan_250_80_t"  value="1"}>
                    <{assign var="colspan_250_80_f"  value="1"}>
                    <{assign var="colspan_260_90_t"  value="1"}>
                    <{assign var="colspan_260_90_f"  value="1"}>
                <{elseif in_array(250, $grp_flags)}>
                    <{assign var="colspan_250_80_t"  value="1"}>
                    <{assign var="colspan_250_80_f"  value="3"}>
                <{elseif in_array(260, $grp_flags)}>
                    <{assign var="colspan_260_90_t"  value="1"}>
                    <{assign var="colspan_260_90_f"  value="3"}>
                <{/if}>

                <{if in_array(270, $grp_flags) && in_array(280, $grp_flags)}>
                    <{assign var="colspan_270_100_t" value="1"}>
                    <{assign var="colspan_270_100_f" value="1"}>
                    <{assign var="colspan_280_110_t" value="1"}>
                    <{assign var="colspan_280_110_f" value="1"}>
                <{elseif in_array(270, $grp_flags)}>
                    <{assign var="colspan_270_100_t" value="1"}>
                    <{assign var="colspan_270_100_f" value="3"}>
                <{elseif in_array(280, $grp_flags)}>
                    <{assign var="colspan_280_110_t" value="1"}>
                    <{assign var="colspan_280_110_f" value="3"}>
                <{/if}>

            <{elseif in_array(243, $grp_flags) || in_array(246, $grp_flags)}>
                <{if in_array(230, $grp_flags) && in_array(240, $grp_flags)}>
                <{elseif in_array(230, $grp_flags)}>
                    <{assign var="colspan_230_60_t"  value="1"}>
                    <{assign var="colspan_230_60_f"  value="2"}>
                <{elseif in_array(240, $grp_flags)}>
                    <{assign var="colspan_240_70_t"  value="1"}>
                    <{assign var="colspan_240_70_f"  value="2"}>
                <{/if}>

                <{assign var="colspan_243_73_t"  value="1"}>
                <{assign var="colspan_243_73_f"  value="1"}>
                <{assign var="colspan_243_73_e"  value="1"}>

                <{assign var="colspan_246_76_t"  value="1"}>
                <{assign var="colspan_246_76_f"  value="1"}>
                <{assign var="colspan_246_76_e"  value="1"}>

                <{if in_array(250, $grp_flags) && in_array(260, $grp_flags)}>
                <{elseif in_array(250, $grp_flags)}>
                    <{assign var="colspan_250_80_t"  value="1"}>
                    <{assign var="colspan_250_80_f"  value="2"}>
                <{elseif in_array(260, $grp_flags)}>
                    <{assign var="colspan_260_90_t"  value="1"}>
                    <{assign var="colspan_260_90_f"  value="2"}>
                <{/if}>

                <{if in_array(270, $grp_flags) && in_array(280, $grp_flags)}>
                <{elseif in_array(270, $grp_flags)}>
                    <{assign var="colspan_270_100_t" value="1"}>
                    <{assign var="colspan_270_100_f" value="2"}>
                <{elseif in_array(280, $grp_flags)}>
                    <{assign var="colspan_280_110_t" value="1"}>
                    <{assign var="colspan_280_110_f" value="2"}>
                <{/if}>
            <{else}>
                <{assign var="colspan_230_60_t"  value="1"}>
                <{assign var="colspan_230_60_f"  value="1"}>
                <{assign var="colspan_240_70_t"  value="1"}>
                <{assign var="colspan_240_70_f"  value="1"}>
                <{assign var="colspan_250_80_t"  value="1"}>
                <{assign var="colspan_250_80_f"  value="1"}>
                <{assign var="colspan_260_90_t"  value="1"}>
                <{assign var="colspan_260_90_f"  value="1"}>
                <{assign var="colspan_270_100_t" value="1"}>
                <{assign var="colspan_270_100_f" value="1"}>
                <{assign var="colspan_280_110_t" value="1"}>
                <{assign var="colspan_280_110_f" value="1"}>
            <{/if}>
        <{/if}>

            <table width="100%" cellspacing="0" cellpadding="2" class="list">

            <{if in_array(200, $grp_flags)}>
                <tr>
                    <td bgcolor="#DFFFDC" width="250" colspan="<{$colspan_200_10_t}>">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=200 j=10}>
                    </td>
                    <td width="300" colspan="<{$colspan_200_10_f}>">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=200 j=10}>
                    </td>

                    <td bgcolor="#DFFFDC" width="150" colspan="<{$colspan_200_20_t}>">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=200 j=20}>
                    </td>
                    <td colspan="<{$colspan_200_20_f}>">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=200 j=20 ime="disabled"}>
                    </td>
                </tr>
            <{/if}>

            <{if in_array(210, $grp_flags)}>
                <{if in_array(10, $patient_use) || in_array(20, $patient_use)}>
                    <tr height="22">
                    <{if in_array(10, $patient_use)}>
                        <td width="195" bgcolor="#DFFFDC">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=210 j=10}>
                        </td>
                        <td width="300" colspan="<{$colspan_210_10_f}>">
                            <nobr>
                            <{include file="hiyari_edit_easyinput_item_field.tpl" i=210 j=10 ime="disabled"}>
                            <{if $patient_profile_flg == 't'}>
                                <{if $kameda_flg}>
                                    <input type="button" value="コピー" onclick="getPtid();">
                                <{elseif $cis_flg}>
                                    <input type="button" value="検索" onclick="cis_patient_search('report_input');">
                                <{elseif $kinkyo_flg}>
                                    <input type="button" value="検索" onclick="kinkyo_patient_search('report_input');">
                                <{elseif $nishiyodo_flg}>
                                    <input type="button" value="検索" onclick="nishiyodo_patient_search('report_input');">
                                <{elseif $gifudaigaku_flg}>
                                    <input type="button" value="検索" onclick="gifudaigaku_patient_search('report_input')">
                                <{elseif $boueiidai_flg}>
                                    <input type="button" value="検索" onclick="boueiidai_patient_search('report_input')">
                                <{elseif $fukushima_flg}>
                                    <input type="button" value="検索" onclick="fukushima_patient_search('report_input')">
                                <{elseif $patient_search_ex_flg}>
                                    <input type="button" value="検索" onclick="patient_search_ex('report_input')">
                                <{elseif $iwata_flg}>
                                    <input type="button" value="検索" onclick="iwata_patient_search('report_input')">
                                <{else}>
                                    <input type="button" value="検索" onclick="patient_search();" <{if $pt_auth != "1"}> disabled<{/if}>>
                                <{/if}>
                            <{/if}>
                            </nobr>
                        </td>
                    <{/if}>
                    <{if in_array(20, $patient_use)}>
                        <td width="150" bgcolor="#DFFFDC" colspan="<{$colspan_200_20_t}>">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=210 j=20}>
                        </td>
                        <td colspan="<{$colspan_210_20_f}>">
                            <{include file="hiyari_edit_easyinput_item_field.tpl" i=210 j=20 ime="active"}>
                        </td>
                    <{/if}>
                    </tr>
                <{/if}>

                <{if in_array(30, $patient_use) || in_array(40, $patient_use) || in_array(50, $patient_use)}>
                    <tr height="22">
                        <{if in_array(30, $patient_use)}>
                            <td width="250" bgcolor="#DFFFDC" colspan="<{$colspan_210_30_t}>">
                                <{include file="hiyari_edit_easyinput_item_title.tpl" i=210 j=30}>
                            </td>
                            <td width="300" colspan="<{$colspan_210_30_f}>">
                                <{include file="hiyari_edit_easyinput_item_field.tpl" i=210 j=30}>
                            </td>
                        <{/if}>
                        <{if in_array(40, $patient_use)}>
                            <td bgcolor="#DFFFDC" width="150" colspan="<{$colspan_210_40_t}>">
                                <{include file="hiyari_edit_easyinput_item_title.tpl" i=210 j=40}>
                            </td>
                            <td width="75" colspan="<{$colspan_210_40_f}>">
                                <{include file="hiyari_edit_easyinput_item_field.tpl" i=210 j=40}>
                            </td>
                        <{/if}>
                        <{if in_array(50, $patient_use)}>
                            <td bgcolor="#DFFFDC" width="150" colspan="<{$colspan_210_50_t}>">
                                <{include file="hiyari_edit_easyinput_item_title.tpl" i=210 j=50}>
                            </td>
                            <td colspan="<{$colspan_210_50_f}>">
                                <{include file="hiyari_edit_easyinput_item_field.tpl" i=210 j=50}>
                            </td>
                        <{/if}>
                    </tr>
                <{/if}>
            <{/if}>

            <{if in_array(230, $grp_flags) || in_array(240, $grp_flags)}>
                <tr>
                <{if in_array(230, $grp_flags) && in_array(240, $grp_flags)}>

                    <td bgcolor="#DFFFDC" width="250" colspan="<{$colspan_230_60_t}>">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=230 j=60}>
                    </td>
                    <td width="300" colspan="<{$colspan_230_60_f}>">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=230 j=60}>
                    </td>

                    <td bgcolor="#DFFFDC" width="150" colspan="<{$colspan_240_70_t}>">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=240 j=70}>
                    </td>
                    <td colspan="<{$colspan_240_70_f}>">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=240 j=70}>
                    </td>

                <{elseif in_array(230, $grp_flags)}>
                    <td bgcolor="#DFFFDC" width="250" colspan="<{$colspan_230_60_t}>">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=230 j=60}>
                    </td>
                    <td colspan="<{$colspan_230_60_f}>">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=230 j=60}>
                    </td>
                <{elseif in_array(240, $grp_flags)}>
                    <td bgcolor="#DFFFDC" width="250" colspan="<{$colspan_240_70_t}>">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=240 j=70}>
                    </td>
                    <td colspan="<{$colspan_240_70_f}>">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=240 j=70}>
                    </td>
                <{/if}>
                </tr>
            <{/if}>

            <{if in_array(243, $grp_flags)}>
                <tr height="22">
                    <td bgcolor="#DFFFDC" width="250" colspan="<{$colspan_243_73_t}>">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=243 j=73 tr=false}>
                    </td>
                    <td width="300" colspan="<{$colspan_243_73_f}>">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=243 j=73 tr=false add_tag_element="onchange='clear_doctor_emp_id();'" ime="active"}>
                    </td>

                    <td colspan="<{$colspan_243_73_e}>" nowrap>
                        <input type="button" value="職員マスターからコピー" onclick="window.open('hiyari_doctor_search.php?session=<{$session}>', 'newwin', 'width=640,height=480,scrollbars=yes');">
                        <font class="j12"><span id="doctor_have_span"></span></font>
                    </td>
                </tr>
            <{/if}>

            <{if in_array(246, $grp_flags) || in_array(1200, $grp_flags)}>
                <tr height="22">
                    <{if in_array(246, $grp_flags)}>
                    <td bgcolor="#DFFFDC" width="250" colspan="<{$colspan_246_76_t}>">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=246 j=76 tr=false}>
                    </td>
                    <td width="300" colspan="<{$colspan_246_76_f}>">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=246 j=76 tr=false}>
                        <img id="caller_246_76" src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer;" onclick="window.open('hiyari_calendar.php?session=<{$session}>&caller=246_76', 'newwin', 'width=640,height=480,scrollbars=yes');"/>
                    </td>
                    <{/if}>
                    <!-- 病棟名 -->
                    <{if in_array(1200, $grp_flags)}>
                        <td bgcolor="#DFFFDC" width="150">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=1200 j=10}>
                        </td>
                        <td>
                            <{include file="hiyari_edit_easyinput_item_field.tpl" i=1200 j=10 ime="active"}>
                        </td>
                    <{/if}>

                    <{if in_array(246, $grp_flags)}>
                    <td colspan="<{$colspan_246_76_e}>">
                        <{if $hospital_ymd_flg == 't'}>
                            <{if $iwata_flg || $patient_search_ex_flg}>
                                <!-- 入院年月日個別取得は行わない -->
                            <{else}>
                                <script>
                                    function inpt_search_open() {
                                        var target_date = false;
                                        /*if(document.getElementById("_100_5")) {
                                            target_date = document.getElementById("_100_5").value;
                                        }
                                        if(document.getElementById("_105_5") && data == "") {
                                            target_date = document.getElementById("_105_5").value;
                                        }
                                        if(target_date == "") {
                                            target_date = false;
                                        }*/
                                        //alert(target_date);
                                        url = 'hiyari_inpatient_search.php?session=<{$session}>&target_date='+target_date;
                                        window.open(url, 'newwin', 'width=640,height=480,scrollbars=yes');
                                    }
                                </script>
                                <input type="button" value="患者マスターからコピー" onclick="JavaScript:inpt_search_open()">
                            <{/if}>
                        <{/if}>
                    </td>
                    <{/if}>

                </tr>
            <{/if}>

            <{if in_array(250, $grp_flags) || in_array(260, $grp_flags) || in_array(270, $grp_flags) || in_array(280, $grp_flags)}>
                <script type="text/javascript">
                    function popupByomeiSelect(id){
                        var ipt = document.getElementById(id).value;
                        window.open(
                            "summary_byoumei_select.php?session=<{$session}>&search_input="+ipt+"&caller="+id,
                            "summary_byoumei_select_window",
                            "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1000,height=700"
                        );
                    }
                    function call_back_summary_byoumei_select(id, result){                        document.getElementById(id).value = result.byoumei;
                    }
                </script>
                <{if in_array(250, $grp_flags) || in_array(260, $grp_flags)}>
                    <tr>

                    <{if in_array(250, $grp_flags) && in_array(260, $grp_flags)}>
                        <td bgcolor="#DFFFDC" width="250" colspan="<{$colspan_250_80_t}>">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=250 j=80}>
                        </td>
                        <td width="300" colspan="<{$colspan_250_80_f}>" style="white-space: nowrap;">
                            <{include file="hiyari_edit_easyinput_item_field.tpl" i=250 j=80 ime="active"}>
                            <{if $byoumei_search_flg == 't'}><input type="button" value="検索" onclick="popupByomeiSelect('id_250_80');"><{/if}>
                        </td>

                        <td bgcolor="#DFFFDC" width="150" colspan="<{$colspan_260_90_t}>">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=260 j=90}>
                        </td>
                        <td colspan="<{$colspan_260_90_f}>" style="white-space: nowrap;">
                            <{include file="hiyari_edit_easyinput_item_field.tpl" i=260 j=90 ime="active"}>
                            <{if $byoumei_search_flg == 't'}><input type="button" value="検索" onclick="popupByomeiSelect('id_260_90');"><{/if}>
                        </td>
                    <{elseif in_array(250, $grp_flags)}>
                        <td bgcolor="#DFFFDC" width="250" colspan="<{$colspan_250_80_t}>">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=250 j=80}>
                        </td>
                        <td colspan="<{$colspan_250_80_f}>" style="white-space: nowrap;">
                            <{include file="hiyari_edit_easyinput_item_field.tpl" i=250 j=80}>
                            <{if $byoumei_search_flg == 't'}><input type="button" value="検索" onclick="popupByomeiSelect('id_250_80');"><{/if}>
                        </td>
                    <{elseif in_array(260, $grp_flags)}>
                        <td bgcolor="#DFFFDC" width="250" colspan="<{$colspan_260_90_t}>">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=260 j=90}>
                        </td>
                        <td colspan="<{$colspan_260_90_f}>" style="white-space: nowrap;">
                            <{include file="hiyari_edit_easyinput_item_field.tpl" i=260 j=90}>
                            <{if $byoumei_search_flg == 't'}><input type="button" value="検索" onclick="popupByomeiSelect('id_260_90');"><{/if}>
                        </td>
                    <{/if}>

                    </tr>
                <{/if}>
                <{if in_array(270, $grp_flags) || in_array(280, $grp_flags)}>
                    <tr>

                    <{if in_array(270, $grp_flags) && in_array(280, $grp_flags)}>
                        <td bgcolor="#DFFFDC" width="250" colspan="<{$colspan_270_100_t}>">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=270 j=100}>
                        </td>
                        <td width="300" colspan="<{$colspan_240_100_f}>" style="white-space: nowrap;">
                            <{include file="hiyari_edit_easyinput_item_field.tpl" i=270 j=100 ime="active"}>
                            <{if $byoumei_search_flg == 't'}><input type="button" value="検索" onclick="popupByomeiSelect('id_270_100');"><{/if}>
                        </td>

                        <td bgcolor="#DFFFDC" width="150" colspan="<{$colspan_280_110_t}>">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=280 j=110}>
                        </td>
                        <td colspan="<{$colspan_280_110_f}>" style="white-space: nowrap;">
                            <{include file="hiyari_edit_easyinput_item_field.tpl" i=280 j=110 ime="active"}>
                            <{if $byoumei_search_flg == 't'}><input type="button" value="検索" onclick="popupByomeiSelect('id_280_110');"><{/if}>
                        </td>
                    <{elseif in_array(270, $grp_flags)}>
                        <td bgcolor="#DFFFDC" width="250" colspan="<{$colspan_270_100_t}>">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=270 j=100}>
                        </td>
                        <td colspan="<{$colspan_270_100_f}>" style="white-space: nowrap;">
                            <{include file="hiyari_edit_easyinput_item_field.tpl" i=270 j=100}>
                            <{if $byoumei_search_flg == 't'}><input type="button" value="検索" onclick="popupByomeiSelect('id_270_100');"><{/if}>
                        </td>
                    <{elseif in_array(280, $grp_flags)}>
                        <td bgcolor="#DFFFDC" width="250" colspan="<{$colspan_280_110_t}>">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=280 j=110}>
                        </td>
                        <td colspan="<{$colspan_280_110_f}>" style="white-space: nowrap;">
                            <{include file="hiyari_edit_easyinput_item_field.tpl" i=280 j=110}>
                            <{if $byoumei_search_flg == 't'}><input type="button" value="検索" onclick="popupByomeiSelect('id_280_110');"><{/if}>
                        </td>
                    <{/if}>

                    </tr>
                <{/if}>
            <{/if}>

            </table>
        <{/if}>

        <{if (in_array(200, $grp_flags) || in_array(210, $grp_flags)|| in_array(230, $grp_flags) || in_array(240, $grp_flags) || in_array(243, $grp_flags) || in_array(246, $grp_flags) || in_array(250, $grp_flags) || in_array(260, $grp_flags) || in_array(270, $grp_flags) || in_array(280, $grp_flags)) && in_array(290, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(290, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=290 j=120 tr=true}>
                <{include file="hiyari_edit_easyinput_item.tpl" i=290 j=130 tr=true ime="active"}>
            </table>
            <{if in_array(295, $grp_flags)}>
                <{foreach from=$grps[295].easy_item_list key="easy_item_code_295" item="tmp_295_data"}>
                    <div id="area_295_<{$easy_item_code_295}>" style="display:none">
                        <table width="100%" cellspacing="0" cellpadding="2" class="list">
                            <tr height="22">
                                <td width=250 bgcolor="#DFFFDC" style="border-top:0px;">
                                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=295 j=$easy_item_code_295}>
                                </td>
                                <td style="border-top:0px;">
                                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=295 j=$easy_item_code_295}>
                                </td>
                            </tr>
                        </table>
                    </div>
                <{/foreach}>
            <{/if}>
        <{/if}>

    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<br>
<{/if}>


<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ４．何をどうした                                                                                     -->
<!-- ---------------------------------------------------------------------------------------------------- -->

<{if in_array(120, $grp_flags) || in_array(130, $grp_flags) || in_array(140, $grp_flags) || in_array(700, $grp_flags) || in_array(900, $grp_flags) || in_array(1300, $grp_flags) || in_array(1310, $grp_flags) || in_array(1320, $grp_flags)}>
<table width="100%" cellspacing="0" cellpadding="0">
    <tr><td><font class="j12"><b>何をどうした</b></font></td></tr>
</table>
<table width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>

        <{if in_array(120, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=120 j=80 tr=true}>
            </table>
            <{if $grps[120].easy_item_list[80].relate_group != "" and in_array(400, $grp_flags)}>
            <div id="rel_<{$grps[120].easy_item_list[80].relate_group}>"></div>
                <{if $grps[120].easy_item_list[80].easy_item_type == 'select'}>
                    <script type="text/javascript">
                        relate_detail(document.FRM_MAIN['_120_80'], 'rel_400');
                    </script>
                <{/if}>
            <{/if}>
        <{/if}>


        <{if in_array(700, $grp_flags)}>

<script type="text/javascript">
<!--//
function cng_scene(id) {
    <{foreach from=$scene key=key item=val}>
    if(id == <{$key}>) {
        document.FRM_MAIN._710_10.length=<{$val|@count}>+1;
        document.FRM_MAIN._710_10.options[0] = new Option("","");
    <{foreach from=$val key=key2 item=val2}>
        document.FRM_MAIN._710_10.options[<{$key2+1}>] = new Option("<{$val2.item_name}>","<{$val2.item_id}>");
        <{if $vals[710][10][0] == $val2.item_id}>
            document.FRM_MAIN._710_10.options[<{$key2+1}>].selected = true;
        <{/if}>
    <{/foreach}>
    }
    <{/foreach}>
}

function cng_scene_item(id) {
    <{foreach from=$scene_item key=key item=val}>
    if(id == <{$key}>) {
        document.FRM_MAIN._720_10.length=<{$val|@count}>+1;
        document.FRM_MAIN._720_10.options[0] = new Option("","");
        <{foreach from=$val key=key2 item=val2}>
            document.FRM_MAIN._720_10.options[<{$key2+1}>] = new Option("<{$val2.sub_item_name}>","<{$val2.sub_item_id}>");
            <{if $vals[720][10][0] == $val2.sub_item_id}>
                document.FRM_MAIN._720_10.options[<{$key2+1}>].selected = true;
            <{/if}>
        <{/foreach}>
    }
    <{/foreach}>
}

function cng_kind(id) {
    <{foreach from=$kind key=key item=val}>
    if(id == <{$key}>) {
        document.FRM_MAIN._740_10.length=<{$val|@count}>+1;
        document.FRM_MAIN._740_10.options[0] = new Option("","");
        <{foreach from=$val key=key2 item=val2}>
            document.FRM_MAIN._740_10.options[<{$key2+1}>] = new Option("<{$val2.item_name}>","<{$val2.item_id}>");
            <{if $vals[740][10][0] == $val2.item_id}>
                document.FRM_MAIN._740_10.options[<{$key2+1}>].selected = true;
            <{/if}>
        <{/foreach}>
    }
    <{/foreach}>
}

function cng_kind_item(id) {
    <{foreach from=$kind_item key=key item=val}>
    if(id == <{$key}>) {
        document.FRM_MAIN._750_10.length=<{$val|@count}>+1;
        document.FRM_MAIN._750_10.options[0] = new Option("","");
        <{foreach from=$val key=key2 item=val2}>
            document.FRM_MAIN._750_10.options[<{$key2+1}>] = new Option("<{$val2.sub_item_name}>","<{$val2.sub_item_id}>");
            <{if $vals[750][10][0] == $val2.sub_item_id}>
                document.FRM_MAIN._750_10.options[<{$key2+1}>].selected = true;
            <{/if}>
        <{/foreach}>
    }
    <{/foreach}>
}

function cng_content(id) {
    <{foreach from=$content key=key item=val}>
    if(id == <{$key}>) {
        document.FRM_MAIN._770_10.length=<{$val|@count}>+1;
        document.FRM_MAIN._770_10.options[0] = new Option("","");
    <{foreach from=$val key=key2 item=val2}>
        document.FRM_MAIN._770_10.options[<{$key2+1}>] = new Option("<{$val2.item_name}>","<{$val2.item_id}>");
        <{if $vals[770][10][0] == $val2.item_id}>
            document.FRM_MAIN._770_10.options[<{$key2+1}>].selected = true;
        <{/if}>
    <{/foreach}>
    }
    <{/foreach}>
}
function cng_content_item(id) {
    <{foreach from=$content_item key=key item=val}>
    if(id == <{$key}>) {
        document.FRM_MAIN._780_10.length=<{$val|@count}>+1;
        document.FRM_MAIN._780_10.options[0] = new Option("","");
        <{foreach from=$val key=key2 item=val2}>
            document.FRM_MAIN._780_10.options[<{$key2+1}>] = new Option("<{$val2.sub_item_name}>","<{$val2.sub_item_id}>");
        <{if $vals[780][10][0] == $val2.sub_item_id}>
            document.FRM_MAIN._780_10.options[<{$key2+1}>].selected = true;
        <{/if}>
        <{/foreach}>
    }
    <{/foreach}>
}

function reset_item() {
    document.FRM_MAIN._720_10.length=0;
    document.FRM_MAIN._750_10.length=0;
    document.FRM_MAIN._780_10.length=0;
    if(document.FRM_MAIN._700_10.selectedIndex == 0) {
        document.FRM_MAIN._710_10.length=0;
        document.FRM_MAIN._740_10.length=0;
        document.FRM_MAIN._770_10.length=0;
    } else {
        document.FRM_MAIN._710_10.options[0].selected = true;
        document.FRM_MAIN._740_10.options[0].selected = true;
        document.FRM_MAIN._770_10.options[0].selected = true;
    }
}

function disp_item(id) {
    ele1 = document.getElementById("add_item_1");
    ele2 = document.getElementById("add_item_2");
    ele3 = document.getElementById("add_item_3");

    if(ele1 && id != 3) ele1.style.display = 'none';
    if(ele2 && id != 6) ele2.style.display = 'none';
    if(ele3 && id != 8) ele3.style.display = 'none';

    if(ele1 && id == 3) ele1.style.display = '';
    if(ele2 && id == 6) ele2.style.display = '';
    if(ele3 && id == 8) ele3.style.display = '';
}

-->
</script>

            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
            <{include file="hiyari_edit_easyinput_item.tpl" i=700 j=10 tr=true}>
            </table>

        <div id="scene_content">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
            <{include file="hiyari_edit_easyinput_item.tpl" i=710 j=10 tr=true}>
            <{include file="hiyari_edit_easyinput_item.tpl" i=720 j=10 tr=true td_title_style="text-align:right"}>
            <{include file="hiyari_edit_easyinput_item.tpl" i=730 j=10 tr=true td_title_style="text-align:right"}>
            </table>

            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
            </table>
            <{* 薬剤・製剤の種類は必要なときだけ表示 *}>
            <table id="kind_disp" width="100%" cellspacing="0" cellpadding="2" class="list">
            <{include file="hiyari_edit_easyinput_item.tpl" i=740 j=10 tr=true}>
            <{include file="hiyari_edit_easyinput_item.tpl" i=750 j=10 tr=true td_title_style="text-align:right"}>
            <{include file="hiyari_edit_easyinput_item.tpl" i=760 j=10 tr=true td_title_style="text-align:right"}>
            </table>

            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
            <{include file="hiyari_edit_easyinput_item.tpl" i=770 j=10 tr=true}>
            <{include file="hiyari_edit_easyinput_item.tpl" i=780 j=10 tr=true td_title_style="text-align:right"}>
            <{include file="hiyari_edit_easyinput_item.tpl" i=790 j=10 tr=true td_title_style="text-align:right"}>
            </table>
        </div>
        <{/if}>

        <{if in_array(800, $grp_flags)}>
        <div id="add_item_1">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
            <!--// 調剤・製剤管理等に関する項目 START -->
            <table width="100%" cellspacing="0" cellpadding="2" class="list">

            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>
                    <font class="j12"></font>
                </td>
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>
                    <font class="j12">関連した薬剤</font>
                </td>
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>
                    <font class="j12">医薬品の取り違え事例の場合、<br>本来投与すべき薬剤</font>

                </td>
            </tr>
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>
                    <font class="j12">販売名</font>
                </td>
                <td width="150" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_800_5" name="_800_5" value="<{$vals[800][5][0]}>" size="60" style="ime-mode:active"></font>
                </td>

                <td width="150" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_800_10" name="_800_10" value="<{$vals[800][10][0]}>" size="60" style="ime-mode:active"></font>
                </td>
            </tr>
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>
                    <font class="j12">剤型</font>
                </td>
                <td width="150" align="center" bgcolor="#FFFFFF" nowrap>

                    <font class="j12"><input type="text" id="id_800_15" name="_800_15" value="<{$vals[800][15][0]}>" size="60" style="ime-mode:active"></font>
                </td>
                <td width="150" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_800_20" name="_800_20" value="<{$vals[800][20][0]}>" size="60" style="ime-mode:active"></font>
                </td>
            </tr>
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>
                    <font class="j12">規格単位（含有量、濃度）</font>

                </td>
                <td width="150" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_800_25" name="_800_25" value="<{$vals[800][25][0]}>" size="60" style="ime-mode:active"></font>
                </td>
                <td width="150" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_800_30" name="_800_30" value="<{$vals[800][30][0]}>" size="60" style="ime-mode:active"></font>
                </td>
            </tr>
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>

                    <font class="j12">製造（輸入販売）業者名</font>
                </td>
                <td width="150" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_800_35" name="_800_35" value="<{$vals[800][35][0]}>" size="60" style="ime-mode:active"></font>
                </td>
                <td width="150" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_800_40" name="_800_40" value="<{$vals[800][40][0]}>" size="60" style="ime-mode:active"></font>
                </td>

            </tr>
            </table>
            <!--// 調剤・製剤管理等に関する項目 END -->
        </div>
        <div id="add_item_2">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
            <!--// 医療用具（機器）の使用・管理に関する項目 START -->
            <table width="100%" cellspacing="0" cellpadding="2" class="list">

            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>
                    <font class="j12"></font>
                </td>
                <td width="300" align="center" bgcolor="#DFFFDC" nowrap>
                    <font class="j12">○医療・歯科医療用具（機器）○諸物品</font>
                </td>
            </tr>
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>

                    <font class="j12">販売名</font>
                </td>
                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_800_45" name="_800_45" value="<{$vals[800][45][0]}>" size="120" style="ime-mode:active"></font>
                </td>
            </tr>
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>
                    <font class="j12">製造（輸入販売）業者名</font>

                </td>
                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_800_50" name="_800_50" value="<{$vals[800][50][0]}>" size="120" style="ime-mode:active"></font>
                </td>
            </tr>
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>
                    <font class="j12">購入年</font>
                </td>

                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_800_55" name="_800_55" value="<{$vals[800][55][0]}>" size="120" style="ime-mode:active"></font>
                </td>
            </tr>
            </table>
            <!--// 医療用具（機器）の使用・管理に関する項目 END -->
        </div>
        <div id="add_item_3">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
            <!--// 歯科医療用具（機器）・材料の使用・管理に関する項目 START -->
            <table width="100%" cellspacing="0" cellpadding="2" class="list">

            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>
                    <font class="j12"></font>
                </td>
                <td width="300" align="center" bgcolor="#DFFFDC" nowrap>
                    <font class="j12">○医療・歯科医療用具（機器）○諸物品</font>
                </td>
            </tr>
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>

                    <font class="j12">販売名</font>
                </td>
                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_800_60" name="_800_60" value="<{$vals[800][60][0]}>" size="120" style="ime-mode:active"></font>
                </td>
            </tr>
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>
                    <font class="j12">製造（輸入販売）業者名</font>

                </td>
                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_800_65" name="_800_65" value="<{$vals[800][65][0]}>" size="120" style="ime-mode:active"></font>
                </td>
            </tr>
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>
                    <font class="j12">購入年</font>
                </td>

                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_800_70" name="_800_70" value="<{$vals[800][70][0]}>" size="120" style="ime-mode:active"></font>
                </td>
            </tr>
            </table>
            <!--// 歯科医療用具（機器）・材料の使用・管理に関する項目 END -->
        </div>
        <{/if}>



        <!--// 2010年版 START -->
        <{if in_array(900, $grp_flags)}>

            <script type="text/javascript">
            <!--//
            function cng_kind_2010(id) {
                document.FRM_MAIN._910_10.length=1;
                document.FRM_MAIN._910_10.options[0] = new Option("","");
               
                <{foreach from=$kind_2010 key=key item=val}>
                if(id == <{$key}>) {
                    document.FRM_MAIN._910_10.length=<{$val|@count}>+1;
                    document.FRM_MAIN._910_10.options[0] = new Option("","");
                    <{foreach from=$val key=key2 item=val2}>
                        document.FRM_MAIN._910_10.options[<{$key2+1}>] = new Option("<{$val2.item_name}>","<{$val2.item_id}>");
                        <{if $vals[910][10][0] == $val2.item_id}>
                            document.FRM_MAIN._910_10.options[<{$key2+1}>].selected = true;
                        <{/if}>
                    <{/foreach}>
                }
                <{/foreach}>
            }

            function cng_kind_item_2010(id) {
                <{foreach from=$kind_item_2010 key=key item=val}>
                if(id == <{$key}>) {
                    document.FRM_MAIN._920_10.length=<{$val|@count}>+1;
                    document.FRM_MAIN._920_10.options[0] = new Option("","");
                    <{foreach from=$val key=key2 item=val2}>
                        document.FRM_MAIN._920_10.options[<{$key2+1}>] = new Option("<{$val2.sub_item_name}>","<{$val2.sub_item_id}>");
                        <{if $vals[920][10][0] == $val2.sub_item_id}>
                            document.FRM_MAIN._920_10.options[<{$key2+1}>].selected = true;
                        <{/if}>
                    <{/foreach}>
                }
                <{/foreach}>
            }
            function cng_scene_2010(id) {
                
                document.FRM_MAIN._940_10.length=1;
                document.FRM_MAIN._940_10.options[0] = new Option("","");
                
                <{foreach from=$scene_2010 key=key item=val}>
                if(id == <{$key}>) {
                    document.FRM_MAIN._940_10.length=<{$val|@count}>+1;
                    document.FRM_MAIN._940_10.options[0] = new Option("","");
                    <{foreach from=$val key=key2 item=val2}>
                        document.FRM_MAIN._940_10.options[<{$key2+1}>] = new Option("<{$val2.item_name}>","<{$val2.item_id}>");
                        <{if $vals[940][10][0] == $val2.item_id}>
                            document.FRM_MAIN._940_10.options[<{$key2+1}>].selected = true;
                        <{/if}>
                    <{/foreach}>
                }
                <{/foreach}>
            }
            function cng_scene_item_2010(id) {
                <{foreach from=$scene_item_2010 key=key item=val}>
                if(id == <{$key}>) {
                    document.FRM_MAIN._950_10.length=<{$val|@count}>+1;
                    document.FRM_MAIN._950_10.options[0] = new Option("","");
                    <{foreach from=$val key=key2 item=val2}>
                        document.FRM_MAIN._950_10.options[<{$key2+1}>] = new Option("<{$val2.sub_item_name}>","<{$val2.sub_item_id}>");
                        <{if $vals[950][10][0] == $val2.sub_item_id}>
                            document.FRM_MAIN._950_10.options[<{$key2+1}>].selected = true;
                        <{/if}>
                    <{/foreach}>
                }
                <{/foreach}>
            }
            function cng_content_2010(id) {
            
                document.FRM_MAIN._970_10.length=1;
                document.FRM_MAIN._970_10.options[0] = new Option("","");
            
                <{foreach from=$content_2010 key=key item=val}>
                if(id == <{$key}>) {
                    document.FRM_MAIN._970_10.length=<{$val|@count}>+1;
                    document.FRM_MAIN._970_10.options[0] = new Option("","");
                    <{foreach from=$val key=key2 item=val2}>
                        document.FRM_MAIN._970_10.options[<{$key2+1}>] = new Option("<{$val2.item_name}>","<{$val2.item_id}>");
                        <{if $vals[970][10][0] == $val2.item_id}>
                            document.FRM_MAIN._970_10.options[<{$key2+1}>].selected = true;
                        <{/if}>
                    <{/foreach}>
                }
                <{/foreach}>
            }
            function cng_content_item_2010(id) {
                <{foreach from=$content_item_2010 key=key item=val}>
                if(id == <{$key}>) {
                    document.FRM_MAIN._980_10.length=<{$val|@count}>+1;
                    document.FRM_MAIN._980_10.options[0] = new Option("","");
                    <{foreach from=$val key=key2 item=val2}>
                        document.FRM_MAIN._980_10.options[<{$key2+1}>] = new Option("<{$val2.sub_item_name}>","<{$val2.sub_item_id}>");
                        <{if $vals[980][10][0] == $val2.sub_item_id}>
                            document.FRM_MAIN._980_10.options[<{$key2+1}>].selected = true;
                        <{/if}>
                    <{/foreach}>
                }
                <{/foreach}>
            }
            function reset_item_2010() {
                document.FRM_MAIN._920_10.length=0;
                document.FRM_MAIN._950_10.length=0;
                document.FRM_MAIN._980_10.length=0;
                if(document.FRM_MAIN._900_10.selectedIndex == 0) {
                    document.FRM_MAIN._910_10.length=0;
                    document.FRM_MAIN._940_10.length=0;
                    document.FRM_MAIN._970_10.length=0;
                } else {
                    document.FRM_MAIN._910_10.options[0].selected = true;
                    document.FRM_MAIN._940_10.options[0].selected = true;
                    document.FRM_MAIN._970_10.options[0].selected = true;
                }
            }

            function reset_kind_2010() {
                document.FRM_MAIN._910_10.length=1
                document.FRM_MAIN._910_10.options[0] = new Option("","");
            }
            function disp_item_2010(id) {
                if (id != "") {
                        obj = document.getElementsByName("_900_10_" + id + "_hdn");
                        id = obj[0].value;
                }
                <{if in_array(1300, $grp_flags)}>
                    ele1 = document.getElementById("add_item_2010_1");
                    if(id != 1) ele1.style.display = 'none';
                    if(id == 1) ele1.style.display = '';
                <{/if}>
                <{if in_array(1310, $grp_flags)}>
                    ele2 = document.getElementById("add_item_2010_2");
                    if(id != 3 || id != 5 || id != 7 || id != 4 || id != 6) ele2.style.display = 'none';
                    if(id == 3 || id == 5 || id == 7 || id == 4 || id == 6) ele2.style.display = '';
                <{/if}>
                <{if in_array(1320, $grp_flags)}>
                    ele3 = document.getElementById("add_item_2010_3");
                    if(id != 4 || id != 6) ele3.style.display = 'none';
                    if(id == 4 || id == 6) ele3.style.display = '';
                <{/if}>
                if(id == 7) {
                    <{if $must_item_disp_mode == 2 && in_array("1310_10", $item_must)}>
                        if(obj = document.getElementById("must_write_1000_20")) obj.style.backgroundColor = '';
                        if(obj = document.getElementById("must_write_1000_25")) obj.style.backgroundColor = '';
                        if(obj = document.getElementById("must_write_1000_30")) obj.style.backgroundColor = '';
                    <{/if}>
                    <{if $must_item_disp_mode == 1 && in_array("1310_10", $item_must)}>
                        if(obj2 = document.getElementById("must_write2_1000_20")) obj2.innerHTML = '';
                        if(obj2 = document.getElementById("must_write2_1000_25")) obj2.innerHTML = '';
                        if(obj2 = document.getElementById("must_write2_1000_30")) obj2.innerHTML = '';
                    <{/if}>
                } else {
                    <{if $must_item_disp_mode == 2 && (in_array("1300_10", $item_must) || in_array("1310_10", $item_must) || in_array("1320_10", $item_must))}>
                        if(obj = document.getElementById("must_write_1000_20")) obj.style.backgroundColor = '#FFDDFD';
                        if(obj = document.getElementById("must_write_1000_25")) obj.style.backgroundColor = '#FFDDFD';
                        if(obj = document.getElementById("must_write_1000_30")) obj.style.backgroundColor = '#FFDDFD';
                    <{/if}>
                    <{if $must_item_disp_mode == 1 && (in_array("1300_10", $item_must) || in_array("1310_10", $item_must) || in_array("1320_10", $item_must))}>
                        if(obj2 = document.getElementById("must_write2_1000_20")) obj2.innerHTML = '＊';
                        if(obj2 = document.getElementById("must_write2_1000_25")) obj2.innerHTML = '＊';
                        if(obj2 = document.getElementById("must_write2_1000_30")) obj2.innerHTML = '＊';
                    <{/if}>
                }
            }

            function cng_item_2010(OBJ) {
                var n    = OBJ._940_10.selectedIndex;
                var TEXT = OBJ._940_10.options[n].text;
                var OBJ2 = OBJ._970_10;
                var num  = OBJ2.options.length;

                for(i=0; num > i; i++) {
                    if(TEXT == OBJ2.options[i].text) {
                        OBJ2.options[i].selected = true;
                        cng_content_item_2010(document.FRM_MAIN._970_10.value);
                    }
                }
            }
            function delete_detail_item_2010(OBJ) {
                <{if in_array(1300, $grp_flags)}>
                    OBJ._1000_5.value = '';
                    //OBJ._1000_10.value = '';
                    OBJ._1000_15.value = '';
                <{/if}>
                <{if in_array(1310, $grp_flags)}>
                    OBJ._1000_20.value = '';
                    OBJ._1000_25.value = '';
                    OBJ._1000_30.value = '';
                <{/if}>
                <{if in_array(1320, $grp_flags)}>
                    OBJ._1000_35.value = '';
                    OBJ._1000_40.value = '';
                    OBJ._1000_45.value = '';
                    OBJ._1000_50.value = '';
                    OBJ._1000_55.value = '';
                <{/if}>
            }

            function hide_must_2010() {
                if(document.getElementById('id_900_10').value == 8) {
                    <{if in_array('900_10', $item_must)}><{if $must_item_disp_mode == 1}>
                        if(obj = document.getElementById('id_910_10_must')) {
                            obj.style.display = 'none';
                        }

                        if(obj = document.getElementById('id_920_10_must')) {
                            obj.style.display = 'none';
                        }

                        if(obj = document.getElementById('id_930_10_must')) {
                            obj.style.display = '';
                        }

                        if(obj = document.getElementById('id_940_10_must')) {
                            obj.style.display = 'none';
                        }

                        if(obj = document.getElementById('id_950_10_must')) {
                            obj.style.display = 'none';
                        }

                        if(obj = document.getElementById('id_960_10_must')) {
                            obj.style.display = '';
                        }

                        if(obj = document.getElementById('id_970_10_must')) {
                            obj.style.display = 'none';
                        }

                        if(obj = document.getElementById('id_980_10_must')) {
                            obj.style.display = 'none';
                        }

                        if(obj = document.getElementById('id_990_10_must')) {
                            obj.style.display = '';
                        }

                    <{/if}>
                    <{/if}>
                    <{if in_array('900_10', $item_must)}><{if $must_item_disp_mode == 2}>
                    if(obj = document.getElementById('id_910_10_color')) {
                        obj.style.backgroundColor = '';
                    }

                    if(obj = document.getElementById('id_920_10_color')) {
                        obj.style.backgroundColor = '';
                    }

                    if(obj = document.getElementById('id_930_10_color')) {
                        obj.style.backgroundColor = '#FFDDFD';
                    }

                    if(obj = document.getElementById('id_940_10_color')) {
                        obj.style.backgroundColor = '';
                    }

                    if(obj = document.getElementById('id_950_10_color')) {
                        obj.style.backgroundColor = '';
                    }

                    if(obj = document.getElementById('id_960_10_color')) {
                        obj.style.backgroundColor = '#FFDDFD';
                    }

                    if(obj = document.getElementById('id_970_10_color')) {
                        obj.style.backgroundColor = '';
                    }

                    if(obj = document.getElementById('id_980_10_color')) {
                        obj.style.backgroundColor = '';
                    }

                    if(obj = document.getElementById('id_990_10_color')) {
                        obj.style.backgroundColor = '#FFDDFD';
                    }

                                        <{/if}><{/if}>
                                    } else {
                                        <{if in_array('900_10', $item_must)}><{if $must_item_disp_mode == 1}>
                                        if(obj = document.getElementById('id_910_10_must')) {
                        obj.style.display = '';
                    }

                    if(obj = document.getElementById('id_920_10_must')) {
                        obj.style.display = '';
                    }

                    if(obj = document.getElementById('id_930_10_must')) {
                        obj.style.display = 'none';
                    }

                    if(obj = document.getElementById('id_940_10_must')) {
                        obj.style.display = '';
                    }

                    if(obj = document.getElementById('id_950_10_must')) {
                        obj.style.display = '';
                    }

                    if(obj = document.getElementById('id_960_10_must')) {
                        obj.style.display = 'none';
                    }

                    if(obj = document.getElementById('id_970_10_must')) {
                        obj.style.display = '';
                    }

                    if(obj = document.getElementById('id_980_10_must')) {
                        obj.style.display = '';
                    }

                    if(obj = document.getElementById('id_990_10_must')) {
                        obj.style.display = 'none';
                    }

                                        <{/if}><{/if}>
                    <{if in_array('900_10', $item_must)}><{if $must_item_disp_mode == 2}>
                                        if(obj = document.getElementById('id_910_10_color')) {
                        obj.style.backgroundColor = '#FFDDFD';
                    }

                    if(obj = document.getElementById('id_920_10_color')) {
                        obj.style.backgroundColor = '#FFDDFD';
                    }

                    if(obj = document.getElementById('id_930_10_color')) {
                        obj.style.backgroundColor = '';
                    }

                    if(obj = document.getElementById('id_940_10_color')) {
                        obj.style.backgroundColor = '#FFDDFD';
                    }

                    if(obj = document.getElementById('id_950_10_color')) {
                        obj.style.backgroundColor = '#FFDDFD';
                    }

                    if(obj = document.getElementById('id_960_10_color')) {
                        obj.style.backgroundColor = '';
                    }

                    if(obj = document.getElementById('id_970_10_color')) {
                        obj.style.backgroundColor = '#FFDDFD';
                    }

                    if(obj = document.getElementById('id_980_10_color')) {
                        obj.style.backgroundColor = '#FFDDFD';
                    }

                    if(obj = document.getElementById('id_990_10_color')) {
                        obj.style.backgroundColor = '';
                    }

                                        <{/if}><{/if}>
                                    }
            }
                        -->
            </script>

            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
            <{include file="hiyari_edit_easyinput_item.tpl" i=900 j=10 tr=true}>
            <{include file="hiyari_edit_easyinput_item.tpl" i=900 j=20 tr=true ime="active"}>
            </table>

        <div id="scene_content_2010">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
            </table>
            <{* 種類は必要なときだけ表示 *}>
            <table id="kind_disp_2010" width="100%" cellspacing="0" cellpadding="2" class="list">
            <{include file="hiyari_edit_easyinput_item.tpl" i=910 j=10 tr=true}>
            <{include file="hiyari_edit_easyinput_item.tpl" i=920 j=10 tr=true td_title_style="text-align:right"}>
            <{include file="hiyari_edit_easyinput_item.tpl" i=930 j=10 tr=true td_title_style="text-align:right"  ime="active"}>
            </table>

            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
            <{include file="hiyari_edit_easyinput_item.tpl" i=940 j=10 tr=true}>
            <{include file="hiyari_edit_easyinput_item.tpl" i=950 j=10 tr=true td_title_style="text-align:right"}>
            <{include file="hiyari_edit_easyinput_item.tpl" i=960 j=10 tr=true td_title_style="text-align:right"  ime="active"}>
            </table>

            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
            <{include file="hiyari_edit_easyinput_item.tpl" i=970 j=10 tr=true}>
            <{include file="hiyari_edit_easyinput_item.tpl" i=980 j=10 tr=true td_title_style="text-align:right"}>
            <{include file="hiyari_edit_easyinput_item.tpl" i=990 j=10 tr=true td_title_style="text-align:right"  ime="active"}>
            </table>
        </div>
        <{/if}>

        <{if in_array(1300, $grp_flags)}>
        <div id="add_item_2010_1">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
            <!--// START -->
            <table width="100%" cellspacing="0" cellpadding="2" class="list">

            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>

                    <font class="j12" <{if in_array('1300_10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>関連医薬品の販売名<{if in_array('1300_10', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></font>
                </td>
                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_1000_5" name="_1000_5" value="<{$vals[1000][5][0]}>" size="120" style="ime-mode:active"></font>
                </td>
            </tr>
            <!--<tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>

                    <font class="j12">コード</font>
                </td>
                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_1000_10" name="_1000_10" value="<{$vals[1000][10][0]}>" size="120"></font>
                </td>
            </tr>-->
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>

                    <font class="j12" <{if in_array('1300_10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>製造販売業者名<{if in_array('1300_10', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></font>
                </td>
                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_1000_15" name="_1000_15" value="<{$vals[1000][15][0]}>" size="120" style="ime-mode:active"></font>
                </td>
            </tr>

            </table>
            <!--// END -->
        </div>
        <{/if}>
        <{if in_array(1310, $grp_flags)}>
        <div id="add_item_2010_2">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
            <!--// START -->
            <table width="100%" cellspacing="0" cellpadding="2" class="list">

            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>

                    <font id="must_write_1000_20"  size="3" class="j12" <{if in_array('1310_10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>医療材料・諸物品等の販売名<{if in_array('1310_10', $item_must)}><{if $must_item_disp_mode == 1}><span id="must_write2_1000_20" style="color:red">＊</span><{/if}><{/if}></font>
                </td>
                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_1000_20" name="_1000_20" value="<{$vals[1000][20][0]}>" size="120" style="ime-mode:active"></font>
                </td>
            </tr>
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>

                    <font id="must_write_1000_25" size="3" class="j12" <{if in_array('1310_10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>製造販売業者名<{if in_array('1310_10', $item_must)}><{if $must_item_disp_mode == 1}><span id="must_write2_1000_25" style="color:red">＊</span><{/if}><{/if}></font>
                </td>
                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_1000_25" name="_1000_25" value="<{$vals[1000][25][0]}>" size="120" style="ime-mode:active"></font>
                </td>
            </tr>
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>

                    <font id="must_write_1000_30" size="3" class="j12" <{if in_array('1310_10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>購入年月日<{if in_array('1310_10', $item_must)}><{if $must_item_disp_mode == 1}><span id="must_write2_1000_30" style="color:red">＊</span><{/if}><{/if}></font>
                </td>
                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_1000_30" name="_1000_30" value="<{$vals[1000][30][0]}>" size="120" style="ime-mode:active"></font>
                </td>
            </tr>

            </table>
            <!--// END -->
        </div>
        <{/if}>
        <{if in_array(1320, $grp_flags)}>
        <div id="add_item_2010_3">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
            <!--// START -->
            <table width="100%" cellspacing="0" cellpadding="2" class="list">

            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>

                    <font class="j12" <{if in_array('1320_10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>医療機器等の販売名<{if in_array('1320_10', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></font>
                </td>
                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_1000_35" name="_1000_35" value="<{$vals[1000][35][0]}>" size="120" style="ime-mode:active"></font>
                </td>
            </tr>
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>

                    <font class="j12" <{if in_array('1320_10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>製造販売業者名<{if in_array('1320_10', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></font>
                </td>
                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_1000_40" name="_1000_40" value="<{$vals[1000][40][0]}>" size="120" style="ime-mode:active"></font>
                </td>
            </tr>
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>

                    <font class="j12" <{if in_array('1320_10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>製造年月<{if in_array('1320_10', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></font>
                </td>
                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_1000_45" name="_1000_45" value="<{$vals[1000][45][0]}>" size="120" style="ime-mode:active"></font>
                </td>
            </tr>
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>

                    <font class="j12" <{if in_array('1320_10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>購入年月<{if in_array('1320_10', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></font>
                </td>
                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_1000_50" name="_1000_50" value="<{$vals[1000][50][0]}>" size="120" style="ime-mode:active"></font>
                </td>
            </tr>
            <tr height="22">
                <td width="150" align="center" bgcolor="#DFFFDC" nowrap>

                    <font class="j12" <{if in_array('1320_10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>直近の保守・点検年月<{if in_array('1320_10', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></font>
                </td>
                <td width="300" align="center" bgcolor="#FFFFFF" nowrap>
                    <font class="j12"><input type="text" id="id_1000_55" name="_1000_55" value="<{$vals[1000][55][0]}>" size="120" style="ime-mode:active"></font>
                </td>
            </tr>

            </table>
            <!--// END -->
        </div>
        <{/if}>
        <!--// 2010年版 END -->

        <{if in_array(120, $grp_flags) && in_array(140, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(140, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">

<{* アセスメント・患者の状態*}>
<{if $org_cd == '0132070936'}>
<script type="text/javascript">
<!--//
function patient_search_nerima() {

    // 患者IDの要素が無い
    if ( document.getElementById("id_210_10") == null ) {
        confirm('報告書様式に患者IDの項目がありません');
    }

    // 患者IDの要素がある
    else {
        // 患者IDを取得
        var patientId = document.getElementById("id_210_10").value;
        if (patientId == '') {
            confirm('患者IDが入力されていません');
        }
        else {
            // 0パディング（10桁）
            patientId = ("0000000000" + patientId).slice(-10);

            // URL指定
            var url = 'http://10.10.56.11/cgi-bin/cds/start.folder.tcl?patientid='+patientId;

            // ウインドウ属性指定
            var dx = screen.width;
            var dy = screen.top;
            var base = 0;
            var wx = 720;
            var wy = 600;

            // ウインドウ表示
            childwin = window.open(url, null);
            childwin.focus();
        }
    }
}
-->
</script>
<{/if}>

<tr height="22">
<td width="250" bgcolor="#DFFFDC">
<{include file="hiyari_edit_easyinput_item_title.tpl" i=140 j=10}>
<{if $org_cd == '0132070936'}>
<input type="button" id="nerima_patient_search" name="nerima_patient_search" value="確認" onclick="patient_search_nerima();" />
<{/if}>
</td>

<td>
<{foreach from=$grps[140].easy_item_list[10].easy_list item=e140_item}>
<{assign var="e140_code" value=$e140_item.easy_code}>
<{assign var="v140" value=$vals[140][10]}>
<{if in_array($e140_code, $asses_use)}>
    <div>
    <label>
        <input  type='checkbox'
                id="140_10_<{$e140_code}>"
                class="140_10"
                name="_140_10[]" value="<{$e140_code}>"
                <{if in_array($e140_code,(array)@$v140)}>checked<{/if}>
        >
        <font class="j12"><{$e140_item.easy_name|escape:"html"}>：</font>
    </label>

<{* アセスメント・患者の状態詳細 *}>
    <{assign var="e150" value=$grps[150].easy_item_list[$e140_code]}>
    <{assign var="v150" value=$vals[150][$e140_code]}>
    <{if $e150.easy_item_type != 'select'}>
        <{foreach from=$e150.easy_list item=e150_item}>
            <{assign var="e150_code" value=$e150_item.easy_code}>
            <{if $item_element_no_disp[150][$e140_code] == "" || !in_array($e150_code, $item_element_no_disp[150][$e140_code])}>
                <label>
                <input  type="radio"
                        id="id_150_<{$e140_code}>_<{$e150_code}>"
                        class="150_<{$e140_code}>"
                        name="_150_<{$e140_code}>[]"
                        value="<{$e150_code}>"
                        <{if in_array($e150_code, (array)@$v150)}>checked<{/if}>
                        <{if !in_array($e140_code,(array)@$v140)}>disabled<{/if}>
                >
                <font class="j12"><{$e150_item.easy_name|escape:"html"}></font>
                </label>
            <{/if}>
        <{/foreach}>

        <{* ミトンあり詳細 *}>
        <{if $e140_code == 20}>
            <div id="detail_150_20" style="margin:3px 0 10px 20px;">
            <{foreach from=$grps[160].easy_item_list[10].easy_list item=e160_item}>
            <{if !in_array($e160_item.easy_code,$item_element_no_disp[160][10])}>
            <label style="white-space:nowrap;">
            <input  type='checkbox'
                    name="_160_10[]"
                    id="150_20_10_<{$e160_item.easy_code}>"
                    class="150_20_10"
                    value="<{$e160_item.easy_code}>"
                    <{if in_array($e160_item.easy_code, (array)@$vals[160][10])}>checked<{/if}>
                    <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "20"}>disabled<{/if}>
            >
            <font class="j12"><{$e160_item.easy_name|escape:"html"}></font>
            </label>
            <{/if}>
            <{/foreach}>
            </div>
        <{/if}>

        <{* リスク回避用具 *}>
        <{if $e140_code == 6}>
            <div id="detail_150_6" style="margin:3px 0 10px 20px;">
            <{foreach from=$grps[170].easy_item_list[10].easy_list item=e170_item}>
            <{if !in_array($e170_item.easy_code,$item_element_no_disp[170][10])}>
            <label style="white-space:nowrap;">
            <input  type='checkbox'
                    name="_170_10[]"
                    id="150_6_10_<{$e170_item.easy_code}>"
                    class="150_6_10"
                    value="<{$e170_item.easy_code}>"
                    <{if in_array($e170_item.easy_code, (array)@$vals[170][10])}>checked<{/if}>
                    <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10"}>disabled<{/if}>
            >
            <font class="j12"><{$e170_item.easy_name|escape:"html"}></font>
            </label>
            <{if $e170_item.easy_code == 99}>
            <input type="text" class="150_6_20" id="150_6_20" name="_170_20" value="<{$vals[170][20][0]}>" style="ime-mode:active" <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10" || !in_array(99,(array)@$vals[170][10])}>disabled<{/if}> />
            <{/if}>
            <{/if}>
            <{/foreach}>
            </div>
        <{/if}>

        <{* 拘束用具 *}>
        <{if $e140_code == 7}>
            <div id="detail_150_7" style="margin:3px 0 10px 20px;">
            <{foreach from=$grps[180].easy_item_list[10].easy_list item=e180_item}>
            <{if !in_array($e180_item.easy_code,$item_element_no_disp[180][10])}>
            <label style="white-space:nowrap;">
            <input  type='checkbox'
                    name="_180_10[]"
                    id="150_7_10_<{$e180_item.easy_code}>"
                    class="150_7_10"
                    value="<{$e180_item.easy_code}>"
                    <{if in_array($e180_item.easy_code, (array)@$vals[180][10])}>checked<{/if}>
                    <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10"}>disabled<{/if}>
            >
            <font class="j12"><{$e180_item.easy_name|escape:"html"}></font>
            </label>
            <{if $e180_item.easy_code == 99}>
            <input type="text" class="150_7_20" id="150_7_20" name="_180_20" value="<{$vals[180][20][0]}>" style="ime-mode:active" <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10" || !in_array(99,(array)@$vals[180][10])}>disabled<{/if}> />
            <{/if}>
            <{/if}>
            <{/foreach}>
            </div>
        <{/if}>

        <{* 身体拘束行為 *}>
        <{if $e140_code == 8}>
            <div id="detail_150_8" style="margin:3px 0 10px 20px;">
            <{foreach from=$grps[190].easy_item_list[10].easy_list item=e190_item}>
            <{if !in_array($e190_item.easy_code,$item_element_no_disp[190][10])}>
            <label style="white-space:nowrap;">
            <input  type='checkbox'
                    name="_190_10[]"
                    id="150_8_10_<{$e190_item.easy_code}>"
                    class="150_8_10"
                    value="<{$e190_item.easy_code}>"
                    <{if in_array($e190_item.easy_code, (array)@$vals[190][10])}>checked<{/if}>
                    <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10"}>disabled<{/if}>
            >
            <font class="j12"><{$e190_item.easy_name|escape:"html"}></font>
            </label>
            <{if $e190_item.easy_code == 99}>
            <input type="text" class="150_8_20" id="150_8_20" name="_190_20" value="<{$vals[190][20][0]}>" style="ime-mode:active" <{if !in_array($e140_code,(array)@$v140) || $v150[0] != "10" || !in_array(99,(array)@$vals[190][10])}>disabled<{/if}>  />
            <{/if}>
            <{/if}>
            <{/foreach}>
            </div>
        <{/if}>

    <{else}>
        <select name="_150_<{$e140_code}>"
                class="150_<{$e140_code}>"
                id="id_150_<{$e140_code}>_0"
                <{if !in_array($e140_code, (array)@$vals[140][10])}>disabled<{/if}>
        >
            <option value=""></option>
            <{foreach from=$e150.easy_list item=e150_list}>
            <{if $item_element_no_disp[150][$e140_code] == "" || !in_array($e150_list.easy_code,$item_element_no_disp[150][$e140_code])}>
            <option value="<{$e150_list.easy_code}>"
            <{if in_array($e150_list.easy_code, (array)@$vals[150][$e140_code])}>selected<{/if}>
            >
            <{$e150_list.easy_name|escape:"html"}>
            <{/if}>
            <{/foreach}>
        </select>
    <{/if}>
</div>
<{/if}>
<{/foreach}>
</td>
</tr>
            </table>
        <{/if}>

        <{if (in_array(120, $grp_flags) || in_array(140, $grp_flags)) && in_array(130, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(130, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=130 j=90 tr=true}>
                <{include file="hiyari_edit_easyinput_item.tpl" i=130 j=100 tr=true ime="active"}>
            </table>
        <{/if}>


    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<br>
<{/if}>


























<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ５．インシデントの内容                                                                               -->
<!-- ---------------------------------------------------------------------------------------------------- -->


<{if in_array(500, $grp_flags) || in_array(520, $grp_flags) || in_array(525, $grp_flags) || in_array(530, $grp_flags) || in_array(570, $grp_flags) || in_array(1400, $grp_flags) || in_array(4000, $grp_flags)}>



    <table width="100%" cellspacing="0" cellpadding="0">
        <tr><td><font class="j12"><b><!--インシデントの-->内容</b></font></td></tr>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
      </tr>
      <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td>



        <{if in_array(500, $grp_flags) || in_array(520, $grp_flags) || in_array(525, $grp_flags) || in_array(530, $grp_flags)}>
            <{assign var="ta1_array_list" value=""}>
            <{php}>
                $wk_arr = array();
                $wk_arr[] = array(500,10);
                $wk_arr[] = array(520,30);
                $wk_arr[] = array(525,10);
                $wk_arr[] = array(530,40);
                $this->_tpl_vars['ta1_array_list'] = $wk_arr;

            <{/php}>
            <{assign var="ta1_1_use" value=false}>
            <{assign var="ta1_2_use" value=false}>
            <{assign var="ta1_3_use" value=false}>
            <{assign var="ta1_4_use" value=false}>
            <{foreach item=ta1_array from=$ta1_array_list}>
                <{assign var="ta1_grp_work" value=$ta1_array[0]}>
                <{assign var="ta1_item_work" value=$ta1_array[1]}>
                <{if in_array($ta1_grp_work, $grp_flags)}>
                    <{if !$ta1_1_use}>
                        <{assign var="ta1_1_use" value=true}>
                        <{assign var="ta1_1_grp" value=$ta1_grp_work}>
                        <{assign var="ta1_1_item" value=$ta1_item_work}>
                    <{elseif !$ta1_2_use}>
                        <{assign var="ta1_2_use" value=true}>
                        <{assign var="ta1_2_grp" value=$ta1_grp_work}>
                        <{assign var="ta1_2_item" value=$ta1_item_work}>
                    <{elseif !$ta1_3_use}>
                        <{assign var="ta1_3_use" value=true}>
                        <{assign var="ta1_3_grp" value=$ta1_grp_work}>
                        <{assign var="ta1_3_item" value=$ta1_item_work}>
                    <{else}>
                        <{assign var="ta1_4_use" value=true}>
                        <{assign var="ta1_4_grp" value=$ta1_grp_work}>
                        <{assign var="ta1_4_item" value=$ta1_item_work}>
                    <{/if}>
                <{/if}>
            <{/foreach}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">

            <{if $ta1_1_use || $ta1_2_use}>
                <tr>
                    <td bgcolor="#DFFFDC" width="10%">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=$ta1_1_grp j=$ta1_1_item}>
                    </td>
                    <td width="40%">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=$ta1_1_grp j=$ta1_1_item style=big  ime="active"}>
                    </td>
                <{if $ta1_2_use}>
                    <td bgcolor="#DFFFDC" width="10%">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=$ta1_2_grp j=$ta1_2_item}>
                    </td>
                    <td>
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=$ta1_2_grp j=$ta1_2_item style=big ime="active"}>
                    </td>
                <{else}>
                    <td colspan="2"></td>
                <{/if}>
                </tr>
            <{/if}>

            <{if $ta1_3_use}>
                <tr>
                    <td bgcolor="#DFFFDC" width="10%">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=$ta1_3_grp j=$ta1_3_item}>
                    </td>
                    <td width="40%">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=$ta1_3_grp j=$ta1_3_item style=big  ime="active"}>
                    </td>
                    <td colspan="2"></td>
                </tr>
            <{/if}>

            <{if $ta1_4_use}>
                <tr>
                    <td bgcolor="#DFFFDC" width="10%">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=$ta1_4_grp j=$ta1_4_item}>
                    </td>
                    <td width="40%">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=$ta1_4_grp j=$ta1_4_item style=big  ime="active"}>
                    </td>
                    <td colspan="2"></td>
                </tr>
            <{/if}>

            </table>
        <{/if}>


        <{if (in_array(500, $grp_flags) || in_array(520, $grp_flags) || in_array(525, $grp_flags) || in_array(530, $grp_flags)) && in_array(4000, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

<{* 事例の詳細（時系列）4000〜4049
   Smartyにロジック書かれすぎてわけわからないので、
       hiyari_edit_easyinput_item.tpl
       hiyari_edit_easyinput_item_field.tpl
       hiyari_edit_easyinput_item_string.tpl
   とか使わないでやってます。
*}>
        <{if in_array(4000, $grp_flags)}>

            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <thead>
                <tr>
                    <td bgcolor="#DFFFDC" colspan="3"><font size="3" class="j12">事例の詳細(時系列)</font></td>
                    <td bgcolor="#DFFFDC" align="right"><input type="button" value="行を追加する" onclick="addTable_jirei();"> <input type="button" value="最後の行を削除する" onclick="deleteTable_jirei();"></td>
                </tr>
                <tr>
                    <td bgcolor="#DFFFDC"><font size="3" class="j12">No</font></td>
                    <td bgcolor="#DFFFDC" width="300">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=4000 j=1}>
                    </td>
                    <td bgcolor="#DFFFDC" width="250">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=4000 j=4}>
                    </td>
                    <td bgcolor="#DFFFDC">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=4000 j=5}>
                    </td>
                </tr>
                </thead>
                <tbody id="tbody_4000">
                <{section name=tbody_4000 loop=50}>
                    <{assign var="index" value=$smarty.section.tbody_4000.index}>
                    <{math assign=i4000 equation="x + y" x=$index y=4000}>
                    <{if (array_intersect(array($i4000), $grp_flags) || array_intersect(array($i4000), array_keys($vals))) && ($i4000 == 4000 || !empty($vals[$i4000][1][0]) || !empty($vals[$i4000][2][0]) || !empty($vals[$i4000][3][0]) || !empty($vals[$i4000][4][0]) || !empty($vals[$i4000][5][0]) || !empty($vals[$i4000][6][0]))}>
                    <{math assign=i4000_no equation="xx + yy" xx=$index yy=1}>
                    <tr>
                        <td><font size="3" class="j12"><{$i4000_no}></font></td>
                        <td><font size="3" class="j12">
                            <input type="text" id="id_<{$i4000}>_1" name="_<{$i4000}>_1" value="<{$vals[$i4000][1][0]}>" style="width:80px;border:#35B341 solid 2px;" readonly>
                            <img id="caller_<{$i4000}>_1" src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer;" onclick="window.open('hiyari_calendar.php?session=<{$session}>&caller=<{$i4000}>_1', 'newwin', 'width=640,height=480,scrollbars=yes');"/>
                            <button type="button" onclick="document.getElementById('id_<{$i4000}>_1').value = '';">クリア</button>

                            <select id="id_<{$i4000}>_2" name="_<{$i4000}>_2" onchange="selectbox_onchange(this);">
                            <option value=""></option>
                            <{section name=k2 loop=$grps[$i4000].easy_item_list[2].easy_list}>
                                <{assign var="easy_4000_2" value=$grps[$i4000].easy_item_list[2].easy_list[k2]}>
                                <{if $item_element_no_disp[$i4000][2] == "" || !in_array($easy_4000_2.easy_code, $item_element_no_disp[$i4000][2])}>
                                <option value="<{$easy_4000_2.easy_code}>" <{if in_array($easy_4000_2.easy_code, (array)@$vals[$i4000][2])}> selected<{/if}>><{$easy_4000_2.easy_name|escape:"html"}></option>
                                <{/if}>
                            <{/section}>
                            </select>時

                            <select id="id_<{$i4000}>_3" name="_<{$i4000}>_3" onchange="selectbox_onchange(this);">
                            <option value=""></option>
                            <{section name=k3 loop=$grps[$i4000].easy_item_list[3].easy_list}>
                                <{assign var="easy_4000_3" value=$grps[$i4000].easy_item_list[3].easy_list[k3]}>
                                <{if $item_element_no_disp[$i4000][3] == "" || !in_array($easy_4000_3.easy_code, $item_element_no_disp[$i4000][3])}>
                                <option value="<{$easy_4000_3.easy_code}>" <{if in_array($easy_4000_3.easy_code, (array)@$vals[$i4000][3])}> selected<{/if}>><{$easy_4000_3.easy_name|escape:"html"}></option>
                                <{/if}>
                            <{/section}>
                            </select>分</font>
                        </td>
                        <td>
                            <select id="id_<{$i4000}>_4" name="_<{$i4000}>_4" onchange="selectbox_onchange(this);">
                            <option value=""></option>
                            <{section name=k4 loop=$grps[$i4000].easy_item_list[4].easy_list}>
                                <{assign var="easy_4000_4" value=$grps[$i4000].easy_item_list[4].easy_list[k4]}>
                                <{if $item_element_no_disp[$i4000][4] == "" || !in_array($easy_4000_4.easy_code, $item_element_no_disp[$i4000][4])}>
                                <option value="<{$easy_4000_4.easy_code}>" <{if in_array($easy_4000_4.easy_code, (array)@$vals[$i4000][4])}> selected<{/if}>><{$easy_4000_4.easy_name|escape:"html"}></option>
                                <{/if}>
                            <{/section}>
                            </select>
                            <input type="text" name="_<{$i4000}>_6" value="<{$vals[$i4000][6][0]|escape:"html"}>" style="ime-mode:active;"/>
                        </td>
                        <td>
                            <textarea style="width:100%;height:50px;ime-mode:active;" rows="2" name="_<{$i4000}>_5"><{$vals[$i4000][5][0]|escape:"html"}></textarea>
                        </td>
                    </tr>
                    <{/if}>
                <{/section}>
                </tbody>
                <tfoot>
                <tr>
                    <td bgcolor="#DFFFDC" colspan="4" align="right"><input type="button" value="行を追加する" onclick="addTable_jirei();"> <input type="button" value="最後の行を削除する" onclick="deleteTable_jirei();"></td>
                </tr>
                </tfoot>
            </table>

        <{/if}>

        <{if (in_array(500, $grp_flags) || in_array(520, $grp_flags) || in_array(525, $grp_flags) || in_array(530, $grp_flags) || in_array(4000, $grp_flags)) && in_array(570, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(570, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=570 j=5 tr=true}>
            </table>
            <div id="area_570_1" style="display:none">
                <table width="100%" cellspacing="0" cellpadding="2" class="list">
                    <{include file="hiyari_edit_easyinput_item.tpl" i=570 j=11 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                    <{include file="hiyari_edit_easyinput_item.tpl" i=570 j=12 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                </table>
            </div>
            <div id="area_570_2" style="display:none">
                <table width="100%" cellspacing="0" cellpadding="2" class="list">
                    <{include file="hiyari_edit_easyinput_item.tpl" i=570 j=21 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                    <tr height="22">
                        <td width=250 bgcolor="#DFFFDC" style="border-top:0px;">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=570 j=22}>
                        </td>
                        <td style="border-top:0px;">
                            <font class="j12">
                            約
                            </font>
                            <{include file="hiyari_edit_easyinput_item_field.tpl" i=570 j=22 text_field_width=50}>
                            <font class="j12">
                            cc
                            </font>
                        </td>
                    </tr>
                    <{include file="hiyari_edit_easyinput_item.tpl" i=570 j=23 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                </table>
            </div>
            <div id="area_570_3" style="display:none">
                <table width="100%" cellspacing="0" cellpadding="2" class="list">
                    <{include file="hiyari_edit_easyinput_item.tpl" i=570 j=30 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                    <{include file="hiyari_edit_easyinput_item.tpl" i=570 j=31 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                    <{include file="hiyari_edit_easyinput_item.tpl" i=570 j=32 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                </table>
            </div>
            <div id="area_570_4" style="display:none">
                <table width="100%" cellspacing="0" cellpadding="2" class="list">
                    <{include file="hiyari_edit_easyinput_item.tpl" i=570 j=41 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                    <{include file="hiyari_edit_easyinput_item.tpl" i=570 j=42 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                    <{include file="hiyari_edit_easyinput_item.tpl" i=570 j=43 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                    <{include file="hiyari_edit_easyinput_item.tpl" i=570 j=44 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                </table>
            </div>
        <{/if}>



        <{if (in_array(500, $grp_flags) || in_array(520, $grp_flags) || in_array(525, $grp_flags) || in_array(530, $grp_flags)) || in_array(570, $grp_flags) && in_array(1400, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(1400, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=1400 j=10 tr=true}>
            </table>
            <div id="id_1400_20_ic" style="display:none;">
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=1400 j=20 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
            </table>
            </div>
            <{foreach item=influence key=key from=$ic_influence}>
            <div id="id_1400_30_ic_<{$key}>" style="display:none;">
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=1400 j=30 tr=true ic=$influence key=$key td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
            </table>
            </div>
            <{/foreach}>
            <div id="id_1400_40_ic" style="display:none;">
                <table width="100%" cellspacing="0" cellpadding="2" class="list">
                    <tr height="22">
                        <td width=250 bgcolor="#DFFFDC" style="border-top:0px;">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=1400 j=40}>
                        </td>
                        <td style="border-top:0px;">
                            <font class="j12">
                            約
                            </font>
                            <{include file="hiyari_edit_easyinput_item_field.tpl" i=1400 j=40 text_field_width=50}>
                            <font class="j12">
                            cc
                            </font>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="id_1400_50_ic" style="display:none;">
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=1400 j=50 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
            </table>
            </div>
            <{foreach item=mental key=key from=$ic_mental}>
            <div id="id_1400_60_ic_<{$key}>" style="display:none;">
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=1400 j=60 tr=true ic=$mental key=$key td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
            </table>
            </div>
            <{/foreach}>
            <div id="id_1400_70_ic" style="display:none;">
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=1400 j=70 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
            </table>
            </div>
        <{/if}>




        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
      </tr>
      <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
      </tr>
    </table>
    <br>
<{/if}>











<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ６．インシデントへの対応                                                                             -->
<!-- ---------------------------------------------------------------------------------------------------- -->

<{if in_array(540, $grp_flags) || in_array(550, $grp_flags) || in_array(580, $grp_flags) || in_array(1410, $grp_flags)}>

    <table width="100%" cellspacing="0" cellpadding="0">
        <tr><td><font class="j12"><b><!--インシデントへの-->対応</b></font></td></tr>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
      </tr>
      <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td>


        <{if in_array(540, $grp_flags) || in_array(550, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">

                <tr>

                <{if in_array(540, $grp_flags) && in_array(550, $grp_flags)}>
                    <td bgcolor="#DFFFDC" width="10%" rowspan=2>
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=540 j=50}>
                    </td>
                    <td width="40%" rowspan=2>
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=540 j=50 style=big ime="active"}>
                    </td>
                    <td bgcolor="#DFFFDC" width="10%">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=550 j=55}>
                    </td>
                    <td>
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=550 j=55 ime="active"}>
                    </td>
                </tr><tr>
                    <td bgcolor="#DFFFDC" width="10%">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=550 j=60}>
                    </td>
                    <td>
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=550 j=60 style=genmensoti ime="active"}>
                    </td>
                <{elseif in_array(540, $grp_flags)}>
                    <td bgcolor="#DFFFDC" width="10%">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=540 j=50}>
                    </td>
                    <td width="40%">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=540 j=50 style=big ime="active"}>
                    </td>
                    <td colspan="2"></td>
                <{else}>
                    <td bgcolor="#DFFFDC" width="10%">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=550 j=55}>
                    </td>
                    <td width="40%">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=550 j=55  ime="active"}>
                    </td>
                    <td colspan="2" rowspan="2"></td>
                </tr><tr>
                    <td bgcolor="#DFFFDC" width="10%">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=550 j=60}>
                    </td>
                    <td width="40%">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=550 j=60 style=genmensoti ime="active"}>
                    </td>
                <{/if}>

                </tr>

            </table>
        <{/if}>




        <{if (in_array(540, $grp_flags) || in_array(550, $grp_flags)) && in_array(580, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>



        <{if in_array(580, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=580 j=5 tr=true}>
            </table>
            <div id="area_580_1" style="display:none">
                <table width="100%" cellspacing="0" cellpadding="2" class="list">
                    <{include file="hiyari_edit_easyinput_item.tpl" i=580 j=11 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                    <{include file="hiyari_edit_easyinput_item.tpl" i=580 j=12 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                    <{include file="hiyari_edit_easyinput_item.tpl" i=580 j=13 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                </table>
            </div>
            <div id="area_580_2" style="display:none">
                <table width="100%" cellspacing="0" cellpadding="2" class="list">
                    <{include file="hiyari_edit_easyinput_item.tpl" i=580 j=21 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                    <{include file="hiyari_edit_easyinput_item.tpl" i=580 j=22 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                    <{include file="hiyari_edit_easyinput_item.tpl" i=580 j=23 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                </table>
            </div>
            <div id="area_580_3" style="display:none">
                <table width="100%" cellspacing="0" cellpadding="2" class="list">
                    <{include file="hiyari_edit_easyinput_item.tpl" i=580 j=31 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                    <{include file="hiyari_edit_easyinput_item.tpl" i=580 j=32 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                    <{include file="hiyari_edit_easyinput_item.tpl" i=580 j=33 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                </table>
            </div>
            <div id="area_580_4" style="display:none">
                <table width="100%" cellspacing="0" cellpadding="2" class="list">
                    <{include file="hiyari_edit_easyinput_item.tpl" i=580 j=41 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                </table>
            </div>
        <{/if}>


        <{if (in_array(540, $grp_flags) || in_array(550, $grp_flags)) || in_array(580, $grp_flags) && in_array(1410, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>


        <{if in_array(1410, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=1410 j=10 tr=true}>
            </table>
            <{foreach item=correspondence key=key from=$ic_correspondence}>
            <div id="id_1410_20_ic_<{$key}>" style="display:none;">
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=1410 j=20 tr=true ic=$correspondence key=$key td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
            </table>
            </div>
            <{/foreach}>
            <{foreach item=ic_item key=key2 from=$ic_sub_item}>
                <{foreach item=sub_item key=key from=$ic_item}>
                <div id="id_1410_30_ic_<{$key}>" style="display:none;">
                    <table width="100%" cellspacing="0" cellpadding="2" class="list">
                        <{include file="hiyari_edit_easyinput_item.tpl" i=1410 j=30 tr=true ic=$sub_item parent_ic=$ic_parent_item key=$key td_title_style="border-top:0px;" td_field_style="border-top:0px;" }>
                    </table>
                </div>
                <{/foreach}>
            <{/foreach}>
            <div id="id_1410_40_ic" style="display:none;">
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=1410 j=40 tr=true td_title_style="border-top:0px;" td_field_style="border-top:0px;"  ime="active"}>
            </table>
            </div>
        <{/if}>

        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
      </tr>
      <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
      </tr>
    </table>
    <br>
<{/if}>













<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ７．インシデントの分析                                                                               -->
<!-- ---------------------------------------------------------------------------------------------------- -->

<{if in_array(125, $grp_flags) || in_array(650, $grp_flags) || in_array(90, $grp_flags) || in_array(96, $grp_flags) || in_array(600, $grp_flags) || in_array(510, $grp_flags) || in_array(590, $grp_flags) || in_array(610, $grp_flags) || in_array(620, $grp_flags) || in_array(625, $grp_flags) || in_array(630, $grp_flags) || in_array(635, $grp_flags) || in_array(690, $grp_flags) || in_array(1500, $grp_flags) || in_array(1502, $grp_flags) || in_array(1504, $grp_flags) || in_array(1510, $grp_flags) || in_array(1520, $grp_flags) || in_array(1530, $grp_flags)}>
<table width="100%" cellspacing="0" cellpadding="0">
    <tr><td><font class="j12"><b><!--インシデントの-->分析</b></font></td></tr>
</table>
<table width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>
        <{if in_array(125, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=125 j=85 tr=true}>
                <{include file="hiyari_edit_easyinput_item.tpl" i=125 j=86 tr=true ime="active"}>
            </table>
        <{/if}>
        <{if in_array(650, $grp_flags)}>
            <{if in_array(125, $grp_flags)}>
                <table width="100%" cellspacing="0" cellpadding="0">
                    <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
                </table>
            <{/if}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=650 j=50 tr=true}>
            </table>
        <{/if}>

        <{if in_array(125, $grp_flags) || in_array(650, $grp_flags) && (in_array(90, $grp_flags) || in_array(96, $grp_flags) || in_array(600, $grp_flags) || in_array(510, $grp_flags) || in_array(590, $grp_flags) || in_array(610, $grp_flags) || in_array(620, $grp_flags) || in_array(625, $grp_flags) || in_array(630, $grp_flags) || in_array(635, $grp_flags) || in_array(690, $grp_flags))}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(90, $grp_flags) || in_array(96, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
            <{if in_array(90, $grp_flags)}>
                <tr height="22">
                    <{include file="hiyari_edit_easyinput_item.tpl" i=90 j=10 tr=false}>
                </tr>
            <{/if}>
            <{if in_array(96, $grp_flags)}>
                <tr height="22">
                    <{include file="hiyari_edit_easyinput_item.tpl" i=96 j=20 tr=false}>
                </tr>
            <{/if}>
            </table>
        <{/if}>

        <{if (in_array(90, $grp_flags) || in_array(96, $grp_flags)) && (in_array(1500, $grp_flags) || in_array(125, $grp_flags) || in_array(1502, $grp_flags) || in_array(1504, $grp_flags) || in_array(1510, $grp_flags) || in_array(1520, $grp_flags) || in_array(1530, $grp_flags))}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(1500, $grp_flags) || in_array(1502, $grp_flags) || in_array(1504, $grp_flags) || in_array(1510, $grp_flags) || in_array(1520, $grp_flags) || in_array(1530, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
            <{* 1500: リスクの評価: 重要性 *}>
            <{if in_array(1500, $grp_flags)}>
                <tr height="22">
                    <{include file="hiyari_edit_easyinput_item.tpl" i=1500 j=10 tr=false}>
                </tr>
            <{/if}>

            <{* 1502: リスクの評価: 緊急性 *}>
            <{if in_array(1502, $grp_flags)}>
                <tr height="22">
                    <{include file="hiyari_edit_easyinput_item.tpl" i=1502 j=10 tr=false}>
                </tr>
            <{/if}>

            <{* 1504: リスクの評価: 頻度 *}>
            <{if in_array(1504, $grp_flags)}>
                <tr height="22">
                    <{include file="hiyari_edit_easyinput_item.tpl" i=1504 j=10 tr=false}>
                </tr>
            <{/if}>

            <{* 1510: リスクの予測 *}>
            <{if in_array(1510, $grp_flags)}>
                <tr height="22">
                    <{include file="hiyari_edit_easyinput_item.tpl" i=1510 j=10 tr=false}>
                </tr>
            <{/if}>

            <{* 1520: システム改善の必要性 *}>
            <{if in_array(1520, $grp_flags)}>
                <tr height="22">
                    <{include file="hiyari_edit_easyinput_item.tpl" i=1520 j=10 tr=false}>
                </tr>
            <{/if}>

            <{* 1530: 教育研修への活用 *}>
            <{if in_array(1530, $grp_flags)}>
                <tr height="22">
                    <{include file="hiyari_edit_easyinput_item.tpl" i=1530 j=10 tr=false}>
                </tr>
            <{/if}>
            </table>
        <{/if}>

        <{if (in_array(1500, $grp_flags) || in_array(1502, $grp_flags) || in_array(1504, $grp_flags) || in_array(1510, $grp_flags) || in_array(1520, $grp_flags) || in_array(1530, $grp_flags)) && (in_array(600, $grp_flags) || in_array(510, $grp_flags) || in_array(590, $grp_flags) || in_array(610, $grp_flags) || in_array(620, $grp_flags) || in_array(625, $grp_flags) || in_array(630, $grp_flags) || in_array(635, $grp_flags) || in_array(690, $grp_flags))}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(600, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=600 j=10 tr=true}>
                <{include file="hiyari_edit_easyinput_item.tpl" i=600 j=20 tr=true ime="active"}>
            </table>

            <{if in_array(605, $grp_flags)}>
                <{foreach from=$grps[605].easy_item_list key="easy_item_code_605" item="tmp_605_data"}>
                    <{if $easy_item_code_605 < 100 || $easy_item_code_605 > 1000}>
                    <div id="area_605_<{$easy_item_code_605}>" style="display:none">
                        <table width="100%" cellspacing="0" cellpadding="2" class="list">
                            <tr height="22">
                                <td width=250 bgcolor="#DFFFDC" style="border-top:0px;">
                                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=605 j=$easy_item_code_605}>
                                </td>
                                <td style="border-top:0px;">
                                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=605 j=$easy_item_code_605}>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <{* ここから下、その他の内容コードを入れる *}>
                    <{if $easy_item_code_605 < "12" || $easy_item_code_605 > "18" || $easy_item_code_605 ==   "15"}>
                    <div id="area_605_<{$easy_item_code_605+100}>" style="display:none">
                        <table width="100%" cellspacing="0" cellpadding="2" class="list">
                            <tr height="22">
                                <td width=250 bgcolor="#DFFFDC" style="border-top:0px;">
                                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=605 j=$easy_item_code_605+100}>
                                </td>
                                <td style="border-top:0px;">
                                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=605 j=$easy_item_code_605+100  ime="active"}>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <{elseif $easy_item_code_605 > "12" && $easy_item_code_605 != "15" && $easy_item_code_605 != "16" }>
                    <div id="area_605_<{$easy_item_code_605+99}>" style="display:none">
                        <table width="100%" cellspacing="0" cellpadding="2" class="list">
                            <tr height="22">
                                <td width=250 bgcolor="#DFFFDC" style="border-top:0px;">
                                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=605 j=$easy_item_code_605+99}>
                                </td>
                                <td style="border-top:0px;">
                                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=605 j=$easy_item_code_605+99  ime="active"}>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <{elseif $easy_item_code_605 == "16" }>
                    <div id="area_605_<{$easy_item_code_605+98}>" style="display:none">
                        <table width="100%" cellspacing="0" cellpadding="2" class="list">
                            <tr height="22">
                                <td width=250 bgcolor="#DFFFDC" style="border-top:0px;">
                                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=605 j=$easy_item_code_605+98}>
                                </td>
                                <td style="border-top:0px;">
                                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=605 j=$easy_item_code_605+98  ime="active"}>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <{/if}>
                    <{if $easy_item_code_605 == "18"}>
                    <div id="area_605_<{$easy_item_code_605+100}>" style="display:none">
                        <table width="100%" cellspacing="0" cellpadding="2" class="list">
                            <tr height="22">
                                <td width=250 bgcolor="#DFFFDC" style="border-top:0px;">
                                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=605 j=$easy_item_code_605+100}>
                                </td>
                                <td style="border-top:0px;">
                                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=605 j=$easy_item_code_605+100  ime="active"}>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <{/if}>
                    <{/if}>
                    <{* ここまで *}>
                <{/foreach}>
            <{/if}>

        <{/if}>

        <{if in_array(600, $grp_flags) && (in_array(510, $grp_flags) || in_array(125, $grp_flags) || in_array(590, $grp_flags) || in_array(610, $grp_flags) || in_array(620, $grp_flags) || in_array(625, $grp_flags) || in_array(630, $grp_flags) || in_array(635, $grp_flags) || in_array(690, $grp_flags))}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(510, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=510 j=20 tr=true}>
            </table>
        <{/if}>


        <{if in_array(510, $grp_flags) && (in_array(590, $grp_flags) || in_array(125, $grp_flags) || in_array(610, $grp_flags) || in_array(620, $grp_flags) || in_array(625, $grp_flags) || in_array(630, $grp_flags) || in_array(635, $grp_flags) || in_array(690, $grp_flags))}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>



        <{if in_array(590, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">

                <tr>
                    <td bgcolor="#DFFFDC" width="250">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=590 j=90}>
                    </td>
                    <td width="300">
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=590 j=90}>
                    </td>

                    <td bgcolor="#DFFFDC" width="250">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=590 j=91}>
                    </td>
                    <td>
                        <{include file="hiyari_edit_easyinput_item_field.tpl" i=590 j=91 ime="active"}>
                    </td>
                </tr>

            </table>
        <{/if}>


        <{if in_array(590, $grp_flags) && (in_array(610, $grp_flags) || in_array(125, $grp_flags) || in_array(620, $grp_flags) || in_array(625, $grp_flags) || in_array(630, $grp_flags) || in_array(635, $grp_flags) || in_array(690, $grp_flags))}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(610, $grp_flags) || in_array(620, $grp_flags) || in_array(625, $grp_flags) || in_array(630, $grp_flags) || in_array(635, $grp_flags)}>
                <{assign var="ta3_array_list" value=""}>
                <{php}>
                $wk_arr = array();
                $wk_arr[] = array(610,20);
                $wk_arr[] = array(620,30);
                $wk_arr[] = array(625,35);
                $wk_arr[] = array(630,40);
                $wk_arr[] = array(635,45);
                $this->_tpl_vars['ta3_array_list'] = $wk_arr;

                <{/php}>
                <{assign var="ta3_1_use" value=false}>
                <{assign var="ta3_2_use" value=false}>
                <{assign var="ta3_3_use" value=false}>
                <{assign var="ta3_4_use" value=false}>
                <{assign var="ta3_5_use" value=false}>
                <{foreach item=ta3_array from=$ta3_array_list}>
                    <{assign var="ta3_grp_work" value=$ta3_array[0]}>
                    <{assign var="ta3_item_work" value=$ta3_array[1]}>
                    <{if in_array($ta3_grp_work, $grp_flags)}>
                        <{if !$ta3_1_use}>
                            <{assign var="ta3_1_use" value=true}>
                            <{assign var="ta3_1_grp" value=$ta3_grp_work}>
                            <{assign var="ta3_1_item" value=$ta3_item_work}>
                        <{elseif !$ta3_2_use}>
                            <{assign var="ta3_2_use" value=true}>
                            <{assign var="ta3_2_grp" value=$ta3_grp_work}>
                            <{assign var="ta3_2_item" value=$ta3_item_work}>
                        <{elseif !$ta3_3_use}>
                            <{assign var="ta3_3_use" value=true}>
                            <{assign var="ta3_3_grp" value=$ta3_grp_work}>
                            <{assign var="ta3_3_item" value=$ta3_item_work}>
                        <{elseif !$ta3_4_use}>
                            <{assign var="ta3_4_use" value=true}>
                            <{assign var="ta3_4_grp" value=$ta3_grp_work}>
                            <{assign var="ta3_4_item" value=$ta3_item_work}>
                        <{else}>
                            <{assign var="ta3_5_use" value=true}>
                            <{assign var="ta3_5_grp" value=$ta3_grp_work}>
                            <{assign var="ta3_5_item" value=$ta3_item_work}>
                        <{/if}>
                    <{/if}>
                <{/foreach}>

                <table width="100%" cellspacing="0" cellpadding="2" class="list">

                <{if $ta3_1_use || $ta3_2_use}>
                    <tr>
                        <td bgcolor="#DFFFDC" width="10%">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=$ta3_1_grp j=$ta3_1_item style=big}>
                        </td>
                        <td width="40%" valign="top">
                            <{if $ta3_1_grp == 620 && $ta3_1_item == 30}>
                                <{include file="hiyari_edit_easyinput_item_field.tpl" i=$ta3_1_grp j=$ta3_1_item style=kaizensaku ime="active"}>
                                <nobr>
                                <{include file="hiyari_edit_easyinput_item_title.tpl" i=620 j=33}>
                                <input type="text" id="_620_33" name="_620_33" value="<{$vals[620][33][0]}>" style="width:100;border:#35B341 solid 2px;" readonly>
                                <img id="caller_620_33" src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer;" onclick="window.open('hiyari_calendar.php?session=<{$session}>&caller=620_33', 'newwin', 'width=640,height=480,scrollbars=yes');"/>
                                <input type="button" value="クリア" onclick="clear_620_33()">
                                </nobr>
                            <{else}>
                                <{include file="hiyari_edit_easyinput_item_field.tpl" i=$ta3_1_grp j=$ta3_1_item style=big ime="active"}>
                            <{/if}>
                        </td>

                    <{if $ta3_2_use}>
                        <td bgcolor="#DFFFDC" width="10%">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=$ta3_2_grp j=$ta3_2_item style=big}>
                        </td>
                        <td valign="top">
                            <{if $ta3_2_grp == 620 && $ta3_2_item == 30}>
                                <{include file="hiyari_edit_easyinput_item_field.tpl" i=$ta3_2_grp j=$ta3_2_item style=kaizensaku ime="active"}>
                                <nobr>
                                <{include file="hiyari_edit_easyinput_item_title.tpl" i=620 j=33}>
                                <input type="text" id="_620_33" name="_620_33" value="<{$vals[620][33][0]}>" style="width:100;border:#35B341 solid 2px;" readonly>
                                <img id="caller_620_33" src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer;" onclick="window.open('hiyari_calendar.php?session=<{$session}>&caller=620_33', 'newwin', 'width=640,height=480,scrollbars=yes');"/>
                                <input type="button" value="クリア" onclick="clear_620_33()">
                                </nobr>
                            <{else}>
                                <{include file="hiyari_edit_easyinput_item_field.tpl" i=$ta3_2_grp j=$ta3_2_item style=big ime="active"}>
                            <{/if}>
                        </td>
                    <{else}>
                        <td colspan="2"></td>
                    <{/if}>
                    </tr>
                <{/if}>

                <{if $ta3_3_use || $ta3_4_use}>
                    <tr>
                        <td bgcolor="#DFFFDC" width="10%">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=$ta3_3_grp j=$ta3_3_item style=big}>
                        </td>
                        <td width="40%">
                            <{if $ta3_3_grp == 620 && $ta3_3_item == 30}>
                                <{include file="hiyari_edit_easyinput_item_field.tpl" i=$ta3_3_grp j=$ta3_3_item style=kaizensaku ime="active"}>
                                <nobr>
                                <{include file="hiyari_edit_easyinput_item_title.tpl" i=620 j=33}>
                                <input type="text" id="_620_33" name="_620_33" value="<{$vals[620][33][0]}>" style="width:100;border:#35B341 solid 2px;" readonly>
                                <img id="caller_620_33" src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer;" onclick="window.open('hiyari_calendar.php?session=<{$session}>&caller=620_33', 'newwin', 'width=640,height=480,scrollbars=yes');"/>
                                <input type="button" value="クリア" onclick="clear_620_33()">
                                </nobr>
                            <{else}>
                                <{include file="hiyari_edit_easyinput_item_field.tpl" i=$ta3_3_grp j=$ta3_3_item style=big ime="active"}>
                            <{/if}>
                        </td>
                    <{if $ta3_2_use}>
                        <td bgcolor="#DFFFDC" width="10%">
                            <{include file="hiyari_edit_easyinput_item_title.tpl" i=$ta3_4_grp j=$ta3_4_item style=big}>
                        </td>
                        <td>
                            <{if $ta3_4_grp == 620 && $ta3_4_item == 30}>
                                <{include file="hiyari_edit_easyinput_item_field.tpl" i=$ta3_4_grp j=$ta3_4_item style=kaizensaku ime="active"}>
                                <nobr>
                                <{include file="hiyari_edit_easyinput_item_title.tpl" i=620 j=33}>
                                <input type="text" id="_620_33" name="_620_33" value="<{$vals[620][33][0]}>" style="width:100;border:#35B341 solid 2px;" readonly>
                                <img id="caller_620_33" src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer;" onclick="window.open('hiyari_calendar.php?session=<{$session}>&caller=620_33', 'newwin', 'width=640,height=480,scrollbars=yes');"/>
                                <input type="button" value="クリア" onclick="clear_620_33()">
                                </nobr>
                            <{else}>
                                <{include file="hiyari_edit_easyinput_item_field.tpl" i=$ta3_4_grp j=$ta3_4_item style=big ime="active"}>
                            <{/if}>
                        </td>
                    <{else}>
                        <td colspan="2"></td>
                    <{/if}>
                    </tr>
                <{/if}>
                <{if $ta3_5_use}>
                    <tr>
                        <td bgcolor="#DFFFDC" width="10%">
                        <{include file="hiyari_edit_easyinput_item_title.tpl" i=$ta3_5_grp j=$ta3_5_item style=big}>
                        </td>
                        <td width="40%">
                        <{if $ta3_5_grp == 620 && $ta3_5_item == 30}>
                                <{include file="hiyari_edit_easyinput_item_field.tpl" i=$ta3_5_grp j=$ta3_5_item style=kaizensaku ime="active"}>
                                <nobr>
                                <{include file="hiyari_edit_easyinput_item_title.tpl" i=620 j=33}>
                                <input type="text" id="_620_33" name="_620_33" value="<{$vals[620][33][0]}>" style="width:100;border:#35B341 solid 2px;" readonly>
                                <img id="caller_620_33" src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer;" onclick="window.open('hiyari_calendar.php?session=<{$session}>&caller=620_33', 'newwin', 'width=640,height=480,scrollbars=yes');"/>
                                <input type="button" value="クリア" onclick="clear_620_33()">
                                </nobr>
                        <{else}>
                                <{include file="hiyari_edit_easyinput_item_field.tpl" i=$ta3_5_grp j=$ta3_5_item style=big ime="active"}>
                        <{/if}>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                <{/if}>
                </table>
        <{/if}>

        <{if (in_array(600, $grp_flags) || in_array(510, $grp_flags) || in_array(590, $grp_flags) || in_array(610, $grp_flags) || in_array(620, $grp_flags) || in_array(625, $grp_flags) || in_array(630, $grp_flags) || in_array(635, $grp_flags)) && in_array(690, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
            </table>
        <{/if}>

        <{if in_array(690, $grp_flags)}>
            <table width="100%" cellspacing="0" cellpadding="2" class="list">
                <{include file="hiyari_edit_easyinput_item.tpl" i=690 j=90 tr=true}>
            </table>
        <{/if}>



    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<br>
<{/if}>

<!-- レポート内容END -->

<{/if}>

<!-- 全体 -->
</td></tr>
</table>
</td></tr>
</table>

<!--// データが全て表示されたかのフラグ -->
<input type="hidden" name="data_exist_flg" value="1">
