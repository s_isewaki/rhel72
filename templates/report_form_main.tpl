<{*
インターフェース
    $grps
        array   項目情報配列
    $vals
        array   値の配列
    $grp_flags
        array   レポートに使用する項目グループ
    $user_grp_flags
        array   レポートに使用するユーザー定義項目グループ
    $rel_grps
        array   連動グループ
    $level_infos
        array   インシレベルとメッセージの配列
    $item_element_no_disp
        array   項目非表示情報配列
    $regist_date_y
        string  報告日(年)
    $regist_date_m
        string  報告日(月)
    $regist_date_d
        string  報告日(非)
    $report_title
        string  タイトル(表示値)
    $user_cate_name
        string  ユーザー定義カテゴリ名
    $experience
        object  ログインユーザーのプロフィール情報
    $input_nocheck
        int     入力チェック実行フラグ(1なら有効)
    $item_groups
        array   項目グループ
    $profile
        object  プロフィール情報
    $pt_auth
        int     "1"以外のときに患者マスターからコピーが使用不可
    $session
        string  セッションID
    $input_mode
        string  入力モード。"analysis"の場合は分析・再発防止用入力表示となる。省略可能。
    $tabletypes
        array   テーブルタイプ

使用するサブテンプレート
    report_form_item.tpl(報告アイテム表示)
    report_form_title.tpl（報告アイテムのタイトル表示）
    report_form_field.tpl（報告アイテムのフィールド表示）
*}>

<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
<{*
//====================================================================================================
//起動時の処理
//====================================================================================================
*}>
var is_onload = true;
$(function(){
  //---------------------------------------------------------------------------
  //初期化
  //---------------------------------------------------------------------------
  <{*発生場所詳細*}>

  <{if in_array(110, $grp_flags)}>
    set_place_detail("<{$vals[110][60][0]}>", "<{$vals[110][65][0]}>");
  <{/if}>

  <{*患者の数*}>
  <{if in_array(200, $grp_flags)}>
    set_patients_num();
  <{/if}>

  <{*主治医情報*}>
  disp_update_doctor_have();

  <{*インシデント直前の患者の状態の詳細*}>
  <{if in_array(295, $grp_flags)}>
    set_patient_detail();
  <{/if}>

  <{*関連する患者情報*}>
  <{if array_intersect(array(310,320,330,340,350,360,370,380,390), $grp_flags)}>
    show_patients("<{$vals[200][30][0]}>")
  <{/if}>

  <{*概要*}>
  <{if in_array(900, $grp_flags)}>
    set_gaiyo_items("<{$vals[900][10][0]}>");
    $('.gaiyo_kind').live('click', function(){set_gaiyo_detail(920, this.value);});
    $('.gaiyo_scene').live('click', function(){set_gaiyo_detail(950, this.value); field_940_970_relate(this);});
    $('.gaiyo_content').live('click', function(){set_gaiyo_detail(980, this.value);});
  <{/if}>

  <{*影響区分*}>
  <{if in_array(1400, $grp_flags)}>
    set_eikyo_items("<{$vals[1400][10][0]}>");
  <{/if}>

  <{*対応区分*}>
  <{if in_array(1410, $grp_flags)}>
    set_taio_items("<{$vals[1410][10][0]}>");
    set_taio_detail();
  <{/if}>

  <{*発生要因*}>
  <{if in_array(605, $grp_flags)}>
    set_factors_detail();
  <{/if}>

  <{*誤った医療の実施の有無*}>
  <{if in_array(1100, $grp_flags)}>
    set_medical_error_items('<{$vals[1100][10][0]}>');
  <{/if}>

  <{*onload終了*}>
  is_onload = false;

  //---------------------------------------------------------------------------
  //イベント
  //---------------------------------------------------------------------------
  <{*カレンダー*}>
  $('.calendar_text').datepicker({
    showOn: 'focus',
    showOtherMonths: true,
    selectOtherMonths: true,
    beforeShow: function(){ $('#ui-datepicker-div').bgiframe(); }
  });
  $('.calendar_btn').live('click', function(){
    var id = $(this).attr("id").replace("_btn", "");
    $("#" + id).datepicker( 'show' );
    $('#ui-datepicker-div').bgiframe();
  });

  <{*発生日の変更*}>
  $('#id_105_5').change(function(){
    set_date_type(105, $('#id_105_5').val());
  })

  <{*発見日の変更*}>
  $('#id_100_5').change(function(){
    set_date_type(100, $('#id_100_5').val());
  })
});

function set_date_type(id, ymd)
{
  $.post(
      'hiyari_get_date_type.php',
      {
        'session' : '<{$session}>',
        'ymd' : ymd
      },
      function(data) {
        $('#id_' + id + '_10').val(data.month);
        $('#id_' + id + '_20').val(data.weekday);
        $('#id_' + id + '_30').val(data.weekday_type);

        //事例の詳細(時系列)があれば、一緒にセット（1行目のみ & 日付が未設定の場合）
        if (document.getElementById("id_4000_1")) {
            if (!document.getElementById("id_4000_1").value) {
                $("#id_4000_1").val(ymd);
            }
        }
      },
      'json'
  );
}

<{*
//====================================================================================================
//入力チェック
//====================================================================================================
*}>
var m_error_color = "#FF6666";//"red";

<{if $input_nocheck != 1}>
  //---------------------------------------------
  //下書き保存用のチェック
  //---------------------------------------------
  function chkShitagakiSubmit(){
    // データが全て表示されるまで、submit出来ないようにする 20100319
    if (!document.FRM_MAIN.data_exist_flg){
        alert("データが全て出揃ってから行ってください");
        return false;
    }

    var submitFlag = true;
    var errMsg = "";

    //-------------------------------------------
    //評価画面
    //-------------------------------------------
    <{if $input_mode == "analysis"}>
      errMsg += checkInput('_620_33', '評価予定期日');

    //-------------------------------------------
    //報告書画面
    //-------------------------------------------
    <{else}>
      //タイトル未入力時は、ヒヤリ・ハット分類を優先して設定
      set_title_from_125_85();

      //タイトル未入力時は、インシデントの内容を設定
      <{if $subject_mode == 0}>
        set_title_from_520_30();
      <{/if}>

      <{if $subject_mode == 1}>
        errMsg += checkInput('subject', '<{$report_title_wording}>');
      <{else}>
        errMsg += checkInput('report_title', '<{$report_title_wording}>');
      <{/if}>
      errMsg += dateCheck('regist_date_y', 'regist_date_m', 'regist_date_d', '報告日');
    <{/if}>

    if ( errMsg != "" ){
        alert( errMsg );
        submitFlag = false;
    }

    return submitFlag;
  }

  //---------------------------------------------
  //全項目チェック
  //---------------------------------------------
  function chkSubmit(){
    // データが全て表示されるまで、submit出来ないようにする 20100319
    if (!document.FRM_MAIN.data_exist_flg){
      alert("データが全て出揃ってから内容確認を行ってください");
      return false;
    }

    //タイトル未入力時は、ヒヤリ・ハット分類を優先して設定
    set_title_from_125_85();
    <{if $subject_mode == 0}>
      //タイトル未入力時は、インシデントの内容を設定
      set_title_from_520_30();
    <{/if}>
    var submitFlag = true;
    var errMsg = "";
    <{if $subject_mode == 1}>
      errMsg += checkInput('subject', '<{$report_title_wording}>');
    <{else}>
      errMsg += checkInput('report_title', '<{$report_title_wording}>');
    <{/if}>

    errMsg += dateCheck('regist_date_y', 'regist_date_m', 'regist_date_d', '報告日');

    <{foreach key=grp_code item=grp_val from=$grps}>
      <{foreach key=easy_item_code item=easy_item_val from=$grp_val.easy_item_list}>
        <{if ($grp_code == 100 && $easy_item_code == 10) ||($grp_code == 100 && $easy_item_code == 20) ||($grp_code == 100 && $easy_item_code == 30)}>
          <{*非表示項目のため、チェック不要。*}>
        <{elseif ($grp_code == 105 && $easy_item_code == 10) ||($grp_code == 105 && $easy_item_code == 20) ||($grp_code == 105 && $easy_item_code == 30) || ($grp_code == 105 && $easy_item_code == 70) ||($grp_code == 105 && $easy_item_code == 75)}>
          <{*非表示項目のため、チェック不要。*}>
        <{elseif ($grp_code >= 4000 && $grp_code <= 4049)}>
          <{*非表示項目のため、チェック不要。*}>
        <{elseif $grp_code == 605}>
          <{*発生要因詳細*}>
          <{if in_array($grp_code|cat:'_'|cat:$easy_item_code, $item_must)}>
            var area_605_obj = document.getElementById("id_605_<{$easy_item_code}>_area");
            if (area_605_obj && $('#id_605_<{$easy_item_code}>_area').css('display') != 'none') {
              errMsg += checkInput('_<{$grp_code}>_<{$easy_item_code}><{if $easy_item_val.easy_item_type == 'checkbox' or $easy_item_val.easy_item_type == 'radio'}>[]<{/if}>', '<{$easy_item_val.easy_item_name}>');
             }
          <{/if}>
        <{elseif $grp_code == 110 && $easy_item_code == 65}>
          <{*発生場所詳細*}>
         <{if in_array('110_60', $item_must)}>
            if (is_110_65_selection){
              errMsg += checkInput('_110_65[]', '発生場所詳細');
            }
          <{/if}>
        <{elseif $grp_code == "1100" && $easy_item_code == "10"}>
          <{if in_array($grp_code|cat:'_'|cat:$easy_item_code, $item_must)}>
            var obj1100_1 = document.getElementById("id_1100_10_1");
            var obj1100_2 = document.getElementById("id_1100_10_2");
            if ( obj1100_1 && obj1100_2 ){
              errMsg += checkInput('_<{$grp_code}>_<{$easy_item_code}>[]', '誤った医療の実施の有無');
            }

            if (obj1100_1.checked == true){
              var obj1100_20_1 = document.getElementById("id_1100_20_1");
              var obj1100_20_2 = document.getElementById("id_1100_20_2");
              var obj1100_20_3 = document.getElementById("id_1100_20_3");
              var obj1100_20_4 = document.getElementById("id_1100_20_4");
              if ( obj1100_20_1 && obj1100_20_2 && obj1100_20_3 && obj1100_20_4){
                errMsg += checkInput('_1100_20[]', '実施の程度');
              }
            }

            if (obj1100_2.checked == true){
              var obj1100_50_1 = document.getElementById("id_1100_50_1");
              var obj1100_50_2 = document.getElementById("id_1100_50_2");
              var obj1100_50_3 = document.getElementById("id_1100_50_3");
              if ( obj1100_50_1 && obj1100_50_2 && obj1100_50_3){
                errMsg += checkInput('_1100_50[]', '実施の程度');
              }
            }
          <{/if}>
        <{elseif $grp_code == "900" && $easy_item_code == "10"}>
          // 1000番台の2010年版は手動でエラーを確認する
          <{if in_array('900_10', $item_must)}>
            <{*概要*}>
            var obj = $("input:radio[name='_900_10[]']");
            var val_900_10 = get_radio_value(900,10);
            if (obj){
              var items = obj.closest('table').find('.item_set');
              if (val_900_10){
                items.css("backgroundColor","");
              }
              else {
                items.css("backgroundColor",m_error_color);
                errMsg += "概要 が未記入です\n";
              }

              <{*概要のその他：その他が選択されている場合のみ必須*}>
              var val_other = "";
              <{section name=k loop=$grps[900].easy_item_list[10].easy_list}>
                <{if $item_element_no_disp[900][10] == "" || !in_array($grps[900].easy_item_list[10].easy_list[k].easy_code, $item_element_no_disp[900][10])}>
                  <{if isset($grps[900].easy_item_list[10].easy_list[k].other_code)}>
                    val_other = "<{$grps[900].easy_item_list[10].easy_list[k].easy_code}>";
                  <{/if}>
                <{/if}>
              <{/section}>
              //obj = $("#id_900_20");
              //.if (obj){
                //obj.css("backgroundColor","");
                //if (val_900_10==val_other){
                  //if (obj.val()==""){
                    //obj.css("backgroundColor",m_error_color);
                    //errMsg += "概要のその他の内容が未記入です\n";
                  //}
                //}
              //}
            }

            <{*種類*}>
            if ( $("#id_910_10_area").css("display")!="none" ){
              obj = $("input:radio[name='_910_10[]']");
              var items = obj.closest('table').find('.item_set');
              if (get_radio_value(910,10) || obj.length==0){
                items.css("backgroundColor","");
              }
              else {
                items.css("backgroundColor",m_error_color);
                errMsg += "種類 が未記入です\n";
              }
            }

            <{*種類-項目*}>
            obj = $("input:radio[name='_920_10[]']");
            if (obj){
              var items = obj.closest('table').find('.item_set');
              if (get_radio_value(920,10) || obj.length==0){
                items.css("backgroundColor","");
              }
              else {
                items.css("backgroundColor",m_error_color);
                errMsg += "種類の項目 が未記入です\n";
              }
            }

            <{*種類-項目のその他*}>
            //obj = $("#id_930_10");
            //if (obj){
              //<{*入力あり or 概要!=その他*}>
              //if (obj.val()!="" || val_900_10!=8){
                //obj.css("backgroundColor","");
              //}
              //else {
                //obj.css("backgroundColor",m_error_color);
                //errMsg += "種類のその他 が未記入です\n";
             // }
            //}

            <{*発生場面*}>
            obj = $("input:radio[name='_940_10[]']");
            if (obj){
              var items = obj.closest('table').find('.item_set');
              if (get_radio_value(940,10) || obj.length==0){
                items.css("backgroundColor","");
              }
              else {
                items.css("backgroundColor",m_error_color);
                errMsg += "発生場面 が未記入です\n";
              }
            }

            <{*発生場面-項目*}>
            obj = $("input:radio[name='_950_10[]']");
            if (obj){
              var items = obj.closest('table').find('.item_set');
              if (get_radio_value(950,10) || obj.length==0){
                items.css("backgroundColor","");
              }
              else {
                items.css("backgroundColor",m_error_color);
                errMsg += "発生場面の項目 が未記入です\n";
              }
            }

            <{*発生場面-項目のその他*}>
            //obj = $("#id_960_10");
            //if (obj){
              //<{*入力あり or 概要!=その他*}>
              //if (obj.val()!="" || val_900_10!=8){
               // obj.css("backgroundColor","");
              //}
              //else {
                //obj.css("backgroundColor",m_error_color);
                //errMsg += "発生場面のその他 が未記入です\n";
              //}
            //}

            <{*事例の内容*}>
            obj = $("input:radio[name='_970_10[]']");
            if (obj){
              var items = obj.closest('table').find('.item_set');
              if (get_radio_value(970,10) || obj.length==0){
                items.css("backgroundColor","");
              }
              else {
                items.css("backgroundColor",m_error_color);
                errMsg += "事故(事例)の内容 が未記入です\n";
              }
            }

            <{*事例の内容-項目*}>
            obj = $("input:radio[name='_980_10[]']");
            if (obj){
              var items = obj.closest('table').find('.item_set');
              if (get_radio_value(980,10) || obj.length==0){
                items.css("backgroundColor","");
              }
              else {
                items.css("backgroundColor",m_error_color);
                errMsg += "事故(事例)の内容の項目 が未記入です\n";
              }
            }

            <{*事例の内容-項目のその他*}>
            //obj = $("#id_990_10");
            //if (obj){
              //<{*入力あり or 概要!=その他*}>
              //if (obj.val()!="" || val_900_10!=8){
                //obj.css("backgroundColor","");
              //}
              //else {
                //obj.css("backgroundColor",m_error_color);
               // errMsg += "事故(事例)の内容のその他 が未記入です\n";
             // }
            //}
          <{/if}>

          <{if in_array("1300_10", $item_must) || in_array("1310_10", $item_must) || in_array("1320_10", $item_must)}>
            var obj = $("input:radio[name='_900_10[]']");
            if (obj){
              var val_900_10 = get_radio_value(900,10);
              <{*関連医薬品*}>
              <{if in_array("1300_10", $item_must)}>
                if ( val_900_10 == 1 ){
                  if ( document.FRM_MAIN._1000_5.value ){
                    document.FRM_MAIN._1000_5.style.backgroundColor = "";
                  }
                  else {
                    document.FRM_MAIN._1000_5.style.backgroundColor = m_error_color;
                    errMsg += "販売名 が未記入です\n";
                  }

                  if ( document.FRM_MAIN._1000_15.value ){
                    document.FRM_MAIN._1000_15.style.backgroundColor = "";
                  }
                  else {
                    document.FRM_MAIN._1000_15.style.backgroundColor = m_error_color;
                    errMsg += "製造販売業者名 が未記入です\n";
                  }
                }
              <{/if}>

              <{*医療材料・諸物品等*}>
              <{if in_array("1310_10", $item_must)}>
                if ( val_900_10==3　|| val_900_10==4 || val_900_10==5 || val_900_10==6 || val_900_10==7 ){
                  if ( document.FRM_MAIN._1000_20.value || val_900_10 == 7){
                    document.FRM_MAIN._1000_20.style.backgroundColor = "";
                  }
                  else {
                    document.FRM_MAIN._1000_20.style.backgroundColor = m_error_color;
                    errMsg += "販売名 が未記入です\n";
                  }
                  if ( document.FRM_MAIN._1000_25.value || val_900_10 == 7){
                    document.FRM_MAIN._1000_25.style.backgroundColor = "";
                  }
                  else {
                    document.FRM_MAIN._1000_25.style.backgroundColor = m_error_color;
                    errMsg += "製造販売業者名 が未記入です\n";
                  }
                  if ( document.FRM_MAIN._1000_30.value || val_900_10 == 7){
                    document.FRM_MAIN._1000_30.style.backgroundColor = "";
                  }
                  else {
                    document.FRM_MAIN._1000_30.style.backgroundColor = m_error_color;
                    errMsg += "購入年月 が未記入です\n";
                  }
                }
              <{/if}>

              <{*医療機器等*}>
              <{if in_array("1320_10", $item_must)}>
                if ( val_900_10 == 4 || val_900_10 == 6 ){
                  if ( document.FRM_MAIN._1000_35.value ){
                    document.FRM_MAIN._1000_35.style.backgroundColor = "";
                  }
                  else {
                    document.FRM_MAIN._1000_35.style.backgroundColor = m_error_color;
                    errMsg += "販売名 が未記入です\n";
                  }
                  if ( document.FRM_MAIN._1000_40.value ){
                    document.FRM_MAIN._1000_40.style.backgroundColor = "";
                  }
                  else {
                    document.FRM_MAIN._1000_40.style.backgroundColor = m_error_color;
                    errMsg += "製造販売業者名 が未記入です\n";
                  }
                  if ( document.FRM_MAIN._1000_45.value ){
                    document.FRM_MAIN._1000_45.style.backgroundColor = "";
                  }
                  else {
                    document.FRM_MAIN._1000_45.style.backgroundColor = m_error_color;
                    errMsg += "製造年月 が未記入です\n";
                  }
                  if ( document.FRM_MAIN._1000_50.value ){
                    document.FRM_MAIN._1000_50.style.backgroundColor = "";
                  }
                  else {
                    document.FRM_MAIN._1000_50.style.backgroundColor = m_error_color;
                    errMsg += "購入年月 が未記入です\n";
                  }
                  if ( document.FRM_MAIN._1000_55.value ){
                    document.FRM_MAIN._1000_55.style.backgroundColor = "";
                  }
                  else {
                    document.FRM_MAIN._1000_55.style.backgroundColor = m_error_color;
                    errMsg += "直近の保守・点検年月 が未記入です\n";
                  }
                }
              <{/if}>
            }
          <{/if}>

        <{*患者への影響*}>
        <{elseif $grp_code == "1400" && $easy_item_code == "30"}>
          <{if in_array($grp_code|cat:'_'|cat:$easy_item_code, $item_must)}>
            var val_1400_10 = get_radio_value("1400","10");
            if (val_1400_10){
              var obj = document.getElementById("id_1400_30_" + val_1400_10 + "_area");
              if (obj){
                if ($( "#id_1400_30_" + val_1400_10 + "_area" ).css("display") != "none"){
                  errMsg += checkInput('_<{$grp_code}>_<{$easy_item_code}><{if $easy_item_val.easy_item_type == 'checkbox' or $easy_item_val.easy_item_type == 'radio'}>[]<{/if}>', '<{$easy_item_val.easy_item_name}>');
                }
              }
            }
          <{/if}>

        <{*対応*}>
        <{elseif $grp_code == "1410" && $easy_item_code == "20"}>
          <{if in_array($grp_code|cat:'_'|cat:$easy_item_code, $item_must)}>
            var val_1410_10 = get_radio_value("1410","10");
            if (val_1410_10){
              var obj = document.getElementById("id_1410_20_" + val_1410_10 + "_area");
              if (obj){
                var area = $( "#id_1410_20_" + val_1410_10 + "_area" );
                if (area.css("display") != "none"){
                  var box = area.find('.box')
                  var sel = box.find('input:checked');
                  if (sel.length > 0){
                    box.find('.item_set').css('background-color', '');
                  }
                  else{
                    box.find('.item_set').css('background-color', m_error_color);
                    errMsg += '<{$easy_item_val.easy_item_name}>' + ' が未選択です\n';
                  }
                }
              }
            }
          <{/if}>

        <{*直前の状態詳細*}>
        <{elseif $grp_code == "295"}>
          <{if in_array($grp_code|cat:'_'|cat:$easy_item_code, $item_must)}>
            if ($("#id_295_<{$easy_item_code}>_area").css("display") != "none"){
              errMsg += checkInput('_295_<{$easy_item_code}>[]', '<{$easy_item_val.easy_item_name}>');
            }
          <{/if}>

        <{*関連する患者情報*}>
        <{elseif $grp_code >= "300" && $grp_code <="399"}>
          <{if in_array($grp_code|cat:'_'|cat:$easy_item_code, $item_must)}>
            if ( ("<{$grp_code}>" % 10) < $("#id_200_30").val() ){
              errMsg += checkInput('_<{$grp_code}>_<{$easy_item_code}><{if $easy_item_val.easy_item_type == 'checkbox' or $easy_item_val.easy_item_type == 'radio'}>[]<{/if}>', '<{$easy_item_val.easy_item_name}>');
            }
          <{/if}>

        <{*他*}>
        <{else}>
          <{if in_array($grp_code|cat:'_'|cat:$easy_item_code, $item_must)}>
            <{if $time_setting_flg}>
              <{if !( ($grp_code == 100 && $easy_item_code == 60) || ($grp_code == 100 && $easy_item_code == 65) || ($grp_code == 105 && $easy_item_code == 60) || ($grp_code == 105 && $easy_item_code == 65) )}>
                errMsg += checkInput('_<{$grp_code}>_<{$easy_item_code}><{if $easy_item_val.easy_item_type == 'checkbox' or $easy_item_val.easy_item_type == 'radio'}>[]<{/if}>', '<{$easy_item_val.easy_item_name}>');
              <{/if}>
            <{else}>
              <{if !( ($grp_code == 100 && $easy_item_code == 40) || ($grp_code == 105 && $easy_item_code == 40) )}>
                errMsg += checkInput('_<{$grp_code}>_<{$easy_item_code}><{if $easy_item_val.easy_item_type == 'checkbox' or $easy_item_val.easy_item_type == 'radio'}>[]<{/if}>', '<{$easy_item_val.easy_item_name}>');
              <{/if}>
            <{/if}>
          <{/if}>
        <{/if}>
      <{/foreach}>
    <{/foreach}>

    errMsg += multi_touzisha_check();
    errMsg += multi_jirei_check();

    if ( errMsg != "" ){
        alert( errMsg );
        submitFlag = false;
    }

    return submitFlag;
  }

  //---------------------------------------------
  //タイトルの設定
  //---------------------------------------------
  //タイトル未入力時は、インシデントの内容の先頭10文字を設定
  function set_title_from_520_30()
  {
      var obj_title = document.FRM_MAIN[ 'report_title' ];
      if ( !obj_title ) return "";
      var obj_520_30= document.FRM_MAIN[ '_520_30' ];
      if ( !obj_520_30 ) return "";

      if (obj_title.value == ""){
          var t = obj_520_30.value;

          //改行コード等を取り除く
          t = t.replace(/(\n|\r|\t)+/g, "")

          //先頭10文字以外をカット
          if (t.length > 10){
              t = t.substr(0,10);
          }

          obj_title.value = t;
      }
  }

  //タイトル未入力時は、ヒヤリ・ハット分類を設定
  function set_title_from_125_85()
  {
    var obj_title = document.FRM_MAIN[ 'report_title' ];
    if ( !obj_title ) return "";
    var obj_125_85= document.getElementsByName("_125_85[]");
    if ( !obj_125_85 ) return "";

    if (obj_title.value == ""){
      for (i=0; i < obj_125_85.length; i++){
        if (obj_125_85[i].checked){
          obj_title.value = document.getElementById("item_125_85_" + obj_125_85[i].value).value;
          break;
        }
      }
    }
  }

  //---------------------------------------------
  //日付チェック
  //---------------------------------------------
  // 日付妥当性チェック
  function dateCheck(year, month, day, msg){
      var obj_year = document.FRM_MAIN[ year ];
      var obj_month = document.FRM_MAIN[ month ];
      var obj_day = document.FRM_MAIN[ day ];

      year = parseInt(obj_year.value,10);
      month = parseInt(obj_month.value,10);
      day = parseInt(obj_day.value,10);

      //閏年
      if ((month == 2) && YearChk(year)){
          month=13;
      }

      //末日チェック
      if (LastDayChk(month,day)){
          return "";
      }
      else {
          obj_year.style.backgroundColor = m_error_color;
          obj_month.style.backgroundColor = m_error_color;
          obj_day.style.backgroundColor = m_error_color;

          return msg  + 'の日付が正しくありません';
      }
  }

  //末日チェック
  function LastDayChk(in_Month,in_Day){
    lastDay = new Array(31,28,31,30,31,30,31,31,30,31,30,31,29);
    if (lastDay[in_Month-1] >= in_Day){
      return true;
    }
     else {
      return false;
    }
  }

  //年チェック
  function YearChk(in_Year){
    if ((in_Year % 4) == 0 && ((in_Year % 100) != 0 || (in_Year % 400))){
      return true;
    }
    return false;
  }

  //---------------------------------------------
  //複数当事者の入力チェック
  //最下部の当事者に当事者情報が入力されている場合、上部の当事者の項目のいずれかは入力必須となる。
  //但し、以下の場合はチェック対象外となる。
  //・当事者が１人の場合
  //・当事者項目に１つでも必須項目がある場合
  //・最終表示当事者
  //---------------------------------------------
  function multi_touzisha_check(){
    //返却用エラーメッセージ
    var err_msg = "";

    //-----------------------------------------------------
    //オブジェクトID配列を初期化
    //-----------------------------------------------------
    var touzisha_obj_id_list = new Array(10);
    for (var i=0;i<10;i++){
        touzisha_obj_id_list[i] = new Object();
    }

    //-----------------------------------------------------
    //オブジェクトID配列にID情報を格納
    //-----------------------------------------------------
    <{foreach key=grp_code item=grp_val from=$grps}>
      <{if $grp_code > 3000 && $grp_code < 3500}>
        <{foreach key=easy_item_code item=easy_item_val from=$grp_val.easy_item_list}>
          <{if in_array($grp_code|cat:'_'|cat:$easy_item_code, $item_must)}>
            //必須項目が存在するため、チェック不要。
            return "";
          <{/if}>
          <{if $grps[$grp_code].easy_item_list[$easy_item_code].easy_item_type == 'radio'}>
            touzisha_obj_id_list[<{$grp_code}>%10].id_<{$grp_code}>_<{$easy_item_code}> = '_<{$grp_code}>_<{$easy_item_code}>[]';
          <{else}>
            touzisha_obj_id_list[<{$grp_code}>%10].id_<{$grp_code}>_<{$easy_item_code}> = '_<{$grp_code}>_<{$easy_item_code}>';
          <{/if}>
        <{/foreach}>
      <{/if}>
    <{/foreach}>

    //-----------------------------------------------------
    //最終表示当事者を判定／エラークリア
    //-----------------------------------------------------
    var last_disp_touzisha = 0;
    //各担当者に対して
    for (var i=0;i<10;i++){
        var is_no_obj = true;

        //各当事者入力項目に対して
        for (var object_field_name in touzisha_obj_id_list[i]){
            //入力項目オブジェクト
            var obj_id = eval('touzisha_obj_id_list[i].' + object_field_name);
            var obj = document.FRM_MAIN[ obj_id ];
            if ( obj ){
                //入力項目オブジェクトあり
                is_no_obj = false;

                //エラークリア
                if (!obj.type){ //チェックボックス／ラジオボタン
                  for (var j = 0; j < obj.length; j++){
                      obj[j].parentNode.style.backgroundColor = "";//親TDの背景色
                  }
                }
                else{
                  obj.style.backgroundColor = "";
                }
            }
        }

        //オブジェクトが１つも無い場合
        if (is_no_obj){
            //以降、非表示状態のため、ループ終了
            break;
        }
        else{
            //最終表示当事者を更新
            last_disp_touzisha = i;
        }
    }

    //-----------------------------------------------------
    //当事者が１人の場合はチェック不要。
    //-----------------------------------------------------
    if (last_disp_touzisha == 0){
      return "";
    }

    //-----------------------------------------------------
    //必須チェック＆エラー処理
    //-----------------------------------------------------
    //最終表示当事者以外の各表示当事者に対して
    for (var i=0;i<last_disp_touzisha;i++){
      var is_no_input = true;

      //各当事者入力項目に対して
      for (var object_field_name in touzisha_obj_id_list[i]){
        //入力項目オブジェクト
        var obj_id = eval('touzisha_obj_id_list[i].' + object_field_name);
        var obj = document.FRM_MAIN[ obj_id ];
        if ( !obj ){
            continue;
        }

        //入力チェック
        if (!obj.type){//チェックボックス／ラジオボタン
          for (var j = 0; j < obj.length; j++){
            if (obj[j].checked){
              if (obj[j].value!=""){
                is_no_input = false
              }
              break;
            }
          }
        }
        else if ( obj.type == "select-one" ){
          if (obj.options[ obj.selectedIndex ].value != ""){
            is_no_input = false
            break;
          }
        }
        else{
          if (obj.value != ""){
            is_no_input = false
            break;
          }
        }
      }

      //まったく入力されていない場合
      if (is_no_input){
        //エラーとみなす。
        err_msg += "当事者" + (i + 1) + "が未入力です\n";

        //各当事者入力項目に対して
        for (var object_field_name in touzisha_obj_id_list[i]){
          //入力項目オブジェクト
          var obj_id = eval('touzisha_obj_id_list[i].' + object_field_name);
          var obj = document.FRM_MAIN[ obj_id ];
          if ( obj ){
            //エラーカラー設定
            if (obj.type){
              obj.style.backgroundColor = m_error_color;
            }
            else{
              for (var j = 0; j < obj.length; j++){
                obj[j].parentNode.style.backgroundColor = m_error_color;//親TDの背景色
              }
            }
          }
        }
      }
    }

    return err_msg;
  }

  //---------------------------------------------
  //事例の詳細(時系列)の入力チェック
  //---------------------------------------------
  function multi_jirei_check(){
    <{if in_array('4000_1', $item_must)}>
      //返却用エラーメッセージ
      var err_msg = "";
      var is_no_input = true;
      for (var grp_code = 4000; grp_code <= 4049; grp_code++){
        if (document.FRM_MAIN[ '_'+grp_code+'_1']){
          if (document.FRM_MAIN[ '_'+grp_code+'_1'].value){
            document.FRM_MAIN[ '_'+grp_code+'_1'].style.backgroundColor = "";
          }
          else{
            is_no_input = false;
            document.FRM_MAIN[ '_'+grp_code+'_1'].style.backgroundColor = m_error_color;
          }

          if (document.FRM_MAIN[ '_'+grp_code+'_2'].value){
            document.FRM_MAIN[ '_'+grp_code+'_2'].style.backgroundColor = "";
          }
          else{
            is_no_input = false;
            document.FRM_MAIN[ '_'+grp_code+'_2'].style.backgroundColor = m_error_color;
          }

          if (document.FRM_MAIN[ '_'+grp_code+'_3'].value){
            document.FRM_MAIN[ '_'+grp_code+'_3'].style.backgroundColor = "";
          }
          else{
            is_no_input = false;
            document.FRM_MAIN[ '_'+grp_code+'_3'].style.backgroundColor = m_error_color;
          }

          if (document.FRM_MAIN[ '_'+grp_code+'_4'].value){
            document.FRM_MAIN[ '_'+grp_code+'_4'].style.backgroundColor = "";
          }
          else{
            is_no_input = false;
            document.FRM_MAIN[ '_'+grp_code+'_4'].style.backgroundColor = m_error_color;
          }

          if (document.FRM_MAIN[ '_'+grp_code+'_5'].value){
            document.FRM_MAIN[ '_'+grp_code+'_5'].style.backgroundColor = "";
          }
          else{
            is_no_input = false;
            document.FRM_MAIN[ '_'+grp_code+'_5'].style.backgroundColor = m_error_color;
          }
        }
      }
      if (!is_no_input){
        err_msg += "事例の詳細(時系列)が未入力です\n";
      }
      return err_msg;
    <{else}>
      return "";
    <{/if}>
  }

<{else}>

  function chkSubmit(){
    return true;
  }
<{/if}>

//入力チェック
function checkInput(objId, objName){
    var obj = document.FRM_MAIN[ objId ];
    if ( !obj ) return "";

    //FireFox対応
    //FireFoxで、インシデント概要の必須エラー後別のインシデント概要に変更した場合に
    //上記のobj判定で正しく判定されないケースがある。(エラー対象となったオブジェクトが何故か取得できてしまうため)
    if ( objId.substr(1,3) >= 400 && objId.substr(1,3) < 500 )
    {
        var obj120s = document.getElementsByName("_120_80");
        var obj120  = obj120s[0];
        if (obj120 && objId.substr(2,1) != obj120.options[obj120.selectedIndex].value -1)
        {
            return "";
        }
    }

    //チェックボックス／ラジオボタン配列の場合
    if (!obj.type){
        //エラー判定
        var is_ok = false;
        var item = $('#' + obj[0].id);
        for ( var i = 0; i < obj.length; i++ ){
            if ( obj[i].checked == true ) is_ok = true;
        }

        //エラーカラー更新
        var color = (is_ok) ? "" :m_error_color;
        var ul = item.closest('ul');
        if (ul.length > 0){
          ul.css('background-color', color);
        }
        else{
          var dd = item.closest('dd');
          if (dd.length > 0){
            dd.css('background-color', color);
          }
          else{
            var tbl = item.closest('table');
            if (tbl.hasClass('box')){
              tbl.find('.item_set').css('background-color', color);
            }
            else{
              item.closest('td').css('background-color', color);
            }
          }
        }
        /*
        for ( var i = 0; i < obj.length; i++ ){
            //obj[i].style.backgroundColor = color;//FireFoxでは何も起こらない。(color,borderColorも同様)
            obj[i].parentNode.style.backgroundColor = color;//親TDの背景色
        }
        */

<{if isset($item_groups)}>
        // グループ項目入力チェック
        if (is_ok){
            is_ok = checkGroupItemInput(objId);
        }
<{/if}>
        //エラーメッセージ返却
        if (is_ok)
        {
            return "";
        }
        else
        {
            return objName + " が未選択です\n";
        }
    }

    //チェックボックス配列(配列数=1)の場合(通常はitem_must_ableをfalseとし、必須対象としないのが適切。)
    else if ( obj.type == "checkbox" || obj.type == "radio" ){
        //エラー判定
        var is_ok = is_ok = obj.checked;

        //エラーカラー更新
        var color = (is_ok) ? "" :m_error_color;
        obj.parentNode.style.backgroundColor = color;//親TDの背景色

        //エラーメッセージ返却
        if (is_ok)
        {
            return "";
        }
        else
        {
            return objName + " が未選択です\n";
        }
    }

    //プルダウンメニューの場合
    else if ( obj.type == "select-one" ){
        //エラー判定
        var is_ok = ( obj.options[ obj.selectedIndex ].value != "" );

        //エラーカラー更新
        var color = (is_ok) ? "" :m_error_color;
        obj.style.backgroundColor = color;

        //エラーメッセージ返却
        if (is_ok)
        {
            return "";
        }
        else
        {
            return objName + " が未選択です\n";
        }
    }

    //それ以外の場合
    else {
        //エラー判定
        var is_ok = ( obj.value != "" );

        //エラーカラー更新
        var color = (is_ok) ? "" :m_error_color;
        obj.style.backgroundColor = color;

        //エラーメッセージ返却
        if (is_ok)
        {
            return "";
        }
        else
        {
            return objName + " が未入力です\n";
        }
    }
    return "";
}

<{if isset($item_groups)}>
//グループ項目入力チェック
function checkGroupItemInput(objId){
    var itemGroups = <{$item_groups}>;
    var itemGroup = itemGroups[objId];
    if (itemGroup == null){  // グループ指定なし
        return true;
    }

    var itemGroupOk = true;
    for (var grpIdx = 0; grpIdx < itemGroup.length; grpIdx++){
        var items = itemGroup[grpIdx];
        if (items.length >= 0){
            var is_ok = false;
            for (var itmIdx = 0; itmIdx < items.length; itmIdx++){
                var item = $('#' + items[itmIdx]);
                if (item && item.prop('checked')){
                    is_ok = true;
                    break;
                }
            }
            if (!is_ok){
                itemGroupOk = false;
            }
            for (var itmIdx = 0; itmIdx < items.length; itmIdx++){
                var item = $('#' + items[itmIdx]);
                if (item){

                    //エラーカラー更新
                    var color = is_ok ? "" : m_error_color;
                    var ul = item.closest('ul');
                    if (ul.length > 0){
                      ul.css('background-color', color);
                    }
                    else{
                      var dd = item.closest('dd');
                      if (dd.length > 0){
                        dd.css('background-color', color);
                      }
                      else{
                        var tbl = item.closest('table');
                        if (tbl.hasClass('box')){
                          item.closest('.item_set').css('background-color', color);
                        }
                        else{
                          item.closest('td').css('background-color', color);
                        }
                      }
                    }

                }
            }
        }
    }

    return itemGroupOk;
}
<{/if}>

<{*
//====================================================================================================
//テキストボックス変更時の動作
//====================================================================================================
*}>
function text_onchange(this_obj){
    var obj_id = this_obj.id;
    if (! obj_id){
        return;
    }

    var obj_id_info_arr = obj_id.split("_");//0:固定文字｢id｣,1:グループコード,2:項目コード
    if (obj_id_info_arr.length != 3){
        return;
    }

    var grp_code = obj_id_info_arr[1];
    var easy_item_code = obj_id_info_arr[2];

    // 主治医
    if (grp_code == "243" && easy_item_code == "73"){
      clear_doctor_emp_id();
    }
}

<{*
//====================================================================================================
//テキストエリアクリック時の動作
//====================================================================================================
*}>
function textarea_onclick(this_obj){
    var obj_id = this_obj.id;
    if (! obj_id){
        return;
    }

    var obj_id_info_arr = obj_id.split("_");//0:固定文字｢id｣,1:グループコード,2:項目コード
    if (obj_id_info_arr.length != 3){
        return;
    }

    var grp_code = obj_id_info_arr[1];
    var easy_item_code = obj_id_info_arr[2];

    // 事象
    <{if $is_grpcode_4000_template}>
      if (grp_code>=4000 && grp_code<=4049 && easy_item_code==5){
        edit_phenomenon(grp_code, this_obj.value);
      }
    <{/if}>
}

<{*
//====================================================================================================
//ラジオボタンクリック時の動作
//====================================================================================================
*}>
function radio_onclick(this_obj){
    var obj_id = this_obj.id;
    if (!obj_id) return;

    var obj_id_info_arr = obj_id.split("_");//0:固定文字｢id｣,1:グループコード,2:項目コード,3:項目インデックス(※選択項目コードではない。)
    if (obj_id_info_arr.length != 4) return;

    var grp_code = obj_id_info_arr[1];
    var easy_item_code = obj_id_info_arr[2];

    //発生場所
    
    if (grp_code == "110" && easy_item_code == "60"){
        set_place_detail(this_obj.value,this_obj.value);
    }
  
    //発見者
    <{if $gamen_mode != 'update'}>
      if (grp_code == "3000" && easy_item_code == "10"){
        //当事者本人または同職種者の場合、プロフィールの職種を設定
        if (this_obj.value == 1 || this_obj.value == 2){
          $("input:radio[name='_3020_10[]']").val(['<{$profile.exp_code}>']);
          check_radio($("input:radio[name='_3020_10[]']:checked"), true);
        }
      }
    <{/if}>

    //患者の数
    if (grp_code==200 && easy_item_code==10){
      set_patients_num();
    }

    //概要
    if (grp_code==900 && easy_item_code==10){
      set_gaiyo_items(this_obj.value);
    }

    //影響区分
    if (grp_code == "1400" && easy_item_code == "10"){
      set_eikyo_items(this_obj.value);

      //対応区分との連動
      <{if in_array(1410, $grp_flags)}>
        if (this_obj.value != ""){
          if (!get_radio_value("1410","10")){
            $("input:radio[name='_1410_10[]']").val([this_obj.value]);
            check_radio($("input:radio[name='_1410_10[]']:checked"), true);
            set_taio_items(this_obj.value);
          }
        }
      <{/if}>
    }

    //対応区分
    if (grp_code == "1410" && easy_item_code == "10"){
      set_taio_items(this_obj.value);
      set_taio_detail();

      //影響区分との連動
      <{if in_array(1400, $grp_flags)}>
        if (this_obj.value != ""){
          if (!get_radio_value("1400","10")){
            $("input:radio[name='_1400_10[]']").val([this_obj.value]);
            check_radio($("input:radio[name='_1400_10[]']:checked"), true);
            set_eikyo_items(this_obj.value);
          }
        }
      <{/if}>
    }
}

<{*
//====================================================================================================
//チェックボックスクリック時の動作
//====================================================================================================
*}>
function checkbox_onclick(this_obj){
  var obj_id = this_obj.id;
  if (! obj_id){
    return;
  }

  var obj_id_info_arr = obj_id.split("_");//0:固定文字｢id｣,1:グループコード,2:項目コード,3:項目インデックス(※選択項目コードではない。)
  if (obj_id_info_arr.length != 4 && obj_id_info_arr[1] != 1410){
    return;
  }

  var grp_code = obj_id_info_arr[1];
  var easy_item_code = obj_id_info_arr[2];

  // インシデント直前の患者の状態の場合
  <{if in_array(295, $grp_flags)}>
    if (grp_code == "290" && easy_item_code == "120"){
        set_patient_detail();
    }
  <{/if}>

  //対応の場合
  if (grp_code == "1410" && easy_item_code == "20"){
    set_taio_detail();
  }

  //発生要因の場合
  <{if in_array(600, $grp_flags) && in_array(605, $grp_flags)}>
  	if (grp_code == "600" && easy_item_code == "10"){
    	set_factors_detail();
  	}
  <{/if}>
}

<{*
//====================================================================================================
//セレクトボックス変更時の動作
//====================================================================================================
*}>
function selectbox_onchange(this_obj){
    var obj_id = this_obj.id;
    if (!obj_id)　return;

    var obj_id_info_arr = obj_id.split("_");//0:固定文字｢id｣,1:グループコード,2:項目コード
    if (obj_id_info_arr.length != 3)　return;

    var grp_code = obj_id_info_arr[1];
    var easy_item_code = obj_id_info_arr[2];

    // 発見時刻の場合
    if (grp_code == "105" && easy_item_code == "60"){
        var value = parseInt(this_obj.value);
        var obj_time = document.getElementById("_105_40");

        switch (value){
            case 0: case 1:
                obj_time.value = '1';
                break;
            case 2: case 3:
                obj_time.value = '2';
                break;
            case 4: case 5:
                obj_time.value = '3';
                break;
            case 6: case 7:
                obj_time.value = '4';
                break;
            case 8: case 9:
                obj_time.value = '5';
                break;
            case 10: case 11:
                obj_time.value = '6';
                break;
            case 12: case 13:
                obj_time.value = '7';
                break;
            case 14: case 15:
                obj_time.value = '8';
                break;
            case 16: case 17:
                obj_time.value = '9';
                break;
            case 18: case 19:
                obj_time.value = '10';
                break;
            case 20: case 21:
                obj_time.value = '11';
                break;
            case 22: case 23:
                obj_time.value = '12';
                break;
        }
    }

    // 発生時刻の場合
    if (grp_code == "100" && easy_item_code == "60"){
        var value = parseInt(this_obj.value);
        var obj_time = document.getElementById("_100_40");

        switch (value){
            case 0: case 1:
                obj_time.value = '1';
                break;
            case 2: case 3:
                obj_time.value = '2';
                break;
            case 4: case 5:
                obj_time.value = '3';
                break;
            case 6: case 7:
                obj_time.value = '4';
                break;
            case 8: case 9:
                obj_time.value = '5';
                break;
            case 10: case 11:
                obj_time.value = '6';
                break;
            case 12: case 13:
                obj_time.value = '7';
                break;
            case 14: case 15:
                obj_time.value = '8';
                break;
            case 16: case 17:
                obj_time.value = '9';
                break;
            case 18: case 19:
                obj_time.value = '10';
                break;
            case 20: case 21:
                obj_time.value = '11';
                break;
            case 22: case 23:
                obj_time.value = '12';
                break;
        }
    }

    // 関連する患者情報の数の場合
    <{if array_intersect(array(310,320,330,340,350,360,370,380,390), $grp_flags)}>
      if (grp_code == "200" && easy_item_code == "30"){
          show_patients(this_obj.value);
      }
    <{/if}>
}

<{*
//====================================================================================================
//コメント
//====================================================================================================
*}>
function set_comment(edited_obj, data_obj_id)
{
  $('#' + data_obj_id).val(edited_obj.value);
}

<{*
//====================================================================================================
//表題
//====================================================================================================
*}>
function change_classification()
{
    var subject    = document.getElementById("subject");
    var obj_125_85 = document.getElementsByName("_125_85[]");

    <{if in_array(125, $grp_flags)}>
        for (var i=0; obj_125_85.length > i; i++){
        if (obj_125_85[i].value == subject[subject.selectedIndex].value){
            obj_125_85[i].checked = true;
            document.getElementById("report_title").value = subject[subject.selectedIndex].innerHTML;
            break;
        }
    }
    <{else}>
        document.getElementById("_125_85").value = subject[subject.selectedIndex].value;
    document.getElementById("report_title").value = subject[subject.selectedIndex].innerHTML;
    <{/if}>
}

<{*
//====================================================================================================
//発生場所詳細
//====================================================================================================
*}>
var is_110_65_selection = false;  //発生場所詳細が選択肢を持つかどうかのフラグ
function set_place_detail(val_110_60, val_110_65){
  val_110_65 = (arguments[1]) ? val_110_65 : "";
      <{if ($is_110_65_use)}>
    is_110_65_selection = true;
    <{/if}>
   //if(val_110_65){
    //is_110_65_selection = true;
   //}
  //入力欄を設定
  var html="";
  switch(val_110_60){
    <{foreach from=$grps_115 key=item_id item=list_115}>
      case '<{$item_id}>':
        html = '<table class="box">';
        <{foreach name=list from=$list_115 key=code item=name}>
          <{if $smarty.foreach.list.first || ($smarty.foreach.list.index % 4) == 0}>
            html += '<tr>';
          <{/if}>

          html += '<td width="25%">';
          <{if $name != 'is_extra'}>
            html += '<span class="item_set'+
                      <{if $code==$vals[110][65][0]}>
                        ' checked'+
                      <{/if}>
                      '">';
            html += '<label for="id_110_65_<{$smarty.foreach.list.index}>">';
            html += '<input type="radio" id="id_110_65_<{$smarty.foreach.list.index}>" class="radio" name="_110_65[]"'+
                      ' value="<{$code|escape:"javascript"}>"'+
                      <{if $code==$vals[110][65][0]}>
                        ' checked'+
                      <{/if}>
                    '>';
            html += '<{$name|escape:"javascript"}>';
            html += '</label>';
            html += '</span>';
          <{else}>
            html += '<span></span>';
          <{/if}>
          html += '</td>';

          <{if $smarty.foreach.list.last || ($smarty.foreach.list.index % 4) == 3}>
            html += '</tr>';
          <{/if}>
        <{/foreach}>

        <{*<!--クリアボタン-->*}>
        html += "<tr>";
        html += "<td colspan='4' class='lineB lineR'>";
        html += "<span>";
        html += "<a class='clear_btn cb_type02' href='javascript:void(0);' onclick='clear_item(\"radio\", 110, 65);return false;'>";
        html += "クリア";
        html += "</a>";
        html += "</span>";
        html += "</td>";
        html += "</tr>";

        html += '</table>';
        break;
    <{/foreach}>
    default:
     is_110_65_selection = false;
     if(val_110_65.match(/[^0-9]+/)){
             html = '<input type="text" class="size03" id="id_110_65" name="_110_65" value="'+val_110_65+'">';
      }else{
            html = '<input type="text" class="size03" id="id_110_65" name="_110_65" value="">';
      }
      //html = '<input type="text" class="size03" id="id_110_65" name="_110_65" value="'+val_110_65+'">';
  }
  $("#id_110_65_field").html(html);

  if (is_110_65_selection){
    $('#id_110_65_field').addClass('tabletype02');
    $("#id_110_65_btn a").css("display", "");
  }
  else{
    $('#id_110_65_field').removeClass('tabletype02');
    $("#id_110_65_btn a").css("display", "none");
  }

  <{if !$is_110_65_use}>
    if (val_110_65 == ""){ <{*値がある場合（ロード時に保存値がある場合のみ）は表示のまま*}>
      if (is_110_65_selection){
        show_hide("id_110_65_area", true);
      }
      else{
        show_hide("id_110_65_area", false);
      }
    }
  <{/if}>

  //必須を設定
  <{if in_array("110_60", $item_must)}>
    $("#id_110_65_required").html(is_110_65_selection ? '必須' : "");
  <{/if}>
}

<{*
//====================================================================================================
//発見者
//====================================================================================================
*}>
<{if array_intersect(array(3020, 3030, 3032, 3034, 3036, 3038), $grp_flags)}>
  //---------------------------------------------
  //発見者のプロフィール
  //---------------------------------------------
  //本人プロフィールコピー
  function copyFromSelfProfile2(){
	  <{*発見者職種*}>
      <{if in_array(3020, $grp_flags)}>
        <{if in_array($profile.exp_code, (array)@$item_element_no_disp[3020][10])}>
          clear_item('radio', '3020', '10');
        <{else}>
          if ('<{$profile.exp_code}>' == ''){
            //check_radio(job_grp, false);
          }
          else{
	        var job_grp = $("input:radio[name='_3020_10[]']");
	        job_grp.val(['<{$profile.exp_code}>']);
            var job_item = $("input:radio[name='_3020_10[]']:checked");
            check_radio(job_item, true);
          }
        <{/if}>
      <{/if}>

      <{*発見者氏名*}>
      <{if in_array(3030, $grp_flags)}>
      	<{if $profile.emp_nm != ""}>
        	$("#id_3030_10").val('<{$profile.emp_nm}>');
		<{/if}>        
      <{/if}>

      <{*発見者所属部署*}>
      <{if in_array(3032, $grp_flags)}>
      	  <{if $emp_class_nm != ""}>
          	$("#id_3032_10").val('<{$emp_class_nm}>');
          <{/if}>
      <{/if}>

      <{*専門医・認定医及びその他の医療従事者の専門・認定医資格*}>
      <{if in_array(3034, $grp_flags)}>
      	  <{if $profile.qualification != ""}>
          	$("#id_3034_10").val( '<{$profile.qualification|regex_replace:"/\r?\n/":"<br>"|regex_replace:"/'/":"\'"}>'.replace(/<br>/g, '\n') );
          <{/if}>
      <{/if}>

      <{*発見者職種経験年数*}>
      <{if in_array(3036, $grp_flags)}>
      	  <{if $experience.exp_year != ""}>
          	$("#id_3036_10").val('<{$experience.exp_year}>');
          <{/if}>
          <{if $experience.exp_month != ""}>
          	$("#id_3036_20").val('<{$experience.exp_month}>');
          <{/if}>
      <{/if}>

      <{*発見者部署配属年数*}>
      <{if in_array(3038, $grp_flags)}>
          <{if $experience.dept_year != ""}>
          	$("#id_3038_10").val('<{$experience.dept_year}>');
          <{/if}>
          <{if $experience.dept_month != ""}>
          	$("#id_3038_20").val('<{$experience.dept_month}>');
          <{/if}>
      <{/if}>
  }

  //プロフィール不明コピー
  function setUnknownProfile2(){
      <{*発見者職種*}>
      <{if in_array(3020, $grp_flags)}>
        var job_grp = $("input:radio[name='_3020_10[]']");
        <{if in_array(99, (array)@$item_element_no_disp[3020][10])}>
          clear_item('radio', '3020', '10');
        <{else}>
          job_grp.val(['99']);
          var job_item = $("input:radio[name='_3020_10[]']:checked");
          check_radio(job_item, true);
        <{/if}>
      <{/if}>

      <{*発見者氏名*}>
      <{if in_array(3030, $grp_flags)}>
        $('#id_3030_10').val('不明');
      <{/if}>

      <{*発見者所属部署*}>
      <{if in_array(3032, $grp_flags)}>
        $('#id_3032_10').val('不明');
      <{/if}>

      <{*専門医・認定医及びその他の医療従事者の専門・認定資格*}>
      <{if in_array(3034, $grp_flags)}>
        $('#id_3034_10').val('不明');
      <{/if}>

      <{*発見者職種経験年数*}>
      <{if in_array(3036, $grp_flags)}>
          $("#id_3036_10").val('900');
          $("#id_3036_20").val('90');
      <{/if}>

      <{*発見者部署配属年数*}>
      <{if in_array(3038, $grp_flags)}>
          $("#id_3038_10").val('900');
          $("#id_3038_20").val('90');
      <{/if}>
  }

  //他職員プロフィールコピー
  function copyFromChildWindowProfile2(emp_nm, class_nm, qualification, exp_year, exp_month, dept_year, dept_month, exp_code){
	  <{*発見者職種*}>
      <{if in_array(3020, $grp_flags)}>
        <{if in_array(exp_code, (array)@$item_element_no_disp[3020][10])}>
          clear_item('radio', '3020', '10');
        <{else}>
          if (exp_code){
            var job_grp = $("input:radio[name='_3020_10[]']");
            job_grp.val([exp_code]);
            var job_item = $("input:radio[name='_3020_10[]']:checked");
            check_radio(job_item, true);
          }
          else{
            //check_radio(job_grp, false);
          }
        <{/if}>
      <{/if}>

      <{*発見者氏名*}>
      <{if in_array(3030, $grp_flags)}>
       if (emp_nm != "") {
        	$("#id_3030_10").val(emp_nm);
        }
      <{/if}>

      <{*発見者所属部署*}>
      <{if in_array(3032, $grp_flags)}>
      	if (class_nm != "") {
          $("#id_3032_10").val(class_nm);
      	}
      <{/if}>

      <{*専門医・認定医及びその他の医療従事者の専門・認定医資格*}>
      <{if in_array(3034, $grp_flags)}>
        if (qualification != "") {
          $("#id_3034_10").val(qualification);
        }
      <{/if}>

      <{*発見者職種経験年数*}>
      <{if in_array(3036, $grp_flags)}>
        if (exp_year != "") {
          $("#id_3036_10").val(exp_year);
        }
        if (exp_month != "") {
          $("#id_3036_20").val(exp_month);
        }
      <{/if}>

      <{*発見者部署配属年数*}>
      <{if in_array(3038, $grp_flags)}>
        if (dept_year != "") {
          $("#id_3038_10").val(dept_year);
        }
        if (dept_month != "") {
          $("#id_3038_20").val(dept_month);
        }
      <{/if}>
  }

  function other_profile_copy2(){
      call_emp_search2();
  }

  //職員名簿画面
  function call_emp_search2(){
      var childwin = null;

      dx = screen.width;
      dy = screen.top;
      base = 0;
      wx = 720;
      wy = 600;
      var url = 'hiyari_emp_list.php';
      url += '?session=<{$session}>&call_back_mode=inci_profile&mode=2';
      childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

      childwin.focus();
  }

  function add_emp_list2(emp_id, emp_nm, other_info){
      copyFromChildWindowProfile2(emp_nm, other_info.class_nm, other_info.qualification, other_info.exp_year, other_info.exp_month, other_info.dept_year, other_info.dept_month, other_info.exp_code);
  }
<{/if}>

<{*
//====================================================================================================
//当事者
//====================================================================================================
*}>
<{if array_intersect(array(3050, 3100, 3150, 3200, 3250, 3300, 3350, 3400, 3450), $grp_flags)}>
  //---------------------------------------------
  //当事者の追加・削除
  //---------------------------------------------
  //追加
  function addParty(){
    var parties = $("#parties");
    if (!parties) return;

    var num = $("#parties > table").length;
    if (num >= 10) return;

    //最初の行をコピー
    var html = $("#party_0").html();
    html = html.replace("当事者1", "当事者" + (num+1));
    html = html.replace("copyFromSelfProfile(0)", "copyFromSelfProfile(" + num + ")");
    html = html.replace("other_profile_copy(0)", "other_profile_copy(" + num + ")");
    html = html.replace("setUnknownProfile(0)", "setUnknownProfile(" + num + ")");
    html = html.replace(/(3[0-4][05])0/g, "$1" + num);
    parties.append('<table id="party_' + num + '">' + html + '</table>');

    //コピー先のデータをクリア
    <{*当事者職種*}>
    <{if in_array(3050, $grp_flags)}>
      clear_item('radio', 3050+num, 30);
      clear_item('text', 3050+num, 35);
    <{/if}>

    <{*当事者氏名*}>
    <{if in_array(3100, $grp_flags)}>
      clear_item('text', 3100+num, 40);
    <{/if}>

    <{*当事者所属部署*}>
    <{if in_array(3150, $grp_flags)}>
      clear_item('text', 3150+num, 50);
    <{/if}>

    <{*専門医・認定医及びその他の医療従事者の専門・認定資格*}>
    <{if in_array(3200, $grp_flags)}>
      clear_item('textarea', 3200+num, 60);
    <{/if}>

    <{*当事者職種経験年数*}>
    <{if in_array(3250, $grp_flags)}>
      clear_item('select', 3250+num, 70);
      clear_item('select', 3250+num, 80);
    <{/if}>

    <{*当事者部署配属年数*}>
    <{if in_array(3300, $grp_flags)}>
      clear_item('select', 3300+num, 90);
      clear_item('select', 3300+num, 100);
    <{/if}>

    <{*直前1週間の夜勤回数*}>
    <{if in_array(3350, $grp_flags)}>
      clear_item('radio', 3350+num, 110);
      clear_item('text', 3350+num, 115);
    <{/if}>

    <{*勤務形態*}>
    <{if in_array(3400, $grp_flags)}>
      clear_item('radio', 3400+num, 120);
      clear_item('text', 3400+num, 130);
    <{/if}>

    <{*直前1週間の勤務時間*}>
    <{if in_array(3450, $grp_flags)}>
      clear_item('select', 3450+num, 140);
    <{/if}>
  }

  //削除
  function deleteParty(){
      var parties = $("#parties");
      if (!parties) return;

      var num = $("#parties > table").length;
      if (num < 2) return;

      $("#party_" + (num-1)).remove();
  }

  //---------------------------------------------
  //当事者のプロフィール
  //---------------------------------------------
  //本人プロフィールコピー
  function copyFromSelfProfile(party_index){
      <{*当事者職種*}>
      <{if in_array(3050, $grp_flags)}>
        <{if in_array($profile.exp_code, (array)@$item_element_no_disp[3050][30])}>
          clear_item('radio', '305' + party_index, '30');
        <{else}>
          if ('<{$profile.exp_code}>' == ''){
            //check_radio(job_grp, false);
          }
          else{
            var job_grp = $("input:radio[name='_305" + party_index + "_30[]']");
            job_grp.val(['<{$profile.exp_code}>']);
            var job_item = $("input:radio[name='_305" + party_index + "_30[]']:checked");
            check_radio(job_item, true);
          }
        <{/if}>
      <{/if}>

      <{*当事者氏名*}>
      <{if in_array(3100, $grp_flags)}>
        <{if $profile.emp_nm != ""}>
          $('#id_310' + party_index + '_40').val('<{$profile.emp_nm}>');
        <{/if}>
      <{/if}>

      <{*当事者所属部署*}>
      <{if in_array(3150, $grp_flags)}>
        <{if $emp_class_nm != ""}>
          $('#id_315' + party_index + '_50').val('<{$emp_class_nm}>');
        <{/if}>
      <{/if}>

      <{*専門医・認定医及びその他の医療従事者の専門・認定資格*}>
      <{if in_array(3200, $grp_flags)}>
        <{if $profile.qualification != ""}>
          $('#id_320' + party_index + '_60').val('<{$profile.qualification|regex_replace:"/\r?\n/":"<br>"|regex_replace:"/'/":"\'"}>'.replace(/<br>/g, '\n'));
        <{/if}>
      <{/if}>

      <{*当事者職種経験年数*}>
      <{if in_array(3250, $grp_flags)}>
        <{if $experience.exp_year != ""}>
          $('#id_325' + party_index + '_70').val('<{$experience.exp_year}>');
        <{/if}>
        <{if $experience.exp_month != ""}>
          $('#id_325' + party_index + '_80').val('<{$experience.exp_month}>');
        <{/if}>
      <{/if}>

      <{*当事者部署配属年数*}>
      <{if in_array(3300, $grp_flags)}>
        <{if $experience.dept_year != ""}>
          $('#id_330' + party_index + '_90').val('<{$experience.dept_year}>');
        <{/if}>
        <{if $experience.dept_month != ""}>
          $('#id_330' + party_index + '_100').val('<{$experience.dept_month}>');
        <{/if}>
      <{/if}>
  }

  //プロフィール不明コピー
  function setUnknownProfile(party_index){
      <{*当事者職種*}>
      <{if in_array(3050, $grp_flags)}>
        var job_grp = $("input:radio[name='_305" + party_index + "_30[]']");
        <{if in_array(99, (array)@$item_element_no_disp[3050][30])}>
          clear_item('radio', '305' + party_index, '30');
        <{else}>
          job_grp.val(['99']);
          var job_item = $("input:radio[name='_305" + party_index + "_30[]']:checked");
          check_radio(job_item, true);
        <{/if}>

        $("input:radio[name='_305" + party_index + "_35']").val('不明');
      <{/if}>

      <{*当事者氏名*}>
      <{if in_array(3100, $grp_flags)}>
        $('#id_310' + party_index + '_40').val('不明');
      <{/if}>

      <{*当事者所属部署*}>
      <{if in_array(3150, $grp_flags)}>
        $('#id_315' + party_index + '_50').val('不明');
      <{/if}>

      <{*専門医・認定医及びその他の医療従事者の専門・認定資格*}>
      <{if in_array(3200, $grp_flags)}>
        $('#id_320' + party_index + '_60').val('不明');
      <{/if}>

      <{*当事者職種経験年数*}>
      <{if in_array(3250, $grp_flags)}>
        $('#id_325' + party_index + '_70').val('900');
        $('#id_325' + party_index + '_80').val('90');
      <{/if}>

      <{*当事者部署配属年数*}>
      <{if in_array(3300, $grp_flags)}>
        $('#id_330' + party_index + '_90').val('900');
        $('#id_330' + party_index + '_100').val('90');
      <{/if}>

      <{*直前1週間の夜勤回数*}>
      <{if in_array(3350, $grp_flags)}>
        var yakin_grp = $("input:radio[name='_335" + party_index + "_110[]']");
        yakin_grp.val(['99']);
        var yakin_item = $("input:radio[name='_335" + party_index + "_110[]']:checked");
        check_radio(yakin_item, true);

        $("input:radio[name='_335" + party_index + "_115']").val('不明');
      <{/if}>

      <{*勤務形態*}>
      <{if in_array(3400, $grp_flags)}>
        <{if !in_array(5, (array)@$item_element_no_disp[3400][120])}>
          var kinmu_grp = $("input:radio[name='_340" + party_index + "_120[]']");
          kinmu_grp.val(['5']);
          var kinmu_item = $("input:radio[name='_340" + party_index + "_120[]']:checked");
          check_radio(kinmu_item, true);
        <{/if}>

        $("input:radio[name='_340" + party_index + "_130']").val('不明');
      <{/if}>

      <{*直前1週間の勤務時間*}>
      <{if in_array(3450, $grp_flags)}>
        $('#id_345' + party_index + '_140').val('');
      <{/if}>
  }

  //他職員プロフィールコピー
  function copyFromChildWindowProfile(index, emp_nm, class_nm, qualification, exp_year, exp_month, dept_year, dept_month, exp_code){
      <{*当事者職種*}>
      <{if in_array(3050, $grp_flags)}>
        <{if in_array(exp_code, (array)@$item_element_no_disp[3050][30])}>
          clear_item('radio', '305' + index, '30');
        <{else}>
          if (exp_code){
            var job_grp = $("input:radio[name='_305" + index + "_30[]']");
            job_grp.val([exp_code]);
            var job_item = $("input:radio[name='_305" + index + "_30[]']:checked");
            check_radio(job_item, true);
          }
          else{
            //check_radio(job_grp, false);
          }
        <{/if}>
      <{/if}>

      <{*当事者氏名*}>
      <{if in_array(3100, $grp_flags)}>
        if (emp_nm != "") {
          $('#id_310' + index + '_40').val(emp_nm);
        }
      <{/if}>

      <{*当事者所属部署*}>
      <{if in_array(3150, $grp_flags)}>
        if (class_nm != "") {
          $('#id_315' + index + '_50').val(class_nm);
        }
      <{/if}>

      <{*専門医・認定医及びその他の医療従事者の専門・認定資格*}>
      <{if in_array(3200, $grp_flags)}>
        if (qualification != "") {
        	$('#id_320' + index + '_60').val(qualification);
        }
      <{/if}>

      <{*当事者職種経験年数*}>
      <{if in_array(3250, $grp_flags)}>
        if (exp_year != "") {
        	$('#id_325' + index + '_70').val(exp_year);
        }
        if (exp_month != "") {
          $('#id_325' + index + '_80').val(exp_month);
        }
      <{/if}>

      <{*当事者部署配属年数*}>
      <{if in_array(3300, $grp_flags)}>
        if (dept_year != "") {
          $('#id_330' + index + '_90').val(dept_year);
        }
        if (dept_month != "") {
          $('#id_330' + index + '_100').val(dept_month);
        }
      <{/if}>
  }

  //他の人のプロフィールをコピーします。
  function other_profile_copy(party_index){
      call_emp_search(party_index);
  }

  //職員名簿画面
  function call_emp_search(input_div){
      var childwin = null;

      dx = screen.width;
      dy = screen.top;
      base = 0;
      wx = 720;
      wy = 600;
      var url = 'hiyari_emp_list.php';
      url += '?session=<{$session}>&call_back_mode=inci_profile&input_div=' + input_div;
      childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

      childwin.focus();
  }

  function add_emp_list(emp_id, emp_nm, input_div, other_info){
      copyFromChildWindowProfile(input_div, emp_nm, other_info.class_nm, other_info.qualification, other_info.exp_year, other_info.exp_month, other_info.dept_year, other_info.dept_month, other_info.exp_code);
  }
<{/if}>

<{*
//====================================================================================================
//患者情報
//====================================================================================================
*}>
//患者情報の取得
function getPtid(){
    if (chkInputPtid()){
        var id_objs = document.getElementsByName("_210_10");
        var wk_ptid = id_objs[0].value;

        var url = "db2_hiyari_patient_get.php?session=<{$session}>&patient_id=" + wk_ptid;
        var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=200,top=200,width=420,height=100";
        window.open(url, '_blank',option);
    }
}

//患者IDのチェック
function chkInputPtid(){
    var submitFlag = true;
    var errMsg = "";
    errMsg += checkInput('_210_10', '患者ID');

    if ( errMsg != "" ){
        alert( errMsg );
        submitFlag = false;
    }

    return submitFlag;
}

//患者検索を行います。
function patient_search()
{
    var id_objs = document.getElementsByName("_210_10");
    var pt_id = id_objs[0].value;

    var target_date = false;
    if (document.getElementById("_100_5")){
        target_date = document.getElementById("_100_5").value;
    }
    if (target_date == ""){
        target_date = false;
    }

    if (pt_id == "")
    {
        //患者IDが未入力の場合は子画面検索を行う。
        window.open('hiyari_patient_search.php?session=<{$session}>&target_date='+target_date, 'newwin', 'width=640,height=480,scrollbars=yes');
    }
    else
    {
        //患者IDが入力されている場合はAjax患者検索を行う。
        patient_search_ajax(pt_id, target_date);
    }


}

//Ajax患者検索を開始します。
function patient_search_ajax(pt_id, target_date)
{

    var url = 'hiyari_patient_search_ajax.php';
    var params = $H({'session':'<{$session}>','pt_id':pt_id,'date':target_date}).toQueryString();
    var myAjax = new Ajax.Request(
        url,
        {
            method: 'post',
            postBody: params,
            onComplete: patient_search_ajax_response
        });
}

//Ajax患者検索結果に対する処理を行います。
function patient_search_ajax_response(oXMLHttpRequest)
{
    if (oXMLHttpRequest.status == 200)
    {
        //alert(oXMLHttpRequest.responseText);
        var response = new ajax_response_object(oXMLHttpRequest.responseText);
        if (response.ajax == "success")
        {
            //患者情報を反映
            copyPatientData(response.patient_id, response.patient_name, response.sex, response.age_year, response.age_month, response.in_dt, response.disease, response.class1, response.class2);
        }
        else if (response.ajax == "0hit")
        {
            alert("該当する患者が見つかりませんでした。");
        }
        else
        {
            //処理失敗(何もしない)
        }
    }
    else
    {
        //通信失敗(何もしない)
    }
}

//レスポンスデータよりレスポンスオブジェクトを生成します。(コンストラクタ)
function ajax_response_object(response_text)
{
    var a1 = response_text.split("\n");
    for (var i = 0; i < a1.length; i++)
    {
        var line = a1[i];
        var sep_index = line.indexOf("=");
        if (sep_index == -1)
        {
            break;//最後の改行行と判定
        }
        var key = line.substring(0,sep_index);
        var val = line.substring(sep_index+1);

        var eval_string = "this." + key + " = '" + val.replace("'", "\'") + "';";
        eval(eval_string);
    }
}

//患者情報を反映させます。
function copyPatientData(patient_id, patient_name, sex, age_year, age_month, in_dt, disease, class1, class2){
    if (document.FRM_MAIN.elements['_210_10']){
        document.FRM_MAIN.elements['_210_10'].value = patient_id;
    }
    if (document.FRM_MAIN.elements['_210_20']){
        document.FRM_MAIN.elements['_210_20'].value = patient_name;
    }
    if (document.FRM_MAIN.elements['_210_30']){
        document.FRM_MAIN.elements['_210_30'].value = sex;
    }
    if (document.FRM_MAIN.elements['_210_40']){
        document.FRM_MAIN.elements['_210_40'].value = age_year;
    }
    if (document.FRM_MAIN.elements['_210_50']){
        document.FRM_MAIN.elements['_210_50'].value = age_month;
    }
    if (document.FRM_MAIN.elements['_246_76']){
        document.FRM_MAIN.elements['_246_76'].value = in_dt;
    }
    if (document.FRM_MAIN.elements['_250_80']){
        document.FRM_MAIN.elements['_250_80'].value = disease;
    }
    if (document.FRM_MAIN.elements['_230_60']){
        document.FRM_MAIN.elements['_230_60'].value = class1;
    }
    if (document.FRM_MAIN.elements['_240_70']){
        document.FRM_MAIN.elements['_240_70'].value = class2;
    }
}

//nishiyodo環境患者情報取得
function nishiyodo_patient_search(caller)
{
    //発生日付
    var target_date = "";
    if (document.FRM_MAIN.elements['_100_5'])
    {
        target_date = document.FRM_MAIN.elements['_100_5'].value;
    }

    //患者ID
    var patient_id = "";
    if (document.FRM_MAIN.elements['_210_10'])
    {
        patient_id = document.FRM_MAIN.elements['_210_10'].value;
    }

    //取得結果連携URL
    var this_full_url = document.URL;
    var idx = this_full_url.indexOf("hiyari_easyinput.php");
    var callbacker_url = this_full_url.substr(0,idx) + "hiyari_nishiyodo_patient_search_callbacker.php";

    //呼び出しパラメータセット
    document.patient_search_form.caller.value = caller;
    document.patient_search_form.target_date.value = target_date;
    document.patient_search_form.patient_id.value = patient_id;
    document.patient_search_form.patient_name.value = "";
    document.patient_search_form.page.value = "1";
    document.patient_search_form.callbacker_url.value = callbacker_url;

    //子画面呼び出し
    window.open('', 'newwin', 'width=640,height=480,scrollbars=yes');
    document.patient_search_form.action = "hiyari_patient_search_ny.php";
    document.patient_search_form.target = "newwin";
    document.patient_search_form.submit();
}

//nishiyodo環境患者情報取得結果受信
function call_back_nishiyodo_patient_search(caller,result)
{
    //患者ID
    if (document.FRM_MAIN.elements['_210_10'])
    {
        document.FRM_MAIN.elements['_210_10'].value = result.patient_id;
    }
    //患者氏名
    if (document.FRM_MAIN.elements['_210_20'])
    {
        document.FRM_MAIN.elements['_210_20'].value = result.patient_name;
    }
    //患者の性別
    if (document.FRM_MAIN.elements['_210_30'])
    {
        document.FRM_MAIN.elements['_210_30'].value = result.sex;
    }
    //患者の主治医
    if (document.FRM_MAIN.elements['_243_73'])
    {
        document.FRM_MAIN.elements['_243_73'].value = result.syujii;
    }
    //発生日付があるときのみコピーする項目
    if (result.target_date != "")
    {
        //患者の年齢(年数)
        if (document.FRM_MAIN.elements['_210_40'])
        {
            document.FRM_MAIN.elements['_210_40'].value = result.age_year;
        }
        //患者の年齢(月数)
        if (document.FRM_MAIN.elements['_210_50'])
        {
            document.FRM_MAIN.elements['_210_50'].value = result.age_month;
        }
        //患者区分1
        if (document.FRM_MAIN.elements['_230_60'])
        {
            document.FRM_MAIN.elements['_230_60'].value = result.patient_class1;
        }
        //患者区分2
        if (document.FRM_MAIN.elements['_240_70'])
        {
            document.FRM_MAIN.elements['_240_70'].value = result.patient_class2;
        }
        //入院日
        if (document.FRM_MAIN.elements['_246_76'])
        {
            document.FRM_MAIN.elements['_246_76'].value = result.inpt_date;
        }
    }
}

//CIS環境パターン１患者情報取得結果受信
function call_back_cis1_patient_search(caller,result)
{
    //患者ID
    if (document.FRM_MAIN.elements['_210_10'])
    {
        document.FRM_MAIN.elements['_210_10'].value = result.patient_id;
    }
    //患者氏名
    if (document.FRM_MAIN.elements['_210_20'])
    {
        document.FRM_MAIN.elements['_210_20'].value = result.patient_name;
    }
    //患者の性別
    if (document.FRM_MAIN.elements['_210_30'])
    {
        document.FRM_MAIN.elements['_210_30'].value = result.sex;
    }
    //病棟
    if (document.FRM_MAIN.elements['_1200_10'])
    {
        document.FRM_MAIN.elements['_1200_10'].value = result.byoto;
    }
    //疾患名
    if (document.FRM_MAIN.elements['_250_80'])
    {
        document.FRM_MAIN.elements['_250_80'].value = result.byomei_name1;
    }

    //関連する疾患名１
    if (document.FRM_MAIN.elements['_260_90'])
    {
        document.FRM_MAIN.elements['_260_90'].value = result.byomei_name2;
    }

    //関連する疾患名２
    if (document.FRM_MAIN.elements['_270_100'])
    {
        document.FRM_MAIN.elements['_270_100'].value = result.byomei_name3;
    }

    //関連する疾患名３
    if (document.FRM_MAIN.elements['_280_110'])
    {
        document.FRM_MAIN.elements['_280_110'].value = result.byomei_name4;
    }


    // 関連診療所
    obj_130_90 = document.getElementsByName("_130_90[]");
    if (obj_130_90){
        var bp = false;
        var j  = false;

        for (var i=0; obj_130_90.length > i; i++){
            if (obj_130_90[i].value == result.ka_name){
                obj_130_90[i].checked = true;
                bp = true;
            } else if (obj_130_90[i].value == '38'){
                j = i;
                obj_130_90[i].checked = false;
            } else {
                obj_130_90[i].checked = false;
            }
        }

        if (bp == false && j != false){
            obj_130_90[j].checked = true;
        }
    }
    //発生日付があるときのみコピーする項目
    if (result.target_date != "")
    {
        //患者の年齢(年数)
        if (document.FRM_MAIN.elements['_210_40'])
        {
            document.FRM_MAIN.elements['_210_40'].value = result.age_year;
        }
        //患者の年齢(月数)
        if (document.FRM_MAIN.elements['_210_50'])
        {
            document.FRM_MAIN.elements['_210_50'].value = result.age_month;
        }
        //患者区分1
        if (document.FRM_MAIN.elements['_230_60'])
        {
            document.FRM_MAIN.elements['_230_60'].value = result.patient_class1;
        }
        //患者区分2
        if (document.FRM_MAIN.elements['_240_70'])
        {
            document.FRM_MAIN.elements['_240_70'].value = result.patient_class2;
        }
        //入院日
        if (document.FRM_MAIN.elements['_246_76'])
        {
            document.FRM_MAIN.elements['_246_76'].value = result.inpt_date;
        }
    }
}

//CIS環境パターン１患者情報取得
function cis_patient_search(caller)
{
    //発生日付
    var target_date = "";
    if (document.FRM_MAIN.elements['_100_5'])
    {
        target_date = document.FRM_MAIN.elements['_100_5'].value;
    }

    //患者ID
    var patient_id = "";
    if (document.FRM_MAIN.elements['_210_10'])
    {
        patient_id = document.FRM_MAIN.elements['_210_10'].value;
    }

    //取得結果連携URL
    var this_full_url = document.URL;
    var idx = this_full_url.indexOf("hiyari_easyinput.php");
    var callbacker_url = this_full_url.substr(0,idx) + "hiyari_cis_patient_search_callbacker.php";

    //呼び出しパラメータセット
    document.patient_search_form.caller.value = caller;
    document.patient_search_form.target_date.value = target_date;
    document.patient_search_form.patient_id.value = patient_id;
    document.patient_search_form.patient_name.value = "";
    document.patient_search_form.page.value = "1";
    document.patient_search_form.callbacker_url.value = callbacker_url;

    //子画面呼び出し
    window.open('', 'newwin', 'width=640,height=480,scrollbars=yes');
    document.patient_search_form.action = "hiyari_patient_search_cis.php";
    document.patient_search_form.target = "newwin";
    document.patient_search_form.submit();
}

//国立福島病院様環境患者情報取得
function fukushima_patient_search(caller)
{
    //発生日付
    var target_date = "";
    if (document.FRM_MAIN.elements['_100_5'])
    {
        target_date = document.FRM_MAIN.elements['_100_5'].value;
    }

    //患者ID
    var patient_id = "";
    if (document.FRM_MAIN.elements['_210_10'])
    {
        patient_id = document.FRM_MAIN.elements['_210_10'].value;
    }

    //取得結果連携URL
    var this_full_url = document.URL;
    var idx = this_full_url.indexOf("hiyari_easyinput.php");
    var callbacker_url = this_full_url.substr(0,idx) + "hiyari_fukushima_patient_search_callbacker.php";

    //呼び出しパラメータセット
    document.patient_search_form.caller.value = caller;
    document.patient_search_form.target_date.value = target_date;
    document.patient_search_form.patient_id.value = patient_id;
    document.patient_search_form.patient_name.value = "";
    document.patient_search_form.page.value = "1";
    document.patient_search_form.callbacker_url.value = callbacker_url;

    //子画面呼び出し
    window.open('', 'newwin', 'width=640,height=480,scrollbars=yes');
    document.patient_search_form.action = "hiyari_patient_search_kf.php";
    document.patient_search_form.target = "newwin";
    document.patient_search_form.submit();
}

//国立福島様環境患者情報取得結果受信
function call_back_fukushima_patient_search(caller,result)
{
    //患者ID
    if (document.FRM_MAIN.elements['_210_10'])
    {
        document.FRM_MAIN.elements['_210_10'].value = result.patient_id;
    }
    //患者氏名
    if (document.FRM_MAIN.elements['_210_20'])
    {
        document.FRM_MAIN.elements['_210_20'].value = result.patient_name;
    }
    //患者の性別
    if (document.FRM_MAIN.elements['_210_30'])
    {
        document.FRM_MAIN.elements['_210_30'].value = result.sex;
    }
    //発生日付があるときのみコピーする項目
    if (result.target_date != "")
    {
        //患者の年齢(年数)
        if (document.FRM_MAIN.elements['_210_40'])
        {
            document.FRM_MAIN.elements['_210_40'].value = result.age_year;
        }
        //患者の年齢(月数)
        if (document.FRM_MAIN.elements['_210_50'])
        {
            document.FRM_MAIN.elements['_210_50'].value = result.age_month;
        }
    }
}

//勤医協中央病院様環境患者情報取得
function kinkyo_patient_search(caller)
{
    //発生日付
    var target_date = "";
    if (document.FRM_MAIN.elements['_100_5'])
    {
        target_date = document.FRM_MAIN.elements['_100_5'].value;
    }

    //患者ID
    var patient_id = "";
    if (document.FRM_MAIN.elements['_210_10'])
    {
        patient_id = document.FRM_MAIN.elements['_210_10'].value;
    }

    //取得結果連携URL
    var this_full_url = document.URL;
    var idx = this_full_url.indexOf("hiyari_easyinput.php");
    var callbacker_url = this_full_url.substr(0,idx) + "hiyari_kinkyo_patient_search_callbacker.php";

    //呼び出しパラメータセット
    document.patient_search_form.caller.value = caller;
    document.patient_search_form.target_date.value = target_date;
    document.patient_search_form.patient_id.value = patient_id;
    document.patient_search_form.patient_name.value = "";
    document.patient_search_form.page.value = "1";
    document.patient_search_form.callbacker_url.value = callbacker_url;

    //子画面呼び出し
    window.open('', 'newwin', 'width=640,height=480,scrollbars=yes');
    document.patient_search_form.action = "hiyari_patient_search_kk.php";
    document.patient_search_form.target = "newwin";
    document.patient_search_form.submit();
}

//勤医協中央病院様環境患者情報取得結果受信
function call_back_kinkyo_patient_search(caller,result)
{
    //患者ID
    if (document.FRM_MAIN.elements['_210_10'])
    {
        document.FRM_MAIN.elements['_210_10'].value = result.patient_id;
    }
    //患者氏名
    if (document.FRM_MAIN.elements['_210_20'])
    {
        document.FRM_MAIN.elements['_210_20'].value = result.patient_name;
    }
    //患者の性別
    if (document.FRM_MAIN.elements['_210_30'])
    {
        document.FRM_MAIN.elements['_210_30'].value = result.sex;
    }
    //患者区分1
    if (document.FRM_MAIN.elements['_230_60'])
    {
        document.FRM_MAIN.elements['_230_60'].value = result.patient_class1;
    }
    //患者区分2
    if (document.FRM_MAIN.elements['_240_70'])
    {
        document.FRM_MAIN.elements['_240_70'].value = result.patient_class2;
    }
    /*
    //病棟
    if (document.FRM_MAIN.elements['_1200_10'])
    {
        document.FRM_MAIN.elements['_1200_10'].value = result.byoto;
    }
    //疾患名
    if (document.FRM_MAIN.elements['_250_80'])
    {
        document.FRM_MAIN.elements['_250_80'].value = result.byomei_name1;
    }
    // 関連診療所
    obj_130_90 = document.getElementsByName("_130_90[]");
    if (obj_130_90){
        var bp = false;
        var j  = false;

        for (var i=0; obj_130_90.length > i; i++){
            if (obj_130_90[i].value == result.ka_name){
                obj_130_90[i].checked = true;
                bp = true;
            } else if (obj_130_90[i].value == '38'){
                j = i;
                obj_130_90[i].checked = false;
            } else {
                obj_130_90[i].checked = false;
            }
        }

        if (bp == false && j != false){
            obj_130_90[j].checked = true;
        }
    }
    */
    //発生日付があるときのみコピーする項目
    if (result.target_date != "")
    {
        //患者の年齢(年数)
        if (document.FRM_MAIN.elements['_210_40'])
        {
            document.FRM_MAIN.elements['_210_40'].value = result.age_year;
        }
        //患者の年齢(月数)
        if (document.FRM_MAIN.elements['_210_50'])
        {
            document.FRM_MAIN.elements['_210_50'].value = result.age_month;
        }
        //入院日
        /*if (document.FRM_MAIN.elements['_246_76'])
        {
            document.FRM_MAIN.elements['_246_76'].value = result.inpt_date;
        }*/
    }
}

//iwata環境患者情報取得
function iwata_patient_search(caller)
{
    //発生日付
    var target_date = "";
    if (document.FRM_MAIN.elements['_100_5'])
    {
        target_date = document.FRM_MAIN.elements['_100_5'].value;
    }

    //患者ID
    var patient_id = "";
    if (document.FRM_MAIN.elements['_210_10'])
    {
        patient_id = document.FRM_MAIN.elements['_210_10'].value;
    }

    //取得結果連携URL
    var this_full_url = document.URL;
    var idx = this_full_url.indexOf("hiyari_easyinput.php");
    var callbacker_url = this_full_url.substr(0,idx) + "hiyari_iwata_patient_search_callbacker.php";

    //呼び出しパラメータセット
    document.iwata_patient_search_form.caller.value = caller;
    document.iwata_patient_search_form.target_date.value = target_date;
    document.iwata_patient_search_form.patient_id.value = patient_id;
    document.iwata_patient_search_form.patient_name.value = "";
    document.iwata_patient_search_form.page.value = "1";
    document.iwata_patient_search_form.callbacker_url.value = callbacker_url;

    //子画面呼び出し
    window.open('', 'newwin', 'width=640,height=480,scrollbars=yes');
    document.iwata_patient_search_form.action = "<{$iwata_patient_search_url}>";
    document.iwata_patient_search_form.target = "newwin";
    document.iwata_patient_search_form.submit();
}

//iwata環境患者情報取得結果受信
function call_back_iwata_patient_search(caller,result)
{
    //患者ID
    if (document.FRM_MAIN.elements['_210_10'])
    {
        document.FRM_MAIN.elements['_210_10'].value = result.patient_id;
    }
    //患者氏名
    if (document.FRM_MAIN.elements['_210_20'])
    {
        document.FRM_MAIN.elements['_210_20'].value = result.patient_name;
    }
    //患者の性別
    if (document.FRM_MAIN.elements['_210_30'])
    {
        document.FRM_MAIN.elements['_210_30'].value = result.sex;
    }
    //発生日付があるときのみコピーする項目
    if (result.target_date != "")
    {
        //患者の年齢(年数)
        if (document.FRM_MAIN.elements['_210_40'])
        {
            document.FRM_MAIN.elements['_210_40'].value = result.age_year;
        }
        //患者の年齢(月数)
        if (document.FRM_MAIN.elements['_210_50'])
        {
            document.FRM_MAIN.elements['_210_50'].value = result.age_month;
        }
        //患者区分1
        if (document.FRM_MAIN.elements['_230_60'])
        {
            document.FRM_MAIN.elements['_230_60'].value = result.patient_class1;
        }
        //患者区分2
        if (document.FRM_MAIN.elements['_240_70'])
        {
            document.FRM_MAIN.elements['_240_70'].value = result.patient_class2;
        }
        //入院日
        if (document.FRM_MAIN.elements['_246_76'])
        {
            document.FRM_MAIN.elements['_246_76'].value = result.inpt_date;
        }
    }
}

// 患者情報取得汎用
function patient_search_ex(caller)
{
    // 報告日
    regist_date_y_idx = document.FRM_MAIN.regist_date_y.selectedIndex;
    date_y = document.FRM_MAIN.regist_date_y.options[regist_date_y_idx].value;
    regist_date_m_idx = document.FRM_MAIN.regist_date_m.selectedIndex;
    date_m = document.FRM_MAIN.regist_date_m.options[regist_date_m_idx].value;
    regist_date_d_idx = document.FRM_MAIN.regist_date_d.selectedIndex;
    date_d = document.FRM_MAIN.regist_date_d.options[regist_date_d_idx].value;
    regist_date = date_y + "/" + date_m + "/" + date_d;

    // 発生日付
    var target_date = "";
    if (document.FRM_MAIN.elements['_100_5'])
    {
        target_date = document.FRM_MAIN.elements['_100_5'].value;
    }

    // 患者ID
    var patient_id = "";
    if (document.FRM_MAIN.elements['_210_10'])
    {
        patient_id = document.FRM_MAIN.elements['_210_10'].value;
    }

    // 取得結果連携URL
    var this_full_url = document.URL;
    var idx = this_full_url.indexOf("hiyari_easyinput.php");
    var callbacker_url = this_full_url.substr(0,idx) + "hiyari_patient_search_callbacker.php";

    // 呼び出しパラメータセット
    document.patient_search_form.regist_date.value = regist_date;
    document.patient_search_form.caller.value = caller;
    document.patient_search_form.target_date.value = target_date;
    document.patient_search_form.patient_id.value = patient_id;
    document.patient_search_form.patient_name.value = "";
    document.patient_search_form.page.value = "1";
    document.patient_search_form.callbacker_url.value = callbacker_url;

    // 子画面呼び出し
    window.open('', 'newwin', 'width=640,height=480,scrollbars=yes');
    document.patient_search_form.action = "<{$patient_search_ex_url}>?session=<{$session}>";
    document.patient_search_form.target = "newwin";
    document.patient_search_form.submit();
}

//患者情報取得結果受信汎用
function call_back_patient_search(caller,result)
{
    //患者ID
    if (document.FRM_MAIN.elements['_210_10'])
    {
        document.FRM_MAIN.elements['_210_10'].value = result.patient_id;
    }
    //患者氏名
    if (document.FRM_MAIN.elements['_210_20'])
    {
        document.FRM_MAIN.elements['_210_20'].value = result.patient_name;
    }
    //患者の性別
    if (document.FRM_MAIN.elements['_210_30'])
    {
        document.FRM_MAIN.elements['_210_30'].value = result.sex;
    }
    //発生日付があるときのみコピーする項目
    if (result.target_date != "")
    {
        //患者の年齢(年数)
        if (document.FRM_MAIN.elements['_210_40'])
        {
            document.FRM_MAIN.elements['_210_40'].value = result.age_year;
        }
        //患者の年齢(月数)
        if (document.FRM_MAIN.elements['_210_50'])
        {
            document.FRM_MAIN.elements['_210_50'].value = result.age_month;
        }
        //患者区分1
        if (document.FRM_MAIN.elements['_230_60'])
        {
            document.FRM_MAIN.elements['_230_60'].value = result.patient_class1;
        }
        //患者区分2
        if (document.FRM_MAIN.elements['_240_70'])
        {
            document.FRM_MAIN.elements['_240_70'].value = result.patient_class2;
        }
        //入院日
        if (document.FRM_MAIN.elements['_246_76'])
        {
            document.FRM_MAIN.elements['_246_76'].value = result.inpt_date;
        }
    }
}

<{*
//====================================================================================================
//主治医情報
//====================================================================================================
*}>
//主治医情報を保持しているかどうかを表示します。
function disp_update_doctor_have()
{
    var span = document.getElementById("doctor_have_span");
    if (span){
        if (document.FRM_MAIN.doctor_emp_id.value != ""){
            span.innerHTML = "(職員マスタ連携あり)";
        }
        else{
            span.innerHTML = "";
        }
    }
}

//主治医情報を設定します。
function set_doctor_emp_id(doctor_emp_id)
{
    document.FRM_MAIN.doctor_emp_id.value = doctor_emp_id;
    disp_update_doctor_have();
}

//主治医情報をクリアします。
function clear_doctor_emp_id()
{
    document.FRM_MAIN.doctor_emp_id.value = "";
    disp_update_doctor_have();
}

<{*
//====================================================================================================
//入院日
//====================================================================================================
*}>
<{if $hospital_ymd_flg == 't'}>
  function inpt_search_open(){
      var target_date = false;
      url = 'hiyari_inpatient_search.php?session=<{$session}>&target_date='+target_date;
      window.open(url, 'newwin', 'width=640,height=480,scrollbars=yes');
  }
<{/if}>

<{*
//====================================================================================================
//疾患名の検索
//====================================================================================================
*}>
<{if in_array(250, $grp_flags) || in_array(260, $grp_flags) || in_array(270, $grp_flags) || in_array(280, $grp_flags)}>
    function popupByomeiSelect(id){
        var ipt = document.getElementById(id).value;
        window.open(
            "summary_byoumei_select.php?session=<{$session}>&search_input="+ipt+"&caller="+id,
            "summary_byoumei_select_window",
            "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1000,height=700"
        );
    }

    function call_back_summary_byoumei_select(id, result){
        document.getElementById(id).value = result.byoumei;
    }
<{/if}>

<{*
//====================================================================================================
//インシデント直前の患者の状態の詳細
//====================================================================================================
*}>
<{if in_array(290, $grp_flags) && in_array(295, $grp_flags)}>
  function set_patient_detail(){
    <{foreach name=parent from=$selection[290][120].list item=p}>
      var checkbox_290_obj = document.getElementById("id_290_120_<{$smarty.foreach.parent.index}>");
      if (checkbox_290_obj){
        <{*連動対象の場合*}>
        <{if isset($selection[295][$p.easy_code])}>
          //インシデント直前の患者の状態がチェックされている場合
          if (checkbox_290_obj.checked){
            <{if count($selection[295][$p.easy_code].list) == 0}>
              //詳細が0項目の場合：詳細を非表示(現状、このケースはない。)
              show_hide('id_295_<{$p.easy_code}>_area', false);
            <{elseif count($selection[295][$p.easy_code].list) == 1}>
              //詳細が1項目の場合：詳細を非表示、項目をチェック状態にする。
              show_hide('id_295_<{$p.easy_code}>_area', false);
              document.getElementById("id_295_<{$p.easy_code}>_0").checked = true;
            <{else}>
              //詳細が2項目以上の場合：詳細を表示
              show_hide('id_295_<{$p.easy_code}>_area', true);
            <{/if}>
          }

          //インシデント直前の患者の状態がチェックされていない場合
          else {
            //詳細を非表示
            show_hide('id_295_<{$p.easy_code}>_area', false);
            //詳細項目のチェック状態を解除
            clear_item('checkbox', 295, '<{$p.easy_code}>');
          }
        <{/if}>
      }
    <{/foreach}>
  }
<{/if}>

<{*
//====================================================================================================
//関連する患者情報
//====================================================================================================
*}>
<{if in_array(200, $grp_flags)}>
  function set_patients_num(){
    if (get_radio_value("200","10")==2){
      $('#id_200_30').attr("disabled", false);
    }
    else{
      $('#id_200_30').attr("disabled", true);
      clear_item('select', 200, 30);
      <{if array_intersect(array(310,320,330,340,350,360,370,380,390), $grp_flags)}>
        show_patients(0);
      <{/if}>
    }
  }
<{/if}>

<{if array_intersect(array(310,320,330,340,350,360,370,380,390), $grp_flags)}>
  function show_patients(patients_num)
  {
    if (patients_num > 0){
      $('#patinet_lineWhite').css('display', 'block');
    }
    else{
      $('#patinet_lineWhite').css('display', 'none');
    }

    for (var i=0; i<10; i++){
      if (i < patients_num){
        $('#patient_' + i).css('display', '');
      }
      else{
        $('#patient_' + i).css('display', 'none');
        clear_item('text', 310+i, 10);
        clear_item('text', 320+i, 10);
        clear_item('radio', 330+i, 10);
        clear_item('select', 340+i, 10);
        clear_item('select', 340+i, 20);
        clear_item('radio', 350+i, 10);
        clear_item('radio', 360+i, 10);
        clear_item('text', 370+i, 10);
        clear_item('text', 380+i, 10);
        clear_item('text', 390+i, 10);
      }
    }
  }
<{/if}>

<{*
//====================================================================================================
//アセスメント・患者の状態 20090304
//====================================================================================================
*}>
$(document).ready(function(){
    // アセスメント・患者の状態
    $(".140_10").click(function(){
        if (this.checked){
            $(".150_"+this.value).attr("disabled", false);
            // ラジオ選択チェック
            if ($(".150_"+this.value).attr("type") == 'radio'){
                var radio_flg = false;
                $(".150_"+this.value).each(function(){
                    if (this.checked){
                        $(this).click();
                        radio_flg = true;
                    }
                },radio_flg);
                if (!radio_flg){
                    $(".150_"+this.value).each(function(){
                        $(this).click();
                        this.checked = true;
                        return false;
                    });
                }
            }
        }
        else {
        	$(".150_"+this.value+"_10").each(function(){
                if ($(this).is(':checked')){
					$(this).click();
                }
            });
        	$(".150_"+this.value+"_20").val('');
            
            $(".150_"+this.value).attr("disabled", true);
            var grp1 = $(".150_"+this.value+"_10");
            grp1.attr("disabled", true);
            grp1.closest('span').addClass('disabled');
            var grp2 = $(".150_"+this.value+"_20");
            grp2.attr("disabled", true);
            grp2.closest('span').addClass('disabled');
        }
    });

    // リスク回避用具
    $(".150_6").click(function(){
        var obj_150_6_10 = $(".150_6_10");
        var obj_150_6_20 = $(".150_6_20");
        if (this.value == 10){
            obj_150_6_10.attr("disabled", false);
            obj_150_6_10.closest('span').removeClass('disabled');
        }
        else{
			obj_150_6_10.each(function(){
				if ($(this).is(':checked')){
					$(this).click();
                }
			});
			obj_150_6_20.val('');
            
            obj_150_6_10.attr("disabled", true);
            obj_150_6_10.closest('span').addClass('disabled');
            obj_150_6_20.attr("disabled", true);
        }
        obj_150_6_20.attr("disabled", !$("#150_6_10_99").attr('checked') || $("#150_6_10_99").attr('disabled'));
    });
    // リスク回避器具：有り：その他
    $("#150_6_10_99").click(function(){
        $(".150_6_20").attr("disabled", !this.checked || this.disabled);
    });

    // 拘束用具
    $(".150_7").click(function(){
        var obj_150_7_10 = $(".150_7_10");
        var obj_150_7_20 = $(".150_7_20");
        if (this.value == 10){
            obj_150_7_10.attr("disabled", false);
            obj_150_7_10.closest('span').removeClass('disabled');
        }
        else{
        	obj_150_7_10.each(function(){
				if ($(this).is(':checked')){
					$(this).click();
                }
			});
        	obj_150_7_20.val('');
			
            obj_150_7_10.attr("disabled", true);
            obj_150_7_10.closest('span').addClass('disabled');
            obj_150_7_20.attr("disabled", true);
        }
        obj_150_7_20.attr("disabled", !$("#150_7_10_99").attr('checked') || $("#150_7_10_99").attr('disabled'));
    });
    // リスク回避器具：有り：その他
    $("#150_7_10_99").click(function(){
        $(".150_7_20").attr("disabled", !this.checked || this.disabled);
    });

    // 身体拘束行為
    $(".150_8").click(function(){
        var obj_150_8_10 = $(".150_8_10");
        var obj_150_8_20 = $(".150_8_20");
        if (this.value == 10){
            obj_150_8_10.attr("disabled", false);
            obj_150_8_10.closest('span').removeClass('disabled');
        }
        else{
        	obj_150_8_10.each(function(){
				if ($(this).is(':checked')){
					$(this).click();
                }
			});
        	obj_150_8_20.val('');
			
            obj_150_8_10.attr("disabled", true);
            obj_150_8_10.closest('span').addClass('disabled');
            obj_150_8_20.attr("disabled", true);
        }
        obj_150_8_20.attr("disabled", !$("#150_8_10_99").attr('checked') || $("#150_8_10_99").attr('disabled'));
    });
    // 身体拘束行為：有り：その他
    $("#150_8_10_99").click(function(){
        $(".150_8_20").attr("disabled", !this.checked || this.disabled);
    });

    // ミトン
    $(".150_20").click(function(){
        var obj_150_20_10 = $(".150_20_10");
        if (this.value == 20){
            obj_150_20_10.attr("disabled", false);
            obj_150_20_10.closest('span').removeClass('disabled');
        }
        else{
            obj_150_20_10.attr("disabled", true);
            obj_150_20_10.closest('span').addClass('disabled');
        }
    });
});

<{*
//====================================================================================================
//概要
//====================================================================================================
*}>
<{if in_array(900, $grp_flags)}>
  //---------------------------------------------
  //概要
  //---------------------------------------------
  function set_gaiyo_items(gaiyo_id)
  {
    //種類、発生場面、事例の内容
    if (is_onload){
      set_gaiyo_detail(910, gaiyo_id, '<{$vals[910][10][0]}>', 'gaiyo_kind');
      set_gaiyo_detail(940, gaiyo_id, '<{$vals[940][10][0]}>', 'gaiyo_scene');
      set_gaiyo_detail(970, gaiyo_id, '<{$vals[970][10][0]}>', 'gaiyo_content');
      set_gaiyo_detail(920, '<{$vals[910][10][0]}>', '<{$vals[920][10][0]}>', '', '<{$vals[930][10][0]}>');
      set_gaiyo_detail(950, '<{$vals[940][10][0]}>', '<{$vals[950][10][0]}>', '', '<{$vals[960][10][0]}>');
      set_gaiyo_detail(980, '<{$vals[970][10][0]}>', '<{$vals[980][10][0]}>', '', '<{$vals[990][10][0]}>');
    }
    else{
      set_gaiyo_detail(910, gaiyo_id, -1, 'gaiyo_kind');
      set_gaiyo_detail(940, gaiyo_id, -1, 'gaiyo_scene');
      set_gaiyo_detail(970, gaiyo_id, -1, 'gaiyo_content');
      set_gaiyo_detail(920, -1);
      set_gaiyo_detail(950, -1);
      set_gaiyo_detail(980, -1);
    }

    //医療
    set_iryo_2010(gaiyo_id);
  }

  //---------------------------------------------
  //概要の詳細を設定
  //---------------------------------------------
  function set_gaiyo_detail(grp_code, parent_id, my_id, class_name, other_value)
  {
    var other_grp_code = 0;
    switch (grp_code){
      case 920:
        other_grp_code = 930;
        break;
      case 950:
        other_grp_code = 960;
        break;
      case 980:
        other_grp_code = 990;
        break;
    }

    var area = $("#id_" + grp_code + "_10_area");
    if (area){
      var fld = $("#id_" + grp_code + "_10_field");
      if (fld){
        var html = "";
        var new_line;

        <{foreach from=$gaiyo_selection key=grp_key item=data}>
          if (grp_code == <{$grp_key}>){
            colnum = 2;
            var cnt = colnum;
            <{foreach from=$data.list key=k1 item=v1}>
              if (parent_id == <{$k1}>){
                <{foreach from=$v1 key=k2 item=v2}>
                <{if !isset($v2.sub_item_id) || $v2.sub_item_id!=""}>
                  <{*<!--改行の要否-->*}>
                  new_line = false;
                  <{if isset($v2.other_code)}>
                    new_line = true;
                  <{/if}>
                  if (cnt == colnum){
                    new_line = true;
                  }

                  <{*<!--改行-->*}>
                  if (new_line){
                    <{*<!--必要な場合、改行前に空セルを追加-->*}>
                    if (cnt < colnum){
                      html += "<td colspan='" + (colnum - cnt) + "' class='lineR'><span></span></td>";
                    }

                    html += "</tr><tr>"

                    <{if isset($v2.other_code)}>
                      cnt = colnum;
                    <{else}>
                      cnt = 1;
                    <{/if}>
                  }
                  else{
                    cnt++;
                  }

                  <{*<!-- 選択肢 -->*}>
                  html += "<td";
                  if (cnt == colnum){
                    html += " class='lineR'";
                  }
                  <{if isset($v2.other_code)}>
                    html += " colspan='" + colnum + "'";
                  <{else}>
                    html += " width='50%'";
                  <{/if}>
                  html += ">";

                  <{if $grp_key == 910 || $grp_key == 940 || $grp_key == 970}>
                    html += "<span class='item_set";
                            if (my_id == '<{$v2.item_id}>'){
                              html += " checked";
                            }
                            html += "'>";
                    html += "<label for='id_" + grp_code + "_10_<{$k2}>'>";
                    html += "<input type='radio' id='id_" + grp_code + "_10_<{$k2}>' class='" + class_name + " radio' name='_" + grp_code + "_10[]' value='<{$v2.item_id}>'";
                    if (my_id == '<{$v2.item_id}>'){
                      html += " checked";
                    }
                    html += ">";
                    html += "&nbsp;<span id='id_" + grp_code + "_10_<{$k2}>_text'><{$v2.item_name}></span>";
                  <{else}>
                    html += "<span class='item_set";
                            if (my_id == '<{$v2.sub_item_id}>'){
                              html += " checked";
                            }
                            html += "'>";
                    html += "<label for='id_" + grp_code + "_10_<{$k2}>'>";
                    html += "<input type='radio' id='id_" + grp_code + "_10_<{$k2}>' class='radio' name='_" + grp_code + "_10[]' value='<{$v2.sub_item_id}>'";
                    if (my_id == '<{$v2.sub_item_id}>'){
                      html += " checked";
                    }
                    html += ">";
                    html += "&nbsp;<{$v2.sub_item_name}>";
                  <{/if}>
                  html += "</label>";

                  <{if isset($v2.other_code)}>
                    html += "&nbsp;（";
                    html += "<input type='text' class='text' id='id_" + other_grp_code + "_<{$v2.other_code}>' name='_" + other_grp_code + "_<{$v2.other_code}>'";
                    if (is_onload){
                      html += " value='" + other_value + "'";
                    }
                    html += ">）";
                  <{/if}>

                  html += "</span>";
                  html += "</td>";
                <{/if}>
                <{/foreach}>
              }
            <{/foreach}>
          }
        <{/foreach}>

        if (html != ""){
          <{*<!--空セル-->*}>
          if (cnt < colnum){
            html += "<td colspan='" + (colnum - cnt) + "' class='lineR'><span></span></td>";
          }
          html += "</tr>";

          <{*<!--クリアボタン-->*}>
          html += "<tr>";
          html += "<td colspan='" + colnum + "' class='lineB lineR'>";
          html += "<span>";
          html += "<a class='clear_btn cb_type02' href='javascript:void(0);' onclick='clear_item(\"radio\", " + grp_code + ", 10);return false;'>";
          html += "クリア";
          html += "</a>";
          html += "</span>";
          html += "</td>";
          html += "</tr>";

          html = "<table class='box'>" + html +"</table>";
        }

        fld.html(html);

        if (html == ""){
          show_hide("id_" + grp_code + "_10_area", false);
        }
        else{
          show_hide("id_" + grp_code + "_10_area", true);
        }
      }
    }
  }

  //---------------------------------------------
  //医療
  //---------------------------------------------
  function set_iryo_2010(gaiyo_id)
  {
    //-------------------------------------------
    //値クリア
    //-------------------------------------------
    if (!is_onload){
      //関連医薬品
      <{if in_array(1300, $grp_flags)}>
        document.getElementById("id_1000_5").value = "";
        document.getElementById("id_1000_15").value = "";
      <{/if}>
      //医療材料・諸物品等
      <{if in_array(1310, $grp_flags)}>
        document.getElementById("id_1000_20").value = "";
        document.getElementById("id_1000_25").value = "";
        document.getElementById("id_1000_30").value = "";
      <{/if}>
      //医療機器等
      <{if in_array(1320, $grp_flags)}>
        document.getElementById("id_1000_35").value = "";
        document.getElementById("id_1000_40").value = "";
        document.getElementById("id_1000_45").value = "";
        document.getElementById("id_1000_50").value = "";
        document.getElementById("id_1000_55").value = "";
      <{/if}>
    }

    //-------------------------------------------
    //概要によって表示・非表示を設定
    //-------------------------------------------
    if (gaiyo_id>0){
      obj = document.getElementsByName("_900_10_" + gaiyo_id + "_hdn");
      gaiyo_id = obj[0].value;
    }
    //関連医薬品
    <{if in_array(1300, $grp_flags)}>
        ele1 = document.getElementById("add_item_2010_1");
        <{if array_intersect(array(900), $grp_flags)}>
        	line = document.getElementById("lineWhite_1300");
        <{/if}>
        if (gaiyo_id == 1){
          ele1.style.display = '';
          <{if array_intersect(array(900), $grp_flags)}>
          	line.style.display = 'block';
          <{/if}>
        }
        else{
          ele1.style.display = 'none';
          <{if array_intersect(array(900), $grp_flags)}>
          	line.style.display = 'none';
          <{/if}>
        }
    <{/if}>
    //医療材料・諸物品等
    <{if in_array(1310, $grp_flags)}>
        ele2 = document.getElementById("add_item_2010_2");
        <{if array_intersect(array(900, 1300), $grp_flags)}>
        	line = document.getElementById("lineWhite_1310");
        <{/if}>
        if (gaiyo_id == 3 || gaiyo_id == 4 || gaiyo_id == 5 || gaiyo_id == 6 || gaiyo_id == 7) {
          ele2.style.display = '';
          <{if array_intersect(array(900, 1300), $grp_flags)}>
          	line.style.display = 'block';
          <{/if}>
        }
        else{
          ele2.style.display = 'none';
          <{if array_intersect(array(900, 1300), $grp_flags)}>
          	line.style.display = 'none';
          <{/if}>
        }
    <{/if}>
    //医療機器等
    <{if in_array(1320, $grp_flags)}>
        ele3 = document.getElementById("add_item_2010_3");
        <{if array_intersect(array(900, 1300, 1310), $grp_flags)}>
        	line = document.getElementById("lineWhite_1320");
        <{/if}>
        if (gaiyo_id == 4 || gaiyo_id == 6){
          ele3.style.display = '';
          <{if array_intersect(array(900, 1300, 1310), $grp_flags)}>
          	line.style.display = 'block';
          <{/if}>  	
        }
        else{
          ele3.style.display = 'none';
          <{if array_intersect(array(900, 1300, 1310), $grp_flags)}>
          	line.style.display = 'none';
          <{/if}>
        }
    <{/if}>

    //-------------------------------------------
    //概要が「療養上の世話」(7)の場合、医療材料・諸物品等の必須を外す
    //-------------------------------------------
    <{if in_array(1310, $grp_flags) && in_array("1310_10", $item_must)}>
      var val = (gaiyo_id==7 ? '':'必須');
      $("#id_1000_20_required").html(val);
      $("#id_1000_25_required").html(val);
      $("#id_1000_30_required").html(val);
    <{/if}>
  }

  //---------------------------------------------
  //発生場面と事例の内容の連動（発生場面変更時に事例の内容を設定）
  //---------------------------------------------
  function field_940_970_relate(obj_940)
  {
    var txt_940_10 = $("#" + obj_940.id + "_text").html();
    var obj_970_10 = $("input:radio[name='_970_10[]']")

    for (i=0; i<obj_970_10.length; i++){
      txt_970_10 = $("#" + obj_970_10[i].id + "_text").html();
      if (txt_940_10 == txt_970_10){
          obj_970_10[i].checked = true;
          $('#'+obj_970_10[i].id).closest('table').find('span').removeClass('checked');
          $('#'+obj_970_10[i].id).closest('span').addClass('checked');
          set_gaiyo_detail(980, obj_970_10[i].value);
          break;
      }
    }
  }
<{/if}>

<{*
//===================================================================================================
//事例の詳細(時系列)
//====================================================================================================
*}>
<{if array_intersect(array(4000), $grp_flags)}>
  // 事例の詳細(時系列)のテーブル行「追加」
  function addTable_jirei()
  {
      var tbody = $("#tbody_4000");
      if (!tbody) return;

      var row_cnt = tbody[0].rows.length;
      if (row_cnt >= 50) return;

      <{*追加行のグループコード*}>
      var grp_code = 4000 + row_cnt;

      <{*追加行作成*}>
      var tr = $('<tr></tr>').appendTo(tbody);

      <{*１行目のtdを取得*}>
      var tds = tbody.find('td');

      <{*No欄作成*}>
      tr.append('<td class="center">' + (row_cnt + 1) + '</td>');

      <{*日時欄作成(日付欄は、コピーするとdatepickerが設定した値もコピーされてdatepickerの不具合になるので再作成)*}>
      var td = $('<td></td>').appendTo(tr);
      td.append('<input type="text" id="id_' + grp_code + '_1" class="mR3 size04 calendar_text" name="_' + grp_code + '_1" value="" readonly /> ');
      td.append(tds[1].children[1].outerHTML.replace(/4000/g, grp_code));
      td.append(tds[1].children[2].outerHTML.replace(/4000/g, grp_code) + ' ');
      td.append(tds[1].children[3].outerHTML.replace(/4000/g, grp_code) + '時');
      td.append(tds[1].children[4].outerHTML.replace(/4000/g, grp_code) + '分');
      //td.find('select').css('z-index', 0);

      <{*関係者・物・システム欄作成*}>
      tr.append('<td>' + tds[2].innerHTML.replace(/4000/g, grp_code) + '</td>');

      <{*事象欄作成*}>
      tr.append('<td>' + tds[3].innerHTML.replace(/4000/g, grp_code) + '</td>');

      <{*前の行から日付をコピー*}>
      $("#id_" + grp_code + "_" + 1).val( $("#id_" + (grp_code-1) + "_" + 1).val() );

      <{*日付以外をクリア*}>
      clear_item("select", grp_code, 2);
      clear_item("select", grp_code, 3);
      clear_item("select", grp_code, 4);
      clear_item("text", grp_code, 6);
      clear_item("textarea", grp_code, 5);

      <{*カレンダー*}>
      $('#id_' + grp_code + '_1').datepicker({showOn: 'focus', showOtherMonths: true, selectOtherMonths: true});

      /*
      $('#id_' + grp_code + '_1').live('click', function(){
        $(this).datepicker({showOn: 'focus', showOtherMonths: true, selectOtherMonths: true});
      });
      $('#id_' + grp_code + '_1').removeClass('calendar_text');
      $('#id_' + grp_code + '_1_btn').live('click', function(){$('#id_' + grp_code + '_1').datepicker( 'show' );});
      */
  }

  // 事例の詳細(時系列)のテーブル行「削除」
  function deleteTable_jirei()
  {
      var table = document.getElementById("tbody_4000");
      if (!table) return;
      if (table.rows.length <= 1) return;

      //行削除
      table.deleteRow(-1);
  }

  //事象を子画面で編集
  function edit_phenomenon(grp_code, value){
      //子画面用データ
      document.phenomenon_form.session.value = "<{$session}>";
      document.phenomenon_form.grp_code.value = grp_code;
      document.phenomenon_form.phenomenon.value = value;

      //子画面呼び出し
      window.open('', 'newwin', 'width=500,height=430');
      document.phenomenon_form.target = "newwin";
      document.phenomenon_form.submit();
  }

  //子画面で編集した事象の更新
  function update_phenomenon(grp_code, value){
      $('#id_'+grp_code+'_5').val(value);
  }
<{/if}>

<{*
//====================================================================================================
//影響区分・対応区分
//====================================================================================================
*}>
<{if in_array(1400, $grp_flags)}>
  //---------------------------------------------
  //影響区分の変更時
  //---------------------------------------------
  function set_eikyo_items(val_1400_10){
    //関連項目の値クリア
    if (!is_onload){
      clear_item('text',     1400, 20); <{*部位*}>
      clear_item('checkbox', 1400, 30); <{*患者への影響*}>
      clear_item('text',     1400, 40); <{*出血量*}>
      clear_item('text',     1400, 50); <{*患者への影響：その他*}>
      clear_item('checkbox', 1400, 60); <{*患者への精神的影響*}>
      clear_item('text',     1400, 70); <{*患者への精神的影響：その他*}>
    }

    //関連項目の表示設定
    if (val_1400_10==3){
      show_hide("id_1400_20_area", true);
    }
    else{
      show_hide("id_1400_20_area", false);
    }
    if (val_1400_10==2){
      show_hide("id_1400_40_area", true);
    }
    else{
      show_hide("id_1400_40_area", false);
    }

    var obj_1400_10 = document.getElementsByName("_1400_10[]");
    for (var idx = 0; idx < obj_1400_10.length; idx++){
      var i = obj_1400_10[idx].value;

      <{*患者への影響*}>
      var eikyo_30 = document.getElementById("id_1400_30_"+i+"_area");
      if (eikyo_30) {
        if (i == val_1400_10){
          show_hide("id_1400_30_"+i+"_area", true);
        }
        else{
          show_hide("id_1400_30_"+i+"_area", false);
        }
      }

      <{*患者への精神的影響*}>
      var eikyo_60 = document.getElementById("id_1400_60_"+i+"_area");
      if (eikyo_60) {
        if (i == val_1400_10){
          show_hide("id_1400_60_"+i+"_area", true);
        }
        else{
          show_hide("id_1400_60_"+i+"_area", false);
        }
      }

      <{*患者への影響/患者への精神的影響：その他*}>
      <{*（影響区分毎に表示エリアが存在するが、区分が異なってもその他コードは同じ。全てのエリアに入れておくとnameが重複するので入れ替える）*}>
      if (!is_onload){
        var other_50 = document.getElementById("id_1400_50_"+i);
        if (other_50){
          if (i == val_1400_10){
            other_50.innerHTML = "(<input type='text' class='text' id='id_1400_50' name='_1400_50' value=''>)";
          }
          else{
            other_50.innerHTML = "";
          }
        }

        var other_70 = document.getElementById("id_1400_70_"+i);
        if (other_70){
          if (i == val_1400_10){
            other_70.innerHTML = "(<input type='text' class='text' id='id_1400_70' name='_1400_70' value=''>)";
          }
          else{
            other_70.innerHTML = "";
          }
        }
      }
    }
  }
<{/if}>

<{if in_array(1410, $grp_flags)}>
  //---------------------------------------------
  //対応区分によって関連項目を設定
  //---------------------------------------------
  function set_taio_items(val_1410_10){
    //関連項目の値クリア
    if (!is_onload){
      clear_item('checkbox', 1410, 20); <{*対応*}>
      clear_item('checkbox', 1410, 30); <{*詳細*}>
      clear_item('text',     1410, 40); <{*対応：その他*}>
    }

    //関連項目の表示設定
    var obj_1410_10 = document.getElementsByName("_1410_10[]");
    for (var idx = 0; idx < obj_1410_10.length; idx++){
      var i = obj_1410_10[idx].value;

      <{*対応*}>
      var taio =  document.getElementById("id_1410_20_"+ i+"_area");
      if (taio) {
        if (i == val_1410_10){
          show_hide("id_1410_20_"+ i+"_area",true);
        }
        else{
          show_hide("id_1410_20_"+ i+"_area",false);
        }
      }

      <{*対応：その他*}>
      <{*（対応区分毎に表示エリアが存在するが、区分が異なってもその他コードは同じ。全てのエリアに入れておくとnameが重複するので入れ替える）*}>
      if (!is_onload){
        var other = document.getElementById("id_1410_40_"+i);
        if (other){
          if (i == val_1410_10){
            other.innerHTML = "(<input type='text' class='text' id='id_1410_40' name='_1410_40' value=''>)";
          }
          else{
            if (other) other.innerHTML = "";
          }
        }
      }

      <{*対応の詳細*}>
      set_taio_detail();
    }
  }

  //---------------------------------------------
  //対応によって対応の詳細を設定
  //---------------------------------------------
  function set_taio_detail()
  {
    var taio_options = document.getElementsByName("_1410_20[]");
    for (i=0; taio_options.length>i; i++){
      var taio = taio_options[i];
      var detail_area = document.getElementById("id_1410_30_" + taio.value + "_area")
      if (detail_area){
        if (taio.checked){
          show_hide("id_1410_30_" + taio.value + "_area",true);
        }
        else{
          show_hide("id_1410_30_" + taio.value + "_area",false);
          var detail_options = document.getElementsByName("_1410_30[]");
          for(j=0; detail_options.length>j; j++) {
            var detail = detail_options[j];
            var id_arr = detail.id.split("_");
            if(id_arr[3] == taio.value) {
              detail.checked = false;
              remove_checked_from_checkbox($('#'+detail.id));
              /*
              var span = $('#'+detail.id).closest('span');
              span.removeClass('checked');
              span.closest('li').removeClass('checked');
              */
            }
          }
        }
      }
    }
  }
<{/if}>

<{*
//====================================================================================================
//発生要因
//※grp_code=600のeasy_code が grp_code=605のeasy_item_code
//====================================================================================================
*}>
<{if in_array(600, $grp_flags) && in_array(605, $grp_flags)}>
  function set_factors_detail(){
    <{foreach from=$selection[600][10].list item=item_600 name=n}>
      //発生要因のチェックボックスオブジェクト
      var checkbox_600_obj = document.getElementById("id_600_10_<{$smarty.foreach.n.index}>");
      if (checkbox_600_obj){
        //連動対象の発生要因の場合
        <{if array_key_exists($item_600.easy_code, $grps[605].easy_item_list)}>
          //発生要因がチェックされている場合
          if (checkbox_600_obj.checked){
            <{if count($selection[605][$item_600.easy_code].list) == 0}>
              //詳細が0項目の場合(現状、このケースはない。)
              //詳細を非表示(非表示のままにする。)
              show_hide("id_605_<{$item_600.easy_code}>_area",false);
            <{else}>
              //詳細が2項目以上の場合 詳細を表示
              show_hide("id_605_<{$item_600.easy_code}>_area",true);
            <{/if}>
          }

          //発生要因がチェックされていない場合
          else {
            if ($("#"+"id_605_<{$item_600.easy_code}>_area").css('display') != 'none'){
              //詳細の全項目のチェック状態を解除
              //※非表示のままチェック状態になっているケースもあるため、必ず行う。
              clear_item('<{$grps[605].easy_item_list[$item_600.easy_code].easy_item_type}>', 605, "<{$item_600.easy_code}>");
              //詳細を非表示
              show_hide("id_605_<{$item_600.easy_code}>_area",false);
            }
          }
        <{/if}>
      }
    <{/foreach}>

    //その他の内容クリア
    var chk = document.getElementsByName("_600_10[]");
    for (var i=0; i<chk.length; i++){
      if (!chk.item(i).checked){
        var easy_code = chk.item(i).value;
        if (easy_code <= 12 || easy_code > 18){
          other_code = parseInt(easy_code)+100;
        }
        else if (easy_code > 12){
          other_code = parseInt(easy_code)+99;
        }
        if (easy_code == 18){
          other_code = parseInt(easy_code)+100;
        }
        clear_item('text', 605, other_code);
      }
    }
  }
<{/if}>

<{*
//====================================================================================================
//誤った医療の実施の有無
//====================================================================================================
*}>
function set_medical_error_items(val)
{
  //変更前のエリアを非表示＆クリア
  if (val=="実施あり" || val==""){
    show_hide('id_1100_50_area', false);
    clear_item('radio', 1100, 50);
  }
  if (val=="実施なし" || val==""){
    show_hide('id_1100_20_area', false);
    show_hide('id_1100_30_area', false);
    clear_item('radio', 1100, 20);
    clear_item('radio', 1100, 30);
    clear_item('text', 1100, 40);
  }

  //変更後のエリアを表示
  if (val=="実施あり"){
    show_hide('id_1100_20_area', true);
    show_hide('id_1100_30_area', true);
  }
  if (val=="実施なし"){
    show_hide('id_1100_50_area', true);
  }
}

<{*
//====================================================================================================
//ボタン
//====================================================================================================
*}>
//---------------------------------------------
//不明ボタン
//---------------------------------------------
function unknown_item(grp_code, easy_item_code)
{
  $("#id_" + grp_code + "_" + easy_item_code).val("不明");
}

//---------------------------------------------
//クリアボタン
//---------------------------------------------
function clear_item(type, grp_code, easy_item_code){
  //値のクリア
  switch(type){
  case "checkbox":
    var obj = $("input:checkbox[name='_" + grp_code + "_" + easy_item_code + "[]']");
    obj.attr("checked", false);
    remove_checked_from_checkbox(obj);
    break;
  case "radio":
    var obj = $("input:radio[name='_" + grp_code + "_" + easy_item_code + "[]']");
    obj.attr("checked", false);
    check_radio(obj, false);
    break;
  case "select":
  case "text":
  case "textarea":
  case "calendar":
    $("#id_" + grp_code + "_" + easy_item_code).val("");
    break;
  }

  //連動
  <{*発見・発生日時*}>
  if ((grp_code==100 || grp_code==105) && easy_item_code==5){
    $("#id_" + grp_code + "_10").val("");
    $("#id_" + grp_code + "_20").val("");
    $("#id_" + grp_code + "_30").val("");
  }

  <{*発生場所*}>
  if (grp_code==110 && easy_item_code==60){
    set_place_detail();
  }

  <{*患者の数*}>
  if (grp_code==200 && easy_item_code==10){
    set_patients_num();
  }

  <{*概要*}>
  else if (grp_code==900 && easy_item_code==10){
    set_gaiyo_items(-1);
  }

  <{*概要-種類*}>
  else if (grp_code==910 && easy_item_code==10){
    set_gaiyo_detail(920, -1);
  }

  <{*概要-発生場面*}>
  else if (grp_code==940 && easy_item_code==10){
    set_gaiyo_detail(950, -1);
  }

  <{*概要-事例の内容*}>
  else if (grp_code==970 && easy_item_code==10){
    set_gaiyo_detail(980, -1);
  }

  <{*影響*}>
  else if (grp_code==1400 && easy_item_code==10){
    set_eikyo_items();
  }

  <{*対応*}>
  else if (grp_code==1410 && easy_item_code==10){
    set_taio_items();
    set_taio_detail();
  }

  <{*誤った医療の実施の有無*}>
  else if (grp_code==1100 && easy_item_code==10){
    set_medical_error_items("");
  }
}

<{*
//====================================================================================================
//共通
//====================================================================================================
*}>
//ラジオボタンの選択値取得
function get_radio_value(gcode,itemcode){
  return $("input:radio[name='_"+gcode+"_"+itemcode+"[]']:checked").val();
}

//表示・非表示切替
function show_hide(id, is_show, is_last)
{
  if (is_show){
    if (is_last != null){
      if (is_last){
        $('#'+id + ' th').addClass('last');
        $('#'+id + ' td').addClass('last');
      }
      else{
        $('#'+id + ' th').removeClass('last');
        $('#'+id + ' td').removeClass('last');
      }
    }
    $('#'+id).css('display', '');
  }
  else{
    $('#'+id).css('display', 'none');
  }
}
</script>

<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- 報告日と表題                                                                                           -->
<!-- ---------------------------------------------------------------------------------------------------- -->
<div class="color01 radius mB40 clearfix">
  <table class="mT5">
    <tr>
      <th width="188">● 報告日</th>
      <td>
        <{if $is_report_date_lock}>
          <input type="text" class="report_date" name="regist_date_y" value="<{$regist_date_y}>" readonly>年
          <input type="text" class="report_date" name="regist_date_m" value="<{$regist_date_m}>" readonly>月
          <input type="text" class="report_date" name="regist_date_d" value="<{$regist_date_d}>" readonly>日
        <{else}>
          <{php}>require_once("show_select_values.ini"); <{/php}>
          <select id="regist_date_y" name="regist_date_y">
            <{php}>show_select_years(5, $this->_tpl_vars['regist_date_y'], false);<{/php}>
          </select>年
          <select id="regist_date_m" name="regist_date_m">
            <{php}>show_select_months($this->_tpl_vars['regist_date_m'], false); <{/php}>
          </select>月
          <select id="regist_date_d" name="regist_date_d">
            <{php}>show_select_days($this->_tpl_vars['regist_date_d'], false);<{/php}>
          </select>日
        <{/if}>
      </td>
    </tr>
  </table>
  <span class="lineWhite">　</span>
  <table>
    <tr>
      <th width="188">
        ● <{$report_title_wording}>
        <span class="notice01">必須</span>
      </th>
      <td>
        <{if $subject_mode == 0}>
          <input type="text" name="report_title" class="size03" value="<{$report_title}>">
        <{elseif $subject_mode == 2}>
          <select name="report_title">
            <option value=""></option>
            <{foreach item=item from=$classification}>
              <{if !in_array($item.easy_code, (array)@$item_element_no_disp[125][85])}>
                <option value="<{$item.easy_name}>" <{if $item.easy_name == $report_title}>selected<{/if}>><{$item.easy_name}></option>
              <{/if}>
            <{/foreach}>
          </select>
        <{elseif $subject_mode == 1}>
          <select name="subject" id="subject" onChange="change_classification()">
            <option value=""></option>
            <{foreach item=item from=$classification}>
              <{if !in_array($item.easy_code, (array)@$item_element_no_disp[125][85])}>
                <option value="<{$item.easy_code}>" <{if $item.easy_name == $report_title}>selected<{/if}>><{$item.easy_name}></option>
              <{/if}>
            <{/foreach}>
          </select>
          <input type="hidden" id="report_title" name="report_title" value="<{$report_title}>">
          <{if !in_array(125, $grp_flags)}>
            <input type="hidden" id="_125_85" name="_125_85[]" value="<{$vals[125][85][0]}>">
          <{/if}>
        <{/if}>
      </td>
    </tr>
  </table>
</div>

<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- コメント（評価画面の場合）
<!-- ---------------------------------------------------------------------------------------------------- -->
<{if $input_mode == "analysis"}>
  <{if isset($progress_comment_data)}>
    <div class="section tabletype01">
      <h2 class="type02">コメント</h2>
      <{foreach from=$progress_comment_data key="auth" item="info"}>
        <span class="auth_name"><{$info.auth_name|escape}></span>
        <input type="hidden" name="comment[<{$auth}>][auth_name]" value="<{$info.auth_name}>">
        <div id="progress_comment" class="color02 radius mB10">
          <table class="list">
            <tr>
              <th class="lineB" width="70">入力日付</th>
              <th class="lineB" width="104">担当者</th>
              <th class="none">コメント</th>
            </tr>
            <{foreach from=$info.data item="row" key="k" name="n"}>
              <tr>
                <td class="<{if $smarty.foreach.n.last}> lineB<{/if}>">
                  <{$row.date|escape}>
                  <input type="hidden" name="comment[<{$auth}>][data][<{$k}>][date]" value="<{$row.date}>">
                </td>
                <td class="<{if $smarty.foreach.n.last}> lineB<{/if}>">
                  <{$row.emp_name|escape}>
                  <input type="hidden" name="comment[<{$auth}>][data][<{$k}>][emp_name]" value="<{$row.emp_name|escape}>">
                </td>
                <td class="none<{if $smarty.foreach.n.last}> lineB<{/if}>">
                  <{if $row.is_editable}>
                    <textarea rows="3" name="comment_<{$auth}>" onchange="set_comment(this, 'comment_<{$auth}>_<{$k}>')"><{$row.comment|escape}></textarea>
                  <{else}>
                    <{$row.comment|escape|nl2br}>
                  <{/if}>
                  <input type="hidden" name="comment[<{$auth}>][data][<{$k}>][comment]" id="comment_<{$auth}>_<{$k}>" value="<{$row.comment|escape}>">
                </td>
              </tr>
            <{/foreach}>
          </table>
        </div>
      <{/foreach}>

      <p class="pagetop"><a href="#top">ページの先頭へ</a></p>
      <input type="hidden" name="target_auth_list" value="<{$target_auth_list}>">
    </div>
  <{/if}>
<{/if}>

<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- １．いつ、どこで                                                                                           -->
<!-- ---------------------------------------------------------------------------------------------------- -->
<{if array_intersect(array(105, 100, 116, 110), $grp_flags)}>
  <div class="section tabletype01">
    <h2 class="type02">いつ、どこで</h2>
    <div class="color02 radius">
      <table>
        <{if in_array(105, $grp_flags)}>
          <{*発見日時*}>
          <{include file="report_form_item.tpl" i=105 j=5 title="発見日時" tabletype='tabletype03'}>

          <{*いつごろ(不明の時に記載)*}>
          <{if !$time_setting_flg}>
            <{include file="report_form_item.tpl" i=105 j=50 txt_class="text size07"}>
          <{/if}>
        <{/if}>

        <{if in_array(100, $grp_flags)}>
          <{*発生日時*}>
          <{include file="report_form_item.tpl" i=100 j=5 title="発生日時" tabletype='tabletype03'}>

          <{*いつごろ(不明の時に記載)*}>
          <{if !$time_setting_flg}>
            <{include file="report_form_item.tpl" i=100 j=50 txt_class="text size07"}>
          <{/if}>
        <{/if}>

        <{*発生部署*}>
        <{if in_array(116, $grp_flags)}>
          <{include file="report_form_item.tpl" i=116 j=10 tabletype='tabletype02'}>
        <{/if}>

        <{*発生場所*}>
        <{if in_array(110, $grp_flags)}>
          <{include file="report_form_item.tpl" i=110 j=60 tabletype='tabletype02'}>
        <{/if}>
      </table>

      <table id="id_110_65_area">
        <{*発生場所詳細*}>
        <{if in_array(110, $grp_flags)}>
          <{include file="report_form_item.tpl" i=110 j=65 is_last=true}>
        <{/if}>
      </table>
    </div>
  </div>
  <p class="pagetop"><a href="#top">ページの先頭へ</a></p>
<{/if}>


<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ２．誰が                                                                                             -->
<!-- ---------------------------------------------------------------------------------------------------- -->
<{if array_intersect(array(3000, 3020, 3030, 3032, 3034, 3036, 3038, 3050, 3100, 3150, 3200, 3250, 3300, 3350, 3400, 3450, 3500), $grp_flags)}>
  <div id="section02" class="section tabletype01">
    <h2 class="type02">誰 が</h2>
    <div class="color02 radius">
      <{*発見者*}>
      <{if in_array(3000, $grp_flags)}>
        <table>
          <{include file="report_form_item.tpl" i=3000 j=10 tabletype='tabletype02'}>
        </table>
      <{/if}>

      <{if array_intersect(array(3020, 3030, 3032, 3034, 3036, 3038), $grp_flags)}>
        <{if in_array(3000, $grp_flags)}>
          <span class="lineWhite">　</span>
          <table>
        <{else}>
          <table>
        <{/if}>

          <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
            <tr>
              <th>発見者プロフィール</th>
              <td>
                <{if $self_button_flg == true}>
                  <input type="button" class="button radius_mini" value="本人プロフィール" onclick="copyFromSelfProfile2();" style="width:200px;margin-bottom:2px;">
                <{/if}>
                <{if $other_user_button_flg == true}>
                  <input type="button" class="button radius_mini" value="他の職種プロフィール" onclick="other_profile_copy2();" style="width:200px;margin-bottom:2px;">
                <{/if}>
                <{if $unknown_button_flg == true}>
                  <input type="button" class="button radius_mini" value="不明" onclick="setUnknownProfile2();" style="width:200px;margin-bottom:2px;">
                <{/if}>
              </td>
            </tr>
          <{/if}>

          <{*発見者職種*}>
          <{if in_array(3020, $grp_flags)}>
            <{include file="report_form_item.tpl" i=3020 j=10 tabletype='tabletype02'}>
          <{/if}>

          <{*発見者氏名*}>
          <{if in_array(3030, $grp_flags)}>
            <{include file="report_form_item.tpl" i=3030 j=10}>
          <{/if}>

          <{*発見者所属部署*}>
          <{if in_array(3032, $grp_flags)}>
            <{include file="report_form_item.tpl" i=3032 j=10}>
          <{/if}>

          <{*専門医・認定医及びその他の医療従事者の専門・認定医資格*}>
          <{if in_array(3034, $grp_flags)}>
            <{include file="report_form_item.tpl" i=3034 j=10 row_num=4}>
          <{/if}>

          <{*発見者職種経験年数*}>
          <{if in_array(3036, $grp_flags)}>
            <{include file="report_form_item.tpl" i=3036 j=10}>
          <{/if}>

          <{*発見者部署配属年数*}>
          <{if in_array(3038, $grp_flags)}>
            <{include file="report_form_item.tpl" i=3038 j=10}>
          <{/if}>
        </table>
      <{/if}>

      <{*当事者*}>
      <{if array_intersect(array(3050, 3100, 3150, 3200, 3250, 3300, 3350, 3400, 3450), $grp_flags)}>
        <{if array_intersect(array(3000, 3020, 3030, 3032, 3034, 3036, 3038), $grp_flags)}>
          <span class="lineWhite">　</span>
          <div id="parties">
        <{else}>
          <div id="parties">
        <{/if}>
          <{section name=parties_index loop=10}>
            <{assign var="index" value=$smarty.section.parties_index.index}>
            <{if $index==0 || array_intersect(array(3050 + $index, 3100 + $index, 3150 + $index, 3200 + $index, 3250 + $index, 3300 + $index, 3350 + $index, 3400 + $index, 3450 + $index), array_keys($vals))}>
              <table id="party_<{$index}>">
                <tbody>
                  <tr>
                    <{if $self_button_flg == true || $other_user_button_flg == true || $unknown_button_flg == true}>
                      <th>
                        当事者<{$index+1}>
                      </th>
                      <td>
                        <{if $self_button_flg == true}>
                          <input type="button" class="button radius_mini" value="本人プロフィール" onclick="copyFromSelfProfile(<{$index}>);" style="width:200px;">
                        <{/if}>
                        <{if $other_user_button_flg == true}>
                          <input type="button" class="button radius_mini" value="他の職種プロフィール" onclick="other_profile_copy(<{$index}>);" style="width:200px;">
                        <{/if}>
                        <{if $unknown_button_flg == true}>
                          <input type="button" class="button radius_mini" value="不明" onclick="setUnknownProfile(<{$index}>);" style="width:200px;">
                        <{/if}>
                      </td>
                    <{else}>
                      <th colspan="3">
                        当事者<{$index+1}>
                      </th>
                    <{/if}>
                  </tr>

                  <{*当事者職種*}>
                  <{if in_array(3050, $grp_flags)}>
                    <{include file="report_form_item.tpl" i=$index+3050 j=30 tabletype='tabletype02'}>
                  <{/if}>

                  <{*当事者氏名*}>
                  <{if in_array(3100, $grp_flags)}>
                    <{include file="report_form_item.tpl" i=$index+3100 j=40}>
                  <{/if}>

                  <{*当事者所属部署*}>
                  <{if in_array(3150, $grp_flags)}>
                    <{include file="report_form_item.tpl" i=$index+3150 j=50}>
                  <{/if}>

                  <{*専門医・認定医及びその他の医療従事者の専門・認定資格*}>
                  <{if in_array(3200, $grp_flags)}>
                    <{include file="report_form_item.tpl" i=$index+3200 j=60 row_num=4}>
                  <{/if}>

                  <{*当事者職種経験年数*}>
                  <{if in_array(3250, $grp_flags)}>
                    <{include file="report_form_item.tpl" i=$index+3250 j=70}>
                  <{/if}>

                  <{*当事者部署配属年数*}>
                  <{if in_array(3300, $grp_flags)}>
                    <{include file="report_form_item.tpl" i=$index+3300 j=90}>
                  <{/if}>

                  <{*直前1週間の夜勤回数*}>
                  <{if in_array(3350, $grp_flags)}>
                    <{include file="report_form_item.tpl" i=$index+3350 j=110 tabletype='tabletype02'}>
                  <{/if}>

                  <{*勤務形態*}>
                  <{if in_array(3400, $grp_flags)}>
                    <{include file="report_form_item.tpl" i=$index+3400 j=120 tabletype='tabletype02'}>
                  <{/if}>

                  <{*直前1週間の勤務時間*}>
                  <{if in_array(3450, $grp_flags)}>
                    <{include file="report_form_item.tpl" i=$index+3450 j=140}>
                  <{/if}>
                </tbody>
              </table>
            <{/if}>
          <{/section}>
        </div>

        <div id="party_btn" class="optionBtn">
          <input type="button" class="button radius_mini" value="追加" onclick="addParty();">
          <input type="button" class="button radius_mini" value="削除" onclick="deleteParty();">
        </div>
      <{/if}>

      <{*当事者以外の関連職種*}>
      <{if in_array(3500, $grp_flags)}>
        <{if array_intersect(array(3000, 3020, 3030, 3032, 3034, 3036, 3038, 3050, 3100, 3150, 3200, 3250, 3300, 3350, 3400, 3450), $grp_flags)}>
          <span class="lineWhite">　</span>
          <table>
        <{else}>
          <table>
        <{/if}>
          <{include file="report_form_item.tpl" i=3500 j=150 tabletype='tabletype02'}>
        </table>
      <{/if}>
    </div>
  </div>
  <p class="pagetop"><a href="#top">ページの先頭へ</a></p>
<{/if}>


<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ３．誰に                                                                                             -->
<!-- ---------------------------------------------------------------------------------------------------- -->
<{if array_intersect(array(200, 210, 230, 240, 243, 246, 1200, 250, 260, 270, 280, 130, 290, 140), $grp_flags)}>
  <div id="section03" class="section tabletype01">
    <h2 class="type02">誰 に</h2>
    <div class="color02 radius">
      <{if array_intersect(array(200, 210, 230, 240, 243, 246, 1200, 250, 260, 270, 280), $grp_flags)}>
        <table>
          <{*患者の数*}>
          <{if in_array(200, $grp_flags)}>
            <{include file="report_form_item.tpl" i=200 j=10}>
          <{/if}>

          <{if in_array(210, $grp_flags)}>
            <{if in_array(10, $patient_use) || in_array(20, $patient_use)}>
              <{*患者ID*}>
              <{if in_array(10, $patient_use)}>
                <{include file="report_form_item.tpl" i=210 j=10 txt_class="text text_numeric"}>
              <{/if}>

              <{*患者氏名*}>
              <{if in_array(20, $patient_use)}>
                <{include file="report_form_item.tpl" i=210 j=20}>
              <{/if}>
            <{/if}>

            <{*患者の性別*}>
            <{if in_array(30, $patient_use)}>
              <{include file="report_form_item.tpl" i=210 j=30 tabletype='tabletype03'}>
            <{/if}>

            <{*患者の年齢*}>
            <{if in_array(40, $patient_use) || in_array(50, $patient_use)}>
              <{include file="report_form_item.tpl" i=210 j=40}>
            <{/if}>
          <{/if}>

          <{*患者区分1*}>
          <{if in_array(230, $grp_flags)}>
            <{include file="report_form_item.tpl" i=230 j=60 tabletype='tabletype03'}>
          <{/if}>

          <{*患者区分2*}>
          <{if in_array(240, $grp_flags)}>
            <{include file="report_form_item.tpl" i=240 j=70 tabletype='tabletype03'}>
          <{/if}>

          <{*主治医*}>
          <{if in_array(243, $grp_flags)}>
            <{include file="report_form_item.tpl" i=243 j=73}>
            <input type='hidden' name='doctor_emp_id'       id='doctor_emp_id'       value='<{$doctor_emp_id}>'>
          <{/if}>

          <{*入院日*}>
          <{if in_array(246, $grp_flags)}>
            <{include file="report_form_item.tpl" i=246 j=76}>
          <{/if}>

          <{*病棟名*}>
          <{if in_array(1200, $grp_flags)}>
            <{include file="report_form_item.tpl" i=1200 j=10}>
          <{/if}>

          <{*インシデントに直接関連する疾患名*}>
          <{if in_array(250, $grp_flags)}>
            <{include file="report_form_item.tpl" i=250 j=80}>
          <{/if}>

          <{*関連する疾患名1*}>
          <{if in_array(260, $grp_flags)}>
            <{include file="report_form_item.tpl" i=260 j=90}>
          <{/if}>

          <{*関連する疾患名2*}>
          <{if in_array(270, $grp_flags)}>
            <{include file="report_form_item.tpl" i=270 j=100}>
          <{/if}>

          <{*関連する疾患名3*}>
          <{if in_array(280, $grp_flags)}>
            <{include file="report_form_item.tpl" i=280 j=110}>
          <{/if}>
        </table>
      <{/if}>

      <{*関連診療科*}>
      <{if in_array(130, $grp_flags)}>
        <{if array_intersect(array(200, 210, 230, 240, 243, 246, 1200, 250, 260, 270, 280), $grp_flags)}>
          <span class="lineWhite">　</span>
          <table>
        <{else}>
          <table>
        <{/if}>

          <{include file="report_form_item.tpl" i=130 j=90 tabletype='tabletype02'}>
        </table>
      <{/if}>

      <{if in_array(290, $grp_flags)}>
        <{if array_intersect(array(200, 210, 230, 240, 243, 246, 1200, 250, 260, 270, 280, 130), $grp_flags)}>
          <span class="lineWhite">　</span>
          <table>
        <{else}>
          <table>
        <{/if}>
          <{*インシデント直前の患者の状態*}>
          <{include file="report_form_item.tpl" i=290 j=120 tabletype='tabletype02'}>
        </table>

        <{*インシデント直前の患者の状態の詳細*}>
        <{if in_array(295, $grp_flags)}>
          <{foreach name=parent from=$selection[290][120].list item=p}>
            <{if isset($selection[295][$p.easy_code])}>
              <table id="id_295_<{$p.easy_code}>_area">
                <{include file="report_form_item.tpl" i=295 j=$p.easy_code is_last=$smarty.foreach.parent.last tabletype='tabletype02'}>
              </table>
            <{/if}>
          <{/foreach}>
        <{/if}>
      <{/if}>

      <{*アセスメント・患者の状態*}>
      <{if in_array(140, $grp_flags)}>
        <{if array_intersect(array(200, 210, 230, 240, 243, 246, 1200, 250, 260, 270, 280, 130, 290), $grp_flags)}>
          <span class="lineWhite">　</span>
          <table>
        <{else}>
          <table>
        <{/if}>
          <{include file="report_form_item.tpl" i=140 j=10 tabletype='tabletype04'}>
        </table>
      <{/if}>

      <{*関連する患者情報*}>
      <{if array_intersect(array(310,320,330,340,350,360,370,380,390), $grp_flags)}>
        <{section name=patients_index loop=10}>
          <{assign var="index" value=$smarty.section.patients_index.index}>
          <{if $index==0 and  array_intersect(array(200, 210, 230, 240, 243, 246, 1200, 250, 260, 270, 280, 130, 290), $grp_flags)}>
            <span id="patinet_lineWhite" class="lineWhite">　</span>
            <table id="patient_<{$index}>">
          <{else}>
            <table id="patient_<{$index}>">
          <{/if}>
            <tbody>
              <tr>
                <th width="188">● <span class="strong">関連する患者情報<{$index+1}></span></th>
                <td class="tabletype04 last"></td>
              </tr>

              <{*grp_code:310=患者ID,320=患者氏名,330=患者の性別,340=患者の年齢,350=患者区分1,360=患者区分2,370=主治医,380=入院日,390=病棟名*}>
              <{section name=groups start=310 step=10 loop=400}>
                <{if $smarty.section.groups.index==330 || $smarty.section.groups.index==350 || $smarty.section.groups.index==360}>
                  <{assign var="tabletype" value="tabletype03"}>
                <{else}>
                  <{assign var="tabletype" value=""}>
                <{/if}>
                <{if in_array($smarty.section.groups.index, $grp_flags)}>
                  <{if $smarty.section.groups.index>=310 && $smarty.section.groups.index<=319}>
                    <{include file="report_form_item.tpl" i=$smarty.section.groups.index+$index j=10 txt_class="text text_numeric" is_last=$smarty.section.groups.last}>
                  <{else}>
                    <{include file="report_form_item.tpl" i=$smarty.section.groups.index+$index j=10 is_last=$smarty.section.groups.last}>
                  <{/if}>
                <{/if}>
              <{/section}>

            </tbody>
          </table>
        <{/section}>
      <{/if}>
    </div>
  </div>
  <p class="pagetop"><a href="#top">ページの先頭へ</a></p>
<{/if}>


<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ４．何をどうした                                                                                     -->
<!-- ---------------------------------------------------------------------------------------------------- -->
<{if array_intersect(array(900, 1300, 1310, 1320), $grp_flags)}>
  <div id="section04" class="section tabletype01">
    <h2 class="type02">何をどうした</h2>
    <div class="color02 radius">

      <{if in_array(900, $grp_flags)}>
        <{*概要*}>
        <table>
          <{include file="report_form_item.tpl" i=900 j=10 tabletype='tabletype02'}>
        </table>

        <{*種類・項目*}>
        <table id="id_910_10_area">
          <{include file="report_form_item.tpl" i=910 j=10 tabletype='tabletype02'}>
        </table>
        <table id="id_920_10_area">
          <{include file="report_form_item.tpl" i=920 j=10 tabletype='tabletype02'}>
        </table>

        <{*発生場面・項目*}>
        <table id="id_940_10_area">
          <{include file="report_form_item.tpl" i=940 j=10 tabletype='tabletype02'}>
        </table>
        <table id="id_950_10_area">
          <{include file="report_form_item.tpl" i=950 j=10 tabletype='tabletype02'}>
        </table>

        <{*事例の内容・項目*}>
        <table id="id_970_10_area">
          <{include file="report_form_item.tpl" i=970 j=10 tabletype='tabletype02'}>
        </table>
        <table id="id_980_10_area">
          <{include file="report_form_item.tpl" i=980 j=10 tabletype='tabletype02' is_last=true}>
        </table>
      <{/if}>

      <{*関連医薬品*}>
      <{if in_array(1300, $grp_flags)}>
        <{if array_intersect(array(900), $grp_flags)}>
          <span id="lineWhite_1300" class="lineWhite">　</span>
          <table id="add_item_2010_1">
        <{else}>
          <table id="add_item_2010_1">
        <{/if}>
          <{include file="report_form_item.tpl" i=1000 j=5  m='1300_10' title="関連医薬品の販売名" txt_class="text size07"}>
          <{include file="report_form_item.tpl" i=1000 j=15 m='1300_10' title="製造販売業者名" txt_class="text size07"}>
        </table>
      <{/if}>

      <{*医療材料・諸物品等*}>
      <{if in_array(1310, $grp_flags)}>
        <{if array_intersect(array(900, 1300), $grp_flags)}>
          <span id="lineWhite_1310" class="lineWhite">　</span>
          <table id="add_item_2010_2">
        <{else}>
          <table id="add_item_2010_2">
        <{/if}>
          <{include file="report_form_item.tpl" i=1000 j=20 m='1310_10' title="医療材料・諸物品等の販売名" txt_class="text size07"}>
          <{include file="report_form_item.tpl" i=1000 j=25 m='1310_10' title="製造販売業者名" txt_class="text size07"}>
          <{include file="report_form_item.tpl" i=1000 j=30 m='1310_10' title="購入年月日" txt_class="text size07"}>
        </table>
      <{/if}>

      <{*医療機器等*}>
      <{if in_array(1320, $grp_flags)}>
        <{if array_intersect(array(900, 1300, 1310), $grp_flags)}>
          <span id="lineWhite_1320" class="lineWhite">　</span>
          <table id="add_item_2010_3">
        <{else}>
          <table id="add_item_2010_3">
        <{/if}>
          <{include file="report_form_item.tpl" i=1000 j=35 m='1320_10' title="医療機器等の販売名" txt_class="text size07"}>
          <{include file="report_form_item.tpl" i=1000 j=40 m='1320_10' title="製造販売業者名" txt_class="text size07"}>
          <{include file="report_form_item.tpl" i=1000 j=45 m='1320_10' title="製造年月" txt_class="text size07"}>
          <{include file="report_form_item.tpl" i=1000 j=50 m='1320_10' title="購入年月" txt_class="text size07"}>
          <{include file="report_form_item.tpl" i=1000 j=55 m='1320_10' title="直近の保守・点検年月" txt_class="text size07"}>
        </table>
      <{/if}>
    </div>
  </div>
  <p class="pagetop"><a href="#top">ページの先頭へ</a></p>
<{/if}>

<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ５．内容                                                                               -->
<!-- ---------------------------------------------------------------------------------------------------- -->
<{if array_intersect(array(500, 520, 525, 530, 4000, 1400), $grp_flags)}>
  <div id="section05" class="section tabletype01">
    <h2 class="type02">内 容</h2>
    <div class="color02 radius">
      <{if array_intersect(array(500, 520, 525, 530), $grp_flags)}>
        <table>
          <{*実施した医療行為の目的*}>
          <{if in_array(500, $grp_flags)}>
            <{include file="report_form_item.tpl" i=500 j=10}>
          <{/if}>

          <{*インシデントの内容*}>
          <{if in_array(520, $grp_flags)}>
            <{include file="report_form_item.tpl" i=520 j=30}>
          <{/if}>
          
          <{*気づき R.C 20140604*}>
          <{if in_array(525, $grp_flags)}>
            <{include file="report_form_item.tpl" i=525 j=10}>
          <{/if}>

          <{*インシデント発生（発見）時の経過*}>
          <{if in_array(530, $grp_flags)}>
            <{include file="report_form_item.tpl" i=530 j=40}>
          <{/if}>
        </table>
      <{/if}>

      <{*事例の詳細（時系列）4000〜4049*}>
      <{if in_array(4000, $grp_flags)}>
        <{if array_intersect(array(500, 520, 525, 530), $grp_flags)}>
          <span class="lineWhite">　</span>
          <table id="jirei" class="tabletype09 next_block">
        <{else}>
          <table class="tabletype09">
        <{/if}>
          <thead>
            <tr>
              <th class="lineB" colspan="4">
                <span class="strong">事例の詳細(時系列)</span>
                <span class="btn_area floatR">
                  <input type="button" class="button radius_mini" value="行を追加する" onclick="addTable_jirei();"/>
                  <input type="button" class="button mL10 radius_mini" value="最後の行を削除する" onclick="deleteTable_jirei();"/>
                </span>
              </th>
            </tr>
            <tr>
              <th width="24" class="center">No</th>
              <th width="360"><{include file="report_form_title.tpl" i=4000 j=1}></th>
              <th width="270"><{include file="report_form_title.tpl" i=4000 j=4}></th>
              <th width="256"><{include file="report_form_title.tpl" i=4000 j=5}></th>
            </tr>
          </thead>
          <tbody id="tbody_4000">
            <{section name=tbody_4000 loop=50}>
              <{assign var="index" value=$smarty.section.tbody_4000.index}>
              <{math assign=i4000 equation="x + y" x=$index y=4000}>
              <{if (array_intersect(array($i4000), $grp_flags) || array_intersect(array($i4000), array_keys($vals))) && ($i4000 == 4000 || !empty($vals[$i4000][1][0]) || !empty($vals[$i4000][2][0]) || !empty($vals[$i4000][3][0]) || !empty($vals[$i4000][4][0]) || !empty($vals[$i4000][5][0]) || !empty($vals[$i4000][6][0]))}>
                <{math assign=i4000_no equation="xx + yy" xx=$index yy=1}>
                <tr>
                  <td class="center">
                    <{$i4000_no}>
                  </td>

                  <{*日時*}>
                  <td>
                      <input type="text" id="id_<{$i4000}>_1" class="mR3 size04 calendar_text" name="_<{$i4000}>_1" value="<{$vals[$i4000][1][0]}>" readonly />
                      <a class="clear_btn cb_type02" href="javascript:void(0);"
                        onclick="clear_item('text','<{$i4000}>','1');clear_item('select','<{$i4000}>','2');clear_item('select','<{$i4000}>','3');">
                        クリア
                      </a>
                      <a class="calendar_btn" href="javascript:void(0);" id="id_<{$i4000}>_1_btn">カレンダー</a>
                      <select id="id_<{$i4000}>_2" class="mL5" name="_<{$i4000}>_2" onchange="selectbox_onchange(this);">
                        <option value=""></option>
                        <{section name=k2 loop=$grps[$i4000].easy_item_list[2].easy_list}>
                          <{assign var="easy_4000_2" value=$grps[$i4000].easy_item_list[2].easy_list[k2]}>
                          <{if $item_element_no_disp[$i4000][2] == "" || !in_array($easy_4000_2.easy_code, $item_element_no_disp[$i4000][2])}>
                            <option value="<{$easy_4000_2.easy_code}>" <{if in_array($easy_4000_2.easy_code, (array)@$vals[$i4000][2])}> selected<{/if}>><{$easy_4000_2.easy_name|escape}></option>
                          <{/if}>
                        <{/section}>
                      </select>時
                      <select id="id_<{$i4000}>_3" class="mL5" name="_<{$i4000}>_3" onchange="selectbox_onchange(this);">
                        <option value=""></option>
                        <{section name=k3 loop=$grps[$i4000].easy_item_list[3].easy_list}>
                          <{assign var="easy_4000_3" value=$grps[$i4000].easy_item_list[3].easy_list[k3]}>
                          <{if $item_element_no_disp[$i4000][3] == "" || !in_array($easy_4000_3.easy_code, $item_element_no_disp[$i4000][3])}>
                            <option value="<{$easy_4000_3.easy_code}>" <{if in_array($easy_4000_3.easy_code, (array)@$vals[$i4000][3])}> selected<{/if}>><{$easy_4000_3.easy_name|escape}></option>
                          <{/if}>
                        <{/section}>
                      </select>分
                  </td>

                  <{*関係者・物・システム*}>
                  <td class="jirei_middle">
                    <select id="id_<{$i4000}>_4" class="mL0 mR5 size08" name="_<{$i4000}>_4" onchange="selectbox_onchange(this);">
                      <option value=""></option>
                      <{section name=k4 loop=$grps[$i4000].easy_item_list[4].easy_list}>
                        <{assign var="easy_4000_4" value=$grps[$i4000].easy_item_list[4].easy_list[k4]}>
                        <{if $item_element_no_disp[$i4000][4] == "" || !in_array($easy_4000_4.easy_code, $item_element_no_disp[$i4000][4])}>
                          <option value="<{$easy_4000_4.easy_code}>" <{if in_array($easy_4000_4.easy_code, (array)@$vals[$i4000][4])}> selected<{/if}>><{$easy_4000_4.easy_name|escape}></option>
                        <{/if}>
                      <{/section}>
                    </select>
                    <input type="text" id="id_<{$i4000}>_6" class="text size08" name="_<{$i4000}>_6" value="<{$vals[$i4000][6][0]|escape}>" />
                  </td>

                  <{*事象*}>
                  <td>
                    <textarea id="id_<{$i4000}>_5" name="_<{$i4000}>_5"
                      <{if $is_grpcode_4000_template}>readonly onclick="textarea_onclick(this);"<{/if}>><{$vals[$i4000][5][0]|escape}></textarea>
                  </td>
                </tr>
              <{/if}>
            <{/section}>
          </tbody>
          <tfoot>
            <tr>
              <th colspan="4">
                <span class="btn_area floatR">
                  <input type="button" class="button radius_mini" value="行を追加する" onclick="addTable_jirei();"/>
                  <input type="button" class="button mL10 radius_mini" value="最後の行を削除する" onclick="deleteTable_jirei();"/>
                </span>
              </th>
            </tr>
          </tfoot>
        </table>
      <{/if}>

      <{*患者への影響*}>
      <{if in_array(1400, $grp_flags)}>
        <{*影響区分*}>
        <{if array_intersect(array(500, 520, 525, 530, 4000), $grp_flags)}>
          <span class="lineWhite">　</span>
          <table>
        <{else}>
          <table>
        <{/if}>
          <{include file="report_form_item.tpl" i=1400 j=10 tabletype='tabletype03'}>
        </table>

        <{*部位*}>
        <table id="id_1400_20_area">
          <{include file="report_form_item.tpl" i=1400 j=20}>
        </table>

        <{*患者への影響*}>
        <{foreach item=influence key=key from=$ic_influence}>
          <table id="id_1400_30_<{$key}>_area">
            <{include file="report_form_item.tpl" i=1400 j=30 ic=$influence key=$key tabletype='tabletype02'}>
          </table>
        <{/foreach}>

        <{*出血量*}>
        <table id="id_1400_40_area">
          <{include file="report_form_item.tpl" i=1400 j=40}>
        </table>

        <{*患者への精神的影響*}>
        <{foreach item=mental key=key from=$ic_mental}>
          <table id="id_1400_60_<{$key}>_area">
            <{include file="report_form_item.tpl" i=1400 j=60 ic=$mental key=$key tabletype='tabletype02'}>
          </table>
        <{/foreach}>
      <{/if}>
    </div>
  </div>
  <p class="pagetop"><a href="#top">ページの先頭へ</a></p>
<{/if}>

<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ６．インシデントへの対応                                                                             -->
<!-- ---------------------------------------------------------------------------------------------------- -->
<{if array_intersect(array(540, 550, 1410), $grp_flags)}>
  <div id="section06" class="section tabletype01">
    <h2 class="type02">対 応</h2>
    <div class="color02 radius">
      <{if array_intersect(array(540, 550), $grp_flags)}>
        <table>
          <{*初期対応*}>
          <{if in_array(540, $grp_flags)}>
            <{include file="report_form_item.tpl" i=540 j=50}>
          <{/if}>

          <{*減免措置*}>
          <{if in_array(550, $grp_flags)}>
            <{include file="report_form_item.tpl" i=550 j=55 tabletype='tabletype03'}>
            <{include file="report_form_item.tpl" i=550 j=60}>
          <{/if}>
        </table>
      <{/if}>

      <{*対応*}>
      <{if in_array(1410, $grp_flags)}>
        <{*対応区分*}>
        <{if array_intersect(array(540, 550), $grp_flags)}>
          <span class="lineWhite">　</span>
          <table>
        <{else}>
          <table>
        <{/if}>
          <{include file="report_form_item.tpl" i=1410 j=10 tabletype='tabletype03'}>
        </table>

        <{*対応*}>
        <{foreach item=correspondence key=key from=$ic_correspondence}>
          <table id="id_1410_20_<{$key}>_area">
            <{include file="report_form_item.tpl" i=1410 j=20 ic=$correspondence key=$key tabletype='tabletype02'}>
          </table>
        <{/foreach}>

        <{*詳細*}>
        <{foreach item=ic_item key=key2 from=$ic_sub_item}>
          <{foreach item=sub_item key=key from=$ic_item}>
            <table id="id_1410_30_<{$key}>_area">
              <{include file="report_form_item.tpl" i=1410 j=30 ic=$sub_item parent_ic=$ic_parent_item key=$key tabletype='tabletype03'}>
            </table>
          <{/foreach}>
        <{/foreach}>
      <{/if}>
    </div>
  </div>
  <p class="pagetop"><a href="#top">ページの先頭へ</a></p>
<{/if}>

<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ユーザー定義カテゴリ
<!-- ---------------------------------------------------------------------------------------------------- -->
<{foreach from=$user_grp_flags item=grp_code name=user_grp}>
  <{if $smarty.foreach.user_grp.first}>
  <div id="section08" class="section tabletype01">
    <h2 class="type02"><{$user_cate_name}></h2>
    <div class="color02 radius">
  <{/if}>

     <table>
       <{assign var="grp_item_code" value="`$grp_code`_10"}>
       <{include file="report_form_item.tpl" i=$grp_code j=10 tabletype=$tabletypes[$grp_item_code] is_last=$smarty.foreach.user_grp.last}>
     </table>

  <{if $smarty.foreach.user_grp.last}>
    </div>
  </div>
  <p class="pagetop"><a href="#top">ページの先頭へ</a></p>
  <{/if}>
<{/foreach}>

<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- ７．インシデントの分析                                                                               -->
<!-- ---------------------------------------------------------------------------------------------------- -->
<{if array_intersect(array(650, 90, 96, 1500, 1502, 1504, 1510, 1520, 1530, 600, 1100, 1110, 1120, 510, 590, 610, 620, 625, 630, 635, 690, 125), $grp_flags)}>
  <div id="section08" class="section tabletype01">
    <h2 class="type02"><{if $input_mode == "analysis"}>評価項目<{else}>分 析<{/if}></h2>
    <div class="color02 radius">
      <{*分類*}>
      <{if in_array(125, $grp_flags)}>
        <table>
          <{include file="report_form_item.tpl" i=125 j=85 tabletype='tabletype02'}>
        </table>
      <{/if}>

      <{*事例分析検討対象*}>
      <{if in_array(650, $grp_flags)}>
        <{if in_array(125, $grp_flags)}>
            <span class="lineWhite">　</span>
        <{/if}>
        <table>
          <{include file="report_form_item.tpl" i=650 j=50 tabletype='tabletype03'}>
        </table>
      <{/if}>

      <{if in_array(90, $grp_flags) || in_array(96, $grp_flags)}>
        <{if in_array(125, $grp_flags) || in_array(650, $grp_flags)}>
          <span class="lineWhite">　</span>
          <table>
        <{else}>
          <table>
        <{/if}>
          <{*患者影響レベル*}>
          <{if in_array(90, $grp_flags)}>
            <{include file="report_form_item.tpl" i=90 j=10 tabletype=''}>
          <{/if}>

          <{*ヒヤリ・ハットの影響度*}>
          <{if in_array(96, $grp_flags)}>
            <{include file="report_form_item.tpl" i=96 j=20 tabletype=''}>
          <{/if}>
        </table>
      <{/if}>

      <{if array_intersect(array(1500, 1502, 1504, 1510, 1520, 1530), $grp_flags)}>
        <{if array_intersect(array(125, 650, 90, 96), $grp_flags)}>
          <span class="lineWhite">　</span>
          <table>
        <{else}>
          <table>
        <{/if}>
          <{*リスクの評価: 重要性*}>
          <{if in_array(1500, $grp_flags)}>
            <{include file="report_form_item.tpl" i=1500 j=10 tabletype='tabletype03'}>
          <{/if}>

          <{*リスクの評価: 緊急性*}>
          <{if in_array(1502, $grp_flags)}>
            <{include file="report_form_item.tpl" i=1502 j=10 tabletype='tabletype03'}>
          <{/if}>

          <{*リスクの評価: 頻度*}>
          <{if in_array(1504, $grp_flags)}>
            <{include file="report_form_item.tpl" i=1504 j=10 tabletype='tabletype03'}>
          <{/if}>

          <{*リスクの予測*}>
          <{if in_array(1510, $grp_flags)}>
            <{include file="report_form_item.tpl" i=1510 j=10 tabletype='tabletype03'}>
          <{/if}>

          <{*システム改善の必要性*}>
          <{if in_array(1520, $grp_flags)}>
            <{include file="report_form_item.tpl" i=1520 j=10 tabletype='tabletype03'}>
          <{/if}>

          <{*教育研修への活用*}>
          <{if in_array(1530, $grp_flags)}>
            <{include file="report_form_item.tpl" i=1530 j=10 tabletype='tabletype03'}>
          <{/if}>
        </table>
      <{/if}>

      <{if in_array(600, $grp_flags)}>
        <{*発生要因*}>
        <{if array_intersect(array(125, 650, 90, 96, 1500, 1502, 1504, 1510, 1520, 1530), $grp_flags)}>
          <span class="lineWhite">　</span>
          <table>
        <{else}>
          <table>
        <{/if}>
          <{include file="report_form_item.tpl" i=600 j=10 tabletype='tabletype02'}>
        </table>

        <{*発生要因の詳細*}>
        <{if in_array(605, $grp_flags)}>
          <{foreach from=$selection[600][10].list item=item_600 name=n}>
            <{if $item_600.easy_code < 100 || $item_600.easy_code > 1000}>
              <{if count($selection[605][$item_600.easy_code].list) > 0}>
                <table id="id_605_<{$item_600.easy_code}>_area">
                  <{include file="report_form_item.tpl" i=605 j=$item_600.easy_code tabletype='tabletype02' is_last=$smarty.foreach.n.last}>
                </table>
              <{/if}>
            <{/if}>
          <{/foreach}>
        <{/if}>
      <{/if}>

      <{*誤った医療の実施の有無*}>
      <{if in_array(1100, $grp_flags)}>
        <{if array_intersect(array(125, 650, 90, 96, 1500, 1502, 1504, 1510, 1520, 1530, 600), $grp_flags)}>
          <span class="lineWhite">　</span>
          <table>
        <{else}>
          <table>
        <{/if}>
          <tr>
            <th width="188">
              <{include file="report_form_title.tpl" i=1100 j=10 title="誤った医療の実施の有無"}>
            </th>
            <td class="tabletype03">
              <ul>
                <li class="item_set<{if $vals[1100][10][0] == '実施あり'}> checked<{/if}>">
                  <label for="id_1100_10_1">
                    <input type="radio" class="radio" id="id_1100_10_1" name="_1100_10[]" value="実施あり"
                      onclick="set_medical_error_items(this.value);"
                      <{if $vals[1100][10][0] == '実施あり'}>checked<{/if}>>
                    実施あり
                  </label>
                </li>
                <li class="item_set<{if $vals[1100][10][0] == '実施なし'}> checked<{/if}>">
                  <label for="id_1100_10_2">
                    <input type="radio" class="radio" id="id_1100_10_2" name="_1100_10[]" value="実施なし"
                      onclick="set_medical_error_items(this.value);"
                      <{if $vals[1100][10][0] == '実施なし'}>checked<{/if}>>
                    実施なし
                  </label>
                </li>
              </ul>
              <a class="clear_btn cb_type03" href="javascript:void(0);" onclick="clear_item('radio',1100,10);return false;">クリア</a>
            </td>
          </tr>
        </table>

        <{*実施の程度*}>
        <table id="id_1100_20_area">
          <tr>
            <th width="188">
              <{include file="report_form_title.tpl" i=1100 j=10 title="実施の程度"}>
            </td>
            <td class="item_selection">
              <ul>
                <li class="item_set<{if $vals[1100][20][0] == '濃厚な治療'}> checked<{/if}>">
                  <label for="id_1100_20_1">
                    <input type='radio' class="radio" id="id_1100_20_1" name="_1100_20[]" value="濃厚な治療"
                    <{if $vals[1100][20][0] == '濃厚な治療'}>checked<{/if}>>
                    濃厚な治療
                  </label>
                </li>
                <li class="item_set<{if $vals[1100][20][0] == '軽微な治療'}> checked<{/if}>">
                  <label for="id_1100_20_2">
                    <input type='radio' class="radio" id="id_1100_20_2" name="_1100_20[]" value="軽微な治療"
                    <{if $vals[1100][20][0] == '軽微な治療'}>checked<{/if}>>
                    軽微な治療
                  </label>
                </li>
                <li class="item_set<{if $vals[1100][20][0] == 'なし'}> checked<{/if}>">
                  <label for="id_1100_20_3">
                    <input type='radio' class="radio" id="id_1100_20_3" name="_1100_20[]" value="なし"
                    <{if $vals[1100][20][0] == 'なし'}>checked<{/if}>>
                    なし
                  </label>
                </li>
                <li class="item_set<{if $vals[1100][20][0] == '不明'}> checked<{/if}>">
                  <label for="id_1100_20_4">
                    <input type='radio' class="radio" id="id_1100_20_4" name="_1100_20[]" value="不明"
                    <{if $vals[1100][20][0] == '不明'}>checked<{/if}>>
                    不明
                  </label>
                </li>
              </ul>
              <a class="clear_btn cb_type01" href="javascript:void(0);" onclick="clear_item('radio',1100,20);return false;">クリア</a>
            </td>
          </tr>
        </table>

        <{*事故の程度*}>
        <table id="id_1100_30_area">
          <tr>
            <th width="188" class="last">事故の程度</th>
            <td class="item_selection last">
              <ul>
                <li class="item_set<{if $vals[1100][30][0] == '死亡'}> checked<{/if}>">
                  <label for="id_1100_30_1">
                    <input type='radio' class="radio" id="id_1100_30_1" name="_1100_30[]" value="死亡"
                    onclick="clear_item('text', 1100, 40);"
                    <{if $vals[1100][30][0] == '死亡'}>checked<{/if}>>
                    死亡
                  </label>
                </li>
                <li class="item_set<{if $vals[1100][30][0] == '障害残存の可能性がある(高い)'}> checked<{/if}>">
                  <label for="id_1100_30_2">
                    <input type='radio' class="radio" id="id_1100_30_2" name="_1100_30[]" value="障害残存の可能性がある(高い)"
                    onclick="clear_item('text', 1100, 40);"
                    <{if $vals[1100][30][0] == '障害残存の可能性がある(高い)'}>checked<{/if}>>
                    障害残存の可能性がある(高い)
                  </label>
                </li>
                <li class="item_set<{if $vals[1100][30][0] == '障害残存の可能性がある(低い)'}> checked<{/if}>">
                  <label for="id_1100_30_3">
                    <input type='radio' class="radio" id="id_1100_30_3" name="_1100_30[]" value="障害残存の可能性がある(低い)"
                    onclick="clear_item('text', 1100, 40);"
                    <{if $vals[1100][30][0] == '障害残存の可能性がある(低い)'}>checked<{/if}>>
                    障害残存の可能性がある(低い)
                  </label>
                </li>
                <li class="item_set<{if $vals[1100][30][0] == '障害残存の可能性なし'}> checked<{/if}>">
                  <label for="id_1100_30_4">
                    <input type='radio' class="radio" id="id_1100_30_4" name="_1100_30[]" value="障害残存の可能性なし"
                    onclick="clear_item('text', 1100, 40);"
                    <{if $vals[1100][30][0] == '障害残存の可能性なし'}>checked<{/if}>>
                    障害残存の可能性なし
                  </label>
                </li>
                <li class="item_set<{if $vals[1100][30][0] == '障害なし'}> checked<{/if}>">
                  <label for="id_1100_30_5">
                    <input type='radio' class="radio" id="id_1100_30_5" name="_1100_30[]" value="障害なし"
                    onclick="clear_item('text', 1100, 40);"
                    <{if $vals[1100][30][0] == '障害なし'}>checked<{/if}>>
                    障害なし
                  </label>
                </li>
                <li class="item_set<{if $vals[1100][30][0] == '不明'}> checked<{/if}>">
                  <label for="id_1100_30_6">
                    <input type='radio' class="radio" id="id_1100_30_6" name="_1100_30[]" value="不明"
                    <{if $vals[1100][30][0] == '不明'}>checked<{/if}>>
                    不明
                  </label>
                  (
                  <input type="text" class="text" id="id_1100_40" name="_1100_40" size="60" value="<{$vals[1100][40][0]}>">
                  )
                </li>
              </ul>
              <a class="clear_btn cb_type01" href="javascript:void(0);" onclick="clear_item('radio',1100,30);return false;">クリア</a>
            </td>
          </tr>
        </table>

        <{*仮に実施された場合の影響度*}>
        <table id="id_1100_50_area">
          <tr>
            <th width="188" class="last">
              <{include file="report_form_title.tpl" i=1100 j=10 title="仮に実施された場合の影響度"}>
            </th>
            <td class="item_selection last">
              <ul>
                <li class="item_set<{if $vals[1100][50][0] == '死亡もしくは重篤な状況に至ったと考えられる'}> checked<{/if}>">
                  <label for="id_1100_50_1">
                    <input type="radio" class="radio" id="id_1100_50_1" name="_1100_50[]" value="死亡もしくは重篤な状況に至ったと考えられる"
                    <{if $vals[1100][50][0] == '死亡もしくは重篤な状況に至ったと考えられる'}>checked<{/if}> >
                    死亡もしくは重篤な状況に至ったと考えられる
                  </label>
                </li>
                <li class="item_set<{if $vals[1100][50][0] == '濃厚な処置・治療が必要であると考えられる'}> checked<{/if}>">
                  <label for="id_1100_50_2">
                    <input type="radio" class="radio" id="id_1100_50_2" name="_1100_50[]" value="濃厚な処置・治療が必要であると考えられる"
                    <{if $vals[1100][50][0] == '濃厚な処置・治療が必要であると考えられる'}>checked<{/if}> >
                    濃厚な処置・治療が必要であると考えられる
                  </label>
                </li>
                <li class="item_set<{if $vals[1100][50][0] == '軽微な処置・治療が必要もしくは処置・治療が不要と考えられる'}> checked<{/if}>">
                  <label for="id_1100_50_3">
                    <input type="radio" class="radio" id="id_1100_50_3" name="_1100_50[]" value="軽微な処置・治療が必要もしくは処置・治療が不要と考えられる"
                    <{if $vals[1100][50][0] == '軽微な処置・治療が必要もしくは処置・治療が不要と考えられる'}>checked<{/if}> >
                    軽微な処置・治療が必要もしくは処置・治療が不要と考えられる
                  </label>
                </li>
              </ul>
              <a class="clear_btn cb_type01" href="javascript:void(0);" onclick="clear_item('radio',1100,50);return false;">クリア</a>
            </td>
          </tr>
        </table>
      <{/if}>

      <{*特に報告を求める事例*}>
      <{if in_array(1110, $grp_flags)}>
        <{if array_intersect(array(125, 650, 90, 96, 1500, 1502, 1504, 1510, 1520, 1530, 600, 1100), $grp_flags)}>
          <span class="lineWhite">　</span>
          <table>
        <{else}>
          <table>
        <{/if}>
          <tr>
            <th width="188" class="last">
              <{include file="report_form_title.tpl" i=1110 j=10}>
            </th>
            <td class="item_selection last">
              <ul>
                <li class="item_set<{if $vals[1110][10][0] == '院内感染による死亡や障害'}> checked<{/if}>">
                  <label for="id_1110_10_0">
                    <input type="radio" class="radio" id="id_1110_10_0" name="_1110_10[]" value='院内感染による死亡や障害'
                    onclick="clear_item('text', 1110, 20);"
                    <{if $vals[1110][10][0] == '院内感染による死亡や障害'}> checked<{/if}>>
                    院内感染による死亡や障害
                  </label>
                </li>
                <li class="item_set<{if $vals[1110][10][0] == '患者の自殺又は自殺企図'}> checked<{/if}>">
                  <label for="id_1110_10_1">
                    <input type="radio" class="radio" id="id_1110_10_1" name="_1110_10[]" value='患者の自殺又は自殺企図'
                    onclick="clear_item('text', 1110, 20);"
                    <{if $vals[1110][10][0] == '患者の自殺又は自殺企図'}> checked<{/if}>>
                    患者の自殺又は自殺企図
                  </label>
                </li>
                <li class="item_set<{if $vals[1110][10][0] == '入院患者の失踪'}> checked<{/if}>">
                  <label for="id_1110_10_2">
                    <input type="radio" class="radio" id="id_1110_10_2" name="_1110_10[]" value='入院患者の失踪'
                    onclick="clear_item('text', 1110, 20);"
                    <{if $vals[1110][10][0] == '入院患者の失踪'}> checked<{/if}>>
                    入院患者の失踪
                  </label>
                </li>
                <li class="item_set<{if $vals[1110][10][0] == '入院中の熱傷'}> checked<{/if}>">
                  <label for="id_1110_10_3">
                    <input type="radio" class="radio" id="id_1110_10_3" name="_1110_10[]" value='入院中の熱傷'
                    onclick="clear_item('text', 1110, 20);"
                    <{if $vals[1110][10][0] == '入院中の熱傷'}> checked<{/if}>>
                    入院中の熱傷
                  </label>
                </li>
                <li class="item_set<{if $vals[1110][10][0] == '入院中の感電'}> checked<{/if}>">
                  <label for="id_1110_10_4">
                    <input type="radio" class="radio" id="id_1110_10_4" name="_1110_10[]" value='入院中の感電'
                    onclick="clear_item('text', 1110, 20);"
                    <{if $vals[1110][10][0] == '入院中の感電'}> checked<{/if}>>
                    入院中の感電
                  </label>
                </li>
                <li class="item_set<{if $vals[1110][10][0] == '医療施設内の火災による患者の死亡や障害'}> checked<{/if}>">
                  <label for="id_1110_10_5">
                    <input type="radio" class="radio" id="id_1110_10_5" name="_1110_10[]" value='医療施設内の火災による患者の死亡や障害'
                    onclick="clear_item('text', 1110, 20);"
                    <{if $vals[1110][10][0] == '医療施設内の火災による患者の死亡や障害'}> checked<{/if}>>
                    医療施設内の火災による患者の死亡や障害
                  </label>
                </li>
                <li class="item_set<{if $vals[1110][10][0] == '間違った保護者の許への新生児の引渡し'}> checked<{/if}>">
                  <label for="id_1110_10_6">
                    <input type="radio" class="radio" id="id_1110_10_6" name="_1110_10[]" value='間違った保護者の許への新生児の引渡し'
                    onclick="clear_item('text', 1110, 20);"
                    <{if $vals[1110][10][0] == '間違った保護者の許への新生児の引渡し'}> checked<{/if}>>
                    間違った保護者の許への新生児の引渡し
                  </label>
                </li>
                <li class="item_set<{if $vals[1110][10][0] == '本事例は選択肢には該当しない（その他）'}> checked<{/if}>">
                  <label for="id_1110_10_7">
                    <input type="radio" class="radio" id="id_1110_10_7" name="_1110_10[]" value='本事例は選択肢には該当しない（その他）'
                    <{if $vals[1110][10][0] == '本事例は選択肢には該当しない（その他）'}> checked<{/if}>>
                    本事例は選択肢には該当しない（その他）
                  </label>
                  (
                  <input type="text" class="text" id="id_1110_20" name="_1110_20" value="<{$vals[1110][20][0]}>" size="60">
                  )
                </li>
                <a class="clear_btn cb_type01" href="javascript:void(0);" onclick="clear_item('radio',1110,10);return false;">クリア</a>
              </ul>
            </td>
          </tr>
        </table>
      <{/if}>

      <{*発生件数集計再掲*}>
      <{if in_array(1120, $grp_flags)}>
        <{if array_intersect(array(125, 650, 90, 96, 1500, 1502, 1504, 1510, 1520, 1530, 600, 1100, 1110), $grp_flags)}>
          <span class="lineWhite">　</span>
          <table>
        <{else}>
          <table>
        <{/if}>
          <tr>
            <th width="188" class="last">
              <{include file="report_form_title.tpl" i=1120 j=10}>
            </th>
            <td class="item_selection last">
              <ul>
                <li class="item_set<{if $vals[1120][10][0] == '該当無し'}> checked<{/if}>">
                  <label for="id_1120_10_0">
                    <input type='radio' class="radio" id="id_1120_10_0" name="_1120_10[]" value="該当無し"
                    onclick="radio_onclick(this);"
                    <{if $vals[1120][10][0] == '該当無し'}>checked<{/if}>>
                    該当無し
                  </label>
                </li>
                <li class="item_set<{if $vals[1120][10][0] == '薬剤の名称や形状に関連する事例'}> checked<{/if}>">
                  <label for="id_1120_10_1">
                    <input type='radio' class="radio" id="id_1120_10_1" name="_1120_10[]" value="薬剤の名称や形状に関連する事例"
                    onclick="radio_onclick(this);"
                    <{if $vals[1120][10][0] == '薬剤の名称や形状に関連する事例'}>checked<{/if}>>
                    薬剤の名称や形状に関連する事例
                  </label>
                </li>
                <li class="item_set<{if $vals[1120][10][0] == '薬剤に由来する事例'}> checked<{/if}>">
                  <label for="id_1120_10_2">
                    <input type='radio' class="radio" id="id_1120_10_2" name="_1120_10[]" value="薬剤に由来する事例"
                    onclick="radio_onclick(this);"
                    <{if $vals[1120][10][0] == '薬剤に由来する事例'}>checked<{/if}>>
                    薬剤に由来する事例
                  </label>
                </li>
                <li class="item_set<{if $vals[1120][10][0] == '医療機器等に由来する事例'}> checked<{/if}>">
                  <label for="id_1120_10_3">
                    <input type='radio' class="radio" id="id_1120_10_3" name="_1120_10[]" value="医療機器等に由来する事例"
                    onclick="radio_onclick(this);"
                    <{if $vals[1120][10][0] == '医療機器等に由来する事例'}>checked<{/if}>>
                    医療機器等に由来する事例
                  </label>
                </li>
                <li class="item_set<{if ereg("^今期のテーマ", $vals[1120][10][0])}> checked<{/if}>">
                  <label for="id_1120_10_4">
                    <input type='radio' class="radio" id="id_1120_10_4" name="_1120_10[]" value="今期のテーマ(<{$theme}>)"
                    onclick="radio_onclick(this);"
                    <{if ereg("^今期のテーマ", $vals[1120][10][0])}>checked<{/if}>>
                    今期のテーマ(<{$theme}>)
                  </label>
                </li>
              </ul>
              <a class="clear_btn cb_type01" href="javascript:void(0);" onclick="clear_item('radio',1120,10);return false;">クリア</a>
            </td>
          </tr>
        </table>
      <{/if}>

      <{*再発防止に資する(警鐘的)事例*}>
      <{if in_array(510, $grp_flags)}>
        <{if array_intersect(array(125, 650, 90, 96, 1500, 1502, 1504, 1510, 1520, 1530, 600, 1100, 1110, 1120), $grp_flags)}>
          <span class="lineWhite">　</span>
          <table>
        <{else}>
          <table>
        <{/if}>
          <{include file="report_form_item.tpl" i=510 j=20 tabletype=''}>
        </table>
      <{/if}>

      <{*事故調査委員会の設置有無*}>
      <{if in_array(590, $grp_flags)}>
        <{if array_intersect(array(125, 650, 90, 96, 1500, 1502, 1504, 1510, 1520, 1530, 600, 1100, 1110, 1120, 510), $grp_flags)}>
          <span class="lineWhite">　</span>
          <table>
        <{else}>
          <table>
        <{/if}>
          <{include file="report_form_item.tpl" i=590 j=90 tabletype=''}>
        </table>
      <{/if}>

      <{if array_intersect(array(610, 620, 625, 630, 635), $grp_flags)}>
        <{if array_intersect(array(125, 650, 90, 96, 1500, 1502, 1504, 1510, 1520, 1530, 600, 1100, 1110, 1120, 510, 590), $grp_flags)}>
          <span class="lineWhite">　</span>
          <table>
        <{else}>
          <table>
        <{/if}>
          <{*インシデントの背景・要因*}>
          <{if in_array(610, $grp_flags)}>
            <{include file="report_form_item.tpl" i=610 j=20}>
          <{/if}>

          <{if in_array(620, $grp_flags)}>
            <{*改善策（部内処置指示及び再発防止策）*}>
            <{include file="report_form_item.tpl" i=620 j=30}>

            <{*評価予定期日*}>
            <{if $predetermined_eval=="t" }>
            <{include file="report_form_item.tpl" i=620 j=33}>
			 <{/if}>
          <{/if}>

          <{*効果の確認*}>
          <{if in_array(625, $grp_flags)}>
            <{include file="report_form_item.tpl" i=625 j=35}>
          <{/if}>

          <{*インシデント内容に関する自由記載欄*}>
          <{if in_array(630, $grp_flags)}>
            <{include file="report_form_item.tpl" i=630 j=40}>
          <{/if}>

          <{*備考*}>
          <{if in_array(635, $grp_flags)}>
            <{include file="report_form_item.tpl" i=635 j=45}>
          <{/if}>
        </table>
      <{/if}>

      <{*医療安全者専用記載項目*}>
      <{if in_array(690, $grp_flags)}>
        <span class="lineWhite">　</span>
        <table>
          <{include file="report_form_item.tpl" i=690 j=90}>
        </table>
      <{/if}>
    </div>
  </div>
  <p class="pagetop"><a href="#top">ページの先頭へ</a></p>
<{/if}>

<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- 報告書内容（評価画面の場合）
<!-- ---------------------------------------------------------------------------------------------------- -->
<{if $input_mode == "analysis"}>
  <{include file="report_contents_main.tpl"}>
<{/if}>

<!-- ---------------------------------------------------------------------------------------------------- -->
<!-- 電子カルテ
<!-- ---------------------------------------------------------------------------------------------------- -->
<{if $emr_flg=='t'}>
<script language="JavaScript">
// 電子カルテ呼び出し
function call_emr(gcode, itemcode){
    var patient_id = $('#id_'+gcode+'_'+itemcode).val();
    if (patient_id==''){
        alert('患者IDが未入力です');
        return;
    }
    start_emr(patient_id);
}

// 電子カルテ起動
function start_emr(ptid){
    var emr=document.getElementById('emr');
    var c=(emr.contentWindow || emr.contentDocument);
    c.location.replace("hiyari_exec_emr.php?session=<{$session}>&ptid="+ptid);
}
</script>
<iframe id="emr" src="about:blank" width="0" height="0" style="visibility:hidden;position:absolute;"></iframe>
<{/if}>

<!-- データが全て表示されたかのフラグ -->
<input type="hidden" name="data_exist_flg" value="1">