<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix <{$INCIDENT_TITLE}> | EXCEL出力</title>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript">
      function setCheckboxOnOff(cate_code,is_on) {
          var ckbox = $('#cate_' + cate_code).find('input');
          var child = $('#cate_' + cate_code).find('.child');
          if (is_on){
              child.prop('disabled', false);
              ckbox.prop('checked', true);
          }
          else{
              ckbox.prop('checked', false);
              child.prop('disabled', true);
          }
      }
      
      function setRelateCheck(is_parent_on, child_code){
          var child = $('#id_'+ child_code);
          if (is_parent_on){
              child.prop('disabled', false);
          }
          else{                                  
              child.prop('checked', false);
              child.prop('disabled', true);
          }      
      }

      function set_output_item() {
          var output_cnt = get_output_count();
          if(output_cnt > 256){
              alert("出力項目が多すぎます。項目を絞り込んでください。\n\nEXCEL列数:" + output_cnt + "/256 (" + (output_cnt-256) + "列超過)" );
              return;
          }
          document.mainform.action="hiyari_rp_report_list_excel.php";
          document.mainform.submit();
      }

      function set_output_item_csv() {
          document.mainform.action="hiyari_rp_report_list_csv.php";
          document.mainform.submit();
      }

      function set_output_item_pdf() {
          var h = window.screen.availHeight;
          var w = window.screen.availWidth;
          var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
          window.open("", "pdf_window", option);
          document.mainform.target = "pdf_window";
          document.mainform.action="hiyari_rp_report_list_pdf.php";
          document.mainform.submit();
      }

      function get_output_count() {
          //未指定の時の出力件数
          var cnt = 4;

          var input_object_list = document.getElementsByTagName("input");
          for (var i = 0; i < input_object_list.length; i++) {
              var obj = input_object_list[i];
              if (obj.type == "checkbox" && obj.checked) {
                  switch (obj.value) {
                      <{foreach from=$grp_excel_output_cnt key=grp_code item=cnt}>
                      case "<{$grp_code}>":cnt = cnt + <{$cnt}>;break;
                      <{/foreach}>
                  }
              }
          }
          return cnt;
      }

      function set_max_report_dl_cnt() {
          document.mainform.action="hiyari_excel_output_item.php";
          document.mainform.mode.value = "set_max_report_dl_cnt";
          document.mainform.submit();
      }
  </script>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <style type="text/css">
    .list {border-collapse:collapse;}
    .list td {border:#35B341 solid 1px;}
  </style>
</head>  

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
  <form name="mainform" action="" method="post">
    <input type="hidden" name="session" value="<{$session}>">
    <input type="hidden" name="report_id_list" value="<{$report_id_list}>">
    <input type="hidden" name="emp_id" value="<{$emp_id}>">
    <input type="hidden" name="mode" value="">
    
    <!-------------------------------------------------------------------------
    ヘッダー 
    -------------------------------------------------------------------------->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr height="32" bgcolor="#35B341">
        <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><{$title}></b></font></td>
        <td>&nbsp;</td>
        <td width="10">&nbsp;</td>
        <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
        </tr>
      </table>
      <img src="img/spacer.gif" width="10" height="10" alt=""><br>   
    </table>
    
    <!-------------------------------------------------------------------------
    コンテンツ
    -------------------------------------------------------------------------->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20">
                <img src="img/spacer.gif" alt="" width="1" height="1">
              </td>
            </tr>
          </table>
        </td>
        <td>
          <table width="600" border="0" cellspacing="0" cellpadding="0">            
            <!-----------------------------------------------------------------
            件数設定
            ------------------------------------------------------------------>
            <tr>
              <td>
                <table width="200" border="0" cellspacing="0" cellpadding="3"  class="list">
                  <tr>
                    <td align="center" width="80" bgcolor="#DFFFDC">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                      件数設定
                      </font>
                    </td>
                    <td align="left" bgcolor="#FFFFFF">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                        <select name="max_report_dl_cnt" onchange="set_max_report_dl_cnt()">
                          <option value="10"  <{if $max_report_dl_cnt == 10 }>selected<{/if}>>10件単位</option>
                          <option value="100" <{if $max_report_dl_cnt == 100}>selected<{/if}>>100件単位</option>
                          <option value="200" <{if $max_report_dl_cnt == 200}>selected<{/if}>>200件単位</option>
                          <option value="300" <{if $max_report_dl_cnt == 300}>selected<{/if}>>300件単位</option>
                          <option value="0"   <{if $max_report_dl_cnt == 0  }>selected<{/if}>>全件</option>
                        </select>
                      </font>
                    </td>
                  </tr>
                </table>
                
                <img src="img/spacer.gif" alt="" width="1" height="6"><br>

                <table width="300" border="0" cellspacing="0" cellpadding="3"  class="list">
                  <{foreach name=range from=$arr_start_end item=i}>
                    <tr>
                      <td align="center" width="30" bgcolor="#FFFFFF"><input type="radio" name="report_idx"; value="<{$smarty.foreach.range.index}>" <{if $smarty.foreach.range.index == 0}> checked<{/if}>></td>
                      <td width="270" bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$i.start}>件から<{$i.end}>件まで出力</font></td>
                    </tr>
                  <{/foreach}>
                </table>
              </td>
            </tr>
            
            <!-----------------------------------------------------------------
            実行ボタン（上部）
            ------------------------------------------------------------------>
            <tr>
              <td align="right" nowrap>
                <input type="button" value="PDF出力" onclick="set_output_item_pdf()" <{if $max_report_dl_cnt!=10}>disabled<{/if}>>
                <input type="button" value="CSV出力" onclick="set_output_item_csv()">
                <input type="button" value="EXCEL出力" onclick="set_output_item()">
              </td>
            </tr>
            
            <!-----------------------------------------------------------------
            一覧
            ------------------------------------------------------------------>
            <tr>
              <td>
                <br>
                  <{foreach from=$cate_list item=cate}>
                    <img src="img/spacer.gif" alt="" width="1" height="6"><br>
                    
                    <nobr>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><{$cate.cate_name}></b></font>
                      <input type="button" value="全てON"  onclick="setCheckboxOnOff('<{$cate.cate_code}>',true)">
                      <input type="button" value="全てOFF" onclick="setCheckboxOnOff('<{$cate.cate_code}>',false)">
                    </nobr>
                    
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
                        <td width="600" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
                      </tr>
                      <tr>
                        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                        <td bgcolor="#F5FFE5">
                          <table id="cate_<{$cate.cate_code}>" width="600" border="0" cellspacing="0" cellpadding="2" class="list">
                            <tr>
                              <td bgcolor="#DFFFDC" width="600">&nbsp;</td>
                            </tr>
                            <{foreach from=$cate.grp_list item=grp}>
                              <{* 発生日時 *}>
                              <{if $grp.grp_code == 100}>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    &nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生日時</font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_100_5" name="use_grp_item_code[]" value="100_5" style="margin-left:20px;" <{if in_array("100_5", $flags)}>checked<{/if}> >
                                      <label for="id_100_5">発生年月日</label>
                                    </font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_100_40" name="use_grp_item_code[]" value="100_40" style="margin-left:20px;" <{if in_array("100_40", $flags)}>checked<{/if}> >
                                      <label for="id_100_40">発生時間帯</label>
                                    </font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_100_50" name="use_grp_item_code[]" value="100_50" style="margin-left:20px;" <{if in_array("100_50", $flags)}>checked<{/if}> >
                                      <label for="id_100_50">いつごろ</label>
                                    </font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_100_65" name="use_grp_item_code[]" value="100_65" style="margin-left:20px;" <{if in_array("100_65", $flags)}>checked<{/if}> >
                                      <label for="id_100_65">発生時刻：時　と　分</label>
                                    </font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_100_60" name="use_grp_item_code[]" value="100_60" style="margin-left:20px;" <{if in_array("100_60", $flags)}>checked<{/if}> >
                                      <label for="id_100_60">発生時刻：時　のみ</label>
                                    </font>
                                  </td>
                                </tr>

                              <{* 発生場所 *}>
                              <{elseif $grp.grp_code == 110}>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    &nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生場所</font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_110_60" name="use_grp_item_code[]" value="110_60" style="margin-left:20px;" <{if in_array("110_60", $flags)}>checked<{/if}> >
                                      <label for="id_110_60">発生場所</label>
                                    </font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_110_65" name="use_grp_item_code[]" value="110_65" style="margin-left:20px;" <{if in_array("110_65", $flags)}>checked<{/if}> >
                                      <label for="id_110_65">発生場所詳細</label>
                                    </font>
                                  </td>
                                </tr>

                              <{* 患者プロフィール *}>
                              <{elseif $grp.grp_code == 210}>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    &nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者プロフィール</font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_210_10" name="use_grp_item_code[]" value="210_10" style="margin-left:20px;" <{if in_array("210_10", $flags)}>checked<{/if}> >
                                      <label for="id_210_10">患者ID</label>
                                    </font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_210_20" name="use_grp_item_code[]" value="210_20" style="margin-left:20px;" <{if in_array("210_20", $flags)}>checked<{/if}> >
                                      <label for="id_210_20">患者氏名</label>
                                    </font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_210_30" name="use_grp_item_code[]" value="210_30"  style="margin-left:20px;" <{if in_array("210_30", $flags)}>checked<{/if}> >
                                      <label for="id_210_30">患者性別</label>
                                    </font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_210_40" name="use_grp_item_code[]" value="210_40" style="margin-left:20px;" <{if in_array("210_40", $flags)}>checked<{/if}> >
                                      <label for="id_210_40">患者年齢（年数）</label>
                                    </font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_210_50" name="use_grp_item_code[]" value="210_50" style="margin-left:20px;" <{if in_array("210_50", $flags)}>checked<{/if}> >
                                      <label for="id_210_50">患者年齢（月数）</label>
                                    </font>
                                  </td>
                                </tr>
                                
                              <{* アセスメント・患者の状態は出力対象外。 20090304*}>
                              <{elseif $grp.grp_code == 140}>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    &nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アセスメント・患者の状態</font>
                                  </td>
                                </tr>
                              
                              <{* 改善策 *}>
                              <{elseif $grp.grp_code == 620}>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    &nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">改善策</font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_620_30" name="use_grp_item_code[]" value="620_30" style="margin-left:20px;" <{if in_array("620_30", $flags)}>checked<{/if}> >
                                      <label for="id_620_30">改善策</label>
                                    </font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_620_33" name="use_grp_item_code[]" value="620_33" style="margin-left:20px;" <{if in_array("620_33", $flags)}>checked<{/if}> >
                                      <label for="id_620_33">評価予定期日</label>
                                    </font>
                                  </td>
                                </tr>

                              <{* 概要・場面・内容（2010年改訂） *}>
                              <{elseif $grp.grp_code == 900}>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    &nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">概要・場面・内容（2010年改訂）</font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_900_10" name="use_grp_item_code[]" value="900_10" style="margin-left:20px;" <{if in_array("900_10", $flags)}>checked<{/if}> >
                                      <label for="id_900_10">概要</label>
                                    </font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_910_10" name="use_grp_item_code[]" value="910_10" style="margin-left:20px;" <{if in_array("910_10", $flags)}>checked<{/if}> >
                                      <label for="id_910_10">種類</label>
                                    </font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_920_10" name="use_grp_item_code[]" value="920_10" style="margin-left:20px;" <{if in_array("920_10", $flags)}>checked<{/if}> >
                                      <label for="id_920_10">種類の項目</label>
                                    </font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_940_10" name="use_grp_item_code[]" value="940_10" style="margin-left:20px;" <{if in_array("940_10", $flags)}>checked<{/if}> >
                                      <label for="id_940_10">発生場面</label>
                                    </font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_950_10" name="use_grp_item_code[]" value="950_10" style="margin-left:20px;" <{if in_array("950_10", $flags)}>checked<{/if}> >
                                      <label for="id_950_10">発生場面の項目</label>
                                    </font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_970_10" name="use_grp_item_code[]" value="970_10" style="margin-left:20px;" <{if in_array("970_10", $flags)}>checked<{/if}> >
                                      <label for="id_970_10">事例の内容</label>
                                    </font>
                                  </td>
                                </tr>
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox" id="id_980_10" name="use_grp_item_code[]" value="980_10" style="margin-left:20px;" <{if in_array("980_10", $flags)}>checked<{/if}> >
                                      <label for="id_980_10">事例の内容の項目</label>
                                    </font>
                                  </td>
                                </tr>
                              
                              <{* 通常 *}>
                              <{else}>
                                <{* 関連する患者情報 *}>
                                <{if $grp.grp_code == 310}>
                                  <tr>
                                    <td bgcolor="#FFFFFF">
                                      &nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">関連する患者情報</font>
                                    </td>
                                  </tr>
                                <{/if}>
                              
                                <tr>
                                  <td bgcolor="#FFFFFF">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <input type="checkbox"
                                             id="id_<{$grp.grp_code}>"
                                             name="use_grp_item_code[]" value="<{$grp.grp_code}>"
                                             <{if in_array($grp.grp_code, $flags)}>checked<{/if}>
                                             <{if isset($grp.rel_grp)}>
                                                onclick="setRelateCheck(this.checked, <{$grp.rel_grp.grp_code}>);"
                                             <{/if}>
                                             <{if ($grp.grp_code >= 15000 and $grp.grp_code < 15100) || $grp.grp_code >= 310 and $grp.grp_code < 390}>
                                               style="margin-left:20px;"
                                             <{/if}>
                                        >
                                      <label for="id_<{$grp.grp_code}>"><{$grp.grp_name}></label>
                                    </font>
                                  </td>
                                </tr>
                              
                                <{if isset($grp.rel_grp)}>
                                  <tr>
                                    <td bgcolor="#FFFFFF">
                                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                        <input type="checkbox"
                                               id="id_<{$grp.rel_grp.grp_code}>"
                                               class="child"
                                               name="use_grp_item_code[]"
                                               value="<{$grp.rel_grp.grp_code}>"
                                               <{if in_array($grp.rel_grp.grp_code, $flags)}>
                                                checked
                                               <{/if}>
                                               style="margin-left:20px;"
                                               >
                                        <label for="id_<{$grp.rel_grp.grp_code}>"><{$grp.rel_grp.grp_name}></label><br>
                                      </font>
                                    </td>
                                  </tr>
                                <{/if}>
                              <{/if}>
                            <{/foreach}>
                          </table>
                        </td>
                        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                      </tr>
                      <tr>
                        <td><img src="img/r_3.gif" width="10" height="10"></td>
                        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                        <td><img src="img/r_4.gif" width="10" height="10"></td>
                      </tr>
                    </table>
                  <{/foreach}>
                <img src="img/spacer.gif" width="1" height="5" alt=""><br>
              </td>
            </tr>
            
            <!-----------------------------------------------------------------
            実行ボタン（下部）
            ------------------------------------------------------------------>
            <tr>
              <td align="left">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#FF0000">環境によっては件数、項目数が多い場合にダウンロードが正しく動作しない場合があります。<BR>その場合は件数、項目数を減らして再度実行してください。</font>
              </td>
            </tr>
            <tr>
              <td align="right" nowrap>
                <input type="button" value="PDF出力" onclick="set_output_item_pdf()" <{if $max_report_dl_cnt!=10}>disabled<{/if}> >
                <input type="button" value="CSV出力" onclick="set_output_item_csv()">
                <input type="button" value="EXCEL出力" onclick="set_output_item()">
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </form>
</body>
</html>