<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <{$INCIDENT_TITLE}> | 代行者設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/hiyari_agent_register.js"></script>
<{php}>
  // YUIファイルを読み込む
  read_yui_calendar_use_file_0_12_2();

  // カレンダー作成、関数出力
  read_yui_calendar_script(array('start_date', 'end_date'));
<{/php}>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
  .list {border-collapse:collapse;}
  .list td {border:#35B341 solid 1px;}

  table.block {border-collapse:collapse;}
  table.block td {border:#35B341 solid 1px;padding:1px;}
  table.block td td {border-width:0;}

  .non_in_list {border-collapse:collapse;}
  .non_in_list td {border:0px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage('<{$agent_ids}>', '<{$agent_names}>'); initcal();">
<form name="mainform" action="hiyari_agent_register.php" method="post">
<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<{include file="hiyari_header1.tpl"}>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table border="0" cellspacing="0" cellpadding="0" width="640">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">

<!-- ここから -->

			<table width="20%" border="0" cellspacing="0" cellpadding="0"  class="list">
				<tr height="22">
					<td bgcolor="#DFFFDC" align="center" nowrap>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">■代行者の設定</font>
					</td>
				</tr>
			</table>

			<table cellspacing="0" cellpadding="0" border="0">
				<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
			</table>

			<table cellspacing="1" cellpadding="1" border="0">
				<tr height="22">
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">代行期間</font></td>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
              <input type="text" id="start_date" name="start_date" value="<{$start_date}>" readonly>
              <img src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
              <div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
						</font>
					</td>
					<td>
						<input type="button" value="クリア" onclick="date_clear('start_date');">
					</td>
					<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">〜</font></td>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
              <input type="text" id="end_date" name="end_date" value="<{$end_date}>" readonly>
              <img src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer;" onclick="show_cal2();"/><br>
              <div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
						</font>
					</td>
					<td>
						<input type="button" value="クリア" onclick="date_clear('end_date');">
					</td>
				</tr>
				<tr>
					<td colspan="6"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">＊代行期間を設けたくない場合、入力は不要です。</font></td>
				</tr>
			</table>
			<table width="100%" border='0' cellspacing='0' cellpadding='0' class="block">
				<tr height="100">
				<td width="20%" bgcolor="#DFFFDC" align="center">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
				<input type="button" value="職員名簿" onclick="call_emp_search('1', '<{$session}>');"><br>
				<input type="button" value="クリア" onclick="emp_clear('1');">
				</td>
				<td colspan="2" bgcolor="#FFFFFF">
				<table width="100%" height="100%" border="0" cellspacing="3" cellpadding="0" class="non_in_list">
				<tr>
				<td style="border:#35B341 solid 1px;">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><span id="disp_area_1"></span></font>
				</td>
				</tr>
				</table>
				</td>
				</tr>
				<input type="hidden" name="disp_area_ids_1" value="" id="disp_area_ids_1">
				<input type="hidden" name="disp_area_names_1" value="" id="disp_area_names_1">
			</table>

			<table cellspacing="0" cellpadding="0" border="0">
				<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
			</table>

			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td align="right">
						<input type="button" value="登録" onclick="regist('<{$session}>');">
					</td>
				</tr>
			</table>

<!-- ここまで -->

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<input type="hidden" name="emp_id" value="<{$emp_id}>" id="emp_id">
<input type="hidden" name="emp_nm" value="<{$emp_nm}>" id="emp_nm">
</td>
</tr>
</table>
</form>
</body>
</html>
