<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><{$INCIDENT_TITLE}> | お知らせ管理</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
  function deleteNews() {
    if (confirm('削除してよろしいですか？')) {
      document.mainform.submit();
    }
  }
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
  .list {border-collapse:collapse;}
  .list td {border:#35B341 solid 1px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <{include file="hiyari_header1.tpl"}>
      <img src="img/spacer.gif" alt="" width="1" height="2"><br>
      <form name="mainform" action="hiyari_news_delete.php" method="post">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
            <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
          </tr>
          <tr>
            <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
            <td bgcolor="#F5FFE5">
              <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22" bgcolor="#DFFFDC">
                  <td width="30">&nbsp;</td>
                  <td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録日</font></td>
                  <td width="60"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ</font></td>
                  <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
                  <td width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲載期間</font></td>
                  <td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照者一覧</font></td>
                </tr>
                <{foreach from=$news_list key=news_id item=news}>
                  <tr height="22" bgcolor="#FFFFFF">
                    <td align="center">
                      <input name="news_ids[]" type="checkbox" value="<{$news_id}>">
                    </td>
                    <td>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$news.news_date}></font>
                    </td>
                    <td>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$news.category}></font>
                    </td>
                    <td>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                        <a href="hiyari_news_update.php?session=<{$session}>&news_id=<{$news_id}>&page=<{$page}>" <{$news.style}>><{$news.news_title}></a>
                      </font>
                    </td>
                    <td>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$news.news_begin}> 〜 <{$news.news_end}></font>
                    </td>
                    <td>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                        <{if $news.record_flg == "t"}>
                          <a href="hiyari_news_refer_list.php?session=<{$session}>&news_id=<{$news_id}>&menu_page=<{$page}>">参照者一覧</a>
                        <{else}>
                          &nbsp;
                        <{/if}>
                      </font>
                    </td>
                  </tr>
                <{/foreach}>
              </table>
            </td>
            <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
          </tr>
          <tr>
            <td><img src="img/r_3.gif" width="10" height="10"></td>
            <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td><img src="img/r_4.gif" width="10" height="10"></td>
          </tr>
        </table>
        <input type="hidden" name="session" value="<{$session}>">
        <input type="hidden" name="page" value="<{$page}>">
      </form>
      <div align="right">
        <{if isset($last_page)}>
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
              <a href="hiyari_news_menu.php?session=<{$session}>&page=<{$last_page}>">←</a>
            </font>
        <{/if}>
        <{section name=cnt loop=$page_max}>
            <{if $smarty.section.cnt.iteration == $page}>
              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#FF6600">[<{$smarty.section.cnt.iteration}>]</font>
            <{else}>
              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                <a href="hiyari_news_menu.php?session=<{$session}>&page=<{$smarty.section.cnt.iteration}>"><{$smarty.section.cnt.iteration}></a>
              </font>
            <{/if}>
        <{/section}>
        <{if isset($next_page)}>
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
              <a href="hiyari_news_menu.php?session=<{$session}>&page=<{$next_page}>">→</a>
            </font>
        <{/if}>
      </div>
    </td>
  </tr>
</table>
</body>
</html>