<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$title}></title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <style>
	table#mail_message_toolbar1 tr td.mceFirst {width:1px;}
	div#content #mail_message_tbl tr td {padding: 0px;}
	div#content #mail_message_toolbargroup span {padding: 0px;} 
	div#content #mail_message_fontsizeselect{width:90px;}
	div#content #mail_message_fontsizeselect_text {width: 100px;}
  </style>
  <script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript">
    var w = 1024 + 37;
    var h = window.screen.availHeight;
    window.resizeTo(w, h);
    
    $(function(){
        //チェックボックス
        $('.checkbox').click(function() {
            $(this).closest('.checkbox_wrap').toggleClass('checked');
        })
    })
    
    //--------------------------------------------------
    //TinyMCE関連
    //--------------------------------------------------
    var m_use_tyny_mce = false;
    if(!tinyMCE.isOpera){
        m_use_tyny_mce = true;
        tinyMCE.init({
            mode : "textareas",
            theme : "advanced",
            plugins : "preview,table,emotions,fullscreen,layer",
            //language : "ja_euc-jp",
			language : "ja",
            width : "100%",
            height : "100",
            editor_selector : "mceEditor",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat,|,bullist,numlist,|,outdent,indent,|,hr,|,charmap,emotions,|,preview,|,undo,redo,|,fullscreen",
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3 : "",
            content_css : "tinymce/tinymce_content.css",
			theme_advanced_statusbar_location : "none",
            force_br_newlines : true,
			forced_root_block : '',
            force_p_newlines : false
        });
    }

    //--------------------------------------------------
    //ヘッダー処理関連
    //--------------------------------------------------
    // メッセージ表示
    function rp_action_message_disp() {
        location.href = "hiyari_rp_mail_disp.php?session=<{$session}>&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>";
    }

    //--------------------------------------------------
    //起動時の処理を行います。
    //--------------------------------------------------
    function loadaction()
    {
        //送信先を設定する。
        update_sendto_html();
    }

    //--------------------------------------------------
    //送信可能かチェックします。
    //--------------------------------------------------
    function send_check()
    {
        //入力チェック
        var err_msg = "";
        var is_ok = true;
        //TO必須
        if(document.getElementById("to_emp_id_list").value == ""){
            is_ok = false;
            err_msg = err_msg + "宛先を入力してください。\n";
        }
        //件名必須
        if(document.getElementById("mail_subject").value == ""){
            is_ok = false;
            err_msg = err_msg + "件名を入力してください。\n";
        }
        //メッセージ必須
        var mail_message = "";
        if(m_use_tyny_mce){
            //mail_message = tinyMCE.getContent();
            mail_message = tinyMCE.activeEditor.getContent();
        }
        else{
            mail_message = document.getElementById("mail_message").value;
        }
        if(mail_message == ""){
            is_ok = false;
            err_msg = err_msg + "メッセージを入力してください。\n";
        }
        if(!is_ok){
            alert(err_msg);
            return false;
        }
        if(!is_ok){
            alert(err_msg);
            return false;
        }

        //意思確認
        if(! confirm("送信します。よろしいですか？")){
            return false;
        }
        
        <{if isset($mail_message)}>
          //document.getElementById("mail_message").value = mail_message + '<{$mail_message}>';
        <{/if}>
        
        return true;
    }
  </script>
</head>

<body id="top" onload="loadaction()">
  <a id="pagetop" name="pagetop"></a>
  
  <form id="form1" name="form1" action="hiyari_rp_mail_input.php" method="post" onsubmit="return false;">
    <input type="hidden" name="is_postback" value="true">
    <input type="hidden" name="session" value="<{$session}>">
    <input type="hidden" name="mail_id" value="<{$mail_id}>">
    <input type="hidden" name="mode" value="<{$mode}>">
    <input type="hidden" name="report_id" value="<{$report_id}>">
    <input type="hidden" name="moto_mail_sender" value="<{$moto_mail_sender}>">

    <div id="header">
      <div class="inner">
        <h1><{$title}></h1>
        <ul class="sub_nav clearfix">
          <li class="sub_nav_send"><a href="javascript:void(0);" onclick="if(send_check()){document.form1.submit();};return false;">送信</a></li>
          <li class="sub_nav_return"><a href="javascript:void(0);" onclick="rp_action_message_disp();return false;">表示</a></li>
        </ul>
      </div>
    </div>

    <div id="content" class="clearfix">
      <div class="section">
        <div class="color01 radius mB40 clearfix">
          <!-- メールヘッダー -->
          <{include file="hiyari_mail_header_js.tpl"}>
          <{include file="hiyari_mail_header2.tpl"}>               
        </div>
        
        <div id="tinymce_div">
          <textarea id="mail_message" name="mail_message" rows="10" class="mceEditor"></textarea>
        </div>
      </div>
      
      <{include file="hiyari_mail_contents.tpl"}>
    </div>
  
    <div id="footer">
      <div class="inner">
        <!-- 実行アイコン START -->      
        <ul class="sub_nav clearfix">
          <li class="sub_nav_send"><a href="javascript:void(0);" onclick="if(send_check()){document.form1.submit();};return false;">送信</a></li>
          <li class="sub_nav_return"><a href="javascript:void(0);" onclick="rp_action_message_disp();return false;">表示</a></li>
        </ul>
        <!-- 実行アイコン END -->
      </div>
    </div>
  </form>
</body>
</html>