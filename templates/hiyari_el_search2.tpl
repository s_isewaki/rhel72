<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
  <script type="text/javascript">    
    var w = 1024 + 37;
    var h = window.screen.availHeight;
    window.resizeTo(w, h);
  </script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
</head>

<body id="el_search">
	<div id="header">
		<div class="inner">
			<h1><{$PAGE_TITLE}></h1>
		</div>
	</div>

  <div id="content" class="clearfix">
    <div class="section">
      <form name="search" action="hiyari_el_search.php" method="post">
        <div class="color01 radius clearfix mB10">
          <input type="hidden" name="session" value="<{$session}>">
          <input type="hidden" name="arch_id" value="<{$a}>">
          <table class="v_title">
            <tr>
              <th width="120">コンテンツ名</th>
              <td><input type="text" name="lib_nm" size="50" value="<{$lib_nm}>" style="ime-mode:active;"></td>
            </tr>
            <tr>
              <th>キーワード</th>
              <td><input type="text" name="keywd" size="50" value="<{$keywd}>" style="ime-mode:active;"></td>
            </tr>
            <tr>
              <th>登録者</td>
              <td>
                <input type="text" name="emp_name" size="30" value="<{$hid_emp_name}>" disabled>
                <input type="button" class="button radius_mini" value="選択" onclick="window.open('hiyari_el_member_list.php?session=<{$session}>', 'newwin', 'width=640,height=480,scrollbars=yes')">
                <input type="button" class="button radius_mini" value="クリア" onclick="document.search.emp_name.value = ''; document.search.hid_emp_name.value = ''; document.search.hid_emp_id.value = '';">
                <input type="hidden" name="hid_emp_id" value="<{$hid_emp_id}>">
                <input type="hidden" name="hid_emp_name" value="<{$hid_emp_name}>">
              </td>
            </tr>
            <tr>
              <th class="none">更新日</td>
              <td class="none">
                <select name="year">
                  <option value="-">
                  <{foreach from=$years item=y}>
                    <option value="<{$y.value}>" <{if $y.is_selected}>selected<{/if}>><{$y.year}></option>
                  <{/foreach}>
                </select>/
                <select name="month">
                  <option value="-">
                  <{foreach from=$months item=m}>
                    <option value="<{$m.value}>" <{if $m.is_selected}>selected<{/if}>><{$m.month}></option>
                  <{/foreach}>
                </select>/
                <select name="day">
                  <option value="-">
                  <{foreach from=$days item=d}>
                    <option value="<{$d.value}>" <{if $d.is_selected}>selected<{/if}>><{$d.day}></option>
                  <{/foreach}>
                </select>
              </td>
            </tr>
          </table>
        </div>
      
        <div id="main_btn" class="mB20">
            <input type="submit" class="button radius_mini" name="submit" value="検索">
        </div>
        
        <{if $submit != ""}>
          <div class="color02 radius clearfix mB20">
            <table class="list">
              <tr>
                <th>コンテンツ名</th>
                <th>タイプ</th>
                <th>容量</th>
                <th>更新日時</th>
                <th>権限</th>
                <th class="none">参照</th>
              </tr>
              <{foreach from=$library_list item=i}>
                <tr>
                  <td width="32%">
                    <a href="hiyari_el_update.php?session=<{$session}>&lib_id=<{$i.did}>&a=<{$a}>&c=<{$i.cid}>&f=<{$i.fid}>"><{$i.dnm}></a>
                  </td>
                  <td width="14%"><{$i.type}></td>
                  <td width="11%"><{$i.size}></td>
                  <td width="18%"><{$i.upd_time}></td>
                  <td width="7%" ><{$i.auth}></td>
                  <{if $lib_get_filename_flg > 0}>
                    <td width="8%" class="none">
                      <input type="button" class="button radius_mini" value="参照" onclick="location.href='hiyari_el_refer.php?s=<{$session}>&i=<{$i.did}>&u=<{$i.url}>'">
                    </td>
                  <{else}>
                    <td width="8%" class="none">
                      <input type="button" class="button radius_mini" value="参照" onclick="window.open('hiyari_el_refer.php?s=<{$session}>&i=<{$i.did}>&u=<{$i.url}>');">
                    </td>
                  <{/if}>
                </tr>
              <{/foreach}>
            </table>
          </div>
        <{/if}>         
      </form>
    </div>
  </div>    
</body>
</html>
