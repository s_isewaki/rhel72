<{*
インターフェース

	$colspan
		int     項目値側のTDタグの横結合数。省略時は未指定。
	$rowspan
		int     項目値側のTDタグの縦結合数。省略時は未指定。
	$td_title_width
		string  (未使用)タイトルのTDの幅。省略時は250
	$td_field_width
		string  (未使用)フィールドのTDの幅。省略時は未指定。


	$i
		string  グループコード
	$j
		string  アイテムコード
	$grps
		array   項目情報配列
	$vals
		array   値の配列

*}>
<{if $td_title_width == ""}>
	<{assign var="td_title_width" value="250"}>
<{/if}>


<td <{if $td_title_width != ""}>width="<{$td_title_width}>"<{/if}> bgcolor="#DFFFDC"   <{if $rowspan > 1}>rowspan="<{$rowspan}>"<{/if}> >
<{include file="hiyari_show_easyinput_item_title.tpl"}>
</td>
<td <{if $td_field_width != ""}>width="<{$td_field_width}>"<{/if}>   <{if $colspan > 1}>colspan="<{$colspan}>"<{/if}>  <{if $rowspan > 1}>rowspan="<{$rowspan}>"<{/if}>　 >
<{include file="hiyari_show_easyinput_item_field.tpl"}>
</td>
