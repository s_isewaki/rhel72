<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/pagination.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
  <script type="text/javascript">    
    var w = 1024 + 37;
    var h = window.screen.availHeight;
    window.resizeTo(w, h);
  </script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
</head>

<body id="el_ref">
	<div id="header">
		<div class="inner">
			<h1><{$PAGE_TITLE}></h1>
		</div>
	</div>
  
  <div id="content" class="clearfix">
    <div class="section">
      <div class="color01 radius clearfix mB20">
        <table id="sort_type">
          <{foreach name=row from=$sort_type item=st}>
            <tr>
              <{foreach name=col from=$st item=i}>
                <td class="<{if $smarty.foreach.row.last}>last_row<{/if}> <{if $smarty.foreach.col.last}>last_col<{/if}>">
                  <{if $i.is_selected}>
                    <b>
                      <{$i.title}>
                    </b>
                  <{else}>
                    <a href="hiyari_el_refer_log.php?session=<{$session}>&sort=<{$i.sort_id}>">
                      <{$i.title}>
                    </a>
                  <{/if}>
                </td>
              <{/foreach}>
            </tr>
          <{/foreach}>
        </table>
      </div>
      
      <{if isset($list)}>
        <div class="color02 radius clearfix mB10">
          <table class="list">
            <tr>
              <th width="22%">コンテンツ名</th>
              <th width="6%" class="num">参照数</th>
              <th width="14%">更新日時</th>
              <th width="12%">登録者</th>
              <th width="36%">保存先</th>
              <th width="10%" class="none btn">参照</th>
            </tr>
            <{foreach from=$list item=i}>
              <tr valign="top" bgcolor="#FFFFFF">
                <td><{$i.lib_nm}></td>
                <td class="num">
                    <{$i.ref_count}>
                </td>
                <td><{$i.lib_up_date}></td>
                <td><{$i.emp_nm}></td>
                <td>
                    <{$i.lib_cate_nm}>
                    <{$i.path}>
                </td>
                <{if $lib_get_filename_flg > 0}>
                  <td align="center" class="none btn"><input type="button" class="button radius_mini" value="参照" onclick="location.href = '<{$i.url}>';"></td>
                <{else}>
                  <td align="center" class="none btn"><input type="button" class="button radius_mini" value="参照" onclick="window.open('<{$i.url}>');"></td>
                <{/if}>
              </tr>
            <{/foreach}>
          </table>
          <form name="list_form" action="<{$fname}>" method="post">
            <{include file="hiyari_paging2.tpl"}>
            <input type="hidden" name="session" value="<{$session}>">
            <input type="hidden" name="sort" value="<{$sort}>">
            <input type="hidden" name="page" value="<{$page}>">
            <input type="hidden" name="mode" value="">
          </form>
        </div>
      <{/if}>
    </div>
  </div>
  
  <form name="delform" action="hiyari_el_delete.php" method="post">
    <input type="hidden" name="session" value="<{$session}>">
    <input type="hidden" name="did[]" value="">
    <input type="hidden" name="path" value="2">
    <input type="hidden" name="sort" value="<{$sort}>">
    <input type="hidden" name="page" value="<{$page}>">
  </form>
  <iframe name="download" width="0" height="0" frameborder="0"></iframe>
</body>
</html>
