<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.7.2.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
  <script type="text/javascript">
    $(function(){
      $('#show_in_another_window_btn2').hide();
      $('#back_to_analysis_window_btn').show();
    })
    
    window.onbeforeunload = function(){
//20131211  
//ページ遷移する度に画面を閉じられるとファイルダウンロードできなくなるのでコメント
//ページ遷移するのは「閉じる」「ファイルダウンロード」時のみと想定
//     back_to_analysis_window();
    } 
    
    function back_to_analysis_window()
    {
      if(window.opener && !window.opener.closed){
        window.opener.show_report_contents_in_this_window();
        window.opener.focus();
      }
      window.close();
    }
  </script>
</head>

<body>
  <div id="content">
    <{include file="report_contents_main.tpl"}>
  </div>  

  <!-- ファイル添付機能 -->
    <{if  $file_attach == "1"}>
       <{include file="report_form_file.tpl" input_mode=$gamen_mode}>
    <{/if}>

</body>
</html>
