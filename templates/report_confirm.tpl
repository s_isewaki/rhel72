<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <style>
    div#content #tinymce_div table span { padding : 0px; }
    div#tinymce_div table { width : 1%; }
    a#mail_message_fontsizeselect_text { width : 100px; }
  </style>
  <script type="text/javascript">
    $(function(){
        //チェックボックス
        $('.checkbox').click(function() {
            $(this).closest('.checkbox_wrap').toggleClass('checked');
        })
    })
    
    //--------------------------------------------------
    //TinyMCE関連
    //--------------------------------------------------
    var m_use_tyny_mce = false;
    if(!tinyMCE.isOpera)
    {
        m_use_tyny_mce = true;
        tinyMCE.init({
            mode : "textareas",
            theme : "advanced",
            plugins : "preview,table,emotions,fullscreen,layer",
            //language : "ja_euc-jp",
            language : "ja",
            width : "100%",
            height : "200",
            editor_selector : "mceEditor",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat,|,bullist,numlist,|,outdent,indent,|,hr,|,charmap,emotions,|,preview,|,undo,redo,|,fullscreen",
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3 : "",
            content_css : "tinymce/tinymce_content.css",
            theme_advanced_statusbar_location : "none",
            force_br_newlines : true,
            forced_root_block : '',
            force_p_newlines : false
        });
    }

    function show_non_tag_message()
    {
        //Operaの場合はタグ抜きしたTEXTを表示
        document.form1.mail_message.value = "<{$mail_message_non_tag}>";
    }

    //--------------------------------------------------
    //起動時の処理を行います。
    //--------------------------------------------------
    function loadaction()
    {
        <{if $is_mail_sendable}>    //送信先を設定する。
            update_sendto_html();

            //TinyMCEを使用しない場合はタグ抜きメッセージを表示する。
            if(! m_use_tyny_mce)
            {
                show_non_tag_message();
            }
        <{/if}>
    }

    //--------------------------------------------------
    //送信可能かチェックします。
    //--------------------------------------------------
    function send_check()
    {
        //入力チェック
        var err_msg = "";
        var is_ok = true;
        //TO必須
        if(document.getElementById("to_emp_id_list").value == "")
        {
            is_ok = false;
            err_msg = err_msg + "宛先を入力してください。\n";
        }
        //件名必須
        if(document.getElementById("mail_subject").value == "")
        {
            is_ok = false;
            err_msg = err_msg + "件名を入力してください。\n";
        }
    //  //メッセージ必須
    //  var mail_message = "";
    //  if(m_use_tyny_mce)
    //  {
    //      mail_message = tinyMCE.getContent();
    //  }
    //  else
    //  {
    //      mail_message = document.getElementById("mail_message").value;
    //  }
    //  if(mail_message == "")
    //  {
    //      is_ok = false;
    //      err_msg = err_msg + "メッセージを入力してください。\n";
    //  }
        if(!is_ok)
        {
            alert(err_msg);
            return false;
        }
        if(!is_ok)
        {
            alert(err_msg);
            return false;
        }

        //意思確認
        if(! confirm("送信します。よろしいですか？"))
        {
            return false;
        }

        return true;
    }
    
    
    
    // バックスペースキー押下による、ブラウザバック抑制
$(document).keydown(function(e) {
    if (   e.keyCode === 8
        || (e.altKey === true &&  e.keyCode === 37)) {
        var tag = e.target.nodeName.toLowerCase();
        var $target = $(e.target);
        if ((tag !== 'input' && tag !== 'textarea') || $target.attr('readonly') || $target.is(':disabled')) {
            document.FRM_MAIN.back.value='clicked';document.FRM_MAIN.submit();
            return false;
        }
    }
    return true;
});

  </script>
</head>
<body id="top" class="houkoku" onload="loadaction();">
  <a id="pagetop" name="pagetop"></a>

  <!-- ヘッダー START -->
	<div id="header">
		<div class="inner">
			<h1><{$PAGE_TITLE}></h1>
      <!-- 実行アイコン START -->
			<ul class="sub_nav clearfix">
        <li class="sub_nav_return"><a href="javascript:void(0);" onclick="document.FRM_MAIN.back.value='clicked';document.FRM_MAIN.submit();return false;">戻る</a></li>
        <{if $gamen_mode == 'new' || $gamen_mode == 'shitagaki'}>
          <li class="sub_nav_send"><a href="javascript:void(0);" onclick="if(send_check()){document.FRM_MAIN.send_create.value='clicked';document.FRM_MAIN.submit();return false;}">送信</a></li>
        <{else}>
          <{if $is_mail_not_sendable}>
            <li class="sub_nav_update"><a href="javascript:void(0);" onclick="document.FRM_MAIN.update.value='clicked';document.FRM_MAIN.submit();return false;">更新</a></li>
            <{/if}>
          <{if $is_mail_sendable}>
            <li class="sub_nav_send"><a href="javascript:void(0);" onclick="if(send_check()){document.FRM_MAIN.send_update.value='clicked';document.FRM_MAIN.submit();return false;}">送信</a></li>
          <{/if}>
        <{/if}>
      </ul>
      <!-- 実行アイコン END -->
		</div>    
	</div>  
  <!-- ヘッダー END -->

  <!-- 画面進捗 -->
  <div id="content" class="clearfix">
    <div class="section">
      <div id="flow" class="radius_mini ie_radius_mini mB30">
        <ul class="clearfix">
          <li class="flow_list01">出来事報告の流れ</li>
          <li class="flow_list02 actived"><span class="wrap"><span class="number_bg">●</span><span class="number">1</span>出来事の記入</span></li>            
          <{if $gamen_mode == "cate_update"}>
            <li class="flow_list04"><span class="wrap"><span class="number_bg">●</span><span class="number">2</span>更新</span></li>
          <{else}>
            <li class="flow_list03 active"><span class="wrap"><span class="number_bg">●</span><span class="number">2</span>記入内容を確認</span></li>
            <li class="flow_list04"><span class="wrap"><span class="number_bg">●</span><span class="number">3</span>送信</span></li>
          <{/if}>
        </ul>
      </div>
    </div>

    <form  name='FRM_MAIN' action='hiyari_easyinput_confirm.php' method='post'>
      <!-- ポストバック情報 -->
      <input type='hidden' name='is_postback' id='is_postback' value='ture'>
      <input type='hidden' name='shitagaki'   id='shitagaki'   value=''>
      <input type='hidden' name='update'      id='update'      value=''>
      <input type='hidden' name='send_update' id='send_update' value=''>
      <input type='hidden' name='send_create' id='send_create' value=''>
      <input type='hidden' name='back'        id='back'        value=''>
      <input type='hidden' name='eis_title_id' id='eis_title_id' value="<{$eis_title_id}>">

      <!-- セッション情報 -->
      <input type="hidden" name='session'     id='session'     value="<{$session}>">
      <input type="hidden" name='hyr_sid'     id='hyr_sid'     value="<{$hyr_sid}>">

      <!-- 報告部署 -->
      <div class="section">
        <div class="clearfix">
          <!-- 報告部署 -->
          <ul id="pankuzu" class="clearfix">
            <li class="title radius_mini">
                報告部署
            </li>
            <li>
              <span id="registrant_class_name"><{$class_name}></span>
            </li>
            <li>
              <span id="registrant_attribute_name">
                <{if isset($attribute_name)}>
                  <{$attribute_name}>
                <{/if}>
              </span>
            </li>
            <li <{if !isset($room_name)}>class="last"<{/if}>>
              <span id="registrant_dept_name">
                <{if isset($dept_name)}>
                  <{$dept_name}>
                <{/if}>
              </span>
            </li>
            <li class="last">
              <span id="registrant_room_name">
                <{if isset($room_name)}>
                  <{$room_name}>
                <{/if}>
              </span>
            </li>
          </ul>
          <input type="hidden" id="registrant_class" name="registrant_class" value="<{$registrant_class}>">
          <input type="hidden" id="registrant_attribute" name="registrant_attribute" value="<{$registrant_attribute}>">
          <input type="hidden" id="registrant_dept" name="registrant_dept" value="<{$registrant_dept}>">
          <input type="hidden" id="registrant_room" name="registrant_room" value="<{$registrant_room}>">
        </div>
        
        <!-- 事案番号 -->
        <{if $report_no != ""}>
          <div class="color01 radius mB40 clearfix">
            <table class="wrap_height">
              <tr>
                <th width="188">
                  事案番号
                </th>
                <td>
                  <span class="bold"><{$report_no}></span>
                </td>
              </tr>
            </table>
          </div>
        <{/if}>
      </div>
      
      <!-- メール情報 START -->
      <{if $is_mail_sendable}>
        <div class="section mB0">
          <h2 class="type01">メール送信設定</h2>
          <div class="color01 radius mB0">
            <{include file="hiyari_mail_header_js.tpl"}>
            <{include file="hiyari_mail_header2.tpl"}>            
          </div>
        </div>
          <div id="tinymce_div">
            <textarea id="mail_message" name="mail_message" style="width:100%; height:100;" rows="50" class="mceEditor"></textarea>
          </div>
      <{/if}>      
      <!-- メール情報 END -->

      <!-- 入力フォーム START -->
      <div class="section mT40">  
        <{include file="report_confirm_main.tpl"}>        
      </div>
      <!-- 入力フォーム END -->
    </form>    
  </div>
  <div id="footer">
    <div class="inner">
      <!-- 実行アイコン START -->      
      <ul class="sub_nav clearfix">
        <li class="sub_nav_return"><a href="javascript:void(0);" onclick="document.FRM_MAIN.back.value='clicked';document.FRM_MAIN.submit();return false;">戻る</a></li>
        <{if $gamen_mode == 'new' || $gamen_mode == 'shitagaki'}>
          <li class="sub_nav_send"><a href="javascript:void(0);" onclick="if(send_check()){document.FRM_MAIN.send_create.value='clicked';document.FRM_MAIN.submit();return false;}">送信</a></li>
        <{else}>
          <{if $is_mail_not_sendable}>
            <li class="sub_nav_update"><a href="javascript:void(0);" onclick="document.FRM_MAIN.update.value='clicked';document.FRM_MAIN.submit();return false;">更新</a></li>
            <{/if}>
          <{if $is_mail_sendable}>
            <li class="sub_nav_send"><a href="javascript:void(0);" onclick="if(send_check()){document.FRM_MAIN.send_update.value='clicked';document.FRM_MAIN.submit();return false;}">送信</a></li>
          <{/if}>
        <{/if}>
      </ul>
      <!-- 実行アイコン END -->
    </div>
  </div>
</body>
</html>
