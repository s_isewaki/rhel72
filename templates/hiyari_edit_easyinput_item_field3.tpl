<{*
インターフェース
    $grps
        array   項目情報配列
    $vals
        array   値の配列
    $grp_flags
        array   レポートに使用する項目グループ
    $i
        string  グループコード
    $j
        string  アイテムコード
    $style
        string  "textarea"に対する書式指定。"big"、"genmensoti"、"kaizensaku"
    $checkbox_line_count
        integer "checkbox"に対する一行辺りの表示項目数。省略時は4。
    $radio_line_count
        integer "radio"に対する一行辺りの表示項目数。省略時は4。
    $comp
        string  "select"の選択値を判定する際に項目名を判定するか項目コードを判定するかを決定する。"name"の場合は項目名判定。
                $compが設定されるケースは現状ない。(廃止可能)
    $text_field_width
        string  "text"のwidth値。省略時は250。
    $level_infos
        array   インシレベルとメッセージの配列
    $item_element_no_disp
        array   項目表示情報配列
    $add_tag_element
        string  "text"のみ対応。追加するタグ属性。

*}>
<{if $text_field_width == ""}>
  <{assign var="text_field_width" value="250"}>
<{/if}>

<{*影響区分・対応区分(カスタマイズ用)*}>
<{if isset($ic)}>
  <{if count($ic) == 1 && ($i==1400 || ($i==1410 && $j==20))}>
    <input type='checkbox' id="id_<{$i}>_<{$j}>_<{$key}>_0" name="_<{$i}>_<{$j}>[]" 
      value="<{$ic[0].item_id}>" onclick="checkbox_onclick(this)"
      <{if in_array($ic[0].item_id, (array)@$vals[$i][$j])}> checked<{/if}>>
    <label for="id_<{$i}>_<{$j}>_<{$key}>_0">
      <font class="j12">
        <{$ic[0].item_name|escape:"html"}>        
        <{*不明やその他の入力欄*}>
        <{if isset($ic[0].other_code)}>
          <{assign var="other_code" value=$ic[0].other_code}>
          <span id="id_<{$i}>_<{$other_code}>_<{$key}>">
            <{if in_array($key, (array)@$vals[$i][10])}>  <{*区分が変わってもother_codeは同じなので、選択中の区分の場合のみ出力*}>
              (
                <input type="text" id="id_<{$i}>_<{$other_code}>" name="_<{$i}>_<{$other_code}>"
                  value="<{$vals[$i][$other_code][0]}>"
                  style="width:<{$text_field_width}>;ime-mode:active;">
              )
            <{/if}>
          </span>
        <{/if}>
      </font>
    </label>
  <{elseif ($i==1400 || ($i==1410 && $j==20))}>
    <{if $checkbox_line_count == ""}>
      <{assign var="checkbox_line_count" value="4"}>
    <{/if}>
    <table width="100%" cellspacing="0" cellpadding="0" class="sellist">
      <tr height="22">
        <{php}>
          $checkbox_col_index = 0;
        <{/php}>
        <{section name=k loop=$ic}>
          <{if $item_element_no_disp[$i][$j] == "" 
          || !in_array($ic[$smarty.section.k.index].item_id,$item_element_no_disp[$i][$j])}>
            <{if isset($ic[$smarty.section.k.index].other_code)}>
              <{php}>$checkbox_col_index=$this->_tpl_vars['checkbox_line_count'];<{/php}>
              </tr><tr>
              <td colspan="<{$checkbox_line_count}>">
            <{else}>
              <{php}>
                $checkbox_col_index++;
                if($checkbox_col_index == $this->_tpl_vars['checkbox_line_count']+1)
                {
                  $checkbox_col_index=1;
              <{/php}>
                </tr><tr>
              <{php}>
                }
              <{/php}>
              <td style="width:25%">
            <{/if}>
              <input type='checkbox' id="id_<{$i}>_<{$j}>_<{$key}>_<{$smarty.section.k.index}>" name="_<{$i}>_<{$j}>[]" 
                value="<{$ic[$smarty.section.k.index].item_id}>"
                onclick="checkbox_onclick(this)"
                <{if in_array($ic[$smarty.section.k.index].item_id, (array)@$vals[$i][$j])}> checked<{/if}>>
              <label for="id_<{$i}>_<{$j}>_<{$key}>_<{$smarty.section.k.index}>">
                <font class="j12">
                  <{$ic[$smarty.section.k.index].item_name|escape:"html"}>
                  <{*不明やその他の入力欄*}>
                  <{if isset($ic[$smarty.section.k.index].other_code)}>  
                    <{assign var="other_code" value=$ic[$smarty.section.k.index].other_code}>
                    <span id="id_<{$i}>_<{$other_code}>_<{$key}>">
                      <{if in_array($key, (array)@$vals[$i][10])}>  <{*区分が変わってもother_codeは同じなので、選択中の区分の場合のみ出力*}>
                        (
                          <input type="text" id="id_<{$i}>_<{$other_code}>" name="_<{$i}>_<{$other_code}>"
                            value="<{$vals[$i][$other_code][0]}>"
                            style="width:<{$text_field_width}>;ime-mode:active">
                        )
                      <{/if}>
                    </span>
                  <{/if}>
                </font>
              </label>
            </td>
          <{/if}>
        <{/section}>
      </tr>
    </table>
  <{elseif count($ic) == 1 && ($i==1410 && $j==30)}>
    <input type='checkbox' id="id_<{$i}>_<{$j}>_<{$key}>_0" name="_<{$i}>_<{$j}>[]" 
      value="<{$ic[0].sub_item_id}>" onclick="checkbox_onclick(this)"
      <{if in_array($ic[0].sub_item_id, (array)@$vals[$i][$j])}> checked<{/if}>>
    <label for="id_<{$i}>_<{$j}>_<{$key}>_0">
      <font class="j12"><{$ic[0].sub_item_name|escape:"html"}></font>
    </label>
  <{elseif ($i==1410 && $j==30)}>
    <{if $checkbox_line_count == ""}>
      <{assign var="checkbox_line_count" value="4"}>
    <{/if}>
    <table width="100%" cellspacing="0" cellpadding="0" class="sellist">
      <tr height="22">
        <{php}>
          $checkbox_col_index = 0;
        <{/php}>
        <{section name=k loop=$ic}>
          <{if $item_element_no_disp[$i][$j] == "" || !in_array($ic[$smarty.section.k.index].item_id,$item_element_no_disp[$i][$j])}>
            <{php}>
              $checkbox_col_index++;
                if($checkbox_col_index == $this->_tpl_vars['checkbox_line_count']+1)
                {
                  $checkbox_col_index=1;
            <{/php}>
                    </tr><tr>
            <{php}>
                }
            <{/php}>
            <td style="width:25%">
              <input type='checkbox' id="id_<{$i}>_<{$j}>_<{$key}>_<{$smarty.section.k.index}>" name="_<{$i}>_<{$j}>[]"
                value="<{$ic[$smarty.section.k.index].sub_item_id}>" onclick="checkbox_onclick(this)"
                <{if in_array($ic[$smarty.section.k.index].sub_item_id, (array)@$vals[$i][$j])}> checked<{/if}>>
              <label for="id_<{$i}>_<{$j}>_<{$key}>_<{$smarty.section.k.index}>">
                <font class="j12"><{$ic[$smarty.section.k.index].sub_item_name|escape:"html"}></font>
              </label>
            </td>
          <{/if}>
        <{/section}>
      </tr>
    </table>
  <{/if}>
<{else}>
  <{*患者影響レベル*}>
  <{if $i == 90 && $j == 10}>
    <table width="100%" cellspacing="0" cellpadding="0" class="sellist">
      <tr>
        <td>
          <input type='radio' id="id_<{$i}>_<{$j}>_NON" name="_<{$i}>_<{$j}>[]" value=""
            <{if (array)@$vals[$i][$j] == ""}> checked<{/if}>
          <label for="id_<{$i}>_<{$j}>_NON"><font class="j12">&nbsp;未入力</font></label>
        </td>
      </tr>
      <{section name=k loop=$level_infos}>
        <{if $level_infos[k].use_flg}>
          <tr>
            <td>
              <input type='radio' name="_<{$i}>_<{$j}>[]" id="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>"
                value="<{$level_infos[k].easy_code}>"
                <{if in_array($level_infos[k].easy_code, (array)@$vals[$i][$j])}> checked<{/if}>>
              <label for="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>">
                <font class="j12"><{$level_infos[k].easy_name|escape:"html"}></font>
              </label>
            </td>
            <td>
              <font class="j12">：<{$level_infos[k].message|escape:"html"}></font>
            </td>
          </tr>
        <{/if}>
      <{/section}>
    </table>

  <{*発見者職種*}>
  <{elseif $i == 3020 && $j == 10}>
    <{if $radio_line_count == ""}>
      <{assign var="radio_line_count" value="4"}>
    <{/if}>
    <table width="100%" cellspacing="0" cellpadding="0" class="sellist">
      <tr>
        <td style="width:25%">
          <input type='radio' id="id_<{$i}>_<{$j}>_NON" name="_<{$i}>_<{$j}>[]" value=""
            <{if (array)@$vals[$i][$j] == ""}> checked<{/if}>>
          <label for="id_<{$i}>_<{$j}>_NON">
            <font class="j12">未入力</font>
          </label>
        </td>
        <{php}>$radio_col_index = 1;<{/php}>     
        <{section name=k loop=$grps[3050].easy_item_list[30].easy_list}>
          <{if $item_element_no_disp[3050][30] == "" || !in_array($grps[3050].easy_item_list[30].easy_list[k].easy_code,$item_element_no_disp[3050][30])}>
            <{php}>
              $radio_col_index++;
                if($radio_col_index == $this->_tpl_vars['radio_line_count']+1)
                {
                  $radio_col_index=1;
            <{/php}>
                    </tr><tr>
            <{php}>
                }
            <{/php}>
            <td style="width:25%">
              <input type='radio' id="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>" name="_<{$i}>_<{$j}>[]"
                value="<{$grps[3050].easy_item_list[30].easy_list[k].easy_code}>"
                <{if (in_array($grps[3050].easy_item_list[30].easy_list[k].easy_code, (array)@$vals[3020][10]))}> checked<{/if}>>
              <label for="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>">
                <font class="j12">
                  <{$grps[3050].easy_item_list[30].easy_list[k].easy_name|escape:"html"}>
                </font>
              </label>
            </td>
          <{/if}>
        <{/section}>
      </tr>
    </table>
    
  <{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'select'}>
      <select id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>"
        onchange="selectbox_onchange(this);
        <{if in_array($grps[$i].easy_item_list[$j].relate_group, $grp_flags)}>
          relate_detail(this,'rel_<{$grps[$i].easy_item_list[$j].relate_group}>');
        <{/if}>
        <{if $i==3000 && $j==10 && $gamen_mode != 'update'}>
          getDiscovererJob();
        <{/if}>"
      >
        <option value="">
        <{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}>
          <{if $item_element_no_disp[$i][$j] == "" 
          || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}>
            <option value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>"
              <{if ($comp != "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code, (array)@$vals[$i][$j])) 
              || ($comp == "name" && in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_name, (array)@$vals[$i][$j]))}>
                selected
              <{/if}>
            >
            <{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}>
          <{/if}>
        <{/section}>
      </select>
  <{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'text'}>
    <input type="text" id="id_<{$i}>_<{$j}>" name="_<{$i}>_<{$j}>" value="<{$vals[$i][$j][0]}>" 
      style="width:<{$text_field_width}>;ime-mode:<{$ime}>" <{$add_tag_element}> >
  <{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'textarea'}>
    <textarea style="width:100%;ime-mode:<{$ime}>" rows="<{if $style == "big"}>12<{elseif $style == "genmensoti" || $style == "kaizensaku" }>10<{else}>4<{/if}>" name="_<{$i}>_<{$j}>"><{$vals[$i][$j][0]}></textarea>
  <{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'checkbox'}>
    <{if count($grps[$i].easy_item_list[$j].easy_list) == 1}>
      <input type='checkbox' id="id_<{$i}>_<{$j}>_0" name="_<{$i}>_<{$j}>[]" 
        value="<{$grps[$i].easy_item_list[$j].easy_list[0].easy_code}>" 
        onclick="checkbox_onclick(this)"
        <{if in_array($grps[$i].easy_item_list[$j].easy_list[0].easy_code, (array)@$vals[$i][$j])}> checked<{/if}>>
      <label for="id_<{$i}>_<{$j}>_0">
        <font class="j12">
          <{$grps[$i].easy_item_list[$j].easy_list[0].easy_name|escape:"html"}>
          <{if isset($grps[$i].easy_item_list[$j].easy_list[0].other_code)}>  <{*不明やその他の入力欄*}>
            <{assign var="other_code" value=$grps[$i].easy_item_list[$j].easy_list[0].other_code}>
            (
            <input type="text" id="id_<{$i}>_<{$other_code}>" name="_<{$i}>_<{$other_code}>"
              value="<{$vals[$i][$other_code][0]}>"
              style="width:<{$text_field_width}>;ime-mode:<{$ime}>">
            )
          <{/if}>                    
        </font>
      </label>
    <{else}>
      <{if $checkbox_line_count == ""}>
        <{assign var="checkbox_line_count" value="4"}>
      <{/if}>
      <table width="100%" cellspacing="0" cellpadding="0" class="sellist">
        <tr height="22">
          <{php}>$checkbox_col_index = 0;<{/php}>
          <{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}>
            <{if $item_element_no_disp[$i][$j] == "" 
              || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}>            
              <{if isset($grps[$i].easy_item_list[$j].easy_list[k].other_code)}>  <{*不明やその他の入力欄がある場合*}>
                <{php}>$checkbox_col_index=$this->_tpl_vars['checkbox_line_count'];<{/php}>
                </tr><tr>
                <td colspan="<{$checkbox_line_count}>">
              <{else}>
                <{php}>
                  $checkbox_col_index++;
                  if($checkbox_col_index == $this->_tpl_vars['checkbox_line_count']+1) 
                  {
                    $checkbox_col_index=1;
                <{/php}>
                    </tr><tr>
                <{php}>
                  }
                <{/php}>
                <td style="width:25%">               
              <{/if}>
                  <input type='checkbox' id="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>" name="_<{$i}>_<{$j}>[]" 
                    value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>" 
                    onclick="checkbox_onclick(this)"
                    <{if in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code, (array)@$vals[$i][$j])}> checked<{/if}>>
                  <label for="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>">
                    <font class="j12">
                      <{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}>
                    </font>
                  </label>
                  <{if isset($grps[$i].easy_item_list[$j].easy_list[k].other_code)}>  <{*不明やその他の入力欄*}>
                    <{assign var="other_code" value=$grps[$i].easy_item_list[$j].easy_list[k].other_code}>
                    (
                    <input type="text" id="id_<{$i}>_<{$other_code}>" name="_<{$i}>_<{$other_code}>"
                      value="<{$vals[$i][$other_code][0]}>"
                      style="width:<{$text_field_width}>;ime-mode:<{$ime}>">
                    )
                  <{/if}>                    
                </td>
            <{/if}>
          <{/section}>
        </tr>
      </table>
    <{/if}>

  <{elseif $grps[$i].easy_item_list[$j].easy_item_type == 'radio'}>
    <{if $radio_line_count == ""}>
      <{assign var="radio_line_count" value="4"}>
    <{/if}>
    <table id="id_<{$i}>_<{$j}>" width="100%" cellspacing="0" cellpadding="0" class="sellist">
      <tr>
        <td style="width:25%">
          <input type='radio' id="id_<{$i}>_<{$j}>_NON" name="_<{$i}>_<{$j}>[]" value=""
            <{if $i==900 && $j==10}>          <{*概要・場面・内容（2010年版）：概要*}>
              onclick="hide_must_2010();disp_hide_scene_content_2010();cng_scene_2010();cng_kind_2010();cng_content_2010();disp_item_2010();reset_item_2010();disp_kind_2010();delete_detail_item_2010(this.form);"
            <{elseif $i==910 && $j==10}>      <{*概要・場面・内容（2010年版）：種類*}>
              onclick="cng_kind_item_2010();"
            <{elseif $i == 940 && $j == 10}>  <{*概要・場面・内容（2010年版）：発生場面*}>
              onclick="cng_scene_item_2010();cng_item_2010();"
            <{elseif $i == 970 && $j == 10}>  <{*概要・場面・内容（2010年版）：事例の内容*}>
              onclick="cng_content_item_2010();"
            <{else}>
              onclick="radio_onclick(this)"
            <{/if}>
            <{if in_array($grps[$i].easy_item_list[$j].relate_group, $grp_flags)}> 
              onchange="relate_detail(this,'rel_<{$grps[$i].easy_item_list[$j].relate_group}>');"
            <{/if}>            
            <{*if (array)@$vals[$i][$j]=="" || !isset($vals[$i][$j])*}>
            <{if (array)@$vals[$i][$j]==""}>
              checked
            <{/if}>
          >
          <label id="id_<{$i}>_<{$j}>_NON_label" for="id_<{$i}>_<{$j}>_NON"><font class="j12">未入力</font></label>
        </td>
        <{php}>$radio_col_index = 1;<{/php}>
        <{section name=k loop=$grps[$i].easy_item_list[$j].easy_list}>
          <{if $item_element_no_disp[$i][$j] == "" 
            || !in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code,$item_element_no_disp[$i][$j])}>
            <{if isset($grps[$i].easy_item_list[$j].easy_list[k].other_code)}>  <{*不明やその他の入力欄がある場合*}>
              <{php}>$radio_col_index=$this->_tpl_vars['radio_line_count'];<{/php}>
              </tr><tr>
              <td colspan="<{$radio_line_count}>">
            <{else}>
              <{php}>
                $radio_col_index++;
                if($radio_col_index == $this->_tpl_vars['radio_line_count']+1)
                {
                  $radio_col_index=1;
              <{/php}>  
                  </tr><tr>
              <{php}>
                }
              <{/php}>
              <td style="width:25%">
            <{/if}>
            <input type='radio' id="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>" name="_<{$i}>_<{$j}>[]"
              value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>"
              <{if $i==900 && $j==10}>          <{*概要・場面・内容（2010年版）：概要*}>
                onclick="hide_must_2010();disp_hide_scene_content_2010();cng_scene_2010();cng_kind_2010();cng_content_2010();disp_item_2010();reset_item_2010();disp_kind_2010();delete_detail_item_2010(this.form);"
              <{elseif $i==910 && $j==10}>      <{*概要・場面・内容（2010年版）：種類*}>
                onclick="cng_kind_item_2010();"
              <{elseif $i == 940 && $j == 10}>  <{*概要・場面・内容（2010年版）：発生場面*}>
                onclick="cng_scene_item_2010();cng_item_2010();"
              <{elseif $i == 970 && $j == 10}>  <{*概要・場面・内容（2010年版）：事例の内容*}>
                onclick="cng_content_item_2010();"
              <{else}>
                onclick="radio_onclick(this)"
              <{/if}>
              <{if in_array($grps[$i].easy_item_list[$j].relate_group, $grp_flags)}>
                onchange="relate_detail(this,'rel_<{$grps[$i].easy_item_list[$j].relate_group}>');"
              <{/if}>              
              <{if in_array($grps[$i].easy_item_list[$j].easy_list[k].easy_code, (array)@$vals[$i][$j])}> 
                checked
              <{/if}>
            >
            <label for="id_<{$i}>_<{$j}>_<{$smarty.section.k.index}>">
              <font class="j12">
                <{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}>
              </font>
            </label>
            <{if isset($grps[$i].easy_item_list[$j].easy_list[k].other_code)}>  <{*不明やその他の入力欄*}>
              <{assign var="other_code" value=$grps[$i].easy_item_list[$j].easy_list[k].other_code}>
              (
              <input type="text" id="id_<{$i}>_<{$other_code}>" name="_<{$i}>_<{$other_code}>"
                value="<{$vals[$i][$other_code][0]}>"
                style="width:<{$text_field_width}>;ime-mode:<{$ime}>">
              )
            <{/if}>
            <{if $i==125 && $j==85}>
              <input type="hidden" id="item_125_85_<{$grps[$i].easy_item_list[$j].easy_list[k].easy_code}>"
                value="<{$grps[$i].easy_item_list[$j].easy_list[k].easy_name|escape:"html"}>">
            <{/if}>
            </td>
          <{/if}>
        <{/section}>
      </tr>
    </table>
  <{/if}>
<{/if}>
