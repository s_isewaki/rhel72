<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
         <meta http-equiv="X-UA-Compatible" content="IE=8" />
 
        <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
        <meta http-equiv="imagetoolbar" content="no" />
        <link type="text/css" rel="stylesheet" media="all" href="css/default.css"/>
        <link type="text/css" rel="stylesheet" media="all" href="css/common.css"/>
        <link type="text/css" rel="stylesheet" media="all" href="css/pastreport.css"/>
         <link rel="stylesheet" type="text/css" href="css/main.css">
      <!--  <link type="text/css" rel="stylesheet" media="all" href="css/date_search.css" />
         <link type="text/css" rel="stylesheet" media="all" href="css/pagination.css" />-->
        <style tyle="text/css">
            body {padding-bottom: 50px;}
        </style>
            
        <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="js/libs.js"></script>
        <script type="text/javascript" src="js/jquery/jquery-ui-1.10.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery/jquery.ui.datepicker-ja.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="js/hiyari_report.js"></script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
  <script type="text/javascript">    
   
   var session = "<{$session}>";
   
   
   function rp_action_return_disp() {
       location.href  = "hiyari_easyinput.php?session=<{$session}>&gamen_mode=update&report_id=" + <{$original_report_id}> + "&callerpage="+"<{$callerpage}>"+"&mail_id="+<{$mail_id}>;
        
    }
    
    //報告書表示(参照モード)
    function rp_action_update_from_report_id(report_id) {
        var url = "hiyari_easyinput.php?session=<{$session}>&gamen_mode=update&report_id=" + report_id + "&callerpage=" + "<{$callerpage}>" + "<{$non_update_flg}>"+"&mail_id="+<{$mail_id}>;
        show_rp_sub_window(url);
    }

    //レポーティング子画面を表示
    function show_rp_sub_window(url) {
        var h = window.screen.availHeight - 30;
        var w = window.screen.availWidth - 10;
        var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;

        window.open(url, '_blank',option);
    }
       // ソート処理
    function set_report_list_sort(item)
    {
        var sort_item = document.list_form.sort_item.value;
        if(sort_item == item){
            var sort_div = document.list_form.sort_div.value
            if(sort_div == "0"){
                // 降順
                document.list_form.sort_div.value = "1";
            }else{
                // 昇順
                document.list_form.sort_div.value = "0";
            }
        }else{
            // 昇順
            document.list_form.sort_div.value = "0";
        }

        document.list_form.sort_item.value = item;
        document.list_form.mode.value="sort";
        document.list_form.submit();
    }

    jQuery(function($){
    //チェックボックスのチェックがある時のtr
    $('.checkbox').click(function() {
        $(this).closest('tr').toggleClass('checked');
    });	
      

    });
    
    </script>
        <link rel="stylesheet" type="text/css" href="css/main.css">
       
    </head>
    
    <body id="pastreport_list">
        <form name="list_form" method="post" action="hiyari_pastreport_list.php">
        <input type="hidden" name="session" value="<{$session}>">
        <input type="hidden" name="sort_item" value="<{$sort_item}>">
        <input type="hidden" name="sort_div" value="<{$sort_div}>">
        <input type="hidden" name="mode" value="search">
         
        <input type="hidden" name="report_id" value="">
        <input type="hidden" name="report_id_list" value="<{$excel_target_report_id_list}>">
            
        <div id="header">
            <div class="inner">
		<h1>同一人報告書</h1>
                <!-- 実行アイコン START -->
                <ul class="sub_nav clearfix">
                    <li class="sub_nav_return"><a href="javascript:rp_action_return_disp();">戻る</a></li>
                </ul>
                <!-- 実行アイコン END -->

            </div>
	</div>
         
             
             
        <div id="content">
            <!-- 一覧 START -->
            <div  class="color02">
            <!-- 実行アイコン START -->
                <ul id="main_nav">
                    <li class="main_nav_10"><a href="#" onclick="rp_action_analysis_pastreport();">分析</a></li>
                    <li class="main_nav_08"><a href="#" onclick="rp_action_report_excel();">出力</a></li>
                </ul>
                <table id="main_list" class="list">
                <!----------------------------------------------------------------->
                <!-- 一覧のタイトル -->
                <!----------------------------------------------------------------->
                    <tr>
                        <th  width="15"></th>
                        <th width="80"><!--<a href="javascript:set_report_list_sort('REPORT_NO');">-->事案番号</a></th>
                        <th class="subject"><!--<a href="javascript:set_report_list_sort('REPORT_TITLE');">-->表題</a></th>
                        <th width="58">報告部署</a></th>
                        <th width="50">報告者</a></th>
                        <th width="75"><!--<a href="javascript:set_report_list_sort('<{$date_type}>');">-->報告年月日</a></th>
                    </tr>      
                    <{foreach from=$report_list item=i name=n}>
                    <tr class="kidokus">
                        <td class="select">
                            <span class="checkbox_wrap" <{if $i.checked}> checked<{/if}>>
                              <input type="checkbox" class="checkbox" name="list_select[]" value="<{$i.report_id}>">
                            </span>
                            <input type="hidden" id="report_id_of_list_id_<{$i.report_id}>" value="<{$i.report_id}>">
                        </td>
                        <td  style="text-align:right;">
                            <{$i.report_no}>
                        </td>
                        <td style="text-align:left;">
                            <a href="javascript:rp_action_update_from_report_id('<{$i.report_id}>');">
                            <{$i.report_title}>
                        </td>
                        <td>
                            <{$i.class_nm}>
                        </td>
                        <td>
                        <{if $i.is_anonymous_report && $is_mouseover_popup}>
                          <a href="#" title="<{$i.real_name|escape}>">匿名</a>
                        <{else}>
                            <{$i.disp_emp_name}>
                        <{/if}>
                            
                            
                            <!--<{$i.registrant_name}>-->
                        </td>
                        <td>
                            <{$i.send_time}>
                        </td>
                    </tr>
                   <{/foreach}>   
                </table>
            </div>
        
        </div>
        </form>
        <div id="footer">
            <p>Copyright Comedix all right reserved.</p>
	</div>
    </body>
</html>