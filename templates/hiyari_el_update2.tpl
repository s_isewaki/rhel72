<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
  <script type="text/javascript" src="js/hiyari_el.js"></script>
  <script type="text/javascript">
    var w = 1024 + 37;
    var h = window.screen.availHeight;
    window.resizeTo(w, h);
    //--------------------------------------------------
    //登録対象者配列
    //--------------------------------------------------
    m_target_list = new Array();
    //参照権限の対象職員
    m_target_list[1] = new Array(
        <{foreach name=n from=$arr_target[1] item=i}>
          <{if !$smarty.foreach.n.first}>,<{/if}>
          new user_info('<{$i.id}>','<{$i.name}>')
        <{/foreach}>
      );
    //更新権限の対象職員
    m_target_list[2] = new Array(
        <{foreach name=n from=$arr_target[2] item=i}>
          <{if !$smarty.foreach.n.first}>,<{/if}>
          new user_info('<{$i.id}>','<{$i.name}>')
        <{/foreach}>
      );

    //--------------------------------------------------
    //部署情報配列
    //--------------------------------------------------
    //部
    var classes = [];
    <{foreach from=$sel_class item=i}>
      classes.push({id: '<{$i.class_id}>', name: '<{$i.class_nm}>'});
    <{/foreach}>

    //課
    var atrbs = {};
    <{foreach from=$sel_atrb key=class_id item=c}>
      atrbs['<{$class_id}>'] = [];
      <{foreach from=$c key=atrb_id item=a}>
        atrbs['<{$class_id}>'].push({id: '<{$atrb_id}>', name: '<{$a}>'});
      <{/foreach}>
    <{/foreach}>

    //科
    var depts = {};
    <{foreach from=$sel_dept key=class_id item=c}>
      depts['<{$class_id}>'] = {};
      <{foreach from=$c key=atrb_id item=a}>
        depts['<{$class_id}>']['<{$atrb_id}>'] = [];
        <{foreach from=$a key=dept_id item=d}>
          depts['<{$class_id}>']['<{$atrb_id}>'].push({id: '<{$dept_id}>', name: '<{$d}>'});
        <{/foreach}>
      <{/foreach}>
    <{/foreach}>

    //--------------------------------------------------
    //画面起動時の処理
    //--------------------------------------------------
    function initPage()
    {
        //登録対象者を設定する。
        update_target_html("1");
        update_target_html("2");

        //履歴管理有無の有効制御
        setManageHistoryButtonDisabled();

        //権限部分の初期制御
        onChangeArchive(true, '<{$ref_class_src}>', '<{$ref_atrb_src}>', '<{$upd_class_src}>', '<{$upd_atrb_src}>');
    }
    
    //----------------------------------------------------------------------------------------------------
    // 権限関係以外の画面処理
    //----------------------------------------------------------------------------------------------------
    //ファイルパスを解析し、コンテンツ名・コンテンツタイプをセットします。
    function setFileInfo(path)
    {
        //コンテンツ名にセット
        var fname;
        if (path.indexOf('\\') != -1)
        {
            fname = path.replace(/^.*\\/, '');
        }
        else if (path.indexOf('/') != -1)
        {
            fname = path.replace(/^.*\//, '');
        }
        else
        {
            fname = path;
        }
        var docname = fname.replace(/\.[^.]*$/, '');
        document.mainform.document_name.value = docname;

        //コンテンツタイプにセット
        var ext = fname.replace(/.*\./, '');
        switch (ext)
        {
            case 'doc':
            case 'docx':
                document.mainform.document_type.options[1].selected = true;
                break;
            case 'xls':
            case 'xlsx':
                document.mainform.document_type.options[2].selected = true;
                break;
            case 'ppt':
            case 'pptx':
                document.mainform.document_type.options[3].selected = true;
                break;
            case 'pdf':
                document.mainform.document_type.options[4].selected = true;
                break;
            case 'txt':
                document.mainform.document_type.options[5].selected = true;
                break;
            case 'jpg':
            case 'jpeg':
                document.mainform.document_type.options[6].selected = true;
                break;
            case 'gif':
                document.mainform.document_type.options[7].selected = true;
                break;
            default:
                document.mainform.document_type.options[8].selected = true;
                break;
        }
    }

    //履歴管理有無の有効制御
    function setManageHistoryButtonDisabled()
    {
        //新たにファイルがアップロードされる場合のみ有効
        var disabled = (document.mainform.upfile.value == '');
        document.mainform.manage_history[0].disabled = disabled;
        document.mainform.manage_history[1].disabled = disabled;
    }
  </script>
  <script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
  <script type="text/javascript" src="js/yui/build/connection/connection-min.js"></script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
</head>

<body id="el_reg" onload="initPage();">
  <!-------------------------------------------------------------------------
  ヘッダー 
  -------------------------------------------------------------------------->
	<div id="header" class="clearfix">
		<div class="inner" class="clearfix">
			<h1><{$PAGE_TITLE}></h1>
      <p class="action_btn">
        <input type="button" class="<{if $upd_flg}>button<{else}>button_disabled<{/if}> radius_mini" value="更新" onclick="submitForm();"<{if !$upd_flg}> disabled<{/if}>>
        <input type="button" class="button radius_mini" value="参照" onclick="<{$refer_onclick}>">
      </p>
      <p class="notice">
        ※ファイルを更新したい場合のみ、参照ボタンでファイルを選択してください。<{$upload_max_filesize}>を超えるファイルは登録できません。
      </p>
		</div>
	</div>

  <!-------------------------------------------------------------------------
  コンテンツ
  -------------------------------------------------------------------------->
  <div id="content" class="clearfix">
    <div class="section">
      <form name="mainform" action="hiyari_el_update_exe.php" method="post" enctype="multipart/form-data">
        <div class="color02 radius clearfix mB20">
          <table class="v_title">
            <!-- ファイル名-履歴管理 START -->
            <tr>
              <th>ファイル</th>
              <td>
                <input type="file" name="upfile" size="50" value="" style="ime-mode:inactive;" onchange="setFileInfo(this.value);setManageHistoryButtonDisabled();">
              </td>
              <th>履歴管理</th>
              <td>
                <input type="radio" name="manage_history" value="t"<{if $manage_history == "t"}> checked<{/if}>>する
                <input type="radio" name="manage_history" value="f"<{if $manage_history == "f"}> checked<{/if}>>しない
              </td>
            </tr>
            <!-- ファイル名-履歴管理 END -->
                    
            <!-- コンテンツ名-コンテンツタイプ START -->
            <tr>
              <th>コンテンツ名</th>
              <td>
                <input name="document_name" type="text" size="50" maxlength="100" value="<{$document_name}>" style="ime-mode:active;">
              </td>
              <th>コンテンツタイプ</th>
              <td width="17%" bgcolor="#FFFFFF">
                <select name="document_type">
                  <option value="0"<{if $document_type == "0"}> selected<{/if}>>
                  <option value="1"<{if $document_type == "1"}> selected<{/if}>>Word
                  <option value="2"<{if $document_type == "2"}> selected<{/if}>>Excel
                  <option value="3"<{if $document_type == "3"}> selected<{/if}>>PowerPoint
                  <option value="4"<{if $document_type == "4"}> selected<{/if}>>PDF
                  <option value="5"<{if $document_type == "5"}> selected<{/if}>>テキスト
                  <option value="6"<{if $document_type == "6"}> selected<{/if}>>JPEG
                  <option value="7"<{if $document_type == "7"}> selected<{/if}>>GIF
                  <option value="99"<{if $document_type == "99"}> selected<{/if}>>その他
                </select>
              </td>
            </tr>
            <!-- コンテンツ名-コンテンツタイプ END -->

            <!-- キーワード-コンテンツ番号 START -->
            <tr>
              <th>キーワード</th>
              <td>
                <input name="keywd" type="text" size="50" maxlength="50" value="<{$keywd}>" style="ime-mode:active;">
              </td>
              <th class="borderb">コンテンツ番号</th>
              <td>
                <input type="text" name="lib_no" value="<{$lib_no}>" style="ime-mode:inactive;">
              </td>
            </tr>
            <!-- キーワード-コンテンツ番号 END -->

            <!-- 説明 START -->
            <tr>
              <th>説明</th>
              <td colspan="3">
                <textarea name="explain" rows="5" cols="40" style="ime-mode:active;"><{$explain}></textarea>
              </td>
            </tr>
            <!-- 説明 END -->

            <!-- 保存先 START -->
            <tr>
              <th>保存先</th>
              <td colspan="3">
                <span id="folder_path" style="padding-right:2px;">
                  <{$cate_nm}>
                  <{foreach name=n from=$folder_path item=fp}>
                    &gt;&nbsp;<{$fp.name}>
                  <{/foreach}>
                </span>
                <input type="button" class="button radius_mini" value="選択" onclick="selectFolder('<{$session}>', '<{$path}>');">
                <input type="hidden" name="archive" value="<{$archive}>">
                <input type="hidden" name="category" value="<{$category}>">
                <input type="hidden" name="folder_id" value="<{$folder_id}>">
              </td>
            </tr>
            <!-- 保存先 END -->

            <!-- 登録者-更新日時 START -->
            <tr>
              <th>登録者</th>
              <td><{$reg_emp_name}>（<{$reg_class_nm}>）</td>
              <th>更新日時</th>
              <td><{$upd_time}></td>
            </tr>
            <!-- 登録者-更新日時 END -->

            <!-- 参照数-版番号 START -->
            <tr>
              <th class="none">参照数</th>
              <td class="none">
                <{if $has_admin_auth && $ref_count > 0}>
                  <a href="javascript:void(0);" onclick="window.open('hiyari_el_admin_refer_log_detail.php?session=<{$session}>&lib_id=<{$lib_id}>', 'newwin', 'scrollbars=yes,width=640,height=700');"><{$ref_count}></a>
                <{else}>
                  <{$ref_count}>
                <{/if}>
              </td>
              <th class="none">版番号</th>
              <td class="none">
                <{if $edition_no > 1}>
                  <a href="javascript:void(0);" onclick="window.open('hiyari_el_history.php?session=<{$session}>&lib_id=<{$lib_id}>&path=<{$path}>', 'libhist', 'width=640,height=480,scrollbars=yes');"><{$edition_no}></a>
                <{else}>
                  <{$edition_no}>
                <{/if}>
              </td>
            </tr>
            <!-- 参照数-版番号 END -->
          </table>
        </div>
    
        <!-- 参照・更新可能職員指定 -->
        <{include file="hiyari_el_emp2.tpl"}>               

        <input type="hidden" name="session" value="<{$session}>">
        <input type="hidden" name="lib_id" value="<{$lib_id}>">
        <input type="hidden" name="a" value="<{$a}>">
        <input type="hidden" name="c" value="<{$c}>">
        <input type="hidden" name="f" value="<{$f}>">
        <input type="hidden" name="o" value="<{$o}>">
        <input type="hidden" name="path" value="<{$path}>">
        <input type="hidden" name="ref_toggle_mode" value="">
        <input type="hidden" name="upd_toggle_mode" value="">
        <input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
        <input type="hidden" id="target_name_list1" name="target_name_list1" value="">
        <input type="hidden" id="target_id_list2"   name="target_id_list2" value="">
        <input type="hidden" id="target_name_list2" name="target_name_list2" value="">
      </form>
    </div>
  </div>
  
  <!-----------------------------------------------------------------
  フッター
  ------------------------------------------------------------------>
  <div id="footer">
    <div class="inner">
      <p class="action_btn">
        <input type="button" class="<{if $upd_flg}>button<{else}>button_disabled<{/if}> radius_mini" value="更新" onclick="submitForm();"<{if !$upd_flg}> disabled<{/if}>>
        <input type="button" class="button radius_mini" value="参照" onclick="<{$refer_onclick}>">
      </p>
      <p class="notice">
        ※ファイルを更新したい場合のみ、参照ボタンでファイルを選択してください。<{$upload_max_filesize}>を超えるファイルは登録できません。
      </p>
    </div>
  </div>
</body>
</html>
