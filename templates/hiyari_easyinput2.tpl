<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
  jQuery.noConflict();
  var j$ = jQuery;
</script>
<script type="text/javascript">
  //====================================================================================================
  //実行アイコン
  //====================================================================================================
  // 返信
  function rp_action_return() 
  {
    location.href = "hiyari_rp_mail_input.php?session=<{$session}>&mode=return&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>";
  }
  // 転送
  function rp_action_forward()
  {
    location.href = "hiyari_rp_mail_input.php?session=<{$session}>&mode=forward&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>";
  }
  // メッセージ表示
  function rp_action_message_disp() 
  {
    location.href = "hiyari_rp_mail_disp.php?session=<{$session}>&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>";
  }
  //進捗登録
  function rp_action_progress() 
  {
    location.href = "hiyari_rp_progress.php?session=<{$session}>&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>&report_id=<{$report_id}>";
  }
  //印刷
  function rp_action_print()
  {
    <{if $patient_non_print_flg}>
      var patient_non_print_flg = true;
    <{else}>
      var patient_non_print_flg = false;
    <{/if}>
    
    <{if $patient_non_print_select_flg}>
      patient_non_print_flg = !confirm("患者ID、氏名の印刷を有効にしますか？");
    <{/if}>
    
    var param_patient_non_print = "false";
    if(patient_non_print_flg){
        param_patient_non_print = "true";
    }

    <{if $gamen_mode == "analysis"}>
      var url = "hiyari_report_print.php?session=<{$session}>&report_id=<{$report_id}>&patient_non_print=" + param_patient_non_print;
    <{elseif $gamen_mode == "update"}>
      //var url = "hiyari_report_print.php?session=<{$session}>&report_id=<{$report_id}>&eis_id=<{$mail_eis_id}>&gamen_mode=<{$gamen_mode}>&patient_non_print=" + param_patient_non_print;
        <{if $sel_job != ""}>//指定様式の時
            var url = "hiyari_report_print.php?session=<{$session}>&report_id=<{$report_id}>&eis_id=<{$mail_eis_id}>&gamen_mode=<{$gamen_mode}>&patient_non_print=" + param_patient_non_print;
        <{else}>//全項目のとき
            var url = "hiyari_report_print.php?session=<{$session}>&report_id=<{$report_id}>&eis_id=&gamen_mode=<{$gamen_mode}>&patient_non_print=" + param_patient_non_print;
        <{/if}>
    <{else}>
      var url = "hiyari_report_print.php?session=<{$session}>&report_id=<{$report_id}>&eis_id=<{$mail_eis_id}>&patient_non_print=" + param_patient_non_print;
    <{/if}>
    
    window.open(url);
  }

  //====================================================================================================
  //起動時の処理
  //====================================================================================================
  j$(document).ready(function(){
    <{if !$readonly_flg && $eis_select_able}>
      set_eis_input_list();
      set_eis_no(<{$sel_eis_no}>);
    <{/if}>

    <{if $auto_reload}>
      start_auto_session_update();
    <{/if}>  
  });
</script>
<link rel="stylesheet" type="text/css" href="css/incident.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/easyinput.css">
</head>

<body>
  <script type="text/javascript">
    var all_contents = document.getElementById("all_contents");
  </script>
  <a id="pagetop" name="pagetop"></a>

  <!-- セッションタイムアウト防止 START -->
  <{if $auto_reload}>
    <script type="text/javascript">
    function start_auto_session_update()
    {
        //1分(60*1000ms)後に開始
        setTimeout(auto_sesson_update,60000);
    }
    function auto_sesson_update()
    {
        //セッション更新
        document.session_update_form.submit();

        //1分(60*1000ms)後に最呼び出し
        setTimeout(auto_sesson_update,60000);
    }
    </script>
    <form name="session_update_form" action="hiyari_session_update.php" method="post" target="session_update_frame">
      <input type="hidden" name="session" value="<{$session}>">
    </form>
    <iframe name="session_update_frame" width="0" height="0" frameborder="0"></iframe>
  <{/if}>
  <!-- セッションタイムアウト防止 END -->

  <!-- ヘッダー START -->
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr height="32" bgcolor="#35B341">
      <td class="spacing">
        <span id="title"><{$PAGE_TITLE}></span>
      </td>
      <td>&nbsp;</td>
      <td width="10">&nbsp;</td>
      <td width="32" align="center">
        <a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a>
      </td>
    </tr>
  </table>
  <img src="img/spacer.gif" width="10" height="10" alt=""><br>  
  <!-- ヘッダー END -->
  
  <div id="all_contents">
    <table width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <!-- 上部 START -->
        <td class="tbl_layout">          
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <!-- 実行アイコン START -->
              <td class="tbl_layout">
                <table cellspacing="0" cellpadding="0">
                  <tr>
                    <{if !$readonly_flg}>
                      <{if $gamen_mode == "new" || $gamen_mode == "shitagaki"}>
                        <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td class="tbl_layout" valign="middle" align="center"><img src="img/hiyari_rp_shitagaki.gif" style="cursor: pointer;" onclick="if(chkShitagakiSubmit()){document.FRM_MAIN.shitagaki.value='clicked';document.FRM_MAIN.submit();};"></td>
                      <{/if}>

                      <{if $gamen_mode != "cate_update"}>
                        <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td class="tbl_layout" valign="middle" align="center"><img src="img/right.gif" height="30" style="cursor: pointer;" onclick="if(chkSubmit()){document.FRM_MAIN.next.value='clicked';document.FRM_MAIN.submit();};"></td>
                      <{/if}>
                    <{/if}>
                      
                    <{if $gamen_mode == "cate_update"}>
                      <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                      <td class="tbl_layout" valign="middle" align="center"><img src="img/hiyari_rp_shitagaki.gif" height="30" style="cursor: pointer;" onclick="if(chkSubmit()){document.FRM_MAIN.report_update.value='clicked';document.FRM_MAIN.submit();};"></td>
                    <{/if}>
                    
                    <{if !$readonly_flg}>
                      <{if $mail_id != "" || $progres_edit_flg}>
                        <td class="tbl_layout" rowspan="2" align="center" width="3">&nbsp</td>
                        <td class="tbl_layout" rowspan="2" align="center" bgcolor="#35B341"><img src="img/spacer.gif" width="1" height="8" alt=""></td>
                      <{/if}>
                    <{/if}>
                    
                    <{if $mail_id != ""}>
                      <{if $mail_id_mode == "recv" && $return_btn_show_flg}>
                        <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td class="tbl_layout" align="center"><img src="img/hiyari_rp_return.gif" width="32" height="29" style="cursor: pointer;" onclick="rp_action_return();"></td>
                      <{/if}>
                      <{if $forward_btn_show_flg}>
                        <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td class="tbl_layout" align="center"><img src="img/hiyari_rp_forward.gif" width="32" height="29" style="cursor: pointer;" onclick="rp_action_forward();"></td>
                      <{/if}>
                      <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                      <td class="tbl_layout" align="center"><img src="img/hiyari_rp_mail_disp.gif" height="30" style="cursor: pointer;" onclick="rp_action_message_disp();"></td>
                    <{/if}>
                    
                    <{if $progres_edit_flg}>
                      <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                      <td class="tbl_layout" align="center"><img src="img/hiyari_rp_progress.gif" width="38" height="30" style="cursor: pointer;" onclick="rp_action_progress();"></td>
                    <{/if}>
                    
                    <{if $gamen_mode == "update" || $gamen_mode == "analysis" || $gamen_mode == "shitagaki"}>
                      <{if $print_btn_show_flg}>
                        <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td class="tbl_layout" align="center"><img src="img/hiyari_rp_print.gif" style="cursor: pointer;" onclick="rp_action_print();"></td>
                      <{/if}>
                    <{/if}>
                  </tr>
                  <tr class="icon">
                    <{if !$readonly_flg}>
                      <{if $gamen_mode == "new" || $gamen_mode == "shitagaki"}>
                        <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td class="tbl_layout" align="center">下書き保存</td>
                      <{/if}>
                      
                      <{if $gamen_mode != "cate_update"}>
                        <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td class="tbl_layout" align="center">内容確認</td>
                      <{/if}>
                    <{/if}>
                      
                    <{if $gamen_mode == "cate_update"}>
                      <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                      <td class="tbl_layout" align="center">更新</td>
                    <{/if}>

                    <{if $mail_id != ""}>
                      <{if $mail_id_mode == "recv" && $return_btn_show_flg}>
                        <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td class="tbl_layout" align="center">返信</td>
                      <{/if}>
                      <{if $forward_btn_show_flg}>
                        <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td class="tbl_layout" align="center">転送</td>
                      <{/if}>
                      <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                      <td class="tbl_layout" align="center">表示</td>
                    <{/if}>
                    
                    <{if $progres_edit_flg}>
                      <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                      <td class="tbl_layout" align="center">進捗登録</td>
                    <{/if}>
                    <{if $gamen_mode == "update" || $gamen_mode == "analysis" || $gamen_mode == "shitagaki"}>
                      <{if $print_btn_show_flg}>
                        <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td class="tbl_layout" align="center">印刷</td>
                      <{/if}>
                    <{/if}>
                  </tr>
                </table>
              </td>
              <!-- 実行アイコン END -->

              <!-- 職種選択 START -->
              <{if !$readonly_flg}>
                <td class="tbl_layout" width="400">
                  <{if $eis_select_able}>
                    <table width="400" class="info">
                      <tr>
                        <td  align="center" bgcolor="#DFFFDC">対象職種・報告書様式</td>
                      </tr>
                      <tr>
                        <td align="center">
                          <nobr>
                            <form name='job_change_form' action='' method='post' onSubmit="return false;">
                              <select name="sel_job_input" onchange="set_eis_input_list();change_job();">
                                <{section loop=$job_ids name=i}>
                                  <option value="<{$job_ids[i]}>"<{if $job_ids[i] == $sel_job}> selected<{/if}>><{$job_names[i]}>
                                <{/section}>
                              </select>

                              <select name="sel_eis_input" onchange="change_job();">
                              </select>
                            </form>
                          </nobr>
                        </td>
                      </tr>
                    </table>
                    <script type="text/javascript">
                      function change_job()
                      {
                        //document.FRM_MAIN.sel_job.value = sel_job_input.value;
                        document.FRM_MAIN.sel_job.value = job_change_form.sel_job_input.options[job_change_form.sel_job_input.selectedIndex].value;
                        document.FRM_MAIN.sel_eis_no.value = job_change_form.sel_eis_input.options[job_change_form.sel_eis_input.selectedIndex].value;
                        document.FRM_MAIN.sel_eis_change.value='changed';
                        document.FRM_MAIN.submit();
                      }

                      function set_eis_input_list()
                      {
                        var job_box = job_change_form.sel_job_input;//document.getElementById("sel_job_input");
                        var eis_box = job_change_form.sel_eis_input;//document.getElementById("sel_eis_input");
                        var job_id = job_box.options[job_box.selectedIndex].value;

                        deleteAllOptions(eis_box);
                        if(false){
                        }
                        <{section loop=$job_ids name=i}>
                          else if(job_id == "<{$job_ids[i]}>") { //デフォルトの様式番号
                            <{section loop=$eis_job_link_info name=ii}>
                              <{if $eis_job_link_info[ii].job_id == $job_ids[i] && $eis_job_link_info[ii].default_flg == "t"}>
                                var default_eis_no = "<{$eis_job_link_info[ii].eis_no}>";
                              <{/if}>
                            <{/section}>
                            //様式をリストに追加
                            <{section loop=$eis_job_link_info name=ii}>
                              <{if $eis_job_link_info[ii].job_id == $job_ids[i]}>
                                addOption(eis_box, "<{$eis_job_link_info[ii].eis_no}>", "<{$eis_job_link_info[ii].eis_name}>", default_eis_no);
                              <{/if}>
                            <{/section}>
                          }
                        <{/section}>
                        else{
                        }
                      }

                      function deleteAllOptions(box)
                      {
                        for (var i = box.length - 1; i >= 0; i--)
                        {
                          box.options[i] = null;
                        }
                      }

                      function addOption(box, value, text, selected)
                      {
                        var opt = document.createElement("option");
                        opt.value = value;
                        opt.text = text;
                        if (selected == value)
                        {
                            opt.selected = true;
                        }
                        box.options[box.length] = opt;
                        try {box.style.fontSize = 'auto';} catch (e) {}
                        box.style.overflow = 'auto';
                      }

                      function set_eis_no(eis_no)
                      {
                        var obj = job_change_form.sel_eis_input;//document.getElementById("sel_eis_input");
                        set_select_ctl_from_value(obj,eis_no);
                      }

                      function set_select_ctl_from_value(obj,val)
                      {
                        var is_ok = false;
                        for(var i=0; i<obj.options.length; i++)
                        {
                          if(obj.options[i].value == val)
                          {
                            obj.selectedIndex = i;
                            is_ok = true;
                            break;
                          }
                        }
                        return is_ok;
                      }
                    </script>
                  <{/if}>
                </td>              
                <!-- 職種選択 END -->
                
                <td class="tbl_layout" width="10">
                    &nbsp
                </td>
                
                <!-- 画面進捗 START -->
                <td class="tbl_layout" width="400">
                  <table width="100%" class="info">
                    <tr>
                      <td align="center" bgcolor="#FFDDFD">出来事報告の流れ</td>
                    </tr>
                    <tr>
                      <td align="center" id="flow">
                        <{if $gamen_mode == "cate_update"}>
                          <nobr>
                            <b><span style="background-color:#35B341;color:#FFFFFF;padding:2px;">
                            1.出来事の記入
                            </span></b>
                            &nbsp;
                            ⇒
                            &nbsp;
                            (更新)
                          </nobr>
                        <{else}>
                          <nobr>
                            <b><span style="background-color:#35B341;color:#FFFFFF;padding:2px;">
                            1.出来事の記入
                            </span></b>
                            &nbsp;
                            ⇒
                            &nbsp;
                            2.記入内容を確認
                            &nbsp;
                            ⇒
                            &nbsp;
                            (送信)
                          </nobr>
                        <{/if}>
                      </td>
                    </tr>
                  </table>
                </td>
                <td class="tbl_layout" width="10">
                    &nbsp
                </td>
                <!-- 画面進捗 END -->
              <{/if}>                
            </tr>
          </table>
          <!-- 上部 END -->

          <form name='FRM_MAIN' action='hiyari_easyinput.php?callerpage=hiyari_easyinput.php' method='post' onSubmit="return false;" >
            <{* 確認画面で様式を取得するためにデータを送る START *}>
            <input type="hidden" name="eis_title_id" value="<{$eis_title_id}>">
            <{* 確認画面で様式を取得するためにデータを送る END *}>

            <!-- ポストバック情報 -->
            <input type='hidden' name='is_postback'         id='is_postback'         value='ture'>
            <input type='hidden' name='next'                id='next'                value=''>
            <input type='hidden' name='shitagaki'           id='shitagaki'           value=''>
            <input type='hidden' name='report_update'       id='report_update'       value=''>
            <input type='hidden' name='sel_eis_change'      id='sel_job_change'      value=''>
            <input type='hidden' name='registrant_post_change' id='registrant_post_change' value=''>
			<input type='hidden' name='subject_mode'            id='subject_mode'        value='<{$subject_mode}>'>
<input type='hidden' name='callerpage'            id='callerpage'        value='<{$callerpage}>'>
                        
            <!-- セッション情報 -->
            <input type='hidden' name='session'             id='session'             value='<{$session}>'>
            <input type='hidden' name='hyr_sid'             id='hyr_sid'             value='<{$hyr_sid}>'>

            <!-- 画面パラメータ -->
            <input type='hidden' name='gamen_mode'          id='gamen_mode'          value='<{$gamen_mode}>'>
            <input type='hidden' name='report_id'           id='report_id'           value='<{$report_id}>'>
            <input type='hidden' name='mail_id'             id='mail_id'             value='<{$mail_id}>'>
            <input type='hidden' name='mail_id_mode'        id='mail_id_mode'        value='<{$mail_id_mode}>'>
            <input type='hidden' name='cate'                id='cate'                value='<{$cate}>'>

            <!-- 職種／様式 (JavaScriptにより動的変更／ポストバック用) -->
            <input type='hidden' name='sel_job'             id='sel_job'             value='<{$sel_job}>'>
            <input type='hidden' name='sel_eis_no'          id='sel_eis_no'          value='<{$sel_eis_no}>'>

            <!-- 職種／様式 (JavaScriptによる変更なし／確認画面送信用) -->
            <input type='hidden' name='job_id'              id='job_id'              value='<{$sel_job}>'>
            <input type='hidden' name='eis_no'              id='eis_no'              value='<{$sel_eis_no}>'>

            <!-- 様式スタイル -->
            <input type='hidden' name='style_code'          id='style_code'         value='<{$style_code}>'>

            <!-- 主治医 -->
            <input type='hidden' name='doctor_emp_id'       id='doctor_emp_id'       value='<{$doctor_emp_id}>'>

            <!-- 特殊様式情報 -->
            <input type="hidden" name="patient_use" value="<{$patient_use_csv}>">
            <input type="hidden" name="eis_400_use" value="<{$eis_400_use_csv}>">
            <input type="hidden" name="eis_410_use" value="<{$eis_410_use_csv}>">
            <input type="hidden" name="eis_420_use" value="<{$eis_420_use_csv}>">
            <input type="hidden" name="eis_430_use" value="<{$eis_430_use_csv}>">
            <input type="hidden" name="eis_440_use" value="<{$eis_440_use_csv}>">
            <input type="hidden" name="eis_450_use" value="<{$eis_450_use_csv}>">
            <input type="hidden" name="eis_460_use" value="<{$eis_460_use_csv}>">
            <input type="hidden" name="eis_470_use" value="<{$eis_470_use_csv}>">
            <input type="hidden" name="eis_480_use" value="<{$eis_480_use_csv}>">
            <input type="hidden" name="eis_490_use" value="<{$eis_490_use_csv}>">

            <!-- タイムラグ -->
            <input type="hidden" name="first_send_time" value="<{$first_send_time}>">
            <input type="hidden" id="_105_75" name="_105_75" value="<{$vals[105][75][0]}>">

            <!-- 所属情報 -->
            <img src="img/spacer.gif" width="1" height="5" alt=""><br>
            <table width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="tbl_layout" align="left">
                  <table cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="tbl_layout" width="10">
                        <img src="img/spacer.gif" width="1" height="5" alt=""><br>
                      </td>
                      <td class="tbl_layout">                  
                        <!-- タイムラグ -->
                        <{if $is_sm_emp_flg == true 
                         && $vals[105][75][0] != '' 
                         && $disp_taimelag_flg == true
                         && $gamen_mode != 'cate_update'
                         && $gamen_mode != 'shitagaki'
                         && $first_send_time != ''
                        }>
                          <table cellspacing="0" cellpadding="2" class="info">
                            <tr height="22">
                              <td bgcolor="#DFFFDC">
                                <nobr>
                                第一報送信時刻
                                </nobr>
                              </td>
                                
                              <td>
                                <nobr>
                                <{$first_send_time}>
                                </nobr>
                              </td>
                              
                              <td bgcolor="#DFFFDC">
                                <nobr>
                                発見時刻から第一報送信時刻までの時間
                                </nobr>
                              </td>
                              
                              <td>
                                <nobr>
                                <{$timelag_view}>
                                </nobr>
                              </td>							
                            </tr>
                          </table>
                        <{/if}>
                      </td>
                    </tr>
                  </table>
                </td>
                <td class="tbl_layout" align="right">
                  <table cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="tbl_layout">
                      <{if $report_no != ""}>
                        <table cellspacing="0" cellpadding="2" class="info">
                          <tr height="22">
                            <td bgcolor="#DFFFDC">
                              <nobr>
                              事案番号
                              </nobr>
                            </td>
                            <td>
                              <nobr>
                                <{$report_no}>
                              </nobr>
                            </td>
                          </tr>
                        </table>
                      <{/if}>
                      </td>
                      <td class="tbl_layout" width="10">
                      <img src="img/spacer.gif" width="1" height="5" alt=""><br>
                      </td>
                      <td class="tbl_layout">
                        <table border="0" cellspacing="0" cellpadding="2" class="info">
                          <tr height="22">
                            <td bgcolor="#DFFFDC">
                              <nobr>
                                <{if !$readonly_flg}>
                                  <a href="javascript:show_registrant_post_edit();">
                                <{/if}>
                                    報告部署
                                <{if !$readonly_flg}>
                                  </a>
                                <{/if}>
                              </nobr>
                            </td>
                            <td>
                              <nobr>
                                <span id="registrant_class_name"><{$class_name}></span>
                                <input type="hidden" id="registrant_class" name="registrant_class" value="<{$registrant_class}>">

                                <span id="registrant_attribute_name">
                                  <{if isset($attribute_name)}>
                                    &nbsp;&gt;&nbsp;<{$attribute_name}>
                                  <{/if}>
                                </span>
                                <input type="hidden" id="registrant_attribute" name="registrant_attribute" value="<{$registrant_attribute}>">

                                <span id="registrant_dept_name">
                                  <{if isset($dept_name)}>
                                    &nbsp;&gt;&nbsp;<{$dept_name}>
                                  <{/if}>
                                </span>
                                <input type="hidden" id="registrant_dept" name="registrant_dept" value="<{$registrant_dept}>">

                                <span id="registrant_room_name">
                                  <{if isset($room_name)}>
                                    &nbsp;&gt;&nbsp;<{$room_name}>
                                  <{/if}>
                                </span>
                                <input type="hidden" id="registrant_room" name="registrant_room" value="<{$registrant_room}>">
                              </nobr>
                            </td>
                          </tr>
                        </table>
                        <script type="text/javascript">
                          //登録部署変更画面を呼び出します。
                          function show_registrant_post_edit()
                          {
                            var class_id_hidden = document.getElementById("registrant_class");
                            var attribute_id_hidden = document.getElementById("registrant_attribute");
                            var dept_id_hidden = document.getElementById("registrant_dept");
                            var room_id_hidden = document.getElementById("registrant_room");

                            var class_id = class_id_hidden.value;
                            var attribute_id = attribute_id_hidden.value;
                            var dept_id = dept_id_hidden.value;
                            var room_id = room_id_hidden.value;

                            var url = "hiyari_registrant_post_edit.php?session=<{$session}>&class_id="+class_id+"&attribute_id="+attribute_id+"&dept_id="+dept_id+"&room_id="+room_id;
                            window.open(url, "newwin", "width=640,height=480,scrollbars=yes");
                          }

                          //登録部署変更画面からの戻りを反映します。
                          function callback_registrant_post_edit(class_id,attribute_id,dept_id,room_id,class_name,attribute_name,dept_name,room_name)
                          {
                            var class_name_span = document.getElementById("registrant_class_name");
                            var attribute_name_span = document.getElementById("registrant_attribute_name");
                            var dept_name_span = document.getElementById("registrant_dept_name");
                            var room_name_span = document.getElementById("registrant_room_name");
                            var class_id_hidden = document.getElementById("registrant_class");
                            var attribute_id_hidden = document.getElementById("registrant_attribute");
                            var dept_id_hidden = document.getElementById("registrant_dept");
                            var room_id_hidden = document.getElementById("registrant_room");

                            var sep = "&gt;";
                            if(attribute_name != "")
                            {
                              attribute_name = sep + attribute_name;
                            }
                            if(dept_name != "")
                            {
                              dept_name = sep + dept_name;
                            }
                            if(room_name != "")
                            {
                              room_name = sep + room_name;
                            }
                            if(class_name == "")
                            {
                              class_name = "不明";
                            }

                            class_name_span.innerHTML = class_name;
                            attribute_name_span.innerHTML = attribute_name;
                            dept_name_span.innerHTML = dept_name;
                            room_name_span.innerHTML = room_name;
                            class_id_hidden.value = class_id;
                            attribute_id_hidden.value = attribute_id;
                            dept_id_hidden.value = dept_id;
                            room_id_hidden.value = room_id;

                            //報告部署による項目非表示設定を有効にするためポストバックする。
                            document.FRM_MAIN.registrant_post_change.value='changed';
                            document.FRM_MAIN.submit();

                          }
                        </script>
                      </td>
                      <td class="tbl_layout" width="10">
                          &nbsp
                      </td>                      
                    <tr>
                  </table>
                </td>
              </tr>
            </table>
            <img src="img/spacer.gif" width="1" height="5" alt=""><br>
            <{include file="hiyari_edit_easyinput2.tpl" input_mode=$gamen_mode}>
            <br>
          </form>

<!-- ファイル添付 START 	//20130630-->
            <{if  $file_attach == "1"}>
            <br>
				<form method="post" enctype="multipart/form-data" action="hiyari_file_upload.php">

				<{if !$readonly_flg}>

					<script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
					<script type="text/javascript" src="js/jquery/jquery-upload-1.0.2.min.js"></script>
					<script>
					jQuery(function($){
					 
						$(document).on('click', '.delF', function(){
							//alert(this.getAttribute("name"));
							$.ajax({
								context: self,
								url: 'hiyari_file_upload.php',
								type: 'post',
								data: {
									func_id: this.getAttribute("name"),
									StateTrns: 'delFile',
									Ids: '<{$report_id}>',
									fuld_session: '<{$session}>'
								},
								success:function(resDel){
									//alert('success');
									resDel = resDel.replace(/\r\n/g, "");
									var atchList = $('#atchList');
									var wwwTmp = $('#'+resDel);
									wwwTmp.remove();
								}
							});
						});

						$(document).on('click', '.delFR', function(){
							if(!confirm("ファイルが完全削除されます。よろしいですか？"))
							{
								return false;
							}
							
							$.ajax({
								context: self,
								url: 'hiyari_file_upload.php',
								type: 'post',
								data: {
									func_id: this.getAttribute("name"),
									StateTrns: 'delFileReg',
									Ids: '<{$report_id}>',
									fuld_session: '<{$session}>'
								},
								success:function(resDel){
									//alert('success');
									resDel = resDel.replace(/\r\n/g, "");
									var atchList = $('#atchList');
									var wwwTmp = $('#'+resDel);
									wwwTmp.remove();
								}
							});
						});
					 


					 
						$('#img').change(function(){
							//アップロード、表示
							var postD = $("form").serialize();
							
							//ボタン非表示
							$(this).hide();
							$(this).upload('hiyari_file_upload.php',
								postD,
								function(resHtml){
									var atchList = $('#atchList');
									atchList.html(resHtml);

									//ボタン再表示
									$(this).show();
									
								},'html'
							);
						});
					});




					
					</script>
            
					<table width="100%">
						<tr>
							<td class="tbl_layout" width="10">&nbsp;</td>
							<td class="tbl_layout">
								<table class="frame">  
									<tr>
										<td class="frame_topleft">&nbsp;</td>
										<td class="frame_topmiddle">&nbsp;</td>
										<td class="frame_topright">&nbsp;</td>
									</tr>
									<tr>
										<td class="frame_left">&nbsp;</td>
										<td class="data">
											<table>
												<tr>
													<th>ファイル添付</th>
													<td>
														<input type='hidden' name='fuld_session' id='fuld_session' value='<{$session}>'>
														<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※<{$file_max_size}>まで</font>
														<input type="file" size="100" value="" id="img" name="img">
														<br><br>
														
														<div id="atchList">
															<{foreach from=$arr_file key=tmp_hy_key item=tmp_hy_file}>
																<div id="<{$tmp_hy_key}>"><input type="button" name="<{$tmp_hy_key}>" class="delF" value="削除"/> <{$tmp_hy_file}></div>
															<{/foreach}>
														</div>
													</td>
												</tr>
											</table>
										</td>
										<td class="frame_right">&nbsp;</td>
									</tr>
									<tr>
										<td class="frame_bottomleft">&nbsp;</td>
										<td class="frame_bottommiddle">&nbsp;</td>
										<td class="frame_bottomright">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<br>
				<{/if}>
				<{if $file_list|@count > 0}>
		
					<table width="100%" >
						<tr>
							<td class="tbl_layout" width="10">&nbsp;</td>
							<td class="tbl_layout">
								<table class="frame">  
									<tr>
										<td class="frame_topleft">&nbsp;</td>
										<td class="frame_topmiddle">&nbsp;</td>
										<td class="frame_topright">&nbsp;</td>
									</tr>
									<tr>
										<td class="frame_left">&nbsp;</td>
										<td class="data">
											<table>
												<tr>
													<td VALIGN="bottom">
														<div id="hyr_preview">
														
															<{foreach from=$file_list item=tmp_hy_file}>
																<div  style="float: left; border : 10px  solid white" id="<{$tmp_hy_file.file_no}>">
																	<a href="<{$tmp_hy_file.view}>=<{$session}>&r=<{$report_id}>&u=<{$tmp_hy_file.file_no}>" <{$tmp_hy_file.target}> >
																		<img border=0 src="<{$tmp_hy_file.base}>" title="<{$tmp_hy_file.file_name}>"><br><{$tmp_hy_file.file_name}>
																	</a>
																		<{if !$readonly_flg}>
																			<input type="button" name="<{$tmp_hy_file.file_no}>" class="delFR" value="削除"/>
																		<{/if}>
																	
																</div>
																
															<{/foreach}>
														
														</div>
													</td>
												</tr>
											</table>
										</td>
										<td class="frame_right">&nbsp;</td>
									</tr>
									<tr>
									  <td class="frame_bottomleft">&nbsp;</td>
									  <td class="frame_bottommiddle">&nbsp;</td>
									  <td class="frame_bottomright">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				<{/if}>
	
					
				</form>
            <{/if}>

<!-- ファイル添付 END 	//20130630-->


          <!-- iwata患者情報取得フォームSTART -->
          <form name='iwata_patient_search_form' action='' method='post'>
            <input type="hidden" id="caller"         name="caller">
            <input type="hidden" id="target_date"    name="target_date">
            <input type="hidden" id="patient_id"     name="patient_id">
            <input type="hidden" id="patient_name"   name="patient_name">
            <input type="hidden" id="page"           name="page">
            <input type="hidden" id="callbacker_url" name="callbacker_url">
          </form>
          <!-- iwata患者情報取得フォームEND -->
          
          <!-- 患者情報取得汎用フォームSTART -->
          <form name='patient_search_form' action='' method='post'>
            <input type="hidden" id="caller"         name="caller">
            <input type="hidden" id="target_date"    name="target_date">
            <input type="hidden" id="patient_id"     name="patient_id">
            <input type="hidden" id="patient_name"   name="patient_name">
            <input type="hidden" id="page"           name="page">
            <input type="hidden" id="callbacker_url" name="callbacker_url">
          </form>
          <!-- 患者情報取得汎用フォームEND -->

          <!-- 事例の詳細（時系列）の事象用フォームSTART -->
          <form name='phenomenon_form' action='hiyari_easyinput_phenomenon.php' method='post'>
            <input type="hidden" name="session">
            <input type="hidden" name="grp_code">
            <input type="hidden" name="phenomenon">
          </form>
          <!-- 事例の詳細（時系列）の事象用フォームEND -->
        </td>
      </tr>
    </table>

    <{if $gamen_mode != "analysis"}>
      <table cellspacing="0" cellpadding="0">
        <tr>
          <td  class="tbl_layout">
            &nbsp<a href="#pagetop">このページの先頭へ</a><br>
          </td>
          <td  class="tbl_layout">
          <{if !$readonly_flg}>
            <!-- 実行アイコン２ START -->
            <table cellspacing="0" cellpadding="0">
              <tr>
                <{if $gamen_mode != "cate_update"}>
                  <td  class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                  <td  class="tbl_layout" valign="middle" align="center"><img src="img/right.gif" height="30" style="cursor: pointer;" onclick="if(chkSubmit()){document.FRM_MAIN.next.value='clicked';document.FRM_MAIN.submit();};"></td>
                <{/if}>
              </tr>
              <tr class="icon">
                <{if $gamen_mode != "cate_update"}>
                  <td class="tbl_layout"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                  <td class="tbl_layout" align="center">内容確認</td>
                <{/if}>
              </tr>
            </table>
            <!-- 実行アイコン２ END -->
          <{/if}>
          </td>
        </tr>
      </table>
    <{/if}>
    <br>
  </div>
</body>
</html>
