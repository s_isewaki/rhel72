<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/hiyari_el.js"></script>
  <script type="text/javascript">
    //--------------------------------------------------
    //登録対象者配列
    //--------------------------------------------------
    m_target_list = new Array();
    //参照権限の対象職員
    m_target_list[1] = new Array(
        <{foreach name=n from=$arr_target[1] item=i}>
          <{if !$smarty.foreach.n.first}>,<{/if}>
          new user_info('<{$i.id}>','<{$i.name}>')
        <{/foreach}>
      );
    //更新権限の対象職員
    m_target_list[2] = new Array(
        <{foreach name=n from=$arr_target[2] item=i}>
          <{if !$smarty.foreach.n.first}>,<{/if}>
          new user_info('<{$i.id}>','<{$i.name}>')
        <{/foreach}>
      );

    //--------------------------------------------------
    //部署情報配列
    //--------------------------------------------------
    //部
    var classes = [];
    <{foreach from=$sel_class item=i}>
      classes.push({id: '<{$i.class_id}>', name: '<{$i.class_nm}>'});
    <{/foreach}>

    //課
    var atrbs = {};
    <{foreach from=$sel_atrb key=class_id item=c}>
      atrbs['<{$class_id}>'] = [];
      <{foreach from=$c key=atrb_id item=a}>
        atrbs['<{$class_id}>'].push({id: '<{$atrb_id}>', name: '<{$a}>'});
      <{/foreach}>
    <{/foreach}>

    //科
    var depts = {};
    <{foreach from=$sel_dept key=class_id item=c}>
      depts['<{$class_id}>'] = {};
      <{foreach from=$c key=atrb_id item=a}>
        depts['<{$class_id}>']['<{$atrb_id}>'] = [];
        <{foreach from=$a key=dept_id item=d}>
          depts['<{$class_id}>']['<{$atrb_id}>'].push({id: '<{$dept_id}>', name: '<{$d}>'});
        <{/foreach}>
      <{/foreach}>
    <{/foreach}>

    //--------------------------------------------------
    //画面起動時の処理
    //--------------------------------------------------
    function initPage()
    {
        //登録対象者を設定する。
        update_target_html("1");
        update_target_html("2");

        //登録ファイル数に対する入力項目の制御
        setFileElementsDisplay1();

        //権限部分の初期制御
        onChangeArchive(true, '<{$ref_class_src}>', '<{$ref_atrb_src}>', '<{$upd_class_src}>', '<{$upd_atrb_src}>');
    }

    //----------------------------------------------------------------------------------------------------
    // 権限関係以外の画面処理
    //----------------------------------------------------------------------------------------------------
    //ファイルパスを解析し、コンテンツ名・コンテンツタイプをセットします。
    function setFileInfo(no, path)
    {
        //コンテンツ名にセット
        var fname;
        if (path.indexOf('\\') != -1)
        {
            fname = path.replace(/^.*\\/, '');
        }
        else if (path.indexOf('/') != -1)
        {
            fname = path.replace(/^.*\//, '');
        }
        else
        {
            fname = path;
        }
        var docname = fname.replace(/\.[^.]*$/, '');
        document.mainform.elements['document_name'.concat(no)].value = docname;

        //コンテンツタイプにセット
        var ext = fname.replace(/.*\./, '');
        switch (ext)
        {
            case 'doc':
            case 'docx':
                document.mainform.elements['document_type'.concat(no)].options[1].selected = true;
                break;
            case 'xls':
            case 'xlsx':
                document.mainform.elements['document_type'.concat(no)].options[2].selected = true;
                break;
            case 'ppt':
            case 'pptx':
                document.mainform.elements['document_type'.concat(no)].options[3].selected = true;
                break;
            case 'pdf':
                document.mainform.elements['document_type'.concat(no)].options[4].selected = true;
                break;
            case 'txt':
                document.mainform.elements['document_type'.concat(no)].options[5].selected = true;
                break;
            case 'jpg':
            case 'jpeg':
                document.mainform.elements['document_type'.concat(no)].options[6].selected = true;
                break;
            case 'gif':
                document.mainform.elements['document_type'.concat(no)].options[7].selected = true;
                break;
            default:
                document.mainform.elements['document_type'.concat(no)].options[8].selected = true;
                break;
        }
    }

    //登録ファイル数に対する入力項目の制御(旧デザイン用)
    function setFileElementsDisplay1()
    {
        var file_count = document.mainform.file_count.value;

        for (var i = 1; i <= 5; i++)
        {
            var border_width = (file_count > 1) ? '2px' : '1px';

            var top_r_id = 'file'.concat(i).concat('_r1');
            var cells = document.getElementById(top_r_id).cells;
            for (var c = 0; c <= 1; c++)
            {
                cells.item(c).style.borderTopWidth = border_width;
            }

            var bottom_r_id = 'file'.concat(i).concat('_r4');
            var cells = document.getElementById(bottom_r_id).cells;
            for (var c = 0; c <= 1; c++)
            {
                cells.item(c).style.borderBottomWidth = border_width;
            }

            var display = (i <= file_count) ? '' : 'none';
            for (var r = 1; r <= 4; r++)
            {
                var r_id = 'file'.concat(i).concat('_r').concat(r);
                document.getElementById(r_id).style.display = display;
            }
        }
    }
  </script>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
  <script type="text/javascript" src="js/yui/build/connection/connection-min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <style type="text/css">
    .list {border-collapse:collapse; border:#5279a5 solid 1px;}
    .list td {border:#5279a5 solid 1px;}
    .list td td {border-width:0;}
    .non_in_list {border-collapse:collapse;}
    .non_in_list td {border:0px;}
  </style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <!-- ヘッダー START -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr height="32" bgcolor="#35B341">
            <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><{$PAGE_TITLE}></b></font></td>
            <td>&nbsp;</td>
            <td width="10">&nbsp;</td>
            <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
          </tr>
        </table>
        <img src="img/spacer.gif" width="10" height="10" alt=""><br>
        <!-- ヘッダー END -->
      </td>
    </tr>
    <tr>
      <td>
        <!-- 本体 START -->
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
            <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
          </tr>
          <tr>
            <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
            <td bgcolor="#F5FFE5">
              <form name="mainform" action="hiyari_el_register_exe.php" method="post" enctype="multipart/form-data">
                <!-- 上部ボタン START -->
                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                  <tr height="22">
                    <td style="vertical-align:bottom;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10" color="red">※<{$upload_max_filesize}>を超えるファイルは登録できません。</font></td>
                    <td align="right"><input type="button" value="登録" onclick="submitForm();"></td>
                  </tr>
                </table>
                <!-- 上部ボタン END -->

                <!-- 表全体 START -->
                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                  <!-- 登録ファイル数 START -->
                  <tr height="22">
                    <td align="right" bgcolor="#DFFFDC">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録ファイル数</font>
                    </td>
                    <td colspan="3" bgcolor="#FFFFFF">
                      <select name="file_count" onchange="setFileElementsDisplay1();">
                        <{foreach from=$contents key=k item=i}>
                          <option value="<{$k}>" <{if $file_count == $k}>selected<{/if}>><{$k}>
                        <{/foreach}>
                      </select>
                    </td>
                  </tr>
                  <!-- 登録ファイル数 END -->

                  <!-- ファイル名-説明x5 START -->
                  <{foreach from=$contents key=k item=i}>
                    <!-- ファイル名 START -->
                    <tr id="file<{$k}>_r1" height="22" style="display:none;">
                      <td align="right" bgcolor="#DFFFDC">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイル</font>
                      </td>
                      <td colspan="3" bgcolor="#FFFFFF">
                        <input type="file" name="upfile<{$k}>" size="50" value="" style="ime-mode:inactive;" onchange="setFileInfo(<{$k}>, this.value);">
                      </td>
                    </tr>
                    <!-- ファイル名 END -->

                    <!-- コンテンツ名-コンテンツタイプ START -->
                    <tr id="file<{$k}>_r2" height="22" style="display:none;">
                      <td width="15%" align="right" bgcolor="#DFFFDC">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コンテンツ名</font>
                      </td>
                      <td bgcolor="#FFFFFF">
                        <input name="document_name<{$k}>" type="text" size="50" maxlength="100" value="<{$i.document_name}>" style="ime-mode:active;">
                      </td>
                      <td width="14%" align="right" bgcolor="#DFFFDC">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コンテンツタイプ</font>
                      </td>
                      <td width="17%" bgcolor="#FFFFFF">
                        <select name="document_type<{$k}>">
                          <option value="0"<{if $i.document_type_var == "0"}> selected<{/if}>>
                          <option value="1"<{if $i.document_type_var == "1"}> selected<{/if}>>Word
                          <option value="2"<{if $i.document_type_var == "2"}> selected<{/if}>>Excel
                          <option value="3"<{if $i.document_type_var == "3"}> selected<{/if}>>PowerPoint
                          <option value="4"<{if $i.document_type_var == "4"}> selected<{/if}>>PDF
                          <option value="5"<{if $i.document_type_var == "5"}> selected<{/if}>>テキスト
                          <option value="6"<{if $i.document_type_var == "6"}> selected<{/if}>>JPEG
                          <option value="7"<{if $i.document_type_var == "7"}> selected<{/if}>>GIF
                          <option value="99"<{if $i.document_type_var == "99"}> selected<{/if}>>その他
                        </select>
                      </td>
                    </tr>
                    <!-- コンテンツ名-コンテンツタイプ START -->

                    <!-- キーワード-コンテンツ番号 START -->
                    <tr id="file<{$k}>_r3" height="22" style="display:none;">
                      <td align="right" bgcolor="#DFFFDC">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">キーワード</font>
                      </td>
                      <td bgcolor="#FFFFFF">
                        <input name="keywd<{$k}>" type="text" size="50" maxlength="50" value="<{$i.keyword}>" style="ime-mode:active;">
                      </td>
                      <td align="right" bgcolor="#DFFFDC">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コンテンツ番号</font>
                      </td>
                      <td bgcolor="#FFFFFF">
                        <input type="text" name="lib_no<{$k}>" value="<{$i.lib_no}>" style="ime-mode:inactive;">
                      </td>
                    </tr>
                    <!-- キーワード-コンテンツ番号 END -->

                    <!-- 説明 START -->
                    <tr id="file<{$k}>_r4" style="display:none;">
                      <td align="right" bgcolor="#DFFFDC" >
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">説明</font>
                      </td>
                      <td colspan="3" bgcolor="#FFFFFF">
                        <textarea name="explain<{$k}>" rows="5" cols="40" style="ime-mode:active;"><{$i.explain}></textarea>
                      </td>
                    </tr>
                    <!-- 説明 END -->
                  <{/foreach}>
                  <!-- ファイル名-説明x5 END -->

                  <!-- 保存先 START -->
                  <tr height="22">
                    <td align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">保存先</font></td>
                    <td colspan="3" bgcolor="#FFFFFF">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                        <span id="folder_path" style="padding-right:2px;">
                          <{if $category == ""}>
                            選択してください
                          <{else}>
                            <{$cate_nm}>
                            <{foreach name=n from=$folder_path item=fp}>
                              &gt;&nbsp;<{$fp.name}>
                            <{/foreach}>
                          <{/if}>
                        </span>
                        <input type="button" value="選択" onclick="selectFolder('<{$session}>', '<{$path}>');">
                        <input type="hidden" name="archive" value="<{$archive}>">
                        <input type="hidden" name="category" value="<{$category}>">
                        <input type="hidden" name="folder_id" value="<{$folder_id}>">
                      </font>
                    </td>
                  </tr>
                  <!-- 保存先 END -->

                  <!-- 参照・更新可能職員指定 -->
                  <{include file="hiyari_el_emp1.tpl"}>
                </table>
                <!-- 表全体 END -->

                <!-- 下部ボタン START -->
                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                  <tr height="22">
                    <td align="right"><input type="button" value="登録" onclick="submitForm();"></td>
                  </tr>
                </table>
                <!-- 下部ボタン END -->

                <input type="hidden" name="session" value="<{$session}>">
                <input type="hidden" name="o" value="<{$o}>">
                <input type="hidden" name="path" value="<{$path}>">
                <input type="hidden" name="ref_toggle_mode" value="">
                <input type="hidden" name="upd_toggle_mode" value="">
                <input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
                <input type="hidden" id="target_name_list1" name="target_name_list1" value="">
                <input type="hidden" id="target_id_list2"   name="target_id_list2" value="">
                <input type="hidden" id="target_name_list2" name="target_name_list2" value="">
              </form>
            </td>
            <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
          </tr>
          <tr>
            <td><img src="img/r_3.gif" width="10" height="10"></td>
            <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td><img src="img/r_4.gif" width="10" height="10"></td>
          </tr>
        </table>
        <!-- 本体 END -->
      </td>
    </tr>
  </table>
</body>
</html>
