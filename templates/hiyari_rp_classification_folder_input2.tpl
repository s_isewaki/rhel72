<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/pagination.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
  <script type="text/javascript">
    var w = 1024 + 37;
    var h = window.screen.availHeight;
    window.resizeTo(w, h);

    jQuery(function($){
        //------------------------------
        //チェックボックス
        //------------------------------
        //初期表示
        $('.checkbox:checked').each(function(){
            set_checkbox_look($(this));
        });
        
        //クリック時
        $('.checkbox').click(function(){
            set_checkbox_look($(this));
        });	    
    })

    //チェックボックスの見た目を設定
    function set_checkbox_look(cb)
    {
        var wrap = cb.closest('.checkbox_wrap')
        var parent = wrap.closest('.item_set');
        
        if (cb.prop('checked')){
          wrap.css('background-position', '0px -28px');
          parent.css('background-color', '#FFF9D4');
        }
        else{
          wrap.css('background-position', '0px 0px');
          parent.css('background-color', '#FFF');
        }
    }
    
    function init_page()
    {
        //部署
        set_post_select_box_obj_key("search_emp_post","search_emp_class","search_emp_attribute","search_emp_dept","search_emp_room");
        setClassOptions("search_emp_post",'<{$class_id}>','<{$attribute_id}>','<{$dept_id}>','<{$room_id}>');
        
        //患者年齢
        if(document.getElementById('patient_year_from').value == '900'){
            document.getElementById('patient_year_to').disabled = true;
        }
        
        //概要・場面・内容(2010年改訂)
        disp_control_900_1000();
        
        set_disabled_enabled();
    }


    //患者年齢
    function check_patient_year(div)
    {
        if(div == 'from'){
            var year_from = document.getElementById('patient_year_from').value;
            if(year_from == '900'){
                document.getElementById('patient_year_to').value = '';
                document.getElementById('patient_year_to').disabled = true;
            }
            else{
                document.getElementById('patient_year_to').disabled = false;
            }
        }
        else if(div == 'to'){
            var year_to = document.getElementById('patient_year_to').value;
            if(year_to == '900'){
                document.getElementById('patient_year_from').value = '900';
                document.getElementById('patient_year_to').value = '';
                document.getElementById('patient_year_to').disabled = true;
            }
            else{
                document.getElementById('patient_year_to').disabled = false;
            }
        }
    }

    function set_disabled_enabled()
    {
        var inci_level_regist_flg = false;
        for(i=0; i<document.mainform.inci_level_regist_flg.length;i++) {
            if(document.mainform.inci_level_regist_flg[i].type == "checkbox") {
                inci_level_regist_flg = document.mainform.inci_level_regist_flg[i].checked;
            }
        }

        var happened_cause_regist_flg = false;
        for(i=0; i<document.mainform.happened_cause_regist_flg.length;i++) {
            if(document.mainform.happened_cause_regist_flg[i].type == "checkbox") {
                happened_cause_regist_flg = document.mainform.happened_cause_regist_flg[i].checked;
            }
        }

        var inci_background_regist_flg = false;
        for(i=0; i<document.mainform.inci_background_regist_flg.length;i++) {
            if(document.mainform.inci_background_regist_flg[i].type == "checkbox") {
                inci_background_regist_flg = document.mainform.inci_background_regist_flg[i].checked;
            }
        }

        var improve_plan_regist_flg = false;
        for(i=0; i<document.mainform.improve_plan_regist_flg.length;i++) {
            if(document.mainform.improve_plan_regist_flg[i].type == "checkbox") {
                improve_plan_regist_flg = document.mainform.improve_plan_regist_flg[i].checked;
            }
        }

        var effect_confirm_regist_flg = false;
        for(i=0; i<document.mainform.effect_confirm_regist_flg.length;i++) {
            if(document.mainform.effect_confirm_regist_flg[i].type == "checkbox") {
                effect_confirm_regist_flg = document.mainform.effect_confirm_regist_flg[i].checked;
            }
        }

        var warning_case_regist_flg = false;
        for(i=0; i<document.mainform.warning_case_regist_flg.length;i++) {
            if(document.mainform.warning_case_regist_flg[i].type == "checkbox") {
                warning_case_regist_flg = document.mainform.warning_case_regist_flg[i].checked;
            }
        }

        var analysis_target_flg = false;
        for(i=0; i<document.mainform.analysis_target_flg.length;i++) {
            if(document.mainform.analysis_target_flg[i].type == "checkbox") {
                analysis_target_flg = document.mainform.analysis_target_flg[i].checked;
            }
        }

        var title_keyword               = document.mainform.title_keyword.value;
        var incident_contents_keyword   = document.mainform.incident_contents_keyword.value;
        var search_emp_class            = document.mainform.search_emp_class.value;
        var search_emp_name             = document.mainform.search_emp_name.value;
        var search_emp_job              = document.mainform.search_emp_job.value;
        var patient_id                  = document.mainform.patient_id.value;
        var patient_year_from           = document.mainform.patient_year_from.value;
        var patient_year_to             = document.mainform.patient_year_to.value;
        var patient_name                = document.mainform.patient_name.value;
        var evaluation_date             = document.mainform.evaluation_date.value;

        var use_level_flg = false;
        var use_level_obj_list = document.getElementsByName("use_level[]");
        for(i=0;i<use_level_obj_list.length;i++){
            if(use_level_obj_list[i].checked){
                use_level_flg = true;
                break;
            }
        }

        var hiyari_summary_flg = false;
        var hiyari_summary_obj_list = document.getElementsByName("hiyari_summary[]");
        for(i=0;i<hiyari_summary_obj_list.length;i++){
            if(hiyari_summary_obj_list[i].checked){
                hiyari_summary_flg = true;
                break;
            }
        }

        var use_900_flg = false;
        var use_900_obj_list = document.getElementsByName("use_900[]");
        for(i=0;i<use_900_obj_list.length;i++){
            if(use_900_obj_list[i].checked){
                use_900_flg = true;
                break;
            }
        }


        var input_cnt = 0;

        if(inci_level_regist_flg == true) {
            input_cnt++;
        }
        if(happened_cause_regist_flg == true) {
            input_cnt++;
        }
        if(inci_background_regist_flg == true) {
            input_cnt++;
        }
        if(improve_plan_regist_flg == true) {
            input_cnt++;
        }
        if(effect_confirm_regist_flg == true) {
            input_cnt++;
        }
        if(warning_case_regist_flg == true) {
            input_cnt++;
        }
        if(analysis_target_flg == true) {
            input_cnt++;
        }
        if(analysis_target_flg == true) {
            input_cnt++;
        }

        if(title_keyword != "") {
            input_cnt++;
        }
        if(incident_contents_keyword != "") {
            input_cnt++;
        }
        if(search_emp_class != "") {
            input_cnt++;
        }
        if(search_emp_name != "") {
            input_cnt++;
        }
        if(search_emp_job != "") {
            input_cnt++;
        }
        if(patient_id != "") {
            input_cnt++;
        }
        if(patient_year_from != "" || patient_year_to != "") {
            input_cnt++;
        }
        if(patient_name != "") {
            input_cnt++;
        }
        if(evaluation_date != "") {
            input_cnt++;
        }
        if(use_level_flg){
            input_cnt++;
        }
        if(hiyari_summary_flg){
            input_cnt++;
        }
        if(use_900_flg){
            input_cnt++;
        }

        //AND･ORのdisabledを判定
        var dis_enable_flg = false;
        if(input_cnt < 2) {
            document.mainform.and_or_div[0].checked = true;
            dis_enable_flg = true;
        }

        //AND･ORのdisabledを設定
        for(i=0; i<document.mainform.and_or_div.length; i++){
            document.mainform.and_or_div[i].disabled = dis_enable_flg;
        }
    }

    //概要・場面・内容（2010年改訂）の表示制御
    function disp_control_900_1000()
    {
        //各インシデントの概要に対して
        <{foreach from=$gaiyo2010_list key=code_900 item=i}>
            disp_control_900_1000_item(<{$code_900}>);
        <{/foreach }>
    }

    function disp_control_900_1000_item(code_900)    
    {
        var item_list_1000_index = code_900 - 1;
        //--------------------------------------------------
        //関連オブジェクトの取得
        //--------------------------------------------------

        //インシデントの概要checkboxオブジェクト
        var use_900_obj = document.getElementById("use_900_" + item_list_1000_index);

        //場面・内容制限checkboxオブジェクト
        var use_1000_kind_obj    = document.getElementById("use_1000_kind_" + item_list_1000_index);
        var use_1000_scene_obj   = document.getElementById("use_1000_scene_" + item_list_1000_index);
        var use_1000_content_obj = document.getElementById("use_1000_content_" + item_list_1000_index);

        //場面・内容制限の表示領域divオブジェクト
        var area_1000_use_obj = document.getElementById("area_1000_use_" + item_list_1000_index);

        //場面・内容の表示領域divオブジェクト
        var area_1000_obj         = document.getElementById("area_1000_" + item_list_1000_index);
        var area_1000_kind_obj    = document.getElementById("area_1000_kind_" + item_list_1000_index);
        var area_1000_scene_obj   = document.getElementById("area_1000_scene_" + item_list_1000_index);
        var area_1000_content_obj = document.getElementById("area_1000_content_" + item_list_1000_index);

        //--------------------------------------------------
        //制御?
        //--------------------------------------------------

        //概要が選択されている場合
        if(use_900_obj.checked){
            //場面・内容制限を表示
            area_1000_use_obj.style.display = "";
        }
        //概要が選択されていない場合
        else{
            //場面・内容制限を非表示
            area_1000_use_obj.style.display = "none";

            //場面・内容制限は未選択
            use_1000_kind_obj.checked = false;
            use_1000_scene_obj.checked = false;
            use_1000_content_obj.checked = false;
            set_checkbox_look($('#' + use_1000_kind_obj.id));
            set_checkbox_look($('#' + use_1000_scene_obj.id));
            set_checkbox_look($('#' + use_1000_content_obj.id));
            
            //全ての種類の指定をクリア
            check_disappear_item('kind', item_list_1000_index);
            check_disappear_item('scene', item_list_1000_index);
            check_disappear_item('content', item_list_1000_index);
        }

        //--------------------------------------------------
        //制御?
        //--------------------------------------------------
        //場面・内容制限を行う場合
        if(use_1000_kind_obj.checked || use_1000_scene_obj.checked || use_1000_content_obj.checked){
            //場面・内容領域を表示
            area_1000_obj.style.display = "";
        }
        //場面・内容制限を行わない場合
        else{
            //場面・内容領域を非表示
            area_1000_obj.style.display = "none";
            //全ての種類の指定をクリア
            check_disappear_item('kind', item_list_1000_index);
            check_disappear_item('scene', item_list_1000_index);
            check_disappear_item('content', item_list_1000_index);
        }

        //種類制限を行う場合
        if(use_1000_kind_obj.checked){
            //種類領域を表示
            area_1000_kind_obj.style.display = "";
            check_appear_detail_item('kind', item_list_1000_index)
        }
        //種類制限を行わない場合
        else{
            //種類領域を非表示
            area_1000_kind_obj.style.display = "none";

            //全ての種類の指定をクリア
            check_disappear_item('kind', item_list_1000_index);
        }

        //場面制限を行う場合
        if(use_1000_scene_obj.checked){
            //場面領域を表示
            area_1000_scene_obj.style.display = "";
            check_appear_detail_item('scene', item_list_1000_index)
        }
        //場面制限を行わない場合
        else{
            //場面領域を非表示
            area_1000_scene_obj.style.display = "none";

            //全ての場面の指定をクリア
            check_disappear_item('scene', item_list_1000_index);
        }

        //内容制限を行う場合
        if(use_1000_content_obj.checked){
            //内容領域を表示
            area_1000_content_obj.style.display = "";
            check_appear_detail_item('content', item_list_1000_index)
        }
        //内容制限を行わない場合
        else{
            //内容領域を非表示
            area_1000_content_obj.style.display = "none";

            //全ての場面の指定をクリア
            check_disappear_item('content', item_list_1000_index);
        }
    }

    function check_disappear_item(code, item_list_1000_index) {
        //場面・内容のcheckboxオブジェクト配列
        var obj_list = document.getElementsByName(code + "_" + item_list_1000_index + "[]");
        // 絞込みのcheckboxオブジェクト配列
        var n_obj_list = document.getElementsByName("n_"+code+"_" + item_list_1000_index + "[]");
        // 詳細を取得
        var area_1000_detail_obj_list = document.getElementsByName("area_1000_"+code+"_detail_list_" + item_list_1000_index + "[]");

        //全ての種類の指定をクリア
        for(var i = 0; i < obj_list.length; i++){
            obj_list[i].checked = false;
            set_checkbox_look($('#' + obj_list[i].id));
        }
        for(var i = 0; i < n_obj_list.length; i++){
            n_obj_list[i].checked = false;
            set_checkbox_look($('#' + n_obj_list[i].id));
        }
        if(area_1000_detail_obj_list) {
            for(var i = 0; i < area_1000_detail_obj_list.length; i++) {
                var j = area_1000_detail_obj_list[i].value;
                document.getElementById("area_1000_"+code+"_detail_"+j).style.display = 'none';
            }
        }

        // 項目のcheckboxオブジェクト配列
        var detail_index = document.getElementsByName("area_1000_"+code+"_detail_list_"+item_list_1000_index+"[]");
        for(var i = 0; i < detail_index.length; i++) {
            var detail_obj_list = document.getElementsByName(code+"_detail_" + detail_index[i].value + "[]");
            for(var j = 0; j < detail_obj_list.length; j++){
                detail_obj_list[j].checked = false;
                set_checkbox_look($('#' + detail_obj_list[j].id));
            }
        }

    }

    function check_appear_detail_item(code, item_list_1000_index) {
        var area_1000_detail_obj_list = document.getElementsByName("area_1000_"+code+"_detail_list_" + item_list_1000_index + "[]");

        if(area_1000_detail_obj_list) {
            for(var i = 0; i < area_1000_detail_obj_list.length; i++) {
                var j = area_1000_detail_obj_list[i].value;

                if(document.getElementById("id_n_"+j).checked == true) {
                    document.getElementById("area_1000_"+code+"_detail_"+j).style.display = '';
                }
            }
        }
    }
    // 詳細項目の表示・非表示
    function check_appear_item(OBJ, index, code, item_index) {
        var disp = document.getElementById("area_1000_"+code+"_detail_"+index);
        var item = document.getElementById("id_c_"+index);

        if(OBJ.checked == true) {
            disp.style.display = '';
            item.checked = true;
            set_checkbox_look($('#' + item.id));
        } else {
            disp.style.display = 'none';
            var item_obj = document.getElementsByName(code+"_detail_"+index+"[]");
            for(var i=0; i<item_obj.length; i++) {
                item_obj[i].checked = false;
                set_checkbox_look($('#' + item_obj[i].id));
            }
        }
    }
  </script>
  <{include file="hiyari_post_select_js.tpl"}>
</head>

<body id="classification_label" onload="init_page();">
  <!-------------------------------------------------------------------------
  ヘッダー 
  -------------------------------------------------------------------------->
	<div id="header" class="clearfix">
		<div class="inner" class="clearfix">
			<h1><{$PAGE_TITLE}></h1>
      <p class="action_btn">
        <input type="button" class="button radius_mini" value="<{$SUBMIT_BUTTON_TITLE}>" onclick="document.mainform.submit();">
      </p>
		</div>
	</div>

  <!-------------------------------------------------------------------------
  コンテンツ
  -------------------------------------------------------------------------->
  <div id="content" class="clearfix">
    <div class="section">
      <form name="mainform" action="hiyari_rp_classification_folder_input.php" method="post">
        <input type="hidden" name="session" value="<{$session}>">
        <input type="hidden" name="mode" value="<{$mode}>">
        <input type="hidden" name="is_postback" value="true">
        <input type="hidden" name="folder_id" value="<{$folder_id}>">

        <!--===========================================================
        フォルダ名とフォルダタイプ
        ============================================================-->
        <div class="color02 radius clearfix mB20">
          <table class="v_title">
            <tr>
              <th width="135">分類ラベル名</th>
              <td>
                <input type="text" name="folder_name" value="<{$folder_name|escape}>" maxlength="50" style="width:500px">
              </td>
            </tr>
            <tr>
              <th class="none">共有／個人</td>
              <td class="none"> 
                <{if $mode == "new"}>
                  <select name="common_flg">
                    <option value="f">個人ラベル</option>
                    <option value="t">共有ラベル</option>
                  </select>
                <{else}>
                  <{$folder_type}>
                <{/if}>
              </td>
            </tr>
          </table>
        </div>
        
        <!--===========================================================
        自動分類の条件設定
        ============================================================-->
        <h2 class="type02">
          自動分類用の条件設定
          <span class="notice">＊以下の条件設定を行った場合、このラベルに対して、その条件に該当するレポートを自動的に紐付けます。</span>
        </h2>
        
        <div class="color02 radius clearfix">
          <table class="v_title">
            <!-----------------------------------------------------
            報告者
            ------------------------------------------------------>
            <tr>
              <th rowspan="2" width="53">報告者</th>
              <th width="68">報告部署</th>
              <td colspan="3">
                  <select id="search_emp_class" class="mR10" name="search_emp_class" onchange="setAtrbOptions('search_emp_post');set_disabled_enabled();"></select>
                  &gt;
                  <select id="search_emp_attribute" class="mL10 mR10" name="search_emp_attribute" onchange="setDeptOptions('search_emp_post');"></select>
                  &gt;
                  <select id="search_emp_dept" class="mL10 mR10" name="search_emp_dept" onchange="setRoomOptions('search_emp_post');"></select>
                  <{if $arr_class_name.class_cnt == 4}>
                    &gt;
                    <select id="search_emp_room" class="mL10" name="search_emp_room"></select>
                  <{/if}>
              </td>
            </tr>
            <tr>
              <th>氏名</th>
              <td>
                <input type="text" name="search_emp_name" id="search_emp_name" size="40" value="<{$search_emp_name|escape}>" maxlength="40" style="ime-mode:active" onkeyup="set_disabled_enabled();">
              </td>
              <th>職種</th>
              <td>
                <select name="search_emp_job" id="search_emp_job" onchange="set_disabled_enabled();">
                  <option value=""></option>
                  <{foreach from=$job_list key=job_id item=job_name}>
                    <option value="<{$job_id}>" <{if $job_id == $search_emp_job}>selected<{/if}>>
                      <{$job_name}>
                    </option>
                  <{/foreach}>
                </select>
              </td>
            </tr>
                
            <!-----------------------------------------------------
            患者
            ------------------------------------------------------>
            <tr>
              <th rowspan="2">患者</th>
              <th>患者ＩＤ</th>
              <td>
                <input type="text" name="patient_id" size="20" id="patient_id" value="<{$patient_id|escape}>" onkeyup="set_disabled_enabled();" maxlength="30">
              </td>
              <th class="borderb">患者年齢</th>
              <td>
                <select name="patient_year_from" id="patient_year_from" class="mR10" onchange="check_patient_year('from');"  onchange="set_disabled_enabled();">
                  <option value=""></option>
                  <{foreach from=$patient_year key=k item=year}>
                    <option value="<{$k}>" <{if $k == $patient_year_from}>selected<{/if}>>
                      <{$year}>
                    </option>
                  <{/foreach}>
                </select>
                〜
                <select name="patient_year_to" id="patient_year_to" class="mL10" onchange="check_patient_year('to');" onchange="set_disabled_enabled();">
                  <option value="">
                  <{foreach from=$patient_year key=k item=year}>
                    <option value="<{$k}>" <{if $k == $patient_year_to}>selected<{/if}>>
                      <{$year}>
                    </option>
                  <{/foreach}>
                </select>
              </td>
            </tr>
            <tr>
              <th>氏名</th>
              <td colspan="3">
                <input type="text" name="patient_name" size="40" id="patient_name" value="<{$patient_name|escape}>" maxlength="40" style="ime-mode:active" onkeyup="set_disabled_enabled();">
              </td>
            </tr>
              
            <!-----------------------------------------------------
            キーワード
            ------------------------------------------------------>
            <tr>
              <th colspan="2">表題へのキーワード</th>
              <td colspan="3">
                <input type="text" name="title_keyword" size="50" maxlength="50" value="<{$title_keyword}>" onkeyup="set_disabled_enabled();">
                &nbsp;空白で区切られたキーワードを
                <input type="radio" name="title_and_or_div" value="1" <{if $title_and_or_div == 1}>checked<{/if}>>
                AND
                <input type="radio" name="title_and_or_div" value="2" <{if $title_and_or_div == 2 || $title_and_or_div == ""}>checked<{/if}>>
                OR
                検索
              </td>
            </tr>
            <tr>
              <th colspan="2">インシデントの内容<BR>へのキーワード</th>
              <td colspan="3">
                <input type="text" name="incident_contents_keyword" size="50" maxlength="50" value="<{$incident_contents_keyword}>" onkeyup="set_disabled_enabled();">
                &nbsp;空白で区切られたキーワードを
                <input type="radio" name="incident_and_or_div" value="1" <{ if $incident_and_or_div == 1}>checked<{/if}>>
                AND
                <input type="radio" name="incident_and_or_div" value="2" <{if $incident_and_or_div == 2 || $incident_and_or_div == ""}>checked<{/if}>>
                OR
                検索
              </td>
            </tr>
                        
            <!-----------------------------------------------------
            未入力チェック
            ------------------------------------------------------>
            <tr>
              <th colspan="2">未入力チェック</th>
              <td colspan="3">
                <ul>
                  <li class="item_set">
                    <input type="hidden" name="inci_level_regist_flg" value="t">
                    <span class="checkbox_wrap">
                      <input type="checkbox" id="check1" class="checkbox" name="inci_level_regist_flg" value="f" onclick="set_disabled_enabled();"<{if $inci_level_regist_flg == "f"}>checked<{/if}>>
                    </span>
                    <label for="check1">
                      患者影響レベル
                    </label>                    
                  </li>
                  <li class="item_set">
                    <input type="hidden" name="happened_cause_regist_flg" value="t">
                    <span class="checkbox_wrap">
                      <input type="checkbox" id="check2" class="checkbox" name="happened_cause_regist_flg" value="f" onclick="set_disabled_enabled();"<{if $happened_cause_regist_flg == "f"}>checked<{/if}>>
                    </span>
                    <label for="check2">
                      発生要因
                    </label>
                  </li>
                  <li class="item_set">
                    <input type="hidden" name="inci_background_regist_flg" value="t">
                    <span class="checkbox_wrap">
                      <input type="checkbox" id="check3" class="checkbox" name="inci_background_regist_flg" value="f" onclick="set_disabled_enabled();"<{if $inci_background_regist_flg == "f"}>checked<{/if}>>
                    </span>
                    <label for="check3">
                      インシデントの背景・要因
                    </label>
                  </li>
                  <li class="item_set">
                    <input type="hidden" name="improve_plan_regist_flg" value="t">
                    <span class="checkbox_wrap">
                      <input type="checkbox" id="check4" class="checkbox" name="improve_plan_regist_flg" value="f" onclick="set_disabled_enabled();"<{if $improve_plan_regist_flg == "f"}>checked<{/if}>>
                    </span>
                    <label for="check4">
                      改善策
                    </label>
                  </li>
                  <li class="item_set">
                    <input type="hidden" name="effect_confirm_regist_flg" value="t">
                    <span class="checkbox_wrap">
                      <input type="checkbox" id="check5" class="checkbox" name="effect_confirm_regist_flg" value="f" onclick="set_disabled_enabled();"<{if $effect_confirm_regist_flg == "f"}>checked<{/if}>>
                    </span>
                    <label for="check5">
                      効果の確認
                    </label>
                  </li>
                </ul>
              </td>
            </tr>
                        
            <!-----------------------------------------------------
            評価予定日
            ------------------------------------------------------>
             <{if $predetermined_eval=="t" }>
            <tr>
              <th colspan="2">評価予定日</th>
              <td colspan="3">
                <select id="evaluation_date" name="evaluation_date" onchange="set_disabled_enabled();">
                  <option value="">
                  <option value="today"    <{if $evaluation_date == "today"   }>selected<{/if}> >本日
                  <option value="tomorrow" <{if $evaluation_date == "tomorrow"}>selected<{/if}> >翌日
                  <option value="1week"    <{if $evaluation_date == "1week"   }>selected<{/if}> >1週間以内
                  <option value="2week"    <{if $evaluation_date == "2week"   }>selected<{/if}> >2週間以内
                </select>
              </td>
            </tr>
            <{/if}>
            <!-----------------------------------------------------
            事例分析検討対象
            ------------------------------------------------------>
            <tr>
              <th colspan="2">事例分析検討対象</th>
              <td colspan="3" class="item_set">
                <input type="hidden" name="analysis_target_flg" value="f">
                <span class="checkbox_wrap">
                  <input type="checkbox" id="analysis_target" class="checkbox" name="analysis_target_flg" value="t" onclick="set_disabled_enabled();"<{if $analysis_target_flg == "t"}>checked<{/if}>>
                </span>
                <label for="analysis_target">
                  事例分析検討対象としての登録あり
                </label>
              </td>
            </tr>
                        
            <!-----------------------------------------------------
            警鐘事例
            ------------------------------------------------------>
            <tr>
              <th colspan="2">警鐘事例</th>
              <td colspan="3" class="item_set">
                <input type="hidden" name="warning_case_regist_flg" value="f">
                <span class="checkbox_wrap">
                  <input type="checkbox" id="warning_case" class="checkbox" name="warning_case_regist_flg" value="t" onclick="set_disabled_enabled();" <{if $warning_case_regist_flg == "t"}>checked<{/if}>>
                </span>
                <label for="warning_case">
                  警鐘事例としての登録あり
                </label>
              </td>
            </tr>

            <!-----------------------------------------------------
            患者影響レベル
            ------------------------------------------------------>
            <tr>
              <th colspan="2">患者影響レベル</th>
              <td colspan="3">
                <ul>
                  <{foreach from=$level item=row}>
                    <{foreach from=$row key=code item=info}>
                      <li class="item_set">
                        <span class="checkbox_wrap">
                          <input type="checkbox" id="level<{$code}>" class="checkbox" name="use_level[]" value="<{$code}>" <{if $info.is_checked}>checked<{/if}> onclick="set_disabled_enabled();">
                        </span>
                        <label for="level<{$code}>">
                          <{$info.name|escape}>
                        </label>
                      </li>
                    <{/foreach}>
                  <{/foreach}>
                </ul>
              </td>
            </tr>

            <!-----------------------------------------------------
            ヒヤリ・ハット分類
            ------------------------------------------------------>
            <tr>
              <th colspan="2">分類</th>
              <td colspan="3" class="tabletype02">
                <table class="box">
                  <{foreach name=hiyari from=$hiyari_list key=code item=i}>
                    <{if $smarty.foreach.hiyari.index % 4 == 0}>
                      <tr>
                    <{/if}>
                    
                    <{if $smarty.foreach.hiyari.index % 4 == 3}>
                      <td width="25%" style="border-right:none;">
                    <{else}>
                      <td width="25%">
                    <{/if}>
                    
                        <span class="item_set">
                          <span class="checkbox_wrap">
                            <input id="hiyari_<{$code}>" type="checkbox" class="checkbox" name="hiyari_summary[]" value="<{$code}>" <{if $i.is_checked}>checked<{/if}> onclick="set_disabled_enabled();">
                          </span>
                          <label for="hiyari_<{$code}>"><{$i.name|escape}></span>
                        </span>
                      </td>
                      
                    <{if $smarty.foreach.hiyari.index % 4 == 3 || $smarty.foreach.hiyari.last}>
                      </tr>
                    <{/if}>
                  <{/foreach}>
                </table>
              </td>
            </tr>
                        
            <!-----------------------------------------------------
            概要・場面・内容(2010年改訂)                        
            ------------------------------------------------------>
            <tr>
              <th colspan="2">概要・場面・内容<br>(2010年改訂)</th>
              <td colspan="3" class="tabletype02">
                <table>
                  <{foreach name=gaiyo2010 from=$gaiyo2010_list key=code_900 item=item_900}>
                    <!--概要-->
                    <tr>
                      <{if $smarty.foreach.gaiyo2010.first}>
                        <td class="gaiyo_item_top">
                      <{else}>
                        <td class="gaiyo_item">
                      <{/if}>
                        <span class="item_set">
                          <span class="checkbox_wrap">
                            <input type="checkbox" id="use_900_<{$item_900.item_list_1000_index}>" class="checkbox" name="use_900[]" value="<{$code_900}>" <{if $item_900.is_checked}>checked<{/if}> onclick="disp_control_900_1000();set_disabled_enabled();">
                          </span>
                          <label for="use_900_<{$item_900.item_list_1000_index}>">
                            <{$item_900.name|escape}>
                          </label>
                        </span>
                      </td>
                      <{if $smarty.foreach.gaiyo2010.first}>
                        <td class="gaiyo_item_top">
                      <{else}>
                        <td class="gaiyo_item">
                      <{/if}>
                        <div id="area_1000_use_<{$item_900.item_list_1000_index}>" style="display:none">
                          （
                          <span class="item_set" style="display:inline;">
                            <span class="checkbox_wrap">
                              <input type="checkbox" id="use_1000_kind_<{$item_900.item_list_1000_index}>" class="checkbox" name="use_1000_kind[]" value="<{$item_900.item_list_1000_index}>" <{if $item_900.is_kind_checked}>checked<{/if}> onclick="disp_control_900_1000();">
                            </span>
                            <label for="use_1000_kind_<{$item_900.item_list_1000_index}>">種類で絞り込む</label>
                          </span>
                          <span class="item_set" style="display:inline;">
                            <span class="checkbox_wrap">
                              <input type="checkbox" id="use_1000_scene_<{$item_900.item_list_1000_index}>" class="checkbox" name="use_1000_scene[]" value="<{$item_900.item_list_1000_index}>" <{if $item_900.is_scene_checked}>checked<{/if}> onclick="disp_control_900_1000();">
                            </span>
                            <label for="use_1000_scene_<{$item_900.item_list_1000_index}>">場面で絞り込む</label>
                          </span>
                          <span class="item_set" style="display:inline;">
                            <span class="checkbox_wrap">
                              <input type="checkbox" id="use_1000_content_<{$item_900.item_list_1000_index}>" class="checkbox" name="use_1000_content[]" value="<{$item_900.item_list_1000_index}>" <{if $item_900.is_content_checked}>checked<{/if}> onclick="disp_control_900_1000();">
                            </span>
                            <label for="use_1000_content_<{$item_900.item_list_1000_index}>">内容で絞り込む</label>
                          </span>
                          ）
                        </div>
                      </td>
                    </tr>

                    <!--種類・場面・内容-->
                    <tr id="area_1000_<{$item_900.item_list_1000_index}>" class="gaiyo_sub" style="display:none;">
                      <td colspan="2" class="sub_group">
                        <{foreach from=$item_900.sub key=sub_type item=sub}>
                          <table id="area_1000_<{$sub_type}>_<{$item_900.item_list_1000_index}>" class="sub_item" style="display:none;">
                            <tr>
                              <th rowspan="<{$sub.row_count}>">
                                <{$sub.title}>
                              </th>
                              
                              <{foreach name=list from=$sub.list key=code item=i}>
                                <{if !$smarty.foreach.list.first && $smarty.foreach.list.index % 2 == 0}>
                                  </tr><tr>
                                <{/if}>
                              
                                <td class="<{if $smarty.foreach.list.index % 2 == 0}>left_col<{/if}><{if $smarty.foreach.list.index <= 1}> top_row<{/if}>">
                                  <span class="item_set" style="display:inline-block;">
                                    <span class="checkbox_wrap">
                                      <input type="checkbox" id="id_c_<{$code}>" class="checkbox" name="<{$i.obj_name}>[]" value="<{$code}>" <{if $i.is_checked}>checked<{/if}> onclick="set_disabled_enabled();">
                                    </span>
                                    <label for="id_c_<{$code}>"><{$i.name|escape}></label>
                                  </span>
                                  <span class="item_set" style="display:inline-block;">
                                    （
                                      <span class="checkbox_wrap">
                                        <input type="checkbox" id="id_n_<{$code}>" class="checkbox" name="use_1000_detail_<{$sub_type}>[]" value="<{$code}>" <{if $i.is_detail_checked}>checked<{/if}> onClick="check_appear_item(document.getElementById('id_n_<{$code}>'), <{$code}>, '<{$sub_type}>', '<{$item_900.item_list_1000_index}>')">
                                      </span>
                                      <label for="id_n_<{$code}>">絞り込み</label>
                                    ）
                                  </span>
                                </td>
                              <{/foreach}>
                              
                              <{if $smarty.foreach.list.total % 2 == 1}>
                                <td <{if $smarty.foreach.list.total <= 2}>class="top_row"<{/if}>>&nbsp;</td>
                              <{/if}>
                            </tr>
                          </table>

                          <{foreach name=list from=$sub.list key=code item=i}>
                            <input type="hidden" name="area_1000_<{$sub_type}>_detail_list_<{$item_900.item_list_1000_index}>[]" value="<{$code}>">
                            <table id="area_1000_<{$sub_type}>_detail_<{$code}>" class="sub_item" style="display:none">
                              <tr>
                                <th rowspan="<{$i.row_count}>" style="font-weight:normal;">
                                  項目
                                </th>
                                  
                                <{foreach name=detail from=$i.detail_list key=detail_code item=d}>
                                  <{if !$smarty.foreach.detail.first && $smarty.foreach.detail.index % 2 == 0}>
                                    </tr><tr>
                                  <{/if}>
                                
                                  <td class="<{if $smarty.foreach.detail.index % 2 == 0}>left_col<{/if}><{if $smarty.foreach.detail.index <= 1}> top_row<{/if}>">
                                    <span class="item_set">
                                      <span class="checkbox_wrap">
                                        <input type="checkbox" id="<{$d.obj_name}>_<{$detail_code}>" class="checkbox" name="<{$d.obj_name}>[]" value="<{$detail_code}>" <{if $d.is_checked}>checked<{/if}> onclick="set_disabled_enabled();">
                                      </span>
                                      <label for="<{$d.obj_name}>_<{$detail_code}>">
                                        <{$d.name|escape}>
                                      </label>
                                    </span>
                                  </td>
                                <{/foreach}>
                                      
                                <{if $smarty.foreach.detail.total % 2 == 1}>
                                    <td <{if $smarty.foreach.detail.total <= 2}>class="top_row"<{/if}>>&nbsp;</td>
                                <{/if}>
                              </tr>
                            </table>
                          <{/foreach}>
                        <{/foreach}>
                      </td>
                    </tr>
                  <{/foreach}>
                </table>
              </td>              
            </tr>
            
            <!-----------------------------------------------------
            AND_OR条件
            ------------------------------------------------------>
            <tr>
              <th class="none" colspan="2">AND_OR条件</th>
              <td class="none" colspan="3">
                <input type="radio" name="and_or_div" value="0" <{if $and_or_div == "0"}>checked<{/if}>>AND
                <input type="radio" name="and_or_div" value="1" <{if $and_or_div == "1"}>checked<{/if}>>OR
              </td>
            </tr>
          </table>
        </div>
      </form>
    </div>
  </div>
  
  <!-----------------------------------------------------------------
  フッター
  ------------------------------------------------------------------>
  <div id="footer">
		<div class="inner" class="clearfix">
      <p class="action_btn">
        <input type="button" class="button radius_mini" value="<{$SUBMIT_BUTTON_TITLE}>" onclick="document.mainform.submit();">
      </p>
    </div>
  </div>
</body>
</html>
