<{*
 $page:現在のページ番号
 $page_max:最大ページ番号
 $page_list:表示するページ番号のリスト（現在のページの前後5ページなど。）
*}>
<{if $page_max > 1}>
  <script type="text/javascript">
    //ページ遷移
    function page_change(page){
        document.list_form.page.value=page;
        document.list_form.mode.value="page_change";
        document.list_form.submit();
    }
  </script>
  <ul id="pagination" class="clearfix">
    <{if $page == 1}>
      <li class="current"><a href="javascript:void(0);">&lt;&lt;</a></li>
    <{else}>
      <li class="active"><a href="javascript:void(0);" onclick="page_change(1);return false;">&lt;&lt;</a></li>
    <{/if}>
    
    <{if $page == 1}>
      <li class="current"><a href="javascript:void(0);">&lt;</a></li>
    <{else}>
      <li class="active"><a href="javascript:void(0);" onclick="page_change(<{$page-1}>);return false;">&lt;</a></li>
    <{/if}>
    
    <{foreach from=$page_list item=p}>
      <{if $p == $page}>
        <li class="current"><a href="javascript:void(0);"><{$p}></a></li>
      <{else}>
        <li class="active"><a href="javascript:void(0);" onclick="page_change(<{$p}>);return false;"><{$p}></a></li>
      <{/if}>
    <{/foreach}>
    
    <{if $page == $page_max}>
      <li class="current"><a href="javascript:void(0);">&gt;</a></li>
    <{else}>
      <li class="active"><a href="javascript:void(0);" onclick="page_change(<{$page+1}>);return false;">&gt;</a></li>
    <{/if}>
    
    <{if $page == $page_max}>
      <li class="current"><a href="javascript:void(0);">&gt;&gt;</a></li>
    <{else}>
      <li class="active"><a href="javascript:void(0);" onclick="javascript:page_change(<{$page_max}>);return false;">&gt;&gt;</a></li>
    <{/if}>
  </ul>
<{/if}>
