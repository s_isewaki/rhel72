<{*
項目タイトル(TDタグ)を出力します。

インターフェース
	$grps
		array   項目情報配列
	$item_must
		array   必須項目情報
	$i
		string  グループコード
	$j
		string  アイテムコード
*}>

<{*** 患者影響レベル ***}>
<{if $i==90 && $j==10}> 
<span <{if in_array($i|cat:'_'|cat:$j, $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>
    <{php}>show_inci_level_popup($this->_tpl_vars['con'], $this->_tpl_vars['fname'], $this->_tpl_vars['level_infos'])<{/php}>
</span>

<{*** 種類・場面・内容（2010年度版） ***}>
<{elseif $i==910 || $i==920 || $i==930 || $i==940 || $i==950 || $i==960 || $i==970 || $i==980 || $i==990}>
    <font size="3" class="j12">
        <span id="id_<{$i}>_<{$j}>_color" <{if in_array('900_10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>
            <{$grps[$i].easy_item_list[$j].easy_item_name}>
        </span>
    </font>
    <font size="3" class="j12">
        <{if in_array('900_10', $item_must)}>
            <{if $must_item_disp_mode == 1}>
                <span id="id_<{$i}>_<{$j}>_must" style="color:red">＊</span>
            <{/if}>
        <{/if}>
    </font>

<{*** 対応（その他） ***}>
<{elseif $i==1410 && $j==30}>
    <font size="3" class="j12">
        <span <{if in_array($i|cat:'_'|cat:$j, $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>
            <{$parent_ic[$key]}>の<{$grps[$i].easy_item_list[$j].easy_item_name}>
            <{if $grps[$i].easy_item_list[$j].easy_item_type == 'checkbox'
                && (($i!=140 && count($grps[$i].easy_item_list[$j].easy_list) != 1) 
                || ($i==140 && count($asses_use) != 1))
            }>
                <br />（<{$grps[$i].easy_item_list[$j].explanatory_note}>）
            <{/if}>
        </span>
    </font>

<{*** 
【文言変更可能項目】
 ・ヒヤリハット分類
 ・インシデント概要
 ・インシデントの内容
 ・インシデント発生（発見）時の経過
 ・ヒヤリ・ハットの影響度
 ・インシデントの背景・要因
 ・インシデント内容に関する自由記載欄
 ・概要
 ・インシデントに直接関連する疾患名
 ・インシデント直前の患者の状態
 ・当事者部署配属年月
 ・患者区分2
 ・関連診療科
 ・影響区分
 ・対応区分
 ・リスクの評価: 重要性
 ・リスクの評価: 緊急性 
 ・リスクの評価: 頻度
 ・リスクの予測
 ・システム改善の必要性
 ・教育研修への活用
***}>	
<{elseif $i==125						
	||  $i==120
	||  $i==520
	||  $i==525
	||  $i==530	 
	||  $i==96	 
	||  $i==610	 
	||  $i==630	 
	||  $i==900	 
	||  $i==250  
	||  $i==290  
	||  $i==295  
	||  $i==3300 
	||  $i==240	 
	||  $i==130
	|| ($i==1400 && $j==10) 
	|| ($i==1410 && $j==10)  
	|| ($i==1500 && $j==10) 
	|| ($i==1502 && $j==10)
	|| ($i==1504 && $j==10)
	|| ($i==1510 && $j==10)
	|| ($i==1520 && $j==10)
	|| ($i==1530 && $j==10)
}>
    <font size="3" class="j12">
        <span <{if in_array($i|cat:'_'|cat:$j, $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>
            <{if $eis_item_title[$i]}>
                <{$eis_item_title[$i]}>
                <{if ($i==125 && $j==86) || ($i==290 && $j==130) || ($i==900 && $j==20) }>
                       その他
                <{/if}>
            <{else}>
                <{$grps[$i].easy_item_list[$j].easy_item_name}>
            <{/if}>

            <{if $grps[$i].easy_item_list[$j].easy_item_type == 'checkbox' 
                && (($i!=140 && count($grps[$i].easy_item_list[$j].easy_list) != 1) 
                || ($i==140 && count($asses_use) != 1))
            }>
                <{* チェックボックスの注釈出力（デフォルトは「複数選択可能」） *}>
                <br />（<{$grps[$i].easy_item_list[$j].explanatory_note}>）
            <{/if}>
        </span>
    </font>

<{*** 上記以外の項目 ***}>
<{else}>
    <font size="3" class="j12">
        <span <{if in_array($i|cat:'_'|cat:$j, $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>
            <{$grps[$i].easy_item_list[$j].easy_item_name}>
            <{if $grps[$i].easy_item_list[$j].easy_item_type == 'checkbox' 
            && (($i!=140 && count($grps[$i].easy_item_list[$j].easy_list) != 1) 
            || ($i==140 && count($asses_use) != 1))
            }>
                <{* チェックボックスの注釈出力（デフォルトは「複数選択可能」） *}>
                <br />（<{$grps[$i].easy_item_list[$j].explanatory_note}>）
            <{/if}>
        </span>
    </font>
<{/if}>

<{*** 必須マークの出力 ***}>
<font size="3" class="j12">
    <{if in_array($i|cat:'_'|cat:$j, $item_must)}>
        <{if $must_item_disp_mode == 1}>
            <span style="color:red">＊</span>
        <{/if}>
    <{/if}>
</font>
