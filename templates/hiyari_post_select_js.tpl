<script type="text/javascript">
  //オブジェクトリスト
  var m_post_select_box_id_list = new Array();

  //指定されたキーでオブジェクトリストにselectオブジェクト(ID)を登録
  function set_post_select_box_obj_key(obj_key, class_obj_id, atrb_obj_id, dept_obj_id, room_obj_id)
  {
    m_post_select_box_id_list[obj_key] = new Array(class_obj_id,atrb_obj_id,dept_obj_id,room_obj_id);
  }

  //指定されたキーでオブジェクトリストよりオブジェクトを取得
  function get_post_select_box_obj(obj_key, obj_index)
  {
    var obj_id = m_post_select_box_id_list[obj_key][obj_index];
    return document.getElementById(obj_id);
  }
  
  //部門のオプションを設定
  function setClassOptions(obj_key, class_id, atrb_id, dept_id, room_id)
  {
    //selectオブジェクト取得
    var class_obj = get_post_select_box_obj(obj_key, 0);

    //全削除
    deleteAllOptionsPost(class_obj);

    //全、部門を追加
    <{foreach from=$post_select_data.class_list key=id item=cls}>
      addOptionPost(class_obj, '<{$id}>', '<{$cls}>', class_id);
    <{/foreach}>

    //課のオプションを設定
    setAtrbOptions(obj_key, atrb_id, dept_id, room_id);
  }

  //課のオプションを設定
  function setAtrbOptions(obj_key, atrb_id, dept_id, room_id)
  {
    //selectオブジェクト取得
    var class_obj = get_post_select_box_obj(obj_key, 0);
    var attribute_obj = get_post_select_box_obj(obj_key, 1);

    //全削除
    deleteAllOptionsPost(attribute_obj);

    //部門に対する課の項目を追加
    var class_id = class_obj.value;
    switch (class_id)
    {
      <{foreach from=$post_select_data.atrb_list key=cls_id item=cls}>
        case '<{$cls_id}>':
        <{foreach from=$cls key=atrb_id item=atrb}>
          addOptionPost(attribute_obj, '<{$atrb_id}>', '<{$atrb}>', atrb_id);
        <{/foreach}>
        break;
      <{/foreach}>
      default:
        addOptionPost(attribute_obj, '', '全て', atrb_id);
    }

    //科のオプションを設定
    setDeptOptions(obj_key, dept_id, room_id);
  }

  //科のオプションを設定
  function setDeptOptions(obj_key, dept_id, room_id)
  {
    //selectオブジェクト取得
    var attribute_obj = get_post_select_box_obj(obj_key, 1);
    var dept_obj = get_post_select_box_obj(obj_key, 2);

    //全削除
    deleteAllOptionsPost(dept_obj);
   
    //課に対する科の項目を追加
    var atrb_id = attribute_obj.value;
    switch (atrb_id)
    {
      <{foreach from=$post_select_data.dept_list key=atrb_id item=atrb}>
        case '<{$atrb_id}>':
        <{foreach from=$atrb key=dept_id item=dept}>
          addOptionPost(dept_obj, '<{$dept_id}>', '<{$dept}>', dept_id);
        <{/foreach}>
        break;
      <{/foreach}>
      default:
        addOptionPost(dept_obj, '', '全て', dept_id);
    }

    //室のオプションを設定
    setRoomOptions(obj_key, room_id);
  }
  
  //室のオプションを設定
  function setRoomOptions(obj_key, room_id)
  {
    <{if $post_select_data.class_cnt < 4}>
      return;
    <{/if}>

    //selectオブジェクト取得
    var dept_obj = get_post_select_box_obj(obj_key, 2);
    var room_obj = get_post_select_box_obj(obj_key, 3);

    //全削除
    deleteAllOptionsPost(room_obj);

    //科に対する室の項目を追加
    var dept_id = dept_obj.value;
    switch (dept_id)
    {
      <{foreach from=$post_select_data.room_list key=dept_id item=dept}>
        case '<{$dept_id}>':
        <{foreach from=$dept key=room_id item=room}>
          addOptionPost(room_obj, '<{$room_id}>', '<{$room}>', room_id);
        <{/foreach}>
        break;
      <{/foreach}>
      default:
        addOptionPost(room_obj, '', '全て', room_id);
    }
  }

  //所属のプルダウンに項目を追加
  function addOptionPost(box, value, text, selected) {
    if (box == null) return;
    
    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;

    //FireFox用
    if (selected == value) {
      opt.setAttribute("selected", true);
    }

    box.options[box.length] = opt;

    //IE用
    if (selected == value) {
      opt.setAttribute("selected", true);
    }

    try {box.style.fontSize = 'auto';} catch (e) {}
    box.style.overflow = 'auto';
  }

  //所属のプルダウンの項目を全削除
  function deleteAllOptionsPost(box) {
    if (box == null) return;
    
    for (var i = box.length - 1; i >= 0; i--) {
      box.options[i] = null;
    }
  }
</script>