<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | 医療事故等情報報告</title>
	<meta http-equiv="imagetoolbar" content="no" />
	<link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/hiyari_hiyari.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
</head>
<script type="text/javascript">
  jQuery(function($){
      //分類フォルダ
      $('#b_scrollbar').tinyscrollbar();
  });

  //EXCEL出力実効
  function excel_download()
  {
    document.xls.submit();
  }

  //子画面起動実効
  function show_report_list_window(vertical_index,horizontal_index,report_id_list)
  {
    //報告書一覧データを設定
    document.report_id_list_form.report_id_list.value = report_id_list;

    //縦軸の項目名を設定
    var v_name = '合計';
    <{foreach from=$v_names item=i key=k}>
      if (vertical_index==<{$k}>){
          v_name = "<{$i.v_name}>";
      }
    <{/foreach}>
    document.report_id_list_form.vertical_item_label.value = v_name;

    //横軸の項目名を設定
    var h_name = '合計';
    <{foreach from=$h_names item=i key=k}>
      if (horizontal_index==<{$k}>){
          h_name = "<{$i.h_name}>";
      }
    <{/foreach}>
    document.report_id_list_form.horizontal_item_label.value = h_name;

    //子画面に対して送信
    var h = window.screen.availHeight - 30;
    var w = window.screen.availWidth - 10;
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
    window.open('', 'rm_stats_reports_window', option);
    document.report_id_list_form.submit();
  }
</script>
</head>

<body id="top">
  <{*ヘッダー*}>
  <{include file="hiyari_header2.tpl"}>     
  
  <div id="content" class="clearfix <{$tab_info.total_width_class}>">
    <div id="sub" class="clearfix">
      <{* リンク *}>
      <div id="sub_menu" class="color01 radius">
        <h2><img src="img/side_nav_txt01.gif" alt="MENU" /></h2>
        <ul>
          <li class="active"><a href="hiyari_hiyari_info_2010.php?session=<{$session}>">発生件数情報</a></li>
          <li><a href="hiyari_hiyari_list.php?session=<{$session}>">データ提出(CSV)</a></li>
          <li class="none"><a href="hiyari_hiyari_xml_list.php?session=<{$session}>">データ提出(XML)</a></li>
        </ul>
      </div>
      
      <{* 年月 *}>
      <div id="sub_years" class="color01">
        <ul class="radius_mini">
          <{foreach from=$years item=i name=n}>
            <li <{if $smarty.foreach.n.index == 0}>class="none"<{/if}>>
              <span id="text<{$i}>" onClick="getElementById('num<{$i}>').style.display=(getElementById('num<{$i}>').style.display=='block'?'none':'block');if(getElementById('num<{$i}>').style.display=='block'){getElementById('text<{$i}>').innerHTML='<span style=\'cursor: pointer\'>−</span>';}else{getElementById('text<{$i}>').innerHTML='<span style=\'cursor: pointer\'>＋</span>';}">
                <{if $year==$i && $mode=='quarter' || $i==$cur_year}>
                  <span style="cursor: pointer">−</span>
                <{else}>
                  <span style="cursor: pointer">＋</span>
                <{/if}>
              </span>
              <a href="hiyari_hiyari_info_2010.php?session=<{$session}>&year=<{$i}>&mode=all" <{if $year==$i && $mode=='all'}>class="active"<{/if}>>
                <{$i}>年
              </a>
              <ul class="month" id="num<{$i}>" style="display:<{if $i==$cur_year}>block<{elseif $year==$i && $mode=='quarter'}>block<{else}>none<{/if}>">
                <li class="none">
                  −<a href="hiyari_hiyari_info_2010.php?session=<{$session}>&year=<{$i}>&month=1&mode=quarter" <{if $year==$i && $month == 1 && $mode=='quarter'}>class="active"<{/if}>>1〜3月</a><br>
                </li>
                <li>
                  −<a href="hiyari_hiyari_info_2010.php?session=<{$session}>&year=<{$i}>&month=4&mode=quarter" <{if $year==$i && $month == 4 && $mode=='quarter'}>class="active"<{/if}>>4〜6月</a><br>
                </li>
                <li>
                  −<a href="hiyari_hiyari_info_2010.php?session=<{$session}>&year=<{$i}>&month=7&mode=quarter" <{if $year==$i && $month == 7 && $mode=='quarter'}>class="active"<{/if}>>7〜9月</a><br>
                </li>
                <li>
                  −<a href="hiyari_hiyari_info_2010.php?session=<{$session}>&year=<{$i}>&month=10&mode=quarter" <{if $year==$i && $month == 10 && $mode=='quarter'}>class="active"<{/if}>>10〜12月</a><br>
                </li>
              </ul>
            </li>
          <{/foreach}>
        </ul>
      </div>
    </div>
        
    <div id="main" class="<{$tab_info.main_width_class}>">
      <{* 集計結果表 *}>
      <div id="num_list" class="section">
        <div class="color02 radius">     
          <{* 実行アイコン *}>
          <ul id="main_nav" class="clearfix">
            <li class="main_nav_08"><a href="#" onclick="excel_download();">Excel出力</a></li>
          </ul>
          
          <table>
            <!-- 集計結果タイトル行 START -->                     
              <!-- 表タイトル START -->
              <tr>
                <th width="18%" rowspan="5">項目</th>
                <th width="64%" colspan="4">誤った医療の実施の有無</th>
                <th width="18%" rowspan="5">合計</th>
              </tr>
              <tr>
                <th colspan="3">実施なし</th>
                <th rowspan="4">実施あり</th>
              </tr>
              <tr>
                <th colspan="3">影響度</th>
              </tr>
              <tr>
                <th colspan="3">当該事例の内容が仮に実施された場合</th>
              </tr>
              <!-- 表タイトル END -->
              
              <!-- 列タイトル START -->
              <tr>
                <{foreach from=$col_title item=i}>
                  <th width="<{$col_width}>%">
                    <{$i}>
                  </th>
                <{/foreach}>
              </tr>
              <!-- 列タイトル END -->                
            <!-- 集計結果タイトル行 END -->

            <!-- 集計結果セル行 START -->
              <{foreach from=$data1 item=v key=vkey}>
                <tr>
                  <!-- 行タイトル START -->
                  <th class="left row_title"><{$v.row_title}></th>
                  <!-- 行タイトル END -->
                
                  <!-- セル START -->
                  <{foreach from=$v.items item=h key=hkey}>
                    <td>
                      <{if $h.count > 0}>
                        <a href="javascript:void(0)" onclick="show_report_list_window(<{$vkey}>,<{$hkey}>,'<{$h.report_ids}>');return false;">                        
                      <{/if}>                          
                          <{$h.count}>
                      <{if $h.count > 0}>
                        </a>
                      <{/if}>
                    </td>
                  <{/foreach}>
                  <!-- セル END -->
                  
                  <!-- 行合計 START -->
                  <td>
                    <{if $v.sum > 0}>
                      <a href="javascript:void(0)" onclick="show_report_list_window(<{$vkey}>,-1,'<{$v.report_ids}>');return false;">
                    <{/if}>
                        <{$v.sum}>
                    <{if $v.sum > 0}>
                      </a>
                    <{/if}>
                  </td>
                  <!-- 行合計 END -->
                </tr>
              <{/foreach}>
            <!-- 集計結果セル行 END -->

            <!-- 集計結果合計行 START -->
              <tr>
                <!-- 計タイトル START -->
                <th class="left row_title">合計</th>
                <!-- 計タイトル END -->
                
                <!-- 列合計 START -->
                <{foreach from=$data1_sums item=i key=k}>
                  <td>
                    <{if $i.sum > 0}>
                      <a href="javascript:void(0)" onclick="show_report_list_window(-1,<{$k}>,'<{$i.report_ids}>');return false;">
                    <{/if}>
                        <{$i.sum}>
                    <{if $i.sum > 0}>
                      </a>
                    <{/if}>
                  </td>
                <{/foreach}>
                <!-- 列合計 END -->
                
                <!-- 総合計 START -->
                <td>
                  <{if $sums_sum > 0}>
                    <a href="javascript:void(0)" onclick="show_report_list_window(-1,-1,'<{$report_ids}>');return false;">
                  <{/if}>
                      <{$sums_sum}>
                  <{if $sums_sum > 0}>
                    </a>
                  <{/if}>
                </td>
                <!-- 総合計 END -->
              </tr>
            <!-- 集計結果合計行 END -->

            <!-- 行タイトル START -->
            <tr>
              <th>再掲</th>
            </tr>
            <!-- 行タイトル END -->

            <!-- 集計結果セル行 START -->
              <{foreach from=$data2 item=v key=vkey}>
                <tr>
                  <!-- 行タイトル START -->
                  <th class="left row_title">
                    <{if $vkey==0}>��<{$v.row_title}><{/if}>
                    <{if $vkey==1}>��<{$v.row_title}><{/if}>
                    <{if $vkey==2}>��<{$v.row_title}><{/if}>
                    <{if $vkey==3}>��<{$v.row_title}><{/if}>
                  </th>
                  <!-- 行タイトル END -->
                  
                  <!-- セル START -->
                  <{foreach from=$v.items item=h key=hkey}>
                    <td>
                      <{if $h.count > 0}>
                        <a href="javascript:void(0)" onclick="show_report_list_window(<{$vkey}>,<{$hkey}>,'<{$h.report_ids}>');return false;">
                      <{/if}>
                          <{$h.count}>
                      <{if $h.count > 0}>
                        </a>
                      <{/if}>
                    </td>                        
                  <{/foreach}>
                  <!-- セル END -->
                  
                  <!-- 行合計 START -->
                  <td>
                    <{if $v.sum > 0}>
                      <a href="javascript:void(0)" onclick="show_report_list_window(<{$vkey}>,-1,'<{$v.report_ids}>');return false;">
                    <{/if}>
                      <{$v.sum}>
                    <{if $v.sum > 0}>
                      </a>
                    <{/if}>
                  </td>
                  <!-- 行合計 END -->
                </tr>
              <{/foreach}>
            <!-- 集計結果セル行 END -->              
          </table>           
        </div>
      </div>
    </div>
  </div>
  
  <!--------------------------------------------------------------------------------------------------------------------------->
  <!-- 他画面送信:子画面用フォーム -->
  <!--------------------------------------------------------------------------------------------------------------------------->
  <form name="report_id_list_form" action="hiyari_rm_stats_reports.php" method="post" target="rm_stats_reports_window">
    <input type="hidden" name="<{$session_name}>" value="<{$session_id}>">
    <!-- 送信条件 -->
    <input type="hidden" name="session" value="<{$session}>">
    <!-- 検索条件表示 -->
    <input type="hidden" name="vertical_item_label" value="">
    <input type="hidden" name="horizontal_item_label" value="">
    <!-- 集計データ -->
    <input type="hidden" name="report_id_list" value="">
  </form>

  <!--------------------------------------------------------------------------------------------------------------------------->
  <!-- 他画面送信:Excel用フォーム -->
  <!--------------------------------------------------------------------------------------------------------------------------->
  <form name="xls" action="hiyari_hiyari_info_2010_excel.php" method="post" target="download">
    <input type="hidden" name="<{$session_name}>" value="<{$session_id}>">
    <!-- 送信条件 -->
    <input type="hidden" name="session" value="<{$session}>">
    <!-- 検索条件 -->
    <input type="hidden" name="year" value="<{$year}>">
    <input type="hidden" name="month" value="<{$month}>">
    <input type="hidden" name="mode" value="<{$mode}>">
    <!-- 結果 -->
    <input type="hidden" name="total_item_count" value="<{$total_item_count_list}>">
    <input type="hidden" name="total_vertical_sum_count" value="<{$total_vertical_sum_count_list}>">
    <input type="hidden" name="total_horizontal_sum_count" value="<{$total_horizontal_sum_count_list}>">
    <input type="hidden" name="total_sum_count" value="<{$total_sum_count_list}>">
    <input type="hidden" name="total_show_count" value="<{$total_show_count_list}>">
    <input type="hidden" name="total_vertical2_sum_count" value="<{$total_vertical2_sum_count_list}>">
  </form>
  <iframe name="download" width="0" height="0" frameborder="0"></iframe>
  
	<div id="footer">
		<p>Copyright Comedix all right reserved.</p>
	</div>
</body>
</html>
