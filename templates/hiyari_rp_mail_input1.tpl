<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$title}></title>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
  <script type="text/javascript">

  //--------------------------------------------------
  //TinyMCE関連
  //--------------------------------------------------
  var m_use_tyny_mce = false;
  if(!tinyMCE.isOpera){
      m_use_tyny_mce = true;
      tinyMCE.init({
          mode : "textareas",
          theme : "advanced",
          plugins : "preview,table,emotions,fullscreen,layer",
          //language : "ja_euc-jp",
		  language : "ja",
          width : "100%",
          height : "630",
          editor_selector : "mceEditor",
          theme_advanced_toolbar_location : "top",
          theme_advanced_toolbar_align : "left",
          theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat,|,bullist,numlist,|,outdent,indent,|,hr,|,charmap,emotions,|,preview,|,undo,redo,|,fullscreen",
          theme_advanced_buttons2 : "",
          theme_advanced_buttons3 : "",
          content_css : "tinymce/tinymce_content.css",
		  theme_advanced_statusbar_location : "none",
          force_br_newlines : true,
		  forced_root_block : '',
          force_p_newlines : false
      });
  }

  function show_non_tag_message()
  {
      //Operaの場合はタグ抜きしたTEXTを表示
      document.form1.mail_message.value = "<{$mail_message_non_tag}>";
  }

  //--------------------------------------------------
  //ヘッダー処理関連
  //--------------------------------------------------
  // 返信
  function rp_action_return()
  {
      location.href = "hiyari_rp_mail_input.php?session=<{$session}>&mode=return&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>";
  }
  //転送
  function rp_action_forward()
  {
      location.href = "hiyari_rp_mail_input.php?session=<{$session}>&mode=forward&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>";
  }
  //編集
  function rp_action_update()
  {
      location.href = "hiyari_easyinput.php?session=<{$session}>&gamen_mode=update&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>&report_id=<{$report_id}>";
  }
  //進捗登録
  function rp_action_progress()
  {
      location.href = "hiyari_rp_progress.php?session=<{$session}>&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>&report_id=<{$report_id}>";
  }
  // メッセージ表示
  function rp_action_message_disp() {
      location.href = "hiyari_rp_mail_disp.php?session=<{$session}>&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>";
  }

  //--------------------------------------------------
  //起動時の処理を行います。
  //--------------------------------------------------
  function loadaction()
  {
      //送信先を設定する。
      update_sendto_html();

      //TinyMCEを使用しない場合はタグ抜きメッセージを表示する。
      if(! m_use_tyny_mce){
          show_non_tag_message();
      }
  }

  //--------------------------------------------------
  //送信可能かチェックします。
  //--------------------------------------------------
  function send_check()
  {
      //入力チェック
      var err_msg = "";
      var is_ok = true;
      //TO必須
      if(document.getElementById("to_emp_id_list").value == ""){
          is_ok = false;
          err_msg = err_msg + "宛先を入力してください。\n";
      }
      //件名必須
      if(document.getElementById("mail_subject").value == ""){
          is_ok = false;
          err_msg = err_msg + "件名を入力してください。\n";
      }
      //メッセージ必須
      var mail_message = "";
      if(m_use_tyny_mce){
          //mail_message = tinyMCE.getContent('mail_message');
          mail_message = tinyMCE.activeEditor.getContent();
      }
      else{
          mail_message = document.getElementById("mail_message").value;
      }
      if(mail_message == ""){
          is_ok = false;
          err_msg = err_msg + "メッセージを入力してください。\n";
      }
      if(!is_ok){
          alert(err_msg);
          return false;
      }
      if(!is_ok){
          alert(err_msg);
          return false;
      }

      //意思確認
      if(! confirm("送信します。よろしいですか？")){
          return false;
      }

      return true;
  }
  </script>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <style type="text/css">
    .list {border-collapse:collapse;}
    .list td {border:#35B341 solid 1px;}

    .non_in_list {border-collapse:collapse;}
    .non_in_list td {border:0px;}
  </style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="loadaction()">
  <form id="form1" name="form1" action="hiyari_rp_mail_input.php" method="post" onsubmit="return false;">
    <input type="hidden" name="is_postback" value="true">
    <input type="hidden" name="session" value="<{$session}>">
    <input type="hidden" name="mail_id" value="<{$mail_id}>">
    <input type="hidden" name="mode" value="<{$mode}>">
    <input type="hidden" name="report_id" value="<{$report_id}>">

    <!-- BODY全体 START -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
          <!-- ヘッダー START -->
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr height="32" bgcolor="#35B341">
              <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><{$title}></b></font></td>
              <td>&nbsp;</td>
              <td width="10">&nbsp;</td>
              <td width="32" align="center">
                <a href="javascript:void(0);" onclick="window.close();return false;">
                  <img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';">
                </a>
              </td>
            </tr>
          </table>
          <img src="img/spacer.gif" width="10" height="10" alt=""><br>
          <!-- ヘッダー END -->

          <!-- メイン START -->
          <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
              <td>
                <!-- 実行アイコン START -->
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <td align="center"><img src="img/hiyari_rp_soushin.gif" width="32" height="29" style="cursor: pointer;" onclick="if(send_check()){document.form1.submit();};"></td>
                    <td rowspan="2"><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <td rowspan="2" align="center" bgcolor="#35B341"><img src="img/spacer.gif" width="1" height="8" alt=""></td>
                    
                    <{if $mode == "forward" && $mail_id_mode == "recv" && $arr_btn_flg.return_btn_show_flg == "t"}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td align="center"><img src="img/hiyari_rp_return.gif" width="32" height="29" style="cursor: pointer;" onclick="rp_action_return();"></td>
                    <{/if}>
                    
                    <{if $mode == "return" && $arr_btn_flg.forward_btn_show_flg == "t"}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td align="center"><img src="img/hiyari_rp_forward.gif" width="32" height="29" style="cursor: pointer;" onclick="rp_action_forward();"></td>
                    <{/if}>
                    
                    <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <td align="center"><img src="img/hiyari_rp_mail_disp.gif" height="30" style="cursor: pointer;" onclick="rp_action_message_disp();"></td>
                    
                    <{if $report_edit_flg  && $report_torey_update_flg}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td align="center"><img src="img/hiyari_rp_update.gif" width="36" height="30" style="cursor: pointer;" onclick="rp_action_update();"></td>
                    <{/if}>
                    
                    <{if $progres_edit_flg}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td align="center"><img src="img/hiyari_rp_progress.gif" width="38" height="30" style="cursor: pointer;" onclick="rp_action_progress();"></td>
                    <{/if}>
                  </tr>

                  <tr>
                    <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">送信</font></td>

                    <{if $mode == "forward" && $mail_id_mode == "recv" && $arr_btn_flg.return_btn_show_flg == "t"}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">返信</font></td>
                    <{/if}>

                    <{if $mode == "return" && $arr_btn_flg.forward_btn_show_flg == "t"}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">転送</font></td>
                    <{/if}>
                    
                    <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                    <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">表示</font></td>
                    
                    <{if $report_edit_flg && $report_torey_update_flg}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">編集</font></td>
                    <{/if}>
                    
                    <{if $progres_edit_flg}>
                        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">進捗登録</font></td>
                    <{/if}>
                  </tr>
                </table>
                <!-- 実行アイコン END -->

                <!-- メールヘッダー -->
                <{include file="hiyari_mail_header_js.tpl"}>
                <{include file="hiyari_mail_header1.tpl"}>               
              </td>
            </tr>
            <tr>
              <td align="center" valign="top">
                <textarea id="mail_message" name="mail_message" style="width:100%; height:630;" rows="50" class="mceEditor"><{$mail_message}></textarea>
              </td>
            </tr>
          </table>
          <!-- メイン END -->
        </td>
      </tr>
    </table>
    <!-- BODY全体 END -->
  </form>
</body>
</html>