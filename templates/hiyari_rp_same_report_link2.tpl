<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$title}></title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/smoothness/jquery-ui-1.10.3.custom.min.css">
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/pagination.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
	<script type="text/javascript" src="js/jquery/jquery-ui-1.10.2.custom.min.js"></script>  
  <script type="text/javascript" src="js/jquery/jquery.ui.datepicker-ja.min.js" charset="utf-8"></script>
  <script language="javascript">
    var w = 1024 + 37;
    var h = window.screen.availHeight;
    window.resizeTo(w, h);
    
    var is_dragging = false;
    
    jQuery(function($){
        //部署の初期設定
        setAtrbOptions('<{$search_emp_attribute}>', '<{$search_emp_dept}>', '<{$search_emp_room}>');
        
        //カレンダー
        $('.calendar_text').datepicker({showOn: 'focus', showOtherMonths: true, selectOtherMonths: true});
        $('.calendar_btn').click(function(){
            $(this).prev('input').datepicker( 'show' );
        });     
        
        //ドラッグアイコンの表示・非表示
        $(".report").mouseenter(function(){
            if (!is_dragging){
              $(this).find('.drag_handle').css('display', 'inline');
            }
        }).mouseleave(function(){
            if (!is_dragging){
              $(this).find('.drag_handle').css('display', 'none');
            }
        });    
        
        //---------------------------------------------------------------------
        //ドラッグアンドドロップ
        //---------------------------------------------------------------------
        $( ".drag_entry" ).draggable({ cursor: "move" ,revert:true });
        $( ".drag_add" ).draggable({ cursor: "move",revert:true  });
        // 候補一覧から関連付け一覧へドラッグ＆ドロップ
        $( ".entry_area" ).droppable({
            accept: ".drag_add",
            activate: function( event, ui ) {
              is_dragging = true;
              
              //アイコンを非表示にしてメッセージを表示
              ui.draggable.find('img').css('display', 'none');
              ui.draggable.find('p').css('display', 'block');
            },
            drop: function( event, ui ) {
              document.list_form.add_report_id.value = ui.draggable.prop('id');
              document.list_form.action="hiyari_rp_same_report_link.php?session=<{$session}>";
              document.list_form.submit();
            },
            deactivate: function( event, ui ) {
              is_dragging = false;
              ui.draggable.find('p').css('display', 'none');
            }            
        });
        // 関連付け一覧から候補一覧へドラッグ＆ドロップ
        $( ".add_area" ).droppable({
            accept: ".drag_entry",
            activate: function( event, ui ) {
              is_dragging = true;
              
              //アイコンを非表示にしてメッセージを表示
              ui.draggable.find('img').css('display', 'none');
              ui.draggable.find('p').css('display', 'block');
            },
            drop: function( event, ui ) {
              document.list_form.del_report_id.value = ui.draggable.prop('id');
              document.list_form.action="hiyari_rp_same_report_link.php?session=<{$session}>";
              document.list_form.submit();
            },
            deactivate: function( event, ui ) {
              is_dragging = false;
              ui.draggable.find('p').css('display', 'none');
            }            
        });
    });
    
    // 日付クリア
    function date_clear(id) {
        var obj = document.getElementById(id);
        obj.value = '';
    }

    // 検索処理
    function search_report() {
        document.list_form.action="hiyari_rp_same_report_link.php?session=<{$session}>";
        document.list_form.submit();
    }

    // 部署マスタ設定
    function setAtrbOptions(atrb_id, dept_id, room_id) {
        deleteAllOptions(document.list_form.search_emp_attribute);

        addOption(document.list_form.search_emp_attribute, '', '----------', atrb_id);

        var class_id = document.list_form.search_emp_class.value;
        switch (class_id) {
            <{foreach from=$atrb_list key=class_id item=atrb}>
                case '<{$class_id}>':
                    <{foreach from=$atrb key=atrb_id item=atrb_name}>
                        addOption(document.list_form.search_emp_attribute, '<{$atrb_id}>', '<{$atrb_name}>', atrb_id);
                    <{/foreach}>
                    break;
            <{/foreach}>
        }

        setDeptOptions(dept_id, room_id);
    }

    function setDeptOptions(dept_id, room_id) {
        deleteAllOptions(document.list_form.search_emp_dept);

        addOption(document.list_form.search_emp_dept, '', '----------', dept_id);

        var atrb_id = document.list_form.search_emp_attribute.value;
        switch (atrb_id) {
            <{foreach from=$dept_list key=atrb_id item=dept}>
                case '<{$atrb_id}>':
                    <{foreach from=$dept key=dept_id item=dept_name}>
                        addOption(document.list_form.search_emp_dept, '<{$dept_id}>', '<{$dept_name}>', dept_id);
                    <{/foreach}>
                break;
            <{/foreach}>
        }

        <{if isset($room_list)}>
            setRoomOptions(room_id);
        <{/if}>
    }

    function setRoomOptions(room_id) {
        deleteAllOptions(document.list_form.search_emp_room);

        addOption(document.list_form.search_emp_room, '', '----------', room_id);

        var dept_id = document.list_form.search_emp_dept.value;
        switch (dept_id) {
            <{foreach from=$room_list key=dept_id item=room}>
                case '<{$dept_id}>':
                    <{foreach from=$room key=room_id item=room_name}>
                        addOption(document.list_form.search_emp_room, '<{$room_id}>', '<{$room_name}>', room_id);
                    <{/foreach}>
                  break;
            <{/foreach}>
        }
    }

    function addOption(box, value, text, selected) {
        var opt = document.createElement("option");
        opt.value = value;
        opt.text = text;
        if (selected == value) {
            opt.setAttribute("selected", true);
        }
        box.options[box.length] = opt;
        try {box.style.fontSize = 'auto';} catch (e) {}
        box.style.overflow = 'auto';
    }

    function deleteAllOptions(box) {
        for (var i = box.length - 1; i >= 0; i--) {
            box.options[i] = null;
        }
    }

    function regist_report() {
        document.list_form.mode.value = "regist";
        document.list_form.action="hiyari_rp_same_report_link.php?session=<{$session}>";
        document.list_form.submit();
    }
  </script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
</head>

<body id="top">
  <{* ヘッダー *}>
	<div id="header">
    <div class="inner">
      <h1><{$title}></h1>
    </div>
  </div>
  
  <form name="list_form" action="hiyari_rp_same_report_link.php" method="post">
    <input type="hidden" name="session" value="<{$session}>">
    <input type="hidden" name="page" value="<{$page}>">
    <input type="hidden" name="add_report_id" value="">
    <input type="hidden" name="entry_report_id" value="<{$entry_report_id}>">
    <input type="hidden" name="del_report_id" value="">
    <input type="hidden" name="target_report_id" value="<{$target_report_id}>">
    <input type="hidden" name="is_postback" value="true">
    <input type="hidden" name="mode" value="">
    <input type="hidden" name="color_target_report_id" value="<{$color_target_report_id}>">
    <input type="hidden" name="color_add_report_id" value="<{$color_add_report_id}>">

    <{* 関連付けファイル一覧 *}>
    <div id="content">
      <h2 class="type01 mT0">検索条件指定</h2>
      <div id="search_area" class="color01 radius mB5">        
        <table class="v_title">
          <tr>
            <th>部署</th>
            <td>
              <select name="search_emp_class" onchange="setAtrbOptions();">
                <option value="">----------</option>
                <{foreach from=$class_list key=class_id item=class_name}>
                    <option value="<{$class_id}>" <{if $search_emp_class == $class_id}>selected<{/if}>>
                        <{$class_name}>
                    </option>
                <{/foreach}>
              </select>
              <{$arr_class_name[0]}>
              
              <select name="search_emp_attribute" onchange="setDeptOptions();"></select>
              <{$arr_class_name[1]}>
              
              <select name="search_emp_dept" <{if isset($room_list)}>onchange="setRoomOptions();"<{/if}>></select>
              <{$arr_class_name[2]}>
              
              <{if isset($room_list)}>
                <select name="search_emp_room"></select>
                <{$arr_class_name[3]}>
              <{/if}>
            </td>
          </tr>
<!--           <tr> -->
<!--             <th>報告者名</th> -->
<!--             <td> -->
<!--              <input type="text" name="search_emp_name" value="<{$search_emp_name}>" size="30" maxlength="30" style="ime-mode:active"> -->
<!--             </td> -->
<!--           </tr> -->
<!--           <tr> -->
<!--             <th>報告日</th> -->
<!--             <td> -->
<!--               <input type="text" id="start_date" class="calendar_text" name="start_date" size="14" value="<{$start_date}>" readonly /> -->
<!--               <a class="calendar_btn" href="#">カレンダー</a> -->
<!--              <input type="button" class="button radius_mini"value="クリア" onclick="date_clear('start_date');" /> -->
<!--               〜 -->
<!--               <input type="text" id="end_date" class="calendar_text" name="end_date" size="14" value="<{$end_date}>" readonly /> -->
<!--               <a class="calendar_btn" href="#">カレンダー</a> -->
<!--              <input type="button" class="button radius_mini"value="クリア" onclick="date_clear('end_date');" /> -->
<!--             </td> -->
<!--           </tr> -->
          <tr>
            <th>発生日</th>
            <td>
              <input type="text" id="search_incident_date_start" class="calendar_text" name="search_incident_date_start" size="14" value="<{$search_incident_date_start}>" readonly />
              <a class="calendar_btn" href="#">カレンダー</a>
              <input type="button" class="button radius_mini"value="クリア" onclick="date_clear('search_incident_date_start');" />
              〜
              <input type="text" id="search_incident_date_end" class="calendar_text" name="search_incident_date_end" size="14" value="<{$search_incident_date_end}>" readonly />
              <a class="calendar_btn" href="#">カレンダー</a>
              <input type="button" class="button radius_mini"value="クリア" onclick="date_clear('search_incident_date_end');" />
            </td>
          </tr>
          <tr>
            <th>発生時間帯</th>
            <td>
              <select name="search_incident_hour" onchange="setAtrbOptions();">
                <option value="">----------</option>
                <{section name=index loop=$incident_hour_list}>
                    <option value="<{$incident_hour_list[index].easy_code}>" <{if $search_incident_hour == $incident_hour_list[index].easy_code}>selected<{/if}>>
                        <{$incident_hour_list[index].easy_name}>
                    </option>
                <{/section}>
              </select>
            </td>
          </tr>
          <tr>
            <th>発生場所</th>
            <td>
              <select name="search_incident_place" onchange="setAtrbOptions();">
                <option value="">----------</option>
                <{section name=index loop=$incident_place_list}>
                    <option value="<{$incident_place_list[index].easy_code}>" <{if $search_incident_place == $incident_place_list[index].easy_code}>selected<{/if}>>
                        <{$incident_place_list[index].easy_name}>
                    </option>
                <{/section}>
              </select>
            </td>
          </tr>
          <tr>
            <th>概要</th>
            <td>
              <select name="search_super_item_2010" onchange="setAtrbOptions();">
                <option value="">----------</option>
                <{section name=index loop=$super_item_2010_list}>
                    <option value="<{$super_item_2010_list[index].super_item_id}>" <{if $search_super_item_2010 == $super_item_2010_list[index].super_item_id}>selected<{/if}>>
                        <{$super_item_2010_list[index].super_item_name}>
                    </option>
                <{/section}>
              </select>
            </td>
          </tr>
          <tr>
            <th class="none">内容への<br>キーワード</th>
            <td class="none">
              <input type="text" id="search_incident_keyword" name="search_incident_keyword" size="42" value="<{$search_incident_keyword}>" />
              &nbsp;空白で区切られたキーワードを
              <label><input type="radio" name="search_keyword_type" value="and" <{if $search_keyword_type == 'and'}>checked<{/if}> />AND</label>
              <label><input type="radio" name="search_keyword_type" value="or" <{if $search_keyword_type == 'or'}>checked<{/if}> />OR検索</label>
            </td>
          </tr>
        </table>
      </div>
      <div id="search_btn">
        <input type="button" class="button radius_mini"value="検索" onclick="search_report();">
      </div>
      
      <div id="linked_reports" class="section clearfix">
        <h2 class="type02 mT0">【事案番号関連付け】(主報告にはチェックをつけてください)</h2>
        <!--------------------------------------------------------------------- 
        関連付け報告書一覧
        ---------------------------------------------------------------------->
        <div id="entry_list">
          <div class="color02 radius entry_area">
            <table>
              <tr>
                <th width="3" class="drag_icon"></th>
                <th width="20">主</th>
                <th width="70">事案番号</th>
                <th class="none">表題</th>
              </tr>
              <{foreach from=$linked_report_info item=info}>
                <tr class="report <{if $info.is_add_report}>add_report<{elseif $info.is_target_report}>target_report<{/if}>">
                  <td class="drag_icon">
                    <div id="<{$info.report_id}>" class="drag_entry">
                      <img class="drag_handle" src="img/drag.gif" width="3" height="19">
                      <p>一件の報告書を移動</p>
                    </div>
                  </td>
                  <td class="report_id">
                    <input type="radio" name="main_report_id" value="<{$info.report_id}>" <{if $main_report_id == $info.report_id}>checked<{/if}>>
                  </td>
                  <td>
                      <{$info.report_no}>
                  </td>
                  <td class="none">
                    <{$info.report_title|escape}>
                  </td>
                </tr>
              <{/foreach}>
            </table>
          </div>
          <div id="main_btn">
            <input type="button" class="button radius_mini"value="登録" onclick="regist_report();">
          </div>
        </div>

        <div id="arrow">&lt;==&gt;</div>
          
        <!--------------------------------------------------------------------- 
        追加候補報告書一覧
        ---------------------------------------------------------------------->
        <div id="add_list">
          <div class="color02 radius add_area">
            <table>
              <tr>
                <th width="3" class="drag_icon"></th>
                <th width="90">事案番号</th>
                <th>表題</th>
                <th width="90">報告者</th>
                <th width="60" class="none">報告日</th>
              </tr>
              <{foreach from=$add_report_info item=info}>
                <tr class="report <{if $info.is_add_report}>add_report<{elseif $info.is_target_report}>target_report<{/if}>">
                  <td class="drag_icon">
                    <div id="<{$info.report_id}>" class="drag_add">
                      <img class="drag_handle" src="img/drag.gif" width="3" height="19">
                      <p>一件の報告書を移動</p>
                    </div>  
                  </td>
                  <td class="report_id">
                    <div class='report_link_status<{$info.report_link_status}>'>&nbsp;</div>
                    <{$info.report_no}>
                  </td>
                  <td><{$info.report_title|escape}></td>
                  <td><{$info.create_emp_name}></td>
                  <td class="none"><{$info.registration_date}></td>
                </tr>
              <{/foreach}>
            </table>
            <{include file="hiyari_paging2.tpl"}>
          </div>
        </div>
        
      </div>
    </div>
  </form>
</body>
</html>
