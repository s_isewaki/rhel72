<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
    <meta http-equiv="content-style-type" content="text/css" />
    <meta http-equiv="content-script-type" content="text/javascript" />
    <title>CoMedix <{$INCIDENT_TITLE}> | プロフィール</title>
    <meta http-equiv="imagetoolbar" content="no" />
    <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
    <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
    <link type="text/css" rel="stylesheet" media="all" href="css/profile.css" />
    <!--[if IE]>
      <script type="text/javascript" src="js/ie-radius.js"></script>
    <![endif]-->
    <script type="text/javascript">
        function checkForm() {
            var exp_year = document.FRM_MAIN.exp_Year.value;
            var exp_month = document.FRM_MAIN.exp_Month.value;
            var dept_year = document.FRM_MAIN.dept_Year.value;
            var dept_month = document.FRM_MAIN.dept_Month.value;

            if ((exp_year == '' && exp_month != '') || (exp_year != '' && exp_month == '') || (dept_year == '' && dept_month != '') || (dept_year != '' && dept_month == '')) {
                alert('年月は両方指定してください。');
                return;
            }
            <{if $can_belong_change}>
                if(document.FRM_MAIN.cls.value == "") {
                    alert("所属を選択してください");
                    return false;
                }

                if(document.FRM_MAIN.dept.value == "0") {
                    alert("第三階層を選択してください");
                    return false;
                }
            <{/if}>
            document.FRM_MAIN.submit();
        }
            
        function initPage() {
        <{if $can_belong_change}>
            setAttributeOptions(document.FRM_MAIN.cls, document.FRM_MAIN.atrb, document.FRM_MAIN.dept , document.FRM_MAIN.room , '<{$emp_atrb|escape:"html"}>', '<{$emp_dept|escape:"html"}>', '<{$emp_room|escape:"html"}>');
        <{/if}>
        }

        function setAttributeOptions(class_box, atrb_box, dept_box, room_box, atrb_id, dept_id, room_id) {
          delAllOptions(atrb_box);

          var class_id = class_box.value;
          <{foreach item=arr_atrb_list from=$atrb_list}>
              if (class_id == '<{$arr_atrb_list.class_id|escape:"html"}>') {
                addOption(atrb_box, '<{$arr_atrb_list.atrb_id|escape:"html"}>', '<{$arr_atrb_list.atrb_nm|escape:"html"}>', atrb_id);
              }
          <{/foreach}>
          if (atrb_box.options.length == 0) {
              addOption(atrb_box, '0', '（未登録）');
          }
          
          setDepartmentOptions(atrb_box, dept_box, room_box, dept_id, room_id);
        }

        function setDepartmentOptions(atrb_box, dept_box, room_box, dept_id, room_id) {
            delAllOptions(dept_box);

            var atrb_id = atrb_box.value;

            <{foreach item=arr_dept_list from=$dept_list}>
                if (atrb_id == '<{$arr_dept_list.atrb_id|escape:"html"}>') {
                    addOption(dept_box, '<{$arr_dept_list.dept_id|escape:"html"}>', '<{$arr_dept_list.dept_nm|escape:"html"}>', dept_id);
                }
            <{/foreach}>
            if (dept_box.options.length == 0) {
                addOption(dept_box, '0', '（未登録）');
            }

            setRoomOptions(dept_box, room_box, room_id);
        }

        function setRoomOptions(dept_box, room_box, room_id) {
            if (!room_box) {
                return;
            }

            delAllOptions(room_box);

            var dept_id = dept_box.value;

            <{foreach item=arr_room_list from=$room_list}>
                if (dept_id == '<{$arr_room_list.dept_id|escape:"html"}>') {
                    if (room_box.options.length == 0) {
                        addOption(room_box, '0', '　　　　　');
                    }
                    addOption(room_box, '<{$arr_room_list.room_id|escape:"html"}>', '<{$arr_room_list.room_nm|escape:"html"}>', room_id);
                }
            <{/foreach}>
            
            if (room_box.options.length == 0) {
                addOption(room_box, '0', '（未登録）');
            }
        }

        function delAllOptions(box) {
            for (var i = box.length - 1; i >= 0; i--) {
              box.options[i] = null;
            }
        }

        function addOption(box, value, text, selected) {
            var opt = document.createElement("option");
            opt.value = value;
            opt.text = text;
            if (selected == value) {
                opt.selected = true;
            }
            box.options[box.length] = opt;
            try {box.style.fontSize = 'auto';} catch (e) {}
            box.style.overflow = 'auto';
        }
    </script>
</head>

<body id="top" onload="initPage();">
    <{* ヘッダー *}>
    <{include file="hiyari_header2.tpl"}>

    <{* メイン *}>
    <div id="content" class="<{$tab_info.total_width_class}>">
        <form name='FRM_MAIN' action='hiyari_profile_register.php' method='post'>
            <div class="color02 radius">
                <table class="v_title">
                    <tr>
                        <th>
                            専門医・認定医及びその他の医療従事者の専門・認定資格
                        </th>
                        <td>
                            <textarea name="qualification"><{$qualification}></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            職種
                        </th>
                        <td>
                            <select name="exp_code" class="mL0">
                                <option value="">　</option>
                                <{foreach item=exp1 from=$exp_list}>
                                    <{if $exp1.easy_code == $exp_code}>
                                        <{assign var="exp_code_selected" value="selected"}>
                                    <{else}>
                                        <{assign var="exp_code_selected" value=""}>
                                    <{/if}>
                                    <option value="<{$exp1.easy_code}>" <{$exp_code_selected}>>
                                        <{$exp1.easy_name|escape:"html"}>
                                    </option>
                                <{/foreach}>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            職種開始年月
                        </th>
                        <td>
                            <select name="exp_Year" class="mL0">
                                <option value="">　</option>
                                <{foreach from=$exp_year_list item=year}>
                                  <option value="<{$year}>" <{if $exp_year==$year}>selected<{/if}>>
                                      <{$year}>
                                  </option>
                                <{/foreach}>
                            </select>年
                            <select class="mL10" name="exp_Month" >
                                <option value="">　</option>
                                <{foreach from=$month_list item=month}>
                                  <option value="<{$month}>" <{if $exp_month==$month}>selected<{/if}>>
                                      <{$month}>
                                  </option>
                                <{/foreach}>
                            </select>月
                        </td>
                    </tr>
                    <tr>
                        <{if $can_belong_change}>
                            <th>部署開始年月</th>
                            <td>
                        <{else}>
                            <th>部署開始年月</th>
                            <td>
                        <{/if}>
                            <select name="dept_Year" class="mL0">
                                <option value="">　</option>
                                <{foreach from=$dept_year_list item=year}>
                                  <option value="<{$year}>" <{if $dept_year==$year}>selected<{/if}>>
                                      <{$year}>
                                  </option>
                                <{/foreach}>
                            </select>年
                            <select class="mL10" name="dept_Month" >
                                <option value="">　</option>
                                <{foreach from=$month_list item=month}>
                                  <option value="<{$month}>" <{if $dept_month==$month}>selected<{/if}>>
                                      <{$month}>
                                  </option>
                                <{/foreach}>
                            </select>月
                        </td>
                    </tr>
                    <{if $can_belong_change}>
                        <tr>
                            <th class="none">所属部署</th>
                            <td class="none">
                                <select name="cls" class="mL0" onchange="setAttributeOptions(this.form.cls, this.form.atrb, this.form.dept, this.form.room);">>
                                    <option value="">　</option>
                                    <{foreach item=arr_cls_list from=$cls_list}>
                                        <{if $arr_cls_list.class_id == $emp_cls}>
                                            <{assign var="arr_cls_list_selected" value="selected"}>
                                        <{else}>
                                            <{assign var="arr_cls_list_selected" value=""}>
                                        <{/if}>
                                        <option value="<{$arr_cls_list.class_id}>" <{$arr_cls_list_selected}>><{$arr_cls_list.class_nm|escape:"html"}></option>
                                    <{/foreach}>
                                </select>
                                <{$belong1|escape:"html"}>
                                
                                <select class="mL10" name="atrb" onchange="setDepartmentOptions(this.form.atrb, this.form.dept, this.form.room);">
                                </select>
                                <{$belong2|escape:"html"}>
                                
                                <select class="mL10" name="dept" onchange="setRoomOptions(this.form.dept, this.form.room);"></select>
                                </select>
                                <{$belong3|escape:"html"}>

                                <{if $belong_num == "4"}>
                                    <select class="mL10" name="room">
                                    </select>
                                    </select>
                                    <{$belong4|escape:"html"}>
                                <{/if}>
                            </td>
                        </tr>
                    <{/if}>
                </table>                
            </div>
            <div id="main_btn">
              <input type="button" class="button radius_mini" value="更 新" onclick="checkForm();"　/>
              <input type="hidden" name="session" value="<{$session}>">
              <input type="hidden" name="ok" value="更新">
            </div>
        </form>
    </div>
    <!-- /#content -->

    <{*フッター*}>
    <{include file="hiyari_footer2.tpl"}>
</body>
</html>