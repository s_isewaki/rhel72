<{*
インターフェース
	
	$tr
		boolean 全体を囲むTRタグが必要な場合はtrue。省略時はfalse。
	$colspan
		int     項目値側のTDタグの横結合数。省略時は未指定。
	$td_title_width
		string  (未使用)タイトルのTDの幅。省略時は250
	$td_field_width
		string  (未使用)フィールドのTDの幅。省略時は未指定。
	$td_title_style
		string  タイトルのTDのスタイル
	$td_field_style
		string  フィールドのTDのスタイル
	
使用するサブテンプレート
	hiyari_edit_easyinput_item_title.tpl
	hiyari_edit_easyinput_item_field.tpl

	$grps
		array   項目情報配列
	$vals
		array   値の配列
	$grp_flags
		array   レポートに使用する項目グループ
	$item_must
		array   必須項目情報

	$i
		string  グループコード
	$j
		string  アイテムコード
	$style
		string  "textarea"に対する書式指定。"big"のみ可能。
	
	$comp
		string  "select"の選択値を判定する際に項目名を判定するか項目コードを判定するかを決定する。"name"の場合は項目名判定。
		        $compが設定されるケースは現状ない。(廃止可能)
	$level_infos
		array   インシレベルとメッセージの配列
*}>
<{if $td_title_width == ""}>
	<{assign var="td_title_width" value="250"}>
<{/if}>
<{if $tr == ""}>
	<{assign var="tr" value=false}>
<{/if}>

<{if $tr}>
	<tr height="22">
<{/if}>
    <td<{if $td_title_width != ""}> width=<{$td_title_width}><{/if}>  <{if $td_title_style != ""}>style="<{$td_title_style}>"<{/if}>      bgcolor="#DFFFDC">
    <{include file="hiyari_edit_easyinput_item_title.tpl"}>
    </td>
    <td<{if $td_field_width != ""}> width=<{$td_field_width}><{/if}>  <{if $td_field_style != ""}>style="<{$td_field_style}>"<{/if}>      <{if $colspan > 1}>colspan="<{$colspan}>"<{/if}>   >
    <{if ($i==1400 && $j==30) || ($i==1400 && $j==60) || ($i==1410 && $j==20) || ($i==1410 && $j==30)}>
            <{include file="hiyari_edit_easyinput_item_field2.tpl"}>
    <{else}>
            <{include file="hiyari_edit_easyinput_item_field.tpl"}>
    <{/if}>
    </td>
<{if $tr}>
	</tr>
<{/if}>
