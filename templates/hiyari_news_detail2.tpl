<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
	<title>CoMedix <{$INCIDENT_TITLE}> | <{$page_title}></title>
	<meta http-equiv="imagetoolbar" content="no" />
	<link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/news.css" />
  <{if $imgfile_flg}>
    <script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
    <script type="text/javascript">
      window.onload = function() {
        var ret = Element.getDimensions($('table'));
        window.resizeTo(ret.width+90,ret.height+150);
      }
    </script>
  <{/if}>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
</head>

<body id="top">
	<div id="header" class="ss_header">
    <h1>お知らせ</h1>
	</div>

	<div id="ss_content">
    <div id="ss_main">
      <!-- 参照確認 -->
      <{if $record_flg == "t"}>
        <div id="referred" class="clearfix">
          <{if $referred_flg}>
            内容を確認した方は確認ボタンをクリックしてください。
            <input type="button" class="button_disabled radius_mini" value="確認済み" disabled>
          <{else}>
            <form action="hiyari_news_refer.php" method="post">
              内容を確認した方は確認ボタンをクリックしてください。
              <input type="submit" class="button radius_mini" value="確認">
              <input type="hidden" name="session" value="<{$session}>">
              <input type="hidden" name="news_id" value="<{$news_id}>">
            </form>
          <{/if}>
        </div>
      <{/if}>

      <!-- お知らせ詳細 -->
      <div id="detail" class="color02 radius clear">
        <table id="table" class="v_title">
          <tr>
            <th>タイトル</th>
            <td><{$news_title}></td>
          </tr>
          <tr>
            <th>カテゴリ</th>
            <td><{$news_category}></td>
          </tr>
          <tr>
            <th>内容</th>
            <td><{$news}></td>
          </tr>
          <tr>
            <th>添付ファイル</th>
            <td>
              <{foreach from=$file_list item=file}>
                <p id="p_<{$file.newsfile_noe}>" class="attach">
                  <a href="<{$file.src}>"><{$file.newsfile_name}></a>
                </p>
              <{/foreach}>
            </td>
          </tr>
          <{if $imgfile_flg}>
            <tr>
              <th>画像</th>
              <td>
                <{foreach from=$file_list item=file}>
                  <{if $file.is_img}>
                    <img src="<{$file.src}>" alt="<{$file.newsfile_name}>" title="<{$file.newsfile_name}>" style="padding:3px;border:0;" /><br />
                  <{/if}>
                <{/foreach}>
              </td>
            </tr>
          <{/if}>
          <tr>
            <th>掲載期間</th>
            <td><{$news_begin}>&nbsp;〜&nbsp;<{$news_end}></td>
          </tr>
          <tr>
            <th>登録者</th>
            <td><{$emp_lt_nm}> <{$emp_ft_nm}></td>
          </tr>
          <tr>
            <th class="none">登録日</th>
            <td class="none"><{$news_date}></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</body>
</html>
