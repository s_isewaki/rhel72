<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix <{$INCIDENT_TITLE}> | ヒヤリハット提出</title>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript" src="js/incident_popup.js"></script>
  <script type="text/javascript" src="js/hiyari_hiyari.js"></script>
  <style type="text/css">
    .list {border-collapse:collapse;}
    .list td {border:#35B341 solid 1px;}
    .list_2 {border-collapse:collapse;}
    .list_2 td {border:#999999 solid 1px;}
    .list_in {border-collapse:collapse;}
    .list_in td {border:#FFFFFF solid 0px;}
  </style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
  <!-- ヘッダー START -->
  <{include file="hiyari_header1.tpl"}>
  <!-- ヘッダー END -->

  <!-- リンク START -->
  <img src="img/spacer.gif" width="1" height="5" alt=""><br>
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td style="padding:3px 6px; float:left; background-color:#f4f4f4; border:1px solid #ccc;">
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <a href="hiyari_hiyari_info_2010.php?session=<{$session}>" style="color:#00f">発生件数情報</a>
        </font>
      </td>
      <td width="5"></td>
      <td style="padding:3px 6px; float:left; background-color:#35B341; border:1px solid #ccc;">
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <a href="hiyari_hiyari_list.php?session=<{$session}>" style="color:#FFFFFF">データ提出(CSV)</a>
        </font>
      </td>
      <td width="5"></td>
      <td style="padding:3px 6px; float:left; background-color:#f4f4f4; border:1px solid #ccc;">
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
        <a href="hiyari_hiyari_xml_list.php?session=<{$session}>" style="color:#00f">データ提出(XML)</a>
        </font>
      </td>
    </tr>
  </table>
  <img src="img/spacer.gif" width="1" height="5" alt=""><br>
  <!-- リンク END -->

  <!-- 本体 START -->
  <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  <td valign="top">
    <!-- 分類・一覧・ページ選択 START -->
    <form name="list_form" method="post" action="hiyari_hiyari_list.php">
      <input type="hidden" name="is_postback" value="true">
      <input type="hidden" name="session" value="<{$session}>">
      <input type="hidden" name="mode" value="">
      <input type="hidden" name="change_output_target_in_out" value="">
      <input type="hidden" name="list_tab_mode" value="<{$list_tab_mode}>">
      <input type="hidden" name="page" value="<{$page}>">
      <input type="hidden" name="folder_id" value="<{$folder_id}>">
      <input type="hidden" name="sort_item" value="<{$sort_item}>">
      <input type="hidden" name="sort_div" value="<{$sort_div}>">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <!-- 画面左 START -->
          <td width="280" valign="top">
            <!-- 分類 START -->
            <table width="100%" border="0" cellspacing="0" cellpadding="3" class="list_2">
              <!-- 分類表題 START -->
              <tr>
                <td bgcolor="#FFFBE7" align="center">
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">ヒヤリ・ハット分類フォルダ</font>
                </td>
              </tr>
              <!-- 分類表題 END -->

              <!-- 分類年選択 START -->
              <tr>
                <td bgcolor="#FFFFFF" align="center" nowrap>
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                    <select name="hiyari_year" onchange="hiyari_year_changed(this.value);">
                      <{foreach from=$cat_years item=i}>
                        <option value="<{$i}>" <{if $i==$hiyari_year}>selected="selected"<{/if}>><{$i}></option>
                      <{/foreach}>
                    </select>年
                  </font>
                </td>
              </tr>
              <!-- 分類年選択 END -->

              <!-- 分類フォルダ一覧領域 START -->
              <tr>
                <td bgcolor="#FFFFFF">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list_in">
                    <!-- フォルダ一覧 START -->
                    <tr>
                      <td height="200" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="1" class="list_in">
                          <{foreach from=$folders item=i}>
                            <tr>
                              <td valign="top">
                                <input type="checkbox" value="<{$i.folder_id}>" id="folder_select[]" name="folder_select[]">
                              </td>
                              <td valign="top" <{if $i.folder_id == $folder_id}>bgcolor="#FFDDFD"<{/if}>>
                                <img src="<{$i.folder_image}>" style="vertical-align:middle;cursor: pointer;" onclick="javascript:selected_hiyari_folder_changed('<{$i.folder_id}>');">
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                                  <a href="javascript:selected_hiyari_folder_changed('<{$i.folder_id}>');" <{if $i.folder_id == $folder_id}>style="color:#000000;font-weight:bold"<{/if}>>
                                    <{$i.folder_name}>
                                  </a>
                                </font>
                                <{if $i.folder_id == $folder_id}>
                                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">
                                    (<{$i.report_count}>)
                                  </font>
                                <{/if}>
                              </td>
                            </tr>
                          <{/foreach}>
                        </table>
                      </td>
                    </tr>
                    <!-- フォルダ一覧 END -->
                    
                    <!-- 分類オペレーション START -->
                    <tr>
                      <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="3" class="list_in">
                          <tr>
                            <td width="33%" align="center">
                              <input type="button" value="作成" onclick="create_hiyari_folder('<{$session}>', '<{$hiyari_year}>', false);">
                            </td>
                            <td width="34%" align="center">
                              <input type="button" value="変更" onclick="update_hiyari_folder('<{$session}>', '<{$hiyari_year}>', false);">
                            </td>
                            <td width="33%" align="center">
                              <input type="button" value="削除" onclick="delete_hiyari_folder();">
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <!-- 分類オペレーション END -->
                  </table>

                  <!-- 分類オペレーション START -->
                  <!-- 分類オペレーション END -->
                <!-- 分類枠内 END -->
                </td>
              </tr>
              <!-- 分類フォルダ一覧領域 END -->
            </table>
            <!-- 分類 END -->
          </td>
          <!-- 画面左 END -->
          
          <td width="5">
            <img src="img/spacer.gif" width="1" height="1" alt="">
          </td>
          
          <!-- 画面右 START -->
          <td valign="top">
            <{if $folder_id == ""}>
              <{if $all_folder_info_count == 0}>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">フォルダを作成してください。</font>
              <{else}>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">フォルダを選択してください。</font>
              <{/if}>
            <{else}>
              <!-- 実効アイコン START -->
              <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="center"><img src="img/hiyari_rp_excel.gif" width="36" height="30" style="cursor: pointer;" onclick="output_hiyari_csv();"></td>
                  <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                </tr>
                <tr>
                  <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">CSV出力</font></td>
                  <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
                </tr>
              </table>
              <!-- 実効アイコン END -->

              <!-- 対象タブ／ボタン START -->
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr height="22">
                  <{if $list_tab_mode == "target_list"}>
                    <td width="5"><img src="img/menu_left.gif" width="5" height="22"></td>
                    <td width="90" align="center" bgcolor="#35B341"><a href="javascript:change_list_tab('target_list')"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><nobr><b>提出対象報告</b></nobr></font></a></td>
                    <td width="5"><img src="img/menu_right.gif" width="5" height="22"></td>
                  <{else}>
                    <td width="5"><img src="img/menu_left_40.gif" width="5" height="22"></td>
                    <td width="90" align="center" bgcolor="#AEE1B3"><a href="javascript:change_list_tab('target_list')"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>提出対象報告</nobr></font></a></td>
                    <td width="5"><img src="img/menu_right_40.gif" width="5" height="22"></td>
                  <{/if}>
                  <td width="5">&nbsp;</td>
                  <{if $list_tab_mode == "target_list_ng"}>
                    <{if $list_tab_mode == "target_list_ng"}>
                      <td width="5"><img src="img/menu_left.gif" width="5" height="22"></td>
                      <td width="90" align="center" bgcolor="#35B341"><a href="javascript:change_list_tab('target_list_ng')"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><nobr><b>提出対象報告(NG)</b></nobr></font></a></td>
                      <td width="5"><img src="img/menu_right.gif" width="5" height="22"></td>
                    <{else}>
                      <td width="5"><img src="img/menu_left_40.gif" width="5" height="22"></td>
                      <td width="90" align="center" bgcolor="#AEE1B3"><a href="javascript:change_list_tab('target_list_ng')"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>提出対象報告(NG)</nobr></font></a></td>
                      <td width="5"><img src="img/menu_right_40.gif" width="5" height="22"></td>
                    <{/if}>
                    <td width="5">&nbsp;</td>
                  <{/if}>
                  <{if $list_tab_mode == "target_out_list"}>
                    <td width="5"><img src="img/menu_left.gif" width="5" height="22"></td>
                    <td width="90" align="center" bgcolor="#35B341"><a href="javascript:change_list_tab('target_out_list')"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><nobr><b>提出対象外報告</b></nobr></font></a></td>
                    <td width="5"><img src="img/menu_right.gif" width="5" height="22"></td>
                  <{else}>
                    <td width="5"><img src="img/menu_left_40.gif" width="5" height="22"></td>
                    <td width="90" align="center" bgcolor="#AEE1B3"><a href="javascript:change_list_tab('target_out_list')"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>提出対象外報告</nobr></font></a></td>
                    <td width="5"><img src="img/menu_right_40.gif" width="5" height="22"></td>
                  <{/if}>
                  <td width="5">&nbsp;</td>
                  <td>&nbsp;</td>
                  <{if $list_tab_mode == "target_list" || $list_tab_mode == "target_list_ng"}>
                    <td width=80>
                      <input type="button" value="形式チェック" style="width:100%" onclick="check_ng_report()">
                    </td>
                    <td width=10>
                      &nbsp;
                    </td>
                    <td width=80>
                      <input type="button" value="提出除外" style="width:100%" onclick="change_output_target('out')">
                    </td>
                  <{/if}>
                  <{if $list_tab_mode == "target_out_list"}>
                    <td width=80>
                      <input type="button" value="提出対象" style="width:100%" onclick="change_output_target('in')">
                    </td>
                  <{/if}>
                </tr>
              </table>
                
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td bgcolor="#35B341"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
                </tr>
              </table>
              <img src="img/spacer.gif" width="1" height="5" alt=""><br>
              <!-- 対象タブ／ボタン END -->

              <!-- 一覧 START -->
              <table width="100%" border="0" cellspacing="0" cellpadding="3" class="list_2">
                <tr>
                  <td align="center" bgcolor="#FFFBE7" width="25">&nbsp;</td>
                  <td align="center" bgcolor="#FFFBE7" width="130"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('REPORT_NO');">事案番号</a></font></td>
                  <td align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('REPORT_TITLE');">表題</a></font></td>
                  <td align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('CLASS');">報告部署</a></font></td>
                  <td align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('REGIST_NAME');">報告者</a></font></td>
                  <td align="center" bgcolor="#FFFBE7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><a href="javascript:set_report_list_sort('INCIDENT_DATE');">発生日</a></font></td>
                </tr>
                <{foreach from=$report_list item=row}>
                  <tr> 
                    <td align="center">
                      <input type="checkbox" name="list_select[]" value="<{$row.id}>">
                      <input type="hidden" id="report_id_of_list_id_<{$row.id}>" value="<{$row.id}>">
                    </td>
                    <td align="left">
                      <nobr>
                      <a href="javascript:show_report_ng_info('<{$row.id}>', '<{$session}>', '<{$hiyari_data_type}>', false);">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$row.report_no}></font>
                      </a>
                      </nobr>
                    </td>  
                    <td>
                      <a href="javascript:open_report_update_from_report_id('<{$row.id}>', '<{$session}>','<{$row.mail_id}>');">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$row.title}></font>
                      </a>
                    </td>    
                    <td>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$row.class_nm}></font>
                    </td>	
                    <td>
                      <{if $row.is_anonymous_report && $is_mouseover_popup}>
                        <span onmousemove="name_popup_disp('<{$row.real_name}>', event);" onmouseout="closeDetail();" style="cursor:pointer;">
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="blue"><B><{$row.disp_emp_name}></B></font>
                        </span>
                      <{else}>
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$row.disp_emp_name}></font>
                      <{/if}>
                    </td>	
                    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$row.incident_date}></font></td>
                  </tr>
                <{/foreach}>
              </table>
              <!-- 一覧 END -->
              
              <!-- ページ選択 START -->
              <{if $page_max != 1}>
                <table width="100%">
                  <tr>
                    <td>
                      <table>
                        <tr>
                          <td>
                            <{if $page == 1}>
                              <nobr>
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                                  <span style="color:silver">
                                    先頭へ
                                  </span>
                                </font>
                              </nobr>
                            <{else}>
                              <nobr>
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                                  <a href="javascript:page_change(1);">
                                    先頭へ
                                  </a>
                                </font>
                              </nobr>
                            <{/if}>
                          </td>
                          <td>
                            <{if $page == 1}>
                              <nobr>
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                                  <span style="color:silver">
                                    前へ
                                  </span>
                                </font>
                              </nobr>
                            <{else}>
                              <nobr>
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                                  <a href="javascript:page_change(<{$page-1}>);">
                                    前へ
                                  </a>
                                </font>
                              </nobr>
                            <{/if}>
                          </td>
                          <td>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                              <{section name=i loop=$page_max}>
                                  <{if $smarty.section.i.iteration == $page}>
                                    [<{$smarty.section.i.iteration}>]
                                  <{else}>
                                    <a href="javascript:page_change(<{$smarty.section.i.iteration}>);">[<{$smarty.section.i.iteration}>]</a>
                                  <{/if}>
                              <{/section}>
                            </font>
                          </td>
                          <td>
                            <{if $page == $page_max}>
                              <nobr>
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                                  <span style="color:silver">
                                    次へ
                                  </span>
                                </font>
                              </nobr>
                            <{else}>
                              <nobr>
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                                  <a href="javascript:page_change(<{$page+1}>);">
                                    次へ
                                  </a>
                                </font>
                              </nobr>
                            <{/if}>                        
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              <{/if}>
              <!-- ページ選択 END -->
            <{/if}>
          </td>
          <!-- 画面右 END -->
        </tr>
      </table>
    </form>
    <!-- 分類・一覧・ページ選択 END -->

    <!-- CSV出力用フォーム START -->
    <form name="csv_output_form" action="hiyari_hiyari_csv_output.php" method="post" target="download">
      <input type="hidden" name="session" value="<{$session}>">
      <input type="hidden" name="folder_id" value="<{$folder_id}>">
    </form>
    <iframe name="download" width="0" height="0" frameborder="0"></iframe>
    <!-- CSV出力用フォーム END -->
  </td>
  </tr>
  </table>
  <!-- 本体 END -->
</td>
</tr>
</table>
<!-- BODY全体 END -->

</body>
</html>
