<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui/build/treeview/treeview-min.js"></script>
<link rel="stylesheet" type="text/css" href="js/yui/build/treeview/css/folders/tree.css">
<script type="text/javascript" src="js/yui/build/event/event-min.js"></script>
<script type="text/javascript" src="js/yui/build/dom/dom-min.js"></script>
<script type="text/javascript" src="js/yui/build/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript">
  //起動時の処理
  function initPage(){
      //フォルダーツリー作成
      drawTree();

      <{if $dragdrop_flg}>
        //ドラッグアンドドロップ作成
        constructDragDrop();
      <{/if}>
  }

  //フォルダーツリーを生成
  var tree = null;
  function drawTree(){
      <{if $cnt_of_tree==0}>
          var div = document.getElementById('folders');
          div.style.padding = '2px';
          div.innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダは登録されていません。</font>';
          return;
      <{else}>
          tree = new YAHOO.widget.TreeView('folders');
          var root = tree.getRoot();
          <{php}>
            $tree = $this->_tpl_vars['tree'];
            $a = $this->_tpl_vars['a'];
            $c = $this->_tpl_vars['c'];
            $f = $this->_tpl_vars['f'];
            $o = $this->_tpl_vars['o'];
            $doc_counts = $this->_tpl_vars['doc_counts'];
            $session = $this->_tpl_vars['session'];
            $fname = $this->_tpl_vars['fname'];
            $parent_folder_id = $this->_tpl_vars['parent_folder_id'];
            $dragdrop_flg = $this->_tpl_vars['dragdrop_flg'];
            lib_write_tree_js($tree, $a, $c, $f, $o, $doc_counts, $session, $fname, $parent_folder_id, $dragdrop_flg);
          <{/php}>
          tree.draw();
      <{/if}>
  }

  //ドラッグアンドドロップを生成
  function constructDragDrop(){
      var divs = document.getElementsByTagName('div');
      for (var i = 0, j = divs.length; i < j; i++)
      {
          if (divs[i].className == 'doc')
          {
              var doc = new YAHOO.util.DD(divs[i].id, 'docs');
              doc.setHandleElId('h_'.concat(divs[i].id));

              doc.startPos = YAHOO.util.Dom.getXY(divs[i]);

              doc.startDrag = function (x, y)
              {
                  var el = this.getDragEl();
                  el.style.backgroundColor = '#fefe83';
                  el.style.zIndex = 999;
              }

              doc.onDragDrop = function (e, id)
              {
                  var nodeIndex = id.match(/\d+$/);
                  tree.getNodeByIndex(nodeIndex).getNodeHtml().match(/(\d+),(\d*)<\/span>/);
                  var c = RegExp.$1;
                  var f = RegExp.$2;
                  var lib_id = this.getDragEl().id.match(/\d+$/);
                  var url = 'hiyari_el_move.php?session=<{$session}>&c='.concat(c).concat('&f=').concat(f).concat('&o=<{$o}>&lib_id=').concat(lib_id)<{if $path=="3"}>.concat('&path=3')<{/if}>;
                  location.href = url;
              }

              doc.endDrag = function (x, y)
              {
                  var el = this.getDragEl();
                  YAHOO.util.Dom.setXY(el, this.startPos);
                  el.style.backgroundColor = '';
                  el.style.zIndex = 0;
              }
          }
      }
  }

  function expandAncestor(node){
      if (node.parent){
          node.parent.expand();
          expandAncestor(node.parent);
      }
  }

  function expandAll(){
      if (tree){
          tree.expandAll();
          document.getElementById('expand').style.display = 'none';
          document.getElementById('collapse').style.display = '';
      }
  }

  function collapseAll(){
      if (tree){
          tree.collapseAll();
          document.getElementById('collapse').style.display = 'none';
          document.getElementById('expand').style.display = '';
      }
  }

  //コンテンツ、フォルダの削除
  function deleteDocument(){
      var checked_cid = getCheckedIds(document.mainform.elements['cid[]']);
      var checked_did = getCheckedIds(document.mainform.elements['did[]']);
      var checked_fid = getCheckedIds(document.mainform.elements['fid[]']);

      if (checked_cid.length == 0 && checked_did.length == 0 && checked_fid.length == 0)
      {
          alert('削除対象が選択されていません。');
          return;
      }

      if (!confirm('選択されたデータを削除します。よろしいですか？'))
      {
          return;
      }

      document.mainform.action = 'hiyari_el_delete.php';
      addHiddenElement(document.mainform, 'archive', '<{$a}>');
      addHiddenElement(document.mainform, 'category', '<{$c}>');
      addHiddenElement(document.mainform, 'folder_id', '<{$f}>');
      addHiddenElement(document.mainform, 'o', '<{$o}>');
      document.mainform.submit();
  }

  //※deleteDocument()用
  function getCheckedIds(boxes){
      var checked_ids = new Array();
      if (boxes)
      {
          if (!boxes.length)
          {
              if (boxes.checked)
              {
                  checked_ids.push(boxes.value);
              }
          }
          else
          {
              for (var i = 0, j = boxes.length; i < j; i++)
              {
                  if (boxes[i].checked)
                  {
                      checked_ids.push(boxes[i].value);
                  }
              }
          }
      }
      return checked_ids;
  }
  
  //※deleteDocument()用
  function addHiddenElement(frm, name, value){
      var input = document.createElement('input');
      input.type = 'hidden';
      input.name = name;
      input.value = value;
      frm.appendChild(input);
  }

  //「上の階層へ」
  function moveToParent(){
      <{if $f == ""}>
      //書庫へ移動
          location.href = '<{$fname}>?session=<{$session}>&a=<{$a}>&o=<{$o}>';
      <{elseif $parent_folder_id == ""}>
      //カテゴリへ移動
          location.href = '<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&o=<{$o}>';
      <{else}>
      //親フォルダへ移動
          location.href = '<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&f=<{$parent_folder_id}>&o=<{$o}>';
      <{/if}>
  }

  //コンテンツ登録画面を表示します。
  function registDocument(){
      var url = "hiyari_el_register.php?session=<{$session}>&archive=<{$a}>&category=<{$c}>&folder_id=<{$f}>&o=<{$o}>";
      show_sub_window(url);
  }

  //フォルダ作成画面を表示します。
  function createFolder(){
      var url = "hiyari_el_folder_register.php?session=<{$session}>&archive=<{$a}>&cate_id=<{$c}>&parent=<{$f}>";
      show_sub_window(url);
  }

  //フォルダ更新画面を表示します。
  function updateFolder(){
      var url = "hiyari_el_folder_update.php?session=<{$session}>&archive=<{$a}>&cate_id=<{$c}>&folder_id=<{$f}>";
      show_sub_window(url);
  }

  //検索画面を表示します。
  function show_search_page(){
      var url = "hiyari_el_search.php?session=<{$session}>";
      show_sub_window(url);
  }

  //参照履歴画面を表示します。
  function show_refer_log_page(){
      var url = "hiyari_el_refer_log.php?session=<{$session}>";
      show_sub_window(url);
  }

  //子画面を表示します。
  function show_sub_window(url){
      var h = window.screen.availHeight - 30;
      var w = window.screen.availWidth - 10;
      var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
      window.open(url, '_blank',option);
  }

  //リロードします。※注意：この関数は別画面から呼び出されます。
  function reload_page(){
      location.href = "<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&f=<{$f}>&o=<{$o}>";
  }
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
  .list {border-collapse:collapse;}
  .list td {border:#5279a5 solid 1px;}
  .list td td {border-width:0;}
  .selectedLabel {font-weight:bold; color:black; padding:1px;}
  .ygtvlabel {padding-right:6px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<form name="mainform" method="post">
  <input type="hidden" name="session" value="<{$session}>">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <{include file="hiyari_header1.tpl"}>
        <img src="img/spacer.gif" width="1" height="5" alt=""><br>
      </td>
    </tr>
    <tr>
      <td>
        <!-- 本体 START -->
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
            <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
          </tr>
          <tr>
            <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
            <td bgcolor="#F5FFE5">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <!-- 上部ボタン START -->
                <tr valign="top">
                  <td align="right" colspan="3">
                    <input type="button" value="コンテンツ検索" onclick="show_search_page()">
                    <input type="button" value="参照履歴" onclick="show_refer_log_page()">
                    <input type="button" value="上の階層へ" <{$parent_button_disabled}> onclick="moveToParent();">
                    <input type="button" value="フォルダ作成" <{$folder_button_disabled}> onclick="createFolder();">
                    <input type="button" value="コンテンツ登録" <{$document_button_disabled}> onclick="registDocument();">
                    <input type="button" value="フォルダ名更新" <{$folder_edit_button_disabled}> onclick="updateFolder();">
                    <input type="button" value="削除" <{$delete_button_disabled}> onclick="deleteDocument();">
                  </td>
                </tr>
                <!-- 上部ボタン END -->
                
                <tr height="2">
                  <td colspan="3"></td>
                </tr>
                
                <!-- フォルダ/コンテンツ START -->
                <tr valign="top">
                  <!-- フォルダ一覧 START -->
                  <td bgcolor="#FFFFFF">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="2">
                            <tr height="22" bgcolor="#DFFFDC">
                              <td class="spacing">
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font>
                              </td>
                              <td align="right">
                                <span id="expand"><input type="button" value="全て開く" onclick="expandAll();"></span>
                                <span id="collapse" style="display:none;"><input type="button" value="全て閉じる" onclick="collapseAll();"></span>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" height="400">
                          <div id="folders"></div>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <!-- フォルダ一覧 END -->
                  
                  <td>
                  </td>
                
                  <!-- コンテンツ START -->
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                      <tr height="22" bgcolor="#DFFFDC">
                        <td align="center" width="40">
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">削除</font>
                        </td>
                        <td>
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                            <{if $o=="1" }> <{*コンテンツ名：昇順*}>
                                <a href="<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&f=<{$f}>&o=2"><img src="img/up.gif" alt="" width="17" height="17" border="0" style="vertical-align:middle;">コンテンツ名</a>
                            <{else}> <{*コンテンツ名：降順*}>
                                <a href="<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&f=<{$f}>&o=1"><img src="img/down.gif" alt="" width="17" height="17" border="0" style="vertical-align:middle;">コンテンツ名</a>
                            <{/if}>
                          </font>
                        </td>
                        <td width="50">
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">感想</font>
                        </td>
                        <td width="95">
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                            <{if $o=="3" }> <{*更新日時：昇順*}>
                              <a href="<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&f=<{$f}>&o=4"><img src="img/up.gif" alt="" width="17" height="17" border="0" style="vertical-align:middle;">更新日</a>
                            <{else}><{*更新日時：降順*}>
                              <a href="<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&f=<{$f}>&o=3"><img src="img/down.gif" alt="" width="17" height="17" border="0" style="vertical-align:middle;">更新日</a>
                            <{/if}>
                          </font>
                        </td>
                        <td width="75">
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                            <{if $o=="5"}><{*サイズ：昇順*}>
                              <a href="<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&f=<{$f}>&o=6"><img src="img/up.gif" alt="" width="17" height="17" border="0" style="vertical-align:middle;">サイズ</a>
                            <{else}><{*サイズ：降順*}>
                              <a href="<{$fname}>?session=<{$session}>&a=<{$a}>&c=<{$c}>&f=<{$f}>&o=5"><img src="img/down.gif" alt="" width="17" height="17" border="0" style="vertical-align:middle;">サイズ</a>
                            <{/if}>
                          </font>
                        </td>
                        <td width="50">
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限</font>
                        </td>
                      </tr>
                      <{foreach from=$contents item=i}>
                        <tr height="22" bgcolor="#FFFFFF">
                          <td align="center"><{$i.cbox}></td>
                          <td style="padding:0;">
                            <div id="doc<{$i.did}>" class="<{$i.div_class}>">
                              <table border="0" cellspacing="0" cellpadding="2">
                                <tr valign="top">
                                  <td><{$i.icon}></td>
                                  <td>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$i.anchor}></font>
                                  </td>
                                </tr>
                              </table>
                            </div>
                          </td>
                          <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$i.kansou}></font></td>
                          <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$i.modified_str}></font></td>
                          <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$i.size}></font></td>
                          <td>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                              <a href="javascript:void(0);" onclick="show_sub_window('<{$i.auth_update_url}>');return false;"><{$i.auth}></a>
                            </font>
                          </td>
                        </tr>
                      <{/foreach}>
                    </table>
                  </td>
                  <!-- コンテンツ END -->
                </tr>
                <!-- フォルダ/コンテンツ END -->
              </table>
            </td>
            <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
          </tr>
          <tr>
            <td><img src="img/r_3.gif" width="10" height="10"></td>
            <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td><img src="img/r_4.gif" width="10" height="10"></td>
          </tr>
        </table>
        <!-- 本体 END -->
      </td>
    </tr>
  </table>
</form>
</body>
</html>
