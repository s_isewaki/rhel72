<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | 報告書プレビュー</title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/smoothness/jquery-ui.custom.css">
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
  <script type="text/javascript" src="js/report_form.js"></script>
  <script type="text/javascript" src="js/jquery/jquery-ui-1.10.2.custom.min.js"></script>
  <script type="text/javascript" src="js/jquery/jquery.ui.datepicker-ja.min.js" charset="utf-8"></script>
  <script type="text/javascript" src="js/jquery/jquery.bgiframe.js"></script>
  <!-- [if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
  <script type="text/javascript">
  //====================================================================================================
  //起動時の処理
  //====================================================================================================
  var w = 1024 + 37;
  var h = window.screen.availHeight;
  window.resizeTo(w, h);
  
  <{if $style_code==2}>
    $(document).ready(function(){
      <{if !$readonly_flg && $eis_select_able}>
        set_eis_input_list();
        set_eis_no(<{$sel_eis_no}>);
      <{/if}>

      <{if $auto_reload}>
        start_auto_session_update();
      <{/if}>  
    });
  <{else}>
    function load_item() {
      cng_scene(document.FRM_MAIN._700_10.value);
      cng_kind(document.FRM_MAIN._700_10.value);
      cng_content(document.FRM_MAIN._700_10.value);
      cng_scene_item(document.FRM_MAIN._710_10.value);
      cng_kind_item(document.FRM_MAIN._740_10.value);
      cng_content_item(document.FRM_MAIN._770_10.value);

      disp_kind();
    }

    function disp_kind() {
      var id   = document.FRM_MAIN._700_10.value;
      var disp = document.getElementById("kind_disp");

      if(id == 2 || id == 3) {
          disp.style.display = '';
      } else {
          disp.style.display = 'none';
      }
    }

    function load_item_2010() {
      cng_kind_2010();
      cng_scene_2010();
      cng_content_2010();
      cng_kind_item_2010();
      cng_scene_item_2010();
      cng_content_item_2010();

      disp_kind_2010();
    }

    function disp_kind_2010() {
      var disp = document.getElementById("kind_disp_2010");

      if( $("input:radio[name='_910_10[]']").length > 1 ) {
        disp.style.display = '';
      } else {
        disp.style.display = 'none';
      }
    }

    function undisp(id) {
      ele1 = document.getElementById("add_item_1");
      ele2 = document.getElementById("add_item_2");
      ele3 = document.getElementById("add_item_3");

      if(id != 3) ele1.style.display = 'none';
      if(id != 6) ele2.style.display = 'none';
      if(id != 8) ele3.style.display = 'none';
    }
  <{/if}>
</script>

<body id="top" class="houkoku onLoad="<{if in_array(700, $grp_flags)}>load_item();<{/if}><{if in_array(800, $grp_flags)}>undisp(document.FRM_MAIN._700_10.value);<{/if}><{if in_array(900, $grp_flags)}>load_item_2010();disp_item_2010();<{/if}><{if in_array(110, $grp_flags)}>area_110_65_disp_change('<{$vals[110][60][0]}>', '<{$vals[110][65][0]}>');<{/if}>">
  <a id="pagetop" name="pagetop"></a>

	<div id="header">
		<div class="inner">
			<h1>報告書様式プレビュー</h1>
		</div>    
	</div>  
  
  <div id="content" class="clearfix">
    <form name='FRM_MAIN' action='hiyari_easyinput_preview.php' method='post'>
      <div class="section">
        <input type='hidden' name='session' value='<{$session}>'>
        <input type='hidden' name='hyr_sid' value='<{$hyr_sid}>'>
        <{include file="report_form_main.tpl"}>
      </div>
    </form>
  </div>
</body>
</html>
