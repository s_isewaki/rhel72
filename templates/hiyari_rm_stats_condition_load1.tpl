<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix ファントルくん | 集計条件読込</title>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript">
    function load_stats_condition(id) { 
      opener.load_stats_condition(id);
      window.close();
    }
    
    function delete_condition(){
      var stats = document.getElementsByName('stats_delete[]');
      var flg = false;
      for ( var i=0; i < stats.length; i++ ) {
          if (stats[i].checked) flg = true;
      }
      if (flg == false) {
          alert('削除対象が選択されていません。');
          return;
      }
      document.main_form.postback_mode.value='delete';
      document.main_form.submit();
    }
  </script>
  <style type="text/css">
    form {margin:0;}
    table.list {border-collapse:collapse;}
    table.list td {border:solid #35B341 1px;}

    form {margin:0;}
    table.list {border-collapse:collapse;}
    table.list td {border:solid #35B341 1px;}
  </style>
  <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
  <center>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr height="32" bgcolor="#35B341">
        <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>集計条件一覧</b></font></td>
        <td>&nbsp;</td>
        <td width="10">&nbsp;</td>
        <td width="32" align="center">
          <a href="javascript:void(0);" onclick="window.close();">
            <img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';">
          </a>
        </td>
      </tr>
    </table>
    <img src="img/spacer.gif" width="10" height="10" alt=""><br>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
      </tr>
      <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td>
          <form name="main_form" action="hiyari_rm_stats_condition_load.php" method="post">
            <input type="hidden" name="session" value="<{$session}>">
            <input type="hidden" name="postback_mode" value="">
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
              <tr height="22">
                <td align="right" bgcolor="#F5FFE5" style="border-style:none;">
                  <input type="button" value="削除" onclick="delete_condition();">
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
              <tr height="22" bgcolor="#DFFFDC">
                <td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
                <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計条件</font></td>
              </tr>
              <{if isset($stats_condition)}>
                <{foreach from=$stats_condition item=i}>
                  <tr height="22">
                    <td>
                      <input type="checkbox" name="stats_delete[]" value="<{$i.condition_id|escape}>">
                    </td>
                    <td width="99%">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                        <a name="selected_link" href="javascript:load_stats_condition('<{$i.condition_id|escape}>');"><{$i.stats_name}></a>
                      </font>
                    </td>
                  </tr>
                <{/foreach}>
              <{/if}>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><img src="img/spacer.gif" alt="" width="10" height="1"></td>
              </tr>
            </table>
          </form>
        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
      </tr>
      <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
      </tr>
    </table>
  </center>
  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <img src="img/spacer.gif" width="1" height="1" alt="">
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td><img src="img/spacer.gif" alt="" width="10" height="1"></td>
    </tr>
  </table>
</body>
</html>
