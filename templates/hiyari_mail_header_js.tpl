<{*************************************************************************************************
ファイル名：hiyar_mail_header_js.tpl
概要：hiyari_mail_input.iniにあったshow_hiyari_mail_header_input_form()のjs部分をテンプレート化したもの
*************************************************************************************************}>  
<script type="text/javascript">
  //--------------------------------------------------
  //送信先対象一覧
  //--------------------------------------------------
  var m_auth_user_list = new Array();
  m_auth_user_list["SM"] = new Array();
  m_auth_user_list["RM"] = new Array();
  m_auth_user_list["RA"] = new Array();
  m_auth_user_list["SD"] = new Array();
  m_auth_user_list["MD"] = new Array();
  m_auth_user_list["HD"] = new Array();
          
  <{foreach from=$auth_user_list item=i}>
    m_auth_user_list['<{$i.auth}>'] = new Array(
    <{foreach from=$i.emp item=e name=list}>
      <{if !$smarty.foreach.list.first}>,<{/if}>
      new user_info('<{$e.emp_id}>','<{$e.emp_name}>',false)
    <{/foreach}>
    );
  <{/foreach}>

  //--------------------------------------------------
  //送信先一覧
  //--------------------------------------------------
  var m_sendto_list = new Array();
  m_sendto_list["TO"] = new Array();
  m_sendto_list["CC"] = new Array();
  var m_first_sendto_list = new Array();
  m_first_sendto_list["TO"] = new Array();
  m_first_sendto_list["CC"] = new Array();
  
  <{if isset($to_list)}>
    m_sendto_list['TO'] = new Array(
    <{foreach from=$to_list item=i name=list}>
      <{if !$smarty.foreach.list.first}>,<{/if}>
      new user_info('<{$i.emp_id}>', '<{$i.emp_name}>', <{$i.anonymous}>)
    <{/foreach}>  
    );
    
    m_first_sendto_list['TO'] = new Array(
    <{foreach from=$to_list item=i name=list}>
      <{if !$smarty.foreach.list.first}>,<{/if}>
      new user_info('<{$i.emp_id}>', '<{$i.emp_name}>', <{$i.anonymous}>)
    <{/foreach}>  
    );
  <{/if}>
        
  <{if isset($cc_list)}>
    m_sendto_list['CC'] = new Array(
    <{foreach from=$cc_list item=i name=list}>
      <{if !$smarty.foreach.list.first}>,<{/if}>
      new user_info('<{$i.emp_id}>', '<{$i.emp_name}>', <{$i.anonymous}>)
    <{/foreach}>  
    );
    
    m_first_sendto_list['CC'] = new Array(
    <{foreach from=$cc_list item=i name=list}>
      <{if !$smarty.foreach.list.first}>,<{/if}>
      new user_info('<{$i.emp_id}>', '<{$i.emp_name}>', <{$i.anonymous}>)
    <{/foreach}>  
    );
  <{/if}>

  //--------------------------------------------------
  //CCの表示/非表示
  //--------------------------------------------------
  function switch_display(type) {
      // 表示/非表示を切り替えるオブジェクトのIDを設定
      id_name = 'send_emp_list_' + type ;

      // 現在の状態を取得
      display_type = document.getElementById(id_name).style.display;

      // 現在の状態とは逆の状態を設定
      if ( display_type == 'none') {
          document.getElementById(id_name).style.display = 'block';
      } else {
          document.getElementById(id_name).style.display = 'none';
      }

  }

  //--------------------------------------------------
  //CC使用設定
  //--------------------------------------------------
  <{if $is_cc_usable}>
    var m_is_cc_usable = true;
  <{else}>
    var m_is_cc_usable = false;
  <{/if}>
  function is_cc_usable(is_usable)
  {
      // 表示/非表示を切り替えるオブジェクトのIDを設定
      id_name = 'send_emp_list_cc';
      //使用フラグを更新
      m_is_cc_usable = is_usable;


      //CCの宛先を全削除
      m_sendto_list["CC"] = new Array();

      //HTML反映処理
      update_sendto_html();

      // 現在の状態を取得
      display_type = document.getElementById(id_name).style.display;

      // 現在の状態とは逆の状態を設定
      if ( display_type == 'none') {
          document.getElementById(id_name).style.display = '';
      }
  }

  //--------------------------------------------------
  //職員検索画面を表示します。
  //--------------------------------------------------
  function show_sendto_add_window(tocc)
  {
    call_emp_search(tocc);
  }

  //--------------------------------------------------
  //職員名簿画面
  //--------------------------------------------------
  function call_emp_search(input_div)
  {
    var childwin = null;

    dx = screen.availWidth - 10;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = 'hiyari_emp_list.php';
    url += '?session=<{$session}>&input_div=' + input_div;
    childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    childwin.focus();
  }

  //登録対象者追加 引数：", "で区切られたID、名前
  //※この画面は職員名簿画面から呼び出されます。
  function add_emp_list(emp_id, emp_nm, input_div)
  {
    add_recv_list_from_emplist(input_div, emp_id, emp_nm);
  }

  //--------------------------------------------------
  //送信先対象者情報
  //--------------------------------------------------
  function user_info(emp_id,emp_name,anonymous)
  {
    this.emp_id = emp_id;
    this.emp_name = emp_name;
    this.anonymous = anonymous;
  }

  //--------------------------------------------------
  //初期宛先判定
  //--------------------------------------------------
  function is_first_tocc_emp(emp_id)
  {
    var tocc = "TO";
    for(var i=0;i<m_first_sendto_list[tocc].length;i++)
    {
      if(emp_id == m_first_sendto_list[tocc][i].emp_id)
      {
        return true;
      }
    }
    var tocc = "CC";
    for(var i=0;i<m_first_sendto_list[tocc].length;i++)
    {
      if(emp_id == m_first_sendto_list[tocc][i].emp_id)
      {
        return true;
      }
    }
    return false;
  }

  //--------------------------------------------------
  //送信先を追加します。(個人単位)
  //--------------------------------------------------
  //tocc:'TO','CC'のいずれか。
  //※この関数は他の画面から呼ばれる予定です。
  function add_recv_list_from_emp(tocc,emp_id,emp_name)
  {
    //初期宛先の場合は追加(削除)をしない。
    if(is_first_tocc_emp(emp_id)){
      return;
    }

    //ユーザーの追加
    var add_emp = new user_info(emp_id,emp_name,false);

    //別方に存在する場合は削除
    if(tocc == "TO"){
      <{if $is_cc_editable}>
        delete_sendto_list("CC",add_emp.emp_id);
      <{/if}>
    }
    else{
      <{if $is_to_editable}>
        delete_sendto_list("TO",add_emp.emp_id);
      <{/if}>
    }

    //追加
    m_sendto_list[tocc] = array_add(m_sendto_list[tocc],add_emp);

    //送信先一覧の重複除去
    single_target_sendto_list();

    //HTML反映処理
    update_sendto_html();
  }

  //--------------------------------------------------
  //送信先を追加します。(職員名簿専用)
  //--------------------------------------------------
  //tocc:'TO','CC'のいずれか。
  //emp_id_csv,emp_name_csv:", "で区切られたID、名前
  function add_recv_list_from_emplist(tocc,emp_id_list,emp_name_list)
  {
    var emp_ids = emp_id_list.split(", ");
    var emp_names = emp_name_list.split(", ");

    for(var i=0;i<emp_ids.length;i++)
    {
      var emp_id = emp_ids[i];
      var emp_name = emp_names[i];

      //初期宛先の場合は追加(削除)をしない。
      if(is_first_tocc_emp(emp_id)){
        continue;
      }

      //ユーザーの追加
      var add_emp = new user_info(emp_id,emp_name,false);

      //別方に存在する場合は削除
      if(tocc == "TO"){
        <{if $is_cc_editable}>
          delete_sendto_list("CC",add_emp.emp_id);
        <{/if}>
      }
      else{
        <{if $is_to_editable}>
          delete_sendto_list("TO",add_emp.emp_id);
        <{/if}>
      }

      //追加
      m_sendto_list[tocc] = array_add(m_sendto_list[tocc],add_emp);
    }

    //送信先一覧の重複除去
    single_target_sendto_list();

    //HTML反映処理
    update_sendto_html();
  }

  //--------------------------------------------------
  //送信先を追加します。(担当単位)
  //--------------------------------------------------
  //tocc:'TO','CC'のいずれか。
  //auth:'SM','RM','RA','SD','MD','HD'のいずれか。
  function add_recv_list_from_auth(tocc,auth)
  {
    //送信対象者の特定
    auth_user_array = m_auth_user_list[auth];
    if(auth_user_array.length == 0){
      alert("担当者が設定されていません。");
      return;
    }

    //送信先一覧に追加
    for(var i = 0; i < auth_user_array.length; i++){
      //初期宛先の場合は追加(削除)をしない。
      if(is_first_tocc_emp(auth_user_array[i].emp_id)){
        continue;
      }

      //別方に存在する場合は削除
      if(tocc == "TO"){
        <{if $is_cc_editable}>
          delete_sendto_list("CC",auth_user_array[i].emp_id);
        <{/if}>
      }
      else{
        <{if $is_to_editable}>
          delete_sendto_list("TO",auth_user_array[i].emp_id);
        <{/if}>
      }

      //追加
      m_sendto_list[tocc] = array_add(m_sendto_list[tocc],auth_user_array[i]);
    }

    //送信先一覧の重複除去
    single_target_sendto_list();

    //HTML反映処理
    update_sendto_html();
  }

  //--------------------------------------------------
  //送信先を削除します。
  //--------------------------------------------------
  function delete_sendto(tocc,emp_id,emp_name)
  {
    if(confirm("「" + emp_name + "」を宛先から削除します。よろしいですか？"))
    {
      delete_sendto_list(tocc,emp_id);

      //HTML反映処理
      update_sendto_html();
    }
  }


  //--------------------------------------------------
  //m_sendto_listより送信先を削除します。
  //--------------------------------------------------
  //※ここではHTML反映は行わないため、update_sendto_html()を実行する必要あり。
  function delete_sendto_list(tocc,emp_id)
  {
    var new_array = new Array();
    for(var i=0;i<m_sendto_list[tocc].length;i++)
    {
      if(emp_id != m_sendto_list[tocc][i].emp_id)
      {
        new_array = array_add(new_array,m_sendto_list[tocc][i]);
      }
    }
    m_sendto_list[tocc] = new_array;
  }

  //--------------------------------------------------
  //m_sendto_listの送信先の重複をなくします。
  //--------------------------------------------------
  //TOを優先的に残します。
  function single_target_sendto_list()
  {
    var new_sendto_list = new Array();
    new_sendto_list["TO"] = new Array();
    new_sendto_list["CC"] = new Array();
    var emp_id_csv = "";
    for(var i_tocc = 0; i_tocc < 2; i_tocc++)
    {
      var tocc2 = "TO";
      if(i_tocc == 1)
      {
        tocc2 = "CC";
      }

      if(m_sendto_list[tocc2].length != 0)
      {
        for(var i = 0; i < m_sendto_list[tocc2].length; i++)
        {
          var emp_id = m_sendto_list[tocc2][i].emp_id;
          if(emp_id_csv.indexOf(emp_id) == -1)
          {
            //重複していない。
            emp_id_csv = emp_id_csv + "," + emp_id;
            new_sendto_list[tocc2] = array_add(new_sendto_list[tocc2],m_sendto_list[tocc2][i]);
          }
        }
      }
    }
    m_sendto_list = new_sendto_list;
  }

  //--------------------------------------------------
  //m_sendto_listより宛先のHTMLを再生成します。
  //--------------------------------------------------
  function update_sendto_html()
  {
    //CC表示/非表示切り替え
    if(m_is_cc_usable)
    {
      var cc_obj = document.getElementById("mail_header_cc_tr");
      cc_obj.style.display = "";
      var non_cc_obj = document.getElementById("mail_header_non_cc_tr");
      non_cc_obj.style.display = "none";
    }
    else
    {
      var cc_obj = document.getElementById("mail_header_cc_tr");
      cc_obj.style.display = "none";
      var non_cc_obj = document.getElementById("mail_header_non_cc_tr");
      non_cc_obj.style.display = "";
    }

    //TO宛先HTMLを更新
    _update_sendto_html2("TO",document.getElementById("to_disp_area"),document.getElementById("to_emp_id_list"),document.getElementById("to_emp_name_list"),document.getElementById("to_emp_anonymous_list"));
    //CC宛先HTMLを更新
    _update_sendto_html2("CC",document.getElementById("cc_disp_area"),document.getElementById("cc_emp_id_list"),document.getElementById("cc_emp_name_list"),document.getElementById("cc_emp_anonymous_list"));
  }
  
  function _update_sendto_html2(tocc,disp_obj,send_hidden_id,send_hidden_name,send_hidden_anonymous)
  {
    if(m_sendto_list[tocc].length == 0){
      disp_obj.innerHTML = "";
      send_hidden_id.value = "";
      send_hidden_name.value = "";
      send_hidden_anonymous.value = "";
    }
    else{
      var disp_obj_html = "";
      var send_hidden_id_value = "";
      var send_hidden_name_value = "";
      var send_hidden_anonymous_value = "";
      for(var i=0;i<m_sendto_list[tocc].length;i++){
        var emp_id   = m_sendto_list[tocc][i].emp_id;
        var emp_name = m_sendto_list[tocc][i].emp_name;
        var emp_anonymous = m_sendto_list[tocc][i].anonymous;
        if(i!=0){
          disp_obj_html = disp_obj_html + ",";
          send_hidden_id_value = send_hidden_id_value + ",";
          send_hidden_name_value = send_hidden_name_value + ",";
          send_hidden_anonymous_value = send_hidden_anonymous_value + ",";
        }

        var editable = true;
        <{if !$is_to_editable}>
          if(tocc == "TO"){
            editable = false;
          }
        <{/if}>
        
        <{if !$is_cc_editable}>
          if(tocc == "CC"){
            editable = false;
          }
        <{/if}>

        //初期宛先の場合は削除できない。
        if(is_first_tocc_emp(emp_id)){
          editable = false;
        }

        if(editable){
  //<nobr>はOperaのバグのため使えない。
  //				disp_obj_html = disp_obj_html + "<nobr><a href=\"javascript:delete_sendto('" + tocc + "','" + emp_id + "','" + emp_name + "')\">" + emp_name + "</a></nobr>";
          disp_obj_html = disp_obj_html + "<a href=\"javascript:delete_sendto('" + tocc + "','" + emp_id + "','" + emp_name + "')\">" + emp_name + "</a>";
        }
        else{
  //				disp_obj_html = disp_obj_html + "<nobr>" + emp_name + "</nobr>";
          disp_obj_html = disp_obj_html + emp_name;
        }

        send_hidden_id_value = send_hidden_id_value + emp_id;
        send_hidden_name_value = send_hidden_name_value + emp_name;
        send_hidden_anonymous_value = send_hidden_anonymous_value + emp_anonymous;
      }
      disp_obj.innerHTML = disp_obj_html;
      send_hidden_id.value = send_hidden_id_value;
      send_hidden_name.value = send_hidden_name_value;
      send_hidden_anonymous.value = send_hidden_anonymous_value;
    }
  }

  //--------------------------------------------------
  //配列追加
  //--------------------------------------------------
  function array_add(array_obj,add_obj)
  {
    return array_obj.concat( new Array( add_obj ) );
  }

  //--------------------------------------------------
  //m_sendto_list形式データをデバッグします。
  //--------------------------------------------------
  function debug_sendto_list(sendto_list)
  {
    var str = "[TO]\n";
    if(sendto_list["TO"].length != 0)
    {
      for(var i=0;i<sendto_list["TO"].length;i++)
      {
        str = str + sendto_list["TO"][i].emp_id + "(" + sendto_list["TO"][i].emp_name + ")" + "\n";
      }
    }
    str = str + "[CC]\n";
    if(sendto_list["CC"].length != 0)
    {
      for(var i=0;i<sendto_list["CC"].length;i++)
      {
        str = str + sendto_list["CC"][i].emp_id + "(" + sendto_list["CC"][i].emp_name + ")" + "\n";
      }
    }
    alert(str);
  }
</script>
