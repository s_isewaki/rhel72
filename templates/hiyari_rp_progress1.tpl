<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix <{$INCIDENT_TITLE}> | 進捗管理</title>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript" src="js/incident_popup.js"></script>
  <script language="javascript">
    //===============================================
    //実行アイコン
    //===============================================
    // 返信
    function rp_action_return() {
        location.href = "hiyari_rp_mail_input.php?session=<{$session}>&mode=return&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>";
    }
    // 転送
    function rp_action_forward(){
        location.href = "hiyari_rp_mail_input.php?session=<{$session}>&mode=forward&mail_id=<{$mail_id}>&mail_id_mode=<{$mail_id_mode}>";
    }

    // 編集
    function rp_action_update() {
        location.href = "hiyari_easyinput.php?session=<{$session}>&gamen_mode=update&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>&report_id=<{$report_id}>";
    }

    // メッセージ表示
    function rp_action_message_disp() {
        location.href = "hiyari_rp_mail_disp.php?session=<{$session}>&mail_id_mode=<{$mail_id_mode}>&mail_id=<{$mail_id}>";
    }
    
    //===============================================
    // 進捗設定
    //===============================================
    function fncSetUseFlg() {
      document.mainform.action="hiyari_rp_progress.php?session=<{$session}>&tab_switch=regist_tab1";
      document.mainform.submit();
    }
      
    // インシレベルポップアップ
    function popup_inci_level(content, e) {
        popupDetailGreen(
            new Array(
                '<font size=\'3\' face=\'ＭＳ Ｐゴシック, Osaka\' class=\'j12\'><{$level_title}></font>', content
            ), 1000, 100, e
        );
    }
    
    // 匿名ポップアップ
    function name_popup_disp(real_name, e) {
        var content = "<font size=\\'3\\' face=\\'ＭＳ Ｐゴシック, Osaka\\' class=\\'j12\\'>" + real_name + "</font>";
        popupDetailGreen(
            new Array(
                "<font size=\'3\' face=\'ＭＳ Ｐゴシック, Osaka\' class=\'j12\'>" + "更新者" + "</font>", content
            ), 250, 50, e
        );
    }
    
    //===============================================
    // 進捗登録
    //===============================================
    function fncRegist(){
        if (chkDate() == true) {
            document.mainform.action="hiyari_rp_progress.php?session=<{$session}>&tab_switch=regist_tab2";
            document.mainform.submit();
        }
    }

    function chkDate() {
        // 年月日の歯抜けチェック
        for (i=0; i<5; i++) {
            regist_flg = "regist_flg" + i;
            if(document.mainform.elements[regist_flg] != null) {
                flg = document.mainform.elements[regist_flg].value;
                if (flg == "1") {
                    from_year = "from_year" + i;
                    from_mon = "from_mon" + i;
                    from_day = "from_day" + i;

                    val1 = document.mainform.elements[from_year].value;
                    val2 = document.mainform.elements[from_mon].value;
                    val3 = document.mainform.elements[from_day].value;

                    if(!hanukeChk(val1, val2, val3)) {
                        return false;
                    }
                }
            }
        }

        for (i=0; i<5; i++) {
            regist_flg = "regist_flg" + i;
            if(document.mainform.elements[regist_flg] != null) {
                flg = document.mainform.elements[regist_flg].value;
                if (flg == "1") {
                    to_year = "to_year" + i;
                    to_mon = "to_mon" + i;
                    to_day = "to_day" + i;

                    val1 = document.mainform.elements[to_year].value;
                    val2 = document.mainform.elements[to_mon].value;
                    val3 = document.mainform.elements[to_day].value;

                    if(!hanukeChk(val1, val2, val3)) {
                        return false;
                    }
                }
            }
        }
        
        // 年月日の前後チェック
        for (i=0; i<5; i++) {
            regist_flg = "regist_flg" + i;
            if(document.mainform.elements[regist_flg] != null) {
                flg = document.mainform.elements[regist_flg].value;
                if (flg == "1") {
                    from_year = "from_year" + i;
                    from_mon = "from_mon" + i;
                    from_day = "from_day" + i;
                    to_year = "to_year" + i;
                    to_mon = "to_mon" + i;
                    to_day = "to_day" + i;

                    from_1 = document.mainform.elements[from_year].value;
                    from_2 = document.mainform.elements[from_mon].value;
                    from_3 = document.mainform.elements[from_day].value;
                    to_1 = document.mainform.elements[to_year].value;
                    to_2 = document.mainform.elements[to_mon].value;
                    to_3 = document.mainform.elements[to_day].value;

                    ret = fromToChk(from_1, from_2, from_3, to_1, to_2, to_3)

                    if (ret == "NG_1") {
                        return false;
                    } else if(ret == "NG_2") {
                        return false;
                    }
                }
            }
        }

        // 日付妥当性チェック
        for (i=0; i<5; i++) {
            regist_flg = "regist_flg" + i;
            if(document.mainform.elements[regist_flg] != null) {
                flg = document.mainform.elements[regist_flg].value;
                if (flg == "1") {

                    from_year = "from_year" + i;
                    from_mon = "from_mon" + i;
                    from_day = "from_day" + i;

                    from_1 = document.mainform.elements[from_year].value;
                    from_2 = document.mainform.elements[from_mon].value;
                    from_3 = document.mainform.elements[from_day].value;

                    if(!dateCheck(from_1, from_2, from_3)) {
                        return false;
                    }
                }
            }
        }

        for (i=0; i<5; i++) {
            regist_flg = "regist_flg" + i;
            if(document.mainform.elements[regist_flg] != null) {
                flg = document.mainform.elements[regist_flg].value;
                if (flg == "1") {
                    to_year = "to_year" + i;
                    to_mon = "to_mon" + i;
                    to_day = "to_day" + i;

                    to_1 = document.mainform.elements[to_year].value;
                    to_2 = document.mainform.elements[to_mon].value;
                    to_3 = document.mainform.elements[to_day].value;

                    if(!dateCheck(to_1, to_2, to_3)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    // 歯抜けチェック
    function hanukeChk(val1, val2, val3) {
        if(val1 == "-" && val2 == "-" && val3 == "-") {
            return true;
        } else if(val1 != "-" && val2 != "-" && val3 != "-") {
            return true;
        } else {
            alert('年月日の入力漏れがあります。');
            return false;
        }
    }

    // 日付前後チェック
    function fromToChk(from_1, from_2, from_3, to_1, to_2, to_3) {
        from_date = from_1 + from_2 + from_3;
        to_date = to_1 + to_2 + to_3;
        
        if(isNaN(from_date)) {
            if(!isNaN(to_date)) {
                alert('受付日付を入力してください。');
                return "NG_1";
            }
        }

        if (from_date > to_date) {
            if (!isNaN(from_date)) {
                if(!isNaN(to_date)) {
                    alert('期間の前後が合っていません。');
                    return "NG_2";
                }
            }
        }

        return "OK";
    }

    // 日付妥当性チェック
    function dateCheck(year, month, day){
        if (year == "-" && month == "-" && day == "-") {
            return true;
        }

        year = parseInt(year,10);
        month = parseInt(month,10);
        day = parseInt(day,10);

        //閏年
        if ((month == 2) && YearChk(year)) {
            month=13;
        }

        //末日チェック
        if (LastDayChk(month,day)) {
            return true;
        }
        else {
            alert('日付が正しくありません。');
            return false;
        }
    }

    //末日チェック
    function LastDayChk(in_Month,in_Day){
      lastDay = new Array(31,28,31,30,31,30,31,31,30,31,30,31,29);
      if (lastDay[in_Month-1] >= in_Day) {
        return true;
      }
       else {
        return false;
      }
    }

    //年チェック
    function YearChk(in_Year){
      if ((in_Year % 4) == 0 && ((in_Year % 100) != 0 || (in_Year % 400))) {
        return true;
      }
      return false;
    }

    function setToday(kbn, no) {
        if(kbn == "from") {
            year = "from_year" + no;
            mon = "from_mon" + no;
            day = "from_day" + no;
        } else if(kbn == "to") {
            year = "to_year" + no;
            mon = "to_mon" + no;
            day = "to_day" + no;
        } else if(kbn == "eval") {
            year = "yearEval" + no;
            mon = "monEval" + no;
            day = "dayEval" + no;
        }

        for(i=0; i<document.mainform.elements[year].length; i++) {
            val = document.mainform.elements[year].options[i].value
            if(val == '<{$this_year}>') {
                document.mainform.elements[year].options[i].selected = true;
            }
        }

        for(i=0; i<document.mainform.elements[mon].length; i++) {
            val = document.mainform.elements[mon].options[i].value
            if(val == '<{$this_month}>') {
                document.mainform.elements[mon].options[i].selected = true;
            }
        }

        for(i=0; i<document.mainform.elements[day].length; i++) {
            val = document.mainform.elements[day].options[i].value
            if(val == '<{$this_day}>') {
                document.mainform.elements[day].options[i].selected = true;
            }
        }
    }

    function clearDate(kbn, no) {
        if(kbn == "from") {
            year = "from_year" + no;
            mon = "from_mon" + no;
            day = "from_day" + no;
        } else if(kbn == "to") {
            year = "to_year" + no;
            mon = "to_mon" + no;
            day = "to_day" + no;
        } else if(kbn == "eval") {
            year = "yearEval" + no;
            mon = "monEval" + no;
            day = "dayEval" + no;
        }

        for(i=0; i<document.mainform.elements[year].length; i++) {
            val = document.mainform.elements[year].options[i].value
            if(val == '-') {
                document.mainform.elements[year].options[i].selected = true;
            }
        }

        for(i=0; i<document.mainform.elements[mon].length; i++) {
            val = document.mainform.elements[mon].options[i].value
            if(val == '-') {
                document.mainform.elements[mon].options[i].selected = true;
            }
        }

        for(i=0; i<document.mainform.elements[day].length; i++) {
            val = document.mainform.elements[day].options[i].value
            if(val == '-') {
                document.mainform.elements[day].options[i].selected = true;
            }
        }
    }
  </script>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <style type="text/css">
    .list_2 {border-collapse:collapse;}
    .list_2 td {border:#999999 solid 1px;}
    .desc td {border-style:none;}

    col.komoku0  { width: 15%;}
    col.komoku1  { width: 10%;}
    col.komoku2  { width: 30%;}
    col.komoku3  { width: 30%;}
    col.komoku4  { width: *;}
  </style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
  <form name="mainform" action="hiyari_progress_registration.php" method="post">
    <input type="hidden" name="emp_id" value="<{$emp_id}>">
    <input type="hidden" name="report_id" value="<{$report_id}>">
    <input type="hidden" name="mail_id" value="<{$mail_id}>">
    <input type="hidden" name="mail_id_mode" value="<{$mail_id_mode}>">    
    <input type="hidden" name="tab_switch" value="<{$tab_switch}>">    
    <{if $tab_switch == "PRGRES_TAB_2"}>
      <input type="hidden" name="sm_auth_flg" value="<{$sm_auth_flg}>">
    <{/if}>
    
    <{* ヘッダー *}>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr height="32" bgcolor="#35B341">
        <td class="spacing">
          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>進捗管理</b></font>
        </td>
        <td>&nbsp;</td>
        <td width="10">&nbsp;</td>
        <td width="32" align="center">
          <a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a>
        </td>
      </tr>
    </table>
    <img src="img/spacer.gif" width="10" height="10" alt=""><br>
    
    <{* 実行アイコン *}>
    <table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <{if $mail_id != ""}>
          <{if $mail_id_mode == "recv" && $arr_btn_flg.return_btn_show_flg == "t"}>
            <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
            <td align="center"><img src="img/hiyari_rp_return.gif" width="32" height="29" style="cursor: pointer;" onclick="rp_action_return();"></td>
          <{/if}>
          <{if $arr_btn_flg.forward_btn_show_flg == "t"}>
            <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
            <td align="center"><img src="img/hiyari_rp_forward.gif" width="32" height="29" style="cursor: pointer;" onclick="rp_action_forward();"></td>
          <{/if}>
        <{/if}>
        
        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
        <td align="center"><img src="img/hiyari_rp_update.gif" width="36" height="30" style="cursor: pointer;" onclick="rp_action_update();"></td>
        
        <{if $mail_id != ""}>
          <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
          <td align="center"><img src="img/hiyari_rp_mail_disp.gif" height="30" style="cursor: pointer;" onclick="rp_action_message_disp();"></td>
        <{/if}>
      </tr>
      <tr>
        <{if $mail_id != ""}>
          <{if $mail_id_mode == "recv" && $arr_btn_flg.return_btn_show_flg == "t"}>
            <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
            <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">返信</font></td>
          <{/if}>
          <{if $arr_btn_flg.forward_btn_show_flg == "t"}>
            <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
            <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">転送</font></td>
          <{/if}>
        <{/if}>
        
        <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">編集</font></td>
        
        <{if $mail_id != ""}>
          <td><img src="img/spacer.gif" width="15" height="1" alt=""></td>
          <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#13781D">表示</font></td>
        <{/if}>
      </tr>
    </table>
    
    <{* 設定 *}>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
              <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
              <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
            </tr>
            <tr>
              <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
              <td bgcolor="#F5FFE5">
                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                  <{*　表題　*}>
                  <tr>
                    <td>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr height="10">
                            <td><font size="4" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b>【表題】&nbsp;<{$report_title|escape}></b></font></td>
                        </tr>
                      </table>
                    </td>
                  </tr>                  
                  <tr>
                    <td>
                      <img src="img/spacer.gif" width="1" height="1" alt="">
                    </td>
                  </tr>
                  
                  <{********************************************************************************
                  進捗設定画面 
                  ********************************************************************************}>
                  <{if $tab_switch == "PRGRES_TAB_1"}>
                    <{*　タブ　*}>
                    <tr width="100%">
                      <td>
                        <table border="0" cellspacing="0" cellpadding="0">
                          <tr>
                              <td><img src="img/menu_left.gif" width="5" height="22"></td>
                              <td width="90" align="center" bgcolor="#35B341">
                                  <a href="hiyari_rp_progress.php?session=<{$session}>&amp;tab_switch=PRGRES_TAB_1&amp;report_id=<{$report_id}>&amp;mail_id=<{$mail_id}>&amp;mail_id_mode=<{$mail_id_mode}>">
                                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><nobr><b>進捗設定</b></nobr></font>
                                  </a>
                              </td>
                              <td><img src="img/menu_right.gif" width="5" height="22"></td>

                              <td><img src="img/spacer.gif" width="3" height="1" alt=""></td>

                              <td><img src="img/menu_left_40.gif" width="5" height="22"></td>
                              <td width="90" align="center" bgcolor="#AEE1B3">
                                  <a href="hiyari_rp_progress.php?session=<{$session}>&amp;tab_switch=PRGRES_TAB_2&amp;report_id=<{$report_id}>&amp;mail_id=<{$mail_id}>&amp;mail_id_mode=<{$mail_id_mode}>">
                                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr><b>進捗登録</b></nobr></font>
                                  </a>
                              </td>
                              <td><img src="img/menu_right_40.gif" width="5" height="22"></td>
                          </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr valign="top"><td bgcolor="#35B341"><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
                        </table>
                      </td>
                    </tr>
                  
                    <tr>
                      <td><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                    </tr>
                  
                    <{*患者影響レベル*}>
                    <tr>
                      <td>
                        <table width="600" border="0" cellspacing="0" cellpadding="2"  class="list_2">
                          <tr height="22">
                            <td bgcolor="#DFFFDC" width="50%">
                              <span onmousemove="popup_inci_level('<{$level_detail}>', event);" onmouseout="closeDetail();" style="cursor:pointer;">
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="blue">
                                  <B><{$level_title}></B>
                                  <img src="./img/hiyari_report_help_icon.gif" alt="説明" border="0" style="vertical-align: text-bottom;" />
                                </font>
                              </span>
                            </td>
                            
                            <td bgcolor="#FFFFFF" width="50%">
                              <select name="inci_lvl">
                                <option value=""></option>
                                <{foreach from=$level_list item=i}>
                                  <option value="<{$i.key}>" <{$i.selected}>>
                                    <{$i.val}>
                                  </option>
                                <{/foreach}>
                              </select>
                            </td>
                          </tr>
                            
                          <{if isset($level_rireki_info)}>
                            <tr>
                              <td colspan=2 bgcolor="#FFFFFF" align="center">
                                <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="3" class="list_2">
                                  <{foreach from=$level_rireki_info item=info}>
                                    <tr>
                                      <td align="center" bgcolor="#FFFBE7" height="22">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$info.index_name}></font>
                                      </td>
                                      <td align="center" bgcolor="#FFFFFF">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$info.date}></font>
                                      </td>
                                      <td align="center" bgcolor="#FFFFFF">
                                        <{if isset($info.real_name)}>
                                          <span onmousemove="name_popup_disp('<{$info.real_name}>', event);" onmouseout="closeDetail();" style="cursor:pointer;">
                                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="blue"><B><{$info.disp_emp_name}></B></font>
                                          </span>
                                        <{else}>  
                                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><{$info.disp_emp_name}></font>
                                        <{/if}>  
                                      </td>
                                      <td align="left" bgcolor="#FFFFFF" width="50%">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$info.level_name}></font>
                                      </td>
                                    </tr>
                                  <{/foreach}>
                                </table>
                              </td>
                            </tr>
                          <{/if}>
                        </table>
                      </td>
                    </tr>
                    
                    <{*進捗設定*}>
                    <tr>
                      <td>
                        <table width="600" height="100%" border="0" cellspacing="0" cellpadding="3" class="list_2">
                          <tr>
                            <td width="50%" align="center" bgcolor="#FFDDFD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">工程(担当)</font></td>
                            <td width="50%" align="center" bgcolor="#FFDDFD">
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設定</font>
                            </td>
                          </tr>
                          <{foreach from=$setting_info item=i}>
                            <tr>
                              <td align="left" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$i.auth_name}></font></td>
                              <td align="center" bgcolor="#FFFFFF">
                                  <input type="checkbox" name="<{$i.auth}>" value="t" <{if $i.use_flg == "t"}> checked<{/if}>/>
                              </td>
                            </tr>
                          <{/foreach}>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <table width="600" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td align="right">
                              <input type="button" value="登録" onclick="fncSetUseFlg()">
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>                  
                    
                  <{********************************************************************************
                  進捗登録画面 
                  ********************************************************************************}>
                  <{else}>
                    <tr width="100%">
                      <td>
                        <table border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <{if $sm_auth_flg == "1"}>
                              <td><img src="img/menu_left_40.gif" width="5" height="22"></td>
                              <td width="90" align="center" bgcolor="#AEE1B3">
                                <a href="hiyari_rp_progress.php?session=<{$session}>&amp;tab_switch=PRGRES_TAB_1&amp;report_id=<{$report_id}>&amp;mail_id=<{$mail_id}>&amp;mail_id_mode=<{$mail_id_mode}>">
                                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr><b>進捗設定</b></nobr></font>
                                </a>
                              </td>
                              <td><img src="img/menu_right_40.gif" width="5" height="22"></td>
                              <td><img src="img/spacer.gif" width="3" height="1" alt=""></td>
                            <{/if}>
                            <td><img src="img/menu_left.gif" width="5" height="22"></td>
                            <td width="90" align="center" bgcolor="#35B341">
                              <a href="hiyari_rp_progress.php?session=<{$session}>&amp;tab_switch=PRGRES_TAB_2&amp;report_id=<{$report_id}>&amp;mail_id=<{$mail_id}>&amp;mail_id_mode=<{$mail_id_mode}>">
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><nobr><b>進捗登録</b></nobr></font>
                              </a>
                            </td>
                            <td><img src="img/menu_right.gif" width="5" height="22"></td>
                          </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr valign="top"><td bgcolor="#35B341"><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
                        </table>
                      </td>
                    </tr>
                  
                    <tr>
                      <td><img src="img/spacer.gif" width="1" height="1" alt=""></td>
                    </tr>

                    <tr>
                      <td>
                        <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="3" class="list_2">
                          <col class="komoku0" span="1">
                          <col class="komoku1" span="1">
                          <col class="komoku2" span="1">
                          <col class="komoku3" span="1">
                          <col class="komoku4" span="1">

                          <tr>
                            <td align="center" bgcolor="#FFDDFD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">工程(担当)</font></td>
                            <td align="center" bgcolor="#FFDDFD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">あなたの<BR>権限</font></td>
                            <td align="center" bgcolor="#FFDDFD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受付日付</font></td>
                            <td align="center" bgcolor="#FFDDFD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">確認日付</font></td>
                            <td align="center" bgcolor="#FFDDFD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新者</font></td>
                          </tr>

                          <{foreach name=n from=$progress_info item=i}>
                            <tr>
                              <td align="left" bgcolor="#DFFFDC" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$i.auth_name}></font></td>
                              <td align="center" bgcolor="#FFFFFF" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$i.your_auth}></font></td>
                              <{foreach from=$i.dates key=date_type item=date}>
                                <td align="center" bgcolor="#FFFFFF" nowrap>
                                  <select name="<{$date_type}>_year<{$smarty.foreach.n.index}>" <{if $date.is_disabled}>style="background-color:#cccccc;" disabled<{/if}>>
                                    <option value="-"></option>
                                    <{foreach from=$date.years item=y}>
                                      <option value="<{$y.year}>" <{if $y.is_selected}>selected<{/if}>><{$y.year}></option>
                                    <{/foreach}>
                                  </select>
                                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font>
                                  
                                  <select name="<{$date_type}>_mon<{$smarty.foreach.n.index}>" <{if $date.is_disabled}>style="background-color:#cccccc;" disabled<{/if}>>
                                    <option value="-"></option>
                                    <{foreach from=$date.months item=m}>
                                      <option value="<{$m.val}>" <{if $m.is_selected}>selected<{/if}>><{$m.month}></option>
                                    <{/foreach}>
                                  </select>
                                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font>
                                  
                                  <select name="<{$date_type}>_day<{$smarty.foreach.n.index}>" <{if $date.is_disabled}>style="background-color:#cccccc;" disabled<{/if}>>
                                    <option value="-"></option>
                                    <{foreach from=$date.days item=d}>
                                      <option value="<{$d.val}>" <{if $d.is_selected}>selected<{/if}>><{$d.day}></option>
                                    <{/foreach}>
                                  </select>
                                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font>
                                  
                                  <input type="button" value="今日" onclick="setToday('<{$date_type}>','<{$smarty.foreach.n.index}>');" <{if $date.is_disabled}>disabled<{/if}>>
                                  <input type="button" value="クリア" onclick="clearDate('<{$date_type}>','<{$smarty.foreach.n.index}>');" <{if $date.is_disabled}>disabled<{/if}>>
                                </td>
                              <{/foreach}>
                              <td align="center" bgcolor="#FFFFFF" nowrap>
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$i.updater}></font>
                              </td>
                            </tr>
                            <input type="hidden" name="regist_flg<{$smarty.foreach.n.index}>" value="<{$i.regist_flg}>">
                            <input type="hidden" name="auth<{$smarty.foreach.n.index}>" value="<{$i.auth}>">
                          <{/foreach}>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td>
                              <input type="checkbox" name="edit_lock_flg" value="t" <{if $edit_lock_flg == "t"}>checked<{/if}> <{if $sm_auth_flg == '0'}>disabled<{/if}>/>
                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$sm_short_name}>以外編集ロック</font>
                            </td>
                            <td align="right">
                              <input type="button" value="登録" onclick="fncRegist();">
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  <{/if}>  
                  
                </table>
              </td>
              <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
            </tr>
            <tr>
              <td><img src="img/r_3.gif" width="10" height="10"></td>
              <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
              <td><img src="img/r_4.gif" width="10" height="10"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </form>
</body>
</html>
