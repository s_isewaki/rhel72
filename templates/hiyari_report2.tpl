<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
  <meta http-equiv="X-UA-Compatible" content="IE=8" />
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$page_title}></title>
	<meta http-equiv="imagetoolbar" content="no" />
	<link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/smoothness/jquery-ui-1.10.3.custom.min.css">
  <link type="text/css" rel="stylesheet" media="all" href="css/report.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/date_search.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/pagination.css" />
  <style tyle="text/css">
  body {padding-bottom: 50px;}
  </style>
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/libs.js"></script>
	<script type="text/javascript" src="js/jquery/jquery-ui-1.10.2.custom.min.js"></script>  
  <script type="text/javascript" src="js/jquery/jquery.ui.datepicker-ja.min.js" charset="utf-8"></script>
  <script type="text/javascript">
    jQuery.noConflict();
    var j$ = jQuery;
    
    var session = "<{$session}>";
    var is_report_classification_usable = false;
    <{if $file == "hiyari_rpt_report_classification.php"}>
        var folder_id = "<{$folder_id}>";
        var is_report_classification_usable = '<{$is_report_classification_usable}>';
    <{/if}>    
    
    
    jQuery(function($){
        //件名の幅調節
        var tbody = $('#main_list').find('tbody');
        var width = tbody.width();
        $('#main_list th').each(function(){
            if (!$(this).hasClass('subject')){
                width -= $(this).width();
            }
        });
        <{if $file == "hiyari_rpt_recv_torey.php"}>
            tbody.find('td.subject').find('span').width(width - 40);
        <{else}>
            tbody.find('td.subject').find('span').width(width - 10);
        <{/if}>
    })
    j$(document).ready(function() {
		$('#all_select').click(function(){
			allCheckOnOff();
		});
	});
	
    function allCheckOnOff()
    {
        
        var list_select_objs = document.getElementsByName("list_select[]");
        var th_span_obj = document.getElementById("all_select");
               
       $('[id^="span_wrap"]').each(function(){
         if(th_span_obj.checked){
              $(this).attr('class','checkbox_wrap');
         }else{
             $(this).attr('class','checkbox_wrap checked');
         }                  
     });
       
        $(list_select_objs).prop('checked', !th_span_obj.checked);
        th_span_obj.checked = !th_span_obj.checked;         
    }    
  </script>
  <script type="text/javascript" src="js/hiyari_report.js"></script>
  <script type="text/javascript" src="js/hiyari_report2.js"></script>
  <script type="text/javascript" src="js/hiyari_calendar.js"></script>
  <{include file="hiyari_post_select_js.tpl"}>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->  
</head>

<body id="top">
  <{*ヘッダー*}>
  <{include file="hiyari_header2.tpl"}>

	<div id="content" class="clearfix <{$tab_info.total_width_class}>">
	<form name="open_report" method="post" action="hiyari_news_register.php">
	  <input type="hidden" name="session" value="<{$session}>">
      <input type="hidden" name="open_content" value="">
	</form>
	
    <!-- レポーティング本体 START -->
    <form name="list_form" method="post" action="<{$file}>">
      <input type="hidden" name="session" value="<{$session}>">
      <input type="hidden" name="search_evaluation_date_post" value="<{$search_evaluation_date}>">
      <input type="hidden" name="is_postback" value="true">
      <input type="hidden" name="mode" value="">
      
      <input type="hidden" name="search_toggle" value="<{$search_toggle}>">   
      
      <!-- 期間指定データ START -->
      <input type="hidden" name="search_date_mode" value="<{$search_date_mode}>">
      <input type="hidden" name="search_date_year" value="<{$search_date_year}>">
      <input type="hidden" name="search_date_month" value="<{$search_date_month}>">
      <!-- 期間指定データ END -->
      
      <input type="hidden" name="sort_item" value="<{$sort_item}>">
      <input type="hidden" name="sort_div" value="<{$sort_div}>">    
      <input type="hidden" name="page" value="<{$page}>">
      
      <{if $file == "hiyari_rpt_report_classification.php"}>
        <!-- フォルダ分類関連 START -->
        <input type="hidden" name="folder_id" value="<{$folder_id}>">
        <input type="hidden" name="change_report_classification_report_id" value="">
        <input type="hidden" name="change_report_classification_folder_id" value="">            
        <!-- フォルダ分類関連 END -->
        
        <!-- Excel出力関連 START -->
        <input type="hidden" name="report_id" value="">
        <input type="hidden" name="report_id_list" value="<{$excel_target_report_id_list}>">
        <!-- Excel出力関連 END -->      
      <{/if}>   
      
      <{if $file == "hiyari_rpt_shitagaki_db.php"}>
        <input type="hidden" name="disp_mode" value="<{$disp_mode}>">
      <{/if}>
      
      <{if $file != "hiyari_rpt_shitagaki_db.php"}>
        <input type="hidden" id="is_report_db_update_able" name="is_report_db_update_able" value="<{if $is_report_db_update_able}>1<{else}>0<{/if}>">
        <input type="hidden" id="is_update" name="is_update" value="<{if $is_update}>1<{else}>0<{/if}>">
        <input type="hidden" id="is_report_file_page" name="is_report_file_page" value="<{if $file=='hiyari_rpt_report_classification.php'}>1<{else}>0<{/if}>">      
      <{/if}>
    
      <div id="sub" class="clearfix">
        <{* トレイ *}>
        <div id="sub_menu" class="color01 radius">
          <h2><img src="img/side_nav_txt01.gif" alt="MENU" /></h2>
          <ul>
            <{foreach from=$torey_list item=torey}>
              <li class="<{$torey.class}><{if $torey.is_active}> active<{/if}>">
                <a href="<{$torey.link}>?session=<{$session}>"><{$torey.name}></a>
              </li>
            <{/foreach}>
          </ul>
        </div>
        
        <{* 日付条件 *}>
        <{if $search_date_mode != "non"}>
          <{include file="hiyari_date_search2.tpl"}>
        <{/if}>
                  
        <!-- 分類フォルダ START -->
        <{if $file == "hiyari_rpt_report_classification.php"}>
          <div id="sub_bunrui" class="color01 radius">
            <h2><img src="img/side_nav_txt02.gif" alt="分類ラベル" /></h2>
            <!-- フォルダ一覧 -->
            <div id="b_scrollbar">
              <div class="scrollbar"><div class="track radius"><div class="thumb radius"><div class="end"></div></div></div></div>
              <div class="viewport inner radius_mini">
                <div class="overview">
                  <ul class="radius_mini">
                    <{foreach from=$folders item=fld}>
                      <li id="<{$fld.id}>" class="<{if $fld.is_selected}>selected<{else}>droppable_label<{/if}><{if !$fld.is_common}> person<{/if}>">
                        <span class="fld_checkbox">
                          <{if $fld.has_checkbox}>
                            <span class="checkbox_wrap">
                              <input type="checkbox" value="<{$fld.id}>" id="folder_select[]" class="checkbox" name="folder_select[]">
                            </span>
                          <{/if}>
                        </span>
                        <span class="fld_name">
                          <a href="javascript:rp_action_change_classification_folder('<{$fld.id}>');">
                            <{$fld.name}>
                            <{if !$fld.has_checkbox or ($auto_group_flg == "1" || $fld.is_selected)}>
                              (<{$fld.count}>)
                            <{/if}>
                          </a>
                        </span>
                      </li>
                    <{/foreach}>
                  </ul>
                </div>
              </div>
            </div>
            <!-- 分類オペレーション -->
            <ul class="sub_nav_bottom clearfix">
              <li class="sub_nav_05"><a href="#" onclick="rp_action_new_classification_folder();">作成</a></li>
              <li class="sub_nav_06"><a href="#" onclick="rp_action_update_classification_folder();">変更</a></li>
              <li class="sub_nav_07"><a href="#" onclick="rp_action_delete_classification_folder();">削除</a></li>
            </ul>
          </div>
        <{/if}>
        <!-- 分類フォルダ END -->
      </div>
        
      <div id="main" class="<{$tab_info.main_width_class}>">
        <!-- 検索領域 START -->
        <div id="search_area" class="color02 radius">
          <table class="v_title">
            <tr>
              <th width="130">
                <{$search_name}>
              </th>
              <td>      
                <table>
                  <tr>
                    <th>報告部署</th>
                    <td>
                      <{if $file=='hiyari_rpt_report_classification.php' || $file=='hiyari_analysis_search.php'}>
                        <label for="search_emp_all">
                          <span class="checkbox_wrap<{if $search_emp_all}> checked<{/if}>">
                            <input type="checkbox" value="true" id="search_emp_all" class="checkbox" name="search_emp_all" onClick="hiyari_search_emp_all(this.checked, <{$post_select_data.class_cnt}>)" <{if $search_emp_all}>checked<{/if}>>
                          </span>
                          兼務部署の全てを表示<br>
                        </label>
                      <{/if}>
                      <select id="search_emp_class" name="search_emp_class" onchange="setAtrbOptions('search_emp_post');"></select>
                      &nbsp;&nbsp;&gt;&nbsp;&nbsp;
                      <select id="search_emp_attribute" name="search_emp_attribute" onchange="setDeptOptions('search_emp_post');"></select>
                      &nbsp;&nbsp;&gt;&nbsp;&nbsp;
                      <select id="search_emp_dept" name="search_emp_dept" onchange="setRoomOptions('search_emp_post');"></select>
                      <{if $post_select_data.class_cnt == 4}>
                        &nbsp;&nbsp;&gt;&nbsp;&nbsp;
                        <select id="search_emp_room" name="search_emp_room"></select>
                      <{/if}>                
                    </td>
                  </tr>
                  <tr>
                    <th>氏名</th>
                    <td class="input_str">
                      <input type="text" name="search_emp_name" id="search_emp_name" value="<{$search_emp_name|escape}>" maxlength="40">
                    </td>
                  </tr>
                  <tr>
                    <th>職種</th>
                    <td>
                      <select name="search_emp_job" id="search_emp_job">
                        <option value=""></option>
                        <{foreach from=$job_list key=k item=job_name}>
                          <option value="<{$k}>" <{if $k == $sel_job_id}> selected<{/if}>>
                            <{$job_name}>
                          </option>
                        <{/foreach}>
                      </select>
                    </td>
                  </tr>
                </table>
              </td>      
            </tr>
            <{if $file != "hiyari_rpt_shitagaki_db.php"}>
              <tr>
                <th>患者</th>
                <td>
                  <table>
                    <tr>
                      <th>患者ＩＤ</th>
                      <td class="input_no">
                        <input type="text" name="search_patient_id" id="search_patient_id" value="<{$search_patient_id|escape}>" maxlength="30">
                      </td>
                    </tr>
                    <tr>
                      <th>患者年齢</th>
                      <td>
                        <select name="search_patient_year_from" id="search_patient_year_from" onchange="check_patient_year('from');">
                          <option value=""></option>
                          <{foreach from=$patient_year key=k item=value}>
                            <option value="<{$k}>" <{if $k == $search_patient_year_from}> selected<{/if}>>
                              <{$value}>
                            </option>
                          <{/foreach}>
                        </select>
                        〜
                        <select name="search_patient_year_to" id="search_patient_year_to" onchange="check_patient_year('to');">
                          <option value=""></option>
                          <{foreach from=$patient_year key=k item=value}>
                            <option value="<{$k}>" <{if $k == $search_patient_year_to}> selected<{/if}>>
                              <{$value}>
                            </option>
                          <{/foreach}>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <th>氏名</th>
                      <td class="input_str">
                        <input type="text" name="search_patient_name" id="search_patient_name" value="<{$search_patient_name|escape}>" maxlength="40"> 
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <script type="text/javascript">
                if(document.getElementById('search_patient_year_from').value == '900'){
                  document.getElementById('search_patient_year_to').disabled = true;
                }
                
                function check_patient_year(div)
                {
                  if(div == 'from'){
                      var year_from = document.getElementById('search_patient_year_from').value;
                      if(year_from == '900'){
                          document.getElementById('search_patient_year_to').value = '';
                          document.getElementById('search_patient_year_to').disabled = true;
                      }
                      else{
                          document.getElementById('search_patient_year_to').disabled = false;
                      }
                  }
                  else if(div == 'to'){
                      var year_to = document.getElementById('search_patient_year_to').value;
                      if(year_to == '900'){
                          document.getElementById('search_patient_year_from').value = '900';
                          document.getElementById('search_patient_year_to').value = '';
                          document.getElementById('search_patient_year_to').disabled = true;
                      }
                      else{
                          document.getElementById('search_patient_year_to').disabled = false;
                      }
                  }
                }
              </script>
            <{/if}>
            
            <{if $file == "hiyari_rpt_report_classification.php" || $file == "hiyari_analysis_search.php"}>
              <tr>
                <th>事案番号</th>
                <td class="input_no">
                  <input type="text" value="<{$search_report_no}>" id="search_report_no" name="search_report_no" maxlength="11">
                </td>
              </tr>
            
             <{if $predetermined_eval == 't'}>
              <tr>
               
                <th>評価予定日</th>
                <td>
                  <select id="search_evaluation_date" name="search_evaluation_date">
                    <option value=""></option>
                    <option value="today"    <{if $search_evaluation_date == "today"   }>selected<{/if}> >本日</option>
                    <option value="tomorrow" <{if $search_evaluation_date == "tomorrow"}>selected<{/if}> >翌日</option>
                    <option value="1week"    <{if $search_evaluation_date == "1week"   }>selected<{/if}> >1週間以内</option>
                    <option value="2week"    <{if $search_evaluation_date == "2week"   }>selected<{/if}> >2週間以内</option>
                  </select>
                </td>
              </tr>
              <{/if}>
            <{/if}>

            <{if $file == "hiyari_rpt_recv_torey.php" || $file == "hiyari_rpt_send_torey.php"}>
              <tr>
                <th>事案番号</th>
                <td class="input_no">
                    <input type="text" value="<{$search_report_no}>" id="search_report_no" name="search_report_no" maxlength="11">
                </td>
              </tr>
            <{/if}>
            
            <{if $file == "hiyari_rpt_recv_torey.php" || $file == "hiyari_rpt_report_classification.php" || $file == "hiyari_analysis_search.php"}>
              <tr>
                <th>表題へのキーワード</th>
                <td>
                  <input type="text" value="<{$search_title}>" id="search_title" name="search_title" size="55">
                  空白で区切られたキーワードを
                  <label for="title_and">
                    <input type="radio" name="title_and_or_div" id="title_and" value="1" <{if $title_and_or_div == 1 || $title_and_or_div == ""}>checked<{/if}>>
                    AND
                  </label>
                  <label for="title_or">
                    <input type="radio" name="title_and_or_div" id="title_or" value="2" <{if $title_and_or_div == 2}>checked<{/if}>>
                    OR検索
                  </label>
                </td>
              </tr>
              <tr>
                <th>インシデントの内容へのキーワード</th>
                <td>
                  <input type="text" value="<{$search_incident}>" id="search_incident" name="search_incident" size="55">
                  空白で区切られたキーワードを
                  <label for="incident_and">
                    <input type="radio" name="incident_and_or_div" id="incident_and" value="1" <{if $incident_and_or_div == 1 || $incident_and_or_div == ""}>checked<{/if}>>
                    AND
                  </label>
                  <label for="incident_or">
                    <input type="radio" name="incident_and_or_div" id="incident_or" value="2" <{if $incident_and_or_div == 2}>checked<{/if}>>
                    OR検索
                  </label>
                </td>
              </tr>
            <{/if}>
            
            <tr>
              <th class="<{if $file == 'hiyari_rpt_shitagaki_db.php'}>none<{/if}>">
                <{$search_torey_date_in_search_area_title_name}>
              </th>
              <td class="input_date<{if $file == 'hiyari_rpt_shitagaki_db.php'}> none<{/if}>">
                開始年月日
                <input type="text" id="search_torey_date_in_search_area_start" class="calendar_text" name="search_torey_date_in_search_area_start" value="<{$search_torey_date_in_search_area_start}>" maxlength="10">
                <a class="calendar_btn" href="#">カレンダー</a>
                終了年月日
                <input type="text" id="search_torey_date_in_search_area_end" class="calendar_text" name="search_torey_date_in_search_area_end" value="<{$search_torey_date_in_search_area_end}>" maxlength="10">
                <a class="calendar_btn" href="#">カレンダー</a>
              </td>
            </tr>

            <{if $file != "hiyari_rpt_shitagaki_db.php"}>
              <tr>
                <th class="none">発生日</th>
                <td class="none input_date">
                  開始年月日
                  <input type="text" id="search_incident_date_start" class="calendar_text" name="search_incident_date_start" value="<{$search_incident_date_start}>" maxlength="10">
                  <a class="calendar_btn" href="#">カレンダー</a>
                  終了年月日
                  <input type="text" id="search_incident_date_end" class="calendar_text" name="search_incident_date_end" value="<{$search_incident_date_end}>" maxlength="10">
                  <a class="calendar_btn" href="#">カレンダー</a>
                </td>
              </tr>
            <{/if}>
          </table>
          <div id="search_action" class="clearfix">
            <div id="search_btn">
              <input type="button" class="button radius_mini" value="検索" onclick="search_torey('<{$file}>', '<{$search_torey_date_in_search_area_title_name}>');">
              <{if $file == "hiyari_rpt_report_classification.php"}>
                <label for="auto_group_flg">
                  <span class="checkbox_wrap<{if $auto_group_flg == '1'}> checked<{/if}>">
                    <input type="checkbox" id="auto_group_flg" class="checkbox" name="auto_group_flg" value="1" <{if $auto_group_flg == "1"}>checked<{/if}>>
                  </span>
                  自動分類する
                </label>
              <{/if}>
            </div>
            <input type="button" id="close_search_btn" class="button radius_mini" value="閉じる" onclick="show_search_input_page('<{$file}>');">
          </div>
          <script type="text/javascript">
            j$("#search_area").css("display", ("<{$search_toggle}>" == "open") ? "" : "none");
            set_post_select_box_obj_key("search_emp_post","search_emp_class","search_emp_attribute","search_emp_dept","search_emp_room");

            <{if $file=='hiyari_rpt_report_classification.php' || $file=='hiyari_analysis_search.php'}>
              hiyari_search_emp_all(<{if $search_emp_all}>true<{else}>false<{/if}>, <{$post_select_data.class_cnt}>);
            <{/if}>
            
            <{if isset($sel_class_id)}>
              setClassOptions("search_emp_post",'<{$sel_class_id}>','<{$sel_atrb_id}>','<{$sel_dept_id}>','<{$sel_room_id}>');
            <{else}>
              setClassOptions("search_emp_post",'','','','');
            <{/if}>  
          </script>          
        </div>
        <!-- 検索領域 END -->
            
        <!-- 一覧 START -->
        <div id="list_area" class="color02 radius">
          <!-- 実行アイコン START -->
          <ul id="main_nav" class="clearfix">
            <{if $icon_disp.create}>
              <li class="main_nav_01"><a href="#" onclick="rp_action_new();">作成</a></li>
            <{/if}>
            
            <{if $icon_disp.return}>
              <li class="main_nav_02"><a href="#" onclick="rp_action_return('<{$mail_id_mode}>');">返信</a></li>
            <{/if}>
            
            <{if $icon_disp.forward}>
              <li class="main_nav_03"><a href="#" onclick="rp_action_forward('<{$mail_id_mode}>');">転送</a></li>
            <{/if}>

            <{if $icon_disp.edit}>              
              <{if $file == "hiyari_rpt_shitagaki_db.php"}>
                <li class="main_nav_04"><a href="#" onclick="rp_action_shitagaki_update();">編集</a></li>                
              <{else}>
                <li class="main_nav_04"><a href="#" onclick="rp_action_update('<{$mail_id_mode}>', '<{$list_id_is_mail_id}>', '<{$file}>');">編集</a></li>                
              <{/if}>
            <{/if}>
            
            <{if $icon_disp.progress}>
              <li class="main_nav_05"><a href="#" onclick="rp_action_progress('<{$mail_id_mode}>', '<{$list_id_is_mail_id}>','return_list');">進捗登録</a></li>
            <{/if}>

            <{if $icon_disp.analysis_register}>
              <li class="main_nav_10"><a href="#" onclick="rp_action_analysis2();">分析</a></li>
            <{/if}>            

            <{if $icon_disp.excel}>
              <li class="main_nav_08"><a href="#" onclick="rp_action_report_excel();">出力</a></li>
            <{/if}>
            
            
            <{if $icon_disp.undelete}>
              <li class="main_nav_91"><a href="#" onclick="rp_action_delete_cancel();">削除取消</a></li>
            <{/if}>
            
            <{if $icon_disp.search}>
              <li class="main_nav_09"><a href="#" onclick="show_search_input_page('<{$file}>');">検索</a></li>
            <{/if}>            
            
            <{if $icon_disp.delete1 || $icon_disp.delete2}>
              <li class="main_nav_90"><a href="#" onclick="rp_action_delete();">削除</a></li>
            <{/if}>
            
            <{if $icon_disp.kill}>
              <li class="main_nav_99"><a href="#" onclick="rp_action_kill();">ゴミ箱を空にする</a></li>
            <{/if}>
          </ul>

          <!-- 下書きタブ START -->
          <{if $is_other_shitagaki_readable}>
            <ul id="target_nav" class="clearfix">
              <{if $disp_mode == "all"}>
                <li><a class="tab radius_mini" href="hiyari_rpt_shitagaki_db.php?session=<{$session}>&disp_mode=self">本人分</a></li>
                <li class="active"><a class="tab radius_mini" href="hiyari_rpt_shitagaki_db.php?session=<{$session}>&disp_mode=all">すべて</a></li>
              <{else}>
                <li class="active"><a class="tab radius_mini" href="hiyari_rpt_shitagaki_db.php?session=<{$session}>&disp_mode=self">本人分</a></li>
                <li><a class="tab radius_mini" href="hiyari_rpt_shitagaki_db.php?session=<{$session}>&disp_mode=all">すべて</a></li>
              <{/if}>
            </ul>
          <{/if}>
          <!-- 下書きタブ END -->
          
          <table id="main_list" class="list">
            <!----------------------------------------------------------------->
            <!-- 一覧のタイトル -->
            <!----------------------------------------------------------------->
            <tr>
                <th width="31" <{if $file == "hiyari_rpt_report_classification.php"}>colspan="2"<{/if}>>
                    <span id="all_select" class="checkbox_wrap">
                        <input type="checkbox" class="checkbox" name="th_all_select" value="all">
                    </span>
                </th>
                
              <!-- 報告ファイル -->
              <{if $file == "hiyari_rpt_report_classification.php"}>                
                <th width="70">ステータス</th>
                <th width="80"><a title="初期ソート順" href="javascript:set_report_list_sort('0');">○</a><span style="margin-right:0.5em;"></span><a href="javascript:set_report_list_sort('REPORT_NO');">事案番号</a></th>
                <th class="subject"><a href="javascript:set_report_list_sort('REPORT_TITLE');">表題</a></th>
                <th width="58"><a href="javascript:set_report_list_sort('CLASS');">報告部署</a></th>
                <th width="50"><a href="javascript:set_report_list_sort('REGIST_NAME');">報告者</a></th>
                <th width="75"><a href="javascript:set_report_list_sort('<{$date_type}>');"><{$date_name}></a></th>
                <th width="40"<{if $last_item == 'level'}> class="none"<{/if}>>
                  <a href="javascript:set_report_list_sort('LEVEL');">影響</a>
                </th>
               
              <!-- 下書きファイル -->
              <{elseif $file == "hiyari_rpt_shitagaki_db.php"}>                
                <th class="subject"><a href="javascript:set_report_list_sort('REPORT_TITLE');">表題</a></th>
                <th width="50"><a href="javascript:set_report_list_sort('REGIST_NAME');">報告者</a></th>
                <th width="75" class="none"><a href="javascript:set_report_list_sort('REGIST_DATE');">作成日</a></th>
                
              <!-- 受信・送信トレイ -->
              <{else}>
                <th width="70">ステータス</th>
                <{if $disp_flag.report_no_disp == "t"}>
                  <th width="80">
                    <a href="javascript:set_report_list_sort('REPORT_NO');">事案番号</a>
                  </th>
                <{/if}>
                
                <{if $disp_flag.subject_disp == "t"}>
                  <th class="subject<{if $last_item == 'subject'}> none<{/if}>">
                    <a href="javascript:set_report_list_sort('SUBJECT');">件名</a>
                  </th>
                <{/if}>
                
                <{if $disp_flag.send_emp_class_disp == "t"}>
                  <th width="58"<{if $last_item == 'send_emp_class'}> class="none"<{/if}>>
                    <a href="javascript:set_report_list_sort('SEND_CLASS');">報告部署</a>
                  </th>
                <{/if}>
                
                <{if $disp_flag.send_emp_disp == "t"}>
                  <th width="50"<{if $last_item == 'send_emp'}> class="none"<{/if}>>
                    <a href="javascript:set_report_list_sort('SEND_NAME');">送信者</a>
                  </th>
                <{/if}>
                
                <{if $disp_flag.recv_emp_disp == "t"}>
                  <th width="50"<{if $last_item == 'recv_emp'}> class="none"<{/if}>>
                    受信者
                  </th>
                <{/if}>      
                
                <{if $disp_flag.send_date_disp == "t"}>
                  <th width="75"<{if $last_item == 'send_date'}> class="none"<{/if}>>
                    <a href="javascript:set_report_list_sort('SEND_DATE');">送信日</a>
                  </th>
                <{/if}>
                
                <{if $disp_flag.read_count_disp == "t"}>
                  <th width="30"<{if $last_item == 'read_count'}> class="none"<{/if}>>
                    既読
                  </th>
                <{/if}>

                <{if $disp_flag.level_disp == "t"}>
                  <th width="30"<{if $last_item == 'level'}> class="none"<{/if}>>
                    <a href="javascript:set_report_list_sort('LEVEL');">影響</a>
                  </th>
                <{/if}>
              <{/if}>
              
              <!-- 共通 -->
              <{if $disp_progress}>
                <{foreach from=$auth_list key=k item=i name=n}>
                  <th width="<{if $i.eval_use}>61<{else}>44<{/if}>" <{if $smarty.foreach.n.last}>class="none"<{/if}>>
                    <{$i.auth_name}>
                    <ul class="clearfix <{if $i.eval_use}>check3<{else}>check2<{/if}>">
                      <li><a href="javascript:set_report_list_sort('<{$k}>_START');">受</a></li>
                      <li><a href="javascript:set_report_list_sort('<{$k}>_END');">確</a></li>
                      <{if $i.eval_use}>
                        <li><a href="javascript:set_report_list_sort('<{$k}>_EVAL');">評</a></li>
                      <{/if}>
                    </ul>
                  </th>
                <{/foreach}>
              <{/if}>
            </tr>
                
            <!----------------------------------------------------------------->
            <!-- 一覧の内容 -->
            <!----------------------------------------------------------------->
            <{foreach from=$list item=i name=n}>       
              <{if $file == "hiyari_rpt_report_classification.php"}>
                <tr id="droppable_report_<{$i.report_id}>" class="houkoku droppable_report<{if $i.is_read}> kidoku<{/if}><{if $i.checked}> checked<{/if}>">
                  <td class="drag_icon">
                    <div id="draggable_report_<{$i.report_id}>" class="draggable_report" style="cursor:move;">
                      <img class="drag_handle" src="img/drag.gif" width="3" height="19">
                      <p>一件の報告書を移動</p>
                    </div>
                  </td>
                  <td class="select">
                    <span id="span_wrap[]<{$i.report_id}>" class="checkbox_wrap<{if $i.checked}> checked<{/if}>">
                      <input type="checkbox" class="checkbox" name="list_select[]" value="<{$i.list_id}>" <{if $i.checked}>checked<{/if}>>
                    </span>
                    <input type="hidden" id="report_id_of_list_id_<{$i.list_id}>" value="<{$i.report_id}>">
                    <input type="hidden" id="mail_id_of_list_id_<{$i.list_id}>" value="<{$i.mail_id}>">
                    <input type="hidden" id="report_torey_update_flg_id_<{$i.list_id}>" value="<{$i.report_torey_update_flg_value}>">
                  </td>
              <{else}>
                <tr class="<{if $i.is_read}>kidoku<{/if}>">
                  <td class="select">
                    <span id="span_wrap[]<{$i.report_id}>" class="checkbox_wrap <{if $i.checked}> checked<{/if}>">
                      <input type="checkbox" class="checkbox" name="list_select[]" value="<{$i.list_id}>" <{if $i.checked}>checked<{/if}>>
                    </span>
                    <input type="hidden" id="report_id_of_list_id_<{$i.list_id}>" value="<{$i.report_id}>">
                    <input type="hidden" id="report_torey_update_flg_id_<{$i.list_id}>" value="<{$i.report_torey_update_flg_value}>">
                  </td>
              <{/if}>
                
                <{if $file != "hiyari_rpt_shitagaki_db.php"}>
                  <td class="icon<{$status_icon_cnt}>">
                    <ul class="icon clearfix">
                      <{foreach from=$i.status key=k item=s}>
                        <{if $s.name == '評価'}>
                          <li class="rpt_status icon_0<{$k}> <{if $s.flag}>active<{/if}> eval" title="<{$s.name}>" onclick="rp_action_analysis_disp('<{$i.report_id}>');">
                        <{else}>
                          <li class="rpt_status icon_0<{$k}> <{if $s.flag}>active<{/if}>" title="<{$s.name}>">
                        <{/if}>
                          <{$s.name}>
                        </li>
                      <{/foreach}>
                    </ul>
                  </td>
                <{/if}>
                
                <{if $file == "hiyari_rpt_report_classification.php"}>
                  <!-- 報告ファイル -->
                  <td align="<{$i.report_no_align}>">
                    <table>
                      <tr>
                        <td>
                          <div class='report_link_status<{$i.report_link_status}>'></div>
                        </td>
                        <td align="left">
                          <nobr>
                          <{if $folder_id != "delete" && $is_report_classification_usable}>
                            <a href="javascript:rp_action_update_same_report_link('<{$i.report_id}>');">
                          <{/if}>
                            <{$i.report_no}>
                          <{if $folder_id != "delete" && $is_report_classification_usable}>
                            </a>
                          <{/if}>
                          </nobr>
                        </td>
                      </tr>
                    </table>
                    <input type="hidden" id="link_id_for_<{$i.report_id}>" value="<{$i.report_link_id}>">
                    <input type="hidden" id="link_status_for_<{$i.report_id}>" value="<{$i.report_link_status}>">
                  </td>
                  <td class="subject">
                    <span class="ellipsis">
                      <a href="javascript:rp_action_update_from_report_id('<{$i.report_id}>', '<{$file}>', '<{$i.mail_id}>');" title="<{$i.title}>">
                        <{$i.title}>
                      </a>
                    </span>
                  </td>
                  <td class="belong">
                    <span class="ellipsis">
                      <{$i.class_nm}>
                    </span>
                  </td>                  
                  <td class="emp_name">
                    <{if $i.anonymous_flg && $i.mouseover_popup_flg}>
                      <span class="anonymous">
                        <a href="#" title="<{$i.real_name|escape}>"><{$i.disp_emp_name|escape}></a>
                      </span>
                    <{else}>
                      <span class="ellipsis">
                        <a href="#" title="<{$i.disp_emp_name|escape}>"><{$i.disp_emp_name|escape}></a>
                      </span>
                    <{/if}>
                  </td>                  
                  <td>
                    <{$i.report_date}>
                  </td>                  
                  <td width="37"<{if $last_item == 'level'}> class="none"<{/if}>>
                    <{$i.level}>
                  </td>
                
                <{elseif $file == "hiyari_rpt_shitagaki_db.php"}>
                  <!-- 下書きファイル -->
                  <td class="subject">
                    <span class="ellipsis">
                      <a href="javascript:rp_action_shitagaki_update_from_report_id('<{$i.report_id}>');" title="<{$i.title}>">
                        <{$i.title}>
                      </a>
                    </span>
                  </td>
                  <td class="emp_name">
                    <{if $i.anonymous_flg && $i.mouseover_popup_flg}>                        
                      <span class="anonymous">
                        <a href="#" title="<{$i.real_name|escape}>"><{$i.disp_emp_name|escape}></a>
                      </span>
                    <{else}>
                      <span class="ellipsis">
                        <a href="#" title="<{$i.disp_emp_name|escape}>"><{$i.disp_emp_name|escape}></a>
                      </span>
                    <{/if}>
                  </td>
                  <td class="none"><{$i.create_date}></td>
                
                <{else}>
                  <{*$disp_flagを使用しているのは受信・送信トレイのみ*}>
                  <{if $disp_flag.report_no_disp == "t"}>
                    <td><{$i.report_no}></td>
                  <{/if}>
                
                  <{if $disp_flag.subject_disp == "t"}>
                    <td class="subject<{if $last_item == 'subject'}> none<{/if}>" <{$i.marker_style}>  style="text-align:left;">
                      <span class="ellipsis">
                        <a href="javascript:rp_action_mail_disp('<{$i.mail_id}>', '<{$mail_id_mode}>');" title="<{$i.subject}>" <{$i.marker_style}>>
                          <{$i.subject}>
                        </a>
                      </span>
                    </td>
                  <{/if}>

                  <{if $disp_flag.send_emp_class_disp == "t"}>
                    <td class="belong<{if $last_item == 'send_emp_class'}> none <{/if}>" <{$i.marker_style}>>
                      <span class="ellipsis">
                        <{$i.class_nm}>
                      </span>
                    </td>
                  <{/if}>

                  <{if $disp_flag.send_emp_disp == "t"}>
                    <td class="emp_name<{if $last_item == 'send_emp'}> none<{/if}>" <{$i.marker_style}>>
                      <{if $i.anonymous_flg && $i.mouseover_popup_flg}>
                        <span class="anonymous">
                          <a href="#" title="<{$i.real_name|escape}>"><{$i.disp_emp_name|escape}></a>
                        </span>
                      <{else}>
                        <span class="ellipsis">
                          <a href="#" title="<{$i.disp_emp_name|escape}>"><{$i.disp_emp_name|escape}></a>
                        </span>
                      <{/if}>
                    </td>
                  <{/if}>

                  <{if $disp_flag.recv_emp_disp == "t"}>
                    <td class="emp_name<{if $last_item == 'recv_emp'}> none<{/if}>" <{$i.marker_style}>>
                      <{if $i.anonymous_flg && $i.mouseover_popup_flg}>                        
                        <span class="anonymous">
                          <a href="#" title="<{$i.real_name|escape}>"><{$i.disp_emp_name|escape}></a>
                        </span>
                      <{else}>
                        <span class="ellipsis">
                          <a href="#" title="<{$i.disp_emp_name|escape}>"><{$i.disp_emp_name|escape}></a>
                        </span>
                      <{/if}>
                    </td>
                  <{/if}>
                  
                  <{if $disp_flag.send_date_disp == "t"}>
                    <td <{$i.marker_style}><{if $last_item == 'send_date'}> class="none"<{/if}>>
                      <{$i.send_date}>
                    </td>
                  <{/if}>

                  <{if $disp_flag.read_count_disp == "t"}>
                    <td<{if $last_item == 'read_count'}> class="none"<{/if}>>
                      <a href="javascript:rp_action_recv_emp_list_disp('<{$i.mail_id}>');">
                        <{$i.read_count}>
                      </a>
                    </td>
                  <{/if}>
                  
                  <{if $disp_flag.level_disp == "t"}>
                    <td<{if $last_item == 'level'}> class="none"<{/if}>>
                      <a href="javascript:rp_action_analysis_disp('<{$i.report_id}>');">
                        <{$i.level}>
                      </a>
                    </td>
                  <{/if}>
                <{/if}>
                
                <{if $disp_progress}>
                  <{foreach from=$i.auth_info key=k item=auth name=n}>
                    <td <{if $smarty.foreach.n.last}>class="none"<{/if}>>
                      <{if $auth.use}>
                        <ul class="check clearfix <{if isset($auth.evaluated)}>check3<{else}>check2<{/if}>">
                          <li>
                            <{if $auth.accepted}>
                              <a href="javascript:void(0);" class="active">◯</a>
                            <{else}>
                              <a href="javascript:void(0);"></a>
                            <{/if}>
                          </li>
                          
                          <li>
                            <{if $auth.confirmed}>
                              <a class="active<{if $progress_comment_use_flg}> comment<{/if}>" href="<{if $progress_comment_use_flg}>javascript:rp_action_progress_comment_list('<{$i.report_id}>','<{$k}>');<{else}>javascript:void(0);<{/if}>">
                                ◯
                              </a>
                            <{else}>
                              <a href="javascript:void(0);"></a>
                            <{/if}>
                          </li>
                          
                          <{if isset($auth.evaluated)}>
                            <li>
                              <{if $auth.evaluated}>
                                <a href="javascript:void(0);" class="active">◯</a>
                              <{else}>
                                <a href="javascript:void(0);"></a>
                              <{/if}>
                            </li>
                          <{/if}>
                        </ul>
                      <{/if}>
                    </td>
                  <{/foreach}>
                <{/if}>
              </tr>
            <{/foreach}>
          </table>  
          
          <!-- ページング -->
          <{include file="hiyari_paging2.tpl"}>
        </div>
      </div>
    </form>
  </div>
	<div id="footer">
		<p>Copyright Comedix all right reserved.</p>
	</div>
</body>
</html>
