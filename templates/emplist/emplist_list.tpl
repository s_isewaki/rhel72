<{if $header}>
    <table id="emplist">
        <thead>
            <tr>
                <td class="emplist-button">ボタン</td>
                <td class="emplist-name">氏名</td>
                <{if $mode === 'webmail' || $mode === 'address_group'}>
                    <td class="emplist-address">アドレス</td>
                <{elseif $mode === 'webmail_shoot'}>
                    <td class="emplist-address">メールID</td>
                <{/if}>
                <{if $type !== 'class'}>
                    <td class="emplist-class">所属</td>
                <{/if}>
                <{if $type !== 'job'}>
                    <td class="emplist-job">職種</td>
                <{/if}>
                <{if $type !== 'status'}>
                    <td class="emplist-status">役職</td>
                <{/if}>
            </tr>
        </thead>
        <tbody>
        <{/if}>
        <{foreach from=$lists item=row}>
            <{if $webmail}>
                <{assign var=mail value=`$row.loginid`}>
            <{else}>
                <{assign var=mail value=`$row.email`}>
            <{/if}>
            <{if $kind === 'emp' or $row.kind === 'emp'}>
                <tr>
                    <td class="emplist-button">
                        <{if $mode == 'project'}>
                            <span>
                                <input type="button" value="責任者" onclick="add_target('0', '<{$row.name|escape:'javascript'}>', '<{$row.emp_id|escape:'javascript'}>');">
                                <input type="button" value="事務局" onclick="add_target('1', '<{$row.name|escape:'javascript'}>', '<{$row.emp_id|escape:'javascript'}>');">
                                <input type="button" value="メンバー" onclick="add_target('2', '<{$row.name|escape:'javascript'}>', '<{$row.emp_id|escape:'javascript'}>');">
                            </span>
                        <{elseif $mode == 'webmail'}>
                            <span>
                                <input type="button" value="To" onclick="to_address('<{$row.name|escape:'javascript'}>', '<{$mail|escape}>', '<{$row.emp_id|escape:'javascript'}>');">
                                <input type="button" value="CC" onclick="cc_address('<{$row.name|escape:'javascript'}>', '<{$mail|escape}>', '<{$row.emp_id|escape:'javascript'}>');">
                                <input type="button" value="BCC" onclick="bcc_address('<{$row.name|escape:'javascript'}>', '<{$mail|escape}>', '<{$row.emp_id|escape:'javascript'}>');">
                            </span>
                        <{elseif $mode === 'address_group' || $mode === 'webmail_shoot'}>
                            <input type="button" value="コピー" onclick="add_target('<{$item_id|escape:'javascript'}>', '<{$row.name|escape:'javascript'}>', '<{$row.emp_id|escape:'javascript'}>', '<{$mail|escape}>', '<{$webmail}>');">
                        <{elseif $mode === '20'}>
                            <input type="button" value="コピー" onclick="add_target('<{$item_id|escape:'javascript'}>', '<{$row.name|escape:'javascript'}>(<{$row.job_nm|escape:'javascript'}>)', '<{$row.emp_id|escape:'javascript'}>');">
                        <{else}>
                            <input type="button" value="コピー" onclick="add_target('<{$item_id|escape:'javascript'}>', '<{$row.name|escape:'javascript'}>', '<{$row.emp_id|escape:'javascript'}>');">
                        <{/if}>
                    </td>
                    <td class="emplist-name"><{$row.name|escape}></td>
                    <{if $mode === 'webmail' || $mode === 'address_group' || $mode === 'webmail_shoot'}>
                        <td class="emplist-address">&lt;<{$mail|escape}>&gt;</td>
                    <{/if}>
                    <{if $type !== 'class'}>
                        <td class="emplist-class">
                            <{$row.class_nm|escape}>
                            &gt; <{$row.atrb_nm|escape}>
                            &gt; <{$row.dept_nm|escape}>
                        <{if $row.room_nm}> &gt; <{$row.room_nm|escape}><{/if}>
                    </td>
                <{/if}>
                <{if $type !== 'job'}>
                    <td class="emplist-job"><{$row.job_nm|escape}></td>
                <{/if}>
                <{if $type !== 'status'}>
                    <td class="emplist-status"><{$row.st_nm|escape}></td>
                <{/if}>
            </tr>

        <{elseif $kind === 'group' or $row.kind === 'group'}>
            <tr>
                <td class="emplist-button">
                    <{if $row.count > 0}>
                        <{if $mode === 'project'}>
                            <span>
                                <input type="button" value="責任者" disabled="disabled">
                                <input type="button" value="事務局" onclick="add_members('1', '<{$row.type|default:$type|escape:'javascript'}>-<{$row.id|escape:'javascript'}>');">
                                <input type="button" value="メンバー" onclick="add_members('2', '<{$row.type|default:$type|escape:'javascript'}>-<{$row.id|escape:'javascript'}>');">
                            </span>
                        <{elseif $mode == 'webmail'}>
                            <span>
                                <input type="button" value="To" onclick="to_address_list('<{$row.type|default:$type|escape:'javascript'}>-<{$row.id|escape:'javascript'}>');">
                                <input type="button" value="CC" onclick="cc_address_list('<{$row.type|default:$type|escape:'javascript'}>-<{$row.id|escape:'javascript'}>');">
                                <input type="button" value="BCC" onclick="bcc_address_list('<{$row.type|default:$type|escape:'javascript'}>-<{$row.id|escape:'javascript'}>');">
                            </span>
                        <{elseif $mode == 'address_group'}>
                            <input type="button" value="コピー" onclick="add_members('<{$item_id|escape:'javascript'}>', '<{$row.type|default:$type|escape:'javascript'}>-<{$row.id|escape:'javascript'}>-<{$webmail}>');">
                        <{elseif $mode == 'webmail_shoot'}>
                        <{else}>
                            <input type="button" value="コピー" onclick="add_members('<{$item_id|escape:'javascript'}>', '<{$row.type|default:$type|escape:'javascript'}>-<{$row.id|escape:'javascript'}>');">
                        <{/if}>
                    <{/if}>
                </td>
                <td colspan="<{if $mode === 'webmail' || $mode === 'address_group' || $mode === 'webmail_shoot'}>5<{else}>4<{/if}>" class="emplist-group <{if $row.count > 0}>empgroup<{/if}>" id="<{$row.type|default:$type|escape}>-<{$row.id|escape}>">
                    <img src="img/spacer.gif" class="close">
                    <{$row.name|escape}>
                    (<{$row.count|escape}>)
                </td>
            </tr>

        <{elseif $kind === 'subgroup' or $row.kind === 'subgroup'}>
            <tr class="<{$tr_class|escape}>">
                <td class="emplist-button">
                    <{if $row.count > 0}>
                        <{if $mode === 'project'}>
                            <span>
                                <input type="button" value="責任者" disabled="disabled">
                                <input type="button" value="事務局" onclick="add_members('1', '<{$row.type|default:$type|escape:'javascript'}>-<{$row.id|escape:'javascript'}>');">
                                <input type="button" value="メンバー" onclick="add_members('2', '<{$row.type|default:$type|escape:'javascript'}>-<{$row.id|escape:'javascript'}>');">
                            </span>
                        <{elseif $mode == 'webmail'}>
                            <span>
                                <input type="button" value="To" onclick="to_address_list('<{$row.type|default:$type|escape:'javascript'}>-<{$row.id|escape:'javascript'}>');">
                                <input type="button" value="CC" onclick="cc_address_list('<{$row.type|default:$type|escape:'javascript'}>-<{$row.id|escape:'javascript'}>');">
                                <input type="button" value="BCC" onclick="bcc_address_list('<{$row.type|default:$type|escape:'javascript'}>-<{$row.id|escape:'javascript'}>');">
                            </span>
                        <{elseif $mode == 'address_group'}>
                            <input type="button" value="コピー" onclick="add_members('<{$item_id|escape:'javascript'}>', '<{$row.type|default:$type|escape:'javascript'}>-<{$row.id|escape:'javascript'}>-<{$webmail}>');">
                        <{elseif $mode == 'webmail_shoot'}>
                        <{else}>
                            <input type="button" value="コピー" onclick="add_members('<{$item_id|escape:'javascript'}>', '<{$row.type|default:$type|escape:'javascript'}>-<{$row.id|escape:'javascript'}>');">
                        <{/if}>
                    <{/if}>
                </td>
                <td colspan="<{if $mode === 'webmail' || $mode === 'address_group' || $mode === 'webmail_shoot'}>5<{else}>4<{/if}>" class="emplist-group <{if $row.count > 0}>empgroup<{/if}>" id="<{$row.type|default:$type|escape}>-<{$row.id|escape}>" style="padding-left:<{$indent|escape}>px;">
                    <img src="img/spacer.gif" class="close">
                    <{$row.name|escape}>
                    (<{$row.count|escape}>)
                </td>
            </tr>

        <{elseif $row.emp_room === '' || $row.emp_room === null}>
            <tr class="<{$tr_class|escape}>">
                <td class="emplist-button">
                    <{if $mode === 'project'}>
                        <span>
                            <input type="button" value="責任者" onclick="add_target('0', '<{$row.name|escape:'javascript'}>', '<{$row.emp_id|escape:'javascript'}>');">
                            <input type="button" value="事務局" onclick="add_target('1', '<{$row.name|escape:'javascript'}>', '<{$row.emp_id|escape:'javascript'}>');">
                            <input type="button" value="メンバー" onclick="add_target('2', '<{$row.name|escape:'javascript'}>', '<{$row.emp_id|escape:'javascript'}>');">
                        </span>
                    <{elseif $mode == 'webmail'}>
                        <span>
                            <input type="button" value="To" onclick="to_address('<{$row.name|escape:'javascript'}>', '<{$mail|escape}>', '<{$row.emp_id|escape:'javascript'}>');">
                            <input type="button" value="CC" onclick="cc_address('<{$row.name|escape:'javascript'}>', '<{$mail|escape}>', '<{$row.emp_id|escape:'javascript'}>');">
                            <input type="button" value="BCC" onclick="bcc_address('<{$row.name|escape:'javascript'}>', '<{$mail|escape}>', '<{$row.emp_id|escape:'javascript'}>');">
                        </span>
                    <{elseif $mode == 'address_group' || $mode == 'webmail_shoot'}>
                        <input type="button" value="コピー" onclick="add_target('<{$item_id|escape:'javascript'}>', '<{$row.name|escape:'javascript'}>', '<{$row.emp_id|escape:'javascript'}>', '<{$mail|escape}>', '<{$webmail}>');">
                    <{elseif $mode === '20'}>
                        <input type="button" value="コピー" onclick="add_target('<{$item_id|escape:'javascript'}>', '<{$row.name|escape:'javascript'}>(<{$row.job_nm|escape:'javascript'}>)', '<{$row.emp_id|escape:'javascript'}>');">
                    <{else}>
                        <input type="button" value="コピー" onclick="add_target('<{$item_id|escape:'javascript'}>', '<{$row.name|escape:'javascript'}>', '<{$row.emp_id|escape:'javascript'}>');">
                    <{/if}>
                </td>
                <td class="emplist-group" style="padding-left:<{$indent|escape}>px;">
                <{if $row.emp_concurrent}>【兼】<{/if}>
                <{$row.name|escape}>
            </td>
            <{if $mode === 'webmail' || $mode === 'address_group' || $mode === 'webmail_shoot'}>
                <td class="emplist-address">&lt;<{$mail|escape}>&gt;</td>
            <{/if}>
            <{if $type !== 'empclass' && $type !== 'emproom'}>
                <td class="emplist-class">
                    <{$row.class_nm|escape}>
                    &gt; <{$row.atrb_nm|escape}>
                    &gt; <{$row.dept_nm|escape}>
                <{if $row.room_nm}> &gt; <{$row.room_nm|escape}><{/if}>
            </td>
        <{/if}>
        <{if $type !== 'empjob'}>
            <td class="emplist-job"><{$row.job_nm|escape}></td>
        <{/if}>
        <{if $type !== 'empstatus'}>
            <td class="emplist-status"><{$row.st_nm|escape}></td>
        <{/if}>
    </tr>
<{/if}>
<{/foreach}>
<{if $header}>
</tbody>
</table>
<{/if}>
