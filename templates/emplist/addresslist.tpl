<{include file="__header.tpl" title="アドレス帳" prefix='emplist' ie='EmulateIE8'}>

<body>
    <div id="close"></div>
    <div id="tab">
        <ul class="clearfix">
            <li class="active tab-nav" id="ps">個人+共有</li>
            <li class="tab-nav" id="personal">個人アドレス帳</li>
            <li class="tab-nav" id="share">共有アドレス帳</li>
<{if $mode !== 'address_group'}>
            <li class="tab-nav" id="group">グループ</li>
<{/if}>
        </ul>
        <div id="search">
            <input type="text" id="keyword" name="keyword">
            <input type="hidden" id="mode" name="mode" value="<{$mode|escape}>">
            <button id="address_search" type="button">検索</button>
            <button id="full" type="button">全表示</button>
        </div>
    </div>
    <div id="lists"></div>

<{capture name="js"}>
<script type="text/javascript" src="js/emplist/addresslist.js" charset="euc-jp"></script>
<script type="text/javascript" src="js/emplist/to_address.js?<{php}>echo time();<{/php}>" charset="euc-jp"></script>
<{/capture}>
<{include file="__footer.tpl" prefix='emplist'}>
