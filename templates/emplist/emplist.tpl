<{include file="__header.tpl" title="職員名簿" prefix='emplist' ie='EmulateIE8'}>

<body>
    <div id="close"></div>
    <div id="tab">
        <ul class="clearfix">
            <li class="active tab-nav" id="used">よく使う人</li>
            <li class="tab-nav" id="class">所属</li>
            <li class="tab-nav" id="job">職種</li>
            <li class="tab-nav" id="status">役職</li>
            <li class="tab-nav" id="project">委員会・WG</li>
            <li class="tab-nav" id="mygroup">マイグループ</li>
        </ul>
        <div id="search">
            <input type="text" id="keyword" name="keyword">
            <input type="hidden" id="mode" name="mode" value="<{$mode|escape}>">
            <input type="hidden" id="callback" name="callback" value="<{$callback|escape}>">
            <input type="hidden" id="item_id" name="item_id" value="<{$item_id|escape}>">
            <button id="name_search" type="button">検索</button>
            <{if $mode === 'webmail' || $mode === 'address_group'}>
            <button id="mail" type="button"><{if $mail_option.init_disp === 't'}>E-Mail<{else}>ウェブメール<{/if}>に切替</button>
            <input type="hidden" id="webmail" name="webmail" value="<{if $mail_option.init_disp === 't'}>1<{else}>0<{/if}>">
            <{/if}>
            <{if $mode === 'webmail'}>
            <span id="myself">
            自分を送信先に
            <label><input type="radio" id="myself_ok" name="myself" value="ok" <{if $mail_option.mygroup === 't'}>checked="checked"<{/if}>>含める</label>
            <label><input type="radio" id="myself_ng" name="myself" value="ng" <{if $mail_option.mygroup === 'f'}>checked="checked"<{/if}>>含めない</label>
            </span>
            <{/if}>
        </div>
    </div>
    <div id="lists"></div>

<{capture name="js"}>
<script type="text/javascript" src="js/emplist/emplist.js" charset="euc-jp"></script>
<script type="text/javascript" src="js/emplist/to_address.js?<{php}>echo time();<{/php}>" charset="euc-jp"></script>
<{/capture}>
<{include file="__footer.tpl" prefix='emplist'}>
