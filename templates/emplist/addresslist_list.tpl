<{if $header}>
<table id="emplist">
    <thead>
        <tr>
            <td class="emplist-button">ボタン</td>
            <td class="emplist-name">氏名</td>
            <td class="emplist-address">アドレス</td>
            <{if $type !== 'group'}><td class="emplist-src">ソース</td><{/if}>
        </tr>
    </thead>
    <tbody>
<{/if}>
<{if $kind === 'address'}>
<{foreach from=$lists item=row}>
<{if $row.email_pc}>
    <tr>
        <td class="emplist-button">
            <span>
            <{if $mode === 'address_group'}>
                <input type="button" value="コピー" onclick="addAddress('<{$row.address_id|escape:'javascript'}>', '<{$row.name|escape:'javascript'}>', '<{$row.email_pc|escape:'javascript'}>', '1');">
            <{else}>
                <input type="button" value="To" onclick="to_address('<{$row.name|escape:'javascript'}>', '<{$row.email_pc|escape:'javascript'}>', '');">
                <input type="button" value="CC" onclick="cc_address('<{$row.name|escape:'javascript'}>', '<{$row.email_pc|escape:'javascript'}>', '');">
                <input type="button" value="BCC" onclick="bcc_address('<{$row.name|escape:'javascript'}>', '<{$row.email_pc|escape:'javascript'}>', '');">
            <{/if}>
            </span>
        </td>
        <td class="emplist-name" <{if $row.email_mobile}>rowspan="2"<{/if}>><{$row.name|escape}></td>
        <td class="emplist-address">
            [PC] <{$row.email_pc|escape}>
        </td>
        <td class="emplist-src"<{if $row.email_mobile}>rowspan="2"<{/if}>><{if $row.shared_flg === 't'}>共有<{else}>個人<{/if}></td>
    </tr>
<{/if}>
<{if $row.email_mobile}>
    <tr>
        <td class="emplist-button">
            <span>
            <{if $mode === 'address_group'}>
                <input type="button" value="コピー" onclick="addAddress('<{$row.address_id|escape:'javascript'}>', '<{$row.name|escape:'javascript'}>', '<{$row.email_mobile|escape:'javascript'}>', '2');">
            <{else}>
                <input type="button" value="To" onclick="to_address('<{$row.name|escape:'javascript'}>', '<{$row.email_mobile|escape:'javascript'}>', '');">
                <input type="button" value="CC" onclick="cc_address('<{$row.name|escape:'javascript'}>', '<{$row.email_mobile|escape:'javascript'}>', '');">
                <input type="button" value="BCC" onclick="bcc_address('<{$row.name|escape:'javascript'}>', '<{$row.email_mobile|escape:'javascript'}>', '');">
            <{/if}>
            </span>
        </td>
        <{if ! $row.email_pc}><td class="emplist-name"><{$row.name|escape}></td><{/if}>
        <td class="emplist-address">
            [携帯] <{$row.email_mobile|escape}>
        </td>
        <{if ! $row.email_pc}><td class="emplist-src"><{if $row.shared_flg === 't'}>共有<{else}>個人<{/if}></td><{/if}>
    </tr>
<{/if}>
<{/foreach}>
<{elseif $kind === 'group'}>
<{foreach from=$lists item=row}>
    <tr>
        <td class="emplist-button">
            <{if $row.count > 0}>
                <input type="button" value="To" onclick="to_address_list('address_group-<{$row.id|escape:'javascript'}>');">
                <input type="button" value="CC" onclick="cc_address_list('address_group-<{$row.id|escape:'javascript'}>');">
                <input type="button" value="BCC" onclick="bcc_address_list('address_group-<{$row.id|escape:'javascript'}>');">
            <{/if}>
        </td>
        <td colspan="2" class="emplist-group <{if $row.count > 0}>empgroup<{/if}>" id="group-<{$row.id|escape}>">
        <img src="img/spacer.gif" class="close">
        <{$row.name|escape}>
        (<{$row.count|escape}>)
        </td>
    </tr>
<{/foreach}>
<{elseif $kind === 'subgroup'}>
<{foreach from=$lists item=row}>
    <tr class="<{$tr_class|escape}>">
        <td class="emplist-button">
            <input type="button" value="To" onclick="to_address('<{$row.name|escape:'javascript'}>', '<{$row.mail|escape:'javascript'}>', '');">
            <input type="button" value="CC" onclick="cc_address('<{$row.name|escape:'javascript'}>', '<{$row.mail|escape:'javascript'}>', '');">
            <input type="button" value="BCC" onclick="bcc_address('<{$row.name|escape:'javascript'}>', '<{$row.mail|escape:'javascript'}>', '');">
        </td>
        <td class="emplist-name"><{$row.name|escape}></td>
        <td class="emplist-address">
            <{if $row.type === '1'}>
                [院内]
            <{elseif $row.type === '2'}>
                [E-Mail]
            <{elseif $row.type === '3'}>
                [PC]
            <{elseif $row.type === '4'}>
                [携帯]
            <{/if}>
            <{$row.mail|escape}>
        </td>
    </tr>
<{/foreach}>
<{/if}>
<{if $header}>
    </tbody>
</table>
<{/if}>
