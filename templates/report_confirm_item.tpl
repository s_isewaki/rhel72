<{*
インターフェース

	$colspan
		int     項目値側のTDタグの横結合数。省略時は未指定。
	$rowspan
		int     項目値側のTDタグの縦結合数。省略時は未指定。
	$td_width
		string  (未使用)フィールドのTDの幅。省略時は未指定。


	$i
		string  グループコード
	$j
		string  アイテムコード
	$grps
		array   項目情報配列
	$vals
		array   値の配列

*}>
<{if !isset($th_width) || $th_width == ""}>
	<{assign var="th_width" value="188"}>
<{/if}>

<th <{if $is_last}>class="last"<{/if}> <{if $th_width != ""}>width="<{$th_width}>"<{/if}> <{if $rowspan > 1}>rowspan="<{$rowspan}>"<{/if}> >
  <{include file="report_confirm_title.tpl"}>
</th>
<td <{if $is_last}>class="last"<{/if}> <{if $td_width != ""}>width="<{$td_width}>"<{/if}> <{if $colspan > 1}>colspan="<{$colspan}>"<{/if}> <{if $rowspan > 1}>rowspan="<{$rowspan}>"<{/if}> >
  <{include file="report_confirm_field.tpl"}>
</td>
<{assign var="th_width" value=""}>
