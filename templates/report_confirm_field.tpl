<{*
インターフェース

    $i
        string  グループコード
    $j
        string  アイテムコード
    $vals
        array   値の配列
    $grps
        array   項目情報配列
*}>

<{* アセスメント・患者の状態 20090304 *}>
<{if $i == 140 && $j == 10}>   
    <{section name=k loop=$vals[$i][$j]}>
        <{* $vals[$i][$j][k] *}>
        <{section name=x loop=$grps[$i].easy_item_list[$j].easy_list}>
            <{* $grps[$i].easy_item_list[$j].easy_list[x].easy_name *}>

            <{* 選択されているか確認 値と項目情報の名称が一致 *}>
            <{if $vals[$i][$j][k]==$grps[$i].easy_item_list[$j].easy_list[x].easy_name}>

                <{$vals[$i][$j][k]}>

                <{* アセスメント・患者の状態詳細 *}>
                <{assign var="l" value="150"}>
                <{assign var="m" value=$grps[$i].easy_item_list[$j].easy_list[x].easy_code}>：
                <{section name=n loop=$vals[$l][$m]}>
                    <{$vals[$l][$m][n]}>
                <{/section}>
                
                <{* リスク回避器具 *}>
                <{if $m==6}>
                    <{assign var="l" value="170"}>
                    <{assign var="m" value="10"}>
                    <{section name=n loop=$vals[$l][$m]}>
                        (<{$vals[$l][$m][n]}><{if $vals[$l][$m][n]=='その他'}>:<{$vals[$l][20][0]}><{/if}>)
                    <{/section}>
                <{/if}>
                
                <{* 拘束用具 *}>
                <{if $m==7}>
                    <{assign var="l" value="180"}>
                    <{assign var="m" value="10"}>
                    <{section name=n loop=$vals[$l][$m]}>
                        (<{$vals[$l][$m][n]}><{if $vals[$l][$m][n]=='その他'}>:<{$vals[$l][20][0]}><{/if}>)
                    <{/section}>
                <{/if}>
                
                <{* 身体拘束行為 *}>
                <{if $m==8}>
                    <{assign var="l" value="190"}>
                    <{assign var="m" value="10"}>
                    <{section name=n loop=$vals[$l][$m]}>
                        (<{$vals[$l][$m][n]}><{if $vals[$l][$m][n]=='その他'}>:<{$vals[$l][20][0]}><{/if}>)
                    <{/section}>
                <{/if}>
                
                <{* ミトンあり詳細 *}>
                <{if $m==20}>
                    <{assign var="l" value="160"}>
                    <{assign var="m" value="10"}>
                    <{section name=n loop=$vals[$l][$m]}>
                        (<{$vals[$l][$m][n]}>)
                    <{/section}>
                <{/if}>
                
                <br />
            <{/if}>
        <{/section}>
    <{/section}>
   
<{elseif $i == 700 && $j == 10}>
    <{$eis_700_10.super_item_name}>

<{elseif $i == 710 && $j == 10}>
    <{$eis_710_10.item_name}>

<{elseif $i == 720 && $j == 10}>
    <{$eis_720_10.sub_item_name}>

<{elseif $i == 740 && $j == 10}>
    <{$eis_740_10.item_name}>

<{elseif $i == 750 && $j == 10}>
    <{$eis_750_10.sub_item_name}>

<{elseif $i == 770 && $j == 10}>
    <{$eis_770_10.item_name}>

<{elseif $i == 780 && $j == 10}>
    <{$eis_780_10.sub_item_name}>

<{elseif $i == 900 && $j == 10}>
    <{$eis_900_10.super_item_name}>

<{elseif $i == 910 && $j == 10}>
    <{$eis_910_10.item_name}>

<{elseif $i == 920 && $j == 10}>
    <{$eis_920_10.sub_item_name}>

<{elseif $i == 940 && $j == 10}>
    <{$eis_940_10.item_name}>

<{elseif $i == 950 && $j == 10}>
    <{$eis_950_10.sub_item_name}>

<{elseif $i == 970 && $j == 10}>
    <{$eis_970_10.item_name}>

<{elseif $i == 980 && $j == 10}>
    <{$eis_980_10.sub_item_name}>

<{elseif $i == 1400 && $j == 10}>
    <{$eis_1400_10.super_item_name}>

<{elseif $i == 1400 && $j == 30}>
    <{section name=k loop=$eis_1400_30}>
        <{if k != 0}><br><{/if}>
        <{$eis_1400_30[k].item_name}>
    <{/section}>

<{elseif $i == 1400 && $j == 60}>
    <{section name=k loop=$eis_1400_60}>
        <{if k != 0}><br><{/if}>
        <{$eis_1400_60[k].item_name}>
    <{/section}>

<{elseif $i == 1410 && $j == 10}>
    <{$eis_1410_10.super_item_name}>

<{elseif $i == 1410 && $j == 20}>
    <{section name=k loop=$eis_1410_20}>
        <{if k != 0}><br><{/if}>
        <{$eis_1410_20[k].item_name}>
    <{/section}>

<{elseif $i == 1410 && $j == 30}>
    <{section name=k loop=$eis_1410_30}>
        <{if k != 0}><br><{/if}>
        <{$eis_1410_30[k].sub_item_name}>
    <{/section}>

<{elseif $i == 600 && $j == 20}>
    <{$vals[600][30][0]}>
    <{$vals[600][40][0]}>
    <{$vals[600][20][0]}>
    
<{elseif ($i >= 340 && $i <=349)}>
    <{$vals[$i][10][0]}>
    <{$vals[$i][20][0]}>
    
<{else}>
    <{section name=k loop=$vals[$i][$j]}>
        <{if k != 0}><br><{/if}>
        <{$vals[$i][$j][k]}>
    <{/section}>

<{/if}>
