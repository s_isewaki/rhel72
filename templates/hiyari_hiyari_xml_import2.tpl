<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | XMLインポート</title>
	<meta http-equiv="imagetoolbar" content="no" />
	<link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/xml_import.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/pagination.css" />
  <script type="text/javascript" src="js/jquery.js"></script>
  <{include file="hiyari_post_select_js.tpl"}>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
  <script type="text/javascript">
    function load_action()
    {
      set_post_select_box_obj_key("default", "search_emp_class", "search_emp_attribute", "search_emp_dept", "search_emp_room");
      setClassOptions("default", "<{$registrant_class}>", "<{$registrant_attribute}>", "<{$registrant_dept}>", "<{$registrant_room}>");
      set_eis_list();
    }

    function import_xml() {
      document.import_form.page.value = 1;
      document.import_form.mode.value = "import";
      document.import_form.submit();
    }
    
    function page_change(page) {
      document.import_form.page.value = page;
      document.import_form.mode.value = "paging";
      document.import_form.submit();
    }
    
    function show_sub_window(url) {
      var h = window.screen.availHeight - 30;
      var w = window.screen.availWidth - 10;
      var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
      
      window.open(url, '_blank', option);
    }

    //職種と様式    
    function set_eis_list()
    {
      var job_box = document.getElementById("sel_job_id");
      var eis_box = document.getElementById("sel_eis_id");
      var job_id = job_box.options[job_box.selectedIndex].value;
      
      deleteAllOptions(eis_box);
      if (false) {}
      <{foreach from=$registrant_jobs item=job_data}>
        else if (job_id == "<{$job_data.job_id}>") {
          <{foreach from=$eis_sel_arr item=eis_data}>
            <{if $job_data.job_id == $eis_data.job_id && $eis_data.default_flg == "t"}>
              var default_eis_no = "<{$eis_data.eis_no}>";
            <{/if}>
          <{/foreach}>
          <{foreach from=$eis_sel_arr item=eis_data}>
            <{if $job_data.job_id == $eis_data.job_id}>
              addOption(eis_box, "<{$eis_data.eis_no}>", "<{$eis_data.eis_name}>", default_eis_no);
            <{/if}>
          <{/foreach}>
        }
      <{/foreach}>
    }
    
    function deleteAllOptions(box)
    {
      for (var i = box.length - 1; i >= 0; i--)
      {
        box.options[i] = null;
      }
    }
    
    function addOption(box, value, text, selected)
    {
      var opt = document.createElement("option");
      opt.value = value;
      opt.text = text;
      if (selected == value)
      {
      opt.selected = true;
      }
      box.options[box.length] = opt;
      try {box.style.fontSize = 'auto';} catch (e) {}
      box.style.overflow = 'auto';
    }    
  </script>
</head>

<body id="top" onload="load_action()">
  <{*ヘッダー*}>
  <{include file="hiyari_header2.tpl"}>

  <div id="content" class="<{$tab_info.total_width_class}>">
    <form name="import_form" method="post" action="hiyari_hiyari_xml_import.php?session=<{$session}>" enctype="multipart/form-data">
      <input type="hidden" name="mode" value="">
      <input type="hidden" name="page" value="<{$page}>">
      <input type="hidden" name="index" value="">
      <input type="hidden" name="target_xml_index" value="">
      <input type="hidden" name="target_db_index" value="">
      <input type="hidden" name="design_mode" value="<{$design_mode}>">
      
      <!-- 画面上部（条件） START -->
      <div id="condition_area"class="color01 radius mB5 clearfix">
        <table class="v_title">
          <tr>
            <th>報告部署</th>
            <td id="busho_data">
              <{$arr_class_name[0]|escape}>
              <select id="search_emp_class" name="search_emp_class" onchange="setAtrbOptions('default');"></select>
              <{$arr_class_name[1]|escape}>
              <select id="search_emp_attribute" name="search_emp_attribute" onchange="setDeptOptions('default');"></select>
              <{$arr_class_name[2]|escape}>
              <select id="search_emp_dept" name="search_emp_dept" onchange="setRoomOptions('default');"></select>
              <{if $arr_class_name.class_cnt == 4}>
                <{$arr_class_name[3]|escape}>
                <select id="search_emp_room" name="search_emp_room"></select>
              <{/if}>
            </td>
          </tr>
          <tr>
            <th>対象職種・報告書様式</th>
            <td>
              <select id="sel_job_id" name="sel_job_id" onchange="set_eis_list()">
                <{foreach from=$registrant_jobs item=job}>
                  <option value="<{$job.job_id}>"<{$job.selected}>><{$job.job_nm}></option>
                <{/foreach}>
              </select>
              <select id="sel_eis_id" name="sel_eis_id"></select>
            </td>
          </tr>
          <tr>
            <th><{$title}></th>
            <td id="inci_level">
              <{foreach from=$level_infos item=level}>
                <{if $level.use_flg}>
                  <label for="id_level_<{$level.easy_code}>">
                    <input type="radio" name="patient_level[]" id="id_level_<{$level.easy_code}>" value="<{$level.easy_code}>" />
                    <{$level.detail}>
                  </label>
                  <!--[if lte IE 6.0]>
                    <br/>
                  <![endif]-->
                <{/if}>
              <{/foreach}>
            </td>
          </tr>
          <tr>
            <th>
              重複チェック
            </th>
            <td>
              <table>
                <tr>
                  <td width="13" style="border:none; padding-left: 0; padding-right: 5px;">
                    <input type="radio" name="repetition_rb" id="repetition_rb_1" value="1" checked>
                  </td>
                  <td width="100" style="border:none; padding-left: 0; padding-right: 5px;">
                    <label for="repetition_rb_1">
                      登録部署<br>
                      登録者<br>
                      シーケンス番号
                    </label>
                  </td>
                  <td width="13" style="border:none; padding-left: 0; padding-right: 5px;">
                    <input type="radio" name="repetition_rb" id="repetition_rb_2" value="2">
                  </td>
                  <td style="border:none; padding-left: 0; padding-right: 5px;">
                    <label for="repetition_rb_2">
                      登録部署、登録者、シーケンス番号<br>
                      発生年月、発生曜日、発生時間帯、事故(事例)の概要<br>
                      患者の数、患者性別、患者の年齢(年)、患者の年齢(月)<br>
                    </label>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <th>重複データの処理</th>
            <td>
              <input type="radio" name="replace_rd" id="replace_rd_1" value="1" checked>                
              <label for="replace_rd_1">インポートしない</label>
              <input type="radio" name="replace_rd" id="replace_rd_2" class="mL20" value="2">                
              <label for="replace_rd_2">インポートする(置換)</label>
            </td>
          </tr>
          <tr>
            <th class="none">XMLファイル</th>
            <td class="none">
              <input type="file" id="file_hide" name="xml_file" value="">
              <!--
              <div id="xml_file">
                <input type="file" id="file_hide" name="xml_file" value="" onchange="document.getElementById('file_path').value = this.value;">
                <div id="file_show">
                  <input type="text" id="file_path" readonly="readonly">
                  <input type="button" id="file_btn" class="button radius_mini" value="参照">        
                </div>
              </div>
              -->
            </td>
          </tr>
        </table>        
      </div>
      <div id="main_btn">
        <input type="button" class="button radius_mini" value="登録" onclick="import_xml()">
      </div>
      <!-- 画面上部（条件） END -->
          
      <!-- 画面下部（結果） START -->      
      <div id="result_area">
        <{* メッセージ *}>
        <{if $under_disp_message != ""}>
          <div id="ref_info" class="color03 radius">
            <p><{$under_disp_message}></p>
          </div>
        <{/if}>

        <{* 処理内容 *}>
        <{if ($is_verified_ok && $num_indispensable == 0) || $mode == "paging"}>							
          <h2 class="type02">処理結果</h2>
          <div id="cnt_list" class="color02 radius">
            <{* 処理結果（件数） *}>
            <table class="list">
              <tr>
                <th>処理件数</th>
                <th>インポート件数</th>
                <th>置換件数</th>
                <th class="none">重複エラー</th>
              </tr>
              <tr>
                <td><{$TOTAL_CNT}>件</td>
                <td><{$IMPORT_CNT}>件</td>
                <td><{$REPLACE_CNT}>件</td>
                <td class="none"><{$ERROR_CNT}>件</td>
              </tr>
            </table>
          </div>
          
          <{* 重複データ一覧 *}>
          <{if $num_duplicated > 0}>
            <h2 class="type02">重複データ内容</h2>
            <div id="duplicated_list" class="color02 radius">
              <table class="list">
                <tr>
                  <th>SEQ</th>
                  <th>発生年月</th>
                  <th>時間帯</th>
                  <th>発生場所</th>
                  <th class="none">概要</th>
                </tr>
                <{foreach from=$duplicated item=result}>
                  <tr>
                    <td>
                      <{$result.seq}>
                    </td>
                    <td>
                      <{$result.year}>年<{$result.month}>月
                    </td>
                    <td>
                      <{$result.time}>
                    </td>
                    <td>
                      <{$result.site}>
                    </td>
                    <td class="none">
                      <{$result.summary}>
                    </td>
                  </tr>
                <{/foreach}>
              </table>
              <{include file="hiyari_paging2.tpl"}>
            </div>
          <{/if}>
            
          <{* 未処理データ一覧 *}>
          <{if $num_repetition > 0}>
            <div id="repetition_list">
              <h2>未処理データ内容</h2>
              （重複データが複数存在し、置換対象を選択できません）
              <table class="list">
                <tr>
                  <th>SEQ</th>
                  <th>発生年月</th>
                  <th>時間帯</th>
                  <th>発生場所</th>
                  <th>概要</th>
                  <th class="none"></th>
                </tr>
                <{foreach from=$repetition item=report key=k}>
                  <tr>
                    <td>
                      <{$report.seq}>
                    </td>
                    <td>
                      <{$report.year}>年<{$report.month}>月
                    </td>
                    <td>
                      <{$report.time}>
                    </td>
                    <td>
                      <{$report.site}>
                    </td>
                    <td>
                      <{$report.summary}>
                    </td>
                    <td class="none">
                      <input type="button" class="button radius_mini" value="置換対象選択" <{$report.button_str}> onclick="show_sub_window('hiyari_hiyari_xml_import_sub.php?session=<{$session}>&index=<{$k}>')">
                    </td>
                  </tr>
                <{/foreach}>
              </table>
            </div>
          <{/if}>
        <{/if}>                
      </div>
      <!-- 画面下部（結果） END -->
    </form>
  </div>
  <{*フッター*}>
  <{include file="hiyari_footer2.tpl"}>
</body>
</html>
