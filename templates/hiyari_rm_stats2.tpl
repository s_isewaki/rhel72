<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}> | 統計分析</title>
	<meta http-equiv="imagetoolbar" content="no" />
	<link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/rm_stats.css" />
  <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/hiyari_rm_stats.js?v=1"></script>
  <{include file="hiyari_post_select_js.tpl"}>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
  <style type="text/css">
     body select {font-size: 13px;}
  </style>
<script type="text/javascript">
    jQuery(function($){
		//集計ダウンロード表示
    	<{if $stats_download_mode == true}>
	       	var sub_menu_width = document.getElementById('sub_menu');
	       	sub_menu_width.style.width = "630px";
	    <{/if}>
		    
        //チェックボックス
        $('.checkbox').click(function(){
            $(this).closest('.checkbox_wrap').toggleClass('checked');
        });

        //ライドボックス全チェック
        $('#all_folder').click(function(){
			allCheckOnOff('folder');
        });
        $('#all_style').click(function(){
			allCheckOnOff('style');
        });
        function allCheckOnOff(mode){
            var all_flag = $('#all_' + mode ).is(':checked');
            
            $('[id^=' + mode + '_select_]').each(function(){
                if (all_flag != $(this).is(':checked')){
					$(this).prop('checked', all_flag);
	            	$(this).closest('.checkbox_wrap').toggleClass('checked');
                }
                
            });
        }
    });
    
    function init_page()
    {    
        <{* 集計部署の選択肢の初期化 *}>
        <{section name=cnt loop=$count_search}>
            set_post_select_box_obj_key("search_emp_post_<{$smarty.section.cnt.index}>",
                                        "search_emp_class_<{$smarty.section.cnt.index}>",
                                        "search_emp_attribute_<{$smarty.section.cnt.index}>",
                                        "search_emp_dept_<{$smarty.section.cnt.index}>",
                                        "search_emp_room_<{$smarty.section.cnt.index}>");
            setClassOptions("search_emp_post_<{$smarty.section.cnt.index}>",
                            "<{$search_emp_class[$smarty.section.cnt.index]}>",
                            "<{$search_emp_attribute[$smarty.section.cnt.index]}>",
                            "<{$search_emp_dept[$smarty.section.cnt.index]}>",
                            "<{$search_emp_room[$smarty.section.cnt.index]}>");
        <{/section}>
        
        <{* 縦軸・横軸選択肢の初期化 *}>
        vertical_axis_update();
        horizontal_axis_update();
    }
  
    /////////////////////////////////////////////////////////////////////////////
    //縦軸・横軸の選択肢設定
    /////////////////////////////////////////////////////////////////////////////
    function vertical_axis_update()
    {
        var obj = document.mainform.vertical_axis;
        var selected_value = obj.options[obj.selectedIndex].value;

        //FFではdisplay:noneにすることができるが、IEの場合はできないので入れ替え
        deleteAllSelectBoxItem(obj);

        switch (document.mainform.count_date_mode.value){
        case "incident_date":
            if(selected_value == "report_month"){
                selected_value = "month";
            }
            <{foreach from=$vertical_axises key=vertical_axis_code item=vertical_axis_text}>
                <{if $vertical_axis_code != 'report_month'}>
                    addSelectBoxItem(obj, '<{$vertical_axis_code}>', '<{$vertical_axis_text|escape:"quotes"}>', selected_value);
                <{/if}>
            <{/foreach}>
            break;
        case "report_date":
            if(selected_value == "month"){
                selected_value = "report_month";
            }
            <{foreach from=$vertical_axises key=vertical_axis_code item=vertical_axis_text}>
                <{if $vertical_axis_code != 'month'}>
                    addSelectBoxItem(obj, '<{$vertical_axis_code}>', '<{$vertical_axis_text|escape:"quotes"}>', selected_value);
                <{/if}>
            <{/foreach}>
            break;
        }
    }
    
    function horizontal_axis_update()
    {
        var obj = document.mainform.horizontal_axis;
        var selected_value = obj.options[obj.selectedIndex].value;

        //FFではdisplay:noneにすることができるが、IEの場合はできないので入れ替え
        deleteAllSelectBoxItem(obj);

        switch(document.mainform.count_date_mode.value){
        case "incident_date":
            if(selected_value == "report_month"){
                selected_value = "month";
            }
            <{foreach from=$horizontal_axises key=horizontal_axis_code item=horizontal_axis_text}>
                <{if $horizontal_axis_code != 'report_month'}>
                    addSelectBoxItem(obj, '<{$horizontal_axis_code}>', '<{$horizontal_axis_text|escape:"quotes"}>', selected_value);
                <{/if}>
            <{/foreach}>
            break;
        case "report_date":
            if(selected_value == "month"){
                selected_value = "report_month";
            }
            <{foreach from=$horizontal_axises key=horizontal_axis_code item=horizontal_axis_text}>
                <{if $horizontal_axis_code != 'month'}>
                    addSelectBoxItem(obj, '<{$horizontal_axis_code}>', '<{$horizontal_axis_text|escape:"quotes"}>', selected_value);
                <{/if}>
            <{/foreach}>
        }
    }

    function addSelectBoxItem(box, value, text, selected_value)
    {
        var opt = document.createElement("option");
        opt.value = value;
        opt.text = text;

        //FireFox用
        if (selected_value == value){
            opt.setAttribute("selected", true);
        }

        box.options[box.length] = opt;

        //IE用
        if (selected_value == value){
            opt.setAttribute("selected", true);
        }

        try {box.style.fontSize = 'auto';} catch (e) {}
        box.style.overflow = 'auto';
    }
    
    function deleteAllSelectBoxItem(box)
    {
        for (var i = box.length - 1; i >= 0; i--){
            box.options[i] = null;
        }
    } 
</script>
</head>
<body id="top" onload="init_page()";>
  <{*ヘッダー*}>
  <{include file="hiyari_header2.tpl"}>

  <div id="content" class="<{$tab_info.total_width_class}>">
    <{* 上部ボタン *}>
    <div id="sub_menu" class="color01 radius">
      <table class="v_title">
        <tr>
          <th class="none">MENU</th>
          <td class="active none">
            <a href="hiyari_rm_stats.php?session=<{$session}>">クロス集計</a>
          </td>
          <td class="none">
            <a href="hiyari_rm_stats_pattern.php?session=<{$session}>">定型集計</a>
          </td>
          <{if $stats_download_mode == true}>
	          <td class="none">
	            <a href="hiyari_rm_stats_download.php?session=<{$session}>">集計ダウンロード</a>
	          </td>
          <{/if}>
        </tr>
      </table>
    </div>
  
    <!----------------------------------------------------------------------------------- 
    集計条件領域 START
    ------------------------------------------------------------------------------------>
    <form name="mainform" action="hiyari_rm_stats.php" method="post">
      <input type="hidden" name="<{$session_name}>" value="<{$session_id}>">
      <input type="hidden" name="session" value="<{$session}>">
      <input type="hidden" name="postback_mode" value="count">
      <input type="hidden" id="get_stats_condition" name="get_stats_condition" value="">
      <{* 所属 *}>
      <input type="hidden" name="count_search" value="<{if empty($count_search)}>0<{else}><{$count_search}><{/if}>">
      <input type="hidden" name="delete_line_no" value="">
      <{* 集計データクリア *}>
      <input type="hidden" id="is_item_shibori_clear" name="is_item_shibori_clear" value="false">
      <input type="hidden" name="shibori_info_clear" value="">
	  
	  <{* 絞込条件追加 *}>
	  <{foreach from=$folder_id item=folder}>
        <input type="hidden" name="folder_id[]" value="<{$folder}>">
      <{/foreach}>
      <{foreach from=$style_id item=style}>
        <input type="hidden" name="style_id[]" value="<{$style}>">
      <{/foreach}>

      <div id="conditions1" class="color01 radius">
        <table class="v_title">
          <!-- 集計条件:集計年月 -->
          <tr>
            <th>集計年月</th>
            <td colspan="<{$post_select_data.class_cnt+1}>">
              <select id="Date_Year" name="Date_Year">
                <{foreach from=$year_list item=y}>
                  <option label="<{$y}>" value="<{$y}>" <{if $y==$year}>selected<{/if}>><{$y}></option>
                <{/foreach}>
              </select>年
              
              <select id="Date_Month" class="mL10" name="Date_Month">
                <{foreach from=$month_list item=m}>
                  <option value="<{$m|string_format:'%02d'}>" <{if $m==$month}>selected<{/if}>><{$m}></option>
                <{/foreach}>
              </select>月〜

              <select id="Date_Term" class="mL10" name="Date_Term">
                <{foreach from=$month_list item=d}>
                  <option value="<{$d|string_format:'%02d'}>" <{if $d==$Date_Term}>selected<{/if}>><{$d}>ヶ月分</option>
                <{/foreach}>
              </select>
              　　　　
              <!-- 集計条件:集計モード -->
              <select id="count_date_mode" name="count_date_mode" onchange="vertical_axis_update();horizontal_axis_update()">
                <option value="report_date" <{if $is_report_date_mode}>selected<{/if}> >報告日</option>
                <option value="incident_date" <{if $is_incident_date_mode}>selected<{/if}> >発生日</option>
              </select>
              <!-- 集計条件:副報告 -->
              <label for="sub_report_use">
                <span class="checkbox_wrap<{if $sub_report_use_flg}> checked<{/if}>">
                  <input type="checkbox" id="sub_report_use" class="mL10 checkbox" name="sub_report_use" value="true" <{if $sub_report_use_flg}>checked<{/if}> >
                </span>
                副報告を集計に含める
              </label>
            </td>
          </tr>    
          
          <!-- 集計条件:所属 -->
          <{section name=cnt loop=$count_search}>
            <tr>
              <{if $smarty.section.cnt.first}>
                <th class="none" rowspan="<{$count_search}>">所属</th>
              <{/if}>              
              <td <{if $smarty.section.cnt.index == $count_search-1}>class="none"<{/if}>>
                <div id="busho_data">
                  <{$arr_class_name[0]|escape}>
                  <select id="search_emp_class_<{$smarty.section.cnt.index}>" name="search_emp_class[]" onchange="setAtrbOptions('search_emp_post_<{$smarty.section.cnt.index}>');"></select>
                  <{$arr_class_name[1]|escape}>
                  <select id="search_emp_attribute_<{$smarty.section.cnt.index}>" name="search_emp_attribute[]" onchange="setDeptOptions('search_emp_post_<{$smarty.section.cnt.index}>');"></select>
                  <{$arr_class_name[2]|escape}>
                  <select id="search_emp_dept_<{$smarty.section.cnt.index}>" name="search_emp_dept[]" onchange="setRoomOptions('search_emp_post_<{$smarty.section.cnt.index}>');"></select>
                  <{if $post_select_data.class_cnt == 4}>
                    <{$arr_class_name[3]|escape}>
                    <select id="search_emp_room_<{$smarty.section.cnt.index}>" name="search_emp_room[]"></select>
                  <{/if}>
                </div>
                <div id="busho_btn">
                  <input class="button radius_mini clear" type="button" name="add_search" value="追加" onClick="search_add(this.form)">
                  <{if $smarty.section.cnt.index == 0}>
                    &nbsp;<div class="dummy"></div>
                  <{else}>
                    &nbsp;<input class="button radius_mini" type="button" name="delete_search" value="削除" onClick="search_delete(this.form, <{$smarty.section.cnt.index}>)">
                  <{/if}>
                </div>
              </td>
            </tr>
          <{/section}>
        </table>
      </div>
    
      <div id="conditions2" class="color01 clearfix">
        <table class="v_title">
          <!-- 集計条件:集計データ -->
          <tr>
            <th width="100">集計データ</th>
            <td colspan="4" id="item_shibori_disp_area">
              <{if $item_shibori_info == "" && $item_folder_info == ""}>
                全てのデータ
              <{else}>
                <div id="shibori_data">
                  <table>
                    <{foreach from=$item_shibori_info item=item_shibori_info1 name=shibori}>
                      <tr>
                        <td width="170">
                            <{if $item_shibori_info1.item_name != ""}>
                             <{$item_shibori_info1.item_name|escape}>
                             <{else}>
                                報告書様式
                            <{/if}>
                        </td>
                        <td>
                          <{foreach from=$item_shibori_info1.code_list item=item_shibori_info2 name=code_list}>
                            <{if !$smarty.foreach.code_list.first}>、<{/if}>
                            <{$item_shibori_info2.code_name|escape}>
                          <{/foreach}>
                        </td>
                      </tr>
                    <{/foreach}>
                    
                    <{if count($item_folder_info) > 0}>
                      <tr>
                        <td width="170">
                          ラベル
                        </td>
                        <td>
                          <{foreach from=$item_folder_info item=item_folder_info name=folder}>
                            <{if !$smarty.foreach.folder.first}>、<{/if}>
                            <{$item_folder_info|escape}>
                          <{/foreach}>
                        </td>
                      </tr>
                    <{/if}>
                    
                    
                  </table>
                </div>
                <div id="shibori_btn">
                  <input class="button radius_mini" type="button" id="item_shibori_clear_button" value="全てのデータ" onclick="item_shibori_clear();">
                </div>
              <{/if}>
              
            </td>
          </tr>
          
          <!-- 集計条件:軸 -->
          <tr>
            <th>軸</th>
            <td>
              <div>
              縦軸
              <select name="vertical_axis" class="mR10">
                <{foreach from=$vertical_axises key=vertical_axis_code item=vertical_axis_text}>
                  <option value="<{$vertical_axis_code}>" <{if $vertical_axis_code == $vertical_axis}>selected<{/if}>><{$vertical_axis_text}></option>
                <{/foreach}>
              </select>
                
              横軸
              <select id="horizontal_axis" name="horizontal_axis">
                <{foreach from=$horizontal_axises key=horizontal_axis_code item=horizontal_axis_text}>
                  <option value="<{$horizontal_axis_code}>" <{if $horizontal_axis_code == $horizontal_axis}>selected<{/if}> ><{$horizontal_axis_text}></option>
                <{/foreach}>
              </select>
              </div>
              
              <!--  絞り込み条件ボックス表示 -->
              <div id="shadowing"></div>
              <div id="box" width="100%">
				<div id="conditions1" class="color01"  style="height:320px;overflow-y:scroll;">
			        <table class="v_title">
			          <tr height="150px">
			            <th>ラベル</th>
			            <td width="60px">
			              <label for="all_folder">
			                <span class="checkbox_wrap">
			                  <input type="checkbox" id="all_folder" class="mL10 checkbox" name="all_folder">
			                </span>
			                全て
			              </label>
			            </td>
			            <td width="540px">
			              <{foreach from=$all_folder_info item=fld}>
			                <div style="clear:both; float:left;">
					          <label for="folder_select_<{$fld.folder_id}>">
					            <span class="checkbox_wrap <{$fld.checked}>">
					              <input type="checkbox" id="folder_select_<{$fld.folder_id}>" class="mL10 checkbox" name="folder_select[]" value="<{$fld.folder_id}>_<{$fld.folder_name|escape}>" <{$fld.checked}>>
					            </span>
					            <{$fld.folder_name|escape}>  
					          </label>
			                </div>
			              <{/foreach}>
            </td>
			            
					  </tr>
					  
					  <tr height="150px">
			            <th class="none">報告書様式</th>
			            <td width="60px">
			              <label for="all_style">
			                <span class="checkbox_wrap">
			                  <input type="checkbox" id="all_style" class="mL10 checkbox" name="all_style" value="true">
			                </span>
			                全て
			              </label>
			            </td>
			            <td width="540px">
			              <{foreach from=$all_style_info item=sty}>
			              	<div style="clear:both; float:left;">
					          <label for="style_select_<{$sty.eis_id}>">
					            <span class="checkbox_wrap <{$sty.checked}>">
					              <input type="checkbox" id="style_select_<{$sty.eis_id}>" class="mL10 checkbox" name="style_select[]" value="<{$sty.eis_id}>_<{$sty.eis_name|escape}>" <{$sty.checked}>>
					            </span>
					            <{$sty.eis_name|escape}>  
					          </label>
				            </div>
			              <{/foreach}>
			              
			            </td>
					  </tr>
					</table>
				</div>
				
				<!--<div style="clear:both; float:right;">-->
                                <div style="float:right;">
				　<input class="button radius_mini" type="button" name="settle" value="確定" onClick="add_box_item()">
			      <input class="button radius_mini" type="button" name="cancel" value="戻る" onClick="closebox()">
			      <input type="hidden" name="boxflag" value="">
			    </div>
			  </div>
			  
              <div id="add_condition_btn" style="float:right;">
            　	<input class="button radius_mini" type="button" name="add_condition" value="絞込条件追加" onClick="openbox('集計データ追加')">
              </div>
            </td>
            
          </tr>
          
          <!-- 集計条件保存・呼出 -->
          <tr>
            <th class="none">集計条件</th>
            <td class="none">
              <ul class="clearfix" style="float:right;">
                <li style="float:left; margin-left: 5px;">
                  <span id="condition_name_area" style="display:none;">
                    集計条件保存名
                    <input type="text" name="stats_condition" size=80 id="condition_name">              
                  </span>
                  <input class="button radius_mini" type="button" value="集計条件保存" onclick="save_condition(this.form)">
                  <input type="hidden" id="is_stats_condition_set" name="is_stats_condition_set" value="">
                </li>
                <li style="float:left; margin-left: 5px;">
                  <input class="button radius_mini" type="button" value="集計条件呼出" onclick="window.open('hiyari_rm_stats_condition_load.php?session=<{$session}>','stats_condition_load','directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=600,height=350')">
                </li>
              </ul>
            </td>
          </tr>
        </table>
      </div>
      
      <div id="main_btn">      
        <input class="button radius_mini" type="submit" name="action" value="集計">      
      </div>
    </form>

    <!----------------------------------------------------------------------------------- 
    集計結果領域 START
    ------------------------------------------------------------------------------------>
    <{if $has_result}>      
      <div class="section" id="action_btns">
        <div class="clearfix">
          <ul style="float:right;">
            <li>
              <input class="button radius_mini" type="button" value="集計データ設定" onclick="item_shibori_change();">
            </li>
            <li>
              <input class="button radius_mini" type="button" value="指定項目を除外" onclick="item_no_disp_action();">
            </li>
            <{if $vertical_no_disp_code_list != "" || $horizontal_no_disp_code_list != ""}>
              <li>
                <input class="button radius_mini" type="button" value="除外解除" onclick="item_all_disp_action();">
              </li>
            <{/if}>
            <{if $vertical_axises_detail != ""}>
              <li>
                <input class="button radius_mini" type="button" value="縦軸を戻す" onclick="vertical_axis_detail_return('<{$vertical_axises_detail_link_return}>');">
              </li>
            <{/if}>
            <{if $horizontal_axises_detail != ""}>
              <li>
                <input class="button radius_mini" type="button" value="横軸を戻す" onclick="horizontal_axis_detail_return('<{$horizontal_axises_detail_link_return}>');">
              </li>
            <{/if}>
            <li>
              <input class="button radius_mini" type="button" value="印刷" onclick="print_download();">
            </li>
            <{if $excel_disp_flg.collect_excel_btn_show_flg == 't'}>
              <li>
                <input class="button radius_mini" type="button" value="EXCEL出力" onclick="excel_download();">
              </li>
            <{/if}>
            <li>
              <input class="button radius_mini" type="button" value="グラフ" onclick="chart_disp('<{$session}>', '<{$session_name}>', '<{$session_id}>');">
            </li>
          </ul>
        </div>
      </div>
      
      <!-- 集計結果 START -->
      <div id="result_area" class="section">
        <div class="color02 radius">
          <table id="axis_label">
            <tr>
              <th width="8%">縦軸</th>
              <td width="42%"><{$vertical_label|escape}></td>
              <th width="8%">横軸</th>
              <td width="42%"><{$horizontal_label|escape}></td>
            </tr>
          </table>

          <table id="result">
            <!---------------------------------------------
            タイトル行（列タイトル）
            ---------------------------------------------->
            <tr class="col_title col_top">
              <th width="18%" rowspan="3"></th>
              <{foreach from=$title_row item=cell}>
                <th width="<{$col_width}>%">
                  <span class="checkbox_wrap <{$cell.checked}>">
                    <input type="checkbox" class="checkbox" name="horizontal_axis_checkbox[]" value="<{$cell.easy_code}>" <{$cell.checked}> >
                  </span>
                </th>
              <{/foreach}>
              <th width="<{$col_width}>%" rowspan="3">
                計
              </th>
            </tr>
            <tr class="col_title">
              <{foreach from=$title_row item=cell}>
                <th>
                  <{if $cell.horizontal_link_flg}>
                    <a href="javascript:horizontal_axis_detail_action('<{$horizontal_axises_detail_link}>','<{$cell.easy_code}>');">
                  <{/if}>
                      <span><{$cell.easy_name|escape}></span>
                  <{if $cell.horizontal_link_flg}>
                    </a>
                  <{/if}>
                </th>
              <{/foreach}>
            </tr>
            <tr class="col_title">
              <{foreach from=$title_row item=cell}>
                <th>
                  <{foreach name=btn from=$cell.btn_list item=btn key=k}>
                    <{if $k > 0}><br><{/if}>
                    <input type="button" value="<{$btn.value}>" onclick="javascript:horizontal_axis_detail_action('<{$btn.detail}>','<{$cell.easy_code}>');" <{$btn.disabled}>>
                  <{/foreach}>
                </th>
              <{/foreach}>
            </tr>

            <!---------------------------------------------
            詳細行
            ---------------------------------------------->
            <{foreach from=$detail_rows item=row}>
              <tr>
                <!-- 行タイトル START --> 
                <th class="row_title">
                  <table>
                    <tr>
                      <th width="15">
                        <span class="checkbox_wrap <{$row.checked}>">
                          <input type="checkbox" class="checkbox" name="vertical_axis_checkbox[]" value="<{$row.easy_code}>" <{$row.checked}> >
                        </span>
                      </th>
                      <th>
                        <{if $row.vertical_link_flg}>
                          <a href="javascript:vertical_axis_detail_action('<{$vertical_axises_detail_link}>','<{$row.easy_code}>');">
                        <{/if}>
                        <{$row.easy_name|escape}>
                        <{if $row.vertical_link_flg}>
                          </a>
                        <{/if}>
                      </th>
                      <{foreach from=$row.btn_list item=btn}>
                        <th width="30">
                          <input type="button" value="<{$btn.value}>" onclick="javascript:vertical_axis_detail_action('<{$btn.detail}>','<{$row.easy_code}>');" <{$btn.disabled}> >
                        </th>
                      <{/foreach}>
                    </tr>
                  </table>
                </th>
                <!-- 行タイトル END --> 
                
                <!-- 件数 START -->
                <{foreach from=$row.cells item=cell}>
                  <td>
                    <{if $cell.count != 0}>
                      <a href="javascript:void(0)" onclick="show_report_list_window('<{$v_name_list[$row.index]}>', '<{$h_name_list[$cell.index]}>','<{$cell.id_list}>');return false;">
                    <{/if}>
                      <{$cell.count}>
                    <{if $cell.count != 0}>
                      </a>
                    <{/if}>
                  </td>
                <{/foreach}>
                <!-- 件数 END -->
                
                <!-- 行合計 START -->
                <td>
                  <{if $row.sum != 0}>
                    <a href="javascript:void(0)" onclick="show_report_list_window('<{$v_name_list[$row.index]}>', '合計', '<{$row.id_list}>');return false;">
                  <{/if}>
                    <{$row.sum}>
                  <{if $row.sum != 0}>
                    </a>
                  <{/if}>
                </td>
                <!-- 行合計 END -->                      
              </tr>
            <{/foreach}>
            
            <!---------------------------------------------
            合計行
            ---------------------------------------------->
            <tr>
              <th id="sum_title">計</th>

              <!-- 列合計 START -->
              <{foreach from=$sum_row item=cell}>
                <td>
                  <{if $cell.sum != 0}>
                    <a href="javascript:void(0)" onclick="show_report_list_window('合計', '<{$h_name_list[$cell.index]}>', '<{$cell.id_list}>');return false;">
                  <{/if}>
                    <{$cell.sum}>
                  <{if $cell.sum != 0}>
                    </a>
                  <{/if}>
                </td>
              <{/foreach}>
              <!-- 列合計 END -->

              <!-- 総合計 START -->
              <td>
                <{if $sums_sum != 0}>
                  <a href="javascript:void(0)" onclick="show_report_list_window('合計', '合計', '<{$sum_id_list}>');return false;">
                <{/if}>
                  <{$sums_sum}>
                <{if $sums_sum != 0}>
                  </a>
                <{/if}>
              </td>
              <!-- 総合計 END -->
            </tr>
          </table>
        </div>
      </div>
      <!-- 集計結果 END -->
    <{/if}>
    <!-- 集計結果領域 END -->

    <!--------------------------------------------------------------------------------------------------------------------------->
    <!-- 軸詳細用フォーム(前回送信データを元に実行) -->
    <!--------------------------------------------------------------------------------------------------------------------------->
    <form id="result_edit_form" name="result_edit_form" action="hiyari_rm_stats.php" method="post">
      <input type="hidden" name="<{$session_name}>" value="<{$session_id}>">
      <!-- 送信条件 -->
      <input type="hidden" name="session" value="<{$session}>">
      <input type="hidden" id="postback_mode" name="postback_mode" value="">
      <!-- 検索条件 -->
      <input type="hidden" name="count_date_mode" value="<{$count_date_mode}>">
      <input type="hidden" name="year" value="<{$year}>">
      <input type="hidden" name="month" value="<{$month}>">
      <input type="hidden" name="Date_Year" value="<{$year}>">
      <input type="hidden" name="Date_Month" value="<{$month}>">
      <input type="hidden" name="Date_Term" value="<{$Date_Term}>">
      <input type="hidden" name="sub_report_use" value="<{$sub_report_use}>">
      <input type="hidden" name="count_search" value="<{if empty($count_search)}>0<{else}><{$count_search}><{/if}>">
      <input type="hidden" name="delete_line_no" value="">
      <{section name=cnt loop=$count_search}>
        <input type="hidden" name="search_emp_class[]" value="<{$search_emp_class[$smarty.section.cnt.index]}>">
        <input type="hidden" name="search_emp_attribute[]" value="<{$search_emp_attribute[$smarty.section.cnt.index]}>">
        <input type="hidden" name="search_emp_dept[]" value="<{$search_emp_dept[$smarty.section.cnt.index]}>">
        <input type="hidden" name="search_emp_room[]" value="<{$search_emp_room[$smarty.section.cnt.index]}>">
      <{/section}>
      <input type="hidden" name="vertical_axis" value="<{$vertical_axis}>">
      <input type="hidden" name="horizontal_axis" value="<{$horizontal_axis}>">
      <!-- ドリルダウン -->
      <input type="hidden" name="vertical_axises_detail" value="<{$vertical_axises_detail}>">
      <input type="hidden" name="vertical_axises_detail_old" value="<{$vertical_axises_detail}>">
      <input type="hidden" name="horizontal_axises_detail" value="<{$horizontal_axises_detail}>">
      <input type="hidden" name="horizontal_axises_detail_old" value="<{$horizontal_axises_detail}>">
      <input type="hidden" name="vertical_axises_summary_item" value="<{$vertical_axises_summary_item}>">
      <input type="hidden" name="horizontal_axises_summary_item" value="<{$horizontal_axises_summary_item}>">
      <input type="hidden" name="vertical_axises_post_class" value="<{$vertical_axises_post_class}>">
      <input type="hidden" name="vertical_axises_post_attribute" value="<{$vertical_axises_post_attribute}>">
      <input type="hidden" name="vertical_axises_post_dept" value="<{$vertical_axises_post_dept}>">
      <input type="hidden" name="horizontal_axises_post_class" value="<{$horizontal_axises_post_class}>">
      <input type="hidden" name="horizontal_axises_post_attribute" value="<{$horizontal_axises_post_attribute}>">
      <input type="hidden" name="horizontal_axises_post_dept" value="<{$horizontal_axises_post_dept}>">
      <!-- 非表示項目 -->
      <input type="hidden" name="vertical_no_disp_code_list_csv" value="<{$vertical_no_disp_code_list_csv}>">
      <input type="hidden" name="horizontal_no_disp_code_list_csv" value="<{$horizontal_no_disp_code_list_csv}>">
      <!-- 項目絞り込み情報文字列 -->
      <input type="hidden" name="item_shibori_info_string" value="<{$item_shibori_info_string|escape}>">
      <!-- 非表示項目(チェックボックス表示用) -->
      <input type="hidden" name="vertical_no_disp_code_list_old_csv" value="">
      <input type="hidden" name="horizontal_no_disp_code_list_old_csv" value="">
      <!-- 項目絞り込み変更 -->
      <input type="hidden" name="vertical_item_shibori_change_csv" value="">
      <input type="hidden" name="horizontal_item_shibori_change_csv" value="">
      <!-- 項目絞り込みクリア -->
      <input type="hidden" id="is_item_shibori_clear_of_result_edit_form" name="is_item_shibori_clear" value="false">
      <!-- 集計結果キャッシュ -->
      <input type="hidden" name="cells_report_id_list_full_string" value="<{$cells_report_id_list_full_string}>">
      <!-- 集計条件取得 -->
      <input type="hidden" id="get_stats_condition" name="get_stats_condition" value="">
      <!-- 絞込追加条件 -->
      <{foreach from=$folder_id item=folder}>
      	<input type="hidden" name="folder_id[]" value="<{$folder}>">
      <{/foreach}>
      <{foreach from=$style_id item=style}>
      	<input type="hidden" name="style_id[]" value="<{$style}>">
      <{/foreach}>
      
    </form>
    <!--------------------------------------------------------------------------------------------------------------------------->

    <!--------------------------------------------------------------------------------------------------------------------------->
    <!-- 他画面送信:子画面用フォーム -->
    <!--------------------------------------------------------------------------------------------------------------------------->
    <form name="report_id_list_form" action="hiyari_rm_stats_reports.php" method="post" target="rm_stats_reports_window">
      <input type="hidden" name="<{$session_name}>" value="<{$session_id}>">
      <!-- 送信条件 -->
      <input type="hidden" name="session" value="<{$session}>">
      <!-- 検索条件 -->
      <input type="hidden" name="count_date_mode" value="<{$count_date_mode}>">
      <!-- 検索条件表示 -->
      <input type="hidden" name="vertical_label" value="<{$vertical_label}>">
      <input type="hidden" name="horizontal_label" value="<{$horizontal_label}>">
      <input type="hidden" name="vertical_item_label" value="">
      <input type="hidden" name="horizontal_item_label" value="">
      <!-- 集計データ -->
      <input type="hidden" name="report_id_list" value="">
    </form>
    <!--------------------------------------------------------------------------------------------------------------------------->

    <!--------------------------------------------------------------------------------------------------------------------------->
    <!-- 他画面送信:Excel用フォーム -->
    <!--------------------------------------------------------------------------------------------------------------------------->
    <form name="xls" action="hiyari_rm_stats_excel.php" method="post" target="download">
      <input type="hidden" name="<{$session_name}>" value="<{$session_id}>">
      <!-- 送信条件 -->
      <input type="hidden" name="session" value="<{$session}>">
      <!-- 検索条件 -->
      <input type="hidden" name="count_date_mode" value="<{$count_date_mode}>">
      <input type="hidden" name="year" value="<{$year}>">
      <input type="hidden" name="month" value="<{$month}>">
      <input type="hidden" name="Date_Term" value="<{$Date_Term}>">
      <input type="hidden" name="sub_report_use" value="<{$sub_report_use}>">
      <!--// ループ開始 -->
      <{section name=cnt loop=$count_search}>
        <input type="hidden" name="search_emp_class[]" value="<{$search_emp_class[$smarty.section.cnt.index]}>">
        <input type="hidden" name="search_emp_attribute[]" value="<{$search_emp_attribute[$smarty.section.cnt.index]}>">
        <input type="hidden" name="search_emp_dept[]" value="<{$search_emp_dept[$smarty.section.cnt.index]}>">
        <input type="hidden" name="search_emp_room[]" value="<{$search_emp_room[$smarty.section.cnt.index]}>">
      <{/section}>
      <!--// ループ終了 -->
      <input type="hidden" name="vertical_axis" value="<{$vertical_axis}>">
      <input type="hidden" name="horizontal_axis" value="<{$horizontal_axis}>">
      <!-- 検索条件表示 -->
      <input type="hidden" name="vertical_label" value="<{$vertical_label}>">
      <input type="hidden" name="horizontal_label" value="<{$horizontal_label}>">
      <!-- ドリルダウン -->
      <input type="hidden" name="vertical_axises_detail" value="<{$vertical_axises_detail}>">
      <input type="hidden" name="horizontal_axises_detail" value="<{$horizontal_axises_detail}>">
      <input type="hidden" name="vertical_axises_summary_item" value="<{$vertical_axises_summary_item}>">
      <input type="hidden" name="horizontal_axises_summary_item" value="<{$horizontal_axises_summary_item}>">
      <input type="hidden" name="vertical_axises_post_class" value="<{$vertical_axises_post_class}>">
      <input type="hidden" name="vertical_axises_post_attribute" value="<{$vertical_axises_post_attribute}>">
      <input type="hidden" name="vertical_axises_post_dept" value="<{$vertical_axises_post_dept}>">
      <input type="hidden" name="horizontal_axises_post_class" value="<{$horizontal_axises_post_class}>">
      <input type="hidden" name="horizontal_axises_post_attribute" value="<{$horizontal_axises_post_attribute}>">
      <input type="hidden" name="horizontal_axises_post_dept" value="<{$horizontal_axises_post_dept}>">
      <!-- 非表示項目 -->
      <input type="hidden" name="vertical_no_disp_code_list_csv" value="<{$vertical_no_disp_code_list_csv}>">
      <input type="hidden" name="horizontal_no_disp_code_list_csv" value="<{$horizontal_no_disp_code_list_csv}>">
      <!-- 項目絞り込み情報文字列 -->
      <input type="hidden" name="item_shibori_info_string" value="<{$item_shibori_info_string|escape}>">
      <!-- 集計結果キャッシュ -->
      <input type="hidden" name="cells_report_id_list_full_string" value="<{$cells_report_id_list_full_string}>">
    </form>
    <iframe name="download" width="0" height="0" frameborder="0"></iframe>
    <!--------------------------------------------------------------------------------------------------------------------------->

    <!--------------------------------------------------------------------------------------------------------------------------->
    <!-- 他画面送信:印刷用フォーム -->
    <!--------------------------------------------------------------------------------------------------------------------------->
    <form name="print" action="hiyari_rm_stats_print.php" method="post" target="_blank">
      <input type="hidden" name="<{$session_name}>" value="<{$session_id}>">
      <!-- 送信条件 -->
      <input type="hidden" name="session" value="<{$session}>">
      <!-- 検索条件 -->
      <input type="hidden" name="count_date_mode" value="<{$count_date_mode}>">
      <input type="hidden" name="year" value="<{$year}>">
      <input type="hidden" name="month" value="<{$month}>">
      <input type="hidden" name="Date_Term" value="<{$Date_Term}>">
      <input type="hidden" name="sub_report_use" value="<{$sub_report_use}>">
      <!--// ループ開始 -->
      <{section name=cnt loop=$count_search}>
        <input type="hidden" name="search_emp_class[]" value="<{$search_emp_class[$smarty.section.cnt.index]}>">
        <input type="hidden" name="search_emp_attribute[]" value="<{$search_emp_attribute[$smarty.section.cnt.index]}>">
        <input type="hidden" name="search_emp_dept[]" value="<{$search_emp_dept[$smarty.section.cnt.index]}>">
        <input type="hidden" name="search_emp_room[]" value="<{$search_emp_room[$smarty.section.cnt.index]}>">
      <{/section}>
      <!--// ループ終了 -->
      <input type="hidden" name="vertical_axis" value="<{$vertical_axis}>">
      <input type="hidden" name="horizontal_axis" value="<{$horizontal_axis}>">
      <!-- 検索条件表示 -->
      <input type="hidden" name="vertical_label" value="<{$vertical_label}>">
      <input type="hidden" name="horizontal_label" value="<{$horizontal_label}>">
      <!-- ドリルダウン -->
      <input type="hidden" name="vertical_axises_detail" value="<{$vertical_axises_detail}>">
      <input type="hidden" name="horizontal_axises_detail" value="<{$horizontal_axises_detail}>">
      <input type="hidden" name="vertical_axises_summary_item" value="<{$vertical_axises_summary_item}>">
      <input type="hidden" name="horizontal_axises_summary_item" value="<{$horizontal_axises_summary_item}>">
      <input type="hidden" name="vertical_axises_post_class" value="<{$vertical_axises_post_class}>">
      <input type="hidden" name="vertical_axises_post_attribute" value="<{$vertical_axises_post_attribute}>">
      <input type="hidden" name="vertical_axises_post_dept" value="<{$vertical_axises_post_dept}>">
      <input type="hidden" name="horizontal_axises_post_class" value="<{$horizontal_axises_post_class}>">
      <input type="hidden" name="horizontal_axises_post_attribute" value="<{$horizontal_axises_post_attribute}>">
      <input type="hidden" name="horizontal_axises_post_dept" value="<{$horizontal_axises_post_dept}>">
      <!-- 非表示項目 -->
      <input type="hidden" name="vertical_no_disp_code_list_csv" value="<{$vertical_no_disp_code_list_csv}>">
      <input type="hidden" name="horizontal_no_disp_code_list_csv" value="<{$horizontal_no_disp_code_list_csv}>">
      <!-- 項目絞り込み情報文字列 -->
      <input type="hidden" name="item_shibori_info_string" value="<{$item_shibori_info_string|escape}>">
      <!-- 集計結果キャッシュ -->
      <input type="hidden" name="cells_report_id_list_full_string" value="<{$cells_report_id_list_full_string}>">
    </form>
  </div>
	<div id="footer">
		<p>Copyright Comedix all right reserved.</p>
	</div>
</body>
</html>
