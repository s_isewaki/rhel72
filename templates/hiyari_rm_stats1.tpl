<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <{$INCIDENT_TITLE}> | 統計分析</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/hiyari_rm_stats.js"></script>
<{include file="hiyari_post_select_js.tpl"}>
<script type="text/javascript">
    function init_page()
    {    
        <{* 集計部署の選択肢の初期化 *}>
        <{section name=cnt loop=$count_search}>
            set_post_select_box_obj_key("search_emp_post_<{$smarty.section.cnt.index}>",
                                        "search_emp_class_<{$smarty.section.cnt.index}>",
                                        "search_emp_attribute_<{$smarty.section.cnt.index}>",
                                        "search_emp_dept_<{$smarty.section.cnt.index}>",
                                        "search_emp_room_<{$smarty.section.cnt.index}>");
            setClassOptions("search_emp_post_<{$smarty.section.cnt.index}>",
                            "<{$search_emp_class[$smarty.section.cnt.index]}>",
                            "<{$search_emp_attribute[$smarty.section.cnt.index]}>",
                            "<{$search_emp_dept[$smarty.section.cnt.index]}>",
                            "<{$search_emp_room[$smarty.section.cnt.index]}>");
        <{/section}>
        
        <{* 縦軸・横軸選択肢の初期化 *}>
        vertical_axis_update();
        horizontal_axis_update();    
    }
  
    /////////////////////////////////////////////////////////////////////////////
    //縦軸・横軸の選択肢設定
    /////////////////////////////////////////////////////////////////////////////
    function vertical_axis_update()
    {
        var obj = document.mainform.vertical_axis;
        var selected_value = obj.options[obj.selectedIndex].value;

        //FFではdisplay:noneにすることができるが、IEの場合はできないので入れ替え
        deleteAllSelectBoxItem(obj);

        switch (document.mainform.count_date_mode.value){
        case "incident_date":
            if(selected_value == "report_month"){
                selected_value = "month";
            }
            <{foreach from=$vertical_axises key=vertical_axis_code item=vertical_axis_text}>
                <{if $vertical_axis_code != 'report_month'}>
                    addSelectBoxItem(obj, '<{$vertical_axis_code}>', '<{$vertical_axis_text|escape:"quotes"}>', selected_value);
                <{/if}>
            <{/foreach}>
            break;
        case "report_date":
            if(selected_value == "month"){
                selected_value = "report_month";
            }
            <{foreach from=$vertical_axises key=vertical_axis_code item=vertical_axis_text}>
                <{if $vertical_axis_code != 'month'}>
                    addSelectBoxItem(obj, '<{$vertical_axis_code}>', '<{$vertical_axis_text|escape:"quotes"}>', selected_value);
                <{/if}>
            <{/foreach}>
            break;
        }
    }
    
    function horizontal_axis_update()
    {
        var obj = document.mainform.horizontal_axis;
        var selected_value = obj.options[obj.selectedIndex].value;

        //FFではdisplay:noneにすることができるが、IEの場合はできないので入れ替え
        deleteAllSelectBoxItem(obj);

        switch(document.mainform.count_date_mode.value){
        case "incident_date":
            if(selected_value == "report_month"){
                selected_value = "month";
            }
            <{foreach from=$horizontal_axises key=horizontal_axis_code item=horizontal_axis_text}>
                <{if $horizontal_axis_code != 'report_month'}>
                    addSelectBoxItem(obj, '<{$horizontal_axis_code}>', '<{$horizontal_axis_text|escape:"quotes"}>', selected_value);
                <{/if}>
            <{/foreach}>
            break;
        case "report_date":
            if(selected_value == "month"){
                selected_value = "report_month";
            }
            <{foreach from=$horizontal_axises key=horizontal_axis_code item=horizontal_axis_text}>
                <{if $horizontal_axis_code != 'month'}>
                    addSelectBoxItem(obj, '<{$horizontal_axis_code}>', '<{$horizontal_axis_text|escape:"quotes"}>', selected_value);
                <{/if}>
            <{/foreach}>
        }
    }

    function addSelectBoxItem(box, value, text, selected_value)
    {
        var opt = document.createElement("option");
        opt.value = value;
        opt.text = text;

        //FireFox用
        if (selected_value == value){
            opt.setAttribute("selected", true);
        }

        box.options[box.length] = opt;

        //IE用
        if (selected_value == value){
            opt.setAttribute("selected", true);
        }

        try {box.style.fontSize = 'auto';} catch (e) {}
        box.style.overflow = 'auto';
    }
    
    function deleteAllSelectBoxItem(box)
    {
        for (var i = box.length - 1; i >= 0; i--){
            box.options[i] = null;
        }
    } 
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
.list_in {border-collapse:collapse;}
.list_in td {border:#FFFFFF solid 0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init_page()";>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <!-- ヘッダー -->
        <{include file="hiyari_header1.tpl"}>

        <img src="img/spacer.gif" width="1" height="5" alt=""><br>

        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="padding:3px 6px; float:left; background-color:#35B341; border:1px solid #ccc;">
              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                <a href="hiyari_rm_stats.php?session=<{$session}>" style="color:#FFFFFF">クロス集計</a>
              </font>
            </td>
            <td style="margin-left: 6px; padding:3px 6px; float:left; background-color:#f4f4f4; border:1px solid #ccc;">
              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                <a href="hiyari_rm_stats_pattern.php?session=<{$session}>" style="color:#00f">定型集計</a>
              </font>
            </td>
          </tr>
        </table>

        <img src="img/spacer.gif" width="1" height="5" alt=""><br>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
            <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
          </tr>
          <tr>
            <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
            <td bgcolor="#F5FFE5">
              <!----------------------------------------------------------------------------------- 
              集計条件領域 START
              ------------------------------------------------------------------------------------>
              <form name="mainform" action="hiyari_rm_stats.php" method="post">
                <input type="hidden" name="<{$session_name}>" value="<{$session_id}>">
                <input type="hidden" name="session" value="<{$session}>">
                <input type="hidden" name="postback_mode" value="count">
				<input type="hidden" id="get_stats_condition" name="get_stats_condition" value="">
                <table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
                  <tr height="22">
                    <!-- 集計条件:集計モード -->
                    <td width="80" align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計モード</font></td>
                    <td bgcolor="#FFFFFF">
                      <select id="count_date_mode" name="count_date_mode" onchange="vertical_axis_update();horizontal_axis_update()">
                        <option value="report_date" <{if $is_report_date_mode}>selected<{/if}> >報告日モード</option>
                        <option value="incident_date" <{if $is_incident_date_mode}>selected<{/if}> >発生日モード</option>
                      </select>
                    </td>
                    
                    <!-- 集計条件:集計年月 -->
                    <td width="80" align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計年月</font></td>
                    <td bgcolor="#FFFFFF" nowrap>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                        <select id="Date_Year" name="Date_Year">
                          <{foreach from=$year_list item=y}>
                            <option label="<{$y}>" value="<{$y}>" <{if $y==$year}>selected<{/if}>><{$y}></option>
                          <{/foreach}>
                        </select>年
                        
                        <select id="Date_Month" name="Date_Month">
                          <{foreach from=$month_list item=m}>
                            <option value="<{$m|string_format:'%02d'}>" <{if $m==$month}>selected<{/if}>><{$m}></option>
                          <{/foreach}>
                        </select>月〜

                        <select id="Date_Term" name="Date_Term">
                          <{foreach from=$month_list item=d}>
                            <option value="<{$d|string_format:'%02d'}>" <{if $d==$Date_Term}>selected<{/if}>><{$d}>ヶ月分</option>
                          <{/foreach}>
                        </select>
                      </font>
                    </td>
                    
                    <!-- 集計条件:副報告 -->
                    <td width="200" bgcolor="#FFFFFF" norap>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                      <input type="checkbox" name="sub_report_use" value="true" <{if $sub_report_use_flg}>checked<{/if}> >
                      副報告を集計に含める
                      </font>
                    </td>
                  </tr>
                </table>

                <img src="img/spacer.gif" width="1" height="5" alt=""><br>
                
                <!-- 集計条件:部署 START -->
                <{section name=cnt loop=$count_search}>
                  <table border='0' cellspacing='0' cellpadding='2' class="list">
                    <tr>
                      <td width="80" align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属</font></td>
                      <td bgcolor="#FFFFFF" nowrap>
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                          &nbsp;<{$arr_class_name[0]|escape}><select id="search_emp_class_<{$smarty.section.cnt.index}>" name="search_emp_class[]" onchange="setAtrbOptions('search_emp_post_<{$smarty.section.cnt.index}>');"></select>
                          &nbsp;<{$arr_class_name[1]|escape}><select id="search_emp_attribute_<{$smarty.section.cnt.index}>" name="search_emp_attribute[]" onchange="setDeptOptions('search_emp_post_<{$smarty.section.cnt.index}>');"></select>
                          &nbsp;<{$arr_class_name[2]|escape}><select id="search_emp_dept_<{$smarty.section.cnt.index}>" name="search_emp_dept[]" onchange="setRoomOptions('search_emp_post_<{$smarty.section.cnt.index}>');"></select>
                          <{if $post_select_data.class_cnt == 4}>
                            &nbsp;<{$arr_class_name[3]|escape}><select id="search_emp_room_<{$smarty.section.cnt.index}>" name="search_emp_room[]"></select>
                          <{/if}>
                          &nbsp;<input type="button" name="add_search" value="追加" onClick="search_add(this.form)">
                          <{if $smarty.section.cnt.index != 0}>
                              &nbsp;<input type="button" name="delete_search" value="削除" onClick="search_delete(this.form, <{$smarty.section.cnt.index}>)">
                          <{/if}>
                        </font>
                      </td>
                    </tr>
                  </table>
                <{/section}>
                
                <input type="hidden" name="count_search" value="<{if empty($count_search)}>0<{else}><{$count_search}><{/if}>">
                <input type="hidden" name="delete_line_no" value="">
                <!-- 集計条件:部署 END -->
                
                <hr>
                
                <!-- 集計条件:集計データ START -->
                <table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
                  <tr height="22">
                    <td width="80" align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計データ</font></td>
                      <td bgcolor="#FFFFFF">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                          <div id="item_shibori_disp_area">
                            <{if $item_shibori_info == ""}>
                              全てのデータ
                            <{else}>
                              <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list_in">
                                <tr>
                                  <td>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                      <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                                        <{foreach from=$item_shibori_info item=item_shibori_info1 name=shibori}>
                                          <tr>
                                            <td width="130" align="right" bgcolor="FFFBE7" style="border:#35B341 solid 1px;">
                                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                <{$item_shibori_info1.item_name|escape}>
                                              </font>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="border:#35B341 solid 1px;">
                                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                <{foreach from=$item_shibori_info1.code_list item=item_shibori_info2 name=code_list}>
                                                  <{if !$smarty.foreach.code_list.first}>、<{/if}>
                                                  <{$item_shibori_info2.code_name|escape}>
                                                <{/foreach}>
                                              </font>
                                            </td>
                                          </tr>
                                        <{/foreach}>
                                      </table>
                                    </font>
                                  </td>
                                  <td width="120" align="center" valign="bottom">
                                    <input type="button" id="item_shibori_clear_button" value="全てのデータ" onclick="item_shibori_clear();">
                                  </td>
                                </tr>
                              </table>
                            <{/if}>
                          </div>
                        <input type="hidden" id="is_item_shibori_clear" name="is_item_shibori_clear" value="false">
                      </font>
                    </td>
                  </tr>
                </table>
                <!-- 集計条件:集計データ END -->

                <img src="img/spacer.gif" width="1" height="5" alt=""><br>

                <table width="800" border="0" cellspacing="0" cellpadding="2" class="list">
                  <tr height="22">
                    <!-- 集計条件:縦軸 -->
                    <td width="80" align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>縦軸</nobr></font></td>
                    <td bgcolor="#FFFFFF">
                      <select name="vertical_axis">
                        <{foreach from=$vertical_axises key=vertical_axis_code item=vertical_axis_text}>
                          <option value="<{$vertical_axis_code}>" <{if $vertical_axis_code == $vertical_axis}>selected<{/if}>><{$vertical_axis_text}></option>
                        <{/foreach}>
                      </select>
                    </td>
                    
                    <!-- 集計条件:横軸 -->
                    <td width="80" align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>横軸</nobr></font></td>
                    <td bgcolor="#FFFFFF">
                      <select id="horizontal_axis" name="horizontal_axis">
                      <{foreach from=$horizontal_axises key=horizontal_axis_code item=horizontal_axis_text}>
                        <option value="<{$horizontal_axis_code}>" <{if $horizontal_axis_code == $horizontal_axis}>selected<{/if}> ><{$horizontal_axis_text}></option>
                      <{/foreach}>
                      </select>
                    </td>
                    
                    <!-- 集計ボタン -->
                    <td style="border-style:none;" width="99%">
                      <nobr>
                        <input type="submit" name="action" value="集計">
                        <input type="button" value="集計条件呼出" onclick="window.open('hiyari_rm_stats_condition_load.php?session=<{$session}>','stats_condition_load','directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=600,height=350')">
                        <input type="button" value="集計条件保存" onclick="document.getElementById('stats_condition_area').style.display='block';">
                      </nobr>
                    </td>
                  </tr>
                </table>
                
                <!-- 集計条件保存名 START -->
                <div id="stats_condition_area" style="display:none;">
                  <img src="img/spacer.gif" width="1" height="5" alt=""><br>
                  <table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
                    <tr height="22">
                      <td width="80" align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計条件保存名</font></td>
                      <td bgcolor="#FFFFFF">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list_in">
                            <tr>
                              <td>
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                  <input type="text" name="stats_condition" size=80>
                                </font>
                              </td>
                              <td width="120" align="center" valign="bottom">
                                <input type="hidden" name="is_stats_condition_set" value="">
                                <input type="button" value="集計条件保存" onclick="this.form.is_stats_condition_set.value=true;this.form.submit();">
                              </td>
                            </tr>
                          </table>
                        </font>
                      </td>
                    </tr>
                  </table>
                </div>
                <!-- 集計条件保存名 END -->
              </form>

              <!----------------------------------------------------------------------------------- 
              集計結果領域 START
              ------------------------------------------------------------------------------------>
              <{if $has_result}>
                <hr>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="left">
                      <table border="0" cellspacing="0" cellpadding="2" class="list">
                        <tr>
                          <td align="right" bgcolor="#DFFFDC" width="30"  ><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">縦軸</font></nobr></td>
                          <td align="left"  bgcolor="#FFFFFF" width="100" ><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$vertical_label|escape}></font></nobr></td>
                          <td align="right" bgcolor="#DFFFDC" width="30"  ><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">横軸</font></nobr></td>
                          <td align="left"  bgcolor="#FFFFFF" width="100" ><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$horizontal_label|escape}></font></nobr></td>
                        </tr>
                      </table>
                    </td>
                    <td align="right" nowrap>
                      <input type="button" value="集計データ設定" style="width:110px" onclick="item_shibori_change();">
                      <input type="button" value="指定項目を除外" style="width:110px" onclick="item_no_disp_action();">
                      <{if $vertical_no_disp_code_list != "" || $horizontal_no_disp_code_list != ""}>
                        <input type="button" value="除外解除" style="width:110px" onclick="item_all_disp_action();">
                      <{/if}>
                      <{if $vertical_axises_detail != ""}>
                        <input type="button" value="縦軸を戻す" style="width:110px" onclick="vertical_axis_detail_return('<{$vertical_axises_detail_link_return}>');">
                      <{/if}>
                      <{if $horizontal_axises_detail != ""}>
                        <input type="button" value="横軸を戻す" style="width:110px" onclick="horizontal_axis_detail_return('<{$horizontal_axises_detail_link_return}>');">
                      <{/if}>
                      <input type="button" value="印刷" style="width:110px" onclick="print_download();">
                      <{if $excel_disp_flg.collect_excel_btn_show_flg == 't'}>
                        <input type="button" value="EXCEL出力" style="width:110px" onclick="excel_download();">
                      <{/if}>
                      <input type="button" value="グラフ" style="width:110px" onclick="chart_disp('<{$session}>', '<{$session_name}>', '<{$session_id}>');">
                    </td>
                  </tr>
                </table>

                <!-- 集計結果 START -->
                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                  <!---------------------------------------------
                  タイトル行（列タイトル）
                  ---------------------------------------------->
                  <tr height="22" bgcolor="#DFFFDC" valign="top">
                    <td width="18%"></td>                    
                    <{foreach from=$title_row item=cell}>
                      <td width="<{$col_width}>%">
                        <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="list_in">
                          <tr>
                            <td valign="top" height="20" align="center">
                              <input type="checkbox" name="horizontal_axis_checkbox[]" value="<{$cell.easy_code}>" <{$cell.checked}> >
                            </td>
                          </tr>
                          <tr>
                            <td valign="top">
                              <{if $cell.horizontal_link_flg}>
                                <a href="javascript:horizontal_axis_detail_action('<{$horizontal_axises_detail_link}>','<{$cell.easy_code}>');">
                              <{/if}>
                                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$cell.easy_name|escape}></font>
                              <{if $cell.horizontal_link_flg}>
                                </a>
                              <{/if}>
                            </td>
                          </tr>
                          <tr>
                            <td valign="bottom">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list_in">
                                <{foreach from=$cell.btn_list item=btn}>
                                  <tr>
                                    <td align="center">
                                      <input type="button" value="<{$btn.value}>" onclick="javascript:horizontal_axis_detail_action('<{$btn.detail}>','<{$cell.easy_code}>');" <{$btn.disabled}>>
                                    </td>
                                  </tr>
                                <{/foreach}>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    <{/foreach}>
                    
                    <td width="<{$col_width}>%">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">計</font>
                    </td>
                  </tr>

                  <!---------------------------------------------
                  詳細行
                  ---------------------------------------------->
                  <{foreach from=$detail_rows item=row}>
                    <tr height="22" valign="top">
                      <!-- 行タイトル START --> 
                      <td bgcolor="#DFFFDC" nowrap>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list_in">
                          <tr>
                            <td align="left" width="20">
                              <input type="checkbox" name="vertical_axis_checkbox[]" value="<{$row.easy_code}>" <{$row.checked}> >
                            </td>
                            <td align="left">
                              <{if $row.vertical_link_flg}>
                                <a href="javascript:vertical_axis_detail_action('<{$vertical_axises_detail_link}>','<{$row.easy_code}>');">
                              <{/if}>
                              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$row.easy_name|escape}></font>
                              <{if $row.vertical_link_flg}>
                                </a>
                              <{/if}>
                            </td>
                            <{foreach from=$row.btn_list item=btn}>
                              <td width="40">
                                <input type="button" value="<{$btn.value}>" onclick="javascript:vertical_axis_detail_action('<{$btn.detail}>','<{$row.easy_code}>');" <{$btn.disabled}> >
                              </td>
                            <{/foreach}>
                          </tr>
                        </table>
                      </td>
                      <!-- 行タイトル END --> 
                      
                      <!-- 件数 START -->
                      <{foreach from=$row.cells item=cell}>
                        <td align="right" bgcolor="#FFFFFF">
                          <{if $cell.count != 0}>
                            <a href="javascript:void(0)" onclick="show_report_list_window('<{$v_name_list[$row.index]}>', '<{$h_name_list[$cell.index]}>','<{$cell.id_list}>');return false;">
                          <{/if}>
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$cell.count}></font>
                          <{if $cell.count != 0}>
                            </a>
                          <{/if}>
                        </td>
                      <{/foreach}>
                      <!-- 件数 END -->
                      
                      <!-- 行合計 START -->
                      <td align="right" bgcolor="#FFFFFF">
                        <{if $row.sum != 0}>
                          <a href="javascript:void(0)" onclick="show_report_list_window('<{$v_name_list[$row.index]}>', '合計', '<{$row.id_list}>');return false;">
                        <{/if}>
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$row.sum}></font>
                        <{if $row.sum != 0}>
                          </a>
                        <{/if}>
                      </td>
                      <!-- 行合計 END -->                      
                    </tr>
                  <{/foreach}>
                  
                  <!---------------------------------------------
                  合計行
                  ---------------------------------------------->
                  <tr height="22" valign="top">
                    <td bgcolor="#DFFFDC">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">計</font>
                    </td>

                    <!-- 列合計 START -->
                    <{foreach from=$sum_row item=cell}>
                      <td align="right" bgcolor="#FFFFFF">
                        <{if $cell.sum != 0}>
                          <a href="javascript:void(0)" onclick="show_report_list_window('合計', '<{$h_name_list[$cell.index]}>', '<{$cell.id_list}>');return false;">
                        <{/if}>
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$cell.sum}></font>
                        <{if $cell.sum != 0}>
                          </a>
                        <{/if}>
                      </td>
                    <{/foreach}>
                    <!-- 列合計 END -->

                    <!-- 総合計 START -->
                    <td align="right" bgcolor="#FFFFFF">
                      <{if $sums_sum != 0}>
                        <a href="javascript:void(0)" onclick="show_report_list_window('合計', '合計', '<{$sum_id_list}>');return false;">
                      <{/if}>
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$sums_sum}></font>
                      <{if $sums_sum != 0}>
                        </a>
                      <{/if}>
                    </td>
                    <!-- 総合計 END -->
                  </tr>
                </table>
                <!-- 集計結果 END -->
              <{/if}>
              <!-- 集計結果領域 END -->
            </td>
            <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
          </tr>
          <tr>
            <td><img src="img/r_3.gif" width="10" height="10"></td>
            <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td><img src="img/r_4.gif" width="10" height="10"></td>
          </tr>
        </table>

        <!--------------------------------------------------------------------------------------------------------------------------->
        <!-- 軸詳細用フォーム(前回送信データを元に実行) -->
        <!--------------------------------------------------------------------------------------------------------------------------->
        <form id="result_edit_form" name="result_edit_form" action="hiyari_rm_stats.php" method="post">
          <input type="hidden" name="<{$session_name}>" value="<{$session_id}>">
          <!-- 送信条件 -->
          <input type="hidden" name="session" value="<{$session}>">
          <input type="hidden" id="postback_mode" name="postback_mode" value="">
          <!-- 検索条件 -->
          <input type="hidden" name="count_date_mode" value="<{$count_date_mode}>">
          <input type="hidden" name="year" value="<{$year}>">
          <input type="hidden" name="month" value="<{$month}>">
          <input type="hidden" name="Date_Year" value="<{$year}>">
          <input type="hidden" name="Date_Month" value="<{$month}>">
          <input type="hidden" name="Date_Term" value="<{$Date_Term}>">
          <input type="hidden" name="sub_report_use" value="<{$sub_report_use}>">
          <input type="hidden" name="count_search" value="<{if empty($count_search)}>0<{else}><{$count_search}><{/if}>">
          <input type="hidden" name="delete_line_no" value="">
          <{section name=cnt loop=$count_search}>
            <input type="hidden" name="search_emp_class[]" value="<{$search_emp_class[$smarty.section.cnt.index]}>">
            <input type="hidden" name="search_emp_attribute[]" value="<{$search_emp_attribute[$smarty.section.cnt.index]}>">
            <input type="hidden" name="search_emp_dept[]" value="<{$search_emp_dept[$smarty.section.cnt.index]}>">
            <input type="hidden" name="search_emp_room[]" value="<{$search_emp_room[$smarty.section.cnt.index]}>">
          <{/section}>
          <input type="hidden" name="vertical_axis" value="<{$vertical_axis}>">
          <input type="hidden" name="horizontal_axis" value="<{$horizontal_axis}>">
          <!-- ドリルダウン -->
          <input type="hidden" name="vertical_axises_detail" value="<{$vertical_axises_detail}>">
          <input type="hidden" name="vertical_axises_detail_old" value="<{$vertical_axises_detail}>">
          <input type="hidden" name="horizontal_axises_detail" value="<{$horizontal_axises_detail}>">
          <input type="hidden" name="horizontal_axises_detail_old" value="<{$horizontal_axises_detail}>">
          <input type="hidden" name="vertical_axises_summary_item" value="<{$vertical_axises_summary_item}>">
          <input type="hidden" name="horizontal_axises_summary_item" value="<{$horizontal_axises_summary_item}>">
          <input type="hidden" name="vertical_axises_post_class" value="<{$vertical_axises_post_class}>">
          <input type="hidden" name="vertical_axises_post_attribute" value="<{$vertical_axises_post_attribute}>">
          <input type="hidden" name="vertical_axises_post_dept" value="<{$vertical_axises_post_dept}>">
          <input type="hidden" name="horizontal_axises_post_class" value="<{$horizontal_axises_post_class}>">
          <input type="hidden" name="horizontal_axises_post_attribute" value="<{$horizontal_axises_post_attribute}>">
          <input type="hidden" name="horizontal_axises_post_dept" value="<{$horizontal_axises_post_dept}>">
          <!-- 非表示項目 -->
          <input type="hidden" name="vertical_no_disp_code_list_csv" value="<{$vertical_no_disp_code_list_csv}>">
          <input type="hidden" name="horizontal_no_disp_code_list_csv" value="<{$horizontal_no_disp_code_list_csv}>">
          <!-- 項目絞り込み情報文字列 -->
          <input type="hidden" name="item_shibori_info_string" value="<{$item_shibori_info_string|escape}>">
          <!-- 非表示項目(チェックボックス表示用) -->
          <input type="hidden" name="vertical_no_disp_code_list_old_csv" value="">
          <input type="hidden" name="horizontal_no_disp_code_list_old_csv" value="">
          <!-- 項目絞り込み変更 -->
          <input type="hidden" name="vertical_item_shibori_change_csv" value="">
          <input type="hidden" name="horizontal_item_shibori_change_csv" value="">
          <!-- 項目絞り込みクリア -->
          <input type="hidden" id="is_item_shibori_clear_of_result_edit_form" name="is_item_shibori_clear" value="false">
          <!-- 集計結果キャッシュ -->
          <input type="hidden" name="cells_report_id_list_full_string" value="<{$cells_report_id_list_full_string}>">
          <!-- 集計条件取得 -->
          <input type="hidden" id="get_stats_condition" name="get_stats_condition" value="">
        </form>
        <!--------------------------------------------------------------------------------------------------------------------------->

        <!--------------------------------------------------------------------------------------------------------------------------->
        <!-- 他画面送信:子画面用フォーム -->
        <!--------------------------------------------------------------------------------------------------------------------------->
        <form name="report_id_list_form" action="hiyari_rm_stats_reports.php" method="post" target="rm_stats_reports_window">
          <input type="hidden" name="<{$session_name}>" value="<{$session_id}>">
          <!-- 送信条件 -->
          <input type="hidden" name="session" value="<{$session}>">
          <!-- 検索条件 -->
          <input type="hidden" name="count_date_mode" value="<{$count_date_mode}>">
          <!-- 検索条件表示 -->
          <input type="hidden" name="vertical_label" value="<{$vertical_label}>">
          <input type="hidden" name="horizontal_label" value="<{$horizontal_label}>">
          <input type="hidden" name="vertical_item_label" value="">
          <input type="hidden" name="horizontal_item_label" value="">
          <!-- 集計データ -->
          <input type="hidden" name="report_id_list" value="">
        </form>
        <!--------------------------------------------------------------------------------------------------------------------------->

        <!--------------------------------------------------------------------------------------------------------------------------->
        <!-- 他画面送信:Excel用フォーム -->
        <!--------------------------------------------------------------------------------------------------------------------------->
        <form name="xls" action="hiyari_rm_stats_excel.php" method="post" target="download">
          <input type="hidden" name="<{$session_name}>" value="<{$session_id}>">
          <!-- 送信条件 -->
          <input type="hidden" name="session" value="<{$session}>">
          <!-- 検索条件 -->
          <input type="hidden" name="count_date_mode" value="<{$count_date_mode}>">
          <input type="hidden" name="year" value="<{$year}>">
          <input type="hidden" name="month" value="<{$month}>">
          <input type="hidden" name="Date_Term" value="<{$Date_Term}>">
          <input type="hidden" name="sub_report_use" value="<{$sub_report_use}>">
          <!--// ループ開始 -->
          <{section name=cnt loop=$count_search}>
            <input type="hidden" name="search_emp_class[]" value="<{$search_emp_class[$smarty.section.cnt.index]}>">
            <input type="hidden" name="search_emp_attribute[]" value="<{$search_emp_attribute[$smarty.section.cnt.index]}>">
            <input type="hidden" name="search_emp_dept[]" value="<{$search_emp_dept[$smarty.section.cnt.index]}>">
            <input type="hidden" name="search_emp_room[]" value="<{$search_emp_room[$smarty.section.cnt.index]}>">
          <{/section}>
          <!--// ループ終了 -->
          <input type="hidden" name="vertical_axis" value="<{$vertical_axis}>">
          <input type="hidden" name="horizontal_axis" value="<{$horizontal_axis}>">
          <!-- 検索条件表示 -->
          <input type="hidden" name="vertical_label" value="<{$vertical_label}>">
          <input type="hidden" name="horizontal_label" value="<{$horizontal_label}>">
          <!-- ドリルダウン -->
          <input type="hidden" name="vertical_axises_detail" value="<{$vertical_axises_detail}>">
          <input type="hidden" name="horizontal_axises_detail" value="<{$horizontal_axises_detail}>">
          <input type="hidden" name="vertical_axises_summary_item" value="<{$vertical_axises_summary_item}>">
          <input type="hidden" name="horizontal_axises_summary_item" value="<{$horizontal_axises_summary_item}>">
          <input type="hidden" name="vertical_axises_post_class" value="<{$vertical_axises_post_class}>">
          <input type="hidden" name="vertical_axises_post_attribute" value="<{$vertical_axises_post_attribute}>">
          <input type="hidden" name="vertical_axises_post_dept" value="<{$vertical_axises_post_dept}>">
          <input type="hidden" name="horizontal_axises_post_class" value="<{$horizontal_axises_post_class}>">
          <input type="hidden" name="horizontal_axises_post_attribute" value="<{$horizontal_axises_post_attribute}>">
          <input type="hidden" name="horizontal_axises_post_dept" value="<{$horizontal_axises_post_dept}>">
          <!-- 非表示項目 -->
          <input type="hidden" name="vertical_no_disp_code_list_csv" value="<{$vertical_no_disp_code_list_csv}>">
          <input type="hidden" name="horizontal_no_disp_code_list_csv" value="<{$horizontal_no_disp_code_list_csv}>">
          <!-- 項目絞り込み情報文字列 -->
          <input type="hidden" name="item_shibori_info_string" value="<{$item_shibori_info_string|escape}>">
          <!-- 集計結果キャッシュ -->
          <input type="hidden" name="cells_report_id_list_full_string" value="<{$cells_report_id_list_full_string}>">
        </form>
        <iframe name="download" width="0" height="0" frameborder="0"></iframe>
        <!--------------------------------------------------------------------------------------------------------------------------->

        <!--------------------------------------------------------------------------------------------------------------------------->
        <!-- 他画面送信:印刷用フォーム -->
        <!--------------------------------------------------------------------------------------------------------------------------->
        <form name="print" action="hiyari_rm_stats_print.php" method="post" target="_blank">
          <input type="hidden" name="<{$session_name}>" value="<{$session_id}>">
          <!-- 送信条件 -->
          <input type="hidden" name="session" value="<{$session}>">
          <!-- 検索条件 -->
          <input type="hidden" name="count_date_mode" value="<{$count_date_mode}>">
          <input type="hidden" name="year" value="<{$year}>">
          <input type="hidden" name="month" value="<{$month}>">
          <input type="hidden" name="Date_Term" value="<{$Date_Term}>">
          <input type="hidden" name="sub_report_use" value="<{$sub_report_use}>">
          <!--// ループ開始 -->
          <{section name=cnt loop=$count_search}>
            <input type="hidden" name="search_emp_class[]" value="<{$search_emp_class[$smarty.section.cnt.index]}>">
            <input type="hidden" name="search_emp_attribute[]" value="<{$search_emp_attribute[$smarty.section.cnt.index]}>">
            <input type="hidden" name="search_emp_dept[]" value="<{$search_emp_dept[$smarty.section.cnt.index]}>">
            <input type="hidden" name="search_emp_room[]" value="<{$search_emp_room[$smarty.section.cnt.index]}>">
          <{/section}>
          <!--// ループ終了 -->
          <input type="hidden" name="vertical_axis" value="<{$vertical_axis}>">
          <input type="hidden" name="horizontal_axis" value="<{$horizontal_axis}>">
          <!-- 検索条件表示 -->
          <input type="hidden" name="vertical_label" value="<{$vertical_label}>">
          <input type="hidden" name="horizontal_label" value="<{$horizontal_label}>">
          <!-- ドリルダウン -->
          <input type="hidden" name="vertical_axises_detail" value="<{$vertical_axises_detail}>">
          <input type="hidden" name="horizontal_axises_detail" value="<{$horizontal_axises_detail}>">
          <input type="hidden" name="vertical_axises_summary_item" value="<{$vertical_axises_summary_item}>">
          <input type="hidden" name="horizontal_axises_summary_item" value="<{$horizontal_axises_summary_item}>">
          <input type="hidden" name="vertical_axises_post_class" value="<{$vertical_axises_post_class}>">
          <input type="hidden" name="vertical_axises_post_attribute" value="<{$vertical_axises_post_attribute}>">
          <input type="hidden" name="vertical_axises_post_dept" value="<{$vertical_axises_post_dept}>">
          <input type="hidden" name="horizontal_axises_post_class" value="<{$horizontal_axises_post_class}>">
          <input type="hidden" name="horizontal_axises_post_attribute" value="<{$horizontal_axises_post_attribute}>">
          <input type="hidden" name="horizontal_axises_post_dept" value="<{$horizontal_axises_post_dept}>">
          <!-- 非表示項目 -->
          <input type="hidden" name="vertical_no_disp_code_list_csv" value="<{$vertical_no_disp_code_list_csv}>">
          <input type="hidden" name="horizontal_no_disp_code_list_csv" value="<{$horizontal_no_disp_code_list_csv}>">
          <!-- 項目絞り込み情報文字列 -->
          <input type="hidden" name="item_shibori_info_string" value="<{$item_shibori_info_string|escape}>">
          <!-- 集計結果キャッシュ -->
          <input type="hidden" name="cells_report_id_list_full_string" value="<{$cells_report_id_list_full_string}>">
        </form>
        <!--------------------------------------------------------------------------------------------------------------------------->
      </td>
    </tr>
  </table>
</body>
</html>
