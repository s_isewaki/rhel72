<{include file="shift/header.tpl" title="管理画面：オプション" tab="08"}>

<{include file="shift/adm_options_submenu.tpl"}>

<div id="main">

    <div class="attention">
        運用開始後に、この設定を変更すると、勤務表の予定、
        実績、集計値など全てのデータの整合性に影響が出ます。
        ご注意ください。
    </div>

    <form id="cours" action="shift/adm_options_cours.php" method="post">
        <input type="hidden" name="mode" value="confirm" />

        <fieldset>
            <legend>基準となる年</legend>
            <label><input type="text" name="base_year" size="5" maxlength="4" value="<{$base_year|escape}>" class="eng" />年</label>
            <{if $err.base_year}><p class="err"><{$err.base_year}></p><{/if}>
        </fieldset>

        <fieldset>
            <legend>基準年の第1クールの初日の日付</legend>
            <label><input type="text" id="datepicker" name="base_date" size="11" maxlength="10" value="<{$base_date|escape}>" class="eng" /></label>
            <{if $err.base_date}><p class="err"><{$err.base_date}></p><{/if}>
        </fieldset>

        <fieldset>
            <legend>1年間のクール数</legend>
            <label><input type="number" name="base_count" size="4" maxlength="3" value="<{$base_count|default:13|escape}>" class="num" /></label>
            <{if $err.base_count}><p class="err"><{$err.base_count}></p><{/if}>
        </fieldset>

        <button type="submit" class="confirm">登録する</button>
    </form>
</div>

<{capture name="js"}>
    <script type="text/javascript" src="js/shift/adm_options.js"></script>
<{/capture}>
<{include file="shift/footer.tpl"}>
