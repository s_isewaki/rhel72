<{include file="shift/header.tpl" title="管理画面：オプション" tab="08"}>

<{include file="shift/adm_options_submenu.tpl"}>

<div id="main">
    <form action="shift/adm_options.php" method="post">
        <input type="hidden" name="mode" value="confirm" />

        <fieldset>
            <legend>打刻システム連携</legend>
            <label><input type="radio" name="dakoku_use" value="1" <{if $dakoku_use === '1'}>checked<{/if}>/> 利用する</label>
            <label><input type="radio" name="dakoku_use" value="0" <{if $dakoku_use === '0'}>checked<{/if}> /> 利用しない</label>
            <{if $err.dakoku_use}><p class="err"><{$err.dakoku_use}></p><{/if}>
        </fieldset>

        <fieldset>
            <legend>打刻システムURL</legend>
            <input type="text" name="dakoku_url" value="<{$dakoku_url|escape}>" size="100" class="eng" />
        </fieldset>

        <fieldset>
            <legend>個人勤務表:残業入力期限</legend>
            <input type="text" name="zangyo_limit_days" value="<{$zangyo_limit_days|escape}>" size="3" class="num" />日前まで
            <{if $err.zangyo_limit_days}><p class="err"><{$err.zangyo_limit_days}></p><{/if}>
        </fieldset>

        <fieldset>
            <legend>個人勤務表:勤務希望入力</legend>
            <label><input type="radio" name="personal_hope_use" value="1" <{if $personal_hope_use === '1'}>checked<{/if}>/> 利用する</label>
            <label><input type="radio" name="personal_hope_use" value="0" <{if $personal_hope_use === '0'}>checked<{/if}>/> 利用しない</label>
            <{if $err.personal_hope_use}><p class="err"><{$err.personal_hope_use}></p><{/if}>
        </fieldset>

        <fieldset>
            <legend>ランチャーによる打刻</legend>
            <label><input type="radio" name="launcher_dakoku" value="1" <{if $launcher_dakoku === '1'}>checked<{/if}>/> 利用する</label>
            <label><input type="radio" name="launcher_dakoku" value="0" <{if $launcher_dakoku === '0'}>checked<{/if}>/> 利用しない</label>
            <{if $err.launcher_dakoku}><p class="err"><{$err.launcher_dakoku}></p><{/if}>
        </fieldset>

        <fieldset>
            <legend>COMPANY連携（未処理件数取得）</legend>
            <label><input type="radio" name="notice_company_use" value="1" <{if $notice_company_use === '1'}>checked<{/if}>/> 利用する</label>
            <label><input type="radio" name="notice_company_use" value="0" <{if $notice_company_use === '0'}>checked<{/if}>/> 利用しない</label>
            <{if $err.notice_company_use}><p class="err"><{$err.notice_company_use}></p><{/if}>
        </fieldset>

        <fieldset>
            <legend>COMPANY連携URL</legend>
            <input type="text" name="notice_company_url" value="<{$notice_company_url|escape}>" size="100" class="eng" />
            <{if $err.notice_company_url}><p class="err"><{$err.notice_company_url}></p><{/if}>
        </fieldset>

        <fieldset>
            <legend>COMPANY連携タイムアウト時間</legend>
            <input type="text" name="notice_company_timeout" value="<{$notice_company_timeout|escape}>" size="3" class="num" />秒でタイムアウト ※無制限の場合は 0 として下さい
            <{if $err.notice_company_timeout}><p class="err"><{$err.notice_company_timeout}></p><{/if}>
        </fieldset>

        <fieldset>
            <legend>COMPANY画面遷移先URL</legend>
            <input type="text" name="notice_company_transition_url" value="<{$notice_company_transition_url|escape}>" size="100" class="eng" />
            <{if $err.notice_company_transition_url}><p class="err"><{$err.notice_company_transition_url}></p><{/if}>
        </fieldset>

        <button type="submit">登録する</button>
    </form>
</div>

<{capture name="js"}>
    <script type="text/javascript" src="js/shift/adm_options.js"></script>
<{/capture}>
<{include file="shift/footer.tpl"}>
