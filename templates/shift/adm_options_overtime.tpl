<{include file="shift/header.tpl" title="管理画面：オプション" tab="08"}>

<{include file="shift/adm_options_submenu.tpl"}>

<div id="main">
    <form action="shift/adm_options_overtime.php" method="post">
        <input type="hidden" name="mode" value="confirm" />

        <fieldset>
            <legend>警告時間（この時間を超えた残業の申請時に「警告メッセージ」が表示されます。）</legend>
            <input type="text" name="warning_time" value="<{$warning_time|escape}>" size="4" class="num" />時間 ※無制限の場合は空欄として下さい
            <{if $err.warning_time}><p class="err"><{$err.warning_time}></p><{/if}>
        </fieldset>

        <fieldset>
            <legend>警告メッセージ</legend>
            <input type="text" name="warning_message" value="<{$warning_message|escape}>" size="100" class="jpn" />
            <{if $err.warning_message}><p class="err"><{$err.warning_message}></p><{/if}>
        </fieldset>

        <button type="submit">登録する</button>
    </form>
</div>

<{capture name="js"}>
    <script type="text/javascript" src="js/shift/adm_options.js"></script>
<{/capture}>
<{include file="shift/footer.tpl"}>
