<{include file="shift/header.tpl" title="管理画面：データ出力" tab="07"}>

<div id="submenu">
    <ul>
        <li>
            <a href="shift/adm_output.php"><span>データ出力</span></a>
        </li>
        <li>
            <a href="shift/adm_output_setting.php"><span>レイアウト設定</span></a>
        </li>
        <li>
            <a href="shift/adm_output_batch_setting.php"><span>バッチ出力</span></a>
        </li>
    </ul>
</div>
<hr style="margin:10px 0;"/>

<div style="margin:20px 10px;">
    <form method="post" action="shift/adm_output_setting_edit.php">

        <table class="form">
            <tr>
                <td>名称</td>
                <td><input type="text" name="title" value="<{$layout.title|escape}>" /></td>
            </tr>
            <tr>
                <td>出力する文字コード</td>
                <td>
                    <select name="encoding">
                        <option value="shift_jis" <{if $layout.encoding === 'shift_jis'}>selected="selected"<{/if}>>Shift_JIS</option>
                        <option value="utf-8" <{if $layout.encoding === 'utf-8'}>selected="selected"<{/if}>>UTF-8</option>
                        <option value="euc-jp" <{if $layout.encoding === 'euc-jp'}>selected="selected"<{/if}>>EUC-JP</option>
                    </select>
                </td>
            </tr>
        </table>

        <b>■レイアウト</b>

        <table class="list">
            <thead>
                <tr>
                    <td>ヘッダ</td>
                    <td>名称</td>
                    <td>種類</td>
                    <td>設定</td>
                    <td><button type="button" class="js-row-insert-top">行追加</button></td>
                    <td>行削除</td>
                </tr>
            </thead>
            <tbody id="layout">
                <{foreach from=$layout.layout key=i item=row}>
                    <tr id="i<{$i}>">
                        <td>
                            <input type="hidden" name="order[]" value="i<{$i}>" />
                            <input type="text" class="row-header" name="header[i<{$i}>]" size="10" value="<{$row.header|escape}>" />
                        </td>
                        <td><input type="text" class="row-title" name="name[i<{$i}>]" size="20" value="<{$row.name|escape}>" /></td>
                        <td><select name="type[i<{$i}>]" class="js-type-select"><{pulldown array=$layout_lists id=$row.type}></select></td>
                        <td>
                            <{if $row.type === 'count'}>
                                <button type="button" style="font-size:8px;" class="js-pattern-insert">記号追加</button>
                                <{foreach from=$row.count item=ptn}>
                                    <div>
                                        <select class="pattern-name" name="count[i<{$i}>][pattern][]"><{pulldown array=$pattern_lists id=$ptn.pattern}></select>
                                        × <input type="text" name="count[i<{$i}>][coeffient][]" size="3" value="<{$ptn.coeffient|escape|default:'1'}>" />(係数)
                                        <button type="button" class="js-pattern-remove">×</button>
                                    </div>
                                <{/foreach}>
                            <{elseif $row.type|regex_replace:'/^.*_date$/':'_date_' === '_date_'}>
                                <select name="format[i<{$i}>]"><{pulldown array=$layout_date_lists id=$row.format}></select>
                            <{/if}>
                        </td>
                        <td><button type="button" class="js-row-insert">行追加</button></td>
                        <td><button type="button" class="js-row-remove">行削除</button></td>
                    </tr>
                <{/foreach}>
            </tbody>
        </table>

        <input type="hidden" name="id" value="<{$id|escape}>" />
        <button type="submit" name="save" value="save">保存する</button>
    </form>
</div>

<{capture name="js"}>
    <script type="text/javascript">
<!--

        // レイアウト一覧の追加行
        function layout_row(id) {
            var row = [
                '<tr id="' + id + '">',
                '<td><input type="hidden" name="order[]" value="' + id + '" /><input type="text" class="row-header" name="header[' + id + ']" size="10" /></td>',
                '<td><input type="text" class="row-title" name="name[' + id + ']" size="20" /></td>',
                '<td><select name="type[' + id + ']" class="js-type-select"><{pulldown array=$layout_lists}></select></td>',
                '<td></td>',
                '<td><button type="button" class="js-row-insert">行追加</button></td>',
                '<td><button type="button" class="js-row-remove">行削除</button></td>',
                '</tr>'
            ].join('');
            return row;
        }

        // レイアウト一覧の日付フォーマット選択
        function layout_date(id) {
            return '<select name="format[' + id + ']"><{pulldown array=$layout_date_lists}></select>';
        }

        // 勤務記号の追加行
        function layout_pattern(id) {
            var ptn = [
                '<div>',
                '<select class="pattern-name" name="count[' + id + '][pattern][]"><{pulldown array=$pattern_lists}></select>',
                ' × <input type="text" name="count[' + id + '][coeffient][]" size="3" value="1" />(係数)',
                ' <button type="button" class="js-pattern-remove">×</button>',
                '</div>'
            ].join('');
            return ptn;
        }

        // ID生成
        function create_id() {
            var $id = Math.random().toString(36).slice(-8);
            if ($('#' + $id).get(0)) {
                $id = create_id();
            }
            return $id;
        }

        $(function() {

            // 行追加（ヘッダ）
            $('.js-row-insert-top').on('click', function() {
                $('#layout').prepend(layout_row(create_id()));
            });

            // 行追加
            $('body').on('click', '.js-row-insert', function() {
                $(this).parent().parent().after(layout_row(create_id()));
            });

            // 行削除
            $('body').on('click', '.js-row-remove', function() {
                var title = $(this).parent().parent().find('input.row-title').val();
                if (confirm('[' + title + ']を削除してよろしいですか？')) {
                    $(this).parent().parent().remove();
                }
            });

            // 勤務記号追加
            $('body').on('click', '.js-pattern-insert', function() {
                var id = $(this).parent().parent().attr('id');
                $(this).parent().append(layout_pattern(id));
            });

            // 勤務記号削除
            $('body').on('click', '.js-pattern-remove', function() {
                var pattern = $(this).parent().find('select.pattern-name').find('option:selected').text();
                if (confirm('[' + pattern + ']を削除してよろしいですか？')) {
                    $(this).parent().remove();
                }
            });

            // 種類選択
            $('body').on('change', '.js-type-select', function() {
                var val = $(this).val();
                var id = $(this).parent().parent().attr('id');

                // 勤務記号
                if (val === 'count') {
                    $(this).parent().next().html('<button type="button" style="font-size:8px;" class="js-pattern-insert">記号追加</button>' + layout_pattern(id));
                }
                else if (val.match(/_date$/)) {
                    $(this).parent().next().html(layout_date(id));
                }
                else {
                    $(this).parent().next().html('');
                }

                // 名称設定
                var title = $(this).parent().parent().find('input.row-title');
                if (!title.val()) {
                    title.val($(this).find('option:selected').text());
                }
            });

        });

        // -->
    </script>
<{/capture}>

<{include file="shift/footer.tpl"}>
