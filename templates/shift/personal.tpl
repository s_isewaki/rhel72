<{include file="shift/header.tpl" title="個人勤務表" tab="10"}>

<{if ! $staff->group_id()}>
    <div class="attention">あなたが所属している部署がありません。</div>
<{else}>

    <script type="text/javascript">
        <!--
        var base_href = '<{$BASE_HREF}>/';
        var shift_pattern = '<{pattern_pulldown array=$pattern_list}>';
        // -->
    </script>

    <div id="main">
        <div class="pulldown"><b><{$ward->group_name()|escape}></b>:</div>

        <!-- 月・クール切り替えタブ -->
        <div id="month">
            <ul>
                <li><a href="shift/personal.php?y=<{$lastyear}>&m=<{$month}>"><span>&lt;前年</span></a></li>
                    <{foreach from=$yearmonth item=row}>
                    <li <{if $row.i === 0}>class="selected"<{/if}>><a href="shift/personal.php?y=<{$row.year}>&m=<{$row.month}>"><span><{$row.vyear}>/<{$row.vmonth}></span></a></li>
                    <{/foreach}>
                <li><a href="shift/personal.php?y=<{$nextyear}>&m=<{$month}>"><span>翌年&gt;</span></a></li>
            </ul>
        </div>
        <div class="clear"></div>

        <div style="margin:10px 0;">
            <{if $ward->month_type() === '2'}>
                <button type="button" class="btn_view" data-y="<{$year}>" data-m="<{$month}>" data-mode="month">月表示</button>
            <{/if}>
            <b><{$staff->emp_name()}></b> (<{$staff->class_full_name()}>)
        </div>

        <form action="shift/personal.php" method="post">
            <input type="hidden" name="mode" value="confirm" />
            <input type="hidden" name="y" value="<{$year}>" />
            <input type="hidden" name="m" value="<{$month}>" />
            <{if $personal_hope_use && $hope_limit}><button type="submit">勤務希望を登録する</button><{/if}>
            <table id="personal" class="list">
                <thead>
                    <tr>
                        <td rowspan="2">日付</td>
                        <td rowspan="2">曜日</td>
                        <td rowspan="2">所属</td>
                        <td colspan="2">勤務予定</td>
                        <td colspan="3">勤務実績</td>
                        <td colspan="6">残業</td>

                        <{if $is_shorter}><td rowspan="2">時短</td><{/if}>

                        <td rowspan="2">備考</td>

                        <{if $personal_hope_use}><td rowspan="2">勤務希望<{if $hope_limit}><br />(締切:<{$hope_limit_day|regex_replace:'/\d\d\d\d\-(\d\d)\-(\d\d)/':'$1/$2'|escape}>)<{/if}></td><{/if}>
                    </tr>
                    <tr>
                        <td>予定</td>
                        <td>当直</td>
                        <td>実績</td>
                        <td>当直</td>
                        <td>勤務</td>
                        <td>開始</td>
                        <td>終了</td>
                        <td>(休憩)</td>
                        <td>ｶﾝﾌｧ</td>
                        <td>小計</td>
                        <td>理由</td>
                    </tr>
                </thead>

                <tbody>
                    <{foreach from=$lists item=row}>
                        <tr class="ty<{$row.type}> <{if ($staff->join_date() && $staff->join_date() > $row.date) || ($staff->retire_date() && $row.date > $staff->retire_date())}>retire<{/if}>">
                            <td id="<{$row.date|escape}>">
                                <{if $row.date > $last_approved_date && (($ward->result_edit_flg() && $row.date <= $today) || ($row.results && $row.date >= $zan_limit) || ($staff->is_shorter($row.date) && $row.date <= $today))}>
                                    <span class="editor js-edit-result" data-date="<{$row.date|escape}>"><{$row.date|regex_replace:'/^\d\d\d\d(\d\d)(\d\d)/':'$1/$2'|escape}></span>
                                <{else}>
                                    <{$row.date|regex_replace:'/^\d\d\d\d(\d\d)(\d\d)/':'$1/$2'|escape}>
                                <{/if}>
                            </td>
                            <td class="week"><{$wj[$row.week]|escape}></td>
                            <td class="<{$row.assist|escape}>"><{if $row.assist_group_name}><{$row.assist_group_name}><{else}><{$ward->group_name()|escape}><{/if}></td>
                            <td class="sign">
                                <{if $ward->plan_edit_flg() && empty($row.results) && (empty($row.plan) && $row.date > $today)}>
                                    <span class="editor js-edit-plan" data-date="<{$row.date|escape}>"><{$row.plan|default:'*'|escape}></span>
                                <{else}>
                                    <{$row.plan|escape}>
                                <{/if}>
                            </td>
                            <td class="duty"><{if $row.plan_duty}>●<{/if}></td>
                            <td class="sign"><{$row.results|escape}></td>
                            <td class="duty"><{if $row.results_duty}>●<{/if}></td>
                            <td class="time"><{if $row.start_time}><{$row.worktime|min2time}><{/if}></td>

                            <td class="time">
                                <{foreach from=$row.ovtm_start_time item=ovtm_start_time}>
                                    <{$ovtm_start_time|regex_replace:'/(\d\d)(\d\d)/':'$1:$2'|escape}><br>
                                <{/foreach}>
                            </td>
                            <td class="time">
                                <{foreach from=$row.ovtm_end_time item=ovtm_end_time}>
                                    <{$ovtm_end_time|regex_replace:'/(\d\d)(\d\d)/':'$1:$2'|escape}><br>
                                <{/foreach}>
                            </td>
                            <td class="time"><{if $row.over_resttime}>(<{$row.over_resttime|min2time}>)<{/if}></td>
                            <td class="time"><{if $row.conference_minutes}><{$row.conference_minutes|min2time}><{/if}></td>
                            <td class="time"><{if $row.overtime2}><{$row.overtime2|min2time}><{/if}></td>
                            <td>
                                <{foreach from=$row.ovtm_reason item=ovtm_reason}>
                                    <{$ovtm_reason|escape}><br>
                                <{/foreach}>
                            </td>

                            <{if $is_shorter}><td class="time"><{$row.base_shorter_minutes|escape}></td><{/if}>

                            <td width="150"><{$row.memo|escape}></td>

                            <{if $personal_hope_use}>
                                <td class="sign">
                                    <{if $hope_limit}>
                                        <select name="hope[<{$row.date|escape}>]">
                                            <option value=""></option>
                                            <{pattern_pulldown array=$pattern_list ptn=$row.hope}>
                                        </select>
                                    <{else}>
                                        <{$row.hope|escape}>
                                    <{/if}>
                                </td>
                            <{/if}>
                        </tr>
                    <{/foreach}>
                    <tr>
                        <td colspan="3">合計</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="time"><{$total.worktime|min2time}></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="time"><{$total.overtime2|min2time}></td>
                        <td></td>

                        <{if $is_shorter}><td class="time"><{$total.base_shorter_minutes|escape}></td><{/if}>
                        <{if $personal_hope_use}><td></td><{/if}>
                    </tr>
                </tbody>
            </table>

            <table id="holiday">
                <tr>
                    <td></td>
                    <td class="head">繰越</td>
                    <td class="head">付与</td>
                    <td class="head">取得</td>
                    <td class="head">残</td>
                </tr>
                <tr>
                    <td class="head">有休</td>
                    <td><{$holiday.holiday.kuri|escape}></td>
                    <td><{$holiday.holiday.fuyo|escape}></td>
                    <td><{$holiday.holiday.use|escape}></td>
                    <td><{$holiday.holiday.zan|escape}></td>
                </tr>
                <tr>
                    <td class="head">夏休</td>
                    <td></td>
                    <td><{$holiday.summer.fuyo|escape}></td>
                    <td><{$holiday.summer.use|escape}></td>
                    <td><{$holiday.summer.zan|escape}></td>
                </tr>
                <tr>
                    <td class="head">週休</td>
                    <td></td>
                    <td><{$holiday.week.fuyo|escape}></td>
                    <td><{$holiday.week.use|escape}></td>
                    <td><{$holiday.week.zan|escape}></td>
                </tr>
            </table>

            <{if $personal_hope_use && $hope_limit}><button type="submit">勤務希望を登録する</button><{/if}>
        </form>

    </div>

    <{capture name="js"}>
        <script type="text/javascript" src="js/shift/common.js"></script>
        <script type="text/javascript" src="js/shift/personal.js"></script>
    <{/capture}>
<{/if}>
<{include file="shift/footer.tpl"}>
