<{include file="shift/header.tpl" title="管理画面：月次承認" tab="06"}>

<style type="text/css">
td{
    width: 180px;
}
td.long{
    width: 250px;
}
</style>

<div style="margin:10px 0 10px 0; font-size:12px;">
    <form name="approval" action="shift/adm_approval.php" method="get">
        <b>年月：</b>
        <select name="year" onchange="this.form.submit();">
            <{section name=y loop=$nextyear step=-1 max=20}>
                <option value="<{$smarty.section.y.index|escape}>" <{if $smarty.section.y.index|string_format:"%d" === $year}>selected="selected"<{/if}>><{$smarty.section.y.index|escape}></option>
            <{/section}>
        </select> /
        <select name="month" onchange="this.form.submit();">
            <{section name=m loop=12}>
                <option value="<{$smarty.section.m.iteration|escape}>" <{if $smarty.section.m.iteration|string_format:"%d" === $month}>selected="selected"<{/if}>><{$smarty.section.m.iteration|escape}>月</option>
            <{/section}>
        </select>
    </form>
</div>
<table class="list">
    <thead>
        <tr>
            <td>部門長</td>
            <td>部門</td>
            <td>部門長承認</td>
            <td>所属長承認</td>
        </tr>
    </thead>
    <tbody>
        <{foreach from=$lists key=mk item=manager name=manager_loop}>
            <tr>
                <{foreach from=$manager key=gk item=group name=group_loop}>
                    <{if $smarty.foreach.group_loop.first}>
                        <{if $group.manager_emp_name}>
                            <td nowrap rowspan=<{$manager|@count}>><{$group.manager_emp_name|escape}></td>
                        <{else}>
                            <td style="color :red;" nowrap rowspan=<{$manager|@count}>>部門長が設定されていません。</td>
                        <{/if}>
                    <{/if}>
                    <td nowrap><{$group.group_name|escape}></td>
                    <{if $group.boss_approval_flg}>
                        <td class="long" nowrap><{$group.boss_approval_date|date_format_jp:"%Y年%-m月%-d日(%a) %R"}> ： <{$group.boss_approval_emp_name}></td>
                    <{else}>
                        <td class="long" style="color :red;" nowrap>未承認</td>
                    <{/if}>
                    <{if  $group.manager_approval_flg}>
                        <td class="long" nowrap><{$group.manager_approval_date|date_format_jp:"%Y年%-m月%-d日(%a) %R"}> ： <{$group.manager_approval_emp_name}></td>
                    <{else}>
                        <td class="long" style="color :red;" nowrap>未承認</td>
                    <{/if}>
                    </tr>
                    <{if $smarty.foreach.manager_loop.first}>
                        <tr>
                    <{/if}>
                <{/foreach}>
            </tr>
            <{foreachelse}>
            <tr>
                <td colspan="4">
                    部署がありません。
                </td>
            </tr>
        <{/foreach}>
    </tbody>
</table>

<{include file="shift/footer.tpl"}>
