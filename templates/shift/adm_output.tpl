<{include file="shift/header.tpl" title="管理画面：データ出力" tab="07"}>

<div id="submenu">
    <ul>
        <li>
            <a href="shift/adm_output.php"><span>データ出力</span></a>
        </li>
        <li>
            <a href="shift/adm_output_setting.php"><span>レイアウト設定</span></a>
        </li>
        <li>
            <a href="shift/adm_output_batch_setting.php"><span>バッチ出力</span></a>
        </li>
    </ul>
</div>
<hr style="margin:10px 0;"/>

<div style="margin:20px 10px; font-size:12px;">
    <form action="shift/adm_output_data.php" method="post">
        <table class="form">
            <tr>
                <td class="header">期間</td>
                <td>
                    <input type="text" id="start_date" class="datepicker" name="start_date" size="11" maxlength="10" value="<{$base_date|escape}>" class="eng" />
                    〜
                    <input type="text" id="end_date" class="datepicker" name="end_date" size="11" maxlength="10" value="<{$base_date|escape}>" class="eng" />
                    <button type="button" id="js-date-thismonth">今月</button>
                    <button type="button" id="js-date-lastmonth">先月</button>
                </td>
            </tr>
            <tr>
                <td class="header">部署</td>
                <td>
                    <select name="ward">
                        <{foreach from=$ward_lists item=row}>
                            <option value="<{$row->group_id()|escape}>"><{$row->group_name()|escape}></option>
                        <{/foreach}>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="header">給与支給区分</td>
                <td>
                    <label><input type="radio" name="wage" value="4" checked="checked" /> 時間給制</label>&nbsp;&nbsp;&nbsp;
                    <label><input type="radio" name="wage" value="0" /> それ以外</label>
                </td>
            </tr>
            <tr>
                <td class="header">出力レイアウト</td>
                <td>
                    <select name="layout">
                        <{foreach from=$layout_lists item=row}>
                            <option value="<{$row.id|escape}>"><{$row.title|escape}></option>
                        <{/foreach}>
                    </select>
                </td>
            </tr>
        </table>

        <div>
            <button type="submit" style="padding:10px 15px; font-size:1.2em; font-weight:bold;">データ出力する</button>
        </div>
    </form>
</div>

<{capture name="js"}>
    <script type="text/javascript">
<!--
        $(function() {
            $(".datepicker").datepicker({
                dateFormat: 'yy-mm-dd',
                buttonImage: 'img/calendar_link.gif',
                buttonImageOnly: true,
                showOn: 'button'
            });

            //  今月の日付
            $('#js-date-thismonth').click(function() {
                var s = new Date();
                var e = new Date(s.getFullYear(), s.getMonth() + 1, 0);

                var y = String(s.getFullYear());
                var m = String(100 + s.getMonth() + 1).substr(1, 2);
                var d = String(100 + e.getDate()).substr(1, 2);

                $('#start_date').val(y + '-' + m + '-01');
                $('#end_date').val(y + '-' + m + '-' + d);
            }).click();

            //  先月の日付
            $('#js-date-lastmonth').click(function() {
                var b = new Date();
                var s = new Date(b.getFullYear(), b.getMonth() - 1, 1);
                var e = new Date(b.getFullYear(), b.getMonth(), 0);

                var y = String(s.getFullYear());
                var m = String(100 + s.getMonth() + 1).substr(1, 2);
                var d = String(100 + e.getDate()).substr(1, 2);

                $('#start_date').val(y + '-' + m + '-01');
                $('#end_date').val(y + '-' + m + '-' + d);
            });
        });

        // -->
    </script>
<{/capture}>

<{include file="shift/footer.tpl"}>
