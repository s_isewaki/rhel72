<{include file="__header.tpl" title="��̳ɽ $title" prefix='shift'}>
<body>

    <{if $fullscreen}>
        <div class="winheader">
            <span class="title"><{$title|escape}>: <{if $this_ward}><{$this_ward->group_name()|escape}><{else}><{$staff->emp_name()|escape}><{/if}>: <{$viewym[0]|escape}>/<{$viewym[1]|escape}></span>
            <span class="close"><img src="img/icon/close.gif" alt="�Ĥ���" width="24" height="24" border="0" onclick="window.close();" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';" /></span>
        </div>
    <{else}>
        <!-- Header -->
        <div id="header">
            <img src="img/icon/duty_shift.gif" width="32" height="32" border="0" alt="��̳ɽ" />
            <a href="shift/index.php">��̳ɽ</a>
            <{if $admin}>
                &gt; <a href="shift/admin.php">��������</a>
            <{/if}>
        </div>

        <!-- Admin -->
        <{if $auth_user or $auth_admin}>
            <{if $auth_user and $admin}>
                <div id="admin"><a href="shift/index.php">�桼������</a></div>
            <{/if}>
            <{if $auth_admin and ! $admin}>
                <div id="admin"><a href="shift/admin.php">��������</a></div>
            <{/if}>
        <{/if}>

        <!-- Menu -->
        <div id="menu">
            <ul>
                <{if $admin}>
                    <li <{if $tab=="01"}>class="selected"<{/if}>><a href="shift/adm_ward.php">����</a></li>
                    <li <{if $tab=="04"}>class="selected"<{/if}>><a href="shift/adm_sign.php">��̳����</a></li>
                    <li <{if $tab=="09"}>class="selected"<{/if}>><a href="shift/adm_default.php">����ε���</a></li>
                    <li <{if $tab=="05"}>class="selected"<{/if}>><a href="shift/adm_count.php">��̳ɽ����</a></li>
                    <li <{if $tab=="06"}>class="selected"<{/if}>><a href="shift/adm_approval.php">���ǧ</a></li>
                    <li <{if $tab=="07"}>class="selected"<{/if}>><a href="shift/adm_output.php">�ǡ�������</a></li>
                    <li <{if $tab=="08"}>class="selected"<{/if}>><a href="shift/adm_options.php">���ץ����</a></li>
                    <li <{if $tab=="02"}>class="selected"<{/if}>><a href="shift/adm_logs.php">��ȥ���</a></li>
                    <li <{if $tab=="03"}>class="selected"<{/if}>><a href="shift/adm_approval_log.php">��ǧ����</a></li>

                <{else}>
                    <li <{if $tab=="10"}>class="selected"<{/if}>><a href="shift/personal.php?y=<{$year}>&amp;m=<{$month}>">�ĿͶ�̳ɽ</a></li>
                    <li <{if $tab=="11"}>class="selected"<{/if}>><a href="shift/plan.php?y=<{$year}>&amp;m=<{$month}><{if $ward_lists}>&amp;group_id=<{$this_ward->group_id()|escape}><{/if}>">��̳ͽ��</a></li>
                    <li <{if $tab=="12"}>class="selected"<{/if}>><a href="shift/results.php?y=<{$year}>&amp;m=<{$month}><{if $ward_lists}>&amp;group_id=<{$this_ward->group_id()|escape}><{/if}>">��̳����</a></li>
                    <li <{if $tab=="13"}>class="selected"<{/if}>><a href="shift/duty.php?y=<{$year}>&amp;m=<{$month}><{if $ward_lists}>&amp;group_id=<{$this_ward->group_id()|escape}><{/if}>">��ľɽ</a></li>
                    <li <{if $tab=="15"}>class="selected"<{/if}>><a href="shift/print.php?y=<{$year}>&amp;m=<{$month}><{if $ward_lists}>&amp;group_id=<{$this_ward->group_id()|escape}><{/if}>">Ģɼ����</a></li>
                    <li <{if $tab=="16"}>class="selected"<{/if}>><a href="shift/ward.php?y=<{$year}>&amp;m=<{$month}><{if $ward_lists}>&amp;group_id=<{$this_ward->group_id()|escape}><{/if}>">��������</a></li>
                    <li <{if $tab=="17"}>class="selected"<{/if}>><a href="shift/staff.php?y=<{$year}>&amp;m=<{$month}><{if $ward_lists}>&amp;group_id=<{$this_ward->group_id()|escape}><{/if}>">��������</a></li>

                <{/if}>
            </ul>
        </div>

    <{/if}>