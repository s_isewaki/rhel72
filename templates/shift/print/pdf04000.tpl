<style type="text/css">
    .title {
        font-size: 13pt;
        text-align:center;
    }
    table.head {
        width: 780px;
    }
    table.list {
        width: 780px;
    }
    table.list td {
        font-size: 8pt;
        border: 1px solid #000000;
    }
    tr.listhead td {
        text-align:center;
    }
    td.hanko {
        width: 40px;
        font-size: 8pt;
        border: 1px solid #000000;
        text-align:center;
    }
    td.header-right {
        width: 490px;
        text-align:right;
    }
    td.header-body {
        width: 290px;
        font-size: 10pt;
        text-align:center;
    }
    td.name {
        width: 100px;
        font-size: 8pt;
        border: 1px solid #000000;
        text-align:center;
    }
    td.date {
        width: 24px;
    }
    td.kigo {
        width: 24px;
    }
    td.hour {
        width: 80px;
    }
    td.time {
        width: 30px;
    }
    td.hourtime3 {
        width: 170px;
    }
    td.time5 {
        width: 150px;
    }
    td.mark {
        width: 24px;
    }
    td.mark2 {
        width: 48px;
    }
    td.mark4 {
        width: 96px;
    }
    td.note {
        width: 138px;
    }
    td.note2 {
        width: 100px;
    }
</style>
<div class="title">���ֳ� ����ľ��̳���ؼ���</div>
<table class="head" cellpadding="1">
    <tr>
        <td class="hanko">��̳��Ĺ</td>
        <td class="hanko">��̳��Ĺ</td>
        <td class="hanko">�ͻ���Ĺ</td>
        <td width="10" rowspan="4"></td>
        <td class="hanko">��Ĺ</td>
        <td class="hanko">��°Ĺ</td>
        <td class="hanko"></td>
        <td class="hanko">��̳��</td>
        <td class="header-right" colspan="3"><{$today|date_format_jp:"%Yǯ%-m��%-d��"}></td>
    </tr>
    <tr>
        <td class="hanko" rowspan="3"></td>
        <td class="hanko" rowspan="3"></td>
        <td class="hanko" rowspan="3"></td>
        <td class="hanko" rowspan="3"></td>
        <td class="hanko" rowspan="3"></td>
        <td class="hanko" rowspan="3"></td>
        <td class="hanko" rowspan="3"></td>
        <td class="header-right" colspan="3">���ٻ��±�</td>
    </tr>
    <tr>
        <td class="header-body"><{$year}>ǯ<{$month}>��ʬ</td>
        <td class="name">��°</td>
        <td class="name">��̾</td>
    </tr>
    <tr>
        <td class="header-body"></td>
        <td class="name"><{$ward->group_name()|escape}></td>
        <td class="name"><{$staff->emp_name()|escape}></td>
    </tr>
</table>

<table class="list" cellpadding="2">
    <tr class="listhead">
        <td class="date" rowspan="3"><br><br><br>��̳��</td>
        <td class="kigo" rowspan="3"><br><br>��̳<br>����</td>
        <td class="hourtime3" colspan="4">���ֳ��� ��̳����</td>
        <td class="time5" colspan="5">���ֳ�</td>
        <td class="time">����</td>
        <td class="mark4" colspan="4">��ľ</td>
        <td class="mark2" colspan="2">����</td>
        <td class="note" rowspan="3"><br><br><br>��̳���Ƥ���¾���ͻ���</td>
        <td class="note2" rowspan="3"><br><br><br>����</td>
    </tr>
    <tr class="listhead">
        <td class="hour" rowspan="2"><br><br>��̳����(24����)</td>
        <td class="time">����</td>
        <td class="time">��Ư<br>����</td>
        <td class="time" style="font-size:5pt;">�ǻ��˷���륫��ե����</td>
        <td class="time">100<br>100</td>
        <td class="time">125<br>100</td>
        <td class="time">25<br>100</td>
        <td class="time">150<br>100</td>
        <td class="time">���<br>���</td>
        <td class="time">135<br>100</td>
        <td class="mark">��ľ</td>
        <td class="mark">��ľ</td>
        <td class="mark">Ⱦ��ľ</td>
        <td class="mark">�ܥ��顼</td>
        <td class="mark">ǯ��<br>ǯ��</td>
        <td class="mark">�Ե�<br>����</td>
    </tr>
    <tr>
        <td class="time" align="right"><{$total.overtime|min2time}></td>
        <td class="time" align="right"><{$total.overtime2|min2time}></td>
        <td class="time" align="right"><{$total.conference_minutes|min2time}></td>
        <td class="time" align="right"><{$total.over100|min2time}></td>
        <td class="time" align="right"><{$total.over125|min2time}></td>
        <td class="time" align="right"><{$total.over25|min2time}></td>
        <td class="time" align="right"><{$total.over150|min2time}></td>
        <td class="time" align="right"><{$total.nightduty_count|escape}></td>
        <td class="time" align="right"><{$total.over135|min2time}></td>
        <td class="mark"></td>
        <td class="mark"></td>
        <td class="mark"></td>
        <td class="mark"></td>
        <td class="mark"></td>
        <td class="mark"></td>
    </tr>
    <{foreach from=$lists item=row}>
        <{if $row.print}>
            <tr>
                <td class="date" align="right"><{$row.date|regex_replace:'/^(\d\d\d\d)(\d\d)(\d\d)/':'$1-$2-$3'|date_format_jp:"%-d%a"}></td>
                <td class="kigo" align="center"><{$row.kigo|escape}></td>
                <td class="hour" align="center">
                    <{foreach from=$row.ovtm_start_time key=i item=ovtm_start_time}>
                        <br><{$ovtm_start_time|regex_replace:'/(\d\d)(\d\d)/':'$1:$2'|escape}>������<{$row.ovtm_end_time[$i]|regex_replace:'/(\d\d)(\d\d)/':'$1:$2'|escape}>
                    <{/foreach}>
                </td>
                <td class="time" align="right"><{if $row.overtime}><{$row.overtime|min2time}><{/if}></td>
                <td class="time" align="right"><{if $row.overtime2}><{$row.overtime2|min2time}><{/if}></td>
                <td class="time" align="right"><{if $row.conference_minutes}><{$row.conference_minutes|min2time}><{/if}></td>
                <td class="time" align="right"><{if $row.over100}><{$row.over100|min2time}><{/if}></td>
                <td class="time" align="right"><{if $row.over125}><{$row.over125|min2time}><{/if}></td>
                <td class="time" align="right"><{if $row.over25}><{$row.over25|min2time}><{/if}></td>
                <td class="time" align="right"><{if $row.over150}><{$row.over150|min2time}><{/if}></td>
                <td class="time"></td>
                <td class="time" align="right"><{if $row.over135}><{$row.over135|min2time}><{/if}></td>
                <td class="mark" align="center"><{if $row.duty3}>��<{/if}></td>
                <td class="mark" align="center"><{if $row.duty1}>��<{/if}></td>
                <td class="mark" align="center"><{if $row.duty2}>��<{/if}></td>
                <td class="mark" align="center"><{if $row.boiler}>��<{/if}></td>
                <td class="mark" align="center"><{if $row.newyear}>��<{/if}></td>
                <td class="mark" align="center"><{if $row.taiki}>��<{/if}></td>
                <td class="note">
                    <{foreach from=$row.ovtm_reason item=ovtm_reason}>
                        <br><{$ovtm_reason|escape}>
                    <{/foreach}>
                </td>
                <td class="note2" style="font-size: 6pt;"><{$row.memo|escape|nl2br}></td>
            </tr>
        <{/if}>
    <{/foreach}>
    <{section name=filler loop=$filler}>
        <tr>
            <td class="date"></td>
            <td class="kigo"></td>
            <td class="hour"></td>
            <td class="time"></td>
            <td class="time"></td>
            <td class="time"></td>
            <td class="time"></td>
            <td class="time"></td>
            <td class="time"></td>
            <td class="time"></td>
            <td class="time"></td>
            <td class="time"></td>
            <td class="mark"></td>
            <td class="mark"></td>
            <td class="mark"></td>
            <td class="mark"></td>
            <td class="mark"></td>
            <td class="mark"></td>
            <td class="note"></td>
            <td class="note2"></td>
        </tr>
    <{/section}>
</table>
