<style type="text/css">
    table td {
        text-align:center;
    }

    .title {
        border: none;
        width: 200px;
        font-size: 20pt;
    }
    .name {
        border: 1px solid #000000;
        font-size: 12pt;
        width: 200px;
    }
    .emp_id {
        border: 1px solid #000000;
        font-size: 12pt;
        width: 100px;
    }
    .emp_duty {
        border: 1px solid #000000;
        font-size: 12pt;
        width: 200px;
    }
    .emp_job {
        border: 1px solid #000000;
        font-size: 12pt;
        width: 100px;
    }

    .month {
        font-size: 12pt;
        width: 21px;
    }
    .dayhead {
        font-size: 12pt;
        width: 25px;
    }
    .dayshort {
        font-size: 9pt;
        width: 25px;
        height: 10px;
    }
    .day {
        width: 25px;
        height: 20px;
    }
    .dayL {
        font-size: 11pt;
        width: 25px;
        height: 20px;
    }
    .dayM {
        font-size: 9pt;
        width: 25px;
        height: 20px;
    }
    .dayS {
        font-size: 6pt;
        width: 25px;
        height: 20px;
    }
    .dayblank {
        background-color: #757575;
        width: 25px;
    }
</style>

<table class="header_table">
    <tr>
        <td rowspan="2" class="title"><{$year}>ǯ�� �ж���</td>
        <td class="name">��̾</td>
        <td class="emp_id">�����ֹ�</td>
        <td class="emp_duty">��°</td>
        <td class="emp_job">����</td>
    </tr>
    <tr>
        <td class="name"><{$staff->emp_name()|escape}></td>
        <td class="emp_id"><{$staff->emp_personal_id()|escape}></td>
        <td class="emp_duty"><{$staff->atrb_name()|escape}> > <{$staff->dept_name()|escape}></td>
        <td class="emp_job"><{$staff->job_name()|escape}></td>
    </tr>
</table>
<br />
<br />
<table border="1">
    <tr>
        <td class="month"></td>
        <{foreach from=$days item=d}>
            <td class="dayhead"><{$d|escape}></td>
        <{/foreach}>
    </tr>
    <{foreach from=$month item=m}>
        <tr>
            <td class="month" rowspan="2">
                <{$m|escape}><br />��
            </td>
            <{foreach from=$days item=d}>
                <{if $d <= $month_limit[$m]}>
                    <td valign="middle" class="day<{$calendar.$m.$d.class|escape}>">
                        <{$calendar.$m.$d.sign|escape}>
                    </td>
                <{else}>
                    <td rowspan="2" class="dayblank"></td>
                <{/if}>
            <{/foreach}>
        </tr>
        <tr>
            <{foreach from=$days item=d}>
                <{if $d <= $month_limit[$m]}>
                    <td class="dayshort">
                        <{$calendar.$m.$d.base_shorter_minutes|escape}>
                    </td>
                <{/if}>
            <{/foreach}>
        </tr>
    <{/foreach}>
</table>