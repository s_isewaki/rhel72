<{include file="shift/header.tpl" title="職員設定" tab="17"}>

<{if ! $ward_lists}>
    <div class="attention">利用できません。</div>
<{else}>

    <script type="text/javascript">
        <!--
        var this_group_id = '<{$this_ward->group_id()}>';
        var this_y = '<{$year}>';
        var this_m = '<{$month}>';
        // -->
    </script>

    <div id="shift">

        <!-- 部署プルダウン -->
        <form name="shift/plan.php" method="get">
            <input type="hidden" id="this_y" name="y" value="<{$year}>" />
            <input type="hidden" id="this_m" name="m" value="<{$month}>" />
            <div class="pulldown">
                <b>部署</b>：
                <select id="this_group_id" name="group_id" onchange="submit(this.form)">
                    <{foreach from=$ward_lists item=row}>
                        <option value="<{$row->group_id()|escape}>" <{if $row->group_id() === $this_ward->group_id()}>selected<{/if}>><{$row->group_name()|escape}></option>
                    <{/foreach}>
                </select>
            </div>
        </form>
        <div class="clear"></div>

        <div id="err" class="alert" style="display: none;"></div>

        <div id="main">
            <form action="shift/staff.php" method="post">
                <input type="hidden" name="mode" value="confirm" />
                <input type="hidden" name="y" value="<{$year}>" />
                <input type="hidden" name="m" value="<{$month}>" />
                <input type="hidden" name="group_id" value="<{$this_ward->group_id()}>" />

                <div>
                    <button type="button" onclick="openEmployeeList('<{$SESSION}>', '<{$BASE_HREF}>');">職員を追加する</button>
                    <button type="submit">職員リストを更新する</button>
                </div>
                <table class="list" id="staff">

                    <thead>
                        <tr>
                            <td rowspan="2">表示順</td>
                            <td rowspan="2" colspan="<{$team_rows|escape}>">チーム</td>
                            <td rowspan="2">氏名</td>
                            <td rowspan="2">職種</td>
                            <td rowspan="2">役職</td>
                            <td rowspan="2">勤続年数</td>
                            <td colspan="5">勤務条件</td>
                            <td rowspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>支給区分</td>
                            <td>勤務形態</td>
                            <td>他部署</td>
                            <td>夜勤</td>
                            <td>設定</td>
                        </tr>
                    </thead>

                    <tbody>
                        <{foreach from=$staff item=row}>
                            <tr id="<{$row->emp_id()|escape}>">
                                <td class="dragHandle"><input type="hidden" name="order[]" value="<{$row->emp_id()|escape}>" /></td>
                                <td>
                                    <label><input type="radio" name="team[<{$row->emp_id()|escape}>]" value="" <{if ! $row->team_id()}>checked<{/if}> /> なし</label>
                                </td>
                                <{foreach from=$team item=t}>
                                    <td>
                                        <label><input type="radio" name="team[<{$row->emp_id()|escape}>]" value="<{$t.team_id|escape}>" <{if $t.team_id == $row->team_id()}>checked<{/if}>/> <{$t.team_name|escape}></label>
                                    </td>
                                <{/foreach}>
                                <td><{$row->emp_name()|escape}></td>
                                <td><{$row->job_name()|escape}></td>
                                <td><{$row->st_name()|escape}></td>
                                <td><{$row->duty_years()|escape}></td>
                                <td><{$row->wage_name()|escape}></td>
                                <td><{$row->duty_form_name()|escape}></td>
                                <td><{$row->other_post_name()|escape}></td>
                                <td><{$row->night_duty_name()|escape}></td>
                                <td><a href="shift/staff_employee.php?emp_id=<{$row->emp_id()|escape}>&amp;y=<{$year}>&amp;m=<{$month}>&amp;group_id=<{$this_ward->group_id()|escape}>">個人設定</a></td>
                                <td><button type="button" onclick="removeStaff(this);">削除する</button></td>
                            </tr>
                        <{/foreach}>
                    </tbody>
                </table>
                <div>
                    <button type="button" onclick="openEmployeeList('<{$SESSION}>', '<{$BASE_HREF}>');">職員を追加する</button>
                    <button type="submit">職員リストを更新する</button>
                </div>
            </form>
        </div>
    </div>

<{/if}>

<{capture name="js"}>
    <script type="text/javascript" src="js/shift/common.js"></script>
    <script type="text/javascript" src="js/shift/staff.js"></script>
<{/capture}>
<{include file="shift/footer.tpl"}>
