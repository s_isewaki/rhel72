<{include file="shift/header.tpl" title="部署設定" tab="16"}>

<{if ! $ward_lists}>
    <div class="attention">利用できません。</div>
<{else}>

    <script type="text/javascript">
        <!--
        var this_group_id = '<{$this_ward->group_id()}>';
        var this_y = '<{$year}>';
        var this_m = '<{$month}>';
        var shift_pattern = '<{pattern_pulldown array=$pattern_list}>';
        // -->
    </script>

    <!-- 部署プルダウン -->
    <form name="shift/ward.php" method="get" >
        <input type="hidden" name="y" value="<{$year}>" />
        <input type="hidden" name="m" value="<{$month}>" />
        <div class="pulldown">
            <b>部署</b>：
            <select name="group_id" onchange="submit(this.form)">
                <{foreach from=$ward_lists item=row}>
                    <option value="<{$row->group_id()|escape}>" <{if $row->group_id() === $this_ward->group_id()}>selected<{/if}>><{$row->group_name()|escape}></option>
                <{/foreach}>
            </select>
        </div>
    </form>
    <div class="clear"></div>

    <!-- 設定画面 -->
    <div id="main">
        <form action="shift/ward.php" method="post">
            <input type="hidden" name="mode" value="confirm" />
            <input type="hidden" name="y" value="<{$year}>" />
            <input type="hidden" name="m" value="<{$month}>" />
            <input type="hidden" name="group_id" value="<{$this_ward->group_id()}>" />

            <!-- 部署設定 -->
            <div id="subheader">■部署設定</div>
            <table class="form">
                <tr>
                    <td class="header">勤務希望の締切日</td>
                    <td>
                        開始<input type="number" name="close_day" value="<{$this_ward->close_day()|escape}>" class="num" size="2" maxlength="2" />日前
                        <{if $err.close_day}><p class="err"><{foreach from=$err.close_day key=msg item=v}><{$msg}><br /><{/foreach}></p><{/if}>
                    </td>
                </tr>

                <tr>
                    <td class="header">連続勤務日 上限</td>
                    <td>
                        <input type="number" name="duty_upper_limit_day" value="<{$this_ward->duty_upper_limit_day()|escape}>" class="num" size="2" maxlength="2" />日
                        <{if $err.duty_upper_limit_day}><p class="err"><{foreach from=$err.duty_upper_limit_day key=msg item=v}><{$msg}><br /><{/foreach}></p><{/if}>
                    </td>
                </tr>

                <tr>
                    <td class="header">作成期間</td>
                    <td>
                        <select name="start_month_flg1">
                            <option value="1" <{if $this_ward->start_month_flg1() === "1"}>selected<{/if}>>当月</option>
                            <option value="0" <{if $this_ward->start_month_flg1() === "0"}>selected<{/if}>>前月</option>
                        </select>
                        <select name="start_day1">
                            <option value=""></option>
                            <{section name=day start=1 loop=31}>
                                <option value="<{$smarty.section.day.index}>" <{if $this_ward->start_day1() == $smarty.section.day.index}>selected<{/if}>><{$smarty.section.day.index}>日</option>
                            <{/section}>
                        </select>
                        〜
                        <select id="month_flg1" name="month_flg1">
                            <option value="1" <{if $this_ward->month_flg1() === "1"}>selected<{/if}>>当月</option>
                            <option value="2" <{if $this_ward->month_flg1() === "2"}>selected<{/if}>>翌月</option>
                        </select>
                        <select id="end_day1" name="end_day1">
                            <option value="0"></option>
                            <{section name=day start=1 loop=31}>
                                <option value="<{$smarty.section.day.index}>" <{if $this_ward->end_day1() == $smarty.section.day.index}>selected<{/if}>><{$smarty.section.day.index}>日</option>
                            <{/section}>
                            <option value="99" <{if $this_ward->end_day1() == "99"}>selected<{/if}>>月末</option>
                        </select>
                        <{if $err.start_month}><p class="err"><{foreach from=$err.start_month key=msg item=v}><{$msg}><br /><{/foreach}></p><{/if}>
                    </td>
                </tr>

                <tr>
                    <td class="header">事由設定の表示</td>
                    <td>
                        <label><input type="radio" name="reason_setting_flg" value="t" <{if $this_ward->reason_setting_flg() == "t"}>checked<{/if}>> 表示する</label>
                        <label><input type="radio" name="reason_setting_flg" value="f" <{if $this_ward->reason_setting_flg() == "f"}>checked<{/if}>> 表示しない</label>
                        <{if $err.reason_setting_flg}><p class="err"><{foreach from=$err.reason_setting_flg key=msg item=v}><{$msg}><br /><{/foreach}></p><{/if}>
                    </td>
                </tr>
            </table>

            <div id="subheader">■条件設定</div>
            <table class="form" id="ok_pattern">
                <thead>
                    <tr>
                        <td class="header">組み合わせ設定</td>
                        <td style="width:125px;">当日</td>
                        <td style="width:125px;">翌日</td>
                        <td><button type="button" onclick="add_ok_pattern();">追加</button></td>
                    </tr>
                </thead>
                <tbody>
                    <{foreach from=$pattern_ok item=row}>
                        <tr>
                            <td class="header"><{$row.no|escape}></td>
                            <td <{if $row.err}>class="err"<{/if}>><select name="seq_today[]"><{pattern_pulldown array=$pattern_list ptn=$row.today_atdptn_ptn_id reason=$row.today_reason}></select></td>
                            <td <{if $row.err}>class="err"<{/if}>><select name="seq_next[]"><{pattern_pulldown array=$pattern_list ptn=$row.nextday_atdptn_ptn_id reason=$row.nextday_reason}></select></td>
                            <td><button type="button" onclick="remove_table_row(this);">削除</button></td>
                        </tr>
                    <{/foreach}>
                </tbody>
                <{if $err.ok_pattern}><tfoot><tr><td colspan="4"><p class="err"><{foreach from=$err.ok_pattern key=msg item=v}><{$msg}><br /><{/foreach}></p></td></tr></tfoot><{/if}>
            </table>

            <table class="form" id="ng_pattern">
                <thead>
                    <tr>
                        <td class="header">組み合わせ禁止</td>
                        <td style="width:125px;">当日</td>
                        <td style="width:125px;">翌日</td>
                        <td><button type="button" onclick="add_ng_pattern();">追加</button></td>
                    </tr>
                </thead>
                <tbody>
                    <{foreach from=$pattern_ng item=row}>
                        <tr>
                            <td class="header"><{$row.no|escape}></td>
                            <td <{if $row.err}>class="err"<{/if}>><select name="ng_today[]"><{pattern_pulldown array=$pattern_list ptn=$row.today_atdptn_ptn_id reason=$row.today_reason}></select></td>
                            <td <{if $row.err}>class="err"<{/if}>><select name="ng_next[]"><{pattern_pulldown array=$pattern_list ptn=$row.nextday_atdptn_ptn_id reason=$row.nextday_reason}></select></td>
                            <td><button type="button" onclick="remove_table_row(this);">削除</button></td>
                        </tr>
                    <{/foreach}>
                </tbody>
                <{if $err.ng_pattern}><tfoot><tr><td colspan="4"><p class="err"><{foreach from=$err.ng_pattern key=msg item=v}><{$msg}><br /><{/foreach}></p></td></tr></tfoot><{/if}>
            </table>

            <table class="form" id="up_limit">
                <thead>
                    <tr>
                        <td class="header">連続勤務シフトの上限</td>
                        <td style="width:125px;">シフト</td>
                        <td style="width:125px;">上限</td>
                        <td><button type="button" onclick="add_up_limit();">追加</button></td>
                    </tr>
                </thead>
                <tbody>
                    <{foreach from=$upper_limit item=row}>
                        <tr>
                            <td class="header"><{$row.no|escape}></td>
                            <td <{if $row.err}>class="err"<{/if}>><select name="limit_id[]"><{pattern_pulldown array=$pattern_list ptn=$row.atdptn_ptn_id reason=$row.reason}></select></td>
                            <td <{if $row.err}>class="err"<{/if}>><input type="number" class="num" name="limit_day[]" value="<{$row.upper_limit_day|escape}>" size="3">日</td>
                            <td><button type="button" onclick="remove_table_row(this);">削除</button></td>
                        </tr>
                    <{/foreach}>
                </tbody>
                <{if $err.up_limit}><tfoot><tr><td colspan="4"><p class="err"><{foreach from=$err.up_limit key=msg item=v}><{$msg}><br /><{/foreach}></p></td></tr></tfoot><{/if}>
            </table>

            <table class="form" id="interval">
                <thead>
                    <tr>
                        <td class="header">勤務シフトの間隔</td>
                        <td style="width:125px;">シフト</td>
                        <td style="width:125px;">間隔日数</td>
                        <td><button type="button" onclick="add_interval();">追加</button></td>
                    </tr>
                </thead>
                <tbody>
                    <{foreach from=$interval item=row}>
                        <tr>
                            <td class="header"><{$row.no|escape}></td>
                            <td <{if $row.err}>class="err"<{/if}>><select name="interval_id[]"><{pattern_pulldown array=$pattern_list ptn=$row.atdptn_ptn_id reason=$row.reason}></select></td>
                            <td <{if $row.err}>class="err"<{/if}>><input type="number" class="num" name="interval_day[]" value="<{$row.interval_day|escape}>" size="3">日</td>
                            <td><button type="button" onclick="remove_table_row(this);">削除</button></td>
                        </tr>
                    <{/foreach}>
                </tbody>
                <{if $err.interval}><tfoot><tr><td colspan="4"><p class="err"><{foreach from=$err.interval key=msg item=v}><{$msg}><br /><{/foreach}></p></td></tr></tfoot><{/if}>
            </table>

            <table class="form" id="sex_pattern">
                <thead>
                    <tr>
                        <td class="header">同時勤務の性別を固定するシフト</td>
                        <td style="width:125px;">シフト</td>
                        <td style="width:125px;">-</td>
                        <td><button type="button" onclick="add_sex_pattern();">追加</button></td>
                    </tr>
                </thead>
                <tbody>
                    <{foreach from=$pattern_sex item=row}>
                        <tr>
                            <td class="header"><{$row.no|escape}></td>
                            <td <{if $row.err}>class="err"<{/if}>><select name="sex_pattern[]"><{pattern_pulldown array=$pattern_list ptn=$row.ptn_id}></select></td>
                            <td <{if $row.err}>class="err"<{/if}>>-</td>
                            <td><button type="button" onclick="remove_table_row(this);">削除</button></td>
                        </tr>
                    <{/foreach}>
                </tbody>
                <{if $err.sex_pattern}><tfoot><tr><td colspan="4"><p class="err"><{foreach from=$err.sex_pattern key=msg item=v}><{$msg}><br /><{/foreach}></p></td></tr></tfoot><{/if}>
            </table>

            <!-- チェック設定 -->
            <div id="subheader">■チェック設定</div>
            <table class="form">
                <tr>
                    <td class="header">4週8休のチェック</td>
                    <td colspan="2">
                        <label><input type="radio" name="four_week_chk_flg" value="t" <{if $this_ward->four_week_chk_flg() == "t"}>checked<{/if}>> チェックする</label>
                        <label><input type="radio" name="four_week_chk_flg" value="f" <{if $this_ward->four_week_chk_flg() == "f"}>checked<{/if}>> チェックしない</label>
                        <{if $err.four_week_chk_flg}><p class="err"><{foreach from=$err.four_week_chk_flg key=msg item=v}><{$msg}><br /><{/foreach}></p><{/if}>
                    </td>
                </tr>

                <tr>
                    <td class="header">4週8休の開始日</td>
                    <td colspan="2">
                        <label><input type="text" id="datepicker" name="chk_start_date" size="11" maxlength="10" value="<{$this_ward->chk_start_date()|escape}>" class="eng" /></label>
                        <{if $err.chk_start_date}><p class="err"><{foreach from=$err.chk_start_date key=msg item=v}><{$msg}><br /><{/foreach}></p><{/if}>
                    </td>
                </tr>

                <tr>
                    <td class="header">4週8休で休暇とするシフト</td>
                    <td id="hol_pattern">
                        <span style="color:red;">※休暇、休暇(公休、法定、所定)は最初から含みます。</span><br />
                        <{foreach from=$pattern_holiday item=row}>
                            <span><select name="hol_pattern[]" <{if $row.err}>class="err"<{/if}>><{pattern_pulldown array=$pattern_list ptn=$row.ptn_id}></select><button type="button" onclick="remove_span(this);">削除</button><br></span>
                                <{/foreach}>
                                <{if $err.hol_pattern}><p class="err"><{foreach from=$err.hol_pattern key=msg item=v}><{$msg}><br /><{/foreach}></p><{/if}>
                    </td>
                    <td>
                        <button type="button" onclick="add_hol_pattern();">追加</button>
                    </td>
                </tr>

                <tr>
                    <td class="header">警告表示</td>
                    <td colspan="2">
                        <label><input type="checkbox" name="need_less_chk_flg" value="t" <{if $this_ward->need_less_chk_flg() == "t"}>checked<{/if}>> 必要人数より少ないときに警告表示する</label><br />
                        <label><input type="checkbox" name="need_more_chk_flg" value="t" <{if $this_ward->need_more_chk_flg() == "t"}>checked<{/if}>> 必要人数より多いときに警告表示する</label><br />
                        <{if $err.need_more_chk_flg}><p class="err"><{foreach from=$err.need_more_chk_flg key=msg item=v}><{$msg}><br /><{/foreach}></p><{/if}>
                    </td>
                </tr>
            </table>

            <!-- 自動シフト設定 -->
            <div id="subheader">■自動シフト</div>
            <table class="form">
                <tr>
                    <td class="header">解析対象とする期間</td>
                    <td>
                        <input type="number" name="auto_check_months" value="<{$this_ward->auto_check_months()|escape}>" class="num" size="2" maxlength="2" />ヶ月間
                        <{if $err.auto_check_months}><p class="err"><{foreach from=$err.auto_check_months key=msg item=v}><{$msg}><br /><{/foreach}></p><{/if}>
                    </td>
                </tr>

                <tr>
                    <td class="header">自動割り当てできなかったシフト</td>
                    <td>
                        <select name="auto_shift_filler">
                            <option value="1" <{if $this_ward->auto_shift_filler() === "1"}>selected<{/if}>>勤務記号表示の1番目を設定</option>
                            <option value="2" <{if $this_ward->auto_shift_filler() === "2"}>selected<{/if}>>必要人数設定の優先順位1番目を設定</option>
                            <option value="3" <{if $this_ward->auto_shift_filler() === "3"}>selected<{/if}>>設定なし</option>
                        </select>
                        <{if $err.auto_shift_filler}><p class="err"><{foreach from=$err.auto_shift_filler key=msg item=v}><{$msg}><br /><{/foreach}></p><{/if}>
                    </td>
                </tr>

            </table>

            <!-- 印刷設定 -->
            <div id="subheader">■印刷設定</div>
            <table class="form">
                <tr>
                    <td class="header">勤務シフト表タイトル</td>
                    <td colspan="2">
                        <input type="text" name="print_title" size="60" maxlength="50" value="<{$this_ward->print_title()|default:'看護職員勤務表'|escape}>" class="jpn" />
                        <{if $err.print_title}><p class="err"><{foreach from=$err.print_title key=msg item=v}><{$msg}><br /><{/foreach}></p><{/if}>
                    </td>
                </tr>

                <tr>
                    <td class="header">決済欄役職(左から)</td>
                    <td id="st_name">
                        <span><input type="text" name="st_name[]" size="30" maxlength="30" value="<{$print_st_names[0].st_name|default:'院長'|escape}>" class="jpn"><br></span>
                        <span><input type="text" name="st_name[]" size="30" maxlength="30" value="<{$print_st_names[1].st_name|default:'看護部長'|escape}>" class="jpn"><br></span>
                        <span><input type="text" name="st_name[]" size="30" maxlength="30" value="<{$print_st_names[2].st_name|default:'師長'|escape}>" class="jpn"><br></span>
                            <{foreach from=$print_st_names item=row name=st_name}>
                                <{if $smarty.foreach.st_name.index > 2}>
                                <span><input type="text" name="st_name[]" size="30" maxlength="30" value="<{$row.st_name|escape}>" class="jpn"><button type="button" onclick="remove_span(this);">削除</button><br></span>
                                <{/if}>
                            <{/foreach}>
                            <{if $err.st_name}><p class="err"><{foreach from=$err.st_name key=msg item=v}><{$msg}><br /><{/foreach}></p><{/if}>
                    </td>
                    <td>
                        <button type="button" onclick="add_st_name();">追加</button>
                    </td>
                </tr>
            </table>

            <!-- 画面レイアウト設定 -->
            <div id="subheader">■画面レイアウト</div>
            <table class="form">
                <tr>
                    <td class="header">チーム表示</td>
                    <td>
                        <label><input type="radio" name="team_disp_flg" value="t" <{if $this_ward->team_disp_flg() == "t"}>checked<{/if}>> する</label>
                        <label><input type="radio" name="team_disp_flg" value="f" <{if $this_ward->team_disp_flg() == "f"}>checked<{/if}>> しない</label>
                            <{if $err.team_disp_flg}>
                            <p class="err"><{foreach from=$err.team_disp_flg key=msg item=v}><{$msg}><br /><{/foreach}></p>
                            <{/if}>
                    </td>
                </tr>
                <tr>
                    <td class="header">職種表示</td>
                    <td>
                        <label><input type="radio" name="job_disp_flg" value="t" <{if $this_ward->job_disp_flg() == "t"}>checked<{/if}>> する</label>
                        <label><input type="radio" name="job_disp_flg" value="f" <{if $this_ward->job_disp_flg() == "f"}>checked<{/if}>> しない</label>
                            <{if $err.job_disp_flg}>
                            <p class="err"><{foreach from=$err.job_disp_flg key=msg item=v}><{$msg}><br /><{/foreach}></p>
                            <{/if}>
                    </td>
                </tr>
                <tr>
                    <td class="header">役職表示</td>
                    <td>
                        <label><input type="radio" name="st_disp_flg" value="t" <{if $this_ward->st_disp_flg() == "t"}>checked<{/if}>> する</label>
                        <label><input type="radio" name="st_disp_flg" value="f" <{if $this_ward->st_disp_flg() == "f"}>checked<{/if}>> しない</label>
                            <{if $err.st_disp_flg}>
                            <p class="err"><{foreach from=$err.st_disp_flg key=msg item=v}><{$msg}><br /><{/foreach}></p>
                            <{/if}>
                    </td>
                </tr>
                <tr>
                    <td class="header">稼働時間表示</td>
                    <td>
                        <label><input type="radio" name="display[kadou]" value="1" <{if $this_ward->display('kadou') == "1"}>checked<{/if}>> する</label>
                        <label><input type="radio" name="display[kadou]" value="0" <{if $this_ward->display('kadou') == "0"}>checked<{/if}>> しない</label>
                    </td>
                </tr>
                <tr>
                    <td class="header">集計表示</td>
                    <td>
                        <label><input type="radio" name="display[count]" value="1" <{if $this_ward->display('count') == "1"}>checked<{/if}>> する</label>
                        <label><input type="radio" name="display[count]" value="0" <{if $this_ward->display('count') == "0"}>checked<{/if}>> しない</label>
                    </td>
                </tr>
                <tr>
                    <td class="header">有休表示</td>
                    <td>
                        <label><input type="radio" name="display[holiday]" value="1" <{if $this_ward->display('holiday') == "1"}>checked<{/if}>> する</label>
                        <label><input type="radio" name="display[holiday]" value="0" <{if $this_ward->display('holiday') == "0"}>checked<{/if}>> しない</label>
                    </td>
                </tr>
                <tr>
                    <td class="header">夏休表示</td>
                    <td>
                        <label><input type="radio" name="display[summer]" value="1" <{if $this_ward->display('summer') == "1"}>checked<{/if}>> する</label>
                        <label><input type="radio" name="display[summer]" value="0" <{if $this_ward->display('summer') == "0"}>checked<{/if}>> しない</label>
                    </td>
                </tr>
                <tr>
                    <td class="header">週休表示</td>
                    <td>
                        <label><input type="radio" name="display[week]" value="1" <{if $this_ward->display('week') == "1"}>checked<{/if}>> する</label>
                        <label><input type="radio" name="display[week]" value="0" <{if $this_ward->display('week') == "0"}>checked<{/if}>> しない</label>
                    </td>
                </tr>
            </table>

            <div>
                <button type="submit">登録する</button>
            </div>

        </form>
    </div>

    <{capture name="js"}>
        <script type="text/javascript" src="js/shift/common.js"></script>
        <script type="text/javascript" src="js/shift/ward.js"></script>
    <{/capture}>
<{/if}>
<{include file="shift/footer.tpl"}>
