<{include file="shift/header.tpl" title="管理画面：部署" tab="01"}>

<div id="submenu">
    <ul>
        <li>
            <a href="shift/adm_ward_form.php">部署を新規作成する</a>
        </li>
    </ul>
</div>

<div id="main">

    <{if $lists}>
        <table class="list" id="ward">

            <thead>
                <tr>
                    <td rowspan="2">表示順</td>
                    <td rowspan="2">部署名</td>
                    <td rowspan="2">施設基準</td>
                    <td rowspan="2">管理者</td>
                    <td rowspan="2">勤務パターン</td>
                    <td rowspan="2">周期</td>
                    <td rowspan="2">連携</td>
                    <td colspan="4">作成ステータス</td>
                    <td rowspan="2">部門長</td>
                    <td rowspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <{foreach from=$yearmonth item=row}>
                        <td><{$row.vyear}>年<{$row.vmonth}>C<br><{$row.year}>年<{$row.rmonth}>月</td>
                        <{/foreach}>
                </tr>
            </thead>

            <tbody>
                <{foreach from=$lists item=row}>
                    <tr id="<{$row->group_id()|escape}>">
                        <td class="dragHandle">&nbsp;</td>
                        <td><a href="shift/adm_ward_form.php?group_id=<{$row->group_id()|escape}>"><{$row->group_name()|escape}></a></td>
                        <td><{if $row->standard_id()}>施設基準<{$row->standard_id()|escape}><{else}>適用なし<{/if}></td>
                        <td><{foreach from=$row->administrator() item=admin}><{$admin->emp_name()|escape}><br /><{/foreach}></td>
                        <td><{assign var=pattern value=$row->pattern_group()}><{$pattern->group_name()}></td>
                        <td><{if $row->month_type()==="1"}>1ヶ月<{elseif $row->month_type()==="2"}>28日<{/if}></td>
                        <td><{$row->sfc_group_code()|escape}></td>
                        <{foreach from=$yearmonth item=ym}>
                            <{assign var=status value=$row->status($ym.year, $ym.month)}>
                            <td>
                                <{if $status == 0}>
                                    <span style="color:red;">未登録</span>
                                <{elseif $status == 1}>
                                    <b>登録済</b>
                                <{else}>
                                    下書き
                                <{/if}>
                            </td>
                        <{/foreach}>

                        <td>
                            <{assign var=manager_item value=$row->manager()}>
                            <{if !$manager_item}>
                                <span style="color:red;">未登録</span>
                            <{else}>
                                <{$manager_item->emp_name()|escape}>
                            <{/if}>
                        </td>
                        <td><button type="button" onclick="removeWard(this);">削除する</button></td>
                    </tr>
                <{/foreach}>
            <tbody>
        </table>
        <button type="button" onclick="sortWard(this);">表示順を更新する</button>
    <{else}>
        <p>
            部署が登録されていません。<br />
            <a href="shift/adm_ward_form.php">部署を作成してください。</a>
        </p>
    <{/if}>

</div>
<hr />

<{capture name="js"}>
    <script type="text/javascript" src="js/shift/common.js"></script>
    <script type="text/javascript" src="js/shift/adm_ward.js"></script>
<{/capture}>
<{include file="shift/footer.tpl"}>
