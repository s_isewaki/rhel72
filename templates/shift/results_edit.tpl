<{include file="__header.tpl" title="勤務実績修正" prefix='shift'}>

<script type="text/javascript">
    <!--
    var e = 'e<{$emp_id|escape:'javascript'}>';
    var d = 'd<{$date|escape:'javascript'}>';
    var window_close = false;
    // -->
</script>
<div class="winheader">
    <span class="title">勤務実績修正</span>
    <span class="close"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onclick="window.close();" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';" /></span>
</div>

<div id="main">
    <form id="edit_form" action="shift/results_edit.php" method="post">
        <input type="hidden" name="mode" value="confirm" />
        <input type="hidden" name="is_overtime" value="1" />
        <input type="hidden" name="is_result_edit" value="1" />
        <input type="hidden" name="is_shorter" value="<{$is_shorter|escape}>" />
        <input type="hidden" name="emp_id" value="<{$emp_id|escape}>" />
        <input type="hidden" name="prev_emp_id" value="<{$prev_emp_id|escape}>" />
        <input type="hidden" name="next_emp_id" value="<{$next_emp_id|escape}>" />
        <input type="hidden" name="date" value="<{$date|escape}>" />
        <input type="hidden" name="submit_status" id="submit_status" value="" />
        <input type="hidden" name="night_duty" value="" />
        <table class="form">

            <tr>
                <td class="header">氏名</td>
                <td><{$emp.name|escape}></td>
            </tr>

            <tr>
                <td class="header">日付</td>
                <td><{$date|regex_replace:'/(\d\d\d\d)(\d\d)(\d\d)/':'$1-$2-$3'|date_format_jp:"%Y年%-m月%-d日(%a)"}></td>
            </tr>

            <tr>
                <td class="header">グループ</td>
                <td>
                    <select id="tmcd_group_id" name="tmcd_group_id">
                        <{foreach from=$pgroup_lists item=row}>
                            <option value="<{$row->group_id()|escape}>" <{if $row->group_id()==$results.tmcd_group_id}>selected="selected"<{/if}>><{$row->group_name()|escape}></option>
                        <{/foreach}>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="header">勤務実績</td>
                <td>
                    <select id="ptn" name="ptn"><option value=""></option><{pattern_pulldown array=$pattern_list ptn=$results.pattern reason=$results.reason}></select>

                    <{if $err.ptn}>
                        <p class="err"><{$err.ptn}></p>
                    <{/if}>
                </td>
            </tr>

            <tr>
                <td class="header">出退勤</td>
                <td>
                    <select id="start_hour" name="start_hour">
                        <option value=""></option>
                        <{foreach from=$hours24 key=value item=hour}>
                            <option value="<{$value|string_format:"%02d"|escape}>" <{if $results.start_hour != '' && $value==$results.start_hour}>selected="selected"<{/if}>><{$hour|escape}></option>
                        <{/foreach}>
                    </select>：
                    <select id="start_min" name="start_min">
                        <option value=""></option>
                        <{foreach from=$fifteen_minutes item=m}>
                            <option value="<{$m|string_format:"%02d"|escape}>"><{$m|string_format:"%02d"|escape}></option>
                        <{/foreach}>
                        <{foreach from=$minutes item=m}>
                            <option value="<{$m|string_format:"%02d"|escape}>" <{if $results.start_min != '' && $m==$results.start_min}>selected="selected"<{/if}>><{$m|string_format:"%02d"|escape}></option>
                        <{/foreach}>
                    </select>
                    <input type="hidden" name="previous_day_flag" value="0"/>
                    〜
                    <select id="end_hour" name="end_hour">
                        <option value=""></option>
                        <{foreach from=$hours48 key=value item=hour}>
                            <option value="<{$value|string_format:"%02d"|escape}>" <{if $results.end_hour != '' && $value==$results.end_hour}>selected="selected"<{/if}>><{$hour|escape}></option>
                        <{/foreach}>
                    </select>：
                    <select id="end_min" name="end_min">
                        <option value=""></option>
                        <{foreach from=$fifteen_minutes item=m}>
                            <option value="<{$m|string_format:"%02d"|escape}>"><{$m|string_format:"%02d"|escape}></option>
                        <{/foreach}>
                        <{foreach from=$minutes item=m}>
                            <option value="<{$m|string_format:"%02d"|escape}>" <{if $results.end_min != '' && $m==$results.end_min}>selected="selected"<{/if}>><{$m|string_format:"%02d"|escape}></option>
                        <{/foreach}>
                    </select>

                    <{if $err.work_time}>
                        <p class="err"><{$err.work_time}></p>
                    <{/if}>
                </td>
            </tr>

            <tr>
                <td class="header">休憩</td>
                <td>
                    <select id="rest_start_hour" name="rest_start_hour">
                        <option value=""></option>
                        <{foreach from=$hours24 key=value item=hour}>
                            <option value="<{$value|string_format:"%02d"|escape}>" <{if $results.rest_start_hour != '' && $value==$results.rest_start_hour}>selected="selected"<{/if}>><{$hour|escape}></option>
                        <{/foreach}>
                    </select>：
                    <select id="rest_start_min" name="rest_start_min">
                        <option value=""></option>
                        <{foreach from=$fifteen_minutes item=m}>
                            <option value="<{$m|string_format:"%02d"|escape}>"><{$m|string_format:"%02d"|escape}></option>
                        <{/foreach}>
                        <{foreach from=$minutes item=m}>
                            <option value="<{$m|string_format:"%02d"|escape}>" <{if $results.rest_start_min != '' && $m==$results.rest_start_min}>selected="selected"<{/if}>><{$m|string_format:"%02d"|escape}></option>
                        <{/foreach}>
                    </select>
                    〜
                    <select id="rest_end_hour" name="rest_end_hour">
                        <option value=""></option>
                        <{foreach from=$hours24 key=value item=hour}>
                            <option value="<{$value|string_format:"%02d"|escape}>" <{if $results.rest_end_hour != '' && $value==$results.rest_end_hour}>selected="selected"<{/if}>><{$hour|escape}></option>
                        <{/foreach}>
                    </select>：
                    <select id="rest_end_min" name="rest_end_min">
                        <option value=""></option>
                        <{foreach from=$fifteen_minutes item=m}>
                            <option value="<{$m|string_format:"%02d"|escape}>"><{$m|string_format:"%02d"|escape}></option>
                        <{/foreach}>
                        <{foreach from=$minutes item=m}>
                            <option value="<{$m|string_format:"%02d"|escape}>" <{if $results.rest_end_min != '' && $m==$results.rest_end_min}>selected="selected"<{/if}>><{$m|string_format:"%02d"|escape}></option>
                        <{/foreach}>
                    </select>

                    <{if $err.rest_time}>
                        <p class="err"><{$err.rest_time}></p>
                    <{/if}>
                </td>
            </tr>

            <tr>
                <td class="header">カンファレンス時間</td>
                <td>
                    <input type="text" name="conference_time" value="<{$results.conference_time|escape}>" size="4" maxlength="4" class="num" /> 時間

                    <{if $err.conference_time}>
                        <p class="err"><{$err.conference_time}></p>
                    <{/if}>
                </td>
            </tr>

            <{if $is_shorter}>
                <tr>
                    <td class="header">時短</td>
                    <td>
                        <label><input type="radio" name="base_shorter_minutes" value="" <{if empty($results.base_shorter_minutes)}>checked="checked"<{/if}> />無し</label>&nbsp;&nbsp;&nbsp;
                        <label><input type="radio" name="base_shorter_minutes" value="30" <{if $results.base_shorter_minutes==="30"}>checked="checked"<{/if}> />30分</label>&nbsp;&nbsp;&nbsp;
                        <label><input type="radio" name="base_shorter_minutes" value="60" <{if $results.base_shorter_minutes==="60"}>checked="checked"<{/if}> />60分</label>&nbsp;&nbsp;&nbsp;
                        <label><input type="radio" name="base_shorter_minutes" value="90" <{if $results.base_shorter_minutes==="90"}>checked="checked"<{/if}> />90分</label>

                        <{if $err.base_shorter_minutes}>
                            <p class="err"><{$err.base_shorter_minutes}></p>
                        <{/if}>
                    </td>
                </tr>
            <{/if}>

            <{section name=i start=1 loop=6}>
                <{assign var=i1 value=$smarty.section.i.index}>
                <{assign var=i2 value=$smarty.section.i.index_next}>
                <{assign var=i0 value=$smarty.section.i.index_prev}>
                <tr <{if $i1 >= 2}>class="js-overtime2345"<{/if}> id="overtime<{$i1}>">
                    <td class="header">残業時刻<{$i1}></td>
                    <td>
                        <select class="js-group<{$i1}> js-overtime js-reset<{$i1}> shh" data-reason="#reason<{$i1}>" data-next="#overtime<{$i2}>" data-group=".js-group<{$i1}>" name="over[<{$i0}>][over_start_hour]">
                            <option value=""></option>
                            <{foreach from=$hours48 key=value item=hour}>
                                <option value="<{$value|string_format:"%02d"|escape}>" <{if $results.over[$i0].over_start_hour != '' && $value==$results.over[$i0].over_start_hour}>selected="selected"<{/if}>><{$hour|escape}></option>
                            <{/foreach}>
                        </select>：
                        <select class="js-group<{$i1}> js-overtime js-reset<{$i1}> smm" data-reason="#reason<{$i1}>" data-next="#overtime<{$i2}>" data-group=".js-group<{$i1}>" name="over[<{$i0}>][over_start_min]">
                            <option value=""></option>
                            <{foreach from=$fifteen_minutes item=m}>
                                <option value="<{$m|string_format:"%02d"|escape}>"><{$m|string_format:"%02d"|escape}></option>
                            <{/foreach}>
                            <{foreach from=$minutes item=m}>
                                <option value="<{$m|string_format:"%02d"|escape}>" <{if $results.over[$i0].over_start_min != '' && $m==$results.over[$i0].over_start_min}>selected="selected"<{/if}>><{$m|string_format:"%02d"|escape}></option>
                            <{/foreach}>
                        </select>
                        〜
                        <select class="js-group<{$i1}> js-overtime js-reset<{$i1}> ehh" data-reason="#reason<{$i1}>" data-next="#overtime<{$i2}>" data-group=".js-group<{$i1}>" name="over[<{$i0}>][over_end_hour]">
                            <option value=""></option>
                            <{foreach from=$hours48 key=value item=hour}>
                                <option value="<{$value|string_format:"%02d"|escape}>" <{if $results.over[$i0].over_end_hour != '' && $value==$results.over[$i0].over_end_hour}>selected="selected"<{/if}>><{$hour|escape}></option>
                            <{/foreach}>
                        </select>：
                        <select class="js-group<{$i1}> js-overtime js-reset<{$i1}> emm" data-reason="#reason<{$i1}>" data-next="#overtime<{$i2}>" data-group=".js-group<{$i1}>" name="over[<{$i0}>][over_end_min]">
                            <option value=""></option>
                            <{foreach from=$fifteen_minutes item=m}>
                                <option value="<{$m|string_format:"%02d"|escape}>"><{$m|string_format:"%02d"|escape}></option>
                            <{/foreach}>
                            <{foreach from=$minutes item=m}>
                                <option value="<{$m|string_format:"%02d"|escape}>" <{if $results.over[$i0].over_end_min != '' && $m==$results.over[$i0].over_end_min}>selected="selected"<{/if}>><{$m|string_format:"%02d"|escape}></option>
                            <{/foreach}>
                        </select>
                        <button type="button" class="js-overtime-reset" data-reset=".js-reset<{$i1}>">リセット</button>
                        <br>
                        (休憩：
                        <select class="js-overtime js-reset<{$i1}>" name="over[<{$i0}>][rest_over_start_hour]">
                            <option value=""></option>
                            <{foreach from=$hours48 key=value item=hour}>
                                <option value="<{$value|string_format:"%02d"|escape}>" <{if $results.over[$i0].rest_over_start_hour != '' && $value==$results.over[$i0].rest_over_start_hour}>selected="selected"<{/if}>><{$hour|escape}></option>
                            <{/foreach}>
                        </select>：
                        <select class="js-overtime js-reset<{$i1}>" name="over[<{$i0}>][rest_over_start_min]">
                            <option value=""></option>
                            <{foreach from=$fifteen_minutes item=m}>
                                <option value="<{$m|string_format:"%02d"|escape}>"><{$m|string_format:"%02d"|escape}></option>
                            <{/foreach}>
                            <{foreach from=$minutes item=m}>
                                <option value="<{$m|string_format:"%02d"|escape}>" <{if $results.over[$i0].rest_over_start_min != '' && $m==$results.over[$i0].rest_over_start_min}>selected="selected"<{/if}>><{$m|string_format:"%02d"|escape}></option>
                            <{/foreach}>
                        </select>
                        〜
                        <select class="js-overtime js-reset<{$i1}>" name="over[<{$i0}>][rest_over_end_hour]">
                            <option value=""></option>
                            <{foreach from=$hours48 key=value item=hour}>
                                <option value="<{$value|string_format:"%02d"|escape}>" <{if $results.over[$i0].rest_over_end_hour != '' && $value==$results.over[$i0].rest_over_end_hour}>selected="selected"<{/if}>><{$hour|escape}></option>
                            <{/foreach}>
                        </select>：
                        <select class="js-overtime js-reset<{$i1}>" name="over[<{$i0}>][rest_over_end_min]">
                            <option value=""></option>
                            <{foreach from=$fifteen_minutes item=m}>
                                <option value="<{$m|string_format:"%02d"|escape}>"><{$m|string_format:"%02d"|escape}></option>
                            <{/foreach}>
                            <{foreach from=$minutes item=m}>
                                <option value="<{$m|string_format:"%02d"|escape}>" <{if $results.over[$i0].rest_over_end_min != '' && $m==$results.over[$i0].rest_over_end_min}>selected="selected"<{/if}>><{$m|string_format:"%02d"|escape}></option>
                            <{/foreach}>
                        </select>
                        )

                        <{if $err.over_time[$i0]}>
                            <p class="err"><{$err.over_time[$i0]}></p>
                        <{/if}>

                        <div class="js-overtime-reason" id="reason<{$i1}>" style="margin-top:5px;">
                            理由：
                            <select class="js-reason" name="over[<{$i0}>][ovtm_reason_id]" data-text="#reason_text<{$i1}>">
                                <{foreach from=$ovtm_reason_lists item=row}>
                                    <option value="<{$row.reason_id|escape}>" <{if $results.over[$i0].ovtm_reason_id !== '' && $row.reason_id==$results.over[$i0].ovtm_reason_id}>selected="selected"<{/if}>><{$row.reason|escape}></option>
                                <{/foreach}>
                                <option value="" <{if !$results.over[$i0].ovtm_reason_id}>selected="selected"<{/if}>>その他</option>
                            </select><br />
                            <input type="hidden" class="js-hidden" name="over[<{$i0}>][ovtm_reason_name]" value="">
                            <div class="js-overtime-reason-text" id="reason_text<{$i1}>" style="margin-top:5px;">
                                <input type="text" name="over[<{$i0}>][ovtm_reason]" value="<{$results.over[$i0].ovtm_reason|escape}>" size="50" maxlength="100" class="jpn">
                            </div>

                            <{if $err.reason[$i0]}>
                                <p class="err"><{$err.reason[$i0]}></p>
                            <{/if}>
                        </div>
                    </td>
                </tr>
            <{/section}>

            <tr>
                <td class="header">備考</td>
                <td>
                    <textarea name="memo" rows="2" cols="30"><{$results.memo|escape}></textarea>
                </td>
            </tr>

            <tr>
                <td class="header">最終更新日時</td>
                <td><{if $results.update_time}><{$results.update_time|regex_replace:'/^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)/':'$1-$2-$3 $4:$5:$6'|escape}><{/if}>
                </td>
            </tr>

            <tr>
                <td class="header">最終更新者</td>
                <td><{$update_emp_id->emp_name()|escape}>
                </td>
            </tr>
        </table>
        <button type="button" id="submit_close">登録して閉じる</button>
        <button type="button" id="submit_prev" <{if !$prev_emp_id}>disabled="disabled"<{/if}>>登録して前の職員を開く</button>
        <button type="button" id="submit_next" <{if !$next_emp_id}>disabled="disabled"<{/if}>>登録して次の職員を開く</button>
        <{if $dakoku_use}><button type="button" id="submit_dakoku">打刻内容を開く</button><{/if}>

    </form>
</div>
<hr />

<div align="center">
    <button type="button" onclick="window.close();">ウインドウを閉じる</button>
</div>

<form id="dakoku_form" action="<{$dakoku_url}>" method="get" target="_new">
    <input type="hidden" name="staff_no" value="<{$emp.login|escape}>" />
    <input type="hidden" name="user_name" value="<{$emp.utf8_name|escape:'url'}>" />
    <input type="hidden" name="month" value="<{$date|regex_replace:'/^(\d{4})(\d\d)\d\d/':'$1/$2'|escape}>" />
    <input type="hidden" name="day" value="<{$date|regex_replace:'/^\d{6}(\d\d)/':'$1'|escape}>" />
</form>

<{capture name="js"}>
    <script type="text/javascript" src="js/shift/common.js"></script>
    <script type="text/javascript" src="js/shift/results_edit.js"></script>
    <{if $results_update}>
        <script type="text/javascript">
            <!--
            $(function() {
                var update_pattern = <{$pattern_json}>;
                set_parent_results(
                    'e<{$res_emp_id|escape:'javascript'}>',
                    'd<{$res_date|escape:'javascript'}>',
                    'p<{$res_pattern|escape:'javascript'}>',
                    '<{$res_duty|escape:'javascript'}>',
                    '<{$res_tmcd_group_id|escape:'javascript'}>',
                    '<{$res_ovtm|escape:'javascript'}>',
                    '<{$res_ovtmrsn|escape:'javascript'}>',
                    update_pattern
                    );
            });
            // -->
        </script>
    <{/if}>
<{/capture}>
<{include file="shift/footer.tpl"}>
