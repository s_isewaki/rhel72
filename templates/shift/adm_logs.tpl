<{include file="shift/header.tpl" title="管理画面：作業ログ" tab="02"}>

<div id="main">
    <div style="margin:0 0 10px 0; font-size:11px;">
        <form action="shift/adm_logs.php" method="post">
            <b>期間：</b>
            <input type="text" name="srh_start_date" class="datepicker" value="<{$srh_start_date|escape}>" style="width:80px; font-size:11px;" />
            〜
            <input type="text" name="srh_last_date" class="datepicker" value="<{$srh_last_date|escape}>" style="width:80px; font-size:11px;" />

            &nbsp;&nbsp;<b>職員：</b>
            <input type="text" id="srh_name" name="srh_name" value='<{$srh_name|escape}>' style="width:100px; font-size:11px;" />

            &nbsp;&nbsp;<b>機能：</b>
            <select name="srh_function">
                <option value="">すべて</option>
                <{foreach key=value item=name from=$function}>
                    <option value="<{$value|escape}>" <{if $value === $srh_function}>selected="selected"<{/if}>><{$name|escape}></option>
                <{/foreach}>
            </select>

            &nbsp;&nbsp;<b>パラメータ：</b>
            <input type="text" id="srh_keyword" name="srh_keyword" value='<{$srh_keyword|escape}>' style="width:100px; font-size:11px;" />

            <button type="submit" name="btn_search" value="search" class="btn btn-small btn-primary"><i class="icon-search"></i> 検索</button>
        </form>
    </div>

    <div style="margin:0 0 5px 0; font-size:14px; text-align:right;">
        <a href="shift/adm_logs.php?p=1">&lt;&lt;</a>&nbsp;
        <{foreach item=p from=$pages}>
            <a href="shift/adm_logs.php?p=<{$p}>"><{$p}></a>&nbsp;
        <{/foreach}>
        <a href="shift/adm_logs.php?p=<{$max_pages}>">&gt;&gt;</a>
    </div>

    <table class="list">
        <thead>
            <tr>
                <td>日時</td>
                <td>職員</td>
                <td>機能</td>
                <td>メッセージ</td>
                <td>パラメータ</td>
            </tr>
        </thead>
        <tbody>
            <{foreach from=$lists item=row}>
                <tr>
                    <td nowrap><{$row.timestamp|date_format_jp:"%Y年%-m月%-d日(%a) %R"}></td>
                    <td nowrap><span title="<{$row.emp_personal_id|escape}>"><{$row.emp_name|escape}></span></td>
                    <td nowrap><{$function[$row.function]|default:$row.function|escape}></td>
                    <td nowrap><{$row.message|escape}></td>
                    <td class="param"><span class="line"><{$row.param|truncate:200}></span><span class="detail" style="white-space: pre; display:none;"><{$row.param}></span></td>
                </tr>
            <{/foreach}>
        </tbody>
    </table>
</div>


<{capture name="js"}>
    <script type="text/javascript" src="js/shift/common.js"></script>
    <script type="text/javascript">
        <!--
        $(function() {
            $(".datepicker").datepicker({
                dateFormat: 'yy-mm-dd',
                buttonImage: 'img/calendar_link.gif',
                buttonImageOnly: true,
                showOn: 'button'
            });

            $('.param').toggle(
                function() {
                    $(this).find('.line').hide();
                    $(this).find('.detail').show();
                },
                function() {
                    $(this).find('.line').show();
                    $(this).find('.detail').hide();
                }
            );

        });
        // -->
    </script>
<{/capture}>

<{include file="shift/footer.tpl"}>
