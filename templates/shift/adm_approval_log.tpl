<{include file="shift/header.tpl" title="管理画面：承認ログ" tab="03"}>
<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css" />

<div id="main">
    <div style="margin:0 0 10px 0; font-size:11px;">
        <form action="shift/adm_approval_log.php" method="post">
            <b>期間：</b>
            <input type="text" name="srh_start_date" class="datepicker" value="<{$srh_start_date|escape}>" style="width:80px; font-size:11px;" />
            〜
            <input type="text" name="srh_last_date" class="datepicker" value="<{$srh_last_date|escape}>" style="width:80px; font-size:11px;" />

            &nbsp;&nbsp;<b>職員：</b>
            <input type="text" id="srh_name" name="srh_name" value='<{$srh_name|escape}>' style="width:150px; font-size:11px;" />

            <button type="submit" name="btn_search" value="search" class="btn btn-small btn-primary"><i class="icon-search"></i> 検索</button>
        </form>
    </div>

    <div style="margin:0 0 5px 0; font-size:14px; text-align:right;">
        <a href="shift/adm_approval_log.php?p=1">&lt;&lt;</a>&nbsp;
        <{foreach item=p from=$pages}>
            <a href="shift/adm_approval_log.php?p=<{$p}>"><{$p}></a>&nbsp;
        <{/foreach}>
        <a href="shift/adm_approval_log.php?p=<{$max_pages}>">&gt;&gt;</a>
    </div>

    <table class="list" id="edit-table">
        <thead>
            <tr>
                <td colspan="2">承認日時</td>
                <td>年月</td>
                <td>職員</td>
                <td colspan="2">承認者</td>
            </tr>
        </thead>
        <tbody>
            <{foreach from=$lists item=row key=k}>
                <tr>
                    <td nowrap id="approval_date_<{$row.id|escape}>"><{$row.approval_date|date_format_jp:"%Y年%-m月%-d日(%a) %R"}></td>
                    <td nowrap><input class="js-editdate" type="button" value="変更" data-flag='false' data-date="<{$row.approval_date|date_format_jp:"%Y-%02m-%02d_%R"}>" data-id="<{$row.id|escape}>"></td>
                    <td nowrap><{$row.year|string_format:'%04d'|escape}>-<{$row.month|string_format:'%02d'|escape}></td>
                    <td nowrap><{$row.emp_name|escape}></td>
                    <td nowrap><{$row.approval_emp_name|escape}></td>
                    <td nowrap>(<{if $row.approval_type === '0'}>本人<{elseif $row.approval_type === '1'}>所属長<{else}>部門長<{/if}>)</td>
                </tr>
            <{/foreach}>
        </tbody>
    </table>
</div>

<{capture name="js"}>
    <script type="text/javascript" src="js/shift/common.js"></script>
    <script type="text/javascript" src="js/jquery/jquery.datetimepicker.js"></script>
    <script type="text/javascript" src="js/shift/adm_approval_log.js"></script>
<{/capture}>

<{include file="shift/footer.tpl"}>
