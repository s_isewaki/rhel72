<{include file="shift/header.tpl" title="管理画面：オプション" tab="08"}>

<{include file="shift/adm_options_submenu.tpl"}>

<div id="main">

    <form action="shift/adm_options_notice.php" method="post">
        <input type="hidden" name="mode" value="confirm" />

        <fieldset>
            <legend>通知する職員</legend>
            <button type="button" onclick="openEmployeeList('<{$SESSION}>', '<{$BASE_HREF}>', 'employee');">追加する</button>
            <div id="target_user_area" class="emp_division">
                <{foreach from=$emplist item=row}>
                    <div id="emp_<{$row->emp_id()|escape}>">
                        <{$row->emp_name()|escape}>
                        <a class="close" href="javascript:void(0);" onclick="remove_row(this);">[X]</a>
                        <input type="hidden" name="employee[]" value="<{$row->emp_id()|escape}>" />
                    </div>
                <{/foreach}>
            </div>
        </fieldset>

        <fieldset>
            <legend>通知するメールアドレス</legend>
            <button type="button" onclick="add_email_fields('target_email_area');">追加する</button>
            <div id="target_email_area" class="emp_division">
                <{foreach from=$email item=address}>
                    <div>
                        <input type="text" name="email[]" value="<{$address|escape}>" size="50" />
                        <a class="close" href="javascript:void(0);" onclick="remove_row(this);">[X]</a>
                    </div>
                <{/foreach}>
            </div>
        </fieldset>

        <button type="submit" name="btn" value="save">登録する</button>
        <button type="submit" name="btn" value="testmail">テスト送信する</button>
    </form>

</div>

<{capture name="js"}>
    <script type="text/javascript" src="js/shift/common.js"></script>
    <script type="text/javascript" src="js/shift/adm_ward_form.js"></script>
<{/capture}>
<{include file="shift/footer.tpl"}>
