<{include file="shift/header.tpl" title="当月職員設定" tab="11"}>

<script type="text/javascript">
    <!--
    var this_group_id = '<{$this_ward->group_id()}>';
    var this_y = '<{$year}>';
    var this_m = '<{$month}>';
    // -->
</script>

<div id="loading" style="display: none;"></div>

<div id="submenu">
    <ul>
        <li>
            <a id="back" href="shift/plan.php?y=<{$year|escape}>&amp;m=<{$month|escape}>&amp;group_id=<{$this_ward->group_id()|escape}>">勤務予定に戻る</a>
        </li>
    </ul>
</div>

<div id="err" class="alert" style="display: none;"></div>

<div id="main">
    <form action="shift/staff_plan.php" method="post">
        <input type="hidden" name="mode" value="confirm" />
        <input type="hidden" name="y" value="<{$year}>" />
        <input type="hidden" name="m" value="<{$month}>" />
        <input type="hidden" name="group_id" value="<{$this_ward->group_id()}>" />

        <div class="title"><{$this_ward->group_name()|escape}>：<{$vyear|escape}>/<{$vmonth|escape}></div>

        <div>
            <button type="button" onclick="openEmployeeList('<{$SESSION}>', '<{$BASE_HREF}>');">職員を追加する(当月のみ)</button>
            <button type="submit">更新する</button>
            <button type="button" onclick="importMaster();">職員設定を取り込む</button>
        </div>
        <table class="list" id="staff">

            <thead>
                <tr>
                    <td rowspan="2">表示順</td>
                    <td rowspan="2" colspan="<{$team_rows|escape}>">チーム<br />(当月のみ)</td>
                    <td rowspan="2">氏名</td>
                    <td colspan="2">稼働時間</td>
                    <td rowspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td>繰越時間</td>
                    <td>精算時間</td>
                </tr>
            </thead>

            <tbody>
                <{foreach from=$staff item=row}>
                    <tr id="<{$row->emp_id()|escape}>">
                        <td class="dragHandle">
                            <input type="hidden" name="order[]" value="<{$row->emp_id()|escape}>" />
                            <input type="hidden" name="main_group_id[<{$row->emp_id()|escape}>]" value="<{$row->main_group_id()|escape}>" />
                        </td>
                        <td>
                            <label><input type="radio" name="team_id[<{$row->emp_id()|escape}>]" value="" <{if ! $row->team_id()}>checked<{/if}> /> なし</label>
                        </td>
                        <{foreach from=$team item=t}>
                            <td>
                                <label><input type="radio" name="team_id[<{$row->emp_id()|escape}>]" value="<{$t.team_id|escape}>" <{if $t.team_id == $row->team_id()}>checked<{/if}>/> <{$t.team_name|escape}></label>
                            </td>
                        <{/foreach}>
                        <td <{if $row->assist()}>class="assist"<{/if}>><{$row->emp_name()|escape}></td>
                        <td class="right"><{$row->carryover()|escape}></td>
                        <td><input type="text" name="adjustment[<{$row->emp_id()|escape}>]" value="<{$row->adjustment()|escape}>" size="6" class="num" /></td>
                        <td><button type="button" onclick="removeStaff(this);">削除する(当月のみ)</button></td>
                    </tr>
                <{/foreach}>
            </tbody>
        </table>
        <div>
            <button type="button" onclick="openEmployeeList('<{$SESSION}>', '<{$BASE_HREF}>');">職員を追加する(当月のみ)</button>
            <button type="submit">更新する</button>
        </div>
    </form>
</div>

<{capture name="js"}>
    <script type="text/javascript" src="js/shift/common.js"></script>
    <script type="text/javascript" src="js/shift/staff_plan.js"></script>
<{/capture}>
<{include file="shift/footer.tpl"}>
