<{include file="shift/header.tpl" title="��̳����" tab="12"}>

<{if ! $ward_lists}>
    <div class="attention">���ѤǤ��ޤ���</div>
<{else}>
    <style type="text/css">
        <!--
        html {
            overflow: auto;
            overflow-y: hidden;
        }
        // -->
    </style>

    <script type="text/javascript">
        <!--
        var base_href = '<{$BASE_HREF}>/';
        var this_group_id = '<{$this_ward->group_id()}>';
        var this_pattern_id = '<{$this_ward->pattern_id()}>';
        var this_y = '<{$year}>';
        var this_m = '<{$month}>';
        var cours = 'cours';
        var pattern = <{$pattern_json}>;
        var pattern_ok = <{$pattern_ok}>;
        var display = <{$this_ward->display()}>;
        var data = <{$table->table_json()}>;
        var fscreen = <{if $fullscreen}>true<{else}>false<{/if}>;
            // -->
    </script>

    <div id="shift">
        <div id="wrapHead">
            <{if !$fullscreen}>
                <!-- ����ץ������ -->
                <form name="shift/results.php" method="get">
                    <input type="hidden" id="this_y" name="y" value="<{$year}>" />
                    <input type="hidden" id="this_m" name="m" value="<{$month}>" />
                    <input type="hidden" id="this_mode" name="mode" value="cours" />
                    <div class="pulldown">
                        <b>����</b>��
                        <select id="this_group_id" name="group_id" onchange="submit(this.form);">
                            <{foreach from=$ward_lists item=row}>
                                <option value="<{$row->group_id()|escape}>" <{if $row->group_id() === $this_ward->group_id()}>selected<{/if}>><{$row->group_name()|escape}></option>
                            <{/foreach}>
                        </select>
                    </div>
                </form>

                <!-- ��������ڤ��ؤ����� -->
                <div id="month">
                    <ul>
                        <li><a href="shift/results.php?y=<{$lastyear}>&amp;m=<{$month}>&amp;group_id=<{$this_ward->group_id()|escape}>"><span>&lt;��ǯ</span></a></li>
                            <{foreach from=$yearmonth item=row}>
                            <li <{if $row.i === 0}>class="selected"<{/if}>>
                                <a href="shift/results.php?y=<{$row.year}>&amp;m=<{$row.month}>&amp;group_id=<{$this_ward->group_id()|escape}>"><span><{$row.vyear}>/<{$row.vmonth}></span></a>
                            </li>
                        <{/foreach}>
                        <li><a href="shift/results.php?y=<{$nextyear}>&amp;m=<{$month}>&amp;group_id=<{$this_ward->group_id()|escape}>"><span>��ǯ&gt;</span></a></li>
                    </ul>
                </div>
                <div class="clear"></div>
            <{/if}>

            <div id="loading"></div>

            <!-- �ܥ��� -->
            <div id="shiftmenu">
                <ul id="shiftmenu_icon">
                    <li id="icon_results"><img src="img/ico-illust-touroku.gif" alt="��Ͽ" width="36" height="36" border="0" /><br />��Ͽ</li>
                    <li id="icon_stamp"><img src="img/ico-illust-stamp.gif" alt="�������" width="36" height="36" border="0" /><br />�������</li>
                    <li id="icon_reload"><img src="img/ico-illust-saikeisan.gif" alt="�Ʒ׻�" width="36" height="36"border="0" /><br />�Ʒ׻�</li>
                    <li id="icon_copy"><img src="img/ico-illust-copy-kako.gif" alt="���ԡ�" width="36" height="36" border="0" /><br />���ԡ�</li>
                    <{if !$fullscreen}><li id="icon_fullscreen"><img src="img/ico-illust-zengamen.gif" alt="������" width="36" height="36" border="0" /><br />������</li><{/if}>
                </ul>
                <ul id="shiftmenu_button">
                    <li><button type="button" id="button_zangyo">�Ķ�ɽ��</button></li>

                    <{if $this_ward->month_type() === '2' && !$fullscreen}>
                        <li>
                            <button type="button" class="btn_view" data-y="<{$year}>" data-m="<{$month}>" data-mode="month">��ɽ��</button>
                        </li>
                    <{/if}>
                </ul>
                <ul id="shiftview_button">
                    <li><button type="button" id="button_team">������</button></li>
                    <li><button type="button" id="button_job">����</button></li>
                    <li><button type="button" id="button_status">��</button></li>
                    <li><button type="button" id="button_kadou">��Ư����</button></li>
                    <li><button type="button" id="button_count">����</button></li>
                    <li><button type="button" id="button_holiday">ͭ��</button></li>
                    <li><button type="button" id="button_summer">�Ƶ�</button></li>
                    <li><button type="button" id="button_week">����</button></li>
                </ul>
            </div>
            <div class="clear"></div>

            <!-- ���� -->
            <div id="info">
                <span><{$start_date}> �� <{$end_date}></span>
                <{if $this_ward->month_type() === '2'}>
                    <{assign var=working_hours value=$whours.working_hours/60}>
                    <span>�ײ�Ư���֡�<{$working_hours}>���� (����<{$whours.holiday_count}>��)</span>
                <{/if}>
                <span id="night_hours_avg" title="��н����Ԥα����л��֡�<{$table->night_hours('results')|escape}>���� �� ��н����Կ���<{$table->night_nurse_count('results')|escape}>��">��ʿ����л��֡�<{$table->night_hours_avg('results')|escape}>����</span>
            </div>
            <div class="clear"></div>

            <!-- ��̳ɽ header -->
            <table id="results_head" class="shift">
                <thead>
                    <tr>
                        <td rowspan="2" class="scrollbar">&nbsp;</td>
                        <td rowspan="2" class="team">����<br />��</td>
                        <td rowspan="2" class="name">��̾</td>
                        <td rowspan="2" class="job">����</td>
                        <td rowspan="2" class="status">��</td>
                        <{foreach from=$dates item=dt}>
                            <td rowspan="2" class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}>"><{$dt.d}><br /><{$dt.wj}></td>
                            <{/foreach}>
                        <td class="kadou" colspan="4">��Ư����</td>

                        <td rowspan="2" class="total timew">��Ư<br />����</td>
                        <td rowspan="2" class="total timew">�Ķ�<br />����</td>

                        <td class="total" colspan="<{$count_row_count|escape}>">����</td>
                        <td class="holiday" colspan="4">ͭ��</td>
                        <td class="summer" colspan="3">�Ƶ�</td>
                        <td class="week" colspan="3">����</td>
                        <td rowspan="2" class="name">��̾</td>
                    </tr>
                    <tr>
                        <td class="kadou timew">����<br />����</td>
                        <td class="kadou timew">����<br />��Ư</td>
                        <td class="kadou timew">��Ư<br />��</td>
                        <td class="kadou timew">���<br />����</td>

                        <{foreach from=$count_row item=cr}>
                            <td class="total countw" style="font-size:<{$cr.size|default:10|escape}>px;"><{$cr.name|escape}></td>
                        <{/foreach}>
                        <td class="holiday countw">����</td>
                        <td class="holiday countw">��Ϳ</td>
                        <td class="holiday countw">����</td>
                        <td class="holiday countw">��</td>
                        <td class="summer countw">��Ϳ</td>
                        <td class="summer countw">����</td>
                        <td class="summer countw">��</td>
                        <td class="week countw">��Ϳ</td>
                        <td class="week countw">����</td>
                        <td class="week countw">��</td>
                    </tr>
                </thead>
            </table>
        </div>
        <div id="wrapCont">
            <!-- ��̳ɽ -->
            <table id="results" class="shift">
                <tfoot>
                    <{foreach from=$count_col item=cc}>
                        <tr class="count">
                            <td class="team">&nbsp;</td>
                            <td class="name"><{$cc.name}></td>
                            <td class="job">&nbsp;</td>
                            <td class="status">&nbsp;</td>
                            <{foreach from=$dates item=dt}>
                                <td id="d<{$dt.date}>_c<{$cc.count}>" class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}>"><{$table->cell_col_count($dt.date,$cc.count,'results')|default:'0'|escape}></td>
                            <{/foreach}>
                        </tr>
                    <{/foreach}>
                </tfoot>

                <tbody>
                    <{foreach from=$staff item=row}>
                        <!-- ��̳ͽ�� -->
                        <tr class="e<{$row->emp_id()}>_plan plan">
                            <td id="e<{$row->emp_id()}>_team" class="team"><{$table->cell_emp($row->emp_id(),'team')|escape}></td>
                            <td id="e<{$row->emp_id()}>_name" class="name <{if $row->assist()}>assist<{/if}>"><{$table->cell_emp($row->emp_id(),'name')|escape}></td>
                            <td id="e<{$row->emp_id()}>_job" class="job"><{$table->cell_emp($row->emp_id(),'job_name')|escape}></td>
                            <td id="e<{$row->emp_id()}>_status" class="status"><{$table->cell_emp($row->emp_id(),'st_name')|escape}></td>
                            <{foreach from=$dates item=dt}>
                                <td id="e<{$row->emp_id()}>_d<{$dt.date}>_plan"
                                    class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}> <{if $row->is_employed($dt.date)}><{else}>retire<{/if}>"
                                    style="<{$table->cell_style($row->emp_id(),$dt.date,'plan')|escape}>">
                                    <{$table->cell_sign($row->emp_id(),$dt.date,'plan')|escape}>
                                </td>
                            <{/foreach}>

                            <td id="e<{$row->emp_id()}>_wh_over_plan" class="kadou time"><{$table->cell_emp($row->emp_id(),'wh_over','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_wh_hour_plan" class="kadou time"><{$table->cell_emp($row->emp_id(),'wh_hour','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_wh_diff_plan" class="kadou time" title="<{$table->cell_emp($row->emp_id(),'wh_base','plan')|escape}>"><{$table->cell_emp($row->emp_id(),'wh_diff','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_wh_night_plan" class="kadou time"><{$table->cell_emp($row->emp_id(),'wh_night','plan')|escape}></td>

                            <td class="total timew">&nbsp;</td>
                            <td class="total timew">&nbsp;</td>

                            <{foreach from=$count_row item=cr}>
                                <td id="e<{$row->emp_id()}>_c<{$cr.count}>_plan" class="total count"><{$table->cell_row_count($row->emp_id(),$cr.count,'plan')|default:'0'|escape}></td>
                            <{/foreach}>

                            <td id="e<{$row->emp_id()}>_paid_h_kuri_plan" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_kuri','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_fuyo_plan" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_fuyo','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_get_plan" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_get','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_plan"     class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h','plan')|escape}></td>

                            <td id="e<{$row->emp_id()}>_paid_s_all_plan" class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s_all','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_get_plan" class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s_get','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_plan"     class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s','plan')|escape}></td>

                            <td id="e<{$row->emp_id()}>_paid_w_all_plan" class="week count"><{$table->cell_emp($row->emp_id(),'paid_w_all','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_get_plan" class="week count"><{$table->cell_emp($row->emp_id(),'paid_w_get','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_plan"     class="week count"><{$table->cell_emp($row->emp_id(),'paid_w','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_name" class="name <{if $row->assist()}>assist<{/if}>"><{$table->cell_emp($row->emp_id(),'name')|escape}></td>
                        </tr>

                        <!-- ��̳���� -->
                        <tr class="e<{$row->emp_id()}>_results results input">
                            <td class="team filler">&nbsp;</td>
                            <td class="name filler">��̳����</td>
                            <td class="job filler">&nbsp;</td>
                            <td class="status filler">&nbsp;</td>
                            <{foreach from=$dates item=dt}>
                                <td id="e<{$row->emp_id()}>_d<{$dt.date}>_results"
                                    class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}> <{if $row->is_employed($dt.date)}>cell<{else}>retire<{/if}>"
                                    style="<{$table->cell_style($row->emp_id(),$dt.date,'results')|escape}>">
                                    <{$table->cell_sign($row->emp_id(),$dt.date,'results')|escape}>
                                </td>
                            <{/foreach}>

                            <td id="e<{$row->emp_id()}>_wh_over_results" class="kadou time"><{$table->cell_emp($row->emp_id(),'wh_over','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_wh_hour_results" class="kadou time"><{$table->cell_emp($row->emp_id(),'wh_hour','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_wh_diff_results" class="kadou time" title="<{$table->cell_emp($row->emp_id(),'wh_base','results')|escape}>"><{$table->cell_emp($row->emp_id(),'wh_diff','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_wh_night_results" class="kadou time"><{$table->cell_emp($row->emp_id(),'wh_night','results')|escape}></td>

                            <td class="total time"><{$table->cell_emp($row->emp_id(),'total_worktime')|min2time}></td>
                            <td class="total time"><{$table->cell_emp($row->emp_id(),'total_overtime')|min2time}></td>

                            <{foreach from=$count_row item=cr}>
                                <td id="e<{$row->emp_id()}>_c<{$cr.count}>_results" class="total count"><{$table->cell_row_count($row->emp_id(),$cr.count,'results')|default:'0'|escape}></td>
                            <{/foreach}>

                            <td id="e<{$row->emp_id()}>_paid_h_kuri_results" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_kuri','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_fuyo_results" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_fuyo','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_get_results" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_get','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_results"     class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h','results')|escape}></td>

                            <td id="e<{$row->emp_id()}>_paid_s_all_results" class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s_all','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_get_results" class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s_get','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_results"     class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s','results')|escape}></td>

                            <td id="e<{$row->emp_id()}>_paid_w_all_results" class="week count"><{$table->cell_emp($row->emp_id(),'paid_w_all','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_get_results" class="week count"><{$table->cell_emp($row->emp_id(),'paid_w_get','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_results"     class="week count"><{$table->cell_emp($row->emp_id(),'paid_w','results')|escape}></td>
                            <td class="name filler">��̳����</td>
                        </tr>

                        <!-- �ĶȻ��� -->
                        <tr class="e<{$row->emp_id()}>_results_ovtm results_ovtm">
                            <td class="team filler">&nbsp;</td>
                            <td class="name filler">�ĶȻ���</td>
                            <td class="job filler">&nbsp;</td>
                            <td class="status filler">&nbsp;</td>
                            <{foreach from=$dates item=dt}>
                                <td id="e<{$row->emp_id()}>_d<{$dt.date}>_results_ovtm" class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}> <{if $row->is_employed($dt.date)}><{else}>retire<{/if}>">
                                    <{$table->cell_sign($row->emp_id(),$dt.date,'results_ovtm')|escape}>
                                </td>
                            <{/foreach}>

                            <td class="kadou filler"></td>
                            <td class="kadou filler"></td>
                            <td class="kadou filler"></td>
                            <td class="kadou filler"></td>

                            <td class="total filler"></td>
                            <td class="total filler"></td>

                            <{foreach from=$count_row item=cr}>
                                <td class="total filler"></td>
                            <{/foreach}>

                            <td class="holiday filler"></td>
                            <td class="holiday filler"></td>
                            <td class="holiday filler"></td>
                            <td class="holiday filler"></td>

                            <td class="summer filler"></td>
                            <td class="summer filler"></td>
                            <td class="summer filler"></td>

                            <td class="week filler"></td>
                            <td class="week filler"></td>
                            <td class="week filler"></td>
                            <td class="name filler"></td>
                        </tr>

                        <!-- �Ķ���ͳ -->
                        <tr class="e<{$row->emp_id()}>_results_ovtmrsn results_ovtmrsn">
                            <td class="team filler">&nbsp;</td>
                            <td class="name filler">�Ķ���ͳ</td>
                            <td class="job filler">&nbsp;</td>
                            <td class="status filler">&nbsp;</td>
                            <{foreach from=$dates item=dt}>
                                <td id="e<{$row->emp_id()}>_d<{$dt.date}>_results_ovtmrsn" class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}> <{if $row->is_employed($dt.date)}><{else}>retire<{/if}>">
                                    <{$table->cell_sign($row->emp_id(),$dt.date,'results_ovtmrsn')|escape}>
                                </td>
                            <{/foreach}>

                            <td class="kadou filler"></td>
                            <td class="kadou filler"></td>
                            <td class="kadou filler"></td>
                            <td class="kadou filler"></td>

                            <td class="total filler"></td>
                            <td class="total filler"></td>

                            <{foreach from=$count_row item=cr}>
                                <td class="total filler"></td>
                            <{/foreach}>

                            <td class="holiday filler"></td>
                            <td class="holiday filler"></td>
                            <td class="holiday filler"></td>
                            <td class="holiday filler"></td>

                            <td class="summer filler"></td>
                            <td class="summer filler"></td>
                            <td class="summer filler"></td>

                            <td class="week filler"></td>
                            <td class="week filler"></td>
                            <td class="week filler"></td>
                            <td class="name filler"></td>
                        </tr>
                    <{/foreach}>
                </tbody>
            </table>
        </div>
    </div>

    <!-- ������� -->
    <div id="stamp">
        <div id="stamp_header">�������<span class="close">[��]</span></div>
        <div id="current_window"><b>����</b><div id="current_stamp" style="font-size:10px;">̤����</div></div>
        <ul id="stamp_list">
            <li class="stamp" onclick="setCurrentStamp('0');" style="font-size:10px;">̤����</li>
                <{foreach from=$pattern_list item=row}>
                    <{if $row->font_name() != ''}>
                    <li class="stamp" onclick="setCurrentStamp('<{$row->ptn_id()}>');" style="font-size:12px;color:<{$row->font_color_id()|escape}>; background-color:<{$row->back_color_id()|escape}>;" title="<{$row->ptn_name()|escape}>"><{$row->font_name()|escape}></li>
                    <{/if}>
                <{/foreach}>
        </ul>
    </div>

    <{capture name="js"}>
        <script type="text/javascript" src="js/shift/common.js" charset="euc-jp"></script>
        <script type="text/javascript" src="js/shift/results.js" charset="euc-jp"></script>
        <script type="text/javascript">
            <!--
            if (fscreen) {
                resizeIframeEx();
            }
            // -->
        </script>
    <{/capture}>
<{/if}>
<{include file="shift/footer.tpl"}>
