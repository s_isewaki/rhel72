<div id="submenu">
    <ul>
        <li>
            <a href="shift/adm_options.php"><span>全体設定</span></a>
        </li>
        <li>
            <a href="shift/adm_options_overtime.php"><span>時間外超過</span></a>
        </li>
        <li>
            <a href="shift/adm_options_yukyu.php"><span>有休一括付与</span></a>
        </li>
        <li>
            <a href="shift/adm_options_cours.php"><span>クール基準日</span></a>
        </li>
        <li>
            <a href="shift/adm_options_working_hours.php"><span>要稼働時間</span></a>
        </li>
        <li>
            <a href="shift/adm_options_notice.php"><span>通知</span></a>
        </li>
        <li>
            <a href="shift/adm_options_maintenance.php"><span>メンテナンス</span></a>
        </li>
    </ul>
</div>
