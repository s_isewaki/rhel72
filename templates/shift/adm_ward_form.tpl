<{include file="shift/header.tpl" title="管理画面：部署" tab="01"}>

<script type="text/javascript">
    <!--
    var class_count = '$emp_class->class_count()';
    var pulldown_class = '<{pulldown array=$emp_class->class_lists()}>';
    var pulldown_atrb  = '<{class_pulldown array=$emp_class->atrb_lists()}>';
    var pulldown_dept  = '<{class_pulldown array=$emp_class->dept_lists()}>';
    <{if $emp_class->class_count() != "3"}>
        var pulldown_room  = '<{class_pulldown array=$emp_class->room_lists()}>';
    <{/if}>
        var pulldown_job   = '<{pulldown array=$emp_job->lists()}>';
        var pulldown_st    = '<{pulldown array=$emp_status->lists()}>';
        -->
</script>

<div id="main">

    <div class="subheader">部署の登録・更新</div>

    <form action="shift/adm_ward_form.php" method="post">
        <input type="hidden" name="mode" value="confirm" />
        <input type="hidden" name="group_id" value="<{$ward->group_id()|escape}>" />

        <table class="form">
            <col class="header" />
            <col class="input" />

            <tr>
                <td>部署名</td>
                <td>
                    <input type="text" name="group_name" value="<{$ward->group_name()|escape}>" class="jpn" size="50" maxlength="50" />
                    <{if $err.group_name}>
                        <p class="err"><{$err.group_name}></p>
                    <{/if}>
                </td>
            </tr>

            <tr>
                <td>施設基準</td>
                <td>
                    <select name="standard_id">
                        <option value="">適用なし</option>
                        <{foreach from=$inst_lists item=inst_id}>
                            <option value="<{$inst_id|escape}>" <{if $inst_id==$ward->standard_id()}>selected<{/if}>>施設基準<{$inst_id|escape}></option>
                        <{/foreach}>
                    </select>
                    <{if $err.standard_id}>
                        <p class="err"><{$err.standard_id}></p>
                    <{/if}>
                </td>
            </tr>

            <tr>
                <td>勤務パターン</td>
                <td>
                    <select name="pattern_id">
                        <{foreach from=$pgroup_lists item=row}>
                            <option value="<{$row->group_id()|escape}>" <{if $row->group_id()==$ward->pattern_id()}>selected<{/if}>><{$row->group_name()|escape}></option>
                        <{/foreach}>
                    </select>
                    <{if $err.pattern_id}>
                        <p class="err"><{$err.pattern_id}></p>
                    <{/if}>
                </td>
            </tr>

            <tr>
                <td>勤務表の周期</td>
                <td>
                    <label><input type="radio" name="month_type" value="1" <{if "1"==$ward->month_type()}>checked<{/if}>/> 1ヶ月周期</label>
                    <label><input type="radio" name="month_type" value="2" <{if "2"==$ward->month_type()}>checked<{/if}>/> 28日周期</label>
                        <{if $err.month_type}>
                        <p class="err"><{$err.month_type}></p>
                    <{/if}>
                </td>
            </tr>

            <tr>
                <td>
                    シフト管理者<br />
                    (最大11名まで)<br />
                    <br />
                    <button type="button" onclick="openEmployeeList('<{$SESSION}>', '<{$BASE_HREF}>', 'admin');">追加する</button>
                </td>
                <td>
                    <div id="target_admin_area" class="emp_division">
                        <{foreach from=$ward->administrator() item=admin}>
                            <div id="emp_<{$admin->emp_id()|escape}>">
                                <{$admin->emp_name()|escape}>
                                <a class="close" href="javascript:void(0);" onclick="remove_row(this);">[X]</a>
                                <input type="hidden" name="administrator[]" value="<{$admin->emp_id()|escape}>" />
                            </div>
                        <{/foreach}>
                    </div>
                    <{if $err.administrator}>
                        <p class="err"><{$err.administrator}></p>
                    <{/if}>
                </td>
            </tr>

            <tr>
                <td>
                    参照可能な職員<br />
                    <br />
                    <button type="button" onclick="openEmployeeList('<{$SESSION}>', '<{$BASE_HREF}>', 'user');">追加する</button>
                </td>
                <td>
                    <div id="target_user_area" class="emp_division">
                        <{foreach from=$ward->access_user() item=row}>
                            <div id="emp_<{$row->emp_id()|escape}>">
                                <{$row->emp_name()|escape}>
                                <a class="close" href="javascript:void(0);" onclick="remove_row(this);">[X]</a>
                                <input type="hidden" name="access_user[]" value="<{$row->emp_id()|escape}>" />
                            </div>
                        <{/foreach}>
                    </div>
                    <{if $err.user}>
                        <p class="err"><{$err.user}></p>
                    <{/if}>
                </td>
            </tr>

            <tr>
                <td>
                    参照可能な職員属性<br />
                    <br />
                    <button type="button" onclick="add_permission('target_permission_area');">追加する</button>
                </td>
                <td>
                    <div id="target_permission_area">
                        <{foreach from=$ward->access_permission() item=row}>
                            <div>
                                <select onchange="change_emp_class(this);" class="class" name="access_class[]">
                                    <option value="">-</option>
                                    <{pulldown array=$emp_class->class_lists() id=$row.class_id}>
                                </select>
                                &gt; <select onchange="change_emp_atrb(this);" class="atrb" name="access_atrb[]">
                                    <option value="">-</option>
                                    <{class_pulldown array=$emp_class->atrb_lists() id=$row.atrb_id}>
                                </select>
                                &gt; <select onchange="change_emp_dept(this);" class="dept" name="access_dept[]">
                                    <option value="">-</option>
                                    <{class_pulldown array=$emp_class->dept_lists() id=$row.dept_id}>
                                </select>
                                <{if $emp_class->class_count() != '3'}>
                                    &gt; <select class="room" name="access_room[]">
                                        <option value="">-</option>
                                        <{class_pulldown array=$emp_class->room_lists() id=$row.room_id}>
                                    </select>
                                <{/if}>
                                AND <select class="job" name="access_job[]">
                                    <option value="">-</option>
                                    <{pulldown array=$emp_job->lists() id=$row.job_id}>
                                </select>
                                AND <select class="status" name="access_status[]">
                                    <option value="">-</option>
                                    <{pulldown array=$emp_status->lists() id=$row.st_id}>
                                </select>
                                <a class="close" href="javascript:void(0);" onclick="remove_row(this);">[X]</a>
                            </div>
                        <{/foreach}>
                    </div>
                </td>
            </tr>

            <tr>
                <td>連携用部署コード</td>
                <td>
                    <input type="text" id="sfc_group_code" name="sfc_group_code" size="7" maxlength="6" value="<{$ward->sfc_group_code()|escape}>" class="eng" />
                    <{if $err.sfc_group_code}>
                        <p class="err"><{$err.sfc_group_code}></p>
                    <{/if}>
                </td>
            </tr>

            <tr>
                <td>買取日対象</td>
                <td>
                    <label><input type="radio" name="kaitori_day" value="1" <{if "1"==$ward->kaitori_day()}>checked<{/if}>/> 対象である</label>
                    <label><input type="radio" name="kaitori_day" value="0" <{if "0"==$ward->kaitori_day()}>checked<{/if}>/> 対象ではない</label>
                        <{if $err.kaitori_day}>
                        <p class="err"><{$err.kaitori_day}></p>
                    <{/if}>
                </td>
            </tr>

            <tr>
                <td>
                    部門長<br />
                    <br />
                    <button type="button" onclick="openEmployeeList('<{$SESSION}>', '<{$BASE_HREF}>', 'manager');">選択する</button>
                </td>
                <td>
                    <{assign var=manager_item value=$ward->manager()}>
                    <div id="target_manager_area" class="emp_division">
                        <{if $manager_item}>
                            <{$manager_item->emp_name()|escape}>
                        <{/if}>
                    </div>
                    <input type="hidden" id="manager" name="manager" value="<{if $manager_item}><{$manager_item->emp_id()|escape}><{/if}>" />
                    <{if $err.manager}>
                        <p class="err"><{$err.manager}></p>
                    <{/if}>
                </td>
            </tr>

            <tr>
                <td>職員による予定の変更</td>
                <td>
                    <label><input type="radio" name="plan_edit_flg" value="t" <{if $ward->plan_edit_flg()}>checked="checked"<{/if}>/> 許可する</label>
                    <label><input type="radio" name="plan_edit_flg" value="f" <{if !$ward->plan_edit_flg()}>checked="checked"<{/if}>/> 許可しない</label>
                    <{if $err.plan_edit_flg}>
                        <p class="err"><{$err.plan_edit_flg}></p>
                    <{/if}>
                </td>
            </tr>

            <tr>
                <td>職員による実績の変更</td>
                <td>
                    <label><input type="radio" name="result_edit_flg" value="t" <{if $ward->result_edit_flg()}>checked="checked"<{/if}>/> 許可する</label>
                    <label><input type="radio" name="result_edit_flg" value="f" <{if !$ward->result_edit_flg()}>checked="checked"<{/if}>/> 許可しない</label>
                    <{if $err.result_edit_flg}>
                        <p class="err"><{$err.result_edit_flg}></p>
                    <{/if}>
                </td>
            </tr>

        </table>

        <button type="submit">登録する</button>

    </form>

</div>
<hr />

<{capture name="js"}>
    <script type="text/javascript" src="js/shift/common.js"></script>
    <script type="text/javascript" src="js/shift/adm_ward_form.js"></script>
<{/capture}>
<{include file="shift/footer.tpl"}>
