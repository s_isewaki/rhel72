<{include file="shift/header.tpl" title="個人勤務表" tab="10"}>

<{if ! $staff->group_id()}>
    <div class="attention">あなたが所属している部署がありません。</div>
<{else}>

    <script type="text/javascript">
        <!--
        var base_href = '<{$BASE_HREF}>/';
        var shift_pattern = '<{pattern_pulldown array=$pattern_list}>';
        // -->
    </script>

    <div id="main">
        <{if $view_mode === 0}>
            <div class="pulldown"><b><{$ward->group_name()|escape}></b>:</div>

            <!-- 月・クール切り替えタブ -->
            <div id="month">
                <ul>
                    <li><a href="shift/personal.php?y=<{$lastyear}>&m=<{$month}>&mode=month"><span>&lt;前年</span></a></li>
                        <{foreach from=$yearmonth item=row}>
                        <li <{if $row.i === 0}>class="selected"<{/if}>><a href="shift/personal.php?y=<{$row.year}>&m=<{$row.month}>&mode=month"><span><{$row.vyear}>/<{$row.vmonth}></span></a></li>
                        <{/foreach}>
                    <li><a href="shift/personal.php?y=<{$nextyear}>&m=<{$month}>&mode=month"><span>翌年&gt;</span></a></li>
                </ul>
            </div>
            <div class="clear"></div>
        <{else}>
            <div>
                <{if $view_mode === '1'}>
                    <a href="shift/results.php?y=<{$year}>&amp;m=<{$month}>&group_id=<{$ward->group_id()|escape}>&amp;mode=month">[勤務実績に戻る]</a>
                <{else}>
                    <a href="shift/results_approve.php?id=<{$approve_id|escape}>">[勤務実績に戻る]</a>
                <{/if}>
            </div>
        <{/if}>

        <div style="margin:10px 0;">
            <{if $ward->month_type() === '2' && $view_mode === 0}>
                <button type="button" class="btn_view" data-y="<{$year}>" data-m="<{$month}>" data-mode="cours">クール表示</button>
            <{/if}>
            <b><{$staff->emp_name()}></b> (<{$staff->class_full_name()}>)
        </div>

        <form id="form-personal" action="shift/personal.php" method="post">
            <input type="hidden" name="mode" value="confirm" />
            <input type="hidden" name="y" value="<{$year|escape}>" />
            <input type="hidden" name="m" value="<{$month|escape}>" />
            <input type="hidden" name="view_emp_id" value="<{$view_emp_id|escape}>" />
            <input type="hidden" name="view_mode" value="<{$view_mode|escape}>" />
            <input type="hidden" name="approve_id" value="<{$approve_id|escape}>" />
            <{if $personal_hope_use && $hope_limit && $view_mode === 0}><button type="submit">勤務希望を登録する</button><{/if}>
            <table id="personal" class="list">
                <thead>
                    <tr>
                        <td rowspan="2">日付</td>
                        <td rowspan="2">曜日</td>
                        <td rowspan="2">所属</td>
                        <td colspan="2">勤務予定</td>
                        <{if $view_mode !== 0}>
                            <td colspan="6">勤務実績</td>
                        <{else}>
                            <td colspan="3">勤務実績</td>
                        <{/if}>
                        <td colspan="6">残業</td>

                        <{if $view_mode !== 0}>
                            <td rowspan="2">100<hr/>100</td>
                            <td rowspan="2">125<hr/>100</td>
                            <td rowspan="2">135<hr/>100</td>
                            <td rowspan="2">150<hr/>100</td>
                            <td rowspan="2">25<hr/>100</td>

                        <{/if}>

                        <{if $is_shorter}><td rowspan="2">時短</td><{/if}>

                        <td rowspan="2">備考</td>

                        <{if $personal_hope_use}><td rowspan="2">勤務希望<{if $hope_limit}><br />(締切:<{$hope_limit_day|regex_replace:'/\d\d\d\d\-(\d\d)\-(\d\d)/':'$1/$2'|escape}>)<{/if}></td><{/if}>
                    </tr>
                    <tr>
                        <td>予定</td>
                        <td>当直</td>
                        <td>実績</td>
                        <td>当直</td>
                        <{if $view_mode !== 0}>
                            <td>出勤</td>
                            <td>退勤</td>
                            <td>(休憩)</td>
                        <{/if}>
                        <td>勤務</td>
                        <td>開始</td>
                        <td>終了</td>
                        <td>(休憩)</td>
                        <td>ｶﾝﾌｧ</td>
                        <td>小計</td>
                        <td>理由</td>
                    </tr>
                </thead>

                <tbody>
                    <{foreach from=$lists item=row}>
                        <tr class="ty<{$row.type}> <{if ($staff->join_date() && $staff->join_date() > $row.date) || ($staff->retire_date() && $row.date > $staff->retire_date())}>retire<{/if}>">
                            <td id="<{$row.date|escape}>">
                                <{if ($view_mode === 0 && !$approved[0] && (($ward->result_edit_flg() && $row.date <= $today) || ($row.results && $row.date >= $zan_limit) || ($staff->is_shorter($row.date) && $row.date <= $today)))}>
                                    <span class="editor js-edit-result" data-date="<{$row.date|escape}>"><{$row.date|regex_replace:'/^\d\d\d\d(\d\d)(\d\d)/':'$1/$2'|escape}></span>
                                <{else}>
                                    <{$row.date|regex_replace:'/^\d\d\d\d(\d\d)(\d\d)/':'$1/$2'|escape}>
                                <{/if}>
                            </td>
                            <td class="week"><{$wj[$row.week]|escape}></td>
                            <td class="<{$row.assist|escape}>"><{if $row.assist_group_name}><{$row.assist_group_name}><{else}><{$ward->group_name()|escape}><{/if}></td>
                            <td class="sign">
                                <{if $view_mode === 0 && !$approved[0] && $ward->plan_edit_flg() && empty($row.results) && ($row.date > $today)}>
                                    <span class="editor js-edit-plan" data-date="<{$row.date|escape}>"><{$row.plan|default:'*'|escape}></span>
                                <{else}>
                                    <{$row.plan|escape}>
                                <{/if}>
                            </td>
                            <td class="duty"><{if $row.plan_duty}>●<{/if}></td>
                            <td class="sign"><{$row.results|escape}></td>
                            <td class="duty"><{if $row.results_duty}>●<{/if}></td>
                            <{if $view_mode !== 0}>
                                <td class="time"><{$row.start_time|regex_replace:'/(\d\d)(\d\d)/':'$1:$2'|escape}></td>
                                <td class="time"><{$row.end_time|regex_replace:'/(\d\d)(\d\d)/':'$1:$2'|escape}></td>
                                <td class="time"><{if $row.start_time}>(<{$row.resttime|min2time}>)<{/if}></td>
                            <{/if}>
                            <td class="time"><{if $row.start_time}><{$row.worktime|min2time}><{/if}></td>

                            <td class="time">
                                <{foreach from=$row.ovtm_start_time item=ovtm_start_time}>
                                    <{$ovtm_start_time|regex_replace:'/(\d\d)(\d\d)/':'$1:$2'|escape}><br>
                                <{/foreach}>
                            </td>
                            <td class="time">
                                <{foreach from=$row.ovtm_end_time item=ovtm_end_time}>
                                    <{$ovtm_end_time|regex_replace:'/(\d\d)(\d\d)/':'$1:$2'|escape}><br>
                                <{/foreach}>
                            </td>
                            <td class="time"><{if $row.over_resttime}>(<{$row.over_resttime|min2time}>)<{/if}></td>
                            <td class="time"><{if $row.conference_minutes}><{$row.conference_minutes|min2time}><{/if}></td>
                            <td class="time"><{if $row.overtime2}><{$row.overtime2|min2time}><{/if}></td>
                            <td>
                                <{foreach from=$row.ovtm_reason item=ovtm_reason}>
                                    <{$ovtm_reason|escape}><br>
                                <{/foreach}>
                            </td>

                            <{if $view_mode !== 0}>
                                <td class="time"><{if $row.over100}><{$row.over100|min2time}><{/if}></td>
                                <td class="time"><{if $row.over125}><{$row.over125|min2time}><{/if}></td>
                                <td class="time"><{if $row.over135}><{$row.over135|min2time}><{/if}></td>
                                <td class="time"><{if $row.over150}><{$row.over150|min2time}><{/if}></td>
                                <td class="time"><{if $row.over25}><{$row.over25|min2time}><{/if}></td>
                            <{/if}>

                            <{if $is_shorter}><td class="time"><{$row.base_shorter_minutes|escape}></td><{/if}>

                            <td width="150"><{$row.memo|escape}></td>

                            <{if $personal_hope_use}>
                                <td class="sign">
                                    <{if $hope_limit}>
                                        <select name="hope[<{$row.date|escape}>]">
                                            <option value=""></option>
                                            <{pattern_pulldown array=$pattern_list ptn=$row.hope}>
                                        </select>
                                    <{else}>
                                        <{$row.hope|escape}>
                                    <{/if}>
                                </td>
                            <{/if}>
                        </tr>
                    <{/foreach}>
                    <tr>
                        <td colspan="3">合計</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <{if $view_mode !== 0}>
                            <td></td>
                            <td></td>
                            <td class="time"><{$total.resttime|min2time}></td>
                        <{/if}>
                        <td class="time"><{$total.worktime|min2time}></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="time"><{$total.overtime2|min2time}></td>
                        <td></td>

                        <{if $view_mode !== 0}>
                            <td class="time"><{$total.over100|min2time}></td>
                            <td class="time"><{$total.over125|min2time}></td>
                            <td class="time"><{$total.over135|min2time}></td>
                            <td class="time"><{$total.over150|min2time}></td>
                            <td class="time"><{$total.over25|min2time}></td>
                        <{/if}>

                        <{if $is_shorter}><td class="time"><{$total.base_shorter_minutes|escape}></td><{/if}>
                        <{if $personal_hope_use}><td></td><{/if}>
                    </tr>
                </tbody>
            </table>

            <table id="holiday">
                <tr>
                    <td></td>
                    <td class="head">繰越</td>
                    <td class="head">付与</td>
                    <td class="head">取得</td>
                    <td class="head">残</td>
                </tr>
                <tr>
                    <td class="head">有休</td>
                    <td><{$holiday.holiday.kuri|escape}></td>
                    <td><{$holiday.holiday.fuyo|escape}></td>
                    <td><{$holiday.holiday.use|escape}></td>
                    <td><{$holiday.holiday.zan|escape}></td>
                </tr>
                <tr>
                    <td class="head">夏休</td>
                    <td></td>
                    <td><{$holiday.summer.fuyo|escape}></td>
                    <td><{$holiday.summer.use|escape}></td>
                    <td><{$holiday.summer.zan|escape}></td>
                </tr>
                <tr>
                    <td class="head">週休</td>
                    <td></td>
                    <td><{$holiday.week.fuyo|escape}></td>
                    <td><{$holiday.week.use|escape}></td>
                    <td><{$holiday.week.zan|escape}></td>
                </tr>
            </table>

            <table class="list" style="margin-top:5px;">
                <tr>
                    <td>-</td>
                    <td align="center">承認者</td>
                    <td align="center">承認日時</td>
                </tr>
                <tr>
                    <td align="center">本人</td>
                    <td><{if $approved[0]}><{$approved[0].approval_name|escape}><{else}><button type="submit" name="submit_approval" value="1" <{if !$is_month_approval}>disabled="disabled"<{/if}>>月次確定</button><{/if}></td>
                    <td><{if $approved[0]}><{$approved[0].approval_date|date_format_jp:"%Y年%-m月%-d日(%a) %R"}><{/if}></td>
                </tr>
                <tr>
                    <td align="center">所属長</td>
                    <td><{if $approved[1]}><{$approved[1].approval_name|escape}><{elseif $view_mode === '1'}><button type="submit" name="submit_manager_approval" value="1" <{if !$approved[0]}>disabled="disabled"<{/if}>>月次確定</button><{else}>未承認<{/if}></td>
                    <td><{if $approved[1]}><{$approved[1].approval_date|date_format_jp:"%Y年%-m月%-d日(%a) %R"}><{else}>未承認<{/if}></td>
                </tr>
                <tr>
                    <td align="center">部門長</td>
                    <td><{if $approved[2]}><{$approved[2].approval_name|escape}><{elseif $view_mode === '999'}><button type="submit" name="submit_boss_approval" value="1" <{if !$approved[1]}>disabled="disabled"<{/if}>>月次確定</button><{else}>未承認<{/if}></td>
                    <td><{if $approved[2]}><{$approved[2].approval_date|date_format_jp:"%Y年%-m月%-d日(%a) %R"}><{else}>未承認<{/if}></td>
                </tr>
            </table>

            <{if $personal_hope_use && $hope_limit && $view_mode === 0}>
                <div>
                    <button type="submit" name="submit_hope" value="1">勤務希望を登録する</button>
                </div>
            <{/if}>

        </form>

    </div>

    <{capture name="js"}>
        <script type="text/javascript" src="js/shift/common.js"></script>
        <script type="text/javascript" src="js/shift/personal.js"></script>
    <{/capture}>
<{/if}>
<{include file="shift/footer.tpl"}>
