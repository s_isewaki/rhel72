<{include file="__header.tpl" title="��̳���ӽ���" prefix='shift'}>

<script type="text/javascript">
    <!--
    var e = 'e<{$emp_id|escape:'javascript'}>';
    var d = 'd<{$date|escape:'javascript'}>';
    var window_close = false;
    // -->
</script>

<div class="winheader">
    <span class="title">��̳���ӽ���</span>
    <span class="close"><img src="img/icon/close.gif" alt="�Ĥ���" width="24" height="24" border="0" onclick="window.close();" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';" /></span>
</div>

<div id="main">
    <form id="edit_form" action="shift/personal_edit_result.php" method="post">
        <input type="hidden" name="mode" value="confirm" />
        <input type="hidden" name="emp_id" value="<{$emp_id|escape}>" />
        <input type="hidden" name="date" value="<{$date|escape}>" />
        <input type="hidden" name="is_result_edit" value="<{$is_result_edit|escape}>" />
        <input type="hidden" name="is_shorter" value="<{$is_shorter|escape}>" />
        <input type="hidden" name="is_overtime" value="<{$is_overtime|escape}>" />
        <input type="hidden" name="night_duty" value="" />

        <{if $is_overtime_warn}>
            <div class="attention"><{$warning_message|escape}></div>
        <{/if}>

        <table class="form">

            <tr>
                <td class="header">��̾</td>
                <td><{$emp.name|escape}></td>
            </tr>


            <tr>
                <td class="header">����</td>
                <td><{$date|regex_replace:'/(\d\d\d\d)(\d\d)(\d\d)/':'$1-$2-$3'|date_format_jp:"%Yǯ%-m��%-d��(%a)"}></td>
            </tr>

            <{if $is_result_edit}>
                <tr>
                    <td class="header">���롼��</td>
                    <td>
                        <select id="tmcd_group_id" name="tmcd_group_id">
                            <{foreach from=$pgroup_lists item=row}>
                                <option value="<{$row->group_id()|escape}>" <{if $row->group_id()==$results.tmcd_group_id}>selected="selected"<{/if}>><{$row->group_name()|escape}></option>
                            <{/foreach}>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td class="header">��̳����</td>
                    <td>
                        <select id="ptn" name="ptn"><option value=""></option><{pattern_pulldown array=$pattern_list ptn=$results.pattern reason=$results.reason}></select>

                        <{if $err.ptn}>
                            <p class="err"><{$err.ptn}></p>
                        <{/if}>
                    </td>
                </tr>

                <tr>
                    <td class="header">�����</td>
                    <td>
                        <select id="start_hour" name="start_hour">
                            <option value=""></option>
                            <{foreach from=$hours24 key=value item=hour}>
                                <option value="<{$value|string_format:"%02d"|escape}>" <{if $results.start_hour != '' && $value==$results.start_hour}>selected="selected"<{/if}>><{$hour|escape}></option>
                            <{/foreach}>
                        </select>��
                        <select id="start_min" name="start_min">
                            <option value=""></option>
                            <{foreach from=$fifteen_minutes item=m}>
                                <option value="<{$m|string_format:"%02d"|escape}>"><{$m|string_format:"%02d"|escape}></option>
                            <{/foreach}>
                            <{foreach from=$minutes item=m}>
                                <option value="<{$m|string_format:"%02d"|escape}>" <{if $results.start_min != '' && $m==$results.start_min}>selected="selected"<{/if}>><{$m|string_format:"%02d"|escape}></option>
                            <{/foreach}>
                        </select>
                        <input type="hidden" name="previous_day_flag" value="0"/>
                        ��
                        <select id="end_hour" name="end_hour">
                            <option value=""></option>
                            <{foreach from=$hours48 key=value item=hour}>
                                <option value="<{$value|string_format:"%02d"|escape}>" <{if $results.end_hour != '' && $value==$results.end_hour}>selected="selected"<{/if}>><{$hour|escape}></option>
                            <{/foreach}>
                        </select>��
                        <select id="end_min" name="end_min">
                            <option value=""></option>
                            <{foreach from=$fifteen_minutes item=m}>
                                <option value="<{$m|string_format:"%02d"|escape}>"><{$m|string_format:"%02d"|escape}></option>
                            <{/foreach}>
                            <{foreach from=$minutes item=m}>
                                <option value="<{$m|string_format:"%02d"|escape}>" <{if $results.end_min != '' && $m==$results.end_min}>selected="selected"<{/if}>><{$m|string_format:"%02d"|escape}></option>
                            <{/foreach}>
                        </select>

                        <{if $err.work_time}>
                            <p class="err"><{$err.work_time}></p>
                        <{/if}>
                    </td>
                </tr>

                <tr>
                    <td class="header">�ٷ�</td>
                    <td>
                        <select id="rest_start_hour" name="rest_start_hour">
                            <option value=""></option>
                            <{foreach from=$hours24 key=value item=hour}>
                                <option value="<{$value|string_format:"%02d"|escape}>" <{if $results.rest_start_hour != '' && $value==$results.rest_start_hour}>selected="selected"<{/if}>><{$hour|escape}></option>
                            <{/foreach}>
                        </select>��
                        <select id="rest_start_min" name="rest_start_min">
                            <option value=""></option>
                            <{foreach from=$fifteen_minutes item=m}>
                                <option value="<{$m|string_format:"%02d"|escape}>"><{$m|string_format:"%02d"|escape}></option>
                            <{/foreach}>
                            <{foreach from=$minutes item=m}>
                                <option value="<{$m|string_format:"%02d"|escape}>" <{if $results.rest_start_min != '' && $m==$results.rest_start_min}>selected="selected"<{/if}>><{$m|string_format:"%02d"|escape}></option>
                            <{/foreach}>
                        </select>
                        ��
                        <select id="rest_end_hour" name="rest_end_hour">
                            <option value=""></option>
                            <{foreach from=$hours24 key=value item=hour}>
                                <option value="<{$value|string_format:"%02d"|escape}>" <{if $results.rest_end_hour != '' && $value==$results.rest_end_hour}>selected="selected"<{/if}>><{$hour|escape}></option>
                            <{/foreach}>
                        </select>��
                        <select id="rest_end_min" name="rest_end_min">
                            <option value=""></option>
                            <{foreach from=$fifteen_minutes item=m}>
                                <option value="<{$m|string_format:"%02d"|escape}>"><{$m|string_format:"%02d"|escape}></option>
                            <{/foreach}>
                            <{foreach from=$minutes item=m}>
                                <option value="<{$m|string_format:"%02d"|escape}>" <{if $results.rest_end_min != '' && $m==$results.rest_end_min}>selected="selected"<{/if}>><{$m|string_format:"%02d"|escape}></option>
                            <{/foreach}>
                        </select>

                        <{if $err.rest_time}>
                            <p class="err"><{$err.rest_time}></p>
                        <{/if}>
                    </td>
                </tr>

                <tr>
                    <td class="header">����ե���󥹻���</td>
                    <td>
                        <input type="text" name="conference_time" value="<{$results.conference_time|escape}>" size="4" maxlength="4" class="num" /> ����

                        <{if $err.conference_time}>
                            <p class="err"><{$err.conference_time}></p>
                        <{/if}>
                    </td>
                </tr>
            <{else}>
                <tr>
                    <td class="header">���롼��</td>
                    <td>
                        <{$results.group_name|escape}>
                        <input type="hidden" name="tmcd_group_id" value="<{$results.tmcd_group_id|escape}>" />
                    </td>
                </tr>

                <tr>
                    <td class="header">��̳����</td>
                    <td>
                        <{$results.pattern_name|escape}>
                        <input type="hidden" name="ptn" value="<{$results.ptn|escape}>" />
                    </td>
                </tr>

                <tr>
                    <td class="header">��ľ</td>
                    <td>
                        <{if $results.night_duty==="1"}>ͭ��<{/if}>
                        <{if $results.night_duty!=="1"}>̵��<{/if}>
                        <input type="hidden" name="night_duty" value="<{$results.night_duty|escape}>" />
                    </td>
                </tr>

                <tr>
                    <td class="header">�����(�ٷ�)</td>
                    <td>
                        <{if $results.start_time}>
                            <{$results.start_time|regex_replace:'/(\d\d)(\d\d)/':'$1:$2'|escape}>
                            ��
                            <{if $results.next_day_flag}>��<{/if}>
                            <{$results.end_time|regex_replace:'/(\d\d)(\d\d)/':'$1:$2'|escape}>
                            <{if $results.rest_start_time}>
                                (
                                <{$results.rest_start_time|regex_replace:'/(\d\d)(\d\d)/':'$1:$2'|escape}>
                                ��
                                <{$results.rest_end_time|regex_replace:'/(\d\d)(\d\d)/':'$1:$2'|escape}>
                                )
                            <{/if}>
                        <{/if}>
                        <input type="hidden" id="start_time" name="start_time" value="<{$results.start_time|escape}>" />
                        <input type="hidden" id="end_time" name="end_time" value="<{$results.end_time|escape}>" />
                        <input type="hidden" id="rest_start_time" name="rest_start_time" value="<{$results.rest_start_time|escape}>" />
                        <input type="hidden" id="rest_end_time" name="rest_end_time" value="<{$results.rest_end_time|escape}>" />
                        <input type="hidden" id="next_day_flag" name="next_day_flag" value="<{$results.next_day_flag|escape}>" />
                    </td>
                </tr>
            <{/if}>

            <{if $is_shorter}>
                <tr>
                    <td class="header">��û</td>
                    <td>
                        <label><input type="radio" name="base_shorter_minutes" value="" <{if empty($results.base_shorter_minutes)}>checked="checked"<{/if}> />̵��</label>&nbsp;&nbsp;&nbsp;
                        <label><input type="radio" name="base_shorter_minutes" value="30" <{if $results.base_shorter_minutes==="30"}>checked="checked"<{/if}> />30ʬ</label>&nbsp;&nbsp;&nbsp;
                        <label><input type="radio" name="base_shorter_minutes" value="60" <{if $results.base_shorter_minutes==="60"}>checked="checked"<{/if}> />60ʬ</label>&nbsp;&nbsp;&nbsp;
                        <label><input type="radio" name="base_shorter_minutes" value="90" <{if $results.base_shorter_minutes==="90"}>checked="checked"<{/if}> />90ʬ</label>

                        <{if $err.base_shorter_minutes}>
                            <p class="err"><{$err.base_shorter_minutes}></p>
                        <{/if}>
                    </td>
                </tr>
            <{/if}>

            <{if $is_overtime}>
                <{section name=i start=1 loop=6}>
                    <{assign var=i1 value=$smarty.section.i.index}>
                    <{assign var=i2 value=$smarty.section.i.index_next}>
                    <{assign var=i0 value=$smarty.section.i.index_prev}>
                    <tr <{if $i1 >= 2}>class="js-overtime2345"<{/if}> id="overtime<{$i1}>">
                        <td class="header">�ĶȻ���<{$i1}></td>
                        <td>
                            <select class="js-group<{$i1}> js-overtime js-reset<{$i1}> shh" data-reason="#reason<{$i1}>" data-next="#overtime<{$i2}>" data-group=".js-group<{$i1}>" name="over[<{$i0}>][over_start_hour]">
                                <option value=""></option>
                                <{foreach from=$hours48 key=value item=hour}>
                                    <option value="<{$value|string_format:"%02d"|escape}>" <{if $results.over[$i0].over_start_hour != '' && $value==$results.over[$i0].over_start_hour}>selected="selected"<{/if}>><{$hour|escape}></option>
                                <{/foreach}>
                            </select>��
                            <select class="js-group<{$i1}> js-overtime js-reset<{$i1}> smm" data-reason="#reason<{$i1}>" data-next="#overtime<{$i2}>" data-group=".js-group<{$i1}>" name="over[<{$i0}>][over_start_min]">
                                <option value=""></option>
                                <{foreach from=$fifteen_minutes item=m}>
                                    <option value="<{$m|string_format:"%02d"|escape}>"><{$m|string_format:"%02d"|escape}></option>
                                <{/foreach}>
                                <{foreach from=$minutes item=m}>
                                    <option value="<{$m|string_format:"%02d"|escape}>" <{if $results.over[$i0].over_start_min != '' && $m==$results.over[$i0].over_start_min}>selected="selected"<{/if}>><{$m|string_format:"%02d"|escape}></option>
                                <{/foreach}>
                            </select>
                            ��
                            <select class="js-group<{$i1}> js-overtime js-reset<{$i1}> ehh" data-reason="#reason<{$i1}>" data-next="#overtime<{$i2}>" data-group=".js-group<{$i1}>" name="over[<{$i0}>][over_end_hour]">
                                <option value=""></option>
                                <{foreach from=$hours48 key=value item=hour}>
                                    <option value="<{$value|string_format:"%02d"|escape}>" <{if $results.over[$i0].over_end_hour != '' && $value==$results.over[$i0].over_end_hour}>selected="selected"<{/if}>><{$hour|escape}></option>
                                <{/foreach}>
                            </select>��
                            <select class="js-group<{$i1}> js-overtime js-reset<{$i1}> emm" data-reason="#reason<{$i1}>" data-next="#overtime<{$i2}>" data-group=".js-group<{$i1}>" name="over[<{$i0}>][over_end_min]">
                                <option value=""></option>
                                <{foreach from=$fifteen_minutes item=m}>
                                    <option value="<{$m|string_format:"%02d"|escape}>"><{$m|string_format:"%02d"|escape}></option>
                                <{/foreach}>
                                <{foreach from=$minutes item=m}>
                                    <option value="<{$m|string_format:"%02d"|escape}>" <{if $results.over[$i0].over_end_min != '' && $m==$results.over[$i0].over_end_min}>selected="selected"<{/if}>><{$m|string_format:"%02d"|escape}></option>
                                <{/foreach}>
                            </select>
                            <button type="button" class="js-overtime-reset" data-reset=".js-reset<{$i1}>">�ꥻ�å�</button>
                            <br>
                            (�ٷơ�
                            <select class="js-overtime js-reset<{$i1}>" name="over[<{$i0}>][rest_over_start_hour]">
                                <option value=""></option>
                                <{foreach from=$hours48 key=value item=hour}>
                                    <option value="<{$value|string_format:"%02d"|escape}>" <{if $results.over[$i0].rest_over_start_hour != '' && $value==$results.over[$i0].rest_over_start_hour}>selected="selected"<{/if}>><{$hour|escape}></option>
                                <{/foreach}>
                            </select>��
                            <select class="js-overtime js-reset<{$i1}>" name="over[<{$i0}>][rest_over_start_min]">
                                <option value=""></option>
                                <{foreach from=$fifteen_minutes item=m}>
                                    <option value="<{$m|string_format:"%02d"|escape}>"><{$m|string_format:"%02d"|escape}></option>
                                <{/foreach}>
                                <{foreach from=$minutes item=m}>
                                    <option value="<{$m|string_format:"%02d"|escape}>" <{if $results.over[$i0].rest_over_start_min != '' && $m==$results.over[$i0].rest_over_start_min}>selected="selected"<{/if}>><{$m|string_format:"%02d"|escape}></option>
                                <{/foreach}>
                            </select>
                            ��
                            <select class="js-overtime js-reset<{$i1}>" name="over[<{$i0}>][rest_over_end_hour]">
                                <option value=""></option>
                                <{foreach from=$hours48 key=value item=hour}>
                                    <option value="<{$value|string_format:"%02d"|escape}>" <{if $results.over[$i0].rest_over_end_hour != '' && $value==$results.over[$i0].rest_over_end_hour}>selected="selected"<{/if}>><{$hour|escape}></option>
                                <{/foreach}>
                            </select>��
                            <select class="js-overtime js-reset<{$i1}>" name="over[<{$i0}>][rest_over_end_min]">
                                <option value=""></option>
                                <{foreach from=$fifteen_minutes item=m}>
                                    <option value="<{$m|string_format:"%02d"|escape}>"><{$m|string_format:"%02d"|escape}></option>
                                <{/foreach}>
                                <{foreach from=$minutes item=m}>
                                    <option value="<{$m|string_format:"%02d"|escape}>" <{if $results.over[$i0].rest_over_end_min != '' && $m==$results.over[$i0].rest_over_end_min}>selected="selected"<{/if}>><{$m|string_format:"%02d"|escape}></option>
                                <{/foreach}>
                            </select>
                            )

                            <{if $err.over_time[$i0]}>
                                <p class="err"><{$err.over_time[$i0]}></p>
                            <{/if}>

                            <div class="js-overtime-reason" id="reason<{$i1}>" style="margin-top:5px;">
                                ��ͳ��
                                <select class="js-reason" name="over[<{$i0}>][ovtm_reason_id]" data-text="#reason_text<{$i1}>">
                                    <{foreach from=$ovtm_reason_lists item=row}>
                                        <option value="<{$row.reason_id|escape}>" <{if $results.over[$i0].ovtm_reason_id !== '' && $row.reason_id==$results.over[$i0].ovtm_reason_id}>selected="selected"<{/if}>><{$row.reason|escape}></option>
                                    <{/foreach}>
                                    <option value="" <{if !$results.over[$i0].ovtm_reason_id}>selected="selected"<{/if}>>����¾</option>
                                </select><br />
                                <input type="hidden" class="js-hidden" name="over[<{$i0}>][ovtm_reason_name]" value="">
                                <div class="js-overtime-reason-text" id="reason_text<{$i1}>" style="margin-top:5px;">
                                    <input type="text" name="over[<{$i0}>][ovtm_reason]" value="<{$results.over[$i0].ovtm_reason|escape}>" size="50" maxlength="100" class="jpn">
                                </div>

                                <{if $err.reason[$i0]}>
                                    <p class="err"><{$err.reason[$i0]}></p>
                                <{/if}>
                            </div>
                        </td>
                    </tr>
                <{/section}>
            <{/if}>

            <tr>
                <td class="header">����</td>
                <td>
                    <textarea name="memo" rows="2" cols="30"><{$results.memo|escape}></textarea>
                </td>
            </tr>

            <tr>
                <td class="header">�ǽ���������</td>
                <td><{if $results.update_time}><{$results.update_time|regex_replace:'/^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)/':'$1-$2-$3 $4:$5:$6'|escape}><{/if}>
                </td>
            </tr>

            <tr>
                <td class="header">�ǽ�������</td>
                <td><{$update_emp_id->emp_name()|escape}>
                </td>
            </tr>

        </table>

        <div align="center">
            <button type="submit">��Ͽ����</button>
            <button type="button" onclick="window.close();">������ɥ����Ĥ���</button>
        </div>

    </form>
</div>

<{capture name="js"}>
    <script type="text/javascript" src="js/shift/common.js"></script>
    <script type="text/javascript" src="js/shift/personal_edit_result.js"></script>
<{/capture}>
<{include file="shift/footer.tpl"}>
