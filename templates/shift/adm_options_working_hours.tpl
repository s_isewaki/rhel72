<{include file="shift/header.tpl" title="管理画面：オプション" tab="08"}>

<{include file="shift/adm_options_submenu.tpl"}>

<div id="main">
    <form id="cours" action="shift/adm_options_working_hours.php" method="post">
        <input type="hidden" name="mode" value="confirm" />
        <input type="hidden" name="y" value="<{$nowyear|escape}>" />

        <{if $err}>
            <div class="err">
            <{if $err.mes.working_hours}><{foreach from=$err.mes.working_hours item=mes}><{$mes}><br /><{/foreach}><{/if}>
        <{if $err.mes.holiday_count}><{foreach from=$err.mes.holiday_count item=mes}><{$mes}><br /><{/foreach}><{/if}>
    </div>
<{/if}>

<p>
    <a href="shift/adm_options_working_hours.php?y=<{$lastyear}>">&lt;&lt;</a>
    <b><{$nowyear|escape}>年</b>
    <a href="shift/adm_options_working_hours.php?y=<{$nextyear}>">&gt;&gt;</a>
</p>

<table class="list">

    <col>
    <col>
    <col>
    <col>

    <thead>
        <tr>
            <td>クール</td>
            <td>期間</td>
            <td>要稼働時間<br />(0.25時間単位)</td>
            <td>週休日数</td>
        </tr>
    </thead>

    <tbody>
        <{foreach from=$lists item=row}>
            <{assign var=cours value=$row.cours}>
            <tr>
                <td><{$row.view[0]|escape}>/<{$row.view[1]|escape}></td>
                <td><{$row.start_date|escape}> 〜 <{$row.end_date|escape}></td>
                <td <{if $err.working_hours.$cours}>class="err"<{/if}>><label><input type="number" name="working_hours[<{$row.cours|escape}>]" size="7" maxlength="6" value="<{$row.working_hours|escape}>" class="num" />時間</label></td>
                <td <{if $err.holiday_count.$cours}>class="err"<{/if}>><label><input type="number" name="holiday_count[<{$row.cours|escape}>]" size="3" maxlength="2" value="<{$row.holiday_count|escape}>" class="num" />日</label></td>
            </tr>
        <{/foreach}>
    <tbody>
</table>

<button type="submit" class="confirm">登録する</button>
</form>
</div>

<{capture name="js"}>
    <script type="text/javascript" src="js/shift/adm_options.js"></script>
<{/capture}>
<{include file="shift/footer.tpl"}>
