<{include file="shift/header.tpl" title="管理画面：オプション" tab="08"}>

<{include file="shift/adm_options_submenu.tpl"}>

<div id="main">
    <fieldset>
        <legend>電子カルテ連携</legend>
        <form action="shift/adm_options_maintenance.php" method="post">
            <input type="hidden" name="mode" value="renkei" />
            <button type="submit" class="confirm">連携プログラムを実行する<br />(完了するまでに時間がかかります)</button>
                <{if $exec}>
                <pre class="command"><{$exec|escape}></pre>
            <{/if}>
        </form>
    </fieldset>

</div>

<{capture name="js"}>
    <script type="text/javascript" src="js/shift/adm_options.js"></script>
<{/capture}>
<{include file="shift/footer.tpl"}>
