<{include file="shift/header.tpl" title="当直表" tab="13"}>

<{if ! $ward_lists}>
    <div class="attention">利用できません。</div>
<{else}>
    <style type="text/css">
        <!--
        html {
            overflow: auto;
            overflow-y: hidden;
        }
        // -->
    </style>

    <script type="text/javascript">
        <!--
        var this_group_id = '<{$this_ward->group_id()}>';
        var this_y = '<{$year}>';
        var this_m = '<{$month}>';
        var pattern = <{$pattern_json}>;
        var display = <{$this_ward->display()}>;
        var data = <{$table->table_json()}>;
        var fscreen = <{if $fullscreen}>true<{else}>false<{/if}>;
            // -->
    </script>

    <div id="loading"></div>

    <div id="shift">
        <div id="wrapHead">
            <{if !$fullscreen}>
                <!-- 部署プルダウン -->
                <form name="shift/duty.php" method="get">
                    <input type="hidden" id="this_y" name="y" value="<{$year}>" />
                    <input type="hidden" id="this_m" name="m" value="<{$month}>" />
                    <div class="pulldown">
                        <b>部署</b>：
                        <select id="this_group_id" name="group_id" onchange="submit(this.form)">
                            <{foreach from=$ward_lists item=row}>
                                <option value="<{$row->group_id()|escape}>" <{if $row->group_id() === $this_ward->group_id()}>selected<{/if}>><{$row->group_name()|escape}></option>
                            <{/foreach}>
                        </select>
                    </div>
                </form>

                <!-- 月・クール切り替えタブ -->
                <div id="month">
                    <ul>
                        <li><a href="shift/duty.php?y=<{$lastyear}>&m=<{$month}>&group_id=<{$this_ward->group_id()|escape}>"><span>&lt;前年</span></a></li>
                            <{foreach from=$yearmonth item=row}>
                            <li <{if $row.i === 0}>class="selected"<{/if}>><a href="shift/duty.php?y=<{$row.year}>&m=<{$row.month}>&group_id=<{$this_ward->group_id()|escape}>"><span><{$row.vyear}>/<{$row.vmonth}></span></a></li>
                            <{/foreach}>
                        <li><a href="shift/duty.php?y=<{$nextyear}>&m=<{$month}>&group_id=<{$this_ward->group_id()|escape}>"><span>翌年&gt;</span></a></li>
                    </ul>
                </div>
                <div class="clear"></div>
            <{/if}>

            <!-- ボタン -->
            <div id="shiftmenu">
                <ul id="shiftmenu_icon">
                    <li id="icon_duty"><img src="img/ico-illust-touroku.gif" alt="登録" width="36" height="36" border="0" /><br />登録</li>
                    <!-- li id="icon_draft"><img src="img/ico-illust-shitagaki.gif" alt="下書き" width="36" height="36" border="0" /><br />下書き</li -->
                    <!-- li><img src="img/ico-illust-insatu.gif" alt="印刷" width="36" height="36" border="0" /><br />印刷</li -->
                    <!-- li><img src="img/ico-illust-excel.gif" alt="エクセル" width="36" height="36" border="0" /><br />エクセル</li -->
                    <{if !$fullscreen}><li id="icon_fullscreen"><img src="img/ico-illust-zengamen.gif" alt="全画面" width="36" height="36" border="0" /><br />全画面</li><{/if}>
                </ul>
                <ul id="shiftmenu_button">
                    <li><button type="button" id="button_results">実績表示</button></li>
                    <li><button type="button" id="button_only">当直のみ</button></li>
                </ul>
                <ul id="shiftview_button">
                    <li><button type="button" id="button_team">チーム</button></li>
                    <li><button type="button" id="button_job">職種</button></li>
                    <li><button type="button" id="button_status">役職</button></li>
                </ul>
            </div>
            <div class="clear"></div>

            <!-- 情報 -->
            <div id="info">
                <span><{$start_date}> 〜 <{$end_date}></span>
            </div>
            <div class="clear"></div>

            <!-- 勤務表 header -->
            <table id="duty_head" class="shift">
                <thead>
                    <tr>
                        <td class="scrollbar">&nbsp;</td>
                        <td class="team">チー<br />ム</td>
                        <td class="name">氏名</td>
                        <td class="job">職種</td>
                        <td class="status">役職</td>
                        <{foreach from=$dates item=dt}>
                            <td class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}>"><{$dt.d}><br /><{$dt.wj}></td>
                            <{/foreach}>
                        <td class="total countw">合計</td>
                    </tr>
                </thead>
            </table>
        </div>
        <div id="wrapCont">
            <!-- 勤務表 -->
            <table id="duty" class="shift">
                <tfoot>
                    <tr class="total">
                        <td class="team">&nbsp;</td>
                        <td class="name">合計</td>
                        <td class="job">&nbsp;</td>
                        <td class="status">&nbsp;</td>
                        <{foreach from=$dates item=dt}>
                            <td id="count_d<{$dt.date}>" class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}>"><{$table->cell_col_count_duty($dt.date,'duty')|escape}></td>
                        <{/foreach}>
                    </tr>
                </tfoot>

                <tbody>
                    <{foreach from=$staff item=row}>
                        <tr class="e<{$row->emp_id()}> duty input">
                            <td id="e<{$row->emp_id()}>_team" class="team"><{$table->cell_emp($row->emp_id(),'team')|escape}></td>
                            <td id="e<{$row->emp_id()}>_name" class="name <{if $row->assist()}>assist<{/if}>"><{$table->cell_emp($row->emp_id(),'name')|escape}></td>
                            <td id="e<{$row->emp_id()}>_job" class="job"><{$table->cell_emp($row->emp_id(),'job_name')|escape}></td>
                            <td id="e<{$row->emp_id()}>_status" class="status"><{$table->cell_emp($row->emp_id(),'st_name')|escape}></td>
                            <{foreach from=$dates item=dt}>
                                <td id="e<{$row->emp_id()}>_d<{$dt.date}>_duty" class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}> <{if $row->is_employed($dt.date)}>cell<{else}>retire<{/if}>">
                                    <{$table->cell_sign($row->emp_id(),$dt.date,'duty')|escape}>
                                </td>
                            <{/foreach}>
                            <td id="e<{$row->emp_id()}>_count_duty" class="total count"><{$table->cell_row_count_duty($row->emp_id(),'duty')|escape}></td>
                        </tr>
                        <tr class="<{$row->emp_id()}> results_duty line2">
                            <td class="team filler">&nbsp;</td>
                            <td class="name filler">当直実績</td>
                            <td class="job filler">&nbsp;</td>
                            <td class="status filler">&nbsp;</td>
                            <{foreach from=$dates item=dt}>
                                <td id="e<{$row->emp_id()}>_d<{$dt.date}>_results_duty" class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}> <{if $row->is_employed($dt.date)}><{else}>retire<{/if}>">
                                    <{$table->cell_sign($row->emp_id(),$dt.date,'results_duty')|escape}>
                                </td>
                            <{/foreach}>
                            <td id="e<{$row->emp_id()}>_count_dutyr" class="total count"><{$table->cell_row_count_duty($row->emp_id(),'dutyr')|escape}></td>
                        </tr>
                    <{/foreach}>
                </tbody>
            </table>
        </div>
    </div>

    <{capture name="js"}>
        <script type="text/javascript" src="js/shift/common.js" charset="euc-jp"></script>
        <script type="text/javascript" src="js/shift/duty.js" charset="euc-jp"></script>
        <script type="text/javascript">
            <!--
            if (fscreen) {
                resizeIframeEx();
            }
            // -->
        </script>
    <{/capture}>
<{/if}>
<{include file="shift/footer.tpl"}>
