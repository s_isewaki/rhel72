<{foreach from=$staff item=row}>
    <tr id="<{$row->emp_id()|escape}>">
        <td class="dragHandle">
            <input type="hidden" name="order[]" value="<{$row->emp_id()|escape}>" />
            <input type="hidden" name="main_group_id[<{$row->emp_id()|escape}>]" value="<{$row->main_group_id()|escape}>" />
        </td>
        <td>
            <label><input type="radio" name="team_id[<{$row->emp_id()|escape}>]" value="" <{if ! $row->team_id()}>checked<{/if}> /> なし</label>
        </td>
        <{foreach from=$team item=t}>
            <td>
                <label><input type="radio" name="team_id[<{$row->emp_id()|escape}>]" value="<{$t.team_id|escape}>" <{if $t.team_id == $row->team_id()}>checked<{/if}>/> <{$t.team_name|escape}></label>
            </td>
        <{/foreach}>
        <td <{if $group_id != $row->main_group_id()}>class="assist"<{/if}>><{$row->emp_name()|escape}></td>
        <td class="right"><{$row->carryover()|escape}></td>
        <td><input type="text" name="adjustment[<{$row->emp_id()|escape}>]" value="<{$row->adjustment()|escape}>" size="6" class="num" /></td>
        <td><button type="button" onclick="removeStaff(this);">削除する(当月のみ)</button></td>
    </tr>
<{/foreach}>