<{include file="shift/header.tpl" title="管理画面：データ出力" tab="07"}>

<div id="submenu">
    <ul>
        <li>
            <a href="shift/adm_output.php"><span>データ出力</span></a>
        </li>
        <li>
            <a href="shift/adm_output_setting.php"><span>レイアウト設定</span></a>
        </li>
        <li>
            <a href="shift/adm_output_batch_setting.php"><span>バッチ出力</span></a>
        </li>
    </ul>
</div>

<div id="main">
    <form action="shift/adm_output_batch_setting.php" method="post">
        <input type="hidden" name="mode" value="confirm" />

        <fieldset>
            毎月
            <select name="csv_output_routinedate">
                <option value=""><{$dirname|escape}></option>
                <{foreach from=$days item=d}>
                    <option value="<{$d|escape}>" <{if $d|string_format:"%d" === $csv_output_routinedate}>selected="selected"<{/if}>><{$d|escape}></option>
                <{/foreach}>
                <option value="99">末</option>
            </select>
            日1時に実行する ※月次で出力しない場合は空欄として下さい
        </fieldset>

        <fieldset>
            当月を含む
            <input type="text" name="csv_output_month_ago" value="<{$csv_output_month_ago|escape}>" size="3" class="num" />
            ヶ月分を出力する
            <{if $err.csv_output_month_ago}><p class="err"><{$err.csv_output_month_ago}></p><{/if}>
        </fieldset>

        <fieldset>
            <input type="checkbox" name="csv_output_nextday" <{if $csv_output_nextday === 'on'}>checked="checked"<{/if}>>
            CSV出力を明日の1時に実行する
        </fieldset>

        <button type="submit">保存する</button>
    </form>
    <{if $message}><p class="err"><{$message}></p><{/if}>
    <hr>
    当月、前月分は月次、それ以前は遡及で出力されます
    <ul>
        <{foreach from=$filelist item=val}>
        <li>
            <{$val.timestamp|escape}>作成&nbsp;&nbsp;-&nbsp;&nbsp;<a href="./shift/csv/<{$val.name|escape}>"><span><{$val.name|escape}></span></a>
        </li>
        <{/foreach}>
    </ul>
</div>

<{include file="shift/footer.tpl"}>
