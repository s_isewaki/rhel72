<{include file="shift/header.tpl" title="勤務予定" tab="11"}>

<{if ! $ward_lists}>
    <div class="attention">参照可能な部署はありません。</div>
<{elseif $future}>
    <{if $this_ward->month_type() === '2'}>
        <div class="attention"><{$viewym[0]|escape}>年<{$viewym[1]|escape}>クールの予定表はまだ参照できません。</div>
    <{else}>
        <div class="attention"><{$viewym[0]|escape}>年<{$viewym[1]|escape}>月の予定表はまだ参照できません。</div>
    <{/if}>
    <button type="button" onclick="history.back();">前の画面に戻る</button>
</div>
<{else}>
    <style type="text/css">
        <!--
        html {
            overflow: auto;
            overflow-y: hidden;
        }
        // -->
    </style>

    <script type="text/javascript">
        <!--
        var base_href = '<{$BASE_HREF}>/';
        var this_group_id = '<{$this_ward->group_id()}>';
        var this_pattern_id = '<{$this_ward->pattern_id()}>';
        var this_y = '<{$year}>';
        var this_m = '<{$month}>';
        var pattern = <{$pattern_json}>;
        var pattern_ok = <{$pattern_ok}>;
        var display = <{$this_ward->display()}>;
        var data = <{$table->table_json()}>;
        var fscreen = <{if $fullscreen}>true<{else}>false<{/if}>;
            var start_date = '<{$start_date}>';
            var end_date = '<{$end_date}>';
            var prev_start_date = '<{$prev_start_date}>';
            var prev_end_date = '<{$prev_end_date}>';
            // -->
    </script>


    <div id="shift">
        <div id="wrapHead">
            <{if !$fullscreen}>
                <!-- 部署プルダウン -->
                <form name="shift/plan.php" method="get">
                    <input type="hidden" id="this_y" name="y" value="<{$year}>" />
                    <input type="hidden" id="this_m" name="m" value="<{$month}>" />
                    <div class="pulldown">
                        <b>部署</b>：
                        <select id="this_group_id" name="group_id" onchange="submit(this.form)">
                            <{foreach from=$ward_lists item=row}>
                                <option value="<{$row->group_id()|escape}>" <{if $row->group_id() === $this_ward->group_id()}>selected<{/if}>><{$row->group_name()|escape}></option>
                            <{/foreach}>
                        </select>
                    </div>
                </form>

                <!-- 月・クール切り替えタブ -->
                <div id="month">
                    <ul>
                        <li><a href="shift/plan.php?y=<{$lastyear}>&m=<{$month}>&group_id=<{$this_ward->group_id()|escape}>"><span>&lt;前年</span></a></li>
                            <{foreach from=$yearmonth item=row}>
                            <li <{if $row.i === 0}>class="selected"<{/if}>>
                                <a href="shift/plan.php?y=<{$row.year}>&m=<{$row.month}>&group_id=<{$this_ward->group_id()|escape}>"><span><{$row.vyear}>/<{$row.vmonth}></span></a>
                            </li>
                        <{/foreach}>
                        <{if $shift_admin || $shift_access}>
                            <li><a href="shift/plan.php?y=<{$nextyear}>&m=<{$month}>&group_id=<{$this_ward->group_id()|escape}>"><span>翌年&gt;</span></a></li>
                            <{/if}>
                    </ul>
                </div>
                <div class="clear"></div>
            <{/if}>

            <div id="loading"></div>

            <!-- ボタン -->
            <div id="shiftmenu">
                <ul id="shiftmenu_icon">
                    <{if $shift_admin}>
                        <li id="icon_plan"><img src="img/ico-illust-touroku.gif" alt="登録" width="36" height="36" border="0" /><br />登録</li>
                        <li id="icon_draft"><img src="img/ico-illust-shitagaki.gif" alt="下書き" width="36" height="36" border="0" /><br />下書き</li>
                        <li id="icon_stamp"><img src="img/ico-illust-stamp.gif" alt="スタンプ" width="36" height="36" border="0" /><br />スタンプ</li>
                        <li id="icon_reload"><img src="img/ico-illust-saikeisan.gif" alt="再計算" width="36" height="36" border="0" /><br />再計算</li>
                        <li id="icon_copy"><img src="img/ico-illust-copy-kako.gif" alt="コピー" width="36" height="36" border="0" /><br />コピー</li>
                        <!-- li id="icon_check"><img src="img/ico-illust-check.gif" alt="チェック" width="36" height="36" border="0" /><br />チェック</li -->
                        <!-- li><img src="img/ico-illust-insatu.gif" alt="印刷" width="36" height="36" border="0" /><br />印刷</li -->
                        <!-- li><img src="img/ico-illust-excel.gif" alt="エクセル" width="36" height="36" border="0" /><br />エクセル</li -->
                    <{/if}>
                    <{if !$fullscreen}><li id="icon_fullscreen"><img src="img/ico-illust-zengamen.gif" alt="全画面" width="36" height="36" border="0" /><br />全画面</li><{/if}>
                </ul>
                <ul id="shiftmenu_button">
                    <li><button type="button" class="line2" id="button_hope">勤務希望</button></li>
                    <li><button type="button" class="line2" id="button_results">予実績</button></li>
                    <li><button type="button" class="line2" id="button_duty">当直</button></li>
                    <li><button type="button" class="line2" id="button_comment">コメント</button></li>
                        <{if $shift_admin}>
                            <{if !$fullscreen}>
                            <li><button type="button" id="button_plan_staff">当月職員設定</button></li>
                            <{/if}>
                        <li><button type="button" id="button_hope_import">勤務希望取込</button></li>
                        <li><button type="button" id="button_history">登録履歴</button>
                            <div id="history"></div>
                        </li>
                    <{/if}>
                </ul>
                <ul id="shiftview_button">
                    <li><button type="button" id="button_team">チーム</button></li>
                    <li><button type="button" id="button_job">職種</button></li>
                    <li><button type="button" id="button_status">役職</button></li>
                    <li><button type="button" id="button_kadou">稼働時間</button></li>
                    <li><button type="button" id="button_count">集計</button></li>
                    <li><button type="button" id="button_holiday">有休</button></li>
                    <li><button type="button" id="button_summer">夏休</button></li>
                    <li><button type="button" id="button_week">週休</button></li>
                </ul>
            </div>
            <div class="clear"></div>

            <!-- 情報 -->
            <div id="info">
                <span><{$start_date}> 〜 <{$end_date}></span>
                <span id="display_draft" <{if !$table->is_draft()}>style="display:none;"<{/if}>>下書き:あり</span>
                <span id="display_plan" <{if !$table->is_plan()}>style="display:none;"<{/if}>>登録:あり</span>
                <{if $this_ward->month_type() === '2'}>
                    <{assign var=working_hours value=$whours.working_hours/60}>
                    <span>要稼働時間：<{$working_hours}>時間 (週休<{$whours.holiday_count}>回)</span>
                <{/if}>
                <span id="night_hours_avg" title="夜勤従事者の延べ夜勤時間：<{$table->night_hours('plan')|escape}>時間 ÷ 夜勤従事者数：<{$table->night_nurse_count('plan')|escape}>人">月平均夜勤時間：<{$table->night_hours_avg('plan')|escape}>時間</span>
            </div>
            <div class="clear"></div>

            <!-- 勤務表 header -->
            <table id="plan_head" class="shift">
                <thead>
                    <tr>
                        <td rowspan="2" class="scrollbar">&nbsp;</td>
                        <td rowspan="2" class="team">チー<br />ム</td>
                        <td rowspan="2" class="name">氏名</td>
                        <td rowspan="2" class="job">職種</td>
                        <td rowspan="2" class="status">役職</td>
                        <{foreach from=$dates item=dt}>
                            <td rowspan="2" class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}>"><{$dt.d}><br /><{$dt.wj}></td>
                            <{/foreach}>
                        <td rowspan="2" class="alert">警<br />告</td>
                        <td class="kadou" colspan="4">稼働時間</td>
                        <td class="total" colspan="<{$count_row_count|escape}>">集計</td>
                        <td class="holiday" colspan="4">有休</td>
                        <td class="summer" colspan="3">夏休</td>
                        <td class="week" colspan="3">週休</td>
                        <td rowspan="2" class="name">氏名</td>
                    </tr>
                    <tr>
                        <td class="kadou timew">前月<br />繰越</td>
                        <td class="kadou timew">当月<br />実働</td>
                        <td class="kadou timew">実働<br />差</td>
                        <td class="kadou timew">夜勤<br />時間</td>
                            <{foreach from=$count_row item=cr}>
                            <td class="total countw" style="font-size:<{$cr.size|default:10|escape}>px;"><{$cr.name|escape}></td>
                        <{/foreach}>
                        <td class="holiday countw">繰越</td>
                        <td class="holiday countw">付与</td>
                        <td class="holiday countw">取得</td>
                        <td class="holiday countw">残</td>
                        <td class="summer countw">付与</td>
                        <td class="summer countw">取得</td>
                        <td class="summer countw">残</td>
                        <td class="week countw">付与</td>
                        <td class="week countw">取得</td>
                        <td class="week countw">残</td>
                    </tr>
                </thead>
            </table>
        </div>
        <div id="wrapCont">
            <!-- 勤務表 -->
            <table id="plan" class="shift">
                <tfoot>
                    <tr class="alert">
                        <td class="team">&nbsp;</td>
                        <td class="name">警告</td>
                        <td class="job">&nbsp;</td>
                        <td class="status">&nbsp;</td>
                        <{foreach from=$dates item=dt}>
                            <td id="alert_d<{$dt.date}>" class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}>">&nbsp;</td>
                        <{/foreach}>
                    </tr>
                    <{foreach from=$count_col item=cc}>
                        <tr class="count">
                            <td class="team">&nbsp;</td>
                            <td class="name"><{$cc.name}></td>
                            <td class="job">&nbsp;</td>
                            <td class="status">&nbsp;</td>
                            <{foreach from=$dates item=dt}>
                                <td id="d<{$dt.date}>_c<{$cc.count}>" class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}>"><{$table->cell_col_count($dt.date,$cc.count,'plan')|default:'0'|escape}></td>
                            <{/foreach}>
                        </tr>
                    <{/foreach}>
                </tfoot>

                <tbody>
                    <{foreach from=$staff item=row}>
                        <!-- 勤務予定 -->
                        <tr class="e<{$row->emp_id()}>_plan plan input">
                            <td id="e<{$row->emp_id()}>_team" class="team"><{$table->cell_emp($row->emp_id(),'team')|escape}></td>
                            <td id="e<{$row->emp_id()}>_name" class="name <{if $row->assist()}>assist<{/if}>"><{$table->cell_emp($row->emp_id(),'name')|escape}></td>
                            <td id="e<{$row->emp_id()}>_job" class="job"><{$table->cell_emp($row->emp_id(),'job_name')|escape}></td>
                            <td id="e<{$row->emp_id()}>_status" class="status"><{$table->cell_emp($row->emp_id(),'st_name')|escape}></td>
                            <{foreach from=$dates item=dt}>
                                <td id="e<{$row->emp_id()}>_d<{$dt.date}>_plan"
                                    class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}> <{if $row->is_employed($dt.date)}><{if $shift_admin && !$table->cell_assist($row->emp_id(),$dt.date,'plan')}>cell<{/if}><{else}>retire<{/if}>"
                                    style="<{$table->cell_style($row->emp_id(),$dt.date,'plan')|escape}>">
                                    <{$table->cell_sign($row->emp_id(),$dt.date,'plan')|escape}>
                                </td>
                            <{/foreach}>
                            <td id="e<{$row->emp_id()}>_alert" class="alert">&nbsp;</td>
                            <td id="e<{$row->emp_id()}>_wh_over_plan" class="kadou time"><{$table->cell_emp($row->emp_id(),'wh_over','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_wh_hour_plan" class="kadou time"><{$table->cell_emp($row->emp_id(),'wh_hour','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_wh_diff_plan" class="kadou time" title="<{$table->cell_emp($row->emp_id(),'wh_base','plan')|escape}>"><{$table->cell_emp($row->emp_id(),'wh_diff','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_wh_night_plan" class="kadou time"><{$table->cell_emp($row->emp_id(),'wh_night','plan')|escape}></td>
                            <{foreach from=$count_row item=cr}>
                                <td id="e<{$row->emp_id()}>_c<{$cr.count}>_plan" class="total count"><{$table->cell_row_count($row->emp_id(),$cr.count,'plan')|default:'0'|escape}></td>
                            <{/foreach}>
                            <td id="e<{$row->emp_id()}>_paid_h_kuri_plan" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_kuri','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_fuyo_plan" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_fuyo','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_get_plan" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_get','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_plan"     class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_all_plan" class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s_all','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_get_plan" class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s_get','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_plan"     class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_all_plan" class="week count"><{$table->cell_emp($row->emp_id(),'paid_w_all','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_get_plan" class="week count"><{$table->cell_emp($row->emp_id(),'paid_w_get','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_plan"     class="week count"><{$table->cell_emp($row->emp_id(),'paid_w','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_name" class="name <{if $row->assist()}>assist<{/if}>"><{$table->cell_emp($row->emp_id(),'name')|escape}></td>
                        </tr>

                        <!-- 勤務希望 -->
                        <tr class="e<{$row->emp_id()}>_hope hope input line2">
                            <td class="team filler">&nbsp;</td>
                            <td class="name filler">勤務希望</td>
                            <td class="job filler">&nbsp;</td>
                            <td class="status filler">&nbsp;</td>
                            <{foreach from=$dates item=dt}>
                                <td id="e<{$row->emp_id()}>_d<{$dt.date}>_hope"
                                    class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}> <{if $row->is_employed($dt.date)}><{if $shift_admin && !$table->cell_assist($row->emp_id(),$dt.date,'hope')}>cell<{/if}><{else}>retire<{/if}>"
                                    style="<{$table->cell_style($row->emp_id(),$dt.date,'hope')|escape}>">
                                    <{$table->cell_sign($row->emp_id(),$dt.date,'hope')|escape}>
                                </td>
                            <{/foreach}>
                            <td class="alert">&nbsp;</td>
                            <td class="kadou filler" colspan="4">&nbsp;</td>
                            <{foreach from=$count_row item=cr}>
                                <td id="e<{$row->emp_id()}>_c<{$cr.count}>_hope" class="total count"><{$table->cell_row_count($row->emp_id(),$cr.count,'hope')|default:'0'|escape}></td>
                            <{/foreach}>
                            <td id="e<{$row->emp_id()}>_paid_h_kuri_hope" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_kuri','hope')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_fuyo_hope" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_fuyo','hope')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_get_hope" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_get','hope')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_hope"     class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h','hope')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_all_hope" class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s_all','hope')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_get_hope" class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s_get','hope')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_hope"     class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s','hope')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_all_hope" class="week count"><{$table->cell_emp($row->emp_id(),'paid_w_all','hope')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_get_hope" class="week count"><{$table->cell_emp($row->emp_id(),'paid_w_get','hope')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_hope"     class="week count"><{$table->cell_emp($row->emp_id(),'paid_w','hope')|escape}></td>
                            <td class="name filler">勤務希望</td>
                        </tr>

                        <!-- 勤務実績 -->
                        <tr class="e<{$row->emp_id()}>_results results line2">
                            <td class="team filler">&nbsp;</td>
                            <td class="name filler">勤務実績</td>
                            <td class="job filler">&nbsp;</td>
                            <td class="status filler">&nbsp;</td>
                            <{foreach from=$dates item=dt}>
                                <td id="e<{$row->emp_id()}>_d<{$dt.date}>_results"
                                    class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}> <{if $row->is_employed($dt.date)}><{else}>retire<{/if}>"
                                    style="<{$table->cell_style($row->emp_id(),$dt.date,'results')|escape}>">
                                    <{$table->cell_sign($row->emp_id(),$dt.date,'results')|escape}>
                                </td>
                            <{/foreach}>
                            <td class="alert">&nbsp;</td>
                            <td id="e<{$row->emp_id()}>_wh_over_results" class="kadou time"><{$table->cell_emp($row->emp_id(),'wh_over','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_wh_hour_results" class="kadou time"><{$table->cell_emp($row->emp_id(),'wh_hour','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_wh_diff_results" class="kadou time" title="<{$table->cell_emp($row->emp_id(),'wh_base','results')|escape}>"><{$table->cell_emp($row->emp_id(),'wh_diff','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_wh_night_results" class="kadou time"><{$table->cell_emp($row->emp_id(),'wh_night','results')|escape}></td>
                            <{foreach from=$count_row item=cr}>
                                <td id="e<{$row->emp_id()}>_c<{$cr.count}>_results" class="total count"><{$table->cell_row_count($row->emp_id(),$cr.count,'results')|default:'0'|escape}></td>
                            <{/foreach}>
                            <td id="e<{$row->emp_id()}>_paid_h_kuri_results" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_kuri','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_fuyo_results" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_fuyo','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_get_results" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_get','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_results"     class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_all_results" class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s_all','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_get_results" class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s_get','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_results"     class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_all_results" class="week count"><{$table->cell_emp($row->emp_id(),'paid_w_all','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_get_results" class="week count"><{$table->cell_emp($row->emp_id(),'paid_w_get','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_results"     class="week count"><{$table->cell_emp($row->emp_id(),'paid_w','results')|escape}></td>
                            <td class="name filler">勤務実績</td>
                        </tr>

                        <!-- 当直 -->
                        <tr class="e<{$row->emp_id()}>_duty duty line2">
                            <td class="team filler">&nbsp;</td>
                            <td class="name filler">当直</td>
                            <td class="job filler">&nbsp;</td>
                            <td class="status filler">&nbsp;</td>
                            <{foreach from=$dates item=dt}>
                                <td id="e<{$row->emp_id()}>_d<{$dt.date}>_duty"
                                    class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}> <{if $row->is_employed($dt.date)}><{else}>retire<{/if}>">
                                    <{$table->cell_sign($row->emp_id(),$dt.date,'duty')|escape}>
                                </td>
                            <{/foreach}>
                            <td class="alert">&nbsp;</td>
                            <td class="kadou filler" colspan="4">&nbsp;</td>
                            <td class="total filler" colspan="<{$count_row_count}>">&nbsp;</td>
                            <td class="holiday filler" colspan="4">&nbsp;</td>
                            <td class="summer filler" colspan="3">&nbsp;</td>
                            <td class="week filler" colspan="3">&nbsp;</td>
                            <td class="name filler">当直</td>
                        </tr>

                        <!-- コメント -->
                        <tr class="e<{$row->emp_id()}>_comment comment line2">
                            <td class="team filler">&nbsp;</td>
                            <td class="name filler">コメント</td>
                            <td class="job filler">&nbsp;</td>
                            <td class="status filler">&nbsp;</td>
                            <{foreach from=$dates item=dt}>
                                <td id="e<{$row->emp_id()}>_d<{$dt.date}>_comment" class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}>"><{$table->cell_sign($row->emp_id(),$dt.date,'comment')|escape}></td>
                            <{/foreach}>
                            <td class="alert">&nbsp;</td>
                            <td class="kadou filler" colspan="4">&nbsp;</td>
                            <td class="total filler" colspan="<{$count_row_count}>">&nbsp;</td>
                            <td class="holiday filler" colspan="4">&nbsp;</td>
                            <td class="summer filler" colspan="3">&nbsp;</td>
                            <td class="week filler" colspan="3">&nbsp;</td>
                            <td class="name filler">コメント</td>
                        </tr>
                    <{/foreach}>
                </tbody>
            </table>
        </div>
    </div>

    <!-- スタンプ -->
    <div id="stamp">
        <div id="stamp_header">スタンプ<span class="close">[×]</span></div>
        <div id="current_window"><b>現在</b><div id="current_stamp" style="font-size:10px;">未設定</div></div>
        <ul id="stamp_list">
            <li class="stamp" onclick="setCurrentStamp('0');" style="font-size:10px;">未設定</li>
                <{foreach from=$pattern_list item=row}>
                    <{if $row->font_name() != ''}>
                    <li class="stamp" onclick="setCurrentStamp('<{$row->ptn_id()}>');" style="font-size:12px;color:<{$row->font_color_id()|escape}>; background-color:<{$row->back_color_id()|escape}>;" title="<{$row->ptn_name()|escape}>"><{$row->font_name()|escape}></li>
                    <{/if}>
                <{/foreach}>
        </ul>
    </div>

    <!-- キー入力 -->
    <div id="keyboard">
        <div id="keyboard_header">キー入力<span class="close">[×]</span></div>
        <b>[スペース]</b>キーで未設定<br />
        <b>[矢印]</b>キーで移動<br />
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td valign="top">
                    <table class="keyboard_map">
                        <{foreach from=$pattern_list item=row}>
                            <{if $row->font_name() != ''}>
                                <{assign var=i value=$i+1}>
                                <{if $i is div by 22}>
                                </table>
                            </td>
                            <td valign="top">
                                <table class="keyboard_map">
                                <{/if}>
                                <tr>
                                    <td class="keyboard_key"><{$arr_key[$i]|escape}></td>
                                    <td class="keyboard_value" id="key_<{$i|escape}>" style="color:<{$row->font_color_id()|escape}>; background-color:<{$row->back_color_id()|escape}>;" onClick="setPtn('<{$row->ptn_id()}>');">
                                        <div title="<{$row->ptn_name()|escape}>"><{$row->font_name()|escape}></div>
                                    </td>
                                </tr>
                            <{/if}>
                        <{/foreach}>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <!-- コピーフォーム -->
    <div id="copy">
        <div class="background"></div>
        <div class="contents">
            <p>過去の勤務表をコピーします</p>
            <table>
                <tr>
                    <td>予定/実績</td>
                    <td>
                        <label><input type="radio" name="copy_mode" value="plan" checked /> 勤務予定</label>
                        <label><input type="radio" name="copy_mode" value="results" /> 勤務実績</label>
                    </td>
                </tr>
                <tr>
                    <td>コピー元</td>
                    <td>
                        <input readonly type="text" class="datepicker" id="copy_from" size="11" maxlength="10" value="<{$prev_start_date|escape}>" class="eng" /> から
                        <select id="copy_days">
                            <option value="31" <{if $this_ward->month_type() === '1'}>selected<{/if}>>31日分</option>
                            <option value="30">30日分</option>
                            <option value="29">29日分</option>
                            <option value="28" <{if $this_ward->month_type() === '2'}>selected<{/if}>>28日分</option>
                            <option value="27">27日分</option>
                            <option value="26">26日分</option>
                            <option value="25">25日分</option>
                            <option value="24">24日分</option>
                            <option value="23">23日分</option>
                            <option value="22">22日分</option>
                            <option value="21">21日分</option>
                            <option value="20">20日分</option>
                            <option value="19">19日分</option>
                            <option value="18">18日分</option>
                            <option value="17">17日分</option>
                            <option value="16">16日分</option>
                            <option value="15">15日分</option>
                            <option value="14">14日分</option>
                            <option value="13">13日分</option>
                            <option value="12">12日分</option>
                            <option value="11">11日分</option>
                            <option value="10">10日分</option>
                            <option value="9">9日分</option>
                            <option value="8">8日分</option>
                            <option value="7">7日分</option>
                            <option value="6">6日分</option>
                            <option value="5">5日分</option>
                            <option value="4">4日分</option>
                            <option value="3">3日分</option>
                            <option value="2">2日分</option>
                            <option value="1">1日分</option>
                        </select> を
                    </td>
                </tr>
                <tr>
                    <td>コピー先</td>
                    <td>
                        <input readonly type="text" class="datepicker" id="copy_to" size="11" maxlength="10" value="<{$start_date|escape}>" class="eng" /> にコピーする
                    </td>
                </tr>
            </table>
            <button type="button" onclick="copy_submit();">コピーする</button>
            <button type="button" onclick="copy_close();">キャンセル</button>
        </div>
    </div>

    <{capture name="js"}>
        <script type="text/javascript" src="js/shift/common.js" charset="euc-jp"></script>
        <script type="text/javascript" src="js/shift/plan.js" charset="euc-jp"></script>
        <script type="text/javascript">
            <!--
            if (fscreen) {
                resizeIframeEx();
            }
            // -->
        </script>
    <{/capture}>
<{/if}>
<{include file="shift/footer.tpl"}>
