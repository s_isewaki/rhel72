<{include file="shift/header.tpl" title="職員設定" tab="17"}>

<{if ! $ward_lists}>
    <div class="attention">更新可能な部署がありません。</div>
<{else}>

    <script type="text/javascript">
        <!--
        var shift_pattern = '<{pattern_pulldown array=$pattern_list}>';
        // -->
    </script>

    <div id="submenu">
        <ul>
            <li>
                <a id="back" href="<{$BASE_HREF}>/shift/staff.php?y=<{$year|escape}>&amp;m=<{$month|escape}>&amp;group_id=<{$this_ward->group_id()|escape}>">職員一覧に戻る</a>
            </li>
        </ul>
    </div>

    <div id="main">
        <form id="form" action="shift/staff_employee.php" method="post">
            <input type="hidden" name="mode" value="confirm" />
            <input type="hidden" name="y" value="<{$year}>" />
            <input type="hidden" name="m" value="<{$month}>" />
            <input type="hidden" name="group_id" value="<{$this_ward->group_id()}>" />
            <input type="hidden" name="emp_id" value="<{$staff->emp_id()}>" />

            <div class="title"><{$this_ward->group_name()|escape}> : <{$staff->emp_name()|escape}></div>

            <div>
                <button type="submit" class="submit">登録する</button>
            </div>
            <table class="form">
                <tr>
                    <td class="header">雇用・勤務形態</td>
                    <td colspan="8">
                        <label><input type="radio" name="duty_form" value="1" <{if $staff->duty_form() === "1" || !$staff->duty_form()}>checked<{/if}> /> 常勤</label>
                        <label><input type="radio" name="duty_form" value="2" <{if $staff->duty_form() === "2"}>checked<{/if}> /> 非常勤</label>
                        <label><input type="radio" name="duty_form" value="3" <{if $staff->duty_form() === "3"}>checked<{/if}> /> 短時間正職員</label>
                    </td>
                </tr>

                <tr>
                    <td class="header">他部署兼務</td>
                    <td colspan="8">
                        <label><input type="radio" name="other_post_flg" value="1" <{if   $staff->other_post_flg()}>checked<{/if}> /> あり</label>
                        <label><input type="radio" name="other_post_flg" value=""  <{if ! $staff->other_post_flg()}>checked<{/if}> /> なし</label>
                    </td>
                </tr>

                <tr>
                    <td class="header">勤務日</td>
                    <td colspan="8">
                        <label><input type="checkbox" name="duty_mon_day_flg"   value="1" <{if $staff->duty_mon_day_flg()}>checked<{/if}>> 月</label>
                        <label><input type="checkbox" name="duty_tue_day_flg"   value="1" <{if $staff->duty_tue_day_flg()}>checked<{/if}>> 火</label>
                        <label><input type="checkbox" name="duty_wed_day_flg"   value="1" <{if $staff->duty_wed_day_flg()}>checked<{/if}>> 水</label>
                        <label><input type="checkbox" name="duty_thurs_day_flg" value="1" <{if $staff->duty_thurs_day_flg()}>checked<{/if}>> 木</label>
                        <label><input type="checkbox" name="duty_fri_day_flg"   value="1" <{if $staff->duty_fri_day_flg()}>checked<{/if}>> 金</label>
                        <label><input type="checkbox" name="duty_sat_day_flg"   value="1" <{if $staff->duty_sat_day_flg()}>checked<{/if}>> 土</label>
                        <label><input type="checkbox" name="duty_sun_day_flg"   value="1" <{if $staff->duty_sun_day_flg()}>checked<{/if}>> 日</label>
                        <label><input type="checkbox" name="duty_hol_day_flg"   value="1" <{if $staff->duty_hol_day_flg()}>checked<{/if}>> 祝日</label>
                    </td>
                </tr>

                <tr>
                    <td class="header">夜勤の有無</td>
                    <td colspan="8">
                        <label><input type="radio" name="night_duty_flg" value=""  <{if $staff->night_duty_flg() === "0"}>checked<{/if}> /> なし</label>
                        <label><input type="radio" name="night_duty_flg" value="1" <{if $staff->night_duty_flg() === "1"}>checked<{/if}> /> あり</label>
                        <label><input type="radio" name="night_duty_flg" value="2" <{if $staff->night_duty_flg() === "2"}>checked<{/if}> /> 夜勤専従者</label>
                        <p>（夜勤専従者以外の夜勤「あり」「なし」については、届出書添付書類上は夜勤時間により自動判定されます。）</p>
                    </td>
                </tr>

                <tr>
                    <td class="header" rowspan="2">勤務可能なシフト</td>
                    <td colspan="2">
                        月ー金<br />
                        <button type="button" class="small" onclick="check_pattern(1, 'on');">全ON</button>
                        <button type="button" class="small" onclick="check_pattern(1, 'off');">全OFF</button>
                        <button type="button" class="small" onclick="check_pattern(1, 'toggle');">反転</button>
                    </td>
                    <td colspan="2">土<br />
                        <button type="button" class="small" onclick="check_pattern(2, 'on');">全ON</button>
                        <button type="button" class="small" onclick="check_pattern(2, 'off');">全OFF</button>
                        <button type="button" class="small" onclick="check_pattern(2, 'toggle');">反転</button>
                    </td>
                    <td colspan="2">日<br />
                        <button type="button" class="small" onclick="check_pattern(3, 'on');">全ON</button>
                        <button type="button" class="small" onclick="check_pattern(3, 'off');">全OFF</button>
                        <button type="button" class="small" onclick="check_pattern(3, 'toggle');">反転</button>
                    </td>
                    <td colspan="2">祝日<br />
                        <button type="button" class="small" onclick="check_pattern(4, 'on');">全ON</button>
                        <button type="button" class="small" onclick="check_pattern(4, 'off');">全OFF</button>
                        <button type="button" class="small" onclick="check_pattern(4, 'toggle');">反転</button>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <{foreach from=$pattern_list item=row}>
                            <input type="hidden" name="week_ptn[]" value="<{$row->ptn_id()|escape}>" />
                            <label><input class="check1" type="checkbox" name="week_flg[1][<{$row->ptn_id()|escape}>]" value="1" <{if $staff->is_pattern(1, $row->ptn_id())}>checked<{/if}>> <{$row->ptn_name()|escape}></label><br />
                            <{/foreach}>
                    </td>
                    <td colspan="2">
                        <{foreach from=$pattern_list item=row}>
                            <label><input class="check2" type="checkbox" name="week_flg[2][<{$row->ptn_id()|escape}>]" value="1" <{if $staff->is_pattern(2, $row->ptn_id())}>checked<{/if}>> <{$row->ptn_name()|escape}></label><br />
                            <{/foreach}>
                    </td>
                    <td colspan="2">
                        <{foreach from=$pattern_list item=row}>
                            <label><input class="check3" type="checkbox" name="week_flg[3][<{$row->ptn_id()|escape}>]" value="1" <{if $staff->is_pattern(3, $row->ptn_id())}>checked<{/if}>> <{$row->ptn_name()|escape}></label><br />
                            <{/foreach}>
                    </td>
                    <td colspan="2">
                        <{foreach from=$pattern_list item=row}>
                            <label><input class="check4" type="checkbox" name="week_flg[4][<{$row->ptn_id()|escape}>]" value="1" <{if $staff->is_pattern(4, $row->ptn_id())}>checked<{/if}>> <{$row->ptn_name()|escape}></label><br />
                            <{/foreach}>
                    </td>
                </tr>

                <tr>
                    <td class="header">長期休暇</td>
                    <td colspan="8">
                        <button type="button" onclick="add_holiday();">行を追加する</button>
                        <div id="holiday">
                            <{foreach from=$staff->holiday() item=row}>
                                <span>
                                    <select name="holiday[]"><{pattern_pulldown array=$pattern_list ptn=$row.atdptn_ptn_id}></select>
                                    <input type="text" class="datepicker" name="start_date[]" size="11" maxlength="10" value="<{$row.start_date|escape}>" class="eng" /> ー
                                    <input type="text" class="datepicker" name="end_date[]" size="11" maxlength="10" value="<{$row.end_date|escape}>" class="eng" />
                                    <button type="button" onclick="remove_span(this);">削除</button><br>
                                </span>
                            <{/foreach}>
                        </div>
                        <p class="err" id="err_holiday" style="display: none;"></p>
                    </td>
                </tr>

                <tr>
                    <td class="header">
                        時短終了日
                        <p class="err" id="err_end_date" style="display: none;"></p>
                    </td>
                    <td colspan="8">
                        1歳の終了日
                        <p>
                            <input type="text" id="end_1y_date" class="datepicker" name="end_1y_date" size="11" maxlength="10" value="<{$staff->end_1y_date()|escape}>" class="eng" />
                            <button type="button" onclick="clear_text('end_1y_date');">消す</button>
                        </p>
                        3歳 (6歳)の終了日
                        <p>
                            <input type="text" id="end_3y_date" class="datepicker" name="end_3y_date" size="11" maxlength="10" value="<{$staff->end_3y_date()|escape}>" class="eng" />
                            <button type="button" onclick="clear_text('end_3y_date');">消す</button>
                            <button type="button" onclick="add_year('end_1y_date', 'end_3y_date', 2);">1歳+2をセット</button>
                            <button type="button" onclick="add_year('end_1y_date', 'end_3y_date', 5);">1歳+5をセット</button>
                        </p>
                    </td>
                </tr>

                <{assign var="shorter_week" value=$staff->shorter_week()}>
                <tr>
                    <td class="header">
                        時短時間
                    </td>
                    <{foreach from=$week_list key=week item=week_text}>
                        <td>
                            <{$week_text|escape}>
                            <p>
                                <select name="week[]">
                                    <option value=""></option>
                                    <{foreach from=$shorter_list key=min item=min_text}>
                                        <option value="<{$min|escape}>" <{if $min|string_format:"%d" === $shorter_week.$week}> selected="selected"<{/if}>><{$min_text|escape}></option>
                                    <{/foreach}>
                                </select>
                            </p>
                        </td>
                    <{/foreach}>
                </tr>
            </table>

            <div>
                <button type="submit" class="submit">登録する</button>
            </div>
        </form>
    </div>
    <div style="height:100px"></div>

<{/if}>

<{capture name="js"}>
    <script type="text/javascript" src="js/shift/common.js"></script>
    <script type="text/javascript" src="js/shift/staff_employee.js"></script>
<{/capture}>
<{include file="shift/footer.tpl"}>
