<{include file="shift/header.tpl" title="勤務実績" tab="12"}>

<{if ! $ward_lists && ! $approve}>
    <div class="attention">利用できません。</div>
<{else}>
    <style type="text/css">
        <!--
        html {
            overflow: auto;
            overflow-y: hidden;
        }
        // -->
    </style>

    <script type="text/javascript">
        <!--
        var base_href = '<{$BASE_HREF}>/';
        var this_group_id = '<{$this_ward->group_id()}>';
        var this_pattern_id = '<{$this_ward->pattern_id()}>';
        var this_y = '<{$year}>';
        var this_m = '<{$month}>';
        var cours = 'month';
        var pattern = <{$pattern_json}>;
        var pattern_ok = <{$pattern_ok}>;
        var display = <{$this_ward->display()}>;
        var data = <{$table->table_json()}>;
        var fscreen = <{if $fullscreen}>true<{else}>false<{/if}>;
            // -->
    </script>

    <div id="loading"></div>

    <div id="shift">
        <div id="wrapHead">
            <{if !$fullscreen}>
                <!-- 部署プルダウン -->
                <form name="shift/results.php" method="get">
                    <input type="hidden" id="this_y" name="y" value="<{$year}>" />
                    <input type="hidden" id="this_m" name="m" value="<{$month}>" />
                    <input type="hidden" id="this_mode" name="mode" value="month" />
                    <div class="pulldown">
                        <b>部署</b>：
                        <select id="this_group_id" name="group_id" onchange="submit(this.form);">
                            <{foreach from=$ward_lists item=row}>
                                <option value="<{$row->group_id()|escape}>" <{if $row->group_id() === $this_ward->group_id()}>selected<{/if}>><{$row->group_name()|escape}></option>
                            <{/foreach}>
                        </select>
                    </div>
                </form>

                <!-- 月・クール切り替えタブ -->
                <div id="month">
                    <ul>
                        <li><a href="shift/results.php?y=<{$lastyear}>&amp;m=<{$month}>&amp;group_id=<{$this_ward->group_id()|escape}>&amp;mode=month"><span>&lt;前年</span></a></li>
                            <{foreach from=$yearmonth item=row}>
                            <li <{if $row.i === 0}>class="selected"<{/if}>>
                                <a href="shift/results.php?y=<{$row.year}>&amp;m=<{$row.month}>&amp;group_id=<{$this_ward->group_id()|escape}>&amp;mode=month"><span><{$row.year}>/<{$row.month}></span></a>
                            </li>
                        <{/foreach}>
                        <li><a href="shift/results.php?y=<{$nextyear}>&amp;m=<{$month}>&amp;group_id=<{$this_ward->group_id()|escape}>&amp;mode=month"><span>翌年&gt;</span></a></li>
                    </ul>
                </div>
                <div class="clear"></div>
            <{/if}>

            <!-- ボタン -->
            <div id="shiftmenu">
                <ul id="shiftmenu_icon">
                    <{if !$approve}>
                        <li id="icon_results"><img src="img/ico-illust-touroku.gif" alt="登録" width="36" height="36" border="0" /><br />登録</li>
                        <li id="icon_stamp"><img src="img/ico-illust-stamp.gif" alt="スタンプ" width="36" height="36" border="0" /><br />スタンプ</li>
                        <li id="icon_reload"><img src="img/ico-illust-saikeisan.gif" alt="再計算" width="36" height="36"border="0" /><br />再計算</li>
                        <li id="icon_copy"><img src="img/ico-illust-copy-kako.gif" alt="コピー" width="36" height="36" border="0" /><br />コピー</li>

                    <{/if}>
                    <{if !$fullscreen}><li id="icon_fullscreen"><img src="img/ico-illust-zengamen.gif" alt="全画面" width="36" height="36" border="0" /><br />全画面</li><{/if}>
                </ul>
                <ul id="shiftmenu_button">
                    <li><button type="button" id="button_zangyo">残業表示</button></li>

                    <{if $this_ward->month_type() === '2' && !$fullscreen}>
                        <li>
                            <button type="button" class="btn_view" data-y="<{$year}>" data-m="<{$month}>" data-mode="cours">クール表示</button>
                        </li>
                    <{/if}>
                    <li><button type="button" id="approval" data-type="<{if $approve}>999<{else}>1<{/if}>" <{if !$is_month_approval}>disabled="disabled"<{/if}>>月次承認</button></li>
                </ul>
                <ul id="shiftview_button">
                    <li><button type="button" id="button_team">チーム</button></li>
                    <li><button type="button" id="button_job">職種</button></li>
                    <li><button type="button" id="button_status">役職</button></li>
                    <li><button type="button" id="button_count">集計</button></li>
                    <li><button type="button" id="button_holiday">有休</button></li>
                    <li><button type="button" id="button_summer">夏休</button></li>
                    <li><button type="button" id="button_week">週休</button></li>
                </ul>
            </div>
            <div class="clear"></div>

            <!-- 情報 -->
            <div id="info">
                <span><{$start_date}> 〜 <{$end_date}></span>

                <!-- 月次承認 -->
                <span>所属長: <{if $approved[1]}><{$approved[1].approval_date|date_format_jp:"%Y年%-m月%-d日(%a) %R"}> 承認 (<{$approved[1].approval_name|escape}>)<{else}>未承認<{/if}></span>
                <span>部門長: <{if $approved[2]}><{$approved[2].approval_date|date_format_jp:"%Y年%-m月%-d日(%a) %R"}> 承認 (<{$approved[2].approval_name|escape}>)<{else}>未承認<{/if}></span>

            </div>
            <div class="clear"></div>

            <!-- 勤務表 header -->
            <table id="results_head" class="shift">
                <thead>
                    <tr>
                        <td rowspan="2" class="scrollbar">&nbsp;</td>
                        <td rowspan="2" class="team">チー<br />ム</td>
                        <td rowspan="2" class="name">氏名</td>
                        <td rowspan="2" class="approved">確<br/>定</td>

                        <{if $approve}>
                            <td rowspan="2" class="total timew">実働<br />時間</td>
                            <td rowspan="2" class="total timew">残業<br />時間</td>

                        <{/if}>

                        <td rowspan="2" class="job">職種</td>
                        <td rowspan="2" class="status">役職</td>

                        <{foreach from=$dates item=dt}>
                            <td rowspan="2" class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}>">
                                <{$dt.d}><br /><{$dt.wj}>
                            </td>
                        <{/foreach}>

                        <{if !$approve}>
                            <td rowspan="2" class="total timew">実働<br />時間</td>
                            <td rowspan="2" class="total timew">残業<br />時間</td>

                        <{/if}>

                        <{if $count_row}>
                            <td class="total" colspan="<{$count_row_count|escape}>">集計</td>
                        <{/if}>

                        <td class="holiday" colspan="4">有休</td>
                        <td class="summer" colspan="3">夏休</td>
                        <td class="week" colspan="3">週休</td>
                        <td rowspan="2" class="name">氏名</td>
                    </tr>
                    <tr>
                        <{foreach from=$count_row item=cr}>
                            <td class="total countw" style="font-size:<{$cr.size|default:10|escape}>px;"><{$cr.name|escape}></td>
                        <{/foreach}>
                        <td class="holiday countw">繰越</td>
                        <td class="holiday countw">付与</td>
                        <td class="holiday countw">取得</td>
                        <td class="holiday countw">残</td>
                        <td class="summer countw">付与</td>
                        <td class="summer countw">取得</td>
                        <td class="summer countw">残</td>
                        <td class="week countw">付与</td>
                        <td class="week countw">取得</td>
                        <td class="week countw">残</td>
                    </tr>
                </thead>
            </table>
        </div>
        <div id="wrapCont">
            <!-- 勤務表 -->
            <table id="results" class="shift">
                <tfoot>
                    <{foreach from=$count_col item=cc}>
                        <tr class="count">
                            <td class="team">&nbsp;</td>
                            <td class="name"><{$cc.name}></td>
                            <td class="approved">&nbsp;</td>
                            <{if $approve}>
                                <td class="total timew">&nbsp;</td>
                                <td class="total timew">&nbsp;</td>
                            <{/if}>
                            <td class="job">&nbsp;</td>
                            <td class="status">&nbsp;</td>
                            <{foreach from=$dates item=dt}>
                                <td id="d<{$dt.date}>_c<{$cc.count}>" class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}>"><{$table->cell_col_count($dt.date,$cc.count,'results')|default:'0'|escape}></td>
                            <{/foreach}>
                        </tr>
                    <{/foreach}>
                </tfoot>

                <tbody>
                    <{foreach from=$staff item=row}>
                        <!-- 勤務予定 -->
                        <tr class="e<{$row->emp_id()}>_plan plan">
                            <td id="e<{$row->emp_id()}>_team" class="team"><{$table->cell_emp($row->emp_id(),'team')|escape}></td>
                            <td id="e<{$row->emp_id()}>_name" class="name <{if $row->assist()}>assist<{/if}>"><{$table->cell_emp($row->emp_id(),'name')|escape}></td>
                            <td class="approved">&nbsp;</td>

                            <{if $approve}>
                                <td class="total timew">&nbsp;</td>
                                <td class="total timew">&nbsp;</td>
                            <{/if}>

                            <td id="e<{$row->emp_id()}>_job" class="job"><{$table->cell_emp($row->emp_id(),'job_name')|escape}></td>
                            <td id="e<{$row->emp_id()}>_status" class="status"><{$table->cell_emp($row->emp_id(),'st_name')|escape}></td>
                            <{foreach from=$dates item=dt}>
                                <td id="e<{$row->emp_id()}>_d<{$dt.date}>_plan"
                                    class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}> <{if $row->is_employed($dt.date)}><{else}>retire<{/if}>"
                                    style="<{$table->cell_style($row->emp_id(),$dt.date,'plan')|escape}>">
                                    <{$table->cell_sign($row->emp_id(),$dt.date,'plan')|escape}>
                                </td>
                            <{/foreach}>

                            <{if !$approve}>
                                <td class="total timew">&nbsp;</td>
                                <td class="total timew">&nbsp;</td>
                            <{/if}>

                            <{foreach from=$count_row item=cr}>
                                <td id="e<{$row->emp_id()}>_c<{$cr.count}>_plan" class="total count"><{$table->cell_row_count($row->emp_id(),$cr.count,'plan')|default:'0'|escape}></td>
                            <{/foreach}>

                            <td id="e<{$row->emp_id()}>_paid_h_kuri_plan" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_kuri','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_fuyo_plan" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_fuyo','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_get_plan" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_get','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_plan"     class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h','plan')|escape}></td>

                            <td id="e<{$row->emp_id()}>_paid_s_all_plan" class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s_all','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_get_plan" class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s_get','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_plan"     class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s','plan')|escape}></td>

                            <td id="e<{$row->emp_id()}>_paid_w_all_plan" class="week count"><{$table->cell_emp($row->emp_id(),'paid_w_all','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_get_plan" class="week count"><{$table->cell_emp($row->emp_id(),'paid_w_get','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_plan"     class="week count"><{$table->cell_emp($row->emp_id(),'paid_w','plan')|escape}></td>
                            <td id="e<{$row->emp_id()}>_name" class="name <{if $row->assist()}>assist<{/if}>"><{$table->cell_emp($row->emp_id(),'name')|escape}></td>
                        </tr>

                        <!-- 勤務実績 -->
                        <tr class="e<{$row->emp_id()}>_results results input">
                            <td class="team filler">&nbsp;</td>
                            <td class="name filler"><a href="shift/personal.php?y=<{$year}>&amp;m=<{$month}>&amp;view_emp_id=<{$row->emp_id()|escape}>&amp;view_mode=<{if $approve}>999&amp;approve_id=<{$approve_id|escape}><{else}>1<{/if}>">勤務実績</a></td>
                            <td class="approved"><{if $table->cell_emp($row->emp_id(),'approved')}>○<{else}>&nbsp;<{/if}></td>

                            <{if $approve}>
                                <td class="total time"><{$table->cell_emp($row->emp_id(),'total_worktime')|min2time}></td>
                                <td class="total time"><{$table->cell_emp($row->emp_id(),'total_overtime')|min2time}></td>
                            <{/if}>

                            <td class="job filler">&nbsp;</td>
                            <td class="status filler">&nbsp;</td>
                            <{foreach from=$dates item=dt}>
                                <td id="e<{$row->emp_id()}>_d<{$dt.date}>_results"
                                    class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}> <{if $row->is_employed($dt.date)}>cell<{else}>retire<{/if}>"
                                    style="<{$table->cell_style($row->emp_id(),$dt.date,'results')|escape}>">
                                    <{$table->cell_sign($row->emp_id(),$dt.date,'results')|escape}>
                                </td>
                            <{/foreach}>

                            <{if !$approve}>
                                <td class="total time"><{$table->cell_emp($row->emp_id(),'total_worktime')|min2time}></td>
                                <td class="total time"><{$table->cell_emp($row->emp_id(),'total_overtime')|min2time}></td>
                            <{/if}>

                            <{foreach from=$count_row item=cr}>
                                <td id="e<{$row->emp_id()}>_c<{$cr.count}>_results" class="total count"><{$table->cell_row_count($row->emp_id(),$cr.count,'results')|default:'0'|escape}></td>
                            <{/foreach}>

                            <td id="e<{$row->emp_id()}>_paid_h_kuri_results" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_kuri','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_fuyo_results" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_fuyo','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_get_results" class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h_get','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_h_results"     class="holiday count"><{$table->cell_emp($row->emp_id(),'paid_h','results')|escape}></td>

                            <td id="e<{$row->emp_id()}>_paid_s_all_results" class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s_all','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_get_results" class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s_get','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_s_results"     class="summer count"><{$table->cell_emp($row->emp_id(),'paid_s','results')|escape}></td>

                            <td id="e<{$row->emp_id()}>_paid_w_all_results" class="week count"><{$table->cell_emp($row->emp_id(),'paid_w_all','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_get_results" class="week count"><{$table->cell_emp($row->emp_id(),'paid_w_get','results')|escape}></td>
                            <td id="e<{$row->emp_id()}>_paid_w_results"     class="week count"><{$table->cell_emp($row->emp_id(),'paid_w','results')|escape}></td>
                            <td class="name filler"><a href="shift/personal.php?y=<{$year}>&amp;m=<{$month}>&amp;view_emp_id=<{$row->emp_id()|escape}>&amp;view_mode=1">勤務実績</a></td>
                        </tr>

                        <!-- 残業時間 -->
                        <tr class="e<{$row->emp_id()}>_results_ovtm results_ovtm">
                            <td class="team filler">&nbsp;</td>
                            <td class="name filler">残業時間</td>
                            <td class="approved filler">&nbsp;</td>

                            <{if $approve}>
                                <td class="total filler"></td>
                                <td class="total filler"></td>
                            <{/if}>

                            <td class="job filler">&nbsp;</td>
                            <td class="status filler">&nbsp;</td>
                            <{foreach from=$dates item=dt}>
                                <td id="e<{$row->emp_id()}>_d<{$dt.date}>_results_ovtm" class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}> <{if $row->is_employed($dt.date)}><{else}>retire<{/if}>">
                                    <{$table->cell_sign($row->emp_id(),$dt.date,'results_ovtm')|escape}>
                                </td>
                            <{/foreach}>

                            <{if !$approve}>
                                <td class="total filler"></td>
                                <td class="total filler"></td>
                            <{/if}>

                            <{foreach from=$count_row item=cr}>
                                <td class="total filler"></td>
                            <{/foreach}>

                            <td class="holiday filler"></td>
                            <td class="holiday filler"></td>
                            <td class="holiday filler"></td>
                            <td class="holiday filler"></td>

                            <td class="summer filler"></td>
                            <td class="summer filler"></td>
                            <td class="summer filler"></td>

                            <td class="week filler"></td>
                            <td class="week filler"></td>
                            <td class="week filler"></td>
                            <td class="name filler"></td>
                        </tr>

                        <!-- 残業理由 -->
                        <tr class="e<{$row->emp_id()}>_results_ovtmrsn results_ovtmrsn">
                            <td class="team filler">&nbsp;</td>
                            <td class="name filler">残業理由</td>
                            <td class="approved filler">&nbsp;</td>

                            <{if $approve}>
                                <td class="total filler"></td>
                                <td class="total filler"></td>
                            <{/if}>

                            <td class="job filler">&nbsp;</td>
                            <td class="status filler">&nbsp;</td>
                            <{foreach from=$dates item=dt}>
                                <td id="e<{$row->emp_id()}>_d<{$dt.date}>_results_ovtmrsn" class="day d<{$dt.date}> wk<{$dt.w}> ty<{$dt.type}> <{if $row->is_employed($dt.date)}><{else}>retire<{/if}>">
                                    <{$table->cell_sign($row->emp_id(),$dt.date,'results_ovtmrsn')|escape}>
                                </td>
                            <{/foreach}>

                            <{if !$approve}>
                                <td class="total filler"></td>
                                <td class="total filler"></td>
                            <{/if}>

                            <{foreach from=$count_row item=cr}>
                                <td class="total filler"></td>
                            <{/foreach}>

                            <td class="holiday filler"></td>
                            <td class="holiday filler"></td>
                            <td class="holiday filler"></td>
                            <td class="holiday filler"></td>

                            <td class="summer filler"></td>
                            <td class="summer filler"></td>
                            <td class="summer filler"></td>

                            <td class="week filler"></td>
                            <td class="week filler"></td>
                            <td class="week filler"></td>
                            <td class="name filler"></td>
                        </tr>
                    <{/foreach}>
                </tbody>
            </table>
        </div>
    </div>

    <!-- スタンプ -->
    <div id="stamp">
        <div id="stamp_header">スタンプ<span class="close">[×]</span></div>
        <div id="current_window"><b>現在</b><div id="current_stamp" style="font-size:10px;">未設定</div></div>
        <ul id="stamp_list">
            <li class="stamp" onclick="setCurrentStamp('0');" style="font-size:10px;">未設定</li>
                <{foreach from=$pattern_list item=row}>
                    <{if $row->font_name() != ''}>
                    <li class="stamp" onclick="setCurrentStamp('<{$row->ptn_id()}>');" style="font-size:12px;color:<{$row->font_color_id()|escape}>; background-color:<{$row->back_color_id()|escape}>;" title="<{$row->ptn_name()|escape}>"><{$row->font_name()|escape}></li>
                    <{/if}>
                <{/foreach}>
        </ul>
    </div>

    <{capture name="js"}>
        <script type="text/javascript" src="js/shift/common.js" charset="euc-jp"></script>
        <script type="text/javascript" src="js/shift/results.js" charset="euc-jp"></script>
        <script type="text/javascript">
            <!--
            if (fscreen) {
                resizeIframeEx();
            }
            $(function() {
                $('#approval').on('click', function() {
                    var type = $(this).data('type');

                    if (confirm("この月の勤務表を承認してよろしいですか?\n (この操作は取り消せません)")) {
                        $.ajax({
                            type: "POST",
                            url: 'shift/results_approval_ajax.php',
                            data: {
                                year: this_y,
                                month: this_m,
                                group_id: this_group_id,
                                type: type
                            },
                            success: function() {
                                if (type === 999) {
                                    if (window.opener && !window.opener.closed && window.opener.reload_page) {
                                        window.opener.reload_page();
                                    }
                                    window.close();
                                }
                                else {
                                    location.reload();
                                }
                            },
                            error: function() {
                                alert('月次確定に失敗しました。');
                                location.reload();
                            }
                        });
                    }
                    return false;
                });

            });
            // -->
        </script>
    <{/capture}>
<{/if}>
<{include file="shift/footer.tpl"}>
