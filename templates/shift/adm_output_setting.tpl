<{include file="shift/header.tpl" title="管理画面：データ出力" tab="07"}>

<div id="submenu">
    <ul>
        <li>
            <a href="shift/adm_output.php"><span>データ出力</span></a>
        </li>
        <li>
            <a href="shift/adm_output_setting.php"><span>レイアウト設定</span></a>
        </li>
        <li>
            <a href="shift/adm_output_batch_setting.php"><span>バッチ出力</span></a>
        </li>
    </ul>
</div>
<hr style="margin:10px 0;"/>

<div style="margin:20px 10px;">
    <form method="post" action="shift/adm_output_setting_edit.php">
        <button type="submit">レイアウトを追加する</button>
    </form>

    <{if $lists}>
        <table class="list">
            <thead>
                <tr>
                    <td>名称</td>
                    <td>文字コード</td>
                    <td>レイアウト設定</td>
                    <td>CSV</td>
                </tr>
            </thead>
            <tbody>
                <{foreach from=$lists item=row}>
                    <tr>
                        <td><{$row.title|escape}></td>
                        <td align="center"><{if $row.encoding === 'shift_jis'}>Shift_JIS<{elseif $row.encoding === 'euc-jp'}>EUC-JP<{else}>UTF-8<{/if}></td>
                        <td>
                            <form method="post" action="shift/adm_output_setting_edit.php">
                                <input type="hidden" name="id" value="<{$row.id|escape}>" />
                                <button type="submit">レイアウト設定</button>
                            </form>
                        </td>
                        <td>
                            <form method="post" action="shift/adm_output_setting_csv.php">
                                <input type="hidden" name="id" value="<{$row.id|escape}>" />
                                <button type="submit">CSV</button>
                            </form>
                        </td>
                    </tr>
                <{/foreach}>
            </tbody>
        </table>
    <{/if}>
</div>

<{include file="shift/footer.tpl"}>
