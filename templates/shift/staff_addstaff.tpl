<{foreach from=$staff item=row}>
    <tr id="<{$row->emp_id()|escape}>">
        <td class="dragHandle"><input type="hidden" name="order[]" value="<{$row->emp_id()|escape}>" /></td>
        <td>
            <label><input type="radio" name="team[<{$row->emp_id()|escape}>]" value="" <{if ! $row->team_id()}>checked<{/if}> /> なし</label>
        </td>
        <{foreach from=$team item=t}>
            <td>
                <label><input type="radio" name="team[<{$row->emp_id()|escape}>]" value="<{$t.team_id|escape}>" <{if $t.team_id == $row->team_id()}>checked<{/if}>/> <{$t.team_name|escape}></label>
            </td>
        <{/foreach}>
        <td><{$row->emp_name()|escape}></td>
        <td><{$row->job_name()|escape}></td>
        <td><{$row->st_name()|escape}></td>
        <td><{$row->duty_years()|escape}></td>
        <td><{$row->duty_form_name()|escape}></td>
        <td><{$row->other_post_name()|escape}></td>
        <td><{$row->night_duty_name()|escape}></td>
        <td><a href="shift/staff_employee.php?emp_id=<{$row->emp_id()|escape}>&amp;y=<{$year|escape}>&amp;m=<{$month|escape}>&amp;group_id=<{$group_id|escape}>">個人設定</a></td>
        <td><button type="button" onclick="removeStaff(this);">削除する</button></td>
    </tr>
<{/foreach}>
