<{include file="shift/header.tpl" title="帳票出力" tab="15"}>

<{if ! $ward_lists}>
    <div class="attention">利用できません。</div>
<{else}>

    <script type="text/javascript">
        <!--
        var base_href = '<{$BASE_HREF}>/';
        var this_group_id = '<{$this_ward->group_id()}>';
        var this_y = '<{$year}>';
        var this_m = '<{$month}>';
        var view_y = '<{$view_year}>';
        var view_m = '<{$view_month}>';
        // -->
    </script>

    <div id="main">
        <form name="print" method="get">
            <!-- 部署プルダウン -->
            <div class="group_id">
                <div class="pulldown">
                    <b>■部署</b>：
                    <select id="this_group_id" name="group_id">
                        <{foreach from=$ward_lists item=row}>
                            <option value="<{$row->group_id()|escape}>"><{$row->group_name()|escape}></option>
                        <{/foreach}>
                    </select>
                </div>
            </div>
            <div class="clear"></div>
            <hr/>

            <div class="cours">
                <div class="pulldown">
                    <b>■クール</b>：
                    <select name="year" id="cours_year">
                        <{section name=y loop=$nextyear step=-1 max=20}>
                            <option value="<{$smarty.section.y.index}>" <{if $smarty.section.y.index === $view_year}>selected="selected"<{/if}>><{$smarty.section.y.index}></option>
                        <{/section}>
                    </select>年
                    <select name="month" id="cours_month">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                    </select>クール
                </div>
                <div class="clear"></div>
                <table class="list">
                    <thead>
                        <tr>
                            <td width="150">種類</td>
                            <td width="50">PDF</td>
                            <td width="50">Excel</td>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>勤務表</td>
                            <td><button type="button" onclick="print_pdf('shift/pdf01000.php', 'cours');">PDF</button></td>
                            <td><button type="button" onclick="print_xls('shift/xls01000.php', 'cours');">Excel</button></td>
                        </tr>

                        <tr>
                            <td>出勤簿</td>
                            <td><button type="button" onclick="print_pdf('shift/pdf02000.php', 'cours');">PDF</button></td>
                            <td><button type="button" onclick="print_xls('shift/xls02000.php', 'cours');">Excel</button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <hr/>

            <div class="month">
                <div class="pulldown">
                    <b>■年月</b>：
                    <select name="year" id="select_year">
                        <{section name=y loop=$nextyear step=-1 max=20}>
                            <option value="<{$smarty.section.y.index}>"><{$smarty.section.y.index}></option>
                        <{/section}>
                    </select>年
                    <select name="month" id="select_month">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                    </select>月
                </div>
                <div class="clear"></div>
                <table class="list">
                    <thead>
                        <tr>
                            <td width="150">種類</td>
                            <td width="50">PDF</td>
                            <td width="50">Excel</td>
                            <td>職員</td>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>勤務表</td>
                            <td><button type="button" onclick="print_pdf('shift/pdf01000.php', 'month');">PDF</button></td>
                            <td><button type="button" onclick="print_xls('shift/xls01000.php', 'month');">Excel</button></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>出勤簿</td>
                            <td><button type="button" onclick="print_pdf('shift/pdf02000.php', 'month');">PDF</button></td>
                            <td><button type="button" onclick="print_xls('shift/xls02000.php', 'month');">Excel</button></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>実績簿</td>
                            <td><button type="button" onclick="print_pdf('shift/pdf03000.php', 'month');">PDF</button></td>
                            <td><button type="button" onclick="print_xls('shift/xls03000.php', 'month');">Excel</button></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>実績簿(残業時間あり)</td>
                            <td><button type="button" onclick="print_pdf('shift/pdf03100.php', 'month');">PDF</button></td>
                            <td><button type="button" onclick="print_xls('shift/xls03100.php', 'month');">Excel</button></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>集計表</td>
                            <td><button type="button" onclick="print_pdf('shift/pdf05000.php', 'month');">PDF</button></td>
                            <td><button type="button" onclick="print_xls('shift/xls05000.php', 'month');">Excel</button></td>
                            <td>
                                <select id="month_length">
                                    <option value="1">当月分</option>
                                    <option value="2">前2ヶ月分</option>
                                    <option value="3">前3ヶ月分</option>
                                    <option value="4">前4ヶ月分</option>
                                    <option value="5">前5ヶ月分</option>
                                    <option value="6">前6ヶ月分</option>
                                    <option value="7">前7ヶ月分</option>
                                    <option value="8">前8ヶ月分</option>
                                    <option value="9">前9ヶ月分</option>
                                    <option value="10">前10ヶ月分</option>
                                    <option value="11">前11ヶ月分</option>
                                    <option value="12">前12ヶ月分</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>時間外指示簿</td>
                            <td><button type="button" onclick="print_pdf('shift/pdf04000.php', 'month');">PDF</button></td>
                            <td rowspan="2">&nbsp;</td>
                            <td rowspan="2">
                                <select id="this_staff"></select>
                                <div id="print_staff"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>出勤簿(年単位)</td>
                            <td><button type="button" onclick="print_pdf('shift/pdf06000.php', 'month');">PDF</button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <{capture name="js"}>
        <script type="text/javascript" src="js/shift/common.js"></script>
        <script type="text/javascript" src="js/shift/print.js"></script>
    <{/capture}>
<{/if}>
<{include file="shift/footer.tpl"}>
