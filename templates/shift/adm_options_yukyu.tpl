<{include file="shift/header.tpl" title="管理画面：オプション" tab="08"}>

<{include file="shift/adm_options_submenu.tpl"}>

<div id="main">

    <fieldset>
        <b><a href="work_admin_paid_holiday.php?session=<{$SESSION|escape}>">勤務管理 [有休繰越・付与日数更新] へ移動</a></b>
    </fieldset>

    <form action="shift/adm_options_yukyu.php" method="post">

        <fieldset>
            <legend>有休付与日</legend>
            <label><select name="year"><{foreach item=y from=$years}><option value="<{$y|escape}>" <{if $y == $year}>selected="selected"<{/if}>><{$y|escape}></option><{/foreach}></select>年</label>
            4月 1日
        </fieldset>

        <button type="submit" name="mode" value="confirm">付与内容を確認する</button>

        <{if $mode === 'update' }>
            <hr/>
            <{if $lists}>
                <div style="margin:10px 0; padding:10px;border:1px solid black;">対象者：<b><{$cnt|escape}>名</b></div>

                <table class="list">
                    <thead>
                        <tr>
                            <td rowspan="2">職員ID</td>
                            <td rowspan="2">職員名</td>
                            <td rowspan="2">入職日</td>
                            <td rowspan="2">有休付与日</td>
                            <td rowspan="2">付与日の<br>勤続年数</td>
                            <td rowspan="2">2015/1/1<br>付与</td>
                            <td rowspan="2">有休<br>表</td>

                            <td rowspan="2">当年<br>付与</td>

                            <td colspan="8">前年</td>

                            <td rowspan="2">前年<br>付与</td>

                            <td rowspan="2">当年<br>繰越 <span title="※残日数と前年付与日数のうち、少ないほうを繰越とする">[?]</span></td>
                        </tr>
                        <tr>
                            <td align="right">有休 (</td>
                            <td align="right">前々年 +</td>
                            <td align="right">付与 +</td>
                            <td align="right">調整 ) -</td>

                            <td align="right">使用 (</td>
                            <td align="right">調整 +</td>
                            <td align="right">消化 ) =</td>
                            <td align="right">残日数</td>
                        </tr>
                    </thead>
                    <tbody>
                        <{foreach item=row from=$lists}>
                            <tr>
                                <td><span title="<{$row[0]|escape}>"><{$row[1]|escape}></span></td>
                                <td><{$row[2]|escape}></td>
                                <td><{$row[3]|regex_replace:'/(\d\d\d\d)(\d\d)(\d\d)/':'$1-$2-$3'|date_format_jp:"%Y年%-m月%-d日"}></td>
                                <td><{$row[4]|regex_replace:'/(\d\d\d\d)(\d\d)(\d\d)/':'$1-$2-$3'|date_format_jp:"%Y年%-m月%-d日"}></td>
                                <td><{$row[5]|escape}>年<{$row[6]|escape}>ヶ月</td>
                                <td align="center"><{if $row[19]}>◯<{else}>-<{/if}></td>
                                <td align="center"><{$row[18]|default:'0年'|escape}></td>

                                <td align="right"><b><{$row[7]|default:0|escape}></b></td>

                                <td align="right"><{$row[8]|default:0|escape}> (</td>
                                <td align="right"><{$row[9]|default:0|escape}> +</td>
                                <td align="right"><{$row[10]|default:0|escape}> +</td>
                                <td align="right"><{$row[11]|default:0|escape}> ) -</td>

                                <td align="right"><{$row[12]|string_format:'%.2f'}> (</td>
                                <td align="right"><{$row[13]|default:0|escape}> +</td>
                                <td align="right"><{$row[14]|string_format:'%.2f'}> ) =</td>

                                <td align="right"><{$row[15]|default:0|escape}></td>

                                <td align="right"><{$row[16]|default:0|escape}></td>

                                <td align="right"><b><{$row[17]|default:0|escape}></b></td>
                            </tr>
                        <{/foreach}>
                    </tbody>
                </table>

                <button type="submit" name="mode" value="csv">この内容をCSVでダウンロードする</button>
                <button type="submit" name="mode" value="update">この内容で付与する</button>
            <{else}>
                <div class="alert">対象者：なし</div>

            <{/if}>
        <{/if}>
    </form>
</div>

<{include file="shift/footer.tpl"}>
