<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="content-script-type" content="text/javascript" />
  <title>CoMedix <{$INCIDENT_TITLE}>｜担当者設定確認</title>
  <meta http-equiv="imagetoolbar" content="no" />
  <link type="text/css" rel="stylesheet" media="all" href="css/default.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/common.css" />
  <link type="text/css" rel="stylesheet" media="all" href="css/sub_form.css" />
  <script type="text/javascript">
    var w = 1024 + 37;
    var h = window.screen.availHeight;
    window.resizeTo(w, h);
  </script>
  <!--[if IE]>
    <script type="text/javascript" src="js/ie-radius.js"></script>
  <![endif]-->
</head>

<body id="charge_disp">
  <!-------------------------------------------------------------------------
  ヘッダー 
  -------------------------------------------------------------------------->
	<div id="header" class="clearfix">
		<div class="inner" class="clearfix">
			<h1>担当者設定確認</h1>
		</div>
	</div>

  <!-------------------------------------------------------------------------
  コンテンツ
  -------------------------------------------------------------------------->
  <div id="content" class="clearfix">
    <div class="section">
      <h1><{$sel_auth_name}></h1>

      <!-- 決裁者の設定 -->
      <h2 class="type02">決裁者の設定</h2>
      <div class="color02 radius">
        <table class="v_title">
          <{if $auth == 'RM' || $auth == 'RA' || $auth == 'SD' || $auth == 'MD'}>
            <tr>
              <th width="20%" rowspan="2" <{if $auth == 'SD'}>class="none"<{/if}>>
                部署・役職
              </th>
              <th width="40%" class="none">
                部署
              </th>
              <th width="40%" class="none">
                役職
              </th>
            </tr>
            <tr>
              <{if $decide_use_flg == 't'}>
                <td style="border-right: 2px dotted #cef2f6;" <{if $auth == 'SD'}>class="none"<{/if}>>
                  申請者の所属する【<{$decide_class}>】
                </td>
                <td class="last<{if $auth == 'SD'}> none<{/if}>">
                  <{foreach name=n from=$decide_posts item=post}>
                    <{if !$smarty.foreach.n.first}><br><{/if}>
                    <{$post}>
                  <{/foreach}>
                </td>
              <{elseif $decide_use_flg == 'f'}>
                <td colspan="2"<{if $auth == 'SD'}>class="none"<{/if}>>
                  指定しない
                </td>
              <{/if}>
            </tr>
          <{/if}>
          
          <{if $auth != 'SD'}>
            <tr>
              <th width="20%" class="none">
                職員
              </th>
              <td colspan="2" class="none">
                <{$decide_target}>
              </td>
            </tr>
          <{/if}>
        </table>
      </div>

        
      <!-- 審議者の設定 -->
      <{if $auth == 'SM' || $auth == 'RM' || $auth == 'RA' || $auth == 'SD'}>
        <h2 class="type02">審議者の設定</h2>
        <div class="color02 radius">
          <table class="v_title">
            <{if $auth == 'RM' || $auth == 'RA' || $auth == 'SD'}>
              <tr>
                <th width="20%" rowspan="2" <{if $auth == 'SD'}>class="none"<{/if}>>
                  部署・役職
                </th>
                <th width="40%" class="none">
                  部署
                </th>
                <th width="40%" class="none">
                  役職
                </th>
              </tr>
          
              <tr>
                <{if $discuss_use_flg == 't'}>
                  <td style="border-right: 2px dotted #cef2f6;" <{if $auth == 'SD'}>class="none"<{/if}>>
                    申請者の所属する【<{$discuss_class}>】
                  </td>
                  <td class="last <{if $auth == 'SD'}>none<{/if}>">
                    <{foreach name=n from=$discuss_posts item=post}>
                      <{if !$smarty.foreach.n.first}><br><{/if}>
                      <{$post}>
                    <{/foreach}>
                  </td>
                <{elseif $discuss_use_flg == 'f'}>
                  <td colspan="2">
                    指定しない
                  </td>
                <{/if}>
              </tr>
            <{/if}>
            
            <{if $auth != 'SD'}>
              <tr>
                <th width="20%" class="none">
                  職員
                </th>
                <td colspan="2" class="none">
                  <{$discuss_target}>
                </td>
              </tr>
            <{/if}>
          </table>
        </div>
      <{/if}>
    
    </div>
  </div>
</body>
</html>
