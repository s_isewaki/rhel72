<script type="text/javascript">
//報告ファイルの日付種類を変更し、日付検索を実行
function change_search_date_target(search_date_target)
{
    //ポストバック
    document.list_form.search_date_target.value = search_date_target;
    document.list_form.page.value=1;
    if(search_date_target == "report_date"){
        var sort_item_value = "REGIST_DATE";
    }
    if(search_date_target == "evalutioin_date"){
        var sort_item_value = "EVALUATION_DATE";
    }
    if(search_date_target == "incident_date"){
        var sort_item_value = "INCIDENT_DATE";
    }
    document.list_form.sort_item.value = search_date_target;
    document.list_form.sort_div.value = "0";//昇順
    document.list_form.submit();
}

//年を変更し、日付検索(月指定)を実行します。
function change_cal_year(cur_date_year, cur_date_month, add_year)
{
    //変更後の年
    var new_year = cur_date_year + add_year;

    //チェック
    if(new_year < 0 || new_year > 9999){
        return;
    }

    //ポストバック
    document.list_form.search_date_mode.value="month";
    document.list_form.search_date_year.value=new_year;
    document.list_form.search_date_month.value=cur_date_month;
    document.list_form.page.value=1;
    document.list_form.submit();
}

//月を変更し、日付検索(月指定)を実行します。
function change_cal_month(cur_date_year, month)
{
    //前0付加
    if(month <= 9){
        month = "0" + month;
    }

    //ポストバック
    document.list_form.search_date_mode.value="month";
    document.list_form.search_date_year.value=cur_date_year;
    document.list_form.search_date_month.value=month;
    document.list_form.page.value=1;
    document.list_form.submit();
}

//カレンダーの月の色を選択色にします。
function changeCalMonthColor(month_td_obj)
{
    month_td_obj.style.backgroundColor = '#FFDDFD';
}

//カレンダーの月の色を非選択色にします。
function resetCalMonthColor(month_td_obj)
{
    month_td_obj.style.backgroundColor = '#E5F8E1';
}
</script>

<{* 年月指定 *}>
<div class="color01 radius">
  <h2><img src="img/side_nav_txt03.gif" alt="CALENDAR" /></h2>
  <ul id="sub_calendar">
    <li>
      <ul class="clearfix">
        <li class="back"><a href="#" onclick="change_cal_year(<{$search_date_year}>,'<{$search_date_month}>',-1);">＜</a></li>
        <li class="year" id="torey_cal_year_disp"><{$search_date_year}></li>
        <li class="next"><a href="#" onclick="change_cal_year(<{$search_date_year}>,'<{$search_date_month}>',1);">＞</a></li>
      </ul>
    </li>
    <li>
      <table class="h_title radius_mini">
        <{section loop=13 name=m start=1}>
          <{if $smarty.section.m.index==1 || $smarty.section.m.index==5 || $smarty.section.m.index==9}>
            <tr>
          <{/if}>
          <td class="<{if $smarty.section.m.index < 5}>lineT<{/if}><{if $smarty.section.m.index > 8}> lineB<{/if}><{if $smarty.section.m.index == 1 || $smarty.section.m.index==5 || $smarty.section.m.index==9}> lineL<{/if}><{if $smarty.section.m.index==4 || $smarty.section.m.index==8 || $smarty.section.m.index==12}> lineR<{/if}>">
            <{if $search_date_mode == "month" && $search_date_month == $smarty.section.m.index}>
              <a href="#" class="active" onclick="change_cal_month(<{$search_date_year}>, <{$smarty.section.m.index}>);">
            <{else}>            
              <a href="#" onmouseover="changeCalMonthColor(this);" onmouseout="resetCalMonthColor(this);" onclick="change_cal_month(<{$search_date_year}>, <{$smarty.section.m.index}>);">
            <{/if}>
              <{$smarty.section.m.index}>月
            </a>
          </td>
          <{if $smarty.section.m.index==4 || $smarty.section.m.index==8 || $smarty.section.m.index==12}>
            </tr>
          <{/if}>         
        <{/section}>
      </table>
    </li>
  </ul>

  <{* 報告ファイルの日付種類指定 *}>
  <{if $file == "hiyari_rpt_report_classification.php"}>
    <div id="sub_date_type">
      <select name="search_date_target" onchange="change_search_date_target(this.value)">
        <option value="report_date"     <{if $search_date_target == "report_date"    }>selected<{/if}> >報告日</option>
        <option value="incident_date"   <{if $search_date_target == "incident_date"  }>selected<{/if}> >発生日</option>
         <{if $predetermined_eval == 't'}>
        <option value="evaluation_date" <{if $search_date_target == "evaluation_date"}>selected<{/if}> >評価予定日</option>
       <{/if}>
      </select>
    </div>    
  <{/if}>
</div>