<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title>CoMedix <{$INCIDENT_TITLE}> | <{$page_title}></title>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <{if $imgfile_flg}>
    <script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
    <script type="text/javascript">
      window.onload = function() {
        var ret = Element.getDimensions($('table'));
        window.resizeTo(ret.width+50,ret.height+150);
      }
    </script>
  <{/if}>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <style type="text/css">
    .list {border-collapse:collapse;}
    .list td {border:#35B341 solid 1px;}
    p.attach {margin:0;}
  </style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<center>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr height="32" bgcolor="#35B341">
        <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>お知らせ</b></font></td>
        <td>&nbsp</td>
        <td width="10">&nbsp</td>
        <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();return false;"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
      </tr>
    </table>
    <img src="img/spacer.gif" width="10" height="10" alt=""><br>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" id="table">
    <tr>
      <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
      <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
      <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
    </tr>
    <tr>
      <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
      <td>
        <!-- 参照確認 START -->
        <{if $record_flg == "t"}>
          <table width="100%" border="0" cellspacing="0" cellpadding="1">
            <tr height="22">
              <td align="right" bgcolor="#F5FFE5">
                <{if $referred_flg}>
                  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16" color="gray">
                    内容を確認した方は確認ボタンをクリックしてください。
                  </font>
                  <input type="button" value="確認済み" style="font-size:1.1em;" disabled>
                <{else}>
                  <form action="hiyari_news_refer.php" method="post">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">
                      内容を確認した方は確認ボタンをクリックしてください。
                    </font>
                    <input type="submit" value="確認" style="font-size:1.1em;">
                    <input type="hidden" name="session" value="<{$session}>">
                    <input type="hidden" name="news_id" value="<{$news_id}>">
                  </form>
                <{/if}>
              </td>
            </tr>
          </table>
        <{/if}>
        <!-- 参照確認 END -->
        
        <!-- お知らせ詳細 START -->
        <table width="100%" border="0" cellspacing="0" cellpadding="4" class="list">
          <tr height="22">
            <td align="right" bgcolor="#DFFFDC" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">タイトル</font></td>
            <td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><{$news_title}></font></td>
          </tr>
          <tr height="22">
            <td align="right" bgcolor="#DFFFDC" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">カテゴリ</font></td>
            <td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><{$news_category}></font></td>
          </tr>
          <tr height="22">
            <td align="right" bgcolor="#DFFFDC" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">内容</font></td>
            <td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><{$news}></font></td>
          </tr>
          <tr height="22">
            <td align="right" bgcolor="#DFFFDC" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">添付ファイル</font></td>
            <td colspan="3">
              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">
                <{foreach from=$file_list item=file}>
                  <p id="p_<{$file.newsfile_noe}>" class="attach">
                    <a href="<{$file.src}>"><{$file.newsfile_name}></a>
                  </p>
                <{/foreach}>
              </font>
            </td>
          </tr>
          <{if $imgfile_flg}>
            <tr height="22">
              <td align="right" bgcolor="#DFFFDC" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">画像</font></td>
              <td colspan="3">
                <{foreach from=$file_list item=file}>
                  <{if $file.is_img}>
                    <img src="<{$file.src}>" alt="<{$file.newsfile_name}>" title="<{$file.newsfile_name}>" style="padding:3px;border:0;" /><br />
                  <{/if}>
                <{/foreach}>
              </td>
            </tr>
          <{/if}>
          <tr height="22">
            <td align="right" bgcolor="#DFFFDC" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">掲載期間</font></td>
            <td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><{$news_begin}>&nbsp;〜&nbsp;<{$news_end}></font></td>
          </tr>
          <tr height="22">
            <td align="right" bgcolor="#DFFFDC" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">登録者</font></td>
            <td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><{$emp_lt_nm}> <{$emp_ft_nm}></font></td>
          </tr>
          <tr height="22">
            <td align="right" bgcolor="#DFFFDC" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">登録日</font></td>
            <td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><{$news_date}></font></td>
          </tr>
        </table>
        <!-- お知らせ詳細 END -->
      </td>
      <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    </tr>
    <tr>
      <td><img src="img/r_3.gif" width="10" height="10"></td>
      <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
      <td><img src="img/r_4.gif" width="10" height="10"></td>
    </tr>
  </table>
</center>
</body>
</html>
