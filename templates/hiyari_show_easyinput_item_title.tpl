<{*
インターフェース

    $i
        string  グループコード
    $j
        string  アイテムコード
    $grps
        array   項目情報配列
*}>
<font size="3" class="j12">
<{if $i==125
  || $i==120
  || $i==520
  || $i==525
  || $i==530
  || $i==96
  || $i==610
  || $i==630
  || $i==900
  || $i==250
  || $i==290
  || $i==295
  || $i==3300
  || $i==240
  || $i==130
  || $i==1400 && $j==10
  || $i==1410 && $j==10
  || $i==1500 && $j==10
  || $i==1502 && $j==10
  || $i==1504 && $j==10
  || $i==1510 && $j==10
  || $i==1520 && $j==10
  || $i==1530 && $j==10
  || $i>=10000}>
    <{if $eis_item_title[$i]}>
        <{$eis_item_title[$i]|escape}><{if ($i==125 && $j==86) || ($i==290 && $j==130)}>その他<{/if}>
    <{else}>
        <{$grps[$i].easy_item_list[$j].easy_item_name}>
    <{/if}>
<{else}>
    <{$grps[$i].easy_item_list[$j].easy_item_name}>
<{/if}>
</font>
