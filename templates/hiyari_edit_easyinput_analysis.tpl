<{*
インターフェース
//  $grps
//      array   項目情報配列
    $vals
        array   値の配列
//  $grp_flags
//      array   レポートに使用する項目グループ
    $level_infos
        array   インシレベルとメッセージの配列

    $session
        string  セッションID

使用するサブテンプレート
    hiyari_edit_easyinput_item.tpl
*}>


<!-- 分析・再発防止用入力エリア START -->
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td width = "30%" valign="top">


<!-- 報告内容 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top">
<div style="height:550px; overflow: scroll;border:#CCCCCC solid 1px;">


<!-- 報告内容内容 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>報告内容</b></font></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        <td valign="top">
        <{php}>

        require_once("hiyari_mail.ini");
        //TODO eis_idが空の場合の対応
        print get_report_html($this->_tpl_vars['con'],$this->_tpl_vars['fname'],$this->_tpl_vars['report_id'],"","",$this->_tpl_vars['session'],$this->_tpl_vars['eis_title_id']);
        <{/php}>
        </td>
        </tr>
        </table>

    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<!-- 報告内容内容 END -->


</div>
</td>
</tr>
</table>
<!-- 報告内容 END -->


</td>
<td valign="top">


<!-- 分析項目入力 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top">
<div style="height:550px; overflow: scroll;border:#CCCCCC solid 1px;">


<!-- 確認コメント START -->
<{if isset($progress_comment_data)}>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>確認コメント</b></font></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>
      <{foreach from=$progress_comment_data item="auth" name="n1"}>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr bgcolor="#F5FFE5">
            <td>
              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                <b>
                  <{$auth.auth_name|escape:"html"}>
                </b>
              </font>
            </td>
          </tr>
        </table>

        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
          <tr>
            <td bgcolor="#DFFFDC" width="80">
              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                入力日付
              </font>
            </td>
            <td bgcolor="#DFFFDC" width="120">
              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                担当者
              </font>
            </td>
            <td bgcolor="#DFFFDC">
              <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                コメント
              </font>
            </td>
          </tr>
          <{foreach from=$auth.data item="row"}>
            <tr>
              <td>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                  <{$row.date|escape:"html"}>
                </font>
              </td>
              <td>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                  <{$row.emp_name|escape:"html"}>
                </font>
              </td>
              <td>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                  <{$row.comment|escape:"html"|nl2br}>
                </font>
              </td>
            </tr>
          <{/foreach}>
        </table>
        <{if !$smarty.foreach.n1.last}>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
          </table>
        <{/if}>
      <{/foreach}>
    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
</table>
<{/if}>
<!-- 確認コメント END -->



<!-- 分析項目入力内容 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>インシデントの分析</b></font></td></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    <td>
      <{if in_array(1100, $grp_flags)}>
        <!-- 発生件数情報 -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr bgcolor="#F5FFE5"><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>医療事故情報収集事業の発生件数情報等</b></font></td></tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
            <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
          </tr>
          <tr>
            <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                  <td width=250 bgcolor="#DFFFDC">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <span <{if in_array('1100'|cat:'_'|cat:'10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>医療の実施の有無<{if in_array('1100'|cat:'_'|cat:'10', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></span>
                    </font>
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font>
                  </td>
                  <td class="item_selection">
                    <span class="item_set">
                      <input type="radio" id="id_1100_10_1" name="_1100_10[]" value="実施あり"
                        onclick="document.getElementById('info_1').style.display='';document.getElementById('info_2').style.display='none';check_unset_1100_1();"
                        <{if $vals[1100][10][0] == '実施あり'}>checked<{/if}>>
                      <label for="id_1100_10_1">
                        <font class="j12">実施あり</font>
                      </label>
                    </span>
                    <span class="item_set">
                      <input type="radio" id="id_1100_10_2" name="_1100_10[]" value="実施なし"
                        onclick="document.getElementById('info_2').style.display='';document.getElementById('info_1').style.display='none';check_unset_1100_2();"
                        <{if $vals[1100][10][0] == '実施なし'}>checked<{/if}>>
                      <label for="id_1100_10_2">
                        <font class="j12">実施なし</font>
                      </label>
                    </span>
                  </td>
                  <td class="item_btn">
                    <a href="javascript:void(0);" onclick="clear_item('radio',1100,10);">
                      <font class="j12">[クリア]</font>
                    </a>
                  </td>
                </tr>
              </table>

              <div id="info_1" style="display:none">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                  <tr height="22">
                    <td width=250 bgcolor="#DFFFDC">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                      <span>実施の程度</span>
                      </font>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font>
                    </td>
                    <td class="item_selection">
                      <span class="item_set">
                        <input type='radio' id="id_1100_20_1" name="_1100_20[]" value="濃厚な治療"
                        <{if $vals[1100][20][0] == '濃厚な治療'}>checked<{/if}>>
                        <label for="id_1100_20_1"><font class="j12">濃厚な治療</font></label>
                      </span>
                      <br>
                      <span class="item_set">
                        <input type='radio' id="id_1100_20_2" name="_1100_20[]" value="軽微な治療"
                        <{if $vals[1100][20][0] == '軽微な治療'}>checked<{/if}>>
                        <label for="id_1100_20_2"><font class="j12">軽微な治療</font></label>
                      </span>
                      <br>
                      <span class="item_set">
                        <input type='radio' id="id_1100_20_3" name="_1100_20[]" value="なし"
                        <{if $vals[1100][20][0] == 'なし'}>checked<{/if}>>
                        <label for="id_1100_20_3"><font class="j12">なし</font></label>
                      </span>
                      <br>
                      <span class="item_set">
                        <input type='radio' id="id_1100_20_4" name="_1100_20[]" value="不明"
                        <{if $vals[1100][20][0] == '不明'}>checked<{/if}>>
                        <label for="id_1100_20_4"><font class="j12">不明</font></label>
                      </span>
                    </td>
                    <td class="item_btn">
                      <a href="javascript:void(0);" onclick="clear_item('radio',1100,20);">
                        <font class="j12">[クリア]</font>
                      </a>
                    </td>
                  </tr>
                  <tr height="22">
                    <td width=250 bgcolor="#DFFFDC">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                      <span>事故の程度</span>
                      </font>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font>
                    </td>
                    <td class="item_selection">
                      <span class="item_set">
                        <input type='radio' id="id_1100_30_1" name="_1100_30[]" value="死亡"
                        onclick="check_unset_1100_3()"
                        <{if $vals[1100][30][0] == '死亡'}>checked<{/if}>>
                        <label for="id_1100_30_1"><font class="j12">死亡</font></label>
                      </span>
                      <br>
                      <span class="item_set">
                        <input type='radio' id="id_1100_30_2" name="_1100_30[]" value="障害残存の可能性がある(高い)"
                        onclick="check_unset_1100_3()"
                        <{if $vals[1100][30][0] == '障害残存の可能性がある(高い)'}>checked<{/if}>>
                        <label for="id_1100_30_2"><font class="j12">障害残存の可能性がある(高い)</font></label>
                      </span>
                      <br>
                      <span class="item_set">
                        <input type='radio' id="id_1100_30_3" name="_1100_30[]" value="障害残存の可能性がある(低い)"
                        onclick="check_unset_1100_3()"
                        <{if $vals[1100][30][0] == '障害残存の可能性がある(低い)'}>checked<{/if}>>
                        <label for="id_1100_30_3"><font class="j12">障害残存の可能性がある(低い)</font></label>
                      </span>
                      <br>
                      <span class="item_set">
                        <input type='radio' id="id_1100_30_4" name="_1100_30[]" value="障害残存の可能性なし"
                        onclick="check_unset_1100_3()"
                        <{if $vals[1100][30][0] == '障害残存の可能性なし'}>checked<{/if}>>
                        <label for="id_1100_30_4"><font class="j12">障害残存の可能性なし</font></label>
                      </span>
                      <br>
                      <span class="item_set">
                        <input type='radio' id="id_1100_30_5" name="_1100_30[]" value="障害なし"
                        onclick="check_unset_1100_3()"
                        <{if $vals[1100][30][0] == '障害なし'}>checked<{/if}>>
                        <label for="id_1100_30_5"><font class="j12">障害なし</font></label>
                      </span>
                      <br>
                      <span class="item_set">
                        <input type='radio' id="id_1100_30_6" name="_1100_30[]" value="不明"
                        <{if $vals[1100][30][0] == '不明'}>checked<{/if}>>
                        <label for="id_1100_30_6"><font class="j12">不明</font></label>
                        (
                        <input type="text" id="id_1100_40" name="_1100_40" size="60" value="<{$vals[1100][40][0]}>" style="ime-mode:active">
                        )
                      </span>
                    </td>
                    <td class="item_btn">
                      <a href="javascript:void(0);" onclick="clear_item('radio',1100,30)">
                        <font class="j12">[クリア]</font>
                      </a>
                    </td>
                  </tr>
                </table>
              </div>

              <div id="info_2" style="display:none">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                  <tr height="22">
                    <td width=250 bgcolor="#DFFFDC">
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                      <span>仮に実施された場合の影響度</span>
                      </font>
                      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font>
                    </td>
                    <td class="item_selection">
                      <span class="item_set">
                        <input type="radio" id="id_1100_50_1" name="_1100_50[]" value="死亡もしくは重篤な状況に至ったと考えられる"
                        <{if $vals[1100][50][0] == '死亡もしくは重篤な状況に至ったと考えられる'}>checked<{/if}> >
                        <label for="id_1100_50_1">
                          <font class="j12">死亡もしくは重篤な状況に至ったと考えられる</font>
                        </label>
                      </span>
                      <br>
                      <span class="item_set">
                        <input type="radio" id="id_1100_50_2" name="_1100_50[]" value="濃厚な処置・治療が必要であると考えられる"
                        <{if $vals[1100][50][0] == '濃厚な処置・治療が必要であると考えられる'}>checked<{/if}> >
                        <label for="id_1100_50_2">
                          <font class="j12">濃厚な処置・治療が必要であると考えられる</font>
                        </label>
                      </span>
                      <br>
                      <span class="item_set">
                        <input type="radio" id="id_1100_50_3" name="_1100_50[]" value="軽微な処置・治療が必要もしくは処置・治療が不要と考えられる"
                        <{if $vals[1100][50][0] == '軽微な処置・治療が必要もしくは処置・治療が不要と考えられる'}>checked<{/if}> >
                        <label for="id_1100_50_3">
                          <font class="j12">軽微な処置・治療が必要もしくは処置・治療が不要と考えられる</font>
                        </label>
                      </span>
                    </td>
                    <td class="item_btn">
                      <a href="javascript:void(0);" onclick="clear_item('radio',1100,50);">
                        <font class="j12">[クリア]</font>
                      </a>
                    </td>
                  </tr>
                </table>
              </div>

              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
              </table>

              <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                  <td width=250 bgcolor="#DFFFDC">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <span <{if in_array('1110'|cat:'_'|cat:'10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>特に報告を求める事例<{if in_array('1110'|cat:'_'|cat:'10', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></span>
                    </font>
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font>
                  </td>
                  <td class="item_selection">
                    <span class="item_set">
                      <input type="radio" id="id_1110_10_0" name="_1110_10[]" value='院内感染による死亡や障害'
                      onclick="clear_item('text', 1110, 20);"
                      <{if $vals[1110][10][0] == '院内感染による死亡や障害'}> checked<{/if}>>
                      <label for="id_1110_10_0">
                        <font class="j12">院内感染による死亡や障害</font>
                      </label>
                    </span>
                    <br>
                    <span class="item_set">
                      <input type="radio" id="id_1110_10_1" name="_1110_10[]" value='患者の自殺又は自殺企図'
                      onclick="clear_item('text', 1110, 20);"
                      <{if $vals[1110][10][0] == '患者の自殺又は自殺企図'}> checked<{/if}>>
                      <label for="id_1110_10_1">
                        <font class="j12">患者の自殺又は自殺企図</font>
                      </label>
                    </span>
                    <br>
                    <span class="item_set">
                      <input type="radio" id="id_1110_10_2" name="_1110_10[]" value='入院患者の失踪'
                      onclick="clear_item('text', 1110, 20);"
                      <{if $vals[1110][10][0] == '入院患者の失踪'}> checked<{/if}>>
                      <label for="id_1110_10_2">
                        <font class="j12">入院患者の失踪</font>
                      </label>
                    </span>
                    <br>
                    <span class="item_set">
                      <input type="radio" id="id_1110_10_3" name="_1110_10[]" value='入院中の熱傷'
                      onclick="clear_item('text', 1110, 20);"
                      <{if $vals[1110][10][0] == '入院中の熱傷'}> checked<{/if}>>
                      <label for="id_1110_10_3">
                        <font class="j12">入院中の熱傷</font>
                      </label>
                    </span>
                    <br>
                    <span class="item_set">
                      <input type="radio" id="id_1110_10_4" name="_1110_10[]" value='入院中の感電'
                      onclick="clear_item('text', 1110, 20);"
                      <{if $vals[1110][10][0] == '入院中の感電'}> checked<{/if}>>
                      <label for="id_1110_10_4">
                        <font class="j12">入院中の感電</font>
                      </label>
                    </span>
                    <br>
                    <span class="item_set">
                      <input type="radio" id="id_1110_10_5" name="_1110_10[]" value='医療施設内の火災による患者の死亡や障害'
                      onclick="clear_item('text', 1110, 20);"
                      <{if $vals[1110][10][0] == '医療施設内の火災による患者の死亡や障害'}> checked<{/if}>>
                      <label for="id_1110_10_5">
                        <font class="j12">医療施設内の火災による患者の死亡や障害</font>
                      </label>
                    </span>
                    <br>
                    <span class="item_set">
                      <input type="radio" id="id_1110_10_6" name="_1110_10[]" value='間違った保護者の許への新生児の引渡し'
                      onclick="clear_item('text', 1110, 20);"
                      <{if $vals[1110][10][0] == '間違った保護者の許への新生児の引渡し'}> checked<{/if}>>
                      <label for="id_1110_10_6">
                        <font class="j12">間違った保護者の許への新生児の引渡し</font>
                      </label>
                    </span>
                    <br>
                    <span class="item_set">
                      <input type="radio" id="id_1110_10_7" name="_1110_10[]" value='本事例は選択肢には該当しない（その他）'
                      <{if $vals[1110][10][0] == '本事例は選択肢には該当しない（その他）'}> checked<{/if}>>
                      <label for="id_1110_10_7">
                        <font class="j12">本事例は選択肢には該当しない（その他）</font>
                      </label>
                    </span>
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <span class="item_set">
                      (
                      <input type="text" id="id_1110_20" name="_1110_20" value="<{$vals[1110][20][0]}>" size="60" style="ime-mode:active">
                      )
                    </span>
                  </td>
                  <td class="item_btn">
                    <a href="javascript:void(0);" onclick="clear_item('radio',1110,10);">
                      <font class="j12">[クリア]</font>
                    </a>
                  </td>
                </tr>
              </table>

              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
              </table>

              <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                  <td width=250 bgcolor="#DFFFDC">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <span <{if in_array('1120'|cat:'_'|cat:'10', $item_must)}><{if $must_item_disp_mode == 2}>style="background-color:#FFDDFD"<{/if}><{/if}>>発生件数集計再掲<{if in_array('1120'|cat:'_'|cat:'10', $item_must)}><{if $must_item_disp_mode == 1}><span style="color:red">＊</span><{/if}><{/if}></span>
                    </font>
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font>
                  </td>
                  <td class="item_selection">
                    <label for="id_1120_10_1">
                      <input type='radio' id="id_1120_10_1" name="_1120_10[]" value="薬剤の名称や形状に関連する事例" onclick="radio_onclick(this)" <{if $vals[1120][10][0] == '薬剤の名称や形状に関連する事例'}>checked<{/if}>>
                      <font class="j12">薬剤の名称や形状に関連する事例</font>
                    </label>
                    <br>
                    <label for="id_1120_10_2">
                      <input type='radio' id="id_1120_10_2" name="_1120_10[]" value="薬剤に由来する事例" onclick="radio_onclick(this)" <{if $vals[1120][10][0] == '薬剤に由来する事例'}>checked<{/if}>>
                      <font class="j12">薬剤に由来する事例</font>
                    </label>
                    <br>
                    <label for="id_1120_10_3">
                      <input type='radio' id="id_1120_10_3" name="_1120_10[]" value="医療機器等に由来する事例" onclick="radio_onclick(this)" <{if $vals[1120][10][0] == '医療機器等に由来する事例'}>checked<{/if}>>
                      <font class="j12">医療機器等に由来する事例</font>
                    </label>
                    <br>
                    <label for="id_1120_10_4">
                      <input type='radio' id="id_1120_10_4" name="_1120_10[]" value="今期のテーマ(<{$theme}>)" onclick="radio_onclick(this)" <{if ereg("^今期のテーマ", $vals[1120][10][0])}>checked<{/if}>>
                      <font class="j12">今期のテーマ(<{$theme}>)</font>
                    </label>
                  </td>
                  <td class="item_btn">
                    <a href="javascript:void(0);" onclick="clear_item('radio',1120,10);">
                      <font class="j12">[クリア]</font>
                    </a>
                  </td>
                </tr>
              </table>
            </td>
            <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
          </tr>
          <tr>
            <td><img src="img/r_3.gif" width="10" height="10"></td>
            <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
            <td><img src="img/r_4.gif" width="10" height="10"></td>
          </tr>
        </table>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
        </table>
      <{/if}>

      <{if in_array(125, $grp_flags)}>
        <!-- 分類 -->
        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
            <{include file="hiyari_edit_easyinput_item.tpl" i=125 j=85 tr=true}>
        </table>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
        </table>
      <{/if}>
      
      <{if in_array(650, $grp_flags)}>
        <!-- 警鐘事例 -->
        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
            <{include file="hiyari_edit_easyinput_item.tpl" i=650 j=50 tr=true}>
        </table>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
        </table>
      <{/if}>


      <{if in_array(90, $grp_flags) || in_array(96, $grp_flags)}>
        <!-- 影響レベル -->
        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
          <{if in_array(90, $grp_flags)}>
            <tr height="22">
                <{include file="hiyari_edit_easyinput_item.tpl" i=90 j=10 tr=false}>
            </tr>
          <{/if}>
          <{if in_array(96, $grp_flags)}>
            <tr height="22">
                <{include file="hiyari_edit_easyinput_item.tpl" i=96 j=20 tr=false}>
            </tr>
          <{/if}>
        </table>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
        </table>
      <{/if}>

      <{* 1500: リスクの評価 *}>
      <{if in_array(1500, $grp_flags) || in_array(1502, $grp_flags) || in_array(1504, $grp_flags) || in_array(1510, $grp_flags) || in_array(1520, $grp_flags) || in_array(1530, $grp_flags)}>
        <!-- リスクの評価 -->
        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
          <{if in_array(1500, $grp_flags)}>
            <tr height="22">
                <{include file="hiyari_edit_easyinput_item.tpl" i=1500 j=10 tr=false}>
            </tr>
          <{/if}>
          <{if in_array(1502, $grp_flags)}>
            <tr height="22">
                <{include file="hiyari_edit_easyinput_item.tpl" i=1502 j=10 tr=false}>
            </tr>
          <{/if}>
          <{if in_array(1504, $grp_flags)}>
            <tr height="22">
                <{include file="hiyari_edit_easyinput_item.tpl" i=1504 j=10 tr=false}>
            </tr>
          <{/if}>
          <{if in_array(1510, $grp_flags)}>
            <tr height="22">
                <{include file="hiyari_edit_easyinput_item.tpl" i=1510 j=10 tr=false}>
            </tr>
          <{/if}>
          <{if in_array(1520, $grp_flags)}>
            <tr height="22">
                <{include file="hiyari_edit_easyinput_item.tpl" i=1520 j=10 tr=false}>
            </tr>
          <{/if}>
          <{if in_array(1530, $grp_flags)}>
            <tr height="22">
                <{include file="hiyari_edit_easyinput_item.tpl" i=1530 j=10 tr=false}>
            </tr>
          <{/if}>
        </table>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
        </table>
      <{/if}>

      <{if in_array(600, $grp_flags)}>
        <!-- 発生要因 -->
        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
            <tr>
                <td bgcolor="#DFFFDC" width="250">
                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=600 j=10}>
                </td>
                <td valign="top">
                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=600 j=10 checkbox_line_count=2}>
                </td>
            </tr>
        </table>

        <{if in_array(605, $grp_flags)}>
            <{foreach from=$grps[605].easy_item_list key="easy_item_code_605" item="tmp_605_data"}>
                <div id="area_605_<{$easy_item_code_605}>" style="display:none">
                    <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                        <tr height="22">
                            <td width=250 bgcolor="#DFFFDC" style="border-top:0px;">
                                <{include file="hiyari_edit_easyinput_item_title.tpl" i=605 j=$easy_item_code_605}>
                            </td>
                            <td style="border-top:0px;">
                                <{include file="hiyari_edit_easyinput_item_field.tpl" i=605 j=$easy_item_code_605 checkbox_line_count=2}>
                            </td>
                        </tr>
                    </table>
                </div>
            <{/foreach}>
        <{/if}>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
        </table>
      <{/if}>

      <{if in_array(510, $grp_flags)}>
        <!-- 警鐘事例 -->
        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
            <{include file="hiyari_edit_easyinput_item.tpl" i=510 j=20 tr=true}>
        </table>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
        </table>
      <{/if}>

      <{if in_array(590, $grp_flags)}>
        <!-- 事故調査委員会の設置の有無 -->
        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
            <{include file="hiyari_edit_easyinput_item.tpl" i=590 j=90 tr=true}>
            <{include file="hiyari_edit_easyinput_item.tpl" i=590 j=91 tr=true}>
        </table>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr bgcolor="#F5FFE5"><td><img src="img/spacer.gif" width="1" height="10" alt=""></td></tr>
        </table>
      <{/if}>


      <{if array_intersect(array(610,620,625,630,635,690), $grp_flags)}>
        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
          <{if in_array(610, $grp_flags)}>
            <!-- 背景・要因 -->
            <tr>
                <td bgcolor="#DFFFDC" width="200">
                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=610 j=20 style=big}>
                </td>
                <td valign="top">
                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=610 j=20 style=big}>
                </td>
            </tr>
          <{/if}>

          <{if in_array(620, $grp_flags)}>
            <!-- 改善策 -->
            <tr>
                <td bgcolor="#DFFFDC" width="200">
                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=620 j=30 style=big}>
                </td>
                <td valign="top">
                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=620 j=30 style=kaizensaku}>
                    <nobr>
                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=620 j=33}>
                    <input type="text" id="_620_33" name="_620_33" value="<{$vals[620][33][0]}>" style="width:100;border:#35B341 solid 2px;" readonly>
                    <img id="caller_620_33" src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer;" onclick="window.open('hiyari_calendar.php?session=<{$session}>&caller=620_33&future=1', 'newwin', 'width=640,height=480,scrollbars=yes');"/>
                    <input type="button" value="クリア" onclick="clear_620_33()">
                    </nobr>
                </td>
            </tr>
          <{/if}>

          <{if in_array(625, $grp_flags)}>
            <!-- 効果の確認 -->
            <tr>
                <td bgcolor="#DFFFDC" width="200">
                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=625 j=35 style=big}>
                </td>
                <td valign="top">
                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=625 j=35 style=big}>
                </td>
            </tr>
          <{/if}>

          <{if in_array(630, $grp_flags)}>
            <!-- 自由記載欄 -->
            <tr>
                <td bgcolor="#DFFFDC" width="200">
                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=630 j=40 style=big}>
                </td>
                <td valign="top">
                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=630 j=40 style=big}>
                </td>
            </tr>
          <{/if}>

          <{if in_array(635, $grp_flags)}>
            <!-- 備考 -->
            <tr>
                <td bgcolor="#DFFFDC" width="200">
                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=635 j=45 style=big}>
                </td>
                <td valign="top">
                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=635 j=45 style=big}>
                </td>
            </tr>
          <{/if}>

          <{if in_array(690, $grp_flags)}>
            <!-- 備考 -->
            <tr>
                <td bgcolor="#DFFFDC" width="200">
                    <{include file="hiyari_edit_easyinput_item_title.tpl" i=690 j=90 style=big}>
                </td>
                <td valign="top">
                    <{include file="hiyari_edit_easyinput_item_field.tpl" i=690 j=90 style=big}>
                </td>
            </tr>
          <{/if}>
        </table>
      <{/if}>
      <!-- 分析項目入力内容 END -->
    </td>
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>

</div>
</td>
</tr>
</table>
<!-- 分析項目入力 END -->

</td>
</tr>
</table>
