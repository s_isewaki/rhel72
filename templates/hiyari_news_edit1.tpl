<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
  <title><{$INCIDENT_TITLE}> | <{$PAGE_TITLE}></title>
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
  <script type="text/javascript" src="js/hiyari_news_edit.js"></script>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <style type="text/css">
    .list {border-collapse:collapse;}
    .list td {border:#35B341 solid 1px;}
    p.attach {margin:0;}
  </style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init_page('<{$mode}>', '<{$news_non_tag}>')">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <{include file="hiyari_header1.tpl"}>
      <img src="img/spacer.gif" width="1" height="2" alt=""><br>
      <table width="725" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
          <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
          <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
        </tr>
        <tr>
          <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
          <td bgcolor="#F5FFE5">
            <form name="mainform" action="<{$action_file}>" method="post">
              <table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
                <tr height="22">
                  <td width="140" align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
                  <td bgcolor="#FFFFFF"><input type="text" name="news_title" value="<{$news_title|escape}>" size="50" maxlength="100" style="ime-mode:active;"></td>
                  <td bgcolor="#f6f9ff" width="20%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ラインマーカー</font></td>
                  <td width="12%">
                    <select name="marker">
                      <option value="0" style="background-color:white;color:black;"   <{if $marker == 0}>selected<{/if}>>なし
                      <option value="1" style="background-color:red;color:white;"     <{if $marker == 1}>selected<{/if}>>赤
                      <option value="2" style="background-color:aqua;color:blue;"     <{if $marker == 2}>selected<{/if}>>青
                      <option value="3" style="background-color:yellow;color:blue;"   <{if $marker == 3}>selected<{/if}>>黄
                      <option value="4" style="background-color:lime;color:blue;"     <{if $marker == 4}>selected<{/if}>>緑
                      <option value="5" style="background-color:fuchsia;color:white;" <{if $marker == 5}>selected<{/if}>>ピンク
                    </select>
                  </td>
                </tr>
                <tr height="22">
                  <td align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ</font></td>
                  <td colspan="3" bgcolor="#FFFFFF">
                    <select name="news_category" onchange="showSubCategory();">
                      <{foreach from=$category_list key=id item=name}>
                        <option value="<{$id}>" <{if $id == $news_category}>selected<{/if}>><{$name}></option>
                      <{/foreach}>
                    </select>&nbsp;
                    <select name="class_id" style="display:none;">
                      <{foreach from=$class_list key=id item=name}>
                        <option value="<{$id}>" <{if $id == $class_id}>selected<{/if}>><{$name}></option>
                      <{/foreach}>
                    </select>
                    <select name="job_id" style="display:none;">
                      <{foreach from=$job_list key=id item=name}>
                        <option value="<{$id}>" <{if $id == $job_id}>selected<{/if}>><{$name}></option>
                      <{/foreach}>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font></td>
                  <td colspan="3" bgcolor="#FFFFFF"><textarea name="news" cols="40" rows="12" style="ime-mode:active;"><{$news|escape}></textarea></td>
                </tr>
                <tr height="22">
                  <td align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">添付ファイル</font></td>
                  <td colspan="3" bgcolor="#FFFFFF">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                      <div id="attach">
                        <{foreach from=$file_list key=id item=file}>
                          <p id="p_<{$id}>" class="attach">
                            <a href="<{$newsfile_folder_name}>/tmp/<{$session}>_<{$id}><{$file.ext}>" target="_blank"><{$file.name}></a>
                            <input type="button" id="btn_<{$id}>" name="btn_<{$id}>" value="削除" onclick="detachFile(event);">
                            <input type="hidden" name="filename[]" value="<{$file.name}>">
                            <input type="hidden" name="file_id[]" value="<{$id}>">
                          </p>
                        <{/foreach}>                    
                      </div>
                      <input type="button" value="追加" onclick="attachFile('<{$session}>');">
                    </font>
                  </td>
                </tr>
                <tr height="22">
                  <td align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照者を記録する</font></td>
                  <td colspan="3" bgcolor="#FFFFFF">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                      <input type="radio" name="record_flg" value="t" <{if $record_flg == "t"}>checked<{/if}>>する&nbsp;
                      <input type="radio" name="record_flg" value="f" <{if $record_flg == "f"}>checked<{/if}>>しない
                    </font>
                  </td>
                </tr>
                <tr height="22">
                  <td align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲載期間</font></td>
                  <td colspan="3" bgcolor="#FFFFFF">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                      <select name="news_begin1">
                        <{foreach from=$stt_years item=y}>
                          <option value="<{$y}>" <{if $y == $news_begin1}>selected<{/if}>><{$y}></option>
                        <{/foreach}>
                      </select>/
                      <select name="news_begin2">
                        <{foreach from=$months item=m}>
                          <option value="<{$m}>" <{if $m == $news_begin2}>selected<{/if}>><{$m}></option>
                        <{/foreach}>
                      </select>/
                      <select name="news_begin3">
                        <{foreach from=$days item=d}>
                          <option value="<{$d}>" <{if $d == $news_begin3}>selected<{/if}>><{$d}></option>
                        <{/foreach}>
                      </select> 〜
                      <select name="news_end1">
                        <{foreach from=$end_years item=y}>
                          <option value="<{$y}>" <{if $y == $news_end1}>selected<{/if}>><{$y}></option>
                        <{/foreach}>
                      </select>/
                      <select name="news_end2">
                        <{foreach from=$months item=m}>
                          <option value="<{$m}>" <{if $m == $news_end2}>selected<{/if}>><{$m}></option>
                        <{/foreach}>
                      </select>/
                      <select name="news_end3">
                        <{foreach from=$days item=d}>
                          <option value="<{$d}>" <{if $d == $news_end3}>selected<{/if}>><{$d}></option>
                        <{/foreach}>
                      </select>
                    </font>
                  </td>
                </tr>
                <{if $mode=="update"}>
                  <tr height="22">
                    <td align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録者</font></td>
                    <td colspan="3" bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><{$emp_name}></font><input type="hidden" name="emp_name" value="<{$emp_name}>"></td>
                  </tr>
                <{/if}>
                <tr height="22">
                  <td align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録日</font></td>
                  <td colspan="3" bgcolor="#FFFFFF">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                      <select name="news_date1">
                        <{foreach from=$reg_years item=y}>
                          <option value="<{$y}>" <{if $y == $news_date1}>selected<{/if}>><{$y}></option>
                        <{/foreach}>
                      </select>/
                      <select name="news_date2">
                        <{foreach from=$months item=m}>
                          <option value="<{$m}>" <{if $m == $news_date2}>selected<{/if}>><{$m}></option>
                        <{/foreach}>
                      </select>/
                      <select name="news_date3">
                        <{foreach from=$days item=d}>
                          <option value="<{$d}>" <{if $d == $news_date3}>selected<{/if}>><{$d}></option>
                        <{/foreach}>
                      </select>
                    </font>
                  </td>
                </tr>
              </table>
              <table width="700" border="0" cellspacing="0" cellpadding="2">
                <tr height="22">
                  <td align="right"><input type="submit" value="<{$action_btn}>"></td>
                </tr>
              </table>
              <input type="hidden" name="session" value="<{$session}>">
              <{if $mode=="update"}>
                <input type="hidden" name="news_id" value="<{$news_id}>">
                <input type="hidden" name="page" value="<{$page}>">
              <{/if}>
            </form>
          </td>
          <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        </tr>
        <tr>
          <td><img src="img/r_3.gif" width="10" height="10"></td>
          <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
          <td><img src="img/r_4.gif" width="10" height="10"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
