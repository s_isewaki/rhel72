<{*************************************************************************************************
ファイル名：hiyar_mail_header1.tpl
概要：hiyari_mail_input.iniにあったshow_hiyari_mail_header_input_form()のhtml部分をテンプレート化したもの
（js部分は、hiyar_mail_header_js.tpl）
*************************************************************************************************}>  
<input type="hidden" id="to_emp_id_list"   name="to_emp_id_list" value="">
<input type="hidden" id="cc_emp_id_list"   name="cc_emp_id_list" value="">
<input type="hidden" id="to_emp_name_list" name="to_emp_name_list" value="">
<input type="hidden" id="cc_emp_name_list" name="cc_emp_name_list" value="">
<input type="hidden" id="to_emp_anonymous_list" name="to_emp_anonymous_list" value="">
<input type="hidden" id="cc_emp_anonymous_list" name="cc_emp_anonymous_list" value="">

<!-- メールヘッダーの枠 START -->
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
  </tr>
  <tr>
    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    
    <!-- メールヘッダー情報 START -->
    <td bgcolor="#F5FFE5">      
      <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
      
        <!-- TO START -->
        <tr height="22">
          <td align="right" bgcolor="#DFFFDC" width="100">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">TO</font>
          </td>
          <td align="reft"  bgcolor="#FFFFFF">
            <{if $is_to_editable}>
              <!-- TO入力領域 START -->
              <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
                <tr>
                  <td width="250" style="border:#35B341 solid 1px;">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                      <span id="to_disp_area"></span>
                    </font>
                  </td>
                  <!-- TO宛先追加ボタン START -->
                  <{if $address_search_flag}>
                    <td width="620">
                      <ul style="list-style:none;padding:0;margin:3px;">
                        <li style="display:inline;">
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                            <input type="button" style="WIDTH: 200px; HEIGHT: 24px" value="職員検索" onclick="show_sendto_add_window('TO');">
                          </font>
                        </li>
                        
                        <{foreach from=$usable_auth key=auth_key item=auth name=list}>
                          <li style="display:inline;">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                              <input type="button" style="WIDTH: 200px; HEIGHT: 24px" value="<{$auth.auth_name}>" onclick="add_recv_list_from_auth('TO','<{$auth_key}>');">
                            </font>
                          </li>
                        <{/foreach}>

                        <{if $doctor_emp_id != ""}>
                          <li style="display:inline;">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                              <input type="button" style="WIDTH: 200px; HEIGHT: 24px" value="主治医" onclick="add_recv_list_from_emp('TO','<{$doctor_emp_id}>','<{$doctor_emp_name}>');">
                            </font>
                          </li>
                        <{/if}>
                      </ul>
                    </td>
                  <{/if}>
                  <!-- TO宛先ボタン END -->
                </tr>
              </table>
              <!-- TO入力領域 END -->             
            <{else}>
              <!-- TO入力領域 START -->
              <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
                <tr>
                  <td width="250" style="border:#35B341 solid 1px;">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                      <span id="to_disp_area"></span>
                    </font>
                  </td>
                  <td width="620">
                    &nbsp;
                  </td>
                </tr>
              </table>
              <!-- TO入力領域 END -->
            <{/if}>           
          </td>
        </tr>
        <!-- TO END -->
        
        <!-- CC START -->
        <tr height="22" id="mail_header_cc_tr">
          <td align="right" bgcolor="#DFFFDC" width="100">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
              <a href="#" onclick="switch_display('cc');">CC</a>
            </font>
          </td>
          <td align="reft"  bgcolor="#FFFFFF">
            <{if $is_cc_editable}>
              <!-- CC入力領域 START -->
              <table id="send_emp_list_cc" width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list" style="display:none">
                <tr>
                  <td width="250" style="border:#35B341 solid 1px;">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                      <span id="cc_disp_area"></span>
                    </font>
                  </td>

                  <!-- CC宛先ボタン START -->
                  <{if $address_search_flag}>
                    <td width="620">
                      <ul style="list-style:none;padding:0;margin:3px;">
                        <li style="display:inline;">
                          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                            <input type="button" style="WIDTH: 200px; HEIGHT: 24px" value="職員検索" onclick="show_sendto_add_window('CC');">
                          </font>
                        </li>
                        <{foreach from=$usable_auth key=auth_key item=auth name=list}>
                          <li style="display:inline;">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                              <input type="button" style="WIDTH: 200px; HEIGHT: 24px" value="<{$auth.auth_name}>" onclick="add_recv_list_from_auth('CC','<{$auth_key}>');">
                            </font>
                          </li>
                        <{/foreach}>
                        <{if $doctor_emp_id != ""}>
                          <li style="display:inline;">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                              <input type="button" style="WIDTH: 200px; HEIGHT: 24px" value="主治医" onclick="add_recv_list_from_emp('CC','<{$doctor_emp_id}>','<{$doctor_emp_name}>');">
                            </font>
                          </li>
                        <{/if}>
                      </ul>
                    </td>
                  <{/if}>
                  <!-- CC宛先ボタン END -->
                </tr>
              </table>
              <!-- CC入力領域 END -->
            <{else}>
              <!-- CC入力領域 START -->
              <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
                <tr>
                  <td width="250" style="border:#35B341 solid 1px;">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
                      <span id="cc_disp_area"></span>
                    </font>
                  </td>
                  <td width="620">&nbsp;</td>
                </tr>
              </table>
              <!-- CC入力領域 END -->
            <{/if}>
          </td>
        </tr>        
        <!-- CC END -->
        
        <!-- CC非表示 START -->
        <tr height="22" id="mail_header_non_cc_tr">
          <td align="right" bgcolor="#DFFFDC" width="100">CC</td>
          <td align="reft"  bgcolor="#FFFFFF">
            <{if $is_cc_editable}>
              <{if $address_search_flag}>
                <a href="javascript:void(0);" onclick="is_cc_usable( true );return false;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">CCを追加</font></a>
              <{/if}>
            <{else}>
              <nobr>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10" color="#FF0000">※<{$anonymous_name}>からのメールを返信する場合、CCを指定することはできません。</font>
              </nobr>
            <{/if}>
          </td>
        </tr>
        <!-- CC非表示 END -->
        
        <tr height="22">
          <td align="right" bgcolor="#DFFFDC" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">件名</font></td>
          <td align="reft"  bgcolor="#FFFFFF">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
              <input type="text" id="mail_subject" name="mail_subject" style="width:350" value="<{$default_subject}>">
            </font>
          </td>
        </tr>
        
        <tr height="22">
          <td align="right" bgcolor="#DFFFDC" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">ラインマーカー</font></td>
          <td align="reft"  bgcolor="#FFFFFF">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
              <select name="marker">
                <option value="0" style="background-color:#FFFFFF;color:#000000;" selected>なし
                <option value="1" style="background-color:#FF8080;color:#FFFFFF;">赤
                <option value="2" style="background-color:#80FFFF;color:#0000FF;">青
                <option value="3" style="background-color:#FFFF80;color:#0000FF;">黄
                <option value="4" style="background-color:#80FF80;color:#0000FF;">緑
                <option value="5" style="background-color:#FF80FF;color:#FFFFFF;">ピンク
              </select>
            </font>
          </td>
        </tr>
      </table>
      
      <{if $anonymous_mail_use_flg}>
        <nobr>
          <input type="checkbox" id="anonymous" name="anonymous" value="true" <{if $is_anonymous_default}>checked<{/if}>>
          <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">匿名で送信する</font>
        </nobr>
      <{/if}>
    </td>
    <!-- メールヘッダー情報 END -->
    
    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
  </tr>
  <tr>
    <td><img src="img/r_3.gif" width="10" height="10"></td>
    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
    <td><img src="img/r_4.gif" width="10" height="10"></td>
  </tr>
</table>
<!-- メールヘッダーの枠 END -->
  