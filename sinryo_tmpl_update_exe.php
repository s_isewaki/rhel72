<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require_once("sum_application_workflow_common_class.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療区分登録権限チェック
$checkauth = check_authority($session,59,$fname);
if($checkauth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// テンプレートのバージョンの記述があれば1、なければ0
$template_version_description = (strpos($tmpl_content, "COMEDIX_TEMPLATE_VERSION_DEFINITION") === false ? 0 : 1);


// データベースに接続
$con = connect2db($fname);

$obj = new application_workflow_common_class($con, $fname);

// content 保存
$savefilename = "workflow/tmp/{$session}_{$tmpl_file}";

if (!$template_version_description){
	$tmpl_content = eregi_replace("_(textarea)", "/\\1", $tmpl_content);
	$tmpl_content = eregi_replace("__(script)", "/\\1", $tmpl_content);
}

// 内容書き込み
$fp = fopen($savefilename, "w");
if (!fwrite($fp, $tmpl_content, 2000000)) {
	fclose($fp);
	echo("<script language=\"javascript\">alert('ファイルの書き込みができませんでした。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}
fclose($fp);

if (strlen($tmpl_name) > 40) {
	echo("<script type=\"text/javascript\">alert('テンプレート名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 入力チェック
if (!$template_version_description){
	if (strpos($tmpl_content, "<? // XML") === false) {
		echo("<script type=\"text/javascript\">alert('XML作成用コードがありません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}

// 内容チェック
$content_upper = strtoupper($tmpl_content);
$sqlchkflg = true;

//$chkarray = array("INSERT ", "INSERT	", "UPDATE ", "UPDATE	", "DELETE ", "DELETE	", "CONNECT2DB", "SELECT_FROM_TABLE ", "PG_CONNECT", "PG_EXEC", "<FORM");
$chkarray = array("INSERT ", "INSERT	", "UPDATE ", "UPDATE	", "CONNECT2DB", "SELECT_FROM_TABLE ", "PG_CONNECT", "PG_EXEC", "<FORM");

/*
foreach($chkarray as $chkstr){
	if(!(strpos($content_upper, $chkstr) === false)){
		$sqlchkflg = false;
		break;
	}
}

if($sqlchkflg == false){
	echo("<script language=\"javascript\">alert('指定できない文字列があります($chkstr)。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}
*/

//==============================================================================
// 2010/01以降のバージョンでは、create_fileとtmpl_fileは同一のものが中心となる。
// ファイルは２分割しない。また、replaceは不要。
//==============================================================================
if ($template_version_description){
	$fp1 = @fopen($tmpl_file, "w");
	if (!$fp1 || !fwrite($fp1, $tmpl_content, 2000000)) {
		@fclose($fp1);
		echo("<script language=\"javascript\">alert('ファイルの書き込みができませんでした。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	@fclose($fp1);
	@chmod($tmpl_file, 0666);
}

//==============================================================================
// 以下は2009/12までの主流形式
//==============================================================================
else {
	// ファイル保存
	$create_content = $tmpl_content;
	$tmpl_content = ereg_replace("<\? \/\/ XML.*\?>", "", $tmpl_content);

	$create_content = ereg_replace("^.*<\? // XML", "<? // XML", $create_content);
	// 文字コード変換　EUC -> UTF-8
	$create_content = mb_convert_encoding($create_content, "UTF-8", "EUC-JP");

	// DBに保存する場合は'エスケープが必要、$content=pg_escape_string($content);

	// XML作成用ファイル名
	$pos = strpos($tmpl_content, "xml_creator");
	//$pos = @strrpos($tmpl_content, "<", $pos);  // strrposの第三引数は、PHP5から
	$pos1 = strpos($tmpl_content, "create_", $pos);
	$pos2 = strpos($tmpl_content, "\"", $pos1);
	$create_filename = substr($tmpl_content, $pos1, $pos2 - $pos1);

	// 内容書込み
	$fp1 = @fopen($tmpl_file, "w");
	if (!$fp1 || !fwrite($fp1, $tmpl_content, 2000000)) {
		@fclose($fp1);
		echo("<script language=\"javascript\">alert('ファイルの書き込みができませんでした。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	@fclose($fp1);
	@chmod($tmpl_file, 0666);

	// XML作成用コード書込み
	$fp2 = fopen($create_filename, "w");
	if (!$fp2 || !fwrite($fp2, $create_content, 2000000)) {
		@fclose($fp2);
		echo("<script language=\"javascript\">alert('ファイルの書き込みができませんでした。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
	fclose($fp2);
	chmod($create_filename, 0666);
}

if ($wkfw_enabled == "1"){
	if ($approve_num == 0) {
		echo("<script type=\"text/javascript\">alert('受信階層数を選択してください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}

if ($wkfw_id){
	$sql = "select * from sum_wkfwmst where wkfw_id = ". (int)$wkfw_id;
  $sel = select_from_table($con, $sql, "", $fname);
	$current_wkfw_id = pg_fetch_result($sel, 0, "wkfw_id");
}

if ($wkfw_enabled == "1" && !$current_wkfw_id){
	$wkfw_id = (int)($obj->get_max_wkfw_id("REAL")) + 1;
	$idx = 0;
	for (;;){
		$idx++;
		if ($idx > 999) break;
		$short_wkfw_name = "w" . sprintf("%03d", $idx);
		$sel = select_from_table($con, "select short_wkfw_name from sum_wkfwmst where short_wkfw_name = '".$short_wkfw_name."'", "", $fname);
		if ($sel == 0) {
			pg_close($con);
			require_once("summary_common.ini");
			summary_common_show_error_page_and_die();
		}
		if (pg_fetch_result($sel, 0, "short_wkfw_name")) continue;
		break;
	}
}

$approve_mng = array();
$chk_apv_exist_flg = true;
for ($i = 0; $i < @$approve_num; $i++) {
	$idx = $i + 1;

	$apv_div0_flg = "apv_div0_flg$idx";
	$apv_div1_flg = "apv_div1_flg$idx";
	$apv_div2_flg = "apv_div2_flg$idx";
	$apv_div3_flg = "apv_div3_flg$idx";
	$apv_div4_flg = "apv_div4_flg$idx";
	$apv_div5_flg = "apv_div5_flg$idx";
	$apv_div6_flg = "apv_div6_flg$idx";

	$target_class_div = "target_class_div$idx";
	$multi_apv_flg = "multi_apv_flg$idx";
	$next_notice_div = "next_notice_div$idx";
	$next_notice_recv_div = "next_notice_recv_div$idx";

	$apv_num = "apv_num$idx";
	$apv_common_group_id = "apv_common_group_id$idx";

	array_push($approve_mng, array(
                                    "apv_div0_flg" => (@$$apv_div0_flg == "") ? "f" : @$$apv_div0_flg,
                                    "apv_div1_flg" => (@$$apv_div1_flg == "") ? "f" : @$$apv_div1_flg,
                                    "apv_div2_flg" => (@$$apv_div2_flg == "") ? "f" : @$$apv_div2_flg,
                                    "apv_div3_flg" => (@$$apv_div3_flg == "") ? "f" : @$$apv_div3_flg,
                                    "apv_div4_flg" => (@$$apv_div4_flg == "") ? "f" : @$$apv_div4_flg,
                                    "apv_div5_flg" => (@$$apv_div4_flg == "") ? "f" : @$$apv_div5_flg,
                                    "apv_div6_flg" => (@$$apv_div4_flg == "") ? "f" : @$$apv_div6_flg,
                                    "deci_flg" => "t",
                                    "target_class_div" => @$$target_class_div,
                                    "multi_apv_flg" => @$$multi_apv_flg,
                                    "next_notice_div" => (@$$next_notice_div == "") ? null : @$$next_notice_div,
                                    "next_notice_recv_div" => (@$$next_notice_recv_div == "") ? null : @$$next_notice_recv_div,
                                    "apv_num" => (@$$apv_num == "") ? null : @$$apv_num,
                                    "apv_common_group_id" => (@$$apv_common_group_id == "") ? null : @$$apv_common_group_id
                                   ));

	$approve = "approve$idx";
	if(@$$approve == "") {
		$chk_apv_exist_flg = false;
	}
}

if(!$chk_apv_exist_flg) {
	echo("<script type=\"text/javascript\">alert('受信階層を設定してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 内容チェック
$content_upper = strtoupper($tmpl_content);

$sqlchkflg = true;

// 診療区分-テンプレートの情報を更新
$tmpl_name_sql   = pg_escape_string($tmpl_name);
$tmpl_file_sql   = pg_escape_string($tmpl_file);
$undeletable_sql = ($disp_del_bttn == "1") ? "0" : "1" ;
$sql = "update tmplmst set";
//$set = array("tmpl_name","tmpl_file","window_size");
//$setvalue = array($tmpl_name_sql,$tmpl_file_sql,$window_size);
$set = array("tmpl_name","tmpl_file","window_size","undeletable");
$setvalue = array($tmpl_name_sql,$tmpl_file_sql,$window_size,$undeletable_sql);
$cond = "where tmpl_id = '$tmpl_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}


// 登録内容の編集
$wkfw_type = (@$wkfw_type == "") ? null : $wkfw_type;
$wkfw_folder_id = (@$wkfw_folder_id == "") ? null : $wkfw_folder_id;
$ref_dept_st_flg = (@$ref_dept_st_flg != "t") ? "f" : $ref_dept_st_flg;
$ref_dept_flg = (@$ref_dept_flg == "") ? null : $ref_dept_flg;
$ref_st_flg   = (@$ref_st_flg == "") ? null : $ref_st_flg;

// ワークフロー情報を更新
if (@$wkfw_id){
	$setvalue = array($tmpl_id, $wkfw_enabled, $wkfw_type, pg_escape_string(@$wkfw_title), pg_escape_string(@$wkfw_content), @$start_date, @$end_date, @$wkfw_appr, @$wkfw_content_type, @$wkfw_folder_id, @$ref_dept_st_flg, @$ref_dept_flg, @$ref_st_flg, pg_escape_string(@$short_wkfw_name), @$apply_title_disp_flg, @$send_mail_flg, @$approve_label, @$is_hide_apply_list, @$is_hide_approve_list, (int)@$is_modifyable_by_other_emp, (int)@$is_hide_approve_status);

	if ($current_wkfw_id){
		$set = array("tmpl_id", "wkfw_enabled", "wkfw_type", "wkfw_title", "wkfw_content", "wkfw_start_date", "wkfw_end_date", "wkfw_appr", "wkfw_content_type", "wkfw_folder_id", "ref_dept_st_flg", "ref_dept_flg", "ref_st_flg", "short_wkfw_name", "apply_title_disp_flg", "send_mail_flg", "approve_label","is_hide_apply_list","is_hide_approve_list","is_modifyable_by_other_emp", "is_hide_approve_status");

		update_set_table($con, "update sum_wkfwmst set", $set, $setvalue, "where wkfw_id = ".$wkfw_id, $fname);
	} else {
		$setvalue[] = $wkfw_id;
		$sql = "insert into sum_wkfwmst ";
		$sql .= "(tmpl_id, wkfw_enabled, wkfw_type, wkfw_title, wkfw_content, wkfw_start_date, wkfw_end_date, wkfw_appr, wkfw_content_type, wkfw_folder_id, ref_dept_st_flg, ref_dept_flg, ref_st_flg, short_wkfw_name, apply_title_disp_flg, send_mail_flg, approve_label, is_hide_apply_list, is_hide_approve_list, is_modifyable_by_other_emp, is_hide_approve_status, wkfw_id) values (";

		insert_into_table($con, $sql, $setvalue, $fname);
	}

	// 承認者管理削除・登録
	delete_from_table($con, "delete from sum_wkfwapvmng where wkfw_id = ".$wkfw_id, "", $fname);
	$apv_order = 1;
	foreach($approve_mng as $mng) {
		$obj->regist_wkfwapvmng($wkfw_id, $apv_order, $mng, $mode);
		$apv_order++;
	}


	// 部署役職(申請者所属)、職員、委員会・ＷＧ削除、部署役職(部署指定)登録
	delete_from_table($con, "delete from sum_wkfwapvpstdtl where wkfw_id = ".$wkfw_id, "", $fname);
	delete_from_table($con, "delete from sum_wkfwapvdtl where wkfw_id = ".$wkfw_id, "", $fname);
	delete_from_table($con, "delete from sum_wkfwpjtdtl where wkfw_id = ".$wkfw_id, "", $fname);
	delete_from_table($con, "delete from sum_wkfwapvsectdtl where wkfw_id = ".$wkfw_id, "", $fname);

	for ($i = 0; $i < @$approve_num; $i++) {
		$idx = $i + 1;
		$apv_div0_flg = "apv_div0_flg$idx";
		$apv_div1_flg = "apv_div1_flg$idx";
		$apv_div2_flg = "apv_div2_flg$idx";
		$apv_div3_flg = "apv_div3_flg$idx";
		$apv_div4_flg = "apv_div4_flg$idx";
		$apv_div5_flg = "apv_div5_flg$idx";
		$apv_div6_flg = "apv_div6_flg$idx";

		// 部署役職(申請者所属)登録
		if($$apv_div0_flg == "t") {
			$st_id = "st_id$idx";
			$arr_st_id = split(",", $$st_id);
			for($j=0; $j<count($arr_st_id); $j++) {
				$obj->regist_wkfwapvpstdtl($wkfw_id, $idx, $arr_st_id[$j], "0", $mode);
			}
		}
		// 職員登録
		if($$apv_div1_flg == "t") {
			$emp_id = "emp_id$idx";
			$arr_emp_id = split(",", $$emp_id);
			for($j=0; $j<count($arr_emp_id); $j++) {
				$sub_order = $j + 1;
				if(count($arr_emp_id) == 1) $sub_order = null;
				$obj->regist_wkfwapvdtl($wkfw_id, $idx, $arr_emp_id[$j], $sub_order, $mode);
			}
		}
		// 委員会・ＷＧ登録
		if($$apv_div3_flg == "t") {
			$pjt_parent_id = "pjt_parent_id$idx";
			$pjt_child_id = "pjt_child_id$idx";
			$obj->regist_wkfwpjtdtl($wkfw_id, $idx, $$pjt_parent_id, $$pjt_child_id, $mode);
		}
		// 部署役職(部署指定)登録
		if($$apv_div4_flg == "t") {
			// 部署
			$class_sect_id     = "class_sect_id$idx";
			$atrb_sect_id      = "atrb_sect_id$idx";
			$dept_sect_id      = "dept_sect_id$idx";
			$room_sect_id      = "room_sect_id$idx";
			$obj->regist_wkfwapvsectdtl($wkfw_id, $idx, $$class_sect_id, $$atrb_sect_id, $$dept_sect_id, $$room_sect_id, $mode);

			// 役職
			$st_sect_id = "st_sect_id$idx";
			$arr_st_sect_id = split(",", $$st_sect_id);
			for($j=0; $j<count($arr_st_sect_id); $j++) {
				$obj->regist_wkfwapvpstdtl($wkfw_id, $idx, $arr_st_sect_id[$j], "4", $mode);
			}
		}
	}

	// 申請者以外の結果通知削除・登録
	delete_from_table($con, "delete from sum_wkfwnoticemng where wkfw_id = ".$wkfw_id, "", $fname);
	delete_from_table($con, "delete from sum_wkfwnoticestdtl where wkfw_id = ".$wkfw_id, "", $fname);
	delete_from_table($con, "delete from sum_wkfwnoticedtl where wkfw_id = ".$wkfw_id, "", $fname);
	delete_from_table($con, "delete from sum_wkfwnoticepjtdtl where wkfw_id = ".$wkfw_id, "", $fname);
	delete_from_table($con, "delete from sum_wkfwnoticesectdtl where wkfw_id = ".$wkfw_id, "", $fname);

	if($notice != "" ) {
		// 結果通知管理登録
		$arr = array(
			"wkfw_id" => $wkfw_id,
			"target_class_div" => ($notice_target_class_div == "") ? null : $notice_target_class_div,
			"rslt_ntc_div0_flg" => ($rslt_ntc_div0_flg == "") ? "f" : $rslt_ntc_div0_flg,
			"rslt_ntc_div1_flg" => ($rslt_ntc_div1_flg == "") ? "f" : $rslt_ntc_div1_flg,
			"rslt_ntc_div2_flg" => ($rslt_ntc_div2_flg == "") ? "f" : $rslt_ntc_div2_flg,
			"rslt_ntc_div3_flg" => ($rslt_ntc_div3_flg == "") ? "f" : $rslt_ntc_div3_flg,
			"rslt_ntc_div4_flg" => ($rslt_ntc_div4_flg == "") ? "f" : $rslt_ntc_div4_flg,
			"rslt_ntc_div5_flg" => ($rslt_ntc_div5_flg == "") ? "f" : $rslt_ntc_div5_flg,
			"rslt_ntc_div6_flg" => ($rslt_ntc_div6_flg == "") ? "f" : $rslt_ntc_div6_flg
		);

		$obj->regist_wkfwnoticemng($arr, $mode);

		// 部署役職指定(結果通知)登録
		if($rslt_ntc_div0_flg == "t") {
			$arr_notice_st_id = split(",", $notice_st_id);
			for($i=0; $i<count($arr_notice_st_id); $i++) {
				$obj->regist_wkfwnoticestdtl($wkfw_id, $arr_notice_st_id[$i], "0", $mode);
			}
		}
		// 職員指定(結果通知)登録
		if($rslt_ntc_div1_flg == "t") {
			$arr_notice_emp_id = split(",", $notice_emp_id);
			for($i=0; $i<count($arr_notice_emp_id); $i++) {
				$obj->regist_wkfwnoticedtl($wkfw_id, $arr_notice_emp_id[$i], $mode);
			}
		}
		// 委員会・ＷＧ指定(結果通知)登録
		if($rslt_ntc_div3_flg == "t") {
			$obj->regist_wkfwnoticepjtdtl($wkfw_id, $notice_pjt_parent_id, $notice_pjt_child_id, $mode);
		}
		// 部署役職指定(結果通知)(部署指定)登録
		if($rslt_ntc_div4_flg == "t") {
			$obj->regist_wkfwnoticesectdtl($wkfw_id, $notice_class_sect_id, $notice_atrb_sect_id, $notice_dept_sect_id, $notice_room_sect_id, $mode);

			$arr_notice_st_sect_id = split(",", $notice_st_sect_id);
			for($i=0; $i<count($arr_notice_st_sect_id); $i++) {
				$obj->regist_wkfwnoticestdtl($wkfw_id, $arr_notice_st_sect_id[$i], "4", $mode);
			}
		}
	}
}

// データベース接続を切断
pg_close($con);

// テンプレート一覧画面に遷移
echo("<script language=\"javascript\">location.href = 'sinryo_tmpl_list.php?session=$session';</script>");
?>
