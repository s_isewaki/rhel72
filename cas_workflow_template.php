<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("referer_common.ini");
require_once("get_values.ini");
require_once("cas_workflow_common.ini");
require_once("./conf/sql.inf");
require_once("cas_application_workflow_common_class.php");
require_once("show_class_name.ini");
require_once("label_by_profile_type.ini");
require_once("cas_common.ini");
require_once("get_cas_title_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, $CAS_MANAGE_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);

$obj = new cas_application_workflow_common_class($con, $fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 遷移元の取得
$referer = get_referer($con, $session, "workflow", $fname);

// 部門一覧を取得
$sel_class = $obj->get_classmst();
$classes = array();
$classes["-"] = "----------";
while ($row = pg_fetch_array($sel_class)) {
	$tmp_class_id = $row["class_id"];
	$tmp_class_nm = $row["class_nm"];
	$classes[$tmp_class_id] = $tmp_class_nm;
}
pg_result_seek($sel_class, 0);

// 課一覧を取得
$sel_atrb = $obj->get_atrbmst();

// 科一覧を取得
$sel_dept = $obj->get_deptmst();
$dept_names = Array();
while ($row = pg_fetch_array($sel_dept)) {
	$tmp_class_id = $row["class_id"];
	$tmp_atrb_id = $row["atrb_id"];
	$tmp_dept_id = $row["dept_id"];
	$tmp_dept_nm = $row["dept_nm"];
	$dept_names["{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"] = $tmp_dept_nm;
}
pg_result_seek($sel_dept, 0);

// 役職一覧を取得
$sel_st = $obj->get_stmst();

// 組織名を取得
$arr_class_name = get_class_name_array($con, $fname);


// 職員ID取得
$emp_id = get_emp_id($con,$session,$fname);

// ログインID取得
$emp_login_id = get_login_id($con,$emp_id,$fname);

// guest*ユーザはテンプレートを登録できない
$can_regist_flg = true;
if (substr($emp_login_id, 0, 5) == "guest") {
	$can_regist_flg = false;
}

// プレビュー押下時
if ($preview_flg == "1") {
// wkfw_content 保存
	if (strlen($wkfw_content) > 0) {
		$ext = ".php";
		$savefilename = "cas_workflow/tmp/{$session}_t{$ext}";

		// 内容書き込み
		if (!$fp = fopen($savefilename, "w")) {
			echo("<script language=\"javascript\">alert('ファイルのオープンができませんでした。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}
		if (!fwrite($fp, $wkfw_content, 2000000)) {
			echo("<script language=\"javascript\">alert('ファイルの書き込みができませんでした。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		fclose($fp);

		$wkfw_title = str_replace("'","\'", $wkfw_title);
	}
}


if ($back != "t") {

	if($mode == "ALIAS")
	{
		$sql = "select * from cas_wkfwmst";
	}
	else
	{
		$sql = "select * from cas_wkfwmst_real";
	}

	$cond = "where wkfw_id = $wkfw_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$wkfw_type = pg_fetch_result($sel, 0, "wkfw_type");
	$wkfw_title = pg_fetch_result($sel, 0, "wkfw_title");
	$wkfw_content = pg_fetch_result($sel, 0, "wkfw_content");
	$start_date = pg_fetch_result($sel, 0, "wkfw_start_date");
	$end_date = pg_fetch_result($sel, 0, "wkfw_end_date");
	$wkfw_appr = pg_fetch_result($sel, 0, "wkfw_appr");
	$wkfw_content_type = pg_fetch_result($sel, 0, "wkfw_content_type");
	$wkfw_folder_id = pg_fetch_result($sel, 0, "wkfw_folder_id");

	$start_year = substr($start_date, 0, 4);
	$start_month = substr($start_date, 4, 2);
	$start_day = substr($start_date, 6, 2);
	$end_year = substr($end_date, 0, 4);
	$end_month = substr($end_date, 4, 2);
	$end_day = substr($end_date, 6, 2);


	$ref_dept_st_flg = pg_fetch_result($sel, 0, "ref_dept_st_flg");
	$ref_dept_flg = pg_fetch_result($sel, 0, "ref_dept_flg");
	$ref_st_flg = pg_fetch_result($sel, 0, "ref_st_flg");

	$short_wkfw_name = pg_fetch_result($sel, 0, "short_wkfw_name");
	$apply_title_disp_flg = pg_fetch_result($sel, 0, "apply_title_disp_flg");


	// 本文形式タイプのデフォルトを「テキスト」とする
	if ($wkfw_content_type == "") {$wkfw_content_type = "1";}


	// 利用権限
	// 部署
	if($ref_dept_flg == "2")
	{
		$arr_refdept = $obj->get_wkfw_refdept($wkfw_id, $mode);
		$ref_dept = array();
		foreach($arr_refdept as $dept)
		{
			$class_id = $dept["class_id"];
			$atrb_id  = $dept["atrb_id"];
			$dept_id  = $dept["dept_id"];

			$ref_dept[] = "$class_id-$atrb_id-$dept_id";
		}
	}

	// 役職
	if($ref_st_flg == "2")
	{
		$arr_refst = $obj->get_wkfw_refst($wkfw_id, $mode);
		$ref_st = array();
		foreach($arr_refst as $refst)
		{
			$ref_st[] = $refst["st_id"];
		}
	}

	// 職員
	$arr_refemp = $obj->get_wkfw_refemp($wkfw_id, $mode);
	$target_id_list1 = "";
	foreach($arr_refemp as $refemp)
	{
		if($target_id_list1 != "")
		{
			$target_id_list1 .= ",";
		}
		$target_id_list1 .= $refemp["emp_id"];
	}

	// 承認階層情報取得
	if($mode == "ALIAS")
	{
		$sql = "select * from cas_wkfwapvmng";
	}
	else
	{
		$sql = "select * from cas_wkfwapvmng_real";
	}
	$cond = "where wkfw_id = $wkfw_id order by apv_order";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$approve_num = pg_numrows($sel);
	for ($i = 1; $i <= $approve_num; $i++) {

		$ret_arr = array();

		// 承認者指定区分
		$apv_div0_flg = "apv_div0_flg$i";
		$apv_div1_flg = "apv_div1_flg$i";
		$apv_div2_flg = "apv_div2_flg$i";
		$apv_div3_flg = "apv_div3_flg$i";
		$apv_div4_flg = "apv_div4_flg$i";

		$$apv_div0_flg .= pg_fetch_result($sel, $i - 1, "apv_div0_flg");
		$$apv_div1_flg .= pg_fetch_result($sel, $i - 1, "apv_div1_flg");
		$$apv_div2_flg .= pg_fetch_result($sel, $i - 1, "apv_div2_flg");
		$$apv_div3_flg .= pg_fetch_result($sel, $i - 1, "apv_div3_flg");
		$$apv_div4_flg .= pg_fetch_result($sel, $i - 1, "apv_div4_flg");

		$apv_order = pg_fetch_result($sel, $i - 1, "apv_order");

		$apv_num = pg_fetch_result($sel, $i - 1, "apv_num");

		// 部署役職(申請書所属)
		if(pg_fetch_result($sel, $i - 1, "apv_div0_flg") == "t")
		{
			search_wkfw_apv_pst($con, $fname, $wkfw_id, $apv_order, $mode, $ret_arr);
		}

		// 部署役職(部署指定)
		if(pg_fetch_result($sel, $i - 1, "apv_div4_flg") == "t")
		{
			search_wkfw_apv_pst_sect($con, $fname, $wkfw_id, $apv_order, $mode, $ret_arr);
		}

		// 職員
		if(pg_fetch_result($sel, $i - 1, "apv_div1_flg") == "t")
		{
			search_wkfw_apv_dtl($con, $fname, $wkfw_id, $apv_order, $mode, $ret_arr);
		}

		// 委員会・ＷＧ
		if(pg_fetch_result($sel, $i - 1, "apv_div3_flg") == "t")
		{
			search_wkfw_pjt_dtl($con, $fname, $wkfw_id, $apv_order, $mode, $ret_arr);
		}

		// その他
		if(pg_fetch_result($sel, $i - 1, "apv_div2_flg") == "t")
		{
			set_aply_apv($apv_num, $ret_arr);
		}

		$j = 0;
		foreach ($ret_arr as $row)
		{

			// 承認者欄(表示用)
			$var_name = "approve$i";

			if($j > 0)
			{
				$$var_name .= "<BR>";
			}

			$$var_name .= $row["approve"];
			$j++;

			// 部署区分：「1」部門　「2」課　「3」科　「4」室 「0」部署指定なし
			$var_name = "target_class_div$i";
			$$var_name .= $row["target_class_div"];
			// 役職ＩＤ
			$var_name = "st_id$i";
			$$var_name .= $row["st_id"];
			// 承認者ＩＤ
			$var_name = "emp_id$i";
			$$var_name .= $row["emp_id"];
			// 承認者名
			$var_name = "emp_nm$i";
			$$var_name .= $row["emp_nm"];
			// 承認者削除フラグ
			$var_name = "emp_del_flg$i";
			$$var_name .= $row["emp_del_flg"];

			// 委員会ＷＧ親ＩＤ
			$var_name = "pjt_parent_id$i";
			$$var_name .= $row["pjt_parent_id"];

			// 委員会ＷＧ親名称
			$var_name = "pjt_parent_nm$i";
			$$var_name .= $row["pjt_parent_nm"];

			// 委員会ＷＧ子ＩＤ
			$var_name = "pjt_child_id$i";
			$$var_name .= $row["pjt_child_id"];

			// 委員会ＷＧ子名称
			$var_name = "pjt_child_nm$i";
			$$var_name .= $row["pjt_child_nm"];

			// 部門ＩＤ
			$var_name = "class_sect_id$i";
			$$var_name .= $row["class_sect_id"];

			// 課ＩＤ
			$var_name = "atrb_sect_id$i";
			$$var_name .= $row["atrb_sect_id"];

			// 科ＩＤ
			$var_name = "dept_sect_id$i";
			$$var_name .= $row["dept_sect_id"];

			// 室ＩＤ
			$var_name = "room_sect_id$i";
			$$var_name .= $row["room_sect_id"];

			// 役職ＩＤ
			$var_name = "st_sect_id$i";
			$$var_name .= $row["st_sect_id"];

		}

		$var_name = "multi_apv_flg$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "multi_apv_flg"));

		$var_name = "next_notice_div$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "next_notice_div"));

		$var_name = "apv_num$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "apv_num"));

	}

	// 結果通知管理情報取得
	$notice = "";
	$arr_wkfwnoticemng = $obj->get_wkfwnoticemng($wkfw_id, $mode);
	$rslt_ntc_div0_flg = $arr_wkfwnoticemng["rslt_ntc_div0_flg"];
	$rslt_ntc_div1_flg = $arr_wkfwnoticemng["rslt_ntc_div1_flg"];
	$rslt_ntc_div2_flg = $arr_wkfwnoticemng["rslt_ntc_div2_flg"];
	$rslt_ntc_div3_flg = $arr_wkfwnoticemng["rslt_ntc_div3_flg"];
	$rslt_ntc_div4_flg = $arr_wkfwnoticemng["rslt_ntc_div4_flg"];

	$notice_target_class_div = $arr_wkfwnoticemng["notice_target_class_div"];

	// 部署役職指定(結果通知)
	if($rslt_ntc_div0_flg == "t")
	{
		$arr_wkfwnoticestdtl = $obj->get_wkfwnoticestdtl($wkfw_id, "0", $mode);
		if($notice_target_class_div != 0)
		{
			$arr_classname = $obj->get_classname();
			if($notice_target_class_div == "1")
			{
				$notice_target_class_name = $arr_classname[0]["class_nm"];
			}
			else if($notice_target_class_div == "2")
			{
				$notice_target_class_name = $arr_classname[0]["atrb_nm"];
			}
			else if($notice_target_class_div == "3")
			{
				$notice_target_class_name = $arr_classname[0]["dept_nm"];
			}
			else if($notice_target_class_div == "4")
			{
				$notice_target_class_name = $arr_classname[0]["room_nm"];
			}

			$notice .= "申請者の所属する【";
	 		$notice .=	$notice_target_class_name;
 			$notice .=	"】の";
		}
		else
		{
			if(count($arr_wkfwnoticestdtl) > 0)
			{
				// 病院内/所内
				$in_hospital2_title = $_label_by_profile["IN_HOSPITAL2"][$profile_type];
				if($in_hospital2_title != "")
				{
	     			$notice .=	"【".$in_hospital2_title."】の";
				}
			}
		}

		foreach($arr_wkfwnoticestdtl as $wkfwnoticestdtl)
		{
			if($notice_st_id != "")
			{
				$notice_st_id .= ",";
				$notice .= ", ";
			}
			$notice_st_id .= $wkfwnoticestdtl["st_id"];
			$notice .= $wkfwnoticestdtl["st_nm"];
		}
	}

	// 部署役職指定(結果通知)(部署指定)
	if($rslt_ntc_div4_flg == "t")
	{
		if($notice != "")
		{
			$notice .= "<BR>";
		}

		// 部署
		$arr_wkfwnoticesectdtl = $obj->get_wkfwnoticesectdtl($wkfw_id, $mode);
		$notice_class_sect_id  = $arr_wkfwnoticesectdtl["class_id"];
		$notice_atrb_sect_id   = $arr_wkfwnoticesectdtl["atrb_id"];
		$notice_dept_sect_id   = $arr_wkfwnoticesectdtl["dept_id"];
		$notice_room_sect_id   = $arr_wkfwnoticesectdtl["room_id"];

		$notice_class_sect_nm = $arr_wkfwnoticesectdtl["class_nm"];
		$notice_atrb_sect_nm  = $arr_wkfwnoticesectdtl["atrb_nm"];
		$notice_dept_sect_nm  = $arr_wkfwnoticesectdtl["dept_nm"];
		$notice_room_sect_nm  = $arr_wkfwnoticesectdtl["room_nm"];

		$notice_all_class_nm = $notice_class_sect_nm;
		if($notice_all_class_nm != "")
		{
			if($notice_atrb_sect_nm != "")
			{
				$notice_all_class_nm .= " > ";
				$notice_all_class_nm .= $notice_atrb_sect_nm;
			}

			if($notice_dept_sect_nm != "")
			{
				$notice_all_class_nm .= " > ";
				$notice_all_class_nm .= $notice_dept_sect_nm;
			}

			if($notice_room_sect_nm != "")
			{
				$notice_all_class_nm .= " > ";
				$notice_all_class_nm .= $notice_room_sect_nm;
			}
		}

		// 役職
		$arr_wkfwnoticestdtl = $obj->get_wkfwnoticestdtl($wkfw_id, "4", $mode);

		$notice_st_sect_id = "";
		$notice_st_sect_nm = "";
		foreach($arr_wkfwnoticestdtl as $wkfwnoticestdtl)
		{
			if($notice_st_sect_id != "")
			{
				$notice_st_sect_id .= ",";
				$notice_st_sect_nm .= ", ";
			}
			$notice_st_sect_id .= $wkfwnoticestdtl["st_id"];
			$notice_st_sect_nm .= $wkfwnoticestdtl["st_nm"];
		}

		if($notice_all_class_nm != "" && $notice_st_sect_nm != "")
		{
			$notice .= $notice_all_class_nm."の".$notice_st_sect_nm;
		}
	}

	// 職員指定(結果通知)
	if($rslt_ntc_div1_flg == "t")
	{
		if($notice != "")
		{
			$notice .= "<BR>";
		}

		$arr_wkfwnoticedtl = $obj->get_wkfwnoticedtl($wkfw_id, $mode);
		foreach($arr_wkfwnoticedtl as $wkfwnoticedtl)
		{
			if($notice_emp_id != "")
			{
				$notice_emp_id .= ",";
				$notice_emp_nm .= ", ";
				$notice .= ", ";
			}
			$notice_emp_id .= $wkfwnoticedtl["emp_id"];
			$notice_emp_lt_nm = $wkfwnoticedtl["emp_lt_nm"];
			$notice_emp_ft_nm = $wkfwnoticedtl["emp_ft_nm"];

			$notice_emp_nm .= $notice_emp_lt_nm." ".$notice_emp_ft_nm;
			$notice .= $notice_emp_lt_nm." ".$notice_emp_ft_nm;
		}
	}

	// 委員会・ＷＧ指定(結果通知)
	if($rslt_ntc_div3_flg == "t")
	{
		if($notice != "")
		{
			$notice .= "<BR>";
		}

		$arr_wkfwnoticepjtdtl = $obj->get_wkfwnoticepjtdtl($wkfw_id, $mode);
		$notice_pjt_parent_id = $arr_wkfwnoticepjtdtl[0]["pjt_parent_id"];
		$notice_pjt_parent_nm = $arr_wkfwnoticepjtdtl[0]["pjt_parent_nm"];
		$notice_pjt_child_id  = $arr_wkfwnoticepjtdtl[0]["pjt_child_id"];
		$notice_pjt_child_nm  = $arr_wkfwnoticepjtdtl[0]["pjt_child_nm"];

		$notice .= $notice_pjt_parent_nm;
		if($notice_pjt_child_nm != "")
		{
			$notice .= " > ";
			$notice .= $notice_pjt_child_nm;
		}
	}

	// その他
	if($rslt_ntc_div2_flg == "t")
	{
		if($notice != "")
		{
			$notice .= "<BR>";
		}
		$notice .= "申請時に結果通知者を指定する";

	}

	// 添付ファイル情報を取得
	if($mode == "ALIAS")
	{
		$sql = "select wkfwfile_no, wkfwfile_name from cas_wkfwfile";
	}
	else
	{
		$sql = "select wkfwfile_no, wkfwfile_name from cas_wkfwfile_real";
	}
	$cond = "where wkfw_id = $wkfw_id order by wkfwfile_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$file_id = array();
	$filename = array();
	while ($row = pg_fetch_array($sel)) {
		$tmp_file_no = $row["wkfwfile_no"];
		$tmp_filename = $row["wkfwfile_name"];

		array_push($file_id, $tmp_file_no);
		array_push($filename, $tmp_filename);

		// 一時フォルダにコピー
		$ext = strrchr($tmp_filename, ".");

		if($mode == "ALIAS")
		{
			copy("cas_workflow/{$wkfw_id}_{$tmp_file_no}{$ext}", "cas_workflow/tmp/{$session}_{$tmp_file_no}{$ext}");
		}
		else
		{
			copy("cas_workflow/real/{$wkfw_id}_{$tmp_file_no}{$ext}", "cas_workflow/tmp/{$session}_{$tmp_file_no}{$ext}");
		}
	}

	// 前提とする申請書取得
	$arr_wkfwfprecond = $obj->get_wkfwfprecond($wkfw_id, $mode);
	$precond_wkfw_id = array();
	$precond_wkfw_title = array();
	foreach($arr_wkfwfprecond as $wkfwfprecond)
	{
		array_push($precond_wkfw_id, $wkfwfprecond["precond_wkfw_id"]);
		array_push($precond_wkfw_title, $wkfwfprecond["wkfw_title"]);
	}

	// デフォルト値の設定
	$ref_dept_flg = ($ref_dept_flg == "") ? "1" : $ref_dept_flg;
	$ref_st_flg = ($ref_st_flg == "") ? "1" : $ref_st_flg;


//	if ($ref_toggle_mode == "") {$ref_toggle_mode = "▼";}
//	$ref_toggle_display = ($ref_toggle_mode == "▼") ? "none" : "";

}

// メンバー情報を配列に格納
$arr_target['1'] = array();
if ($target_id_list1 != "")
{
	$arr_target_id = split(",", $target_id_list1);
	$arr_target["1"] = array();
	for ($i = 0; $i < count($arr_target_id); $i++)
	{
		$tmp_emp_id = $arr_target_id[$i];
		$sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
		$cond = "where emp_id = '$tmp_emp_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
		}
		$tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
		array_push($arr_target["1"], array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
	}
}


// 30分以上前に保存されたファイルを削除
if (!is_dir("cas_workflow")) {
	mkdir("cas_workflow", 0755);
}
if (!is_dir("cas_workflow/tmp")) {
	mkdir("cas_workflow/tmp", 0755);
}
foreach (glob("cas_workflow/tmp/*.*") as $tmpfile) {
	if (time() - filemtime($tmpfile) >= 30 * 60) {
		unlink($tmpfile);
	}
}

if($mode == "ALIAS")
{
	// カテゴリ名
	if($wkfw_type != "")
	{
		$wkfl_mst = $obj->get_wkfwcate_mst($wkfw_type);
		$folder_path = $wkfl_mst["wkfw_nm"];
	}
	// フォルダ名
	if($wkfw_folder_id != "")
	{
		$folder_list = $obj->get_folder_path($wkfw_folder_id);
		foreach($folder_list as $folder)
		{
			if($folder_path != "")
			{
				$folder_path .= " > ";
			}
			$folder_path .= $folder["name"];
		}
	}

	if($folder_path == "")
	{
		$folder_path = "選択してください";
	}

	$folder_path = htmlspecialchars($folder_path, ENT_QUOTES);
	$folder_path = str_replace("&#039;", "\'", $folder_path);
	$folder_path = str_replace("&quot;", "\"", $folder_path);
}

if ($ref_toggle_mode == "") {$ref_toggle_mode = "▼";}
$ref_toggle_display = ($ref_toggle_mode == "▼") ? "none" : "";

$cas_title = get_cas_title_name();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cas_title?> | ワークフロー更新</title>
<script type="text/javascript" src="js/fontsize.js"></script>

<?
require_once("emplist_caller_javascript.php");
insert_javascript();
?>
<script type="text/javascript">
var childwin = null;
function openEmployeeList(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 700;
	var url = './emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=$emp_id?>';
	url += '&mode=16';
	url += '&item_id='+item_id;
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}


function initPage()
{
	<?
	if($mode == "ALIAS"){
	?>
    document.getElementById('folder_path').innerHTML = '<?=$folder_path?>';
	<?
	}
	?>

    setRefClassSrcOptions(true, '<? echo($ref_class_src); ?>', '<? echo($ref_atrb_src); ?>');
    setDisabled();
    update_target_html("1");

}

function checkApprove(){

	var ref_dept_box = document.wkfw.ref_dept;
	if (!ref_dept_box.disabled) {
		for (var i = 0, j = ref_dept_box.length; i < j; i++) {
			addHiddenElement(document.wkfw, 'hid_ref_dept[]', ref_dept_box.options[i].value);
		}
	}
	document.wkfw.ref_toggle_mode.value = document.getElementById('ref_toggle').innerHTML;
	document.wkfw.action = "cas_workflow_template.php";
	document.wkfw.submit();
}

function attachFile() {
	window.open('cas_workflow_attach.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function detachFile(e) {
	if (e == undefined) {
		e = window.event;
	}

	var btn_id;
	if (e.target) {
		btn_id = e.target.getAttribute('id');
	} else {
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_', '');

	var p = document.getElementById('p_' + id);
	document.getElementById('attach').removeChild(p);
}

function submitForm() {
	var circulation = document.wkfw.wkfw_appr[1].checked;
	if (circulation) {
		for (var i = 0, j = document.wkfw.elements.length; i < j; i++) {
			if (document.wkfw.elements[i].type == 'checkbox') {
				document.wkfw.elements[i].disabled = false;
			}
		}
	}

	var ref_dept_box = document.wkfw.ref_dept;
	if (!ref_dept_box.disabled) {
		for (var i = 0, j = ref_dept_box.length; i < j; i++) {
			addHiddenElement(document.wkfw, 'hid_ref_dept[]', ref_dept_box.options[i].value);
		}
	}

	document.wkfw.ref_toggle_mode.value = document.getElementById('ref_toggle').innerHTML;

	closeEmployeeList();

	document.wkfw.submit();
}

function referTemplate() {
	window.open('cas_workflow_template_refer.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function submitPreviewForm() {
	document.wkfw.action = "cas_workflow_template.php";
	document.wkfw.preview_flg.value = "1";
	document.wkfw.submit();
}

function show_preview_window(url){
	var option = "directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=500,top=100,width=650,height=700";
	window.open(url, 'preview_window',option);
}

function openFolderSetting()
{
    var cate_id   = document.getElementById('wkfw_type').value;
    var folder_id = document.getElementById('wkfw_folder_id').value;
    window.open('cas_workflow_folder_setting.php?session=<?=$session?>&screen_mode=workflow&selected_cate=' + cate_id + '&selected_folder=' + folder_id, 'newwin', 'width=640,height=700,scrollbars=yes');
}

function setCategory(folder_name, cate_id, folder_id)
{
    document.getElementById('folder_path').innerHTML = folder_name;
    document.getElementById('wkfw_type').value = cate_id;
    document.getElementById('wkfw_folder_id').value = folder_id;
}

// 前提とする申請書追加
function addPrecond()
{
	window.open('cas_workflow_setting.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=600,scrollbars=yes');
}

// 前提とする申請書削除
function delPrecond(e) {
	if (e == undefined) {
		e = window.event;
	}

	var btn_id;
	if (e.target) {
		btn_id = e.target.getAttribute('id');
	} else {
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_precond_', '');

	var p = document.getElementById('p_precond_' + id);
	document.getElementById('precond_wkfw').removeChild(p);

	sel_precond_wkfw_id = document.getElementById('sel_precond_wkfw_id').value;
	arr_sel_precond_wkfw_id = sel_precond_wkfw_id.split(",");
	tmp_precond_wkfw_id = "";
	for(i=0; i<arr_sel_precond_wkfw_id.length;i++)
	{
		if(arr_sel_precond_wkfw_id[i] != id)
		{
			if(tmp_precond_wkfw_id != "")
			{
				tmp_precond_wkfw_id += ",";
			}
			tmp_precond_wkfw_id += arr_sel_precond_wkfw_id[i];
		}
	}
	document.getElementById('sel_precond_wkfw_id').value = tmp_precond_wkfw_id;
}



var classes = new Array();
<?
while ($row = pg_fetch_array($sel_class)) {
	$tmp_class_id = $row["class_id"];
	$tmp_class_nm = $row["class_nm"];
	echo("classes[$tmp_class_id] = '$tmp_class_nm';\n");
}
pg_result_seek($sel_class, 0);
?>

var atrbs = new Array();
<?
$pre_class_id = "";
while ($row = pg_fetch_array($sel_atrb)) {
	$tmp_class_id = $row["class_id"];
	$tmp_atrb_id = $row["atrb_id"];
	$tmp_class_nm = $row["class_nm"];
	$tmp_atrb_nm = $row["atrb_nm"];

	if ($tmp_class_id != $pre_class_id) {
		echo("atrbs[$tmp_class_id] = new Array();\n");
	}

	echo("atrbs[$tmp_class_id][$tmp_atrb_id] = '$tmp_atrb_nm';\n");

	$pre_class_id = $tmp_class_id;
}
pg_result_seek($sel_atrb, 0);
?>

var depts = new Array();
<?
$pre_class_id = "";
$pre_atrb_id = "";
while ($row = pg_fetch_array($sel_dept)) {
	$tmp_class_id = $row["class_id"];
	$tmp_atrb_id = $row["atrb_id"];
	$tmp_dept_id = $row["dept_id"];
	$tmp_class_nm = $row["class_nm"];
	$tmp_atrb_nm = $row["atrb_nm"];
	$tmp_dept_nm = $row["dept_nm"];

	if ($tmp_class_id != $pre_class_id) {
		echo("depts[$tmp_class_id] = new Array();\n");
	}

	if ($tmp_class_id != $pre_class_id || $tmp_atrb_id != $pre_atrb_id) {
		echo("depts[$tmp_class_id][$tmp_atrb_id] = new Array();\n");
	}

	echo("depts[$tmp_class_id][$tmp_atrb_id][$tmp_dept_id] = '$tmp_dept_nm';\n");

	$pre_class_id = $tmp_class_id;
	$pre_atrb_id = $tmp_atrb_id;
}
pg_result_seek($sel_dept, 0);
?>




function setRefClassSrcOptions(init_flg, default_ref_class_src, default_ref_atrb_src) {

	if (!init_flg) {
		deleteAllOptions(document.getElementById('ref_dept'));
	}
	deleteAllOptions(document.getElementById('ref_class_src'));

	//addOption(document.lib.ref_class_src, '-', '----------', default_ref_class_src);
	addOption(document.getElementById('ref_class_src'), '-', '----------', default_ref_class_src);

	for (var i in classes) {
		addOption(document.getElementById('ref_class_src'), i, classes[i], default_ref_class_src);
	}

	setRefAtrbSrcOptions(default_ref_atrb_src);
}

function setRefAtrbSrcOptions(default_ref_atrb_src) {

	deleteAllOptions(document.getElementById('ref_atrb_src'));
	addOption(document.getElementById('ref_atrb_src'), '-', '----------', default_ref_atrb_src);

	var class_id = document.getElementById('ref_class_src').value;
	if (atrbs[class_id]) {
		for (var i in atrbs[class_id]) {
			//addOption(document.lib.ref_atrb_src, i, atrbs[class_id][i], default_ref_atrb_src);
			addOption(document.getElementById('ref_atrb_src'), i, atrbs[class_id][i], default_ref_atrb_src);
		}
	}

	setRefDeptSrcOptions();
}

function setRefDeptSrcOptions() {

	deleteAllOptions(document.getElementById('ref_dept_src'));
	var class_id = document.getElementById('ref_class_src').value;
	var atrb_id = document.getElementById('ref_atrb_src').value;

	if (depts[class_id]) {
		if (depts[class_id][atrb_id]) {
			for (var i in depts[class_id][atrb_id]) {
				var value = class_id + '-' + atrb_id + '-' + i;
				//addOption(document.lib.ref_dept_src, value, depts[class_id][atrb_id][i]);
				addOption(document.getElementById('ref_dept_src'), value, depts[class_id][atrb_id][i]);

			}
		} else if (atrb_id == '-') {
			for (var i in atrbs[class_id]) {
				for (var j in depts[class_id][i]) {
					var value = class_id + '-' + i + '-' + j;
					//addOption(document.lib.ref_dept_src, value, depts[class_id][i][j]);
					addOption(document.getElementById('ref_dept_src'), value, depts[class_id][i][j]);
				}
			}
		}
	}
}

function deleteAllOptions(box)
{
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}


function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	if (!box.multiple) {
		try {box.style.fontSize = 'auto';} catch (e) {}
		box.style.overflow = 'auto';
	}
}

function setDisabled()
{
	var disabled;
	disabled = !(!document.getElementById('ref_dept_st_flg').disabled && !document.getElementById('ref_dept_st_flg').checked);

	var ref_dept_flg = document.getElementsByName('ref_dept_flg');
	ref_dept_flg[0].disabled = disabled;
	ref_dept_flg[1].disabled = disabled;

	disabled = !(!ref_dept_flg[1].disabled && ref_dept_flg[1].checked);

	document.getElementById('ref_class_src').disabled = disabled;
	document.getElementById('ref_atrb_src').disabled = disabled;
	document.getElementById('ref_dept_src').disabled = disabled;
	document.getElementById('ref_dept').disabled = disabled;
	document.getElementById('add_ref_dept').disabled = disabled;
	document.getElementById('delete_ref_dept').disabled = disabled;
	document.getElementById('delete_all_ref_dept').disabled = disabled;
	document.getElementById('select_all_ref_dept').disabled = disabled;

	disabled = (document.getElementById('ref_dept_st_flg').disabled || document.getElementById('ref_dept_st_flg').checked);

	var ref_st_flg = document.getElementsByName('ref_st_flg');
	ref_st_flg[0].disabled = disabled;
	ref_st_flg[1].disabled = disabled;

	disabled = !(!ref_st_flg[1].disabled && ref_st_flg[1].checked);
	document.getElementById('ref_st[]').disabled = disabled;
}

function addSelectedOptions(dest_box, src_box) {
	var options = new Array();
	for (var i = 0, j = dest_box.length; i < j; i++) {
		options[dest_box.options[i].value] = dest_box.options[i].text;
	}
	deleteAllOptions(dest_box);
	for (var i = 0, j = src_box.length; i < j; i++) {
		if (src_box.options[i].selected) {
			options[src_box.options[i].value] = src_box.options[i].text;
		}
	}
	options.sort();

	for (var i in options) {
		addOption(dest_box, i, options[i]);
	}
}

function deleteSelectedOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		if (box.options[i].selected) {
			box.options[i] = null;
		}
	}
}

function selectAllOptions(box) {
	for (var i = 0, j = box.length; i < j; i++) {
		box.options[i].selected = true;
	}
}

function toggle(button)
{
	var display;
	if (button.innerHTML == '▼') {
		button.innerHTML = '▲';
		display = '';
	} else {
		button.innerHTML = '▼';
		display = 'none';
	}
	document.getElementById('ref_toggle1').style.display = display;
}


function closeEmployeeList() {
	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;
}

// クリア
function clear_target(item_id, emp_id,emp_name) {
	if(confirm("登録対象者を削除します。よろしいですか？"))
	{
		var is_exist_flg = false;
		for(var i=0;i<m_target_list[item_id].length;i++)
		{
			if(emp_id == m_target_list[item_id][i].emp_id)
			{
				is_exist_flg = true;
				break;
			}
		}
		m_target_list[item_id] = new Array();
		if (is_exist_flg == true) {
			m_target_list[item_id] = array_add(m_target_list[item_id],new user_info(emp_id,emp_name));
		}
		update_target_html(item_id);
	}
}

function clear_notice()
{
	document.getElementById('notice_content').innerHTML = '';
	document.getElementById('notice').value = '';

	document.getElementById('rslt_ntc_div0_flg').value = '';
	document.getElementById('rslt_ntc_div1_flg').value = '';
	document.getElementById('rslt_ntc_div2_flg').value = '';
	document.getElementById('rslt_ntc_div3_flg').value = '';
	document.getElementById('rslt_ntc_div4_flg').value = '';

	document.getElementById('notice_target_class_div').value = '';
	document.getElementById('notice_st_id').value = '';
	document.getElementById('notice_emp_id').value = '';
	document.getElementById('notice_emp_nm').value = '';
	document.getElementById('notice_pjt_parent_id').value = '';
	document.getElementById('notice_pjt_child_id').value = '';
	document.getElementById('notice_pjt_parent_nm').value = '';
	document.getElementById('notice_pjt_child_nm').value = '';

	document.getElementById('notice_class_sect_id').value = '';
	document.getElementById('notice_atrb_sect_id').value = '';
	document.getElementById('notice_dept_sect_id').value = '';
	document.getElementById('notice_room_sect_id').value = '';
	document.getElementById('notice_st_sect_id').value = '';
}

function addHiddenElement(frm, name, value) {
	var input = document.createElement('input');
	input.type = 'hidden';
	input.name = name;
	input.value = value;
	frm.appendChild(input);
}


//--------------------------------------------------
//登録対象者一覧
//--------------------------------------------------
<?
	$script = "m_target_list['1'] = new Array(";
	$is_first = true;
	foreach($arr_target["1"] as $row)
	{
		if($is_first)
		{
			$is_first = false;
		}
		else
		{
			$script .= ",";
		}
		$tmp_emp_id = $row["id"];
		$tmp_emp_name = $row["name"];
		$script .= "new user_info('$tmp_emp_id','$tmp_emp_name')";
	}
	$script .= ");\n";
	print $script;
?>

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse; border:#A0D25A solid 1px;}
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="cas_workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="cas_workflow_menu.php?session=<? echo($session); ?>"><b>ワークフロー</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="cas_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/s42.gif" width="32" height="32" border="0" alt="<? echo($cas_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="cas_application_menu.php?session=<? echo($session); ?>"><b><? echo($cas_title); ?></b></a> &gt; <a href="cas_workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="cas_application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
$option = array("wkfw_id" => $wkfw_id, "mode" => $mode);
show_wkfw_menuitem($session, $fname, $option);
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="wkfw" action="cas_workflow_update.php" method="post">
<table width="860" border="0" cellspacing="0" cellpadding="2" class="list">
<!-- 承認タイプ -->
<tr height="22">
<td width="160" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認タイプ</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="wkfw_appr" value="1"<? if ($wkfw_appr == "1") {echo(" checked");} ?>>同報
<input type="radio" name="wkfw_appr" value="2"<? if ($wkfw_appr == "2") {echo(" checked");} ?>>稟議（回覧）
</font></td>
</tr>
<!-- 管理ＣＤ -->
<tr height="22">
<td width="160" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理ＣＤ</font></td>
<?
if($mode == "REAL")
{
?>
<td colspan="2"><input name="short_wkfw_name" type="text" size="5" maxlength="4" value="<?=htmlspecialchars($short_wkfw_name);?>" style="ime-mode:inactive;"></td>
<?
}
else
{
?>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($short_wkfw_name);?></font></td>
<input type="hidden" name="short_wkfw_name" value="<?=htmlspecialchars($short_wkfw_name);?>">
<?
}
?>

</tr>
<!-- 表題の表示・非表示 -->
<tr height="22">
<td width="160" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題の表示・非表示</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="apply_title_disp_flg" value="t"<? if ($apply_title_disp_flg == "t") {echo(" checked");} ?>>表示
<input type="radio" name="apply_title_disp_flg" value="f"<? if ($apply_title_disp_flg == "f") {echo(" checked");} ?>>非表示
</font></td>
</tr>
<!-- 承認階層 -->
<tr height="22">
<td width="160" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認階層数</font></td>
<td colspan="2"><select name="approve_num" onChange="checkApprove();"><option value="0">選択してください</option><?
for ($i = 1; $i <= 20; $i++) {
	echo("<option value=\"$i\"");
	if ($approve_num == $i) {
		echo(" selected");
	}
	echo(">$i</option>\n");
}
?>
</select></td>
</tr>
<?
$approve = array($approve1, $approve2, $approve3, $approve4, $approve5, $approve6, $approve7, $approve8, $approve9, $approve10, $approve11, $approve12, $approve13, $approve14, $approve15, $approve16, $approve17, $approve18, $approve19, $approve20);

$apv_div0_flg = array($apv_div0_flg1, $apv_div0_flg2, $apv_div0_flg3, $apv_div0_flg4, $apv_div0_flg5, $apv_div0_flg6, $apv_div0_flg7, $apv_div0_flg8, $apv_div0_flg9, $apv_div0_flg10, $apv_div0_flg11, $apv_div0_flg12, $apv_div0_flg13, $apv_div0_flg14, $apv_div0_flg15, $apv_div0_flg16, $apv_div0_flg17, $apv_div0_flg18, $apv_div0_flg19, $apv_div0_flg20);
$apv_div1_flg = array($apv_div1_flg1, $apv_div1_flg2, $apv_div1_flg3, $apv_div1_flg4, $apv_div1_flg5, $apv_div1_flg6, $apv_div1_flg7, $apv_div1_flg8, $apv_div1_flg9, $apv_div1_flg10, $apv_div1_flg11, $apv_div1_flg12, $apv_div1_flg13, $apv_div1_flg14, $apv_div1_flg15, $apv_div1_flg16, $apv_div1_flg17, $apv_div1_flg18, $apv_div1_flg19, $apv_div1_flg20);
$apv_div2_flg = array($apv_div2_flg1, $apv_div2_flg2, $apv_div2_flg3, $apv_div2_flg4, $apv_div2_flg5, $apv_div2_flg6, $apv_div2_flg7, $apv_div2_flg8, $apv_div2_flg9, $apv_div2_flg10, $apv_div2_flg11, $apv_div2_flg12, $apv_div2_flg13, $apv_div2_flg14, $apv_div2_flg15, $apv_div2_flg16, $apv_div2_flg17, $apv_div2_flg18, $apv_div2_flg19, $apv_div2_flg20);
$apv_div3_flg = array($apv_div3_flg1, $apv_div3_flg2, $apv_div3_flg3, $apv_div3_flg4, $apv_div3_flg5, $apv_div3_flg6, $apv_div3_flg7, $apv_div3_flg8, $apv_div3_flg9, $apv_div3_flg10, $apv_div3_flg11, $apv_div3_flg12, $apv_div3_flg13, $apv_div3_flg14, $apv_div3_flg15, $apv_div3_flg16, $apv_div3_flg17, $apv_div3_flg18, $apv_div3_flg19, $apv_div3_flg20);
$apv_div4_flg = array($apv_div4_flg1, $apv_div4_flg2, $apv_div4_flg3, $apv_div4_flg4, $apv_div4_flg5, $apv_div4_flg6, $apv_div4_flg7, $apv_div4_flg8, $apv_div4_flg9, $apv_div4_flg10, $apv_div4_flg11, $apv_div4_flg12, $apv_div4_flg13, $apv_div4_flg14, $apv_div4_flg15, $apv_div4_flg16, $apv_div4_flg17, $apv_div4_flg18, $apv_div4_flg19, $apv_div4_flg20);


$target_class_div = array($target_class_div1, $target_class_div2, $target_class_div3, $target_class_div4, $target_class_div5, $target_class_div6, $target_class_div7, $target_class_div8, $target_class_div9, $target_class_div10, $target_class_div11, $target_class_div12, $target_class_div13, $target_class_div14, $target_class_div15, $target_class_div16, $target_class_div17, $target_class_div18, $target_class_div19, $target_class_div20);
$st_id = array($st_id1, $st_id2, $st_id3, $st_id4, $st_id5, $st_id6, $st_id7, $st_id8, $st_id9, $st_id10, $st_id11, $st_id12, $st_id13, $st_id14, $st_id15, $st_id16, $st_id17, $st_id18, $st_id19, $st_id20);
$emp_id = array($emp_id1, $emp_id2, $emp_id3, $emp_id4, $emp_id5, $emp_id6, $emp_id7, $emp_id8, $emp_id9, $emp_id10, $emp_id11, $emp_id12, $emp_id13, $emp_id14, $emp_id15, $emp_id16, $emp_id17, $emp_id18, $emp_id19, $emp_id20);
$emp_nm = array($emp_nm1, $emp_nm2, $emp_nm3, $emp_nm4, $emp_nm5, $emp_nm6, $emp_nm7, $emp_nm8, $emp_nm9, $emp_nm10, $emp_nm11, $emp_nm12, $emp_nm13, $emp_nm14, $emp_nm15, $emp_nm16, $emp_nm17, $emp_nm18, $emp_nm19, $emp_nm20);
$emp_del_flg = array($emp_del_flg1, $emp_del_flg2, $emp_del_flg3, $emp_del_flg4, $emp_del_flg5, $emp_del_flg6, $emp_del_flg7, $emp_del_flg8, $emp_del_flg9, $emp_del_flg10, $emp_del_flg11, $emp_del_flg12, $emp_del_flg13, $emp_del_flg14, $emp_del_flg15, $emp_del_flg16, $emp_del_flg17, $emp_del_flg18, $emp_del_flg19, $emp_del_flg20);
$pjt_parent_id = array($pjt_parent_id1, $pjt_parent_id2, $pjt_parent_id3, $pjt_parent_id4, $pjt_parent_id5, $pjt_parent_id6, $pjt_parent_id7, $pjt_parent_id8, $pjt_parent_id9, $pjt_parent_id10, $pjt_parent_id11, $pjt_parent_id12, $pjt_parent_id13, $pjt_parent_id14, $pjt_parent_id15, $pjt_parent_id16, $pjt_parent_id17, $pjt_parent_id18, $pjt_parent_id19, $pjt_parent_id20);
$pjt_child_id = array($pjt_child_id1, $pjt_child_id2, $pjt_child_id3, $pjt_child_id4, $pjt_child_id5, $pjt_child_id6, $pjt_child_id7, $pjt_child_id8, $pjt_child_id9, $pjt_child_id10, $pjt_child_id11, $pjt_child_id12, $pjt_child_id13, $pjt_child_id14, $pjt_child_id15, $pjt_child_id16, $pjt_child_id17, $pjt_child_id18, $pjt_child_id19, $pjt_child_id20);
$pjt_parent_nm = array($pjt_parent_nm1, $pjt_parent_nm2, $pjt_parent_nm3, $pjt_parent_nm4, $pjt_parent_nm5, $pjt_parent_nm6, $pjt_parent_nm7, $pjt_parent_nm8, $pjt_parent_nm9, $pjt_parent_nm10, $pjt_parent_nm11, $pjt_parent_nm12, $pjt_parent_nm13, $pjt_parent_nm14, $pjt_parent_nm15, $pjt_parent_nm16, $pjt_parent_nm17, $pjt_parent_nm18, $pjt_parent_nm19, $pjt_parent_nm20);
$pjt_child_nm = array($pjt_child_nm1, $pjt_child_nm2, $pjt_child_nm3, $pjt_child_nm4, $pjt_child_nm5, $pjt_child_nm6, $pjt_child_nm7, $pjt_child_nm8, $pjt_child_nm9, $pjt_child_nm10, $pjt_child_nm11, $pjt_child_nm12, $pjt_child_nm13, $pjt_child_nm14, $pjt_child_nm15, $pjt_child_nm16, $pjt_child_nm17, $pjt_child_nm18, $pjt_child_nm19, $pjt_child_nm20);

$class_sect_id = array($class_sect_id1, $class_sect_id2, $class_sect_id3, $class_sect_id4, $class_sect_id5, $class_sect_id6, $class_sect_id7, $class_sect_id8, $class_sect_id9, $class_sect_id10, $class_sect_id11, $class_sect_id12, $class_sect_id13, $class_sect_id14, $class_sect_id15, $class_sect_id16, $class_sect_id17, $class_sect_id18, $class_sect_id19, $class_sect_id20);
$atrb_sect_id = array($atrb_sect_id1, $atrb_sect_id2, $atrb_sect_id3, $atrb_sect_id4, $atrb_sect_id5, $atrb_sect_id6, $atrb_sect_id7, $atrb_sect_id8, $atrb_sect_id9, $atrb_sect_id10, $atrb_sect_id11, $atrb_sect_id12, $atrb_sect_id13, $atrb_sect_id14, $atrb_sect_id15, $atrb_sect_id16, $atrb_sect_id17, $atrb_sect_id18, $atrb_sect_id19, $atrb_sect_id20);
$dept_sect_id = array($dept_sect_id1, $dept_sect_id2, $dept_sect_id3, $dept_sect_id4, $dept_sect_id5, $dept_sect_id6, $dept_sect_id7, $dept_sect_id8, $dept_sect_id9, $dept_sect_id10, $dept_sect_id11, $dept_sect_id12, $dept_sect_id13, $dept_sect_id14, $dept_sect_id15, $dept_sect_id16, $dept_sect_id17, $dept_sect_id18, $dept_sect_id19, $dept_sect_id20);
$room_sect_id = array($room_sect_id1, $room_sect_id2, $room_sect_id3, $room_sect_id4, $room_sect_id5, $room_sect_id6, $room_sect_id7, $room_sect_id8, $room_sect_id9, $room_sect_id10, $room_sect_id11, $room_sect_id12, $room_sect_id13, $room_sect_id14, $room_sect_id15, $room_sect_id16, $room_sect_id17, $room_sect_id18, $room_sect_id19, $room_sect_id20);

$st_sect_id = array($st_sect_id1, $st_sect_id2, $st_sect_id3, $st_sect_id4, $st_sect_id5, $st_sect_id6, $st_sect_id7, $st_sect_id8, $st_sect_id9, $st_sect_id10, $st_sect_id11, $st_sect_id12, $st_sect_id13, $st_sect_id14, $st_sect_id15, $st_sect_id16, $st_sect_id17, $st_sect_id18, $st_sect_id19, $st_sect_id20);

$multi_apv_flg = array($multi_apv_flg1, $multi_apv_flg2, $multi_apv_flg3, $multi_apv_flg4, $multi_apv_flg5, $multi_apv_flg6, $multi_apv_flg7, $multi_apv_flg8, $multi_apv_flg9, $multi_apv_flg10, $multi_apv_flg11, $multi_apv_flg12, $multi_apv_flg13, $multi_apv_flg14, $multi_apv_flg15, $multi_apv_flg16, $multi_apv_flg17, $multi_apv_flg18, $multi_apv_flg19, $multi_apv_flg20);
$next_notice_div = array($next_notice_div1, $next_notice_div2, $next_notice_div3, $next_notice_div4, $next_notice_div5, $next_notice_div6, $next_notice_div7, $next_notice_div8, $next_notice_div9, $next_notice_div10, $next_notice_div11, $next_notice_div12, $next_notice_div13, $next_notice_div14, $next_notice_div15, $next_notice_div16, $next_notice_div17, $next_notice_div18, $next_notice_div19, $next_notice_div20);

$apv_num = array($apv_num1, $apv_num2, $apv_num3, $apv_num4, $apv_num5, $apv_num6, $apv_num7, $apv_num8, $apv_num9, $apv_num10, $apv_num11, $apv_num12, $apv_num13, $apv_num14, $apv_num15, $apv_num16, $apv_num17, $apv_num18, $apv_num19, $apv_num20);

//$deci_flg = array($deci_flg1, $deci_flg2, $deci_flg3, $deci_flg4, $deci_flg5, $deci_flg6, $deci_flg7, $deci_flg8, $deci_flg9, $deci_flg10, $deci_flg11, $deci_flg12, $deci_flg13, $deci_flg14, $deci_flg15, $deci_flg16, $deci_flg17, $deci_flg18, $deci_flg19, $deci_flg20);

for ($i = 0; $i < $approve_num; $i++) {
	$j = $i + 1;

//	if($emp_del_flg[$i] == 't') {
//		$text_color = '#FF0000';
//	} else {
		$text_color = '#000000';
//	}
	echo("<tr>\n");
	echo("<td height=\"22\" align=\"right\" bgcolor=\"#E5F6CD\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"javascript:void(0);\" onclick=\"window.open('cas_workflow_approve_list.php?session=$session&approve=$j', 'newwin2', 'width=640,height=700,scrollbars=yes')\">承認階層" . $j . "</a></font></td>\n");
	echo("<td colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"green\"><span id=\"approve_content". $j ."\"></span></font>");

//	echo("<td colspan=\"2\"><input name=\"approve_content" . $j . "\" type=\"text\" size=\"50\" maxlength=\"50\" value=\"".$approve[$i]."\" style=\"color:".$text_color."\" readonly>\n");
//	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">決定権</font><input type=\"checkbox\" name=\"deci_flg" . $j . "\"");
//	if ($deci_flg[$i] == "on") {
//		echo(" checked");
//	}
//	echo(">");

	$target_class_div[$i] = ($target_class_div[$i] == "") ? "1" : $target_class_div[$i];
	$multi_apv_flg[$i] = ($multi_apv_flg[$i] == "") ? "f" : $multi_apv_flg[$i];
	$next_notice_div[$i] = ($next_notice_div[$i] == "") ? "2" : $next_notice_div[$i];
	$apv_num[$i] = ($apv_num[$i] == "") ? "1" : $apv_num[$i];

	echo("<input type=\"hidden\" name=\"approve" . $j . "\" value=\"" . $approve[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"apv_div0_flg" . $j . "\" value=\"" . $apv_div0_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"apv_div1_flg" . $j . "\" value=\"" . $apv_div1_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"apv_div2_flg" . $j . "\" value=\"" . $apv_div2_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"apv_div3_flg" . $j . "\" value=\"" . $apv_div3_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"apv_div4_flg" . $j . "\" value=\"" . $apv_div4_flg[$i] . "\">\n");


	echo("<input type=\"hidden\" name=\"target_class_div" . $j . "\" value=\"" . $target_class_div[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"st_id" . $j . "\" value=\"" . $st_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"emp_id" . $j . "\" value=\"" . $emp_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"emp_nm" . $j . "\" value=\"" . $emp_nm[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"pjt_parent_id" . $j . "\" value=\"" . $pjt_parent_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"pjt_child_id" . $j . "\" value=\"" . $pjt_child_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"pjt_parent_nm" . $j . "\" value=\"" . $pjt_parent_nm[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"pjt_child_nm" . $j . "\" value=\"" . $pjt_child_nm[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"class_sect_id" . $j . "\" value=\"" . $class_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"atrb_sect_id" . $j . "\" value=\"" . $atrb_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"dept_sect_id" . $j . "\" value=\"" . $dept_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"room_sect_id" . $j . "\" value=\"" . $room_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"st_sect_id" . $j . "\" value=\"" . $st_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"multi_apv_flg" . $j . "\" value=\"" . $multi_apv_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"next_notice_div" . $j . "\" value=\"" . $next_notice_div[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"apv_num" . $j . "\" value=\"" . $apv_num[$i] . "\">\n");

	echo("</td>\n");
	echo("</tr>\n");
}
?>
<?
if($mode == "ALIAS")
{
?>
<!-- フォルダ -->
<tr height="22">
<td width="160" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font></td>
<td colspan="2">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<span id="folder_path" style="padding-right:2px;"></span>
<input type="button" value="選択" onclick="openFolderSetting();">
</font>
</td>
</tr>
<?
}
?>
<!-- ワークフロー名 -->
<tr height="22">
<td width="160" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ワークフロー名</font></td>
<td colspan="2"><input name="wkfw_title" type="text" size="50" maxlength="50" value="<?=htmlspecialchars($wkfw_title);?>" style="ime-mode: active;"></td>
</tr>
<!-- 本文形式 -->
<tr height="22">
<td width="160" width="160" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">本文形式</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="wkfw_content_type" value="1"<? if ($wkfw_content_type == "1") {echo(" checked");} ?> onclick="document.wkfw.referbtn.disabled = true;
<?
if ($can_regist_flg == false && $wkfw_content_type == "1") {
	echo("document.wkfw.update.disabled = false;");
}
?>
">テキスト
<input type="radio" name="wkfw_content_type" value="2"<? if ($wkfw_content_type == "2") {echo(" checked");} ?> onclick="document.wkfw.referbtn.disabled = false;
<?
if ($can_regist_flg == false) {
	echo("document.wkfw.update.disabled = true;");
}
?>
">テンプレート（ＰＨＰ）
</font>
<input type="button" name="referbtn" value="参照" onclick="referTemplate();">
</td>
</tr>
<tr>
<td colspan="3">
<textarea name="wkfw_content" rows="15" cols="80">
<?
$wkfw_content_tmp = eregi_replace("/(textarea)", "_\\1", $wkfw_content);
echo($wkfw_content_tmp);
?>
</textarea></td>
</tr>

<!-- 申請者以外への結果通知 -->
<tr height="100">
<td width="160" align="right" bgcolor="#E5F6CD">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者以外への結果通知</font><br>
<input type="button" value="設定" style="margin-left:2em;width=4.0em;" onclick="window.open('cas_workflow_notice_list.php?session=<?=$session?>', 'newwin2', 'width=640,height=700,scrollbars=yes');"><br>
<input type="button" value="クリア" style="margin-left:2em;width=4.0em;" onclick="clear_notice();">
</td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="green"><span id="notice_content"></span></font></td>
</font></td>
</tr>

<input type="hidden" name="notice" id="notice" value="<?=$notice?>">

<input type="hidden" name="rslt_ntc_div0_flg" id="rslt_ntc_div0_flg" value="<?=$rslt_ntc_div0_flg?>">
<input type="hidden" name="rslt_ntc_div1_flg" id="rslt_ntc_div1_flg" value="<?=$rslt_ntc_div1_flg?>">
<input type="hidden" name="rslt_ntc_div2_flg" id="rslt_ntc_div2_flg" value="<?=$rslt_ntc_div2_flg?>">
<input type="hidden" name="rslt_ntc_div3_flg" id="rslt_ntc_div3_flg" value="<?=$rslt_ntc_div3_flg?>">
<input type="hidden" name="rslt_ntc_div4_flg" id="rslt_ntc_div4_flg" value="<?=$rslt_ntc_div4_flg?>">

<input type="hidden" name="notice_target_class_div" value="<?=$notice_target_class_div?>">
<input type="hidden" name="notice_st_id" value="<?=$notice_st_id?>">
<input type="hidden" name="notice_emp_id" value="<?=$notice_emp_id?>">
<input type="hidden" name="notice_emp_nm" value="<?=$notice_emp_nm?>">
<input type="hidden" name="notice_pjt_parent_id" value="<?=$notice_pjt_parent_id?>">
<input type="hidden" name="notice_pjt_child_id" value="<?=$notice_pjt_child_id?>">
<input type="hidden" name="notice_pjt_parent_nm" value="<?=$notice_pjt_parent_nm?>">
<input type="hidden" name="notice_pjt_child_nm" value="<?=$notice_pjt_child_nm?>">

<input type="hidden" name="notice_class_sect_id" id="notice_class_sect_id" value="<?=$notice_class_sect_id?>">
<input type="hidden" name="notice_atrb_sect_id" id="notice_atrb_sect_id" value="<?=$notice_atrb_sect_id?>">
<input type="hidden" name="notice_dept_sect_id" id="notice_dept_sect_id" value="<?=$notice_dept_sect_id?>">
<input type="hidden" name="notice_room_sect_id" id="notice_room_sect_id" value="<?=$notice_room_sect_id?>">
<input type="hidden" name="notice_st_sect_id" id="notice_st_sect_id" value="<?=$notice_st_sect_id?>">

<!-- 前提とする申請書 -->
<tr height="22">
<td width="160" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前提とする申請書</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<span id="precond_wkfw">
<?
for ($i = 0; $i < count($precond_wkfw_id); $i++) {
	$tmp_wkfw_id = $precond_wkfw_id[$i];
	$tmp_wkfw_title = $precond_wkfw_title[$i];

	echo("<p id=\"p_precond_{$tmp_wkfw_id}\">\n");
	echo("{$tmp_wkfw_title}\n");
	echo("<input type=\"button\" id=\"btn_precond_{$tmp_wkfw_id}\" name=\"btn_precond_{$tmp_wkfw_id}\" value=\"削除\" onclick=\"delPrecond(event);\">\n");
	echo("<input type=\"hidden\" name=\"precond_wkfw_title[]\" value=\"{$tmp_wkfw_title}\">\n");
	echo("<input type=\"hidden\" name=\"precond_wkfw_id[]\" value=\"{$tmp_wkfw_id}\">\n");
	echo("</p>\n");
}
?>
</span>
</font>
<input type="button" value="追加" onclick="addPrecond();">
</td>
</tr>
<input type="hidden" name="sel_precond_wkfw_id" id="sel_precond_wkfw_id" value="">


<!-- フォーマットファイル -->
<tr height="22">
<td width="160" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォーマットファイル</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<div id="attach">
<?
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$ext = strrchr($tmp_filename, ".");

	echo("<p id=\"p_{$tmp_file_id}\">\n");
	echo("<a href=\"cas_workflow/tmp/{$session}_{$tmp_file_id}{$ext}\" target=\"_blank\">{$tmp_filename}</a>\n");
	echo("<input type=\"button\" id=\"btn_{$tmp_file_id}\" name=\"btn_{$tmp_file_id}\" value=\"削除\" onclick=\"detachFile(event);\">\n");
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"{$tmp_filename}\">\n");
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"{$tmp_file_id}\">\n");
	echo("</p>\n");
}
?>
</div>
<input type="button" value="追加" onclick="attachFile();">
</font></td>
</tr>
<!-- 利用期間 -->
<tr height="22">
<td width="160" align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">利用期間</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="start_year"><? show_update_years($start_year, 1, true); ?></select>/<select name="start_month"><? show_select_months($start_month, true); ?></select>/<select name="start_day"><? show_select_days($start_day, true); ?></select> 〜 <select name="end_year"><? show_update_years($end_year, 1, true); ?></select>/<select name="end_month"><? show_select_months($end_month, true); ?></select>/<select name="end_day"><? show_select_days($end_day, true); ?></select></font><br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">※常に利用する場合は設定不要です</font>
</td>
</tr>


<!-- 利用可能範囲の設定　-->
<tr height="22">
<td bgcolor="#E5F6CD" colspan="3" class="spacing">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<span style="cursor:pointer;" onclick="toggle(document.getElementById('ref_toggle'));">利用可能範囲の設定&nbsp;</span>
<span id="ref_toggle" style="cursor:pointer;" onclick="toggle(this);"><? echo($ref_toggle_mode); ?></span>
</font>
</td>
</tr>
</table>

<div id="ref_toggle1" style="display:<? echo($ref_toggle_display); ?>;">
<table width="860" border="0" cellspacing="0" cellpadding="2" class="list" style="position:relative;top:-1px;">
<tr height="22">
<td width="160" bgcolor="#E5F6CD" rowspan="2" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署・役職<br><input type="checkbox" name="ref_dept_st_flg" id="ref_dept_st_flg" value="t"<? if ($ref_dept_st_flg == "t") {echo(" checked");} ?> onclick="setDisabled();">許可しない</font></td>
<td width="480" bgcolor="#E5F6CD" class="spacing" width="400"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
<td width="220" bgcolor="#E5F6CD" class="spacing" width="300"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
</tr>

<tr>
<td>
<table cellspacing="0" cellpadding="0" border="0" class="non_in_list">
<tr>
<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="ref_dept_flg" id="ref_dept_flg" value="1"<? if ($ref_dept_flg == "1") {echo(" checked");} ?> onclick="setDisabled();">すべて<br>
<input type="radio" name="ref_dept_flg" id="ref_dept_flg" value="2"<? if ($ref_dept_flg == "2") {echo(" checked");} ?> onclick="setDisabled();">指定する
</font>
</td>
<td>
<table cellspacing="2" cellpadding="0" border="0">
<tr>
<td valign="bottom" style="position:relative;top:3px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">可能とする<? echo($arr_class_name[2]); ?></font></td>
<td></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="ref_class_src" id="ref_class_src" onchange="setRefAtrbSrcOptions();">
</select><? echo($arr_class_name[0]); ?><br>
<select name="ref_atrb_src" id="ref_atrb_src" onchange="setRefDeptSrcOptions();">
</select><? echo($arr_class_name[1]); ?>
</font></td>
</tr>
<tr>
<td>
<select name="ref_dept" id="ref_dept" size="6" multiple style="width:120px;">
	<?
	foreach ($ref_dept as $tmp_dept_id) {
		echo("<option value=\"$tmp_dept_id\">{$dept_names[$tmp_dept_id]}");
	}
	?>
</select>
</td>
<td align="center"><input type="button" name="add_ref_dept" id="add_ref_dept" value=" &lt; " onclick="addSelectedOptions(this.form.ref_dept, this.form.ref_dept_src);"><br><br><input type="button" name="delete_ref_dept" id="delete_ref_dept" value=" &gt; " onclick="deleteSelectedOptions(this.form.ref_dept);"></td>
<td><select name="ref_dept_src" id="ref_dept_src" size="6" multiple style="width:120px;"></select></td>
</tr>
<tr>
<td><input type="button" name="delete_all_ref_dept" id="delete_all_ref_dept" value="全て消去" onclick="deleteAllOptions(this.form.ref_dept);"></td>
<td></td>
<td><input type="button" name="select_all_ref_dept" id="select_all_ref_dept" value="全て選択" onclick="selectAllOptions(this.form.ref_dept_src);"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<td>
<table cellspacing="0" cellpadding="0" border="0" class="non_in_list">
<tr>
<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="ref_st_flg" id="ref_st_flg" value="1"<? if ($ref_st_flg == "1") {echo(" checked");} ?> onclick="setDisabled();">すべて<br>
<input type="radio" name="ref_st_flg" id="ref_st_flg" value="2"<? if ($ref_st_flg == "2") {echo(" checked");} ?> onclick="setDisabled();">指定する
</font></td>
<td>
<table cellspacing="2" cellpadding="0" border="0">
<tr>
<td>
<select name="ref_st[]" id="ref_st[]" size="10" multiple>
<?
while ($row = pg_fetch_array($sel_st)) {
	$tmp_st_id = $row["st_id"];
	$tmp_st_nm = $row["st_nm"];
	echo("<option value=\"$tmp_st_id\"");
	if (in_array($tmp_st_id, $ref_st)) {
		echo(" selected");
	}
	echo(">$tmp_st_nm");
}
pg_result_seek($sel_st, 0);
?>
</select>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td width="160" bgcolor="#E5F6CD" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
<input type="button" name="emplist1" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList('1');"><br>
<input type="button" value="クリア" style="margin-left:2em;width=5.5em;" onclick="clear_target('1','<?=$emp_id?>','<?=$emp_name?>');"><br></td>
<td colspan="2">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
<tr>
<td width="350" height="60" style="border:#A0D25A solid 1px;">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<span id="target_disp_area1"></span>
</font>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>


<table width="860" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right">
<input type="button" name="prevbtn" value="プレビュー" onclick="submitPreviewForm();">
<input type="button" name="update" value="更新" onclick="submitForm();"
<?
if ($can_regist_flg == false && $wkfw_content_type == "2") {
	echo(" disabled");
}
?>
></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="wkfw_id" id="wkfw_id" value="<? echo($wkfw_id); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="preview_flg" value="">
<input type="hidden" name="wkfw_type" id="wkfw_type" value="<?=$wkfw_type?>">
<input type="hidden" name="wkfw_folder_id" id="wkfw_folder_id" value="<?=$wkfw_folder_id?>">
<input type="hidden" name="ref_toggle_mode" value="">
<input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
<input type="hidden" id="target_name_list1" name="target_name_list1" value="">
<input type="hidden" name="mode" value="<?=$mode?>">
</form>
</td>
</tr>
</table>
<script type="text/javascript">
<?
// プレビュー押下時
if ($preview_flg == "1") {


// wkfw_content 保存
$ext = ".php";
$savefilename = "cas_workflow/tmp/{$session}_d{$ext}";

// 内容書き込み
$fp = fopen($savefilename, "w");
fwrite($fp, $wkfw_content, 2000000);

fclose($fp);

// 承認者とフォーマットファイル情報をファイルで引き渡す
$format_cnt = count($filename);
$id_data = "approve_num=$approve_num";
$id_data .= "&format_cnt=$format_cnt";
// 承認者
for ($i=0; $i<$approve_num; $i++) {

	$tmp_apv_div0_flg = "apv_div0_flg".($i+1);
	$tmp_apv_div1_flg = "apv_div1_flg".($i+1);
	$tmp_apv_div2_flg = "apv_div2_flg".($i+1);
	$tmp_apv_div3_flg = "apv_div3_flg".($i+1);
	$tmp_apv_div4_flg = "apv_div4_flg".($i+1);

	$tmp_target_class_div = "target_class_div".($i+1);
	$tmp_st_id = "st_id".($i+1);
	$tmp_emp_id = "emp_id".($i+1);
	$tmp_pjt_parent_id = "pjt_parent_id".($i+1);
	$tmp_pjt_child_id = "pjt_child_id".($i+1);
	$tmp_class_sect_id = "class_sect_id".($i+1);
	$tmp_atrb_sect_id = "atrb_sect_id".($i+1);
	$tmp_dept_sect_id = "dept_sect_id".($i+1);
	$tmp_room_sect_id = "room_sect_id".($i+1);
	$tmp_st_sect_id = "st_sect_id".($i+1);
	$tmp_multi_apv_flg = "multi_apv_flg".($i+1);
	$tmp_apv_num = "apv_num".($i+1);

	$id_data .= "&$tmp_apv_div0_flg=$apv_div0_flg[$i]";
	$id_data .= "&$tmp_apv_div1_flg=$apv_div1_flg[$i]";
	$id_data .= "&$tmp_apv_div2_flg=$apv_div2_flg[$i]";
	$id_data .= "&$tmp_apv_div3_flg=$apv_div3_flg[$i]";
	$id_data .= "&$tmp_apv_div4_flg=$apv_div4_flg[$i]";

	$id_data .= "&$tmp_target_class_div=$target_class_div[$i]";
	$id_data .= "&$tmp_st_id=$st_id[$i]";
	$id_data .= "&$tmp_emp_id=$emp_id[$i]";
	$id_data .= "&$tmp_pjt_parent_id=$pjt_parent_id[$i]";
	$id_data .= "&$tmp_pjt_child_id=$pjt_child_id[$i]";
	$id_data .= "&$tmp_class_sect_id=$class_sect_id[$i]";
	$id_data .= "&$tmp_atrb_sect_id=$atrb_sect_id[$i]";
	$id_data .= "&$tmp_dept_sect_id=$dept_sect_id[$i]";
	$id_data .= "&$tmp_room_sect_id=$room_sect_id[$i]";
	$id_data .= "&$tmp_st_sect_id=$st_sect_id[$i]";
	$id_data .= "&$tmp_multi_apv_flg=$multi_apv_flg[$i]";
	$id_data .= "&$tmp_apv_num=$apv_num[$i]";
}
$id_data .= "&notice=$notice";

$id_data .= "&rslt_ntc_div0_flg=$rslt_ntc_div0_flg";
$id_data .= "&rslt_ntc_div1_flg=$rslt_ntc_div1_flg";
$id_data .= "&rslt_ntc_div2_flg=$rslt_ntc_div2_flg";
$id_data .= "&rslt_ntc_div3_flg=$rslt_ntc_div3_flg";
$id_data .= "&rslt_ntc_div4_flg=$rslt_ntc_div4_flg";

$id_data .= "&notice_target_class_div=$notice_target_class_div";
$id_data .= "&notice_st_id=$notice_st_id";
$id_data .= "&notice_emp_id=$notice_emp_id";
$id_data .= "&notice_pjt_parent_id=$notice_pjt_parent_id";
$id_data .= "&notice_pjt_child_id=$notice_pjt_child_id";

$id_data .= "&notice_class_sect_id=$notice_class_sect_id";
$id_data .= "&notice_atrb_sect_id=$notice_atrb_sect_id";
$id_data .= "&notice_dept_sect_id=$notice_dept_sect_id";
$id_data .= "&notice_room_sect_id=$notice_room_sect_id";
$id_data .= "&notice_st_sect_id=$notice_st_sect_id";


// フォーマットファイル
for ($i=0; $i<$format_cnt; $i++) {
	$tmp_file_id = "file_id".($i+1);
	$tmp_filename = "filename".($i+1);
	$id_data .= "&$tmp_file_id=$file_id[$i]";
	$id_data .= "&$tmp_filename=$filename[$i]";
}

// 前提とする申請書
$precond_cnt = count($precond_wkfw_id);
$id_data .= "&precond_cnt=$precond_cnt";
for ($i=0; $i<$precond_cnt; $i++)
{
	$tmp_precond_wkfw_id = "precond_wkfw_id".($i+1);
	$tmp_precond_wkfw_title = "precond_wkfw_title".($i+1);
	$id_data .= "&$tmp_precond_wkfw_id=$precond_wkfw_id[$i]";
	$id_data .= "&$tmp_precond_wkfw_title=$precond_wkfw_title[$i]";
}


$idfilename = "cas_workflow/tmp/{$session}_id{$ext}";
// 内容書き込み
$fp = fopen($idfilename, "w");
fwrite($fp, $id_data, 2000000);

fclose($fp);


$wkfw_title = urlencode($wkfw_title);
?>
	wkfw_type      = document.getElementById('wkfw_type').value;
	wkfw_folder_id = document.getElementById('wkfw_folder_id').value;;
	var url = 'cas_workflow_register_preview.php'
		+ '?session=<? echo($session); ?>'
		+ '&wkfw_id=<? echo($wkfw_id); ?>'
		+ '&wkfw_appr=<? echo($wkfw_appr); ?>'
		+ '&wkfw_type='+ wkfw_type
		+ '&wkfw_folder_id='+ wkfw_folder_id
		+ '&wkfw_content_type=<? echo($wkfw_content_type); ?>'
		+ '&wkfw_title=<? echo($wkfw_title); ?>'
		+ '&short_wkfw_name=<? echo($short_wkfw_name); ?>'
		+ '&apply_title_disp_flg=<? echo($apply_title_disp_flg); ?>'
		;
	show_preview_window(url);
<?
}

// テキスト時はボタンを無効化
if ($wkfw_content_type == "1") {
?>
document.wkfw.referbtn.disabled = true;
<?
}
?>

// 承認階層
<?
for ($i = 0; $i < $approve_num; $i++)
{
$j = $i+1;
?>
document.getElementById('approve_content<?=$j?>').innerHTML = '<?=$approve[$i]?>';
<?
}
?>

// 申請者以外の結果通知
document.getElementById('notice_content').innerHTML = '<?=$notice?>';


// 前提となる申請書
<?
$sel_precond_wkfw_id = "";
foreach($precond_wkfw_id as $tmp_precond_wkfw_id)
{
	if($sel_precond_wkfw_id != "")
	{
		$sel_precond_wkfw_id .= ",";
	}
	$sel_precond_wkfw_id .= $tmp_precond_wkfw_id;
}
?>
document.getElementById('sel_precond_wkfw_id').value = '<?=$sel_precond_wkfw_id?>';

</script>
</body>
</html>

<?
// ワークロー・承認者詳細情報取得
function search_wkfw_apv_dtl($con, $fname, $wkfw_id, $apv_order, $mode, &$ret_arr) {

	$sql  = "select a.emp_id, b.emp_lt_nm, b.emp_ft_nm, c.emp_del_flg ";
	if($mode == "ALIAS")
	{
		$sql .= "from cas_wkfwapvdtl a ";
	}
	else
	{
		$sql .= "from cas_wkfwapvdtl_real a ";
	}
	$sql .= "inner join empmst b on a.emp_id = b.emp_id ";
	$sql .= "inner join authmst c on a.emp_id = c.emp_id ";
	$sql .= "where a.wkfw_id = $wkfw_id and a.apv_order = $apv_order order by a.apv_sub_order asc";
    $cond = "";


	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$idx = 0;
	while($row = pg_fetch_array($sel))
	{

		if($idx > 0)
		{
			$emp_id .= ",";
			$approve .= ", ";
			$emp_del_flg .= ",";
		}
		$emp_id .= $row["emp_id"];
		$emp_lt_nm = $row["emp_lt_nm"];
		$emp_ft_nm = $row["emp_ft_nm"];
		$emp_del_flg .= $row["emp_del_flg"];

		$emp_full_nm = $emp_lt_nm ." ".$emp_ft_nm;
		$approve .= $emp_full_nm;
		$idx++;
	}

	array_push($ret_arr, array("approve" => $approve,
							    "target_class_div" => "",
								"st_id" => "",
							    "emp_id" => $emp_id,
							    "emp_nm" => $approve,
							    "emp_del_flg" => $emp_del_flg,
							    "pjt_parent_id" => "",
							    "pjt_parent_nm" => "",
							    "pjt_child_id" => "",
							    "pjt_child_nm" => "",
   							    "class_sect_id" => "",
							    "atrb_sect_id" => "",
							    "dept_sect_id" => "",
							    "room_sect_id" => "",
							    "st_sect_id" => ""));

}

// ワークフロー・部署役職情報取得
function search_wkfw_apv_pst($con, $fname, $wkfw_id, $apv_order, $mode, &$ret_arr) {
	global $_label_by_profile;
	global $profile_type;

	$sql  = "select ";
	$sql .= "a.target_class_div, ";
	$sql .= "case a.target_class_div ";
	$sql .= "when '1' then c.class_nm ";
	$sql .= "when '2' then c.atrb_nm ";
	$sql .= "when '3' then c.dept_nm ";
	$sql .= "when '4' then c.room_nm ";
	$sql .= "else '' ";
	$sql .= "end as target_class_name, ";
	$sql .= "b.st_id,";
	$sql .= "b.st_nm ";
	if($mode == "ALIAS")
	{
		$sql .= "from cas_wkfwapvmng a left outer join ";
	}
	else
	{
		$sql .= "from cas_wkfwapvmng_real a left outer join ";
	}
	$sql .= "(select a.wkfw_id, a.apv_order, a.st_id, b.st_nm ";

	if($mode == "ALIAS")
	{
		$sql .= "from cas_wkfwapvpstdtl a inner join stmst b on a.st_id = b.st_id where st_div = 0) b on ";
	}
	else
	{
		$sql .= "from cas_wkfwapvpstdtl_real a inner join stmst b on a.st_id = b.st_id where st_div = 0) b on ";
	}
	$sql .= "a.wkfw_id = b.wkfw_id and a.apv_order = b.apv_order, ";
	$sql .= "(select * from classname) c ";
	$sql .= "where a.wkfw_id = $wkfw_id and a.apv_order = $apv_order ";

    $cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$num = pg_numrows($sel);

	$approve = "";
	$target_class_div = "";
	$st_id = "";
	$st_nm = "";


	for ($i=0; $i<$num; $i++) {

		if($i == 0) {
		  	$target_class_div = pg_fetch_result($sel, $i, "target_class_div");
		  	$target_class_name = pg_fetch_result($sel, $i, "target_class_name");
     		$st_id = pg_fetch_result($sel, $i, "st_id");
			$st_nm = pg_fetch_result($sel, $i, "st_nm");

			if($target_class_div != 0) {
				$approve .= "申請者の所属する【";
    	 		$approve .=	$target_class_name;
     			$approve .=	"】の";
			} else {
				// 病院内/所内
				$in_hospital2_title = $_label_by_profile["IN_HOSPITAL2"][$profile_type];
				if($in_hospital2_title != "")
				{
	     			$approve .=	"【".$in_hospital2_title."】の";
				}
			}

			$approve .=	$st_nm;

		} else {
			$st_id .=",";
     		$st_id .= pg_fetch_result($sel, $i, "st_id");

			$st_nm = pg_fetch_result($sel, $i, "st_nm");
			$approve .=	", ";
			$approve .=	$st_nm;
		}


	}

	array_push($ret_arr, array("approve" => $approve,
							    "target_class_div" => $target_class_div,
								"st_id" => $st_id,
							    "emp_id" => "",
							    "emp_nm" => "",
							    "emp_del_flg" => "f",
							    "pjt_parent_id" => "",
							    "pjt_parent_nm" => "",
							    "pjt_child_id" => "",
							    "pjt_child_nm" => "",
							    "class_sect_id" => "",
							    "atrb_sect_id" => "",
							    "dept_sect_id" => "",
							    "room_sect_id" => "",
							    "st_sect_id" => ""));
}

// 委員会ＷＧ情報取得
function search_wkfw_pjt_dtl($con, $fname, $wkfw_id, $apv_order, $mode, &$ret_arr)
{

	$sql  = "select pjt_id, pjt_name from project ";
	if($mode == "ALIAS")
	{
		$sql .= "where pjt_id in (select parent_pjt_id from cas_wkfwpjtdtl where wkfw_id = $wkfw_id and apv_order = $apv_order) ";
	}
	else
	{
		$sql .= "where pjt_id in (select parent_pjt_id from cas_wkfwpjtdtl_real where wkfw_id = $wkfw_id and apv_order = $apv_order) ";
	}

    $sql .= "union all ";
    $sql .= "select pjt_id, pjt_name from project ";

	if($mode == "ALIAS")
	{
	    $sql .= "where pjt_id in (select child_pjt_id from cas_wkfwpjtdtl where wkfw_id = $wkfw_id and apv_order = $apv_order)";
	}
	else
	{
	    $sql .= "where pjt_id in (select child_pjt_id from cas_wkfwpjtdtl_real where wkfw_id = $wkfw_id and apv_order = $apv_order)";
	}
	$sel  = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if(pg_numrows($sel) == 1)
	{
		$pjt_parent_id = pg_fetch_result($sel, 0, "pjt_id");
		$pjt_parent_nm = pg_fetch_result($sel, 0, "pjt_name");

		$approve = $pjt_parent_nm;
	}
	else if(pg_numrows($sel) == 2)
	{
		$pjt_parent_id = pg_fetch_result($sel, 0, "pjt_id");
		$pjt_parent_nm = pg_fetch_result($sel, 0, "pjt_name");
		$pjt_child_id = pg_fetch_result($sel, 1, "pjt_id");
		$pjt_child_nm = pg_fetch_result($sel, 1, "pjt_name");

		$approve  = $pjt_parent_nm;
		$approve .= " > ";
		$approve .= $pjt_child_nm;
	}
	array_push($ret_arr, array("approve" => $approve,
							    "target_class_div" => "",
								"st_id" => "",
							    "emp_id" => "",
							    "emp_nm" => "",
							    "emp_del_flg" => "f",
							    "pjt_parent_id" => $pjt_parent_id,
							    "pjt_parent_nm" => $pjt_parent_nm,
							    "pjt_child_id" => $pjt_child_id,
							    "pjt_child_nm" => $pjt_child_nm,
							    "class_sect_id" => "",
							    "atrb_sect_id" => "",
							    "dept_sect_id" => "",
							    "room_sect_id" => "",
							    "st_sect_id" => ""));
}

// 部署役職(部署所属)取得
function search_wkfw_apv_pst_sect($con, $fname, $wkfw_id, $apv_order, $mode, &$ret_arr)
{

	// 部署取得
	$sql  = "select a.*, b.class_nm, c.atrb_nm, d.dept_nm, e.room_nm ";
	if($mode == "ALIAS")
	{
		$sql .= "from cas_wkfwapvsectdtl a ";
	}
	else
	{
		$sql .= "from cas_wkfwapvsectdtl_real a ";
	}
	$sql .= "left join classmst b on a.class_id = b.class_id and not b.class_del_flg ";
	$sql .= "left join atrbmst c on a.atrb_id = c.atrb_id and not c.atrb_del_flg ";
	$sql .= "left join deptmst d on a.dept_id = d.dept_id and not d.dept_del_flg ";
	$sql .= "left join classroom e on a.room_id = e.room_id and not e.room_del_flg ";
	$sql .= "where a.wkfw_id = $wkfw_id ";
	$sql .= "and a.apv_order = $apv_order ";

	$sel  = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$class_sect_id = pg_fetch_result($sel, 0, "class_id");
	$atrb_sect_id = pg_fetch_result($sel, 0, "atrb_id");
	$dept_sect_id = pg_fetch_result($sel, 0, "dept_id");
	$room_sect_id = pg_fetch_result($sel, 0, "room_id");

	$class_nm = pg_fetch_result($sel, 0, "class_nm");
	$atrb_nm = pg_fetch_result($sel, 0, "atrb_nm");
	$dept_nm = pg_fetch_result($sel, 0, "dept_nm");
	$room_nm = pg_fetch_result($sel, 0, "room_nm");

	if($class_nm != "")
	{
		$class_sect_nm .= $class_nm;

		if($atrb_nm != "")
		{
			$class_sect_nm .= " > ";
			$class_sect_nm .= $atrb_nm;
		}

		if($dept_nm != "")
		{
			$class_sect_nm .= " > ";
			$class_sect_nm .= $dept_nm;
		}

		if($room_nm != "")
		{
			$class_sect_nm .= " > ";
			$class_sect_nm .= $room_nm;
		}
	}

	// 役職
	if($class_sect_nm != "")
	{
		$sql  = "select a.*, b.st_nm ";
		if($mode == "ALIAS")
		{
			$sql .= "from cas_wkfwapvpstdtl a ";
		}
		else
		{
			$sql .= "from cas_wkfwapvpstdtl_real a ";
		}
		$sql .= "left join stmst b on a.st_id = b.st_id and not st_del_flg ";
		$sql .= "where a.wkfw_id = $wkfw_id ";
		$sql .= "and a.apv_order = $apv_order ";
		$sql .= "and a.st_div = 4 ";

		$sel  = select_from_table($con, $sql, "", $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$all_st_sect_id = "";
		$all_st_sect_nm = "";
		while($row = pg_fetch_array($sel))
		{
			if($all_st_sect_id != "")
			{
				$all_st_sect_id .= ",";
				$all_st_sect_nm .= ", ";
			}

			$all_st_sect_id .= $row["st_id"];
			$all_st_sect_nm .= $row["st_nm"];
		}

	}
	$approve = "";
	if($class_sect_nm != "" && $all_st_sect_nm != "")
	{
		$approve = $class_sect_nm."の".$all_st_sect_nm;
	}

	array_push($ret_arr, array("approve" => $approve,
	 						    "target_class_div" => "",
								"st_id" => "",
							    "emp_id" => "",
							    "emp_nm" => "",
							    "emp_del_flg" => "f",
							    "pjt_parent_id" => "",
							    "pjt_parent_nm" => "",
							    "pjt_child_id" => "",
							    "pjt_child_nm" => "",
							    "class_sect_id" => $class_sect_id,
							    "atrb_sect_id" => $atrb_sect_id,
							    "dept_sect_id" => $dept_sect_id,
							    "room_sect_id" => $room_sect_id,
							    "st_sect_id" => $all_st_sect_id));
}

function set_aply_apv($apv_num, &$ret_arr) {

	$approve = "申請時に承認者を指定する(".$apv_num."名)";
	array_push($ret_arr, array("approve" => $approve,
							    "target_class_div" => "",
								"st_id" => "",
							    "emp_id" => "",
							    "emp_nm" => "",
   							    "emp_del_flg" => "f",
							    "pjt_parent_id" => "",
							    "pjt_parent_nm" => "",
							    "pjt_child_id" => "",
							    "pjt_child_nm" => "",
							    "class_sect_id" => "",
							    "atrb_sect_id" => "",
							    "dept_sect_id" => "",
							    "room_sect_id" => "",
							    "st_sect_id" => ""));

}
?>

