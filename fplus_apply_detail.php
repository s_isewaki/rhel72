<?
require_once("about_comedix.php");
require_once("show_fplus_apply_history.ini");
require_once("show_fplus_apply_detail.ini");
require_once("show_select_values.ini");
require_once("fplus_imprint_common.ini");
require_once("yui_calendar_util.ini");
require_once("get_values.ini");
require_once("fplus_common_class.php");
require_once("get_values_for_template.ini");
require_once("library_common.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$checkauth=check_authority($session,78,$fname);
if($checkauth=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new fplus_common_class($con, $fname);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 報告詳細</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>

<?
if($target_apply_id == "") {
	$target_apply_id = $apply_id;
}

// 申請・ワークフロー情報取得
$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($target_apply_id);
$wkfw_content      = $arr_apply_wkfwmst[0]["wkfw_content"];
$wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
$apply_content     = $arr_apply_wkfwmst[0]["apply_content"];
$wkfw_id           = $arr_apply_wkfwmst[0]["wkfw_id"];
$wkfw_history_no   = $arr_apply_wkfwmst[0]["wkfw_history_no"];

// ワークフロー情報取得
/*
$sel_apply_wkfwmst = search_apply_wkfwmst($con, $fname, $target_apply_id);
$wkfw_content = pg_fetch_result($sel_apply_wkfwmst, 0, "wkfw_content");
$wkfw_content_type = pg_fetch_result($sel_apply_wkfwmst, 0, "wkfw_content_type");
$apply_content = pg_fetch_result($sel_apply_wkfwmst, 0, "apply_content");
*/
// 本文形式タイプのデフォルトを「テキスト」とする
if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

// 形式をテキストからテンプレートに変更した場合の古いデータ対応
// wkfw_content_typeは2:テンプレートだが、登録データがXMLでない場合、1:テキストとして処理する
if ($wkfw_content_type == "2") {
	if (strpos($apply_content, "<?xml") === false) {
		$wkfw_content_type = "1";
	}
}

if($wkfw_history_no != "")
{
	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];
}

$num = 0;
if ($wkfw_content_type == "2") {
	$pos = 0;
	while (1) {
		$pos = strpos($wkfw_content, 'show_cal', $pos);
		if ($pos === false) {
			break;
		} else {
			$num++;
		}
		$pos++;
	}
}

if ($num > 0) {
	// 外部ファイルを読み込む
	write_yui_calendar_use_file_read_0_12_2();
}
// カレンダー作成、関数出力
write_yui_calendar_script2($num);

if ($mode == "apply_printwin") {

//---------テンプレートの場合XML形式のテキスト$contentを作成---------

	if ($wkfw_content_type == "2") {
		// XML変換用コードの保存
		$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
		$savexmlfilename = "fplus/workflow/tmp/{$session}_x.php";
		$fp = fopen($savexmlfilename, "w");

		if (!$fp) {
			echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
//			echo("<script language='javascript'>history.back();</script>");
			$print_failed = true;
		}
		if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
			fclose($fp);
			echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
//			echo("<script language='javascript'>history.back();</script>");
			$print_failed = true;
		}

		fclose($fp);

		include( $savexmlfilename );

	}
	$savexmlfilename = "fplus/workflow/tmp/{$session}_d.php";
	$fp = fopen($savexmlfilename, "w");

	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
//		echo("<script language='javascript'>history.back();</script>");
		$print_failed = true;
	}

	if ($content == "") {
		$content = " ";
	}
	if(!fwrite($fp, $content, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
//		echo("<script language='javascript'>history.back();</script>");
		$print_failed = true;
	}
	fclose($fp);
}
?>
<script type="text/javascript">

function history_select(apply_id, target_apply_id) {

	document.apply.apply_id.value = apply_id;
	document.apply.target_apply_id.value = target_apply_id;
	document.apply.action="fplus_apply_detail.php?session=<?=$session?>";
	document.apply.submit();
}

function set_regist_emp_id(obj, idx) {
	var emp_id = 'regist_emp_id' + idx;
	document.apply.elements[emp_id].value = obj.value;
}

function attachFile() {
	window.open('fplus_apply_attach.php?session=<? echo($session); ?>', 'newwin2', 'width=640,height=480,scrollbars=yes');
}

function detachFile(e) {
	if (e == undefined) {
		e = window.event;
	}

	var btn_id;
	if (e.target) {
		btn_id = e.target.getAttribute('id');
	} else {
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_', '');

	var p = document.getElementById('p_' + id);
	document.getElementById('attach').removeChild(p);
}

function apply_update(apv_num) {

	if (window.InputCheck) {
		if (!InputCheck()) {
			return;
		}
	}

	//エピネット用入力チェック処理ここから
	var epi_element = document.getElementById("EPINET_REPORT_PIERCE_CUT_INFECTION");

	if(epi_element != undefined)
	{
	
		if(epi_element.value == "Episys107A")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107A();
			if(ret == false)
			{
				return false;
			}
		}
		else if(epi_element.value == "Episys107B")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107B();
			if(ret == false)
			{
				return false;
			}
		}
                else if(epi_element.value == "Episys107AO")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107AO();
			if(ret == false)
			{
				return false;
			}
		}
                else if(epi_element.value == "Episys107BO")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107BO();
			if(ret == false)
			{
				return false;
			}
		}

	}
	//エピネット用入力チェック処理ここまで

	//MRSA報告書入力チェック処理ここから
	var epi_element = document.getElementById("MRSA_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check();
		if(ret == false)
		{
			return false;
		}

	}
	//MRSA報告書入力チェック処理ここまで

	// 医療事故報告用入力チェック処理ここから
	// 緊急報告
	var epi_element = document.getElementById("IMMEDIATE_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_immediate_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// 第一報
	var epi_element = document.getElementById("FIRST_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_first_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// 第二報
	var epi_element = document.getElementById("SECOND_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_second_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// コンサルテーション依頼書
	var epi_element = document.getElementById("CONSULTATION_REQUEST");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_cons_req();
		if(ret == false)
		{
			return false;
		}
	}
	// コンサルテーション報告書
	var epi_element = document.getElementById("CONSULTATION_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_cons_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// 医療機器不具合報告
	var epi_element = document.getElementById("MEDICAL_APPARATUS_FAULT_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_apparatus_fault_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// ヒヤリハット件数表
	var epi_element = document.getElementById("NUMBER_OF_OCCURRENCES");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_number_of_occurrences();
		if(ret == false)
		{
			return false;
		}
	}
	// 医療事故報告用入力チェック処理ここまで

	if (confirm('更新します。よろしいですか？')) {
		document.apply.action="fplus_apply_detail_update.php?session=<?=$session?>";
		document.apply.submit();
	}
}

function apply_cancel() {

	if (confirm('報告取消します。よろしいですか？')) {
		document.apply.action="fplus_apply_detail_cancel.php?session=<?=$session?>";
		document.apply.submit();
	}
}

function re_apply() {
	//エピネット用入力チェック処理ここから
	var epi_element = document.getElementById("EPINET_REPORT_PIERCE_CUT_INFECTION");

	if(epi_element != undefined)
	{
	
		if(epi_element.value == "Episys107A")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107A();
			if(ret == false)
			{
				return false;
			}
		}
		else if(epi_element.value == "Episys107B")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107B();
			if(ret == false)
			{
				return false;
			}
		}else if(epi_element.value == "Episys107AO")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107AO();
			if(ret == false)
			{
				return false;
			}
		}
                else if(epi_element.value == "Episys107BO")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107BO();
			if(ret == false)
			{
				return false;
			}
		}

	}
	//エピネット用入力チェック処理ここまで

	//MRSA報告書入力チェック処理ここから
	var epi_element = document.getElementById("MRSA_REPORT");
	if(epi_element != undefined)
	{

		//感染症報告（MRSA）
		//confirm(epi_element.value);
		ret = fc_regist_check();
		if(ret == false)
		{
			return false;
		}
	}
	//MRSA報告書入力チェック処理ここまで

	// 医療事故報告用入力チェック処理ここから
	// 緊急報告
	var epi_element = document.getElementById("IMMEDIATE_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_immediate_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// 第一報
	var epi_element = document.getElementById("FIRST_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_first_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// 第二報
	var epi_element = document.getElementById("SECOND_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_second_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// コンサルテーション依頼書
	var epi_element = document.getElementById("CONSULTATION_REQUEST");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_cons_req();
		if(ret == false)
		{
			return false;
		}
	}
	// コンサルテーション報告書
	var epi_element = document.getElementById("CONSULTATION_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_cons_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// 医療機器不具合報告
	var epi_element = document.getElementById("MEDICAL_APPARATUS_FAULT_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_apparatus_fault_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// ヒヤリハット件数表
	var epi_element = document.getElementById("NUMBER_OF_OCCURRENCES");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_number_of_occurrences();
		if(ret == false)
		{
			return false;
		}
	}
	// 医療事故報告用入力チェック処理ここまで

	if (confirm('再報告します。よろしいですか？')) {
		document.apply.action="fplus_apply_detail_re_apply.php?session=<?=$session?>";
		document.apply.submit();
	}
}

function init() {

	if('<?=$back?>' == 't') {
<?

		$approve_num = $_POST[approve_num];
		for($i=1; $i<=$approve_num;$i++) {

			$regist_emp_id = "regist_emp_id$i";
			$radio_emp_id = "radio_emp_id$i";
			$approve_name = "approve_name$i";
?>
			if(document.getElementById('<?=$radio_emp_id?>')) {
				for(j=0; j<document.apply.elements['<?=$radio_emp_id?>'].length; j++) {

					if(document.apply.elements['<?=$radio_emp_id?>'][j].value == '<?=$_POST[$regist_emp_id];?>') {
						document.apply.elements['<?=$radio_emp_id?>'][j].checked = true;
					}
				}
				document.apply.elements['<?=$regist_emp_id?>'].value = '<?=$_POST[$regist_emp_id];?>';
			}


			if(document.getElementById('<?=$approve_name?>')) {
				document.apply.elements['<?=$approve_name?>'].value = '<?=$_POST[$approve_name];?>'
				document.apply.elements['<?=$regist_emp_id?>'].value = '<?=$_POST[$regist_emp_id];?>';
			}
<?
		}
?>
	}
	
	
	//エピネット用初期処理ここから
	var epi_element = document.getElementById("EPINET_REPORT_PIERCE_CUT_INFECTION");

	if(epi_element != undefined)
	{
	
		if(epi_element.value == "Episys107A")
		{
			//confirm(epi_element.value);
			fc_upd_init_107A();
		}
		if(epi_element.value == "Episys107B")
		{
			//confirm(epi_element.value);
			fc_upd_init_107B();
		}
                if(epi_element.value == "Episys107AO"){
                 
                        fc_upd_init_107AO();
                    }
                if(epi_element.value == "Episys107BO"){
                        fc_upd_init_107BO();
                                
                }
	}
	//エピネット用初期処理ここまで
	
	//MRSA系初期処理ここから
	var epi_element = document.getElementById("MRSA_REPORT");
	if(epi_element != undefined)
	{
		fc_init();
	}
	//MRSA系初期処理ここまで

	// 医療事故報告用初期処理ここから
	// 緊急報告受付
	var epi_element = document.getElementById("IMMEDIATE_REPORT");
	if(epi_element != undefined)
	{
		fc_upd_init_immediate_repo();
	}
	// 第一報
	var epi_element = document.getElementById("FIRST_REPORT");
	if(epi_element != undefined)
	{
		fc_upd_init_first_repo();
	}
	// 第二報
	var epi_element = document.getElementById("SECOND_REPORT");
	if(epi_element != undefined)
	{
		fc_upd_init_second_repo();
	}
	// コンサルテーション依頼書
	var epi_element = document.getElementById("CONSULTATION_REQUEST");
	if(epi_element != undefined)
	{
		fc_upd_init_cons_req();
	}
	// コンサルテーション報告書
	var epi_element = document.getElementById("CONSULTATION_REPORT");
	if(epi_element != undefined)
	{
		fc_upd_init_cons_repo();
	}
	// 医療機器不具合報告
	var epi_element = document.getElementById("MEDICAL_APPARATUS_FAULT_REPORT");
	if(epi_element != undefined)
	{
		fc_upd_init_apparatus_fault_repo();
	}
	// ヒヤリハット件数表
	var epi_element = document.getElementById("NUMBER_OF_OCCURRENCES");
	if(epi_element != undefined)
	{
		fc_upd_init_number_of_occurrences();
	}
	// 医療事故報告用初期処理ここまで

}

function apply_printwin() {
	document.apply.mode.value = "apply_printwin";
	document.apply.action="fplus_apply_detail.php?session=<?=$session?>";
	var default_target = document.apply.target;
	document.apply.target = 'printframe';
	document.apply.submit();
	document.apply.target = default_target;
}

function apply_printwin_epinet() {
	document.apply.mode.value = "apply_printwin";
	document.apply.action="fplus_apply_detail_print_epinet.php?session=<?=$session?>";
	var default_target = document.apply.target;
	document.apply.target = '_blank';
	document.apply.submit();
	//document.apply.target = default_target;
}

function apply_printwin_mrsa() {
	document.apply.mode.value = "apply_printwin";
	document.apply.action="fplus_apply_detail_print_mrsa.php?session=<?=$session?>";
	var default_target = document.apply.target;
	document.apply.target = '_blank';
	document.apply.submit();
	//document.apply.target = default_target;
}

// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);

        // 履歴表示table高さ調整
        resize_history_tbl();
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];
		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}

// 履歴表示tableの高さ調節
function resize_history_tbl()
{
    document.getElementById('history_tbl').style.height = document.getElementById('dtl_tbl').offsetHeight;
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#E6B3D4 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#E6B3D4 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#E6B3D4 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#E6B3D4 solid 0px;}
table.block3 td {border-width:0;}


p {margin:0;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="init();initcal();
<? if ($mode == "apply_printwin" && !$print_failed) { ?>
window.open('fplus_apply_detail_print.php?session=<?=$session?>&fname=<?=$fname?>&target_apply_id=<?=$target_apply_id?>&back=f&mode=apply_print', 'apply_print', 'width=670,height=700,scrollbars=yes');
<? } ?>
if (window.OnloadSub) { OnloadSub(); };resizeAllTextArea();resize_history_tbl();if (window.refreshApproveOrders) {refreshApproveOrders();}">
<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="target_apply_id" value="<? echo($target_apply_id); ?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="wkfw_id" value="<? echo($wkfw_id); ?>">
<input type="hidden" name="wkfw_history_no" value="<? echo($wkfw_history_no); ?>">

<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#E6B3D4">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>報告詳細</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="page_close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#E6B3D4"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td valign="top" width="25%">
<? show_application_history_for_apply($con, $session, $fname, $apply_id, $target_apply_id);?>
</td>
<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top" width="75%">
<? show_application_apply_detail($con, $session, $fname, $target_apply_id, $apply_title, $content, $file_id, $filename, $back, $mode); ?>
</td>
</center>
</form>
</body>
<iframe name="printframe" width="0" height="0" frameborder="0"></iframe>
</html>
<?
$update_cnt = 0;
$show_flg_name = "";
if($mode == 'show_flg_update') {

	// トランザクションを開始
	pg_query($con, "begin");

	$sql = "select apply_stat, apv_fix_show_flg, apv_ng_show_flg, apv_bak_show_flg from fplusapply";
	$cond = "where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$apply_stat = pg_fetch_result($sel, 0, "apply_stat");
	$apv_fix_show_flg = pg_fetch_result($sel, 0, "apv_fix_show_flg");
	$apv_ng_show_flg = pg_fetch_result($sel, 0, "apv_ng_show_flg");
	$apv_bak_show_flg = pg_fetch_result($sel, 0, "apv_bak_show_flg");

	if ($apply_stat == "1" && $apv_fix_show_flg == "t") {
		$update_cnt = 1;
		$show_flg_name = "apv_fix_show_flg";
	}
	if ($apply_stat == "2" && $apv_ng_show_flg == "t") {
		$update_cnt = 1;
		$show_flg_name = "apv_ng_show_flg";
	}
	if ($apply_stat == "3" && $apv_bak_show_flg == "t") {
		$update_cnt = 1;
		$show_flg_name = "apv_bak_show_flg";
	}

	if ($update_cnt > 0) {
		$sql = "update fplusapply set";
		$set = array($show_flg_name);
		$setvalue = array('f');
		$cond = "where apply_id = $apply_id and $show_flg_name = 't' and apply_stat = '$apply_stat'";
		$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if($up ==0){
			pg_exec($con,"rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

// 一部承認の場合
	if ($apply_stat == "0") {
		$sql = "select count(apply_id) as show_flg_cnt from fplusapplyapv";
		$cond = "where apply_id = $apply_id and apv_stat = '1' and apv_fix_show_flg = 't'";

		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$show_flg_cnt = pg_fetch_result($sel, 0, "show_flg_cnt");

		if ($show_flg_cnt > 0) {
			$update_cnt = 1;
			$sql = "update fplusapplyapv set";
			$set = array("apv_fix_show_flg");
			$setvalue = array('f');
			$cond = "where apply_id = $apply_id and apv_fix_show_flg = 't' and apv_stat = '1'";
			$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if($up ==0){
				pg_exec($con,"rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
		}

	}
	// トランザクションをコミット
	pg_query($con, "commit");
}

pg_close($con);
?>

<script type="text/javascript">
<!--
function page_close() {
<?
if($mode == 'show_flg_update' && $update_cnt > 0) {
?>
	if(window.opener && !window.opener.closed && window.opener.reload_page) {
		window.opener.reload_page();
		window.close();
	} else {
		window.close();
	}
<?
} else {
?>
	window.close();
<?
}
?>

}
//-->
</script>
