<?php
require_once("Cmx.php");
require_once("about_comedix.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("show_class_name.ini");
require_once("aclg_set.php");

//画面名
$fname = $_SERVER['PHP_SELF'];

//セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

// 権限チェック
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') {
    js_login_exit();
}


//DBコネクション取得
$con = connect2db($fname);
if ($con == "0") {
    js_login_exit();
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"], $_POST, $con);

// 組織階層情報取得
$arr_class_name = get_class_name_array($con, $fname);

// 更新処理
if ($_POST['mode'] === 'update') {

    // トランザクションの開始
    pg_query($con, "begin transaction");

    foreach ($_POST['c1_all'] as $id) {
        update_set_table($con, 'UPDATE classmst SET', array('fantol_del_flg'), array(in_array($id, $_POST['c1']) ? 'f' : 't'), "WHERE class_id={$id}", $fname);
    }

    foreach ($_POST['c2_all'] as $id) {
        update_set_table($con, 'UPDATE atrbmst SET', array('fantol_del_flg'), array(in_array($id, $_POST['c2']) ? 'f' : 't'), "WHERE atrb_id={$id}", $fname);
    }

    foreach ($_POST['c3_all'] as $id) {
        update_set_table($con, 'UPDATE deptmst SET', array('fantol_del_flg'), array(in_array($id, $_POST['c3']) ? 'f' : 't'), "WHERE dept_id={$id}", $fname);
    }

    foreach ($_POST['c4_all'] as $id) {
        update_set_table($con, 'UPDATE classroom SET', array('fantol_del_flg'), array(in_array($id, $_POST['c4']) ? 'f' : 't'), "WHERE room_id={$id}", $fname);
    }

    // トランザクションをコミット
    pg_query($con, "commit");
}

// 組織1データ取得
$sel_c1 = select_from_table($con, "SELECT * FROM classmst ORDER BY order_no", "", $fname);
if ($sel_c1 == 0) {
    pg_close($con);
    js_error_exit();
}
$c1 = array();
while ($row = pg_fetch_assoc($sel_c1)) {
    $c1[] = array(
        id         => $row['class_id'],
        name       => $row['class_nm'],
        del_flg    => $row['class_del_flg'] === 't',
        fantol_flg => $row['fantol_del_flg'] === 't',
    );
}

// 組織2データ取得
$sel_c2 = select_from_table($con, "SELECT * FROM atrbmst ORDER BY order_no", "", $fname);
if ($sel_c2 == 0) {
    pg_close($con);
    js_error_exit();
}
$c2 = array();
while ($row = pg_fetch_assoc($sel_c2)) {
    $c2[$row['class_id']][] = array(
        id         => $row['atrb_id'],
        name       => $row['atrb_nm'],
        del_flg    => $row['atrb_del_flg'] === 't',
        fantol_flg => $row['fantol_del_flg'] === 't',
    );
}

// 組織3データ取得
$sel_c3 = select_from_table($con, "SELECT * FROM deptmst ORDER BY order_no", "", $fname);
if ($sel_c3 == 0) {
    pg_close($con);
    js_error_exit();
}
$c3 = array();
while ($row = pg_fetch_assoc($sel_c3)) {
    $c3[$row['atrb_id']][] = array(
        id         => $row['dept_id'],
        name       => $row['dept_nm'],
        del_flg    => $row['dept_del_flg'] === 't',
        fantol_flg => $row['fantol_del_flg'] === 't',
    );
}

// 組織4データ取得
$sel_c4 = select_from_table($con, "SELECT * FROM classroom ORDER BY order_no", "", $fname);
if ($sel_c4 == 0) {
    pg_close($con);
    js_error_exit();
}
$c4 = array();
while ($row = pg_fetch_assoc($sel_c4)) {
    $c4[$row['dept_id']][] = array(
        id         => $row['room_id'],
        name       => $row['room_nm'],
        del_flg    => $row['room_del_flg'] === 't',
        fantol_flg => $row['fantol_del_flg'] === 't',
    );
}

pg_close($con);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
        <title>CoMedix ファントルくん | 統計部署表示</title>
        <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="js/fontsize.js"></script>
        <script type="text/javascript">
            $(function() {
                // 組織1
                $('input.c1').click(function() {
                    if ($(this).get(0).checked) {
                        $(this).parent().parent().removeClass('delete');
                    }
                    else {
                        $(this).parent().parent().addClass('delete');
                        $('input.c1_' + $(this).val()).each(function() {
                            if ($(this).get(0).checked) {
                                $(this).click();
                            }
                        });
                    }
                });

                // 組織2
                $('input.c2').click(function() {
                    if ($(this).get(0).checked) {
                        $(this).parent().parent().removeClass('delete');
                        var parent = $('#' + $(this).attr('data-parent'));
                        if (!parent.get(0).checked) {
                            parent.click();
                        }
                    }
                    else {
                        $(this).parent().parent().addClass('delete');
                        $('input.c2_' + $(this).val()).each(function() {
                            if ($(this).get(0).checked) {
                                $(this).click();
                            }
                        });
                    }
                });

                // 組織3
                $('input.c3').click(function() {
                    if ($(this).get(0).checked) {
                        $(this).parent().parent().removeClass('delete');
                        var parent = $('#' + $(this).attr('data-parent'));
                        if (!parent.get(0).checked) {
                            parent.click();
                        }
                    }
                    else {
                        $(this).parent().parent().addClass('delete');
                        $('input.c3_' + $(this).val()).each(function() {
                            if ($(this).get(0).checked) {
                                $(this).click();
                            }
                        });
                    }
                });

                // 組織4
                $('input.c4').click(function() {
                    if ($(this).get(0).checked) {
                        $(this).parent().parent().removeClass('delete');
                        var parent = $('#' + $(this).attr('data-parent'));
                        if (!parent.get(0).checked) {
                            parent.click();
                        }
                    }
                    else {
                        $(this).parent().parent().addClass('delete');
                        $('input.c4_' + $(this).val()).each(function() {
                            if ($(this).get(0).checked) {
                                $(this).click();
                            }
                        });
                    }
                });

            });
        </script>
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <style type="text/css">
            #title {
                font-weight: bold;
                font-size: 16px;
                margin-bottom: 10px;
            }
            #list {
                border: 1px solid black;
                border-collapse: collapse;
                border-spacing: 0;
            }
            #list thead td {
                text-align: center;
                background-color: #009933;
                color: #fff;
                font-size: 14px;
            }
            #list td {
                border: 1px solid black;
                padding: 3px 4px;
                font-size: 12px;
            }
            #list tbody tr {
                background-color: #fffafa;
            }
            #list tbody tr:nth-child(2n) {
                background-color: #F5FFE5;
            }
            #list tbody tr:hover {
                background-color: #cee4ae;
            }
            .check {
                text-align: center;
            }
            .master {
                text-align: center;
            }
            .delete {
                background-color: gray !important;
            }
            .comment {
                margin: 8px;
            }
        </style>
    </head>
    <body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
        <form name="mainform" action="hiyari_class_master_setting.php" method="post">
            <input type="hidden" name="session" value="<?= $session ?>">
            <input type="hidden" name="mode" value="update">

            <?
            show_hiyari_header($session, $fname, true);
            ?>

            <img src="img/spacer.gif" width="1" height="5" alt=""><br>

            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
                    <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
                </tr>
                <tr>
                    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                    <td bgcolor="#F5FFE5">

                        <div id="title">■統計部署表示</div>

                        <div class="comment">
                            統計分析で利用する組織をチェックしてください。<br>
                            （デフォルト:組織マスタで有効な組織)<br>
                            マスタ上で削除済みの組織も、指定することができます。
                        </div>

                        <div>
                            <button type="submit">更新する</button>
                        </div>

                        <table id="list">
                            <thead>
                                <tr>
                                    <td><?= h($arr_class_name[0]); ?></td>
                                    <td><?= h($arr_class_name[1]); ?></td>
                                    <td><?= h($arr_class_name[2]); ?></td>
                                    <? if ($arr_class_name["class_cnt"] === '4') { ?>
                                        <td><?= h($arr_class_name[3]); ?></td>
                                    <? } ?>
                                    <td>マスタ</td>
                                    <td>表示</td>
                                </tr>
                            </thead>

                            <tbody>
                                <? foreach ($c1 as $r1) { ?>
                                    <tr <? if ($r1['fantol_flg']) echo 'class="delete"'; ?>>
                                        <td><?= h($r1['name']); ?></td>
                                        <td></td>
                                        <td></td>
                                        <? if ($arr_class_name["class_cnt"] === '4') { ?><td></td><? } ?>
                                        <td class="master <? if ($r1['del_flg']) echo 'delete'; ?>"><? if ($r1['del_flg']) echo '×'; ?></td>
                                        <td class="check">
                                            <input
                                                id="c1_<?= h($r1['id']); ?>"
                                                class="c1"
                                                type="checkbox"
                                                name="c1[]"
                                                value="<?= h($r1['id']); ?>" <? if (!$r1['fantol_flg']) echo 'checked'; ?>>
                                            <input type="hidden" name="c1_all[]" value="<?= h($r1['id']); ?>">
                                        </td>
                                    </tr>

                                    <? foreach ($c2[$r1['id']] as $r2) { ?>
                                        <tr <? if ($r2['fantol_flg']) echo 'class="delete"'; ?>>
                                            <td></td>
                                            <td><?= h($r2['name']); ?></td>
                                            <td></td>
                                            <? if ($arr_class_name["class_cnt"] === '4') { ?><td></td><? } ?>
                                            <td class="master <? if ($r2['del_flg']) echo 'delete'; ?>"><? if ($r2['del_flg']) echo '×'; ?></td>
                                            <td class="check">
                                                <input
                                                    id="c2_<?= h($r2['id']); ?>"
                                                    data-parent="c1_<?= h($r1['id']); ?>"
                                                    class="c2 c1_<?= h($r1['id']); ?>"
                                                    type="checkbox"
                                                    name="c2[]"
                                                    value="<?= h($r2['id']); ?>" <? if (!$r2['fantol_flg']) echo 'checked'; ?>>
                                                <input type="hidden" name="c2_all[]" value="<?= h($r2['id']); ?>">
                                            </td>
                                        </tr>

                                        <? foreach ($c3[$r2['id']] as $r3) { ?>
                                            <tr <? if ($r3['fantol_flg']) echo 'class="delete"'; ?>>
                                                <td></td>
                                                <td></td>
                                                <td><?= h($r3['name']); ?></td>
                                                <? if ($arr_class_name["class_cnt"] === '4') { ?><td></td><? } ?>
                                                <td class="master <? if ($r3['del_flg']) echo 'delete'; ?>"><? if ($r3['del_flg']) echo '×'; ?></td>
                                                <td class="check">
                                                    <input
                                                        id="c3_<?= h($r3['id']); ?>"
                                                        data-parent="c2_<?= h($r2['id']); ?>"
                                                        class="c3 c2_<?= h($r2['id']); ?>"
                                                        type="checkbox"
                                                        name="c3[]"
                                                        value="<?= h($r3['id']); ?>" <? if (!$r3['fantol_flg']) echo 'checked'; ?>>
                                                    <input type="hidden" name="c3_all[]" value="<?= h($r3['id']); ?>">
                                                </td>
                                            </tr>

                                            <? if ($arr_class_name["class_cnt"] === '4') { ?>
                                                <? foreach ($c4[$r3['id']] as $r4) { ?>
                                                    <tr <? if ($r4['fantol_flg']) echo 'class="delete"'; ?>>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td><?= h($r4['name']); ?></td>
                                                        <td class="master <? if ($r4['del_flg']) echo 'delete'; ?>"><? if ($r4['del_flg']) echo '×'; ?></td>
                                                        <td class="check">
                                                            <input
                                                                id="c4_<?= h($r4['id']); ?>"
                                                                data-parent="c3_<?= h($r3['id']); ?>"
                                                                class="c4 c3_<?= h($r3['id']); ?>"
                                                                type="checkbox"
                                                                name="c4[]"
                                                                value="<?= h($r4['id']); ?>" <? if (!$r4['fantol_flg']) echo 'checked'; ?>>
                                                            <input type="hidden" name="c4_all[]" value="<?= h($r4['id']); ?>">
                                                        </td>
                                                    </tr>
                                                <? } ?>
                                            <? } ?>
                                        <? } ?>
                                    <? } ?>
                                <? } ?>
                            </tbody>
                        </table>

                        <div>
                            <button type="submit">更新する</button>
                        </div>

                    </td>
                    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                </tr>
                <tr>
                    <td><img src="img/r_3.gif" width="10" height="10"></td>
                    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                    <td><img src="img/r_4.gif" width="10" height="10"></td>
                </tr>
            </table>
            <img src="img/spacer.gif" width="1" height="5" alt=""><br>

        </form>
    </body>
</html>
