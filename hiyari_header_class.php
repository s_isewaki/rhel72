<?
require_once('Cmx.php');
require_once('class/Cmx/Model/SystemConfig.php');
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");

class hiyari_header_class {

    var $INCIDENT_TITLE = "ファントルくん";

    var $session;
    var $fname;
    var $con;
    var $sysConf;
    var $design_mode;

    /**
     * コンストラクタ
     * @param string $session セッションID。
     * @param string $fname 画面名
     * @param object $con DBコネクション
     */
    function hiyari_header_class($session, $fname, $con) {
        $this->session = $session;
        $this->fname = $fname;
        $this->con = $con;
        $this->sysConf = new Cmx_SystemConfig();
        $this->design_mode = $this->sysConf->get('fantol.design.mode');
    }

    /**
     * インシデントタイトルの取得
     * @return boolean インシデントタイトル
     */
    function get_inci_title() {
        return $this->INCIDENT_TITLE;
    }

    /**
     * ログイン者のインシデント権限名取得
     * @return string ログイン者のインシデント権限名
     */
    function get_inci_auth_name() {
        return get_self_inci_auth_html_string($this->session, $this->fname, $this->con, $this->design_mode==2);
    }

    /**
     * 管理者権限有無判定
     * @return boolean 管理者権限がある場合にtrue
     */
    function is_kanrisha() {
        require_once("about_authority.php");

        $checkauth = check_authority($this->session, 48, $this->fname);
        $is_kanrisya = FALSE;
        if ($checkauth != "0") {
            $is_kanrisya = TRUE;
        }

        return $is_kanrisya;
    }

    /**
     * ユーザ画面のタブ情報を取得
     * @param array $option 補足情報(省略可)
     * @return array タブ情報配列
     */
    function get_user_tab_info($option = array()) {
        //======================================================================
        //報告画面で表示する進捗の権限数を取得（この数により画面の幅が変わる）
        //======================================================================
        $auth_count = 0;
        $recv_disp = get_recv_torey_list_disp($this->con, $this->fname);
        $send_disp = get_send_torey_list_disp($this->con, $this->fname);
        if ($recv_disp['progress_disp']=='t' || $send_disp['progress_disp']=='t'){
            if ($this->design_mode == 1){
                $arr_inci = get_inci_mst($this->con, $this->fname);
                foreach($arr_inci as $arr_key => $arr){
                    if(is_usable_auth($arr_key, $this->fname, $this->con)){
                        $auth_count++;
                    }
                }
            }
            else{
                $arr_inci_progress = search_inci_report_progress_dflt_flg($this->con, $this->fname);
                $arr = pg_fetch_all($arr_inci_progress);
                foreach($arr as $auth) {
                    if ($auth['use_flg'] == 't'){
                        $auth_count++;
                    }
                }
            }
        }
        
        //======================================================================
        //タブ文字列定数
        //======================================================================
        define("HIYARI_TAB_U_TOP", "お知らせ");
        define("HIYARI_TAB_U_REPORT", "報告");
        define("HIYARI_TAB_U_ANALYSIS", "分析");
        define("HIYARI_TAB_U_PROFILE", "プロフィール");
        define("HIYARI_TAB_U_FROW", "運用フロー");
        define("HIYARI_TAB_U_ELEARNING", "書庫");
        define("HIYARI_TAB_U_IINKAI", "委員会報告");
        define("HIYARI_TAB_U_TAISAKU", "対策実施");
        define("HIYARI_TAB_U_TOUKEI", "集計");
        define("HIYARI_TAB_U_NEWS", "お知らせ管理");
        define("HIYARI_TAB_U_AGENT", "代行者設定");
        define("HIYARI_TAB_U_HIYARI", "医療事故等情報報告"); // ヒヤリ･ハット提出
        define("HIYARI_TAB_U_IMPORT", "インポート");

        //======================================================================
        //選択タブの決定
        //======================================================================
        switch (get_file_name_from_php_self($this->fname)) {
            case "hiyari_rpt_recv_torey.php":
            case "hiyari_rpt_send_torey.php":
            case "hiyari_rpt_report_db.php":
            case "hiyari_rpt_report_classification.php":
            case "hiyari_rpt_shitagaki_db.php":
                $selected_tab = HIYARI_TAB_U_REPORT;
                break;
            case "hiyari_analysis_disp.php":
                $selected_tab = HIYARI_TAB_U_ANALYSIS;
                break;
            case "hiyari_profile_register.php":
                $selected_tab = HIYARI_TAB_U_PROFILE;
                break;
            case "hiyari_operation_flow_1.php":
            case "hiyari_operation_flow_2.php":
            case "hiyari_operation_flow_3.php":
                $selected_tab = HIYARI_TAB_U_FROW;
                break;
            case "hiyari_el_list_all.php":
                $selected_tab = HIYARI_TAB_U_ELEARNING;
                break;
            case "hiyari_iinkai.php":
                $selected_tab = HIYARI_TAB_U_IINKAI;
                break;
            case "hiyari_taisaku.php":
                $selected_tab = HIYARI_TAB_U_TAISAKU;
                break;
            case "hiyari_rm_stats.php":
            case "hiyari_rm_stats_pattern.php":
            case "hiyari_rm_stats_download.php":
                $selected_tab = HIYARI_TAB_U_TOUKEI;
                break;
            case "hiyari_news_menu.php":
                $selected_tab = HIYARI_TAB_U_NEWS;
                break;
            case "hiyari_menu.php":
                $selected_tab = HIYARI_TAB_U_TOP;
                break;
            case "hiyari_agent_register.php":
                $selected_tab = HIYARI_TAB_U_AGENT;
                break;
            case "hiyari_hiyari_list.php":
            case "hiyari_hiyari_xml_list.php":
            case "hiyari_hiyari_info_2010.php":
                $selected_tab = HIYARI_TAB_U_HIYARI;
                break;
            case "hiyari_hiyari_xml_import.php":
                $selected_tab = HIYARI_TAB_U_IMPORT;
                break;
            default:
                $selected_tab = HIYARI_TAB_U_TOP;
                break;
        }

        //======================================================================
        //タブ情報配列
        //======================================================================
        $sm_emp_flg = is_sm_emp($this->session, $this->fname);

        $tab_list = array();

        $tab["class_name"] = "nav_01";
        $tab["title"] = HIYARI_TAB_U_TOP;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_U_TOP;
        $tab["is_usable"] = true;
        $tab["url"] = "hiyari_menu.php?session=$this->session";
        $tab_list[] = $tab;

        $tab["class_name"] = "nav_02";
        $tab["title"] = HIYARI_TAB_U_REPORT;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_U_REPORT;
        $tab["is_usable"] = true;
        $tab["url"] = "hiyari_rpt_recv_torey.php?session=$this->session";
        $tab_list[] = $tab;

        $tab["class_name"] = "nav_03";
        $tab["title"] = HIYARI_TAB_U_ANALYSIS;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_U_ANALYSIS;
        $tab["is_usable"] = false;
        if (is_analysis_available($this->con, $this->fname) &&
                ( is_analysis_read_usable($this->session, $this->fname, $this->con) || is_analysis_update_usable($this->session, $this->fname, $this->con) )) {
            $tab["is_usable"] = true;
        }
        $tab["url"] = "hiyari_analysis_disp.php?session=$this->session";
        $tab_list[] = $tab;

        $tab["class_name"] = "nav_04";
        $tab["title"] = HIYARI_TAB_U_PROFILE;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_U_PROFILE;
        $arr_inci_profile = get_inci_profile($this->fname, $this->con);
        $profile_use_flg = $arr_inci_profile["use_flg"];
        $tab["is_usable"] = ($profile_use_flg == "t");
        $tab["url"] = "hiyari_profile_register.php?session=$this->session";
        $tab_list[] = $tab;

        $tab["class_name"] = "nav_05";
        $tab["title"] = HIYARI_TAB_U_FROW;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_U_FROW;
        $tab["is_usable"] = true;
        $tab["url"] = "hiyari_operation_flow_1.php?session=$this->session&mode=user";
        $tab_list[] = $tab;

        $tab["class_name"] = "nav_06";
        $tab["title"] = HIYARI_TAB_U_ELEARNING;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_U_ELEARNING;
        $tab["is_usable"] = true;
        $tab["url"] = "hiyari_el_list_all.php?session=$this->session";
        $tab_list[] = $tab;

        $tab["class_name"] = "nav_07";
        $tab["title"] = HIYARI_TAB_U_TOUKEI;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_U_TOUKEI;
        $tab["is_usable"] = is_stats_usable($this->session, $this->fname, $this->con);
        $tab["url"] = "hiyari_rm_stats.php?session=$this->session";
        $tab_list[] = $tab;

        $tab["class_name"] = "nav_08";
        $tab["title"] = HIYARI_TAB_U_NEWS;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_U_NEWS;
        $tab["is_usable"] = $sm_emp_flg;
        $tab["url"] = "hiyari_news_menu.php?session=$this->session";
        $tab_list[] = $tab;

        $tab["class_name"] = "nav_09";
        $tab["title"] = HIYARI_TAB_U_AGENT;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_U_AGENT;
        $tab["is_usable"] = is_deciding_emp($this->session, $this->fname);
        $tab["url"] = "hiyari_agent_register.php?session=$this->session";
        $tab_list[] = $tab;

        $tab["class_name"] = "nav_10";
        $tab["title"] = HIYARI_TAB_U_HIYARI;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_U_HIYARI;
        $tab["is_usable"] = $sm_emp_flg;
        $tab["url"] = "hiyari_hiyari_info_2010.php?session=$this->session";
        $tab_list[] = $tab;

        if ($auth_count <= 3) {
            $tab["class_name"] = "nav_11"; //タブが画面の右端になる場合
        }
        else{
            $tab["class_name"] = "nav_12"; //タブの右側に空エリア（nav_13~）が表示される場合
        }
        $tab["title"] = HIYARI_TAB_U_IMPORT;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_U_IMPORT;
        $tab["is_usable"] = is_import_emp($this->session, $this->fname, $this->con);
        $tab["url"] = "hiyari_hiyari_xml_import.php?session=$this->session";
        $tab_list[] = $tab;

        $tab_info['tabs'] = $tab_list;

        //======================================================================
        //幅調整用のクラス
        //======================================================================
        switch ($auth_count){
            case 4:
                $tab_info['right_end_class'] = 'nav_13';            //タブ群の右端
                $tab_info['total_width_class'] = 'total_width1';    //全体の幅
                $tab_info['main_width_class'] = 'main_width1';      //サブとメインの2カラムの場合のメインの幅
                break;
            case 5:
                $tab_info['right_end_class'] = 'nav_14';
                $tab_info['total_width_class'] = 'total_width2';    //全体の幅
                $tab_info['main_width_class'] = 'main_width2';      //サブとメインの2カラムの場合のメインの幅
                break;
            case 6:
                $tab_info['right_end_class'] = 'nav_15';
                $tab_info['total_width_class'] = 'total_width3';    //全体の幅
                $tab_info['main_width_class'] = 'main_width3';      //サブとメインの2カラムの場合のメインの幅
                break;
            default://担当者３人までは追加幅なし
                $tab_info['width_class'] = 'width0';
                $tab_info['total_width_class'] = 'total_width0';    //全体の幅
                $tab_info['main_width_class'] = 'main_width0';      //サブとメインの2カラムの場合のメインの幅
        }

        return $tab_info;
    }

    /**
     * ユーザ画面のお知らせ管理用タブ情報を取得
     * @param string $page ページ番号
     * @param string $news_id お知らせID
     * @return array タブ情報配列
     */
    function get_news_tab_info($page = "", $news_id = "") {
        define("HIYARI_TAB_U_TOP", "お知らせ");
        define("HIYARI_TAB_U_NEWS_MENU", "お知らせ管理");
        define("HIYARI_TAB_U_NEWS_REGIST", "お知らせ登録");
        define("HIYARI_TAB_U_NEWS_UPDATE", "お知らせ詳細");
        define("HIYARI_TAB_U_NEWS_REFER_LIST", "参照者一覧");

        //======================================================================
        //選択タブの決定
        //======================================================================
        switch (get_file_name_from_php_self($this->fname)) {
            case "hiyari_news_menu.php":
                $selected_tab = HIYARI_TAB_U_NEWS_MENU;
                break;
            case "hiyari_news_register.php":
                $selected_tab = HIYARI_TAB_U_NEWS_REGIST;
                break;
            case "hiyari_news_update.php":
                $selected_tab = HIYARI_TAB_U_NEWS_UPDATE;
                break;
            case "hiyari_news_refer_list.php":
                $selected_tab = HIYARI_TAB_U_NEWS_REFER_LIST;
                break;
            default:
                $selected_tab = HIYARI_TAB_U_TOP;
                break;
        }

        //======================================================================
        //タブ情報配列
        //======================================================================
        $tab_list = array();

        $tab["url"] = "hiyari_menu.php?session=$this->session";
        $tab["title"] = HIYARI_TAB_U_TOP;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_U_TOP;
        $tab["is_usable"] = true;
        $tab_list[] = $tab;

        $tab["url"] = "hiyari_news_menu.php?session=$this->session";
        if ($page != "") {
            $tab["url"] .= "&page=$page";
        }
        $tab["title"] = HIYARI_TAB_U_NEWS_MENU;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_U_NEWS_MENU;
        $tab["is_usable"] = true;
        $tab_list[] = $tab;

        if ($selected_tab == HIYARI_TAB_U_NEWS_MENU || $selected_tab == HIYARI_TAB_U_NEWS_REGIST) {
            $tab["url"] = "hiyari_news_register.php?session=$this->session";
            $tab["title"] = HIYARI_TAB_U_NEWS_REGIST;
            $tab["is_active"] = $selected_tab == HIYARI_TAB_U_NEWS_REGIST;
            $tab["is_usable"] = true;
            $tab_list[] = $tab;
        }

        if ($selected_tab == HIYARI_TAB_U_NEWS_UPDATE) {
            $tab["url"] = "hiyari_news_update.php?session=$this->session";
            if ($news_id != "") {
                $tab["url"] .= "&news_id=$news_id";
            }
            $tab["title"] = HIYARI_TAB_U_NEWS_UPDATE;
            $tab["is_active"] = $selected_tab == HIYARI_TAB_U_NEWS_UPDATE;
            $tab["is_usable"] = true;
            $tab_list[] = $tab;
        }

        if ($selected_tab == HIYARI_TAB_U_NEWS_REFER_LIST) {
            $tab["url"] = "hiyari_news_refer_list.php?session=$this->session";
            if ($news_id != "") {
                $tab["url"] .= "&news_id=$news_id";
            }
            $tab["title"] = HIYARI_TAB_U_NEWS_REFER_LIST;
            $tab["is_active"] = $selected_tab == HIYARI_TAB_U_NEWS_REFER_LIST;
            $tab["is_usable"] = true;
            $tab_list[] = $tab;
        }

        $tab_info['tabs'] = $tab_list;
        return $tab_info;
    }

    /**
     * 管理画面用タブ情報を取得(今のところ運用フローのみ対応)
     * @param array $option 補足情報(省略可)
     * @return array タブ情報配列
     */
    function get_kanri_tab_info($option = array()) {
        //======================================================================
        //タブ文字列定数
        //======================================================================
        define("HIYARI_TAB_K_FROW", "運用フロー");
        define("HIYARI_TAB_K_ANONYMOUS", "匿名設定");
        define("HIYARI_TAB_K_SEND", "送信先設定");
        define("HIYARI_TAB_K_PROGRESS_FLG", "進捗設定初期登録");
        define("HIYARI_TAB_K_FROW_OTHER", "その他");

        //======================================================================
        //選択タブの決定
        //======================================================================
        switch (get_file_name_from_php_self($this->fname)) {
            case "hiyari_operation_flow_1.php":
            case "hiyari_operation_flow_2.php":
            case "hiyari_operation_flow_3.php":
                $selected_tab = HIYARI_TAB_K_FROW;
                break;
        }

        //======================================================================
        //タブ情報配列
        //======================================================================
        $tab_list = array();
        $tab["is_usable"] = true;

        $tab["url"] = "hiyari_operation_flow_1.php?session=$this->session&mode=kanri";
        $tab["title"] = HIYARI_TAB_K_FROW;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_K_FROW;
        $tab_list[] = $tab;

        $tab["url"] = "hiyari_anonymous_register.php?session=$this->session";
        $tab["title"] = HIYARI_TAB_K_ANONYMOUS;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_K_ANONYMOUS;
        $tab_list[] = $tab;

        $tab["url"] = "hiyari_send_register.php?session=$this->session";
        $tab["title"] = HIYARI_TAB_K_SEND;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_K_SEND;
        $tab_list[] = $tab;

        $tab["url"] = "hiyari_progress_dflt_flg_update.php?session=$this->session";
        $tab["title"] = HIYARI_TAB_K_PROGRESS_FLG;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_K_PROGRESS_FLG;
        $tab_list[] = $tab;

        $tab["url"] = "hiyari_operation_other.php?session=$this->session";
        $tab["title"] = HIYARI_TAB_K_FROW_OTHER;
        $tab["is_active"] = $selected_tab == HIYARI_TAB_K_FROW_OTHER;
        $tab_list[] = $tab;

        $tab_info['tabs'] = $tab_list;
        return $tab_info;
    }
}
