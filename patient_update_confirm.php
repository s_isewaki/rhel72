<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="patient_detail.php">
<input type="hidden" name="session" value="<? echo $session; ?>">
<input type="hidden" name="pt_id" value="<? echo $pt_id; ?>">
<input type="hidden" name="new_pt_id" value="<? echo $new_pt_id; ?>">
<input type="hidden" name="lt_nm" value="<? echo $lt_nm; ?>">
<input type="hidden" name="ft_nm" value="<? echo $ft_nm; ?>">
<input type="hidden" name="lt_kana_nm" value="<? echo $lt_kana_nm; ?>">
<input type="hidden" name="ft_kana_nm" value="<? echo $ft_kana_nm; ?>">
<input type="hidden" name="birth_yr" value="<? echo $birth_yr; ?>">
<input type="hidden" name="birth_yr2" value="<? echo $birth_yr2; ?>">
<input type="hidden" name="birth_mon" value="<? echo $birth_mon; ?>">
<input type="hidden" name="birth_day" value="<? echo $birth_day; ?>">
<input type="hidden" name="sex" value="<? echo $sex; ?>">
<input type="hidden" name="job" value="<? echo $job; ?>">
<input type="hidden" name="key1" value="<? echo $key1; ?>">
<input type="hidden" name="key2" value="<? echo $key2; ?>">
<input type="hidden" name="page" value="<? echo $page; ?>">
<input type="hidden" name="wareki_flg" value="<? echo $wareki_flg; ?>">
<input type="hidden" name="back" value="t">
</form>
<?
require_once("about_comedix.php");
require_once("patient_common.ini");
require_once("get_values.ini");
require_once("label_by_profile_type.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 患者登録権限のチェック
$reg_auth = check_authority($session,22,$fname);
if($reg_auth == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$profile_type = get_profile_type($con, $fname);

// トランザクションの開始
pg_query($con, "begin transaction");

// 患者ID未入力チェック
if ($new_pt_id == "") {
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"{$_label_by_profile["PATIENT_ID"][$profile_type]}を入力してください\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

// 患者ID文字種チェック
if (preg_match("/[^0-9a-zA-Z]/", $new_pt_id) > 0) {
	echo("<script type=\"text/javascript\">alert(\"{$_label_by_profile["PATIENT_ID"][$profile_type]}に使える文字は半角英数字のみです。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//患者ID 文字数 チェック
if (strlen($new_pt_id) > 12) {
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"{$_label_by_profile["PATIENT_ID"][$profile_type]}が長すぎます\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

// 患者ID重複チェック
if (strcmp($new_pt_id, $pt_id) != 0) {
	$sql = "select count(*) from ptifmst";
	$cond = "where ptif_id = '$new_pt_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel==0) {
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script language=\"javascript\">alert(\"指定された{$_label_by_profile["PATIENT_ID"][$profile_type]}は既に使用されています\");</script>\n");
		echo("<script language=\"javascript\">document.items.submit();</script>\n");
		exit;
	}
}

//姓（漢字）null チェック
if($lt_nm ==""){
	echo("<script language=\"javascript\">alert(\"姓（漢字）を入力してください\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//姓（漢字）文字数チェック
if(strlen($lt_nm) > 20){
	echo("<script language=\"javascript\">alert(\"姓（漢字）が長すぎます\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//名前（漢字）null チェック
if($ft_nm ==""){
	echo("<script language=\"javascript\">alert(\"名前（漢字）を入力してください\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//名前（漢字）文字数チェック
if(strlen($ft_nm) > 20){
	echo("<script language=\"javascript\">alert(\"名前（漢字）が長すぎます\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//姓（かな）null チェック
if($lt_kana_nm ==""){
	echo("<script language=\"javascript\">alert(\"姓（かな）を入力してください\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}
$lt_kana_nm = mb_convert_kana($lt_kana_nm, "HVc");

//姓（かな）文字数チェック
if(strlen($lt_kana_nm) > 20){
	echo("<script language=\"javascript\">alert(\"姓（かな）が長すぎます\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//名前（かな）null チェック
if($ft_kana_nm ==""){
	echo("<script language=\"javascript\">alert(\"名前（かな）を入力してください\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}
$ft_kana_nm = mb_convert_kana($ft_kana_nm, "HVc");

//名前（かな）文字数チェック
if(strlen($ft_kana_nm) > 20){
	echo("<script language=\"javascript\">alert(\"名前（かな）が長すぎます\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//生年月日 実在日 チェック
if (!checkdate($birth_mon, $birth_day, $birth_yr)) {
	echo("<script language=\"javascript\">alert(\"生年月日が無効です\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//生年月日 未来日 チェック
$birth = "$birth_yr$birth_mon$birth_day";
$today = date(Ymd);
if ($birth > $today) {
	echo("<script language=\"javascript\">alert(\"生年月日が未来日付です\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

// 職業 文字列長チェック
if(strlen($job) > 100){
	echo("<script language=\"javascript\">alert(\"職業が長すぎます\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

// 姓（かな）のキーワードを取得
$keywd = get_keyword($lt_kana_nm);

// 患者情報の更新
$sql = "update ptifmst set";
$set = array("ptif_id", "ptif_lt_kana_nm", "ptif_ft_kana_nm", "ptif_lt_kaj_nm", "ptif_ft_kaj_nm", "ptif_keywd", "ptif_birth", "ptif_sex", "ptif_buis");
$setvalue = array($new_pt_id, $lt_kana_nm, $ft_kana_nm, $lt_nm, $ft_nm, $keywd, $birth, $sex, $job);
$cond = "where ptif_id = '$pt_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 入院患者マスタの更新（なければ無視）
$sql = "update inptmst set";
$set = array("ptif_id", "inpt_lt_kn_nm", "inpt_ft_kn_nm", "inpt_lt_kj_nm", "inpt_ft_kj_nm", "inpt_keywd");
$setvalue = array($new_pt_id, $lt_kana_nm, $ft_kana_nm, $lt_nm, $ft_nm, $keywd);
$cond = "where ptif_id = '$pt_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 入院予定患者マスタの更新（なければ無視）
$sql = "update inptres set";
$set = array("inpt_lt_kn_nm", "inpt_ft_kn_nm", "inpt_lt_kj_nm", "inpt_ft_kj_nm", "inpt_keywd");
$setvalue = array($lt_kana_nm, $ft_kana_nm, $lt_nm, $ft_nm, $keywd);
$cond = "where ptif_id = '$pt_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 退院患者マスタの更新（なければ無視）
$sql = "update inpthist set";
$set = array("inpt_lt_kn_nm", "inpt_ft_kn_nm", "inpt_lt_kj_nm", "inpt_ft_kj_nm", "inpt_keywd");
$setvalue = array($lt_kana_nm, $ft_kana_nm, $lt_nm, $ft_nm, $keywd);
$cond = "where ptif_id = '$pt_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// キャンセル患者マスタの更新（なければ無視）
$sql = "update inptcancel set";
$set = array("inpt_lt_kn_nm", "inpt_ft_kn_nm", "inpt_lt_kj_nm", "inpt_ft_kj_nm", "inpt_keywd");
$setvalue = array($lt_kana_nm, $ft_kana_nm, $lt_nm, $ft_nm, $keywd);
$cond = "where ptif_id = '$pt_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 患者IDを更新
if (strcmp($new_pt_id, $pt_id) != 0) {
	update_patient_id($con, $pt_id, $new_pt_id, $fname);
}

// トランザクションの終了
pg_query($con, "commit");
//pg_query($con, "rollback");

// データベース接続を閉じる
pg_close($con);

if ($wareki_flg == "") {
	$wareki_flg = "f";
}
// 基本画面に遷移
$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);
echo("<script language=\"javascript\">location.href = 'patient_detail.php?session=$session&pt_id=$new_pt_id&key1=$url_key1&key2=$url_key2&page=$page&wareki_flg=$wareki_flg'</script>");
?>
</body>
