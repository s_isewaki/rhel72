<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 設備予約 | 日</title>
<?
//ini_set("display_errors","1");
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_comedix.php");
require("show_facility.ini");
require("facility_common.ini");
require("facility_menu_common.ini");
require("holiday.php");
require("label_by_profile_type.ini");
require("show_calendar_memo.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 設備予約権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 施設・設備登録権限を取得
$fcl_admin_auth = check_authority($session, 41, $fname);

// 表示範囲が渡されていない場合は「全て」に設定
if ($range == "") {
	$range = "all";
}

// タイムスタンプが渡されていない場合は当日日付のタイムスタンプを取得
$today = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
if ($date == "") {
	$date = $today;
}

// 前月・前日・翌日・翌月のタイムスタンプを変数に格納
$last_month = get_last_month($date);
$last_date = strtotime("-1 day", $date);
$next_date = strtotime("+1 day", $date);
$next_month = get_next_month($date);

// データベースに接続
$con = connect2db($fname);

$start_date = date("Ymd", $date);
// カレンダーのメモを取得
$arr_calendar_memo = get_calendar_memo($con, $fname, $start_date, $start_date);

$holiday_name = ktHolidayName($date);

if ($holiday_name != "") {
	$holiday_bgcolor = " bgcolor=\"#fadede\"";
} else if ($arr_calendar_memo["{$start_date}_type"] == "5") {
	$holiday_bgcolor = " bgcolor=\"#defafa\"";
	$holiday_name = "&nbsp;";
} else {
	$holiday_bgcolor = "";
	$holiday_name = "&nbsp;";
}

if ($arr_calendar_memo["$start_date"] != "") {
	if ($holiday_name == "&nbsp;") {
		$holiday_name = $arr_calendar_memo["$start_date"];
	} else {
		$holiday_name .= "<br>".$arr_calendar_memo["$start_date"];
	}
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// アクセスログ
$optsql = "select facility_default from option";
$optcond = "where emp_id = '{$emp_id}'";
$optsel = select_from_table($con, $optsql, $optcond, $fname);
if ($optsel == 0) {
    pg_close($con);
    js_error_exit();
}
$facility_default = pg_fetch_result($optsel, 0, "facility_default");

if ($facility_default === '1') {
	aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);
}

// 参照／予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_referable_categories($con, $emp_id, $fname);

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];

// 表の開始時刻
$start_hour = 6;

// 患者氏名/利用者氏名
$patient_name_title = $_label_by_profile["PATIENT_NAME"][$profile_type];
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">
function changeDate(dt) {
	document.view.date.value = dt;
	document.view.submit();
}

function popupReservationDetail(facility, date, time, title, type, text, user_count, reg_emp, upd_emp, e, with_patient, patient, show_type, ext, tmp_class) {

	var arr_value = new Array(
			'施設・設備', facility,
			'日付', date,
			'時刻', time,
			'タイトル', title
		);

	if (show_type) {
		arr_value.push( '種別', type );
	}
	if (with_patient) {
		arr_value.push( '<?=$patient_name_title?>', patient );
	}
	arr_value.push(
			'内容', text,
			'利用人数', user_count,
			'登録者', reg_emp + '<br />' + tmp_class,
			'登録者内線番号', ext,
			'最終更新者', upd_emp
		);

	popupDetailBlue(arr_value, 400, 100, e);
}

function openPrintWindow() {
	window.open('facility_print_menu.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>', 'newwin', 'width=840,height=600,scrollbars=yes');
}
// CSVダウンロード用
function downloadCSV(){
    document.csv.action = 'facility_csv.php';
    document.csv.submit();
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>"><img src="img/icon/b06.gif" width="32" height="32" border="0" alt="設備予約"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>"><b>設備予約</b></a></font></td>
<? if ($fcl_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="facility_master_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="95" align="center" bgcolor="#5279a5"><a href="facility_menu.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>日</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_week.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週（表）</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_week_chart.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週（チャート）</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_month.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_year.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_bulk_register.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_option.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td align="right"><input type="button" value="印刷" onclick="openPrintWindow();"></td>
<td width="20"><input type="button" value="CSV出力" onclick="downloadCSV();"></td>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="view" action="facility_menu.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="30"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示範囲
<select name="range" style="vertical-align:middle;" onchange="this.form.submit();"><? show_range_options($categories, $range); ?></select><img src="img/spacer.gif" width="50" height="1" alt=""><a href="facility_menu.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($last_month); ?>">&lt;&lt;前月</a>
<a href="facility_menu.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($last_date); ?>">&lt;前日</a>
<a href="facility_menu.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($today); ?>">今日</a>
<select onchange="changeDate(this.options[this.selectedIndex].value);" style="vertical-align:middle;"><? show_date_options($date, $session, $range) ?></select>
<a href="facility_menu.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($next_date); ?>">翌日&gt;</a>
<a href="facility_menu.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($next_month); ?>">翌月&gt;&gt;</a>
</font></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
</form>
<div style="position:relative;">
<table border="1" cellspacing="0" cellpadding="0">
<tr height="22">
<td colspan="2" width="140">&nbsp;</td>

<?
for ($i = $start_hour; $i < 24; $i++) {

	echo '<td width="28" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">'.sprintf("%02d",$i).'</font></td>'."\n";
}
?>
<td width="128" align="center"<? echo($holiday_bgcolor); ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red"><? echo($holiday_name); ?></font></td>
</tr>
<?
$reservations = get_reservation($con, $categories, $range, $emp_id, $date, $session, $fname);

show_border($con, $categories, $range, $emp_id, $date, $session, $fname, $start_hour, $reservations);
?>
</table>
<table border="1" cellspacing="0" cellpadding="0" style="position:absolute;top:0;left:0;">
<tr height="22">
<td colspan="2" width="140">&nbsp;</td>
<?
for ($i = $start_hour; $i < 24; $i++) {

	echo '<td width="28" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">'.sprintf("%02d",$i).'</font></td>'."\n";
}
?>
<td width="128" align="center"<? echo($holiday_bgcolor); ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red"><? echo($holiday_name); ?></font></td>
</tr>
<? show_reservation($con, $categories, $range, $emp_id, $date, $session, $fname, $start_hour, $reservations);
?>
</table>
</div>
</td>
</tr>
</table>
</body>
<!-- CSV出力用form -->
<form name="csv" method="get">
    <input type="hidden" name="session" value="<?php echo $session; ?>">
    <input type="hidden" name="start_date" value="<?php echo $start_date ?>">
    <input type="hidden" name="end_date" value="<?php echo $start_date ?>">
    <input type="hidden" name="range" value="<?php echo $range; ?>">
</form>

<? pg_close($con); ?>
</html>
<?
// 日付オプションを出力
function show_date_options($date, $session, $range) {

	for ($i = -7; $i <= -1; $i++) {
		$tmp_date = strtotime("$i days", $date);
		$tmp_ymd = date("Y/m/d", $tmp_date);
		$tmp_wd = "(" . get_weekday($tmp_date) . ")";
		echo("<option value=\"$tmp_date\">$tmp_ymd$tmp_wd");
	}

	$tmp_ymd = date("Y/m/d", $date);
	$tmp_wd = "(" . get_weekday($date) . ")";
	echo("<option value=\"$date\" selected>【{$tmp_ymd}{$tmp_wd}】");

	for ($i = 1; $i <= 7; $i++) {
		$tmp_date = strtotime("+$i days", $date);
		$tmp_ymd = date("Y/m/d", $tmp_date);
		$tmp_wd = "(" . get_weekday($tmp_date) . ")";
		echo("<option value=\"$tmp_date\">$tmp_ymd$tmp_wd");
	}

}

// 予約状況の罫線を出力
function show_border($con, $categories, $range, $emp_id, $date, $session, $fname, $start_hour, $reservations) {
	$cate_col = 3 + (24 - $start_hour);

	list($selected_category_id, $selected_facility_id) = split("-", $range);

	// 不要なカテゴリー情報を削除
	if ($selected_category_id != "all") {
		$categories = array($selected_category_id => $categories[$selected_category_id]);
	}

	// 不要な設備情報を削除
	if ($selected_facility_id != "") {
		$categories[$selected_category_id]["facilities"] = array(
			$selected_facility_id => $categories[$selected_category_id]["facilities"][$selected_facility_id]
		);
	}

	$ymd = date("Ymd", $date);

	// カテゴリ一覧をループ
	foreach ($categories as $tmp_category_id => $tmp_category) {
		$tmp_category_name = $tmp_category["name"];
		$tmp_bg_color = $tmp_category["bg_color"];

		// カテゴリ行を出力
		echo("<tr bgcolor=\"#$tmp_bg_color\">\n");
		echo("<td height=\"22\" colspan=\"{$cate_col}\" style=\"padding:1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><b>$tmp_category_name</b></font></td>\n");
		echo("</tr>\n");

		// 施設・設備一覧をループ
		foreach ($tmp_category["facilities"] as $tmp_facility_id => $tmp_facility) {
			$tmp_facility_name = $tmp_facility["name"];
			$tmp_reservable = $tmp_facility["reservable"];
			$tmp_admin_flg = $tmp_facility["admin_flg"];

			// 予約情報配列を参照
			$tmp_reservation = $reservations["{$tmp_facility_id}"];
			$rsv_count = count($tmp_reservation);
			if ($rsv_count <= 1) {
				$rowspan = "";
			} else {
				$rowspan = " rowspan=\"$rsv_count\"";
			}
			echo("<tr height=\"22\">\n");
			echo("<td$rowspan valign=\"top\" style=\"border-right-style:none;padding:1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"javascript:void(0);\" onclick=\"window.open('facility_detail.php?session=$session&facility_id=$tmp_facility_id', 'newwin', 'width=730,height=480,scrollingbars=yes');\">$tmp_facility_name</a></font></td>\n");
			if ($tmp_reservable) {
				echo("<td$rowspan align=\"right\" valign=\"top\" style=\"border-left-style:none;border-right-style:none;padding:1px;\"><img src=\"img/pencil.gif\" alt=\"\" width=\"13\" height=\"13\" onclick=\"window.open('facility_reservation_register.php?session=$session&cate_id=$tmp_category_id&facility_id=$tmp_facility_id&ymd=$ymd&path=1&range=$range', 'newwin', 'width=730,height='+((screen.availHeight-60<750)? screen.availHeight-60: 750)+' ,scrollbars=yes');\" style=\"cursor:pointer\"></td>\n");
			} else {
				echo("<td$rowspan style=\"border-left-style:none;border-right-style:none;\">&nbsp;</td>");
			}
			if ($rsv_count == 0) {
				for ($j = $start_hour; $j <= 23; $j++) {
					echo("<td style=\"border-left:#0000cc solid 1px;border-right:none;\">&nbsp;</td>\n");
				}
				echo("<td style=\"border-left:#0000cc solid 1px;\">&nbsp;</td>\n");
				echo("</tr>\n");
			} else {

				// 予約行を出力
				for ($i = 0; $i < $rsv_count; $i++) {
					$tmp_rsv_id = $tmp_reservation[$i]["reservation_id"];
					$tmp_s_time = $tmp_reservation[$i]["start_time"];
					$tmp_e_time = $tmp_reservation[$i]["end_time"];
					$tmp_title = $tmp_reservation[$i]["title"];
					$tmp_rsv_emp_id = $tmp_reservation[$i]["emp_id"];
					$tmp_marker = $tmp_reservation[$i]["marker"];

					if ($i == 0) {
						if ($rsv_count == 1) {
							$row_style = "";
						} else {
							$row_style = "border-bottom:silver solid 1px;";
						}
					} else if ($i < $rsv_count - 1) {
						$row_style = "border-top-style:none;border-bottom:silver solid 1px;";
						echo("<tr>\n");
					} else {
						$row_style = "border-top-style:none;";
						echo("<tr>\n");
					}

					if ($tmp_rsv_emp_id == $emp_id) {
						$bgcolor = "blue";
						$detail_url = "facility_reservation_update.php?session=$session&rsv_id=$tmp_rsv_id&path=1&range=$range";
					} else if ($tmp_admin_flg) {
						$bgcolor = "red";
						$detail_url = "facility_reservation_update.php?session=$session&rsv_id=$tmp_rsv_id&path=1&range=$range";
					} else {
						$bgcolor = "red";
						$detail_url = "facility_reservation_detail.php?session=$session&rsv_id=$tmp_rsv_id&path=1&range=$range";
					}

					// 時刻指定なしの場合
					if ($tmp_s_time == "") {
						$timeless = "t";
						$tmp_s_time = "0600";
						$tmp_e_time = "0600";
						$detail_url .= "&timeless=t";
					} else {
						$timeless = "f";
					}

					$tmp_s_minutes = substr($tmp_s_time, 0, 2) * 60 + substr($tmp_s_time, 2, 2);
					$tmp_e_minutes = substr($tmp_e_time, 0, 2) * 60 + substr($tmp_e_time, 2, 2);
					// 表の開始時刻以降の場合
					if ($tmp_s_minutes - $start_hour * 60 >= 0) {

						$position_left = round(($tmp_s_minutes - $start_hour * 60) / 5 * 2.5);

						$color_cell_width = round(($tmp_e_minutes - $tmp_s_minutes) / 5 * 2.5);
					} else {
					// 表の開始時刻より前の場合
						$position_left = 0;
						$color_cell_width = round(($tmp_e_minutes - ($start_hour * 60)) / 5 * 2.5);
					}

					$tmp_s_time_str = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $tmp_s_time);
					$tmp_e_time_str = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $tmp_e_time);
					if ($timeless != "t") {
						$tmp_time_str = "{$tmp_s_time_str}-{$tmp_e_time_str}<br>";
					} else {
						// 縦方向の位置あわせ用に空白画像を設定
						$tmp_time_str = "<img src=\"img/spacer.gif\" alt=\"\" width=\"8\" height=\"8\">&nbsp;";
					}

					$style = get_style_by_marker_dummy($tmp_marker);

					for ($j = $start_hour; $j <= 23; $j++) {
						echo("<td style=\"border-left:#0000cc solid 1px;border-right:none;$row_style\">&nbsp;</td>\n");
					}
					echo("<td style=\"border-left:#0000cc solid 1px;$row_style\">\n");
					echo("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin:0;\">\n");
					echo("<tr>\n");
					echo("<td width=\"120\" style=\"padding:0 3px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"#ffffff\">{$tmp_time_str}<span$style>" . h($tmp_title) . "</span></font></td>\n");
					echo("</tr>\n");
					echo("</table>\n");
					echo("</td>\n");
					echo("</tr>\n");
				}
			}
		}
	}
}

// 予約状況を出力
function show_reservation($con, $categories, $range, $emp_id, $date, $session, $fname, $start_hour, $reservations) {
	$cate_col = 3 + (24 - $start_hour);
	$rsv_col = 1 + (24 - $start_hour);

	list($selected_category_id, $selected_facility_id) = split("-", $range);

	// 不要なカテゴリー情報を削除
	if ($selected_category_id != "all") {
		$categories = array($selected_category_id => $categories[$selected_category_id]);
	}

	// 不要な設備情報を削除
	if ($selected_facility_id != "") {
		$categories[$selected_category_id]["facilities"] = array(
			$selected_facility_id => $categories[$selected_category_id]["facilities"][$selected_facility_id]
		);
	}

	$ymd = date("Ymd", $date);

	// カテゴリ一覧をループ
	foreach ($categories as $tmp_category_id => $tmp_category) {
		$tmp_category_name = $tmp_category["name"];
		$tmp_bg_color = $tmp_category["bg_color"];

		// カテゴリ行を出力
		echo("<tr bgcolor=\"#$tmp_bg_color\">\n");
		echo("<td height=\"22\" colspan=\"{$cate_col}\" style=\"padding:1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><b>$tmp_category_name</b></font></td>\n");
		echo("</tr>\n");

		// 施設・設備一覧をループ
		foreach ($tmp_category["facilities"] as $tmp_facility_id => $tmp_facility) {
			$tmp_facility_name = $tmp_facility["name"];
			$tmp_reservable = $tmp_facility["reservable"];
			$tmp_admin_flg = $tmp_facility["admin_flg"];
			$show_type = ($tmp_facility["show_type_flg"] == 't') ? "true" : "false";

			// 予約情報配列を参照
			$tmp_reservation = $reservations["{$tmp_facility_id}"];
			$rsv_count = count($tmp_reservation);
			if ($rsv_count <= 1) {
				$rowspan = "";
			} else {
				$rowspan = " rowspan=\"$rsv_count\"";
			}
			echo("<tr height=\"22\">\n");
			echo("<td$rowspan valign=\"top\" style=\"border-right-style:none;padding:1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"javascript:void(0);\" onclick=\"window.open('facility_detail.php?session=$session&facility_id=$tmp_facility_id', 'newwin', 'width=730,height=480,scrollingbars=yes');\">$tmp_facility_name</a></font></td>\n");
			if ($tmp_reservable) {
				echo("<td$rowspan align=\"right\" valign=\"top\" style=\"border-left-style:none;border-right-style:none;padding:1px;\"><img src=\"img/pencil.gif\" alt=\"\" width=\"13\" height=\"13\" onclick=\"window.open('facility_reservation_register.php?session=$session&cate_id=$tmp_category_id&facility_id=$tmp_facility_id&ymd=$ymd&path=1&range=$range', 'newwin', 'width=730,height='+((screen.availHeight-60<750)? screen.availHeight-60: 750)+' ,scrollbars=yes');\" style=\"cursor:pointer\"></td>\n");
			} else {
				echo("<td$rowspan style=\"border-left-style:none;border-right-style:none;\">&nbsp;</td>");
			}
			if ($rsv_count == 0) {
				echo("<td colspan=\"{$rsv_col}\" style=\"border-left:#0000cc solid 1px;\">&nbsp</td>\n");
				echo("</tr>\n");
			} else {

				// 予約行を出力
				for ($i = 0; $i < $rsv_count; $i++) {

					$tmp_rsv_id = $tmp_reservation[$i]["reservation_id"];
					$tmp_s_time = $tmp_reservation[$i]["start_time"];
					$tmp_e_time = $tmp_reservation[$i]["end_time"];
					$tmp_title = $tmp_reservation[$i]["title"];
					$tmp_rsv_emp_id = $tmp_reservation[$i]["emp_id"];

					$tmp_type1_nm = $tmp_reservation[$i]["type1_nm"];
					$tmp_type2_nm = $tmp_reservation[$i]["type2_nm"];
					$tmp_type3_nm = $tmp_reservation[$i]["type3_nm"];
					$tmp_content = $tmp_reservation[$i]["content"];
					$tmp_users = $tmp_reservation[$i]["users"];
					$tmp_reg_emp_nm = $tmp_reservation[$i]["reg_emp_nm"];
					$tmp_upd_emp_nm = $tmp_reservation[$i]["upd_emp_nm"];
					$tmp_pt_flg = $tmp_reservation[$i]["pt_flg"];
					$tmp_marker = $tmp_reservation[$i]["marker"];
					$tmp_ext = $tmp_reservation[$i]["emp_ext"];

					$sql = "select class_nm, atrb_nm, dept_nm, room_nm from empmst inner join classmst on empmst.emp_class = classmst.class_id inner join atrbmst on empmst.emp_attribute = atrbmst.atrb_id inner join deptmst on empmst.emp_dept = deptmst.dept_id left join classroom on empmst.emp_room = classroom.room_id";
					$cond = "where emp_id = '{$tmp_rsv_emp_id}'";
					$sel = select_from_table($con, $sql, $cond, $fname);
					if ($sel == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					$tmp_class = pg_fetch_result($sel, 0, "class_nm");
					$tmp_atrb = pg_fetch_result($sel, 0, "atrb_nm");
					$tmp_dept = pg_fetch_result($sel, 0, "dept_nm");
					$tmp_room = pg_fetch_result($sel, 0, "room_nm");

					$tmp_class_disp = "$tmp_class &gt; $tmp_atrb &gt; $tmp_dept";
					if ($tmp_room != "") {
						$tmp_class_disp .= " &gt; $tmp_room";
					}

					$tmp_fclhist_content_type = $tmp_reservation[$i]["fclhist_content_type"];

					// 内容編集
					if ($tmp_fclhist_content_type == "2") {
						$tmp_content = get_text_from_xml($tmp_content);
					}

					if ($i == 0) {
						if ($rsv_count == 1) {
							$row_style = "";
						} else {
							$row_style = "border-bottom:silver solid 1px;";
						}
					} else if ($i < $rsv_count - 1) {
						$row_style = "border-top-style:none;border-bottom:silver solid 1px;";
						echo("<tr>\n");
					} else {
						$row_style = "border-top-style:none;";
						echo("<tr>\n");
					}

					if ($tmp_rsv_emp_id == $emp_id) {
						$bgcolor = "blue";
						$detail_url = "facility_reservation_update.php?session=$session&rsv_id=$tmp_rsv_id&path=1&range=$range";
						$height = "'+((screen.availHeight-60<760)? screen.availHeight-60: 760)+'";
					} else if ($tmp_admin_flg) {
						$bgcolor = "red";
						$detail_url = "facility_reservation_update.php?session=$session&rsv_id=$tmp_rsv_id&path=1&range=$range";
						$height = "'+((screen.availHeight-60<760)? screen.availHeight-60: 760)+'";
					} else {
						$bgcolor = "red";
						$detail_url = "facility_reservation_detail.php?session=$session&rsv_id=$tmp_rsv_id&path=1&range=$range";
						$height = "480";
					}

					// 時刻指定なしの場合
					if ($tmp_s_time == "") {
						$timeless = "t";
						$tmp_s_time = "0600";
						$tmp_e_time = "0600";
						$detail_url .= "&timeless=t";
					} else {
						$timeless = "f";
					}
					$tmp_s_minutes = substr($tmp_s_time, 0, 2) * 60 + substr($tmp_s_time, 2, 2);
					$tmp_e_minutes = substr($tmp_e_time, 0, 2) * 60 + substr($tmp_e_time, 2, 2);
					// 表の開始時刻以降の場合
					if ($tmp_s_minutes - $start_hour * 60 >= 0) {

						$position_left = round(($tmp_s_minutes - $start_hour * 60) / 5 * 2.5);

						$color_cell_width = round(($tmp_e_minutes - $tmp_s_minutes) / 5 * 2.5);
					} else {
					// 表の開始時刻より前の場合
						$position_left = 0;
						$color_cell_width = round(($tmp_e_minutes - ($start_hour * 60)) / 5 * 2.5);
					}

					$tmp_s_time_str = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $tmp_s_time);
					$tmp_e_time_str = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $tmp_e_time);

					if ($timeless != "t") {
						$tmp_time_str = "{$tmp_s_time_str}-{$tmp_e_time_str}<br>";
						$tmp_time_str_popup = "$tmp_s_time_str 〜 $tmp_e_time_str";
					} else {
						$tmp_time_str = "<img src=\"img/icon/timeless.gif\" alt=\"時刻指定なし\" width=\"8\" height=\"8\">&nbsp;";
						$tmp_time_str_popup = "指定なし";
					}
					$tmp_date = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $ymd);
					$tmp_types = array();
					if ($tmp_type1_nm != "") {$tmp_types[] = $tmp_type1_nm;}
					if ($tmp_type2_nm != "") {$tmp_types[] = $tmp_type2_nm;}
					if ($tmp_type3_nm != "") {$tmp_types[] = $tmp_type3_nm;}
					$tmp_type_nm = join(", ", $tmp_types);
					$tmp_users = ($tmp_users != "") ? $tmp_users . "名" : "";
					$with_patients = ($tmp_pt_flg == "t") ? "true" : "false";
					if ($tmp_pt_flg == "t") {
						$tmp_patients = $tmp_reservation[$i]["pt_name"];
					}

					$style = get_style_by_marker($tmp_marker);

					$popup = "onmousemove=\"popupReservationDetail('$tmp_category_name &gt; $tmp_facility_name', '$tmp_date', '$tmp_time_str_popup', '" . h(h_jsparam($tmp_title)) . "', '$tmp_type_nm', '" . h(preg_replace("/\r?\n/", "<br>", h_jsparam($tmp_content))) . "', '$tmp_users', '$tmp_reg_emp_nm', '$tmp_upd_emp_nm', event, $with_patients, '$tmp_patients', $show_type, '$tmp_ext', '$tmp_class_disp');\" onmouseout=\"closeDetail();\"";

					echo("<td colspan=\"{$rsv_col}\" style=\"border-left:#0000cc solid 1px;$row_style\">\n");
					echo("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"position:relative;left:$position_left;margin:0;\">\n");
					echo("<tr>\n");
					echo("<td bgcolor=\"$bgcolor\" width=\"$color_cell_width\" style=\"cursor:pointer;\" onclick=\"window.open('$detail_url', 'newwin', 'width=730,height=$height,scrollbars=yes');\" $popup>&nbsp;</td>\n");
					echo("<td width=\"120\" bgcolor=\"#ffffff\" style=\"padding:0 3px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_time_str}<a href=\"javascript:void(0);\" onclick=\"window.open('$detail_url', 'newwin', 'width=730,height=$height,scrollbars=yes');\" $popup$style>" . h($tmp_title) . "</a></font></td>\n");
					echo("</tr>\n");
					echo("</table>\n");
					echo("</td>\n");
					echo("</tr>\n");
				}
			}
		}
	}
}

?>
