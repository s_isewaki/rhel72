<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 選択されている棟の病室をいったん除外対象から外す
$sql = "update ptrmmst set";
$set = array("date_except_flg");
$setvalue = array("f");
$cond = ($bldg_cd != "0") ? "where bldg_cd = '$bldg_cd'" : "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//print_r($room_no);

// チェックされた病室を除外対象にする
foreach ($room_no as $tmp_room_no) {
	list($tmp_bldg_cd, $tmp_ward_cd, $tmp_ptrm_room_no) = split("-", $tmp_room_no);

	$sql = "update ptrmmst set";
	$set = array("date_except_flg");
	$setvalue = array("t");
	$cond = "where bldg_cd = '$tmp_bldg_cd' and ward_cd = '$tmp_ward_cd' and ptrm_room_no = '$tmp_ptrm_room_no'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面遷移
echo "<script type=\"text/javascript\">location.href = 'bed_date_exception_room.php?session=$session&bldg_cd=$bldg_cd';</script>";
?>
