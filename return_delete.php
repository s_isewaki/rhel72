<?
require("about_session.php");
require("get_values.ini");
require_once("about_postgres.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");


$sql = "select apply_id from rtnapply a ";
$cond = "where exists (select * from rtnapply b where a.emp_id = b.emp_id and a.target_date = b.target_date and apply_id = $apply_id)";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

while($row = pg_fetch_array($sel))
{
    $rtn_apply_id = $row["apply_id"];

	// 承認情報を論理削除
	$sql = "update rtnaprv set";
	$set = array("delete_flg");
	$setvalue = array("t");
	$cond = "where apply_id = $rtn_apply_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	// 申請情報を論理削除
	$sql = "update rtnapply set";
	$set = array("delete_flg");
	$setvalue = array("t");
	$cond = "where apply_id = $rtn_apply_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	// 承認候補を論理削除
	$sql = "update rtnaprvemp set";
	$set = array("delete_flg");
	$setvalue = array("t");
	$cond = "where apply_id = $rtn_apply_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	// 同期・非同期情報を論理削除
	$sql = "update rtn_async_recv set";
	$set = array("delete_flg");
	$setvalue = array("t");
	$cond = "where apply_id = $rtn_apply_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュして自画面を閉じる
echo("<script type=\"text/javascript\">opener.location.href = '$pnt_url'; self.close();</script>");
?>
