<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="cl_application_apply_list.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="category" value="<?echo($category)?>">
<input type="hidden" name="workflow" value="<?echo($workflow)?>">
<input type="hidden" name="apply_title" value="<?echo($apply_title)?>">
<input type="hidden" name="approve_emp_nm" value="<?echo($approve_emp_nm)?>">
<input type="hidden" name="date_y1" value="<?echo($date_y1)?>">
<input type="hidden" name="date_m1" value="<?echo($date_m1)?>">
<input type="hidden" name="date_d1" value="<?echo($date_d1)?>">
<input type="hidden" name="date_y2" value="<?echo($date_y2)?>">
<input type="hidden" name="date_m2" value="<?echo($date_m2)?>">
<input type="hidden" name="date_d2" value="<?echo($date_d2)?>">
<input type="hidden" name="apply_stat" value="<?echo($apply_stat)?>">
<input type="hidden" name="page" value="<?echo($page)?>">
<input type="hidden" id="date_y3" name="date_y3" value="<?=$date_y3?>">
<input type="hidden" id="date_m3" name="date_m3" value="<?=$date_m3?>">
<input type="hidden" id="date_d3" name="date_d3" value="<?=$date_d3?>">
<input type="hidden" id="date_y4" name="date_y4" value="<?=$date_y4?>">
<input type="hidden" id="date_m4" name="date_m4" value="<?=$date_m4?>">
<input type="hidden" id="date_d4" name="date_d4" value="<?=$date_d4?>">
</form>

<?

require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("cl_application_workflow_common_class.php");
require_once("cl_common.ini");


$fname=$PHP_SELF;


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 決裁・申請権限のチェック
$checkauth = check_authority($session, $CAS_MENU_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

	
// データベースに接続
$con = connect2db($fname);

$obj = new cl_application_workflow_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

$count = count($apply_cancel_chk);

for ($i = 0; $i < $count; $i++) {
	if ($apply_cancel_chk[$i] != "") {

		$apply_id = $apply_cancel_chk[$i];

		// 承認状況チェック
		$apv_cnt = $obj->get_applyapv_cnt($apply_id);
		if($apv_cnt > 0) {
			echo("<script language=\"javascript\">alert('他の承認状況が変更されたため、申請取消ができません。');</script>");
			echo("<script language=\"javascript\">document.items.submit();</script>");		
		}

		// 申請論理削除
		$obj->update_delflg_all_apply($apply_id, "t");
	}

}


// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);


// 添付ファイルを削除
/*
if (!is_dir("apply")) {
	mkdir("apply", 0755);
}
for ($i = 0; $i < $count; $i++) {
	if ($apply_cancel_chk[$i] != "") {

		$apply_id = $apply_cancel_chk[$i];

		foreach (glob("apply/{$apply_id}_*.*") as $tmpfile) {
			unlink($tmpfile);
		}
	}
}
*/

//// 一覧画面に遷移
//echo("<script type=\"text/javascript\">document.items.submit();</script>");
?>
	<script type="text/javascript">
		// 一覧画面に遷移
		document.items.submit();
	</script>
</body>