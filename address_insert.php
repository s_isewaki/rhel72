<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<? require("about_postgres.php"); ?>
<? require("about_session.php"); ?>
<? require("about_authority.php"); ?>
<? require_once("Cmx.php"); ?>
<? require_once("aclg_set.php"); ?>

<?
$fname = $PHP_SELF;

$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$ward = check_authority($session,9,$fname);
if($ward == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_GET);
?>
<?
$shared_flg = trim($shared_flg);
$name1 = trim($name1);
$name2 = trim($name2);
$name_kana1 = trim($name_kana1);
$name_kana2 = trim($name_kana2);
$company = trim($company);
$department1 = trim($department1);
$department2 = trim($department2);
$post1 = trim($post1);
$post2 = trim($post2);
$postcode1 = trim($postcode1);
$postcode2 = trim($postcode2);
$address1 = trim($address1);
$address2 = trim($address2);
$address3 = trim($address3);
$tel1 = trim($tel1);
$tel2 = trim($tel2);
$tel3 = trim($tel3);
$fax1 = trim($fax1);
$fax2 = trim($fax2);
$fax3 = trim($fax3);
$mobile1 = trim($mobile1);
$mobile2 = trim($mobile2);
$mobile3 = trim($mobile3);
$email_pc = trim($email_pc);
$email_mobile = trim($email_mobile);
$memo = trim($memo);

if(!$name1){
echo("<script language='javascript'>alert(\"名前（漢字）を入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($name1) > 20){
echo("<script language='javascript'>alert(\"名前（漢字）を10文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(!$name2){
echo("<script language='javascript'>alert(\"名前（漢字）を入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($name2) > 20){
echo("<script language='javascript'>alert(\"名前（漢字）を10文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($name_kana1) > 20){
echo("<script language='javascript'>alert(\"名前（カナ）を10文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($name_kana2) > 20){
echo("<script language='javascript'>alert(\"名前（カナ）を10文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(!$company){
echo("<script language='javascript'>alert(\"会社名を入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($company) > 50){
echo("<script language='javascript'>alert(\"会社名を25文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($department1) > 50){
echo("<script language='javascript'>alert(\"部門名１を25文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($department2) > 50){
echo("<script language='javascript'>alert(\"部門名２を25文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($post1) > 50){
echo("<script language='javascript'>alert(\"役職１を25文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($post2) > 50){
echo("<script language='javascript'>alert(\"役職２を25文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($postcode1) > 3){
echo("<script language='javascript'>alert(\"郵便番号１を3文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(ereg("~[0-9]+$",$postcode1)){
echo("<script language='javascript'>alert(\"郵便番号１を数字で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($postcode2) > 4){
echo("<script language='javascript'>alert(\"郵便番号２を4文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(ereg("~[0-9]+$",$postcode2)){
echo("<script language='javascript'>alert(\"郵便番号２を数字で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($address1) > 50){
echo("<script language='javascript'>alert(\"住所１を25文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($address2) > 50){
echo("<script language='javascript'>alert(\"住所２を25文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($address3) > 50){
echo("<script language='javascript'>alert(\"住所３を25文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($tel1) > 6){
echo("<script language='javascript'>alert(\"電話を6文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(ereg("~[0-9]+$",$postcode3)){
echo("<script language='javascript'>alert(\"電話を数字で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($tel2) > 6){
echo("<script language='javascript'>alert(\"電話を6文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(ereg("~[0-9]+$",$tel2)){
echo("<script language='javascript'>alert(\"電話を数字で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($tel3) > 6){
echo("<script language='javascript'>alert(\"電話を6文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(ereg("~[0-9]+$",$tel3)){
echo("<script language='javascript'>alert(\"電話を数字で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($fax1) > 6){
echo("<script language='javascript'>alert(\"FAXを6文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(ereg("~[0-9]+$",$fax1)){
echo("<script language='javascript'>alert(\"FAXを数字で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($fax2) > 6){
echo("<script language='javascript'>alert(\"FAXを6文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(ereg("~[0-9]+$",$fax2)){
echo("<script language='javascript'>alert(\"FAXを数字で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($fax3) > 6){
echo("<script language='javascript'>alert(\"FAXを6文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(ereg("~[0-9]+$",$fax3)){
echo("<script language='javascript'>alert(\"FAXを数字で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($mobile1) > 3){
echo("<script language='javascript'>alert(\"携帯／PHSを3文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(ereg("~[0-9]+$",$mobile1)){
echo("<script language='javascript'>alert(\"携帯／PHSを数字で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($mobile2) > 4){
echo("<script language='javascript'>alert(\"携帯／PHSを4文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(ereg("~[0-9]+$",$mobile2)){
echo("<script language='javascript'>alert(\"携帯／PHSを数字で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($mobile3) > 4){
echo("<script language='javascript'>alert(\"携帯／PHSを4文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(ereg("~[0-9]+$",$mobile3)){
echo("<script language='javascript'>alert(\"携帯／PHSを数字で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($email_pc) > 50){
echo("<script language='javascript'>alert(\"E-MAIL（PC）を25文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(ereg("~.+@.+\\..+$",$email_pc)){
echo("<script language='javascript'>alert(\"正しいE-MAIL（PC）を入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($email_mobile) > 50){
echo("<script language='javascript'>alert(\"E-MAIL（携帯）を25文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(ereg("~.+@.+\\..+$",$email_mobile)){
echo("<script language='javascript'>alert(\"正しいE-MAIL（携帯）を入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

// emp_idが未設定の場合はログインユーザのID、設定済みはデータからのコピー
if ($emp_id == "") {
	$select_id="select emp_id from login where emp_id in (select emp_id from session where session_id='$session')";
	//echo($select_id);
	$result_select = pg_exec($con, $select_id);
	if($result_select==false){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_result($result_select,0,"emp_id");
}

$date=date(YmdHi);

$select_max = "select max(address_id) from address";
$result_max = pg_exec($con,$select_max);
$count = pg_numrows($result_max);
if($count > 0){
	$address_id = pg_result($result_max,0,"max");
	$address_id = $address_id + 1;
}else{
	$address_id = "1";
}

$insert_address = "insert into address (emp_id, address_id, date, name1, name2, name_kana1, name_kana2, company, department1, department2, post1, post2, postcode1, postcode2, prefectures, address1, address2, address3, tel1, tel2, tel3, fax1, fax2, fax3, mobile1, mobile2, mobile3, email_pc, email_mobile, memo, address_del_flg, shared_flg) values ('$emp_id', '$address_id', '$date', '$name1', '$name2', '$name_kana1', '$name_kana2', '$company', '$department1', '$department2', '$post1', '$post2', '$postcode1', '$postcode2', '$prefectures', '$address1', '$address2', '$address3', '$tel1', '$tel2', '$tel3', '$fax1', '$fax2', '$fax3', '$mobile1', '$mobile2', '$mobile3', '$email_pc', '$email_mobile', '$memo', 'f', '$shared_flg')";
//echo($insert_address);
$result_insert = pg_exec($con,$insert_address);
pg_close($con);
if($result_insert == true){
// <del>
//   echo("<script language='javascript'>window.opener.location.reload();</script>");	
//	echo("<script language='javascript'>self.close();</script>");
// </del>
// <ins>
	$url_name = urlencode($search_name);
	$url_submit = urlencode($search_submit);
	$search_shared_flg = ($shared_flg == "t") ? "3" : "2" ;
	if ($admin_flg == "t") {
		$next_php = "address_admin_menu.php";
	} else {
		$next_php = "address_menu.php";
	}
	echo("<script language='javascript'>location.href = '$next_php?session=$session&search_shared_flg=$search_shared_flg&search_name=$url_name&search_submit=$url_submit&page=';</script>");
//	echo("<script language='javascript'>alert('$next_php?session=$session&search_shared_flg=$search_shared_flg&search_name=$url_name&search_submit=$url_submit&page=$page');</script>");
// </ins>
	exit;
}else{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
?>