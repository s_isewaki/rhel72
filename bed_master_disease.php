<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理 | マスタ管理 | 病名</title>
<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 21, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

$default_url = ($section_admin_auth == "1") ? "entity_menu.php" : "building_list.php";

// データベースに接続
$con = connect2db($fname);

// 削除処理
if ($is_postback == "true") {
	if (count($data_select) == 0) {
		echo("<script type=\"text/javascript\">alert('チェックボックスをオンにしてください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
	}

	// トランザクションの開始
	pg_query($con, "begin");

	// 病名情報削除
	$sql = "delete from org_byoumei";
	$org_id_list = join(",", $data_select);
	$cond = "where org_id in ($org_id_list)";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 病名インデックス情報削除
	$sql = "delete from org_byoumei_index";
	$cond = "where org_id in ($org_id_list)";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// トランザクションのコミット
	pg_query($con, "commit");
}

// 病名一覧を取得
$sql = "select * from org_byoumei";
$cond = "order by (case when byoumei_kana <> '' then byoumei_kana else byoumei end)";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$data_list = pg_fetch_all($sel);
?>
<script type="text/javascript">
function reload_page() {
	location.href = "bed_master_disease.php?session=<? echo($session); ?>";
}

function add_byoumei() {
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=640,height=500";
	window.open('bed_master_disease_input.php?session=<? echo($session); ?>&mode=insert', 'newwin', option);
}

function update_byoumei(org_id) {
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=640,height=500";
	window.open('bed_master_disease_input.php?session=<? echo($session); ?>&mode=update&org_id=' + org_id, 'newwin', option);
}

function delete_byoumei() {
	if (confirm('選択された病名を削除します。よろしいですか？')) {
		document.mainform.submit();
	}
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.list2 {border-collapse:collapse;}
.list2 td {border:#5279a5 solid 1px;}
.list2_in1 {border-collapse:collapse;}
.list2_in1 td {border-width:0;}
.list2_in2 {border-collapse:collapse;}
.list2_in2 td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a> &gt; <a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="bed_master_menu.php?session=<? echo($session); ?>"><b>マスタ管理</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<table width="118" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279A5">
<td height="22" class="spacing" colspan="2"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">管理項目</font></b></td>
</tr>
<? if ($section_admin_auth == "1") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="entity_menu.php?session=<? echo($session); ?>">診療科</a></font></td>
</tr>
<? } ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="building_list.php?session=<? echo($session); ?>">病棟</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_display_setting.php?session=<? echo($session); ?>">表示設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>">一括登録</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_master_menu.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>
</table>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="85" align="center" bgcolor="#bdd1e7"><a href="bed_master_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マスタ作成</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#5279a5"><a href="bed_master_disease.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病名</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="105" align="center" bgcolor="#bdd1e7"><a href="bed_exception_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">計算除外理由</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_master_decubation.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回復期リハ算定</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bed_master_rehabilitation.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リハビリ算定</font></a></td>
<td width="5">&nbsp;</td>
<td width="115" align="center" bgcolor="#bdd1e7"><a href="bed_master_institution.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">医療機関・施設</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="3"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td align="right">
<input type="button" value="追加" onclick="add_byoumei();">
<input type="button" value="削除" onclick="delete_byoumei();">
</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="1"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100%" valign="top">
<form name="mainform" action="bed_master_disease.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list2_in2">
<tr>
<td bgcolor="#f6f9ff" width="30"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp</font></td>
<td bgcolor="#f6f9ff" width="*"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病名</font></td>
<td bgcolor="#f6f9ff" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ICD10</font></td>
<td bgcolor="#f6f9ff" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">変換用コード</font></td>
<td bgcolor="#f6f9ff" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">傷病名コード</font></td>
<td bgcolor="#f6f9ff" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理番号</font></td>
</tr>
<?
foreach ($data_list as $data_list1) {
	$org_id = $data_list1['org_id'];
	$byoumei = $data_list1['byoumei'];
	$icd10 = $data_list1['icd10'];
	$byoumei_code = $data_list1['byoumei_code'];
	$rece_code = $data_list1['rece_code'];
	$byoumei_id = $data_list1['byoumei_id'];
?>
<tr bgcolor="#ffffff">
<td align="center">
<input type="checkbox" name="data_select[]" value="<? echo(h($org_id)); ?>">
<input type="hidden" name="list_item_cd[]" value="<? echo(h($item_cd)); ?>">
</td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:update_byoumei('<? echo(h($org_id)); ?>')"><? echo(h($byoumei)); ?></a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(h($icd10)); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(h($byoumei_code)); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(h($rece_code)); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(h($byoumei_id)); ?></font></td>
</tr>
<?
}
?>
</table>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo(h($session)); ?>">
<input type="hidden" name="is_postback" value="true">
</form>
</td>
</tr>
</table>
</td>
<!-- right -->
</tr>
</table>
</body>
</html>
<? pg_close($con); ?>
