<?php
ob_start(); // includeしたファイルの内部でHTMLを吐かれても大丈夫なようにしておく
require_once("about_comedix.php");
ob_end_clean();

$c_sot_util = new sot_util_class;


$global_update_seq_new = 0;
$global_update_seq_old = 0;
$global_regist_timestamp = "";
$global_regist_ymd = 0;


class node_class{
  var $attr = array();
  var $value = "";
  var $nodes = array();
}

class sot_util_class {

  var $IS_SINGLE_DAY_ONLY = 1;

  function select_from_table($sql, $ignore_error=true, $con=null, $fname=null){
    if (!$fname) global $fname;
    if (!$fname) $fname = $_SERVER["PHP_SELF"];
    if (!$con) global $con;
    if (!$con) $con = connect2db($fname);
    $sel = @select_from_table($con, $sql, "", $fname);
    if($sel) return $sel;
    if($ignore_error) return null;
    pg_close($con);
    summary_common_show_error_page_and_die();
  }

  function escape_for_textarea($str){
    return str_replace("\n", "<br/>", h($str));
  }

  function escape_for_javascript($str){
      return str_replace("\r", "", str_replace("\n", "\\n", str_replace('"', '\\"', str_replace("'","\\'", str_replace("\\","\\\\", $str)))));
  }

  function get_age($birth_ymd, $current_ymd = "") {
      if ($current_ymd) $current_ymd = date("Ymd");
      $month = substr($birth_ymd, 4, 4);
      $age = substr($current_ymd, 0, 4) - substr($birth_ymd, 0, 4);
      if (substr($current_ymd, 4, 4) < $month) $age = $age - 1;
      return $age;
  }

  function slash_yyyymmdd($yyyymmdd){
      $yyyymmdd = trim(@$yyyymmdd);
      if (strlen($yyyymmdd) != 8) return "";
    return substr($yyyymmdd, 0, 4)."/".substr($yyyymmdd, 4, 2)."/".substr($yyyymmdd, 6, 2);
  }

  function slash_ymd($yyyymmdd){
    $yyyymmdd = trim(@$yyyymmdd);
    if (strlen($yyyymmdd) != 8) return "";
    return substr($yyyymmdd, 0, 4)."/".(int)substr($yyyymmdd, 4, 2)."/".(int)substr($yyyymmdd, 6, 2);
  }

  function kanji_ymd($yyyymmdd, $is_wide_type = false){
    $yyyymmdd = trim(@$yyyymmdd);
    if (strlen($yyyymmdd) != 8) return "";
    if ($is_wide_type) return substr($yyyymmdd, 0, 4)." 年 ".(int)substr($yyyymmdd, 4, 2)." 月 ".(int)substr($yyyymmdd, 6, 2)." 日";
    return substr($yyyymmdd, 0, 4)."年".(int)substr($yyyymmdd, 4, 2)."月".(int)substr($yyyymmdd, 6, 2)."日";
  }

  function slash_ymd_from_ymd($y, $m, $d){
    if (!(int)$y) return "";
    if (!(int)$m) return "";
    if (!(int)$d) return "";
    return (int)$y."/".(int)$m."/".(int)$d;
  }

  var $emp_all_list = 0;
  function get_emp_name_using_cache($emp_id){
    if ($emp_id=="") return "";
    global $emp_all_list;
    if (!is_array($emp_all_list)) {
      $emp_all_list = array();
      $sel = $this->select_from_table("select emp_id, emp_lt_nm, emp_ft_nm from empmst");
      while ($row = pg_fetch_array($sel)){
        $emp_all_list[$row["emp_id"]] = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];
      }
    }
    return @$emp_all_list[$emp_id];
  }

  function get_emp_name($emp_id){
    $sel = $this->select_from_table("select emp_lt_nm, emp_ft_nm from empmst where emp_id ='".pg_escape_string($emp_id)."'");
    $emp = pg_fetch_array($sel);
    return trim($emp["emp_lt_nm"] . " " . $emp["emp_ft_nm"]);
  }

  // 指定年月日時点での入院状態を取得する。（2009頃より）
  // 入院には同日複数入院（現実的には同日の再入院）があり得る。このため入院は時刻データがある。
  // とある年月日時点の入院状態を取得するには、時刻を考える必要がある。以下となっている。
  // ・本日の入院情報を取得したい場合は、本日現在時刻時点の入院情報を返す。
  // ・過去の入院情報を取得したい場合は、指定年月日のゼロ時時点の入院情報を返す。
  // ・未来の入院情報を取得したい場合は、指定年月日の23:59時点の入院情報を返す。
  // ゼロ時時点を取得するのか23:59時点を取得するのかは、上記の逆が正しいような気もするが、
  // 局面によってはこれでよい。よく考えると、逆にすると不都合がある場合があることに気づくだろう。
  // 例えば1月1日に、Ａ患者は12時入院開始、Ｂ患者は12時退院、であったとする。
  // 1月1日時点の入院者はＡＢ両方であるにもかかわらず、ゼロ時基準ならＡは未入院であり、23:59基準ならＢは退院済である。両方を同時に取得できない。
  // そこで、2014/12 アバウトモード($is_about_mode)の追加。$is_about_modeでは、$is_ignore_betweenモードは無視する。
  function get_nyuin_info($con, $fname, $ptif_id, $yyyymmdd, $is_ignore_between = false, $is_about_mode = 0){
    $ret = array();

    // 2014/12 アバウトモードの追加
    // 従来までは、同一日入退院をはじき入退院を確実に一意にするため、
    // 指定された年月日のうち、とある所定計算方法での「時刻」時点の入退院を取得していたが、
    // これでは不都合がある場合がある。
    // こと過去情報を確認する場合（これがほとんどだが）、
    // 時刻はゼロ時の時点の情報を取得するため、以下はヒットしない。
    // 例）過去2010年1月1日1時1分に入院場合 ⇒ 画面より1月1日時点の入院状態を知りたい場合
    //     ⇒従来の検索では1月1日ゼロ時ゼロ分時点の入院情報を検索するため、ヒットしない
    //
    // このため、$is_about_modeを指定された場合には、
    // 同日何時であろうと最適と思われる入院情報を持ってくることにする。
    if ($is_about_mode) {
        // 入院の確認。指定日以内で入院が開始されていること。inptmstは１患者につき１レコードなので、ここではゼロor１レコード取得される。
        $sql =
          " select inpt.*, bldg.bldg_name, ward.ward_name, ptrm.ptrm_name from inptmst inpt".
          " left outer join bldgmst bldg on (bldg.bldg_cd = inpt.bldg_cd)".
          " left outer join wdmst ward on (ward.bldg_cd = inpt.bldg_cd and ward.ward_cd = inpt.ward_cd)".
          " left outer join ptrmmst ptrm on (ptrm.bldg_cd = inpt.bldg_cd and ptrm.ward_cd = inpt.ward_cd and ptrm.ptrm_room_no = inpt.ptrm_room_no)".
          " where inpt.ptif_id = '".pg_escape_string($ptif_id)."'".
          " and inpt.inpt_in_dt <= '" . (int)$yyyymmdd . "'";
        $sel = select_from_table($con, $sql, "", $fname);
        $ret = pg_fetch_array($sel);
        // 入院でなければ入院履歴の確認。指定日以内に入院し、指定日またはそれ以降に退院していること。
        // 退院descにつき、以下のように複数存在した場合は�△鮗萋世垢�
        // １）引数で$yyyymmddが1ヶ月前の年月日を指定されたとする。仮に今日が11/11で、画面から10/10を指定されたとする。
        // ２）以下の２つの入院データが存在したとする
        //     �＿甬遒�ら10/10の18時まで入院
        //     ��10/10の20時から再度入院
        // ３）例えば現在時刻が12時であったとする
        // ４）従来ならゼロ時時点の入院情報を返すが、�△�フェッチされることになる
        // ５）例えば本日12時退院、現在15時、で、20時から再入院となった場合は、20時再入院分が取得されることに注意。常に最終的入院状態を取得するということ。
        if (!is_array($ret)){
          $sql =
            " select inpt.*, bldg.bldg_name, ward.ward_name, ptrm.ptrm_name from inpthist inpt".
            " left outer join bldgmst bldg on (bldg.bldg_cd = inpt.bldg_cd)".
            " left outer join wdmst ward on (ward.bldg_cd = inpt.bldg_cd and ward.ward_cd = inpt.ward_cd)".
            " left outer join ptrmmst ptrm on (ptrm.bldg_cd = inpt.bldg_cd and ptrm.ward_cd = inpt.ward_cd and ptrm.ptrm_room_no = inpt.ptrm_room_no)".
            " where inpt.ptif_id = '".pg_escape_string($ptif_id)."'".
            " and inpt.inpt_in_dt <= '" . (int)$yyyymmdd . "'".
            " and inpt.inpt_out_dt >= '" . (int)$yyyymmdd . "'".
            " order by inpt_out_dt desc, inpt_out_tm desc".
            " limit 1";
          $sel = select_from_table($con, $sql, "", $fname);
          $ret = pg_fetch_array($sel);
        }
        return (array)$ret;
    }

    //----------------------------------
    // ここから従来の処理
    //----------------------------------

    $t = "0000"; // （未来なら）0時0分
    if (date("Ymd") == $yyyymmdd) $t = date("Hi"); // 本日なら現在時刻
    if (date("Ymd")  < $yyyymmdd) $t = "2359"; // 過去なら23時59分

    // 入院の確認。指定日より過去に入院が開始されていること。
    $sql =
      " select inpt.*, bldg.bldg_name, ward.ward_name, ptrm.ptrm_name from inptmst inpt".
      " left outer join bldgmst bldg on (bldg.bldg_cd = inpt.bldg_cd)".
      " left outer join wdmst ward on (ward.bldg_cd = inpt.bldg_cd and ward.ward_cd = inpt.ward_cd)".
      " left outer join ptrmmst ptrm on (ptrm.bldg_cd = inpt.bldg_cd and ptrm.ward_cd = inpt.ward_cd and ptrm.ptrm_room_no = inpt.ptrm_room_no)".
      " where inpt.ptif_id = '".pg_escape_string($ptif_id)."'".
      " and (inpt.inpt_in_dt < '" . (int)$yyyymmdd . "' or (inpt.inpt_in_dt = '" . (int)$yyyymmdd. "' and inpt.inpt_in_tm <= '".$t."'))";
    $sel = select_from_table($con, $sql, "", $fname);
    $ret = pg_fetch_array($sel);
    // 入院でなければ入院履歴の確認。指定日以前に入院し、指定日以降に退院していること。
    if (!is_array($ret)){
      $sql =
        " select inpt.*, bldg.bldg_name, ward.ward_name, ptrm.ptrm_name from inpthist inpt".
        " left outer join bldgmst bldg on (bldg.bldg_cd = inpt.bldg_cd)".
        " left outer join wdmst ward on (ward.bldg_cd = inpt.bldg_cd and ward.ward_cd = inpt.ward_cd)".
        " left outer join ptrmmst ptrm on (ptrm.bldg_cd = inpt.bldg_cd and ptrm.ward_cd = inpt.ward_cd and ptrm.ptrm_room_no = inpt.ptrm_room_no)".
        " where inpt.ptif_id = '".pg_escape_string($ptif_id)."'".
        " and (inpt.inpt_in_dt < '" . (int)$yyyymmdd . "' or (inpt.inpt_in_dt = '" . (int)$yyyymmdd. "' and inpt.inpt_in_tm <= '".$t."'))".
        " and (inpt.inpt_out_dt > '" . (int)$yyyymmdd . "' or (inpt.inpt_out_dt = '" . (int)$yyyymmdd. "' and inpt.inpt_out_tm >= '".$t."'))".
        " order by inpt_out_dt desc, inpt_out_tm desc".
        " limit 1";
      $sel = select_from_table($con, $sql, "", $fname);
      $ret = pg_fetch_array($sel);
    }
    // 指定日を含む入院履歴がなければ、退院以降入院していないか確認。指定日の直近の退院データがあるかどうか確認。
    // ただし、$is_ignore_betweenが指定された場合は、直近は取得しない。
    if (!$is_ignore_between){
      if (!is_array($ret)){
        $sql =
          " select inpt.*, bldg.bldg_name, ward.ward_name, ptrm.ptrm_name from inpthist inpt".
          " left outer join bldgmst bldg on (bldg.bldg_cd = inpt.bldg_cd)".
          " left outer join wdmst ward on (ward.bldg_cd = inpt.bldg_cd and ward.ward_cd = inpt.ward_cd)".
          " left outer join ptrmmst ptrm on (ptrm.bldg_cd = inpt.bldg_cd and ptrm.ward_cd = inpt.ward_cd and ptrm.ptrm_room_no = inpt.ptrm_room_no)".
          " where inpt.ptif_id = '".pg_escape_string($ptif_id)."'".
          " and (inpt.inpt_in_dt < '" . (int)$yyyymmdd . "' or (inpt.inpt_in_dt = '" . (int)$yyyymmdd. "' and inpt.inpt_in_tm <= '".$t."'))".
          " and (inpt.inpt_out_dt < '" . (int)$yyyymmdd . "' or (inpt.inpt_out_dt = '" . (int)$yyyymmdd. "' and inpt.inpt_out_tm <= '".$t."'))".
          " order by inpt_out_dt desc, inpt_out_tm desc".
          " limit 1";
        $sel = select_from_table($con, $sql, "", $fname);
        $ret = pg_fetch_array($sel);
      }
    }
    return (array)$ret;
  }

  function get_nyuin_info_with_prev_next($con, $fname, $ptif_id, $yyyymmdd){
    $sql =
      " select inpt.*, bldg.bldg_name, ward.ward_name, ptrm.ptrm_name from inptmst inpt".
      " left outer join bldgmst bldg on (bldg.bldg_cd = inpt.bldg_cd)".
      " left outer join wdmst ward on (ward.bldg_cd = inpt.bldg_cd and ward.ward_cd = inpt.ward_cd)".
      " left outer join ptrmmst ptrm on (ptrm.bldg_cd = inpt.bldg_cd and ptrm.ward_cd = inpt.ward_cd and ptrm.ptrm_room_no = inpt.ptrm_room_no)".
      " where inpt.inpt_in_dt <= '" . (int)$yyyymmdd . "'" .
      " and inpt.inpt_in_dt is not null and inpt.inpt_in_dt <> ''".
      " order by inpt.bldg_cd, inpt.ward_cd, ptrm.ptrm_name, inpt.ptrm_room_no, inpt.inpt_bed_no";
    $sel = select_from_table($con, $sql, "", $fname);
    $all = pg_fetch_all($sel);
    $rec_cnt = count($all);
    $prev = array();
    $next = array();
    $curr = array();
    for ($i = 0; $i < $rec_cnt; $i++){
      if ($all[$i]["ptif_id"] == $ptif_id){
        $curr = $all[$i];
        if ($i>0)          $prev = $all[$i-1];
        if ($i<$rec_cnt-1) $next = $all[$i+1];
        break;
      }
    }
    return array("prev"=>$prev, "next"=>$next, "curr"=>$curr);
  }


  // 指定した日が入院かそうでないか（外来か）を判断し、オーダ履歴が有効な期間を求める
  // ◆対象日に入院している場合
  //   検索開始日：入院日
  //   検索終了日：退院日、退院日がなければ退院予定日、それもなければ無限に未来まで
  // ◆対象日に入院していない場合
  //   検索開始日：最後の退院日、退院日がなければ退院予定日、それもなければシステム開始から
  //   検索終了日：次の入院日、入院がなければ無限に未来まで
  function getBetweenNyuinGairai($con, $fname, $ptif_id, $yyyymmdd, &$start_ymd, &$end_ymd){
    $nyuin_info = $this->get_nyuin_info($con, $fname, $ptif_id, $yyyymmdd, true);
    $start_ymd = (int)@$nyuin_info["inpt_in_dt"];
    $end_ymd = (int)@$nyuin_info["inpt_out_dt"];
    if (!$end_ymd) $end_ymd = 99999999;
    if ($start_ymd) return; // 入院か過去入院履歴があれば終了

    $t = "0000"; // （未来なら）0時0分
    if (date("Ymd") == $yyyymmdd) $t = date("Hi"); // 本日なら現在時刻
    if (date("Ymd")  < $yyyymmdd) $t = "2359"; // 過去なら23時59分

    // 指定日には退院中であるか、外来の場合、開始日は指定日の直前の退院日
    $sql =
      " select max(inpt_out_dt) from inpthist".
      " where (inpt_out_dt < '".(int)$yyyymmdd. "' or (inpt_out_dt = '".(int)$yyyymmdd. "' and inpt_out_tm < '".$t."'))".
      " and ptif_id = '".pg_escape_string($ptif_id)."'";
    $sel = $this->select_from_table($sql);
    $start_ymd = (int)@pg_fetch_result($sel, 0, 0);

    // 同じく指定日には退院中であるか、外来の場合、終了日は指定日の直後の入院日
    $sql =
      " select min(inpt_in_dt) from inptmst".
      " where (inpt_in_dt > '".(int)$yyyymmdd. "' or (inpt_in_dt = '".(int)$yyyymmdd. "' and inpt_in_tm > '".$t."'))".
      " and ptif_id = '".pg_escape_string($ptif_id)."'";
    $sel = $this->select_from_table($sql);
    $end_ymd = (int)@pg_fetch_result($sel, 0, 0);
    if (!$end_ymd) {
      $sql =
        " select min(inpt_in_dt) from inpthist".
        " where (inpt_in_dt > '".(int)$yyyymmdd. "' or (inpt_in_dt = '".(int)$yyyymmdd. "' and inpt_in_tm > '".$t."'))".
        " and ptif_id = '".pg_escape_string($ptif_id)."'";
      $sel = $this->select_from_table($sql);
      $end_ymd = (int)@pg_fetch_result($sel, 0, 0);
    }
    if (!$end_ymd) $end_ymd = 99999999;
  }

  function generate_dropdown_times_per_ymd_options($def = "xxx"){
    $out = array();
    $out[] = '<option value=""></option>';
    for ($i = 0; $i <= 9; $i++) $out[] = '<option value="'.$i.'"'.($def==$i&&strlen($def)>0 ? " selected" : "").'>'.$i.'</option>';
    return implode("\n", $out);
  }

  function generate_dropdown_options_y($def = 0){
    $def = (int)$def;
    $y = date("Y")-2;
    $out = '<option value=""></option>'."\n";
    $out .= '<option value="'.$y.'"'.($def==$y ? " selected" : "").'>'.$y."</option>\n";
    $y++;
    $out .= '<option value="'.$y.'"'.($def==$y ? " selected" : "").'>'.$y."</option>\n";
    $y++;
    $out .= '<option value="'.$y.'"'.($def==$y ? " selected" : "").'>'.$y."</option>\n";
    $y++;
    $out .= '<option value="'.$y.'"'.($def==$y ? " selected" : "").'>'.$y."</option>\n";
    return $out;
  }
  function generate_dropdown_options_m($def = 0){
    $def = (int)$def;
    $out = '<option value=""></option>'."\n";
    for ($i=1; $i<=12; $i++) $out .= '<option value="'.$i.'"'.($def==$i ? " selected" : "").'>'.$i."</option>\n";
    return $out;
  }
  function generate_dropdown_options_d($def = 0){
    $def = (int)$def;
    $out = '<option value=""></option>'."\n";
    for ($i=1; $i<=31; $i++) $out .= '<option value="'.$i.'"'.($def==$i ? " selected" : "").'>'.$i."</option>\n";
    return $out;
  }

  function generate_dropdown_options_hour($def = 0){
    $def = (int)$def;
    $out = '<option value=""></option>'."\n";
    for ($i=0; $i<=23; $i++) $out .= '<option value="'.$i.'"'.($def==$i ? " selected" : "").'>'.$i."</option>\n";
    return $out;
  }
  function generate_dropdown_options_minute($def = 0){
    $def = (int)$def;
    $out = '<option value=""></option>'."\n";
    for ($i=0; $i<=59; $i++) $out .= '<option value="'.$i.'"'.($def==$i ? " selected" : "").'>'.$i."</option>\n";
    return $out;
  }

  function javascript($javascript_command){
    return "<script type=\"text/javascript\">" . $javascript_command . "</script>\n";
  }

  function url_get_template_contents($param){
    global $session;
    $subdir = substr($_SERVER["PHP_SELF"], 0, strrpos($_SERVER["PHP_SELF"], "/")+1);
    $url = "http://".$_SERVER["HTTP_HOST"].$subdir."sot_template_wrapper.php?session=".$session."&";
    return file_get_contents($url.$param);
  }

  // PHP5以上でしか使えないので注意
  function url_get_template_contents_multi($params) {
    global $session;
    $subdir = substr($_SERVER["PHP_SELF"], 0, strrpos($_SERVER["PHP_SELF"], "/")+1);
    $url = "http://".$_SERVER["HTTP_HOST"].$subdir."sot_template_wrapper.php?session=".$session."&";

    $mh = curl_multi_init();

    foreach ($params as $i => $param) {
      $ch[$i] = curl_init($url . $param);

      curl_setopt($ch[$i], CURLOPT_HEADER, false);
      curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch[$i], CURLOPT_FAILONERROR, true);

      // SSL証明書を無視
      curl_setopt($ch[$i], CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch[$i], CURLOPT_SSL_VERIFYHOST, false);

      curl_multi_add_handle($mh, $ch[$i]);
    }

    $active = null;
    do {
      $mrc = curl_multi_exec($mh, $active);
    } while ($mrc == CURLM_CALL_MULTI_PERFORM);

    while ($active && $mrc == CURLM_OK) {
      if (curl_multi_select($mh) != -1) {
        do {
          $mrc = curl_multi_exec($mh, $active);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);
      }
    }

    $htmls = array();
    foreach ($params as $i => $param) {
      $htmls[$param] = curl_multi_getcontent($ch[$i]);
      curl_multi_remove_handle($mh, $ch[$i]);
      curl_close($ch[$i]);
    }
    curl_multi_close($mh);
    return $htmls;
  }

  function url_get_template_contents_with_post(){
    $sock = fsockopen($_SERVER["HTTP_HOST"], 80, $errno, $errstr, 20);
    if(!$sock) return "";
    $adata = array();
    foreach($_REQUEST as $key => $val){
      if (is_array($val)){
        foreach ($val as $v) $adata[] = urlencode($key)."[]=".urlencode($v);
      } else {
        $adata[] = urlencode($key)."=".urlencode($val);
      }
    }
    $sdata = implode("&", $adata);
    $subdir = substr($_SERVER["PHP_SELF"], 0, strrpos($_SERVER["PHP_SELF"], "/")+1);
    $url = "http://".$_SERVER["HTTP_HOST"].$subdir."sot_template_wrapper.php";
    fwrite($sock, "POST " . $url . " HTTP/1.0\r\n");
    fwrite($sock, "User-Agent: PHP\r\n");
    fwrite($sock, "Referer: ".$_SERVER["PHP_SELF"]."\r\n");
    fwrite($sock, "Content-Type: application/x-www-form-urlencoded\r\n");
    fwrite($sock, "Content-Length: " . strlen($sdata) . "\r\n");
    fwrite($sock, "Connection: close\r\n");
    fwrite($sock, "\r\n");
    fwrite($sock, $sdata);
    fwrite($sock, "\r\n");
    $response = array();
    while (!feof($sock)) if (trim(fgets($sock, 128)) == "") break;
    while (!feof($sock)) $response[] = fgets($sock, 4096);
    fclose($sock);
    return trim(implode("", $response));
  }

  function regist_summary_seq_if_not_exists($summary_id, $ptif_id, $con, $fname){
    if (!$summary_id) return "";
    if (!$ptif_id) return "";
    $summary_id = (int)$summary_id;
    $ptif_id = pg_escape_string($ptif_id);
    $sel = $this->select_from_table("select summary_seq from summary where summary_id = ".$summary_id." and ptif_id = '".$ptif_id."'");
    $summary_seq = @pg_fetch_result($sel, 0, "summary_seq");
    if ($summary_seq) return $summary_seq;

    // 旧：サマリ連番の採番
    // $sel = $this->select_from_table("select max(summary_seq) as max_summary_seq from summary");
    // $summary_seq = (int)(@pg_fetch_result($sel, 0, "max_summary_seq")) + 1;

    // 新：サマリ連番の採番方法変更 (採番用テーブルにて管理)
    require_once("summary_seqcnt.ini");
    $summary_seq = get_summary_seq($con, $fname);
    if($summary_seq == 0) {
      pg_query($con, "rollback");
      pg_close($con);
      summary_common_show_error_page_and_die();
    }

    $sql = "update summary set summary_seq = ".$summary_seq." where summary_id = ".$summary_id." and ptif_id = '".$ptif_id."'";
    update_set_table($con, $sql, array(), null, "", $fname);
    return $summary_seq;
  }

  function regist_summary_schedule_various_to_db_temporary($schedule_seq, $tmpval, $con, $fname){
    $insval = array();
    $insval["schedule_seq"] = (int)$schedule_seq;
    for ($i = 1; $i <= 20; $i++){
      $str = trim(@$tmpval["string".$i]);
      $insval["string".$i] = ($str != "" ? "'".pg_escape_string($str)."'" : "''");
    }
    for ($i = 1; $i <= 10; $i++){
      $str = trim(@$tmpval["int".$i]);
      $insval["int".$i] = ($str != "" ? (int)$str : "null");
    }
    $insval["memo"] = "'".pg_escape_string($tmpval["memo"])."'";
    $sql = "insert into sot_summary_schedule_various (".implode(",", array_keys($insval)).") values (".implode(",", $insval);
    $ins = insert_into_table($con, $sql, array(), $fname);
  }

  function regist_summary_schedule_to_db_temporary($insval, $con, $fname, $is_single_day_only=0){
    $today_ymd = date("Ymd");
    global $global_update_seq_new;
    global $global_update_seq_old;
    global $global_regist_timestamp;
    global $global_regist_ymd;

    foreach($insval as $key => $val) $insval[$key] = trim($val);

    $assign_pattern = $insval["assign_pattern"];

    $insval["xml_file_name"]  = "'".pg_escape_string(@$_REQUEST["xml_file"])."'";
    $insval["tmpl_file_name"] = "'".pg_escape_string(@$_REQUEST["xml_creator"])."'";
    $insval["emp_id"]         = "'".pg_escape_string(@$insval["emp_id"]!="" ? @$insval["emp_id"] : @$_REQUEST["emp_id"])."'";
    $insval["ptif_id"]        = "'".pg_escape_string(@$_REQUEST["pt_id"])."'";
    $insval["wkfw_id"]        = @$_REQUEST["wkfw_id"]!="" ? (int)@$_REQUEST["wkfw_id"] : "null";
    $insval["summary_id"]     = @$_REQUEST["summary_id"]!="" ? (int)@$_REQUEST["summary_id"] : "null";
    $insval["summary_seq"]    = @$_REQUEST["summary_seq"]!="" ? (int)@$_REQUEST["summary_seq"] : "null";
    $insval["tmpl_id"]        = @$_REQUEST["tmpl_id"]!="" ? (int)@$_REQUEST["tmpl_id"] : "null";
    $insval["tmpl_chapter_code"]= "'".$insval["tmpl_chapter_code"]."'";
    $insval["assign_pattern"] = "'".$insval["assign_pattern"]."'";
    $insval["is_hide_worksheet_date"] = (int)@$insval["is_hide_worksheet_date"];
    $insval["is_hide_worksheet_apply"] = (int)@$insval["is_hide_worksheet_apply"];

    // 正式登録していないのに何度も新規作成されたときのゴミがあったら消す。最初の一回だけ実行する。
    if (!$global_update_seq_new){
      $sql =
        " select schedule_seq from sot_summary_schedule".
        " where temporary_update_flg = 1 and xml_file_name = " . $insval["xml_file_name"];
      $sel = $this->select_from_table($sql);
      while($row = pg_fetch_row($sel)){
        delete_from_table($con, "delete from sot_summary_schedule where schedule_seq = ".(int)$row[0], "", $fname);
        delete_from_table($con, "delete from sot_summary_schedule_various where schedule_seq = ".(int)$row[0], "", $fname);
      }

      // この一連の更新処理のキーを作成
      $sql = "select nextval('sot_summary_schedule_update_seq_seq')";
      $sel = $this->select_from_table($sql);
      $global_update_seq_new = (int)pg_fetch_result($sel, 0, 0);
    }
    $insval["update_seq"] = $global_update_seq_new;

    // 現在有効なデータがあれば、そのキーを取得する。
    // 作成日付けも引き継ぐ。なければ現在日時。
    if (!$global_update_seq_old){
      $sql =
        " select update_seq, regist_timestamp, regist_ymd from sot_summary_schedule".
        " where xml_file_name = " . $insval["xml_file_name"].
        " and temporary_update_flg = 0".
        " and delete_flg = false limit 1";
      $sel = $this->select_from_table($sql);
      $global_update_seq_old = (int)@pg_result($sel,0,"update_seq");
      $global_regist_timestamp = @pg_result($sel,0,"regist_timestamp");
      $global_regist_ymd = (int)@pg_result($sel,0,"regist_ymd");
      if ($global_regist_timestamp!= "") $global_regist_timestamp = "'".$global_regist_timestamp."'";
    }
    if ($global_regist_timestamp =="") $global_regist_timestamp = "current_timestamp";
    if (!$global_regist_ymd) $global_regist_ymd = $today_ymd;
    $insval["regist_timestamp"] = $global_regist_timestamp;
    $insval["regist_ymd"] = $global_regist_ymd;

    // データの日付けを補足する。更新前データがあれば、更新前データからからもってくる。
    if ($global_update_seq_old){
      $sql =
        " select start_ymd, end_ymd from sot_summary_schedule".
        " where update_seq = ". $global_update_seq_old.
        " and assign_pattern = '".$assign_pattern."'".
        " and tmpl_chapter_code = " . $insval["tmpl_chapter_code"];
      $sel = $this->select_from_table($sql);
      if (!(int)@$insval["start_ymd"]) $insval["start_ymd"] = (int)@pg_result($sel,0,"start_ymd");
      if (!(int)@$insval["end_ymd"]) $insval["end_ymd"] = (int)@pg_result($sel,0,"end_ymd");
    }
    if (!(int)@$insval["start_ymd"]) $insval["start_ymd"] = $today_ymd;
    if (!(int)@$insval["end_ymd"] || (int)@$insval["start_ymd"] > (int)@$insval["end_ymd"]) {
      $insval["end_ymd"] = ($is_single_day_only ? $insval["start_ymd"] : "99999999");
    }

    // レコードIDを発行
    $sel = $this->select_from_table("select nextval('sot_summary_schedule_seq')");
    $insval["schedule_seq"] = pg_fetch_result($sel, 0, 0);

    // 仮登録する。
    $sql = "insert into sot_summary_schedule (".implode(",", array_keys($insval)).") values (".implode(",", $insval);
    $ins = insert_into_table($con, $sql, array(), $fname);

    // 発行したIDを返す
    return $insval["schedule_seq"];
  }


  //******************************************************************************
  // 処方注射オーダテンプレートのRP情報ダンプ(ordsc_basedata_streamとordsc_rpdata_stream)が、
  // REQUESTで渡されてくる。このダンプ情報を連想配列にバラして返す。
  // バラしたデータはDB登録などに利用する。
  // この関数は以下から呼ばれる。
  // ・sum_ordsc_ajax_for_template.phpで、セットRPを登録するとき
  // ・tmpl_Ordsc_XXXXXXX.phpで、処方や注射RPデータを登録するとき
  //******************************************************************************
  function convert_ordsc_rp_dump_from_request_to_array($need_convert_charset_for_ajax = true){

    function mergeParamSet(&$l, $s){
      $p = strpos($s, "=");
      if ($p===FALSE) return array(""=>"");
      $l[substr($s, 0, $p)] = substr($s, $p+1);
    }

    $out = array();
    $out["base"] = array();
    $out["rp"]   = array();

    $dmp_base_stream = urldecode(urldecode(@$_REQUEST["ordsc_basedata_stream"]));
    if ($need_convert_charset_for_ajax) $dmp_base_stream = mb_convert_encoding($dmp_base_stream, "EUC-JP", "UTF-8");
    $a_dmp_base = explode("\t", $dmp_base_stream);
    foreach($a_dmp_base as $idx => $row) mergeParamSet($out["base"], $row);

    $dmp_rp_stream = urldecode(urldecode(@$_REQUEST["ordsc_rpdata_stream"]));
    if ($need_convert_charset_for_ajax) $dmp_rp_stream = mb_convert_encoding($dmp_rp_stream, "EUC-JP", "UTF-8");
    $a_dmp_rp = explode("\t*\t", $dmp_rp_stream);
    foreach($a_dmp_rp as $idx => $rprow) {
      $a_dmp_rprow = explode("\t@\t", $rprow);
      $out["rp"][$idx] = array();
      foreach($a_dmp_rprow as $idx2 => $row) mergeParamSet($out["rp"][$idx], $row);
    }
    return $out;
  }
    //******************************************************************************
    // 処方注射オーダの登録（仮登録のタイミング)
    //******************************************************************************
    function regist_ordsc_from_request($con, $fname, $req_base, $req_rp, $schedule_seq, $summary_seq, $set_seq){

        $sql =
            " insert into sum_order_syoho_cyusya (".
            " schedule_seq".
            ",summary_seq".
            ",set_seq".
            ",is_syoho_cyusya".
            ",is_rinji_teiki".
            ",is_nyuin_gairai".
            ",is_innai_ingai".
            ",set_target".
            ",set_name".
            ",order_ymd".
            ",byoumei".
            ",byoumei_code".
            ",icd10".
            ",pt_id".
            ",enti_id".
            ",sect_id".
            ",force_end_ymd".
            ",is_uketuke_zumi".
            ",is_kansa_zumi".
            ",is_cyozai_zumi".
            ",is_touyo_zumi".
            ",regist_emp_id".
            " ) values (".
            " " .(int)$schedule_seq. // schedule_seq
            "," .(int)$summary_seq. // summary_seq
            "," .(int)$set_seq. // set_seq
            ",'".pg_escape_string($req_base["is_syoho_cyusya"])."'" . // is_syoho_cyusya
            ",'".pg_escape_string($req_base["is_rinji_teiki"])."'" . // is_rinji_teiki
            ",'"."N"."'" . // is_nyuin_gairai
            ",'"."N"."'" . // is_innai_ingai
            ",'".pg_escape_string($req_base["set_target"])."'" . // set_target
            ",'".pg_escape_string($req_base["set_name"])."'" . // set_name
            ",'". pg_escape_string($req_base["order_ymd"]) ."'" . // order_ymd
            ",'". pg_escape_string($req_base["byoumei"]) . "'" . // byoumei
            ",'". pg_escape_string($req_base["byoumei_code"]) . "'" . // byoumei_code
            ",'". pg_escape_string($req_base["icd10"]) . "'" . // icd10
            ",'".pg_escape_string($req_base["pt_id"])."'" . // pt_id
            ",0" . // enti_id
            ",0" . // sect_id
            ",'" . pg_escape_string($req_base["force_end_ymd"]) ."'". // force_end_ymd
            ",''" . // is_uketuke_zumi
            ",''" . // is_kansa_zumi
            ",''" . // is_cyozai_zumi
            ",''" . // is_touyo_zumi
            ",'".pg_escape_string($req_base["regist_emp_id"])."'" . // regist_emp_id
            " )";
        $ret = update_set_table($con, $sql, array(), null, "", $fname);

        foreach ($req_rp as $idx => $row){
            $sql =
                " insert into sum_order_syoho_cyusya_rp (" .
                " schedule_seq" .
                ",summary_seq" .
                ",set_seq" .
                ",rp_seq" .
                ",rp_line_seq" .
                ",rp_line_div" .
                ",med_seq_cd" .
                ",hot13_cd" .
                ",med_name" .
                ",med_yoryo" .
                ",med_unit_name" .
                ",yoho_kubun" .
                ",yoho_touyo_start_ymd" .
                ",yoho_touyo_end_ymd" .
                ",yoho_touyo_nichi_bun" .
                ",yoho_touyo_nichi_oki" .
                ",yoho_comment" .
                ",yoho_naiyo_per_day" .
                ",yoho_cyusyasiji_cd" .
                ",yoho_cyusyasokudo" .
                ",yoho_cyusyasokudo_unit" .
                ",yoho_cyusyatiming" .
                ",yoho_naiyo_timing" .
                ",yoho_naiyo_timing_fukintou" .
                ",yoho_naiyo_meal_timing" .
                ",yoho_naiyo_meal_timing_fukintou" .
                ",yoho_naiyo_hour_dist" .
                ",yoho_naiyo_hours" .
                ",yoho_naiyo_hours_fukintou" .
                ",yoho_naiyo_fukintou_type" .
                ",yoho_naiyo_chozai_siji" .
                ",yoho_gaiyo_touyo_kaisu_cd" .
                ",yoho_gaiyo_siyou_houhou_cd" .
                ",yoho_gaiyo_bui_cd" .
                ",yoho_gaiyo_bui_lrb" .
                ",yoho_tonpu_kaisu" .
                ",yoho_tonpu_timing1_cd" .
                ",yoho_tonpu_timing2_cd" .
                ",yoho_tonpu_timing2_text" .
                ",yoho_tonpu_ondo" .
                ",yoho_display_string" .
                ",status" .
                ",yoho_cyusyaroute_cd" .
                ",yoho_naiyo_timing_ishi_shiji" .
                ",yoryo_type" .
                ",yoho_touyo_teiji_tunagi" .
                ",med_chozai_siji" .
                ",med_kouhatu_fuka" .
                ",yoho_content" .
                " ) values (".
                " " .(int)$schedule_seq . // schedule_seq
                "," .(int)$summary_seq . // summary_seq
                "," .(int)$set_seq . // set_seq
                ",".(int)$row["rp_seq"] . // rp_seq
                ",".(int)$row["rp_line_seq"] . // rp_line_seq
                ",'".pg_escape_string($row["rp_line_div"])."'" . // rp_line_div
                ",".(int)$row["med_seq_cd"] . // med_seq_cd
                ",'".pg_escape_string($row["hot13_cd"])."'" . // hot13_cd
                ",'".pg_escape_string($row["med_name"])."'" . // med_name
                ",".(double)pg_escape_string($row["med_yoryo"]). // med_yoryo
                ",'".pg_escape_string($row["med_unit_name"])."'" . // med_unit_name
                ",'".pg_escape_string($row["yoho_kubun"])."'" . // yoho_kubun
                ",'".pg_escape_string($row["yoho_touyo_start_ymd"])."'" . // yoho_touyo_start_ymd
                ",'".pg_escape_string($row["yoho_touyo_end_ymd"])."'" . // yoho_touyo_end_ymd
                "," .(int)$row["yoho_touyo_nichi_bun"] . // yoho_touyo_nichi_bun
                "," .(int)$row["yoho_touyo_nichi_oki"] . // yoho_touyo_nichi_oki
                ",'".pg_escape_string($row["yoho_comment"])."'" . // yoho_comment
                "," .(int)$row["yoho_naiyo_per_day"] . // yoho_naiyo_per_day
                "," .(int)$row["yoho_cyusyasiji_cd"] . // yoho_cyusyasiji_cd
                "," .(int)$row["yoho_cyusyasokudo"] . // yoho_cyusyasokudo
                ",'".pg_escape_string($row["yoho_cyusyasokudo_unit"]) ."'" . // yoho_cyusyasokudo_unit
                ",'".pg_escape_string($row["yoho_cyusyatiming"]) ."'" . // yoho_cyusyatiming
                ",'".pg_escape_string($row["yoho_naiyo_timing"])."'" . // yoho_naiyo_timing
                ",'".pg_escape_string($row["yoho_naiyo_timing_fukintou"])."'" . // yoho_naiyo_hours_fukintou
                ",'".pg_escape_string($row["yoho_naiyo_meal_timing"])."'" . // yoho_naiyo_meal_timing
                ",'".pg_escape_string($row["yoho_naiyo_meal_timing_fukintou"])."'" . // yoho_naiyo_hours_fukintou
                ",".(int)$row["yoho_naiyo_hour_dist"] . // yoho_naiyo_hour_dist
                ",'".pg_escape_string($row["yoho_naiyo_hours"])."'" . // yoho_naiyo_hours
                ",'".pg_escape_string($row["yoho_naiyo_hours_fukintou"])."'" . // yoho_naiyo_hours_fukintou
                ",'".pg_escape_string($row["yoho_naiyo_fukintou_type"])."'" . // yoho_naiyo_fukintou_type
                ",'".pg_escape_string($row["yoho_naiyo_chozai_siji"])."'" . // yoho_naiyo_chozai_siji
                "," .(int)$row["yoho_gaiyo_touyo_kaisu_cd"] . // yoho_gaiyo_touyo_kaisu_cd
                "," .(int)$row["yoho_gaiyo_siyou_houhou_cd"] . // yoho_gaiyo_siyou_houhou_cd
                "," .(int)$row["yoho_gaiyo_bui_cd"] . // yoho_gaiyo_bui_cd
                ",'".pg_escape_string($row["yoho_gaiyo_bui_lrb"])."'" . // yoho_gaiyo_bui_lrb
                "," .(int)$row["yoho_tonpu_kaisu"] . // yoho_tonpu_kaisu
                "," .(int)$row["yoho_tonpu_timing1_cd"] . // yoho_tonpu_timing1_cd
                "," .(int)$row["yoho_tonpu_timing2_cd"] . // yoho_tonpu_timing2_cd
                ",'".pg_escape_string($row["yoho_tonpu_timing2_text"])."'" . // yoho_tonpu_timing2_text
                ",'".pg_escape_string($row["yoho_tonpu_ondo"])."'" . // yoho_tonpu_ondo
                ",'".pg_escape_string($row["yoho_display_string"])."'" . // yoho_display_string
                ", ".(int)$row["status"]." " . // 更新状況
                ", ".(int)$row["yoho_cyusyaroute_cd"]." " . // ルート
                ", ".(int)$row["yoho_naiyo_timing_ishi_shiji"]." " . // 医師指示
                ",'".pg_escape_string($row["yoryo_type"])."'" . // 用法（1日／1回）
                ", ".(int)$row["yoho_touyo_teiji_tunagi"] . " " . // 定時つなぎ
                ",'".pg_escape_string($row["med_chozai_siji"]) . "'" . // 薬剤毎の調剤指示
                ", ".(int)$row["med_kouhatu_fuka"] . " " . // 後発品変更不可
                ",'".pg_escape_string($row["yoho_content"]) . "'" . // 用法内容
                " )";
            $ret = update_set_table($con, $sql, array(), null, "", $fname);
            $sql =
                " update sum_med_saiyou_mst set".
                " remark = '".pg_escape_string($row["remark"])."'".
                " where seq = " . (int)$row["med_seq_cd"];
            $ret = update_set_table($con, $sql, array(), null, "", $fname);

            if ($row["med_unit_name"]){
                $newary = array();
                $newary[] = $row["med_unit_name"];
                $sql =
                    " select units_separated_tab from sum_med_saiyou_mst set".
                    " where seq = " . (int)$row["med_seq_cd"];
                $sel = $this->select_from_table($sql);
                $units_separated_tab = @pg_result($sel,0,0);
                $ary = explode(",", $units_separated_tab);
                for ($i = 0; $i < count($ary); $i++){
                    if ($ary[$i]=="") continue;
                    if ($ary[$i]== $row["med_unit_name"]) continue;
                    $newary[] = trim($ary[$i]);
                }
                $sql =
                    " update sum_med_saiyou_mst set".
                    " units_separated_tab = '".pg_escape_string(implode($newary,","))."'".
                    " where seq = " . (int)$row["med_seq_cd"];
                $ret = update_set_table($con, $sql, array(), null, "", $fname);
            }

            $ret = update_set_table($con, $sql, array(), null, "", $fname);
        }
    }

    /**
     * 配列に対して、指定したパターンにマッチする要素数を取得する
     *
     * @param string $pattern 正規表現によるパターン
     * @param array  $data    要素数を取得する配列
     *
     * @return integer  要素数
     */
    function countPatternMatchElement($pattern, $data)
    {
        $count = 0;
        foreach ($data as $value) {
            if (preg_match($pattern, $value)) {
                $count++;
            }
        }
        return $count;
    }

    /**
     * 病室情報をDBから選択し、配列として返す
     *
     * @return array
     */
    function getPatientsRoomInfo() {
        $sel = $this->select_from_table("select bldg_cd, ward_cd, ptrm_room_no, ptrm_name from ptrmmst where ptrm_del_flg = 'f' order by ptrm_name, ptrm_room_no");
        $mst = (array)pg_fetch_all($sel);
        return $mst;
    }

    /**
     * ブラウザ種別を取得する
     *
     * @return string ブラウザ種別
     */
    function getBrowserKind()
    {
        $kind = '';
        $browsers = array('MSIE', 'Firefox', 'Chrome', 'Safari', 'Opera');
        $agent = $_SERVER['HTTP_USER_AGENT'];

        foreach ($browsers as $value) {
            // ユーザーエージェントに含まれているブラウザ名をブラウザ種別とする
            if (strpos($agent, $value) !== false) {
                $kind = $value;
                break;
            }
        }

        return $kind;
    }

    /**
     * yyyymmdd形式の生年月日を和暦に変換する
     *
     * @param string $birth yyyymmdd形式の生年月日
     *
     * @return string 和暦に変換した生年月日
     */
    function birth_wareki($birth)
    {
        $y = substr($birth,0,4);
        $m = substr($birth,4,2);
        $d = substr($birth,6,2);
        $md = (string)$m . (string)$d;
        $str_ymd = "年" . (int)$m . "月" . (int)$d . "日";

        if($y <= 1912) {
            if ($y == 1912 && $md > 730) {
                $wareki = '大正' . ($y - 1911) . $str_ymd;
            } else {
                $wareki = '明治' . ($y - 1867) . $str_ymd;
            }
        } elseif ($y <= 1926) {
            if ($y == 1926 && $md > 1225) {
                $wareki = '昭和' . ($y - 1925) . $str_ymd;
            } else {
                $wareki = '大正' . ($y - 1911) . $str_ymd;
            }
        } elseif ($y <= 1989) {
            if ($y == 1989 && $md > 107) {
                $wareki = '平成' . ($y - 1988) . $str_ymd;
            } else {
                $wareki = '昭和' . ($y - 1925) . $str_ymd;
            }
        } else {
            $wareki = '平成' . ($y - 1988) . $str_ymd;
        }
        return $wareki;
    }

    /**
     * 目標グループマスタデータを取得する
     *
     * @param resource $con          データベース接続リソース
     * @param string   $therapy_type 療法種別
     * @param string   $group_code   グループコード
     * @param string   $data_type    データ種別('G':リハビリ目標 'P'治療プログラム)
     * @param string   $fname        ページ名
     *
     * @return mixed 成功時:目標グループマスタデータ/失敗時:false
     */
    function getGoalGroupMst($con, $therapy_type, $group_code, $data_type, $fname)
    {
        $table_name = '';
        if ($data_type == 'G') {
            $table_name = 'sum_med_rehabili_goal_mst';
        } elseif ($data_type == 'P') {
            $table_name = 'sum_med_cure_program_mst';
        }
        $sql  =  "select sum_med_goal_group_mst.*, $table_name.caption, $table_name.sort_order";
        $sql .= " from sum_med_goal_group_mst";
        $sql .= " left join $table_name";
        $sql .= " on sum_med_goal_group_mst.therapy_type = $table_name.therapy_type";
        $sql .= " and sum_med_goal_group_mst.code = $table_name.code";
        $sql .= " where sum_med_goal_group_mst.therapy_type='$therapy_type'";
        if ($group_code) {
            $sql .= " and sum_med_goal_group_mst.group_code='$group_code'";
        }
        $sql .= " and sum_med_goal_group_mst.data_type='$data_type'";
        $sql .= " and $table_name.is_disabled = 0";
        $sql .= " order by $table_name.code";
        $sel = select_from_table($con, $sql, "", $fname);

        if ($sel == 0) {
            return false;
        }

        $data = array();
        if ($group_code) {    // グループコードを指定してデータを取得
            while ($row = pg_fetch_array($sel)) {
                $data[$row['code']] = $row['caption'];
            }
        } else {    // 全グループのデータを取得
            while ($row = pg_fetch_array($sel)) {
                if (array_key_exists($row['group_code'], $data)) {
                    $data[$row['group_code']][] = $row['caption'];
                } else {
                    $data[$row['group_code']] = array($row['caption']);
                }
            }
        }
        return $data;
    }
}

/**
 * 処方箋ユーティリティクラス
 */
class ordsc_util_class
{
    /**
     * 改行数をカウントする(処方箋PDF出力時に利用)
     *
     * @param string  $str    改行数カウント対象
     * @param integer $maxlen 1行の最大文字数
     *
     * @return integer 改行数
     */
    function countBreak($str, $maxlen)
    {
        $str = trim($str);
        $split = preg_split("/\n/", $str);
        $break_cnt = 0;
        foreach ($split as $value) {
            $len = strlen($value);
            if ($len > $maxlen) {
                // 1行の文字列が長い場合は、改行数を追加
                $break_cnt += floor($len / $maxlen);
            }
            $break_cnt++;
        }
        return $break_cnt;
    }

    /**
     * 処方箋機能コントロールマスタテーブルからデータを取得する
     *
     * @param resource $con   データベース接続リソース
     * @param string   $fname ページ名
     *
     * @return mixed 成功時:取得したデータ/失敗時:false
     */
    function getSumMedSyohouMst($con = null, $fname = null)
    {
        if (!$fname) {
            $fname = $_SERVER["PHP_SELF"];
        }

        if (!$con) {
            if (!($con = connect2db($fname))) {
                return false;
            }
        }

        // 処方箋機能コントロールマスタテーブルに登録されているレコード数を取得する
        $count = $this->countSumMedSyohouMst($con, $fname);
        if ($count === false) {
            return false;
        }

        if ($count == 0) {
            return $this->initSumMedSyohouMstData();
        }

        $sql = "SELECT * FROM sum_med_syohou_mst";
        $result = select_from_table($con, $sql, "", $fname);
        if ($result === 0) {
            return false;
        }
        $data = pg_fetch_assoc($result);

        return $data;
    }

    /**
     * 処方箋機能コントロールマスタ初期化データを作成する
     *
     * @return array 初期化データ
     */
    function initSumMedSyohouMstData()
    {
        $data = array(
                "search_all_medicine"        => "1",
                "default_tab"                => "P",
                "last_do_target"             => "K",
                "send_info_button"           => "1",
                "print_label_button"         => "1",
                "print_sub_button"           => "1",
                "print_direction_button"     => "1",
                "print_button"               => "1",
                "dosing_timing"              => "1",
                "meal_timing_before"         => "1",
                "meal_timing_just_before"    => "1",
                "meal_timing_just_after"     => "1",
                "meal_timing_after"          => "1",
                "meal_timing_during"         => "1",
                "meal_timing_between"        => "1",
                "hour_distance"              => "1",
                "doctor_direction"           => "1",
                "time_assign_num"            => "4",
                "dosing_period_word"         => "投与期間",
                "fixed_time_continuance"     => "0",
                "enable_dispensing_medicine" => "0",
                "dispensing_medicine_crush"  => "0",
                "dispensing_medicine_off"    => "0",
                "dispensing_tablet"          => "1",
                "dispensing_suspension"      => "1",
                "dispensing_crush"           => "1",
                "dispensing_package"         => "1",
                "dispensing_off"             => "0",
                "input_dispensing"           => "0",
                "print_rp_line"              => "0",
                "print_dosing_period"        => "0",
                "print_injected_record"      => "1",
                "input_injection_content"    => "0",
                "print_dialog_auto"          => "0"
        );

        return $data;
    }

    /**
     * 処方箋機能コントロールマスタテーブルのレコード数を取得する
     *
     * @param resource $con   データベース接続リソース
     * @param string   $fname ページ名
     *
     * @return mixed 成功時:レコード数/失敗時:false
     */
    function countSumMedSyohouMst($con, $fname)
    {
        $sql = "SELECT COUNT(*) FROM sum_med_syohou_mst";
        $result = select_from_table($con, $sql, "", $fname);
        if ($result === 0) {
            return false;
        }
        $count = pg_fetch_result($result, 0, 0);
        return $count;
    }

    /**
     * 処方歴を取得する
     *
     * @param resource $con            データベース接続リソース
     * @param string   $fname          ページ名
     * @param array    $tmpl_param     テンプレートパラメータ
     * @param string   $last_do_target 前回DOの表示対象("K"：同じ種類の履歴を表示　"P"：同じ処方箋の履歴のみ表示)
     * @param string   $tmpl_file_name テンプレートファイル名
     * @param array    $res            処方歴データ
     * @param array    $sh_list        表示用に編集した処方歴リスト
     * @param array    $naiyoDisp      処方歴内容
     *
     * @return boolean 成功時:true/失敗時:false
     */
    function getPrescriptionHistory($con, $fname, $tmpl_param, $last_do_target, $tmpl_file_name, &$res, &$sh_list, &$naiyoDisp)
    {
        $sql = "select summary_id, cre_date, summary.tmpl_id, summary_seq, tmplmst.tmpl_name from summary inner join tmplmst on summary.tmpl_id = tmplmst.tmpl_id";
        if ($last_do_target === "P") {    // 同じ処方箋の履歴
            $cond = "where ptif_id = '{$tmpl_param['pt_id']}' and tmplmst.tmpl_file = '$tmpl_file_name' order by cre_date desc limit 10";
        } else {                          // 同じ種類の履歴
            if (($tmpl_file_name === "tmpl_OrdSC_RinjiCyusya.php") || ($tmpl_file_name === "tmpl_OrdSC_TeijiCyusya.php")) {
                $str = "注射";
            } else {
                $str = "処方";
            }
            $cond = "where ptif_id = '{$tmpl_param['pt_id']}' and tmplmst.tmpl_name like '%{$str}%' order by cre_date desc limit 10";
        }

        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel === 0) {
            return false;
        }

        $res     = pg_fetch_all($sel);
        $res_cnt = count($res);
        $sh_list = array();

        if ($res) {
            // 処方歴の内容を保持
            $naiyoDisp = array();

            for ($i = 0; $i < $res_cnt; ++$i) {
                $sh_list[$i] = substr($res[$i]['cre_date'], 0, 4) . '/' . substr($res[$i]['cre_date'], 4, 2) . '/' . substr($res[$i]['cre_date'], 6, 2);

                // 処方歴内容の取得
                $naiyoDisp[$i] = h(get_summary_naiyo_html_from_sum_xml($con,$fname,$res[$i]['summary_id'],$tmpl_param['pt_id'],$tmpl_param['login_emp_id'],$res[$i]['summary_seq']));
            }
        }

        return true;
    }

    /**
     * 処方箋のボタン作成
     *
     * @param array  $syohou_mst   処方箋機能コントロールマスタデータ
     * @param string $nyuin_gairai 入院/外来
     *
     * @return void
     */
    function createPrescriptionButtons($syohou_mst, $nyuin_gairai = "N", $moreOption = array())
    {
        if ($syohou_mst["send_info_button"] === "1") {    // 薬情送信ボタン
            echo "<input type=\"button\" value=\"薬情送信\" onclick=\"sendCooperationData()\" />\n";
        }

        if ($syohou_mst["print_label_button"] === "1") {    // 印刷（ラベル）ボタン
            echo "<input type=\"button\" value=\"印刷（ラベル）\" onclick=\"printPDF3()\" />\n";
        }

        if (($nyuin_gairai === "N") && ($syohou_mst["print_direction_button"] === "1")) {    // 印刷（指示書） ボタン
            echo "<input type=\"button\" value=\"印刷（指示書）\" onclick=\"printPDF2()\" />\n";
        }
        if (in_array("print_direction_touyaku_button", $moreOption) && ($syohou_mst["print_direction_touyaku_button"] === "1")) {    // 印刷（投薬指示書） ボタン
            echo "<input type=\"button\" value=\"印刷（投薬指示書）\" onclick=\"printPDF4()\" />\n";
        }
        if (in_array("print_direction_cyusya_button", $moreOption) && ($syohou_mst["print_direction_cyusya_button"] === "1")) {    // 印刷（注射指示書） ボタン
            echo "<input type=\"button\" value=\"印刷（注射指示書）\" onclick=\"printPDF4()\" />\n";
        }
        if (($nyuin_gairai === "N") && ($syohou_mst["print_sub_button"] === "1")) {    // 印刷（控え）ボタン
            echo "<input type=\"button\" value=\"印刷（控え）\" onclick=\"printPDF(true)\" />\n";
        }

        if ($syohou_mst["print_button"] === "1") {    // 印刷ボタン
            echo "<input type=\"button\" value=\"印刷\" onclick=\"printPDF()\" />\n";
        }
    }

    /**
     * 項目の表示／非表示切替の為に、class="display_none"の記述を追加する
     * (呼出元で、必ずsyohou.cssをインクルードする。)
     *
     * @param string $value      設定値
     * @param string $hide_value 非表示にする場合の値
     *
     * @return void
     */
    function addDisplayNone($value, $hide_value)
    {
        if ($value === $hide_value) {
            echo " class=\"display_none\"";
        }
    }

    /**
     * 時刻指定枠作成
     *
     * @param integer $num 時刻指定枠数
     *
     * @return void
     */
    function createTimeAssignForm($num)
    {
        for ($i = 1; $i <= 6; $i++) {
            $display_none = "";
            $nowrap = "";

            if ($i > $num) {    // 指定枠数より大きければ非表示
                $display_none = " class=\"display_none\"";
            }

            if ($i == 5) {    // 途中改行させない
                $nowrap = " style=\"white-space: nowrap;\"";
            }

            echo "<span{$nowrap}{$display_none}>\n";
            echo "<input type=\"text\" id=\"yoho_naiyo_hours_$i\" style=\"width:24px; ime-mode:disabled; border:1px solid #aaa\" autocomplete=\"off\">&nbsp;時\n";
            echo "<input type=\"text\" id=\"yoho_naiyo_hours_fukintou_$i\" style=\"width:28px; ime-mode:disabled; border:1px solid #aaa; margin-right:10px\" autocomplete=\"off\"/>\n";
            echo "</span>\n";
        }
    }

    /**
     * テンプレート情報をセットする
     *
     * @param string $tmpl_file_name    テンプレートファイル名
     * @param string $is_syoho_cyusya   処方箋/注射箋
     * @param string $is_rinji_teiki    臨時/定時
     * @param string $template_title_jp テンプレートファイル和名
     * @param string $nyuin_gairai      入院/外来
     *
     * @return void
     */
    function setTemplateInfo($tmpl_file_name, &$is_syoho_cyusya, &$is_rinji_teiki, &$template_title_jp, &$nyuin_gairai)
    {
        $is_syoho_cyusya = ''; $is_rinji_teiki = ''; $nyuin_gairai = '';

        if ($tmpl_file_name == 'tmpl_OrdSC_NyuinRinji')  {
            $is_syoho_cyusya = 'S'; $is_rinji_teiki = 'R'; $template_title_jp = '入院臨時処方';   $nyuin_gairai = 'N';
        }
        if ($tmpl_file_name == 'tmpl_OrdSC_NyuinTeiki')  {
            $is_syoho_cyusya = 'S'; $is_rinji_teiki = 'T'; $template_title_jp = '入院定時処方';   $nyuin_gairai = 'N';
        }
        if ($tmpl_file_name == 'tmpl_OrdSC_RinjiCyusya') {
            $is_syoho_cyusya = 'C'; $is_rinji_teiki = 'R'; $template_title_jp = '臨時注射箋';     $nyuin_gairai = 'N';
        }
        if ($tmpl_file_name == 'tmpl_OrdSC_TeijiCyusya') {
            $is_syoho_cyusya = 'C'; $is_rinji_teiki = 'T'; $template_title_jp = '定時注射箋';     $nyuin_gairai = 'N';
        }
        if ($tmpl_file_name == 'tmpl_OrdSC_InnaiGairai') {
            $is_syoho_cyusya = 'S'; $is_rinji_teiki = 'R'; $template_title_jp = '院内外来処方箋'; $nyuin_gairai = 'G';
        }
        if ($tmpl_file_name == 'tmpl_OrdSC_IngaiGairai') {
            $is_syoho_cyusya = 'S'; $is_rinji_teiki = 'R'; $template_title_jp = '院外外来処方箋'; $nyuin_gairai = 'G';
        }
        if ($tmpl_file_name == 'tmpl_OrdSC_Taiinji')     {
            $is_syoho_cyusya = 'S'; $is_rinji_teiki = 'T'; $template_title_jp = '退院時処方';   $nyuin_gairai = 'N';
        }
    }

    /**
     * 行を分ける場合の投薬期間文字列を作成する
     *
     * @param array  $yoho_arr                用法データ
     * @param string $toyaku_start_ymd        投薬開始日
     * @param string $toyaku_days             投薬日数
     * @param string $input_injection_content 注射箋用法内容入力有無('0':なし '1':あり)
     * @param string $yoho_kubun              用法区分('N':内用 'G':外用 'Y':外用頓服 'P':頓服 'C':注射)
     *
     * @return string 投薬期間文字列
     */
    function createToyakuPeriodStr(&$yoho_arr, $toyaku_start_ymd, $toyaku_days, $input_injection_content = '0', $yoho_kubun = '')
    {
        // 投与開始日をいったん削除
        $pattern = '/(\d+)年(\d+)月(\d+)日〜|(\d+)年(\d+)月(\d+)日/';
        $yoho_arr = preg_replace($pattern, '', $yoho_arr);
        $start = mktime(0, 0, 0, substr($toyaku_start_ymd, 4, 2), substr($toyaku_start_ymd, 6, 2), substr($toyaku_start_ymd, 0, 4));
        $toyaku_period = date('Y年n月j日', $start);

        if ($input_injection_content !== '1' || $yoho_kubun !== 'C') {
            $toyaku_period .= '〜';
            if ($toyaku_days > 0) {
                $end = mktime(0, 0, 0, substr($toyaku_start_ymd, 4, 2), substr($toyaku_start_ymd, 6, 2) + $toyaku_days - 1, substr($toyaku_start_ymd, 0, 4));
                $toyaku_period .= date('Y年n月j日', $end);
            }
        }

        return $toyaku_period;
    }

    /**
     * コメント入力サポート子画面を生成
     *
     * @return void
     */
    function createCommentSupportWindow($target_id)
    {
        echo <<< _HTML_
<div id="div_yoho_comment_yougo" style="position:absolute; left:300px; top:200px; width:460px; display:none">
  <div style="color:#fff; margin-top:4px; background-color:#e04307; border-top:2px solid #e04307">
    <div style="text-align:left; padding:3px;">内容</div>
    <div style="background-color:#ffffe6; border:2px solid #e04307; border-top:0; padding:4px; color:#000; font-size:14px">
      <table>
        <tr>
          <td style="width:260px; vertical-align:top;">
            <div id="div_med_list"></div>
          </td>
          <td style="width:25px; vertical-align:top; padding-left:6px;">
            <div class="btn_cyusyatiming2" onclick="btnSupportWindowClicked(this, 'yoho_comment_result')" onmouseover="hovIn(this)" onmouseout="hovOut(this)" style="width:22px;">＋</div>
            <div class="btn_cyusyatiming2" onclick="btnSupportWindowClicked(this, 'yoho_comment_result')" onmouseover="hovIn(this)" onmouseout="hovOut(this)" style="width:22px;">＝</div>
          </td>
          <td style="width:84px; vertical-align:top; padding-left:6px">
            <div>
              <div class="btn_cyusyatiming2" onclick="btnSupportWindowClicked(this, 'yoho_comment_result')" onmouseover="hovIn(this)" onmouseout="hovOut(this)" style="width:22px">7</div>
              <div class="btn_cyusyatiming2" onclick="btnSupportWindowClicked(this, 'yoho_comment_result')" onmouseover="hovIn(this)" onmouseout="hovOut(this)" style="width:22px">8</div>
              <div class="btn_cyusyatiming2" onclick="btnSupportWindowClicked(this, 'yoho_comment_result')" onmouseover="hovIn(this)" onmouseout="hovOut(this)" style="width:22px">9</div>
            </div>
            <div>
              <div class="btn_cyusyatiming2" onclick="btnSupportWindowClicked(this, 'yoho_comment_result')" onmouseover="hovIn(this)" onmouseout="hovOut(this)" style="width:22px">4</div>
              <div class="btn_cyusyatiming2" onclick="btnSupportWindowClicked(this, 'yoho_comment_result')" onmouseover="hovIn(this)" onmouseout="hovOut(this)" style="width:22px">5</div>
              <div class="btn_cyusyatiming2" onclick="btnSupportWindowClicked(this, 'yoho_comment_result')" onmouseover="hovIn(this)" onmouseout="hovOut(this)" style="width:22px">6</div>
            </div>
            <div>
              <div class="btn_cyusyatiming2" onclick="btnSupportWindowClicked(this, 'yoho_comment_result')" onmouseover="hovIn(this)" onmouseout="hovOut(this)" style="width:22px">1</div>
              <div class="btn_cyusyatiming2" onclick="btnSupportWindowClicked(this, 'yoho_comment_result')" onmouseover="hovIn(this)" onmouseout="hovOut(this)" style="width:22px">2</div>
              <div class="btn_cyusyatiming2" onclick="btnSupportWindowClicked(this, 'yoho_comment_result')" onmouseover="hovIn(this)" onmouseout="hovOut(this)" style="width:22px">3</div>
            </div>
            <div>
              <div class="btn_cyusyatiming2" onclick="btnSupportWindowClicked(this, 'yoho_comment_result')" onmouseover="hovIn(this)" onmouseout="hovOut(this)" style="width:22px">0</div>
            </div>
          </td>
          <td style="width:46px; vertical-align:top; padding-left:6px;">
            <div class="btn_cyusyatiming1" onclick="btnSupportWindowClicked(this, 'yoho_comment_result')" onmouseover="hovIn(this)" onmouseout="hovOut(this)">ml</div>
            <div class="btn_cyusyatiming1" onclick="btnSupportWindowClicked(this, 'yoho_comment_result')" onmouseover="hovIn(this)" onmouseout="hovOut(this)">mg</div>
          </td>
        </tr>
      </table>
      <div style="margin-top:12px">
        <input type="text" id="yoho_comment_result" style="width:370px; border:1px solid #aaa" autocomplete="off">
        <input type="button" value="クリア" onclick="elems['yoho_comment_result'].value='';" />
      </div>
      <div style="margin-top:4px; text-align:center">
        <input type="button" value="適用" onclick="applyYohoCommentString('$target_id')" />
        <input type="button" value="閉じる" onclick="cancelYohoCommentString('$target_id')" />
      </div>
    </div>
  </div>
</div>
_HTML_;
    }

    /**
     * 回数入力欄生成
     *
     * @return void
     */
    function createDivYohoNaiyoPerDay()
    {
        $html = <<< _HTML_
        <div id="div_yoho_naiyo_per_day" style="padding:2px; padding-left:4px; border:1px solid #aaa; background-color:#ffffcc; margin-top:3px">
          １日
          <select id="yoho_naiyo_per_day" name="yoho_naiyo_per_day" style="width:50px">
            <option value=""></option>\n
_HTML_;
    for ($i = 1; $i <= 10; $i++) {
        $html .= "<option value=\"$i\">$i</option>\n";
    }
    $html .= <<< _HTML_
          </select>
          回
        </div>\n
_HTML_;

        echo $html;
    }

    /**
     * 投与法入力欄生成
     *
     * @param array $yoho_cyusyasiji_mst_list 注射指示マスタリスト
     *
     * @return void
     */
    function createDivYohoCyusyasiji($yoho_cyusyasiji_mst_list)
    {
        $html = "";
        $html .= "<div id=\"div_yoho_cyusyasiji\" style=\"float: left; padding: 1px;\">\n";
        $html .= "投与法\n"
              . "<select id=\"yoho_cyusyasiji_cd\">\n"
              . "<option value=\"\"></option>\n";
        foreach ($yoho_cyusyasiji_mst_list as $idx => $row) {
            $html .= "<option value=\"{$row["code"]}\">" . h($row['caption']) . "</option>\n";
        }
        $html .= "</select>\n";
        $html .= "</div>\n";

        echo $html;
    }

    /**
     * ルート入力欄生成
     *
     * @param array $yoho_cyusyaroute_mst_list 注射ルートマスタリスト
     *
     * @return void
     */
    function createDivYohoCyusyaroute($yoho_cyusyaroute_mst_list)
    {
        $html = "";
        $html .= "<div id=\"div_yoho_cyusyaroute\" style=\"float: left; padding: 1px;\">\n";
        $html .= "ルート\n"
              . "<select id=\"yoho_cyusyaroute_cd\">\n"
              . "<option value=\"\"></option>\n";
        foreach ($yoho_cyusyaroute_mst_list as $idx => $row) {
            $html .= "<option value=\"{$row['code']}\">" . h($row['caption']). "</option>\n";
        }
        $html .= "</select>\n";
        $html .= "</div>\n";
        echo $html;
    }

    /**
     * 速度入力欄生成
     *
     * @return void
     */
    function createDivYohoCyusyasokudo()
    {
        $html = <<< _HTML_
        <div id="div_yoho_cyusyasokudo" style="float: left; padding: 1px;">
          速度
          <input type="text" id="yoho_cyusyasokudo" style="width:50px; ime-mode:disabled; border:1px solid #aaa" autocomplete="off" />
          <select id="yoho_cyusyasokudo_unit">
            <option value=""></option>
            <option value="ml/時" >ml/時</option>
            <option value="滴/分" >滴/分</option>
          </select>
        </div>\n
_HTML_;

        echo $html;
    }

    /**
     * 注射指示HTMlを生成
     *
     * @param string $input_injection_content   注射箋用法内容入力有無("0":なし "1":あり)
     * @param array  $yoho_cyusyaroute_mst_list 注射ルートマスタリスト
     * @param array  $yoho_cyusyasiji_mst_list  注射指示マスタリスト
     *
     * @return void
     */
    function createYohoCyusyaSettingHTML($input_injection_content, $yoho_cyusyaroute_mst_list, $yoho_cyusyasiji_mst_list)
    {
        if ($input_injection_content) {    // 用法内容入力あり
            echo "<div id=\"div_yoho_cyusya_setting\" style=\"padding:2px; padding-left:4px; border:1px solid #aaa; background-color:#ffffcc; margin-top:3px; height: 20px; overflow: hidden;\">\n";
            $this->createDivYohoCyusyaroute($yoho_cyusyaroute_mst_list);
            echo "<div style=\"width: 10px; float: left;\">&nbsp</div>\n";
            $this->createDivYohoCyusyasokudo();    // 速度
            echo "<div style=\"width: 10px; float: left;\">&nbsp</div>\n";
            $this->createDivYohoCyusyasiji($yoho_cyusyasiji_mst_list);    // 投与法
            echo "</div>\n";
            $this->createDivYohoNaiyoPerDay();     // 回数
        } else {    // 用法内容入力なし
            $this->createDivYohoNaiyoPerDay();     // 回数
            echo "<div id=\"div_yoho_cyusya_setting\" style=\"padding:2px; padding-left:4px; border:1px solid #aaa; background-color:#ffffcc; margin-top:3px; height: 20px;\">\n";
            $this->createDivYohoCyusyaroute($yoho_cyusyaroute_mst_list);
            echo "<div style=\"width: 10px; float: left;\">&nbsp</div>\n";
            $this->createDivYohoCyusyasiji($yoho_cyusyasiji_mst_list);    // 投与法
            echo "</div>\n";
            echo "<div id=\"div_yoho_cyusyasokudo\" style=\"padding:2px; padding-left:4px; border:1px solid #aaa; background-color:#ffffcc; margin-top:3px; height: 20px;\">\n";
            $this->createDivYohoCyusyasokudo();    // 速度
            echo "</div>\n";
        }
    }

    /**
     * 注射箋用法内容HTMlを生成
     *
     * @param string $input_injection_content 注射箋用法内容入力有無("0":なし "1":あり)
     *
     * @return void
     */
    function createYohoContentHTML($input_injection_content)
    {
        echo "<div id=\"div_yoho_content\" style=\"padding:2px; padding-left:4px; border:1px solid #aaa; background-color:#ffffcc; margin-top:3px\"";
        $this->addDisplayNone($input_injection_content, '0');
        echo ">\n";
        echo "<div style=\"float: left;\">内容</div>\n";
        echo "<div style=\"float: left;\">\n";
        echo "<textarea id=\"yoho_content\" cols=\"60\" rows=\"1\" style=\"height:30px; border:1px solid #aaa; ime-mode:active;\"></textarea>\n";
        echo "</div>\n";
        echo "<div>\n";
        echo "<a href=\"\" onclick=\"showYohoCommentYougoDiv('yoho_content'); return false\"><img src=\"img/icon/s05.gif\" style=\"border:0\"></a>\n";
        echo "</div>\n";
        echo "<br style=\"clear:both\" />\n";
        echo "</div>\n";
    }
}

//------------------------------------------------------------------------------
// テンプレート用XML構造保持、およびユーティリティクラス
//------------------------------------------------------------------------------
class template_xml_class {
  var $doc;   // クラス変数 xmldomオブジェクト
  var $root;  // クラス変数 xmldomオブジェクトのルートノード

  // DBに格納されているXML文字列を抽出して、「node_class」クラス配列を作成して返却する。
  function get_xml_object_array_from_db($con, $fname, $xml_file_name){
    if ($xml_file_name == "") return "";
    $xml_text = $this->get_utf8_xml_text_from_db($con, $fname, $xml_file_name);
    if (!$xml_text) return "";
    return $this->xml_text_to_object_ary($xml_text);
  }


  // DBにEUCで格納されているXML文字列を抽出して、UTF8に変換して返す
  function get_utf8_xml_text_from_db($con, $fname, $xml_file_name){
    global $c_sot_util;
    $sel = $c_sot_util->select_from_table("select smry_xml from sum_xml where xml_file_name = '".$xml_file_name."' limit 1", 0);
    $xml_text = @pg_fetch_result($sel, 0, "smry_xml");
    if ($xml_text == "") return "";
//    return mb_convert_encoding($xml_text, "UTF-8", "EUC-JP");
    return mb_convert_encoding(mb_convert_encoding($xml_text,"sjis-win","eucJP-win"),"UTF-8","sjis-win");
  }

  // XML文字列から、PHP4のXMLDOMを作成するためのエイリアス。失敗時はfalseを返す
  function get_xmldom_from_utf8_xml_text($utf8_xml_text){
    return @domxml_open_mem($utf8_xml_text);
  }


  // XML文字列から、「node_class」クラス配列を作成する。
  function xml_text_to_object_ary(&$string) {
    $parser = xml_parser_create("UTF-8");
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parse_into_struct($parser, $string, $list, $index);
    xml_parser_free($parser);
    $obj = new node_class();
    $idx = 0;
    $this->node2ary($obj, $list, $idx);
    return $obj;
  }
  // 上記xml_text_to_object_ary()関数から始まり、再帰コールされる解析用関数
  function node2ary(&$parent, &$list, &$idx){
    while ($idx < count($list)){
      $node = new node_class();
      $data = $list[$idx];
      $idx++;
      if (isset($data["attributes"])) {
        foreach($data["attributes"] as $key => $val){
//          $node->attr[mb_convert_encoding($key, "EUC-JP", "UTF-8")] = mb_convert_encoding($val, "EUC-JP", "UTF-8");
          $node->attr[mb_convert_encoding($key, "eucJP-win", "UTF-8")] = mb_convert_encoding($val, "eucJP-win", "UTF-8");
        }
      }
      if ($data["type"] == "open") {
        $this->node2ary($node, $list, $idx);
        $parent->nodes[$data["tag"]] = $node;
      } elseif ($data["type"] == "complete") {
//        $node->value = (@$data["value"] != null ? mb_convert_encoding(@$data["value"], "EUC-JP", "UTF-8") : "");
        $node->value = (@$data["value"] != null ? mb_convert_encoding(@$data["value"], "eucJP-win", "UTF-8") : "");
        $parent->nodes[$data["tag"]] = $node;
      } elseif ($data["type"] == "close") return;
    }
  }

  // PHP4のXMLDOMを、UTF8でダンプ(タグ文字列に変換)する
  function dump_mem_utf8(){
    return $this->doc->dump_mem(true, "UTF-8");
  }

  // PHP4のXMLDOMをクリエイトし、トップノードを[template]で作成する。
  function create_root_template_element(){
    $this->doc = domxml_new_doc("1.0");
    $this->root = $this->append_element($this->doc, "template");
    return $this->root;
  }

  // 指定した$parentに、$element_nameという名前のカラの子要素を追加する。
  function append_element(&$parent, $element_name){
    $elem = $this->doc->create_element($element_name);
    $parent->append_child($elem);
    return $elem;
  }

  // 指定した$parentに、$keyという名前の子要素を追加する。
  // 本システムで利用されている[main][disp]および[hidden]属性と、テキストフィールドもあわせて設定する。
  // 内部でappend_element()を呼んでいる。
  function add_node(&$parent, $is_hidden, $key, $disp="", $val=""){
//    $key  = mb_convert_encoding($key,  "UTF-8", "EUC-JP");
//    $disp = mb_convert_encoding($disp, "UTF-8", "EUC-JP");
//    $val  = mb_convert_encoding($val,  "UTF-8", "EUC-JP");
    $key  = mb_convert_encoding(mb_convert_encoding($key, "sjis-win","eucJP-win"),"UTF-8","sjis-win");
    $disp = mb_convert_encoding(mb_convert_encoding($disp,"sjis-win","eucJP-win"),"UTF-8","sjis-win");
    $val  = mb_convert_encoding(mb_convert_encoding($val, "sjis-win","eucJP-win"),"UTF-8","sjis-win");
    $node = $this->append_element($parent, $key);
    $node->set_attribute("main", $key);
    if ($disp != "") $node->set_attribute("disp", $disp);
    if ($is_hidden)  $node->set_attribute("hidden", "yes");
    if ($val != "")  $node->append_child($this->doc->create_text_node($val));
    return $node;
  }

  // $_REQUESTから、年月日ドロップダウン合計３個を取得して、「Y年M月D日」文字列を返却する。
  function concat_request_ymd($prefix){
    $y = (int)@$_REQUEST[$prefix."y"];
    $m = (int)@$_REQUEST[$prefix."m"];
    $d = (int)@$_REQUEST[$prefix."d"];
    if (!$y || !$m || !$d) return "";
    if (!checkdate($m,$d,$y)) return "";
    $t = mktime(0,0,0, $m, $d, $y);
    return $y."年".$m."月".$d."日";
  }

  // $_REQUESTから、年月日ドロップダウン合計３個を取得して、「YYYYMMDD」文字列を返却する。
  function concat_request_ymd_to_yyyymmdd($prefix){
    $y = (int)@$_REQUEST[$prefix."y"];
    $m = (int)@$_REQUEST[$prefix."m"];
    $d = (int)@$_REQUEST[$prefix."d"];
    if (!$y || !$m || !$d) return "";
    if (!checkdate($m,$d,$y)) return "";
    $t = mktime(0,0,0, $m, $d, $y);
    return date("Ymd", $t);
  }

  // $_REQUESTから、曜日チエックボックス7個を取得して、「日,月,火,水,木,金,土」の文字列を返す。
  function concat_request_week($prefix){
    $weeks_en = array("sun","mon","tue","wed","thu","fri","sat");
    $weeks_jp = array("日","月","火","水","木","金","土");
    $out = array();
    for ($i = 0; $i < 7; $i++){
      if (@$_REQUEST[$prefix.$weeks_en[$i]]) $out[] = $weeks_jp[$i];
    }
    if (!count($out)) return "";
    return implode(",", $out);
  }

  function get_mst_item_names($con, $fname, $mst_cd, $item_cd_array){
    $item_cd_array = (array)$item_cd_array;
    $out = array();
    foreach($item_cd_array as $item_cd) $out[] = $this->get_mst_item_name($con, $fname, $mst_cd, $item_cd);
    return implode(",", $out);
  }

  function get_mst_item_name($con, $fname, $mst_cd, $item_cd){
    global $c_sot_util;
    if (trim($item_cd) == "") return;
    $sql =
    " select item_name from tmplitem".
    " where mst_cd = '".pg_escape_string($mst_cd)."'".
    " and item_cd = '".pg_escape_string($item_cd)."'";
    $sel = $c_sot_util->select_from_table($sql, 0);
    return @pg_result($sel, 0, "item_name");
  }

}
