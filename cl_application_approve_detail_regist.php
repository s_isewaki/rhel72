<?
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_common.ini");
require_once("cl_common_log_class.inc");
require_once("cl_common_apply.inc");
require_once(dirname(__FILE__) . "/Cmx.php");
require_once('MDB2.php');

$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

$fname=$PHP_SELF;

//------------------------------------------------------------------------------
// セッションのチェック
//------------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	$log->error('セッションのチェックエラー',__FILE__,__LINE__);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//------------------------------------------------------------------------------
// 決裁・申請権限のチェック
//------------------------------------------------------------------------------
$checkauth = check_authority($session, $CAS_MENU_AUTH, $fname);
if ($checkauth == "0") {
	$log->error('決裁・申請権限のチェックエラー',__FILE__,__LINE__);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$con = connect2db($fname);
$mdb2 = MDB2::connect(CMX_DB_DSN);

$obj = new cl_application_workflow_common_class($con, $fname);
//------------------------------------------------------------------------------
// 職員情報取得
//------------------------------------------------------------------------------
$arr_empmst = $obj->get_empmst($session);
$login_emp_id        = $arr_empmst[0]["emp_id"];

//------------------------------------------------------------------------------
// トランザクションを開始
//------------------------------------------------------------------------------
//pg_query($con, "begin");
$mdb2->beginTransaction();

require_once(dirname(__FILE__) . "/cl/model/workflow/empmst_model.php");
$empmst_model = new empmst_model($mdb2,$emp_id);
$arr_empmst = $empmst_model->select_login_emp($session);
$emp_id = $arr_empmst[0]["emp_id"];

//------------------------------------------------------------------------------
// ワークフローマスタ取得
//------------------------------------------------------------------------------
//$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
require_once(dirname(__FILE__) . "/cl/model/workflow/cl_wkfwmst_model.php");
$cl_wkfwmst_model = new cl_wkfwmst_model($mdb2,$login_emp_id);
$arr_apply_wkfwmst = $cl_wkfwmst_model->get_apply_wkfwmst($apply_id);
$wkfw_id           = $arr_apply_wkfwmst["wkfw_id"];
$wkfw_history_no   = $arr_apply_wkfwmst["wkfw_history_no"];
$wkfw_content_type = $arr_apply_wkfwmst["wkfw_content_type"];
$short_wkfw_name   = $arr_apply_wkfwmst["short_wkfw_name"];


require_once(dirname(__FILE__) . "/cl/model/workflow/cl_applyapv_model.php");
$cl_applyapv_model = new cl_applyapv_model($mdb2,$login_emp_id);;

require_once(dirname(__FILE__) . "/cl/model/workflow/cl_apply_model.php");
$cl_apply_model = new cl_apply_model($mdb2,$login_emp_id);;

require_once(dirname(__FILE__) . "/cl/model/workflow/cl_applynotice_model.php");
$cl_applynotice_model = new cl_applynotice_model($mdb2, $emp_id);

require_once(dirname(__FILE__) . "/cl/model/workflow/cl_applyasyncrecv_model.php");
$cl_applyasyncrecv_model = new cl_applyasyncrecv_model($mdb2, $emp_id);

//------------------------------------------------------------------------------
// テンプレート独自の申請処理を呼び出す
//------------------------------------------------------------------------------
//ハンドラーモジュールロード
$log->debug('ハンドラーモジュールロード　開始　管理番号：'.$short_wkfw_name,__FILE__,__LINE__);
$handler_script=dirname(__FILE__) . "/cl/handler/".$short_wkfw_name."_handler.php";
$log->debug($handler_script,__FILE__,__LINE__);
require_once($handler_script);
$log->debug('ハンドラーモジュールロード　終了',__FILE__,__LINE__);

// 2012/04/21 Yamagawa add(s)
// ハンドラー　クラス化対応
$log->debug('ハンドラークラスインスタンス化　開始',__FILE__,__LINE__);
$handler_name = $short_wkfw_name."_handler";
$handler = new $handler_name();
$log->debug('ハンドラークラスインスタンス化　終了',__FILE__,__LINE__);
// 2012/04/21 Yamagawa add(e)

//------------------------------------------------------------------------------
// テンプレート項目のみを取得
//------------------------------------------------------------------------------
$temp_param=template_parameter_pre_proccess($_POST);

//------------------------------------------------------------------------------
// パラメータに申請ＩＤを追加
//------------------------------------------------------------------------------
$temp_param['apply_id'] = $apply_id;
$temp_param['emp_id'] = $login_emp_id;
// 2012/10/25 Yamagawa add(s)
$temp_param['apv_order'] = $_POST['apv_order'];
$temp_param['apv_sub_order'] = $_POST['apv_sub_order'];
$temp_param['approve'] = $_POST['approve'];
// 2012/10/25 Yamagawa add(e)

//$agent_emp_id = $agent_user;	// パラメータの代理承認者の職員IDを使う

// 2012/11/29 Yamagawa upd(s)
/*
if ( isset($_GET["agent_user"]) ){
	$agent_emp_id = $_GET["agent_user"];	// パラメータの代理承認者の職員IDを使う
}

	$log->debug('■isset　　'.$_GET["agent_user"],__FILE__,__LINE__);
	$log->debug('■agent_user*********　　'.$agent_user,__FILE__,__LINE__);
*/
if ($_POST["agent_user"] != ""){
	$agent_emp_id = $_POST["agent_user"];	// パラメータの代理承認者の職員IDを使う
}
// 2012/11/29 Yamagawa upd(e)

//------------------------------------------------------------------------------
// 承認詳細　下書き保存　の場合
//------------------------------------------------------------------------------
if($mode == "approve_draft_regist" )
{
;
	$log->debug('■agent_emp_id　　'.$agent_emp_id,__FILE__,__LINE__);
	$param  = array(
		"apply_id"		=> $apply_id		,
		"apv_order"		=> $apv_order		,
		"apv_sub_order"	=> $apv_sub_order	,
		"apv_comment"	=> $apv_comment		,
		"draft_flg"		=> "t",
		"agent_user"	=> $agent_emp_id		// 代理承認者の$emp_id
	);
	//$obj->update_draft_flg($arr);
	$cl_applyapv_model->update_draft_flg($param);

	//--------------------------------------------------------------------------
	// テンプレート独自の承認時　一時保存処理を呼び出す
	//--------------------------------------------------------------------------
	//dataApproveTmp($mdb2, $login_emp_id, $temp_param);
	$log->debug('■ハンドラー承認詳細　下書き保存　開始',__FILE__,__LINE__);
	// 2012/04/21 Yamagawa upd(s)
	//ApvDetail_Draft($mdb2, $login_emp_id, $temp_param);
	$handler->ApvDetail_Draft($mdb2, $login_emp_id, $temp_param);
	// 2012/04/21 Yamagawa upd(e)
	$log->debug('■ハンドラー承認詳細　下書き保存　終了',__FILE__,__LINE__);

}
//------------------------------------------------------------------------------
// 承認詳細　登録　の場合
//------------------------------------------------------------------------------
else if($mode == "approve_regist")
{
	// 承認ステータス更新
	$param = array(
		"apv_stat"		=> $approve,
		"apv_date"		=> date("YmdHi"),
		"apv_comment"	=> $apv_comment,
		"apply_id"		=> $apply_id,
		"apv_order"		=> $apv_order,
		"apv_sub_order"	=> $apv_sub_order,

		"agent_user"	=> $agent_emp_id		// 代理人$emp_id
	);
	$cl_applyapv_model->update_apvstat_detail($param);

		// 同報タイプ
		if($wkfw_appr == "1")
		{
			// 全承認者数取得
			$allapvcnt = $cl_applyapv_model->get_allapvcnt($apply_id);
			switch($approve)
			{
				case "1":   // 承認
					$param = array(
						"apply_id"	=> $apply_id,
						"apv_stat"	=> $approve
					);
					$apvstatcnt = $cl_applyapv_model->get_apvstatcnt($param);
					if($allapvcnt == $apvstatcnt)
					{
						// 申請ステータス更新
						$param = array(
							"apply_id"		=> $apply_id,
							"apply_stat"	=> "1",

							"agent_user"	=> $agent_emp_id		// 代理人$emp_id
						);
						$cl_apply_model->update_applystat($param);

						// 申請結果通知更新
						$param = array(
							"apply_id"		=> $apply_id,
							"send_emp_id"	=> $emp_id,
							"send_date"		=> date("YmdHi")
						);
						$cl_applynotice_model->update_send($param);
					}
					break;
				case "2":   // 否認
				case "3":   // 差戻し
					// 申請ステータス更新
					$param = array(
						"apply_id"		=> $apply_id,
						"apply_stat"	=> $approve,

						"agent_user"	=> $agent_emp_id		// 代理人$emp_id
					);
					$cl_apply_model->update_applystat($param);
					// 申請結果通知更新
					$param = array(
						"apply_id"		=> $apply_id,
						"send_emp_id"	=> $emp_id,
						"send_date"		=> date("YmdHi")
					);
					$cl_applynotice_model->update_send($param);
					break;
				default:
					break;
			}
		}
		// 稟議タイプ
		else if($wkfw_appr == "2")
		{
			$param = array(
				"apply_id"	=> $apply_id,
				"apv_order"	=> $apv_order
			);
			$same_hierarchy_apvcnt = $cl_applyapv_model->get_same_hierarchy_apvcnt($param);

			// 該当承認者の同一階層に他の承認者がいる場合(複数承認者)
			if($same_hierarchy_apvcnt > 1)
			{
				// 最終承認階層取得
				$last_apv_order = $cl_applyapv_model->get_last_apv_order($apply_id);

				// 承認者の階層より後につづく階層がある場合
				if($apv_order < $last_apv_order)
				{
					// 承認者の階層が非同期指定の場合
					if($next_notice_div == "1")
					{
						switch($approve)
						{
							case "1":   // 承認
								// 非同期・同期受信テーブル更新
								$param = array(
									"apply_id"	=> $apply_id,
									"apv_order"	=> $apv_order
								);
								$apved_order = $cl_applyasyncrecv_model->select_max_send_apved_order($apply_id, $apv_order);

								$param = array(
									"apply_id"		=> $apply_id,
									"apv_order"		=> $apv_order,
									"apv_sub_order"	=> $apv_sub_order,
									"apved_order"	=> $apved_order + 1
								);
								$cl_applyasyncrecv_model->update_apv_show_flg($param);
								$log->debug('非同期・同期受信更新',__FILE__,__LINE__);
								break;
							case "2":   // 否認

								$param = array(
									"apply_id"	=> $apply_id,
									"apv_order"	=> $apv_order,
									"apv_stat"	=> "2"
								);
								$same_hierarchy_apvstatcnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);

								// 全員が否認の場合
								if($same_hierarchy_apvcnt == $same_hierarchy_apvstatcnt)
								{
									// 申請ステータス更新
									$param = array(
										"apply_id"		=>	$apply_id,
										"apply_stat"	=>	"2",
										"agent_user"	=>	$agent_emp_id		// 代理人$emp_id
									);
									$cl_apply_model->update_applystat($param);
									// 申請結果通知更新
									$param = array(
										"apply_id"		=> $apply_id,
										"send_emp_id"	=> $emp_id,
										"send_date"		=> date("YmdHi")
									);
									$cl_applynotice_model->update_send($param);
								}

							case "3":   // 差戻し
								// 同一階層で先に「承認」した人がいた場合
								$param = array(
									"apply_id"	=> $apply_id,
									"apv_order"	=> $apv_order,
									"apv_stat"	=> "1"
								);
								$same_hierarchy_apvstatcnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);
								if($same_hierarchy_apvstatcnt > 0)
								{
									// 非同期・同期受信テーブル更新
									$param = array(
										"apply_id"	=> $apply_id,
										"apv_order"	=> $apv_order
									);
									$apved_order = $cl_applyasyncrecv_model->select_max_send_apved_order($apply_id, $apv_order);
									$param = array(
										"apply_id"		=> $apply_id,
										"apv_order"		=> $apv_order,
										"apv_sub_order"	=> $apv_sub_order,
										"apved_order"	=> $apved_order + 1
									);
									$cl_applyasyncrecv_model->update_apv_show_flg($param);
									$log->debug('非同期・同期受信更新',__FILE__,__LINE__);
								}
								break;

							default:
								break;
						}

						// 未承認者がいない ＡＮＤ 承認が一人もいない ＡＮＤ 差戻しが一人でもいる
						$param = array(
							"apply_id"	=> $apply_id,
							"apv_order"	=> $apv_order,
							"apv_stat"	=> "0"
						);
						$non_apvstatcnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);
						$param = array(
							"apply_id"	=> $apply_id,
							"apv_order"	=> $apv_order,
							"apv_stat"	=> "1"
						);
						$ok_apvstatcnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);
						$param = array(
							"apply_id"	=> $apply_id,
							"apv_order"	=> $apv_order,
							"apv_stat"	=> "3"
						);
						$bak_apvstatcnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);
						if($non_apvstatcnt == 0 && $ok_apvstatcnt == 0 && $bak_apvstatcnt > 0)
						{
							// 申請ステータス更新
							$param = array(
								"apply_id"		=> $apply_id,
								"apply_stat"	=> "3",

								"agent_user"	=>	$agent_emp_id		// 代理人$emp_id
							);
							$cl_apply_model->update_applystat($param);
							// 申請結果通知更新
							$param = array(
								"apply_id"		=> $apply_id,
								"send_emp_id"	=> $emp_id,
								"send_date"		=> date("YmdHi")
							);
							$cl_applynotice_model->update_send($param);
						}
					}
					// 承認者の階層が同期指定の場合
					else if($next_notice_div == "2")
					{
						$param = array(
							"apply_id"	=> $apply_id,
							"apv_order"	=> $apv_order,
							"apv_stat"	=> "0"
						);
						$same_hierarchy_apvstatcnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);
						// 同一階層で未承認者がいない場合
						if($same_hierarchy_apvstatcnt == 0)
						{
							$param = array(
								"apply_id"	=> $apply_id,
								"apv_order"	=> $apv_order
							);
							$same_hierarchy_apvcnt = $cl_applyapv_model->get_same_hierarchy_apvcnt($param);
							$param = array(
								"apply_id"	=> $apply_id,
								"apv_order"	=> $apv_order,
								"apv_stat"	=> "1"
							);
							$ok_approvecnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);
							$param = array(
								"apply_id"	=> $apply_id,
								"apv_order"	=> $apv_order,
								"apv_stat"	=> "2"
							);
							$no_approvecnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);
							$param = array(
								"apply_id"	=> $apply_id,
								"apv_order"	=> $apv_order,
								"apv_stat"	=> "3"
							);
							$bak_approvecnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);

							// 全員が承認の場合
							if($same_hierarchy_apvcnt == $ok_approvecnt)
							{
								// 非同期・同期受信テーブル更新
								$param = array(
									"apply_id"		=> $apply_id,
									"apv_order"		=> $apv_order,
									"apv_sub_order"	=> '',
									"apved_order"	=> 1
								);
								$cl_applyasyncrecv_model->update_apv_show_flg($param);
								$log->debug('非同期・同期受信更新',__FILE__,__LINE__);
							}

							// 否認があって差戻しがない場合、否認にする。
							if($no_approvecnt > 0 && $bak_approvecnt == 0)
							{
								// 申請ステータス更新
								$param = array(
									"apply_id"		=> $apply_id,
									"apply_stat"	=> "2",

									"agent_user"	=>	$agent_emp_id		// 代理人$emp_id
								);
								$cl_apply_model->update_applystat($param);
								// 申請結果通知更新
								$param = array(
									"apply_id"		=> $apply_id,
									"send_emp_id"	=> $emp_id,
									"send_date"		=> date("YmdHi")
								);
								$cl_applynotice_model->update_send($param);
							}

							// 差戻しが１つでもある場合
							if($bak_approvecnt > 0)
							{
								// 申請ステータス更新
								$param = array(
									"apply_id"		=> $apply_id,
									"apply_stat"	=> "3",

									"agent_user"	=>	$agent_emp_id		// 代理人$emp_id
								);
								$cl_apply_model->update_applystat($param);
								// 申請結果通知更新
								$param = array(
									"apply_id"		=> $apply_id,
									"send_emp_id"	=> $emp_id,
									"send_date"		=> date("YmdHi")
								);
								$cl_applynotice_model->update_send($param);

							}
						}
					}
					// 権限並列指定の場合
					else if($next_notice_div == "3")
					{
						$param = array(
							"apply_id"	=> $apply_id,
							"apv_order"	=> $apv_order,
							"apv_stat"	=> "0"
						);
						$non_apvstatcnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);

						// 未承認者がいた場合
						if($non_apvstatcnt > 0)
						{
							// 他の承認者の承認ステータスが「未承認」の場合、ステータスを更新
							$param = array(
								"apply_id"	=> $apply_id,
								"apv_order"	=> $apv_order,
								"apv_date"	=> $apv_date,
								"apv_stat"	=> $approve
							);
							$cl_applyapv_model->update_applyapv_for_parallel($param);

							switch($approve)
							{
								case "1":   // 承認

									// 非同期・同期受信テーブル更新
									$param = array(
										"apply_id"		=> $apply_id,
										"apv_order"		=> $apv_order,
										"apv_sub_order"	=> '',
										"apved_order"	=> 1
									);
									$cl_applyasyncrecv_model->update_apv_show_flg($param);
									$log->debug('非同期・同期受信更新',__FILE__,__LINE__);

									break;
								case "2":   // 否認
								case "3":   // 差戻し

									// 申請ステータス更新
									$param = array(
										"apply_id"		=> $apply_id,
										"apply_stat"	=> $approve,

										"agent_user"	=>	$agent_emp_id		// 代理人$emp_id
									);
									$cl_apply_model->update_applystat($param);
									// 申請結果通知更新
									$param = array(
										"apply_id"		=> $apply_id,
										"send_emp_id"	=> $emp_id,
										"send_date"		=> date("YmdHi")
									);
									$cl_applynotice_model->update_send($param);
									break;

								default:
									break;
							}
						}
					}
				}
				// 承認者の階層より後につづく階層がない場合（最終階層）
				else if($apv_order == $last_apv_order)
				{

					// 承認者の階層が非同期指定の場合
					if($next_notice_div == "1")
					{
						$param = array(
							"apply_id"	=> $apply_id,
							"apv_order"	=> $apv_order,
							"apv_stat"	=> "0"
						);
						$same_hierarchy_apvstatcnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);
						// 同一階層で未承認者がいない場合
						if($same_hierarchy_apvstatcnt == 0)
						{
							// 承認が１つでもある場合
							$param = array(
								"apply_id"	=> $apply_id,
								"apv_order"	=> $apv_order,
								"apv_stat"	=> "1"
							);
							$approvecnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);
							if($approvecnt > 0)
							{
								// 申請ステータス更新
								$param = array(
									"apply_id"		=> $apply_id,
									"apply_stat"	=> "1",

									"agent_user"	=>	$agent_emp_id		// 代理人$emp_id
								);
								$cl_apply_model->update_applystat($param);
								// 申請結果通知更新
								$param = array(
									"apply_id"		=> $apply_id,
									"send_emp_id"	=> $emp_id,
									"send_date"		=> date("YmdHi")
								);
								$cl_applynotice_model->update_send($param);
							}

							// 全員が否認の場合
							$param = array(
								"apply_id"	=> $apply_id,
								"apv_order"	=> $apv_order
							);
							$same_hierarchy_apvcnt = $cl_applyapv_model->get_same_hierarchy_apvcnt($param);
							$param = array(
								"apply_id"	=> $apply_id,
								"apv_order"	=> $apv_order,
								"apv_stat"	=> "2"
							);
							$no_approvecnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);
							if($same_hierarchy_apvcnt == $no_approvecnt)
							{
								// 申請ステータス更新
								$param = array(
									"apply_id"		=> $apply_id,
									"apply_stat"	=> "2",

									"agent_user"	=>	$agent_emp_id		// 代理人$emp_id
								);
								$cl_apply_model->update_applystat($param);
								// 申請結果通知更新
								$param = array(
									"apply_id"		=> $apply_id,
									"send_emp_id"	=> $emp_id,
									"send_date"		=> date("YmdHi")
								);
								$cl_applynotice_model->update_send($param);
							}

							// 承認がなく差戻しがある場合
							if($approvecnt == 0)
							{
								$param = array(
									"apply_id"	=> $apply_id,
									"apv_order"	=> $apv_order,
									"apv_stat"	=> "3"
								);
								$bak_cnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);
								if($bak_cnt > 0)
								{
									// 申請ステータス更新
									$param = array(
										"apply_id"		=> $apply_id,
										"apply_stat"	=> "3",

										"agent_user"	=>	$agent_emp_id		// 代理人$emp_id
									);
									$cl_apply_model->update_applystat($param);
									// 申請結果通知更新
									$param = array(
										"apply_id"		=> $apply_id,
										"send_emp_id"	=> $emp_id,
										"send_date"		=> date("YmdHi")
									);
									$cl_applynotice_model->update_send($param);
								}
							}
						}
					}
					// 承認者の階層が同期指定の場合
					else if($next_notice_div == "2")
					{
						$param = array(
							"apply_id"	=> $apply_id,
							"apv_order"	=> $apv_order,
							"apv_stat"	=> "0"
						);
						$same_hierarchy_apvstatcnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);
						// 同一階層で未承認者がいない場合
						if($same_hierarchy_apvstatcnt == 0)
						{
							$param = array(
								"apply_id"	=> $apply_id,
								"apv_order"	=> $apv_order
							);
							$same_hierarchy_apvcnt = $cl_applyapv_model->get_same_hierarchy_apvcnt($param);
							$param = array(
								"apply_id"	=> $apply_id,
								"apv_order"	=> $apv_order,
								"apv_stat"	=> "1"
							);
							$ok_approvecnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);
							$param = array(
								"apply_id"	=> $apply_id,
								"apv_order"	=> $apv_order,
								"apv_stat"	=> "2"
							);
							$no_approvecnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);
							$param = array(
								"apply_id"	=> $apply_id,
								"apv_order"	=> $apv_order,
								"apv_stat"	=> "3"
							);
							$bak_approvecnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);

							// 全員が承認の場合
							if($same_hierarchy_apvcnt == $ok_approvecnt)
							{
								// 申請ステータス更新
								$param = array(
									"apply_id"		=> $apply_id,
									"apply_stat"	=> "1",

									"agent_user"	=>	$agent_emp_id		// 代理人$emp_id
								);
								$cl_apply_model->update_applystat($param);
								// 申請結果通知更新
								$param = array(
									"apply_id"		=> $apply_id,
									"send_emp_id"	=> $emp_id,
									"send_date"		=> date("YmdHi")
								);
								$cl_applynotice_model->update_send($param);
							}

							// 否認があって差戻しがない場合、否認にする。
							if($no_approvecnt > 0 && $bak_approvecnt == 0)
							{
								// 申請ステータス更新
								$param = array(
									"apply_id"		=> $apply_id,
									"apply_stat"	=> "2",

									"agent_user"	=>	$agent_emp_id		// 代理人$emp_id
								);
								$cl_apply_model->update_applystat($param);
								// 申請結果通知更新
								$param = array(
									"apply_id"		=> $apply_id,
									"send_emp_id"	=> $emp_id,
									"send_date"		=> date("YmdHi")
								);
								$cl_applynotice_model->update_send($param);
							}

							// 差戻しが１つでもある場合
							if($bak_approvecnt > 0)
							{
								// 申請ステータス更新
								$param = array(
									"apply_id"		=> $apply_id,
									"apply_stat"	=> "3",

									"agent_user"	=>	$agent_emp_id		// 代理人$emp_id
								);
								$cl_apply_model->update_applystat($param);
								// 申請結果通知更新
								$param = array(
									"apply_id"		=> $apply_id,
									"send_emp_id"	=> $emp_id,
									"send_date"		=> date("YmdHi")
								);
								$cl_applynotice_model->update_send($param);

							}
						}
					}
					// 権限並列指定の場合
					else if($next_notice_div == "3")
					{
						$param = array(
							"apply_id"	=> $apply_id,
							"apv_order"	=> $apv_order,
							"apv_stat"	=> "0"
						);
						$non_apvstatcnt = $cl_applyapv_model->get_same_hierarchy_apvstatcnt($param);

						// 未承認者がいた場合
						if($non_apvstatcnt > 0)
						{
							// 他の承認者の承認ステータスが「未承認」の場合、「承認」に更新
							$param = array(
								"apply_id"	=> $apply_id,
								"apv_order"	=> $apv_order,
								"apv_date"	=> $apv_date,
								"apv_stat"	=> $approve
							);
							$cl_applyapv_model->update_applyapv_for_parallel($param);
							// 申請ステータス更新
							$param = array(
								"apply_id"		=> $apply_id,
								"apply_stat"	=> $approve,

								"agent_user"	=>	$agent_emp_id		// 代理人$emp_id
							);
							$cl_apply_model->update_applystat($param);
							// 申請結果通知更新
							$param = array(
								"apply_id"		=> $apply_id,
								"send_emp_id"	=> $emp_id,
								"send_date"		=> date("YmdHi")
							);
							$cl_applynotice_model->update_send($param);
						}
					}
				}
			}
			// 該当承認者の同一階層に他の承認者がいない場合(承認者一人)
			else if($same_hierarchy_apvcnt == 1)
			{
				switch($approve)
				{
					case "1":   // 承認
						// 最終承認階層取得
						$last_apv_order = $cl_applyapv_model->get_last_apv_order($apply_id);
						if($apv_order == $last_apv_order)
						{
							// 申請ステータス更新
							$param = array(
								"apply_id"		=> $apply_id,
								"apply_stat"	=> "1",

								"agent_user"	=>	$agent_emp_id		// 代理人$emp_id
							);
							$cl_apply_model->update_applystat($param);
							// 申請結果通知更新
							$param = array(
								"apply_id"		=> $apply_id,
								"send_emp_id"	=> $emp_id,
								"send_date"		=> date("YmdHi")
							);
							$cl_applynotice_model->update_send($param);
						}
						break;
					case "2":   // 否認
					case "3":   // 差戻し
						// 申請ステータス更新
						$param = array(
								"apply_id"		=> $apply_id,
								"apply_stat"	=> $approve,

								"agent_user"	=>	$agent_emp_id		// 代理人$emp_id
							);
						$cl_apply_model->update_applystat($param);
						// 申請結果通知更新
						$param = array(
							"apply_id"		=> $apply_id,
							"send_emp_id"	=> $emp_id,
							"send_date"		=> date("YmdHi")
						);
						$cl_applynotice_model->update_send($param);
						break;
					default:
						break;

				}
			}
		}


	//--------------------------------------------------------------------------
	// 承認詳細　登録の場合
	//--------------------------------------------------------------------------
	//dataApproveEdit($mdb2, $emp_id, $temp_param);
	$log->debug('■ハンドラー承認詳細　登録　開始',__FILE__,__LINE__);
	// 2012/04/21 Yamagawa upd(s)
	//ApvDetail_Regist($mdb2, $login_emp_id, $temp_param);
	$handler->ApvDetail_Regist($mdb2, $login_emp_id, $temp_param);
	// 2012/04/21 Yamagawa upd(e)
	$log->debug('■ハンドラー承認詳細　登録　終了',__FILE__,__LINE__);

}
else if ($mode == "approve_update") {
	$log->debug('■agent_emp_id　　'.$agent_emp_id,__FILE__,__LINE__);
	$param  = array(
		"apply_id"		=> $apply_id		,
		"apv_order"		=> $apv_order		,
		"apv_sub_order"	=> $apv_sub_order	,
		"apv_comment"	=> $apv_comment		,
		"draft_flg"		=> "f",
		"agent_user"	=> $agent_emp_id		// 代理承認者の$emp_id
	);
	//$obj->update_draft_flg($arr);
	$cl_applyapv_model->update_draft_flg($param);

	//--------------------------------------------------------------------------
	// テンプレート独自の承認時　一時保存処理を呼び出す
	//--------------------------------------------------------------------------
	//dataApproveTmp($mdb2, $login_emp_id, $temp_param);
	$log->debug('■ハンドラー承認詳細　更新　開始',__FILE__,__LINE__);
	// 2012/04/21 Yamagawa upd(s)
	//ApvDetail_Draft($mdb2, $login_emp_id, $temp_param);
	$handler->ApvDetail_Draft($mdb2, $login_emp_id, $temp_param);
	// 2012/04/21 Yamagawa upd(e)
	$log->debug('■ハンドラー承認詳細　更新　終了',__FILE__,__LINE__);
}
//------------------------------------------------------------------------------
// その他
//------------------------------------------------------------------------------
else{

}

//------------------------------------------------------------------------------
// トランザクションをコミット
//------------------------------------------------------------------------------
//pg_query($con, "commit");
$mdb2->commit();

// 2012/04/21 Yamagawa add(s)
//------------------------------------------------------------------------------
// 承認詳細　登録　の場合
//------------------------------------------------------------------------------
/*
if ($mode == "approve_regist") {

	$log->debug("require START cl_mst_ladder_wkfw_link_model.php",__FILE__,__LINE__);
//	require_once(dirname(__FILE__) . "/../model/search/cl_mst_ladder_wkfw_link_model.php");
	require_once("./cl/model/search/cl_mst_ladder_wkfw_link_model.php");
	$log->debug("require END cl_mst_ladder_wkfw_link_model.php",__FILE__,__LINE__);

	$log->debug("cl_mst_ladder_wkfw_link_model インスタンス作成開始",__FILE__,__LINE__);
	$link_model = new cl_mst_ladder_wkfw_link_model($mdb2,$user);
	$log->debug("cl_mst_ladder_wkfw_link_model インスタンス作成終了",__FILE__,__LINE__);

	// 関連付けマスタ取得
	$log->debug("関連付けマスタ取得開始",__FILE__,__LINE__);
	$link_data = $link_model->getList($short_wkfw_name);
	$log->debug("関連付けマスタ取得終了",__FILE__,__LINE__);

	if (count($link_data) > 0) {
		$evaluation_colleague_short_wkfw_name = $link_data[0]['evaluation_colleague_short_wkfw_name'];

		$log->debug("require START cl_apply_model.php",__FILE__,__LINE__);
		require_once(dirname(__FILE__) . "/cl/model/workflow/cl_apply_model.php");
		$log->debug("require END cl_apply_model.php",__FILE__,__LINE__);

		$log->debug("cl_apply_model インスタンス作成開始",__FILE__,__LINE__);
		$apply_model = new cl_apply_model($mdb2,$user);
		$log->debug("cl_apply_model インスタンス作成終了",__FILE__,__LINE__);

		// 申請データ取得
		$log->debug("申請データ取得開始",__FILE__,__LINE__);
		$apply_data = $apply_model->select($apply_id);
		$log->debug("申請データ取得終了",__FILE__,__LINE__);

		// 同僚評価表を自動申請する
		if ($apply_data['apply_stat'] == 1){

			require_once('cl_auto_application_apply.php');

			//------------------------------------------------------------------------------
			// テンプレート独自の申請処理を呼び出す
			//------------------------------------------------------------------------------
			//ハンドラーモジュールロード
			$log->debug('ハンドラーモジュールロード　開始　管理番号：'.$short_wkfw_name,__FILE__,__LINE__);
			$evaluation_colleague_handler_script=dirname(__FILE__) . "/cl/handler/".$evaluation_colleague_short_wkfw_name."_handler.php";
			$log->debug($evaluation_colleague_handler_script,__FILE__,__LINE__);
			require_once($evaluation_colleague_handler_script);
			$log->debug('ハンドラーモジュールロード　終了',__FILE__,__LINE__);

			// ハンドラー　クラス化対応
			$log->debug('ハンドラークラスインスタンス化　開始',__FILE__,__LINE__);
			$evaluation_colleague_handler_name = $evaluation_colleague_short_wkfw_name."_handler";
			$evaluation_colleague_handler = new $evaluation_colleague_handler_name();
			$log->debug('ハンドラークラスインスタンス化　終了',__FILE__,__LINE__);

			//------------------------------------------------------------------------------
			// トランザクションを開始
			//------------------------------------------------------------------------------
			$mdb2->beginTransaction();

			// 自動申請API呼出し
			$param["apply_id"] = auto_application_apply($obj, $mdb2, $evaluation_colleague_short_wkfw_name, $login_emp_id, $param['hdn_apply_chief_id']);

			$param["colleague_division"] = "1";

			$log->debug('■ハンドラー新規申請　申請処理　開始',__FILE__,__LINE__);
			$evaluation_colleague_handler->NewApl_Regist($mdb2, $login_emp_id, $param);
			$log->debug('■ハンドラー新規申請　申請処理　終了',__FILE__,__LINE__);

			//------------------------------------------------------------------------------
			// トランザクションをコミット
			//------------------------------------------------------------------------------
			$mdb2->commit();

			//------------------------------------------------------------------------------
			// トランザクションを開始
			//------------------------------------------------------------------------------
			$mdb2->beginTransaction();

			// 自動申請API呼出し
			$param["apply_id"] = auto_application_apply($obj, $mdb2, $evaluation_colleague_short_wkfw_name, $login_emp_id, $param['hdn_apply_general_id']);

			$param["colleague_division"] = "2";

			$log->debug('■ハンドラー新規申請　申請処理　開始',__FILE__,__LINE__);
			$evaluation_colleague_handler->NewApl_Regist($mdb2, $login_emp_id, $param);
			$log->debug('■ハンドラー新規申請　申請処理　終了',__FILE__,__LINE__);

			//------------------------------------------------------------------------------
			// トランザクションをコミット
			//------------------------------------------------------------------------------
			$mdb2->commit();
		}

	}

}
*/
// 2012/04/21 Yamagawa add(e)

//------------------------------------------------------------------------------
// データベース接続を切断
//------------------------------------------------------------------------------
pg_close($con);
$mdb2->disconnect();

// 一覧画面に遷移
// 2012/11/29 Yamagawa upd(s)
//echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
if ($agent_emp_id == "") {
	echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
} else {
	echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page_all_approve){window.opener.reload_page_all_approve();}</script>");
}
// 2012/11/29 Yamagawa upd(e)
echo("<script language=\"javascript\">window.close();</script>\n");

?>
