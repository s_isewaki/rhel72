<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 日付の設定
$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));

// 職員名をURLエンコード
$enc_emp_nm = urlencode($emp_nm);

// 公開スケジュール画面のURLを設定
$other_url = "schedule_search.php?session=$session&emp_nm=$enc_emp_nm&cls=$cls&atrb=$atrb&dept=$dept&pjt_id=$pjt_id&st_id=$st_id&job_id=$job_id";

// データベースに接続
$con = connect2db($fname);

// 表示対象職員の取得
$sql = "select e.emp_id from empmst e";
$cond = "where exists (select * from authmst a where a.emp_id = e.emp_id and a.emp_del_flg = 'f')";
if ($cls != 0) {
	$cond .= " and ((e.emp_class = $cls) or exists (select * from concurrent c where c.emp_id = e.emp_id and c.emp_class = $cls))";
}
if ($atrb != 0) {
	$cond .= " and ((e.emp_attribute = $atrb) or exists (select * from concurrent c where c.emp_id = e.emp_id and c.emp_attribute = $atrb))";
}
if ($dept != 0) {
	$cond .= " and ((e.emp_dept = $dept) or exists (select * from concurrent c where c.emp_id = e.emp_id and c.emp_dept = $dept))";
}
if ($emp_nm != "") {
	$full_nm = str_replace("　", "", str_replace(" ", "", $emp_nm));
	$cond .= " and (e.emp_lt_nm like '%$full_nm%' or e.emp_ft_nm like '%$full_nm%' or e.emp_kn_lt_nm like '%$full_nm%' or e.emp_kn_ft_nm like '%$full_nm%' or (e.emp_lt_nm || e.emp_ft_nm) like '%$full_nm%' or (e.emp_kn_lt_nm || e.emp_kn_ft_nm) like '%$full_nm%')";
}
if ($pjt_id != 0) {
	$cond .= " and (exists (select * from project p where p.pjt_id = '$pjt_id' and p.pjt_response = e.emp_id) or exists (select * from promember pm where pm.pjt_id = '$pjt_id' and pm.emp_id = e.emp_id))";
}
if ($st_id != 0) {
	$cond .= " and ((e.emp_st = $st_id) or exists (select * from concurrent c where c.emp_id = e.emp_id and c.emp_st = $st_id))";
}
if ($job_id != 0) {
	$cond .= " and e.emp_job = $job_id";
}
$cond .= " order by e.emp_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 該当職員数を取得
$emp_cnt = pg_num_rows($sel);

// 委員会・WGが指定されていて、表示対象職員が存在する場合（特殊ケース）
if ($pjt_id != 0 && $emp_cnt > 0) {

	// 週間スケジュール画面を開く
	$url = "schedule_other_emps_week.php?session=$session&date=$date&srch_emp_nm=$enc_emp_nm&srch_cls=$cls&srch_atrb=$atrb&srch_dept=$dept&srch_pjt_id=$pjt_id&srch_st_id=$st_id&srch_job_id=$job_id";
	while ($row = pg_fetch_array($sel)) {
		$url .= "&emp_id[]={$row["emp_id"]}";
	}
	$win_name = "floatingpage";
	$win_width = 840;
	$win_height = 700;

// 表示対象職員が1件の場合（特殊ケース）
} else if ($emp_cnt == 1) {

	// スケジュールのデフォルト画面を取得
	$sql = "select schedule2_default from option";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel_opt = select_from_table($con, $sql, $cond, $fname);
	if ($sel_opt == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$default = pg_fetch_result($sel_opt, 0, "schedule2_default");

	// デフォルト画面を開く
	switch ($default) {
	case "1":
		$url = "schedule_other_menu.php";
		break;
	case "2":
		$url = "schedule_other_week_menu.php";
		break;
	case "3":
		$url = "schedule_other_month_menu.php";
		break;
	}
	$url .= "?session=$session&date=$date&emp_id=" . pg_fetch_result($sel, 0, "emp_id");
	$win_name = "other";
	$win_width = 840;
	$win_height = 700;

// その他の場合（通常ケース）
} else {

	// 公開スケジュール画面を開く
	$url = $other_url;
	$win_name = "floatingpage";
	$win_width = 640;
	$win_height = 700;
}

// データベース接続を閉じる
pg_close($con);

// 画面を開く
echo("<script type=\"text/javascript\">window.open('$url', '$win_name', 'scrollbars=yes,width=$win_width,height=$win_height');</script>");

// 表示対象職員が1件の場合
if ($emp_cnt == 1) {

	// 公開スケジュール画面をリフレッシュ
	echo("<script type=\"text/javascript\">parent.location.href = '$other_url';</script>");
}
?>
