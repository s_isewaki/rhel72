<?php
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_postgres.php");				// DB操作用のソース
require_once("about_session.php");				// セッション管理用のソース
require_once("about_authority.php");			// 権限管理用のソース
require_once("hiyari_item_time_setting_models.php");

//===================================================================================================
//初期処理
//===================================================================================================

//セッションのチェック
if( $session == "0" ) {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限確認
$hiyari = check_authority($session, 48, $PHP_SELF);

if ($hiyari == "0") {
    showLoginPage();
    exit;
}

// DBコネクションを設定
$con = connect2db($PHP_SELF);

if ($con == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 設定情報を取得
$checked = getSetting($con, $PHP_SELF, $_POST);

//===================================================================================================
//HTML出力
//===================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> 
<html> 
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP"> 
    <title>CoMedix ファントルくん | 発見時間帯・発生時間帯設定</title> 
    <script type="text/javascript" src="js/fontsize.js"></script>

    <script type="text/javascript">
        //ＤＢ更新処理
        function update_data() {
            document.form1.submit();
        
        }
    </script> 

    <link rel="stylesheet" type="text/css" href="css/main.css"> 

    <style type="text/css"> 
        .list {border-collapse:collapse;}
        .list td {border:#35B341 solid 1px;}
        .non_in_list {border-collapse:collapse;}
        .non_in_list td {border:0px;}
    </style> 

</head> 
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"> 
    <form id="form1" name="form1" action="" method="post" onsubmit="return false;"> 
 
    <input type="hidden" name="session"         value="<?=$session?>">
    <input type="hidden" name="is_postback"     value="true">
 
    <!-- BODY全体 START --> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
        <tr> 
            <td> 
 
            <!-- ヘッダー START --> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
                <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
				    <tr height="32" bgcolor="#35B341"> 
					    <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>項目管理</b></font></td> 
					    <td>&nbsp</td> 
					    <td width="10">&nbsp</td> 
					    <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td> 
				    </tr> 
                </table> 
                <img src="img/spacer.gif" width="10" height="10" alt=""><br /> 
                </table> 
            <!-- ヘッダー END --> 
  
            <img src="img/spacer.gif" width="1" height="5" alt=""><br /> 
                <!-- メイン START --> 
                <table width="100%" border="0" cellspacing="0" cellpadding="5"> 
                    <tr> 
                        <td>
                            <!-- 外の枠 START --> 
                            <table border="0" cellspacing="0" cellpadding="0"> 
                                <tr> 
                                    <td width="10"><img src="img/r_1.gif" width="10" height="10"></td> 
                                    <td width="800" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td> 
                                    <td width="10"><img src="img/r_2.gif" width="10" height="10"></td> 
                                </tr>
     
                                <tr> 
                                    <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td> 
                                    <td bgcolor="#F5FFE5"> 
                                        <table width="100%" border="0" cellspacing="0" cellpadding="5"> 
                                            <tr> 
                                                <td> 
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
                                                        <tr height="22"> 
                                                            <td align="left" nowrap> 
                                                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b>発見時間帯・発生時間帯</b></font>
                                                            </td> 
                                                        
                                                            <td align="right"> 
                                                                <input type="button" value="更新" onclick="update_data();">
                                                            </td> 
                                                        </tr> 
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr> 
                                                <td> 
                                                    <!-- 一覧 START--> 
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="2"> 
                                                        <tr height="22"> 
                                                            <td width="300"  bgcolor="#DFFFDC" nowrap>
                                                                <input id="Radio1" name="two_hour_time_divide" value="true" type="radio" <?=$checked['true']?> />2時間単位選択表示
                                                            </td> 
                                                        </tr>
                                                        <tr height="22"> 
                                                            <td width="300"  bgcolor="#DFFFDC" nowrap>
                                                                <input id="Radio2" name="two_hour_time_divide" value="false" type="radio" <?=$checked['false']?> />時間・分選択表示
                                                            </td> 
                                                        </tr>
                                                    </table>
                                                    <!-- 一覧 END --> 
                                                </td>
                                            </tr>
                                                                                    
                                            <tr>
                                                <td> 
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
                                                        <tr> 
                                                            <td align="right"> 
                                                                <input type="button" value="更新" onclick="update_data();"> 
                                                            </td> 
                                                        </tr> 
                                                    </table>
                                                </td>
                                            </tr> 
                                        </table>                             

                                    </td>
                                    <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td> 
                                </tr>
                                <tr> 
                                    <td><img src="img/r_3.gif" width="10" height="10"></td> 
                                    <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td> 
                                    <td><img src="img/r_4.gif" width="10" height="10"></td> 
                                </tr>
                            </table> 
                            <!-- 外の枠 END -->

                        </td> 
                    </tr> 
                </table> 
                <!-- メイン END -->  
            </td> 
        </tr> 
    </table> 
    <!-- BODY全体 END --> 
    </form> 
    </body> 
</html> 