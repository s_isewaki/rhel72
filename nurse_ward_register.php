<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require("referer_common.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require_once("get_menu_label.ini");

$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//診療科権限チェック
$dept = check_authority($session,28,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// 患者管理管理者権限を取得
$patient_admin_auth = check_authority($session, 77, $fname);

// メドレポート管理権限を取得
$summary_admin_auth = check_authority($session, 59, $fname);

// 事業所IDの設定
$enti_id = 1;

//DBコネクションの作成
$con = connect2db($fname);



//==============================================================================
// もし登録・更新指定なら、更新する。（リダイレクトはしない）
//==============================================================================
if (@$_REQUEST["action"] == "regist"){
	$sql =
		" select b.bldg_cd, w.ward_cd from bldgmst b".
		" inner join wdmst w on (w.bldg_cd = b.bldg_cd)".
		" where b.bldg_del_flg = 'f' and w.ward_del_flg = 'f'";
	$sel = $c_sot_util->select_from_table($sql);
	$master_array = pg_fetch_all($sel);

	pg_query($con, "begin transaction");
	delete_from_table($con, "delete from sot_ward_empclass_relation", "", $fname);
	foreach ($master_array as $idx => $row){
		$cmb_id = "sel_" . $row["bldg_cd"] . "_" . $row["ward_cd"];
		$sql =
			" insert into sot_ward_empclass_relation (".
			" bldg_cd, ward_cd, class_id, atrb_id, dept_id, room_id".
			" ) values (".
			" " . $row["bldg_cd"].
			"," . $row["ward_cd"].
			"," . (@$_REQUEST[$cmb_id."_1"] != "" ? $_REQUEST[$cmb_id."_1"] : "null").
			"," . (@$_REQUEST[$cmb_id."_2"] != "" ? $_REQUEST[$cmb_id."_2"] : "null").
			"," . (@$_REQUEST[$cmb_id."_3"] != "" ? $_REQUEST[$cmb_id."_3"] : "null").
			"," . (@$_REQUEST[$cmb_id."_4"] != "" ? $_REQUEST[$cmb_id."_4"] : "null").
			" )";
		update_set_table($con, $sql, array(), null, "", $fname);
	}
	pg_query($con, "commit");
//	pg_close($con);
//	header("Location: nurse_ward_register.php?session=".$session);
//	echo("<script type=\"text/javascript\">location.href = 'nurse_ward_register.php?session=".$session."';</script>\n");
//	die;
}

// 遷移元の取得
$referer = get_referer($con, $session, "entity", $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];

// 病床管理/居室管理
$bed_manage_title = $_label_by_profile["BED_MANAGE"][$profile_type];

// 患者管理/利用者管理
$patient_manage_title = $_label_by_profile["PATIENT_MANAGE"][$profile_type];

// 病棟/施設
$ward_title = $_label_by_profile["WARD"][$profile_type];

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

// 担当医/担当者１
$doctor_title = $_label_by_profile["DOCTOR"][$profile_type];

// 担当看護師/担当者２
$nurse_title = $_label_by_profile["NURSE"][$profile_type];

// 担当看護師名/担当者２
$nurse_name_title = $_label_by_profile["NURSE_NAME"][$profile_type];

// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = get_report_menu_label($con, $fname);
?>
<title>CoMedix マスターメンテナンス | <?=$nurse_title?>登録</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function clearEmployeeID() {
	document.nu.nu_emp_personal_id.value = '';
	document.nu.nu_emp_id.value = '';
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? if ($referer == "1") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="<? echo ($bed_manage_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b><? echo ($bed_manage_title); ?></b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b><? echo ($section_title); ?></b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } else if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="patient_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b16.gif" width="32" height="32" border="0" alt="<? echo($patient_manage_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="patient_info_menu.php?session=<? echo($session); ?>"><b><? echo($patient_manage_title); ?></b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b><? echo ($section_title); ?></b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="patient_info_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } else if ($referer == "3") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="summary_menu.php?session=<? echo($session); ?>"><img src="img/icon/b57.gif" width="32" height="32" border="0" alt="<? echo($med_report_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="summary_menu.php?session=<? echo($session); ?>"><b><? echo($med_report_title); ?></b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b><? echo ($section_title); ?></b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="summary_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<table width="118" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279A5">
<td height="22" class="spacing" colspan="2"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">管理項目</font></b></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="entity_menu.php?session=<? echo($session); ?>"><? echo ($section_title); ?></a></font></td>
</tr>
<? if ($ward_admin_auth == "1" && $referer == "1") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="building_list.php?session=<? echo($session); ?>"><?=$ward_title?></a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_display_setting.php?session=<? echo($session); ?>">表示設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>">一括登録</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_master_menu.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>
<? } ?>
<? if ($patient_admin_auth == "1" && $referer == "2") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="patient_admin_menu.php?session=<? echo($session); ?>">オプション</a></font></td>
</tr>
<? } ?>
<? if ($summary_admin_auth == "1" && $referer == "3") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="summary_kanri_kubun.php?session=<? echo($session); ?>"><?=$summary_kubun_title?></a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="sum_ordsc_med_list.php?session=<? echo($session); ?>">処方・注射オーダ</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="summary_kanri_tmpl.php?session=<? echo($session); ?>">テンプレート</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="summary_mst.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="summary_access_log_daily.php?session=<? echo($session); ?>">アクセスログ</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="summary_kanri_option.php?session=<? echo($session); ?>">オプション</a></font></td>
</tr>

<? } ?>
</table>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="90" align="center" bgcolor="#bdd1e7"><a href="entity_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo ($section_title); ?>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="doctor_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$doctor_title?>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="nurse_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$nurse_title?>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="section_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo ($section_title); ?>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="doctor_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$doctor_title?>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="nurse_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><?=$nurse_title?>登録</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>


<img src="img/spacer.gif" alt="" width="1" height="3"><br>

<table border="0" cellspacing="0" cellpadding="6">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="nurse_register.php?session=<?=$session?>">診療科担当</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="nurse_ward_register.php?session=<?=$session?>">病棟担当</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="nurse_team_register.php?session=<?=$session?>">病棟担当(チーム)</a></font></td>
</tr>
</table>



<script type="text/javascript">
var selections = {};
var combobox = {};
function sel_changed(cmb_id, node, value){
	for(var i=1; i<=4; i++) combobox[i] = document.getElementById(cmb_id+"_"+i);<?// コンボ取得?>
	for(var i=4; i>=1; i--) if(node<i) combobox[i].options.length = 1;<? //下位層コンボをクリア ?>

	<? // 初期化用 自分コンボを再選択?>
	var cmb = combobox[node];
	if (node > 0 && cmb.value != value) {
		for(var i=0; i<cmb.options.length; i++) if (cmb.options[i].value==value) cmb.selectedIndex = i;
	}

	<? // 次階層をセット。最後の階層なら何もしない?>
	if (node >= 4) return;
	var idx = 1;
	var list = selections["node"+(node*1+1*1)];
	var cmb_next = combobox[node*1+1*1];
	for(var i in list){
		if (list[i].parent != value) continue;
		cmb_next.options.length = cmb_next.options.length + 1;
		cmb_next.options[idx].text  = list[i].name;
		cmb_next.options[idx].value = list[i].code;
		idx++;
	}
}
</script>
<?

// 各階層名の取得
$sel = $c_sot_util->select_from_table("select class_nm, atrb_nm, dept_nm, room_nm, class_cnt from classname");
$arr_class_name = pg_fetch_array($sel);

// 第１階層選択肢(部門一覧) JavaScript変数として出力する
$ary = array();
$sel = $c_sot_util->select_from_table("select * from classmst where class_del_flg = 'f' order by order_no");
$mst = pg_fetch_all($sel);
if (!$mst || !$mst[0]) $mst = array();
foreach($mst as $key => $row) $ary[] = '{"parent":"","code":"'.$row["class_id"].'","name":"'.$row["class_nm"].'"}';
echo "<script type='text/javascript'>\nselections.node1 = [". implode(",", $ary) . "]\n</script>\n";

// 第２階層選択肢(課一覧) JavaScript変数として出力する
$ary = array();
$sel = $c_sot_util->select_from_table("select * from atrbmst where atrb_del_flg = 'f' order by order_no");
$mst = pg_fetch_all($sel);
if (!$mst || !$mst[0]) $mst = array();
foreach($mst as $key => $row) $ary[] = '{"parent":"'.$row["class_id"].'","code":"'.$row["atrb_id"].'","name":"'.$row["atrb_nm"].'"}';
echo "<script type='text/javascript'>\nselections.node2 = [". implode(",", $ary) . "]\n</script>\n";

// 第３階層選択肢(科一覧) JavaScript変数として出力する
$ary = array();
$sql =
	" select deptmst.*, atrbmst.class_id from deptmst inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id".
	" where deptmst.dept_del_flg = 'f' order by deptmst.order_no";
$sel = $c_sot_util->select_from_table($sql);
$mst = pg_fetch_all($sel);
if (!$mst || !$mst[0]) $mst = array();
foreach($mst as $key => $row) $ary[] = '{"parent":"'.$row["atrb_id"].'","code":"'.$row["dept_id"].'","name":"'.$row["dept_nm"].'"}';
echo "<script type='text/javascript'>\nselections.node3 = [". implode(",", $ary) . "]\n</script>\n";

// 第４階層選択肢(室一覧) JavaScript変数として出力する
$ary = array();
if ($arr_class_name["class_cnt"] == 4) {
	$sql =
		" select classroom.*, atrbmst.class_id, atrbmst.atrb_id from classroom".
		" inner join deptmst on classroom.dept_id = deptmst.dept_id".
		" inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id".
		" where classroom.room_del_flg = 'f' order by classroom.order_no";
	$sel = $c_sot_util->select_from_table($sql);
	$mst = pg_fetch_all($sel);
	if (!$mst || !$mst[0]) $mst = array();
	foreach($mst as $key => $row) $ary[] = '{"parent":"'.$row["dept_id"].'","code":"'.$row["room_id"].'","name":"'.$row["room_nm"].'"}';
}
echo "<script type='text/javascript'>\nselections.node4 = [". implode(",", $ary) . "]\n</script>\n";

// 事業所と病棟マスタ、およびリレーション済みデータ一覧の取得
$sql =
	" select".
	" b.bldg_cd, b.bldg_name, w.ward_cd, w.ward_name".
	",s.class_id, s.atrb_id, s.dept_id, s.room_id".
	" from bldgmst b".
	" inner join wdmst w on (w.bldg_cd = b.bldg_cd)".
	" left join sot_ward_empclass_relation s on (s.bldg_cd = b.bldg_cd and s.ward_cd = w.ward_cd)".
	" where b.bldg_del_flg = 'f'".
	" and w.ward_del_flg = 'f'".
	" order by b.bldg_cd, w.ward_cd";
$sel = $c_sot_util->select_from_table($sql);
$relation_array = pg_fetch_all($sel);
// 事業所は、同じ名前が出現する場合は消しておく
$tmp_register = array();
for($i = 0; $i < count($relation_array); $i++){
	if (in_array($relation_array[$i]["bldg_cd"], $tmp_register)) $relation_array[$i]["bldg_name"] = "";
	$tmp_register[] = $relation_array[$i]["bldg_cd"];
}

$style_hidden = ($arr_class_name["class_cnt"] == 4 ? "" : 'visibility:hidden');
$empty_option = '<option value="">　　　　</option>';
$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

?>
<form name="nu" action="nurse_ward_register.php" method="post">
	<input type="hidden" name="action" value="regist"/>
	<input type="hidden" name="session" value="<?=$session ?>"/>
	<table width="98%" border="0" cellspacing="0" cellpadding="3" class="list" style="white-space:nowrap">
		<tr style="background-color:#f6f9ff">
			<td width="10%"><?=$font?>事業所</font></td>
			<td width="10%"><?=$font?>病棟</font></td>
			<td width="80%"><?=$font?>担当組織</font></td>
		</tr>

		<? foreach($relation_array as $idx => $row){ ?>
		<?   $bldg_cd = $row["bldg_cd"]; ?>
		<?   $ward_cd = $row["ward_cd"]; ?>
		<?   $cmb_id = "sel_" . $bldg_cd . "_" . $ward_cd; ?>
		<tr>
			<? // １列目：事業所マスタ -> 事業所名 ?>
			<td style="background-color:#f6f9ff"><?=$font?><?=$row["bldg_name"]?></font></td>
			<? // ２列目：病棟マスタ -> 病棟名 ?>
			<td style="background-color:#f6f9ff"><?=$font?><?=$row["ward_name"]?></font></td>
			<? // ３列目：リレーションデータのドロップダウン ?>
			<td><?=$font?>
				<?= $arr_class_name[0] ?>
				<select onchange="sel_changed('<?=$cmb_id?>', 1, this.value);" name="<?=$cmb_id?>_1" id="<?=$cmb_id?>_1"><?=$empty_option?></select>
				&nbsp;&nbsp;<?= $arr_class_name[1] ?>
				<select onchange="sel_changed('<?=$cmb_id?>', 2, this.value);" name="<?=$cmb_id?>_2" id="<?=$cmb_id?>_2"><?=$empty_option?></select>
				&nbsp;&nbsp;<?= $arr_class_name[2] ?>
				<select onchange="sel_changed('<?=$cmb_id?>', 3, this.value);" name="<?=$cmb_id?>_3" id="<?=$cmb_id?>_3"><?=$empty_option?></select>
				&nbsp;&nbsp;<?= $arr_class_name[3] ?>
				<select onchange="sel_changed('<?=$cmb_id?>', 4, this.value);" name="<?=$cmb_id?>_4" id="<?=$cmb_id?>_4" style="<?=$style_hidden?>"><?=$empty_option?></select>
				<script type="text/javascript">sel_changed("<?=$cmb_id?>", 0, "");</script>
				<script type="text/javascript">sel_changed("<?=$cmb_id?>", 1, "<?=$row["class_id"]?>");</script>
				<script type="text/javascript">sel_changed("<?=$cmb_id?>", 2, "<?=$row["atrb_id"]?>");</script>
				<script type="text/javascript">sel_changed("<?=$cmb_id?>", 3, "<?=$row["dept_id"]?>");</script>
				<script type="text/javascript">sel_changed("<?=$cmb_id?>", 4, "<?=$row["room_id"]?>");</script>
			</font></td>
		</tr>
		<? } ?>
	</table>

	<div style="width:98%; text-align:right; padding:2px"><input type="submit" value="登録"></div>
</form>




</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
