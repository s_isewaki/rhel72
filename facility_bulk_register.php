<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 設備予約｜一括登録</title>
<?
require("about_session.php");
require("about_authority.php");
require("facility_common.ini");
require("show_facility.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 設備予約権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 施設・設備登録権限を取得
$fcl_admin_auth = check_authority($session, 41, $fname);

// 表示範囲が渡されていない場合は「全て」に設定
if ($range == "") {
	$range = "all";
}

// タイムスタンプが渡されていない場合は当日日付のタイムスタンプを取得
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_reservable_categories($con, $emp_id, $fname);

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];

// ファイル入力ありの場合は登録処理を実行
$result = "";
if (array_key_exists("csvfile", $_FILES)) {
	$result = bulk_insert($con, $session, $emp_id, $cate_id, $facility_id, $_FILES["csvfile"], $fcl_admin_auth, $log, $fname);
}
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	for (var i = 0, j = document.mainform.cate_id.options.length; i < j; i++) {
		if (document.mainform.cate_id.options[i].value == '<? echo($cate_id); ?>') {
			document.mainform.cate_id.options[i].selected = true;
		}
	}
	cateOnChange('<? echo($facility_id); ?>');
}

function cateOnChange(facility_id) {

	// 施設・設備セレクトボックスのオプションを全削除
	deleteAllOptions(document.mainform.facility_id);

	// 施設・設備セレクトボックスのオプションを作成
	var category_id = document.mainform.cate_id.value;
<?
foreach ($categories as $tmp_category_id => $tmp_category) {
	echo("\tif (category_id == '$tmp_category_id') {\n");
	foreach ($tmp_category["facilities"] as $tmp_facility_id => $tmp_facility) {
		echo("\t\taddOption(document.mainform.facility_id, '$tmp_facility_id', '{$tmp_facility["name"]}', facility_id);\n");
	}
	echo("\t}\n");
}
?>

	// オプションが1件も作成されなかった場合
	var opt_cnt = document.mainform.facility_id.options.length;
	if (opt_cnt == 0) {
		addOption(document.mainform.facility_id, '0', '（なし）', facility_id);
	}

	// 施設・設備がなしの場合、登録ボタンを押下不可にする
	document.mainform.sbmt.disabled = (opt_cnt == 0);
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function checkForm(form) {
	if (form.csvfile.value == '') {
		alert('ファイルを選択してください。');
		return false;
	} else {
		return true;
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>"><img src="img/icon/b06.gif" width="32" height="32" border="0" alt="設備予約"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>"><b>設備予約</b></a></font></td>
<? if ($fcl_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="facility_master_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_menu.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_week.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週（表）</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_week_chart.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週（チャート）</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_month.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_year.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#5279a5"><a href="facility_bulk_register.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一括登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_option.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="mainform" action="facility_bulk_register.php" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this);">
<table width="600" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22" width="120" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設・設備：</font></td>
<td><select name="cate_id" onchange="cateOnChange();"><? show_category_options($categories, $cate_id); ?></select><select name="facility_id"></select></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル：</font></td>
<td><input name="csvfile" type="file" value="" size="50"></td>
</tr>
<tr>
<td height="22" colspan="2" align="right"><input type="submit" name="sbmt" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="range" value="<? echo($range); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
</form>
<? if ($result != "") { ?>
<table width="600" border="0" cellspacing="0" cellpadding="1">
<? } ?>
<? if ($result != "" && $result != "t") { ?>
<tr>
<td height="22" width="120"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">ファイルの内容に問題があるため、登録処理が実行できませんでした。</font></td>
<?
/*<br><?=$result?>*/
?>
</tr>
<? } else if ($result == "t") {?>
<tr>
<td height="22" width="120"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録処理が完了しました。</font></td>
</tr>
<? } ?>
<? if ($result != "") { ?>
</table>
<? } ?>
<?
if ($result == "t") {
	show_log($log);
}
?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="600" border="0" cellspacing="0" cellpadding="1">
<tr>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>CSVのフォーマット</b></font></td>
</tr>
<tr>
<td width="30"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<p style="margin:0;">下記項目をカンマで区切り、レコードごとに改行します。</p>
<ul style="margin:0;">
<li style="margin-left:40px;">日付･･･必須、YYYYMMDD形式</li>
<li style="margin-left:40px;">開始時刻･･･必須、HHMM形式（0000〜2355）</li>
<li style="margin-left:40px;">終了時刻･･･必須、HHMM形式（0005〜2400）</li>
<li style="margin-left:40px;">タイトル･･･必須</li>
<li style="margin-left:40px;">内容･･･改行は「\n」で表現</li>
<li style="margin-left:40px;">利用人数･･･入力する場合は半角数字1〜4桁</li>
</ul>
<dl style="margin:0;">
<dt>例：</dt>
<dd style="margin-left:20px;font-family:monospace;">20041221,1830,2000,○○委員会,定例会議\n机はコの字,20</dd>
<dd style="margin-left:20px;font-family:monospace;">20041222,1315,1515,××会議,人数未定,</dd>
</dl>
</font></td>
</tr>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
// カテゴリオプションを出力
function show_category_options($categories, $cate_id) {
	foreach ($categories as $tmp_category_id => $tmp_category) {
		echo("<option value=\"$tmp_category_id\">{$tmp_category["name"]}\n");
	}
}

// 登録処理
function bulk_insert($con, $session, $emp_id, $cate_id, $facility_id, $file_info, $fcl_admin_auth, &$log, $fname) {

	$uploaded = false;
	if ($file_info["error"] == 0 && $file_info["size"] > 0) {
		$uploaded = true;
		$log = array();
	}

	// 正常にアップロードされた場合、登録処理を実行
	if ($uploaded) {
		// アクセスログ
		aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);
		
		$result = "t";
		$line_cnt = 0;

		// 予約可能期間を取得
		list($start_date, $end_date) = get_term($con, $fname, $facility_id, $emp_id);

		// ファイルの読み込み
		$lines = file($file_info["tmp_name"]);

		// トランザクションの開始
		pg_query($con, "begin transaction");

		// 1行分ずつinsert
		foreach ($lines as $line) {
			$line_cnt++;
			$line = mb_convert_encoding($line, "EUC-JP", "auto");
			$values = split(",", trim($line));
			if (count($values) != 6) {
				$result = "f";
				pg_query($con, "rollback");
				break;
			}

			// 日付の妥当性チェック
			$year = substr($values[0], 0, 4);
			$month = substr($values[0], 4, 2);
			$day = substr($values[0], 6, 2);
			if (!checkdate($month, $day, $year)) {
				$result = "f";
//				$result = $line_cnt."行目の日付が正しくありません。";
				pg_query($con, "rollback");
				break;
			}

			// 予約可能期間チェック
			if ($values[0] < $start_date || $values[0] > $end_date) {
				$result = "f";
				pg_query($con, "rollback");
				break;
			}

			// 時刻の妥当性チェック
			$arr_minute = array();
			for ($i = 0; $i < 60; $i += 5) {
				$arr_minute[] = sprintf("%02d", $i);
			}
			$s_hour = substr($values[1], 0, 2);
			$s_time = substr($values[1], 0, 2).substr($values[1], 2, 2);
			if (!("0000" <= $s_time && $s_time <= "2355")) {
				$result = "f";
				pg_query($con, "rollback");
				break;
			}
			$s_minute = substr($values[1], 2, 2);
			if (!in_array($s_minute, $arr_minute)) {
				$result = "f";
				pg_query($con, "rollback");
				break;
			}
			$e_hour = substr($values[2], 0, 2);
			$e_time = substr($values[2], 0, 2).substr($values[2], 2, 2);
			if (!("0005" <= $e_time && $e_time <= "2400")) {
				$result = "f";
				pg_query($con, "rollback");
				break;
			}
			$e_minute = substr($values[2], 2, 2);
			if (!in_array($e_minute, $arr_minute)) {
				$result = "f";
				pg_query($con, "rollback");
				break;
			}
			if ($values[1] >= $values[2]) {
				$result = "f";
				pg_query($con, "rollback");
				break;
			}

			// タイトルのチェック
			if ($values[3] == "") {
				$result = "f";
				pg_query($con, "rollback");
				break;
			}
			if (strlen($values[3]) > 100) {
				$result = "f";
				pg_query($con, "rollback");
				break;
			}

			// 利用人数のチェック
			if ($values[5] != "") {
				if (preg_match("/^[0-9]{1,4}$/", $values[5]) == 0) {
					$result = "f";
					pg_query($con, "rollback");
					break;
				}
			}

			// 日時の重複する予約件数を取得
			$sql = "select count(*) from reservation";
			$cond = "where fclcate_id = $cate_id and facility_id = $facility_id and date = '{$values[0]}' and not (start_time >= '{$values[2]}' or end_time <= '{$values[1]}') and reservation_del_flg = 'f'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_query("rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$dup_count = pg_fetch_result($sel, 0, 0);

			// 重複しない場合
			if ($dup_count == 0) {

				// 「内容」の「\n」を改行に変換
				$values[4] = str_replace("\\n", "\n", $values[4]);

				// 予約IDを採番
				$reservation_id = get_reservation_id($con, $fname);

				// 登録処理
				$sql = "insert into reservation (reservation_id, fclcate_id, facility_id, emp_id, date, start_time, end_time, title, content, users) values (";
				$content = array($reservation_id, $cate_id, $facility_id, $emp_id, $values[0], $values[1], $values[2], $values[3], $values[4], $values[5]);
				$ins = insert_into_table($con, $sql, $content, $fname);
				if ($ins == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}

				// ログ作成
				array_push($log, array("$year/$month/$day $s_hour:{$s_minute}〜{$e_hour}:$e_minute", "成功"));

			// 重複する場合
			} else {

				// ログ作成
				array_push($log, array("$year/$month/$day $s_hour:{$s_minute}〜{$e_hour}:$e_minute", "失敗（すでに予約あり）"));
			}
		}

		// トランザクションのコミット
		if ($result == "t") {
			pg_query($con, "commit");
		}
	} else {
		$result = "f";
	}

	return $result;

}

// ログを表示
function show_log($log) {

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">\n");
	echo("<tr>\n");
	echo("<td colspan=\"3\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><b>登録結果</b></font></td>\n");
	echo("</tr>\n");
	echo("<tr bgcolor=\"#f6f9ff\">\n");
	echo("<td height=\"22\" width=\"30\"></td>\n");
	echo("<td width=\"200\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日時</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">結果</font></td>\n");
	echo("</tr>\n");

	foreach ($log as $row) {
		echo("<tr>\n");
		echo("<td height=\"22\" width=\"30\"></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$row[0]}</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$row[1]}</font></td>\n");
		echo("</tr>\n");
	}

	echo("</table>\n");

}

// 設備の予約可能期間取得 list(開始日, 終了日)を返す 
function get_term($con, $fname, $facility_id, $emp_id) {
	// 設備の管理者設定を確認
	$sql = "select emp_class,emp_attribute,emp_dept,emp_st from empmst";
	$cond = "where emp_id='$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$emp_class = pg_fetch_result($sel, 0, "emp_class");
	$emp_attribute = pg_fetch_result($sel, 0, "emp_attribute");
	$emp_dept = pg_fetch_result($sel, 0, "emp_dept");
	$emp_st = pg_fetch_result($sel, 0, "emp_st");

	$sql = "select a.fclcate_id,a.has_admin_flg from facility a";
	$cond = "where a.facility_del_flg = 'f' and a.facility_id = $facility_id";
		$cond .= " and";
			$cond .= " (";
				$cond .= " exists(select * from fcladmin where emp_id = '$emp_id' and facility_id = $facility_id)";
				$cond .= " or";
				$cond .= " (";
					$cond .= " ((a.admin_dept_flg = '1' and a.facility_id = $facility_id) or (admin_dept_flg = '2' and exists(select * from fcladmindept where class_id = $emp_class and atrb_id = $emp_attribute and dept_id = $emp_dept and a.facility_id = $facility_id)))";
					$cond .= " and";
					$cond .= " ((a.admin_st_flg = '1' and a.facility_id = $facility_id) or (admin_st_flg = '2' and exists(select * from fcladminst where st_id = $emp_st and a.facility_id = $facility_id)))";
				$cond .= ") or has_admin_flg = 'f'";
			$cond .= ")";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$has_admin_flg = pg_fetch_result($sel, 0, "has_admin_flg");
	$fclcate_id = pg_fetch_result($sel, 0, "fclcate_id");
	// 設備の管理者設定をしないの場合は、カテゴリをチェック
	if ($has_admin_flg == "f") {
		$sql = "select a.has_admin_flg from fclcate a";
		$cond = "where a.fclcate_del_flg = 'f' and a.fclcate_id = $fclcate_id";
			$cond .= " and";
				$cond .= " (";
					$cond .= " exists(select * from fclcateadmin where emp_id = '$emp_id' and fclcate_id = $fclcate_id)";
					$cond .= " or";
					$cond .= " (";
						$cond .= " ((a.admin_dept_flg = '1' and a.fclcate_id = $fclcate_id) or (admin_dept_flg = '2' and exists(select * from fclcateadmindept where class_id = $emp_class and atrb_id = $emp_attribute and dept_id = $emp_dept and fclcate_id = $fclcate_id)))";
						$cond .= " and";
						$cond .= " ((a.admin_st_flg = '1' and a.fclcate_id = $fclcate_id) or (admin_st_flg = '2' and exists(select * from fclcateadminst where st_id = $emp_st and fclcate_id = $fclcate_id)))";
					$cond .= ") or has_admin_flg = 'f'";
				$cond .= ")";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$has_admin_flg = pg_fetch_result($sel, 0, "has_admin_flg");
	}
	// 管理者設定がある場合は、期限なしとなる値を返す
	if ($has_admin_flg == "t") {
		$start_date = "00000000";
		$end_date = "99999999";
		return array($start_date, $end_date);
	}
	$sql = "select facility.facility_name, facility.start_date as fcl_start_date, facility.end_date_flg as fcl_end_date_flg, facility.end_date as fcl_end_date, facility.end_month as fcl_end_month, fclcate.start_date as cate_start_date, fclcate.end_date_flg as cate_end_date_flg, fclcate.end_date as cate_end_date, fclcate.end_month as cate_end_month from facility inner join fclcate on facility.fclcate_id = fclcate.fclcate_id";
	$cond = "where facility.facility_id = $facility_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$facility_name = pg_fetch_result($sel, 0, "facility_name");
	$fcl_start_date = pg_fetch_result($sel, 0, "fcl_start_date");
	$fcl_end_date_flg = pg_fetch_result($sel, 0, "fcl_end_date_flg");
	$fcl_end_date = pg_fetch_result($sel, 0, "fcl_end_date");
	$fcl_end_month = pg_fetch_result($sel, 0, "fcl_end_month");
	$cate_start_date = pg_fetch_result($sel, 0, "cate_start_date");
	$cate_end_date_flg = pg_fetch_result($sel, 0, "cate_end_date_flg");
	$cate_end_date = pg_fetch_result($sel, 0, "cate_end_date");
	$cate_end_month = pg_fetch_result($sel, 0, "cate_end_month");

	if ($fcl_start_date != "") {
		$start_date = $fcl_start_date;
	}
	if ($fcl_end_date_flg == "1") {
		if ($fcl_end_date != "") {
			$end_date = $fcl_end_date;
		}
	} else {
		$end_month = $fcl_end_month;
	}
	// 設備が未設定の場合、カテゴリから取得
	if ($start_date == "" && $end_date == "") {
		if ($cate_start_date != "") {
			$start_date = $cate_start_date;
		}
		if ($cate_end_date_flg == "1") {
			if ($cate_end_date != "") {
				$end_date = $cate_end_date;
			}
		} else {
			$end_month = $cate_end_month;
		}
	}
	if ($start_date == "") {
		$start_date = "00000000";
	}
	// 期限月対応
	if ($end_date == "") {
		if ($end_month == "") {
			$end_date = "99999999";
		} else {
			$end_date = date("Ymd", strtotime($end_month . " month"));
		}
	}
	return array($start_date, $end_date);
}

// 日付をタイムスタンプに変換
function to_timestamp($yyyymmdd) {
	$y = substr($yyyymmdd, 0, 4);
	$m = substr($yyyymmdd, 4, 2);
	$d = substr($yyyymmdd, 6, 2);
	return mktime(0, 0, 0, $m, $d, $y);
}
