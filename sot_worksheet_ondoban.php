<?
// このスクリプトは以下で検証されました。
//   IE6 IE8 Opera10.6 Chrome5.0 Safari5.0 Firefox3.5 IPad(Safari)
// paddingの捉え方などがブラウザによって違うため、罫線など幾分ずれます。
// IE8ではExplorerCanvasが動作しないため、metaタグでIE7互換で動かしています。

$THIS_WORKSHEET_ID = 1;
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("sot_util.php");
require_once("summary_common.ini");
require_once("get_values.php");
require_once("get_menu_label.ini");


//====================================================================
// ユーティリティ
//====================================================================
$global_index = 0;
function wbr($s,$s2=""){
	$s = trim($s);
	$s2 = trim($s2);
	if (!mb_strlen($s) && !mb_strlen($s2)) return "";
	return wrapByUniqueDiv(wordBreak($s).$s2);
}

function wordBreak($s){
	$out = array();
	for ($i=0; $i<mb_strlen($s); $i++){
		$out[] = h(mb_substr($s, $i, 1));
	}
	return implode("<wbr>", $out);
}

function wrapByUniqueDiv($s){
	global $global_index;
	$global_index++;
	return "<div style='overflow:hidden; font-size:13px; display:none' id='div".$global_index."'>".$s."</div>";
}

//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $_SERVER["PHP_SELF"];
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
	echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
	exit;
}
$emp_id = get_emp_id($con, $session, $fname);//ログイン職員ＩＤ
summary_common_write_operate_log("access", $con, $fname, $session, $emp_id, "", "");// ログ


//=================================================
// 検索対象日付がない場合は本日を設定
//=================================================
$date_y = (int)(@$_REQUEST["date_y"] ? $_REQUEST["date_y"] : date("Y"));
$date_m = (int)(@$_REQUEST["date_m"] ? $_REQUEST["date_m"] : date("m"));
$date_d = (int)(@$_REQUEST["date_d"] ? $_REQUEST["date_d"] : date("d"));
if (!checkdate($date_m, $date_d, $date_y)) list($date_y, $date_m, $date_d) = split("/", date("Y/m/d", strtotime("today")));
$target_yyyymmdd = $date_y. ((int)@$date_m <10 ? "0".(int)@$date_m : $date_m) . ((int)@$date_d <10 ? "0".(int)@$date_d : $date_d);


//=================================================
// 病棟などが指定されていなければデフォルトを設定する
//=================================================
if (@$apply_date_default && @$sel_bldg_cd=="" && @$sel_ward_cd==""){
	$sql =
		" select swer.bldg_cd, swer.ward_cd from sot_ward_empclass_relation swer".
		" inner join empmst emp on (".
		"   emp.emp_class = swer.class_id".
		"   and emp.emp_attribute = swer.atrb_id".
		"   and emp.emp_dept = swer.dept_id".
		"   and emp.emp_id = '".pg_escape_string($emp_id)."'".
		" )";
	$emp_ward = @$c_sot_util->select_from_table($sql);
	$_REQUEST["sel_bldg_cd"] = @pg_fetch_result($emp_ward, 0, "bldg_cd");
	$_REQUEST["sel_ward_cd"] = @pg_fetch_result($emp_ward, 0, "ward_cd");
	$sel_bldg_cd= $_REQUEST["sel_bldg_cd"];
	$sel_ward_cd= $_REQUEST["sel_ward_cd"];
}
$sel_team_id = (int)@$_REQUEST["sel_team_id"];

$border_color = @$_REQUEST["print_preview"] ? "#999999" : "#9bc8ec";

$sql = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst where ptif_id = '".pg_escape_string(@$_REQUEST["sel_ptif_id"])."'";
$sel = select_from_table($con, $sql, "", $fname);
$ptif_name = trim(@pg_fetch_result($sel, 0, "ptif_lt_kaj_nm") . " " . @pg_fetch_result($sel, 0, "ptif_ft_kaj_nm"));

$nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, @$_REQUEST["sel_ptif_id"], $target_yyyymmdd);
$ptif_ward_cd = @$nyuin_info["ward_cd"];
$ward_name = @$nyuin_info["ward_name"];

//=================================================
// 対象日付リストを作成する。本日を右端にして七日間。
// $ymdには "[1](６日前)" 〜 "[7](指定日)" まで格納
//=================================================
$ymd = array();
$care = array();
$weeks = array("日", "月", "火", "水", "木", "金", "土");
$slashed_target_ymd = $c_sot_util->slash_yyyymmdd($target_yyyymmdd);
$date_y_s = "";
$date_m_s = "";
$date_d_s = "";
for ($i = 0; $i <= 7; $i++){
	$t = strtotime($slashed_target_ymd." -".$i." day");
	$d = date("Ymd", $t);
	$week = date("w", $t);
	$ymd[$d] = 7-$i;
	$care[7-$i]["yyyymmdd"] = $d;
	$care[7-$i]["date"] = substr($c_sot_util->slash_ymd($d), 5) . "（".$weeks[$week]."）"; // 月日と曜日文字列
	if ($i==6) $date_y_s = (int)date("Y", $t);
	if ($i==6) $date_m_s = (int)date("m", $t);
	if ($i==6) $date_d_s = (int)date("d", $t);
}

//==============================================================================
// オーダが出ている処置の名前の取得
// 入院期間開始から、表示日付けの期間まで
//==============================================================================

// 対象患者の入院情報（表示７日中の最初の日）
$nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, @$_REQUEST["sel_ptif_id"], $care[1]["yyyymmdd"]);
$nyuin_start_day = @$nyuin_info["inpt_in_dt"];
if (!$nyuin_start_day) $nyuin_start_day = $care[1]["yyyymmdd"];


//テンプレートの日本語、つけあわせはどこにも無い。固定でもつ。
$tmpl_names = array();
$tmpl_names["joks"] = "褥創";
$tmpl_names["nank"] = "軟膏処置";
$tmpl_names["sosh"] = "創処置";
$tmpl_names["teng"] = "点眼";

$sql =
	" select".
	" assign_pattern".
	",assign_sun_flg".
	",assign_mon_flg".
	",assign_tue_flg".
	",assign_wed_flg".
	",assign_thu_flg".
	",assign_fri_flg".
	",assign_sat_flg".
	",start_ymd".
	",tmpl_chapter_code".
	" from sot_summary_schedule".
	" where start_ymd >= ". $nyuin_start_day.
	" and ptif_id = '" . pg_escape_string(@$_REQUEST["sel_ptif_id"]) ."'".
	" and end_ymd >= ".$care[1]["yyyymmdd"].
  " and temporary_update_flg = 0".
  " and delete_flg = false".
  " and times_per_ymd > 0".
  " and tmpl_chapter_code in ('joks','nank','sosh','teng')".
  " order by start_ymd, regist_timestamp";
$sel = $c_sot_util->select_from_table($sql);
$order_names = array();
while($row = pg_fetch_array($sel)){
	// 曜日つきは曜日がひとつでもチェックされているか
	$is_enable = 1;
	if ($row["assign_pattern"] == "WEEK_OVERRIDE"){
		if (!$row["assign_sun_flg"] && !$row["assign_mon_flg"] && !$row["assign_tue_flg"] &&
				!$row["assign_wed_flg"] && !$row["assign_thu_flg"] && !$row["assign_fri_flg"] && !$row["assign_sat_flg"]){
			$is_enable = 0;
		}
	}
	// グラフ初日以前であれば、有効な最新を取得する。
	if ($row["start_ymd"] < $care[1]["yyyymmdd"]) $order_names[trim($row["tmpl_chapter_code"])] = $is_enable;
	// グラフ初日から７日のものは、すべて有効である。
	else if ($is_enable) $order_names[trim($row["tmpl_chapter_code"])] = 1;
}

//==============================================================================
// 温度板バイタル（グラフ情報）の収集、
// バイタルはXMLを見ないでも良いように、「ナンデモテーブル」に格納されている。
//==============================================================================
$sql =
	" select * from (".
	" select".
	" sss.apply_id". // 承認ID
	",sss.start_ymd". // オーダ対象日
	",sssv.string2". // 収縮期血圧
	",sssv.string3". // 拡張時血圧
	",sssv.string4". // 脈拍数
	",sssv.string5". // 呼吸数
	",sssv.string6". // 体温
	",sssv.string7". // SPO2
	",sssv.string8". // 脈拍数マーカ
	",coalesce(sssv.int1,0) as int1".    // 測定時
	",coalesce(sssv.int2,0) as int2".    // 測定分
	" from sot_summary_schedule sss".
	" inner join sot_summary_schedule_various sssv on (sssv.schedule_seq = sss.schedule_seq)".
	" where sss.tmpl_file_name = 'tmpl_OndobanVital.php'".
	" and sss.delete_flg = false".
	" and sss.temporary_update_flg = 0".
	" and sss.ptif_id = '".pg_escape_string(@$_REQUEST["sel_ptif_id"])."'".
	" and sss.start_ymd between ".$care[1]["yyyymmdd"]." and ".$care[7]["yyyymmdd"].
	" ) d".
	" order by start_ymd, int1, int2";

$sel = $c_sot_util->select_from_table($sql);
$data = (array)pg_fetch_all($sel);

$graphP = array(); // 脈拍数
$graphT = array(); // 体温
$graphR = array(); // 呼吸数
$graphS = array(); // SPO2
$graphBP = array(); // 血圧

$tooltipData = array();

$vital = array();
foreach($data as $key => $row){
	if (!$row) break;
	$day_index = $ymd[$row["start_ymd"]];
	$vital[$day_index][] = $row;

	$hour =   (int)trim($row["int1"]); // 測定時
	$minute = (int)trim($row["int2"]); // 測定分

	$valBPs = trim($row["string2"]); // 収縮期血圧 (最小40)
	$valBPk = trim($row["string3"]); // 拡張時血圧 (最大240)
	$valP =   trim($row["string4"]); // 脈拍数 (10-210)
	$valR =   trim($row["string5"]); // 呼吸数 (5-55)
	$valT =   trim($row["string6"]); // 体温 (33-43度)
	$valS =   trim($row["string7"]); // SPO2 (30-100％)
	$valPM =  trim($row["string8"]); // 脈拍数マーカ（空文字または「x」）

	$graph_day = ((int)$day_index-1)*24*60 + $hour*60 + $minute; // グラフ上の横位置（分単位）

	$toolmsg = array();

	//--------------------------
	// 血圧
	//--------------------------
	if ($valBPs!="" || $valBPk!=""){
		$valBPs = (int)$valBPs;
		$valBPk = (int)$valBPk;
		if ($valBPs < 240 && $valBPk > 40 && $valBPs < $valBPk) {
			$graphBP[] = $graph_day."-".$valBPs."_".$valBPk;
			$toolmsg["血圧"] = $valBPk." 〜 ".$valBPs;
		} else if ($valBPk < 240 && $valBPs > 40 && $valBPk < $valBPs) {
			$graphBP[] = $graph_day."-".$valBPk."_".$valBPs;
			$toolmsg["血圧"] = $valBPs." 〜 ".$valBPk;
		}
	}
	//--------------------------
	// 脈拍
	//--------------------------
	if ($valP!=""){
		$valP = (float)$valP;
		if ($valP < 10) $valP = 10;
		if ($valP > 210) $valP = 210;
		$graphP[] = $graph_day."-".$valP."-".$valPM;
		$toolmsg["脈拍"] = $valP;
	}
	//--------------------------
	// 呼吸数
	//--------------------------
	if ($valR!=""){
		$valR = (float)$valR;
		if ($valR < 5) $valR = 5;
		if ($valR > 55) $valR = 55;
		$graphR[] = $graph_day."-".$valR;
		$toolmsg["呼吸数"] = $valR;
	}
	//--------------------------
	// 体温
	//--------------------------
	if ($valT!=""){
		$valT = (float)$valT;
		if ($valT < 33) $valT = 33;
		if ($valT > 43) $valT = 43;
		$graphT[] = $graph_day."-".$valT;
		$toolmsg["体温"] = $valT;
	}
	//--------------------------
	// SPO2
	//--------------------------
	if ($valS!=""){
		$valS = (float)$valS;
		if ($valS < 30) $valS = 30;
		if ($valS > 100) $valS = 100;
		$graphS[] = $graph_day."-".$valS;
		$toolmsg["SPO2"] = $valS;
	}
	//--------------------------
	// ツールチップ用
	//--------------------------
	$tooltipData[] = array(
		"left" => $graph_day,
		"datetime" => $c_sot_util->slash_ymd($row["start_ymd"]) . " " . (int)$hour .":" . sprintf("%02d",$minute),
		"msg" => $toolmsg
	);
}
//==============================================================================
// 温度板酸素吸入（グラフ情報）の収集、
// これらもXMLを見ないでも良いように、「ナンデモテーブル」に格納されている。
//==============================================================================
$sql =
	" select".
	" sssv.int1". // 開始日(YYYYMMDD) オーダ対象日に、48時間制で指定された増加を含めたもの
	",sssv.int2". // 終了日(YYYYMMDD)
	",sssv.int3". // 開始分 0から1440分以内であるはず
	",sssv.int4". // 終了分 0から1440分以内であるはず
	",sssv.string2". // チャプター名（酸素吸入または心電図モニターまたは呼吸器）
	",sssv.string3". // 酸素吸入のリットル表示
	",sssv.string4". // 終了日が登録されていなければ"y"
	" from sot_summary_schedule sss".
	" inner join sot_summary_schedule_various sssv on (".
	"   sssv.schedule_seq = sss.schedule_seq".
	"   and string2 ='ocar_sanso_ji'".
	" )".
	" where sss.tmpl_file_name = 'tmpl_OndobanCare.php'".
	" and sss.delete_flg = false".
	" and sss.temporary_update_flg = 0".
	" and sss.ptif_id = '".pg_escape_string(@$_REQUEST["sel_ptif_id"])."'".
	" and sssv.int1 <= ".$care[7]["yyyymmdd"]. // スケジュールの開始日は、画面最右列の日付けよりは過去
	" and sssv.int2 >= ".$care[1]["yyyymmdd"]. // スケジュールの終了日は、画面最左列の日付けよりは未来
	" order by int1, int3, schedule_various_seq";

$sel = $c_sot_util->select_from_table($sql);
$data = (array)pg_fetch_all($sel);

$gSK = array(); // 酸素吸入 データ並べ替えのための一時的なもの
$gSM = array(); // 心電図モニター データ並べ替えのための一時的なもの
$gKK = array(); // 呼吸器 データ並べ替えのための一時的なもの


foreach($data as $key => $row){
	if (!$row) break;

	// ソート用文字列の作成（YYYYMMDD@[4桁の分数])
	$from_key = $row["int1"] ."@". substr("0000".(int)$row["int3"], -4);
	$to_key =   $row["int2"] ."@". substr("0000".(int)$row["int4"], -4);

	$from_day_index = @$ymd[$row["int1"]];
	$to_day_index   = @$ymd[$row["int2"]];

	// 開始日がグラフの外なら、グラフの左端となるようにする
	$from_hm = $row["int3"];
	$is_from_overflow = "n";
	if (!$from_day_index) {
		$from_day_index = 1;
		$from_hm = 0;
		$is_from_overflow = "y";
	}

	// 終了日がグラフの外なら、グラフの右端となるようにする
	$to_hm = $row["int4"];
	$is_to_overflow = "n";
	if (!$to_day_index) {
		$to_day_index = 7;
		$to_hm = 1440;
		$is_to_overflow = "y";
	}

	$ratio_from = (($from_day_index-1)*24*60) + $from_hm;
	$ratio_to   = (($to_day_index-1)*24*60) + $to_hm;

	// 酸素吸入をいったんならべる

	// ひとつ前の終了日時とかぶってないか比較、
	// かぶっている場合はひとつ前のデータの終了日を修正
	$c = count($gSK);
	$next_idx = $c;
	if ($c > 0 && $gSK[$c-1]["to_key"] > $from_key){
		$gSK[$c-1]["ratio_to"] = $ratio_from;
		if (!@$ymd[$row["int1"]]) $next_idx = $c-1;
	}
	$gSK[$next_idx] = array(
		"to_key"=>$to_key,
		"is_from_overflow" => $is_from_overflow,
		"is_to_overflow" => $is_to_overflow,
		"ratio_from"=> $ratio_from,
		"ratio_to"=> $ratio_to,
		"litter"=>$row["string3"],
		"not_ended"=>$row["string4"]
	);
}
// 酸素吸入のURL文字列を作成
$graphSK = array();
foreach($gSK as $idx => $d){
	$graphSK[] = $d["is_from_overflow"]."-".$d["is_to_overflow"]."-".$d["ratio_from"]."-".$d["ratio_to"]."-".$d["not_ended"]."-".$d["litter"];
}


//==============================================================================
// 心電図モニター（グラフ情報）の収集、
// これらもXMLを見ないでも良いように、「ナンデモテーブル」に格納されている。
//==============================================================================

// 心電図モニターはずっと昔に開始されっぱなし・終了日無限」だけを指定している場合がある。
// このため、表示範囲手前のデータを一件取得しておく。
// スケジュールの開始日は、画面最左列の日付けの前日より過去
$sql =
	" select max(sssv.schedule_various_seq)".
	" from sot_summary_schedule sss".
	" inner join sot_summary_schedule_various sssv on (sssv.schedule_seq = sss.schedule_seq)".
	" where sss.tmpl_file_name = 'tmpl_OndobanCare.php'".
	" and sss.delete_flg = false".
	" and sss.temporary_update_flg = 0".
	" and sss.ptif_id = '".pg_escape_string(@$_REQUEST["sel_ptif_id"])."'".
	" and sssv.string2 = 'ocar_sinden'".
	" and sssv.int1 <= ".$care[0]["yyyymmdd"].
	" and sssv.int2 > ".$care[0]["yyyymmdd"];

$sel = $c_sot_util->select_from_table($sql);
$last_schedule_various_seq = (int)@pg_fetch_result($sel, 0, 0);
if (!$last_schedule_various_seq) $last_schedule_various_seq = 99999999;

// 以下を検索
// ・期間内に開始した場合
// ・期間初日以降に終了する場合
// ・または、過去に開始された直近レコード
$sql =
	" select".
	" sssv.int1". // 開始日(YYYYMMDD) オーダ対象日に、48時間制で指定された増加を含めたもの
	",sssv.int2". // 終了日(YYYYMMDD)
	",sssv.int3". // 開始分 0から1440分以内であるはず
	",sssv.int4". // 終了分 0から1440分以内であるはず
	",sssv.string2". // チャプター名（酸素吸入または心電図モニターまたは呼吸器）
	",sssv.string3". // 酸素吸入のリットル表示
	",sssv.string4". // 終了日が登録されていなければ"y"
	" from sot_summary_schedule sss".
	" inner join sot_summary_schedule_various sssv on (".
	"   sssv.schedule_seq = sss.schedule_seq".
	"   and string2 = 'ocar_sinden'".
	" )".
	" where sss.tmpl_file_name = 'tmpl_OndobanCare.php'".
	" and sss.delete_flg = false".
	" and sss.temporary_update_flg = 0".
	" and sss.ptif_id = '".pg_escape_string(@$_REQUEST["sel_ptif_id"])."'".
	" and (".
	"   (sssv.int1 <= ".$care[7]["yyyymmdd"]. " and sssv.int1 >= ".$care[1]["yyyymmdd"] .")" .// スケジュールの開始日は、画面表示期間内
	"   or".
	"   (sssv.schedule_various_seq = ".$last_schedule_various_seq.")". // 直近最終レコードを含める
	" )".
	" and sssv.int2 >= ".$care[1]["yyyymmdd"].
	" order by int1, int3, schedule_various_seq";

$sel = $c_sot_util->select_from_table($sql);
$data = (array)pg_fetch_all($sel);

$gSM = array(); // 心電図モニター 並べ替えるための一時的なもの
foreach($data as $key => $row){
	if (!$row) break;

	// ソート用文字列の作成（YYYYMMDD@[4桁の分数])
	$from_key = $row["int1"] ."@". substr("0000".(int)$row["int3"], -4);
	$to_key =   $row["int2"] ."@". substr("0000".(int)$row["int4"], -4);

	$from_day_index = @$ymd[$row["int1"]];
	$to_day_index   = @$ymd[$row["int2"]];

	// 開始日がグラフの外なら、グラフの左端となるようにする
	$from_hm = $row["int3"];
	$is_from_overflow = "n";
	if (!$from_day_index) {
		$from_day_index = 1;
		$from_hm = 0;
		$is_from_overflow = "y";
	}

	// 終了日がグラフの外なら、グラフの右端となるようにする
	$to_hm = $row["int4"];
	$is_to_overflow = "n";
	if (!$to_day_index) {
		$to_day_index = 7;
		$to_hm = 1440;
		$is_to_overflow = "y";
	}

	$ratio_from = (($from_day_index-1)*24*60) + $from_hm;
	$ratio_to   = (($to_day_index-1)*24*60) + $to_hm;

	// 心電図モニターをいったんならべる
	// ひとつ前の終了日時とかぶってないか比較、
	// かぶっている場合はひとつ前のデータの終了日を修正
	$c = count($gSM);
	$next_idx = $c;
	if ($c > 0 && $gSM[$c-1]["to_key"] > $from_key){
		$gSM[$c-1]["ratio_to"] = $ratio_from;
		if (!@$ymd[$row["int1"]]) $next_idx = $c-1;
	}

	$gSM[$next_idx] = array(
		"to_key"=>$to_key,
		"is_from_overflow" => $is_from_overflow,
		"is_to_overflow" => $is_to_overflow,
		"ratio_from"=> $ratio_from,
		"ratio_to"=> $ratio_to,
		"not_ended"=>$row["string4"]
	);
}

// 心電図モニターのURL文字列を作成
$graphSM = array();
foreach($gSM as $idx => $d){
	$graphSM[] = $d["is_from_overflow"]."-".$d["is_to_overflow"]."-".$d["ratio_from"]."-".$d["ratio_to"]."-".$d["not_ended"];
}



//==============================================================================
// 呼吸器（グラフ情報）の収集、
// やっていることは心電図モニターとまったく同様
//==============================================================================
$sql =
	" select max(sssv.schedule_various_seq)".
	" from sot_summary_schedule sss".
	" inner join sot_summary_schedule_various sssv on (sssv.schedule_seq = sss.schedule_seq)".
	" where sss.tmpl_file_name = 'tmpl_OndobanCare.php'".
	" and sss.delete_flg = false".
	" and sss.temporary_update_flg = 0".
	" and sss.ptif_id = '".pg_escape_string(@$_REQUEST["sel_ptif_id"])."'".
	" and sssv.string2 = 'ocar_kokyuki'".
	" and sssv.int1 <= ".$care[0]["yyyymmdd"].
	" and sssv.int2 > ".$care[0]["yyyymmdd"];

$sel = $c_sot_util->select_from_table($sql);
$last_schedule_various_seq = (int)@pg_fetch_result($sel, 0, 0);
if (!$last_schedule_various_seq) $last_schedule_various_seq = 99999999;

// 以下を検索
// ・期間内に開始した場合
// ・期間初日以降に終了する場合
// ・または、過去に開始された直近レコード
$sql =
	" select".
	" sssv.int1". // 開始日(YYYYMMDD) オーダ対象日に、48時間制で指定された増加を含めたもの
	",sssv.int2". // 終了日(YYYYMMDD)
	",sssv.int3". // 開始分 0から1440分以内であるはず
	",sssv.int4". // 終了分 0から1440分以内であるはず
	",sssv.string2". // チャプター名（酸素吸入または心電図モニターまたは呼吸器）
	",sssv.string3". // 酸素吸入のリットル表示
	",sssv.string4". // 終了日が登録されていなければ"y"
	" from sot_summary_schedule sss".
	" inner join sot_summary_schedule_various sssv on (".
	"   sssv.schedule_seq = sss.schedule_seq".
	"   and string2 = 'ocar_kokyuki'".
	" )".
	" where sss.tmpl_file_name = 'tmpl_OndobanCare.php'".
	" and sss.delete_flg = false".
	" and sss.temporary_update_flg = 0".
	" and sss.ptif_id = '".pg_escape_string(@$_REQUEST["sel_ptif_id"])."'".
	" and (".
	"   (sssv.int1 <= ".$care[7]["yyyymmdd"]. " and sssv.int1 >= ".$care[1]["yyyymmdd"] .")" .// スケジュールの開始日は、画面表示期間内
	"   or".
	"   (sssv.schedule_various_seq = ".$last_schedule_various_seq.")". // 直近最終レコードを含める
	" )".
	" and sssv.int2 >= ".$care[1]["yyyymmdd"].
	" order by int1, int3, schedule_various_seq";

$sel = $c_sot_util->select_from_table($sql);
$data = (array)pg_fetch_all($sel);

foreach($data as $key => $row){
	if (!$row) break;

	// ソート用文字列の作成（YYYYMMDD@[4桁の分数])
	$from_key = $row["int1"] ."@". substr("0000".(int)$row["int3"], -4);
	$to_key =   $row["int2"] ."@". substr("0000".(int)$row["int4"], -4);

	$from_day_index = @$ymd[$row["int1"]];
	$to_day_index   = @$ymd[$row["int2"]];

	// 開始日がグラフの外なら、グラフの左端となるようにする
	$from_hm = $row["int3"];
	$is_from_overflow = "n";
	if (!$from_day_index) {
		$from_day_index = 1;
		$from_hm = 0;
		$is_from_overflow = "y";
	}

	// 終了日がグラフの外なら、グラフの右端となるようにする
	$to_hm = $row["int4"];
	$is_to_overflow = "n";
	if (!$to_day_index) {
		$to_day_index = 7;
		$to_hm = 1440;
		$is_to_overflow = "y";
	}

	$ratio_from = (($from_day_index-1)*24*60) + $from_hm;
	$ratio_to   = (($to_day_index-1)*24*60) + $to_hm;


	// 呼吸器をいったんならべる
	// ひとつ前の終了日時とかぶってないか比較、
	// かぶっている場合はひとつ前のデータの終了日を修正
	$c = count($gKK);
	$next_idx = $c;
	if ($c > 0 && $gKK[$c-1]["to_key"] > $from_key){
		$gKK[$c-1]["ratio_to"] = $ratio_from;
		if (!@$ymd[$row["int1"]]) $next_idx = $c-1;
	}

	$gKK[$next_idx] = array(
		"to_key"=>$to_key,
		"is_from_overflow" => $is_from_overflow,
		"is_to_overflow" => $is_to_overflow,
		"ratio_from"=> $ratio_from,
		"ratio_to"=> $ratio_to,
		"not_ended"=>$row["string4"]
	);
}

// 呼吸器のURL文字列を作成
$graphKK = array();
foreach($gKK as $idx => $d){
	$graphKK[] = $d["is_from_overflow"]."-".$d["is_to_overflow"]."-".$d["ratio_from"]."-".$d["ratio_to"]."-".$d["not_ended"];
}


//==============================================================================
// 温度板ケア情報の収集
// ケアはＤＢに格納されているXMLをほじくり出す。
//==============================================================================
$uni = array();
for($i = 1; $i <= 7; $i++) $uni[] = "select " . $care[$i]["yyyymmdd"] . " as ymd";
$sql =
  " select sss.*, dt.ymd".
  ",soa_kango.nikkin_accepted_emp_id as nikkin_kango_accepted_emp_id".
  ",soa_kango.yakin_accepted_emp_id as yakin_kango_accepted_emp_id".
  ",soa_kaigo.nikkin_accepted_emp_id as nikkin_kaigo_accepted_emp_id".
  ",soa_kaigo.yakin_accepted_emp_id as yakin_kaigo_accepted_emp_id".
  ",emp_kango1.emp_lt_nm || ' ' || emp_kango1.emp_ft_nm as nikkin_kango_emp_name".
  ",emp_kango2.emp_lt_nm || ' ' || emp_kango2.emp_ft_nm as yakin_kango_emp_name".
  ",emp_kaigo1.emp_lt_nm || ' ' || emp_kaigo1.emp_ft_nm as nikkin_kaigo_emp_name".
  ",emp_kaigo2.emp_lt_nm || ' ' || emp_kaigo2.emp_ft_nm as yakin_kaigo_emp_name".
  " from (".implode(" union ", $uni).") dt".
  " left join sot_summary_schedule sss on (sss.start_ymd = dt.ymd and sss.ptif_id = '".pg_escape_string(@$_REQUEST["sel_ptif_id"])."' and sss.delete_flg = false and sss.temporary_update_flg = 0 and  sss.tmpl_file_name = 'tmpl_OndobanCare.php')".
  " left outer join sot_ondoban_apply soa_kango on (soa_kango.kango_or_kaigo='kango' and soa_kango.ondoban_date = dt.ymd and soa_kango.ptif_id = '".pg_escape_string(@$_REQUEST["sel_ptif_id"])."')".
  " left outer join empmst emp_kango1 on (emp_kango1.emp_id = soa_kango.nikkin_accepted_emp_id)".
  " left outer join empmst emp_kango2 on (emp_kango2.emp_id = soa_kango.yakin_accepted_emp_id)".
  " left outer join sot_ondoban_apply soa_kaigo on (soa_kaigo.kango_or_kaigo='kaigo' and soa_kaigo.ondoban_date = dt.ymd and soa_kaigo.ptif_id = '".pg_escape_string(@$_REQUEST["sel_ptif_id"])."')".
  " left outer join empmst emp_kaigo1 on (emp_kaigo1.emp_id = soa_kaigo.nikkin_accepted_emp_id)".
  " left outer join empmst emp_kaigo2 on (emp_kaigo2.emp_id = soa_kaigo.yakin_accepted_emp_id)".
" order by dt.ymd, sss.schedule_seq";

$sel = $c_sot_util->select_from_table($sql);
$data = (array)pg_fetch_all($sel);
$kansatu_index_list = array();
$kansatu_data = array();
$kansatu_blank_data = array();
$kansatu_blank_data_max_size = 0;
foreach($data as $key => $row){
	if (!$row) break;
//	$idx = $ymd[$row["start_ymd"]];
  $idx = $ymd[$row["ymd"]];
  $care[$idx]["db"] = $row;
	$xml = new template_xml_class();
	$data = $xml->get_xml_object_array_from_db($con, $fname, $row["xml_file_name"]);
	$worksheet = @$data->nodes["template"]->nodes["Ondoban"];
	$care[$idx]["xml"] = @$worksheet->nodes["ocar"];// ケアはその日最新のものをひとつだけ取得
}

for ($idx = 1; $idx<= 7; $idx++){
	if (!@$care[$idx]["xml"]) continue;

	// タイトルが入力されている観察項目は、７日間を通じてグルーピングする。
	// タイトルが無い観察項目も、表示したい。別で保存しておく。
    $field_len_kansatu = $c_sot_util->countPatternMatchElement("/^ocar_kansatu_[0-9]+$/", array_keys($care[$idx]["xml"]->nodes));
    $field_len_kansatu = $field_len_kansatu >= 9 ? $field_len_kansatu : 9;
	for ($i = 1; $i <= $field_len_kansatu; $i++){
		$k_title = trim(@$care[$idx]["xml"]->nodes["ocar_kansatu_".$i]->value);
		$k_day =   trim(@$care[$idx]["xml"]->nodes["ocar_kansatu_".$i."_day"]->value);
		$k_night = trim(@$care[$idx]["xml"]->nodes["ocar_kansatu_".$i."_night"]->value);
		if ($k_title=="" && $k_day=="" && $k_night=="") continue;
		if ($k_title==""){
			$kansatu_blank_data[$idx]["day"][] = $k_day;
			$kansatu_blank_data[$idx]["night"][] = $k_night;
			$kansatu_blank_data_max_size = max($kansatu_blank_data_max_size, count($kansatu_blank_data[$idx]["day"]));
		} else {
			if (!in_array($k_title, $kansatu_index_list)) $kansatu_index_list[] = $k_title;
			@$kansatu_data[$idx][$k_title]["day"] .= $k_day;
			@$kansatu_data[$idx][$k_title]["night"] .= $k_night;
		}
	}
}


$sel = $c_sot_util->select_from_table("select tmpl_id from tmplmst where tmpl_file = 'tmpl_OndobanCare.php'");
$tmpl_id_care = pg_fetch_result($sel, 0, 0);
$sel = $c_sot_util->select_from_table("select tmpl_id from tmplmst where tmpl_file = 'tmpl_OndobanVital.php'");
$tmpl_id_vital = pg_fetch_result($sel, 0, 0);


?>
<? //************************************************************************* ?>
<? // HTMLここから                                                             ?>
<? //************************************************************************* ?>
<!DOCTYPE PUBLIC>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=emulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<!--[if IE]><script type="text/javascript" src="excanvas_r3/excanvas.compiled.js"></script><![endif]-->
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<title>CoMedix <?=get_report_menu_label($con, $fname)?> | 温度板</title>
<? write_yui_calendar_use_file_read_0_12_2();// 外部ファイルを読み込む ?>
<? write_yui_calendar_script2(1);// カレンダー作成、関数出力 ?>
<script type="text/javascript">


<? //************************************************************************* ?>
<? // グラフ作成クラス                                                         ?>
<? //************************************************************************* ?>

var customGraph = new Object();

<? //******************************** ?>
<? // コンストラクタ                  ?>
<? //******************************** ?>
customGraph.line = function (id) {
	var elm = document.getElementById(id);
	if(! elm) { return; }
	if( ! elm.nodeName.match(/^CANVAS$/i) ) { return; }
	if( ! elm.parentNode.nodeName.match(/^DIV$/i) ) { return; };
	// CANVAS要素
	if ( ! elm.getContext ){ return; }
	this.canvas = elm;
	// 2D コンテクストの生成
	this.ctx = this.canvas.getContext('2d');
	this.canvas.style.margin = "0";
	this.canvas.parentNode.style.position = "relative";
	this.canvas.parentNode.style.padding = "0";
	// CANVAS要素の親要素となるDIV要素の幅と高さをセット
	this.canvas.parentNode.style.width = this.canvas.width + "px";
	this.canvas.parentNode.style.height = this.canvas.height + "px";
	this.xMax = this.canvas.width;
	this.yMax = this.canvas.height;
};

<? //******************************** ?>
<? // 描画                            ?>
<? //******************************** ?>
customGraph.line.prototype.draw = function(items, borders) {
	if( ! this.ctx ) {return;}

	// 背景を白に
	this.ctx.fillStyle = "#ffffff";
	this.ctx.fillRect(0, 0, this.xMax, this.yMax);

	// 補助線類の描画
	for (b in borders){
		this.ctx.beginPath();
		if (borders[b].type=="holizont-dashed"){
			var frX = borders[b].fx;
			for(;;) {
				var toX = Math.min(borders[b].tx, frX+4);
				this.ctx.moveTo(frX, borders[b].fy);
				this.ctx.lineTo(toX, borders[b].fy);
				frX += 8;
				if (frX >= borders[b].tx) break;
			}
		} else {
			this.ctx.moveTo(borders[b].fx, borders[b].fy);
			this.ctx.lineTo(borders[b].tx, borders[b].ty);
		}
		this.ctx.lineWidth = 1;
		this.ctx.strokeStyle = borders[b].color;
		this.ctx.stroke();
	}

	// 折線グラフを描写
	for(var i in items) {
		this.ctx.beginPath();
		this.ctx.lineJoin = "round";

		// 折れ線グラフを描画
		if (items[i].type=="ORESEN"){
			this.ctx.lineWidth = 1;
			for(var j=0; j<items[i].items.length; j++) {
				if(j == 0) {
					this.ctx.moveTo(items[i].items[j].x-0.5, items[i].items[j].y-0.5);
				} else {
					this.ctx.lineTo(items[i].items[j].x-0.5, items[i].items[j].y-0.5);
				}
			}
		}
		// BPタイプ棒グラフを描画
		if (items[i].type=="BAR"){
			this.ctx.lineWidth = 6;
			for(var j=0; j<items[i].items.length; j++) {
				this.ctx.moveTo(items[i].items[j].x, items[i].items[j].u);
				this.ctx.lineTo(items[i].items[j].x, items[i].items[j].d);
			}
		}
		this.ctx.strokeStyle = items[i].color;
		this.ctx.stroke();

		// ドットを描画
		if (items[i].type=="ORESEN" || items[i].type=="MARK"){
			for(var k=0; k<items[i].items.length; k++) {
				var dottype = (items[i].items[k].dottype && items[i].items[k].dottype != "") ? items[i].items[k].dottype : items[i].dottype;
				this._draw_dot(items[i].items[k].x, items[i].items[k].y, 3, dottype, items[i].color);
			}
		}
		// 文字を描画
		if (items[i].type=="LABEL"){
			var posX = items[i].pos.x;
			var posY = items[i].pos.y;
			if(posX < 0 || posX > this.xMax || posY > this.yMax || posY < 0) continue;
			var margin = 3;
			this._drawText(posX, posY, items[i].label, items[i].size, items[i].color);
		}
	}
};

<? //******************************** ?>
<? // 頂点の描画                      ?>
<? //******************************** ?>
customGraph.line.prototype._draw_dot = function(x, y, rad, type, color) {
	this.ctx.beginPath();
	if( type == "circle" ) {
		this.ctx.fillStyle = color;
		this.ctx.arc(x, y, rad, 0, Math.PI*2, false);
		this.ctx.closePath();
		this.ctx.fill();
	} else if( type == "wsquare" ) {
		this.ctx.fillStyle = "#ffffff";
		this.ctx.moveTo(x-rad, y-rad);
		this.ctx.lineTo(x+rad, y-rad);
		this.ctx.lineTo(x+rad, y+rad);
		this.ctx.lineTo(x-rad, y+rad);
		this.ctx.closePath();
		this.ctx.fill();
		this.ctx.beginPath();
		this.ctx.strokeStyle = color;
		this.ctx.moveTo(x-rad-0.5, y-rad-0.5);
		this.ctx.lineTo(x+rad+0.5, y-rad-0.5);
		this.ctx.lineTo(x+rad+0.5, y+rad+0.5);
		this.ctx.lineTo(x-rad-0.5, y+rad+0.5);
		this.ctx.closePath();
		this.ctx.stroke();
	} else if( type == "cross" ) {
		this.ctx.strokeStyle = color;
		this.ctx.moveTo(x-0.5, y-rad-1);
		this.ctx.lineTo(x-0.5, y+rad);
		this.ctx.moveTo(x-rad-1, y-0.5);
		this.ctx.lineTo(x+rad, y-0.5);
		this.ctx.stroke();
	} else if( type == "square" ) {
		this.ctx.fillStyle = color;
		this.ctx.moveTo(x-rad, y-rad);
		this.ctx.lineTo(x+rad, y-rad);
		this.ctx.lineTo(x+rad, y+rad);
		this.ctx.lineTo(x-rad, y+rad);
		this.ctx.closePath();
		this.ctx.fill();
	} else if( type == "triangle" ) {
		this.ctx.fillStyle = color;
		this.ctx.moveTo(x, y-rad);
		this.ctx.lineTo(x+rad, y+rad);
		this.ctx.lineTo(x-rad, y+rad);
		this.ctx.closePath();
		this.ctx.fill();
	} else if( type == "i-triangle" ) {
		this.ctx.fillStyle = color;
		this.ctx.moveTo(x, y+rad);
		this.ctx.lineTo(x+rad, y-rad);
		this.ctx.lineTo(x-rad, y-rad);
		this.ctx.closePath();
		this.ctx.fill();
	} else if( type == "diamond" ) {
		this.ctx.fillStyle = color;
		this.ctx.moveTo(x, y-rad);
		this.ctx.lineTo(x+rad, y);
		this.ctx.lineTo(x, y+rad);
		this.ctx.lineTo(x-rad, y);
		this.ctx.closePath();
		this.ctx.fill();
	} else if( type == "batsu" ) {
		this.ctx.strokeStyle = "#000000";
		this.ctx.moveTo(x-rad, y-rad);
		this.ctx.lineTo(x+rad, y+rad);
		this.ctx.moveTo(x-rad, y+rad);
		this.ctx.lineTo(x+rad, y-rad);
		this.ctx.stroke();
	} else {
		this.ctx.fillStyle = color;
		this.ctx.arc(x, y, rad, 0, Math.PI*2, false);
		this.ctx.closePath();
		this.ctx.fill();
	}
};

<? //******************************** ?>
<? // キャプションの描画              ?>
<? //******************************** ?>
customGraph.line.prototype._drawText = function(x, y, text, font_size, color) {
	var div = document.createElement('DIV');
	div.appendChild(document.createTextNode(text));
	div.style.fontSize = font_size;
	div.style.fontWeight = "normal";
	div.style.color = color;
	div.style.margin = "0";
	div.style.padding = "0";
	div.style.position = "absolute";
	div.style.left = x.toString() + "px";
	div.style.top = y.toString() + "px";
	this.canvas.parentNode.appendChild(div);
}


<? //************************************************************************* ?>
<? // 画面遷移                                                                 ?>
<? //************************************************************************* ?>

<? //************************************* ?>
<? // ケア・バイタル編集テンプレートを開く ?>
<? //************************************* ?>
function popupCareVital(care_or_vital, dt, apply_id){
	<? if (!$ptif_name){ ?>
		alert("患者を指定してください。");
		return;
	<? } ?>
	var tmpl_title = (care_or_vital=="care" ? "温度板ケア" : "温度板バイタル");
	var tmpl_id = (care_or_vital=="care" ? "<?=$tmpl_id_care?>" : "<?=$tmpl_id_vital?>");
	for (var i = 1; i <= 7; i++){
		document.getElementById("vital_selector"+i).style.display = "none";
	}
	if (!apply_id){
		window.open("sum_application_apply_new.php?session=<?=$session?>&tmpl_id="+tmpl_id+"&ptif_id=<?=@$_REQUEST["sel_ptif_id"]?>&ymd="+dt,
			"", "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=600,height=700");
	} else {
		window.open("sum_application_approve_detail.php?session=<?=$session?>&tmpl_id="+tmpl_id+"&ptif_id=<?=@$_REQUEST["sel_ptif_id"]?>&ymd="+dt+"&apply_id="+apply_id+"&exec_mode=apply",
			"", "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=980,height=700");
	}
}

<? //************************************* ?>
<? // PDF印刷                              ?>
<? //************************************* ?>
function popupPrintPreview(){
	<? if ($ptif_name==""){ ?>
	alert("患者を指定してください。");
	return;
	<? } ?>
	window.open("sot_worksheet_ondoban_print.php?session=<?=$session?>&date_y1=<?=@$date_y_s?>&date_m1=<?=@$date_m_s?>&date_d1=<?=@$date_d_s?>&date_y2=<?=@$date_y?>&date_m2=<?=@$date_m?>&date_d2=<?=@$date_d?>&print_preview=y&sel_ptif_id=<?=@$sel_ptif_id?>&request_print_execute=1&empty_print_and_show_preview=1",
		"", "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=800,height=700");
}

<? //************************************* ?>
<? // 印刷設定画面                         ?>
<? //************************************* ?>
function popupPrintSetting(){
	window.open("sot_worksheet_ondoban_print.php?session=<?=$session?>&date_y1=<?=@$date_y_s?>&date_m1=<?=@$date_m_s?>&date_d1=<?=@$date_d_s?>&date_y2=<?=@$date_y?>&date_m2=<?=@$date_m?>&date_d2=<?=@$date_d?>&print_preview=y&sel_ptif_id=<?=@$sel_ptif_id?>&sel_bldg_cd=<?=@$sel_bldg_cd?>&sel_ward_cd=<?=@$sel_ward_cd?>",
		"", "directories=no,location=no,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no,width=600,height=330");
}

<? //************************************* ?>
<? // 患者検索ダイアログのポップアップ     ?>
<? //************************************* ?>
function popupKanjaSelect(){
	window.open(
		"sot_kanja_select.php?session=<?=$session?>&js_callback=callbackKanjaSelect&sel_bldg_cd=1&sel_ward_cd=<?=@$ptif_ward_cd?>&sel_ptif_id=<?=@$sel_ptif_id?>&sel_sort_by_byoto=1",
		"kanjaSelectWindow",
		"directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,width=630,height="+window.screen.availHeight+",left=0,top=0"
	);
}
<? //************************************* ?>
<? // 患者検索結果を受けるコールバック関数 ?>
<? //************************************* ?>
function callbackKanjaSelect(ptif_id){
	document.getElementById("sel_ptif_id").value = ptif_id;
	document.search.submit();
}

<? //************************************* ?>
<? // バイタルの選択ダイアログを表示する   ?>
<? //************************************* ?>
function showVitalSelector(td, idx, dt){
	var vital_counts = {};
	var vital_days = {};
	<? for($idx = 1; $idx <= 7; $idx++){ ?>
	<? $rows = @$vital[$idx]; ?>
	vital_counts[<?=$idx?>] = <?= (is_array($rows) ? count($rows) : 0) ?>;
	vital_days[<?=$idx?>] = "<?=@$care[$idx]["yyyymmdd"]?>";
	<? } ?>

	<? if (!$ptif_name){ ?>
		alert("患者を指定してください。");
		return;
	<? } ?>
	for (var i = 1; i <= 7; i++){
		if (i != idx){
			document.getElementById("vital_selector"+i).style.display = "none";
			continue;
		}
		if (vital_counts[idx] == 0) {
			popupCareVital('vital',vital_days[idx],'');
			continue;
		}

		vs = document.getElementById("vital_selector"+idx);
		vs.style.left = (getClientWidth() / 2 - 100) + "px";
		vs.style.top = ((window.parent.document.body.scrollTop || window.parent.document.documentElement.scrollTop) + 10) + "px";
		vs.style.display = "block";
	}
}
function getClientWidth(){
	if (document.documentElement && document.documentElement.clientWidth) return document.documentElement.clientWidth;
	if (document.body.clientWidth) return document.body.clientWidth;
	if (window.innerWidth) return window.innerWidth;
	return 0;
}

<? //************************************* ?>
<? // 事業所/病棟/病室のドロップダウン選択 ?>
<? //************************************* ?>
var ward_selections = {};
var room_selections = {};
var combobox = {};
function sel_changed(node, selval){
	combobox[1] = document.getElementById("sel_bldg_cd");
	combobox[2] = document.getElementById("sel_ward_cd");
	combobox[3] = document.getElementById("sel_ptrm_room_no");
	if (node < 3) combobox[3].options.length = 1;
	if (node < 2) combobox[2].options.length = 1;

	<? // 初期化用 自分コンボを再選択?>
	var cmb = combobox[node];
	if (node > 0 && cmb.value != selval) {
		for(var i=0; i<cmb.options.length; i++) if (cmb.options[i].value==selval) cmb.selectedIndex = i;
	}

	var idx = 1;
	if (node == 1){
		for(var i in ward_selections){
			if (ward_selections[i].bldg_cd != selval) continue;
			combobox[2].options.length = combobox[2].options.length + 1;
			combobox[2].options[idx].text  = ward_selections[i].name;
			combobox[2].options[idx].value = ward_selections[i].code;
			idx++;
		}
	}
	if (node == 2){
		for(var i in room_selections){
			if (room_selections[i].bldg_cd != combobox[1].value) continue;
			if (room_selections[i].ward_cd != selval) continue;
			combobox[3].options.length = combobox[3].options.length + 1;
			combobox[3].options[idx].text  = room_selections[i].name;
			combobox[3].options[idx].value = room_selections[i].code;
			idx++;
		}
	}
}

<? //************************************* ?>
<? // 前日/翌日などの検索ボタン            ?>
<? //************************************* ?>
function searchByDay(day_incriment){
	var yy = document.search.date_y;
	var mm = document.search.date_m;
	var dd = document.search.date_d;
	var y = (parseInt(yy.value,10) ? yy.value : yy.options[1].value);
	var m = (parseInt(mm.value,10) ? mm.value : 1)*1;
	var d = (parseInt(dd.value,10) ? dd.value : 1)*1;
	var dt = new Date(y, m-1, d);
	dt.setTime(dt.getTime() + day_incriment * 86400000);

	var idx_y = yy.options[1].value - dt.getFullYear() + 1;
	if (idx_y < 1) return;
	yy.selectedIndex = idx_y;
	mm.selectedIndex = dt.getMonth()+1;
	dd.selectedIndex = dt.getDate();
	document.search.submit();
}

<? //************************************* ?>
<? // 本日検索ボタン                       ?>
<? //************************************* ?>
function searchByToday(){
	var dt = new Date();
	document.search.date_y.selectedIndex = document.search.date_y.options[1].value - dt.getFullYear() + 1;
	document.search.date_m.selectedIndex = dt.getMonth()+1;
	document.search.date_d.selectedIndex = dt.getDate();
	document.search.submit();
}



<? //************************************************************************* ?>
<? // リサイズイベント、グラフの作成、各種位置調整                             ?>
<? //************************************************************************* ?>
var graph_td_cell_width = 0;
var image_width = 0;
var parentResized = 0;
function windowResized(){

	<? if (@$_REQUEST["print_preview"]) { ?>
	var w = (document.getElementById("print_title_header").clientWidth || document.getElementById("print_title_header").offsetWidth);
	<? } else { ?>
	var w = (document.getElementById("header_menu_table").clientWidth || document.getElementById("header_menu_table").offsetWidth);
	<? } ?>

	var tdw1 = Math.floor((w-152-44-1-2) / 42); // ボーダを抜いた１セルの幅
	var tdw6 = tdw1*6+5; // ６セルの幅
	if (tdw1 == graph_td_cell_width) return;
	graph_td_cell_width = tdw1;

	var divlen = divlist.length;
	for (var i=0; i<divlen; i++){
		workArea.appendChild(divlist[i]);
	}

	var canvasDivNames = ["main","head", "sanso", "sinden", "kokyuki"];
	for (var dn = 0; dn < 5; dn++){
		var div1 = document.getElementById("graph_"+canvasDivNames[dn]+"_canvas_div");
		for (cn = 0; cn < div1.childNodes.length; cn++){
			var cnode = div1.childNodes[cn];
			if( cnode.nodeName.match(/^CANVAS$/i) ) continue;
			var retnode = div1.removeChild(cnode);
			delete retnode;
		}
	}

	document.getElementById("tdA").style.width = "20px"; // 1列目の行ヘッダ幅、パディングを含めない
	document.getElementById("tdB").style.width = "91px"; // 2列目の行ヘッダ幅、パディングを含めない
	document.getElementById("td0").style.width = "116px"; // 1+2列目の行ヘッダ幅、パディングを含めない
	if (document.getElementById("td0").clientWidth > 152) document.getElementById("td0").style.width = "152px"; // IE/Firefoxコンパチ

	document.getElementById("tbl1").style.width = (tdw1*42 + 152 + 44+1+2) + "px"; // ボーダを入れたテーブルの幅 // IPAD対応DIVボーダ2pxをさらに追加
	for (var i = 1; i <= 42; i++){
		var most_left_col_margin = (i==1 ? 1 : 0);
		var ipadBorder = (i==1) ? 1 : 0;
		document.getElementById("td"+i).style.width = (tdw1+most_left_col_margin+2+ipadBorder) + "px";
	}
	for (var i = 1; i <= 7; i++){
		var ipadBorder = (i==1 || i==7) ? 1 : 0;
		document.getElementById("tdH"+i).style.width = (tdw6+ipadBorder) + "px";
	}
	prepareTooltipX(tdw6*7+7);

	var expansion = <? if (@$_REQUEST["print_preview"]) {?>3<? } else {?>1<? } ?>;

	var canvasWidth = (tdw6*7+7);
	var canvasHeight = 300;
	document.getElementById("graph_main_canvas").width = canvasWidth;
	document.getElementById("graph_main_canvas").height = canvasHeight;
	document.getElementById("graph_main_canvas").style.width = canvasWidth + "px";
	document.getElementById("graph_main_canvas").style.height = canvasHeight + "px";
	var widthPerMinute = canvasWidth / (60*24*7);
	document.getElementById("graph_sanso_canvas").width = canvasWidth;
	document.getElementById("graph_sanso_canvas").style.width = canvasWidth + "px";
	document.getElementById("graph_sinden_canvas").width = canvasWidth;
	document.getElementById("graph_sinden_canvas").style.width = canvasWidth + "px";
	document.getElementById("graph_kokyuki_canvas").width = canvasWidth;
	document.getElementById("graph_kokyuki_canvas").style.width = canvasWidth + "px";

	<? //************************************* ?>
	<? // メイングラフ                         ?>
	<? //************************************* ?>
	var graphMain = new customGraph.line("graph_main_canvas");
	var heightPerGaugeT = canvasHeight / (43 - 33);
	var dataT = "<?=implode(",",$graphT)?>".split(",");
	var listT = [];
	for (var i=0; i<dataT.length; i++){
		var p = dataT[i].split("-");
		if (p.length!=2) continue;
		p[0] = parseInt(p[0] * widthPerMinute);
		p[1] = parseInt((43-p[1]) * heightPerGaugeT);
		listT.push({"x":p[0],"y":p[1]});
	}
	var heightPerGaugeR = canvasHeight / (55 - 5);
	var dataR = "<?=implode(",",$graphR)?>".split(",");
	var listR = [];
	for (var i=0; i<dataR.length; i++){
		var p = dataR[i].split("-");
		if (p.length!=2) continue;
		p[0] = parseInt(p[0] * widthPerMinute);
		p[1] = parseInt((55-p[1]) * heightPerGaugeR);
		listR.push({"x":p[0],"y":p[1]});
	}
	var heightPerGaugeP = canvasHeight / (210 - 10);
	var dataP = "<?=implode(",",$graphP)?>".split(",");
	var listP = [];
	for (var i=0; i<dataP.length; i++){
		var p = dataP[i].split("-");
		if (p.length!=3) continue;
		p[0] = parseInt(p[0] * widthPerMinute);
		p[1] = parseInt((210-p[1]) * heightPerGaugeP);
		p[2] = (p[2] == "") ? "circle" : "batsu";
		listP.push({"x":p[0],"y":p[1],"dottype":p[2]});
	}
	var heightPerGaugeS = canvasHeight / (130 - 30);
	var dataS = "<?=implode(",",$graphS)?>".split(",");
	var listS = [];
	for (var i=0; i<dataS.length; i++){
		var p = dataS[i].split("-");
		if (p.length!=2) continue;
		p[0] = parseInt(p[0] * widthPerMinute);
		p[1] = parseInt((130-p[1]) * heightPerGaugeS);
		listS.push({"x":p[0],"y":p[1]});
	}
	var heightPerGaugeBP = canvasHeight / (240 - 40);
	var dataBP = "<?=implode(",",$graphBP)?>".split(",");
	var listBP = [];
	for (var i=0; i<dataBP.length; i++){
		var p = dataBP[i].split("-");
		if (p.length!=2) continue;
		var ud = p[1].split("_");
		if (ud.length!=2) continue;
		p[0] = parseInt(p[0] * widthPerMinute);
		ud[0] = parseInt((240-ud[0]) * heightPerGaugeBP);
		ud[1] = parseInt((240-ud[1]) * heightPerGaugeBP);
		listBP.push({"x":p[0],"u":ud[0],"d":ud[1]});
	}

	var bordersM = [];
	// 横軸(中間ピンク)
	bordersM.push({"type":"solid","color":"#ff60cf","fx":0,"fy":180.5,"tx":canvasWidth,"ty":180.5});
	// 横軸(灰色)
	for (var i = 0; i <= 9; i++){
		if (i > 0 && i != 6) bordersM.push({"type":"solid","color":"#dddddd","fx":0,"fy":(i*30)+0.5,"tx":canvasWidth,"ty":(i*30)+0.5});
		bordersM.push({"type":"holizont-dashed","color":"#eeeeee","fx":0,"fy":(i*30)+15.5,"tx":canvasWidth,"ty":(i*30)+15.5});
	}
	// 縦軸(水色)
	for (var i = 1; i <= 6; i++){
		bordersM.push({"type":"solid","color":"#9bc8ec","fx":tdw6*i+i-0.5,"fy":0,"tx":tdw6*i+i-0.5,"ty":canvasHeight});
	}

	var itemsM = [
		{"type":"BAR","color":"#aaaa55","dottype":"circle","items":listBP},
		{"type":"ORESEN", "color":"#0000ff","dottype":"wsquare","items":listT},
		{"type":"ORESEN", "color":"#ff0000","dottype":"circle","items":listP},
		{"type":"ORESEN", "color":"#008822","dottype":"cross","items":listR},
		{"type":"ORESEN", "color":"#1b9cab","dottype":"square","items":listS}
	];



	<? //************************************* ?>
	<? // ヘッダグラフ                         ?>
	<? //************************************* ?>
	var graphHead = new customGraph.line("graph_head_canvas");

	var bordersH = [];
	// 横軸(一番上水色)
	bordersH.push({"type":"solid","color":"#9bc8ec","fx":0,"fy":15.5,"tx":150,"ty":15.5});
	// 横軸(中間ピンク)
	bordersH.push({"type":"solid","color":"#ff60cf","fx":0,"fy":180.5,"tx":150,"ty":180.5});
	// 横軸(灰色)
	for (var i = 1; i <= 9; i++){
		if (i != 6) bordersH.push({"type":"solid","color":"#dddddd","fx":0,"fy":(i*30)+0.5,"tx":150,"ty":(i*30)+0.5});
		bordersH.push({"type":"holizont-dashed","color":"#eeeeee","fx":0,"fy":(i*30)+15.5,"tx":150,"ty":(i*30)+15.5});
	}
	// 縦軸(水色)
	for (var i = 1; i <= 6; i++){
		bordersH.push({"type":"solid","color":"#9bc8ec","fx":30*i+i-0.5,"fy":0,"tx":30*i+i-0.5,"ty":canvasHeight});
	}

	bordersH.push({"type":"solid","color":"#0000ff","fx":16,"fy":7.5,"tx":28,"ty":7.5});
	bordersH.push({"type":"solid","color":"#ff0000","fx":47,"fy":7.5,"tx":59,"ty":7.5});
	bordersH.push({"type":"solid","color":"#008822","fx":78,"fy":7.5,"tx":91,"ty":7.5});
	bordersH.push({"type":"solid","color":"#1b9cab","fx":109,"fy":7.5,"tx":122,"ty":7.5});

	var itemsH = [
		{"type":"MARK", "color":"#0000ff","dottype":"wsquare", "items":[{"x":22, "y":8}]},
		{"type":"MARK", "color":"#ff0000","dottype":"circle",  "items":[{"x":53, "y":7.5}]},
		{"type":"MARK", "color":"#008822","dottype":"cross",   "items":[{"x":85, "y":8}]},
		{"type":"MARK", "color":"#1b9cab","dottype":"square",  "items":[{"x":115,"y":8}]},

		{"type":"LABEL", "color":"#0000ff", "pos":{"x":3,  "y":2}, "label":"T",  "size":"11px"},
		{"type":"LABEL", "color":"#ff0000", "pos":{"x":34, "y":2}, "label":"P",  "size":"11px"},
		{"type":"LABEL", "color":"#008822", "pos":{"x":65, "y":2}, "label":"R",  "size":"11px"},
		{"type":"LABEL", "color":"#1b9cab", "pos":{"x":94, "y":2}, "label":"SP", "size":"11px"},
		{"type":"LABEL", "color":"#aaaa55", "pos":{"x":127,"y":2}, "label":"BP", "size":"11px"}
	];
	for (var i = 0; i < 10; i++){
		var lblT = lblP = lblR = lblS = lblBP = 0;
		if (i > 0) {
			lblT = 33 + (i);
			lblP = 10 + (i*20);
			lblR = 5 + (i*5);
			lblS = 30 + (i*10);
			lblBP = 40 + (i*20);
		}
		var y = 288-(i*30);
		itemsH.push({"type":"LABEL", "color":"#0000ff", "pos":{"x":3,  "y":y}, "label":lblT,  "size":"11px"});
		itemsH.push({"type":"LABEL", "color":"#ff0000", "pos":{"x":34, "y":y}, "label":lblP,  "size":"11px"});
		itemsH.push({"type":"LABEL", "color":"#008822", "pos":{"x":65, "y":y}, "label":lblR,  "size":"11px"});
		if (i < 8) itemsH.push({"type":"LABEL", "color":"#1b9cab", "pos":{"x":96, "y":y}, "label":lblS,  "size":"11px"});
		itemsH.push({"type":"LABEL", "color":"#aaaa55", "pos":{"x":127,"y":y}, "label":lblBP, "size":"11px"});
	}

	<? //************************************* ?>
	<? // 酸素吸入グラフ                       ?>
	<? //************************************* ?>
	var graphSanso = new customGraph.line("graph_sanso_canvas");
	var bordersSK = [];
	var itemsSK = [];
	// 縦軸(水色)
	for (var i = 1; i <= 6; i++){
		bordersSK.push({"type":"solid","color":"#9bc8ec","fx":tdw6*i+i-0.5,"fy":0,"tx":tdw6*i+i-0.5,"ty":24});
	}
	<? foreach($gSK as $idx => $d){ ?>
		var fromX = widthPerMinute*<?=$d["ratio_from"]?>;
		var toX = widthPerMinute*<?=$d["ratio_to"]?>;
		bordersSK.push({"type":"solid","color":"#8064a2","fx":fromX,"fy":18.5,"tx":toX,"ty":18.5});
		<? if ($d["is_from_overflow"]=="n") { ?>
			itemsSK.push({"type":"MARK", "color":"#8064a2","dottype":"square", "items":[{"x":fromX, "y":18}]});
		<? } ?>
		<? if ($d["is_to_overflow"]=="n") { ?>
			<? $dottype = ($d["not_ended"]=="y" ? "wsquare" : "square" ); ?>
			itemsSK.push({"type":"MARK", "color":"#8064a2","dottype":"<?=$dottype?>", "items":[{"x":toX, "y":18}]});
		<? } ?>
		<? if ($d["litter"]!="") { ?>
			itemsSK.push({"type":"LABEL", "color":"#000000", "pos":{"x":fromX,"y":2}, "label":"<?=$d["litter"]?>", "size":"11px"});
		<? } ?>
	<? } ?>

	<? //************************************* ?>
	<? // 心電図モニターグラフ                 ?>
	<? //************************************* ?>
	var graphSinden = new customGraph.line("graph_sinden_canvas");
	var bordersSM = [];
	var itemsSM = [];
	for (var i = 1; i <= 6; i++){
		bordersSM.push({"type":"solid","color":"#9bc8ec","fx":tdw6*i+i-0.5,"fy":0,"tx":tdw6*i+i-0.5,"ty":24});
	}
	<? foreach($gSM as $idx => $d){ ?>
		var fromX = widthPerMinute*<?=$d["ratio_from"]?>;
		var toX = widthPerMinute*<?=$d["ratio_to"]?>;
		bordersSM.push({"type":"solid","color":"#9bbb59","fx":fromX,"fy":18.5,"tx":toX,"ty":18.5});
		<? if ($d["is_from_overflow"]=="n") { ?>
			itemsSM.push({"type":"MARK", "color":"#9bbb59","dottype":"square", "items":[{"x":fromX, "y":18}]});
		<? } ?>
		<? if ($d["is_to_overflow"]=="n") { ?>
			<? $dottype = ($d["not_ended"]=="y" ? "wsquare" : "square" ); ?>
			itemsSM.push({"type":"MARK", "color":"#9bbb59","dottype":"<?=$dottype?>", "items":[{"x":toX, "y":18}]});
		<? } ?>
	<? } ?>

	<? //************************************* ?>
	<? // 呼吸器グラフ                         ?>
	<? //************************************* ?>
	var graphKokyuki = new customGraph.line("graph_kokyuki_canvas");
	var bordersKK = [];
	var itemsKK = [];
	for (var i = 1; i <= 6; i++){
		bordersKK.push({"type":"solid","color":"#9bc8ec","fx":tdw6*i+i-0.5,"fy":0,"tx":tdw6*i+i-0.5,"ty":24});
	}
	<? foreach($gKK as $idx => $d){ ?>
		var fromX = widthPerMinute*<?=$d["ratio_from"]?>;
		var toX = widthPerMinute*<?=$d["ratio_to"]?>;
		bordersKK.push({"type":"solid","color":"#ff9155","fx":fromX,"fy":18.5,"tx":toX,"ty":18.5});
		<? if ($d["is_from_overflow"]=="n") { ?>
			itemsKK.push({"type":"MARK", "color":"#ff9155","dottype":"square", "items":[{"x":fromX, "y":18}]});
		<? } ?>
		<? if ($d["is_to_overflow"]=="n") { ?>
			<? $dottype = ($d["not_ended"]=="y" ? "wsquare" : "square" ); ?>
			itemsKK.push({"type":"MARK", "color":"#ff9155","dottype":"<?=$dottype?>", "items":[{"x":toX, "y":18}]});
		<? } ?>
	<? } ?>

	<? //************************************* ?>
	<? // まとめて画面表示                     ?>
	<? //************************************* ?>
	graphMain.draw(itemsM, bordersM);
	graphHead.draw(itemsH, bordersH);
	graphSanso.draw(itemsSK, bordersSK);
	graphSinden.draw(itemsSM, bordersSM);
	graphKokyuki.draw(itemsKK, bordersKK);

	document.getElementById("ondoban_pane").style.display = "";

	setDivWidths();

	var ondobanBody = document.getElementById("ondoban_body");
	document.getElementById("ondoban_pane").style.height = (ondobanBody.offsetHeight || ondobanBody.clientHeight) + "px";
	if (!parentResized && parent && parent.resizeIframe) parent.resizeIframe();
	parentResized = 1;

	<? if (@$_REQUEST["print_preview"]) {?>
	print();
	close();
	<? } ?>
}

<?//--------------------------------------------?>
<?// サーバへ登録送信                           ?>
<?//--------------------------------------------?>
function inquiryRegist(nikkin_yakin, ondoban_date, kango_kaigo){
	var empid = document.getElementById("sign_"+ondoban_date+"_"+nikkin_yakin+"_"+kango_kaigo+"_emp_id");
	var is_delete = (empid.value=="" ? 0 : 1);
	var ptif_id = "<?=@$_REQUEST["sel_ptif_id"]?>";
	if (is_delete==0){
		if (!confirm("確認サインを登録します。よろしいですか？")) return;
	} else {
		if (!confirm("確認サインの登録を消去します。よろしいですか？")) return;
	}

	var postval =
		"session=<?=$session?>"+
		"&nikkin_yakin="+nikkin_yakin+
		"&kango_kaigo="+kango_kaigo+
		"&ptif_id=<?=@$_REQUEST["sel_ptif_id"]?>"+
		"&ondoban_date="+ondoban_date+
		"&is_delete="+is_delete;
	if (window.XMLHttpRequest) ajaxObj = new XMLHttpRequest();
	else {
		try { ajaxObj = new ActiveXObject("Msxml2.XMLHTTP"); }
		catch(e) { ajaxObj = new ActiveXObject("Microsoft.XMLHTTP"); }
	}
  ajaxObj.onreadystatechange = returnInquiryRegist;
  ajaxObj.open("POST", "sot_worksheet_ondoban_apply.php", true);
  ajaxObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  ajaxObj.send(postval);
}
<?//--------------------------------------------?>
<?// サーバからの戻りを取得して画面表示を変更   ?>
<?//--------------------------------------------?>
function returnInquiryRegist(){
	if (ajaxObj == null) return;
	if (ajaxObj.readyState != 4) return;
	if (typeof(ajaxObj.status) != "number") return;
	if (ajaxObj.status != 200) return;
	if (!ajaxObj.responseText) return;
	try{ var ret = false; eval("ret = " + ajaxObj.responseText); if (ret.error) { alert(ret.error); return; }}
	catch(e){ alert("[response invalid]\n"+ajaxObj.responseText); return; }
	if (!ret) return;
	var nikkin_yakin = ret.nikkin_yakin;
	var kango_kaigo = ret.kango_kaigo;
	var ondoban_date = ret.ondoban_date;
	var emp_id = ret.emp_id;
	var emp_name = ret.emp_name;
	var is_delete = (parseInt(ret.is_delete,10)>0) ? 1 : 0;
	ajaxObj = null;
	if (!ret.is_succeed) return;
	if (!ondoban_date) return;
	if (!nikkin_yakin) return;
	if (!kango_kaigo) return;
	var elem = document.getElementById("sign_"+ret.ondoban_date+"_"+ret.nikkin_yakin+"_"+ret.kango_kaigo);
	var empid = document.getElementById("sign_"+ret.ondoban_date+"_"+ret.nikkin_yakin+"_"+ret.kango_kaigo+"_emp_id");
	if (!elem || !empid) return;
	empid.value = (is_delete ? "" : emp_id);
	elem.style.color = (is_delete ? "#f05" : "#880");
	elem.innerHTML = (is_delete ? "確認" : emp_name);
	setDivWidths();
}

<? //++++++++++++++++++++++++++++++++++++++++++++?>
<? // 印刷画面の印刷開始プロパティダイアログを開く?>
<? // 印刷開始はグラフ画像を読み込んだ後にする。  ?>
<? //++++++++++++++++++++++++++++++++++++++++++++?>
var loaded_graph_main = false;
var loaded_graph_sanso = false;
var loaded_graph_sinden = false;
var loaded_graph_kokyuki = false;
var loaded_graph_head = false;
function startPrint(type){
	if (type=="main") loaded_graph_main = true;
	if (type=="sanso") loaded_graph_sanso = true;
	if (type=="sinden") loaded_graph_sinden = true;
	if (type=="kokyuki") loaded_graph_kokyuki = true;
	if (type=="head") loaded_graph_head = true;
	if (!loaded_graph_main) return;
	if (!loaded_graph_sanso) return;
	if (!loaded_graph_sinden) return;
	if (!loaded_graph_kokyuki) return;
	if (!loaded_graph_head) return;
	print();
	close();
}

</script>

<? //=========================================================================?>
<? // スタイルシート                                                          ?>
<? //=========================================================================?>
<style type="text/css" media="all">
	.ondo_base_table { border-collapse:separate; border-spacing:1px; }

	.cent { text-align:center }
	.print_preview_display { display:none; }
	.d { vertical-align:top; border:1px solid <?=$border_color?>; padding:2px; }
	.d div { text-align:center; }
	.pad1 { padding:1px }
	.f10px { font-size:10px }
	.rowhead { background-color:#f6f9ff }
	.btmborder2px { border-bottom:2px solid <?=$border_color?> }
	.vital_selector { border-collapse:collapse; background-color:#fff; border:0; width:100% }
	.vital_selector td { border:1px solid #1f97f5; padding:2px }

	.btn_care { vertical-align:top; padding-top:2px; padding-bottom:2px }
	.btn_care div { border:1px solid #bbb; text-align:center; background-image:url(img/basic_button_back.gif); background-repeat:repeat-x; cursor:pointer; color:#000; }

	.btn_vital{ vertical-align:top; padding-top:2px; padding-bottom:2px }
	.btn_vital div { border:1px solid #bbb; text-align:center; background-image:url(img/basic_button_back.gif); background-repeat:repeat-x; cursor:pointer; color:#000; }

	.suggest_div { font-size:12px; padding:4px; background-color:#f7ffd2; border:2px solid #ddff3e; color:#777; white-space:nowrap }
	.suggest_result { background-color:#f7ffd2 }
	.suggest_result th { background:0; border:1px solid #ddff3e; padding:2px 6px; background-color:#f7ffd2 }
	.suggest_result td { background:0; border:1px solid #ddff3e; padding:2px 6px; background-color:#f7ffd2 }

	a.white:link {color:#fff; text-decoration: underline; }
	a.white:visited {color:#fff; text-decoration: underline; }
	a.white:hover {color:#fc5; text-decoration: underline; }
	.tooltip td { padding:1px; padding-left:3px; }
	.tooltip th { font-weight:normal; padding-right:4px; padding-left:4px }
<? if (@$_REQUEST["print_preview"]) {?>
	.print_preview_hidden { display:none; }
	.print_preview_display { display:block; }
	.print_hidden { display:none; }
<? } ?>
</style>
</head>
<body onload="initcal(); windowResized();" onresize="windowResized();"<? if (@$_REQUEST["print_preview"]) {?>style="padding:1px"<? } ?>>
<div class="print_preview_hidden">

<? $param = "&date_y=".@$date_y."&date_m=".@$date_m."&date_d=".@$date_d."&sel_bldg_cd=".@$sel_bldg_cd."&sel_ward_cd=".@$sel_ward_cd."&sel_ptrm_room_no=".@$sel_ptrm_room_no."&sel_team_id=".$sel_team_id; ?>

<? summary_common_show_sinryo_top_header($session, $fname, "温度板", $param); ?>

<? summary_common_show_main_tab_menu($con, $fname, "看護支援", 1); ?>


<?
$EMPTY_OPTION = '<option value="">　　　　</option>';

// チーム一覧
$team_options = array();
$sel = $c_sot_util->select_from_table("select team_id, team_nm from teammst where team_del_flg = 'f'");
while($row = pg_fetch_array($sel)){
	$team_options[] =
	$selected = ($row["team_id"] == @$_REQUEST["sel_team_id"] ? " selected" : "");
	$team_options[] = '<option value="'.$row["team_id"].'"'.$selected.'>'.$row["team_nm"].'</option>';
}

// 事業所選択肢(部門一覧)
$bldg_data_options = array();
$sel = $c_sot_util->select_from_table("select bldg_cd, bldg_name from bldgmst where bldg_del_flg = 'f' order by bldg_cd");
$mst = (array)pg_fetch_all($sel);
foreach($mst as $key => $row) {
	$selected = ($row["bldg_cd"] == @$_REQUEST["sel_bldg_cd"] ? " selected" : "");
	$bldg_data_options[] = '<option value="'.$row["bldg_cd"].'"'.$selected.'>'.$row["bldg_name"].'</option>';
}

// 病棟選択肢 JavaScript変数として出力する
$ary = array();
$sel = $c_sot_util->select_from_table("select bldg_cd, ward_cd, ward_name from wdmst where ward_del_flg = 'f' order by ward_cd");
$mst = (array)pg_fetch_all($sel);
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","code":"'.$row["ward_cd"].'","name":"'.$row["ward_name"].'"}';
echo "<script type='text/javascript'>ward_selections = [". implode(",", $ary) . "];\n</script>\n";

// 病室選択肢 JavaScript変数として出力する
$mst = $c_sot_util->getPatientsRoomInfo();
$ary = array();
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","ward_cd":"'.$row["ward_cd"].'","code":"'.$row["ptrm_room_no"].'","name":"'.$row["ptrm_name"].'"}';
echo "<script type='text/javascript'>room_selections = [". implode(",", $ary) . "];\n</script>\n";
?>

<form name="search" action="sot_worksheet_ondoban.php?session=<?=$session?>" method="get">
<input type="hidden" name="session" value="<? echo($session); ?>">
<table class="list_table" style="margin-top:4px; margin-bottom:8px" cellspacing="1" cellpadding="1">
	<tr>
		<th>事業所(棟)</th>
		<td><select onchange="sel_changed(1, this.value);" name="sel_bldg_cd" id="sel_bldg_cd"><?=implode("\n", $bldg_data_options);?></select></td>
		<th>病棟</th>
		<td><select onchange="sel_changed(2, this.value);" name="sel_ward_cd" id="sel_ward_cd"><?=$EMPTY_OPTION?></select></td>
		<th>病室</th>
		<td><select name="sel_ptrm_room_no" id="sel_ptrm_room_no"><?=$EMPTY_OPTION?></select></td>
		<script type="text/javascript">sel_changed(1, document.getElementById("sel_bldg_cd").value);</script>
		<script type="text/javascript">sel_changed(2, "<?=@$_REQUEST["sel_ward_cd"]?>");</script>
		<script type="text/javascript">sel_changed(3, "<?=@$_REQUEST["sel_ptrm_room_no"]?>");</script>
		<th bgcolor="#fefcdf">チーム</th>
		<td>
			<select name="sel_team_id" id="sel_team_id"><?=$EMPTY_OPTION?><?=implode("\n", $team_options)?></select>
		</td>
	</tr>
	<tr>
		<th>日付</th>
		<td colspan="7">
			<div style="float:left">
				<div style="float:left">
					<select id="date_y1" name="date_y"><option value="-"><? show_select_years_span(date("Y") - 14, date("Y") + 1, $date_y, false); ?></select>&nbsp;/
					<select id="date_m1" name="date_m"><? show_select_months($date_m, true); ?></select>&nbsp;/
					<select id="date_d1" name="date_d"><? show_select_days($date_d, true); ?></select>
				</div>
				<div style="float:left">
					<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
					<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
				</div>
				<div style="float:left; margin-left:4px">
					<input type="button" onclick="searchByDay('-7');" value="≪" title="１週間前"/><!--
				--><input type="button" onclick="searchByDay('-1');" value="<" title="１日前"/><!--
				--><input type="button" onclick="searchByDay('1');" value=">" title="１日後"/><!--
				--><input type="button" onclick="searchByDay('7');" value="≫" title="１週間後"/>
				</div>
				<div style="float:left;">
					<input type="button" onclick="searchByToday();" value="本日"/>
				</div>
				<div style="clear:both"></div>
			</div>
			<div style="float:right">
				<input type="button" onclick="document.search.submit();" value="検索"/>
				<input type="button" onclick="popupPrintSetting();" value="一括印刷"/>
				<input type="button" onclick="popupPrintPreview();" value="印刷"/>
			</div>
			<div style="clear:both"></div>
		</td>
	</tr>
</table>



<? summary_common_show_worksheet_tab_menu("温度板", $param); ?>


<? //=========================================================================?>
<? // バイタルのポップアップセレクタ                                          ?>
<? //=========================================================================?>
<? for($idx = 1; $idx <= 7; $idx++){ ?>
<? $rows = @$vital[$idx]; ?>
<div id="vital_selector<?=$idx?>" style="position:absolute; display:none; z-index:1000; margin-top:20px; border:0;">
	<div style="border:3px solid #1f97f5; width:200px; background-color:#1f97f5">
		<div style="float:left; color:#fff">バイタル編集&nbsp;&nbsp;&nbsp;&nbsp;<?=@$care[$idx]["date"]?></div>
		<div style="text-align:right; border-bottom:3px solid #1f97f5; float:right">
			<a class="white" href="" onclick="document.getElementById('vital_selector<?=$idx?>').style.display='none'; return false;">閉じる</a>
		</div>
		<div style="clear:both"></div>
		<div style="border:3px solid #fff;">
			<table class="vital_selector">
				<tr><td><a href="" onclick="popupCareVital('vital','<?=@$care[$idx]["yyyymmdd"]?>',''); return false;" class="always_blue">新規登録</a></td></tr>
				<? if (is_array($rows)){ ?>
				<? foreach($rows as $row){ ?>
				<? $jifun = (int)$row["int1"]."時".sprintf("%02d",(int)$row["int2"])."分" ?>
				<tr><td><a href="" onclick="popupCareVital('vital','<?=@$row["start_ymd"]?>','<?=@$row["apply_id"]?>'); return false;" class="always_blue"><?=$jifun?></a></td></tr>
				<? } ?>
				<? } ?>
			</table>
		</div>
	</div>
</div>
<? } ?>

<? //=========================================================================?>
<? // 検索指定領域（患者）                                                    ?>
<? //=========================================================================?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<table class="list_table" cellspacing="1">
	<tr>
		<th width="50" style="white-space:nowrap; text-align:center">患者ID</th>
		<td width="">
			<input type="text" style="width:100px; ime-mode:disabled" name="sel_ptif_id" id="sel_ptif_id" value="<?=@$_REQUEST["sel_ptif_id"]?>"/><!--
			--><span id="sel_ptif_name" style="padding-left:2px; padding-right:2px"><?=$ptif_name?></span><!--
			--><input type="button" onclick="popupKanjaSelect();" value="患者検索"/>
			<input type="button" onclick="location.href='summary_rireki.php?session=<?=$session?>&pt_id=<?=@$_REQUEST["sel_ptif_id"]?>'" value="レポート"  <?=$ptif_name ? "" : "disabled='disabled'"?> />
		</td>
		<th width="70" style="text-align:center; white-space:nowrap">
			患者名検索
		</th>
		<td width="220" style="text-align:right">
			<div style="float:left">
				<input type="text" style="width:200px" onkeyup="inquirySuggest(this.value, '', 1)" onfocus="inquirySuggest(this.value, 'show', 1);" onblur="inquirySuggest(this.value, 'hide', 1);" /><br />
				<div id="patient_suggest_div1" style="position:absolute; width:200px; z-index:1000; display:none;">
					<div id="patient_suggest_script1" style="padding-top:4px; text-align:left;"></div>
				</div>
			</div>
			<br style="clear:both" />
		</td>
		<td width="">
			<? // 入院してれば表示 ?>
			<? $nyuin_prev_next_curr_info = $c_sot_util->get_nyuin_info_with_prev_next($con, $fname, @$_REQUEST["sel_ptif_id"], date("Ymd")); ?>
			<? $tmp_nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, @$_REQUEST["sel_ptif_id"], date("Ymd")); ?>

			<? $prev_ptif_id = @$nyuin_prev_next_curr_info["prev"]["ptif_id"]; ?>
			<? if (@$nyuin_prev_next_curr_info["prev"]["ward_cd"] != $tmp_nyuin_info["ward_cd"]) $prev_ptif_id = ""; ?>
			<? $next_ptif_id = @$nyuin_prev_next_curr_info["next"]["ptif_id"]; ?>
			<? if (@$nyuin_prev_next_curr_info["next"]["ward_cd"] != $tmp_nyuin_info["ward_cd"]) $next_ptif_id = ""; ?>
			<div style="float:left; padding:4px;">
				<? if ($prev_ptif_id){ ?>
				<a href="" class="always_blue" onclick="document.search.sel_ptif_id.value='<?=$prev_ptif_id?>'; document.search.submit(); return false;">前患者</a>
				<? } else { ?>
				<span style="color:#aaa; white-space:nowrap">前患者</span>
				<? } ?>
			</div>
			<div style="float:left; padding:4px;">
					<? if ($next_ptif_id){ ?>
					<a href="" class="always_blue" onclick="document.search.sel_ptif_id.value='<?=$next_ptif_id?>'; document.search.submit(); return false;">次患者</a>
					<? } else { ?>
					<span style="color:#aaa; white-space:nowrap">次患者</span>
					<? } ?>
			</div>
			<div style="clear:both"></div>
		</td>
	</tr>
</table>
</form>

</div><!-- end of [class="print_preview_hidden"] -->

<? //=========================================================================?>
<? // 患者名サジェスト                                                        ?>
<? //=========================================================================?>
<script type="text/javascript">

<?//--------------------------------------------?>
<?// 患者名サジェスト：サーバへ送信             ?>
<?//--------------------------------------------?>
var suggestExistResult = false;
var suggestLinkClicked = false;
var currentSuggestString = "";
function inquirySuggest(ptif_name, disp, no){
	var curDisp = document.getElementById("patient_suggest_div" + no).style.display;
	var newDisp = (disp=="hide" && !suggestExistResult ? "none" : "");
	if (curDisp != newDisp) {
		document.getElementById("patient_suggest_div" + no).style.display = newDisp;
	}
	if (disp=="hide") {
		currentSuggestString = "";
		return;
	}

	if (ptif_name=="") {
		suggestExistResult = false;
		document.getElementById("patient_suggest_script" + no).innerHTML = '' +
			'<div class="suggest_div">'+
			'・患者名/患者IDを指定してください<br/>'+
			'・指定事業所/病棟範囲内で検索<br/>'+
			'・本日入院中の患者、該当10件まで</div>';
		currentSuggestString = "";
		return;
	}

	if (currentSuggestString == ptif_name) return;
	currentSuggestString = ptif_name;

	var postval =
		"session=<?=$session?>"+
		"&sel_ptif_name="+encodeURIComponent(encodeURIComponent(ptif_name)) +
		"&sel_bldg_cd="+document.search.sel_bldg_cd.value +
		"&sel_ward_cd="+document.search.sel_ward_cd.value;
	if (window.XMLHttpRequest) ajaxObj = new XMLHttpRequest();
	else {
		try { ajaxObj = new ActiveXObject("Msxml2.XMLHTTP"); }
		catch(e) { ajaxObj = new ActiveXObject("Microsoft.XMLHTTP"); }
	}
  ajaxObj.onreadystatechange = function(){returnInquirySuggest(no);};
  ajaxObj.open("POST", "sot_kanja_suggest.php", true);
  ajaxObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  ajaxObj.send(postval);
}
<?//--------------------------------------------?>
<?// 患者名サジェスト：サーバからの応答リスト   ?>
<?//--------------------------------------------?>
function returnInquirySuggest(no){
	if (ajaxObj == null) return;
	if (ajaxObj.readyState != 4) return;
	if (typeof(ajaxObj.status) != "number") return;
	if (ajaxObj.status != 200) return;
	if (!ajaxObj.responseText) return;
	try{ var ret = false; eval("ret = " + ajaxObj.responseText); if (ret.error) { alert(ret.error); return; }}
	catch(e){ alert("[response invalid]\n"+ajaxObj.responseText); return; }
	if (!ret) return;
	var out = [];
	if (!ret.list_length) {
		suggestExistResult = false;
		document.getElementById("patient_suggest_script" + no).innerHTML = "<div class='suggest_div'>（該当はありません）</div>";
		return;
	}
	out.push('<table class="suggest_result">');
	for (var i = 0; i < ret.list_length; i++){
		out.push('<tr><th>'+ret.list[i].id+'</th><td><a href="" class="always_blue" onclick="suggestLinkClick(\''+ret.list[i].id+'\'); return false;">'+ret.list[i].name+'</a></td></tr>');
	}
	out.push("</table>");
	document.getElementById("patient_suggest_script" + no).innerHTML = out.join("");
	suggestExistResult = true;
}

<?//--------------------------------------------?>
<?// 患者名サジェスト：サジェストの患者IDのクリック   ?>
<?//--------------------------------------------?>
function suggestLinkClick(ptif_id){
	suggestLinkClicked = true;
	callbackKanjaSelect(ptif_id);
}

</script>



<? //=========================================================================?>
<? // 印刷用ヘッダ                                                            ?>
<? //=========================================================================?>
<div style="font-size:20px; text-align:right" id="print_title_header" class="print_preview_display">個人情報用紙（<?=(int)$date_m?>月）</div>
<div style="font-size:14px; text-align:left; padding-bottom:4px" class="print_preview_display">病棟名：<?=@$ward_name?>　　　氏名：<?=@$ptif_name?></div>


<? //*************************************************************************?>
<? // 一覧表の作成                                                            ?>
<? //*************************************************************************?>
<div id="ondoban_pane" style="display:none">
<table id="ondoban_body" class="ondo_base_table" style="position:absolute"><tr><td style="padding:0">
<table id="tbl1" class="tbl1">

<? //=========================================================================?>
<? // 一覧表のセル幅をそろえるための非表示TDタグ一覧                          ?>
<? // ⇒colspan/rowspanを使うとうまく制御できないため。                       ?>
<? //=========================================================================?>
<tr>
	<td id="tdA" style="width:20px"></td><td id="tdB" style="width:91px"></td>
  <? for ($i = 1; $i <= 42; $i++){ ?>
  <td id="td<?=$i?>"></td>
  <? } ?>
</tr>

<? //=========================================================================?>
<? // ボタン１                                                                ?>
<? //=========================================================================?>
<tr class="print_preview_hidden" style="height:24px">
    <td colspan="2" style="height:24px">&nbsp;</td>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <td colspan="3" class="btn_care" onclick="if (!suggestLinkClicked) popupCareVital('care','<?=@$care[$i]["yyyymmdd"]?>', '<?=@$care[$i]["db"]["apply_id"]?>');"><div style="margin:1px">Ｃ</div></td>
    <td colspan="3" class="btn_vital" onclick="if (!suggestLinkClicked) showVitalSelector(this, <?=$i?>, '<?=@$care[$i]["yyyymmdd"]?>');"><div style="margin:1px">Ｖ</div></td>
    <? } ?>
</tr>

<? //=========================================================================?>
<? // 日付                                                                    ?>
<? //=========================================================================?>
<tr class="rowhead">
    <td colspan="2" id="td0" class="d btmborder2px" style="width:116px"><br/></td>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <td colspan="6" id="tdH<?=$i?>" class="d btmborder2px" style="text-align:center; white-space:nowrap"><?=@$care[$i]["date"]?></td>
    <? } ?>
</tr>
<? //=========================================================================?>
<? // 目盛り画像と折れ線メイングラフ                                          ?>
<? //=========================================================================?>
<tr>
    <th colspan="2" style="border:1px solid <?=$border_color?>; border-bottom:2px solid <?=$border_color?>; padding:0px; text-align:left; vertical-align:top; width:152px"><div style="border:1px solid #cbe2f5; width:150px; height:300px;" id="graph_head_canvas_div"><canvas width="150" height="300" id="graph_head_canvas" style="width:150px; height:300px"></canvas></div></th>
    <th style="border:1px solid <?=$border_color?>; border-bottom:2px solid <?=$border_color?>; padding:0px; text-align:left; vertical-align:top;" colspan="42"><div style="border:1px solid #cbe2f5" id="graph_main_canvas_div"><canvas width="400" height="300" id="graph_main_canvas" onmousemove="showOndobanTooltip(event)" onmouseout="tooltip.style.display='none';"></canvas></div>
</tr>

<? //=========================================================================?>
<? // ボタン２                                                                ?>
<? //=========================================================================?>
<tr class="print_preview_hidden" style="height:25px">
    <td colspan="2" style="height:24px">&nbsp;</td>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <td colspan="3" class="btn_care" onclick="popupCareVital('care','<?=@$care[$i]["yyyymmdd"]?>', '<?=@$care[$i]["db"]["apply_id"]?>');"><div style="margin:1px">Ｃ</div></td>
    <td colspan="3" class="btn_vital" onclick="showVitalSelector(this, <?=$i?>, '<?=@$care[$i]["yyyymmdd"]?>');"><div style="margin:1px">Ｖ</div></td>
    <? } ?>
</tr>

<tr>
    <td colspan="2" class="cent d rowhead" style="vertical-align:middle;"><div>酸素吸入</div></td>
    <th style="border:1px solid <?=$border_color?>; padding:0px; text-align:left; vertical-align:top; height:24px" colspan="42"><div style="border:1px solid #cbe2f5" id="graph_sanso_canvas_div"><canvas id="graph_sanso_canvas" height="24"></canvas></div></th>
</tr>
<tr>
    <td colspan="2" class="cent d rowhead"><div>心電図モニター</div></td>
    <th style="border:1px solid <?=$border_color?>; padding:0px; text-align:left; vertical-align:top; height:24px" colspan="42"><div style="border:1px solid #cbe2f5" id="graph_sinden_canvas_div"><canvas id="graph_sinden_canvas" height="24"></canvas></div></th>
</tr>
<tr>
    <td colspan="2" class="cent d rowhead"><div>呼吸器</div></td>
    <th style="border:1px solid <?=$border_color?>; padding:0px; text-align:left; vertical-align:top; height:24px" colspan="42"><div style="border:1px solid #cbe2f5" id="graph_kokyuki_canvas_div"><canvas id="graph_kokyuki_canvas" height="24"></canvas></div></th>
</tr>
<tr>
    <td colspan="2" class="cent d rowhead"><div>尿（回数・ml）</div></div></td>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <td colspan="6" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_nyou_ondoban"]->value)?></td>
    <? } ?>
</tr>
<tr>
    <td colspan="2" class="cent d rowhead"><div>便（回数）</div></td>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <td colspan="6" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_ben_ondoban"]->value)?></td>
    <? } ?>
</tr>
<tr>
    <td colspan="2" class="cent d rowhead"><div>排液（対象+回数・ml）</div></td>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <td colspan="6" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_haieki_ondoban"]->value)?></td>
    <? } ?>
</tr>
<tr>
    <td colspan="2" class="cent d rowhead"><div>経口摂取量</div></td>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <td colspan="2" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_sessyu_asa_ondoban"]->value)?></td>
    <td colspan="2" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_sessyu_hiru_ondoban"]->value)?></td>
    <td colspan="2" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_sessyu_yu_ondoban"]->value)?></td>
    <? } ?>
</tr>
<tr>
    <td colspan="2" class="cent d rowhead"><div>経管摂取量</div></td>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <td colspan="2" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_sessyu2_asa_ondoban"]->value)?></td>
    <td colspan="2" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_sessyu2_hiru_ondoban"]->value)?></td>
    <td colspan="2" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_sessyu2_yu_ondoban"]->value)?></td>
    <? } ?>
</tr>
<tr>
    <td colspan="2" class="cent d rowhead"><div>補水・補食</div></td>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <td colspan="2" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_hosui_asa_ondoban"]->value,@$care[$i]["xml"]->nodes["ocar_hosui_asa_ondoban_sasiire_img"]->value)?></td>
    <td colspan="2" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_hosui_hiru_ondoban"]->value,@$care[$i]["xml"]->nodes["ocar_hosui_hiru_ondoban_sasiire_img"]->value)?></td>
    <td colspan="2" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_hosui_yu_ondoban"]->value,@$care[$i]["xml"]->nodes["ocar_hosui_yu_ondoban_sasiire_img"]->value)?></td>
    <? } ?>
</tr>
<tr>
    <td colspan="2" class="cent d rowhead"><div>口腔ケア</div></td>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <td colspan="2" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_koukou_asa"]->value)?></td>
    <td colspan="2" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_koukou_hiru"]->value)?></td>
    <td colspan="2" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_koukou_yu"]->value)?></td>
    <? } ?>
</tr>

<tr>
    <td colspan="2" class="cent d rowhead"><div>入浴</div></td>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <td colspan="3" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_nyuyoku_gozen"]->value)?></td>
    <td colspan="3" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_nyuyoku_gogo"]->value)?></td>
    <? } ?>
</tr>

<tr>
    <td colspan="2" class="cent d rowhead btmborder2px"><div>整容</div></td>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <td colspan="3" class="d btmborder2px"><?=wbr(@$care[$i]["xml"]->nodes["ocar_seiyou_gozen"]->value)?></td>
    <td colspan="3" class="d btmborder2px"><?=wbr(@$care[$i]["xml"]->nodes["ocar_seiyou_gogo"]->value)?></td>
    <? } ?>
</tr>

<tr class="f10px">
    <td colspan="2" style="padding:1px" class="d rowhead"><div><br/></div></td>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <td colspan="3" class="cent d pad1 rowhead" style="text-align:center">日勤</td>
    <td colspan="3" class="cent d pad1 rowhead" style="text-align:center">夜勤</td>
    <? } ?>
</tr>

<tr>
    <td colspan="2" class="cent d rowhead"><div>吸引回数</div></td>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <td colspan="3" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_kyuin_day"]->value)?></td>
    <td colspan="3" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_kyuin_night"]->value)?></td>
    <? } ?>
</tr>





<? //=========================================================================?>
<? // 処置                                                                    ?>
<? //=========================================================================?>
<? $is_toprow = true; ?>
<? foreach ($tmpl_names as $chapter_code => $caption){ ?>
<tr>
    <?if ($is_toprow) { ?><td colspan="1" rowspan="4" class="d rowhead" style="vertical-align:middle"><div>処<div>置</div></td><? } ?>
    <td colspan="1" class="d rowhead"><div><?=$caption?></div></td>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <td colspan="3" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_syoti_".$chapter_code."_day"]->value)?></td>
    <td colspan="3" class="d"><?=wbr(@$care[$i]["xml"]->nodes["ocar_syoti_".$chapter_code."_night"]->value)?></td>
    <? } ?>
</tr>
<? $is_toprow = false; ?>
<? } ?>


<? //=========================================================================?>
<? // 観察項目１（タイトルがある観察項目）                                    ?>
<? //=========================================================================?>
<? $is_toprow = true; ?>
<? $rowspan = count($kansatu_index_list) + $kansatu_blank_data_max_size ?>
<? for ($row=0; $row < count($kansatu_index_list); $row++){ ?>
<?   $k_title = $kansatu_index_list[$row]; ?>
<tr>
    <?if ($is_toprow) { ?><td colspan="1" rowspan="<?=$rowspan?>" class="d rowhead" style="vertical-align:middle"><div>観<br/>察<br/>項<br/>目</div></td><? } ?>
    <td colspan="1" class="d rowhead"><div><?=$k_title?></div></td>
    <? for ($i=1; $i<=7; $i++){ ?>
    <td colspan="3" class="d"><?=wbr(@$kansatu_data[$i][$k_title]["day"])?></td>
    <td colspan="3" class="d"><?=wbr(@$kansatu_data[$i][$k_title]["night"])?></td>
    <? } ?>
</tr>
<? $is_toprow = false; ?>
<? } ?>


<? //=========================================================================?>
<? // 観察項目２（タイトルが無い観察項目）                                      ?>
<? //=========================================================================?>
<? for ($row=0; $row < $kansatu_blank_data_max_size; $row++){ ?>
<tr>
    <?if ($is_toprow) { ?><td colspan="1" rowspan="<?=$rowspan?>" class="d rowhead" style="vertical-align:middle"><div>観<br/>察<br/>項<br/>目</div></td><? } ?>
    <td colspan="1" class="d rowhead"><div><br/></div></td>
    <? for ($i=1; $i<=7; $i++){ ?>
    <td colspan="3" class="d"><?=wbr(@$kansatu_blank_data[$i]["day"][$row])?></td>
    <td colspan="3" class="d"><?=wbr(@$kansatu_blank_data[$i]["night"][$row])?></td>
    <? } ?>
</tr>
<? $is_toprow = false; ?>
<? } ?>


<? //=========================================================================?>
<? // サイン（看護）                                                          ?>
<? //=========================================================================?>
<? if ($ptif_name){ ?>
<tr>
    <td colspan="2" class="cent d rowhead"><div>サイン（看護）</div></td>
    <?   $clr_red = (@$_REQUEST["print_preview"] ? "" : "color:#f05") ?>
    <?   $clr_yel = (@$_REQUEST["print_preview"] ? "color:#000" : "color:#880") ?>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <?   $ymd = @$care[$i]["yyyymmdd"]; ?>
    <?   $nikkin_kango_emp_id = @$care[$i]["db"]["nikkin_kango_accepted_emp_id"] ?>
    <?   $yakin_kango_emp_id = @$care[$i]["db"]["yakin_kango_accepted_emp_id"] ?>
    <td colspan="3" class="d">
	    <input type="hidden" id="sign_<?=$ymd?>_nikkin_kango_emp_id" value="<?=$nikkin_kango_emp_id?>" />
	    <?
    		$script =
    			' <a href="" id="sign_'.$ymd.'_nikkin_kango"'.
    			' onclick="inquiryRegist(\'nikkin\', \''.$ymd.'\',\'kango\'); return false;"';
    		if ($nikkin_kango_emp_id !="") {
					$script .= ' style="'.$clr_yel.'">'.wordBreak(@$care[$i]["db"]["nikkin_kango_emp_name"]).'</a>';
				} else {
					$script .= ' style="'.$clr_red.'" class="print_preview_hidden">確認</a>';
				}
				echo wrapByUniqueDiv($script);
	  ?></td>
    <td colspan="3" class="d">
	    <input type="hidden" id="sign_<?=$ymd?>_yakin_kango_emp_id" value="<?=$yakin_kango_emp_id?>" />
    	<?
    		$script =
    			' <a href="" id="sign_'.$ymd.'_yakin_kango"'.
    			' onclick="inquiryRegist(\'yakin\', \''.$ymd.'\',\'kango\'); return false;"';
    		if ($yakin_kango_emp_id !="") {
					$script .= ' style="'.$clr_yel.'">'.wordBreak(@$care[$i]["db"]["yakin_kango_emp_name"]).'</a>';
				} else {
					$script .= ' style="'.$clr_red.'" class="print_preview_hidden">確認</a>';
				}
				echo wrapByUniqueDiv($script);
	  ?></td>
    <? } ?>
</tr>
<? } ?>


<? //=========================================================================?>
<? // サイン（介護）                                                          ?>
<? //=========================================================================?>
<? if ($ptif_name){ ?>
<tr>
    <td colspan="2" class="cent d rowhead"><div>サイン（介護）</div></td>
    <?   $clr_red = (@$_REQUEST["print_preview"] ? "" : "color:#f05") ?>
    <?   $clr_yel = (@$_REQUEST["print_preview"] ? "color:#000" : "color:#880") ?>
    <? for ($i = 1; $i <= 7; $i++){ ?>
    <?   $ymd = @$care[$i]["yyyymmdd"]; ?>
    <?   $nikkin_kaigo_emp_id = @$care[$i]["db"]["nikkin_kaigo_accepted_emp_id"] ?>
    <?   $yakin_kaigo_emp_id = @$care[$i]["db"]["yakin_kaigo_accepted_emp_id"] ?>
    <td colspan="3" class="d">
	    <input type="hidden" id="sign_<?=$ymd?>_nikkin_kaigo_emp_id" value="<?=$nikkin_kaigo_emp_id?>" />
	    <?
    		$script =
    			' <a href="" id="sign_'.$ymd.'_nikkin_kaigo"'.
    			' onclick="inquiryRegist(\'nikkin\', \''.$ymd.'\',\'kaigo\'); return false;"';
    		if ($nikkin_kaigo_emp_id !="") {
					$script .= ' style="'.$clr_yel.'">'.wordBreak(@$care[$i]["db"]["nikkin_kaigo_emp_name"]).'</a>';
				} else {
					$script .= ' style="'.$clr_red.'" class="print_preview_hidden">確認</a>';
				}
				echo wrapByUniqueDiv($script);
	  ?></td>
    <td colspan="3" class="d">
	    <input type="hidden" id="sign_<?=$ymd?>_yakin_kaigo_emp_id" value="<?=$yakin_kaigo_emp_id?>" />
    	<?
    		$script =
    			' <a href="" id="sign_'.$ymd.'_yakin_kaigo"'.
    			' onclick="inquiryRegist(\'yakin\', \''.$ymd.'\',\'kaigo\'); return false;"';
    		if ($yakin_kaigo_emp_id !="") {
					$script .= ' style="'.$clr_yel.'">'.wordBreak(@$care[$i]["db"]["yakin_kaigo_emp_name"]).'</a>';
				} else {
					$script .= ' style="'.$clr_red.'" class="print_preview_hidden">確認</a>';
				}
				echo wrapByUniqueDiv($script);
	  ?></td>
    <? } ?>
</tr>
<? } ?>


</table>
</td></tr></table>
</div>

<? //=========================================================================?>
<? // 患者検索フォーム（下部）                                                ?>
<? //=========================================================================?>
<img src="img/spacer.gif" width="1" height="2" alt="">
<table class="list_table" cellspacing="1">
	<tr>
		<th width="50" style="white-space:nowrap; text-align:center">患者ID</th>
		<td width="">
			<input type="text" style="width:100px; ime-mode:disabled" name="sel_ptif_id" id="sel_ptif_id" value="<?=@$_REQUEST["sel_ptif_id"]?>"/><!--
			--><span id="sel_ptif_name" style="padding-left:2px; padding-right:2px"><?=$ptif_name?></span><!--
			--><input type="button" onclick="popupKanjaSelect();" value="患者検索"/>
			<input type="button" onclick="location.href='summary_rireki.php?session=<?=$session?>&pt_id=<?=@$_REQUEST["sel_ptif_id"]?>'" value="レポート"  <?=$ptif_name ? "" : "disabled='disabled'"?> />
		</td>
		<th width="70" style="text-align:center; white-space:nowrap">
			患者名検索
		</th>
		<td width="220" style="text-align:right">
			<div style="float:left">
				<input type="text" style="width:200px" onkeyup="inquirySuggest(this.value, '', 2)" onfocus="inquirySuggest(this.value, 'show', 2);" onblur="inquirySuggest(this.value, 'hide', 2);" /><br />
				<div id="patient_suggest_div2" style="position:absolute; width:200px; z-index:1000; display:none;">
					<div id="patient_suggest_script2" style="padding-top:4px; text-align:left;"></div>
				</div>
			</div>
			<br style="clear:both" />
		</td>
		<td width="">
			<? // 入院してれば表示 ?>
			<div style="float:left; padding:4px;">
				<? if ($prev_ptif_id){ ?>
				<a href="" class="always_blue" onclick="document.search.sel_ptif_id.value='<?=$prev_ptif_id?>'; document.search.submit(); return false;">前患者</a>
				<? } else { ?>
				<span style="color:#aaa; white-space:nowrap">前患者</span>
				<? } ?>
			</div>
			<div style="float:left; padding:4px;">
					<? if ($next_ptif_id){ ?>
					<a href="" class="always_blue" onclick="document.search.sel_ptif_id.value='<?=$next_ptif_id?>'; document.search.submit(); return false;">次患者</a>
					<? } else { ?>
					<span style="color:#aaa; white-space:nowrap">次患者</span>
					<? } ?>
			</div>
			<div style="clear:both"></div>
		</td>
	</tr>
</table>

<? //=========================================================================?>
<? // グラフ上のマウスオーバーロールのツールチップ制御                        ?>
<? //=========================================================================?>
<div id="ondoban_tooltip" style="position:absolute; top:0; height:0; z-index:1000; display:none"></div>


<script type="text/javascript">
	var tooltipData = [];
	var tooltip = document.getElementById("ondoban_tooltip");
	<? foreach ($tooltipData as $idx => $data){ ?>
	var div = document.createElement("DIV");
	div.id = "tooltip_detail_<?=$idx?>";
	tooltip.appendChild(div);
	div.innerHTML = ""+
		'<table style="border:0; border-collapse:collapse" class="plane"><tr><td style="border:2px solid #b70080; background-color:#fff; padding:2px;">'+
		'<table class="tooltip" style="margin:1px; background-color:#fff; border:1px solid #fbb ">'+
		"<tr><td colspan='4' style='text-align:center'><div style='border-bottom:2px solid #9d88ff; margin:3px; color:#7d62ff'><?=$data["datetime"]?></div></td></tr>"+
<? if (@$data["msg"]["体温"])   {?> "<tr><th style='color:#0000ff'>T</th><td style='color:#777; font-size:12px'>体温</td><td><?=$data["msg"]["体温"]?></td><td style='color:#aaa; font-size:12px'>℃</td></tr>"+      <? } ?>
<? if (@$data["msg"]["脈拍"])   {?> "<tr><th style='color:#ff0000'>P</th><td style='color:#777; font-size:12px'>脈拍</td><td><?=$data["msg"]["脈拍"]?></td><td style='color:#aaa; font-size:12px'>回/分</td></tr>"+      <? } ?>
<? if (@$data["msg"]["呼吸数"]) {?> "<tr><th style='color:#008822'>R</th><td style='color:#777; font-size:12px'>呼吸数</td><td><?=$data["msg"]["呼吸数"]?></td><td style='color:#aaa; font-size:12px'>回/分</td></tr>"+  <? } ?>
<? if (@$data["msg"]["血圧"])   {?> "<tr><th style='color:#8a8a41'>BP</th><td style='color:#777; font-size:12px'>血圧</td><td><?=$data["msg"]["血圧"]?></td><td style='color:#aaa; font-size:12px'>mmHg</td></tr>"+      <? } ?>
<? if (@$data["msg"]["SPO2"])   {?> "<tr><th style='color:#1b9cab'>SP</th><td style='color:#777; font-size:12px'>SPO2</td><td><?=$data["msg"]["SPO2"]?></td><td style='color:#aaa; font-size:12px'>％</td></tr>"+      <? } ?>
		"</table></tr></td></table>";
	tooltipData.push({"left":"<?=$data["left"]?>", "leftpx":0, "elem":div});
	<? } ?>

	var GRAPH_LEFT = 152; // 適当
	var GRAPH_TOP = 220; // 適当
	var MOUSE_MARGIN = 20; // 適当
	var GRAPH_MAX_TIME_WIDTH = 60*24*7; // 7日分

	var graphMaxWidth = 0;
	function prepareTooltipX(graphWidth){
		graphMaxWidth = graphWidth;
		for (var t in tooltipData){
			tooltipData[t].leftpx = tooltipData[t].left / GRAPH_MAX_TIME_WIDTH * graphWidth;
		}
	}


	var uAgent = navigator.userAgent.toLowerCase();
	var isIE = (uAgent.indexOf("msie") != -1);

	function showOndobanTooltip(event){
		var isVisibleExist = false;
		var posx = (event.offsetX || event.x || event.layerX);
		var posy = (event.offsetY || event.y || event.layerY);
		// IECanvasの副作用？ canvas上のオブジェクトにも反応してしまう。このためoffset/layerでない座標で判断。
		if (isIE) { posx = event.x; posy = event.y; }
		if (!posx || !posy) return;
		for (var t in tooltipData){
			var isVisible = (Math.abs(tooltipData[t].leftpx - posx) < 7);
			if (isVisible) isVisibleExist = true;
			tooltipData[t].elem.style.display = isVisible ? "" : "none";
		}
		tooltip.style.display = isVisibleExist ? "" : "none";
		if (isVisibleExist){
			tooltip.style.height = "";
			tooltip.style.width = "";
			var ttWidth = (tooltip.offsetWidth || tooltip.clientWidth);
			var halfWidth = ttWidth / 2;
			var ttHeight = (tooltip.offsetHeight || tooltip.clientHeight);
			var l = parseInt(posx) + GRAPH_LEFT - halfWidth;
			if (l + ttWidth > graphMaxWidth + GRAPH_LEFT) l = graphMaxWidth + GRAPH_LEFT - ttWidth;
			var t = parseInt(posy) + GRAPH_TOP - parseInt(ttHeight) - MOUSE_MARGIN;
			tooltip.style.left = l+"px";
			tooltip.style.top  = t+"px";
		}
	}
</script>



<? //=========================================================================?>
<? // 一覧表のセル幅をそろえ、改行したり文字の大きさを変更するJavaScript      ?>
<? //=========================================================================?>
<div id="size_adjust_workarea_div" style="display:none"></div>
<script type="text/javascript">
	var divlist = [];
	var parentList = [];
	var workArea = document.getElementById("size_adjust_workarea_div");
<? for ($i=1; $i<=$global_index; $i++){ ?>
	var tmpd = document.getElementById("div<?=$i?>");
	divlist.push(tmpd);
	parentList.push(tmpd.parentNode);
<? } ?>
	function setDivWidths(){
		var divlen = divlist.length;
		var parentWList = [];
		var pmargin = <?= @$_REQUEST["print_preview"] ? "12" : "4" ?>;
		for (var i=0; i<divlen; i++){
			parentWList.push((parentList[i].clientWidth-pmargin) + "px");
		}
		for (var i=0; i<divlen; i++){
			divlist[i].style.width = parentWList[i];
		}

		workArea.style.display = "";
		for (var i=0; i<divlen; i++){
			var div = divlist[i];
			div.style.display = "";
			<?//リサイズにより、DIVのフォントが変更されている場合は、戻す ?>
			if (div.style.fontSize != "13px") div.style.fontSize = "13px";
			<?//DIVの高さが1行以下なら終了 ?>
			if (div.clientHeight < 26) continue;

			<?//文字を小さくしてみる ?>
			div.style.fontSize = "12px";
			div.style.display = "none";
			div.style.display = "";
			<?//DIVの高さが1行以下なら終了 ?>
			if (div.clientHeight < 24) continue;
			<?//文字を小さくしてみる ?>
			div.style.fontSize = "11px";
			div.style.display = "none";
			div.style.display = "";
			<?//DIVの高さが1行以下になったら終了 ?>
			if (div.clientHeight < 22) continue;
			<?//文字を小さくしてみる ?>
			div.style.fontSize = "10px";
<? if (@$_REQUEST["print_preview"]) {?>
			div.style.display = "none";
			div.style.display = "";
			<?//DIVの高さが1行以下になったら終了 ?>
			if (div.clientHeight < 20) continue;
			<?//印刷時はもう一声、文字を小さくしてみる ?>
			div.style.fontSize = "9px";
			div.style.display = "none";
			div.style.display = "";
			<?//DIVの高さが1行以下になったら終了 ?>
			if (div.clientHeight < 18) continue;
			<?//印刷時はもう一声、文字を小さくしてみる ?>
			div.style.fontSize = "8px";
<? } ?>
			div.style.display = "none";
			div.style.display = "";
			<?//高く ?>
		}
		workArea.style.display = "none";

		for (var i=0; i<divlen; i++){
			parentList[i].appendChild(divlist[i]);
		}
	}
</script>
</body>
<? pg_close($con); ?>
</html>
