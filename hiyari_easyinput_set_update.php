<?php
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');

require_once("about_comedix.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("smarty_setting.ini");
require_once("hiyari_report_class.php");
require_once('class/Cmx/Model/SystemConfig.php');
require_once("aclg_set.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$hiyari = check_authority($session, 48, $fname);
if ($hiyari == "0")
{
    showLoginPage();
    exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==============================
// セッション開始
//==============================
session_name("hyr_sid");
session_start();

//==============================
//ポストバック時の処理
//==============================
if( $postback != "")
{
    //==============================
    //様式名の必須チェック
    //==============================

    if($eis_name == "")
    {
        echo("<script language='javascript'>alert(\"様式名称を入力してください。\");</script>");
        echo("<script language='javascript'>history.back();</script>");
        pg_close($con);
        exit;
    }

    //==============================
    //様式名の重複チェック
    //==============================

    $sql = "select * from inci_easyinput_set where not del_flag and eis_name = '". pg_escape_string($eis_name) ."' and not eis_no = $eis_no";
    $sel_chk = select_from_table($con,$sql,"",$fname);
    if($sel_chk == 0)
    {
        pg_close($con);
        showErrorPage();
        exit;
    }
    $chk_count = pg_numrows($sel_chk);
    if($chk_count != 0)
    {
        echo("<script language='javascript'>alert(\"様式名称が重複しています。様式名称を変更して下さい。\");</script>");
        echo("<script language='javascript'>history.back();</script>");
        pg_close($con);
        exit;
    }

    //==============================
    //カテゴリ名の必須チェック
    //==============================
    if(isset($user_cate_name) && $user_cate_name == "")
    {
        echo("<script language='javascript'>alert(\"カテゴリ名称を入力してください。\");</script>");
        echo("<script language='javascript'>history.back();</script>");
        pg_close($con);
        exit;
    }

    //==============================
    //様式の更新(論理削除＆インサート)
    //==============================

    //トランザクション開始
    pg_query($con,"begin transaction");

    //様式情報の更新
    $rep_obj = new hiyari_report_class($con, $fname);

    // 報告書様式登録
    if(count($patient_use_easy_item_code) > 0) {
        $use_grp_code[] = "210";
    }
    if(count($summary_use_item_grp_code) > 0) {
        $use_grp_code[] = "400";
    }
    if(count($asses_use_easy_item_code) > 0) {
        $use_grp_code[] = "140";
    }
    if(count($place_use_easy_item_code) > 0) {
        $use_grp_code[] = "110";
    }
    // order_no を取得する
    $sql = "select order_no from inci_easyinput_set where not del_flag and eis_name = '". pg_escape_string($eis_name) ."' and eis_no = $eis_no";
    $sel_order_no = select_from_table($con,$sql,"",$fname);
    if($sel_chk == 0)
    {
        pg_close($con);
        showErrorPage();
        exit;
    }
    $order_no = pg_fetch_result($sel_order_no, 0, "order_no");

    $eis_id = $rep_obj->set_report_flags($eis_name,$use_grp_code,$style_code,$must_grp_code,$user_cate_name,$eis_no,$order_no);

    // 患者プロフィール・項目使用および必須設定登録
    $rep_obj->set_report_flags_for_easy_item_code($eis_id, 210, $patient_use_easy_item_code);
    $rep_obj->set_item_must_for_easy_item_code($eis_id, 210, $patient_must_easy_item_code);
    $rep_obj->set_report_flags_for_easy_item_code($eis_id, 150, $asses_use_easy_item_code);
    $rep_obj->set_item_must_for_easy_item_code($eis_id, 150, $asses_must_easy_item_code);
    $rep_obj->set_item_must_for_easy_item_code($eis_id, 700, $info_must_easy_item_code);
    $rep_obj->set_item_must_for_easy_item_code($eis_id, 800, $item_must_easy_item_code);
    $rep_obj->set_item_must_for_easy_item_code($eis_id, 900, $item_must_2010_easy_item_code);

    //発生場所
    if (in_array("60", $place_use_easy_item_code)){
        $place_use_easy_item_code[] = "70";
    }
    $rep_obj->set_report_flags_for_easy_item_code($eis_id, 110, $place_use_easy_item_code);

    //患者影響レベル 20140423 R.C
    $rep_obj->insert_inci_easyinput_other_disp($eis_id, 90, 10, $patient_level_names);
    
    //インシデントの概要の項目コード・項目使用および必須設定登録
    $rep_obj->set_eis_400_use($eis_id,$summary_use_item_grp_code);
    $rep_obj->set_eis_400_must($eis_id,$summary_must_item_grp_code);

    // デフォルト値保存
    $old_eis_id = $_POST['eis_id'];
    $rep_obj->set_default_value($eis_id, $_POST, $old_eis_id);

    // 項目名の変更 
    foreach ($_POST as $param_name => $value){
        if (starts_with($param_name, "name_")){
            $eis_column = substr($param_name, strlen("name_"));
            if (!empty($value)){
                set_item_title($con, $fname, $eis_id, $value, $eis_column);
            }
            continue;
        }
        if (starts_with($param_name, "letter_")){
            $eis_column = substr($param_name, strlen("letter_"));
            if (!empty($value)){
                set_item_title($con, $fname, $eis_id, $value, $eis_column);
            }
            continue;
        }
    }

    // 表題入力設定
    if($_POST['subject']!="") set_subject_mode($con, $fname, $eis_id, $_POST['subject']);

    // 今期のテーマを挿入
    $rep_obj->set_this_term_theme($eis_no, $_POST['this_term_theme']);

    // ユーザー定義カテゴリ項目(グループ)の並び順設定
    set_group_order($con, $fname, $eis_id, $use_grp_code);

    //トランザクション終了
    pg_query($con, "commit");

    //==============================
    //処理続行して画面再表示。
    //==============================
}


//========================================
//カテゴリグループ項目情報の取得
//========================================
$rep_obj = new hiyari_report_class($con, $fname);
$cate_list = $rep_obj->get_report_category_list();

//========================================
//様式番号より、様式データを取得する。
//========================================
$sql = "select * from inci_easyinput_set where not del_flag and eis_no = $eis_no";
$sel_eis = select_from_table($con,$sql,"",$fname);
$eis_name = pg_result($sel_eis,0,"eis_name");
$eis_id = pg_result($sel_eis,0,"eis_id");
$user_cate_name = pg_result($sel_eis,0,"user_cate_name");
$style_code = pg_result($sel_eis,0,"style_code");
if (!$style_code) $style_code=1;

$flags = $rep_obj->get_report_flags( $eis_id );
$must_flags = $rep_obj->get_item_must_grp_from_eis_id( $eis_id );


// ユーザー定義カテゴリ名変更
if(!empty($user_cate_name)){
    $cate_list_count = count($cate_list);
    for ($idx = 0; $idx < $cate_list_count; $idx++){
        if ($cate_list[$idx]['cate_code'] == 10){
            $cate_list[$idx]['cate_name'] = $user_cate_name;
            break;
        }
    }
}

// ユーザー定義項目並び替え
sort_user_cate_group($cate_list, get_group_order($con, $fname, $eis_id));

// 文言変更データを取得する
if(!empty($eis_id)) $tmp_eis_item_title = get_item_title($con, $fname, $eis_id);
foreach($tmp_eis_item_title as $val) {
    $eis_item_title[$val['eis_column']] = $val['eis_title'];
}

// 文字制限データを取得する
if(!empty($eis_id)) $tmp_number_of_charcter = get_number_of_charcter($con, $fname, $eis_id);
foreach($tmp_number_of_charcter as $val) {
    $eis_number_of_charcter[$val['group_code']] = $val['number_of_characters'];
}


// 表題モード情報を取得する
if(!empty($eis_id)) $subject = get_subject_mode($con, $fname, $eis_id);

//インシデントの概要の項目グループ情報
$summary_use = $rep_obj->get_eis_400_use($eis_id);
$summary_must = $rep_obj->get_eis_400_must($eis_id);

// 今期のテーマを取得する
$theme = $rep_obj->get_this_term_theme($eis_no);
//========================================
//スマーティ設定
//========================================
$smarty = new Cmx_View();
$smarty->assign("session", $session);
$smarty->assign("fname", $fname);
$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("hyr_sid", session_id());
$smarty->assign("cate_list", $cate_list);

//様式データ情報
$smarty->assign("eis_no", $eis_no);
$smarty->assign("eis_name", $eis_name);
$smarty->assign("flags", $flags['use_grp_code']);
$smarty->assign("must_flags", $must_flags);
$smarty->assign("eis_id", $eis_id);

//報告書デザインとスタイル
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');
$smarty->assign("design_mode", $design_mode);
if ($design_mode == 2){
    $style_code = 2;
}
$smarty->assign("style_code", $style_code);

// 表題モード情報
$smarty->assign("subject", $subject);

// 文言変更用データ
$smarty->assign('eis_item_title', $eis_item_title);

// 文字制限データ
$smarty->assign('eis_number_of_charcter',$eis_number_of_charcter);

// 患者プロフィール情報
$smarty->assign("item_mst_patient", $rep_obj->get_inci_easyinput_item_mst(210));
$smarty->assign("patient_use", $rep_obj->get_inci_easyinput_set2($eis_id, 210));
$smarty->assign("patient_must", $rep_obj->get_item_must_for_easy_item_code($eis_id, 210));

//アセスメント・患者の状態情報 20090309
// 各項目の並び順は、並び順のフィールドを持っていないため、SQLでコントロールできない
// コード上で、明示的に順番の入れ替えを行っている
$item_mst_asses = $rep_obj->get_inci_easyinput_item_mst(150);
$item_mst_asses_buf = $item_mst_asses[1];
$item_mst_asses[1]  = $item_mst_asses[5];
$item_mst_asses[5]  = $item_mst_asses_buf;
$smarty->assign("item_mst_asses", $item_mst_asses);
$smarty->assign("asses_use", $rep_obj->get_inci_easyinput_set2($eis_id, 150));
$smarty->assign("asses_must", $rep_obj->get_item_must_for_easy_item_code($eis_id, 150));

//インシデントの概要の項目グループ情報
$smarty->assign("summary_use", $summary_use);
$smarty->assign("summary_must", $summary_must);

//発生場所詳細
$place_items = $rep_obj->get_inci_easyinput_set2($eis_id, 110);
$smarty->assign("is_place_detail_use", in_array(65, $place_items) ? true : false);

// 今期のテーマを送信
$smarty->assign("theme", $theme[0]);

//デフォルト値
$smarty->assign("default", $rep_obj->get_default_value($eis_id));

//概要2010年版
$method = $rep_obj->get_all_super_item_2010();
$kind_2010=array();
$kind_item_2010=array();
$scene_2010=array();
$scene_item_2010=array();
$content_2010=array();
$content_item_2010=array();
foreach($method as $value) {
    // 種類
    $param = $rep_obj->get_kind_2010($value['super_item_id']);
    if ($param) $kind_2010[$value['super_item_id']] = $param;
    if (is_array($param)){
        foreach($param as $value2) {
            $item = $rep_obj->get_kind_item_2010($value2['item_id']);
            if ($item) $kind_item_2010[$value2['item_id']] = $item;
        }
    }

    // 発生場面
    $param = $rep_obj->get_scene_2010($value['super_item_id']);
    if ($param) $scene_2010[$value['super_item_id']] = $param;
    if (is_array($param)){
        foreach($param as $value2) {
            $item = $rep_obj->get_scene_item_2010($value2['item_id']);
            if ($item) $scene_item_2010[$value2['item_id']] = $item;
        }
    }

    // 事例の内容
    $param = $rep_obj->get_content_2010($value['super_item_id']);
    if ($param) $content_2010[$value['super_item_id']] = $param;
    if (is_array($param)){
        foreach($param as $value2) {
            $item = $rep_obj->get_content_item_2010($value2['item_id']);
            if ($item) $content_item_2010[$value2['item_id']] = $item;
        }
    }
}
$smarty->assign("method", $method);
$smarty->assign("kind_2010", $kind_2010);
$smarty->assign("kind_item_2010", $kind_item_2010);
$smarty->assign("scene_2010", $scene_2010);
$smarty->assign("scene_item_2010", $scene_item_2010);
$smarty->assign("content_2010", $content_2010);
$smarty->assign("content_item_2010", $content_item_2010);

//患者影響レベル
$level_infos = $rep_obj->get_inci_level_infos("",true);
$smarty->assign("level_use", $rep_obj->get_inci_easyinput_other_disp($eis_id, 90, 10)); //項目 20140423 R.C
$smarty->assign("level_infos", $level_infos);

//========================================
//スマーティ表示
//========================================
$smarty->assign("post_back_url", "hiyari_easyinput_set_update.php");
$smarty->display("hiyari_easyinput_create.tpl");

function set_item_title($con, $fname, $eis_id, $eis_title, $eis_column) {
    $sql = "insert into inci_item_title (eis_id, eis_title, eis_column) values(";
    $content = array(pg_escape_string($eis_id), pg_escape_string($eis_title), $eis_column);
    $result = insert_into_table($con, $sql, $content, $fname);
    if ($result == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        showErrorPage();
        exit;
    }
}

function get_item_title($con, $fname, $eis_id) {
    $sql  = "select * from inci_item_title ";
    $cond = "where eis_id = " . pg_escape_string($eis_id);
    $result = select_from_table( $con, $sql, $cond, $fname );
    if ($result == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        showErrorPage();
        exit;
    }

    return pg_fetch_all($result);
}

function set_subject_mode($con, $fname, $eis_id, $mode) {
    $sql     = "insert into inci_subject_mode (eis_id, subject_mode) values (";
    $content = array(pg_escape_string($eis_id), pg_escape_string($mode));
    $result  = insert_into_table($con, $sql, $content, $fname);
    if ($result == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        showErrorPage();
        exit;
    }
}

function set_number_of_charcter($con, $fname, $eis_id, $group_code,$number_of_characters) {
    $sql = "insert into inci_item_number_of_character (eis_id, group_code, number_of_characters) values(";
    $content = array(pg_escape_string($eis_id), pg_escape_string($group_code), pg_escape_string($number_of_characters));
    $result = insert_into_table($con, $sql, $content, $fname);
    if ($result == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        showErrorPage();
        exit;
    }
}

function get_number_of_charcter($con, $fname, $eis_id) {
    $sql  = "select * from inci_item_number_of_character ";
    $cond = "where eis_id = " . pg_escape_string($eis_id);
    $result = select_from_table( $con, $sql, $cond, $fname );
    if ($result == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        showErrorPage();
        exit;
    }

    return pg_fetch_all($result);
}

function set_group_order($con, $fname, $eis_id, $use_grp_code){
    $order_no = 1;
    foreach ($use_grp_code as $grp_code) {
        if ($grp_code >= 10000){
            $sql = "insert into inci_easyinput_set_group_order (eis_id, grp_code, order_no) values (";
            $content = array($eis_id, $grp_code, $order_no);
            $result = insert_into_table($con, $sql, $content, $fname);
            if ($result == 0) {
                pg_exec($con, "rollback");
                pg_close($con);
                showErrorPage();
                exit;
            }
            $order_no++;
        }
    }
}

function get_group_order($con, $fname, $eis_id){
    $sql  = "select grp_code from inci_easyinput_set_group_order ";
    $cond = "where eis_id = " . pg_escape_string($eis_id);
    $cond .= " order by order_no";
    $result = select_from_table( $con, $sql, $cond, $fname );
    if ($result == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        showErrorPage();
        exit;
    }

    $grp_code = array();
    $grp_code_list = pg_fetch_all($result);
    foreach($grp_code_list as $row){
        $grp_code[] = $row['grp_code'];
    }
    return $grp_code;

}

function sort_user_cate_group(&$cate_list, $user_grp_code_list){
    if ($user_grp_code_list == null || count($user_grp_code_list) == 0){
        return;
    }

    $cat_10 = null;
    $cate_list_count = count($cate_list);
    for ($idx = 0; $idx < $cate_list_count; $idx++){
        if ($cate_list[$idx]['cate_code'] == 10){
            $cat_10 = &$cate_list[$idx];
            break;
        }
    }
    if ($cat_10 == null){
        return;
    }
    
    $cat_10_grp = array();
    foreach($cat_10['grp_list'] as $grp){
        $cat_10_grp['_'.$grp['grp_code']] = $grp;
    }
    $cat_10['grp_list'] = array();
    foreach($user_grp_code_list as $grp_code){
        $cat_10['grp_list'][] = $cat_10_grp['_'.$grp_code];
        unset($cat_10_grp['_'.$grp_code]);
    }
    foreach($cat_10_grp as $grp){
        $cat_10['grp_list'][] = $grp;
    }
    unset($cat_10);
}

function starts_with($str, $prefix){
    return (substr($str,  0, strlen($prefix)) === $prefix);
}
