<?php

switch( $_REQUEST["mode"] ){
  case "search":			//検索画面
	require_once("./jinji/jinji_search.php");
      break;
  case "refer":				//参照画面
	require_once("./jinji/jinji_refer.php");
      break;
  case "edit":				//更新画面
	require_once("./jinji/jinji_edit.php");
      break;
  case "edit_update":		//更新画面更新機能
	require_once("./jinji/jinji_edit_update.php");
      break;
  case "ctrl_item":			//管理　項目画面
	require_once("./jinji/jinji_control_item.php");
      break;
  case "ctrl_item_update":	//管理　項目画面更新機能
	require_once("./jinji/jinji_control_item_update.php");
      break;
  case "ctrl_user":			//管理　利用者一覧
	require_once("./jinji/jinji_control_user.php");
      break;
  case "ctrl_auth":			//管理　権限
	require_once("./jinji/jinji_control_auth.php");
      break;
  case "ctrl_auth_update":	//管理　権限
	require_once("./jinji/jinji_control_auth_update.php");
      break;
  case "ctrl_csv":			//管理　CSVインポート
	require_once("./jinji/jinji_control_csv.php");
      break;
  case "ctrl_csv_update":	//管理　CSVインポート更新
	require_once("./jinji/jinji_control_csv_update.php");
      break;
  case "print_1":	//印刷
	require_once("./jinji/jinji_print_1.php");
      break;
  case "print_list":	//印刷
	require_once("./jinji/jinji_print_list.php");
      break;
  default:					//デフォルトは検索画面
	require_once("./jinji/jinji_search.php");
}


?>