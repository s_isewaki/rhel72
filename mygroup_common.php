<?
// マイグループ名リストと職員名リスト取得
// array($mygroups, $employees)を返す
function get_mygroups_employees($con, $fname, $emp_id, $login_emp_id = null) {

	$employees = array();
// マイグループを取得
  $sql = "select mygroup_id, mygroup_nm from mygroupmst";
  $cond = "where owner_id = '$emp_id' order by mygroup_id";
  $sel = select_from_table($con, $sql, $cond, $fname);
  if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
  }
  while ($row = pg_fetch_array($sel)) {
    $mygroups[$row["mygroup_id"]] = $row["mygroup_nm"];
	$max_group_id = $row["mygroup_id"];
    $sql_emp  = "select m.mygroup_id, get_emp_name(m.owner_id), get_mail_login_id(m.owner_id), get_emp_email(m.owner_id), m.owner_id, get_emp_name(m.member_id), get_mail_login_id(m.member_id), get_emp_email(m.member_id), m.member_id from mygroup m where exists (select emp_id from authmst a where a.emp_id = m.member_id and a.emp_del_flg = 'f') and m.mygroup_id = $row[mygroup_id] and m.owner_id = '$emp_id' order by order_no";
    $sel_emp = select_from_table($con, $sql_emp, '', $fname);
    if ($sel == 0) {
      pg_close($con);
      echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
      echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
      exit;
    }

    $i = 0;
    while ($row_emp = pg_fetch_array($sel_emp)) {
      if($i==0){
	$employees[$row['mygroup_id']][] = array('name' => $row_emp[1], 'email'=> $row_emp[2], 'emp_id' => $row_emp[4]);
      }
      $employees[$row['mygroup_id']][] = array('name' => $row_emp[5], 'email'=> $row_emp[6], 'emp_id' => $row_emp[8]);
      $i++;
    }
  }
	$max_group_id++;
	$mygroups[$max_group_id] = "よく使う人";

    $sql_emp  = "select c.used_emp_id as emp_id, get_emp_name(c.used_emp_id) as name from wm_counter c";
	$cond = "where c.emp_id = '$emp_id' and c.used_emp_id not in (select emp_id from authmst where emp_del_flg) order by c.count_num desc limit 20";
    $sel_emp = select_from_table($con, $sql_emp, $cond, $fname);
    if ($sel == 0) {
      pg_close($con);
      echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
      echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
      exit;
    }

    $i = 0;
    while ($row_emp = pg_fetch_array($sel_emp)) {
      $employees[$max_group_id][] = array('name' => $row_emp[1], 'emp_id' => $row_emp[0]);
      $i++;
    }

	// ログインユーザの職員IDが渡ってきた場合、共通グループも追加する
	if (!is_null($login_emp_id)) {
		$sql = "select m.group_id, m.group_nm from comgroupmst m";
		$cond = "where m.public_flg or exists (select * from comgroup g where g.group_id = m.group_id and g.emp_id = '$login_emp_id') order by m.group_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$comgroups = array();
		while ($row = pg_fetch_array($sel)) {
			$mygroups[$row["group_id"]] = $row["group_nm"];
			$comgroups[] = $row["group_id"];
		}

		if (count($comgroups) > 0) {
			$sql = "select g.group_id, g.emp_id, e.emp_lt_nm, e.emp_ft_nm from comgroup g inner join empmst e on e.emp_id = g.emp_id";
			$cond = "where g.group_id in (" . join(",", $comgroups) . ") and exists (select * from authmst a where a.emp_id = g.emp_id and a.emp_del_flg = 'f') order by g.order_no";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			while ($row = pg_fetch_array($sel)) {
				$employees[$row["group_id"]][] = array(
					"name" => $row["emp_lt_nm"] . " " . $row["emp_ft_nm"],
					"emp_id" => $row["emp_id"]
				);
			}
		}
	}

	return array($mygroups, $employees);
}
?>
