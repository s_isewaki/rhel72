<!-- ************************************************************************ -->
<!-- 勤務シフト作成｜スタッフ設定「登録」 -->
<!-- ************************************************************************ -->

<?
require_once("about_session.php");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);

///-----------------------------------------------------------------------------
// 職員データ取得
///-----------------------------------------------------------------------------
$i = 0;
$emp_array = array();
$emp_id_array = split(",", $emp_id_array);
foreach ($emp_id_array as $emp_id) {
	// テーブルのDrap&Drop並べ替え対応
//	$emp_array[$i]["no"] = $no[$i];
//	$emp_array[$i]["team_id"] = $team_id[$i];
//	$emp_array[$i]["emp_id"] = $emp_id;
	$num=$i+1;
	$emp_array[$i]["no"] = $num; 
	$emp_array[$i]["team_id"] = $_POST["team_id"][$i];
	$emp_array[$i]["emp_id"]  = $_POST["new_emp_id"][$i];
	$i++;
}


///-----------------------------------------------------------------------------
// 職員データを表示順で昇順にソート
///-----------------------------------------------------------------------------
//列方向の配列を得る --- 2012.3.3 テーブルのDrap&Drop並べ替え対応によりソート廃止
 for ($i=0; $i<count($emp_array); $i++) {
	$wk_key1[$i] = $emp_array[$i]["no"];
	$wk_key2[$i] = $i + 1;
}
// キーでソートする。
array_multisort($wk_key1, SORT_ASC, $wk_key2, SORT_ASC, $emp_array);

///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");
///-----------------------------------------------------------------------------
// リアルタイム同期先の組織を取得
///-----------------------------------------------------------------------------
$sync_link = get_sync_link($con, $group_id, count($emp_array), $fname);
//削除前に所属していた職員IDを取得
$arr_old_emp_id = array();
$sql = "select emp_id from duty_shift_staff";
$cond = "where group_id = '$group_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {
    $arr_old_emp_id[] = $row["emp_id"];
}
//今回登録分と更新前職員を比較し、新規分の移動元グループの確認後削除と履歴登録
foreach ($emp_id_array as $emp_id) {
    if (!in_array($emp_id, $arr_old_emp_id[$i])) {
        $sql = "select emp_id, group_id from duty_shift_staff";
        $cond = "where group_id != '$group_id' and emp_id = '$emp_id' ";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        if (pg_num_rows($sel) > 0) {
            $old_group_id = pg_fetch_result($sel, 0, "group_id");
            $sql = "delete from duty_shift_staff";
            $cond = "where group_id = '$old_group_id' and emp_id = '$emp_id'";
            $del = delete_from_table($con, $sql, $cond, $fname);
            if ($del == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $sql = "insert into duty_shift_staff_history (emp_id, group_id, histdate) values(";
            $content = array($emp_id, $old_group_id, date('Y-m-d H:i:s'));
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_query($con,"rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            
        }
        
    }
}
///-----------------------------------------------------------------------------
// 指定シフトグループ（病棟）のスタッフ情報を全削除
///-----------------------------------------------------------------------------
$sql = "delete from duty_shift_staff";
$cond = "where group_id = '$group_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//職員データ分を登録
///-----------------------------------------------------------------------------
for ($i=0; $i<count($emp_array); $i++) {
	///-----------------------------------------------------------------------------
	//登録情報取得
	///-----------------------------------------------------------------------------
	$wk_no = $emp_array[$i]["no"];
	$wk_team_id = $emp_array[$i]["team_id"] ? $emp_array[$i]["team_id"] : null;
	$wk_emp_id = $emp_array[$i]["emp_id"];
	///-----------------------------------------------------------------------------
	//職員データ分を登録
	///-----------------------------------------------------------------------------
	if ($wk_emp_id != "") {
		///-----------------------------------------------------------------------------
		// スタッフ情報を登録
		///-----------------------------------------------------------------------------
		$sql = "insert into duty_shift_staff (group_id, emp_id, no, team_id) values (";
		$content = array($group_id, $wk_emp_id, $wk_no, $wk_team_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$no++;
		///-----------------------------------------------------------------------------
		//出勤パターンを変更（ＵＰＤＡＴＥ）
		///-----------------------------------------------------------------------------
		$sql = "update empcond set";
		$set = array("tmcd_group_id");
		$setvalue = array($pattern_id);
		$cond = "where emp_id = '$wk_emp_id' ";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		///-----------------------------------------------------------------------------
		// リアルタイム同期する場合は同期
		///-----------------------------------------------------------------------------
		if ($sync_link !== false) {
			$sql = "update empmst set";
			$set = array("emp_class", "emp_attribute", "emp_dept", "emp_room");
			$setvalue = array($sync_link["class_id"], $sync_link["atrb_id"], $sync_link["dept_id"], $sync_link["room_id"]);
			$cond = "where emp_id = '$wk_emp_id' ";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
		}
	}
}
///-----------------------------------------------------------------------------
// 職員スキル
///-----------------------------------------------------------------------------
// 該当職員のスキル情報削除
for ($i=0; $i<count($emp_array); $i++) {
	$wk_emp_id = $_POST["new_emp_id"][$i];
	$sklevel = $_POST["skill_level"][$i];
	$sql = "DELETE FROM emp_skill ";
	$cond = "WHERE emp_id = '$wk_emp_id'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

for ($i=0; $i<count($emp_array); $i++) {
	$wk_emp_id = $_POST["new_emp_id"][$i];
	$sklevel = $_POST["skill_level"][$i];
	if($sklevel == "" ){ // もしものため
		$sklevel = 0;
	}

	if($sklevel != 0 ){ // スキル登録しない職員はINSERTしない
		// 該当職員のスキル情報作成
		$sql = "INSERT INTO emp_skill (emp_id , emp_skill_level) VALUES (";
//		$content = array($wk_emp_id , $sklevel."\n");
		$content = array($wk_emp_id , $sklevel);

		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}
//更新前職員と今回登録分を比較し不足分（削除）を履歴登録
$histdate = date('Y-m-d H:i:s');
for ($i=0; $i<count($arr_old_emp_id); $i++) {
    if (!in_array($arr_old_emp_id[$i], $emp_id_array)) {
        $sql = "insert into duty_shift_staff_history (emp_id, group_id, histdate) values(";
        $content = array($arr_old_emp_id[$i], $group_id, $histdate);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con,"rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        
    }
}

///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query($con, "commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);
///-----------------------------------------------------------------------------
// 遷移元画面を再表示
///-----------------------------------------------------------------------------

$back_show = "<script type=\"text/javascript\">location.href = 'duty_shift_staff.php?session=$session&group_id=$group_id'</script>";
echo($back_show);
?>

<!-- ************************************************************************ -->
<!-- デバック用 -->
<!-- ************************************************************************ -->
<HTML>
<!--
<? echo("group_id=$group_id \n"); ?>
<? echo("emp_id_array=$emp_id_array"); ?>
-->
</HTML>
<?
function get_sync_link($con, $group_id, $emp_count, $fname) {
	if ($emp_count == 0) {
		return false;
	}

	$sql = "select sync_flag from duty_shift_option";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$sync_flag = pg_fetch_result($sel, 0, "sync_flag");
	if ($sync_flag != "t") {
		return false;
	}

	$sql = "select class_id, atrb_id, dept_id, room_id from duty_shift_sync_link";
	$cond = "where group_id = '$group_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) == 0) {
		return false;
	}

	$sync_link = array(
		"class_id" => pg_fetch_result($sel, 0, "class_id"),
		"atrb_id"  => pg_fetch_result($sel, 0, "atrb_id"),
		"dept_id"  => pg_fetch_result($sel, 0, "dept_id"),
		"room_id"  => pg_fetch_result($sel, 0, "room_id")
	);
	if ($sync_link["room_id"] == "") $sync_link["room_id"] = null;
	return $sync_link;
}
