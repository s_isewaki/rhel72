<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("label_by_profile_type.ini");
require_once("get_cas_title_name.ini");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// グループ一覧を配列に格納

$sql = "select group_id, group_nm from dispgroup";
$cond = "order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$groups = array();
while ($row = pg_fetch_array($sel)) {
	$groups[$row["group_id"]] = $row["group_nm"];
}
if ($group_id == "") {
	$group_id = key($groups);
}


// 職種一覧を取得
$sql = "select job_id, job_nm from jobmst";
$cond = "where job_del_flg = 'f' order by order_no";
$sel_job = select_from_table($con, $sql, $cond, $fname);
if ($sel_job == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 役職一覧を取得
$sql = "select st_id, st_nm from stmst";
$cond = "where st_del_flg = 'f' order by order_no";
$sel_st = select_from_table($con, $sql, $cond, $fname);
if ($sel_st == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 業務機能の使用可否を取得
$sql = "select * from license";
$cond = "";
$sel_func = select_from_table($con, $sql, $cond, $fname);
if ($sel_func == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$lcs_func = array();
for ($i = 1; $i <= 60; $i++) {
	$lcs_func[$i] = pg_fetch_result($sel_func, 0, "lcs_func$i");
}

// 表示グループを表示する場合
if ($group_id != "") {

	// 表示対象の表示グループの設定値を取得
	$sql = "select * from dispgroup";
	$cond = "where group_id = $group_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$dispinfo = pg_fetch_array($sel);
}

$whatsnew_flg = ($dispinfo["top_msg_flg"] == "t" && $dispinfo["top_aprv_flg"] == "t") ? "t" : "f";

// system_config
$conf = new Cmx_SystemConfig();

// 表示グループの初期設定を取得する
$default_group_id = $conf->get("default_set.disp_group_id");

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | 表示グループ</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function registerGroup() {
	window.open('employee_display_group_register.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function deleteGroup() {
	var selected = false;
	if (document.delform.elements['group_ids[]']) {
		if (document.delform.elements['group_ids[]'].length) {
			for (var i = 0, j = document.delform.elements['group_ids[]'].length; i < j; i++) {
				if (document.delform.elements['group_ids[]'][i].checked) {
					selected = true;
					break;
				}
			}
		} else if (document.delform.elements['group_ids[]'].checked) {
			selected = true;
		}
	}

	if (!selected) {
		alert('削除対象が選択されていません。');
		return;
	}

	if (confirm('削除します。よろしいですか？')) {
		document.delform.submit();
	}
}

function default_set() {
		document.defaultform.submit();
}
function reverseAllOptions(box) {
	for (var i = 0, j = box.length; i < j; i++) {
		box.options[i].selected = !box.options[i].selected;
	}
}
function default_set() {
		document.defaultform.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="employee_db_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録用DB操作</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_auth_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#5279a5"><a href="employee_display_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>表示グループ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_bulk_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_field_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目管理</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<? if ($result == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループを設定しました。</font></td>
<? } ?>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td width="15%">
<form name="delform" action="employee_display_group_delete.php">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
表示設定グループ<br>
<input type="button" value="作成" onclick="registerGroup();">
<input type="button" value="削除" onclick="deleteGroup();">
</font></td>
</tr>
<tr height="100" valign="top">
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<?
foreach ($groups as $tmp_group_id => $tmp_group_nm) {
	echo("<tr height=\"22\" valign=\"middle\">\n");
	echo("<td width=\"1\"><input type=\"checkbox\" name=\"group_ids[]\" value=\"$tmp_group_id\"></td>\n");
	if ($tmp_group_id == $group_id) {
		echo("<td style=\"padding-left:3px;background-color:#ffff66;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_group_nm</font></td>\n");
	} else {
		echo("<td style=\"padding-left:3px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"employee_display_group.php?session=$session&group_id=$tmp_group_id\">$tmp_group_nm</a></font></td>\n");
	}
	echo("</tr>\n");
}
?>
</table>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="default_group_id" value="<? echo($default_group_id); ?>">
</form>


<img src="img/spacer.gif" width="1" height="8" alt=""><br>
<form name="defaultform" action="employee_display_group_default.php">
	<table border="0" cellspacing="0" cellpadding="2" class="list">
		<tr height="22">
			<td width="15%">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">初期表示グループ<input type="button" value="保存" onclick="default_set();"><br>
				<select name="group_id">
					<option value="">　　　　　</option>
<?
foreach ($groups as $tmp_group_id => $tmp_group_nm) {
	echo("<option value=\"$tmp_group_id\"");
	if ($tmp_group_id == $default_group_id) {
		echo(" selected");
	}
	echo(">$tmp_group_nm\n");
}
						?>
				</select>
				</font>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red"><br>※保存された表示設定は<br>「職員登録＞新規登録」画面で初期表示されます</font>
			</td>
		</tr>
	</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>


</td>
<td><img src="img/spacer.gif" alt="" width="5" height="1"></td>
<td>




<form name="employee" action="employee_display_group_update_exe.php" method="post">
<? if ($group_id != "") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ名</font></td>
<td colspan="3"><input type="text" name="group_nm" value="<? echo($groups[$group_id]); ?>" size="25" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログイン時の表示ページ</font></td>
<td colspan="3"><? show_page_list($con, $dispinfo["default_page"], $lcs_func, $fname); ?></td>
</tr>

<input type="hidden" name="logo_type" value="1" /><!-- ロゴタイプ:GIF -->

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイページの文字サイズ</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="font_size" value="1"<? if ($dispinfo["font_size"] == "1") {echo(" checked");} ?>>標準
<input type="radio" name="font_size" value="2"<? if ($dispinfo["font_size"] == "2") {echo(" checked");} ?>>大
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイページに表示</font></td>
<td colspan="3">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="mail_flg" value="t"<? if ($dispinfo["top_mail_flg"] == "t") {echo(" checked");} ?>>ウェブメール</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="ext_flg" value="t"<? if ($dispinfo["top_ext_flg"] == "t") {echo(" checked");} ?>>一発電話検索</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="inci_flg" value="t"<? if ($dispinfo["top_inci_flg"] == "t") {echo(" checked");} ?><? if ($lcs_func["5"] == "f") {echo(" disabled");} ?>>ファントルくん</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="fplus_flg" value="t"<? if ($dispinfo["top_fplus_flg"] == "t") {echo(" checked");} ?><? if ($lcs_func["13"] == "f") {echo(" disabled");} ?>>ファントルくん＋</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="jnl_flg" value="t"<? if ($dispinfo["top_jnl_flg"] == "t") {echo(" checked");} ?><? if ($lcs_func["14"] == "f") {echo(" disabled");} ?>>日報・月報</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="manabu_flg" value="t"<? if ($dispinfo["top_manabu_flg"] == "t") {echo(" checked");} ?><? if ($lcs_func["12"] == "f") {echo(" disabled");} ?>>バリテス</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="cas_flg" value="t"<? if ($dispinfo["top_cas_flg"] == "t") {echo(" checked");} ?><? if ($lcs_func["8"] == "f") {echo(" disabled");} ?>><? echo(get_cas_title_name()); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="event_flg" value="t"<? if ($dispinfo["top_event_flg"] == "t") {echo(" checked");} ?>><? echo($_label_by_profile["EVENT"][$profile_type]); ?></font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="schd_flg" value="t"<? if ($dispinfo["top_schd_flg"] == "t") {echo(" checked");} ?>>スケジュール</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="info_flg" value="t"<? if ($dispinfo["top_info_flg"] == "t") {echo(" checked");} ?>>お知らせ・回覧板</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="task_flg" value="t"<? if ($dispinfo["top_task_flg"] == "t") {echo(" checked");} ?>>タスク</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="whatsnew_flg" value="t"<? if ($whatsnew_flg == "t") {echo(" checked");} ?>>最新情報</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="schdsrch_flg" value="t"<? if ($dispinfo["top_schdsrch_flg"] == "t") {echo(" checked");} ?>>スケジュール検索</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="fcl_flg" value="t"<? if ($dispinfo["top_fcl_flg"] == "t") {echo(" checked");} ?>>設備予約</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="lib_flg" value="t"<? if ($dispinfo["top_lib_flg"] == "t") {echo(" checked");} ?>>文書管理</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="bbs_flg" value="t"<? if ($dispinfo["top_bbs_flg"] == "t") {echo(" checked");} ?>>掲示板</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="memo_flg" value="t"<? if ($dispinfo["top_memo_flg"] == "t") {echo(" checked");} ?>>備忘録</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="link_flg" value="t"<? if ($dispinfo["top_link_flg"] == "t") {echo(" checked");} ?>>リンクライブラリ</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="intra_flg" value="t"<? if ($dispinfo["top_intra_flg"] == "t") {echo(" checked");} ?>>イントラネット</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="bedinfo" value="t"<? if ($dispinfo["bed_info"] == "t") {echo(" checked");} ?><? if ($lcs_func["2"] == "f") {echo(" disabled");} ?>><?
// 入退院カレンダー/入退室カレンダー
echo $_label_by_profile["BED_INFO"][$profile_type];
?>
</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="wic_flg" value="t"<? if ($dispinfo["top_wic_flg"] == "t") {echo(" checked");} ?>>WICレポート</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="top_free_text_flg" value="t"<? if ($dispinfo["top_free_text_flg"] == "t") {echo(" checked");} ?>>フリーテキスト</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="ladder_flg" value="t"<? if ($dispinfo["ladder_flg"] == "t") {echo(" checked");} ?><? if ($lcs_func["17"] == "f") {echo(" disabled");} ?>>クリニカルラダー</font></td>
<!-- 打刻エラー件数表示 START -->
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="top_timecard_error_flg" value="t"<? if ($dispinfo["top_timecard_error_flg"] == "t") {echo(" checked");} ?>>打刻エラー件数</font></td>
<!-- 打刻エラー件数表示 START -->

<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="top_workflow_flg" value="t"<? if ($dispinfo["top_workflow_flg"] === "t") {echo(" checked");} ?>>決裁・申請</font></td>

<? if ($lcs_func[31]=="t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="top_ccusr1_flg" value="t"<? if ($dispinfo["top_ccusr1_flg"] === "t") {echo(" checked");} ?>>スタッフ・ポートフォリオ</font></td>
<? } else { ?>
<td></td>
<? } ?>
<td></td>
<td></td>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スケジュールに出勤予定を表示</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="schd_type" value="t"<? if ($dispinfo["schd_type"] == "t") {echo(" checked");} ?>>する
<input type="radio" name="schd_type" value="f"<? if ($dispinfo["schd_type"] == "f") {echo(" checked");} ?>>しない
</font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
</form>
</td>
</tr>
</table>
<? } ?>
</body>
<? pg_close($con); ?>
</html>
<?
function show_options($sel, $id_col, $nm_col, $tgt_ids) {
	while ($row = pg_fetch_array($sel)) {
		$id = $row[$id_col];
		$nm = $row[$nm_col];
		echo("<option value=\"$id\"");
		if (in_array($id, $tgt_ids)) {
			echo(" selected");
		}
		echo(">$nm\n");
	}
}

function show_page_list($con, $selected, $lcs_func, $fname) {
	global $_label_by_profile;
	global $profile_type;

	require_once("get_menu_label.ini");
	$report_menu_label = get_report_menu_label($con, $fname);
	$shift_menu_label = get_shift_menu_label($con, $fname);

	echo("<select name=\"default_page\">");

	echo("<option value=\"01\"");
	if ($selected == "01") {echo(" selected");}
	echo(">マイページ");

	echo("<option value=\"02\"");
	if ($selected == "02") {echo(" selected");}
	echo(">ウェブメール");

	echo("<option value=\"03\"");
	if ($selected == "03") {echo(" selected");}
	echo(">スケジュール");

	echo("<option value=\"04\"");
	if ($selected == "04") {echo(" selected");}
	echo(">委員会・WG");

	echo("<option value=\"05\"");
	if ($selected == "05") {echo(" selected");}
	echo(">タスク");

	echo("<option value=\"06\"");
	if ($selected == "06") {echo(" selected");}
	echo(">伝言メモ");

	echo("<option value=\"07\"");
	if ($selected == "07") {echo(" selected");}
	echo(">設備予約");

	echo("<option value=\"08\"");
	if ($selected == "08") {echo(" selected");}
	echo(">出勤表");

	echo("<option value=\"09\"");
	if ($selected == "09") {echo(" selected");}
	echo(">決裁・申請");

	echo("<option value=\"10\"");
	if ($selected == "10") {echo(" selected");}
	echo(">ネットカンファレンス");

	echo("<option value=\"11\"");
	if ($selected == "11") {echo(" selected");}
	echo(">掲示板");

	echo("<option value=\"12\"");
	if ($selected == "12") {echo(" selected");}
	echo(">Q&amp;A");

	echo("<option value=\"13\"");
	if ($selected == "13") {echo(" selected");}
	echo(">文書管理");

	echo("<option value=\"22\"");
	if ($selected == "22") {echo(" selected");}
	echo(">お知らせ・回覧板");

	echo("<option value=\"14\"");
	if ($selected == "14") {echo(" selected");}
	echo(">アドレス帳");

	echo("<option value=\"23\"");
	if ($selected == "23") {echo(" selected");}
	echo(">内線電話帳");

	echo("<option value=\"26\"");
	if ($selected == "26") {echo(" selected");}
	echo(">リンクライブラリ");

	echo("<option value=\"15\"");
	if ($selected == "15") {echo(" selected");}
	echo(">パスワード・本人情報");

	echo("<option value=\"16\"");
	if ($selected == "16") {echo(" selected");}
	echo(">オプション設定");

//XX	echo("<option value=\"17\"");
//XX	if ($selected == "17") {echo(" selected");}
//XX	echo(">コミュニティサイト");

	if ($lcs_func["4"] == "t") {
		echo("<option value=\"21\"");
		if ($selected == "21") {echo(" selected");}
		echo(">イントラネット");
	}

	if ($lcs_func["7"] == "t") {
		echo("<option value=\"27\"");
		if ($selected == "27") {echo(" selected");}
		echo(">検索ちゃん");
	}

	if ($lcs_func["1"] == "t") {
		echo("<option value=\"18\"");
		if ($selected == "18") {echo(" selected");}
		// 患者管理/利用者管理
		echo(">".$_label_by_profile["PATIENT_MANAGE"][$profile_type]);
	}

	if ($lcs_func["15"] == "t") {
		echo("<option value=\"35\"");
		if ($selected == "35") {echo(" selected");}
		echo(">看護支援");
	}

	if ($lcs_func["2"] == "t") {
		echo("<option value=\"19\"");
		if ($selected == "19") {echo(" selected");}
		// 病床管理/居室管理
		echo(">".$_label_by_profile["BED_MANAGE"][$profile_type]);
	}

	if ($lcs_func["10"] == "t") {
		echo("<option value=\"30\"");
		if ($selected == "30") {echo(" selected");}
		echo(">看護観察記録");
	}

	if ($lcs_func["5"] == "t") {
		echo("<option value=\"24\"");
		if ($selected == "24") {echo(" selected");}
		echo(">ファントルくん");
	}

	if ($lcs_func["13"] == "t") {
		echo("<option value=\"33\"");
		if ($selected == "33") {echo(" selected");}
		echo(">ファントルくん＋");
	}

	if ($lcs_func["6"] == "t") {
		echo("<option value=\"25\"");
		if ($selected == "25") {echo(" selected");}
		// メドレポート/ＰＯレポート(Problem Oriented)
		echo(">$report_menu_label");
	}

	if ($lcs_func["12"] == "t") {
		echo("<option value=\"31\"");
		if ($selected == "31") {echo(" selected");}
		echo(">バリテス");
	}

	if ($lcs_func["9"] == "t") {
		echo("<option value=\"29\"");
		if ($selected == "29") {echo(" selected");}
		// 勤務シフト作成
		echo(">$shift_menu_label");
	}

	if ($lcs_func["11"] == "t") {
		echo("<option value=\"32\"");
		if ($selected == "32") {echo(" selected");}
		echo(">カルテビューワー");
	}

	if ($lcs_func["8"] == "t") {
		echo("<option value=\"28\"");
		if ($selected == "28") {echo(" selected");}
		require_once("get_cas_title_name.ini");
		echo(">" . get_cas_title_name());
	}

	if ($lcs_func["16"] == "t") {
		echo("<option value=\"36\"");
		if ($selected == "36") {echo(" selected");}
		echo(">人事管理");
	}

	if ($lcs_func["17"] == "t") {
		echo("<option value=\"37\"");
		if ($selected == "37") {echo(" selected");}
		echo(">クリニカルラダー");
	}

	if ($lcs_func["19"] == "t") {
		echo("<option value=\"39\"");
		if ($selected == "39") {echo(" selected");}
		echo(">キャリア開発ラダー");
	}

	if ($lcs_func["18"] == "t") {
		echo("<option value=\"38\"");
		if ($selected == "38") {echo(" selected");}
		echo(">勤務表");
	}

	if ($lcs_func["14"] == "t") {
		echo("<option value=\"34\"");
		if ($selected == "34") {echo(" selected");}
		echo(">日報・月報");
	}

	if ($lcs_func["3"] == "t") {
		echo("<option value=\"20\"");
		if ($selected == "20") {echo(" selected");}
		echo(">経営支援");
	}

	if ($lcs_func["31"] == "t") {
		echo("<option value=\"41\"");
		if ($selected == "41") {echo(" selected");}
		echo(">スタッフ・ポートフォリオ");
	}

	echo("</select>");
}
