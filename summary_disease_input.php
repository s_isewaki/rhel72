<?/*

画面パラメータ
	$session
		セッションID
	$pt_id
		患者ID
  $disease
    診断名(主病名)
  $in_dt
    入院年月日     例："20040712"
  $in_tm
    入院時刻       例："1400"
  $patho_from
    発症年月日from 例："20040710"
  $patho_to
    発症年月日to   例："20040801"
  $id_sect
    診療科ID       int
  $dr_id
    担当医ID       int
  $tbl_nm
    テーブル名("inpthist" or "inptmst")

*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("about_postgres.php"); ?>
<? require("summary_common.ini"); ?>
<? require("show_disease_input_form.ini"); ?>
<?

//ページ名
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname); // 57:メドレポート
if ($summary == "0")
{
	showLoginPage();
	exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==========================================================================================
//HTML出力
//==========================================================================================
//==============================
//スクリプト/スタイルシート/BODY宣言 出力
//==============================
?>
<title>診断名(主病名)更新</title>
<script type="text/javascript">

function load_action()
{
	//フォームに対するロード時の処理を行います。
	onload_show_disease_input_form();
}

</script>
</head>
<body onload="load_action();">
<div style="padding:4px">

<? //ヘッダー 出力 ?>
<? summary_common_show_dialog_header("診断名(主病名)更新"); ?>

<? //入力フィールド 出力 ?>

<div style="margin-top:8px">
<?
$pt_id_      = $_REQUEST["pt_id"     ] ;
$in_dt_      = $_REQUEST["in_dt"     ] ;
$in_tm_      = $_REQUEST["in_tm"     ] ;
$tbl_nm_     = $_REQUEST["tbl_nm"    ] ;
$disease_    = $_REQUEST["disease"   ] ;
$patho_from_ = $_REQUEST["patho_from"] ;
$patho_to_   = $_REQUEST["patho_to"  ] ;
$sect_id_    = $_REQUEST["id_sect"   ] ;
$dr_id_      = $_REQUEST["dr_id"     ] ;
?>
<? show_disease_input_form($con, $fname, $session, $pt_id_, $disease_, $in_dt_, $in_tm_, $patho_from_, $patho_to_, $sect_id_, $dr_id_, $tbl_nm_, "〜"); ?>
</div>


</div>
</body>
</html>
<?

pg_close($con);
?>
