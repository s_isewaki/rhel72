<?
ob_start();
$fname = $_SERVER["PHP_SELF"];
require_once("about_comedix.php");
require("get_values.php");
require("show_sinryo_top_header.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require("summary_common.ini");
require_once("tmpl_common.ini"); 
ob_end_clean();

$session = qualify_session($session,$fname);
$session_auth = check_authority($session, 59, $fname);
$con = @connect2db($fname);
if(!$session || !$session_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}



$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

if (@$_REQUEST["encoded_sel_med_name"]) {
	//$_REQUEST["sel_med_name"] = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["encoded_sel_med_name"])), "EUC-JP", "UTF-8");
	$_REQUEST["sel_med_name"] = mb_convert_encoding(rawurldecode(rawurldecode(@$_REQUEST["encoded_sel_med_name"])), "EUC-JP", "UTF-8");
} else {
	$_REQUEST["sel_med_name"] = mb_convert_encoding(rawurldecode(rawurldecode(@$_REQUEST["sel_med_name"])), "EUC-JP", "UTF-8");
}


// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];
// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 薬品選択 | 処方・注射オーダ</title>
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<style type="text/css">
.list_table th { white-space:nowrap }
.list_table td { white-space:nowrap }
</style>
<script type="text/javascript">
	function getClientHeight(){
		if (document.body.clientHeight) return document.body.clientHeight;
		if (window.innerHeight) return window.innerHeight;
		if (document.documentElement && document.documentElement.clientHeight) {
			return document.documentElement.clientHeight;
		}
		return 0;
	}

	function trClicked(hot13_cd){
		if (!window.opener) return;
		if (window.opener.closed) return;
<? if (!@$_REQUEST["js_callback"]){?>
		alert("値をセットできません。");
		return;
<? } ?>
		if (!window.opener.<?= @$_REQUEST["js_callback"] ?>){
			return;
		}
		window.opener.<?= @$_REQUEST["js_callback"] ?>(hot13_cd);
		window.opener.focus();
	}

	function adjustListSize(){
		if (!document.getElementById("scroll_div")) return;
		var h = getClientHeight();
		if (h < 1) return;
		document.getElementById("scroll_div").style.height = (h - 150) + "px";
	}
</script>

</head>
<body onload="adjustListSize()" onresize="//adjustListSize()">


<!-- ヘッダエリア -->
<?= summary_common_show_dialog_header("薬品選択"); ?>



<?//==========================================================================?>
<?// 検索条件                                                                 ?>
<?//==========================================================================?>
<form name="search" action="sum_ordsc_med_selector.php" method="get">
<input type="hidden" name="searching" value="1">
<input type="hidden" name="js_callback" value="<?=$_REQUEST["js_callback"] ?>">
<input type="hidden" name="session" value="<?= $session ?>">
<input type="submit" value="dummy" style="display:none"/>


<?
	$page = max(1 ,(int)@$_REQUEST["summary_current_page"]);
	if (@$_REQUEST["rows_per_page"] != @$_REQUEST["rows_per_page_old"]) $page = 1;
  $rows_per_page = max(20, (int)@$_REQUEST["rows_per_page"]);


?>
<script type="text/javascript">
  function summary_page_change(page){
    document.getElementById("summary_current_page").value = page;
    document.frm.submit();
    return false;
  }
</script>

<form action="sum_ordsc_med_list.php" method="get" name="frm">
<div class="dialog_searchfield" style="margin:4px">
	<input type="hidden" name="session" value="<?=$session ?>">
	<input type="hidden" name="mode" value="search">
  <input type="hidden" name="summary_current_page" id="summary_current_page" value="<?=$page?>">
	<div style="padding:4px">
		<span style="padding-right:3px">基準番号</span>
		<input type="text" name="sel_hot13_cd" style="width:120px; ime-mode:disabled" value="<?=@$_REQUEST["sel_hot13_cd"]?>" />
		<span style="padding-right:3px; padding-left:10px">薬品名</span>
		<input type="text" name="sel_med_name" style="width:300px" value="<?=h(@$_REQUEST["sel_med_name"])?>"/>
	</div>
	<div style="padding:4px">
		<span style="padding-right:3px">処方用番号</span>
		<input type="text" name="sel_hot7_cd" style="width:80px; ime-mode:disabled" value="<?=@$_REQUEST["sel_hot7_cd"]?>" />
		<span style="padding-right:3px; padding-left:10px">JANコード</span>
		<input type="text" name="sel_jan_cd" style="width:120px; ime-mode:disabled" value="<?=@$_REQUEST["sel_jan_cd"]?>" />
		<span style="padding-right:3px; padding-left:10px">薬価基準収蔵医薬品コード</span>
		<input type="text" name="sel_unitary_cd" style="width:120px; ime-mode:disabled" value="<?=@$_REQUEST["sel_unitary_cd"]?>" />
	</div>
  <div style="padding:4px; text-align:right">
  	<label><input type="checkbox" name="sel_show_update_kubun" value="1" <?=@$_REQUEST["sel_show_update_kubun"]?"checked":""?> />削除済み薬品を表示</label>
  	<label><input type="checkbox" name="sel_used_counter" value="1" <?=@$_REQUEST["sel_used_counter"]?"checked":""?> />採用薬割当済を隠す</label>
		<span style="padding-left:10px">
			<input type="text" name="rows_per_page" value="<?=$rows_per_page?>" style="width:40px" />行ずつ
			<input type="hidden" name="rows_per_page_old" value="<?=$rows_per_page?>" />
			<input type="submit" value="検索"/>
		</span>
  </div>
</div>
</form>






<?//==========================================================================?>
<?// 一覧                                                                     ?>
<?//==========================================================================?>
<?
	$do_search = false;
	if (@$_REQUEST["sel_hot7_cd"])    $do_search = true;
	if (@$_REQUEST["sel_jan_cd"])     $do_search = true;
	if (@$_REQUEST["sel_unitary_cd"]) $do_search = true;
	if (@$_REQUEST["sel_hot13_cd"])   $do_search = true;
	if (@$_REQUEST["sel_med_name"])   $do_search = true;

  $offset = ($page - 1) * $rows_per_page;

	if ($do_search){
		$sql = " select sai.*, mst.* from (";
		$sql .= " select * from sum_medmst where 1=1";
		if (!@$_REQUEST["sel_show_update_kubun"]) $sql .= " and update_kubun <> 4";
		if (@$_REQUEST["sel_hot7_cd"])            $sql .= " and hot7_cd = '".pg_escape_string($_REQUEST["sel_hot7_cd"])."'";
		if (@$_REQUEST["sel_jan_cd"])             $sql .= " and jan_cd = '".pg_escape_string($_REQUEST["sel_jan_cd"])."'";
		if (@$_REQUEST["sel_unitary_cd"])         $sql .= " and unitary_cd = '".pg_escape_string($_REQUEST["sel_unitary_cd"])."'";
		if (@$_REQUEST["sel_hot13_cd"])           $sql .= " and hot13_cd = '".pg_escape_string($_REQUEST["sel_hot13_cd"])."'";
		if (@$_REQUEST["sel_med_name"]) {
			$sql .= " and (";
			$sql .= " kokuji_name like '%".pg_escape_string($_REQUEST["sel_med_name"])."%'";
			$sql .= " or hanbai_name like '%".pg_escape_string($_REQUEST["sel_med_name"])."%'";
			$sql .= " or receipt_name like '%".pg_escape_string($_REQUEST["sel_med_name"])."%'";
			$sql .= " )";
		}
		$sql .= " ) mst";

		$sql .= " left join (";
		$sql .= "   select seq, used_counter, hot13_cd from sum_med_saiyou_mst";
		$sql .= " ) sai on (sai.hot13_cd = mst.hot13_cd)";

		$sql .= " where 1=1";
		if (@$_REQUEST["sel_used_counter"]) $sql .= " and (sai.seq is null or sai.used_counter = 0)";

		// 全レコード数の取得、ページの割り出し
		$sel = select_from_table($con, "select count(*) from (".$sql.") d", "", $fname);
		$total_record = pg_fetch_result($sel, 0, 0);
	  $total_page_count = floor( ($total_record-1) / $rows_per_page ) + 1;

		$sql .= " order by seq, sai.hot13_cd offset ".$offset." limit ".$rows_per_page;
		$sel = select_from_table($con, $sql, "", $fname);
	}
?>

<?//==========================================================================?>
<?// 検索しない場合                                                           ?>
<?//==========================================================================?>
<? if (!$do_search){ ?>
	<div style="margin-top:20px; text-align:center">検索条件を指定してください。</div>
	</body>
	</html>
	<? die; ?>
<? } ?>


<?//==========================================================================?>
<?//ページャ(上)                                                              ?>
<?//==========================================================================?>
<table width="100%"><tr><td>
  <table><tr>
    <td style="white-space:nowrap; padding:4px">全<?=$total_record?>行</td>
		<? if ($total_page_count > 1){ ?>
    <td style="white-space:nowrap; padding:4px">
		<span style="color:silver"><? if($page>1){?><a href="" onclick="return summary_page_change(1);"><? } ?>先頭へ</a></span>
    </td>
    <td style="white-space:nowrap; padding:4px">
		<span style="color:silver"><? if($page>1) { ?><a href="" onclick="return summary_page_change(<?=$page-1?>);"><? } ?>前へ</a></span>
    </td>
    <td>
			<? for($i=1;$i<=$total_page_count;$i++) { ?>
				<? if ($page > $i && $page - $i > 10) continue; ?>
				<? if ($page < $i && $i - $page > 10) continue; ?>
				<? if($i != $page) { ?><a href="" onclick="return summary_page_change(<?=$i?>);">[<?=$i?>]</a><? }else{ ?>[<?=$i?>]<? } ?>
			<? } ?>
    </td>
    <td style="white-space:nowrap; padding:4px">
		<span style="color:silver"><? if($page<$total_page_count){ ?><a href="" onclick="return summary_page_change(<?=$page+1?>);"><? } ?>次へ</a></span>
    </td>
    <? } ?>
  </tr></table>
</td></tr></table>


<? if ($total_record > 0) { ?>
<div id="scroll_div" style="width:99%; overflow:scroll; border:1px solid #aaa; margin:4px">
<table class="list_table">
	<tr style="background-color:#f6f9ff">
		<th>割当</th>

		<th>基準番号（ＨＯＴコード）</th>

		<th>処方用番号(HOT7)</th>
		<th>会社識別用番号</th>
		<th>調剤用番号</th>
		<th>物流用番号</th>
		<th>JANコード</th>
		<th>薬価基準収蔵医薬品コード</th>
		<th>個別医薬品コード(YJコード）</th>
		<th>レセプト電算処理システムコード（１）</th>
		<th>レセプト電算処理システムコード（２）</th>
		<th>告示名称</th>
		<th>販売名</th>
		<th>レセプト電算処理システム医薬品名</th>
		<th>規格単位</th>
		<th>包装形態</th>
		<th>包装単位数</th>
		<th>包装単位単位</th>
		<th>包装総量数</th>
		<th>包装総量単位</th>
		<th>区分</th>
		<th>製造会社</th>
		<th>販売会社</th>
		<th>更新区分</th>
		<th>更新年月日(yyyymmdd)</th>
		<th>剤形分類コード</th>
		<th>剤形分類</th>
		<th>成分分類コード</th>
		<th>成分分類</th>
		<th>薬効分類コード</th>
	</tr>
<? while($row = pg_fetch_array($sel)){ ?>
<?   $tr_color = @$tr_color=="#fff" ? "#f1f7fd": "#fff"; ?>
<?   $bg_color = $row["update_kubun"]=="4" ? "#999" : $tr_color; ?>
<?   $dummy = @$dummy=="disabled" ? "inactive": "disabled"; ?>
<?   $d = $row["op_time"]; ?>
<?   $ymdhms = substr($d,0,4)."/".substr($d,4,2)."/".substr($d,6,2)." ".substr($d,8,2).":".substr($d,10,2).":".substr($d,12,2); ?>


	<tr onmouseover="this.style.backgroundColor='#fefc8f';" onmouseout="this.style.backgroundColor='<?=$bg_color?>';" onclick="trClicked('<?=$row["hot13_cd"]?>');"
		style="background-color:<?=$bg_color?>; ime-mode:<?=$dummy?>; cursor:pointer">
		<td style="text-align:center"><?=$row["seq"]?"<span style='color:#f06'>済</span>":"未"?></td>
		<td><?=$row["hot13_cd"]?></td>
		<td><?=$row["hot7_cd"]?></td>
		<td><?=$row["company_cd"]?></td>
		<td><?=$row["cyouzai_cd"]?></td>
		<td><?=$row["buturyu_cd"]?></td>
		<td><?=$row["jan_cd"]?></td>
		<td><?=$row["unitary_cd"]?></td>
		<td><?=$row["yj_cd"]?></td>
		<td><?=$row["receipt1_cd"]?></td>
		<td><?=$row["receipt2_cd"]?></td>
		<td style="text-align:left"><?=$row["kokuji_name"]?></td>
		<td style="text-align:left"><?=$row["hanbai_name"]?></td>
		<td style="text-align:left"><?=$row["receipt_name"]?></td>
		<td style="text-align:left"><?=$row["kikaku_unit_name"]?></td>
		<td><?=$row["wrap_keitai"]?></td>
		<td><?=$row["wrap_unit_num"]?></td>
		<td><?=$row["wrap_unit_unit"]?></td>
		<td><?=$row["wrap_amount_num"]?></td>
		<td><?=$row["wrap_amount_unit"]?></td>
		<td><?=$row["kubun"]?></td>
		<td><?=$row["maker_name"]?></td>
		<td><?=$row["distributor_name"]?></td>
		<td><?=$row["update_kubun"]?></td>
		<td><?=$row["update_ymd"]?></td>
		<td><?=$row["zaikei_bunrui_cd"]?></td>
		<td><?=$row["zaikei_bunrui"]?></td>
		<td><?=$row["seibun_bunrui_cd"]?></td>
		<td><?=$row["seibun_bunrui"]?></td>
		<td><?=$row["yakkou_bunrui_cd"]?></td>
	</tr>
<? } ?>
</table>
</div>
<? } ?>


</body>
</html>
