<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 設備予約｜詳細</title>
<?
require_once("about_comedix.php");
require("label_by_profile_type.ini");
require("facility_common.ini");
require("schedule_repeat_common.php");

$fname = $PHP_SELF;

// データベースに接続
$con = connect2db($fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 予約情報を取得
$table_name = ($timeless != "t") ? "reservation" : "reservation2";
$sql = "select $table_name.*, (select fclcate_name from fclcate where fclcate.fclcate_id = $table_name.fclcate_id) as fclcate_name, (select facility_name from facility where facility.facility_id = $table_name.facility_id) as facility_name, (select emp_lt_nm || ' ' || emp_ft_nm from empmst where empmst.emp_id = $table_name.emp_id) as reg_emp_name, (select emp_lt_nm || ' ' || emp_ft_nm from empmst where empmst.emp_id = $table_name.upd_emp_id) as upd_emp_name, (select type_name from rsvtype where type_id = $table_name.type1) as type_name1, (select type_name from rsvtype where type_id = $table_name.type2) as type_name2, (select type_name from rsvtype where type_id = $table_name.type3) as type_name3, (select pt_flg from fclcate where fclcate.fclcate_id = $table_name.fclcate_id) as pt_flg, (select show_type_flg from facility where facility.facility_id = $table_name.facility_id) as show_type_flg from $table_name";
$cond = "where reservation_id = $id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$cate_name = pg_fetch_result($sel, 0, "fclcate_name");
$facility_name = pg_fetch_result($sel, 0, "facility_name");

$ymd = pg_fetch_result($sel, 0, "date");
$year = substr($ymd, 0, 4);
$month = substr($ymd, 4, 2);
$day = substr($ymd, 6, 2);

$s_time = pg_fetch_result($sel, 0, "start_time");
$s_hour = substr($s_time, 0, 2);
$s_minute = substr($s_time, 2, 2);

$e_time = pg_fetch_result($sel, 0, "end_time");
$e_hour = substr($e_time, 0, 2);
$e_minute = substr($e_time, 2, 2);

$title = pg_fetch_result($sel, 0, "title");
$content = pg_fetch_result($sel, 0, "content");

$users = pg_fetch_result($sel, 0, "users");
if ($users != "") {
	$users .= "名";
}

$reg_emp_name = pg_fetch_result($sel, 0, "reg_emp_name");
$upd_emp_name = pg_fetch_result($sel, 0, "upd_emp_name");

$type_name1 = pg_fetch_result($sel, 0, "type_name1");
$type_name2 = pg_fetch_result($sel, 0, "type_name2");
$type_name3 = pg_fetch_result($sel, 0, "type_name3");

$facility_id = pg_fetch_result($sel, 0, "facility_id");
$fclhist_id = pg_fetch_result($sel, 0, "fclhist_id");
$show_type_flg = pg_fetch_result($sel, 0, "show_type_flg");
$schd_flg = pg_fetch_result($sel, 0, "schd_flg");

$arr_type = array();
if ($type_name1 != "") {$arr_type[] = $type_name1;}
if ($type_name2 != "") {$arr_type[] = $type_name2;}
if ($type_name3 != "") {$arr_type[] = $type_name3;}
$type_string = join(", ", $arr_type);

// 予約患者情報を取得
$pt_flg = pg_fetch_result($sel, 0, "pt_flg");
if ($pt_flg == "t") {
	$table_name2 = ($timeless != "t") ? "rsvpt" : "rsvpt2";
	$sql = "select ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm from ptifmst inner join $table_name2 on ptifmst.ptif_id = $table_name2.ptif_id";
	$cond = "where $table_name2.reservation_id = $id order by $table_name2.order_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$pt_nms = array();
	while ($row = pg_fetch_array($sel)) {
		$pt_nms[] = $row["ptif_lt_kaj_nm"] . " " . $row["ptif_ft_kaj_nm"];
	}
	$pt_nms = implode(", ", $pt_nms);
}
// 内容形式によりテキストとテンプレートに表示を変更。
if ($fclhist_id == 0) {
	$content_type = 1;
} else {
	$fclhist_info = get_fclhist($con, $fname, $facility_id, $fclhist_id);
	$content_type = $fclhist_info["fclhist_content_type"];
}
// 内容編集
if ($content_type == "2") {
	$content = get_text_from_xml($content, "1");
}

// 登録対象者
$arr_target_id = array();
// 個人スケジュール
if ($schd_flg == "t") {
	// 同時予約したスケジュールIDを取得
	$schd_id = get_sync_schd_id($con, $fname, $id, $timeless);
	// スケジュールデータがある場合
	if ($schd_id != "") {
		// 対象職員情報を配列に格納
		$arr_target_id = get_emp_id_from_schedules($con, $schd_id, $timeless, $fname);
	}

}

$arr_target = array();
for ($i = 0; $i < count($arr_target_id); $i++) {
	$tmp_emp_id = $arr_target_id[$i];
	$sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
	$cond = "where emp_id = '$tmp_emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	}
	$tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
	array_push($arr_target, array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
}

$target_emp_name = "";
$is_first = true;
foreach($arr_target as $row)
{
	if($is_first)
	{
		$is_first = false;
	}
	else
	{
		$target_emp_name .= ",";
	}
	$tmp_emp_name = $row["name"];
	$target_emp_name .= "$tmp_emp_name";
}

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>詳細</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="4" class="list">
<tr height="22">
<td width="120" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">施設・設備</font></td>
<td width="480"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo("$cate_name &gt; $facility_name"); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">日付</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo("$year/$month/$day"); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?
if ($timeless != "t") {
	echo("$s_hour:{$s_minute}〜$e_hour:$e_minute");
} else {
	echo("指定なし");
}
?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">タイトル</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? eh($title); ?></font></td>
</tr>
<? if ($show_type_flg == "t") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">種別</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($type_string); ?></font></td>
</tr>
<? } ?>
<? if ($pt_flg == "t") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?
// 患者氏名/利用者氏名
echo $_label_by_profile["PATIENT_NAME"][$profile_type];
?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($pt_nms); ?></font></td>
</tr>
<? } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">内容</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo(preg_replace("/\r?\n/", "<br>", h($content))); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">登録対象者</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?
 echo($target_emp_name); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">利用人数</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($users); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">登録者</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($reg_emp_name); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">最終更新者</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($upd_emp_name); ?></font></td>
</tr>
</table>
</center>
</body>
</html>
