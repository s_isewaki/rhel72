<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_report_class.php");
require_once("smarty_setting.ini");
require_once("hiyari_common.ini");
require_once("show_class_name.ini");
require_once("hiyari_rm_stats_util.ini");
require_once("get_values.php");
require_once("Cmx.php");
require_once('Cmx/View/Smarty.php');

$fname = $PHP_SELF;

//==================================================
// セッションのチェック
//==================================================
$session = qualify_session($session, $fname);
if ($session == "0") {
	showLoginPage();
	exit;
}

//==================================================
// 権限のチェック
//==================================================
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
	showLoginPage();
	exit;
}

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

//==================================================
// データベースに接続
//==================================================
$con = connect2db($fname);

// 条件論理削除処理
if($_POST['postback_mode'] == 'delete') { 
	$all_stats_delete = implode(",", $_POST['stats_delete']);
	
	$sql = "UPDATE inci_stats_condition SET";
	$set = array("available");
	$setvalue = array("f");
	$cond = "WHERE condition_id in ({$all_stats_delete})";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
}



// 集計条件読込
$sql = "SELECT * FROM inci_stats_condition where available = 't' ORDER BY condition_id";
$result = select_from_table($con, $sql, '', $fname);
if($result == 0) { 
	pg_close($con);
	showErrorPage();
	exit;
}
$arr_stats_condition = pg_fetch_all($result);

//====================================
//表示
//====================================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("session", $session);

if ($arr_stats_condition){
    $smarty->assign("stats_condition", $arr_stats_condition);
}

if ($design_mode == 1){
    $smarty->display("hiyari_rm_stats_condition_load1.tpl");
}
else{
    $smarty->display("hiyari_rm_stats_condition_load2.tpl");
}

