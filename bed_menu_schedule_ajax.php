<?
ob_start();
require_once("about_session.php");
require_once("about_authority.php");
ob_end_clean();

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
	show_system_error();
	exit;
}

$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	show_system_error();
	exit;
}

$pt = $_POST["pt"];
$date = $_POST["date"];

$con = connect2db($fname);
pg_query($con, "begin");

// 入院予定患者の場合
if (strpos($pt, "in_res_pt_") === 0) {
	$type = preg_replace("/^.*_([1-3])$/", "$1", $pt);

	switch ($type) {

	case "1":  // 入院予定患者
		$pt_id = preg_replace("/^in_res_pt_(.*)_[1-3]$/", "$1", $pt);

		// 旬別への移動の場合
		if (strpos($date, "t") === 6) {
			$set = array("inpt_in_res_dt_flg", "inpt_in_res_ym", "inpt_in_res_td", "in_res_updated");
			$setvalue = array("f", substr($date, 0, 6), substr($date, 7, 1), date("YmdHis"));

		// 日別への移動の場合
		} else {
			$set = array("inpt_in_res_dt_flg", "inpt_in_res_dt", "in_res_updated");
			$setvalue = array("t", $date, date("YmdHis"));
		}

		$sql = "update inptres set";
		$cond = "where ptif_id = '$pt_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			show_system_error();
			exit;
		}
		$message = "入院予定日を変更しました。";
		break;

	case "2":  // 退院予定患者
		$pt_id = preg_replace("/^in_res_pt_(.*)_[1-3]$/", "$1", $pt);

		// 旬別への移動の場合
		if (strpos($date, "t") === 6) {
			pg_query($con, "rollback");
			pg_close($con);
			show_error("戻り入院予定は日付指定必須です。");
			exit;
		} else {
			$sql = "update inptmst set";
			$set = array("inpt_back_in_dt", "back_res_updated");
			$setvalue = array($date, date("YmdHis"));
			$cond = "where ptif_id = '$pt_id'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				show_system_error();
				exit;
			}
			$message = "戻り入院予定日を変更しました。";
		}
		break;

	case "3":  // 退院患者
		$pt_id = preg_replace("/^in_res_pt_(.*)_\d{8}_\d{4}_[1-3]$/", "$1", $pt);
		$in_dt = preg_replace("/^in_res_pt_.*_(\d{8})_\d{4}_[1-3]$/", "$1", $pt);
		$in_tm = preg_replace("/^in_res_pt_.*_\d{8}_(\d{4})_[1-3]$/", "$1", $pt);

		// 旬別への移動の場合
		if (strpos($date, "t") === 6) {
			pg_query($con, "rollback");
			pg_close($con);
			show_error("戻り入院予定は日付指定必須です。");
			exit;
		} else {
			$sql = "update inpthist set";
			$set = array("inpt_back_in_dt", "back_res_updated");
			$setvalue = array($date, date("YmdHis"));
			$cond = "where ptif_id = '$pt_id' and inpt_in_dt = '$in_dt' and inpt_in_tm = '$in_tm'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				show_system_error();
				exit;
			}
			$message = "戻り入院予定日を変更しました。";
		}
		break;

	default:
		show_system_error();
		exit;

	}

// 退院予定患者の場合
} else if (strpos($pt, "out_res_pt_") === 0) {
	$pt_id = preg_replace("/^out_res_pt_(.*)$/", "$1", $pt);

	// 入院状況レコードを削除
	$sql = "delete from inptcond";
	$cond = "where ptif_id = '$pt_id' and cond_check = '9' and hist_flg = 'f'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		show_system_error();
		exit;
	}

	// 退院予定時刻を取得
	$sql = "select inpt_out_res_tm from inptmst";
	$cond = "where ptif_id = '$pt_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		show_system_error();
		exit;
	}
	$time = pg_fetch_result($sel, 0, "inpt_out_res_tm");

	// 入院状況レコードを登録
	update_inpatient_condition($con, $pt_id, $date, $time, "9", $session, $fname, true);

	// 退院予定日を更新
	$sql = "update inptmst set";
	$set = array("inpt_out_res_dt", "out_res_updated");
	$setvalue = array($date, date("YmdHis"));
	$cond = "where ptif_id = '$pt_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		show_system_error();
		exit;
	}
	$message = "退院予定日を変更しました。";

// 転床予定患者の場合
} else if (strpos($pt, "move_res_pt_") === 0) {
	$pt_id = preg_replace("/^move_res_pt_(.*)$/", "$1", $pt);

	// 変更前の転床予定日時を取得
	$sql = "select move_dt, move_tm from inptmove";
	$cond = "where ptif_id = '$pt_id' and move_cfm_flg = 'f' and move_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		show_system_error();
		exit;
	}
	$org_date = pg_fetch_result($sel, 0, "move_dt");
	$org_time = pg_fetch_result($sel, 0, "move_tm");

	// 入院状況レコードを削除
	$sql = "delete from inptcond";
	$cond = "where ptif_id = '$ptif_id' and date = '$org_date' and hist_flg = 'f'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		show_system_error();
		exit;
	}

	// 入院状況レコードを更新
	update_inpatient_condition($con, $pt_id, $date, $org_time, "3", $session, $fname);

	// 転床予定日を更新
	$sql = "update inptmove set";
	$set = array("move_dt", "updated");
	$setvalue = array($date, date("YmdHis"));
	$cond = "where ptif_id = '$pt_id' and move_cfm_flg = 'f' and move_del_flg = 'f'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		show_system_error();
		exit;
	}

	$message = "転床予定日を変更しました。";

} else {
	show_system_error();
	exit;
}

pg_query($con, "commit");
pg_close($con);

header("HTTP/1.0 200 OK");
echo($message);

function show_error($message) {
	header("HTTP/1.0 400 Bad Request");
	echo($message);
}

function show_system_error() {
	show_error("エラーが発生しました。");
}

// 入院状況レコードを更新
function update_inpatient_condition($con, $pt_id, $date, $time, $status, $session, $fname) {
	// 入院情報を取得
	$sql = "select inpt_in_dt, inpt_in_tm, inpt_out_res_flg from inptmst";
	$cond = "where ptif_id = '$pt_id'";
	$sel = select_from_table($con, $sql ,$cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		show_system_error();
		exit;
	}
	$inpt_in_dt = pg_fetch_result($sel, 0, "inpt_in_dt");
	$inpt_in_tm = pg_fetch_result($sel, 0, "inpt_in_tm");
	$inpt_out_res_flg = pg_fetch_result($sel, 0, "inpt_out_res_flg");

	// 関連チェック
	if ($date < $inpt_in_dt) {
		pg_query($con, "rollback");
		pg_close($con);
		show_error("入院日より前には変更できません。");
		exit;
	}
	if ($time != "" && ($date == $inpt_in_dt && $time <= $inpt_in_tm)) {
		pg_query($con, "rollback");
		pg_close($con);
		show_error("入院日時より前には変更できません。");
		exit;
	}

	// 指定日付に移転予定情報があれば論理削除
	$sql = "update inptmove set";
	$set = array("move_del_flg");
	$setvalue = array("t");
	$cond = "where ptif_id = '$pt_id' and move_dt = '$date' and move_cfm_flg = 'f'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		show_system_error();
		exit;
	}

	// 指定日付に外出情報があれば削除
	$sql = "delete from inptgoout";
	$cond = "where ptif_id = '$pt_id' and out_type = '1' and out_date = '$date'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		show_system_error();
		exit;
	}

	// 指定日付を含む外泊情報があれば削除
	$sql = "select out_date, ret_date from inptgoout";
	$cond = "where ptif_id = '$pt_id' and out_type = '2' and to_date('$date', 'YYYYMMDD') between to_date(out_date, 'YYYYMMDD') and to_date(ret_date, 'YYYYMMDD')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		show_system_error();
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$sql = "delete from inptcond";
		$cond = "where ptif_id = '$pt_id' and to_date(date, 'YYYYMMDD') between to_date('{$row["out_date"]}', 'YYYYMMDD') and to_date('{$row["ret_date"]}', 'YYYYMMDD') and hist_flg = 'f'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			show_system_error();
			exit;
		}
	}
	$sql = "delete from inptgoout";
	$cond = "where ptif_id = '$pt_id' and out_type = '2' and to_date('$date', 'YYYYMMDD') between to_date(out_date, 'YYYYMMDD') and to_date(ret_date, 'YYYYMMDD')";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		show_system_error();
		exit;
	}

	// 「退院」の場合、退院予定・移転予定を削除
	if ($status == "2") {
		$sql = "delete from inptcond";
		$cond = "where ptif_id = '$pt_id' and cond_check in ('3', '9') and hist_flg ='f'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			show_system_error();
			exit;
		}

		$sql = "update inptmove set";
		$set = array("move_del_flg");
		$setvalue = array("t");
		$cond = "where ptif_id = '$pt_id' and move_cfm_flg = 'f' and move_del_flg = 'f'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			show_system_error();
			exit;
		}
	}

	// 入院状況IDを取得
	$cond_id = select_from_inpatient_condition_id($con, $pt_id, $fname);

	// 入院状況レコードをDELETE〜INSERT（「在院」の場合はDELETEのみ）
	$sql = "delete from inptcond";
	$cond = "where ptif_id = '$pt_id' and date = '$date' and hist_flg = 'f'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		show_system_error();
		exit;
	}
	if ($status != "8") {
		$sql = "insert into inptcond (pt_cond_id, ptif_id, cond_check, date) values (";
		$content = array($cond_id, $pt_id, $status, $date);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			show_system_error();
			exit;
		}
	}

	// 入院日に入院状況レコードがなければ作成
	$sql = "select count(*) from inptcond";
	$cond = "where ptif_id = '$pt_id' and date = '$inpt_in_dt' and hist_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		show_system_error();
		exit;
	}
	if (pg_fetch_result($sel, 0, 0) == 0) {
		$sql = "insert into inptcond (pt_cond_id, ptif_id, cond_check, date) values (";
		$content = array($cond_id, $pt_id, "1", $inpt_in_dt);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			show_system_error();
			exit;
		}
	}

	// 退院予定患者の退院予定がなくなった場合、入院患者扱いに戻す
	if ($inpt_out_res_flg == "t" && $status != "2") {
		$sql = "select count(*) from inptcond";
		$cond = "where ptif_id = '$pt_id' and cond_check = '9' and hist_flg = 'f'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			show_system_error();
			exit;
		}
		if (pg_fetch_result($sel, 0, 0) == 0) {
			$sql = "select emp_id from session";
			$cond = "where session_id = '$session'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				show_system_error();
				exit;
			}
			$emp_id = pg_fetch_result($sel, 0, "emp_id");

			$sql = "update inptmst set";
			$set = array("inpt_in_flg", "inpt_out_res_dt", "inpt_out_res_flg", "inpt_up_dt", "inpt_up_tm", "inpt_op_no", "inpt_out_res_tm", "inpt_result", "inpt_result_dtl", "inpt_out_pos_rireki", "inpt_out_pos_cd", "inpt_out_pos_dtl", "inpt_fd_end", "inpt_out_comment", "inpt_out_inst_cd", "inpt_out_sect_rireki", "inpt_out_sect_cd", "inpt_out_doctor_no", "inpt_out_city", "inpt_back_bldg_cd", "inpt_back_ward_cd", "inpt_back_ptrm_room_no", "inpt_back_bed_no", "inpt_back_in_dt", "inpt_back_in_tm", "inpt_back_sect_id", "inpt_back_dr_id", "inpt_back_change_purpose", "inpt_out_rsn_rireki", "inpt_out_rsn_cd");
			$setvalue = array("t", "", "f", date("Ymd"), date("Hi"), $emp_id, "", "", "", null, null, "", "", "", "", null, "", null, "", null, null, null, null, null, null, null, null, "", null, null);
			$cond = "where ptif_id = '$pt_id'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				show_system_error();
				exit;
			}
		}
	}
}

// 入院状況IDを取得
function select_from_inpatient_condition_id($con, $pt_id, $fname) {
	$sql = "select max(pt_cond_id) from inptcond";
	$cond = "where ptif_id = '$pt_id' and hist_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		show_system_error();
		exit;
	}
	$cond_id = pg_fetch_result($sel, 0, 0);
	if ($cond_id > 0) {
		return $cond_id;
	}

	$sql = "select max(pt_cond_id) from inptcond";
	$cond = "where ptif_id = '$pt_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		show_system_error();
		exit;
	}
	$cond_id = pg_fetch_result($sel, 0, 0);
	return ($cond_id + 1);
}
