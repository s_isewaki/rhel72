<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 休日出勤判定</title>
<?
require_once("about_comedix.php");
require_once("show_attendance_pattern.ini");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("show_timecard_common.ini");
require_once("atdbk_menu_common.ini");
require_once("timecard_hol_menu_common.php");
require_once("timecard_bean.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

//事由データ取得
$arr_rsn_list = get_project_list($con, $session, $fname);

//設定保存
if ($mode == "update") {
	//入力チェック
	for ($i=1; $i<$data_cnt; $i++) {
		if ($job_id[$i-1] == "") {
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('".($i+1)."行目の職種を指定してください。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
	}
	pg_query($con, "begin transaction");
	//削除
	$sql = "delete from timecard_holwk_day";
	$cond = "";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	for ($i=0; $i<$data_cnt; $i++) {
		//1行目
		if ($i==0) {
			$wk_job_id = 0;
			$wk_duty_form = 0;
		} else {
			//2行目以降
			$wk_job_id = $job_id[$i-1];
			$wk_duty_form = $duty_form[$i-1];
			if ($wk_duty_form == "") {
				$wk_duty_form = "0";
			}
		}
		$varname0 = "hol_{$i}_0";
		$varname1 = "hol_{$i}_1";
		$varname2 = "hol_{$i}_2";
		$varname3 = "hol_{$i}_3";
		$legal_holiday = ($$varname0 == "t") ? "t" : "f";
		$national_holiday = ($$varname1 == "t") ? "t" : "f";
		$newyear_holiday = ($$varname2 == "t") ? "t" : "f";
		$prescribed_holiday = ($$varname3 == "t") ? "t" : "f";
		$sql = "insert into timecard_holwk_day (order_no, job_id, duty_form, legal_holiday, national_holiday, newyear_holiday, prescribed_holiday) values (";
		$content = array($i, $wk_job_id, $wk_duty_form, $legal_holiday, $national_holiday, $newyear_holiday, $prescribed_holiday);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
    //公休の日の残業を休日残業としない。20140512
    if ($no_hol_overtime_flg == "") {
        $no_hol_overtime_flg = "f";
    }
    if (empty($sixty_overtime_nonlegal_flg)) {
    	$sixty_overtime_nonlegal_flg = "f";
    }
    $sql = "update timecard set";
    $set = array("no_hol_overtime_flg","sixty_overtime_nonlegal_flg");
    $setvalue = array($no_hol_overtime_flg,$sixty_overtime_nonlegal_flg);
    $cond = "";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    // delete 
    $del_rsn = delete_from_table($con, "delete from timecard_hol_ovtm_except_reason", '', $fname);
    if ($del_rsn === 0) {
        pg_query($con, "rollback");
        pg_close($con);
        js_error_exit();
    }
    // insert
    $reason_list = explode(',', $_POST['reason_list']);
    foreach ($reason_list as $i => $id) {
        //空白を除く
        if (empty($id)) {
            continue;
        }
        $ins = insert_into_table($con, "insert into timecard_hol_ovtm_except_reason (no, reason_id) values (", array($i + 1, $id), $fname);
        if ($ins === 0) {
            pg_query($con, "rollback");
            pg_close($con);
            js_error_exit();
        }
    }
    
	pg_query("commit");

	$arr_holwk = get_timecard_holwk_day($con, $fname, "");

	$data_cnt = count($arr_holwk);
} else {
	// 再表示時、データ再設定
	if ($data_cnt > 0) {
		$m=0;
		for($i=0; $i<$data_cnt; $i++) {
			//削除行以外
			$wk_flg = "";
			if ($del_idx == "") {
				$wk_flg = "1";
			} else {
				if ($i != $del_idx) {
					$wk_flg = "1";
				}
			}
			if ($wk_flg == "1") {
				$varname0 = "hol_{$i}_0";
				$varname1 = "hol_{$i}_1";
				$varname2 = "hol_{$i}_2";
				$varname3 = "hol_{$i}_3";
				$legal_holiday = ($$varname0 == "t") ? "t" : "f";
				$national_holiday = ($$varname1 == "t") ? "t" : "f";
				$newyear_holiday = ($$varname2 == "t") ? "t" : "f";
				$prescribed_holiday = ($$varname3 == "t") ? "t" : "f";
				$arr_holwk[$m]["order_no"] = $m;								//表示順
				//2行目以降の場合（1行目は標準のため）
				if ($i > 0) {
					$arr_holwk[$m]["job_id"] = $job_id[$i-1];				//職種
					$arr_holwk[$m]["duty_form"] = $duty_form[$i-1];			//勤務形態
				}
				$arr_holwk[$m]["legal_holiday"] = $legal_holiday;			//法定
				$arr_holwk[$m]["national_holiday"] = $national_holiday;		//祝祭日
				$arr_holwk[$m]["newyear_holiday"] = $newyear_holiday;		//年末年始
				$arr_holwk[$m]["prescribed_holiday"] = $prescribed_holiday;//所定
				$m++;
			}
			//追加時
			if (($add_idx != "") && ($i == $add_idx)) {
				$arr_holwk[$m]["order_no"] = $m;								//表示順
				$arr_holwk[$m]["job_id"] = "";								//職種
				$arr_holwk[$m]["duty_form"] = "";								//勤務形態
				$arr_holwk[$m]["legal_holiday"] = "";							//法定
				$arr_holwk[$m]["national_holiday"] = "";						//祝祭日
				$arr_holwk[$m]["newyear_holiday"] = "";						//年末年始
				$arr_holwk[$m]["prescribed_holiday"] = "";					//所定
				$m++;
			}
		}
		$data_cnt = $m;
	} else {
		//初期表示時、DBから取得
		//休日出勤と判定する日設定取得
		$arr_holwk = get_timecard_holwk_day($con, $fname, "");

		$data_cnt = count($arr_holwk);
        
        // 事由
        $reason_list = array();
        $sql_rsn = "select * from timecard_hol_ovtm_except_reason order by no";
        $sel_rsn = select_from_table($con, $sql_rsn, "", $fname);
        if ($sel_rsn === 0) {
            pg_close($con);
            js_error_exit();
        }
        while ($row = pg_fetch_assoc($sel_rsn)) {
            $reason_list[] = $row['reason_id'];
        }
    }
    $timecard_bean = new timecard_bean();
    $timecard_bean->select($con, $fname);
    
    $no_hol_overtime_flg = $timecard_bean->no_hol_overtime_flg;
    $sixty_overtime_nonlegal_flg = $timecard_bean->sixty_overtime_nonlegal_flg; // 20140908
    
}
$no_hol_overtime_checked = ($no_hol_overtime_flg =="t") ? " checked" : "";
$sixty_overtime_checked = ($sixty_overtime_nonlegal_flg =="t") ? " checked" : "";
//職種取得
$data_job = get_jobmst_array($con, $fname);

//休日出勤判定にカレンダーマスタを利用しない勤務グループ取得
//配列のインデックス　1から始まるので注意
//$arr_data = get_timecard_holwk_group($con, $fname);

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
	///-----------------------------------------------------------------------------
	// 追加
	///-----------------------------------------------------------------------------
	function addData(idx)
	{
		//行追加
		document.mainform.del_idx.value = "";
		document.mainform.add_idx.value = idx;
		document.mainform.action="timecard_holwk_day.php";
		document.mainform.submit();
	}
	///-----------------------------------------------------------------------------
	// 削除
	///-----------------------------------------------------------------------------
	function delData(idx)
	{
		//行削除
		document.mainform.del_idx.value = idx;
		document.mainform.add_idx.value = "";
		document.mainform.action="timecard_holwk_day.php";
		document.mainform.submit();
	}
	///-----------------------------------------------------------------------------
	// 登録
	///-----------------------------------------------------------------------------
	function updateData()
	{
		document.mainform.mode.value="update";
        
	    document.mainform.reason_list.value = '';
	    for (var i = 0, j = document.mainform.rsn.options.length; i < j; i++) 
	    {
		    if (i > 0) 
		    {
			    document.mainform.reason_list.value += ',';
		    }
		    document.mainform.reason_list.value += document.mainform.rsn.options[i].value;
	    }
        
		document.mainform.action="timecard_holwk_day.php";
		document.mainform.submit();
	}
/**
* selectItem 全リストら指定された項目を選択済みボックスへコピーする
* @param srcname　全リスト
* @param dstname　選択したい項目
**/	
function selectItem(srcname, dstname) 
{
	srcobj = document.getElementById(srcname);
	dstobj = document.getElementById(dstname);
	for (var i = 0, j = srcobj.options.length; i < j; i++)
	{
		if (srcobj.options[i].selected) 
		{
			var id = srcobj.options[i].value;
			var name = srcobj.options[i].text;

			var in_group = false;
			for (var k = 0, l = dstobj.options.length; k < l; k++) 
			{
				if (dstobj.options[k].value == id) 
				{
					in_group = true;
					break;
				}
			}

			if (!in_group) 
			{
				addOption(dstobj, id, name, '');
			}
		}
	}
}

/**
* addOption 実データを設定する
* @param box　指定されたリストボックス
* @param value　指定されたリストボックスに設定する値
* @param text　指定されたリストボックスの名前
* @param selected　選択されている項目
**/	
function addOption(box, value, text, selected) 
{
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
	if (selected == value) 
	{
		box.selectedIndex = box.options.length -1;
		return;
	}
}


/**
* deleteItem 選択済みボックスから項目を削除する
* @param objname　指定されたリストボックスの名前（ID）
**/
function deleteItem(objname) 
{
	obj = document.getElementById(objname);
	for (var i = obj.options.length - 1; i >= 0; i--) 
	{
		if (obj.options[i].selected) 
		{
			obj.options[i] = null;
		}
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#bdd1e7 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block th {background: #bdd1e7; border:#5279a5 solid 1px; font:"ＭＳ Ｐゴシック"; }
button.select {
    display: block;
    width: 80px;
    margin: 20px 50px;
}
select.select {
    width: 200px;
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
<tr>
<td>
<?
//休暇種別メニュータブ表示
show_timecard_hol_menuitem($session,$fname);
?>
</td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>

<form name="mainform" action="timecard_holwk_day.php" method="post">

<table>
	<tr>
		<td colspan="2" align="right"><input type="button" value="登録" onClick="updateData();"></td>
	</tr>
	<tr>
		<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		チェックした休日に出勤した場合に休日出勤と自動判定する。<br>
		（平日でも、事由に「休日出勤」選択された場合にも、休日出勤と判定する。）<br>
		</font>
		</td>
	</tr>
	<tr>

		<td>
			<table width="720" border="0" cellspacing="0" cellpadding="1"  class="list">
	<tr bgcolor="#f6f9ff">
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種&nbsp;&nbsp;勤務形態</font></td>
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休日出勤として扱う日（カレンダーマスタ）</font></td>
		<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
	</tr>
<?
for ($i=0; $i<count($arr_holwk); $i++) {
	$wk_id = $i+1;
	echo("<tr>");
	//職種、勤務形態
	echo("<td width=\"240\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
	//
	if ($i == 0) {
		echo("標準");
	} else {
		echo("<select name=\"job_id[]\">\n");
		echo("<option value=\"\">\n");
		for ($m=0; $m<count($data_job); $m++) {
			$wk_id = $data_job[$m]["id"];
			$wk_name = $data_job[$m]["name"];
			echo("<option value=\"$wk_id\"");
			if ($arr_holwk[$i]["job_id"] == $wk_id) { //
				echo(" selected");
			}
			echo(">$wk_name \n");
		}
		echo("</select> \n");
		$arr_duty_form = array("常勤", "非常勤");
		echo("<select name=\"duty_form[]\">\n");
		echo("<option value=\"\">\n");
		for ($m=0; $m<count($arr_duty_form); $m++) {
			$wk_id = $m + 1;
			$wk_name = $arr_duty_form[$m];
			echo("<option value=\"$wk_id\"");
			if ($arr_holwk[$i]["duty_form"] == $wk_id) { //
				echo(" selected");
			}
			echo(">$wk_name \n");
		}
		echo("</select> \n");
	}
	echo("</font></td>");
	echo("<td width=\"380\" align=\"center\">");
	$legal_holiday_checked = ($arr_holwk[$i]["legal_holiday"]=="t") ? " checked" : "";
	$national_holiday_checked = ($arr_holwk[$i]["national_holiday"]=="t") ? " checked" : "";
	$newyear_holiday_checked = ($arr_holwk[$i]["newyear_holiday"]=="t") ? " checked" : "";
	$prescribed_holiday_checked = ($arr_holwk[$i]["prescribed_holiday"] == "t") ? " checked" : "";
?>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<label><input type="checkbox" name="hol_<?=$i?>_0" value="t" <?=$legal_holiday_checked?>>法定休日</label>
		&nbsp;&nbsp;
		<label><input type="checkbox" name="hol_<?=$i?>_1" value="t" <?=$national_holiday_checked?>>祝祭日</label>
		&nbsp;&nbsp;
		<label><input type="checkbox" name="hol_<?=$i?>_2" value="t" <?=$newyear_holiday_checked?>>年末年始休暇</label>
		&nbsp;&nbsp;
		<label><input type="checkbox" name="hol_<?=$i?>_3" value="t" <?=$prescribed_holiday_checked?>>所定休日</label>
	</font>
<?
	echo("</td>");
	//追加、削除ボタン
	echo("<td width=\"\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
	echo("<input type=\"button\" value=\"追加\" onclick=\"addData($i);\"> \n");
	if ($i > 0) {
		echo("<input type=\"button\" value=\"削除\" onclick=\"delData($i);\"> \n");
	}
	echo("</font></td>");
	echo("</tr>");
}

?>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="left">
        	<label><input type="checkbox" name="no_hol_overtime_flg" value="t" <? echo($no_hol_overtime_checked); ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公休の日の残業を休日残業としない。</font></label>
</td>
	</tr>
	<tr>
		<td>
			<label><input type="checkbox" name="sixty_overtime_nonlegal_flg" value="t" <? echo($sixty_overtime_checked); ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">法定休暇以外の休日残業とする休暇は６０時間超の残業時間の計算に含める。</font></label>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="left">
        	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><br>　休日残業として計算しない休暇事由（公休以外）</font>
            <table width="600">
                <tr>
                    <td>
                        <select class="select" name="rsn" id="rsn" size="7" multiple>
<?
show_rsn_options($arr_rsn_list, "reg", $reason_list);
                            ?>
                        </select>
                    </td>
                    <td>
                        <button class="select" type="button" onclick="selectItem('rsn_list', 'rsn');">←</button>
                        <button class="select" type="button" onclick="deleteItem('rsn');">→</button>
                    </td>
                    <td>
                        <select class="select" name="rsn_list" id="rsn_list" size="7" multiple>
<?
show_rsn_options($arr_rsn_list, "all", array());
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
</td>
	</tr>
	<tr>
		<td colspan="2" align="right"><input type="button" value="登録" onClick="updateData();"></td>
	</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="postback" value="t">
<input type="hidden" name="mode" value="">
<input type="hidden" name="data_cnt" value="<? echo($data_cnt); ?>">
<input type="hidden" name="add_idx" value="<? echo($add_idx); ?>">
<input type="hidden" name="del_idx" value="<? echo($del_idx); ?>">
<input type="hidden" name="reason_list" value="">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>

<?
/*************************************************************************/
// 職種ＤＢ(jobmst)情報取得
// @param
//
// @return	$data_job	取得情報
/*************************************************************************/
function get_jobmst_array($con, $fname) {
	//-------------------------------------------------------------------
	//検索
	//-------------------------------------------------------------------
	$sql = "select job_id,job_nm from jobmst";
	$cond = "where job_del_flg = 'f' order by order_no";
	$sel = select_from_table($con, $sql, $cond, $fname);		//職種一覧を取得
	if($sel == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);
	//-------------------------------------------------------------------
	//情報設定
	//-------------------------------------------------------------------
	$data_job = array();
	for($i=0;$i<$num;$i++){
		$data_job[$i]["id"] = pg_result($sel,$i,"job_id");
		$data_job[$i]["name"] = pg_result($sel,$i,"job_nm");
	}

	return $data_job;
}

/**
 * 事由メニュー用にoptionを出力
 * @param type $arr_rsn_list 事由情報
 * @param type $reg_all_flg reg:登録分 all:全て
 * @param type $tgt_ids 登録分の場合、idの配列
 */
function show_rsn_options($arr_rsn_list, $reg_all_flg, $tgt_ids) {
    
    if ($reg_all_flg === "all") {
        foreach ($arr_rsn_list as $row) {
            printf('<option value="%s">%s</option>', h($row["reason_id"]), h($row["reason_name"]));
            echo("\n");
        }
    }
    else {
        $rsn_name_list = array();
        foreach ($arr_rsn_list as $row) {
            $rsn_name_list[$row["reason_id"]] = $row["reason_name"];
        }
        foreach ($tgt_ids as $id) {
            if (empty($id)) {
                continue;
            }
            printf('<option value="%s">%s</option>', h($id), h($rsn_name_list[$id]));
        }
    }
}

// 事由データを取得（オプションリスト用）
function get_project_list($con, $session, $fname) {
    $arr_list = array();
    // 事由情報を取得
    $sql = "select reason_id, default_name, display_name from atdbk_reason_mst ";
    $cond = "where display_flag = 't' and reason_id != '24' order by sequence ";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    
    
    // 0件の場合は復帰
    if (pg_num_rows($sel) == 0) {
        return $arr_list;
    }
    
    $reasons = pg_fetch_all($sel);
    
    foreach ($reasons as $tmp_reason) 
    {
        //データ設定
        if ($tmp_reason["display_name"] != "") {
            $tmp_reason["reason_name"] = $tmp_reason["display_name"];
        }
        else {
            $tmp_reason["reason_name"] = $tmp_reason["default_name"];
        }
            
            
        $arr_list[] = $tmp_reason;
        
    }
    return $arr_list;
}


?>