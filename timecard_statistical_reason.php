<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理｜理由別残業統計</title>
<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("show_select_values.ini");
require_once("atdbk_menu_common.ini");
require_once("show_timecard_common.ini");
require_once("get_values.ini");
require_once("timecard_tantou_class.php");

// ページ名func
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0" && $mmode != "usr") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// DBコネクション作成
$con = connect2db($fname);

// 遷移元の設定
if ($work != "") {
	set_referer($con, $session, "work", $work, $fname);
	$referer = $work;
} else {
	$referer = get_referer($con, $session, "work", $fname);
}

// 締め日を取得
// ************ oose update start *****************
//$sql = "select closing from timecard";
$sql = "select closing, closing_parttime, closing_month_flg from timecard";
// ************ oose update end *****************
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$closing = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing") : "";
//未設定時は末日とする 20110720
if ($closing == "") {
	$closing = "6";
}
// ************ oose add start *****************
$closing_parttime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_parttime") : "";
// ************ oose add end *****************
$closing_month_flg = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_month_flg") : "1";
//非常勤が未設定時は常勤を使用する 20091214
if ($closing_parttime == "") {
	$closing_parttime = $closing;
}

$year = date("Y");
$month = date("n");
$yyyymm = $year . date("m");

// 開始日・締め日を算出
$calced = false;
switch ($closing) {
case "1":  // 1日
	$closing_day = 1;
	break;
case "2":  // 5日
	$closing_day = 5;
	break;
case "3":  // 10日
	$closing_day = 10;
	break;
case "4":  // 15日
	$closing_day = 15;
	break;
case "5":  // 20日
	$closing_day = 20;
	break;
case "6":  // 末日
	$start_year = $year;
	$start_month = $month;
	$start_day = 1;
	$end_year = $start_year;
	$end_month = $start_month;
	$end_day = days_in_month($end_year, $end_month);
	$calced = true;
	break;
}
if (!$calced) {
	$day = date("j");
	$start_day = $closing_day + 1;
	$end_day = $closing_day;

	if ($day <= $closing_day) {
		$start_year = $year;
		$start_month = $month - 1;
		if ($start_month == 0) {
			$start_year--;
			$start_month = 12;
		}

		$end_year = $year;
		$end_month = $month;
	} else {
		$start_year = $year;
		$start_month = $month;

		$end_year = $year;
		$end_month = $month + 1;
		if ($end_month == 13) {
			$end_year++;
			$end_month = 1;
		}
	}
}

$arr_class_name = get_class_name_array($con, $fname);

$emp_id = get_emp_id($con, $session, $fname);
$timecard_tantou_class = new timecard_tantou_class($con, $fname);
if ($mmode == "usr") {
	$timecard_tantou_branches = $timecard_tantou_class->get_tantou_branches($emp_id);
}
else {
	$timecard_tantou_branches = array();
}
// 組織情報を取得し、配列に格納
list($arr_cls, $arr_clsatrb, $arr_atrbdept) = $timecard_tantou_class->get_clsatrb_info($timecard_tantou_branches, $mmode);

?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--
function initPage() {
	classOnChange('<?php echo($atrb); ?>', '<?php echo($dept); ?>');
}

function classOnChange(atrb_id, dept_id) {
	if (!document.mainform) {
		return;
	}

	var class_id = document.mainform.cls.value;

	deleteAllOptions(document.mainform.atrb);

	addOption(document.mainform.atrb, '-', '----------', atrb_id);

<?php foreach ($arr_clsatrb as $tmp_clsatrb) { ?>
	if (class_id == '<?php echo $tmp_clsatrb["class_id"]; ?>') {
	<?php foreach($tmp_clsatrb["atrbs"] as $tmp_atrb) { ?>
		addOption(document.mainform.atrb, '<?php echo $tmp_atrb["id"]; ?>', '<?php echo $tmp_atrb["name"]; ?>', atrb_id);
	<?php } ?>
	}
<?php } ?>

	atrbOnChange(dept_id);
}

function atrbOnChange(dept_id) {
	var atrb_id = document.mainform.atrb.value;

	deleteAllOptions(document.mainform.dept);

	addOption(document.mainform.dept, '-', '----------', dept_id);

<?php foreach ($arr_atrbdept as $tmp_atrbdept) { ?>
	if (atrb_id == '<?php echo $tmp_atrbdept["atrb_id"]; ?>') {
	<?php foreach($tmp_atrbdept["depts"] as $tmp_dept) { ?>
		addOption(document.mainform.dept, '<?php echo $tmp_dept["id"]; ?>', '<?php echo $tmp_dept["name"]; ?>', dept_id);
	<?php } ?>
	}
<?php } ?>
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
	if (selected == value) {
		box.selectedIndex = box.options.length -1;
	}
}

//ダウンロード
function downloadCSV() {
	if (check_date() == false){
		return false;
	}
	//document.mainform.action="timecard_statistical_reason_csv.php";
	document.mainform.submit();
}
function check_date(){
	start_yr = document.mainform.select_start_yr.value;
	start_mon = document.mainform.select_start_mon.value;
	start_day = document.mainform.select_start_day.value;

	end_yr = document.mainform.select_end_yr.value;
	end_mon = document.mainform.select_end_mon.value;
	end_day = document.mainform.select_end_day.value;

	start_dt = new Date(start_yr, start_mon-1, start_day);
	end_dt = new Date(end_yr, end_mon-1, end_day);

	msg = "";
	if (start_dt.getFullYear() != start_yr || start_dt.getMonth() != start_mon-1 || start_dt.getDate() != start_day) {
		msg += "開始日付が正しく指定されていません。" + "\n";
	}

	if (end_dt.getFullYear() != end_yr || end_dt.getMonth() != end_mon-1 || end_dt.getDate() != end_day) {
		msg += "終了日付が正しく指定されていません。" + "\n";
	}

	if (start_dt > end_dt){
		msg += "日付が正しく指定されていません。" + "\n";
	}
	
	if (msg != ""){
		alert(msg);
		return false;
	}

	return true;
}

//終了日月末設定
function set_last_day() {
	day = document.mainform.select_end_day.value;
	day = parseInt(day,10);
	if (day >= 28) {
		year = document.mainform.select_end_yr.value;
		month = document.mainform.select_end_mon.value;
		last_day = days_in_month(year, month);
		if (last_day != day) {
			day = last_day;
			dd = (day < 10) ? '0'+day : day;
			document.mainform.select_end_day.value = dd;
		}
	}

}
//月末取得
function days_in_month(year, month) {
	//月別日数配列
	lastDay = new Array(31,28,31,30,31,30,31,31,30,31,30,31,29);
	//閏年
	wk_year = parseInt(year,10);
	wk_mon = parseInt(month,10);
	if ((wk_mon == 2) &&
		((wk_year % 4) == 0 && ((wk_year % 100) != 0 || (wk_year % 400)))) {
		wk_mon = 13;
	}

	days = lastDay[wk_mon-1];
	return days;
}

<?php
//条件に合う職員をプログラム内で検索するため、CSVダウンロードと同様の処理、引数渡しとする。
?>
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
table.block3 {border-collapse:collapse;}
table.block3 td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($mmode == "usr") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a></font></td>
<? if ($checkauth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>&work=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<?
}
elseif ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<?php echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<?php } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<?php } ?>

<?php
// メニュータブ
if ($mmode != "usr") {
	show_work_admin_menuitem($session, $fname, "");
}
else {
	show_atdbk_menuitem($session, $fname, "");
}
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<?php
//サブメニュー表示
show_work_admin_submenu_statiscal($session, $fname, "");
?>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="mainform" action="timecard_statistical_reason_csv.php" method="post">
	<table width="1050" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td>
			<b>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">理由別残業統計</font>
			</b>
		</td>
	</tr>
	<tr>
	<td>
		<table border="0" cellspacing="0" cellpadding="1" class="block3">
	    <tr height="22">
		<td nowrap valign="top">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<select name="cls" onchange="classOnChange();">
				<option value="-">----------
<?php
foreach ($arr_cls as $tmp_cls) {
	$tmp_id = $tmp_cls["id"];
	$tmp_name = $tmp_cls["name"];
	echo("<option value=\"$tmp_id\"");
	if ($tmp_id == $cls) {
		echo(" selected");
	}
	echo(">$tmp_name");
}
?>
			</select><?php echo($arr_class_name[0]); ?>
			</font>
		</td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<select name="atrb" onchange="atrbOnChange();"></select><?php echo($arr_class_name[1]); ?>
			</font>
		</td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<select name="dept"></select><?php echo($arr_class_name[2]); ?>
			</font>
		</td>
	    </tr>
	    </font>
		</table>
	</td>
	</tr>
	<tr>
	<td nowrap valign="top">
		<table border="0" cellspacing="0" cellpadding="1" class="block3">
		<tr>
			<td>
				<select name="select_start_yr">
				<?php show_select_years(10, $start_year, false, true); ?>
				</select>年<select name="select_start_mon">
				<?php show_select_months($start_month, false); ?>
				</select>月<select name="select_start_day">
				<?php show_select_days($start_day, false); ?>
				</select>日 〜 <select name="select_end_yr" onchange="set_last_day();">
				<?php show_select_years(10, $end_year, false, true); ?>
				</select>年<select name="select_end_mon" onchange="set_last_day();">
				<?php show_select_months($end_month, false); ?>
				</select>月<select name="select_end_day">
				<?php show_select_days($end_day, false); ?>
				</select>日
			</td>
			<td>
			</td>
			<td><input type="submit" value="CSV出力" onclick="return downloadCSV();"></td>
		</tr>
		</table>
	</td>
	</tr>
	</table>

<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="cls_name" value="<?php echo($arr_class_name[0]); ?>">
<input type="hidden" name="atrb_name" value="<?php echo($arr_class_name[1]); ?>">
<input type="hidden" name="dept_name" value="<?php echo($arr_class_name[2]); ?>">
<input type="hidden" name="mmode" value="<?php echo($mmode); ?>">
</td>
</tr>
</table>
</form>
</body>
<?php pg_close($con); ?>
</html>

