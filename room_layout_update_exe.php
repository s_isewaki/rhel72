<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="room_layout.php">
<input type="hidden" name="back" value="t">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="room_count1" value="<? echo($room_count1); ?>">
<?
for ($i = 1; $i <= 24; $i++) {
	$var_name = "pt_info$i";
?>
<input type="hidden" name="<? echo($var_name); ?>" value="<? echo($$var_name); ?>">
<?
}
?>
<input type="hidden" name="refresh_sec" value="<? echo($refresh_sec); ?>">
<input type="hidden" name="warn_time" value="<? echo($warn_time); ?>">
<?
// レイアウトデータを配列で定義
$layout_detail = array(
	  2 => 2,  // 入り口（上） 1〜2床
	  4 => 4,  // 入り口（上） 3〜4床
	  6 => 6,  // 入り口（上） 5〜6床
	  8 => 8,  // 入り口（上） 7〜8床
	  0 => 5,  // 入り口（上） 2〜5床、縦置き
	102 => 2,  // 入り口（下） 1〜2床
	104 => 4,  // 入り口（下） 3〜4床
	106 => 6,  // 入り口（下） 5〜6床
	108 => 8,  // 入り口（下） 7〜8床
	100 => 5,  // 入り口（下） 2〜5床、縦置き
	202 => 2,  // 入り口（右） 1〜2床
	204 => 4,  // 入り口（右） 3〜4床
	206 => 6,  // 入り口（右） 5〜6床
	200 => 5,  // 入り口（右） 2〜5床、横置き
	302 => 2,  // 入り口（左） 1〜2床
	304 => 4,  // 入り口（左） 3〜4床
	306 => 6,  // 入り口（左） 5〜6床
	300 => 5   // 入り口（左） 2〜5床、横置き
);

foreach ($layout_detail as $key => $bed_count) {
	for ($i = 1; $i <= $bed_count; $i++) {
		$elm = "bed_{$key}_{$i}";
		$val = $_POST[$elm];
		echo("<input type=\"hidden\" name=\"$elm\" value=\"$val\">\n");
	}
}
?>
</form>
<?
require("about_authority.php");
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkuath = check_authority($session, 21, $fname);
if ($checkuath == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($room_count1 == "") {
	echo("<script type=\"text/javascript\">alert('「1行に表示する病室数」が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (preg_match("/^\d{1,2}$/", $room_count1) == 0) {
	echo("<script type=\"text/javascript\">alert('「1行に表示する病室数」は半角数字1〜2桁で入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($warn_time == "") {
	echo("<script type=\"text/javascript\">alert('「待機患者警告時間」が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (preg_match("/^\d{1,3}$/", $warn_time) == 0) {
	echo("<script type=\"text/javascript\">alert('「待機患者警告時間」は半角数字1〜3桁で入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($refresh_sec == "") {
	echo("<script type=\"text/javascript\">alert('「画面更新間隔」が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (preg_match("/^\d{1,4}$/", $refresh_sec) == 0) {
	echo("<script type=\"text/javascript\">alert('「画面更新間隔」は半角数字1〜4桁で入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// 登録値を編集
$pt_info1 = "t";
for ($i = 2; $i <= 24; $i++) {
	$var_name = "pt_info$i";
	if ($$var_name != "t") {
		$$var_name = "f";
	}
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// レイアウト情報をDELETE〜INSERT
$sql = "delete from roomlayout";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sql = "insert into roomlayout (room_count1, pt_info1, pt_info2, pt_info3, pt_info4, pt_info5, pt_info6, pt_info7, pt_info8, pt_info9, pt_info10, pt_info11, pt_info12, pt_info13, pt_info14, pt_info15, pt_info16, pt_info17, pt_info18, pt_info19, pt_info20, pt_info21, pt_info22, pt_info23, pt_info24, warn_time, refresh_sec) values (";
$content = array($room_count1, $pt_info1, $pt_info2, $pt_info3, $pt_info4, $pt_info5, $pt_info6, $pt_info7, $pt_info8, $pt_info9, $pt_info10, $pt_info11, $pt_info12, $pt_info13, $pt_info14, $pt_info15, $pt_info16, $pt_info17, $pt_info18, $pt_info19, $pt_info20, $pt_info21, $pt_info22, $pt_info23, $pt_info24, $warn_time, $refresh_sec);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// ベッド表示位置情報を更新
$sql = "update bedlayout set";
$set = array("position");
foreach ($layout_detail as $key => $bed_count) {
	for ($i = 1; $i <= $bed_count; $i++) {
		$elm = "bed_{$key}_{$i}";
		list($prefix, $key, $bed_no) = explode("_", $elm);

		$setvalue = array($_POST[$elm]);
		$cond = "where layout_key = $key and bed_no = $bed_no";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'room_layout.php?session=$session';</script>");
?>
</body>
