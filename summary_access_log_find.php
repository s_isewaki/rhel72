<? ob_start(); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
$fname = $_SERVER["PHP_SELF"];
require("about_authority.php");
require("about_session.php");
require("get_values.php");
//require("show_sinryo_kubun_list.ini");
require("show_sinryo_top_header.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require("summary_common.ini");

$initerror = true;
for (;;){
	//セッションのチェック
	$session = qualify_session($session,$fname);
	if(!$session) break;
	// 役職登録権限チェック
	$checkauth = check_authority($session,59,$fname);
	if(!$checkauth) break;
	//DB接続
	$con = connect2db($fname);
	if(!$con) break;
	$initerror = false;
	break;
}
// エラー時は終了
if ($initerror){
	ob_end_flush();
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo $c_sot_util->javascript("showLoginPage(window);");
	exit;
}

if (@$_REQUEST["mode"] == "regist"){
	$sel = select_from_table($con,"select * from summary_option","",$fname);
	$row = pg_fetch_array($sel);
	if (!$row) update_set_table($con, "insert into summary_option (is_activate_access_log) values (null);", array(), null, "", $fname);

	$sql =
		" update summary_option set".
		" is_hide_checklist_window = " . (int)$_REQUEST["is_hide_checklist_window"].
		",is_hide_eiyou_monsin_window = " . (int)$_REQUEST["is_hide_eiyou_monsin_window"].
		",is_activate_access_log = " . (int)$_REQUEST["is_activate_access_log"];
	$upd = update_set_table($con, $sql, array(), null, "", $fname);
	ob_end_clean();
	header("Location: summary_kanri_option.php?session=".$session);
	die;
}

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';




// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);

// DBコネクションの作成
$con = connect2db($fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];
?>
<title>CoMedix <? echo($med_report_title); ?>管理 | オプション</title>
<? /*
<script language="JavaScript" type="text/JavaScript">
<!--
function deleteStatus() {
  if (confirm("削除してよろしいですか？")) {
    document.status.submit();
  }
}
-->
</script>
*/ ?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td { border:#5279a5 solid 1px; text-align:center; padding:2px }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? show_sinryo_top_header($session,$fname,"KANRI_ACCESS_LOG"); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">


<?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>

</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->






<!-- right -->
<td valign="top">
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="80" align="center" bgcolor="#bdd1e7"><a href="./summary_access_log_daily.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日別一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="./summary_access_log_find.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>検索</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="./summary_access_log_delete.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">削除</font></a></td>
<td></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>


<img src="img/spacer.gif" alt="" width="1" height="3"><br>

<?//==========================================================================?>
<?// 検索条件                                                                 ?>
<?//==========================================================================?>
<?
	$sel = select_from_table($con, "select min(op_ymd) from oplog_daily","",$fname);
	$min_op_ymd = @pg_fetch_result($sel, 0, 0);
	if (strlen($min_op_ymd)!=8) $min_op_ymd = date("Y");
	$min_op_y = (int)substr($min_op_ymd, 0, 4);
	$max_op_y = date("Y");

	if (@$_REQUEST["mode"]!="search"){
		$date_y1 = date("Y");
		$date_m1 = date("m");
		$date_d1 = date("d");
		$date_y2 = $date_y1;
		$date_m2 = $date_m1;
		$date_d2 = $date_d1;
	}
	$date_ymd1 = ($date_y1*10000)+($date_m1*100)+$date_d1;
	$date_ymd2 = ($date_y2*10000)+($date_m2*100)+$date_d2;

	$page = max(1 ,(int)@$_REQUEST["summary_current_page"]);
	if (@$_REQUEST["rows_per_page"] != @$_REQUEST["rows_per_page_old"]) $page = 1;
  $rows_per_page = max(15, (int)@$_REQUEST["rows_per_page"]);


?>
<div><?=$font?>
<script type="text/javascript">
  function summary_page_change(page){
    document.getElementById("summary_current_page").value = page;
    document.frm.submit();
    return false;
  }
</script>
<form action="summary_access_log_find.php" method="get" name="frm">
	<input type="hidden" name="session" value="<?=$session ?>">
	<input type="hidden" name="mode" value="search">
  <input type="hidden" name="summary_current_page" id="summary_current_page" value="<?=$page?>">
	<div style="padding:4px">
	<select name="date_y1">
		<? for ($i=$min_op_y; $i <= $max_op_y; $i++){ ?><option value="<?=$i?>" <?= $i==$date_y1?"selected":"" ?>><?=$i?></option><?}?>
	</select>&nbsp;年&nbsp;
	<select name="date_m1">
		<? for ($i=1; $i <= 12; $i++){ ?><option value="<?=$i?>" <?= $i==$date_m1?"selected":"" ?>><?=$i?></option><?}?>
	</select>&nbsp;月&nbsp;
	<select name="date_d1">
		<? for ($i=1; $i <= 31; $i++){ ?><option value="<?=$i?>" <?= $i==$date_d1?"selected":"" ?>><?=$i?></option><?}?>
	</select>&nbsp;日
	&nbsp;〜&nbsp;
	<select name="date_y2">
		<? for ($i=$min_op_y; $i <= $max_op_y; $i++){ ?><option value="<?=$i?>" <?= $i==$date_y2?"selected":"" ?>><?=$i?></option><?}?>
	</select>&nbsp;年&nbsp;
	<select name="date_m2">
		<? for ($i=1; $i <= 12; $i++){ ?><option value="<?=$i?>" <?= $i==$date_m2?"selected":"" ?>><?=$i?></option><?}?>
	</select>&nbsp;月&nbsp;
	<select name="date_d2">
		<? for ($i=1; $i <= 31; $i++){ ?><option value="<?=$i?>" <?= $i==$date_d2?"selected":"" ?>><?=$i?></option><?}?>
	</select>&nbsp;日
	</div>
	<div style="padding:4px">
		<span style="padding-right:3px">職員氏名</span>
		<input type="text" name="sel_emp_name" style="width:150px" value="<?=@$_REQUEST["sel_emp_name"]?>" />
		<span style="padding-right:3px; padding-left:10px">処理</span>
		<select name="sel_optype_exec_flg">
			<option value=""></option>
			<option value="0" <?= @$_REQUEST["sel_optype_exec_flg"]=="0"? "selected":""?>>参照</option>
			<option value="1" <?= @$_REQUEST["sel_optype_exec_flg"]=="1"? "selected":""?>>更新</option>
		</select>
		<span style="padding-right:3px; padding-left:10px">患者氏名</span>
		<input type="text" name="sel_ptif_name" style="width:150px" value="<?=@$_REQUEST["sel_ptif_name"]?>"/>
		<span style="padding-left:10px">
			<input type="text" name="rows_per_page" value="<?=$rows_per_page?>" style="width:40px" />行ずつ
			<input type="hidden" name="rows_per_page_old" value="<?=$rows_per_page?>" />
			<input type="submit" value="検索"/>
		</span>
	</div>
</form>
</font></div>


<?//==========================================================================?>
<?// 一覧                                                                     ?>
<?//==========================================================================?>
<?
  $offset = ($page - 1) * $rows_per_page;

	$exec_flg_list = array(0=>"参照",1=>"更新");
	$sql = array();
	$sql[] = "select op.op_time, op.op_emp_id, op.op_emp_nm, op.optype_id, opm.optype_proc, op.op_exec_flg, op.op_pt_id";
	$sql[] = "from oplog op";
	$sql[] = "inner join optype opm on (opm.optype_id = op.optype_id)";
	if (trim(@$_REQUEST["sel_ptif_name"])!= ""){
		$sql[] = "left join ptifmst pt on (pt.ptif_id = op.op_pt_id)";
	}
	$sql[] = "where op.op_time >= '".$date_ymd1."000000'";
	$sql[] = "and op.op_time <= '".$date_ymd2."999999'";

	if (trim(@$_REQUEST["sel_emp_name"])!= ""){
		$sql[] = "and (op.op_emp_nm like '%".pg_escape_string(trim($_REQUEST["sel_emp_name"]))."%'";
		$sql[] = "or op.op_emp_id = '".pg_escape_string(trim($_REQUEST["sel_emp_name"]))."')";
	}
	if (trim(@$_REQUEST["sel_optype_exec_flg"])!= ""){
		$sql[] = "and op.op_exec_flg = '" . pg_escape_string(trim($_REQUEST["sel_optype_exec_flg"]))."'";
	}
	if (trim(@$_REQUEST["sel_ptif_name"])!= ""){
		$ptif_name = explode(" ", trim($_REQUEST["sel_ptif_name"]));
		foreach($ptif_name as $part){
			$part = pg_escape_string($part);
		$sql[] = "and (";
		$sql[] = "pt.ptif_lt_kaj_nm like '%".$part."%'";
		$sql[] = "or pt.ptif_ft_kaj_nm like '%".$part."%'";
		$sql[] = "or op.op_pt_id = '".$part."'";
		$sql[] = ")";
		}
	}
	// 全レコード数の取得、ページの割り出し
	$sel = select_from_table($con, "select count(*) from (".implode(" ",$sql).") d", "", $fname);
	$total_record = pg_fetch_result($sel, 0, 0);
  $total_page_count = floor( ($total_record-1) / $rows_per_page ) + 1;

	$sql[] = "order by op.op_time";
	$sql[] = "offset ".$offset." limit ".$rows_per_page;
	$sel = select_from_table($con, implode(" ",$sql), "", $fname);
?>



<?//==========================================================================?>
<?//ページャ(上)                                                              ?>
<?//==========================================================================?>
<table width="100%"><tr><td>
  <table><tr>
    <td style="white-space:nowrap"><?=$font?>全<?=$total_record?>行</font></td>
		<? if ($total_page_count > 1){ ?>
    <td style="white-space:nowrap"><?=$font?>
		<span style="color:silver"><? if($page>1){?><a href="" onclick="return summary_page_change(1);"><? } ?>先頭へ</a></span>
    </font></td>
    <td style="white-space:nowrap"><?=$font?>
		<span style="color:silver"><? if($page>1) { ?><a href="" onclick="return summary_page_change(<?=$page-1?>);"><? } ?>前へ</a></span>
    </font></td>
    <td><?=$font?>
			<? for($i=1;$i<=$total_page_count;$i++) { ?>
				<? if($i != $page) { ?><a href="" onclick="return summary_page_change(<?=$i?>);">[<?=$i?>]</a><? }else{ ?>[<?=$i?>]<? } ?>
			<? } ?>
    </font></td>
    <td style="white-space:nowrap"><?=$font?>
		<span style="color:silver"><? if($page<$total_page_count){ ?><a href="" onclick="return summary_page_change(<?=$page+1?>);"><? } ?>次へ</a></span>
    </font></td>
    <? } ?>
  </tr></table>
</td></tr></table>


<? if ($total_record > 0) { ?>
<table border="0" cellspacing="0" cellpadding="2" class="list">
	<tr style="background-color:#f6f9ff">
		<td><?=$font?>アクセス日</font></td>
		<td><?=$font?>職員ID</font></td>
		<td><?=$font?>職員氏名</font></td>
		<td><?=$font?>処理名</font></td>
		<td><?=$font?>患者ID</font></td>
		<td><?=$font?>参照／更新</font></td>
	</tr>
<? while($row = pg_fetch_array($sel)){ ?>
<?   $d = $row["op_time"]; ?>
<?   $ymdhms = substr($d,0,4)."/".substr($d,4,2)."/".substr($d,6,2)." ".substr($d,8,2).":".substr($d,10,2).":".substr($d,12,2); ?>
	<tr>
		<td><?=$font?><?=$ymdhms?></font></td>
		<td><?=$font?><?=$row["op_emp_id"]?></font></td>
		<td><?=$font?><?=$row["op_emp_nm"]?></font></td>
		<td><?=$font?><?=$row["optype_proc"]?></font></td>
		<td><?=$font?><?=$row["op_pt_id"]?></font></td>
		<td><?=$font?><?=$exec_flg_list[(int)$row["op_exec_flg"]]?></font></td>
	</tr>
<? } ?>
</table>
<? } ?>


<?//==========================================================================?>
<?//ページャ(下)                                                              ?>
<?//==========================================================================?>
<? if ($total_page_count > 1){ ?>
<table width="100%"><tr><td>
  <table><tr>
    <td style="white-space:nowrap"><?=$font?>全<?=$total_record?>行</font></td>
    <td style="white-space:nowrap"><?=$font?>
		<span style="color:silver"><? if($page>1){?><a href="" onclick="return summary_page_change(1);"><? } ?>先頭へ</a></span>
    </font></td>
    <td style="white-space:nowrap"><?=$font?>
		<span style="color:silver"><? if($page>1) { ?><a href="" onclick="return summary_page_change(<?=$page-1?>);"><? } ?>前へ</a></span>
    </font></td>
    <td><?=$font?>
			<? for($i=1;$i<=$total_page_count;$i++) { ?>
				<? if($i != $page) { ?><a href="" onclick="return summary_page_change(<?=$i?>);">[<?=$i?>]</a><? }else{ ?>[<?=$i?>]<? } ?>
			<? } ?>
    </font></td>
    <td style="white-space:nowrap"><?=$font?>
		<span style="color:silver"><? if($page<$total_page_count){ ?><a href="" onclick="return summary_page_change(<?=$page+1?>);"><? } ?>次へ</a></span>
    </font></td>
  </tr></table>
</td></tr></table>
<? } ?>




</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
