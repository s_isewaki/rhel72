<?php
require_once('about_comedix.php');
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");
require_once("hiyari_report_class.php");
require_once('class/Cmx/Model/SystemConfig.php');

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==================================================
//オプション
//==================================================
$sysConf = new Cmx_SystemConfig();
//デザイン（1=旧デザイン、2=新デザイン）
$design_mode = $sysConf->get('fantol.design.mode');

//==============================
//パラメータ精査
//==============================

//日付検索(年)初期値は今年
if($search_date_year == "") {
    $search_date_year = date("Y");
}
//日付検索(月)初期値は今月
if($search_date_month == "") {
    $search_date_month = date("m");
}

//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);

//==============================
//匿名設定取得
//==============================
$anonymous_mail_use_flg = get_anonymous_mail_use_flg($con, $fname);

//==============================
//処理モードごとの処理
//==============================
if($is_postback == "true")
{
    switch ($mode)
    {
        case "delete":

            //==============================
            //下書き削除
            //==============================
            if(!empty($list_select))
            {
                $select_count=count($list_select);

                //全て自作レポートであるかチェック
                $is_all_my_report = true;
                for($i=0;$i<$select_count;$i++)
                {
                    if(!$list_select[$i]=="")
                    {
                        //レポートID
                        $report_id = $list_select[$i];

                        //自作レポートであるかチェック
                        $sql = "select * from inci_report";
                        $cond =  "where report_id = $report_id and registrant_id = '$emp_id'";
                        $sel = select_from_table($con,$sql,$cond,$fname);
                        if($sel == 0){
                            pg_close($con);
                            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                            exit;
                        }
                        $num = pg_numrows($sel);

                        if($num < 1)
                        {
                            $is_all_my_report = false;
                        }
                    }
                }

                //他人のレポートが選択されていた場合
                if(! $is_all_my_report)
                {
                    ?>
                    <script type="text/javascript">
                    alert("他の人が作成したレポートは削除できません。");
                    </script>
                    <?
                }

                //自分のレポートだけ選択されていた場合
                else
                {
                    for($i=0;$i<$select_count;$i++)
                    {
                        if(!$list_select[$i]=="")
                        {
                            //レポートID
                            $report_id = $list_select[$i];

                            //論理削除
//                          delete_report($con,$fname,$report_id);
                            $rep_obj = new hiyari_report_class($con, $fname);
                            $rep_obj->del_report($report_id);
                        }
                    }
                }
            }
            break;
        default:
            break;
    }
}

//==============================
//レポート一覧取得(下書き)
//==============================
if($disp_mode == "") {$disp_mode = "self";}
$where = "where not del_flag = 't' and shitagaki_flg = 't'";

// 本人分
if($disp_mode == "self")
{
    $where .= " and registrant_id = '$emp_id' ";
}

//日付による絞込み
if($search_date_mode == "month")
{
    $search_date_1 = $search_date_year."/".$search_date_month;
    $where .= " and registration_date like '$search_date_1%' ";
}
elseif($search_date_mode == "ymd_st_ed")
{
    if($search_date_ymd_st != "")
    {
        $where .= " and registration_date >= '$search_date_ymd_st' ";
    }
    if($search_date_ymd_ed != "")
    {
        $where .= " and registration_date <= '$search_date_ymd_ed' ";
    }
}
elseif($search_date_mode == "non")
{
    //絞り込み条件なし。
}

//検索条件による絞込み
$serch_emp_target = "report";
if($search_emp_class != "")
{
    $where .= " and registrant_class = $search_emp_class ";
}
if($search_emp_attribute != "")
{
    $where .= " and registrant_attribute = $search_emp_attribute ";
}
if($search_emp_dept != "")
{
    $where .= " and registrant_dept = $search_emp_dept ";
}
if($search_emp_room != "")
{
    $where .= " and registrant_room = $search_emp_room ";
}
if($search_emp_job != "")
{
    $where .= " and ".$serch_emp_target."_emp_job = $search_emp_job ";
}
if($search_emp_name != "")
{
    $search_emp_name2 = str_replace(" ", "", pg_escape_string($search_emp_name));
    $search_emp_name2 = str_replace("　", "", $search_emp_name2);
    $where .= " and ";
    $where .= " ( ";
    $where .= "    ".$serch_emp_target."_emp_lt_nm like '%$search_emp_name2%' ";
    $where .= " or ".$serch_emp_target."_emp_ft_nm like '%$search_emp_name2%' ";
    $where .= " or ".$serch_emp_target."_emp_kn_lt_nm like '%$search_emp_name2%' ";
    $where .= " or ".$serch_emp_target."_emp_kn_ft_nm like '%$search_emp_name2%' ";
    $where .= " or (".$serch_emp_target."_emp_lt_nm || ".$serch_emp_target."_emp_ft_nm) like '%$search_emp_name2%' ";
    $where .= " or (".$serch_emp_target."_emp_kn_lt_nm || ".$serch_emp_target."_emp_kn_ft_nm) like '%$search_emp_name2%' ";
    $where .= " ) ";
}

if($search_torey_date_in_search_area_start != "")
{
    $where .= " and registration_date >= '$search_torey_date_in_search_area_start' ";
}
if($search_torey_date_in_search_area_end != "")
{
    $where .= " and registration_date <= '$search_torey_date_in_search_area_end' ";
}

if($sort_div == "0")
{
    $sort = "asc";
}
else
{
    $sort = "desc";
}

$order = "order by report_id desc";
switch($sort_item)
{
    case "REPORT_TITLE":
        $order = "order by report_title $sort, report_id desc";
        break;
    case "REGIST_NAME":
        $order = "order by report_emp_kn_lt_nm || report_emp_kn_ft_nm $sort, report_id desc";
        break;
    case "REGIST_DATE":
        $order = "order by registration_date $sort, report_id desc";
        break;
    default:
        //何もしない。
        break;
}


$fields = array(
            'report_id',
            'report_title',
//          'job_id',
            'registrant_id',
            'registrant_name',
            'registration_date',
//          'eis_id',
//          'eid_id',
            'del_flag',
//          'report_no',
            'shitagaki_flg',
//          'report_comment',
//          'edit_lock_flg',
//          'anonymous_report_flg',
//          'registrant_class',
//          'registrant_attribute',
//          'registrant_dept',
//          'registrant_room',

//          'report_emp_class',
//          'report_emp_attribute',
//          'report_emp_dept',
//          'report_emp_room',
//          'report_emp_job',
//          'report_emp_lt_nm',
//          'report_emp_ft_nm',
            'report_emp_kn_lt_nm',
            'report_emp_kn_ft_nm',

//          '[auth]_update_emp_id',
//          '[auth]_start_flg',
//          '[auth]_end_flg',
//          '[auth]_start_date',
//          '[auth]_end_date',
//          '[auth]_use_flg',

//          'mail_id',
//          'subject',
//          'send_emp_id',
//          'send_emp_name',
//          'send_date',
//          'send_del_flg',
//          'send_job_id',
//          'send_eis_id',
//          'anonymous_send_flg',
//          'marker',
//
//          'send_emp_class',
//          'send_emp_attribute',
//          'send_emp_dept',
//          'send_emp_room',
//          'send_emp_job',
//          'send_emp_lt_nm',
//          'send_emp_ft_nm',
//          'send_emp_kn_lt_nm',
//          'send_emp_kn_ft_nm',

//          'recv_emp_id',
//          'recv_emp_name',
//          'recv_class',
//          'recv_class_disp_index',
//          'recv_read_flg',
//          'recv_read_flg_update_date',
//          'recv_del_flg',
//          'anonymous_recv_flg',
//
//          'recv_emp_class',
//          'recv_emp_attribute',
//          'recv_emp_dept',
//          'recv_emp_room',
//          'recv_emp_job',
//          'recv_emp_lt_nm',
//          'recv_emp_ft_nm',
//          'recv_emp_kn_lt_nm',
//          'recv_emp_kn_ft_nm',

//          'report_link_id',
//          'report_link_status',
//          'main_report_id',
    );
if($search_emp_class != "" || $search_emp_attribute != "" || $search_emp_dept != "" || $search_emp_room != "" || $search_emp_job != "" || $search_emp_name != "")
{
    $fields[] = 'registrant_class';
    $fields[] = 'registrant_attribute';
    $fields[] = 'registrant_dept';
    $fields[] = 'registrant_room';
    $fields[] = 'report_emp_job';
    $fields[] = 'report_emp_lt_nm';
    $fields[] = 'report_emp_ft_nm';
    $fields[] = 'report_emp_kn_lt_nm';
    $fields[] = 'report_emp_kn_ft_nm';
}
$list_data_array = get_report_db_list($con,$fname,$where,$order,$fields);



//==============================
//ページングに関する情報
//==============================

//pate省略時は先頭ページを対象とする。
if($page == "")
{
    $page = 1;
}

//一画面内の最大表示件数
$disp_count_max_in_page = 20;

//表示開始/終了インデックス
$disp_start_index = ($page - 1)*$disp_count_max_in_page;
$disp_end_index   = $disp_start_index + $disp_count_max_in_page - 1;

//全データ件数
$rec_count = count($list_data_array);
if($rec_count == 1 && $list_data_array[0] == "")
{
    $rec_count = 0;
}

//最大ページ数
if($rec_count == 0)
{
    $page_max  = 1;
}
else
{
    $page_max  = floor( ($rec_count-1) / $disp_count_max_in_page ) + 1;
}

//==============================
//ページングによる絞込み
//==============================

$list_data_array_work = array();
for($i=$disp_start_index;$i<=$disp_end_index;$i++)
{
    if($i >= $rec_count)
    {
        break;
    }
    $list_data_array_work[] = $list_data_array[$i];
}
$list_data_array = $list_data_array_work;


//==============================
//表示
//==============================
$page_title = "下書きファイル";

//------------------------------
//実行アイコン
//------------------------------
$arr_btn_flg = get_inci_btn_show_flg($con, $fname, $session);
$icon_disp = array();
$icon_disp['create'] = $arr_btn_flg.create_btn_show_flg;
$icon_disp['return'] = FALSE;
$icon_disp['forward'] = FALSE;
$icon_disp['delete1'] = TRUE;
$icon_disp['edit'] = TRUE;
$icon_disp['read_flg'] = FALSE;
$icon_disp['progress'] = FALSE;
$icon_disp['analysis'] = FALSE;
$icon_disp['analysis_register'] = FALSE;
$icon_disp['excel'] = FALSE;
$icon_disp['delete2'] = FALSE;
$icon_disp['undelete'] = FALSE;
$icon_disp['kill'] = FALSE;
$icon_disp['search'] = $disp_mode == "all";

//他人の下書き参照権限
$is_other_shitagaki_readable = is_other_shitagaki_readable($session,$fname,$con);

//------------------------------
//一覧データ
//------------------------------
//匿名表示
if ($design_mode == 1){
    $anonymous_name = h(get_anonymous_name($con,$fname));
}
else{
    $anonymous_name = '匿名';
}
$anonymous_setting = get_anonymous_setting($session, $con, $fname);
$mouseover_popup_base_flg = false;
// セーフティマネージャで一部匿名の場合 → 実名が見える
if(is_inci_auth_emp_of_auth($session,$fname,$con,"SM") and !empty($anonymous_setting)) {
    $mouseover_popup_base_flg = true;
}

$list = array();
foreach($list_data_array as $i => $list_data)
{
    $report_id =  $list_data["report_id"];
    $list[$i]['report_id'] =  $report_id;
    $list[$i]['list_id'] = $report_id;

    //表題
    $list[$i]['title'] = h($list_data["report_title"]);

    //報告者
    $real_name = h($list_data["registrant_name"]);
    $list[$i]['real_name'] = $real_name;
    $list[$i]['anonymous_flg'] = $anonymous_mail_use_flg;
    
    if ($disp_mode == 'all') {
        $mouseover_popup_flg = false;
        if($mouseover_popup_base_flg) {
            $mouseover_popup_flg = true;
        }
        else {
            foreach ($anonymous_setting as $auth => $data) {
                if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 1) {
                    if ($list_data["registrant_class"] == $emp["emp_class"]) {
                        $mouseover_popup_flg = true;
                    }
                }
                else if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 2) {
                    if ($list_data["registrant_class"] == $emp["emp_class"] and $list_data["registrant_attribute"] == $emp["emp_attribute"]) {
                        $mouseover_popup_flg = true;
                    }
                }
                else if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 3) {
                    if ($list_data["registrant_class"] == $emp["emp_class"] and $list_data["registrant_attribute"] == $emp["emp_attribute"] and $list_data["registrant_dept"] == $emp["emp_dept"]) {
                        $mouseover_popup_flg = true;
                    }
                }
                if ($data["report_db_read_auth_flg"] == 't') {
                    if ($list_data["registrant_room"]) {
                        if ($data["rm"][ $list_data["registrant_class"] ][ $list_data["registrant_attribute"] ][ $list_data["registrant_dept"] ][ $list_data["registrant_room"] ]) {
                            $mouseover_popup_flg = true;
                        }
                    }
                    else {
                        if ($data["rm"][ $list_data["registrant_class"] ][ $list_data["registrant_attribute"] ][ $list_data["registrant_dept"] ]) {
                            $mouseover_popup_flg = true;
                        }
                    }
                }
            }
        }
        $list[$i]['mouseover_popup_flg'] = $mouseover_popup_flg;
        if ($anonymous_mail_use_flg){
            $list[$i]['disp_emp_name'] = $anonymous_name;
        }
        else{
            $list[$i]['disp_emp_name'] = $real_name;
        }
    }
    else {
        $list[$i]['mouseover_popup_flg'] = false;
        $list[$i]['disp_emp_name'] = $real_name;
    }
    
    //作成日
    $list[$i]['create_date'] = $list_data["registration_date"];
}

include("hiyari_rpt_display.php");

//==============================
//DBコネクション終了
//==============================
pg_close($con);
