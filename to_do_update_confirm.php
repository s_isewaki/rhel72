<?
//ファイルの読み込み
require("about_session.php");
require("about_authority.php");
require("./conf/sql.inf");
require_once("Cmx.php");
require_once("aclg_set.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病棟情報権限チェック
$work = check_authority($session,30,$fname);
if($work == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//**********DB処理**********
//----------ＤＢのコネクション作成----------
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);



$s_date = "$date1$date2$date3";
$e_date = "$e_date1$e_date2$e_date3";
//----------正規表現チェック----------
	if($work_name == ""){
		echo("<script language=\"javascript\">alert(\"作業名が入力されていません。\");</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}

	if(strlen($work_name) > 40){
		echo("<script language=\"javascript\">alert(\"作業名が長すぎます。\");</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}

	if (!checkdate($date2, $date3, $date1)) {
		echo("<script language=\"javascript\">alert(\"開始日が無効です。\");</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}

	if (!checkdate($e_date2, $e_date3, $e_date1)) {
		echo("<script language=\"javascript\">alert(\"終了予定日が無効です。\");</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}

/*
	if(strlen($detail) > 200){
		echo("<script language=\"javascript\">alert(\"作業内容が長すぎます。\");</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
*/

	if($s_date > $e_date){
		echo("<script language=\"javascript\">alert(\"終了予定日は開始日よりも前に設定はできません\");</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
//----------Transaction begin----------
pg_exec($con,"begin transaction");

//----------workの更新----------
if($public_flag == 1){
	$public_flag = "t";
}else{
	$public_flag = "f";
}

$cond = "where work_id = '$work_id'";
$set  = array("work_name","work_status","work_public_flag","work_start_date","work_expire_date","work_priority","work_detail","work_update");
$setvalue = array($work_name,$status,$public_flag,$s_date,$e_date,$priority,$detail,date("Ymd"));
$up_work = update_set_table($con,$SQL150,$set,$setvalue,$cond,$fname);

if($up_work==0){
	pg_exec($con,"rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
pg_exec($con, "commit");
pg_close($con);

echo("<script language='javascript'>location.href = 'work_menu.php?session=$session&date=$date&mode=$mode';</script>");
?>
