<?

require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_jnl_application_category_list.ini");
require_once("jnl_application_template.ini");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("get_values.ini");
require_once("jnl_application_draft_template.ini");
require_once("jnl_application_workflow_common_class.php");
require_once("jnl_application_common.ini");
require_once("get_values_for_template.ini");
require_once("library_common.php");
require_once("smarty_setting.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,80,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, 81, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new jnl_application_workflow_common_class($con, $fname);


?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 日報・月報 | 申請</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<?

///-----------------------------------------------------------------------------
// ログインユーザの職員ID・氏名を取得
///-----------------------------------------------------------------------------
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) 
{
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");


if($wkfw_id != "")
{

	if($apply_id == "")
	{
		
		//日報・月報の新規入力（下書き保存したものではない）
			
			
		//病院・施設が選択されていなければ処理を実行しない
		if($select_display_facility != "")
		{

			//wkfw_idから管理CDを取得し、報告書の種別を判別し、同日、同種の報告書があるかどうかチェックする
			//→存在していたら申請一覧へ遷移する
			/*
			(管理CD)0001:病院日報
			(管理CD)0002:病院月報１
			(管理CD)0003:病院月報２
			(管理CD)0004:老人保健施設日報
			(管理CD)0005:老人保健施設月報
			(管理CD)0006:返戻月報
			*/
			$chk_redundant = fnc_chk_redundant($con,$wkfw_id,$select_display_facility,$regist_yr,$regist_mon,$regist_day);
			
			if($chk_redundant == 0)
			{
				//同日、同種の報告書があるので申請一覧画面へリダイレクト
				redirectPage("jnl_application_apply_list.php?session=$session&apply_date_default=on","指定された日付・施設の報告書は申請済みです。「申請一覧」画面で参照してください。");
			}
		
			//表題に「XXXX病院日報（XXXX年XX月XX日付け）」を入力する
			$apply_title = func_make_title($con,$wkfw_id,$select_display_facility,$regist_yr,$regist_mon,$regist_day);
			//$apply_title = "病院日報（".$regist_yr."年".$regist_mon."月".$regist_day."日）付け";
			
		
			// ワークフロー情報取得
			$arr_wkfwmst       = $obj->get_wkfwmst($wkfw_id);
			$wkfw_content      = $arr_wkfwmst[0]["wkfw_content"];
			$wkfw_content_type = $arr_wkfwmst[0]["wkfw_content_type"];
			$approve_label     = $arr_wkfwmst[0]["approve_label"];

			// 本文形式タイプのデフォルトを「テキスト」とする
			if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

			// 本文形式タイプのデフォルトを「テンプレート」の場合
			if($wkfw_content_type == "2")
			{
				$wkfw_history_no = $obj->get_max_wkfw_history_no($wkfw_id);
			}

			$num = 0;
			$pos = 0;
			while (1) {
				$pos = strpos($wkfw_content, 'show_cal', $pos);
				if ($pos === false) {
					break;
				} else {
					$num++;
				}
				$pos++;
			}

			if ($num > 0) {
				// 外部ファイルを読み込む
				write_yui_calendar_use_file_read_0_12_2();
			}
			// カレンダー作成、関数出力
			write_yui_calendar_script2($num);

			if ($mode == "apply_printwin") 
			{

			//---------テンプレートの場合XML形式のテキスト$contentを作成---------

				if ($wkfw_content_type == "2") {
					// XML変換用コードの保存
					$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
					$savexmlfilename = "jnl_workflow/tmp/{$session}_x.php";
					$fp = fopen($savexmlfilename, "w");

					if (!$fp) {
						echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
						$print_failed = true;
					}
					if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
						fclose($fp);
						echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
						$print_failed = true;
					}
					fclose($fp);

					include( $savexmlfilename );

				}
				$savexmlfilename = "jnl_workflow/tmp/{$session}_d.php";
				$fp = fopen($savexmlfilename, "w");

				if (!$fp) {
					echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
					$print_failed = true;
				}

				if ($content == "") {
					$content = " ";
				}
				if(!fwrite($fp, $content, 2000000)) {
					fclose($fp);
					echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
					$print_failed = true;
				}

				fclose($fp);
			}
		}
		else
		{
			////////////////////////////////////////////////////////////////////////////////////////
			//初期表示時（病院・施設が選択されていない場合）
			////////////////////////////////////////////////////////////////////////////////////////
			

			$sql = "select short_wkfw_name from jnl_wkfwmst";
			$cond = "where wkfw_id = '$wkfw_id'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) 
			{
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			
			/*
			(管理CD)0001:病院日報
			(管理CD)0002:病院月報１
			(管理CD)0003:病院月報２
			(管理CD)0004:老人保健施設日報
			(管理CD)0005:老人保健施設日報
			*/
			
			$kind_report = pg_fetch_result($sel, 0, "short_wkfw_name");
	

			//現在日付取得
			$systemDate = getDate();
			$sysYear = $systemDate[year];
			$sysMon = $systemDate[mon];
			$sysDay = $systemDate[mday];

			if($kind_report == "0001" || $kind_report == "0004")
			{	
				//病院日報、老健日報の場合(一日前の年月日を初期表示)

				$regist_yr = date("Y",mktime(0,0,0,$sysMon,$sysDay-1,$sysYear));
				$regist_mon = date("m",mktime(0,0,0,$sysMon,$sysDay-1,$sysYear));
				$regist_day = date("d",mktime(0,0,0,$sysMon,$sysDay-1,$sysYear));
				
			}
			else
			{
				//病院月報、老健月報、返戻月報の場合(一ヶ月前の年月日を初期表示)

				$regist_yr = date("Y",mktime(0,0,0,$sysMon-1,$sysDay,$sysYear));
				$regist_mon = date("m",mktime(0,0,0,$sysMon-1,$sysDay,$sysYear));
				$regist_day = date("d",mktime(0,0,0,$sysMon-1,$sysDay,$sysYear));
				
			}
			
		
		//ユーザが入力可能な病院・老健施設を取得する
			$arr_facility_list = array();
			$arr_facility_list=func_get_facility_list($con,$emp_id,$wkfw_type);
		}
	
	}
	else
	{
		$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
		$wkfw_content      = $arr_apply_wkfwmst[0]["wkfw_content"];
		$wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
		$apply_content     = $arr_apply_wkfwmst[0]["apply_content"];
		$wkfw_history_no   = $arr_apply_wkfwmst[0]["wkfw_history_no"];
		$approve_label     = $arr_apply_wkfwmst[0]["approve_label"];

		// 本文形式タイプのデフォルトを「テキスト」とする
		if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

		// 形式をテキストからテンプレートに変更した場合の古いデータ対応
		// wkfw_content_typeは2:テンプレートだが、登録データがXMLでない場合、1:テキストとして処理する
		if ($wkfw_content_type == "2") {
			if (strpos($apply_content, "<?xml") === false) {
				$wkfw_content_type = "1";
			}
		}

		if($wkfw_history_no != "")
		{
			$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
			$wkfw_content = $arr_wkfw_template_history["wkfw_content"];
		}

		$num = 0;
		if ($wkfw_content_type == "2") {
			$pos = 0;
			while (1) {
				$pos = strpos($wkfw_content, 'show_cal', $pos);
				if ($pos === false) {
					break;
				} else {
					$num++;
				}
				$pos++;
			}
		}

		if ($num > 0) {
			// 外部ファイルを読み込む
			write_yui_calendar_use_file_read_0_12_2();
		}
		// カレンダー作成、関数出力
		write_yui_calendar_script2($num);

		if ($mode == "apply_printwin") {

		//---------テンプレートの場合XML形式のテキスト$contentを作成---------

			if ($wkfw_content_type == "2") {
				// XML変換用コードの保存
				$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
				$savexmlfilename = "jnl_workflow/tmp/{$session}_x.php";
				$fp = fopen($savexmlfilename, "w");

				if (!$fp) {
					echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
					$print_failed = true;
				}
				if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
					fclose($fp);
					echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
					$print_failed = true;
				}
				fclose($fp);

				include( $savexmlfilename );

			}
			$savexmlfilename = "jnl_workflow/tmp/{$session}_d.php";
			$fp = fopen($savexmlfilename, "w");

			if (!$fp) {
				echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
				$print_failed = true;
			}

			if ($content == "") {
				$content = " ";
			}
			if(!fwrite($fp, $content, 2000000)) {
				fclose($fp);
				echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
				$print_failed = true;
			}
			fclose($fp);

		}
	}
}

$approve_label = ($approve_label != "2") ? "承認" : "確認";




//ユーザが入力可能な病院施設を取得する
function func_get_facility_list($con,$emp_id,$wkfw_type)
{

	//返却する配列
	$arr_facility_list = array();

	//ユーザが入力可能な病院施設を取得する
	$sql = "select a.jnl_facility_id, a.jnl_facility_name ";
	$sql .= "from jnl_facility_master a,jnl_facility_register b ";
	$cond = "where a.jnl_facility_id = b.jnl_facility_id and jnl_facility_type = '$wkfw_type' and b.emp_id = '$emp_id' order by a.jnl_facility_id";
	
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$count = pg_numrows($sel);
	
	
	if($count > 0)
	{
		for ($i=0; $i<$count; $i++) 
		{
			$arr_facility_list[$i]["id"] =  pg_result($sel,$i,"jnl_facility_id");
			$arr_facility_list[$i]["name"] =  pg_result($sel,$i,"jnl_facility_name");
		}
	}
	
	return $arr_facility_list;
	
}


/**
 * show_options 日報・月報の入力対象職員をセレクトリストのリスト欄に出力する項目を表示する
 *
 * @param mixed $arr_input_member_list 入力対象職員リスト
 * @return なし
 *
 */
function show_options($arr_input_member_list) 
{
	
	for ($i=0; $i<count($arr_input_member_list); $i++) 
	{
		
		$id = $arr_input_member_list[$i]["id"];
		$nm = $arr_input_member_list[$i]["name"];
		echo("<option value=\"$id\"");
		echo(">$nm</option>\n");
	}
	
}




/**
 * fnc_chk_redundant 報告書の重複をチェックする
 *
 * @param mixed $con 接続情報
 * @param mixed $wkfw_id ワークフローID
 * @param mixed $select_display_facility 病院・施設ID
 * @param mixed $regist_yr 指定年
 * @param mixed $regist_mon 指定月
 * @param mixed $regist_day 指定日
 * @return 報告書種別情報
 *
 */
function fnc_chk_redundant($con,$wkfw_id,$select_display_facility,$regist_yr,$regist_mon,$regist_day) 
{
	
	/*
	(管理CD)0001:病院日報
	(管理CD)0002:病院月報１
	(管理CD)0003:病院月報２
	(管理CD)0004:老人保健施設日報
	(管理CD)0005:老人保健施設月報
	(管理CD)0006:返戻月報
	(管理CD)0007:診療報酬総括ー医科
	(管理CD)0008:診療報酬総括ー歯科
	(管理CD)0009:入院診療行為別明細
	(管理CD)0010:外来診療行為別明細
	(管理CD)0011:療養費明細書
	(管理CD)0012:予防給付療養費明細書
	*/
	
	//リターン値
	$chk_flg = 0;

	$sql = "select short_wkfw_name from jnl_wkfwmst";
	$cond = "where wkfw_id = '$wkfw_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) 
	{
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$kind_report = pg_fetch_result($sel, 0, "short_wkfw_name");
	
	
	if($kind_report == "0001")
	{
		//病院日報
		
		
		$sel_date = $regist_yr.$regist_mon.$regist_day;
		
		$sql = "select emp_id from jnl_hospital_day ";
		$cond = "where del_flg = 'f' and jnl_status != '4' and regist_date = '$sel_date' and jnl_facility_id = '".$select_display_facility."'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) 
		{
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$count = pg_numrows($sel);
		
		
		if($count > 0)
		{
			//指定の報告書はすでに登録されている状態
			$chk_flg = 0;
		}
		else
		{
			//指定の報告書はまだ登録されていない状態
			$chk_flg = 1;
			
		}
		
		return $chk_flg;
		
	}
	else if($kind_report == "0002")
	{
		//病院月報１


		$sel_date = $regist_yr.$regist_mon.$regist_day;
		
		$sql = "select emp_id from jnl_hospital_month_action ";
		$cond = "where del_flg = 'f' and jnl_status != '4' and regist_date = '$sel_date' and jnl_facility_id = '".$select_display_facility."'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) 
		{
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		
		$count = pg_numrows($sel);
		
		
		if($count > 0)
		{
			//指定の報告書はすでに登録されている状態
			$chk_flg = 0;
		}
		else
		{
			//指定の報告書はまだ登録されていない状態
			$chk_flg = 1;
			
		}
		
		return $chk_flg;
		
	}
	else if($kind_report == "0003")
	{
		//病院月報２
		
		$sel_date = $regist_yr.$regist_mon.$regist_day;
		
		$sql = "select emp_id from jnl_hospital_month_patient ";
		$cond = "where del_flg = 'f' and jnl_status != '4' and regist_date = '$sel_date' and jnl_facility_id = '".$select_display_facility."'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) 
		{
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		
		$count = pg_numrows($sel);
		
		
		if($count > 0)
		{
			//指定の報告書はすでに登録されている状態
			$chk_flg = 0;
		}
		else
		{
			//指定の報告書はまだ登録されていない状態
			$chk_flg = 1;
			
		}
		
		return $chk_flg;
		
		
	}
	else if(($kind_report == "0004") || ($kind_report == "0005"))
	{
		//老人保健施設日報,月報
		
		$sel_date = $regist_yr.$regist_mon.$regist_day;
		
		$sql = "select emp_id from jnl_nurse_home_main ";
		$cond = "where del_flg = 'f' and jnl_status != '4' and regist_date = '$sel_date' and jnl_facility_id = '".$select_display_facility."'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) 
		{
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		
		$count = pg_numrows($sel);
		
		
		if($count > 0)
		{
			//指定の報告書はすでに登録されている状態
			$chk_flg = 0;
		}
		else
		{
			//指定の報告書はまだ登録されていない状態
			$chk_flg = 1;
			
		}
		
		return $chk_flg;
		
		
	}
	else if($kind_report == "0006")
	{
		//返戻月報
		
		$sel_date = $regist_yr.$regist_mon.$regist_day;
		
		$sql = "select emp_id from jnl_return_main ";
		$cond = "where del_flg = 'f' and jnl_status != '4' and regist_date = '$sel_date' and jnl_facility_id = '".$select_display_facility."'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) 
		{
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		
		$count = pg_numrows($sel);
		
		
		if($count > 0)
		{
			//指定の報告書はすでに登録されている状態
			$chk_flg = 0;
		}
		else
		{
			//指定の報告書はまだ登録されていない状態
			$chk_flg = 1;
			
		}
		
		return $chk_flg;
	}
	else if($kind_report == "0007")
	{
		//診療報酬総括表（医科）
		
		$sel_date = $regist_yr.$regist_mon.$regist_day;
		
		$sql = "select emp_id from jnl_reward_synthesis_medi_main ";
		$cond = "where del_flg = 'f' and jnl_status != '4' and regist_date = '$sel_date' and jnl_facility_id = '".$select_display_facility."'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) 
		{
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		
		$count = pg_numrows($sel);
		
		
		if($count > 0)
		{
			//指定の報告書はすでに登録されている状態
			$chk_flg = 0;
		}
		else
		{
			//指定の報告書はまだ登録されていない状態
			$chk_flg = 1;
			
		}
		
		return $chk_flg;
	}
	else if($kind_report == "0008")
	{
		//診療報酬総括表（歯科）
		
		$sel_date = $regist_yr.$regist_mon.$regist_day;
		
		$sql = "select emp_id from jnl_reward_synthesis_dental_main ";
		$cond = "where del_flg = 'f' and jnl_status != '4' and regist_date = '$sel_date' and jnl_facility_id = '".$select_display_facility."'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) 
		{
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		
		$count = pg_numrows($sel);
		
		
		if($count > 0)
		{
			//指定の報告書はすでに登録されている状態
			$chk_flg = 0;
		}
		else
		{
			//指定の報告書はまだ登録されていない状態
			$chk_flg = 1;
			
		}
		
		return $chk_flg;
	}
	else if($kind_report == "0009")
	{
		//入院診療行為別明細
		
		$sel_date = $regist_yr.$regist_mon.$regist_day;
		
		$sql = "select emp_id from jnl_bill_diagnosis_enter_main ";
		$cond = "where del_flg = 'f' and jnl_status != '4' and regist_date = '$sel_date' and jnl_facility_id = '".$select_display_facility."'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) 
		{
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		
		$count = pg_numrows($sel);
		
		
		if($count > 0)
		{
			//指定の報告書はすでに登録されている状態
			$chk_flg = 0;
		}
		else
		{
			//指定の報告書はまだ登録されていない状態
			$chk_flg = 1;
			
		}
		
		return $chk_flg;
	}
	else if($kind_report == "0010")
	{
		//外来診療行為別明細
		
		$sel_date = $regist_yr.$regist_mon.$regist_day;
		
		$sql = "select emp_id from jnl_bill_diagnosis_visit_main ";
		$cond = "where del_flg = 'f' and jnl_status != '4' and regist_date = '$sel_date' and jnl_facility_id = '".$select_display_facility."'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) 
		{
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		
		$count = pg_numrows($sel);
		
		
		if($count > 0)
		{
			//指定の報告書はすでに登録されている状態
			$chk_flg = 0;
		}
		else
		{
			//指定の報告書はまだ登録されていない状態
			$chk_flg = 1;
			
		}
		
		return $chk_flg;
	}
	else if($kind_report == "0011")
	{
		//療養費明細書
		
		$sel_date = $regist_yr.$regist_mon.$regist_day;
		
		$sql = "select emp_id from jnl_bill_recuperation_main ";
		$cond = "where del_flg = 'f' and jnl_status != '4' and regist_date = '$sel_date' and jnl_facility_id = '".$select_display_facility."'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) 
		{
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		
		$count = pg_numrows($sel);
		
		
		if($count > 0)
		{
			//指定の報告書はすでに登録されている状態
			$chk_flg = 0;
		}
		else
		{
			//指定の報告書はまだ登録されていない状態
			$chk_flg = 1;
			
		}
		
		return $chk_flg;
	}
	else if($kind_report == "0012")
	{
		//予防給付療養費明細書
		
		$sel_date = $regist_yr.$regist_mon.$regist_day;
		
		$sql = "select emp_id from jnl_bill_recuperation_prepay_main ";
		$cond = "where del_flg = 'f' and jnl_status != '4' and regist_date = '$sel_date' and jnl_facility_id = '".$select_display_facility."'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) 
		{
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		
		$count = pg_numrows($sel);
		
		
		if($count > 0)
		{
			//指定の報告書はすでに登録されている状態
			$chk_flg = 0;
		}
		else
		{
			//指定の報告書はまだ登録されていない状態
			$chk_flg = 1;
			
		}
		
		return $chk_flg;
	}
	
	
	
	return $chk_flg;
	
}

/**
 * func_make_title 報告書の表題を作成する
 *
 * @param mixed $con 接続情報
 * @param mixed $wkfw_id ワークフローID
 * @param mixed $select_display_facility 病院・施設ID
 * @param mixed $regist_yr 指定年
 * @param mixed $regist_mon 指定月
 * @param mixed $regist_day 指定日
 * @return 表題
 *
 */
function func_make_title($con,$wkfw_id,$select_display_facility,$regist_yr,$regist_mon,$regist_day) 
{
	//リターン(表題)
	$title = "";
	
	$sql = "select a.wkfw_title , a.wkfw_type ,a.short_wkfw_name , b.jnl_facility_name from jnl_wkfwmst a , jnl_facility_master b";
	$cond = "where a.wkfw_id = '$wkfw_id' and a.wkfw_type = b.jnl_facility_type and b.jnl_facility_id ='$select_display_facility'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) 
	{
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	
	//日報月報の名前
	$name1 = pg_fetch_result($sel, 0, "wkfw_title");
	
	//病院・老健施設の名前
	$name2 = pg_fetch_result($sel, 0, "jnl_facility_name");
	
	//病院・老健施設の種別
	$kind = pg_fetch_result($sel, 0, "wkfw_type");
	
	//日報・月報の種別
	$type = pg_fetch_result($sel, 0, "short_wkfw_name");
	
	if($type == "0001" || $type == "0004")
	{
		//種別が日報なので日付まで表記する

		//表題作成(日報)
		$title = "「".$name2."」".$name1."（".$regist_yr."年".$regist_mon."月".$regist_day."日付け）";
	}
	else
	{
		//種別が月報なので月まで表記する

		//表題作成(月報)
		$title = "「".$name2."」".$name1."（".$regist_yr."年".$regist_mon."月付け）";
	}
	
	
	
	return $title;
	
}







?>
<script type="text/javascript">



function init() {


	if('<?=$back?>' == 't') {
<?

		$approve_num = $_POST[approve_num];
		for($i=1; $i<=$approve_num;$i++) {

			$regist_emp_id = "regist_emp_id$i";
			$radio_emp_id = "radio_emp_id$i";
			$approve_name = "approve_name$i";

?>
			var radio_emp_id = 'radio_emp_id' + <?=$i?>;
			if(document.getElementById('<?=$radio_emp_id?>')) {
				for(j=0; j<document.apply.elements['<?=$radio_emp_id?>'].length; j++) {
					if(document.apply.elements['<?=$radio_emp_id?>'][j].value == '<?=$_POST[$regist_emp_id];?>') {
						document.apply.elements['<?=$radio_emp_id?>'][j].checked = true;
					}
				}
				document.apply.elements['<?=$regist_emp_id?>'].value = '<?=$_POST[$regist_emp_id];?>';
			}
			if(document.getElementById('<?=$approve_name?>')) {
				document.apply.elements['<?=$approve_name?>'].value = '<?=$_POST[$approve_name];?>';
				document.apply.elements['<?=$regist_emp_id?>'].value = '<?=$_POST[$regist_emp_id];?>';
			}
<?
		}
?>
	}
}

function attachFile() {
	window.open('jnl_apply_attach.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function detachFile(e) {
	if (e == undefined) {
		e = window.event;
	}

	var btn_id;
	if (e.target) {
		btn_id = e.target.getAttribute('id');
	} else {
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_', '');

	var p = document.getElementById('p_' + id);
	document.getElementById('attach').removeChild(p);
}

// 申請処理
function apply_regist(apv_num, precond_num) {

	//申請ボタンを非アクティブにする
	document.apply.apply_button1.disabled = true;
	document.apply.apply_button2.disabled = true;

	// 未入力チェック
	for(i=1; i <= precond_num; i++)
	{
		obj = 'precond_apply_id' + i;
		apply_id = document.apply.elements[obj].value;
		if(apply_id == "") {
			alert('<? echo($approve_label); ?>確定の申請書を設定してくだい。');
			//申請ボタンをアクティブにする
			document.apply.apply_button1.disabled = false;
			document.apply.apply_button2.disabled = false;
			return;
		}
	}

	// 承認者が全く存在しない場合
    var regist_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj = 'regist_emp_id' + i;
		emp_id = document.apply.elements[obj].value;

		if(emp_id != "") {
			regist_flg = true;
            break;
		}
	}

    if(!regist_flg)
    {
        alert('<? echo($approve_label); ?>者が存在しないため、申請できません。');
		//申請ボタンをアクティブにする
		document.apply.apply_button1.disabled = false;
		document.apply.apply_button2.disabled = false;
        return;
    }

	if (window.InputCheck) {
		if (!InputCheck()) {
			//申請ボタンをアクティブにする
			document.apply.apply_button1.disabled = false;
			document.apply.apply_button2.disabled = false;
			return;
		}
	}

	// 重複チェック
	dpl_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj_src = 'regist_emp_id' + i;
		emp_id_src = document.apply.elements[obj_src].value;

        if(emp_id_src == "")
        {
            continue;
        }
		for(j=i+1; j<=apv_num; j++) {
			obj_dist = 'regist_emp_id' + j;
			emp_id_dist = document.apply.elements[obj_dist].value;
			if(emp_id_src == emp_id_dist) {
				dpl_flg = true;
			}
		}
	}

	if(dpl_flg)
	{
		if (!confirm('<? echo($approve_label); ?>者が重複していますが、申請しますか？'))
		{
			//申請ボタンをアクティブにする
			document.apply.apply_button1.disabled = false;
			document.apply.apply_button2.disabled = false;
			return;
		}
	}

	//入力チェック処理
	ret = check_input_data_report();
	if(ret == 1)
	{
		//申請ボタンをアクティブにする
		document.apply.apply_button1.disabled = false;
		document.apply.apply_button2.disabled = false;
		return false;
	}
	
	document.apply.action="jnl_application_submit.php";
	document.apply.submit();

}

function apply_printwin(id) {
	document.apply.mode.value = "apply_printwin";
	if(id == "")
	{
		document.apply.action="jnl_application_menu.php";
	}
	else
	{
		document.apply.action="jnl_application_menu.php?apply_id=" + id;
	}
	var default_target = document.apply.target;
	document.apply.target = 'printframe';
	document.apply.submit();
	document.apply.target = default_target;
}


function apply_draft(id, apv_num, precond_num)
{
	//申請ボタンを非アクティブにする
	document.apply.apply_button1.disabled = true;
	document.apply.apply_button2.disabled = true;


	// 未入力チェック
	for(i=1; i<=precond_num; i++)
	{
		obj = 'precond_apply_id' + i;
		apply_id = document.apply.elements[obj].value;
		if(apply_id == "") {
			alert('<? echo($approve_label); ?>確定の申請書を設定してくだい。');
			//申請ボタンをアクティブにする
			document.apply.apply_button1.disabled = false;
			document.apply.apply_button2.disabled = false;
			return;
		}
	}

	// 承認者が全く存在しない場合
    var regist_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj = 'regist_emp_id' + i;
		emp_id = document.apply.elements[obj].value;

		if(emp_id != "") {
			regist_flg = true;
            break;
		}
	}

    if(!regist_flg)
    {
        alert('<? echo($approve_label); ?>者が存在しないため、申請できません。');
		//申請ボタンをアクティブにする
		document.apply.apply_button1.disabled = false;
		document.apply.apply_button2.disabled = false;
        return;
    }

	if (window.InputCheck) {
		if (!InputCheck()) {
			//申請ボタンをアクティブにする
			document.apply.apply_button1.disabled = false;
			document.apply.apply_button2.disabled = false;
			return;
		}
	}

	// 重複チェック
	dpl_flg = false;
	for(i=1; i<=apv_num; i++) {
		obj_src = 'regist_emp_id' + i;
		emp_id_src = document.apply.elements[obj_src].value;

        if(emp_id_src == "")
        {
            continue;
        }

		for(j=i+1; j<=apv_num; j++) {
			obj_dist = 'regist_emp_id' + j;
			emp_id_dist = document.apply.elements[obj_dist].value;

			if(emp_id_src == emp_id_dist) {
                dpl_flg = true;
			}
		}
	}

	if(dpl_flg)
	{
		if (!confirm('<? echo($approve_label); ?>者が重複していますが、申請しますか？'))
		{
			//申請ボタンをアクティブにする
			document.apply.apply_button1.disabled = false;
			document.apply.apply_button2.disabled = false;
			return;
		}
	}

	//それぞれの報告書の種類ごとに入力値をチェックする処理
	ret = check_input_data_report();
	if(ret == 1)
	{
		//申請ボタンをアクティブにする
		document.apply.apply_button1.disabled = false;
		document.apply.apply_button2.disabled = false;
		return false;
	}
	
	document.apply.action="jnl_application_draft_submit.php?apply_id=" + id;
	document.apply.submit();

}

// 下書き保存
function draft_regist()
{
	document.apply.action="jnl_application_submit.php?draft=on";
	document.apply.submit();
}

function draft_update(id)
{
	document.apply.action="jnl_application_draft_submit.php?apply_id=" + id + "&draft=on";
	document.apply.submit();
}

function draft_delete(id)
{
	document.apply.action="jnl_application_draft_delete.php?apply_id=" + id;
	document.apply.submit();
}


// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];
		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}


function updateDisplay() 
{
	if(document.apply.select_display_facility.value == "")
	{
		//病院・施設が選択されていない（どの施設・病院にも報告書を入力する権限がない）
		alert("報告書を入力する権限がありません。");		
	}
	
	document.apply.action = "jnl_application_menu.php";
	document.apply.submit();
}



function changeOptionType(e) 
{
	    
   plant = document.getElementById('select_display_facility');
   plant.innerHTML = '';
    if (e.value == 1) 
	{
        hospital = document.getElementById('select_hospital_facility');
        for (l=0; l < hospital.options.length; l++) 
		{
            plant.options[l] = new Option(hospital.options[l].text, hospital.options[l].value);
        }
    }
    else if (e.value == 2) 
	{
        healthFacilities = document.getElementById('select_old_facility');
        for (l=0; l<healthFacilities.options.length; l++) 
		{
            plant.options[l] = new Option(healthFacilities.options[l].text, healthFacilities.options[l].value);
        }
    }
    
}

var nextElement = null;//項目の移動先(次)
var backElement = null;//項目の移動先（戻り）
function focusToNext(e)
{
	if((getKEYCODE(e) == 13) || (getKEYCODE(e) == 40))//改行コード,下キーだったら
	{
		nextElement.focus();
		return false;
	}

	if(getKEYCODE(e) == 38)//上キーだったら
	{
		backElement.focus();
		return false;
	}

}

function getKEYCODE(e)
{

	var ua = navigator.userAgent;
	
	if(navigator.userAgent.indexOf("MSIE") != -1)
	{					//IE
		return event.keyCode;
	}
	else if(navigator.userAgent.indexOf("Firefox") != -1)
	{		//firefox
		return (e.keyCode)? e.keyCode: e.charCode;
	}
	else if(navigator.userAgent.indexOf("Safari") != -1)
	{		//safari
		return event.keyCode;
	}
	else
	{
		return null;
	}
}


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">


.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}

table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}


p {margin:0;}


</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init();self.focus();document.onkeydown=focusToNext;
<? if ($wkfw_id != "" && $select_display_facility != "") { ?>
initcal();
<? } ?>
<?
if ($mode == "apply_printwin" && !$print_failed)
{

if($wkfw_id != "" && $apply_id == "")
{
?>
window.open('jnl_application_template_print.php?session=<?=$session?>&fname=<?=$fname?>&wkfw_id=<?=$wkfw_id?>&back=f&mode=apply_print', 'apply_print', 'width=670,height=700,scrollbars=yes');
<?
}
else if($wkfw_id != "" && $apply_id != "")
{
?>
window.open('jnl_application_draft_template_print.php?session=<?=$session?>&fname=<?=$fname?>&target_apply_id=<?=$apply_id?>&back=f&mode=apply_print', 'apply_print', 'width=670,height=700,scrollbars=yes');
<?
}
?>

<?
}
?>

if (window.OnloadSub) { OnloadSub(); };resizeAllTextArea();if (window.refreshApproveOrders) {refreshApproveOrders();}">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="jnl_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b48.gif" width="32" height="32" border="0" alt="日報・月報"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>日報・月報</b></a> &gt; <a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>申請</b></a></font></td>
<? if ($workflow_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_workflow_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?
show_applycation_menuitem($session,$fname,"");
?>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<form name="apply" action="#" method="post">
	<input type="hidden" name="session" value="<?=$session?>">
	<input type="hidden" name="wkfw_id" value="<?=$wkfw_id?>">
	<input type="hidden" name="wkfw_type" value="<?=$wkfw_type?>">
	<input type="hidden" name="mode" value="">
	<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">

	<table width="100%" border="0" cellspacing="0" cellpadding="2">
		<tr>
			<td width="15%" valign="top">

<?
//報告書フォルダ表示
show_application_cate_list($con, $session, $fname, $wkfw_type, $wkfw_id, $apply_id);
?>

			</td>
			<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>

<?
if($wkfw_id != "" && $select_display_facility == "" && $apply_id == "")
{
	//初期表示の場合
	
?>
			<td valign="top"  >
				<table border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td valign="top">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病院・施設選択：
								<select id="select_display_facility" name="select_display_facility">
									<? show_options($arr_facility_list);?>
								</select>
							</font>
						</td>
					</tr>
					<tr>
						<td valign="top">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">　　報告書日付：
								<select id='regist_yr' name='regist_yr'><?show_select_years(3, $regist_yr, false);?></select>年
								<select id='regist_mon' name='regist_mon'><?show_select_months($regist_mon, false);?></select>月
<?
			//月報の場合は日付までの指定は必要なし
			
			/*
			(管理CD)0001:病院日報
			(管理CD)0002:病院月報１
			(管理CD)0003:病院月報２
			(管理CD)0004:老人保健施設日報
			(管理CD)0005:老人保健施設日報
			*/
			
			$kind_report = pg_fetch_result($sel, 0, "short_wkfw_name");
	
			if($kind_report == "0001" || $kind_report == "0004")
			{	
				//病院日報、老健日報の場合
?>								
								<select id='regist_day' name='regist_day'><?show_select_days($regist_day, false);?></select>日
<?
			}
			else
			{
				//病院月報、老健月報の場合(日付はゼロに固定する)
?>
								<input type="hidden" name="regist_day" value="00">
<?				
			}

?>								
							</font>
						</td>
					</tr>
					<tr>
						<td valign="top">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
								<input type="button" value="表示" onclick="updateDisplay();">
							</font>
						</td>
					</tr>
				</table>
			</td>
<?
}
else
{
	
	//病院・施設を選択したあとの表示の場合
?>
			<td valign="top"  width="85%">

<?
	if($wkfw_id != "" && $apply_id == "")
	{
		//下書きが存在していないので、下書き保存ボタンの処理を「draft_regist」にする、などなど
		show_application_template($con, $session, $fname, $wkfw_id, $apply_title, $content, $file_id, $filename, $back, $mode,$select_display_facility,$regist_yr,$regist_mon,$regist_day);
	}
	else if($wkfw_id != "" && $apply_id != "")
	{
		//下書きが存在しているので、下書き保存ボタンの処理を「draft_update」にする、などなど
		show_draft_template($con, $session, $fname, $apply_id, $apply_title, $content, $file_id, $filename, $back, $mode,$select_display_facility,$regist_yr,$regist_mon,$regist_day);
	}
?>
				<input type="hidden" name="regist_yr" value="<?=$regist_yr?>">
				<input type="hidden" name="regist_mon" value="<?=$regist_mon?>">
				<input type="hidden" name="regist_day" value="<?=$regist_day?>">

<?
}
?>
			</td>
		</tr>
	</table>
	
</form>

</td>
</tr>
</table>
<iframe name="printframe" width="0" height="0" frameborder="0"></iframe>
</body>
</html>
<?
// jnl_workflow/tmpディレクトリ作成
$obj->create_wkfwtmp_directory();
// jnl_apply/tmpディレクトリ作成
$obj->create_applytmp_directory();
pg_close($con);
?>

