<?php
require("about_comedix.php");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');
require("show_select_values.ini");
require("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

if ($sex == "") {
    $sex = "3";
}

// データベースに接続
$con = connect2db($fname);

$arr_class_name = get_class_name_array($con, $fname);

// 部門一覧を取得
$sql = "select * from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 課一覧を取得
$sql = "select * from atrbmst";
$cond = "where atrb_del_flg = 'f' order by order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);
if ($sel_atrb == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 科一覧を取得
$sql = "select * from deptmst";
$cond = "where dept_del_flg = 'f' order by order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
if ($sel_dept == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 室一覧を取得
if ($arr_class_name["class_cnt"] == 4) {
    $sql = "select * from classroom";
    $cond = "where room_del_flg = 'f' order by order_no";
    $sel_room = select_from_table($con, $sql, $cond, $fname);
    if ($sel_room == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }
} else {
    $sel_room = null;
}

// 環境設定情報を取得
$sql = "select site_id, use_cyrus, hide_passwd from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}
$site_id = pg_fetch_result($sel, 0, "site_id");
$use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");
$hide_passwd = pg_fetch_result($sel, 0, "hide_passwd");


// グループ一覧を配列に格納
$sql = "select group_id, group_nm from authgroup";
$cond = "order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$groups = array();
while ($row = pg_fetch_array($sel)) {
	$groups[$row["group_id"]] = $row["group_nm"];
}
// system_config
$conf = new Cmx_SystemConfig();

// グループでcheckedになる値を指定。（ポストバックで無ければデフォルト値を指定）
if($postback !== "t"){
    //グループの初期設定を取得する
    $auth_group_id = $conf->get("default_set.group_id");
    $disp_group_id = $conf->get("default_set.disp_group_id");
    $menu_group_id = $conf->get("default_set.menu_group_id");
}

// 表示グループの一覧を配列に格納する
$sql = "select group_id, group_nm from dispgroup";
$cond = "order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$disp_groups = array();
while ($row = pg_fetch_array($sel)) {
	$disp_groups[$row["group_id"]] = $row["group_nm"];
}

// メニューグループの一覧を配列に格納する
$sql = "select group_id, group_nm from menugroup";
$cond = "order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu_groups = array();
while ($row = pg_fetch_array($sel)) {
	$menu_groups[$row["group_id"]] = $row["group_nm"];
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | 新規登録</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
function initPage() {
    if (window.opera) document.mainform.photofile.size = 8;

    setAttributeOptions(document.mainform.cls, document.mainform.atrb, document.mainform.dept, document.mainform.room, '<? echo($atrb); ?>', '<? echo($dept); ?>', '<? echo($room); ?>');
<?
for ($i = 1; $i <= $concurrent; $i++) {
    $atrb_var_name = "o_atrb$i";
    $dept_var_name = "o_dept$i";
    $room_var_name = "o_room$i";
    echo("\tsetAttributeOptions(document.mainform.o_cls$i, document.mainform.o_atrb$i, document.mainform.o_dept$i, document.mainform.o_room$i, '{$$atrb_var_name}', '{$$dept_var_name}', '{$$room_var_name}');\n");
}
?>
}

function setAttributeOptions(class_box, atrb_box, dept_box, room_box, atrb_id, dept_id, room_id) {
    deleteAllOptions(atrb_box);

    var class_id = class_box.value;
<? while ($row = pg_fetch_array($sel_atrb)) { ?>
    if (class_id == '<? echo $row["class_id"]; ?>') {
        addOption(atrb_box, '<? echo $row["atrb_id"]; ?>', '<? echo $row["atrb_nm"]; ?>', atrb_id);
    }
<? } ?>

    if (atrb_box.options.length == 0) {
        addOption(atrb_box, '0', '（未登録）');
    }

    setDepartmentOptions(atrb_box, dept_box, room_box, dept_id, room_id);
}

function setDepartmentOptions(atrb_box, dept_box, room_box, dept_id, room_id) {
    deleteAllOptions(dept_box);

    var atrb_id = atrb_box.value;
<? while ($row = pg_fetch_array($sel_dept)) { ?>
    if (atrb_id == '<? echo $row["atrb_id"]; ?>') {
        addOption(dept_box, '<? echo $row["dept_id"]; ?>', '<? echo $row["dept_nm"]; ?>', dept_id);
    }
<? } ?>

    if (dept_box.options.length == 0) {
        addOption(dept_box, '0', '（未登録）');
    }

    setRoomOptions(dept_box, room_box, room_id);
}

function setRoomOptions(dept_box, room_box, room_id) {
    if (!room_box) {
        return;
    }

    deleteAllOptions(room_box);

    var dept_id = dept_box.value;
<? while ($row = pg_fetch_array($sel_room)) { ?>
    if (dept_id == '<? echo $row["dept_id"]; ?>') {
        if (room_box.options.length == 0) {
            addOption(room_box, '0', '　　　　　');
        }
        addOption(room_box, '<? echo $row["room_id"]; ?>', '<? echo $row["room_nm"]; ?>', room_id);
    }
<? } ?>

    if (room_box.options.length == 0) {
        addOption(room_box, '0', '（未登録）');
    }
}

function deleteAllOptions(box) {
    for (var i = box.length - 1; i >= 0; i--) {
        box.options[i] = null;
    }
}

function addOption(box, value, text, selected) {
    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;
    if (selected == value) {
        opt.selected = true;
    }
    box.options[box.length] = opt;
    try {box.style.fontSize = 'auto';} catch (e) {}
    box.style.overflow = 'auto';
}

function setConcurrent(concurrent) {
    if (document.mainform.updatephoto.value == 't' &&
                !confirm('写真の選択が無効になります。よろしいですか？')) {
        return;
    }

    document.mainform.concurrent.value = concurrent;
    document.mainform.action = 'employee_register.php';
    document.mainform.submit();
}

function readFelica() {
    var felica_anchor = document.getElementById('felica_anchor');
    if (!felica_anchor) {
        alert('プラグインがインストールされていないため利用できません。');
        return;
    }
    felica_anchor.click();
    document.mainform.idm.value = document.mainform.idm_hid.value;
}

function setPhoto() {
    var filename = document.mainform.photofile.value;
    if (!filename) {
        document.mainform.updatephoto.value = 'f';
        return;
    }

    var extension = filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();
    if (extension != 'jpg' && extension != 'jpeg') {
        alert('登録可能な画像はJPEG形式のみです。写真は更新されません。');
        document.mainform.updatephoto.value = 'f';
        return;
    }

    document.mainform.updatephoto.value = 't';

    if (document.all) {
        document.getElementById('photo').src = filename;
    }
}
/**
 * jQuery
 */
$(function() {
    $('#deadlink').click(function() {
        $('.js-deadlink').prop('checked', $(this).prop('checked'));
    });
    $('.js-deadlink').click(function() {
        var flg = true;
        $('.js-deadlink').each(function(i, e) {
            if (! $(this).prop('checked')) {
                flg = false;
                return;
            }
        });
        $('#deadlink').prop('checked', flg);
    });
});
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="employee_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>新規登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="employee_db_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録用DB操作</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_auth_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_display_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_bulk_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_auth_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_field_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目管理</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<? if ($site_id == "" && $use_cyrus == "t") { ?>
<div><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">※ログインIDにアルファベットを含まない職員はウェブメール機能を利用できません</font></div>
<? } ?>
<form name="mainform" action="employee_insert.php" method="post" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
<td><input name="personal_id" type="text" maxlength="12" value="<? echo($personal_id); ?>" style="ime-mode:inactive;"></td>
<td colspan="2" align="center"><input type="button" value="職員登録用DBからコピー" onclick="window.open('employee_copy.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');" tabindex="-1"></td>
<td rowspan="12" align="center" valign="top">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<?
if ($updatephoto == "t") {
    $photo_bg_color = "#fefe83";
    $photo_src = "img/spacer.gif";
} else {
    $photo_bg_color = "white";
    $photo_src = (file_exists("profile/{$emp_id}.jpg")) ? "profile/{$emp_id}.jpg" : "img/noprofile.jpg";
}
?>
<tr>
<td align="center" style="border-style:none;"><img id="photo" src="<? echo($photo_src); ?>" height="152" width="114" style="border:solid 1px #5279a5; margin:5px auto 2px; background-color:<? echo($photo_bg_color); ?>"></td>
</tr>
<tr>
<td align="center" style="border-style:none;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="file" name="photofile" size="13" style="width=:135px;" onchange="setPhoto();" tabindex="-1"></font></td>
</tr>
<tr>
<td style="padding:5px 2px 2px; border-style:none;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プロフィール<br><textarea name="profile" style="width:100%; height:116px;" tabindex="-1"><? echo($profile); ?></textarea></font></td>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td width="15%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">苗字（漢字）</font></td>
<td width="23%"><input name="lt_nm" type="text" maxlength="10" value="<? echo($lt_nm); ?>" style="ime-mode:active;"></td>
<td width="15%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名前（漢字）</font></td>
<td width="23%"><input name="ft_nm" type="text" maxlength="10" value="<? echo($ft_nm); ?>" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">苗字（ひらがな）</font></td>
<td><input name="lt_kana_nm" type="text" maxlength="10" value="<? echo($lt_kana_nm); ?>" style="ime-mode:active;"></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名前（ひらがな）</font></td>
<td><input name="ft_kana_nm" type="text" maxlength="10" value="<? echo($ft_kana_nm); ?>" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログインID</font></td>
<td><input name="id" type="text" maxlength="20" value="<? echo($id); ?>" style="ime-mode:inactive;"></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">パスワード</font></td>
<td><input name="pass" type="<? if ($hide_passwd == "t") {echo("password");} else {echo("text");} ?>" value="<? echo($pass); ?>" style="ime-mode:inactive;"></td>
</tr>
<tr height="22">
<? if ($site_id != "") { ?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メールID</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input name="mail_id" type="text" maxlength="20" value="<? echo($mail_id); ?>" style="ime-mode:inactive;"><? if ($use_cyrus == "t") {echo("_$site_id");} ?></font></td>
<? } ?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">性別</font></td>
<td<? if ($site_id == "") {echo(" colspan=\"3\"");} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="sex" value="1"<? if ($sex == "1") {echo(" checked");} ?>>男性
<input type="radio" name="sex" value="2"<? if ($sex == "2") {echo(" checked");} ?>>女性
<input type="radio" name="sex" value="3"<? if ($sex == "3") {echo(" checked");} ?>>未設定
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">生年月日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="birth_yr"><? show_select_years(100, $birth_yr, true); ?></select>/<select name="birth_mon"><? show_select_months($birth_mon, true); ?></select>/<select name="birth_day"><? show_select_days($birth_day, true); ?></select></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入職日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="entry_yr"><? show_select_years(100, $entry_yr, true, true); ?></select>/<select name="entry_mon"><? show_select_months($entry_mon, true); ?></select>/<select name="entry_day"><? show_select_days($entry_day, true); ?></select></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[0]); ?></font></td>
<td><select name="cls" onchange="setAttributeOptions(this.form.cls, this.form.atrb, this.form.dept, this.form.room);">
<?
while ($row = pg_fetch_array($sel_class)) {
    echo("<option value=\"{$row["class_id"]}\"");
    if ($row["class_id"] == $cls) {
        echo(" selected");
    }
    echo(">{$row["class_nm"]}");
}
?>
</select></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[1]); ?></font></td>
<td><select name="atrb" onchange="setDepartmentOptions(this.form.atrb, this.form.dept, this.form.room);"></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[2]); ?></font></td>
<td<? if ($arr_class_name["class_cnt"] == 3) {echo(" colspan=\"3\"");} ?>><select name="dept" onchange="setRoomOptions(this.form.dept, this.form.room);"></select></td>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[3]); ?></font></td>
<td><select name="room"></select></td>
<? } ?>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
<td><select name="status"><? show_select_status($con, $status, $fname); ?></select></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
<td><select name="job"><? show_select_job($con, $job, $fname); ?></select></td>
</tr>

<tr height="22">
	<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></td>
	<td colspan="3">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="auth_group_id">
<option value="">　　　　　</option>
<?
foreach ($groups as $tmp_group_id => $tmp_group_nm) {
	echo("<option value=\"$tmp_group_id\"");
	if ($tmp_group_id == $auth_group_id) {
		echo(" selected");
	}
	echo(">$tmp_group_nm\n");
}
?>
</select></font>
	</td>
</tr>

<tr height="22">
	<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></td>
	<td colspan="3">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="disp_group_id">
<option value="">　　　　　</option>
<?
foreach ($disp_groups as $tmp_group_id => $tmp_group_nm) {
	echo("<option value=\"$tmp_group_id\"");
	if ($tmp_group_id == $disp_group_id) {
		echo(" selected");
	}
	echo(">$tmp_group_nm\n");
}
?>
</select></font>
	</td>
</tr>

<tr height="22">
	<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></td>
	<td colspan="3">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="menu_group_id">
<option value="">　　　　　</option>
<?
foreach ($menu_groups as $tmp_group_id => $tmp_group_nm) {
	echo("<option value=\"$tmp_group_id\"");
	if ($tmp_group_id == $menu_group_id) {
		echo(" selected");
	}
	echo(">$tmp_group_nm\n");
}
?>
</select></font>
	</td>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ICカードIDm</font></td>
<td colspan="3">
<input type="text" name="idm" value="<? echo($idm); ?>" size="30">
<input type="button" value="カード読み込み" onclick="readFelica();">
<span id="FeliCa">
<!--
<a id="felica_anchor" href="felica:">
<input type="hidden" name="idm_hid" />
<polling target="cTafzB6PR9ZGl7MmdfBDm37wfzoBABhLg6mKuzDiXFAH" result="idm_hid" />
</a>
-->
</span>
<div><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j10" color="red">
注意：<br>
・ICカードを読み込むためには，felica対応のカードリーダを接続する必要があります。<br>
・カード読み込みができるのは、IEブラウザエクステンションのプラグインをインストールしたIEブラウザのみです。
</font></div>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">決裁者</font></td>
<td colspan="3"><input type="checkbox" name="aprv" value="t"<? if ($aprv == "t") {echo(" checked");} ?>></td>
</tr>
<? if (version_compare(PHP_VERSION, '5.0.0') >= 0 && substr(pg_parameter_status($con, 'server_version'),0,1) >= 8) { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員連携</font></td>
<td colspan="3">
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <label><input type="checkbox" id="deadlink" <? if ($emp_name_deadlink_flg === "t" && $emp_class_deadlink_flg === "t" && $emp_job_deadlink_flg === "t" && $emp_st_deadlink_flg === "t") {echo("checked");} ?>>連携更新しない</label>
     (
     <label><input type="checkbox" class="js-deadlink" name="emp_name_deadlink_flg" value="t" <? if ($emp_name_deadlink_flg === "t") {echo("checked");} ?>>苗字、名前の連携更新をしない</label>
     <label><input type="checkbox" class="js-deadlink" name="emp_class_deadlink_flg" value="t" <? if ($emp_class_deadlink_flg === "t") {echo("checked");} ?>>所属の連携更新しない</label>
     <label><input type="checkbox" class="js-deadlink" name="emp_job_deadlink_flg" value="t" <? if ($emp_job_deadlink_flg === "t") {echo("checked");} ?>>職種の連携更新しない</label>
     <label><input type="checkbox" class="js-deadlink" name="emp_st_deadlink_flg" value="t" <? if ($emp_st_deadlink_flg === "t") {echo("checked");} ?>>役職の連携更新しない</label>
     )
     </font>
</td>
</tr>
<? } ?>
<?
$border_width = ($concurrent > 1) ? "2px" : "1px";
?>
<tr height="22" bgcolor="#f6f9ff">
<td colspan="3" style="border-top-width:<? echo($border_width); ?>;border-right-style:none;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">兼務所属</font></td>
<td align="right" style="border-top-width:<? echo($border_width); ?>;border-left-style:none;"><input type="button" value="追加" onclick="setConcurrent(<? echo($concurrent + 1); ?>);"></td>
</tr>
<?
for ($i = 1; $i <= $concurrent; $i++) {
    $class_var_name = "o_cls$i";
    $status_var_name = "o_status$i";
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" style="border-top-width:<? echo($border_width); ?>;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[0]); ?></font></td>
<td style="border-top-width:<? echo($border_width); ?>;"><select name="o_cls<? echo($i); ?>" onchange="setAttributeOptions(this.form.o_cls<? echo($i); ?>, this.form.o_atrb<? echo($i); ?>, this.form.o_dept<? echo($i); ?>, this.form.o_room<? echo($i); ?>);">
<?
pg_result_seek($sel_class, 0);
while ($row = pg_fetch_array($sel_class)) {
    echo("<option value=\"{$row["class_id"]}\"");
    if ($row["class_id"] == $$class_var_name) {
        echo(" selected");
    }
    echo(">{$row["class_nm"]}");
}
?>
</select></td>
<td align="right" bgcolor="#f6f9ff" style="border-top-width:<? echo($border_width); ?>;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[1]); ?></font></td>
<td style="border-top-width:<? echo($border_width); ?>;"><select name="o_atrb<? echo($i); ?>" onchange="setDepartmentOptions(this.form.o_atrb<? echo($i); ?>, this.form.o_dept<? echo($i); ?>, this.form.o_room<? echo($i); ?>);"></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[2]); ?></font></td>
<td<? if ($arr_class_name["class_cnt"] == 3) {echo(" colspan=\"3\"");} ?>><select name="o_dept<? echo($i); ?>" onchange="setRoomOptions(this.form.o_dept<? echo($i); ?>, this.form.o_room<? echo($i); ?>);"></select></td>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[3]); ?></font></td>
<td><select name="o_room<? echo($i); ?>"></select></td>
<? } ?>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" style="border-bottom-width:<? echo($border_width); ?>;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
<td colspan="2" style="border-right-style:none;border-bottom-width:<? echo($border_width); ?>;"><select name="o_status<? echo($i); ?>"><? show_select_status($con, $$status_var_name, $fname); ?></select></td>
<td align="right" style="border-left-style:none;border-bottom-width:<? echo($border_width); ?>;"><? if ($i == $concurrent) { ?><input type="button" value="削除" onclick="setConcurrent(<? echo($concurrent - 1); ?>);"><? } else { ?>&nbsp;<? } ?></td>
</tr>
<?
}
?>
<tr height="22" id="delrow" style="border-left-style:none; border-right-style:none; border-bottom-style:none;<? if ($del_id == "") {echo(" display:none;");} ?>">
<td colspan="4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">「<span id="dellabel"><? if ($del_id != "") {echo("$del_id $del_nm");} ?></span>」の情報を職員登録用DBから削除：<input type="checkbox" name="del" value="on"<? if ($del == "on") {echo(" checked");} ?>><input type="hidden" name="del_id" value="<? echo($del_id); ?>"><input type="hidden" name="del_nm" value="<? echo($del_nm); ?>"></font></td>
</tr>
<tr height="22">
<td colspan="4" align="right" style="border-left-style:none; border-right-style:none; border-bottom-style:none;"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="concurrent" value="<? echo($concurrent); ?>">
<input type="hidden" name="updatephoto" value="f">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
