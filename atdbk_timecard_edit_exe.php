<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<script type="text/javascript" src="./js/fontsize.js"></script>
<body>

<form name='errorForm' action='atdbk_timecard_edit.php' method='post'>
	<input type='hidden' name='back'                value='t'>
	<input type='hidden' name='session'             value='<?= $session ?>'>
	<input type='hidden' name='groupId'             value='<?= $groupId ?>'>
	<input type='hidden' name='pattern'             value='<?= $pattern ?>'>
	<input type='hidden' name='wherefrom'           value='<?= $wherefrom ?>'>
	<input type='hidden' name='emp_id'              value='<?= $emp_id ?>'>
	<input type='hidden' name='date'                value='<?= $date ?>'>
	<input type='hidden' name='yyyymm'              value='<?= $yyyymm ?>'>
	<input type='hidden' name='view'                value='<?= $view ?>'>
	<input type='hidden' name='reason'              value='<?= $reason ?>'>
	<input type='hidden' name='night_duty'          value='<?= $night_duty ?>'>
	<input type='hidden' name='allow_ids[]'         value='<?= $allow_ids[0] ?>'>
	<input type='hidden' name='start_hour'          value='<?= $start_hour ?>'>
	<input type='hidden' name='start_min'           value='<?= $start_min ?>'>
	<input type='hidden' name='out_hour'            value='<?= $out_hour ?>'>
	<input type='hidden' name='out_min'             value='<?= $out_min ?>'>
	<input type='hidden' name='ret_hour'            value='<?= $ret_hour ?>'>
	<input type='hidden' name='ret_min'             value='<?= $ret_min ?>'>
	<input type='hidden' name='end_hour'            value='<?= $end_hour ?>'>
	<input type='hidden' name='end_min'             value='<?= $end_min ?>'>
	<input type='hidden' name='pnt_url'             value='<?= $pnt_url ?>'>
	<input type='hidden' name='previous_day_flag'   value='<?= $previous_day_flag ?>'>
	<input type='hidden' name='next_day_flag'       value='<?= $next_day_flag ?>'>
	<input type='hidden' name='shift_admin_flg'     value='<?php echo($shift_admin_flg); ?>'>
	<input type='hidden' name='shift_from_flg'     value='<?php echo($shift_from_flg); ?>'>
<?php
for ($i = 1; $i <= 10; $i++) {
	$start_hour_var = "o_start_hour$i";
	$start_min_var = "o_start_min$i";
	$end_hour_var = "o_end_hour$i";
	$end_min_var = "o_end_min$i";
?>
	<input type='hidden' name='<?=$start_hour_var?>' value='<?= $$start_hour_var ?>'>
	<input type='hidden' name='<?=$start_min_var?>' value='<?= $$start_min_var ?>'>
	<input type='hidden' name='<?=$end_hour_var?>' value='<?= $$end_hour_var ?>'>
	<input type='hidden' name='<?=$end_min_var?>' value='<?= $$end_min_var ?>'>
<?
}
?>

	<input type='hidden' name='status'      value='<?= $status ?>'>

	<input type='hidden' name='meeting_time_hh' value='<?= $meeting_time_hh ?>'>
	<input type='hidden' name='meeting_time_mm' value='<?= $meeting_time_mm ?>'>
	<input type="hidden" name="start_btn_time" value="<? echo($start_btn_time); ?>">
	<input type="hidden" name="end_btn_time" value="<? echo($end_btn_time); ?>">
	<input type="hidden" name="meeting_start_hour" value="<? echo($meeting_start_hour); ?>">
	<input type="hidden" name="meeting_start_min" value="<? echo($meeting_start_min); ?>">
	<input type="hidden" name="meeting_end_hour" value="<? echo($meeting_end_hour); ?>">
	<input type="hidden" name="meeting_end_min" value="<? echo($meeting_end_min); ?>">
<? /* 手当回数、残業時刻追加 20100114 */ ?>
	<input type='hidden' name='allow_counts[]'         value='<?= $allow_counts[0] ?>'>
	<input type="hidden" name="over_start_hour" value="<? echo($over_start_hour); ?>">
	<input type="hidden" name="over_start_min" value="<? echo($over_start_min); ?>">
	<input type="hidden" name="over_end_hour" value="<? echo($over_end_hour); ?>">
	<input type="hidden" name="over_end_min" value="<? echo($over_end_min); ?>">
	<input type='hidden' name='over_start_next_day_flag' value='<?= $over_start_next_day_flag ?>'>
	<input type='hidden' name='over_end_next_day_flag' value='<?= $over_end_next_day_flag ?>'>
<? /* 残業時刻2追加 20110621 */ ?>
	<input type="hidden" name="over_start_hour2" value="<? echo($over_start_hour2); ?>">
	<input type="hidden" name="over_start_min2" value="<? echo($over_start_min2); ?>">
	<input type="hidden" name="over_end_hour2" value="<? echo($over_end_hour2); ?>">
	<input type="hidden" name="over_end_min2" value="<? echo($over_end_min2); ?>">
	<input type='hidden' name='over_start_next_day_flag2' value='<?= $over_start_next_day_flag2 ?>'>
	<input type='hidden' name='over_end_next_day_flag2' value='<?= $over_end_next_day_flag2 ?>'>
	<input type='hidden' name='plan_hope_flg' value='<?= $plan_hope_flg ?>'>
	<input type='hidden' name='emp_idx' value='<?= $emp_idx ?>'>
	<input type='hidden' name='day_idx' value='<?= $day_idx ?>'>
	<? /* 残業理由 */ ?>
	<input type='hidden' name='ovtm_reason_id' value='<? echo($ovtm_reason_id); ?>'>
	<input type='hidden' name='ovtm_reason' value='<? echo($ovtm_reason); ?>'>
<? //時間有休追加 20111207 ?>
	<input type="hidden" name="paid_hol_start_hour" value="<? echo($paid_hol_start_hour); ?>">
	<input type="hidden" name="paid_hol_start_min" value="<? echo($paid_hol_start_min); ?>">
	<input type="hidden" name="paid_hol_hour" value="<? echo($paid_hol_hour); ?>">
	<input type="hidden" name="paid_hol_min" value="<? echo($paid_hol_min); ?>">
	<input type="hidden" name="check_work_flg" value="<? echo($check_work_flg); ?>">
	<input type="hidden" name="empid_list" value="<? echo($empid_list); ?>">
	<input type='hidden' name='allow_ids2[]'         value='<?= $allow_ids2[0] ?>'>
	<input type='hidden' name='allow_counts2[]'         value='<?= $allow_counts2[0] ?>'>
	<input type='hidden' name='allow_ids3[]'         value='<?= $allow_ids3[0] ?>'>
	<input type='hidden' name='allow_counts3[]'         value='<?= $allow_counts3[0] ?>'>
	<input type="hidden" name="mmode"				 value="<? echo($mmode); ?>">
</form>

<form name='mainForm' action='work_admin_timecard_shift.php' method='post'>
	<input type='hidden' name='session'             value='<?php echo($session); ?>'>
	<input type='hidden' name='emp_id'              value='<?php echo($emp_id); ?>'>
	<input type='hidden' name='yyyymm'              value='<?php echo($yyyymm); ?>'>
	<input type='hidden' name='view'                value='<?php echo($view); ?>'>
	<input type='hidden' name='srch_name'            value='<?php urlencode($srch_name); ?>'>
	<input type="hidden" name="cls" 				   value="<?php echo($cls); ?>">
	<input type="hidden" name="atrb"                value="<?php echo($atrb); ?>">
	<input type="hidden" name="dept"                value="<?php echo($dept); ?>">
	<input type="hidden" name="page"                value="<?php echo($page); ?>">
	<input type="hidden" name="wherefrom"            value="<?php echo($wherefrom); ?>">
	<input type="hidden" name="group_id"             value="<?php echo($group_id); ?>">
	<input type="hidden" name="check_work_flg"        value="<?php echo($check_work_flg); ?>">
	<input type="hidden" name="empid_list"            value="<?php echo($empid_list); ?>">
	<input type="hidden" name="srch_id" 				value="<?php echo($srch_id); ?>">
	<input type='hidden' name='shift_admin_flg'     value='<?php echo($shift_admin_flg); ?>'>
	<input type='hidden' name='shift_from_flg'     value='<?php echo($shift_from_flg); ?>'>
	<input type="hidden" name="mypage" 				value="<?php  echo($mypage); ?>">
	<input type='hidden' name='staff_ids'      	  value=''>
	<input type='hidden' name='refresh_flg'     	  value=''>
	<input type="hidden" name="shift_group_id"		 value="<? echo($shift_group_id); ?>">
	<input type="hidden" name="csv_layout_id"		 value="<? echo($csv_layout_id); ?>">
	<input type="hidden" name="duty_form_jokin"		 value="<? echo($duty_form_jokin); ?>">
	<input type="hidden" name="duty_form_hijokin"	 value="<? echo($duty_form_hijokin); ?>">
	<input type="hidden" name="sus_flg"				 value="<? echo($sus_flg); ?>">
	<input type="hidden" name="mmode"				 value="<? echo($mmode); ?>">

</form>


<script type="text/javascript">
<!--
function send_information(refresh_flg,staff_ids) {
	document.mainForm.refresh_flg.value = refresh_flg;
	document.mainForm.staff_ids.value = staff_ids;
	document.mainForm.target = 'timecard';
	document.mainForm.action = 'work_admin_timecard_shift.php';
	document.mainForm.submit();
}
//-->
</script>

<?
require_once("about_comedix.php");
require_once("show_clock_in_common.ini");
require_once("get_values.ini");
require_once("timecard_common_class.php");
require_once("atdbk_workflow_common_class.php");
require_once("timecard_bean.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("date_utils.php");
require_once("calendar_name_class.php");
require_once("ovtm_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$user_flg = false;
switch ($wherefrom) {
case "0":  // タイムカードの出勤簿入力画面より 20130129
case "1":  // タイムカード入力画面より
case "6":
	$checkauth = check_authority($session, 5, $fname);
	if ($checkauth == "0") {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
	$user_flg = true;
	break;
case "2":  // タイムカード修正画面より
case "5":
case "9":
	$checkauth = check_authority($session, 42, $fname);
	if ($checkauth == "0" && $mmode != "usr") {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
	break;
case "3":  // トップ画面より
case "4":  // 勤務シフト作成の実績入力画面より
	case "7":
	case "8":
	case "10":
		break;
default:
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
	break;
}

//更新情報用フラグ 管理画面で変更があった場合に更新するが残業等で更新済みの場合の確認に使用
$update_time_flg = false;

// データベースに接続
$con = connect2db($fname);
$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $date);
//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();

//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出勤務";

//カレンダー名、所定労働時間履歴用
$calendar_name_class = new calendar_name_class($con, $fname);

$ovtm_class = new ovtm_class($con, $fname);

// 時刻のチェック
if (($start_hour == "--" && $start_min != "--") || ($start_hour != "--" && $start_min == "--") || ($end_hour != "--" && $end_min == "--") || ($end_hour == "--" && $end_min != "--") || ($out_hour == "--" && $out_min != "--") || ($out_hour != "--" && $out_min == "--") || ($ret_hour == "--" && $ret_min != "--") || ($ret_hour != "--" && $ret_min == "--")) {
	echo("<script type=\"text/javascript\">alert('時と分の一方のみは登録できません。');</script>");
	echo("<script language='javascript'>document.errorForm.submit();</script>");
	exit;
}
if (($over_start_hour == "--" && $over_start_min != "--") || ($over_start_hour != "--" && $over_start_min == "--") || ($over_end_hour != "--" && $over_end_min == "--") || ($over_end_hour == "--" && $over_end_min != "--")) {
	echo("<script type=\"text/javascript\">alert('残業時刻1の時と分の一方のみは登録できません。');</script>");
	echo("<script language='javascript'>document.errorForm.submit();</script>");
	exit;
}
if (($over_start_hour2 == "--" && $over_start_min2 != "--") || ($over_start_hour2 != "--" && $over_start_min2 == "--") || ($over_end_hour2 != "--" && $over_end_min2 == "--") || ($over_end_hour2 == "--" && $over_end_min2 != "--")) {
	echo("<script type=\"text/javascript\">alert('残業時刻2の時と分の一方のみは登録できません。');</script>");
	echo("<script language='javascript'>document.errorForm.submit();</script>");
	exit;
}
// 勤務条件テーブルより残業管理を取得
$sql = "select no_overtime, specified_time from empcond";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}
//残業管理するフラグ
$no_overtime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "no_overtime") : "";
$paid_specified_time = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "specified_time") : "";

//時間有休追加 20111207
if ($obj_hol_hour->paid_hol_hour_flag == "t" &&
	(!$user_flg || ($user_flg && $ovtm_class->hol_hour_input_nodisp_flg != "t"))) { //表示する場合を条件追加 20150129
    $duty_form = $obj_hol_hour->get_duty_form($emp_id);
    if (($duty_form != "2" && $obj_hol_hour->paid_hol_hour_full_flag == "t") ||
            ($duty_form == "2" && $obj_hol_hour->paid_hol_hour_part_flag == "t")) {


    	//1項目でも入力
        if ($paid_hol_start_hour != "--" || $paid_hol_start_min != "--" || $paid_hol_hour != "--" || $paid_hol_min != "--") {
	    	//勤務実績がない場合 20130507
	    	if ($pattern == "--"){
	    		echo("<script type=\"text/javascript\">alert('勤務実績が登録されていません。');</script>");
	    		echo("<script language='javascript'>document.errorForm.submit();</script>");
	    		exit;
	    	}
            //休暇の場合はエラーとする
            if ($pattern == "10") {
                echo("<script type=\"text/javascript\">alert('休暇の場合には時間有休を指定できません。');</script>");
                echo("<script language='javascript'>document.errorForm.submit();</script>");
                exit;

            }
            //勤務時間を計算しないパターンの場合
            if ($timecard_common_class->check_no_count_pattern($groupId, $pattern)) {
                echo("<script type=\"text/javascript\">alert('勤務時間を計算しないパターンの場合には時間有休を指定できません。');</script>");
                //echo("<script language='javascript'>document.errorForm.submit();</script>");
                echo("<script language='javascript'>history.back();</script>");
                exit;
            }
            //全項目が入力されていない場合
            if (!($paid_hol_start_hour != "--" && $paid_hol_start_min != "--" && $paid_hol_hour != "--" && $paid_hol_min != "--")) {
                echo("<script type=\"text/javascript\">alert('時間有休の開始時刻、時間、分を全て指定してください。');</script>");
                echo("<script language='javascript'>document.errorForm.submit();</script>");
                exit;
            }
        }

        if ($obj_hol_hour->paid_hol_hour_max_flag == "t" && $paid_hol_start_hour != "--") {

            //時間有休使用時間数、限度数確認
            $year = substr($yyyymm, 0, 4);
            $month = substr($yyyymm, 4, 2);

            //カレンダーの所定労働時間を取得
            //$day1_time = $timecard_common_class->get_calendarname_day1_time();
            //所定労働時間履歴対応 20121129
            if ($paid_specified_time != "") {
                $day1_time = $paid_specified_time;
            }
            else {
                $arr_timehist = $calendar_name_class->get_calendarname_time($date);
                $day1_time = $arr_timehist["day1_time"];
            }

            $arr_ret = $obj_hol_hour->paid_hol_hour_max_check($year, $month, $emp_id, $timecard_bean->closing_paid_holiday, $timecard_bean->criteria_months, $date, $paid_hol_hour, $paid_hol_min, $day1_time);
            if ($arr_ret["err_flg"] == "1") {
                echo("<script type=\"text/javascript\">alert('{$arr_ret["err_msg"]}');</script>");
//                echo("<script language='javascript'>document.errorForm.submit();</script>");
                echo("<script language='javascript'>history.back();</script>");
                exit;

            }
        }
    }
}

$empty_flg = false;
for ($i = 1; $i <= 10; $i++) {
	$start_hour_var = "o_start_hour$i";
	$start_min_var = "o_start_min$i";
	$end_hour_var = "o_end_hour$i";
	$end_min_var = "o_end_min$i";

	if ($$start_hour_var == "") {$$start_hour_var = "--";}
	if ($$start_min_var == "") {$$start_min_var = "--";}
	if ($$end_hour_var == "") {$$end_hour_var = "--";}
	if ($$end_min_var == "") {$$end_min_var = "--";}

	if (($$start_hour_var == "--" && $$start_min_var != "--") || ($$start_hour_var != "--" && $$start_min_var == "--") || ($$end_hour_var == "--" && $$end_min_var != "--") || ($$end_hour_var != "--" && $$end_min_var == "--")) {
		echo("<script type=\"text/javascript\">alert('{$ret_str}時刻の時と分の一方のみは登録できません。');</script>");
		echo("<script language='javascript'>document.errorForm.submit();</script>");
		exit;
	}
	if ($empty_flg && ($$start_hour_var != "--" || $$end_hour_var != "--")) {
		echo("<script type=\"text/javascript\">alert('{$ret_str}時刻が不正です。');</script>");
		echo("<script language='javascript'>document.errorForm.submit();</script>");
		exit;
	}
	if ($$start_hour_var == "--" && $$end_hour_var != "--") {
		echo("<script type=\"text/javascript\">alert('{$ret_str}時刻が不正です。');</script>");
		echo("<script language='javascript'>document.errorForm.submit();</script>");
		exit;
	}
	if ($$start_hour_var == "--" || $$end_hour_var == "--") {
		$empty_flg = true;
	}
}
if (($meeting_start_hour == "--" && $meeting_start_min != "--") || ($meeting_start_hour != "--" && $meeting_start_min == "--") || ($meeting_end_hour != "--" && $meeting_end_min == "--") || ($meeting_end_hour == "--" && $meeting_end_min != "--")) {
	echo("<script type=\"text/javascript\">alert('会議・研修・病棟外勤務の時と分の一方のみは登録できません。');</script>");
	echo("<script language='javascript'>document.errorForm.submit();</script>");
	exit;
}

if ($ovtm_reason_id == "other") {
	//残業時刻が指定された時、残業理由の未入力チェック 20120125
	if (($over_start_hour != "--" || $over_start_min != "--" || $over_end_hour != "--" || $over_end_min != "--" ||
				$over_start_hour2 != "--" || $over_start_min2 != "--" || $over_end_hour2 != "--" || $over_end_min2 != "--")
			&& $timecard_bean->over_time_apply_type != "0"
			&& $no_overtime != "t" // 残業管理をする場合
		){
		if ($ovtm_reason == "") {
			echo("<script type=\"text/javascript\">alert('残業理由が入力されていません。');</script>");
			echo("<script language='javascript'>document.errorForm.submit();</script>");
			exit;
		}
	}
	if (strlen($ovtm_reason) > 200) {
		echo("<script type=\"text/javascript\">alert('残業理由が長すぎます。');</script>");
		echo("<script language='javascript'>document.errorForm.submit();</script>");
		exit;
	}
	$ovtm_reason_id = null;
} else {
	$ovtm_reason = "";
}

//フラグ、前ゼロ等編集
//キーボート入力の場合、前ゼロ対応
if ($ovtm_class->keyboard_input_flg == "t") {

    if ($start_hour != "" && ($start_hour >= "0" && $start_hour <= "9")) {
        $start_hour = sprintf("%02d", $start_hour);
    }
    if ($start_min != "" && ($start_min >= "0" && $start_min <= "9")) {
        $start_min = sprintf("%02d", $start_min);
    }
    if ($end_hour != "" && ($end_hour >= "0" && $end_hour <= "9")) {
        $end_hour = sprintf("%02d", $end_hour);
    }
    if ($end_min != "" && ($end_min >= "0" && $end_min <= "9")) {
        $end_min = sprintf("%02d", $end_min);
    }

    if ($ovtm_class->rest_disp_flg == "t") {
        if ($rest_start_hour != "" && ($rest_start_hour >= "0" && $rest_start_hour <= "9")) {
            $rest_start_hour = sprintf("%02d", $rest_start_hour);
        }
        if ($rest_start_min != "" && ($rest_start_min >= "0" && $rest_start_min <= "9")) {
            $rest_start_min = sprintf("%02d", $rest_start_min);
        }
        if ($rest_end_hour != "" && ($rest_end_hour >= "0" && $rest_end_hour <= "9")) {
            $rest_end_hour = sprintf("%02d", $rest_end_hour);
        }
        if ($rest_end_min != "" && ($rest_end_min >= "0" && $rest_end_min <= "9")) {
            $rest_end_min = sprintf("%02d", $rest_end_min);
        }

    }
    if ($out_hour != "" && ($out_hour >= "0" && $out_hour <= "9")) {
        $out_hour = sprintf("%02d", $out_hour);
    }
    if ($out_min != "" && ($out_min >= "0" && $out_min <= "9")) {
        $out_min = sprintf("%02d", $out_min);
    }
    if ($ret_hour != "" && ($ret_hour >= "0" && $ret_hour <= "9")) {
        $ret_hour = sprintf("%02d", $ret_hour);
    }
    if ($ret_min != "" && ($ret_min >= "0" && $ret_min <= "9")) {
        $ret_min = sprintf("%02d", $ret_min);
    }

    for ($i=1; $i<=5; $i++) {
        $idx = ($i == 1) ? "" : $i;
        $s_h = "over_start_hour".$idx;
        $s_m = "over_start_min".$idx;
        $e_h = "over_end_hour".$idx;
        $e_m = "over_end_min".$idx;

        if ($$s_h != "" && ($$s_h >= "0" && $$s_h <= "9")) {
            $$s_h = sprintf("%02d", $$s_h);
        }
        if ($$s_m != "" && ($$s_m >= "0" && $$s_m <= "9")) {
            $$s_m = sprintf("%02d", $$s_m);
        }
        if ($$e_h != "" && ($$e_h >= "0" && $$e_h <= "9")) {
            $$e_h = sprintf("%02d", $$e_h);
        }
        if ($$e_m != "" && ($$e_m >= "0" && $$e_m <= "9")) {
            $$e_m = sprintf("%02d", $$e_m);
        }
        $rs_h = "rest_start_hour".$i;
        $rs_m = "rest_start_min".$i;
        $re_h = "rest_end_hour".$i;
        $re_m = "rest_end_min".$i;
        if ($$rs_h != "" && ($$rs_h >= "0" && $$rs_h <= "9")) {
            $$rs_h = sprintf("%02d", $$rs_h);
        }
        if ($$rs_m != "" && ($$rs_m >= "0" && $$rs_m <= "9")) {
            $$rs_m = sprintf("%02d", $$rs_m);
        }
        if ($$re_h != "" && ($$re_h >= "0" && $$re_h <= "9")) {
            $$re_h = sprintf("%02d", $$re_h);
        }
        if ($$re_m != "" && ($$re_m >= "0" && $$re_m <= "9")) {
            $$re_m = sprintf("%02d", $$re_m);
        }
    }
}

// データベース登録値の編集
if ($pattern == "--") {$pattern = "";}
if ($reason == "--") {$reason = "";}
$start_time = ($start_hour == "--") ? "" : "$start_hour$start_min";
$end_time = ($end_hour == "--") ? "" : "$end_hour$end_min";
$out_time = ($out_hour == "--") ? "" : "$out_hour$out_min";
$ret_time = ($ret_hour == "--") ? "" : "$ret_hour$ret_min";
$meeting_start_time = ($meeting_start_hour == "--") ? "" : "$meeting_start_hour$meeting_start_min";
$meeting_end_time = ($meeting_end_hour == "--") ? "" : "$meeting_end_hour$meeting_end_min";
// 残業時刻追加 20100114
$over_start_time = ($over_start_hour == "--") ? "" : "$over_start_hour$over_start_min";
$over_end_time = ($over_end_hour == "--") ? "" : "$over_end_hour$over_end_min";
// 残業時刻2追加 20110622
$over_start_time2 = ($over_start_hour2 == "--") ? "" : "$over_start_hour2$over_start_min2";
$over_end_time2 = ($over_end_hour2 == "--") ? "" : "$over_end_hour2$over_end_min2";
for ($i = 1; $i <= 10; $i++) {
	$start_time_var = "o_start_time$i";
	$start_hour_var = "o_start_hour$i";
	$start_min_var = "o_start_min$i";
	$end_time_var = "o_end_time$i";
	$end_hour_var = "o_end_hour$i";
	$end_min_var = "o_end_min$i";
	$$start_time_var = ($$start_hour_var == "--") ? "" : "{$$start_hour_var}{$$start_min_var}";
	$$end_time_var = ($$end_hour_var == "--") ? "" : "{$$end_hour_var}{$$end_min_var}";
}
$allow_id = ($allow_ids[0] == "--") ? NULL : $allow_ids[0];
$allow_count = ($allow_ids[0] == "--") ? NULL : $allow_counts[0];
$allow_id2 = ($allow_ids2[0] == "--") ? NULL : $allow_ids2[0];
$allow_count2 = ($allow_ids2[0] == "--") ? NULL : $allow_counts2[0];
$allow_id3 = ($allow_ids3[0] == "--") ? NULL : $allow_ids3[0];
$allow_count3 = ($allow_ids3[0] == "--") ? NULL : $allow_counts3[0];

$maxline = 5; //20141205

$arr_values = array();
for ($i=1; $i<=$maxline; $i++) {
    $idx = ($i == 1) ? "" : $i;
    $s_t = "over_start_time".$idx;
    $s_h = "over_start_hour".$idx;
    $s_m = "over_start_min".$idx;
    $s_f = "over_start_next_day_flag".$idx;
    $$s_t = ($$s_h == "--") ? "" : $$s_h.$$s_m;
    if ($$s_t == "" || $$s_t == null || $$s_f == "" || $$s_f == null) {
        $$s_f = 0;
    }
    $e_t = "over_end_time".$idx;
    $e_h = "over_end_hour".$idx;
    $e_m = "over_end_min".$idx;
    $e_f = "over_end_next_day_flag".$idx;
    $$e_t = ($$e_h == "--") ? "" : $$e_h.$$e_m;
    if ($$e_t == "" || $$e_t == null || $$e_f == "" || $$e_f == null) {
        $$e_f = 0;
    }
    $arr_values[$s_t] = $$s_t;
    $arr_values[$e_t] = $$e_t;
    $arr_values[$s_f] = $$s_f;
    $arr_values[$e_f] = $$e_f;
}
//rest_start_time
$rest_start_time = ($rest_start_hour == "--") ? "" : "$rest_start_hour$rest_start_min";
$rest_end_time = ($rest_end_hour == "--") ? "" : "$rest_end_hour$rest_end_min";

for ($i=1; $i<=$maxline; $i++) {
    $s_t = "rest_start_time".$i;
    $s_h = "rest_start_hour".$i;
    $s_m = "rest_start_min".$i;
    $s_f = "rest_start_next_day_flag".$i;
    $$s_t = ($$s_h == "--") ? "" : $$s_h.$$s_m;
    if ($$s_t == "" || $$s_t == null || $$s_f == "" || $$s_f == null) {
        $$s_f = 0;
    }
    $e_t = "rest_end_time".$i;
    $e_h = "rest_end_hour".$i;
    $e_m = "rest_end_min".$i;
    $e_f = "rest_end_next_day_flag".$i;
    $$e_t = ($$e_h == "--") ? "" : $$e_h.$$e_m;
    if ($$e_t == "" || $$e_t == null || $$e_f == "" || $$e_f == null) {
        $$e_f = 0;
    }
    $arr_values[$s_t] = $$s_t;
    $arr_values[$e_t] = $$e_t;
    $arr_values[$s_f] = $$s_f;
    $arr_values[$e_f] = $$e_f;
}
//追加 20150107
for ($i=2; $i<=$maxline; $i++) {
    $reason_id_num = "ovtm_reason_id".$i;
    $reason_num = "ovtm_reason".$i;
    $arr_values[$reason_id_num] = $$reason_id_num;
    $arr_values[$reason_num] = $$reason_num;
}

//ログインユーザID
$login_emp_id = get_emp_id($con,$session,$fname);

//ワークフロー関連共通クラス
$obj = new atdbk_workflow_common_class($con, $fname);

// 月給者以外も勤務実績がない場合は、登録を不可とする。20141021
if ($pattern == "") {
	if ($start_time != "" || $end_time != "" || $out_time != "" || $ret_time != "" || $o_start_time1 != "" || $o_end_time1 != "" || $o_start_time2 != "" || $o_end_time2 != "" || $o_start_time3 != "" || $o_end_time3 != "" || $o_start_time4 != "" || $o_end_time4 != "" || $o_start_time5 != "" || $o_end_time5 != "" || $o_start_time6 != "" || $o_end_time6 != "" || $o_start_time7 != "" || $o_end_time7 != "" || $o_start_time8 != "" || $o_end_time8 != "" || $o_start_time9 != "" || $o_end_time9 != "" || $o_start_time10 != "" || $o_end_time10) {
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('勤務実績を選択してください。');</script>");
        echo("<script language='javascript'>history.back();</script>");
        exit;
	}
}

// 処理日付の勤務実績レコード数を取得
$sql = "select * from atdbkrslt";
$cond = "where emp_id = '$emp_id' and date = '$date'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$rec_count = pg_num_rows($sel);
if ($rec_count > 0){
	//前回登録情報取得
	$pre_end_time        = pg_fetch_result($sel, 0, "end_time");
	$pre_next_day_flag   = pg_fetch_result($sel, 0, "next_day_flag");
	$pre_night_duty      = pg_fetch_result($sel, 0, "night_duty");
	$pre_o_start_time1   = pg_fetch_result($sel, 0, "o_start_time1");
	$pre_o_start_time2   = pg_fetch_result($sel, 0, "o_start_time2");
	$pre_o_start_time3   = pg_fetch_result($sel, 0, "o_start_time3");
	$pre_o_start_time4   = pg_fetch_result($sel, 0, "o_start_time4");
	$pre_o_start_time5   = pg_fetch_result($sel, 0, "o_start_time5");
	$pre_o_start_time6   = pg_fetch_result($sel, 0, "o_start_time6");
	$pre_o_start_time7   = pg_fetch_result($sel, 0, "o_start_time7");
	$pre_o_start_time8   = pg_fetch_result($sel, 0, "o_start_time8");
	$pre_o_start_time9   = pg_fetch_result($sel, 0, "o_start_time9");
	$pre_o_start_time10  = pg_fetch_result($sel, 0, "o_start_time10");
	// 残業時刻追加 20100114
	$pre_start_time        = pg_fetch_result($sel, 0, "start_time");
	$pre_over_start_time        = pg_fetch_result($sel, 0, "over_start_time");
	$pre_over_end_time        = pg_fetch_result($sel, 0, "over_end_time");
	$pre_over_start_time2        = pg_fetch_result($sel, 0, "over_start_time2");
	$pre_over_end_time2        = pg_fetch_result($sel, 0, "over_end_time2");

	$pre_over_start_next_day_flag        = pg_fetch_result($sel, 0, "over_start_next_day_flag");
	$pre_over_end_next_day_flag        = pg_fetch_result($sel, 0, "over_end_next_day_flag");
	$pre_over_start_next_day_flag2        = pg_fetch_result($sel, 0, "over_start_next_day_flag2");
	$pre_over_end_next_day_flag2        = pg_fetch_result($sel, 0, "over_end_next_day_flag2");

	$pre_tmcd_group_id        = pg_fetch_result($sel, 0, "tmcd_group_id");
	$pre_pattern        = pg_fetch_result($sel, 0, "pattern");
	$pre_reason        = pg_fetch_result($sel, 0, "reason");
	$pre_allow_id        = pg_fetch_result($sel, 0, "allow_id");
    if ($pre_allow_id != "") {
        $pre_allow_count        = pg_fetch_result($sel, 0, "allow_count");
    }
    else {
        $pre_allow_count        = "";
    }
	$pre_allow_id2        = pg_fetch_result($sel, 0, "allow_id2");
    if ($pre_allow_id2 != "") {
        $pre_allow_count2        = pg_fetch_result($sel, 0, "allow_count2");
    }
    else {
        $pre_allow_count2        = "";
    }
	$pre_allow_id3        = pg_fetch_result($sel, 0, "allow_id3");
    if ($pre_allow_id3 != "") {
        $pre_allow_count3        = pg_fetch_result($sel, 0, "allow_count3");
    }
    else {
        $pre_allow_count3        = "";
    }
    $pre_out_time        = pg_fetch_result($sel, 0, "out_time");
	$pre_ret_time        = pg_fetch_result($sel, 0, "ret_time");
	$pre_meeting_start_time        = pg_fetch_result($sel, 0, "meeting_start_time");
	$pre_meeting_end_time        = pg_fetch_result($sel, 0, "meeting_end_time");
    $pre_previous_day_flag        = pg_fetch_result($sel, 0, "previous_day_flag");
    //項目追加 20140717
    for ($i=3; $i<=5; $i++) {
        $s_t = "over_start_time".$i;
        $s_f = "over_start_next_day_flag".$i;
        $ps_t = "pre_over_start_time".$i;
        $ps_f = "pre_over_start_next_day_flag".$i;
        $e_t = "over_end_time".$i;
        $e_f = "over_end_next_day_flag".$i;
        $pe_t = "pre_over_end_time".$i;
        $pe_f = "pre_over_end_next_day_flag".$i;
        $$ps_t = pg_fetch_result($sel, 0, "$s_t");
        $$ps_f = pg_fetch_result($sel, 0, "$s_f");
        $$pe_t = pg_fetch_result($sel, 0, "$e_t");
        $$pe_f = pg_fetch_result($sel, 0, "$e_f");
    }
    $pre_rest_start_time        = pg_fetch_result($sel, 0, "rest_start_time");
    $pre_rest_end_time        = pg_fetch_result($sel, 0, "rest_end_time");
    for ($i=1; $i<=5; $i++) {
        $rs_t = "rest_start_time".$i;
        $rs_f = "rest_start_next_day_flag".$i;
        $prs_t = "pre_rest_start_time".$i;
        $prs_f = "pre_rest_start_next_day_flag".$i;

        $re_t = "rest_end_time".$i;
        $re_f = "rest_end_next_day_flag".$i;
        $pre_t = "pre_rest_end_time".$i;
        $pre_f = "pre_rest_end_next_day_flag".$i;

        $$prs_t = pg_fetch_result($sel, 0, "$rs_t");
        $$prs_f = pg_fetch_result($sel, 0, "$rs_f");
        $$pre_t = pg_fetch_result($sel, 0, "$re_t");
        $$pre_f = pg_fetch_result($sel, 0, "$re_f");
    }
}
else{
	$pre_end_time        = "";
	$pre_next_day_flag   = "";
	$pre_night_duty      = "";
	$pre_o_start_time1   = "";
	$pre_o_start_time2   = "";
	$pre_o_start_time3   = "";
	$pre_o_start_time4   = "";
	$pre_o_start_time5   = "";
	$pre_o_start_time6   = "";
	$pre_o_start_time7   = "";
	$pre_o_start_time8   = "";
	$pre_o_start_time9   = "";
	$pre_o_start_time10  = "";
	// 残業時刻追加 20100114
	$pre_start_time       = "";
	$pre_over_start_time = "";
	$pre_over_end_time   = "";
	$pre_over_start_time2 = "";
	$pre_over_end_time2   = "";

	$pre_over_start_next_day_flag        = "";
	$pre_over_end_next_day_flag        = "";
	$pre_over_start_next_day_flag2        = "";
	$pre_over_end_next_day_flag2        = "";

	$pre_tmcd_group_id        = "";
	$pre_pattern        = "";
	$pre_reason        = "";
	$pre_allow_id        = "";
    $pre_allow_count        = "";
	$pre_allow_id2        = "";
    $pre_allow_count2        = "";
	$pre_allow_id3        = "";
    $pre_allow_count3        = "";
    $pre_out_time        = "";
	$pre_ret_time        = "";
	$pre_meeting_start_time        = "";
	$pre_meeting_end_time        = "";
    $pre_previous_day_flag        = "";
    //項目追加 20140717
    for ($i=3; $i<=5; $i++) {
        $ps_t = "pre_over_start_time".$i;
        $ps_f = "pre_over_start_next_day_flag".$i;
        $pe_t = "pre_over_end_time".$i;
        $pe_f = "pre_over_end_next_day_flag".$i;
        $$ps_t = "";
        $$ps_f = "";
        $$pe_t = "";
        $$pe_f = "";
    }
    $pre_rest_start_time        = "";
    $pre_rest_end_time        = "";
    for ($i=1; $i<=5; $i++) {
        $prs_t = "pre_rest_start_time".$i;
        $pre_t = "pre_rest_end_time".$i;
        $prs_f = "pre_rest_start_next_day_flag".$i;
        $pre_f = "pre_rest_end_next_day_flag".$i;
        $$prs_t = "";
        $$pre_t = "";
        $$prs_f = "";
        $$pre_f = "";
    }
}
//時間有休の情報取得 20130621
if ($obj_hol_hour->paid_hol_hour_flag == "t") {
    $tmp_arr_hol_hour = $obj_hol_hour->get_paid_hol_hour($emp_id, $date);
    $pre_paid_hol_hour_start_time = $tmp_arr_hol_hour["start_time"];
    $pre_paid_hol_hour = $tmp_arr_hol_hour["use_hour"];
    $pre_paid_hol_min = $tmp_arr_hol_hour["use_minute"];
}
else {
    $pre_paid_hol_hour_start_time = null;
    $pre_paid_hol_hour = null;
    $pre_paid_hol_min = null;
}

//前日フラグが未設定、出勤時刻が入力されていない場合、前日フラグを初期化(0にする)
if ($start_time == "" || $start_time == null || $previous_day_flag == "" || $previous_day_flag == null){
	$previous_day_flag = 0;
}

//翌日フラグが未設定、退勤時刻が入力されていない場合、翌日フラグを初期化(0にする)
if ($end_time == "" || $end_time == null || $next_day_flag == "" || $next_day_flag == null){
	$next_day_flag = 0;
}

// 残業時刻追加 20100114
//残業開始翌日フラグを初期化(0にする)
if ($over_start_time == "" || $over_start_time == null || $over_start_next_day_flag == "" || $over_start_next_day_flag == null){
	$over_start_next_day_flag = 0;
}
//残業終了翌日フラグを初期化(0にする)
if ($over_end_time == "" || $over_end_time == null || $over_end_next_day_flag == "" || $over_end_next_day_flag == null){
	$over_end_next_day_flag = 0;
}
//残業開始2翌日フラグを初期化(0にする)
if ($over_start_time2 == "" || $over_start_time2 == null || $over_start_next_day_flag2 == "" || $over_start_next_day_flag2 == null){
	$over_start_next_day_flag2 = 0;
}
//残業終了2翌日フラグを初期化(0にする)
if ($over_end_time2 == "" || $over_end_time2 == null || $over_end_next_day_flag2 == "" || $over_end_next_day_flag2 == null){
	$over_end_next_day_flag2 = 0;
}
//残業理由
$pre_ovtm_reason_id = "";
$pre_ovtm_reason = "";
if ($pre_over_start_time != "" || $pre_over_end_time != "" ||
		$pre_over_start_time2 != "" || $pre_over_end_time2 != "") {
	//残業申請情報
	$sql = "select apply_id, reason_id, reason, reason_detail, apply_status from ovtmapply";
	$cond = "where emp_id = '$emp_id' and target_date = '$date' and delete_flg = 'f' order by apply_id desc limit 1";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0)
	{
		//$apply_id = pg_fetch_result($sel, 0, "apply_id");
		$pre_ovtm_reason_id = pg_fetch_result($sel, 0, "reason_id");
		$pre_ovtm_reason = pg_fetch_result($sel, 0, "reason");
		//$reason_detail = pg_fetch_result($sel, 0, "reason_detail");
	}
}


// 勤務実績レコードがなければ作成、あれば更新
if ($rec_count == 0) {
	$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, night_duty, allow_id, start_time, end_time, out_time, ret_time, o_start_time1, o_end_time1, o_start_time2, o_end_time2, o_start_time3, o_end_time3, o_start_time4, o_end_time4, o_start_time5, o_end_time5, o_start_time6, o_end_time6, o_start_time7, o_end_time7, o_start_time8, o_end_time8, o_start_time9, o_end_time9, o_start_time10, o_end_time10, status, meeting_time, tmcd_group_id, previous_day_flag, next_day_flag, reg_prg_flg, meeting_start_time, meeting_end_time, allow_count, over_start_time, over_end_time, over_start_next_day_flag, over_end_next_day_flag, over_start_time2, over_end_time2, over_start_next_day_flag2, over_end_next_day_flag2";
    //追加 20140623
    for ($i=3; $i<=$maxline; $i++) {
        $sql .= ", over_start_time".$i;
        $sql .= ", over_end_time".$i;
        $sql .= ", over_start_next_day_flag".$i;
        $sql .= ", over_end_next_day_flag".$i;
    }
    for ($i=1; $i<=$maxline; $i++) {
        $sql .= ", rest_start_time".$i;
        $sql .= ", rest_end_time".$i;
        $sql .= ", rest_start_next_day_flag".$i;
        $sql .= ", rest_end_next_day_flag".$i;
    }
    if ($ovtm_class->rest_disp_flg == "t") {
        $sql .= ", rest_start_time";
        $sql .= ", rest_end_time";
    }
    $sql .= ", allow_id2, allow_count2, allow_id3, allow_count3";
	$sql .= ") values (";
	$content = array($emp_id, $date, $pattern, $reason, $night_duty, $allow_id, $start_time, $end_time, $out_time, $ret_time, $o_start_time1, $o_end_time1, $o_start_time2, $o_end_time2, $o_start_time3, $o_end_time3, $o_start_time4, $o_end_time4, $o_start_time5, $o_end_time5, $o_start_time6, $o_end_time6, $o_start_time7, $o_end_time7, $o_start_time8, $o_end_time8, $o_start_time9, $o_end_time9, $o_start_time10, $o_end_time10, $status, $meeting_time, $groupId, $previous_day_flag, $next_day_flag, "1", $meeting_start_time, $meeting_end_time, $allow_count, $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
    //追加
    for ($i=3; $i<=$maxline; $i++) {
        $s_t = "over_start_time".$i;
        $e_t = "over_end_time".$i;
        $s_f = "over_start_next_day_flag".$i;
        $e_f = "over_end_next_day_flag".$i;
        array_push($content, $$s_t);
        array_push($content, $$e_t);
        array_push($content, $$s_f);
        array_push($content, $$e_f);
    }
    for ($i=1; $i<=$maxline; $i++) {
        $s_t = "rest_start_time".$i;
        $e_t = "rest_end_time".$i;
        $s_f = "rest_start_next_day_flag".$i;
        $e_f = "rest_end_next_day_flag".$i;
        array_push($content, $$s_t);
        array_push($content, $$e_t);
        array_push($content, $$s_f);
        array_push($content, $$e_f);
    }
    if ($ovtm_class->rest_disp_flg == "t") {
        array_push($content, "$rest_start_time");
        array_push($content, "$rest_end_time");
    }
    array_push($content, $allow_id2);
    array_push($content, $allow_count2);
    array_push($content, $allow_id3);
    array_push($content, $allow_count3);
    $ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
} else {
	$sql = "update atdbkrslt set";
	$set = array("pattern", "reason", "night_duty", "allow_id", "start_time", "end_time", "out_time", "ret_time", "o_start_time1", "o_end_time1", "o_start_time2", "o_end_time2", "o_start_time3", "o_end_time3", "o_start_time4", "o_end_time4", "o_start_time5", "o_end_time5", "o_start_time6", "o_end_time6", "o_start_time7", "o_end_time7", "o_start_time8", "o_end_time8", "o_start_time9", "o_end_time9", "o_start_time10", "o_end_time10", "status", "meeting_time", "tmcd_group_id", "previous_day_flag", "next_day_flag", "reg_prg_flg", "meeting_start_time", "meeting_end_time", "allow_count", "over_start_time", "over_end_time", "over_start_next_day_flag", "over_end_next_day_flag", "over_start_time2", "over_end_time2", "over_start_next_day_flag2", "over_end_next_day_flag2", "allow_id2", "allow_count2", "allow_id3", "allow_count3");
	$setvalue = array($pattern, $reason, $night_duty, $allow_id, $start_time, $end_time, $out_time, $ret_time, $o_start_time1, $o_end_time1, $o_start_time2, $o_end_time2, $o_start_time3, $o_end_time3, $o_start_time4, $o_end_time4, $o_start_time5, $o_end_time5, $o_start_time6, $o_end_time6, $o_start_time7, $o_end_time7, $o_start_time8, $o_end_time8, $o_start_time9, $o_end_time9, $o_start_time10, $o_end_time10, $status, $meeting_time, $groupId, $previous_day_flag, $next_day_flag, "1", $meeting_start_time, $meeting_end_time, $allow_count, $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2, $allow_id2, $allow_count2, $allow_id3, $allow_count3);
    //追加
    for ($i=3; $i<=5; $i++) {
        $s_t = "over_start_time".$i;
        $e_t = "over_end_time".$i;
        $s_f = "over_start_next_day_flag".$i;
        $e_f = "over_end_next_day_flag".$i;
        array_push($set, $s_t);
        array_push($set, $e_t);
        array_push($set, $s_f);
        array_push($set, $e_f);
        array_push($setvalue, $$s_t);
        array_push($setvalue, $$e_t);
        array_push($setvalue, $$s_f);
        array_push($setvalue, $$e_f);
    }
    for ($i=1; $i<=5; $i++) {
        $s_t = "rest_start_time".$i;
        $e_t = "rest_end_time".$i;
        $s_f = "rest_start_next_day_flag".$i;
        $e_f = "rest_end_next_day_flag".$i;
        array_push($set, $s_t);
        array_push($set, $e_t);
        array_push($set, $s_f);
        array_push($set, $e_f);
        array_push($setvalue, $$s_t);
        array_push($setvalue, $$e_t);
        array_push($setvalue, $$s_f);
        array_push($setvalue, $$e_f);
    }
    if ($ovtm_class->rest_disp_flg == "t") {
        array_push($set, "rest_start_time");
        array_push($set, "rest_end_time");
        array_push($setvalue, "$rest_start_time");
        array_push($setvalue, "$rest_end_time");
    }
    $cond = "where emp_id = '$emp_id' and date = '$date'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//時間有休追加 20111207
if ($obj_hol_hour->paid_hol_hour_flag == "t") {
	$obj_hol_hour->update_paid_hol_hour($emp_id, $date, $paid_hol_start_hour, $paid_hol_start_min, $paid_hol_hour, $paid_hol_min);
}

// 翌日以降の休暇を実績へ登録
$timecard_common_class->next_hol_set($date, $emp_id);

//理由詳細、現在入力なし
$reason_detail = null;

// 退勤時刻が変わった場合
$night_duty_flag = ($night_duty == 1);
$pre_night_duty_flag = ($pre_night_duty == 1);
// 残業時刻、出勤時刻（早出残業の可能性）が変わった場合
//管理画面の場合にも処理する 20130621
if ($start_time != $pre_start_time || $over_start_time != $pre_over_start_time || $over_end_time != $pre_over_end_time || $end_time != $pre_end_time || $next_day_flag != $pre_next_day_flag || $pre_night_duty_flag != $night_duty_flag || $over_start_time2 != $pre_over_start_time2 || $over_end_time2 != $pre_over_end_time2 || $over_start_time3 != $pre_over_start_time3 || $over_end_time3 != $pre_over_end_time3 || $over_start_time4 != $pre_over_start_time4 || $over_end_time4 != $pre_over_end_time4 || $over_start_time5 != $pre_over_start_time5 || $over_end_time5 != $pre_over_end_time5 || $ovtm_reason_id != $pre_ovtm_reason_id || $ovtm_reason != $pre_ovtm_reason
        || ($wherefrom == "2" || $wherefrom == "4" || $wherefrom == "5" || $wherefrom == "7" || $wherefrom == "8" || $wherefrom == "9" || $wherefrom == "10")
) {

	// 残業申請画面表示フラグの設定
	$ovtmscr_flg = false;
	if ($no_overtime == "t" || ($over_start_time == "" && $over_end_time == "" && $over_start_time2 == "" && $over_end_time2 == "")) { //残業時刻が未入力の場合、残業の処理をしないに変更 20100819
		//残業有無
		$ovtm_time_flg = false;
	} else {
		$arr_result = get_atdbkrslt($con, $emp_id, $date, $fname);

		//管理画面からの場合、事後申請時もtrueとするためのフラグの設定をする
		if ($wherefrom == "2" || $wherefrom == "4" || $wherefrom == "5" || $wherefrom == "7" || $wherefrom == "8" || $wherefrom == "9" || $wherefrom == "10") {
			$link_type_flg = true;
		} else {
			$link_type_flg = false;
		}
		//事由に関わらず休暇で残業時刻がありの条件へ変更 20100916
		//明けを条件に追加
		$after_night_duty_flag = "";
		if ($pattern != "" && $pattern != "10" && (($over_start_time != "" && $over_end_time != "") || ($over_start_time2 != "" && $over_end_time2 != ""))) {
			$sql = "select after_night_duty_flag from atdptn";
			$cond = "where group_id = $groupId and atdptn_id = $pattern";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			if (pg_num_rows($sel) > 0) {
				$after_night_duty_flag = pg_fetch_result($sel, 0, "after_night_duty_flag");
			}
		}
		//休暇、明け、事後申請
		if (($pattern == "10" || $after_night_duty_flag == "1" || $timecard_bean->over_time_apply_type == "2") && (($over_start_time != "" && $over_end_time != "") || ($over_start_time2 != "" && $over_end_time2 != ""))) {
			$ovtm_time_flg = true;
		} else {
			//残業有無確認用の翌日フラグ
			if ($over_end_time != "") {
				$tmp_chk_next_day_flag = $over_end_next_day_flag;
			}
			else {
				$tmp_chk_next_day_flag = $next_day_flag;
			}
            //個人別勤務時間帯履歴対応 20130220 start
            $emp_officehours = $timecard_common_class->get_officehours_emp($emp_id, $date, $arr_result["tmcd_group_id"], $arr_result["pattern"]);
            if ($emp_officehours["office_start_time"] != "") {
                $arr_result["office_start_time"] = $emp_officehours["office_start_time"];
                $arr_result["office_end_time"] = $emp_officehours["office_end_time"];
            }
            //個人別勤務時間帯履歴対応 20130220 end
            //残業有無
			$ovtm_time_flg = $timecard_common_class->is_overtime_apply($date, $start_time, $previous_day_flag, $end_time, $tmp_chk_next_day_flag, $night_duty, $arr_result["office_start_time"], $arr_result["office_end_time"], $link_type_flg, $arr_result["tmcd_group_id"], $over_start_time, $over_end_time, "", $arr_result["over_24hour_flag"], $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
		}
		if ($wherefrom == "0" || $wherefrom == "1" || $wherefrom == "3" || $wherefrom == "6") {
			if ($ovtm_time_flg) {
				$ovtmscr_flg = true;
			}
		}
	}

	//apply_id
    $sql = "select apply_id, apply_status from ovtmapply ";  //状態を取得20130621
	$cond = "where emp_id = '$emp_id' and target_date = '$date' and delete_flg = 'f' order by apply_id desc";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$ovtm_apply_num = pg_numrows($sel);

	//ユーザ画面、管理でIDあり残業なし
	//delete
	if ((($wherefrom == "0" || $wherefrom == "1" || $wherefrom == "3" || $wherefrom == "6") && $ovtm_apply_num > 0 && $ovtm_time_flg == false) ||
			(($wherefrom == "2" || $wherefrom == "4" || $wherefrom == "5" || $wherefrom == "7" || $wherefrom == "8" || $wherefrom == "9" || $wherefrom == "10") && $ovtm_apply_num > 0 && $ovtm_time_flg == false)) {

		while($row = pg_fetch_array($sel))
		{
			$ovtm_apply_id = $row["apply_id"];
            //申請不要は除く
            if ($row["apply_status"] != "4") {
                // 残業申請承認論理削除
                $obj->delete_ovtm_apply_id($ovtm_apply_id);
                //管理画面からの場合、最終更新者、日時も更新 20160325
                $obj->update_atdbkrslt_emp_id_time($emp_id, $date, $login_emp_id);
				$update_time_flg = true;
            }
		}

	}
	//ユーザ画面、残業あり、残業申請 20111102
	if (($wherefrom == "0" || $wherefrom == "1" || $wherefrom == "3" || $wherefrom == "6") && $ovtm_time_flg == true) {
		//残業時刻が変わった場合
        $overtime_chg_flg = false;
        $next_day_flg_chg_flg = false;
        for ($i=1; $i<=5; $i++) {
            $idx = ($i == 1) ? "" : $i;
            $s_t = "over_start_time".$idx;
            $ps_t = "pre_over_start_time".$idx;
            $e_t = "over_end_time".$idx;
            $pe_t = "pre_over_end_time".$idx;
            $s_f = "over_start_next_day_flag".$idx;
            $ps_f = "pre_over_start_next_day_flag".$idx;
            $e_f = "over_end_next_day_flag".$idx;
            $pe_f = "pre_over_end_next_day_flag".$idx;
            if ($$s_t != $$ps_t || $$e_t != $$pe_t || $$s_f != $$ps_f || $$e_f != $$pe_f) {
                $overtime_chg_flg = true;
            }
        }
        if ($overtime_chg_flg) {
			//既存データ確認
			$sql = "select apply_id from ovtmapply ";
			$cond = "where emp_id = '$emp_id' and target_date = '$date' and delete_flg = 'f' order by apply_id";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			while($row = pg_fetch_array($sel))
			{
				$ovtm_apply_id = $row["apply_id"];

				// 残業申請承認論理削除
				$obj->delete_ovtm_apply_id($ovtm_apply_id);

			}
			//入力がある場合
			if ($over_start_time != "" || $over_end_time != "" ||
					$over_start_time2 != "" || $over_end_time2 != "") {

                //項目追加 20140717
                $obj->insert_ovtm_apply	($emp_id, $date, $ovtm_reason_id, $ovtm_reason, $end_time, $next_day_flag, $start_time, $previous_day_flag, $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, "0", $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2, $reason_detail, $arr_values, $maxline);


			}
		}
		//変更なし、残業入力あり
		else {
			//入力がある場合
			if ($over_start_time != "" || $over_end_time != "" ||
					$over_start_time2 != "" || $over_end_time2 != "") {
				//既存データ確認、削除以外、(申請不要以外?)
				$sql = "select apply_id, apply_status from ovtmapply ";
				$cond = "where emp_id = '$emp_id' and target_date = '$date' and delete_flg = 'f' order by apply_id desc limit 1";
				$sel = select_from_table($con, $sql, $cond, $fname);
				if ($sel == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				$sinsei_flg = false;
				$num = pg_num_rows($sel);
				if ($num > 0) {
					$apply_status = pg_fetch_result($sel, 0, "apply_status");
					$apply_id = pg_fetch_result($sel, 0, "apply_id");
					//申請中、承認済み、そのまま変更しない
					if ($apply_status == "0" || $apply_status == "1") {
						$sinsei_flg = false;
						//出勤時刻等を更新
						$sql = "update ovtmapply set";
						$set = array("start_time", "end_time", "previous_day_flag", "next_day_flag", "reason_id", "reason");
						$setvalue = array($start_time, $end_time, $previous_day_flag, $next_day_flag, $ovtm_reason_id, $ovtm_reason);
						$cond = "where apply_id = $apply_id ";
						$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
						if ($upd == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}

					}
					//上記以外は申請
					else {
						$sinsei_flg = true;
					}
				} else {
					$sinsei_flg = true;
				}
				if ($sinsei_flg) {
                    $obj->insert_ovtm_apply	($emp_id, $date, $ovtm_reason_id, $ovtm_reason, $end_time, $next_day_flag, $start_time, $previous_day_flag, $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, "0", $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2, $reason_detail, $arr_values, $maxline);
				}
			}
		}


	}

	//管理で残業あり
	//insert
	if (($wherefrom == "2" || $wherefrom == "4" || $wherefrom == "5" || $wherefrom == "7" || $wherefrom == "8" || $wherefrom == "9" || $wherefrom == "10") && $ovtm_time_flg == true) {

        $ovtm_apply_status = "";
        $ovtm_chg_flg = false;
        if ($start_time != $pre_start_time || $over_start_time != $pre_over_start_time || $over_end_time != $pre_over_end_time || $end_time != $pre_end_time || $next_day_flag != $pre_next_day_flag || $pre_night_duty_flag != $night_duty_flag || $over_start_time2 != $pre_over_start_time2 || $over_end_time2 != $pre_over_end_time2 || $ovtm_reason_id != $pre_ovtm_reason_id || $ovtm_reason != $pre_ovtm_reason) {
            $ovtm_chg_flg = true;
        }

		//IDなし insert
		if ( $ovtm_apply_num == 0) {
			//申請、承認済で追加
            $obj->insert_ovtm_apply($emp_id, $date, $ovtm_reason_id, $ovtm_reason, $end_time, $next_day_flag, $start_time, $previous_day_flag, $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, "1", $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2, $reason_detail, $arr_values, $maxline);
		}
		//IDあり update
		else {
			$ovtm_apply_id = pg_fetch_result($sel, 0, "apply_id");
            $ovtm_apply_status = pg_fetch_result($sel, 0, "apply_status");
            //申請中の場合、承認済に更新 20130621
            if ($ovtm_apply_status == "0") {
                $obj->update_ovtm_status_all($ovtm_apply_id, "1");
            }
            //申請中、承認済み以外（残業申請不要も含む）
            elseif ($ovtm_apply_status != "1") {
                //項目が変更されていたら承認済みに更新
                if ($ovtm_chg_flg) {
                    $obj->update_ovtm_status_all($ovtm_apply_id, "1");
                }
            }
			//時刻の更新
			$sql = "update ovtmapply set";
			$set = array("end_time", "next_day_flag", "start_time", "previous_day_flag", "over_start_time", "over_end_time", "over_start_next_day_flag", "over_end_next_day_flag", "over_start_time2", "over_end_time2", "over_start_next_day_flag2", "over_end_next_day_flag2", "reason_id", "reason");
			$setvalue = array($end_time, $next_day_flag, $start_time, $previous_day_flag, $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2, $ovtm_reason_id, $ovtm_reason);
            //追加
            for ($i=3; $i<=$maxline; $i++) {
                $s_t = "over_start_time".$i;
                $e_t = "over_end_time".$i;
                $s_f = "over_start_next_day_flag".$i;
                $e_f = "over_end_next_day_flag".$i;
                array_push($set, $s_t);
                array_push($set, $e_t);
                array_push($set, $s_f);
                array_push($set, $e_f);
                array_push($setvalue, $$s_t);
                array_push($setvalue, $$e_t);
                array_push($setvalue, $$s_f);
                array_push($setvalue, $$e_f);
            }
            for ($i=1; $i<=$maxline; $i++) {
                $s_t = "rest_start_time".$i;
                $e_t = "rest_end_time".$i;
                $s_f = "rest_start_next_day_flag".$i;
                $e_f = "rest_end_next_day_flag".$i;
                array_push($set, $s_t);
                array_push($set, $e_t);
                array_push($set, $s_f);
                array_push($set, $e_f);
                array_push($setvalue, $$s_t);
                array_push($setvalue, $$e_t);
                array_push($setvalue, $$s_f);
                array_push($setvalue, $$e_f);
            }
			//追加 20150105
			for ($i=2; $i<=$maxline; $i++) {
				//db
			    $reason_id_nm = "reason_id".$i;
			    $reason_nm = "reason".$i;
			    //gamen
			    $ovtm_reason_id_nm = "ovtm_reason_id".$i;
			    $ovtm_reason_nm = "ovtm_reason".$i;
			    $s_t = "over_start_time".$i;
			    if ($$s_t == "") {
					$$ovtm_reason_id_nm = null;
					$$ovtm_reason_nm = null;
			    }
			    else {
					if ($$ovtm_reason_id_nm == "other") {
						$$ovtm_reason_id_nm = null;
					}
				}
			    array_push($set, $reason_id_nm);
			    array_push($set, $reason_nm);
			    array_push($setvalue, $$ovtm_reason_id_nm);
			    array_push($setvalue, pg_escape_string($$ovtm_reason_nm));
			}
            $cond = "where apply_id = $ovtm_apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
        //申請中、または、項目変更があった場合
        if ($ovtm_apply_status == "0" || $ovtm_chg_flg) {
            //管理画面からの場合、最終更新者、日時も更新
            $obj->update_atdbkrslt_emp_id_time($emp_id, $date, $login_emp_id);
			$update_time_flg = true;
        }
	}

}

//呼出時刻変更時の申請対応 20130213
$rtn_chg_flg = false; //呼出時刻変更フラグ
for ($i=1; $i<=10; $i++) {
    $start_time_var = "o_start_time$i";
    $end_time_var = "o_end_time$i";
    $pre_start_time_var = "pre_o_start_time$i";
    $pre_end_time_var = "pre_o_end_time$i";
    //1件でも変更があればtrue
    if ($$start_time_var != $$pre_start_time_var || $$end_time_var != $$pre_end_time_var) {
        $rtn_chg_flg = true;
        break;
    }
}
//ユーザ画面側の場合 20130213
if ($wherefrom == "0" || $wherefrom == "1" || $wherefrom == "3" || $wherefrom == "6") {
	// 退勤後復帰時刻が変わった場合
	if ($rtn_chg_flg) {

		$sql = "select apply_id from rtnapply ";
		$cond = "where emp_id = '$emp_id' and target_date = '$date' order by apply_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		while($row = pg_fetch_array($sel))
		{
			$rtn_apply_id = $row["apply_id"];

			// 退勤後復帰申請承認論理削除
			$sql = "update rtnaprv set";
			$set = array("delete_flg");
			$setvalue = array("t");
			$cond = "where apply_id = $rtn_apply_id";

			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 退勤後復帰申請論理削除
			$sql = "update rtnapply set";
			$set = array("delete_flg");
			$setvalue = array("t");
			$cond = "where apply_id = $rtn_apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 承認候補を論理削除
			$sql = "update rtnaprvemp set";
			$set = array("delete_flg");
			$setvalue = array("t");
			$cond = "where apply_id = $rtn_apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			// 同期・非同期情報を論理削除
			$sql = "update rtn_async_recv set";
			$set = array("delete_flg");
			$setvalue = array("t");
			$cond = "where apply_id = $rtn_apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}


		// 退勤後復帰申請画面表示フラグの設定
		$rtnscr_flg = ($o_start_time1 != "");
	} else {
		$rtnscr_flg = false;
	}
}
//管理側の場合呼出承認 20130213 start
//管理側、勤務シフト作成からの場合
if ($wherefrom == "2" || $wherefrom == "4" || $wherefrom == "5" || $wherefrom == "7" || $wherefrom == "8" || $wherefrom == "9" || $wherefrom == "10") {
    //現在の状態取得
    $sql = "select apply_id, apply_status from rtnapply";
    $cond = "where emp_id = '$emp_id' and target_date = '$date' and delete_flg = 'f' order by apply_id desc limit 1";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo ("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo ("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $rtn_status = "";
    if (pg_num_rows($sel) > 0)
    {
        $rtn_apply_id = pg_fetch_result($sel, 0, "apply_id");
        $rtn_status = pg_fetch_result($sel, 0, "apply_status");
    }
    //申請中の場合
    if ($rtn_status == "0") {
        //時刻設定ありの場合
        if ($o_start_time1 != "") {
            //承認へ状態更新、呼出申請
            $obj->update_rtn_status_all($rtn_apply_id, "1");
        }
        //時刻設定なしの場合
        else {
            //削除
            $obj->delete_rtn_apply_id($rtn_apply_id);
        }

    }
    //申請中以外の場合
    else {
        //時刻の変更がある場合
        if ($rtn_chg_flg) {
            //存在する場合、削除
            if ($rtn_apply_id != "") {
                $obj->delete_rtn_apply_id($rtn_apply_id);
            }

            //時刻設定ありの場合
            if ($o_start_time1 != "") {
                //レコード追加、承認、時刻は勤務実績に格納参照するため、不要
                $obj->insert_rtn_apply($emp_id, $date, "1", "", "", "", "", "", "");
            }
        }
    }
    //申請中の場合、または、時刻が変わった場合
    if ($rtn_status == "0" || $rtn_chg_flg) {
        //管理画面からの場合、最終更新者、日時も更新
        $obj->update_atdbkrslt_emp_id_time($emp_id, $date, $login_emp_id);
		$update_time_flg = true;
    }
}
//管理側の場合呼出承認 20130213 end

//勤務シフト作成から開いたタイムカード修正画面からの場合、勤務時間修正も更新 20120328
//管理側からの場合も勤務時間修正を更新 20130621
if ($wherefrom == "2" || $wherefrom == "4" || $wherefrom == "5" || $wherefrom == "7" || $wherefrom == "8" || $wherefrom == "9" || $wherefrom == "10") {
    $sql = " select "
        ." apply_id, apply_status "
        ." from tmmdapply "
    ;
    $cond = "where emp_id = '$emp_id' "
        ." and target_date = '$date' "
        ." and delete_flg = false and re_apply_id is null "
    ;
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0)
    {
        $tmmd_apply_id = pg_fetch_result($sel, 0, "apply_id");
        if ($tmmd_apply_id != "") {
            //時間有休
            if ($paid_hol_start_hour != "--") {
                $paid_hol_hour_start_time = $paid_hol_start_hour.$paid_hol_start_min;
            }
            else {
                $paid_hol_hour_start_time = null;
                $paid_hol_hour = null;
                $paid_hol_min = null;
            }
            // 申請情報を更新
            $sql = "update tmmdapply set";

            $set = array("a_tmcd_group_id", "a_pattern", "a_reason", "a_night_duty", "a_allow_id", "a_start_time", "a_out_time", "a_ret_time", "a_end_time", "a_o_start_time1", "a_o_end_time1", "a_o_start_time2", "a_o_end_time2", "a_o_start_time3", "a_o_end_time3", "a_o_start_time4", "a_o_end_time4", "a_o_start_time5", "a_o_end_time5", "a_o_start_time6", "a_o_end_time6", "a_o_start_time7", "a_o_end_time7", "a_o_start_time8", "a_o_end_time8", "a_o_start_time9", "a_o_end_time9", "a_o_start_time10", "a_o_end_time10", "a_previous_day_flag", "a_next_day_flag", "a_meeting_start_time", "a_meeting_end_time", "a_allow_count", "a_over_start_time", "a_over_end_time", "a_over_start_next_day_flag", "a_over_end_next_day_flag", "a_over_start_time2", "a_over_end_time2", "a_over_start_next_day_flag2", "a_over_end_next_day_flag2", "a_paid_hol_hour_start_time", "a_paid_hol_use_hour", "a_paid_hol_use_minute", "a_allow_id2", "a_allow_count2", "a_allow_id3", "a_allow_count3");
            $setvalue = array($groupId, $pattern, $reason, $night_duty, $allow_id, $start_time, $out_time, $ret_time, $end_time, $o_start_time1, $o_end_time1, $o_start_time2, $o_end_time2, $o_start_time3, $o_end_time3, $o_start_time4, $o_end_time4, $o_start_time5, $o_end_time5, $o_start_time6, $o_end_time6, $o_start_time7, $o_end_time7, $o_start_time8, $o_end_time8, $o_start_time9, $o_end_time9, $o_start_time10, $o_end_time10, $previous_day_flag, $next_day_flag, $meeting_start_time, $meeting_end_time, $allow_counts[0], $over_start_time, $over_end_time, $over_start_next_day_flag, $over_end_next_day_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2, $paid_hol_hour_start_time, $paid_hol_hour, $paid_hol_min, $allow_id2, $allow_count2, $allow_id3, $allow_count3);
            //項目追加 20140717
            array_push($set, "a_rest_start_time");
            array_push($set, "a_rest_end_time");
            array_push($setvalue, $rest_start_time);
            array_push($setvalue, $rest_end_time);
            //追加
            for ($i=3; $i<=$maxline; $i++) {
                $s_t = "a_over_start_time".$i;
                $e_t = "a_over_end_time".$i;
                $s_f = "a_over_start_next_day_flag".$i;
                $e_f = "a_over_end_next_day_flag".$i;
                $s_tv = "over_start_time".$i;
                $e_tv = "over_end_time".$i;
                $s_fv = "over_start_next_day_flag".$i;
                $e_fv = "over_end_next_day_flag".$i;
                array_push($set, $s_t);
                array_push($set, $e_t);
                array_push($set, $s_f);
                array_push($set, $e_f);
                array_push($setvalue, $$s_tv);
                array_push($setvalue, $$e_tv);
                array_push($setvalue, $$s_fv);
                array_push($setvalue, $$e_fv);
            }
            for ($i=1; $i<=$maxline; $i++) {
                $s_t = "a_rest_start_time".$i;
                $e_t = "a_rest_end_time".$i;
                $s_f = "a_rest_start_next_day_flag".$i;
                $e_f = "a_rest_end_next_day_flag".$i;
                $s_tv = "rest_start_time".$i;
                $e_tv = "rest_end_time".$i;
                $s_fv = "rest_start_next_day_flag".$i;
                $e_fv = "rest_end_next_day_flag".$i;
                array_push($set, $s_t);
                array_push($set, $e_t);
                array_push($set, $s_f);
                array_push($set, $e_f);
                array_push($setvalue, $$s_tv);
                array_push($setvalue, $$e_tv);
                array_push($setvalue, $$s_fv);
                array_push($setvalue, $$e_fv);
            }

            $cond = "where apply_id = $tmmd_apply_id";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            //項目が未入力ならば削除 20130621
            if ($pattern == "" && $reason == "" && $night_duty == "" && $allow_id == "" && $allow_id2 == "" && $allow_id3 == "" && $start_time == "" && $out_time == "" && $ret_time == "" && $end_time == "" && $o_start_time1 == "" && $o_end_time1 == "" && $meeting_start_time == "" && $meeting_end_time == "" && $over_start_time == "" && $over_end_time == "" && $over_start_time2 == "" && $over_end_time2 == "" && $paid_hol_hour_start_time == "" && $paid_hol_hour == "" && $paid_hol_min == "") {
                $obj->delete_tmmd_apply_id($tmmd_apply_id);
            }
            else {
                //状態が申請中なら承認済みとする
                $tmmd_apply_status = pg_fetch_result($sel, 0, "apply_status");
                if ($tmmd_apply_status == "0") {
                    $obj->update_tmmd_status_all($tmmd_apply_id, "1");
                    //管理画面からの場合、最終更新者、日時も更新
                    $obj->update_atdbkrslt_emp_id_time($emp_id, $date, $login_emp_id);
					$update_time_flg = true;
                }
                //申請中、承認済み以外で、項目が変更された場合
                elseif ($tmmd_apply_status != "1") {
                    if ($groupId != $pre_tmcd_group_id || $pattern != $pre_pattern || $reason != $pre_reason || $night_duty != $pre_night_duty || $allow_id != $pre_allow_id
                            || $allow_count != $pre_allow_count
                            || $allow_id2 != $pre_allow_id2
                            || $allow_count2 != $pre_allow_count2
                            || $allow_id3 != $pre_allow_id3
                            || $allow_count3 != $pre_allow_count3
                            || $start_time != $pre_start_time || $out_time != $pre_out_time || $ret_time != $pre_ret_time || $end_time != $pre_end_time || $meeting_start_time != $pre_meeting_start_time || $meeting_end_time != $pre_meeting_end_time || $over_start_time != $pre_over_start_time || $over_end_time != $pre_over_end_time || $over_start_time2 != $pre_over_start_time2 || $over_end_time2 != $pre_over_end_time2 || $paid_hol_hour_start_time != $pre_paid_hol_hour_start_time || $paid_hol_hour != $pre_paid_hol_hour || $paid_hol_min != $pre_paid_hol_min
                            || $previous_day_flag != $pre_previous_day_flag
                            || $next_day_flag != $pre_next_day_flag
                            || $over_start_next_day_flag != $pre_over_start_next_day_flag
                            || $over_end_next_day_flag != $pre_over_end_next_day_flag
                            || $over_start_next_day_flag2 != $pre_over_start_next_day_flag2
                            || $over_end_next_day_flag2 != $pre_over_end_next_day_flag2
                            || $rtn_chg_flg) {
                        $obj->update_tmmd_status_all($tmmd_apply_id, "1");
                        //管理画面からの場合、最終更新者、日時も更新
                        $obj->update_atdbkrslt_emp_id_time($emp_id, $date, $login_emp_id);
						$update_time_flg = true;
                    }
                }
            }
        }
    }
}
if ($update_time_flg == false) {
	if ($wherefrom == "2" || $wherefrom == "4" || $wherefrom == "5" || $wherefrom == "7" || $wherefrom == "8" || $wherefrom == "9" || $wherefrom == "10") {
		$paid_hol_hour_start_time = ($paid_hol_hour_start_time == "--") ? NULL : "$paid_hol_hour_start_time";
		$paid_hol_hour = ($paid_hol_hour == "--") ? NULL : "$paid_hol_hour";
		$paid_hol_min = ($paid_hol_min == "--") ? NULL : "$paid_hol_min";
        if ($groupId != $pre_tmcd_group_id || $pattern != $pre_pattern || $reason != $pre_reason || $night_duty != $pre_night_duty || $allow_id != $pre_allow_id
            || $allow_count != $pre_allow_count
            || $allow_id2 != $pre_allow_id2
            || $allow_count2 != $pre_allow_count2
            || $allow_id3 != $pre_allow_id3
            || $allow_count3 != $pre_allow_count3
            || $start_time != $pre_start_time || $out_time != $pre_out_time || $ret_time != $pre_ret_time || $end_time != $pre_end_time || $meeting_start_time != $pre_meeting_start_time || $meeting_end_time != $pre_meeting_end_time  || $paid_hol_hour_start_time != $pre_paid_hol_hour_start_time || $paid_hol_hour != $pre_paid_hol_hour || $paid_hol_min != $pre_paid_hol_min
            || $previous_day_flag != $pre_previous_day_flag
            || $next_day_flag != $pre_next_day_flag
            ) {
            $obj->update_atdbkrslt_emp_id_time($emp_id, $date, $login_emp_id);
		}
	}
}
//勤務シフト作成からの場合以外
if ($wherefrom != "4") {
	// 親画面をリフレッシュ
	switch ($wherefrom) {
		case "0":  // タイムカード入力出勤簿画面より 20130129
			echo("<script type=\"text/javascript\">opener.location.href = 'atdbk_timecard_shift.php?session=$session&yyyymm=$yyyymm&view=$view';</script>");
			break;
		case "1":  // タイムカード入力画面より
			echo("<script type=\"text/javascript\">opener.location.href = 'atdbk_timecard.php?session=$session&yyyymm=$yyyymm&view=$view';</script>");
			break;
		case "2":  // タイムカード修正画面より
		case "7":
			$url_srch_name = urlencode($srch_name);
			$refresh_flg = ($wherefrom == "7") ? "true" : "false";
			echo("<script type=\"text/javascript\">opener.location.href = 'work_admin_timecard.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&view=$view&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&wherefrom=$wherefrom&group_id=$group_id&staff_ids=$staff_ids&refresh_flg=$refresh_flg&srch_id=$srch_id&sus_flg=$sus_flg';</script>");
			break;
		case "3":  // トップ画面より
			echo("<script type=\"text/javascript\">opener.location.href = 'main_menu.php?session=$session';</script>");
			break;
		case "5":  // タイムカード修正A4横画面より（管理側）
		case "8":
			$url_srch_name = urlencode($srch_name);
			$refresh_flg = ($wherefrom == "8") ? "true" : "false";
            echo("<script type=\"text/javascript\">opener.location.href = 'work_admin_timecard_a4.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&view=$view&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&wherefrom=$wherefrom&group_id=$group_id&staff_ids=$staff_ids&refresh_flg=$refresh_flg&srch_id=$srch_id&sus_flg=$sus_flg';</script>");
			break;
		case "6":  // タイムカード入力A4横画面より
			echo("<script type=\"text/javascript\">opener.location.href = 'atdbk_timecard_a4.php?session=$session&yyyymm=$yyyymm&view=$view';</script>");
			break;
		case "9":  // タイムカード出勤簿画面より 20130129
		case "10":
			$url_srch_name = urlencode($srch_name);
 			$refresh_flg = ($wherefrom == "10") ? "true" : "false";

 			if ($shift_from_flg == "t"){
            	echo("<script type=\"text/javascript\">opener.location.href = 'work_admin_timecard_shift.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&view=$view&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&wherefrom=$wherefrom&group_id=$group_id&staff_ids=$staff_ids&refresh_flg=$refresh_flg&check_work_flg=$check_work_flg&srch_id=$srch_id&empid_list=$empid_list&shift_from_flg=$shift_from_flg&sus_flg=$sus_flg&mmode=$mmode&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin';</script>");
            }else{
            	echo("<script type=\"text/javascript\">send_information('{$refresh_flg}', '{$staff_ids}');</script>");
            }
			break;
	}

	// 親画面のURLをエンコード
	switch ($wherefrom) {
		case "0":  // タイムカード入力出勤簿画面より 20130129
			$pnt_url = urlencode("atdbk_timecard_shift.php?session=$session&yyyymm=$yyyymm&view=$view");
			break;
		case "1":  // タイムカード入力画面より
			$pnt_url = urlencode("atdbk_timecard.php?session=$session&yyyymm=$yyyymm&view=$view");
			break;
		case "2":  // タイムカード修正画面より
		case "7":
			$url_srch_name = urlencode($srch_name);
            $pnt_url = urlencode("work_admin_timecard.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&view=$view&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&wherefrom=$wherefrom&group_id=$group_id&srch_id=$srch_id");
			break;
		case "3":  // トップ画面より
			$pnt_url = urlencode("main_menu.php?session=$session");
			break;
		case "5":  // A4横画面より
		case "8":
			$url_srch_name = urlencode($srch_name);
            $pnt_url = urlencode("work_admin_timecard_a4.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&view=$view&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&wherefrom=$wherefrom&group_id=$group_id&srch_id=$srch_id");
			break;
		case "6":  // タイムカード入力A4横画面より
			$pnt_url = urlencode("atdbk_timecard_a4.php?session=$session&yyyymm=$yyyymm&view=$view");
			break;
		case "9":  // タイムカード出勤簿画面より 20130129
		case "10":
			$url_srch_name = urlencode($srch_name);
            $pnt_url = urlencode("work_admin_timecard_shift.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&view=$view&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&wherefrom=$wherefrom&group_id=$group_id&check_work_flg=$check_work_flg&srch_id=$srch_id&empid_list=$empid_list&shift_admin_flg=$shift_admin_flg&sus_flg=$sus_flg&mmode=$mmode");
			break;
	}

	// 必要に応じて残業申請画面を表示
//	if ($ovtmscr_flg) {
//		echo("<script type=\"text/javascript\">opener.open('overtime_apply.php?session=$session&date=$date&pnt_url=$pnt_url', 'ovtmwin', 'width=640,height=480,scrollbars=yes');</script>");
//	}

	// 必要に応じて退勤後復帰申請画面を表示
	if ($rtnscr_flg) {
		echo("<script type=\"text/javascript\">opener.open('return_apply.php?session=$session&date=$date&pnt_url=$pnt_url', 'rtnwin', 'width=640,height=480,scrollbars=yes');</script>");
	}
} else {

//add 20111115-->
	$font_name = '';
	$font_color='';
	$back_color='';
	$cel_id    ='data'.(($emp_idx*2)+2).'_'.$day_idx;

	$color_cel_id ='day'.$day_idx;

	// 勤務実績未選択時はデータ取得をスルー
	if ($groupId != '' AND $pattern != ''){
		$sql   = "SELECT FONT_NAME";
		$sql  .= "     , FONT_COLOR_ID";
		$sql  .= "     , BACK_COLOR_ID";
		$sql  .= "  FROM DUTY_SHIFT_PATTERN";
		$cond  = " WHERE PATTERN_ID     = $groupId";
		$cond .= "   AND ATDPTN_PTN_ID  = $pattern";
		if ($pattern == 10){
			if ($reason==''){
				$cond .= "   AND REASON         = '34'";
			} else {
				$cond .= "   AND REASON         = '$reason'";
			}
		}
		$cond .= " ORDER BY PATTERN_ID, ATDPTN_PTN_ID LIMIT 1";
		$sel  = select_from_table($con, $sql, $cond, $fname);

		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		if (pg_num_rows($sel) > 0) {
			$font_name = str_replace("'", "\'", h(pg_fetch_result($sel, 0, "FONT_NAME")));
			$font_color = pg_fetch_result($sel, 0, "FONT_COLOR_ID");
			$back_color = pg_fetch_result($sel, 0, "BACK_COLOR_ID");
		}
	}
//add 20111115<--

	//リフレッシュ
	$day_idx = intval($day_idx);

//mod 20111115-->
	//画面情報を再設定しないよう、元の情報（グループID）をクリアする
//	echo("<script type=\"text/javascript\">parent.opener.document.getElementById('cause_group_id').value = '';</script>");
//	echo("<script type=\"text/javascript\">parent.opener.callbackPtnUpdate('$plan_hope_flg','$emp_idx','$day_idx','$pattern','$reason');</script>");

	echo("<script type=\"text/javascript\">");
	echo("parent.opener.document.getElementById('cause_group_id').value = '';");

	echo("parent.opener.document.getElementById('$cel_id').style.background = '$back_color';");
//	echo("parent.opener.document.getElementById('$cel_id').innerHTML='<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$font_color\">$font_name</font>';");
	echo("parent.opener.document.getElementById('$cel_id').innerHTML='<font face=\"ＭＳ Ｐゴシック, Osaka\" color=\"$font_color\">$font_name</font>';");

	echo("parent.opener.callbackPtnUpdate('$plan_hope_flg','$emp_idx','$day_idx','$pattern','$reason','non');");
	echo("</script>");
//mod 20111115<--

}
// データベース接続を閉じる
pg_close($con);


// 自画面を閉じる
echo("<script type=\"text/javascript\">self.close();</script>");
?>
</body>
