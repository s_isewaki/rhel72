<?
require_once("about_comedix.php");
require_once("fplus_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// インシデント権限のチェック
$checkauth = check_authority($session, 78, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

//検索ボタン押下時に１が入る
$serch = @$_REQUEST["search_mode"];

// 指定された日付
if($arisedate == ""){
	$arisedate = @$_REQUEST["arise_date"];
}

$hospital_id = @$_REQUEST["hospital_id"];

//検索ボタン押下後
if($serch == "1"){
	$sql = " SELECT ".
		" 	rep.medical_accident_id ".
		" 	, rep.report_no ".
		" 	, to_char(rep.occurrences_date, 'YYYY/MM/DD') AS occurrences_date ".
		" 	, to_char(rep.report_date, 'YYYY/MM/DD') AS report_date ".
		" 	, rep.hospital_id ".
		" 	, hosp.hospital_name ".
		" 	, rep.requirements1 ".
		" 	, rep.requirements2 ".
		" 	, rep.requirements3 ".
		" 	, rep.patient_name ".
		" 	, rep.patient_sex ".
		" 	, (CASE rep.patient_sex ".
		" 		WHEN '1' THEN '男性' ".
		" 		ELSE '女性' ".
		" 	  END) as sex_nm ".
		" 	, rep.patient_age ".
		" 	, rep.patient_class ".
		" 	, (CASE rep.patient_class ".
		" 		WHEN '1' THEN '入院' ".
		" 		ELSE '外来' ".
		" 	  END) as hos_vis_nm ".
		" 	, rep.clinical_departments ".
		" 	, hos_dep.item_name as hos_dep_nm ".
		" 	, rep.main_disease ".
		" 	, rep.hospitalization_progress ".
		" 	, rep.title ".
		" 	, rep.outline ".
		" 	, (CASE rep.outline ".
		" 		WHEN '1' THEN '薬剤' ".
		" 		WHEN '2' THEN '輸血' ".
		" 		WHEN '3' THEN '治療・処置' ".
		" 		WHEN '4' THEN '医療機器等' ".
		" 		WHEN '5' THEN 'ドレーンチューブ' ".
		" 		WHEN '6' THEN '検査' ".
		" 		ELSE '療養上の世話（転倒・転落等）' ".
		" 	  END) as outline_nm ".
		" 	, rep.fact_progress ".
		" 	, rep.accident_cope ".
		" 	, rep.patient_family_cope ".
		" 	, rep.occurrences_factor ".
		" FROM ".
		" 	fplus_first_report rep left join (SELECT ".
		" 						item_cd ".
		" 						, item_name ".
		" 					FROM ".
		" 						tmplitem ".
		" 					WHERE ".
		" 						mst_cd = 'A402' ".
		" 						and disp_flg = 't') hos_dep ".
		" 					on rep.clinical_departments = hos_dep.item_cd ".
		" 	,fplusapply app ".
		" 	,fplus_hospital_master hosp ".
		" WHERE ".
		" 	rep.occurrences_date = '$arisedate' ".
		" 	and rep.apply_id = app.apply_id ".
		" 	and app.delete_flg = 'f' ".
		" 	and rep.hospital_id = hosp.hospital_id ".
		" 	and rep.hospital_id = '$hospital_id' ".
		" 	and hosp.del_flg = 'f' ".
		" 	and rep.medical_accident_id NOT IN (SELECT medical_accident_id ".
		" 						FROM fplus_second_report s, fplusapply app ".
		" 						WHERE s.apply_id = app.apply_id ".
		" 							AND app.delete_flg = 'f' ";

		if($link_id_self == ""){
			$sql .= " 						) ";
		}else{
			$sql .= " 							AND s.medical_accident_id <> '$link_id_self') ";
		}
		$sql .= " ORDER BY ".
			" 	rep.occurrences_date ".
			" 	, rep.report_date desc ";


	$sel = @select_from_table($con, $sql, "", $fname);
	$result = pg_fetch_all($sel);
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>CoMedix 検索 | 事故内容</title>
</head>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td {border:#E6B3D4 solid 1px; padding:3px; }
</style>
<script type="text/javascript">

<? //検索結果を返す ?>
function setData(rowidx) {
	opener.document.apply.elements["data_occurrences_date"].value = document.getElementById("hdn_arize_date_" + rowidx).value;
	opener.document.apply.elements["data_hospital_id"].value = document.getElementById("hdn_hospital_" + rowidx).value;
	var aci1 = opener.document.apply.elements['data_requirements1[0]'];
	var aci2 = opener.document.apply.elements['data_requirements2[0]'];
	var aci3 = opener.document.apply.elements['data_requirements3[0]'];
	if(document.getElementById("hdn_requirements1_" + rowidx).value == "t"){
		aci1.checked = true;
		aci2.disabled = true;
	}
	if(document.getElementById("hdn_requirements2_" + rowidx).value == "t"){
		aci2.checked = true;
		aci1.disabled = true;
	}
	if(document.getElementById("hdn_requirements3_" + rowidx).value == "t"){
		aci3.checked = true;
	}
	opener.document.apply.elements["data_patient_name"].value = document.getElementById("hdn_patient_name_" + rowidx).value;
	var sex = opener.document.apply.elements['data_patient_sex[]'];
	for(var i = 0; i < sex.length; i++){
		if(document.getElementById("hdn_sex_" + rowidx).value==sex[i].value){
			sex[i].checked = true;
		}
	}
	opener.document.apply.elements["data_patient_age"].value = document.getElementById("hdn_age_" + rowidx).value;
	var hos_vis = opener.document.apply.elements['data_patient_class[]'];
	for(var i = 0; i < hos_vis.length; i++){
		if(document.getElementById("hdn_hos_vis_kbn_" + rowidx).value==hos_vis[i].value){
			hos_vis[i].checked = true;
		}
	}
	opener.document.apply.elements["data_clinical_departments"].value = document.getElementById("hdn_hospital_dept_" + rowidx).value;
	opener.document.apply.elements["data_main_disease"].value = document.getElementById("hdn_disease_" + rowidx).value;
	opener.document.apply.elements["data_title"].value = document.getElementById("hdn_title_" + rowidx).value;
	var outline = opener.document.apply.elements['data_outline[]'];
	for(var i = 0; i < outline.length; i++){
		if(document.getElementById("hdn_outline_" + rowidx).value==outline[i].value){
			outline[i].checked = true;
		}
	}
	opener.document.apply.elements["data_fact_progress"].value = document.getElementById("hdn_fact_prog_" + rowidx).value;
	opener.document.apply.elements["data_accident_cope"].value = document.getElementById("hdn_aci_cor_" + rowidx).value;
	opener.document.apply.elements["data_patient_family_cope"].value = document.getElementById("hdn_family_cor_" + rowidx).value;
	opener.document.apply.elements["data_accident_report"].value = document.getElementById("hdn_gene_cause_" + rowidx).value;
	opener.document.apply.elements["hdn_link_id"].value = document.getElementById("hdn_link_id_" + rowidx).value;
	opener.document.apply.elements["first_report_no"].value = document.getElementById("hdn_report_no_" + rowidx).value;

	self.close();
}

//今日ボタン押下処理
function getDateLabel() {

	currentDateObject = new Date();

	wYear = currentDateObject.getYear();

	currentFullYear = (wYear < 2000) ? wYear + 1900 : wYear;
	currentMonth = to2String(currentDateObject.getMonth() + 1);
	currentDate = to2String(currentDateObject.getDate());

	dateString = currentFullYear + "/" + currentMonth + "/" + currentDate;

	var obj_ymd = document.getElementById("arise_date");
	obj_ymd.value = dateString;
}

function to2String(value) {
	label = "" + value;
	if (label.length < 2) {
		label = "0" + label;
	}
	return label;
}

//カレンダーからの戻り処理を行います。
function call_back_fplus_calendar(caller,ymd)
{
	var obj_ymd = document.getElementById("arise_date");
	obj_ymd.value = ymd;
}

function dateClear() {
	var obj_ymd = document.getElementById("arise_date");
	obj_ymd.value = "";
}

function clickselbutton() {
	if(document.getElementById("arise_date").value == "") {
		alert("検索条件を入力してください。");
		return false;
	}else{
		document.sel.action = 'fplus_selfirst_repo_pop.php';
		document.sel.search_mode.value = "1";
		document.sel.target = "_self";
		document.sel.submit();
		return false;
	}
}

function highlightCells(class_name) {
	changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}

</script>
<style type="text/css">
form {margin:0;}
/*table.list {border-collapse:collapse;}*/
table.list td {border:solid #5279a5 1px;}
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<center>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?
	show_fplus_header_for_sub_window("事故内容検索");
?>
</table>
<form name="sel" action="#" method="post">
	<input type="hidden" name="session" id="session" value="<? echo($session); ?>">
	<input type="hidden" name="search_mode" id="search_mode" value="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td  bgcolor="#F5FFE5">
<? //検索条件入力フォーム　ここから-------------------- ?>
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><font class="j12"><span>発生年月日</span></font>
			<font class="j12">
					<input type="text" name="arise_date" id="arise_date" size="15" readonly="readonly" value="<?=$arisedate?>" />
					<input type="button" value="今日" onclick="getDateLabel();" />
					<img id="arise_date_caller" src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer; left: 0px;" alt="カレンダー" onclick="window.open('fplus_calendar.php?session=<? echo($session); ?>&caller=arise_date', 'epinet_arise_calendar', 'width=640,height=480,scrollbars=yes');"/>
					<input type="button" value="クリア" onclick="dateClear();" />
					<input id="Button2" type="button" value="検索" style="width:100px;" onclick="clickselbutton();" />
			</font>			
		</td>
	</tr>
</table>
<? //検索条件入力フォーム　ここまで-------------------- ?>
<? //検索結果一覧表示　ここから-------------- ?>
<table border="0" cellpadding="0" cellspacing="0" class="list1" style="background-color:#fff;">
	<input type="hidden" id="hospital_id" name="hospital_id" value="<?=$hospital_id?>"/>
	<tr  style="background-color:#ffdfff;">
		<td rowspan="2" align="center" style="width:80px;"><font class="j12"><span>発生年月日</span></font></td>
		<td rowspan="2" align="center" style="width:80px;"><font class="j12"><span>報告年月日</span></font></td>
		<td rowspan="2" align="center" style="width:150px;"><font class="j12"><span>表題</span></font></td>
		<td rowspan="2" align="center" style="width:90px;"><font class="j12"><span>概要</span></font></td>
		<td colspan="6" align="center" style="width:390px;"><font class="j12"><span>対象患者</span></font></td>
	</tr>
	<tr  style="background-color:#ffdfff;">
		<td align="center" style="width:80px;"><font class="j12"><span>氏名</span></font></td>
		<td align="center" style="width:40px;"><font class="j12"><span>性別</span></font></td>
		<td align="center" style="width:40px;"><font class="j12"><span>年齢</span></font></td>
		<td align="center" style="width:50px;"><font class="j12"><span>入外別</span></font></td>
		<td align="center" style="width:90px;"><font class="j12"><span>診療科</span></font></td>
		<td align="center" style="width:90px;"><font class="j12"><span>主たる病名</span></font></td>
	</tr>
	<? if($serch=="1"){ ?>
		<? if($result[0]["title"]==""){ ?>
		<? //検索結果が0件 ?>
			<tr>
				<td colspan="12" align="center">データが存在しません</td>
			</tr>
		<? }else{ ?>
		<? //検索結果が1件以上 ?>
			<? $count=0 ?>
			<? foreach($result as $idx => $row){ ?>
			<tr>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center"><font class="j12"><?=$row["occurrences_date"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center"><font class="j12"><?=$row["report_date"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left"><font class="j12"><?=$row["title"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left"><font class="j12"><?=$row["outline_nm"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left"><font class="j12"><?=$row["patient_name"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center"><font class="j12"><?=$row["sex_nm"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center"><font class="j12"><?=$row["patient_age"]."歳"?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center"><font class="j12"><?=$row["hos_vis_nm"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center"><font class="j12"><?=$row["hos_dep_nm"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left"><font class="j12"><?=$row["main_disease"]?></font></td>

				<? //hiddenにデータ格納 ?>
				<input type="hidden" id="hdn_arize_date_<?=$count?>" value="<?=$row["occurrences_date"]?>" />
				<input type="hidden" id="hdn_hospital_<?=$count?>" value="<?=$row["hospital_id"]?>" />
				<input type="hidden" id="hdn_requirements1_<?=$count?>" value="<?=$row["requirements1"]?>" />
				<input type="hidden" id="hdn_requirements2_<?=$count?>" value="<?=$row["requirements2"]?>" />
				<input type="hidden" id="hdn_requirements3_<?=$count?>" value="<?=$row["requirements3"]?>" />
				<input type="hidden" id="hdn_patient_name_<?=$count?>" value="<?=$row["patient_name"]?>" />
				<input type="hidden" id="hdn_sex_<?=$count?>" value="<?=$row["patient_sex"]?>" />
				<input type="hidden" id="hdn_age_<?=$count?>" value="<?=$row["patient_age"]?>" />
				<input type="hidden" id="hdn_hos_vis_kbn_<?=$count?>" value="<?=$row["patient_class"]?>" />
				<input type="hidden" id="hdn_hospital_dept_<?=$count?>" value="<?=$row["clinical_departments"]?>" />
				<input type="hidden" id="hdn_disease_<?=$count?>" value="<?=$row["main_disease"]?>" />
				<input type="hidden" id="hdn_title_<?=$count?>" value="<?=$row["title"]?>" />
				<input type="hidden" id="hdn_outline_<?=$count?>" value="<?=$row["outline"]?>" />
				<input type="hidden" id="hdn_fact_prog_<?=$count?>" value="<?=$row["fact_progress"]?>" />
				<input type="hidden" id="hdn_aci_cor_<?=$count?>" value="<?=$row["accident_cope"]?>" />
				<input type="hidden" id="hdn_family_cor_<?=$count?>" value="<?=$row["patient_family_cope"]?>" />
				<input type="hidden" id="hdn_gene_cause_<?=$count?>" value="<?=$row["occurrences_factor"]?>" />
				<input type="hidden" id="hdn_link_id_<?=$count?>" value="<?=$row["medical_accident_id"]?>" />
				<input type="hidden" id="hdn_report_no_<?=$count?>" value="<?=$row["report_no"]?>" />
			</tr>
			<? $count++; ?>
			<?}?>
		<? } ?>
	<? } ?>
</table>
<? //検索結果一覧表示　ここまで-------------- ?>
</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
