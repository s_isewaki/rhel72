<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>委員会・WG | スケジュール（年）</title>
<script LANGUAGE="JavaScript">
<!--
function goLastMonth(month, year,set_date){
<? echo("var today=".$date.";"); ?>
--today;
var set_date = set_date;
document.location.href = "./project_schedule_year_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date="+today+"&s_date="+set_date;
}
function goNextMonth(month, year){
<? echo("var today=".$date.";"); ?>
var set_date = set_date;
++today;
document.location.href = "./project_schedule_year_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date="+today+"&s_date="+set_date;
}
//-->
</script>
<?
$fname=$PHP_SELF;
require("./conf/sql.inf");						//sqlの一覧ファイルを読み込む
require("./about_postgres.php");			//db処理のためのファイルを読み込む
require("./show_select_values.ini");
require("./about_session.php");
require("./about_authority.php");
require("./show_schedule_to_do.ini");
require("./time_check3.ini");
require("./show_date_navigation_year.ini");
require("show_class_name.ini");

$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$pro = check_authority($session,31,$fname);
if($pro == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

if(!isset($_REQUEST['s_date'])){
$date1 = mktime(0,0,0,s_date('m'), s_date('d'), s_date('Y'));
}else{
$date1 = $_REQUEST['s_date'];
}
$dayx = date('d', $date1);
$monthx = date('m', $date1);
$yearx = date('Y', $date1);


$day = "01";
$month = date('m');
$year =$date;
$todayday = date('d');
$todaymonth = date('m');
$todayyear = date('Y');

$con = connect2db($fname);

$arr_class_name = get_class_name_array($con, $fname);

$cond = " where session_id='$session'";
$sel = select_from_table($con,$SQL1,$cond,$fname);

if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

//--0217追加ここから---
$emp_id=pg_result($sel,0,"emp_id");

//--0302追加ここから---
$cond_pjt = " where pjt_id='$pjt_id'";
$sel_pjt = select_from_table($con,$SQL165,$cond_pjt,$fname);

if($sel_pjt==0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$project_name=pg_result($sel_pjt,0,"pjt_name");
//--0302追加ここまで---

if($timegd == "on" || $timegd == "off"){
	time_update($con,$emp_id,$timegd,$fname);
}

if($timegd == ""){
	$timegd = time_check($con,$emp_id,$fname);
}
//--0217追加ここまで---

// 開始時刻のデフォルトは9時
if ($hr == "") {
	$hr = "9";
}

// 選択プロジェクトのスケジュール（開始日）を取得
$sql = "select pjt_schd_start_date from proschd where pjt_id = '$pjt_id' union select pjt_schd_start_date from proschd2 where pjt_id = '$pjt_id'";
$cond = "";
$sel_schd = select_from_table($con, $sql, $cond, $fname);
if ($sel_schd == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// 取得したスケジュール（開始日）を配列に保存（YYYY-MM-DD形式）
$arr_schd = array();
while ($row = pg_fetch_array($sel_schd)) {
	array_push($arr_schd, $row["pjt_schd_start_date"]);
}

echo("<script language=\"javascript\">\n");
echo("function clickClass(){\n");
echo("var c_id=document.search.cls.value;\n");
echo("var a_len=document.search.atrb.length;\n");
echo("var d_len=document.search.dept.length;\n");

//----------class_idを取得----------
$cond_class="order by class_id";
$sel_class=select_from_table($con,$SQL22,$cond_class,$fname);
if($sel_class==0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
//----------atrb_idを取得----------
$rows_class=pg_numrows($sel_class);

echo("if(c_id==0){\n");
echo("document.search.atrb.options[0].selected=true;\n");

echo("if(a_len>1){\n");
echo("for(k=1;k<a_len;k++){\n");
echo("document.search.atrb.options[k]=new Option(\"\",k);\n");
echo("document.search.atrb.options[k].value=null;\n");
echo("}\n");
echo("}\n");

echo("if(d_len > 1){\n");
echo("for(k=1;k<d_len;k++){\n");
echo("document.search.dept.options[k]=new Option(\"\",k);\n");
echo("document.search.dept.options[k].value=null;\n");
echo("}\n");
echo("}\n");
echo("}\n");

for($i=0;$i<$rows_class;$i++){
	$class_id=pg_result($sel_class,$i,"class_id");
	$cond_atrb="where class_id = '$class_id'";
	$sel_atrb=select_from_table($con,$SQL8,$cond_atrb,$fname);
	if($sel_atrb==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
		echo("var len_check=".$total_rows.";\n");
		echo("if(a_len>len_check){\n");
		echo("for(k=len_check;k<a_len;k++){\n");
		echo("document.search.atrb.options[k]=new Option(\"\",k);\n");
		echo("document.search.atrb.options[k].value=null;\n");
		echo("}\n");
		echo("}\n");
	}

	echo("if(c_id==".$class_id."){\n");

	echo("document.search.atrb.options[0]=new Option(\"すべて\",0);\n");
	echo("document.search.atrb.options[0].value=0;\n");

	$rows_atrb=pg_numrows($sel_atrb);
	echo("var len_check=0;\n");
	for($k=0;$k<$rows_atrb;$k++){
		$atrb_id=pg_result($sel_atrb,$k,"atrb_id");
		$atrb_nm=pg_result($sel_atrb,$k,"atrb_nm");
		$j=$k+1;
		echo("document.search.atrb.options[".$j."]=new Option(\"".$atrb_nm."\",".$j.");\n");
		echo("document.search.atrb.options[".$j."].value=".$atrb_id.";\n");
	}

	$total_rows=$rows_atrb+1;

	echo("var len_check=".$total_rows.";\n");
	echo("if(a_len>len_check){\n");
	echo("for(k=len_check;k<a_len;k++){\n");
	echo("document.search.atrb.options[k]=new Option(\"\",k);\n");
	echo("document.search.atrb.options[k].value=null;\n");
	echo("}\n");
	echo("}\n");

	echo("if(d_len>1){\n");
	echo("for(k=1;k<d_len;k++){\n");
	echo("document.search.dept.options[k]=new Option(\"\",k);\n");
	echo("document.search.dept.options[k].value=null;\n");
	echo("}\n");
	echo("}\n");

echo("}\n");
}
echo("}\n");

echo("function clickAtrb(){\n");
echo("var c_id=document.search.cls.value;\n");
echo("var a_id=document.search.atrb.value;\n");
echo("var d_len=document.search.dept.length;\n");
//----------class_idを取得----------
$cond_class="order by class_id";
$sel_class=select_from_table($con,$SQL22,$cond_class,$fname);
if($sel_class==0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

//----------atrb_idを取得----------
$rows_class=pg_numrows($sel_class);
for($j=0;$j<$rows_class;$j++){
	$class_id=pg_result($sel_class,$j,"class_id");
	$cond_atrb="where class_id = '$class_id'";
	$sel_atrb=select_from_table($con,$SQL8,$cond_atrb,$fname);
	if($sel_atrb==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
//----------dept_idを取得----------
	$rows_atrb=pg_numrows($sel_atrb);

	echo("if(a_id==0){\n");
		echo("if(d_len>1){\n");
			echo("for(k=1;k<d_len;k++){\n");
				echo("document.search.dept.options[k]=new Option(\"\",k);\n");
				echo("document.search.dept.options[k].value=null;\n");
			echo("}\n");
		echo("}\n");
	echo("}\n");

	for($i=0;$i<$rows_atrb;$i++){
		$atrb_id=pg_result($sel_atrb,$i,"atrb_id");
		$cond_dept="where atrb_id = '$atrb_id'";
		$sel_dept=select_from_table($con,$SQL5,$cond_dept,$fname);
		if($sel_dept==0){
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		echo("if(c_id==".$class_id." && a_id == ".$atrb_id."){\n");

		echo("document.search.dept.options[0]=new Option(\"すべて\",0);\n");
		echo("document.search.dept.options[0].value=0;\n");

		$rows_dept=pg_numrows($sel_dept);
		echo("var len_check=0;\n");
		for($h=0;$h<$rows_dept;$h++){
			$dept_id=pg_result($sel_dept,$h,"dept_id");
			$dept_nm=pg_result($sel_dept,$h,"dept_nm");
			$l=$h+1;
			echo("document.search.dept.options[".$l."]=new Option(\"".$dept_nm."\",".$l.");\n");
			echo("document.search.dept.options[".$l."].value=".$dept_id.";\n");
		}

		$total_rows=$rows_dept+1;

		echo("var len_check=".$total_rows.";\n");
		echo("if(d_len>len_check){\n");
		echo("for(k=len_check;k<d_len;k++){\n");
		echo("document.search.dept.options[k]=new Option(\"\",k);\n");
		echo("document.search.dept.options[k].value=null;\n");
		echo("}\n");
		echo("}\n");

		echo("}\n");
	}
}
echo("}\n");

echo("</script>\n");

echo("<script language=\"javascript\">\n");
echo("function showGuide(){\n");
	echo("var ses = \"".$session."\";\n");

	$sd =mktime(0,0,0,$monthx,$dayx,$yearx);

	echo("var dt  = ".$year.";\n");
	echo("var sdt  = ".$sd.";\n");
	echo("var pjt_id = ".$pjt_id.";\n");
	echo("if(document.timegd.timeguide.checked == true){\n");
		echo("var url = \"project_schedule_year_menu.php?session=\"+ses+\"&pjt_id=\"+pjt_id+\"&date=\"+dt+\"&s_date=\"+sdt+\"&timegd=on\";\n");
		echo("location.href=url;\n");
	echo("}else{\n");
		echo("var url = \"project_schedule_year_menu.php?session=\"+ses+\"&pjt_id=\"+pjt_id+\"&date=\"+dt+\"&s_date=\"+sdt+\"&timegd=off\";\n");
		echo("location.href=url;\n");
	echo("}\n");
echo("}\n");
echo("</script>\n");

// 「前年」「翌年」のタイムスタンプを取得
$last_year = get_last_year($date1);
$next_year = get_next_year($date1);

// オプション設定情報を取得
$sql = "select calendar_start2 from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$calendar_start = pg_fetch_result($sel, 0, "calendar_start2");

// 委員会・WK管理権限を取得
$pjt_admin_auth = check_authority($session, 82, $fname);

?>
<script language="javascript">
function changeDate(dt) {
	var arr_dt = dt.split(',');
	location.href = 'project_schedule_year_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=' + arr_dt[0] + '&s_date=' + arr_dt[1];
}
</script>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" colspan="2" class="spacing"><a href="project_menu.php?session=<? echo($session); ?>"><img src="img/icon/b03.gif" width="32" height="32" border="0" alt="委員会・WG"></a></td>

<? if ($pjt_admin_auth == "1") { ?>

	<? if($adm_flg=="t") {?>
		<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="project_menu.php?session=<? echo($session); ?>&entity=1"><b>ユーザ画面へ</b></a></font></td>
	<? }else{ ?>
		<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">　<a href="project_menu_adm.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
	<? } ?>

<? }else{ ?>
		<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>

<? } ?>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>

<? if($adm_flg=="t") {?>
	<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu_adm.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<? }else{ ?>
	<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<? } ?>

<td width="5"></td>
<td width="120" align="center" bgcolor="#5279a5"><a href="./project_schedule_year_menu.php?session=<? echo($session); ?>&date=<? echo($year); ?>&s_date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>&pjt_id=<? echo($pjt_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>スケジュール</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5" colspan="10"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="./img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo($project_name); ?></b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="./project_schedule_menu.php?session=<? echo($session); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="./project_schedule_week_menu.php?session=<? echo($session); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="./project_schedule_month_menu.php?session=<? echo($session); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#5279a5"><a href="./project_schedule_year_menu.php?session=<? echo($session); ?>&date=<? echo($year); ?>&s_date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>年</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="./project_schedule_list_upcoming.php?session=<? echo($session); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="project_schedule_option.php?session=<? echo($session); ?>&date=<? echo(mktime(0,0,0,$month,$day,$year)); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="timegd">
<?
error_reporting('0');
ini_set('display_errors', '0');

$day = "01";
$month = "01";
$year = $date;
$set_date = mktime(0,0,0,$month,$day,$year);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="./project_schedule_year_menu.php?session=<? echo($session); ?>&date=<? echo(date("Y", $last_year)); ?>&s_date=<? echo($last_year); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>">&lt;前年</a>&nbsp;<select onchange="changeDate(this.options[this.selectedIndex].value);"><? show_date_options_y($last_year, $date1, $next_year); ?></select>&nbsp;<a href="./project_schedule_year_menu.php?session=<? echo($session); ?>&date=<? echo(date("Y", $next_year)); ?>&s_date=<? echo($next_year); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>">翌年&gt;</a></font></td>
<!--
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">院内行事</font><input type="checkbox" name="timeguide" onClick="showGuide()"<? if ($timegd == "on") {echo("checked");} ?>></td>
-->
</tr>
</table>
</form>
<?
echo("<table width=\"750\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");

$tp = 0;
for($p=0; $p<4; $p++){
	$tp = $tp+1;
	echo("<tr valign=\"top\">\n");

	for($z=0; $z<3; $z++){

		$month_start = mktime(0,0,0,$month, 01, $year);

		$month_name = date('m', $month_start);

		$month_start_day = date('D', $month_start);

		switch($month_start_day){
			case "Sun": $offset = ($calendar_start == 1) ? 0 : 6; break;
			case "Mon": $offset = ($calendar_start == 1) ? 1 : 0; break;
			case "Tue": $offset = ($calendar_start == 1) ? 2 : 1; break;
			case "Wed": $offset = ($calendar_start == 1) ? 3 : 2; break;
			case "Thu": $offset = ($calendar_start == 1) ? 4 : 3; break;
			case "Fri": $offset = ($calendar_start == 1) ? 5 : 4; break;
			case "Sat": $offset = ($calendar_start == 1) ? 6 : 5; break;
		}

		if($month == 1){
			$num_days_last = 0;
			$num_days_last = cal_days_in_month(0, 12, ($year -1));
		}else{
			$num_days_last = 0;
			$num_days_last = cal_days_in_month(0, ($month -1), $year);
		}
		$num_days_current = 0;
		$num_days_current = cal_days_in_month(0, $month, $year);

		$num_days_array = array();
		for($i=1; $i<=$num_days_current; $i++){
			$num_days_array[] = $i;
		}

		$num_days_last_array = array();
		for($i=1; $i<=$num_days_last; $i++){
			$num_days_last_array[] = $i;
		}

		if($offset > 0){
			$offset_correction = array_slice($num_days_last_array, -$offset, $offset);
			$new_count = array_merge($offset_correction, $num_days_array);
			$offset_count = count($offset_correction);
		}else{
			$offset_count = 0;
			$new_count = $num_days_array;
		}

		$current_num = count($new_count);

		if($current_num > 35){
			$num_weeks = 6;
			$outset = (42 - $current_num);
		}elseif($current_num < 35){
			$num_weeks = 5;
			$outset = (35 - $current_num);
		}

		if($current_num == 35){
			$num_weeks = 5;
			$outset = 0;
		}
		for($i=1; $i<=$outset; $i++){
			$new_count[] = $i;
		}

		$weeks = array_chunk($new_count, 7);

	  	echo("<td colspan=\"7\">\n");

echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-color: #5279a5;border-width:1px;border-style:solid;\">\n");
echo("<tr>\n");
 echo("<td height=\"22\" colspan=\"7\" bgcolor=\"f6f9ff\">\n");
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
echo($year);
echo("/");
echo($month_name);
echo("</font>");
echo("</td>\n");
echo("</tr>\n");	//追加
echo("<tr>\n");
echo("<td height=\"22\" colspan=\"7\">");
echo("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"0\">\n");
echo("<tr>\n");
if ($calendar_start == 1) {
	echo("<td align=\"center\" bgcolor=\"#fadede\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日</font></td>\n");
}
echo("<td align=\"center\" bgcolor=\"#bdd1e7\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">月</font></td>\n");
echo("<td align=\"center\" bgcolor=\"#bdd1e7\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">火</font></td>\n");
echo("<td align=\"center\" bgcolor=\"#bdd1e7\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">水</font></td>\n");
echo("<td align=\"center\" bgcolor=\"#bdd1e7\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">木</font></td>\n");
echo("<td align=\"center\" bgcolor=\"#bdd1e7\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">金</font></td>\n");
echo("<td align=\"center\" bgcolor=\"#defafa\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">土</font></td>\n");
if ($calendar_start == 2) {
	echo("<td align=\"center\" bgcolor=\"#fadede\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日</font></td>\n");
}
echo("</tr>\n");
echo("</table>\n");
echo("</td>\n");
echo("</tr>\n");

			$i = 0;
			foreach($weeks AS $week){
				echo("<tr>\n");
				foreach($week as $d){
					if($i < $offset_count){
						$day_link = "<a href=\"".$_SERVER['PHP_SELF']."?date=".mktime(0,0,0,$month -1,$d,$year)."\">$d</a>";
						echo("<td>&nbsp;</td>\n");
					}

					if(($i >= $offset_count) && ($i < ($num_weeks * 7) - $outset)){


					$temp_day = $d;
					if (strlen($d) == 1) {
						$temp_day = "0$d";
					}
					if (in_array("$year-$month-$temp_day", $arr_schd)) {
						$cell_color = "#fbdade";
					} else {
						$cell_color = "#ffffff";
					}

					$day_link = "<a href=\"./project_schedule_menu.php?session=$session&pjt_id=$pjt_id&adm_flg=$adm_flg&date=".mktime(0,0,0,$month,$d,$year)."\">$d</a>";
					echo("<td align=\"center\" bgcolor=\"$cell_color\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$day_link</fonr></td>\n");

					}elseif(($outset > 0)){
						if(($i >= ($num_weeks * 7) - $outset)){
							/*if(strlen($d)==1){
								$d="0".$d;
							}*/
							$day_link = "<a href=\"./project_schedule_menu.php?session=$session&pjt_id=$pjt_id&adm_flg=$adm_flg&date=".mktime(0,0,0,$month,$d,$year)."\">$d</a>";
							echo("<td>&nbsp;</td>\n");
						}
					}
				$i++;
				}
				echo("</tr>\n");
			}

			$month=$month+1;
			if(strlen($month)==1){
				$month="0".$month;
			}

echo("</table></td><td width=\"5\"><img src=\"img/spacer.gif\" width=\"5\" height=\"1\" alt=\"\"></td>");
		$tp=$tp+1;
	}
echo("</tr>\n");
echo("<tr>\n");
echo("<td colspan=\"3\">\n");
echo("<img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
echo("</td>\n");
echo("</tr>\n");
}
echo("</table>\n");

?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
