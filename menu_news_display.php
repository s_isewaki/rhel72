<?
require("about_session.php");
require("about_authority.php");
require("news_common.ini");
require_once("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 46, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// 前回指定ステータスを更新
update_newsempoption($con, $fname, $emp_id, "", $sele_stat);

// データベース接続を閉じる
pg_close($con);

// トップページを再表示
echo("<script type=\"text/javascript\">location.href = 'left.php?session=$session';</script>");
?>
