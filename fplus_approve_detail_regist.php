<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("fplus_common_class.php");

$fname=$PHP_SELF;


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 78, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$obj = new fplus_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");


if($approve_mode == "UPDATE")
{
	//レポートの種類
	//"Episys107A"-エピネット報告書Ａ針刺し・切創
	//"Episys107B"-エピネット報告書Ｂ皮膚・粘膜汚染
	if($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107A")
	{
		
		//報告書上にdisableの項目があります。その場合に、以前作成されたデータが今回disbleになった場合に
		//$_REQUESTで取得できない（データが上書きされない）ので、
		//一旦	$apply_idでデータを削除してからレコードを正しいデータで再作成します。
		$sql=	"delete from fplus_epi_a ";		
		$cond = "where apply_id = $apply_id";
		$sel = delete_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		$set_data = array();
		$set_key = array();
		$set_data[0] = $apply_id;
		$set_key[0] = "apply_id";
		
		$wcnt0 = 1;
		
		foreach($_REQUEST as $epi_key => $epi_val)
		{
			$chk_key = substr($epi_key,0, 5);
			
			if($chk_key == "data_")
			{
				//エピネット報告書の入力データである
				
				//$set_data[$wcnt0] = $epi_val;
				$set_data[$wcnt0] = pg_escape_string($epi_val);
				$set_key[$wcnt0] = $epi_key;
				$wcnt0++;
			}
		}
		
		
		//集計用データの登録処理
		$obj->regist_Episys107A($set_key,$set_data);
		
		//集計用データの登録処理
		//$obj->update_Episys107A($apply_id,$set_key,$set_data);
		
		
	}
	else if(($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107B")|| ($MRSA_REPORT == "MRSA"))
	{

		//報告書上にdisableの項目があります。その場合に、以前作成されたデータが今回disbleになった場合に
		//$_REQUESTで取得できない（データが上書きされない）ので、
		//一旦	$apply_idでデータを削除してからレコードを正しいデータで再作成します。
		if($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107B")
		{ 
			$sql=	"delete from fplus_epi_b ";		
			$cond = "where apply_id = $apply_id";
			$sel = delete_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		elseif($MRSA_REPORT == "MRSA")
		{
			//MRSA等耐性菌検出報告書
			
			$sql=	"delete from fplus_infection_mrsa ";		
			$cond = "where apply_id = $apply_id";
			$sel = delete_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		
		
		$set_data = array();
		$set_key = array();
		$set_data[0] = $apply_id;
		$set_key[0] = "apply_id";
		
		$wcnt0 = 1;
		
		foreach($_REQUEST as $epi_key => $epi_val)
		{
			$chk_key = substr($epi_key,0, 5);
			
			if($chk_key == "data_")
			{
				if(is_array($epi_val))
				{
					//エピネット報告書の入力データである(checkbox用)
					
					//$set_data[$wcnt0] = $epi_val[0];
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
				else
				{
					//エピネット報告書の入力データである
					
					//$set_data[$wcnt0] = $epi_val;
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
		
		
		if($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107B")
		{ 
			//集計用データの登録処理
			$obj->regist_Episys107B($set_key,$set_data);
		
		}
		elseif($MRSA_REPORT == "MRSA")
		{
			//集計用データの登録処理(MRSA等耐性菌検出報告書)
			$obj->regist_MRSA($set_key,$set_data);
		}
		

		//集計用データの登録処理
		//$obj->update_Episys107B($apply_id,$set_key,$set_data);
		
		
	}else if($IMMEDIATE_REPORT == "Immediaterepo"){
	
		//緊急報告受付

		//検索処理
		$sql=	"SELECT immediate_report_id, medical_accident_id, report_no FROM fplus_immediate_report ";
		$cond = "where apply_id = $apply_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//レポートID、リンクID、報告�發琉�き継ぎ
		$report_id = pg_fetch_result($sel, 0, "immediate_report_id");
		$link_id = pg_fetch_result($sel, 0, "medical_accident_id");
		$report_no = pg_fetch_result($sel, 0, "report_no");

		//デリート処理
		$sql=	"delete from fplus_immediate_report ";		
		$cond = "where apply_id = $apply_id";
		$sel = delete_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//インサート処理
		//緊急報告受付テーブル登録項目ここから
		$set_data = array();
		$set_key = array();
		$set_data[0] = $apply_id;
		$set_key[0] = "apply_id";
		$set_data[1] = $report_id;
		$set_key[1] = "immediate_report_id";
		$set_data[2] = $link_id;
		$set_key[2] = "medical_accident_id";
		$set_data[3] = $report_no;
		$set_key[3] = "report_no";
		$set_data[4] = date("Y-m-d H:i:s");
		$set_key[4] = "update_time";
		$set_data[5] = "f";
		$set_key[5] = "del_flg";

		$wcnt0 = 6;
		
		foreach($_REQUEST as $epi_key => $epi_val)
		{
			$chk_key = substr($epi_key,0, 5);
			
			if($chk_key == "data_")
			{
				$epi_key = substr($epi_key,5);
				if(is_array($epi_val))
				{
					//入力データcheckbox,radio用
					if($epi_val[0]!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
				else
				{
					//入力データ
					if($epi_val!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
			}
		}

		//集計用データの登録処理(緊急報告受付)
		$obj->regist_immediate_repo($set_key,$set_data);
	
	}else if($FIRST_REPORT == "Firstrepo"){
	
		//第一報

		//検索処理
		$sql=	"SELECT first_report_id, medical_accident_id FROM fplus_first_report ";
		$cond = "where apply_id = $apply_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//レポートID、リンクID、報告�發琉�き継ぎ
		$report_id = pg_fetch_result($sel, 0, "first_report_id");
		$link_id = pg_fetch_result($sel, 0, "medical_accident_id");
		$report_no = pg_fetch_result($sel, 0, "report_no");

		//デリート処理
		$sql=	"delete from fplus_first_report ";
		$cond = "where apply_id = $apply_id";
		$sel = delete_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//インサート処理
		$set_data = array();
		$set_key = array();
		$set_data[0] = $apply_id;
		$set_key[0] = "apply_id";
		$set_data[1] = $report_id;
		$set_key[1] = "first_report_id";
		$set_data[2] = $link_id;
		$set_key[2] = "medical_accident_id";
		$set_data[3] = $report_no;
		$set_key[3] = "report_no";
		$set_data[4] = date("Y-m-d H:i:s");
		$set_key[4] = "update_time";
		$set_data[5] = "f";
		$set_key[5] = "del_flg";

		$wcnt0 = 6;
		
		foreach($_REQUEST as $epi_key => $epi_val)
		{
			$chk_key = substr($epi_key,0, 5);
			
			if($chk_key == "data_")
			{
				$epi_key = substr($epi_key,5);
				if(is_array($epi_val))
				{
					//入力データcheckbox,radio用
					if($epi_val[0]!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
				else
				{
					//入力データ
					if($epi_val!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
			}
		}

		//集計用データの登録処理(第一報)
		$obj->regist_first_repo($set_key,$set_data);

	}else if($SECOND_REPORT == "Secondrepo"){
	
		//様式２

		//第一報リンクの有無を調べるここから
		$sql= " SELECT ".
			" 	'first' as table_id ".
			" FROM ".
			" 	fplus_second_report s ".
			" 	, fplus_first_report f ".
			" 	, fplusapply app1 ".
			" 	, fplusapply app2 ".
			" WHERE ".
			" 	s.medical_accident_id = f.medical_accident_id ".
			" 	AND s.apply_id = app1.apply_id ".
			" 	AND f.apply_id = app2.apply_id ".
			" 	AND app1.delete_flg = 'f' ".
			" 	AND app2.delete_flg = 'f' ".
			" 	AND s.apply_id = '$apply_id' ".

		$cond = "";
		$sel_link = select_from_table($con, $sql, $cond, $fname);
		$link_data = pg_fetch_all($sel_link);

		$first_flg = 0;
		foreach($link_data as $idx => $row){
			if($row["table_id"] == "first"){
				$first_flg = 1;
			}
		}
		//リンクの有無を調べるここまで

		//検索処理
		$sql=	"SELECT second_report_id, medical_accident_id, report_no FROM fplus_second_report ";
		$cond = "where apply_id = $apply_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//レポートID、リンクID、報告�發琉�き継ぎ(リンクIDは変更がない場合のみ引き継ぐ)
		$report_id = pg_fetch_result($sel, 0, "second_report_id");
		$old_link_id = pg_fetch_result($sel, 0, "medical_accident_id");
		$report_no = pg_fetch_result($sel, 0, "report_no");

		//デリート処理
		$sql=	"delete from fplus_second_report ";		
		$cond = "where apply_id = $apply_id";
		$sel = delete_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		if($hdn_link_id==""){
			// 新規リンク�盧糧�
			$sql = "SELECT max(medical_accident_id) FROM fplus_medical_accident_report_management";
			$cond = "";
			$link_id_sel = select_from_table($con,$sql,$cond,$fname);
			$link_id = pg_result($link_id_sel,0,"max");
			if($link_id == ""){
				$link_id = 1;
			}else{
				$link_id = $link_id + 1;
			}

			//リンクテーブル登録項目ここから
			$set_data_link = array();
			$set_key_link = array();
			$set_data_link[0] = $link_id;
			$set_key_link[0] = "medical_accident_id";
			$set_data_link[1] = date("Y-m-d H:i:s");
			$set_key_link[1] = "update_time";
			$set_data_link[2] = "f";
			$set_key_link[2] = "del_flg";
			//リンクテーブル登録項目ここまで
		}else{
			//引用した第一報のリンクID
			$link_id = $hdn_link_id;
		}

		//第二報登録項目
		$set_data = array();
		$set_key = array();
		$set_data[0] = $apply_id;
		$set_key[0] = "apply_id";
		$set_data[1] = $report_id;
		$set_key[1] = "second_report_id";
		$set_data[2] = $link_id;
		$set_key[2] = "medical_accident_id";
		$set_data[3] = $report_no;
		$set_key[3] = "report_no";
		$set_data[4] = date("Y-m-d H:i:s");
		$set_key[4] = "update_time";
		$set_data[5] = "f";
		$set_key[5] = "del_flg";

		$wcnt0 = 6;
		
		foreach($_REQUEST as $epi_key => $epi_val)
		{
			$chk_key = substr($epi_key,0, 5);
			$check .= $chk_key.",";
			if($chk_key == "data_")
			{
				$epi_key = substr($epi_key,5);
				if(is_array($epi_val))
				{
					//入力データcheckbox,radio用
					if($epi_val[0]!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
				else
				{
					//入力データ
					if($epi_val!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
			}
		}

		//リンクテーブル操作ここから
		if($hdn_link_id==""){
			//リンクテーブルの登録処理(様式２)
			$obj->regist_link_table($set_key_link,$set_data_link);
		}else{
			if($first_flg == 0){
				//リンクテーブルデータの削除(論理削除)

				//リンクテーブル登録項目ここから
				$set_data_link = array();
				$set_key_link = array();
				$set_data_link[0] = $old_link_id;
				$set_key_link[0] = "medical_accident_id";
				$set_data_link[1] = date("Y-m-d H:i:s");
				$set_key_link[1] = "update_time";
				$set_data_link[2] = "t";
				$set_key_link[2] = "del_flg";
				//リンクテーブル登録項目ここまで

				$obj->update_medical_accident_report_management($old_link_id,$set_key,$set_data);
			}
		}
		//リンクテーブル操作ここまで

		//第二報データの登録処理(様式２)
		$obj->regist_second_repo($set_key,$set_data);

	}else if($CONSULTATION_REQUEST == "Consreq"){

		//検索処理
		$sql=	"SELECT consultation_request_id, report_no FROM fplus_consultation_request ";
		$cond = "where apply_id = $apply_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//レポートID、報告�發琉�き継ぎ
		$report_id = pg_fetch_result($sel, 0, "consultation_request_id");
		$report_no = pg_fetch_result($sel, 0, "report_no");

		//"コンサルテーション依頼書
		//デリート処理
		$sql=	"delete from fplus_consultation_request ";		
		$cond = "where apply_id = $apply_id";
		$sel = delete_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		if($hdn_link_id!=""){
			$link_id = NULL;
		}else{
			//引用した第一報のリンクID
			$link_id = $hdn_link_id;
		}
		$link_kbn = "";
		if($hdn_link_report_kbn!=""){
			//引用した第一報のリンクID
			$link_kbn = $hdn_link_report_kbn;
		}

		//インサート処理
		$set_data = array();
		$set_key = array();
		$set_data[0] = $apply_id;
		$set_key[0] = "apply_id";
		$set_data[1] = $report_id;
		$set_key[1] = "consultation_request_id";
		$set_data[2] = $report_no;
		$set_key[2] = "report_no";
		$set_data[3] = $link_id;
		$set_key[3] = "report_id";
		$set_data[4] = $link_kbn;
		$set_key[4] = "report_type";
		$set_data[5] = date("Y-m-d H:i:s");
		$set_key[5] = "update_time";
		$set_data[6] = "f";
		$set_key[6] = "del_flg";

		$wcnt0 = 7;
		
		foreach($_REQUEST as $epi_key => $epi_val)
		{
			$chk_key = substr($epi_key,0, 5);
			
			if($chk_key == "data_")
			{
				$epi_key = substr($epi_key,5);
				if(is_array($epi_val))
				{
					//入力データcheckbox,radio用
					if($epi_val[0]!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
				else
				{
					//入力データ
					if($epi_val!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
			}
		}

		//集計用データの登録処理("コンサルテーション依頼書)
		$obj->regist_EpisysConsreq($set_key,$set_data);

	}else if($CONSULTATION_REPORT == "Consrep"){
	
		//"コンサルテーション報告書

		//検索処理
		$sql=	"SELECT consultation_report_id, report_no FROM fplus_consultation_report ";
		$cond = "where apply_id = $apply_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//レポートID、報告�發琉�き継ぎ
		$report_id = pg_fetch_result($sel, 0, "consultation_report_id");
		$report_no = pg_fetch_result($sel, 0, "report_no");

		//デリート処理
		$sql=	"delete from fplus_consultation_report ";		
		$cond = "where apply_id = $apply_id";
		$sel = delete_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$consultation_request_id = "";
		if($hdn_consultation_request_id!=""){
			$consultation_request_id = NULL;
		}else{
			//引用したレポートID
			$consultation_request_id = $hdn_consultation_request_id;
		}

		//インサート処理
		$set_data = array();
		$set_key = array();
		$set_data[0] = $apply_id;
		$set_key[0] = "apply_id";
		$set_data[1] = $report_id;
		$set_key[1] = "consultation_report_id";
		$set_data[2] = $report_no;
		$set_key[2] = "report_no";
		$set_data[3] = $consultation_request_id;
		$set_key[3] = "consultation_request_id";
		$set_data[4] = date("Y-m-d H:i:s");
		$set_key[4] = "update_time";
		$set_data[5] = "f";
		$set_key[5] = "del_flg";

		$wcnt0 = 6;
		
		foreach($_REQUEST as $epi_key => $epi_val)
		{
			$chk_key = substr($epi_key,0, 5);
			
			if($chk_key == "data_")
			{
				$epi_key = substr($epi_key,5);
				if(is_array($epi_val))
				{
					//入力データcheckbox,radio用
					if($epi_val[0]!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
				else
				{
					//入力データ
					if($epi_val!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
			}
		}

		//集計用データの登録処理("コンサルテーション報告書)
		$obj->regist_EpisysConsrep($set_key,$set_data);

	}else if($MEDICAL_APPARATUS_FAULT_REPORT == "medicalapparatusfaultreport"){
	
		//医療機器不具合報告

		//検索処理
		$sql=	"SELECT medical_apparatus_fault_report_id, report_no FROM fplus_medical_apparatus_fault_report ";
		$cond = "where apply_id = $apply_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//レポートID、報告�發琉�き継ぎ
		$report_id = pg_fetch_result($sel, 0, "medical_apparatus_fault_report_id");
		$report_no = pg_fetch_result($sel, 0, "report_no");

		//デリート処理
		$sql=	"delete from fplus_medical_apparatus_fault_report ";		
		$cond = "where apply_id = $apply_id";
		$sel = delete_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$consultation_request_id = "";
		if($hdn_consultation_request_id!=""){
			//引用したレポートID
			$consultation_request_id = $hdn_consultation_request_id;
		}

		//インサート処理
		$set_data = array();
		$set_key = array();
		$set_data[0] = $apply_id;
		$set_key[0] = "apply_id";
		$set_data[1] = $report_id;
		$set_key[1] = "medical_apparatus_fault_report_id";
		$set_data[2] = $report_no;
		$set_key[2] = "report_no";
		$set_data[3] = date("Y-m-d H:i:s");
		$set_key[3] = "update_time";
		$set_data[4] = "f";
		$set_key[4] = "del_flg";

		$wcnt0 = 5;
		
		foreach($_REQUEST as $epi_key => $epi_val)
		{
			$chk_key = substr($epi_key,0, 5);
			
			if($chk_key == "data_")
			{
				$epi_key = substr($epi_key,5);
				if(is_array($epi_val))
				{
					//入力データcheckbox,radio用
					if($epi_val[0]!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
				else
				{
					//入力データ
					if($epi_val!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
			}
		}

		//データの登録処理(医療機器不具合報告)
		$obj->regist_medical_apparatus_fault_report($set_key,$set_data);

	}else if($NUMBER_OF_OCCURRENCES == "number_of_occurrences"){
	
		//ヒヤリハット件数表

		//年度、病院IDで既存データ検索
		$hospital_id = @$_REQUEST["data_hospital_id"];
		$data_occurrences_year = @$_REQUEST["data_occurrences_year"];
		$sql = " SELECT ".
			" 	lev.apply_id ".
			" FROM ".
			" 	fplus_number_of_occurrences_level lev ".
			" 	,fplusapply app  ".
			" WHERE ".
			" 	hospital_id = '$hospital_id' ".
			" 	AND occurrences_year = '$data_occurrences_year' ".
			" 	AND del_flg = 'f' ".
			" 	AND lev.apply_id = app.apply_id ".
			" 	AND app.delete_flg = 'f' ";
		$cond = "";
		$del_apply_id_sel = select_from_table($con,$sql,$cond,$fname);
		$del_apply_id = pg_result($del_apply_id_sel,0,"apply_id");

		if($del_apply_id != ""){
			//レベル別発生件数データ削除処理(論理削除)
			$sql=	"update fplus_number_of_occurrences_level set";
			$set =	array("del_flg");
			$cond = "where apply_id = '$del_apply_id' AND del_flg = 'f' ";

			$setvalue = array("t");
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			//職種別発生件数データ削除処理(論理削除)
			$sql=	"update fplus_number_of_occurrences set";
			$set =	array("del_flg");
			$cond = "where apply_id = '$del_apply_id' AND del_flg = 'f' ";

			$setvalue = array("t");
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			//転倒・転落発生件数データ削除処理(論理削除)
			$sql=	"update fplus_number_of_occurrences_fall_accident set";
			$set =	array("del_flg");
			$cond = "where apply_id = '$del_apply_id' AND del_flg = 'f' ";

			$setvalue = array("t");
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			//報告データ削除（論理削除）
			$sql=	"update fplusapply set";
			$set =	array("delete_flg");
			$cond = "where apply_id = '$del_apply_id' AND delete_flg = 'f' ";

			$setvalue = array("t");
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}

		//レベル別発生件数データ
		//検索処理
		$sql=	"SELECT levelt_id, report_no FROM fplus_number_of_occurrences_level ";
		$cond = "where apply_id = $apply_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//レポートID、報告�發琉�き継ぎ
		$report_id = pg_fetch_result($sel, 0, "levelt_id");
		$report_no = pg_fetch_result($sel, 0, "report_no");

		//デリート処理
		$sql=	"delete from fplus_number_of_occurrences_level ";		
		$cond = "where apply_id = $apply_id";
		$sel = delete_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$consultation_request_id = "";
		if($hdn_consultation_request_id!=""){
			//引用したレポートID
			$consultation_request_id = $hdn_consultation_request_id;
		}

		//インサート処理
		$set_data = array();
		$set_key = array();
		$set_data[0] = $apply_id;
		$set_key[0] = "apply_id";
		$set_data[1] = $report_id;
		$set_key[1] = "levelt_id";
		$set_data[2] = $report_no;
		$set_key[2] = "report_no";
		$set_data[3] = date("Y-m-d H:i:s");
		$set_key[3] = "update_time";
		$set_data[4] = "f";
		$set_key[4] = "del_flg";

		$wcnt0 = 5;
		
		foreach($_REQUEST as $epi_key => $epi_val)
		{
			$chk_key = substr($epi_key,0, 5);
			
			if($chk_key == "data_" or $chk_key == "dat1_")
			{
				$epi_key = substr($epi_key,5);
				if(is_array($epi_val))
				{
					//入力データcheckbox,radio用
					if($epi_val[0]!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
				else
				{
					//入力データ
					if($epi_val!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
			}
		}

		//データの登録処理(レベル別発生件数データ)
		$obj->regist_number_of_occurrences_level($set_key,$set_data);

		//職種別発生件数データ
		//検索処理
		$sql=	"SELECT number_of_occurrences_id, report_no FROM fplus_number_of_occurrences ";
		$cond = "where apply_id = $apply_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//レポートID、報告�發琉�き継ぎ
		$report_id = pg_fetch_result($sel, 0, "number_of_occurrences_id");
		$report_no = pg_fetch_result($sel, 0, "report_no");

		//デリート処理
		$sql=	"delete from fplus_number_of_occurrences ";		
		$cond = "where apply_id = $apply_id";
		$sel = delete_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$consultation_request_id = "";
		if($hdn_consultation_request_id!=""){
			//引用したレポートID
			$consultation_request_id = $hdn_consultation_request_id;
		}

		//インサート処理
		$set_data = array();
		$set_key = array();
		$set_data[0] = $apply_id;
		$set_key[0] = "apply_id";
		$set_data[1] = $report_id;
		$set_key[1] = "number_of_occurrences_id";
		$set_data[2] = $report_no;
		$set_key[2] = "report_no";
		$set_data[3] = date("Y-m-d H:i:s");
		$set_key[3] = "update_time";
		$set_data[4] = "f";
		$set_key[4] = "del_flg";

		$wcnt0 = 5;
		
		foreach($_REQUEST as $epi_key => $epi_val)
		{
			$chk_key = substr($epi_key,0, 5);
			
			if($chk_key == "data_" or $chk_key == "dat2_")
			{
				$epi_key = substr($epi_key,5);
				if(is_array($epi_val))
				{
					//入力データcheckbox,radio用
					if($epi_val[0]!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
				else
				{
					//入力データ
					if($epi_val!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
			}
		}

		//データの登録処理(職種別発生件数データ)
		$obj->regist_number_of_occurrences($set_key,$set_data);

		//転倒・転落発生件数データ
		//検索処理
		$sql=	"SELECT fall_accident_id, report_no FROM fplus_number_of_occurrences_fall_accident ";
		$cond = "where apply_id = $apply_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//レポートID、報告�發琉�き継ぎ
		$report_id = pg_fetch_result($sel, 0, "fall_accident_id");
		$report_no = pg_fetch_result($sel, 0, "report_no");

		//デリート処理
		$sql=	"delete from fplus_number_of_occurrences_fall_accident ";		
		$cond = "where apply_id = $apply_id";
		$sel = delete_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$consultation_request_id = "";
		if($hdn_consultation_request_id!=""){
			//引用したレポートID
			$consultation_request_id = $hdn_consultation_request_id;
		}

		//インサート処理
		$set_data = array();
		$set_key = array();
		$set_data[0] = $apply_id;
		$set_key[0] = "apply_id";
		$set_data[1] = $report_id;
		$set_key[1] = "fall_accident_id";
		$set_data[2] = $report_no;
		$set_key[2] = "report_no";
		$set_data[3] = date("Y-m-d H:i:s");
		$set_key[3] = "update_time";
		$set_data[4] = "f";
		$set_key[4] = "del_flg";

		$wcnt0 = 5;
		
		foreach($_REQUEST as $epi_key => $epi_val)
		{
			$chk_key = substr($epi_key,0, 5);
			
			if($chk_key == "data_" or $chk_key == "dat3_")
			{
				$epi_key = substr($epi_key,5);
				if(is_array($epi_val))
				{
					//入力データcheckbox,radio用
					if($epi_val[0]!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
				else
				{
					//入力データ
					if($epi_val!=""){
						$set_data[$wcnt0] = pg_escape_string($epi_val);
						$set_key[$wcnt0] = $epi_key;
						$wcnt0++;
					}
				}
			}
		}

		//データの登録処理(転倒・転落発生件数データ)
		$obj->regist_number_of_occurrences_fall_accident($set_key,$set_data);

}

	
	$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
	$wkfw_id           = $arr_apply_wkfwmst[0]["wkfw_id"];
	$wkfw_history_no   = $arr_apply_wkfwmst[0]["wkfw_history_no"];
	$wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
	$short_wkfw_name   = $arr_apply_wkfwmst[0]["short_wkfw_name"];


	//テンプレートの場合XML形式のテキスト$contentを作成
	$ext = ".php";
	$savexmlfilename = "fplus/workflow/tmp/{$session}_x{$ext}";

	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];

	$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
	$fp = fopen($savexmlfilename, "w");
	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、更新してください。$savexmlfilename');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、更新してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	fclose($fp);

	include( $savexmlfilename );


	// 申請内容更新
	//$obj->update_apply_content($apply_id, $content);
	$apply = fplus_get_apply_by_apply_id($con, $apply_id, $fname);
	$apply_title = $apply["apply_title"];
	
	$obj->update_apply($apply_id, $content, $apply_title);

	// トランザクションをコミット
	pg_query($con, "commit");
	
	// データベース接続を切断
	pg_close($con);
	
}
else
{
	
	//$approve_modeを取得できなかった場合もここを通る
	//→$approve_modeを設定しているのはエピネット報告書の更新ボタンのみです。

	// 承認処理
	
	
	// 申請結果通知削除・登録
	$obj->delete_applynotice($apply_id);
	if($notice_emp_id != "")
	{
		$arr_notice_emp_id = split(",", $notice_emp_id);
		$arr_rslt_ntc_div  = split(",", $rslt_ntc_div);
		for($i=0; $i<count($arr_notice_emp_id); $i++)
		{
			$obj->regist_applynotice($apply_id, $arr_notice_emp_id[$i], $arr_rslt_ntc_div[$i]);
		}
	}
	
	// 承認処理
	$obj->approve_application($apply_id, $wkfw_appr, $apv_order, $apv_sub_order, $approve, $apv_comment, $next_notice_div, $session, "DETAIL");
	
	// メールを送るかどうか判断、送る場合は宛先を配列に追加
	$to_addresses = array();
	if (fplus_must_send_mail_to_next_approvers($con, $obj, $apply_id, $wkfw_appr, $apv_order, $approve, $next_notice_div, $fname)) {
		
		// 次の階層で未承認の承認者（全員のはず）のメールアドレスを取得
		$next_apv_order = $apv_order + 1;
		$sql = "select e.emp_email2 from empmst e";
		$cond = "where exists (select * from fplusapplyapv a where a.emp_id = e.emp_id and a.apply_id = $apply_id and a.apv_order = $next_apv_order and a.apv_stat = '0')";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			if ($row["emp_email2"] != "") {
				$to_addresses[] = $row["emp_email2"];
			}
		}
	}
	
	// メール送信に必要な情報を取得
	if (count($to_addresses) > 0) {
		$apply = fplus_get_apply_by_apply_id($con, $apply_id, $fname);
		$wkfw_content_type = $apply["wkfw_content_type"];
		$content = $apply["apply_content"];
		$apply_title = $apply["apply_title"];
		$apply_emp_id = $apply["emp_id"];
		
		$emp_detail = $obj->get_empmst_detail($apply_emp_id);
		$emp_nm = fplus_format_emp_nm($emp_detail[0]);
		$emp_mail = fplus_format_emp_mail($emp_detail[0]);
		$emp_pos = fplus_format_emp_pos($emp_detail[0]);
		
		$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($target_apply_id);
		$approve_label = ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認";
	}
	
	// 文書登録処理
	$files = fplus_register_library($con, $approve, $obj, $apply_id, $fname);
	
	// トランザクションをコミット
	//pg_query($con, "rollback");
	pg_query($con, "commit");
	
	// データベース接続を切断
	pg_close($con);
	
	// メール送信
	if (count($to_addresses) > 0) {
		mb_internal_encoding("EUC-JP");
		mb_language("Japanese");
		
		$mail_subject = "[CoMedix] 受付依頼のお知らせ";
		$mail_content = fplus_format_mail_content($wkfw_content_type, $content);
		$mail_separator = str_repeat("-", 60) . "\n";
		$additional_headers = "From: $emp_mail";
		$additional_parameter = "-f$emp_mail";
		
		foreach ($to_addresses as $to_address) {
			$mail_body = "以下の受付依頼がありました。\n\n";
			$mail_body .= "報告者：{$emp_nm}\n";
			$mail_body .= "所属：{$emp_pos}\n";
			$mail_body .= "表題：{$apply_title}\n";
			if ($mail_content != "") {
				$mail_body .= $mail_separator;
				$mail_body .= "{$mail_content}\n";
			}
			
			mb_send_mail($to_address, $mail_subject, $mail_body, $additional_headers, $additional_parameter);
		}
	}
	
	// ファイルコピー
	foreach ($files as $tmp_file) {
		fplus_copy_apply_file_to_library($tmp_file);
	}
}




// 一覧画面に遷移
echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
echo("<script language=\"javascript\">window.close();</script>\n");
?>
