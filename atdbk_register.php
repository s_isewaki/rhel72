<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 出勤表 | 出勤予定表</title>
<?
require_once("about_comedix.php");
require_once("show_attendance_pattern.ini");
require_once("atdbk_menu_common.ini");
require_once("atdbk_common_class.php");
require_once("show_timecard_common.ini");
require_once("timecard_bean.php");
require_once("show_select_values.ini");
require_once("atdbk_close_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 5, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限の取得
$work_admin_auth = check_authority($session, 42, $fname);

// データベースに接続
$con = connect2db($fname);

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);
//javascript出力
$atdbk_common_class->setJavascript();

//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

// ログインユーザの職員ID・氏名を取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm, emp_personal_id from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
$emp_personal_id = pg_fetch_result($sel, 0, "emp_personal_id");

// ログインユーザの出勤グループを取得
$tmcd_group_id = get_timecard_group_id($con, $emp_id, $fname);

// ************ oose add start *****************
// 勤務条件テーブルより勤務形態（常勤、非常勤）を取得
$sql = "select duty_form from empcond";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";
// ************ oose add end *****************

$sql = "select * from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
//職員本人による修正可否フラグ
$timecard_modify_flg = pg_fetch_result($sel, 0, "modify_flg");

$atdbk_close_class = new atdbk_close_class($con, $fname);
$closing = $atdbk_close_class->get_empcond_closing($emp_id);
$closing_month_flg = $atdbk_close_class->closing_month_flg;

// 締め日が登録済みの場合
if ($closing != "") {
	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
		$month_set_flg = true; //月が指定された場合
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
		$month_set_flg = false; //月が未指定の場合
	}

	// 開始日・締め日を算出
	$calced = false;
	switch ($closing) {
	case "1":  // 1日
		$closing_day = 1;
		break;
	case "2":  // 5日
		$closing_day = 5;
		break;
	case "3":  // 10日
		$closing_day = 10;
		break;
	case "4":  // 15日
		$closing_day = 15;
		break;
	case "5":  // 20日
		$closing_day = 20;
		break;
	case "6":  // 末日
		$start_year = $year;
		$start_month = $month;
		$start_day = 1;
		$end_year = $start_year;
		$end_month = $start_month;
		$end_day = days_in_month($end_year, $end_month);
		$calced = true;
		break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		//月が指定された場合
		if ($month_set_flg) {
			//当月〜翌月
			if ($closing_month_flg != "2") {
				$start_year = $year;
				$start_month = $month;

				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
			//前月〜当月
			else {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}

				$end_year = $year;
				$end_month = $month;
			}

		} else {
			if ($day <= $closing_day) {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}

				$end_year = $year;
				$end_month = $month;
			} else {
				$start_year = $year;
				$start_month = $month;

				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
		}
	}
	$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);

	//月が未指定の場合
	if (!$month_set_flg) {
		if ($closing_month_flg != "2") {
			$year = $start_year;
			$month = $start_month;
		} else {
			$year = $end_year;
			$month = $end_month;
		}
		$yyyymm = sprintf("%04d%02d", $year, $month);
	}

	//前月、翌月リンク用
	$last_month_y = $year;
	$last_month_m = $month - 1;
	if ($last_month_m == 0) {
		$last_month_y--;
		$last_month_m = 12;
	}
	$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);

	$next_month_y = $year;
	$next_month_m = $month + 1;
	if ($next_month_m == 13) {
		$next_month_y++;
		$next_month_m = 1;
	}
	$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);

	// 給与支給区分を取得
	$sql = "select wage from empcond";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$wage = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "wage") : "";

	// 対象年月の締め状況を取得
//	$tmp_yyyymm = substr($start_date, 0, 6);
	//月度対応 20100415
	$tmp_yyyymm = $year.sprintf("%02d", $month);
	$sql = "select count(*) from atdbkclose";
	$cond = "where emp_id = '$emp_id' and yyyymm = '$tmp_yyyymm'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$atdbk_closed = (pg_fetch_result($sel, 0, 0) > 0);
}

?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--
function openBulkRegPage() {
// ************ oose update start *****************
//	window.open('atdbk_bulk_register.php?session=<? echo($session); ?>&yyyymm=<? echo("$yyyymm"); ?>&wherefrom=1', 'newwin', 'width=640,height=480,scrollbars=yes');
	window.open('atdbk_bulk_register.php?session=<? echo($session); ?>&yyyymm=<? echo("$yyyymm"); ?>&wherefrom=1&inp_chk_flg=1', 'newwin', 'width=640,height=480,scrollbars=yes');
// ************ oose update end *****************
}

function openPrintPage() {
	window.open('atdbk_register_print.php?session=<? echo($session); ?>&yyyymm=<? echo("$yyyymm"); ?>&wherefrom=1', 'newwin', 'width=800,height=700,scrollbars=yes');
}

function downloadCSV() {
	document.csv.submit();
}
var arr_atdptn_reason = new Array();
<?
//事由情報を出力
$arr_group_pattern_reason = $atdbk_common_class->get_pattern_reason_array("");
foreach ($arr_group_pattern_reason as $wk_tmcd_group_id => $arr_pattern_reason) {
	foreach ($arr_pattern_reason as $wk_atdptn_id => $reason) {
		if ($reason != "") {
			echo("arr_atdptn_reason['{$wk_tmcd_group_id}_{$wk_atdptn_id}'] = '$reason';\n");
		}
	}
}
?>
function setReason(pos, tmcd_group_id, atdptn_id) {

	if (atdptn_id == '--') {
		document.timecard["reason[]"][pos].value = '--';
		return;
	}
	wk_tmcd_group_id = document.timecard["list_tmcd_group_id[]"][pos].value;
	wk_id = wk_tmcd_group_id+'_'+atdptn_id;
	if (arr_atdptn_reason[wk_id] == undefined) {
		document.timecard["reason[]"][pos].value = '--';
		return;
	}
	document.timecard["reason[]"][pos].value = arr_atdptn_reason[wk_id];
}
//登録
function editTimecard() {
	document.timecard.action = 'atdbk_insert.php';
	document.timecard.submit();
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
td.lbl {padding-right:1em;}
td.txt {padding-right:0.5em;}
td.tm {padding-left:0.5em;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a></font></td>
<? if ($work_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>&work=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<?
/*
echo("【".$emp_id."】");
echo("【".$yyyymm."】");
*/
?>
<?
// メニュー表示
show_atdbk_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? if ($closing == "") {  // 締め日未登録の場合 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムカード締め日が登録されていません。管理者に連絡してください。</font></td>
</tr>
</table>
<? } else if ($wage == "") {  // 給与支給区分未登録の場合 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">給与支給区分が登録されていません。管理者に連絡してください。</font></td>
</tr>
</table>
<? } else { ?>
<form name="timecard" method="post">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22" width="260"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象期間&nbsp;&nbsp;<a href="atdbk_register.php?session=<? echo($session); ?>&yyyymm=<? echo($last_yyyymm); ?>">&lt;前月</a>&nbsp;<?
    $str = show_yyyymm_menu($year, $month);
    echo $str;
?>
&nbsp;<a href="atdbk_register.php?session=<? echo($session); ?>&yyyymm=<? echo($next_yyyymm); ?>">翌月&gt;</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始日&nbsp;&nbsp;<? echo("{$start_month}月{$start_day}日"); ?>&nbsp;&nbsp;〜&nbsp;&nbsp;締め日&nbsp;&nbsp;<? echo("{$end_month}月{$end_day}日"); ?></font></td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID&nbsp;<? echo($emp_personal_id); ?>&nbsp;&nbsp;職員氏名&nbsp;<? echo($emp_name);
// 勤務条件表示
$str_empcond = get_employee_empcond2($con, $fname, $emp_id);

$str_shiftname = get_shiftname($con, $fname, $atdbk_closed, $emp_id, $start_date, $end_date);
//所属表示
$str_orgname = get_orgname($con, $fname, $emp_id, $timecard_bean);
echo("　<b>（".$str_empcond."　".$str_shiftname."）$str_orgname</b>");
?></font></td>
<td align="right" width="280">
<?php
//本人による変更(出勤予定表)追加 20140226
if ($timecard_bean->modify_plan_flg == "t") { ?>
<input type="button" value="登録" onclick="editTimecard();"<? if ($atdbk_closed) {echo(" disabled");} ?>>&nbsp;
<input type="button" value="一括登録"<? if ($atdbk_closed) {echo(" disabled");} ?> onclick="openBulkRegPage();">&nbsp;
<?php } ?>
<input type="button" value="印刷" onclick="openPrintPage();">&nbsp;<input type="button" value="CSV出力" onclick="downloadCSV();"></td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table width="100%" border="1" cellspacing="0" cellpadding="1">
<tr>
<td height="22" width="" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td width="" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">曜日</font></td>
<td width="" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></td>
<td width="" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">グループ</font></td>
<td width="" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤予定</font></td>
<td width="" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font></td>
<td width="" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($atdbk_common_class->duty_or_oncall_str); ?></font></td>
<td width="" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当</font></td>
<td width="" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務開始予定</font></td>
<td width="" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務終了予定</font></td>
</tr>
<?
	show_attendance_book($con, $emp_id, $start_date, $end_date, $fname, $atdbk_closed, $wage, $timecard_modify_flg, $tmcd_group_id, $atdbk_common_class, $timecard_bean);
?>
</table>
</td>
</tr>
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22" align="right">
<?php
//本人による変更(出勤予定表)追加 20140226
if ($timecard_bean->modify_plan_flg == "t") { ?>
<input type="button" value="登録" onclick="editTimecard();"<? if ($atdbk_closed) {echo(" disabled");} ?>>&nbsp;
<input type="button" value="一括登録"<? if ($atdbk_closed) {echo(" disabled");} ?> onclick="openBulkRegPage();">&nbsp;
<?php } ?>
<input type="button" value="印刷" onclick="openPrintPage();">&nbsp;<input type="button" value="CSV出力" onclick="downloadCSV();"></td>
</tr>
</table>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="yyyymm" value="<? echo($yyyymm); ?>">
<input type="hidden" name="wherefrom" value="1">
</form>
<form name="csv" action="atdbk_register_csv.php" method="get" target="download">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="yyyymm" value="<? echo($yyyymm); ?>">
<input type="hidden" name="wherefrom" value="1">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
<? } ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
// 出勤予定を出力
function show_attendance_book($con, $emp_id, $start_date, $end_date, $fname, $atdbk_closed, $wage, $timecard_modify_flg, $tmcd_group_id, $atdbk_common_class, $timecard_bean) {

	// 手当情報取得
	$arr_allowance = get_timecard_allowance($con, $fname);

// ************ oose add start *****************
	$date = getdate(); //現在日付の取得
	$now_yyyy = $date["year"];
	$now_mm = $date["mon"];
	$now_dd = $date["mday"];
	$now_date = sprintf("%04d%02d%02d", $now_yyyy, $now_mm,$now_dd);
// ************ oose add end *****************
	// 出勤パターングループを取得
//	$arr_timecard_group = get_timecard_group_names($con, $fname);
	$arr_timecard_group = $atdbk_common_class->get_group_array();

	// 処理日付の勤務日種別を取得
	$arr_type = array();
	$sql = "select date, type from calendar";
	$cond = "where date >= '$start_date' and date <= '$end_date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_type[$date] = $row["type"];
	}
// 予定データをまとめて取得
	$arr_atdbk = array();
	$sql = "select date, pattern, reason, night_duty, prov_start_time, prov_end_time, allow_id , tmcd_group_id from atdbk";
	$cond = "where emp_id = '$emp_id' and date >= '$start_date' and date <= '$end_date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_atdbk[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
		$arr_atdbk[$date]["pattern"] = $row["pattern"];
		$arr_atdbk[$date]["reason"] = $row["reason"];
		$arr_atdbk[$date]["night_duty"] = $row["night_duty"];
		$arr_atdbk[$date]["allow_id"] = $row["allow_id"];
		$arr_atdbk[$date]["prov_start_time"] = $row["prov_start_time"];
		$arr_atdbk[$date]["prov_end_time"] = $row["prov_end_time"];
	}

	$wk_pos = 0; //行位置
	$tmp_date = $start_date;
	while ($tmp_date <= $end_date) {
		$tmp_year = intval(substr($tmp_date, 0, 4));
		$tmp_month = intval(substr($tmp_date, 4, 2));
		$tmp_day = intval(substr($tmp_date, 6, 2));

		// 処理日付の勤務日種別を取得
		$type = $arr_type[$tmp_date];
		// カレンダー名を取得
		$type_name = $atdbk_common_class->get_calendarname($type, $tmp_date);
		// 処理日付の出勤予定を取得
		$list_tmcd_group_id = $arr_atdbk[$tmp_date]["tmcd_group_id"];
		$pattern = $arr_atdbk[$tmp_date]["pattern"];
		$reason = $arr_atdbk[$tmp_date]["reason"];
		$night_duty = $arr_atdbk[$tmp_date]["night_duty"];
		$allow_id = $arr_atdbk[$tmp_date]["allow_id"];

		$prov_start_time = $arr_atdbk[$tmp_date]["prov_start_time"];
		$prov_end_time = $arr_atdbk[$tmp_date]["prov_end_time"];

		//出勤予定にグループが指定されていた場合、出勤予定のグループを優先する
		if ($list_tmcd_group_id == null && strlen($list_tmcd_group_id) == 0){
			// 出勤パターン名を取得
			$list_tmcd_group_id = $tmcd_group_id;
		}

		$selectGroupId   = "list_tmcd_group_id_".$tmp_date;
		$selectPatternId = "pattern_".$tmp_date;

		//背景色設定。法定、祝祭日を赤、所定を青にする
		$bgcolor = get_timecard_bgcolor($type);
		echo("<tr bgcolor=\"$bgcolor\">\n");
        // 日付
		echo("<td height=\"22\" class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_month}月{$tmp_day}日</font><input type=\"hidden\" name=\"date[]\" value=\"$tmp_date\"></td>\n");
        // 曜日
		echo("<td class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . get_weekday(to_timestamp($tmp_date)) . "</font></td>\n");
        // 種別
		echo("<td class=\"lbl\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$type_name</font></td>\n");
        // グループ
		$arr_attendance_pattern = $atdbk_common_class->get_pattern_array($list_tmcd_group_id);
		//echo("<option value=\"--\">");
		echo("<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");

		if ($atdbk_closed || $timecard_bean->modify_plan_flg == "f") { //本人による変更（出勤予定表） 20140226
			if ($pattern != "") {
				echo($atdbk_common_class->get_group_name($list_tmcd_group_id));
			} else {
				echo("&nbsp;");
			}
		} else {
			$enableFlag = (($now_date >= $tmp_date)) == false; //($timecard_modify_flg == "f") && 20140226
			$atdbk_common_class->setGroupOption("list_tmcd_group_id[]", $selectGroupId, $list_tmcd_group_id, $selectPatternId, $enableFlag);
		}

		echo("</font></td>\n");

        // 出勤予定
		echo("<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		if ($atdbk_closed || $timecard_bean->modify_plan_flg == "f") { //本人による変更（出勤予定表） 20140226
			if ($pattern != "") {
				echo($arr_attendance_pattern[$pattern]);
			} else {
				echo("&nbsp;");
			}
		} else {
// ************ oose update start *****************

//			echo("<select name=\"pattern[]\">");
			if (($now_date >= $tmp_date)) { //20140226
				echo("<input type=\"hidden\" name=\"pattern[]\" value=\"$pattern\"> \n");
				echo("<select name=\"\" id=\"$selectPatternId\" disabled>");
			} else {
				echo("<select name=\"pattern[]\" id=\"$selectPatternId\" onChange=\"setReason($wk_pos, $tmcd_group_id, this.value);\">");
			}
			$wk_pos++;
// ************ oose update end *****************
			echo("<option value=\"--\">");
			foreach ($arr_attendance_pattern as $id => $val) {
				echo("<option value=\"$id\"");
				if ($pattern == $id) {
					echo(" selected");
				}
				echo(">$val");
			}
			echo("</select>");

		}
		echo("</font></td>\n");

        // 事由
		echo("<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
		if ($atdbk_closed || $timecard_bean->modify_plan_flg == "f") { //本人による変更（出勤予定表） 20140226
			echo($atdbk_common_class->get_reason_name($reason)."<br>");
		} else {
			$atdbk_common_class->set_reason_option("reason[]", $reason, (($now_date >= $tmp_date)) == false); //20140226
		}

		echo("</font></td>\n");

        // 当直
		echo("<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
		if ($atdbk_closed || $timecard_bean->modify_plan_flg == "f") { //本人による変更（出勤予定表） 20140226
			switch ($night_duty) {
			case "1":
				echo("有り");
				break;
			case "2":
				echo("無し");
				break;
			default:
				echo("&nbsp;");
				break;
			}
		} else {
// ************ oose update start *****************
//			echo("<select name=\"night_duty[]\">");
			if (($now_date >= $tmp_date)) { //20140226
				echo("<input type=\"hidden\" name=\"night_duty[]\" value=\"$night_duty\"> \n");
				echo("<select name=\"\" disabled>");
			} else {
				echo("<select name=\"night_duty[]\">");
			}
// ************ oose update end *****************
			echo("<option value=\"--\">");
			echo("<option value=\"1\""); if ($night_duty == "1") {echo(" selected");} echo(">有り");
			echo("<option value=\"2\""); if ($night_duty == "2") {echo(" selected");} echo(">無し");
			echo("</select>");
		}
		echo("</font></td>\n");

        // 手当
		echo("<td class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
// ************ oose update start *****************
//		show_allowance_list($allow_id, $arr_allowance);
		if ($atdbk_closed || $timecard_bean->modify_plan_flg == "f") { //本人による変更（出勤予定表） 20140226

            if($allow_id != "")
            {
				foreach($arr_allowance as $allowance)
				{
					$tmp_allow_id = $allowance["allow_id"];
					$tmp_allow_contents = $allowance["allow_contents"];
					if($allow_id == $tmp_allow_id)
					{
					    echo($tmp_allow_contents);
                        break;
					}
				}
            }
            else
            {
				echo("&nbsp;");
            }
		} else {
			if (($now_date >= $tmp_date)) { //20140226
				if ($allow_id == "") {
					$wk = "--";
				} else {
					$wk = $allow_id;
				}
				echo("<input type=\"hidden\" name=\"allow_ids[]\" value=\"$wk\"> \n");
				echo("<select name=\"\" disabled>");
			} else {
				echo("<select name=\"allow_ids[]\">");
			}
			echo("<option value=\"--\">");

			foreach($arr_allowance as $allowance)
			{
				$tmp_allow_id = $allowance["allow_id"];
				$tmp_allow_contents = $allowance["allow_contents"];
				$tmp_del_flg = $allowance["del_flg"];

				if($allow_id == $tmp_allow_id)
				{
					echo("<option value=\"$tmp_allow_id\"");
					echo(" selected");
					echo(">$tmp_allow_contents");
				}
				else
				{
					if($tmp_del_flg == 'f')
					{
						echo("<option value=\"$tmp_allow_id\"");
						echo(">$tmp_allow_contents");
					}
				}
			}
			echo("</select>");
		}
// ************ oose update end *****************
		echo("</font></td>\n");

        // 勤務開始予定
		$tmp_start_hour = ($prov_start_time != "") ? substr($prov_start_time, 0, 2) : "";
		$tmp_start_min = ($prov_start_time != "") ? substr($prov_start_time, 2, 2) : "";
		echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
		if ($atdbk_closed || $timecard_bean->modify_plan_flg == "f") { //本人による変更（出勤予定表） 20140226
			if ($prov_start_time != "") {
				$prov_start_time = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $prov_start_time);
				$prov_start_time = preg_replace("/^0(.*)/", "$1", $prov_start_time);
				echo($prov_start_time);
			} else {
				echo("&nbsp;");
			}
		} else {
			echo("<select name=\"start_hour[]\"");
			if ($wage != "4") {
				echo(" disabled");
			}
			echo(">");
			show_hour_options_0_23($tmp_start_hour);
			echo("</select>：");
			echo("<select name=\"start_min[]\"");
			if ($wage != "4") {
				echo(" disabled");
			}
			echo(">");
			show_min_options_5($tmp_start_min);
			echo("</select>");
		}
		echo("</font></td>\n");

        // 勤務終了予定
		$tmp_end_hour = ($prov_end_time != "") ? substr($prov_end_time, 0, 2) : "";
		$tmp_end_min = ($prov_end_time != "") ? substr($prov_end_time, 2, 2) : "";
		echo("<td class=\"tm\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
		if ($atdbk_closed || $timecard_bean->modify_plan_flg == "f") { //本人による変更（出勤予定表） 20140226
			if ($prov_end_time != "") {
				$prov_end_time = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $prov_end_time);
				$prov_end_time = preg_replace("/^0(.*)/", "$1", $prov_end_time);
				echo($prov_end_time);
			} else {
				echo("&nbsp;");
			}
		} else {
			echo("<select name=\"end_hour[]\"");
			if ($wage != "4") {
				echo(" disabled");
			}
			echo(">");
			show_hour_options_0_23($tmp_end_hour);
			echo("</select>：");
			echo("<select name=\"end_min[]\"");
			if ($wage != "4") {
				echo(" disabled");
			}
			echo(">");
			show_min_options_5($tmp_end_min);
			echo("</select>");
		}
		echo("</font></td>\n");

		echo("</tr>\n");

		$tmp_date = next_date($tmp_date);
	}

}
/*
function get_atdbk_group_id($con, $emp_id, $fname) {
	$sql = "select tmcd_group_id from empcond";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		return pg_fetch_result($sel, 0, "tmcd_group_id");
	} else {
		return 1;
	}
}
*/
?>
