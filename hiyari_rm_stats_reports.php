<?php
ob_start();
require_once('about_comedix.php');
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once('Cmx/View/Smarty.php');
require_once("hiyari_report_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    showLoginPage();
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
    showLoginPage();
    exit;
}
$con = connect2db($fname);

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$file = get_file_name_from_php_self($fname);

//==============================
//ユーザーID取得
//==============================
$sql = "SELECT * FROM empmst JOIN session USING (emp_id) WHERE session_id='{$session}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp = pg_fetch_assoc($sel);
$emp_id = $emp["emp_id"];

//==================================================
//レポートクラス
//==================================================

$rep_obj = new hiyari_report_class($con, $fname);
//==============================
//報告書一覧のヘッダー
//==============================
$list_header = array();
if ($_POST['mode'] != 'excel'){
    $list_header[] ="&nbsp;";
}
$list_header[] ="事案番号";
$list_header[] ="表題";
$list_header[] ="報告部署";
$list_header[] ="報告者";
if ($count_date_mode == "incident_date"){
    $list_header[] ="発生年月日";
}
else{
    $list_header[] ="報告年月日";
}
$list_header[] ="集計数";

//==============================
//報告書一覧のデータ
//==============================
$anonymous_setting = get_anonymous_setting($session, $con, $fname);

$list_data = array();
$report_data = get_stats_report_data($con, $fname, $report_id_list);
foreach ($report_data as $report) {
    $info = array();
    $info['mail_id'] = $rep_obj->get_newest_mail_id($report["report_id"]);
    $info['report_no'] = $report["report_no"];
    $info['title'] = h($report["report_title"]);
    $info['class_nm'] = h($report["class_nm"]);

    if($report["anonymous_report_flg"] == "t"){
        $info['disp_emp_name'] = '匿名';
        if ($design_mode == 1){
            $info['disp_emp_name'] = h(get_anonymous_name($con,$fname));
        }
    }
    else{
        $info['disp_emp_name'] = h($report["registrant_nm"]);
    }

    if ($count_date_mode == "incident_date"){
        $info['ymd'] = $report["happened_ymd"];
    }
    else{
        $info['ymd'] = $report["registration_date"];
    }

    $info['count'] = $report["count"];

    if ($_POST['mode'] != 'excel'){
        $info['report_id'] = $report['report_id'];
        $info['is_linkable'] = is_report_db_readable_report_post($session, $fname, $con, $report["registrant_class"], $report["registrant_attribute"], $report["registrant_dept"], $report["registrant_room"]);

        //匿名の場合の実名ポップアップ表示
        $info['is_popup_name'] = false;
        if ($report["anonymous_report_flg"] == "t"){
            $mouseover_popup_flg = false;
            if(is_inci_auth_emp_of_auth($session,$fname,$con,"SM") and !empty($anonymous_setting)) {
                $mouseover_popup_flg = true;
            }
            else {
                foreach ($anonymous_setting as $auth => $data) {
                    if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 1) {
                        if ($report["registrant_class"] == $emp["emp_class"]) {
                            $mouseover_popup_flg = true;
                        }
                    }
                    else if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 2) {
                        if ($report["registrant_class"] == $emp["emp_class"] and $report["registrant_attribute"] == $emp["emp_attribute"]) {
                            $mouseover_popup_flg = true;
                        }
                    }
                    else if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 3) {
                        if ($report["registrant_class"] == $emp["emp_class"] and $report["registrant_attribute"] == $emp["emp_attribute"] and $report["registrant_dept"] == $emp["emp_dept"]) {
                            $mouseover_popup_flg = true;
                        }
                    }
                    if ($data["report_db_read_auth_flg"] == 't') {
                        if ($report["registrant_room"]) {
                            if ($data["rm"][ $report["registrant_class"] ][ $report["registrant_attribute"] ][ $report["registrant_dept"] ][ $report["registrant_room"] ]) {
                                $mouseover_popup_flg = true;
                            }
                        }
                        else {
                            if ($data["rm"][ $report["registrant_class"] ][ $report["registrant_attribute"] ][ $report["registrant_dept"] ]) {
                                $mouseover_popup_flg = true;
                            }
                        }
                    }
                }
            }

            if ($mouseover_popup_flg){
                $info['is_popup_name'] = true;
                if ($design_mode == 1){
                    $info['real_name'] = "<font size=\\'3\\' face=\\'ＭＳ Ｐゴシック, Osaka\\' class=\\'j12\\'>" . h($report["registrant_nm"]) . "</font>";
                }
                else{
                    $info['real_name'] = h($report["registrant_nm"]);
                }
            }
        }
    }
    
    $list_data[] = $info;
}

if ($_POST['mode'] == 'excel'){
	$list_data_excel = array();
	$list_data_excel = $list_data;
	foreach ($list_data_excel as $execl_key=>$excel_item) {
		unset($list_data_excel[$execl_key]["mail_id"]);
	}
    output_excel($list_header, $list_data_excel);
    exit;
}

//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("session", $session);

$smarty->assign("file", $file);
$smarty->assign("report_id_list", $report_id_list);

//SM以外は、報告書を編集モードで開かない
if ( !is_sm_emp($session,$fname) ) {
    $non_update_flg = "&non_update_flg=true";
}
$smarty->assign("non_update_flg", $non_update_flg);

//縦軸・横軸
$smarty->assign("vertical_label", h($vertical_label));
$smarty->assign("vertical_item_label", h($vertical_item_label));
$smarty->assign("horizontal_label", h($horizontal_label));
$smarty->assign("horizontal_item_label", h($horizontal_item_label));

//出来事分析
$is_analysis_available = is_analysis_available($con, $fname);                   //出来事分析の利用（管理＞オプション＞出来事分析設定）
$is_analysis_updatable = is_analysis_update_usable($session, $fname, $con);     //出来事分析の新規作成権限（管理＞オプション＞出来事分析設定）
$smarty->assign("is_analysis_use", ($is_analysis_available && $is_analysis_updatable));

//集計モード
if ($design_mode == 1){
    $smarty->assign("count_date_mode", $count_date_mode);
}

//報告書一覧
$smarty->assign("list_header", $list_header);
$smarty->assign("list_data", $list_data);

if ($design_mode == 1){
    $smarty->display("hiyari_rm_stats_reports1.tpl");
}
else{
    $smarty->display("hiyari_rm_stats_reports2.tpl");
}

//==============================================================================
//内部関数
//==============================================================================
//レポートデータ取得
function get_stats_report_data($con, $fname, $report_id_list) {
    $arr_ret = array();

    $arr_report_id = split(",", $report_id_list);
    
    //重複データ除去
    $arr_unique_report_id = array_unique($arr_report_id);

    $unique_report_id_csv = "";
    foreach ($arr_unique_report_id as $unique_report_id) {
        if ( $unique_report_id_csv != "" ) {
            $unique_report_id_csv .= ",";
        }
        $unique_report_id_csv .= $unique_report_id;
    }

    $sql  = "select ";
    $sql .= "a.report_id, ";
    $sql .= "a.report_no, ";
    $sql .= "a.report_title, ";
    $sql .= "a.registration_date, ";
    $sql .= "a.anonymous_report_flg, ";
    $sql .= "a.registrant_class, ";
    $sql .= "a.registrant_attribute, ";
    $sql .= "a.registrant_dept, ";
    $sql .= "a.registrant_room, ";
    $sql .= "b.emp_lt_nm  || ' ' || emp_ft_nm as registrant_nm, ";
    $sql .= "c.class_nm, ";
    $sql .= "d.atrb_nm, ";
    $sql .= "e.dept_nm, ";
    $sql .= "f.room_nm, ";
    $sql .= "g.input_item as happened_ymd ";
    $sql .= "from inci_report a ";
    $sql .= "left join empmst b on a.registrant_id = b.emp_id ";
    $sql .= "left join classmst c on a.registrant_class = c.class_id ";
    $sql .= "left join atrbmst d on a.registrant_attribute = d.atrb_id ";
    $sql .= "left join deptmst e on a.registrant_dept = e.dept_id ";
    $sql .= "left join classroom f on a.registrant_room = f.room_id ";
    $sql .= "left join (select eid_id, input_item from inci_easyinput_data where grp_code = 100 and easy_item_code = 5) g on a.eid_id = g.eid_id ";

    $cond = "where report_id in ($unique_report_id_csv) order by report_id desc";

    $sel = select_from_table($con,$sql,$cond,$fname);

    if ( $sel == 0 ) {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    //プロフィールの個人設定情報
    $profile_use = get_inci_profile($fname, $con);

    while ( $row = pg_fetch_array($sel) ) {
        $class_nm = "";

        $cnt = 0;
        foreach ( $arr_report_id as $report_id) {
            if ( $report_id == $row["report_id"]) {
                $cnt++;
            }
        }

        if ( $profile_use["class_flg"] == "t" && $row["class_nm"] != "" ) {
            $class_nm .= $row["class_nm"];
        }
        
        if ( $profile_use["attribute_flg"] == "t" && $row["atrb_nm"] != "" ) {
            $class_nm .= $row["atrb_nm"];
        }
        if ( $profile_use["dept_flg"] == "t" && $row["dept_nm"] != "" ) {
            $class_nm .= $row["dept_nm"];
        }
        if ( $profile_use["room_flg"] == "t" && $row["room_nm"] != "") {
            $class_nm .= $row["room_nm"];
        }

        $arr_ret[] = array(
            "report_id" => $row["report_id"],
            "report_no" => $row["report_no"],
            "report_title" => $row["report_title"],
            "registrant_nm" => $row["registrant_nm"],
            "anonymous_report_flg" => $row["anonymous_report_flg"],
            "class_nm" => $class_nm,
            "happened_ymd" => $row["happened_ymd"],
            "registration_date" => $row["registration_date"],
            "count" => $cnt,
            "registrant_class" => $row["registrant_class"],
            "registrant_attribute" => $row["registrant_attribute"],
            "registrant_dept" => $row["registrant_dept"],
            "registrant_room" => $row["registrant_room"],
        );
    }
    return $arr_ret;
}

/**
 * 報告書データをHTML形式に変換し、Excel出力する
 *
 * @param array $header 報告書データのタイトル配列
 * @param array $data   報告書データの配列
 */
function output_excel($header, $data)
{
    //==============================
    // HTML作成
    //==============================
    $message = array();

    // metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
    $message[] = '<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">';

    $message[] = "<table border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";

    //ヘッダー
    $message[] = '<tr>';
    foreach($header as $str){
        $message[] = '<th style="background-color:#c8c8c8;">' . $str . '</th>';
    }
    $message[] = '</tr>';

    //報告書データ
    foreach ($data as $row) {
        $message[] = '<tr>';
        foreach ($row as $col) {
            $message[] = '<td>' . $col . '</td>';
        }
        $message[] = '</tr>';
    }

    $message[] ="</table>";

    $download_data =  implode($message, "");

    //==============================
    //EXCEL出力
    //==============================
    //ファイル名
    $filename = 'fantol_reports.xls';
    if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
        $encoding = 'sjis';
    }
    else {
        $encoding = mb_http_output();   // Firefox
    }
    $filename = mb_convert_encoding($filename,$encoding,mb_internal_encoding());

    //出力
    ob_clean();
    header("Content-Type: application/vnd.ms-excel");
    header('Content-Disposition: attachment; filename=' . $filename);
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
    header('Pragma: public');
    echo mb_convert_encoding(nl2br($download_data), 'sjis', mb_internal_encoding());
    ob_end_flush();
}

//==============================
//データベース接続を閉じる
//==============================
pg_close($con);
