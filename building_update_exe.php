<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病棟登録権限チェック
$ward = check_authority($session,21,$fname);
if($ward == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 棟名未入力チェック
if ($bldg_name == "") {
	echo("<script language=\"javascript\">alert('棟名が入力されていません。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin transaction");

// 指定棟に属する病棟の最大階数を取得
$sql = "select max(floor) from wdmst";
$cond = "where bldg_cd = '$bldg_cd' and ward_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) == 1) {
	$ward_floor = pg_fetch_result($sel, 0, 0);

	// 棟全階数＜病棟最大階数の場合、エラーとする
	if ($floor < $ward_floor) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script language=\"javascript\">alert('棟全階数より上の病棟が存在します。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
}

// 棟情報の更新
$sql = "update bldgmst set";
$set = array("bldg_name", "bldg_floor");
$setvalue = array($bldg_name, $floor);
$cond = "where bldg_cd = '$bldg_cd'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 棟修正画面を再表示
	echo("<script language=\"javascript\">location.href = 'building_update.php?session=$session&bldg_cd=$bldg_cd'</script>");
?>
