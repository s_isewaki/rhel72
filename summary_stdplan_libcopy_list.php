<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML XMLNS="http://www.w3.org/1999/xhtml">
<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=EUC-JP">
<LINK REL="STYLEsheet" TYPE="text/css" HREF="css/main_summary.css"/>
<?
require_once("about_comedix.php");
require_once("label_by_profile_type.ini");
require_once("show_sinryo_top_header.ini");
require_once("summary_common.ini");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");

//==============================
//ヘッダー処理
//==============================

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}

// 診療記録使用権限のチェック
$checkauth = check_authority($session,57,$fname);
if($checkauth == "0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}

// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);

// DBコネクションの作成
$con = connect2db($fname);
if($con == "0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療記録区分/記録区分
$summary_record_kubun_title = $_label_by_profile["SUMMARY_RECORD_KUBUN"][$profile_type];

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];

$mode = isset($_GET['mode']) ? $_GET['mode'] : '';
$is_postback = isset($_POST['is_postback']) ? $_POST['is_postback'] : '';
$problem_name = isset($_POST['problem_name']) ? trim($_POST['problem_name']) : '';
$case_name = isset($_POST['case_name']) ? trim($_POST['case_name']) : '';

$create_date = date("Y-m-d"); // 今日の年月日
//==========================================
// チーム計画マスター
//==========================================
$cond = 'where usable = true';  // 検索条件
$sql = "SELECT create_date FROM sum_med_std_team WHERE ptif_id='$pt_id' AND problem_id=$problem_id "; // AND TO_CHAR(create_date,'YYYY-mm-dd')='{$create_date}'";
$sel = select_from_table($con,$sql,"",$fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
$master_date_list = array();
if( pg_num_rows($sel) != 0 ){
	$master_date_list = pg_fetch_all($sel);
}
//==============================
//ポストバック時
//==============================
if($is_postback == 'true') {
    if ($mode == 'search' || isset($_POST['summary_current_page'])) {    // 検索(ページ数がセットされていたら、検索条件の有無をチェック)
        if (strlen($problem_name) != 0) {
            $cond .= " and problem_name like '%{$problem_name}%'";
        }
        if (strlen($case_name) != 0) {
            $cond .= " and case_name like '%{$case_name}%'";
        }
    }
}

//==============================
//標準計画一覧を取得
//==============================
$data_list = getStandardPlanLibraryData($con, $cond, $fname);
//==============================
// 日付とカレンダー
//==============================

$s_date = date("Ymd"); // 今日
$date_y1 = substr($s_date, 0, 4);
$date_m1 = substr($s_date, 4, 2);
$date_d1 = substr($s_date, 6, 2);

?>
<TITLE>CoMedix マスターメンテナンス | マスタ管理</TITLE>
<script language="JavaScript" type="text/JavaScript">
function reload_page()
{
  location.href = "summary_stdplan_libcopy_list.php?session=<? echo $session; ?>";
}

function search_stdplan_library()
{
    document.mainform.action = 'summary_stdplan_libcopy_list.php?mode=search';
    document.mainform.submit();
}

function summary_page_change(page)
{
    document.getElementById("summary_current_page").value = page;
    document.mainform.submit();
    return false;
}
function selectlib(){
	// すでにチーム計画が存在している場合。(problem_idと日付)
	master_date_list = new Array();
	<?
	foreach($master_date_list as $num=>$col){
		$wk=date("Y-m-d",strtotime($col["create_date"]));
		echo "master_date_list[".$num."]='".$wk."';\n\t";
	}
	?>

	var set_date=document.mainform.selectyy.value+"-"+document.mainform.selectmo.value+"-"+document.mainform.selectdd.value;
	for(var i=0; i<master_date_list.length; i++ ){
		if ( set_date == master_date_list[i] ){
			var a_date=document.mainform.selectyy.value+"年"+document.mainform.selectmo.value+"月"+document.mainform.selectdd.value+"日";
			alert ( a_date + "は、すでにチーム計画が存在します。\nプロブレムと日付を確認してください");
			return ;
		}
	}
	var wkfw_id = "";
	if ( document.mainform.data_select.length ){ // ライブラリのリストが複数件表示されているとき
		var len=document.mainform.data_select.length;
		for(var i=0;i<len;i++){
			if(document.mainform.data_select[i].checked){
				wkfw_id = document.mainform.data_select[i].value;
				break;
			}
		}
	}else{ // ライブラリのリストが1件だけ表示されているとき
		if ( document.mainform.data_select.checked ){
				wkfw_id = document.mainform.data_select.value;
		}
	}
	if ( wkfw_id != "" ){ // ライブラリコピー
		document.mainform.wkfw_id.value=wkfw_id;
		document.mainform.action="summary_stdplan_libcopy_exe.php";
		document.mainform.submit();
	}else{ // ライブラリコピーしないで指定した日付で新規
		parent.opener.teamSelect(<? echo $problem_id; ?>,set_date,"new");
		window.close();
	}
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<?
write_yui_calendar_use_file_read_0_12_2();
write_yui_calendar_script2(1);
?>

<style type="text/css">
.list_table_custom { width:100%; border-collapse:separate; border-spacing:1px; background-color:#9bc8ec; empty-cells:show; }
.list_table_custom tr { background-color:#fff }
.list_table_custom th { padding:5px 1px; font-weight:normal; background:url(css/img/bg-b-gura.gif) top repeat-x; }
.list_table_custom td { padding:1px; }
</style>

</HEAD>

<BODY OnLoad="javascript:if (document.getElementById('cal1Container')) {initcal();}">
<form name="mainform" method="post">

<DIV STYLE="padding:4px">

<!-- ヘッダエリア -->
<? echo summary_common_show_dialog_header("標準チーム計画・ライブラリ選択"); ?>
<BR>
<img src="img/spacer.gif" width="1" height="6" alt="">

<table>
<tr>
<td>立案年月日&nbsp;</td><td>

<select id="date_y1" name="selectyy"><? show_select_years_span(min($date_y1, 2010), max($date_y1, date("Y") + 1), $date_y1, false); ?></select>年
<select id="date_m1" name="selectmo"><? show_select_months($date_m1, true); ?></select>月
<select id="date_d1" name="selectdd"><? show_select_days($date_d1, true); ?></select>日
  <img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"><br><div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>


</td>
<TD ALIGN="CENTER"><input type="button" onclick="selectlib();" value="新規作成"></TD>
<TD>&nbsp;</TD>
</tr>
<tr><td COLSPAN="5">&nbsp;<br></td></tr>

<tr>
<td>プロブレム&nbsp;</td><td colspan="3"><input type="text" id="problem_name" name="problem_name" style="width: 150px;" value="<? echo $problem_name; ?>">&nbsp;<input type="button" value="検索" onclick="search_stdplan_library();">
</td>
</td>
</tr>
</table>
<table width="100%">
<tr>
<td>

<tr>
<?
// ページャー
//$page = max(1 ,(int)@$_REQUEST["summary_current_page"]);
$page = isset($_REQUEST["summary_current_page"]) ? max(1 ,(int)$_REQUEST["summary_current_page"]) : 1;
$rows_per_page = 10;
$total_record = $data_list ? count($data_list) : 0;
if ($page > floor(($total_record - 1) / $rows_per_page) + 1) {
    $page = 1;
}
createPager($total_record, $rows_per_page, $page);
?>

</tr>
<!--一覧表示-->
<table width="100%" CLASS="list_table_custom">
<tr>
 <th align="center" width="10%">選択</th><th align="left" width="90%">プロブレム</th>
</tr>

<?
$start = ($page - 1) * $rows_per_page;
$end = min($total_record, $page * $rows_per_page);
$color_count=0;
for ($i = $start; $i < $end; $i++) {
    if (!$data_list[$i]) {
        break;
    }
    if( $data_list[$i]['usable'] == 'f'){
	continue;
    }
    if ($color_count % 2 == 0) {
        $bgcolor = '#e6e6fa';
    } else {
        $bgcolor = '#f5f5f5';
    }
    $color_count++;
?>
<tr>

<td bgcolor="<? echo $bgcolor; ?>" align="center">
   <input type="radio" name="data_select" value="<? echo $data_list[$i]['med_wkfw_id']; ?>">
</td>
<td bgcolor="<? echo $bgcolor; ?>">
   <? echo h($data_list[$i]['problem_name']); ?>
</td>

</tr>
<?
}
?>

</table>
</td>
</tr>
</table>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" value="<? echo $emp_id;?>">
<input type="hidden" name="team_id" value="">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="summary_current_page" id="summary_current_page" value="<? echo $page; ?>">
<input type="hidden" name="wkfw_id" value="">
<input type="hidden" name="pt_id" value=<? echo $pt_id;?>>
<INPUT TYPE="hidden" NAME="problem_id" VALUE=<? echo $problem_id; ?>>
<input type="hidden" name="create_date" value="">

</form>

</DIV>
<? echo $DEBUG;?>
<DIV ID="debug"></DIV>
</BODY>
</HTML>
<?
/**
 * 標準計画ライブラリデータを取得する
 * 
 * @param resource $con   データベース接続リソース
 * @param string   $cond  条件
 * @param string   $fname ページ名
 * 
 * @return mixed 成功時:取得したレコードの配列/結果の行数が 0だった場合、又はその他のエラーが発生:false
 */
function getStandardPlanLibraryData($con, $cond, $fname)
{
    $sql = "select * from sum_med_std_wkfwmst";
    $cond .= " order by med_wkfw_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        return false;
    }
    $data_list = pg_fetch_all($sel);

    return $data_list;
}

/**
 * ページャー作成
 * 
 * @param integer $total_record  全レコード数
 * @param integer $rows_per_page 1ページあたり表示する件数
 * @param integer $page          表示するページ
 * 
 * @return void
 */
function createPager($total_record, $rows_per_page, $page)
{
    $total_page_count = floor(($total_record-1) / $rows_per_page) + 1;
    $font = "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
    echo "<td>\n";
    echo "<table><tr>\n";
    echo "<td style=\"white-space:nowrap\">{$font}全{$total_record}行</font></td>\n";
    if ($total_page_count > 1) {
        echo "<td style=\"white-space:nowrap\">{$font}";
        echo "<span style=\"color:silver\">";
        if($page > 1) {
            echo "<a href=\"\" onclick=\"return summary_page_change(1);\">\n";
        }
        echo "先頭へ</a></span>\n";
        echo "</font></td>";
        echo "<td style=\"white-space:nowrap\">{$font}";
        echo "<span style=\"color:silver\">";
        if($page > 1) {
            echo "<a href=\"\" onclick=\"return summary_page_change(" . sprintf("%d", $page - 1) . ");\">\n";
        }
        echo "前へ</a></span>\n";
        echo "</font></td>\n";
        echo "<td>{$font}\n";
        for($i = 1; $i <= $total_page_count; $i++) {
            if ($page > $i && $page - $i > 10) {
                continue; 
            }
            if ($page < $i && $i - $page > 10) {
                continue; 
            }
            if($i != $page) {
                echo "<a href=\"\" onclick=\"return summary_page_change({$i});\">[{$i}]</a>\n";
            } else {
                echo "[{$i}]\n";
            }
        }
        echo "</font></td>\n";
        echo "<td style=\"white-space:nowrap\">{$font}\n";
        echo "<span style=\"color:silver\">\n";
        if ($page < $total_page_count) {
            echo "<a href=\"\" onclick=\"return summary_page_change(" . sprintf("%d", $page + 1) . ")\">;";
        } 
        echo "次へ</a></span>";
        echo "</font></td>";
    }
    echo "</tr></table>";
    echo "</td>";
}


pg_close($con);
?>
