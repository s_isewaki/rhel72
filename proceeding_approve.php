<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 委員会・WG | 議事録承認</title>
<?
require_once("about_comedix.php");
require("show_select_values.ini");
require("show_facility.ini");
require_once("library_common.php");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 委員会・WG権限 と 決裁・申請権限のチェック
$checkauth_pjt = check_authority($session, 31, $fname);
$checkauth_aprv = check_authority($session, 7, $fname);
if (($checkauth_pjt == "1")||($checkauth_aprv == "1")) {
	$checkauth = "1";
} else {
	$checkauth = "0";
}

if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 委員会IDを取得
$sql = "select pjt_id from proschd";
$cond = "where pjt_schd_id = '$pjt_schd_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel)) {
	$pjt_id = pg_fetch_result($sel, 0, "pjt_id");
} else {
	$sql = "select pjt_id from proschd2";
	$cond = "where pjt_schd_id = '$pjt_schd_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$pjt_id = pg_fetch_result($sel, 0, "pjt_id");
}

$conf = new Cmx_SystemConfig();
$link_to_yoshiki9 = ($conf->get('project.link_to_yoshiki9') == 't');

// 初期表示時
if ($back != "t") {

	// 議事録情報を取得
	$sql = "select prcd_subject, prcd_place_id, prcd_place_detail, prcd_date, prcd_start_time, prcd_end_time, prcd_person, prcd_absentee, prcd_summary from proceeding";
	$cond = "where pjt_schd_id = '$pjt_schd_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$prcd_subject = pg_fetch_result($sel, 0, "prcd_subject");
	$prcd_place_id = pg_fetch_result($sel, 0, "prcd_place_id");
	$prcd_place_detail = pg_fetch_result($sel, 0, "prcd_place_detail");
	$prcd_date = pg_fetch_result($sel, 0, "prcd_date");
	$prcd_start_time = pg_fetch_result($sel, 0, "prcd_start_time");
	$prcd_end_time = pg_fetch_result($sel, 0, "prcd_end_time");
	$prcd_person = pg_fetch_result($sel, 0, "prcd_person");
	$prcd_absentee = pg_fetch_result($sel, 0, "prcd_absentee");
	$prcd_summary = pg_fetch_result($sel, 0, "prcd_summary");
	$prcd_year = substr($prcd_date, 0, 4);
	$prcd_month = substr($prcd_date, 4, 2);
	$prcd_day = substr($prcd_date, 6, 2);
	$prcd_start_hour = substr($prcd_start_time, 0, 2);
	$prcd_start_min = substr($prcd_start_time, 2, 2);
	$prcd_end_hour = substr($prcd_end_time, 0, 2);
	$prcd_end_min = substr($prcd_end_time, 2, 2);
	
	// 添付ファイル情報を取得
	$sql = "select prcdfile_no, prcdfile_name from prcdfile";
	$cond = "where pjt_schd_id = '$pjt_schd_id' order by prcdfile_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$file_id = array();
	$filename = array();
	while ($row = pg_fetch_array($sel)) {
		$tmp_file_no = $row["prcdfile_no"];
		$tmp_filename = $row["prcdfile_name"];

		array_push($file_id, $tmp_file_no);
		array_push($filename, $tmp_filename);
		// 一時フォルダにコピー
		$ext = strrchr($tmp_filename, ".");
		copy("proceeding/{$pjt_schd_id}_{$tmp_file_no}{$ext}", "proceeding/tmp/{$session}_{$tmp_file_no}{$ext}");
	}

	// 様式9連動時は出席者情報を別テーブルから取得
	if ($link_to_yoshiki9) {
		$sql = "select e.emp_lt_nm, e.emp_ft_nm from prcdatnd a inner join empmst e on a.emp_id = e.emp_id";
		$cond = "where pjt_schd_id = '$pjt_schd_id' order by order_no";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_emp_nms = array();
		while ($row = pg_fetch_array($sel)) {
			$tmp_emp_nms[] = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];
		}
		$prcd_person = implode("、", $tmp_emp_nms);
	}
}

// 30分以上前に保存されたファイルを削除
if (!is_dir("proceeding")) {
	mkdir("proceeding", 0755);
}
if (!is_dir("proceeding/tmp")) {
	mkdir("proceeding/tmp", 0755);
}
foreach (glob("proceeding/tmp/*.*") as $tmpfile) {
	if (time() - filemtime($tmpfile) >= 30 * 60) {
		unlink($tmpfile);
	}
}

// 承認者情報を取得
$sql = "select prcdaprv.prcdaprv_emp_id, prcdaprv.prcdaprv_decide_flg, prcdaprv.prcdaprv_date, empmst.emp_lt_nm, empmst.emp_ft_nm from prcdaprv inner join empmst on prcdaprv.prcdaprv_emp_id = empmst.emp_id";
$cond = "where prcdaprv.pjt_schd_id = '$pjt_schd_id' order by prcdaprv.prcdaprv_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$aprv_cnt = pg_num_rows($sel);
$approve = array();
$approve_id = array();
//$deci_flg = array();
$prcdaprv_date = array();
while ($row = pg_fetch_array($sel)) {
	array_push($approve, "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}");
	array_push($approve_id, $row["prcdaprv_emp_id"]);
//	if ($row["prcdaprv_decide_flg"] == "t") {
//		array_push($deci_flg, $row["prcdaprv_emp_id"]);
//	}
	array_push($prcdaprv_date, $row["prcdaprv_date"]);
}



// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

//ログインユーザの承認状況を取得する

$sql = "select a.prcd_status as prcd_status, b.prcdaprv_date as prcdaprv_date from 
		(select * from proceeding where pjt_schd_id = '$pjt_schd_id') a
		inner join prcdaprv b on a.pjt_schd_id = b.pjt_schd_id and  b.prcdaprv_emp_id = '$emp_id' 
		";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$prcd_status = pg_fetch_result($sel, 0, "prcd_status");
$prcdaprv_dates = pg_fetch_result($sel, 0, "prcdaprv_date");



// 議事録作成者の職員ID,コメントを取得
$sql = "select prcd_create_emp_id, prcd_status, prcd_aprv_comment from proceeding";
$cond = "where pjt_schd_id = '$pjt_schd_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$prcd_create_emp_id = pg_fetch_result($sel, 0, "prcd_create_emp_id");
$prcd_aprv_comment_display = pg_fetch_result($sel, 0, "prcd_aprv_comment");

// 議事録作成者名を取得
$sql = "select emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = '$prcd_create_emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$prcd_create_emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . ' ' . pg_fetch_result($sel, 0, "emp_ft_nm");

// 委員会名（WG名）を取得
$sql = "select project.pjt_name, parent.pjt_name as parent_name from project left join project parent on project.pjt_parent_id = parent.pjt_id";
$cond = "where project.pjt_id = $pjt_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$pjt_name = pg_fetch_result($sel, 0, "pjt_name");
$parent_name = pg_fetch_result($sel, 0, "parent_name");
if ($parent_name != "") {
	$pjt_name = "$parent_name &gt; $pjt_name";
}


// 承認階層数,承認者情報(設定部分)を取得〜配列に格納
$sql = "select * from prcdaprv_mng ";
$cond = "where pjt_schd_id = '$pjt_schd_id' order by prcdaprv_order";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == "0") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$approve_cnt = pg_numrows($sel);
$arr_def_mng_ap = array();
$cnt = 0;
while ($row = pg_fetch_array($sel)) 
{
	$arr_def_mng_ap[$cnt]["notice"] = $row["next_notice_div"];
	$arr_def_mng_ap[$cnt]["order"] =  $row["prcdaprv_order"];
	$cnt++;
	
}



// 承認者情報を取得〜配列に格納
$sql = "select prcdaprv.prcdaprv_order, prcdaprv.prcdaprv_emp_id, prcdaprv.prcdaprv_decide_flg, prcdaprv.substitute_flg,
prcdaprv.prcdaprv_date, empmst.emp_lt_nm, empmst.emp_ft_nm from prcdaprv inner join empmst on prcdaprv.prcdaprv_emp_id = empmst.emp_id";
$cond = "where prcdaprv.pjt_schd_id = '$pjt_schd_id' order by prcdaprv.prcdaprv_order, prcdaprv.prcdaprv_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$aprv_cnt = pg_num_rows($sel);
$approve = array();
$approve_id = array();
//$deci_flg = array();




$arr_def_ap_user = array();
$cnt = 0;
$level_cnt_forward = "";
while ($row = pg_fetch_array($sel)) 
{
	$tmp_emp_nm = "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}";
	
	
	if($row["prcdaprv_order"] == "")
	{
		$level_cnt = 0;
	}
	else
	{
		$level_cnt = ($row["prcdaprv_order"] - 1);
	}
	
	if($cnt == 0)
	{
		//初期値設定
		$level_cnt_forward = $level_cnt;
	}
	else
	{
		if($level_cnt_forward != $level_cnt)
		{
			//階層が変わったので配列を変えます
			$level_cnt_forward = $level_cnt;
			$cnt = 0;
		}
	}
	
	if(($row["prcdaprv_date"] != "")&&($row["prcdaprv_date"] != "00000000"))
	{
		
		if($row["substitute_flg"] == "")
		{
			//代理承認ではなく、自分で承認しました
			
			$str_aprv_date = "(承認済&nbsp;" . preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $row["prcdaprv_date"]).")";
			//echo("承認済&nbsp;" . preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $row["prcdaprv_date"]));
		}
		else
		{
			$str_aprv_date = "(代理承認済)";
		}
	}
	elseif($row["prcdaprv_date"] == "00000000")
	{
		//差戻しデータなので、「差戻」を表示する
		$str_aprv_date = "(差戻)";
	}
	else
	{
		$str_aprv_date = "";
	}
	
	$arr_def_ap_user[$level_cnt][$cnt]["name"] = $tmp_emp_nm.$str_aprv_date;
	//$arr_def_mng_ap[$cnt]["no"] =$row["prcdaprv_no"];
	
	$cnt++;
	
}



?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
if(!tinyMCE.isOpera)
{
	tinyMCE.init({
		mode : "exact",
		elements : "prcd_summary",
		theme : "advanced",
		plugins : "preview,table,emotions,fullscreen,layer",
		//language : "ja_euc-jp",
		language : "ja",
		width : "100%",
		height : "300",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat",
		theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,|,hr,|,charmap,emotions,|,preview,|,undo,redo,|,fullscreen",
		theme_advanced_buttons3 : "tablecontrols,|,visualaid",
		content_css : "tinymce/tinymce_content.css",
		theme_advanced_statusbar_location : "none",
		force_br_newlines : true,
		forced_root_block : '',
		force_p_newlines : false
	});
}

//印刷
function printPage() {
	window.open('proceeding_print.php?session=<? echo($session); ?>&pjt_schd_id=<? echo($pjt_schd_id); ?>', 'proprint', 'width=640,height=700,scrollbars=yes');
}

//PDF印刷
function printPDF() {
	window.open('proceeding_print.php?session=<? echo($session); ?>&pjt_schd_id=<? echo($pjt_schd_id); ?>&direct_template_pdf_print_requested=1', 'proprint', 'width=640,height=700,scrollbars=yes');
	//document.frm_pdf_print.submit() ;
}

function checkEndMinute() {
	if (document.prcd.prcd_end_hour.value == '24' &&
		document.prcd.prcd_end_min.value != '00') {
		document.prcd.prcd_end_min.value = '00';
		alert('終了時刻が24時の場合には00分しか選べません。');
	}
}

function onRemand() {

	var str = document.prcd.prcd_aprv_comment.value;
	var trimmed_str = str.replace(/^[ 　\t\r\n]+|[ 　\t\r\n]+$/g, "");
	if(trimmed_str == "")
	{
		alert('差戻し理由をコメント欄に入力してください。');
		return false;
	}
	//alert(trimmed_str);

	if(!confirm("差戻します。よろしいですか"))
	{
		return false;
	}

	//下書き保存用のフラグを設定する。
	document.prcd.status_flg.value = "remand";
	document.prcd.submit();

}

function onApprove() {
	//登録用のフラグを設定する。
	document.prcd.status_flg.value = "approve";

	document.prcd.submit();
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>議事録承認</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="prcd" action="proceeding_approve_exe.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right">
  <input type="button" value="印刷" onclick="printPage();" />
  <input type="button" value="PDF印刷" onclick="printPDF();" />
</td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="100" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG名</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pjt_name); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">議題</font></td>
<td><input type="text" name="prcd_subject" value="<? echo($prcd_subject); ?>" size="65" maxlength="200" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">場所</font></td>
<td><select name="prcd_place_id"><? show_schedule_place_selected($con, $prcd_place_id, $fname); ?><option value="0"<? if ($prcd_place_id == 0) {echo(" selected");} ?>>その他</select>&nbsp;<input type="text" name="prcd_place_detail" value="<? echo($prcd_place_detail); ?>" size="35" maxlength="100" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="prcd_year"><? show_update_years($prcd_year, 1); ?></select>/<select name="prcd_month"><? show_select_months($prcd_month); ?></select>/<select name="prcd_day"><? show_select_days($prcd_day); ?></select></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="prcd_start_hour"><? show_hour_options_0_23($prcd_start_hour, false); ?></select>：<select name="prcd_start_min"><? show_min_options_5($prcd_start_min, false); ?></select>〜<select name="prcd_end_hour" onchange="checkEndMinute();"><? show_select_hrs_by_args(0, 24, $prcd_end_hour); ?></select>：<select name="prcd_end_min" onchange="checkEndMinute();"><? show_min_options_5($prcd_end_min, false); ?></select></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出席者</font></td>
<td><textarea name="prcd_person" cols="50" rows="3" style="ime-mode:active;"<? if ($link_to_yoshiki9) {echo(" readonly");} ?>><? echo($prcd_person); ?></textarea></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">欠席者</font></td>
<td><textarea name="prcd_absentee" cols="50" rows="3" style="ime-mode:active;"><? echo($prcd_absentee); ?></textarea></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">記録者</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($prcd_create_emp_nm); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">概要</font></td>
<td><textarea name="prcd_summary" cols="50" rows="20" style="ime-mode:active;"><? echo($prcd_summary); ?></textarea></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">添付ファイル</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<div id="attach">
<?
// ダウンロード文書名の設定を取得（文書管理で設定されている内容を使用）
$filename_flg = lib_get_filename_flg();
if ($filename_flg == 0) {
	$target = "target=\"_blank\"";
} else {
	$target = "";
}

for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$ext = strrchr($tmp_filename, ".");

	echo("<p id=\"p_{$tmp_file_id}\">\n");
//	echo("<a href=\"proceeding/{$pjt_schd_id}_{$tmp_file_id}{$ext}\" target=\"_blank\">{$tmp_filename}</a>\n");
	echo("<a href=\"proceeding_attach_refer.php?session=$session&file_id=$tmp_file_id&filename=" . urlencode($tmp_filename) . "\" $target>$tmp_filename</a>\n");
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"{$tmp_filename}\">\n");
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"{$tmp_file_id}\">\n");
	echo("</p>\n");
}
?>
</div>
</font></td>
</tr>

<?

if(($aprv_cnt > 0) && ($approve_cnt < 1))
{
	//途中から階層が追加されたので追加前の承認者をデフォルトで1階層に設定する
	$approve_cnt = 1;
}
for ($i = 0; $i < $approve_cnt; $i++) 
{
	
	if($arr_def_ap_user[$i][0]["name"] != "" )
	{
		
		for ($wcnt1 = 0; $wcnt1 < count($arr_def_ap_user[$i]); $wcnt1++)
		{
			if($wcnt1 == 0)
			{
				$str_emp_nm = $arr_def_ap_user[$i][$wcnt1]["name"];
			}
			else
			{
				$str_emp_nm = $str_emp_nm.", ".$arr_def_ap_user[$i][$wcnt1]["name"];
			}			
		}
	}
	else
	{
		$str_emp_nm = "";
	}
	
	$str_dis_notice = "";
	if($arr_def_mng_ap[$i]["notice"] == "2")
	{
		$str_dis_notice = "(並列)";
	}
	else
	{
		$str_dis_notice = "(同期)";
	}
	
?>

<tr height="22">
	<td align="right" bgcolor="#f6f9ff">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認階層<?=$i+1?><?=$str_dis_notice?></font>
	</td>
	<td>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$str_emp_nm?></font>
	</td>
</tr>
<?
}
?>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認/差戻コメント</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($prcd_aprv_comment_display); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コメント入力</font></td>
<td><textarea name="prcd_aprv_comment" cols="60" rows="5" style="ime-mode:active;"><? echo($prcd_aprv_comment); ?></textarea></td>
</tr>

</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="left"><input type="button" value="差戻し" onclick="onRemand()"></td>

<td align="right">
<?

	if(($prcd_status == "1") &&($prcdaprv_dates == NULL))
	{
	//自分が未承認の場合のみボタンを表示する
?>
<input type="button" value="承認" onclick="onApprove()">
<?
	}
?>


</td>


</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pjt_schd_id" value="<? echo($pjt_schd_id); ?>">
<input type="hidden" name="pnt_url" value="<? echo($pnt_url); ?>">
<input type="hidden" name="status_flg" value="">
<input type="hidden" name="prcd_aprv_comment_display" value="<? echo($prcd_aprv_comment_display); ?>">

</form>
</center>
</body>
<? pg_close($con); ?>
</html>

<?
$approve_cnt = 0;
if($mode == 'show_flg_update') {

	$sql = "select * from proceeding";
	$cond = "where pjt_schd_id = '$pjt_schd_id' and apv_fix_show_flg = 't' and (prcd_status = '2' or  prcd_status = '3')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$approve_cnt = pg_numrows($sel);
	if($approve_cnt > 0) {


		// トランザクションを開始
		pg_query($con, "begin");

		$sql = "update proceeding set";
		$set = array("apv_fix_show_flg");
		$setvalue = array('f');
		$cond = "where pjt_schd_id = '$pjt_schd_id' and apv_fix_show_flg = 't' and (prcd_status = '2' or  prcd_status = '3')";
		$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if($up ==0){
			pg_exec($con,"rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		// トランザクションをコミット
		pg_query($con, "commit");
	} else {
	// 一部承認のフラグ更新確認
		$sql = "select pjt_schd_id from prcdaprv";
		$cond = "where pjt_schd_id = '$pjt_schd_id' and apv_fix_show_flg = 't' and prcdaprv_date is not null";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$approve_cnt = pg_numrows($sel);
		if($approve_cnt > 0) {

			// トランザクションを開始
			pg_query($con, "begin");

			$sql = "update prcdaprv set";
			$set = array("apv_fix_show_flg");
			$setvalue = array('f');
			$cond = "where pjt_schd_id = '$pjt_schd_id' and apv_fix_show_flg = 't' and prcdaprv_date is not null";
			$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if($up ==0){
				pg_exec($con,"rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			// トランザクションをコミット
			pg_query($con, "commit");

		}
	}
}
pg_close($con);
?>


<script type="text/javascript">
<!--
function page_close() {
<?
if($mode == 'show_flg_update' && $approve_cnt > 0) {
?>
	if(window.opener && !window.opener.closed && window.opener.reload_page) {
		window.opener.reload_page();
		window.close();
	} else {
		window.close();
	}
<?
} else {
?>
	window.close();
<?
}
?>

}
//-->
</script>