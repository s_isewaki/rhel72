<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 文書管理 | 文書更新</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("get_values.ini");
require_once("show_class_name.ini");
require_once("referer_common.ini");
require_once("menu_common.ini");
require_once("library_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$auth_id = ($path == "3") ? 63 : 32;
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 文書管理管理者権限を取得
$lib_admin_auth = check_authority($session, 63, $fname);

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "library", $fname);

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$intra_menu2 = pg_fetch_result($sel, 0, "menu2");
$intra_menu2_3 = pg_fetch_result($sel, 0, "menu2_3");

// ログインユーザの職員情報を取得
$sql = "select emp_id, emp_class, emp_attribute, emp_dept, emp_st, (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel_emp = select_from_table($con, $sql, $cond, $fname);
if ($sel_emp == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp_id = pg_fetch_result($sel_emp, 0, "emp_id");
$emp_class = pg_fetch_result($sel_emp, 0, "emp_class");
$emp_atrb = pg_fetch_result($sel_emp, 0, "emp_attribute");
$emp_dept = pg_fetch_result($sel_emp, 0, "emp_dept");
$emp_st = pg_fetch_result($sel_emp, 0, "emp_st");
$emp_name = pg_fetch_result($sel_emp, 0, "emp_name");

$IS_OWNERS = "";
if (defined("IS_OWNERS_CLUB")) {
    $IS_OWNERS = "1";
    if ($emp_class=="1") $IS_OWNERS = "";
}

// 更新可能フラグの設定
if ($path == "3") {
    $upd_flg = true;
} else {
    // 文書の更新権限取得
    $sql = "select count(*) from libinfo inner join (select * from libcate where libcate.libcate_del_flg = 'f') libcate on libinfo.lib_cate_id = libcate.lib_cate_id";
    $cond = "where lib_id = $lib_id";
    // 個人の場合
    if ($a == "1") {
        $cond .= " and (libinfo.lib_archive = '1' and exists (select * from libcate where libcate.lib_cate_id = libinfo.lib_cate_id and libcate.lib_link_id = '$emp_id'))";
    }
    // 部署の場合、兼務所属と職員も確認
    if ($a == "3") {
        $cond .= " and (libinfo.lib_archive = '3' and (exists (select * from libcate where libcate.lib_cate_id = libinfo.lib_cate_id and libcate.lib_link_id = '$emp_class') or exists (select * from concurrent where concurrent.emp_id = '$emp_id' and to_char(concurrent.emp_class,'FM999999999999') = libcate.lib_link_id) )";
        // 職員
        $cond .= " or exists (select * from libupdemp where libupdemp.lib_id = libinfo.lib_id and libupdemp.emp_id = '$emp_id')";
        $cond .= ")";
    }
    // 委員会の場合
    if ($a == "4") {
        $cond .= " and (libinfo.lib_archive = '4' and (exists (select * from project where project.pjt_response = '$emp_id' and project.pjt_delete_flag = 'f' and to_char(project.pjt_id,'FM999999999999') = libcate.lib_link_id) or exists (select * from promember where promember.emp_id = '$emp_id' and exists (select * from project where project.pjt_delete_flag = 'f' and to_char(project.pjt_id,'FM999999999999') = libcate.lib_link_id) and to_char(promember.pjt_id,'FM999999999999') = libcate.lib_link_id))";
        // 職員
        $cond .= " or exists (select * from libupdemp where libupdemp.lib_id = libinfo.lib_id and libupdemp.emp_id = '$emp_id')";
        $cond .= ")";
    }
    // 全体、部署の場合
    if ($a == "2" || $a == "3") {
        $cond .= " and (";
            $cond .= "(";
                $cond .= "(libinfo.upd_dept_flg = '1' or (libinfo.upd_dept_flg = '2' and exists (select * from libupddept where libupddept.lib_id = libinfo.lib_id and libupddept.class_id = $emp_class and libupddept.atrb_id = $emp_atrb and libupddept.dept_id = $emp_dept)))";
                $cond .= " and ";
                $cond .= "(libinfo.upd_st_flg = '1' or (libinfo.upd_st_flg = '2' and exists (select * from libupdst where libupdst.lib_id = libinfo.lib_id and libupdst.st_id = $emp_st)))";
            $cond .= ")";
            // 兼務所属
            $cond .= " or ";
            $cond .= "(";
                $cond .= "(libinfo.upd_dept_flg = '1' or (libinfo.upd_dept_flg = '2' and exists (select * from concurrent, libupddept where  concurrent.emp_id = '$emp_id' and concurrent.emp_class = libupddept.class_id and concurrent.emp_attribute = libupddept.atrb_id and concurrent.emp_dept = libupddept.dept_id and libupddept.lib_id = libinfo.lib_id)))";
                $cond .= " and ";
                $cond .= "(libinfo.upd_st_flg = '1' or (libinfo.upd_st_flg = '2' and exists (select * from concurrent, libupdst where concurrent.emp_id = '$emp_id' and libupdst.st_id = concurrent.emp_st and libupdst.lib_id = libinfo.lib_id)))";
            $cond .= ")";
            // 職員
            $cond .= " or exists (select * from libupdemp where libupdemp.lib_id = libinfo.lib_id and libupdemp.emp_id = '$emp_id')";
        $cond .= ")";
    }
    $sel_upd_flg = select_from_table($con, $sql, $cond, $fname);
    if ($sel_upd_flg == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $upd_flg = (pg_fetch_result($sel_upd_flg, 0, 0) > 0);
}

// 部門一覧を取得
$sql = "select class_id, class_nm from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$classes = array();
$classes["-"] = "----------";
while ($row = pg_fetch_array($sel_class)) {
    $tmp_class_id = $row["class_id"];
    $tmp_class_nm = $row["class_nm"];
    $classes[$tmp_class_id] = $tmp_class_nm;
}
pg_result_seek($sel_class, 0);

// 課一覧を取得
$sql = "select classmst.class_id, atrbmst.atrb_id, classmst.class_nm, atrbmst.atrb_nm from atrbmst inner join classmst on atrbmst.class_id = classmst.class_id";
$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' order by classmst.order_no, atrbmst.order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);
if ($sel_atrb == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 科一覧を取得
$sql = "select classmst.class_id, atrbmst.atrb_id, deptmst.dept_id, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm from deptmst inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id inner join classmst on atrbmst.class_id = classmst.class_id";
$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' and deptmst.dept_del_flg = 'f' order by classmst.order_no, atrbmst.order_no, deptmst.order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
if ($sel_dept == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$dept_names = Array();
while ($row = pg_fetch_array($sel_dept)) {
    $tmp_class_id = $row["class_id"];
    $tmp_atrb_id = $row["atrb_id"];
    $tmp_dept_id = $row["dept_id"];
    $tmp_dept_nm = "{$row['class_nm']}＞{$row['atrb_nm']}＞{$row['dept_nm']}";
    $dept_names["{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"] = $tmp_dept_nm;
}
pg_result_seek($sel_dept, 0);

// 役職一覧を取得
$sql = "select st_id, st_nm from stmst";
$cond = "where st_del_flg = 'f' order by order_no";
$sel_st = select_from_table($con, $sql, $cond, $fname);
if ($sel_st == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 組織名を取得
$arr_class_name = get_class_name_array($con, $fname);

// 文書情報を取得
$sql = "select libinfo.*, libcate.lib_cate_nm, classmst.class_nm from ((libinfo inner join libcate on libinfo.lib_cate_id = libcate.lib_cate_id) inner join empmst on libinfo.emp_id = empmst.emp_id) inner join classmst on empmst.emp_class = classmst.class_id";
$cond = "where libinfo.lib_id = '$lib_id'";
$sel_lib = select_from_table($con, $sql, $cond, $fname);
if ($sel_lib == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$reg_emp_id = pg_fetch_result($sel_lib, 0, "emp_id");
$reg_emp_name = get_emp_kanji_name($con, $reg_emp_id, $fname);
$reg_class_nm = pg_fetch_result($sel_lib, 0, "class_nm");
$upd_time = pg_fetch_result($sel_lib, 0, "lib_up_date");
$apply_id = pg_fetch_result($sel_lib, 0, "apply_id");

if (strlen($upd_time) == 14) {
    $upd_time = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5:$6", $upd_time);
} else {
    $upd_time = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $upd_time);
}
$ref_count = pg_fetch_result($sel_lib, 0, "ref_count");

// 文書ファイルのパス
$document_path = get_document_path(
    pg_fetch_result($sel_lib, 0, "lib_archive"),
    pg_fetch_result($sel_lib, 0, "lib_cate_id"),
    $lib_id,
    pg_fetch_result($sel_lib, 0, "lib_extension")
);
$lib_url = "library_refer.php?s=$session&i=$lib_id&u=" . urlencode($document_path);

// 版番号を取得
$sql = "select edition_no from libedition";
$cond = "where lib_id = $lib_id";
$sel_edition = select_from_table($con, $sql, $cond, $fname);
if ($sel_edition == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$edition_no = @pg_fetch_result($sel_edition, 0, "edition_no");

// 初期表示時の設定
if (!is_array($ref_dept)) {$ref_dept = array();}
if (!is_array($ref_st)) {$ref_st = array();}
if (!is_array($upd_dept)) {$upd_dept = array();}
if (!is_array($upd_st)) {$upd_st = array();}
if ($back != "t") {
    $archive = pg_fetch_result($sel_lib, 0, "lib_archive");
    $category = pg_fetch_result($sel_lib, 0, "lib_cate_id");
    $folder_id = pg_fetch_result($sel_lib, 0, "folder_id");
    $document_name = pg_fetch_result($sel_lib, 0, "lib_nm");
    $document_type = pg_fetch_result($sel_lib, 0, "lib_type");
    $keywd = pg_fetch_result($sel_lib, 0, "lib_keyword");
    $lib_no = pg_fetch_result($sel_lib, 0, "lib_no");
    $explain = pg_fetch_result($sel_lib, 0, "lib_summary");
    $show_login_flg = pg_fetch_result($sel_lib, 0, "show_login_flg");
    $show_login_begin = pg_fetch_result($sel_lib, 0, "show_login_begin");
    $show_login_end = pg_fetch_result($sel_lib, 0, "show_login_end");
    $private_flg = pg_fetch_result($sel_lib, 0, "private_flg");
    $ref_dept_st_flg = pg_fetch_result($sel_lib, 0, "ref_dept_st_flg");
    $ref_dept_flg = pg_fetch_result($sel_lib, 0, "ref_dept_flg");
    $ref_st_flg = pg_fetch_result($sel_lib, 0, "ref_st_flg");
    $upd_dept_st_flg = pg_fetch_result($sel_lib, 0, "upd_dept_st_flg");
    $upd_dept_flg = pg_fetch_result($sel_lib, 0, "upd_dept_flg");
    $upd_st_flg = pg_fetch_result($sel_lib, 0, "upd_st_flg");
    $show_mypage_flg = pg_fetch_result($sel_lib, 0, "show_mypage_flg");

    switch ($archive) {
    case "1":  // 個人ファイル
    case "2":  // 全体共有
        $private_flg1 = "";
        $private_flg2 = "";
        break;
    case "3":  // 部署共有
        $private_flg1 = $private_flg;
        $private_flg2 = "";
        break;
    case "4":  // 委員会・WG共有
        $private_flg1 = "";
        $private_flg2 = $private_flg;
        break;
    }

    $show_login_begin1 = substr($show_login_begin, 0, 4);
    $show_login_begin2 = substr($show_login_begin, 4, 2);
    $show_login_begin3 = substr($show_login_begin, 6, 2);
    $show_login_end1 = substr($show_login_end, 0, 4);
    $show_login_end2 = substr($show_login_end, 4, 2);
    $show_login_end3 = substr($show_login_end, 6, 2);

    $sql = "select l.class_id, l.atrb_id, l.dept_id from librefdept l left join classmst c on l.class_id = c.class_id left join atrbmst a on l.atrb_id = a.atrb_id left join deptmst d on l.dept_id = d.dept_id";
    $cond = "where l.lib_id = $lib_id order by c.order_no, a.order_no, d.order_no";
    $sel_ref_dept = select_from_table($con, $sql, $cond, $fname);
    if ($sel_ref_dept == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel_ref_dept)) {
        $tmp_class_id = $row["class_id"];
        $tmp_atrb_id = $row["atrb_id"];
        $tmp_dept_id = $row["dept_id"];
        $ref_dept[] = "$tmp_class_id-$tmp_atrb_id-$tmp_dept_id";
    }

    $sql = "select st_id from librefst";
    $cond = "where lib_id = $lib_id order by st_id";
    $sel_ref_st = select_from_table($con, $sql, $cond, $fname);
    if ($sel_ref_st == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel_ref_st)) {
        $tmp_st_id = $row["st_id"];
        $ref_st[] = $tmp_st_id;
    }

    $sql = "select emp_id from librefemp";
    $cond = "where lib_id = $lib_id order by emp_id";
    $sel_ref_emp = select_from_table($con, $sql, $cond, $fname);
    if ($sel_ref_emp == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $target_id_list1 = "";
    $is_first_flg = true;
    while ($row = pg_fetch_array($sel_ref_emp)) {
        $tmp_emp_id = $row["emp_id"];
        if ($is_first_flg) {
            $is_first_flg = false;
        } else {
            $target_id_list1 .= ",";
        }
        $target_id_list1 .= $tmp_emp_id;
    }

    $sql = "select l.class_id, l.atrb_id, l.dept_id from libupddept l left join classmst c on l.class_id = c.class_id left join atrbmst a on l.atrb_id = a.atrb_id left join deptmst d on l.dept_id = d.dept_id";
    $cond = "where l.lib_id = $lib_id order by c.order_no, a.order_no, d.order_no";
    $sel_upd_dept = select_from_table($con, $sql, $cond, $fname);
    if ($sel_upd_dept == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel_upd_dept)) {
        $tmp_class_id = $row["class_id"];
        $tmp_atrb_id = $row["atrb_id"];
        $tmp_dept_id = $row["dept_id"];
        $upd_dept[] = "$tmp_class_id-$tmp_atrb_id-$tmp_dept_id";
    }

    $sql = "select st_id from libupdst";
    $cond = "where lib_id = $lib_id order by st_id";
    $sel_upd_st = select_from_table($con, $sql, $cond, $fname);
    if ($sel_upd_st == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel_upd_st)) {
        $tmp_st_id = $row["st_id"];
        $upd_st[] = $tmp_st_id;
    }

    $sql = "select emp_id from libupdemp";
    $cond = "where lib_id = $lib_id order by emp_id";
    $sel_upd_emp = select_from_table($con, $sql, $cond, $fname);
    if ($sel_upd_emp == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $target_id_list2 = "";
    $is_first_flg = true;
    while ($row = pg_fetch_array($sel_upd_emp)) {
        $tmp_emp_id = $row["emp_id"];
        if ($is_first_flg) {
            $is_first_flg = false;
        } else {
            $target_id_list2 .= ",";
        }
        $target_id_list2 .= $tmp_emp_id;
    }
}

// デフォルト値の設定
if ($manage_history == "") {$manage_history = "t";}
if ($upd_dept_flg != "1" && $upd_dept_flg != "2") {$upd_dept_flg = "1";}
if ($upd_st_flg != "1" && $upd_st_flg != "2") {$upd_st_flg = "1";}
if ($ref_dept_flg != "1" && $ref_dept_flg != "2") {$ref_dept_flg = "1";}
if ($ref_st_flg != "1" && $ref_st_flg != "2") {$ref_st_flg = "1";}

// 書庫名を取得
$archive_nm = lib_get_archive_name($archive);

// カテゴリ名を取得
$cate_nm = lib_get_category_name($con, $category, $fname);

// フォルダのパスを取得（フォルダ未指定の場合は空配列）
$folder_path = lib_get_folder_path($con, $folder_id, $fname, $archive);

// 最新文書一覧表示フラグを取得
$newlist_flg = lib_get_show_newlist_flg($con, $fname);

if ($ref_toggle_mode == "") {$ref_toggle_mode = "▼";}
$ref_toggle_display = ($ref_toggle_mode == "▼") ? "none" : "";
if ($upd_toggle_mode == "") {$upd_toggle_mode = "▼";}
$upd_toggle_display = ($upd_toggle_mode == "▼") ? "none" : "";

// メンバー情報を配列に格納
for ($list_idx=1; $list_idx<=2; $list_idx++) {
    $arr_target["$list_idx"] = array();
    $target_id_list_name = "target_id_list$list_idx";
    $target_id_list = $$target_id_list_name;
    if ($target_id_list != "") {
        $arr_target_id = split(",", $target_id_list);
        for ($i = 0; $i < count($arr_target_id); $i++) {
            $tmp_emp_id = $arr_target_id[$i];
            $sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
            $cond = "where emp_id = '$tmp_emp_id'";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_close($con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
            }
            $tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
            array_push($arr_target["$list_idx"], array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
        }
    }
}

?>
<?
require("emplist_caller_javascript.php");
insert_javascript();
?>
<script type="text/javascript">
var childwin = null;
function openEmployeeList(item_id) {
    dx = screen.availWidth - 10;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = './emplist_popup.php';
    url += '?session=<?=$session?>';
    url += '&emp_id=<?=$emp_id?>';
    url += '&mode='+item_id;
    url += '&item_id='+item_id;
    childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    childwin.focus();
}

function closeEmployeeList() {
    if (childwin != null && !childwin.closed) {
        childwin.close();
    }
    childwin = null;
}

//--------------------------------------------------
//登録対象者一覧
//--------------------------------------------------
<?
    for ($i=1; $i<=2; $i++) {
        $script = "m_target_list['$i'] = new Array(";
        $is_first = true;
        foreach($arr_target["$i"] as $row)
        {
            if($is_first)
            {
                $is_first = false;
            }
            else
            {
                $script .= ",";
            }
            $tmp_emp_id = $row["id"];
            $tmp_emp_name = $row["name"];
            $script .= "new user_info('$tmp_emp_id','$tmp_emp_name')";
        }
        $script .= ");\n";
        print $script;
    }
?>

// クリア
function clear_target(item_id, emp_id,emp_name) {
    if(confirm("登録対象者を削除します。よろしいですか？"))
    {
        var is_exist_flg = false;
        for(var i=0;i<m_target_list[item_id].length;i++)
        {
            if(emp_id == m_target_list[item_id][i].emp_id)
            {
                is_exist_flg = true;
                break;
            }
        }
        m_target_list[item_id] = new Array();
        if (is_exist_flg == true) {
            m_target_list[item_id] = array_add(m_target_list[item_id],new user_info(emp_id,emp_name));
        }
        update_target_html(item_id);
    }
}

var classes = [];
<?
while ($row = pg_fetch_array($sel_class)) {
    $tmp_class_id = $row["class_id"];
    $tmp_class_nm = $row["class_nm"];
    echo("classes.push({id: $tmp_class_id, name: '$tmp_class_nm'});\n");
}
pg_result_seek($sel_class, 0);
?>

var atrbs = {};
<?
$pre_class_id = "";
while ($row = pg_fetch_array($sel_atrb)) {
    $tmp_class_id = $row["class_id"];
    $tmp_atrb_id = $row["atrb_id"];
    $tmp_class_nm = $row["class_nm"];
    $tmp_atrb_nm = $row["atrb_nm"];

    if ($tmp_class_id != $pre_class_id) {
        echo("atrbs[$tmp_class_id] = [];\n");
    }

    echo("atrbs[$tmp_class_id].push({id: $tmp_atrb_id, name: '$tmp_atrb_nm'});\n");

    $pre_class_id = $tmp_class_id;
}
pg_result_seek($sel_atrb, 0);
?>

var depts = {};
<?
$pre_class_id = "";
$pre_atrb_id = "";
while ($row = pg_fetch_array($sel_dept)) {
    $tmp_class_id = $row["class_id"];
    $tmp_atrb_id = $row["atrb_id"];
    $tmp_dept_id = $row["dept_id"];
    $tmp_class_nm = $row["class_nm"];
    $tmp_atrb_nm = $row["atrb_nm"];
    $tmp_dept_nm = $row["dept_nm"];

    if ($tmp_class_id != $pre_class_id) {
        echo("depts[$tmp_class_id] = {};\n");
    }

    if ($tmp_class_id != $pre_class_id || $tmp_atrb_id != $pre_atrb_id) {
        echo("depts[$tmp_class_id][$tmp_atrb_id] = [];\n");
    }

    echo("depts[$tmp_class_id][$tmp_atrb_id].push({id: $tmp_dept_id, hierarchy: '{$tmp_class_nm}＞{$tmp_atrb_nm}＞', name: '$tmp_dept_nm'});\n");

    $pre_class_id = $tmp_class_id;
    $pre_atrb_id = $tmp_atrb_id;
}
pg_result_seek($sel_dept, 0);
?>

//----------

var ARCHIVE_ALL = '2';
var ARCHIVE_SECTION = '3';
var ARCHIVE_PROJECT = '4';
var ARCHIVE_PRIVATE = '1';

var dept_win = null;

function initPage() {
    try{ parent.scrollTo(0,0); } catch(ex){}

    //登録対象者を設定する。
    update_target_html("1");
    update_target_html("2");

    setManageHistoryButtonDisabled();
    onChangeArchive(true, '<? echo($ref_class_src); ?>', '<? echo($ref_atrb_src); ?>',  '<? echo($upd_class_src); ?>', '<? echo($upd_atrb_src); ?>');

    if (!(document.mainform.upd_dept.options[0])) {
        document.mainform.upd_dept.style.width = '100%';
    }

    if (!(document.mainform.ref_dept.options[0])) {
        document.mainform.ref_dept.style.width = '100%';
    }
}

function onChangeArchive(
    init_flg,
    default_ref_class_src,
    default_ref_atrb_src,
    default_upd_class_src,
    default_upd_atrb_src
) {
    if (!init_flg) document.mainform.private_flg1.checked = false;
    setRefClassSrcOptions(init_flg, default_ref_class_src, default_ref_atrb_src);
    setUpdClassSrcOptions(init_flg, default_upd_class_src, default_upd_atrb_src);
    setDisplay();
    setDisabled();
}

function setRefClassSrcOptions(init_flg, default_ref_class_src, default_ref_atrb_src) {
    if (!init_flg) {
        deleteAllOptions(document.mainform.ref_dept);
    }

    deleteAllOptions(document.mainform.ref_class_src);

    var archive = document.mainform.archive.value;
    if (document.mainform.private_flg1.checked) {
        var value = '<? echo($emp_class); ?>';
        for (var i = 0, len = classes.length; i < len; i++) {
            if (classes[i].id != value) continue;
            addOption(document.mainform.ref_class_src, value, classes[i].name);
        }
    } else {
        addOption(document.mainform.ref_class_src, '-', '----------', default_ref_class_src);
        for (var i = 0, len = classes.length; i < len; i++) {
            addOption(document.mainform.ref_class_src, classes[i].id, classes[i].name, default_ref_class_src);
        }
    }

    setRefAtrbSrcOptions(default_ref_atrb_src);
}

function setRefAtrbSrcOptions(default_ref_atrb_src) {
    deleteAllOptions(document.mainform.ref_atrb_src);

    addOption(document.mainform.ref_atrb_src, '-', '----------', default_ref_atrb_src);

    var class_id = document.mainform.ref_class_src.value;
    if (atrbs[class_id]) {
        for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
            addOption(document.mainform.ref_atrb_src, atrbs[class_id][i].id, atrbs[class_id][i].name, default_ref_atrb_src);
        }
    }

    setRefDeptSrcOptions();
}

function setRefDeptSrcOptions() {
    deleteAllOptions(document.mainform.ref_dept_src);

    var class_id = document.mainform.ref_class_src.value;
    var atrb_id = document.mainform.ref_atrb_src.value;
    if (depts[class_id]) {
        if (depts[class_id][atrb_id]) {
            for (var i = 0, len = depts[class_id][atrb_id].length; i < len; i++) {
                var dept_id = depts[class_id][atrb_id][i].id;
                var value = class_id + '-' + atrb_id + '-' + dept_id;
                addOption(document.mainform.ref_dept_src, value, depts[class_id][atrb_id][i].name);
            }
        } else if (atrb_id == '-') {
            for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
                var atrb_id = atrbs[class_id][i].id;
                if (depts[class_id][atrb_id]) {
                    for (var j = 0, len2 = depts[class_id][atrb_id].length; j < len2; j++) {
                        var dept_id = depts[class_id][atrb_id][j].id;
                        var value = class_id + '-' + atrb_id + '-' + dept_id;
                        addOption(document.mainform.ref_dept_src, value, depts[class_id][atrb_id][j].name);
                    }
                }
            }
        }
    }
}

function setUpdClassSrcOptions(init_flg, default_upd_class_src, default_upd_atrb_src) {
    if (!init_flg) {
        deleteAllOptions(document.mainform.upd_dept);
    }

    deleteAllOptions(document.mainform.upd_class_src);

    var archive = document.mainform.archive.value;
    if (0) {
        var value = '<? echo($emp_class); ?>';
        for (var i = 0, len = classes.length; i < len; i++) {
            if (classes[i].id != value) continue;
            addOption(document.mainform.upd_class_src, value, classes[i].name);
        }
    } else {
        addOption(document.mainform.upd_class_src, '-', '----------', default_upd_class_src);
        for (var i = 0, len = classes.length; i < len; i++) {
            addOption(document.mainform.upd_class_src, classes[i].id, classes[i].name, default_upd_class_src);
        }
    }

    setUpdAtrbSrcOptions(default_upd_atrb_src);
}

function setUpdAtrbSrcOptions(default_upd_atrb_src) {
    deleteAllOptions(document.mainform.upd_atrb_src);

    addOption(document.mainform.upd_atrb_src, '-', '----------', default_upd_atrb_src);

    var class_id = document.mainform.upd_class_src.value;
    if (atrbs[class_id]) {
        for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
            addOption(document.mainform.upd_atrb_src, atrbs[class_id][i].id, atrbs[class_id][i].name, default_upd_atrb_src);
        }
    }

    setUpdDeptSrcOptions();
}

function setUpdDeptSrcOptions() {
    deleteAllOptions(document.mainform.upd_dept_src);

    var class_id = document.mainform.upd_class_src.value;
    var atrb_id = document.mainform.upd_atrb_src.value;
    if (depts[class_id]) {
        if (depts[class_id][atrb_id]) {
            for (var i = 0, len = depts[class_id][atrb_id].length; i < len; i++) {
                var dept_id = depts[class_id][atrb_id][i].id;
                var value = class_id + '-' + atrb_id + '-' + dept_id;
                addOption(document.mainform.upd_dept_src, value, depts[class_id][atrb_id][i].name);
            }
        } else if (atrb_id == '-') {
            for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
                var atrb_id = atrbs[class_id][i].id;
                if (depts[class_id][atrb_id]) {
                    for (var j = 0, len2 = depts[class_id][atrb_id].length; j < len2; j++) {
                        var dept_id = depts[class_id][atrb_id][j].id;
                        var value = class_id + '-' + atrb_id + '-' + dept_id;
                        addOption(document.mainform.upd_dept_src, value, depts[class_id][atrb_id][j].name);
                    }
                }
            }
        }
    }
}

function setDisplay() {
    var archive = document.mainform.archive.value;
    switch (archive) {
    case ARCHIVE_ALL:
    case ARCHIVE_PRIVATE:
        document.getElementById('limit_section').style.display = 'none';
        document.getElementById('limit_project').style.display = 'none';
        break;
    case ARCHIVE_SECTION:
        document.getElementById('limit_section').style.display = '';
        document.getElementById('limit_project').style.display = 'none';
        break;
    case ARCHIVE_PROJECT:
        document.getElementById('limit_section').style.display = 'none';
        document.getElementById('limit_project').style.display = '';
        break;
    }
}

function setDisabled() {
    var disabled;
    var archive = document.mainform.archive.value;

    //20130604 中嶌改修 個人カテゴリ以外チェックがつけられるように変更
    disabled = (archive == ARCHIVE_PRIVATE);
    document.mainform.show_login_flg.disabled = disabled;

    disabled = !(archive != ARCHIVE_PRIVATE && document.mainform.show_login_flg.checked);
    document.mainform.show_login_begin1.disabled = disabled;
    document.mainform.show_login_begin2.disabled = disabled;
    document.mainform.show_login_begin3.disabled = disabled;
    document.mainform.show_login_end1.disabled = disabled;
    document.mainform.show_login_end2.disabled = disabled;
    document.mainform.show_login_end3.disabled = disabled;

    disabled = ((archive == ARCHIVE_PROJECT && document.mainform.private_flg2.checked) || archive == ARCHIVE_PRIVATE);
    document.mainform.ref_dept_st_flg.disabled = disabled;

    disabled = (document.mainform.private_flg2.checked);
    document.mainform.emplist1_project.disabled = disabled;

    disabled = !(!document.mainform.ref_dept_st_flg.disabled && !document.mainform.ref_dept_st_flg.checked);
    document.mainform.ref_dept_flg[0].disabled = disabled;
    document.mainform.ref_dept_flg[1].disabled = disabled;

    disabled = !(!document.mainform.ref_dept_flg[1].disabled && document.mainform.ref_dept_flg[1].checked);
    document.mainform.ref_dept_all.disabled = disabled;
    document.mainform.ref_class_src.disabled = disabled;
    document.mainform.ref_atrb_src.disabled = disabled;
    document.mainform.ref_dept_src.disabled = disabled;
    document.mainform.ref_dept.disabled = disabled;
    document.mainform.add_ref_dept.disabled = disabled;
    document.mainform.delete_ref_dept.disabled = disabled;
    document.mainform.delete_all_ref_dept.disabled = disabled;
    document.mainform.select_all_ref_dept.disabled = disabled;

    disabled = (document.mainform.ref_dept_st_flg.disabled || document.mainform.ref_dept_st_flg.checked);
    document.mainform.ref_st_flg[0].disabled = disabled;
    document.mainform.ref_st_flg[1].disabled = disabled;

    disabled = !(!document.mainform.ref_st_flg[1].disabled && document.mainform.ref_st_flg[1].checked);
    document.mainform.elements['ref_st[]'].disabled = disabled;

    disabled = ((archive == ARCHIVE_SECTION && document.mainform.private_flg1.checked) || (archive == ARCHIVE_PROJECT && document.mainform.private_flg2.checked) || archive == ARCHIVE_PRIVATE);
    document.mainform.emplist1.disabled = disabled;
    document.mainform.emplist1_clear.disabled = disabled;
    if (disabled) {
        clearAllTargets('1');
    }

    disabled = (archive == ARCHIVE_PRIVATE);
    document.mainform.emplist2.disabled = disabled;
    document.mainform.emplist2_clear.disabled = disabled;
    if (disabled) {
        clearAllTargets('2');
    }

    disabled = (archive == ARCHIVE_PROJECT || archive == ARCHIVE_PRIVATE);
    document.mainform.upd_dept_st_flg.disabled = disabled;

    disabled = !(!document.mainform.upd_dept_st_flg.disabled && !document.mainform.upd_dept_st_flg.checked);
    document.mainform.upd_dept_flg[0].disabled = disabled;
    document.mainform.upd_dept_flg[1].disabled = disabled;

    disabled = !(!document.mainform.upd_dept_flg[1].disabled && document.mainform.upd_dept_flg[1].checked);
    document.mainform.upd_dept_all.disabled = disabled;
    document.mainform.upd_class_src.disabled = disabled;
    document.mainform.upd_atrb_src.disabled = disabled;
    document.mainform.upd_dept_src.disabled = disabled;
    document.mainform.upd_dept.disabled = disabled;
    document.mainform.add_upd_dept.disabled = disabled;
    document.mainform.delete_upd_dept.disabled = disabled;
    document.mainform.delete_all_upd_dept.disabled = disabled;
    document.mainform.select_all_upd_dept.disabled = disabled;

    disabled = (document.mainform.upd_dept_st_flg.disabled || document.mainform.upd_dept_st_flg.checked);
    document.mainform.upd_st_flg[0].disabled = disabled;
    document.mainform.upd_st_flg[1].disabled = disabled;

    disabled = !(!document.mainform.upd_st_flg[1].disabled && document.mainform.upd_st_flg[1].checked);
    document.mainform.elements['upd_st[]'].disabled = disabled;

}

function setFileInfo(path) {
    var fname;
    if (path.indexOf('\\') != -1) {
        fname = path.replace(/^.*\\/, '');
    } else if (path.indexOf('/') != -1) {
        fname = path.replace(/^.*\//, '');
    } else {
        fname = path;
    }

    var docname = fname.replace(/\.[^.]*$/, '');
    document.mainform.document_name.value = docname;

    var ext = fname.replace(/.*\./, '');
    switch (ext) {
    case 'doc':
    case 'docx':
        document.mainform.document_type.options[1].selected = true;
        break;
    case 'xls':
    case 'xlsx':
        document.mainform.document_type.options[2].selected = true;
        break;
    case 'ppt':
    case 'pptx':
        document.mainform.document_type.options[3].selected = true;
        break;
    case 'pdf':
        document.mainform.document_type.options[4].selected = true;
        break;
    case 'txt':
        document.mainform.document_type.options[5].selected = true;
        break;
    case 'jpg':
    case 'jpeg':
        document.mainform.document_type.options[6].selected = true;
        break;
    case 'gif':
        document.mainform.document_type.options[7].selected = true;
        break;
    default:
        document.mainform.document_type.options[8].selected = true;
        break;
    }
}

function setManageHistoryButtonDisabled() {
    var disabled = (document.mainform.upfile.value == '');
    document.mainform.manage_history[0].disabled = disabled;
    document.mainform.manage_history[1].disabled = disabled;
}

function submitForm() {
    var ref_dept_box = document.mainform.ref_dept;
    if (!ref_dept_box.disabled) {
        for (var i = 0, j = ref_dept_box.length; i < j; i++) {
            addHiddenElement(document.mainform, 'hid_ref_dept[]', ref_dept_box.options[i].value);
        }
    }

    var upd_dept_box = document.mainform.upd_dept;
    if (!upd_dept_box.disabled) {
        for (var i = 0, j = upd_dept_box.length; i < j; i++) {
            addHiddenElement(document.mainform, 'hid_upd_dept[]', upd_dept_box.options[i].value);
        }
    }

    document.mainform.ref_toggle_mode.value = document.getElementById('ref_toggle').innerHTML;
    document.mainform.upd_toggle_mode.value = document.getElementById('upd_toggle').innerHTML;

    closeEmployeeList();

    document.mainform.submit();
}

function addOption(box, value, text, selected) {
    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;
    if (selected == value) {
        opt.selected = true;
    }
    box.options[box.length] = opt;
    if (!box.multiple) {
        try {box.style.fontSize = 'auto';} catch (e) {}
        box.style.overflow = 'auto';
    }
}

function addSelectedOptions(dest_box, src_box) {
    var options = new Array();
    for (var i = 0, j = dest_box.length; i < j; i++) {
        options[dest_box.options[i].value] = dest_box.options[i].text;
    }
    deleteAllOptions(dest_box);
    for (var i = 0, j = src_box.length; i < j; i++) {
        if (src_box.options[i].selected) {
            var class_atrb_dept = src_box.options[i].value.split('-');
            var class_id = class_atrb_dept[0];
            var atrb_id = class_atrb_dept[1];
            var dept_id = class_atrb_dept[2];
            for (var k = 0; k < depts[class_id][atrb_id].length; k++) {
                if (depts[class_id][atrb_id][k].id == dept_id) {
                    options[src_box.options[i].value] = depts[class_id][atrb_id][k].hierarchy + depts[class_id][atrb_id][k].name;
                }
            }
        }
    }

    for (var i in options) {
        addOption(dest_box, i, options[i]);
    }
    dest_box.style.width = '';
}

function selectAllOptions(box) {
    for (var i = 0, j = box.length; i < j; i++) {
        box.options[i].selected = true;
    }
}

function deleteSelectedOptions(box) {
    for (var i = box.length - 1; i >= 0; i--) {
        if (box.options[i].selected) {
            box.options[i] = null;
        }
    }
    if (!(box.options[0])) {
        box.style.width = '100%';
    } else {
        box.style.width = '';
    }
}

function deleteAllOptions(box) {
    for (var i = box.length - 1; i >= 0; i--) {
        box.options[i] = null;
    }
    if (box.name == 'upd_dept' || box.name == 'ref_dept') {
        box.style.width = '100%';
    }
}

function addHiddenElement(frm, name, value) {
    var input = document.createElement('input');
    input.type = 'hidden';
    input.name = name;
    input.value = value;
    frm.appendChild(input);
}

function closeDeptWin() {
    if (dept_win) {
        if (!dept_win.closed) {
            dept_win.close();
        }
        dept_win = null;
    }
}

function showDeptAll(div) {
    var emp_class = '';
    if (document.mainform.archive.value == ARCHIVE_SECTION) {
        if (div == 'upd' || document.mainform.private_flg1.checked) {
            emp_class = '<? echo($emp_class); ?>';
        }
    }

    dept_win = window.open('select_dept_all.php?session=<? echo($session); ?>&module=library&emp_class='.concat(emp_class).concat('&div=').concat(div), 'deptall', 'width=800,height=500,scrollbars=yes');
}

function selectFolder() {
    var a = document.mainform.archive.value;
    var c = document.mainform.category.value;
    var f = document.mainform.folder_id.value;
    window.open('library_folder_select.php?session=<? echo($session); ?>&a=' + a + '&c=' + c + '&f=' + f + '&path=<? echo($path); ?>', 'newwin', 'width=640,height=700,scrollbars=yes');
}

function toggle(button) {
    var display;
    if (button.innerHTML == '▼') {
        button.innerHTML = '▲';
        display = '';
    } else {
        button.innerHTML = '▼';
        display = 'none';
    }

    for (var i = 1; i <= 3; i++) {
        document.getElementById(button.id.concat(i)).style.display = display;
    }
}

function backToList() {
<? if ($listtype == "1") { ?>
    location.href = 'library_list_all.php?session=<? echo($session); ?>&a=<? echo($archive); ?>&c=<? echo($category); ?>&f=<? echo($folder_id); ?>&o=<? echo($o); ?>';
<? } else if ($listtype == "2") { ?>
    location.href = 'library_admin_menu.php?session=<? echo($session); ?>&a=<? echo($archive); ?>&c=<? echo($category); ?>&f=<? echo($folder_id); ?>&o=<? echo($o); ?>';
<? } else if ($listtype == "3") { ?>
    location.href = '<? echo($back_url); ?>';
<? } ?>
}

function clearAllTargets(idx) {
    document.getElementById('target_disp_area'.concat(idx)).innerHTML = '';
    document.mainform.elements['target_id_list'.concat(idx)].value = '';
    document.mainform.elements['target_name_list'.concat(idx)].value = '';
}

function setProjectMembers() {
    var url = 'library_project_member_xml.php?session=<? echo($session); ?>&category='.concat(document.mainform.category.value);
    var callback = {
        success: function (o) {
            var ids = o.responseXML.getElementsByTagName('ID');
            var names = o.responseXML.getElementsByTagName('Name');
            for (var i = 0, j = ids.length; i < j; i++) {
                add_target_list('1', ids[i].firstChild.nodeValue, names[i].firstChild.nodeValue);
            }
        },
        failure: function () {alert('データの取得に失敗しました。');}
    };
    YAHOO.util.Connect.asyncRequest('GET', url, callback);

}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui/build/connection/connection-min.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse; border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="98%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($path == "3") { ?>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_info.php?session=<? echo($session); ?>"><b><? echo($intra_menu2); ?></b></a> &gt; <a href="library_list_all.php?session=<? echo($session); ?>"><b><? echo($intra_menu2_3); ?></b></a> &gt; <a href="library_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="library_list_all.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="library_list_all.php?session=<? echo($session); ?>"><img src="img/icon/b12.gif" width="32" height="32" border="0" alt="文書管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="library_list_all.php?session=<? echo($session); ?>"><b>文書管理</b></a> &gt; <a href="library_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="library_list_all.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_admin_menu.php?session=<? echo($session); ?>&a=<? echo($archive); ?>&c=<? echo($category); ?>&f=<? echo($folder_id); ?>&o=<? echo($o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#5279a5"><a href="library_detail.php?session=<? echo($session); ?>&a=<? echo($a); ?>&c=<? echo($c); ?>&f=<? echo($f); ?>&lib_id=<? echo($lib_id); ?>&path=3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>文書更新</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_admin_refer_log.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照履歴</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_config.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<? } else { ?>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_info.php?session=<? echo($session); ?>"><b><? echo($intra_menu2); ?></b></a> &gt; <a href="library_list_all.php?session=<? echo($session); ?>"><b><? echo($intra_menu2_3); ?></b></a> &gt; <a href="library_list_all.php?session=<? echo($session); ?>&a=<? echo($archive); ?>&c=<? echo($category); ?>&f=<? echo($folder_id); ?>&o=<? echo($o); ?>"><b>文書一覧</b></a> &gt; <a href="library_detail.php?session=<? echo($session); ?>&a=<? echo($a); ?>&c=<? echo($c); ?>&f=<? echo($f); ?>&lib_id=<? echo($lib_id); ?>"><b>文書更新</b></a></font></td>
<? if ($lib_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="library_admin_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="105" align="center" bgcolor="#bdd1e7"><a href="intra_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メインメニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($intra_menu2)); ?>" align="center" bgcolor="#bdd1e7"><a href="intra_info.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($intra_menu2); ?></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_list_all.php?session=<? echo($session); ?>&a=<? echo($archive); ?>&c=<? echo($category); ?>&f=<? echo($folder_id); ?>&o=<? echo($o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#5279a5"><a href="library_detail.php?session=<? echo($session); ?>&a=<? echo($a); ?>&c=<? echo($c); ?>&f=<? echo($f); ?>&lib_id=<? echo($lib_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>文書更新</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_refer_log.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照履歴</font></a></td>
<? if ($newlist_flg == 't') { ?>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_list_new.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>最新文書一覧</nobr></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<? } ?>
<td>&nbsp;</td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="library_list_all.php?session=<? echo($session); ?>"><img src="img/icon/b12.gif" width="32" height="32" border="0" alt="文書管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="library_list_all.php?session=<? echo($session); ?>"><b>文書管理</b></a> &gt; <a href="library_list_all.php?session=<? echo($session); ?>&a=<? echo($archive); ?>&c=<? echo($category); ?>&f=<? echo($folder_id); ?>&o=<? echo($o); ?>"><b>文書一覧</b></a> &gt; <a href="library_detail.php?session=<? echo($session); ?>&a=<? echo($a); ?>&c=<? echo($c); ?>&f=<? echo($f); ?>&lib_id=<? echo($lib_id); ?>"><b>文書更新</b></a></font></td>
<? if ($lib_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="library_admin_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_list_all.php?session=<? echo($session); ?>&a=<? echo($archive); ?>&c=<? echo($category); ?>&f=<? echo($folder_id); ?>&o=<? echo($o); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#5279a5"><a href="library_detail.php?session=<? echo($session); ?>&a=<? echo($a); ?>&c=<? echo($c); ?>&f=<? echo($f); ?>&lib_id=<? echo($lib_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>文書更新</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_refer_log.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照履歴</font></a></td>
<? if ($newlist_flg == 't') { ?>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_list_new.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>最新文書一覧</nobr></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<? } ?>
<td width="">&nbsp;</td>
</tr>
</table>
<? } ?>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="mainform" action="library_update_exe.php" method="post" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td style="vertical-align:bottom;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10" color="red">※ファイルを更新したい場合のみ、参照ボタンでファイルを選択してください。<? echo(ini_get("upload_max_filesize")); ?>を超えるファイルは登録できません。</font></td>
<td align="right">
<input type="button" value="更新" onclick="submitForm();"<? if (!$upd_flg) {echo(" disabled");} ?>>
<?
if (lib_get_filename_flg() > 0) {
    $refer_onclick = "location.href='$lib_url';";
} else {
    $refer_onclick = "window.open('$lib_url');";
}
?>
<input type="button" value="参照" onclick="<? echo($refer_onclick); ?>">
<? if ($listtype != "") { ?>
<input type="button" value="戻る" onclick="backToList();">
<? } ?>
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイル</font></td>
<td><input type="file" name="upfile" size="50" value="" style="ime-mode:inactive;" onchange="setFileInfo(this.value);setManageHistoryButtonDisabled();"></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">履歴管理</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="manage_history" value="t"<? if ($manage_history == "t") {echo(" checked");} ?>>する
<input type="radio" name="manage_history" value="f"<? if ($manage_history == "f") {echo(" checked");} ?>>しない
</font></td>
</tr>
<tr height="22">
<td width="15%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書名</font></td>
<td><input name="document_name" type="text" size="50" maxlength="100" value="<? echo($document_name); ?>" style="ime-mode:active;"></td>
<td width="14%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書タイプ</font></td>
<td width="17%">
<select name="document_type">
<option value="0"<? if ($document_type == "0") {echo(" selected");} ?>>
<option value="1"<? if ($document_type == "1") {echo(" selected");} ?>>Word
<option value="2"<? if ($document_type == "2") {echo(" selected");} ?>>Excel
<option value="3"<? if ($document_type == "3") {echo(" selected");} ?>>PowerPoint
<option value="4"<? if ($document_type == "4") {echo(" selected");} ?>>PDF
<option value="5"<? if ($document_type == "5") {echo(" selected");} ?>>テキスト
<option value="6"<? if ($document_type == "6") {echo(" selected");} ?>>JPEG
<option value="7"<? if ($document_type == "7") {echo(" selected");} ?>>GIF
<option value="99"<? if ($document_type == "99") {echo(" selected");} ?>>その他
</select>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">キーワード</font></td>
<td><input name="keywd" type="text" size="50" maxlength="50" value="<? echo($keywd); ?>" style="ime-mode:active;"></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書番号</font></td>
<td><input type="text" name="lib_no" value="<? echo($lib_no); ?>" style="ime-mode:inactive;"></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">説明</font></td>
<td colspan="3"><textarea name="explain" rows="5" cols="40" style="ime-mode:active;"><? echo($explain); ?></textarea></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">保存先</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<span id="folder_path" style="padding-right:2px;">
<?
echo("$archive_nm &gt; $cate_nm");
lib_write_folder_path($folder_path, true);
?>
</span>
<? if (!$IS_OWNERS) { ?>
<input type="button" value="選択" onclick="selectFolder();">
<? } ?>
<input type="hidden" name="archive" value="<? echo($archive); ?>">
<input type="hidden" name="category" value="<? echo($category); ?>">
<input type="hidden" name="folder_id" value="<? echo($folder_id); ?>">

<?if($IS_OWNERS) {?>
    <span style="visibility:hidden">
        <input type="checkbox" name="show_login_flg" value="t" disabled />
        <input type="checkbox" name="show_mypage_flg" value="f" disabled />
        <select name="show_login_begin1"></select>
        <select name="show_login_begin2"></select>
        <select name="show_login_begin3"></select>
        <select name="show_login_end1"></select>
        <select name="show_login_end2"></select>
        <select name="show_login_end3"></select>
    </span>
<? } ?>

</font></td>
</tr>




<?if(!$IS_OWNERS) {?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">


<span title="ログイン画面、およびログイン画面からの「文書一覧＋文書検索」ポップアップ画面に表示する際は、チェックしてください。（管理画面オプションにて「ログインページへ表示する」チェックが行われている場合もログイン画面へ公開されます）ログインページはログイン前のページです。誰でも参照可能となります。">
<input type="checkbox" name="show_login_flg" value="t"<? if ($show_login_flg == "t") {echo(" checked");} ?> onclick="setDisabled();">ログインページに表示する
    <font color="#FF0000">（ログインページで誰でも参照可能となります）</font>
</span>
<img src="img/spacer.gif" alt="" width="10" height="1">


<span title="ログイン画面への表示期間です。無期限表示する際は終了年月日を空欄指定してください。（ログイン画面からの「文書一覧＋文書検索」ポップアップ画面ではこの期間は考慮されません。）">
表示期間：
<select name="show_login_begin1"><? show_update_years($show_login_begin1, 1, true); ?></select>/<select name="show_login_begin2"><? show_select_months($show_login_begin2, true); ?></select>/<select name="show_login_begin3"><? show_select_days($show_login_begin3, true); ?></select> 〜 <select name="show_login_end1"><? show_update_years($show_login_end1, 1, true); ?></select>/<select name="show_login_end2"><? show_select_months($show_login_end2, true); ?></select>/<select name="show_login_end3"><? show_select_days($show_login_end3, true); ?></select></span><br />


<? if ($newlist_flg == 't') { ?>
<span title="マイページ「最新情報」＞「最近登録された文書」、および「最新文書一覧」画面へ表示されないようにするには、チェックしてください。">
<input type="checkbox" name="show_mypage_flg" value="f"<? echo ($show_mypage_flg == 'f') ? ' checked' : ''; ?>>「最近登録された文書」に表示させない
</span>
<? } ?>



</td>



</tr>
<? } ?>






<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録者</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($reg_emp_name); ?>（<? echo($reg_class_nm); ?>）</font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新日時</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($upd_time); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照数</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($lib_admin_auth == "1" && $ref_count > 0) {
    echo("<a href=\"javascript:void(0);\" onclick=\"window.open('library_admin_refer_log_detail.php?session=$session&lib_id=$lib_id', 'newwin', 'scrollbars=yes,width=640,height=700');\">$ref_count</a>");
} else {
    echo($ref_count);
}
?>
</font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">版番号</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($edition_no > 1) {
    echo("<a href=\"javascript:void(0);\" onclick=\"window.open('library_history.php?session=$session&lib_id=$lib_id&path=$path', 'libhist', 'width=640,height=480,scrollbars=yes');\">$edition_no</a>");
} else {
    echo($edition_no);
}
?>
</font></td>
</tr>

<?php
    if(!empty($apply_id)){

        // 申請情報取得
        require_once("application_workflow_common_class.php");
        $obj = new application_workflow_common_class($con, $fname);

        $arr_apply_wkfwmst      = $obj->get_apply_wkfwmst($apply_id);
        $short_wkfw_name        = $arr_apply_wkfwmst[0]["short_wkfw_name"];
        $apply_no               = $arr_apply_wkfwmst[0]["apply_no"];
        $apply_date             = $arr_apply_wkfwmst[0]["apply_date"];
        $apply_delete_flg       = $arr_apply_wkfwmst[0]["delete_flg"];


        if($apply_delete_flg == "f" ){

            // 申請番号作成
            $year = substr($apply_date, 0, 4);
            $md   = substr($apply_date, 4, 4);
            if($md >= "0101" and $md <= "0331")
            {
                $year = $year - 1;
            }

            $apply_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);


            // 申請ルート表示
            echo("<tr height='22'>\n");
            echo("<td align='right' bgcolor='#f6f9ff'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>申請番号</font></td>\n");
            echo("<td colspan='3'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>\n");
            echo("<a href=\"javascript:void(0)\" onclick=\"window.open('application_workflow_detail.php?session={$session}&apply_id=$apply_id', 'approvewin', 'directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=700,height=780');\">{$apply_no}</a></font>\n");
            echo("</font></td>\n");
            echo("</tr>\n\n");
        }

    }
?>

<tr height="22"<?=($IS_OWNERS ? ' style="visibility:hidden"' : '')?>>
<td bgcolor="#f6f9ff" colspan="4" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span style="cursor:pointer;" onclick="toggle(document.getElementById('ref_toggle'));">参照可能範囲の指定&nbsp;</span><span id="ref_toggle" style="cursor:pointer;" onclick="toggle(this);"><? echo($ref_toggle_mode); ?></span><span id="limit_section" style="display:none;padding-left:15px;"><input type="checkbox" name="private_flg1" value="t"<? if ($private_flg1 == "t") {echo(" checked");} ?> onclick="setRefClassSrcOptions(false);setDisabled();">所属<? echo($arr_class_name[0]); ?>内のみ参照可能とする</span><span id="limit_project" style="display:none;padding-left:15px;"><input type="checkbox" name="private_flg2" value="t"<? if ($private_flg2 == "t") {echo(" checked");} ?> onclick="setDisabled();">委員会メンバーのみ参照可能とする<input type="button" name="emplist1_project" value="職員に委員会メンバーを追加" onclick="setProjectMembers();" style="margin-left:10px;"></span></font></td>
</tr>
<tr height="22" id="ref_toggle1" style="display:<? echo($ref_toggle_display); ?>;">
<td bgcolor="#f6f9ff" rowspan="2" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署・役職<br><input type="checkbox" name="ref_dept_st_flg" value="t"<? if ($ref_dept_st_flg == "t") {echo(" checked");} ?> onclick="setDisabled();">許可しない</font></td>
<td bgcolor="#f6f9ff" class="spacing">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
<td align="right"><input type="button" name="ref_dept_all" value="全画面" onclick="showDeptAll('ref');"></td>
</tr>
</table>
</td>
<td bgcolor="#f6f9ff" colspan="2" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
</tr>
<tr id="ref_toggle2" style="display:<? echo($ref_toggle_display); ?>;">
<td>
<table cellspacing="0" cellpadding="0" border="0">
<tr>
<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="ref_dept_flg" value="1"<? if ($ref_dept_flg == "1") {echo(" checked");} ?> onclick="setDisabled();">すべて<br>
<input type="radio" name="ref_dept_flg" value="2"<? if ($ref_dept_flg == "2") {echo(" checked");} ?> onclick="setDisabled();">指定する
</font></td>
<td>
<table cellspacing="2" cellpadding="0" border="0">
<tr>
<td valign="bottom" style="position:relative;top:3px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">可能とする<? echo($arr_class_name[2]); ?></font></td>
<td></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="ref_class_src" onchange="setRefAtrbSrcOptions();">
</select><? echo($arr_class_name[0]); ?><br>
<select name="ref_atrb_src" onchange="setRefDeptSrcOptions();">
</select><? echo($arr_class_name[1]); ?>
</font></td>
</tr>
<tr>
<td>
<select name="ref_dept" size="6" multiple>
<?
foreach ($ref_dept as $tmp_dept_id) {
    echo("<option value=\"$tmp_dept_id\">{$dept_names[$tmp_dept_id]}");
}
?>
</select>
</td>
<td align="center"><input type="button" name="add_ref_dept" value=" &lt; " onclick="addSelectedOptions(this.form.ref_dept, this.form.ref_dept_src);"><br><br><input type="button" name="delete_ref_dept" value=" &gt; " onclick="deleteSelectedOptions(this.form.ref_dept);"></td>
<td><select name="ref_dept_src" size="6" multiple style="width:120px;"></select></td>
</tr>
<tr>
<td><input type="button" name="delete_all_ref_dept" value="全て消去" onclick="deleteAllOptions(this.form.ref_dept);"></td>
<td></td>
<td><input type="button" name="select_all_ref_dept" value="全て選択" onclick="selectAllOptions(this.form.ref_dept_src);"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<td colspan="2">
<table cellspacing="0" cellpadding="0" border="0">
<tr>
<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="ref_st_flg" value="1"<? if ($ref_st_flg == "1") {echo(" checked");} ?> onclick="setDisabled();">すべて<br>
<input type="radio" name="ref_st_flg" value="2"<? if ($ref_st_flg == "2") {echo(" checked");} ?> onclick="setDisabled();">指定する
</font></td>
<td>
<table cellspacing="2" cellpadding="0" border="0">
<tr>
<td>
<select name="ref_st[]" size="10" multiple>
<?
while ($row = pg_fetch_array($sel_st)) {
    $tmp_st_id = $row["st_id"];
    $tmp_st_nm = $row["st_nm"];
    echo("<option value=\"$tmp_st_id\"");
    if (in_array($tmp_st_id, $ref_st)) {
        echo(" selected");
    }
    echo(">$tmp_st_nm");
}
pg_result_seek($sel_st, 0);
?>
</select>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr height="22" id="ref_toggle3" style="display:<? echo($ref_toggle_display); ?>;">
<td bgcolor="#f6f9ff" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
<input type="button" name="emplist1" value="職員名簿" style="margin-left:2em;width:5.5em;" onclick="openEmployeeList('1');"><br>
<input type="button" name="emplist1_clear" value="クリア" style="margin-left:2em;width:5.5em;" onclick="clear_target('1','<?=$emp_id?>','<?=$emp_name?>');"><br></td>
<td colspan="3">

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
<tr>
<td width="350" height="60" style="border:#5279a5 solid 1px;">

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<span id="target_disp_area1"></span>
</font>

</td>
</tr>
</table>

</td>
</tr>
<tr height="22"<?=($IS_OWNERS ? ' style="visibility:hidden"' : '')?>>
<td bgcolor="#f6f9ff" colspan="4" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span style="cursor:pointer;" onclick="toggle(document.getElementById('upd_toggle'));">更新可能範囲の指定&nbsp;</span><span id="upd_toggle" style="cursor:pointer;" onclick="toggle(this);"><? echo($upd_toggle_mode); ?></span></font></td>
</tr>
<tr height="22" id="upd_toggle1" style="display:<? echo($upd_toggle_display); ?>;">
<td bgcolor="#f6f9ff" rowspan="2" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署・役職<br><input type="checkbox" name="upd_dept_st_flg" value="t"<? if ($upd_dept_st_flg == "t") {echo(" checked");} ?> onclick="setDisabled();">許可しない</font></td>
<td bgcolor="#f6f9ff" class="spacing">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
<td align="right"><input type="button" name="upd_dept_all" value="全画面" onclick="showDeptAll('upd');"></td>
</tr>
</table>
</td>
<td bgcolor="#f6f9ff" colspan="2" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
</tr>
<tr id="upd_toggle2" style="display:<? echo($upd_toggle_display); ?>;">
<td>
<table cellspacing="0" cellpadding="0" border="0">
<tr>
<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="upd_dept_flg" value="1"<? if ($upd_dept_flg == "1") {echo(" checked");} ?> onclick="setDisabled();">すべて<br>
<input type="radio" name="upd_dept_flg" value="2"<? if ($upd_dept_flg == "2") {echo(" checked");} ?> onclick="setDisabled();">指定する
</font></td>
<td>
<table cellspacing="2" cellpadding="0" border="0">
<tr>
<td valign="bottom" style="position:relative;top:3px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">可能とする<? echo($arr_class_name[2]); ?></font></td>
<td></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="upd_class_src" onchange="setUpdAtrbSrcOptions();">
</select><? echo($arr_class_name[0]); ?><br>
<select name="upd_atrb_src" onchange="setUpdDeptSrcOptions();">
</select><? echo($arr_class_name[1]); ?>
</font></td>
</tr>
<tr>
<td>
<select name="upd_dept" size="6" multiple>
<?
foreach ($upd_dept as $tmp_dept_id) {
    echo("<option value=\"$tmp_dept_id\">{$dept_names[$tmp_dept_id]}");
}
?>
</select>
</td>
<td align="center"><input type="button" name="add_upd_dept" value=" &lt; " onclick="addSelectedOptions(this.form.upd_dept, this.form.upd_dept_src);"><br><br><input type="button" name="delete_upd_dept" value=" &gt; " onclick="deleteSelectedOptions(this.form.upd_dept);"></td>
<td><select name="upd_dept_src" size="6" multiple style="width:120px;"></select></td>
</tr>
<tr>
<td><input type="button" name="delete_all_upd_dept" value="全て消去" onclick="deleteAllOptions(this.form.upd_dept);"></td>
<td></td>
<td><input type="button" name="select_all_upd_dept" value="全て選択" onclick="selectAllOptions(this.form.upd_dept_src);"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<td colspan="2">
<table cellspacing="0" cellpadding="0" border="0">
<tr>
<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="upd_st_flg" value="1"<? if ($upd_st_flg == "1") {echo(" checked");} ?> onclick="setDisabled();">すべて<br>
<input type="radio" name="upd_st_flg" value="2"<? if ($upd_st_flg == "2") {echo(" checked");} ?> onclick="setDisabled();">指定する
</font></td>
<td>
<table cellspacing="2" cellpadding="0" border="0">
<tr>
<td>
<select name="upd_st[]" size="10" multiple>
<?
while ($row = pg_fetch_array($sel_st)) {
    $tmp_st_id = $row["st_id"];
    $tmp_st_nm = $row["st_nm"];
    echo("<option value=\"$tmp_st_id\"");
    if (in_array($tmp_st_id, $upd_st)) {
        echo(" selected");
    }
    echo(">$tmp_st_nm");
}
pg_result_seek($sel_st, 0);
?>
</select>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr height="22" id="upd_toggle3" style="display:<? echo($upd_toggle_display); ?>;">
<td bgcolor="#f6f9ff" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
<input type="button" name="emplist2" value="職員名簿" style="margin-left:2em;width:5.5em;" onclick="openEmployeeList('2');"><br>
<input type="button" name="emplist2_clear" value="クリア" style="margin-left:2em;width:5.5em;" onclick="clear_target('2','<?=$emp_id?>','<?=$emp_name?>');"><br></td>
<td colspan="3">

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
<tr>
<td width="350" height="60" style="border:#5279a5 solid 1px;">

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<span id="target_disp_area2"></span>
</font>

</td>
</tr>
</table>

</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right">
<input type="button" value="更新" onclick="submitForm();"<? if (!$upd_flg) {echo(" disabled");} ?>>
<input type="button" value="参照" onclick="<? echo($refer_onclick); ?>">
<? if ($listtype != "") { ?>
<input type="button" value="戻る" onclick="backToList();">
<? } ?>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="lib_id" value="<? echo($lib_id); ?>">
<input type="hidden" name="a" value="<? echo($a); ?>">
<input type="hidden" name="c" value="<? echo($c); ?>">
<input type="hidden" name="f" value="<? echo($f); ?>">
<input type="hidden" name="o" value="<? echo($o); ?>">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="ref_toggle_mode" value="">
<input type="hidden" name="upd_toggle_mode" value="">
<input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
<input type="hidden" id="target_name_list1" name="target_name_list1" value="">
<input type="hidden" id="target_id_list2"   name="target_id_list2" value="">
<input type="hidden" id="target_name_list2" name="target_name_list2" value="">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
