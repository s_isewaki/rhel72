<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>文書管理 | オプション</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("referer_common.ini");
require_once("library_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 63, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// 使用可能な書庫一覧を取得
$available_archives = lib_available_archives($con, $fname, false);

// フォルダ固定フラグを取得
$stick_folder = lib_get_stick_folder($con, $fname);

// 鍵つきフォルダ表示フラグを取得
$lock_show_flg = lib_get_lock_show_flg($con, $fname);

// 鍵つきフォルダ表示フラグを取得
$lock_show_all_flg = lib_get_lock_show_all_flg($con, $fname);

// フォルダ内文書カウントフラグを取得
$count_flg = lib_get_count_flg($con, $fname);

// ドラッグ＆ドロップ使用フラグを取得
$dragdrop_flg = lib_get_dragdrop_flg($con, $fname);

// 文書名フラグを取得
$real_name_flg = lib_get_real_name_flg($con, $fname);

// ログイン画面へ表示するの取得
$show_login_flg = lib_get_show_login_flg($con, $fname);

// 遷移元の取得
$referer = get_referer($con, $session, "library", $fname);

// ツリー表示フラグを取得
$tree_flg = lib_get_show_tree_flg($con, $fname);

// 委員会デフォルトチェックフラグを取得
$pjt_private_flg = lib_get_pjt_private_flg($con, $fname);

// 最新文書一覧表示フラグを取得
$newlist_flg = lib_get_show_newlist_flg($con, $fname);

// 最新文書一覧表示フラグを取得
$newlist_chk_flg = lib_get_newlist_chk_flg($con, $fname);

// 全体共有の最上位でフォルダの作成を許可するフラグを取得
$all_createfolder_flg = lib_get_all_createfolder_flg($con, $fname);

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$intra_menu2 = pg_fetch_result($sel, 0, "menu2");
$intra_menu2_3 = pg_fetch_result($sel, 0, "menu2_3");
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
span.archive {margin-right:6px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_info.php?session=<? echo($session); ?>"><b><? echo($intra_menu2); ?></b></a> &gt; <a href="library_list_all.php?session=<? echo($session); ?>"><b><? echo($intra_menu2_3); ?></b></a> &gt; <a href="library_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="library_list_all.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="library_list_all.php?session=<? echo($session); ?>"><img src="img/icon/b12.gif" width="32" height="32" border="0" alt="文書管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="library_list_all.php?session=<? echo($session); ?>"><b>文書管理</b></a> &gt; <a href="library_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="library_list_all.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_admin_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_admin_refer_log.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照履歴</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#5279a5"><a href="library_config.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>オプション</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form action="library_config_update_exe.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">



<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">使用する書庫</font></td>
<td title="文書一覧画面、文書検索画面に対する設定です。参照履歴画面、最新文書画面に対して制御することはできません。なお、全体共有を外すことはできません。"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<span class="archive"><label><input type="checkbox" checked disabled>全体共有</label></span>
<span class="archive"><label><input type="checkbox" name="section_use" value="t"<? if (in_array("section", $available_archives)) {echo(" checked");} ?>>部署共有</label></span>
<span class="archive"><label><input type="checkbox" name="project_use" value="t"<? if (in_array("project", $available_archives)) {echo(" checked");} ?>>委員会・WG共有</label></span>
<span class="archive"><label><input type="checkbox" name="private_use" value="t"<? if (in_array("private", $available_archives)) {echo(" checked");} ?>>個人ファイル</label></span>
</font></td>
</tr>



<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログインページへ表示する</font></td>
<td title="すべての文書を無期限に公開します。ログイン画面、およびログイン画面からの「文書一覧＋文書検索」ポップアップ画面から閲覧できるようになります。"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<span class="archive"><label><input type="checkbox" name="show_login_flg" value="t"<? if ($show_login_flg == "t") {echo(" checked");} ?>>表示する</label></span><br>
<span style="color:red;padding-left:5px;">※チェックすると、各文書のオプションにかかわらず全ての文書をログインページに表示します</span>
</font></td>
</tr>



<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書一覧でフォルダを上に表示する</font></td>
<td title="文書一覧画面に対する指定です。ツリー表示を行っている場合、「文書一覧」とは右側エリアを指します。「表示する」場合はフォルダを上に固めて表示します。「表示しない」場合の表示順は、各列の表示順指定機能に準拠します。"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="stick_folder" value="t"<? if ($stick_folder == "t") {echo(" checked");} ?>>表示する</label>
<label><input type="radio" name="stick_folder" value="f"<? if ($stick_folder == "f") {echo(" checked");} ?>>表示しない</label>
</font></td>
</tr>



<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">鍵つきフォルダ表示</font></td>
<td title="鍵は「参照のみ」であることを示します。
鍵つき「文書」については関与しません。鍵つき「文書」を一括で表示切替することはできません。"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="lock_show_flg" value="t"<? if ($lock_show_all_flg!="f" && $lock_show_flg == "t") {echo(" checked");} ?>>表示する</label>
<label><input type="radio" name="lock_show_flg" value="f"<? if ($lock_show_all_flg!="f" && $lock_show_flg == "f") {echo(" checked");} ?>>部署・委員会・WGのトップフォルダに対し表示しない</label>
<label><input type="radio" name="lock_show_flg" value="all"<? if ($lock_show_all_flg == "f") {echo(" checked");} ?>>全般的に表示しない</label><br>
</font></td>
</tr>



<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ内文書数表示</font></td>
<td title="フォルダ内文書数は、「フォルダ更新」画面に表示されます。文書数に非表示の文書はカウントされません。"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="count_flg" value="t"<? if ($count_flg == "t") {echo(" checked");} ?>>表示する</label>
<label><input type="radio" name="count_flg" value="f"<? if ($count_flg == "f") {echo(" checked");} ?>>表示しない</label>
</font></td>
</tr>



<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書/フォルダのドラッグ＆ドロップ</font></td>
<td title="ドラッグ＆ドロップは、文書一覧画面で行うことができます。文書管理の管理者のみ操作が可能です。ユーザ画面/管理画面問わず行えます。"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="dragdrop_flg" value="t"<? if ($dragdrop_flg == "t") {echo(" checked");} ?>>有効にする</label>
<label><input type="radio" name="dragdrop_flg" value="f"<? if ($dragdrop_flg == "f") {echo(" checked");} ?>>無効にする</label><br>
<span style="color:red;padding-left:5px;">※有効にすると、フォルダ数が多くなった場合にレスポンスが悪化する場合があります</span>
</font></td>
</tr>



<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ダウンロード時の文書名</font></td>
<td title="文書管理機能内、および委員会・WGでの管理文書をダウンロードしようとした際の設定です。"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="real_name_flg" value="t"<? if ($real_name_flg == "t") {echo(" checked");} ?>>登録文書名</label>
<label><input type="radio" name="real_name_flg" value="f"<? if ($real_name_flg == "f") {echo(" checked");} ?>>システム名</label><br>
<span style="color:red;padding-left:5px;">※登録文書名にすると、古いAcrobat Readerなどでダウンロードに失敗することがあります</span>
</font></td>
</tr>



<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダツリーの表示</font></td>
<td title="フォルダツリーは文書一覧画面にあり、ログインページ＞ポップアップ画面、マイページ画面、文書管理機能＞ユーザ画面、文書管理機能＞管理画面の４箇所にあります。"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="show_tree_flg" value="t"<? if ($tree_flg == "t") {echo(" checked");} ?>>表示する</label>
<label><input type="radio" name="show_tree_flg" value="f"<? if ($tree_flg == "f") {echo(" checked");} ?>>表示しない</label><br>
<span style="color:red;padding-left:5px;">※「表示しない」にすると、文書一覧左側にあるフォルダツリー選択画面が表示されなくなります</span>
</font></td>
</tr>



<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照可能範囲の初期設定</font></td>
<td title="委員会・WG機能で委員会やワークグループを作成すると同時に、文書フォルダがひとつ自動的に作成されます。この最初のフォルダに対する権限を指定することができます。この設定は文書管理機能での委員会・WGフォルダの作成には関与しません。"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<span class="archive"><label><input type="checkbox" name="pjt_private_flg" value="t"<? if ($pjt_private_flg == "t") {echo(" checked");} ?>>委員会・WG登録時にフォルダをメンバーのみ参照可能とする</label></span>
<br>
</font></td>
</tr>



<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最新文書一覧を表示する</font></td>
<td title="文書の新規登録、あるいは文書の更新画面での画面保存操作で最新順が決まります。アップロードした文書自体の日付ではありません。
 「最近登録された文書」に表示させないチェックボックスは、新規文書を登録する際の設定です。文書登録画面にあります。"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <label><input type="radio" id="show_newlist_flg_f" name="show_newlist_flg" value="f"<?=$newlist_flg=="f"?" checked":""?>
        onclick="document.getElementById('span_newlist_chk').style.color=(this.checked?'#aaaaaa':'#000000');">表示しない</label>
    <label><input type="radio" id="show_newlist_flg_t" name="show_newlist_flg" value="t"<?=$newlist_flg=="t"?" checked":""?>
        onclick="document.getElementById('span_newlist_chk').style.color=(!this.checked?'#aaaaaa':'#000000');">表示する</label>
    <span id="span_newlist_chk" style="color:<?=$newlist_flg=="f"?"#aaaaaa":"#000000"?>">
    （
        「最近登録された文書」に表示させないチェックボックス初期状態
        <label><input type="radio" id="newlist_chk_flg_t" name="newlist_chk_flg" value="t"<?=$newlist_chk_flg=="t"?" checked":""?>
            onclick="document.getElementById('span_newlist_chk').style.color='#000000';
            if (this.checked) document.getElementById('show_newlist_flg_t').checked=true;">オン</label>
        <label><input type="radio" id="newlist_chk_flg_f" name="newlist_chk_flg" value="f"<?=$newlist_chk_flg=="f"?" checked":""?>
            onclick="document.getElementById('span_newlist_chk').style.color='#000000';
            if (this.checked) document.getElementById('show_newlist_flg_t').checked=true;">オフ</label>
    ）
    </span>
    <br>
    <span style="color:red;padding-left:5px;">※「表示しない」にすると、「最新文書一覧」画面、およびマイページ＞「最新情報」＞「最近登録された文書」が表示されなくなります</span>
</font>
</td>
</tr>



<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>全体共有の最上位でフォルダの作成を許可する</nobr></font></td>
<td title=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="all_createfolder_flg" value="t"<? if ($all_createfolder_flg == "t") {echo(" checked");} ?>>許可する</label>
<label><input type="radio" name="all_createfolder_flg" value="f"<? if ($all_createfolder_flg == "f") {echo(" checked");} ?>>許可しない</label><br>
<span style="color:red;padding-left:5px;">※「許可しない」にすると、ユーザ画面で全体共有の最上位にフォルダを作成できなくなります</span>
</font></td>
</tr>



</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
