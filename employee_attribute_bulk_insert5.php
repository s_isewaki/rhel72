<?php
ini_set("max_execution_time", 0);

require("about_session.php");
require("about_authority.php");
require("show_class_name.ini");
require("label_by_profile_type.ini");
require("get_values.ini");
require("employee_auth_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// ファイルが正常にアップロードされたかどうかチェック
$uploaded = false;
if (array_key_exists("csvfile", $_FILES)) {
    if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
        $uploaded = true;
    }
}
if (!$uploaded) {
    echo("<script type=\"text/javascript\">alert('ファイルのアップロードに失敗しました。');</script>\n");
    echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f';</script>");
    exit;
}

// EUC-JPで扱えない文字列は「・」に変換するよう設定
mb_substitute_character(0x30FB);

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 組織情報を取得
$arr_class_name = get_class_name_array($con, $fname);

$sql = "select class_id, class_nm from classmst";
$cond = "where class_del_flg = 'f' order by class_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$class_tree = array();
while ($row = pg_fetch_array($sel)) {
    $class_tree[$row["class_id"]] = array(
        "name" => $row["class_nm"], "attributes" => array()
    );
}

$sql = "select class_id, atrb_id, atrb_nm from atrbmst";
$cond = "where atrb_del_flg = 'f' order by atrb_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {
    $class_tree[$row["class_id"]]["attributes"][$row["atrb_id"]] = array(
        "name" => $row["atrb_nm"], "depts" => array()
    );
}

$sql = "select atrbmst.class_id, deptmst.atrb_id, deptmst.dept_id, deptmst.dept_nm from deptmst inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id";
$cond = "where deptmst.dept_del_flg = 'f' order by deptmst.dept_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {
    $class_tree[$row["class_id"]]["attributes"][$row["atrb_id"]]["depts"][$row["dept_id"]] = array(
        "name" => $row["dept_nm"], "rooms" => array()
    );
}

if ($arr_class_name[4] == 4) {
    $sql = "select atrbmst.class_id, deptmst.atrb_id, classroom.dept_id, classroom.room_id, classroom.room_nm from classroom inner join deptmst on classroom.dept_id = deptmst.dept_id inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id";
    $cond = "where classroom.room_del_flg = 'f' order by classroom.room_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $rooms = array();
    while ($row = pg_fetch_array($sel)) {
        $class_tree[$row["class_id"]]["attributes"][$row["atrb_id"]]["depts"][$row["dept_id"]]["rooms"][$row["room_id"]] = array("name" => $row["room_nm"]);
    }
}

// 役職情報を配列に格納
$sql = "select st_id, st_nm from stmst";
$cond = "where st_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$statuses = array();
while ($row = pg_fetch_array($sel)) {
	$statuses[$row["st_id"]] = $row["st_nm"];
}

// 職種情報を配列に格納
$sql = "select job_id, job_nm from jobmst";
$cond = "where job_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$jobs = array();
while ($row = pg_fetch_array($sel)) {
    $jobs[$row["job_id"]] = $row["job_nm"];
}

// カラム数
$column_count = ($arr_class_name[4] == 4) ? 11 : 10;

// CSVデータを配列に格納
$employees = array();
$employee_no = 1;
$lines = file($_FILES["csvfile"]["tmp_name"]);
foreach ($lines as $line) {

    // 文字コードを変換
    $line = trim(mb_convert_encoding($line, "cp51932", "cp932"));
    $line = mb_convert_kana($line, 'KV', 'EUC-JP');

    // EOFを削除
    $line = str_replace(chr(0x1A), "", $line);

    // 空行は無視
    if ($line == "") {
        continue;
    }

    // カラム数チェック
    $employee_data = split(",", $line);
    if (count($employee_data) < $column_count) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('カラム数が不正です。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    $employees[$employee_no] = $employee_data;
    $employee_no++;
}

// 職員データを登録
$extra_ids = array();
foreach ($employees as $employee_no => $employee_data) {
    $extra_id = insert_employee_data($con, $employee_no, $employee_data, $arr_class_name[4], $class_tree, $arr_class_name, $statuses, $jobs, $session, $fname);
    if ($extra_id != "") {
        $extra_ids[] = $extra_id;
    }
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 存在しない職員IDが指定されていたらalert
if (count($extra_ids) > 0) {
    $tmp_extra_ids = "・" . implode("\\n・", $extra_ids);
    echo("<script type=\"text/javascript\">alert('下記の職員は存在しないため、無視されました。\\n\\n$tmp_extra_ids');</script>\n");
}

// 成功画面を表示
echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=t&encoding=$encoding';</script>");

// 職員データを登録
function insert_employee_data($con, $employee_no, $employee_data, $class_cnt, $class_tree, $arr_class_name, $statuses, $jobs, $session, $fname) {
    global $_label_by_profile;
    global $profile_type;

    $i = 0;
    $personal_id = trim($employee_data[$i++]);
    $emp_lt_nm = $employee_data[$i++];
    $emp_ft_nm = $employee_data[$i++];
    $emp_lt_kana_nm = $employee_data[$i++];
    $emp_ft_kana_nm = $employee_data[$i++];
    $class = trim($employee_data[$i++]);
    $atrb = trim($employee_data[$i++]);
    $dept = trim($employee_data[$i++]);
    if ($arr_class_name[4] == "4") {
        $room = trim($employee_data[$i++]);
    } else {
        $room = "";
    }
    $status = trim($employee_data[$i++]);
    $job = trim($employee_data[$i++]);
    $del_flg = trim($employee_data[$i++]);
    $auth_group = trim($employee_data[$i++]);               // 権限グループ
    $disp_group = trim($employee_data[$i++]);               // 表示グループ
    $menu_group = trim($employee_data[$i++]);               // メニューグループ
    $emp_name_deadlink_flg = trim($employee_data[$i++]);    // 氏名連携不可フラグ
    $emp_class_deadlink_flg = trim($employee_data[$i++]);   // 部署連携不可フラグ
    $emp_st_deadlink_flg = trim($employee_data[$i++]);      // 役職連携不可フラグ
    $emp_job_deadlink_flg = trim($employee_data[$i++]);     // 職種連携不可フラグ

    // 当該職員が存在するかチェック
    $sql = "select emp_id from empmst";
    $cond = "where emp_personal_id = '$personal_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    if (pg_num_rows($sel) === 0) {
        // 存在しない場合はemp_personal_idを返す
        return $personal_id;
    } else {
        // 存在する場合はemp_idを取得する。
        $emp_id = pg_fetch_result($sel, 0, "emp_id");
    }

    // 現在の連携不可フラグを取得
    $sql_rel = sprintf("SELECT emp_name_deadlink_flg, emp_class_deadlink_flg, emp_job_deadlink_flg, emp_st_deadlink_flg FROM emp_relation WHERE emp_id='%s'", pg_escape_string($emp_id));
    $sel_rel = select_from_table($con, $sql_rel, "", $fname);
    if ($sel_rel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    if (pg_num_rows($sel_rel) > 0) {
        if (empty($emp_name_deadlink_flg)) {
            $emp_name_deadlink_flg  = pg_fetch_result($sel_rel, 0, "emp_name_deadlink_flg");
        }
        if (empty($emp_class_deadlink_flg)) {
            $emp_class_deadlink_flg = pg_fetch_result($sel_rel, 0, "emp_class_deadlink_flg");
        }
        if (empty($emp_st_deadlink_flg)) {
            $emp_st_deadlink_flg    = pg_fetch_result($sel_rel, 0, "emp_st_deadlink_flg");
        }
        if (empty($emp_job_deadlink_flg)) {
            $emp_job_deadlink_flg   = pg_fetch_result($sel_rel, 0, "emp_job_deadlink_flg");
        }
    }

    // 部門IDを取得
    $class_id = "";
    foreach ($class_tree as $tmp_class_id => $tmp_class) {
        if ($tmp_class["name"] == $class) {
            $class_id = $tmp_class_id ;
        }
    }
    if ($class_id == "") {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[0]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    // 課IDを取得
    $atrb_id = "";
    foreach ($class_tree[$class_id]["attributes"] as $tmp_atrb_id => $tmp_atrb) {
        if ($tmp_atrb["name"] == $atrb) {
            $atrb_id = $tmp_atrb_id ;
        }
    }
    if ($atrb_id == "") {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[1]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    // 科IDを取得
    $dept_id = "";
    foreach ($class_tree[$class_id]["attributes"][$atrb_id]["depts"] as $tmp_dept_id => $tmp_dept) {
        if ($tmp_dept["name"] == $dept) {
            $dept_id = $tmp_dept_id ;
        }
    }
    if ($dept_id == "") {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[2]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    // 室IDを取得
    if ($room != "") {
        foreach ($class_tree[$class_id]["attributes"][$atrb_id]["depts"][$dept_id]["rooms"] as $tmp_room_id => $tmp_room) {
            if ($tmp_room["name"] == $room) {
                $room_id = $tmp_room_id ;
            }
        }
        if ($room_id == "") {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[3]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
    }  else {
        $room_id = null;
    }

    // 役職IDを取得
    if ($status != "") {
        $st_id = array_search($status, $statuses);
        if (!$st_id) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('指定された役職({$status})は登録されていません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
    } else {
        $st_id = null;
    }

    // 職種IDを取得
    if ($job != "") {
        $job_id = array_search($job, $jobs);
        if (!$job_id) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('指定された職種({$job})は登録されていません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
    } else {
        $job_id = null;
    }

    // 入力チェック
    if (strlen($emp_lt_nm) > 20) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('苗字（漢字）が長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    if (strlen($emp_ft_nm) > 20) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('名前（漢字）が長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    if (strlen($emp_lt_kana_nm) > 20) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('苗字（かな）が長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    if (strlen($emp_ft_kana_nm) > 20) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('名前（かな）が長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    if ($del_flg != "" && $del_flg != "f" && $del_flg != "t") {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('利用停止フラグが不正です。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    // 権限グループが存在するかチェック
    if ($auth_group != "") {
        $sql = "select group_id from authgroup";
        $cond = "where group_nm = '$auth_group'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        } else if (pg_num_rows($sel) == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('指定された権限グループは登録されていません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        } else {
            $auth_group_id = pg_fetch_result($sel, 0, "group_id");
        }
    }

    // 表示グループが存在するかチェック
    if ($disp_group != "") {
        $sql = "select group_id from dispgroup";
        $cond = "where group_nm = '$disp_group'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        } else if (pg_num_rows($sel) == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('指定された表示グループは登録されていません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        } else {
            $disp_group_id = pg_fetch_result($sel, 0, "group_id");
        }
    }

    // メニューグループが存在するかチェック
    if ($menu_group != "") {
        $sql = "select group_id from menugroup";
        $cond = "where group_nm = '$menu_group'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        } else if (pg_num_rows($sel) == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('指定されたメニューグループは登録されていません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        } else {
            $menu_group_id = pg_fetch_result($sel, 0, "group_id");
        }
    }

    if (version_compare(PHP_VERSION, '5.0.0') >= 0 && substr(pg_parameter_status($con, 'server_version'), 0, 1) >= 8) {
        if ($emp_name_deadlink_flg != "f" && $emp_name_deadlink_flg != "t") {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('氏名連携不可フラグが不正です。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }

        if ($emp_class_deadlink_flg != "f" && $emp_class_deadlink_flg != "t") {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('部署連携不可フラグが不正です。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }

        if ($emp_st_deadlink_flg != "f" && $emp_st_deadlink_flg != "t") {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('役職連携不可フラグが不正です。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }

        if ($emp_job_deadlink_flg != "f" && $emp_job_deadlink_flg != "t") {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('職種連携不可フラグが不正です。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register5.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
    }
    $sql = "update empmst set";
    $set = array("emp_lt_nm", "emp_ft_nm", "emp_kn_lt_nm", "emp_kn_ft_nm", "emp_keywd", "emp_class", "emp_attribute", "emp_dept", "emp_room");
    $setvalue = array($emp_lt_nm, $emp_ft_nm, $emp_lt_kana_nm, $emp_ft_kana_nm, get_keyword($emp_lt_kana_nm), $class_id, $atrb_id, $dept_id, $room_id);
    if (!is_null($st_id)) {
        $set[] = "emp_st";
        $setvalue[] = $st_id;
    }
    if (!is_null($job_id)) {
        $set[] = "emp_job";
        $setvalue[] = $job_id;
    }

    $cond = "where emp_personal_id = '$personal_id'";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    if ($del_flg != "") {
        $sql = "update authmst set";
        $set = array("emp_del_flg");
        $setvalue = array($del_flg);
        $cond = "where emp_id = '$emp_id'";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }

    // 権限グループ/表示グループ/メニュグループ/を登録する処理を追加
    // 権限グループが指定されないとgroup_id=nullになるので、指定しない場合は現状の権限グループを維持するため、この時点で判定をする。
    if ($auth_group_id != "") {
        auth_change_group_of_employee($con, $emp_id, $auth_group_id, $fname);
    }
    dispgroup_change($con, $emp_id, $disp_group_id, $fname);
    menugroup_change($con, $emp_id, $menu_group_id, fname);

    if (version_compare(PHP_VERSION, '5.0.0') >= 0 && substr(pg_parameter_status($con, 'server_version'), 0, 1) >= 8) {
        if (!empty($emp_name_deadlink_flg) || !empty($emp_class_deadlink_flg) || !empty($emp_job_deadlink_flg) || !empty($emp_st_deadlink_flg)) {
            $emp_name_deadlink_flg  = $emp_name_deadlink_flg  === 't' ? 't' : 'f';
            $emp_class_deadlink_flg = $emp_class_deadlink_flg === 't' ? 't' : 'f';
            $emp_job_deadlink_flg   = $emp_job_deadlink_flg   === 't' ? 't' : 'f';
            $emp_st_deadlink_flg    = $emp_st_deadlink_flg    === 't' ? 't' : 'f';
            $sql_rel = "SELECT update_emp_relation(";
            $content_rel = array($emp_id, $emp_name_deadlink_flg, $emp_class_deadlink_flg, $emp_job_deadlink_flg, $emp_st_deadlink_flg);
            $in_rel = insert_into_table($con, $sql_rel, $content_rel, $fname);
            if ($in_rel == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }
}

function menugroup_change($con, $emp_id, $menu_group_id, $fname)
{
    // メニューグループを設定している場合、メニューを設定
    if ($menu_group_id != "") {
        // メニューグループの設定情報を取得
        $sql = "select * from menugroup";
        $cond = "where group_id = $menu_group_id";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
            exit;
        }

        $menu_groups = pg_fetch_assoc($sel);

        // サイドメニュー更新
        $sql = "update option set";
        $set = array("sidemenu_show_flg", "sidemenu_position");
        $setvalue = array($menu_groups["sidemenu_show_flg"], $menu_groups["sidemenu_position"]);
        $cond = "where emp_id = '$emp_id'";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        //メニュー設定の初期化
        $sql = " delete  from headermenu where emp_id = '$emp_id'";
        $cond = "";
        $sel = select_from_table($con, $sql, $cond, $fname);

        // メニュー設定
        for ($i = 1; $i <= 12; $i++) {
            $menu_num = 'menu_id_' . $i;
            if ($menu_groups[$menu_num] == "") break;
            $sql = "insert into headermenu(emp_id, menu_id, menu_order) values(";
            $content = array($emp_id, $menu_groups[$menu_num], $i);
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }

        // menu_group_idの登録
        $sql = "update option set";
        $set = array("menu_group_id");
        $setvalue = array($menu_group_id);
        $cond = "where emp_id = '$emp_id'";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

function dispgroup_change($con, $emp_id, $disp_group_id, $fname)
{
    if ($disp_group_id != "") {
        // 表示グループの設定情報を取得
        $sql = "select * from dispgroup";
        $cond = "where group_id = $disp_group_id";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
            exit;
        }

        $disp_groups = pg_fetch_assoc($sel);


        // 表示設定更新
        $sql = "update option set";
        $set = array("default_page", "font_size", "top_mail_flg", "top_ext_flg", "top_event_flg", "top_schd_flg", "top_info_flg", "top_task_flg", "top_msg_flg", "top_aprv_flg", "top_schdsrch_flg", "top_lib_flg", "top_bbs_flg", "top_memo_flg", "top_link_flg", "top_intra_flg", "bed_info", "schd_type", "top_wic_flg", "top_fcl_flg", "top_inci_flg", "top_cas_flg", "top_fplus_flg", "top_jnl_flg", "top_free_text_flg", "top_manabu_flg", "top_workflow_flg", "disp_group_id");
        $setvalue = array($disp_groups["default_page"], $disp_groups["font_size"], $disp_groups["top_mail_flg"], $disp_groups["top_ext_flg"], $disp_groups["top_event_flg"], $disp_groups["top_schd_flg"], $disp_groups["top_info_flg"], $disp_groups["top_task_flg"], $disp_groups["top_msg_flg"], $disp_groups["top_aprv_flg"], $disp_groups["top_schdsrch_flg"], $disp_groups["top_lib_flg"], $disp_groups["top_bbs_flg"], $disp_groups["top_memo_flg"], $disp_groups["top_link_flg"], $disp_groups["top_intra_flg"], $disp_groups["bed_info"], $disp_groups["schd_type"], $disp_groups["top_wic_flg"], $disp_groups["top_fcl_flg"], $disp_groups["top_inci_flg"], $disp_groups["top_cas_flg"], $disp_groups["top_fplus_flg"], $disp_groups["top_jnl_flg"], $disp_groups["top_free_text_flg"], $disp_groups["top_manabu_flg"], $disp_groups["top_workflow_flg"], $disp_group_id);
        $cond = "where emp_id = '$emp_id'";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}
