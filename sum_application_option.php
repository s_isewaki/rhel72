<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー｜オプション</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("referer_common.ini");
require_once("sum_application_imprint_common.ini");
require_once("sot_util.php");
require_once("summary_common.ini");
require_once("get_values.php");
require_once("get_menu_label.ini");


//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $_SERVER["PHP_SELF"];
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}

//ログイン職員ＩＤ
$emp_id = get_emp_id($con, $session, $fname);

// ログ
summary_common_write_operate_log("access", $con, $fname, $session, $emp_id, "", "");


?>

<?
$update_message =  "";

//====================================================================
//印影機能
//====================================================================

// 印影機能フラグ取得
$db_imprint_flg = get_imprint_flg($con, $emp_id, $fname);

$delbtn_flg = "f";

// 画像登録
if (@$regist_flg == "1") {
  // ファイルアップロードチェック
  define(UPLOAD_ERR_OK, 0);
  define(UPLOAD_ERR_INI_SIZE, 1);
  define(UPLOAD_ERR_FORM_SIZE, 2);
  define(UPLOAD_ERR_PARTIAL, 3);
  define(UPLOAD_ERR_NO_FILE, 4);

  $filename_accepted = $_FILES["imprint_accepted"]["name"];
  $filename_operated = $_FILES["imprint_operated"]["name"];

  if ($filename_accepted != "") {
    $delbtn_flg = "t";
    // gif, jpgチェック
    $pos = strrpos($filename_accepted, ".");
    $ext = "";
    if ($pos > 0 ) {
      $ext = substr($filename_accepted, $pos+1, 3);
      $ext = strtolower($ext);
    }

    if ($pos === false || ($ext != "gif" && $ext != "jpg")) {
      echo("<script language=\"javascript\">alert('拡張子はgifかjpgのファイルを指定してください。');</script>");
      echo("<script language=\"javascript\">history.back();</script>");
      exit;
    }

    switch ($_FILES["imprint_accepted"]["error"]) {
    case UPLOAD_ERR_INI_SIZE:
    case UPLOAD_ERR_FORM_SIZE:
      echo("<script language=\"javascript\">alert('ファイルサイズが大きすぎます。');</script>");
      echo("<script language=\"javascript\">history.back();</script>");
      exit;
    case UPLOAD_ERR_PARTIAL:
    case UPLOAD_ERR_NO_FILE:
      echo("<script language=\"javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
      echo("<script language=\"javascript\">history.back();</script>");
      exit;
    }

    // ファイル保存用ディレクトリがなければ作成
    if (!is_dir("summary")) {
      mkdir("summary", 0755);
    }
    if (!is_dir("summary/imprint")) {
      mkdir("summary/imprint", 0755);
    }


    // 画像がある場合は削除
    $filename_accepted1 = "summary/imprint/".$emp_id."_accepted.gif";
    $filename_accepted2 = "summary/imprint/".$emp_id."_accepted.jpg";

    if (is_file($filename_accepted1)) {
      unlink($filename_accepted1);
    }
    if (is_file($filename_accepted2)) {
      unlink($filename_accepted2);
    }
    // アップロードされたファイルを保存
    $savefilename = "summary/imprint/".$emp_id."_accepted.".$ext;
    $ret = copy($_FILES["imprint_accepted"]["tmp_name"], $savefilename);

    if ($ret == false) {
      echo("<script language=\"javascript\">alert('ファイルのコピーに失敗しました。再度実行してください。');</script>");
      echo("<script language=\"javascript\">history.back();</script>");
      exit;
    }
  }

  if ($filename_operated != "") {
    $delbtn_flg = "t";
    // gif, jpgチェック
    $pos = strrpos($filename_operated, ".");
    $ext = "";
    if ($pos > 0 ) {
      $ext = substr($filename_operated, $pos+1, 3);
      $ext = strtolower($ext);
    }

    if ($pos === false || ($ext != "gif" && $ext != "jpg")) {
      echo("<script language=\"javascript\">alert('拡張子はgifかjpgのファイルを指定してください。');</script>");
      echo("<script language=\"javascript\">history.back();</script>");
      exit;
    }

    switch ($_FILES["imprint_operated"]["error"]) {
    case UPLOAD_ERR_INI_SIZE:
    case UPLOAD_ERR_FORM_SIZE:
      echo("<script language=\"javascript\">alert('ファイルサイズが大きすぎます。');</script>");
      echo("<script language=\"javascript\">history.back();</script>");
      exit;
    case UPLOAD_ERR_PARTIAL:
    case UPLOAD_ERR_NO_FILE:
      echo("<script language=\"javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
      echo("<script language=\"javascript\">history.back();</script>");
      exit;
    }

    // ファイル保存用ディレクトリがなければ作成
    if (!is_dir("summary")) {
      mkdir("summary", 0755);
    }
    if (!is_dir("summary/imprint")) {
      mkdir("summary/imprint", 0755);
    }


    // 画像がある場合は削除
    $filename_operated1 = "summary/imprint/".$emp_id."_operated.gif";
    $filename_operated2 = "summary/imprint/".$emp_id."_operated.jpg";

    if (is_file($filename_operated1)) {
      unlink($filename_operated1);
    }
    if (is_file($filename_operated2)) {
      unlink($filename_operated2);
    }
    // アップロードされたファイルを保存
    $savefilename = "summary/imprint/".$emp_id."_operated.".$ext;
    $ret = copy($_FILES["imprint_operated"]["tmp_name"], $savefilename);

    if ($ret == false) {
      echo("<script language=\"javascript\">alert('ファイルのコピーに失敗しました。再度実行してください。');</script>");
      echo("<script language=\"javascript\">history.back();</script>");
      exit;
    }
  }

  // 印影機能フラグ登録
  $insert_update_flg = "insert";
  if ($db_imprint_flg != "") {
    $insert_update_flg = "update";
  }
  regist_imprint_flg($con, $fname, $emp_id, $imprint_flg, $insert_update_flg);


} else if (@$regist_flg == "2") {
  $delbtn_flg = "f";
  // 画像がある場合は削除
  $filename_accepted1 = "summary/imprint/".$emp_id."_accepted.gif";
  $filename_accepted2 = "summary/imprint/".$emp_id."_accepted.jpg";
  $filename_operated1 = "summary/imprint/".$emp_id."_operated.gif";
  $filename_operated2 = "summary/imprint/".$emp_id."_operated.jpg";

  if (is_file($filename_accepted1)) unlink($filename_accepted1);
  if (is_file($filename_accepted2)) unlink($filename_accepted2);
  if (is_file($filename_operated1)) unlink($filename_operated1);
  if (is_file($filename_operated2)) unlink($filename_operated2);

  // 印影機能レコード削除
  regist_imprint_flg($con, $fname, $emp_id, "", "delete");
}

if (@$imprint_flg == "") {
  $imprint_flg = "f";
  if ($db_imprint_flg != "") $imprint_flg = $db_imprint_flg;
}

?>

<?
//====================================================================
// 初期メニュー オプション機能
//====================================================================

//=================================================
// 初期値
//=================================================
$menu_data = array(
	array("mvalue" => "1", "mword"=>"お知らせ"),
	array("mvalue" => "2", "mword"=>"患者検索"),
	array("mvalue" => "3", "mword"=>"病床検索"),
	array("mvalue" => "4", "mword"=>"複合検索"),
	array("mvalue" => "5", "mword"=>"送信一覧"),
	array("mvalue" => "6", "mword"=>"受信一覧"),
	array("mvalue" => "7", "mword"=>"看護支援")
);
$worksheet_data = array(
	array("mvalue" => "1", "mword"=>"ワークシート"),
	array("mvalue" => "2", "mword"=>"経管栄養一覧"),
	array("mvalue" => "3", "mword"=>"医師指示一覧"),
	array("mvalue" => "4", "mword"=>"温度板"),
	array("mvalue" => "5", "mword"=>"チーム記録"),
	array("mvalue" => "6", "mword"=>"FIM評価表"),
	array("mvalue" => "7", "mword"=>"体重集計表")
);

//=================================================
// 初期表示準備  オプションの値を取得する。
//=================================================
$sql0 =
  " select menu_flg, worksheet_flg, bldg_cd, ward_cd, ptrm_room_no, team_id from sum_application_option".
  "   where emp_id = '".pg_escape_string($emp_id)."'";
$sel0 = @$c_sot_util->select_from_table($sql0);
if (pg_numrows($sel0) == 1) $opt_update_mode = 1;


//=================================================
// 初期メニューオプション表示
//=================================================
if ($opt_update_flg == "") {

	if ($opt_update_mode == 1) {
		$chk_menu_flg = @pg_fetch_result($sel0, 0, "menu_flg");
		$chk_worksheet_flg = @pg_fetch_result($sel0, 0, "worksheet_flg");
		$sel_bldg_cd = @pg_fetch_result($sel0, 0, "bldg_cd");
		$sel_ward_cd = @pg_fetch_result($sel0, 0, "ward_cd");
		$sel_ptrm_room_no = @pg_fetch_result($sel0, 0, "ptrm_room_no");
		$sel_team_id = @pg_fetch_result($sel0, 0, "team_id");
	}


//=================================================
// 初期メニューオプション登録
//=================================================
} else if ($opt_update_flg == 1) {

	// オプション更新
	if ($opt_update_mode == 1) {

		// 初期メニュー オプション更新
		$sql =
			" update sum_application_option set".
			" menu_flg = ".(int)$chk_menu_flg.
			",worksheet_flg = ".(int)$chk_worksheet_flg.
			",bldg_cd = ".($sel_bldg_cd != "" ? (int)$sel_bldg_cd : "null").
			",ward_cd = ".($sel_ward_cd != "" ? (int)$sel_ward_cd : "null").
			",ptrm_room_no = ".($sel_ptrm_room_no != "" ? (int)$sel_ptrm_room_no : "null").
			",team_id = ".($sel_team_id != "" ? (int)$sel_team_id : "null").
			" where emp_id = "."'".$emp_id."'";
		$upd = update_set_table($con, $sql, array(), null, "", $fname);
		if ($upd == 0) {
			pg_close($con);
			summary_common_show_error_page_and_die();
		}
		$update_message =  "オプションは正しく更新されました。";

	// オプション登録
	} else {

		// 初期メニュー オプション登録
		$sql =
			" insert into sum_application_option (".
			" emp_id, menu_flg, worksheet_flg, bldg_cd, ward_cd, ptrm_room_no, team_id".
			" ) values (".
			" '".pg_escape_string($emp_id)."'".
			",".(int)$chk_menu_flg.
			",".(int)$chk_worksheet_flg.
			",".($sel_bldg_cd != "" ? (int)$sel_bldg_cd : "null").
			",".($sel_ward_cd != "" ? (int)$sel_ward_cd : "null").
			",".($sel_ptrm_room_no != "" ? (int)$sel_ptrm_room_no : "null").
			",".($sel_team_id != "" ? (int)$sel_team_id : "null").
			" )";
		//$ins = insert_into_table($con, $sql, array(), $fname);
		$ins = update_set_table($con, $sql, array(), null, "", $fname);
		if ($ins == 0) {
			pg_close($con);
			summary_common_show_error_page_and_die();
		}
		$update_message =  "オプションは正しく登録されました。";

	}
}

if ($chk_menu_flg == "") $chk_menu_flg = "2";
if ($chk_worksheet_flg == "") $chk_worksheet_flg = "1";

//$sel_team_id = (int)@$sel_team_id;


// 事業所
$sql1 = "select bldg_name from bldgmst where bldg_cd = " . (int)@$sel_bldg_cd;
$sel1 = $c_sot_util->select_from_table($sql1);
$bldg_name = @pg_fetch_result($sel1, 0, "bldg_name");


// 病棟
$sql2 =
  " select ward_name from wdmst".
  " where bldg_cd = " . (int)@$sel_bldg_cd.
  " and ward_cd = " . (int)@$sel_ward_cd;
$sel2 = $c_sot_util->select_from_table($sql2);
$ward_name = @pg_fetch_result($sel2, 0, "ward_name");


$EMPTY_OPTION = '<option value="">　　　　</option>';

?>


<script type="text/javascript">
<?//**************************************************************************?>
<?// JavaScript  病棟病室ドロップダウン制御                                   ?>
<?//**************************************************************************?>
var ward_selections = {};
var room_selections = {};
var combobox = {};
function sel_changed(node, selval){
  combobox[1] = document.getElementById("sel_bldg_cd");
  combobox[2] = document.getElementById("sel_ward_cd");
  combobox[3] = document.getElementById("sel_ptrm_room_no");
  if (node < 3) combobox[3].options.length = 1;
  if (node < 2) combobox[2].options.length = 1;

  <? // 初期化用 自分コンボを再選択?>
  var cmb = combobox[node];
  if (node > 0 && cmb.value != selval) {
    for(var i=0; i<cmb.options.length; i++) if (cmb.options[i].value==selval) cmb.selectedIndex = i;
  }

  var idx = 1;
  if (node == 1){
    for(var i in ward_selections){
      if (ward_selections[i].bldg_cd != selval) continue;
      combobox[2].options.length = combobox[2].options.length + 1;
      combobox[2].options[idx].text  = ward_selections[i].name;
      combobox[2].options[idx].value = ward_selections[i].code;
      idx++;
    }
  }
  if (node == 2){
    for(var i in room_selections){
      if (room_selections[i].bldg_cd != combobox[1].value) continue;
      if (room_selections[i].ward_cd != selval) continue;
      combobox[3].options.length = combobox[3].options.length + 1;
      combobox[3].options[idx].text  = room_selections[i].name;
      combobox[3].options[idx].value = room_selections[i].code;
      idx++;
    }
  }
}

</script>

<script type="text/javascript">
function submitForm(flg) {
  document.wkfw.regist_flg.value = flg;
  document.wkfw.opt_update_flg.value = 1;
  document.wkfw.submit();
}

function referTemplate() {
  window.open('sum_workflow_template_refer.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
</head>
<body>

<? summary_common_show_sinryo_top_header($session, $fname, "オプション", $param); ?>

<? summary_common_show_main_tab_menu($con, $fname, "オプション", 1); ?>

<?

// チーム一覧
$team_options = array();
$sel = $c_sot_util->select_from_table("select team_id, team_nm from teammst where team_del_flg = 'f'");
while($row = pg_fetch_array($sel)){
  $team_options[] =
  $selected = ($row["team_id"] == $sel_team_id ? " selected" : "");
  $team_options[] = '<option value="'.$row["team_id"].'"'.$selected.'>'.$row["team_nm"].'</option>';
}


// 事業所選択肢(部門一覧)
$bldg_data_options = array();
$sel = $c_sot_util->select_from_table("select bldg_cd, bldg_name from bldgmst where bldg_del_flg = 'f' order by bldg_cd");
$mst = (array)pg_fetch_all($sel);
foreach($mst as $key => $row) {
  $selected = ($row["bldg_cd"] == @$sel_bldg_cd ? " selected" : "");
  $bldg_data_options[] = '<option value="'.$row["bldg_cd"].'"'.$selected.'>'.$row["bldg_name"].'</option>';
}

// 病棟選択肢 JavaScript変数として出力する
$ary = array();
$sel = $c_sot_util->select_from_table("select bldg_cd, ward_cd, ward_name from wdmst where ward_del_flg = 'f' order by ward_cd");
$mst = (array)pg_fetch_all($sel);
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","code":"'.$row["ward_cd"].'","name":"'.$row["ward_name"].'"}';
echo "<script type='text/javascript'>ward_selections = [". implode(",", $ary) . "];\n</script>\n";

// 病室選択肢 JavaScript変数として出力する
$ary = array();
$sel = $c_sot_util->select_from_table("select bldg_cd, ward_cd, ptrm_room_no, ptrm_name from ptrmmst where ptrm_del_flg = 'f' order by ptrm_room_no");
$mst = (array)pg_fetch_all($sel);
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","ward_cd":"'.$row["ward_cd"].'","code":"'.$row["ptrm_room_no"].'","name":"'.$row["ptrm_name"].'"}';
echo "<script type='text/javascript'>room_selections = [". implode(",", $ary) . "];\n</script>\n";

?>

<table width="100%"; style="background-color:#ffffff"><tr height="26"><td style="text-align:left">
	<span style="background-color:#ffffff; color:#aaaaaa; border:0px; font-size:14px"><? echo($update_message) ?></br></span>
</td></tr></table>

<form name="wkfw" action="sum_application_option.php" method="post" enctype="multipart/form-data">

<table width="100%"; style="background-color:#f4f4f4">
	<tr height="30"><td style="text-align:left"><span style="color:#000000; border:0px; font-size:16px">印影</br></span></td></tr>
</table>
<table class="list_table" cellspacing="1">
	<tr>
		<th style="text-align:right; width:200px">印影機能を利用する</th>
		<td>
			<label><input type="radio" name="imprint_flg" class="baseline" value="t"<? if ($imprint_flg == "t") {echo(" checked");} ?>>はい</label>
			<label><input type="radio" name="imprint_flg" class="baseline" value="f"<? if ($imprint_flg != "t") {echo(" checked");} ?>>いいえ</label>
		</td>
		<th style="text-align:center; width:100px">プレビュー</th>
	</tr>
	<tr>
		<th style="text-align:right">[受付]印影画像指定(jpeg,gif)</th>
		<td><input type="file" name="imprint_accepted" size="60"></td>
		<td style="text-align:center">
			<?
			// 画像がある場合は表示
			$filename = "";
			$filename1 = "summary/imprint/".$emp_id."_accepted.gif";
			$filename2 = "summary/imprint/".$emp_id."_accepted.jpg";
			if (is_file($filename1)) $filename = $filename1;
			else if (is_file($filename2)) $filename = $filename2;

			if ($filename == "") { ?>
			<img src="img/inei.gif">
			<? } else {
			  show_imprint_image($session, $emp_id, 1, "t", "t", "1", 1);
			  $delbtn_flg = "t";
			} ?>
		</td>
	</tr>

	<tr>
		<th style="text-align:right">[実施]印影画像指定(jpeg,gif)</th>
		<td><input type="file" name="imprint_operated" size="60"></td>
		<td style="text-align:center">
			<?
			// 画像がある場合は表示
			$filename = "";
			$filename1 = "summary/imprint/".$emp_id."_operated.gif";
			$filename2 = "summary/imprint/".$emp_id."_operated.jpg";
			if (is_file($filename1)) $filename = $filename1;
			else if (is_file($filename2)) $filename = $filename2;

			if ($filename == "") { ?>
			<img src="img/inei.gif">
			<? } else {
			  // 印影表示関数
			  show_imprint_image($session, $emp_id, 4, "t", "t", "1", 1);
			  $delbtn_flg = "t";
			} ?>
		</td>
	</tr>
</table>

<table width="100%"; style="background-color:#f4f4f4">
	<tr height="5"><td>&nbsp;</td></tr><tr height="30"><td style="text-align:left"><span style="color:#000000; border:0px; font-size:16px">メドレポート</br></span></td></tr>
</table>
<table class="list_table" cellspacing="1">
	<tr>
		<th style="text-align:right; width:200px">最初に開く画面</th>
		<td>
		<? foreach ($menu_data as $mdata){ ?>
			<label><input type="radio" name="chk_menu_flg" class="baseline" value=<?=$mdata["mvalue"]?><? if ($chk_menu_flg == $mdata["mvalue"]) {echo(" checked");} ?>><?=$mdata["mword"]?></label>
		<?  } ?>
		</td>
	</tr>
</table>

<table width="100%"; style="background-color:#f4f4f4">
	<tr height="5"><td>&nbsp;</td></tr><tr height="30"><td style="text-align:left"><span style="color:#000000; border:0px; font-size:16px">看護支援</br></span></td></tr>
</table>
<table class="list_table" cellspacing="1">
	<tr>
		<th style="text-align:right; width:200px">最初に開く画面</th>
		<td>
		<? foreach ($worksheet_data as $wdata){ ?>
			<label><input type="radio" name="chk_worksheet_flg" class="baseline" value=<?=$wdata["mvalue"]?><? if ($chk_worksheet_flg == $wdata["mvalue"]) {echo(" checked");} ?>><?=$wdata["mword"]?></label>
		<?  } ?>
		</td>
	</tr>
	<tr>
		<th style="text-align:right; width:200px">ワークシートの初期検索条件</th>
		<td>
			<table class="list_table" style="margin-bottom:8px; margin-top:4px" cellspacing="1">
			<th bgcolor="#fefcdf">事業所(棟)</th>
			<td><select onchange="sel_changed(1, this.value);" name="sel_bldg_cd" id="sel_bldg_cd"><?=implode("\n", $bldg_data_options);?></select></td>
			<th bgcolor="#fefcdf">病棟</th>
			<td><select onchange="sel_changed(2, this.value);" name="sel_ward_cd" id="sel_ward_cd"><?=$EMPTY_OPTION?></select></td>
			<th bgcolor="#fefcdf">病室</th>
			<td><select name="sel_ptrm_room_no" id="sel_ptrm_room_no"><?=$EMPTY_OPTION?></select></td>
			<script type="text/javascript">sel_changed(1, document.getElementById("sel_bldg_cd").value);</script>
			<script type="text/javascript">sel_changed(2, "<?=@$sel_ward_cd?>");</script>
			<script type="text/javascript">sel_changed(3, "<?=@$sel_ptrm_room_no?>");</script>
			<th bgcolor="#fefcdf">チーム</th>
			<td>
				<select name="sel_team_id" id="sel_team_id"><?=$EMPTY_OPTION?><?=implode("\n", $team_options)?></select>
			</td>
			</tr>
			</table>
		</td>
	</tr>
</table>


<div class="search_button_div">
	<? if ($delbtn_flg == "t") { ?>
	<input type="button" onclick="if(confirm('削除します。よろしいですか？')){submitForm('2');}" value="削除"/>
	<? } ?>
	<input type="button" onclick="submitForm('1');" value="登録"/>
</div>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="regist_flg" value="">
<input type="hidden" name="opt_update_flg" value="">

</form>

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
※メドレポートの最初に開く画面をオプション設定した場合は再ログイン後に有効となります。
</font>

</body>
<? pg_close($con); ?>

</html>
