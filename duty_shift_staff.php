<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">

<?
require_once("about_session.php");
require_once("show_class_name.ini");

require_once("duty_shift_common.ini");
require_once("duty_shift_user_tab_common.ini");
require_once("duty_shift_common_class.php");
require_once("get_menu_label.ini");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// 勤務シフト作成
$shift_menu_label = get_shift_menu_label($con, $fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//管理画面用
$section_admin_auth = $obj->check_authority_Management($session, $fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
//初期値設定
///-----------------------------------------------------------------------------
$create_flg = "";			//権限チェック
$person_charge_id = "";		//シフト管理者ID（責任者）
$caretaker_id = "";			//シフト管理者ID（代行者）
$caretaker2_id = "";		//シフト管理者ID（代行者）
$caretaker3_id = "";		//シフト管理者ID（代行者）
$caretaker4_id = "";		//シフト管理者ID（代行者）
// 20110705 シフト管理者ID（代行者）6人追加開始
$caretaker5_id = "";
$caretaker6_id = "";
$caretaker7_id = "";
$caretaker8_id = "";
$caretaker9_id = "";
$caretaker10_id = "";
// 20110705 シフト管理者ID（代行者）6人追加終了

///-----------------------------------------------------------------------------
// ログインユーザの職員ID・氏名を取得
///-----------------------------------------------------------------------------
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
///-----------------------------------------------------------------------------
//ＤＢ(stmst)より役職情報取得
//ＤＢ(jobmst)より職種情報取得
//ＤＢ(empmst)より職員情報を取得
//ＤＢ(wktmgrp)より勤務パターン情報を取得
///-----------------------------------------------------------------------------
$data_st = $obj->get_stmst_array();
$data_job = $obj->get_jobmst_array();
$data_emp = $obj->get_empmst_array("");
$data_wktmgrp = $obj->get_wktmgrp_array();
///-----------------------------------------------------------------------------
// チーム情報を取得
///-----------------------------------------------------------------------------
$team_array = $obj->get_duty_shift_staff_team_array();
///-----------------------------------------------------------------------------
// グループ情報を取得
///-----------------------------------------------------------------------------
$group_array = $obj->get_duty_shift_group_array("", $data_emp, $data_wktmgrp);
if (count($group_array) <= 0) {
	$err_msg_1 = "勤務シフトグループ情報が未設定です。管理画面で登録してください";
} else {
	if ($group_id == "") {
		//画面初期表示時
		$group_id = $group_array[0]["group_id"];
		$person_charge_id = $group_array[0]["person_charge_id"];			//シフト管理者ID（責任者）
		$caretaker_id = $group_array[0]["caretaker_id"];					//シフト管理者ID（代行者）
		$caretaker2_id = $group_array[0]["caretaker2_id"];
		$caretaker3_id = $group_array[0]["caretaker3_id"];
		$caretaker4_id = $group_array[0]["caretaker4_id"];
		// 20110705 シフト管理者ID（代行者）6人追加開始
		$caretaker5_id = $group_array[0]["caretaker5_id"];
		$caretaker6_id = $group_array[0]["caretaker6_id"];
		$caretaker7_id = $group_array[0]["caretaker7_id"];
		$caretaker8_id = $group_array[0]["caretaker8_id"];
		$caretaker9_id = $group_array[0]["caretaker9_id"];
		$caretaker10_id = $group_array[0]["caretaker10_id"];
		// 20110705 シフト管理者ID（代行者）6人追加終了
		$person_charge_name = $group_array[0]["person_charge_name"];		//シフト管理者名（責任者）
		$caretaker_name = $group_array[0]["caretaker_name"];				//シフト管理者名（代行者）
		$caretaker2_name = $group_array[0]["caretaker2_name"];
		$caretaker3_name = $group_array[0]["caretaker3_name"];
		$caretaker4_name = $group_array[0]["caretaker4_name"];
		// 20110705 シフト管理者ID（代行者）6人追加開始
		$caretaker5_name = $group_array[0]["caretaker5_name"];
		$caretaker6_name = $group_array[0]["caretaker6_name"];
		$caretaker7_name = $group_array[0]["caretaker7_name"];
		$caretaker8_name = $group_array[0]["caretaker8_name"];
		$caretaker9_name = $group_array[0]["caretaker9_name"];
		$caretaker10_name = $group_array[0]["caretaker10_name"];
		// 20110705 シフト管理者ID（代行者）6人追加終了
		$pattern_id = $group_array[0]["pattern_id"];
		$pattern_name = $group_array[0]["pattern_name"];
	} else {
		//病棟名リストボックス選択時
		for ($k=0; $k < count($group_array); $k++) {
			if ($group_array[$k]["group_id"] == $group_id) {
				$person_charge_id = $group_array[$k]["person_charge_id"];			//シフト管理者ID（責任者）
				$caretaker_id = $group_array[$k]["caretaker_id"];
				$caretaker2_id = $group_array[$k]["caretaker2_id"];
				$caretaker3_id = $group_array[$k]["caretaker3_id"];
				$caretaker4_id = $group_array[$k]["caretaker4_id"];					//シフト管理者ID（代行者）
				// 20110705 シフト管理者ID（代行者）6人追加開始
				$caretaker5_id = $group_array[$k]["caretaker5_id"];
				$caretaker6_id = $group_array[$k]["caretaker6_id"];
				$caretaker7_id = $group_array[$k]["caretaker7_id"];
				$caretaker8_id = $group_array[$k]["caretaker8_id"];
				$caretaker9_id = $group_array[$k]["caretaker9_id"];
				$caretaker10_id = $group_array[$k]["caretaker10_id"];
				// 20110705 シフト管理者ID（代行者）6人追加終了

				$person_charge_name = $group_array[$k]["person_charge_name"];
				$caretaker_name = $group_array[$k]["caretaker_name"];
				$caretaker2_name = $group_array[$k]["caretaker2_name"];
				$caretaker3_name = $group_array[$k]["caretaker3_name"];
				$caretaker4_name = $group_array[$k]["caretaker4_name"];
				// 20110705 シフト管理者ID（代行者）6人追加開始
				$caretaker5_name = $group_array[$k]["caretaker5_name"];
				$caretaker6_name = $group_array[$k]["caretaker6_name"];
				$caretaker7_name = $group_array[$k]["caretaker7_name"];
				$caretaker8_name = $group_array[$k]["caretaker8_name"];
				$caretaker9_name = $group_array[$k]["caretaker9_name"];
				$caretaker10_name = $group_array[$k]["caretaker10_name"];
				// 20110705 シフト管理者ID（代行者）6人追加終了
				$pattern_id = $group_array[$k]["pattern_id"];
				$pattern_name = $group_array[$k]["pattern_name"];
			}
		}
	}
	//権限チェック
	if (($emp_id == $person_charge_id) || ($emp_id == $caretaker_id) || ($emp_id == $caretaker2_id) || ($emp_id == $caretaker3_id) || ($emp_id == $caretaker4_id) || ($emp_id == $caretaker5_id) || ($emp_id == $caretaker6_id) || ($emp_id == $caretaker7_id) || ($emp_id == $caretaker8_id) || ($emp_id == $caretaker9_id) || ($emp_id == $caretaker10_id)) {
		$create_flg = "1";
	}
}
///-----------------------------------------------------------------------------
//ＤＢ(atdptn)より出勤パターン情報取得
///-----------------------------------------------------------------------------
$data_atdptn = $obj->get_atdptn_array($pattern_id);
///-----------------------------------------------------------------------------
// スタッフ情報より全登録職員数を取得
///-----------------------------------------------------------------------------
if ($staff_all_cnt == "") {
	$staff_all_cnt = 0;
	for ($k=0; $k<count($group_array); $k++) {
		///-----------------------------------------------------------------------------
		//検索キー設定
		///-----------------------------------------------------------------------------
		$wk_group_id = $group_array[$k]["group_id"];
		///-----------------------------------------------------------------------------
		//データ取得
		///-----------------------------------------------------------------------------
		$sql = "select * from duty_shift_staff";
		$cond = "where group_id = '$wk_group_id' ";
		$cond .= "order by group_id, emp_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		///-----------------------------------------------------------------------------
		// 対象人数カウント
		///-----------------------------------------------------------------------------
		for($i=0;$i<$num;$i++){
			$wk_emp_id = pg_result($sel,$i,"emp_id");
			$staff_all_cnt += 1;
		}
	}
}
///-----------------------------------------------------------------------------
//ライセンス管理より勤務シフト対象者職員数を取得
///-----------------------------------------------------------------------------
$sql = "select * from license";
$cond = "where 1 = 1 ";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$staff_max_cnt = pg_fetch_result($sel, 0, "lcs_shift_emps");
if ($staff_max_cnt == "") { $staff_max_cnt = 0; }
///-----------------------------------------------------------------------------
// スタッフ情報を取得
///-----------------------------------------------------------------------------
$set_array = array();
if (($cause_group_id == $group_id) &&
		($reload_flg == "1")) {
	///-----------------------------------------------------------------------------
	//再表示の場合、元データ取得
	///-----------------------------------------------------------------------------
	$wk_array = array();
	$wk_emp_id_array = split(",", $emp_id_array);
	$m=0;
	foreach ($wk_emp_id_array as $wk_emp_id) {
		$wk_array[$m]["id"] = $wk_emp_id;
		$m++;
	}
	///-----------------------------------------------------------------------------
	//指定職員IDの追加／更新／削除
	///-----------------------------------------------------------------------------
	if ($up_idx != "") {
		///-----------------------------------------------------------------------------
		//変更の場合
		///-----------------------------------------------------------------------------
		$wk_array[$up_idx]["id"] = $up_emp_id;
		$set_array = $wk_array;
	} else if ($del_emp_id != "") {
		///-----------------------------------------------------------------------------
		//削除の場合
		///-----------------------------------------------------------------------------
		$m=0;
		for($i=0; $i<count($wk_array); $i++) {
			if ($wk_array[$i]["id"] != $del_emp_id) {
				$set_array[$m] = $wk_array[$i];
				$m++;
			}
		}
		//追加人数設定
		$staff_all_cnt -= 1;
	} else {
		///-----------------------------------------------------------------------------
		//追加データの取得
		///-----------------------------------------------------------------------------
		$m=0;
		$wk_add_array = array();
		$wk_emp_id_array = split(",", $add_emp_id_array);
		foreach ($wk_emp_id_array as $wk_emp_id) {
			$wk_add_array[$m]["id"] = $wk_emp_id;
			$m++;
		}
		$m=0;
		if ($add_idx == -1) {
			for($k=0; $k<count($wk_add_array); $k++) {
				$set_array[$m] = $wk_add_array[$k];
				$m++;
			}
		}
		if ($emp_id_array != "") {
			for($i=0; $i<count($wk_array); $i++) {
				if ($i == $add_idx) {
					$set_array[$m] = $wk_array[$i];
					$m++;
					for($k=0; $k<count($wk_add_array); $k++) {
						$set_array[$m] = $wk_add_array[$k];
						$m++;
					}
				} else {
					$set_array[$m] = $wk_array[$i];
					$m++;
				}
			}
		}
		///-----------------------------------------------------------------------------
		//追加人数チェック 無制限FULLの場合はチェックしない
		///-----------------------------------------------------------------------------
		if ($staff_max_cnt != "FULL" && ($staff_all_cnt + count($wk_add_array)) > $staff_max_cnt) {
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>alert('ライセンス管理で指定されたシフト対象者数を超えての追加はできません。');</script>");
			///-----------------------------------------------------------------------------
			//追加前のデータに戻す
			///-----------------------------------------------------------------------------
			$set_array = array();
			$m=0;
			for($i=0; $i<count($wk_array); $i++) {
				$set_array[$m] = $wk_array[$i];
				$m++;
			}
		} else {
			$staff_all_cnt += count($wk_add_array);
		}
	}
	///-----------------------------------------------------------------------------
	//データ再設定
	///-----------------------------------------------------------------------------
	$staff_array = array();
	for($i=0;$i<count($set_array);$i++){
		$staff_array[$i] = $obj->get_duty_shift_staff_one_select($set_array[$i]["id"], $group_id);
		$staff_array[$i]["no"] = $i + 1;						//表示順
	}
} else {
	///-----------------------------------------------------------------------------
	//初期表示の場合
	///-----------------------------------------------------------------------------
	$staff_array = $obj->get_duty_shift_staff_array($group_id, $data_st, $data_job, $data_emp);
}

///-----------------------------------------------------------------------------
// 職員のスキルレベル取得
///-----------------------------------------------------------------------------
$skill_array = array();
$sql = "SELECT emp_skill_level FROM emp_skill ";
for($i=0; $i<count($staff_array); $i++){
	$wk_emp_id = $staff_array[$i]["id"];
	$cond = "WHERE emp_id = '$wk_emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$skill_array[$wk_emp_id] = pg_fetch_result($sel, 0, "emp_skill_level");
}
// スキル名表示用
$skill_display = $obj->get_duty_shift_skill_array();

///-----------------------------------------------------------------------------
// ユニット表示仕変
$unit_disp_level = $obj->get_emp_unitlevel($group_id);
///-----------------------------------------------------------------------------

// ユニット（３階層名）表示のため追加
$arr_class_name = get_class_name_array($con, $fname);
$unit_name = $arr_class_name[intval($unit_disp_level) - 1];

// ユニット表示フラグを取得する
$wk_group = $obj->get_duty_shift_group_array($group_id, $data_emp, $data_wktmgrp);

///-----------------------------------------------------------------------------
//他病棟のスタッフ取得
///-----------------------------------------------------------------------------
/*
$m=0;
$wk_array = $obj->get_duty_shift_staff_array("", $data_st, $data_job, $data_emp);
$other_staff_array = array();
for ($i=0;$i<count($wk_array);$i++) {
	if ($wk_array[$i]["group_id"] != $group_id) {
		$other_staff_array[$m] = $wk_array[$i];
		$m++;
	}
}
$other_staff_cnt = count($other_staff_array);
*/
?>

<title>CoMedix <? echo($shift_menu_label); ?>｜スタッフ設定</title>
<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/prototype-1.7.1.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">

	///-----------------------------------------------------------------------------
	// テーブルのDrap&Drop並べ替え
	///-----------------------------------------------------------------------------
	jQuery.noConflict();
	jQuery(document).ready(function($) {
		$('#list > tbody').sortable({
			items: '> tr',
			axis: 'y',
			cancel: 'select',
			placeholder: 'droptarget',
			opacity: 0.4,
			scroll: false
		});
	});

	///-----------------------------------------------------------------------------
	// 副window
	///-----------------------------------------------------------------------------

	var childwin = null;

	///-----------------------------------------------------------------------------
	// openEmployeeList
	///-----------------------------------------------------------------------------
	function openEmployeeList(item_id) {
		dx = screen.width;
		dy = screen.top;
		base = 0;
		wx = 720;
		wy = 600;
		var url = './emplist_popup.php';
		url += '?session=<?=$session?>';
		url += '&emp_id=<?=$emp_id?>';
		url += '&mode=19';
		url += '&item_id='+item_id;
		childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

		childwin.focus();
	}
	///-----------------------------------------------------------------------------
	// closeEmployeeList
	///-----------------------------------------------------------------------------
	function closeEmployeeList() {
		if (childwin != null && !childwin.closed) {
			childwin.close();
		}
		childwin = null;
	}
	///-----------------------------------------------------------------------------
	// add_target_list
	///-----------------------------------------------------------------------------
	function add_target_list(item_id, emp_id, emp_name)
	{
		try {
			var emp_ids = emp_id.split(", ");
			var emp_names = emp_name.split(", ");

			///-----------------------------------------------------------------------------
			//変更用（先頭の人で判定）
			///-----------------------------------------------------------------------------
			wk_emp_id = emp_ids[0];
			wk_emp_name = emp_names[0];
			///-----------------------------------------------------------------------------
			//入力データチェック（他病棟で設定済み）
			///-----------------------------------------------------------------------------
			for(i=0;i<emp_ids.length;i++){
				//他グループに登録済かajaxで確認
				get_group_ajax(emp_ids[i], '<? echo($group_id); ?>' );

				var group_name = document.mainform.chk_group_name.value;
				if (group_name == 'error') {
					alert('エラーが発生したため設定できません。');
					return;
				}
				if (group_name != '0hit') {
					if (group_name != '') {
						msg = '「'+group_name + '」で設定されている職員';
						if (emp_ids.length > 1) {
							msg += '('+emp_names[i]+')';
						}
						msg += 'のため、追加できません。\n「'+group_name + '」で先に削除をして下さい。';
						//alert(msg);
                        msg = '「'+group_name + '」で設定されている職員';
                        if (emp_ids.length > 1) {
							msg += '('+emp_names[i]+')';
						}
						msg += 'ですが、OKボタンで追加します。\n更新ボタンを押した時にもとのシフトグループから削除し移動します。';
                        if (!confirm(msg)) {
						    return;
                        }
					}
				}
			}
			///-----------------------------------------------------------------------------
			//重複チェック（重複データは対象外）
			///-----------------------------------------------------------------------------
			var wk = document.mainform.emp_id_array.value;
			var emp_ids2 = wk.split(",");
			var ok_emp_ids="";
			var err_flg = 0;
			for(i=0;i<emp_ids.length;i++){
				err_flg = 0;
				for(k=0;k<emp_ids2.length;k++){
					if (emp_ids[i] == emp_ids2[k]) {
						err_flg = 1;
						break;
					}
				}
				if (err_flg == 0){
					if (ok_emp_ids != "") {
						ok_emp_ids += ",";
					}
					ok_emp_ids += emp_ids[i];
				}
			}
			if (ok_emp_ids == "") {
				alert('職員の重複指定はできません。');
				return;
			}
			///-----------------------------------------------------------------------------
			//画面再表示
			///-----------------------------------------------------------------------------
			document.mainform.reload_flg.value = "1";
			document.mainform.del_emp_id.value = "";
			document.mainform.up_emp_id.value = wk_emp_id;
			document.mainform.add_emp_id_array.value = ok_emp_ids;
			document.mainform.action="duty_shift_staff.php";
			document.mainform.submit();
		}
		catch(err){
		}
	}

	///-----------------------------------------------------------------------------
	// 勤務条件
	///-----------------------------------------------------------------------------
	function showEmployment(emp_id)
	{
		document.subform.staff_id.value = emp_id;
		document.subform.action="duty_shift_staff_employment.php";
		document.subform.submit();
	}
	///-----------------------------------------------------------------------------
	// 追加
	///-----------------------------------------------------------------------------
	function addEmp(idx, staff_all_cnt, staff_max_cnt)
	{
		///-----------------------------------------------------------------------------
		//追加可能チェック 無制限FULLの場合はチェックしない
		///-----------------------------------------------------------------------------
		if (staff_max_cnt != 'FULL') {
			var int_staff_max_cnt = parseInt(staff_max_cnt, 10);
			if ((staff_all_cnt + 1) > int_staff_max_cnt) {
				alert("ライセンス管理で指定されたシフト対象者数を超えての追加はできません。");
				return;
			}
		}
		///-----------------------------------------------------------------------------
		//追加行
		///-----------------------------------------------------------------------------
		document.mainform.add_idx.value = idx;
		document.mainform.up_idx.value = "";
		document.mainform.del_emp_id.value = "";
	//	document.mainform.submit();
		//職員名簿表示
		openEmployeeList('1');
	}
	///-----------------------------------------------------------------------------
	// 変更
	///-----------------------------------------------------------------------------
	function updateEmp(idx)
	{
		//変更行
		document.mainform.add_idx.value = "";
		document.mainform.up_idx.value = idx;
		document.mainform.del_emp_id.value = "";
	//	document.mainform.submit();
		//職員名簿表示
		openEmployeeList('1');
	}
	///-----------------------------------------------------------------------------
	// 削除
	///-----------------------------------------------------------------------------
	function delEmp(emp_id)
	{
		//画面再表示
		document.mainform.reload_flg.value = "1";
		document.mainform.add_idx.value = "";
		document.mainform.up_idx.value = "";
		document.mainform.up_emp_id.value = "";
		document.mainform.del_emp_id.value = emp_id;
		document.mainform.action="duty_shift_staff.php";
		document.mainform.submit();
	}
	///-----------------------------------------------------------------------------
	// 更新
	///-----------------------------------------------------------------------------
	function editData() {
		///-----------------------------------------------------------------------------
		//職員登録子画面をＣＬＯＳＥ
		///-----------------------------------------------------------------------------
		closeEmployeeList();
		///-----------------------------------------------------------------------------
		//更新処理
		///-----------------------------------------------------------------------------
		document.mainform.action="duty_shift_staff_update_exe.php"
		document.mainform.submit();
	}

	//登録済グループ名取得
	function get_group_ajax(emp_id, group_id)
	{
		var url = 'duty_shift_staff_get_group_ajax.php';
		var params = $H({'session':'<?=$session?>','emp_id':emp_id, 'group_id':group_id}).toQueryString();
		var myAjax = new Ajax.Request(
			url,
			{
				method: 'post',
				postBody: params,
				asynchronous: false
			});
			//同期処理後のデータ処理
			var transport = myAjax.transport;
			set_group_ajax_response(transport);
	}

	//ajax取得データの処理
	function set_group_ajax_response(oXMLHttpRequest)
	{
		if(oXMLHttpRequest.status == 200)
		{
			//alert(oXMLHttpRequest.responseText);
			var response = new ajax_response_object(oXMLHttpRequest.responseText);
			if(response.ajax == "success")
			{
				document.mainform.chk_group_name.value = response.group_nm;
			}
			else if(response.ajax == "0hit")
			{
				//他グループへの登録なし、正常処理
				document.mainform.chk_group_name.value = '0hit';
			}
			else
			{
				//処理失敗
				document.mainform.chk_group_name.value = 'error';
			}
		}
		else
		{
			//通信失敗
			document.mainform.chk_group_name.value = 'error';
		}
	}

	//レスポンスデータよりレスポンスオブジェクトを生成します。(コンストラクタ)
	function ajax_response_object(response_text)
	{
		var a1 = response_text.split("\n");
		for (var i = 0; i < a1.length; i++)
		{
			var line = a1[i];
			var sep_index = line.indexOf("=");
			if(sep_index == -1)
			{
				break;//最後の改行行と判定
			}
			var key = line.substring(0,sep_index);
			var val = line.substring(sep_index+1);

			var eval_string = "this." + key + " = '" + val.replace("'", "\'") + "';";
			eval(eval_string);
		}
	}
	// 相性設定 2012.2.27
	function showPair()
	{
		// 相性設定条件を追加する パラメータ追加
//		document.subform2.action="duty_shift_staff_compatiblity.php";
		document.subform2.action="duty_shift_staff_compatiblity.php?session=<?=$session?>&group_id=<?=$group_id?>&pattern_id=<?=$pattern_id?>";
		document.subform2.submit();
	}
    //履歴子画面を開く
    function openHistWin(emp_id) {

        wx = 500;
        wy = 500;
        dx = screen.width / 2 - (wx / 2);
        dy = screen.top;
        base = screen.height / 2 - (wy / 2);
        var url = 'duty_shift_staff_history_list.php';
        url += '?session=<?=$session?>';
        url += '&emp_id='+emp_id;
        childwin = window.open(url, 'HistPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
        childwin.focus();
    }
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
tr.staff {cursor:move;}
tr.droptarget {background-color:#ffff99; height:60px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<?
// 画面遷移
echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
show_user_title($session, $section_admin_auth);			//duty_shift_common.ini
echo("</table>\n");

// タブ
// 相性設定（パラメータ追加） START
//$arr_option = "&group_id=" . $group_id;
$arr_option = "&group_id=" . $group_id . "&pattern_id=" . $pattern_id;
// 相性設定（パラメータ追加） END
echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
show_user_tab_menuitem($session, $fname, $arr_option);	//duty_shift_user_tab_common.ini
echo("</table>\n");


// 下線
echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	?>
	</td></tr></table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- データ不備時 -->
	<!-- ------------------------------------------------------------------------ -->
<?
if ($err_msg_1 != "") {
	echo($err_msg_1);
	echo("<br>\n");
} else {
	?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 表 -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="staff" method="post">
		<table width="1000" border="0" cellspacing="0" cellpadding="2" class="list">
			<!-- シフトグループ（病棟）名 -->
			<tr height="22">
			<td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフトグループ（病棟）名</font></td>
			<td colspan="3" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<select name="group_id" onchange="this.form.action = '<? echo($fname); ?>'; this.form.submit();">
	<?
	for ($k=0; $k<count($group_array); $k++) {
		$wk_id = $group_array[$k]["group_id"];
		$wk_name = $group_array[$k]["group_name"];
		echo("<option  value=\"$wk_id\"");
		if ($group_id == $wk_id) {
			echo(" selected");
		}
		echo(">$wk_name  \n");
	}
			?>
			</select>
			</font></td>
			</tr>

			<!-- シフト管理者（責任者） -->
			<tr height="22">
			<td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフト管理者（責任者）</font></td>
			<td colspan="3"><? echo($person_charge_name); ?></td>
			</tr>

			<!-- シフト管理者（代行者） -->
			<tr height="22">
			<td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフト管理者（代行者）</font></td>
			<td colspan="3">
	<?
	$caretaker_name_str = $caretaker_name;

	for ($j=2; $j<=10; $j++) {
		$varname = "caretaker".$j."_name";
		if ($caretaker_name_str != "" && $$varname != "") {
			$caretaker_name_str .= "、";
		}
		$caretaker_name_str .= $$varname;
	}

/*
	if ($caretaker_name_str != "" && $caretaker2_name != "") {
		$caretaker_name_str .= "、";
	}
	$caretaker_name_str .= $caretaker2_name;
	if ($caretaker_name_str != "" && $caretaker3_name != "") {
		$caretaker_name_str .= "、";
	}
	$caretaker_name_str .= $caretaker3_name;
	if ($caretaker_name_str != "" && $caretaker4_name != "") {
		$caretaker_name_str .= "、";
	}
	$caretaker_name_str .= $caretaker4_name;
*/
	echo($caretaker_name_str);
?>
			</td>
			</tr>

			<!-- 勤務パターングループ -->
			<tr height="22">
			<td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務パターングループ名</font></td>
			<td colspan="3"><? echo($pattern_name); ?></td>
			</tr>
		</table>

		<input type="hidden" name="session" value="<? echo($session); ?>">
	</form>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 職員一覧 -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="mainform" method="post">
		<table width="1000" border="0" cellspacing="0" cellpadding="0">
		<tr height="22"><td align="right">
	<?
	if ($create_flg == "1") {
		echo("<input type=\"button\" value=\"更新\" onclick=\"editData();\">\n");
	} else {
		echo("<input disabled type=\"button\" value=\"更新\" onclick=\"editData();\">\n");
	}
		?>
		</td></tr>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 職員一覧（見出し） -->
		<!-- ------------------------------------------------------------------------ -->
		<table id="list" class="list" width="1000">
		<thead>
	<?
	// FireFoxではDrag&Drop移動の不具合となるため、特に書式定義をしていないのでthead/tbody/tfootを廃止 2012.5.8
	echo("<tr height=\"22\"> \n");
	echo("<td width=\"60\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">表示順</font></td> \n");
	echo("<td width=\"50\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">チーム</font></td> \n");
    if ($wk_group[0]["unit_disp_flg"] == "t") {
        echo("<td width=\"70\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$unit_name</font></td> \n"); //ユニット表示
    }
    echo("<td width=\"120\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">職員ID</font></td> \n");
	echo("<td width=\"170\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">氏名</font></td> \n");
	echo("<td width=\"100\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">職種</font></td> \n");
	echo("<td width=\"70\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">役職</font></td> \n");
	echo("<td width=\"70\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">スキル</font></td> \n");
	echo("<td width=\"70\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">勤務年数</font></td> \n");
	echo("<td width=\"70\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">夜勤の有無</font></td> \n");
	echo("<td width=\"210\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
	if ($create_flg == "1") {
		echo("<input type=\"button\" value=\"追加\" onclick=\"addEmp(-1, $staff_all_cnt, '$staff_max_cnt');\">\n");
	} else {
		echo("<input disabled type=\"button\" value=\"追加\" onclick=\"addEmp(-1, $staff_all_cnt, $staff_max_cnt);\"> \n");
	}
	// 2012.2.27
	echo("<input type=\"button\" value=\"相性設定\" onclick=\"showPair();\">\n");
	echo("</font></td> \n");
	echo("</tr> \n");
		?>
		</thead>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 職員一覧 -->
		<!-- ------------------------------------------------------------------------ -->
	<?
	for($i=0; $i<count($staff_array); $i++) {
//		echo("<tr height=\"22\"> \n");
		echo("<tr class=\"staff\"> \n");

		echo "<input type=\"hidden\" name=\"new_emp_id[]\" value=\"".$staff_array[$i]['id']."\">"; // テーブルのDrap&Drop並べ替え対応
		// 表示順
		echo("<td width=\"60\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		$disp_num = $i + 1;
		echo ( $disp_num ); // テーブルのDrap&Drop並べ替え対応。選択式から連番を表示するだけに変更
		echo("</font></td>\n");

		// チーム
		echo("<td width=\"50\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
//		echo("<select name=\"team_id[$i]\">\n");
		echo("<select name=\"team_id[]\">\n"); // テーブルのDrap&Drop並べ替え対応
		echo("<option value=\"\"></option>");
		foreach($team_array as $team){
			$selected = "";
			if($team["team_id"] == $staff_array[$i]["team_id"]){
				$selected = 'selected';
			}
			echo("<option value=\"{$team["team_id"]}\" $selected>{$team["team_name"]}</option>");
		}
		echo("</select>\n");
		echo("</font></td>\n");

        // ユニット
        if ($wk_group[0]["unit_disp_flg"] == "t") {
            echo("<td width=\"70\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
            // ユニット表示仕変
            echo ($obj->get_emp_unitname($staff_array[$i]["id"], $group_id));
            echo("</font></td> \n");
        }
        // 職員ID 20130114
		echo("<td width=\"120\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo($staff_array[$i]["emp_personal_id"]);
		echo("</font></td> \n");

		// 氏名
		$color = ( $staff_array[$i]["sex"] == 1 ) ? 'blue' : 'black';
		echo("<td width=\"170\" align=\"center\" bgcolor=\"$wk_bgcolor\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$color\">");
		echo($staff_array[$i]["name"]);
		echo("</font></td> \n");

		// 職種
		echo("<td width=\"100\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo($staff_array[$i]["job_name"]);
		echo("</font></td> \n");

		// 役職
		echo("<td width=\"70\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo($staff_array[$i]["status"]);
		echo("</font></td> \n");

		// スキル
		$wk_emp_id = $staff_array[$i]["id"];
		$skill_level = $skill_array[$wk_emp_id];
		echo("<td width=\"70\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo "<select name=\"skill_level[]\">"; // テーブルのDrap&Drop並べ替え対応
//		for($j=0; $j<count($skill_display); $j++){
		echo("<option value=\"\"></option>");
		foreach($skill_display as $skill_level_name){
			$selected ='';
			if( $skill_level_name["skill_id"] == $skill_level ){
				$selected = 'selected';
			}
			echo "  <option value=\"{$skill_level_name["skill_id"]}\" $selected>{$skill_level_name["skill_name"]}</option>";
		}
		echo "	</select></font></td> "; //<!-- スキル追加・暫定画面。DBから読み込んだものを表示する-->

//		echo("<td width=\"70\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
//		echo($staff_array[$i]["status"]);
//		echo("</font></td> \n");

		// 勤務年数
		echo("<td width=\"70\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo($staff_array[$i]["duty_years"]);
		echo("</font></td> \n");

		// 夜勤の有無
		switch ($staff_array[$i]["night_duty_flg"]) {
			case '1':
				$night_duty = '有り';
				break;
			case '2':
				$night_duty = '夜専';
				break;
			default:
				$night_duty = '無し';
				break;
		}
		echo("<td width=\"80\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo($night_duty);
		echo("</font></td> \n");

		// 勤務条件
		$wk_id = $staff_array[$i]["id"];
		echo("<td width=\"210\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");
		if ($create_flg == "1") {
			echo("<input type=\"button\" value=\"勤務条件\" onclick=\"showEmployment('$wk_id');\"> \n");
            //シフトグループ履歴 20121217病棟
            echo("<input type=\"button\" value=\"シフトグループ履歴\" onclick=\"openHistWin('$wk_id');\"> \n");
		} else {
			echo("<input disabled type=\"button\" value=\"勤務条件\" onclick=\"showEmployment('$wk_id');\"> \n");
		}
		echo("</font></td> \n");

        // 追加／変更／削除
		echo("<td width=\"200\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");
		if ($create_flg == "1") {
			echo("<input type=\"button\" value=\"追加\" onclick=\"addEmp($i, $staff_all_cnt, '$staff_max_cnt');\">\n");
			echo("<input type=\"button\" value=\"変更\" onclick=\"updateEmp($i);\">\n");
			echo("<input type=\"button\" value=\"削除\" onclick=\"delEmp('$wk_id');\">\n");
		} else {
			echo("<input disabled type=\"button\" value=\"追加\" onclick=\"addEmp($i, $staff_all_cnt, $staff_max_cnt);\">\n");
			echo("<input disabled type=\"button\" value=\"変更\" onclick=\"updateEmp($i);\">\n");
			echo("<input disabled type=\"button\" value=\"削除\" onclick=\"delEmp('$wk_id');\">\n");
		}
		echo("</font></td> \n");
//		echo ("</tbody>");
//		echo ("<tfoot></tfoot>");

		echo("</tr> \n");
	}
		?>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 「更新」ボタン -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="1000" border="0" cellspacing="0" cellpadding="0">
		<tr height="22"><td align="right">
	<?
	if ($create_flg == "1") {
		echo("<input type=\"button\" value=\"更新\" onclick=\"editData();\">\n");
	} else {
		echo("<input disabled type=\"button\" value=\"更新\" onclick=\"editData();\">\n");
	}
		?>
		</td></tr>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
		<input type="hidden" name="cause_group_id" value="<? echo($group_id); ?>">
		<input type="hidden" name="pattern_id" value="<? echo($pattern_id); ?>">
		<input type="hidden" name="emp_id_array" value="<? show_emp_id_array($staff_array); ?>">

		<input type="hidden" name="other_emp_id_array" value="<? show_emp_id_array($other_staff_array); ?>">

		<input type="hidden" name="add_idx" value="<? echo($add_idx); ?>">
		<input type="hidden" name="up_idx" value="<? echo($up_idx); ?>">
		<input type="hidden" name="up_emp_id" value="<? echo($up_emp_id); ?>">
		<input type="hidden" name="del_emp_id" value="<? echo($del_emp_id); ?>">
		<input type="hidden" name="add_emp_id_array" value="<? echo($add_emp_id_array); ?>">
		<input type="hidden" name="reload_flg" value="<? echo($reload_flg); ?>">

		<input type="hidden" name="staff_all_cnt" value="<? echo($staff_all_cnt); ?>">
		<input type="hidden" name="chk_group_name" value="">
	</form>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 個人勤務条件設定用  -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="subform" method="post">
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
		<input type="hidden" name="staff_id" value="<? echo($staff_id); ?>">
	</form>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 相性設定 -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="subform2" method="post">
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
<?
		$staff_count = count($staff_array);
		echo ("<input type=\"hidden\" name=\"staff_count\" value=\"$staff_count\">\n");
		for($i=0; $i<count($staff_array); $i++) {
			$staff_id=$staff_array[$i]['id'];
			echo ("<input type=\"hidden\" name=\"staff_id_$i\" value=\"$staff_id\">");
		}
?>

	</form>

	<?
}
	?>

</td>
</tr>
</table>

<div id="comment">
<p>※順番はドラッグ＆ドロップで入れ替えられます。</p>

</div>

</body>
<? pg_close($con); ?>
</html>

<?
///-----------------------------------------------------------------------------
// show_emp_id_array
///-----------------------------------------------------------------------------
function show_emp_id_array($staff_array) {
	for ($i = 0; $i < count($staff_array); $i++) {
		if ($i > 0) {
			echo(",");
		}
		echo($staff_array[$i]["id"]);
	}
}
?>

