<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 17, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (!checkdate($month, $day, $year)) {
	echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}
if ($type == "") {
	echo("<script type=\"text/javascript\">alert('属性を選択してください。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// 対象日付のレコードを削除
$sql = "delete from calendar";
$cond = "where date = '$year$month$day'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// 対象日付のレコードを作成
$sql = "insert into calendar (date, type, memo) values (";
$content = array("$year$month$day", $type, $memo);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query("commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュ、自画面を閉じる
echo("<script type=\"text/javascript\">opener.location.reload();</script>");
echo("<script type=\"text/javascript\">self.close();</script>");
?>
