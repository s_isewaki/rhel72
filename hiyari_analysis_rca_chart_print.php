<?
//****************************************************************************//
//                                                                            //
//          このファイルはShift-JIS、CRLFで保存してください。                 //
//                                                                            //
//****************************************************************************//

define("HORZ_L_UPPER", 16);
define("HORZ_R_UPPER", 32);
define("HORZ_L_UNDER", 64);
define("HORZ_R_UNDER", 128);
define("VERT_UPPER", 256);
define("VERT_CENTER", 512);
define("VERT_UNDER", 1024);
define("ARROW_RIGHT", 2048);
define("ARROW_BOTTOM", 4096);

$PXPY_RATIO = (72/96); //dpi(windows)
$LINESPACE_MARGIN = 30 * $PXPY_RATIO;
$LINESPACE_MARGIN_TOP = 100 * $PXPY_RATIO;
$PAPER_SHORT = 595.28; // A4の場合
$PAPER_LONG = 841.89; // A4の場合
//$PAPER_RATIO = ($PAPER_SHORT-$LINESPACE_MARGIN*2) / ($PAPER_LONG-$LINESPACE_MARGIN_TOP-$LINESPACE_MARGIN*2); // A4の場合の縦横比に、ヘッダ部を加味したもの

ob_start();
require_once("about_session.php");
require_once("about_authority.php");
ob_end_clean();
$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$session_auth = check_authority($session, 47, $fname);
$con = @connect2db($fname);
if(!$session || !$session_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}
$print_title = mb_convert_encoding($_REQUEST["print_title"], "SJIS");
$print_analysis_no = $_REQUEST["print_analysis_no"];
$xls_or_pdf = $_REQUEST["print_xls_or_pdf"];

$print_area_width = $_REQUEST["print_area_width"] * $PXPY_RATIO;
$print_area_height = $_REQUEST["print_area_height"] * $PXPY_RATIO;
$print_coloring = $_REQUEST["print_coloring"];

//echo $print_area_width . " " . $print_area_height . " " . ($print_area_width/$print_area_height) . " " . $PAPER_RATIO;
$orientation = ($print_area_width/$print_area_height) < 1 ? "P" : "L";
$pdf_bairitu = 1;
if ($xls_or_pdf == "pdf"){
	if ($orientation == "L") {
		$pdf_bairitu = min($pdf_bairitu, ($PAPER_LONG-$LINESPACE_MARGIN) / $print_area_width); // 横幅がせまいほう
		$pdf_bairitu = min($pdf_bairitu, ($PAPER_SHORT-$LINESPACE_MARGIN_TOP) / $print_area_height); //縦幅がせまいほう
	}
	if ($orientation == "P") {
		$pdf_bairitu = min($pdf_bairitu, ($PAPER_SHORT-$LINESPACE_MARGIN) / $print_area_width); // 横幅がせまいほう
		$pdf_bairitu = min($pdf_bairitu, ($PAPER_LONG-$LINESPACE_MARGIN_TOP) / $print_area_height); //縦幅がせまいほう
	}
}

$chart_data = chartdata_json_to_array(mb_convert_encoding($_REQUEST["print_chart_data"], "shift_jis", "euc-jp"));
//$chart_data = chartdata_json_to_array($_REQUEST["print_chart_data"]);
$line_data  = linedata_comma_to_array($_REQUEST["print_line_data"]);
$row_heights_data = explode(",", $_REQUEST["print_row_heights_data"]);
$col_widths_data = explode(",", $_REQUEST["print_col_widths_data"]);

//echo $_REQUEST["print_line_data"];
//print_r($line_data);
//print_r($chart_data);
//die;
//$data_col_count = 0;
//$data_row_count = 0;
$col_max_width = array();
$row_max_height =array();
$total_width = array();
$total_height = array();


$col = 0;
foreach($col_widths_data as $w){
	$col++;
	$col_max_width[$col]["px"] = $w;
	$col_max_width[$col]["pt"] = $col_max_width[$col]["px"] * $PXPY_RATIO * $pdf_bairitu;
	$col_max_width[$col]["alt"] = $col_max_width[$col]["px"] * 32;
	$total_width["px"] = @$total_width["px"] + $col_max_width[$col]["px"];
	$total_width["pt"] = @$total_width["pt"] + $col_max_width[$col]["pt"];
	$total_width["alt"] = @$total_width["alt"] + $col_max_width[$col]["alt"];
}

$row = 0;
foreach($row_heights_data as $h){
	$row++;
	$row_max_height[$row]["px"] =$h;
	$row_max_height[$row]["pt"] = $row_max_height[$row]["px"] * $PXPY_RATIO * $pdf_bairitu;
	$row_max_height[$row]["alt"] = $row_max_height[$row]["px"] * 32;
	$total_height["px"] = @$total_height["px"] + $row_max_height[$row]["px"];
	$total_height["pt"] = @$total_height["pt"] + $row_max_height[$row]["pt"];
	$total_height["alt"] = @$total_height["alt"] + $row_max_height[$row]["alt"];
}





function escape_crlf($str){
	return str_replace("\r", "", str_replace("\n", "\\n", trim($str)));
}

function chartdata_json_to_array($str){
	global $PXPY_RATIO;
	global $pdf_bairitu;
	$box_webcolor = array("工程"=>"#ffff77","結果"=>"#d1d1d1","質問"=>"#7eb5ff","原因"=>"#ffa7d3","根本原因"=>"#ff9080","パス"=>"#fff");
	$box_rgbcolor = array(
		"工程"=>array("r"=>255,"g"=>255,"b"=>119),
		"結果"=>array("r"=>209,"g"=>209,"b"=>209),
		"質問"=>array("r"=>126,"g"=>181,"b"=>255),
		"原因"=>array("r"=>255,"g"=>167,"b"=>211),
		"根本原因"=>array("r"=>255,"g"=>144,"b"=>128),
		"パス"=>array("r"=>255,"g"=>255,"b"=>255));
	$out = array();
	$tmp = array();
	$startpos = 0;
	$safe = 0;
	for (;;){
		$safe++;
		if ($safe > 1000) break;
		$strcnt = 0;
		$nextpos1 = strpos($str, ']},"', $startpos);
		$nextpos2 = strpos($str, '"},"',  $startpos);
		if ($nextpos1 > 0 && $nextpos1!==FALSE && ($nextpos1 < $nextpos2 || $nextpos2===FALSE)){ // destがあって、次の要素がある
			$strcnt = $nextpos1 + 3 - $startpos;
			$tmp[] = substr($str, $startpos, $strcnt-1);
		}
		else if ($nextpos2 > 0 && $nextpos2!==FALSE && ($nextpos2 < $nextpos1 || $nextpos1===FALSE)){ // destがなくて、次の要素がある
			$strcnt = $nextpos2 + 3 - $startpos;
			$tmp[] = substr($str, $startpos, $strcnt-1);
		}
		else {
			$tmp[] = substr($str, $startpos);
			break;
		}
		$startpos += $strcnt;
	}
	foreach ($tmp as $row){
		$colonpos = strpos($row, '":{"');
		$rcaid = substr($row, 1, $colonpos-1);
		$rowdata = substr($row, $colonpos + 3, strlen($row)-$colonpos-4);

		$colonpos = strpos($rowdata, '"w":"');
		$endpos = strpos($rowdata, '"', $colonpos+5);
		$rca_w = substr($rowdata, $colonpos+5, $endpos-$colonpos-5);

		$colonpos = strpos($rowdata, '"h":"');
		$endpos = strpos($rowdata, '"', $colonpos+5);
		$rca_h = substr($rowdata, $colonpos+5, $endpos-$colonpos-5);

		$colonpos = strpos($rowdata, '"boxtype":"');
		$endpos = strpos($rowdata, '"', $colonpos+11);
		$rca_boxtype = substr($rowdata, $colonpos+11, $endpos-$colonpos-11);

		$colonpos_dest = strpos($rowdata, '"dest":[');
		$rca_dest = "";
		if ($colonpos_dest!==FALSE) {
			$endpos = strpos($rowdata, ']', $colonpos_dest+7);
			$rca_dest = substr($rowdata, $colonpos_dest+8, $endpos-$colonpos_dest-8);
		}

		$colonpos = strpos($rowdata, '"text":"');
		$endpos = ($colonpos_dest!==FALSE ? $colonpos_dest-4-$colonpos-6 : strlen($rowdata)-$colonpos-8-1);
		$rca_text = substr($rowdata, $colonpos+8, $endpos);
		$rca_text = str_replace('\\"', '"', $rca_text);
		if ($rca_text=="") $rca_text = "<br/>";

		$out[$rcaid] = array(
			"w_px" => $rca_w,
			"h_px" => $rca_h,
			"w_pt" => $rca_w * $PXPY_RATIO * $pdf_bairitu,
			"h_pt" => $rca_h * $PXPY_RATIO * $pdf_bairitu,
			"boxtype" => $rca_boxtype,
			"box_webcolor" => $box_webcolor[$rca_boxtype],
			"box_rgbcolor" => $box_rgbcolor[$rca_boxtype],
			"text" => $rca_text,
			"dest" => ($rca_dest!="" ? explode(",", str_replace('"','',$rca_dest)) : array())
		);
	}
	return $out;
}


function linedata_comma_to_array($str){
	global $PXPY_RATIO;
	global $pdf_bairitu;
	global $LINESPACE_MARGIN;
	global $LINESPACE_MARGIN_TOP;
	$tmp = explode(" ", $str);
	$out = array();
	$startpos = 0;
	$safe = 0;
	foreach ($tmp as $idx => $row){
		$params = explode(",", $row);
		$tmp = explode("_", $params[0]);
		if ($tmp[0]=="imgr" || $tmp[0]=="imgb" || $tmp[0]=="imgl" || $tmp[0]=="imgu") {
			$out[$tmp[1]."_".$tmp[2]."_".$tmp[3]."_arrow_".substr($tmp[0],3,1)] = array(
				"abs_pt_x1" => (($params[5] * $PXPY_RATIO) + $LINESPACE_MARGIN) * $pdf_bairitu,
				"abs_pt_y1" => (($params[6] * $PXPY_RATIO) + $LINESPACE_MARGIN_TOP) * $pdf_bairitu
			);
			continue;
		}
		$out[$tmp[1]."_".$tmp[2]."_".$tmp[3]][] = array (
			"type" =>     $tmp[0],
			"zy" =>       $tmp[1] * $pdf_bairitu,
			"zx" =>       $tmp[2] * $pdf_bairitu,
			"position" => $tmp[3],
			"linebit" => $tmp[4],
			"zl" => $params[1] * $pdf_bairitu,
			"zt" => $params[2] * $pdf_bairitu,
			"zw" => $params[3] * $pdf_bairitu,
			"zh" => $params[4] * $pdf_bairitu,
			"abs_pt_x1" => (($params[5] * $PXPY_RATIO) + $LINESPACE_MARGIN) * $pdf_bairitu,
			"abs_pt_y1" => (($params[6] * $PXPY_RATIO) + $LINESPACE_MARGIN_TOP) * $pdf_bairitu,
			"abs_pt_x2" => ((($params[5] + $params[3]) * $PXPY_RATIO) + $LINESPACE_MARGIN) * $pdf_bairitu,
			"abs_pt_y2" => ((($params[6] + $params[4]) * $PXPY_RATIO) + $LINESPACE_MARGIN_TOP) * $pdf_bairitu
		);
	}
	return $out;
}





//**************************************************************************************************
//**************************************************************************************************
//
//  こ  こ  か  ら  P  D  F  印  刷
//
//**************************************************************************************************
//**************************************************************************************************
// PDF作成については以下を参照しました。
// http://www.phpbook.jp/fpdf/index.html

if ($xls_or_pdf == "pdf"){

	require('fpdf153/mbfpdf.php');

	class CustomMBFPDF extends MBFPDF {
		var $bairitu;
		function SetBairitu($b){
			$this->bairitu = $b;
		}
		function ArrowR($x1,$y1) {
			$sx1 = $x1*$this->k;
			$sy1 = ($this->h-$y1)*$this->k;
			$sx2 = $x1*$this->k;
			$sy2 = ($this->h-$y1)*$this->k - (9*$this->bairitu);
			$sx3 = $x1*$this->k + (4.3*$this->bairitu);
			$sy3 = ($this->h-$y1)*$this->k - (4.5*$this->bairitu);
			$this->SetFillColor(0, 0, 0);
			$this->_out(sprintf('%.2f %.2f m %.2f %.2f l %.2f %.2f l f', $sx1, $sy1, $sx2, $sy2, $sx3, $sy3));
		}
		function ArrowB($x1,$y1) {
			$sx1 = $x1*$this->k;
			$sy1 = ($this->h-$y1)*$this->k;
			$sx2 = $x1*$this->k + (9*$this->bairitu);
			$sy2 = ($this->h-$y1)*$this->k;
			$sx3 = $x1*$this->k + (4.5*$this->bairitu);
			$sy3 = ($this->h-$y1)*$this->k - (4.3*$this->bairitu);
			$this->SetFillColor(0, 0, 0);
			$this->_out(sprintf('%.2f %.2f m %.2f %.2f l %.2f %.2f l f', $sx1, $sy1, $sx2, $sy2, $sx3, $sy3));
		}
		function ArrowL($x1,$y1) {
			$sx1 = $x1*$this->k + (2.15*$this->bairitu);
			$sy1 = ($this->h-$y1)*$this->k;
			$sx2 = $x1*$this->k + (2.15*$this->bairitu);
			$sy2 = ($this->h-$y1)*$this->k - (9*$this->bairitu);
			$sx3 = $x1*$this->k - (2.15*$this->bairitu);
			$sy3 = ($this->h-$y1)*$this->k - (4.5*$this->bairitu);
			$this->SetFillColor(0, 0, 0);
			$this->_out(sprintf('%.2f %.2f m %.2f %.2f l %.2f %.2f l f', $sx1, $sy1, $sx2, $sy2, $sx3, $sy3));
		}
		function ArrowU($x1,$y1) {
			$sx1 = $x1*$this->k;
			$sy1 = ($this->h-$y1)*$this->k - (2.15*$this->bairitu);
			$sx2 = $x1*$this->k + (9*$this->bairitu);
			$sy2 = ($this->h-$y1)*$this->k - (2.15*$this->bairitu);
			$sx3 = $x1*$this->k + (4.5*$this->bairitu);
			$sy3 = ($this->h-$y1)*$this->k + (2.15*$this->bairitu);
			$this->SetFillColor(0, 0, 0);
			$this->_out(sprintf('%.2f %.2f m %.2f %.2f l %.2f %.2f l f', $sx1, $sy1, $sx2, $sy2, $sx3, $sy3));
		}
	}

	$font_size1 = 11 * $PXPY_RATIO * $pdf_bairitu;
	$font_size2 = 22 * $PXPY_RATIO * $pdf_bairitu;
	$pdf = new CustomMBFPDF($orientation, 'pt', 'A4');
	$pdf->AddMBFont(PGOTHIC,'SJIS');
	$pdf->Open();

	$cnt = 0;
	$pdf->AddPage();
	$pdf->SetBairitu($pdf_bairitu);
	$pdf->SetLineWidth(0.7*$pdf_bairitu);
	$pdf->SetAutoPageBreak(false);


	$pdf->SetFont(PGOTHIC,'',$font_size2);

	$pdf->SetXY($LINESPACE_MARGIN * $pdf_bairitu, $LINESPACE_MARGIN * $pdf_bairitu);
	$pdf->Cell(650*$pdf_bairitu, 36*$pdf_bairitu, $print_title, 0, 0, 'L'); // タイトル

	$pdf->SetFont(PGOTHIC,'',$font_size1);
	if ($print_coloring){
		$pdf->SetFillColor(223, 255, 220);
		$pdf->Rect(650*$pdf_bairitu, $LINESPACE_MARGIN * $pdf_bairitu, 150*$pdf_bairitu, 18*$pdf_bairitu, "DF"); // ボックス
		$pdf->SetXY(650*$pdf_bairitu, $LINESPACE_MARGIN * $pdf_bairitu);
		$pdf->MultiCell(150*$pdf_bairitu, 18*$pdf_bairitu, "分析番号", 0, 'C');
	}
	else {
		$pdf->SetXY(650*$pdf_bairitu, $LINESPACE_MARGIN * $pdf_bairitu);
		$pdf->MultiCell(150*$pdf_bairitu, 18*$pdf_bairitu, "分析番号", 1, 'C');
	}
	$pdf->SetXY(650*$pdf_bairitu, ($LINESPACE_MARGIN+18)*$pdf_bairitu);
	$pdf->MultiCell(150*$pdf_bairitu, 18*$pdf_bairitu, $print_analysis_no, 1, 'C');



	$pdf->SetFont(PGOTHIC,'',$font_size1);
	$crow = $LINESPACE_MARGIN_TOP * $pdf_bairitu;
	for ($row = 1; $row <= count($row_heights_data); $row++){
		$ccol = $LINESPACE_MARGIN * $pdf_bairitu;
		for ($col = 1; $col <= count($col_widths_data); $col++){

			//====================================
			// ボックス要素と文字を書く
			//====================================
			if (@$chart_data[$row."_".$col]) {
				$w = @$chart_data[$row."_".$col]["w_pt"];
				$h = @$chart_data[$row."_".$col]["h_pt"];
				$t = @$chart_data[$row."_".$col]["text"];
				if ($t == "<br/>") $t = "";
				if (@$chart_data[$row."_".$col]["boxtype"]!="パス"){
					if ($print_coloring){
						$clr = @$chart_data[$row."_".$col]["box_rgbcolor"];
						$pdf->SetFillColor($clr["r"], $clr["g"], $clr["b"]);
						$pdf->Rect($ccol, $crow, $w, $h, "DF"); // ボックス
					}
					else {
						$pdf->SetFillColor(255, 255, 255);
						$pdf->Rect($ccol, $crow, $w, $h, "DF"); // ボックス
					}
				}

				$pdf->SetXY($ccol+(3*$pdf_bairitu), $crow+(6*$pdf_bairitu)); // 上と左を少々あけつつ・・・
				$pdf->MultiCell($w-(10*$pdf_bairitu), 9*$pdf_bairitu, $t, 0, 'C'); // 左右パディングを含めて、文字のみ書く。必然的に上寄せになってしまう
			}

			//====================================
			// 線を書く
			//====================================
			if (@$line_data[$row."_".$col."_PosBox"]) {
				foreach($line_data[$row."_".$col."_PosBox"] as $idx => $ld) {
					$pdf->Line($ld["abs_pt_x1"], $ld["abs_pt_y1"], $ld["abs_pt_x2"], $ld["abs_pt_y2"]);
				}
			}
			if (@$line_data[$row."_".$col."_PosR"]) {
				foreach($line_data[$row."_".$col."_PosR"] as $idx => $ld) {
					$pdf->Line($ld["abs_pt_x1"], $ld["abs_pt_y1"], $ld["abs_pt_x2"], $ld["abs_pt_y2"]);
				}
			}
			if (@$line_data[$row."_".$col."_PosB"]) {
				foreach($line_data[$row."_".$col."_PosB"] as $idx => $ld) {
					$pdf->Line($ld["abs_pt_x1"], $ld["abs_pt_y1"], $ld["abs_pt_x2"], $ld["abs_pt_y2"]);
				}
			}
			if (@$line_data[$row."_".$col."_PosB2"]) {
				foreach($line_data[$row."_".$col."_PosB2"] as $idx => $ld) {
					$pdf->Line($ld["abs_pt_x1"], $ld["abs_pt_y1"], $ld["abs_pt_x2"], $ld["abs_pt_y2"]);
				}
			}
			if (@$line_data[$row."_".$col."_PosRB"]) {
				foreach($line_data[$row."_".$col."_PosRB"] as $idx => $ld) {
					$pdf->Line($ld["abs_pt_x1"], $ld["abs_pt_y1"], $ld["abs_pt_x2"], $ld["abs_pt_y2"]);
				}
			}
			//====================================
			// 矢印の先を書く
			//====================================
			if (@$line_data[$row."_".$col."_PosR_arrow_r"]) {
				$ld = $line_data[$row."_".$col."_PosR_arrow_r"];
				$pdf->ArrowR($ld["abs_pt_x1"],$ld["abs_pt_y1"]);
			}
			if (@$line_data[$row."_".$col."_PosB_arrow_b"]) {
				$ld = $line_data[$row."_".$col."_PosB_arrow_b"];
				$pdf->ArrowB($ld["abs_pt_x1"],$ld["abs_pt_y1"]);
			}
			if (@$line_data[$row."_".$col."_PosR_arrow_l"]) {
				$ld = $line_data[$row."_".$col."_PosR_arrow_l"];
				$pdf->ArrowL($ld["abs_pt_x1"],$ld["abs_pt_y1"]);
			}
			if (@$line_data[$row."_".$col."_PosB_arrow_u"]) {
				$ld = $line_data[$row."_".$col."_PosB_arrow_u"];
				$pdf->ArrowU($ld["abs_pt_x1"],$ld["abs_pt_y1"]);
			}

			$ccol += ($col_max_width[$col]["pt"] + ($LINESPACE_MARGIN * $pdf_bairitu));
		}
		$crow += ($row_max_height[$row]["pt"] + ($LINESPACE_MARGIN * $pdf_bairitu));
	}

	$pdf->Output("rca_chart_".Date("Ymd_Hi").".pdf", "D");
	die;
}



//**************************************************************************************************
//**************************************************************************************************
//
//  こ  こ  か  ら  E  x  c  e  l  印  刷
//
//**************************************************************************************************
//**************************************************************************************************

//====================================================================
// ヘッダ設定
//====================================================================

header("content-type: application/vnd.ms-excel");
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Disposition: attachment; filename=rca_chart_".Date("Ymd_Hi").".xls;");
header('Content-Type: application/vnd.ms-excel');
header("Content-Transfer-Encoding: binary");

?>

<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=shift_jis">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 11">
<link rel=File-List href="print_excel.files/filelist.xml">
<link rel=Edit-Time-Data href="print_excel.files/editdata.mso">
<link rel=OLE-Object-Data href="print_excel.files/oledata.mso">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>Medi-System</o:Author>
  <o:LastAuthor>Medi-System</o:LastAuthor>
  <o:Created>2010-06-01T00:26:32Z</o:Created>
  <o:LastSaved>2010-06-01T00:35:59Z</o:LastSaved>
  <o:Company>（株）メディシステムソリューション</o:Company>
  <o:Version>11.9999</o:Version>
 </o:DocumentProperties>
</xml><![endif]-->
<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.98in .79in .98in .79in;
	mso-header-margin:.51in;
	mso-footer-margin:.51in;}
tr
	{mso-height-source:auto;
	mso-ruby-visibility:none;}
col
	{mso-width-source:auto;
	mso-ruby-visibility:none;}
br
	{mso-data-placement:same-cell;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:windowtext;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"ＭＳ Ｐゴシック", monospace;
	mso-font-charset:128;
	border:none;
	mso-protection:locked visible;
	mso-style-name:標準;
	mso-style-id:0;}
.font0
	{color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"ＭＳ Ｐゴシック", monospace;
	mso-font-charset:128;}
.font7
	{color:windowtext;
	font-size:14.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"ＭＳ Ｐゴシック", monospace;
	mso-font-charset:128;}
td
	{mso-style-parent:style0;
	padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"ＭＳ Ｐゴシック", monospace;
	mso-font-charset:128;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
.xl_boxY
	{mso-style-parent:style0;
	text-align:center;
	border:.5pt solid windowtext;
	background:#FFFFCC;
	mso-pattern:auto none;
	white-space:normal;}
.xl25
	{mso-style-parent:style0;
	text-align:center;}
.xl_boxG
	{mso-style-parent:style0;
	text-align:center;
	border:.5pt solid windowtext;
	background:silver;
	mso-pattern:auto none;
	white-space:normal;}
.xl_boxB
	{mso-style-parent:style0;
	text-align:center;
	border:.5pt solid windowtext;
	background:#CCFFFF;
	mso-pattern:auto none;
	white-space:normal;}
.xl_boxR
	{mso-style-parent:style0;
	text-align:center;
	border:.5pt solid windowtext;
	background:#FF99CC;
	mso-pattern:auto none;
	white-space:normal;}
.xl_boxV
	{mso-style-parent:style0;
	text-align:center;
	border:.5pt solid windowtext;
	background:#FFCC99;
	mso-pattern:auto none;
	white-space:normal;}
ruby
	{ruby-align:left;}
rt
	{color:windowtext;
	font-size:6.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"ＭＳ Ｐゴシック", monospace;
	mso-font-charset:128;
	mso-char-type:katakana;
	display:none;}
-->
</style>
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
    <x:Name>RCA分析チャート</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:Zoom>100</x:Zoom>
     <x:Selected/>
     <x:DoNotDisplayGridlines/>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>9615</x:WindowHeight>
  <x:WindowWidth>15075</x:WindowWidth>
  <x:WindowTopX>120</x:WindowTopX>
  <x:WindowTopY>30</x:WindowTopY>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="1043"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body link=blue vlink=purple class=xl25>

<table x:str border=0 cellpadding=0 cellspacing=0 width=<?=$total_width["px"]?> style='border-collapse:
 collapse;table-layout:fixed;width:<?=$total_width["px"]?>px'>



<? //======================================================?>
<? // 列の定義                                             ?>
<? //======================================================?>

 <? // 一列目はカラ列30px ?>
 <col class=xl25 width=30 style='mso-width-source:userset;mso-width-alt:960; width:23pt'>
<? for ($col = 1; $col <= count($col_widths_data); $col++){ ?>
	<? // データ行 ?>
 <col class=xl25 width=<?=$col_max_width[$col]["px"]?> style='mso-width-source:userset;mso-width-alt:<?=$col_max_width[$col]["alt"]?>;
 width:<?=$col_max_width[$col]["px"]?>px'>
 <? // ライン行は固定幅30px ?>
 <col class=xl25 width=30 style='mso-width-source:userset;mso-width-alt:960;
 width:23pt'>
<? } ?>


<? //======================================================?>
<? // マージン用の行・ヘッダ行                             ?>
<? //======================================================?>
 <tr height=18 style='height:13.5pt'>
  <td height=18 class=xl25 width=30 style='height:13.5pt;width:23pt'></td>
  <? for ($col = 1; $col <= count($col_widths_data); $col++){ ?>
  <td class=xl25 width=<?=$col_max_width[$col]["px"]?> style='width:<?=$col_max_width[$col]["px"]?>px'></td>
  <td class=xl25 width=30 style='width:30px'></td>
  <? } ?>
 </tr>
 <tr height=18 style='height:13.5pt'>
 	<td height=18 class=xl25 width=30 style='height:13.5pt;width:23pt'></td>
  <td height=18 colspan=<?=count($col_widths_data)*2?> class=xl25 style='height:13.5pt;mso-ignore:colspan'>
<!--[if gte vml 1]><v:rect id="_x0000_s1146"
   style='position:absolute;margin-left:2.25pt;margin-top:.75pt;width:561pt;
   height:27.75pt;mso-wrap-style:tight' filled="f" fillcolor="window [65]"
   stroked="f" strokecolor="windowText [64]" o:insetmode="auto">
   <v:textbox style='mso-direction-alt:auto'>
    <div style='text-align:left;padding-top:5.25pt'><font class="font7"><?=$print_title?></font></div>
   </v:textbox>
   <x:ClientData ObjectType="Rect">
    <x:TextVAlign>Center</x:TextVAlign>
   </x:ClientData>
  </v:rect><v:rect id="_x0000_s1147" style='position:absolute;margin-left:623.25pt;
   margin-top:1.5pt;width:94.25pt;height:18pt;z-index:123;mso-wrap-style:tight'
   fillcolor="#cfc [42]" strokecolor="windowText [64]" o:insetmode="auto">
   <v:textbox style='mso-direction-alt:auto'>
    <div style='text-align:center;padding-top:1.875pt'><font class="font0">分析番号</font></div>
   </v:textbox>
   <x:ClientData ObjectType="Rect">
    <x:TextHAlign>Center</x:TextHAlign>
    <x:TextVAlign>Center</x:TextVAlign>
   </x:ClientData>
  </v:rect><v:rect id="_x0000_s1148" style='position:absolute;margin-left:623.25pt;
   margin-top:19.5pt;width:94.25pt;height:18pt;z-index:124' fillcolor="window [65]"
   strokecolor="windowText [64]" o:insetmode="auto">
   <v:textbox style='mso-direction-alt:auto'>
    <div style='text-align:center;padding-top:1.875pt'><font class="font0"><?=$print_analysis_no?></font></div>
   </v:textbox>
   <x:ClientData ObjectType="Rect">
    <x:TextHAlign>Center</x:TextHAlign>
    <x:TextVAlign>Center</x:TextVAlign>
   </x:ClientData>
   </v:rect><![endif]--><![if !vml]>
  </td>
 </tr>
 <tr height=18 style='height:13.5pt'>
  <td height=18 colspan=<?=count($col_widths_data)*2+1?> class=xl25 style='height:13.5pt;mso-ignore:colspan'></td>
 </tr>
 <tr height=18 style='height:13.5pt'>
  <td height=18 colspan=<?=count($col_widths_data)*2+1?> class=xl25 style='height:13.5pt;mso-ignore:colspan'></td>
 </tr>
 <tr height=18 style='height:13.5pt'>
  <td height=18 colspan=<?=count($col_widths_data)*2+1?> class=xl25 style='height:13.5pt;mso-ignore:colspan'></td>
 </tr>


<? for ($row = 1; $row <= count($row_heights_data); $row++){ ?>

	<? //======================================================?>
	<? // データ行開始                                         ?>
	<? //======================================================?>
 <tr height=<?=$row_max_height[$row]["px"]?> style='mso-height-source:userset;height:<?=$row_max_height[$row]["px"]?>px'>

	<? // １列目A列はカラ列 ?>
  <td height=<?=$row_max_height[$row]["px"]?> class=xl25 style='height:<?=$row_max_height[$row]["px"]?>px'></td>

	<? //======================================================?>
	<? // ２列目以降のデータ行                                 ?>
	<? //======================================================?>
  <? for ($col = 1; $col <= count($col_widths_data); $col++){ ?>

  <td width=<?=$col_max_width[$col]["px"]?> style='width:<?=$col_max_width[$col]["px"]?>px; vertical-align:top'
    align=left valign=top>

<? if (@$chart_data[$row."_".$col]["boxtype"]) { ?>
 <!--[if gte vml 1]><v:rect
   style='position:absolute; margin-left:0pt; margin-top:0pt; z-index:5000; mso-wrap-style:tight;
   width:<?=@$chart_data[$row."_".$col]["w_px"]?>px;
   height:<?=@$chart_data[$row."_".$col]["h_px"]?>px;'
   fillcolor="<?=@$chart_data[$row."_".$col]["box_webcolor"]?>" strokecolor="#000">
   <v:textbox style='mso-direction-alt:auto;' inset="1.5mm,0.8mm,1.5mm,0.8mm">
    <div style='text-align:center;'><font class="font0"><?=str_replace("\n","<br>\n",@$chart_data[$row."_".$col]["text"]) ?></font></div>
   </v:textbox>
   <x:ClientData ObjectType="Rect">
    <x:TextHAlign>Center</x:TextHAlign>
    <x:TextVAlign>Center</x:TextVAlign>
   </x:ClientData>
  </v:rect><![endif]-->
<? } ?>


<? if (@$line_data[$row."_".$col."_PosBox"]) { ?>
<?   foreach ($line_data[$row."_".$col."_PosBox"] as $idx => $ld) {?>
  <!--[if gte vml 1]><v:line style='position:absolute; z-index:1'
    from="<?=$ld["zl"]?>px,<?=$ld["zt"]?>px" to="<?=$ld["zl"]+$ld["zw"]?>px,<?=$ld["zt"]+$ld["zh"]?>px"
    coordsize="21600,21600" strokecolor="windowText [64]" o:insetmode="auto" /><![endif]-->
<?   } ?>
<? } ?>



</td>


	<? // ライン用 ?>
  <td class=xl25 style='mso-ignore:colspan;'>
    <? if (@$line_data[$row."_".$col."_PosR"]) { ?>
      <? foreach ($line_data[$row."_".$col."_PosR"] as $idx => $ld) {?>
        <!--[if gte vml 1]><v:line style='position:absolute; z-index:1'
        from="<?=$ld["zl"]?>px,<?=$ld["zt"]?>px" to="<?=$ld["zl"]+$ld["zw"]?>px,<?=$ld["zt"]+$ld["zh"]?>px"
        coordsize="21600,21600" strokecolor="windowText [64]" o:insetmode="auto">
        <? if (@$line_data[$row."_".$col."_PosR_arrow_r"]){ ?>
        <v:stroke endarrow="block"/>
        <? } ?>
        <? if (@$line_data[$row."_".$col."_PosR_arrow_l"]){ ?>
        <v:stroke startarrow="block"/>
        <? } ?>
        </v:line><![endif]-->
      <? } ?>
    <? } ?>
  </td>

	<? } ?>


 </tr>


	<? //======================================================?>
	<? // ライン行開始                                         ?>
	<? //======================================================?>
 <tr height=30 style='mso-height-source:userset;height:22.5pt'>

	<? // １列目A列はカラ列 ?>
  <td height=30 class=xl25 style='height:22.5pt'></td>


  <? for ($col = 1; $col <= count($col_widths_data); $col++){ ?>


  <td class=xl25 style='mso-ignore:colspan'>
    <? if (@$line_data[$row."_".$col."_PosB"]) { ?>
      <? foreach ($line_data[$row."_".$col."_PosB"] as $idx => $ld) {?>
        <!--[if gte vml 1]><v:line
        style='position:absolute; z-index:1'
        from="<?=$ld["zl"]?>px,<?=$ld["zt"]?>px" to="<?=$ld["zl"]+$ld["zw"]?>px,<?=$ld["zt"]+$ld["zh"]?>px"
        coordsize="21600,21600"
        strokecolor="windowText [64]" o:insetmode="auto">
        <? $line_bit = @$ld["linebit"]; ?>
        <? if (@$line_data[$row."_".$col."_PosB_arrow_r"] && $line_bit == ($line_bit | HORZ_R_UNDER)){ ?>
        <v:stroke endarrow="block"/>
        <? } ?>
        <? if (@$line_data[$row."_".$col."_PosB_arrow_r"] && $line_bit == ($line_bit | HORZ_L_UPPER)){ ?>
        <v:stroke startarrow="block"/>
        <? } ?>
        <? if (@$line_data[$row."_".$col."_PosB_arrow_b"] && $line_bit == ($line_bit | VERT_UNDER)){ ?>
        <v:stroke endarrow="block"/>
        <? } ?>
        <? if (@$line_data[$row."_".$col."_PosB_arrow_u"] && $line_bit == ($line_bit | VERT_UPPER)){ ?>
        <v:stroke startarrow="block"/>
        <? } ?>
        </v:line><![endif]-->
      <? } ?>
    <? } ?>
    <? if (@$line_data[$row."_".$col."_PosB2"]) { ?>
      <? foreach ($line_data[$row."_".$col."_PosB2"] as $idx => $ld) {?>
        <!--[if gte vml 1]><v:line
        style='position:absolute; z-index:1'
        from="<?=$ld["zl"]?>px,<?=$ld["zt"]?>px" to="<?=$ld["zl"]+$ld["zw"]?>px,<?=$ld["zt"]+$ld["zh"]?>px"
        coordsize="21600,21600"
        strokecolor="windowText [64]" o:insetmode="auto">
        <? $line_bit = @$ld["linebit"]; ?>
        <? if (@$line_data[$row."_".$col."_PosB_arrow_r"] && $line_bit == ($line_bit | HORZ_R_UNDER)){ ?>
        <v:stroke endarrow="block"/>
        <? } ?>
        <? if (@$line_data[$row."_".$col."_PosB_arrow_r"] && $line_bit == ($line_bit | HORZ_L_UPPER)){ ?>
        <v:stroke startarrow="block"/>
        <? } ?>
        <? if (@$line_data[$row."_".$col."_PosB_arrow_b"] && $line_bit == ($line_bit | VERT_UNDER)){ ?>
        <v:stroke endarrow="block"/>
        <? } ?>
        <? if (@$line_data[$row."_".$col."_PosB_arrow_u"] && $line_bit == ($line_bit | VERT_UPPER)){ ?>
        <v:stroke startarrow="block"/>
        <? } ?>
        </v:line><![endif]-->
      <? } ?>
    <? } ?>
  </td>


  <td class=xl25 style='mso-ignore:colspan'>
    <? if (@$line_data[$row."_".$col."_PosRB"]) { ?>
      <? foreach ($line_data[$row."_".$col."_PosRB"] as $idx => $ld) {?>
        <!--[if gte vml 1]><v:line style='position:absolute; z-index:1'
        from="<?=$ld["zl"]?>px,<?=$ld["zt"]?>px" to="<?=$ld["zl"]+$ld["zw"]?>px,<?=$ld["zt"]+$ld["zh"]?>px"
        coordsize="21600,21600"
        strokecolor="windowText [64]" o:insetmode="auto" /><![endif]-->
      <? } ?>
    <? } ?>
  </td>

<? } ?>
<? } ?>
 </tr>


 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <? for ($col = 1; $col <= count($col_widths_data); $col++){ ?>
  <td width=30 style='width:23pt'></td>
  <td width=<?=$col_max_width[$col]["px"]?> style='width:<?=$col_max_width[$col]["pt"]?>pt'></td>
  <? } ?>
  <td width=30 style='width:23pt'></td>
 </tr>







 <![endif]>
</table>

</body>

</html>
