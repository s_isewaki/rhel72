<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, 23, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$arr_class_name = get_class_name_array($con, $fname);

// 入力チェック
if ($room_name == "") {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('{$arr_class_name[3]}名を入力してください');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($room_name) > 60) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('{$arr_class_name[3]}名が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($link_key) > 12) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('外部連携キーが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// 室IDを採番
$sql = "select max(room_id) from classroom";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$room_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 表示順の決定
$sql = "select max(order_no) from classroom";
$cond = "where dept_id = $dept_id and not room_del_flg";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$order_no = intval(pg_fetch_result($sel, 0, 0)) + 1;

// トランザクションの開始
pg_query($con, "begin");

// 室情報を登録
$sql = "insert into classroom (room_id, dept_id, room_nm, link_key, order_no) values (";
$content = array($room_id, $dept_id, $room_name, $link_key, $order_no);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_query($con, "commit");
pg_close($con);

// 組織一覧画面へ遷移
echo("<script type=\"text/javascript\">location.href = 'class_menu.php?session=$session&class_id=$class_id&atrb_id=$atrb_id&dept_id=$dept_id';</script>\n");