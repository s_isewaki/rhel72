<?php
require_once("about_comedix.php");
require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");

require_once("show_select_values.ini");
require_once("show_schedule_to_do.ini");
require_once("time_check.ini");
require_once("project_check.ini");
require_once("show_schedule_common.ini");
require_once("show_schedule_chart.ini");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// スケジュール管理権限を取得
$schd_place_auth = check_authority($session, 13, $fname);

if(!isset($_REQUEST['date'])){
    $date = mktime(0,0,0,date('m'), date('d'), date('Y'));
}
else {
    $date = $_REQUEST['date'];
}
$year = date('Y', $date);

$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "SELECT emp_id FROM session WHERE session_id='{$session}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// スケジュール機能の初期画面情報を取得
$default_view = get_default_view($con, $emp_id, $fname);

// アラーム機能
$conf = new Cmx_SystemConfig();
$schedule_alarm = $conf->get('schedule.alarm');

if ($schedule_alarm === '0') {
    header("Location: {$default_view}?session={$session}");
    exit;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | スケジュール確認</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
function alarm_confirm(schd_id, timeless) {
    $.ajax({
        type: "POST",
        url: 'schedule_alarm_confirm.php',
        data: {
            session: '<?php eh($session);?>',
            schd_id: schd_id,
            timeless: timeless
        },
        success: function(){
            location.reload();
        },
        error: function(){
            location.href = 'error.php';
        }
    });
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
    div.subtitle {
        font-size:11pt;
        font-weight: bold;
        margin-top:15px;
    }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php eh($default_view); ?>?session=<?php eh($session); ?>"><img src="img/icon/b02.gif" width="32" height="32" border="0" alt="スケジュール"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php eh($default_view); ?>?session=<?php eh($session); ?>"><b>スケジュール</b></a></font></td>
<? if ($schd_place_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="schedule_place_list.php?session=<?php eh($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #5279a5; margin-bottom:5px;">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_month_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_year_menu.php?session=<?php eh($session); ?>&year=<?php eh($year); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_list_upcoming.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#5279a5"><a href="schedule_confirm.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>確認</b></font></a></td>
<td width="5"></td>
<td width="160" align="center" bgcolor="#bdd1e7"><a href="schedule_search.php?session=<?php eh($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公開スケジュール検索</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_mygroup.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイグループ</font></a></td>
<td width="5"></td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_ical.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カレンダー連携</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_option.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td>&nbsp;</td>
</tr>
</table>

<div class="subtitle">スケジュール確認</div>

<table width="100%" border="1" cellspacing="0" cellpadding="0">
<?
// スケジュール一覧を取得
if ($schedule_alarm === "1") {
    $sql = "
        select schd_id, schd_title, schd_type, schd_start_time_v, schd_dur_v, schd_start_date, schd_imprt, schd_status
        from schdmst
        where emp_id = '$emp_id'
          and alarm_confirm=0
          and alarm>0
          and now() between schd_start_date+(substring(schd_start_time_v from 1 for 2)||':'||substring(schd_start_time_v from 3 for 2))::time - (alarm||'m')::interval and schd_start_date+(substring(schd_start_time_v from 1 for 2)||':'||substring(schd_start_time_v from 3 for 2))::time
        union
        select schd_id, schd_title, schd_type, null, null, schd_start_date, schd_imprt, schd_status
        from schdmst2
        where emp_id = '$emp_id'
          and alarm_confirm=0
          and alarm>0
          and now() between schd_start_date - (alarm||'m')::interval and schd_start_date
        order by schd_start_date, schd_start_time_v
    "; //"
}
else if ($schedule_alarm == "2") {
    $sql = "
        select schd_id, schd_title, schd_type, schd_start_time_v, schd_dur_v, schd_start_date, schd_imprt, schd_status
        from schdmst
        where emp_id = '$emp_id'
          and alarm_confirm=0
          and alarm>0
          and now() <= schd_start_date+(substring(schd_start_time_v from 1 for 2)||':'||substring(schd_start_time_v from 3 for 2))::time
        union
        select schd_id, schd_title, schd_type, null, null, schd_start_date, schd_imprt, schd_status
        from schdmst2
        where emp_id = '$emp_id'
          and alarm_confirm=0
          and alarm>0
          and now() <= schd_start_date
        order by schd_start_date, schd_start_time_v
        limit 5
    "; //"
}
else {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// スケジュール一覧を表示
while ($row = pg_fetch_array($sel)) {
    $schd_id = $row["schd_id"];
    $schd_title = $row["schd_title"];
    $schd_type = $row["schd_type"];
    $schd_start_date = $row["schd_start_date"];
    $schd_start_time = substr($row["schd_start_time_v"], 0, 2).":".substr($row["schd_start_time_v"], 2, 2);
    $schd_dur = substr($row["schd_dur_v"], 0, 2).":".substr($row["schd_dur_v"], 2, 2);
    $schd_status = $row["schd_status"];
    $color = get_schedule_color($schd_type);

    $tmp_date = mktime(0, 0, 0, substr($schd_start_date, 5, 2), substr($schd_start_date, 8, 2), substr($schd_start_date, 0, 4));
    if ($schd_start_time != ":" && $schd_dur != ":") {
        $timeless = "";
        $formatted_time = substr($schd_start_time, 0, 5) . "-" . substr($schd_dur, 0, 5);
    } else {
        $timeless = "t";
        $formatted_time = "指定なし";
    }
    $status = ($schd_status == "2") ? "[未承認]" : "";

?>
<tr height="22" bgcolor="<?php eh($color);?>">
    <td width="100" class="j12">
        <a href="schedule_menu.php?session=<?php eh($session);?>&date=<?php eh($tmp_date);?>"><?php eh($schd_start_date);?></a>
    </td>
    <td width="100" class="j12">
        <?php eh($formatted_time);?>
    </td>
    <td class="j12">
        <a href="javascript:void(0);" onclick="window.open('schedule_update.php?session=<?php eh($session);?>&date=<?php eh($tmp_date);?>&time=<?php eh($schd_start_time);?>&schd_id=<?php eh($schd_id);?>&timeless=<?php eh($timeless);?>', 'newwin', 'width=640,height=560,scrollbars=yes');"><?php eh($schd_title);?><?php eh($status);?></a>
    </td>
    <td width="50" align="center">
        <input type="button" value="確認" onclick="alarm_confirm('<?php eh($schd_id);?>','<?php eh($timeless);?>');">
    </td>
</tr>
<?php
}
?>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width=""><? show_schd_type_color_list($con, $fname); ?></td>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
</td>
</tr>
</table>
</body>
</html>
<? pg_close($con); ?>
