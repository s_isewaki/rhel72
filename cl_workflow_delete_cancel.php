<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_authority.php");
require("about_session.php");
require_once("cl_common.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$check_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//--------------------------------
// パラメータ
// $file_ids
//--------------------------------
$file_ids      = (!is_array($file_ids)) ? array() : $file_ids;
$file_ids_real = (!is_array($file_ids_real)) ? array() : $file_ids_real;


// データベースに接続
$con = connect2db($fname);



// トランザクションの開始
pg_query($con, "begin");

// エイリアスワークフロー
foreach($file_ids as $file_id)
{
	update_del_cancel_wkfwmst($con, $fname, $file_id);
}

// 本体ワークフロー
foreach($file_ids_real as $file_id_real)
{
	update_del_cancel_wkfwmst_real($con, $fname, $file_id_real);
}


// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 一覧画面を再表示
echo("<script type=\"text/javascript\">location.href = 'cl_workflow_menu.php?session=$session'</script>");






// エイリアスワークフロー論理削除更新
function update_del_cancel_wkfwmst($con, $fname, $wkfw_id)
{
	$sql = "update cl_wkfwmst set";
	$set = array("wkfw_del_flg");
	$setvalue = array("f");
	$cond = "where wkfw_id = '$wkfw_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 本体ワークフロー論理削除更新
function update_del_cancel_wkfwmst_real($con, $fname, $wkfw_id)
{
	$sql = "update cl_wkfwmst_real set";
	$set = array("wkfw_del_flg");
	$setvalue = array("f");
	$cond = "where wkfw_id = '$wkfw_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}


?>
