<?
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 23, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
pg_query($con, "begin");

foreach ($_POST as $name => $values) {
	if ($name !== "class") continue;

	$sql = "update classmst set";
	$set = array("order_no");
	$order_no = 1;
	foreach ($values as $class_id) {
		$setvalue = array($order_no);
		$cond = "where class_id = " . (int)$class_id;
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$order_no++;
	}
}

pg_query($con, "commit");
pg_close($con);

echo("<script type=\"text/javascript\">location.href = 'class_menu.php?session=$session';</script>");
