<?
require_once("Cmx.php");
require_once("aclg_set.php");
require_once('about_comedix.php');
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once('Cmx/View/Smarty.php');


//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$auth_id = ($path != "3") ? 47 : 48;
if (check_authority($session, $auth_id, $fname) == "0")
{
    showLoginPage();
    exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==============================
//ログインユーザーID
//==============================
$emp_id = get_emp_id($con,$session,$fname);


//==============================
//初回アクセス時
//==============================
if($is_postback != "true")
{
    //感想ユーザー = ログインユーザ
    $kansou_emp_id = $emp_id;

    //ベースコンテンツIDを取得
    $base_lib_id = get_base_lib_id($con,$fname,$lib_id);
}

//==============================
//感想更新時
//==============================
if($mode == "update_kansou")
{
    //==============================
    //トランザクション開始
    //==============================
    pg_query($con,"begin transaction");

    //==============================
    //デリート
    //==============================
    $sql  = "delete from el_kansou where base_lib_id = {$base_lib_id} and emp_id = '{$kansou_emp_id}'";
    $result = delete_from_table($con, $sql, "", $fname);
    if ($result == 0)
    {
        pg_query($con,"rollback");
        pg_close($con);
        showErrorPage();
        exit;
    }

    //==============================
    //インサート
    //==============================
    $upd_time = date("YmdHis");
    $sql = "insert into el_kansou values(";
    $registry_data = array(
        $base_lib_id,
        $kansou_emp_id,
        pg_escape_string($kansou),
        $upd_time
    );
    $result = insert_into_table($con, $sql, $registry_data, $fname);
    if ($result == 0)
    {
        pg_query($con,"rollback");
        pg_close($db_con);
        showErrorPage();
        exit;
    }

    //==============================
    //コミット
    //==============================
    pg_query($con, "commit");
}

//==============================
//感想削除時
//==============================
if($mode == "delete_kansou")
{
    //==============================
    //デリート
    //==============================
    $sql  = "delete from el_kansou where base_lib_id = {$base_lib_id} and emp_id = '{$kansou_emp_id}'";
    $result = delete_from_table($con, $sql, "", $fname);
    if ($result == 0)
    {
        pg_close($con);
        showErrorPage();
        exit;
    }
}



//==============================
//対象コンテンツに対する感想を全件取得
//==============================

$sql  = " select * from";
$sql .= " (";
$sql .= " select emp_id,kansou,upd_time from el_kansou where base_lib_id = {$base_lib_id}";
$sql .= " ) all_kansou_of_lib";

$sql .= " natural left join";
$sql .= " (";
$sql .= " select ";
$sql .= " emp_id,(emp_lt_nm || ' ' ||emp_ft_nm) as emp_nm,";
$sql .= " emp_class as class_id,";
$sql .= " emp_attribute as atrb_id,";
$sql .= " emp_dept as dept_id,";
$sql .= " emp_room as room_id";
$sql .= " from empmst";
$sql .= " ) all_emp";

$sql .= " natural left join(select class_id,class_nm from classmst where not class_del_flg) all_class";
$sql .= " natural left join(select class_id,atrb_id,atrb_nm from atrbmst where not atrb_del_flg) all_atrb";
$sql .= " natural left join(select atrb_id,dept_id,dept_nm from deptmst where not dept_del_flg) all_dept";
$sql .= " natural left join(select dept_id,room_id,room_nm from classroom where not room_del_flg) all_room";

$sql .= " order by upd_time desc";

$sel = select_from_table($con,$sql,"",$fname);
if($sel == 0)
{
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$kansou_data_list = pg_fetch_all($sel);


//==============================
//ページングに関する情報
//==============================

//pate省略時は先頭ページを対象とする。
if($page == "")
{
    $page = 1;
}

//一画面内の最大表示件数
$disp_count_max_in_page = 20;

//表示開始/終了インデックス
$disp_start_index = ($page - 1)*$disp_count_max_in_page;
$disp_end_index   = $disp_start_index + $disp_count_max_in_page - 1;

//全データ件数
$rec_count = count($kansou_data_list);
if($rec_count == 1 && $kansou_data_list[0] == "")
{
    $rec_count = 0;
}

//最大ページ数
if($rec_count == 0)
{
    $page_max  = 1;
}
else
{
    $page_max  = floor( ($rec_count-1) / $disp_count_max_in_page ) + 1;
}



//==============================
//ページングによる絞込み
//==============================

$kansou_data_list_in_page = array();
for($i=$disp_start_index;$i<=$disp_end_index;$i++)
{
    if($i >= $rec_count)
    {
        break;
    }
    $kansou_data_list_in_page[] = $kansou_data_list[$i];
}


//==============================
//感想表示対象
//==============================

$kansou = "";
foreach($kansou_data_list as $kansou_data)
{
    if($kansou_data['emp_id'] == $kansou_emp_id)
    {
        $kansou = $kansou_data['kansou'];
    }
}


//====================================
//表示
//====================================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("PAGE_TITLE", "感想登録");
$smarty->assign("session", $session);

$smarty->assign("emp_id", $emp_id);
$smarty->assign("kansou_emp_id", $kansou_emp_id);
$smarty->assign("kansou_emp_name", h(get_emp_kanji_name($con,$kansou_emp_id,$fname)));
$smarty->assign("kansou", h($kansou));
$smarty->assign("path", $path);
$smarty->assign("base_lib_id", $base_lib_id);

//ページング
$smarty->assign("page", $page);
$smarty->assign("page_max", $page_max);
$page_list = array();
$page_stt = max(1, $page - 3);
$page_end = min($page_stt + 6, $page_max);
$page_stt = max(1, $page_end - 6);
for ($i=$page_stt; $i<=$page_end; $i++){
    $page_list[] = $i;
}
$smarty->assign("page_list", $page_list);

//一覧
$smarty->assign("rec_count", $rec_count);
$list = array();
foreach($kansou_data_list_in_page as $kansou_data){
    $temp = array();

    $temp['emp_id'] = $kansou_data['emp_id'];
    $temp['emp_nm'] = h($kansou_data['emp_nm']);

    //感想
    $temp['kansou'] = nl2br(h($kansou_data['kansou']));

    //更新日付
    $l_upd_time = $kansou_data['upd_time'];
    $temp['upd_time'] = substr($l_upd_time,0,4).'/'.substr($l_upd_time,4,2).'/'.substr($l_upd_time,6,2);

    //部署名(部署表示設定に依存)
    $l_class_nm = $kansou_data['class_nm'];
    $l_atrb_nm = $kansou_data['atrb_nm'];
    $l_dept_nm = $kansou_data['dept_nm'];
    $l_room_nm = $kansou_data['room_nm'];
    $l_classes_nm = "";
    $profile_use = get_inci_profile($fname, $con);
    if($profile_use["class_flg"] == "t"){
        $l_classes_nm .= $l_class_nm;
    }
    if($profile_use["attribute_flg"] == "t"){
        $l_classes_nm .= $l_atrb_nm;
    }
    if($profile_use["dept_flg"] == "t"){
        $l_classes_nm .= $l_dept_nm;
    }
    if($profile_use["room_flg"] == "t"){
        $l_classes_nm .= $l_room_nm;
    }
    $temp['classes_nm'] = h($l_classes_nm);

    $list[] = $temp;
}
$smarty->assign("list", $list);

if ($design_mode == 1){
    $smarty->display("hiyari_el_kansou1.tpl");
}
else{
    $smarty->display("hiyari_el_kansou2.tpl");
}

pg_close($con);
