<?php
// 出張旅費
// 経路マスタ・フォーム
require_once("about_comedix.php");
require_once("workflow_common.ini");
require_once("referer_common.ini");

$fname = $_SERVER["PHP_SELF"];
$workflow = @$_REQUEST["workflow"];

// セッションと権限のチェック
$session = qualify_session($_REQUEST["session"], $fname);
$checkauth = check_authority($session, 3, $fname);
if ($session == "0" or $checkauth == "0") {
	js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の設定
if ($workflow != "") {
	set_referer($con, $session, "workflow", $workflow, $fname);
	$referer = $workflow;
} else {
	$referer = get_referer($con, $session, "workflow", $fname);
}

if (!empty($_REQUEST["id"])) {
	// 経路マスタの取得
	$id = $_REQUEST["id"];
	$sql = "SELECT ts.*,tp.title as transport_name FROM trip_transit ts JOIN trip_transport tp USING (transport_id) WHERE transit_id={$id}";
	$sel = select_from_table($con,$sql,"",$fname);
	if ($sel == 0) {
		js_error_exit();
	}
	$row = pg_fetch_assoc($sel);
}
else {
	$id = "";
}

// 交通機関マスタの取得
$transport = "";
$sql = "SELECT * FROM trip_transport WHERE view_flg='t' ORDER BY no, transport_id";
$sel = select_from_table($con,$sql,"",$fname);
if ($sel == 0) {
	js_error_exit();
}
while ($r = pg_fetch_assoc($sel)) {
	$transport .= sprintf('<option value="%s">%s</option>', $r['transport_id'], $r['title']);
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー | 出張旅費 | 経路マスタ・フォーム</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/workflow/workflow.css">
<link rel="stylesheet" type="text/css" href="css/workflow/trip.css">
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<? if ($referer == "2") { ?>
<table id="header">
<tr>
<td class="icon"><a href="workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><a href="workflow_menu.php?session=<?=$session?>"><b>ワークフロー</b></a></td>
</tr>
</table>
<? } else { ?>
<table id="header">
<tr>
<td class="icon"><a href="application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b08.gif" width="32" height="32" border="0" alt="決裁・申請"></a></td>
<td>
	<a href="application_menu.php?session=<? echo($session); ?>"><b>決裁・申請</b></a> &gt;
	<a href="workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt;
	<a href="workflow_trip_seisan.php?session=<? echo($session); ?>"><b>出張旅費</b></a>
</td>
<td class="kanri"><a href="application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></td>
</tr>
</table>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="120"><?show_wkfw_sidemenu($session);?></td>
<td width="5"><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top">

<?
show_wkfw_menuitem($session, $fname, array('id' => $id));
show_wkfw_menu2item($session, $fname, array('id' => $id));
?>

<form action="workflow_trip_transit_submit.php" method="post">
<p><b>■経路の登録・変更</b></p>
<table id="form">
<tr>
	<td class="form_title">名称</td>
	<td><input type="text" name="title" size="100" value="<?eh(!empty($row) ? $row["title"] : "");?>" style="ime-mode:active;" /></td>
</tr>

<tr>
	<td class="form_title">交通機関</td>
	<td><select name="transport_id"><?e(!empty($row) ? sprintf('<option value="%s">%s</option>',h($row['transport_id']),h($row['transport_name'])) : "");?><?=$transport?></select></td>
</tr>

<tr>
	<td class="form_title">出発地</td>
	<td><input type="text" name="from_name" size="50" value="<?eh(!empty($row) ? $row["from_name"] : "");?>" style="ime-mode:active;" /></td>
</tr>

<tr>
	<td class="form_title">到着地</td>
	<td><input type="text" name="to_name" size="50" value="<?eh(!empty($row) ? $row["to_name"] : "");?>" style="ime-mode:active;" /></td>
</tr>

<tr>
	<td class="form_title">片道料金</td>
	<td><input type="text" name="fee" size="10" value="<?eh(!empty($row) ? $row["fee"] : "");?>" style="ime-mode:disabled;" />円</td>
</tr>

</table>
<button type="submit">この内容で登録する</button>

<input type="hidden" name="id" value="<?=$id?>" />
<input type="hidden" name="session" value="<?=$session?>" />
</form>

</td>
</tr>
</table>
</body>
</html>
<?php
pg_close($con);