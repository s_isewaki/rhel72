<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">

<?php
// 管理帳票表示
// duty_shift_mprint_setting_preview.php

require_once("about_comedix.php");

require_once("duty_shift_common_class.php");
require_once("duty_shift_mprint_common_class.php");
require_once("work_admin_csv_common.ini");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type='text/javascript' src='js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
//レイアウト名称取得
$obj_mprint = new duty_shift_mprint_common_class($con, $fname);
$arr_layout_mst = $obj_mprint->get_duty_shift_layout_mst_one($layout_id);
$layout_name = $arr_layout_mst["layout_name"];

$shift_group_id = "-";

//レイアウト別に設定
$arr_config = get_timecard_csv_config($con, $fname, $layout_id, "2");
$header_flg = $arr_config["csv_header_flg"];
$wk_ext = $arr_config["csv_ext_name"];
//集計項目見出し
$total_item_title = $arr_config["total_item_title"];
for ($i=2; $i<=8; $i++) {
    $varname = "total_item_title".$i;
    $$varname = $arr_config["$varname"];
}

//項目情報取得 work_admin_csv_common.ini
$arr_items = get_items($con, $fname, $shift_group_id, "2");
//項目名配列
$arr_itemname = array();
for ($i=0; $i<count($arr_items); $i++) {
    $tmp_id = $arr_items[$i]["id"];
    //集計項目見出し
    if ($tmp_id == "2_74" && $total_item_title != "") {
        $arr_items[$i]["name"] = $total_item_title;
    }
    //集計項目"2_69"〜"2_63"見出し
    for ($j=2; $j<=8; $j++) {
        $wk_code = "2_".(69+2-$j);
        if ($tmp_id == $wk_code) {
            $varname = "total_item_title".$j;
            $wk_title = $$varname;
            if ($wk_title != "") {
                $arr_items[$i]["name"] = $wk_title;
            }
        }
    }
    $tmp_name = $arr_items[$i]["name"];
    $arr_itemname[$tmp_id] = $tmp_name;
}
$arr_itemname["9_0"] = "空セル";

$arr_csv_list = get_csv_list($con, $fname, $layout_id, "2");

?>
<title>CoMedix 勤務シフト作成 ｜ 帳票表示</title>
<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
		<tr height="32" bgcolor="#5279a5">
		<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>帳票表示</b></font></td>
		<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr height="32">
<td>&nbsp;&nbsp;<b><?
echo($layout_name); ?></b></td>
</tr>
</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
		<tr height="32" bgcolor="#4f81bd">
<?
foreach ($arr_csv_list as $j => $id) {
    
    $name = $arr_itemname[$id];
    echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"#ffffff\"><b>");
    echo("&nbsp;");
    echo($name);
    echo("</b></font></td>");
    echo("\n");
}
echo("</tr>");
for ($i=0; $i<4; $i++) {
    $wk_color = ($i % 2) ? "#e9edf4" : "#d0d8e8";
    
    echo("<tr height=\"22\" bgcolor=\"$wk_color\">");
    for ($j=0; $j<count($arr_csv_list); $j++) {
        echo("<td><br></td>\n");
    }
    echo("</tr>");
}
?>
</table>

</body>
<? pg_close($con); ?>
</html>

