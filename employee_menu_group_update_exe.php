<?

require("Cmx.php");
require("about_comedix.php");
require("employee_auth_common.php");

// パラメータ
$fname = $_SERVER['PHP_SELF'];
$group_id = $_POST['group_id'];
$group_nm = $_POST['group_nm'];
$menu_ids = $_POST['menu_ids'];
$sidemenu_show_flg = $_POST['sidemenu_show_flg'];
$sidemenu_position = $_POST['sidemenu_position'];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// 入力チェック
if ($group_nm == "") {
    echo("<script type=\"text/javascript\">alert('表示グループ名が入力されていません。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}
if (strlen($group_nm) > 30) {
    echo("<script type=\"text/javascript\">alert('表示グループ名が長すぎます。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

// 重複チェック
$valid_menu_ids = array();
foreach ($menu_ids as $menu_id) {
    if ($menu_id === "-") {
        continue;
    }

    if (in_array($menu_id, $valid_menu_ids)) {
        echo("<script type=\"text/javascript\">alert('ヘッダメニューの選択肢が重複しています。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }

    array_push($valid_menu_ids, $menu_id);
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// ヘッダーグループ更新
$set = array(
    "group_nm"          => p($group_nm),
    "sidemenu_show_flg" => p($sidemenu_show_flg),
    "sidemenu_position" => p($sidemenu_position),
);

$menu_ids_valid = preg_grep('/^\d\d$/', $menu_ids);
foreach (range(1,11) as $count) {
    $set['menu_id_' . $count] = array_shift($menu_ids_valid);
}

$sql = "update menugroup set";
$cond = "where group_id = " . p($group_id);
$upd = update_set_table($con, $sql, array_keys($set), array_values($set), $cond, $fname);
if ($upd == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    js_error_exit();
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面を再表示
echo("<script type=\"text/javascript\">location.href = 'employee_menu_group.php?session={$session}&group_id={$group_id}';</script>");
