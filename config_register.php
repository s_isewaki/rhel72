<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 環境設定| 環境設定</title>
<?
require_once("Cmx.php");
require_once("about_comedix.php");
require("get_values.ini");
require("label_by_profile_type.ini");
require_once("timecard_bean.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 20, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);


//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

// 初期表示時は環境設定値を取得
if ($back != "t") {
	$sql = "select * from config";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$login_page_reflesh = pg_fetch_result($sel, 0, "login_page_reflesh");
	$interval = pg_fetch_result($sel, 0, "interval");
	$login_color = pg_fetch_result($sel, 0, "login_color");
	$login_pos = pg_fetch_result($sel, 0, "login_pos");
	$timecard_use = pg_fetch_result($sel, 0, "timecard_use");
	$title = pg_fetch_result($sel, 0, "title");
	$tmcd_chg_flg = pg_fetch_result($sel, 0, "tmcd_chg_flg");
	$ret_btn_flg = pg_fetch_result($sel, 0, "ret_btn_flg");
	$ic_read_flg = pg_fetch_result($sel, 0, "ic_read_flg");
	$text_show_flg = pg_fetch_result($sel, 0, "text_show_flg");
	$event_show_flg = pg_fetch_result($sel, 0, "event_show_flg");
	$free_text = pg_fetch_result($sel, 0, "free_text");
	$area_show_flg = pg_fetch_result($sel, 0, "area_show_flg");
	$allot_show_flg = pg_fetch_result($sel, 0, "allot_show_flg");
	$welfare_show_flg = pg_fetch_result($sel, 0, "welfare_show_flg");
	$lib_show_flg = pg_fetch_result($sel, 0, "lib_show_flg");
	$qa_show_flg = pg_fetch_result($sel, 0, "qa_show_flg");
	$stat_show_flg = pg_fetch_result($sel, 0, "stat_show_flg");
	$rsv_show_flg = pg_fetch_result($sel, 0, "rsv_show_flg");
	$info_show_flg = pg_fetch_result($sel, 0, "info_show_flg");
	$cal_show_flg = pg_fetch_result($sel, 0, "cal_show_flg");
	$weather_show_flg = pg_fetch_result($sel, 0, "weather_show_flg");
	$wic_show_flg = pg_fetch_result($sel, 0, "wic_show_flg");
	$ext_search_show_flg = pg_fetch_result($sel, 0, "ext_search_show_flg");
	$link_show_flg = pg_fetch_result($sel, 0, "link_show_flg");
	$extension_show_flg = pg_fetch_result($sel, 0, "extension_show_flg");
	$news_new_days = pg_fetch_result($sel, 0, "news_new_days");
	$cal_start_wd = pg_fetch_result($sel, 0, "cal_start_wd");
	$weather_area = pg_fetch_result($sel, 0, "weather_area");
	$user_intra_label_qa = pg_fetch_result($sel, 0, "user_intra_label_qa");
	$user_intra_label_ext = pg_fetch_result($sel, 0, "user_intra_label_ext");
	$login_focus = pg_fetch_result($sel, 0, "login_focus");

	$sql = "select * from loginblock";
	$cond = "order by order_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$center_blocks = array();
	$right_blocks = array();
	while ($row = pg_fetch_array($sel)) {
		if (substr($row["block_id"], 0, 1) == "2") {
			$center_blocks[$row["block_id"]] = $row["order_no"];
		} else if (substr($row["block_id"], 0, 1) == "3") {
			$right_blocks[$row["block_id"]] = $row["order_no"];
		}
	}
} else {
	$center_blocks = array();
	$right_blocks = array();
	foreach ($block_id as $tmp_block_id) {
		$varname = "order_no_$tmp_block_id";
		if (substr($tmp_block_id, 0, 1) == "2") {
			$center_blocks[$tmp_block_id] = $$varname;
		} else if (substr($tmp_block_id, 0, 1) == "3") {
			$right_blocks[$tmp_block_id] = $$varname;
		}
	}
}

// デフォルト値の設定
if ($news_new_days == "") {$news_new_days = 3;}

// イントラネット機能を購入済みかチェック
$sql = "select lcs_func4 from license";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$intra_use_flg = (pg_fetch_result($sel, 0, "lcs_func4") == "t");

// イントラネット機能の各サブ機能名を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$extension_menu = pg_fetch_result($sel, 0, "menu2_2");
$allot_menu = pg_fetch_result($sel, 0, "menu2_4");
$life_menu = pg_fetch_result($sel, 0, "menu3");
$qa_menu = pg_fetch_result($sel, 0, "menu3_3");
$stat_menu = pg_fetch_result($sel, 0, "menu1");
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 社会福祉法人の場合、「・外来」を削除
if ($profile_type == PROFILE_TYPE_WELFARE) {
	$allot_menu = mbereg_replace("・外来", "", $allot_menu);
}
?>
<script type="text/javascript">
function initPage() {
	document.mainform.weather_area.value = '<? echo($weather_area); ?>';
	setRefleshTimeDisabled();
	onLoginPositionChanged();
	setNewsNewDaysDisabled();
	setStartWeekdayDisabled();
	setWeatherDisabled();
	setQaLabelDisabled();
	setExtLabelDisabled();
}

function setRefleshTimeDisabled() {
	var disabled = document.mainform.interval[1].checked;
	document.mainform.login_page_reflesh.disabled = disabled;
}

function onLoginPositionChanged() {
	setTimecardUseDisabled();
	onTimecardUseChanged();
}

function setTimecardUseDisabled() {
	var disabled = !document.mainform.login_pos[0].checked;
	document.mainform.timecard_use[0].disabled = disabled;
	document.mainform.timecard_use[1].disabled = disabled;
}

function onTimecardUseChanged() {
	setTitleDisabled();
	setTimecardChangeDisabled();
	setReturnButtonDisabled();
}

function setTitleDisabled() {
	var disabled = !(document.mainform.login_pos[0].checked && !document.mainform.timecard_use[1].disabled && document.mainform.timecard_use[1].checked);
	document.mainform.title.disabled = disabled;
}

function setTimecardChangeDisabled() {
	var disabled = (document.mainform.login_pos[0].checked && !document.mainform.timecard_use[0].disabled && document.mainform.timecard_use[0].checked);
	document.mainform.tmcd_chg_flg[0].disabled = disabled;
	document.mainform.tmcd_chg_flg[1].disabled = disabled;
}

function setReturnButtonDisabled() {
	var disabled = !((!document.mainform.timecard_use[0].disabled && document.mainform.timecard_use[0].checked) || (!document.mainform.tmcd_chg_flg[0].disabled && document.mainform.tmcd_chg_flg[0].checked));
	document.mainform.ret_btn_flg[0].disabled = disabled;
	document.mainform.ret_btn_flg[1].disabled = disabled;
}

function setNewsNewDaysDisabled() {
	var disabled = !document.mainform.info_show_flg.checked;
	document.mainform.news_new_days.disabled = disabled;
}

function setStartWeekdayDisabled() {
	var disabled = !document.mainform.cal_show_flg.checked;
	document.mainform.cal_start_wd[0].disabled = disabled;
	document.mainform.cal_start_wd[1].disabled = disabled;
}

function setWeatherDisabled() {
	var disabled = !document.mainform.cal_show_flg.checked;
	document.mainform.weather_show_flg.disabled = disabled;
	setWeatherAreaDisabled();
}

function setQaLabelDisabled() {
	if (document.mainform.user_intra_label_qa) {
		var disabled = !document.mainform.qa_show_flg.checked;
		document.mainform.user_intra_label_qa.disabled = disabled;
	}
}

function setExtLabelDisabled() {
	if (document.mainform.user_intra_label_ext) {
		var disabled = !document.mainform.extension_show_flg.checked;
		document.mainform.user_intra_label_ext.disabled = disabled;
	}
}

function setWeatherAreaDisabled() {
	var disabled = (!document.mainform.weather_show_flg.checked || document.mainform.weather_show_flg.disabled);
	document.mainform.weather_area.disabled = disabled;
}

function describeLoginURL() {
	var url = location.href.replace(/[^\/]*$/, '').concat('?mode=admin');
	alert('ログインするには下記URLにアクセスしてください。\n\n'.concat(url));
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
if(!tinyMCE.isOpera)
{
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		plugins : "preview,table,emotions,fullscreen,layer",
		//language : "ja_euc-jp",
		language : "ja",
		width : "100%",
		height : "200",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat",
		theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,|,hr,|,charmap,emotions,|,preview,|,undo,redo,|,fullscreen",
		theme_advanced_buttons3 : "tablecontrols,|,visualaid,|,code",
		content_css : "tinymce/tinymce_content.css",
		extended_valid_elements : "iframe[align<bottom?left?middle?right?top|class|frameborder|height|id|longdesc|marginheight|marginwidth|name|scrolling<auto?no?yes|src|style|title|width]",
		theme_advanced_statusbar_location : "none",
		force_br_newlines : true,
		forced_root_block : '',
		force_p_newlines : false
	});
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="config_register.php?session=<? echo($session); ?>"><img src="img/icon/b28.gif" width="32" height="32" border="0" alt="環境設定"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="config_register.php?session=<? echo($session); ?>"><b>環境設定</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="110" align="center" bgcolor="#5279a5"><a href="config_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>ログイン画面</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="config_sidemenu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="config_siteid.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サイトID</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="config_log.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログ</font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="config_server_information.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サーバ情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="config_client_auth.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">端末認証履歴</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="config_other.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">その他</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="4"><br>
<form name="mainform" action="config_insert.php" method="post">
<table width="800" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="30">
<td width="23%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">自動更新</font></td>
<td width="31%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="interval" type="radio" value="t" onclick="setRefleshTimeDisabled();"<? if ($interval == "t") {echo(" checked");} ?>>する
<input name="interval" type="radio" value="f" onclick="setRefleshTimeDisabled();"<? if ($interval == "f") {echo(" checked");} ?>>しない
</font></td>
<td width="21%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新間隔</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input name="login_page_reflesh" type="text" size="3" maxlength="3" value="<? echo($login_page_reflesh); ?>">秒</font></td>
</tr>
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カラートーン</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="login_color" type="radio" value="1"<? if ($login_color == "1") {echo(" checked");} ?>>ブルー
<input name="login_color" type="radio" value="2"<? if ($login_color == "2") {echo(" checked");} ?>>グリーン
</font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログインフォームの位置</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="login_pos" type="radio" value="1" onclick="onLoginPositionChanged();"<? if ($login_pos == "1") {echo(" checked");} ?>>中央
<input name="login_pos" type="radio" value="2" onclick="onLoginPositionChanged();"<? if ($login_pos == "2") {echo(" checked");} ?>>ヘッダー部
<input name="login_pos" type="radio" value="3" onclick="onLoginPositionChanged();describeLoginURL();"<? if ($login_pos == "3") {echo(" checked");} ?>>なし
</font></td>
</tr>
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">画面形式</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="timecard_use" type="radio" value="t" onclick="onTimecardUseChanged();"<? if ($timecard_use == "t") {echo(" checked");} ?>>タイムカード表示
<input name="timecard_use" type="radio" value="f" onclick="onTimecardUseChanged();"<? if ($timecard_use == "f") {echo(" checked");} ?>>タイトル表示
</font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td><input name="title" type="text" size="30" maxlength="50" value="<? echo(h($title)); ?>"></td>
</tr>
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムカード切り替えボタン</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="tmcd_chg_flg" type="radio" value="t" onclick="setReturnButtonDisabled();"<? if ($tmcd_chg_flg == "t") {echo(" checked");} ?> onclick="">表示
<input name="tmcd_chg_flg" type="radio" value="f" onclick="setReturnButtonDisabled();"<? if ($tmcd_chg_flg == "f") {echo(" checked");} ?> onclick="">非表示
</font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出出勤";
echo($ret_str);
?>ボタン</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="ret_btn_flg" type="radio" value="t"<? if ($ret_btn_flg == "t") {echo(" checked");} ?>>表示
<input name="ret_btn_flg" type="radio" value="f"<? if ($ret_btn_flg != "t") {echo(" checked");} ?>>非表示
</font></td>
</tr>
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログインIDの自動フォーカス</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="login_focus" type="radio" value="t"<? if ($login_focus == "t") {echo(" checked");} ?>>する
<input name="login_focus" type="radio" value="f"<? if ($login_focus == "f") {echo(" checked");} ?>>しない<br>
<font color="red">※ログインIDに自動フォーカスする場合、ログイン画面からリンクライブラリで外部プログラムを起動した際に、フォーカスが自動的にログインIDフィールドに移動するため、外部プログラムが最小化されることがあります。</font>
</font></td>
</tr>
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ICカード読み込みボタン</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="ic_read_flg" type="radio" value="t"<? if ($ic_read_flg == "t") {echo(" checked");} ?>>表示
<input name="ic_read_flg" type="radio" value="f"<? if ($ic_read_flg == "f") {echo(" checked");} ?>>非表示
</font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="30">
<td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>ブロック設定</b></font></td>
</tr>
</table>
<table width="800" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="30" bgcolor="#f6f9ff">
<td align="right" width="23%" rowspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">トップブロック</font></td>
<td align="center" width="7%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示</font></td>
<td colspan="2" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容（フリーテキスト）</font></td>
</tr>
<tr height="30">
<td align="center"><input type="checkbox" name="text_show_flg" value="t"<? if ($text_show_flg == "t") {echo(" checked");} ?>></td>
<td colspan="2"><textarea name="free_text" cols="50" rows="6" style="ime-mode:active;"><? echo($free_text); ?></textarea></td>
</tr>

<tr height="30">
<td align="center"><input type="checkbox" name="event_show_flg" value="t"<? if ($event_show_flg == "t") {echo(" checked");} ?>></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">院内行事</font></td>
</tr>

<tr height="30" bgcolor="#f6f9ff">
<td align="right" width="23%" rowspan="11"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">中央ブロック</font></td>
<td align="center" width="7%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font></td>
<td align="center" width="12%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示順</font></td>
</tr>
<? foreach ($center_blocks as $block_id => $order_no) { ?>
<tr height="30">
<? 	if ($block_id == "201") { ?>
<td align="center"><input type="checkbox" name="area_show_flg" value="t"<? if ($area_show_flg == "t") {echo(" checked");} ?>></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">エリア別電話番号検索</font></td>
<? 	} ?>
<? 	if ($block_id == "202") { ?>
<td align="center"><input type="checkbox" name="allot_show_flg" value="t"<? if ($allot_show_flg == "t") {echo(" checked");} ?><? if (!$intra_use_flg) {echo(" disabled");} ?>></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($allot_menu); ?></font></td>
<? 	} ?>
<? 	if ($block_id == "203") { ?>
<td align="center"><input type="checkbox" name="welfare_show_flg" value="t"<? if ($welfare_show_flg == "t") {echo(" checked");} ?><? if (!$intra_use_flg) {echo(" disabled");} ?>></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($life_menu); ?></font></td>
<? 	} ?>
<? 	if ($block_id == "204") { ?>
<td align="center"><input type="checkbox" name="lib_show_flg" value="t"<? if ($lib_show_flg == "t") {echo(" checked");} ?>></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書管理</font></td>
<? 	} ?>
<? 	if ($block_id == "205") { ?>
<td align="center"><input type="checkbox" name="qa_show_flg" value="t"<? if ($qa_show_flg == "t") {echo(" checked");} ?> onclick="setQaLabelDisabled();"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">Q&amp;A<? if ($intra_use_flg) { ?>（<input type="checkbox" name="user_intra_label_qa" value="t"<? if ($user_intra_label_qa == "t") {echo(" checked");} ?>>イントラネットのメニュー名を使用)<? } ?></font></td>
<? 	} ?>
<? 	if ($block_id == "206") { ?>
<td align="center"><input type="checkbox" name="stat_show_flg" value="t"<? if ($stat_show_flg == "t") {echo(" checked");} ?><? if (!$intra_use_flg) {echo(" disabled");} ?>></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($stat_menu); ?></font></td>
<? 	} ?>
<? 	if ($block_id == "207") { ?>
<td align="center"><input type="checkbox" name="info_show_flg" value="t"<? if ($info_show_flg == "t") {echo(" checked");} ?> onclick="setNewsNewDaysDisabled();"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ</font></td>
<? 	} ?>
<? 	if ($block_id == "208") { ?>
<td align="center"><input type="checkbox" name="rsv_show_flg" value="t"<? if ($rsv_show_flg == "t") {echo(" checked");} ?>></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設備予約</font></td>
<? 	} ?>
<? 	if ($block_id == "209") { ?>
<td align="center"><input type="checkbox" name="cal_show_flg" value="t"<? if ($cal_show_flg == "t") {echo(" checked");} ?> onclick="setStartWeekdayDisabled();setWeatherDisabled();"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カレンダー（<input type="checkbox" name="weather_show_flg" value="t"<? if ($weather_show_flg == "t") {echo(" checked");} ?> onclick="setWeatherAreaDisabled();">天気予報も表示）</font></td>
<? 	} ?>
<? 	if ($block_id == "210") { ?>
<td align="center"><input type="checkbox" name="wic_show_flg" value="t"<? if ($wic_show_flg == "t") {echo(" checked");} ?>></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">WICレポート</font></td>
<? 	} ?>
<td align="center"><input type="text" name="order_no_<? echo($block_id); ?>" value="<? echo($order_no); ?>" size="3" maxlength="2" style="ime-mode:inactive;"><input type="hidden" name="block_id[]" value="<? echo($block_id); ?>"></td>
</tr>
<? } ?>
<tr height="30" bgcolor="#f6f9ff">
<td align="right" width="23%" rowspan="4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">右ブロック</font></td>
<td align="center" width="7%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font></td>
<td align="center" width="12%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示順</font></td>
</tr>
<? foreach ($right_blocks as $block_id => $order_no) { ?>
<tr height="30">
<? 	if ($block_id == "301") { ?>
<td align="center"><input type="checkbox" name="ext_search_show_flg" value="t"<? if ($ext_search_show_flg == "t") {echo(" checked");} ?>></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一発電話検索</font></td>
<? 	} ?>
<? 	if ($block_id == "302") { ?>
<td align="center"><input type="checkbox" name="link_show_flg" value="t"<? if ($link_show_flg == "t") {echo(" checked");} ?>></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リンクライブラリ</font></td>
<? 	} ?>
<? 	if ($block_id == "303") { ?>
<td align="center"><input type="checkbox" name="extension_show_flg" value="t"<? if ($extension_show_flg == "t") {echo(" checked");} ?> onclick="setExtLabelDisabled();"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内線電話帳<? if ($intra_use_flg) { ?>（<input type="checkbox" name="user_intra_label_ext" value="t"<? if ($user_intra_label_ext == "t") {echo(" checked");} ?>>イントラネットのメニュー名を使用)<? } ?></font></td>
<? 	} ?>
<td align="center"><input type="text" name="order_no_<? echo($block_id); ?>" value="<? echo($order_no); ?>" size="3" maxlength="2" style="ime-mode:inactive;"><input type="hidden" name="block_id[]" value="<? echo($block_id); ?>"></td>
</tr>
<? } ?>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="30">
<td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>オプション設定</b></font></td>
</tr>
</table>
<table width="800" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせのNEW期間</font></td>
<td colspan="3">
<select name="news_new_days">
<? show_new_days_options($news_new_days); ?>
</select>
</td>
</tr>
<tr height="30">
<td align="right" width="23%" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カレンダーのスタート曜日</font></td>
<td width="31%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="cal_start_wd" type="radio" value="1"<? if ($cal_start_wd == "1") {echo(" checked");} ?>>日曜
<input name="cal_start_wd" type="radio" value="2"<? if ($cal_start_wd == "2") {echo(" checked");} ?>>月曜
</font></td>
<td align="right" width="21%" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">天気予報の地域</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="weather_area">
<optgroup label="道北">
<option value="1100">宗谷
<option value="1200">上川
<option value="1300">留萌
</optgroup>
<optgroup label="道央">
<option value="1400">札幌
<option value="1500">岩見沢
<option value="1600">倶知安
</optgroup>
<optgroup label="道東">
<option value="1710">網走
<option value="1720">北見
<option value="1730">紋別
<option value="1800">根室
<option value="1900">釧路
<option value="2000">帯広
</optgroup>
<optgroup label="道南">
<option value="2100">室蘭
<option value="2200">浦河
<option value="2300">函館
<option value="2400">江差
</optgroup>
<optgroup label="青森">
<option value="3110">青森
<option value="3120">むつ
<option value="3130">八戸
</optgroup>
<optgroup label="秋田">
<option value="3210">秋田
<option value="3220">横手
</optgroup>
<optgroup label="岩手">
<option value="3310">盛岡
<option value="3320">宮古
<option value="3330">大船渡
</optgroup>
<optgroup label="宮城">
<option value="3410">仙台
<option value="3420">白石
</optgroup>
<optgroup label="山形">
<option value="3510">山形
<option value="3520">米沢
<option value="3530">酒田
<option value="3540">新庄
</optgroup>
<optgroup label="福島">
<option value="3610">福島
<option value="3620">小名浜
<option value="3630">若松
</optgroup>
<optgroup label="茨城">
<option value="4010">水戸
<option value="4020">土浦
</optgroup>
<optgroup label="栃木">
<option value="4110">宇都宮
<option value="4120">大田原
</optgroup>
<optgroup label="群馬">
<option value="4210">前橋
<option value="4220">水上
</optgroup>
<optgroup label="埼玉">
<option value="4310">さいたま
<option value="4320">熊谷
<option value="4330">秩父
</optgroup>
<optgroup label="東京">
<option value="100">八丈島
<option value="4410">東京
<option value="4420">大島
<option value="9600">父島
</optgroup>
<optgroup label="千葉">
<option value="4510">千葉
<option value="4520">銚子
<option value="4530">館山
</optgroup>
<optgroup label="神奈川">
<option value="4610">横浜
<option value="4620">小田原
</optgroup>
<optgroup label="長野">
<option value="4810">長野
<option value="4820">松本
<option value="4830">飯田
</optgroup>
<optgroup label="山梨">
<option value="4910">甲府
<option value="4920">河口湖
</optgroup>
<optgroup label="静岡">
<option value="5010">静岡
<option value="5020">網代
<option value="5030">三島
<option value="5040">浜松
</optgroup>
<optgroup label="愛知">
<option value="5110">名古屋
<option value="5120">豊橋
</optgroup>
<optgroup label="岐阜">
<option value="5210">岐阜
<option value="5220">高山
</optgroup>
<optgroup label="三重">
<option value="5310">津
<option value="5320">尾鷲
</optgroup>
<optgroup label="新潟">
<option value="5410">新潟
<option value="5420">長岡
<option value="5430">高田
<option value="5440">相川
</optgroup>
<optgroup label="富山">
<option value="5510">富山
<option value="5520">伏木
</optgroup>
<optgroup label="石川">
<option value="5610">金沢
<option value="5620">輪島
</optgroup>
<optgroup label="福井">
<option value="5710">福井
<option value="5720">敦賀
</optgroup>
<optgroup label="滋賀">
<option value="6010">大津
<option value="6020">彦根
</optgroup>
<optgroup label="京都">
<option value="400">舞鶴
<option value="6100">京都
</optgroup>
<optgroup label="大阪">
<option value="6200">大阪
</optgroup>
<optgroup label="兵庫">
<option value="6310">神戸
<option value="6320">豊岡
</optgroup>
<optgroup label="奈良">
<option value="6410">奈良
<option value="6420">風屋
</optgroup>
<optgroup label="和歌山">
<option value="6510">和歌山
<option value="6520">潮岬
</optgroup>
<optgroup label="岡山">
<option value="6610">岡山
<option value="6620">津山
</optgroup>
<optgroup label="広島">
<option value="6710">広島
<option value="6720">庄原
</optgroup>
<optgroup label="島根">
<option value="6810">松江
<option value="6820">浜田
<option value="6830">西郷
</optgroup>
<optgroup label="鳥取">
<option value="6910">鳥取
<option value="6920">米子
</optgroup>
<optgroup label="山口">
<option value="8110">下関
<option value="8120">山口
<option value="8130">柳井
<option value="8140">萩
</optgroup>
<optgroup label="徳島">
<option value="7110">徳島
<option value="7120">日和佐
</optgroup>
<optgroup label="香川">
<option value="7200">高松
</optgroup>
<optgroup label="愛媛">
<option value="7310">松山
<option value="7320">新居浜
<option value="7330">宇和島
</optgroup>
<optgroup label="高知">
<option value="7410">高知
<option value="7420">室戸
<option value="7430">足摺
</optgroup>
<optgroup label="福岡">
<option value="8210">福岡
<option value="8220">八幡
<option value="8230">飯塚
<option value="8240">久留米
</optgroup>
<optgroup label="大分">
<option value="8310">大分
<option value="8320">中津
<option value="8330">日田
<option value="8340">佐伯
</optgroup>
<optgroup label="長崎">
<option value="700">厳原
<option value="800">福江
<option value="8410">長崎
<option value="8420">佐世保
</optgroup>
<optgroup label="佐賀">
<option value="8510">佐賀
<option value="8520">伊万里
</optgroup>
<optgroup label="熊本">
<option value="8610">熊本
<option value="8620">阿蘇乙姫
<option value="8630">牛深
<option value="8640">人吉
</optgroup>
<optgroup label="宮崎">
<option value="8710">宮崎
<option value="8720">延岡
<option value="8730">都城
<option value="8740">高千穂
</optgroup>
<optgroup label="鹿児島">
<option value="1000">名瀬
<option value="8810">鹿児島
<option value="8820">鹿屋
<option value="8830">西之表
</optgroup>
<optgroup label="沖縄本島">
<option value="9110">那覇
<option value="9120">名護
</optgroup>
<optgroup label="宮古・石垣">
<option value="9300">宮古島
<option value="9400">石垣島
<option value="9500">与那国島
</optgroup>
<optgroup label="久米島">
<option value="9130">久米島
</optgroup>
<optgroup label="南大東島">
<option value="9200">南大東島
</optgroup>
</select>
</font></td>
</tr>
</table>
<table width="800" border="0" cellspacing="0" cellpadding="2">
<tr height="30">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_new_days_options($selected) {
	echo("<option value=\"1\"");
	if ($selected == "1") {echo(" selected");}
	echo(">1日間（当日分のみ）\n");

	for ($i = 2; $i <= 31; $i++) {
		echo("<option value=\"$i\"");
		if ($selected == $i) {echo(" selected");}
		echo(">{$i}日間\n");
	}
}
?>
