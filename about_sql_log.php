<?php
//**********このファイルは実行されたSQLについてのログ処理するものです**********

//*****実行されたSQLの書き込み*****
function write_sql_log($SQLLOG,$fname,$SQL,$SQLLOGFILE,$ERRORLOG){				   								//引数としてエラー番号を受け取る　エラー番号はerror.infに記述
	if (is_file("./log/atmodule_sql.log")){
		if (filesize("./log/atmodule_sql.log") < 1024*1024) { // 1Mbまで
			require_once("./about_file.php");
			$open = @open_file("./log/atmodule_sql.log","a");
			$write = @write_into_file($open, "[".$fname."][".date("Y/m/d H:i:s")."] ".$SQL."\n\n");
			$close = @close_file($open);
		}
	}

	if($SQLLOG == "on"){								  										//エラーログの書き込み有無チェック
		require_once("./about_file.php");
		$sql_date = date("D M j G:i:s T Y");                								// 例　Sat Mar 10 15:16:08 MST 2001
		$SQL = $fname.":".$SQL;
		$SQL .= "--->$sql_date\n";
		$arw = "a";																				//追加形式でファイルを開く
		$exist = check_file($SQLLOGFILE);												//logファイルの存在を確認する
		$open = open_file($SQLLOGFILE,$arw);
		$write = write_into_file($open,$SQL);
		$close = close_file($open);
			if($write == "0"){
				require("./conf/error.inf");													//エラーメッセージ一覧ファイルを読み込む
				require_once("./about_error.php");										//エラーメッセージの書き込み処理をする関数のファイルを読み込む
				echo("<script language='javascript'>alert(\"$ERROR002\");</script>");
				echo("<script language='javascript'>alert(\"$ERRORLOG\");</script>");
				write_error($fname,$ERRORLOG, $ERROR12);
				return 0;
			}else{
				return 1;
			}
	}
	return 0;
}
?>