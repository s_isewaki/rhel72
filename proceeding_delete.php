<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

// セッションのチェック
$session = qualify_session($session, $fname);
if($session == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 委員会・WG権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 議事録情報を削除
$sql = "delete from proceeding";
$cond = "where pjt_schd_id = '$pjt_schd_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 添付ファイル情報を削除
$sql = "delete from prcdfile";
$cond = "where pjt_schd_id = '$pjt_schd_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 承認情報を削除
$sql = "delete from prcdaprv";
$cond = "where pjt_schd_id = '$pjt_schd_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}


// 承認情報(設定情報)を削除
$sql = "delete from prcdaprv_mng";
$cond = "where pjt_schd_id = '$pjt_schd_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");
//pg_query($con, "rollback");

// データベース接続を閉じる
pg_close($con);

// 添付ファイルの削除
foreach (glob("proceeding/{$pjt_schd_id}_*.*") as $tmpfile) {
	unlink($tmpfile);
}
foreach (glob("proceeding/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}

// 30分以上前に保存された一時ファイルを削除
foreach (glob("proceeding/tmp/*.*") as $tmpfile) {
	if (time() - filemtime($tmpfile) >= 30 * 60) {
		unlink($tmpfile);
	}
}

// 親画面をリフレッシュし、自画面を閉じる
echo("<script type=\"text/javascript\">opener.location.href = '$pnt_url'; self.close();</script>");
?>
