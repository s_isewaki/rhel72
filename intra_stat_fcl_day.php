<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("show_date_navigation_month.ini");
require("menu_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 56, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 稼動状況統計設定権限の取得
$stat_setting_auth = check_authority($session, 52, $fname);

// データベースに接続
$con = connect2db($fname);

// 施設名を取得
$sql = "select name from statfcl";
$cond = "where statfcl_id = $fcl_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$fcl_name = pg_fetch_result($sel, 0, "name");

// 管理項目一覧を取得
$sql = "select statitem_id, name, total_flg, total_start_month, total_start_day from statitem";
$cond = "where del_flg = 'f' order by statitem_id";
$sel_item = select_from_table($con, $sql, $cond, $fname);
if ($sel_item == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$items = array();
while ($row = pg_fetch_array($sel_item)) {
	$items[$row["statitem_id"]] = array(
		"name" => $row["name"],
		"total_flg" => $row["total_flg"],
		"total_start_month" => $row["total_start_month"],
		"total_start_day" => $row["total_start_day"],
		"assists" => array()
	);
}

// 補助項目一覧を取得
$sql = "select statassist_id, statassist_name, statitem_id, assist_type, calc_var1_type, calc_var1_const_id, calc_var1_item_id, calc_var1_assist_id, calc_operator, calc_var2_type, calc_var2_const_id, calc_var2_item_id, calc_var2_assist_id, calc_format from statassist";
$cond = "where del_flg = 'f' order by statassist_id";
$sel_assist = select_from_table($con, $sql, $cond, $fname);
if ($sel_assist == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel_assist)) {
	$items[$row["statitem_id"]]["assists"][$row["statassist_id"]] = array(
		"statassist_name" => $row["statassist_name"],
		"assist_type" => $row["assist_type"],
		"var1_type" => $row["calc_var1_type"],
		"var1_const_id" => $row["calc_var1_const_id"],
		"var1_item_id" => $row["calc_var1_item_id"],
		"var1_assist_id" => $row["calc_var1_assist_id"],
		"operator" => $row["calc_operator"],
		"var2_type" => $row["calc_var2_type"],
		"var2_const_id" => $row["calc_var2_const_id"],
		"var2_item_id" => $row["calc_var2_item_id"],
		"var2_assist_id" => $row["calc_var2_assist_id"],
		"format" => $row["calc_format"]
	);
}

// 定数一覧を取得
$sql = "select statconst_id, statconst_value from statconst";
$cond = "where del_flg = 'f' order by statconst_id";
$sel_const = select_from_table($con, $sql, $cond, $fname);
if ($sel_const == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$consts = array();
while ($row = pg_fetch_array($sel_const)) {
	$consts[$row["statconst_id"]] = $row["statconst_value"];
}

// 当月の全統計値を取得
$selected_ym = date("Ym", $date);
$sql = "select statitem_id, statassist_id, stat_date, stat_val from stat";
$cond = "where stat_date like '{$selected_ym}__' and statfcl_id = $fcl_id";
$sel_stat = select_from_table($con, $sql, $cond, $fname);
if ($sel_stat == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$stats = array();
while ($row = pg_fetch_array($sel_stat)) {
	$tmp_item_id = $row["statitem_id"];
	$tmp_assist_id = $row["statassist_id"];
	$tmp_date = $row["stat_date"];
	$tmp_val = $row["stat_val"];

	if ($tmp_val === "") {
		continue;
	}

	if ($tmp_assist_id == "") {
		$tmp_id = $tmp_item_id;
	} else {
		$tmp_id = $tmp_item_id . "_" . $tmp_assist_id;
	}

	$tmp_day = intval(substr($tmp_date, 6, 2));

	$stats[$tmp_id][$tmp_day] = $tmp_val;
}

// 管理項目をループ
$start_md = date("m", $date) . "01";
$year = date("Y", $date);
$month = date("m", $date);
$days = days_in_month($year, $month);
foreach ($items as $tmp_item_id => $tmp_item) {
	$tmp_total_flg = $tmp_item["total_flg"];
	$tmp_total_start_month = $tmp_item["total_start_month"];
	$tmp_total_start_day = $tmp_item["total_start_day"];
	$tmp_assists = $tmp_item["assists"];

	// 補助項目が存在しない場合
	if (count($tmp_assists) == 0) {

		// 延べ設定でない場合、次の管理項目へ
		if ($tmp_total_flg == "f") {
			continue;
		}

		// 過去の統計値を取得
		$sql = "select stat_date, stat_val from stat";
		$tmp_total_start_md = sprintf("%02d%02d", $tmp_total_start_month, $tmp_total_start_day);
		if ($start_md < $tmp_total_start_md) {
			$tmp_total_start_y = date("Y", $date) - 1;
		} else {
			$tmp_total_start_y = date("Y", $date);
		}
		$cond = "where stat_date >= '$tmp_total_start_y$tmp_total_start_md' and stat_date < '{$selected_ym}01' and statitem_id = $tmp_item_id and statassist_id is null and statfcl_id = $fcl_id";
		$sel_stat = select_from_table($con, $sql, $cond, $fname);
		if ($sel_stat == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 延べ値を保存
		$stats[$tmp_item_id]["total"] = 0;
		while ($row = pg_fetch_array($sel_stat)) {
			$tmp_date = $row["stat_date"];
			$tmp_val = $row["stat_val"];

			if (preg_match("/^-?[0-9.]+$/", $tmp_val)) {
				$stats[$tmp_item_id]["total"] += $tmp_val;
			}
		}

	// 補助項目が存在する場合
	} else {

		// 補助項目をループ
		foreach ($tmp_assists as $tmp_assist_id => $tmp_assist) {
			$tmp_assist_type = $tmp_assist["assist_type"];
			$tmp_var1_type = $tmp_assist["var1_type"];
			$tmp_var1_const_id = $tmp_assist["var1_const_id"];
			$tmp_var1_item_id = $tmp_assist["var1_item_id"];
			$tmp_var1_assist_id = $tmp_assist["var1_assist_id"];
			$tmp_operator = $tmp_assist["operator"];
			$tmp_var2_type = $tmp_assist["var2_type"];
			$tmp_var2_const_id = $tmp_assist["var2_const_id"];
			$tmp_var2_item_id = $tmp_assist["var2_item_id"];
			$tmp_var2_assist_id = $tmp_assist["var2_assist_id"];
			$tmp_format = $tmp_assist["format"];

			// 入力項目の場合、次の補助項目へ
			if ($tmp_assist_type == "1") {
				continue;
			}

			// 統計値を初期化
			$stats["{$tmp_item_id}_{$tmp_assist_id}"] = array();

			// 1日〜月末までループ
			for ($i = 1; $i <= $days; $i++) {

				// 項目1が定数の場合
				if ($tmp_var1_type == "1") {
					$tmp_var1_val = $consts[$tmp_var1_const_id];

				// 項目1が項目指定の場合
				} else {
					if ($tmp_var1_assist_id == "") {
						$tmp_var1_val = $stats[$tmp_var1_item_id][$i];
					} else {
						$tmp_var1_val = $stats["{$tmp_var1_item_id}_{$tmp_var1_assist_id}"][$i];
					}
				}

				// 項目1が空の場合、計算しない
				if ($tmp_var1_val == "") {
					continue;
				}

				// 項目2が定数の場合
				if ($tmp_var2_type == "1") {
					$tmp_var2_val = $consts[$tmp_var2_const_id];

				// 項目2が項目指定の場合
				} else {
					if ($tmp_var2_assist_id == "") {
						$tmp_var2_val = $stats[$tmp_var2_item_id][$i];
					} else {
						$tmp_var2_val = $stats["{$tmp_var2_item_id}_{$tmp_var2_assist_id}"][$i];
					}
				}

				// 項目2が空の場合、計算しない
				if ($tmp_var2_val == "") {
					continue;
				}

				// 四則演算
				$tmp_var1_val = floatval($tmp_var1_val);
				$tmp_var2_val = floatval($tmp_var2_val);
				switch ($tmp_operator) {
				case "1":  // ＋
					$tmp_val = $tmp_var1_val + $tmp_var2_val;
					break;
				case "2":  // −
					$tmp_val = $tmp_var1_val - $tmp_var2_val;
					break;
				case "3":  // ×
					$tmp_val = $tmp_var1_val * $tmp_var2_val;
					break;
				case "4":  // ÷
					if ($tmp_var2_val <> 0) {
						$tmp_val = $tmp_var1_val / $tmp_var2_val;
					} else {
						$tmp_val = null;
					}
					break;
				}

				// 実数表示の場合
				if ($tmp_format == "1") {
					$tmp_val = round($tmp_val, 1);

				// %表示の場合
				} else {
					$tmp_val *= 100;
					$tmp_val = round($tmp_val, 1) . "%";
				}

				// 統計値を保存
				$stats["{$tmp_item_id}_{$tmp_assist_id}"][$i] = $tmp_val;
			}
		}
	}
}

// イントラメニュー情報を取得
$sql = "select menu1 from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu1 = pg_fetch_result($sel, 0, "menu1");

$last_month = get_last_month($date);
$next_month = get_next_month($date);
?>
<title>イントラネット | <? echo($menu1); ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function changeDate(dt) {
	location.href = 'intra_stat_fcl_day.php?session=<? echo($session); ?>&fcl_id=<? echo($fcl_id); ?>&date=' + dt;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_stat.php?session=<? echo($session); ?>"><b><? echo($menu1); ?></b></a></font></td>
<? if ($stat_setting_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="stat_master_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="105" align="center" bgcolor="#bdd1e7"><a href="intra_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メインメニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($menu1)); ?>" align="center" bgcolor="#5279a5"><a href="intra_stat.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><? echo($menu1); ?></b></font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="300"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="intra_stat_fcl_day.php?session=<? echo($session); ?>&date=<? echo($last_month); ?>&fcl_id=<? echo($fcl_id); ?>">&lt;前月</a>&nbsp;<select onchange="changeDate(this.value);"><? show_date_options_m($date); ?></select>&nbsp;<a href="intra_stat_fcl_day.php?session=<? echo($session); ?>&date=<? echo($next_month); ?>&fcl_id=<? echo($fcl_id); ?>">翌月&gt;</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="intra_stat_fcl_month.php?session=<? echo($session); ?>&date=<? echo($date); ?>&fcl_id=<? echo($fcl_id); ?>">月</a><img src="img/spacer.gif" alt="" width="10" height="1"><a href="intra_stat.php?session=<? echo($session); ?>&date=<? echo($date); ?>">日</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="150"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo($fcl_name); ?></b></font></td>
<?
for ($i = 1; $i <= 15; $i++) {
	$tmp_date = mktime(0, 0, 0, $month, $i, $year);
	echo("<td width=\"35\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"intra_stat.php?session=$session&date=$tmp_date\">{$i}(" . get_weekday($tmp_date) . ")</a></font></td>\n");
}
?>
</tr>
<?
$selected_m = date("m", $date);
reset($items);
foreach ($items as $tmp_item_id => $tmp_item) {
	$tmp_item_name = $tmp_item["name"];
	$tmp_total_flg = $tmp_item["total_flg"];
	$tmp_total_start_month = $tmp_item["total_start_month"];
	$tmp_total_start_day = $tmp_item["total_start_day"];
	$tmp_assists = $tmp_item["assists"];

	echo("<tr height=\"22\">\n");
	echo("<td bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_item_name</font></td>\n");
	for ($i = 1; $i <= 15; $i++) {
		echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		if (count($tmp_assists) == 0) {
			echo($stats[$tmp_item_id][$i]);
		} else {
			echo("&nbsp;");
		}
		echo("</font></td>\n");
	}
	echo("</tr>\n");

	if (count($tmp_assists) == 0 && $tmp_total_flg == "t") {
		echo("<tr height=\"22\">\n");
		echo("<td bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_item_name}（延べ）</font></td>\n");
		for ($i = 1; $i <= 15; $i++) {
			if (intval($selected_m) == intval($tmp_total_start_month) && intval($i) == intval($tmp_total_start_day)) {
				$stats[$tmp_item_id]["total"] = 0;
			}

			if (preg_match("/^-?[0-9.]+$/", $stats[$tmp_item_id][$i])) {
				$stats[$tmp_item_id]["total"] += $stats[$tmp_item_id][$i];
			}

			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$stats[$tmp_item_id]["total"]}</font></td>\n");
		}
	}

	$show_sum = false;
	$sums = array();
	foreach ($tmp_assists as $tmp_assist_id => $tmp_assist) {
		$tmp_assist_name = $tmp_assist["statassist_name"];
		$tmp_assist_type = $tmp_assist["assist_type"];

		if (!$show_sum && $tmp_assist_type == "1") {
			$show_sum = true;
		}

		echo("<tr height=\"22\">\n");
		echo("<td bgcolor=\"#f6f9ff\" style=\"padding-left:30px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_assist_name}</font></td>\n");
		for ($i = 1; $i <= 15; $i++) {
			$tmp_val = $stats["{$tmp_item_id}_{$tmp_assist_id}"][$i];

			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_val</font></td>\n");

			if ($show_sum && $tmp_val != "" && substr($tmp_val, -1) != "%") {
				if (!isset($sums[$i])) {
					$sums[$i] = 0;
				}
				$sums[$i] += $tmp_val;
			}
		}
		echo("</tr>\n");
	}

	if (!$show_sum) {
		continue;
	}

	echo("<tr height=\"22\">\n");
	echo("<td bgcolor=\"#f6f9ff\" style=\"padding-left:30px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">計</font></td>\n");
	for ($i = 1; $i <= 15; $i++) {
		echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo($sums[$i]);
		echo("</font></td>\n");
	}
	echo("</tr>\n");
}
?>
<tr>
<td colspan="16" height="5" style="border-style:none;"></td>
</tr>
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<?
for ($i = 16; $i <= $days; $i++) {
	$tmp_date = mktime(0, 0, 0, $month, $i, $year);
	echo("<td width=\"35\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"intra_stat.php?session=$session&date=$tmp_date\">{$i}(" . get_weekday($tmp_date) . ")</a></font></td>\n");
}
?>
</tr>
<?
reset($items);
foreach ($items as $tmp_item_id => $tmp_item) {
	$tmp_item_name = $tmp_item["name"];
	$tmp_total_flg = $tmp_item["total_flg"];
	$tmp_total_start_month = $tmp_item["total_start_month"];
	$tmp_total_start_day = $tmp_item["total_start_day"];
	$tmp_assists = $tmp_item["assists"];

	echo("<tr height=\"22\">\n");
	echo("<td bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_item_name</font></td>\n");
	for ($i = 16; $i <= $days; $i++) {
		echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		if (count($tmp_assists) == 0) {
			echo($stats[$tmp_item_id][$i]);
		} else {
			echo("&nbsp;");
		}
		echo("</font></td>\n");
	}
	echo("</tr>\n");

	if (count($tmp_assists) == 0 && $tmp_total_flg == "t") {
		echo("<tr height=\"22\">\n");
		echo("<td bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_item_name}（延べ）</font></td>\n");
		for ($i = 16; $i <= $days; $i++) {
			if (intval($selected_m) == intval($tmp_total_start_month) && intval($i) == intval($tmp_total_start_day)) {
				$stats[$tmp_item_id]["total"] = 0;
			}

			if (preg_match("/^-?[0-9.]+$/", $stats[$tmp_item_id][$i])) {
				$stats[$tmp_item_id]["total"] += $stats[$tmp_item_id][$i];
			}

			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$stats[$tmp_item_id]["total"]}</font></td>\n");
		}
	}

	$show_sum = false;
	$sums = array();
	foreach ($tmp_assists as $tmp_assist_id => $tmp_assist) {
		$tmp_assist_name = $tmp_assist["statassist_name"];
		$tmp_assist_type = $tmp_assist["assist_type"];

		if (!$show_sum && $tmp_assist_type == "1") {
			$show_sum = true;
		}

		echo("<tr height=\"22\">\n");
		echo("<td bgcolor=\"#f6f9ff\" style=\"padding-left:30px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_assist_name}</font></td>\n");
		for ($i = 16; $i <= $days; $i++) {
			$tmp_val = $stats["{$tmp_item_id}_{$tmp_assist_id}"][$i];

			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_val</font></td>\n");

			if ($show_sum && $tmp_val != "" && substr($tmp_val, -1) != "%") {
				if (!isset($sums[$i])) {
					$sums[$i] = 0;
				}
				$sums[$i] += $tmp_val;
			}
		}
		echo("</tr>\n");
	}

	if (!$show_sum) {
		continue;
	}

	echo("<tr height=\"22\">\n");
	echo("<td bgcolor=\"#f6f9ff\" style=\"padding-left:30px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">計</font></td>\n");
	for ($i = 16; $i <= $days; $i++) {
		echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo($sums[$i]);
		echo("</font></td>\n");
	}
	echo("</tr>\n");
}
?>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function days_in_month($year, $month) {
	for ($day = 31; $day >= 28; $day--) {
		if (checkdate($month, $day, $year)) {
			return $day;
		}
	}
	return false;
}

function get_weekday($date) {
	$wd = date("w", $date);
	switch ($wd) {
	case "0":
		return "日";
	case "1":
		return "月";
	case "2":
		return "火";
	case "3":
		return "水";
	case "4":
		return "木";
	case "5":
		return "金";
	case "6":
		return "土";
	}
}
?>
