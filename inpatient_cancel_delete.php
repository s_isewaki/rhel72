<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$check_auth = check_authority($session, 14, $fname);
if ($check_auth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin transaction");

// チェックされた患者をループ
if (is_array($del_pt)) {
	foreach ($del_pt as $inpt_res_ccl_id) {

		// 入院予定キャンセル情報の削除
		$sql = "delete from inptcancel";
		$cond = "where inpt_res_ccl_id = '$inpt_res_ccl_id'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		// 担当者情報の削除
		$sql = "delete from inptopcancel";
		$cond = "where inpt_res_ccl_id = '$inpt_res_ccl_id'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
}


// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// キャンセル患者一覧画面を再表示
echo("<script language=\"javascript\">location.href = 'inpatient_cancel_list.php?session=$session&initial=$initial&bldgwd=$bldgwd&list_sect_id=$list_sect_id&page=$page'</script>");
?>
