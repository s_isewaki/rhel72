<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | 公開スケジュール（週）</title>
<?
$fname = $PHP_SELF;

require("about_session.php");
require("about_authority.php");
require("time_check2.ini");
require("project_check2.ini");
require("show_date_navigation_week.ini");
require("holiday.php");
require("show_schedule_common.ini");
require("show_other_schedule_chart.ini");
require("get_values.ini");
require("label_by_profile_type.ini");
require("show_calendar_memo.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// タスク権限を取得
$task = check_authority($session, 30, $fname);
$task_flg = ($task != "0");

// 日付の設定
if ($date == "") {
    $date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}
$day = date("d", $date);
$month = date("m", $date);
$year = date("Y", $date);

// 「前月」「前週」「翌週」「翌月」のタイムスタンプを取得
$last_month = get_last_month($date);
$last_week = get_last_week($date);
$next_week = get_next_week($date);
$next_month = get_next_month($date);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$e_id = pg_fetch_result($sel, 0, "emp_id");

// 院内行事チェックボックスの処理
if ($timegd == "on" || $timegd == "off") {
    time_update($con, $e_id, $timegd, $fname);
} else if ($timegd == "") {
    $timegd = time_check($con, $e_id, $fname);
}

// 委員会・WGチェックボックスの処理
if ($pjtgd == "on" || $pjtgd == "off") {
    project_update($con, $e_id, $pjtgd, $fname);
} else if ($pjtgd == "") {
    $pjtgd = project_check($con, $emp_id, $e_id, $fname);
}

// 表示対象週のスタート日のタイムスタンプを取得
$start_date = get_start_date_timestamp($con, $date, $e_id, $fname);

// 表示対象週の7日分の日付を配列で取得
$dates = get_dates_in_week($start_date);

// スケジュール更新可能フラグを取得
$updatable_flg = get_updatable_flg($con, $emp_id, $e_id, $fname);

// 時間指定のないスケジュールの情報を配列で取得
$timeless_schedules = get_timeless_schedules($con, $emp_id, $pjtgd, $timegd, $dates, $fname, $e_id);

// 時間指定のあるスケジュール情報を配列で取得
$normal_schedules = get_normal_schedules($con, $emp_id, $e_id, $pjtgd, $timegd, $dates, $fname, $e_id);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">
function showGuide() {
    var timegd = (document.timegd.timeguide.checked) ? 'on' : 'off';
    location.href = 'schedule_other_week_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>&timegd=' + timegd;
}

function showProject() {
    var pjtgd = (document.timegd.pjtguide.checked) ? 'on' : 'off';
    location.href = 'schedule_other_week_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>&pjtgd=' + pjtgd;
}

function openPrint() {
    window.open('schedule_print_other_week_menu.php?&session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>', 'newwin2', 'width=800,height=600,scrollbars=yes');
}

function changeDate(dt) {
    location.href = 'schedule_other_week_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=' + dt;
}

function popupScheduleDetail(title, place, date, time, type, detail, e) {
    popupDetailBlue(
        new Array(
            'タイトル', title,
            '行先', place,
            '日付', date,
            '時刻', time,
            '種別', type,
            '詳細', detail
        ), 400, 80, e
    );
}

function popupEventDetail(fcl_name, evt_date, evt_time, evt_name, evt_content, evt_contact, e) {
    popupDetailBlue(
        new Array(
            '施設', fcl_name,
            '日付', evt_date,
            '時刻', evt_time,
            '行事名', evt_name,
            '詳細', evt_content,
            '連絡先', evt_contact
        ), 400, 80, e
    );
}

function highlightCells(class_name) {
    changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name) {
    changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color) {
    var cells = document.getElementsByTagName('td');
    for (var i = 0, j = cells.length; i < j; i++) {
        if (cells[i].className != class_name) {
            continue;
        }
        cells[i].style.backgroundColor = color;
    }
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>公開スケジュール（週）</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="timegd">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#5279a5"><a href="schedule_other_week_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>週</b></font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_month_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_year_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($year); ?>&s_date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_list_upcoming.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<? if ($task_flg) { ?>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_other_task.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公開タスク</font></a></td>
<? } ?>
<td width="43"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo(get_emp_kanji_name($con,$emp_id,$fname)); ?></b></font></td>
<td align="right"><input type="button" value="印刷" onclick="openPrint();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td height="22" width="60%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="schedule_other_week_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($last_month); ?>">&lt;&lt;前月</a>&nbsp;<a href="schedule_other_week_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($last_week); ?>">&lt;前週</a>&nbsp;<select onchange="changeDate(this.options[this.selectedIndex].value);"><? show_date_options_w($date, $start_date); ?></select>&nbsp;<a href="schedule_other_week_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($next_week); ?>">翌週&gt;</a>&nbsp;<a href="schedule_other_week_menu.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&date=<? echo($next_month); ?>">翌月&gt;&gt;</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($schd_type_name); ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 院内行事/所内行事
echo ($_label_by_profile["EVENT"][$profile_type]); ?></font><input type="checkbox" name="timeguide" onclick="showGuide();"<? if ($timegd == "on") {echo(" checked");} ?>>&nbsp;<a href="javascript:void(0);" onclick="window.open('schedule_project_setting2.php?session=<? echo($session); ?>&target_emp_id=<? echo($emp_id); ?>', 'proreg', 'width=640,height=700,scrollbars=yes');"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG</font></a><input type="checkbox" name="pjtguide" onclick="showProject();"<? if ($pjtgd == "on") {echo(" checked");} ?>></td>
</tr>
</table>
<?
echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">\n");

$start_ymd = date("Ymd", $start_date);
$end_ymd = date("Ymd", strtotime("+6 days", $start_date));
// カレンダーのメモを取得
$arr_calendar_memo = get_calendar_memo($con, $fname, $start_ymd, $end_ymd);

// 日付行を表示
show_dates($con, $dates, $emp_id, $e_id, $normal_schedules, $session, $fname, $arr_calendar_memo);

// 時間指定なし行を表示
show_timeless_schedules($timeless_schedules, $normal_schedules, $updatable_flg, $session, false);

// 時間指定あり行を表示
show_normal_schedules($timeless_schedules, $normal_schedules, $emp_id, $updatable_flg, $session, false);

echo("</table>\n");
?>
</form>
<? /* 凡例非表示
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width=""><? show_schd_type_color_list($con, $fname); ?></td>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
*/ ?>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// 表示対象週のスタート日のタイムスタンプを取得
function get_start_date_timestamp($con, $date, $emp_id, $fname) {
    $sql = "select calendar_start1 from option";
    $cond = "where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $calendar_start = pg_fetch_result($sel, 0, "calendar_start1");

    $start_wd = ($calendar_start == 1) ? 0 : 1;  // 0：日曜、1：月曜

    return get_start_day($date, $start_wd);
}

// 表示対象週の7日分の日付を配列で取得
function get_dates_in_week($start_date) {
    $ret = array();
    for ($i = 0; $i < 7; $i++) {
        $ret[] = date("Ymd", strtotime("+$i days", $start_date));
    }
    return $ret;
}

// 日付行を表示
function show_dates($con, $dates, $emp_id, $e_id, $normal_schedules, $session, $fname, $arr_calendar_memo) {
    $today = date("Ymd");

    echo("<tr height=\"22\">\n");
    echo("<td>&nbsp;</td>\n");
    foreach ($dates as $date) {
        $colspan = get_overlap_schedule_count($normal_schedules[$date]);
        $timestamp = to_timestamp($date);
        $month = date("m", $timestamp);
        $day = date("d", $timestamp);
        $weekday_info = get_weekday_info($timestamp);
        $holiday = ktHolidayName($timestamp);
        if ($date == $today) {
            $bgcolor = "#ccffcc";
        } elseif ($holiday != "") {
            $bgcolor = "#fadede";
        } else if ($arr_calendar_memo["{$date}_type"] == "5") {
            $bgcolor = "#defafa";
        } else {
            $bgcolor = $weekday_info["color"];
        }
        $schd_type_name = get_schd_name($con, $emp_id, $e_id, $date, $fname);

        echo("<td width=\"14%\" align=\"center\" valign=\"top\" colspan=\"$colspan\" bgcolor=\"$bgcolor\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\"><b><a href=\"schedule_other_menu.php?session=$session&emp_id=$emp_id&date=$timestamp\">{$month}/{$day}（{$weekday_info["label"]}）</a></b><br>");
        // カレンダーのメモがある場合は設定する
        if ($arr_calendar_memo["$date"] != "") {
            if ($holiday == "") {
                $holiday = $arr_calendar_memo["$date"];
            } else {
                $holiday .= "<br>".$arr_calendar_memo["$date"];
            }
        }
        if ($holiday != "") {
            echo("<font color=\"red\" class=\"j12\">$holiday</font>&nbsp;");
        }
        if ($schd_type_name != "") {
            echo("<font class=\"j12\">$schd_type_name</font><br>");
        }
        echo("</font></td>\n");
    }
    echo("</tr>\n");
}

// タイムスタンプから曜日情報を取得
function get_weekday_info($date) {
    $wd = date("w", $date);
    switch ($wd) {
    case "0":
        return array("label" => "日", "color" => "#fadede");
        break;
    case "1":
        return array("label" => "月", "color" => "#fefcdf");
        break;
    case "2":
        return array("label" => "火", "color" => "#fefcdf");
        break;
    case "3":
        return array("label" => "水", "color" => "#fefcdf");
        break;
    case "4":
        return array("label" => "木", "color" => "#fefcdf");
        break;
    case "5":
        return array("label" => "金", "color" => "#fefcdf");
        break;
    case "6":
        return array("label" => "土", "color" => "#defafa");
        break;
    }
}
?>
