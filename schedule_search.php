<?php
$fname = $_SERVER["PHP_SELF"];

require_once("about_comedix.php");
require_once("show_class_name.ini");
require_once("show_select_values.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// スケジュール管理権限を取得
$schd_place_auth = check_authority($session, 13, $fname);

// 日付の設定
if ($date == "") {
    $date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}
$year = date("Y", $date);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "SELECT emp_id FROM session WHERE session_id='{$session}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// デフォルト画面情報を取得
$sql = "select schedule1_default, schedule2_default from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$schedule1_default = pg_fetch_result($sel, 0, "schedule1_default");
$schedule2_default = pg_fetch_result($sel, 0, "schedule2_default");

// デフォルトのURLを設定
switch ($schedule1_default) {
case "1":
    $top_url = "schedule_menu.php";
    break;
case "3":
    $top_url = "schedule_month_menu.php";
    break;
default:
    $top_url = "schedule_week_menu.php";
    break;
}
switch ($schedule2_default) {
case "1":
    $other_url = "schedule_other_menu.php";
    break;
case "3":
    $other_url = "schedule_other_month_menu.php";
    break;
default:
    $other_url = "schedule_other_week_menu.php";
    break;
}

// 検索ボタンが押された場合
if ($cls != "") {

    // 検索条件に合致する職員一覧を取得
    $sql = "select e.emp_id, e.emp_lt_nm, e.emp_ft_nm from empmst e";
    $cond = "where exists (select * from authmst a where a.emp_id = e.emp_id and a.emp_del_flg = 'f')";
    if ($cls != 0) {
        $cond .= " and ((e.emp_class = $cls) or exists (select * from concurrent c where c.emp_id = e.emp_id and c.emp_class = $cls))";
    }
    if ($atrb != 0) {
        $cond .= " and ((e.emp_attribute = $atrb) or exists (select * from concurrent c where c.emp_id = e.emp_id and c.emp_attribute = $atrb))";
    }
    if ($dept != 0) {
        $cond .= " and ((e.emp_dept = $dept) or exists (select * from concurrent c where c.emp_id = e.emp_id and c.emp_dept = $dept))";
    }
    if ($emp_nm != "") {
        $full_nm = str_replace("　", "", str_replace(" ", "", $emp_nm));
        $cond .= " and (e.emp_lt_nm like '%$full_nm%' or e.emp_ft_nm like '%$full_nm%' or e.emp_kn_lt_nm like '%$full_nm%' or e.emp_kn_ft_nm like '%$full_nm%' or (e.emp_lt_nm || e.emp_ft_nm) like '%$full_nm%' or (e.emp_kn_lt_nm || e.emp_kn_ft_nm) like '%$full_nm%')";
    }
    if ($pjt_id != 0) {
        $cond .= " and (exists (select * from project p where p.pjt_id = '$pjt_id' and p.pjt_response = e.emp_id) or exists (select * from promember pm where pm.pjt_id = '$pjt_id' and pm.emp_id = e.emp_id))";
    }
    if ($st_id != 0) {
        $cond .= " and ((e.emp_st = $st_id) or exists (select * from concurrent c where c.emp_id = e.emp_id and c.emp_st = $st_id))";
    }
    if ($job_id != 0) {
        $cond .= " and e.emp_job = $job_id";
    }
    $cond .= " order by e.emp_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $employees = array();
    while ($row = pg_fetch_array($sel)) {
        $employees[$row["emp_id"]] = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];
    }
}

// 組織第1階層目の一覧を取得
$sql = "select class_id, class_nm from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$classes = array();
while ($row = pg_fetch_array($sel)) {
    $classes[$row["class_id"]] = $row["class_nm"];
}

// 組織第2階層目の一覧を取得
$sql = "select class_id, atrb_id, atrb_nm from atrbmst";
$cond = "where atrb_del_flg = 'f' order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$attributes = array();
while ($row = pg_fetch_array($sel)) {
    $attributes[$row["atrb_id"]] = array(
        "class_id" => $row["class_id"],
        "atrb_nm" => $row["atrb_nm"]
    );
}

// 組織第3階層目の一覧を取得
$sql = "select atrb_id, dept_id, dept_nm from deptmst";
$cond = "where dept_del_flg = 'f' order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$departments = array();
while ($row = pg_fetch_array($sel)) {
    $departments[$row["dept_id"]] = array(
        "atrb_id" => $row["atrb_id"],
        "dept_nm" => $row["dept_nm"]
    );
}

// 組織名情報を取得
$arr_class_name = get_class_name_array($con, $fname);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | 公開スケジュール検索</title>
<script type="text/javascript">
function onChangeClass() {
    deleteAllOptions(document.search.atrb);
    addOption(document.search.atrb, '0', 'すべて');
    var cls = document.search.cls.value;
<? foreach ($attributes as $tmp_atrb_id => $tmp_attribute) { ?>
    if (cls == '<?php eh($tmp_attribute["class_id"]); ?>') {
        addOption(document.search.atrb, '<?php eh($tmp_atrb_id); ?>', '<?php eh($tmp_attribute["atrb_nm"]); ?>');
    }
<? } ?>
    onChangeAttribute();
}

function onChangeAttribute() {
    deleteAllOptions(document.search.dept);
    addOption(document.search.dept, '0', 'すべて');
    var atrb = document.search.atrb.value;
<? foreach ($departments as $tmp_dept_id => $tmp_department) { ?>
    if (atrb == '<?php eh($tmp_department["atrb_id"]); ?>') {
        addOption(document.search.dept, '<?php eh($tmp_dept_id); ?>', '<?php eh($tmp_department["dept_nm"]); ?>');
    }
<? } ?>
}

function deleteAllOptions(box) {
    for (var i = box.length - 1; i >= 0; i--) {
        box.options[i] = null;
    }
}

function addOption(box, value, text) {
    var opt = document.createElement('option');
    opt.value = value;
    opt.text = text;
    box.options[box.length] = opt;
    try {box.style.fontSize = 'auto';} catch (e) {}
    box.style.overflow = 'auto';
}

function checkAll() {
    var elements = document.list.elements;
    for (var i = 0, j = elements.length; i < j; i++) {
        if (elements[i].type == 'checkbox') {
            elements[i].checked = true;
        }
    }
}

function showSchedule() {
    var selectedEmployees = new Array();
    var elements = document.list.elements;
    for (var i = 0, j = elements.length; i < j; i++) {
        if (elements[i].type == 'checkbox' && elements[i].checked) {
            selectedEmployees.push(elements[i].value);
        }
    }

    if (selectedEmployees.length == 0) {
        alert('職員が選択されていません。');
        return;
    }

    var url = 'schedule_other_emps_week.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>&mode=child';
    for (var i = 0, j = selectedEmployees.length; i < j; i++) {
        url += '&emp_id[]=' + selectedEmployees[i];
    }

    window.open(url, 'other2', 'width=800,height=700,scrollbars=yes');
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php eh($top_url); ?>?session=<?php eh($session); ?>"><img src="img/icon/b02.gif" width="32" height="32" border="0" alt="スケジュール"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php eh($top_url); ?>?session=<?php eh($session); ?>"><b>スケジュール</b></a></font></td>
<? if ($schd_place_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="schedule_place_list.php?session=<?php eh($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #5279a5; margin-bottom:5px;">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_month_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_year_menu.php?session=<?php eh($session); ?>&year=<?php eh($year); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_list_upcoming.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5"></td>
<td width="160" align="center" bgcolor="#5279a5"><a href="schedule_search.php?session=<?php eh("$session"); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>公開スケジュール検索</b></font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_mygroup.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイグループ</font></a></td>
<td width="5"></td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_ical.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カレンダー連携</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_option.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td>&nbsp;</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="320" valign="top">
<form method="post" name="search" action="schedule_pre_search.php" target="schdsrch2">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22" bgcolor="#f6f9ff">
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員名：</font></td>
<td><input type="text" name="emp_nm" maxlength="200" size="25" value="<?php eh($emp_nm); ?>" style="ime-mode:active;"></td>
</tr>
<tr bgcolor="#f6f9ff">
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">属性：</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="cls" onchange="onChangeClass();">
<option value="0">すべて
<?
foreach ($classes as $tmp_class_id => $tmp_class_nm) {
    echo("<option value=\"$tmp_class_id\"");
    if ($tmp_class_id == $cls) {
        echo(" selected");
    }
    echo(">$tmp_class_nm");
}
?>
</select><?php eh($arr_class_name[0]); ?><br>
<select name="atrb" onchange="onChangeAttribute();">
<option value="0">すべて
<?
foreach ($attributes as $tmp_atrb_id => $tmp_attribute) {
    if ($tmp_attribute["class_id"] != $cls) {
        continue;
    }

    echo("<option value=\"$tmp_atrb_id\"");
    if ($tmp_atrb_id == $atrb) {
        echo(" selected");
    }
    echo(">{$tmp_attribute["atrb_nm"]}");
}
?>
</select><?php eh($arr_class_name[1]); ?><br>
<select name="dept">
<option value="0">すべて
<?
foreach ($departments as $tmp_dept_id => $tmp_department) {
    if ($tmp_department["atrb_id"] != $atrb) {
        continue;
    }

    echo("<option value=\"$tmp_dept_id\"");
    if ($tmp_dept_id == $dept) {
        echo(" selected");
    }
    echo(">{$tmp_department["dept_nm"]}");
}
?>
</select><?php eh($arr_class_name[2]); ?>
</font></td>
</tr>
<tr height="22" bgcolor="#f6f9ff">
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職：</font></td>
<td><select name="st_id">
<option value="0">すべて
<? show_select_status($con, $st_id, $fname); ?>
</select></td>
</tr>
<tr height="22" bgcolor="#f6f9ff">
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種：</font></td>
<td><select name="job_id">
<option value="0">すべて
<? show_select_job($con, $job_id, $fname); ?>
</select></td>
</tr>
<tr height="22" bgcolor="#f6f9ff">
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG：</font></td>
<td><select name="pjt_id">
<option value="0">すべて
<? show_project($con, $pjt_id, $fname); ?>
</select></td>
</tr>
<tr height="22" bgcolor="#f6f9ff">
<td colspan="2" align="center">
<input type="submit" value="検索">
<input type="button" value="全員チェック"<? if (count($employees) == 0) {echo(" disabled");} ?> onclick="checkAll();">
<input type="button" value="表示"<? if (count($employees) == 0) {echo(" disabled");} ?> onclick="showSchedule();">
</td>
</tr>
</table>
<input type="hidden" name="session" value="<?php eh($session); ?>">
<input type="hidden" name="refresh" value="true">
</form>
<iframe name="schdsrch2" width="0" height="0" frameborder="0"></iframe>
</td>
<td width="5"><img src="img/spacer.gif" alt="" width="5" height="1"></td>
<td valign="top">
<form name="list">
<? if ($cls != "") { ?>
<table border="0" cellspacing="0" cellpadding="2">
<? foreach ($employees as $tmp_emp_id => $tmp_emp_nm) { ?>
<tr height="22">
<td align="center"><input type="checkbox" value="<?php eh($tmp_emp_id); ?>"></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('<?php eh($other_url); ?>?session=<?php eh($session); ?>&date=<?php eh($date); ?>&emp_id=<?php eh($tmp_emp_id); ?>', 'other', 'width=800,height=700,scrollbars=yes');"><?php eh($tmp_emp_nm); ?></a></font></td>
</tr>
<? } ?>
</table>
<? } ?>
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>
<? pg_close($con); ?>
</body>
</html>
