<?
// <!-- ************************************************************************ -->
// <!-- 勤務シフト作成｜管理 |勤務シフトグループ登録「登録」 -->
// <!-- ************************************************************************ -->
require_once("about_session.php");
require_once("about_authority.php");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;

///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script type='text/javascript'>showLoginPage(window);</script>");
    exit;
}

//エラーチェック
if (empty($group_name)) {
    echo("<script type=\"text/javascript\">alert('シフトグループ（病棟）名を入力してください。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}
if (empty($dr_emp_id_1)) {
    echo("<script type=\"text/javascript\">alert('シフト管理者（責任者）を入力してください。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);

///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");

///******************************************************************************
// 勤務シフトグループＤＢ
///******************************************************************************
///-----------------------------------------------------------------------------
// ＫＥＹ指定されてなければ作成、あれば更新
///-----------------------------------------------------------------------------
if ($group_id == "") {
    ///-----------------------------------------------------------------------------
    // ＫＥＹの採番
    ///-----------------------------------------------------------------------------
    $obj = new duty_shift_common_class($con, $fname);
    $group_id = $obj->get_new_duty_shift_group_id($con, $fname);
    ///-----------------------------------------------------------------------------
    // 登録
    ///-----------------------------------------------------------------------------
    $sql = "insert into duty_shift_group ("
         . "group_id,"
         . "group_name,"
         . "standard_id,"
         . "person_charge_id,"
         . "caretaker_id,"
         . "caretaker2_id,"
         . "caretaker3_id,"
         . "caretaker4_id,"
         . "caretaker5_id,"
         . "caretaker6_id,"
         . "caretaker7_id,"
         . "caretaker8_id,"
         . "caretaker9_id,"
         . "caretaker10_id,"
         . "pattern_id,"
         . "sfc_group_code,"
         . "timecard_auto_flg,"
         . "hosp_patient_cnt"
         . ") values (";
    $content = array(
            $group_id,
            pg_escape_string($group_name),
            $standard_id,
            $dr_emp_id_1,
            $dr_emp_id_2,
            $dr_emp_id_3,
            $dr_emp_id_4,
            $dr_emp_id_5,
            $dr_emp_id_6,
            $dr_emp_id_7,
            $dr_emp_id_8,
            $dr_emp_id_9,
            $dr_emp_id_10,
            $dr_emp_id_11,
            $pattern_id,
            $sfc_group_code,
            $timecard_auto_flg,
            $hosp_patient_cnt
    );
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script type='text/javascript'>showErrorPage(window);</script>");
        exit;
    }
} else {
    ///-----------------------------------------------------------------------------
    // 更新
    ///-----------------------------------------------------------------------------
    $sql = "update duty_shift_group set";
    $set = array(
            "group_name",
            "standard_id",
            "person_charge_id",
            "caretaker_id",
            "caretaker2_id",
            "caretaker3_id",
            "caretaker4_id",
            "caretaker5_id",
            "caretaker6_id",
            "caretaker7_id",
            "caretaker8_id",
            "caretaker9_id",
            "caretaker10_id",
            "pattern_id",
            "sfc_group_code",
            "timecard_auto_flg",
            "hosp_patient_cnt"
    );
    $setvalue = array(
            pg_escape_string($group_name),
            $standard_id,
            $dr_emp_id_1,
            $dr_emp_id_2,
            $dr_emp_id_3,
            $dr_emp_id_4,
            $dr_emp_id_5,
            $dr_emp_id_6,
            $dr_emp_id_7,
            $dr_emp_id_8,
            $dr_emp_id_9,
            $dr_emp_id_10,
            $dr_emp_id_11,
            $pattern_id,
            $sfc_group_code,
            $timecard_auto_flg,
            $hosp_patient_cnt
    );
    $cond = "where group_id = '$group_id'";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script type='text/javascript'>showErrorPage(window);</script>");
        exit;
    }
}

///******************************************************************************
// 出勤パターンが変更された場合、対象シフトグループ(病棟)の職員と下書きデータを削除
///******************************************************************************
if (($pattern_id != "") &&
    ($cause_pattern_id != "") &&
    ($pattern_id != $cause_pattern_id)) {
    ///-----------------------------------------------------------------------------
    // 指定シフトグループ（病棟）のスタッフ情報
    ///-----------------------------------------------------------------------------
    $sql = "delete from duty_shift_staff";
    $cond = "where group_id = '$group_id'";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    ///-----------------------------------------------------------------------------
    //指定シフトグループ（病棟）のシフト情報
    ///-----------------------------------------------------------------------------
    $sql = "delete from duty_shift_plan_draft";
    $cond = "where group_id = '$group_id' ";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query("commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);
///-----------------------------------------------------------------------------
// 遷移元画面を再表示
///-----------------------------------------------------------------------------
echo("<script type=\"text/javascript\">location.href = 'duty_shift_entity_group.php?session=$session&group_id=$group_id';</script>");