<?php
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');

require_once("about_comedix.php");
require_once("show_class_name.ini");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_header_class.php");
require_once('class/Cmx/Model/SystemConfig.php');

// CmxSystemConfigインスタンス生成
$sysConf = new Cmx_SystemConfig();

//デザイン（1=旧デザイン、2=新デザイン）
$design_mode = $sysConf->get('fantol.design.mode');

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// お知らせ情報を取得
$sql = "SELECT inci_news.*, emp_lt_nm, emp_ft_nm, class_nm, job_nm FROM inci_news INNER JOIN empmst e USING (emp_id) LEFT JOIN classmst USING (class_id) LEFT JOIN jobmst USING (job_id) WHERE inci_news.news_id={$news_id}";
$sel_news = select_from_table($con, $sql, "", $fname);
if ($sel_news == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$news = pg_fetch_assoc($sel_news);
$record_flg = $news["record_flg"];

// 添付ファイル情報を取得
$sql = "SELECT newsfile_no, newsfile_name FROM inci_newsfile WHERE news_id={$news_id} ORDER BY newsfile_no";
$sel_file = select_from_table($con, $sql, "", $fname);
if ($sel_file == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$newsfile_folder_name = get_newsfile_folder_name();
$filename_flg = get_newsfile_filename_flg();
$files = array();
$imgfile_flg = false;
while ($row = pg_fetch_assoc($sel_file)) {
    $srcfile = sprintf('%s/%s_%s%s', $newsfile_folder_name, $news_id, $row["newsfile_no"], strrchr($row["newsfile_name"], "."));

    $files[] = array(
        "newsfile_no" => $row["newsfile_no"],
        "newsfile_name" => $row["newsfile_name"],
        "img" => preg_match("/(png|jpe*g|gif)$/i", $row["newsfile_name"]),
        "src" => $filename_flg > 0 ? sprintf('hiyari_news_file_refer.php?session=%s&news_id=%s&newsfile_no=%s', $session, $news_id, $row["newsfile_no"]) : $srcfile
    );

    if (preg_match("/(png|jpe*g|gif)$/i", $row["newsfile_name"])) {
        $imgfile_flg = true;
    }
}

// 組織名の取得
$arr_class_name = get_class_name_array($con, $fname);
$class_called = $arr_class_name[0];

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// お知らせ確認済み？
if ($record_flg == "t") {
    $sql = "SELECT COUNT(*) FROM inci_news_ref WHERE news_id={$news_id} AND emp_id='{$emp_id}'";
    $sel_ref = select_from_table($con, $sql, "", $fname);
    if ($sel_ref == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $referred_flg = (pg_fetch_result($sel_ref, 0, 0) > 0);
}

// カテゴリの表示設定
switch ($news["news_category"]) {
    case "1":
        $news_category = "全館";
        break;

    case "2":
        $news_category = "$class_called {$news["class_nm"]}";
        break;

    case "3":
        $news_category = "職種 {$news["job_nm"]}";
        break;
}

pg_close($con);

//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("session", h($session));

//ヘッダ用データ
$header = new hiyari_header_class($session, $fname, $con);
$smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
$smarty->assign("page_title", "お知らせ");

//参照確認
$smarty->assign("record_flg", $record_flg == "t");
$smarty->assign("referred_flg", $referred_flg);

//お知らせデータ
$smarty->assign("news_id", h($news_id));
$smarty->assign("news_title", $news["news_title"]);
$smarty->assign("news_category", h($news_category));
$smarty->assign("news", nl2br($news["news"]));

$file_list = array();
foreach ($files as $file) {
    $info['newsfile_no'] = $file["newsfile_no"];
    $info['src'] = $file["src"];
    $info['newsfile_name'] = $file["newsfile_name"];
    $info['is_img'] = $file["img"];
    $file_list[] = $info;
}
$smarty->assign("file_list", $file_list);
$smarty->assign("imgfile_flg", $imgfile_flg);

$smarty->assign("news_begin", preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $news["news_begin"]));
$smarty->assign("news_end", preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $news["news_end"]));
$smarty->assign("emp_lt_nm", h($news["emp_lt_nm"]));
$smarty->assign("emp_ft_nm", h($news["emp_ft_nm"]));
$smarty->assign("news_date", preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $news["news_date"]));

if ($design_mode == 1){
    $smarty->display("hiyari_news_detail1.tpl");
}
else{
    $smarty->display("hiyari_news_detail2.tpl");
}