<?
ob_start();
require("about_authority.php");
require("about_session.php");
ob_end_clean();

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	ajax_server_erorr();
	exit;
}

// ワークフロー権限のチェック
$check_auth = check_authority($session, 80, $fname);
if ($check_auth == "0") {
	pg_close($con);
	ajax_server_erorr();
	exit;
}


// データベースに接続
$con = connect2db($fname);

$st_nm = get_empst_ajax($con, $fname, $emp_id);

//レスポンスデータ作成
if($st_nm == "")
{
	//検索にヒットしない。
	$response_data = "ajax=0hit\n";
}
else
{

	$response_data = "ajax=success\n";

	$p_key = "st_nm";
	$p_value = $st_nm;
	$response_data .= "$p_key=$p_value\n";

	$p_key = "item_id";
	$p_value = $item_id;
	$response_data .= "$p_key=$p_value\n";
}

print $response_data;

// データベース切断
pg_close($con);



function get_empst_ajax($con, $fname, $emp_id)
{
	$sql  = "select stmst.st_nm from empmst ";
	$sql .= "inner join stmst on empmst.emp_st = stmst.st_id ";
	$cond = "where empmst.emp_id = '$emp_id' ";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		ajax_server_erorr();
		exit;
	}

	$st_nm = pg_fetch_result($sel, 0, "st_nm");

	return $st_nm;
}


function ajax_server_erorr()
{
	print "ajax=error\n";
	exit;
}

?>
