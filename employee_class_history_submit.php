<?php
require_once("about_comedix.php");
require_once("show_class_name.ini");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 18, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 職員登録権限を取得
$reg_auth = check_authority($session, 19, $fname);

// データベースに接続
$con = connect2db($fname);
$arr_class_name = get_class_name_array($con, $fname);

// 入力内容のチェック
if (empty($_POST["class_id"])) {
    echo("<script type=\"text/javascript\">alert('{$arr_class_name[0]}が選択されていません。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}
if (empty($_POST["atrb_id"])) {
    echo("<script type=\"text/javascript\">alert('{$arr_class_name[1]}が選択されていません。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}
if (empty($_POST["dept_id"])) {
    echo("<script type=\"text/javascript\">alert('{$arr_class_name[2]}が選択されていません。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}
if (!preg_match("/^\d\d\d\d\-\d\d\-\d\d \d\d:\d\d:\d\d$/", $_POST["histdate"])) {
    echo("<script type=\"text/javascript\">alert('日付が正しく入力されていません。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}

pg_query($con, "begin");

// UPDATE
if (!empty($_POST["class_history_id"])) {
    $sql = "UPDATE class_history SET ";
    $cond = "WHERE class_history_id={$_POST["class_history_id"]}";
    $upd = update_set_table(
        $con,
        $sql,
        array('emp_id', 'class_id', 'atrb_id', 'dept_id', 'room_id', 'histdate'),
        array($_POST["emp_id"], $_POST["class_id"], $_POST["atrb_id"], $_POST["dept_id"], $_POST["room_id"], $_POST["histdate"]),
        $cond,
        $fname
    );
    if ($upd == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}
// INSERT
else {
    $sql = "insert into class_history (emp_id, class_id, atrb_id, dept_id, room_id, histdate) values(";
    $content = array(
        $_POST["emp_id"],
        $_POST["class_id"],
        $_POST["atrb_id"],
        $_POST["dept_id"],
        $_POST["room_id"],
        $_POST["histdate"]
    );
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

pg_query($con, "commit");
pg_close($con);
?>
<script type="text/javascript">
location.href = 'employee_detail.php?session=<?=$session?>&emp_id=<?php eh($_POST["emp_id"]);?>';
</script>