<?
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");

define("IMAGETYPE_GIF", 1);
define("IMAGETYPE_JPEG", 2);
define("IMAGETYPE_PNG", 3);

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病棟登録権限チェック
$check_auth = check_authority($session, 21, $fname);
if ($check_auth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin transaction");

// 棟全階数を取得
$sql = "select bldg_floor from bldgmst";
$cond = "where bldg_cd = '$bldg_cd'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$max_flr = pg_result($sel, 0, "bldg_floor");

// 指定された階が棟全階数を超える場合はエラーとする
if($max_flr < $floor){
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert(\"指定された階は棟全階数を超えています。\");</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 棟名未入力チェック
if ($ward_nm == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert(\"病棟名が入力されていません。\");</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 病室数未入力チェック
if ($total_rm == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert(\"病室数が入力されていません。\");</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 病室数文字種チェック
$check = positive_number_validation($total_rm);
if ($check == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert(\"病室数は半角数字で入力してください。\");</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// 最大病床数未入力チェック
if ($max_bed == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert(\"最大病床数が入力されていません。\");</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 最大病床数文字種チェック
$check = positive_number_validation($max_bed);
if ($check == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert(\"最大病床数は半角数字で入力してください。\");</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// 稼働率計算用病床数チェック
if ($calc_bed == "") {
	$calc_bed = null;
} else if (!preg_match("/^\d{1,3}$/", $calc_bed)) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert(\"稼働率計算用病床数は半角数字で入力してください。\");</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
} else {
	$calc_bed = intval($calc_bed);
}

// 病棟区分未使用チェック
$sql = "select wddv_use_flg from wddvmst";
$cond = "where wddv_id = '$ward_type'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_fetch_result($sel, 0, "wddv_use_flg") == "f") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('選択された病棟区分は「使用しない」と設定されています。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// 料率未入力チェック
if ($ward_rate == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert(\"料率が入力されていません。\");</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// 料率チェック
if (!is_numeric($ward_rate)) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert(\"料率が正しくありません。\");</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// 画像タイプチェック
$ward_map_name_len = strlen($ward_map_name);
if ($ward_map_name_len > 0) {
	$ward_map_info = @getimagesize($ward_map);
	if ($ward_map_info === false) {
		$ward_map_format = "";
	} else {
		$ward_map_format = $ward_map_info[2];
	}

	if ($ward_map_format != IMAGETYPE_GIF && $ward_map_format != IMAGETYPE_JPEG && $ward_map_format != IMAGETYPE_PNG) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script language='javascript'>alert('病棟マップとして登録できるのはGIF、JPEG、PNGのみです');</script>");
		echo("<script language='javascript'>history.back();</script>");
		exit;
	}
}

// 病棟情報の更新
$sql = "update wdmst set";
$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd'";
$set = array("ward_name", "floor", "ward_rate", "calc_bed_no", "ward_type", "total_rm_no", "max_bed_no");
$setvalue = array($ward_nm, $floor, $ward_rate, $calc_bed, $ward_type, $total_rm, $max_bed);
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画像ファイルの保存
if ($ward_map_name_len > 0) {

	// ファイル名の作成
	$file_name = "{$bldg_cd}-{$ward_cd}";
	switch ($ward_map_format) {
	case IMAGETYPE_GIF:
		$file_name .= ".gif";
		$del_files = array("$file_name.jpg", "$file_name.png");
		break;
	case IMAGETYPE_JPEG:
		$file_name .= ".jpg";
		$del_files = array("$file_name.gif", "$file_name.png");
		break;
	case IMAGETYPE_PNG:
		$file_name .= ".png";
		$del_files = array("$file_name.gif", "$file_name.jpg");
		break;
	}

	// 保存先ディレクトリがなければ作成
	if (!is_dir("map")) {
		mkdir("map", 0755);
	}

	// ファイルの保存
	copy($ward_map, "map/$file_name");

	// 不要ファイルの削除
	foreach ($del_files as $del_file) {
		if (is_file("map/$del_file")) {
			unlink("map/$del_file");
		}
	}

}

// 病棟一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'ward_list.php?session=$session&bldg_cd=$bldg_cd';</script>");
?>
