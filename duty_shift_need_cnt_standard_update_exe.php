<?php
//=========================================================
// 必要人数設定 登録
//
//=========================================================
require_once("about_comedix.php");

//---------------------------------------------------------
ob_start();
//ページ名
$fname = $_SERVER['PHP_SELF'];

// データベースに接続
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 69, $fname);
$adminauth = check_authority($session, 70, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//---------------------------------------------------------
// 勤務シフトグループの権限のチェック
$group_id = p($_REQUEST["group_id"]);
$emp_id = p($_REQUEST["emp_id"]);
$sql = "SELECT * FROM duty_shift_group g
		WHERE group_id='{$group_id}'
		AND (person_charge_id='{$emp_id}'
		OR caretaker_id='{$emp_id}'
		OR caretaker2_id='{$emp_id}'
		OR caretaker3_id='{$emp_id}'
		OR caretaker4_id='{$emp_id}'
		OR caretaker5_id='{$emp_id}'
		OR caretaker6_id='{$emp_id}'
		OR caretaker7_id='{$emp_id}'
		OR caretaker8_id='{$emp_id}'
		OR caretaker9_id='{$emp_id}'
		OR caretaker10_id='{$emp_id}')";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel === 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) <= 0) {
	js_alert_exit("シフトグループを更新する権限がありません。");
}

//---------------------------------------------------------
// トランザクションを開始
pg_query($con, "begin transaction");

//--- duty_shift_need_cnt_standard
$sql = "DELETE FROM duty_shift_need_cnt_standard WHERE group_id='{$group_id}'";
$del = delete_from_table($con, $sql, "", $fname);
if ($del === 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// need_skill追加 2012.4.17
//$sql = "INSERT INTO duty_shift_need_cnt_standard (group_id,atdptn_ptn_id,job_id,job_id2,job_id3,mon_cnt,tue_cnt,wed_cnt,thu_cnt,fri_cnt,sat_cnt,sun_cnt,holiday_cnt,no,team,sex,st_id,need_skill) VALUES (";
// need_skill2追加 2014.1.21
$sql = "INSERT INTO duty_shift_need_cnt_standard (group_id,atdptn_ptn_id,job_id,job_id2,job_id3,mon_cnt,tue_cnt,wed_cnt,thu_cnt,fri_cnt,sat_cnt,sun_cnt,holiday_cnt,no,team,sex,st_id,need_skill,need_skill2) VALUES (";

for($i=0; $i<count($_REQUEST["atdptn_ptn_id"]); $i++) {
	$content = array(
		$group_id,
		p($_REQUEST["atdptn_ptn_id"][$i]),
		!empty($_REQUEST["job_id"][$i])?p($_REQUEST["job_id"][$i]):0,
		!empty($_REQUEST["job_id2"][$i])?p($_REQUEST["job_id2"][$i]):null,
		!empty($_REQUEST["job_id3"][$i])?p($_REQUEST["job_id3"][$i]):null,
		p($_REQUEST["mon_cnt"][$i]),
		p($_REQUEST["tue_cnt"][$i]),
		p($_REQUEST["wed_cnt"][$i]),
		p($_REQUEST["thu_cnt"][$i]),
		p($_REQUEST["fri_cnt"][$i]),
		p($_REQUEST["sat_cnt"][$i]),
		p($_REQUEST["sun_cnt"][$i]),
		p($_REQUEST["holiday_cnt"][$i]),
		$i+1,
		p($_REQUEST["team"][$i]?$_REQUEST["team"][$i]:0),
		p($_REQUEST["sex"][$i]),
		p($_REQUEST["st_id"][$i]?$_REQUEST["st_id"][$i]:0),
		p($_REQUEST["need_skill"][$i]?$_REQUEST["need_skill"][$i]:0),
		p($_REQUEST["need_skill2"][$i]?$_REQUEST["need_skill2"][$i]:0)
		);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins === 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);
ob_end_clean();
?>
<script type="text/javascript">location.href='duty_shift_need_cnt_standard.php?session=<?=$session?>&group_id=<?=$group_id?>'</script>