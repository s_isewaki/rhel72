<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">

<?
//ini_set("display_errors","1");
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_diary_common.ini");
require_once("date_utils.php");
require_once("get_menu_label.ini");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// 勤務シフト作成
$shift_menu_label = get_shift_menu_label($con, $fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	//初期値設定
	///-----------------------------------------------------------------------------
	//duty_shift_diary_common.iniから一覧の見出しと幅情報取得
	list($arr_title, $arr_width) = get_title();

	///-----------------------------------------------------------------------------
	// ログインユーザの職員ID・氏名を取得
	///-----------------------------------------------------------------------------
	$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
	$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
	///-----------------------------------------------------------------------------
	//ＤＢ(empmst)より職員情報を取得
	//ＤＢ(wktmgrp)より勤務パターン情報を取得
	///-----------------------------------------------------------------------------
//	$data_emp = $obj->get_empmst_array("");
// 有効なグループの職員を取得
	$data_emp = $obj->get_valid_empmst_array($emp_id, "", "");
	$data_wktmgrp = $obj->get_wktmgrp_array();
	///-----------------------------------------------------------------------------
	// 勤務シフトグループ情報を取得
	///-----------------------------------------------------------------------------
	$group_array = $obj->get_duty_shift_group_array($group_id, $data_emp, $data_wktmgrp);

	$start_date = $start_yr.$start_mon.$start_day;
	$data_atdptn_all = $obj->get_atdptn_array("");
	$data_pattern_all = $obj->get_duty_shift_pattern_array("", $data_atdptn_all);

	$reason_2_array = $obj->get_reason_2("1");

	//下書き有無確認用期間
	$arr_date = $obj->get_term_from_group_date($group_id, $group_array, $start_date);
	$proc_flg = 1; //処理フラグ 1:下書き有無確認する
	$data = get_allotment_data($con, $fname, $group_id, $group_array, $start_date, $data_pattern_all, $reason_2_array, $data_atdptn_all, $proc_flg, $arr_date);

	$wd = date_utils::get_weekday_name($start_date);
?>

<title>CoMedix <? echo($shift_menu_label); ?>｜勤務分担表</title>
<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<? // onload="self.print();self.close();" ?>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="self.print();self.close();" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="650" border="0" cellspacing="2" cellpadding="2">
	<tr><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">
	勤務分担表
	</font></td></tr>
	</table>
	<table width="650" border="0" cellspacing="0" cellpadding="0" class="list">
	<tr height="32">
	<td width="180" style="border-top:none;border-left:none;border-bottom:none;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><u><? echo($group_array[0]["group_name"]) ?></u></font></td>
	<td width="40" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">行事</font></td>
	<td width="290">&nbsp;</td>
	<td width="140" align="right" style="border-top:none;border-right:none;border-bottom:none;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><u><? echo($start_mon) ?>月<? echo($start_day) ?>日（<? echo($wd) ?>）</u></font></td>
	</tr>
	</table>
	<img src="img/spacer.gif" alt="" width="1" height="4"><br>

	<form name="mainform" method="post">
		<!-- ------------------------------------------------------------------------ -->
		<!-- 表 -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="650" border="0" cellspacing="0" cellpadding="2" class="list">
			<!-- ------------------------------------------------------------------------ -->
			<!-- パターン一覧（見出し） -->
			<!-- ------------------------------------------------------------------------ -->
			<tr height="22" bgcolor="#f6f9ff">
			<?
			for ($i=0; $i<count($arr_title); $i++) {
				echo("<td align=\"left\" width=\"{$arr_width[$i]}\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j10\"> \n");
				echo($arr_title[$i]);
				echo("</font></td> \n");
			}
			?>
			</tr>
			<!-- ------------------------------------------------------------------------ -->
			<!-- パターン一覧（データ） -->
			<!-- ------------------------------------------------------------------------ -->
			<?
			for ($i=0; $i<count($data); $i++) {
				echo("<tr height=\"40\"> \n");
				for ($j=0; $j<count($arr_title); $j++) {
					if ($j == 3) {
						$fontclass = "j12";
					} else {
						$fontclass = "j10";
					}
					echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$fontclass\"> \n");
					echo($data[$i][$j]);
					echo("</font></td> \n");
				}
				echo("</tr>\n");
			}
			?>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">

	</form>


</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>


