<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("cl_application_workflow_common_class.php");

// require_once("cl_application_workflow_select_box.ini");
require_once("cl_application_workflow_select_box2.ini");

require_once("cl_common.ini");
require_once("cl_title_name.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;


//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


$obj = new cl_application_workflow_common_class($con, $fname);

$cl_title = cl_title_name();

//====================================
// 初期処理
//====================================
// ログインユーザの職員IDを取得

// echo("before class = ". $emp_class . " atrb = ". $emp_attribute . " dept = ". $emp_dept . " room = ". $emp_room . " emp = " . $emp_emp);

$arr_empmst = $obj->get_empmst($session);
$emp_id = $arr_empmst[0]["emp_id"];
if ($mode != "search" && $mode != "select") {
	$emp_class = $arr_empmst[0]["emp_class"];
	$emp_attribute = $arr_empmst[0]["emp_attribute"];
	$emp_dept = $arr_empmst[0]["emp_dept"];
	$emp_room = $arr_empmst[0]["emp_room"];
	// 2011.11.22 Add by matsuura
	$emp_emp = "";
}

// 2011/12/19 Yamagawa add(s)
if ($nurse_type == "") {
	$nurse_type = "1";
}
// 2011/12/19 Yamagawa add(e)

// echo("after class = ". $emp_class . " atrb = ". $emp_attribute . " dept = ". $emp_dept . " room = ". $emp_room . " emp = " . $emp_emp);
//$arr_tmp_levelup_apply_emp = $obj->get_levelup_apply_emp_list($level, $emp_class, $emp_attribute, $emp_dept, $emp_room);
// 2011/11/29 Yamagawa upd(s)
//$arr_tmp_levelup_apply_emp = $obj->get_levelup_apply_emp_list2($level, $emp_class, $emp_attribute, $emp_dept, $emp_room, $emp_emp);
$arr_tmp_levelup_apply_emp = $obj->get_levelup_apply_emp_list2($level, $emp_class, $emp_attribute, $emp_dept, $emp_room, $emp_emp, $nurse_type);
// 2011/11/29 Yamagawa upd(e)

$arr_levelup_apply_emp = array();
foreach($arr_tmp_levelup_apply_emp as $levelup_apply_emp)
{
	$levelup_apply_id = $levelup_apply_emp["levelup_apply_id"]; // レベルアップ申請ID
    if($levelup_apply_id != "")
    {
	    $arr_apply_wkfwmst = $obj->get_apply_wkfwmst($levelup_apply_id);
        // 論理削除フラグがfalse　かつ　申請が未承認または承認済みの場合、除外する
	    if($arr_apply_wkfwmst[0]["delete_flg"] == "f" && ($arr_apply_wkfwmst[0]["apply_stat"] == "0" || $arr_apply_wkfwmst[0]["apply_stat"] == "1"))
	    {
            continue;
	    }
    }

    if($emp_emp != "" && $emp_emp != $levelup_apply_emp["emp_id"]){
    	continue;
    }

    $arr_levelup_apply_emp[] = $levelup_apply_emp;
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$cl_title?>｜レベルアップ志願者選択</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<script type="text/javascript">
<?
if ($apply_id != "") {
    $arr = $obj->get_levelup_application_emp_info($apply_id, $session);
?>

// 志願者が替わったかどうかを判定する
var firstChoice = judge_emp_change();

/* 2011.11.21 Add by matsuura
 * 　志願者選択画面で、「修正」ボタンを押下したとき、修正フィールドをクリヤーする
 * 　親のファイルにクリヤー関数があったとき、クリヤー関数を呼ぶ
 */
if(firstChoice  || '<? echo($modest); ?>' == 'f'){
	// スコアフィールドをクリヤーする
	if(opener.score_field_clear != null){
		opener.score_field_clear();
	}
	// 修正フィールドをクリヤーする
	if(opener.edit_field_clear != null){
		opener.edit_field_clear();
	}
}

// レベルアップ志願書ID
opener.document.getElementById('levelup_application_book_id').value = '<?=$arr["levelup_application_book_id"]?>';

// 2011.11.14 上で判定
// var firstChoice = opener.document.getElementById('levelup_application_book_id').value == '';


if (firstChoice || '<? echo($modest); ?>' != 't') {

	// レベルアップ志願書レベル
	opener.document.getElementById('levelup_application_book_level').value = '<?=$arr["levelup_application_book_level"]?>';

	// 申請日、氏名、所属、担当師長
	opener.document.getElementById('levelup_apply_date_disp').innerHTML = '<?=$arr["levelup_apply_date"]?>';
	opener.document.getElementById('levelup_apply_emp_name_disp').innerHTML = '<?=$arr["levelup_apply_emp_name"]?>';
	opener.document.getElementById('levelup_apply_emp_class_disp').innerHTML = '<?=$arr["levelup_apply_emp_class"]?>';
	opener.document.getElementById('head_nurse_name_disp').innerHTML = '<?=$arr["head_nurse_name"]?>';
	opener.document.getElementById('levelup_apply_date').value = '<?=$arr["levelup_apply_date"]?>';
	opener.document.getElementById('levelup_apply_emp_name').value = '<?=$arr["levelup_apply_emp_name"]?>';
	opener.document.getElementById('levelup_apply_emp_id').value = '<?=$arr["levelup_apply_emp_id"]?>';
	opener.document.getElementById('levelup_apply_emp_class').value = '<?=$arr["levelup_apply_emp_class"]?>';
	opener.document.getElementById('head_nurse_name').value = '<?=$arr["head_nurse_name"]?>';
	opener.document.getElementById('head_nurse_emp_id').value = '<?=$arr["head_nurse_emp_id"]?>';
}

/**
 * 志願者を選択したとき、以前の志願者と変わったかどうかを判定する
 * 	申請書の選択でなく、申請志願者が変わったかどうかを選択する。
 * @return 人が替わった　：true　　　替わらない: false
 * @author 2001.11.21 Update by matsuura
 */
function judge_emp_change(){
	var judge = false;
	var strBuf = "";

	// 2011.12.27 Add by matsuura  旧テンプレートに levelup_apply_before_emp のフィールドがないため
	if(opener.document.getElementById('levelup_apply_before_emp')){
		if(opener.document.getElementById('levelup_apply_before_emp').value != '<?=$arr["levelup_apply_emp_id"]?>'){
			if(opener.document.getElementById('levelup_apply_before_emp').value != null &&
					opener.document.getElementById('levelup_apply_before_emp').value != ""){
					judge = true;
					opener.document.getElementById('levelup_apply_before_emp').value = '<?=$arr["levelup_apply_emp_id"]?>';
			}else{
				judge = true;
				opener.document.getElementById('levelup_apply_before_emp').value = '<?=$arr["levelup_apply_emp_id"]?>';
			}
		}
	}
	return judge;
}

function getAncestorRow(elm) {
	for (var i = 0; i < 50; i++) {
		if (elm.tagName.toLowerCase() == 'tr') return elm;
		elm = elm.parentNode;
	}
	return null;
}

function getTargetCell(elm, index) {
	var row = getAncestorRow(elm);
	var cells = row.getElementsByTagName('td');
	return cells[index];
}

function getInnerText(elm) {
	var text = elm.innerText;
	if (!text) return elm.textContent;
	return text;
}

function buildLabelHTML(disp, applyID, apvOrder, apvSubOrder, labelText) {
	var labelHTML;
	if (disp != '') {
		labelHTML = '<a href="javascript:void(0);" onclick="window.open(\'cl_application_approve_detail.php?session=<? echo($session); ?>&apply_id=' + applyID + '&apv_order_fix=' + apvOrder + '&apv_sub_order_fix=' + apvSubOrder + '&mode=approve_print\', \'graderwin\', \'directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1024,height=700\');">' + labelText + '</a>';
	} else {
		labelHTML = labelText;
	}
	return '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">' + labelHTML + '</font>';
}

// 評価者別集計表
var disp, labelCell;
if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No1_1"]); ?>' != '') && opener.document.getElementById('grader_score_No1_1')) {
	opener.document.getElementById('grader_emp_name_1_disp').innerHTML = '<?=$arr["grader_emp_name_1"]?>';
	opener.document.getElementById('grader_emp_id_1').value = '<?=$arr["grader_emp_id_1"]?>';
	opener.document.getElementById('grader_emp_name_1').value = '<?=$arr["grader_emp_name_1"]?>';
	disp = opener.document.getElementById('grader_score_No1_1_disp');
	disp.innerHTML = '<?=$arr["grader_score_No1_1"]?>';
	labelCell = getTargetCell(disp, 1);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No1_1"]); ?>', '1', '1', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No1_1').value = '<?=$arr["grader_score_No1_1"]?>';
}

if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No1_2"]); ?>' != '') && opener.document.getElementById('grader_score_No1_2')) {
	opener.document.getElementById('grader_emp_name_2_disp').innerHTML = '<?=$arr["grader_emp_name_2"]?>';
	opener.document.getElementById('grader_emp_id_2').value = '<?=$arr["grader_emp_id_2"]?>';
	opener.document.getElementById('grader_emp_name_2').value = '<?=$arr["grader_emp_name_2"]?>';
	disp = opener.document.getElementById('grader_score_No1_2_disp');
	disp.innerHTML = '<?=$arr["grader_score_No1_2"]?>';
	labelCell = getTargetCell(disp, 0);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No1_2"]); ?>', '1', '2', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No1_2').value = '<?=$arr["grader_score_No1_2"]?>';
}

if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No2_1"]); ?>' != '') && opener.document.getElementById('grader_score_No2_1')) {
	opener.document.getElementById('grader_emp_name_1_disp').innerHTML = '<?=$arr["grader_emp_name_1"]?>';
	opener.document.getElementById('grader_emp_id_1').value = '<?=$arr["grader_emp_id_1"]?>';
	opener.document.getElementById('grader_emp_name_1').value = '<?=$arr["grader_emp_name_1"]?>';
	disp = opener.document.getElementById('grader_score_No2_1_disp');
	disp.innerHTML = '<?=$arr["grader_score_No2_1"]?>';
	labelCell = getTargetCell(disp, 1);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No2_1"]); ?>', '1', '1', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No2_1').value = '<?=$arr["grader_score_No2_1"]?>';
}

if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No2_2"]); ?>' != '') && opener.document.getElementById('grader_score_No2_2')) {
	opener.document.getElementById('grader_emp_name_2_disp').innerHTML = '<?=$arr["grader_emp_name_2"]?>';
	opener.document.getElementById('grader_emp_id_2').value = '<?=$arr["grader_emp_id_2"]?>';
	opener.document.getElementById('grader_emp_name_2').value = '<?=$arr["grader_emp_name_2"]?>';
	disp = opener.document.getElementById('grader_score_No2_2_disp');
	disp.innerHTML = '<?=$arr["grader_score_No2_2"]?>';
	labelCell = getTargetCell(disp, 0);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No2_2"]); ?>', '1', '2', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No2_2').value = '<?=$arr["grader_score_No2_2"]?>';
}

if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No3"]); ?>' != '') && opener.document.getElementById('grader_score_No3')) {
	opener.document.getElementById('grader_emp_name_3_disp').innerHTML = '<?=$arr["grader_emp_name_3"]?>';
	opener.document.getElementById('grader_emp_id_3').value = '<?=$arr["grader_emp_id_3"]?>';
	opener.document.getElementById('grader_emp_name_3').value = '<?=$arr["grader_emp_name_3"]?>';
	disp = opener.document.getElementById('grader_score_No3_disp');
	disp.innerHTML = '<?=$arr["grader_score_No3"]?>';
	labelCell = getTargetCell(disp, 0);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No3"]); ?>', '1', '', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No3').value = '<?=$arr["grader_score_No3"]?>';
}

if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No4"]); ?>' != '') && opener.document.getElementById('grader_score_No4')) {
	opener.document.getElementById('grader_emp_name_3_disp').innerHTML = '<?=$arr["grader_emp_name_3"]?>';
	opener.document.getElementById('grader_emp_id_3').value = '<?=$arr["grader_emp_id_3"]?>';
	opener.document.getElementById('grader_emp_name_3').value = '<?=$arr["grader_emp_name_3"]?>';
	disp = opener.document.getElementById('grader_score_No4_disp');
	disp.innerHTML = '<?=$arr["grader_score_No4"]?>';
	labelCell = getTargetCell(disp, 0);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No4"]); ?>', '1', '', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No4').value = '<?=$arr["grader_score_No4"]?>';
}

var score4Set = false;
if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No5"]); ?>' != '') && opener.document.getElementById('grader_score_No5')) {
	disp = opener.document.getElementById('grader_score_No5_disp');
	disp.innerHTML = '<?=$arr["grader_score_No5"]?>';
	labelCell = getTargetCell(disp, 1);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No5"]); ?>', '1', '', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No5').value = '<?=$arr["grader_score_No5"]?>';
	score4Set = true;
}
if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No6"]); ?>' != '') && opener.document.getElementById('grader_score_No6')) {
	disp = opener.document.getElementById('grader_score_No6_disp');
	disp.innerHTML = '<?=$arr["grader_score_No6"]?>';
	labelCell = getTargetCell(disp, 0);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No6"]); ?>', '1', '', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No6').value = '<?=$arr["grader_score_No6"]?>';
	score4Set = true;
}
if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No7"]); ?>' != '') && opener.document.getElementById('grader_score_No7')) {
	disp = opener.document.getElementById('grader_score_No7_disp');
	disp.innerHTML = '<?=$arr["grader_score_No7"]?>';
	labelCell = getTargetCell(disp, 0);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No7"]); ?>', '1', '', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No7').value = '<?=$arr["grader_score_No7"]?>';
	score4Set = true;
}
if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No8"]); ?>' != '') && opener.document.getElementById('grader_score_No8')) {
	disp = opener.document.getElementById('grader_score_No8_disp');
	disp.innerHTML = '<?=$arr["grader_score_No8"]?>';
	labelCell = getTargetCell(disp, 0);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No8"]); ?>', '1', '', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No8').value = '<?=$arr["grader_score_No8"]?>';
	score4Set = true;
}
if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No9"]); ?>' != '') && opener.document.getElementById('grader_score_No9')) {
	disp = opener.document.getElementById('grader_score_No9_disp');
	disp.innerHTML = '<?=$arr["grader_score_No9"]?>';
	labelCell = getTargetCell(disp, 0);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No9"]); ?>', '1', '', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No9').value = '<?=$arr["grader_score_No9"]?>';
	score4Set = true;
}
if (score4Set) {
	opener.document.getElementById('grader_emp_name_4_1_disp').innerHTML = '<?=$arr["grader_emp_name_4"]?>';
	opener.document.getElementById('grader_emp_name_4_2_disp').innerHTML = '<?=$arr["grader_emp_name_4"]?>';
	opener.document.getElementById('grader_emp_name_4_3_disp').innerHTML = '<?=$arr["grader_emp_name_4"]?>';
	opener.document.getElementById('grader_emp_name_4_4_disp').innerHTML = '<?=$arr["grader_emp_name_4"]?>';
	opener.document.getElementById('grader_emp_id_4').value = '<?=$arr["grader_emp_id_4"]?>';
	opener.document.getElementById('grader_emp_name_4').value = '<?=$arr["grader_emp_name_4"]?>';
}

if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No10"]); ?>' != '') && opener.document.getElementById('grader_score_No10')) {
	opener.document.getElementById('grader_emp_name_5_disp').innerHTML = '<?=$arr["grader_emp_name_5"]?>';
	opener.document.getElementById('grader_emp_id_5').value = '<?=$arr["grader_emp_id_5"]?>';
	opener.document.getElementById('grader_emp_name_5').value = '<?=$arr["grader_emp_name_5"]?>';
	disp = opener.document.getElementById('grader_score_No10_disp');
	disp.innerHTML = '<?=$arr["grader_score_No10"]?>';
	labelCell = getTargetCell(disp, 1);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No10"]); ?>', '1', '', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No10').value = '<?=$arr["grader_score_No10"]?>';
}

if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No11"]); ?>' != '') && opener.document.getElementById('grader_score_No11')) {
	opener.document.getElementById('grader_emp_name_5_disp').innerHTML = '<?=$arr["grader_emp_name_5"]?>';
	opener.document.getElementById('grader_emp_id_5').value = '<?=$arr["grader_emp_id_5"]?>';
	opener.document.getElementById('grader_emp_name_5').value = '<?=$arr["grader_emp_name_5"]?>';
	disp = opener.document.getElementById('grader_score_No11_disp');
	disp.innerHTML = '<?=$arr["grader_score_No11"]?>';
	labelCell = getTargetCell(disp, 1);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No11"]); ?>', '1', '', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No11').value = '<?=$arr["grader_score_No11"]?>';
}

if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No12"]); ?>' != '') && opener.document.getElementById('grader_score_No12')) {
	opener.document.getElementById('grader_emp_name_6_1_disp').innerHTML = '<?=$arr["grader_emp_name_6"]?>';
	opener.document.getElementById('grader_emp_id_6').value = '<?=$arr["grader_emp_id_6"]?>';
	opener.document.getElementById('grader_emp_name_6').value = '<?=$arr["grader_emp_name_6"]?>';
	disp = opener.document.getElementById('grader_score_No12_disp');
	disp.innerHTML = '<?=$arr["grader_score_No12"]?>';
	labelCell = getTargetCell(disp, 1);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No12"]); ?>', '1', '', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No12').value = '<?=$arr["grader_score_No12"]?>';
}

// 2011/11/29 Yamagawa add(s)
if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No31_1"]); ?>' != '') && opener.document.getElementById('grader_score_No31_1')) {
	opener.document.getElementById('grader_emp_name_3_1_disp').innerHTML = '<?=$arr["grader_emp_name_3_1"]?>';
	opener.document.getElementById('grader_emp_id_3_1').value = '<?=$arr["grader_emp_id_3_1"]?>';
	opener.document.getElementById('grader_emp_name_3_1').value = '<?=$arr["grader_emp_name_3_1"]?>';
	disp = opener.document.getElementById('grader_score_No31_1_disp');
	disp.innerHTML = '<?=$arr["grader_score_No31_1"]?>';
	labelCell = getTargetCell(disp, 0);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No31_1"]); ?>', '1', '1', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No31_1').value = '<?=$arr["grader_score_No31_1"]?>';
}

if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No31_2"]); ?>' != '') && opener.document.getElementById('grader_score_No31_2')) {
	opener.document.getElementById('grader_emp_name_3_2_disp').innerHTML = '<?=$arr["grader_emp_name_3_2"]?>';
	opener.document.getElementById('grader_emp_id_3_2').value = '<?=$arr["grader_emp_id_3_2"]?>';
	opener.document.getElementById('grader_emp_name_3_2').value = '<?=$arr["grader_emp_name_3_2"]?>';
	disp = opener.document.getElementById('grader_score_No31_2_disp');
	disp.innerHTML = '<?=$arr["grader_score_No31_2"]?>';
	labelCell = getTargetCell(disp, 0);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No31_2"]); ?>', '1', '2', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No31_2').value = '<?=$arr["grader_score_No31_2"]?>';
}

if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No32"]); ?>' != '') && opener.document.getElementById('grader_score_No32')) {
	opener.document.getElementById('grader_emp_name_5_2_disp').innerHTML = '<?=$arr["grader_emp_name_5_2"]?>';
	opener.document.getElementById('grader_emp_id_5_2').value = '<?=$arr["grader_emp_id_5_2"]?>';
	opener.document.getElementById('grader_emp_name_5_2').value = '<?=$arr["grader_emp_name_5_2"]?>';
	disp = opener.document.getElementById('grader_score_No32_disp');
	disp.innerHTML = '<?=$arr["grader_score_No32"]?>';
	labelCell = getTargetCell(disp, 0);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No32"]); ?>', '1', '', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No32').value = '<?=$arr["grader_score_No32"]?>';
}

if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["grader_score_No33"]); ?>' != '') && opener.document.getElementById('grader_score_No33')) {
	opener.document.getElementById('grader_emp_name_6_2_disp').innerHTML = '<?=$arr["grader_emp_name_6_2"]?>';
	opener.document.getElementById('grader_emp_id_6_2').value = '<?=$arr["grader_emp_id_6_2"]?>';
	opener.document.getElementById('grader_emp_name_6_2').value = '<?=$arr["grader_emp_name_6_2"]?>';
	disp = opener.document.getElementById('grader_score_No33_disp');
	disp.innerHTML = '<?=$arr["grader_score_No33"]?>';
	labelCell = getTargetCell(disp, 0);
	labelCell.innerHTML = buildLabelHTML(disp.innerHTML, '<? echo($arr["grader_apply_id_No33"]); ?>', '1', '', getInnerText(labelCell));
	opener.document.getElementById('grader_score_No33').value = '<?=$arr["grader_score_No33"]?>';
}
// 2011/11/29 Yamagawa add(e)

// 評価項目別集計表
<?
// 2011/11/29 Yamagawa upd(s)
//for ($i = 1; $i <= 12; $i++) {
for ($i = 1; $i <= 33; $i++) {
// 2011/11/29 Yamagawa upd(e)
?>
if ((firstChoice || '<? echo($modest); ?>' != 't' || '<? echo($arr["item_score_No$i"]); ?>' != '') && opener.document.getElementById('item_score_No<? echo($i); ?>')) {
	opener.document.getElementById('item_score_No<? echo($i); ?>_disp').innerHTML = '<?=$arr["item_score_No$i"]?>';
	opener.document.getElementById('item_score_No<? echo($i); ?>').value = '<?=$arr["item_score_No$i"]?>';
}
<?
}
?>

// 総合計点
var totalScore = null;
// 2011/11/29 Yamagawa upd(s)
//for (var i = 1; i <= 12; i++) {
for (var i = 1; i <= 33; i++) {
// 2011/11/29 Yamagawa upd(e)
	var scoreElement = opener.document.getElementById('item_score_No' + i);
	if (!scoreElement) {
		continue;
	}
	if (scoreElement.value == '') {
		continue;
	}
	if (totalScore === null) {
		totalScore = 0;
	}
	totalScore += parseInt(scoreElement.value, 10);
}
totalScore = (totalScore !== null) ? totalScore.toString() : '';
opener.document.getElementById('total_score').value = totalScore;

if (firstChoice || '<? echo($modest); ?>' != 't') {

	// 申請結果通知者
	opener.document.getElementById('notice_emp_id').value = "";
	opener.document.getElementById('notice_emp_nm').value = "";
	opener.document.getElementById('rslt_ntc_div').value = "";
	opener.add_target_list('0', '<?=$arr["levelup_apply_emp_id"]?>', '<?=$arr["levelup_apply_emp_name"]?>');

	// 添付ファイル
	opener.document.getElementById('attach').innerHTML = '';
<?
	$file_id = 1;
	foreach ($arr["apply_files"] as $tmp_apply_id => $apply_files) {
		if (count($apply_files) == 0) {
			continue;
		}

		foreach ($apply_files as $apply_file) {
			$tmp_filename = $apply_file["applyfile_name"];
			$tmp_file_no = $apply_file["applyfile_no"];

			// 一時フォルダにコピー
			$ext = strrchr($tmp_filename, ".");
			copy("cl/apply/{$tmp_apply_id}_{$tmp_file_no}{$ext}", "cl/apply/tmp/{$session}_{$file_id}{$ext}");
?>
	var fname = '<?=$tmp_filename?>';

	var a = opener.document.createElement('a');
	a.href = 'cl/apply/tmp/<? echo("{$session}_{$file_id}{$ext}"); ?>';
	a.target = '_blank';
	a.appendChild(opener.document.createTextNode(fname));

	var inputA = opener.document.createElement('input');
	inputA.type = 'button';
	inputA.name = 'btn_<? echo($file_id); ?>';
	inputA.value = '削除';
	inputA.id = 'btn_<? echo($file_id); ?>';
	if (inputA.onclick === null) {  // IE, Safari
		inputA.setAttribute('onclick', 'detachFile();');
	}
	if (typeof inputA.onclick == 'string') {  // IE
		inputA.setAttribute('onclick', null);
	}
	if (inputA.onclick == null) {  // IE, FireFox
		inputA.onclick = opener.detachFile;
	}

	var inputB = opener.document.createElement('input');
	inputB.type = 'hidden';
	inputB.name = 'filename[]';
	inputB.value = fname;

	var inputC = opener.document.createElement('input');
	inputC.type = 'hidden';
	inputC.name = 'file_id[]';
	inputC.value = '<? echo($file_id); ?>';

	var p = opener.document.createElement('p');
	p.id = 'p_<? echo($file_id); ?>';
	p.appendChild(a);
	p.appendChild(opener.document.createTextNode(' '));
	p.appendChild(inputA);
	p.appendChild(inputB);
	p.appendChild(inputC);

	var div = opener.document.getElementById('attach');
	div.appendChild(p);
<?
			$file_id++;
		}
	}
?>
}

// 2011.11.15 Edit by matsuura 　連続選択を可能にするため、志願者を選択しても画面は閉じない
// 画面を閉じる
// self.close();
<?
}
?>


/**
 * 選択ボタンを押下したときの処理
 * @param apply_id : String 選択された申請ID
 * @param apply_emp_id : String 選択された申請の申請者ID
 * @param class_id : String 検索条件
 * @param atrb_id : String  検索条件
 * @param dept_id : String  検索条件
 * @param room_id : String  検索条件
 * @param emp_id : String   検索条件
 * @author 2011.11.25 Update by matsuura
 */
function set_levelup_apply_data(apply_id, apply_emp_id, class_id, atrb_id, dept_id, room_id, emp_id)
{
	//==================================================
	// 志願者が変更となる場合の確認ダイアログ表示
	//==================================================

	// 2011.12.27 Add by matsuura  旧テンプレートに levelup_apply_before_emp のフィールドがないため
	if(opener.document.getElementById('levelup_apply_before_emp')){
		//前回指定した志願者と今回選択した志願者が異なる場合
		if(opener.document.getElementById('levelup_apply_before_emp').value != apply_emp_id)
		{
			//前回選択した志願者が設定されている場合(nullでも空文字でもない場合)
			if(opener.document.getElementById('levelup_apply_before_emp').value != null &&
					opener.document.getElementById('levelup_apply_before_emp').value != "")
			{
				//実行確認ダイアログを表示
				if(!confirm("前者のスコアはクリヤーされます。よろしいですか？"))
				{
					//実行しない場合、処理終了
					return;
				}
			}
		}
	}

	//==================================================
	// ポストバックしてスコア情報を反映する。
	//==================================================
	document.apply.action="cl_levelup_apply_emp_list.php?apply_id=" + apply_id + "&mode=select&emp_class=" + class_id +
							"&emp_attribute=" + atrb_id	 + "&emp_dept=" + dept_id + "&emp_room=" + room_id + "&emp_emp=" + emp_id;
	document.apply.submit();
}

/**
 * 検索ボタンを押下したときの処理
 */
function searchEmployee() {
	var emp_class = document.apply.search_emp_class.value;
	var emp_attribute = document.apply.search_emp_attribute.value;
	var emp_dept = document.apply.search_emp_dept.value;
	var emp_room = (document.apply.search_emp_room) ? document.apply.search_emp_room.value : '';
	// 2011.11.22 Add by matsuura
	var emp_emp = document.apply.search_emp_emp.value;

	// 2011.11.22 Edit by matsuura
//	location.href = 'cl_levelup_apply_emp_list.php?session=<? echo($session); ?>&level=<? echo($level); ?>&mode=search&emp_class=' + emp_class + '&emp_attribute=' + emp_attribute + '&emp_dept=' + emp_dept + '&emp_room=' + emp_room;
	// 2011.11.29 Edit by Yamagawa
//	location.href = 'cl_levelup_apply_emp_list.php?session=<? echo($session); ?>&level=<? echo($level); ?>&mode=search&emp_class=' + emp_class + '&emp_attribute=' + emp_attribute + '&emp_dept=' + emp_dept + '&emp_room=' + emp_room + '&emp_emp=' + emp_emp;
	location.href = 'cl_levelup_apply_emp_list.php?session=<? echo($session); ?>&level=<? echo($level); ?>&mode=search&emp_class=' + emp_class + '&emp_attribute=' + emp_attribute + '&emp_dept=' + emp_dept + '&emp_room=' + emp_room + '&emp_emp=' + emp_emp + '&nurse_type=<? echo($nurse_type); ?>';
}




</script>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" id="kanri_no" name="kanri_no" value="<?=$kanri_no?>">
<input type="hidden" id="level" name="level" value="<?=$level?>">
<!-- 2011/11/29 Yamagawa add(s) -->
<input type="hidden" id="nurse_type" name="nurse_type" value="<?=$nurse_type?>">
<!-- 2011/11/29 Yamagawa add(e) -->
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>レベルアップ志願者選択</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block">
<tr>
<td>
<?
// 2011.11.25 Edit by matsuura　SelectBox のセット・表示をCallする
// show_post_select_box($con, $fname, $emp_class, $emp_attribute, $emp_dept, $emp_room);
// echo("mode = ". $mode);

// 2011.12.20 Edit by matsuura
/*
if ($mode != "search" && $mode != "select") {
	// 2011.11.29 Edit by Yamagawa
//	show_post_select_box($con, $fname, $level, $emp_class, $emp_attribute, $emp_dept, $emp_room);
	show_post_select_box($con, $fname, $level, $emp_class, $emp_attribute, $emp_dept, $emp_room, $nurse_type);
}else{
	// 2011.11.29 Edit by Yamagawa
//	show_post_select_box2($con, $fname, $level, $emp_class, $emp_attribute, $emp_dept, $emp_room, $emp_emp);
	show_post_select_box2($con, $fname, $level, $emp_class, $emp_attribute, $emp_dept, $emp_room, $emp_emp, $nurse_type);
}
*/
show_post_select_box2($con, $fname, $level, $emp_class, $emp_attribute, $emp_dept, $emp_room, $emp_emp, $nurse_type);

?>
</td>
<td align="center"><input type="button" value="検索" onclick="searchEmployee();"></td>
</tr>
<tr>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="modest" value="t">スコアを上書きする</font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<?
if(count($arr_levelup_apply_emp) == 0)
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">レベルアップ志願者はおりません。</font>
</td>
</tr>
</table>
<?
}
else
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
<tr bgcolor="#E5F6CD" align="center">
<td width="80">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">選択</font>
</td>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請番号</font>
</td>
<td width="120">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">志願日</font>
</td>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">志願者</font>
</td>
</tr>

<?

//print_r($arr_levelup_apply_emp);

foreach($arr_levelup_apply_emp as $levelup_apply_emp)
{
    $apply_id        = $levelup_apply_emp["apply_id"];
	$apply_emp_id   = $levelup_apply_emp["emp_id"];
    $level           = $levelup_apply_emp["level"];
    $apply_date      = $levelup_apply_emp["apply_date"];
    $apply_no        = $levelup_apply_emp["apply_no"];
    $short_wkfw_name = $levelup_apply_emp["short_wkfw_name"];
    $emp_full_nm     = $levelup_apply_emp["emp_lt_nm"]." ".$levelup_apply_emp["emp_ft_nm"];

	// 申請番号
    $year = substr($apply_date, 0, 4);
    $md   = substr($apply_date, 4, 4);
    if($md >= "0101" and $md <= "0331")
    {
        $year = $year - 1;
    }
    $apply_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

    // 志願日
   	$apply_date = $obj->get_apply_date($levelup_apply_emp["apply_content"]);

?>
<tr>
<!-- 2011.11.25 Edit by matsuura -->
<td align="center"><input type="button" value="選択" onclick="set_levelup_apply_data('<?=$apply_id?>','<?=$apply_emp_id?>','<?=$emp_class?>','<?=$emp_attribute?>','<?=$emp_dept?>','<?=$emp_room?>','<?=$emp_emp?>')"></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$apply_no?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$apply_date?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$emp_full_nm?></font></td>

</tr>
<?
}
?>
</table>
<?
}
?>

<center>
</body>
</form>
</html>


<?

pg_close($con);
?>