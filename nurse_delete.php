<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");

// ページ名
$fname = $PHP_SELF;

// セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限チェック
$auth_check = check_authority($session,28,$fname);
if($auth_check == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin transaction");

// 担当看護師の論理削除
if (is_array($del_nurse)) {
	foreach($del_nurse as $key) {
		list($tmp_enti_id, $tmp_sect_id, $tmp_nurse_id) = split("-", $key);
		$cond = "where enti_id = $tmp_enti_id and sect_id = $tmp_sect_id and nurse_id = $tmp_nurse_id";
		$set = array("nurse_del_flg");
		$setvalue = array("t");
		$up = update_set_table($con, $SQL106, $set, $setvalue, $cond, $fname);
		if ($up == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションのコミット
pg_query($con, "commit");

// 担当看護師一覧画面に遷移
echo("<script language=\"javascript\">location.href = 'nurse_list.php?session=$session';</script>\n");
?>
