<?
require("./about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// グループ設定を更新
$sql = "update option set";
$set = array("schedule_group");
$setvalue = array($schedule_group);
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// トップページをリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'left.php?session=$session&date=$date&bedschd_date=$bedschd_date'</script>");
?>
