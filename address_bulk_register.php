<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix アドレス帳 | 一括登録</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("show_prefectures.ini"); ?>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アドレス帳権限のチェック
$checkauth = check_authority($session, 9, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アドレス帳管理権限を取得
$adbk_admin_auth=check_authority($session,64,$fname);

// 登録先のデフォルトは「個人アドレス帳」
if ($shared_flg == "") {
	$shared_flg = "f";
}
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
function checkForm(form) {
	if (form.csvfile.value == '') {
		alert('ファイルを選択してください。');
		return false;
	} else {
		return true;
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="address_menu.php?session=<? echo($session); ?>"><img src="img/icon/b13.gif" width="32" height="32" border="0" alt="アドレス帳"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="address_menu.php?session=<? echo($session); ?>"><b>アドレス帳</b></a></font></td>
<? if ($adbk_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="address_admin_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="./address_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アドレス一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="./address_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規作成</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="address_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一括登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7">
<?
echo ("<a href=\"address_contact_update.php?session=$session&emp_id=&search_shared_flg=$search_shared_flg&search_name=$url_name&search_submit=$url_submit&page=$page&admin_flg=$admin_flg\">");
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">本人連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="address_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">グループ作成</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="address" action="address_bulk_insert.php" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this);">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td width="120" height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録先</font></td>
<td width="480"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="shared_flg" value="f"<? if ($shared_flg == "f") {echo(" checked");} ?>>個人アドレス帳&nbsp;</font></td>
</tr>
<tr>
<td height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル</font></td>
<td><input name="csvfile" type="file" value="" size="50"></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="22" colspan="2" align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="admin_flg" value="f">
</form>
<? if ($result != "") { ?>
<table width="600" border="0" cellspacing="0" cellpadding="1">
<? } ?>
<? if ($result == "f") { ?>
<tr>
<td height="22" width="120"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">ファイルの内容に問題があるため、登録処理が実行できませんでした。</font></td>
</tr>
<? } else if ($result == "t") {?>
<tr>
<td height="22" width="120"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録処理が完了しました。</font></td>
</tr>
<? } ?>
<? if ($result != "") { ?>
</table>
<? } ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="600" border="0" cellspacing="0" cellpadding="1">
<tr>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>CSVのフォーマット</b></font></td>
</tr>
<tr>
<td width="30"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<p style="margin:0;">下記項目をカンマで区切り、レコードごとに改行します。</p>
<ul style="margin:0;">
<li style="margin-left:40px;">姓（漢字）･･･必須</li>
<li style="margin-left:40px;">名（漢字）･･･必須</li>
<li style="margin-left:40px;">姓（カナ）</li>
<li style="margin-left:40px;">名（カナ）</li>
<li style="margin-left:40px;">会社名･･･必須</li>
<li style="margin-left:40px;">部門名１</li>
<li style="margin-left:40px;">部門名２</li>
<li style="margin-left:40px;">役職１</li>
<li style="margin-left:40px;">役職２</li>
<li style="margin-left:40px;">郵便番号･･･ハイフン区切り</li>
<li style="margin-left:40px;">都道府県名･･･「北海道」〜「沖縄県」</li>
<li style="margin-left:40px;">住所１</li>
<li style="margin-left:40px;">住所２</li>
<li style="margin-left:40px;">住所３</li>
<li style="margin-left:40px;">電話･･･ハイフン区切り</li>
<li style="margin-left:40px;">FAX･･･ハイフン区切り</li>
<li style="margin-left:40px;">携帯／PHS･･･ハイフン区切り</li>
<li style="margin-left:40px;">E-MAIL（PC）</li>
<li style="margin-left:40px;">E-MAIL（携帯）</li>
<li style="margin-left:40px;">備考</li>
</ul>
</font></td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>
