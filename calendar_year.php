<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix マスターメンテナンス | カレンダー | 年間カレンダー</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<?
require("about_session.php");
require("about_authority.php");
require("maintenance_menu_list.ini");
require("show_date_navigation_year.ini");
require("holiday.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 17, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 背景色を定数として定義
define("BGCOLOR_DAYTYPE1", "#d4d4d4");  // 通常勤務日
define("BGCOLOR_DAYTYPE2", "#aaeeff");  // 勤務日1
define("BGCOLOR_DAYTYPE3", "#aaffcc");  // 勤務日2
define("BGCOLOR_DAYTYPE4", "#ffaaaa");  // 法定休日
define("BGCOLOR_DAYTYPE5", "#ffddaa");  // 所定休日
define("BGCOLOR_DAYTYPE6", "#fc8181");  // 祝日
define("BGCOLOR_DAYTYPE7", "#e4bf5b");  // 年末年始休暇
define("BGCOLOR_DAYTYPE8", "#ee82ee");  // 買取日


// データベースに接続
$con = connect2db($fname);

// オプション設定情報を取得
$sql = "select calendar_start1 from option";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$calendar_start = pg_fetch_result($sel, 0, "calendar_start1");

// スタート曜日の設定
$start_wd = ($calendar_start == 1) ? 0 : 1;  // 0：日曜、1：月曜

// タイムスタンプが渡されていない場合は当日日付のタイムスタンプを取得
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}

// 前年・翌年のタイプスタンプを変数に格納
$last_year = get_last_year($date);
$next_year = get_next_year($date);

// 勤務日・休日名称を取得
$sql = "select day2, day3, day4, day5 from calendarname";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$day2 = pg_fetch_result($sel, 0, "day2");
	$day3 = pg_fetch_result($sel, 0, "day3");
	$day4 = pg_fetch_result($sel, 0, "day4");
	$day5 = pg_fetch_result($sel, 0, "day5");
}
if ($day2 == "") {
	$day2 = "法定休日";
}
if ($day3 == "") {
	$day3 = "所定休日";
}
if ($day4 == "") {
	$day4 = "勤務日1";
}
if ($day5 == "") {
	$day5 = "勤務日2";
}
$day6 = "祝日";
$day7 = "年末年始休暇";
$day8 = "買取日";
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function changeDate(dt) {
	var url = 'calendar_year.php';
	url += '?session=<? echo($session); ?>';
	url += '&date=' + dt;
	location.href = url;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="maintenance_menu.php?session=<? echo($session); ?>"><img src="img/icon/b27.gif" width="32" height="32" border="0" alt="マスターメンテナンス"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="./maintenance_menu.php?session=<? echo($session); ?>"><b>マスターメンテナンス</b></a>&nbsp;&gt;&nbsp;<a href="./calendar_name.php?session=<? echo($session); ?>"><b>カレンダー</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="160" valign="top">
<? write_menu_list($session, $fname) ?>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="./img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="calendar_name.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務日</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="calendar_year.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>年間カレンダー</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="calendar_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括設定</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="30"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="calendar_year.php?session=<? echo($session); ?>&date=<? echo($last_year); ?>">&lt;前年</a>&nbsp;<select onchange="changeDate(this.options[this.selectedIndex].value);" style="vertical-align:middle;"><? show_date_options_y($last_year, $date, $next_year, 2); ?></select>&nbsp;<a href="calendar_year.php?session=<? echo($session); ?>&date=<? echo($next_year); ?>">翌年&gt;</a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<? show_calendar($con, $fname, $date, $start_wd, $session); ?>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="4" cellpadding="0" bgcolor="#f6f9ff" style="border:#bdd1e7 solid 1px;">
<tr>
<td width="14%" height="22" nowrap><img src="./img/spacer.gif" width="10" height="17" border="0" style="background-color:<? echo(BGCOLOR_DAYTYPE1); ?>;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通常勤務日</font></td>
<td width="14%" nowrap><img src="./img/spacer.gif" width="10" height="17" border="0" style="background-color:<? echo(BGCOLOR_DAYTYPE2); ?>;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($day4); ?></font></td>
<td width="14%" nowrap><img src="./img/spacer.gif" width="10" height="17" border="0" style="background-color:<? echo(BGCOLOR_DAYTYPE3); ?>;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($day5); ?></font></td>
<td width="14%" nowrap><img src="./img/spacer.gif" width="10" height="17" border="0" style="background-color:<? echo(BGCOLOR_DAYTYPE4); ?>;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($day2); ?></font></td>
<td width="14%" nowrap><img src="./img/spacer.gif" width="10" height="17" border="0" style="background-color:<? echo(BGCOLOR_DAYTYPE5); ?>;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($day3); ?></font></td>
<td width="14%" nowrap><img src="./img/spacer.gif" width="10" height="17" border="0" style="background-color:<? echo(BGCOLOR_DAYTYPE6); ?>;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($day6); ?></font></td>
<td width="14%" nowrap><img src="./img/spacer.gif" width="10" height="17" border="0" style="background-color:<? echo(BGCOLOR_DAYTYPE7); ?>;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($day7); ?></font></td>
<td width="14%" nowrap><img src="./img/spacer.gif" width="10" height="17" border="0" style="background-color:<? echo(BGCOLOR_DAYTYPE8); ?>;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($day8); ?></font></td>
</tr>
</table>
</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
// 年間カレンダーを表示
function show_calendar($con, $fname, $date, $start_wd, $session) {

	list($cate_id, $facility_id) = split("-", $range);

	// 日付配列を作成
	$year = date("Y", $date);
	$arr_year = array();
	for ($i = 1; $i <= 12; $i++) {
		$tmp_month = substr("0$i", -2);
		array_push($arr_year, get_arr_month("$year$tmp_month", $start_wd));
	}

	// 配列を4か月単位に分割
	$arr_year = array_chunk($arr_year, 4);

	$arr_type = get_type($con, $fname, $year);

	// カレンダーを出力
	for ($i = 0; $i <= 2; $i++) {
		echo("<tr>\n");
		for ($j = 0; $j <= 3; $j++) {
			$month = substr("0" . ($i * 4 + $j + 1), -2);
			$day = date("d", $date);
			if (!checkdate($month, $day, $year)) {
				$day = substr("0" . $day - 1, -2);
				if (!checkdate($month, $day, $year)) {
					$day = substr("0" . $day - 1, -2);
				}
			}
			$tmp_date = mktime(0, 0, 0, $month, $day, $year);

			echo("<td valign=\"top\" width=\"25%\">\n");
			echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border:#5279a5 solid 1px;\">\n");
			echo("<tr>\n");
			echo("<td>\n");

			echo("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"0\">\n");
			echo("<tr>\n");
			echo("<td colspan=\"7\" height=\"22\" bgcolor=\"f6f9ff\">\n");
			echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$year/$month</font>\n");
			echo("</td>\n");
			echo("</tr>\n");

			echo("<tr>\n");
			for ($k = 0; $k < 7; $k++) {
				$tmp_wd = $start_wd + $k;
				if ($tmp_wd >= 7) {
					$tmp_wd -= 7;
				}

				switch ($tmp_wd) {
				case 0:
					$show_wd = "日";
					$bgcolor = "#fadede";
					break;
				case 1:
					$show_wd = "月";
					$bgcolor = "#bdd1e7";
					break;
				case 2:
					$show_wd = "火";
					$bgcolor = "#bdd1e7";
					break;
				case 3:
					$show_wd = "水";
					$bgcolor = "#bdd1e7";
					break;
				case 4:
					$show_wd = "木";
					$bgcolor = "#bdd1e7";
					break;
				case 5:
					$show_wd = "金";
					$bgcolor = "#bdd1e7";
					break;
				case 6:
					$show_wd = "土";
					$bgcolor = "#defafa";
					break;
				}

				echo("<td align=\"center\" bgcolor=\"$bgcolor\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$show_wd</font></td>\n");
			}
			echo("</tr>\n");

			foreach ($arr_year[$i][$j] as $arr_week) {
				echo("<tr>\n");

				// 日付セルを出力
				foreach ($arr_week as $tmp_ymd) {
					$tmp_year = substr($tmp_ymd, 0, 4);
					$tmp_month = substr($tmp_ymd, 4, 2);
					$tmp_day = intval(substr($tmp_ymd, 6, 2));
					$tmp_day_url =  substr($tmp_ymd, 6, 2);
					$tmp_date = mktime(0, 0, 0, $tmp_month, $tmp_day, $tmp_year);

					// 他の月の場合は空表示
					if ($tmp_month != $month) {
						echo("<td>&nbsp;</td>\n");

					// 当月の場合は詳細表示
					} else {

						// 祝日名の取得
						$tmp_holiday = ktHolidayName($tmp_date);

						// 勤務日・休日属性を取得
						$tmp_type = $arr_type["$tmp_ymd"];
						// 祝日であれば文字色を変える(属性が未設定か法定か祝日の時を条件追加 20110119）
						if ($tmp_holiday != "" && ($tmp_type == "" || $tmp_type == "4" || $tmp_type == "6")) {
							$font_color = " color=\"red\"";
						} else {
							$font_color = " color=\"blue\"";
						}

						// 勤務日・休日属性によってスタイルを変える
						switch ($tmp_type) {
						case "1":  // 通常勤務日
							$cell_style = " style=\"background-color:" . BGCOLOR_DAYTYPE1 . "\"";
							break;
						case "2":  // 勤務日1
							$cell_style = " style=\"background-color:" . BGCOLOR_DAYTYPE2 . "\"";
							break;
						case "3":  // 勤務日2
							$cell_style = " style=\"background-color:" . BGCOLOR_DAYTYPE3 . "\"";
							break;
						case "4":  // 法定休日
							$cell_style = " style=\"background-color:" . BGCOLOR_DAYTYPE4 . "\"";
							break;
						case "5":  // 所定休日
							$cell_style = " style=\"background-color:" . BGCOLOR_DAYTYPE5 . "\"";
							break;
						case "6":  // 祝日
							$cell_style = " style=\"background-color:" . BGCOLOR_DAYTYPE6 . "\"";
							break;
						case "7":  // 年末年始休暇
							$cell_style = " style=\"background-color:" . BGCOLOR_DAYTYPE7 . "\"";
							break;
						case "7":  // 年末年始休暇
							$cell_style = " style=\"background-color:" . BGCOLOR_DAYTYPE7 . "\"";
							break;
						case "8":  // 買取日
							$cell_style = " style=\"background-color:" . BGCOLOR_DAYTYPE8 . "\"";
							break;
						case "":  // 未登録
							$cell_style = "";
							break;
						}

						echo("<td align=\"center\"$cell_style><a href=\"javascript:void(0);\" onclick=\"window.open('calendar_register.php?session=$session&year=$tmp_year&month=$tmp_month&day=$tmp_day_url', 'newwin', 'width=640,height=480,scrollbars=yes');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"$font_color>$tmp_day</font></a></td>\n");
					}
				}

				echo("</tr>\n");
			}

			echo("</table>\n");

			echo("</td>\n");
			echo("</tr>\n");
			echo("</table>\n");
			echo("</td>\n");
			if ($j < 3) {
				echo("<td><img src=\"img/spacer.gif\" alt=\"\" width=\"5\" height=\"1\"></td>\n");
			}
		}
		echo("</tr>\n");
		if ($i < 2) {
			echo("<tr>\n");
			echo("<td colspan=\"4\"><img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"5\"></td>\n");
			echo("</tr>\n");
		}
	}

}

// 指定年月分の日付配列を返す
function get_arr_month($ym, $start_wd) {

	// 当月1日〜末日の配列を作成
	$year = substr($ym, 0, 4);
	$month = substr($ym, 4, 2);
	$arr_date = array();
	for ($i = 1; $i <= 31; $i++) {
		$day = substr("0" . $i, -2);
		if (checkdate($month, $day, $year)) {
			array_push($arr_date, "$year$month$day");
		} else {
			break;
		}
	}

	// 空白セル分の日付を配列の先頭に追加
	$first_day = mktime(0, 0, 0, $month, "01", $year);
	$empty = date("w", $first_day) - $start_wd;
	if ($empty < 0) {
		$empty += 7;
	}
	for ($i = 1; $i <= $empty; $i++) {
		array_unshift($arr_date, date("Ymd", strtotime("-$i days", $first_day)));
	}

	// 空白セル分の日付を配列の末尾に追加
	$day = substr($arr_date[count($arr_date) - 1], -2);
	$end_day = mktime(0, 0, 0, $month, $day, $year);
	$empty = 7 - (count($arr_date) % 7);
	if ($empty == 7) {
		$empty = 0;
	}
	for ($i = 1; $i <= $empty; $i++) {
		array_push($arr_date, date("Ymd", strtotime("$i days", $end_day)));
	}

	// 週単位に分割した配列を返す
	return array_chunk($arr_date, 7);

}

function get_type($con, $fname, $year) {

	$arr_type = array();

	$start_date = $year."0101";
	$end_date = $year."1231";

	// 勤務日・休日属性を取得
	$sql = "select date, type from calendar";
	$cond = "where date >= '$start_date' and date <= '$end_date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$tmp_date = $row["date"];
		$arr_type["$tmp_date"] = $row["type"];
	}
	return $arr_type;
}
?>
