<?php
ob_start();
require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("show_select_values.ini");
require("referer_common.ini");
require("get_values.ini");
require("jnl_application_imprint_common.ini");
require("./conf/sql.inf");
require_once("jnl_application_common.ini");

$fname = $PHP_SELF;

// セッションのチェック

$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,80,$fname);
if($wkfw=="0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, 81, $fname);

$con = connect2db($fname);

// 職員ID取得
$emp_id=get_emp_id($con,$session,$fname);

require_once('jnl_indicator_facility_data.php');
require_once('jnl_indicator_facility_display.php');

/* @var $indicatorData IndicatorFacilityData */
$indicatorData = new IndicatorFacilityData(array('fname' => $fname, 'con' => $con));
/* @var $indicatorDisplay IndicatorFacilityDisplay */
$indicatorDisplay = new IndicatorFacilityDisplay(array(
    'fname'         => $fname,
    'con'           => $con,
    'session'       => $session,
    'display_year'  => $display_year,
    'display_month' => $display_month,
    'display_day'   => $display_day
));

$display_year  = $indicatorDisplay->getDisplayYear();
$display_year  = !empty($fiscal_year)? $fiscal_year : $display_year;
$display_month = $indicatorDisplay->getDisplayMonth();
$display_day   = $indicatorDisplay->getDisplayDay($display_month, $display_year);
$noArr         = $indicatorData->getNoForNo($no);
$fileNameType  = $noArr['file_name'];
switch ($fileNameType) {
    case 'name_y_m_d':
        $fileName = sprintf('%s_%04d_%02d_%02d.xls', $noArr['name'], $display_year, $display_month, $display_day);
        break;

    case 'name_y_m':
        $fileName = sprintf('%s_%04d_%02d.xls', $noArr['name'], $display_year, $display_month);
        break;

    case 'name_y':
        $fileName = sprintf('%s_%04d.xls', $noArr['name'], $indicatorDisplay->getCurrentYear());
        break;
}

if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
    $encoding = 'sjis';
}
else {
    $encoding = mb_http_output();   // Firefox
}
$fileName = mb_convert_encoding($fileName, $encoding, mb_internal_encoding());
ob_clean();
header("Content-Type: application/vnd.ms-excel");
header(sprintf('Content-Disposition: attachment; filename=%s', $fileName));
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');

$plantArr = $indicatorData->getHospitalList($plant);
if ($plantArr==false) {
    $plantArr = $indicatorData->getHealthFacilities($plant);
}
$dataList = $indicatorData->getDisplayDataList(
    $no,
    $indicatorDisplay->getDateToLastYearStartMonth(),
    $indicatorDisplay->getDateToCurrentYearLastMonth(),
    array('year' => $display_year, 'month' => $display_month, 'day' => $display_day, 'facility_id' => $plant)
);
$download_data = $indicatorDisplay->outputExcelData(
    $no,
    $dataList,
    $plantArr,
    $noArr,
    array('year' => $display_year, 'month' => $display_month, 'day' => $display_day, 'facility_id' => $plant)
);
echo mb_convert_encoding(
    nl2br($download_data),
    'sjis',
    mb_internal_encoding()
);
ob_end_flush();
pg_close($con);
?>