<?
//work_admin_csv2.phpを改造
ob_start();
//ini_set("display_errors","1");
ini_set("max_execution_time", 0);

require_once("about_session.php");
require_once("about_authority.php");
require_once("show_timecard_common.ini");
require_once("show_attendance_pattern.ini");
require_once("holiday.php");
require_once("timecard_common_class.php");
require_once("atdbk_common_class.php");
require_once("date_utils.php");
require_once("work_admin_csv_common.ini");
require_once("atdbk_workflow_common_class.php");
require_once("timecard_bean.php");
require_once("duty_shift_total_common_class.php");
require_once("atdbk_info_common.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("calendar_name_class.php");
require_once("work_admin_csv_download_common.php");
require_once("duty_shift_common_class.php");
require_once("duty_shift_mprint_common_class.php");
require_once("get_values.ini");
require_once("timecard_tantou_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 権限チェック
if ($from_flg == "2") {
    // 勤務シフト作成権限チェック
    $obj = new duty_shift_common_class($con, $fname);
    $chk_flg = $obj->check_authority_user($session, $fname);
    if ($chk_flg == "") {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showLoginPage(window);</script>");
        exit;
    }
}
else {    
    // 勤務管理権限チェック
    $checkauth = check_authority($session, 42, $fname);
    if ($checkauth == "0" && $mmode != "usr") {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }
}
$org_csv_layout_id = $csv_layout_id ;
if ($csv_layout_id == "-") {
	$csv_layout_id = "01";
}

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();
//カレンダー名、所定労働時間履歴用
$calendar_name_class = new calendar_name_class($con, $fname);

//管理帳票用情報取得
if ($from_flg == "2") {
    $obj_mprint = new duty_shift_mprint_common_class($con, $fname);
    $arr_layout_mst = $obj_mprint->get_duty_shift_layout_mst_one($csv_layout_id);
    $print_title = $arr_layout_mst["layout_name"];
    if ($shift_group_id == "-") {
        $group_name = "全て";
    }
    else {
        $group_array = $obj->get_duty_shift_group_array($shift_group_id, "", array());
        $group_name = $group_array[0]["group_name"];
    }
}
// 締め日を取得
// ************ oose update start *****************
//$sql = "select closing from timecard";
$sql = "select closing, closing_parttime, closing_month_flg from timecard";
// ************ oose update end *****************
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$closing = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing") : "";
// ************ oose add start *****************
$closing_parttime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_parttime") : "";
// ************ oose add end *****************
$closing_month_flg = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_month_flg") : "1";
//非常勤が未設定時は常勤を使用する 20091214
if ($closing_parttime == "") {
	$closing_parttime = $closing;
}

// 締め日が未登録の場合はエラーとする
// ************ oose update start *****************
//if ($closing == "") {
if (($closing == "") || ($closing_parttime == "")) {
// ************ oose update end *****************
	echo("<script type=\"text/javascript\">alert('タイムカード締め日が登録されていません。');</script>");
	exit;
}

$org_yyyymm = $yyyymm;

//期間指定 20101209
$start_date = $select_start_yr.$select_start_mon.$select_start_day;
$end_date = $select_end_yr.$select_end_mon.$select_end_day;
$start_date_parttime = $start_date;
$end_date_parttime = $end_date;


if ($yyyymm != "") {
	$year = substr($yyyymm, 0, 4);
	$month = intval(substr($yyyymm, 4, 2));
	$month_set_flg = true; //月が指定された場合
} else {
	$year = date("Y");
	$month = date("n");
	$yyyymm = $year . date("m");
	$month_set_flg = false; //月が未指定の場合
}
// タイムカード設定情報を取得
$sql = "select ovtmscr_tm from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$ovtmscr_tm = pg_fetch_result($sel, 0, "ovtmscr_tm");
}
if ($ovtmscr_tm == "") {$ovtmscr_tm = 0;}

// 対象年月度を変数にセット
$c_yyyymm = substr($start_date, 0, 6);

// 事由ID、表示フラグの配列
$arr_reason_id = $atdbk_common_class->get_add_disp_reason_id();
$arr_disp_flg = array();
for ($i=0; $i<count($arr_reason_id); $i++) {
	$arr_disp_flg[$i] = $atdbk_common_class->get_reason_display_flag($arr_reason_id[$i]);
}
//CSV休日出勤回数出力職種ID取得
$arr_job_id_list = get_job_id_list($con, $fname, $csv_layout_id, $from_flg);

//グループ指定group_idの有無により見出し出力を制御
// 指定あり、所属グループ分は見出しあり、応援追加は見出しなし
// 指定なし、どちらも見出しなし

if ($mmode == "usr") {
	$emp_id = get_emp_id($con, $session, $fname);
	$timecard_tantou_class = new timecard_tantou_class($con, $fname);
	$timecard_tantou_branches = $timecard_tantou_class->get_tantou_branches($emp_id);
}
//メニュー画面からの場合
if ($emp_id_list == "") {
	//レイアウト別に設定 20101102
    $arr_config = get_timecard_csv_config($con, $fname, $org_csv_layout_id, $from_flg);
	$header_flg = $arr_config["csv_header_flg"];
	$csv_ext_name = $arr_config["csv_ext_name"];

    if ($from_flg != "2") {
        $arr_emp_id_layout = get_arr_emp_id_from_layout_id($con, $fname, $srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $shift_group_id, $org_csv_layout_id, $arr_config, "", $srch_id, $sus_flg);
    }
    else {
        $arr_emp_id_layout = get_arr_emp_id_for_mprint($con, $fname, $shift_group_id, $arr_config, 0, 0, $start_date, $end_date, $total_flg);
    }
	
	$arr_csv_list = get_csv_list($con, $fname, $csv_layout_id, $from_flg);
} else {
	//$arr_csv_list = explode(",", $csv_list); //20140224 256件超対応、DBから取得に変更
    $arr_csv_list = get_csv_list($con, $fname, $csv_layout_id, $from_flg);
    for($i=1; $i<=5; $i++) {
		$idx = $i - 1;
		$varname = "sortkey".$i;
		$arr_config[$varname] = $$varname;
	}
//画面から指定された職員IDを並び替える
    $arr_emp_id = get_arr_emp_id_from_layout_id($con, $fname, "", "", "", "", "", "", "", "", "", $arr_config, $emp_id_list, "", $sus_flg);
	$emp_id_list = join(",", $arr_emp_id);
    //$emp_id_list = join(",", $emp_id);
    
}

//CSVヘッダーフラグ
if ($header_flg == "") {
	$header_flg = "f";
}
//レイアウト画面からの場合
if ($emp_id_list != "") {
	if ($zerodata_no_out_flg == "") {
		$zerodata_no_out_flg = "f";
	}
	if ($fraction_flg == "") {
		$fraction_flg = "f";
	}
	//レイアウト別設定 20101102
	$sql = "select * from timecard_csv_config";
	$cond = "where layout_id = '$csv_layout_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	if (pg_num_rows($sel) > 0) {
		$sql = "update timecard_csv_config set";
		$set = array("csv_header_flg", "csv_ext_name", "sortkey1", "sortkey2", "sortkey3", "sortkey4", "sortkey5", "zerodata_no_out_flg", "fraction_flg");
		$setvalue = array($header_flg, $csv_ext_name, $sortkey1, $sortkey2, $sortkey3, $sortkey4, $sortkey5, $zerodata_no_out_flg, $fraction_flg);
		$cond = "where layout_id = '$csv_layout_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	} else {
		
		$sql = "insert into timecard_csv_config (csv_header_flg, csv_ext_name, layout_id, sortkey1, sortkey2, sortkey3, sortkey4, sortkey5, zerodata_no_out_flg, fraction_flg";
		$sql .= ") values (";
		
		$content = array($header_flg, $csv_ext_name, $csv_layout_id, $sortkey1, $sortkey2, $sortkey3, $sortkey4, $sortkey5, $zerodata_no_out_flg, $fraction_flg);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}	
}
// 月集計情報をCSV形式で取得
// ************ oose update start *****************
//$csv = get_attendance_book_csv($con, $emp_id_list, $start_date, $end_date, $c_yyyymm, $ovtmscr_tm, $fname);
$csv = get_attendance_book_csv($con, $emp_id_list,
        $start_date, $end_date,
        $start_date_parttime, $end_date_parttime,
        $c_yyyymm, $ovtmscr_tm, $fname, $arr_reason_id, $arr_disp_flg,
        $group_id, $atdbk_common_class, $arr_csv_list, $header_flg, $arr_job_id_list, $closing, $closing_month_flg, $arr_emp_id_layout, $timecard_bean, $shift_group_id, $csv_layout_id, $obj_hol_hour, $calendar_name_class, $from_flg, "1", $total_flg, $org_yyyymm);
// ************ oose update end *****************
//echo $csv;

// データベース接続を閉じる
pg_close($con);

//ダウンロード時データ存在チェック
if ($emp_id_list == "") {
	$exist_flg = true;
	if ($header_flg == "f") {
		if ($csv == "") {
			$exist_flg = false;
		}
	} else {
		$arr_chk = explode("\n", $csv);
		if ($arr_chk[1] == "") {
			$exist_flg = false;
		}
	}
	if ($exist_flg == false) {
		echo("<script type=\"text/javascript\">alert('条件に合う職員を検索できませんでした');</script>");
		exit;
	}
}

// CSVを出力
// ファイル名を、「年月.拡張子」から「開始日_終了日.拡張子」に変更する(期間を画面から変更できるため) 20130204
if ($from_flg == "2") {
    $file_name = $start_date."_".$end_date."_".$print_title;
    //「全て」でない場合
    if ($shift_group_id != "-") {
        $file_name .= "_".$group_name;
    }
    $file_name .= ".".$csv_ext_name;

    $ua = $_SERVER['HTTP_USER_AGENT'];
    $file_name = mb_convert_encoding($file_name, 'UTF-8', mb_internal_encoding());
    
    if(!preg_match("/safari/i",$ua)){
        header('Content-Type: application/octet-stream');
    }
    else {
        header('Content-Type: application/vnd.ms-excel');
    }
    
    if (preg_match("/MSIE/i", $ua)) {
        if (strlen(rawurlencode($file_name)) > 21 * 3 * 3) {
            $file_name = mb_convert_encoding($file_name, "SJIS-win", "UTF-8");
            $file_name = str_replace('#', '%23', $file_name);
        }
        else {
            $file_name = rawurlencode($file_name);
        }
    }
	elseif (preg_match("/Trident/i", $ua)) {// IE11
	    $file_name = rawurlencode($file_name);
	}
    elseif (preg_match("/chrome/i", $ua)) {// Google Chromeには『Safari』という字が含まれる
    }
    elseif (preg_match("/safari/i", $ua)) {// Safariでファイル名を指定しない場合
        $file_name = "";
    }
    
}
else {
    $file_name = $start_date."_".$end_date.".".$csv_ext_name;
}
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

//関数をwork_admin_csv_download_common.phpへ移動


?>
