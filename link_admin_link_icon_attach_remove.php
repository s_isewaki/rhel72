<?php
/*************************************************************************
リンクライブラリ管理
  リンク名表示設定・アイコン削除
*************************************************************************/
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック(リンクライブラリ管理[61])
$checkauth = check_authority($session, 61, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

// リンクIDチェック
if (empty($link_id)) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// DBを更新
$sql = "update link set";
$set = array("icon");
$setvalue = array("");
$cond = "where link_id=$del_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");
pg_close($con);
?>
<script type="text/javascript">
location.href = 'link_admin_link_setting.php?linkcate_id=<? eh($linkcate_id);?>&session=<? eh($session); ?>';
</script>