<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$dept = check_authority($session,59,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$con=connect2db($fname);
if($con==0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

if($trashbox==""){
	echo("<script language='javascript'>alert(\"チェックボックスをオンにしてください。\");</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
}


//共通SQL部品
$in=" in (";
$count=count($trashbox);
for($i=0;$i<$count;$i++){
	if(!$trashbox[$i]==""){
		$in.="'$trashbox[$i]'";
		if($i<($count-1)){
			$in.=",";
		}
	}
}
$in.=")";

//既にその診療区分が使用されているかのチェック
//※summaryテーブルとは診療区分名でリンクしている為、使用されている場合は診療項目名を変更できない。
$tbl = "select count(*) from summary left join divmst on divmst.diag_div = summary.diag_div ";
$cond = "where divmst.div_id $in";
$sel = select_from_table($con,$tbl,$cond,$fname);
if($sel == 0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	pg_close($con);
	exit;
}
$count = pg_result($sel,0,"count");
if($count > 0){
	echo("<script language=\"javascript\">alert(\"この診療区分は既に使用されているため削除できません。\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	pg_close($con);
	exit;
}

//divmstから削除
$delete_status="delete from divmst where div_id $in";
$result_delete=pg_exec($con,$delete_status);
if($result_delete==false){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	pg_close($con);
	exit;
}

//tmpl_div_linkからも削除
$delete_status="delete from tmpl_div_link where div_id $in";
$result_delete=pg_exec($con,$delete_status);
if($result_delete==false){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	pg_close($con);
	exit;
}

pg_close($con);


//画面遷移
echo("<script language='javascript'>location.href=\"./summary_kanri_kubun.php?session=$session\";</script>");
exit;








?>