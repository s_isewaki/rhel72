<?
$THIS_WORKSHEET_ID = 1;
ob_start();
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("sot_util.php");
require_once("summary_common.ini");
require_once("get_values.php");
require_once("get_menu_label.ini");
ob_end_clean();


//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $_SERVER["PHP_SELF"];
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
	echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
	exit;
}
$emp_id = get_emp_id($con, $session, $fname);//ログイン職員ＩＤ


//=================================================
// 検索対象日付がない場合は本日とする
//=================================================
$date_y1 = (int)(@$_REQUEST["date_y1"] ? $_REQUEST["date_y1"] : date("Y"));
$date_m1 = (int)(@$_REQUEST["date_m1"] ? $_REQUEST["date_m1"] : date("m"));
$date_d1 = (int)(@$_REQUEST["date_d1"] ? $_REQUEST["date_d1"] : date("d"));
if (!checkdate($date_m1, $date_d1, $date_y1)) list($date_y1, $date_m1, $date_d1) = split("/", date("Y/m/d", strtotime("today")));
$from_yyyymmdd = $date_y1. ((int)@$date_m1 <10 ? "0".(int)@$date_m1 : $date_m1) . ((int)@$date_d1 <10 ? "0".(int)@$date_d1 : $date_d1);

$date_y2 = (int)(@$_REQUEST["date_y2"] ? $_REQUEST["date_y2"] : date("Y"));
$date_m2 = (int)(@$_REQUEST["date_m2"] ? $_REQUEST["date_m2"] : date("m"));
$date_d2 = (int)(@$_REQUEST["date_d2"] ? $_REQUEST["date_d2"] : date("d"));
if (!checkdate($date_m2, $date_d2, $date_y2)) list($date_y2, $date_m2, $date_d2) = split("/", date("Y/m/d", strtotime("today")));
$to_yyyymmdd = $date_y2. ((int)@$date_m2 <10 ? "0".(int)@$date_m2 : $date_m2) . ((int)@$date_d2 <10 ? "0".(int)@$date_d2 : $date_d2);


$sel_team_id = (int)@$_REQUEST["sel_team_id"];
if (!$sel_bldg_cd) $sel_bldg_cd = 1;

$sql = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst where ptif_id = '".pg_escape_string(@$_REQUEST["sel_ptif_id"])."'";
$sel = select_from_table($con, $sql, "", $fname);
$ptif_name = trim(@pg_fetch_result($sel, 0, "ptif_lt_kaj_nm") . " " . @pg_fetch_result($sel, 0, "ptif_ft_kaj_nm"));

$nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, @$_REQUEST["sel_ptif_id"], $from_yyyymmdd);
$ptif_ward_cd = @$nyuin_info["ward_cd"];
$ward_name = @$nyuin_info["ward_name"];



//=================================================
// 印刷対象患者のリストアップ
// 患者が指定されていない場合は、検索範囲から以下のリストアップ
// ・対象期間内に
// ・指定病棟に入院していること
//=================================================
$pt_list = array();
if (@$_REQUEST["sel_ptif_id"]) $pt_list[] = @$_REQUEST["sel_ptif_id"];
else {
    $sql =
        " select inpt.ptif_id, inpt.inpt_bed_no, ptrmmst.ptrm_name from" .
        "   (select ptif_id, ptrm_room_no, inpt_bed_no, bldg_cd, ward_cd from inptmst" .
        "    where bldg_cd = " . (int)@$_REQUEST["sel_bldg_cd"] .
        "    and ward_cd = " . (int)@$_REQUEST["sel_ward_cd"] .
        "    and inpt_in_dt <= '".(int)$to_yyyymmdd."'" .
        "    union" .
        "    select ptif_id, ptrm_room_no, inpt_bed_no, bldg_cd, ward_cd from inpthist" .
        "    where bldg_cd = " . (int)@$_REQUEST["sel_bldg_cd"] .
        "    and ward_cd = " . (int)@$_REQUEST["sel_ward_cd"] .
        "    and inpt_in_dt is not null and inpt_in_dt <> ''" .
        "    and (" .
        "      (".
        "        inpt_in_dt <= '" . (int)$from_yyyymmdd . "'" .
        "        and inpt_out_dt >= '" . (int)$from_yyyymmdd . "'" .
        "      )".
        "      or".
        "      (".
        "        inpt_in_dt <= '" . (int)$to_yyyymmdd . "'" .
        "        and (inpt_out_dt is null or inpt_out_dt >= '" . (int)$to_yyyymmdd . "')" .
        "      )" .
        "    )" .
        "   ) inpt" .
        " left join ptrmmst" .
        " on (inpt.ptrm_room_no = ptrmmst.ptrm_room_no and inpt.bldg_cd = ptrmmst.bldg_cd and inpt.ward_cd = ptrmmst.ward_cd)" .
        " order by ptrmmst.ptrm_name, inpt.inpt_bed_no";

    $sel = select_from_table($con, $sql, "", $fname);
    while ($row = pg_fetch_array($sel)){
        if (in_array($row["ptif_id"], $pt_list, true)) {
            continue;    // 患者IDの重複回避(期間内に同一患者が複数回入院していた場合、重複して取得されるので)
        }
        $pt_list[] = $row["ptif_id"];
    }
}


//=================================================
// 印刷実行要求がなければ、範囲選択HTMLを作成して終了
//=================================================
if (!$_REQUEST["request_print_execute"] || !is_array($pt_list)){
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 温度板印刷</title>
<link rel="stylesheet" type="text/css" href="css/main_summary.css" />
<? require_once("yui_calendar_util.ini"); ?>
<? write_yui_calendar_use_file_read_0_12_2(); ?>
<? write_yui_calendar_script2(2); ?>
<?
	$bldg_list = array();
	$bldg_data_options = array();
	$bldg_data_options[] = $EMPTY_OPTION;
	$ward_list = array();
	$ward_data_options = array();
	$ward_data_options[] = $EMPTY_OPTION;

	// 事業所と病棟選択肢
	$sql =
		" select bldg.bldg_name, bldg.bldg_cd, wd.ward_cd, wd.ward_name from wdmst wd".
		" inner join bldgmst bldg on (bldg.bldg_cd = wd.bldg_cd)".
		" where wd.ward_del_flg = 'f'".
		" and bldg.bldg_del_flg = 'f'".
		" order by bldg.bldg_cd, wd.ward_cd";
	$sel = $c_sot_util->select_from_table($sql);
	while($row = pg_fetch_array($sel)){
		$selected1 = ($row["bldg_cd"] == $sel_bldg_cd ? " selected" : "");
		if (!@$bldg_list[$row["bldg_cd"]]){
			$bldg_list[$row["bldg_cd"]] = $row["bldg_name"];
			$bldg_data_options[] = '<option value="'.$row["bldg_cd"].'"'.$selected1.'>'.$row["bldg_name"].'</option>';
		}

		$ward_list[$row["bldg_cd"]][$row["ward_cd"]] = $row["ward_name"];
		if ($selected1){
			$selected2 = ($row["ward_cd"] == $sel_ward_cd ? " selected" : "");
			$ward_data_options[] = '<option value="'.$row["ward_cd"].'"'.$selected2.'>'.$row["ward_name"].'</option>';
		}
	}
?>
<script type="text/javascript">
	var bldg_list = {};
<? foreach ($bldg_list as $bldg_cd => $bldg_name){ ?>
		bldg_list["<?=$bldg_cd?>"] = [];
	<? foreach ($ward_list[$bldg_cd] as $ward_cd => $ward_name){ ?>
			bldg_list["<?=$bldg_cd?>"].push({"ward_cd":"<?=$ward_cd?>","ward_name":"<?=str_replace('"','\"',$ward_name)?>"});
	<? } ?>
<? } ?>
	function bldgChanged(bldg_id){
		var dropdown = document.getElementById("sel_ward_cd");
		dropdown.options.length = bldg_list[bldg_id].length + 1;
		for (var i = 0; i < bldg_list[bldg_id].length; i++){
			dropdown.options[i+1].value = bldg_list[bldg_id][i].ward_cd;
			dropdown.options[i+1].text  = bldg_list[bldg_id][i].ward_name;
		}
		dropdown.selectedIndex = 0;
	}

	function popupKanjaSelect(){
		window.open(
			"sot_kanja_select.php?session=<?=$session?>&js_callback=callbackKanjaSelect&sel_bldg_cd=1&sel_ward_cd=<?=@$ptif_ward_cd?>&sel_ptif_id=<?=@$sel_ptif_id?>&sel_sort_by_byoto=1",
			"kanjaSelectWindow",
			"directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,width=630,height="+window.screen.availHeight+",left=0,top=0"
		);
	}
	function startPrint(){
		var wardidx = document.getElementById("sel_ward_cd").options.selectedIndex;
		var wardcd = document.getElementById("sel_ward_cd").options[wardidx].value;
		if (wardcd=="" && document.getElementById("sel_ptif_id").value=="") {
			alert("病棟または患者を指定してください。");
			return;
		}
		if (!confirm("印刷PDF作成処理を開始します。\nこの処理には時間がかかる場合があります。\n\nPDF作成処理を開始してよろしいですか？")) return;
		document.search.request_print_execute.value = "1";
		document.search.submit();
	}

	function callbackKanjaSelect(ptif_id, ptif_name){
		document.getElementById("sel_ptif_id").value = ptif_id;
		document.getElementById("sel_ptif_name").value = ptif_name;
	}
	function clearKanja(){
		document.getElementById("sel_ptif_id").value = "";
		document.getElementById("sel_ptif_name").value = "";
	}
	<? if ($_REQUEST["request_print_execute"]){ ?>
	alert("指定した印刷条件では、印刷対象患者は存在しませんでした。");
	<? } ?>
</script>
</head>
<body onload="initcal();">
<center>
<div style="width:590px; margin-top:4px">

<!-- タイトルヘッダ -->
<?= summary_common_show_dialog_header("温度板印刷"); ?>

<!-- 検索条件エリア -->
<form name="search" action="sot_worksheet_ondoban_print.php" method="get">
<input type="hidden" name="searching" value="1">
<input type="hidden" name="request_print_execute" value="">
<input type="hidden" name="session" value="<?= $session ?>">
<input type="submit" value="dummy" style="display:none"/>
<div class="dialog_searchfield">
	<table style="width:100%">
		<tr>
			<td style="white-space:nowrap">
				<div style="margin-top:4px;">事業所(棟)</div>
			</td>
			<td>
				<div style="float:left; margin-top:2px">
					<select onchange="bldgChanged(this.value);" name="sel_bldg_cd"><?=implode("\n", $bldg_data_options)?></select>
				</div>
				<div style="float:left; margin-top:4px; margin-left:4px">病棟</div>
				<div style="float:left; margin-top:2px; margin-left:4px">
					<select name="sel_ward_cd" id="sel_ward_cd"><option value=""></option><?=implode("\n", $ward_data_options)?></select>
				</div>
				<div style="clear:both"></div>
			</td>
			<td></td>
		</tr>
		<tr>
			<td style="white-space:nowrap">
				<div style="margin-top:4px;">期間</div>
			</td>
			<td>
				<table>
				<tr>
					<td>
						<select id="date_y1" name="date_y1"><? show_select_years(10, $date_y1, true); ?></select>/<select id="date_m1" name="date_m1"><? show_select_months($date_m1, true); ?></select>/<select id="date_d1" name="date_d1"><? show_select_days($date_d1, true); ?></select>
					</td>
					<td>
					  <img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
					    <div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
					</td>
					<td>
						&nbsp;〜<select id="date_y2" name="date_y2"><? show_select_years(10, $date_y2, true); ?></select>/<select id="date_m2" name="date_m2"><? show_select_months($date_m2, true); ?></select>/<select id="date_d2" name="date_d2"><? show_select_days($date_d2, true); ?></select>
					</td>
					<td>
				  	<img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal2()"/><br>
							<div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
					</td>
				</tr>
				</table>
			</td>
			<td></td>
		</tr>
		<tr>
			<td style="width:10%">患者ID</td>
			<td>
				<div style="float:left">
					<input type="text" id="sel_ptif_id" name="sel_ptif_id" style="width:90px; ime-mode:active; background-color:#d9edfd; border:1px solid #aaa; padding:2px" value="<?=@$_REQUEST["sel_ptif_id"]?>" readonly="readonly"/><!--
			--><input type="text" id="sel_ptif_name" name="sel_ptif_name" style="width:120px; ime-mode:active; background-color:#d9edfd; border:1px solid #aaa; padding:2px" value="<?=@$_REQUEST["sel_ptif_name"]?>" readonly="readonly" /><!--
			--><input type="button" onclick="popupKanjaSelect();" value="患者検索"/><input type="button" onclick="clearKanja();" value="患者クリア"/>
				</div>
				<div style="clear:both"></div>
			</td>
			<td style="text-align:right; padding-right:8px">
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<input type="radio" name="print_side" value="one">片面印刷&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="print_side" value="both" checked>両面印刷
			</td>
		</tr>
		<tr>
			<td colspan="3">
			<div style="background-color:#dbeefd; border:1px solid #aaa; margin:4px; padding:4px">
			※ 病棟または患者を指定してください。<br/>
			※ 患者を指定しない場合は、期間内・指定病棟に入院していた患者情報が印刷されます。<br/>
			※ 患者を指定した場合、指定病棟情報は無視されます。<br/>
			<br/>
			※ 印刷用PDF作成には時間がかかる場合があります。<br/>
			　 PDF作成処理完了後は、ダウンロードダイアログが表示されます。<br/>
			<br/>
			※ 指定事業所や指定期間に印字情報がない場合は、空のファイルが作成されます。<br/>
			</div>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="text-align:right; padding-right:8px">
				<input type="button" onclick="startPrint();" value="印刷開始"/>
			</td>
		</tr>
	</table>
</div>
</form>
</div>
</center>
</body>
</html>
<?
	die;
}
//=================================================
// HTMLここまで
//=================================================

//*************************************************************************
// PDFここから（その１）
//*************************************************************************
// PDF作成については以下を参照しました。
// http://www.phpbook.jp/fpdf/index.html
require_once('fpdf153/mbfpdf.php');

//=================================================
// PDF生成ライブラリの拡張クラス定義
// ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
//=================================================
class CustomMBFPDF extends MBFPDF {

  var $currentPageNo;       // 現在描画しているページの番号
  var $kansatuLineAh;       // 観察項目行の高さ累計
  var $kansatuYs = array(); // 観察項目ヘッダーを出力するY位置、(0,1)=(1ページ目,2ページ目以降)
  var $kansatuListCount = 0;
  var $ptpno = 0;
  
  var $M          = 0.3 ;
  var $MOST_LEFT  =  15 ;
  var $MOST_RIGHT = 200 ;
  var $MOST_TOP   =  10 ;
  var $MOST_BTM   = 282 ;
  

	// ■の描画
	function DotSquare1($x0,$y0,$clr="") {
		$this->setXY($x0-0.1, $y0-0.1);
		$lw = $this->LineWidth;
		$this->_out('2.5 w'); // 線幅
		if (!$clr) $this->SetDrawColor(27, 156, 171);
		if ($clr=="sanso") $this->SetDrawColor(128, 100, 162); // 紫
		if ($clr=="sinden") $this->SetDrawColor(155, 187, 89); // 草色
		if ($clr=="kokyuki") $this->SetDrawColor(255, 145, 85); // みかん色
		$this->Cell(0.2, 0.2, "",1,0);
		$this->_out(sprintf('%.2f w', $lw)); // 線幅を戻す
		$this->SetDrawColor(0); // 色を戻す
	}

	// □の描画
	function DotSquare2($x0,$y0,$clr="") {
		$this->_out('0.2 w'); // 線幅
		$this->_out('1 1 1 rg'); // 白色
		$this->Rect($x0-0.7, $y0-0.7, 1.5, 1.5 , "F");
		if (!$clr) $this->SetDrawColor(0, 0, 255); // 青
		if ($clr=="sanso") $this->SetDrawColor(128, 100, 162); // 紫
		if ($clr=="sinden") $this->SetDrawColor(155, 187, 89); // 草色
		if ($clr=="kokyuki") $this->SetDrawColor(255, 145, 85); // みかん色
		$this->Rect($x0-0.7, $y0-0.7, 1.5, 1.5 , "");
		$this->SetDrawColor(0); // 色を戻す
		$this->_out(sprintf('%.2f w', $lw)); // 線幅を戻す
	}

	// ＋の描画
	function DotCross($x0,$y0) {
		$lw = $this->LineWidth;
		$this->SetDrawColor(0, 136, 34);
		$this->_out('0.3 w'); // 線幅
		$this->Line($x0, $y0-0.7, $x0, $y0+0.7);
		$this->Line($x0-0.7, $y0, $x0+0.7, $y0);
		$this->_out(sprintf('%.2f w', $lw)); // 線幅を戻す
		$this->SetDrawColor(0); // 色を戻す
	}

	// バツの描画
	function DotBatsu($x0,$y0) {
		$lw = $this->LineWidth;
		$this->SetDrawColor(0, 0, 0);
		$this->_out('0.3 w'); // 線幅
		$this->Line($x0-0.7, $y0-0.7, $x0+0.7, $y0+0.7);
		$this->Line($x0-0.7, $y0+0.7, $x0+0.7, $y0-0.7);
		$this->_out(sprintf('%.2f w', $lw)); // 線幅を戻す
		$this->SetDrawColor(0); // 色を戻す
	}

	// ●の描画
	function DotCircle($x0,$y0) {
		$w1 = 4;
		$y1 = ($this->h-($y0)) * $this->k + 2;
		$x1 = ($x0) *  $this->k;
		$lw = $this->LineWidth;
		$fc = $this->FillColor;
		$this->_out('0.1 w'); // 線幅
		$this->_out('1 0 0 rg'); // 赤色、ぬりつぶし色
		$r = 0.66; // 真円のレート
		$this->_out(sprintf('%.2f %.2f m', $x1, $y1)); // 開始点に移動
		$this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c', ($x1+$w1*$r), $y1, ($x1+$w1*$r), ($y1-$w1), $x1, $y1-$w1)); // ベジエ曲線で半円描画
		$this->_out('f'); // 塗りつぶしセット
		$this->_out(sprintf('%.2f %.2f m', $x1, $y1)); // 開始点に移動
		$this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c', ($x1-$w1*$r), $y1, ($x1-$w1*$r), ($y1-$w1), $x1, $y1-$w1)); // ベジエ曲線で半円描画
		$this->_out('f'); // 塗りつぶしセット
		$this->_out(sprintf('%.2f w', $lw)); // 線幅を戻す
		$this->SetDrawColor(0); // 色を戻す
	}

	// 文字列を設定して、利用した領域の高さを返す
	function Plot($x, $y, $cellWidth, $txt, $defaultHeight = 4){
		$txt = trim($txt);
		if (!$y) $y = $this->GetY();
		if ($txt=="") return 0;
		$fsize = 8.5;// 標準フォントサイズ8とする
		$minsize = 5;
		$rate = 0;
		$wmax = $cellWidth - 2*$this->cMargin; // 実質描画可能幅
		// 収まるサイズにフォントを縮小する
		while ($fsize >= $minsize && $rate < 1.1){
			$fsize = $fsize - 0.5;
			$this->SetFontSize($fsize);
			$w = $this->GetStringWidth($txt);
			$rate = $wmax / $w;
		}
		$h = $defaultHeight - (8-$fsize)*0.5;
		$this->SetXY($x, $y);
		$this->MultiCell($cellWidth, $h, $txt, 0, 'C');
		$ret = $this->GetY() - $y;
		if ($ret > $defaultHeight) $ret = $ret + 0.5;
		return $ret;
	}

  // ページ共通のヘッダー部を出力する
  function writeHeader($most_left, $ward_name, $ptrm_name, $pt_id, $ptif_name, $lpos, $fwidth, $care) {
    $this->setXY(0, 10);
    $this->SetFont(PMINCHO,'',10);
    $this->Cell(0, 5.0, "個人情報用紙",'',0,'C');
    $this->setXY(0, 10);
    //	$this->Cell(0, 5.0, $date_m1."月　",'',0,'R');
    $this->setXY(0, 15);
    $this->SetFont(PMINCHO,'', 8);
    $this->Cell(0, 5.0, date("Y/m/d H:i:s")."　　",'',0,'R');

    $this->setXY($most_left, 20);
    $this->SetFont(PMINCHO,'',9);
    $this->Cell(0, 5.0, "病棟/病室：".$ward_name."　".$ptrm_name."　　患者：".$pt_id."　".$ptif_name,'',0,'L');

    //==================
    // 日付
    //==================
//    $lineHeight = 4;
    for ($i = 1; $i <= 7; $i++){
      $this->Plot($lpos[$i], 25, $fwidth, str_replace("）",")", str_replace("（"," (", @$care[$i]["date"])));
    }
  }

	// 日勤夜勤タイトルヘッダーを出力する
  function nitiyaHeader($x1, $x2, $y, $M, $lw, $lpos) {
    $lineHeight = 3;
    $this->SetLineWidth($lw);
    $this->Line($x1, $y - $M, $x2, $y - $M);
    $this->SetLineWidth(0.2);
    for ($i = 1; $i <= 7; $i++){
      $lineHeight = max($lineHeight, $this->Plot($lpos[$i],      $y, 10, "日勤", 3));
      $lineHeight = max($lineHeight, $this->Plot($lpos[$i] + 10, $y, 10, "夜勤", 3));
    }
    for ($i = 1; $i <= 7; $i++){
      $this->Line($lpos[$i] + 10, $y - $M,  $lpos[$i] + 10, $y + $lineHeight - $M);
    }
    return ($y + $lineHeight);
  }

	// 観察項目ヘッダーを出力する
  function kansatuHeader($x, $y1, $y2, $M, $most_top) {
    $yS = $this->currentPageNo < 2 ? $this->kansatuYs[0]: $this->kansatuYs[1] ;
    $this->SetXY($x + 0.6, $yS);
    $this->SetFontSize(8.0);
    $height = ($y2 - $y1) / 4.0;
    $this->MultiCell(6, $height, "観察項目", 0, 'L');    // 位置をセンターにすると縦位置がずれるので左寄せに設定
    $this->Line($x + 6, $y1 - $M, $x + 6, $y2 - $M); // 縦線は常に引く
  }

	// 改ページする
  function AddPage2() {
    $this->AddPage() ;
    $this->currentPageNo++;
    $this->kansatuLineAh = 0;
    $this->ptpno++;
  }

  // 指定範囲を白で塗りつぶす
  function fillWhite($x, $y, $w, $h) {
    $this->SetFillColor(255, 255, 255);
    $this->Rect($x, $y, $w, $h, "F");
  }

  // 縦罫線を描く
  function verticalLine($lpos, $y1, $y2) {
//    $this->SetLineWidth(0.2);
    for ($i = 1; $i < count($lpos); $i++){
      $this->Line($lpos[$i], $y1, $lpos[$i], $y2);
    }
  }

  // 外周罫線を描く
  function rectLine($x1, $y1, $x2, $y2) {
    $this->SetDrawColor(0);
//    $this->SetLineWidth(0.2);
    $this->Line($x1, $y1, $x2, $y1);
    $this->Line($x2, $y1, $x2, $y2);
    $this->Line($x2, $y2, $x1, $y2);
    $this->Line($x1, $y2, $x1, $y1);
  }


  //｢EUC-JP → SJIS｣変換のためのオーバーライド
  //MultiCell()もWrite()もこのCell()で表示しているので、オーバーライドはCell()だけでOK
  function Cell($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = 0, $link = '')
  {
    $enc = mb_internal_encoding();
    if($enc != "sjis-win"){
      $txt = mb_convert_encoding($txt, "sjis-win", $enc) ;
      parent::Cell($w, $h, $txt, $border, $ln, $align, $fill, $link) ;
    }
  }

  // フッター
  function Footer()
  {
     $this->SetFontSize(9) ;
     $this->Cell(0, 5, "以下余白", 0, 1, "R");
     $this->setXY($this->MOST_LEFT, $this->MOST_BTM + 3) ;
     $this->Cell(0, 3, "- " . $this->ptpno . " -", '', 0, 'C') ;
  }
}
//=================================================
// ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
// PDF生成ライブラリの拡張クラス定義
//=================================================
$resetPageNoFlg = false;    // ページ番号リセットフラグ
$pdf = new CustomMBFPDF();
//  $pdf->AddMBFont(PMINCHO,'EUC-JP');
$pdf->AddMBFont(PMINCHO,'SJIS');
//$pdf->SetAutoPageBreak(true, 5);
$pdf->SetAutoPageBreak(false);
$pdf->Open();

// 横サイズ 195-15 = 180
// １列目：8x5 = 40
// ２列目以降の７列：20 * 7 = 140
// 行高；4
// グラフ高さ

$M = 0.3;
$most_right = 195;
$most_left = 15;
$most_top = 25;
$most_btm = 290;
$hwidth = 40;
$fwidth = 20;
$tmpw = $most_left + $hwidth;
$lpos = array($most_left, $tmpw, $tmpw+($fwidth*1), $tmpw+($fwidth*2), $tmpw+($fwidth*3), $tmpw+($fwidth*4), $tmpw+($fwidth*5), $tmpw+($fwidth*6));
$graphHeight = 60;
$graphWidth = $fwidth * 7;
$graphStartLeft = $most_left + $hwidth;
$widthPerMinute = $graphWidth / (60*24*7);
$graphStartTop = 30;

//*************************************************************************
// PDFここまで（その１）
//*************************************************************************
$print_side = isset($_REQUEST["print_side"]) ? $_REQUEST["print_side"] : '';    // 印刷面の種類(片面印刷[one]/両面印刷[both])を取得
foreach($pt_list as $idx => $pt_id) {    // 患者毎にループ
  $pdf->kansatuLineAh = 0;
  $pdf->kansatuYs[1]  =  $most_top + 6.5;
  $prstY = $pdf->GetY();

	$sql = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst where ptif_id = '".pg_escape_string($pt_id)."'";
	$sel = select_from_table($con, $sql, "", $fname);
	$ptif_name = trim(@pg_fetch_result($sel, 0, "ptif_lt_kaj_nm") . " " . @pg_fetch_result($sel, 0, "ptif_ft_kaj_nm"));

	$nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, $pt_id, $to_yyyymmdd);
	$ptif_ward_cd = @$nyuin_info["ward_cd"];
	$ward_name = @$nyuin_info["ward_name"];
	$ptrm_name = @$nyuin_info["ptrm_name"];
	$start_yyyymmdd = $from_yyyymmdd;

for(;;) {
    $pdf->currentPageNo = 0;
    
	// 期間外データの場合はこの患者のループを抜ける
	if ($start_yyyymmdd > $to_yyyymmdd) break;

	//=================================================
	// 対象日付リストを作成する。指定日をひだり端にして七日間。
	// $ymdには "[1](６日前)" 〜 "[7](指定日)" まで格納
	//=================================================
	$ymd = array();
	$care = array();
	$weeks = array("日", "月", "火", "水", "木", "金", "土");
	$slashed_target_ymd = $c_sot_util->slash_yyyymmdd($start_yyyymmdd);

	$yesterday = strtotime($slashed_target_ymd." -1 day");
	$d = date("Ymd", $yesterday);
	$slashed_yesterday_ymd = $c_sot_util->slash_yyyymmdd(date("Ymd", $yesterday));

	for ($i = 0; $i <= 8; $i++){
		$d = date("Ymd", strtotime($slashed_yesterday_ymd." +".$i." day"));
		if ($i==8){
			$start_yyyymmdd = $d;
			break;
		}
		$week = date("w", strtotime($slashed_yesterday_ymd." +".$i." day"));
		$ymd[$d] = $i;
		$care[$i]["yyyymmdd"] = $d;
		$care[$i]["date"] = substr($c_sot_util->slash_ymd($d), 5) . "（".$weeks[$week]."）"; // 月日と曜日文字列
	}



	//==============================================================================
	// オーダが出ている処置の名前の取得
	// 入院期間開始から、表示日付の期間まで
	//==============================================================================

	// 対象患者の入院情報（表示７日中の最初の日）
	$nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, $pt_id, $care[1]["yyyymmdd"]);
	$nyuin_start_day = @$nyuin_info["inpt_in_dt"];
	if (!$nyuin_start_day) $nyuin_start_day = $care[1]["yyyymmdd"];


	//テンプレートの日本語、つけあわせはどこにも無い。固定でもつ。
	$tmpl_names = array();
	$tmpl_names["joks"] = "褥創";
	$tmpl_names["nank"] = "軟膏処置";
	$tmpl_names["sosh"] = "創処置";
	$tmpl_names["teng"] = "点眼";

	$sql =
		" select".
		" assign_pattern".
		",assign_sun_flg".
		",assign_mon_flg".
		",assign_tue_flg".
		",assign_wed_flg".
		",assign_thu_flg".
		",assign_fri_flg".
		",assign_sat_flg".
		",start_ymd".
		",tmpl_chapter_code".
		" from sot_summary_schedule".
		" where start_ymd >= ". $nyuin_start_day.
		" and ptif_id = '" . pg_escape_string($pt_id) ."'".
		" and end_ymd >= ".$care[1]["yyyymmdd"].
	  " and temporary_update_flg = 0".
	  " and delete_flg = false".
	  " and times_per_ymd > 0".
	  " and tmpl_chapter_code in ('joks','nank','sosh','teng')".
	  " order by start_ymd, regist_timestamp";
	$sel = $c_sot_util->select_from_table($sql);
	$order_names = array();
	while($row = pg_fetch_array($sel)){
		// 曜日つきは曜日がひとつでもチェックされているか
		$is_enable = 1;
		if ($row["assign_pattern"] == "WEEK_OVERRIDE"){
			if (!$row["assign_sun_flg"] && !$row["assign_mon_flg"] && !$row["assign_tue_flg"] &&
					!$row["assign_wed_flg"] && !$row["assign_thu_flg"] && !$row["assign_fri_flg"] && !$row["assign_sat_flg"]){
				$is_enable = 0;
			}
		}
		// グラフ初日以前であれば、有効な最新を取得する。
		if ($row["start_ymd"] < $care[1]["yyyymmdd"]) $order_names[trim($row["tmpl_chapter_code"])] = $is_enable;
		// グラフ初日から７日のものは、すべて有効である。
		else if ($is_enable) $order_names[trim($row["tmpl_chapter_code"])] = 1;
	}

	//==============================================================================
	// 温度板バイタル（グラフ情報）の収集、
	// バイタルはXMLを見ないでも良いように、「ナンデモテーブル」に格納されている。
	//==============================================================================
	$sql =
		" select * from (".
		" select".
		" sss.apply_id". // 承認ID
		",sss.start_ymd". // オーダ対象日
		",sssv.string2". // 収縮期血圧
		",sssv.string3". // 拡張時血圧
		",sssv.string4". // 脈拍数
		",sssv.string5". // 呼吸数
		",sssv.string6". // 体温
		",sssv.string7". // SPO2
		",sssv.string8". // 脈拍数マーカ
		",coalesce(sssv.int1,0) as int1".    // 測定時
		",coalesce(sssv.int2,0) as int2".    // 測定分
		" from sot_summary_schedule sss".
		" inner join sot_summary_schedule_various sssv on (sssv.schedule_seq = sss.schedule_seq)".
		" where sss.tmpl_file_name = 'tmpl_OndobanVital.php'".
		" and sss.delete_flg = false".
		" and sss.temporary_update_flg = 0".
		" and sss.ptif_id = '".pg_escape_string($pt_id)."'".
		" and sss.start_ymd between ".$care[1]["yyyymmdd"]." and ".$care[7]["yyyymmdd"].
		" ) d".
		" order by start_ymd, int1, int2";

	$sel = $c_sot_util->select_from_table($sql);
	$data = (array)pg_fetch_all($sel);

	$graphP = array(); // 脈拍数
	$graphT = array(); // 体温
	$graphR = array(); // 呼吸数
	$graphS = array(); // SPO2
	$graphBP = array(); // 血圧

	$tooltipData = array();

	$vital = array();
	foreach($data as $key => $row){
		if (!$row) break;
		$day_index = $ymd[$row["start_ymd"]];
		$vital[$day_index][] = $row;

		$hour =   (int)trim($row["int1"]); // 測定時
		$minute = (int)trim($row["int2"]); // 測定分

		$valBPs = trim($row["string2"]); // 収縮期血圧 (最小40)
		$valBPk = trim($row["string3"]); // 拡張時血圧 (最大240)
		$valP =   trim($row["string4"]); // 脈拍数 (10-210)
		$valR =   trim($row["string5"]); // 呼吸数 (5-55)
		$valT =   trim($row["string6"]); // 体温 (33-43度)
		$valS =   trim($row["string7"]); // SPO2 (30-100％)
		$valPM =  trim($row["string8"]); // 脈拍数マーカ（空文字または「x」）

		$graph_day = ((int)$day_index-1)*24*60 + $hour*60 + $minute; // グラフ上の横位置（分単位）

		$toolmsg = array();

		//--------------------------
		// 血圧
		//--------------------------
		if ($valBPs!="" || $valBPk!=""){
			$valBPs = (int)$valBPs;
			$valBPk = (int)$valBPk;
			if ($valBPs < 240 && $valBPk > 40 && $valBPs < $valBPk) {
				$graphBP[] = $graph_day."-".$valBPs."_".$valBPk;
				$toolmsg["血圧"] = $valBPk." 〜 ".$valBPs;
			} else if ($valBPk < 240 && $valBPs > 40 && $valBPk < $valBPs) {
				$graphBP[] = $graph_day."-".$valBPk."_".$valBPs;
				$toolmsg["血圧"] = $valBPs." 〜 ".$valBPk;
			}
		}
		//--------------------------
		// 脈拍
		//--------------------------
		if ($valP!=""){
			$valP = (float)$valP;
			if ($valP < 10) $valP = 10;
			if ($valP > 210) $valP = 210;
			$graphP[] = $graph_day."-".$valP."-".$valPM;
			$toolmsg["脈拍"] = $valP;
		}
		//--------------------------
		// 呼吸数
		//--------------------------
		if ($valR!=""){
			$valR = (float)$valR;
			if ($valR < 5) $valR = 5;
			if ($valR > 55) $valR = 55;
			$graphR[] = $graph_day."-".$valR;
			$toolmsg["呼吸数"] = $valR;
		}
		//--------------------------
		// 体温
		//--------------------------
		if ($valT!=""){
			$valT = (float)$valT;
			if ($valT < 33) $valT = 33;
			if ($valT > 43) $valT = 43;
			$graphT[] = $graph_day."-".$valT;
			$toolmsg["体温"] = $valT;
		}
		//--------------------------
		// SPO2
		//--------------------------
		if ($valS!=""){
			$valS = (float)$valS;
			if ($valS < 30) $valS = 30;
			if ($valS > 100) $valS = 100;
			$graphS[] = $graph_day."-".$valS;
			$toolmsg["SPO2"] = $valS;
		}
		//--------------------------
		// ツールチップ用
		//--------------------------
		$tooltipData[] = array(
			"left" => $graph_day,
			"datetime" => $c_sot_util->slash_ymd($row["start_ymd"]) . " " . (int)$hour .":" . sprintf("%02d",$minute),
			"msg" => $toolmsg
		);
	}
	//==============================================================================
	// 温度板酸素吸入（グラフ情報）の収集、
	// これらもXMLを見ないでも良いように、「ナンデモテーブル」に格納されている。
	//==============================================================================
	$sql =
		" select".
		" sssv.int1". // 開始日(YYYYMMDD) オーダ対象日に、48時間制で指定された増加を含めたもの
		",sssv.int2". // 終了日(YYYYMMDD)
		",sssv.int3". // 開始分 0から1440分以内であるはず
		",sssv.int4". // 終了分 0から1440分以内であるはず
		",sssv.string2". // チャプター名（酸素吸入または心電図モニターまたは呼吸器）
		",sssv.string3". // 酸素吸入のリットル表示
		",sssv.string4". // 終了日が登録されていなければ"y"
		" from sot_summary_schedule sss".
		" inner join sot_summary_schedule_various sssv on (".
		"   sssv.schedule_seq = sss.schedule_seq".
		"   and string2 ='ocar_sanso_ji'".
		" )".
		" where sss.tmpl_file_name = 'tmpl_OndobanCare.php'".
		" and sss.delete_flg = false".
		" and sss.temporary_update_flg = 0".
		" and sss.ptif_id = '".pg_escape_string($pt_id)."'".
		" and sssv.int1 <= ".$care[7]["yyyymmdd"]. // スケジュールの開始日は、画面最右列の日付けよりは過去
		" and sssv.int2 >= ".$care[1]["yyyymmdd"]. // スケジュールの終了日は、画面最左列の日付けよりは未来
		" order by int1, int3, schedule_various_seq";

	$sel = $c_sot_util->select_from_table($sql);
	$data = (array)pg_fetch_all($sel);

	$gSK = array(); // 酸素吸入 データ並べ替えのための一時的なもの
	$gSM = array(); // 心電図モニター データ並べ替えのための一時的なもの
	$gKK = array(); // 呼吸器 データ並べ替えのための一時的なもの


	foreach($data as $key => $row){
		if (!$row) break;

		// ソート用文字列の作成（YYYYMMDD@[4桁の分数])
		$from_key = $row["int1"] ."@". substr("0000".(int)$row["int3"], -4);
		$to_key =   $row["int2"] ."@". substr("0000".(int)$row["int4"], -4);

		$from_day_index = @$ymd[$row["int1"]];
		$to_day_index   = @$ymd[$row["int2"]];

		// 開始日がグラフの外なら、グラフの左端となるようにする
		$from_hm = $row["int3"];
		$is_from_overflow = "n";
		if (!$from_day_index) {
			$from_day_index = 1;
			$from_hm = 0;
			$is_from_overflow = "y";
		}

		// 終了日がグラフの外なら、グラフの右端となるようにする
		$to_hm = $row["int4"];
		$is_to_overflow = "n";
		if (!$to_day_index) {
			$to_day_index = 7;
			$to_hm = 1440;
			$is_to_overflow = "y";
		}

		$ratio_from = (($from_day_index-1)*24*60) + $from_hm;
		$ratio_to   = (($to_day_index-1)*24*60) + $to_hm;

		// 酸素吸入をいったんならべる

		// ひとつ前の終了日時とかぶってないか比較、
		// かぶっている場合はひとつ前のデータの終了日を修正
		$c = count($gSK);
		$next_idx = $c;
		if ($c > 0 && $gSK[$c-1]["to_key"] > $from_key){
			$gSK[$c-1]["ratio_to"] = $ratio_from;
			if (!@$ymd[$row["int1"]]) $next_idx = $c-1;
		}
		$gSK[$next_idx] = array(
			"to_key"=>$to_key,
			"is_from_overflow" => $is_from_overflow,
			"is_to_overflow" => $is_to_overflow,
			"ratio_from"=> $ratio_from,
			"ratio_to"=> $ratio_to,
			"litter"=>$row["string3"],
			"not_ended"=>$row["string4"]
		);
	}
	// 酸素吸入のURL文字列を作成
	$graphSK = array();
	foreach($gSK as $idx => $d){
		$graphSK[] = $d["is_from_overflow"]."-".$d["is_to_overflow"]."-".$d["ratio_from"]."-".$d["ratio_to"]."-".$d["not_ended"]."-".$d["litter"];
	}


	//==============================================================================
	// 心電図モニター（グラフ情報）の収集、
	// これらもXMLを見ないでも良いように、「ナンデモテーブル」に格納されている。
	//==============================================================================

	// 心電図モニターはずっと昔に開始されっぱなし・終了日無限」だけを指定している場合がある。
	// このため、表示範囲手前のデータを一件取得しておく。
	// スケジュールの開始日は、画面最左列の日付けの前日より過去
	$sql =
		" select max(sssv.schedule_various_seq)".
		" from sot_summary_schedule sss".
		" inner join sot_summary_schedule_various sssv on (sssv.schedule_seq = sss.schedule_seq)".
		" where sss.tmpl_file_name = 'tmpl_OndobanCare.php'".
		" and sss.delete_flg = false".
		" and sss.temporary_update_flg = 0".
		" and sss.ptif_id = '".pg_escape_string($pt_id)."'".
		" and sssv.string2 = 'ocar_sinden'".
		" and sssv.int1 <= ".$care[0]["yyyymmdd"].
		" and sssv.int2 > ".$care[0]["yyyymmdd"];

	$sel = $c_sot_util->select_from_table($sql);
	$last_schedule_various_seq = (int)@pg_fetch_result($sel, 0, 0);
	if (!$last_schedule_various_seq) $last_schedule_various_seq = 99999999;

	// 以下を検索
	// ・期間内に開始した場合
	// ・期間初日以降に終了する場合
	// ・または、過去に開始された直近レコード
	$sql =
		" select".
		" sssv.int1". // 開始日(YYYYMMDD) オーダ対象日に、48時間制で指定された増加を含めたもの
		",sssv.int2". // 終了日(YYYYMMDD)
		",sssv.int3". // 開始分 0から1440分以内であるはず
		",sssv.int4". // 終了分 0から1440分以内であるはず
		",sssv.string2". // チャプター名（酸素吸入または心電図モニターまたは呼吸器）
		",sssv.string3". // 酸素吸入のリットル表示
		",sssv.string4". // 終了日が登録されていなければ"y"
		" from sot_summary_schedule sss".
		" inner join sot_summary_schedule_various sssv on (".
		"   sssv.schedule_seq = sss.schedule_seq".
		"   and string2 = 'ocar_sinden'".
		" )".
		" where sss.tmpl_file_name = 'tmpl_OndobanCare.php'".
		" and sss.delete_flg = false".
		" and sss.temporary_update_flg = 0".
		" and sss.ptif_id = '".pg_escape_string($pt_id)."'".
		" and (".
		"   (sssv.int1 <= ".$care[7]["yyyymmdd"]. " and sssv.int1 >= ".$care[1]["yyyymmdd"] .")" .// スケジュールの開始日は、画面表示期間内
		"   or".
		"   (sssv.schedule_various_seq = ".$last_schedule_various_seq.")". // 直近最終レコードを含める
		" )".
		" and sssv.int2 >= ".$care[1]["yyyymmdd"].
		" order by int1, int3, schedule_various_seq";

	$sel = $c_sot_util->select_from_table($sql);
	$data = (array)pg_fetch_all($sel);

	$gSM = array(); // 心電図モニター 並べ替えるための一時的なもの
	foreach($data as $key => $row){
		if (!$row) break;

		// ソート用文字列の作成（YYYYMMDD@[4桁の分数])
		$from_key = $row["int1"] ."@". substr("0000".(int)$row["int3"], -4);
		$to_key =   $row["int2"] ."@". substr("0000".(int)$row["int4"], -4);

		$from_day_index = @$ymd[$row["int1"]];
		$to_day_index   = @$ymd[$row["int2"]];

		// 開始日がグラフの外なら、グラフの左端となるようにする
		$from_hm = $row["int3"];
		$is_from_overflow = "n";
		if (!$from_day_index) {
			$from_day_index = 1;
			$from_hm = 0;
			$is_from_overflow = "y";
		}

		// 終了日がグラフの外なら、グラフの右端となるようにする
		$to_hm = $row["int4"];
		$is_to_overflow = "n";
		if (!$to_day_index) {
			$to_day_index = 7;
			$to_hm = 1440;
			$is_to_overflow = "y";
		}

		$ratio_from = (($from_day_index-1)*24*60) + $from_hm;
		$ratio_to   = (($to_day_index-1)*24*60) + $to_hm;

		// 心電図モニターをいったんならべる
		// ひとつ前の終了日時とかぶってないか比較、
		// かぶっている場合はひとつ前のデータの終了日を修正
		$c = count($gSM);
		$next_idx = $c;
		if ($c > 0 && $gSM[$c-1]["to_key"] > $from_key){
			$gSM[$c-1]["ratio_to"] = $ratio_from;
			if (!@$ymd[$row["int1"]]) $next_idx = $c-1;
		}

		$gSM[$next_idx] = array(
			"to_key"=>$to_key,
			"is_from_overflow" => $is_from_overflow,
			"is_to_overflow" => $is_to_overflow,
			"ratio_from"=> $ratio_from,
			"ratio_to"=> $ratio_to,
			"not_ended"=>$row["string4"]
		);
	}

	// 心電図モニターのURL文字列を作成
	$graphSM = array();
	foreach($gSM as $idx => $d){
		$graphSM[] = $d["is_from_overflow"]."-".$d["is_to_overflow"]."-".$d["ratio_from"]."-".$d["ratio_to"]."-".$d["not_ended"];
	}

	//==============================================================================
	// 呼吸器（グラフ情報）の収集、
	// やっていることは心電図モニターとまったく同様
	//==============================================================================
	$sql =
		" select max(sssv.schedule_various_seq)".
		" from sot_summary_schedule sss".
		" inner join sot_summary_schedule_various sssv on (sssv.schedule_seq = sss.schedule_seq)".
		" where sss.tmpl_file_name = 'tmpl_OndobanCare.php'".
		" and sss.delete_flg = false".
		" and sss.temporary_update_flg = 0".
		" and sss.ptif_id = '".pg_escape_string($pt_id)."'".
		" and sssv.string2 = 'ocar_kokyuki'".
		" and sssv.int1 <= ".$care[0]["yyyymmdd"].
		" and sssv.int2 > ".$care[0]["yyyymmdd"];

	$sel = $c_sot_util->select_from_table($sql);
	$last_schedule_various_seq = (int)@pg_fetch_result($sel, 0, 0);
	if (!$last_schedule_various_seq) $last_schedule_various_seq = 99999999;

	// 以下を検索
	// ・期間内に開始した場合
	// ・期間初日以降に終了する場合
	// ・または、過去に開始された直近レコード
	$sql =
		" select".
		" sssv.int1". // 開始日(YYYYMMDD) オーダ対象日に、48時間制で指定された増加を含めたもの
		",sssv.int2". // 終了日(YYYYMMDD)
		",sssv.int3". // 開始分 0から1440分以内であるはず
		",sssv.int4". // 終了分 0から1440分以内であるはず
		",sssv.string2". // チャプター名（酸素吸入または心電図モニターまたは呼吸器）
		",sssv.string3". // 酸素吸入のリットル表示
		",sssv.string4". // 終了日が登録されていなければ"y"
		" from sot_summary_schedule sss".
		" inner join sot_summary_schedule_various sssv on (".
		"   sssv.schedule_seq = sss.schedule_seq".
		"   and string2 = 'ocar_kokyuki'".
		" )".
		" where sss.tmpl_file_name = 'tmpl_OndobanCare.php'".
		" and sss.delete_flg = false".
		" and sss.temporary_update_flg = 0".
		" and sss.ptif_id = '".pg_escape_string($pt_id)."'".
		" and (".
		"   (sssv.int1 <= ".$care[7]["yyyymmdd"]. " and sssv.int1 >= ".$care[1]["yyyymmdd"] .")" .// スケジュールの開始日は、画面表示期間内
		"   or".
		"   (sssv.schedule_various_seq = ".$last_schedule_various_seq.")". // 直近最終レコードを含める
		" )".
		" and sssv.int2 >= ".$care[1]["yyyymmdd"].
		" order by int1, int3, schedule_various_seq";

	$sel = $c_sot_util->select_from_table($sql);
	$data = (array)pg_fetch_all($sel);

	foreach($data as $key => $row){
		if (!$row) break;

		// ソート用文字列の作成（YYYYMMDD@[4桁の分数])
		$from_key = $row["int1"] ."@". substr("0000".(int)$row["int3"], -4);
		$to_key =   $row["int2"] ."@". substr("0000".(int)$row["int4"], -4);

		$from_day_index = @$ymd[$row["int1"]];
		$to_day_index   = @$ymd[$row["int2"]];

		// 開始日がグラフの外なら、グラフの左端となるようにする
		$from_hm = $row["int3"];
		$is_from_overflow = "n";
		if (!$from_day_index) {
			$from_day_index = 1;
			$from_hm = 0;
			$is_from_overflow = "y";
		}

		// 終了日がグラフの外なら、グラフの右端となるようにする
		$to_hm = $row["int4"];
		$is_to_overflow = "n";
		if (!$to_day_index) {
			$to_day_index = 7;
			$to_hm = 1440;
			$is_to_overflow = "y";
		}

		$ratio_from = (($from_day_index-1)*24*60) + $from_hm;
		$ratio_to   = (($to_day_index-1)*24*60) + $to_hm;


		// 呼吸器をいったんならべる
		// ひとつ前の終了日時とかぶってないか比較、
		// かぶっている場合はひとつ前のデータの終了日を修正
		$c = count($gKK);
		$next_idx = $c;
		if ($c > 0 && $gKK[$c-1]["to_key"] > $from_key){
			$gKK[$c-1]["ratio_to"] = $ratio_from;
			if (!@$ymd[$row["int1"]]) $next_idx = $c-1;
		}

		$gKK[$next_idx] = array(
			"to_key"=>$to_key,
			"is_from_overflow" => $is_from_overflow,
			"is_to_overflow" => $is_to_overflow,
			"ratio_from"=> $ratio_from,
			"ratio_to"=> $ratio_to,
			"not_ended"=>$row["string4"]
		);
	}

	// 呼吸器のURL文字列を作成
	$graphKK = array();
	foreach($gKK as $idx => $d){
		$graphKK[] = $d["is_from_overflow"]."-".$d["is_to_overflow"]."-".$d["ratio_from"]."-".$d["ratio_to"]."-".$d["not_ended"];
	}

	//==============================================================================
	// 温度板ケア情報の収集
	// ケアはＤＢに格納されているXMLをほじくり出す。
	//==============================================================================
	$uni = array();
	for($i = 1; $i <= 7; $i++) $uni[] = "select " . $care[$i]["yyyymmdd"] . " as ymd";
	$sql =
		" select sss.*, dt.ymd".
		",soa_kango.nikkin_accepted_emp_id as nikkin_kango_accepted_emp_id".
		",soa_kango.yakin_accepted_emp_id as yakin_kango_accepted_emp_id".
		",soa_kaigo.nikkin_accepted_emp_id as nikkin_kaigo_accepted_emp_id".
		",soa_kaigo.yakin_accepted_emp_id as yakin_kaigo_accepted_emp_id".
		",emp_kango1.emp_lt_nm || ' ' || emp_kango1.emp_ft_nm as nikkin_kango_emp_name".
		",emp_kango2.emp_lt_nm || ' ' || emp_kango2.emp_ft_nm as yakin_kango_emp_name".
		",emp_kaigo1.emp_lt_nm || ' ' || emp_kaigo1.emp_ft_nm as nikkin_kaigo_emp_name".
		",emp_kaigo2.emp_lt_nm || ' ' || emp_kaigo2.emp_ft_nm as yakin_kaigo_emp_name".
		" from (".implode(" union ", $uni).") dt".
		" left join sot_summary_schedule sss on (sss.start_ymd = dt.ymd and sss.ptif_id = '".pg_escape_string($pt_id)."' and sss.delete_flg = false and sss.temporary_update_flg = 0 and  sss.tmpl_file_name = 'tmpl_OndobanCare.php')".
		" left outer join sot_ondoban_apply soa_kango on (soa_kango.kango_or_kaigo='kango' and soa_kango.ondoban_date = dt.ymd and soa_kango.ptif_id = '".pg_escape_string($pt_id)."')".
		" left outer join empmst emp_kango1 on (emp_kango1.emp_id = soa_kango.nikkin_accepted_emp_id)".
		" left outer join empmst emp_kango2 on (emp_kango2.emp_id = soa_kango.yakin_accepted_emp_id)".
		" left outer join sot_ondoban_apply soa_kaigo on (soa_kaigo.kango_or_kaigo='kaigo' and soa_kaigo.ondoban_date = dt.ymd and soa_kaigo.ptif_id = '".pg_escape_string($pt_id)."')".
		" left outer join empmst emp_kaigo1 on (emp_kaigo1.emp_id = soa_kaigo.nikkin_accepted_emp_id)".
		" left outer join empmst emp_kaigo2 on (emp_kaigo2.emp_id = soa_kaigo.yakin_accepted_emp_id)".
		" order by dt.ymd, sss.schedule_seq";

	$sel = $c_sot_util->select_from_table($sql);
	$data = (array)pg_fetch_all($sel);
	$kansatu_index_list = array();
	$kansatu_data = array();
	$kansatu_blank_data = array();
	$kansatu_blank_data_max_size = 0;
	$existCareData = false;
	foreach($data as $key => $row){
		if (!$row) break;
//		$idx = $ymd[$row["start_ymd"]];
		$idx = $ymd[$row["ymd"]];
		$care[$idx]["db"] = $row;
		$xml = new template_xml_class();
		$data = $xml->get_xml_object_array_from_db($con, $fname, $row["xml_file_name"]);
		$worksheet = @$data->nodes["template"]->nodes["Ondoban"];
		$care[$idx]["xml"] = @$worksheet->nodes["ocar"];// ケアはその日最新のものをひとつだけ取得
		if($care[$idx]["xml"]) $existCareData = true;
	}

	for ($idx = 1; $idx<= 7; $idx++){
		if (!@$care[$idx]["xml"]) continue;

		// タイトルが入力されている観察項目は、７日間を通じてグルーピングする。
		// タイトルが無い観察項目も、表示したい。別で保存しておく。
        $field_len_kansatu = $c_sot_util->countPatternMatchElement("/^ocar_kansatu_[0-9]+$/", array_keys($care[$idx]["xml"]->nodes));
        $field_len_kansatu = $field_len_kansatu >= 9 ? $field_len_kansatu : 9;
		for ($i = 1; $i <= $field_len_kansatu; $i++){
			$k_title = trim(@$care[$idx]["xml"]->nodes["ocar_kansatu_".$i]->value);
			$k_day =   trim(@$care[$idx]["xml"]->nodes["ocar_kansatu_".$i."_day"]->value);
			$k_night = trim(@$care[$idx]["xml"]->nodes["ocar_kansatu_".$i."_night"]->value);
			if ($k_title=="" && $k_day=="" && $k_night=="") continue;
			if ($k_title==""){
				$kansatu_blank_data[$idx]["day"][] = $k_day;
				$kansatu_blank_data[$idx]["night"][] = $k_night;
				$kansatu_blank_data_max_size = max($kansatu_blank_data_max_size, count($kansatu_blank_data[$idx]["day"]));
			} else {
				if (!in_array($k_title, $kansatu_index_list)) $kansatu_index_list[] = $k_title;
				@$kansatu_data[$idx][$k_title]["day"] .= $k_day;
				@$kansatu_data[$idx][$k_title]["night"] .= $k_night;
			}
		}
	}

	//*************************************************************************
	// 印字データがなければ終了
	//*************************************************************************
	if (!@$_REQUEST["empty_print_and_show_preview"]){
		$existPDFData = false;
		if ($existCareData) $existPDFData = true;
		if (count($graphP)) $existPDFData = true;
		if (count($graphT)) $existPDFData = true;
		if (count($graphRv)) $existPDFData = true;
		if (count($graphS)) $existPDFData = true;
		if (count($graphBP)) $existPDFData = true;
		if (count($graphSK)) $existPDFData = true;
		if (count($graphSM)) $existPDFData = true;
		if (count($graphKK)) $existPDFData = true;
		if (!$existPDFData) continue;
	}

	//*************************************************************************
	// ループ内PDF処理ここから（その２）
	//*************************************************************************

	$pdf->AddPage2();
	if ($resetPageNoFlg) {
	    $pdf->ptpno = 1;    // ページ番号をリセット
	    $resetPageNoFlg = false;
	}
	
  // ページ共通のヘッダー部を出力する
  $pdf->writeHeader($most_left,$ward_name,$ptrm_name,$pt_id,$ptif_name,$lpos,$fwidth,$care);

	//==================
	// グラフ内罫線
	//==================
	$pdf->SetLineWidth(0.4);
	$pdf->Line($most_left+0.2,  30 -$M, $most_right-0.2, 30 -$M);
	$pdf->SetLineWidth(0.2);
	$pdf->SetDrawColor(180,180,180);
	for ($i = 1; $i <= 9; $i++){
		$pdf->Line($most_left,  30+($i*6) -$M, $most_right, 30+($i*6) -$M);
	}

	$pdf->SetDrawColor(230,230,230);
	for ($i = 0; $i <= 9; $i++){
		$pdf->Line($most_left,  33+($i*6) -$M, $most_right, 33+($i*6) -$M);
	}

	$pdf->SetDrawColor(180,180,180);
	for ($i = 1; $i <= 4; $i++){
		$pdf->Line($most_left+($i*8),  30-$M, $most_left+($i*8), 90-$M);
	}

	$pdf->SetDrawColor(0);
	$pdf->SetLineWidth(0.4);
	$pdf->Line($most_left+0.2, 90-$M,  $most_right-0.2, 90-$M);

	// ピンク線
	$pdf->SetDrawColor(255, 96, 207);
	$pdf->SetLineWidth(0.2);
	$pdf->Line($most_left,  30+(6*6) -$M, $most_right, 30+(6*6) -$M);
	$pdf->SetDrawColor(0);

	//==================
	// グラフヘッダ
	//==================
	$pdf->SetTextColor(0, 0, 255); // 青
	$pdf->SetDrawColor(0, 0, 255); // 青
	$pdf->setXY($most_left, 29.5);
	$pdf->Cell(8, 4, "T", 0, 0);
	$pdf->Line($most_left+5,  31.3, $most_left+7.5, 31.3);
	$pdf->DotSquare2($most_left+6.25, 31.3);

	$pdf->SetTextColor(255, 0, 0); // 赤
	$pdf->SetDrawColor(255, 0, 0); // 赤
	$pdf->setXY($most_left+8, 29.5);
	$pdf->Cell(8, 4, "P", 0, 0);
	$pdf->Line($most_left+8+5,  31.3, $most_left+8+7.5, 31.3);
	$pdf->DotCircle($most_left+8+6.25, 31.3);

	$pdf->SetTextColor(0, 136, 34); // 緑
	$pdf->SetDrawColor(0, 136, 34); // 緑
	$pdf->setXY($most_left+16, 29.5);
	$pdf->Cell(8, 4, "R", 0, 0);
	$pdf->Line($most_left+16+5,  31.3, $most_left+16+7.5, 31.3);
	$pdf->DotCross($most_left+16+6.25, 31.3);

	$pdf->SetTextColor(27, 156, 171); // 青緑
	$pdf->SetDrawColor(27, 156, 171); // 青緑
	$pdf->setXY($most_left+24, 29.5);
	$pdf->Cell(8, 4, "SP", 0, 0);
	$pdf->Line($most_left+24+5,  31.3, $most_left+24+7.5, 31.3);
	$pdf->DotSquare1($most_left+24+6.25, 31.3);

	$pdf->SetTextColor(170, 170, 85); // 濃黄色
	$pdf->SetDrawColor(170, 170, 85); // 濃黄色
	$pdf->setXY($most_left+32, 29.5);
	$pdf->Cell(8, 4, "BP", 0, 0);

	$pdf->SetDrawColor(0);

	$pdf->Line($most_left,  33-$M, $lpos[1], 33-$M);


	for ($i = 0; $i < 10; $i++){
		$lblT = "0"; $lblP = "0"; $lblR = "0"; $lblS = "0"; $lblBP = "0";
		if ($i > 0) {
			$lblT = 33 + ($i);
			$lblP = 10 + ($i*20);
			$lblR = 5 + ($i*5);
			$lblS = 30 + ($i*10);
			$lblBP = 40 + ($i*20);
		}
		$grpy = 86.3-($i*6);
		$pdf->SetTextColor(0, 0, 255); // 青
		$pdf->setXY($most_left, $grpy);
		$pdf->Cell(8, 4, $lblT, 0, 0);
		$pdf->SetTextColor(255, 0, 0); // 赤
		$pdf->setXY($most_left+8, $grpy);
		$pdf->Cell(8, 4, $lblP, 0, 0);
		$pdf->SetTextColor(0, 136, 34); // 緑
		$pdf->setXY($most_left+16, $grpy);
		$pdf->Cell(8, 4, $lblR, 0, 0);
		if ($i <= 7){
			$pdf->SetTextColor(27, 156, 171); // 青緑
			$pdf->setXY($most_left+24, $grpy);
			$pdf->Cell(8, 4, $lblS, 0, 0);
		}
		$pdf->SetTextColor(170, 170, 85); // 濃黄色
		$pdf->setXY($most_left+32, $grpy);
		$pdf->Cell(8, 4, $lblBP, 0, 0);
		$pdf->SetTextColor(0); // 黒に戻す
	}

	//==================
	// 酸素吸入などのグラフヘッダ
	//==================

	$pdf->SetLineWidth(0.2);

	// 酸素吸入グラフ（図は後で）
	$pdf->Plot($most_left, 90, $hwidth, "酸素吸入");

	// 心電図モニター（図は後で）
	$pdf->Line($most_left, 95-$M,  $most_right, 95-$M);
	$pdf->Plot($most_left, 95, $hwidth, "心電図モニター");

	// 呼吸器（図は後で）
	$pdf->Line($most_left, 100-$M,  $most_right, 100-$M);
	$pdf->Plot($most_left, 100, $hwidth, "呼吸器");

	// 以降は、高さを測定しつつ描画してゆく
	$curY = 105;

	//==================
	// 尿
	//==================
	$lineHeight = 4;
	$pdf->Line($most_left, $curY-$M,  $most_right, $curY-$M);
	$lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "尿 (回数・量)"));
	for ($i = 1; $i <= 7; $i++){
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i], $curY, $fwidth, @$care[$i]["xml"]->nodes["ocar_nyou_ondoban"]->value));
	}
	$curY += $lineHeight;

	//==================
	// 便
	//==================
	$lineHeight = 4;
	$pdf->Line($most_left, $curY-$M,  $most_right, $curY-$M);
	$lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "便 (回数・時刻'量)"));
	for ($i = 1; $i <= 7; $i++){
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i], $curY, $fwidth, @$care[$i]["xml"]->nodes["ocar_ben_ondoban"]->value));
	}
	$curY += $lineHeight;

	//==================
	// 排液
	//==================
	$lineHeight = 4;
	$pdf->Line($most_left, $curY-$M,  $most_right, $curY-$M);
	$lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "排液 (対象+回数・ml)"));
	for ($i = 1; $i <= 7; $i++){
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i], $curY, $fwidth, @$care[$i]["xml"]->nodes["ocar_haieki_ondoban"]->value));
	}
	$curY += $lineHeight;


	//==================
	// 経口摂取量
	//==================
	$lineHeight = 4;
	$pdf->Line($most_left, $curY-$M,  $most_right, $curY-$M);
	$lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "経口摂取量"));
	for ($i = 1; $i <= 7; $i++){
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i],      $curY, 6.66, @$care[$i]["xml"]->nodes["ocar_sessyu_asa_ondoban"]->value));
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i]+6.66, $curY, 6.66, @$care[$i]["xml"]->nodes["ocar_sessyu_hiru_ondoban"]->value));
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i]+13.32,$curY, 6.66, @$care[$i]["xml"]->nodes["ocar_sessyu_yu_ondoban"]->value));
	}
	for ($i = 1; $i <= 7; $i++){
		$pdf->Line($lpos[$i]+6.66,  $curY-$M,  $lpos[$i]+6.66,  $curY+$lineHeight-$M);
		$pdf->Line($lpos[$i]+13.32, $curY-$M,  $lpos[$i]+13.32, $curY+$lineHeight-$M);
	}
	$curY += $lineHeight;

	//==================
	// 経管摂取量
	//==================
	$lineHeight = 4;
	$pdf->Line($most_left, $curY-$M,  $most_right, $curY-$M);
	$lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "経管摂取量"));
	for ($i = 1; $i <= 7; $i++){
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i],    $curY, 6.66, @$care[$i]["xml"]->nodes["ocar_sessyu2_asa_ondoban"]->value));
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i]+7,  $curY, 6.66, @$care[$i]["xml"]->nodes["ocar_sessyu2_hiru_ondoban"]->value));
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i]+14, $curY, 6.66, @$care[$i]["xml"]->nodes["ocar_sessyu2_yu_ondoban"]->value));
	}
	for ($i = 1; $i <= 7; $i++){
		$pdf->Line($lpos[$i]+6.66,  $curY-$M,  $lpos[$i]+6.66,  $curY+$lineHeight-$M);
		$pdf->Line($lpos[$i]+13.32, $curY-$M,  $lpos[$i]+13.32, $curY+$lineHeight-$M);
	}
	$curY += $lineHeight;



	//==================
	// 補水・補食
	//==================
	$lineHeight = 4;
	$pdf->Line($most_left, $curY-$M,  $most_right, $curY-$M);
	$lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "補水・補食"));
	for ($i = 1; $i <= 7; $i++){
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i],      $curY, 6.66, @$care[$i]["xml"]->nodes["ocar_hosui_asa_ondoban"]->value.@$care[$i]["xml"]->nodes["ocar_hosui_asa_ondoban_sasiire"]->value));
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i]+6.66, $curY, 6.66, @$care[$i]["xml"]->nodes["ocar_hosui_hiru_ondoban"]->value.@$care[$i]["xml"]->nodes["ocar_hosui_hiru_ondoban_sasiire"]->value));
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i]+13.32,$curY, 6.66, @$care[$i]["xml"]->nodes["ocar_hosui_yu_ondoban"]->value.@$care[$i]["xml"]->nodes["ocar_hosui_yu_ondoban_sasiire"]->value));
	}
	for ($i = 1; $i <= 7; $i++){
		$pdf->Line($lpos[$i]+6.66,  $curY-$M,  $lpos[$i]+6.66,  $curY+$lineHeight-$M);
		$pdf->Line($lpos[$i]+13.32, $curY-$M,  $lpos[$i]+13.32, $curY+$lineHeight-$M);
	}
	$curY += $lineHeight;



	//==================
	// 口腔ケア
	//==================
	$lineHeight = 4;
	$pdf->Line($most_left, $curY-$M,  $most_right, $curY-$M);
	$lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "口腔ケア"));
	for ($i = 1; $i <= 7; $i++){
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i],       $curY, 6.66, @$care[$i]["xml"]->nodes["ocar_koukou_asa"]->value));
    $lineHeight = max($lineHeight, $pdf->Plot($lpos[$i]+6.66,  $curY, 6.66, @$care[$i]["xml"]->nodes["ocar_koukou_hiru"]->value));
    $lineHeight = max($lineHeight, $pdf->Plot($lpos[$i]+13.32, $curY, 6.66, @$care[$i]["xml"]->nodes["ocar_koukou_yu"]->value));
	}
	for ($i = 1; $i <= 7; $i++){
		$pdf->Line($lpos[$i]+6.66,  $curY-$M,  $lpos[$i]+6.66,  $curY+$lineHeight-$M);
		$pdf->Line($lpos[$i]+13.32, $curY-$M,  $lpos[$i]+13.32, $curY+$lineHeight-$M);
	}
	$curY += $lineHeight;

	//==================
	// 入浴
	//==================
	$lineHeight = 4;
	$pdf->Line($most_left, $curY-$M,  $most_right, $curY-$M);
	$lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "入浴"));
	for ($i = 1; $i <= 7; $i++){
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i],    $curY, 10, @$care[$i]["xml"]->nodes["ocar_nyuyoku_gozen"]->value));
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i]+10, $curY, 10, @$care[$i]["xml"]->nodes["ocar_nyuyoku_gogo"]->value));
	}
	for ($i = 1; $i <= 7; $i++){
		$pdf->Line($lpos[$i]+10, $curY-$M,  $lpos[$i]+10, $curY+$lineHeight-$M);
	}
	$curY += $lineHeight;


	//==================
	// 整容
	//==================
	$lineHeight = 4;
	$pdf->Line($most_left, $curY-$M,  $most_right, $curY-$M);
	$lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "整容"));
	for ($i = 1; $i <= 7; $i++){
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i],    $curY, 10, @$care[$i]["xml"]->nodes["ocar_seiyou_gozen"]->value));
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i]+10, $curY, 10, @$care[$i]["xml"]->nodes["ocar_seiyou_gogo"]->value));
	}
	for ($i = 1; $i <= 7; $i++){
		$pdf->Line($lpos[$i]+10, $curY-$M,  $lpos[$i]+10, $curY+$lineHeight-$M);
	}
	$curY += $lineHeight;


	//==================
	// 日勤夜勤タイトル
	//==================
  $curY = $pdf->nitiyaHeader($most_left + 0.2, $most_right - 0.2, $curY, $M, 0.4, $lpos);


	//==================
	// 吸引回数
	//==================
	$lineHeight = 4;
	$pdf->Line($most_left, $curY-$M,  $most_right, $curY-$M);
	$lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "吸引回数"));
	for ($i = 1; $i <= 7; $i++){
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i],    $curY, 10, @$care[$i]["xml"]->nodes["ocar_kyuin_day"]->value));
		$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i]+10, $curY, 10, @$care[$i]["xml"]->nodes["ocar_kyuin_night"]->value));
	}
	for ($i = 1; $i <= 7; $i++){
		$pdf->Line($lpos[$i]+10, $curY-$M,  $lpos[$i]+10, $curY+$lineHeight-$M);
	}
	$curY += $lineHeight;



	//==================
	// 処置
	//==================
	//テンプレートの日本語、つけあわせはどこにも無い。固定でもつ。
	$tmpl_names = array();
	$tmpl_names["joks"] = "褥創";
	$tmpl_names["nank"] = "軟膏処置";
	$tmpl_names["sosh"] = "創処置";
	$tmpl_names["teng"] = "点眼";


	$pdf->Line($most_left, $curY-$M,  $most_right, $curY-$M);
	$tmpStartY = $curY;
	foreach ($tmpl_names as $chapter_code => $caption){
		$lineHeight = 4;
		$pdf->Line($most_left+6, $curY-$M,  $most_right, $curY-$M);
		$lineHeight = max($lineHeight, $pdf->Plot($most_left+6, $curY, $hwidth-6, $caption));
		for ($i = 1; $i <= 7; $i++){
			$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i],    $curY, 10, @$care[$i]["xml"]->nodes["ocar_syoti_".$chapter_code."_day"]->value));
			$lineHeight = max($lineHeight, $pdf->Plot($lpos[$i]+10, $curY, 10, @$care[$i]["xml"]->nodes["ocar_syoti_".$chapter_code."_night"]->value));
		}
		for ($i = 1; $i <= 7; $i++){
			$pdf->Line($lpos[$i]+10, $curY-$M,  $lpos[$i]+10, $curY+$lineHeight-$M);
		}
		$curY += $lineHeight;
	}
	$pdf->Plot($most_left, $tmpStartY+2, 6, "処");
	$pdf->Plot($most_left, $tmpStartY+8, 6, "置");
	$pdf->Line($most_left+6, $tmpStartY-$M, $most_left+6, $curY-$M);


  // ここまで描いたところでグラフ描画を行う

  // 縦罫線を描く
  $pdf->verticalLine($lpos, $most_top - $M, $curY - $M);

  //**************************************
  // グラフ
  // ↓↓↓
  //**************************************

    //**********
    // メイン・BP
    //**********
    $heightPerGaugeBP = $graphHeight / (240 - 40);
    $pdf->SetLineWidth(1.2);
    foreach($graphBP as $idx => $v){
      $p = explode("-", $v);
      $curL = $graphStartLeft+$widthPerMinute*$p[0];
      $ud = explode("_", $p[1]);
      $curU = (240-$ud[0])*$heightPerGaugeBP + $graphStartTop - $M;
      $curD = (240-$ud[1])*$heightPerGaugeBP + $graphStartTop - $M;
      $pdf->SetDrawColor(170, 170, 85); // 濃黄色
      $pdf->Line($curL, $curU, $curL, $curD);
    }
    $pdf->SetLineWidth(0.2);

    //**********
    // メイン・T
    //**********
    $heightPerGaugeT = $graphHeight / (43 - 33);
    $prevL = 0;
    $prevT = 0;
    foreach($graphT as $idx => $v){
      $p = explode("-", $v);
      $curL = $graphStartLeft+$widthPerMinute*$p[0];
      $curT = (43-$p[1])*$heightPerGaugeT + $graphStartTop - $M;
      $pdf->SetDrawColor(0, 0, 255); // 青
      if ($prevL || $prevT) $pdf->Line($prevL, $prevT, $curL, $curT);
      $prevL = $curL;
      $prevT = $curT;
    }
    foreach($graphT as $idx => $v){
      $p = explode("-", $v);
      $curL = $graphStartLeft+$widthPerMinute*$p[0];
      $curT = (43-$p[1])*$heightPerGaugeT + $graphStartTop - $M;
      $pdf->SetDrawColor(0, 0, 255); // 青
      $pdf->DotSquare2($curL, $curT);
    }

    //**********
    // メイン・P
    //**********
    $heightPerGaugeP = $graphHeight / (210 - 10);
    $prevL = 0;
    $prevT = 0;
    foreach($graphP as $idx => $v){
      $p = explode("-", $v);
      $curL = $graphStartLeft+$widthPerMinute*$p[0];
      $curT = (210-$p[1])*$heightPerGaugeP + $graphStartTop - $M;
      $pdf->SetDrawColor(255, 0, 0); // 赤
      if ($prevL || $prevT) $pdf->Line($prevL, $prevT, $curL, $curT);
      if ($p[2] == "x") {
        $pdf->DotBatsu($curL, $curT);
      } else {
        $pdf->DotCircle($curL, $curT);
      }
      $prevL = $curL;
      $prevT = $curT;
    }

    //**********
    // メイン・R
    //**********
    $heightPerGaugeR = $graphHeight / (55 - 5);
    $prevL = 0;
    $prevT = 0;
    foreach($graphR as $idx => $v){
      $p = explode("-", $v);
      $curL = $graphStartLeft+$widthPerMinute*$p[0];
      $curT = (55-$p[1])*$heightPerGaugeR + $graphStartTop - $M;
      $pdf->SetDrawColor(0, 136, 34); // 緑
      if ($prevL || $prevT) $pdf->Line($prevL, $prevT, $curL, $curT);
      $pdf->DotCross($curL, $curT);
      $prevL = $curL;
      $prevT = $curT;
    }

    //**********
    // メイン・SP
    //**********
    $heightPerGaugeS = $graphHeight / (130 - 30);
    $prevL = 0;
    $prevT = 0;
    foreach($graphS as $idx => $v){
        $p = explode("-", $v);
        $curL = $graphStartLeft+$widthPerMinute*$p[0];
      $curT = (130-$p[1])*$heightPerGaugeS + $graphStartTop - $M;
      $pdf->SetDrawColor(27, 156, 171); // 青緑
      if ($prevL || $prevT) $pdf->Line($prevL, $prevT, $curL, $curT);
      $pdf->DotSquare1($curL, $curT);
      $prevL = $curL;
      $prevT = $curT;
    }

    //**********
    // 酸素吸入
    //**********
    foreach($gSK as $idx => $d){
      $fromX = $graphStartLeft + $widthPerMinute * $d["ratio_from"];
      $toX   = $graphStartLeft + $widthPerMinute * $d["ratio_to"];
      $pdf->SetDrawColor(128, 100, 162); // 紫
      $pdf->SetLineWidth(0.3);
      $pdf->Line($fromX, 93, $toX, 93);

      if ($d["is_from_overflow"]=="n") {
        $pdf->DotSquare1($fromX, 93, "sanso");
      }
      if ($d["is_to_overflow"]=="n") {
        if ($d["not_ended"]=="y") $pdf->DotSquare2($toX, 93, "sanso");
        else $pdf->DotSquare1($toX, 93, "sanso");
      }
      if ($d["litter"]!="") {
        $pdf->setXY($fromX+0.5, 89.3);
        $pdf->SetFontSize(7);
        $pdf->Cell(100, 4, $d["litter"], 0, 0);
      }
    }


    //**********
    // 心電図モニター
    //**********
    foreach($gSM as $idx => $d){
      $fromX = $graphStartLeft + $widthPerMinute * $d["ratio_from"];
      $toX   = $graphStartLeft + $widthPerMinute * $d["ratio_to"];
      $pdf->SetDrawColor(155, 187, 89); // 紫
      $pdf->SetLineWidth(0.3);
      $pdf->Line($fromX, 98, $toX, 98);

      if ($d["is_from_overflow"]=="n") {
        $pdf->DotSquare1($fromX, 98, "sinden");
      }
      if ($d["is_to_overflow"]=="n") {
        if ($d["not_ended"]=="y") $pdf->DotSquare2($toX, 98, "sinden");
        else $pdf->DotSquare1($toX, 98, "sinden");
      }
    }


    //**********
    // 呼吸器
    //**********
    foreach($gKK as $idx => $d){
      $fromX = $graphStartLeft + $widthPerMinute * $d["ratio_from"];
      $toX   = $graphStartLeft + $widthPerMinute * $d["ratio_to"];
      $pdf->SetDrawColor(255, 145, 85); // みかん色
      $pdf->SetLineWidth(0.3);
      $pdf->Line($fromX, 103, $toX, 103);

      if ($d["is_from_overflow"]=="n") {
        $pdf->DotSquare1($fromX, 103, "kokyuki");
      }
      if ($d["is_to_overflow"]=="n") {
        if ($d["not_ended"]=="y") $pdf->DotSquare2($toX, 103, "kokyuki");
        else $pdf->DotSquare1($toX, 103, "kokyuki");
      }
    }

  //**************************************
  // グラフ
  // ↑↑↑
  //**************************************

  // 1ページ目に観察項目ヘッダーを出力するY位置をセットする
  $pdf->kansatuYs[0] = $curY;

  //================
  // 観察項目を出力
  //================
  $pt_info = array(    // 患者情報
          "ward_name" => $ward_name,
          "ptrm_name" => $ptrm_name,
          "pt_id"     => $pt_id,
          "ptif_name" => $ptif_name
  );
  
  $kansatu_title_startY = $curY;    // 観察項目ヘッダー開始Y座標
  $pdf->SetLineWidth(0.2);
  $pdf->Line($most_left, $curY-$M,  $most_right, $curY-$M);
  $pdf->kansatuListCount = 0;

  // 観察項目１（タイトルがある観察項目）
  $tmpStartY = $curY;
  $kansatu_index_list_size = count($kansatu_index_list);
  createkansatuItem($pdf, $kansatu_data, $kansatu_index_list, $kansatu_index_list_size, $curY, $tmpStartY, $kansatu_title_startY, $pt_info, $care, true);
  $pdf->verticalLine($lpos, $tmpStartY - $M, $curY - $M);

  // 観察項目２（タイトルが無い観察項目）
  $tmpStartY = $curY;
  createkansatuItem($pdf, $kansatu_blank_data, array(), $kansatu_blank_data_max_size, $curY, $tmpStartY, $kansatu_title_startY, $pt_info, $care, false);
  
  // レイアウト調整の為、観察項目が4行に満たない場合は、空欄を出力
  $kansatu_total_list_size = $kansatu_index_list_size + $kansatu_blank_data_max_size;
  if ($kansatu_total_list_size > 0 && $pdf->kansatuListCount < 4) {
      $count = $pdf->kansatuListCount;
      while ($count < 4) {
          $lineHeight = 3.25;
          $pdf->Line($most_left + 6, $curY - $M, $most_right, $curY-$M);
          $lineY = $curY + $lineHeight - $M;
          for ($i = 1; $i <= 7; $i++) {
              $pdf->Line($lpos[$i]+10, $curY - $M,  $lpos[$i] + 10, $lineY);
          }
          $curY += $lineHeight;
          $pdf->kansatuLineAh += $lineHeight;
          $count++;
      }
  }

  // 観察項目ヘッダーを出力する
  if ($kansatu_total_list_size) {
      $pdf->kansatuHeader($most_left, $kansatu_title_startY, $curY, $M, $most_top);
  }
  // 縦罫線を描く
  $pdf->verticalLine($lpos, $tmpStartY - $M, $curY - $M);
  // 外周罫線を描く
  $pdf->rectLine($most_left, $most_top - $M, $most_right, $curY - $M);

  $tmpStartY = $curY;

  //==================
  // サイン（看護）
  //==================
  $lineHeight = 4;
  $pdf->Line($most_left, $curY-$M,  $most_right, $curY-$M);
  $lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "サイン (看護)"));
  for ($i = 1; $i <= 7; $i++){
      $lineHeight = max($lineHeight, $pdf->Plot($lpos[$i],    $curY, 10, @$care[$i]["db"]["nikkin_kango_emp_name"]));
      $lineHeight = max($lineHeight, $pdf->Plot($lpos[$i]+10, $curY, 10, @$care[$i]["db"]["yakin_kango_emp_name"]));
  }
  for ($i = 1; $i <= 7; $i++){
      $pdf->Line($lpos[$i]+10, $curY-$M,  $lpos[$i]+10, $curY+$lineHeight-$M);
  }
  $curY += $lineHeight;

  //==================
  // サイン（介護）
  //==================
  $lineHeight = 4;
  $pdf->Line($most_left, $curY-$M,  $most_right, $curY-$M);
  $lineHeight = max($lineHeight, $pdf->Plot($most_left, $curY, $hwidth, "サイン (介護)"));
  for ($i = 1; $i <= 7; $i++){
      $lineHeight = max($lineHeight, $pdf->Plot($lpos[$i],    $curY, 10, @$care[$i]["db"]["nikkin_kaigo_emp_name"]));
      $lineHeight = max($lineHeight, $pdf->Plot($lpos[$i]+10, $curY, 10, @$care[$i]["db"]["yakin_kaigo_emp_name"]));
  }
  for ($i = 1; $i <= 7; $i++){
      $pdf->Line($lpos[$i]+10, $curY-$M,  $lpos[$i]+10, $curY+$lineHeight-$M);
  }
  $curY += $lineHeight;

  // 縦罫線を描く
  $pdf->verticalLine($lpos, $tmpStartY - $M, $curY - $M);
  // 外周罫線を描く
  $pdf->rectLine($most_left, $most_top - $M, $most_right, $curY - $M);


	//*************************************************************************
	// ループ内PDF処理ここまで（その２）
	//*************************************************************************

} // ７日ごと日付ループ終了

if ($print_side === "both") {    // 両面印刷ならば、患者単位に偶数ページ出力になるように改ページする
    if ($pdf->page % 2) {
        $pdf->AddPage2();
    }
}
$resetPageNoFlg = true;
} // $pt_list ループ終了

if (@$_REQUEST["empty_print_and_show_preview"]){
	$pdf->Output();
} else {
	$pdf->Output("ondoban".date("Ymd_His").".pdf", "D");
}

/**
 * 観察項目を出力
 * 
 * @param object  $pdf                  PDF生成オブジェクト
 * @param array   $data                 観察項目データ
 * @param array   $kansatu_index_list   観察項目タイトル
 * @param integer $size                 観察項目数
 * @param float   $curY                 現在のY座標
 * @param float   $tmpStartY            Y座標
 * @param float   $kansatu_title_startY 観察項目ヘッダー開始Y座標
 * @param array   $pt_info              患者情報
 * @param array   $care                 ケア情報
 * @param boolean $isExistTitle         観察項目タイトルの有無
 * 
 * @return void
 */
function createkansatuItem(&$pdf, $data, $kansatu_index_list, $size, &$curY, &$tmpStartY, &$kansatu_title_startY, $pt_info, $care, $isExistTitle)
{
    // 位置情報
    global $M;
    global $most_right;
    global $most_left;
    global $most_top;
    global $most_btm;
    global $hwidth;
    global $fwidth;
    global $lpos;

    $line_count = 0;
    for ($row = 0; $row < $size; $row++){
        $lineHeight = 0;
        $pdf->Line($most_left+6, $curY-$M,  $most_right, $curY-$M);
        if ($isExistTitle) {
            $k_title = $kansatu_index_list[$row];
            $lineHeight = max($lineHeight, $pdf->Plot($most_left+6, $curY, $hwidth-6, $k_title));
            for ($i = 1; $i <= 7; $i++){
                $lineHeight = max($lineHeight, $pdf->Plot($lpos[$i],    $curY, 10, @$data[$i][$k_title]["day"]));
                $lineHeight = max($lineHeight, $pdf->Plot($lpos[$i]+10, $curY, 10, @$data[$i][$k_title]["night"]));
            }
        } else {
            for ($i = 1; $i <= 7; $i++){
                $lineHeight = max($lineHeight, $pdf->Plot($lpos[$i],    $curY, 10, @$data[$i]["day"][(string)$row]));
                $lineHeight = max($lineHeight, $pdf->Plot($lpos[$i]+10, $curY, 10, @$data[$i]["night"][(string)$row]));
            }
        }
        $lineY = $curY+$lineHeight-$M;
        for ($i = 1; $i <= 7; $i++){
            $pdf->Line($lpos[$i]+10, $curY-$M,  $lpos[$i]+10, $lineY);
        }
        // ページ下部を越えたら改ページする
        if ($lineY > ($most_btm - 8)) {
            // 描いたばかりの行を消す
            $pdf->fillWhite($most_left, $curY, $most_right - $most_left, 297 - $curY);
            // 観察項目ヘッダーを出力する
            $pdf->kansatuHeader($most_left, $kansatu_title_startY, $curY, $M, $most_top);
            // 縦罫線を描く
            $pdf->verticalLine($lpos, $tmpStartY - $M, $curY - $M);
            // 外周罫線を描く
            $pdf->rectLine($most_left, $most_top - $M, $most_right, $curY - $M);
            // 改ページする
            $pdf->AddPage2();
            $tmpStartY = $most_top;
            // ヘッダーを出力する
            $pdf->writeHeader($most_left, $pt_info["ward_name"], $pt_info["ptrm_name"], $pt_info["pt_id"], $pt_info["ptif_name"], $lpos, $fwidth, $care);
            $curY = $pdf->GetY();
            // 日勤夜勤タイトルヘッダーを出力する
            $curY = $pdf->nitiyaHeader($most_left + 0.2, $most_right - 0.2, $curY, $M, 0.2, $lpos);
            $pdf->Line($most_left, $curY - $M, $most_right, $curY - $M);
            // 観察項目ヘッダー開始Y座標を更新
            $kansatu_title_startY = $curY;
            $row--;
            $pdf->kansatuListCount = 0;
            continue;
        }
        $curY += $lineHeight;
        $pdf->kansatuLineAh += $lineHeight;
        $pdf->kansatuListCount++;
    }
}
?>
