<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("about_postgres.php");
require_once("hiyari_common.ini");
require_once("hiyari_report_class.php");
require_once("Cmx.php");
require_once("aclg_set.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') {
    showLoginPage();
    exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_GET);

if($mode == "update")
{
    // トランザクションの開始
    pg_query($con, "begin transaction");

    // 利用権限更新処理
    set_auth_analysis_use($con, $fname, $_POST);

    // 「インシデントの分析」登録可能項目の登録処理
    set_inci_auth_analysis_use_item($con, $fname, $_POST);

    // トランザクションをコミット
    pg_query($con, "commit");
}

$arr_auth_analysis_use = get_auth_analysis_use($con, $fname);
$inci_auths = "";
$arr_auth = array_keys($arr_auth_analysis_use);
foreach($arr_auth as $auth)
{
    if($inci_auths != "") {
        $inci_auths .= ",";
    }
    $inci_auths .= $auth;
}

$grp_codes = "";
$tmp_arr_analysis_item = get_inci_analysis_item($con, $fname);

// 下記追加 START
$arr_analysis_item['1100'] = '発生件数情報';
$arr_analysis_item['125'] = '分類';

foreach($tmp_arr_analysis_item as $key => $value) {
    $arr_analysis_item[$key] = $value;
}
// END

$arr_grp_code = array_keys($arr_analysis_item);
foreach($arr_grp_code as $code)
{
    if($grp_codes != "") {
        $grp_codes .= ",";
    }
    $grp_codes .= $code;
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$INCIDENT_TITLE?> | 評価担当者設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function update()
{
    if(checkSubmit())
    {
        document.mainform.action="hiyari_report_analysis_auth.php?session=<?=$session?>&mode=update";
        document.mainform.submit();
    }
}

function setCheckboxOnOff(target,is_on)
{
    <?
    foreach($arr_grp_code as $code)
    {
    ?>
        var obj;
        obj = target + '_' + '<?=$code?>';
        document.getElementById(obj).checked = is_on;
    <?
    }
    ?>
}


function checkSubmit()
{
    <?
    foreach($arr_auth as $auth)
    {
    ?>
    var use_flg_obj = "<?=strtolower($auth)?>_use_flg_t";
    if(document.getElementById(use_flg_obj).checked)
    {
        var no_checked = true;
        <?
        foreach($arr_analysis_item as $grp_code => $grp_name)
        {
        ?>
        var obj = "<?=$auth?>_<?=$grp_code?>";
        if(document.getElementById(obj).checked)
        {
            no_checked = false;
        }
        <?
        }
        ?>
        if(no_checked)
        {
            alert("<?=h($arr_auth_analysis_use[$auth]["auth_name"])?>の登録項目が設定されていません。");
            return false;
        }
    }
    <?
    }
    ?>
    return true;
}


function load_action()
{

    //全ての権限の登録項目の表示／非表示を設定
    <?
    foreach($arr_auth as $auth)
    {
    ?>
    var use_flg_obj = "<?=strtolower($auth)?>_use_flg_t";
    var use_flg = document.getElementById(use_flg_obj).checked;
    item_disp("<?=$auth?>",use_flg);
    <?
    }
    ?>
    item_area_disp();
}

function use_flg_changed(auth,use_flg)
{
    item_disp(auth,use_flg);
    item_area_disp();
}


//権限に対する登録項目の表示を設定します。
function item_disp(auth,use_flg)
{

    var item_div_obj_id = auth + "_item_div";
    var item_div_obj = document.getElementById(item_div_obj_id);
    if(use_flg)
    {
        item_div_obj.style.display = '';
    }
    else
    {
        item_div_obj.style.display = 'none';
    }
}


//登録項目全体の表示を設定します。
//全ての権限のが未利用の場合は登録項目全体を非表示
function item_area_disp()
{
    var no_use_flg = true;
    <?
    foreach($arr_auth as $auth)
    {
    ?>
    var use_flg_obj = "<?=strtolower($auth)?>_use_flg_t";
    var use_flg = document.getElementById(use_flg_obj).checked;
    if(use_flg)
    {
        no_use_flg = false;
    }
    <?
    }
    ?>
    var item_area_div_obj = document.getElementById("item_area_div");
    if(no_use_flg)
    {
        item_area_div_obj.style.display = 'none';
    }
    else
    {
        item_area_div_obj.style.display = '';
    }
}

//項目指定のチェックボックスがクリックされた時の処理
function item_checkbox_cilcked(auth,grp_code)
{
    //発生要因もしくは発生要因詳細がクリックされた場合
    if(grp_code == "600" || grp_code == "605")
    {
        //その結果、発生要因詳細が選択状態の場合は、発生要因は常に選択状態となる。
        var obj_600 = document.getElementById(auth + "_600");
        var obj_605 = document.getElementById(auth + "_605");
        if(obj_605.checked)
        {
            obj_600.checked = true;
        }
    }
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
// -->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="load_action()">
<form name="mainform" action="#" method="post">
<input type="hidden" name="inci_auths" value="<?=$inci_auths?>">
<input type="hidden" name="grp_codes" value="<?=$grp_codes?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー -->

<!-- 利用権限 start -->
<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="250" border="0" cellspacing="0" cellpadding="0"  class="list">
<tr height="22">
<td bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>評価画面の利用権限設定</B></font>
</td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="700" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td valign="top" align="center" bgcolor="#F5FFE5">


<table width="100%" border="0" cellspacing="0" cellpadding="3"  class="list">
<tr>
<td width="40%" align="center" bgcolor="#DFFFDC" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当者名称</font></td>
<td width="60%" align="center" bgcolor="#DFFFDC" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">利用権限</font></td>
</tr>

<?foreach($arr_auth_analysis_use as $key => $arr_val){  ?>
<tr>
<td bgcolor="#FFFFFF" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($arr_val["auth_name"])?></font></td>
<td bgcolor="#FFFFFF" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" id="<?=strtolower($key)?>_use_flg_t" name="<?=strtolower($key)?>_use_flg" value="t" <?if($arr_val["use_flg"] == "t"){?>checked<?}?> onclick="use_flg_changed('<?=$key?>',true);">利用可能
    (<input type="checkbox" id="<?=strtolower($key)?>_update_flg_t" name="<?=strtolower($key)?>_update_flg" value="t" <?if($arr_val["update_flg"] == "t"){?>checked<?}?>>更新ボタンを表示する)
<input type="radio" id="<?=strtolower($key)?>_use_flg_f" name="<?=strtolower($key)?>_use_flg" value="f" <?if($arr_val["use_flg"] == "f"){?>checked<?}?> onclick="use_flg_changed('<?=$key?>',false);">利用不可
</font>
</td>
</tr>
<?}?>
</table>

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<!-- 利用権限 end -->

<img src="img/spacer.gif" width="1" height="10" alt=""><br>

<!-- 登録項目制限 start -->
<div id="item_area_div">

<table width="250" border="0" cellspacing="0" cellpadding="0"  class="list">
<tr height="22">
<td bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>評価画面の登録項目設定</B></font>
</td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="700" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td valign="top" align="center" bgcolor="#F5FFE5">

<?
$arr_auth = split(",", $inci_auths);
foreach($arr_auth as $auth){
$arr_auth_analysis_use_item = get_inci_auth_analysis_use_item($con, $fname, $auth)
?>
<div id="<?=$auth?>_item_div">
<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="left">
<input type="button" value="全てON" onclick="setCheckboxOnOff('<?=$auth?>',true)">
<input type="button" value="全てOFF" onclick="setCheckboxOnOff('<?=$auth?>',false)">
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td valign="top" bgcolor="#F5FFE5">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B><?=h($arr_auth_analysis_use[$auth]["auth_name"])?></B></font></td>
</tr>

<?
foreach($arr_analysis_item as $grp_code => $grp_name)
{
    $item_flg = false;
    foreach($arr_auth_analysis_use_item as $val)
    {
        if($grp_code == $val)
        {
            $item_flg = true;
        }
    }
?>
<tr>
<td bgcolor="#FFFFFF">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input
    type="checkbox"
    name="<?=$auth?>_<?=$grp_code?>"
    id="<?=$auth?>_<?=$grp_code?>"
    value="t" <?if($item_flg){?> checked<?}?>
    <?
    if($grp_code == 605)
    {
    ?>
    style="margin-left:20px;"
    <?
    }
    ?>
    onclick="item_checkbox_cilcked('<?=$auth?>','<?=$grp_code?>')"
    >
<label><?=h($grp_name)?></label>
</font>
</td>
</tr>
<?}?>

</table>

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>


</div>
<?}?>

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>

</div>
<!-- 登録項目制限 end -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="700" border="0" cellspacing="1" cellpadding="0">
<tr>
<td align="right">
<input type="button" value="更新" onclick="update();">
</td>
</tr>
</table>





<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->

</td>
</tr>
</table>

</form>
</body>
</html>

<?
// 評価画面利用権限・取得処理
function get_auth_analysis_use($con, $fname)
{
    $array = array();

    $sql  = "select a.auth, a.use_flg, a.update_flg, case when a.auth = 'GU' then '一般ユーザ' else b.auth_name end ";
    $sql .= "from inci_auth_analysis_use a left join inci_auth_mst b on a.auth = b.auth ";
    $cond = "order by b.auth_rank is not null, b.auth_rank asc";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type='text/javascript' src='js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    while($row = pg_fetch_array($sel))
    {
        $auth = $row["auth"];

        if($auth != "GU" && !is_usable_auth($auth, $fname, $con))
        {
            continue;
        }

        $array[$auth] = array("use_flg" => $row["use_flg"], "auth_name" => $row["auth_name"], "update_flg" => $row["update_flg"]);
    }

    return $array;
}

// 評価画面利用権限・更新処理
function set_auth_analysis_use($con, $fname, $inputs)
{
    $inci_auths = $inputs["inci_auths"];
    $arr_auth = split(",", $inci_auths);

    foreach($arr_auth as $auth)
    {
        $use_flg    = $inputs[strtolower($auth)."_use_flg"];
        $update_flg = $inputs[strtolower($auth)."_update_flg"];

        $sql = "update inci_auth_analysis_use set";
        $set = array("use_flg", "update_flg");
        $setvalue = array($use_flg, $update_flg);
        $cond = "where auth = '$auth'";

        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

// 「インシデントの分析」入力項目・取得処理
function get_inci_analysis_item($con, $fname)
{
    $array = array();


    $rep_obj = new hiyari_report_class($con, $fname);
    $grp_index_list =  $rep_obj->get_original_eis_grps(7);
//  $grp_index_list =  array(650,90,96,600,605,510,590,610,620,625,630,635);

    $sql  = "select distinct mate.cate_code, cate_name, mate.grp_code, grp_name ";
    $sql .= "from inci_report_materials as mate ";
    $sql .= "join inci_easyinput_category_mst as cate on mate.cate_code = cate.cate_code ";
    $sql .= "join inci_easyinput_group_mst as grp on grp.grp_code = mate.grp_code ";
    $cond = "where easy_flag='1' and mate.cate_code > 0 and mate.cate_code = '7' order by cate_code, grp_code";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type='text/javascript' src='js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    $tmp_array = array();
    while($row = pg_fetch_array($sel))
    {
        $grp_code = $row["grp_code"];
        $tmp_array[$grp_code] = $row["grp_name"];
    }


    foreach($grp_index_list as $index)
    {
        foreach($tmp_array as $grp_code => $grp_name)
        {
            if($index == $grp_code)
            {
                $array[$grp_code] = $grp_name;
            }
        }
    }

    return $array;
}

function get_inci_auth_analysis_use_item($con, $fname, $auth)
{
    $array = array();

    $sql  = "select * from inci_auth_analysis_use_item";
    $cond = "where auth = '$auth' order by grp_code asc";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type='text/javascript' src='js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    while($row = pg_fetch_array($sel))
    {
        array_push($array, $row["grp_code"]);
    }

    return $array;

}

function set_inci_auth_analysis_use_item($con, $fname, $inputs)
{
    $inci_auths = $inputs["inci_auths"];
    $arr_auth = split(",", $inci_auths);

    $grp_codes = $inputs["grp_codes"];
    $arr_grp_code = split(",", $grp_codes);

    foreach($arr_auth as $auth)
    {
        // 削除処理
        delete_inci_auth_analysis_use_item($con, $fname, $auth);
        foreach($arr_grp_code as $grp_code)
        {
            $code = $auth."_".$grp_code;
            if($inputs[$code] == "t")
            {
                // 登録処理
                regist_inci_auth_analysis_use_item($con, $fname, $auth, $grp_code);
            }
        }
    }
}

function delete_inci_auth_analysis_use_item($con, $fname, $auth)
{
    $sql = "delete from inci_auth_analysis_use_item";
    $cond = "where auth = '$auth'";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

function regist_inci_auth_analysis_use_item($con, $fname, $auth, $grp_code)
{
    $sql = "insert into inci_auth_analysis_use_item (auth, grp_code) values (";
    $content = array($auth, $grp_code);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}
//==============================
//DBコネクション終了
//==============================
pg_close($con);
