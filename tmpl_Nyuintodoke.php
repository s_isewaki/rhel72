<?
require_once("read_xml.ini");
require_once("show_select_values.ini");
require_once("summary_euc_special_char.ini");

/**
 * 入力フォームを出力します。(テンプレートインターフェース関数)
 */
function write_tmpl_page()
{
?>
<script type="text/javascript">
function InputCheck() {
	
	//第34条による移送の有無
	if ((!document.tmplform.elements['Isou[]'][0].checked)&&(!document.tmplform.elements['Isou[]'][1].checked)) {
		alert('第34条による移送の有無が選択されていません');
		return false;
	}
	
	//主たる精神障害
	var checked = false;
	for (var i = 0; i < 12; i++) {
		if (document.tmplform["CheckShogai[" + i + "]"].checked) {
			checked = true;
			break;
		}
	}
	if (!checked) {
		alert('主たる精神障害が選択されていません');
		return false;
	}
	
	//現在の症状又は状態像
	var checked = false;
	for (var i = 0; i < 60; i++) {
		if (document.tmplform["CheckShojo[" + i + "]"].checked) {
			checked = true;
			break;
		}
	}
	if (!checked) {
		alert('現在の症状又は状態像が選択されていません');
		return false;
	}
	
	//if (confirm('コピーしますか？')) {
	//	document.tmplform.submit();
	//}
	return true;
}
</script>

<style type="text/css">
table.block {border-collapse:collapse;width:100%;}
table.block td {border:#5279a5 solid 1px;padding:2px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;width:100%}
table.block_in td {border:#5279a5 solid 0px;}
table.block_in td td {border-width:0;}

td.block_in_indent{width:20px;}

</style>

<table border='0' cellspacing='0' cellpadding='0' class="block">
<tr bgcolor='#bdd1e7'><td>
	<FONT face="MS UI Gothic" size="4">■入院年月日</FONT><BR>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3">・保護者の同意により入院した年月日</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<SELECT name="year1"><? show_years(40); ?></select>年
		<SELECT name="month1"><? show_months(); ?></select>月
		<SELECT name="day1"><? show_days(); ?></select>日<BR>
	</td></tr>
	</table>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3">・今回の入院年月日</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<SELECT name="year2"><? show_years(40); ?></select>年
		<SELECT name="month2"><? show_months(); ?></select>月
		<SELECT name="day2"><? show_days(); ?></select>日<BR>
		<FONT face="MS UI Gothic" size="3">入院形態</FONT>
		<INPUT id="Text1" style="WIDTH: 82px; HEIGHT: 22px" type="text" size="8" name="Text1"><BR>
	</td></tr>
	</table>
</td></tr>
</table>
<BR>

<table border='0' cellspacing='0' cellpadding='0' class="block">
<tr bgcolor='#bdd1e7'><td>
	<FONT face="MS UI Gothic" size="4">■第34条による移送の有無</FONT><BR>
</td></tr>
<tr><td>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT face="MS UI Gothic" size="3"><INPUT id="Radio1" type="radio" value="あり" name="Isou[]">あり</FONT><BR>
		<FONT face="MS UI Gothic" size="3"><INPUT id="Radio2" type="radio" value="なし" name="Isou[]">なし</FONT><BR>
	</td></tr>
	</table>
</td></tr>
</table>
<BR>

<table border='0' cellspacing='0' cellpadding='0' class="block">
<tr bgcolor='#bdd1e7'><td>
	<FONT face="MS UI Gothic" size="4">■病名</FONT><BR>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3">・主たる精神障害</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT size="3"><INPUT id="Checkbox1" type="checkbox" value="症状性を含む器質性精神障害" name="CheckShogai[0]">症状性を含む器質性精神障害</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox2" type="checkbox" value="精神作用物質使用による精神および行動の障害" name="CheckShogai[1]">精神作用物質使用による精神および行動の障害</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox3" type="checkbox" value="精神分裂病,分裂病型障害および妄想性障害" name="CheckShogai[2]">精神分裂病,分裂病型障害および妄想性障害</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox4" type="checkbox" value="気分(感情)障害" name="CheckShogai[3]">気分(感情)障害</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox5" type="checkbox" value="神経症性障害,ストレス関連障害および身体表現性障害" name="CheckShogai[4]">神経症性障害,ストレス関連障害および身体表現性障害</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox6" type="checkbox" value="生理的障害および身体的要因に関連した行動症候群" name="CheckShogai[5]">生理的障害および身体的要因に関連した行動症候群</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox7" type="checkbox" value="成人の人格および行動の障害" name="CheckShogai[6]">成人の人格および行動の障害</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox8" type="checkbox" value="精神遅滞" name="CheckShogai[7]">精神遅滞</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox9" type="checkbox" value="心理的発達の障害" name="CheckShogai[8]">心理的発達の障害</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox10" type="checkbox" value="小児(児童)期および青年期に通常発症する行動および情緒の障害" name="CheckShogai[9]">小児(児童)期および青年期に通常発症する行動および情緒の障害</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox11" type="checkbox" value="挿間性および発作性障害" name="CheckShogai[10]">挿間性および発作性障害</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox12" type="checkbox" value="その他" name="CheckShogai[11]">その他</FONT><INPUT id="Text2" type="text" name="Text2"><BR>
	</td></tr>
	</table>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3">・従たる精神障害</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<INPUT id="Text3" type="text" name="Text3"><BR>
	</td></tr>
	</table>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3">・身体合併症</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<INPUT id="Text4" type="text" name="Text4"><BR>
	</td></tr>
	</table>
</td></tr>
</table>
<BR>

<table border='0' cellspacing='0' cellpadding='0' class="block">
<tr bgcolor='#bdd1e7'><td>
	<FONT face="MS UI Gothic" size="4">■生活暦及び現病歴</FONT><BR>
</td></tr>
<tr><td>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<TEXTAREA id="Textarea1" style="WIDTH: 100%; HEIGHT: 144px" name="Textarea1" rows="9" cols="50"></TEXTAREA><BR>
	</td></tr>
	</table>
</td></tr>
</table>
<BR>

<table border='0' cellspacing='0' cellpadding='0' class="block">
<tr bgcolor='#bdd1e7'><td>
	<FONT face="MS UI Gothic" size="4">■入院期間</FONT><BR>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3">・初回入院期間</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<SELECT name="year3"><? show_years(40); ?></select>年
		<SELECT name="month3"><? show_months(); ?></select>月
		<SELECT name="day3"><? show_days(); ?></select>日 〜
		<SELECT name="year4"><? show_years(40); ?></select>年
		<SELECT name="month4"><? show_months(); ?></select>月
		<SELECT name="day4"><? show_days(); ?></select>日<BR>
		<FONT face="MS UI Gothic" size="3">入院形態</FONT>
		<INPUT id="Text5" style="WIDTH: 96px; HEIGHT: 22px" type="text" size="10" name="Text5"><BR>
	</td></tr>
	</table>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3">・前回入院期間</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<SELECT name="year5"><? show_years(40); ?></select>年
		<SELECT name="month5"><? show_months(); ?></select>月
		<SELECT name="day5"><? show_days(); ?></select>日 〜
		<SELECT name="year6"><? show_years(40); ?></select>年
		<SELECT name="month6"><? show_months(); ?></select>月
		<SELECT name="day6"><? show_days(); ?></select>日<BR>
		<FONT face="MS UI Gothic" size="3">入院形態</FONT>
		<INPUT id="Text6" style="WIDTH: 96px; HEIGHT: 22px" type="text" size="10" name="Text6"><BR>
	</td></tr>
	</table>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3">・初回から前回までの入院回数</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT face="MS UI Gothic" size="3">計</FONT>
		<INPUT id="Text7" style="WIDTH: 64px; HEIGHT: 20px" type="text" size="5" name="Text7">
		<FONT face="MS UI Gothic" size="3">回</FONT><BR>
	</td></tr>
	</table>
</td></tr>
</table>
<BR>

<table border='0' cellspacing='0' cellpadding='0' class="block">
<tr bgcolor='#bdd1e7'><td>
	<FONT face="MS UI Gothic" size="4">■現在の症状又は状態像</FONT><BR>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3"><?=euc_special_char("ROMAN_1")?>．抑うつ状態</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT size="3"><INPUT id="Checkbox21" type="checkbox" value="抑うつ気分" name="CheckShojo[0]">1抑うつ気分</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox22" type="checkbox" value="内的不穏" name="CheckShojo[1]">2内的不穏</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox23" type="checkbox" value="焦燥・激越" name="CheckShojo[2]">3焦燥・激越</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox24" type="checkbox" value="精神運動制止" name="CheckShojo[3]">4精神運動制止</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox25" type="checkbox" value="罪責感" name="CheckShojo[4]">5罪責感</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox26" type="checkbox" value="自殺念慮" name="CheckShojo[5]">6自殺念慮</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox27" type="checkbox" value="睡眠障害" name="CheckShojo[6]">7睡眠障害</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox28" type="checkbox" value="食欲障害又は重減少" name="CheckShojo[7]">8食欲障害又は重減少</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox29" type="checkbox" value="その他" name="CheckShojo[8]">9その他</FONT><INPUT id="Text8" type="text" name="Text8"><BR>
	</td></tr>
	</table>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3"><?=euc_special_char("ROMAN_2")?>．躁状態</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT size="3"><INPUT id="Checkbox30" type="checkbox" value="高揚気分" name="CheckShojo[9]">1高揚気分</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox31" type="checkbox" value="多弁・多動" name="CheckShojo[10]">2多弁・多動</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox32" type="checkbox" value="行為心迫" name="CheckShojo[11]">3行為心迫</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox33" type="checkbox" value="思考奔逸" name="CheckShojo[12]">4思考奔逸</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox34" type="checkbox" value="易怒性・被刺激性亢進" name="CheckShojo[13]">5易怒性・被刺激性亢進</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox35" type="checkbox" value="睡眠障害" name="CheckShojo[14]">6睡眠障害</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox36" type="checkbox" value="誇大性" name="CheckShojo[15]">7誇大性</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox37" type="checkbox" value="その他" name="CheckShojo[16]">8その他</FONT><INPUT id="Text9" type="text" name="Text9"><BR>
	</td></tr>
	</table>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3"><?=euc_special_char("ROMAN_3")?>．幻覚妄想状態</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT size="3"><INPUT id="Checkbox38" type="checkbox" value="幻覚" name="CheckShojo[17]">1幻覚</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox39" type="checkbox" value="妄想" name="CheckShojo[18]">2妄想</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox40" type="checkbox" value="させられ体験" name="CheckShojo[19]">3させられ体験</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox41" type="checkbox" value="思考形式の障害" name="CheckShojo[20]">4思考形式の障害</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox42" type="checkbox" value="奇異な行動" name="CheckShojo[21]">5奇異な行動</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox43" type="checkbox" value="その他" name="CheckShojo[22]">6その他</FONT><INPUT id="Text10" type="text" name="Text10"><BR>
	</td></tr>
	</table>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3"><?=euc_special_char("ROMAN_4")?>．精神運動興奮状態</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT size="3"><INPUT id="Checkbox44" type="checkbox" value="滅裂思考" name="CheckShojo[23]">1滅裂思考</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox45" type="checkbox" value="硬い表情・姿勢" name="CheckShojo[24]">2硬い表情・姿勢</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox46" type="checkbox" value="興奮状態" name="CheckShojo[25]">3興奮状態</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox47" type="checkbox" value="その他" name="CheckShojo[26]">4その他</FONT><INPUT id="Text11" type="text" name="Text11"><BR>
	</td></tr>
	</table>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3"><?=euc_special_char("ROMAN_5")?>．昏迷状態</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT size="3"><INPUT id="Checkbox48" type="checkbox" value="無言" name="CheckShojo[27]">1無言</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox49" type="checkbox" value="無動・無反応" name="CheckShojo[28]">2無動・無反応</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox50" type="checkbox" value="拒絶・拒食" name="CheckShojo[29]">3拒絶・拒食</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox51" type="checkbox" value="その他" name="CheckShojo[30]">4その他</FONT><INPUT id="Text12" type="text" name="Text12"><BR>
	</td></tr>
	</table>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3"><?=euc_special_char("ROMAN_6")?>．意識障害</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT size="3"><INPUT id="Checkbox52" type="checkbox" value="意識混濁" name="CheckShojo[31]">1意識混濁</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox53" type="checkbox" value="(夜間)せん妄" name="CheckShojo[32]">2(夜間)せん妄</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox54" type="checkbox" value="もうろう" name="CheckShojo[33]">3もうろう</FONT><BR>
	</td></tr>
	</table>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3"><?=euc_special_char("ROMAN_7")?>．知能障害</FONT><BR>
	<FONT face="MS UI Gothic" size="3"> A．精神遅滞</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT size="3"><INPUT id="Checkbox55" type="checkbox" value="軽度" name="CheckShojo[34]">1軽度</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox56" type="checkbox" value="中等度" name="CheckShojo[35]">2中等度</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox57" type="checkbox" value="重度" name="CheckShojo[36]">3重度</FONT><BR>
	</td></tr>
	</table>
	<FONT face="MS UI Gothic" size="3"> B．痴呆</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT size="3"><INPUT id="Checkbox58" type="checkbox" value="全体的" name="CheckShojo[37]">1全体的</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox59" type="checkbox" value="まだら(島状)" name="CheckShojo[38]">2まだら(島状)</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox60" type="checkbox" value="仮性" name="CheckShojo[39]">3仮性</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox61" type="checkbox" value="その他" name="CheckShojo[40]">4その他</FONT><INPUT id="Text13" type="text" name="Text13"><BR>
	</td></tr>
	</table>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3"><?=euc_special_char("ROMAN_8")?>．人格の病的状態</FONT><BR>
	<FONT face="MS UI Gothic" size="3"> A．人格障害</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT size="3"><INPUT id="Checkbox62" type="checkbox" value="妄想性" name="CheckShojo[41]">1妄想性</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox63" type="checkbox" value="衝動性" name="CheckShojo[42]">2衝動性</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox64" type="checkbox" value="演技性" name="CheckShojo[43]">3演技性</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox65" type="checkbox" value="回避性" name="CheckShojo[44]">4回避性</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox66" type="checkbox" value="その他" name="CheckShojo[45]">5その他</FONT><INPUT id="Text14" type="text" name="Text14"><BR>
	</td></tr>
	</table>
	<FONT face="MS UI Gothic" size="3"> B．残遺性人格変化</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT size="3"><INPUT id="Checkbox67" type="checkbox" value="欠陥状態" name="CheckShojo[46]">1欠陥状態</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox68" type="checkbox" value="無関心" name="CheckShojo[47]">2無関心</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox69" type="checkbox" value="無為" name="CheckShojo[48]">3無為</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox70" type="checkbox" value="その他" name="CheckShojo[49]">4その他</FONT><INPUT id="Text15" type="text" name="Text15"><BR>
	</td></tr>
	</table>
</td></tr>
<tr><td>
	<FONT face="MS UI Gothic" size="3"><?=euc_special_char("ROMAN_9")?>．その他</FONT><BR>
	<FONT face="MS UI Gothic" size="3"> A．性心理的障害</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT size="3"><INPUT id="Checkbox71" type="checkbox" value="フェティシズム" name="CheckShojo[50]">1フェティシズム</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox72" type="checkbox" value="サド・マゾヒズム" name="CheckShojo[51]">2サド・マゾヒズム</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox73" type="checkbox" value="小児愛" name="CheckShojo[52]">3小児愛</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox74" type="checkbox" value="その他" name="CheckShojo[53]">4その他</FONT><INPUT id="Text16" type="text" name="Text16"><BR>
	</td></tr>
	</table>
	<FONT face="MS UI Gothic" size="3"> B．薬物依存</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT size="3"><INPUT id="Checkbox75" type="checkbox" value="覚醒剤" name="CheckShojo[54]">1覚醒剤</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox76" type="checkbox" value="有機溶剤" name="CheckShojo[55]">2有機溶剤</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox77" type="checkbox" value="睡眠薬" name="CheckShojo[56]">3睡眠薬</FONT><BR>
		<FONT size="3"><INPUT id="Checkbox78" type="checkbox" value="その他" name="CheckShojo[57]">4その他</FONT><INPUT id="Text17" type="text" name="Text17"><BR>
	</td></tr>
	</table>
	<FONT face="MS UI Gothic" size="3"> C．アルコール症</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT size="3"><INPUT id="Checkbox79" type="checkbox" value="アルコール症" name="CheckShojo[58]">アルコール症</FONT><BR>
	</td></tr>
	</table>
	<FONT face="MS UI Gothic" size="3"> D．その他</FONT><BR>
	<table border='0' cellspacing='0' cellpadding='0' class="block_in">
	<tr><td class="block_in_indent">&nbsp</td><td>
		<FONT size="3"><INPUT id="Checkbox80" type="checkbox" value="その他" name="CheckShojo[59]">4その他</FONT><INPUT id="Text18" type="text" name="Text18"><BR>
	</td></tr>
	</table>
</td></tr>
</table>
<BR>

<input type="hidden" name="xml_creator" value="create_nyuintodoke_xml.php">
<?
}


/**
 * 初期表示データ設定スクリプトを出力します。(テンプレートインターフェース関数)
 * 
 * @param $xml_file データが保存されているXMLファイルを指定します。
 */
function write_tmpl_page_data_script($xml_file)
{
	if($xml_file != ""){
		echo("<script language=\"javascript\">");
		echo("function load_action(){\n");
		
		//XMLよりテンプレート初期表示値設定
		$XMLControl = getTmplControl($xml_file);
		while(list ($key, $val) = each($XMLControl)) {
		    if($key == "Isou"){
				echo("document.tmplform.elements['Isou[]'][".$val."].checked = true;\n");
			}else if((substr($key, 0, 11) == "CheckShogai")||(substr($key, 0, 10) == "CheckShojo")){
				echo("document.tmplform.elements['".$key."'].checked = true;\n");
			}else if(substr($key, 0, 8) == "Textarea"){
				$val = str_replace("\r\n", "\\r\\n", $val);
				$val = str_replace("\"","\\\"", $val);
				$val = str_replace("\'","\\\'", $val);
				echo("document.tmplform.elements['".$key."'].value = \"$val\";\n");
			}else if(($key == "year1")||($key == "year2")||($key == "year3")||($key == "year4")||($key == "year5")||($key == "year6")){
				$selYear = substr($val, 0, 4);
				$selMonth = substr($val, 5, 2);
				$selDay = substr($val, 8, 2);
				$index = substr($key, 4, 1);
				echo("document.tmplform.elements['".$key."'].value=\"".$selYear."\";\n");
				echo("document.tmplform.elements['month".$index."'].value=\"".$selMonth."\";\n");
				echo("document.tmplform.elements['day".$index."'].value=\"".$selDay."\";\n");
			}else{
				$val = str_replace("\"","\\\"", $val);
				$val = str_replace("\'","\\\'", $val);
				echo("document.tmplform.elements['".$key."'].value = \"".$val."\";\n");
			}
		}
		
		echo("}\n");
		echo("</script>\n");
	}
}



/**
 * フローシート一覧表示関数です。(テンプレートインターフェース関数)
 * この関数はテンプレートにおいて非必須です。
 * この関数を定義しない場合、フローシートには内容形式で表示されます。
 * 
 * @param array $xml_files  テンプレートデータXMLファイル名(string)の配列
 * @param int   $disp_count 表示件数
 */
function write_flowsheet_page($xml_files, $disp_count)
{
	require_once("summary_tmpl_util.ini");
	write_flowsheet_page_default($xml_files, $disp_count);
}



?>