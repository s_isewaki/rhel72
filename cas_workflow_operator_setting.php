<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("cas_workflow_common.ini");
require_once("cas_common.ini");
require_once("cas_application_workflow_common_class.php");
require_once("get_cas_title_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo ("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo ("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$checkauth = check_authority($session, $CAS_MANAGE_AUTH, $fname);
if ($checkauth == "0") {
	echo ("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo ("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$obj = new cas_application_workflow_common_class($con, $fname);

//====================================
// 登録更新処理
//====================================
if ($mode == "REGIST") {

	// トランザクションを開始
	pg_query($con, "begin");

	$obj->delete_cas_operator_employee();

	if ($disp_area_ids_1 != "") {
		$arr_disp_area_id = split(",", $disp_area_ids_1);
		foreach ($arr_disp_area_id as $emp_id) {
			$obj->regist_cas_operator_employee($emp_id);
		}
	}

	// トランザクションをコミット
	pg_query($con, "commit");
}

// 病棟評価表・職員参照範囲設定情報取得
$arr_operator_employee = $obj->get_cas_operator_employee();

$cas_title = get_cas_title_name();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cas_title?> | <?=$cas_title?>運用担当者設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
<?
foreach ($arr_operator_employee as $operator_employee) {
	$id = $operator_employee["emp_id"];
	$name = $operator_employee["emp_full_name"];
	if ($disp_area_1 != "") {
		$disp_area_1 .= ",";
		$disp_area_ids .= ",";
		$disp_area_names .= ",";
	}
	$disp_area_1 .= "<a href=\"javascript:delete_emp(\\'{$id}\\', \\'{$name}\\', \\'1\\');\">{$name}</a>";
	$disp_area_ids .= $id;
	$disp_area_names .= $name;
}
?>
	if (document.getElementById('disp_area_1')) {
		document.getElementById('disp_area_1').innerHTML   = '<?=$disp_area_1?>';
		document.getElementById('disp_area_ids_1').value   = '<?=$disp_area_ids?>';
		document.getElementById('disp_area_names_1').value = '<?=$disp_area_names?>';
	}
}

function delete_emp(del_emp_id, del_emp_nm, input_div) {
	if (confirm("「" + del_emp_nm + "」さんを削除します。よろしいですか？")) {
		ids   = "disp_area_ids_" + input_div;
		names = "disp_area_names_" + input_div;

		emp_ids = document.getElementById(ids).value;
		emp_nms = document.getElementById(names).value;

		new_emp_ids = "";
		new_emp_nms = "";

		cnt = 0;
		if (emp_ids != "") {
			arr_emp_id = emp_ids.split(",");
			arr_emp_nm = emp_nms.split(",");

			for(i=0; i<arr_emp_id.length; i++) {
				if (arr_emp_id[i] == del_emp_id) {
					continue;
				} else {
					if (cnt > 0) {
					    new_emp_ids += ",";
						new_emp_nms += ",";
					}
				    new_emp_ids += arr_emp_id[i];
					new_emp_nms += arr_emp_nm[i];
					cnt++;
				}
			}
		}
		document.getElementById(ids).value = new_emp_ids;
		document.getElementById(names).value = new_emp_nms;
		set_disp_area(new_emp_ids, new_emp_nms, input_div);
	}
}

// 子画面から呼ばれる関数
function add_target_list(input_div, input_emp_id, input_emp_nm) {
	ids   = "disp_area_ids_" + input_div;
	names = "disp_area_names_" + input_div;

	// 既存入力されている職員
	emp_ids = document.getElementById(ids).value;
	emp_nms = document.getElementById(names).value;

	// 子画面から入力された職員
	arr_input_emp_id   = input_emp_id.split(",");
	arr_input_emp_name = input_emp_nm.split(",");

	tmp_ids   = "";
	tmp_names = "";
	if (emp_ids != "") {
		arr_emp_id = emp_ids.split(",");

		for (i = 0; i < arr_input_emp_id.length; i++) {
			dpl_flg = false;
			for (j = 0; j < arr_emp_id.length; j++) {

				// 重複した場合
				if (arr_input_emp_id[i] == arr_emp_id[j]) {
					dpl_flg = true;
				}
			}
			if (!dpl_flg) {
				if (tmp_ids != "") {
					tmp_ids   += ",";
					tmp_names += ",";
				}
				tmp_ids   += arr_input_emp_id[i];
				tmp_names += arr_input_emp_name[i];
			}
		}
	} else {
		for (i = 0; i < arr_input_emp_id.length; i++) {
			if (tmp_ids != "") {
				tmp_ids   += ",";
				tmp_names += ",";
			}
			tmp_ids   += arr_input_emp_id[i];
			tmp_names += arr_input_emp_name[i];
		}
	}

	if (emp_ids != "" && tmp_ids != "") {
		emp_ids += ",";
	}
	emp_ids += tmp_ids;
	document.getElementById(ids).value = emp_ids;
	if (emp_nms != ""  && tmp_names != "") {
		emp_nms += ",";
	}
	emp_nms += tmp_names;
	document.getElementById(names).value = emp_nms;

	set_disp_area(emp_ids, emp_nms, input_div);
}

function set_disp_area(emp_id, emp_nm, input_div) {
	disp_area = "disp_area_" + input_div;

	arr_emp_id = emp_id.split(",");
	arr_emp_nm = emp_nm.split(",");
	disp_area_content = "";
	for (i = 0; i < arr_emp_id.length; i++) {
		if (i > 0) {
			disp_area_content += ',';
		}

		disp_area_content += "<a href=\"javascript:delete_emp('" + arr_emp_id[i] + "', '" + arr_emp_nm[i] + "', '" + input_div + "');\">";
		disp_area_content += arr_emp_nm[i];
		disp_area_content += "</a>";
	}
	document.getElementById(disp_area).innerHTML = disp_area_content;
}

var childwin = null;
function openEmployeeList(input_div) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = 'cas_emp_list.php';
	url += '?session=<?=$session?>&input_div=' + input_div;
	childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

function closeEmployeeList() {
	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;
}

function emp_clear(div) {
	if (document.getElementById('disp_area_' + div)) {
		document.getElementById('disp_area_' + div).innerHTML   = '';
		document.getElementById('disp_area_ids_' + div).value   = '';
		document.getElementById('disp_area_names_' + div).value = '';
	}
}

function regist() {
	if (confirm('登録します。よろしいですか？')) {
		document.wkfw.action = "cas_workflow_operator_setting.php?session=<?=$session?>&mode=REGIST";
		document.wkfw.submit();
    }
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#A0D25A solid 1px;}
.list td {border:#A0D25A solid 1px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="cas_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/s42.gif" width="32" height="32" border="0" alt="<? echo($cas_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="cas_application_menu.php?session=<? echo($session); ?>"><b><? echo($cas_title); ?></b></a> &gt; <a href="cas_workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="cas_application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<?
show_wkfw_menuitem($session, $fname, "");
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="wkfw" action="#" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<?
show_cas_option_menu($session, $fname);
?>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td width="75%" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><?=$cas_title?>運用担当者設定</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="50">
<td width="180" bgcolor="#E5F6CD" align="center">
<input type="button" value="職員名簿" style="width=5.5em;" onclick="openEmployeeList('1');"><br>
<input type="button" value="クリア" style="width=5.5em;" onclick="emp_clear('1');">
</td>
<td bgcolor="#FFFFFF">
<table width="100%" height="100%" border="0" cellspacing="3" cellpadding="0" class="non_in_list">
<tr>
<td style="border:#E5F6CD solid 1px;">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><span id="disp_area_1"></span></font>
</td>
</tr>
</table>
</td>
</tr>
<input type="hidden" name="disp_area_ids_1" value="" id="disp_area_ids_1">
<input type="hidden" name="disp_area_names_1" value="" id="disp_area_names_1">
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="right"><input type="button" value="登録" onclick="regist();"></td>
</tr>
</table>
</td>
<!-- right -->
</tr>
</table>
</form>
</td>
</tr>
</table>
</body>
</html>
<? pg_close($con); ?>
