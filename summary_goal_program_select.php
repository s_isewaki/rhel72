<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix メドレポート｜目標・治療プログラム選択</title>
<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 57, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

$therapy_type = isset($_REQUEST['therapy_type']) ? $_REQUEST['therapy_type'] : '';    // 療法種別
$group_code   = isset($_REQUEST['group_code']) ? $_REQUEST['group_code'] : '';        // グループコード
$code   = isset($_REQUEST['code']) ? $_REQUEST['code'] : '';                          // コード

// リハビリ目標一覧を取得
$rehabili_goal_data = getRehabiliGoalData($con, $therapy_type, $fname);
if ($rehabili_goal_data === false) {
    pg_close($con);
    js_error_exit();
}

// 選択されたリハビリ目標に該当する治療プログラム一覧を取得
if (!$group_code) {
    if ($rehabili_goal_data) {
        $group_code = $rehabili_goal_data[0]['group_code'];
    }
}

$cure_program_data = array();
if ($group_code) {
    $cure_program_data = getCureProgramData($con, $therapy_type, $group_code, $fname);
    if ($cure_program_data === false) {
        pg_close($con);
        js_error_exit();
    }
}

// データベースの切断
pg_close($con);

/**
 * リハビリ目標一覧データを取得
 * 
 * @param resource $con          データベース接続リソース
 * @param string   $therapy_type 療法種別('PT':理学療法 'OT':作業療法 'ST':言語聴覚療法 'PST':心理療法)
 * @param string   $fname        ページ名
 * 
 * @return mixed 成功時:障害名一覧/失敗時:false
 */
function getRehabiliGoalData($con, $therapy_type, $fname)
{
    $sql  =  "select sum_med_goal_group_mst.code,";
    $sql .= " sum_med_rehabili_goal_mst.caption,";
    $sql .= " sum_med_rehabili_goal_mst.sort_order,";
    // 複数ユーザーが同時に登録を行った場合、リハビリ目標項目が異なるグループで重複して登録される可能性があり、
    // グループコードが異なるだけの同一項目レコードが存在するかもしれないので、最小のグループコードを取得(グループコードが小さい方が優先される)
    $sql .= " min(sum_med_goal_group_mst.group_code) as group_code";
    $sql .= " from sum_med_goal_group_mst";
    $sql .= " left join sum_med_rehabili_goal_mst";
    $sql .= " on sum_med_goal_group_mst.therapy_type = sum_med_rehabili_goal_mst.therapy_type";
    $sql .= " and sum_med_goal_group_mst.code = sum_med_rehabili_goal_mst.code";
    $sql .= " where sum_med_goal_group_mst.therapy_type='$therapy_type'";
    $sql .= " and sum_med_goal_group_mst.data_type='G'";
    $sql .= " and sum_med_rehabili_goal_mst.is_disabled = 0";
    $sql .= " group by sum_med_goal_group_mst.code, sum_med_rehabili_goal_mst.caption, sum_med_rehabili_goal_mst.sort_order";    // グループコードの重複を除く為、グルーピング
    $sql .= " order by sum_med_rehabili_goal_mst.sort_order";
    $sel = select_from_table($con, $sql, "", $fname);
    
    if ($sel == 0) {
        return false;
    }
    
    $data = pg_fetch_all($sel);
    if (!$data) {
        $data = array();
    }
    return $data;
}

/**
 * 治療プログラム一覧データを取得
 * 
 * @param resource $con          データベース接続リソース
 * @param string   $therapy_type 療法種別('PT':理学療法 'OT':作業療法 'ST':言語聴覚療法 'PST':心理療法)
 * @param string   $group_code   グループコード
 * @param string   $fname        ページ名
 * 
 * @return mixed 成功時:障害名一覧/失敗時:false
 */
function getCureProgramData($con, $therapy_type, $group_code, $fname)
{
    $sql  =  "select sum_med_goal_group_mst.code,";
    $sql .= " sum_med_cure_program_mst.caption,";
    $sql .= " sum_med_cure_program_mst.sort_order";
    $sql .= " from sum_med_goal_group_mst";
    $sql .= " left join sum_med_cure_program_mst";
    $sql .= " on sum_med_goal_group_mst.therapy_type = sum_med_cure_program_mst.therapy_type";
    $sql .= " and sum_med_goal_group_mst.code = sum_med_cure_program_mst.code";
    $sql .= " where sum_med_goal_group_mst.therapy_type='$therapy_type'";
    $sql .= " and group_code=$group_code";
    $sql .= " and sum_med_goal_group_mst.data_type='P'";
    $sql .= " and sum_med_cure_program_mst.is_disabled = 0";
    $sql .= " order by sum_med_cure_program_mst.sort_order";
    $sel = select_from_table($con, $sql, "", $fname);
    
    if ($sel == 0) {
        return false;
    }
    
    $data = pg_fetch_all($sel);
    if (!$data) {
        $data = array();
    }
    return $data;
}
?>
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<script type="text/javascript" src="js/jquery/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="js/swapimg.js"></script>
<script type="text/javascript">
// リハビリ目標選択時の処理
function selectRehabiliGoal(therapy_type) {
  var selected = $('#rehabili_goal').val();
  if (!selected) {
    return;
  }
  var splits = selected.split('_');
  var group_code = splits[0];
  var code = splits[1];
  $('#mainform').append($('<input/>').attr({'type':'hidden', 'name':'therapy_type', 'value':therapy_type}));
  $('#mainform').append($('<input/>').attr({'type':'hidden', 'name':'group_code', 'value':group_code}));
  $('#mainform').append($('<input/>').attr({'type':'hidden', 'name':'code', 'value':code}));
  $('#mainform').submit();
}

// リハビリ目標・治療プログラム選択時の処理
function selectGoalCure() {
  var rehabili_goal = $('#rehabili_goal option:selected').text();
  var cure_program = $('#cure_program option:selected').text();
  if (opener && !opener.closed && opener.selectGoalCure) {
    opener.selectGoalCure(rehabili_goal, cure_program);
  }
}
</script>
</head>
<body style="background-color:#f4f4f4">
<div style="padding:4px">
<div>
  <table class="dialog_titletable"><tr>
    <th>目標・治療プログラム選択</th>
      <td><a href="javascript:window.close()"><img src="img/icon/close.gif" alt="閉じる"
        onmouseover="swapimg(this,'img/icon/closeo.gif');" onmouseout="swapimg(this,'img/icon/close.gif');" /></a></td>
  </tr></table>
</div>

<form id="mainform" action="summary_goal_program_select.php" method="post">
<input type="hidden" name="session" value="<? echo $session; ?>">

<table class="prop_table" cellspacing="1">
<tr>
<th style="text-align: left;">目標</th>
<th style="text-align: left;">治療プログラム</th>
</tr>
<tr>
  <td>
    <select style="width: 100%;" id="rehabili_goal" onchange="selectRehabiliGoal('<? echo $therapy_type; ?>');">
    <?
        foreach ($rehabili_goal_data as $value) {
            if (($value['group_code'] == $group_code) && ($value['code'] == $code)) {
                $selected = ' selected';
            } else {
                $selected = '';
            }
            echo "<option value=\"{$value['group_code']}_{$value['code']}\"$selected>{$value['caption']}</option>\n";
        }
    ?>
    </select>
  </td>
  <td>
    <select style="width: 100%;" id="cure_program">
    <?
        foreach ($cure_program_data as $value) {
            echo "<option>{$value['caption']}</option>\n";
        }
    ?>
    </select>
  </td>
</tr>
</table>
<div style="padding-top: 5px; text-align: right;"><input type="button" value="コピー" onclick="selectGoalCure();"></div>
</form>
</div>
</body>
</html>
