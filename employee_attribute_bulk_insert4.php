<?

ini_set("max_execution_time", 0);

require("about_session.php");
require("about_authority.php");
require("show_class_name.ini");
require("label_by_profile_type.ini");
require("get_values.ini");
require("employee_auth_common.php");
require_once("passwd_change_common.ini");
require_once("license_check_common.php");
//XX require_once("community/mainfile.php");
require_once("webmail/config/config.php");
require_once("webmail_quota_functions.php");

$fname = $PHP_SELF;


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}


// ファイルが正常にアップロードされたかどうかチェック
$uploaded = false;
if (array_key_exists("csvfile", $_FILES)) {
    if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
        $uploaded = true;
    }
}
if (!$uploaded) {
    echo("<script type=\"text/javascript\">alert('ファイルのアップロードに失敗しました。');</script>\n");
    echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
    exit;
}

// 文字コードの設定
switch ($encoding) {
    case "1":
        $file_encoding = "SJIS";
        break;
    case "2":
        $file_encoding = "EUC-JP";
        break;
    case "3":
        $file_encoding = "UTF-8";
        break;
    default:
        exit;
}

// EUC-JPで扱えない文字列は「・」に変換するよう設定
mb_substitute_character(0x30FB);

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// パスワードの長さ
$pass_length = get_pass_length($con, $fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 組織情報を取得
$arr_class_name = get_class_name_array($con, $fname);

$sql = "select class_id, class_nm from classmst";
$cond = "where class_del_flg = 'f' order by class_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$class_tree = array();
while ($row = pg_fetch_array($sel)) {
    $class_tree[$row["class_id"]] = array(
        "name" => $row["class_nm"], "attributes" => array()
    );
}

$sql = "select class_id, atrb_id, atrb_nm from atrbmst";
$cond = "where atrb_del_flg = 'f' order by atrb_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {
    $class_tree[$row["class_id"]]["attributes"][$row["atrb_id"]] = array(
        "name" => $row["atrb_nm"], "depts" => array()
    );
}

$sql = "select atrbmst.class_id, deptmst.atrb_id, deptmst.dept_id, deptmst.dept_nm from deptmst inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id";
$cond = "where deptmst.dept_del_flg = 'f' order by deptmst.dept_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {
    $class_tree[$row["class_id"]]["attributes"][$row["atrb_id"]]["depts"][$row["dept_id"]] = array(
        "name" => $row["dept_nm"], "rooms" => array()
    );
}

if ($arr_class_name[4] == 4) {
    $sql = "select atrbmst.class_id, deptmst.atrb_id, classroom.dept_id, classroom.room_id, classroom.room_nm from classroom inner join deptmst on classroom.dept_id = deptmst.dept_id inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id";
    $cond = "where classroom.room_del_flg = 'f' order by classroom.room_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $rooms = array();
    while ($row = pg_fetch_array($sel)) {
        $class_tree[$row["class_id"]]["attributes"][$row["atrb_id"]]["depts"][$row["dept_id"]]["rooms"][$row["room_id"]] = array("name" => $row["room_nm"]);
    }
}

// 役職情報を配列に格納
$sql = "select st_id, st_nm from stmst";
$cond = "where st_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$statuses = array();
while ($row = pg_fetch_array($sel)) {
    $statuses[$row["st_id"]] = $row["st_nm"];
}

// 職種情報を配列に格納
$sql = "select job_id, job_nm from jobmst";
$cond = "where job_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$jobs = array();
while ($row = pg_fetch_array($sel)) {
    $jobs[$row["job_id"]] = $row["job_nm"];
}

// 勤務条件
$sql = "select * from empcond";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$jobs = array();
while ($row = pg_fetch_array($sel)) {
    $jobs[$row["job_id"]] = $row["job_nm"];
}

// 環境設定情報を取得
$sql = "select site_id, use_cyrus from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$site_id = pg_fetch_result($sel, 0, "site_id");
$use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");

// 最低必要カラム数（最後の必須項目まで）
$column_count = ($arr_class_name[4] == 4) ? 22 : 21;

// 新規登録可能フラグ（１であれば新規登録可）
//$insert_flg = $_REQUEST['insert_flg'];

// CSVデータを配列に格納
$employees = array();
$employee_no = 1;
$lines = file($_FILES["csvfile"]["tmp_name"]);
foreach ($lines as $line) {

    // 文字コードを変換
    $line = trim(mb_convert_encoding($line, "EUC-JP", $file_encoding));

    // EOFを削除
    $line = str_replace(chr(0x1A), "", $line);

    // 空行は無視
    if ($line == "") {
        continue;
    }
    // カラム数チェック
    $employee_data = split(",", $line);
    $count = count($employee_data);
    if (count($employee_data) < $column_count) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('カラム数が不正です。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    // 登録値の編集
    $i = 0;
    $employee = array();
    $employee["auth_group"] = trim($employee_data[$i++]);      // 権限グループ
    //20130607 表示グループとメニューグループを追加 中嶌
    $employee["disp_group"] = trim($employee_data[$i++]);      // 表示グループ
    $employee["menu_group"] = trim($employee_data[$i++]);      // メニューグループ
    $employee["personal_id"] = trim($employee_data[$i++]);    // 職員ID
    $employee["id"] = trim($employee_data[$i++]);      // ログインID
    $employee["pass"] = trim($employee_data[$i++]);      // パスワード
    $employee["lt_nm"] = mb_convert_kana(trim($employee_data[$i++]), "s");  // 苗字
    $employee["ft_nm"] = mb_convert_kana(trim($employee_data[$i++]), "s");  // 名前
    $employee["lt_kana_nm"] = mb_convert_kana(trim($employee_data[$i++]), "HVcs"); // かな苗字
    $employee["ft_kana_nm"] = mb_convert_kana(trim($employee_data[$i++]), "HVcs"); // かな氏名
    $employee["mail_id"] = trim($employee_data[$i++]);     // メールID
    $employee["sex"] = trim($employee_data[$i++]);      // 性別
    $employee["birth"] = trim($employee_data[$i++]);     // 生年月日
    $employee["entry"] = trim($employee_data[$i++]);     // 入職日
    //中嶌 20130514 退職日を配列の中に格納
    $employee["retire"] = trim($employee_data[$i++]);
    $employee["cls_nm"] = trim($employee_data[$i++]);     // 第１階層
    $employee["atrb_nm"] = trim($employee_data[$i++]);     // 第２階層
    $employee["dept_nm"] = trim($employee_data[$i++]);     // 第３階層
    if ($arr_class_name[4] == "4") {
        $employee["room_nm"] = trim($employee_data[$i++]);    // 第４階層
    }
    $employee["status_nm"] = trim($employee_data[$i++]);    // 役職
    $employee["job_nm"] = trim($employee_data[$i++]);     // 職種
    $employee["del_flg"] = trim($employee_data[$i++]);      // 利用停止フラグ
    $emp_zip = trim($employee_data[$i++]);       // 郵便番号
    $emp_zip = explode("-", $emp_zip);
    $employee["zip1"] = $emp_zip[0];            // 郵便番号１
    $employee["zip2"] = $emp_zip[1];            // 郵便番号2
    $prv = trim($employee_data[$i++]);         // 都道府県
    if ($prv != "") $employee["prv"] = prv_change($prv) - 1;
    else $employee["prv"] == "";
    $employee["addr1"] = trim($employee_data[$i++]);       // 住所１
    $employee["addr2"] = trim($employee_data[$i++]);       // 住所２
    $tel = trim($employee_data[$i++]);         // 電話番号
    $tel = explode("-", $tel);
    $employee["tel1"] = $tel[0];
    $employee["tel2"] = $tel[1];
    $employee["tel3"] = $tel[2];
    $employee["email"] = trim($employee_data[$i++]);       // E-Mail

    $mobile = trim($employee_data[$i++]);      // 携帯電話番号
    $mobile = explode("-", $mobile);
    $employee["mobile1"] = $mobile[0];
    $employee["mobile2"] = $mobile[1];
    $employee["mobile3"] = $mobile[2];
    $employee["m_email"] = trim($employee_data[$i++]);     // 携帯Mail
    $employee["ext"] = trim($employee_data[$i++]);         // 内線番号
    $employee["phs"] = trim($employee_data[$i++]);         // 院内PHS
    $employee["wage"] = trim($employee_data[$i++]);            // 給与支給区分(1:月給制 3:日給制 4:時間給制 5:年俸制)
    $employee["group_name"] = trim($employee_data[$i++]);      // 出勤パターンのグループ
    $employee["duty_form"] = trim($employee_data[$i++]);    // 雇用・勤務形態(1:常勤 2:非常勤 3:短時間正職員)

    $employee["emp_name_deadlink_flg"] = trim($employee_data[$i++]);
    $employee["emp_class_deadlink_flg"] = trim($employee_data[$i++]);
    $employee["emp_job_deadlink_flg"] = trim($employee_data[$i++]);
    $employee["emp_st_deadlink_flg"] = trim($employee_data[$i++]);

    $personal_id = $employee["personal_id"];


    // 現在のemp_idを取得
    $sql = "select emp_id from empmst";
    $cond = "where emp_personal_id = '$personal_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $emp_id = pg_fetch_result($sel, 0, "emp_id");

    // 現在のログインID・パスワード・メールIDを取得
    $sql = "select emp_login_id, emp_login_pass, emp_login_mail from login";
    $cond = "where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $employee["cur_id"] = pg_fetch_result($sel, 0, "emp_login_id");
    $employee["cur_pass"] = pg_fetch_result($sel, 0, "emp_login_pass");
    $employee["cur_mail_id"] = pg_fetch_result($sel, 0, "emp_login_mail");

    // パスワードがマスクされた状態（アスタリスク４つ）であれば現在のパスワードを入れる
    if ($employee["pass"] == '****') {
        $employee["pass"] = $employee["cur_pass"];
    }

    // 現在の連携不可フラグを取得
    $sql_rel = sprintf("SELECT * FROM emp_relation WHERE emp_id='%s'", pg_escape_string($emp_id));
    $sel_rel = select_from_table($con, $sql_rel, "", $fname);
    if ($sel_rel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel_rel) > 0) {
        if (empty($employee["emp_name_deadlink_flg"])) {
            $employee["emp_name_deadlink_flg"] = pg_fetch_result($sel_rel, 0, "emp_name_deadlink_flg");
        }
        if (empty($employee["emp_class_deadlink_flg"])) {
            $employee["emp_class_deadlink_flg"] = pg_fetch_result($sel_rel, 0, "emp_class_deadlink_flg");
        }
        if (empty($employee["emp_job_deadlink_flg"])) {
            $employee["emp_job_deadlink_flg"] = pg_fetch_result($sel_rel, 0, "emp_job_deadlink_flg");
        }
        if (empty($employee["emp_st_deadlink_flg"])) {
            $employee["emp_st_deadlink_flg"] = pg_fetch_result($sel_rel, 0, "emp_st_deadlink_flg");
        }
    }

    $employees[$employee_no] = $employee;
    $employee_no++;
}

// CSVのデータチェック

$extra_ids = array();
foreach ($employees as $employee_no => $employee_data) {
    $extra_id = check_csv($con, $employee_no, $employee_data, $arr_class_name[4], $class_tree, $arr_class_name, $employee["status_nm"], $employee["job_nm"], $session, $fname, $site_id, $use_cyrus, $insert_flg);
    if ($extra_id != "") {
        $extra_ids[] = $extra_id . "\\n";
    }
}

// 職員データを登録
foreach ($employees as $employee_no => $employee_data) {

    // 職員ID
    $personal_id = $employee_data["personal_id"];

    // トランザクションを開始
    // pg_query($con, "begin");
    // 当該職員が存在するかチェック
    $sql = "select count(*) from empmst";
    $cond = "where emp_personal_id = '$personal_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    if ($insert_flg == 1) {
        if (intval(pg_fetch_result($sel, 0, 0)) == 0) {
            insert_employee_data($con, $employee_no, $employee_data, $arr_class_name[4], $class_tree, $arr_class_name, $employee["status_nm"], $employee["job_nm"], $session, $fname, $site_id, $use_cyrus);

            // メールアカウントの作成
            if ($use_cyrus == "t") {
                $dir = getcwd();
                make_mail_account($employee_data, $site_id, $dir, $imapServerAddress);
            }
        }
        else {
            update_employee_data($con, $employee_no, $employee_data, $arr_class_name[4], $class_tree, $arr_class_name, $employee["status_nm"], $employee["job_nm"], $session, $fname, $site_id, $use_cyrus, $imapServerAddress);
        }
    }
    else {
        if (intval(pg_fetch_result($sel, 0, 0)) != 0) {
            update_employee_data($con, $employee_no, $employee_data, $arr_class_name[4], $class_tree, $arr_class_name, $employee["status_nm"], $employee["job_nm"], $session, $fname, $site_id, $use_cyrus, $imapServerAddress);
        }
    }

    // ライセンスのチェック
    $result = check_license($con, $fname);
    if (!$result["ok"]) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('{$result["message"]}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
}

// トランザクションをコミット
pg_query($con, "commit");


// データベース接続を閉じる
pg_close($con);

// 存在しない職員IDが指定されていたらalert
if ($insert_flg == "") {
    if (count($extra_ids) > 0) {
        $tmp_extra_ids = "・" . implode("\\・", $extra_ids);
        echo("<script type=\"text/javascript\">alert('下記の職員は存在しないため、無視されました。\\n$tmp_extra_ids');</script>\n");
    }
}
// 成功画面を表示
echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=t&encoding=$encoding';</script>");

// CSVファイルのデータチェック
function check_csv($con, $employee_no, $employee_data, $class_cnt, $class_tree, $arr_class_name, $statuses, $jobs, $session, $fname, $site_id, $use_cyrus, $insert_flg)
{

    global $pass_length;
    global $_label_by_profile;
    global $profile_type;

    $i = 0;
    $auth_group = $employee_data["auth_group"];    // 権限グループ
    //20130607 表示グループとメニューグループを追加 中嶌
    $disp_group = $employee_data["disp_group"];    // 表示グループ
    $menu_group = $employee_data["menu_group"];    // メニュグループ
    $personal_id = $employee_data["personal_id"];  // 職員ID
    $id = $employee_data["id"];               // ログインID
    $pass = $employee_data["pass"];               // パスワード
    $lt_nm = $employee_data["lt_nm"];              // 苗字
    $ft_nm = $employee_data["ft_nm"];              // 氏名
    $lt_kana_nm = $employee_data["lt_kana_nm"];    // かな苗字
    $ft_kana_nm = $employee_data["ft_kana_nm"];    // かな氏名
    $mail_id = $employee_data["mail_id"];          // メールID
    $sex = $employee_data["sex"];                 // 性別
    $birth = $employee_data["birth"];             // 生年月日
    $entry = $employee_data["entry"];             // 入職日
    //中嶌 20130514 退職日を変数に格納
    $retire = $employee_data["retire"];
    $class = $employee_data["cls_nm"];             // 第１階層
    $atrb = $employee_data["atrb_nm"];             // 第２階層
    $dept = $employee_data["dept_nm"];             // 第３階層
    if ($arr_class_name[4] == "4") {
        $room = $employee_data["room_nm"];         // 第４階層
    }
    else {
        $room = "";
    }
    $status = $employee_data["status_nm"];          // 役職
    $job = $employee_data["job_nm"];             // 職種
    $del_flg = $employee_data["del_flg"];     // 利用停止フラグ
    $zip1 = $employee_data["zip1"];         // 郵便番号１
    $zip2 = $employee_data["zip2"];         // 郵便番号2
    $prv = $employee_data["prv"];         // 都道府県
    $addr1 = $employee_data["addr1"];       // 住所１
    $addr2 = $employee_data["addr2"];       // 住所２
    $tel1 = $employee_data["tel1"];         // 電話番号1
    $tel2 = $employee_data["tel2"];         // 電話番号2
    $tel3 = $employee_data["tel3"];         // 電話番号3
    $email = $employee_data["email"];       // E-Mail
    $mobile1 = $employee_data["mobile1"];      // 携帯電話番号1
    $mobile2 = $employee_data["mobile2"];      // 携帯電話番号2
    $mobile3 = $employee_data["mobile3"];      // 携帯電話番号3
    $m_email = $employee_data["m_email"];     // 携帯Mail
    $ext = $employee_data["ext"];         // 内線番号
    $phs = $employee_data["phs"];         // 院内PHS
    $wage = $employee_data["wage"];            // 給与支給区分(1:月給制 3:日給制 4:時間給制 5:年俸制)
    $group_name = $employee_data["group_name"];     // 出勤パターンのグループ
    $duty_form = $employee_data["duty_form"];    // 雇用・勤務形態(1:常勤 2:非常勤 3:短時間正社員)
    // 当該職員が存在するかチェック
    $sql = "select count(*) from empmst";
    $cond = "where emp_personal_id = '$personal_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if ($insert_flg == 0) {
        if (intval(pg_fetch_result($sel, 0, 0)) == 0) {
            return $personal_id;
        }
    }

    // 入力チェック
    if ($auth_group != "") {
        $sql = "select count(*) from authgroup";
        $cond = "where group_nm = '$auth_group'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        if (intval(pg_fetch_result($sel, 0, 0)) == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('指定された権限グループは登録されていません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
    }
    //20130607 表示グループが存在するかチェック 中嶌
    if ($disp_group != "") {
        $sql = "select count(*) from dispgroup";
        $cond = "where group_nm = '$disp_group'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        if (intval(pg_fetch_result($sel, 0, 0)) == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('指定された表示グループは登録されていません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
    }
    //20130607 メニューグループが存在するかチェック 中嶌
    if ($menu_group != "") {
        $sql = "select count(*) from menugroup";
        $cond = "where group_nm = '$menu_group'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        if (intval(pg_fetch_result($sel, 0, 0)) == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('指定されたメニューグループは登録されていません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
    }


    if ($personal_id == "") {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('職員IDを入力してください。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if (strlen($personal_id) > 12) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('職員IDが長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    $sql = "select count(*) from empmst";
    $cond = "where emp_personal_id = '$personal_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if ($id == "") {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('ログインIDを入力してください。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if (strlen($id) > 20) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('ログインIDが長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if ($site_id == "") {
        if (preg_match("/[^0-9a-z_-]/", $id) > 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('ログインIDに使える文字は半角の「0〜9」「a〜z」「_」「-」のみです。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
    }
    else {
        if (preg_match("/[^0-9a-zA-Z_-]/", $id) > 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('ログインIDに使える文字は半角の「0〜9」「a〜z」「A〜Z」「_」「-」のみです。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
    }
    if (substr($id, 0, 1) == "-") {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('ログインIDの先頭に「-」は使えません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if ($site_id == "") {
        if ($id == "cyrus" || $id == "postmaster" || $id == "root") {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('「{$id}」はログインIDとして使用できません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
    }

    if ($pass == "") {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('パスワードを入力してください。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if (strlen($pass) > $pass_length) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('パスワードが長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if (preg_match("/[^0-9a-zA-Z_-]/", $pass) > 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('パスワードに使える文字は半角の「0〜9」「a〜z」「A〜Z」「_」「-」のみです。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if (substr($pass, 0, 1) == "-") {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('パスワードの先頭に「-」は使えません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if ($id == $pass) {
        $sql = "select weak_pwd_flg from config";
        $cond = "";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        if (pg_fetch_result($sel, 0, "weak_pwd_flg") == "f") {
            echo("<script type=\"text/javascript\">alert('ログインIDと同じパスワードは使用できません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
    }
    if ($lt_nm == "") {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('苗字（漢字）を入力してください。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if (strlen($lt_nm) > 20) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('苗字（漢字）が長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if ($ft_nm == "") {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('名前（漢字）を入力してください。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if (strlen($ft_nm) > 20) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('名前（漢字）が長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if ($lt_kana_nm == "") {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('苗字（ひらがな）を入力してください。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if (strlen($lt_kana_nm) > 20) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('苗字（ひらがな）が長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if ($ft_kana_nm == "") {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('名前（ひらがな）を入力してください。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if (strlen($ft_kana_nm) > 20) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('名前（ひらがな）が長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if ($site_id != "") {
        if ($mail_id == "") {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('メールIDを入力してください。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }

        if (strlen($mail_id) > 20) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('メールIDが長すぎます。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }

        if ($use_cyrus == "t") {
            if (preg_match("/[^0-9a-z_-]/", $mail_id) > 0) {
                pg_close($con);
                echo("<script type=\"text/javascript\">alert('メールIDに使える文字は半角の「0〜9」「a〜z」「_」「-」のみです。レコード番号：{$employee_no}');</script>\n");
                echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
                exit;
            }
        }
        else {
            if (preg_match("/[^0-9a-z_.-]/", $mail_id) > 0) {
                pg_close($con);
                echo("<script type=\"text/javascript\">alert('メールIDに使える文字は半角の「0〜9」「a〜z」「_」「-」「.」のみです。レコード番号：{$employee_no}');</script>\n");
                echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
                exit;
            }
        }

        if (substr($mail_id, 0, 1) == "-") {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('メールIDの先頭に「-」は使えません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }

        $sql = "select empmst.emp_personal_id from login inner join authmst on login.emp_id = authmst.emp_id inner join empmst on login.emp_id = empmst.emp_id";
        $cond = "where login.emp_login_mail = '$mail_id' and authmst.emp_del_flg = 'f'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        // 登録前と登録後の職員ID(emp_personal_id)が同じ場合は、mail_id重複と見なさない
        if ((pg_num_rows($sel) != 0) && (pg_fetch_result($sel, 0, "emp_personal_id") != $personal_id)) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('指定されたメールIDは既に使用されています。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
    }
    if ($sex != "1" && $sex != "2" && $sex != "3") {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('性別が不正です。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if ($birth != "") {
        if (preg_match("/^\d{8}$/", $birth) == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('生年月日が不正です。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
        else {
            $birth_yr = substr($birth, 0, 4);
            $birth_mon = substr($birth, 4, 2);
            $birth_day = substr($birth, 6, 2);
            if (!checkdate($birth_mon, $birth_day, $birth_yr)) {
                pg_close($con);
                echo("<script type=\"text/javascript\">alert('生年月日が不正です。レコード番号：{$employee_no}');</script>\n");
                echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
                exit;
            }
        }
    }
    if ($entry != "") {
        if (preg_match("/^\d{8}$/", $entry) == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('入職日が不正です。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
        else {
            $entry_yr = substr($entry, 0, 4);
            $entry_mon = substr($entry, 4, 2);
            $entry_day = substr($entry, 6, 2);
            if (!checkdate($entry_mon, $entry_day, $entry_yr)) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\">alert('入職日が不正です。レコード番号：{$employee_no}');</script>\n");
                echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
                exit;
            }
        }
    }

    //中嶌 20130514 変数に保存した退職日が不正が無いかチェックを行う
    if ($retire != "") {
        if (preg_match("/^\d{8}$/", $retire) == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('退職日が不正です。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
        else {
            $retire_yr = substr($retire, 0, 4);
            $retire_mon = substr($retire, 4, 2);
            $retire_day = substr($retire, 6, 2);
            if (!checkdate($retire_mon, $retire_day, $retire_yr)) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\">alert('退職日が不正です。レコード番号：{$employee_no}');</script>\n");
                echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
                exit;
            }
        }
    }

    // 部門IDの取得
    $sql = "select class_id from classmst";
    $cond = "where class_nm = '$class' and class_del_flg = 'f'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[0]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $class = pg_fetch_result($sel, 0, "class_id");

    // 課IDの取得
    $sql = "select atrb_id from atrbmst";
    $cond = "where class_id = $class and atrb_nm = '$atrb' and atrb_del_flg = 'f'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[1]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $atrb = pg_fetch_result($sel, 0, "atrb_id");

    // 科IDの取得
    $sql = "select dept_id from deptmst";
    $cond = "where atrb_id = $atrb and dept_nm = '$dept' and dept_del_flg = 'f'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[2]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $dept = pg_fetch_result($sel, 0, "dept_id");

    // 室IDの取得
    if ($arr_class_name[4] == "4" && $room != "") {
        $sql = "select room_id from classroom";
        $cond = "where dept_id = $dept and room_nm = '$room' and room_del_flg = 'f'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        if (pg_num_rows($sel) == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[3]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
        $room = pg_fetch_result($sel, 0, "room_id");
    }
    else {
        $room = null;
    }

    // 役職IDの取得
    $sql = "select st_id from stmst";
    $cond = "where st_nm = '$status' and st_del_flg = 'f'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('指定された役職({$status})は登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $status = pg_fetch_result($sel, 0, "st_id");

    // 職種IDの取得
    $sql = "select job_id from jobmst";
    $cond = "where job_nm = '$job' and job_del_flg = 'f'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('指定された職種({$job})は登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $job = pg_fetch_result($sel, 0, "job_id");

    // 利用停止フラグ
    if ($del_flg == "") {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('利用停止フラグを入力してください。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    if ($del_flg != "t" && $del_flg != "f") {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('利用停止フラグが不正です。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    // 郵便番号
    if (strlen($zip1) > 3 || strlen($zip2) > 4) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('郵便番号が長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    if (strlen($zip1) + strlen($zip2) > 8) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('郵便番号が長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    // 都道府県
    if ($prv < 0 || $prv > 47) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('該当する都道府県名はありません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    // 住所１
    if (strlen($addr1) > 40) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('住所１が長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    // 住所２
    if (strlen($addr2) > 40) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('住所２が長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    // 電話番号
    if (strlen($tel1) > 6 or strlen($tel2) > 6 or strlen($tel3) > 6) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('電話番号が長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    // E-Mail
    if (strlen($email) > 120) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('E-Mailが長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    // 携帯番号・外線
    if (strlen($mobile1) > 6 or strlen($mobile2) > 6 or strlen($mobile3) > 6) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('携帯番号・外線が長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    // 携帯E-Mail
    if (strlen($m_email) > 120) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('携帯E-Mailが長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    // 内線番号
    if (strlen($ext) > 10) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('内線番号が長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    // 院内PHS
    if (strlen($phs) > 6) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('院内PHS番号が長すぎます。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    if ($wage != "" && $group_name != "" && $duty_form != "") {
        // 給与支給区分
        if ($wage == '月給制') $wage_cd = 1;
        else if ($wage == '日給制') $wage_cd = 3;
        else if ($wage == '時間給制') $wage_cd = 4;
        else if ($wage == '年俸制') $wage_cd = 5;
        else {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('該当する給与支給区分がありません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }

        // 出勤パターンのグループ
        $sql = "select * from wktmgrp";
        $cond = "where group_name = '$group_name'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('該当する出勤パターン名が登録されていません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
        $row2 = pg_fetch_array($sel);
        $group_id = $row2["group_id"];
        // 雇用・勤務形態
        if ($duty_form == '常勤') {
            $duty_form = 1;
        }
        else if ($duty_form == '非常勤') {
            $duty_form = 2;
        }
        else if ($duty_form == '短時間正職員') {
            $duty_form = 3;
        }
        else if ($duty_form == '') exit;
        else {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('雇用・勤務形態の値が正しくありません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
    }
    else if ($wage == "" && $group_name == "" && $duty_form == "") {

    }
    else {
        $comment = '給与支給区分、出勤パターン、雇用・勤務形態を登録する場合は給与支給区分、出勤パターン、雇用・勤務形態のすべてにデータを入力してください。';
        echo("<script type=\"text/javascript\">alert('{$comment}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
}

// 職員データを更新
function update_employee_data($con, $employee_no, $employee_data, $class_cnt, $class_tree, $arr_class_name, $statuses, $jobs, $session, $fname, $site_id, $use_cyrus, $imapServerAddress)
{

    global $pass_length;
    global $_label_by_profile;
    global $profile_type;

    $i = 0;
    $auth_group = $employee_data["auth_group"];    // 権限グループ
    //20130607 表示グループとメニューグループを追加 中嶌
    $disp_group = $employee_data["disp_group"];    // 表示グループ
    $menu_group = $employee_data["menu_group"];    // メニューグループ
    $personal_id = $employee_data["personal_id"];  // 職員ID
    $id = $employee_data["id"];               // ログインID
    $pass = $employee_data["pass"];               // パスワード
    $lt_nm = $employee_data["lt_nm"];              // 苗字
    $ft_nm = $employee_data["ft_nm"];              // 氏名
    $lt_kana_nm = $employee_data["lt_kana_nm"];    // かな苗字
    $ft_kana_nm = $employee_data["ft_kana_nm"];    // かな氏名
    $mail_id = $employee_data["mail_id"];          // メールID
    $sex = $employee_data["sex"];                 // 性別
    $birth = $employee_data["birth"];             // 生年月日
    $entry = $employee_data["entry"];             // 入職日
    //中嶌 20130514 退職日を変数に格納
    $retire = $employee_data["retire"];
    $class = $employee_data["cls_nm"];             // 第１階層
    $atrb = $employee_data["atrb_nm"];             // 第２階層
    $dept = $employee_data["dept_nm"];             // 第３階層
    if ($arr_class_name[4] == "4") {
        $room = $employee_data["room_nm"];         // 第４階層
    }
    else {
        $room = "";
    }
    $status = $employee_data["status_nm"];          // 役職
    $job = $employee_data["job_nm"];             // 職種
    $del_flg = $employee_data["del_flg"];     // 利用停止フラグ
    $zip1 = $employee_data["zip1"];         // 郵便番号１
    $zip2 = $employee_data["zip2"];         // 郵便番号2
    $prv = $employee_data["prv"];         // 都道府県
    $addr1 = $employee_data["addr1"];       // 住所１
    $addr2 = $employee_data["addr2"];       // 住所２
    $tel1 = $employee_data["tel1"];         // 電話番号1
    $tel2 = $employee_data["tel2"];         // 電話番号2
    $tel3 = $employee_data["tel3"];         // 電話番号3
    $email = $employee_data["email"];       // E-Mail
    $mobile1 = $employee_data["mobile1"];      // 携帯電話番号1
    $mobile2 = $employee_data["mobile2"];      // 携帯電話番号2
    $mobile3 = $employee_data["mobile3"];      // 携帯電話番号3
    $m_email = $employee_data["m_email"];     // 携帯Mail
    $ext = $employee_data["ext"];         // 内線番号
    $phs = $employee_data["phs"];         // 院内PHS
    $wage = $employee_data["wage"];            // 給与支給区分(1:月給制 3:日給制 4:時間給制 5:年俸制)
    $group_name = $employee_data["group_name"];     // 出勤パターンのグループ
    $duty_form = $employee_data["duty_form"];    // 雇用・勤務形態(1:常勤 2:非常勤 3:短時間正職員)
    // 権限グループのIDを取得
    $sql = "select * from authgroup";
    $cond = "where group_nm = '$auth_group'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('該当する権限グループが登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $row2 = pg_fetch_array($sel);
    $auth_group_id = $row2["group_id"];

    // 表示グループのIDを取得 中嶌
    $sql = "select * from dispgroup";
    $cond = "where group_nm = '$disp_group'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        //削除予定
        //echo("<script type=\"text/javascript\">alert('該当する表示グループが登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $row2 = pg_fetch_array($sel);
    $disp_group_id = $row2["group_id"];

    // メニューグループのIDを取得 中嶌
    $sql = "select * from menugroup";
    $cond = "where group_nm = '$menu_group'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        //削除予定
        //echo("<script type=\"text/javascript\">alert('該当するメニューグループが登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $row2 = pg_fetch_array($sel);
    $menu_group_id = $row2["group_id"];

    // emp_idの取得
    $sql = "select emp_id from empmst";
    $cond = "where emp_personal_id = '$personal_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $emp_id = pg_fetch_result($sel, 0, "emp_id");

    if ($group_name != "") {
        // 出勤パターンのグループ
        $sql = "select * from wktmgrp";
        $cond = "where group_name = '$group_name'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $row2 = pg_fetch_array($sel);
        $group_id = $row2["group_id"];

        if ($group_id == "") {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('該当する出勤パターン名が登録されていません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
    }
    // 部門IDの取得
    $sql = "select class_id from classmst";
    $cond = "where class_nm = '$class' and class_del_flg = 'f'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[0]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $class = pg_fetch_result($sel, 0, "class_id");

    // 課IDの取得
    $sql = "select atrb_id from atrbmst";
    $cond = "where class_id = $class and atrb_nm = '$atrb' and atrb_del_flg = 'f'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[1]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $atrb = pg_fetch_result($sel, 0, "atrb_id");

    // 科IDの取得
    $sql = "select dept_id from deptmst";
    $cond = "where atrb_id = $atrb and dept_nm = '$dept' and dept_del_flg = 'f'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[2]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $dept = pg_fetch_result($sel, 0, "dept_id");

    // 室IDの取得
    if ($arr_class_name[4] == "4" && $room != "") {
        $sql = "select room_id from classroom";
        $cond = "where dept_id = $dept and room_nm = '$room' and room_del_flg = 'f'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        if (pg_num_rows($sel) == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[3]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
        $room = pg_fetch_result($sel, 0, "room_id");
    }
    else {
        $room = null;
    }

    // 役職IDの取得
    $sql = "select st_id from stmst";
    $cond = "where st_nm = '$status' and st_del_flg = 'f'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $status = pg_fetch_result($sel, 0, "st_id");

    // 職種IDの取得
    $sql = "select job_id from jobmst";
    $cond = "where job_nm = '$job' and job_del_flg = 'f'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $job = pg_fetch_result($sel, 0, "job_id");

    // ログインIDが変更される場合、変更後のログインIDが重複しないかチェックする
    $sql = "select * from login";
    $cond = "where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $id2 = pg_fetch_result($sel, 0, "emp_login_id");

    if ($id != $id2) {
        $sql = "select count(*) from login";
        $cond = "where emp_login_id = '$id'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('指定されたログインIDは既に使用されています。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
    }

    // 職員情報更新処理
    $sql = "update empmst set";
    $set = array("emp_class", "emp_attribute", "emp_dept", "emp_room", "emp_job", "emp_st", "emp_lt_nm", "emp_ft_nm", "emp_kn_lt_nm", "emp_kn_ft_nm", "emp_sex", "emp_birth", "emp_join", "emp_retire", "emp_keywd", "emp_zip1", "emp_zip2", "emp_prv", "emp_addr1", "emp_addr2", "emp_tel1", "emp_tel2", "emp_tel3", "emp_email2", "emp_mobile1", "emp_mobile2", "emp_mobile3", "emp_m_email", "emp_ext", "emp_phs");
    $setvalue = array($class, $atrb, $dept, $room, $job, $status, $lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $sex, $birth, $entry, $retire, get_keyword($lt_kana_nm), $zip1, $zip2, $prv, $addr1, $addr2, $tel1, $tel2, $tel3, $email, $mobile1, $mobile2, $mobile3, $m_email, $ext, $phs);
    $cond = "where emp_personal_id = '$personal_id'";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // メールアカウントの更新
    if ($use_cyrus == "t") {
        $dir = getcwd();
        update_mail_account($employee_data, $site_id, $dir, $imapServerAddress);
    }

    // loginデータを更新
    $sql = "update login set";
    $set = array("emp_login_id", "emp_login_pass", "emp_login_mail");
    $setvalue = array($id, $pass, $mail_id);
    $cond = "where emp_id = '$emp_id'";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    //---------- emp_relation ----------
    if (version_compare(PHP_VERSION, '5.0.0') >= 0 && substr(pg_parameter_status($con, 'server_version'), 0, 1) >= 8) {
        if (!empty($employee_data['emp_name_deadlink_flg']) || !empty($employee_data['emp_class_deadlink_flg']) || !empty($employee_data['emp_job_deadlink_flg']) || !empty($employee_data['emp_st_deadlink_flg'])) {
            $emp_name_deadlink_flg = $employee_data['emp_name_deadlink_flg'] === 't' ? 't' : 'f';
            $emp_class_deadlink_flg = $employee_data['emp_class_deadlink_flg'] === 't' ? 't' : 'f';
            $emp_job_deadlink_flg = $employee_data['emp_job_deadlink_flg'] === 't' ? 't' : 'f';
            $emp_st_deadlink_flg = $employee_data['emp_st_deadlink_flg'] === 't' ? 't' : 'f';
            $sql_rel = "SELECT update_emp_relation(";
            $content_rel = array($emp_id, $emp_name_deadlink_flg, $emp_class_deadlink_flg, $emp_job_deadlink_flg, $emp_st_deadlink_flg);
            $in_rel = insert_into_table($con, $sql_rel, $content_rel, $fname);
            if ($in_rel == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }

    // 権限グループの更新
//XX	$con_mysql = mysql_connect(XOOPS_DB_HOST, XOOPS_DB_USER, XOOPS_DB_PASS);
//XX	auth_change_group_of_employee($con, $con_mysql, $emp_id, $auth_group_id, $fname);
    auth_change_group_of_employee($con, $emp_id, $auth_group_id, $fname);

    //20130607 表示グループとメニュグループを登録する処理を追加 中嶌
    dispgroup_change($con, $emp_id, $disp_group_id, $fname);
    menugroup_change($con, $emp_id, $menu_group_id, fname);

    // 利用停止フラグの更新
    $sql = "update authmst set";
    $set = array("emp_del_flg");
    $setvalue = array($del_flg);
    $cond = "where emp_id = '$emp_id'";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // 給与支給区分
    if ($wage == '月給制') $wage_cd = 1;
    else if ($wage == '日給制') $wage_cd = 3;
    else if ($wage == '時間給制') $wage_cd = 4;
    else if ($wage == '年俸制') $wage_cd = 5;

    // 雇用・勤務形態
    if ($duty_form == '常勤') {
        $duty_form = 1;
    }
    else if ($duty_form == '非常勤') {
        $duty_form = 2;
    }
    else if ($duty_form == '短時間正職員') {
        $duty_form = 3;
    }

    // 勤務条件のデータを取得
    $sql = "select * from empcond";
    $cond = "where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);

    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // 勤務条件のデータがない場合は、新規で登録
    if (pg_fetch_result($sel, 0, "wage") == "") {

        // 勤務条件コメント
        $comment = '給与支給区分、出勤パターン、雇用・勤務形態を登録する場合は給与支給区分、出勤パターン、雇用・勤務形態のすべてにデータを入力してください。';

        if ($wage != "" && $duty_form != "" && $group_id != "") {
            // 勤務条件のデータを登録
            $sql = "insert into empcond (emp_id, wage, duty_form, tmcd_group_id) values(";
            $content = array($emp_id, $wage_cd, $duty_form, $group_id);
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
        else if ($wage == "" && $duty_form == "" && $group_id == "") {

        }
        else {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('{$comment}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
    }
    else {

        if ($wage_cd == "") $wage_cd = pg_fetch_result($sel, 0, "wage");
        if ($duty_form == "") $duty_form = pg_fetch_result($sel, 0, "duty_form");
        if ($group_id == "") $group_id = pg_fetch_result($sel, 0, "tmcd_group_id");

        // 勤務条件のデータを更新
        $sql = "update empcond set";
        $set = array("wage", "duty_form", "tmcd_group_id");
        $setvalue = array($wage_cd, $duty_form, $group_id);
        $cond = "where emp_id = '$emp_id'";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

// 職員の新規登録
function insert_employee_data($con, $employee_no, $employee_data, $class_cnt, $class_tree, $arr_class_name, $statuses, $jobs, $session, $fname, $site_id, $use_cyrus)
{

    global $pass_length;
    global $_label_by_profile;
    global $profile_type;

    $i = 0;
    $auth_group = $employee_data["auth_group"];    // 権限グループ
    //20130607 表示グループとメニューグループを追加 中嶌
    $disp_group = $employee_data["disp_group"];    // 表示グループ
    $menu_group = $employee_data["menu_group"];    // メニューグループ
    $personal_id = $employee_data["personal_id"];  // 職員ID
    $id = $employee_data["id"];               // ログインID
    $pass = $employee_data["pass"];               // パスワード
    $lt_nm = $employee_data["lt_nm"];              // 苗字
    $ft_nm = $employee_data["ft_nm"];              // 氏名
    $lt_kana_nm = $employee_data["lt_kana_nm"];    // かな苗字
    $ft_kana_nm = $employee_data["ft_kana_nm"];    // かな氏名
    $mail_id = $employee_data["mail_id"];          // メールID
    $sex = $employee_data["sex"];                 // 性別
    $birth = $employee_data["birth"];             // 生年月日
    $entry = $employee_data["entry"];             // 入職日
    //中嶌 20130514 退職日を変数に格納
    $retire = $employee_data["retire"];
    $class = $employee_data["cls_nm"];             // 第１階層
    $atrb = $employee_data["atrb_nm"];             // 第２階層
    $dept = $employee_data["dept_nm"];             // 第３階層
    if ($arr_class_name[4] == "4") {
        $room = $employee_data["room_nm"];         // 第４階層
    }
    else {
        $room = "";
    }
    $status = $employee_data["status_nm"];          // 役職
    $job = $employee_data["job_nm"];             // 職種
    $del_flg = $employee_data["del_flg"];     // 利用停止フラグ
    $zip1 = trim($employee_data["zip1"]);         // 郵便番号１
    $zip2 = trim($employee_data["zip2"]);         // 郵便番号2
    $prv = $employee_data["prv"];         // 都道府県
    $addr1 = $employee_data["addr1"];       // 住所１
    $addr2 = $employee_data["addr2"];       // 住所２
    $tel1 = $employee_data["tel1"];         // 電話番号1
    $tel2 = $employee_data["tel2"];         // 電話番号2
    $tel3 = $employee_data["tel3"];         // 電話番号3
    $email = $employee_data["email"];       // E-Mail
    $mobile1 = $employee_data["mobile1"];      // 携帯電話番号1
    $mobile2 = $employee_data["mobile2"];      // 携帯電話番号2
    $mobile3 = $employee_data["mobile3"];      // 携帯電話番号3
    $m_email = $employee_data["m_email"];     // 携帯Mail
    $ext = $employee_data["ext"];         // 内線番号
    $phs = $employee_data["phs"];         // 院内PHS
    $wage = $employee_data["wage"];            // 給与支給区分(1:月給制 3:日給制 4:時間給制 5:年俸制)
    $group_name = $employee_data["group_name"];     // 出勤パターンのグループ
    $duty_form = $employee_data["duty_form"];    // 雇用・勤務形態(1:常勤 2:非常勤)


    $sql = "select count(*) from login";
    $cond = "where emp_login_id = '$id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('指定されたログインIDは既に使用されています。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }

    // 権限グループのIDを取得
    $sql = "select * from authgroup";
    $cond = "where group_nm = '$auth_group'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('該当する権限グループが登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $row2 = pg_fetch_array($sel);
    $auth_group_id = $row2["group_id"];

    // 表示グループのIDを取得 中嶌
    $sql = "select * from dispgroup";
    $cond = "where group_nm = '$disp_group'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        //削除予定
        //echo("<script type=\"text/javascript\">alert('該当する表示グループが登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $row2 = pg_fetch_array($sel);
    $disp_group_id = $row2["group_id"];

    // メニューグループのIDを取得 中嶌
    $sql = "select * from menugroup";
    $cond = "where group_nm = '$menu_group'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        //削除予定
        //echo("<script type=\"text/javascript\">alert('該当するメニューグループが登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $row2 = pg_fetch_array($sel);
    $menu_group_id = $row2["group_id"];

    if ($group_name != "") {
        // 出勤パターンのグループ
        $sql = "select * from wktmgrp";
        $cond = "where group_name = '$group_name'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('該当する出勤パターン名が登録されていません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
        $row2 = pg_fetch_array($sel);
        $group_id = $row2["group_id"];
    }
    // 部門IDの取得
    $sql = "select class_id from classmst";
    $cond = "where class_nm = '$class' and class_del_flg = 'f'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[0]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $class = pg_fetch_result($sel, 0, "class_id");

    // 課IDの取得
    $sql = "select atrb_id from atrbmst";
    $cond = "where class_id = $class and atrb_nm = '$atrb' and atrb_del_flg = 'f'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[1]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $atrb = pg_fetch_result($sel, 0, "atrb_id");

    // 科IDの取得
    $sql = "select dept_id from deptmst";
    $cond = "where atrb_id = $atrb and dept_nm = '$dept' and dept_del_flg = 'f'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[2]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
        echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
        exit;
    }
    $dept = pg_fetch_result($sel, 0, "dept_id");

    // 室IDの取得
    if ($arr_class_name[4] == "4" && $room != "") {
        $sql = "select room_id from classroom";
        $cond = "where dept_id = $dept and room_nm = '$room' and room_del_flg = 'f'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        if (pg_num_rows($sel) == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[3]}は登録されていません。レコード番号：{$employee_no}');</script>\n");
            echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register4.php?session=$session&result=f&encoding=$encoding';</script>");
            exit;
        }
        $room = pg_fetch_result($sel, 0, "room_id");
    }
    else {
        $room = null;
    }

    // 役職IDの取得
    $sql = "select st_id from stmst";
    $cond = "where st_nm = '$status' and st_del_flg = 'f'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $status = pg_fetch_result($sel, 0, "st_id");

    // 職種IDの取得
    $sql = "select job_id from jobmst";
    $cond = "where job_nm = '$job' and job_del_flg = 'f'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $job = pg_fetch_result($sel, 0, "job_id");

    // 登録値の編集
    $new_emp_id = get_new_emp_id($con, $fname);
    $keywd = get_keyword($lt_kana_nm);

    $sql = "select count(*) from empmst";
    $cond = "where emp_id = '$new_emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
        $new_emp_id = get_new_emp_id($con, $fname);
    }

    // empmstデータを追加
    //中嶌 20130514 登録するデータに退職日を追加
    $sql = "insert into empmst (emp_id, emp_personal_id, emp_class, emp_attribute, emp_dept, emp_room, emp_job, emp_st, emp_lt_nm, emp_ft_nm, emp_kn_lt_nm, emp_kn_ft_nm, emp_sex, emp_birth, emp_join, emp_retire, emp_keywd, emp_zip1, emp_zip2, emp_prv, emp_addr1, emp_addr2, emp_tel1, emp_tel2, emp_tel3, emp_email2, emp_mobile1, emp_mobile2, emp_mobile3, emp_m_email, emp_ext, emp_phs) values (";
    $content = array($new_emp_id, $personal_id, $class, $atrb, $dept, $room, $job, $status, $lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $sex, $birth, $entry, $retire, $keywd, $zip1, $zip2, $prv, $addr1, $addr2, $tel1, $tel2, $tel3, $email, $mobile1, $mobile2, $mobile3, $m_email, $ext, $phs);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // loginデータを追加
    if ($site_id == "") {
        $sql = "insert into login (emp_id, emp_login_id, emp_login_pass, emp_login_flg) values (";
        $content = array($new_emp_id, $id, $pass, "t");
    }
    else {
        $sql = "insert into login (emp_id, emp_login_id, emp_login_pass, emp_login_flg, emp_login_mail) values (";
        $content = array($new_emp_id, $id, $pass, "t", $mail_id);
    }
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // authmstデータを追加
    $pass_flg = $auth_group_id == "" ? "t" : "f";
    $sql = "insert into authmst (emp_id, emp_pass_flg, emp_del_flg) values(";
    $content = array($new_emp_id, $pass_flg, $del_flg);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // 権限グループの追加
//XX	$con_mysql = mysql_connect(XOOPS_DB_HOST, XOOPS_DB_USER, XOOPS_DB_PASS);
//XX	auth_change_group_of_employee($con, $con_mysql, $new_emp_id, $auth_group_id, $fname);
    auth_change_group_of_employee($con, $new_emp_id, $auth_group_id, $fname);


    // optionデータを追加
    $sql = "insert into option (emp_id, schedule1_default, schedule2_default) values (";
    $content = array($new_emp_id, "2", "2");
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    //---------- emp_relation ----------
    if (version_compare(PHP_VERSION, '5.0.0') >= 0 && substr(pg_parameter_status($con, 'server_version'), 0, 1) >= 8) {
        $emp_name_deadlink_flg = $employee_data['emp_name_deadlink_flg'] === 't' ? 't' : 'f';
        $emp_class_deadlink_flg = $employee_data['emp_class_deadlink_flg'] === 't' ? 't' : 'f';
        $emp_job_deadlink_flg = $employee_data['emp_job_deadlink_flg'] === 't' ? 't' : 'f';
        $emp_st_deadlink_flg = $employee_data['emp_st_deadlink_flg'] === 't' ? 't' : 'f';
        $sql_rel = "SELECT update_emp_relation(";
        $content_rel = array($new_emp_id, $emp_name_deadlink_flg, $emp_class_deadlink_flg, $emp_job_deadlink_flg, $emp_st_deadlink_flg);
        $in_rel = insert_into_table($con, $sql_rel, $content_rel, $fname);
        if ($in_rel == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }

    //20130607 表示グループとメニュグループを登録する処理を追加 中嶌
    dispgroup_change($con, $new_emp_id, $disp_group_id, $fname);
    menugroup_change($con, $new_emp_id, $menu_group_id, fname);

    // mygroupmstデータを追加
    $sql = "insert into mygroupmst (owner_id, mygroup_id, mygroup_nm) values(";
    $content = array($new_emp_id, 1, "マイグループ");
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // 給与支給区分
    if ($wage == '月給制') $wage_cd = 1;
    else if ($wage == '日給制') $wage_cd = 3;
    else if ($wage == '時間給制') $wage_cd = 4;
    else if ($wage == '年俸制') $wage_cd = 5;

    // 雇用・勤務形態
    if ($duty_form == '常勤') {
        $duty_form = 1;
    }
    else if ($duty_form == '非常勤') {
        $duty_form = 2;
    }
    else if ($duty_form == '短時間正職員') {
        $duty_form = 3;
    }

    // 勤務条件コメント
    $comment = '給与支給区分、出勤パターン、雇用・勤務形態を登録する場合は給与支給区分、出勤パターン、雇用・勤務形態のすべてにデータを入力してください。';

    // 勤務条件のデータを登録
    if ($wage != "" && $duty_form != "" && $group_id != "") {
        $sql = "insert into empcond (emp_id, wage, duty_form, tmcd_group_id) values(";
        $content = array($new_emp_id, $wage_cd, $duty_form, $group_id);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

//XX // コミュニティサイトデータを登録
//XX function insert_community_data($con_mysql, $con, $employee_data, $domain) {
//XX 	$lt_nm = $employee_data["lt_nm"];
//XX 	$ft_nm = $employee_data["ft_nm"];
//XX 	$id = $employee_data["id"];
//XX 	$pass = $employee_data["pass"];
//XX
//XX 	$sql = "insert into " . XOOPS_DB_PREFIX . "_users (name, uname, email, user_regdate, user_viewemail, pass, rank, level, timezone_offset, notify_method, user_mailok) values ('$lt_nm $ft_nm', '$id', '$id@$domain', " . time() . ", 1, '" . md5($pass) . "', 0, 1, '9.0', 2, 0);";
//XX 	if (!mysql_query($sql, $con_mysql)) {
//XX 		pg_query($con, "rollback");
//XX 		pg_close($con);
//XX 		mysql_close($con_mysql);
//XX 		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX 		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX 		exit;
//XX 	}
//XX
//XX 	$uid = mysql_insert_id($con_mysql);
//XX 	$sql = "insert into " . XOOPS_DB_PREFIX . "_groups_users_link (groupid, uid) values (2, $uid)";
//XX 	if (!mysql_query($sql, $con_mysql)) {
//XX 		pg_query($con, "rollback");
//XX 		pg_close($con);
//XX 		mysql_close($con_mysql);
//XX 		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX 		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX 		exit;
//XX 	}
//XX }
// メールアカウントを作成
function make_mail_account($employee_data, $site_id, $dir, $imapServerAddress)
{
    $id = $employee_data["id"];
    $pass = $employee_data["pass"];
    $mail_id = ($site_id == "") ? "" : $employee_data["mail_id"];

    $account = ($site_id == "") ? $id : "{$mail_id}_{$site_id}";
    if (preg_match("/[a-z]/", $account) > 0) {
        if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
            exec("/usr/bin/perl $dir/mboxadm/addmailbox $account $pass 2>&1 1> /dev/null", $output, $exit_status);
            if ($exit_status !== 0) {
                exec("/usr/bin/expect -f $dir/expect/addmailbox.exp $account $pass");
            }
        }
        else {
            exec("/usr/bin/expect -f $dir/expect/rsyncaddmailbox.exp $account $pass $imapServerAddress $dir");
        }
    }

    webmail_quota_change_to_default($account);
}

// メールアカウントを更新
function update_mail_account($employee_data, $site_id, $dir, $imapServerAddress)
{
    $cur_account = ($site_id == "") ? $employee_data["cur_id"] : "{$employee_data["cur_mail_id"]}_{$site_id}";
    $cur_pass = $employee_data["cur_pass"];
    $new_account = ($site_id == "") ? $employee_data["id"] : "{$employee_data["mail_id"]}_{$site_id}";
    $new_pass = $employee_data["pass"];

    if ($new_account == $cur_account && $new_pass == $cur_pass) {
        return;
    }

    $cur_account_has_alphabet = (preg_match("/[a-z]/", $cur_account) > 0);
    $new_account_has_alphabet = (preg_match("/[a-z]/", $new_account) > 0);

    // アルファベットあり→アルファベットあり：更新
    if ($cur_account_has_alphabet && $new_account_has_alphabet) {
        if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
            exec("/usr/bin/perl $dir/mboxadm/updmailbox $cur_account $new_account $new_pass 2>&1 1> /dev/null", $output, $exit_status);
            if ($exit_status !== 0) {
                exec("/usr/bin/expect -f $dir/expect/updmailbox.exp $cur_account $new_account $new_pass");
            }
        }
        else {
            exec("/usr/bin/expect -f $dir/expect/rsyncupdmailbox.exp $cur_account $new_account $new_pass $imapServerAddress $dir");
        }

        // 数字のみ→アルファベットあり：作成
    }
    else if (!$cur_account_has_alphabet && $new_account_has_alphabet) {
        make_mail_account($employee_data, $site_id, $dir, $imapServerAddress);

        // アルファベットあり→数字のみ：削除
    }
    else {
        if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
            exec("/usr/bin/perl $dir/mboxadm/delmailbox $cur_account 2>&1 1> /dev/null", $output, $exit_status);
            if ($exit_status !== 0) {
                exec("/usr/bin/expect -f $dir/expect/delmailbox.exp $cur_account");
            }
        }
        else {
            exec("/usr/bin/expect -f $dir/expect/rsyncdelmailbox.exp $cur_account $imapServerAddress $dir");
        }
    }
}

// 職員IDを採番
function get_new_emp_id($con, $fname)
{
    $sql = "select max(emp_id) from empmst";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $max = pg_fetch_result($sel, 0, "max");

    $val = substr($max, 4, 12) + 1;
    $yymm = date("y") . date("m");

    for ($i = 8; $i > strlen($val); $i--) {
        $zero .= "0";
    }

    return "$yymm$zero$val";
}

// 都道府県名を都道府県コードに変換
function prv_change($emp_prv)
{

    $prefecture = array(
        '1' => '北海道', '2' => '青森県', '3' => '岩手県', '4' => '宮城県', '5' => '秋田県',
        '6' => '山形県', '7' => '福島県', '8' => '茨城県', '9' => '栃木県', '10' => '群馬県',
        '11' => '埼玉県', '12' => '千葉県', '13' => '東京都', '14' => '神奈川県', '15' => '新潟県',
        '16' => '富山県', '17' => '石川県', '18' => '福井県', '19' => '山梨県', '20' => '長野県',
        '21' => '岐阜県', '22' => '静岡県', '23' => '愛知県', '24' => '三重県', '25' => '滋賀県',
        '26' => '京都府', '27' => '大阪府', '28' => '兵庫県', '29' => '奈良県', '30' => '和歌山県',
        '31' => '鳥取県', '32' => '島根県', '33' => '岡山県', '34' => '広島県', '35' => '山口県',
        '36' => '徳島県', '37' => '香川県', '38' => '愛媛県', '39' => '高知県', '40' => '福岡県',
        '41' => '佐賀県', '42' => '長崎県', '43' => '熊本県', '44' => '大分県', '45' => '宮崎県',
        '46' => '鹿児島県', '47' => '沖縄県'
    );

    foreach ($prefecture as $prv_cd => $prv_nm) {
        if ($prv_nm === $emp_prv) {
            return $prv_cd;
        }
    }
    return 0;
}

// 都道府県コードを都道府県名に変換
function prv2_change($prefecture_cd)
{

    $prefecture = array(
        '1' => '北海道', '2' => '青森県', '3' => '岩手県', '4' => '宮城県', '5' => '秋田県',
        '6' => '山形県', '7' => '福島県', '8' => '茨城県', '9' => '栃木県', '10' => '群馬県',
        '11' => '埼玉県', '12' => '千葉県', '13' => '東京都', '14' => '神奈川県', '15' => '新潟県',
        '16' => '富山県', '17' => '石川県', '18' => '福井県', '19' => '山梨県', '20' => '長野県',
        '21' => '岐阜県', '22' => '静岡県', '23' => '愛知県', '24' => '三重県', '25' => '滋賀県',
        '26' => '京都府', '27' => '大阪府', '28' => '兵庫県', '29' => '奈良県', '30' => '和歌山県',
        '31' => '鳥取県', '32' => '島根県', '33' => '岡山県', '34' => '広島県', '35' => '山口県',
        '36' => '徳島県', '37' => '香川県', '38' => '愛媛県', '39' => '高知県', '40' => '福岡県',
        '41' => '佐賀県', '42' => '長崎県', '43' => '熊本県', '44' => '大分県', '45' => '宮崎県',
        '46' => '鹿児島県', '47' => '沖縄県'
    );

    return $prefecture[$prefecture_cd];
}

function menugroup_change($con, $emp_id, $menu_group_id, $fname)
{

    // メニューグループを設定している場合、メニューを設定
    if ($menu_group_id != "") {
        // メニューグループの設定情報を取得
        $sql = "select * from menugroup";
        $cond = "where group_id = $menu_group_id";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
            exit;
        }

        $menu_groups = pg_fetch_assoc($sel);

        // サイドメニュー更新
        $sql = "update option set";
        $set = array("sidemenu_show_flg", "sidemenu_position");
        $setvalue = array($menu_groups["sidemenu_show_flg"], $menu_groups["sidemenu_position"]);
        $cond = "where emp_id = '$emp_id'";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        //メニュー設定の初期化
        $sql = " delete  from headermenu where emp_id = '$emp_id'";
        $cond = "";
        $sel = select_from_table($con, $sql, $cond, $fname);

        // メニュー設定
        for ($i = 1; $i <= 12; $i++) {
            $menu_num = 'menu_id_' . $i;
            if ($menu_groups[$menu_num] == "") break;
            $sql = "insert into headermenu(emp_id, menu_id, menu_order) values(";
            $content = array($emp_id, $menu_groups[$menu_num], $i);
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }

        // menu_group_idの登録
        $sql = "update option set";
        $set = array("menu_group_id");
        $setvalue = array($menu_group_id);
        $cond = "where emp_id = '$emp_id'";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

function dispgroup_change($con, $emp_id, $disp_group_id, $fname)
{
    if ($disp_group_id != "") {
        // 表示グループの設定情報を取得
        $sql = "select * from dispgroup";
        $cond = "where group_id = $disp_group_id";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
            exit;
        }

        $disp_groups = pg_fetch_assoc($sel);


        // 表示設定更新
        $sql = "update option set";
        $set = array("default_page", "font_size", "top_mail_flg", "top_ext_flg", "top_event_flg", "top_schd_flg", "top_info_flg", "top_task_flg", "top_msg_flg", "top_aprv_flg", "top_schdsrch_flg", "top_lib_flg", "top_bbs_flg", "top_memo_flg", "top_link_flg", "top_intra_flg", "bed_info", "schd_type", "top_wic_flg", "top_fcl_flg", "top_inci_flg", "top_cas_flg", "top_fplus_flg", "top_jnl_flg", "top_free_text_flg", "top_manabu_flg", "top_workflow_flg", "disp_group_id");
        $setvalue = array($disp_groups["default_page"], $disp_groups["font_size"], $disp_groups["top_mail_flg"], $disp_groups["top_ext_flg"], $disp_groups["top_event_flg"], $disp_groups["top_schd_flg"], $disp_groups["top_info_flg"], $disp_groups["top_task_flg"], $disp_groups["top_msg_flg"], $disp_groups["top_aprv_flg"], $disp_groups["top_schdsrch_flg"], $disp_groups["top_lib_flg"], $disp_groups["top_bbs_flg"], $disp_groups["top_memo_flg"], $disp_groups["top_link_flg"], $disp_groups["top_intra_flg"], $disp_groups["bed_info"], $disp_groups["schd_type"], $disp_groups["top_wic_flg"], $disp_groups["top_fcl_flg"], $disp_groups["top_inci_flg"], $disp_groups["top_cas_flg"], $disp_groups["top_fplus_flg"], $disp_groups["top_jnl_flg"], $disp_groups["top_free_text_flg"], $disp_groups["top_manabu_flg"], $disp_groups["top_workflow_flg"], $disp_group_id);
        $cond = "where emp_id = '$emp_id'";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}
