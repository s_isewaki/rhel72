<?
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 【管理側】講座管理 関連を扱うURL
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once 'StudyInitController.php';
require_once 'model/StudyFolderModel.php';
require_once 'model/StudyNotificationModel.php';



display_menu(); // メニューを表示
die;



//**************************************************************************************************
// 管理メニュー画面処理（ツリー＆一覧画面）
//**************************************************************************************************
function display_menu() {
    require_once 'conf/smarty_start.conf';

    $btn_event = $_REQUEST["btn_event"];
    $folder_list = StudyFolderModel::get_folder_list("","",0);

    //==========================================================================
    // 教育担当者に講座を追加
    //==========================================================================
    if ($btn_event=="be_add_employee_folder") {
        $folder_id = $_REQUEST["selected_folder_id"];
        $emp_ids = explode(",", $_REQUEST["emp_id_comma_list"]);
        foreach ($emp_ids as $emp_id) {
            if (!trim($emp_id)) continue;
            StudyCommonModel::add_edu_tantou_folder(trim($emp_id), $folder_id);
        }
        header("Location:study_folder_admin.php?session=".GG_SESSION_ID."&selected_folder_id=".$folder_id);
        die;
    }
    //==========================================================================
    // 教育担当者の講座を削除
    //==========================================================================
    if ($btn_event=="be_del_employee_folder") {
        $selected_folder_id = $_REQUEST["selected_folder_id"];
        $nodes = StudyCommonModel::get_folder_parents("id", $selected_folder_id, $folder_list); // とある講座の階層
        $emp_id = $_REQUEST["target_emp_id"];
        foreach ($nodes as $folder_id) {
            StudyCommonModel::del_edu_tantou_folder($emp_id, $folder_id);
        }
        header("Location:study_folder_admin.php?session=".GG_SESSION_ID."&selected_folder_id=".$selected_folder_id);
        die;
    }

    $smarty->assign("folder_list", $folder_list); // 講座ツリーリスト。管理者用画面につき権限制限なし
    $smarty->assign("selected_folder_id", $_REQUEST["selected_folder_id"]); // 再表示の場合はsubmit前の講座を選択
    $smarty->assign("notif_list_count",  count(StudyNotificationModel::get_user_notif_list("")));// 未読の、お知らせの数
    $smarty->display("study_folder_admin.tpl");
}
?>
