<?
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 【管理側】従業員管理 関連を扱うURL
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once 'StudyInitController.php';
require_once 'model/StudyTestModel.php';
require_once 'model/StudyCommonModel.php';
require_once 'model/StudyContentsModel.php';
require_once 'model/StudyAssemblyModel.php';
require_once 'model/StudyQuestionnaireModel.php';


$btn_event = @$_REQUEST["btn_event"];
if ($btn_event=="be_show_student_import_csv")   show_student_import_csv_and_die();
if ($btn_event=="be_exec_student_import_csv")   exec_student_import_csv_and_die();
if ($btn_event=="be_show_student_export_csv") show_student_export_csv_and_die();
if ($btn_event=="be_show_student_selector")  show_student_selector_and_die();
if ($btn_event=="be_get_application_apply_count") output_application_apply_count_and_die();


default_view($btn_event); // その他のイベント処理
die;

//**************************************************************************************************
// 受講者管理画面
//**************************************************************************************************
function default_view($btn_event) {
    $result = $_REQUEST["result"];
    $contents_id = $_REQUEST["contents_id"];
    $emp_ids = explode(",", $_REQUEST["emp_id_comma_list"]);

    //==========================================================================
    // 受講者を追加
    //==========================================================================
    if ($btn_event=="be_add_employee" && count($emp_ids) > 0){
        dbBeginTransaction();
        StudyCommonModel::add_student($contents_id, $emp_ids);
        dbEndTransaction();
        ob_clean();
        echo "ok";
        die;
    }

    //==========================================================================
    // 受講者を削除
    //==========================================================================
    if ($btn_event=="be_del_employee" && count($emp_ids) > 0){
        dbBeginTransaction();
        StudyCommonModel::del_student($contents_id, $emp_ids);
        dbEndTransaction();
        ob_clean();
        echo "ok";
        die;
    }

    //==========================================================================
    // 集合研修実績日の登録、クリア
    //==========================================================================
    if ($btn_event == "be_change_assembly_actual_status" && count($emp_ids) > 0){
        $st = $_REQUEST["assembly_actual_status"];
        dbBeginTransaction();
        StudyAssemblyModel::change_assembly_actual_status($contents_id, $emp_ids, $st, "");
        dbEndTransaction();
        ob_clean();
        echo "ok";
        die;
    }
    
        //==========================================================================
    // 集合研修予定の登録、クリア
    //==========================================================================
    if ($btn_event == "be_change_assembly_status" && count($emp_ids) > 0){
        $st = $_REQUEST["assembly_status"];
        dbBeginTransaction();
        StudyAssemblyModel::set_manager_assembly_application($contents_id, $st, $emp_ids);
        dbEndTransaction();
        ob_clean();
        echo "ok";
        die;
    }
    
    
    
    //==========================================================================
    // アンケート抹消
    //==========================================================================
    if ($btn_event == "be_change_questionnaire_status" && count($emp_ids) > 0){
        dbBeginTransaction();
        foreach ($emp_ids as $emp_id) {
            if ($emp_id) StudyQuestionnaireModel::update_questionnaire_answer($contents_id, $emp_id, array(), 1);
        }
        dbEndTransaction();
        ob_clean();
        echo "ok";
        die;
    }
    //==========================================================================
    // アンケート抹消
    //==========================================================================
    if ($btn_event == "be_clear_test_result" && count($emp_ids) > 0){
        dbBeginTransaction();
        foreach ($emp_ids as $emp_id) {
            if ($emp_id) StudyTestModel::clear_test_result($contents_id, $emp_id);
        }
        dbEndTransaction();
        ob_clean();
        echo "ok";
        die;
    }


    // ★ここより画面表示関連


    //------------------------------------------------------
    // ページング
    //------------------------------------------------------
    $per_page_size = (int)@$_REQUEST["per_page_size"];
    if (($per_page_size >=0 && $per_page_size<5) || $per_page_size>=10000) $per_page_size = 10;
    $page_num = (int)@$_REQUEST["page_num"];
    if ($page_num<1) $page_num = 1;


    //------------------------------------------------------
    // 研修・講義情報
    //------------------------------------------------------
    $contents = StudyContentsModel::get_contents_row($contents_id);
    $policy_info = get_policy_info($contents["policy_code"]);

    //------------------------------------------------------
    // 受講者一覧取得条件
    // エクセル出力時は条件無視で、全行取得
    //------------------------------------------------------
    $joken = array(
        "emp_class"=>@$_REQUEST["emp_class"],
        "emp_attr"=>@$_REQUEST["emp_attr"],
        "emp_dept"=>@$_REQUEST["emp_dept"],
        "emp_room"=>@$_REQUEST["emp_room"],
        "emp_personal_id"=>@$_REQUEST["emp_personal_id"],
        "emp_name"=>@$_REQUEST["emp_name"],
        "emp_job"=>@$_REQUEST["emp_job"],
        "emp_status"=>@$_REQUEST["emp_status"],
        "emp_expire_type"=>@$_REQUEST["emp_expire_type"],
        "no"=>@$_REQUEST["no"],
        "total_status"=>@$_REQUEST["total_status"],
        "assembly_actual_status"=>@$_REQUEST["assembly_actual_status"],
        "test_status"=>@$_REQUEST["test_status"],
        "questionnaire_status"=>@$_REQUEST["questionnaire_status"]
    );

    if ($btn_event == "be_export_excel") {
        $joken = array("no"=>"-3");
        $page_num = 1;
        $per_page_size = 0;
    }
    if ($btn_event == "be_exec_student_export_csv") {
        $page_num = 1;
        $per_page_size = 0;
    }
    $total_count = StudyCommonModel::get_student_list($contents_id, $joken, $page_num, $per_page_size, 1); // 検索！
    $student_list = StudyCommonModel::get_student_list($contents_id, $joken, $page_num, $per_page_size, 0); // 検索！

    //------------------------------------------------------
    // 取得した受講者一覧データを精査
    //------------------------------------------------------
    $last_page = 1;
    if ($per_page_size > 0) $last_page = ceil($total_count / $per_page_size);
    if ($last_page < $page_num) $page_num = $last_page;

    $arr_week = array("日","月","火","水","木","金","土");
    foreach ($student_list as $key=>$item){
        $assembly_date = "";
        //集合研修予約日
        if (strlen($item["assembly_mosikomi_date"])==8) {
            $tmp_date = date("Y年m月d日",strtotime($item["assembly_mosikomi_date"]));
            $w = date("w", strtotime($item["assembly_mosikomi_date"]));
            $assembly_date = $tmp_date . "(" . $arr_week[$w] . ")";
            $start_time = substr($item["assembly_mosikomi_start_time"], 0 ,2) . ":" . substr($item["assembly_mosikomi_start_time"], 2 ,2);
            $end_time = substr($item["assembly_mosikomi_end_time"], 0 ,2) . ":" . substr($item["assembly_mosikomi_end_time"], 2 ,2);
        }
        $student_list[$key]["assembly_mosikomi_date_jp"] = $assembly_date . ($assembly_date ? " ".$start_time."〜".$end_time : "");
        //集合研修実績日
        if (strlen($item["assembly_actual_date"])==8) {
            $tmp_date = date("Y年m月d日",strtotime($item["assembly_actual_date"]));
            $w = date("w", strtotime($item["assembly_actual_date"]));
            $assembly_date = $tmp_date . "(" . $arr_week[$w] . ")";
            $student_list[$key]["assembly_actual_date_jp"] = $assembly_date;
        }
    }



    //==========================================================================
    // 受講者CSVエクスポートの場合
    //==========================================================================
    if ($btn_event=="be_exec_student_export_csv") {

        $login_id_list = StudyCommonModel::get_login_id_list();

        $id_type = $_REQUEST["id_type"];
        $title = ($id_type == "login") ? "ログインID,氏名,所属" : "職員ID,氏名,所属";

        $output_data = array();
        $output_data[]= mb_convert_encoding($title,"sjis-win","eucJP-win");
        foreach($student_list as $num=>$row){
            $emp_id = $row["emp_id"];
            $emp_personal_id = $row["emp_personal_id"];
            $emp_login = @$login_id_list[$emp_id];    //ログインID追加     20130701
            $emp_name = $row["last_name"]." ".$row["first_name"];
            $emp_class= $row["class_nm"];
            // 組織レベル存在チェック
            if( !empty($row["atrb_nm"])) $emp_class .= "＞".$row["atrb_nm"];
            if( !empty($row["dept_nm"])) $emp_class .= "＞".$row["dept_nm"];
            if( !empty($row["room_nm"])) $emp_class .= "＞".$row["room_nm"];

            $export_id = ($id_type == "login") ? $emp_login : $emp_personal_id;
            $tmp_output_data = "$export_id,$emp_name,$emp_class";
            $output_data[]= mb_convert_encoding($tmp_output_data,"sjis-win","eucJP-win");
        }

        ob_clean();
        $file_name = "list_" . date("Ymd") . ".csv";
        header("Content-Disposition: attachment; filename=$file_name");
        header("Content-Type: application/octet-stream; name=$file_name");
        header("Content-Length: " . strlen(implode("\n",$output_data)));
        echo(implode("\n",$output_data));
        die;
    }




    //==========================================================================
    // EXCEL申込者一覧出力の場合
    //==========================================================================
    if ($btn_event=="be_export_excel") {
        require_once 'conf/smarty_start.conf';
        $tmp_date = date("YmdHis");
        $smarty->assign("list", $student_list);
        ob_start();
        $smarty->display("study_student_excel.tpl");
        $excel_data = mb_convert_encoding(ob_get_contents(), "SJIS-win", 'eucJP-win');
        ob_end_clean();
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=students_{$tmp_date}.xls");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");
        echo($excel_data);
        die;
    }

    //------------------------------------------------------
    // 権限
    //------------------------------------------------------
    $auth_matrix = StudyCommonModel::get_auth_matrix("study_student_list.tpl@all", $contents["folder_id"], "");
    $disp_auth = $auth_matrix["disp_auth"];
    $edit_auth = $auth_matrix["edit_auth"];

    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    $template_vars = array(
        // ページング
        "page_num" => $page_num,
        "per_page_size" => $per_page_size,
        "total_count" => $total_count,
        "last_page" => $last_page,
        "disp_auth" => $disp_auth,
        "edit_auth" => $edit_auth,
        // 検索条件
        "emp_class"              => @$_REQUEST["emp_class"], // 部
        "emp_attr"               => @$_REQUEST["emp_attr"], // 課
        "emp_dept"               => @$_REQUEST["emp_dept"], // 科
        "emp_room"               => @$_REQUEST["emp_room"], // 室
        "emp_personal_id"        => @$_REQUEST["emp_personal_id"], // 職員ID
        "emp_name"               => @$_REQUEST["emp_name"], // 職員名
        "emp_job"                => @$_REQUEST["emp_job"], // 職種
        "emp_status"             => @$_REQUEST["emp_status"], // 役職
        "emp_expire_type"        => @$_REQUEST["emp_expire_type"], // 退職/停止条件
        "assembly_actual_status" => @$_REQUEST["assembly_actual_status"], // 集合研修実績
        "test_status"            => @$_REQUEST["test_status"], // テストステータス
        "total_status"           => @$_REQUEST["total_status"], // 総合ステータス
        "questionnaire_status"   => @$_REQUEST["questionnaire_status"], // アンケートステータス
        "no"                     => @$_REQUEST["no"], // 集合研修申込番号
        // 検索結果関連
        "result" => $result,
        "contents_id" => $contents_id,
        "policy_info" => $policy_info,
        "contents" => $contents,
        "student_list" => $student_list,
        // プルダウン選択肢準備
        "class_list"  => StudyCommonModel::get_class_list(1),
        "attr_list"   => StudyCommonModel::get_attr_list(0, 1),
        "dept_list"   => StudyCommonModel::get_dept_list(0, 1),
        "room_list"   => StudyCommonModel::get_room_list(0, 1),
        "job_list"    => StudyCommonModel::get_job_list(),
        "status_list" => StudyCommonModel::get_status_list(),
        "asm_list"                  => StudyAssemblyModel::get_assembly_list($contents_id), // 開催日一覧
        "assembly_actual_date_list" => StudyAssemblyModel::get_assembly_actual_date_list($contents_id) // 受講済の日付一覧
    );
    display_template("study_student_list.tpl", $template_vars);

}





//**************************************************************************************************
//**************************************************************************************************
function output_application_apply_count_and_die() {
    $joken = array("no"=>"-3");
    $page_num = 1;
    $per_page_size = 0;
    $total_count = StudyCommonModel::get_student_list($_REQUEST["contents_id"], $joken, 1, 0, 1); // 検索！
    ob_clean();
    echo "ok\t".$total_count;
    die;
}


//**************************************************************************************************
// 受講者インポート画面を開く
//**************************************************************************************************
function show_student_import_csv_and_die() {
    require_once 'conf/smarty_start.conf';
    $smarty->assign("contents_id", $_REQUEST["contents_id"]);
    $smarty->assign("id_type", "");
    $smarty->assign("result", "");
    $smarty->assign("message", "");
    $smarty->display("study_student_import_csv.tpl");
    die;
}








//**************************************************************************************************
// 受講者追加のダイアログを表示
//**************************************************************************************************
function show_student_selector_and_die() {
    //require (dirname(__FILE__)."/../conf/conf.inf");
    //------------------------------------------------------
    // ページング
    //------------------------------------------------------
    $per_page_size = (int)@$_REQUEST["per_page_size"];
    if (($per_page_size >=0 && $per_page_size<5) || $per_page_size>=10000) $per_page_size = 10;
    $page_num = (int)@$_REQUEST["page_num"];
    if ($page_num<1) $page_num = 1;

    $total_count = 0;
    $emp_list = array();
    $joken = array(
        "emp_expire_type"  => $_REQUEST["emp_expire_type"],     // 退職を含めるならinclude_emp_retire
        "class"  => $_REQUEST["class"],     // 部
        "attr"   => $_REQUEST["attr"],      // 課
        "dept"   => $_REQUEST["dept"],      // 科
        "room"   => $_REQUEST["room"],      // 室
        "id"     => $_REQUEST["in_emp_id"], // 職員ID
        "name"   => $_REQUEST["emp_name"],  // 職員名
        "job"    => $_REQUEST["job"],       // 職種
        "status" => $_REQUEST["status"]    // 役職
    );

//    $arrow_admin = "";
//    if (GG_ADMIN_FLG && $_REQUEST["js_callback"] == "add_edu_admins") $arrow_admin = "1";
    $total_count = count(StudyCommonModel::get_user_list_auth($joken, 1));
    $joken["per_page_size"] = $per_page_size;
    $joken["page_num"] = $page_num;
    $emp_list = StudyCommonModel::get_user_list_auth($joken, 1);

    foreach ($emp_list as $key => $value){
        $key_class[$key] = $value['class_nm'];
        $key_atrb[$key] = $value['atrb_nm'];
        $key_dept[$key] = $value['dept_nm'];
        $key_room[$key] = $value['room_nm'];
        $key_last[$key] = $value['last_name'];
        $key_first[$key] = $value['first_name'];
    }
    //array_multisort ($key_class , SORT_ASC ,$key_atrb , SORT_ASC , $key_dept , SORT_ASC , $key_room , SORT_ASC , $key_last , SORT_ASC , $key_first , SORT_ASC,$emp_list );



    $today = date("Ymd");
    foreach ($emp_list as $key=>$item) {
        $career = StudyCommonModel::get_staff_career($item['emp_id']);

        $now = date("Y/m/j");

        $exp_start=$career[0]["exp_start"];
        $work_start = $career[0]["work_start"];
        if($exp_start){
            $exp_diff = day_diff($exp_start,$now,$career[0]["exp_minus"]);
            $exp_month = $exp_diff%12;
            $exp_year = floor($exp_diff/12);
            if($exp_year<0) $exp_year = 0;
            if($exp_month<0) $exp_month = 0;
            $emp_list[$key]["exp_diff"] = $exp_year."年".$exp_month."月";
        }

        if($work_start){
            $work_diff = day_diff($work_start,$now,$career[0]["work_minus"]);
            $work_month = $work_diff%12;
            $work_year = floor($work_diff/12);
            if($work_year<0) $work_year = 0;
            if($work_month<0) $work_month = 0;
            $emp_list[$key]["work_diff"] = $work_year."年".$work_month."月";
        }
    }


    //------------------------------------------------------
    // 取得した受講者一覧データを精査
    //------------------------------------------------------
    $last_page = 1;
    if ($per_page_size > 0) $last_page = ceil($total_count / $per_page_size);
    if ($last_page < $page_num) $page_num = $last_page;




    $template_vars = array(
        // ページング
        "page_num" => $page_num,
        "per_page_size" => $per_page_size,
        "total_count" => $total_count,
        "last_page" => $last_page,
        //"BARITESS_NENSUU_OPT_FLAG"=> $BARITESS_NENSUU_OPT_FLAG,

        "js_callback" => $_REQUEST["js_callback"],
        "emp_expire_type" => $_REQUEST["emp_expire_type"],
        "class" => $_REQUEST["class"],
        "attr" => $_REQUEST["attr"],
        "dept" => $_REQUEST["dept"],
        "room" => $_REQUEST["room"],
        "input_emp_id" => $_REQUEST["in_emp_id"],
        "input_emp_name" => $_REQUEST["emp_name"],
        "job" => $_REQUEST["job"],
        "status" => $_REQUEST["status"],
        "class_list" => StudyCommonModel::get_class_list(1),
        "attr_list" => StudyCommonModel::get_attr_list(0, 1),
        "dept_list" => StudyCommonModel::get_dept_list(0, 1),
        "room_list" => StudyCommonModel::get_room_list(0, 1),
        "job_list" => StudyCommonModel::get_job_list(),
        "status_list" => StudyCommonModel::get_status_list(),
        "emp_list" => $emp_list,
        "mode" => $_REQUEST["mode"]
    );
    display_template("study_student_selector.tpl", $template_vars);
    die;
}








//**************************************************************************************************
// 受講者インポート画面からのCSVファイル取り込み処理
//**************************************************************************************************
function exec_student_import_csv_and_die() {
    require_once 'conf/smarty_start.conf';

    $contents_id = $_REQUEST["contents_id"];

    $id_type = $_REQUEST["id_type"]; // login:ログインID、それ以外:職員ID

    $smarty->assign("contents_id", $contents_id);
    $smarty->assign("id_type", $id_type);
    $smarty->assign("message", "");
    $smarty->assign("result", "success");

    $joken = array(); // カラ条件
    //$db_user_list = StudyCommonModel::get_user_list_auth($joken, 1);
    //<!--20151118 順天堂医院 by Y.yamamoto--> 
    $db_user_list = StudyCommonModel::get_user_list_auth_csv(1);
    //ここまで
    
    foreach ($db_user_list as $key => $value){
        $key_class[$key] = $value['class_nm'];
        $key_atrb[$key] = $value['atrb_nm'];
        $key_dept[$key] = $value['dept_nm'];
        $key_room[$key] = $value['room_nm'];
        $key_last[$key] = $value['last_name'];
        $key_first[$key] = $value['first_name'];
    }
    //array_multisort ($key_class , SORT_ASC ,$key_atrb , SORT_ASC , $key_dept , SORT_ASC , $key_room , SORT_ASC , $key_last , SORT_ASC , $key_first , SORT_ASC,$db_user_list );


    $match_id_list = array();
    foreach($db_user_list as $db_item) {
        $key = ($id_type == "login") ? $db_item["login_id"] : $db_item["emp_personal_id"]; //ログインID追加 20130701
        $match_id_list[$key] = $db_item["emp_id"];
    }

    // CSVファイルからは職員IDの配列を取得
    $readLines = explode("\n",mb_convert_encoding(file_get_contents($_FILES['csv']['tmp_name']), "eucJP-win", "sjis-win"));

    $emp_ids = array();
    $reg_list = array();
    $error_line = array();
    $error_count = 0;
    foreach ($readLines as $line) {
        if ($line=="職員ID,氏名,所属") continue;
        if ($line=="ログインID,氏名,所属") continue;
        $line_array = explode(",", $line);
        $id = trim($line_array[0]);
        if (!strlen($id)) continue;
        if (@$match_id_list[$id]=="") {
            $error_count++;
            //if ($error_count <=5) {
                if (strlen($id)>30) $id = substr($id,0,30)." …";
                $error_line[]= $id;
            //}
        }
        else $emp_ids[] = $match_id_list[$id];
    }

    if (count($error_line) > 0) {
        $id_caption = ($id_type == "login") ? "ログインID" : "職員ID";

        $errors = array();
        $errors[]= "以下の" . $id_caption . "に該当する職員が存在しません。";
        foreach($error_line as $line) {
            $errors[] = preg_replace('/[\r\n\"]*/', "", $line);
        }
        //if ($error_count > 5)
        $errors[]= "（合計：".$error_count."件）";

        $smarty->assign("result", "error");
        $smarty->assign("message", implode("<br>",$errors));
        $smarty->display("study_student_import_csv.tpl");
        die;
    }

    dbBeginTransaction();
    StudyCommonModel::add_student($contents_id, $emp_ids); // 受講生追加登録
    dbEndTransaction();

    $smarty->display("study_student_import_csv.tpl");
    die;
}







//**************************************************************************************************
// 職員ＩＤ選択画面
//**************************************************************************************************
function show_student_export_csv_and_die() {
    require_once 'conf/smarty_start.conf';
    $smarty->assign("contents_id", $_REQUEST["contents_id"]);
    $smarty->display("study_student_export_csv.tpl");
    die;
}







?>
