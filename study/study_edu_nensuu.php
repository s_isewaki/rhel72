<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 教育担当者関連を扱うURL
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once 'StudyInitController.php';
require_once 'model/StudyCommonModel.php';
require_once 'model/StudyFolderModel.php';

$btn_event = $_REQUEST["btn_event"];
global $all_staff_career;

if ($btn_event=="export_csv") export_csv_and_die();
if ($btn_event=="import_csv") import_csv();

default_view();
die;

function export_csv_and_die(){
    ob_clean();

    $file_name = "list_" . date("Ymd") . ".csv";
    header("Content-Disposition: attachment; filename=$file_name");
    header("Content-Type: application/octet-stream; name=$file_name");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Pragma: public");

    $title = "職員ID,氏名,職種,開始年月日,離職月数,コメント,開始年月日,離職月数,コメント";
    $title .="\r\n";
    $csv_header = mb_convert_encoding($title, "SJIS-win", 'eucJP-win');
    echo($csv_header);
    //ここまでヘッダ

    $user_list = StudyCommonModel::get_all_staff_career(array());

    foreach($user_list as $user_row){
        $emp_id = $user_row["emp_personal_id"];
        $first_name = $user_row["first_name"];
        $last_name = $user_row["last_name"];
        $job_nm = $user_row["job_nm"];

        $output_dates = "$emp_id".",$last_name $first_name,$job_nm,".$user_row["exp_start"].",".$user_row["exp_minus"].",".$user_row["exp_coment"].",".$user_row["work_start"].",".$user_row["work_minus"].",".$user_row["work_coment"];
        $output_dates .="\r\n";
        $csv_data = mb_convert_encoding($output_dates , "SJIS-win", 'eucJP-win');
        echo($csv_data);
    }
    die;
}

function import_csv(){
    // CSVファイルからは職員IDの配列を取得
    $tmpReadLines = mb_convert_encoding(@file_get_contents($_FILES["csv"][tmp_name]), "eucJP-win", "sjis-win");
    $tmpReadLines = str_replace(array("\r\n","\r","\n"), "\n", $tmpReadLines);
    $readLines =  explode("\n", $tmpReadLines);
    dbBeginTransaction();
    StudyCommonModel::add_staff_career($readLines); // 受講生追加登録
    dbEndTransaction();

}
function default_view() {
    //------------------------------------------------------
    // ページング
    //------------------------------------------------------
    $per_page_size = (int)@$_REQUEST["per_page_size"];
    if (($per_page_size >=0 && $per_page_size<5) || $per_page_size>=10000) $per_page_size = 10;
    $page_num = (int)@$_REQUEST["page_num"];
    if ($page_num<1) $page_num = 1;


    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    require_once 'conf/smarty_start.conf';
    $contents_id = $_REQUEST["contents_id"];
    $total_count = 0;
    $joken = array(
        "class"  => $_REQUEST["class"],     // 部
        "attr"   => $_REQUEST["attr"],      // 課
        "dept"   => $_REQUEST["dept"],      // 科
        "room"   => $_REQUEST["room"],      // 室
        "id"     => $_REQUEST["emp_id"], // 職員ID
        "name"   => $_REQUEST["emp_name"]  // 職員名
    );


    $total_count = count(StudyCommonModel::get_all_staff_career($joken));

    $last_page = 1;
    if ($per_page_size > 0) $last_page = ceil($total_count / $per_page_size);
    if ($last_page < $page_num) $page_num = $last_page;

    $joken["per_page_size"] = $per_page_size;
    $joken["page_num"] = $page_num;


    $emp_list = StudyCommonModel::get_user_list($joken);

    foreach ($emp_list as $key => $value){
        $key_class[$key] = $value['class_nm'];
        $key_atrb[$key] = $value['atrb_nm'];
        $key_dept[$key] = $value['dept_nm'];
        $key_room[$key] = $value['room_nm'];
        $key_last[$key] = $value['last_name'];
        $key_first[$key] = $value['first_name'];
    }
    //array_multisort ($key_class , SORT_ASC ,$key_atrb , SORT_ASC , $key_dept , SORT_ASC , $key_room , SORT_ASC , $key_last , SORT_ASC , $key_first , SORT_ASC,$emp_list );



    $all_staff_career = StudyCommonModel::get_all_staff_career($joken);
    // ページング
    $smarty->assign("page_num", $page_num);
    $smarty->assign("per_page_size", $per_page_size);
    $smarty->assign("total_count", $total_count);
    $smarty->assign("last_page", $last_page);


    //------------------------------------------------------
    // コンボボックス選択肢
    //------------------------------------------------------
    $smarty->assign("class_list", StudyCommonModel::get_class_list(1));
    $smarty->assign("attr_list", StudyCommonModel::get_attr_list(0,1));
    $smarty->assign("dept_list", StudyCommonModel::get_dept_list(0,1));
    $smarty->assign("room_list", StudyCommonModel::get_room_list(0,1));

    //staffの情報
    $smarty->assign("all_staff_career", $all_staff_career);

    //選択されたコンボボックスの値
    $smarty->assign("class", $_REQUEST["class"]);
    $smarty->assign("attr", $_REQUEST["attr"]);
    $smarty->assign("dept", $_REQUEST["dept"]);
    $smarty->assign("room", $_REQUEST["room"]);
    $smarty->assign("id", $_REQUEST["emp_id"]);
    $smarty->assign("name", $_REQUEST["emp_name"]);

    $smarty->display("study_edu_nensuu.tpl");

}

?>