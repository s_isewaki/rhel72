<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/about_comedix.php');

/*
 * ツリー構造を出力する
 * @param $params["folder_list"] 講座リスト
 * @param $params["tree_id"] 講座ツリーのID
 * @param $params["selected_folder_id"] 選択する講座
 */
function smarty_function_tree($params, &$smarty) {
    /*
     * スタイルを取得する。
     * @param $folder_id 講座ID
     * @param $selected_id 選択講座ID
     */
    function get_style($folder_id, $selected_id) {
        if ($folder_id === $selected_id) {
            return "selected_fileitem"; // 選択状態
        } else {
            return "unselected_fileitem"; // 未選択状態
        }
    }

    /*
     * 子講座を再帰的に表示する。
     * @param $folder_list 全ての講座
     * @param $tree_id ツリーID
     * @param $parent_id 親講座のID
     * @param $selected_id 選択する講座ID
     */
    function output_children($folder_list, $tree_id, $parent_id, $selected_id) {
        $filter = create_function('$folder', 'return $folder["parent_id"]  == '.$parent_id.';'); // 親講座IDで講座を探すフィルタ
        $children = array_filter($folder_list, $filter); // 子講座を探す
        if (count($children) > 0) {
            // 子がいたら更にツリーをネスト
            echo '<ul>';
            foreach ($children as $child) {
                $css = get_style($child["id"], $selected_id); // スタイルを取得
                echo '<li><span class="folder"><a type="tree" folder_id="'.$child["id"].'" parent_id="'.$parent_id.'" class="folder '.$css.'" id="'.$tree_id.'_folder_'.$child["id"].'" >'.str_replace("\t", WBR."＞",h($child["long_name"])).'</a></span>';
                output_children($folder_list, $tree_id, $child["id"], $selected_id); // 子講座を探しに再帰
                echo '</li>';
            }
            echo '</ul>';
        }
    }

    $folder_list = $params["folder_list"]; // 講座の連想配列
    $tree_id = $params["tree_id"]; // ツリーID
    $selected_id = $params["selected_folder_id"]; // 選択状態にする講座ID
    if ($folder_list == null || count($folder_list) == 0) return;
    echo '<ul id="'.$params["tree_id"].'" class="filetree" >';
    foreach ($folder_list as $folder) {
        // parent_idがnullの場合はroot要素
        if ($folder["parent_id"] == null) {
            // スタイル取得
            $css = get_style($folder["id"], $selected_id);
            echo '<li><span class="folder"><a root=true name="'.str_replace("\t", WBR."＞",h($folder["long_name"])).'" type="tree" folder_id="'.$folder["id"].'" parent_id="" class="folder '.$css.'" id="'.$tree_id.'_folder_'.$folder["id"].'" >'.str_replace("\t", WBR."＞",h($folder["long_name"])).'</a></span>';
            output_children($folder_list, $tree_id, $folder["id"], $selected_id); // 子の出力
            echo '</li>';
        }
    }
    echo '</ul>';
    return;
}
?>
