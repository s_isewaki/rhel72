<?
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// アンケート関連を扱うURL
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
require_once 'StudyInitController.php';
require_once 'model/StudyCommonModel.php';
require_once 'model/StudyQuestionnaireModel.php';
require_once 'model/StudyContentsModel.php';


$btn_event = $_REQUEST["btn_event"];

if ($btn_event=="be_create_questionnaire") {
    create_questionnaire_and_die();
}
if ($btn_event=="be_result_questionnaire" || $btn_event=="be_questionnaire_excel" || $btn_event=="be_questionnaire_csv"){
    show_result_questionnaire_and_die($btn_event);
}
if ($btn_event=="be_show_questionnaire_user" || $btn_event=="be_exec_questionnaire_user" || $btn_event=="be_exec_questionnaire_user_clear") {
    show_questionnaire_user_and_die($btn_event);
}
if ($btn_event=="be_edit_questionnaire" || $btn_event=="be_add_questionnaire") {
    show_edit_questionnaire_and_die($btn_event);
}



default_view($btn_event);
die;



//**************************************************************************************************
// アンケート編集画面表示
//**************************************************************************************************
function default_view($btn_event) {

    $contents_id = $_REQUEST["contents_id"];

    // 研修・講義タイトルを取得
    $contents = StudyContentsModel::get_contents_row($contents_id);
    $start_date = $contents["questionnaire_start_date"];
    $end_date = $contents["questionnaire_end_date"];

    //------------------------------------------------------
    // 登録済みアンケート取得
    // 登録済みアンケートがあれば更新、なければ新規モードで画面表示
    //------------------------------------------------------
    $arr_info = StudyQuestionnaireModel::get_questionnaire($contents_id);
    if ($arr_info) {
        $mode = "更新";

        $array_start = conv_ymdhm_to_jp_and_time($contents["questionnaire_start_date"]);
        $array_end = conv_ymdhm_to_jp_and_time($contents["questionnaire_end_date"]);

        $key = 1;
        for($j = 0; $j < count($arr_info); $j++){
            $arr_data[$key]["question"] = $arr_info[$j]["question"];
            $arr_data[$key]["question_type"] = $arr_info[$j]["type"];
            $arr_data[$key]["required"] = ($arr_info[$j]["required"] == "t") ? "1" : "0" ;
            $arr_data[$key]["choice_cnt"] = $arr_info[$j]["choice_cnt"];
            $arr_data[$key]["choice_item"] = explode(",", $arr_info[$j]["choices"]);
            $key++;
        }

    }else{
        $mode= "登録";
        $arr_data = array();
        for($j = 1; $j <= 9; $j++){
            $arr_data[$j] = array("question"=>"","question_type"=>1, "required"=>0, "choice_cnt"=>0, "choice_item"=>array(""));
        }
        //初期状態
        $arr_data[1]["question"] = "研修の目的、目標は達成できましたか？";
        $arr_data[1]["choice_item"] = array("達成できた", "ほぼ達成できた", "あまり達成できなかった", "達成できなかった");
        $arr_data[2]["question"] = "研修受講により理解は深まりましたか？";
        $arr_data[2]["choice_item"] = array("大変深まった", "深まった", "あまり深まらなかった", "全く深まらなかった");
        $arr_data[3]["question"] = "研修の難易度はいかがでしたか？";
        $arr_data[3]["choice_item"] = array("難易度が高すぎる", "適当である", "難易度が低すぎる");
        $arr_data[4]["question"] = "研修の方法（進め方)は適切だと思いましたか？";
        $arr_data[4]["choice_item"] = array("適切である", "ほぼ適切である", "やや適切でない");
        $arr_data[5]["question"] = "研修の内容はわかりやすいものでしたか？";
        $arr_data[5]["choice_item"] = array("大変わかりやすい", "わかりやすい", "ややわかりにくい", "わかりにくい");
        $arr_data[6]["question"] = "研修の内容は今後の職務にお役立ちますか？";
        $arr_data[6]["choice_item"] = array("役立つ", "少し役立つ", "あまり役立たない", "役立たない");
        $arr_data[7]["question"] = "今回の研修のご感想をお聞かせください。";
        $arr_data[8]["question"] = "今後受講してみたい研修があればお聞かせください。";
        $arr_data[9]["question"] = "その他、教育・研修全般について、ご意見があれば、何でも結構ですのでご記入ください。";
        for($j = 1; $j <= 9; $j++){
            $cnt = count($arr_data[$j]["choice_item"]);
            if ($cnt==1) $arr_data[$j]["question_type"] = 3;
            if ($cnt>=2) $arr_data[$j]["choice_cnt"] = $cnt;
        }
    }

    //------------------------------------------------------
    // 権限
    //------------------------------------------------------
    $auth_matrix = StudyCommonModel::get_auth_matrix("study_questionnaire_list.tpl@all", $contents["folder_id"], "");
    $disp_auth = $auth_matrix["disp_auth"];
    $edit_auth = $auth_matrix["edit_auth"];

    //------------------------------------------------------
    // 画面表示
    //------------------------------------------------------
    require_once 'conf/smarty_start.conf';
    $smarty->assign("contents", $contents);
    $smarty->assign("disp_auth", $disp_auth);
    $smarty->assign("edit_auth", $edit_auth);
    $smarty->assign("contents_id", $contents_id);
    $smarty->assign("questionnaire", $arr_data);
    $smarty->assign("mode", $mode);
    $smarty->assign("array_start", $array_start);
    $smarty->assign("array_end", $array_end);
    $smarty->assign("result", $_REQUEST["result"]);
    $smarty->display("study_questionnaire_list.tpl");
}









//**************************************************************************************************
// アンケート作成登録・編集登録
//**************************************************************************************************
function create_questionnaire_and_die() {
    $info = $_REQUEST["question_info"];
    $contents_id = $_REQUEST["contents_id"];
    $start_date = $_REQUEST["start_date"];
    $end_date = $_REQUEST["end_date"];

    
    
    if ($contents_id){
        if(!empty($info) && is_array($info)){
            StudyQuestionnaireModel::create_questionnaire($contents_id, $info, $start_date, $end_date);
        }
    }
    header("Location: study_questionnaire.php?session=".GG_SESSION_ID."&contents_id=".$contents_id."&result=success");
    die;
}






//**************************************************************************************************
// アンケート詳細項目編集画面を開く
//**************************************************************************************************
function show_edit_questionnaire_and_die($btn_event) {
    require_once 'conf/smarty_start.conf';
    $smarty->assign("mode", $_REQUEST["mode"]);
    $smarty->assign("item_no", $_REQUEST["item_no"]);
    $smarty->display("study_questionnaire_edit.tpl");
    die;
}








//**************************************************************************************************
// ユーザのアンケート入力画面処理
//**************************************************************************************************
function show_questionnaire_user_and_die($btn_event) {
    require_once 'conf/smarty_start.conf';
    $contents_id = $_REQUEST["contents_id"];

    $target_emp_id = $_REQUEST["target_emp_id"];
    if (!GG_ADMIN_FLG && !GG_STUDY_ADMIN_FLG && !GG_STUDY_TANTOU_FLG) $target_emp_id = GG_LOGIN_EMP_ID;
    if (!$target_emp_id) $target_emp_id = GG_LOGIN_EMP_ID;

    $target_emp_name = "";
    if ($_REQUEST["target_emp_id"]) {
        $emp_info = StudyCommonModel::get_user_data_by_emp_id($target_emp_id);
        $target_emp_name = $emp_info["emp_lt_nm"]." ".$emp_info["emp_ft_nm"];
    }

    $rows = StudyQuestionnaireModel::get_questionnaire_answer($contents_id, $target_emp_id);
    $smarty->assign("success", "");

    $is_delete_only = "";
    if ($btn_event=="be_exec_questionnaire_user_clear") $is_delete_only = "1";

    //==========================================================================
    // 新規登録・更新・削除兼用処理
    //==========================================================================
    if ($btn_event=="be_exec_questionnaire_user" || $btn_event=="be_exec_questionnaire_user_clear") {
        StudyQuestionnaireModel::update_questionnaire_answer($contents_id, $target_emp_id, $rows, $is_delete_only);
        $smarty->assign("success", "true");
    }

    //------------------------------------------------------
    // 研修・講義から書き込み可能期間を取得
    //------------------------------------------------------
    $contents = StudyContentsModel::get_contents_row($contents_id);
    $start_date = $contents["questionnaire_start_date"];
    $end_date = $contents["questionnaire_end_date"];
    $is_out_of_date = "";
    if (date("YmdHi") < $start_date || $end_date < date("YmdHi")) $is_out_of_date = "1";

    //------------------------------------------------------
    // 表示処理
    //------------------------------------------------------
    $smarty->assign("is_out_of_date", $is_out_of_date);
    $smarty->assign("questionnaire", $rows);
    $smarty->assign("target_emp_name", $target_emp_name);
    $smarty->assign("item_count", count($rows));
    $smarty->assign("contents_id", $contents_id);
    $smarty->assign("contents_title", $contents["title"]);
    $smarty->display("study_questionnaire_user.tpl");
    die;
}




//**************************************************************************************************
// アンケート結果一覧画面を開く
//**************************************************************************************************
function show_result_questionnaire_and_die($btn_event) {
    
    //------------------------------------------------------
    // 受講者条件取得
    //------------------------------------------------------
    $joken = array(
        "job"=>@$_REQUEST["emp_job"],
        "emp_job"=>@$_REQUEST["emp_job"],
    );
    
    $contents_id = $_REQUEST["contents_id"];
    $contents = StudyContentsModel::get_contents_row($contents_id);
   // $student_count = $contents["student_count"]; // 受講者数
   
     
    $start_date = conv_ymdhm_to_jp_and_time2($contents["questionnaire_start_date"]);
    $end_date = conv_ymdhm_to_jp_and_time2($contents["questionnaire_end_date"]);
    
    if (!$start_date) $start_date = "未設定";
    if (!$end_date) $end_date   = "未設定";

    
    //--------------------------
    // アンケート設問取得
    //--------------------------
    $questionaire_list = StudyQuestionnaireModel::get_questionnaire($contents_id);
    //--------------------------
    // アンケート回答取得 アンケート回答日時降順
    //--------------------------
    $impression_list = StudyQuestionnaireModel::get_questionnaire_all_answer($contents_id,$joken);
    $student_count = count(StudyCommonModel::get_student_list($contents_id,$joken, 1, 0, 0)); // 受講者数
    //--------------------------
    // 設問ループ、設問番号順に出力内容を準備する
    //--------------------------
    $ret = array();
    foreach ($questionaire_list as $item) {
        $no = $item["no"]; // 設問番号
        //---------------
        // 単数複数選択肢別の集計コンテナ準備
        //---------------
        $choices = array();
        $ary = explode(",", $item["choices"]);
        for ($idx=1; $idx<=count($ary); $idx++) {
            $choices[$idx] = array(
                "choice_string" => $ary[$idx-1], // 選択肢文字列
                "choice_count"  => 0, // この選択肢へ回答した人数
                "choice_ratio"  => 0  // この選択肢への回答率
            );
        }
        //---------------
        // 設問別の集計コンテナ準備
        //---------------
        $ret[$no] = array(
            "type"=>$item["type"], // 1:単数ラジオ 2:複数チェックボックス 3:文章入力
            "question"=>$item["question"], // 設問
            "choices"=>$choices,     // 選択肢別回答集計コンテナ
            "text_answers"=>array(), // 文章入力の場合の回答配列
            "text_answers_ratio"=>0  // 文章入力の場合の回答率
        );
    }
    //--------------------------
    // 回答を紐つける
    //--------------------------
    $type3_answer_exist = 0;
 
    foreach($impression_list as $item) {        
        $no = $item["no"];
        if ($item["impression"] == null) continue; // 回答なしならスルー
        if ($ret[$no]["type"] == "3") { // テキスト回答の場合
            $ret[$no]["text_answers"][] = array(
                "impression"=>$item["impression"], 
                "emp_id"=>$item["emp_id"],
                "emp_personal_id"=>$item["emp_personal_id"],
                "emp_name"=>$item["emp_name"]
            );
            $type3_answer_exist = 1;
        }
        else{
            $impressions = explode(",", $item["impression"]); // 回答番号カンマつなぎをバラす
            for ($idx=1; $idx<=count( $ret[$no]["choices"]); $idx++) {
                for($mdx=1;$mdx<=count($impressions); $mdx++){
                    if( $impressions[$mdx-1] == $idx){
                        $ret[$no]["choices"][$idx]["choice_count"]++; // 回答プラス
                        break;
                    }
                }
            }
           // for ($idx=1; $idx<=count($impressions); $idx++) {
              //  $ret[$no]["choices"][$item["impression"]]["choice_count"]++; // 回答プラス
            //}
        }
    }
    //--------------------------
    //データを集計する
    //--------------------------
    foreach($ret as $no => $item){
        foreach ($item["choices"] as $idx => $choice) {
            $ret[$no]["choices"][$idx]["choice_ratio"] = get_percent($choice["choice_count"], $student_count, 1, 100);
        }
        $ret[$no]["text_answers_ratio"] = get_percent($item["text_answers"], $student_count, 1, 100);
    }

    //------------------------------------------------------
    // 画面表示の場合
    //------------------------------------------------------
    if ($btn_event=="be_result_questionnaire") {
        $job_list = StudyCommonModel::get_job_list();
        $template_vars = array(
            "type3_answer_exist" => $type3_answer_exist,
            "start_date" => $start_date,
            "end_date" => $end_date,
            "total" => $ret,
            "contents" => $contents,
            "contents_id" => $contents_id,
            "job_list" => $job_list,
            "emp_job" => @$_REQUEST["emp_job"], // 職種
                
        );
        display_template("study_questionnaire_result.tpl", $template_vars);
        die;
    }

    //------------------------------------------------------
    // エクセルの場合
    //------------------------------------------------------
    if ($btn_event=="be_questionnaire_excel") {
        require_once 'conf/smarty_start.conf';
        $smarty->assign("total", $ret);
        $smarty->assign("contents", $contents);
        $smarty->assign("contents_id", $contents_id);
        $smarty->assign("start_date", $start_date);
        $smarty->assign("end_date", $end_date);

        ob_start();
        $excel_name = mb_convert_encoding($contents["title"], "SJIS-win", 'eucJP-win');
        $smarty->display("study_questionnaire_excel.tpl");
        $excel_data = mb_convert_encoding(ob_get_contents(), "SJIS-win", 'eucJP-win');
        ob_end_clean();
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=questionnaire.xls");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");
        echo($excel_data);
        die;
    }
    //------------------------------------------------------
    // CSVの場合
    //------------------------------------------------------
     if ($btn_event=="be_questionnaire_csv") {
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=questionnaire.csv");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");
        
        $classnm = GG_CLASS_NM; // 部
        $atrbnm = GG_ATRB_NM;   // 課
        $deptnm = GG_DEPT_NM;   // 科
        $roomnm = GG_ROOM_NM;   // 室 値がなければ３階層まで
       
        $title = "職員ID,氏名,職種,役職,$classnm,$atrbnm,$deptnm";
        if(GG_CLASS_CNT == 4){
            $title .=",$roomnm";
        }
        
        foreach ($questionaire_list as $item) { 
            if($item["type"] == 3){
               $ans = "感想";
            }else{
                $ans = $item["choices"];
            }
            $title .=",設問,$ans";
        }
        $title .="\n";
        $excel_name = mb_convert_encoding($title, "SJIS-win", 'eucJP-win');
        echo($excel_name);

        //ここまでヘッダ
        
        $hairetu = array();
        foreach($impression_list as $item_id) {
            if(!in_array($item_id["emp_id"], $hairetu)){
                array_push($hairetu, $item_id["emp_id"]);
            }
        }
        
        foreach($hairetu as $item_impres) {
            $joken["emp_id"] = $item_impres;
            $user_list = StudyCommonModel::get_user_list($joken,2);
            
            foreach ($user_list as $key => $value){
                $key_class[$key] = $value['class_nm'];
                $key_atrb[$key] = $value['atrb_nm'];
                $key_dept[$key] = $value['dept_nm'];
                $key_room[$key] = $value['room_nm'];
                $key_last[$key] = $value['last_name'];
                $key_first[$key] = $value['first_name'];
            }
            //array_multisort ($key_class , SORT_ASC ,$key_atrb , SORT_ASC , $key_dept , SORT_ASC , $key_room , SORT_ASC , $key_last , SORT_ASC , $key_first , SORT_ASC,$user_list );

  
            $anser_list = StudyQuestionnaireModel::get_questionnaire_answer($contents_id, $item_impres);
            
            foreach($user_list as $row){  
                $emp_id = $row["emp_id"];
                $emp_personal_id = $row["emp_personal_id"];
                $first_name = $row["first_name"];
                $last_name = $row["last_name"];
                $job_nm = $row["job_nm"];
                $st_nm = $row["st_nm"];
                $class_nm = $row["class_nm"];
                $atrb_nm = $row["atrb_nm"];
                $dept_nm = $row["dept_nm"];
                $room_nm = $row["room_nm"];
            }
            
            $output_dates = "$emp_personal_id,$last_name $first_name,$job_nm,$st_nm,$class_nm,$atrb_nm,$dept_nm,";
            if(GG_CLASS_CNT == 4){
                $output_dates .="$room_nm,";
            }
                     
            foreach ($anser_list as $item_question) {
                $str = str_replace(array("\r\n","\r","\n"), '', $item_question["question"]);
                $output_dates .=  commaEscape4csv($str);//設問
                $output_dates .=",";
                
                if ($item_question["type"] == "3") { // テキスト回答の場合
                    $str = str_replace(array("\r\n","\r","\n"), '', $item_question["impression"]);
                     $output_dates .= commaEscape4csv($str);
                     $output_dates .=",";
                }else if ($item_question["type"] == "1"){//単数
                    $icount = $item_question["choice_cnt"];//回答数
                    for ($idx=1; $idx<=$icount; $idx++) {
                        if($item_question["impression"] == $idx){
                            $output_dates .="1,";
                       }else{
                            $output_dates .="0,";
                        }
                    }
                }else{//複数回答
                    $imps = explode(",", $item_question["impression"]); // 回答番号カンマつなぎをバラす
                    $icount = $item_question["choice_cnt"];
                    for ($idx=1; $idx<=$icount; $idx++) {
                        for($mdx=0;$mdx<=count($imps); $mdx++){
                            if($imps[$mdx] == $idx){
                               $multi_flg = true;
                               break;
                             }else{
                               $multi_flg = false;
                            }
                        }
                        if ($multi_flg){  
                            $output_dates .="1,";
                        }else{
                            $output_dates .="0,";
                        }
                    }
                }
            }
            
            $output_dates .="\n";
            $csv_data = mb_convert_encoding($output_dates , "SJIS-win", 'eucJP-win');
            echo($csv_data);       
        }
      die;
     }
}

function commaEscape4csv($str)
{
  if(!(strstr($str, ',') === False)){
    $str = preg_replace('/"/', '""',$str);
    $str = '"' . $str . '"';
  }
  return $str;
}



function show_job_questionnaire_and_die($btn_event) {
    
    
    
    die;
}

?>