{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}研修・講義</title>
    <script type="text/javascript" src="js/jquery.treeview.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.treeview.css" />
    <style type="text/css">
        {literal}
            html { overflow-y:scroll; }
        {/literal}
    </style>
</head>
<body onresize="resized()">
<!-- ///study_contents  -->




<script type="text/javascript">

    var result = "{$result}";
    var contents_id = "{$contents.id}";
    var policy_code = "{$contents.policy_code}";
    var policy_for_questionnaire = "{$contents.policy_for_questionnaire}";
    var policy_for_test = "{$contents.policy_for_test}";
    var student_count = {php} echo (int)@$contents["student_count"]; {/php};
    var btn_event = "{$btn_event}";
    var folder_id = "{$folder_id}";
    var org_folder_id = "{$org_folder_id}";
    var js_colrow_data = {literal}{{/literal}{$js_colrow_data}{literal}}{/literal};
    {literal}

    function show_folder_selector(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var url="study_folder.php?event=ev_pickup_folder&session="+session+"&current_folder_id="+ee("save_folder_id").value+"&js_callback=callbackFolderSelectedForContents&call_from=study_contents";
        window_open(url, "folder_edit", 600, 380);
    }
    function callbackFolderSelectedForContents(folder_id, folder_name) {
        ee("span_folder_name").innerHTML = folder_name;
        ee("save_folder_id").value = folder_id;
    }

    function file_change(elem) {
        var ext_list = {
            "movie":["mpg","mp4","wmv","avi","ogg","mov","asf","mkv","divx","xvid"],
            "browser":["pdf","swf","jpg","gif","png","jpeg","bmp","tif","tiff","mid","midi","wav","mp3","asx","au","aif","qt","mka"]
        }
        if (!elem.name) return;
        var idx = elem.name.substring(4);
        // ファイル名を取得して表示
        var full_file_name = $(elem).val();
        var path_array = full_file_name.split("\\");
        var file_name = path_array[path_array.length-1];
        var ex_strs = file_name.split(".");
        var ex_str = ex_strs[ex_str.length-1];
        var ftype = "";
        for (var ft in ext_list) {
            for (var ii=0; ii<ext_list[ft].length; ii++) {
                if (ex_str==ext_list[ft]) { ftype = ft; break; }
            }
            if (ftype) break;
        }
        $("[name=file_type"+idx+"]").val(ftype);
    }

    function uploadFileToggle(btn, idx) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var toDisp = (ee("new_file_td"+idx).style.display=="none");
        ee("new_file_td"+idx).style.display = (toDisp ? "" : "none");
        ee("delete_msg"+idx).style.display = "none";
        ee("delete_flag"+idx).value = "";
    }
    function uploadFileDelete(btn, idx) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var del = ee("delete_flag"+idx).value;
        del = (del ? "" : "1");
        ee("delete_flag"+idx).value = del;
        ee("new_file_td"+idx).style.display = "none";
        ee("delete_msg"+idx).style.display = (del ? "" : "none");
        ee("td_input"+idx).style.backgroundColor = (del ? "#dddddd" : "transparent");
    }

    function getByte(string){
        if(string.length == 0){return 0;}
        var count = 0;
        var str = "";
        for(var i = 0; i <string.length; i++){
            if( escape(string.charAt(i)).length  < 4 ) count = count + 1;
            else count = count + 2;
        }
        return count;
    }
    function rdoChange(idx) {
        var isUpload = ee("file_whichA"+idx).checked;
        ee("divA"+idx).style.display = (isUpload ? "block" : "none");
        ee("divB"+idx).style.display = (isUpload ? "none" : "block");
    }
    // 複製ボタン
    function copy_contents(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        if (!common_check(1)) return;
        if (!confirm("複製を作成します。よろしいですか？")) return;
        $("#btn_event").val("be_copy");
        start_submit();
    }
    // 作成ボタン
    function create_contents(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        ee("policy_jp1").value = ee("policy_code_pre").options[ee("policy_code_pre").selectedIndex].text;
        ee("policy_jp2").value = ee("policy_code").options[ee("policy_code").selectedIndex].text;
        if (!common_check()) return;
        $("#btn_event").val("be_create");
        start_submit();
    }
    // 編集ボタン
    function update_contents(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        ee("policy_jp1").value = ee("policy_code_pre").options[ee("policy_code_pre").selectedIndex].text;
        ee("policy_jp2").value = ee("policy_code").options[ee("policy_code").selectedIndex].text;
        if (!common_check()) return;
        $("#btn_event").val("be_update");
        start_submit();
    }
    // 削除ボタン
    function delete_contents(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        if (!confirm("削除します。よろしいですか？")) return;
        $("#btn_event").val("be_delete");
        start_submit();
    }

    function start_submit() {
        //ee("contents_container").style.display = "none";
        ee("div_wait").style.display = "block";
        $(".btn-main").addClass("btn-off");
        $(".btn-main").addClass("btn-off");

        // IE11のみ裏でsubmit実行
        var ua = window.navigator.userAgent.toLowerCase();
        if (ua.indexOf("msie")== -1 && ua.indexOf('trident/7') != -1){
            setInterval(function() {
                if (opener && opener!=window && opener.document && opener.document.dummyform) {
                    opener.document.dummyform.submit();
                }
            }, 800);
        }
        document.mainform.submit();
    }

    //出欠登録画面を開く
    function open_student_list(assembly_actual_status, test_status) {
        var url = "study_student.php?session="+session+"&contents_id=" + contents_id +
        "&assembly_actual_status="+assembly_actual_status+"&test_status="+test_status;
        window_open(url, "setting_student3", 1000, 650);
        return false;
    }

    // 共通チェック(作成も更新)
    function common_check(isCopy) {
        if ($("[name=contents_name]").val() == "") { alert("研修講義タイトルが入力されていません。"); $("[name=contents_name]").focus(); return false;  }
        if (getByte($("[name=contents_name]").val()) > 100) { alert("研修講義タイトルが長すぎます。"); $("[name=contents_name]").focus(); return false; }

        var cnt = parseInt($("#file_count").val());
        for (var idx=1; idx<=cnt; idx++) {
            var pre = "「教材・テキスト"+idx+"」の";
            if (isNaN($("[name=file_no"+idx+"]").val())) {
                alert(pre+"教材・テキスト番号には数値を入力してください。");
                $("[name=file_no]").focus();
                return false;
            }
            if (ee("file_whichB"+idx).checked) {
                if (ee("file_url"+idx).value=="") { alert(pre+"URLを指定してください。"); return false; }
                if (ee("file_name"+idx).value=="") { alert(pre+"リンク表示を指定してください。"); return false; }
                if (!checkUrl(idx)) return false;
            }
        }
        if (getByte($("[name=keyword]").val()) > 100) { alert("検索キーワードが長すぎます。"); $("[name=keyword]").focus(); return false; }
        if (!$("[name=save_folder_id]").val()) { alert("保存先を選択してください。"); return false; }

        var url =
        "study_contents.php?session="+session+"&btn_event=be_check_duplicate_contents&t="+(new Date())+
        "&folder_id="+ee("save_folder_id").value+"&contents_name_utf16hex="+utf16Hex($("[name=contents_name]").val());
        if (!isCopy && contents_id) url += "&contents_id="+contents_id;
        var ret = "";
        $.ajax({
          url:url, type:'GET', async:false, success:function(msg) {
            if (msg=="ok") { ret = true; return; }
            alert(msg);
            ret = false;
          }
        });
        return ret;
    }

    function checkUrl(idx) {
        var v = ee("file_url"+idx).value;
        if (v=="") { alert("URLを指定してください。"); return false; }
        if (v.length<6) { alert("URLが短かすぎます。"); return false; }
//        if (v.substring(0,5)!="http:" && v.substring(0,6)!="https:" && v.substring(0,4)!="ftp:") {
//            alert("URL先頭文字は「http」「https」または「ftp」のみ指定可能です。");
//            return false;
//        }
        if (!checkUrl2(v)) return false;
        return true;
    }
    function checkUrl2(v) {
        if (!v.match(/[\*\"\<\>\|\{\}\'\(\}\[\]]/)) return true; // ※厳密ではない。危険そうなもののみ
        alert("URLに指定できない文字が利用されています。");
        return false;
    }

    function show_contents_summary() {
        var new_policy_code = getSelectionValue(ee("policy_code"));
        if (!new_policy_code) return alert("受講済条件を指定してください。");
        var url = "study_contents.php?session="+session+"&contents_id=" + contents_id +
        "&btn_event=be_show_contents_summary&policy_code="+encodeURIComponent(new_policy_code);
        window_open(url, "wn_contents_summary", 700, 550);
    }

    // body_onload
    $(document).ready(function() {
        if (result=="success") {
            if (opener && opener!=window && opener.document && opener.document.mainform) {
                var frm = opener.document.mainform;
                if (btn_event=="be_create" || btn_event=="be_copy") {
                    if (frm.sort_string) frm.sort_string.value = "create_date-desc";
                    if (frm.page_num) frm.page_num.value = "1";
                    if (frm.selected_folder_id) frm.selected_folder_id.value = folder_id;
                } else {
                    if (frm.selected_folder_id && int(folder_id)>0) {
                        frm.selected_folder_id.value = folder_id;
                        if (folder_id!=org_folder_id && frm.page_num) frm.page_num.value = "1";
                    }
                }
            }
            parentRefreshRequest("mainform");
            window.close();
            return;
        }
        PolicyTable.initializePolicy(policy_code, student_count, js_colrow_data, policy_for_questionnaire, policy_for_test);
    });
    {/literal}
</script>

<iframe frameborder="0" name="download_iframe" id="download_iframe" style="position:absolute; left:0; top:0 width:0; height:0"></iframe>




{if $result ne "success" or $btn_event ne "be_delete"}


<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>研修・講義</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="{$contents.title|escape}" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>




<div class="usual_box" id="contents_container">
<form name="mainform" action="study_contents.php?t={php} echo date("YmdHis"); {/php}" method="post" enctype="multipart/form-data">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <input type="hidden" name="contents_id" value="{$contents.id}" />
    <input type="hidden" name="save_folder_id" id="save_folder_id" value="{$contents.folder_id}" />
    <input type="hidden" name="org_folder_id" id="org_folder_id" value="{$contents.folder_id}" />
    <input type="hidden" name="event" value="{$event}" />
    <input type="hidden" name="policy_jp1" id="policy_jp1" value="" />
    <input type="hidden" name="policy_jp2" id="policy_jp2" value="" />
    <input type="hidden" name="btn_event" id="btn_event" value="" />
    <input type="hidden" name="result" value="{$result}" />


    {php}
        $contents = $this->_tpl_vars["contents"];
        $btnOff = "";
        $btnOff2 = "";
        if ($contents["test_ok_count"] or $contents["test_ng_count"] or $contents["questionnaire_answer_count"] or $contents["assembly_actual_count"]) $btnOff = " btn-off";
        if (!$this->_tpl_vars["edit_auth"]) {
            $btnOff = " btn-off";
            $btnOff2 = " btn-off";
        }
    {/php}



    {php} if (!$this->_tpl_vars["edit_auth"]) { {/php}
        <div style="padding-bottom:5px"><div class="warningbox">権限がありません。参照のみ可能です。</div></div>
    {php} } else { {/php}
    {php}
        $contents = @$this->_tpl_vars["contents"];
        if ($contents["test_ok_count"] || $contents["test_ng_count"] || $contents["questionnaire_answer_count"] ||  $contents["assembly_actual_count"]) {
    {/php}
            <div style="padding-bottom:5px"><div class="warningbox">既に実施者が存在します。削除は行えません。</div></div>
        {php} } {/php}
    {php} } {/php}


    <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th width="120px"><nobr>研修講義タイトル</nobr> </th>
            <td><input name="contents_name" maxlength="50" type="text" class="text"
                value="{$contents.title|escape}" style="width:600px;ime-mode:active;"></td>
        </tr>
        <tr>
            <th>内容</th>
            <td><textarea name="summary" rows="3" cols="40" style="width:700px; ime-mode:active;">{$contents.summary|escape}</textarea></td>
        </tr>
    </table>




    <div style="padding-top:10px">
    <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th width="120px"><nobr>実施内容</nobr> </th>
            <td><select id="policy_code_pre" onchange="PolicyTable.pcode_pre_selected(this.value)"
                    onkeyup="PolicyTable.pcode_pre_selected(this.value)" style="width:400px"></select>

                <table cellspacing="0" cellpadding="0" class="rgroup">
                <tr id="div_policy_for_questionnaire" style="display:none">
                <td style="padding-top:4px"><span class="brown">アンケートについて</span></td>
                <td style="padding-top:4px">
                <select id="policy_for_questionnaire" name="policy_for_questionnaire">
                    <option value="">全員</option>
                    <option value="A+">集合研修出席者のみ</option>
                    <option value="A-">集合研修欠席者のみ</option>
                </select>
                が実施可能
                </td>
                </tr>
                <tr id="div_policy_for_test" style="display:none">
                <td style="padding-top:4px"><span class="brown">テストについて</span></td>
                <td style="padding-top:4px"><select id="policy_for_test" name="policy_for_test">
                    <option value="">全員</option>
                    <option value="A+">集合研修出席者のみ</option>
                    <option value="A-">集合研修欠席者のみ</option>
                </select>
                が実施可能
                </td>
                </tr></table>
                <span style="display:none">
                    <select name="policy_code" id="policy_code" onchange="PolicyTable.makeSummaryDisplay(this.value)"
                        onkeyup="PolicyTable.makeSummaryDisplay(this.value)" style="width:400px"></select>
                </span>
            </td>
        </tr>
    </table>
    </div>


    <div id="flow_block" style="display:none"></div>




    <div style="padding-top:10px">
    <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th width="120">講座</th>
            <td><table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                <td><span id="span_folder_name">{$folder_name|escape}</span></td>
                <td><button type="button" id="btn_folder_select" class="btn-main{php}echo $btnOff2;{/php}" onclick="show_folder_selector(this)">選択</button></td>
                </tr></table>
            </td>
        </tr>
        <tr>
            <th><nobr>検索キーワード</nobr></th>
            <td>
                <input name="keyword" type="text" class="text" size="45" maxlength="100"
                    value="{$contents.keyword|escape}" style="width:370px;ime-mode:active;">
                <span style="color:#888888">*検索キーワードは半角スペースで区切ってください。</span>
                <br />
            </td>
        </tr>
        <tr>
            <th>実施年度</th>
            <td>
                <input name="bunrui_year" type="text" size="6" maxlength="4" class="text numeric"
                    value="{$contents.bunrui_year|escape}" style="width:50px;">
            </td>
        </tr>
    </table>
    </div><!-- // end of view_bunrui -->








    <div style="padding-top:10px">
    <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th width="120px"><nobr>教材・テキスト数</nobr></th>
            <td>
                <select name="file_count" id="file_count"
                    onchange="for (var idx=1; idx<=10; idx++) ee('trFile'+idx).style.display=(parseInt(this.value,10)>=idx?'':'none');">
                {php} for ($idx=1; $idx<=10; $idx++) { {/php}
                    <option value="{php}echo $idx; {/php}" {php} if ($this->_tpl_vars['file_count']==$idx){ {/php} selected{php} } {/php}>{php}echo $idx; {/php}</option>
                 {php} } {/php}
                <option value="0"{if $file_count eq 0} selected{/if}>なし</option>
                </select>
            </td>
        </tr>





        {php}
            foreach ($this->_tpl_vars["files"] as $idx2 => $fileinfo) {
            $idx = $idx2+ 1;
        {/php}
        <tr id="trFile{php}echo $idx; {/php}" {php} if ($idx>$this->_tpl_vars['file_count']) echo ' style="display:none"';{/php}>
            <th class="pale" style="text-align:center"><nobr>教材・テキスト{php}echo $idx; {/php}</nobr></th>
            <td id="td_input{php}echo $idx; {/php}">
                <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                    <td><span class="brown">教材・テキスト番号</span></td>
                    <td><input maxlength=4 size=4 type="text" class="text" name="file_no{php}echo $idx; {/php}"
                        value="{php} echo $fileinfo["file_no"] {/php}" style="ime-mode:disabled;"></td>
                    <td><span class="brown">ファイル種類</span></td>
                    <td>
                        <select name="file_type{php}echo $idx; {/php}">
                            <option value="">ドキュメント/ダウンロード/その他一般</option>
                            <option value="browser"{php} if ($fileinfo["file_type"]=="browser") echo " selected"; {/php}>ブラウザで開く(PDF・SWF・静止画像等)</option>
                            <option value="movie"{php} if ($fileinfo["file_type"]=="movie") echo " selected"; {/php}>動画</option>
                        </select>
                    </td>
                </tr></table>



                <div style="padding-top:3px">
                <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                    <td><span class="brown">ファイルの場所</span></td>
                    <th><input type="radio" name="file_which{php}echo $idx; {/php}" value="upload"
                    id="file_whichA{php}echo $idx; {/php}" {php}if ($fileinfo["file_path"]=="") echo " checked";{/php}
                    onclick="rdoChange({php}echo $idx; {/php})"><label for="file_whichA{php}echo $idx; {/php}">アップロード</label></th>
                    <th><input type="radio" name="file_which{php}echo $idx; {/php}" value="url"
                    id="file_whichB{php}echo $idx; {/php}"  {php}if ($fileinfo["file_path"]!="") echo " checked";{/php}
                    onclick="rdoChange({php}echo $idx; {/php})"><label for="file_whichB{php}echo $idx; {/php}">URL指定</label></th>
                </tr></table>
                </div>

                <div id="divA{php}echo $idx; {/php}" style="padding-top:5px; {php}if ($fileinfo["file_url"]!="") echo " display:none";{/php}">
                    <div id="current_file_td{php}echo $idx; {/php}" style="{php}if ($fileinfo["file_path"]=="") echo " display:none";{/php}">
                        <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                            <td><span class="brown">アップロード済ファイル</span></td>
                            <td><a href="{php}echo $fileinfo["download_url"]{/php}" target="download_iframe">{php} echo h($fileinfo["file_name"]){/php}</a></td>
                            <td><button type="button" class="btn-main{php}echo $btnOff2;{/php}"
                                onclick="uploadFileToggle(this, {php} echo $idx; {/php})">変更</button></td>
                            <td><button type="button" class="btn-main{php}echo $btnOff2;{/php}"
                                onclick="uploadFileDelete(this, {php} echo $idx; {/php})">削除</button></td>
                            <td><span style="display:none; color:#888888" id="delete_msg{php} echo $idx; {/php}">更新時、ファイルは削除されます。</span></td>
                        </tr></table>
                    </div>

                    <div id="new_file_td{php}echo $idx; {/php}" style="{php}if ($fileinfo["file_path"]!="") echo " display:none";{/php}">
                        <input type="hidden" name="delete_flag{php} echo $idx; {/php}" id="delete_flag{php} echo $idx; {/php}" />
                        <span style="background-color:#f5f5f5; padding:2px; border:1px solid #dddddd"><input type="file"
                            name="file{php}echo $idx; {/php}" size="50" style="width:600px" onchange="file_change({php}echo $idx;{/php}, $(this).val())"></span>
                        <input type="hidden" name="file_path{php} echo $idx;{/php}" value="{php} echo h($fileinfo["file_path"]);{/php}"/>
                        <input type="hidden" name="cur_file_name{php} echo $idx;{/php}" value="{php} echo h($fileinfo["file_name"]);{/php}"/>
                        <input type="hidden" name="cur_file_capacity{php} echo $idx;{/php}" value="{php} echo h($fileinfo["file_capacity"]);{/php}"/>
                    </div>
                </div>



                <div id="divB{php}echo $idx; {/php}" style="padding-top:5px; {php}if ($fileinfo["file_url"]=="") echo " display:none";{/php}">
                    <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                        <td><span class="brown">URL</span></td>
                        <td><input type="text" class="text" name="file_url{php}echo $idx; {/php}" id="file_url{php}echo $idx; {/php}"
                            value="{php} echo h($fileinfo["file_url"]);{/php}" style="width:350px; ime-mode:disabled"

                             onkeyup="ee('check_url{php}echo $idx; {/php}').setAttribute('href', htmlEscape(this.value))" /></td>

                        <td><span class="brown">リンク表示</span></td>
                        <td><input type="text" class="text" name="file_name{php}echo $idx; {/php}" id="file_name{php}echo $idx; {/php}"
                            value="{php} if($fileinfo["file_url"]!="") echo h($fileinfo["file_name"]);{/php}" style="width:200px"
                            onkeyup="ee('check_url{php}echo $idx; {/php}').innerHTML=htmlEscape(this.value); ee('check_url{php}echo $idx; {/php}').setAttribute('download',this.value)"
                            onblur="ee('check_url{php}echo $idx; {/php}').innerHTML=htmlEscape(this.value); ee('check_url{php}echo $idx; {/php}').setAttribute('download',this.value)" /></td>
                    </tr>
                    </table>
                    <div style="padding:7px 0">
                    <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                    <tr>
                        <td><span class="brown">確認リンク</span></td>
                        <td><a href="javascript:void(0)" onclick="return checkUrl({php}echo $idx; {/php})"
                            target="_blank" id="check_url{php}echo $idx; {/php}"></a></td>
                    </tr>
                    </table>
                    </div>
                </div>

                <script type="text/javascript">
                    var ridx = {php}echo $idx; {/php};
                    if (ee("file_url"+ridx).value=="") ee("file_whichA"+ridx).checked = true;
                    else ee("file_whichB"+ridx).checked = true;
                    ee("check_url"+ridx).innerHTML = htmlEscape(ee("file_name"+ridx).value);
                    ee("check_url"+ridx).setAttribute("href", htmlEscape(ee("file_url"+ridx).value));
                    ee("check_url"+ridx).setAttribute('download', htmlEscape(ee("file_name"+ridx).value));
                    rdoChange(ridx);
                </script>
            </td>
        </tr>
        {php} } {/php}

    </table>
    <div style="padding:5px">
        <span style="color:#888888">*添付ファイルを一度に複数アップロードする場合は、一回の送信あたりの合計を{$upload_max_filesize}Bまでとしてください。</span>
    </div>
    </div>




    <!-- 下部ボタン START -->
    <div style="padding:5px 0; text-align:right">
        <table cellspacing="0" cellpadding="0" style="width:100%"><tr>
        <td>
            <div id="div_wait" style="display:none">
                <div style="background-color:#ffffaa; border:2px solid #888833; text-align:center; color:#888833; padding:4px;">
                    <div>実行中</div>
                    <div style="padding-top:6px; font-size:12px">（アップロードファイルサイズによっては、時間がかかる場合があります。）</div>
                </div>
            </div>
        </td>
        <td style="vertical-align:top; text-align:right">
            <span style="padding-right:50px">
            <button type="button" id="btn_delete" class="btn-main{php}echo $btnOff{/php}" onclick="delete_contents(this)" style="width:50px">削除</button>
            </span>
            {if $event eq "ev_edit_contents" }
            <button type="button" id="btn_copy" class="btn-main{php}echo $btnOff2{/php}" onclick="copy_contents(this)" navititle="研修講義の複製"
                navi="この「研修・講義」を新規作成します。以下があわせて複製されます。
            ・研修・講義登録
            ・受講者
            ・アンケート
            ・テスト
            ※同一の講座には、同一の研修講義タイトルを指定できません。研修講義タイトルまたは講座を変更してください。
            ">&nbsp;複製&nbsp;</button>
            <button type="button" id="btn_update" class="btn-main{php}echo $btnOff2{/php}" onclick="update_contents(this)" style="width:50px">更新</button>
            {else}
            <button type="button" id="btn_create" class="btn-main{php}echo $btnOff2{/php}" onclick="create_contents(this)" style="width:50px">登録</button>
            {/if}
        </td>
        </tr></table>
    </div>


    <div style="height:40px">&nbsp;</div>


</form>
</div>





{/if}

</body>
</html>