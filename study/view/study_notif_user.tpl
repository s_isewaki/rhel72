{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}お知らせ</title>
</head>
<body onresize="resized()">
<!-- ///study_notif_user  -->




<script type="text/javascript">
    var cur_page_num = {$page_num};
    var cur_per_page_size = {$per_page_size};
    var total_count = {$total_count};
    var last_page = {$last_page};
    {literal}
    function paging(mode) {
        var page_num = parseInt($("#page_num").val());
        var new_page_num = parseInt($("#new_page_num").val());
        if (last_page < 1) last_page = 1;
        if (!page_num || isNaN(page_num)) page_num = 1;
        if (mode=="next") $("#page_num").val(++cur_page_num);
        if (mode=="prev") $("#page_num").val(--cur_page_num);
        if (mode=="top") $("#page_num").val(1);
        if (mode=="last") $("#page_num").val(last_page);
        if (mode=="move" && !parseInt(new_page_num,10)) return alert("ページ番号を指定してください。");
        if (mode=="move" && parseInt(new_page_num,10) > last_page) $("#page_num").val(last_page);
        if (mode=="move" && parseInt(new_page_num,10) <= last_page) $("#page_num").val(new_page_num);
        if (mode) document.mainform.submit();
    }
    // お知らせ編集画面をリードオンリーで
    function show_notif_refer(contents_id, notif_id) {
        var url = "study_notif_admin.php?session="+session+"&contents_id="+contents_id+"&event=ev_open_edit&preview_flg=t&notifid="+notif_id;
        window_open(url, "edit_notification", 820, 370);
    }
    {/literal}
</script>





{* コンテンツヘッダー *}
{php} $current_screen = "user@study_notif_user"; require_once("view/contents_header.tpl"); {/php}





<div style="padding-top:5px">
<form name="mainform" method="POST">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <input type="hidden" name="event" id="event" value="" />





    <div style="padding-bottom:5px">
        <table celllspacing="0" cellpadding="0" class="rgroup"><tr>



        <td style="letter-spacing:0"><nobr>
            <span style="padding-right:20px; font-size:11px;">該当<span style="font-size:16px; color:#dd0000; padding:0 2px"><b>{$total_count}</b></span>件</span>
        </td>

        <td>
        <table celllspacing="0" cellpadding="0" class="rgroup"><tr>
            <th><input type="checkbox" id="is_show_archive" name="is_show_archive" value="1"
                onclick="document.mainform.submit();"{if $is_show_archive} checked{/if} /></th>
            <td><label for="is_show_archive">既読も表示</label></td>
        </tr></table>
        </td>

        <td style="padding-left:20px"><nobr>
            <span style="font-size:12px">表示件数</span>
            <select name="per_page_size" onchange="$('#page_num').val(1); document.mainform.submit()">
                {php}
                    $per_page_size = (int)$this->_tpl_vars["per_page_size"];
                    $ary = array(5,10,20,30,40,50,100,200,300,500,-1);
                    foreach ($ary as $v) {
                        echo '<option value="'.$v.'"    '.($v==$per_page_size?" selected":"").'>'.($v==-1?"全件":$v."件").'</option>';
                    }
                {/php}
            </select></nobr>
       </td>



        <td style="text-align:right; font-size:12px"><nobr>
            {php}
                $page_num = $this->_tpl_vars["page_num"];
                $last_page = $this->_tpl_vars["last_page"];
                $dis = ($page_num<=1?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <input type="hidden" name="page_num" id="page_num" value="<?=$page_num?>" />
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('top')"{php}echo $dis{/php}>≪</button>
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('prev')"{php}echo $dis{/php}>＜</button>
            <input type="text" class="text imeoff" maxlength="4" style="width:40px" id="new_page_num" value="{$page_num}"
            ><span style="font-size:12px; letter-spacing:0; padding-left:1px">ページ</span>
            {php}
                $dis = ($last_page<=1?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:38px" onclick="paging('move')"{php}echo $dis{/php}>移動</button>
            {php}
                $dis = ($page_num>=$last_page?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('next')"{php}echo $dis{/php}>＞</button>
            {php}
                $dis = ($page_num>=$last_page?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('last')"{php}echo $dis{/php}>≫</button></nobr>
        </td>
        </tr></table>


    </div>








    <table cellspacing="0" cellpadding="0" class="list" style="table-layout:fixed">
        <tr>
            <th width="15" class="border0r"></th>
            <th width="auto" class="border0l">タイトル</th>
            <th width="100">更新日</th>
            <th style="width:300px">研修・講義</th>
            <th width="150">登録者</th>
        </tr>
        {foreach from=$notif_list item=item}
            <tr class="color_hover{if $item.is_notif_confirmed} is_history{/if}">
                {if $item.is_notif_confirmed}
                <td class="border0r is_history center"><nobr>済</nobr></td>
                {else}
                <td class="border0r center" style="color:#ffaaaa"><nobr></nobr></td>
                {/if}
                <td class="border0l" style="padding-left:4px">
                <a href="javascript:void(0)" onclick="show_notif_refer('{$item.contents_id}','{$item.id}')">{$item.title|escape}</a>
                </td>
                <td class="center">{$item.display_notif_ymd|escape}</td>
                <td>{$item.folder_joint_name|escape}＞{$item.contents_name|escape}</td>
                <td>{$item.emp_lt_nm|escape} {$item.emp_ft_nm|escape}</td>
            </tr>
        {/foreach}
    </table>

    {if not $notif_list|@count}
    <div style="padding:10px 5px" class="gray2">該当データは存在しません。</div>
    {/if}


</form>
</div>
</body>
</html>