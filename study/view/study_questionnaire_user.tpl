{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}アンケート</title>
</head>
<body onresize="resized()">
<!-- ///study_questionnaire_user  -->



<script type="text/javascript">
    {literal}
    function update_questionnaire(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var flg = false;
        var num = 1;

        $("[name^=required]").each(function(){
            var hid = $(this).val();
            var type = $('[name^="survey' + num + '"]').attr("type");
            var value = "";
            if (type == "checkbox") {
                 value = $('[name="survey' + num + '[]"]:checked').val();
            }else if (type == "radio"){
                 value = $('input[name="survey' + num + '"]:checked').val();
            }else{
                 value = $('[name="survey' + num + '"]').val();
            }
            if ((hid == "1") && (value == undefined || trim(value) == "")){
                flg = true;
                return false;
            }
            num++;
        });
        if (flg) { alert("回答必須の項目が入力されていません。"); return; }
        if (!confirm("登録します。よろしですか？")) return;
        document.mainform.btn_event.value = "be_exec_questionnaire_user";
        document.mainform.submit();
    }
    function clear_questionnaire(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        if (!confirm("入力内容をクリアします。よろしですか？")) return;
        $("textarea").each(function(){ this.value = ""; });
        $("input[type=radio]").each(function(){ this.checked = false; });
    }

    $(document).ready(function() {
        // 登録に成功していたら親画面をリロードして子画面を閉じる
        if ("true" == $("[name=success]").val()) {
            parentRefreshRequest("mainform");
            window.close();
        }
    });
    {/literal}
</script>





<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>アンケート</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="{$contents_title|escape}" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>





{if $target_emp_name}
    <div class="findbox"><span class="brown">回答者</span>&nbsp;&nbsp;{$target_emp_name|escape}</div>
{else}
    {if $is_out_of_date}
    <div style="padding:5px"><div class="warningbox">アンケート回答期間ではありません。登録できません。</div></div>
    {else}
    <div style="padding:20px 5px 0px 10px;">以下の設問に回答し、画面最下部の「登録」ボタンを押してください。</div>
    {/if}
{/if}



<div class="usual_box">
<form name="mainform" action="" method="post">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <input type="hidden" name="btn_event" value="be_exec_questionnaire_user" />
    <input type="hidden" name="success" value="{$success}" />
    <input type="hidden" name="contents_id" value="{$contents_id}" />
    <input type="hidden" name="item_count" value="{$item_count}" />



    <table id="all_content" width="100%" border="0" cellspacing="0" cellpadding="0">
        {foreach from=$questionnaire key=keys item=items }
            <tr id="content{$items.no}" name="content{$items.no}" class="position">
                <td>
                    <p id="p{$items.no}" class="test_question">
                            {php}
                               $question_br = str_replace("\n", "<br>",h($this->_tpl_vars['items']["question"]));
                               $question_convert = str_replace(" ", "&nbsp;",$question_br);
                               echo $question_convert;
                            {/php}
                        
                        <!--{$items.question|escape|nl2br}-->
                    {if $items.required eq "t"}
                        <br /><span style="color:#ff0000"><b> ※(回答必須)</b></span>
                    {/if}
                    </p>

                    <input type="hidden" name="required{$items.no}" value="{if $items.required eq "t"}1{else}0{/if}" />

                    <div class="test_answer1"><div class="test_answer2">
                    <table cellspacing="0" cellpadding="0" class="rgroup">


                    {php}
                        $choices = explode(",", $this->_tpl_vars['items']["choices"]);
                        foreach ($choices as $idx=>$c_item) {
                            $num = $idx+1;
                    {/php}
                        {if $items.type eq 1}
                            <tr>
                            <th><input type="radio" id="survey{$items.no}_{php} echo $num; {/php}" name="survey{$items.no}"
                                value="{php} echo $num; {/php}"{php} if($num==$this->_tpl_vars['items']["impression"]) echo " checked"; {/php}></th>
                            <td><label for="survey{$items.no}_{php} echo $num; {/php}">{php} echo h($c_item) {/php}</label></td>
                            </tr>
                        {elseif $items.type eq 2}
                            <tr>
                            <th><input type="checkbox" id="survey{$items.no}_{php} echo $num; {/php}" name="survey{$items.no}[]"
                                value="{php} echo $num; {/php}"{php} if(in_array($num, explode(",",$this->_tpl_vars['items']["impression"]))) echo " checked"; {/php}></th>
                            <td><label for="survey{$items.no}_{php} echo $num; {/php}">{php} echo h($c_item) {/php}</label></td>
                            </tr>
                        {else}
                            <tr><th><textarea name="survey{$items.no}" cols=70 rows=4>{$items.impression}</textarea></th></tr>
                        {/if}
                    {php} } {/php}
                    </table>
                    </div></div>
                </td>
            </tr>
        {/foreach}
    </table>


    <div style="text-align:right; padding:10px 0 10px 0px;">
        <button type="button" class="btn-main{if $is_out_of_date or $target_emp_name} btn-off{/if}" id="btn_update"
            onclick="update_questionnaire(this)">登録</button>
    </div>




</form>
</div>
</body>
</html>
