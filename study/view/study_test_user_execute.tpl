{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}テスト実施</title>
</head>
<body onresize="resized(); adjustContentWidth()">
<!-- ///study_test_user_execute  -->




<script type="text/javascript">
    var is_out_of_date = "{$is_out_of_date}";
    var contents_id = "{$contents_id}";
    var shuffled_order = "{php} echo implode(",", $this->_tpl_vars["shuffled_order"]) {/php}";
    var test_item_count = {$test_items|@count};
    var timer_stop_request = "";
    var is_date_expired = "{$is_date_expired}";
    var is_out_of_date = "{$is_out_of_date}";

    {literal}
    function grading_click(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        grading();
    }
    function grading() {
        var answers = [];
        var no_check_exist = false;
        for (var idx=1; idx<=test_item_count; idx++) {
            answers.push([]);
            $("input[name^=answers_"+idx+"]:checked").each(function() {
            	var array_answer = this.id.split("_");
            	if (idx == array_answer[1]) {
                	answers[idx-1].push(this.value);
            	}
            });
            if (!answers[idx-1].length) no_check_exist = true;
            answers[idx-1] = answers[idx-1].join(",");
        }
        if (no_check_exist) {
            if (!confirm("全ての問題に答えていませんが、このまま採点しますか？")) return;
        }
        $("#grading1").attr("disabled", "disabled").addClass("btn-off");
        timer_stop_request = 1;
        ee("td_notice_for_btn").style.visibility = "hidden";
        ee("td_notice_for_btn").style.border = "1px solid #b1f1af";

        var url =
        "study_test_user.php?session="+session+"&btn_event=be_grading&contents_id="+contents_id+
        "&shuffled_order="+shuffled_order+"&user_answers="+answers.join("|");
        window_open(url, "test_result", 1000, 600);
    }
    var base_now_id = "";
    function show_test(id, count, flg){
        var now_id = base_now_id;
        var show_id;

        if ((id == 0) && (count > 1)){
            show_id = (now_id == "") ? 1 : now_id;

            if (flg == "prev") { show_id--; }
            if (flg == "next") { show_id++; }

        }else{
            show_id = id;
        }

        for (var i = 1; i <= count; i++) {
            if (show_id == i){
                base_now_id = show_id;
                ee("obj" + i).style.display = "inline";
            }else{
                ee("obj" + i).style.display = "none";
            }
        }
        // 前のページ
        btnDisabled("btn_prev", (show_id <= 1 ? true : false));
        btnDisabled("btn_next", (show_id < count ? false : true));
    }

    function adjustContentWidth() {
        var w = ee("title_area").offsetWidth;
        if (w) ee("test_area").style.width = (w-12)+"px";
    }

    var timer = 0;
    $(document).ready(function() {
         set_auto_session_update();  // 20141203TN
        // タイマー値を取得
        if ($("#timer").get(0)) {
            timer = $("#timer").text();
            timer *= 60;
        }
        $("input[name^=answers_]:checked").each(function() {
            this.checked = false;
        });

        if (ee("grading1").disabled) {
            $("#grading1").addClass("btn-off");
            timer_stop_request = 1;
            ee("td_notice_for_btn").style.visibility = "hidden";
            ee("td_notice_for_btn").style.border = "1px solid #b1f1af";
        }

        var result = $("[name=open_result]").val();
        if (result == "true") {
            $("#grading1").attr("disabled", "disabled").addClass("btn-off");
            timer = 0;  // タイマー停止
            $("#timer").text("0：00");
            ee("td_notice_for_btn").style.visibility = "hidden";
            ee("td_notice_for_btn").style.border = "1px solid #b1f1af";
        }
        if (timer > 0) {
            ee("test_wrapper").style.display = "";
            start_countdown();
        } else {
            ee("test_wrapper").style.display = "";
        }
        resized();
    });
    function start_countdown() {
        ee("test_opening").style.display = "none";
        ee("test_wrapper").style.display = "";
        resized();
        count_down();
    }

    // タイマーカウントダウン
    function count_down() {
        var hour = Math.floor(timer / 3600);
        var minute = Math.floor((timer % 3600) / 60);
        var second = timer % 60;
        var time_str = '';
        if (hour) time_str = hour + "：";
        time_str += minute + "：" + ("0" + second).slice(-2);
        $("#timer").text(time_str);
        if (is_date_expired) return;
        if (is_out_of_date) return;
        if (timer_stop_request) return;
        if (timer == 0) {
            alert("テストが終了しました。");
            grading();
            return;
        }
        timer--;
        setTimeout("count_down()", 1000);
    }
    {/literal}
</script>

<!-- セッションタイムアウト防止 START // 20141203TN -->
<script type="text/javascript">
    {literal}
    function set_auto_session_update(){
        setTimeout(auto_session_update,60000);
    }
    
    function auto_session_update(){
        document.session_update_form.submit();
        setTimeout(auto_session_update,60000);
    }
    {/literal}
</script>
<form name="session_update_form" action="study_session_update.php" method="post" target="session_update_frame">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
</form>
<iframe name="session_update_frame" width="0" height="0" frameborder="0"></iframe>
<!-- セッションタイムアウト防止 END -->




<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>テスト実施</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="{$contents.title|escape}" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>





{if not $is_date_expired and $is_out_of_date}
    <div style="padding:5px"><div class="warningbox">テスト期間外です。問題の閲覧のみ行えます。</div></div>
{/if}
{if $is_date_expired}
    <div style="padding:5px"><div class="warningbox">テスト期間は終了いたしました。問題の閲覧のみ行えます。</div></div>
{/if}

<div id="test_wrapper" style="display:none">
<div class="usual_box">



    <!--本体-->
    <table cellspacing="0" cellpadding="0" class="inputlist">
        <!-- テスト期間 -->
        <tr>
            <th width="90">テスト期間</th>
            <td>{$test_start_date_jp} 〜 {$test_end_date_jp}</td>
        </tr>
    </table>





    {* 制限時間が設定されていればタイマー表示 *}
    <div style="padding:5px 0">
        <div class="section_title">
            <table cellspacing="0" cellpadding="0" class="rgroup" style="width:100%"><tr>
                <td style="width:auto"><b>合計<span style="padding:0 5px">{php} echo count($this->_tpl_vars['test_items']);{/php}</span>問</b></td>
                {if $test_info.limit_time}
                    <td style="width:auto"><b>制限時間&nbsp;&nbsp;&nbsp;
                        <span id="timer" style="font-size:16px; letter-spacing:3px">{$test_info.limit_time}</span></b></td>
                {/if}

{if not $is_date_expired and $is_out_of_date}
{else}
{if $is_date_expired}
{else}
                <td id="td_notice_for_btn" style="width:300px; text-align:right; background-color:#ee88d7; color:#ffffff; border:1px solid #d170b9"><nobr>※テスト完了後、必ず採点ボタンを押してください。</nobr></td>
{/if}
{/if}
                <td style="width:50px; text-align:right"><nobr><button type="button" class="btn-main{if $is_out_of_date} btn-off{/if}"
                    id="grading1" onclick="grading_click(this);">採点</button></nobr></td>
            </tr></table>
        </div>
    </div>





    <div style="padding:5px 0">
        <div id="test_area" style="overflow-y:scroll; overflow-x:hidden; border:1px solid #ffb56a; position:relative" class="scroll_div" heightMargin="2">
            <div style="padding:20px 5px">


                <!-- 問題(可変) -->
                {section name=test_item loop=$test_items }
                <!-- ボタン -->
                <div class="content" id="obj{$smarty.section.test_item.index_next}">
                    <div class="beige">
                        <div style="padding-bottom:4px"><b>問題{$smarty.section.test_item.index_next}</b></div>
                        <div style="background-color:#ffffe5; padding:10px; border:1px solid #eeeeee; border-bottom:1px solid #dddddd;word-break: break-all;"
                            >{$test_items[test_item].question|escape|nl2br}</div>
                    </div>

                    <div style="padding:10px 0 26px 0">
                        {if $test_items[test_item].image_path_url ne "" }
                        <div style="padding-bottom:5px"><img src="{$test_items[test_item].image_path_url}" alt="" /></div>
                        {/if}
                        <table cellspacing="0" cellpadding="0" class="rgroup" style="width:100%">
                        {section name=idx start=1 loop=$test_items[test_item].choice_cnt+1 step=1}
                        {assign var="index" value=$smarty.section.idx.index-1 }<tr>
                        {if $test_items[test_item].multiple eq "t"}
                            <th style="width:10px; vertical-align:top"><input id="answers_{$test_items[test_item].no}_{$smarty.section.idx.index}"
                            name="answers_{$test_items[test_item].no}[]" value="{$smarty.section.idx.index}" type="checkbox"></th>
                            <td><label style="display:block;word-break: break-all;" for="answers_{$test_items[test_item].no}_{$smarty.section.idx.index}"
                                >({$smarty.section.idx.index})&nbsp;{$test_items[test_item].choices[$index]|escape}</label></td>
                        {else}
                            <th style="width:10px; vertical-align:top"><input id="answers_{$test_items[test_item].no}_{$smarty.section.idx.index}"
                            name="answers_{$test_items[test_item].no}" value="{$smarty.section.idx.index}" type="radio"></th>
                            <td><label style="display:block;word-break: break-all;" for="answers_{$test_items[test_item].no}_{$smarty.section.idx.index}"
                                >({$smarty.section.idx.index})&nbsp;{$test_items[test_item].choices[$index]|escape}</label></td>
                        {/if}
                        </tr>
                        {/section}
                        </table>
                    </div>
                </div>
                {/section}

                <div style="padding:30px 0"><div class="section_title">（-- 以上 --）</div></div>

            </div>
        </div>
    </div>

</div>
</div>

<div id="test_opening" style="display:none">
    <div style="text-align:center; padding:40px 5px 10px 5px">テストを開始します。よろしいですか？</div>
    <div style="text-align:center; padding:0 5px 20px 5px">制限時間は&nbsp;&nbsp;{$test_info.limit_time}&nbsp;分です。</div>
    <div style="text-align:center; padding:20px 5px">
        <button type="button" class="btn-main" onclick="start_countdown()">開始する</button>
    </div>
</div>



</body>
</html>
