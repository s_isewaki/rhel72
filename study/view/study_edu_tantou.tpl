{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}教育担当者</title>
</head>
<body onresize="resized()">
<!-- ///study_edu_tantou  -->





<script type="text/javascript">
    folder_dropdown_blank_string = "　　　";
    var cur_page_num = {$page_num};
    var cur_per_page_size = {$per_page_size};
    var total_count = {$total_count};
    var last_page = {$last_page};
    {literal}
    function paging(mode) {
        var page_num = parseInt($("#page_num").val());
        var new_page_num = parseInt($("#new_page_num").val());
        if (last_page < 1) last_page = 1;
        if (!page_num || isNaN(page_num)) page_num = 1;
        if (mode=="next") $("#page_num").val(++cur_page_num);
        if (mode=="prev") $("#page_num").val(--cur_page_num);
        if (mode=="top") $("#page_num").val(1);
        if (mode=="last") $("#page_num").val(last_page);
        if (mode=="move" && !parseInt(new_page_num,10)) return alert("ページ番号を指定してください。");
        if (mode=="move" && parseInt(new_page_num,10) > last_page) $("#page_num").val(last_page);
        if (mode=="move" && parseInt(new_page_num,10) <= last_page) $("#page_num").val(new_page_num);
        if (mode) document.mainform.submit();
    }
    function showHistory(emp_id) {
        var url = "study_menu_user.php?session="+session+"&is_popup=1&emp_id="+emp_id;
        window_open(url, "emp_history_view", 1000, 600);
    }

    // 職員選択画面を開く
    function open_emp_select() {
        var url = "study_student.php?btn_event=be_show_student_selector&js_callback=add_edu_tantou&session="+session;
        window_open(url, "emp_select", 800, 580);
    }
    function show_folder_selector(emp_id) {
        document.mainform.target_emp_id.value = emp_id;
        var url = "study_folder.php?event=ev_pickup_folder&session="+session+"&js_callback=callbackFolderSelectedForEduTantou&call_from=study_edu_tantou";
        window_open(url, "folder_edit", 600, 380);
    }
    function callbackFolderSelectedForEduTantou(folder_id) {
        document.mainform.target_folder_id.value = folder_id;
        document.mainform.btn_event.value = "be_add_employee_folder";
        document.mainform.submit();
    }
    function show_edu_tantou_branch(emp_id) {
        var url = "study_edu_tantou.php?btn_event=be_show_edu_tantou_branch&session="+session+"&target_emp_id="+emp_id;
        window_open(url, "branch_edit", 600, 380);
    }
    function delete_folder_select(emp_id, folder_id) {
        if (!confirm("この講座への権限を削除します。よろしいですか？")) return;
        document.mainform.target_folder_id.value = folder_id;
        document.mainform.target_emp_id.value = emp_id;
        document.mainform.btn_event.value = "be_del_employee_folder";
        document.mainform.submit();
    }

    function delete_branch_select(emp_id, branch_id) {
        if (!confirm("この所属への権限を削除します。よろしいですか？")) return;
        document.mainform.target_branch_id.value = branch_id;
        document.mainform.target_emp_id.value = emp_id;
        document.mainform.btn_event.value = "be_del_employee_branch";
        document.mainform.submit();
    }

    // 職員のチェックボックスを制御
    function allCheckEmp(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        var caption = $("#all_check_emp").html();
        var ary = document.getElementsByName("emp_id");
        var bool = (caption == "選択");
        for (var idx=0; idx<ary.length; idx++) ary[idx].checked = bool;
        $("#all_check_emp").html(bool?"解除":"選択");
    }
    function add_edu_tantou(emp_id_list) {
        document.mainform.emp_id_comma_list.value = emp_id_list.join(",");
        document.mainform.btn_event.value = "be_add_employee";
        document.mainform.submit();
    }
    function delete_edu_tantou(btn) {
        if (btn.className.indexOf("btn-off")>=0) return;
        if ($("input:checkbox[name='emp_id']:checked").size() == 0) {
            alert("削除対象の職員チェックボックスが選択されていません。");
            return;
        }
        var ary = document.getElementsByName("emp_id");
        var emp_id_list = [];
        for (var idx=0; idx<ary.length; idx++) {
            var elem = ary[idx];
            if (elem.checked) emp_id_list.push(ary[idx].value);
        }
        if (!confirm("チェックを行った職員を削除します。よろしいですか?")) return;

        ee("emp_id_comma_list").value = emp_id_list.join(",");
        document.mainform.btn_event.value = "be_del_employee";
        document.mainform.submit();
    }
    {/literal}
</script>




{* コンテンツヘッダー *}
{php} $current_screen = "admin@study_edu_tantou"; require_once("view/contents_header.tpl"); {/php}




<div style="padding-top:5px">
<form name="mainform" method="POST">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <input type="hidden" name="event" id="event" value="" />
    <input type="hidden" name="btn_event" value="" />
    <input type="hidden" name="target_folder_id" value="" />
    <input type="hidden" name="target_branch_id" value="" />
    <input type="hidden" name="target_emp_id" value="" />
    <input type="hidden" name="folder_selected_ids" id="folder_selected_ids" value="{$folder_selected_ids}" />
    <input type="hidden" name="emp_id_comma_list" id="emp_id_comma_list" value="" />


    <div style="padding-bottom:5px">
        <table cellspacing="0" cellpadding="0" class="rgroup" style="width:100%"><tr>
            <td>
                <button type="button" class="btn-main" id="select_student" onclick="open_emp_select();">追加</button>
                <button type="button" class="btn-main" id="select_student" onclick="delete_edu_tantou(this);">削除</button></td>
        </tr></table>
    </div>

    <div class="findbox" style="border-top:1px solid #c0c0c0">
        <table cellspacing="0" cellpadding="0" class="rgroup">
            <tr>
                <td><span>担当先講座</span></td>
                <td>
                    <div id="ddl_folder_container"></div>
                    <script type="text/javascript">
                        folderChanged(0, 0);
                        {php}
                            $ids = explode("\t", @$this->_tpl_vars["folder_selected_ids"]);
                            for ($idx=1; $idx<=count($ids); $idx++) {
                                if ($ids[$idx-1]) echo 'folderChanged('.$idx.','.$ids[$idx-1].');';
                            }
                        {/php}
                    </script>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" class="rgroup" style="margin-top:5px; width:100%">
            <tr>
                <td>
                職員ID
                <input type="text" class="text imeoff" size="20" name="emp_personal_id" value="{$emp_personal_id|escape}" />
                <span style="padding-left:20px">職員名</span>
                <input type="text" class="text" size="20" name="emp_name" value="{$name|escape}" />
                </td>
                <td class="right">
        <button type="button" class="btn-main" id="search" onclick="document.mainform.submit()">検索</button>
                </td>
            </tr>
        </table>
    </div>


    <div style="padding-top:5px">
        <table celllspacing="0" cellpadding="0" class="rgroup"><tr>

        <td style="letter-spacing:0"><nobr>
            <span style="padding-right:20px; font-size:11px;">該当<span style="font-size:16px; color:#dd0000; padding:0 2px"><b>{$total_count}</b></span>人</span>
            <span style="font-size:12px">表示件数</span>
            <select name="per_page_size" onchange="$('#page_num').val(1); document.mainform.submit()">
                {php}
                    $per_page_size = (int)$this->_tpl_vars["per_page_size"];
                    $ary = array(5,10,20,30,40,50,100,200,300,500,-1);
                    foreach ($ary as $v) {
                        echo '<option value="'.$v.'"    '.($v==$per_page_size?" selected":"").'>'.($v==-1?"全件":$v."件").'</option>';
                    }
                {/php}
            </select>




        </td>


        <td style="text-align:right; font-size:12px"><nobr>
            {php}
                $page_num = $this->_tpl_vars["page_num"];
                $last_page = $this->_tpl_vars["last_page"];
                $dis = ($page_num<=1?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <input type="hidden" name="page_num" id="page_num" value="<?=$page_num?>" />
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('top')"{php}echo $dis{/php}>≪</button>
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('prev')"{php}echo $dis{/php}>＜</button>
            <input type="text" class="text imeoff" maxlength="4" style="width:40px" id="new_page_num" value="{$page_num}"
            ><span style="font-size:12px; letter-spacing:0; padding-left:1px" class="imeoff">ページ</span>
            {php}
                $dis = ($last_page<=1?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:38px" onclick="paging('move')"{php}echo $dis{/php}>移動</button>
            {php}
                $dis = ($page_num>=$last_page?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('next')"{php}echo $dis{/php}>＞</button>
            {php}
                $dis = ($page_num>=$last_page?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('last')"{php}echo $dis{/php}>≫</button></nobr>
        </td>
        </tr></table>
    </div>

    <div style="padding-top:5px">
        <table cellspacing="0" cellpadding="0" class="list">
            <tr>
                <th width="60"><button type="button" class="btn-sel{if $emp_list_count eq 0} btn-off{/if}" id="all_check_emp"
                    onclick="allCheckEmp(this)">選択</button></th>
                <th width="130">担当者職員ID</th>
                <th width="200">担当者職員氏名</th>
                <th width="30"></th>
                <th width="230">所属</th>
                <th width="30"></th>
                <th>講座</th>
            </tr>
        </table>

        {if isset($emp_list)}
        <div>
            <table cellspacing="0" cellpadding="0" class="list">
                {foreach from=$emp_list item=item}
                <tr>
                    <td width="60" class="center"><input name="emp_id" type="checkBox" value="{$item.emp_id}" /></td>
                    <td width="130">{$item.emp_personal_id}</td>
                    <td width="200">{$item.name|escape}</td>
                    <td width="30"><button type="button" class="btn-main" onclick="show_edu_tantou_branch('{$item.emp_id}')">＋</button></td>
                    <td width="230" class="top">
                        {php}
                            foreach ($this->_tpl_vars['item']['branches'] as $row) {
                                echo '<a href="javascript:void(0)" onclick="delete_branch_select(\''.$this->_tpl_vars['item']['emp_id'];
                                echo '\',\''.$row["branch_id"].'\')">'.$row["branch_disp"].'</a><br>';
                            }
                        {/php}
                    </td>
                    <td width="30"><button type="button" class="btn-main" onclick="show_folder_selector('{$item.emp_id}')">＋</button></td>
                    <td class="top">
                        {php}
                            foreach ($this->_tpl_vars['item']['folder_list'] as $row) {
                                echo '<a href="javascript:void(0)" onclick="delete_folder_select(\'';
                                echo $this->_tpl_vars['item']['emp_id'].'\',\''.$row["folder_id"].'\')">'.$row["folder_disp"].'</a><br>';
                            }
                        {/php}
                    </td>
                </tr>
                {/foreach}
            </table>
        </div>
        {/if}
    </div>

</form>
</div>
</body>
</html>