{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}所属選択</title>
    <link rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
    <script type="text/javascript" src="js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker.custom.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker-ja.js"></script>
</head>
<body onresize="resized()">
<!-- ///study_edu_tantou_branch  -->





<script type="text/javascript">
    var result = "{$result}";
    var gg_class_nm = "{php} echo str_replace('"', '', GG_CLASS_NM); {/php}";
    var gg_atrb_nm  = "{php} echo str_replace('"', '', GG_ATRB_NM); {/php}";
    var gg_dept_nm  = "{php} echo str_replace('"', '', GG_DEPT_NM); {/php}";
    var gg_room_nm  = "{php} echo str_replace('"', '', GG_ROOM_NM); {/php}";
    {literal}
    function exec_edu_tantou_branch() {
        if (ee("emp_class").value=="") return alert(gg_class_nm+"を指定してください。");
        if (ee("emp_attr").value=="" && ee("emp_dept").value!="") {
            return alert(gg_dept_nm+"を指定する場合は、"+gg_atrb_nm+"を指定してください。");
        }
        if (ee("emp_dept").value=="" && ee("emp_room").value!="") {
            return alert(gg_room_nm+"を指定する場合は、"+gg_dept_nm+"を指定してください。");
        }
        document.mainform.submit();
    }
    $(document).ready(function() {
        if (result=="error") { alert(message); return; }
        if (result=="success") { window.opener.document.mainform.submit(); window.close(); }
    });
    {/literal}
</script>




<div id="title_area"><table cellspacing="0" cellpadding="0"><tr>
    <th><nobr>所属選択</nobr></th>
    <td class="contents_title"><input type="text" class="contents_title" readonly value="" /></td>
    <td><a onclick="window.close();"></a></td>
</tr></table></div>
<div id="title_margin">&nbsp;</div>





<div class="usual_box">
<form name="mainform" action="" method="post" enctype="multipart/form-data">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <input type="hidden" name="btn_event" value="be_exec_edu_tantou_branch" />
    <input type="hidden" name="target_emp_id" value="{$target_emp_id}" />
    <input type="hidden" name="target_branch_id" value="{$target_branch_id}" />

    <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th width="50"><nobr>{$smarty.const.GG_CLASS_NM|escape}</nobr></th>
            <td><select name="emp_class" id="emp_class" onchange="ddlClassChanged(this)">
                    <option value="">全て</option>
                    {foreach from=$class_list item=class_item }
                        <option value="{$class_item.class_id}">{$class_item.class_nm|escape}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <th>{$smarty.const.GG_ATRB_NM|escape}</th>
            <td><select name="emp_attr" id="emp_attr" onchange="ddlAttrChanged(this)">
                    <option value="">全て</option>
                    {foreach from=$attr_list item=attr_item }
                        <option value="{$attr_item.atrb_id}">{$attr_item.atrb_nm|escape}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <th>{$smarty.const.GG_DEPT_NM|escape}</th>
            <td>
                <select name="emp_dept" id="emp_dept" onchange="ddlDeptChanged(this)">
                    <option value="">全て</option>
                    {foreach from=$dept_list item=dept_item }
                        <option value="{$dept_item.dept_id}">{$dept_item.dept_nm|escape}</option>
                    {/foreach}
                </select></nobr>
            </td>
        </tr>
        <tr style="{php} if (!GG_ROOM_NM) echo "display:none";{/php}">
            <th>{$smarty.const.GG_ROOM_NM|escape}</th>
            <td>
                <select name="emp_room" id="emp_room">
                    <option value="">全て</option>
                    {foreach from=$room_list item=room_item }
                        <option value="{$room_item.room_id}">{$room_item.room_nm|escape}</option>
                    {/foreach}
                </select></nobr>
            </td>
        </tr>
    </table>

    <div style="padding:5px 0; text-align:left" class="gray">
    指定所属より配下の所属へのアクセス権限を付与します。
    </div>
    <div style="text-align:right;">
        <button type="button" class="btn-main" onclick="exec_edu_tantou_branch()">選択</button>
    </div>

</form>
</div>



</body>
</html>
