{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}職員経験年数</title>
</head>
<body onresize="resized()">
    
    <script type="text/javascript">
        var cur_page_num = {$page_num};
        var cur_per_page_size = {$per_page_size};
        var total_count = {$total_count};
        var last_page = {$last_page};
        {literal}
            
        function paging(mode) {
            var page_num = parseInt($("#page_num").val());
            var new_page_num = parseInt($("#new_page_num").val());
            if (last_page < 1) last_page = 1;
            if (!page_num || isNaN(page_num)) page_num = 1;
            if (mode=="next") $("#page_num").val(++cur_page_num);
            if (mode=="prev") $("#page_num").val(--cur_page_num);
            if (mode=="top") $("#page_num").val(1);
            if (mode=="last") $("#page_num").val(last_page);
            if (mode=="move" && !parseInt(new_page_num,10)) return alert("ページ番号を指定してください。");
            if (mode=="move" && parseInt(new_page_num,10) > last_page) $("#page_num").val(last_page);
            if (mode=="move" && parseInt(new_page_num,10) <= last_page) $("#page_num").val(new_page_num);
            if (mode) document.mainform.submit();
        }     
             
             
        function export_staff() {
            $("#btn_event").val("export_csv");
            document.sub_main.submit();
        }

        function import_staff() {
        if ($("#csv").val() == "") { alert("ファイルが選択されていません。\n本画面の「エクスポート」より取得したファイルを指定してください。"); return; }
            if (!confirm("ファイルのインポートを行います。よろしいですか？")) return;
                convertAndCheckDateTime(1);
            
            $("#btn_event").val("import_csv");
            document.sub_main.submit();
        }
       {/literal} 
    </script>
    
    
    {* コンテンツヘッダー *}
    {php} $current_screen = "admin@study_edu_nensuu"; require_once("view/contents_header.tpl"); require_once ('StudyInitController.php');{/php}
    
    <div style="padding:3px 5px" navititle="経験年数エクスポート/インポート" navi="「エクスポート」:職員の経験年数を含むCSVファイルを出力します。<br>「インポート」:「エクスポート」より取得したCSVファイルのみ指定できます。">
    <form name="sub_main" action="" method="post" target="_self"  enctype="multipart/form-data">
        <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
        <!-- <input type="hidden" name="csv_file" value="" /> -->
        <input type="hidden" name="contents_id" value="{$contents_id}" />
        <nobr><span style="color:#de5836">経験年数インポート</span>
            <input type="file" name="csv" id="csv"> 
            <button type="submit" class="btn-main"  name="btn_event" value="import_csv" onclick="import_staff()">インポート</button>
            <button type="submit" class="btn-main"  name="btn_event" value="export_csv" onclick="export_staff()">エクスポート</button>
        </nobr>
    </form>
    </div>
   
    <!-- 検索領域start -->
    <form name="mainform" method="POST">
        <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
        <div style="padding:5px" class="findbox">
        <table cellspacing="0" cellpadding="0" class="rgroup">
            <tr>
                <th>{$smarty.const.GG_CLASS_NM|escape}</th>
                <td colspan="3">
                    <select name="class" id="emp_class" onchange="ddlClassChanged(this)">
                        <option value="">全て</option>
                        {foreach from=$class_list item=class_item }
                            <option value="{$class_item.class_id}"{if $class eq $class_item.class_id} selected{/if}>{$class_item.class_nm|escape}</option>
                        {/foreach}
                    </select>
                    <span style="padding-left:20px">{$smarty.const.GG_ATRB_NM|escape}</span>
                    <select name="attr" id="emp_attr" onchange="ddlAttrChanged(this)">
                        <option value="">全て</option>
                        {foreach from=$attr_list item=attr_item }
                            <option value="{$attr_item.atrb_id}"{if $attr eq $attr_item.atrb_id} selected{/if}>{$attr_item.atrb_nm|escape}</option>
                        {/foreach}
                    </select>
                    <span style="padding-left:20px">{$smarty.const.GG_DEPT_NM|escape}</span>
                    <select name="dept" id="emp_dept" onchange="ddlDeptChanged(this)">
                        <option value="">全て</option>
                        {foreach from=$dept_list item=dept_item }
                            <option value="{$dept_item.dept_id}"{if $dept eq $dept_item.dept_id} selected{/if}>{$dept_item.dept_nm|escape}</option>
                        {/foreach}
                    </select>
                    <span style="{php} if (!GG_ROOM_NM) echo "display:none";{/php}">
                    <span style="padding-left:20px">{$smarty.const.GG_ROOM_NM|escape}</span>
                    <select name="room" id="emp_room">
                        <option value="">全て</option>
                        {foreach from=$room_list item=room_item }
                            <option value="{$room_item.room_id}"{if $room eq $room_item.room_id} selected{/if}>{$room_item.room_nm|escape}</option>
                        {/foreach}
                    </select>
                    </span>
                    <script type="text/javascript">
                    ddlClassChanged(ee("emp_class"));
                    setSelection(ee("emp_attr"), "{$attr}");
                    ddlAttrChanged(ee("emp_attr"));
                    setSelection(ee("emp_dept"), "{$dept}");
                    ddlDeptChanged(ee("emp_dept"));
                    setSelection(ee("emp_room"), "{$room}");
                    </script>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" class="rgroup" style="margin-top:5px; width:100%">
            <tr>
                <td>
                職員ID
                <input type="text" class="text imeoff" size="20" name="emp_id" value="{$id|escape}" />
                <span style="padding-left:20px">職員名</span>
                <input type="text" class="text" size="20" name="emp_name" value="{$name|escape}" />
                </td>
                <td class="right">
                <button type="button" class="btn-main" id="search" onclick="document.mainform.submit()">検索</button>
                </td>
            </tr>
        </table>
        </div>

        
                
    <div style="padding:5px 0">
        <table celllspacing="0" cellpadding="0" class="rgroup"><tr>
                
        <td style="letter-spacing:0"><nobr>
            <span style="padding-right:20px; font-size:11px;">該当<span style="font-size:16px; color:#dd0000; padding:0 2px"><b>{$total_count}</b></span>人</span>
            <span style="font-size:12px">表示件数</span>
            <select name="per_page_size" onchange="$('#page_num').val(1); document.mainform.submit()">
                {php}
                    $per_page_size = (int)$this->_tpl_vars["per_page_size"];
                    $ary = array(5,10,20,30,40,50,100,200,300,500,-1);
                    foreach ($ary as $v) {
                        echo '<option value="'.$v.'"    '.($v==$per_page_size?" selected":"").'>'.($v==-1?"全件":$v."件").'</option>';
                    }
                {/php}
            </select>
        </td>


        <td style="text-align:right; font-size:12px"><nobr>
            {php}
                $page_num = $this->_tpl_vars["page_num"];
                $last_page = $this->_tpl_vars["last_page"];
                $dis = ($page_num<=1?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <input type="hidden" name="page_num" id="page_num" value="<?=$page_num?>" />
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('top')"{php}echo $dis{/php}>≪</button>
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('prev')"{php}echo $dis{/php}>＜</button>
            <input type="text" class="text imeoff" maxlength="4" style="width:40px" id="new_page_num" value="{$page_num}"
            ><span style="font-size:12px; letter-spacing:0; padding-left:1px" class="imeoff">ページ</span>
            {php}
                $dis = ($last_page<=1?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:38px" onclick="paging('move')"{php}echo $dis{/php}>移動</button>
            {php}
                $dis = ($page_num>=$last_page?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('next')"{php}echo $dis{/php}>＞</button>
            {php}
                $dis = ($page_num>=$last_page?" disabled":"");
                $btnOff = ($dis?" btn-off":"");
            {/php}
            <button type="button" class="btn-main{php} echo $btnOff{/php}" style="width:26px" onclick="paging('last')"{php}echo $dis{/php}>≫</button></nobr>
            
        </td>       
        </tr></table>
    </div>
            
    </form>             
    
        
    <table cellspacing="0" cellpadding="0" class="list">
        <tr>
          <!--  <th width="8%" rowspan="2">職員ID</th>-->
            <th width="15%" rowspan="2">職員氏名</th>
            <th width="15%" rowspan="2">職種</th>
            <th width="35%" colspan="3">職種経験年数</th>
            <th width="35%" colspan="3">勤続年数</th>
        </tr>
        <tr>
            <th width="10%" class="fontsize12"><nobr>開始年月日</nobr></th>
            <th width="5%"class="fontsize12"><nobr>離職月数</nobr></th>
            <th width="20%" class="fontsize12"><nobr>コメント</nobr></th>
            <th width="10%"class="fontsize12"><nobr>開始年月日</nobr></th>
            <th width="5%" class="fontsize12"><nobr>離職月数</nobr></th>
            <th width="20%" class="fontsize12"><nobr>コメント</nobr></th>
        </tr>
        {foreach from=$all_staff_career item=item}
        <tr> 
            <!-- <td width="120" class="center">{$item.emp_id}</td>-->
            <td>{$item.last_name|escape}&nbsp;{$item.first_name|escape}</td>
            <td>{$item.job_nm}</td>
            <td>{$item.exp_start|escape|strtotime|date_format:"%Y/%m/%d"}</td>
            <td>{$item.exp_minus}</td>
            <td>{$item.exp_coment}</td>
            <td>{$item.work_start|escape|strtotime|date_format:"%Y/%m/%d"}</td>
            <td>{$item.work_minus}</td>
            <td>{$item.work_coment}</td>
        </tr>
        {/foreach}
    </table>
    </div>
          
</body>