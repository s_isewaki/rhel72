{php} require_once("view/header.tpl"); {/php}
    <title>{$smarty.const.GG_TITLEBAR_TITLE}受講者CSVインポート</title>



</head>
<body onresize="resized()">
<!-- ///study_student_import_csv  -->



<script type="text/javascript">

    var result = "{$result}";
    var message = "{$message}";
    {literal}

    $(document).ready(function() {
        $("#import").click(function() {
            if ($("[name=csv]").val() == "") { alert("ファイルが選択されていません。"); return; }
            $("[name=btn_event]").val("be_exec_student_import_csv");
            document.mainform.submit();
        });
        if (result=="error") {
			document.getElementById("error_div").style.display = "";
            return;
        }
        if (result=="success") {
            grandParentRefreshRequest("mainform");
            window.opener.$("#search_student").trigger("click");
            window.close();
         }
    });
    {/literal}



</script>




<div id="title_area"><table cellspacing="0" cellpadding="0"><tr><th><nobr>受講者CSVインポート</nobr></th><td class="contents_title"><input type="text" class="contents_title" readonly value="" /></td><td><a onclick="window.close();"></a></td></tr></table></div>
<div id="title_margin">&nbsp;</div>

<div id="error_div" style="display:none; position:absolute; width:100%; height:100%;">
	<div id="shadowing" style="position:fixed; top: 0%; left: 0%; width: 100%; height: 100%; background-color: black; z-index:10; opacity:0.2; filter: alpha(opacity=20);-ms-filter:alpha(opacity=20); -moz-opacity:0.2; -khtml-opacity: 0.2;zoom:1;"></div>

	<div style="width:100%; text-align:center; padding-top:50px; position:relative; z-index:10">
	    <div style="width:400px; margin:auto; position:relative; background-color:#ffffff; text-align:center; border:1px solid #bbbbbb">
			<div style="text-align:right; padding:4px; background-color:#eeeeee;">
				<button type="button" name="cancel" class="btn-main" onClick="document.getElementById('error_div').style.display='none'">閉じる</button>
			</div>
			<div style="text-align:right; padding:10px; background-color:#ffffff; height:250px">
		    	<div style="color:#dd0000; padding-bottom:10px; text-align:left">入力されたデータに誤りがあります。</div>
			    <div class ="example10" style="height:200px; width:360px; overflow-y:scroll; padding:10px; text-align:left; border:1px solid #dddddd">
	                {$message}
	                
			     </div>
			</div>
	     </div>
	</div>
</div>


<div class="usual_box">
<form name="mainform" action="" method="post" enctype="multipart/form-data">
    <input type="hidden" name="session" value="{$smarty.const.GG_SESSION_ID}" />
    <input type="hidden" name="btn_event" value="" />
    <input type="hidden" name="contents_id" value="{$contents_id}" />

    <table cellspacing="0" cellpadding="0" class="inputlist">
        <tr>
            <th width="90"><nobr>CSVファイル</nobr></th>
            <td><input type="file" name="csv" /></td>
        </tr>
        <tr>
            <th>ID区分</th>
            <td>
                <table cellspacing="0" cellpadding="0" class="rgroup"><tr>
                <th><input type="radio" name="id_type" value="personal" id="personal"{if $id_type ne "login"} checked{/if}></th><td><label for="personal">職員ID</label></td>
                <th><input type="radio" name="id_type" value="login" id="login"{if $id_type eq "login"} checked{/if}></th><td><label for="login">ログインID</label></td>
                </tr></table>
            </td>
        </tr>
    </table>



    <div style="text-align:right; padding:5px 0 16px 0">
        <button type="button" class="btn-main" id="import">受講者登録</button>
    </div>



    <div style="background-color:#f5f5f5; padding:8px">
        <div style="padding-bottom:8px"><b>CSVのフォーマット</b></div>
        <div style="line-height:1.3">
            １行につき１つの職員ID（またはログインID）を指定してください。<br/>
            例)<br>
                &nbsp;&nbsp;0000000001<br>
                &nbsp;&nbsp;0000000002<br>
                &nbsp;&nbsp;.......<br>
            <div style="padding-top:6px">
            ※&nbsp;取り込み時は、最初のカンマまでを職員ID（ログインID）と認識し、カンマ以降は無視します。<br>
            以下は正しく取り込むことができます。<br/>
            例)<br>
                &nbsp;&nbsp;0000000001,山田　太郎<br>
                &nbsp;&nbsp;0000000002,鈴木　花子<br>
                &nbsp;&nbsp;.......
            </div>
        </div>
    </div>
</form>
</div>
</body>
</html>